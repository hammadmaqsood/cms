﻿using System;

namespace Aspire.UnitTests
{
	[AttributeUsage(AttributeTargets.Method)]
	public class AspireTestPriorityAttribute : Attribute
	{
		public uint Priority { get; }

		public AspireTestPriorityAttribute(uint priority)
		{
			this.Priority = priority;
		}
	}
}