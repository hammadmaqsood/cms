﻿using System;
using System.Threading;
using Xunit.Runners;

namespace Aspire.UnitTests
{
	internal class Program
	{
		private static readonly object ConsoleLock = new object();
		private static readonly ManualResetEvent Finished = new ManualResetEvent(false);
		public static bool ReCreateDatabase { get; private set; } = true;

		private static void Main()
		{
			using (var runner = AssemblyRunner.WithoutAppDomain(typeof(Program).Assembly.Location))
			{
				runner.OnDiscoveryComplete = OnDiscoveryComplete;
				runner.OnExecutionComplete = OnExecutionComplete;
				runner.OnTestPassed = OnTestPassed;
				runner.OnTestFailed = OnTestFailed;
				runner.OnTestSkipped = OnTestSkipped;

				Console.WriteLine("Discovering...");
				runner.Start(parallel: false);

				Finished.WaitOne();
				Finished.Dispose();
			}
		}

		private static void OnDiscoveryComplete(DiscoveryCompleteInfo info)
		{
			lock (ConsoleLock)
				Console.WriteLine($"Running {info.TestCasesToRun} of {info.TestCasesDiscovered} tests...");
		}

		private static void OnExecutionComplete(ExecutionCompleteInfo info)
		{
			lock (ConsoleLock)
				Console.WriteLine($"Finished: {info.TotalTests} tests in {Math.Round(info.ExecutionTime, 3)}s ({info.TestsFailed} failed, {info.TestsSkipped} skipped)");
			Finished.Set();
		}

		private static void OnTestPassed(TestPassedInfo info)
		{
			lock (ConsoleLock)
			{
				Console.ForegroundColor = ConsoleColor.Green;
				Console.WriteLine($"[PASSED] {info.TestDisplayName}: {Math.Round(info.ExecutionTime, 3)}s");
				Console.ResetColor();
			}
		}

		private static void OnTestFailed(TestFailedInfo info)
		{
			lock (ConsoleLock)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine($"[FAIL] {info.TestDisplayName}: {info.ExceptionMessage}");
				if (info.ExceptionStackTrace != null)
					Console.WriteLine(info.ExceptionStackTrace);
				Console.ResetColor();
			}
		}

		private static void OnTestSkipped(TestSkippedInfo info)
		{
			lock (ConsoleLock)
			{
				Console.ForegroundColor = ConsoleColor.Yellow;
				Console.WriteLine($"[SKIP] {info.TestDisplayName}: {info.SkipReason}");
				Console.ResetColor();
			}
		}
	}
}