﻿using System.Collections.Generic;
using System.Linq;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace Aspire.UnitTests
{
	public sealed class AspireTestPriorityOrderer : ITestCaseOrderer
	{
		public IEnumerable<TTestCase> OrderTestCases<TTestCase>(IEnumerable<TTestCase> testCases) where TTestCase : ITestCase
		{
			return testCases.OrderBy(tc => (tc.TestMethod.Method.GetCustomAttributes(typeof(AspireTestPriorityAttribute).AssemblyQualifiedName).Single() as AspireTestPriorityAttribute)?.Priority);
		}
	}
}