﻿using Aspire.BL.Core.IntegrationServices.AlliedBank;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.UnitTests.AlliedBankService;
using System;
using System.Linq;
using Xunit;

namespace Aspire.UnitTests.Facts
{
	[Collection(nameof(AspireCollections.AlliedBankService))]
	public sealed class AlliedBankService : BaseService
	{
		private new string UserName => base.UserName.ToString();
		private new string Password => base.Password.ToString();

		protected override string BankName => AspireConstants.AlliedBankName;

		protected override string ServiceName => "ABL";

		protected override IntegratedServiceUser.ServiceTypes ServiceTypeEnum => IntegratedServiceUser.ServiceTypes.AlliedBank;

		private ServiceSoapClient ServiceClient => new ServiceSoapClient();

		[Fact, AspireTestPriority(0)]
		public void InvalidCredentials()
		{
			var result = this.ServiceClient.BillInquiry(Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), "011234567890", null, null);
			Assert.Null(result);
		}

		[Theory, AspireTestPriority(1)]
		[MemberData(nameof(GetChallanNosDoesNotExists))]
		public void NotExistedChallanNos(string fullChallanNo)
		{
			var result = this.ServiceClient.BillInquiry(this.UserName, this.Password, fullChallanNo, null, null);
			var billInquiryResult = BillInquiryResult.Parse(result);
			Assert.Equal(BillInquiryResult.ResponseCodes.InvalidChallanNo, billInquiryResult.ResponseCode);
			Assert.Equal(BillInquiryResult.BillStatuses.Blocked, billInquiryResult.BillStatus);
		}

		[Theory, AspireTestPriority(2)]
		[MemberData(nameof(GetInvalidChallanNos))]
		public void InvalidChallanNos(string fullChallanNo)
		{
			var result = this.ServiceClient.BillInquiry(this.UserName, this.Password, fullChallanNo, null, null);
			var billInquiryResult = BillInquiryResult.Parse(result);
			Assert.Equal(BillInquiryResult.ResponseCodes.InvalidChallanNo, billInquiryResult.ResponseCode);
			Assert.Equal(BillInquiryResult.BillStatuses.Blocked, billInquiryResult.BillStatus);
		}

		[Theory, AspireTestPriority(3)]
		[MemberData(nameof(GetChallanNosNotPaid))]
		public void ChallanDetailsNotPaid(AspireFormats.ChallanNo challanNo)
		{
			using (var aspireContext = new AspireContext())
			{
				switch (challanNo.FeeTypeEnum)
				{
					case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
						DeleteAccountTransactionCandidateAppliedProgram(aspireContext, challanNo.ChallanID);
						break;
					case InstituteBankAccount.FeeTypes.NewAdmissionFee:
					case InstituteBankAccount.FeeTypes.StudentFee:
						DeleteAccountTransactionStudentFeeChallan(aspireContext, challanNo.ChallanID);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			var result = this.ServiceClient.BillInquiry(this.UserName, this.Password, challanNo.ToString(), null, null);
			var billInquiryResult = BillInquiryResult.Parse(result);
			Assert.Contains(billInquiryResult.ResponseCode, new[] { BillInquiryResult.ResponseCodes.ChallanNoCanBePaid, BillInquiryResult.ResponseCodes.ChallanNoCannotBePaid });
			Assert.NotEmpty(billInquiryResult.ConsumerDetail);
			Assert.NotEqual(BillInquiryResult.BillStatuses.Paid, billInquiryResult.BillStatus);
			Assert.NotNull(billInquiryResult.DueDate);
			Assert.NotNull(billInquiryResult.AmountWithinDueDate);
			Assert.Null(billInquiryResult.AmountAfterDueDate);
			Assert.Equal(billInquiryResult.DueDate.Value.TruncateMonth(), billInquiryResult.BillingMonth);
			Assert.Null(billInquiryResult.DatePaid);
			Assert.Null(billInquiryResult.AmountPaid);
			Assert.True(string.IsNullOrWhiteSpace(billInquiryResult.TranAuthID));
			Assert.True(string.IsNullOrWhiteSpace(billInquiryResult.Reserved));
		}

		[Theory, AspireTestPriority(4)]
		[MemberData(nameof(GetChallanNosPaid))]
		public void ChallanDetailsPaid(AspireFormats.ChallanNo challanNo)
		{
			var result = this.ServiceClient.BillInquiry(this.UserName, this.Password, challanNo.ToString(), null, null);
			var billInquiryResult = BillInquiryResult.Parse(result);
			Assert.Equal(BillInquiryResult.ResponseCodes.ChallanNoCannotBePaid, billInquiryResult.ResponseCode);
			Assert.NotEmpty(billInquiryResult.ConsumerDetail);
			Assert.Equal(BillInquiryResult.BillStatuses.Paid, billInquiryResult.BillStatus);
			Assert.NotNull(billInquiryResult.DueDate);
			Assert.NotNull(billInquiryResult.AmountWithinDueDate);
			Assert.Null(billInquiryResult.AmountAfterDueDate);
			Assert.Equal(billInquiryResult.DueDate.Value.TruncateMonth(), billInquiryResult.BillingMonth);
			Assert.NotNull(billInquiryResult.DatePaid);
			Assert.NotNull(billInquiryResult.AmountPaid);
			//Assert.Empty(billInquiryResult.TranAuthID);
			Assert.True(string.IsNullOrWhiteSpace(billInquiryResult.Reserved));
		}

		[Theory, AspireTestPriority(6)]
		[MemberData(nameof(GetChallanNosDoesNotExists))]
		public void UpdateChallanChallanNoDoesNotExists(string challanNoString)
		{
			var billPaymentResultString = this.ServiceClient.BillPayment(this.UserName, this.Password, challanNoString, Guid.NewGuid().ToString("N").Substring(0, 6), "+012345678900", DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("HHmmss"), null, "CASH|0118|0051");
			var billPaymentResult = BillPaymentResult.Parse(billPaymentResultString);
			Assert.Equal(BillPaymentResult.ResponseCodes.ChallanNoNotFound, billPaymentResult.ResponseCode);
			Assert.True(string.IsNullOrWhiteSpace(billPaymentResult.Reserved));
			Assert.True(string.IsNullOrWhiteSpace(billPaymentResult.IdentificationParameter));
		}

		[Theory, AspireTestPriority(6)]
		[MemberData(nameof(GetChallanNosPaid))]
		public void UpdateChallanAlreadyPaid(AspireFormats.ChallanNo challanNo)
		{
			var billPaymentResultString = this.ServiceClient.BillPayment(this.UserName, this.Password, challanNo.ToString(), Guid.NewGuid().ToString("N").Substring(0, 6), "+012345678900", DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("HHmmss"), null, "CASH|0118|0051");
			var billPaymentResult = BillPaymentResult.Parse(billPaymentResultString);
			Assert.Equal(BillPaymentResult.ResponseCodes.DuplicateTransaction, billPaymentResult.ResponseCode);
			Assert.True(string.IsNullOrWhiteSpace(billPaymentResult.Reserved));
			Assert.True(string.IsNullOrWhiteSpace(billPaymentResult.IdentificationParameter));
		}

		[Theory, AspireTestPriority(7)]
		[MemberData(nameof(GetChallanNosNotPaid))]
		public void UpdateChallanLastDueDatePassed(AspireFormats.ChallanNo challanNo)
		{
			using (var aspireContext = new AspireContext())
			{
				var yesterday = DateTime.Today.AddDays(-1);
				switch (challanNo.FeeTypeEnum)
				{
					case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
						this.SetDueDateCandidateAppliedProgram(aspireContext, challanNo.ChallanID, yesterday);
						break;
					case InstituteBankAccount.FeeTypes.NewAdmissionFee:
					case InstituteBankAccount.FeeTypes.StudentFee:
						this.SetDueDateStudentFeeChallan(aspireContext, challanNo.ChallanID, yesterday);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			var billPaymentResultString = this.ServiceClient.BillPayment(this.UserName, this.Password, challanNo.ToString(), Guid.NewGuid().ToString("N").Substring(0, 6), "+012345678900", DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("HHmmss"), null, "CASH|0118|0051");
			var billPaymentResult = BillPaymentResult.Parse(billPaymentResultString);
			Assert.Equal(BillPaymentResult.ResponseCodes.ProcessingFailed, billPaymentResult.ResponseCode);
			Assert.True(string.IsNullOrWhiteSpace(billPaymentResult.Reserved));
			Assert.True(string.IsNullOrWhiteSpace(billPaymentResult.IdentificationParameter));
		}

		[Theory, AspireTestPriority(8)]
		[MemberData(nameof(GetChallanNosNotPaid))]
		public void UpdateChallanInvalidAmount(AspireFormats.ChallanNo challanNo)
		{
			int amount;
			using (var aspireContext = new AspireContext())
			{
				var dueDate = DateTime.Today.AddDays(0);
				switch (challanNo.FeeTypeEnum)
				{
					case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
						amount = SetDueDateCandidateAppliedProgram(aspireContext, challanNo.ChallanID, dueDate).Amount;
						break;
					case InstituteBankAccount.FeeTypes.NewAdmissionFee:
					case InstituteBankAccount.FeeTypes.StudentFee:
						amount = this.SetDueDateStudentFeeChallan(aspireContext, challanNo.ChallanID, dueDate).Amount;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}

			amount++;
			Assert.True(amount > 0);
			var billPaymentResultString = this.ServiceClient.BillPayment(this.UserName, this.Password, challanNo.ToString(), Guid.NewGuid().ToString("N").Substring(0, 6), amount.ToString("D11") + "00", DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("HHmmss"), null, "CASH|0118|0051");
			var billPaymentResult = BillPaymentResult.Parse(billPaymentResultString);
			Assert.Equal(BillPaymentResult.ResponseCodes.InvalidData, billPaymentResult.ResponseCode);
			Assert.True(string.IsNullOrWhiteSpace(billPaymentResult.Reserved));
			Assert.True(string.IsNullOrWhiteSpace(billPaymentResult.IdentificationParameter));
		}

		[Theory, AspireTestPriority(9)]
		[MemberData(nameof(GetInvalidChallanNos))]
		public void UpdateChallanInvalidChallanNoFormat(string fullChallanNo)
		{
			var billPaymentResultString = this.ServiceClient.BillPayment(this.UserName, this.Password, fullChallanNo, Guid.NewGuid().ToString("N").Substring(0, 6), 123.ToString("D11") + "00", DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("HHmmss"), null, "CASH|0118|0051");
			var billPaymentResult = BillPaymentResult.Parse(billPaymentResultString);
			Assert.Equal(BillPaymentResult.ResponseCodes.InvalidData, billPaymentResult.ResponseCode);
			Assert.True(string.IsNullOrWhiteSpace(billPaymentResult.Reserved));
			Assert.True(string.IsNullOrWhiteSpace(billPaymentResult.IdentificationParameter));
		}

		[Theory, AspireTestPriority(10)]
		[MemberData(nameof(GetChallanNosNotPaid))]
		public void UpdateChallanInvalidAccountInformation(AspireFormats.ChallanNo challanNo)
		{
			using (var aspireContext = new AspireContext())
			{
				var dueDate = DateTime.Today.AddDays(0);
				int amount;
				switch (challanNo.FeeTypeEnum)
				{
					case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
						amount = SetDueDateCandidateAppliedProgram(aspireContext, challanNo.ChallanID, dueDate).Amount;
						break;
					case InstituteBankAccount.FeeTypes.NewAdmissionFee:
					case InstituteBankAccount.FeeTypes.StudentFee:
						amount = this.SetDueDateStudentFeeChallan(aspireContext, challanNo.ChallanID, dueDate).Amount;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				var instituteBankAccount = aspireContext.InstituteBankAccounts
					.Where(iba => iba.Active && iba.Bank.BankName == this.BankName && iba.Institute.InstituteCode == challanNo.InstituteCode && iba.FeeType == challanNo.FeeTypeByte)
					.SingleOrDefault(iba => iba.AccountType == InstituteBankAccount.AccountTypeOnline || iba.AccountType == InstituteBankAccount.AccountTypeManualOnline);
				Assert.NotNull(instituteBankAccount);
				instituteBankAccount.Active = false;
				aspireContext.SaveChangesAsSystemUser();

				var billPaymentResultString = this.ServiceClient.BillPayment(this.UserName, this.Password, challanNo.ToString(), Guid.NewGuid().ToString("N").Substring(0, 6), amount.ToString("D11") + "00", DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("HHmmss"), null, "CASH|0118|0051");
				var billPaymentResult = BillPaymentResult.Parse(billPaymentResultString);
				Assert.Equal(BillPaymentResult.ResponseCodes.ProcessingFailed, billPaymentResult.ResponseCode);
				Assert.True(string.IsNullOrWhiteSpace(billPaymentResult.Reserved));
				Assert.True(string.IsNullOrWhiteSpace(billPaymentResult.IdentificationParameter));

				instituteBankAccount.Active = true;
				aspireContext.SaveChangesAsSystemUser();
			}
		}

		private void UpdateChallanWithSuccess(AspireFormats.ChallanNo challanNo, string reserved)
		{
			using (var aspireContext = new AspireContext())
			{
				var dueDate = DateTime.Today.AddDays(0);
				int amount;
				switch (challanNo.FeeTypeEnum)
				{
					case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
						amount = this.SetDueDateCandidateAppliedProgram(aspireContext, challanNo.ChallanID, dueDate).Amount;
						DeleteAccountTransactionCandidateAppliedProgram(aspireContext, challanNo.ChallanID);
						break;
					case InstituteBankAccount.FeeTypes.NewAdmissionFee:
					case InstituteBankAccount.FeeTypes.StudentFee:
						amount = this.SetDueDateStudentFeeChallan(aspireContext, challanNo.ChallanID, dueDate).Amount;
						DeleteAccountTransactionStudentFeeChallan(aspireContext, challanNo.ChallanID);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				var billPaymentResultString = this.ServiceClient.BillPayment(this.UserName, this.Password, challanNo.ToString(), Guid.NewGuid().ToString("N").Substring(0, 6), amount.ToString("D11") + "00", DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("HHmmss"), null, reserved);
				var billPaymentResult = BillPaymentResult.Parse(billPaymentResultString);
				Assert.Equal(BillPaymentResult.ResponseCodes.Success, billPaymentResult.ResponseCode);
				Assert.True(string.IsNullOrWhiteSpace(billPaymentResult.Reserved));
				Assert.True(string.IsNullOrWhiteSpace(billPaymentResult.IdentificationParameter));
			}
		}

		[Theory, AspireTestPriority(11)]
		[MemberData(nameof(GetChallanNosNotPaid))]
		public void UpdateChallanSuccess(AspireFormats.ChallanNo challanNo)
		{
			var accountNo = "07220010000123456870";
			var branchCode = "0680";
			var reserved = new[]
			{
				$"{accountNo}|0001|{branchCode}",
				$"{accountNo}|0105|{branchCode}",
				$"{accountNo}|0113|{branchCode}",
				$"{"CASH"}|0118|{branchCode}",
				$"{accountNo}|0056|{branchCode}",
			};
			reserved.ForEach(r => UpdateChallanWithSuccess(challanNo, r));
		}

		[Theory, AspireTestPriority(0)]
		[MemberData(nameof(GetChallanNosNotPaid))]
		public void InvalidInputsBillInquiry(AspireFormats.ChallanNo challanNo)
		{
			var inquiry = new Func<string, string, string>(delegate (string bankMnemonic, string reserved)
			{
				var billInquiryResultString = this.ServiceClient.BillInquiry(this.UserName, this.Password, challanNo.ToString(), bankMnemonic, reserved);
				var billInquiryResult = BillInquiryResult.Parse(billInquiryResultString);
				Assert.Equal(BillInquiryResult.ResponseCodes.InvalidData, billInquiryResult.ResponseCode);
				return billInquiryResultString;
			});

			inquiry("1", null);
			inquiry("1234", null);
			inquiry("123456789", null);

			inquiry("123", "1");
			inquiry("123", RandomString(201));
		}

		private static string RandomString(int length) => string.Join("", Enumerable.Range(0, length).Select(i => Guid.NewGuid().ToString("N")[0]));

		[Theory, AspireTestPriority(1)]
		[MemberData(nameof(GetChallanNosPaid))]
		public void InvalidInputsBillPayment(AspireFormats.ChallanNo challanNo)
		{
			var pay = new Func<string, string, string, string, string, string, BillPaymentResult.ResponseCodes, string>(delegate (string authId, string amount, string tranDate, string tranTime, string bankMnemonic, string reserved, BillPaymentResult.ResponseCodes responseCode)
			 {
				 var billPaymentResultString = this.ServiceClient.BillPayment(this.UserName, this.Password, challanNo.ToString(), authId, amount, tranDate, tranTime, bankMnemonic, reserved);
				 var billPaymentResult = BillPaymentResult.Parse(billPaymentResultString);
				 Assert.Equal(responseCode, billPaymentResult.ResponseCode);
				 Assert.True(string.IsNullOrWhiteSpace(billPaymentResult.Reserved));
				 Assert.True(string.IsNullOrWhiteSpace(billPaymentResult.IdentificationParameter));
				 return billPaymentResultString;
			 });

			var authId1 = RandomString(6);
			var amount1 = 1.ToString("D11") + "00";
			var tranDate1 = DateTime.Now.ToString("yyyyMMdd");
			var tranTime1 = DateTime.Now.ToString("HHmmss");
			var bankMnemonic1 = RandomString(3);
			var reserved1 = "CASH|0118|0051";
			var responseCode1 = BillPaymentResult.ResponseCodes.DuplicateTransaction;

			pay(authId1, amount1, tranDate1, tranTime1, bankMnemonic1, reserved1, responseCode1);
			responseCode1 = BillPaymentResult.ResponseCodes.InvalidData;
			pay(RandomString(1), amount1, tranDate1, tranTime1, bankMnemonic1, reserved1, responseCode1);
			pay(RandomString(7), amount1, tranDate1, tranTime1, bankMnemonic1, reserved1, responseCode1);
			authId1 = RandomString(6);
			pay(authId1, "abc0000000000", tranDate1, tranTime1, bankMnemonic1, reserved1, responseCode1);
			pay(authId1, "0000000000000", tranDate1, tranTime1, bankMnemonic1, reserved1, responseCode1);
			pay(authId1, "0000000000012", tranDate1, tranTime1, bankMnemonic1, reserved1, responseCode1);
			pay(authId1, "0000000000000", tranDate1, tranTime1, bankMnemonic1, reserved1, responseCode1);
			pay(authId1, "0", tranDate1, tranTime1, bankMnemonic1, reserved1, responseCode1);
			pay(authId1, null, tranDate1, tranTime1, bankMnemonic1, reserved1, responseCode1);
			pay(authId1, "", tranDate1, tranTime1, bankMnemonic1, reserved1, responseCode1);
			pay(authId1, amount1, null, tranTime1, bankMnemonic1, reserved1, responseCode1);
			pay(authId1, amount1, "", tranTime1, bankMnemonic1, reserved1, responseCode1);
			pay(authId1, amount1, "abc", tranTime1, bankMnemonic1, reserved1, responseCode1);
			pay(authId1, amount1, tranDate1 + "1", tranTime1, bankMnemonic1, reserved1, responseCode1);
			pay(authId1, amount1, tranDate1, null, bankMnemonic1, reserved1, responseCode1);
			pay(authId1, amount1, tranDate1, "", bankMnemonic1, reserved1, responseCode1);
			pay(authId1, amount1, tranDate1, "abcdef", bankMnemonic1, reserved1, responseCode1);
			pay(authId1, amount1, tranDate1, tranTime1 + "1", bankMnemonic1, reserved1, responseCode1);
			pay(authId1, amount1, tranDate1, tranTime1, RandomString(1), reserved1, responseCode1);
			pay(authId1, amount1, tranDate1, tranTime1, RandomString(2), reserved1, responseCode1);
			pay(authId1, amount1, tranDate1, tranTime1, RandomString(4), reserved1, responseCode1);
			pay(authId1, amount1, tranDate1, tranTime1, RandomString(5), reserved1, responseCode1);
			pay(authId1, amount1, tranDate1, tranTime1, RandomString(6), reserved1, responseCode1);
			pay(authId1, amount1, tranDate1, tranTime1, RandomString(7), reserved1, responseCode1);
			pay(authId1, amount1, tranDate1, tranTime1, RandomString(9), reserved1, responseCode1);
			pay(authId1, amount1, tranDate1, tranTime1, bankMnemonic1, "", responseCode1);
			pay(authId1, amount1, tranDate1, tranTime1, bankMnemonic1, null, responseCode1);
			pay(authId1, amount1, tranDate1, tranTime1, bankMnemonic1, "||", responseCode1);
			pay(authId1, amount1, tranDate1, tranTime1, bankMnemonic1, "|||", responseCode1);
			pay(authId1, amount1, tranDate1, tranTime1, bankMnemonic1, "abc|abc|abc", responseCode1);
			pay(authId1, amount1, tranDate1, tranTime1, bankMnemonic1, reserved1, BillPaymentResult.ResponseCodes.DuplicateTransaction);
		}
	}
}