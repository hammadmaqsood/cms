﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.UnitTests.BankAlfalahService;
using System;
using System.Linq;
using System.ServiceModel;
using Aspire.Lib.Extensions;
using Xunit;

namespace Aspire.UnitTests.Facts
{
	[Collection(nameof(AspireCollections.BankAlfalahService))]
	public sealed class BankAlfalahService : BaseService
	{
		protected override string BankName => AspireConstants.BankAlfalahName;
		protected override string ServiceName => "Bank Alfalah";
		protected override IntegratedServiceUser.ServiceTypes ServiceTypeEnum => IntegratedServiceUser.ServiceTypes.BankAlfalah;

		private ServiceClient ServiceClient => new ServiceClient
		{
			ClientCredentials =
			{
				UserName =
				{
					UserName = this.UserName.ToString(),
					Password = this.Password.ToString(),
				}
			}
		};

		[Theory, AspireTestPriority(1)]
		[MemberData(nameof(GetChallanNosDoesNotExists))]
		public void NotExistedChallanNos(string fullChallanNo)
		{
			var challanDetails = this.ServiceClient.GetChallanDetails(decimal.Parse(fullChallanNo));
			Assert.Null(challanDetails);
		}

		[Theory, AspireTestPriority(2)]
		[MemberData(nameof(GetInvalidChallanNos))]
		public void InvalidChallanNos(string fullChallanNo)
		{
			var faultException = Assert.Throws<FaultException<ServiceFault>>(() => { this.ServiceClient.GetChallanDetails(decimal.Parse(fullChallanNo)); });
			Assert.Equal($"ChallanNo {fullChallanNo.ToDecimal()} is not valid.", faultException.Detail?.Message);
		}

		[Theory, AspireTestPriority(3)]
		[MemberData(nameof(GetChallanNosPaid))]
		public void PaidChallanDetails(AspireFormats.ChallanNo challanNo)
		{
			var challanDetails = this.ServiceClient.GetChallanDetails(challanNo.FullChallanNo);
			Assert.True(challanDetails?.Paid);
		}

		[Theory, AspireTestPriority(4)]
		[MemberData(nameof(GetChallanNosNotPaid))]
		public void NotPaidChallanDetails(AspireFormats.ChallanNo challanNo)
		{
			var challanDetails = this.ServiceClient.GetChallanDetails(challanNo.FullChallanNo);
			Assert.False(challanDetails?.Paid);
		}

		[Theory, AspireTestPriority(5)]
		[MemberData(nameof(GetChallanNosDoesNotExists))]
		public void UpdateChallanChallanNoDoesNotExists(string challanNoString)
		{
			var challanNo = decimal.Parse(challanNoString);
			var exc = Assert.Throws<FaultException<ServiceFault>>(() => { this.ServiceClient.UpdateChallan(challanNo, 1, "123", "ABC", 123, "E-8", "E-8/1"); });
			Assert.Equal($"ChallanNo {challanNo} doesn't exists.", exc.Detail.Message);
		}

		[Theory, AspireTestPriority(6)]
		[MemberData(nameof(GetChallanNosPaid))]
		public void UpdateChallanAlreadyPaid(AspireFormats.ChallanNo challanNo)
		{
			var exc = Assert.Throws<FaultException<ServiceFault>>(() => { this.ServiceClient.UpdateChallan(challanNo.FullChallanNo, 1, "123", "ABC", 123, "E-8", "E-8/1"); });
			Assert.Equal($"ChallanNo {challanNo.FullChallanNo} is already paid.", exc.Detail.Message);
		}

		[Theory, AspireTestPriority(7)]
		[MemberData(nameof(GetChallanNosNotPaid))]
		public void UpdateChallanLastDueDatePassed(AspireFormats.ChallanNo challanNo)
		{
			using (var aspireContext = new AspireContext())
			{
				var yesterday = DateTime.Today.AddDays(-1);
				switch (challanNo.FeeTypeEnum)
				{
					case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
						this.SetDueDateCandidateAppliedProgram(aspireContext, challanNo.ChallanID, yesterday);
						break;
					case InstituteBankAccount.FeeTypes.NewAdmissionFee:
					case InstituteBankAccount.FeeTypes.StudentFee:
						this.SetDueDateStudentFeeChallan(aspireContext, challanNo.ChallanID, yesterday);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			var exc = Assert.Throws<FaultException<ServiceFault>>(() => { this.ServiceClient.UpdateChallan(challanNo.FullChallanNo, 1, "123", "ABC", 123, "E-8", "E-8/1"); });
			Assert.Equal($"Last Due Date for ChallanNo {challanNo.FullChallanNo} is passed.", exc.Detail.Message);
		}

		[Theory, AspireTestPriority(8)]
		[MemberData(nameof(GetChallanNosNotPaid))]
		public void UpdateChallanInvalidAmount(AspireFormats.ChallanNo challanNo)
		{
			int amount;
			using (var aspireContext = new AspireContext())
			{
				var dueDate = DateTime.Today.AddDays(0);
				switch (challanNo.FeeTypeEnum)
				{
					case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
						amount = SetDueDateCandidateAppliedProgram(aspireContext, challanNo.ChallanID, dueDate).Amount;
						break;
					case InstituteBankAccount.FeeTypes.NewAdmissionFee:
					case InstituteBankAccount.FeeTypes.StudentFee:
						amount = this.SetDueDateStudentFeeChallan(aspireContext, challanNo.ChallanID, dueDate).Amount;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}

			amount++;
			var exc = Assert.Throws<FaultException<ServiceFault>>(() => { this.ServiceClient.UpdateChallan(challanNo.FullChallanNo, amount, "123", "ABC", 123, "E-8", "E-8/1"); });
			Assert.Equal($"Amount Rs. {amount} is invalid for ChallanNo {challanNo.FullChallanNo}.", exc.Detail.Message);
		}

		[Theory, AspireTestPriority(9)]
		[MemberData(nameof(GetInvalidChallanNos))]
		public void UpdateChallanInvalidChallanNoFormat(string fullChallanNo)
		{
			var challanNo = decimal.Parse(fullChallanNo);
			var exc = Assert.Throws<FaultException<ServiceFault>>(() => { this.ServiceClient.UpdateChallan(challanNo, 1, "123", "ABC", 123, "E-8", "E-8/1"); });
			Assert.Equal($"ChallanNo {challanNo} is not valid.", exc.Detail.Message);
		}

		[Theory, AspireTestPriority(10)]
		[MemberData(nameof(GetChallanNosNotPaid))]
		public void UpdateChallanSuccess(AspireFormats.ChallanNo challanNo)
		{
			using (var aspireContext = new AspireContext())
			{
				var dueDate = DateTime.Today.AddDays(0);
				int amount;
				switch (challanNo.FeeTypeEnum)
				{
					case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
						amount = this.SetDueDateCandidateAppliedProgram(aspireContext, challanNo.ChallanID, dueDate).Amount;
						break;
					case InstituteBankAccount.FeeTypes.NewAdmissionFee:
					case InstituteBankAccount.FeeTypes.StudentFee:
						amount = this.SetDueDateStudentFeeChallan(aspireContext, challanNo.ChallanID, dueDate).Amount;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				var instituteBankAccount = aspireContext.InstituteBankAccounts
					.Where(iba => iba.Bank.BankName == AspireConstants.BankAlfalahName && iba.Institute.InstituteCode == challanNo.InstituteCode && iba.FeeType == challanNo.FeeTypeByte)
					.SingleOrDefault(iba => iba.AccountType == InstituteBankAccount.AccountTypeOnline || iba.AccountType == InstituteBankAccount.AccountTypeManualOnline);
				Assert.NotNull(instituteBankAccount);
				if (!instituteBankAccount.Active)
				{
					instituteBankAccount.Active = true;
					aspireContext.SaveChangesAsSystemUser();
				}

				var branchCode = (short)new Random().Next(1, short.MaxValue);
				var branchName = "E-8/1";
				var accountNo = instituteBankAccount.AccountNo;
				var accountTitle = instituteBankAccount.AccountTitle;

				this.ServiceClient.UpdateChallan(challanNo.FullChallanNo, amount, accountNo, accountTitle, branchCode, branchName, branchName);
				var challanDetails = this.ServiceClient.GetChallanDetails(challanNo.FullChallanNo);
				Assert.NotNull(challanDetails?.Paid);
				Assert.True(challanDetails.Paid);
				Assert.Equal(challanNo.FullChallanNo, challanDetails.ChallanNo);
				Assert.Equal(amount, challanDetails.Amount);
				Assert.Equal(DateTime.Now.ToString("yyyyMMddHH"), challanDetails.DepositDate?.ToString("yyyyMMddHH"));
				Assert.Equal("Bank", challanDetails.DepositPlace);
				Assert.Null(challanDetails.DepositedInBranchCode);
				Assert.Null(challanDetails.DepositedInBranchName);
				Assert.Equal(accountNo, challanDetails.DepositedInAccountNo);
				Assert.Null(challanDetails.DepositedInAccountTitle);

				var accountTransaction = aspireContext.AccountTransactions.SingleOrDefault(at => at.FullChallanNo == challanNo.FullChallanNo);
				Assert.NotNull(accountTransaction);
				Assert.Null(accountTransaction.AccountNo);
				Assert.Null(accountTransaction.AccountTitle);
				Assert.Equal(branchCode.ToString(), accountTransaction.BranchCode);
				Assert.Equal(branchName, accountTransaction.BranchName);
				Assert.Null(accountTransaction.Remarks);
				Assert.True(accountTransaction.Verified);
				Assert.Equal(amount, accountTransaction.Amount);
				Assert.Equal(instituteBankAccount.InstituteBankAccountID, accountTransaction.InstituteBankAccountID);
				Assert.Equal(DateTime.Now.Date, accountTransaction.TransactionDate.Date);
				Assert.Equal(DateTime.Now.Date, accountTransaction.SystemDate.Date);
				Assert.Equal(AccountTransaction.PaymentTypes.Cash, accountTransaction.PaymentTypeEnum);
				Assert.Equal(AccountTransaction.TransactionChannels.Branch, accountTransaction.TransactionChannelEnum);
				Assert.Equal(AccountTransaction.Services.BankAlfalah, accountTransaction.ServiceEnum);
			}
		}

		[Theory, AspireTestPriority(11)]
		[MemberData(nameof(GetChallanNosNotPaid))]
		public void UpdateChallanInvalidAccountInformation(AspireFormats.ChallanNo challanNo)
		{
			int amount;
			using (var aspireContext = new AspireContext())
			{
				var dueDate = DateTime.Today.AddDays(0);
				switch (challanNo.FeeTypeEnum)
				{
					case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
						amount = SetDueDateCandidateAppliedProgram(aspireContext, challanNo.ChallanID, dueDate).Amount;
						break;
					case InstituteBankAccount.FeeTypes.NewAdmissionFee:
					case InstituteBankAccount.FeeTypes.StudentFee:
						amount = this.SetDueDateStudentFeeChallan(aspireContext, challanNo.ChallanID, dueDate).Amount;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				var instituteBankAccount = aspireContext.InstituteBankAccounts
					.Where(iba => iba.Bank.BankName == AspireConstants.BankAlfalahName && iba.Institute.InstituteCode == challanNo.InstituteCode && iba.FeeType == challanNo.FeeTypeByte)
					.SingleOrDefault(iba => iba.AccountType == InstituteBankAccount.AccountTypeOnline || iba.AccountType == InstituteBankAccount.AccountTypeManualOnline);
				Assert.NotNull(instituteBankAccount);
				if (instituteBankAccount.Active)
				{
					instituteBankAccount.Active = false;
					aspireContext.SaveChangesAsSystemUser();
				}
			}
			var exc = Assert.Throws<FaultException<ServiceFault>>(() => { this.ServiceClient.UpdateChallan(challanNo.FullChallanNo, amount, "123", "ABC", 123, "E-8", "E-8/1"); });
			Assert.Equal($"Invalid Account Information has been provided.", exc.Detail.Message);
		}
	}
}