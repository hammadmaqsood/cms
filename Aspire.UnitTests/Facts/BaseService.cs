﻿using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Xunit;

namespace Aspire.UnitTests.Facts
{
	public abstract class BaseService : AspireTest
	{
		protected abstract string BankName { get; }
		protected abstract string ServiceName { get; }
		protected abstract IntegratedServiceUser.ServiceTypes ServiceTypeEnum { get; }
		public Guid UserName { get; }
		public Guid Password { get; }
		protected BaseService()
		{
			using (var aspireContext = new AspireContext())
			{
				var bank = aspireContext.Banks.Single(b => b.BankName == this.BankName);
				aspireContext.Institutes.Select(i => new
				{
					i.InstituteID,
					InstituteBankAccounts = i.InstituteBankAccounts
						.Where(iba => iba.Bank.BankName == this.BankName)
						.Where(iba => iba.AccountType == InstituteBankAccount.AccountTypeOnline || iba.AccountType == InstituteBankAccount.AccountTypeManualOnline)
						.ToList()
				}).ToList().ForEach(i =>
				{
					foreach (var feeTypeEnum in Enum.GetValues(typeof(InstituteBankAccount.FeeTypes)).Cast<InstituteBankAccount.FeeTypes>())
					{
						if (i.InstituteBankAccounts.Any(iba => iba.FeeTypeEnum == feeTypeEnum))
						{
							i.InstituteBankAccounts.ForEach(iba => iba.Active = true);
							continue;
						}
						aspireContext.InstituteBankAccounts.Add(new InstituteBankAccount
						{
							AccountTypeEnum = InstituteBankAccount.AccountTypes.Online,
							FeeTypeEnum = feeTypeEnum,
							BankID = bank.BankID,
							Active = true,
							InstituteID = i.InstituteID,
							AccountNo = "1234567890",
							AccountNoForDisplay = "1234567890",
							AccountTitle = "Bahria University",
							AccountTitleForDisplay = "Bahria University",
							BranchName = "E-8",
							BranchAddress = "E-8",
							BranchCode = "0680",
							BranchForDisplay = "E-8 Islamabad",
						});
					}
				});

				this.UserName = Guid.NewGuid();
				this.Password = Guid.NewGuid();
				var integratedServiceUser = aspireContext.IntegratedServiceUsers
												.SingleOrDefault(isu => isu.IsDeleted == false && isu.ServiceType == (byte)this.ServiceTypeEnum)
											?? aspireContext.IntegratedServiceUsers.Add(new IntegratedServiceUser
											{
												InstituteID = null,
												IsDeleted = false,
												// ReSharper disable once VirtualMemberCallInConstructor
												ServiceTypeEnum = this.ServiceTypeEnum,
												// ReSharper disable once VirtualMemberCallInConstructor
												ServiceName = this.ServiceName,
											});
				integratedServiceUser.StatusEnum = IntegratedServiceUser.Statuses.Active;
				integratedServiceUser.UserName = this.UserName;
				integratedServiceUser.Password = this.Password.ToString().EncryptPassword();
				integratedServiceUser.CreatedDate = DateTime.Now;
				integratedServiceUser.ExpiryDate = DateTime.Now.AddMonths(1);
				integratedServiceUser.IPAddress = "127.0.0.1";

				var instituteBankAccounts = aspireContext.InstituteBankAccounts
					.Where(iba => iba.Bank.BankName == this.BankName)
					.Where(iba => iba.AccountType == InstituteBankAccount.AccountTypeOnline || iba.AccountType == InstituteBankAccount.AccountTypeManualOnline)
					.ToList();
				foreach (var instituteBankAccount in instituteBankAccounts)
					instituteBankAccount.Active = true;
				aspireContext.SaveChangesAsSystemUser();
			}
		}

		public sealed class ChallanType
		{
			public int InstituteID { get; set; }
			public string InstituteCode { get; set; }
			public InstituteBankAccount.FeeTypes FeeTypeEnum { get; set; }
		}

		protected static IEnumerable<ChallanType> GetChallanTypes(AspireContext aspireContext)
		{
			return Enum.GetValues(typeof(InstituteBankAccount.FeeTypes))
				.Cast<InstituteBankAccount.FeeTypes>()
				.SelectMany(t => aspireContext.Institutes
					.Select(i => new
					{
						i.InstituteID,
						i.InstituteCode
					})
					.OrderBy(i => i.InstituteID)
					.ToList()
					.Select(i => new ChallanType
					{
						InstituteID = i.InstituteID,
						InstituteCode = i.InstituteCode,
						FeeTypeEnum = t
					})).ToList();
		}

		protected static void DeleteAccountTransactionStudentFeeChallan(AspireContext aspireContext, int studentFeeChallanID)
		{
			var studentFeeChallan = aspireContext.StudentFeeChallans.Include(sfc => sfc.AccountTransaction).SingleOrDefault(sfc => sfc.StudentFeeChallanID == studentFeeChallanID);
			Assert.NotNull(studentFeeChallan);
			if (studentFeeChallan.AccountTransaction == null)
				return;
			var accountTransaction = studentFeeChallan.AccountTransaction;
			studentFeeChallan.AccountTransactionID = null;
			aspireContext.SaveChangesAsSystemUser();
			aspireContext.AccountTransactions.Remove(accountTransaction);
			aspireContext.SaveChangesAsSystemUser();
		}

		protected static void DeleteAccountTransactionCandidateAppliedProgram(AspireContext aspireContext, int candidateAppliedProgramID)
		{
			var candidateAppliedProgram = aspireContext.CandidateAppliedPrograms.Include(cap => cap.AccountTransaction).SingleOrDefault(cap => cap.CandidateAppliedProgramID == candidateAppliedProgramID);
			Assert.NotNull(candidateAppliedProgram);
			if (candidateAppliedProgram.AccountTransaction == null)
				return;
			var accountTransaction = candidateAppliedProgram.AccountTransaction;
			candidateAppliedProgram.AccountTransactionID = null;
			aspireContext.SaveChangesAsSystemUser();
			aspireContext.AccountTransactions.Remove(accountTransaction);
			aspireContext.SaveChangesAsSystemUser();
		}

		protected CandidateAppliedProgram SetDueDateCandidateAppliedProgram(AspireContext aspireContext, int candidateAppliedProgramID, DateTime dueDate)
		{
			var candidateAppliedProgram = aspireContext.CandidateAppliedPrograms
				.Where(cap => cap.CandidateAppliedProgramID == candidateAppliedProgramID)
				.Include(cap => cap.AdmissionOpenProgram1)
				.SingleOrDefault();
			Assert.NotNull(candidateAppliedProgram?.AdmissionOpenProgram1);
			candidateAppliedProgram.AdmissionOpenProgram1.AdmissionProcessingFeeDueDate = dueDate;
			aspireContext.SaveChangesAsSystemUser();
			return candidateAppliedProgram;
		}

		protected StudentFeeChallan SetDueDateStudentFeeChallan(AspireContext aspireContext, int studentFeeChallanID, DateTime dueDate)
		{
			var studentFeeChallan = aspireContext.StudentFeeChallans.SingleOrDefault(sfc => sfc.StudentFeeChallanID == studentFeeChallanID);
			Assert.NotNull(studentFeeChallan);
			studentFeeChallan.DueDate = dueDate;
			aspireContext.SaveChangesAsSystemUser();
			return studentFeeChallan;
		}

		protected int GetAmountCandidateAppliedProgram(AspireContext aspireContext, int candidateAppliedProgramID)
		{
			return aspireContext.CandidateAppliedPrograms.Where(cap => cap.CandidateAppliedProgramID == candidateAppliedProgramID).Select(cap => cap.Amount).Single();
		}

		protected int GetAmountStudentFeeChallan(AspireContext aspireContext, int studentFeeChallanID)
		{
			return aspireContext.StudentFeeChallans.Where(sfc => sfc.StudentFeeChallanID == studentFeeChallanID).Select(sfc => sfc.Amount).Single();
		}

		protected static IEnumerable<AspireFormats.ChallanNo> GetChallanNos(AspireContext aspireContext, int count, bool? paid)
		{
			var challanNos = new List<AspireFormats.ChallanNo>();
			var challanTypes = GetChallanTypes(aspireContext);
			foreach (var challanType in challanTypes)
			{
				switch (challanType.FeeTypeEnum)
				{
					case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
						aspireContext.CandidateAppliedPrograms
							.Where(cap => cap.AdmissionOpenProgram1.Program.InstituteID == challanType.InstituteID)
							.Where(cap => paid == null || (paid == true && cap.AccountTransactionID != null) || (paid == false && cap.AccountTransactionID == null))
							.Where(cap => cap.Amount > 0)
							.Select(cap => cap.CandidateAppliedProgramID)
							.Take(count)
							.ToList()
							.ForEach(capID => challanNos.Add(new AspireFormats.ChallanNo(challanType.InstituteCode, challanType.FeeTypeEnum, capID)));
						break;
					case InstituteBankAccount.FeeTypes.NewAdmissionFee:
						aspireContext.StudentFeeChallans
							.Where(sfc => sfc.StudentFee.StudentID == null || sfc.StudentFee.Student.AdmissionOpenProgram.Program.InstituteID == challanType.InstituteID)
							.Where(sfc => sfc.StudentFee.CandidateAppliedProgramID == null || sfc.StudentFee.CandidateAppliedProgram.AdmissionOpenProgram.Program.InstituteID == challanType.InstituteID)
							.Where(sfc => sfc.StudentFee.NewAdmission)
							.Where(sfc => paid == null || (paid == true && sfc.AccountTransactionID != null) || (paid == false && sfc.AccountTransactionID == null))
							.Where(cap => cap.Amount > 0)
							.Select(sfc => sfc.StudentFeeChallanID)
							.Take(count)
							.ToList()
							.ForEach(sfcID => challanNos.Add(new AspireFormats.ChallanNo(challanType.InstituteCode, challanType.FeeTypeEnum, sfcID)));
						break;
					case InstituteBankAccount.FeeTypes.StudentFee:
						aspireContext.StudentFeeChallans
							.Where(sfc => sfc.StudentFee.StudentID != null && sfc.StudentFee.Student.AdmissionOpenProgram.Program.InstituteID == challanType.InstituteID)
							.Where(sfc => !sfc.StudentFee.NewAdmission)
							.Where(sfc => paid == null || (paid == true && sfc.AccountTransactionID != null) || (paid == false && sfc.AccountTransactionID == null))
							.Where(cap => cap.Amount > 0)
							.Select(sfc => sfc.StudentFeeChallanID)
							.Take(count)
							.ToList()
							.ForEach(sfcID => challanNos.Add(new AspireFormats.ChallanNo(challanType.InstituteCode, challanType.FeeTypeEnum, sfcID)));
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			return challanNos;
		}

		private static int ChallanCount => 1;

		private static IEnumerable<AspireFormats.ChallanNo> _challanNosNotPaid;
		private static IEnumerable<AspireFormats.ChallanNo> _challanNosPaid;

		public static IEnumerable<object[]> GetChallanNosNotPaid()
		{
			if (_challanNosNotPaid == null)
				using (var aspireContext = new AspireContext())
				{
					_challanNosNotPaid = GetChallanNos(aspireContext, ChallanCount, false);
				}
			return _challanNosNotPaid.Select(c => new object[] { c }).ToList();
		}

		public static IEnumerable<object[]> GetChallanNosPaid()
		{
			if (_challanNosPaid == null)
				using (var aspireContext = new AspireContext())
				{
					_challanNosPaid = GetChallanNos(aspireContext, ChallanCount, true);
				}
			return _challanNosPaid.Select(c => new object[] { c }).ToList();
		}

		public static IEnumerable<object[]> GetChallanNosDoesNotExists()
		{
			return new[]
			{
				"011999999999",
				"012999999999",
				"013999999999",
				"021999999999",
				"022999999999",
				"023999999999",
				"031999999999",
				"032999999999",
				"033999999999",
				"041999999999",
				"042999999999",
				"043999999999",
				"051999999999",
				"052999999999",
				"053999999999",
				"061999999999",
				"062999999999",
				"063999999999",
				"071999999999",
				"072999999999",
				"073999999999"
			}.Select(c => new object[] { c }).ToList();
		}

		public static IEnumerable<object[]> GetInvalidChallanNos()
		{
			return new[]
			{
				"010999999999",
				"014999999999",
				"01999999999",
				"0",
				"-1"
			}.Select(c => new object[] { c }).ToList();
		}
	}
}
