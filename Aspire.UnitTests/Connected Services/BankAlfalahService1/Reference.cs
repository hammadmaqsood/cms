﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.UnitTests.BankAlfalahService1 {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="BankAlfalahService1.IService")]
    public interface IService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/GetChallanDetails", ReplyAction="http://tempuri.org/IService/GetChallanDetailsResponse")]
        Aspire.BL.Core.IntegrationServices.BankAlfalah1.FeeChallan GetChallanDetails(decimal challanNo);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/GetChallanDetails", ReplyAction="http://tempuri.org/IService/GetChallanDetailsResponse")]
        System.Threading.Tasks.Task<Aspire.BL.Core.IntegrationServices.BankAlfalah1.FeeChallan> GetChallanDetailsAsync(decimal challanNo);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IServiceChannel : Aspire.UnitTests.BankAlfalahService1.IService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ServiceClient : System.ServiceModel.ClientBase<Aspire.UnitTests.BankAlfalahService1.IService>, Aspire.UnitTests.BankAlfalahService1.IService {
        
        public ServiceClient() {
        }
        
        public ServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public Aspire.BL.Core.IntegrationServices.BankAlfalah1.FeeChallan GetChallanDetails(decimal challanNo) {
            return base.Channel.GetChallanDetails(challanNo);
        }
        
        public System.Threading.Tasks.Task<Aspire.BL.Core.IntegrationServices.BankAlfalah1.FeeChallan> GetChallanDetailsAsync(decimal challanNo) {
            return base.Channel.GetChallanDetailsAsync(challanNo);
        }
    }
}
