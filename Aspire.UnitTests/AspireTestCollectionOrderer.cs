﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Xunit.Abstractions;

[assembly: TestCollectionOrderer("Aspire.UnitTests.AspireTestCollectionOrderer", "Aspire.UnitTests")]
[assembly: CollectionBehavior(CollectionBehavior.CollectionPerClass, DisableTestParallelization = true)]

namespace Aspire.UnitTests
{
	public sealed class AspireTestCollectionOrderer : ITestCollectionOrderer
	{
		public IEnumerable<ITestCollection> OrderTestCollections(IEnumerable<ITestCollection> testCollections)
		{
			return testCollections.Select(collection => Enum.TryParse(collection.DisplayName, false, out AspireCollections aspireCollection)
					? new { Collection = collection, AspireCollection = (AspireCollections?)aspireCollection }
					: new { Collection = collection, AspireCollection = (AspireCollections?)null })
						.Where(c => c.AspireCollection != null)
						.OrderBy(c => (int?)c.AspireCollection)
						.Select(c => c.Collection).ToList();
		}
	}
}