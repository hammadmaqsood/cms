﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ClassAttendance.Student
{
	public static class ClassAttendance
	{
		#region Attendance Summary

		public class AttendanceSummary
		{

			public int RegisteredCourseID { get; set; }
			public int OfferedCourseID { get; set; }
			public short SemesterID { get; set; }
			public string CourseCode { get; set; }
			public string Title { get; set; }
			public decimal ContactHours { get; set; }
			public decimal CreditHours { get; set; }
			public string OfferedCourseProgram { get; set; }
			public short OfferedCourseSemesterNo { get; set; }
			public int OfferedCourseSection { get; set; }
			public byte OfferedCourseShift { get; set; }
			public string TeacherName { get; set; }
			public string OfferedCourseClassName => AspireFormats.GetClassName(this.OfferedCourseProgram, this.OfferedCourseSemesterNo, this.OfferedCourseSection, this.OfferedCourseShift);
			public decimal ClassesTotalHours { get; set; }
			public decimal PresentTotalHours { get; set; }
			public decimal PresentTotalPercentage { get; set; }
			public decimal AbsentTotalHours { get; set; }

			public class AttendanceDetail
			{
				public int OfferedCourseAttendanceID { get; set; }
				public int OfferedCourseID { get; set; }
				public DateTime ClassDate { get; set; }
				public byte ClassType { get; set; }
				public string ClassTypeFullName => ((OfferedCourseAttendance.ClassTypes)this.ClassType).ToString();
				public decimal TotalHours { get; set; }
				public decimal PresentHours { get; set; }
				public decimal AbsentHours => this.TotalHours - this.PresentHours;
				public decimal Percentage => this.TotalHours == 0 ? 0 : this.PresentHours * 100 / this.TotalHours;
				public string RoomName { get; set; }
				public string Remarks { get; set; }
				public string TopicsCovered { get; set; }
			}

			public List<AttendanceDetail> AttendanceDetails { get; set; }
		}

		public static AttendanceSummary GetAttendanceSummary(int registeredCourseID)
		{

			using (var aspireContext = new AspireContext(true))
			{
				var attendanceSummary = aspireContext.RegisteredCourses.Where(rc => rc.RegisteredCourseID == registeredCourseID && rc.DeletedDate == null).Select(rc => new AttendanceSummary
				{
					OfferedCourseID = rc.OfferedCourseID,
					SemesterID = rc.OfferedCours.SemesterID,
					CourseCode = rc.OfferedCours.Cours.CourseCode,
					Title = rc.OfferedCours.Cours.Title,
					CreditHours = rc.OfferedCours.Cours.CreditHours,
					ContactHours = rc.OfferedCours.Cours.ContactHours,
					OfferedCourseProgram = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					OfferedCourseSemesterNo = rc.OfferedCours.SemesterNo,
					OfferedCourseSection = rc.OfferedCours.Section,
					OfferedCourseShift = rc.OfferedCours.Shift,
					TeacherName = rc.OfferedCours.FacultyMember.Name,
					RegisteredCourseID = rc.RegisteredCourseID,
				}).SingleOrDefault();
				if (attendanceSummary == null)
					return null;

				attendanceSummary.AttendanceDetails = aspireContext.OfferedCourseAttendances.Where(oca => oca.OfferedCourseID == attendanceSummary.OfferedCourseID).Select(oca => new AttendanceSummary.AttendanceDetail
				{
					OfferedCourseID = oca.OfferedCourseID,
					ClassDate = oca.ClassDate,
					ClassType = oca.ClassType,
					OfferedCourseAttendanceID = oca.OfferedCourseAttendanceID,
					Remarks = oca.Remarks,
					TopicsCovered = oca.TopicsCovered,
					RoomName = oca.Room.RoomName,
					TotalHours = oca.Hours,
					PresentHours = oca.OfferedCourseAttendanceDetails.Where(ocad => ocad.RegisteredCourseID == attendanceSummary.RegisteredCourseID).Select(ocad => (decimal?)ocad.Hours).FirstOrDefault() ?? 0,
				}).OrderBy(a => a.ClassDate).ThenBy(a => a.OfferedCourseAttendanceID).ToList();
				attendanceSummary.ClassesTotalHours = attendanceSummary.AttendanceDetails.Sum(d => d.TotalHours);
				attendanceSummary.PresentTotalHours = attendanceSummary.AttendanceDetails.Sum(d => d.PresentHours);
				attendanceSummary.AbsentTotalHours = attendanceSummary.AttendanceDetails.Sum(d => d.AbsentHours);
				attendanceSummary.PresentTotalPercentage = attendanceSummary.ClassesTotalHours == 0 ? 0 : (attendanceSummary.PresentTotalHours * 100) / attendanceSummary.ClassesTotalHours;
				return attendanceSummary;
			}
		}
		#endregion
	}
}
