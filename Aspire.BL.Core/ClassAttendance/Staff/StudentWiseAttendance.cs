﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ClassAttendance.Staff
{
	public static class StudentWiseAttendance
	{
		private static BL.Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ClassAttendance.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class RegisteredCourse
		{
			public int RegisteredCourseID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Title { get; internal set; }
			public string TeacherName { get; internal set; }
			public string Program { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public decimal? TotalHours { get; internal set; }
			public decimal? PresentHours { get; internal set; }
			public decimal? AbsentHours => this.TotalHours - this.PresentHours;
			public decimal Percentage => this.TotalHours == 0 ? 0 : (this.PresentHours ?? 0) * 100 / (this.TotalHours ?? 1);
			public string PercentageShort => this.Percentage.ToString("0.##");

			public string ClassName => AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift);

			public string RegisteredCourseCode { get; internal set; }
			public string RegisteredTitle { get; internal set; }
			public decimal RegisteredCreditHours { get; internal set; }
			public string RegisteredMajors { get; internal set; }
			public byte? Status { get; internal set; }
			public RegisteredCours.Statuses? StatusEnum => (RegisteredCours.Statuses?)this.Status;
			public string StatusFullName => this.StatusEnum?.ToFullName();
			public byte? FeeStatus { get; internal set; }
			public StudentFee.Statuses? FeeStatusEnum => (StudentFee.Statuses?)this.FeeStatus;
			public string FeeStatusFullName => this.FeeStatusEnum?.ToFullName();
		}

		public static List<RegisteredCourse> GetRegisteredCourses(short semesterID, int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.RegisteredCourses.Where(rc => rc.StudentID == studentID && rc.DeletedDate == null && rc.OfferedCours.SemesterID == semesterID).OrderBy(rc => rc.OfferedCours.SemesterID).Select(rc => new RegisteredCourse
				{
					RegisteredCourseID = rc.RegisteredCourseID,
					SemesterID = rc.OfferedCours.SemesterID,
					Title = rc.OfferedCours.Cours.Title,
					Program = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = rc.OfferedCours.SemesterNo,
					Section = rc.OfferedCours.Section,
					Shift = rc.OfferedCours.Shift,
					RegisteredCreditHours = rc.Cours.CreditHours,
					RegisteredCourseCode = rc.Cours.CourseCode,
					RegisteredMajors = rc.Cours.ProgramMajor.Majors,
					RegisteredTitle = rc.Cours.Title,
					Status = rc.Status,
					TeacherName = rc.OfferedCours.FacultyMember.Name,
					FeeStatus = rc.StudentFee.Status,
					TotalHours = rc.OfferedCours.OfferedCourseAttendances.Sum(oca => (decimal?)oca.Hours)??0,
					PresentHours = rc.OfferedCours.OfferedCourseAttendances.SelectMany(ocad => ocad.OfferedCourseAttendanceDetails).Where(ocad => ocad.RegisteredCourseID == rc.RegisteredCourseID).Sum(ocad => (decimal?)ocad.Hours)??0,
				}).ToList();
			}
		}
	}
}
