﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ClassAttendance.Staff.Reports
{
    public static class ShortAttendance
    {
        private static BL.Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
        {
            return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ClassAttendance.Module, UserGroupPermission.PermissionValues.Allowed);
        }

        public sealed class ShortAttendancePageHeader
        {
            public string Institute { get; internal set; }
            public short SemesterID { get; internal set; }
            public string Title => $"Attendance Short Report  ({this.SemesterID.ToSemesterString()})";
            public static List<ShortAttendancePageHeader> GetList()
            {
                return null;
            }
        }

        public sealed class StudentsShortAttendance
        {
            public int StudentID { get; internal set; }
            public int RegisteredCourseID { get; internal set; }
            public string Enrollment { get; internal set; }
            public string Name { get; internal set; }
            public string Title { get; internal set; }
            public decimal CreditHours { get; internal set; }
            public int ProgramID { get; internal set; }
            public string ProgramAlias { get; internal set; }
            public short SemesterNo { get; internal set; }
            public int Section { get; internal set; }
            public byte Shift { get; internal set; }
            public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
            public string FacultyMemberName { get; internal set; }
            public int? FacultyMemberID { get; internal set; }
            public decimal? PresentHours { get; set; }
            public decimal? TotalHours { get; set; }
            public int? DepartmentID { get; internal set; }
            public string DepartmentName { get; internal set; }
            public decimal? MinimumPercentage { get; internal set; }

            public decimal? AbsentHours => this.TotalHours - this.PresentHours;
            public decimal? Percentage => this.TotalHours == 0 ? 0 : this.PresentHours * 100 / this.TotalHours;

            public static List<StudentsShortAttendance> GetList()
            {
                return null;
            }
        }

        public sealed class ReportDataSet
        {
            internal ReportDataSet() { }
            public ShortAttendancePageHeader PageHeader { get; internal set; }
            public List<StudentsShortAttendance> StudentShortAttendance { get; internal set; }
        }

        public static ReportDataSet GetStudentsShortAttendance(short semesterID, int? departmentID, int? programID, decimal? minimumPercentage, Guid loginSessionGuid)
        {
            using (var aspireContext = new AspireContext())
            {
                aspireContext.Database.CommandTimeout = 180;
                var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

                var offeredCoursesQuery = aspireContext.OfferedCourses
                    .Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
                    .Where(oc => oc.SemesterID == semesterID);
                if (departmentID != null)
                    offeredCoursesQuery = offeredCoursesQuery.Where(oc => oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
                if (programID != null)
                    offeredCoursesQuery = offeredCoursesQuery.Where(oc => oc.Cours.AdmissionOpenProgram.ProgramID == programID.Value);

                var offeredCoursesAttendance = offeredCoursesQuery
                    .SelectMany(oc => oc.OfferedCourseAttendances)
                    .GroupBy(oca => oca.OfferedCourseID)
                    .Select(oca => new
                    {
                        OfferedCourseID = oca.Key,
                        TotalHours = oca.Sum(c => c.Hours)
                    }).ToList();

                var registeredCourseAttendance = offeredCoursesQuery
                    .SelectMany(oc => oc.RegisteredCourses)
                    .SelectMany(rc => rc.OfferedCourseAttendanceDetails)
                    .GroupBy(ocad => ocad.RegisteredCourseID)
                    .Select(ocad => new
                    {
                        RegisteredCourseID = ocad.Key,
                        PresentHours = ocad.Sum(c => c.Hours)
                    }).ToList();

                var registeredCourses = offeredCoursesQuery.SelectMany(oc => oc.RegisteredCourses)
                    .Where(rc => rc.DeletedDate == null && rc.Status == null && rc.FreezedStatus == null && rc.Student.Status == null)
                    .Select(rc => new
                    {
                        rc.RegisteredCourseID,
                        rc.OfferedCourseID,
                        rc.StudentID,
                        rc.Student.Name,
                        rc.Student.Enrollment,
                        rc.OfferedCours.Cours.Title,
                        rc.OfferedCours.Cours.CreditHours,
                        rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
                        rc.OfferedCours.SemesterNo,
                        rc.OfferedCours.Section,
                        rc.OfferedCours.Shift,
                        rc.OfferedCours.FacultyMemberID,
                        FacultyMemberName = rc.OfferedCours.FacultyMember.Name,
                        rc.OfferedCours.Cours.AdmissionOpenProgram.ProgramID,
                        rc.OfferedCours.Cours.AdmissionOpenProgram.Program.DepartmentID,
                        rc.OfferedCours.Cours.AdmissionOpenProgram.Program.Department.DepartmentName,
                    }).ToList();

                var registeredCoursesWithAttendance = from rc in registeredCourses
                                                      join rca in registeredCourseAttendance on rc.RegisteredCourseID equals rca.RegisteredCourseID into grp
                                                      from g in grp.DefaultIfEmpty()
                                                      select new
                                                      {
                                                          rc.RegisteredCourseID,
                                                          rc.OfferedCourseID,
                                                          rc.StudentID,
                                                          rc.Name,
                                                          rc.Enrollment,
                                                          rc.Title,
                                                          rc.CreditHours,
                                                          rc.ProgramAlias,
                                                          rc.SemesterNo,
                                                          rc.Section,
                                                          rc.Shift,
                                                          rc.FacultyMemberID,
                                                          rc.FacultyMemberName,
                                                          rc.ProgramID,
                                                          rc.DepartmentID,
                                                          rc.DepartmentName,
                                                          PresentHours = g != null ? g.PresentHours : 0M,
                                                      };

                var registeredCoursesWithAttendanceWithOfferedCourse = from rc in registeredCoursesWithAttendance
                                                                       join oc in offeredCoursesAttendance on rc.OfferedCourseID equals oc.OfferedCourseID
                                                                       select new { rc, oc };

                if (minimumPercentage != null)
                    registeredCoursesWithAttendanceWithOfferedCourse = registeredCoursesWithAttendanceWithOfferedCourse
                        .Where(x => x.rc.PresentHours * 100 / x.oc.TotalHours < minimumPercentage.Value);


                var query = registeredCoursesWithAttendanceWithOfferedCourse.Select(x => new StudentsShortAttendance
                {
                    RegisteredCourseID = x.rc.RegisteredCourseID,
                    StudentID = x.rc.StudentID,
                    Name = x.rc.Name,
                    Enrollment = x.rc.Enrollment,
                    Title = x.rc.Title,
                    CreditHours = x.rc.CreditHours,
                    ProgramAlias = x.rc.ProgramAlias,
                    SemesterNo = x.rc.SemesterNo,
                    Section = x.rc.Section,
                    Shift = x.rc.Shift,
                    FacultyMemberID = x.rc.FacultyMemberID,
                    FacultyMemberName = x.rc.FacultyMemberName,
                    ProgramID = x.rc.ProgramID,
                    DepartmentID = x.rc.DepartmentID,
                    DepartmentName = x.rc.DepartmentName,
                    PresentHours = x.rc.PresentHours,
                    TotalHours = x.oc.TotalHours,
                    MinimumPercentage = minimumPercentage
                });

                return new ReportDataSet
                {
                    PageHeader = new ShortAttendancePageHeader
                    {
                        SemesterID = semesterID,
                        Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single(),
                    },
                    StudentShortAttendance = query.ToList(),
                };
            }
        }
    }
}