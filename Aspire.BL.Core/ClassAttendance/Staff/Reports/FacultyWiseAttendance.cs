﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ClassAttendance.Staff.Reports
{
	public static class FacultyWiseAttendance
	{

		private static BL.Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ClassAttendance.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class LectureWiseAttendancePageHeader
		{
			public string Institute { get; internal set; }
			public string Title => $"Lecture Wise Attendance";
			public static List<LectureWiseAttendancePageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class CourseInformation
		{
			public int InstituteID { get; set; }
			public int OfferedCourseID { get; set; }
			public short SemesterID { get; internal set; }
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public decimal ContactHours { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public string Program { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string TeacherName { get; internal set; }
			public string ClassName => AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift);
			public string Semester => this.SemesterID.ToSemesterString();

			public static List<CourseInformation> GetList()
			{
				return null;
			}
		}
		public sealed class LectureWiseAttendance
		{
			public int InstituteID { get; internal set; }
			public int OfferedCourseAttendanceID { get; internal set; }
			public int OfferedCourseID { get; internal set; }
			public DateTime ClassDate { get; internal set; }
			public byte ClassType { get; internal set; }
			public decimal Hours { get; internal set; }
			public string HoursValueFullName => this.Hours.ToHoursValue().ToFullName(false);
			public string RoomName { get; internal set; }
			public string Remarks { get; internal set; }
			public string TopicsCovered { get; internal set; }
			public DateTime MarkedDate { get; internal set; }
			public string ClassTypeFullName => ((OfferedCourseAttendance.ClassTypes)this.ClassType).ToString();
			public string ClassDateLong => (this.ClassDate).ToString("D");

			public static List<LectureWiseAttendance> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public LectureWiseAttendancePageHeader PageHeader { get; internal set; }
			public CourseInformation CourseInformation { get; internal set; }
			public List<LectureWiseAttendance> LectureWiseAttendances { get; internal set; }
		}

		public static ReportDataSet GetOfferedCourseAttendances(int offeredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{

				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var courseInformation = aspireContext.OfferedCourses.Where(oc => oc.OfferedCourseID == offeredCourseID).Select(oc => new CourseInformation
				{
					InstituteID = oc.Cours.AdmissionOpenProgram.Program.InstituteID,
					OfferedCourseID = oc.OfferedCourseID,
					SemesterID = oc.SemesterID,
					CourseCode = oc.Cours.CourseCode,
					Title = oc.Cours.Title,
					CreditHours = oc.Cours.CreditHours,
					ContactHours = oc.Cours.ContactHours,
					Program = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = oc.SemesterNo,
					Section = oc.Section,
					Shift = oc.Shift,
					TeacherName = oc.FacultyMember.Name,
				}).SingleOrDefault();

				var lectureWiseAttendance = aspireContext.OfferedCourseAttendances.Where(oc => oc.OfferedCourseID == offeredCourseID).Select(oca => new LectureWiseAttendance
				{
					InstituteID = oca.OfferedCours.Cours.AdmissionOpenProgram.Program.InstituteID,
					OfferedCourseAttendanceID = oca.OfferedCourseAttendanceID,
					OfferedCourseID = oca.OfferedCourseID,
					ClassDate = oca.ClassDate,
					ClassType = oca.ClassType,
					Hours = oca.Hours,
					RoomName = oca.Room.RoomName,
					Remarks = oca.Remarks,
					TopicsCovered = oca.TopicsCovered,
					MarkedDate = oca.MarkedDate,
				}).OrderBy(a => a.ClassDate).ThenBy(a => a.OfferedCourseAttendanceID).ToList();
				return new ReportDataSet
				{
					PageHeader = new LectureWiseAttendancePageHeader
					{
						Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single(),
					},
					CourseInformation = courseInformation,
					LectureWiseAttendances = lectureWiseAttendance.ToList(),
				};
			}
		}
	}
}
