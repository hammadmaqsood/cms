﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ClassAttendance.Staff.Reports
{
	public static class CourseWiseAttendanceSummary
	{
		private static BL.Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ClassAttendance.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class CourseWiseAttendanceSummaryPageHeader
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public DateTime EndDate { get; internal set; }
			public string DepartmentName { get; internal set; }
			public string Title => $"Course Wise Attendance Summary ({this.SemesterID.ToSemesterString()})";
			public static List<ShortAttendance.ShortAttendancePageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class CourseWiseAttendance
		{
			public int? DepartmentID { get; internal set; }
			public string DepartmentName { get; internal set; }
			public int ProgramID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public int? FacultyMemberID { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public string Title { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public decimal? PresentHours { get; set; }
			public decimal? TotalHours { get; set; }
			public decimal? MinimumPercentage { get; internal set; }

			public decimal? AbsentHours => this.TotalHours - this.PresentHours;
			public decimal? Percentage => this.TotalHours == 0 ? 0 : this.PresentHours * 100 / this.TotalHours;

			public static List<CourseWiseAttendance> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public CourseWiseAttendanceSummaryPageHeader PageHeader { get; internal set; }
			public List<CourseWiseAttendance> CourseWiseAttendanceSummary { get; internal set; }
		}

		public static ReportDataSet GetCourseWiseAttendanceSummary(short semesterID, int? departmentID, int? programID, FacultyMember.FacultyTypes? facultyTypeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var offeredCourses = aspireContext.OfferedCourses.Where(oc => oc.SemesterID == semesterID).AsQueryable();
				if (departmentID != null)
					offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID);
				if (programID != null)
					offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.ProgramID == programID);
				if (facultyTypeEnum != null)
					offeredCourses = offeredCourses.Where(oc => oc.FacultyMember.FacultyType == (byte)facultyTypeEnum.Value);
				var facultyCourses = offeredCourses.Select(oc => new
				{
					oc.OfferedCourseID,
					oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
					oc.Cours.AdmissionOpenProgram.Program.Department.DepartmentName,
					oc.Cours.AdmissionOpenProgram.ProgramID,
					oc.FacultyMemberID,
					FacultyMemberName = oc.FacultyMember.Name,
					oc.Cours.Title,
					oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					oc.SemesterNo,
					oc.Section,
					oc.Shift,
				});

				var offeredCourseAttendance = offeredCourses.SelectMany(oc => oc.OfferedCourseAttendances).GroupBy(oca => new
				{
					oca.OfferedCourseID,
				}).Select(oca => new
				{
					oca.Key.OfferedCourseID,
					TotalHours = oca.Sum(s => s.Hours),
				});

				var query = from fc in facultyCourses
							join oca in offeredCourseAttendance on fc.OfferedCourseID equals oca.OfferedCourseID
							select new CourseWiseAttendance
							{
								DepartmentID = fc.DepartmentID,
								DepartmentName = fc.DepartmentName,
								ProgramID = fc.ProgramID,
								ProgramAlias = fc.ProgramAlias,
								FacultyMemberID = fc.FacultyMemberID,
								FacultyMemberName = fc.FacultyMemberName,
								Title = fc.Title,
								SemesterNo = fc.SemesterNo,
								Section = fc.Section,
								Shift = fc.Shift,
								TotalHours = oca.TotalHours,
							};
				return new ReportDataSet
				{
					PageHeader = new CourseWiseAttendanceSummaryPageHeader
					{
						SemesterID = semesterID,
						Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single(),
					},
					CourseWiseAttendanceSummary = query.ToList(),
				};
			}
		}
	}
}
