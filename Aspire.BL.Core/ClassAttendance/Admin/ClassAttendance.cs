﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.ClassAttendance.Admin
{
	public static class ClassAttendance
	{
		private static Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, int instituteID, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID, AdminPermissions.ClassAttendance.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int instituteID, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID, AdminPermissions.ClassAttendance.ManageClassAttendanceSettings, UserGroupPermission.PermissionValues.Allowed);
		}

		#region  Registered Courses of Student or Assigned Course of Faculty 

		public sealed class Course
		{
			internal Course()
			{
			}
			public short SemesterID { get; internal set; }
			public string InstituteAlias { get; internal set; }
			public int InstituteID { get; internal set; }
			public int OfferedCourseID { get; internal set; }
			public int? RegisteredCourseID { get; internal set; }
			public string Title { get; internal set; }
			public string Program { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public bool IsDeleted { get; internal set; }

			public string ClassName => AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift);
			public string FacultyMemberNameNA => this.FacultyMemberName.ToNAIfNullOrEmpty();
			public string IsDeletedYesNo => this.IsDeleted.ToYesNo();
		}

		public static List<Course> GetOfferedCourses(int instituteID, short semesterID, string enrollment, string facultyMember, string sortExpression, SortDirection sortDirection, int pageIndex, int pageSize, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				var coursesOffered = aspireContext.OfferedCourses.Where(oc => oc.SemesterID == semesterID && oc.Cours.AdmissionOpenProgram.Program.InstituteID == instituteID);
				if (!string.IsNullOrWhiteSpace(facultyMember))
					coursesOffered = coursesOffered.Where(oc => oc.FacultyMember.Name.Contains(facultyMember) || oc.FacultyMember.DisplayName.Contains(facultyMember) || oc.FacultyMember.UserName.Contains(facultyMember));
				else if (!string.IsNullOrWhiteSpace(enrollment))
					coursesOffered = coursesOffered.Where(oc => oc.RegisteredCourses.Any(rc => rc.Student.Enrollment == enrollment));

				virtualItemCount = coursesOffered.Count();

				switch (sortExpression)
				{
					case nameof(Course.InstituteAlias):
						coursesOffered = coursesOffered.OrderBy(sortDirection, co => co.Cours.AdmissionOpenProgram.Program.Institute.InstituteAlias);
						break;
					case nameof(Course.Title):
						coursesOffered = coursesOffered.OrderBy(sortDirection, co => co.Cours.Title);
						break;
					case nameof(Course.FacultyMemberName):
						coursesOffered = coursesOffered.OrderBy(sortDirection, co => co.FacultyMember.DisplayName);
						break;
					case nameof(Course.ClassName):
						coursesOffered = coursesOffered.OrderBy(sortDirection, co => co.Cours.AdmissionOpenProgram.Program.ProgramAlias)
							.ThenBy(sortDirection, oc => oc.SemesterNo)
							.ThenBy(sortDirection, oc => oc.Section)
							.ThenBy(sortDirection, oc => oc.Shift);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}

				return coursesOffered.Select(co => new Course
				{
					InstituteID = co.Cours.AdmissionOpenProgram.Program.InstituteID,
					OfferedCourseID = co.OfferedCourseID,
					RegisteredCourseID = co.RegisteredCourses.Where(rc => rc.Student.Enrollment == enrollment).Select(rc => (int?)rc.RegisteredCourseID).FirstOrDefault(),
					IsDeleted = enrollment != null && co.RegisteredCourses.Any(rc => rc.Student.Enrollment == enrollment && rc.DeletedDate != null),
					SemesterID = co.SemesterID,
					InstituteAlias = co.Cours.AdmissionOpenProgram.Program.Institute.InstituteAlias,
					Title = co.Cours.Title,
					Program = co.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = co.SemesterNo,
					Section = co.Section,
					Shift = co.Shift,
					FacultyMemberName = co.FacultyMember.Name,
				}).ToList();
			}
		}

		#endregion

		#region Update Attendance Record Of a Student

		public sealed class Summary
		{
			public sealed class AttendanceDetail
			{
				internal AttendanceDetail() { }
				public int OfferedCourseAttendanceID { get; internal set; }
				public int OfferedCourseID { get; internal set; }
				public DateTime ClassDate { get; internal set; }
				public byte ClassType { get; internal set; }
				public string ClassTypeFullName => ((OfferedCourseAttendance.ClassTypes)this.ClassType).ToString();
				public decimal TotalHours { get; internal set; }
				public string RoomName { get; internal set; }
				public string Remarks { get; internal set; }
				public string TopicsCovered { get; internal set; }
				internal OfferedCourseAttendanceDetail OfferedCourseAttendanceDetail { get; set; }
				public decimal? Hours => this.OfferedCourseAttendanceDetail?.Hours ?? 0M;
				public OfferedCourseAttendance.HoursValues? HoursEnum => this.Hours?.ToHoursValue();
				public OfferedCourseAttendance.HoursValues? TotalHoursEnum => this.Hours?.ToHoursValue();
			}

			internal Summary() { }
			public List<AttendanceDetail> AttendanceDetails { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public DateTime? DeletedDate { get; set; }
			public string StudentProgramAlias { get; internal set; }
			public string Title { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public decimal ContactHours { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public string OfferedProgramAlias { get; internal set; }
			public short OfferedSemesterNo { get; internal set; }
			public int OfferedSection { get; set; }
			public byte OfferedShift { get; set; }
			public short OfferedSemesterID { get; set; }
			public string OfferedClass => AspireFormats.GetClassName(this.OfferedProgramAlias, this.OfferedSemesterNo, this.OfferedSection, this.OfferedShift);
		}

		public static Summary GetStudentAttendanceDetails(int instituteID, int registeredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				return aspireContext.RegisteredCourses
					.Where(rc => rc.RegisteredCourseID == registeredCourseID && rc.Student.AdmissionOpenProgram.Program.InstituteID == instituteID)
					.Select(rc => new Summary
					{
						Enrollment = rc.Student.Enrollment,
						RegistrationNo = rc.Student.RegistrationNo,
						Name = rc.Student.Name,
						StudentProgramAlias = rc.Student.AdmissionOpenProgram.Program.ProgramAlias,
						DeletedDate = rc.DeletedDate,
						Title = rc.OfferedCours.Cours.Title,
						FacultyMemberName = rc.OfferedCours.FacultyMember.Name,
						CreditHours = rc.OfferedCours.Cours.CreditHours,
						ContactHours = rc.OfferedCours.Cours.ContactHours,
						OfferedProgramAlias = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						OfferedSemesterNo = rc.OfferedCours.SemesterNo,
						OfferedSection = rc.OfferedCours.Section,
						OfferedShift = rc.OfferedCours.Shift,
						OfferedSemesterID = rc.OfferedCours.SemesterID,
						AttendanceDetails = aspireContext.OfferedCourseAttendances
							.Where(oca => oca.OfferedCourseID == rc.OfferedCourseID)
							.Select(oca => new Summary.AttendanceDetail
							{
								OfferedCourseID = oca.OfferedCourseID,
								OfferedCourseAttendanceID = oca.OfferedCourseAttendanceID,
								ClassDate = oca.ClassDate,
								ClassType = oca.ClassType,
								Remarks = oca.Remarks,
								TopicsCovered = oca.TopicsCovered,
								RoomName = oca.Room.RoomName,
								TotalHours = oca.Hours,
								OfferedCourseAttendanceDetail = oca.OfferedCourseAttendanceDetails.FirstOrDefault(ocad => ocad.RegisteredCourseID == rc.RegisteredCourseID)
							}).ToList()
					}).SingleOrDefault();
			}
		}

		public class UpdateOfferedCourseAttendanceDetailStatuses
		{
			public int SuccessCount;
			public int TotalRecord;
			public int RegisteredCourseID;
			internal UpdateOfferedCourseAttendanceDetailStatuses()
			{
				this.SuccessCount = this.TotalRecord = 0;
			}

		}

		public static UpdateOfferedCourseAttendanceDetailStatuses UpdateOfferedCourseAttendanceDetailsOfStudent(int instituteID, int registeredCourseID, Dictionary<int, decimal> offeredCourseAttendanceIDs, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);

				var registeredCourse = aspireContext.RegisteredCourses
					.Where(rc => rc.RegisteredCourseID == registeredCourseID && rc.DeletedDate == null)
					.Where(rc => rc.OfferedCours.Cours.AdmissionOpenProgram.Program.InstituteID == instituteID)
					.Select(rc => new { rc.RegisteredCourseID })
					.SingleOrDefault();
				if (registeredCourse == null)
					return null;

				var result = new UpdateOfferedCourseAttendanceDetailStatuses();
				foreach (var offeredCourseAttendanceID in offeredCourseAttendanceIDs)
				{
					result.TotalRecord++;
					var ocaID = offeredCourseAttendanceID.Key;
					var hours = offeredCourseAttendanceID.Value;

					var offeredCourseAttendance = aspireContext.OfferedCourseAttendances
						.Where(ocad => ocad.OfferedCourseAttendanceID == ocaID)
						.Where(ocad => ocad.OfferedCours.Cours.AdmissionOpenProgram.Program.InstituteID == instituteID)
						.Select(oca => new
						{
							OfferedCourseAttendanceDetail = oca.OfferedCourseAttendanceDetails.FirstOrDefault(ocad => ocad.RegisteredCourseID == registeredCourseID)
						}).Single();

					if (offeredCourseAttendance.OfferedCourseAttendanceDetail == null) // means by default student mark as absent.
					{
						var hourValue = hours.ToHoursValue();
						if (hourValue != OfferedCourseAttendance.HoursValues.Absent)
						{
							var offeredCourseAttendanceDetailForAdd = new OfferedCourseAttendanceDetail
							{
								OfferedCourseAttendanceID = ocaID,
								RegisteredCourseID = registeredCourseID,
								Hours = hours,
							};
							aspireContext.OfferedCourseAttendanceDetails.Add(offeredCourseAttendanceDetailForAdd);
							result.SuccessCount++;
						}
					}
					else
					{
						offeredCourseAttendance.OfferedCourseAttendanceDetail.Hours = hours;
						result.SuccessCount++;
					}
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				}
				result.RegisteredCourseID = registeredCourseID;
				aspireContext.CommitTransaction();
				return result;
			}
		}

		#endregion

		#region Update Attendance Record Of a Class

		public class UpdateOfferedCourseAttendanceDetailOfClassStatuses
		{
			public int SuccessCount;
			public int TotalRecord;
			public int OfferedCourseAttendanceID;

			internal UpdateOfferedCourseAttendanceDetailOfClassStatuses()
			{
				this.SuccessCount = this.TotalRecord = 0;
			}

		}

		public static UpdateOfferedCourseAttendanceDetailOfClassStatuses UpdateOfferedCourseAttendanceDetailsOfClass(int instituteID, int offeredCourseAttendanceID, Dictionary<int, decimal> registeredCourseIDs, int roomID, OfferedCourseAttendance.ClassTypes classTypeEnum, string remarks, string topicsCovered, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				var result = new UpdateOfferedCourseAttendanceDetailOfClassStatuses();
				//Update the Offered Course Attendance
				if (!registeredCourseIDs.Any())
					throw new ArgumentNullException(nameof(registeredCourseIDs));
				var offeredCourseAttendance = aspireContext.OfferedCourseAttendances.SingleOrDefault(ocad => ocad.OfferedCourseAttendanceID == offeredCourseAttendanceID);
				if (offeredCourseAttendance == null)
					return result;
				offeredCourseAttendance.RoomID = roomID;
				offeredCourseAttendance.ClassTypeEnum = classTypeEnum;
				offeredCourseAttendance.Remarks = remarks;
				offeredCourseAttendance.TopicsCovered = topicsCovered;
				foreach (var registeredCourseID in registeredCourseIDs)
				{
					result.TotalRecord++;
					var rcID = registeredCourseID.Key;
					var hours = registeredCourseID.Value;
					var offeredCourseAttendanceDetail = aspireContext.OfferedCourseAttendanceDetails.SingleOrDefault(ocad => ocad.OfferedCourseAttendanceID == offeredCourseAttendanceID && ocad.RegisteredCourseID == rcID);
					if (offeredCourseAttendanceDetail == null) // means by default student mark as absent or previous record of old system which has only present records.
					{
						var hourValue = hours.ToHoursValue();
						if (hourValue != OfferedCourseAttendance.HoursValues.Absent)
						{
							var offeredCourseAttendanceDetailForAdd = new OfferedCourseAttendanceDetail
							{
								OfferedCourseAttendanceID = offeredCourseAttendanceID,
								RegisteredCourseID = rcID,
								Hours = hours,
							};
							aspireContext.OfferedCourseAttendanceDetails.Add(offeredCourseAttendanceDetailForAdd);
							result.SuccessCount++;
						}
					}
					else
					{
						offeredCourseAttendanceDetail.Hours = hours;
						result.SuccessCount++;
					}
				}
				result.OfferedCourseAttendanceID = offeredCourseAttendanceID;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return result;
			}
		}

		#endregion

		public class AddOfferedCourseAttendanceDetailOfClassStatuses
		{
			public int SuccessCount;
			public int TotalRecord;
			public int OfferedCourseAttendanceID;

			internal AddOfferedCourseAttendanceDetailOfClassStatuses()
			{
				this.SuccessCount = this.TotalRecord = 0;
			}

		}

		public static AddOfferedCourseAttendanceDetailOfClassStatuses AddOfferedCourseAttendanceDetailsOfClass(int instituteID, int offeredCourseID, Dictionary<int, decimal> registeredCourseIDs, DateTime classDate, int roomID, OfferedCourseAttendance.ClassTypes classTypeEnum, OfferedCourseAttendance.HoursValues hoursValue, string remarks, string topicsCovered, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				var result = new AddOfferedCourseAttendanceDetailOfClassStatuses();
				//Add the Offered Course Attendance First

				if (!registeredCourseIDs.Any())
					throw new ArgumentNullException(nameof(registeredCourseIDs));
				var offeredCourseAttendance = new OfferedCourseAttendance
				{
					OfferedCourseID = offeredCourseID,
					ClassDate = classDate,
					RoomID = roomID,
					ClassTypeEnum = classTypeEnum,
					HoursValue = hoursValue,
					Remarks = remarks,
					TopicsCovered = topicsCovered,
					MarkedByUserID = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == loginHistory.UserLoginHistoryID).Select(h => h.UserID).Single(),
					MarkedDate = DateTime.Now,
				};
				aspireContext.OfferedCourseAttendances.Add(offeredCourseAttendance);
				foreach (var registeredCourseID in registeredCourseIDs)
				{
					result.TotalRecord++;
					var rcID = registeredCourseID.Key;
					var hours = registeredCourseID.Value;
					var studentAttendanceHoursValue = hours.ToHoursValue();
					if (!offeredCourseAttendance.HoursValue.GetSmallerOrEqualHourValue().Contains(studentAttendanceHoursValue))
						throw new InvalidOperationException();
					var offeredCourseAttendanceDetail = new OfferedCourseAttendanceDetail
					{
						RegisteredCourseID = rcID,
						Hours = hours,
					};
					offeredCourseAttendance.OfferedCourseAttendanceDetails.Add(offeredCourseAttendanceDetail);
					result.SuccessCount++;
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				result.OfferedCourseAttendanceID = offeredCourseAttendance.OfferedCourseAttendanceID;
				return result;
			}
		}

		public enum DeleteOfferedCourseAttendanceDetailOfClassStatuses
		{
			NoRecordFound,
			CanNotDelete,
			Success,
		}

		public static DeleteOfferedCourseAttendanceDetailOfClassStatuses DeleteOfferedCourseAttendanceDetailsOfClass(int instituteID, int offeredCourseAttendanceID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				var offeredCourseAttendance = aspireContext.OfferedCourseAttendances.Include(oca => oca.OfferedCourseAttendanceDetails).SingleOrDefault(oca => oca.OfferedCourseAttendanceID == offeredCourseAttendanceID);
				if (offeredCourseAttendance == null)
					return DeleteOfferedCourseAttendanceDetailOfClassStatuses.NoRecordFound;
				if (!aspireContext.OfferedCourses.Any(oc => oc.OfferedCourseID == offeredCourseAttendance.OfferedCourseID))
					return DeleteOfferedCourseAttendanceDetailOfClassStatuses.CanNotDelete;
				aspireContext.OfferedCourseAttendanceDetails.RemoveRange(offeredCourseAttendance.OfferedCourseAttendanceDetails);
				aspireContext.OfferedCourseAttendances.Remove(offeredCourseAttendance);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteOfferedCourseAttendanceDetailOfClassStatuses.Success;
			}
		}
	}
}
