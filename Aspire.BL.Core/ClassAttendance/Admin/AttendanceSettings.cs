﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.ClassAttendance.Admin
{
	public static class AttendanceSettings
	{
		private static Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.ClassAttendance.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.ClassAttendance.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.ClassAttendance.ManageClassAttendanceSettings, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.ClassAttendance.ManageClassAttendanceSettings, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class AddClassAttendanceSettingResults
		{
			internal AddClassAttendanceSettingResults() { }
			public enum Statuses
			{
				None,
				NoRecordFound,
				AlreadyExists,
				InvalidFromDate,
				InvalidDateRange,
				Success,
			}

			public int Added { get; internal set; }
			public int Updated { get; internal set; }
		}

		public static AddClassAttendanceSettingResults AddClassAttendanceSetting(short semesterID, int instituteID, int? programID, DateTime fromDate, DateTime toDate, byte canMarkPastDays, byte canEditPastDays, ClassAttendanceSetting.Statuses statusEnum, Guid loginSessionGuid)
		{
			if (fromDate > toDate)
				throw new ArgumentException(nameof(fromDate));
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				var result = new AddClassAttendanceSettingResults
				{
					Added = 0,
					Updated = 0,
				};

				int[] programIDs;
				if (programID != null)
					programIDs = new[] { programID.Value };
				else
					programIDs = aspireContext.Programs.Where(p => p.InstituteID == instituteID).Select(p => p.ProgramID).ToArray();

				foreach (var pID in programIDs)
				{
					var classAttendanceSetting = aspireContext.ClassAttendanceSettings.SingleOrDefault(s => s.SemesterID == semesterID && s.ProgramID == pID);
					if (classAttendanceSetting != null)
					{
						classAttendanceSetting.CanEditPastDays = canEditPastDays;
						classAttendanceSetting.CanMarkPastDays = canMarkPastDays;
						classAttendanceSetting.FromDate = fromDate.Date;
						classAttendanceSetting.ToDate = toDate.Date;
						classAttendanceSetting.StatusEnum = statusEnum;
						result.Updated++;
					}
					else
					{
						classAttendanceSetting = new ClassAttendanceSetting
						{
							SemesterID = semesterID,
							ProgramID = pID,
							CanEditPastDays = canEditPastDays,
							CanMarkPastDays = canMarkPastDays,
							FromDate = fromDate.Date,
							ToDate = toDate.Date,
							StatusEnum = statusEnum,
						};
						aspireContext.ClassAttendanceSettings.Add(classAttendanceSetting);
						result.Added++;
					}
					classAttendanceSetting.Validate();
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return result;
			}
		}

		public enum UpdateClassAttendanceSettingStatuses
		{
			NoRecordFound,
			Success
		}

		public static UpdateClassAttendanceSettingStatuses UpdateClassAttendanceSetting(int classAttendanceSettingID, DateTime fromDate, DateTime toDate, byte canMarkPastDays, byte canEditPastDays, ClassAttendanceSetting.Statuses statusEnum, Guid loginSessionGuid)
		{
			if (fromDate > toDate)
				throw new ArgumentException(nameof(fromDate));
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var classAttendanceSetting = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Programs).SelectMany(p => p.ClassAttendanceSettings).SingleOrDefault(cas => cas.ClassAttendanceSettingID == classAttendanceSettingID);
				if (classAttendanceSetting == null)
					return UpdateClassAttendanceSettingStatuses.NoRecordFound;

				classAttendanceSetting.FromDate = fromDate.Date;
				classAttendanceSetting.ToDate = toDate.Date;
				classAttendanceSetting.CanMarkPastDays = canMarkPastDays;
				classAttendanceSetting.CanEditPastDays = canEditPastDays;
				classAttendanceSetting.StatusEnum = statusEnum;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateClassAttendanceSettingStatuses.Success;
			}
		}

		public enum DeleteClassAttendanceSettingStatuses
		{
			NoRecordFound,
			Success,
		}

		public static DeleteClassAttendanceSettingStatuses DeleteClassAttendanceSetting(int classAttendanceSettingID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var classAttendanceSetting = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Programs).SelectMany(p => p.ClassAttendanceSettings).SingleOrDefault(cas => cas.ClassAttendanceSettingID == classAttendanceSettingID);
				if (classAttendanceSetting == null)
					return DeleteClassAttendanceSettingStatuses.NoRecordFound;

				aspireContext.ClassAttendanceSettings.Remove(classAttendanceSetting);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteClassAttendanceSettingStatuses.Success;
			}
		}

		public static UpdateClassAttendanceSettingStatuses ToggleClassAttendanceSettingStatus(int classAttendanceSettingID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var classAttendanceSetting = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Programs).SelectMany(p => p.ClassAttendanceSettings).SingleOrDefault(cas => cas.ClassAttendanceSettingID == classAttendanceSettingID);
				if (classAttendanceSetting == null)
					return UpdateClassAttendanceSettingStatuses.NoRecordFound;

				switch (classAttendanceSetting.StatusEnum)
				{
					case ClassAttendanceSetting.Statuses.Active:
						classAttendanceSetting.StatusEnum = ClassAttendanceSetting.Statuses.Inactive;
						break;
					case ClassAttendanceSetting.Statuses.Inactive:
						classAttendanceSetting.StatusEnum = ClassAttendanceSetting.Statuses.Active;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateClassAttendanceSettingStatuses.Success;
			}
		}

		public static ClassAttendanceSetting GetClassAttendanceSetting(int classAttendanceSettingID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Programs).SelectMany(p => p.ClassAttendanceSettings).Include(cas => cas.Program).SingleOrDefault(cas => cas.ClassAttendanceSettingID == classAttendanceSettingID);
			}
		}

		public sealed class GetClassAttendanceSettingsResult
		{
			public sealed class Setting
			{
				public int ClassAttendanceSettingID { get; internal set; }
				public short SemesterID { get; internal set; }
				public string Semester => this.SemesterID.ToSemesterString();
				public string InstituteAlias { get; internal set; }
				public string ProgramAlias { get; internal set; }
				public DateTime FromDate { get; internal set; }
				public DateTime ToDate { get; internal set; }
				public byte CanMarkPastDays { get; internal set; }
				public byte CanEditPastDays { get; internal set; }
				public byte Status { get; internal set; }
				public string StatusFullName => ((ClassAttendanceSetting.Statuses)this.Status).ToFullName();
			}

			internal GetClassAttendanceSettingsResult()
			{
			}

			public List<Setting> Settings { get; internal set; }
			public int VirtualItemCount { get; internal set; }
		}

		public static GetClassAttendanceSettingsResult GetClassAttendanceSettings(short? semesterID, int? instituteID, ClassAttendanceSetting.Statuses? statusEnum, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				var query = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Programs).SelectMany(p => p.ClassAttendanceSettings);

				if (semesterID != null)
					query = query.Where(q => q.SemesterID == semesterID);
				if (instituteID != null)
					query = query.Where(q => q.Program.InstituteID == instituteID);
				if (statusEnum != null)
				{
					var status = (byte?)statusEnum;
					query = query.Where(q => q.Status == status);
				}
				var result = new GetClassAttendanceSettingsResult
				{
					VirtualItemCount = query.Count(),
				};
				switch (sortExpression)
				{
					case nameof(ClassAttendanceSetting.SemesterID):
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.SemesterID) : query.OrderByDescending(q => q.SemesterID);
						break;
					case nameof(Institute.InstituteAlias):
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.Program.Institute.InstituteAlias) : query.OrderByDescending(q => q.Program.Institute.InstituteAlias);
						break;
					case nameof(Program.ProgramAlias):
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.Program.ProgramAlias) : query.OrderByDescending(q => q.Program.ProgramAlias);
						break;
					case nameof(ClassAttendanceSetting.FromDate):
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.FromDate) : query.OrderByDescending(q => q.FromDate);
						break;
					case nameof(ClassAttendanceSetting.ToDate):
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.ToDate) : query.OrderByDescending(q => q.ToDate);
						break;
					case nameof(ClassAttendanceSetting.CanMarkPastDays):
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.CanMarkPastDays) : query.OrderByDescending(q => q.CanMarkPastDays);
						break;
					case nameof(ClassAttendanceSetting.CanEditPastDays):
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.CanEditPastDays) : query.OrderByDescending(q => q.CanEditPastDays);
						break;
					case nameof(ClassAttendanceSetting.Status):
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.Status) : query.OrderByDescending(q => q.Status);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				query = query.Skip(pageIndex * pageSize).Take(pageSize);
				result.Settings = query.Select(q => new GetClassAttendanceSettingsResult.Setting
				{
					ClassAttendanceSettingID = q.ClassAttendanceSettingID,
					SemesterID = q.SemesterID,
					InstituteAlias = q.Program.Institute.InstituteAlias,
					ProgramAlias = q.Program.ProgramAlias,
					FromDate = q.FromDate,
					ToDate = q.ToDate,
					CanMarkPastDays = q.CanMarkPastDays,
					CanEditPastDays = q.CanEditPastDays,
					Status = q.Status,
				}).ToList();
				return result;
			}
		}
	}
}
