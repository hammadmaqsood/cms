using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.LoginManagement
{
	public static class Staff
	{
		public static UserRole.LoginResult Login(string username, string password, string browser, string ipAddress)
		{
			return UserTypes.Staff.Login(username, password, browser, ipAddress);
		}

		public static UserRole.LoginResult ChangeRole(Guid loginSessionGuid, int newUserRoleID, string browser, string ipAddress)
		{
			return UserTypes.Staff.ChangeRole(loginSessionGuid, newUserRoleID, browser, ipAddress);
		}

		public static List<UserGroupMenuLink> GetUserGroupMenuLinks(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var userGroups = aspireContext.ViewActiveStaffUserLogins.Where(h => h.LoginSessionGuid == loginSessionGuid).Select(h => h.UserGroupID);
				return aspireContext.UserGroupMenuLinks.Where(l => userGroups.Contains(l.UserGroupID)).ToList();
			}
		}
	}
}