using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.LoginManagement
{
	internal static class IntegratedService
	{
		internal sealed class LoginHistory
		{
			internal LoginHistory() { }
			public int IntegratedServiceUserID { get; internal set; }
			public string ServiceName { get; internal set; }
			public int UserLoginHistoryID { get; internal set; }
			public Guid LoginSessionGuid { get; internal set; }
			public Guid Username { get; internal set; }
			public int? InstituteID { get; internal set; }
			public string IPAddress { get; internal set; }
			public byte ServiceType { get; internal set; }
			public IntegratedServiceUser.ServiceTypes ServiceTypeEnum => (IntegratedServiceUser.ServiceTypes)this.ServiceType;
		}

		private static IQueryable<IntegratedServiceUser> GetIntegratedServiceUsers(this AspireContext aspireContext, Guid username, IntegratedServiceUser.ServiceTypes serviceTypeEnum)
		{
			var serviceType = (byte)serviceTypeEnum;
			return aspireContext.IntegratedServiceUsers.Where(u => u.UserName == username && u.ServiceType == serviceType && u.IsDeleted == false && u.ExpiryDate > DateTime.Now && u.Status == IntegratedServiceUser.StatusActive);
		}

		internal static bool Authenticate(Guid username, Guid password, IntegratedServiceUser.ServiceTypes serviceTypeEnum)
		{
			if (username == default || username == Guid.Empty)
				return false;
			if (password == default || password == Guid.Empty)
				return false;
			using (var aspireContext = new AspireContext(true))
			{
				var integratedServiceUser = aspireContext.GetIntegratedServiceUsers(username, serviceTypeEnum).Select(u => new { u.IntegratedServiceUserID, u.Password }).SingleOrDefault();
				UserLoginHistory.LoginFailureReasons? loginFailureReasonEnum;
				int? integratedServiceUserID = null;
				if (integratedServiceUser != null)
				{
					if (!string.IsNullOrWhiteSpace(integratedServiceUser.Password) && password.ToString().CheckPassword(integratedServiceUser.Password))
						return true;
					loginFailureReasonEnum = UserLoginHistory.LoginFailureReasons.InvalidPassword;
					integratedServiceUserID = integratedServiceUser.IntegratedServiceUserID;
				}
				else
					loginFailureReasonEnum = UserLoginHistory.LoginFailureReasons.UsernameNotFound;

				var userLoginHistory = aspireContext.UserLoginHistories.Add(new UserLoginHistory
				{
					UserTypeEnum = UserTypes.IntegratedService,
					SubUserTypeEnum = SubUserTypes.None,
					UserAgent = null,
					IPAddress = null,
					UserName = username.ToString("D"),
					LoginDate = DateTime.Now,
					LoginSessionGuid = Guid.NewGuid(),
					LoginStatusEnum = UserLoginHistory.LoginStatuses.Failure,
					CandidateID = null,
					FacultyMemberID = null,
					StudentID = null,
					UserID = null,
					UserRoleID = null,
					ImpersonatedByUserLoginHistoryID = null,
					LogOffDate = null,
					LogOffReasonEnum = null,
					LoginFailureReasonEnum = loginFailureReasonEnum,
					IntegratedServiceUserID = integratedServiceUserID,
				});
				userLoginHistory.Validate();
				aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
				return false;
			}
		}


		internal static LoginHistory Login(Guid username, IntegratedServiceUser.ServiceTypes serviceTypeEnum, string ipAddress)
		{
			var loginAttemptDate = DateTime.Now;
			using (var aspireContext = new AspireContext(true))
			{
				var userLoginHistory = aspireContext.UserLoginHistories.Add(new UserLoginHistory
				{
					UserTypeEnum = UserTypes.IntegratedService,
					SubUserTypeEnum = SubUserTypes.None,
					UserAgent = null,
					IPAddress = ipAddress,
					UserName = username.ToString("D"),
					LoginDate = loginAttemptDate,
					LoginSessionGuid = Guid.NewGuid(),
					LoginStatusEnum = UserLoginHistory.LoginStatuses.Failure,
					CandidateID = null,
					FacultyMemberID = null,
					StudentID = null,
					UserID = null,
					UserRoleID = null,
					ImpersonatedByUserLoginHistoryID = null,
					LogOffDate = null,
					LogOffReasonEnum = null,
					LoginFailureReasonEnum = null,
				});

				var integratedServiceUser = aspireContext.GetIntegratedServiceUsers(username, serviceTypeEnum).Select(u => new { u.IntegratedServiceUserID, u.IPAddress }).Single();
				userLoginHistory.IntegratedServiceUserID = integratedServiceUser.IntegratedServiceUserID;
				var _ipAddress = ipAddress.ToIPAddress();
#if DEBUG
				if (!System.Net.IPAddress.IsLoopback(_ipAddress))
#endif
					if (!integratedServiceUser.IPAddress.ToIPAddress().IsEqual(_ipAddress))
					{
						userLoginHistory.LoginFailureReasonEnum = UserLoginHistory.LoginFailureReasons.IPAddressNotAuthorized;
						userLoginHistory.Validate();
						aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
						return null;
					}
				userLoginHistory.LoginStatusEnum = UserLoginHistory.LoginStatuses.Success;
				userLoginHistory.Validate();
				aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
				return aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == userLoginHistory.UserLoginHistoryID).Select(h => new LoginHistory
				{
					ServiceName = h.IntegratedServiceUser.ServiceName,
					UserLoginHistoryID = h.UserLoginHistoryID,
					IntegratedServiceUserID = h.IntegratedServiceUserID.Value,
					LoginSessionGuid = h.LoginSessionGuid,
					InstituteID = h.IntegratedServiceUser.InstituteID,
					Username = h.IntegratedServiceUser.UserName,
					ServiceType = h.IntegratedServiceUser.ServiceType,
					IPAddress = h.IPAddress,
				}).Single();
			}
		}
	}
}