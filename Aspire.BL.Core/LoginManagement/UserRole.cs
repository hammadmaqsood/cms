﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.LoginManagement
{
	public static class UserRole
	{
		public sealed class Role
		{
			internal Role() { }
			public int UserRoleID { get; internal set; }
			public string InstituteAlias { get; internal set; }
		}

		public sealed class LoginHistory
		{
			internal LoginHistory() { }
			public List<UserGroupPermission> UserGroupPermissions { get; internal set; }
			public string Username { get; internal set; }
			public string Name { get; internal set; }
			public string Email { get; internal set; }
			public int UserLoginHistoryID { get; internal set; }
			public Guid LoginSessionGuid { get; internal set; }
			public int UserID { get; internal set; }
			public int UserRoleID { get; internal set; }
			public List<Role> Roles { get; internal set; }
			public int UserGroupID { get; internal set; }
			public int InstituteID { get; internal set; }
			public string InstituteName { get; internal set; }
			public string InstituteShortName { get; internal set; }
			public string InstituteAlias { get; internal set; }
			public string InstituteCode { get; internal set; }
		}

		public sealed class LoginResult
		{
			public UserLoginHistory.LoginFailureReasons? LoginFailureReason { get; internal set; }
			public LoginHistory LoginHistory { get; internal set; }

			internal LoginResult() { }
			internal LoginResult(UserLoginHistory.LoginFailureReasons loginFailureReasons)
			{
				this.LoginFailureReason = loginFailureReasons;
			}
		}

		private static LoginResult Login(this AspireContext aspireContext, UserTypes userTypeEnum, string username, string password, int? userRoleID, string userAgent, string ipAddress, DateTime loginAttemptDate)
		{
			if (!userTypeEnum.IsStaff() && !userTypeEnum.IsExecutive())
				throw new ArgumentOutOfRangeException(nameof(userTypeEnum));

			username = username.CannotBeEmptyOrWhitespace();
			if (userRoleID == null)
				password = password.CannotBeEmptyOrWhitespace();

			var userLoginHistory = aspireContext.UserLoginHistories.Add(new UserLoginHistory
			{
				UserTypeEnum = userTypeEnum,
				SubUserTypeEnum = SubUserTypes.None,
				UserAgent = userAgent,
				IPAddress = ipAddress,
				UserName = username,
				LoginDate = loginAttemptDate,
				LoginSessionGuid = Guid.NewGuid(),
				LoginStatusEnum = UserLoginHistory.LoginStatuses.Failure,
				CandidateID = null,
				FacultyMemberID = null,
				StudentID = null,
				UserRoleID = null,
				ImpersonatedByUserLoginHistoryID = null,
				LogOffDate = null,
				LogOffReasonEnum = null,
				LoginFailureReasonEnum = null,
				UserID = null,
			});

			var loginFailed = new Func<UserLoginHistory.LoginFailureReasons, LoginResult>(delegate (UserLoginHistory.LoginFailureReasons reason)
			{
				userLoginHistory.LoginFailureReasonEnum = reason;
				userLoginHistory.Validate();
				aspireContext.SaveChangesAsSystemUser();
				return new LoginResult(reason);
			});

			var user = aspireContext.Users.SingleOrDefault(u => u.Username == username && u.IsDeleted == false);
			if (user == null)
				return loginFailed(UserLoginHistory.LoginFailureReasons.UsernameNotFound);
			userLoginHistory.UserName = user.Username;
			userLoginHistory.UserID = user.UserID;
			if (password != null && !password.CheckPassword(user.Password))
				return loginFailed(UserLoginHistory.LoginFailureReasons.InvalidPassword);
			if (user.StatusEnum == User.Statuses.Inactive)
				return loginFailed(UserLoginHistory.LoginFailureReasons.AccountIsInactive);

			var userType = (short)userTypeEnum;
			var userRoles = user.UserRoles1.Where(r => r.UserID == user.UserID && r.UserType == userType && r.IsDeleted == false && r.Institute.Status == Institute.StatusActive);
			if (!userRoles.Any())
				return loginFailed(UserLoginHistory.LoginFailureReasons.NoRoleFound);
			if (!userRoles.Any(r => r.ExpiryDate > DateTime.Today))
				return loginFailed(UserLoginHistory.LoginFailureReasons.AccountExpired);

			Model.Entities.UserRole defaultUserRole;
			if (userRoleID == null)
				defaultUserRole = userRoles.SingleOrDefault(r => r.IsDefault) ?? userRoles.First();
			else
				defaultUserRole = userRoles.Single(r => r.UserRoleID == userRoleID);
			userLoginHistory.UserRoleID = defaultUserRole.UserRoleID;
			userLoginHistory.LoginStatusEnum = UserLoginHistory.LoginStatuses.Success;
			userLoginHistory.Validate();
			aspireContext.SaveChangesAsSystemUser();
			return new LoginResult
			{
				LoginFailureReason = null,
				LoginHistory = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == userLoginHistory.UserLoginHistoryID).Select(h => new LoginHistory
				{
					LoginSessionGuid = h.LoginSessionGuid,
					UserLoginHistoryID = h.UserLoginHistoryID,
					UserID = h.User.UserID,
					UserRoleID = h.UserRole.UserRoleID,
					UserGroupID = h.UserRole.UserGroupID.Value,
					Email = h.User.Email,
					Name = h.User.Name,
					Username = h.User.Username,
					InstituteID = h.UserRole.InstituteID.Value,
					InstituteAlias = h.UserRole.Institute.InstituteAlias,
					InstituteShortName = h.UserRole.Institute.InstituteShortName,
					InstituteName = h.UserRole.Institute.InstituteName,
					InstituteCode = h.UserRole.Institute.InstituteCode,
					UserGroupPermissions = aspireContext.UserGroupPermissions.Where(g => g.UserGroupID == h.UserRole.UserGroupID).ToList(),
					Roles = aspireContext.UserRoles.Where(r => r.UserID == h.UserID && r.IsDeleted == false && r.UserType == userType && r.Status == (byte)Model.Entities.UserRole.Statuses.Active && r.ExpiryDate > DateTime.Today).Select(r => new Role
					{
						UserRoleID = r.UserRoleID,
						InstituteAlias = r.Institute.InstituteAlias,
					}).ToList()
				}).Single(),
			};
		}

		internal static LoginResult Login(this UserTypes userTypeEnum, string username, string password, string browser, string ipAddress)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var result = aspireContext.Login(userTypeEnum, username, password, null, browser, ipAddress, DateTime.Now);
				aspireContext.CommitTransaction();
				return result;
			}
		}

		internal static LoginResult ChangeRole(this UserTypes userTypeEnum, Guid loginSessionGuid, int newUserRoleID, string browser, string ipAddress)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var now = DateTime.Now;
				var status = aspireContext.Logoff(loginSessionGuid, UserLoginHistory.LogOffReasons.ChangeRole, now);
				switch (status)
				{
					case Common.LogoffStatus.NoRecordFound:
					case Common.LogoffStatus.AlreadyLoggedOff:
					case Common.LogoffStatus.LoginStatusIsNotSuccess:
						throw new InvalidOperationException();
					case Common.LogoffStatus.Success:
						var userName = aspireContext.UserLoginHistories.Where(h => h.LoginSessionGuid == loginSessionGuid).Select(h => h.User.Username).Single();
						var result = aspireContext.Login(userTypeEnum, userName, null, newUserRoleID, browser, ipAddress, now);
						aspireContext.CommitTransaction();
						return result;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
	}
}
