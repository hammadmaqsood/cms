using Aspire.BL.Core.Admissions.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.LoginManagement
{
	public static class Candidate
	{
		public sealed class LoginResult
		{
			public UserLoginHistory.LoginFailureReasons? LoginFailureReason { get; }
			public string UserName { get; }
			public string Name { get; }
			public string Email { get; }
			public int UserLoginHistoryID { get; }
			public Guid LoginSessionGuid { get; }
			public int CandidateID { get; }
			public bool ForeignStudent { get; }
			public short SemesterID { get; }
			public Genders GenderEnum { get; }
			public int? ImpersonatedByUserLoginHistoryID { get; }
			public string IPAddress { get; }
			public string UserAgent { get; }

			private LoginResult() { }
			internal LoginResult(string userName, string name, string email, int userLoginHistoryID, Guid loginSessionGuid, int candidateID, bool foreignStudent, short semesterID, Genders? genderEnum, int? impersonatedByUserLoginHistoryID, string ipAddress, string userAgent) : this()
			{
				this.UserName = userName;
				this.Name = name;
				this.Email = email;
				this.UserLoginHistoryID = userLoginHistoryID;
				this.LoginSessionGuid = loginSessionGuid;
				this.CandidateID = candidateID;
				this.ForeignStudent = foreignStudent;
				this.SemesterID = semesterID;
				this.GenderEnum = genderEnum ?? Genders.Male;
				this.ImpersonatedByUserLoginHistoryID = impersonatedByUserLoginHistoryID;
				this.IPAddress = ipAddress;
				this.UserAgent = userAgent;
				this.LoginFailureReason = null;
			}

			internal LoginResult(UserLoginHistory.LoginFailureReasons loginFailureReasons)
			{
				this.LoginFailureReason = loginFailureReasons;
			}
		}

		public static LoginResult Login(short semesterID, string email, string password, int? candidateIDToBeImpersonated, int? impersonatedByUserLoginHistoryID, string userAgent, string ipAddress)
		{
			email = email.TrimAndCannotEmptyAndMustBeLowerCase();
			password = password.CannotBeEmptyOrWhitespace();
			var now = DateTime.Now;
			using (var aspireContext = new AspireContext(true))
			{
				var userLoginHistory = aspireContext.UserLoginHistories.Add(new UserLoginHistory
				{
					UserAgent = userAgent,
					CandidateID = null,
					FacultyMemberID = null,
					IPAddress = ipAddress,
					ImpersonatedByUserLoginHistoryID = impersonatedByUserLoginHistoryID,
					LogOffDate = null,
					LogOffReasonEnum = null,
					LoginDate = now,
					LoginSessionGuid = Guid.NewGuid(),
					StudentID = null,
					UserID = null,
					UserRoleID = null,
					IntegratedServiceUserID = null,
					UserName = email,
					UserTypeEnum = UserTypes.Candidate,
					SubUserTypeEnum = SubUserTypes.None,
					LoginFailureReasonEnum = null,
					LoginStatusEnum = UserLoginHistory.LoginStatuses.Failure,
				});

				var loginFailed = new Func<UserLoginHistory.LoginFailureReasons, LoginResult>(delegate (UserLoginHistory.LoginFailureReasons reason)
				{
					userLoginHistory.LoginFailureReasonEnum = reason;
					userLoginHistory.Validate();
					aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
					return new LoginResult(reason);
				});

				var candidate = aspireContext.Candidates
					.Where(c => c.SemesterID == semesterID && (c.Email == email || c.CandidateID == candidateIDToBeImpersonated))
					.Select(c => new
					{
						c.CandidateID,
						c.ForeignStudent,
						c.Password,
						c.Name,
						c.Email,
						c.SemesterID,
						GenderEnum = (Genders?)c.Gender
					})
					.SingleOrDefault();
				if (candidate == null)
					return loginFailed(UserLoginHistory.LoginFailureReasons.UsernameNotFound);

				userLoginHistory.CandidateID = candidate.CandidateID;

				if (impersonatedByUserLoginHistoryID == null && candidateIDToBeImpersonated == null)
				{
					if (!password.CheckPassword(candidate.Password))
						return loginFailed(UserLoginHistory.LoginFailureReasons.InvalidPassword);
				}
				else if (impersonatedByUserLoginHistoryID != null && candidateIDToBeImpersonated != null)
				{
					//exempted
				}
				else
					throw new ArgumentException();

				var loginSucceed = new Func<LoginResult>(delegate
				{
					userLoginHistory.LoginFailureReasonEnum = null;
					userLoginHistory.LoginStatusEnum = UserLoginHistory.LoginStatuses.Success;
					userLoginHistory.Validate();
					aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
					return new LoginResult(candidate.Email, candidate.Name, candidate.Email, userLoginHistory.UserLoginHistoryID, userLoginHistory.LoginSessionGuid, candidate.CandidateID, candidate.ForeignStudent, candidate.SemesterID, candidate.GenderEnum, userLoginHistory.ImpersonatedByUserLoginHistoryID, userLoginHistory.IPAddress, userLoginHistory.UserAgent);
				});

				if (aspireContext.AdmissionOpenPrograms.FilterOpenAdmissions(candidate.ForeignStudent).Any(s => s.SemesterID == semesterID))
					return loginSucceed();

				var loginAllowedAdmissionOpenPrograms = aspireContext.AdmissionOpenPrograms.Where(aop => aop.SemesterID == semesterID && now <= aop.LoginAllowedEndDate).Select(aop => aop.AdmissionOpenProgramID);
				if (aspireContext.CandidateAppliedPrograms.Any(cap => cap.CandidateID == candidate.CandidateID && loginAllowedAdmissionOpenPrograms.Contains(cap.Choice1AdmissionOpenProgramID)))
					return loginSucceed();

				return loginFailed(UserLoginHistory.LoginFailureReasons.AccountExpired);
			}
		}
	}
}