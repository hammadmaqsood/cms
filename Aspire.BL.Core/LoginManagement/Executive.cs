using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.LoginManagement
{
	public static class Executive
	{
		public static UserRole.LoginResult Login(string username, string password, string browser, string ipAddress)
		{
			return UserTypes.Executive.Login(username, password, browser, ipAddress);
		}

		public static UserRole.LoginResult ChangeRole(Guid loginSessionGuid, int newUserRoleID, string browser, string ipAddress)
		{
			return UserTypes.Executive.ChangeRole(loginSessionGuid, newUserRoleID, browser, ipAddress);
		}

		public static List<UserGroupMenuLink> GetUserGroupMenuLinks(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandExecutiveActiveSession(loginSessionGuid);
				return aspireContext.UserGroupMenuLinks.Where(l => l.UserGroupID == loginHistory.UserGroupID).ToList();
			}
		}
	}
}