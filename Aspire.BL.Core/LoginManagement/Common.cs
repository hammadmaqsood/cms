﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.LoginManagement
{
	public static class Common
	{
		public enum LogoffStatus
		{
			NoRecordFound,
			AlreadyLoggedOff,
			LoginStatusIsNotSuccess,
			Success
		}

		internal static LogoffStatus Logoff(this AspireContext aspireContext, Guid loginSessionGuid, UserLoginHistory.LogOffReasons logOffReason, DateTime logoffDate)
		{
			var userLoginHistory = aspireContext.UserLoginHistories.SingleOrDefault(h => h.LoginSessionGuid == loginSessionGuid);
			if (userLoginHistory == null)
				return LogoffStatus.NoRecordFound;
			if (userLoginHistory.LoginStatusEnum != UserLoginHistory.LoginStatuses.Success)
				return LogoffStatus.LoginStatusIsNotSuccess;
			if (userLoginHistory.LogOffDate != null)
				return LogoffStatus.AlreadyLoggedOff;
			userLoginHistory.LogOffReasonEnum = logOffReason;
			userLoginHistory.LogOffDate = logoffDate;
			userLoginHistory.Validate();
			aspireContext.SaveChangesAsSystemUser();
			return LogoffStatus.Success;
		}

		public static LogoffStatus Logoff(Guid loginSessionGuid, UserLoginHistory.LogOffReasons logOffReason)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var status = aspireContext.Logoff(loginSessionGuid, logOffReason, DateTime.Now);
				aspireContext.CommitTransaction();
				return status;
			}
		}

		public static List<UserPreference> GetUserPreferences(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				return aspireContext.UserPreferences
					.Where(p => p.UserRoleID == loginHistory.UserRoleID)
					.Where(p => p.StudentID == loginHistory.StudentID)
					.Where(p => p.FacultyMemberID == loginHistory.FacultyMemberID)
					.ToList();
			}
		}

		public static UserPreference GetUserPreference(Guid settingGuid, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				return aspireContext.UserPreferences
					.Where(p => p.UserRoleID == loginHistory.UserRoleID)
					.Where(p => p.StudentID == loginHistory.StudentID)
					.Where(p => p.FacultyMemberID == loginHistory.FacultyMemberID)
					.SingleOrDefault(p => p.SettingGuid == settingGuid);
			}
		}

		public static UserPreference SaveUserPreferences(Guid settingGuid, string settingValue, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var userPreference = aspireContext.UserPreferences
					.Where(p => p.UserRoleID == loginHistory.UserRoleID)
					.Where(p => p.StudentID == loginHistory.StudentID)
					.Where(p => p.FacultyMemberID == loginHistory.FacultyMemberID)
					.SingleOrDefault(p => p.SettingGuid == settingGuid)
					?? aspireContext.UserPreferences.Add(new UserPreference
					{
						FacultyMemberID = loginHistory.FacultyMemberID,
						StudentID = loginHistory.StudentID,
						UserRoleID = loginHistory.UserRoleID,
						SettingGuid = settingGuid,
					});
				userPreference.SettingValue = settingValue;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				return userPreference;
			}
		}

		public enum ChangePasswordResult
		{
			NoRecordFound,
			StatusIsNotActive,
			CurrentPasswordDoNotMatch,
			NewPasswordIsSameAsCurrentPassword,
			Success,
		}

		public static ChangePasswordResult ChangePassword(string currentPassword, string newPassword, Guid loginSessionGuid)
		{
			currentPassword = currentPassword.CannotBeEmptyOrWhitespace();
			newPassword = newPassword.ValidatePassword();
			if (currentPassword == newPassword)
				return ChangePasswordResult.NewPasswordIsSameAsCurrentPassword;
			string oldHashedPassword = null;
			string newHashedPassword = null;
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				int? candidateID = null;
				int? facultyMemberID = null;
				int? userID = null;
				int? studentID = null;
				int? studentIDOfParent = null;
				switch (loginHistory.UserTypeEnum)
				{
					case Model.Entities.Common.UserTypes.MasterAdmin:
					case Model.Entities.Common.UserTypes.Admin:
					case Model.Entities.Common.UserTypes.Executive:
					case Model.Entities.Common.UserTypes.Staff:
						{
							var user = aspireContext.Users.SingleOrDefault(u => u.UserID == loginHistory.UserID);
							if (user == null)
								return ChangePasswordResult.NoRecordFound;
							if (user.StatusEnum != User.Statuses.Active)
								return ChangePasswordResult.StatusIsNotActive;
							if (!currentPassword.CheckPassword(user.Password))
								return ChangePasswordResult.CurrentPasswordDoNotMatch;
							oldHashedPassword = user.Password;
							user.Password = newPassword.EncryptPassword();
							newHashedPassword = user.Password;
							break;
						}
					case Model.Entities.Common.UserTypes.Faculty:
						{
							var facultyMember = aspireContext.FacultyMembers.SingleOrDefault(fm => fm.FacultyMemberID == loginHistory.FacultyMemberID);
							if (facultyMember == null)
								return ChangePasswordResult.NoRecordFound;
							if (facultyMember.StatusEnum != FacultyMember.Statuses.Active)
								return ChangePasswordResult.StatusIsNotActive;
							if (!currentPassword.CheckPassword(facultyMember.Password))
								return ChangePasswordResult.CurrentPasswordDoNotMatch;
							oldHashedPassword = facultyMember.Password;
							facultyMember.Password = newPassword.EncryptPassword();
							newHashedPassword = facultyMember.Password;
							break;
						}
					case Model.Entities.Common.UserTypes.Student:
						{
							var student = aspireContext.Students.SingleOrDefault(s => s.StudentID == loginHistory.StudentID);
							if (student == null)
								return ChangePasswordResult.NoRecordFound;
							switch (loginHistory.SubUserTypeEnum)
							{
								case Model.Entities.Common.SubUserTypes.None:
									{
										if (!currentPassword.CheckPassword(student.Password))
											return ChangePasswordResult.CurrentPasswordDoNotMatch;
										oldHashedPassword = student.Password;
										student.Password = newPassword.EncryptPassword();
										newHashedPassword = student.Password;
										student.ConfirmationCode = null;
										student.ConfirmationCodeExpiryDate = null;
										break;
									}
								case Model.Entities.Common.SubUserTypes.Parents:
									{
										if (!currentPassword.CheckPassword(student.ParentsPassword))
											return ChangePasswordResult.CurrentPasswordDoNotMatch;
										oldHashedPassword = student.ParentsPassword;
										student.ParentsPassword = newPassword.EncryptPassword();
										newHashedPassword = student.ParentsPassword;
										student.ParentsConfirmationCode = null;
										student.ParentsConfirmationCodeExpiryDate = null;
										break;
									}
								default:
									throw new NotImplementedEnumException(loginHistory.SubUserTypeEnum);
							}
							break;
						}
					case Model.Entities.Common.UserTypes.Candidate:
						{
							var candidate = aspireContext.Candidates.SingleOrDefault(c => c.CandidateID == loginHistory.CandidateID);
							if (candidate == null)
								return ChangePasswordResult.NoRecordFound;
							if (!currentPassword.CheckPassword(candidate.Password))
								return ChangePasswordResult.CurrentPasswordDoNotMatch;
							oldHashedPassword = candidate.Password;
							candidate.Password = newPassword.EncryptPassword();
							newHashedPassword = candidate.Password;
							break;
						}
					default:
						throw new NotImplementedEnumException(loginHistory.UserTypeEnum);
				}
				aspireContext.PasswordChangeHistories.Add(new PasswordChangeHistory
				{
					ChangeReasonEnum = Model.Entities.Common.PasswordChangeReasons.ByUserItSelf,
					UserLoginHistoryID = loginHistory.UserLoginHistoryID,
					ChangedDate = DateTime.Now,
					UserID = userID,
					CandidateID = candidateID,
					FacultyMemberID = facultyMemberID,
					StudentID = studentID,
					StudentIDOfParent = studentIDOfParent,
					OldPassword = oldHashedPassword,
					NewPassword = newHashedPassword
				});
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return ChangePasswordResult.Success;
			}
		}
	}
}
