﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.LoginManagement
{
	public static class Faculty
	{
		public sealed class LoginResult
		{
			public sealed class LoginInformation
			{
				internal LoginInformation() { }
				public int FacultyMemberID { get; set; }
				public string UserName { get; set; }
				public string Email { get; set; }
				public string Name { get; set; }
				public int UserLoginHistoryID { get; set; }
				public Guid LoginSessionGuid { get; set; }
				public int InstituteID { get; set; }
				public string InstituteName { get; set; }
				public string InstituteShortName { get; set; }
				public string InstituteAlias { get; set; }
			}

			public UserLoginHistory.LoginFailureReasons? LoginFailureReason { get; }
			public LoginInformation LoginInfo { get; }

			private LoginResult() { }
			internal LoginResult(LoginInformation loginInfo) : this()
			{
				this.LoginInfo = loginInfo;
			}
			internal LoginResult(UserLoginHistory.LoginFailureReasons loginFailureReasons) : this(null)
			{
				this.LoginFailureReason = loginFailureReasons;
			}
		}

		public static LoginResult Login(int instituteID, string username, string password, string userAgent, string ipAddress)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginAttemptDate = DateTime.Now;
				username = username.CannotBeEmptyOrWhitespace();
				password = password.CannotBeEmptyOrWhitespace();

				var userLoginHistory = aspireContext.UserLoginHistories.Add(new UserLoginHistory
				{
					UserName = username,
					CandidateID = null,
					FacultyMemberID = null,
					StudentID = null,
					UserRoleID = null,
					UserTypeEnum = UserTypes.Faculty,
					SubUserTypeEnum = SubUserTypes.None,
					UserAgent = userAgent,
					IPAddress = ipAddress,
					ImpersonatedByUserLoginHistoryID = null,
					LogOffDate = null,
					LogOffReasonEnum = null,
					LoginFailureReasonEnum = null,
					LoginDate = loginAttemptDate,
					LoginSessionGuid = Guid.NewGuid(),
					LoginStatusEnum = UserLoginHistory.LoginStatuses.Failure,
					UserID = null,
				});

				var loginFailed = new Func<UserLoginHistory.LoginFailureReasons, LoginResult>(delegate (UserLoginHistory.LoginFailureReasons reason)
				{
					userLoginHistory.LoginFailureReasonEnum = reason;
					userLoginHistory.Validate();
					aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
					return new LoginResult(reason);
				});

				var facultyMember = aspireContext.FacultyMembers.Where(u => u.InstituteID == instituteID && u.UserName == username).Select(fm => new
				{
					fm.FacultyMemberID,
					fm.UserName,
					fm.Name,
					fm.Email,
					fm.Password,
					StatusEnum = (Model.Entities.FacultyMember.Statuses)fm.Status,
					fm.InstituteID,
					fm.Institute.InstituteName,
					fm.Institute.InstituteShortName,
					fm.Institute.InstituteAlias
				}).SingleOrDefault();
				if (facultyMember == null)
					return loginFailed(UserLoginHistory.LoginFailureReasons.UsernameNotFound);

				userLoginHistory.UserName = facultyMember.UserName;
				userLoginHistory.FacultyMemberID = facultyMember.FacultyMemberID;
				if (password != null && !password.CheckPassword(facultyMember.Password))
					return loginFailed(UserLoginHistory.LoginFailureReasons.InvalidPassword);
				if (facultyMember.StatusEnum == Model.Entities.FacultyMember.Statuses.Inactive)
					return loginFailed(UserLoginHistory.LoginFailureReasons.AccountIsInactive);
				userLoginHistory.LoginStatusEnum = UserLoginHistory.LoginStatuses.Success;
				userLoginHistory.Validate();
				aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
				return new LoginResult(new LoginResult.LoginInformation
				{
					InstituteID = facultyMember.InstituteID,
					InstituteName = facultyMember.InstituteName,
					InstituteAlias = facultyMember.InstituteAlias,
					InstituteShortName = facultyMember.InstituteShortName,
					Name = facultyMember.Name,
					FacultyMemberID = facultyMember.FacultyMemberID,
					Email = facultyMember.Email,
					UserName = facultyMember.UserName,
					LoginSessionGuid = userLoginHistory.LoginSessionGuid,
					UserLoginHistoryID = userLoginHistory.UserLoginHistoryID
				});
			}
		}

		public enum ChangePasswordResult
		{
			Success,
			NewPasswordIsSameAsCurrentPassword,
			StatusIsNotActive,
			CurrentPasswordDonotMatch,
			NoRecordFound
		}

		public static ChangePasswordResult ChangePassword(string currentPassword, string newPassword, Guid loginSessionGuid)
		{
			currentPassword = currentPassword.CannotBeEmptyOrWhitespace();
			newPassword = newPassword.ValidatePassword();
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandFacultyActiveSession(loginSessionGuid);
				var facultyMember = aspireContext.FacultyMembers.SingleOrDefault(fm => fm.FacultyMemberID == loginHistory.FacultyMemberID);
				if (facultyMember == null)
					return ChangePasswordResult.NoRecordFound;
				if (facultyMember.StatusEnum != Model.Entities.FacultyMember.Statuses.Active)
					return ChangePasswordResult.StatusIsNotActive;
				if (!currentPassword.CheckPassword(facultyMember.Password))
					return ChangePasswordResult.CurrentPasswordDonotMatch;
				if (currentPassword == newPassword)
					return ChangePasswordResult.NewPasswordIsSameAsCurrentPassword;
				var oldHashedPassword = facultyMember.Password;
				facultyMember.Password = newPassword.EncryptPassword();
				aspireContext.PasswordChangeHistories.Add(new PasswordChangeHistory
				{
					ChangeReasonEnum = PasswordChangeReasons.ByUserItSelf,
					FacultyMemberID = facultyMember.FacultyMemberID,
					ChangedDate = DateTime.Now,
					UserLoginHistoryID = loginHistory.UserLoginHistoryID,
					OldPassword = oldHashedPassword,
					NewPassword = facultyMember.Password,
					StudentID = null,
					CandidateID = null,
					StudentIDOfParent = null,
					UserID = null
				});
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return ChangePasswordResult.Success;
			}
		}

		public sealed class FacultyMember
		{
			internal FacultyMember() { }

			public string Name { get; internal set; }
			public Model.Entities.FacultyMember.Titles? TitleEnum { get; internal set; }
			public string FullName => this.TitleEnum == null ? this.Name : $"{this.TitleEnum.ToString().ToUpper()}. {this.Name}";
		}

		public static FacultyMember GetFacultyMember(Guid confirmationCode)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.FacultyMembers
					.Where(fm => fm.ConfirmationCode == confirmationCode && fm.ConfirmationCodeExpiryDate >= DateTime.Now)
					.Select(fm => new FacultyMember
					{
						Name = fm.Name,
						TitleEnum = (Model.Entities.FacultyMember.Titles?)fm.Title
					}).SingleOrDefault();
			}
		}

		public static bool ResetPassword(Guid confirmationCode, string newPassword)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var facultyMember = aspireContext.FacultyMembers.SingleOrDefault(fm => fm.ConfirmationCode == confirmationCode && fm.ConfirmationCodeExpiryDate.Value >= DateTime.Now);
				if (facultyMember == null)
					return false;
				var oldHashedPassword = facultyMember.Password;
				facultyMember.Password = newPassword.ValidatePassword().EncryptPassword();
				facultyMember.ConfirmationCode = null;
				facultyMember.ConfirmationCodeExpiryDate = null;
				aspireContext.PasswordChangeHistories.Add(new PasswordChangeHistory
				{
					UserLoginHistoryID = UserLoginHistory.SystemUserLoginHistoryID,
					ChangeReasonEnum = PasswordChangeReasons.UsingResetLink,
					ChangedDate = DateTime.Now,
					FacultyMemberID = facultyMember.FacultyMemberID,
					OldPassword = oldHashedPassword,
					NewPassword = facultyMember.Password,
					UserID = null,
					StudentIDOfParent = null,
					CandidateID = null,
					StudentID = null
				});
				aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
				return true;
			}
		}
	}
}
