using Aspire.BL.Core.Common.EmailsManagement;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.LoginManagement
{
	public static class Student
	{
		public sealed class LoginResult
		{
			public sealed class LoginInformation
			{
				internal LoginInformation() { }
				public string UserName { get; internal set; }
				public string Name { get; internal set; }
				public string UniversityEmail { get; internal set; }
				public int UserLoginHistoryID { get; internal set; }
				public Guid LoginSessionGuid { get; internal set; }
				public int InstituteID { get; internal set; }
				public string InstituteCode { get; internal set; }
				public string InstituteName { get; internal set; }
				public string InstituteShortName { get; internal set; }
				public string InstituteAlias { get; internal set; }
				public int StudentID { get; internal set; }
				public string Enrollment { get; internal set; }
				public SubUserTypes SubUserTypeEnum { get; internal set; }
			}

			public UserLoginHistory.LoginFailureReasons? LoginFailureReason { get; }
			public string BlockedReason { get; }
			public LoginInformation LoginInfo { get; }

			internal LoginResult(LoginInformation loginInformation)
			{
				this.LoginInfo = loginInformation;
				this.LoginFailureReason = null;
			}

			internal LoginResult(UserLoginHistory.LoginFailureReasons loginFailureReasons, string blockedReason)
			{
				this.LoginInfo = null;
				this.LoginFailureReason = loginFailureReasons;
				if (this.LoginFailureReason == UserLoginHistory.LoginFailureReasons.StudentStatusBlocked)
					this.BlockedReason = blockedReason.TrimAndMakeItNullIfEmpty();
				else
					this.BlockedReason = blockedReason.MustBeNullOrEmpty();
			}
		}

		public static LoginResult Login(int instituteID, string enrollment, string password, SubUserTypes subUserTypeEnum, string userAgent, string ipAddress)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginAttemptDate = DateTime.Now;
				enrollment = enrollment.CannotBeEmptyOrWhitespace();
				password = password.CannotBeEmptyOrWhitespace();

				var userLoginHistory = aspireContext.UserLoginHistories.Add(new UserLoginHistory
				{
					UserName = enrollment,
					CandidateID = null,
					FacultyMemberID = null,
					StudentID = null,
					UserRoleID = null,
					UserTypeEnum = UserTypes.Student,
					SubUserTypeEnum = subUserTypeEnum,
					UserAgent = userAgent,
					IPAddress = ipAddress,
					ImpersonatedByUserLoginHistoryID = null,
					LogOffDate = null,
					LogOffReasonEnum = null,
					LoginFailureReasonEnum = null,
					LoginDate = loginAttemptDate,
					LoginSessionGuid = Guid.NewGuid(),
					LoginStatusEnum = UserLoginHistory.LoginStatuses.Failure,
					UserID = null,
				});

				LoginResult LoginFailed(UserLoginHistory.LoginFailureReasons reason, string blockedReason = null)
				{
					userLoginHistory.LoginFailureReasonEnum = reason;
					userLoginHistory.Validate();
					aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
					return new LoginResult(reason, blockedReason);
				};

				var student = aspireContext.Students
					.Where(s => s.Enrollment == enrollment && s.AdmissionOpenProgram.Program.InstituteID == instituteID)
					.Select(s => new
					{
						s.StudentID,
						s.Enrollment,
						s.Password,
						s.ParentsPassword,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.Institute.InstituteCode,
						s.AdmissionOpenProgram.Program.Institute.InstituteName,
						s.AdmissionOpenProgram.Program.Institute.InstituteShortName,
						s.AdmissionOpenProgram.Program.Institute.InstituteAlias,
						s.UniversityEmail,
						s.Name,
						s.RegistrationNo,
						StatusEnum = (Model.Entities.Student.Statuses?)s.Status,
						s.BlockedReason
					})
					.SingleOrDefault();
				if (student == null)
					return LoginFailed(UserLoginHistory.LoginFailureReasons.UsernameNotFound);

				userLoginHistory.StudentID = student.StudentID;
				userLoginHistory.UserName = student.Enrollment;
				switch (subUserTypeEnum)
				{
					case SubUserTypes.None:
						if (password == null || !password.CheckPassword(student.Password))
							return LoginFailed(UserLoginHistory.LoginFailureReasons.InvalidPassword);
						break;
					case SubUserTypes.Parents:
						if (password == null || !password.CheckPassword(student.ParentsPassword))
							return LoginFailed(UserLoginHistory.LoginFailureReasons.InvalidPassword);
						break;
					default:
						throw new NotImplementedEnumException(subUserTypeEnum);
				}

				switch (student.StatusEnum)
				{
					case Model.Entities.Student.Statuses.AdmissionCancelled:
						return LoginFailed(UserLoginHistory.LoginFailureReasons.StudentStatusAdmissionCanceled);
					case Model.Entities.Student.Statuses.Blocked:
						return LoginFailed(UserLoginHistory.LoginFailureReasons.StudentStatusBlocked, student.BlockedReason);
					case Model.Entities.Student.Statuses.Deferred:
						return LoginFailed(UserLoginHistory.LoginFailureReasons.StudentStatusDeferred);
					case Model.Entities.Student.Statuses.Dropped:
						return LoginFailed(UserLoginHistory.LoginFailureReasons.StudentStatusDropped);
					case Model.Entities.Student.Statuses.Expelled:
						return LoginFailed(UserLoginHistory.LoginFailureReasons.StudentStatusExpelled);
					case Model.Entities.Student.Statuses.Graduated:
						return LoginFailed(UserLoginHistory.LoginFailureReasons.StudentStatusGraduated);
					case Model.Entities.Student.Statuses.Left:
						return LoginFailed(UserLoginHistory.LoginFailureReasons.StudentStatusLeft);
					case Model.Entities.Student.Statuses.ProgramChanged:
						return LoginFailed(UserLoginHistory.LoginFailureReasons.StudentStatusProgramChanged);
					case Model.Entities.Student.Statuses.Rusticated:
						return LoginFailed(UserLoginHistory.LoginFailureReasons.StudentStatusRusticated);
					case Model.Entities.Student.Statuses.TransferredToOtherCampus:
					case null:
						break;
					default:
						throw new NotImplementedEnumException(student.StatusEnum);
				}

				userLoginHistory.LoginFailureReason = null;
				userLoginHistory.LoginStatusEnum = UserLoginHistory.LoginStatuses.Success;
				aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
				return new LoginResult(new LoginResult.LoginInformation
				{
					StudentID = userLoginHistory.StudentID.Value,
					Enrollment = student.Enrollment,
					InstituteID = student.InstituteID,
					InstituteCode = student.InstituteCode,
					InstituteName = student.InstituteName,
					InstituteShortName = student.InstituteShortName,
					InstituteAlias = student.InstituteAlias,
					LoginSessionGuid = userLoginHistory.LoginSessionGuid,
					Name = student.Name,
					UniversityEmail = student.UniversityEmail,
					UserLoginHistoryID = userLoginHistory.UserLoginHistoryID,
					UserName = userLoginHistory.UserName,
					SubUserTypeEnum = subUserTypeEnum
				});
			}
		}

		public sealed class ForgotPasswordResult
		{
			internal ForgotPasswordResult() { }
			public string UniversityEmail { get; internal set; }
			public string PersonalEmail { get; internal set; }
		}

		public static ForgotPasswordResult ForgotPassword(string enrollment, string email, int instituteID, string pageURL)
		{
			email = email.ValidateEmailAddress().CannotBeEmptyOrWhitespace();
			enrollment = enrollment.TrimAndCannotBeEmpty();
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.Students.SingleOrDefault(s => s.Enrollment == enrollment && s.AdmissionOpenProgram.Program.InstituteID == instituteID);
				if (student == null)
					return null;
				if (!string.Equals(email, student.PersonalEmail, StringComparison.OrdinalIgnoreCase) && !string.Equals(email, student.UniversityEmail, StringComparison.OrdinalIgnoreCase))
					return null;

				student.ConfirmationCode = Guid.NewGuid();
				student.ConfirmationCodeExpiryDate = DateTime.Today.AddDays(3);
				aspireContext.SaveChangesAsSystemUser();

				if (!string.IsNullOrWhiteSpace(student.PersonalEmail) && !string.IsNullOrWhiteSpace(student.UniversityEmail))
				{
					aspireContext.SendForgotPasswordEmailToPersonalEmail(pageURL, student.Name, student.PersonalEmail, student.UniversityEmail, student.UniversityEmailPassword, student.ConfirmationCode.Value, student.ConfirmationCodeExpiryDate.Value);
					aspireContext.SendForgotPasswordEmailToUniversityEmail(pageURL, student.Name, student.UniversityEmail, student.ConfirmationCode.Value, student.ConfirmationCodeExpiryDate.Value);
				}
				else if (!string.IsNullOrWhiteSpace(student.UniversityEmail))
					aspireContext.SendForgotPasswordEmailToUniversityEmail(pageURL, student.Name, student.UniversityEmail, student.ConfirmationCode.Value, student.ConfirmationCodeExpiryDate.Value);
				else if (!string.IsNullOrWhiteSpace(student.PersonalEmail))
					aspireContext.SendForgotPasswordEmailToUniversityEmail(pageURL, student.Name, student.PersonalEmail, student.ConfirmationCode.Value, student.ConfirmationCodeExpiryDate.Value);
				aspireContext.CommitTransaction();
				return new ForgotPasswordResult
				{
					PersonalEmail = student.PersonalEmail.ToNullIfWhiteSpace(),
					UniversityEmail = student.UniversityEmail.ToNullIfWhiteSpace(),
				};
			}
		}

		private static void SendForgotPasswordEmailToUniversityEmail(this AspireContext aspireContext, string pageURL, string name, string universityEmail, Guid confirmationCode, DateTime confirmationCodeExpiryDate)
		{
			var url = $"{pageURL}?C={confirmationCode:N}";
			var subject = "Reset Password";
			var emailBody = Properties.Resources.StudentForgotPasswordForUniversityEmail;
			emailBody = emailBody.Replace("{LinkHtmlAttributeEncoded}", url.HtmlAttributeEncode());
			emailBody = emailBody.Replace("{LinkHtmlEncoded}", url.HtmlEncode());
			emailBody = emailBody.Replace("{NameHtmlEncoded}", name.HtmlEncode());
			emailBody = emailBody.Replace("{ConfirmationLinkExpiryDate}", confirmationCodeExpiryDate.ToString("F").HtmlEncode());
			var createdDate = DateTime.Now;
			var toList = new[] { (name, universityEmail) };
			aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.StudentForgotPassword, subject, emailBody, true, Email.Priorities.Highest, createdDate, confirmationCodeExpiryDate, UserLoginHistory.SystemUserLoginHistoryID, toList: toList);
		}

		private static void SendForgotPasswordEmailToPersonalEmail(this AspireContext aspireContext, string pageURL, string name, string personalEmail, string universityEmail, string universityEmailPassword, Guid confirmationCode, DateTime confirmationCodeExpiryDate)
		{
			var url = $"{pageURL}?C={confirmationCode:N}";
			var subject = "Reset Password";
			var emailBody = Properties.Resources.StudentForgotPasswordForPersonalEmail;
			emailBody = emailBody.Replace("{LinkHtmlAttributeEncoded}", url.HtmlAttributeEncode());
			emailBody = emailBody.Replace("{LinkHtmlEncoded}", url.HtmlEncode());
			emailBody = emailBody.Replace("{NameHtmlEncoded}", name.HtmlEncode());
			emailBody = emailBody.Replace("{ConfirmationLinkExpiryDate}", confirmationCodeExpiryDate.ToString("F").HtmlEncode());
			emailBody = emailBody.Replace("{UniversityEmail}", universityEmail.HtmlEncode());
			emailBody = emailBody.Replace("{UniversityEmailPassword}", universityEmailPassword.HtmlEncode());
			var createdDate = DateTime.Now;
			var toList = new[] { (name, personalEmail) };
			aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.StudentForgotPassword, subject, emailBody, true, Email.Priorities.Highest, createdDate, confirmationCodeExpiryDate, UserLoginHistory.SystemUserLoginHistoryID, toList: toList);
		}

		public enum ConfirmationCodeStatuses
		{
			NoRecordFound,
			LinkExpired,
			Success,
		}

		public static ConfirmationCodeStatuses GetConfirmationCodeStatus(Guid confirmationCode, bool isParent)
		{
			using (var aspireContext = new AspireContext())
			{
				var student = aspireContext.Students
					.Where(s => (isParent == false && s.ConfirmationCode == confirmationCode) || (isParent && s.ParentsConfirmationCode == confirmationCode))
					.Select(s => new
					{
						s.ConfirmationCodeExpiryDate,
						s.ParentsConfirmationCodeExpiryDate,
					}).SingleOrDefault();
				if (student == null)
					return ConfirmationCodeStatuses.NoRecordFound;
				if (isParent)
				{
					if (student.ParentsConfirmationCodeExpiryDate >= DateTime.Now)
						return ConfirmationCodeStatuses.Success;
					return ConfirmationCodeStatuses.LinkExpired;
				}

				if (student.ConfirmationCodeExpiryDate >= DateTime.Now)
					return ConfirmationCodeStatuses.Success;
				return ConfirmationCodeStatuses.LinkExpired;
			}
		}

		public enum ResetPasswordStatuses
		{
			NoRecordFound,
			LinkExpired,
			Success,
		}

		public static ResetPasswordStatuses ResetPassword(Guid confirmationCode, bool isParent, string newPassword)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.Students
					.SingleOrDefault(s => (isParent == false && s.ConfirmationCode == confirmationCode) || (isParent && s.ParentsConfirmationCode == confirmationCode));
				if (student == null)
					return ResetPasswordStatuses.NoRecordFound;
				var now = DateTime.Now;
				if (isParent)
				{
					if (student.ParentsConfirmationCodeExpiryDate >= DateTime.Now)
					{
						var oldHashedPassword = student.ParentsPassword;
						student.ParentsPassword = newPassword.ValidatePassword().EncryptPassword();
						student.ParentsConfirmationCode = null;
						student.ParentsConfirmationCodeExpiryDate = null;
						aspireContext.PasswordChangeHistories.Add(new PasswordChangeHistory
						{
							ChangeReasonEnum = PasswordChangeReasons.UsingResetLink,
							StudentIDOfParent = student.StudentID,
							ChangedDate = now,
							UserLoginHistoryID = UserLoginHistory.SystemUserLoginHistoryID,
							CandidateID = null,
							FacultyMemberID = null,
							StudentID = null,
							UserID = null,
							OldPassword = oldHashedPassword,
							NewPassword = student.ParentsPassword
						});
						aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
						return ResetPasswordStatuses.Success;
					}
					return ResetPasswordStatuses.LinkExpired;
				}
				if (student.ConfirmationCodeExpiryDate >= DateTime.Now)
				{
					var oldHashedPassword = student.Password;
					student.Password = newPassword.ValidatePassword().EncryptPassword();
					student.ConfirmationCode = null;
					student.ConfirmationCodeExpiryDate = null;
					aspireContext.PasswordChangeHistories.Add(new PasswordChangeHistory
					{
						ChangeReasonEnum = PasswordChangeReasons.UsingResetLink,
						StudentID = student.StudentID,
						ChangedDate = now,
						UserLoginHistoryID = UserLoginHistory.SystemUserLoginHistoryID,
						CandidateID = null,
						FacultyMemberID = null,
						StudentIDOfParent = null,
						UserID = null,
						OldPassword = oldHashedPassword,
						NewPassword = student.Password
					});
					aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
					return ResetPasswordStatuses.Success;
				}
				return ResetPasswordStatuses.LinkExpired;
			}
		}

		public enum ChangePasswordStatuses
		{
			WrongOldPassword,
			NewPasswordIsSameAsCurrentPassword,
			Success,
		}

		public static ChangePasswordStatuses ChangePassword(string oldPassword, string newPassword, Guid loginSessionGuid)
		{
			oldPassword = oldPassword.ValidatePassword();
			newPassword = newPassword.ValidatePassword();
			if (oldPassword == newPassword)
				return ChangePasswordStatuses.NewPasswordIsSameAsCurrentPassword;
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStudentActiveSession(loginSessionGuid, null);
				var student = aspireContext.Students.Single(s => s.StudentID == loginHistory.StudentID);
				var now = DateTime.Now;
				switch (loginHistory.SubUserTypeEnum)
				{
					case SubUserTypes.None:
						if (!oldPassword.CheckPassword(student.Password))
							return ChangePasswordStatuses.WrongOldPassword;
						var oldHashedPassword = student.Password;
						student.Password = newPassword.EncryptPassword();
						student.ConfirmationCode = null;
						student.ConfirmationCodeExpiryDate = null;
						aspireContext.PasswordChangeHistories.Add(new PasswordChangeHistory
						{
							ChangeReasonEnum = PasswordChangeReasons.ByUserItSelf,
							StudentID = student.StudentID,
							ChangedDate = now,
							UserLoginHistoryID = UserLoginHistory.SystemUserLoginHistoryID,
							StudentIDOfParent = null,
							CandidateID = null,
							FacultyMemberID = null,
							OldPassword = oldHashedPassword,
							NewPassword = student.Password
						});
						aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
						return ChangePasswordStatuses.Success;
					case SubUserTypes.Parents:
						if (!oldPassword.CheckPassword(student.ParentsPassword))
							return ChangePasswordStatuses.WrongOldPassword;
						oldHashedPassword = student.ParentsPassword;
						student.ParentsPassword = newPassword.EncryptPassword();
						student.ParentsConfirmationCode = null;
						student.ParentsConfirmationCodeExpiryDate = null;
						aspireContext.PasswordChangeHistories.Add(new PasswordChangeHistory
						{
							ChangeReasonEnum = PasswordChangeReasons.ByUserItSelf,
							StudentIDOfParent = student.StudentID,
							ChangedDate = DateTime.Now,
							UserLoginHistoryID = UserLoginHistory.SystemUserLoginHistoryID,
							StudentID = null,
							CandidateID = null,
							FacultyMemberID = null,
							OldPassword = oldHashedPassword,
							NewPassword = student.ParentsPassword
						});
						aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
						return ChangePasswordStatuses.Success;
					default:
						throw new NotImplementedEnumException(loginHistory.SubUserTypeEnum);
				}
			}
		}
	}
}