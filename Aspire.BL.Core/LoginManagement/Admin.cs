﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.LoginManagement
{
	public static class Admin
	{
		public sealed class LoginHistory
		{
			internal LoginHistory() { }
			public List<UserGroupPermission> UserGroupPermissions { get; internal set; }
			public string Username { get; internal set; }
			public string Name { get; internal set; }
			public string Email { get; internal set; }
			public int UserLoginHistoryID { get; internal set; }
			public Guid LoginSessionGuid { get; internal set; }
			public int UserID { get; internal set; }
			public int UserRoleID { get; internal set; }
			public int? UserGroupID { get; set; }
			public short UserType { get; internal set; }
			public UserTypes UserTypeEnum => (UserTypes)this.UserType;
			public int? ImpersonatedByUserLoginHistoryID { get; internal set; }
		}

		public sealed class LoginResult
		{
			public UserLoginHistory.LoginFailureReasons? LoginFailureReason { get; internal set; }
			public LoginHistory LoginHistory { get; internal set; }

			internal LoginResult() { }
			internal LoginResult(UserLoginHistory.LoginFailureReasons loginFailureReasons)
			{
				this.LoginFailureReason = loginFailureReasons;
			}
		}

		public static LoginResult Login(string username, string password, string userAgent, string ipAddress)
		{
			using (var aspireContext = new AspireContext(true))
			{
				username = username.CannotBeEmptyOrWhitespace();
				password = password.CannotBeEmptyOrWhitespace();
				var loginAttemptDate = DateTime.Now;
				var userLoginHistory = aspireContext.UserLoginHistories.Add(new UserLoginHistory
				{
					UserName = username,
					CandidateID = null,
					FacultyMemberID = null,
					StudentID = null,
					UserRoleID = null,
					UserTypeEnum = UserTypes.Admin,
					SubUserTypeEnum = SubUserTypes.None,
					UserAgent = userAgent,
					IPAddress = ipAddress,
					ImpersonatedByUserLoginHistoryID = null,
					LogOffDate = null,
					LogOffReasonEnum = null,
					LoginFailureReasonEnum = null,
					LoginDate = loginAttemptDate,
					LoginSessionGuid = Guid.NewGuid(),
					LoginStatusEnum = UserLoginHistory.LoginStatuses.Failure,
					UserID = null,
				});

				var loginFailed = new Func<UserLoginHistory.LoginFailureReasons, LoginResult>(delegate (UserLoginHistory.LoginFailureReasons reason)
				{
					userLoginHistory.LoginFailureReasonEnum = reason;
					userLoginHistory.Validate();
					aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
					return new LoginResult(reason);
				});

				var user = aspireContext.Users.SingleOrDefault(u => u.Username == username && u.IsDeleted == false);
				if (user == null)
					return loginFailed(UserLoginHistory.LoginFailureReasons.UsernameNotFound);
				userLoginHistory.UserName = user.Username;
				userLoginHistory.UserID = user.UserID;
				if (password != null && !password.CheckPassword(user.Password))
					return loginFailed(UserLoginHistory.LoginFailureReasons.InvalidPassword);
				if (user.StatusEnum == User.Statuses.Inactive)
					return loginFailed(UserLoginHistory.LoginFailureReasons.AccountIsInactive);
				var userRole = aspireContext.UserRoles.SingleOrDefault(r => r.UserID == user.UserID && r.IsDeleted == false && (r.UserType == UserTypeConstants.Admin || r.UserType == UserTypeConstants.MasterAdmin));
				if (userRole == null)
					return loginFailed(UserLoginHistory.LoginFailureReasons.NoRoleFound);
				switch (userRole.StatusEnum)
				{
					case Model.Entities.UserRole.Statuses.Active:
						break;
					case Model.Entities.UserRole.Statuses.Inactive:
						return loginFailed(UserLoginHistory.LoginFailureReasons.UserRoleIsInactive);
					default:
						throw new ArgumentOutOfRangeException();
				}
				if (userRole.ExpiryDate < DateTime.Now)
					return loginFailed(UserLoginHistory.LoginFailureReasons.AccountExpired);

				if (!userRole.UserTypeEnum.IsMasterAdmin())
					if (!aspireContext.UserRoleInstitutes.Any(r => r.UserRoleID == userRole.UserRoleID && r.Institute.Status == Institute.StatusActive))
						return loginFailed(UserLoginHistory.LoginFailureReasons.NoRoleFound);

				userLoginHistory.UserRoleID = userRole.UserRoleID;
				if (userRole.UserTypeEnum.IsMasterAdmin())
					userLoginHistory.UserTypeEnum = UserTypes.MasterAdmin;
				userLoginHistory.LoginStatusEnum = UserLoginHistory.LoginStatuses.Success;
				userLoginHistory.Validate();
				aspireContext.SaveChangesAsSystemUserAndCommitTransaction();

				if (userRole.UserTypeEnum.IsMasterAdmin())
					return new LoginResult
					{
						LoginFailureReason = null,
						LoginHistory = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == userLoginHistory.UserLoginHistoryID).Select(h => new LoginHistory
						{
							LoginSessionGuid = h.LoginSessionGuid,
							UserLoginHistoryID = h.UserLoginHistoryID,
							UserID = h.UserID.Value,
							UserType = h.UserType,
							UserRoleID = h.UserRoleID.Value,
							Name = h.User.Name,
							Username = h.User.Username,
							Email = h.User.Email,
							UserGroupID = null,
							ImpersonatedByUserLoginHistoryID = null,
						}).Single(),
					};
				return new LoginResult
				{
					LoginFailureReason = null,
					LoginHistory = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == userLoginHistory.UserLoginHistoryID).Select(h => new LoginHistory
					{
						LoginSessionGuid = h.LoginSessionGuid,
						UserLoginHistoryID = h.UserLoginHistoryID,
						UserID = h.UserID.Value,
						UserType = h.UserType,
						UserRoleID = h.UserRoleID.Value,
						Name = h.User.Name,
						Username = h.User.Username,
						Email = h.User.Email,
						UserGroupID = h.UserRole.UserGroupID.Value,
						ImpersonatedByUserLoginHistoryID = h.ImpersonatedByUserLoginHistoryID,
						UserGroupPermissions = aspireContext.UserGroupPermissions.Where(p => p.UserGroupID == h.UserRole.UserGroupID.Value).ToList(),
					}).Single(),
				};
			}
		}

		public static List<UserGroupMenuLink> GetUserGroupMenuLinks(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandAdminActiveSession(loginSessionGuid);
				return aspireContext.UserGroupMenuLinks.Where(g => g.UserGroupID == loginHistory.UserGroupID).ToList();
			}
		}
	}
}
