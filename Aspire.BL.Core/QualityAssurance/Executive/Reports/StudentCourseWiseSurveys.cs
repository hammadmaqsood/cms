﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.QualityAssurance.Executive.Reports
{
	public static class StudentCourseWiseSurveys
	{
		private static Core.Common.Permissions.Executive.ExecutiveGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandExecutivePermissions(loginSessionGuid, ExecutivePermissions.QualityAssurance.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class OfferedCourseStudentCourseWiseSurvey
		{
			public int OfferedCourseID { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public string DepartmentName { get; internal set; }
			public string Title { get; internal set; }
			public int? FacultyMemberID { get; internal set; }
			public string Name { get; internal set; }
			internal string ProgramAlias { get; set; }
			internal int Section { get; set; }
			internal short SemesterNo { get; set; }
			internal byte Shift { get; set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			internal List<RegisteredCourse> RegisteredCourses { get; set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			private List<RegisteredCourse> OverAll => this.RegisteredCourses;
			private List<RegisteredCourse> LessThen2 => this.RegisteredCourses.Where(rc => rc.CGPA <= 2).ToList();
			private List<RegisteredCourse> LessThen3 => this.RegisteredCourses.Where(rc => 2 < rc.CGPA && rc.CGPA <= 3).ToList();
			private List<RegisteredCourse> LessThen4 => this.RegisteredCourses.Where(rc => 3 < rc.CGPA && rc.CGPA <= 4).ToList();

			public double? OverAllPercentage
			{
				get
				{
					if (this.OverAll.Any())
						return this.OverAll.Average(s => s.Percentage);
					return null;
				}
			}

			public double? LessThen2Percentage
			{
				get
				{
					if (this.LessThen2.Any())
						return this.LessThen2.Average(s => s.Percentage);
					return null;
				}
			}

			public double? LessThen3Percentage
			{
				get
				{
					if (this.LessThen3.Any())
						return this.LessThen3.Average(s => s.Percentage);
					return null;
				}
			}

			public double? LessThen4Percentage
			{
				get
				{
					if (this.LessThen4.Any())
						return this.LessThen4.Average(s => s.Percentage);
					return null;
				}
			}

			internal sealed class RegisteredCourse
			{
				public decimal? CGPA { get; internal set; }
				public int RegisteredCourseID { get; internal set; }
				public List<double> Weightages { get; internal set; }

				public double? Percentage
				{
					get
					{
						if (this.Weightages.Any())
							return this.Weightages.Average();
						return null;
					}
				}
			}

			public List<OfferedCourseStudentCourseWiseSurvey> GetList()
			{
				return null;
			}
		}

		public sealed class PageHeader
		{
			public string ReportTitle => $"Department-wise {this.SurveyName} [{this.SemesterID.ToSemesterString()}] [{((SurveyConduct.SurveyConductTypes)this.SurveyConductType).ToFullName()}]";
			public string InstituteName { get; internal set; }
			public string SurveyName { get; internal set; }
			public short SemesterID { get; internal set; }
			public short SurveyConductType { get; internal set; }
			public List<PageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public List<OfferedCourseStudentCourseWiseSurvey> Survey { get; internal set; }
			public PageHeader PageHeader { get; internal set; }
		}

		public static ReportDataSet StudentCourseWiseSurveySummary(int? departmentID, int surveyConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.Database.CommandTimeout = 180;
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var pageHeader = aspireContext.SurveyConducts.Where(sc => sc.SurveyConductID == surveyConductID && sc.Survey.InstituteID == loginHistory.InstituteID).Select(sc => new PageHeader
				{
					SemesterID = sc.SemesterID,
					SurveyName = sc.Survey.SurveyName,
					InstituteName = sc.Survey.Institute.InstituteName,
					SurveyConductType = sc.SurveyConductType
				}).SingleOrDefault();
				if (pageHeader == null)
					return null;

				var departmentIDs = aspireContext.UserRoleModules
					.Where(urm => urm.UserRoleID == loginHistory.UserRoleID)
					.Where(urm => urm.Module == loginHistory.PermissionType.Module)
					.Select(urm => urm.DepartmentID).ToList();

				var offeredCourses = aspireContext.OfferedCourses.Where(oc => oc.SemesterID == pageHeader.SemesterID && oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(oc => oc.RegisteredCourses.Any(rc => rc.DeletedDate == null));

				if (departmentIDs.Any(d => d == null))
				{
				}
				else
				{
					offeredCourses = offeredCourses.Where(oc => departmentIDs.Contains(oc.Cours.AdmissionOpenProgram.Program.DepartmentID) || departmentIDs.Contains(oc.FacultyMember.DepartmentID));
				}

				if (departmentID != null)
					offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID || oc.FacultyMember.DepartmentID == departmentID);
				var survey = offeredCourses.Select(oc => new OfferedCourseStudentCourseWiseSurvey
				{
					OfferedCourseID = oc.OfferedCourseID,
					Name = oc.FacultyMember.Name,
					FacultyMemberID = oc.FacultyMemberID,
					DepartmentName = oc.FacultyMember.Department.DepartmentName,
					DepartmentID = oc.FacultyMember.DepartmentID,
					Title = oc.Cours.Title,
					ProgramAlias = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = oc.SemesterNo,
					Section = oc.Section,
					Shift = oc.Shift,
					SemesterID = oc.SemesterID,
					RegisteredCourses = oc.RegisteredCourses.Where(rc => rc.DeletedDate == null).Select(rc => new OfferedCourseStudentCourseWiseSurvey.RegisteredCourse
					{
						RegisteredCourseID = rc.RegisteredCourseID,
						CGPA = rc.Student.StudentSemesters.Where(ss => ss.SemesterID <= pageHeader.SemesterID && ss.CGPA != null).OrderByDescending(ss => ss.SemesterID).Select(ss => ss.CGPA).FirstOrDefault(),
						Weightages = rc.SurveyUsers.Where(su => su.SurveyConductID == surveyConductID)
							.SelectMany(su => su.SurveyUserAnswers)
							.Where(sua => sua.SurveyQuestion.QuestionType == SurveyQuestion.QuestionTypeOptions)
							.Where(sua => sua.SurveyQuestionOption.Weightage != null)
							.Select(sua => sua.SurveyQuestionOption.Weightage.Value).ToList(),
					}).ToList(),
				}).ToList();
				return new ReportDataSet
				{
					PageHeader = pageHeader,
					Survey = survey
				};
			}
		}
	}
}
