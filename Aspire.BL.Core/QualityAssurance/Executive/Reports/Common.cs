﻿using Aspire.BL.Entities.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.QualityAssurance.Executive.Reports
{
	public static class Common
	{
		public static List<CustomListItem> GetSurveyConducts(int instituteID, short semesterID, Model.Entities.Survey.SurveyTypes surveyTypeEnum, SurveyConduct.SurveyConductTypes surveyConductTypeEnum)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.SurveyConducts
					.Where(sc => sc.SemesterID == semesterID)
					.Where(sc => sc.SurveyConductType == (byte)surveyConductTypeEnum)
					.Where(sc => sc.Survey.SurveyType == (int)surveyTypeEnum)
					.Where(sc => sc.Survey.InstituteID == instituteID)
					.Select(sc => new CustomListItem
					{
						ID = sc.SurveyConductID,
						Text = sc.Survey.SurveyName
					}).OrderByDescending(sc => sc.Text).ToList();
			}
		}

		public static List<CustomListItem> GetSurveys(int instituteID, Model.Entities.Survey.SurveyTypes surveyTypeEnum)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.Surveys
					.Where(s => s.SurveyType == (int)surveyTypeEnum)
					.Where(s => s.InstituteID == instituteID)
					.Select(s => new CustomListItem
					{
						ID = s.SurveyID,
						Text = s.SurveyName
					}).OrderByDescending(sc => sc.Text).ToList();
			}
		}
	}
}
