﻿using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using System.Web.UI.WebControls;
using Aspire.BL.Core.Exams.Staff;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;


namespace Aspire.BL.Core.QualityAssurance.Executive.Reports
{
    public static class FacultyEvaluationSummary
    {
        private static Core.Common.Permissions.Executive.ExecutiveGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
        {
            return aspireContext.DemandExecutivePermissions(loginSessionGuid, ExecutivePermissions.QualityAssurance.Module, UserGroupPermission.PermissionValues.Allowed);
        }

        public sealed class QAFacultyEvaluationSummary
        {
            public string HodRemarks { get; internal set; }
            public string DgRemarks { get; internal set; }
            public string DirectorRemarks { get; internal set; }
            public int OfferedCourseID { get; internal set; }
            public int? DepartmentID { get; internal set; }
            public int? RemarksID { get; internal set; }
            public string DepartmentName { get; internal set; }
            public string Title { get; internal set; }
            public int? FacultyMemberID { get; internal set; }
            public int FacultyType { get; internal set; }
            public FacultyMember.FacultyTypes FacutlyTypeEnum => (FacultyMember.FacultyTypes)this.FacultyType;
            public string Name { get; internal set; }
            internal string ProgramAlias { get; set; }
            internal int Section { get; set; }
            internal short SemesterNo { get; set; }
            internal byte Shift { get; set; }
            public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
            internal List<RegisteredCourse> RegisteredCourses { get; set; }
            public short SemesterID { get; internal set; }
            private List<RegisteredCourse> OverAll => this.RegisteredCourses;
            public double? OverAllAvgBeforeMid { get; internal set; }
            public double? OverAllAvgBeforeFinal { get; internal set; }

            public double? BeforeMidsAvg
            {
                get
                {
                    if (this.OverAll.Any())
                        return this.OverAll.Average(s => s.Percentage);
                    return null;
                }
            }

            public double? BeforeFinalAvg
            {
                get
                {
                    if (this.OverAll.Any())
                        return this.OverAll.Average(s => s.Percentage2);
                    return null;
                }
            }

            internal sealed class RegisteredCourse
            {
                public decimal? CGPA { get; internal set; }
                public int RegisteredCourseID { get; internal set; }
                public List<double> Weightages { get; internal set; }
                public List<double> Weightages2 { get; internal set; }

                public double? Percentage
                {
                    get
                    {
                        if (this.Weightages.Any())
                            return this.Weightages.Average();
                        return null;
                    }
                }

                public double? Percentage2
                {
                    get
                    {
                        if (this.Weightages2.Any())
                            return this.Weightages2.Average();
                        return null;
                    }
                }
            }

            public double? OverAllAverage2 { get; set; }
        }

        public sealed class QAPageHeader
        {
            public string InstituteName { get; internal set; }
            public string SurveyName { get; internal set; }
            public short SemesterID { get; internal set; }
            public short SurveyConductType { get; internal set; }
        }

        public sealed class ReportDataSet
        {
            internal ReportDataSet()
            {
            }

            public List<QAFacultyEvaluationSummary> QAEvaluationSummary { get; internal set; }
            public QAPageHeader QAPageHeader { get; internal set; }
        }

        public static int? GetSurveyConducts(int instituteID, short semesterID, Model.Entities.Survey.SurveyTypes surveyTypeEnum, SurveyConduct.SurveyConductTypes surveyConductTypeEnum)
        {
            using (var aspireContext = new AspireContext())
            {
                var results = aspireContext.SurveyConducts
                    .Where(sc => sc.SemesterID == semesterID)
                    .Where(sc => sc.SurveyConductType == (byte)surveyConductTypeEnum)
                    .Where(sc => sc.Survey.SurveyType == (int)surveyTypeEnum)
                    .Where(sc => sc.Survey.InstituteID == instituteID)
                    .Where(sc => sc.Survey.SurveyName == "Teacher Evaluation Form (Survey Form-10)")
                    .Select(sc => new
                    {
                        ID = sc.SurveyConductID
                    })
                    .SingleOrDefault();
                return results?.ID;
            }
        }

        public enum SaveRemarksStatuses
        {
            AddSuccess,
            UpdateSuccess,
            AlreadyLocked,
            InvalidRole
        }

        public static SaveRemarksStatuses SaveRemarks(int surveyID, int facultyID, int deptID, double midsAvg, double finalAvg, string role, string remarks, Guid loginSessionGuid)
        {
            using (var aspireContext = new AspireContext(true))
            {
                var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
                var qaSummaryFacultyEvaluation = aspireContext.QASummaryFacultyEvaluations.SingleOrDefault(sr => sr.SurveyConductID == surveyID && sr.FacultyMemberID == facultyID);
                if (qaSummaryFacultyEvaluation == null)
                {
                   
                    QASummaryFacultyEvaluation qaSummaryFacultyEvaluationsForAdd = new QASummaryFacultyEvaluation
                    {
                        SurveyConductID = surveyID,
                        FacultyMemberID = facultyID,
                        DepartmentID = deptID,
                        MidAverage = (decimal)midsAvg,
                        FinalFinalAverage = (decimal)finalAvg,
                        TotalAverage = (decimal)(midsAvg + finalAvg) / 2,
                        CompilationDate = DateTime.Now,
                    };

                    if (role == "HOD")
                    {
                        qaSummaryFacultyEvaluationsForAdd.HODRemarks = remarks;
                        qaSummaryFacultyEvaluationsForAdd.HODRemarksDate = DateTime.Now;
                        qaSummaryFacultyEvaluationsForAdd.HodRemarksStatusesEnum = QASummaryFacultyEvaluation.HodRemarksStatuses.Save;
                        qaSummaryFacultyEvaluationsForAdd.HODRemarksUserLoginHistoryID = loginHistory.UserLoginHistoryID;
                    }
                    if (role == "Director")
                    {
                        qaSummaryFacultyEvaluationsForAdd.DirectorRemarks = remarks;
                        qaSummaryFacultyEvaluationsForAdd.DirectorRemarksDate = DateTime.Now;
                        qaSummaryFacultyEvaluationsForAdd.DirectorRemarksStatusesEnum = QASummaryFacultyEvaluation.DirectorRemarksStatuses.Save;
                        qaSummaryFacultyEvaluationsForAdd.DirectorRemarksUserLoginHistoryID = loginHistory.UserLoginHistoryID;
                    }
                    if (role == "DG")
                    {
                        qaSummaryFacultyEvaluationsForAdd.DGRemarks = remarks;
                        qaSummaryFacultyEvaluationsForAdd.DGRemarksDate = DateTime.Now;
                        qaSummaryFacultyEvaluationsForAdd.DgRemarksStatusesEnum = QASummaryFacultyEvaluation.DgRemarksStatuses.Save;
                        qaSummaryFacultyEvaluationsForAdd.DGRemarksUserLoginHistoryID = loginHistory.UserLoginHistoryID;
                    }
                    aspireContext.QASummaryFacultyEvaluations.Add(qaSummaryFacultyEvaluationsForAdd);
                    aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
                    return SaveRemarksStatuses.AddSuccess;
                }
                else
                {


                    if (role == "HOD")
                    {
                        if (qaSummaryFacultyEvaluation.HODRemarks == null && qaSummaryFacultyEvaluation.HODRemarksStatus == null)
                        {
                            
                            qaSummaryFacultyEvaluation.HODRemarks = remarks;
                            qaSummaryFacultyEvaluation.HodRemarksStatusesEnum = QASummaryFacultyEvaluation.HodRemarksStatuses.Save;
                            qaSummaryFacultyEvaluation.HODRemarksDate = DateTime.Now;
                            qaSummaryFacultyEvaluation.HODRemarksUserLoginHistoryID = loginHistory.UserLoginHistoryID;
                            aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
                            return SaveRemarksStatuses.AddSuccess;
                        }
                        if (qaSummaryFacultyEvaluation.HodRemarksStatusesEnum == QASummaryFacultyEvaluation.HodRemarksStatuses.Save)
                        {
                          
                            qaSummaryFacultyEvaluation.HODRemarks = remarks;
                            qaSummaryFacultyEvaluation.HodRemarksStatusesEnum = QASummaryFacultyEvaluation.HodRemarksStatuses.Save;
                            qaSummaryFacultyEvaluation.HODRemarksDate = DateTime.Now;
                            qaSummaryFacultyEvaluation.HODRemarksUserLoginHistoryID = loginHistory.UserLoginHistoryID;
                            aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
                            return SaveRemarksStatuses.UpdateSuccess;
                        }
                        if (qaSummaryFacultyEvaluation.HodRemarksStatusesEnum == QASummaryFacultyEvaluation.HodRemarksStatuses.Lock)
                            return SaveRemarksStatuses.AlreadyLocked;
                    }
                    if (role == "Director")
                    {
                        if (qaSummaryFacultyEvaluation.DirectorRemarks == null && qaSummaryFacultyEvaluation.DirectorRemarksStatus == null)
                        {
                            
                            qaSummaryFacultyEvaluation.DirectorRemarks = remarks;
                            qaSummaryFacultyEvaluation.DirectorRemarksStatusesEnum = QASummaryFacultyEvaluation.DirectorRemarksStatuses.Save;
                            qaSummaryFacultyEvaluation.DirectorRemarksDate = DateTime.Now;
                            qaSummaryFacultyEvaluation.DirectorRemarksUserLoginHistoryID = loginHistory.UserLoginHistoryID;
                            aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
                            return SaveRemarksStatuses.AddSuccess;
                        }
                        if (qaSummaryFacultyEvaluation.DirectorRemarksStatusesEnum == QASummaryFacultyEvaluation.DirectorRemarksStatuses.Save)
                        {
                           
                            qaSummaryFacultyEvaluation.DirectorRemarks = remarks;
                            qaSummaryFacultyEvaluation.DirectorRemarksStatusesEnum = QASummaryFacultyEvaluation.DirectorRemarksStatuses.Save;
                            qaSummaryFacultyEvaluation.DirectorRemarksDate = DateTime.Now;
                            qaSummaryFacultyEvaluation.DirectorRemarksUserLoginHistoryID = loginHistory.UserLoginHistoryID;
                            aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
                            return SaveRemarksStatuses.UpdateSuccess;
                        }
                        if (qaSummaryFacultyEvaluation.DirectorRemarksStatusesEnum == QASummaryFacultyEvaluation.DirectorRemarksStatuses.Lock)
                            return SaveRemarksStatuses.AlreadyLocked;

                    }

                    if (role == "DG")
                    {
                        if (qaSummaryFacultyEvaluation.DGRemarksStatus == null && qaSummaryFacultyEvaluation.DGRemarks == null)
                        {
                            
                            qaSummaryFacultyEvaluation.DGRemarks = remarks;
                            qaSummaryFacultyEvaluation.DgRemarksStatusesEnum = QASummaryFacultyEvaluation.DgRemarksStatuses.Save;
                            qaSummaryFacultyEvaluation.DGRemarksDate = DateTime.Now;
                            qaSummaryFacultyEvaluation.DGRemarksUserLoginHistoryID = loginHistory.UserLoginHistoryID;
                            aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
                            return SaveRemarksStatuses.AddSuccess;
                        }
                        if (qaSummaryFacultyEvaluation.DgRemarksStatusesEnum == QASummaryFacultyEvaluation.DgRemarksStatuses.Save)
                        {
                           
                            qaSummaryFacultyEvaluation.DGRemarks = remarks;
                            qaSummaryFacultyEvaluation.DgRemarksStatusesEnum = QASummaryFacultyEvaluation.DgRemarksStatuses.Save;
                            qaSummaryFacultyEvaluation.DGRemarksDate = DateTime.Now;
                            qaSummaryFacultyEvaluation.DGRemarksUserLoginHistoryID = loginHistory.UserLoginHistoryID;
                            aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
                            return SaveRemarksStatuses.UpdateSuccess;
                        }
                        if (qaSummaryFacultyEvaluation.DgRemarksStatusesEnum == QASummaryFacultyEvaluation.DgRemarksStatuses.Lock)
                            return SaveRemarksStatuses.AlreadyLocked;
                    }
                }
                return SaveRemarksStatuses.InvalidRole;
            }

        }

        public static SaveRemarksStatuses LockRemarks(int qaSummaryFacultyEvaluationID, string role, string remarks, Guid loginSessionGuid)
        {
            using (var aspireContext = new AspireContext(true))
            {
                var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
                var qaSummaryFacultyEvaluations = aspireContext.QASummaryFacultyEvaluations.SingleOrDefault(sr => sr.QASummaryFacultyEvaluationID == qaSummaryFacultyEvaluationID);
                if (qaSummaryFacultyEvaluations != null)
                {
                    if (role == "HOD")
                    {
                        qaSummaryFacultyEvaluations.HODRemarks = remarks;
                        qaSummaryFacultyEvaluations.HodRemarksStatusesEnum = QASummaryFacultyEvaluation.HodRemarksStatuses.Lock;
                        qaSummaryFacultyEvaluations.HODRemarksDate = DateTime.Now;
                        qaSummaryFacultyEvaluations.HODRemarksUserLoginHistoryID = loginHistory.UserLoginHistoryID;
                        aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
                        return SaveRemarksStatuses.UpdateSuccess;
                    }
                    if (role == "Director")
                    {
                        
                        qaSummaryFacultyEvaluations.DirectorRemarks = remarks;
                        qaSummaryFacultyEvaluations.DirectorRemarksStatusesEnum = QASummaryFacultyEvaluation.DirectorRemarksStatuses.Lock;
                        qaSummaryFacultyEvaluations.DirectorRemarksDate = DateTime.Now;
                        qaSummaryFacultyEvaluations.DirectorRemarksUserLoginHistoryID = loginHistory.UserLoginHistoryID;
                        aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
                        return SaveRemarksStatuses.UpdateSuccess;
                    }
                    if (role == "DG")
                    {
                       
                        qaSummaryFacultyEvaluations.DGRemarks = remarks;
                        qaSummaryFacultyEvaluations.DgRemarksStatusesEnum = QASummaryFacultyEvaluation.DgRemarksStatuses.Lock;
                        qaSummaryFacultyEvaluations.DGRemarksDate = DateTime.Now;
                        qaSummaryFacultyEvaluations.DGRemarksUserLoginHistoryID = loginHistory.UserLoginHistoryID;
                        aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
                        return SaveRemarksStatuses.UpdateSuccess;
                    }
                    return SaveRemarksStatuses.InvalidRole;
                }
            }
            return SaveRemarksStatuses.InvalidRole;
        }
        public static List<QAFacultyEvaluationSummary> EvaluationSummary(int? departmentID, int surveyConductID1, int surveyConductID2, FacultyMember.FacultyTypes fmType, string role, int pageIndex, int pageSize,  string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
        {
            using (var aspireContext = new AspireContext())
            {
                aspireContext.Database.CommandTimeout = 180;
                var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
                var pageHeader = aspireContext.SurveyConducts.Where(sc => sc.SurveyConductID == surveyConductID1 && sc.Survey.InstituteID == loginHistory.InstituteID)
                    .Select(sc => new QAPageHeader
                    {
                        SemesterID = sc.SemesterID,
                        SurveyName = sc.Survey.SurveyName,
                        InstituteName = sc.Survey.Institute.InstituteName,
                        SurveyConductType = sc.SurveyConductType
                    })
                    .SingleOrDefault();
                var pageHeader2 = aspireContext.SurveyConducts.Where(sc => sc.SurveyConductID == surveyConductID2 && sc.Survey.InstituteID == loginHistory.InstituteID)
                   .Select(sc => new QAPageHeader
                   {
                       SemesterID = sc.SemesterID,
                       SurveyName = sc.Survey.SurveyName,
                       InstituteName = sc.Survey.Institute.InstituteName,
                       SurveyConductType = sc.SurveyConductType
                   })
                   .SingleOrDefault();
                var departmentIDs = aspireContext.UserRoleModules
                    .Where(urm => urm.UserRoleID == loginHistory.UserRoleID)
                    .Where(urm => urm.Module == loginHistory.PermissionType.Module)
                    .Select(urm => urm.DepartmentID)
                    .ToList();

                var offeredCourses = aspireContext.OfferedCourses.Where(oc => oc.SemesterID == pageHeader.SemesterID && oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
                if (departmentIDs.Any(d => d == null))
                {
                }
                else
                {
                    offeredCourses = offeredCourses.Where(oc => departmentIDs.Contains(oc.Cours.AdmissionOpenProgram.Program.DepartmentID));
                }
                if (surveyConductID1 == 0 && surveyConductID2 == 0)
                {
                    virtualItemCount = 0;
                    return null;
                }
                if (departmentID != null)
                    offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID);
                var survey = offeredCourses.Select(oc => new QAFacultyEvaluationSummary
                {
                    OfferedCourseID = oc.OfferedCourseID,
                    Name = oc.FacultyMember.Name,
                    FacultyMemberID = oc.FacultyMemberID,
                    FacultyType = oc.FacultyMember.FacultyType,
                    DepartmentName = oc.FacultyMember.Department.DepartmentName,
                    DepartmentID = oc.FacultyMember.DepartmentID,
                    Title = oc.Cours.Title,
                    ProgramAlias = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
                    SemesterNo = oc.SemesterNo,
                    Section = oc.Section,
                    Shift = oc.Shift,
                    SemesterID = oc.SemesterID,
                    RegisteredCourses = oc.RegisteredCourses.Where(rc => rc.DeletedDate == null).Select(rc => new QAFacultyEvaluationSummary.RegisteredCourse
                    {
                        RegisteredCourseID = rc.RegisteredCourseID,
                        CGPA = rc.Student.StudentSemesters.Where(ss => ss.SemesterID <= pageHeader.SemesterID && ss.CGPA != null).OrderByDescending(ss => ss.SemesterID).Select(ss => ss.CGPA).FirstOrDefault(),
                        Weightages = rc.SurveyUsers.Where(su => su.SurveyConductID == surveyConductID1)
                            .SelectMany(su => su.SurveyUserAnswers)
                            .Where(sua => sua.SurveyQuestion.QuestionType == SurveyQuestion.QuestionTypeOptions)
                            .Where(sua => sua.SurveyQuestionOption.Weightage != null)
                            .Select(sua => sua.SurveyQuestionOption.Weightage.Value).ToList(),
                        Weightages2 = rc.SurveyUsers.Where(su => su.SurveyConductID == surveyConductID2)
                            .SelectMany(su => su.SurveyUserAnswers)
                            .Where(sua => sua.SurveyQuestion.QuestionType == SurveyQuestion.QuestionTypeOptions)
                            .Where(sua => sua.SurveyQuestionOption.Weightage != null)
                            .Select(sua => sua.SurveyQuestionOption.Weightage.Value).ToList(),
                    }).ToList(),

                    RemarksID = aspireContext.QASummaryFacultyEvaluations.DefaultIfEmpty().Where(a => a.FacultyMemberID == oc.FacultyMemberID && a.SurveyConductID == surveyConductID2).Select(a => a.QASummaryFacultyEvaluationID).FirstOrDefault(),
                    HodRemarks = (role == "HOD" ? aspireContext.QASummaryFacultyEvaluations.DefaultIfEmpty().Where(a => a.FacultyMemberID == oc.FacultyMemberID && a.SurveyConductID == surveyConductID2).Select(a => a.HODRemarks).FirstOrDefault() : aspireContext.QASummaryFacultyEvaluations.DefaultIfEmpty().Where(a => a.FacultyMemberID == oc.FacultyMemberID && a.SurveyConductID == surveyConductID2 && a.HODRemarksStatus == 1).Select(a => a.HODRemarks).FirstOrDefault()),
                    DirectorRemarks = (role == "director" ? aspireContext.QASummaryFacultyEvaluations.DefaultIfEmpty().Where(a => a.FacultyMemberID == oc.FacultyMemberID && a.SurveyConductID == surveyConductID2).Select(a => a.DirectorRemarks).FirstOrDefault() : aspireContext.QASummaryFacultyEvaluations.DefaultIfEmpty().Where(a => a.FacultyMemberID == oc.FacultyMemberID && a.SurveyConductID == surveyConductID2 && a.DirectorRemarksStatus == 1).Select(a => a.DirectorRemarks).FirstOrDefault()),
                    DgRemarks = (role == "DG" ? aspireContext.QASummaryFacultyEvaluations.DefaultIfEmpty().Where(a => a.FacultyMemberID == oc.FacultyMemberID && a.SurveyConductID == surveyConductID2).Select(a => a.DGRemarks).FirstOrDefault() : aspireContext.QASummaryFacultyEvaluations.DefaultIfEmpty().Where(a => a.FacultyMemberID == oc.FacultyMemberID && a.SurveyConductID == surveyConductID2 && a.DGRemarksStatus == 1).Select(a => a.DGRemarks).FirstOrDefault()),
                }).Where(oc => oc.Name != null && oc.FacultyType == (byte)fmType).ToList();

                virtualItemCount = survey.Count();
                var survey2 = survey.Select(a => a.FacultyMemberID).Distinct();
                if (role.ToLower() == "director")
                    survey.ForEach(a => a.DgRemarks = "");
                else if (role.ToLower() == "hod")
                {
                    survey.ForEach(a => a.DgRemarks = "");
                    survey.ForEach(a => a.DirectorRemarks = "");
                }
                foreach (var item in survey2)
                {
                    var varer = survey.Where(s => s.FacultyMemberID == item).Select(a => a.BeforeMidsAvg).Average();
                    survey.Where(s => s.FacultyMemberID == item).ForEach(s => s.OverAllAvgBeforeMid = varer);
                }
                foreach (var item in survey2)
                {
                    var varer = survey.Where(s => s.FacultyMemberID == item).Select(a => a.BeforeFinalAvg).Average();
                    survey.Where(s => s.FacultyMemberID == item).ForEach(s => s.OverAllAvgBeforeFinal = varer);
                }
                foreach (var item in survey2)
                {
                    var varer = survey.Where(s => s.FacultyMemberID == item).Select(a => a.BeforeMidsAvg).Average();
                    var varer2 = survey.Where(s => s.FacultyMemberID == item).Select(a => a.BeforeFinalAvg).Average();
                    survey.Where(s => s.FacultyMemberID == item).ForEach(s => s.OverAllAverage2 = (varer + varer2) / 2);
                }

                switch (sortExpression)
                {
                    case nameof(QAFacultyEvaluationSummary.Name):
                        survey = (sortDirection == SortDirection.Ascending ? survey.OrderBy(s => s.Name).ToList() : survey.OrderByDescending(s => s.Name).ToList());
                        break;
                    case nameof(QAFacultyEvaluationSummary.Title):
                        survey = (sortDirection == SortDirection.Ascending ? survey.OrderBy(s => s.Title).ToList() : survey.OrderByDescending(s => s.Title).ToList());
                        break;
                    case nameof(QAFacultyEvaluationSummary.Class):
                        survey = (sortDirection == SortDirection.Ascending ? survey.OrderBy(s => s.Class).ToList() : survey.OrderByDescending(s => s.Class).ToList());
                        break;
                    case nameof(QAFacultyEvaluationSummary.BeforeMidsAvg):
                        survey = (sortDirection == SortDirection.Ascending ? survey.OrderBy(p => p.BeforeMidsAvg).ToList() : survey.OrderByDescending(p => p.BeforeMidsAvg).ToList());
                        break;
                    case nameof(QAFacultyEvaluationSummary.OverAllAvgBeforeMid):
                        survey = (sortDirection == SortDirection.Ascending ? survey.OrderBy(p => p.OverAllAvgBeforeMid).ToList() : survey.OrderByDescending(p => p.OverAllAvgBeforeMid).ToList());
                        break;
                    case nameof(QAFacultyEvaluationSummary.BeforeFinalAvg):
                        survey = (sortDirection == SortDirection.Ascending ? survey.OrderBy(p => p.BeforeFinalAvg).ToList() : survey.OrderByDescending(p => p.BeforeFinalAvg).ToList());
                        break;
                    case nameof(QAFacultyEvaluationSummary.OverAllAvgBeforeFinal):
                        survey = (sortDirection == SortDirection.Ascending ? survey.OrderBy(p => p.OverAllAvgBeforeFinal).ToList() : survey.OrderByDescending(p => p.OverAllAvgBeforeFinal).ToList());
                        break;
                }
                survey = survey.Skip(pageIndex * pageSize).Take(pageSize).ToList();

                return survey;


            }
        }

        public static ReportDataSet EvaluationSummaryReport(int? departmentID, int surveyConductID1, int surveyConductID2, string role, Guid loginSessionGuid)
        {
            using (var aspireContext = new AspireContext())
            {
                aspireContext.Database.CommandTimeout = 180;
                var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
                var pageHeader = aspireContext.SurveyConducts.Where(sc => sc.SurveyConductID == surveyConductID1 && sc.Survey.InstituteID == loginHistory.InstituteID)
                    .Select(sc => new QAPageHeader
                    {
                        SemesterID = sc.SemesterID,
                        SurveyName = sc.Survey.SurveyName,
                        InstituteName = sc.Survey.Institute.InstituteName,
                        SurveyConductType = sc.SurveyConductType
                    })
                    .SingleOrDefault();
                var pageHeader2 = aspireContext.SurveyConducts.Where(sc => sc.SurveyConductID == surveyConductID2 && sc.Survey.InstituteID == loginHistory.InstituteID)
                    .Select(sc => new QAPageHeader
                    {
                        SemesterID = sc.SemesterID,
                        SurveyName = sc.Survey.SurveyName,
                        InstituteName = sc.Survey.Institute.InstituteName,
                        SurveyConductType = sc.SurveyConductType
                    })
                    .SingleOrDefault();
                var departmentIDs = aspireContext.UserRoleModules
                    .Where(urm => urm.UserRoleID == loginHistory.UserRoleID)
                    .Where(urm => urm.Module == loginHistory.PermissionType.Module)
                    .Select(urm => urm.DepartmentID)
                    .ToList();
                var offeredCourses = aspireContext.OfferedCourses.Where(oc => oc.SemesterID == pageHeader.SemesterID && oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
                if (departmentIDs.Any(d => d == null))
                {
                }
                else
                {
                    offeredCourses = offeredCourses.Where(oc => departmentIDs.Contains(oc.Cours.AdmissionOpenProgram.Program.DepartmentID));
                }
                if (surveyConductID1 == 0 && surveyConductID2 == 0)
                {
                    return null;
                }
                if (departmentID != null)
                    offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID);
                var survey = offeredCourses.Select(oc => new QAFacultyEvaluationSummary
                {
                    OfferedCourseID = oc.OfferedCourseID,
                    Name = oc.FacultyMember.Name,
                    FacultyMemberID = oc.FacultyMemberID,
                    DepartmentName = oc.FacultyMember.Department.DepartmentName,
                    DepartmentID = oc.FacultyMember.DepartmentID,
                    Title = oc.Cours.Title,
                    ProgramAlias = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
                    SemesterNo = oc.SemesterNo,
                    Section = oc.Section,
                    Shift = oc.Shift,
                    SemesterID = oc.SemesterID,
                    RegisteredCourses = oc.RegisteredCourses.Where(rc => rc.DeletedDate == null).Select(rc => new QAFacultyEvaluationSummary.RegisteredCourse
                    {
                        RegisteredCourseID = rc.RegisteredCourseID,
                        CGPA = rc.Student.StudentSemesters.Where(ss => ss.SemesterID <= pageHeader.SemesterID && ss.CGPA != null).OrderByDescending(ss => ss.SemesterID).Select(ss => ss.CGPA).FirstOrDefault(),
                        Weightages = rc.SurveyUsers.Where(su => su.SurveyConductID == surveyConductID1)
                            .SelectMany(su => su.SurveyUserAnswers)
                            .Where(sua => sua.SurveyQuestion.QuestionType == SurveyQuestion.QuestionTypeOptions)
                            .Where(sua => sua.SurveyQuestionOption.Weightage != null)
                            .Select(sua => sua.SurveyQuestionOption.Weightage.Value).ToList(),
                        Weightages2 = rc.SurveyUsers.Where(su => su.SurveyConductID == surveyConductID2)
                            .SelectMany(su => su.SurveyUserAnswers)
                            .Where(sua => sua.SurveyQuestion.QuestionType == SurveyQuestion.QuestionTypeOptions)
                            .Where(sua => sua.SurveyQuestionOption.Weightage != null)
                            .Select(sua => sua.SurveyQuestionOption.Weightage.Value).ToList(),
                    }).ToList(),
                    HodRemarks = aspireContext.QASummaryFacultyEvaluations.DefaultIfEmpty().Where(a => a.FacultyMemberID == oc.FacultyMemberID && a.SurveyConductID == surveyConductID2 && a.HODRemarksStatus == QASummaryFacultyEvaluation.HodRemarksStatusesLock).Select(a => a.HODRemarks).FirstOrDefault(),
                    DirectorRemarks = aspireContext.QASummaryFacultyEvaluations.DefaultIfEmpty().Where(a => a.FacultyMemberID == oc.FacultyMemberID && a.SurveyConductID == surveyConductID2 && a.DirectorRemarksStatus == QASummaryFacultyEvaluation.DirectorRemarksStatusesLock).Select(a => a.DirectorRemarks).FirstOrDefault(),
                    DgRemarks = aspireContext.QASummaryFacultyEvaluations.DefaultIfEmpty().Where(a => a.FacultyMemberID == oc.FacultyMemberID && a.SurveyConductID == surveyConductID2 && a.DGRemarksStatus == QASummaryFacultyEvaluation.DgRemarksStatusesLock).Select(a => a.DGRemarks).FirstOrDefault()
                }).Where(oc => oc.Name != null).ToList();
                var survey2 = survey.Select(a => a.FacultyMemberID).Distinct();
                foreach (var item in survey2)
                {
                    var varer = survey.Where(s => s.FacultyMemberID == item).Select(a => a.BeforeMidsAvg).Average();
                    survey.Where(s => s.FacultyMemberID == item).ForEach(s => s.OverAllAvgBeforeMid = varer);
                }
                foreach (var item in survey2)
                {
                    var varer = survey.Where(s => s.FacultyMemberID == item).Select(a => a.BeforeFinalAvg).Average();
                    survey.Where(s => s.FacultyMemberID == item).ForEach(s => s.OverAllAvgBeforeFinal = varer);
                }
                foreach (var item in survey2)
                {
                    var varer = survey.Where(s => s.FacultyMemberID == item).Select(a => a.BeforeMidsAvg).Average();
                    var varer2 = survey.Where(s => s.FacultyMemberID == item).Select(a => a.BeforeFinalAvg).Average();
                    survey.Where(s => s.FacultyMemberID == item).ForEach(s => s.OverAllAverage2 = (varer + varer2) / 2);
                }
                return new ReportDataSet
                {
                    QAPageHeader = pageHeader,
                    QAEvaluationSummary = survey
                };

            }
        }
    }
}
