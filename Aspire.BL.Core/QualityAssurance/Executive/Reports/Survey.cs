﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.QualityAssurance.Executive.Reports
{
	public static class Survey
	{
		private static Core.Common.Permissions.Executive.ExecutiveGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandExecutivePermissions(loginSessionGuid, ExecutivePermissions.QualityAssurance.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		internal sealed class QuestionOption
		{
			public string OptionText { get; }
			public int TurnOut { get; }
			public int Count { get; }
			public double? Weightage { get; }
			public string OptionTextWithWeightage => this.Weightage != null ? $"{this.OptionText}\n({this.Weightage.Value:0.##}%)" : this.OptionText;
			public double? Marks
			{
				get
				{
					if (this.Weightage == null)
						return null;
					if (this.TurnOut == 0)
						return 0;
					return this.Count / (double)this.TurnOut * this.Weightage.Value;
				}
			}

			public int SurveyQuestionOptionID { get; }
			public string OptionValue => $"{this.Count}";

			public QuestionOption(SurveyQuestionOption surveyQuestionOption, List<SurveyUserAnswer> answers, int turnout)
			{
				this.SurveyQuestionOptionID = surveyQuestionOption.SurveyQuestionOptionID;
				this.OptionText = surveyQuestionOption.OptionText;
				this.Weightage = surveyQuestionOption.Weightage;
				this.TurnOut = turnout;
				this.Count = answers.Count(a => a.SurveyQuestionOptionID == surveyQuestionOption.SurveyQuestionOptionID);
			}
		}

		internal sealed class Question
		{
			public Dictionary<string, int> OptionTextList { get; }
			public byte DisplayIndex { get; }
			public string QuestionNo { get; }
			public SurveyQuestion.QuestionTypes QuestionTypeEnum { get; }
			public string QuestionText { get; }
			public int Turnout { get; }
			public double? TotalMarks { get; }
			public List<QuestionOption> QuestionOptions { get; }
			public int SurveyQuestionID { get; }
			public string OptionTextListString => string.Join("\n", this.OptionTextList.Select(t => $"{t.Value} time(s): {t.Key}"));

			public Question(SurveyQuestion surveyQuestion, List<SurveyUserAnswer> surveyUserAnswers)
			{
				this.QuestionNo = surveyQuestion.QuestionNo;
				this.SurveyQuestionID = surveyQuestion.SurveyQuestionID;
				this.QuestionText = surveyQuestion.QuestionText;
				this.DisplayIndex = surveyQuestion.DisplayIndex;
				this.QuestionTypeEnum = surveyQuestion.QuestionTypeEnum;
				var answers = surveyUserAnswers.Where(sua => sua.SurveyQuestionID == surveyQuestion.SurveyQuestionID).ToList();
				this.Turnout = answers.Select(a => a.SurveyUserID).Distinct().Count();
				switch (surveyQuestion.QuestionTypeEnum)
				{
					case SurveyQuestion.QuestionTypes.Options:
						this.QuestionOptions = surveyQuestion.SurveyQuestionOptions.Select(sqo => new QuestionOption(sqo, answers, this.Turnout)).ToList();
						this.TotalMarks = this.QuestionOptions.Sum(o => o.Marks);
						break;
					case SurveyQuestion.QuestionTypes.Text:
						this.OptionTextList = answers.Select(sua => sua.OptionText).Where(t => !string.IsNullOrWhiteSpace(t))
							.GroupBy(t => t).ToDictionary(t => t.Key, t => t.Count());
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		internal sealed class QuestionGroup
		{
			public int DisplayIndex { get; }
			public string QuestionGroupName { get; }
			public List<Question> Questions { get; }
			public int SurveyQuestionGroupID { get; }

			public QuestionGroup(Model.Entities.SurveyQuestionGroup surveyQuestionGroup, List<SurveyUserAnswer> surveyUserAnswers)
			{
				this.SurveyQuestionGroupID = surveyQuestionGroup.SurveyQuestionGroupID;
				this.DisplayIndex = surveyQuestionGroup.DisplayIndex;
				this.QuestionGroupName = surveyQuestionGroup.QuestionGroupName;
				this.Questions = surveyQuestionGroup.SurveyQuestions.Select(sq => new Question(sq, surveyUserAnswers)).OrderBy(q => q.DisplayIndex).ToList();
			}
		}

		internal sealed class SurveySummary
		{
			public string Description { get; }
			public List<QuestionGroup> QuestionGroups { get; }
			public string SurveyName { get; }
			public string InstituteName { get; }
			public double? GrandTotalPercentage => this.QuestionGroups.SelectMany(g => g.Questions).Where(q => q.TotalMarks != null).Average(q => q.TotalMarks);

			public SurveySummary(Model.Entities.Survey survey, List<SurveyUserAnswer> surveyUserAnswers)
			{
				this.SurveyName = survey.SurveyName;
				this.Description = survey.Description;
				this.InstituteName = survey.Institute.InstituteName;
				this.QuestionGroups = survey.SurveyQuestionGroups.Select(sqg => new QuestionGroup(sqg, surveyUserAnswers)).OrderBy(g => g.DisplayIndex).ToList();
			}

			internal IEnumerable<Summary> ToSummary()
			{
				List<string> currentRunningOptions = null;
				var groupNo = 0;

				var getGroupNo = new Func<List<string>, int>(delegate (List<string> optionsList)
				 {
					 if (optionsList != null && currentRunningOptions?.SequenceEqual(optionsList) == true)
						 return groupNo;
					 currentRunningOptions = optionsList;
					 return ++groupNo;
				 });

				foreach (var questionGroup in this.QuestionGroups)
				{
					foreach (var question in questionGroup.Questions)
					{
						switch (question.QuestionTypeEnum)
						{
							case SurveyQuestion.QuestionTypes.Options:
								var optionsList = question.QuestionOptions.Select(qo => qo.OptionTextWithWeightage).ToList();
								foreach (var option in question.QuestionOptions)
									yield return new Summary
									{
										SurveyName = this.SurveyName,
										Description = this.Description,
										GrandTotalPercentage = this.GrandTotalPercentage,
										SurveyQuestionGroupID = questionGroup.SurveyQuestionGroupID,
										QuestionGroupNameDisplayIndex = questionGroup.DisplayIndex,
										QuestionGroupName = questionGroup.QuestionGroupName,
										QuestionDisplayIndex = question.DisplayIndex,
										SurveyQuestionID = question.SurveyQuestionID,
										QuestionNo = question.QuestionNo,
										QuestionText = question.QuestionText,
										IsOptions = question.QuestionTypeEnum == SurveyQuestion.QuestionTypes.Options,
										IsText = question.QuestionTypeEnum == SurveyQuestion.QuestionTypes.Text,
										TotalMarks = question.TotalMarks,
										SurveyQuestionOptionID = option.SurveyQuestionOptionID,
										Turnout = question.Turnout,
										OptionText = option.OptionText,
										OptionTextWithWeightage = option.OptionTextWithWeightage,
										OptionValue = option.OptionValue,
										Marks = option.Marks,
										Count = option.Count,
										Weightage = option.Weightage,
										GroupNo = getGroupNo(optionsList),
									};
								break;
							case SurveyQuestion.QuestionTypes.Text:
								yield return new Summary
								{
									SurveyName = this.SurveyName,
									Description = this.Description,
									GrandTotalPercentage = this.GrandTotalPercentage,
									SurveyQuestionGroupID = questionGroup.SurveyQuestionGroupID,
									QuestionGroupNameDisplayIndex = questionGroup.DisplayIndex,
									QuestionGroupName = questionGroup.QuestionGroupName,
									QuestionDisplayIndex = question.DisplayIndex,
									SurveyQuestionID = question.SurveyQuestionID,
									QuestionNo = question.QuestionNo,
									QuestionText = question.QuestionText,
									IsOptions = question.QuestionTypeEnum == SurveyQuestion.QuestionTypes.Options,
									IsText = question.QuestionTypeEnum == SurveyQuestion.QuestionTypes.Text,
									TotalMarks = question.TotalMarks,
									Turnout = question.Turnout,
									GroupNo = getGroupNo(null),
									OptionTextListString = question.OptionTextListString,
								};
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}
					}
				}
			}
		}

		public sealed class Summary
		{
			public int Count { get; internal set; }
			public string Description { get; internal set; }
			public int GroupNo { get; internal set; }
			public bool IsOptions { get; internal set; }
			public bool IsText { get; internal set; }
			public double? Marks { get; internal set; }
			public string OptionText { get; internal set; }
			public string OptionTextListString { get; internal set; }
			public string OptionTextWithWeightage { get; internal set; }
			public string OptionValue { get; internal set; }
			public byte QuestionDisplayIndex { get; internal set; }
			public string QuestionGroupName { get; internal set; }
			public int QuestionGroupNameDisplayIndex { get; internal set; }
			public string QuestionNo { get; internal set; }
			public string QuestionText { get; internal set; }
			public string SurveyName { get; internal set; }
			public int SurveyQuestionGroupID { get; internal set; }
			public int SurveyQuestionID { get; internal set; }
			public int SurveyQuestionOptionID { get; internal set; }
			public double? TotalMarks { get; internal set; }
			public double? GrandTotalPercentage { get; internal set; }
			public int Turnout { get; internal set; }
			public double? Weightage { get; internal set; }

			public List<Summary> GetList()
			{
				return null;
			}
		}

		public sealed class PageHeader
		{
			public int? DepartmentID { get; internal set; }
			public string DepartmentName { get; internal set; }
			public int? FacultyMemberID { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public int InstituteID { get; internal set; }
			public string InstituteName { get; internal set; }
			public int? OfferedCourseID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public int? ProgramID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public SemesterNos? SemesterNo { get; internal set; }
			public string SemesterNoFullName => this.SemesterNo?.ToFullName() ?? "All";
			public Sections? Section { get; internal set; }
			public string SectionFullName => this.Section?.ToFullName() ?? "All";
			public Shifts? Shift { get; internal set; }
			public string ShiftFullName => this.Shift?.ToFullName() ?? "All";
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public int SurveyID { get; internal set; }
			public string SurveyName { get; internal set; }
			public string Title { get; internal set; }

			public List<PageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			public List<Summary> Summary { get; }
			public PageHeader PageHeader { get; }

			public ReportDataSet(Model.Entities.Survey survey, List<SurveyUserAnswer> surveyUserAnswers, short semesterID, int? departmentID, string departmentName, int? programID, string programAlias, SemesterNos? semesterNo, Sections? section, Shifts? shift, int? facultyMemberID, string facultyMemberName, int? offeredCourseID, string title)
			{
				var surveySummary = new SurveySummary(survey, surveyUserAnswers);
				this.Summary = surveySummary.ToSummary().ToList();

				this.PageHeader = new PageHeader
				{
					SemesterID = semesterID,
					SurveyID = survey.SurveyID,
					SurveyName = survey.SurveyName,
					InstituteID = survey.InstituteID,
					InstituteName = survey.Institute.InstituteName,
					DepartmentID = departmentID,
					DepartmentName = departmentName,
					ProgramID = programID,
					ProgramAlias = programAlias,
					SemesterNo = semesterNo,
					Section = section,
					Shift = shift,
					FacultyMemberID = facultyMemberID,
					FacultyMemberName = facultyMemberName,
					OfferedCourseID = offeredCourseID,
					Title = title,
				};
			}
		}

		public static ReportDataSet GetIndividualSurveySummary(int surveyConductID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int? studentID, int? registeredCourseID, int? offeredCourseID, int? facultyMemberID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.Configuration.AutoDetectChangesEnabled = false;
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var surveyConduct = aspireContext.SurveyConducts.Where(sc => sc.SurveyConductID == surveyConductID).Select(sc => new
				{
					sc.SemesterID,
				}).SingleOrDefault();
				if (surveyConduct == null)
					return null;
				var survey = aspireContext.SurveyConducts.Where(sc => sc.SurveyConductID == surveyConductID)
					.Select(sc => sc.Survey)
					.Include(s => s.SurveyQuestionGroups.Select(g => g.SurveyQuestions.Select(q => q.SurveyQuestionOptions)))
					.Include(s => s.Institute)
					.SingleOrDefault(s => s.InstituteID == loginHistory.InstituteID);
				if (survey == null)
					return null;
				var departmentIDs = aspireContext.UserRoleModules
					.Where(urm => urm.UserRoleID == loginHistory.UserRoleID)
					.Where(urm => urm.Module == loginHistory.PermissionType.Module)
					.Select(urm => urm.DepartmentID).ToList();

				IQueryable<SurveyUser> surveyUsers;
				switch (survey.SurveyTypeEnum)
				{
					case Model.Entities.Survey.SurveyTypes.StudentWise:
						var studentSemesters = aspireContext.StudentSemesters.Where(ss => ss.SemesterID == surveyConduct.SemesterID);
						if (semesterNoEnum != null)
							studentSemesters = studentSemesters.Where(ss => ss.SemesterNo == (short)semesterNoEnum.Value);
						if (sectionEnum != null)
							studentSemesters = studentSemesters.Where(ss => ss.Section == (int)sectionEnum.Value);
						if (shiftEnum != null)
							studentSemesters = studentSemesters.Where(ss => ss.Shift == (byte)shiftEnum.Value);
						var students = studentSemesters.Select(ss => ss.Student).Where(s => s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
						if (!departmentIDs.Contains(null))
							students = students.Where(s => departmentIDs.Contains(s.AdmissionOpenProgram.Program.DepartmentID));
						if (departmentID != null)
							students = students.Where(s => s.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
						if (programID != null)
							students = students.Where(s => s.AdmissionOpenProgram.ProgramID == programID.Value);
						if (studentID != null)
							students = students.Where(s => s.StudentID == studentID.Value);
						if (registeredCourseID != null)
							throw new ArgumentException(nameof(registeredCourseID));
						if (offeredCourseID != null)
							throw new ArgumentException(nameof(offeredCourseID));
						if (facultyMemberID != null)
							throw new ArgumentException(nameof(facultyMemberID));
						surveyUsers = students.SelectMany(s => s.SurveyUsers);
						break;
					case Model.Entities.Survey.SurveyTypes.StudentCourseWise:
						var offeredCourses = aspireContext.OfferedCourses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
							.Where(oc => oc.SemesterID == surveyConduct.SemesterID)
							.Where(oc => oc.RegisteredCourses.Any(rc => rc.DeletedDate == null));
						if (!departmentIDs.Contains(null))
							offeredCourses = offeredCourses.Where(oc => departmentIDs.Contains(oc.Cours.AdmissionOpenProgram.Program.DepartmentID)
							|| (oc.FacultyMember.DepartmentID != null && departmentIDs.Contains(oc.FacultyMember.DepartmentID.Value)));
						if (departmentID != null)
							offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
						if (programID != null)
							offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.ProgramID == programID.Value);
						if (semesterNoEnum != null)
							offeredCourses = offeredCourses.Where(oc => oc.SemesterNo == (short)semesterNoEnum.Value);
						if (sectionEnum != null)
							offeredCourses = offeredCourses.Where(oc => oc.Section == (int)sectionEnum.Value);
						if (shiftEnum != null)
							offeredCourses = offeredCourses.Where(oc => oc.Shift == (byte)shiftEnum.Value);
						if (offeredCourseID != null)
							offeredCourses = offeredCourses.Where(oc => oc.OfferedCourseID == offeredCourseID.Value);
						if (facultyMemberID != null)
							offeredCourses = offeredCourses.Where(oc => oc.FacultyMemberID == facultyMemberID.Value);
						var registeredCourses = offeredCourses.SelectMany(oc => oc.RegisteredCourses).Where(rc => rc.DeletedDate == null);
						if (studentID != null)
							registeredCourses = registeredCourses.Where(rc => rc.StudentID == studentID.Value);
						if (registeredCourseID != null)
							registeredCourses = registeredCourses.Where(rc => rc.RegisteredCourseID == registeredCourseID.Value);
						surveyUsers = registeredCourses.SelectMany(rc => rc.SurveyUsers);
						break;
					case Model.Entities.Survey.SurveyTypes.FacultyMemberWise:
						var facultyMembers = aspireContext.FacultyMembers.Where(fm => fm.InstituteID == loginHistory.InstituteID);
						if (!departmentIDs.Contains(null))
							facultyMembers = facultyMembers.Where(fm => departmentIDs.Contains(fm.DepartmentID));
						if (departmentID != null)
							facultyMembers = facultyMembers.Where(fm => fm.DepartmentID == departmentID.Value);
						if (programID != null)
							throw new ArgumentException(nameof(programID));
						if (semesterNoEnum != null)
							throw new ArgumentException(nameof(semesterNoEnum));
						if (sectionEnum != null)
							throw new ArgumentException(nameof(sectionEnum));
						if (shiftEnum != null)
							throw new ArgumentException(nameof(shiftEnum));
						if (studentID != null)
							throw new ArgumentException(nameof(studentID));
						if (registeredCourseID != null)
							throw new ArgumentException(nameof(registeredCourseID));
						if (offeredCourseID != null)
							throw new ArgumentException(nameof(offeredCourseID));
						if (facultyMemberID != null)
							facultyMembers = facultyMembers.Where(fm => fm.FacultyMemberID == facultyMemberID.Value);
						surveyUsers = facultyMembers.SelectMany(fm => fm.SurveyUsers);
						break;
					case Model.Entities.Survey.SurveyTypes.FacultyMemberCourseWise:
						offeredCourses = aspireContext.OfferedCourses
							.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
							.Where(oc => oc.RegisteredCourses.Any(rc => rc.DeletedDate == null))
							.Where(oc => oc.SemesterID == surveyConduct.SemesterID);
						if (!departmentIDs.Contains(null))
							offeredCourses = offeredCourses.Where(oc => departmentIDs.Contains(oc.Cours.AdmissionOpenProgram.Program.DepartmentID)
								|| (oc.FacultyMember.DepartmentID != null && departmentIDs.Contains(oc.FacultyMember.DepartmentID.Value)));
						if (departmentID != null)
							offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
						if (programID != null)
							offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.ProgramID == programID.Value);
						if (semesterNoEnum != null)
							offeredCourses = offeredCourses.Where(oc => oc.SemesterNo == (short)semesterNoEnum.Value);
						if (sectionEnum != null)
							offeredCourses = offeredCourses.Where(oc => oc.Section == (int)sectionEnum.Value);
						if (shiftEnum != null)
							offeredCourses = offeredCourses.Where(oc => oc.Shift == (byte)shiftEnum.Value);
						if (studentID != null)
							throw new ArgumentException(nameof(studentID));
						if (registeredCourseID != null)
							throw new ArgumentException(nameof(registeredCourseID));
						if (offeredCourseID != null)
							offeredCourses = offeredCourses.Where(oc => oc.OfferedCourseID == offeredCourseID.Value);
						if (facultyMemberID != null)
							offeredCourses = offeredCourses.Where(oc => oc.FacultyMemberID == facultyMemberID.Value);
						surveyUsers = offeredCourses.SelectMany(oc => oc.SurveyUsers);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				if (surveyUsers == null)
					return null;

				var surveyUserAnswers = surveyUsers.Where(su => su.SurveyConductID == surveyConductID).SelectMany(su => su.SurveyUserAnswers).AsNoTracking().ToList();
				var _semesterID = surveyConduct.SemesterID;
				int? _departmentID = null;
				var _departmentName = "All";
				int? _programID = null;
				var _programAlias = "All";
				SemesterNos? _semesterNo = null;
				Sections? _section = null;
				Shifts? _shift = null;
				int? _facultyMemberID = null;
				var _facultyMemberName = "All";
				int? _offeredCourseID = null;
				string _title = null;

				_departmentID = departmentID;
				_programID = programID;
				_semesterNo = semesterNoEnum;
				_section = sectionEnum;
				_shift = shiftEnum;
				_facultyMemberID = null;
				_offeredCourseID = null;
				if (offeredCourseID != null)
				{
					var offeredCourse = aspireContext.OfferedCourses.Where(oc => oc.OfferedCourseID == offeredCourseID.Value).Select(oc => new
					{
						oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
						oc.Cours.AdmissionOpenProgram.Program.Department.DepartmentName,
						oc.Cours.AdmissionOpenProgram.ProgramID,
						oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						oc.SemesterNo,
						oc.Section,
						oc.Shift,
						oc.FacultyMemberID,
						oc.FacultyMember.Name,
						oc.Cours.Title,
					}).Single();
					_departmentID = offeredCourse.DepartmentID;
					_departmentName = offeredCourse.DepartmentName;
					_programID = offeredCourse.ProgramID;
					_programAlias = offeredCourse.ProgramAlias;
					_semesterNo = (SemesterNos?)offeredCourse.SemesterNo;
					_section = (Sections?)offeredCourse.Section;
					_shift = (Shifts?)offeredCourse.Shift;
					_facultyMemberID = offeredCourse.FacultyMemberID;
					_facultyMemberName = offeredCourse.Name;
					_offeredCourseID = offeredCourseID.Value;
					_title = offeredCourse.Title;
				}
				else
				{
					if (_departmentID != null)
						_departmentName = aspireContext.Departments.Where(d => d.DepartmentID == _departmentID.Value).Select(d => d.DepartmentName).Single();
					if (_programID != null)
						_programAlias = aspireContext.Programs.Where(p => p.ProgramID == _programID.Value).Select(p => p.ProgramAlias).Single();
					if (_facultyMemberID != null)
						_facultyMemberName = aspireContext.FacultyMembers.Where(fm => fm.FacultyMemberID == _facultyMemberID.Value).Select(fm => fm.Name).Single();
				}

				return new ReportDataSet(survey, surveyUserAnswers, _semesterID, _departmentID, _departmentName, _programID, _programAlias, _semesterNo, _section, _shift, _facultyMemberID, _facultyMemberName, _offeredCourseID, _title);
			}
		}
	}
}
