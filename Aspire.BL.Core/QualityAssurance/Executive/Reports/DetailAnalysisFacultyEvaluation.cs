﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.QualityAssurance.Executive.Reports
{
	public static class DetailAnalysisFacultyEvaluation
	{
		private static Core.Common.Permissions.Executive.ExecutiveGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandExecutivePermissions(loginSessionGuid, ExecutivePermissions.QualityAssurance.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class OfferedCourseStudentCourseWiseSurvey
		{
			public int OfferedCourseID { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public string DepartmentName { get; internal set; }
			public string Title { get; internal set; }
			public int? FacultyMemberID { get; internal set; }
			public string Name { get; internal set; }
			internal string ProgramAlias { get; set; }
			internal int Section { get; set; }
			internal short SemesterNo { get; set; }
			internal byte Shift { get; set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			internal List<RegisteredCourse> RegisteredCourses { get; set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			private List<RegisteredCourse> OverAll => this.RegisteredCourses;
			private List<RegisteredCourse> LessThen3Point5 => this.RegisteredCourses.Where(rc => 3M < rc.CGPA && rc.CGPA <= 3.5M).ToList();
			private List<RegisteredCourse> LessThen4 => this.RegisteredCourses.Where(rc => 3.5M < rc.CGPA && rc.CGPA <= 4M).ToList();
			public int Students => this.RegisteredCourses.Count;
			public int AGrades => this.RegisteredCourses.Count(rc => rc.GradeEnum == ExamGrades.A);
			public int AMinusGrades => this.RegisteredCourses.Count(rc => rc.GradeEnum == ExamGrades.AMinus);
			public int BPlusGrades => this.RegisteredCourses.Count(rc => rc.GradeEnum == ExamGrades.BPlus);
			public int BGrades => this.RegisteredCourses.Count(rc => rc.GradeEnum == ExamGrades.B);
			public int BMinusGrades => this.RegisteredCourses.Count(rc => rc.GradeEnum == ExamGrades.BMinus);
			public int CPlusGrades => this.RegisteredCourses.Count(rc => rc.GradeEnum == ExamGrades.CPlus);
			public int CGrades => this.RegisteredCourses.Count(rc => rc.GradeEnum == ExamGrades.C);
			public int CMinusGrades => this.RegisteredCourses.Count(rc => rc.GradeEnum == ExamGrades.CMinus);
			public int DPlusGrades => this.RegisteredCourses.Count(rc => rc.GradeEnum == ExamGrades.DPlus);
			public int DGrades => this.RegisteredCourses.Count(rc => rc.GradeEnum == ExamGrades.D);
			public int FGrades => this.RegisteredCourses.Count(rc => rc.GradeEnum == ExamGrades.F);
			public string Comments
			{
				get
				{
					var comments = this.RegisteredCourses.SelectMany(rc => rc.Comments).SelectMany(c => c.Value);
					return string.Join("\n", comments.GroupBy(s => s).Select(s => $"{s.Count()} times: {s.Key}"));
				}
			}

			private Dictionary<SurveyConduct.SurveyConductTypes, double> OverAllPercentages => this.OverAll
				.SelectMany(rc => rc.Percentages)
				.GroupBy(x => x.Key)
				.ToDictionary(x => x.Key, x => x.Average(s => s.Value));

			public double OverAllBeforeMidPercentage
			{
				get
				{
					return this.OverAllPercentages.TryGetValue(SurveyConduct.SurveyConductTypes.BeforeMidTerm, out double value)
						? value
						: 0;
				}
			}

			public double OverAllBeforeFinalPercentage
			{
				get
				{
					return this.OverAllPercentages.TryGetValue(SurveyConduct.SurveyConductTypes.BeforeFinalTerm, out double value)
						? value
						: 0;
				}
			}

			public double OverAllAverage => (this.OverAllBeforeMidPercentage + this.OverAllBeforeFinalPercentage) / 2;

			private Dictionary<SurveyConduct.SurveyConductTypes, double> LessThen3Point5Percentages => this.LessThen3Point5
				.SelectMany(rc => rc.Percentages)
				.GroupBy(x => x.Key)
				.ToDictionary(x => x.Key, x => x.Average(s => s.Value));

			public double LessThen3Point5BeforeMidPercentage
			{
				get
				{
					return this.LessThen3Point5Percentages.TryGetValue(SurveyConduct.SurveyConductTypes.BeforeMidTerm, out double value)
						? value
						: 0;
				}
			}

			public double LessThen3Point5BeforeFinalPercentage
			{
				get
				{
					return this.LessThen3Point5Percentages.TryGetValue(SurveyConduct.SurveyConductTypes.BeforeFinalTerm, out double value)
						? value
						: 0;
				}
			}

			public double LessThen3Point5Average => (this.LessThen3Point5BeforeMidPercentage + this.LessThen3Point5BeforeFinalPercentage) / 2;

			private Dictionary<SurveyConduct.SurveyConductTypes, double> LessThen4Percentages => this.LessThen4
				.SelectMany(rc => rc.Percentages)
				.GroupBy(x => x.Key)
				.ToDictionary(x => x.Key, x => x.Average(s => s.Value));

			public double LessThen4BeforeMidPercentage
			{
				get
				{
					return this.LessThen4Percentages.TryGetValue(SurveyConduct.SurveyConductTypes.BeforeMidTerm, out double value)
						? value
						: 0;
				}
			}

			public double LessThen4BeforeFinalPercentage
			{
				get
				{
					return this.LessThen4Percentages.TryGetValue(SurveyConduct.SurveyConductTypes.BeforeFinalTerm, out double value)
						? value
						: 0;
				}
			}

			public double LessThen4Average => (this.LessThen4BeforeMidPercentage + this.LessThen4BeforeFinalPercentage) / 2;

			internal sealed class SurveyUserAnswer
			{
				public byte SurveyConductType { get; internal set; }
				public SurveyConduct.SurveyConductTypes SurveyConductTypeEnum => (SurveyConduct.SurveyConductTypes)this.SurveyConductType;
				public byte QuestionType { get; internal set; }
				public SurveyQuestion.QuestionTypes QuestionTypeEnum => (SurveyQuestion.QuestionTypes)this.QuestionType;
				public double? Weightage { get; internal set; }
				public string OptionText { get; internal set; }
			}

			internal sealed class RegisteredCourse
			{
				public int RegisteredCourseID { get; internal set; }
				public byte? Grade { get; internal set; }
				public ExamGrades? GradeEnum => (ExamGrades?)this.Grade;
				public decimal? CGPA { get; internal set; }
				public List<SurveyUserAnswer> SurveyUserAnswers { get; internal set; }

				public Dictionary<SurveyConduct.SurveyConductTypes, double> Percentages
				{
					get
					{
						if (this.SurveyUserAnswers.Any())
							return this.SurveyUserAnswers
								.Where(sua => sua.QuestionTypeEnum == SurveyQuestion.QuestionTypes.Options)
								.GroupBy(sua => sua.SurveyConductTypeEnum)
								.ToDictionary(sua => sua.Key, sua => sua.Average(x => x.Weightage.Value));
						return null;
					}
				}

				public Dictionary<SurveyConduct.SurveyConductTypes, List<string>> Comments
				{
					get
					{
						if (this.SurveyUserAnswers.Any())
							return this.SurveyUserAnswers
								.Where(sua => sua.QuestionTypeEnum == SurveyQuestion.QuestionTypes.Text)
								.GroupBy(sua => sua.SurveyConductTypeEnum)
								.ToDictionary(sua => sua.Key, sua => sua.Select(x => x.OptionText).Where(s => !string.IsNullOrWhiteSpace(s)).ToList());
						return null;
					}
				}
			}

			public List<OfferedCourseStudentCourseWiseSurvey> GetOfferedCourseStudentCourseWiseSurveyList()
			{
				return null;
			}
		}

		public sealed class PageHeader
		{
			public string ReportTitle => $"Detail Analysis of Faculty Members [{this.SemesterID.ToSemesterString()}]\nFaculty Evaluation Score < {70}";
			public string InstituteName { get; internal set; }
			public short SemesterID { get; internal set; }
			public int SurveyID { get; internal set; }
			public string SurveyName { get; internal set; }

			public List<PageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public List<OfferedCourseStudentCourseWiseSurvey> Survey { get; internal set; }
			public PageHeader PageHeader { get; internal set; }
		}

		public static ReportDataSet GetDetailedAnalysis(int surveyID, short semesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int? facultyMemberID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.IncreaseCommandTimeout();
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var pageHeader = aspireContext.SurveyConducts
					.Where(sc => sc.SurveyID == surveyID && sc.SemesterID == semesterID && sc.Survey.InstituteID == loginHistory.InstituteID)
					.Select(sc => new PageHeader
					{
						SemesterID = sc.SemesterID,
						SurveyID = sc.SurveyID,
						SurveyName = sc.Survey.SurveyName,
						InstituteName = sc.Survey.Institute.InstituteName,
					}).Distinct().SingleOrDefault();
				if (pageHeader == null)
					return null;

				var departmentIDs = aspireContext.UserRoleModules
					.Where(urm => urm.UserRoleID == loginHistory.UserRoleID)
					.Where(urm => urm.Module == loginHistory.PermissionType.Module)
					.Select(urm => urm.DepartmentID).ToList();

				var offeredCourses = aspireContext.OfferedCourses
					.Where(oc => oc.SemesterID == pageHeader.SemesterID && oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(oc => oc.RegisteredCourses.Any(rc => rc.DeletedDate == null));
				if (departmentIDs.Any(d => d == null))
				{
				}
				else
				{
					offeredCourses = offeredCourses.Where(oc => departmentIDs.Contains(oc.Cours.AdmissionOpenProgram.Program.DepartmentID) || departmentIDs.Contains(oc.FacultyMember.DepartmentID));
				}

				if (departmentID != null)
					offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
				if (programID != null)
					offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.ProgramID == programID.Value);
				if (semesterNoEnum != null)
					offeredCourses = offeredCourses.Where(oc => oc.SemesterNo == (short)semesterNoEnum);
				if (sectionEnum != null)
					offeredCourses = offeredCourses.Where(oc => oc.Section == (int)sectionEnum.Value);
				if (shiftEnum != null)
					offeredCourses = offeredCourses.Where(oc => oc.Shift == (byte)shiftEnum.Value);
				if (facultyMemberID != null)
					offeredCourses = offeredCourses.Where(oc => oc.FacultyMemberID == facultyMemberID.Value);
				var survey = offeredCourses.Select(oc => new OfferedCourseStudentCourseWiseSurvey
				{
					OfferedCourseID = oc.OfferedCourseID,
					Name = oc.FacultyMember.Name,
					FacultyMemberID = oc.FacultyMemberID,
					DepartmentName = oc.FacultyMember.Department.DepartmentName,
					DepartmentID = oc.FacultyMember.DepartmentID,
					Title = oc.Cours.Title,
					ProgramAlias = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = oc.SemesterNo,
					Section = oc.Section,
					Shift = oc.Shift,
					SemesterID = oc.SemesterID,
					RegisteredCourses = oc.RegisteredCourses
						.Where(rc => rc.DeletedDate == null)
						.Select(rc => new OfferedCourseStudentCourseWiseSurvey.RegisteredCourse
						{
							RegisteredCourseID = rc.RegisteredCourseID,
							Grade = rc.Grade,
							CGPA = rc.Student.StudentSemesters
								.Where(ss => ss.SemesterID <= pageHeader.SemesterID && ss.CGPA != null)
								.OrderByDescending(ss => ss.SemesterID)
								.Select(ss => ss.CGPA).FirstOrDefault(),
							SurveyUserAnswers = rc.SurveyUsers
								.Where(su => su.SurveyConduct.SurveyID == surveyID)
								.SelectMany(su => su.SurveyUserAnswers)
								.Select(sua => new OfferedCourseStudentCourseWiseSurvey.SurveyUserAnswer
								{
									QuestionType = sua.SurveyQuestion.QuestionType,
									Weightage = sua.SurveyQuestionOption.Weightage,
									OptionText = sua.OptionText,
									SurveyConductType = sua.SurveyUser.SurveyConduct.SurveyConductType
								}).ToList(),
						}).ToList(),
				}).ToList();
				return new ReportDataSet
				{
					PageHeader = pageHeader,
					Survey = survey
				};
			}
		}
	}
}
