﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.QualityAssurance.Staff.Reports
{
	public static class CourseWise
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class PageHeader
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Title => $"Teacher Evaluation Student CGPA Range ({this.SemesterID.ToSemesterString()})";
			public static List<PageHeader> GetPageHeaders()
			{
				return null;
			}
		}

		public sealed class OfferedCourse
		{
			public int OfferedCourseID { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public string DepartmentName { get; internal set; }
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public byte CreditHours { get; internal set; }
			public string Program { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public CreditHours CreditHoursEnum => (CreditHours)this.CreditHours;
			public string Class => AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift);
			public string CourseCodeTitle => $"{this.CourseCode}-{this.Title}";

			public static List<OfferedCourse> GetOfferedCourse()
			{
				return null;
			}
		}
		public class ReportDataSet
		{
			internal ReportDataSet() { }
			public PageHeader PageHeader { get; internal set; }
			public List<OfferedCourse> OfferedCourses { get; internal set; }
		}
		
		public static ReportDataSet GetOfferedCourses(int instituteID, short semesterID, int? departmentID, int? programID, short? semesterNo, int? section, byte? shift, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var offeredCourses = aspireContext.OfferedCourses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == instituteID);

				offeredCourses = offeredCourses.Where(oc => oc.SemesterID == semesterID);
				if (departmentID != null)
					offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID);
				if (programID != null)
					offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.ProgramID == programID);
				if (semesterNo != null)
					offeredCourses = offeredCourses.Where(oc => oc.SemesterNo == semesterNo);
				if (section != null)
					offeredCourses = offeredCourses.Where(oc => oc.Section == section);
				if (shift != null)
					offeredCourses = offeredCourses.Where(oc => oc.Shift == shift);

				var result = offeredCourses.Select(oc => new OfferedCourse
				{
					OfferedCourseID = oc.OfferedCourseID,
					FacultyMemberName = oc.FacultyMember.Name,
					DepartmentName = oc.Cours.AdmissionOpenProgram.Program.Department.DepartmentName,
					CourseCode = oc.Cours.CourseCode,
					Title = oc.Cours.Title,
					CreditHours = oc.Cours.CreditHours,
					Program = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = oc.SemesterNo,
					Section = oc.Section,
					Shift = oc.Shift,
				}).ToList();


				return new ReportDataSet
				{
					PageHeader = new PageHeader
					{
						SemesterID = semesterID,
						Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single()
					},
					OfferedCourses = result
				};
			}
		}

		public static List<CustomListItem> FillUserAssociatedDepartments(int instituteID, AspireModules aspireMoudleEnum, Department.Statuses? statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var userRolesModules = aspireContext.UserRoleModules.Where(urm => urm.UserRole.InstituteID == loginHistory.InstituteID && urm.UserRoleID == loginHistory.UserRoleID && urm.UserRole.UserID == loginHistory.UserID && urm.Module == (byte)aspireMoudleEnum);
				if (!userRolesModules.Any())
					return null;
				var departments = aspireContext.Departments.Where(d => d.InstituteID == loginHistory.InstituteID).AsQueryable();
				if (statusEnum != null)
					departments = departments.Where(d => d.Status == (byte)statusEnum.Value);
				if (userRolesModules.Any(urm => urm.DepartmentID != null))
				{
					var departmentID = userRolesModules.Select(urm => urm.DepartmentID).SingleOrDefault();
					departments = departments.Where(d => d.DepartmentID == departmentID);

				}
				return departments.Select(d => new CustomListItem
				{
					ID = d.DepartmentID,
					Text = d.DepartmentName,
				}).OrderBy(p => p.Text).ToList();
			}
		}

		public static List<CustomListItem> FillUserAssociatedPrograms(int instituteID, int? departmentID, bool considerNullDepartmentIDasAll, AspireModules aspireMoudleEnum, Program.Statuses? statusEnum, Guid loginSessionGuid)
		{

			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				//	var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, instituteID, departmentID, null, null);
				var userRolesModules = aspireContext.UserRoleModules.Where(urm => urm.UserRole.InstituteID == loginHistory.InstituteID && urm.UserRoleID == loginHistory.UserRoleID && urm.UserRole.UserID == loginHistory.UserID && urm.Module == (byte)aspireMoudleEnum);
				var programs = aspireContext.Programs.Where(pr => pr.InstituteID == loginHistory.InstituteID).AsQueryable();
				if (statusEnum != null)
					programs = programs.Where(d => d.Status == (byte)statusEnum.Value);
				if (departmentID != null || considerNullDepartmentIDasAll == false)
					programs = programs.Where(pr => pr.DepartmentID == departmentID);
				if (userRolesModules.Any(urm => urm.ProgramID != null))
				{
					var programID = userRolesModules.Select(urm => urm.ProgramID).SingleOrDefault();
					programs = programs.Where(d => d.ProgramID == programID);

				}
				return programs.Select(pr => new CustomListItem
				{
					ID = pr.ProgramID,
					Text = pr.ProgramAlias
				}).OrderBy(p => p.Text).ToList();
			}
		}

		public static List<CustomListItem> FillUserAssociatedSemesters(int instituteID, AspireModules aspireMoudleEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var userRolesModules = aspireContext.UserRoleModules.Where(urm => urm.UserRole.InstituteID == loginHistory.InstituteID && urm.UserRoleID == loginHistory.UserRoleID && urm.UserRole.UserID == loginHistory.UserID && urm.Module == (byte)aspireMoudleEnum);
				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms.Where(pr => pr.Program.InstituteID == loginHistory.InstituteID).AsQueryable();
				if (userRolesModules.Any(urm => urm.AdmissionOpenProgramID != null))
				{
					var admissionOpenProgramID = userRolesModules.Select(urm => urm.AdmissionOpenProgramID).SingleOrDefault();
					admissionOpenPrograms = admissionOpenPrograms.Where(d => d.AdmissionOpenProgramID == admissionOpenProgramID);

				}
				return admissionOpenPrograms.Select(aop => new
				{
					aop.SemesterID,
				}).Distinct().ToList().Select(aop => new CustomListItem
				{
					ID = aop.SemesterID,
					Text = aop.SemesterID.ToSemesterString(),
				}).OrderByDescending(o => o.ID).ToList();
			}
		}

		public static List<CustomListItem> FillSurveys(int instituteID, Survey.SurveyTypes surveyTypeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var surveys = aspireContext.Surveys.Where(s => s.SurveyType == (byte)surveyTypeEnum && s.InstituteID == loginHistory.InstituteID).AsQueryable();
				return surveys.Select(s => new CustomListItem
				{
					ID = s.SurveyID,
					Text = s.SurveyName,
				}).OrderByDescending(o => o.ID).ToList();
			}
		}
	}
}