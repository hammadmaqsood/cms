using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.QualityAssurance.Admin
{
	public static class SurveyGroupQuestionOption
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.QualityAssurance.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.QualityAssurance.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.QualityAssurance.ManageSurveysGroupsQuestionsOptions, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.QualityAssurance.ManageSurveysGroupsQuestionsOptions, UserGroupPermission.PermissionValues.Allowed);
		}

		public static List<CustomListItem> GetSurveys(int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				return aspireContext.Surveys.Where(s => s.InstituteID == instituteID).Select(s => new CustomListItem
				{
					ID = s.SurveyID,
					Text = s.SurveyName,
				}).OrderBy(o => o.Text).ToList();
			}
		}

		public static Model.Entities.Survey GetSurveysGroupsQuestionsOptions(int instituteID, int surveyID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				var survey = aspireContext.Surveys.Include(s => s.SurveyQuestionGroups.Select(sqg => sqg.SurveyQuestions.Select(sq => sq.SurveyQuestionOptions))).SingleOrDefault(s => s.SurveyID == surveyID);
				return survey;
			}
		}

		#region Survey Question Groups 

		public enum AddSurveyQuestionGroupsStatuses
		{
			AlreadyExists,
			Success,
		}

		public static AddSurveyQuestionGroupsStatuses AddSurveyQuestionGroup(int surveyID, int instituteID, string questionGroupName, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				var surveyQuestionGroup = new SurveyQuestionGroup
				{
					SurveyID = surveyID,
					QuestionGroupName = questionGroupName,
					DisplayIndex = int.MaxValue,
				}.Validate();
				if (aspireContext.SurveyQuestionGroups.Any(sqg => sqg.QuestionGroupName == surveyQuestionGroup.QuestionGroupName && sqg.SurveyID==surveyID))
					return AddSurveyQuestionGroupsStatuses.AlreadyExists;

				aspireContext.SurveyQuestionGroups.Add(surveyQuestionGroup);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ReorderEntity(surveyID, null, ReorderSurveyTable.SurveyQuestionGroups, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return AddSurveyQuestionGroupsStatuses.Success;
			}
		}

		public enum UpdateSurveysQuestionGroupStatuses
		{
			NoRecordFound,
			AlreadyExists,
			Success,
		}

		public static UpdateSurveysQuestionGroupStatuses UpdateSurveyQuestionGroup(int surveyQuestionGroupID, int instituteID, string questionGroupName, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				var surveyQuestionGroup = aspireContext.SurveyQuestionGroups.SingleOrDefault(sqg => sqg.SurveyQuestionGroupID == surveyQuestionGroupID);
				if (surveyQuestionGroup == null)
					return UpdateSurveysQuestionGroupStatuses.NoRecordFound;
				surveyQuestionGroup.QuestionGroupName = questionGroupName;
				surveyQuestionGroup.Validate();
				if (aspireContext.SurveyQuestionGroups.Any(sqg => sqg.SurveyQuestionGroupID != surveyQuestionGroupID && sqg.QuestionGroupName == surveyQuestionGroup.QuestionGroupName))
					return UpdateSurveysQuestionGroupStatuses.AlreadyExists;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateSurveysQuestionGroupStatuses.Success;
			}
		}

		public enum DeleteSurveysQuestionGroupStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteSurveysQuestionGroupStatuses DeleteSurveyQuestionGroup(int surveyQuestionGroupID, int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				var surveyQuestionGroup = aspireContext.SurveyQuestionGroups.SingleOrDefault(sqg => sqg.SurveyQuestionGroupID == surveyQuestionGroupID);
				if (surveyQuestionGroup == null)
					return DeleteSurveysQuestionGroupStatuses.NoRecordFound;
				if (surveyQuestionGroup.SurveyQuestions.Any())
					return DeleteSurveysQuestionGroupStatuses.ChildRecordExists;
				var savedSurveyID = surveyQuestionGroup.SurveyID;
				aspireContext.SurveyQuestionGroups.Remove(surveyQuestionGroup);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ReorderEntity(savedSurveyID, null, ReorderSurveyTable.SurveyQuestionGroups, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return DeleteSurveysQuestionGroupStatuses.Success;
			}
		}

		public static SurveyQuestionGroup GetSurveyQuestionGroup(int surveyQuestionGroupID, int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				return aspireContext.SurveyQuestionGroups.SingleOrDefault(sqg => sqg.SurveyQuestionGroupID == surveyQuestionGroupID);
			}
		}

		#endregion

		#region Survey Questions

		public enum AddSurveyQuestionStatuses
		{
			AlreadyExists,
			Success,
		}

		public static AddSurveyQuestionStatuses AddSurveyQuestion(int instituteID, int surveyQuestionGroupID, string questionNo, string questionText, SurveyQuestion.QuestionTypes questionTypeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				var surveyQuestion = new SurveyQuestion
				{
					SurveyQuestionGroupID = surveyQuestionGroupID,
					QuestionNo = questionNo,
					QuestionText = questionText,
					QuestionTypeEnum = questionTypeEnum,
					DisplayIndex = byte.MaxValue,
				}.Validate();
				if (aspireContext.SurveyQuestions.Any(sq => sq.SurveyQuestionGroupID == surveyQuestion.SurveyQuestionGroupID && (sq.QuestionNo == surveyQuestion.QuestionNo || sq.QuestionText == surveyQuestion.QuestionText)))
					return AddSurveyQuestionStatuses.AlreadyExists;

				aspireContext.SurveyQuestions.Add(surveyQuestion);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ReorderEntity(null, surveyQuestionGroupID, ReorderSurveyTable.SurveyQuestions, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return AddSurveyQuestionStatuses.Success;
			}
		}

		public enum UpdateSurveyQuestionStatuses
		{
			NoRecordFound,
			AlreadyExists,
			Success,
		}

		public static UpdateSurveyQuestionStatuses UpdateSurveyQuestion(int instituteID, int surveyQuestionID, string questionNo, string questionText, SurveyQuestion.QuestionTypes questionTypeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				var surveyQuestion = aspireContext.SurveyQuestions.SingleOrDefault(sq => sq.SurveyQuestionID == surveyQuestionID);
				if (surveyQuestion == null)
					return UpdateSurveyQuestionStatuses.NoRecordFound;
				surveyQuestion.QuestionNo = questionNo;
				surveyQuestion.QuestionText = questionText;
				surveyQuestion.QuestionTypeEnum = questionTypeEnum;
				surveyQuestion.Validate();
				if (aspireContext.SurveyQuestions.Any(sq => sq.SurveyQuestionID != surveyQuestion.SurveyQuestionID && sq.SurveyQuestionGroupID == surveyQuestion.SurveyQuestionGroupID && (sq.QuestionNo == surveyQuestion.QuestionNo || sq.QuestionText == surveyQuestion.QuestionText)))
					return UpdateSurveyQuestionStatuses.AlreadyExists;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateSurveyQuestionStatuses.Success;
			}
		}

		public enum DeleteSurveysQuestionStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteSurveysQuestionStatuses DeleteSurveyQuestion(int surveyQuestionID, int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				var surveyQuestion = aspireContext.SurveyQuestions.SingleOrDefault(sq => sq.SurveyQuestionID == surveyQuestionID);
				if (surveyQuestion == null)
					return DeleteSurveysQuestionStatuses.NoRecordFound;
				if (surveyQuestion.SurveyQuestionOptions.Any())
					return DeleteSurveysQuestionStatuses.ChildRecordExists;
				if (surveyQuestion.SurveyUserAnswers.Any())
					return DeleteSurveysQuestionStatuses.ChildRecordExists;
				var savedSurveyQuestionGroupID = surveyQuestion.SurveyQuestionGroupID;
				aspireContext.SurveyQuestions.Remove(surveyQuestion);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ReorderEntity(null, savedSurveyQuestionGroupID, ReorderSurveyTable.SurveyQuestions, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return DeleteSurveysQuestionStatuses.Success;
			}
		}

		public static SurveyQuestion GetSurveyQuestion(int surveyQuestionID, int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				return aspireContext.SurveyQuestions.Include(sq => sq.SurveyQuestionGroup).SingleOrDefault(sq => sq.SurveyQuestionID == surveyQuestionID);
			}
		}

		#endregion

		#region Survey Question Options

		public enum AddSurveyOptionStatuses
		{
			AlreadyExists,
			Success,
		}

		public static AddSurveyOptionStatuses AddSurveyOption(int instituteID, int surveyQuestionID, string optionText, double? weightage, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				var surveyQuestionOption = new SurveyQuestionOption
				{
					SurveyQuestionID = surveyQuestionID,
					OptionText = optionText,
					Weightage = weightage,
				}.Validate();
				if (aspireContext.SurveyQuestionOptions.Any(sqo => sqo.SurveyQuestionID == surveyQuestionOption.SurveyQuestionID && (sqo.OptionText == surveyQuestionOption.OptionText || sqo.Weightage == surveyQuestionOption.Weightage)))
					return AddSurveyOptionStatuses.AlreadyExists;

				aspireContext.SurveyQuestionOptions.Add(surveyQuestionOption);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddSurveyOptionStatuses.Success;
			}
		}

		public enum UpdateSurveyOptionStatuses
		{
			NoRecordFound,
			AlreadyExists,
			Success,
		}

		public static UpdateSurveyOptionStatuses UpdateSurveyOption(int instituteID, int surveyQuestionOptionID, string optionText, double? weightage, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				var surveyQuestionOption = aspireContext.SurveyQuestionOptions.SingleOrDefault(sqo => sqo.SurveyQuestionOptionID == surveyQuestionOptionID);
				if (surveyQuestionOption == null)
					return UpdateSurveyOptionStatuses.NoRecordFound;
				surveyQuestionOption.OptionText = optionText;
				surveyQuestionOption.Weightage = weightage;
				surveyQuestionOption.Validate();
				if (aspireContext.SurveyQuestionOptions.Any(sqo => sqo.SurveyQuestionOptionID != surveyQuestionOption.SurveyQuestionOptionID && sqo.SurveyQuestionID == surveyQuestionOption.SurveyQuestionID && (sqo.OptionText == surveyQuestionOption.OptionText || sqo.Weightage == surveyQuestionOption.Weightage)))
					return UpdateSurveyOptionStatuses.AlreadyExists;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateSurveyOptionStatuses.Success;
			}
		}

		public enum DeleteSurveysOptionStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteSurveysOptionStatuses DeleteSurveyOption(int surveyQuestionOptionID, int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				var surveyQuestionOption = aspireContext.SurveyQuestionOptions.SingleOrDefault(sqo => sqo.SurveyQuestionOptionID == surveyQuestionOptionID);
				if (surveyQuestionOption == null)
					return DeleteSurveysOptionStatuses.NoRecordFound;
				if (surveyQuestionOption.SurveyUserAnswers.Any())
					return DeleteSurveysOptionStatuses.ChildRecordExists;
				aspireContext.SurveyQuestionOptions.Remove(surveyQuestionOption);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteSurveysOptionStatuses.Success;
			}
		}

		public static SurveyQuestionOption GetSurveyOption(int surveyQuestionOptionID, int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				return aspireContext.SurveyQuestionOptions.Include(sqo => sqo.SurveyQuestion).SingleOrDefault(sqo => sqo.SurveyQuestionOptionID == surveyQuestionOptionID);
			}
		}

		#endregion

		public enum ReorderSurveyTable
		{
			SurveyQuestionGroups,
			SurveyQuestions,
		}

		private static void ReorderEntity(this AspireContext aspireContext, int? surveyID, int? surveyQuestionGroupID, ReorderSurveyTable entityName, int userLoginHistoryID)
		{

			switch (entityName)
			{
				case ReorderSurveyTable.SurveyQuestionGroups:
					int displayIndexGroups = 1;
					foreach (var surveyQuestionGroup in aspireContext.SurveyQuestionGroups.Where(sqg => sqg.SurveyID == surveyID).OrderBy(sqg => sqg.DisplayIndex).ToList())
						surveyQuestionGroup.DisplayIndex = displayIndexGroups++;
					break;
				case ReorderSurveyTable.SurveyQuestions:
					byte displayIndexQuestions = 1;
					foreach (var surveyQuestion in aspireContext.SurveyQuestions.Where(sq => sq.SurveyQuestionGroupID == surveyQuestionGroupID).OrderBy(sq => sq.DisplayIndex).ToList())
						surveyQuestion.DisplayIndex = displayIndexQuestions++;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(entityName), entityName, null);
			}
			aspireContext.SaveChanges(userLoginHistoryID);
		}
	}
}
