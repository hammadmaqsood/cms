﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.QualityAssurance.Admin
{
	public static class Survey
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.QualityAssurance.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.QualityAssurance.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.QualityAssurance.ManageSurveys, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.QualityAssurance.ManageSurveys, UserGroupPermission.PermissionValues.Allowed);
		}

		public enum AddSurveysStatuses
		{
			AlreadyExists,
			Success,
		}

		public static AddSurveysStatuses AddSurvey(int instituteID, string surveyName, Model.Entities.Survey.SurveyTypes surveyTypeEnum, string description, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				var survey = new Model.Entities.Survey
				{
					InstituteID = instituteID,
					SurveyName = surveyName,
					SurveyTypeEnum = surveyTypeEnum,
					Description = description,
				}.Validate();
				if (aspireContext.Surveys.Any(s => s.InstituteID==instituteID && s.SurveyName == survey.SurveyName && s.SurveyType == (byte)survey.SurveyTypeEnum))
					return AddSurveysStatuses.AlreadyExists;
				aspireContext.Surveys.Add(survey);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddSurveysStatuses.Success;
			}
		}

		public static List<Model.Entities.Survey> GetSurveys(int? instituteID, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				var query = aspireContext.Surveys.Include(s => s.Institute).AsQueryable();
				if (instituteID != null)
					query = query.Where(q => q.InstituteID == instituteID);
				virtualItemCount = query.Count();
				switch (sortExpression)
				{

					case nameof(Model.Entities.Institute.InstituteAlias):
						query = query.OrderBy(sortDirection, q => q.Institute.InstituteAlias);
						break;
					case nameof(Model.Entities.Survey.SurveyName):
						query = query.OrderBy(sortDirection, q => q.SurveyName);
						break;
					case nameof(Model.Entities.Survey.Description):
						query = query.OrderBy(sortDirection, q => q.Description);
						break;
					case nameof(Model.Entities.Survey.SurveyType):
						query = query.OrderBy(sortDirection, q => q.SurveyType);
						break;
				}
				query = query.Skip(pageIndex * pageSize).Take(pageSize);
				return query.ToList();
			}
		}

		public static Model.Entities.Survey GetSurvey(int surveyID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var survey = aspireContext.Surveys.SingleOrDefault(s => s.SurveyID == surveyID);
				if (survey != null)
					aspireContext.DemandModulePermissions(survey.InstituteID, loginSessionGuid);
				return survey;
			}
		}

		public enum UpdateSurveysStatuses
		{
			NoRecordFound,
			AlreadyExists,
			Success,
		}

		public static UpdateSurveysStatuses UpdateSurvey(int surveyID, string surveyName, Model.Entities.Survey.SurveyTypes surveyTypeEnum, string description, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var survey = aspireContext.Surveys.SingleOrDefault(s => s.SurveyID == surveyID);
				if (survey == null)
					return UpdateSurveysStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(survey.InstituteID, loginSessionGuid);
				survey.SurveyName = surveyName;
				survey.Description = description;
				survey.SurveyTypeEnum = surveyTypeEnum;
				survey.Validate();
				if (aspireContext.Surveys.Any(s => s.InstituteID == survey.InstituteID && s.SurveyID != survey.SurveyID && s.SurveyName == survey.SurveyName && s.SurveyType == (byte)survey.SurveyTypeEnum))
					return UpdateSurveysStatuses.AlreadyExists;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateSurveysStatuses.Success;
			}
		}

		public enum DeleteSurveysStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteSurveysStatuses DeleteSurvey(int surveyID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var survey = aspireContext.Surveys.SingleOrDefault(s => s.SurveyID == surveyID);
				if (survey == null)
					return DeleteSurveysStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandModulePermissions(survey.InstituteID, loginSessionGuid);
				if (survey.SurveyConducts.Any())
					return DeleteSurveysStatuses.ChildRecordExists;
				if (survey.SurveyQuestionGroups.Any())
					return DeleteSurveysStatuses.ChildRecordExists;
				aspireContext.Surveys.Remove(survey);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteSurveysStatuses.Success;
			}
		}
	}
}
