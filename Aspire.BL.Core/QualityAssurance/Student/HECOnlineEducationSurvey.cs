﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Linq;

namespace Aspire.BL.Core.QualityAssurance.Student
{
	public static class HECOnlineEducationSurvey
	{
		public static QAStudentOnlineTeachingSurvey GetQAStudentOnlineTeachingSurvey(short semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStudentActiveSession(loginSessionGuid, Model.Entities.Common.SubUserTypes.None);
				return aspireContext.QAStudentOnlineTeachingSurveys.SingleOrDefault(qa => qa.StudentID == loginHistory.StudentID && qa.SemesterID == semesterID);
			}
		}

		public enum UpdateQAStudentOnlineTeachingSurveyStatuses
		{
			AlreadySubmitted,
			Saved,
			Submitted
		}

		public static UpdateQAStudentOnlineTeachingSurveyStatuses UpdateQAStudentOnlineTeachingSurvey(QAStudentOnlineTeachingSurvey qaStudentOnlineTeachingSurvey, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStudentActiveSession(loginSessionGuid, Model.Entities.Common.SubUserTypes.None);
				var qaStudentOnlineTeachingSurveysDB = aspireContext.QAStudentOnlineTeachingSurveys.SingleOrDefault(qa => qa.StudentID == loginHistory.StudentID && qa.SemesterID == qaStudentOnlineTeachingSurvey.SemesterID);
				if (qaStudentOnlineTeachingSurveysDB != null)
				{
					if (qaStudentOnlineTeachingSurveysDB.SubmittedDate != null)
						return UpdateQAStudentOnlineTeachingSurveyStatuses.AlreadySubmitted;
				}
				else
				{
					qaStudentOnlineTeachingSurveysDB = aspireContext.QAStudentOnlineTeachingSurveys.Add(new QAStudentOnlineTeachingSurvey
					{
						QAStudentOnlineTeachingSurveyID = Guid.NewGuid(),
						StudentID = loginHistory.StudentID,
						SemesterID = qaStudentOnlineTeachingSurvey.SemesterID,
					});
				}

				qaStudentOnlineTeachingSurveysDB.Q11WhatsappNo = qaStudentOnlineTeachingSurvey.Q11WhatsappNo;
				qaStudentOnlineTeachingSurveysDB.Q12 = qaStudentOnlineTeachingSurvey.Q12;
				qaStudentOnlineTeachingSurveysDB.Q13 = qaStudentOnlineTeachingSurvey.Q13;
				qaStudentOnlineTeachingSurveysDB.Q14 = qaStudentOnlineTeachingSurvey.Q14;
				qaStudentOnlineTeachingSurveysDB.Q15 = qaStudentOnlineTeachingSurvey.Q15;
				qaStudentOnlineTeachingSurveysDB.Q16 = qaStudentOnlineTeachingSurvey.Q16;
				qaStudentOnlineTeachingSurveysDB.Q17 = qaStudentOnlineTeachingSurvey.Q17;
				qaStudentOnlineTeachingSurveysDB.Q18 = qaStudentOnlineTeachingSurvey.Q18;
				qaStudentOnlineTeachingSurveysDB.Q19 = qaStudentOnlineTeachingSurvey.Q19;
				qaStudentOnlineTeachingSurveysDB.Q20 = qaStudentOnlineTeachingSurvey.Q20;
				qaStudentOnlineTeachingSurveysDB.Q21 = qaStudentOnlineTeachingSurvey.Q21;
				qaStudentOnlineTeachingSurveysDB.Q21Other = qaStudentOnlineTeachingSurvey.Q21Other;
				qaStudentOnlineTeachingSurveysDB.SubmittedDate = DateTime.Now;

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return qaStudentOnlineTeachingSurveysDB.SubmittedDate != null ? UpdateQAStudentOnlineTeachingSurveyStatuses.Submitted : UpdateQAStudentOnlineTeachingSurveyStatuses.Saved;
			}
		}
	}
}
