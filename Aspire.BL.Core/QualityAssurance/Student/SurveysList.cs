﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.QualityAssurance.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using Aspire.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.QualityAssurance.Student
{
	public static class SurveysList
	{
		public sealed class QASurveyUser
		{
			internal QASurveyUser() { }
			public int? StudentID { get; internal set; }
			public int? QASurveyUserID { get; internal set; }
			public int? QASurveyConductID { get; internal set; }
			public short? SemesterID { get; internal set; }
			public DateTime StartDate { get; internal set; }
			public DateTime? EndDate { get; internal set; }
			public DateTime? SubmissionDate { get; internal set; }
			public string SemesterIDEnum => this.SemesterID.ToSemesterString();
			public Guid Code { get; internal set; }
			public Guid SurveyTypeGuid { get; set; }
			public string SurveyName => Surveys.GetSurveyName(this.SurveyTypeGuid);
			public byte? SurveyConductStatus { get; internal set; }
			public string ConductStatusFullName => this.SurveyConductStatus != null ? ((Model.Entities.QASurveyUser.Statuses)this.SurveyConductStatus).ToString().SplitCamelCasing() : ((Model.Entities.QASurveyUser.Statuses.NotStarted)).ToString().SplitCamelCasing();
			public byte? SurveyUserStatus { get; internal set; }
			public string UserStatusFullName => this.SurveyUserStatus != null ? ((Model.Entities.QASurveyUser.Statuses)this.SurveyUserStatus).ToString().SplitCamelCasing() : ((Model.Entities.QASurveyUser.Statuses.NotStarted)).ToString().SplitCamelCasing();
		}

		public static List<QASurveyUser> GetSurveyslist(int studentID, int pageIndex, int pageSize, out int virtualItemCount, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (studentID == 0)
			{
				virtualItemCount = 0;
				return null;
			}
			using (var aspireContext = new AspireContext())
			{
				var qaSurveyUser = aspireContext.QASurveyUsers
					.Where(s => s.StudentID == studentID)
					.Where(s => s.QASurveyConduct.StartDate <= DateTime.Today)
					.Where(s => s.QASurveyConduct.SurveyStatus == (byte) QASurveyConduct.Statuses.Active)
					.Where(s => s.Status != (byte) Model.Entities.QASurveyUser.Statuses.Completed)
					.Select(s => new QASurveyUser
					{
						StudentID = s.StudentID,
						QASurveyUserID = s.QASurveyUserID,
						QASurveyConductID = s.QASurveyConductID,
						SemesterID = s.QASurveyConduct.SemesterID,
						StartDate = s.QASurveyConduct.StartDate,
						SubmissionDate = s.SubmissionDate,
						EndDate = s.QASurveyConduct.EndDate,
						SurveyTypeGuid = s.QASurveyConduct.SurveyTypeGuid,
						Code = s.Code,
						SurveyConductStatus = s.QASurveyConduct.SurveyStatus,
						SurveyUserStatus = s.Status,
					})
					.AsQueryable();
				virtualItemCount = qaSurveyUser.Count();
				switch (sortExpression)
				{
					case nameof(QASurveyUser.SurveyTypeGuid):
						qaSurveyUser = qaSurveyUser.OrderBy(sortDirection, f => f.SurveyTypeGuid);
						break;
					case nameof(QASurveyUser.SemesterID):
						qaSurveyUser = qaSurveyUser.OrderBy(sortDirection, f => f.SemesterID);
						break;
					case nameof(QASurveyUser.StartDate):
						qaSurveyUser = qaSurveyUser.OrderBy(sortDirection, f => f.StartDate);
						break;
					case nameof(QASurveyUser.EndDate):
						qaSurveyUser = qaSurveyUser.OrderBy(sortDirection, f => f.EndDate);
						break;
					case nameof(QASurveyUser.SubmissionDate):
						qaSurveyUser = qaSurveyUser.OrderBy(sortDirection, f => f.SubmissionDate);
						break;
					case nameof(QASurveyUser.SurveyConductStatus):
						qaSurveyUser = qaSurveyUser.OrderBy(sortDirection, f => f.SurveyConductStatus);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return qaSurveyUser.Skip(pageIndex * pageSize).Take(pageSize).ToList();
			}
		}
	}
}
