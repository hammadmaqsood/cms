﻿using System;

namespace Aspire.BL.Core.QualityAssurance.Common
{
	public static class Surveys
	{
		public static string GetSurveyName(Guid surveyGuid)
		{
			if (Guid.Parse("7FDF7020-D39C-4D99-A2EB-B98C277100FD") == surveyGuid)
			{
				return "Faculty Survey (Survey Form-2)";
			}
			if (Guid.Parse("EC1EE946-64AA-402E-ABFC-718D520B0914") == surveyGuid)
			{
				return "Student Survey (Survey Form-4)";
			}
			if (Guid.Parse("AE855BEF-DCF7-4BE4-AA2B-A23BBC9A3C30") == surveyGuid)
			{
				return "Faculty Survey (Survey Form-5)";
			}
			if (Guid.Parse("262F6082-282E-42EF-8FE7-5C8DD7138284") == surveyGuid)
			{
				return "Department Survey (Survey Form-6)";
			}
			if (Guid.Parse("A64BFEB7-AF8B-459E-AB1C-5F13BA0BB719") == surveyGuid)
			{
				return "Alumni Survey (Survey Form-7)";
			}
			if (Guid.Parse("262F6082-282E-42EF-8FE7-5C8DD7138284") == surveyGuid)
			{
				return "Employer Survey (Survey Form-8)";
			}
			throw new InvalidOperationException();
		}
	}
}
