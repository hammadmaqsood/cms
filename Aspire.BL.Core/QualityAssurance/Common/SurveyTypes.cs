﻿using System;

namespace Aspire.BL.Core.QualityAssurance.Common
{
	public static class SurveyTypes
	{
		public static Guid SurveyForm2 => new Guid("990FF133-C585-4EF5-BCA2-4EA705A9DA32");
		public static Guid SurveyForm4 => new Guid("8CBA1E28-904A-4F73-842B-BA3E355AF2CF");
		public static Guid SurveyForm5 => new Guid("B0DCFB58-3B03-450F-91DE-CAA2F43A79AC");
		public static Guid SurveyForm6 => new Guid("ABBA25C5-641C-459C-8CC1-3E268E839C05");
		public static Guid SurveyForm7 => new Guid("CE7BBFF2-1CBC-448F-9D66-D7DCDC8C4DA3");
		public static Guid SurveyForm8 => new Guid("9A23BDE1-B834-41AE-9303-616F0B780BBD");

		public static Guid SummaryAnalysisForm2 => new Guid("06F2FBF5-5163-434C-8C5F-9A2AD3C1BD43");
		public static Guid SummaryAnalysisForm4 => new Guid("DEBADDD0-80DC-4A52-B3B9-D4F2644CA1B4");
		public static Guid SummaryAnalysisForm5 => new Guid("750B5F36-4408-4BE5-9492-34FA98762096");
		public static Guid SummaryAnalysisForm6 => new Guid("A4EB906B-863C-404E-BD0F-C529B1D7FD4B");
		public static Guid SummaryAnalysisForm7 => new Guid("146F34D0-8010-4201-862C-6B6509A76410");
		public static Guid SummaryAnalysisForm8 => new Guid("61532505-6D6D-4197-887C-C529D04B96FF");
	}
}
