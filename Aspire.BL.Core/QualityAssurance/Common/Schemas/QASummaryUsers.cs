﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.QualityAssurance.Common.Schemas
{
	public static class QASummaryUsers
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, StaffPermissions.QualityAssurance qualityAssurance, UserGroupPermission.PermissionValues[] permissionValues, int instituteID, int? departmentID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, qualityAssurance, permissionValues, instituteID, departmentID, null, null);
		}

		public sealed class GetSummaryUserResult
		{
			internal GetSummaryUserResult() { }
			public QASurveySummaryAnalysi QASurveySummaryAnalysi { get; internal set; }
			public QASurveySummaryAnalysi.Statuses SummaryStatusEnum { get; internal set; }
			public int? InstituteID { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public int? ProgramID { get; internal set; }
			public string DepartmentAlias { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short? SemesterID { get; internal set; }
			public string SemesterIDEnum => this.SemesterID.ToSemesterString();
			public bool StaffCanEdit => this.QASurveySummaryAnalysi.Status != (byte)QASurveySummaryAnalysi.Statuses.Completed;
		}

		public static GetSummaryUserResult GetSummary2User(int qaSurveySummaryAnalysisID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSummaryUserResult = aspireContext.QASurveySummaryAnalysis
					.Where(qasu => qasu.QASurveySummaryAnalysisID == qaSurveySummaryAnalysisID)
					.Select(qsu => new GetSummaryUserResult
					{
						QASurveySummaryAnalysi = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID,
						DepartmentID = qsu.DepartmentID,
						DepartmentAlias = qsu.Department.DepartmentName,
						SemesterID = qsu.SemesterID,
						SummaryStatusEnum = (QASurveySummaryAnalysi.Statuses)qsu.Status
					}).SingleOrDefault();
				if (getSummaryUserResult == null)
					return null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.ManageSummarySurveyForm2
, new[] { UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Readonly, }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						return getSummaryUserResult;
					case UserTypes.Faculty:
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Candidate:
					case UserTypes.Student:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
		public static GetSummaryUserResult GetSummary4User(int qaSurveySummaryAnalysisID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSummaryUserResult = aspireContext.QASurveySummaryAnalysis
					.Where(qasu => qasu.QASurveySummaryAnalysisID == qaSurveySummaryAnalysisID)
					.Select(qsu => new GetSummaryUserResult
					{
						QASurveySummaryAnalysi = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID,
						DepartmentID = qsu.DepartmentID,
						DepartmentAlias = qsu.Department.DepartmentName,
						SemesterID = qsu.SemesterID,
						SummaryStatusEnum = (QASurveySummaryAnalysi.Statuses)qsu.Status
					}).SingleOrDefault();
				if (getSummaryUserResult == null)
					return null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.ManageSummarySurveyForm4
, new[] { UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Readonly, }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						return getSummaryUserResult;
					case UserTypes.Faculty:
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Candidate:
					case UserTypes.Student:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
		public static GetSummaryUserResult GetSummary5User(int qaSurveySummaryAnalysisID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSummaryUserResult = aspireContext.QASurveySummaryAnalysis
					.Where(qasu => qasu.QASurveySummaryAnalysisID == qaSurveySummaryAnalysisID)
					.Select(qsu => new GetSummaryUserResult
					{
						QASurveySummaryAnalysi = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID,
						DepartmentID = qsu.DepartmentID,
						DepartmentAlias = qsu.Department.DepartmentName,
						SemesterID = qsu.SemesterID,
						SummaryStatusEnum = (QASurveySummaryAnalysi.Statuses)qsu.Status
					}).SingleOrDefault();
				if (getSummaryUserResult == null)
					return null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.ManageSummarySurveyForm5
, new[] { UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Readonly, }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						return getSummaryUserResult;
					case UserTypes.Faculty:
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Candidate:
					case UserTypes.Student:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
		public static GetSummaryUserResult GetSummary6User(int qaSurveySummaryAnalysisID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSummaryUserResult = aspireContext.QASurveySummaryAnalysis
					.Where(qasu => qasu.QASurveySummaryAnalysisID == qaSurveySummaryAnalysisID)
					.Select(qsu => new GetSummaryUserResult
					{
						QASurveySummaryAnalysi = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID,
						DepartmentID = qsu.DepartmentID,
						DepartmentAlias = qsu.Department.DepartmentName,
						SemesterID = qsu.SemesterID,
						SummaryStatusEnum = (QASurveySummaryAnalysi.Statuses)qsu.Status
					}).SingleOrDefault();
				if (getSummaryUserResult == null)
					return null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.ManageSummarySurveyForm7
, new[] { UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Readonly, }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						return getSummaryUserResult;
					case UserTypes.Faculty:
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Candidate:
					case UserTypes.Student:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
		public static GetSummaryUserResult GetSummary7User(int qaSurveySummaryAnalysisID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSummaryUserResult = aspireContext.QASurveySummaryAnalysis
					.Where(qasu => qasu.QASurveySummaryAnalysisID == qaSurveySummaryAnalysisID)
					.Select(qsu => new GetSummaryUserResult
					{
						QASurveySummaryAnalysi = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID,
						DepartmentID = qsu.DepartmentID,
						DepartmentAlias = qsu.Department.DepartmentName,
						SemesterID = qsu.SemesterID,
						SummaryStatusEnum = (QASurveySummaryAnalysi.Statuses)qsu.Status
					}).SingleOrDefault();
				if (getSummaryUserResult == null)
					return null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.ManageSummarySurveyForm7
, new[] { UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Readonly, }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						return getSummaryUserResult;
					case UserTypes.Faculty:
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Candidate:
					case UserTypes.Student:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
		public static GetSummaryUserResult GetSummary8User(int qaSurveySummaryAnalysisID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSummaryUserResult = aspireContext.QASurveySummaryAnalysis
					.Where(qasu => qasu.QASurveySummaryAnalysisID == qaSurveySummaryAnalysisID)
					.Select(qsu => new GetSummaryUserResult
					{
						QASurveySummaryAnalysi = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID,
						DepartmentID = qsu.DepartmentID,
						DepartmentAlias = qsu.Department.DepartmentName,
						SemesterID = qsu.SemesterID,
						SummaryStatusEnum = (QASurveySummaryAnalysi.Statuses)qsu.Status
					}).SingleOrDefault();
				if (getSummaryUserResult == null)
					return null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.ManageSummarySurveyForm8
, new[] { UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Readonly, }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						return getSummaryUserResult;
					case UserTypes.Faculty:
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Candidate:
					case UserTypes.Student:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
		public static QASurveySummaryAnalysi.ValidationResults UpdateSummary2Values(int qaSurveySummaryAnalysisID, string surveyValues, QASurveySummaryAnalysi.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSummaryUserResult = aspireContext.QASurveySummaryAnalysis
					.Where(qasu => qasu.QASurveySummaryAnalysisID == qaSurveySummaryAnalysisID)
					.Select(qsu => new GetSummaryUserResult
					{
						QASurveySummaryAnalysi = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID,
						DepartmentID = qsu.DepartmentID,
						ProgramID = qsu.ProgramID,
						SummaryStatusEnum = (QASurveySummaryAnalysi.Statuses)qsu.Status,
					}).SingleOrDefault();
				if (getSummaryUserResult == null)
					return QASurveySummaryAnalysi.ValidationResults.NoRecordFound;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.ManageSummarySurveyForm2, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						break;
					case UserTypes.Faculty:
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Candidate:
					case UserTypes.Student:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
				switch (getSummaryUserResult.SummaryStatusEnum)
				{
					case QASurveySummaryAnalysi.Statuses.NotStarted:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.InProgress:
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveySummaryAnalysi.Statuses.InProgress:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.InProgress:
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveySummaryAnalysi.Statuses.Completed:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
							case QASurveySummaryAnalysi.Statuses.InProgress:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				getSummaryUserResult.QASurveySummaryAnalysi.Status = (byte)statusEnum;
				getSummaryUserResult.QASurveySummaryAnalysi.SummaryValues = surveyValues;
				getSummaryUserResult.QASurveySummaryAnalysi.SubmissionDate = DateTime.Now;
				getSummaryUserResult.QASurveySummaryAnalysi.SubmittedByUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return QASurveySummaryAnalysi.ValidationResults.Success;
			}
		}
		public static QASurveySummaryAnalysi.ValidationResults UpdateSummary4Values(int qaSurveySummaryAnalysisID, string surveyValues, QASurveySummaryAnalysi.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSummaryUserResult = aspireContext.QASurveySummaryAnalysis
					.Where(qasu => qasu.QASurveySummaryAnalysisID == qaSurveySummaryAnalysisID)
					.Select(qsu => new GetSummaryUserResult
					{
						QASurveySummaryAnalysi = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID,
						DepartmentID = qsu.DepartmentID,
						ProgramID = qsu.ProgramID,
						SummaryStatusEnum = (QASurveySummaryAnalysi.Statuses)qsu.Status,
					}).SingleOrDefault();
				if (getSummaryUserResult == null)
					return QASurveySummaryAnalysi.ValidationResults.NoRecordFound;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.ManageSummarySurveyForm4, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						break;
					case UserTypes.Faculty:
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Candidate:
					case UserTypes.Student:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
				switch (getSummaryUserResult.SummaryStatusEnum)
				{
					case QASurveySummaryAnalysi.Statuses.NotStarted:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.InProgress:
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveySummaryAnalysi.Statuses.InProgress:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.InProgress:
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveySummaryAnalysi.Statuses.Completed:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
							case QASurveySummaryAnalysi.Statuses.InProgress:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				getSummaryUserResult.QASurveySummaryAnalysi.Status = (byte)statusEnum;
				getSummaryUserResult.QASurveySummaryAnalysi.SummaryValues = surveyValues;
				getSummaryUserResult.QASurveySummaryAnalysi.SubmissionDate = DateTime.Now;
				getSummaryUserResult.QASurveySummaryAnalysi.SubmittedByUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return QASurveySummaryAnalysi.ValidationResults.Success;
			}
		}
		public static QASurveySummaryAnalysi.ValidationResults UpdateSummary5Values(int qaSurveySummaryAnalysisID, string surveyValues, QASurveySummaryAnalysi.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSummaryUserResult = aspireContext.QASurveySummaryAnalysis
					.Where(qasu => qasu.QASurveySummaryAnalysisID == qaSurveySummaryAnalysisID)
					.Select(qsu => new GetSummaryUserResult
					{
						QASurveySummaryAnalysi = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID,
						DepartmentID = qsu.DepartmentID,
						ProgramID = qsu.ProgramID,
						SummaryStatusEnum = (QASurveySummaryAnalysi.Statuses)qsu.Status,
					}).SingleOrDefault();
				if (getSummaryUserResult == null)
					return QASurveySummaryAnalysi.ValidationResults.NoRecordFound;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.ManageSummarySurveyForm5, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						break;
					case UserTypes.Faculty:
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Candidate:
					case UserTypes.Student:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
				switch (getSummaryUserResult.SummaryStatusEnum)
				{
					case QASurveySummaryAnalysi.Statuses.NotStarted:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.InProgress:
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveySummaryAnalysi.Statuses.InProgress:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.InProgress:
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveySummaryAnalysi.Statuses.Completed:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
							case QASurveySummaryAnalysi.Statuses.InProgress:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				getSummaryUserResult.QASurveySummaryAnalysi.Status = (byte)statusEnum;
				getSummaryUserResult.QASurveySummaryAnalysi.SummaryValues = surveyValues;
				getSummaryUserResult.QASurveySummaryAnalysi.SubmissionDate = DateTime.Now;
				getSummaryUserResult.QASurveySummaryAnalysi.SubmittedByUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return QASurveySummaryAnalysi.ValidationResults.Success;
			}
		}
		public static QASurveySummaryAnalysi.ValidationResults UpdateSummary6Values(int qaSurveySummaryAnalysisID, string surveyValues, QASurveySummaryAnalysi.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSummaryUserResult = aspireContext.QASurveySummaryAnalysis
					.Where(qasu => qasu.QASurveySummaryAnalysisID == qaSurveySummaryAnalysisID)
					.Select(qsu => new GetSummaryUserResult
					{
						QASurveySummaryAnalysi = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID,
						DepartmentID = qsu.DepartmentID,
						ProgramID = qsu.ProgramID,
						SummaryStatusEnum = (QASurveySummaryAnalysi.Statuses)qsu.Status,
					}).SingleOrDefault();
				if (getSummaryUserResult == null)
					return QASurveySummaryAnalysi.ValidationResults.NoRecordFound;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.ManageSummarySurveyForm6, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						break;
					case UserTypes.Faculty:
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Candidate:
					case UserTypes.Student:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
				switch (getSummaryUserResult.SummaryStatusEnum)
				{
					case QASurveySummaryAnalysi.Statuses.NotStarted:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.InProgress:
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveySummaryAnalysi.Statuses.InProgress:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.InProgress:
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveySummaryAnalysi.Statuses.Completed:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
							case QASurveySummaryAnalysi.Statuses.InProgress:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				getSummaryUserResult.QASurveySummaryAnalysi.Status = (byte)statusEnum;
				getSummaryUserResult.QASurveySummaryAnalysi.SummaryValues = surveyValues;
				getSummaryUserResult.QASurveySummaryAnalysi.SubmissionDate = DateTime.Now;
				getSummaryUserResult.QASurveySummaryAnalysi.SubmittedByUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return QASurveySummaryAnalysi.ValidationResults.Success;
			}
		}

		public static QASurveySummaryAnalysi.ValidationResults UpdateSummary7Values(int qaSurveySummaryAnalysisID, string surveyValues, QASurveySummaryAnalysi.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSummaryUserResult = aspireContext.QASurveySummaryAnalysis
					.Where(qasu => qasu.QASurveySummaryAnalysisID == qaSurveySummaryAnalysisID)
					.Select(qsu => new GetSummaryUserResult
					{
						QASurveySummaryAnalysi = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID,
						DepartmentID = qsu.DepartmentID,
						ProgramID = qsu.ProgramID,
						SummaryStatusEnum = (QASurveySummaryAnalysi.Statuses)qsu.Status,
					}).SingleOrDefault();
				if (getSummaryUserResult == null)
					return QASurveySummaryAnalysi.ValidationResults.NoRecordFound;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.ManageSummarySurveyForm7, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						break;
					case UserTypes.Faculty:
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Candidate:
					case UserTypes.Student:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
				switch (getSummaryUserResult.SummaryStatusEnum)
				{
					case QASurveySummaryAnalysi.Statuses.NotStarted:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.InProgress:
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveySummaryAnalysi.Statuses.InProgress:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.InProgress:
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveySummaryAnalysi.Statuses.Completed:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
							case QASurveySummaryAnalysi.Statuses.InProgress:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				getSummaryUserResult.QASurveySummaryAnalysi.Status = (byte)statusEnum;
				getSummaryUserResult.QASurveySummaryAnalysi.SummaryValues = surveyValues;
				getSummaryUserResult.QASurveySummaryAnalysi.SubmissionDate = DateTime.Now;
				getSummaryUserResult.QASurveySummaryAnalysi.SubmittedByUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return QASurveySummaryAnalysi.ValidationResults.Success;
			}
		}
		public static QASurveySummaryAnalysi.ValidationResults UpdateSummary8Values(int qaSurveySummaryAnalysisID, string surveyValues, QASurveySummaryAnalysi.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSummaryUserResult = aspireContext.QASurveySummaryAnalysis
					.Where(qasu => qasu.QASurveySummaryAnalysisID == qaSurveySummaryAnalysisID)
					.Select(qsu => new GetSummaryUserResult
					{
						QASurveySummaryAnalysi = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID,
						DepartmentID = qsu.DepartmentID,
						ProgramID = qsu.ProgramID,
						SummaryStatusEnum = (QASurveySummaryAnalysi.Statuses)qsu.Status,
					}).SingleOrDefault();
				if (getSummaryUserResult == null)
					return QASurveySummaryAnalysi.ValidationResults.NoRecordFound;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.ManageSummarySurveyForm7, new[] { UserGroupPermission.PermissionValues.Allowed }, getSummaryUserResult.InstituteID.Value, getSummaryUserResult.DepartmentID);
						break;
					case UserTypes.Faculty:
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Candidate:
					case UserTypes.Student:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
				switch (getSummaryUserResult.SummaryStatusEnum)
				{
					case QASurveySummaryAnalysi.Statuses.NotStarted:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.InProgress:
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveySummaryAnalysi.Statuses.InProgress:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.InProgress:
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveySummaryAnalysi.Statuses.Completed:
						switch (statusEnum)
						{
							case QASurveySummaryAnalysi.Statuses.NotStarted:
							case QASurveySummaryAnalysi.Statuses.InProgress:
								throw new InvalidOperationException();
							case QASurveySummaryAnalysi.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				getSummaryUserResult.QASurveySummaryAnalysi.Status = (byte)statusEnum;
				getSummaryUserResult.QASurveySummaryAnalysi.SummaryValues = surveyValues;
				getSummaryUserResult.QASurveySummaryAnalysi.SubmissionDate = DateTime.Now;
				getSummaryUserResult.QASurveySummaryAnalysi.SubmittedByUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return QASurveySummaryAnalysi.ValidationResults.Success;
			}
		}
	}
}
