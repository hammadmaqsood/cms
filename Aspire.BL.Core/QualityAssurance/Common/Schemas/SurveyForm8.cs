﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspire.BL.Core.QualityAssurance.Common.Schemas
{
	public abstract class SurveyForm8 : BaseSurvey
	{
		public override Guid SurveyTypeGuid => SurveyTypes.SurveyForm8;

		public override string SurveyName => "Employer Survey - Survey Form-8";

		public override string SurveyDescription => "Employer Survey - Survey Form-8";

		public static class Versions
		{
			public static Guid Version1 => new Guid("0C176CDA-27FC-4FCB-9792-F98EC4B6FD80");
			public static List<Guid> GetVersions()
			{
				return new List<Guid>
				{
					Version1
				};
			}
		}
	}
}
