using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspire.BL.Core.QualityAssurance.Common.Schemas
{
	public abstract class SurveyForm6 : BaseSurvey
	{
		public override Guid SurveyTypeGuid => SurveyTypes.SurveyForm6;

		public override string SurveyName => "Department Survey - Survey Form-6";

		public override string SurveyDescription => "Survey of Department Offering Ph.D. Programs - Survey Form-6";

		public static class Versions
		{
			public static Guid Version1 => new Guid("5CCD32AA-FE86-4C10-ABF2-CD80D2AEE046");
			public static List<Guid> GetVersions()
			{
				return new List<Guid>
				{
					Version1
				};
			}
		}
	}
}
