﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspire.BL.Core.QualityAssurance.Common.Schemas
{
	public class Prerequisites
	{
		public string prerequisites { get; set; }
	}
	public class FinalGrades
	{
		public int GradeACount { get; set; }
		public int GradeAMinusCount { get; set; }
		public int GradeBPlusCount { get; set; }
		public int GradeBCount { get; set; }
		public int GradeBMinusCount { get; set; }
		public int GradeCPlusCount { get; set; }
		public int GradeCCount { get; set; }
		public int GradeCMinusCount { get; set; }
		public int GradeDPlusCount { get; set; }
		public int GradeDCount { get; set; }
		public int GradeFCount { get; set; }
		public int GradeWCount { get; set; }
		public int NoGradeCount { get; set; }
		public int TotalGradeCount => GradeACount + GradeAMinusCount + GradeBPlusCount + GradeBCount + GradeBMinusCount + GradeCPlusCount + GradeCCount + GradeCMinusCount + GradeDPlusCount + GradeDCount + GradeFCount + GradeWCount + NoGradeCount;
	}
	public sealed class SurveyForm2Version1 : SurveyForm2
	{
		public override Guid SchemaGuid => Versions.Version1;
		public string Q1p1 { get; set; }
		public string Q1p2 { get; set; }
		public string Q1p3 { get; set; }
		public string Q1p4 { get; set; }
		public string Q1p5 { get; set; }
		public string Q1p6 { get; set; }
		public string Q1p7 { get; set; }
		public string Q1p8 { get; set; }
		public List<Prerequisites> Prerequisites { get; set; }
		public string Q1p10 { get; set; }
		public string Q1p11 { get; set; }
		public string Q1p12 { get; set; }
		public string Q1p13a1 { get; set; }
		public string Q1p13a2 { get; set; }
		public string Q1p13b1 { get; set; }
		public string Q1p13b2 { get; set; }
		public string Q1p13c1 { get; set; }
		public string Q1p13c2 { get; set; }
		public string Q1p13d1 { get; set; }
		public string Q1p13d2 { get; set; }
		public FinalGrades finalGrades { get; set; }
		public string Q3 { get; set; }
		public string Q4 { get; set; }
		public string Q5 { get; set; }
		public string Q6 { get; set; }
		public string Q7 { get; set; }
		public string Q8 { get; set; }
		public string Q9 { get; set; }

		public override IEnumerable<string> ValidateValues()
		{
			if (this.Q1p11 == null)
				yield return "Question 1 part 11 is missing.";
			if (this.Q1p12 == null)
				yield return "Question 1 part 12 is missing.";
			if (this.Q1p13a1 == null)
				yield return "Question 1 part 13 is missing.";
			if (this.Q1p13a2 == null)
				yield return "Question 1 part 13 is missing.";
			if (this.Q1p13b1 == null)
				yield return "Question 1 part 13 is missing.";
			if (this.Q1p13b2 == null)
				yield return "Question 1 part 13 is missing.";
			if (this.Q1p13c1 == null)
				yield return "Question 1 part 13 is missing.";
			if (this.Q1p13c2 == null)
				yield return "Question 1 part 13 is missing.";
			if (this.Q1p13d1 == null)
				yield return "Question 1 part 13 is missing.";
			if (this.Q1p13d2 == null)
				yield return "Question 1 part 13 is missing.";
			if (this.Q3 == null)
				yield return "Question 3 is missing.";
			if (this.Q4 == null)
				yield return "Question 4 is missing.";
			if (this.Q5 == null)
				yield return "Question 5 is missing.";
			if (this.Q6 == null)
				yield return "Question 6 is missing.";
			if (this.Q7 == null)
				yield return "Question 7 is missing.";
			if (this.Q8 == null)
				yield return "Question 8 is missing.";
			if (this.Q9 == null)
				yield return "Question 9 is missing.";
		}
	}
}