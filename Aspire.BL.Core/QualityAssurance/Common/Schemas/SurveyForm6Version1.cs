﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspire.BL.Core.QualityAssurance.Common.Schemas
{
	public sealed class SurveyForm6Version1 : SurveyForm6
	{
		public override Guid SchemaGuid => Versions.Version1;
		public string Q1P1 { get; set; }
		public string Q1P2 { get; set; }
		public string Q1P3 { get; set; }
		public string Q1P4 { get; set; }
		public string Q1P5 { get; set; }
		public string Q1P6 { get; set; }
		public string Q1P7 { get; set; }
		public string Q2P1 { get; set; }
		public string Q2P2 { get; set; }
		public string Q2P3 { get; set; }

		public string Q3P1 { get; set; }
		public string Q3P2 { get; set; }
		public string Q3P3 { get; set; }
		public string Q3P4 { get; set; }
		public string Q3P5 { get; set; }
		public string Q3P6 { get; set; }

		public string Q4P1 { get; set; }
		public string Q4P2 { get; set; }
		public string Q4P3 { get; set; }

		public string Q5P1 { get; set; }
		public byte? Q5P2 { get; set; }
		public string Q5P3 { get; set; }
		public string Q5P4 { get; set; }
		public string Q5P5 { get; set; }
		public string Q5P6 { get; set; }
		public byte? Q5P7a { get; set; }
		public byte? Q5P7b { get; set; }
		public byte? Q5P7c { get; set; }
		public byte? Q5P7d { get; set; }
		public string Q5P7e { get; set; }
		public string Q5P8 { get; set; }
		public string Q5P9 { get; set; }
		public string Q5P10 { get; set; }
		public string Q6P1 { get; set; }

		public override IEnumerable<string> ValidateValues()
		{
			if (string.IsNullOrWhiteSpace(this.Q1P5))
				yield return "Question 1.5 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q1P6))
				yield return "Question 1.6 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q1P7))
				yield return "Question 1.7 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q2P1))
				yield return "Question 2.1 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q2P2))
				yield return "Question 2.2 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q2P3))
				yield return "Question 2.3 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q3P1))
				yield return "Question 3.1 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q3P2))
				yield return "Question 3.2 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q3P3))
				yield return "Question 3.3 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q3P4))
				yield return "Question 3.4 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q3P5))
				yield return "Question 3.5 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q3P6))
				yield return "Question 3.6 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q4P1))
				yield return "Question 4.1 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q4P2))
				yield return "Question 4.2 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q4P3))
				yield return "Question 4.3 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q5P1))
				yield return "Question 5.1 is missing.";
			if (this.Q5P2 == null)
				yield return "Question 5.2 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q5P3))
				yield return "Question 5.3 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q5P4))
				yield return "Question 5.4 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q5P5))
				yield return "Question 5.5 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q5P6))
				yield return "Question 5.6 is missing.";
			if (this.Q5P7a == null)
				yield return "Question 5.7(a) is missing.";
			if (this.Q5P7b == null)
				yield return "Question 5.7(b) is missing.";
			if (this.Q5P7c == null)
				yield return "Question 5.7(c) is missing.";
			if (this.Q5P7d == null)
				yield return "Question 5.7(d) is missing.";
			if (string.IsNullOrWhiteSpace(this.Q5P8))
				yield return "Question 5.8 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q5P9))
				yield return "Question 5.9 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q5P10))
				yield return "Question 5.10 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q6P1))
				yield return "Question 6.1 is missing.";
		}
	}
}
