﻿using System;
using System.Collections.Generic;

namespace Aspire.BL.Core.QualityAssurance.Common.Schemas
{
	[Serializable]
	public abstract class BaseSurvey
	{
		public abstract Guid SurveyTypeGuid { get; }
		public abstract Guid SchemaGuid { get; }
		public abstract string SurveyName { get; }
		public abstract string SurveyDescription { get; }
		public abstract IEnumerable<string> ValidateValues();
	}
}
