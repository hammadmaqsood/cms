﻿using System;
using System.Collections.Generic;

namespace Aspire.BL.Core.QualityAssurance.Common.Schemas
{
	public abstract class SurveyForm5 : BaseSurvey
	{
		public override Guid SurveyTypeGuid => SurveyTypes.SurveyForm5;

		public override string SurveyName => "Faculty Survey";

		public override string SurveyDescription => "Faculty Survey (Survey Form-5)";

		public static class Versions
		{
			public static Guid Version1 => new Guid("4F9A2549-9728-42B6-88AD-E607A0D27612");

			public static List<Guid> GetVersions()
			{
				return new List<Guid>
				{
					Version1
				};
			}
		}
	}
}