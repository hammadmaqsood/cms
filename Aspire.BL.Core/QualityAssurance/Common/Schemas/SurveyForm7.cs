﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspire.BL.Core.QualityAssurance.Common.Schemas
{
	public abstract class SurveyForm7 : BaseSurvey
	{
		public override Guid SurveyTypeGuid => SurveyTypes.SurveyForm7;

		public override string SurveyName => "Alumni Survey - Survey Form-7";

		public override string SurveyDescription => "Alumni Survey - Survey Form-7";

		public static class Versions
		{
			public static Guid Version1 => new Guid("1D26D8D7-8964-4131-BA73-C5B7B39E5F0E");
			public static List<Guid> GetVersions()
			{
				return new List<Guid>
				{
					Version1
				};
			}
		}
	}
}
