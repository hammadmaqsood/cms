﻿using System;
using System.Collections.Generic;

namespace Aspire.BL.Core.QualityAssurance.Common.Schemas
{
	public sealed class SurveyForm5Version1 : SurveyForm5
	{
		public override Guid SchemaGuid => Versions.Version1;
		public byte? Q1 { get; set; }
		public byte? Q2 { get; set; }
		public byte? Q3 { get; set; }
		public byte? Q4 { get; set; }
		public byte? Q5 { get; set; }
		public byte? Q6 { get; set; }
		public byte? Q7 { get; set; }
		public byte? Q8 { get; set; }
		public byte? Q9 { get; set; }
		public byte? Q10 { get; set; }
		public byte? Q11 { get; set; }
		public byte? Q12 { get; set; }
		public byte? Q13 { get; set; }
		public byte? Q14 { get; set; }
		public string Q15 { get; set; }
		public string Q16 { get; set; }
		public byte? Q17 { get; set; }
		public byte? Q18 { get; set; }

		public override IEnumerable<string> ValidateValues()
		{
			if (this.Q1 == null)
				yield return "Question 1 is missing.";
			if (this.Q2 == null)
				yield return "Question 2 is missing.";
			if (this.Q3 == null)
				yield return "Question 3 is missing.";
			if (this.Q4 == null)
				yield return "Question 4 is missing.";
			if (this.Q5 == null)
				yield return "Question 5 is missing.";
			if (this.Q6 == null)
				yield return "Question 6 is missing.";
			if (this.Q7 == null)
				yield return "Question 7 is missing.";
			if (this.Q8 == null)
				yield return "Question 8 is missing.";
			if (this.Q9 == null)
				yield return "Question 9 is missing.";
			if (this.Q10 == null)
				yield return "Question 10 is missing.";
			if (this.Q11 == null)
				yield return "Question 11 is missing.";
			if (this.Q12 == null)
				yield return "Question 12 is missing.";
			if (this.Q13 == null)
				yield return "Question 13 is missing.";
			if (this.Q14 == null)
				yield return "Question 14 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q15))
				yield return "Question 15 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q16))
				yield return "Question 16 is missing.";
			if (this.Q17 == null)
				yield return "Question 17 is missing.";
			if (this.Q18 == null)
				yield return "Question 18 is missing.";
		}
	}
}