﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspire.BL.Core.QualityAssurance.Common.Schemas
{
	public sealed class SurveyForm7Version1 : SurveyForm7
	{
		public override Guid SchemaGuid => Versions.Version1;
		public byte? Q1P1 { get; set; }
		public byte? Q1P2 { get; set; }
		public byte? Q1P3 { get; set; }
		public byte? Q1P4 { get; set; }
		public byte? Q1P5 { get; set; }
		public byte? Q1P6 { get; set; }

		public byte? Q2P1 { get; set; }
		public byte? Q2P2 { get; set; }
		public byte? Q2P3 { get; set; }

		public byte? Q3P1 { get; set; }
		public byte? Q3P2 { get; set; }
		public byte? Q3P3 { get; set; }
		public byte? Q3P4 { get; set; }

		public byte? Q4P1 { get; set; }
		public byte? Q4P2 { get; set; }
		public byte? Q4P3 { get; set; }

		public string Q5 { get; set; }
		public string Q6 { get; set; }

		public byte? Q7P1 { get; set; }
		public byte? Q7P2 { get; set; }
		public byte? Q7P3 { get; set; }
		public byte? Q7P4 { get; set; }

		public string Q8P1 { get; set; }
		public string Q8P2 { get; set; }
		public string Q8P3 { get; set; }
		public DateTime? Q8P4 { get; set; }

		public override IEnumerable<string> ValidateValues()
		{
			if (this.Q1P1 == null)
				yield return "Question 1.1 is missing.";
			if (this.Q1P2 == null)
				yield return "Question 1.2 is missing.";
			if (this.Q1P3 == null)
				yield return "Question 1.3 is missing.";
			if (this.Q1P4 == null)
				yield return "Question 1.4 is missing.";
			if (this.Q1P5 == null)
				yield return "Question 1.5 is missing.";
			if (this.Q1P6 == null)
				yield return "Question 1.6 is missing.";

			if (this.Q2P1 == null)
				yield return "Question 2.1 is missing.";
			if (this.Q2P2 == null)
				yield return "Question 2.2 is missing.";
			if (this.Q2P3 == null)
				yield return "Question 2.3 is missing.";

			if (this.Q3P1 == null)
				yield return "Question 3.1 is missing.";
			if (this.Q3P2 == null)
				yield return "Question 3.2 is missing.";
			if (this.Q3P3 == null)
				yield return "Question 3.3 is missing.";
			if (this.Q3P4 == null)
				yield return "Question 3.4 is missing.";

			if (this.Q4P1 == null)
				yield return "Question 4.1 is missing.";
			if (this.Q4P2 == null)
				yield return "Question 4.2 is missing.";
			if (this.Q4P3 == null)
				yield return "Question 4.3 is missing.";

			if (string.IsNullOrWhiteSpace(this.Q5))
				yield return "Question 5 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q6))
				yield return "Question 6 is missing.";

			if (this.Q7P1 == null)
				yield return "Question 7.1 is missing.";
			if (this.Q7P2 == null)
				yield return "Question 7.2 is missing.";
			if (this.Q7P3 == null)
				yield return "Question 7.3 is missing.";
			if (this.Q7P4 == null)
				yield return "Question 7.4 is missing.";


			if (string.IsNullOrWhiteSpace(this.Q8P1))
				yield return "Question 8.1 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q8P2))
				yield return "Question 8.2 is missing.";
			if (string.IsNullOrWhiteSpace(this.Q8P3))
				yield return "Question 8.3 is missing.";
			if (this.Q8P4 == null)
				yield return "Question 8.4 is missing.";
		}
	}
}
