﻿using System;
using System.Collections.Generic;

namespace Aspire.BL.Core.QualityAssurance.Common.Schemas.SummaryAnalysis
{
	public abstract class SummaryAnalysisForm2 : BaseSurvey
	{
		public override Guid SurveyTypeGuid => SurveyTypes.SummaryAnalysisForm2;
		public override string SurveyName => "Faculty Course Review Report";
		public override string SurveyDescription => "Summary & Analysis";
		public static class Versions
		{
			public static Guid Version1 => new Guid("EC815563-D875-4D95-B141-524A9C439C18");

			public static List<Guid> GetVersions()
			{
				return new List<Guid>
				{
					Version1
				};
			}
		}
	}
	public sealed class SummaryAnalysisForm2Version1 : SummaryAnalysisForm2
	{
		public override Guid SchemaGuid => Versions.Version1;
		public string Q1 { get; set; }
		public string Q2 { get; set; }
		public string Q3 { get; set; }
		public string Q3p1 { get; set; }
		public string Q3p2 { get; set; }
		public string Q3p3 { get; set; }
		public string Q3p4 { get; set; }
		public override IEnumerable<string> ValidateValues()
		{
			if (this.Q1 == null)
				yield return "Question 1 is missing.";
			if (this.Q2 == null)
				yield return "Question 2 is missing.";
			if (this.Q3 == null)
				yield return "Question 3 is missing.";
			if (this.Q3p1 == null)
				yield return "Question 3 part 1 is missing.";
			if (this.Q3p2 == null)
				yield return "Question 3 part 2 is missing.";
			if (this.Q3p3 == null)
				yield return "Question 3 part 3 is missing.";
			if (this.Q3p4 == null)
				yield return "Question 3 part 4 is missing.";
		}
	}
}
