﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspire.BL.Core.QualityAssurance.Common.Schemas.SummaryAnalysis
{
	public abstract class SummaryAnalysisForm6: BaseSurvey
	{
		public override Guid SurveyTypeGuid => SurveyTypes.SummaryAnalysisForm6;
		public override string SurveyName => "SURVEY OF DEPARTMENT OFFERING Ph.D. PROGRAMS";
		public override string SurveyDescription => "Summary & Analysis";

		public static class Versions
		{
			public static Guid Version1 => new Guid("2E780A4C-F392-459F-B23B-A814A75C3E67");

			public static List<Guid> GetVersions()
			{
				return new List<Guid>
				{
					Version1
				};
			}
		}
	}

	public sealed class SummaryAnalysisForm6Version1 : SummaryAnalysisForm6
	{
		public override Guid SchemaGuid => Versions.Version1;
		public string Q1 { get; set; }
		public string Q2 { get; set; }
		public string Q3 { get; set; }
		public string Q3p1 { get; set; }
		public string Q3p2 { get; set; }
		public string Q3p3 { get; set; }
		public string Q3p4 { get; set; }
		public string Q3p5 { get; set; }
		public string Q3p6 { get; set; }

		public override IEnumerable<string> ValidateValues()
		{
			if (this.Q1 == null)
				yield return "Question 1 is missing.";
			if (this.Q2 == null)
				yield return "Question 2 is missing.";
			if (this.Q3 == null)
				yield return "Question 3 is missing.";
			if (this.Q3p1 == null)
				yield return "Question 3 part 1 is missing.";
			if (this.Q3p2 == null)
				yield return "Question 3 part 2 is missing.";
			if (this.Q3p3 == null)
				yield return "Question 3 part 3 is missing.";
			if (this.Q3p4 == null)
				yield return "Question 3 part 4 is missing.";
		}
	}
}
