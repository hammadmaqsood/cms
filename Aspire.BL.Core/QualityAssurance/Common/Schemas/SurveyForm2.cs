using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspire.BL.Core.QualityAssurance.Common.Schemas
{
	public abstract class SurveyForm2 : BaseSurvey
	{
		public override Guid SurveyTypeGuid => SurveyTypes.SurveyForm2;

		public override string SurveyName => "Faculty Survey";

		public override string SurveyDescription => "Faculty Course Review Report (Survey Form-2)";

		public static class Versions
		{
			public static Guid Version1 => new Guid("8A28E350-2DE4-4304-8851-DACA1F482130");

			public static List<Guid> GetVersions()
			{
				return new List<Guid>
				{
					Version1
				};
			}
		}
	}
}
