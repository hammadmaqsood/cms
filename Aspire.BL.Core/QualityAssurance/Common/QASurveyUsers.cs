﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.QualityAssurance.Common
{
	public static class QASurveyUsers
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, StaffPermissions.QualityAssurance qualityAssurance, UserGroupPermission.PermissionValues[] permissionValues, int instituteID, int? departmentID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, qualityAssurance, permissionValues, instituteID, departmentID, null, null);
		}

		public static List<CustomListItem> GetAcademicRankList()
		{
			return new List<CustomListItem>
			{
				new CustomListItem
				{
					ID = 1,
					Text = "Professor"
				},
				new CustomListItem
				{
					ID = 2,
					Text = "Associate Professor"
				},
				new CustomListItem
				{
					ID = 3,
					Text = "Assistant Professor"
				},
				new CustomListItem
				{
					ID = 4,
					Text = "Lecturer"
				},
				new CustomListItem
				{
					ID = 5,
					Text = "Lab Engineer/ Junior Lecturer"
				}
			};
		}

		public static List<CustomListItem> GetYearOfServiceList()
		{
			return new List<CustomListItem>
			{
				new CustomListItem
				{
					ID = 1,
					Text = "1-5"
				},
				new CustomListItem
				{
					ID = 2,
					Text = "6-10"
				},
				new CustomListItem
				{
					ID = 3,
					Text = "11-15"
				},
				new CustomListItem
				{
					ID = 4,
					Text = "16-20"
				},
				new CustomListItem
				{
					ID = 5,
					Text = "> 20"
				},
			};
		}

		public static List<CustomListItem> GetPhdExams()
		{
			return new List<CustomListItem>
			{
				new CustomListItem
				{
					ID = 1,
					Text = "Qualifying Examination"
				},
				new CustomListItem
				{
					ID = 2,
					Text = "Comprehensive Examination"
				},
			};
		}
		public static List<CustomListItem> GetSurveyOptionList()
		{
			return new List<CustomListItem>
			{
				new CustomListItem()
				{
					ID = 1,
					Text = "Very Satisfied"
				},
				new CustomListItem
				{
					ID = 2,
					Text = "Satisfied"
				},
				new CustomListItem
				{
					ID = 3,
					Text = "Uncertain"
				},
				new CustomListItem()
				{
					ID = 4,
					Text = "Dissatisfied"
				},
				new CustomListItem()
				{
					ID = 5,
					Text = "Very dissatisfied"
				},
			};
		}

		public class CoursePrequisite
		{
			public string CourseCode1 { get; internal set; }
			public string CourseCode2 { get; internal set; }
			public string CourseCode3 { get; internal set; }
			public string Title1 { get; internal set; }
			public string Title2 { get; internal set; }
			public string Title3 { get; internal set; }
			public string Prerequisites => CourseCode1 + " " + Title1 + CourseCode2 + " " + Title2 + CourseCode3 + " " + Title3;
		}
		public class FinalGrades
		{
			public int GradeACount { get; internal set; }
			public int GradeAMinusCount { get; internal set; }
			public int GradeBPlusCount { get; internal set; }
			public int GradeBCount { get; internal set; }
			public int GradeBMinusCount { get; internal set; }
			public int GradeCPlusCount { get; internal set; }
			public int GradeCCount { get; internal set; }
			public int GradeCMinusCount { get; internal set; }
			public int GradeDPlusCount { get; internal set; }
			public int GradeDCount { get; internal set; }
			public int GradeFCount { get; internal set; }
			public int GradeWCount { get; internal set; }
			public int NoGradeCount { get; internal set; }
			public int TotalGradeCount => GradeACount + GradeAMinusCount + GradeBPlusCount + GradeBCount + GradeCMinusCount + GradeCPlusCount
				+ GradeCCount + GradeCMinusCount + GradeDPlusCount + GradeDCount + GradeFCount + GradeWCount + NoGradeCount;
		}
		public sealed class GetSurveyUserResult
		{
			internal GetSurveyUserResult() { }
			public QASurveyUser QASurveyUser { get; internal set; }
			public DateTime StartDate { get; internal set; }
			public DateTime? EndDate { get; internal set; }
			public QASurveyConduct.Statuses SurveyStatusEnum { get; internal set; }
			public int InstituteID { get; internal set; } 
			public int? DepartmentID { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public string DepartmentAlias { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public DateTime? ProgramStartDate { get; internal set; }
			public short MaxSemesterID { get; internal set; }
			public string ProgramDurationFullName => GetMaxPhdYears();
			public string FacultyName { get; internal set; }
			public string AlumniStudentName { get; internal set; }
			public string EmployerName { get; internal set; }
			public string CourseCode { get; internal set; }
			public string CourseTitle { get; internal set; }
			public byte? CourseCategory { get; internal set; }
			public string CourseCategoryFullName => ((CourseCategories)this.CourseCategory).ToFullName();
			public int? NoOfApplicants { get; internal set; }
			public int? NoOfStudents { get; internal set; }
			public string StudentsToApplicantstRatio => this.NoOfStudents + ":" + this.NoOfApplicants;
			public short IntakeSemesterID { get; internal set; }
			public string IntakeSemester => this.IntakeSemesterID.ToSemesterString();
			public decimal? CreditHoursCompleted { get; internal set; }
			public DateTime ResearchInitiationDate { get; internal set; }
			public decimal? CGPA { get; internal set; }
			public string StringCGPA => this.CGPA?.ToString("0.##") ?? string.Empty.ToNAIfNullOrEmpty();
			public byte Shift { get; internal set; }
			public Shifts ShiftEnum => (Shifts)this.Shift;
			public short SemesterID { get; internal set; }
			public string SemesterIDEnum => this.SemesterID.ToSemesterString();
			public decimal CreditHours { get; internal set; }
			public byte DegreeLevel { get; internal set; }
			public decimal LectureContactHours { get; internal set; }
			public int? CourseAssignments { get; internal set; }
			public int? CourseQuizzes { get; internal set; }
			public int? CourseInternals { get; internal set; }
			public int? CourseMid { get; internal set; }
			public int? CourseFinal { get; internal set; }
			public int? LabFinal { get; internal set; }
			public int? NoOfQuizzes { get; internal set; }
			public int? NoOfAssignments { get; internal set; }
			public DegreeLevels DegreeLevelEnum => (DegreeLevels)this.DegreeLevel;
			public List<CoursePrequisite> Prerequisites { get; set; }
			public FinalGrades Grades { get; set; }
			public bool FacultyMemberCanEdit => this.QASurveyUser.StatusEnum != QASurveyUser.Statuses.Completed
												&& this.SurveyStatusEnum == QASurveyConduct.Statuses.Active
												&& this.StartDate <= DateTime.Today
												&& (this.EndDate == null || this.EndDate.Value.Date >= DateTime.Today);

			public bool StudentCanEdit => this.QASurveyUser.StatusEnum != QASurveyUser.Statuses.Completed
										&& this.SurveyStatusEnum == QASurveyConduct.Statuses.Active
										&& this.StartDate <= DateTime.Today
										&& (this.EndDate == null || this.EndDate.Value.Date >= DateTime.Today);

			public bool StaffCanEdit => this.QASurveyUser.StatusEnum != QASurveyUser.Statuses.Completed
										&& this.SurveyStatusEnum == QASurveyConduct.Statuses.Active
										&& this.StartDate <= DateTime.Today
										&& (this.EndDate == null || this.EndDate.Value.Date >= DateTime.Today);
			public bool AlumniCanEdit => this.QASurveyUser.StatusEnum != QASurveyUser.Statuses.Completed
										&& this.SurveyStatusEnum == QASurveyConduct.Statuses.Active
										&& this.StartDate <= DateTime.Today
										&& (this.EndDate == null || this.EndDate.Value.Date >= DateTime.Today);
			public bool EmployerCanEdit => this.QASurveyUser.StatusEnum != QASurveyUser.Statuses.Completed
										&& this.SurveyStatusEnum == QASurveyConduct.Statuses.Active
										&& this.StartDate <= DateTime.Today
										&& (this.EndDate == null || this.EndDate.Value.Date >= DateTime.Today);
			private string GetMaxPhdYears()
			{
				int startYear = Semester.GetYear(IntakeSemesterID);
				int endYear = Semester.GetYear(MaxSemesterID);
				SemesterTypes startSemester = Semester.GetSemesterType(IntakeSemesterID);
				SemesterTypes endSemester = Semester.GetSemesterType(MaxSemesterID);
				double maxYears = endYear - startYear;
				if (startSemester == SemesterTypes.Spring && endSemester == SemesterTypes.Fall)
					maxYears += 1;
				else
					if (startSemester == SemesterTypes.Spring && endSemester == SemesterTypes.Spring)
					maxYears += 0.5;
				else
					if (startSemester == SemesterTypes.Fall && endSemester == SemesterTypes.Fall)
					maxYears += 0.5;
				return maxYears + " Years";
			}
		}

		public static GetSurveyUserResult GetSurvey2User(int qaSurveyUserID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSurveyUserResult = aspireContext.QASurveyUsers
					.Where(qasu => qasu.QASurveyUserID == qaSurveyUserID)
					.Select(qsu => new GetSurveyUserResult
					{
						QASurveyUser = qsu,
						InstituteID = qsu.FacultyMember.InstituteID,
						DepartmentID = qsu.FacultyMember.DepartmentID,
						DepartmentAlias = qsu.FacultyMember.Department.DepartmentName,
						FacultyMemberName = qsu.FacultyMember.Name,
						StartDate = qsu.QASurveyConduct.StartDate,
						EndDate = qsu.QASurveyConduct.EndDate,
						FacultyName = qsu.FacultyMember.Department.FacultyName,
						CourseCode = qsu.OfferedCours.Cours.CourseCode,
						CourseTitle = qsu.OfferedCours.Cours.Title,
						Shift = qsu.OfferedCours.Shift,
						SemesterID = qsu.OfferedCours.SemesterID,
						CreditHours = qsu.OfferedCours.Cours.CreditHours,
						CourseCategory = qsu.OfferedCours.Cours.CourseCategory,
						NoOfAssignments = qsu.OfferedCours.OfferedCourseExams.Where(oce => oce.ExamType == (byte)OfferedCourseExam.ExamTypes.Assignment).Max(oce => oce.ExamNo),
						NoOfQuizzes = qsu.OfferedCours.OfferedCourseExams.Where(oce => oce.ExamType == (byte)OfferedCourseExam.ExamTypes.Quiz).Max(oce => oce.ExamNo),
						CourseAssignments = qsu.OfferedCours.Cours.AdmissionOpenProgram.AdmissionOpenProgramExamMarksPolicies.Where(aopemp => aopemp.SemesterID == qsu.OfferedCours.SemesterID).FirstOrDefault().ExamMarksPolicy.CourseAssignments,
						CourseQuizzes = qsu.OfferedCours.Cours.AdmissionOpenProgram.AdmissionOpenProgramExamMarksPolicies.Where(aopemp => aopemp.SemesterID == qsu.OfferedCours.SemesterID).FirstOrDefault().ExamMarksPolicy.CourseQuizzes,
						CourseInternals = qsu.OfferedCours.Cours.AdmissionOpenProgram.AdmissionOpenProgramExamMarksPolicies.Where(aopemp => aopemp.SemesterID == qsu.OfferedCours.SemesterID).FirstOrDefault().ExamMarksPolicy.CourseInternals,
						CourseMid = qsu.OfferedCours.Cours.AdmissionOpenProgram.AdmissionOpenProgramExamMarksPolicies.Where(aopemp => aopemp.SemesterID == qsu.OfferedCours.SemesterID).FirstOrDefault().ExamMarksPolicy.CourseMid,
						CourseFinal = qsu.OfferedCours.Cours.AdmissionOpenProgram.AdmissionOpenProgramExamMarksPolicies.Where(aopemp => aopemp.SemesterID == qsu.OfferedCours.SemesterID).FirstOrDefault().ExamMarksPolicy.CourseFinal,
						LabFinal = qsu.OfferedCours.Cours.AdmissionOpenProgram.AdmissionOpenProgramExamMarksPolicies.Where(aopemp => aopemp.SemesterID == qsu.OfferedCours.SemesterID).FirstOrDefault().ExamMarksPolicy.LabFinal,
						DegreeLevel = qsu.OfferedCours.Cours.AdmissionOpenProgram.Program.DegreeLevel,
						LectureContactHours = qsu.OfferedCours.OfferedCourseAttendances.Sum(a => a.Hours),
						Prerequisites = qsu.OfferedCours.Cours.CoursePreReqs.Select(cprereq =>
						new CoursePrequisite
						{
							CourseCode1 = cprereq.Cours1.CourseCode,
							Title1 = cprereq.Cours1.Title,
							CourseCode2 = cprereq.Cours2.CourseCode,
							Title2 = cprereq.Cours2.Title,
							CourseCode3 = cprereq.Cours3.CourseCode,
							Title3 = cprereq.Cours3.Title,
						}).ToList(),
						SurveyStatusEnum = (QASurveyConduct.Statuses)qsu.QASurveyConduct.SurveyStatus,
						Grades = new FinalGrades
						{
							GradeACount = qsu.OfferedCours.RegisteredCourses.Count(rc => (rc.Grade == (byte)ExamGrades.A) && rc.DeletedDate == null),
							GradeAMinusCount = qsu.OfferedCours.RegisteredCourses.Count(rc => (rc.Grade == (byte)ExamGrades.AMinus) && rc.DeletedDate == null),
							GradeBPlusCount = qsu.OfferedCours.RegisteredCourses.Count(rc => (rc.Grade == (byte)ExamGrades.BPlus) && rc.DeletedDate == null),
							GradeBCount = qsu.OfferedCours.RegisteredCourses.Count(rc => (rc.Grade == (byte)ExamGrades.B) && rc.DeletedDate == null),
							GradeBMinusCount = qsu.OfferedCours.RegisteredCourses.Count(rc => (rc.Grade == (byte)ExamGrades.BMinus) && rc.DeletedDate == null),
							GradeCPlusCount = qsu.OfferedCours.RegisteredCourses.Count(rc => (rc.Grade == (byte)ExamGrades.CPlus) && rc.DeletedDate == null),
							GradeCCount = qsu.OfferedCours.RegisteredCourses.Count(rc => (rc.Grade == (byte)ExamGrades.C) && rc.DeletedDate == null),
							GradeCMinusCount = qsu.OfferedCours.RegisteredCourses.Count(rc => (rc.Grade == (byte)ExamGrades.CMinus) && rc.DeletedDate == null),
							GradeDPlusCount = qsu.OfferedCours.RegisteredCourses.Count(rc => (rc.Grade == (byte)ExamGrades.DPlus) && rc.DeletedDate == null),
							GradeDCount = qsu.OfferedCours.RegisteredCourses.Count(rc => (rc.Grade == (byte)ExamGrades.D) && rc.DeletedDate == null),
							GradeFCount = qsu.OfferedCours.RegisteredCourses.Count(rc => (rc.Grade == (byte)ExamGrades.F) && rc.DeletedDate == null),
							GradeWCount = qsu.OfferedCours.RegisteredCourses.Count(rc => (rc.Status == (byte)RegisteredCours.Statuses.WithdrawWithFee || rc.Status == (byte)RegisteredCours.Statuses.WithdrawWithoutFee) && rc.DeletedDate == null),
							NoGradeCount = qsu.OfferedCours.RegisteredCourses.Count(rc => (rc.Status == (byte)RegisteredCours.Statuses.AttendanceDefaulter || rc.Status == (byte)RegisteredCours.Statuses.Deferred || rc.Status == (byte)RegisteredCours.Statuses.Incomplete
							|| rc.Status == (byte)RegisteredCours.Statuses.UnfairMeans || rc.FreezedStatus == (byte)FreezedStatuses.FreezedWithFee || rc.FreezedStatus == (byte)FreezedStatuses.FreezedWithoutFee) && rc.DeletedDate == null),
						},
					}).SingleOrDefault();
				if (getSurveyUserResult == null)
					return null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.SurveyForm2
, new[] { UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Readonly, }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						return getSurveyUserResult;
					case UserTypes.Faculty:
						if (getSurveyUserResult.QASurveyUser.FacultyMemberID == loginHistory.FacultyMemberID.Value)
							return getSurveyUserResult;
						return null;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
		public static GetSurveyUserResult GetSurvey4User(int qaSurveyUserID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSurveyUserResult = (from stu in aspireContext.Students
										  join qsu in aspireContext.QASurveyUsers on stu.StudentID equals qsu.StudentID
										  where qsu.QASurveyUserID == qaSurveyUserID
										  select new GetSurveyUserResult
										  {
											  QASurveyUser = qsu,
											  InstituteID = stu.AdmissionOpenProgram.Program.InstituteID,
											  DepartmentID = stu.AdmissionOpenProgram.Program.DepartmentID,
											  StartDate = qsu.QASurveyConduct.StartDate,
											  EndDate = qsu.QASurveyConduct.EndDate,
											  IntakeSemesterID = stu.AdmissionOpenProgram.SemesterID,
											  ResearchInitiationDate = stu.RegisteredCourses.Where(rc => rc.Cours.CourseCategory == (byte)CourseCategories.Thesis && rc.DeletedDate == null).OrderBy(rc => rc.RegistrationDate).FirstOrDefault().RegistrationDate,
											  CreditHoursCompleted = stu.RegisteredCourses.Where(rc => rc.DeletedDate == null && rc.Grade != null && rc.Grade != (byte)ExamGrades.F).Sum(rc => rc.Cours.CreditHours),
											  CGPA = stu.StudentSemesters.Where(ss => ss.SemesterID < qsu.QASurveyConduct.SemesterID).OrderByDescending(ss => ss.CGPA).FirstOrDefault().CGPA,
											  SurveyStatusEnum = (QASurveyConduct.Statuses)qsu.QASurveyConduct.SurveyStatus,
										  }).SingleOrDefault();
				if (getSurveyUserResult == null)
					return null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.SurveyForm4, new[] { UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Readonly, }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						return getSurveyUserResult;
					case UserTypes.Student:
						if (getSurveyUserResult.QASurveyUser.StudentID == loginHistory.StudentID.Value)
							return getSurveyUserResult;
						return null;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}

		}
		public static GetSurveyUserResult GetSurvey5User(int qaSurveyUserID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSurveyUserResult = aspireContext.QASurveyUsers
					.Where(qasu => qasu.QASurveyUserID == qaSurveyUserID)
					.Select(qsu => new GetSurveyUserResult
					{
						QASurveyUser = qsu,
						InstituteID = qsu.FacultyMember.InstituteID,
						DepartmentID = qsu.FacultyMember.DepartmentID,
						StartDate = qsu.QASurveyConduct.StartDate,
						EndDate = qsu.QASurveyConduct.EndDate,
						SurveyStatusEnum = (QASurveyConduct.Statuses)qsu.QASurveyConduct.SurveyStatus,
					}).SingleOrDefault();
				if (getSurveyUserResult == null)
					return null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.SurveyForm5, new[] { UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Readonly, }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						return getSurveyUserResult;
					case UserTypes.Faculty:
						if (getSurveyUserResult.QASurveyUser.FacultyMemberID == loginHistory.FacultyMemberID.Value)
							return getSurveyUserResult;
						return null;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		public static GetSurveyUserResult GetSurvey6User(int qaSurveyUserID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var candidateAppliedPrograms = aspireContext.CandidateAppliedPrograms;
				var StudentRegistered = aspireContext.Students;
				var getSurveyUserResult = aspireContext.QASurveyUsers
					.Where(qasu => qasu.QASurveyUserID == qaSurveyUserID)
					.Select(qsu => new GetSurveyUserResult
					{
						QASurveyUser = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID.Value,
						DepartmentID = qsu.QASurveyConduct.DepartmentID,
						DepartmentAlias = qsu.QASurveyConduct.Department.DepartmentAlias,
						ProgramAlias = qsu.QASurveyConduct.Program.ProgramAlias,
						FacultyName = qsu.QASurveyConduct.Department.FacultyName,
						ProgramStartDate = qsu.QASurveyConduct.Program.InitiatedDate,
						NoOfStudents = StudentRegistered.Where(sr => sr.AdmissionOpenProgram.Program.InstituteID == qsu.QASurveyConduct.InstituteID && sr.AdmissionOpenProgram.SemesterID == qsu.QASurveyConduct.SemesterID && sr.AdmissionOpenProgram.ProgramID == qsu.QASurveyConduct.ProgramID).Count(),
						NoOfApplicants = candidateAppliedPrograms.Where(cap => cap.AdmissionOpenProgram1.Program.InstituteID == qsu.QASurveyConduct.InstituteID && cap.AccountTransaction.TransactionDate != null && cap.AdmissionOpenProgram1.SemesterID == qsu.QASurveyConduct.SemesterID && cap.AdmissionOpenProgram1.ProgramID == qsu.QASurveyConduct.ProgramID).Count(),
						IntakeSemesterID = qsu.QASurveyConduct.Program.AdmissionOpenPrograms.OrderByDescending(aop => aop.SemesterID).FirstOrDefault().SemesterID,
						MaxSemesterID = qsu.QASurveyConduct.Program.AdmissionOpenPrograms.OrderByDescending(aop => aop.SemesterID).FirstOrDefault().MaxSemesterID,
						StartDate = qsu.QASurveyConduct.StartDate,
						EndDate = qsu.QASurveyConduct.EndDate,
						SurveyStatusEnum = (QASurveyConduct.Statuses)qsu.QASurveyConduct.SurveyStatus,
					}).SingleOrDefault();
				if (getSurveyUserResult == null)
					return null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.SurveyForm6, new[] { UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Readonly, }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						return getSurveyUserResult;
					case UserTypes.Faculty:
						if (getSurveyUserResult.QASurveyUser.FacultyMemberID == loginHistory.FacultyMemberID.Value)
							return getSurveyUserResult;
						return null;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		public static GetSurveyUserResult GetSurvey7StaffUser(int qaSurveyUserID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSurveyUserResult = (from stu in aspireContext.Students
										   join qsu in aspireContext.QASurveyUsers on stu.StudentID equals qsu.AlumniStudentID
										   where qsu.QASurveyUserID == qaSurveyUserID
										   select new GetSurveyUserResult
										   {
											   QASurveyUser = qsu,
											   InstituteID = stu.AdmissionOpenProgram.Program.InstituteID,
											   DepartmentID = stu.AdmissionOpenProgram.Program.DepartmentID,
											   StartDate = qsu.QASurveyConduct.StartDate,
											   EndDate = qsu.QASurveyConduct.EndDate,
											   AlumniStudentName = qsu.Student.Name,
											   SurveyStatusEnum = (QASurveyConduct.Statuses)qsu.QASurveyConduct.SurveyStatus,
										   }).SingleOrDefault();
				if (getSurveyUserResult == null)
					return null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.SurveyForm7, new[] { UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Readonly, }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						return getSurveyUserResult;
					case UserTypes.Alumni:
						if (getSurveyUserResult.QASurveyUser.AlumniStudentID == loginHistory.StudentID.Value)
							return getSurveyUserResult;
						return null;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
		public static GetSurveyUserResult GetSurvey7AlumniUser(int qaSurveyUserID)
		{
			using (var aspireContext = new AspireContext())
			{
				var getSurveyUserResult = (from stu in aspireContext.Students
										   join qsu in aspireContext.QASurveyUsers on stu.StudentID equals qsu.AlumniStudentID
										   where qsu.QASurveyUserID == qaSurveyUserID
										   select new GetSurveyUserResult
										   {
											   QASurveyUser = qsu,
											   InstituteID = stu.AdmissionOpenProgram.Program.InstituteID,
											   DepartmentID = stu.AdmissionOpenProgram.Program.DepartmentID,
											   StartDate = qsu.QASurveyConduct.StartDate,
											   EndDate = qsu.QASurveyConduct.EndDate,
											   AlumniStudentName = qsu.Student.Name,
											   SurveyStatusEnum = (QASurveyConduct.Statuses)qsu.QASurveyConduct.SurveyStatus,
										   }).SingleOrDefault();
				if (getSurveyUserResult == null)
					return null;
				return getSurveyUserResult;
			}
		}
		public static GetSurveyUserResult GetSurvey8StaffUser(int qaSurveyUserID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSurveyUserResult = aspireContext.QASurveyUsers
					.Where(qasu => qasu.QASurveyUserID == qaSurveyUserID)
					.Select(qsu => new GetSurveyUserResult
					{
						QASurveyUser = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID.Value,
						EmployerName = qsu.Employer.Name,
						StartDate = qsu.QASurveyConduct.StartDate,
						EndDate = qsu.QASurveyConduct.EndDate,
						SurveyStatusEnum = (QASurveyConduct.Statuses)qsu.QASurveyConduct.SurveyStatus,
					}).SingleOrDefault();
				if (getSurveyUserResult == null)
					return null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.SurveyForm8, new[] { UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Readonly, }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						return getSurveyUserResult;
					case UserTypes.Anonymous:
						if (getSurveyUserResult.QASurveyUser.EmployerID == loginHistory.UserRoleID.Value)
							return getSurveyUserResult;
						return null;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
		public static GetSurveyUserResult GetSurvey8EmployerUser(int qaSurveyUserID)
		{
			using (var aspireContext = new AspireContext())
			{
				var getSurveyUserResult = aspireContext.QASurveyUsers
					.Where(qasu => qasu.QASurveyUserID == qaSurveyUserID)
					.Select(qsu => new GetSurveyUserResult
					{
						QASurveyUser = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID.Value,
						EmployerName = qsu.Employer.Name,
						StartDate = qsu.QASurveyConduct.StartDate,
						EndDate = qsu.QASurveyConduct.EndDate,
						SurveyStatusEnum = (QASurveyConduct.Statuses)qsu.QASurveyConduct.SurveyStatus,
					}).SingleOrDefault();
				if (getSurveyUserResult == null)
					return null;
				return getSurveyUserResult;
			}
		}

		public enum UpdateSurveyValuesEnum
		{
			NoRecordFound,
			FacultyMemberCannotEditThisRecord,
			StudentCannotEditThisRecord,
			StaffCannotEditThisRecord,
			AlumniCannotEditThisRecord,
			EmployerCannotEditThisRecord,
			Success
		}
		public static UpdateSurveyValuesEnum UpdateSurvey2Values(int qaSurveyUserID, string surveyValues, QASurveyUser.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSurveyUserResult = aspireContext.QASurveyUsers
					.Where(qasu => qasu.QASurveyUserID == qaSurveyUserID)
					.Select(qsu => new GetSurveyUserResult
					{
						QASurveyUser = qsu,
						InstituteID = qsu.FacultyMember.InstituteID,
						DepartmentID = qsu.FacultyMember.DepartmentID,
						StartDate = qsu.QASurveyConduct.StartDate,
						EndDate = qsu.QASurveyConduct.EndDate,
						SurveyStatusEnum = (QASurveyConduct.Statuses)qsu.QASurveyConduct.SurveyStatus,
					}).SingleOrDefault();
				if (getSurveyUserResult == null)
					return UpdateSurveyValuesEnum.NoRecordFound;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.SurveyForm2, new[] { UserGroupPermission.PermissionValues.Allowed }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						break;
					case UserTypes.Faculty:
						if (getSurveyUserResult.QASurveyUser.FacultyMemberID != loginHistory.FacultyMemberID.Value)
							return UpdateSurveyValuesEnum.NoRecordFound;
						if (!getSurveyUserResult.FacultyMemberCanEdit)
							return UpdateSurveyValuesEnum.FacultyMemberCannotEditThisRecord;
						break;
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Candidate:
					case UserTypes.Student:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
				switch (getSurveyUserResult.QASurveyUser.StatusEnum)
				{
					case QASurveyUser.Statuses.LinkNotSent:
						throw new InvalidOperationException();
					case QASurveyUser.Statuses.NotStarted:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.InProgress:
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveyUser.Statuses.InProgress:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.InProgress:
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveyUser.Statuses.Completed:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
							case QASurveyUser.Statuses.InProgress:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				getSurveyUserResult.QASurveyUser.StatusEnum = statusEnum;
				getSurveyUserResult.QASurveyUser.SurveyValues = surveyValues;
				getSurveyUserResult.QASurveyUser.SubmissionDate = DateTime.Now;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateSurveyValuesEnum.Success;
			}
		}
		public static UpdateSurveyValuesEnum UpdateSurveyV4Alues(int qaSurveyUserID, string surveyValues, QASurveyUser.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSurveyUserResult = aspireContext.QASurveyUsers
					.Where(qasu => qasu.QASurveyUserID == qaSurveyUserID)
					.Select(qsu => new GetSurveyUserResult
					{
						QASurveyUser = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID.Value,
						DepartmentID = qsu.Student.AdmissionOpenProgram.Program.Department.DepartmentID,
						StartDate = qsu.QASurveyConduct.StartDate,
						EndDate = qsu.QASurveyConduct.EndDate,
						SurveyStatusEnum = (QASurveyConduct.Statuses)qsu.QASurveyConduct.SurveyStatus,
					}).SingleOrDefault();
				if (getSurveyUserResult == null)
					return UpdateSurveyValuesEnum.NoRecordFound;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.SurveyForm4, new[] { UserGroupPermission.PermissionValues.Allowed }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						break;
					case UserTypes.Student:
						if (getSurveyUserResult.QASurveyUser.StudentID != loginHistory.StudentID.Value)
							return UpdateSurveyValuesEnum.NoRecordFound;
						if (!getSurveyUserResult.StudentCanEdit)
							return UpdateSurveyValuesEnum.StudentCannotEditThisRecord;
						break;
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Candidate:
					case UserTypes.Faculty:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
				switch (getSurveyUserResult.QASurveyUser.StatusEnum)
				{
					case QASurveyUser.Statuses.LinkNotSent:
						throw new InvalidOperationException();
					case QASurveyUser.Statuses.NotStarted:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.InProgress:
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveyUser.Statuses.InProgress:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.InProgress:
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveyUser.Statuses.Completed:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
							case QASurveyUser.Statuses.InProgress:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				getSurveyUserResult.QASurveyUser.StatusEnum = statusEnum;
				getSurveyUserResult.QASurveyUser.SurveyValues = surveyValues;
				getSurveyUserResult.QASurveyUser.SubmissionDate = DateTime.Now;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateSurveyValuesEnum.Success;
			}
		}

		public static UpdateSurveyValuesEnum UpdateSurvey5Values(int qaSurveyUserID, string surveyValues, QASurveyUser.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSurveyUserResult = aspireContext.QASurveyUsers
					.Where(qasu => qasu.QASurveyUserID == qaSurveyUserID)
					.Select(qsu => new GetSurveyUserResult
					{
						QASurveyUser = qsu,
						InstituteID = qsu.FacultyMember.InstituteID,
						DepartmentID = qsu.FacultyMember.DepartmentID,
						StartDate = qsu.QASurveyConduct.StartDate,
						EndDate = qsu.QASurveyConduct.EndDate,
						SurveyStatusEnum = (QASurveyConduct.Statuses)qsu.QASurveyConduct.SurveyStatus,
					}).SingleOrDefault();
				if (getSurveyUserResult == null)
					return UpdateSurveyValuesEnum.NoRecordFound;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.SurveyForm5, new[] { UserGroupPermission.PermissionValues.Allowed }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						break;
					case UserTypes.Faculty:
						if (getSurveyUserResult.QASurveyUser.FacultyMemberID != loginHistory.FacultyMemberID.Value)
							return UpdateSurveyValuesEnum.NoRecordFound;
						if (!getSurveyUserResult.FacultyMemberCanEdit)
							return UpdateSurveyValuesEnum.FacultyMemberCannotEditThisRecord;
						break;
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Candidate:
					case UserTypes.Student:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
				switch (getSurveyUserResult.QASurveyUser.StatusEnum)
				{
					case QASurveyUser.Statuses.LinkNotSent:
						throw new InvalidOperationException();
					case QASurveyUser.Statuses.NotStarted:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.InProgress:
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveyUser.Statuses.InProgress:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.InProgress:
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveyUser.Statuses.Completed:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
							case QASurveyUser.Statuses.InProgress:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				getSurveyUserResult.QASurveyUser.StatusEnum = statusEnum;
				getSurveyUserResult.QASurveyUser.SurveyValues = surveyValues;
				getSurveyUserResult.QASurveyUser.SubmissionDate = DateTime.Now;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateSurveyValuesEnum.Success;
			}
		}

		public static UpdateSurveyValuesEnum UpdateSurvey6Values(int qaSurveyUserID, string surveyValues, QASurveyUser.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getSurveyUserResult = aspireContext.QASurveyUsers
					.Where(qasu => qasu.QASurveyUserID == qaSurveyUserID)
					.Select(qsu => new GetSurveyUserResult
					{
						QASurveyUser = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID.Value,
						DepartmentID = qsu.QASurveyConduct.DepartmentID,
						StartDate = qsu.QASurveyConduct.StartDate,
						EndDate = qsu.QASurveyConduct.EndDate,
						SurveyStatusEnum = (QASurveyConduct.Statuses)qsu.QASurveyConduct.SurveyStatus,
					}).SingleOrDefault();
				if (getSurveyUserResult == null)
					return UpdateSurveyValuesEnum.NoRecordFound;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.SurveyForm6, new[] { UserGroupPermission.PermissionValues.Allowed }, getSurveyUserResult.InstituteID, getSurveyUserResult.DepartmentID);
						if (!getSurveyUserResult.StaffCanEdit)
							return UpdateSurveyValuesEnum.StaffCannotEditThisRecord;
						break;
					case UserTypes.Faculty:
						if (getSurveyUserResult.QASurveyUser.FacultyMemberID != loginHistory.FacultyMemberID.Value)
							return UpdateSurveyValuesEnum.NoRecordFound;
						if (!getSurveyUserResult.FacultyMemberCanEdit)
							return UpdateSurveyValuesEnum.FacultyMemberCannotEditThisRecord;
						break;
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Candidate:
					case UserTypes.Student:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
				switch (getSurveyUserResult.QASurveyUser.StatusEnum)
				{
					case QASurveyUser.Statuses.LinkNotSent:
						throw new InvalidOperationException();
					case QASurveyUser.Statuses.NotStarted:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.InProgress:
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveyUser.Statuses.InProgress:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.InProgress:
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveyUser.Statuses.Completed:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
							case QASurveyUser.Statuses.InProgress:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				getSurveyUserResult.QASurveyUser.StatusEnum = statusEnum;
				getSurveyUserResult.QASurveyUser.SurveyValues = surveyValues;
				getSurveyUserResult.QASurveyUser.SubmissionDate = DateTime.Now;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateSurveyValuesEnum.Success;
			}
		}

		public static UpdateSurveyValuesEnum UpdateSurvey7Values(int qaSurveyUserID, string surveyValues, QASurveyUser.Statuses statusEnum)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var getSurveyUserResult = aspireContext.QASurveyUsers
					.Where(qasu => qasu.QASurveyUserID == qaSurveyUserID)
					.Select(qsu => new GetSurveyUserResult
					{
						QASurveyUser = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID.Value,
						DepartmentID = qsu.QASurveyConduct.DepartmentID,
						StartDate = qsu.QASurveyConduct.StartDate,
						EndDate = qsu.QASurveyConduct.EndDate,
						SurveyStatusEnum = (QASurveyConduct.Statuses)qsu.QASurveyConduct.SurveyStatus,
					}).SingleOrDefault();
				if (getSurveyUserResult == null)
					return UpdateSurveyValuesEnum.NoRecordFound;
				if (!getSurveyUserResult.AlumniCanEdit)
					return UpdateSurveyValuesEnum.AlumniCannotEditThisRecord;
				switch (getSurveyUserResult.QASurveyUser.StatusEnum)
				{
					case QASurveyUser.Statuses.LinkNotSent:
						throw new InvalidOperationException();
					case QASurveyUser.Statuses.NotStarted:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.InProgress:
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveyUser.Statuses.InProgress:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.InProgress:
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveyUser.Statuses.Completed:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
							case QASurveyUser.Statuses.InProgress:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				getSurveyUserResult.QASurveyUser.StatusEnum = statusEnum;
				getSurveyUserResult.QASurveyUser.SurveyValues = surveyValues;
				getSurveyUserResult.QASurveyUser.SubmissionDate = DateTime.Now;
				aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
				return UpdateSurveyValuesEnum.Success;
			}
		}

		public static UpdateSurveyValuesEnum UpdateSurvey8Values(int qaSurveyUserID, string surveyValues, QASurveyUser.Statuses statusEnum)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var getSurveyUserResult = aspireContext.QASurveyUsers
					.Where(qasu => qasu.QASurveyUserID == qaSurveyUserID)
					.Select(qsu => new GetSurveyUserResult
					{
						QASurveyUser = qsu,
						InstituteID = qsu.QASurveyConduct.InstituteID.Value,
						DepartmentID = qsu.QASurveyConduct.DepartmentID,
						StartDate = qsu.QASurveyConduct.StartDate,
						EndDate = qsu.QASurveyConduct.EndDate,
						SurveyStatusEnum = (QASurveyConduct.Statuses)qsu.QASurveyConduct.SurveyStatus,
					}).SingleOrDefault();
				if (getSurveyUserResult == null)
					return UpdateSurveyValuesEnum.NoRecordFound;
				if (!getSurveyUserResult.EmployerCanEdit)
					return UpdateSurveyValuesEnum.EmployerCannotEditThisRecord;
				switch (getSurveyUserResult.QASurveyUser.StatusEnum)
				{
					case QASurveyUser.Statuses.LinkNotSent:
						throw new InvalidOperationException();
					case QASurveyUser.Statuses.NotStarted:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.InProgress:
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveyUser.Statuses.InProgress:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.InProgress:
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					case QASurveyUser.Statuses.Completed:
						switch (statusEnum)
						{
							case QASurveyUser.Statuses.LinkNotSent:
							case QASurveyUser.Statuses.NotStarted:
							case QASurveyUser.Statuses.InProgress:
								throw new InvalidOperationException();
							case QASurveyUser.Statuses.Completed:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
						}
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				getSurveyUserResult.QASurveyUser.StatusEnum = statusEnum;
				getSurveyUserResult.QASurveyUser.SurveyValues = surveyValues;
				getSurveyUserResult.QASurveyUser.SubmissionDate = DateTime.Now;
				aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
				return UpdateSurveyValuesEnum.Success;
			}
		}
	}
}
