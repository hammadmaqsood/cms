﻿using System;
using System.Linq;
using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.QualityAssurance.Staff.Surveys.SurveyForm6
{
	public static class QASummaryConducts
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.ManageSummarySurveyForm6, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public class Form6SummaryConduct
		{
			public int QASurveySummaryAnalysisID { get; set; }
			public string InstituteAlias { get; set; }
			public string DepartmentAlias { get; set; }
			public string ProgramAlias { get; set; }
			public Guid SummaryTypeGuid { get; internal set; }
			public short? SemesterID { get; set; }
			public string SurveyName => Common.Surveys.GetSurveyName(this.SummaryTypeGuid);
			public string Semester => this.SemesterID.ToSemesterString();
			public byte? Status { get; internal set; }
			public string StatusFullName => ((QASurveySummaryAnalysi.Statuses)this.Status).ToString().SplitCamelCasing();
		}

		public static QASurveySummaryAnalysi.ValidationResults AddQASummaryConduct(short semesterID, int instituteID, int departmentID, Guid surveyTypeGuid, Guid summaryTypeGuid, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, instituteID, null, null, null);
				if (aspireContext.QASurveySummaryAnalysis.Any(sc => sc.SemesterID == semesterID && sc.SummaryTypeGuid == summaryTypeGuid && sc.DepartmentID == departmentID))
					return QASurveySummaryAnalysi.ValidationResults.AlreadyExists;
				var qaSurveyConduct = aspireContext.QASurveyConducts.FirstOrDefault(sc => sc.SemesterID == semesterID && sc.InstituteID == instituteID && sc.DepartmentID == departmentID && sc.SurveyTypeGuid == surveyTypeGuid);
				if (qaSurveyConduct == null)
					return QASurveySummaryAnalysi.ValidationResults.NoRecordFound;
				var summaryConduct = new QASurveySummaryAnalysi
				{
					QASurveyConductID = qaSurveyConduct.QASurveyConductID,
					SemesterID = semesterID,
					SummaryTypeGuid = summaryTypeGuid,
					DepartmentID = departmentID,
					Status = (byte)QASurveySummaryAnalysi.Statuses.NotStarted,
					SubmittedByUserLoginHistoryID = loginHistory.UserLoginHistoryID,
				};
				aspireContext.QASurveySummaryAnalysis.Add(summaryConduct);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return QASurveySummaryAnalysi.ValidationResults.Success;
			}
		}

		public static QASurveySummaryAnalysi GetSummaryConduct(int summaryConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.QASurveySummaryAnalysis.SingleOrDefault(sc => sc.QASurveySummaryAnalysisID == summaryConductID && sc.Status == (byte)QASurveySummaryAnalysi.Statuses.NotStarted);
			}
		}

		public static List<Form6SummaryConduct> GetSummaryConducts(short? semesterID, Guid summaryTypeGuid, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = aspireContext.QASurveySummaryAnalysis.AsQueryable();
				if (loginHistory != null)
				{
					query = query.Where(q => q.QASurveyConduct.InstituteID == loginHistory.InstituteID);
					query = query.Where(q => q.UserLoginHistory.UserID == loginHistory.UserID);
				}
				if (semesterID != null)
					query = query.Where(q => q.SemesterID == semesterID);
				query = query.Where(q => q.SummaryTypeGuid == summaryTypeGuid);
				virtualItemCount = query.Count();
				switch (sortExpression)
				{
					case nameof(Form6SummaryConduct.SemesterID):
						query = query.OrderBy(sortDirection, q => q.SemesterID);
						break;
					case nameof(Form6SummaryConduct.DepartmentAlias):
						query = query.OrderBy(sortDirection, q => q.DepartmentID);
						break;
					case nameof(Form6SummaryConduct.ProgramAlias):
						query = query.OrderBy(sortDirection, q => q.ProgramID);
						break;
					case nameof(Form6SummaryConduct.StatusFullName):
						query = query.OrderBy(sortDirection, q => q.Status);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				query = query.Skip(pageIndex * pageSize).Take(pageSize);

				return query.Select(sc => new Form6SummaryConduct
				{
					QASurveySummaryAnalysisID = sc.QASurveySummaryAnalysisID,
					InstituteAlias = sc.QASurveyConduct.Institute.InstituteAlias,
					SummaryTypeGuid = sc.SummaryTypeGuid,
					SemesterID = sc.SemesterID,
					DepartmentAlias = sc.Department.DepartmentAlias,
					ProgramAlias = sc.Program.ProgramAlias,
					Status = sc.Status,
				}).ToList();
			}
		}

		public static QASurveySummaryAnalysi.ValidationResults DeleteSummaryConduct(int qaSurveySummaryAnalysisID, int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, instituteID, null, null, null);
				var summaryConduct = aspireContext.QASurveySummaryAnalysis.SingleOrDefault(sc => sc.QASurveySummaryAnalysisID == qaSurveySummaryAnalysisID);
				if (summaryConduct == null)
					return QASurveySummaryAnalysi.ValidationResults.NoRecordFound;
				if (summaryConduct.Status == QASurveyUser.StatusInProgress || summaryConduct.Status == QASurveyUser.StatusCompleted)
					return QASurveySummaryAnalysi.ValidationResults.SummaryGenerated;
				aspireContext.QASurveySummaryAnalysis.Remove(summaryConduct);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return QASurveySummaryAnalysi.ValidationResults.Success;
			}
		}

		public static QASurveySummaryAnalysi.ValidationResults UpdateSummaryConduct(int qaSurveySummaryAnalysisID, short semesterID, int departmentID, Guid surveyTypeGuid, Guid summaryTypeGuid, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var summaryConduct = aspireContext.QASurveySummaryAnalysis.SingleOrDefault(sc => sc.QASurveySummaryAnalysisID == qaSurveySummaryAnalysisID);
				if (summaryConduct == null)
					return QASurveySummaryAnalysi.ValidationResults.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, summaryConduct.QASurveyConduct.InstituteID.Value, summaryConduct.DepartmentID, null, null);
				if (aspireContext.QASurveySummaryAnalysis.Any(sc => sc.SemesterID == semesterID && sc.SummaryTypeGuid == summaryTypeGuid && sc.DepartmentID == departmentID))
					return QASurveySummaryAnalysi.ValidationResults.AlreadyExists;
				if (summaryConduct.Status == (byte)QASurveySummaryAnalysi.Statuses.InProgress || summaryConduct.Status == (byte)QASurveySummaryAnalysi.Statuses.Completed)
					return QASurveySummaryAnalysi.ValidationResults.SummaryGenerated;
				var qaSurveyConduct = aspireContext.QASurveyConducts.SingleOrDefault(sc => sc.SemesterID == semesterID && sc.InstituteID == summaryConduct.QASurveyConduct.InstituteID && sc.SurveyTypeGuid == surveyTypeGuid);
				if (qaSurveyConduct == null)
					return QASurveySummaryAnalysi.ValidationResults.NoRecordFound;
				summaryConduct.DepartmentID = departmentID;
				summaryConduct.SemesterID = semesterID;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return QASurveySummaryAnalysi.ValidationResults.Success;
			}
		}
	}
}
