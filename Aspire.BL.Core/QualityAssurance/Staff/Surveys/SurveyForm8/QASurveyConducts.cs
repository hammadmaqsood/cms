﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.QualityAssurance.Staff.Surveys.SurveyForm8
{
	public static class QASurveyConducts
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.ManageConductSurveyForm8, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public class Form8SurveyConduct
		{
			public int QASurveyConductID { get; set; }
			public Guid SurveyTypeGuid { get; internal set; }
			public short? SemesterID { get; set; }
			public string SurveyName => Common.Surveys.GetSurveyName(this.SurveyTypeGuid);
			public byte Type { get; set; }
			public DateTime OpenDate { get; set; }
			public DateTime? CloseDate { get; set; }
			public byte Status { get; set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public string StatusEnumFullName => ((QASurveyConduct.Statuses)this.Status).ToString();
		}

		public static QASurveyConduct GetSurveyConduct(int surveyConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.QASurveyConducts.SingleOrDefault(sc => sc.QASurveyConductID == surveyConductID);
			}
		}

		public static List<Form8SurveyConduct> GetSurveyConducts(short? semesterID, Guid pageGuid, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = aspireContext.QASurveyConducts.AsQueryable();
				if (loginHistory != null)
					query = query.Where(q => q.InstituteID == loginHistory.InstituteID);
				if (semesterID != null)
					query = query.Where(q => q.SemesterID == semesterID);
				query = query.Where(q => q.SurveyTypeGuid == pageGuid);
				virtualItemCount = query.Count();
				switch (sortExpression)
				{
					case nameof(Form8SurveyConduct.SemesterID):
						query = query.OrderBy(sortDirection, q => q.SemesterID);
						break;
					case nameof(Form8SurveyConduct.OpenDate):
						query = query.OrderBy(sortDirection, q => q.StartDate);
						break;
					case nameof(Form8SurveyConduct.CloseDate):
						query = query.OrderBy(sortDirection, q => q.EndDate);
						break;
					case nameof(Form8SurveyConduct.Status):
						query = query.OrderBy(sortDirection, q => q.SurveyStatus);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				query = query.Skip(pageIndex * pageSize).Take(pageSize);

				return query.Select(csc => new Form8SurveyConduct
				{
					QASurveyConductID = csc.QASurveyConductID,
					SurveyTypeGuid = csc.SurveyTypeGuid,
					SemesterID = csc.SemesterID,
					OpenDate = csc.StartDate,
					CloseDate = csc.EndDate,
					Status = csc.SurveyStatus,
				}).ToList();
			}
		}

		public static QASurveyConduct.ValidationResults AddQASurveyConduct(short semesterID, Guid surveyGuid, DateTime openDate, DateTime? closeDate, byte status, int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, instituteID, null, null, null);
				if (aspireContext.QASurveyConducts.Any(sc => sc.InstituteID == loginHistory.InstituteID && sc.SemesterID == semesterID && sc.SurveyTypeGuid == surveyGuid))
					return QASurveyConduct.ValidationResults.AlreadyExists;
				var result = QASurveyConduct.Validate(openDate, closeDate);
				if (result != QASurveyConduct.ValidationResults.Success)
					return result;
				var surveyConduct = new QASurveyConduct
				{
					SurveyTypeGuid = surveyGuid,
					InstituteID = loginHistory.InstituteID,
					SemesterID = semesterID,
					StartDate = openDate,
					EndDate = closeDate,
					SurveyStatus = status,
				};
				aspireContext.QASurveyConducts.Add(surveyConduct);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return QASurveyConduct.ValidationResults.Success;
			}
		}

		public static QASurveyConduct ToggleSurveyConductStatus(int qaSurveyConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var surveyConduct = aspireContext.QASurveyConducts.SingleOrDefault(sc => sc.QASurveyConductID == qaSurveyConductID);
				if (surveyConduct == null)
					return null;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, surveyConduct.InstituteID.Value, surveyConduct.DepartmentID, surveyConduct.ProgramID, null);
				switch (surveyConduct.SurveyStatusEnum)
				{
					case QASurveyConduct.Statuses.Active:
						surveyConduct.SurveyStatusEnum = QASurveyConduct.Statuses.Inactive;
						break;
					case QASurveyConduct.Statuses.Inactive:
						surveyConduct.SurveyStatusEnum = QASurveyConduct.Statuses.Active;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return surveyConduct;
			}
		}

		public static QASurveyConduct.ValidationResults DeleteSurveyConduct(int surveyConductID, int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, instituteID, null, null, null);
				var surveyConduct = aspireContext.QASurveyConducts.SingleOrDefault(sc => sc.QASurveyConductID == surveyConductID);
				if (surveyConduct == null)
					return QASurveyConduct.ValidationResults.NoRecordFound;
				var surveyUser = aspireContext.QASurveyUsers.Any(su => su.QASurveyConductID == surveyConduct.QASurveyConductID);
				if (surveyUser == true)
					return QASurveyConduct.ValidationResults.ChildRecordExists;
				var isDatePassed = surveyConduct.StartDate < DateTime.Today;
				if (isDatePassed)
					return QASurveyConduct.ValidationResults.DatePassed;
				aspireContext.QASurveyConducts.Remove(surveyConduct);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return QASurveyConduct.ValidationResults.Success;
			}
		}

		public static QASurveyConduct.ValidationResults UpdateSurveyConduct(int surveyConductID, DateTime openDate, DateTime? closeDate, byte status, int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, instituteID, null, null, null);
				var surveyConduct = aspireContext.QASurveyConducts.SingleOrDefault(sc => sc.QASurveyConductID == surveyConductID);
				if (surveyConduct == null)
					return QASurveyConduct.ValidationResults.NoRecordFound;
				openDate = openDate.Date;
				closeDate = closeDate?.Date;
				if (closeDate != null)
					if (openDate.Date > closeDate.Value.Date)
						return QASurveyConduct.ValidationResults.InvalidDateRange;
				surveyConduct.StartDate = openDate;
				surveyConduct.EndDate = closeDate;
				surveyConduct.SurveyStatus = status;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return QASurveyConduct.ValidationResults.Success;
			}
		}
	}
}
