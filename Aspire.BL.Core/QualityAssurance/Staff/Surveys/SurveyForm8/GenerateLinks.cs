﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.QualityAssurance.Staff.Surveys.SurveyForm8
{
	public static class GenerateLinks
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Employers, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}
		public sealed class QASurveyUser
		{
			internal QASurveyUser() { }
			public int EmployerID { get; internal set; }
			public string Email { get; internal set; }
			public string CompanyName { get; internal set; }
			public int? QASurveyUserID { get; internal set; }
			public int? QASurveyConductID { get; internal set; }
			public short? SemesterID { get; set; }
			public string Name { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public byte? Status { get; internal set; }
			public string StatusFullName => this.Status != null ? ((Model.Entities.QASurveyUser.Statuses)this.Status).ToString().SplitCamelCasing() : ((Model.Entities.QASurveyUser.Statuses.LinkNotSent)).ToString().SplitCamelCasing();
		}
		public static QASurveyConduct GetSurveyConduct(int semesterID, Guid surveyTypeGuid, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.QASurveyConducts.SingleOrDefault(sc => sc.SemesterID == semesterID && sc.InstituteID == loginHistory.InstituteID && sc.SurveyTypeGuid == surveyTypeGuid);
			}
		}
		public static List<QASurveyUser> GetEmployerMembers(int qaSurveyConductID, Model.Entities.QASurveyUser.Statuses? status, string searchText, int pageIndex, int pageSize, out int virtualItemCount, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));
			if (qaSurveyConductID == 0)
			{
				virtualItemCount = 0;
				return null;
			}
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var qaSurveyConduct = aspireContext.QASurveyConducts
					.Where(s => s.QASurveyConductID == qaSurveyConductID && s.InstituteID == loginHistory.InstituteID)
					.Select(s => new
					{
						s.QASurveyConductID,
						s.SemesterID,
					})
					.SingleOrDefault();
				if (qaSurveyConduct == null)
				{
					virtualItemCount = 0;
					return null;
				}

				searchText = searchText.ToNullIfWhiteSpace();
				var employerQuery = aspireContext.Employers
					.Where(emp => emp.InstituteID == loginHistory.InstituteID)
					.Where(emp => searchText == null || emp.Name.Contains(searchText) || emp.Email.Contains(searchText))
					.Select(emp => new
					{
						emp.EmployerID,
						emp.Name,
						emp.Email,
						emp.CompanyName,
					}).Distinct();

				var qaSurveyUsers = aspireContext.QASurveyUsers
					.Where(u => u.QASurveyConductID == qaSurveyConductID && u.EmployerID != null)
					.Where(u => status == null || u.Status == (byte)status.Value)
					.Select(u => new
					{
						u.QASurveyUserID,
						u.EmployerID,
						u.Status,
					});
				if (status != null)
					qaSurveyUsers = qaSurveyUsers.Where(a => a.Status == (byte)status.Value).AsQueryable();
				IQueryable<QASurveyUser> finalQuery;
				if (status == null)
				{
					finalQuery = from emp in employerQuery
								 join u in qaSurveyUsers on emp.EmployerID equals u.EmployerID into grp
								 from g in grp.DefaultIfEmpty()
								 select new QASurveyUser
								 {
									 EmployerID = emp.EmployerID,
									 Name = emp.Name,
									 Email = emp.Email ?? "N/A",
									 CompanyName = emp.CompanyName,
									 QASurveyUserID = g == null ? null : (int?)g.QASurveyUserID,
									 Status = g.Status,
									 QASurveyConductID = g == null ? null : (int?)qaSurveyConduct.QASurveyConductID,
								 };
				}
				else if (status == Model.Entities.QASurveyUser.Statuses.LinkNotSent)
				{
					finalQuery = (from emp in employerQuery
								  join u in qaSurveyUsers on emp.EmployerID equals u.EmployerID into grp
								  from g in grp.DefaultIfEmpty()
								  select new
								  {
									  QASurveyUser = new QASurveyUser
									  {
										  EmployerID = emp.EmployerID,
										  Name = emp.Name,
										  Email = emp.Email ?? "N/A",
										  CompanyName = emp.CompanyName,
										  QASurveyUserID = g == null ? null : (int?)g.QASurveyUserID,
										  Status = g.Status,
										  QASurveyConductID = g == null ? null : (int?)qaSurveyConduct.QASurveyConductID,
									  },
									  Exists = (g.EmployerID != null)
								  }).Where(oc => !oc.Exists).Select(oc => oc.QASurveyUser);
				}
				else
				{
					finalQuery = from emp in employerQuery
								 join u in qaSurveyUsers on emp.EmployerID equals u.EmployerID into grp
								 from g in grp.DefaultIfEmpty()
								 where g.Status == (byte)status.Value
								 select new QASurveyUser
								 {
									 EmployerID = emp.EmployerID,
									 Name = emp.Name,
									 Email = emp.Email ?? "N/A",
									 CompanyName = emp.CompanyName,
									 QASurveyUserID = g == null ? null : (int?)g.QASurveyUserID,
									 Status = g.Status,
									 QASurveyConductID = g == null ? null : (int?)qaSurveyConduct.QASurveyConductID,
								 };
				}
				virtualItemCount = finalQuery.Count();
				switch (sortExpression)
				{
					case nameof(QASurveyUser.Name):
						finalQuery = finalQuery.OrderBy(sortDirection, emp => emp.Name);
						break;
					case nameof(QASurveyUser.Email):
						finalQuery = finalQuery.OrderBy(sortDirection, emp => emp.Email);
						break;
					case nameof(QASurveyUser.CompanyName):
						finalQuery = finalQuery.OrderBy(sortDirection, emp => emp.CompanyName);
						break;
					case nameof(QASurveyUser.Status):
						finalQuery = finalQuery.OrderBy(sortDirection, emp => emp.Status);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return finalQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList();
			}
		}

		public enum AddSurveyEnum
		{
			None,
			NoRecordFound,
			AlreadyExists,
			Success,
		}
		public static AddSurveyEnum AddSurvey(int qASurveyConductID, int employerID, int instituteID, string pageUrl, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, instituteID, null, null, null);
				if (aspireContext.QASurveyUsers.Any(su => su.QASurveyConductID == qASurveyConductID && su.EmployerID == employerID))
					return AddSurveyEnum.AlreadyExists;
				var code = Guid.NewGuid();
				var surveyUser = new Model.Entities.QASurveyUser()
				{
					QASurveyConductID = qASurveyConductID,
					Code = code,
					EmployerID = employerID,
					StatusEnum = Model.Entities.QASurveyUser.Statuses.NotStarted
				};
				aspireContext.QASurveyUsers.Add(surveyUser);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				var qaSurveyUser = aspireContext.QASurveyUsers.Where(qasu => qasu.QASurveyConductID == qASurveyConductID && qasu.EmployerID == employerID)
					.Select(qasu => new
					{
						qasu.QASurveyConduct.StartDate,
						qasu.QASurveyConduct.EndDate,
						qasu.Employer.Name,
						qasu.Employer.Email,
					}).SingleOrDefault();
				var createdDate = qaSurveyUser.StartDate;
				var expiryDate = qaSurveyUser.EndDate;
				var url = $"{pageUrl}?C={code}";
				var emailBody = Properties.Resources.EmployerEmailforQASurveyForm;
				emailBody = emailBody.Replace("{NameHtmlEncoded}", qaSurveyUser.Name.HtmlEncode());
				emailBody = emailBody.Replace("{LinkHtmlAttributeEncoded}", url.HtmlAttributeEncode());
				emailBody = emailBody.Replace("{LinkHtmlEncoded}", url.HtmlEncode());
				var subject = "Bahria University Quality Assurance Survey";
				var toList = new[] { (qaSurveyUser.Name, qaSurveyUser.Email) };
				Core.Common.EmailsManagement.EmailHelper.AddEmailAndSaveChanges(aspireContext, Email.EmailTypes.EmployerSurveyForm, subject, emailBody, true, Email.Priorities.High, createdDate, expiryDate, loginHistory.UserLoginHistoryID, toList: toList);
				aspireContext.CommitTransaction();
				return AddSurveyEnum.Success;
			}
		}
	}
}
