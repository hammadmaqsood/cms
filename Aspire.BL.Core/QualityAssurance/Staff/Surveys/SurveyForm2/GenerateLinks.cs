﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.QualityAssurance.Staff.Surveys.SurveyForm2
{
	public static class GenerateLinks
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.GenerateLinksSurveyForm2, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class QASurveyUser
		{
			internal QASurveyUser() { }
			public int FacultyMemberID { get; internal set; }
			public string Name { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public string DepartmentAlias { get; internal set; }
			public int CourseID { get; internal set; }
			public string CourseTitle { get; internal set; }
			public string SurveyValues { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public int? QASurveyUserID { get; internal set; }
			public int? QASurveyConductID { get; internal set; }
			public Guid Code { get; internal set; }
			public byte? Status { get; internal set; }
			public string StatusFullName => this.Status != null ? ((Model.Entities.QASurveyUser.Statuses)this.Status).ToString().SplitCamelCasing() : ((Model.Entities.QASurveyUser.Statuses.LinkNotSent)).ToString().SplitCamelCasing();
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
		}

		public static QASurveyConduct GetSurveyConduct(int semesterID, Guid suryveyTypeGuid, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.QASurveyConducts.SingleOrDefault(sc => sc.SemesterID == semesterID && sc.SurveyTypeGuid == suryveyTypeGuid);
			}
		}

		public static List<QASurveyUser> GetFacultyMembers(int qaSurveyConductID, int? departmentID, Model.Entities.QASurveyUser.Statuses? status, string searchText, int pageIndex, int pageSize, out int virtualItemCount, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));
			if (qaSurveyConductID == 0)
			{
				virtualItemCount = 0;
				return null;
			}
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var qaSurveyConduct = aspireContext.QASurveyConducts
					.Where(s => s.QASurveyConductID == qaSurveyConductID && s.InstituteID == loginHistory.InstituteID)
					.Select(s => new
					{
						s.QASurveyConductID,
						s.SemesterID,
					})
					.SingleOrDefault();
				if (qaSurveyConduct == null)
				{
					virtualItemCount = 0;
					return null;
				}
				searchText = searchText.ToNullIfWhiteSpace();
				var offeredCoursesQuery = aspireContext.OfferedCourses
					.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(oc => oc.SemesterID == qaSurveyConduct.SemesterID)
					.Where(oc => departmentID == null || oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value)
					.Where(oc => oc.FacultyMemberID != null)
					.Where(oc => searchText == null || oc.FacultyMember.Name.Contains(searchText) || oc.Cours.Title.Contains(searchText))
					.Select(oc => new
					{
						oc.FacultyMemberID,
						oc.Cours.Title,
						oc.OfferedCourseID,
						oc.FacultyMember.Name,
						oc.FacultyMember.DisplayName,
						oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						oc.SemesterNo,
						oc.Section,
						oc.Shift,
						oc.FacultyMember.DepartmentID,
						oc.FacultyMember.Department.DepartmentAlias,
					}).AsQueryable();

				var qaSurveyUsers = aspireContext.QASurveyUsers
					.Where(u => u.QASurveyConductID == qaSurveyConductID && u.FacultyMemberID != null && u.OfferedCourseID != null)
					.Where(u => status == null || u.Status == (byte)status.Value)
					.Select(u => new
					{
						u.QASurveyUserID,
						u.FacultyMemberID,
						u.OfferedCourseID,
						u.Status,
					});
				if (status != null)
					qaSurveyUsers = qaSurveyUsers.Where(a => a.Status == (byte)status.Value).AsQueryable();
				IQueryable<QASurveyUser> finalQuery;
				if (status == null)
				{
					finalQuery = from oc in offeredCoursesQuery
								 join u in qaSurveyUsers on oc.OfferedCourseID equals u.OfferedCourseID into grp
								 from g in grp.DefaultIfEmpty()
								 select new QASurveyUser
								 {
									 FacultyMemberID = oc.FacultyMemberID.Value,
									 Name = oc.Name,
									 CourseTitle = oc.Title,
									 CourseID = oc.OfferedCourseID,
									 DepartmentID = oc.DepartmentID,
									 DepartmentAlias = oc.DepartmentAlias,
									 ProgramAlias = oc.ProgramAlias,
									 SemesterNo = oc.SemesterNo,
									 Section = oc.Section,
									 Shift = oc.Shift,
									 QASurveyUserID = g == null ? null : (int?)g.QASurveyUserID,
									 Status = g.Status,
									 QASurveyConductID = g == null ? null : (int?)qaSurveyConduct.QASurveyConductID,
								 };
				}
				else if (status == Model.Entities.QASurveyUser.Statuses.LinkNotSent)
				{
					finalQuery = (from oc in offeredCoursesQuery
								  join u in qaSurveyUsers on oc.FacultyMemberID equals u.FacultyMemberID into grp
								  from g in grp.DefaultIfEmpty()
								  select new
								  {
									  QASurveyUser = new QASurveyUser
									  {
										  FacultyMemberID = oc.FacultyMemberID.Value,
										  Name = oc.Name,
										  CourseTitle = oc.Title,
										  CourseID = oc.OfferedCourseID,
										  DepartmentID = oc.DepartmentID,
										  DepartmentAlias = oc.DepartmentAlias,
										  ProgramAlias = oc.ProgramAlias,
										  SemesterNo = oc.SemesterNo,
										  Section = oc.Section,
										  Shift = oc.Shift,
										  QASurveyUserID = g == null ? null : (int?)g.QASurveyUserID,
										  Status = g.Status,
										  QASurveyConductID = g == null ? null : (int?)qaSurveyConduct.QASurveyConductID,
									  },
									  Exists = (g.FacultyMemberID != null)
								  }).Where(oc => !oc.Exists).Select(oc => oc.QASurveyUser);
				}
				else
				{
					finalQuery = from oc in offeredCoursesQuery
								 join u in qaSurveyUsers on oc.FacultyMemberID equals u.FacultyMemberID into grp
								 from g in grp.DefaultIfEmpty()
								 where g.Status == (byte)status.Value
								 select new QASurveyUser
								 {
									 FacultyMemberID = oc.FacultyMemberID.Value,
									 Name = oc.Name,
									 CourseTitle = oc.Title,
									 CourseID = oc.OfferedCourseID,
									 DepartmentID = oc.DepartmentID,
									 DepartmentAlias = oc.DepartmentAlias,
									 ProgramAlias = oc.ProgramAlias,
									 SemesterNo = oc.SemesterNo,
									 Section = oc.Section,
									 Shift = oc.Shift,
									 QASurveyUserID = g == null ? null : (int?)g.QASurveyUserID,
									 Status = g.Status,
									 QASurveyConductID = g == null ? null : (int?)qaSurveyConduct.QASurveyConductID,
								 };
				}
				finalQuery = finalQuery.Distinct();
				virtualItemCount = finalQuery.Count();
				switch (sortExpression)
				{
					case nameof(QASurveyUser.Name):
						finalQuery = finalQuery.OrderBy(sortDirection, f => f.Name);
						break;
					case nameof(QASurveyUser.CourseTitle):
						finalQuery = finalQuery.OrderBy(sortDirection, f => f.CourseTitle);
						break;
					case nameof(QASurveyUser.DepartmentAlias):
						finalQuery = finalQuery.OrderBy(sortDirection, f => f.DepartmentAlias);
						break;
					case nameof(QASurveyUser.Status):
						finalQuery = finalQuery.OrderBy(sortDirection, f => f.Status);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return finalQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList();
			}
		}

		public enum AddSurveyEnum
		{
			None,
			NoRecordFound,
			AlreadyExists,
			Success,
		}

		public static AddSurveyEnum AddSurvey(int qASurveyConductID, int facultyMemberID, int courseID, int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, instituteID, null, null, null);
				if (aspireContext.QASurveyUsers.Any(su => su.QASurveyConductID == qASurveyConductID && su.FacultyMemberID == facultyMemberID && su.OfferedCourseID == courseID))
					return AddSurveyEnum.AlreadyExists;
				var code = Guid.NewGuid();
				var surveyUser = new Model.Entities.QASurveyUser()
				{
					QASurveyConductID = qASurveyConductID,
					Code = code,
					FacultyMemberID = facultyMemberID,
					OfferedCourseID = courseID,
					Status = Model.Entities.QASurveyUser.StatusNotStarted
				};
				aspireContext.QASurveyUsers.Add(surveyUser);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddSurveyEnum.Success;
			}
		}
	}
}
