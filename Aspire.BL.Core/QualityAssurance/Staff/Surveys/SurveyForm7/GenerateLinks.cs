﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.CourseRegistration.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.QualityAssurance.Staff.Surveys.SurveyForm7
{
	public static class GenerateLinks
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.GenerateLinksSurveyForm7, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}
		public sealed class QASurveyUser
		{
			internal QASurveyUser() { }
			public int AlumniStudentID { get; internal set; }
			public int? ProgramID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string Email { get; internal set; }
			public string Enrollment { get; set; }
			public string SurveyValues { get; internal set; }
			public int? QASurveyUserID { get; internal set; }
			public int? QASurveyConductID { get; internal set; }
			public Guid Code { get; internal set; }
			public short? SemesterID { get; set; }
			public string Name { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public byte? Status { get; internal set; }
			public string StatusFullName => this.Status != null ? ((Model.Entities.QASurveyUser.Statuses)this.Status).ToString().SplitCamelCasing() : ((Model.Entities.QASurveyUser.Statuses.LinkNotSent)).ToString().SplitCamelCasing();
		}
		public static QASurveyConduct GetSurveyConduct(int semesterID, int departmentID, int programID, Guid surveyTypeGuid, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.QASurveyConducts.SingleOrDefault(sc => sc.SemesterID == semesterID && sc.InstituteID == loginHistory.InstituteID && sc.DepartmentID == departmentID && sc.ProgramID == programID && sc.SurveyTypeGuid == surveyTypeGuid);
			}
		}
		public static List<QASurveyUser> GetAlumniMembers(int qaSurveyConductID, int? departmentID, int? programID, short semesterID, Model.Entities.QASurveyUser.Statuses? status, string searchText, int pageIndex, int pageSize, out int virtualItemCount, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));
			if (qaSurveyConductID == 0)
			{
				virtualItemCount = 0;
				return null;
			}
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var qaSurveyConduct = aspireContext.QASurveyConducts
					.Where(s => s.QASurveyConductID == qaSurveyConductID && s.InstituteID == loginHistory.InstituteID)
					.Select(s => new
					{
						s.QASurveyConductID,
						s.SemesterID,
					})
					.SingleOrDefault();
				if (qaSurveyConduct == null)
				{
					virtualItemCount = 0;
					return null;
				}

				searchText = searchText.ToNullIfWhiteSpace();
				var graduatedStudents = aspireContext.Students
					.Where(stu => stu.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(stu => stu.Status == (byte)Model.Entities.Student.Statuses.Graduated)
					.Where(stu => departmentID == null || stu.AdmissionOpenProgram.Program.Department.DepartmentID == departmentID)
					.Where(stu => programID == null || stu.AdmissionOpenProgram.Program.ProgramID == programID)
					.Where(stu => searchText == null || stu.Name.Contains(searchText) || stu.PersonalEmail.Contains(searchText))
					.Select(stu => new
					{
						stu.StudentID,
						stu.Enrollment,
						stu.Name,
						stu.PersonalEmail,
						stu.AdmissionOpenProgram.Program.ProgramAlias,
						stu.Status,
					}
					);

				var qaSurveyUsers = aspireContext.QASurveyUsers
					.Where(u => u.QASurveyConductID == qaSurveyConductID && u.AlumniStudentID != null)
					.Where(u => status == null || u.Status == (byte)status.Value)
					.Select(u => new
					{
						u.QASurveyUserID,
						u.AlumniStudentID,
						u.Status,
					});
				if (status != null)
					qaSurveyUsers = qaSurveyUsers.Where(a => a.Status == (byte)status.Value).AsQueryable();
				IQueryable<QASurveyUser> finalQuery;
				if (status == null)
				{
					finalQuery = from gs in graduatedStudents
								 join u in qaSurveyUsers on gs.StudentID equals u.AlumniStudentID into grp
								 from g in grp.DefaultIfEmpty()
								 select new QASurveyUser
								 {
									 AlumniStudentID = gs.StudentID,
									 Name = gs.Name,
									 Enrollment = gs.Enrollment,
									 Email = gs.PersonalEmail ?? "N/A",
									 ProgramAlias = gs.ProgramAlias,
									 QASurveyUserID = g == null ? null : (int?)g.QASurveyUserID,
									 Status = g.Status,
									 QASurveyConductID = g == null ? null : (int?)qaSurveyConduct.QASurveyConductID,
								 };
				}
				else if (status == Model.Entities.QASurveyUser.Statuses.LinkNotSent)
				{
					finalQuery = (from gs in graduatedStudents
								  join u in qaSurveyUsers on gs.StudentID equals u.AlumniStudentID into grp
								  from g in grp.DefaultIfEmpty()
								  select new
								  {
									  QASurveyUser = new QASurveyUser
									  {
										  AlumniStudentID = gs.StudentID,
										  Name = gs.Name,
										  Enrollment = gs.Enrollment,
										  Email = gs.PersonalEmail ?? "N/A",
										  ProgramAlias = gs.ProgramAlias,
										  QASurveyUserID = g == null ? null : (int?)g.QASurveyUserID,
										  Status = g.Status,
										  QASurveyConductID = g == null ? null : (int?)qaSurveyConduct.QASurveyConductID,
									  },
									  Exists = (g.AlumniStudentID != null)
								  }).Where(oc => !oc.Exists).Select(oc => oc.QASurveyUser);
				}
				else
				{
					finalQuery = from gs in graduatedStudents
								 join u in qaSurveyUsers on gs.StudentID equals u.AlumniStudentID into grp
								 from g in grp.DefaultIfEmpty()
								 where g.Status == (byte)status.Value
								 select new QASurveyUser
								 {
									 AlumniStudentID = gs.StudentID,
									 Name = gs.Name,
									 Enrollment = gs.Enrollment,
									 Email = gs.PersonalEmail ?? "N/A",
									 ProgramAlias = gs.ProgramAlias,
									 QASurveyUserID = g == null ? null : (int?)g.QASurveyUserID,
									 Status = g.Status,
									 QASurveyConductID = g == null ? null : (int?)qaSurveyConduct.QASurveyConductID,
								 };
				}
				virtualItemCount = finalQuery.Count();
				switch (sortExpression)
				{
					case nameof(QASurveyUser.Name):
						finalQuery = finalQuery.OrderBy(sortDirection, f => f.Name);
						break;
					case nameof(QASurveyUser.Enrollment):
						finalQuery = finalQuery.OrderBy(sortDirection, f => f.Enrollment);
						break;
					case nameof(QASurveyUser.Email):
						finalQuery = finalQuery.OrderBy(sortDirection, f => f.Email);
						break;
					case nameof(QASurveyUser.ProgramAlias):
						finalQuery = finalQuery.OrderBy(sortDirection, f => f.ProgramAlias);
						break;
					case nameof(QASurveyUser.Status):
						finalQuery = finalQuery.OrderBy(sortDirection, f => f.Status);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return finalQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList();
			}
		}

		public enum AddSurveyEnum
		{
			None,
			NoRecordFound,
			AlreadyExists,
			Success,
		}
		public static AddSurveyEnum AddSurvey(int qASurveyConductID, int alumniStudentID, int instituteID, int departmentID, int programID, string pageUrl, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, instituteID, departmentID, programID, null);
				if (aspireContext.QASurveyUsers.Any(su => su.QASurveyConductID == qASurveyConductID && su.AlumniStudentID == alumniStudentID && su.QASurveyConduct.DepartmentID == departmentID && su.QASurveyConduct.ProgramID == programID))
					return AddSurveyEnum.AlreadyExists;
				var code = Guid.NewGuid();
				var surveyUser = new Model.Entities.QASurveyUser()
				{
					QASurveyConductID = qASurveyConductID,
					Code = code,
					AlumniStudentID = alumniStudentID,
					StatusEnum = Model.Entities.QASurveyUser.Statuses.NotStarted
				};
				aspireContext.QASurveyUsers.Add(surveyUser);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				var qaSurveyUser = aspireContext.QASurveyUsers.Where(qasu => qasu.QASurveyConductID == qASurveyConductID && qasu.AlumniStudentID == alumniStudentID)
					.Select(qasu =>new
					{
						qasu.QASurveyConduct.StartDate,
						qasu.QASurveyConduct.EndDate,
						qasu.Student.Name,
						qasu.Student.PersonalEmail,
					}).SingleOrDefault();
				var createdDate = qaSurveyUser.StartDate;
				var expiryDate = qaSurveyUser.EndDate;
				var url = $"{pageUrl}?C={code}";
				var emailBody = Properties.Resources.AlumniEmailforQASurveyForm;
				emailBody = emailBody.Replace("{NameHtmlEncoded}", qaSurveyUser.Name.HtmlEncode());
				emailBody = emailBody.Replace("{LinkHtmlAttributeEncoded}", url.HtmlAttributeEncode());
				emailBody = emailBody.Replace("{LinkHtmlEncoded}", url.HtmlEncode());
				var subject = "Bahria University Quality Assurance Survey";
				var toList = new[] { (qaSurveyUser.Name, qaSurveyUser.PersonalEmail) };
				Core.Common.EmailsManagement.EmailHelper.AddEmailAndSaveChanges(aspireContext, Email.EmailTypes.AlumniSurveyForm,subject,emailBody,true, Email.Priorities.High,createdDate,expiryDate, loginHistory.UserLoginHistoryID,toList: toList);
				aspireContext.CommitTransaction();
				return AddSurveyEnum.Success;
			}
		}
	}
}
