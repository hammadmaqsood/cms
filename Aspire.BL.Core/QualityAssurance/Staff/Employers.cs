﻿using System;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.Common;

namespace Aspire.BL.Core.QualityAssurance.Staff
{
	public static class Employers
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.Module, UserGroupPermission.PermissionValues.Allowed);
		}
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.QualityAssurance.ManageConductSurveyForm8, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}
		public class EmployerInfo
		{
			public int EmployerID { get; set; }
			public string Name { get; set; }
			public string InstituteName { get; set; }
			public int InstituteID { get; set; }
			public string Email { get; set; }
			public string CompanyName { get; set; }
			public string CompanyAddress { get; set; }
		}
		public enum ValidationResults
		{
			None,
			NoRecordFound,
			AlreadyExists,
			Success,
			ChildRecordExists
		}
		public static Employer GetEmployer(int employerID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.Employers.SingleOrDefault(emp => emp.EmployerID == employerID);
			}
		}
		public static List<EmployerInfo> GetEmployers(string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			searchText = searchText.ToNullIfWhiteSpace();
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = aspireContext.Employers.AsQueryable();
				if (loginHistory != null)
					query = query.Where(q => q.InstituteID == loginHistory.InstituteID);
				if (searchText != null)
					query = query.Where(q => q.Name.Contains(searchText) || q.Email.Contains(searchText) || q.CompanyName.Contains(searchText));
				virtualItemCount = query.Count();
				switch (sortExpression)
				{
					case nameof(EmployerInfo.Name):
						query = query.OrderBy(sortDirection, q => q.Name);
						break;
					case nameof(EmployerInfo.Email):
						query = query.OrderBy(sortDirection, q => q.Email);
						break;
					case nameof(EmployerInfo.CompanyName):
						query = query.OrderBy(sortDirection, q => q.CompanyName);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				query = query.Skip(pageIndex * pageSize).Take(pageSize);
				return query.Select(emp => new EmployerInfo
				{
					EmployerID = emp.EmployerID,
					Name = emp.Name,
					InstituteID = emp.InstituteID,
					InstituteName = emp.Institute.InstituteShortName,
					Email = emp.Email,
					CompanyName = emp.CompanyName,
					CompanyAddress = emp.CompanyAddress
				}).ToList();
			}
		}
		public static ValidationResults AddEmployer(string name, string email, string companyName, string companyAddress, int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid,instituteID, null, null, null);
				if (aspireContext.Employers.Any(emp => emp.Email == email))
					return ValidationResults.AlreadyExists;
				var employer = new Employer
				{
					Name = name,
					InstituteID = instituteID,
					Email = email,
					CompanyName = companyName,
					CompanyAddress = companyAddress,
				};
				aspireContext.Employers.Add(employer);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return ValidationResults.Success;
			}
		}
		public static ValidationResults UpdateEmployer(int employerID,string name, string email, string companyName, string companyAddress, int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, instituteID, null, null, null);
				var employer = aspireContext.Employers.SingleOrDefault(sc => sc.EmployerID == employerID);
				if (employer == null)
					return ValidationResults.NoRecordFound;
				employer.Name = name;
				employer.Email = email;
				employer.CompanyName = companyName;
				employer.CompanyAddress = companyAddress;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return ValidationResults.Success;
			}
		}
		public static ValidationResults DeleteEmployer(int employerID, int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, instituteID, null, null, null);
				var employer = aspireContext.Employers.SingleOrDefault(sc => sc.EmployerID == employerID);
				if (employer == null)
					return ValidationResults.NoRecordFound;
				var surveyUser = aspireContext.QASurveyUsers.Any(su => su.EmployerID == employer.EmployerID);
				if (surveyUser == true)
					return ValidationResults.ChildRecordExists;
				aspireContext.Employers.Remove(employer);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return ValidationResults.Success;
			}
		}
	}
}
