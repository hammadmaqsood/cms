﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Logs.Admin
{
	public static class UsersLoginHistory
	{

		public sealed class UserLoginHistory
		{
			internal UserLoginHistory()
			{ }

			public int UserLoginHistoryID { get; internal set; }
			public Guid LoginSessionGuid { get; internal set; }
			public short UserType { get; internal set; }
			public byte SubUserType { get; internal set; }
			public string CandidateName { get; internal set; }
			public string StudentName { get; internal set; }
			public string UserName { get; internal set; }
			public string User_Name { get; internal set; }
			public string FacultyName { get; set; }
			public string IntegratedServiceUserName { get; internal set; }
			public string IPAddress { get; internal set; }
			public string UserAgent { get; internal set; }
			public byte LoginStatus { get; internal set; }
			public DateTime LoginDate { get; internal set; }
			public byte? LoginFailureReason { get; internal set; }
			public DateTime? LogOffDate { get; internal set; }
			public byte? LogOffReason { get; internal set; }

			public string UserTypeFullName
			{
				get
				{
					var subUserTypeEnum = (SubUserTypes)this.SubUserType;
					switch (subUserTypeEnum)
					{
						case SubUserTypes.None:
							return ((UserTypes)this.UserType).ToFullName();
						case SubUserTypes.Parents:
							return $"{((UserTypes)this.UserType).ToFullName()}/{subUserTypeEnum.ToFullName()}";
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}

			public string UserNameString => this.UserName.ToNAIfNullOrEmpty();

			public string Name
			{
				get
				{
					switch ((UserTypes)this.UserType)
					{
						case UserTypes.System:
							return "System";
						case UserTypes.MasterAdmin:
						case UserTypes.Admin:
						case UserTypes.Executive:
						case UserTypes.Staff:
							return this.User_Name.ToNAIfNullOrEmpty();
						case UserTypes.Candidate:
							return this.CandidateName.ToNAIfNullOrEmpty();
						case UserTypes.Student:
							return this.StudentName.ToNAIfNullOrEmpty();
						case UserTypes.Faculty:
							return this.FacultyName.ToNAIfNullOrEmpty();
						case UserTypes.IntegratedService:
							return this.IntegratedServiceUserName.ToNAIfNullOrEmpty();
						case UserTypes.ManualSQL:
							return this.UserName;
						case UserTypes.Anonymous:
						case UserTypes.Alumni:
						case UserTypes.Admins:
						case UserTypes.Any:
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}

			public string StatusString
			{
				get
				{
					switch ((Model.Entities.UserLoginHistory.LoginStatuses)this.LoginStatus)
					{
						case Model.Entities.UserLoginHistory.LoginStatuses.Success:
							return "Success";
						case Model.Entities.UserLoginHistory.LoginStatuses.Failure:
							switch ((Model.Entities.UserLoginHistory.LoginFailureReasons?)this.LoginFailureReason)
							{
								case Model.Entities.UserLoginHistory.LoginFailureReasons.AccountExpired:
									return $"Failed (<em class=\"text-danger\">Account Expired</em>)";
								case Model.Entities.UserLoginHistory.LoginFailureReasons.AccountIsInactive:
									return $"Failed (<em class=\"text-danger\"><em class=\"text-danger\">Account is inactive</em>)";
								case Model.Entities.UserLoginHistory.LoginFailureReasons.CandidateLoginNotAllowedInSemester:
									return $"Failed (<em class=\"text-danger\">Candidate Login Not Allowed In Semester</em>)";
								case Model.Entities.UserLoginHistory.LoginFailureReasons.InvalidPassword:
									return $"Failed (<em class=\"text-danger\">Invalid Password</em>)";
								case Model.Entities.UserLoginHistory.LoginFailureReasons.IPAddressNotAuthorized:
									return $"Failed (<em class=\"text-danger\">IP Address Not Authorized</em>)";
								case Model.Entities.UserLoginHistory.LoginFailureReasons.NoRoleFound:
									return $"Failed (<em class=\"text-danger\">No Role Found</em>)";
								case Model.Entities.UserLoginHistory.LoginFailureReasons.UsernameNotFound:
									return $"Failed (<em class=\"text-danger\">Username not found</em>)";
								case Model.Entities.UserLoginHistory.LoginFailureReasons.UserRoleIsInactive:
									return $"Failed (<em class=\"text-danger\">User Role is inactive</em>)";
								case Model.Entities.UserLoginHistory.LoginFailureReasons.StudentStatusAdmissionCanceled:
									return $"Failed (<em class=\"text-danger\">Status is Admission Canceled</em>)";
								case Model.Entities.UserLoginHistory.LoginFailureReasons.StudentStatusBlocked:
									return $"Failed (<em class=\"text-danger\">Status is Blocked</em>)";
								case Model.Entities.UserLoginHistory.LoginFailureReasons.StudentStatusDeferred:
									return $"Failed (<em class=\"text-danger\">Status is Deferred</em>)";
								case Model.Entities.UserLoginHistory.LoginFailureReasons.StudentStatusDropped:
									return $"Failed (<em class=\"text-danger\">Status is Dropped</em>)";
								case Model.Entities.UserLoginHistory.LoginFailureReasons.StudentStatusExpelled:
									return $"Failed (<em class=\"text-danger\">Status is Expelled</em>)";
								case Model.Entities.UserLoginHistory.LoginFailureReasons.StudentStatusGraduated:
									return $"Failed (<em class=\"text-danger\">Status is Graduated</em>)";
								case Model.Entities.UserLoginHistory.LoginFailureReasons.StudentStatusLeft:
									return $"Failed (<em class=\"text-danger\">Status is Left</em>)";
								case Model.Entities.UserLoginHistory.LoginFailureReasons.StudentStatusProgramChanged:
									return $"Failed (<em class=\"text-danger\">Status is Program Changed</em>)";
								case Model.Entities.UserLoginHistory.LoginFailureReasons.StudentStatusRusticated:
									return $"Failed (<em class=\"text-danger\">Status is Rusticated</em>)";
								case Model.Entities.UserLoginHistory.LoginFailureReasons.StudentStatusTransferredToOtherCampus:
									return $"Failed (<em class=\"text-danger\">Status is Transferred To Other Campus</em>)";
								case null:
									return $"Failed (<em class=\"text-danger\">N/A</em>)";
								default:
									throw new ArgumentOutOfRangeException();
							}
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}

			public string LogOffReasonEnumString
			{
				get
				{
					switch ((Model.Entities.UserLoginHistory.LogOffReasons?)this.LogOffReason)
					{
						case Model.Entities.UserLoginHistory.LogOffReasons.ChangeRole:
							return "Role Changed";
						case Model.Entities.UserLoginHistory.LogOffReasons.LogOff:
							return "Sign Out";
						case Model.Entities.UserLoginHistory.LogOffReasons.SessionTimeout:
							return "Session Timeout";
						case Model.Entities.UserLoginHistory.LogOffReasons.IPAddressChanged:
							return "IP Address Changed";
						case Model.Entities.UserLoginHistory.LogOffReasons.UserAgentChanged:
							return "User Agent Changed";
						case null:
							return string.Empty.ToNAIfNullOrEmpty();
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
		}

		public static (int virtualItemCount, List<UserLoginHistory> userLoginHistories) GetUserLoginHistory(int? instituteID, UserTypes? userTypeEnum, SubUserTypes? subUserTypeEnum, Model.Entities.UserLoginHistory.LoginStatuses? loginStatusEnum, Aspire.Model.Entities.UserLoginHistory.LoginFailureReasons? loginFailureReasonEnum, Aspire.Model.Entities.UserLoginHistory.LogOffReasons? logOffReasonEnum, string searchText, int pageIndex, int pageSize, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				var userLoginHistoryQuery = aspireContext.UserLoginHistories.AsQueryable();
				if (instituteID != null)
					userLoginHistoryQuery = userLoginHistoryQuery.Where(ulh => ulh.Student.AdmissionOpenProgram.Program.InstituteID == instituteID.Value
																				|| ulh.FacultyMember.InstituteID == instituteID.Value
																				|| ulh.UserRole.InstituteID == instituteID.Value);
				if (userTypeEnum != null)
					userLoginHistoryQuery = userLoginHistoryQuery.Where(ulh => ulh.UserType == (short)userTypeEnum.Value);
				if (subUserTypeEnum != null)
					userLoginHistoryQuery = userLoginHistoryQuery.Where(ulh => ulh.SubUserType == (byte)subUserTypeEnum.Value);
				if (loginStatusEnum != null)
					userLoginHistoryQuery = userLoginHistoryQuery.Where(ulh => ulh.LoginStatus == (byte)loginStatusEnum.Value);
				if (loginFailureReasonEnum != null)
					userLoginHistoryQuery = userLoginHistoryQuery.Where(ulh => ulh.LoginFailureReason == (byte)loginFailureReasonEnum.Value);
				if (logOffReasonEnum != null)
					userLoginHistoryQuery = userLoginHistoryQuery.Where(ulh => ulh.LogOffReason == (byte)logOffReasonEnum.Value);
				searchText = searchText.ToNullIfWhiteSpace();
				if (searchText != null)
					userLoginHistoryQuery = userLoginHistoryQuery.Where(ulh =>
						ulh.UserName.Contains(searchText)
						|| ulh.IPAddress.Contains(searchText)
						|| ulh.UserAgent.Contains(searchText));

				var virtualItemCount = userLoginHistoryQuery.Count();
				var userLoginHistories = userLoginHistoryQuery.OrderByDescending(ulh => ulh.UserLoginHistoryID).Skip(pageIndex * pageSize).Take(pageSize)
					.Select(ulh => new UserLoginHistory
					{
						UserLoginHistoryID = ulh.UserLoginHistoryID,
						LoginSessionGuid = ulh.LoginSessionGuid,
						UserType = ulh.UserType,
						SubUserType = ulh.SubUserType,
						UserName = ulh.UserName,
						IPAddress = ulh.IPAddress,
						UserAgent = ulh.UserAgent,
						LoginStatus = ulh.LoginStatus,
						LoginDate = ulh.LoginDate,
						LoginFailureReason = ulh.LoginFailureReason,
						LogOffDate = ulh.LogOffDate,
						LogOffReason = ulh.LogOffReason,
						User_Name = ulh.User.Name,
						CandidateName = ulh.Candidate.Name,
						StudentName = ulh.Student.Name,
						FacultyName = ulh.FacultyMember.DisplayName,
						IntegratedServiceUserName = ulh.IntegratedServiceUser.ServiceName,
					}).ToList();
				return (virtualItemCount, userLoginHistories);
			}
		}
	}
}
