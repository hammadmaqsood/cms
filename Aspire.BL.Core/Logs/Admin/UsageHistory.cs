﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;

namespace Aspire.BL.Core.Logs.Admin
{
	public static class UsageHistory
	{
		public sealed class History
		{
			public Guid PageGUID { get; internal set; }
			public bool IsPostBack { get; internal set; }
			public int Count { get; internal set; }
			public int Min { get; internal set; }
			public int Avg { get; internal set; }
			public int Max { get; internal set; }
		}

		public static List<History> GetData(Guid? pageGUID, bool? isPostBack, DateTime? from, DateTime? to, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);

				var usageHistory = aspireContext.UsageHistories.AsQueryable();
				if (pageGUID != null)
					usageHistory = usageHistory.Where(h => h.PageGUID == pageGUID.Value);
				if (isPostBack != null)
					usageHistory = usageHistory.Where(h => h.IsPostback == isPostBack.Value);
				if (from != null)
					usageHistory = usageHistory.Where(h => h.StartDate >= from);
				if (to != null)
					usageHistory = usageHistory.Where(h => h.StartDate <= to);

				return usageHistory.GroupBy(h => new { h.PageGUID, h.IsPostback })
					.Select(h => new History
					{
						PageGUID = h.Key.PageGUID,
						IsPostBack = h.Key.IsPostback,
						Count = h.Count(),
						Min = h.Min(hh => (int)SqlFunctions.DateDiff("ms", hh.StartDate, hh.EndDate)),
						Avg = (int)h.Average(hh => SqlFunctions.DateDiff("ms", hh.StartDate, hh.EndDate)),
						Max = h.Max(hh => (int)SqlFunctions.DateDiff("ms", hh.StartDate, hh.EndDate)),
					}).ToList();
			}
		}
	}
}