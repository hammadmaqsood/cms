﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;

namespace Aspire.BL.Core.Logs.Admin
{
	public static class Errors
	{
		public sealed class WebException
		{
			internal WebException()
			{ }
			public int WebExceptionID { get; internal set; }
			public short? UserType { get; internal set; }
			public string UserName { get; internal set; }
			public string IPAddress { get; internal set; }
			public string UserAgent { get; internal set; }
			public string PageURL { get; internal set; }
			public string ExceptionMessage { get; internal set; }
			public string YSOD { get; internal set; }
			public bool HasYSOD { get; internal set; }
			public DateTime ExceptionDate { get; internal set; }
			public int ErrorCode { get; internal set; }
			public string UserTypeEnumString => (((UserTypes?)this.UserType)?.ToFullName()).ToNAIfNullOrEmpty();
			public string UserNameString => this.UserName.ToNAIfNullOrEmpty();
		}

		public static (List<WebException> webExceptions, int virtualItemCount) GetWebExceptions(UserTypes? userTypeEnum, DateTime? exceptionDateFrom, DateTime? exceptionDateTo, string searchText, int pageIndex, int pageSize, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				var webExceptionsQuery = aspireContext.WebExceptions.AsQueryable();
				if (userTypeEnum != null)
					webExceptionsQuery = webExceptionsQuery.Where(we => we.UserLoginHistory.UserType == (short)userTypeEnum.Value);
				if (exceptionDateFrom != null)
					webExceptionsQuery = webExceptionsQuery.Where(we => DbFunctions.TruncateTime(we.ExceptionDate) >= DbFunctions.TruncateTime(exceptionDateFrom.Value));
				if (exceptionDateTo != null)
				{
					exceptionDateTo = exceptionDateTo.Value.Date.AddDays(1);
					webExceptionsQuery = webExceptionsQuery.Where(we => DbFunctions.TruncateTime(we.ExceptionDate) < DbFunctions.TruncateTime(exceptionDateTo.Value));
				}
				searchText = searchText.ToNullIfWhiteSpace();
				if (searchText != null)
					webExceptionsQuery = webExceptionsQuery.Where(we => we.ExceptionMessage.Contains(searchText)
															|| we.PageURL.Contains(searchText)
															|| we.UserLoginHistory.UserName.Contains(searchText)
															|| we.IPAddress.Contains(searchText)
															|| we.UserAgent.Contains(searchText)
															|| SqlFunctions.StringConvert((decimal)we.ErrorCode).Contains(searchText));

				var virtualItemCount = webExceptionsQuery.Count();
				var webExceptions = webExceptionsQuery.Select(we => new WebException
				{
					WebExceptionID = we.WebExceptionID,
					UserType = we.UserLoginHistory.UserType,
					UserName = we.UserLoginHistory.UserName,
					IPAddress = we.IPAddress,
					UserAgent = we.UserAgent,
					PageURL = we.PageURL,
					ExceptionMessage = we.ExceptionMessage,
					ExceptionDate = we.ExceptionDate,
					ErrorCode = we.ErrorCode,
					HasYSOD = we.YSOD != null
				}).OrderByDescending(x => x.WebExceptionID).Skip(pageIndex * pageSize).Take(pageSize).ToList();
				return (webExceptions, virtualItemCount);
			}
		}

		public static WebException GetWebException(int webExceptionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				return aspireContext.WebExceptions.Where(we => we.WebExceptionID == webExceptionID).Select(we => new WebException
				{
					WebExceptionID = we.WebExceptionID,
					UserType = we.UserLoginHistory.UserType,
					UserName = we.UserLoginHistory.UserName,
					IPAddress = we.IPAddress,
					UserAgent = we.UserAgent,
					PageURL = we.PageURL,
					ExceptionMessage = we.ExceptionMessage,
					ExceptionDate = we.ExceptionDate,
					ErrorCode = we.ErrorCode,
					YSOD = we.YSOD,
				}).SingleOrDefault();
			}
		}
	}
}
