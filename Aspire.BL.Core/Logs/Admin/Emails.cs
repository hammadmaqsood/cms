﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Logs.Admin
{
	public static class Emails
	{
		public sealed class Email
		{
			internal Email() { }
			public Guid EmailID { get; internal set; }
			public string Subject { get; internal set; }
			public Guid EmailType { get; internal set; }
			public Model.Entities.Email.EmailTypes EmailTypeEnum => this.EmailType.GetEnum<Model.Entities.Email.EmailTypes>();
			public Guid Status { get; internal set; }
			public Model.Entities.Email.Statuses StatusEnum => this.Status.GetEnum<Model.Entities.Email.Statuses>();
			public Model.Entities.Email.Priorities PriorityEnum { get; internal set; }
			public DateTime CreatedDate { get; internal set; }
			public DateTime? ExpiryDate { get; internal set; }
			public List<EmailRecipient> To { get; internal set; }
			public int AttemptsCount { get; internal set; }

			public sealed class EmailRecipient
			{
				public string DisplayName { get; set; }
				public string EmailAddress { get; set; }
			}
		}

		public static (List<Email> emails, int virtualItemCount) GetEmails(Model.Entities.Email.EmailTypes? emailTypeEnum, Model.Entities.Email.Statuses? statusEnum, DateTime? fromDate, DateTime? toDate, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				var emailsQuery = aspireContext.Emails.AsQueryable();
				if (emailTypeEnum != null)
				{
					var emailType = emailTypeEnum.GetGuid();
					emailsQuery = emailsQuery.Where(e => e.EmailType == emailType);
				}
				if (statusEnum != null)
				{
					var status = statusEnum.GetGuid();
					emailsQuery = emailsQuery.Where(e => e.Status == status);
				}
				if (fromDate != null)
				{
					fromDate = fromDate.Value.Date;
					emailsQuery = emailsQuery.Where(e => e.CreatedDate >= fromDate.Value);
				}
				if (toDate != null)
				{
					toDate = toDate.Value.Date.AddDays(1);
					emailsQuery = emailsQuery.Where(e => e.CreatedDate >= toDate.Value);
				}
				searchText = searchText.ToNullIfWhiteSpace();
				if (searchText != null)
					emailsQuery = emailsQuery.Where(we => we.EmailRecipients.Any(er => er.EmailAddress == searchText));

				var virtualItemCount = emailsQuery.Count();
				var toRecipientType = EmailRecipient.RecipientTypes.To.GetGuid();
				var emailsFinalQuery = emailsQuery.Select(e => new Email
				{
					EmailID = e.EmailID,
					Subject = e.Subject,
					EmailType = e.EmailType,
					Status = e.Status,
					CreatedDate = e.CreatedDate,
					ExpiryDate = e.ExpiryDate,
					PriorityEnum = (Model.Entities.Email.Priorities)e.Priority,
					AttemptsCount = e.AttemptsCount,
					To = e.EmailRecipients.Where(er => er.RecipientType == toRecipientType).Select(er => new Email.EmailRecipient
					{
						DisplayName = er.DisplayName,
						EmailAddress = er.EmailAddress
					}).ToList()
				});

				switch (sortExpression)
				{
					case nameof(Email.Subject):
						emailsFinalQuery = emailsFinalQuery.OrderBy(sortDirection, e => e.Subject);
						break;
					case nameof(Email.CreatedDate):
						emailsFinalQuery = emailsFinalQuery.OrderBy(sortDirection, e => e.CreatedDate);
						break;
					case nameof(Email.ExpiryDate):
						emailsFinalQuery = emailsFinalQuery.OrderBy(sortDirection, e => e.ExpiryDate);
						break;
					case nameof(Email.Status):
					case nameof(Email.StatusEnum):
						emailsFinalQuery = emailsFinalQuery.OrderBy(sortDirection, e => e.ExpiryDate);
						break;
					case nameof(Email.EmailType):
					case nameof(Email.EmailTypeEnum):
						emailsFinalQuery = emailsFinalQuery.OrderBy(sortDirection, e => e.EmailType);
						break;
					case nameof(Email.PriorityEnum):
						emailsFinalQuery = emailsFinalQuery.OrderBy(sortDirection, e => e.PriorityEnum);
						break;
					case nameof(Email.AttemptsCount):
						emailsFinalQuery = emailsFinalQuery.OrderBy(sortDirection, e => e.AttemptsCount);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}

				return (emailsFinalQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList(), virtualItemCount);
			}
		}

		public sealed class EmailInfo
		{
			internal EmailInfo() { }
			public Model.Entities.Email Email { get; internal set; }
			public UserTypes CreatedByUserTypeEnum { get; internal set; }
			public string CreatedByUserName { get; internal set; }
		}

		public static EmailInfo GetEmailInfo(Guid emailID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				var emailInfo = new EmailInfo
				{
					Email = aspireContext.Emails
						.Include(e => e.EmailRecipients)
						.Include(e => e.EmailAttachments)
						.Include(e => e.EmailSendAttempts.Select(esa => esa.SMTPServer))
						.SingleOrDefault(e => e.EmailID == emailID)
				};
				if (emailInfo.Email == null)
					return null;

				var createdByUser = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == emailInfo.Email.CreatedByUserLoginHistoryID).Select(h => new
				{
					UserTypeEnum = (UserTypes)h.UserType,
					h.UserName,
				}).Single();

				emailInfo.CreatedByUserTypeEnum = createdByUser.UserTypeEnum;
				emailInfo.CreatedByUserName = createdByUser.UserName;
				return emailInfo;
			}
		}

		public static EmailAttachment GetEmailAttachment(Guid emailID, Guid emailAttachmentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				return aspireContext.EmailAttachments.SingleOrDefault(ea => ea.EmailAttachmentID == emailAttachmentID && ea.EmailID == emailID);
			}
		}
	}
}
