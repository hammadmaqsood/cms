﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aspire.BL.Core.Logs.Common
{
	public static class ErrorHandler
	{
		private static readonly string ErrorLogFileName = System.IO.Path.Combine(Configurations.Current.AppSettings.AspireFileDirectory, "Logs", "WebExceptions.json");
		private static readonly string ErrorLogDirectory = System.IO.Path.GetDirectoryName(ErrorLogFileName);
		private static readonly object ObjectForLock = new object();

		public static void LogException(int? userLoginHistoryID, Exception exc, int errorCode, string pageURL, string userAgent, string ipAddress, string message = null)
		{
			lock (ObjectForLock)
			{
				var webException = new Model.Entities.WebException
				{
					UserLoginHistoryID = userLoginHistoryID,
					ExceptionMessage = exc?.Message ?? message,
					PageURL = pageURL,
					YSOD = exc.ToHtml(),
					ExceptionDate = DateTime.Now,
					ErrorCode = errorCode,
					UserAgent = userAgent?.TrimUpToMaxLength(1024),
					IPAddress = ipAddress,
				};
				try
				{
					Errors.AddWebException(webException);
				}
				catch
				{
					if (!System.IO.Directory.Exists(ErrorLogDirectory))
						System.IO.Directory.CreateDirectory(ErrorLogDirectory);
					System.IO.File.AppendAllText(ErrorLogFileName, $"{webException.ToJsonString()},{Environment.NewLine}");
				}
			}
		}

		private static string ToHtml(this Exception exc)
		{
			if (exc == null)
				return null;

			if (exc is HttpException httpException)
				return httpException.GetHtmlErrorMessage();
			var depth = 10;
			var messages = new List<string>();
			var e = exc;
			while (depth > 0 && e != null)
			{
				messages.Add(e.Message);
				e = e.InnerException;
				depth--;
			}

			return $@"<html>
<body>
Message(s): <br />
<code>{string.Join("<br />", messages.Select(m => m.HtmlEncode()))}</code>
<br />
Stack Trace: <br />
<code>{exc.StackTrace.HtmlEncode()}</code>
</body>
</html>";
		}

		public static void ClearAndSaveErrorLog()
		{
			lock (ObjectForLock)
			{
				if (!System.IO.Directory.Exists(ErrorLogDirectory))
					System.IO.Directory.CreateDirectory(ErrorLogDirectory);
				if (!System.IO.File.Exists(ErrorLogFileName))
					return;
				var json = System.IO.File.ReadAllText(ErrorLogFileName);
				json = $"[{json.Trim().TrimEnd(',')}]";
				var webExceptionsList = json.FromJsonString<Model.Entities.WebException[]>().ToList();
				Errors.AddWebExceptions(webExceptionsList);
				new System.IO.FileInfo(ErrorLogFileName).Delete();
			}
		}
	}
}