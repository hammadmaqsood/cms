﻿using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Logs.Common
{
	public static class Errors
	{
		public static void AddWebExceptions(List<WebException> webExceptions)
		{
			webExceptions?.Where(e => e.UserLoginHistoryID == null).ForEach(e => e.UserLoginHistoryID = UserLoginHistory.SystemUserLoginHistoryID);
			if (webExceptions == null)
				return;
			using (var aspireContext = new AspireContext(true))
			{
				aspireContext.WebExceptions.AddRange(webExceptions);
				aspireContext.SaveChangesAsSystemUserAndCommitTransaction(false);
			}
		}

		public static void AddWebException(WebException webException)
		{
			AddWebExceptions(new List<WebException> { webException });
		}
	}
}
