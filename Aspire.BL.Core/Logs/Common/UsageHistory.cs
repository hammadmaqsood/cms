﻿using Aspire.Model.Context;
using System;

namespace Aspire.BL.Core.Logs.Common
{
	public static class UsageHistory
	{
		public static void AddUsageHistory(int? userLoginHistoryID, DateTime startDate, DateTime endDate, bool isPostback, Guid pageGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.UsageHistories.Add(new Model.Entities.UsageHistory
				{
					UserLoginHistoryID = userLoginHistoryID,
					StartDate = startDate,
					EndDate = endDate,
					PageGUID = pageGuid,
					IsPostback = isPostback,
				});
				aspireContext.SaveChanges();
			}
		}
	}
}
