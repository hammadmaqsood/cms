﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Courses.Staff
{
	public static class Common
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Courses.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class Course
		{
			internal Course() { }
			public Cours Cours { get; internal set; }
			public Course SubstitutedCourse { get; internal set; }
			public List<string> Errors { get; } = new List<string>();
			public int ErrorsCount => this.Errors.Count;
			public int RegisteredCourses { get; internal set; }
			public int OfferedCourses { get; internal set; }
			public int StudentCreditsTransferredCourses { get; internal set; }
			public int TransferredStudentCourseMappings { get; internal set; }
			public int StudentExemptedCourses { get; internal set; }

			public sealed class Dependency
			{
				internal Dependency() { }
				public Course Course { get; internal set; }
				public string Name { get; internal set; }
				public DependencyTypes DependencyType { get; internal set; }
				public int Count { get; internal set; }
			}

			public IEnumerable<Dependency> Dependencies
			{
				get
				{
					if (this.RegisteredCourses > 0)
						yield return new Dependency { Course = this, Name = "Registered", DependencyType = DependencyTypes.RegisteredCourses, Count = this.RegisteredCourses };
					if (this.StudentCreditsTransferredCourses > 0)
						yield return new Dependency { Course = this, Name = "Credit Transferred", DependencyType = DependencyTypes.StudentCreditsTransferredCourses, Count = this.StudentCreditsTransferredCourses };
					if (this.TransferredStudentCourseMappings > 0)
						yield return new Dependency { Course = this, Name = "Transferred Mappings", DependencyType = DependencyTypes.TransferredStudentCourseMappings, Count = this.TransferredStudentCourseMappings };
					if (this.StudentExemptedCourses > 0)
						yield return new Dependency { Course = this, Name = "Exempted", DependencyType = DependencyTypes.StudentExemptedCourses, Count = this.StudentExemptedCourses };
					if (this.OfferedCourses > 0)
						yield return new Dependency { Course = this, Name = "Offered", DependencyType = DependencyTypes.OfferedCourses, Count = this.OfferedCourses };
				}
			}

			public int DependenciesCount => this.RegisteredCourses
				+ this.StudentCreditsTransferredCourses
				+ this.TransferredStudentCourseMappings
				+ this.StudentExemptedCourses
				+ this.OfferedCourses;

			public bool CanVerify => this.Cours.StatusEnum != Cours.Statuses.Verified;
			public bool CanReject => this.Cours.StatusEnum != Cours.Statuses.Rejected;
			public bool CanDelete => this.DependenciesCount == 0 && this.Cours.StatusEnum == Cours.Statuses.Rejected;
			public bool CanProcess => this.DependenciesCount > 0 && this.Cours.StatusEnum == Cours.Statuses.Rejected && this.Cours.SubstitutedCourseID != null;
		}

		internal static IQueryable<Course> GetCourses(this AspireContext aspireContext, IQueryable<Cours> coursesQuery)
		{
			return coursesQuery.Select(c => new Course
			{
				Cours = c,
				RegisteredCourses = aspireContext.RegisteredCourses.Count(rc => rc.CourseID == c.CourseID),
				OfferedCourses = aspireContext.OfferedCourses.Count(oc => oc.CourseID == c.CourseID),
				StudentCreditsTransferredCourses = aspireContext.StudentCreditsTransferredCourses.Count(sctc => sctc.EquivalentCourseID == c.CourseID),
				TransferredStudentCourseMappings = aspireContext.TransferredStudentCourseMappings.Count(tscm => tscm.FromRegisteredCourseID == c.CourseID || tscm.ToCourseID == c.CourseID),
				StudentExemptedCourses = aspireContext.StudentExemptedCourses.Count(sec => sec.CourseID == c.CourseID || sec.SubstitutedCourseID == c.CourseID),
			});
		}

		public enum DependencyTypes
		{
			RegisteredCourses,
			OfferedCourses,
			StudentCreditsTransferredCourses,
			TransferredStudentCourseMappings,
			StudentExemptedCourses,
		}

		public sealed class RegisteredCourse
		{
			internal RegisteredCourse() { }
			public int RegisteredCourseID { get; internal set; }
			public string Enrollment { get; internal set; }
			public short OfferedSemesterID { get; internal set; }
			public DateTime? DeletedDate { get; internal set; }
		}

		public sealed class OfferedCourse
		{
			internal OfferedCourse() { }
			public int OfferedCourseID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterID { get; internal set; }
			public SemesterNos SemesterNoEnum { get; internal set; }
			public Sections SectionEnum { get; internal set; }
			public Shifts ShiftEnum { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public string ClassName => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNoEnum, this.SectionEnum, this.ShiftEnum);
		}

		public sealed class StudentCreditsTransferredCourse
		{
			internal StudentCreditsTransferredCourse() { }
			public int StudentCreditsTransferredCourseID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
		}

		public sealed class TransferredStudentCourseMapping
		{
			internal TransferredStudentCourseMapping() { }
			public int TransferredStudentCourseMappingID { get; internal set; }
			public string FromEnrollment { get; internal set; }
			public string FromName { get; internal set; }
			public string ToEnrollment { get; internal set; }
			public string ToName { get; internal set; }
		}

		public sealed class StudentExemptedCourse
		{
			internal StudentExemptedCourse() { }
			public int StudentExemptedCourseID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
		}

		public sealed class GetDependenciesResult
		{
			internal GetDependenciesResult() { }
			public int CourseID { get; internal set; }
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public CourseTypes CourseTypeEnum { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterID { get; internal set; }
			public DependencyTypes DependencyType { get; internal set; }
			public List<RegisteredCourse> RegisteredCourses { get; internal set; } = new List<RegisteredCourse>();
			public List<OfferedCourse> OfferedCourses { get; internal set; } = new List<OfferedCourse>();
			public List<StudentCreditsTransferredCourse> StudentCreditsTransferredCourses { get; internal set; } = new List<StudentCreditsTransferredCourse>();
			public List<TransferredStudentCourseMapping> TransferredStudentCourseMappings { get; internal set; } = new List<TransferredStudentCourseMapping>();
			public List<StudentExemptedCourse> StudentExemptedCourses { get; internal set; } = new List<StudentExemptedCourse>();
		}

		public static GetDependenciesResult GetDependencies(int courseID, DependencyTypes dependencyType, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var result = aspireContext.Courses
					.Where(c => c.CourseID == courseID)
					.Where(c => c.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Select(c => new GetDependenciesResult
					{
						CourseID = c.CourseID,
						CourseCode = c.CourseCode,
						Title = c.Title,
						CreditHours = c.CreditHours,
						CourseTypeEnum = (CourseTypes)c.CourseType,
						ProgramAlias = c.AdmissionOpenProgram.Program.ProgramAlias,
						SemesterID = c.AdmissionOpenProgram.SemesterID,
						DependencyType = dependencyType
					}).SingleOrDefault();
				if (result == null)
					return null;
				switch (dependencyType)
				{
					case DependencyTypes.RegisteredCourses:
						result.RegisteredCourses = aspireContext.RegisteredCourses.Where(rc => rc.CourseID == result.CourseID).Select(rc => new RegisteredCourse
						{
							RegisteredCourseID = rc.RegisteredCourseID,
							Enrollment = rc.Student.Enrollment,
							OfferedSemesterID = rc.OfferedCours.SemesterID,
							DeletedDate = rc.DeletedDate
						}).ToList();
						break;
					case DependencyTypes.OfferedCourses:
						result.OfferedCourses = aspireContext.OfferedCourses.Where(oc => oc.CourseID == result.CourseID).Select(oc => new OfferedCourse
						{
							OfferedCourseID = oc.OfferedCourseID,
							SemesterID = oc.SemesterID,
							ProgramAlias = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
							SemesterNoEnum = (SemesterNos)oc.SemesterNo,
							SectionEnum = (Sections)oc.Section,
							ShiftEnum = (Shifts)oc.Shift,
							FacultyMemberName = oc.FacultyMember.Name
						}).ToList();
						break;
					case DependencyTypes.StudentExemptedCourses:
						result.StudentExemptedCourses = aspireContext.StudentExemptedCourses.Where(sec => sec.CourseID == result.CourseID || sec.SubstitutedCourseID == result.CourseID).Select(sec => new StudentExemptedCourse
						{
							StudentExemptedCourseID = sec.StudentExemptedCourseID,
							Enrollment = sec.Student.Enrollment,
							Name = sec.Student.Name,
						}).ToList();
						break;
					case DependencyTypes.StudentCreditsTransferredCourses:
						result.StudentCreditsTransferredCourses = aspireContext.StudentCreditsTransferredCourses.Where(sctc => sctc.EquivalentCourseID == result.CourseID).Select(sctc => new StudentCreditsTransferredCourse
						{
							StudentCreditsTransferredCourseID = sctc.StudentCreditsTransferredCourseID,
							Enrollment = sctc.StudentCreditsTransfer.Student.Enrollment,
							Name = sctc.StudentCreditsTransfer.Student.Name,
						}).ToList();
						break;
					case DependencyTypes.TransferredStudentCourseMappings:
						result.TransferredStudentCourseMappings = aspireContext.TransferredStudentCourseMappings.Where(tscm => tscm.ToCourseID == result.CourseID).Select(tscm => new TransferredStudentCourseMapping
						{
							TransferredStudentCourseMappingID = tscm.TransferredStudentCourseMappingID,
							FromEnrollment = tscm.TransferredStudent.Student.Enrollment,
							FromName = tscm.TransferredStudent.Student.Name,
							ToEnrollment = tscm.TransferredStudent.Student1.Enrollment,
							ToName = tscm.TransferredStudent.Student1.Name,
						}).ToList();
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(dependencyType), dependencyType, null);
				}
				return result;
			}
		}
	}
}
