﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.Courses.Staff
{
	public static class CoursePreRequisites
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Courses.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Courses.ManageCoursePreRequisites, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class GetPreRequisiteCoursesResult
		{
			public sealed class CoursePreRequsite
			{
				internal CoursePreRequsite() { }

				public int CoursePreReqID { get; set; }
				public int PreReqCourseID1 { get; internal set; }
				public int? PreReqCourseID2 { get; internal set; }
				public int? PreReqCourseID3 { get; internal set; }
			}

			internal GetPreRequisiteCoursesResult() { }
			public List<CoursePreRequsite> CoursePreRequsites { get; internal set; }
			public List<CustomListItem> Courses { get; internal set; }
		}

		public static GetPreRequisiteCoursesResult GetPreRequisiteCourses(int courseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var course = aspireContext.Courses.SingleOrDefault(c => c.CourseID == courseID && c.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				if (course == null)
					return null;

				return new GetPreRequisiteCoursesResult
				{
					Courses = aspireContext.Courses
						.Where(c => c.AdmissionOpenProgramID == course.AdmissionOpenProgramID && c.CourseID != courseID)
						.Select(c => new CustomListItem
						{
							ID = c.CourseID,
							Text = "[" + c.CourseCode + "] " + c.Title,
						}).OrderBy(c => c.Text).ToList(),
					CoursePreRequsites = aspireContext.CoursePreReqs.Where(p => p.CourseID == courseID).Select(p => new GetPreRequisiteCoursesResult.CoursePreRequsite
					{
						CoursePreReqID = p.CoursePreReqID,
						PreReqCourseID1 = p.PreReqCourseID1,
						PreReqCourseID2 = p.PreReqCourseID2,
						PreReqCourseID3 = p.PreReqCourseID3,
					}).ToList()
				};
			}
		}

		public enum AddPreRequisiteCoursesStatuses
		{
			NoRecordFound,
			RecordAlreadyExists,
			Success
		}

		public static AddPreRequisiteCoursesStatuses AddPreRequisiteCourses(int courseID, List<int> preRequisiteCourseIDs, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var course = aspireContext.Courses.SingleOrDefault(c => c.CourseID == courseID);
				if (course == null)
					return AddPreRequisiteCoursesStatuses.NoRecordFound;
				var admissionOpenProgram = aspireContext.AdmissionOpenPrograms.Where(aop => aop.AdmissionOpenProgramID == course.AdmissionOpenProgramID).Select(aop => new
				{
					aop.AdmissionOpenProgramID,
					aop.ProgramID,
					aop.Program.DepartmentID,
					aop.Program.InstituteID,
				}).Single();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, admissionOpenProgram.InstituteID, admissionOpenProgram.DepartmentID, admissionOpenProgram.ProgramID, admissionOpenProgram.AdmissionOpenProgramID);

				int preReq1;
				int? preReq2 = null;
				int? preReq3 = null;

				preRequisiteCourseIDs = preRequisiteCourseIDs.Distinct().OrderBy(i => i).ToList();

				switch (preRequisiteCourseIDs.Count)
				{
					case 1:
						preReq1 = preRequisiteCourseIDs[0];
						break;
					case 2:
						preReq1 = preRequisiteCourseIDs[0];
						preReq2 = preRequisiteCourseIDs[1];
						break;
					case 3:
						preReq1 = preRequisiteCourseIDs[0];
						preReq2 = preRequisiteCourseIDs[1];
						preReq3 = preRequisiteCourseIDs[2];
						break;
					default:
						throw new ArgumentException(nameof(preRequisiteCourseIDs));
				}
				var alreadyExists = course.CoursePreReqs.Any(p => p.PreReqCourseID1 == preReq1 && p.PreReqCourseID2 == preReq2 && p.PreReqCourseID3 == preReq3);
				if (alreadyExists)
					return AddPreRequisiteCoursesStatuses.RecordAlreadyExists;
				course.CoursePreReqs.Add(new CoursePreReq
				{
					CourseID = courseID,
					PreReqCourseID1 = preReq1,
					PreReqCourseID2 = preReq2,
					PreReqCourseID3 = preReq3,
				});
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddPreRequisiteCoursesStatuses.Success;
			}
		}

		public enum DeletePreRequisiteCourseStatuses
		{
			NoRecordFound,
			Success
		}

		public static DeletePreRequisiteCourseStatuses DeletePreRequisiteCourse(int coursePreReqID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var preReqCourse = aspireContext.CoursePreReqs.Include(p => p.Cours).SingleOrDefault(p => p.CoursePreReqID == coursePreReqID);
				if (preReqCourse == null)
					return DeletePreRequisiteCourseStatuses.NoRecordFound;
				var admissionOpenProgram = aspireContext.AdmissionOpenPrograms.Where(aop => aop.AdmissionOpenProgramID == preReqCourse.Cours.AdmissionOpenProgramID).Select(aop => new
				{
					aop.AdmissionOpenProgramID,
					aop.ProgramID,
					aop.Program.DepartmentID,
					aop.Program.InstituteID,
				}).Single();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, admissionOpenProgram.InstituteID, admissionOpenProgram.DepartmentID, admissionOpenProgram.ProgramID, admissionOpenProgram.AdmissionOpenProgramID);
				aspireContext.CoursePreReqs.Remove(preReqCourse);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeletePreRequisiteCourseStatuses.Success;
			}
		}
	}
}