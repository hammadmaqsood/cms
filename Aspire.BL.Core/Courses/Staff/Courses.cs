﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FileServer;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Courses.Staff
{
	public static class Courses
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Courses.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Courses.ManageCourses, new[] { UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Special, }, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		private static void DemandPermissionsManageCourseSummerGradeCapping(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Courses.ManageCourseSummerGradeCapping, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public static Cours GetCourse(int courseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.Courses.Include(c => c.AdmissionOpenProgram.Program).SingleOrDefault(c => c.CourseID == courseID && c.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
			}
		}

		public static (Common.Course Course, short SemesterID)? GetCourseWithDependencies(int courseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var coursesQuery = aspireContext.Courses
					.Where(c => c.CourseID == courseID && c.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				var record = aspireContext.GetCourses(coursesQuery).Select(c => new { Course = c, c.Cours.AdmissionOpenProgram.SemesterID }).SingleOrDefault();
				if (record == null)
					return null;
				return (record.Course, record.SemesterID);
			}
		}

		public static int? GetAdmissionOpenProgramID(short semesterID, int programID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.AdmissionOpenPrograms
					.Where(aop => aop.Program.InstituteID == loginHistory.InstituteID)
					.Where(aop => aop.SemesterID == semesterID && aop.ProgramID == programID)
					.Select(aop => (int?)aop.ProgramID)
					.SingleOrDefault();
			}
		}

		public static (short semesterID, int programID)? GetAdmissionOpenProgram(int admissionOpenProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var admissionOpenProgram = aspireContext.AdmissionOpenPrograms
					.Where(aop => aop.Program.InstituteID == loginHistory.InstituteID)
					.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID)
					.Select(aop => new
					{
						aop.SemesterID,
						aop.ProgramID
					})
					.SingleOrDefault();
				if (admissionOpenProgram != null)
					return (admissionOpenProgram.SemesterID, admissionOpenProgram.ProgramID);
				return null;
			}
		}

		private static Cours AddCourse(this AspireContext aspireContext, int admissionOpenProgramID, string courseCode, string title, decimal creditHours, decimal contactHours, CourseCategories courseCategoryEnum, CourseTypes courseTypeEnum, int? programMajorID, SemesterNos semesterNoEnum, bool summerGradeCapping, string remarks, Guid loginSessionGuid)
		{
			var admissionOpenProgram = aspireContext.AdmissionOpenPrograms.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID).Select(aop => new
			{
				aop.AdmissionOpenProgramID,
				aop.ProgramID,
				aop.Program.DepartmentID,
				aop.Program.InstituteID,
			}).SingleOrDefault();
			if (admissionOpenProgram == null)
				return null;
			var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, admissionOpenProgram.InstituteID, admissionOpenProgram.DepartmentID, admissionOpenProgram.ProgramID, admissionOpenProgram.AdmissionOpenProgramID);

			if (summerGradeCapping == false)
				aspireContext.DemandPermissionsManageCourseSummerGradeCapping(loginSessionGuid, admissionOpenProgram.InstituteID, admissionOpenProgram.DepartmentID, admissionOpenProgram.ProgramID, admissionOpenProgram.AdmissionOpenProgramID);

			if (programMajorID != null)
			{
				var programMajor = aspireContext.ProgramMajors.Where(pm => pm.ProgramMajorID == programMajorID.Value)
					.Select(pm => new
					{
						pm.ProgramID
					}).SingleOrDefault();
				if (programMajor == null)
					return null;
			}

			var course = new Cours
			{
				AdmissionOpenProgramID = admissionOpenProgramID,
				CreditHours = creditHours,
				ContactHours = contactHours,
				CourseCategoryEnum = courseCategoryEnum,
				CourseCode = courseCode,
				CourseTypeEnum = courseTypeEnum,
				Title = title,
				Remarks = remarks,
				ProgramMajorID = programMajorID,
				SemesterNoEnum = semesterNoEnum,
				SummerGradeCapping = summerGradeCapping,
				StatusEnum = Cours.Statuses.Pending,
				StatusChangedByUserLoginHistoryID = loginHistory.UserLoginHistoryID,
				StatusDate = DateTime.Now,
				SubstitutedCourseID = null,
				SubstitutedByUserLoginHistoryID = null,
				SubstitutedDateTime = null,
				CustomMarks = false
			};
			aspireContext.Courses.Add(course.Validate());
			aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
			return course;
		}

		public static Cours AddCourse(int admissionOpenProgramID, string courseCode, string title, decimal creditHours, decimal contactHours, CourseCategories courseCategoryEnum, CourseTypes courseTypeEnum, int? programMajorID, SemesterNos semesterNoEnum, bool summerGradeCapping, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var course = aspireContext.AddCourse(admissionOpenProgramID, courseCode, title, creditHours, contactHours, courseCategoryEnum, courseTypeEnum, programMajorID, semesterNoEnum, summerGradeCapping, remarks, loginSessionGuid);
				aspireContext.CommitTransaction();
				return course;
			}
		}

		public static Cours UpdateCourse(int courseID, string courseCode, string title, decimal creditHours, decimal contactHours, CourseCategories courseCategoryEnum, CourseTypes courseTypeEnum, int? programMajorID, SemesterNos semesterNoEnum, bool summerGradeCapping, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var coursesQuery = aspireContext.Courses.Where(c => c.CourseID == courseID);
				var record = aspireContext.GetCourses(coursesQuery).Select(c => new
				{
					Course = c,
					c.Cours.AdmissionOpenProgram.Program.InstituteID,
					c.Cours.AdmissionOpenProgram.Program.DepartmentID,
					c.Cours.AdmissionOpenProgram.ProgramID,
					c.Cours.AdmissionOpenProgramID,
				}).SingleOrDefault();
				if (record == null)
					return null;

				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
				var course = record.Course.Cours;

				var verified = course.StatusEnum == Cours.Statuses.Verified;
				var dependenciesExists = record.Course.Dependencies.Any();
				var verifiedOrDependenciesExists = verified || dependenciesExists;

				course.CourseCode = !verifiedOrDependenciesExists ? courseCode : course.CourseCode;
				course.Title = !verifiedOrDependenciesExists ? title : course.Title;
				course.SemesterNoEnum = !verifiedOrDependenciesExists ? semesterNoEnum : course.SemesterNoEnum;
				course.CreditHours = !verifiedOrDependenciesExists ? creditHours : course.CreditHours;
				course.ContactHours = !verifiedOrDependenciesExists ? contactHours : course.ContactHours;
				course.CourseCategoryEnum = !verified ? courseCategoryEnum : course.CourseCategoryEnum;
				course.CourseTypeEnum = !verified ? courseTypeEnum : course.CourseTypeEnum;
				if (programMajorID != null)
				{
					var programMajor = aspireContext.ProgramMajors.Where(pm => pm.ProgramMajorID == programMajorID.Value)
						.Select(pm => new
						{
							pm.ProgramID
						}).SingleOrDefault();
					if (programMajor?.ProgramID != record.ProgramID)
						return null;
				}
				course.ProgramMajorID = !verified ? programMajorID : course.ProgramMajorID;
				if (course.SummerGradeCapping != summerGradeCapping)
				{
					aspireContext.DemandPermissionsManageCourseSummerGradeCapping(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
					course.SummerGradeCapping = !verified ? summerGradeCapping : course.SummerGradeCapping;
				}
				course.Remarks = !verified ? remarks : course.Remarks;
				course.Validate();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return course;
			}
		}

		public sealed class Course
		{
			internal Course() { }
			public string InstituteAlias { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public int CourseID { get; internal set; }
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public SemesterNos SemesterNoEnum { get; internal set; }
			public string SemesterNoFullName => this.SemesterNoEnum.ToFullName();
			public decimal CreditHours { get; internal set; }
			public string CreditHoursFullName => this.CreditHours.ToCreditHoursFullName();
			public decimal ContactHours { get; internal set; }
			public string ContactHoursFullName => this.ContactHours.ToCreditHoursFullName();
			public CourseCategories CourseCategoryEnum { get; internal set; }
			public string CourseCategoryFullName => this.CourseCategoryEnum.ToFullName();
			public CourseTypes CourseTypeEnum { get; internal set; }
			public string CourseTypeFullName => this.CourseTypeEnum.ToFullName();
			public string Remarks { get; internal set; }
			public string Majors { get; internal set; }
			public bool SummerGradeCapping { get; internal set; }
			public string SummerGradeCappingYesNo => this.SummerGradeCapping.ToYesNo();
			public Guid Status { get; internal set; }
			public Cours.Statuses StatusEnum => this.Status.GetEnum<Cours.Statuses>();
		}

		public sealed class GetCoursesResult
		{
			internal GetCoursesResult()
			{
			}
			public int VirtualItemCount { get; internal set; }
			public List<Course> Courses { get; internal set; }
		}

		private static GetCoursesResult GetCourses(this AspireContext aspireContext, short? semesterID, int? programID, SemesterNos? semesterNoEnum, CourseCategories? courseCategoryEnum, CourseTypes? courseTypeEnum, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));
			var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

			var coursesQuery = aspireContext.Courses.Where(c => c.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
			if (semesterID != null)
				coursesQuery = coursesQuery.Where(c => c.AdmissionOpenProgram.SemesterID == semesterID.Value);
			if (programID != null)
				coursesQuery = coursesQuery.Where(c => c.AdmissionOpenProgram.ProgramID == programID.Value);
			if (semesterNoEnum != null)
				coursesQuery = coursesQuery.Where(c => c.SemesterNo == (short)semesterNoEnum.Value);
			if (courseCategoryEnum != null)
				coursesQuery = coursesQuery.Where(c => c.CourseCategory == (byte)courseCategoryEnum.Value);
			if (courseTypeEnum != null)
				coursesQuery = coursesQuery.Where(c => c.CourseType == (byte)courseTypeEnum.Value);
			if (!string.IsNullOrWhiteSpace(searchText))
				coursesQuery = coursesQuery.Where(c => c.Title.Contains(searchText) || c.CourseCode.Contains(searchText));

			var virtualItemCount = coursesQuery.Count();
			var courses = coursesQuery.Select(c => new Course
			{
				SemesterID = c.AdmissionOpenProgram.SemesterID,
				InstituteAlias = c.AdmissionOpenProgram.Program.Institute.InstituteAlias,
				CourseID = c.CourseID,
				CreditHours = c.CreditHours,
				ContactHours = c.ContactHours,
				SemesterNoEnum = (SemesterNos)c.SemesterNo,
				CourseCode = c.CourseCode,
				Title = c.Title,
				Majors = c.ProgramMajor.Majors,
				CourseCategoryEnum = (CourseCategories)c.CourseCategory,
				CourseTypeEnum = (CourseTypes)c.CourseType,
				Remarks = c.Remarks,
				ProgramAlias = c.AdmissionOpenProgram.Program.ProgramAlias,
				SummerGradeCapping = c.SummerGradeCapping,
				Status = c.Status,
			});
			switch (sortExpression)
			{
				case nameof(Course.Semester):
				case nameof(Course.SemesterID):
					courses = courses.OrderBy(sortDirection, c => c.SemesterID);
					break;
				case nameof(Course.CreditHours):
					courses = courses.OrderBy(sortDirection, c => c.CreditHours);
					break;
				case nameof(Course.ContactHours):
					courses = courses.OrderBy(sortDirection, c => c.ContactHours);
					break;
				case nameof(Course.SemesterNoEnum):
					courses = courses.OrderBy(sortDirection, c => c.SemesterNoEnum);
					break;
				case nameof(Course.CourseCode):
					courses = courses.OrderBy(sortDirection, c => c.CourseCode);
					break;
				case nameof(Course.Title):
					courses = courses.OrderBy(sortDirection, c => c.Title);
					break;
				case nameof(Course.CourseCategoryEnum):
					courses = courses.OrderBy(sortDirection, c => c.CourseCategoryEnum);
					break;
				case nameof(Course.CourseTypeEnum):
					courses = courses.OrderBy(sortDirection, c => c.CourseTypeEnum);
					break;
				case nameof(Course.Remarks):
					courses = courses.OrderBy(sortDirection, c => c.Remarks);
					break;
				case nameof(Course.ProgramAlias):
					courses = courses.OrderBy(sortDirection, c => c.ProgramAlias).ThenBy(sortDirection, c => c.SemesterNoEnum);
					break;
				case nameof(Course.Majors):
					courses = courses.OrderBy(sortDirection, c => c.Majors);
					break;
				case nameof(Course.SummerGradeCapping):
					courses = courses.OrderBy(sortDirection, c => c.SummerGradeCapping);
					break;
				case nameof(Course.InstituteAlias):
					courses = courses.OrderBy(sortDirection, c => c.InstituteAlias);
					break;
				default:
					throw new SortingNotImplementedException(sortExpression);
			}

			return new GetCoursesResult
			{
				VirtualItemCount = virtualItemCount,
				Courses = courses.Skip(pageIndex * pageSize).Take(pageSize).ToList()
			};
		}

		public static GetCoursesResult GetCourses(short? semesterID, int? programID, SemesterNos? semesterNoEnum, CourseCategories? courseCategoryEnum, CourseTypes? courseTypeEnum, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.GetCourses(semesterID, programID, semesterNoEnum, courseCategoryEnum, courseTypeEnum, searchText, pageIndex, pageSize, sortExpression, sortDirection, loginSessionGuid);
			}
		}

		private const string ColumnNameInstitute = "Institute";
		private const string ColumnNameIntakeSemester = "Intake Semester";
		private const string ColumnNameProgram = "Program";
		private const string ColumnNameSemesterNo = "Semester No";
		private const string ColumnNameCourseCode = "Course Code";
		private const string ColumnNameTitle = "Title";
		private const string ColumnNameCreditHours = "Credit Hours";
		private const string ColumnNameContactHours = "Contact Hours";
		private const string ColumnNameCourseCategory = "Course Category";
		private const string ColumnNameCourseType = "Course Type";
		private const string ColumnNameMajors = "Majors";
		private const string ColumnNameGradeCapping = "Grade Capping (In Summers)";

		public static (string csv, string fileName) GetCoursesInCSVFormat(short? semesterID, int? programID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var result = aspireContext.GetCourses(semesterID, programID, null, null, null, null, 0, int.MaxValue, nameof(Course.Title), SortDirection.Ascending, loginSessionGuid);
				if (!result.Courses.Any())
					return (null, null);
				var firstCourse = result.Courses.First();
				var fileName = $"{firstCourse.InstituteAlias}-{firstCourse.ProgramAlias}-{semesterID.ToSemesterString()}-{DateTime.Now:yyyyMMddHHmmss}.csv";
				var csv = result.Courses.ConvertToCSVFormat(new List<CSVHelper.CSVColumnMapping>
				{
					new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = ColumnNameInstitute, PropertyName = nameof(Course.InstituteAlias)},
					new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = ColumnNameIntakeSemester, PropertyName = nameof(Course.Semester)},
					new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = ColumnNameProgram, PropertyName = nameof(Course.ProgramAlias)},
					new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = ColumnNameSemesterNo, PropertyName = nameof(Course.SemesterNoFullName)},
					new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = ColumnNameCourseCode, PropertyName = nameof(Course.CourseCode)},
					new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = ColumnNameTitle, PropertyName = nameof(Course.Title)},
					new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = ColumnNameCreditHours, PropertyName = nameof(Course.CreditHoursFullName)},
					new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = ColumnNameContactHours, PropertyName = nameof(Course.ContactHoursFullName)},
					new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = ColumnNameCourseCategory, PropertyName = nameof(Course.CourseCategoryFullName)},
					new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = ColumnNameCourseType, PropertyName = nameof(Course.CourseTypeFullName)},
					new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = ColumnNameMajors, PropertyName = nameof(Course.Majors)},
					new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = ColumnNameGradeCapping , PropertyName = nameof(Course.SummerGradeCappingYesNo)},
				});
				return (csv, fileName);
			}
		}

		private class CourseCSV
		{
			public int RowNumber { get; set; }
			public SemesterNos SemesterNoEnum { get; set; }
			public string CourseCode { get; set; }
			public string Title { get; set; }
			public decimal CreditHours { get; set; }
			public decimal ContactHours { get; set; }
			public CourseCategories CourseCategoryEnum { get; set; }
			public CourseTypes CourseTypeEnum { get; set; }
			public int? ProgramMajorID { get; set; }
			public bool SummerGradeCapping { get; set; }
		}

		public sealed class ImportCSVResult
		{
			public enum Statuses
			{
				NoRecordFound,
				FileIsNotValid,
				Success,
				Failed
			}

			public short? SemesterID { get; set; }
			public string ProgramAlias { get; set; }
			public Statuses Status { get; internal set; }
			public string ErrorMessage { get; internal set; }
			public int CoursesCount { get; internal set; }

			public ImportCSVResult SetStatus(Statuses status, string errorMessage = null)
			{
				this.Status = status;
				this.ErrorMessage = errorMessage;
				return this;
			}
		}

		public static ImportCSVResult ImportCSV(int admissionOpenProgramID, Guid instanceGuid, Guid temporarySystemFileID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var admissionOpenProgram = aspireContext.AdmissionOpenPrograms
					.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID)
					.Where(aop => aop.Program.InstituteID == loginHistory.InstituteID)
					.Select(aop => new
					{
						aop.AdmissionOpenProgramID,
						aop.SemesterID,
						aop.Program.ProgramAlias,
						ProgramMajors = aop.Program.ProgramMajors.ToList()
					}).SingleOrDefault();
				var result = new ImportCSVResult
				{
					ProgramAlias = admissionOpenProgram?.ProgramAlias,
					SemesterID = admissionOpenProgram?.SemesterID
				};
				if (admissionOpenProgram == null)
					return result.SetStatus(ImportCSVResult.Statuses.NoRecordFound);
				var temporarySystemFile = TemporaryFiles.ReadTemporarySystemFile(instanceGuid, temporarySystemFileID, loginSessionGuid);
				if (temporarySystemFile == null)
					return result.SetStatus(ImportCSVResult.Statuses.NoRecordFound);
				List<CourseCSV> courses;
				try
				{
					var csv = Encoding.Default.GetString(temporarySystemFile.Value.fileBytes);
					var csvRows = csv.ParseCSV(true).Select(r => r.ToArray()).ToArray();
					var headerRow = csvRows.FirstOrDefault();
					if (headerRow == null)
						return result.SetStatus(ImportCSVResult.Statuses.NoRecordFound);
					var index = 0;
					var headerCells = headerRow.Select(c => new
					{
						Index = index++,
						HeaderText = c
					}).ToList();
					var instituteIndex = headerCells.Single(c => c.HeaderText == ColumnNameInstitute).Index;
					var intakeSemesterIndex = headerCells.Single(c => c.HeaderText == ColumnNameIntakeSemester).Index;
					var programIndex = headerCells.Single(c => c.HeaderText == ColumnNameProgram).Index;
					var semesterNoIndex = headerCells.Single(c => c.HeaderText == ColumnNameSemesterNo).Index;
					var courseCodeIndex = headerCells.Single(c => c.HeaderText == ColumnNameCourseCode).Index;
					var titleIndex = headerCells.Single(c => c.HeaderText == ColumnNameTitle).Index;
					var creditHoursIndex = headerCells.Single(c => c.HeaderText == ColumnNameCreditHours).Index;
					var contactHoursIndex = headerCells.Single(c => c.HeaderText == ColumnNameContactHours).Index;
					var courseCategoryIndex = headerCells.Single(c => c.HeaderText == ColumnNameCourseCategory).Index;
					var courseTypeIndex = headerCells.Single(c => c.HeaderText == ColumnNameCourseType).Index;
					var majorsIndex = headerCells.Single(c => c.HeaderText == ColumnNameMajors).Index;
					var summerGradeCappingIndex = headerCells.Single(c => c.HeaderText == ColumnNameGradeCapping).Index;
					var semestersNos = Enum.GetValues(typeof(SemesterNos)).Cast<SemesterNos>().Select(s => new { SemesterNoEnum = s, FullName = s.ToFullName() }).ToList();
					var courseCategories = Enum.GetValues(typeof(CourseCategories)).Cast<CourseCategories>().Select(s => new { CourseCategoryEnum = s, FullName = s.ToFullName() }).ToList();
					var courseTypes = Enum.GetValues(typeof(CourseTypes)).Cast<CourseTypes>().Select(s => new { CourseTypeEnum = s, FullName = s.ToFullName() }).ToList();
					var summerGradeCapping = new[] { new { SummarGradeCapping = true, FullName = true.ToYesNo() }, new { SummarGradeCapping = false, FullName = false.ToYesNo() } }.ToList();
					var programMajors = admissionOpenProgram.ProgramMajors;
					var rowIndex = 1;
					courses = csvRows.Skip(1).Select(r => new CourseCSV
					{
						RowNumber = rowIndex++,
						SemesterNoEnum = semestersNos.Single(s => s.FullName == r[semesterNoIndex]).SemesterNoEnum,
						CourseCode = r[courseCodeIndex],
						Title = r[titleIndex],
						CreditHours = r[creditHoursIndex].ToDecimal(),
						ContactHours = r[contactHoursIndex].ToDecimal(),
						CourseCategoryEnum = courseCategories.Single(cc => cc.FullName == r[courseCategoryIndex]).CourseCategoryEnum,
						CourseTypeEnum = courseTypes.Single(ct => ct.FullName == r[courseTypeIndex]).CourseTypeEnum,
						ProgramMajorID = programMajors.SingleOrDefault(pm => pm.Majors == r[majorsIndex])?.ProgramMajorID,
						SummerGradeCapping = summerGradeCapping.Single(sgc => sgc.FullName == r[summerGradeCappingIndex]).SummarGradeCapping
					}).ToList();
					result.CoursesCount = courses.Count;
				}
				catch (Exception exc)
				{
					return result.SetStatus(ImportCSVResult.Statuses.NoRecordFound, exc.Message);
				}

				foreach (var course in courses)
				{
					var courseAdded = aspireContext.AddCourse(admissionOpenProgram.AdmissionOpenProgramID, course.CourseCode, course.Title, course.CreditHours, course.ContactHours, course.CourseCategoryEnum, course.CourseTypeEnum, course.ProgramMajorID, course.SemesterNoEnum, course.SummerGradeCapping, null, loginSessionGuid);
					if (courseAdded == null)
					{
						aspireContext.RollbackTransaction();
						return result.SetStatus(ImportCSVResult.Statuses.Failed, $"Failed at record number {course.RowNumber}.");
					}
				}
				aspireContext.CommitTransaction();
				return result.SetStatus(ImportCSVResult.Statuses.Success);
			}
		}
	}
}