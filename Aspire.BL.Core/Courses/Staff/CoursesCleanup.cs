﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Courses.Staff
{
	public static class CoursesCleanup
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Courses.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandProcessCoursesCleanUpPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Courses.CanProcessCoursesCleanUp, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandManageCourseSubstitutionPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Courses.ManageCourseSubstitution, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandDeleteCoursePermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Courses.CanDeleteCourse, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class GetCoursesResult
		{
			public List<Common.Course> Courses { get; }
			public int VirtualItemCount { get; }
			internal GetCoursesResult(List<Common.Course> courses, SemesterNos? semesterNoEnum, Cours.Statuses? statusEnum, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection)
			{
				var coursesQuery = courses.AsQueryable();
				if (semesterNoEnum != null)
					coursesQuery = coursesQuery.Where(c => c.Cours.SemesterNoEnum == semesterNoEnum.Value);
				if (statusEnum != null)
					coursesQuery = coursesQuery.Where(c => c.Cours.StatusEnum == statusEnum.Value);
				searchText = searchText?.ToLower().TrimAndMakeItNullIfEmpty();
				if (searchText != null)
					coursesQuery = coursesQuery.Where(c => c.Cours.Title.IndexOf(searchText, StringComparison.CurrentCultureIgnoreCase) >= 0 || c.Cours.CourseCode.IndexOf(searchText, StringComparison.CurrentCultureIgnoreCase) >= 0);

				foreach (var course in courses)
					course.Errors.AddRange(GetErrors(course, courses));

				(from c in courses.Where(c => c.Cours.SubstitutedCourseID != null)
				 join sc in courses on c.Cours.SubstitutedCourseID equals sc.Cours.CourseID
				 select new { Course = c, SubstitutedCourse = sc }).ForEach(r => r.Course.SubstitutedCourse = r.SubstitutedCourse);

				switch (sortExpression)
				{
					case nameof(Common.Course.DependenciesCount):
						coursesQuery = coursesQuery.OrderBy(sortDirection, c => c.DependenciesCount).ThenBy(sortDirection, c => c.Cours.Title);
						break;
					case nameof(Common.Course.Cours.CourseCode):
						coursesQuery = coursesQuery.OrderBy(sortDirection, c => c.Cours.CourseCode).ThenByDescending(c => c.DependenciesCount);
						break;
					case nameof(Common.Course.Cours.Title):
						coursesQuery = coursesQuery.OrderBy(sortDirection, c => c.Cours.Title).ThenByDescending(c => c.DependenciesCount);
						break;
					case nameof(Common.Course.Cours.CreditHours):
						coursesQuery = coursesQuery.OrderBy(sortDirection, c => c.Cours.CreditHours).ThenBy(sortDirection, c => c.Cours.Title);
						break;
					case nameof(Common.Course.Cours.CourseTypeEnum):
						coursesQuery = coursesQuery.OrderBy(sortDirection, c => c.Cours.CourseType).ThenBy(sortDirection, c => c.Cours.Title);
						break;
					case nameof(Common.Course.Cours.SemesterNoEnum):
						coursesQuery = coursesQuery.OrderBy(sortDirection, c => c.Cours.SemesterNo).ThenBy(sortDirection, c => c.Cours.Title);
						break;
					case nameof(Common.Course.Cours.Status):
						coursesQuery = coursesQuery.OrderBy(sortDirection, c => c.Cours.Status).ThenBy(sortDirection, c => c.Cours.Title);
						break;
					case nameof(Common.Course.ErrorsCount):
						coursesQuery = coursesQuery.OrderBy(sortDirection, c => c.ErrorsCount).ThenBy(sortDirection, c => c.Cours.Title);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				this.VirtualItemCount = coursesQuery.Count();
				this.Courses = coursesQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList();

			}

			private static IEnumerable<string> GetErrors(Common.Course course, List<Common.Course> courses)
			{
				if (course.Cours.CourseCode.ToNullIfWhiteSpace() == null)
					yield return "Code is missing.";
				if (!course.Cours.CourseCode.IsMatch(@"^[A-Z]{2,5}[- ]\d{3,}$"))
					yield return "Code format is not valid.";
				if (courses.Except(new[] { course }).Any(c => c.Cours.CourseCode.ToLower().ToNullIfWhiteSpace() == course.Cours.CourseCode.ToLower().ToNullIfWhiteSpace()))
					yield return "Code is duplicate.";
				if (course.Cours.Title.ToNullIfWhiteSpace() == null)
					yield return "Title is missing.";
				if (courses.Except(new[] { course }).Any(c => c.Cours.Title.ToLower().ToNullIfWhiteSpace() == course.Cours.Title.ToLower().ToNullIfWhiteSpace()))
					yield return "Title is duplicate.";
			}
		}

		public static GetCoursesResult GetCourses(short semesterID, int programID, SemesterNos? semesterNoEnum, Cours.Statuses? statusEnum, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var coursesQuery = aspireContext.Courses.Where(c => c.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(c => c.AdmissionOpenProgram.ProgramID == programID)
					.Where(c => c.AdmissionOpenProgram.SemesterID == semesterID);
				var courses = aspireContext.GetCourses(coursesQuery).ToList();
				return new GetCoursesResult(courses, semesterNoEnum, statusEnum, searchText, pageIndex, pageSize, sortExpression, sortDirection);
			}
		}

		public static (Cours course, List<Cours> coursesForSubstition)? GetCourse(int courseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var course = aspireContext.Courses.SingleOrDefault(c => c.CourseID == courseID && c.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				if (course == null)
					return null;
				var verifiedStatus = Cours.Statuses.Verified.GetGuid();
				return (course, aspireContext.Courses.Where(c => c.CourseID != course.CourseID && c.AdmissionOpenProgramID == course.AdmissionOpenProgramID && c.Status == verifiedStatus).ToList());
			}
		}

		public enum UpdateCourseStatuses
		{
			NoRecordFound,
			DuplicateCourseCode,
			DuplicateTitle,
			Success
		}

		private static UpdateCourseStatuses UpdateCourse(this AspireContext aspireContext, int courseID, Cours.Statuses statusEnum, int? substitutedCourseID, Guid loginSessionGuid)
		{
			var course = aspireContext.Courses.Where(c => c.CourseID == courseID).Select(c => new
			{
				c.AdmissionOpenProgram.Program.InstituteID,
				c.AdmissionOpenProgram.Program.DepartmentID,
				c.AdmissionOpenProgram.ProgramID,
				c.AdmissionOpenProgramID,
				Course = c
			}).SingleOrDefault();
			if (course == null)
				return UpdateCourseStatuses.NoRecordFound;
			var loginHistory = aspireContext.DemandManageCourseSubstitutionPermissions(loginSessionGuid, course.InstituteID, course.DepartmentID, course.ProgramID, course.AdmissionOpenProgramID);

			var verifiedStatus = Cours.Statuses.Verified.GetGuid();
			switch (statusEnum)
			{
				case Cours.Statuses.Pending:
					if (substitutedCourseID != null)
						throw new ArgumentException("Must be null.", nameof(substitutedCourseID));
					break;
				case Cours.Statuses.Rejected:
					if (substitutedCourseID != null)
					{
						var substitutedCourse = aspireContext.Courses.Where(c => c.CourseID == substitutedCourseID.Value)
							.Where(c => c.AdmissionOpenProgramID == course.Course.AdmissionOpenProgramID && c.Status == verifiedStatus && c.CourseID != course.Course.CourseID)
							.Select(c => new { c.CourseID }).SingleOrDefault();
						if (substitutedCourse == null)
							return UpdateCourseStatuses.NoRecordFound;
					}
					break;
				case Cours.Statuses.Verified:
					if (substitutedCourseID != null)
						throw new ArgumentException("Must be null.", nameof(substitutedCourseID));
					var roadmapCourses = aspireContext.Courses.Where(c => c.AdmissionOpenProgramID == course.Course.AdmissionOpenProgramID && c.Status == verifiedStatus && c.CourseID != course.Course.CourseID);
					if (roadmapCourses.Any(c => c.CourseCode == course.Course.CourseCode))
						return UpdateCourseStatuses.DuplicateCourseCode;
					if (roadmapCourses.Any(c => c.Title == course.Course.Title))
						return UpdateCourseStatuses.DuplicateTitle;
					break;
				default:
					throw new NotImplementedEnumException(statusEnum);
			}

			if (course.Course.StatusEnum != statusEnum)
			{
				course.Course.Status = statusEnum.GetGuid();
				course.Course.StatusChangedByUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				course.Course.StatusDate = DateTime.Now;
			}
			if (course.Course.SubstitutedCourseID != substitutedCourseID)
			{
				course.Course.SubstitutedCourseID = substitutedCourseID;
				course.Course.SubstitutedByUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				course.Course.SubstitutedDateTime = DateTime.Now;
			}
			if (course.Course.StatusEnum != Cours.Statuses.Verified)
				aspireContext.Courses.Where(c => c.AdmissionOpenProgramID == course.Course.AdmissionOpenProgramID && c.SubstitutedCourseID == course.Course.CourseID)
					.ToList().ForEach(c => c.SubstitutedCourseID = null);

			aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
			return UpdateCourseStatuses.Success;
		}

		public static UpdateCourseStatuses UpdateCourse(int courseID, Cours.Statuses statusEnum, int? substitutedCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				return aspireContext.UpdateCourse(courseID, statusEnum, substitutedCourseID, loginSessionGuid);
			}
		}

		public static List<UpdateCourseStatuses> VerifyCourses(List<int> courseIDs, Guid loginSessionGuid)
		{
			var result = new List<UpdateCourseStatuses>();
			foreach (var courseID in courseIDs)
				result.Add(UpdateCourse(courseID, Cours.Statuses.Verified, null, loginSessionGuid));
			return result;
		}

		public static List<UpdateCourseStatuses> RejectCourses(List<int> courseIDs, Guid loginSessionGuid)
		{
			var result = new List<UpdateCourseStatuses>();
			foreach (var courseID in courseIDs)
				result.Add(UpdateCourse(courseID, Cours.Statuses.Rejected, null, loginSessionGuid));
			return result;
		}

		public static List<DeleteCourseStatuses> DeleteCourses(List<int> courseIDs, Guid loginSessionGuid)
		{
			var result = new List<DeleteCourseStatuses>();
			foreach (var courseID in courseIDs)
				result.Add(DeleteCourse(courseID, loginSessionGuid));
			return result;
		}

		public enum ProcessCleanUpProcedureStatuses
		{
			NoRecordFound,
			CourseStatusMustBeRejected,
			SubstituteCourseNotSet,
			SubstituteCourseNotFound,
			SubstituteCourseIsNotInSameRoadmap,
			Success
		}

		public static ProcessCleanUpProcedureStatuses ProcessCleanUpProcedure(int courseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var course = aspireContext.Courses.Where(c => c.CourseID == courseID).Select(c => new
				{
					c.AdmissionOpenProgram.Program.InstituteID,
					c.AdmissionOpenProgram.Program.DepartmentID,
					c.AdmissionOpenProgram.ProgramID,
					c.AdmissionOpenProgramID,
					c.CourseID,
					c.Status,
					c.CourseCode,
					c.Title,
					c.CreditHours,
					c.SubstitutedCourseID,
				}).SingleOrDefault();
				if (course == null)
					return ProcessCleanUpProcedureStatuses.NoRecordFound;

				if (course.Status.GetEnum<Cours.Statuses>() != Cours.Statuses.Rejected)
					return ProcessCleanUpProcedureStatuses.CourseStatusMustBeRejected;
				if (course.SubstitutedCourseID == null)
					return ProcessCleanUpProcedureStatuses.SubstituteCourseNotSet;

				var substituteCourse = aspireContext.Courses
					.Where(c => c.CourseID == course.SubstitutedCourseID.Value)
					.Select(c => new
					{
						c.CourseID,
						c.AdmissionOpenProgramID,
						c.Status,
						c.CourseCode,
						c.Title,
						c.CreditHours
					}).SingleOrDefault();
				if (substituteCourse == null)
					return ProcessCleanUpProcedureStatuses.SubstituteCourseNotFound;
				if (substituteCourse.AdmissionOpenProgramID != course.AdmissionOpenProgramID)
					return ProcessCleanUpProcedureStatuses.SubstituteCourseIsNotInSameRoadmap;

				Core.Common.Permissions.Staff.StaffGroupPermission loginHistory;
				if (course.CourseCode == substituteCourse.CourseCode && course.Title == substituteCourse.Title && course.CreditHours == substituteCourse.CreditHours)
					loginHistory = aspireContext.DemandManageCourseSubstitutionPermissions(loginSessionGuid, course.InstituteID, course.DepartmentID, course.ProgramID, course.AdmissionOpenProgramID);
				else
					loginHistory = aspireContext.DemandProcessCoursesCleanUpPermissions(loginSessionGuid, course.InstituteID, course.DepartmentID, course.ProgramID, course.AdmissionOpenProgramID);

				aspireContext.RegisteredCourses.Where(rc => rc.CourseID == course.CourseID).ToList().ForEach(rc => rc.CourseID = substituteCourse.CourseID);
				aspireContext.OfferedCourses.Where(oc => oc.CourseID == course.CourseID).ToList().ForEach(oc => oc.CourseID = substituteCourse.CourseID);
				aspireContext.StudentExemptedCourses.Where(sec => sec.CourseID == course.CourseID).ToList().ForEach(sec => sec.CourseID = substituteCourse.CourseID);
				aspireContext.StudentExemptedCourses.Where(sec => sec.SubstitutedCourseID == course.CourseID).ToList().ForEach(sec => sec.SubstitutedCourseID = substituteCourse.CourseID);
				aspireContext.StudentCreditsTransferredCourses.Where(sctc => sctc.EquivalentCourseID == course.CourseID).ToList().ForEach(sctc => sctc.EquivalentCourseID = substituteCourse.CourseID);
				aspireContext.TransferredStudentCourseMappings.Where(tscm => tscm.ToCourseID == course.CourseID).ToList().ForEach(tscm => tscm.ToCourseID = substituteCourse.CourseID);

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return ProcessCleanUpProcedureStatuses.Success;
			}
		}

		public enum DeleteCourseStatuses
		{
			NoRecordFound,
			CourseStatusMustBeRejected,
			Success,
			CanNotDeleteWhenDependenciesExists
		}

		public static DeleteCourseStatuses DeleteCourse(int courseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Courses.Where(c => c.CourseID == courseID).Select(c => new
				{
					c.AdmissionOpenProgram.Program.InstituteID,
					c.AdmissionOpenProgram.Program.DepartmentID,
					c.AdmissionOpenProgram.ProgramID,
					c.AdmissionOpenProgramID,
				}).SingleOrDefault();
				if (record == null)
					return DeleteCourseStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandDeleteCoursePermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				var course = aspireContext.GetCourses(aspireContext.Courses.Where(c => c.CourseID == courseID)).Single();
				if (course.Cours.StatusEnum != Cours.Statuses.Rejected)
					return DeleteCourseStatuses.CourseStatusMustBeRejected;

				if (course.DependenciesCount > 0)
					return DeleteCourseStatuses.CanNotDeleteWhenDependenciesExists;

				var courseEquivalents = aspireContext.CourseEquivalents.Where(ce => ce.CourseID == course.Cours.CourseID || ce.EquivalentCourseID == course.Cours.CourseID);
				aspireContext.CourseEquivalents.RemoveRange(courseEquivalents);

				var coursePreReqs = aspireContext.CoursePreReqs.Where(cpr => cpr.CourseID == course.Cours.CourseID || cpr.PreReqCourseID1 == course.Cours.CourseID || cpr.PreReqCourseID2 == course.Cours.CourseID || cpr.PreReqCourseID3 == course.Cours.CourseID);
				aspireContext.CoursePreReqs.RemoveRange(coursePreReqs);

				aspireContext.Courses.Remove(course.Cours);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteCourseStatuses.Success;
			}
		}
	}
}