﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Courses.Staff
{
	public static class CourseEquivalents
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Courses.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Courses.ManageEquivalentCourses, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class EquivalentCourse
		{
			internal EquivalentCourse()
			{
			}

			public string CourseCode { get; internal set; }
			public int CourseID { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public string CreditHoursFullName => this.CreditHours.ToCreditHoursFullName();
			public string EqCourseCode { get; internal set; }
			public int? EqCourseID { get; internal set; }
			public decimal? EqCreditHours { get; internal set; }
			public string EqCreditHoursFullName => this.EqCreditHours?.ToCreditHoursFullName();
			public string EqProgramAlias { get; internal set; }
			public string EqRemarks { get; internal set; }
			public short? EqSemesterID { get; internal set; }
			public string EqSemester => this.EqSemesterID.ToSemesterString();
			public short? EqSemesterNo { get; internal set; }
			public string EqSemesterNoFullName => ((SemesterNos?)this.EqSemesterNo)?.ToFullName();
			public string EqTitle { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string Remarks { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public short SemesterNo { get; internal set; }
			public string SemesterNoFullName => ((SemesterNos)this.SemesterNo).ToFullName();
			public string Title { get; internal set; }
			public string Class => $"{this.ProgramAlias}-{this.SemesterNoFullName}";
			public string EqClass => this.EqProgramAlias != null ? $"{this.EqProgramAlias}-{this.EqSemesterNoFullName}" : null;
			public int? CourseEquivalentID { get; internal set; }
		}

		public sealed class GetEquivalentCoursesResult
		{
			public int VirtualItemCount { get; internal set; }
			public List<EquivalentCourse> EquivalentCourses { get; internal set; }
		}

		public static GetEquivalentCoursesResult GetEquivalentCourses(short? semesterID, int? admissionOpenProgramID, SemesterNos? semesterNoEnum, CourseCategories? courseCategoryEnum, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var allCourses = aspireContext.Courses.Where(c => c.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				var selectedCourses = allCourses;
				if (semesterID != null)
					selectedCourses = selectedCourses.Where(c => c.AdmissionOpenProgram.SemesterID == semesterID.Value);
				if (admissionOpenProgramID != null)
					selectedCourses = selectedCourses.Where(c => c.AdmissionOpenProgramID == admissionOpenProgramID.Value);
				if (semesterNoEnum != null)
					selectedCourses = selectedCourses.Where(c => c.SemesterNo == (short)semesterNoEnum.Value);
				if (courseCategoryEnum != null)
					selectedCourses = selectedCourses.Where(c => c.CourseCategory == (byte)courseCategoryEnum.Value);
				if (!string.IsNullOrWhiteSpace(searchText))
					selectedCourses = selectedCourses.Where(c => c.Title.Contains(searchText) || c.CourseCode.Contains(searchText));

				var courseEquivalents = from eqc in aspireContext.CourseEquivalents
										join c in aspireContext.Courses on eqc.EquivalentCourseID equals c.CourseID
										select new
										{
											eqc.CourseID,
											eqc.CourseEquivalentID,
											eqc.EquivalentCourseID,
											EquivalentCourseSemesterID = c.AdmissionOpenProgram.SemesterID,
											EquivalentCourseProgramAlias = c.AdmissionOpenProgram.Program.ProgramAlias,
											EquivalentCourseSemesterNo = c.SemesterNo,
											EquivalentCourseCourseCode = c.CourseCode,
											EquivalentCourseTitle = c.Title,
											EquivalentCourseCreditHours = c.CreditHours,
											EquivalentCourseRemarks = eqc.Remarks,
										};
				var equivalentCourses = from c in selectedCourses
										join eqc in courseEquivalents on c.CourseID equals eqc.CourseID into grp
										from g in grp.DefaultIfEmpty()
										select new
										{
											c.CourseID,
											c.AdmissionOpenProgramID,
											c.AdmissionOpenProgram.SemesterID,
											c.AdmissionOpenProgram.Program.ProgramAlias,
											c.SemesterNo,
											c.CourseCode,
											c.Title,
											c.CreditHours,
											c.CourseCategory,
											c.Remarks,
											EqCourseEquivalentID = (int?)g.CourseEquivalentID,
											EqCourseID = g.EquivalentCourseID,
											EqSemesterID = g.EquivalentCourseSemesterID,
											EqProgramAlias = g.EquivalentCourseProgramAlias,
											EqSemesterNo = g.EquivalentCourseSemesterNo,
											EqCourseCode = g.EquivalentCourseCourseCode,
											EqTitle = g.EquivalentCourseTitle,
											EqCreditHours = g.EquivalentCourseCreditHours,
											EqRemarks = g.EquivalentCourseRemarks,
										};

				var virtualItemCount = equivalentCourses.Count();
				switch (sortExpression)
				{
					case nameof(EquivalentCourse.SemesterID):
						equivalentCourses = equivalentCourses.OrderBy(sortDirection, c => c.SemesterID);
						break;
					case nameof(EquivalentCourse.EqSemesterID):
						equivalentCourses = equivalentCourses.OrderBy(sortDirection, c => c.EqSemesterID);
						break;
					case nameof(EquivalentCourse.ProgramAlias):
						equivalentCourses = equivalentCourses.OrderBy(sortDirection, c => c.ProgramAlias);
						break;
					case nameof(EquivalentCourse.EqProgramAlias):
						equivalentCourses = equivalentCourses.OrderBy(sortDirection, c => c.EqProgramAlias);
						break;
					case nameof(EquivalentCourse.SemesterNo):
						equivalentCourses = equivalentCourses.OrderBy(sortDirection, c => c.SemesterNo);
						break;
					case nameof(EquivalentCourse.EqSemesterNo):
						equivalentCourses = equivalentCourses.OrderBy(sortDirection, c => c.EqSemesterNo);
						break;
					case nameof(EquivalentCourse.CourseCode):
						equivalentCourses = equivalentCourses.OrderBy(sortDirection, c => c.CourseCode);
						break;
					case nameof(EquivalentCourse.EqCourseCode):
						equivalentCourses = equivalentCourses.OrderBy(sortDirection, c => c.EqCourseCode);
						break;
					case nameof(EquivalentCourse.CreditHours):
						equivalentCourses = equivalentCourses.OrderBy(sortDirection, c => c.CreditHours);
						break;
					case nameof(EquivalentCourse.EqCreditHours):
						equivalentCourses = equivalentCourses.OrderBy(sortDirection, c => c.EqCreditHours);
						break;
					case nameof(EquivalentCourse.Title):
						equivalentCourses = equivalentCourses.OrderBy(sortDirection, c => c.Title);
						break;
					case nameof(EquivalentCourse.EqTitle):
						equivalentCourses = equivalentCourses.OrderBy(sortDirection, c => c.EqTitle);
						break;
					case nameof(EquivalentCourse.Remarks):
						equivalentCourses = equivalentCourses.OrderBy(sortDirection, c => c.Remarks);
						break;
					case nameof(EquivalentCourse.EqRemarks):
						equivalentCourses = equivalentCourses.OrderBy(sortDirection, c => c.EqRemarks);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}

				equivalentCourses = equivalentCourses.Skip(pageIndex * pageSize).Take(pageSize);

				return new GetEquivalentCoursesResult
				{
					VirtualItemCount = virtualItemCount,
					EquivalentCourses = equivalentCourses.Select(c => new EquivalentCourse
					{
						CourseID = c.CourseID,
						CourseEquivalentID = c.EqCourseEquivalentID,
						SemesterID = c.SemesterID,
						ProgramAlias = c.ProgramAlias,
						SemesterNo = c.SemesterNo,
						CourseCode = c.CourseCode,
						Title = c.Title,
						CreditHours = c.CreditHours,
						Remarks = c.Remarks,
						EqCourseID = c.EqCourseID,
						EqSemesterID = c.EqSemesterID,
						EqProgramAlias = c.EqProgramAlias,
						EqSemesterNo = c.EqSemesterNo,
						EqCourseCode = c.EqCourseCode,
						EqTitle = c.EqTitle,
						EqCreditHours = c.EqCreditHours,
						EqRemarks = c.EqRemarks,
					}).ToList(),
				};
			}
		}

		public enum DeleteEquivalentCourseStatuses
		{
			NoRecordFound,
			Success
		}

		public static DeleteEquivalentCourseStatuses DeleteEquivalentCourse(int courseEquivalentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var eqCourse = aspireContext.CourseEquivalents.SingleOrDefault(ec => ec.CourseEquivalentID == courseEquivalentID);
				if (eqCourse == null)
					return DeleteEquivalentCourseStatuses.NoRecordFound;
				var admissionOpenProgram = aspireContext.Courses.Where(c => c.CourseID == eqCourse.CourseID).Select(c => new
				{
					c.AdmissionOpenProgramID,
					c.AdmissionOpenProgram.ProgramID,
					c.AdmissionOpenProgram.Program.DepartmentID,
					c.AdmissionOpenProgram.Program.InstituteID,
				}).Single();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, admissionOpenProgram.InstituteID, admissionOpenProgram.DepartmentID, admissionOpenProgram.ProgramID, admissionOpenProgram.AdmissionOpenProgramID);
				aspireContext.CourseEquivalents.Remove(eqCourse);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteEquivalentCourseStatuses.Success;
			}
		}

		public enum UpdateCourseEquivalentRemarksStatuses
		{
			NoRecordFound,
			Success
		}

		public static UpdateCourseEquivalentRemarksStatuses UpdateCourseEquivalentRemarks(int courseEquivalentID, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var courseEquivalent = aspireContext.CourseEquivalents.SingleOrDefault(ce => ce.CourseEquivalentID == courseEquivalentID);
				if (courseEquivalent == null)
					return UpdateCourseEquivalentRemarksStatuses.NoRecordFound;
				var admissionOpenProgram = aspireContext.Courses.Where(c => c.CourseID == courseEquivalent.CourseID).Select(c => new
				{
					c.AdmissionOpenProgramID,
					c.AdmissionOpenProgram.ProgramID,
					c.AdmissionOpenProgram.Program.DepartmentID,
					c.AdmissionOpenProgram.Program.InstituteID,
				}).Single();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, admissionOpenProgram.InstituteID, admissionOpenProgram.DepartmentID, admissionOpenProgram.ProgramID, admissionOpenProgram.AdmissionOpenProgramID);

				courseEquivalent.Remarks = remarks;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateCourseEquivalentRemarksStatuses.Success;
			}
		}

		public sealed class CourseEquivalent
		{
			internal CourseEquivalent()
			{
			}

			public int CourseEquivalentID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public string ProgramAlias { get; internal set; }
			public short SemesterNo { get; internal set; }
			public string Class => $"{this.ProgramAlias}-{((SemesterNos)this.SemesterNo).ToFullName()}";
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public string CreditHoursFullName => this.CreditHours.ToCreditHoursFullName();
			public byte CourseCategory { get; internal set; }
			public string CourseCategoryFullName => ((Model.Entities.Common.CourseCategories)this.CourseCategory).ToFullName();
			public string CourseRemarks { get; internal set; }
			public string EquivalencyRemarks { get; internal set; }
		}

		public static CourseEquivalent GetCourseEquivalent(int courseEquivalentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return (from c in aspireContext.Courses
						join ce in aspireContext.CourseEquivalents on c.CourseID equals ce.EquivalentCourseID
						where ce.CourseEquivalentID == courseEquivalentID && c.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID
						select new CourseEquivalent
						{
							SemesterID = c.AdmissionOpenProgram.SemesterID,
							CreditHours = c.CreditHours,
							SemesterNo = c.SemesterNo,
							CourseCode = c.CourseCode,
							Title = c.Title,
							ProgramAlias = c.AdmissionOpenProgram.Program.ProgramAlias,
							CourseCategory = c.CourseCategory,
							CourseRemarks = c.Remarks,
							EquivalencyRemarks = ce.Remarks,
							CourseEquivalentID = ce.CourseEquivalentID,
						}).SingleOrDefault();
			}
		}

		public sealed class GetCourseEquivalentsResult
		{
			internal GetCourseEquivalentsResult()
			{
			}

			public int VirtualItemCount { get; internal set; }
			public List<CourseEquivalent> CourseEquivalents { get; internal set; }
		}

		public static GetCourseEquivalentsResult GetCourseEquivalents(int courseID, string sortExpression, SortDirection sortDirection, int pageIndex, int pageSize, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var equCourses = from c in aspireContext.Courses
								 join ce in aspireContext.CourseEquivalents on c.CourseID equals ce.EquivalentCourseID
								 where ce.CourseID == courseID && c.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID
								 select new CourseEquivalent
								 {
									 SemesterID = c.AdmissionOpenProgram.SemesterID,
									 CreditHours = c.CreditHours,
									 SemesterNo = c.SemesterNo,
									 CourseCode = c.CourseCode,
									 Title = c.Title,
									 ProgramAlias = c.AdmissionOpenProgram.Program.ProgramAlias,
									 CourseCategory = c.CourseCategory,
									 CourseRemarks = c.Remarks,
									 EquivalencyRemarks = ce.Remarks,
									 CourseEquivalentID = ce.CourseEquivalentID,
								 };

				var virtualItemCount = equCourses.Count();
				switch (sortExpression)
				{
					case nameof(CourseEquivalent.SemesterID):
						equCourses = equCourses.OrderBy(sortDirection, c => c.SemesterID);
						break;
					case nameof(CourseEquivalent.Class):
						equCourses = equCourses.OrderBy(sortDirection, c => c.ProgramAlias).ThenBy(sortDirection, c => c.SemesterNo);
						break;
					case nameof(CourseEquivalent.CreditHours):
						equCourses = equCourses.OrderBy(sortDirection, c => c.CreditHours);
						break;
					case nameof(CourseEquivalent.CourseCode):
						equCourses = equCourses.OrderBy(sortDirection, c => c.CourseCode);
						break;
					case nameof(CourseEquivalent.Title):
						equCourses = equCourses.OrderBy(sortDirection, c => c.Title);
						break;
					case nameof(CourseEquivalent.CourseCategory):
						equCourses = equCourses.OrderBy(sortDirection, c => c.CourseCategory);
						break;
					case nameof(CourseEquivalent.CourseRemarks):
						equCourses = equCourses.OrderBy(sortDirection, c => c.CourseRemarks);
						break;
					case nameof(CourseEquivalent.EquivalencyRemarks):
						equCourses = equCourses.OrderBy(sortDirection, c => c.EquivalencyRemarks);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}

				return new GetCourseEquivalentsResult
				{
					VirtualItemCount = virtualItemCount,
					CourseEquivalents = equCourses.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
				};
			}
		}

		public enum AddEquivalentCourseStatuses
		{
			NoRecordFound,
			EquivalentCourseDoesnotExists,
			CannotAddBecauseSameRoadmap,
			CannotAddBecauseCourseIsAlreadyMappedToEquivalentCourseRoadmap,
			Success
		}

		private static AddEquivalentCourseStatuses AddEquivalentCourse(this AspireContext aspireContext, Cours course, int equivalentCourseID, Core.Common.Permissions.Staff.StaffGroupPermission loginHistory)
		{
			var courses = aspireContext.Courses.Where(c => c.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
			var eqAdmissionOpenProgramID = courses.Where(c => c.CourseID == equivalentCourseID).Select(c => (int?)c.AdmissionOpenProgramID).SingleOrDefault();
			if (eqAdmissionOpenProgramID == null)
				return AddEquivalentCourseStatuses.EquivalentCourseDoesnotExists;
			if (course.AdmissionOpenProgramID == eqAdmissionOpenProgramID.Value)
				return AddEquivalentCourseStatuses.CannotAddBecauseSameRoadmap;
			var eqAdmissionOpenProgramIDs = from ce in aspireContext.CourseEquivalents
											join c in aspireContext.Courses on ce.EquivalentCourseID equals c.CourseID
											where ce.CourseID == course.CourseID
											select c.AdmissionOpenProgramID;
			if (eqAdmissionOpenProgramIDs.Contains(eqAdmissionOpenProgramID.Value))
				return AddEquivalentCourseStatuses.CannotAddBecauseCourseIsAlreadyMappedToEquivalentCourseRoadmap;
			aspireContext.CourseEquivalents.Add(new Model.Entities.CourseEquivalent
			{
				CourseID = course.CourseID,
				EquivalentCourseID = equivalentCourseID,
			}.Validate());
			aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
			return AddEquivalentCourseStatuses.Success;
		}

		public static List<AddEquivalentCourseStatuses> AddEquivalentCourses(int courseID, List<int> equivalentCourseIDs, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var course = aspireContext.Courses.SingleOrDefault(c => c.CourseID == courseID);
				if (course == null)
					return null;
				var admissionOpenProgram = aspireContext.AdmissionOpenPrograms.Where(aop => aop.AdmissionOpenProgramID == course.AdmissionOpenProgramID).Select(aop => new
				{
					aop.AdmissionOpenProgramID,
					aop.ProgramID,
					aop.Program.DepartmentID,
					aop.Program.InstituteID,
				}).Single();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, admissionOpenProgram.InstituteID, admissionOpenProgram.DepartmentID, admissionOpenProgram.ProgramID, admissionOpenProgram.AdmissionOpenProgramID);
				var result = new List<AddEquivalentCourseStatuses>();
				foreach (var equivalentCourseID in equivalentCourseIDs.Distinct())
					result.Add(aspireContext.AddEquivalentCourse(course, equivalentCourseID, loginHistory));
				aspireContext.CommitTransaction();
				return result;
			}
		}

		public sealed class Course
		{
			internal Course() { }
			public string CourseCode { get; internal set; }
			public int CourseID { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public string CreditHoursFullName => this.CreditHours.ToCreditHoursFullName();
			public string Remarks { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public string Title { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterNo { get; internal set; }
			public string Class => $"{this.ProgramAlias}-{((SemesterNos)this.SemesterNo).ToFullName()}";
		}

		public sealed class GetCoursesForEquivalencyResult
		{
			internal GetCoursesForEquivalencyResult() { }
			public int VirtualItemCount { get; internal set; }
			public List<Course> Courses { get; set; }
		}

		public static GetCoursesForEquivalencyResult GetCoursesForEquivalency(int courseID, bool autoFind, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var course = aspireContext.Courses.Where(c => c.CourseID == courseID && c.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID).Select(c => new
				{
					c.CourseID,
					c.AdmissionOpenProgram.ProgramID,
					c.AdmissionOpenProgram.Program.DepartmentID,
					c.AdmissionOpenProgramID,
					c.Title,
					c.CourseCode,
					c.CreditHours,
				}).SingleOrDefault();
				if (course == null)
					return null;

				var courseEquivalentAdmissionOpenProgramIDs = (from ce in aspireContext.CourseEquivalents
															   join c in aspireContext.Courses on ce.EquivalentCourseID equals c.CourseID
															   where ce.CourseID == course.CourseID
															   select c.AdmissionOpenProgramID).Distinct();

				var allCourses = aspireContext.Courses.Where(c => c.AdmissionOpenProgramID != course.AdmissionOpenProgramID && c.AdmissionOpenProgram.Program.DepartmentID == course.DepartmentID);
				allCourses = allCourses.Where(c => !courseEquivalentAdmissionOpenProgramIDs.Contains(c.AdmissionOpenProgramID));
				if (autoFind)
				{
					allCourses = allCourses.Where(c => c.AdmissionOpenProgram.ProgramID == course.ProgramID)
						.Where(c => c.Title == course.Title)
						.Where(c => c.CourseCode == course.CourseCode)
						.Where(c => c.CreditHours == course.CreditHours);
				}

				if (!string.IsNullOrWhiteSpace(searchText))
					allCourses = allCourses.Where(c => c.CourseCode.Contains(searchText) || c.Title.Contains(searchText) || c.Remarks.Contains(searchText) || c.AdmissionOpenProgram.Program.ProgramAlias.Contains(searchText));

				var virtualItemCount = allCourses.Count();
				switch (sortExpression)
				{
					case nameof(Course.SemesterID):
						allCourses = allCourses.OrderBy(sortDirection, c => c.AdmissionOpenProgram.SemesterID);
						break;
					case nameof(Course.CourseCode):
						allCourses = allCourses.OrderBy(sortDirection, c => c.CourseCode);
						break;
					case nameof(Course.Title):
						allCourses = allCourses.OrderBy(sortDirection, c => c.Title);
						break;
					case nameof(Course.CreditHours):
						allCourses = allCourses.OrderBy(sortDirection, c => c.CreditHours);
						break;
					case nameof(Course.Remarks):
						allCourses = allCourses.OrderBy(sortDirection, c => c.Remarks);
						break;
					case nameof(Course.Class):
						allCourses = allCourses.OrderBy(sortDirection, c => c.AdmissionOpenProgram.Program.ProgramAlias).ThenBy(sortDirection, c => c.SemesterNo);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}

				return new GetCoursesForEquivalencyResult
				{
					VirtualItemCount = virtualItemCount,
					Courses = allCourses.Skip(pageIndex * pageSize).Take(pageSize).Select(c => new Course
					{
						CourseID = c.CourseID,
						SemesterID = c.AdmissionOpenProgram.SemesterID,
						CourseCode = c.CourseCode,
						Title = c.Title,
						ProgramAlias = c.AdmissionOpenProgram.Program.ProgramAlias,
						SemesterNo = c.SemesterNo,
						CreditHours = c.CreditHours,
						Remarks = c.Remarks,
					}).ToList()
				};
			}
		}
	}
}