﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Courses.Staff.Reports
{
	public static class Courses
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Courses.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class PageHeaderCourses
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string Title => $"Courses Roadmap: {this.ProgramAlias} ({this.SemesterID.ToSemesterString()})";
			public static List<PageHeaderCourses> GetPageHeaderCourses()
			{
				return null;
			}
		}
		public sealed class CourseDataSet
		{
			public int CourseID { get; internal set; }
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public string CreditHoursFullName => this.CreditHours.ToCreditHoursFullName();
			public decimal ContactHours { get; internal set; }
			public string ContactHoursFullName => this.ContactHours.ToCreditHoursFullName();
			public byte CourseCategory { get; internal set; }
			public string CourseCategoryFullName => ((CourseCategories)this.CourseCategory).ToFullName();
			public byte CourseType { get; internal set; }
			public string CourseTypeFullName => ((CourseTypes)this.CourseType).ToFullName();
			public short SemesterNo { get; internal set; }
			public string SemesterNoFullName => ((SemesterNos)this.SemesterNo).ToFullName();
			public string Majors { get; internal set; }
			public string MajorsNA => this.Majors.ToNAIfNullOrEmpty();
			public Guid Status { get; internal set; }
			public string StatusFullName => this.Status.GetFullName();
			internal List<CoursePreReq> CoursePreReqs { get; set; }
			public string PreReqs { get; internal set; }

			public static List<CourseDataSet> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public PageHeaderCourses PageHeader { get; internal set; }
			public List<CourseDataSet> Courses { get; internal set; }
		}

		public static ReportDataSet GetCourses(short semesterID, int programID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var courses = aspireContext.Courses.Where(c => c.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(c => c.AdmissionOpenProgram.SemesterID == semesterID)
					.Where(c => c.AdmissionOpenProgram.ProgramID == programID);
				var result = courses.Select(c => new CourseDataSet
				{
					CourseID = c.CourseID,
					CreditHours = c.CreditHours,
					ContactHours = c.ContactHours,
					SemesterNo = c.SemesterNo,
					CourseCode = c.CourseCode,
					Title = c.Title,
					CourseCategory = c.CourseCategory,
					CourseType = c.CourseType,
					CoursePreReqs = c.CoursePreReqs.ToList(),
					Majors = c.ProgramMajor.Majors,
					Status = c.Status
				}).OrderBy(c => c.SemesterNo).ThenBy(t => t.Title).ToList();

				var dictionary = result.ToDictionary(c => c.CourseID, c => c);

				result.ForEach(c =>
				{
					var preReqs = c.CoursePreReqs.Select(pr =>
					  {
						  var pre = $"{dictionary[pr.PreReqCourseID1].CourseCode.HtmlEncode()}";
						  if (pr.PreReqCourseID2 != null)
						  {
							  pre = "(" + pre;
							  pre += $" <i>&</i> {dictionary[pr.PreReqCourseID2.Value].CourseCode.HtmlEncode()}";
							  if (pr.PreReqCourseID3 != null)
								  pre += $"<i>&</i> {dictionary[pr.PreReqCourseID2.Value].CourseCode.HtmlEncode()}";
							  pre += ")";
						  }
						  return pre;
					  }).ToList();
					if (!preReqs.Any())
						c.PreReqs = string.Empty.ToNAIfNullOrEmpty();
					else if (preReqs.Count == 1)
						c.PreReqs = preReqs.Single().TrimStart('(').TrimEnd(')');
					else
						c.PreReqs = string.Join(" <i>OR</i> ", preReqs);
				});

				return new ReportDataSet
				{
					PageHeader = new PageHeaderCourses
					{
						SemesterID = semesterID,
						Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single(),
						ProgramAlias = aspireContext.Programs.Where(p => p.ProgramID == programID).Select(p => p.ProgramAlias).Single()
					},
					Courses = result
				};
			}
		}
	}
}
