﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Courses.Staff.Reports
{
	public static class CoursesEquivalents
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Courses.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class PageHeader
		{
			public string Institute { get; internal set; }
			public short OfferedSemesterID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string Title => $"Equivalent Courses: {this.ProgramAlias}, Offered Semester: {this.OfferedSemesterID.ToSemesterString()}";
			public static List<PageHeader> GetEquivalentCoursesPageHeader()
			{
				return null;
			}
		}

		public sealed class EquivalentCourse
		{
			public int CourseID { get; internal set; }
			public string CourseCode { get; internal set; }
			public string CourseTitle { get; internal set; }
			public decimal CourseCreditHours { get; internal set; }
			public string CreditHoursFullName => this.CourseCreditHours.ToCreditHoursFullName();
			public short CourseSemesterNo { get; internal set; }
			public string CourseSemesterNoFullName => ((SemesterNos)this.CourseSemesterNo).ToFullName();
			public short EquivalentCourseSemesterID { get; internal set; }
			public short CourseSemesterID { get; internal set; }
			public string CourseSemester => this.CourseSemesterID.ToSemesterString();
			public string EquivalentCourseSemester => this.EquivalentCourseSemesterID.ToSemesterString();
			public int EquivalentCourseID { get; internal set; }
			public string EquivalentCourseCode { get; internal set; }
			public string EquivalentCourseTitle { get; internal set; }
			public decimal? EquivalentCreditHours { get; internal set; }
			public string EquivalentCreditHoursFullName => this.EquivalentCreditHours?.ToCreditHoursFullName();
			public short EquivalentCourseSemesterNo { get; set; }
			public string EquivalentCourseSemesterNoFullName => ((SemesterNos)this.EquivalentCourseSemesterNo).ToFullName();

			public string EquivalentCourseProgramAlias { get; internal set; }

			public static List<EquivalentCourse> GetEquivalentCourseList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public PageHeader PageHeader { get; internal set; }
			public List<EquivalentCourse> EquivalentCourses { get; internal set; }
		}

		public static ReportDataSet GetCoursesEquivalents(short offeredSemesterID, int programID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

				var coursesOffered = aspireContext.OfferedCourses.Where(oc => oc.SemesterID == offeredSemesterID && oc.Cours.AdmissionOpenProgram.ProgramID == programID && oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Select(oc => oc.Cours);

				var courseEquivalents = from c in coursesOffered
										join eqc in aspireContext.CourseEquivalents on c.CourseID equals eqc.CourseID
										join ec in aspireContext.Courses on eqc.EquivalentCourseID equals ec.CourseID
										where ec.AdmissionOpenProgram.SemesterID <= offeredSemesterID
										select new EquivalentCourse
										{
											CourseID = c.CourseID,
											CourseCode = c.CourseCode,
											CourseTitle = c.Title,
											CourseSemesterNo = c.SemesterNo,
											CourseCreditHours = c.CreditHours,
											CourseSemesterID = c.AdmissionOpenProgram.SemesterID,
											EquivalentCourseSemesterID = ec.AdmissionOpenProgram.SemesterID,
											EquivalentCourseProgramAlias = ec.AdmissionOpenProgram.Program.ProgramAlias,
											EquivalentCourseSemesterNo = ec.SemesterNo,
											EquivalentCourseID = ec.CourseID,
											EquivalentCourseCode = ec.CourseCode,
											EquivalentCourseTitle = ec.Title,
											EquivalentCreditHours = ec.CreditHours,
										};

				return new ReportDataSet
				{
					PageHeader = new PageHeader
					{
						OfferedSemesterID = offeredSemesterID,
						Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single(),
						ProgramAlias = aspireContext.Programs.Where(p => p.InstituteID == loginHistory.InstituteID && p.ProgramID == programID).Select(p => p.ProgramAlias).Single(),
					},
					EquivalentCourses = courseEquivalents.Distinct().OrderBy(c => c.CourseTitle).ThenBy(c => c.EquivalentCourseSemesterID).ThenBy(c => c.EquivalentCourseProgramAlias).ToList()
				};
			}
		}
	}
}
