﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.ExecutiveInformation.Executive.Exams
{
	public static class GradesSummary
	{
		private static Common.Permissions.Executive.ExecutiveGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandExecutivePermissions(loginSessionGuid, ExecutivePermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}
		public sealed class OfferedCourseExams
		{
			public int OfferedCourseID { get; internal set; }
			public int CourseID { get; internal set; }
			public string Title { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Program { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift);
			public int GradeACount { get; internal set; }
			public int GradeAMinusCount { get; internal set; }
			public int GradeBPlusCount { get; internal set; }
			public int GradeBCount { get; internal set; }
			public int GradeBMinusCount { get; internal set; }
			public int GradeCPlusCount { get; internal set; }
			public int GradeCCount { get; internal set; }
			public int GradeCMinusCount { get; internal set; }
			public int GradeDPlusCount { get; internal set; }
			public int GradeDCount { get; internal set; }
			public int GradeFCount { get; internal set; }
			public string Teacher { get; internal set; }
			public int Students { get; internal set; }
		}

		public sealed class OfferedCourseExamsDetails
		{
			public int OfferedCourseID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public decimal ContactHours { get; internal set; }
			public string OcProgram { get; internal set; }
			public short OcSemesterNo { get; internal set; }
			public int OcSection { get; internal set; }
			public byte OcShift { get; internal set; }
			public string OcClass => AspireFormats.GetClassName(this.OcProgram, this.OcSemesterNo, this.OcSection, this.OcShift);
			public string Teacher { get; internal set; }
			public byte Category { get; internal set; }
			public byte CourseType { get; internal set; }
			public sealed class ExamDetails
			{
				public int RegisteredCourseID { get; internal set; }
				public string Enrollment { get; internal set; }
				public int? RegistrationNo { get; internal set; }
				public string Name { get; internal set; }
				public string Program { get; internal set; }
				public short SemesterNo { get; internal set; }
				public int Section { get; internal set; }
				public byte Shift { get; internal set; }
				public string Class => AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift);
				public decimal? AssignmentsMarks { get; internal set; }
				public string Assignments => this.AssignmentsMarks.ToExamMarksString();
				public decimal? QuizzesMarks { get; internal set; }
				public string Quizzes => this.QuizzesMarks.ToExamMarksString();
				public decimal? MidMarks { get; internal set; }
				public string Mid => this.MidMarks.ToExamMarksString();
				public decimal? FinalMarks { get; internal set; }
				public string Final => this.FinalMarks.ToExamMarksString();
				public byte? Total { get; internal set; }
				public byte? Grade { get; internal set; }
				public string GradeFullName => ((ExamGrades?)this.Grade)?.ToFullName();
			}
			public List<ExamDetails> Details { get; internal set; }
		}

		public static readonly string ExamMarksFormat = "0.###";

		public static string ToExamMarksString(this decimal? marks)
		{
			return marks?.ToString(ExamMarksFormat);
		}

		public static List<OfferedCourseExams> GetOfferedCoursesGradesSummary(short semesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int pageIndex, int pageSize, out int virtualItemCount, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var courses = aspireContext.OfferedCourses.Where(oc => oc.SemesterID == semesterID && oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);


				var departmentIDs = aspireContext.UserRoleModules
					.Where(urm => urm.UserRoleID == loginHistory.UserRoleID)
					.Where(urm => urm.Module == loginHistory.PermissionType.Module)
					.Select(urm => urm.DepartmentID).ToList();
				if (departmentIDs.All(d => d != null))
					courses = courses.Where(oc => departmentIDs.Contains(oc.Cours.AdmissionOpenProgram.Program.DepartmentID));

				if (departmentID != null)
					courses = courses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID);
				if (programID != null)
					courses = courses.Where(c => c.Cours.AdmissionOpenProgram.ProgramID == programID);
				if (semesterNoEnum != null)
				{
					var semesterNo = (short)semesterNoEnum;
					courses = courses.Where(c => c.SemesterNo == semesterNo);
				}
				if (sectionEnum != null)
				{
					var section = (int)sectionEnum;
					courses = courses.Where(c => c.Section == section);
				}
				if (shiftEnum != null)
				{
					var shift = (byte)shiftEnum;
					courses = courses.Where(c => c.Shift == shift);
				}
				virtualItemCount = courses.Count();
				switch (sortExpression)
				{
					case "Semester":
						courses = courses.OrderBy(sortDirection, c => c.SemesterID);
						break;
					case "ProgramAlias":
						courses = courses.OrderBy(sortDirection, c => c.Cours.AdmissionOpenProgram.Program.ProgramAlias);
						break;
					case "Title":
						courses = courses.OrderBy(sortDirection, c => c.Cours.Title);
						break;
					case "Teacher":
						courses = courses.OrderBy(sortDirection, c => c.FacultyMember.Name);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				virtualItemCount = courses.Count();
				courses = courses.Skip(pageIndex * pageSize).Take(pageSize);
				var result = courses.Select(c => new OfferedCourseExams
				{
					SemesterID = c.SemesterID,
					OfferedCourseID = c.OfferedCourseID,
					CourseID = c.Cours.CourseID,
					Title = c.Cours.Title,
					Program = c.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = c.SemesterNo,
					Section = c.Section,
					Shift = c.Shift,
					Teacher = c.FacultyMember.Name,
				}).AsQueryable();

				var registeredCourses = aspireContext.RegisteredCourses.Where(rc => rc.OfferedCours.SemesterID == semesterID && rc.OfferedCours.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				var listOfferedCourseExams = new List<OfferedCourseExams>();
				foreach (var course in result)
				{
					var regCourses = registeredCourses.Where(rc => rc.OfferedCourseID == course.OfferedCourseID);
					course.Students = regCourses.Count();
					var gradeTypes = Enum.GetValues(typeof(ExamGrades)).Cast<ExamGrades>();
					foreach (var gradeType in gradeTypes)
					{
						switch (gradeType)
						{
							case ExamGrades.A:
								course.GradeACount = regCourses.Count(rc => rc.Grade == (byte)gradeType);
								break;
							case ExamGrades.AMinus:
								course.GradeAMinusCount = regCourses.Count(rc => rc.Grade == (byte)gradeType);
								break;
							case ExamGrades.BPlus:
								course.GradeBPlusCount = regCourses.Count(rc => rc.Grade == (byte)gradeType);
								break;
							case ExamGrades.B:
								course.GradeBCount = regCourses.Count(rc => rc.Grade == (byte)gradeType);
								break;
							case ExamGrades.BMinus:
								course.GradeBMinusCount = regCourses.Count(rc => rc.Grade == (byte)gradeType);
								break;
							case ExamGrades.CPlus:
								course.GradeCPlusCount = regCourses.Count(rc => rc.Grade == (byte)gradeType);
								break;
							case ExamGrades.C:
								course.GradeCCount = regCourses.Count(rc => rc.Grade == (byte)gradeType);
								break;
							case ExamGrades.CMinus:
								course.GradeCMinusCount = regCourses.Count(rc => rc.Grade == (byte)gradeType);
								break;
							case ExamGrades.DPlus:
								course.GradeDPlusCount = regCourses.Count(rc => rc.Grade == (byte)gradeType);
								break;
							case ExamGrades.D:
								course.GradeDCount = regCourses.Count(rc => rc.Grade == (byte)gradeType);
								break;
							case ExamGrades.F:
								course.GradeFCount = regCourses.Count(rc => rc.Grade == (byte)gradeType);
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}
					}
					listOfferedCourseExams.Add(course);
				}
				return listOfferedCourseExams;
			}
		}

		public static OfferedCourseExamsDetails GetOfferedCourseExamDetails(int offeredCourseID)
		{
			using (var aspireContext = new AspireContext())
			{
				var examDetails = aspireContext.OfferedCourses.Where(oc => oc.OfferedCourseID == offeredCourseID).Select(oc => new OfferedCourseExamsDetails
				{
					OfferedCourseID = oc.OfferedCourseID,
					SemesterID = oc.SemesterID,
					CourseCode = oc.Cours.CourseCode,
					Title = oc.Cours.Title,
					CreditHours = oc.Cours.CreditHours,
					ContactHours = oc.Cours.ContactHours,
					OcProgram = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					OcSemesterNo = oc.SemesterNo,
					OcSection = oc.Section,
					OcShift = oc.Shift,
					Teacher = oc.FacultyMember.Name,
					Category = oc.Cours.CourseCategory,
					CourseType = oc.Cours.CourseType
				}).SingleOrDefault();
				if (examDetails == null)
					return null;

				examDetails.Details = aspireContext.RegisteredCourses.Where(rc => rc.OfferedCourseID == examDetails.OfferedCourseID).Select(rc => new OfferedCourseExamsDetails.ExamDetails
				{
					RegisteredCourseID = rc.RegisteredCourseID,
					Program = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = rc.OfferedCours.SemesterNo,
					Section = rc.OfferedCours.Section,
					Shift = rc.OfferedCours.Shift,
					Enrollment = rc.Student.Enrollment,
					RegistrationNo = rc.Student.RegistrationNo,
					Name = rc.Student.Name,
					AssignmentsMarks = rc.Assignments,
					QuizzesMarks = rc.Quizzes,
					MidMarks = rc.Mid,
					FinalMarks = rc.Final,
					Total = rc.Total,
					Grade = rc.Grade
				}).ToList();
				return examDetails;
			}
		}

	}
}
