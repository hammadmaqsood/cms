﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExecutiveInformation.Executive.Admissions
{
	public static class ProgramWiseSummary
	{
		private static Common.Permissions.Executive.ExecutiveModulePermissions DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class AdmissionsSummary
		{
			internal AdmissionsSummary() { }
			public int AdmissionOpenProgramID { get; internal set; }
			public short SemesterID { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public string DepartmentName { get; internal set; }
			public int ProgramID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public int IntakeTargetStudentsCount { get; internal set; }
			public ProgramDurations DurationEnum { get; internal set; }
			public int NationalApplied { get; internal set; }
			public int ForeignApplied { get; internal set; }
			public int AdmissionProcessingFeeSubmitted { get; internal set; }
			public int ETSScorer { get; internal set; }
			public int AppearedInEntryTest { get; internal set; }
			public int ETSScorerAndAppearedInEntryTest { get; internal set; }
			public int ETSScorerOrAppearedInEntryTest { get; internal set; }
			public int AppearedInInterview { get; internal set; }
			public int InterviewCleared { get; internal set; }
			public List<Detail> InterviewClearedShiftedFromOtherProgramsDetails { get; internal set; }
			public List<Detail> InterviewClearedShiftedToOtherProgramsDetails { get; internal set; }
			public List<Detail> DeferredFromOtherProgramsDetailsBeforeEnrollmentGeneration { get; internal set; }
			public List<Detail> DeferredToOtherProgramsDetailsBeforeEnrollmentGeneration { get; internal set; }
			public int AdmissionFeeChallansIssued { get; internal set; }
			public int AdmissionFeePaid { get; internal set; }
			public int AdmissionFeeRefunded { get; internal set; }
			public List<Students> AdmittedStudents { get; internal set; }

			public sealed class Detail
			{
				internal Detail() { }
				public int AdmissionOpenProgramID { get; internal set; }
				public string ProgramAlias { get; internal set; }
				public short SemesterID { get; internal set; }
				public int Count { get; internal set; }
			}

			public sealed class Students
			{
				internal Students() { }
				public int AdmissionOpenProgramID { get; internal set; }
				public bool Foreigner { get; internal set; }
				public Model.Entities.Student.Statuses? StatusEnum { get; internal set; }
				public Model.Entities.Student.AdmissionTypes AdmissionTypeEnum { get; internal set; }
				public int StudentsCount { get; internal set; }
			}
		}

		public static List<AdmissionsSummary> GetProgramWiseAdmissionSummary(short semesterID, int? departmentID, int? programID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var admissionOpenProgramsQuery = from p in aspireContext.Programs
												 join aop in aspireContext.AdmissionOpenPrograms on p.ProgramID equals aop.ProgramID
												 select new
												 {
													 AdmissionOpenProgramID = (int?)aop.AdmissionOpenProgramID,
													 InstituteID = (int?)p.InstituteID,
													 ProgramID = (int?)p.ProgramID,
													 DurationEnum = (ProgramDurations?)p.Duration,
													 p.ProgramAlias,
													 SemesterID = (short?)aop.SemesterID,
													 //IntakeTargetStudentsCount = (int?)aop.IntakeTargetStudentsCount
												 };
				var candidatesQuery = from c in aspireContext.Candidates
									  join cap in aspireContext.CandidateAppliedPrograms on c.CandidateID equals cap.CandidateID
									  join aopChoice1 in admissionOpenProgramsQuery on cap.Choice1AdmissionOpenProgramID equals aopChoice1.AdmissionOpenProgramID
									  where aopChoice1.InstituteID == loginHistory.InstituteID
									  select new
									  {
										  c.ForeignStudent,
										  cap.CandidateAppliedProgramID,
										  Choice1InstituteID = aopChoice1.InstituteID.Value,
										  Choice1ProgramID = aopChoice1.ProgramID.Value,
										  cap.Choice1AdmissionOpenProgramID,
										  Choice1ProgramAlias = aopChoice1.ProgramAlias,
										  Choice1SemesterID = aopChoice1.SemesterID.Value,
										  cap.AdmittedAdmissionOpenProgramID,
										  cap.DeferredToAdmittedAdmissionOpenProgramID,
										  cap.AccountTransactionID,
										  cap.EntryTestMarksPercentage,
										  cap.ETSObtained,
										  cap.InterviewDate,
										  StatusEnum = (CandidateAppliedProgram.Statuses?)cap.Status,
										  cap.StudentID
									  };

				var finalQuery = from cap in candidatesQuery
								 join aopAdmitted in admissionOpenProgramsQuery on cap.AdmittedAdmissionOpenProgramID equals aopAdmitted.AdmissionOpenProgramID into admittedGroup
								 from admittedG in admittedGroup.DefaultIfEmpty()
								 join aopDeferred in admissionOpenProgramsQuery on cap.DeferredToAdmittedAdmissionOpenProgramID equals aopDeferred.AdmissionOpenProgramID into deferredGroup
								 from deferredG in deferredGroup.DefaultIfEmpty()
								 select new
								 {
									 cap.ForeignStudent,
									 cap.CandidateAppliedProgramID,
									 cap.Choice1AdmissionOpenProgramID,
									 cap.Choice1ProgramAlias,
									 cap.Choice1SemesterID,
									 cap.Choice1ProgramID,
									 cap.Choice1InstituteID,
									 cap.AdmittedAdmissionOpenProgramID,
									 cap.DeferredToAdmittedAdmissionOpenProgramID,
									 cap.AccountTransactionID,
									 cap.EntryTestMarksPercentage,
									 cap.ETSObtained,
									 cap.InterviewDate,
									 cap.StatusEnum,
									 cap.StudentID,
									 AdmittedInstituteID = admittedG != null ? admittedG.InstituteID : null,
									 AdmittedProgramID = admittedG != null ? admittedG.ProgramID : null,
									 AdmittedProgramAlias = admittedG != null ? admittedG.ProgramAlias : null,
									 AdmittedSemesterID = admittedG != null ? admittedG.SemesterID : null,
									 DeferredInstituteID = deferredG != null ? deferredG.InstituteID : null,
									 DeferredProgramID = deferredG != null ? deferredG.ProgramID : null,
									 DeferredProgramAlias = deferredG != null ? deferredG.ProgramAlias : null,
									 DeferredSemesterID = deferredG != null ? deferredG.SemesterID : null,
								 };

				var finalResult = finalQuery
					.Where(c => c.Choice1InstituteID == loginHistory.InstituteID)
					.Where(c => c.Choice1SemesterID == semesterID || c.AdmittedSemesterID == semesterID || c.DeferredSemesterID == semesterID)
					.ToList();

				var summaries = (from p in aspireContext.Programs
								 join aop in aspireContext.AdmissionOpenPrograms on p.ProgramID equals aop.ProgramID
								 join d in aspireContext.Departments on p.DepartmentID equals d.DepartmentID into grp
								 from g in grp.DefaultIfEmpty()
								 where aop.SemesterID == semesterID && p.InstituteID == loginHistory.InstituteID
									 && (departmentID == null || p.DepartmentID == departmentID.Value)
									 && (programID == null || p.ProgramID == programID.Value)
								 select new AdmissionsSummary
								 {
									 AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
									 ProgramID = aop.ProgramID,
									 DepartmentID = p.DepartmentID,
									 ProgramAlias = p.ProgramAlias,
									 SemesterID = aop.SemesterID,
									 DurationEnum = (ProgramDurations)p.Duration,
									 IntakeTargetStudentsCount = aop.IntakeTargetStudentsCount,
									 DepartmentName = g == null ? null : g.DepartmentAlias,
								 }).ToList();

				var feeChallans = (from p in aspireContext.Programs
								   join aop in aspireContext.AdmissionOpenPrograms on p.ProgramID equals aop.ProgramID
								   join cap in aspireContext.CandidateAppliedPrograms on aop.AdmissionOpenProgramID equals cap.AdmittedAdmissionOpenProgramID
								   join sf in aspireContext.StudentFees on cap.CandidateAppliedProgramID equals sf.CandidateAppliedProgramID
								   where aop.SemesterID == semesterID
									  && (programID == null || aop.ProgramID == programID.Value)
									  && (departmentID == null || p.DepartmentID == departmentID.Value)
								   select new
								   {
									   aop.AdmissionOpenProgramID,
									   cap.CandidateAppliedProgramID,
									   FeeTypeEnum = (StudentFee.FeeTypes)sf.FeeType,
									   StatusEnum = (StudentFee.Statuses)sf.Status
								   }).GroupBy(sf => new
								   {
									   sf.AdmissionOpenProgramID,
									   sf.FeeTypeEnum,
									   sf.StatusEnum
								   }).Select(g => new
								   {
									   g.Key.AdmissionOpenProgramID,
									   g.Key.StatusEnum,
									   g.Key.FeeTypeEnum,
									   CandidatesCount = g.Select(gg => gg.CandidateAppliedProgramID).Distinct().Count()
								   }).ToList();


				var students = (from p in aspireContext.Programs
								join aop in aspireContext.AdmissionOpenPrograms on p.ProgramID equals aop.ProgramID
								join s in aspireContext.Students on aop.AdmissionOpenProgramID equals s.AdmissionOpenProgramID
								where aop.SemesterID == semesterID
								   && (programID == null || aop.ProgramID == programID.Value)
								   && (departmentID == null || p.DepartmentID == departmentID.Value)
								select new
								{
									aop.AdmissionOpenProgramID,
									s.Status,
									s.AdmissionType,
									Foreigner = s.Nationality != null && s.Nationality != "PAKISTANI"
								})
								.GroupBy(s => new { s.AdmissionOpenProgramID, s.Status, s.AdmissionType, s.Foreigner })
								.Select(g => new AdmissionsSummary.Students
								{
									AdmissionOpenProgramID = g.Key.AdmissionOpenProgramID,
									StatusEnum = (Model.Entities.Student.Statuses?)g.Key.Status,
									AdmissionTypeEnum = (Model.Entities.Student.AdmissionTypes)g.Key.AdmissionType,
									Foreigner = g.Key.Foreigner,
									StudentsCount = g.Count()
								})
								.ToList();

				foreach (var summary in summaries)
				{
					var choice1Candidates = finalResult.Where(c => c.Choice1AdmissionOpenProgramID == summary.AdmissionOpenProgramID);
					var national = choice1Candidates.GroupBy(c => c.ForeignStudent)
					   .Select(c => new
					   {
						   Foreign = c.Key,
						   Students = c.Count()
					   })
					   .ToList();
					summary.NationalApplied = national.SingleOrDefault(c => !c.Foreign)?.Students ?? 0;
					summary.ForeignApplied = national.SingleOrDefault(c => c.Foreign)?.Students ?? 0;
					var choice1ProcessingFeeSubmittedCandidates = choice1Candidates.Where(c => c.AccountTransactionID != null);
					summary.AdmissionProcessingFeeSubmitted = choice1ProcessingFeeSubmittedCandidates.Count();
					summary.ETSScorer = choice1ProcessingFeeSubmittedCandidates.Count(c => c.ETSObtained != null && c.EntryTestMarksPercentage == null);
					summary.AppearedInEntryTest = choice1ProcessingFeeSubmittedCandidates.Count(c => c.ETSObtained == null && c.EntryTestMarksPercentage != null);
					summary.ETSScorerAndAppearedInEntryTest = choice1ProcessingFeeSubmittedCandidates.Count(c => c.ETSObtained != null && c.EntryTestMarksPercentage != null);
					summary.ETSScorerOrAppearedInEntryTest = choice1ProcessingFeeSubmittedCandidates.Count(c => c.ETSObtained != null || c.EntryTestMarksPercentage != null);
					var choice1InterviewedCandidates = choice1ProcessingFeeSubmittedCandidates.Where(c => c.InterviewDate != null);
					summary.AppearedInInterview = choice1InterviewedCandidates.Count();
					summary.InterviewCleared = choice1InterviewedCandidates.Count(c => c.AdmittedAdmissionOpenProgramID == summary.AdmissionOpenProgramID);
					summary.InterviewClearedShiftedFromOtherProgramsDetails = finalResult
						.Where(c => c.Choice1AdmissionOpenProgramID != summary.AdmissionOpenProgramID)
						.Where(c => c.AdmittedAdmissionOpenProgramID == summary.AdmissionOpenProgramID)
						.GroupBy(c => new { c.Choice1AdmissionOpenProgramID, c.Choice1ProgramAlias, c.Choice1SemesterID })
						.Select(g => new AdmissionsSummary.Detail
						{
							AdmissionOpenProgramID = g.Key.Choice1AdmissionOpenProgramID,
							SemesterID = g.Key.Choice1SemesterID,
							ProgramAlias = g.Key.Choice1ProgramAlias,
							Count = g.Count()
						}).ToList();
					summary.InterviewClearedShiftedToOtherProgramsDetails = choice1InterviewedCandidates
						.Where(c => c.AdmittedAdmissionOpenProgramID != summary.AdmissionOpenProgramID && c.AdmittedAdmissionOpenProgramID != null)
						.GroupBy(c => new { c.AdmittedAdmissionOpenProgramID, c.AdmittedProgramAlias, c.AdmittedSemesterID })
						.Select(g => new AdmissionsSummary.Detail
						{
							AdmissionOpenProgramID = g.Key.AdmittedAdmissionOpenProgramID.Value,
							SemesterID = g.Key.AdmittedSemesterID.Value,
							ProgramAlias = g.Key.AdmittedProgramAlias,
							Count = g.Count()
						}).ToList();
					summary.AdmissionFeeChallansIssued = feeChallans
						.Where(sf => sf.AdmissionOpenProgramID == summary.AdmissionOpenProgramID && sf.FeeTypeEnum == StudentFee.FeeTypes.Challan)
						.Sum(sf => sf.CandidatesCount);
					summary.AdmissionFeePaid = feeChallans
						.Where(sf => sf.AdmissionOpenProgramID == summary.AdmissionOpenProgramID && sf.FeeTypeEnum == StudentFee.FeeTypes.Challan && sf.StatusEnum == StudentFee.Statuses.Paid)
						.Sum(sf => sf.CandidatesCount);
					summary.AdmissionFeeRefunded = finalResult
						.Count(c => c.AdmittedAdmissionOpenProgramID == summary.AdmissionOpenProgramID && c.StatusEnum == CandidateAppliedProgram.Statuses.Refunded);

					summary.DeferredFromOtherProgramsDetailsBeforeEnrollmentGeneration = finalResult
						.Where(c => c.AdmittedAdmissionOpenProgramID != summary.AdmissionOpenProgramID && c.DeferredToAdmittedAdmissionOpenProgramID == summary.AdmissionOpenProgramID)
						.GroupBy(c => new
						{
							AdmissionOpenProgramID = c.AdmittedAdmissionOpenProgramID.Value,
							SemesterID = c.AdmittedSemesterID.Value,
							ProgramAlias = c.AdmittedProgramAlias,
						}).Select(g => new AdmissionsSummary.Detail
						{
							AdmissionOpenProgramID = g.Key.AdmissionOpenProgramID,
							ProgramAlias = g.Key.ProgramAlias,
							SemesterID = g.Key.SemesterID,
							Count = g.Count()
						}).ToList();

					summary.DeferredToOtherProgramsDetailsBeforeEnrollmentGeneration = finalResult
						.Where(c => c.AdmittedAdmissionOpenProgramID == summary.AdmissionOpenProgramID && c.DeferredToAdmittedAdmissionOpenProgramID != null && c.DeferredToAdmittedAdmissionOpenProgramID != summary.AdmissionOpenProgramID)
						.GroupBy(c => new
						{
							AdmissionOpenProgramID = c.DeferredToAdmittedAdmissionOpenProgramID.Value,
							SemesterID = c.DeferredSemesterID.Value,
							ProgramAlias = c.DeferredProgramAlias,
						}).Select(g => new AdmissionsSummary.Detail
						{
							AdmissionOpenProgramID = g.Key.AdmissionOpenProgramID,
							ProgramAlias = g.Key.ProgramAlias,
							SemesterID = g.Key.SemesterID,
							Count = g.Count()
						}).ToList();

					summary.AdmittedStudents = students.Where(s => s.AdmissionOpenProgramID == summary.AdmissionOpenProgramID).ToList();
				}

				return summaries.OrderBy(s => s.DepartmentName).ThenBy(s => s.ProgramAlias).ToList();
			}
		}
	}
}
