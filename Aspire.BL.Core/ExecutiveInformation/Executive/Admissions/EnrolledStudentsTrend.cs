﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Data;
using System.Linq;

namespace Aspire.BL.Core.ExecutiveInformation.Executive.Admissions
{
	public static class EnrolledStudentsTrend
	{
		private static Common.Permissions.Executive.ExecutiveModulePermissions DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.CourseRegistration.Module, UserGroupPermission.PermissionValues.Allowed);
		}
		public static DataTable GetEnrolledStudentsTrend(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);

				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms
					.Where(aop => aop.Program.InstituteID == loginHistory.InstituteID)
					.Select(aop => new
					{
						aop.ProgramID,
						aop.Program.ProgramAlias,
						aop.SemesterID,
						Students = aop.Students.Count()
					})
					.OrderBy(aop => aop.ProgramAlias)
					.ToList();

				DataTable table = new DataTable();

				var programIDColumn = table.Columns.Add("ProgramID", typeof(int));
				table.PrimaryKey = new[] { programIDColumn };
				var programColumn = table.Columns.Add("Program", typeof(string));
				var semesterIDs = admissionOpenPrograms.Select(aop => aop.SemesterID).Distinct().OrderByDescending(s => s).ToList();
				semesterIDs.ForEach(semesterID => table.Columns.Add(semesterID.ToSemesterString(), typeof(int)));

				admissionOpenPrograms.ForEach(aop =>
				{
					var row = table.Rows.Find(aop.ProgramID) ?? table.Rows.Add(aop.ProgramID, aop.ProgramAlias);
					row[aop.SemesterID.ToSemesterString()] = aop.Students;
				});
				table.PrimaryKey = null;
				table.Columns.Remove(programIDColumn);
				return table;
			}
		}
	}
}
