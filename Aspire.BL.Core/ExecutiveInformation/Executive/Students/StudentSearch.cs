﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.ExecutiveInformation.Executive.Students
{
	public static class StudentSearch
	{
		private static Common.Permissions.Executive.ExecutiveGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandExecutivePermissions(loginSessionGuid, ExecutivePermissions.StudentInformation.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class SearchStudentsResult
		{
			public sealed class Student
			{
				internal Student() { }
				public int StudentID { get; internal set; }
				public short IntakeSemesterID { get; internal set; }
				public Model.Entities.Student.AdmissionTypes AdmissionTypeEnum { get; internal set; }
				public string Enrollment { get; internal set; }
				public int? RegistrationNo { get; internal set; }
				public string Name { get; internal set; }
				public string FatherName { get; internal set; }
				public string ProgramAlias { get; internal set; }
				public Genders GenderEnum { get; internal set; }
				public string Nationality { get; internal set; }
				public Model.Entities.Student.Statuses? StatusEnum { get; internal set; }
			}

			internal SearchStudentsResult() { }
			public int VirtualItemCount { get; internal set; }
			public List<Student> Students { get; internal set; }
		}

		public static SearchStudentsResult SearchStudents(short? intakeSemesterID, Model.Entities.Student.AdmissionTypes? admissionTypeEnum, string enrollment, int? registrationNo, string name, string fatherName, int? programID, Genders? genderEnum, string nationality, Model.Entities.Student.Statuses? statusEnum, bool statusEnumNullAsAll, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var students = aspireContext.Students
					.Where(s => s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				if (intakeSemesterID != null)
					students = students.Where(s => s.AdmissionOpenProgram.SemesterID == intakeSemesterID.Value);
				if (admissionTypeEnum != null)
					students = students.Where(s => s.AdmissionType == (byte)admissionTypeEnum.Value);
				if (!string.IsNullOrWhiteSpace(enrollment))
					students = students.Where(s => s.Enrollment == enrollment);
				if (registrationNo != null)
					students = students.Where(s => s.RegistrationNo == registrationNo.Value);
				if (!string.IsNullOrWhiteSpace(name))
					students = students.Where(s => s.Name.Contains(name));
				if (!string.IsNullOrWhiteSpace(fatherName))
					students = students.Where(s => s.FatherName.Contains(fatherName));
				if (programID != null)
					students = students.Where(s => s.AdmissionOpenProgram.ProgramID == programID.Value);
				if (genderEnum != null)
					students = students.Where(s => s.Gender == (byte)genderEnum.Value);
				nationality = nationality.ToNullIfWhiteSpace();
				if (nationality != null)
				{
					if (nationality == "PAKISTANI")
						students = students.Where(s => s.Nationality == null || s.Nationality == nationality);
					else
						students = students.Where(s => s.Nationality != null && s.Nationality != "PAKISTANI");
				}
				if (statusEnum != null)
					students = students.Where(s => s.Status == (byte)statusEnum.Value);
				else
				{
					if (!statusEnumNullAsAll)
						students = students.Where(s => s.Status == null);
				}

				var virtualItemCount = students.Count();
				var studentsQuery = students.Select(s => new SearchStudentsResult.Student
				{
					StudentID = s.StudentID,
					IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
					AdmissionTypeEnum = (Model.Entities.Student.AdmissionTypes)s.AdmissionType,
					Enrollment = s.Enrollment,
					RegistrationNo = s.RegistrationNo,
					Name = s.Name,
					FatherName = s.FatherName,
					ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
					GenderEnum = (Genders)s.Gender,
					Nationality = s.Nationality,
					StatusEnum = (Model.Entities.Student.Statuses?)s.Status
				});

				switch (sortExpression)
				{
					case nameof(SearchStudentsResult.Student.IntakeSemesterID):
						studentsQuery = studentsQuery.OrderBy(sortDirection, s => s.IntakeSemesterID);
						break;
					case nameof(SearchStudentsResult.Student.AdmissionTypeEnum):
						studentsQuery = studentsQuery.OrderBy(sortDirection, s => s.AdmissionTypeEnum);
						break;
					case nameof(SearchStudentsResult.Student.Enrollment):
						studentsQuery = studentsQuery.OrderBy(sortDirection, s => s.Enrollment);
						break;
					case nameof(SearchStudentsResult.Student.RegistrationNo):
						studentsQuery = studentsQuery.OrderBy(sortDirection, s => s.RegistrationNo);
						break;
					case nameof(SearchStudentsResult.Student.Name):
						studentsQuery = studentsQuery.OrderBy(sortDirection, s => s.Name);
						break;
					case nameof(SearchStudentsResult.Student.FatherName):
						studentsQuery = studentsQuery.OrderBy(sortDirection, s => s.FatherName);
						break;
					case nameof(SearchStudentsResult.Student.ProgramAlias):
						studentsQuery = studentsQuery.OrderBy(sortDirection, s => s.ProgramAlias);
						break;
					case nameof(SearchStudentsResult.Student.GenderEnum):
						studentsQuery = studentsQuery.OrderBy(sortDirection, s => s.GenderEnum);
						break;
					case nameof(SearchStudentsResult.Student.Nationality):
						studentsQuery = studentsQuery.OrderBy(sortDirection, s => s.Nationality);
						break;
					case nameof(SearchStudentsResult.Student.StatusEnum):
						studentsQuery = studentsQuery.OrderBy(sortDirection, s => s.StatusEnum);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return new SearchStudentsResult
				{
					VirtualItemCount = virtualItemCount,
					Students = studentsQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList()
				};
			}
		}
	}
}
