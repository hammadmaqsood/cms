﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExecutiveInformation.Executive.Students
{
	public static class BatchwiseStudents
	{
		private static Common.Permissions.Executive.ExecutiveModulePermissions DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.StudentInformation.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class Student
		{
			public int StudentID { get; internal set; }
			public int AdmissionOpenProgramID { get; internal set; }
			public string DepartmentAlias { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterID { get; internal set; }
			public string IntakeSemester => this.SemesterID.ToSemesterString();
			public string Enrollment { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string Name { get; internal set; }
			public byte? StudentStatus { get; internal set; }
			public string StatusFullName => ((Model.Entities.Student.Statuses?)this.StudentStatus)?.ToFullName();
		}

		public static List<Student> GetBatchwiseStudents(short semesterID, int? departmentID, int? programID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var modulePermissions = aspireContext.DemandPermissions(loginSessionGuid);
				var admissionOpenPrograms = modulePermissions.Departments.SelectMany(d => d.Programs).SelectMany(p => p.AdmissionOpenPrograms).Where(aop => aop.SemesterID == semesterID);
				if (departmentID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.Program.DepartmentID == departmentID.Value);
				if (programID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.ProgramID == programID.Value);

				var students = admissionOpenPrograms.SelectMany(aop => aop.Students).Select(s => new Student
				{
					StudentID = s.StudentID,
					AdmissionOpenProgramID = s.AdmissionOpenProgramID,
					DepartmentAlias = s.AdmissionOpenProgram.Program.Department.DepartmentAlias,
					ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterID = s.AdmissionOpenProgram.SemesterID,
					Enrollment = s.Enrollment,
					RegistrationNo = s.RegistrationNo,
					Name = s.Name,
					StudentStatus = s.Status,
				});

				return students.ToList();
			}
		}

	}
}
