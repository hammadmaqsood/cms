﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Aspire.BL.Core.ExecutiveInformation.Executive.Students
{
	public static class RegisteredStrengths
	{
		private static Common.Permissions.Executive.ExecutiveModulePermissions DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.CourseRegistration.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		[Flags]
		public enum StudentsStrengthGroupByFields
		{
			Department = 1 << 0,
			Program = 1 << 1,
			SemesterNo = 1 << 2,
			Section = 1 << 3,
			Shift = 1 << 4,
			Gender = 1 << 5,
			Category = 1 << 6,
			Nationality = 1 << 7,
			Status = 1 << 8,
			FeePaid = 1 << 9,
		}

		public static DataTable GetStudentsStrength(short semesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, Genders? genderEnum, Categories? categoryEnum, Model.Entities.Student.Statuses? statusEnum, StudentsStrengthGroupByFields groupByFields, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.IncreaseCommandTimeout();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var programs = loginHistory.Programs;

				if (departmentID != null)
					programs = programs.Where(p => p.DepartmentID == departmentID.Value);

				if (programID != null)
					programs = programs.Where(p => p.ProgramID == programID.Value);

				var students = programs.SelectMany(p => p.AdmissionOpenPrograms).SelectMany(aop => aop.Students);
				if (genderEnum != null)
				{
					var gender = (byte?)genderEnum;
					students = students.Where(s => s.Gender == gender.Value);
				}
				if (categoryEnum != null)
				{
					var category = (byte?)categoryEnum;
					students = students.Where(s => s.Category == category.Value);
				}
				if (statusEnum != null)
				{
					var status = (byte?)statusEnum;
					students = students.Where(s => s.Status == status.Value);
				}

				var studentSemesters = students.SelectMany(s => s.StudentSemesters).Where(ss => ss.SemesterID == semesterID && ss.Status==StudentSemester.StatusRegistered);

				if (semesterNoEnum != null)
					studentSemesters = studentSemesters.Where(ss => ss.SemesterNo == (short)semesterNoEnum);

				if (sectionEnum != null)
					studentSemesters = studentSemesters.Where(ss => ss.Section == (int)sectionEnum);

				if (shiftEnum != null)
					studentSemesters = studentSemesters.Where(ss => ss.Shift == (byte)shiftEnum);

				var groupByDepartmentID = groupByFields.HasFlag(StudentsStrengthGroupByFields.Department);
				var groupByProgramID = groupByFields.HasFlag(StudentsStrengthGroupByFields.Program);
				var groupBySemesterNo = groupByFields.HasFlag(StudentsStrengthGroupByFields.SemesterNo);
				var groupBySection = groupByFields.HasFlag(StudentsStrengthGroupByFields.Section);
				var groupByShift = groupByFields.HasFlag(StudentsStrengthGroupByFields.Shift);
				var groupByGender = groupByFields.HasFlag(StudentsStrengthGroupByFields.Gender);
				var groupByCategory = groupByFields.HasFlag(StudentsStrengthGroupByFields.Category);
				var groupByNationality = groupByFields.HasFlag(StudentsStrengthGroupByFields.Nationality);
				var groupByStatus = groupByFields.HasFlag(StudentsStrengthGroupByFields.Status);
				var groupByFeePaid = groupByFields.HasFlag(StudentsStrengthGroupByFields.FeePaid);
				if (groupByFeePaid)
					aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed);

				var groupedResult = studentSemesters.GroupBy(ss => new
				{
					ss.SemesterID,
					DepartmentID = groupByDepartmentID ? ss.Student.AdmissionOpenProgram.Program.DepartmentID : null,
					DepartmentName = groupByDepartmentID ? ss.Student.AdmissionOpenProgram.Program.Department.DepartmentName : null,
					ProgramID = groupByProgramID ? ss.Student.AdmissionOpenProgram.ProgramID : (int?)null,
					ProgramAlias = groupByProgramID ? ss.Student.AdmissionOpenProgram.Program.ProgramAlias : null,
					SemesterNo = groupBySemesterNo ? ss.SemesterNo : (short?)null,
					Section = groupBySection ? ss.Section : (int?)null,
					Shift = groupByShift ? ss.Shift : (byte?)null,
					Gender = groupByGender ? ss.Student.Gender : (byte?)null,
					Category = groupByCategory ? ss.Student.Category : (byte?)null,
					Nationality = groupByNationality ? ss.Student.Nationality : null,
					Status = groupByStatus ? ss.Student.Status : null,
				});
				dynamic result;
				if (groupByFeePaid)
					result = groupedResult.Select(ss => new
					{
						ss.Key,
						Students = ss.Count(),
						FeePaid = ss.SelectMany(sss => sss.Student.StudentFees)
									 .Where(sf => sf.SemesterID == semesterID && sf.FeeType == StudentFee.FeeTypeChallan && sf.Status == StudentFee.StatusPaid)
									 .Sum(sf => (int?)sf.GrandTotalAmount)
					}).ToList();
				else
					result = groupedResult.Select(ss => new
					{
						ss.Key,
						Students = ss.Count(),
						FeePaid = 0
					}).ToList();

				var table = new DataTable("StudentStrength");
				int? departmentColumnIndex = null;
				int? programColumnIndex = null;
				int? semesterNoColumnIndex = null;
				int? sectionColumnIndex = null;
				int? shiftColumnIndex = null;
				int? genderColumnIndex = null;
				int? categoryColumnIndex = null;
				int? nationalityColumnIndex = null;
				int? statusColumnIndex = null;
				int? feePaidColumnIndex = null;

				var sortExpressions = new List<string>();
				table.Columns.Add("Semester", typeof(string));
				if (groupByDepartmentID)
				{
					departmentColumnIndex = table.Columns.Add("Department", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[departmentColumnIndex.Value].ColumnName);
				}
				if (groupByProgramID)
				{
					programColumnIndex = table.Columns.Add("Program", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[programColumnIndex.Value].ColumnName);
				}
				if (groupBySemesterNo)
				{
					semesterNoColumnIndex = table.Columns.Add("Semester No.", typeof(byte)).Ordinal;
					sortExpressions.Add(table.Columns[semesterNoColumnIndex.Value].ColumnName);
				}
				if (groupBySection)
				{
					sectionColumnIndex = table.Columns.Add("Section", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[sectionColumnIndex.Value].ColumnName);
				}
				if (groupByShift)
				{
					shiftColumnIndex = table.Columns.Add("Shift", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[shiftColumnIndex.Value].ColumnName);
				}
				if (groupByGender)
				{
					genderColumnIndex = table.Columns.Add("Gender", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[genderColumnIndex.Value].ColumnName);
				}
				if (groupByCategory)
				{
					categoryColumnIndex = table.Columns.Add("Category", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[categoryColumnIndex.Value].ColumnName);
				}
				if (groupByNationality)
				{
					nationalityColumnIndex = table.Columns.Add("Nationality", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[nationalityColumnIndex.Value].ColumnName);
				}
				if (groupByStatus)
				{
					statusColumnIndex = table.Columns.Add("Status", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[statusColumnIndex.Value].ColumnName);
				}
				var studentsColumnIndex = table.Columns.Add("Students", typeof(int)).Ordinal;
				if (groupByFeePaid)
				{
					feePaidColumnIndex = table.Columns.Add("Fee Paid", typeof(int)).Ordinal;
				}

				table.DefaultView.Sort = string.Join(",", sortExpressions.Select(e => $"[{e}] ASC"));

				foreach (var record in result)
				{
					var recordSemesterID = (short)record.Key.SemesterID;
					var row = table.Rows.Add(recordSemesterID.ToSemesterString());
					row[0] = recordSemesterID.ToSemesterString();
					if (groupByDepartmentID)
						row[departmentColumnIndex.Value] = ((string)record.Key.DepartmentName).ToNAIfNullOrEmpty();
					if (groupByProgramID)
						row[programColumnIndex.Value] = ((string)record.Key.ProgramAlias).ToNAIfNullOrEmpty();
					if (groupBySemesterNo)
						row[semesterNoColumnIndex.Value] = ((SemesterNos)record.Key.SemesterNo).ToFullName();
					if (groupBySection)
						row[sectionColumnIndex.Value] = ((Sections)record.Key.Section).ToFullName();
					if (groupByShift)
						row[shiftColumnIndex.Value] = ((Shifts)record.Key.Shift).ToFullName();
					if (groupByGender)
						row[genderColumnIndex.Value] = ((Genders)record.Key.Gender).ToFullName();
					if (groupByCategory)
						row[categoryColumnIndex.Value] = ((Categories)record.Key.Category).ToFullName();
					if (groupByNationality)
						row[nationalityColumnIndex.Value] = ((string)record.Key.Nationality).ToNAIfNullOrEmpty();
					if (groupByStatus)
						row[statusColumnIndex.Value] = ((Model.Entities.Student.Statuses?)(byte?)record.Key.Status)?.ToFullName();
					row[studentsColumnIndex] = record.Students;
					if (groupByFeePaid)
						row[feePaidColumnIndex.Value] = record.FeePaid ?? 0;
				}
				return table;
			}
		}
	}
}
