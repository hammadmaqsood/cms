﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FileServer;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.ExecutiveInformation.Executive.Students
{
	public static class StudentProfile
	{
		private static Common.Permissions.Executive.ExecutiveModulePermissions DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.StudentInformation.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public static Model.Entities.Student GetStudent(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.DemandPermissions(loginSessionGuid)
					.Programs.SelectMany(p => p.AdmissionOpenPrograms)
					.SelectMany(aop => aop.Students)
					.Include(s => s.StudentAcademicRecords)
					.Include(s => s.AdmissionOpenProgram.Program)
					.SingleOrDefault(s => s.StudentID == studentID);
			}
		}

		public static byte[] ReadStudentPicture(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandExecutiveActiveSession(loginSessionGuid);
				var students = aspireContext.Programs.Where(p => p.InstituteID == loginHistory.InstituteID)
									.SelectMany(i => i.AdmissionOpenPrograms)
									.SelectMany(i => i.Students);
				if (students.Any(s => s.StudentID == studentID))
					return aspireContext.ReadStudentPicture(studentID);
				return null;
			}
		}
	}
}
