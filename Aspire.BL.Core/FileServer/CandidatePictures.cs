﻿using Aspire.BL.Core.Common;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Linq;

namespace Aspire.BL.Core.FileServer
{
	internal static class CandidatePictures
	{
		private static SystemFolder AddCandidatePictureSystemFolder(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.AddSystemFolder(SystemFolder.FolderTypes.CandidatePicture, loginSessionGuid);
		}

		public enum AddOrUpdateCandidatePngFormatPictureStatuses
		{
			Success,
			ResolutionMustBe400X400,
			InvalidImage
		}

		public static AddOrUpdateCandidatePngFormatPictureStatuses UpdateCandidatePicture(this AspireContext aspireContext, int candidateID, byte[] fileBytes, bool ignoreImageResolution, Guid loginSessionGuid)
		{
			var (finalBytes, status) = StudentPictures.ProcessPngPhoto(fileBytes, ignoreImageResolution);

			switch (status)
			{
				case StudentPictures.ProcessPngPhotoStatuses.ResolutionMustBe400X400:
					return AddOrUpdateCandidatePngFormatPictureStatuses.ResolutionMustBe400X400;
				case StudentPictures.ProcessPngPhotoStatuses.InvalidImage:
					return AddOrUpdateCandidatePngFormatPictureStatuses.InvalidImage;
				case StudentPictures.ProcessPngPhotoStatuses.Success:
					var candidate = aspireContext.Candidates.Single(c => c.CandidateID == candidateID);
					if (candidate.PictureSystemFolderID == null)
						candidate.PictureSystemFolderID = aspireContext.AddCandidatePictureSystemFolder(loginSessionGuid).SystemFolderID;
					aspireContext.AddSystemFileToSingleFileFolder(candidate.PictureSystemFolderID.Value, candidate.CandidateID.ToString(), SystemFile.FileExtensions.Png, finalBytes, loginSessionGuid);
					switch (candidate.StatusEnum)
					{
						case Candidate.Statuses.Blank:
							candidate.StatusEnum = Candidate.Statuses.PictureUploaded;
							break;
						case Candidate.Statuses.PictureUploaded:
							break;
						case Candidate.Statuses.InformationProvided:
							candidate.StatusEnum = candidate.StatusEnum | Candidate.Statuses.PictureUploaded;
							break;
						case Candidate.Statuses.Completed:
							break;
						default:
							throw new NotImplementedEnumException(candidate.StatusEnum);
					}
					aspireContext.SaveChanges(aspireContext.GetUserLoginHistoryID(loginSessionGuid));
					return AddOrUpdateCandidatePngFormatPictureStatuses.Success;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		public static byte[] ReadCandidatePicture(this AspireContext aspireContext, int candidateID)
		{
			var candidate = aspireContext.Candidates.Where(c => c.CandidateID == candidateID).Select(c => new { c.PictureSystemFolderID }).SingleOrDefault();
			if (candidate?.PictureSystemFolderID != null)
				return aspireContext.GetSystemFileFromSingleFileFolder(candidate.PictureSystemFolderID.Value).Read()?.fileBytes;
			return null;
		}
	}
}