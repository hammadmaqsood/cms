﻿using Aspire.BL.Core.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data;

namespace Aspire.BL.Core.FileServer
{
	public static class TemporaryFiles
	{
		private static string ToInstanceGuidString(this Guid instanceGuid)
		{
			return instanceGuid.ToString("N");
		}

		public static SystemFile AddTemporarySystemFile(Guid instanceGuid, string fileName, SystemFile.FileExtensions fileExtensionEnum, byte[] fileBytes, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var instanceGuidString = instanceGuid.ToInstanceGuidString();
				var fileNameWithInstanceGuid = $"{instanceGuidString}{fileName}";
				var systemFile = aspireContext.AddSystemFile(SystemFolders.DefaultSystemFolderIDs[SystemFolder.FolderTypes.Temporary], fileNameWithInstanceGuid, fileExtensionEnum, fileBytes, loginSessionGuid);
				aspireContext.CommitTransaction();
				systemFile.FileName = systemFile.FileName.Substring(instanceGuidString.Length);
				return systemFile;
			}
		}

		public enum DeleteTemporarySystemFileStatuses
		{
			NoRecordFound,
			Success
		}

		private static SystemFile GetTemporarySystemFile(this AspireContext aspireContext, Guid instanceGuid, Guid systemFileID, Guid createdByLoginSessionGuid)
		{
			var createdByUserLoginHistoryID = aspireContext.GetUserLoginHistoryID(createdByLoginSessionGuid);
			var systemFile = aspireContext.GetSystemFile(systemFileID);
			var instanceGuidString = instanceGuid.ToInstanceGuidString();
			if (systemFile == null
				|| systemFile.CreatedByUserLoginHistoryID != createdByUserLoginHistoryID
				|| !systemFile.FileName.StartsWith(instanceGuidString))
				return null;
			return systemFile;
		}

		public static DeleteTemporarySystemFileStatuses DeleteTemporarySystemFile(this AspireContext aspireContext, Guid instanceGuid, Guid systemFileID, Guid createdByLoginSessionGuid)
		{
			var systemFile = aspireContext.GetTemporarySystemFile(instanceGuid, systemFileID, createdByLoginSessionGuid);
			if (systemFile == null)
				return DeleteTemporarySystemFileStatuses.NoRecordFound;
			aspireContext.DeleteSystemFile(systemFile, createdByLoginSessionGuid);
			systemFileID.DeletePhysicalFile();
			return DeleteTemporarySystemFileStatuses.Success;
		}

		public static DeleteTemporarySystemFileStatuses DeleteTemporarySystemFile(Guid instanceGuid, Guid systemFileID, Guid createdByLoginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var result = aspireContext.DeleteTemporarySystemFile(instanceGuid, systemFileID, createdByLoginSessionGuid);
				aspireContext.CommitTransaction();
				return result;
			}
		}

		public static (SystemFile systemFile, byte[] fileBytes)? ReadTemporarySystemFile(this AspireContext aspireContext, Guid instanceGuid, Guid systemFileID, Guid loginSessionGuid)
		{
			var systemFile = aspireContext.GetTemporarySystemFile(instanceGuid, systemFileID, loginSessionGuid);
			if (systemFile == null)
				return null;
			var result = systemFile.Read() ?? throw new InvalidOperationException();
			result.systemFile.FileName = result.systemFile.FileName.Substring(instanceGuid.ToInstanceGuidString().Length);
			return result;
		}

		public static (SystemFile systemFile, byte[] fileBytes)? ReadTemporarySystemFile(Guid instanceGuid, Guid systemFileID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.ReadTemporarySystemFile(instanceGuid, systemFileID, loginSessionGuid);
			}
		}

		public static List<string> GetExcelSheetNames(Lib.Helpers.ExcelFileHelper.OledbProviders provider, Guid instanceGuid, Guid systemFileID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var systemFile = aspireContext.GetTemporarySystemFile(instanceGuid, systemFileID, loginSessionGuid);
				return systemFile.GetExcelSheetNames(provider);
			}
		}

		public static DataTable GetExcelSheetData(Lib.Helpers.ExcelFileHelper.OledbProviders provider, string sheetName, bool hdr, uint imex, Guid instanceGuid, Guid systemFileID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var systemFile = aspireContext.GetTemporarySystemFile(instanceGuid, systemFileID, loginSessionGuid);
				return systemFile.GetExcelSheetData(provider, sheetName, hdr, imex);
			}
		}
	}
}