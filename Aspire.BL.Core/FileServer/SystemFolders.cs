﻿using Aspire.BL.Core.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.FileServer
{
	internal static class SystemFolders
	{
		internal static SystemFolder AddSystemFolder(this AspireContext aspireContext, SystemFolder.FolderTypes folderTypeEnum, Guid createdByLoginSessionGuid)
		{
			var createdByUserLoginHistoryID = aspireContext.GetUserLoginHistoryID(createdByLoginSessionGuid);
			var systemFolder = aspireContext.SystemFolders.Add(new SystemFolder
			{
				SystemFolderID = DefaultSystemFolderIDs.ContainsKey(folderTypeEnum) ? DefaultSystemFolderIDs[folderTypeEnum] : Guid.NewGuid(),
				FolderName = folderTypeEnum.ToString(),
				FolderTypeEnum = folderTypeEnum,
				CreatedByUserLoginHistoryID = createdByUserLoginHistoryID,
				CreatedDate = DateTime.Now,
				DeletedByUserLoginHistoryID = null,
				DeletedDate = null
			});
			aspireContext.SaveChanges(createdByUserLoginHistoryID);
			return systemFolder;
		}

		internal static readonly Dictionary<SystemFolder.FolderTypes, Guid> DefaultSystemFolderIDs = new Dictionary<SystemFolder.FolderTypes, Guid>
		{
			{SystemFolder.FolderTypes.Temporary, new Guid("E84D57FF-A1C6-4B44-BE61-E4DF7CDDDFEA")},
			{SystemFolder.FolderTypes.OfficialDocuments, new Guid("47D718A2-28D5-4B64-8964-D2A0E8D0D686")},
			{SystemFolder.FolderTypes.StudentDocuments, new Guid("40E2B9A1-E674-4B1E-8376-781DAE504DBC")},
			{SystemFolder.FolderTypes.CandidateForeignAcademicRecord, new Guid("ADE2E405-9CEB-43AB-986C-70CEC3B2E809")},
			{SystemFolder.FolderTypes.CandidateForeignDocuments, new Guid("B0D5D3CE-ACCC-4665-94DD-D7D64EB115A4")}
		};

		internal static SystemFolder GetSystemFolder(this AspireContext aspireContext, Guid systemFolderID)
		{
			return aspireContext.SystemFolders.SingleOrDefault(sf => sf.SystemFolderID == systemFolderID && sf.DeletedDate == null && sf.DeletedByUserLoginHistoryID == null);
		}

		private static SystemFolder GetDefaultSystemFolder(this AspireContext aspireContext, SystemFolder.FolderTypes folderTypeEnum)
		{
			return aspireContext.GetSystemFolder(DefaultSystemFolderIDs[folderTypeEnum])
				?? aspireContext.AddSystemFolder(folderTypeEnum, UserLoginHistory.SystemLoginSessionGuid);
		}

		static SystemFolders()
		{
			using (var aspireContext = new AspireContext(true))
			{
				foreach (var defaultFolder in DefaultSystemFolderIDs)
					aspireContext.GetDefaultSystemFolder(defaultFolder.Key);
				aspireContext.CommitTransaction();
			}
		}
	}
}