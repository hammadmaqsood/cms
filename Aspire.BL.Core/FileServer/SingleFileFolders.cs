﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;

namespace Aspire.BL.Core.FileServer
{
	internal static class SingleFileFolders
	{
		public enum DeleteSystemFileFromSingleFileSystemFolderStatuses
		{
			NoRecordFound,
			Success
		}

		internal static DeleteSystemFileFromSingleFileSystemFolderStatuses DeleteSystemFileFromSingleFileFolder(this AspireContext aspireContext, Guid systemFolderID, Guid deletedByLoginSessionGuid)
		{
			var systemFile = aspireContext.GetSystemFileFromSingleFileFolder(systemFolderID);
			if (systemFile == null)
				return DeleteSystemFileFromSingleFileSystemFolderStatuses.NoRecordFound;
			aspireContext.DeleteSystemFile(systemFile, deletedByLoginSessionGuid);
			return DeleteSystemFileFromSingleFileSystemFolderStatuses.Success;
		}


		internal static (SystemFile systemFile, byte[] fileBytes)? ReadSystemFileFromSingleFileFolder(this AspireContext aspireContext, Guid systemFolderID)
		{
			return aspireContext.GetSystemFileFromSingleFileFolder(systemFolderID)?.Read();
		}

		public static SystemFile AddSystemFileToSingleFileFolder(this AspireContext aspireContext, Guid systemFolderID, string fileName, SystemFile.FileExtensions fileExtensionEnum, byte[] fileBytes, Guid loginSessionGuid)
		{
			var deleteSystemFileFromSingleFileSystemFolderStatuses = aspireContext.DeleteSystemFileFromSingleFileFolder(systemFolderID, loginSessionGuid);
			switch (deleteSystemFileFromSingleFileSystemFolderStatuses)
			{
				case DeleteSystemFileFromSingleFileSystemFolderStatuses.NoRecordFound:
				case DeleteSystemFileFromSingleFileSystemFolderStatuses.Success:
					return aspireContext.AddSystemFile(systemFolderID, fileName, fileExtensionEnum, fileBytes, loginSessionGuid);
				default:
					throw new NotImplementedEnumException(deleteSystemFileFromSingleFileSystemFolderStatuses);
			}
		}
	}
}
