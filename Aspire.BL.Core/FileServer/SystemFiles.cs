﻿using Aspire.BL.Core.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace Aspire.BL.Core.FileServer
{
	internal static class SystemFiles
	{
		private static string GetFullFileName(this Guid guid)
		{
			var fileGuidString = guid.ToString("N");
			var i = 0;
			var parts = new[]
			{
				Configurations.Current.AppSettings.FileServerBasePath,
				fileGuidString.Substring(i++ * 3, 3),
				fileGuidString.Substring(i++ * 3, 3),
				fileGuidString.Substring(i++ * 3, 3),
				fileGuidString.Substring(i++ * 3, 3),
				fileGuidString.Substring(i++ * 3, 3),
				fileGuidString.Substring(i++ * 3, 3),
				fileGuidString.Substring(i++ * 3, 3),
				fileGuidString.Substring(i++ * 3, 3),
				fileGuidString.Substring(i++ * 3, 3),
				fileGuidString.Substring(i++ * 3, 3),
				fileGuidString.Substring(i * 3, 2),
				fileGuidString
			};
			return Path.Combine(parts);
		}

		private const string Temp = ".tmp";

		private static (Guid systemFileID, string checksumMD5, int fileSize) SaveFile(byte[] fileBytes)
		{
			if (fileBytes == null || fileBytes.Length == 0)
				throw new ArgumentNullException(nameof(fileBytes));
			var systemFileID = Guid.NewGuid();
			var fileName = systemFileID.GetFullFileName();
			var directory = Path.GetDirectoryName(fileName) ?? throw new InvalidOperationException();
			if (!Directory.Exists(directory))
				Directory.CreateDirectory(directory);
			if (File.Exists(fileName))
				throw new InvalidOperationException($"File already exists. Name: {fileName}");
			fileName = fileName + Temp;
			var checksumMD5 = fileBytes.ComputeChecksumMD5().ToHexFormat();
			File.WriteAllBytes(fileName, fileBytes);
			return (systemFileID, checksumMD5, fileBytes.Length);
		}

		private static SystemFile CommitFile(this SystemFile systemFile)
		{
			var fileName = systemFile.SystemFileID.GetFullFileName();
			File.Move(fileName + Temp, fileName);
			return systemFile;
		}

		internal static SystemFile AddSystemFile(this AspireContext aspireContext, Guid systemFolderID, string fileName, SystemFile.FileExtensions fileExtensionEnum, byte[] fileBytes, Guid loginSessionGuid)
		{
			var (systemFileID, checksumMD5, fileSize) = SaveFile(fileBytes);
			var systemFile = aspireContext.SystemFiles.Add(new SystemFile
			{
				SystemFileID = systemFileID,
				SystemFolderID = systemFolderID,
				CreatedDate = DateTime.Now,
				FileName = fileName.TrimAndCannotBeEmpty().TrimUpToMaxLength(255),
				FileExtensionEnum = fileExtensionEnum,
				CreatedByUserLoginHistoryID = aspireContext.GetUserLoginHistoryID(loginSessionGuid),
				FileSize = fileSize,
				ChecksumMD5 = checksumMD5,
				DeletedDate = null,
				DeletedByUserLoginHistoryID = null,
				FileContentsTypeEnum = SystemFile.FileContentsTypes.None
			});
			aspireContext.SaveChanges(systemFile.CreatedByUserLoginHistoryID);
			return systemFile.CommitFile();
		}

		public enum DeleteFileStatuses
		{
			NoRecordFound,
			Success,
		}

		internal static SystemFile GetSystemFile(this AspireContext aspireContext, Guid systemFileID)
		{
			return aspireContext.SystemFiles.SingleOrDefault(sf => sf.SystemFileID == systemFileID && sf.DeletedDate == null && sf.DeletedByUserLoginHistoryID == null);
		}

		internal static SystemFile GetSystemFileFromSingleFileFolder(this AspireContext aspireContext, Guid systemFolderID)
		{
			return aspireContext.SystemFiles.SingleOrDefault(sf => sf.SystemFolderID == systemFolderID && sf.DeletedDate == null && sf.DeletedByUserLoginHistoryID == null);
		}

		internal static void DeleteSystemFile(this AspireContext aspireContext, SystemFile systemFile, Guid deletedByLoginSessionGuid)
		{
			if (systemFile.DeletedDate != null && systemFile.DeletedByUserLoginHistoryID != null)
				throw new InvalidOperationException($"Already deleted. {nameof(systemFile.SystemFileID)}: {systemFile.SystemFileID}");
			var deletedByUserLoginHistoryID = aspireContext.GetUserLoginHistoryID(deletedByLoginSessionGuid);
			if (systemFile.DeletedDate == null && systemFile.DeletedByUserLoginHistoryID == null)
			{
				systemFile.DeletedByUserLoginHistoryID = deletedByUserLoginHistoryID;
				systemFile.DeletedDate = DateTime.Now;
				aspireContext.SaveChanges(deletedByUserLoginHistoryID);
			}
			else
				throw new InvalidOperationException($"{nameof(systemFile.SystemFileID)}: {systemFile.SystemFileID}");
		}

		public enum DeleteSystemFileStatuses
		{
			NoRecordFound,
			Success
		}

		internal static DeleteSystemFileStatuses DeleteSystemFile(this AspireContext aspireContext, Guid systemFileID, Guid deletedByLoginSessionGuid)
		{
			var systemFile = aspireContext.SystemFiles.SingleOrDefault(sf => sf.SystemFileID == systemFileID && sf.DeletedDate == null && sf.DeletedByUserLoginHistoryID == null);
			if (systemFile == null)
				return DeleteSystemFileStatuses.NoRecordFound;
			aspireContext.DeleteSystemFile(systemFile, deletedByLoginSessionGuid);
			return DeleteSystemFileStatuses.Success;
		}

		internal static (SystemFile systemFile, byte[] fileBytes)? Read(this SystemFile systemFile)
		{
			var fileBytes = File.ReadAllBytes(systemFile.SystemFileID.GetFullFileName());
			if (fileBytes.ComputeChecksumMD5().ToHexFormat() == systemFile.ChecksumMD5)
				return (systemFile, fileBytes);
			throw new InvalidOperationException($"ChecksumMD5 do not match. SystemFileID: {systemFile.SystemFileID}");
		}

		internal static void DeletePhysicalFile(this Guid systemFileID)
		{
			var fileName = systemFileID.GetFullFileName();
			var deletedFileName = fileName + ".del";
			new FileInfo(fileName).MoveTo(deletedFileName);
		}

		internal static (SystemFile systemFile, byte[] fileBytes)? ReadSystemFile(this AspireContext aspireContext, Guid systemFileID)
		{
			return aspireContext.SystemFiles
				.SingleOrDefault(sf => sf.SystemFileID == systemFileID && sf.DeletedByUserLoginHistoryID == null && sf.DeletedDate == null)
				?.Read();
		}

		internal static (SystemFile systemFile, byte[] fileBytes)? ReadSingleFileFromSystemFolder(this AspireContext aspireContext, Guid systemFolderID)
		{
			return aspireContext.SystemFiles
				.SingleOrDefault(sf => sf.SystemFolderID == systemFolderID && sf.DeletedByUserLoginHistoryID == null && sf.DeletedDate == null)
				?.Read();
		}

		internal static List<string> GetExcelSheetNames(this SystemFile systemFile, Lib.Helpers.ExcelFileHelper.OledbProviders provider)
		{
			var fileName = systemFile.SystemFileID.GetFullFileName();
			var connectionString = ExcelFileHelper.GetOledbConnectionString(provider, fileName, systemFile.FileName, true, 1);
			return Lib.Helpers.ExcelFileHelper.GetSheetNames(connectionString);
		}

		internal static DataTable GetExcelSheetData(this SystemFile systemFile, Lib.Helpers.ExcelFileHelper.OledbProviders provider, string sheetName, bool hdr, uint imex)
		{
			var fileName = systemFile.SystemFileID.GetFullFileName();
			var connectionString = ExcelFileHelper.GetOledbConnectionString(provider, fileName, systemFile.FileName, hdr, imex);
			return Lib.Helpers.ExcelFileHelper.GetSheetData(connectionString, sheetName);
		}
	}
}