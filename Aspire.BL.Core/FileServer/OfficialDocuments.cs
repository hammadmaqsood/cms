﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;

namespace Aspire.BL.Core.FileServer
{
	internal static class OfficialDocuments
	{
		public enum AddOfficialDocumentStatuses
		{
			Success
		}

		public static (SystemFile systemFile, AddOfficialDocumentStatuses status) AddOfficialDocument(this AspireContext aspireContext, string documentName, SystemFile.FileExtensions fileExtensionEnum, byte[] fileBytes, Guid loginSessionGuid)
		{
			var systemFile = aspireContext.AddSystemFile(SystemFolders.DefaultSystemFolderIDs[SystemFolder.FolderTypes.OfficialDocuments], documentName, fileExtensionEnum, fileBytes, loginSessionGuid);
			return (systemFile, AddOfficialDocumentStatuses.Success);
		}

		public static byte[] ReadOfficialDocument(this AspireContext aspireContext, Guid systemFileID)
		{
			var result = aspireContext.ReadSystemFile(systemFileID);
			if (result == null)
				return null;
			return result.Value.systemFile.SystemFolderID == SystemFolders.DefaultSystemFolderIDs[SystemFolder.FolderTypes.OfficialDocuments]
				? result.Value.fileBytes
				: null;
		}
	}
}