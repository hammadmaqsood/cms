﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace Aspire.BL.Core.FileServer
{
	internal static class StudentPictures
	{
		private static SystemFolder AddStudentPictureSystemFolder(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.AddSystemFolder(SystemFolder.FolderTypes.StudentPicture, loginSessionGuid);
		}

		public enum UpdateStudentPictureStatuses
		{
			Success,
			ResolutionMustBe400X400,
			InvalidImage
		}

		internal enum ProcessPngPhotoStatuses
		{
			ResolutionMustBe400X400,
			InvalidImage,
			Success
		}

		internal static (byte[] bytes, ProcessPngPhotoStatuses status) ProcessPngPhoto(byte[] fileBytes, bool ignoreImageResolution)
		{
			try
			{
				using (var memoryStream = new MemoryStream(fileBytes))
				using (var image = System.Drawing.Image.FromStream(memoryStream))
				using (var pngMemoryStream = new MemoryStream())
				{
					if (ignoreImageResolution == false && (image.Width != 400 || image.Height != 400))
						return (null, ProcessPngPhotoStatuses.ResolutionMustBe400X400);
					image.Save(pngMemoryStream, ImageFormat.Png);
					return (pngMemoryStream.ToArray(), ProcessPngPhotoStatuses.Success);
				}
			}
			catch
			{
				return (null, ProcessPngPhotoStatuses.InvalidImage);
			}
		}

		public static UpdateStudentPictureStatuses UpdateStudentPicture(this AspireContext aspireContext, int studentID, byte[] bytes, bool ignoreImageResolution, Guid loginSessionGuid)
		{
			var (finalBytes, status) = ProcessPngPhoto(bytes, ignoreImageResolution);

			switch (status)
			{
				case ProcessPngPhotoStatuses.ResolutionMustBe400X400:
					return UpdateStudentPictureStatuses.ResolutionMustBe400X400;
				case ProcessPngPhotoStatuses.InvalidImage:
					return UpdateStudentPictureStatuses.InvalidImage;
				case ProcessPngPhotoStatuses.Success:
					var student = aspireContext.Students.Single(s => s.StudentID == studentID);
					if (student.PictureSystemFolderID == null)
						student.PictureSystemFolderID = aspireContext.AddStudentPictureSystemFolder(loginSessionGuid).SystemFolderID;
					aspireContext.AddSystemFileToSingleFileFolder(student.PictureSystemFolderID.Value, student.Enrollment, SystemFile.FileExtensions.Png, finalBytes, loginSessionGuid);
					return UpdateStudentPictureStatuses.Success;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		public enum UpdateStudentPictureFromItsCandidateStatuses
		{
			CandidateNotFound,
			PictureNotFound,
			PictureIsInvalid,
			Success
		}

		public static UpdateStudentPictureFromItsCandidateStatuses UpdateStudentPictureFromItsCandidateProfile(this AspireContext aspireContext, int studentID, Guid loginSessionGuid)
		{
			var query = from s in aspireContext.Students
						join cap in aspireContext.CandidateAppliedPrograms on s.CandidateAppliedProgramID equals cap.CandidateAppliedProgramID
						where s.StudentID == studentID
						select new { cap.CandidateID, cap.Candidate.PictureSystemFolderID };
			var candidate = query.SingleOrDefault();
			if (candidate == null)
				return UpdateStudentPictureFromItsCandidateStatuses.CandidateNotFound;
			if (candidate.PictureSystemFolderID == null)
				return UpdateStudentPictureFromItsCandidateStatuses.PictureNotFound;
			var bytes = aspireContext.ReadCandidatePicture(candidate.CandidateID);
			if (bytes?.Any() != true)
				return UpdateStudentPictureFromItsCandidateStatuses.PictureNotFound;

			var status = aspireContext.UpdateStudentPicture(studentID, bytes, true, loginSessionGuid);
			switch (status)
			{
				case UpdateStudentPictureStatuses.Success:
					return UpdateStudentPictureFromItsCandidateStatuses.Success;
				case UpdateStudentPictureStatuses.ResolutionMustBe400X400:
					throw new InvalidOperationException("This has been ignored.");
				case UpdateStudentPictureStatuses.InvalidImage:
					return UpdateStudentPictureFromItsCandidateStatuses.PictureIsInvalid;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		public enum UpdateStudentPictureFromOtherStudentProfileStatuses
		{
			PictureNotFound,
			PictureIsInvalid,
			Success
		}

		public static UpdateStudentPictureFromOtherStudentProfileStatuses UpdateStudentPictureFromOtherStudentProfile(this AspireContext aspireContext, int studentID, int fromStudentID, Guid loginSessionGuid)
		{
			var bytes = aspireContext.ReadStudentPicture(fromStudentID);
			if (bytes?.Any() != true)
				return UpdateStudentPictureFromOtherStudentProfileStatuses.PictureNotFound;

			var status = aspireContext.UpdateStudentPicture(studentID, bytes, true, loginSessionGuid);
			switch (status)
			{
				case UpdateStudentPictureStatuses.Success:
					return UpdateStudentPictureFromOtherStudentProfileStatuses.Success;
				case UpdateStudentPictureStatuses.ResolutionMustBe400X400:
					throw new InvalidOperationException("This has been ignored.");
				case UpdateStudentPictureStatuses.InvalidImage:
					return UpdateStudentPictureFromOtherStudentProfileStatuses.PictureIsInvalid;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		public static byte[] ReadStudentPicture(this AspireContext aspireContext, int studentID)
		{
			var student = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => new { s.PictureSystemFolderID }).SingleOrDefault();
			if (student?.PictureSystemFolderID != null)
				return aspireContext.GetSystemFileFromSingleFileFolder(student.PictureSystemFolderID.Value).Read()?.fileBytes;
			return null;
		}
	}
}