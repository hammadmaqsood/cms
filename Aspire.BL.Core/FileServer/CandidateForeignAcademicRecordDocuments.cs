﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;

namespace Aspire.BL.Core.FileServer
{
	internal static class CandidateForeignAcademicRecordDocuments
	{
		public enum ProcessCandidateForeignAcademicRecordDocumentStatuses
		{
			DocumentIsNotValid,
			Success
		}

		public static (ProcessCandidateForeignAcademicRecordDocumentStatuses status, byte[] fileBytes, SystemFile.FileExtensions fileExtension) ProcessCandidateForeignAcademicRecordDocument(this byte[] fileBytes, string fileName)
		{
			var fileExtension = System.IO.Path.GetExtension(fileName).ToLower();
			switch (fileExtension)
			{
				case ".pdf":
					return (ProcessCandidateForeignAcademicRecordDocumentStatuses.Success, fileBytes, SystemFile.FileExtensions.Pdf);
				case ".jpg":
					return (ProcessCandidateForeignAcademicRecordDocumentStatuses.Success, fileBytes, SystemFile.FileExtensions.Jpg);
				case ".jpeg":
					return (ProcessCandidateForeignAcademicRecordDocumentStatuses.Success, fileBytes, SystemFile.FileExtensions.Jpeg);
				default:
					return (ProcessCandidateForeignAcademicRecordDocumentStatuses.DocumentIsNotValid, null, SystemFile.FileExtensions.None);
			}
		}

		public enum AddCandidateForeignAcademicRecordDocumentStatuses
		{
			TemporaryFileNotFound,
			DocumentIsNotValid,
			Success
		}
		
		public static (SystemFile systemFile, AddCandidateForeignAcademicRecordDocumentStatuses status) AddCandidateForeignAcademicRecordDocument(this AspireContext aspireContext, string fileName, Guid instanceGuid, Guid temporarySystemFileID, Guid loginSessionGuid)
		{
			var temporaryFile = aspireContext.ReadTemporarySystemFile(instanceGuid, temporarySystemFileID, loginSessionGuid);
			if (temporaryFile == null)
				return (null, AddCandidateForeignAcademicRecordDocumentStatuses.TemporaryFileNotFound);
			var (processStatus, fileBytes, fileExtension) = temporaryFile.Value.fileBytes.ProcessCandidateForeignAcademicRecordDocument(fileName);
			switch (processStatus)
			{
				case ProcessCandidateForeignAcademicRecordDocumentStatuses.DocumentIsNotValid:
					return (null, AddCandidateForeignAcademicRecordDocumentStatuses.DocumentIsNotValid);
				case ProcessCandidateForeignAcademicRecordDocumentStatuses.Success:
					var systemFile = aspireContext.AddSystemFile(SystemFolders.DefaultSystemFolderIDs[SystemFolder.FolderTypes.CandidateForeignAcademicRecord], fileName, fileExtension, fileBytes, loginSessionGuid);
					aspireContext.DeleteTemporarySystemFile(instanceGuid, temporarySystemFileID, loginSessionGuid);
					return (systemFile, AddCandidateForeignAcademicRecordDocumentStatuses.Success);
				default:
					throw new NotImplementedEnumException(processStatus);
			}
		}

		public static (SystemFile systemFile, byte[] fileBytes)? ReadCandidateForeignAcademicRecordDocument(this AspireContext aspireContext, Guid systemFileID)
		{
			var result = aspireContext.ReadSystemFile(systemFileID);
			return result?.systemFile.SystemFolderID == SystemFolders.DefaultSystemFolderIDs[SystemFolder.FolderTypes.CandidateForeignAcademicRecord]
				? result
				: null;
		}

		public enum AddCandidateForeignDocumentStatuses
		{
			TemporaryFileNotFound,
			DocumentIsNotValid,
			Success
		}

		public static (ProcessCandidateForeignAcademicRecordDocumentStatuses status, byte[] fileBytes, SystemFile.FileExtensions fileExtension) ProcessCandidateForeignDocument(this byte[] fileBytes, string fileName)
		{
			var fileExtension = System.IO.Path.GetExtension(fileName).ToLower();
			switch (fileExtension)
			{
				case ".pdf":
					return (ProcessCandidateForeignAcademicRecordDocumentStatuses.Success, fileBytes, SystemFile.FileExtensions.Pdf);
				//case ".jpg":
				//	return (ProcessCandidateForeignAcademicRecordDocumentStatuses.Success, fileBytes, SystemFile.FileExtensions.Jpg);
				//case ".jpeg":
				//	return (ProcessCandidateForeignAcademicRecordDocumentStatuses.Success, fileBytes, SystemFile.FileExtensions.Jpeg);
				default:
					return (ProcessCandidateForeignAcademicRecordDocumentStatuses.DocumentIsNotValid, null, SystemFile.FileExtensions.None);
			}
		}

		public static (SystemFile systemFile, AddCandidateForeignDocumentStatuses status) AddCandidateForeignDocument(this AspireContext aspireContext, string fileName, Guid instanceGuid, Guid temporarySystemFileID, Guid loginSessionGuid)
		{
			var temporaryFile = aspireContext.ReadTemporarySystemFile(instanceGuid, temporarySystemFileID, loginSessionGuid);
			if (temporaryFile == null)
				return (null, AddCandidateForeignDocumentStatuses.TemporaryFileNotFound);
			var (processStatus, fileBytes, fileExtension) = temporaryFile.Value.fileBytes.ProcessCandidateForeignDocument(fileName);
			switch (processStatus)
			{
				case ProcessCandidateForeignAcademicRecordDocumentStatuses.DocumentIsNotValid:
					return (null, AddCandidateForeignDocumentStatuses.DocumentIsNotValid);
				case ProcessCandidateForeignAcademicRecordDocumentStatuses.Success:
					var systemFile = aspireContext.AddSystemFile(SystemFolders.DefaultSystemFolderIDs[SystemFolder.FolderTypes.CandidateForeignDocuments], fileName, fileExtension, fileBytes, loginSessionGuid);
					aspireContext.DeleteTemporarySystemFile(instanceGuid, temporarySystemFileID, loginSessionGuid);
					return (systemFile, AddCandidateForeignDocumentStatuses.Success);
				default:
					throw new NotImplementedEnumException(processStatus);
			}
		}

		public static (SystemFile systemFile, byte[] fileBytes)? ReadCandidateForeignDocument(this AspireContext aspireContext, Guid systemFileID)
		{
			var result = aspireContext.ReadSystemFile(systemFileID);
			return result?.systemFile.SystemFolderID == SystemFolders.DefaultSystemFolderIDs[SystemFolder.FolderTypes.CandidateForeignDocuments]
				? result
				: null;
		}
	}
}