﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.IO;

namespace Aspire.BL.Core.FileServer
{
	internal static class StudentDocuments
	{
		public enum AddStudentDocumentStatuses
		{
			TemporaryFileNotFound,
			DocumentIsNotValid,
			DocumentIsNotJpeg,
			Success
		}

		public enum ProcessStudentDocumentStatuses
		{
			DocumentIsNotValid,
			DocumentIsNotJpeg,
			Success
		}

		public static (ProcessStudentDocumentStatuses status, byte[] FileBytes, float imageDpiX, float imageDpiY, int imageWidthPixels, int imageHeightPixels) ProcessStudentDocument(this byte[] fileBytes)
		{
			using (var memoryStream = new MemoryStream(fileBytes))
			{
				try
				{
					using (var image = System.Drawing.Image.FromStream(memoryStream))
					{
						if (image.RawFormat.Guid == System.Drawing.Imaging.ImageFormat.Jpeg.Guid)
							return (ProcessStudentDocumentStatuses.Success, fileBytes, image.HorizontalResolution, image.VerticalResolution, image.Width, image.Height);
						return (ProcessStudentDocumentStatuses.DocumentIsNotJpeg, null, 0, 0, 0, 0);
					}
				}
				catch (ArgumentException)
				{
					return (ProcessStudentDocumentStatuses.DocumentIsNotValid, null, 0, 0, 0, 0);
				}
			}
		}

		public static (SystemFile systemFile, AddStudentDocumentStatuses status, float imageDpiX, float imageDpiY, int imageWidthPixels, int imageHeightPixels) AddStudentDocument(this AspireContext aspireContext, string documentName, Guid instanceGuid, Guid temporarySystemFileID, Guid loginSessionGuid)
		{
			var temporaryFile = aspireContext.ReadTemporarySystemFile(instanceGuid, temporarySystemFileID, loginSessionGuid);
			if (temporaryFile == null)
				return (null, AddStudentDocumentStatuses.TemporaryFileNotFound, 0, 0, 0, 0);
			var (status, FileBytes, imageDpiX, imageDpiY, imageWidthPixels, imageHeightPixels) = temporaryFile.Value.fileBytes.ProcessStudentDocument();
			switch (status)
			{
				case ProcessStudentDocumentStatuses.DocumentIsNotValid:
					return (null, AddStudentDocumentStatuses.DocumentIsNotValid, 0, 0, 0, 0);
				case ProcessStudentDocumentStatuses.Success:
					var systemFile = aspireContext.AddSystemFile(SystemFolders.DefaultSystemFolderIDs[SystemFolder.FolderTypes.StudentDocuments], documentName, SystemFile.FileExtensions.Jpeg, FileBytes, loginSessionGuid);
					aspireContext.DeleteTemporarySystemFile(instanceGuid, temporarySystemFileID, loginSessionGuid);
					return (systemFile, AddStudentDocumentStatuses.Success, imageDpiX, imageDpiY, imageWidthPixels, imageHeightPixels);
				case ProcessStudentDocumentStatuses.DocumentIsNotJpeg:
					return (null, AddStudentDocumentStatuses.DocumentIsNotJpeg, 0, 0, 0, 0);
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		public static (SystemFile systemFile, byte[] fileBytes)? ReadStudentDocument(this AspireContext aspireContext, Guid systemFileID)
		{
			var result = aspireContext.ReadSystemFile(systemFileID);
			return result?.systemFile.SystemFolderID == SystemFolders.DefaultSystemFolderIDs[SystemFolder.FolderTypes.StudentDocuments]
				? result
				: null;
		}
	}
}