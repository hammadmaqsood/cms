﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.FileServer.MasterAdmin
{
	public static class FileExplorer
	{
		public sealed class GetSystemFilesResult
		{
			public int VirtualItemCount { get; internal set; }

			public long TotalSize { get; internal set; }

			public List<SystemFile> Files { get; internal set; }

			public sealed class SystemFile
			{
				internal SystemFile() { }
				public Guid SystemFileID { get; internal set; }
				public Guid SystemFolderID { get; internal set; }
				public string FolderType { get; internal set; }
				public string FileName { get; internal set; }
				public string FileExtension { get; internal set; }
				public string ChecksumMD5 { get; internal set; }
				public int FileSize { get; internal set; }
				public DateTime CreatedDate { get; internal set; }
				public DateTime? DeletedDate { get; internal set; }
				public string CreatedByUserName { get; internal set; }
				public UserTypes CreatedByUserTypeEnum { get; internal set; }
				public string DeletedByUserName { get; internal set; }
				public UserTypes? DeletedByUserTypeEnum { get; internal set; }
			}
		}

		public static GetSystemFilesResult GetSystemFiles(Model.Entities.SystemFolder.FolderTypes? folderTypeEnum, bool showDeleted, string searchText, int pageIndex, int pageSize, SortDirection sortDirection, string sortExpression, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				var systemFiles = aspireContext.SystemFiles.Include(sfi => sfi.SystemFolder).AsQueryable();
				searchText = searchText.TrimAndMakeItNullIfEmpty();
				if (folderTypeEnum != null)
				{
					var folderType = folderTypeEnum.ToString();
					systemFiles = systemFiles.Where(sfi => sfi.SystemFolder.FolderType == folderType);
				}
				if (searchText != null)
					systemFiles = systemFiles.Where(sfi => sfi.FileName.Contains(searchText) || sfi.FileExtension == searchText);
				if (!showDeleted)
					systemFiles = systemFiles.Where(sfi => sfi.DeletedDate == null || sfi.DeletedByUserLoginHistoryID == null);

				var virtualItemCount = systemFiles.Count();
				var totalSize = systemFiles.Sum(sf => (long)sf.FileSize);

				var finalQuery = systemFiles.Select(sf => new GetSystemFilesResult.SystemFile
				{
					SystemFileID = sf.SystemFileID,
					SystemFolderID = sf.SystemFolder.SystemFolderID,
					FolderType = sf.SystemFolder.FolderType,
					FileName = sf.FileName,
					FileExtension = sf.FileExtension,
					ChecksumMD5 = sf.ChecksumMD5,
					FileSize = sf.FileSize,
					CreatedDate = sf.CreatedDate,
					DeletedDate = sf.DeletedDate,
					CreatedByUserName = sf.UserLoginHistory.UserName,
					CreatedByUserTypeEnum = (UserTypes)sf.UserLoginHistory.UserType,
					DeletedByUserName = sf.UserLoginHistory1.UserName,
					DeletedByUserTypeEnum = (UserTypes?)sf.UserLoginHistory1.UserType,
				});
				switch (sortExpression)
				{
					case nameof(GetSystemFilesResult.SystemFile.SystemFolderID):
						finalQuery = finalQuery.OrderBy(sortDirection, sf => sf.SystemFolderID).ThenBy(sf => sf.CreatedDate);
						break;
					case nameof(GetSystemFilesResult.SystemFile.SystemFileID):
						finalQuery = finalQuery.OrderBy(sortDirection, sf => sf.SystemFileID).ThenBy(sf => sf.CreatedDate);
						break;
					case nameof(GetSystemFilesResult.SystemFile.FolderType):
						finalQuery = finalQuery.OrderBy(sortDirection, sf => sf.FolderType).ThenBy(sf => sf.CreatedDate);
						break;
					case nameof(GetSystemFilesResult.SystemFile.FileName):
						finalQuery = finalQuery.OrderBy(sortDirection, sf => sf.FileName).ThenBy(sf => sf.CreatedDate);
						break;
					case nameof(GetSystemFilesResult.SystemFile.FileExtension):
						finalQuery = finalQuery.OrderBy(sortDirection, sf => sf.FileExtension).ThenBy(sf => sf.CreatedDate);
						break;
					case nameof(GetSystemFilesResult.SystemFile.ChecksumMD5):
						finalQuery = finalQuery.OrderBy(sortDirection, sf => sf.ChecksumMD5).ThenBy(sf => sf.CreatedDate);
						break;
					case nameof(GetSystemFilesResult.SystemFile.FileSize):
						finalQuery = finalQuery.OrderBy(sortDirection, sf => sf.FileSize).ThenBy(sf => sf.CreatedDate);
						break;
					case nameof(GetSystemFilesResult.SystemFile.CreatedDate):
						finalQuery = finalQuery.OrderBy(sortDirection, sf => sf.CreatedDate);
						break;
					case nameof(GetSystemFilesResult.SystemFile.CreatedByUserName):
						finalQuery = finalQuery.OrderBy(sortDirection, sf => sf.CreatedByUserName).ThenBy(sf => sf.CreatedDate);
						break;
					case nameof(GetSystemFilesResult.SystemFile.CreatedByUserTypeEnum):
						finalQuery = finalQuery.OrderBy(sortDirection, sf => sf.CreatedByUserTypeEnum).ThenBy(sf => sf.CreatedDate);
						break;
					case nameof(GetSystemFilesResult.SystemFile.DeletedDate):
						finalQuery = finalQuery.OrderBy(sortDirection, sf => sf.DeletedDate).ThenBy(sf => sf.CreatedDate);
						break;
					case nameof(GetSystemFilesResult.SystemFile.DeletedByUserName):
						finalQuery = finalQuery.OrderBy(sortDirection, sf => sf.DeletedByUserName).ThenBy(sf => sf.CreatedDate);
						break;
					case nameof(GetSystemFilesResult.SystemFile.DeletedByUserTypeEnum):
						finalQuery = finalQuery.OrderBy(sortDirection, sf => sf.DeletedByUserTypeEnum).ThenBy(sf => sf.CreatedDate);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}

				return new GetSystemFilesResult
				{
					Files = finalQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
					VirtualItemCount = virtualItemCount,
					TotalSize = totalSize
				};
			}
		}
	}
}
