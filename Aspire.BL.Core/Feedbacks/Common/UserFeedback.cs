﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Feedbacks.Common
{
	public static class UserFeedback
	{
		public sealed class Feedback
		{
			internal Feedback() { }
			public Guid UserFeedbackID { get; internal set; }
			public string FeedbackNo { get; internal set; }
			public Guid FeedbackBoxType { get; internal set; }
			public Guid? MovedFromFeedbackBoxType { get; internal set; }
			public Model.Entities.UserFeedback.FeedbackBoxTypes FeedbackBoxTypeEnum => this.FeedbackBoxType.GetEnum<Model.Entities.UserFeedback.FeedbackBoxTypes>();
			public Guid FeedbackType { get; internal set; }
			public Model.Entities.UserFeedback.FeedbackTypes FeedbackTypeEnum => this.FeedbackType.GetEnum<Model.Entities.UserFeedback.FeedbackTypes>();
			public string FeedbackDetails { get; internal set; }
			public DateTime? FeedbackDate { get; internal set; }
			public Guid Status { get; set; }
			public Model.Entities.UserFeedback.Statuses StatusEnum => this.Status.GetEnum<Model.Entities.UserFeedback.Statuses>();
			public List<FeedbackDiscussion> FeedbackDiscussions { get; internal set; }

			public sealed class FeedbackDiscussion
			{
				internal FeedbackDiscussion() { }
				public Guid UserFeedbackDiscussionID { get; internal set; }
				public string Details { get; internal set; }
				public DateTime CreatedDate { get; internal set; }
				public string CreatedByName { get; internal set; }
				public UserTypes CreatedByUserTypeEnum { get; internal set; }
				public int CreatedByUserLoginHistoryID { get; internal set; }
			}
		}

		public static (List<Feedback> userFeedbacks, int virtualItemsCount) GetUserFeedbacks(Model.Entities.UserFeedback.FeedbackTypes? feedbackTypeEnum, Model.Entities.UserFeedback.FeedbackBoxTypes? feedbackBoxTypeEnum, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var userFeedbacksQuery = from f in aspireContext.UserFeedbacks
										 join h in aspireContext.UserLoginHistories on f.UserLoginHistoryID equals h.UserLoginHistoryID
										 select new
										 {
											 h.UserID,
											 h.UserType,
											 UserRoleInstituteID = h.UserRole.InstituteID,
											 h.StudentID,
											 h.FacultyMemberID,
											 f.UserFeedbackID,
											 f.FeedbackNo,
											 f.FeedbackDetails,
											 f.FeedbackDate,
											 f.FeedbackType,
											 f.FeedbackBoxType,
											 f.MovedFromFeedbackBoxType,
											 f.Status
										 };
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Executive:
						var executiveLoginHistory = aspireContext.DemandExecutiveActiveSession(loginSessionGuid);
						userFeedbacksQuery = userFeedbacksQuery.Where(f => f.UserID != null && f.UserID == loginHistory.UserID && f.UserType == (short)loginHistory.UserTypeEnum && f.UserRoleInstituteID == executiveLoginHistory.InstituteID);
						break;
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
						userFeedbacksQuery = userFeedbacksQuery.Where(f => f.UserID != null && f.UserID == loginHistory.UserID && f.UserType == (short)loginHistory.UserTypeEnum && f.UserRoleInstituteID == staffLoginHistory.InstituteID);
						break;
					case UserTypes.Student:
						userFeedbacksQuery = userFeedbacksQuery.Where(f => f.StudentID != null && f.StudentID == loginHistory.StudentID);
						break;
					case UserTypes.Faculty:
						userFeedbacksQuery = userFeedbacksQuery.Where(f => f.FacultyMemberID != null && f.FacultyMemberID == loginHistory.FacultyMemberID);
						break;
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Alumni:
					case UserTypes.Candidate:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new NotImplementedEnumException(loginHistory?.UserTypeEnum);
				}
				if (feedbackBoxTypeEnum != null)
				{
					var feedbackBoxType = feedbackBoxTypeEnum.Value.GetGuid();
					userFeedbacksQuery = userFeedbacksQuery.Where(f => f.FeedbackBoxType == feedbackBoxType);
				}
				if (feedbackTypeEnum != null)
				{
					var feedbackType = feedbackTypeEnum.Value.GetGuid();
					userFeedbacksQuery = userFeedbacksQuery.Where(f => f.FeedbackType == feedbackType);
				}
				var virtualItemsCount = userFeedbacksQuery.Count();
				var userFeedbacksFinalQuery = userFeedbacksQuery.Select(f => new Feedback
				{
					UserFeedbackID = f.UserFeedbackID,
					FeedbackNo = f.FeedbackNo,
					FeedbackBoxType = f.FeedbackBoxType,
					MovedFromFeedbackBoxType = f.MovedFromFeedbackBoxType,
					FeedbackType = f.FeedbackType,
					FeedbackDate = f.FeedbackDate,
					Status = f.Status
				});
				switch (sortExpression)
				{
					case nameof(Feedback.FeedbackBoxType):
						userFeedbacksFinalQuery = userFeedbacksFinalQuery.OrderBy(sortDirection, f => f.FeedbackBoxType);
						break;
					case nameof(Feedback.FeedbackType):
						userFeedbacksFinalQuery = userFeedbacksFinalQuery.OrderBy(sortDirection, f => f.FeedbackType);
						break;
					case nameof(Feedback.FeedbackNo):
						userFeedbacksFinalQuery = userFeedbacksFinalQuery.OrderBy(sortDirection, f => f.FeedbackNo);
						break;
					case nameof(Feedback.FeedbackDate):
						userFeedbacksFinalQuery = userFeedbacksFinalQuery.OrderBy(sortDirection, f => f.FeedbackDate);
						break;
					case nameof(Feedback.Status):
						userFeedbacksFinalQuery = userFeedbacksFinalQuery.OrderBy(sortDirection, f => f.Status);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return (userFeedbacksFinalQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList(), virtualItemsCount);
			}
		}

		public static Feedback GetUserFeedback(Guid userFeedbackID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var userFeedbacksQuery = from f in aspireContext.UserFeedbacks
										 join h in aspireContext.UserLoginHistories on f.UserLoginHistoryID equals h.UserLoginHistoryID
										 select new
										 {
											 h.UserID,
											 h.UserType,
											 UserRoleInstituteID = h.UserRole.InstituteID,
											 h.StudentID,
											 h.FacultyMemberID,
											 f.UserFeedbackID,
											 f.FeedbackNo,
											 f.FeedbackDetails,
											 f.FeedbackDate,
											 f.FeedbackBoxType,
											 f.MovedFromFeedbackBoxType,
											 f.FeedbackType,
											 f.Status
										 };
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Executive:
						var executiveLoginHistory = aspireContext.DemandExecutiveActiveSession(loginSessionGuid);
						userFeedbacksQuery = userFeedbacksQuery.Where(f => f.UserID != null && f.UserID == loginHistory.UserID && f.UserType == (byte)loginHistory.UserTypeEnum && f.UserRoleInstituteID == executiveLoginHistory.InstituteID);
						break;
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
						userFeedbacksQuery = userFeedbacksQuery.Where(f => f.UserID != null && f.UserID == loginHistory.UserID && f.UserType == (byte)loginHistory.UserTypeEnum && f.UserRoleInstituteID == staffLoginHistory.InstituteID);
						break;
					case UserTypes.Student:
						userFeedbacksQuery = userFeedbacksQuery.Where(f => f.StudentID != null && f.StudentID == loginHistory.StudentID);
						break;
					case UserTypes.Faculty:
						userFeedbacksQuery = userFeedbacksQuery.Where(f => f.FacultyMemberID != null && f.FacultyMemberID == loginHistory.FacultyMemberID);
						break;
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Alumni:
					case UserTypes.Candidate:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new NotImplementedEnumException(loginHistory?.UserTypeEnum);
				}
				var userFeedback = userFeedbacksQuery.Where(f => f.UserFeedbackID == userFeedbackID).Select(f => new Feedback
				{
					UserFeedbackID = f.UserFeedbackID,
					FeedbackNo = f.FeedbackNo,
					FeedbackBoxType = f.FeedbackBoxType,
					MovedFromFeedbackBoxType = f.MovedFromFeedbackBoxType,
					FeedbackType = f.FeedbackType,
					FeedbackDate = f.FeedbackDate,
					Status = f.Status,
					FeedbackDetails = f.FeedbackDetails,
					FeedbackDiscussions = aspireContext.UserFeedbackDiscussions.Where(d => d.UserFeedbackID == f.UserFeedbackID).Select(d => new Feedback.FeedbackDiscussion
					{
						UserFeedbackDiscussionID = d.UserFeedbackDiscussionID,
						Details = d.Details,
						CreatedDate = d.CreatedDate,
						CreatedByUserTypeEnum = (UserTypes)d.UserLoginHistory.UserType,
						CreatedByUserLoginHistoryID = d.CreatedByUserLoginHistoryID
					}).ToList()
				}).SingleOrDefault();
				if (userFeedback == null)
					return null;

				userFeedback.FeedbackDiscussions.ForEach(d =>
				{
					switch (d.CreatedByUserTypeEnum)
					{
						case UserTypes.Executive:
						case UserTypes.Staff:
							d.CreatedByName = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == d.CreatedByUserLoginHistoryID).Select(h => new
							{
								h.UserRole.User1.Name
							}).Single().Name;
							break;
						case UserTypes.Student:
							d.CreatedByName = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == d.CreatedByUserLoginHistoryID).Select(h => new
							{
								h.Student.Name
							}).Single().Name;
							break;
						case UserTypes.Faculty:
							d.CreatedByName = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == d.CreatedByUserLoginHistoryID).Select(h => new
							{
								h.FacultyMember.Name
							}).Single().Name;
							break;
						default:
							throw new NotImplementedEnumException(d.CreatedByUserTypeEnum);
					}
				});

				return userFeedback;
			}
		}

		public enum AddUserFeedbackStatuses
		{
			NotAuthorized,
			Success
		}

		private static string GetUserFeedbackNo(this AspireContext aspireContext, UserTypes userTypeEnum, int instituteID, DateTime now)
		{
			var instituteAlias = aspireContext.Institutes.Where(i => i.InstituteID == instituteID).Select(i => i.InstituteAlias).Single();
			var format = $"{instituteAlias}/{userTypeEnum.ToFullName()}/{now:yy/MM/dd}/";
			var userFeedback = aspireContext.UserFeedbacks.Where(f => f.FeedbackNo.StartsWith(format))
				.Select(f => new { f.FeedbackNo, f.FeedbackDate })
				.OrderByDescending(f => f.FeedbackDate)
				.FirstOrDefault();
			var number = userFeedback?.FeedbackNo.Substring(format.Length).TrimStart('0').ToInt() ?? 0;
			return $"{format}{++number:D4}";
		}

		public static AddUserFeedbackStatuses AddUserFeedback(Model.Entities.UserFeedback.FeedbackBoxTypes feedbackBoxTypeEnum, Model.Entities.UserFeedback.FeedbackTypes feedbackTypeEnum, string feedbackDetails, Guid loginSessionGuid)
		{
			lock (Locks.UserFeedback)
			{
				using (var aspireContext = new AspireContext(true))
				{
					var now = DateTime.Now;
					var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
					int instituteID;
					switch (loginHistory.UserTypeEnum)
					{
						case UserTypes.Executive:
						case UserTypes.Staff:
							instituteID = aspireContext.UserRoles.Where(r => r.UserRoleID == loginHistory.UserRoleID.Value).Select(r => r.InstituteID.Value).Single();
							break;
						case UserTypes.Student:
							instituteID = aspireContext.Students.Where(s => s.StudentID == loginHistory.StudentID.Value).Select(s => s.AdmissionOpenProgram.Program.InstituteID).Single();
							break;
						case UserTypes.Faculty:
							instituteID = aspireContext.FacultyMembers.Where(f => f.FacultyMemberID == loginHistory.FacultyMemberID.Value).Select(f => f.InstituteID).Single();
							break;
						case UserTypes.Alumni:
						case UserTypes.Candidate:
						case UserTypes.IntegratedService:
						case UserTypes.ManualSQL:
						case UserTypes.Admins:
						case UserTypes.Any:
							return AddUserFeedbackStatuses.NotAuthorized;
						default:
							throw new NotImplementedEnumException(loginHistory?.UserTypeEnum);
					}
					var userFeedback = aspireContext.UserFeedbacks.Add(new Model.Entities.UserFeedback
					{
						UserFeedbackID = Guid.NewGuid(),
						FeedbackNo = aspireContext.GetUserFeedbackNo(loginHistory.UserTypeEnum, instituteID, now),
						UserLoginHistoryID = loginHistory.UserLoginHistoryID,
						FeedbackDate = now,
						FeedbackDetails = feedbackDetails.CannotBeEmptyOrWhitespace(),
						FeedbackBoxTypeEnum = feedbackBoxTypeEnum,
						MovedFromFeedbackBoxType = null,
						FeedbackTypeEnum = feedbackTypeEnum,
						StatusEnum = Model.Entities.UserFeedback.Statuses.Pending,
						Archived = false,
						LastUpdatedDate = now,
						Notes = null,
						Unread = true
					}.Validate());
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
					return AddUserFeedbackStatuses.Success;
				}
			}
		}

		public enum AddDiscussionStatuses
		{
			NoRecordFound,
			StatusIsNotInProcess,
			Success
		}

		public static AddDiscussionStatuses AddDiscussion(Guid userFeedbackID, string discussionDetails, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var userFeedbacksQuery = from f in aspireContext.UserFeedbacks
										 join h in aspireContext.UserLoginHistories on f.UserLoginHistoryID equals h.UserLoginHistoryID
										 select new
										 {
											 h.UserID,
											 h.UserType,
											 UserRoleInstituteID = h.UserRole.InstituteID,
											 h.StudentID,
											 h.FacultyMemberID,
											 f.UserFeedbackID
										 };
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Executive:
						var executiveLoginHistory = aspireContext.DemandExecutiveActiveSession(loginSessionGuid);
						userFeedbacksQuery = userFeedbacksQuery.Where(f => f.UserID != null && f.UserID == loginHistory.UserID && f.UserType == (byte)loginHistory.UserTypeEnum && f.UserRoleInstituteID == executiveLoginHistory.InstituteID);
						break;
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
						userFeedbacksQuery = userFeedbacksQuery.Where(f => f.UserID != null && f.UserID == loginHistory.UserID && f.UserType == (byte)loginHistory.UserTypeEnum && f.UserRoleInstituteID == staffLoginHistory.InstituteID);
						break;
					case UserTypes.Student:
						userFeedbacksQuery = userFeedbacksQuery.Where(f => f.StudentID != null && f.StudentID == loginHistory.StudentID);
						break;
					case UserTypes.Faculty:
						userFeedbacksQuery = userFeedbacksQuery.Where(f => f.FacultyMemberID != null && f.FacultyMemberID == loginHistory.FacultyMemberID);
						break;
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Alumni:
					case UserTypes.Candidate:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new NotImplementedEnumException(loginHistory?.UserTypeEnum);
				}
				var userFeedback = userFeedbacksQuery.Where(f => f.UserFeedbackID == userFeedbackID).Select(f => new
				{
					f.UserFeedbackID,
				}).SingleOrDefault();
				if (userFeedback == null)
					return AddDiscussionStatuses.NoRecordFound;

				var now = DateTime.Now;
				var feedback = aspireContext.UserFeedbacks.Include(f => f.UserFeedbackDiscussions).Single(f => f.UserFeedbackID == userFeedback.UserFeedbackID);
				if (feedback.StatusEnum == Model.Entities.UserFeedback.Statuses.InProcess && feedback.UserFeedbackDiscussions?.Any() == true)
				{
					feedback.LastUpdatedDate = now;
					feedback.UserFeedbackDiscussions.Add(new Model.Entities.UserFeedbackDiscussion
					{
						UserFeedbackDiscussionID = Guid.NewGuid(),
						CreatedByUserLoginHistoryID = loginHistory.UserLoginHistoryID,
						CreatedDate = now,
						Details = discussionDetails.TrimAndCannotBeEmpty(),
						StatusEnum = Model.Entities.UserFeedbackDiscussion.Statuses.Visible
					});
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
					return AddDiscussionStatuses.Success;
				}
				return AddDiscussionStatuses.StatusIsNotInProcess;
			}
		}
	}
}
