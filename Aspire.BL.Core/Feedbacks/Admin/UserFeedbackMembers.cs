﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Feedbacks.Admin
{
	public static class UserFeedbackMembers
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.FacultyManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandManageAssigneesPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Feedbacks.ManageAssignees, UserGroupPermission.PermissionValues.Allowed);
		}

		public enum AddOrUpdateUserFeedbackMemberStatuses
		{
			DesignationAlreadyExists,
			Success,
			NoRecordFound,
		}

		public static AddOrUpdateUserFeedbackMemberStatuses AddUserFeedbackMember(UserFeedback.FeedbackBoxTypes feedbackBoxTypeEnum, int? instituteID, string designation, string memberName, UserFeedbackMember.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				Core.Common.Permissions.Admin.AdminLoginHistory loginHistory;
				switch (feedbackBoxTypeEnum)
				{
					case UserFeedback.FeedbackBoxTypes.Rector:
						if (instituteID != null)
							throw new ArgumentException(nameof(instituteID));
						loginHistory = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
						break;
					case UserFeedback.FeedbackBoxTypes.DirectorGeneralCampus:
						if (instituteID == null)
							throw new ArgumentNullException(nameof(instituteID));
						loginHistory = aspireContext.DemandManageAssigneesPermissions(loginSessionGuid);
						break;
					default:
						throw new NotImplementedEnumException(feedbackBoxTypeEnum);
				}
				var feedbackBoxType = feedbackBoxTypeEnum.GetGuid();
				var userFeedbackMembers = aspireContext.GetUserFeedbackMembersQuery(feedbackBoxTypeEnum, instituteID, null, loginSessionGuid);
				designation = designation.TrimAndMakeItNullIfEmpty();
				memberName = memberName.TrimAndMakeItNullIfEmpty();
				if (userFeedbackMembers.Any(m => m.MemberDesignation == designation))
					return AddOrUpdateUserFeedbackMemberStatuses.DesignationAlreadyExists;
				var userFeedbackMember = aspireContext.UserFeedbackMembers.Add(new UserFeedbackMember
				{
					InstituteID = instituteID,
					FeedbackBoxType = feedbackBoxType,
					MemberDesignation = designation,
					MemberName = memberName,
					StatusEnum = statusEnum,
					UserFeedbackMemberID = Guid.NewGuid()
				});
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddOrUpdateUserFeedbackMemberStatuses.Success;
			}
		}

		public static AddOrUpdateUserFeedbackMemberStatuses UpdateUserFeedbackMember(Guid userFeedbackMemberID, string designation, string memberName, UserFeedbackMember.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var userFeedbackMember = aspireContext.UserFeedbackMembers.SingleOrDefault(m => m.UserFeedbackMemberID == userFeedbackMemberID);
				if (userFeedbackMember == null)
					return AddOrUpdateUserFeedbackMemberStatuses.NoRecordFound;
				Core.Common.Permissions.Admin.AdminLoginHistory loginHistory;
				var feedbackBoxTypeEnum = userFeedbackMember.FeedbackBoxType.GetEnum<UserFeedback.FeedbackBoxTypes>();
				switch (feedbackBoxTypeEnum)
				{
					case UserFeedback.FeedbackBoxTypes.Rector:
						loginHistory = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
						break;
					case UserFeedback.FeedbackBoxTypes.DirectorGeneralCampus:
						loginHistory = aspireContext.DemandManageAssigneesPermissions(loginSessionGuid);
						break;
					default:
						throw new NotImplementedEnumException(feedbackBoxTypeEnum);
				}
				var userFeedbackMembers = aspireContext.GetUserFeedbackMembersQuery(feedbackBoxTypeEnum, userFeedbackMember.InstituteID, null, loginSessionGuid);
				designation = designation.TrimAndMakeItNullIfEmpty();
				memberName = memberName.TrimAndMakeItNullIfEmpty();
				if (userFeedbackMembers.Any(m => m.UserFeedbackMemberID != userFeedbackMember.UserFeedbackMemberID && m.MemberDesignation == designation))
					return AddOrUpdateUserFeedbackMemberStatuses.DesignationAlreadyExists;
				userFeedbackMember.MemberDesignation = designation;
				userFeedbackMember.MemberName = memberName;
				userFeedbackMember.StatusEnum = statusEnum;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddOrUpdateUserFeedbackMemberStatuses.Success;
			}
		}

		public static IQueryable<UserFeedbackMember> GetUserFeedbackMembersQuery(this AspireContext aspireContext, UserFeedback.FeedbackBoxTypes? feedbackBoxTypeEnum, int? instituteID, UserFeedbackMember.Statuses? statusEnum, Guid loginSessionGuid)
		{
			var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
			IQueryable<UserFeedbackMember> userFeedbackMembers;
			if (loginHistory.IsMasterAdmin)
				userFeedbackMembers = aspireContext.UserFeedbackMembers.AsQueryable();
			else
				userFeedbackMembers = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.UserFeedbackMembers);
			if (feedbackBoxTypeEnum != null)
			{
				var feedbackBoxType = feedbackBoxTypeEnum.Value.GetGuid();
				userFeedbackMembers = userFeedbackMembers.Where(m => m.FeedbackBoxType == feedbackBoxType);
			}
			if (instituteID != null)
				userFeedbackMembers = userFeedbackMembers.Where(m => m.InstituteID == instituteID.Value);
			if (statusEnum != null)
			{
				var status = statusEnum.Value.GetGuid();
				userFeedbackMembers = userFeedbackMembers.Where(m => m.Status == status);
			}
			return userFeedbackMembers;
		}

		public static (List<UserFeedbackMember> userFeedbackMembers, int virtualItemCount) GetUserFeedbackMembersList(UserFeedback.FeedbackBoxTypes? feedbackBoxTypeEnum, int? instituteID, UserFeedbackMember.Statuses? statusEnum, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var userFeedbackMembers = aspireContext.GetUserFeedbackMembersQuery(feedbackBoxTypeEnum, instituteID, statusEnum, loginSessionGuid);
				var virtualItemCount = userFeedbackMembers.Count();
				switch (sortExpression)
				{
					case nameof(UserFeedbackMember.FeedbackBoxType):
						userFeedbackMembers = userFeedbackMembers.OrderBy(sortDirection, m => m.FeedbackBoxType);
						break;
					case nameof(UserFeedbackMember.MemberDesignation):
						userFeedbackMembers = userFeedbackMembers.OrderBy(sortDirection, m => m.MemberDesignation);
						break;
					case nameof(UserFeedbackMember.MemberName):
						userFeedbackMembers = userFeedbackMembers.OrderBy(sortDirection, m => m.MemberName);
						break;
					case nameof(UserFeedbackMember.Institute.InstituteAlias):
						userFeedbackMembers = userFeedbackMembers.OrderBy(sortDirection, m => m.Institute.InstituteAlias);
						break;
					case nameof(UserFeedbackMember.Status):
						userFeedbackMembers = userFeedbackMembers.OrderBy(sortDirection, m => m.Status);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return (userFeedbackMembers.Include(m => m.Institute).Skip(pageIndex * pageSize).Take(pageSize).ToList(), virtualItemCount);
			}
		}

		public static UserFeedbackMember GetUserFeedbackMember(Guid userFeedbackMemberID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.GetUserFeedbackMembersQuery(null, null, null, loginSessionGuid).SingleOrDefault(m => m.UserFeedbackMemberID == userFeedbackMemberID);
			}
		}

		public enum DeleteUserFeedbackMemberStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success
		}

		public static DeleteUserFeedbackMemberStatuses DeleteUserFeedbackMember(Guid userFeedbackMemberID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var userFeedbackMember = aspireContext.GetUserFeedbackMembersQuery(null, null, null, loginSessionGuid).SingleOrDefault(m => m.UserFeedbackMemberID == userFeedbackMemberID);
				if (userFeedbackMember == null)
					return DeleteUserFeedbackMemberStatuses.NoRecordFound;
				if (aspireContext.UserFeedbackAssignees.Any(a => a.UserFeedbackMemberID == userFeedbackMember.UserFeedbackMemberID))
					return DeleteUserFeedbackMemberStatuses.ChildRecordExists;
				var loginHistory = aspireContext.DemandManageAssigneesPermissions(loginSessionGuid);
				aspireContext.UserFeedbackMembers.Remove(userFeedbackMember);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteUserFeedbackMemberStatuses.Success;
			}
		}
	}
}
