﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Feedbacks.Executive
{
	public static class UserFeedback
	{
		public static List<CustomListItemGuid> GetUserFeedbackMembers(Model.Entities.UserFeedback.FeedbackBoxTypes feedbackBoxTypeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				Core.Common.Permissions.Executive.ExecutiveGroupPermission loginHistory;
				IQueryable<Model.Entities.UserFeedbackMember> userFeedbackMembers;
				var feedbackBoxType = feedbackBoxTypeEnum.GetGuid();
				switch (feedbackBoxTypeEnum)
				{
					case Model.Entities.UserFeedback.FeedbackBoxTypes.Rector:
						loginHistory = aspireContext.DemandExecutivePermissions(loginSessionGuid, ExecutivePermissions.Feedbacks.CanViewRectorBox, Model.Entities.UserGroupPermission.Allowed);
						userFeedbackMembers = aspireContext.UserFeedbackMembers.Where(m => m.FeedbackBoxType == feedbackBoxType && m.InstituteID == null);
						break;
					case Model.Entities.UserFeedback.FeedbackBoxTypes.DirectorGeneralCampus:
						loginHistory = aspireContext.DemandExecutivePermissions(loginSessionGuid, ExecutivePermissions.Feedbacks.CanViewDirectorGeneralBox, Model.Entities.UserGroupPermission.Allowed);
						userFeedbackMembers = aspireContext.UserFeedbackMembers.Where(m => m.FeedbackBoxType == feedbackBoxType && m.InstituteID == loginHistory.InstituteID);
						break;
					default:
						throw new NotImplementedEnumException(feedbackBoxTypeEnum);
				}

				return userFeedbackMembers.ToList().Select(m => new CustomListItemGuid
				{
					ID = m.UserFeedbackMemberID,
					Text = $"{m.MemberDesignation} {(m.StatusEnum != Model.Entities.UserFeedbackMember.Statuses.Active ? " (" + m.StatusEnum.GetFullName() + ")" : "")}"
				}).OrderBy(i => i.Text).ToList();
			}
		}

		public sealed class Feedback
		{
			internal Feedback() { }
			public Guid UserFeedbackID { get; internal set; }
			public string FeedbackNo { get; internal set; }
			public Guid FeedbackBoxType { get; internal set; }
			public Guid? MovedFromFeedbackBoxType { get; internal set; }
			public Guid FeedbackType { get; internal set; }
			public string FeedbackDetails { get; internal set; }
			public DateTime? FeedbackDate { get; internal set; }
			public bool Archived { get; internal set; }
			public Guid Status { get; set; }
			public UserTypes UserTypeEnum { get; internal set; }
			public string StudentName { get; internal set; }
			public string StudentEnrollment { get; internal set; }
			public string StudentPersonalEmail { get; internal set; }
			public string StudentUniversityEmail { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public string FacultyMemberEmail { get; internal set; }
			public string UserName { get; internal set; }
			public string UserEmail { get; internal set; }
			public string Name
			{
				get
				{
					switch (this.UserTypeEnum)
					{
						case UserTypes.Executive:
						case UserTypes.Staff:
							return this.UserName;
						case UserTypes.Student:
							return $"<{this.StudentEnrollment}> {this.StudentName}";
						case UserTypes.Faculty:
							return this.FacultyMemberName;
						default:
							throw new NotImplementedEnumException(this.UserTypeEnum);
					}
				}
			}
			public string Email
			{
				get
				{
					switch (this.UserTypeEnum)
					{
						case UserTypes.Executive:
						case UserTypes.Staff:
							return this.UserEmail;
						case UserTypes.Student:
							return this.StudentUniversityEmail.TrimAndMakeItNullIfEmpty() ?? this.StudentPersonalEmail;
						case UserTypes.Faculty:
							return this.FacultyMemberEmail;
						default:
							throw new NotImplementedEnumException(this.UserTypeEnum);
					}
				}
			}
			public List<string> AssignedToMembers { get; internal set; }
			public bool Unread { get; internal set; }
		}

		public sealed class FeedBackStatistics
		{
			public Guid Status { get; set; }
			public Model.Entities.UserFeedback.Statuses StatusEnum => this.Status.GetEnum<Model.Entities.UserFeedback.Statuses>();
			public int Count { get; set; }
		}

		public static (List<Feedback> userFeedbacks, int virtualItemsCount, List<FeedBackStatistics> filteredStatistics, List<FeedBackStatistics> statistics) GetUserFeedbacks(Model.Entities.UserFeedback.FeedbackBoxTypes feedbackBoxTypeEnum, Model.Entities.UserFeedback.FeedbackTypes? feedbackTypeEnum, UserTypes? userTypeEnum, Model.Entities.UserFeedback.Statuses? statusEnum, bool? unread, Guid? assignedToUserFeedbackMemberID, bool? archived, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				Core.Common.Permissions.Executive.ExecutiveModulePermissions loginHistory;
				switch (feedbackBoxTypeEnum)
				{
					case Model.Entities.UserFeedback.FeedbackBoxTypes.Rector:
						loginHistory = aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.Feedbacks.CanViewRectorBox, Model.Entities.UserGroupPermission.Allowed);
						break;
					case Model.Entities.UserFeedback.FeedbackBoxTypes.DirectorGeneralCampus:
						loginHistory = aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.Feedbacks.CanViewDirectorGeneralBox, Model.Entities.UserGroupPermission.Allowed);
						break;
					default:
						throw new NotImplementedEnumException(feedbackBoxTypeEnum);
				}
				var userType = (short?)userTypeEnum;
				var userLoginHistories = aspireContext.UserLoginHistories
					.Where(h => h.StudentID == null || h.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(h => h.FacultyMemberID == null || h.FacultyMember.InstituteID == loginHistory.InstituteID)
					.Where(h => h.UserRoleID == null || h.UserRole.InstituteID == loginHistory.InstituteID)
					.Where(h => h.StudentID != null || h.FacultyMemberID != null || h.UserRoleID != null)
 					.Select(h => new
					 {
						 h.UserLoginHistoryID,
						 UserTypeEnum = (UserTypes)h.UserType,
						 h.UserID,
						 h.UserRoleID,
						 h.StudentID,
						 h.FacultyMemberID,
						 StudentName = h.Student.Name,
						 StudentEnrollment = h.Student.Enrollment,
						 StudentPersonalEmail = h.Student.PersonalEmail,
						 StudentUniversityEmail = h.Student.UniversityEmail,
						 FacultyMemberName = h.FacultyMember.Name,
						 FacultyMemberEmail = h.FacultyMember.Email,
						 UserName = h.UserRole.User1.Name,
						 UserEmail = h.UserRole.User1.Email
					 });

				var feedbackBoxType = feedbackBoxTypeEnum.GetGuid();
				var userFeedbacksQuery = from f in aspireContext.UserFeedbacks
										 join h in userLoginHistories on f.UserLoginHistoryID equals h.UserLoginHistoryID
										 where f.FeedbackBoxType == feedbackBoxType
										 select new Feedback
										 {
											 UserFeedbackID = f.UserFeedbackID,
											 FeedbackNo = f.FeedbackNo,
											 FeedbackBoxType = f.FeedbackBoxType,
											 MovedFromFeedbackBoxType = f.MovedFromFeedbackBoxType,
											 FeedbackType = f.FeedbackType,
											 FeedbackDetails = f.FeedbackDetails,
											 FeedbackDate = f.FeedbackDate,
											 StudentName = h.StudentName,
											 StudentEnrollment = h.StudentEnrollment,
											 StudentPersonalEmail = h.StudentPersonalEmail,
											 StudentUniversityEmail = h.StudentUniversityEmail,
											 FacultyMemberName = h.FacultyMemberName,
											 FacultyMemberEmail = h.FacultyMemberEmail,
											 UserName = h.UserName,
											 UserEmail = h.UserEmail,
											 UserTypeEnum = h.UserTypeEnum,
											 Archived = f.Archived,
											 Status = f.Status,
											 Unread = f.Unread,
											 AssignedToMembers = f.UserFeedbackAssignees.Select(a => a.UserFeedbackMember.MemberDesignation).ToList()
										 };

				var statistics = userFeedbacksQuery.GroupBy(f => new { f.Status })
					.Select(g => new FeedBackStatistics
					{
						Status = g.Key.Status,
						Count = g.Count()
					}).ToList();

				if (feedbackTypeEnum != null)
				{
					var feedbackType = feedbackTypeEnum.Value.GetGuid();
					userFeedbacksQuery = userFeedbacksQuery.Where(f => f.FeedbackType == feedbackType);
				}
				if (userTypeEnum != null)
					userFeedbacksQuery = userFeedbacksQuery.Where(f => (short)f.UserTypeEnum == (short)userTypeEnum.Value);
				if (statusEnum != null)
				{
					var status = statusEnum.GetGuid();
					userFeedbacksQuery = userFeedbacksQuery.Where(f => f.Status == status);
				}
				if (unread != null)
					userFeedbacksQuery = userFeedbacksQuery.Where(f => f.Unread == unread.Value);
				if (assignedToUserFeedbackMemberID != null)
					userFeedbacksQuery = userFeedbacksQuery.Where(f => aspireContext.UserFeedbackAssignees.Any(a => a.UserFeedbackID == f.UserFeedbackID && a.UserFeedbackMemberID == assignedToUserFeedbackMemberID.Value));
				if (archived != null)
					userFeedbacksQuery = userFeedbacksQuery.Where(f => f.Archived == archived.Value);
				searchText = searchText.ToNullIfWhiteSpace();
				if (searchText != null)
					userFeedbacksQuery = userFeedbacksQuery.Where(f => f.FeedbackNo.Contains(searchText) || f.FeedbackDetails.Contains(searchText));

				var filteredStatistics = userFeedbacksQuery.GroupBy(f => new { f.Status })
					.Select(g => new FeedBackStatistics
					{
						Status = g.Key.Status,
						Count = g.Count()
					}).ToList();

				var virtualItemsCount = userFeedbacksQuery.Count();

				switch (sortExpression)
				{
					case nameof(Feedback.UserFeedbackID):
						userFeedbacksQuery = userFeedbacksQuery.OrderBy(sortDirection, f => f.UserFeedbackID);
						break;
					case nameof(Feedback.FeedbackNo):
						userFeedbacksQuery = userFeedbacksQuery.OrderBy(sortDirection, f => f.FeedbackNo);
						break;
					case nameof(Feedback.FeedbackBoxType):
						userFeedbacksQuery = userFeedbacksQuery.OrderBy(sortDirection, f => f.MovedFromFeedbackBoxType).ThenBy(sortDirection, f => f.FeedbackBoxType);
						break;
					case nameof(Feedback.FeedbackType):
						userFeedbacksQuery = userFeedbacksQuery.OrderBy(sortDirection, f => f.FeedbackType);
						break;
					case nameof(Feedback.FeedbackDate):
						userFeedbacksQuery = userFeedbacksQuery.OrderBy(sortDirection, f => f.FeedbackDate);
						break;
					case nameof(Feedback.UserTypeEnum):
						userFeedbacksQuery = userFeedbacksQuery.OrderBy(sortDirection, f => f.UserTypeEnum);
						break;
					case nameof(Feedback.Status):
						userFeedbacksQuery = userFeedbacksQuery.OrderBy(sortDirection, f => f.Status);
						break;
					case nameof(Feedback.Archived):
						userFeedbacksQuery = userFeedbacksQuery.OrderBy(sortDirection, f => f.Archived);
						break;
					case nameof(Feedback.Unread):
						userFeedbacksQuery = userFeedbacksQuery.OrderBy(sortDirection, f => f.Unread);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return (userFeedbacksQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList(), virtualItemsCount, filteredStatistics, statistics);
			}
		}

		public sealed class FeedbackDetailed
		{
			internal FeedbackDetailed() { }
			public Guid UserFeedbackID { get; internal set; }
			public string FeedbackNo { get; internal set; }
			public Guid FeedbackType { get; internal set; }
			public Guid FeedbackBoxType { get; internal set; }
			public Guid? MovedFromFeedbackBoxType { get; internal set; }
			public Model.Entities.UserFeedback.FeedbackBoxTypes FeedbackBoxTypeEnum => this.FeedbackBoxType.GetEnum<Model.Entities.UserFeedback.FeedbackBoxTypes>();
			public DateTime FeedbackDate { get; internal set; }
			public Guid Status { get; internal set; }
			public UserTypes UserTypeEnum { get; internal set; }
			public string StudentName { get; internal set; }
			public string StudentEnrollment { get; internal set; }
			public string StudentPersonalEmail { get; internal set; }
			public string StudentUniversityEmail { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public string FacultyMemberEmail { get; internal set; }
			public string UserName { get; internal set; }
			public string UserEmail { get; internal set; }
			public string Name
			{
				get
				{
					switch (this.UserTypeEnum)
					{
						case UserTypes.Executive:
						case UserTypes.Staff:
							return this.UserName;
						case UserTypes.Student:
							return $"<{this.StudentEnrollment}> {this.StudentName}";
						case UserTypes.Faculty:
							return this.FacultyMemberName;
						default:
							throw new NotImplementedEnumException(this.UserTypeEnum);
					}
				}
			}
			public string Email
			{
				get
				{
					switch (this.UserTypeEnum)
					{
						case UserTypes.Executive:
						case UserTypes.Staff:
							return this.UserEmail;
						case UserTypes.Student:
							return this.StudentUniversityEmail.TrimAndMakeItNullIfEmpty() ?? this.StudentPersonalEmail;
						case UserTypes.Faculty:
							return this.FacultyMemberEmail;
						default:
							throw new NotImplementedEnumException(this.UserTypeEnum);
					}
				}
			}
			public int UserLoginHistoryID { get; internal set; }
			public string FeedbackDetails { get; internal set; }
			public string Notes { get; internal set; }
			public bool Archived { get; internal set; }
			public bool Unread { get; internal set; }
			public List<Assignee> Assignees { get; internal set; }
			public List<Discussion> Discussions { get; internal set; }

			public sealed class Assignee
			{
				internal Assignee() { }
				public Guid UserFeedbackMemberID { get; internal set; }
				public string Designation { get; internal set; }
			}

			public sealed class Discussion
			{
				internal Discussion() { }
				public Guid UserFeedbackDiscussionID { get; internal set; }
				public string Details { get; internal set; }
				public DateTime CreatedDate { get; internal set; }
				public int CreatedByUserLoginHistoryID { get; internal set; }
				public UserTypes UserTypeEnum { get; internal set; }
				public string StudentName { get; internal set; }
				public string StudentEnrollment { get; internal set; }
				public string StudentPersonalEmail { get; internal set; }
				public string StudentUniversityEmail { get; internal set; }
				public string FacultyMemberName { get; internal set; }
				public string FacultyMemberEmail { get; internal set; }
				public string UserName { get; internal set; }
				public string UserEmail { get; internal set; }
				public string CreatedByName
				{
					get
					{
						switch (this.UserTypeEnum)
						{
							case UserTypes.Executive:
							case UserTypes.Staff:
								return this.UserName;
							case UserTypes.Student:
								return $"<{this.StudentEnrollment}> {this.StudentName}";
							case UserTypes.Faculty:
								return this.FacultyMemberName;
							default:
								throw new NotImplementedEnumException(this.UserTypeEnum);
						}
					}
				}
				public string CreatedByEmail
				{
					get
					{
						switch (this.UserTypeEnum)
						{
							case UserTypes.Executive:
							case UserTypes.Staff:
								return this.UserEmail;
							case UserTypes.Student:
								return this.StudentUniversityEmail.TrimAndMakeItNullIfEmpty() ?? this.StudentPersonalEmail;
							case UserTypes.Faculty:
								return this.FacultyMemberEmail;
							default:
								throw new NotImplementedEnumException(this.UserTypeEnum);
						}
					}
				}
				public string NameToBeDisplayed => this.CreatedByEmail == "rector@bahria.edu.pk" ? "Rector" : this.CreatedByName;
			}
		}

		public static List<FeedbackDetailed> GetUserFeedbacks(List<Guid> userFeedbackIDs, Guid loginSessionGuid)
		{
			if (userFeedbackIDs?.Any() != true)
				throw new ArgumentNullException(nameof(userFeedbackIDs));
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.Feedbacks.Module, Model.Entities.UserGroupPermission.Allowed);
				var userFeedbacks = aspireContext.UserFeedbacks.Where(f => userFeedbackIDs.Contains(f.UserFeedbackID))
					.Select(f => new FeedbackDetailed
					{
						UserFeedbackID = f.UserFeedbackID,
						UserLoginHistoryID = f.UserLoginHistoryID,
						FeedbackBoxType = f.FeedbackBoxType,
						MovedFromFeedbackBoxType = f.MovedFromFeedbackBoxType,
						FeedbackNo = f.FeedbackNo,
						FeedbackDate = f.FeedbackDate,
						FeedbackDetails = f.FeedbackDetails,
						Status = f.Status,
						Notes = f.Notes,
						FeedbackType = f.FeedbackType,
						Assignees = f.UserFeedbackAssignees.Select(a => new FeedbackDetailed.Assignee
						{
							Designation = a.UserFeedbackMember.MemberDesignation,
							UserFeedbackMemberID = a.UserFeedbackMemberID
						}).ToList(),
						Discussions = f.UserFeedbackDiscussions.Select(d => new FeedbackDetailed.Discussion
						{
							CreatedDate = d.CreatedDate,
							UserFeedbackDiscussionID = d.UserFeedbackDiscussionID,
							Details = d.Details,
							CreatedByUserLoginHistoryID = d.CreatedByUserLoginHistoryID
						}).ToList(),
						Archived = f.Archived,
						Unread = f.Unread
					}).ToList();

				userFeedbacks.Select(f => f.FeedbackBoxType).Distinct().ForEach(t =>
				{
					var feedbackBoxTypeEnum = t.GetEnum<Model.Entities.UserFeedback.FeedbackBoxTypes>();
					switch (feedbackBoxTypeEnum)
					{
						case Model.Entities.UserFeedback.FeedbackBoxTypes.Rector:
							aspireContext.DemandExecutivePermissions(loginSessionGuid, ExecutivePermissions.Feedbacks.CanViewRectorBox, Model.Entities.UserGroupPermission.PermissionValues.Allowed);
							break;
						case Model.Entities.UserFeedback.FeedbackBoxTypes.DirectorGeneralCampus:
							aspireContext.DemandExecutivePermissions(loginSessionGuid, ExecutivePermissions.Feedbacks.CanViewDirectorGeneralBox, Model.Entities.UserGroupPermission.PermissionValues.Allowed);
							break;
						default:
							throw new NotImplementedEnumException(feedbackBoxTypeEnum);
					}
				});

				foreach (var userFeedback in userFeedbacks)
				{
					var userLoginHistory = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == userFeedback.UserLoginHistoryID).Select(h => new
					{
						UserTypeEnum = (UserTypes)h.UserType,
						h.StudentID,
						StudentInstituteID = (int?)h.Student.AdmissionOpenProgram.Program.InstituteID,
						StudentName = h.Student.Name,
						StudentEnrollment = h.Student.Enrollment,
						StudentPersonalEmail = h.Student.PersonalEmail,
						StudentUniversityEmail = h.Student.UniversityEmail,
						h.FacultyMemberID,
						FacultyMemberName = h.FacultyMember.Name,
						FacultyMemberEmail = h.FacultyMember.Email,
						FacultyMemberInstituteID = (int?)h.FacultyMember.InstituteID,
						h.UserRoleID,
						UserRoleInstituteID = h.UserRole.InstituteID,
						UserRoleName = h.UserRole.User1.Name,
						UserRoleEmail = h.UserRole.User1.Email
					}).Single();

					userFeedback.UserTypeEnum = userLoginHistory.UserTypeEnum;
					userFeedback.StudentEnrollment = userLoginHistory.StudentEnrollment;
					userFeedback.StudentName = userLoginHistory.StudentName;
					userFeedback.StudentPersonalEmail = userLoginHistory.StudentPersonalEmail;
					userFeedback.StudentUniversityEmail = userLoginHistory.StudentUniversityEmail;
					userFeedback.FacultyMemberName = userLoginHistory.FacultyMemberName;
					userFeedback.FacultyMemberEmail = userLoginHistory.FacultyMemberEmail;
					userFeedback.UserEmail = userLoginHistory.UserRoleEmail;
					userFeedback.UserName = userLoginHistory.UserRoleName;
					userFeedback.UserTypeEnum = userLoginHistory.UserTypeEnum;
					switch (userFeedback.UserTypeEnum)
					{
						case UserTypes.Executive:
						case UserTypes.Staff:
							if (userLoginHistory.UserRoleInstituteID != loginHistory.InstituteID)
								return null;
							break;
						case UserTypes.Student:
							if (userLoginHistory.StudentInstituteID != loginHistory.InstituteID)
								return null;
							break;
						case UserTypes.Faculty:
							if (userLoginHistory.FacultyMemberInstituteID != loginHistory.InstituteID)
								return null;
							break;
						default:
							throw new NotImplementedEnumException(userLoginHistory.UserTypeEnum);
					}

					userFeedback.Discussions.ForEach(d =>
					{
						var userLoginHistoryDiscussion = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == d.CreatedByUserLoginHistoryID).Select(h => new
						{
							UserTypeEnum = (UserTypes)h.UserType,
							h.StudentID,
							StudentInstituteID = (int?)h.Student.AdmissionOpenProgram.Program.InstituteID,
							StudentName = h.Student.Name,
							StudentEnrollment = h.Student.Enrollment,
							StudentPersonalEmail = h.Student.PersonalEmail,
							StudentUniversityEmail = h.Student.UniversityEmail,
							h.FacultyMemberID,
							FacultyMemberName = h.FacultyMember.Name,
							FacultyMemberEmail = h.FacultyMember.Email,
							FacultyMemberInstituteID = (int?)h.FacultyMember.InstituteID,
							h.UserRoleID,
							UserRoleInstituteID = h.UserRole.InstituteID,
							UserRoleName = h.UserRole.User1.Name,
							UserRoleEmail = h.UserRole.User1.Email
						}).Single();

						d.UserTypeEnum = userLoginHistoryDiscussion.UserTypeEnum;
						d.StudentEnrollment = userLoginHistoryDiscussion.StudentEnrollment;
						d.StudentName = userLoginHistoryDiscussion.StudentName;
						d.StudentPersonalEmail = userLoginHistoryDiscussion.StudentPersonalEmail;
						d.StudentUniversityEmail = userLoginHistoryDiscussion.StudentUniversityEmail;
						d.FacultyMemberName = userLoginHistoryDiscussion.FacultyMemberName;
						d.FacultyMemberEmail = userLoginHistoryDiscussion.FacultyMemberEmail;
						d.UserEmail = userLoginHistoryDiscussion.UserRoleEmail;
						d.UserName = userLoginHistoryDiscussion.UserRoleName;
						d.UserTypeEnum = userLoginHistoryDiscussion.UserTypeEnum;
					});
				}
				return userFeedbacks;
			}
		}

		public static FeedbackDetailed GetUserFeedback(Guid userFeedbackID, Guid loginSessionGuid)
		{
			return GetUserFeedbacks(new List<Guid> { userFeedbackID }, loginSessionGuid).SingleOrDefault();
		}

		public enum UpdateUserFeedbackStatuses
		{
			NoRecordFound,
			StatusIsClosed,
			Success,
		}

		private static void DemandCanRespondPermissions(this AspireContext aspireContext, Model.Entities.UserFeedback userFeedback, Guid loginSessionGuid)
		{
			switch (userFeedback.FeedbackBoxTypeEnum)
			{
				case Model.Entities.UserFeedback.FeedbackBoxTypes.Rector:
					aspireContext.DemandExecutivePermissions(loginSessionGuid, ExecutivePermissions.Feedbacks.CanRespondAsRector, Model.Entities.UserGroupPermission.PermissionValues.Allowed);
					break;
				case Model.Entities.UserFeedback.FeedbackBoxTypes.DirectorGeneralCampus:
					aspireContext.DemandExecutivePermissions(loginSessionGuid, ExecutivePermissions.Feedbacks.CanRespondAsDirectorGeneral, Model.Entities.UserGroupPermission.PermissionValues.Allowed);
					break;
				default:
					throw new NotImplementedEnumException(userFeedback.FeedbackBoxTypeEnum);
			}
		}

		public static UpdateUserFeedbackStatuses UpdateUserFeedback(Guid userFeedbackID, string notes, List<Guid> assignedToUserFeedbackMemberIDs, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.Feedbacks.Module, Model.Entities.UserGroupPermission.Allowed);
				var userFeedback = aspireContext.UserFeedbacks.Include(f => f.UserFeedbackAssignees).SingleOrDefault(f => f.UserFeedbackID == userFeedbackID);
				if (userFeedback == null)
					return UpdateUserFeedbackStatuses.NoRecordFound;
				var now = DateTime.Now;

				var userLoginHistory = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == userFeedback.UserLoginHistoryID)
					.Select(h => new
					{
						UserTypeEnum = (UserTypes)h.UserType,
						StudentInstituteID = (int?)h.Student.AdmissionOpenProgram.Program.InstituteID,
						FacultyMemberInstituteID = (int?)h.FacultyMember.InstituteID,
						UserRoleInstituteID = h.UserRole.InstituteID
					}).Single();

				switch (userLoginHistory.UserTypeEnum)
				{
					case UserTypes.Executive:
					case UserTypes.Staff:
						if (userLoginHistory.UserRoleInstituteID != loginHistory.InstituteID)
							return UpdateUserFeedbackStatuses.NoRecordFound;
						break;
					case UserTypes.Student:
						if (userLoginHistory.StudentInstituteID != loginHistory.InstituteID)
							return UpdateUserFeedbackStatuses.NoRecordFound;
						break;
					case UserTypes.Faculty:
						if (userLoginHistory.FacultyMemberInstituteID != loginHistory.InstituteID)
							return UpdateUserFeedbackStatuses.NoRecordFound;
						break;
					default:
						throw new NotImplementedEnumException(userLoginHistory.UserTypeEnum);
				}

				if (userFeedback.StatusEnum == Model.Entities.UserFeedback.Statuses.Closed)
					return UpdateUserFeedbackStatuses.StatusIsClosed;

				aspireContext.DemandCanRespondPermissions(userFeedback, loginSessionGuid);

				userFeedback.Notes = notes.ToNullIfWhiteSpace();

				aspireContext.UserFeedbackAssignees.RemoveRange(userFeedback.UserFeedbackAssignees.Where(a => !assignedToUserFeedbackMemberIDs.Contains(a.UserFeedbackMemberID)));
				if (assignedToUserFeedbackMemberIDs.Any())
				{
					switch (userFeedback.Status.GetEnum())
					{
						case Model.Entities.UserFeedback.Statuses.InProcess:
						case Model.Entities.UserFeedback.Statuses.SpamJunk:
						case Model.Entities.UserFeedback.Statuses.Closed:
							break;
						case Model.Entities.UserFeedback.Statuses.Pending:
							userFeedback.Status = Model.Entities.UserFeedback.Statuses.InProcess.GetGuid();
							break;
						default:
							break;
					}
				}

				assignedToUserFeedbackMemberIDs.Where(a => !userFeedback.UserFeedbackAssignees.Any(fa => fa.UserFeedbackMemberID == a))
					.ForEach(id => userFeedback.UserFeedbackAssignees.Add(new Model.Entities.UserFeedbackAssignee
					{
						UserFeedbackAssigneeID = Guid.NewGuid(),
						UserFeedbackMemberID = id
					}));
				userFeedback.LastUpdatedDate = now;
				userFeedback.Unread = false;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateUserFeedbackStatuses.Success;
			}
		}

		public enum AddDiscussionStatuses
		{
			NoRecordFound,
			StatusIsSpamJunk,
			StatusIsClosed,
			Success
		}

		public static AddDiscussionStatuses AddDiscussion(Guid userFeedbackID, string discussionDetails, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.Feedbacks.Module, Model.Entities.UserGroupPermission.Allowed);
				var userFeedback = aspireContext.UserFeedbacks.Include(f => f.UserFeedbackAssignees).SingleOrDefault(f => f.UserFeedbackID == userFeedbackID);
				if (userFeedback == null)
					return AddDiscussionStatuses.NoRecordFound;
				var now = DateTime.Now;

				var userLoginHistory = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == userFeedback.UserLoginHistoryID)
					.Select(h => new
					{
						UserTypeEnum = (UserTypes)h.UserType,
						StudentInstituteID = (int?)h.Student.AdmissionOpenProgram.Program.InstituteID,
						FacultyMemberInstituteID = (int?)h.FacultyMember.InstituteID,
						UserRoleInstituteID = h.UserRole.InstituteID
					}).Single();

				switch (userLoginHistory.UserTypeEnum)
				{
					case UserTypes.Executive:
					case UserTypes.Staff:
						if (userLoginHistory.UserRoleInstituteID != loginHistory.InstituteID)
							return AddDiscussionStatuses.NoRecordFound;
						break;
					case UserTypes.Student:
						if (userLoginHistory.StudentInstituteID != loginHistory.InstituteID)
							return AddDiscussionStatuses.NoRecordFound;
						break;
					case UserTypes.Faculty:
						if (userLoginHistory.FacultyMemberInstituteID != loginHistory.InstituteID)
							return AddDiscussionStatuses.NoRecordFound;
						break;
					default:
						throw new NotImplementedEnumException(userLoginHistory.UserTypeEnum);
				}

				switch (userFeedback.StatusEnum)
				{
					case Model.Entities.UserFeedback.Statuses.SpamJunk:
						return AddDiscussionStatuses.StatusIsSpamJunk;
					case Model.Entities.UserFeedback.Statuses.Closed:
						return AddDiscussionStatuses.StatusIsClosed;
					case Model.Entities.UserFeedback.Statuses.Pending:
						userFeedback.StatusEnum = Model.Entities.UserFeedback.Statuses.InProcess;
						break;
					case Model.Entities.UserFeedback.Statuses.InProcess:
						break;
					default:
						throw new NotImplementedEnumException(userFeedback.StatusEnum);
				}

				aspireContext.DemandCanRespondPermissions(userFeedback, loginSessionGuid);
				userFeedback.UserFeedbackDiscussions.Add(new Model.Entities.UserFeedbackDiscussion
				{
					UserFeedbackDiscussionID = Guid.NewGuid(),
					CreatedByUserLoginHistoryID = loginHistory.UserLoginHistoryID,
					CreatedDate = now,
					Details = discussionDetails,
					StatusEnum = Model.Entities.UserFeedbackDiscussion.Statuses.Visible
				});

				userFeedback.LastUpdatedDate = now;
				userFeedback.Archived = false;
				userFeedback.Unread = false;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddDiscussionStatuses.Success;
			}
		}

		public enum UpdateUserFeedbackStatusResults
		{
			NoRecordFound,
			StatusIsClosed,
			StatusIsAlreadyPending,
			StatusIsAlreadySpamJunk,
			StatusIsAlreadyInProcess,
			Success
		}

		public static UpdateUserFeedbackStatusResults UpdateUserFeedbackStatus(Guid userFeedbackID, Model.Entities.UserFeedback.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.Feedbacks.Module, Model.Entities.UserGroupPermission.Allowed);
				var userFeedback = aspireContext.UserFeedbacks.Include(f => f.UserFeedbackAssignees).SingleOrDefault(f => f.UserFeedbackID == userFeedbackID);
				if (userFeedback == null)
					return UpdateUserFeedbackStatusResults.NoRecordFound;
				var now = DateTime.Now;

				var userLoginHistory = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == userFeedback.UserLoginHistoryID)
					.Select(h => new
					{
						UserTypeEnum = (UserTypes)h.UserType,
						StudentInstituteID = (int?)h.Student.AdmissionOpenProgram.Program.InstituteID,
						FacultyMemberInstituteID = (int?)h.FacultyMember.InstituteID,
						UserRoleInstituteID = h.UserRole.InstituteID
					}).Single();

				switch (userLoginHistory.UserTypeEnum)
				{
					case UserTypes.Executive:
					case UserTypes.Staff:
						if (userLoginHistory.UserRoleInstituteID != loginHistory.InstituteID)
							return UpdateUserFeedbackStatusResults.NoRecordFound;
						break;
					case UserTypes.Student:
						if (userLoginHistory.StudentInstituteID != loginHistory.InstituteID)
							return UpdateUserFeedbackStatusResults.NoRecordFound;
						break;
					case UserTypes.Faculty:
						if (userLoginHistory.FacultyMemberInstituteID != loginHistory.InstituteID)
							return UpdateUserFeedbackStatusResults.NoRecordFound;
						break;
					default:
						throw new NotImplementedEnumException(userLoginHistory.UserTypeEnum);
				}

				aspireContext.DemandCanRespondPermissions(userFeedback, loginSessionGuid);

				switch (statusEnum)
				{
					case Model.Entities.UserFeedback.Statuses.Pending:
						switch (userFeedback.StatusEnum)
						{
							case Model.Entities.UserFeedback.Statuses.Pending:
								return UpdateUserFeedbackStatusResults.StatusIsAlreadyPending;
							case Model.Entities.UserFeedback.Statuses.SpamJunk:
							case Model.Entities.UserFeedback.Statuses.InProcess:
								break;
							case Model.Entities.UserFeedback.Statuses.Closed:
								return UpdateUserFeedbackStatusResults.StatusIsClosed;
							default:
								throw new NotImplementedEnumException(userFeedback.StatusEnum);
						}
						userFeedback.Archived = false;
						break;
					case Model.Entities.UserFeedback.Statuses.SpamJunk:
						switch (userFeedback.StatusEnum)
						{
							case Model.Entities.UserFeedback.Statuses.Pending:
							case Model.Entities.UserFeedback.Statuses.InProcess:
								break;
							case Model.Entities.UserFeedback.Statuses.SpamJunk:
								return UpdateUserFeedbackStatusResults.StatusIsAlreadySpamJunk;
							case Model.Entities.UserFeedback.Statuses.Closed:
								return UpdateUserFeedbackStatusResults.StatusIsClosed;
							default:
								throw new NotImplementedEnumException(userFeedback.StatusEnum);
						}
						break;
					case Model.Entities.UserFeedback.Statuses.InProcess:
						switch (userFeedback.StatusEnum)
						{
							case Model.Entities.UserFeedback.Statuses.Pending:
							case Model.Entities.UserFeedback.Statuses.SpamJunk:
								break;
							case Model.Entities.UserFeedback.Statuses.InProcess:
								return UpdateUserFeedbackStatusResults.StatusIsAlreadyInProcess;
							case Model.Entities.UserFeedback.Statuses.Closed:
								return UpdateUserFeedbackStatusResults.StatusIsClosed;
							default:
								throw new NotImplementedEnumException(userFeedback.StatusEnum);
						}
						userFeedback.Archived = false;
						break;
					case Model.Entities.UserFeedback.Statuses.Closed:
						throw new InvalidOperationException();
					default:
						throw new NotImplementedEnumException(statusEnum);
				}

				userFeedback.StatusEnum = statusEnum;
				userFeedback.Unread = false;
				userFeedback.LastUpdatedDate = now;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateUserFeedbackStatusResults.Success;
			}
		}

		public enum ArchiveUserFeedbackStatuses
		{
			NoRecordFound,
			AlreadyArchived,
			AlreadyNotArchived,
			PendingStatusCannotBeArchived,
			InProcessStatusCannotBeArchived,
			Success
		}

		public static ArchiveUserFeedbackStatuses UpdateUserFeedbackArchiveStatus(Guid userFeedbackID, bool archive, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.Feedbacks.Module, Model.Entities.UserGroupPermission.Allowed);
				var userFeedback = aspireContext.UserFeedbacks.Include(f => f.UserFeedbackAssignees).SingleOrDefault(f => f.UserFeedbackID == userFeedbackID);
				if (userFeedback == null)
					return ArchiveUserFeedbackStatuses.NoRecordFound;
				var now = DateTime.Now;

				var userLoginHistory = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == userFeedback.UserLoginHistoryID)
					.Select(h => new
					{
						UserTypeEnum = (UserTypes)h.UserType,
						StudentInstituteID = (int?)h.Student.AdmissionOpenProgram.Program.InstituteID,
						FacultyMemberInstituteID = (int?)h.FacultyMember.InstituteID,
						UserRoleInstituteID = h.UserRole.InstituteID
					}).Single();

				switch (userLoginHistory.UserTypeEnum)
				{
					case UserTypes.Executive:
					case UserTypes.Staff:
						if (userLoginHistory.UserRoleInstituteID != loginHistory.InstituteID)
							return ArchiveUserFeedbackStatuses.NoRecordFound;
						break;
					case UserTypes.Student:
						if (userLoginHistory.StudentInstituteID != loginHistory.InstituteID)
							return ArchiveUserFeedbackStatuses.NoRecordFound;
						break;
					case UserTypes.Faculty:
						if (userLoginHistory.FacultyMemberInstituteID != loginHistory.InstituteID)
							return ArchiveUserFeedbackStatuses.NoRecordFound;
						break;
					default:
						throw new NotImplementedEnumException(userLoginHistory.UserTypeEnum);
				}

				aspireContext.DemandCanRespondPermissions(userFeedback, loginSessionGuid);

				if (userFeedback.Archived && archive)
					return ArchiveUserFeedbackStatuses.AlreadyArchived;
				if (!userFeedback.Archived && !archive)
					return ArchiveUserFeedbackStatuses.AlreadyNotArchived;
				if (archive)
					switch (userFeedback.StatusEnum)
					{
						case Model.Entities.UserFeedback.Statuses.SpamJunk:
						case Model.Entities.UserFeedback.Statuses.Closed:
							break;
						case Model.Entities.UserFeedback.Statuses.Pending:
							return ArchiveUserFeedbackStatuses.PendingStatusCannotBeArchived;
						case Model.Entities.UserFeedback.Statuses.InProcess:
							return ArchiveUserFeedbackStatuses.InProcessStatusCannotBeArchived;
							throw new InvalidOperationException();
						default:
							throw new NotImplementedEnumException(userFeedback.StatusEnum);
					}

				userFeedback.Archived = archive;
				userFeedback.Unread = false;
				userFeedback.LastUpdatedDate = now;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return ArchiveUserFeedbackStatuses.Success;
			}
		}

		public enum UpdateUserFeedbackReadStatusResults
		{
			NoRecordFound,
			AlreadyUnread,
			AlreadyRead,
			Success,
		}

		public static UpdateUserFeedbackReadStatusResults UpdateUserFeedbackReadStatus(Guid userFeedbackID, bool unread, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.Feedbacks.Module, Model.Entities.UserGroupPermission.Allowed);
				var userFeedback = aspireContext.UserFeedbacks.Include(f => f.UserFeedbackAssignees).SingleOrDefault(f => f.UserFeedbackID == userFeedbackID);
				if (userFeedback == null)
					return UpdateUserFeedbackReadStatusResults.NoRecordFound;
				var now = DateTime.Now;

				var userLoginHistory = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == userFeedback.UserLoginHistoryID)
					.Select(h => new
					{
						UserTypeEnum = (UserTypes)h.UserType,
						StudentInstituteID = (int?)h.Student.AdmissionOpenProgram.Program.InstituteID,
						FacultyMemberInstituteID = (int?)h.FacultyMember.InstituteID,
						UserRoleInstituteID = h.UserRole.InstituteID
					}).Single();

				switch (userLoginHistory.UserTypeEnum)
				{
					case UserTypes.Executive:
					case UserTypes.Staff:
						if (userLoginHistory.UserRoleInstituteID != loginHistory.InstituteID)
							return UpdateUserFeedbackReadStatusResults.NoRecordFound;
						break;
					case UserTypes.Student:
						if (userLoginHistory.StudentInstituteID != loginHistory.InstituteID)
							return UpdateUserFeedbackReadStatusResults.NoRecordFound;
						break;
					case UserTypes.Faculty:
						if (userLoginHistory.FacultyMemberInstituteID != loginHistory.InstituteID)
							return UpdateUserFeedbackReadStatusResults.NoRecordFound;
						break;
					default:
						throw new NotImplementedEnumException(userLoginHistory.UserTypeEnum);
				}

				aspireContext.DemandCanRespondPermissions(userFeedback, loginSessionGuid);

				if (userFeedback.Unread == unread)
					return unread ? UpdateUserFeedbackReadStatusResults.AlreadyUnread : UpdateUserFeedbackReadStatusResults.AlreadyRead;

				switch (userFeedback.StatusEnum)
				{
					case Model.Entities.UserFeedback.Statuses.Pending:
					case Model.Entities.UserFeedback.Statuses.InProcess:
						userFeedback.Unread = unread;
						break;
					case Model.Entities.UserFeedback.Statuses.SpamJunk:
					case Model.Entities.UserFeedback.Statuses.Closed:
						userFeedback.Unread = false;
						break;
					default:
						throw new NotImplementedEnumException(userFeedback.StatusEnum);
				}

				userFeedback.LastUpdatedDate = now;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateUserFeedbackReadStatusResults.Success;
			}
		}

		public enum CloseUserFeedbackStatuses
		{
			NoRecordFound,
			AlreadyClosed,
			CannotClosedWithoutProcessing,
			StatusCannotBeClosedFromSpamJunk,
			StatusCannotBeClosedFromPending,
			Success,
		}

		public static CloseUserFeedbackStatuses CloseUserFeedback(Guid userFeedbackID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.Feedbacks.Module, Model.Entities.UserGroupPermission.Allowed);
				var userFeedback = aspireContext.UserFeedbacks.SingleOrDefault(f => f.UserFeedbackID == userFeedbackID);
				if (userFeedback == null)
					return CloseUserFeedbackStatuses.NoRecordFound;
				var now = DateTime.Now;

				var userLoginHistory = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == userFeedback.UserLoginHistoryID)
					.Select(h => new
					{
						UserTypeEnum = (UserTypes)h.UserType,
						StudentInstituteID = (int?)h.Student.AdmissionOpenProgram.Program.InstituteID,
						FacultyMemberInstituteID = (int?)h.FacultyMember.InstituteID,
						UserRoleInstituteID = h.UserRole.InstituteID
					}).Single();

				switch (userLoginHistory.UserTypeEnum)
				{
					case UserTypes.Executive:
					case UserTypes.Staff:
						if (userLoginHistory.UserRoleInstituteID != loginHistory.InstituteID)
							return CloseUserFeedbackStatuses.NoRecordFound;
						break;
					case UserTypes.Student:
						if (userLoginHistory.StudentInstituteID != loginHistory.InstituteID)
							return CloseUserFeedbackStatuses.NoRecordFound;
						break;
					case UserTypes.Faculty:
						if (userLoginHistory.FacultyMemberInstituteID != loginHistory.InstituteID)
							return CloseUserFeedbackStatuses.NoRecordFound;
						break;
					default:
						throw new NotImplementedEnumException(userLoginHistory.UserTypeEnum);
				}

				aspireContext.DemandCanRespondPermissions(userFeedback, loginSessionGuid);

				switch (userFeedback.StatusEnum)
				{
					case Model.Entities.UserFeedback.Statuses.SpamJunk:
						return CloseUserFeedbackStatuses.StatusCannotBeClosedFromSpamJunk;
					case Model.Entities.UserFeedback.Statuses.Closed:
						return CloseUserFeedbackStatuses.AlreadyClosed;
					case Model.Entities.UserFeedback.Statuses.Pending:
						return CloseUserFeedbackStatuses.StatusCannotBeClosedFromPending;
					case Model.Entities.UserFeedback.Statuses.InProcess:
						if (!aspireContext.UserFeedbackDiscussions.Any(d => d.UserFeedbackID == userFeedback.UserFeedbackID))
							return CloseUserFeedbackStatuses.CannotClosedWithoutProcessing;
						userFeedback.LastUpdatedDate = now;
						userFeedback.StatusEnum = Model.Entities.UserFeedback.Statuses.Closed;
						userFeedback.Unread = false;
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return CloseUserFeedbackStatuses.Success;
					default:
						throw new NotImplementedEnumException(userFeedback.StatusEnum);
				}
			}
		}

		public enum UpdateBoxTypeStatuses
		{
			NoRecordFound,
			CanNotMoveToRector,
			AlreadyInRectorBox,
			AlreadyInDGBox,
			Success
		}

		public static UpdateBoxTypeStatuses UpdateBoxType(Guid userFeedbackID, Model.Entities.UserFeedback.FeedbackBoxTypes feedbackBoxTypeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.Feedbacks.Module, Model.Entities.UserGroupPermission.Allowed);
				var userFeedback = aspireContext.UserFeedbacks.SingleOrDefault(f => f.UserFeedbackID == userFeedbackID);
				if (userFeedback == null)
					return UpdateBoxTypeStatuses.NoRecordFound;
				var now = DateTime.Now;

				var userLoginHistory = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == userFeedback.UserLoginHistoryID)
					.Select(h => new
					{
						UserTypeEnum = (UserTypes)h.UserType,
						StudentInstituteID = (int?)h.Student.AdmissionOpenProgram.Program.InstituteID,
						FacultyMemberInstituteID = (int?)h.FacultyMember.InstituteID,
						UserRoleInstituteID = h.UserRole.InstituteID
					}).Single();

				switch (userLoginHistory.UserTypeEnum)
				{
					case UserTypes.Executive:
					case UserTypes.Staff:
						if (userLoginHistory.UserRoleInstituteID != loginHistory.InstituteID)
							return UpdateBoxTypeStatuses.NoRecordFound;
						break;
					case UserTypes.Student:
						if (userLoginHistory.StudentInstituteID != loginHistory.InstituteID)
							return UpdateBoxTypeStatuses.NoRecordFound;
						break;
					case UserTypes.Faculty:
						if (userLoginHistory.FacultyMemberInstituteID != loginHistory.InstituteID)
							return UpdateBoxTypeStatuses.NoRecordFound;
						break;
					default:
						throw new NotImplementedEnumException(userLoginHistory.UserTypeEnum);
				}

				aspireContext.DemandCanRespondPermissions(userFeedback, loginSessionGuid);

				switch (userFeedback.FeedbackBoxTypeEnum)
				{
					case Model.Entities.UserFeedback.FeedbackBoxTypes.Rector:
						switch (feedbackBoxTypeEnum)
						{
							case Model.Entities.UserFeedback.FeedbackBoxTypes.Rector:
								return UpdateBoxTypeStatuses.AlreadyInRectorBox;
							case Model.Entities.UserFeedback.FeedbackBoxTypes.DirectorGeneralCampus:
								break;
							default:
								throw new NotImplementedEnumException(feedbackBoxTypeEnum);
						}
						break;
					case Model.Entities.UserFeedback.FeedbackBoxTypes.DirectorGeneralCampus:
						switch (feedbackBoxTypeEnum)
						{
							case Model.Entities.UserFeedback.FeedbackBoxTypes.Rector:
								return UpdateBoxTypeStatuses.CanNotMoveToRector;
							case Model.Entities.UserFeedback.FeedbackBoxTypes.DirectorGeneralCampus:
								return UpdateBoxTypeStatuses.AlreadyInDGBox;
							default:
								throw new NotImplementedEnumException(feedbackBoxTypeEnum);
						}
					default:
						throw new NotImplementedEnumException(userFeedback.FeedbackBoxTypeEnum);
				}
				userFeedback.MovedFromFeedbackBoxType = userFeedback.FeedbackBoxType;
				userFeedback.FeedbackBoxTypeEnum = feedbackBoxTypeEnum;
				userFeedback.LastUpdatedDate = now;
				userFeedback.Unread = true;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateBoxTypeStatuses.Success;
			}
		}
	}
}