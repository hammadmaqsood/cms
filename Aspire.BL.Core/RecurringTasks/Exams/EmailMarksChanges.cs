﻿using Aspire.BL.Core.Common.EmailsManagement;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace Aspire.BL.Core.RecurringTasks.Exams
{
	public sealed class EmailMarksChanges : BaseTask
	{
		protected override Guid TaskGuid => new Guid("9819337C-0186-4183-8D2A-DF6D2C9D54A2");

#if DEBUG
		protected override bool Enabled => true;
#else
		protected override bool Enabled => true;
#endif

		protected override TimeSpan PollingTimeSpan => TimeSpan.FromMinutes(1);

		protected override TimeSpan RetryTimeSpan => TimeSpan.FromMinutes(5);

		protected override DateTime RunOn => DateTime.Now.TruncateHour();

		protected override DateTime NextRunOn => this.RunOn.AddHours(1);

		private sealed class MarksSheetChange
		{
			public int InstituteID { get; set; }
			public int? DepartmentID { get; set; }
			public int? FacultyMemberID { get; set; }
			public Guid? FacultyMemberEmailID { get; set; }
			public Guid? ManagementEmailRecipient { get; set; }
			public Guid? ManagementEmailID { get; set; }
		}

		private sealed class ProjectThesisInternshipChange
		{
			public int InstituteID { get; set; }
			public int? DepartmentID { get; set; }
			public Guid ManagementEmailRecipient { get; set; }
			public Guid? ManagementEmailID { get; set; }
		}

		private static readonly object Lock = new object();

		protected override void PerformJob(CancellationToken cancellationToken)
		{
			lock (Lock)
			{
				cancellationToken.ThrowIfCancellationRequested();
				List<MarksSheetChange> marksSheetChanges;
				//List<ProjectThesisInternshipChange> projectThesisInternshipChanges;
				using (var aspireContext = new AspireContext())
				{
					marksSheetChanges = (from oc in aspireContext.OfferedCourses
										 join msc in aspireContext.MarksSheetChanges on oc.OfferedCourseID equals msc.OfferedCourseID
										 where msc.FacultyMemberEmailID == null || (msc.ManagementEmailRecipient != null && msc.ManagementEmailID == null)
										 select new MarksSheetChange
										 {
											 InstituteID = oc.Cours.AdmissionOpenProgram.Program.InstituteID,
											 DepartmentID = oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
											 FacultyMemberID = oc.FacultyMemberID,
											 FacultyMemberEmailID = msc.FacultyMemberEmailID,
											 ManagementEmailRecipient = msc.ManagementEmailRecipient,
											 ManagementEmailID = msc.ManagementEmailID
										 }).Distinct().ToList();

					//var ptiQuery= from ptic in aspireContext.ProjectThesisInternshipChanges
					//	join s  in aspireContext.Students on ptic.StudentID equals s.StudentID
					//	join ulh in aspireContext.UserLoginHistories on 
					//	projectThesisInternshipChanges = (from ptic in aspireContext.ProjectThesisInternshipChanges
					//								  join s in aspireContext.Students on ptic.StudentID equals s.StudentID
					//								  where ptic.ManagementEmailRecipient != null && ptic.ManagementEmailID == null
					//								  select new ProjectThesisInternshipChange
					//								  {
					//									  InstituteID = s.AdmissionOpenProgram.Program.InstituteID,
					//									  DepartmentID = s.AdmissionOpenProgram.Program.DepartmentID,
					//									  ManagementEmailRecipient = ptic.ManagementEmailRecipient.Value,
					//									  ManagementEmailID = ptic.ManagementEmailID
					//								  }).Distinct().ToList();
				}

				marksSheetChanges.Where(c => c.FacultyMemberEmailID == null && c.FacultyMemberID != null).Select(c => c.FacultyMemberID.Value).Distinct()
					.ForEach(facultyMemberID => SendMarksSheetChangesToFacultyMember(facultyMemberID, cancellationToken));
				marksSheetChanges.Where(c => c.ManagementEmailRecipient != null && c.ManagementEmailID == null).Select(c => new
				{
					c.InstituteID,
					c.DepartmentID,
					ManagementEmailRecipient = c.ManagementEmailRecipient.Value
				}).Distinct().ForEach(c => SendMarksSheetChangesToManagement(c.InstituteID, c.DepartmentID, c.ManagementEmailRecipient, cancellationToken));
				//projectThesisInternshipChanges.ForEach(c => SendProjectThesisInternshipChangesToManagement(c.InstituteID, c.DepartmentID, c.ManagementEmailRecipient, cancellationToken));
			}
		}

		private sealed class MarksSheetChangeInfo
		{
			public MarksSheetChanx MakMarksSheetChange { get; set; }
			public string InstituteAlias { get; set; }
			public string DepartmentAlias { get; set; }
			public string ProgramAlias { get; set; }
			public SemesterNos SemesterNoEnum { get; set; }
			public Sections SectionEnum { get; set; }
			public Shifts ShiftEnum { get; set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNoEnum, this.SectionEnum, this.ShiftEnum);
			public string Title { get; set; }
			public short SemesterID { get; set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public UserTypes UserTypeEnum { get; set; }
			public string FacultyMemberName { get; set; }
			public string FacultyMemberEmail { get; set; }
			public string StaffMemberName { get; set; }
			public string StaffMemberEmail { get; set; }
			public string ChangedBy
			{
				get
				{
					switch (this.UserTypeEnum)
					{
						case UserTypes.Staff:
							return $"{this.StaffMemberName} ({this.UserTypeEnum.ToFullName()}) <{this.StaffMemberEmail}>";
						case UserTypes.Faculty:
							return $"{this.FacultyMemberName} ({this.UserTypeEnum.ToFullName()}) <{this.FacultyMemberEmail}>";
						default:
							throw new NotImplementedEnumException(this.UserTypeEnum);
					}
				}
			}
		}

		private static string PrepareChanges(List<MarksSheetChangeInfo> changes, string messageHtml)
		{
			var html = new StringBuilder();
			html.Append("<html>");
			{
				html.Append("\n<head>");
				{
					html.Append(@"<style type=""text/css"">
body {
font-family: ""Helvetica Neue"", Helvetica, Arial, sans-serif;
font-size: 13px;
}
table {
border: 1px solid #ddd;
border-collapse: collapse;
border-spacing: 0;
color: rgb(51, 51, 51);
}
th {
border: 1px solid #ddd;
padding: 4px;
text-align: left;
font-weight: bold;
}
td {
border: 1px solid #ddd;
padding: 4px;
text-align: left;
}
</style>");
				}
				html.Append("\n</head>");
				html.Append("\n<body>");
				{
					html.Append(messageHtml);
					html.Append("\n<table>");
					{
						html.Append("\n<thead>");
						{
							html.Append("\n<tr>");
							{
								html.AppendFormat($"<th>{"Semester".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Institute".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Department".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Class".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Course Title".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Faculty Member".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Change Type".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Date".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Changed By".HtmlEncode()}</th>");
							}
							html.Append("\n</tr>");
						}
						html.Append("\n</thead>");
						html.Append("\n<tbody>");
						{
							changes.OrderBy(c => c.Class).ThenBy(c => c.Title).ThenBy(c => c.MakMarksSheetChange.OfferedCourseID).ThenBy(c => c.MakMarksSheetChange.ActionDate)
								.ForEach(c =>
								{
									html.Append("\n<tr>");
									{
										html.AppendFormat($"<td>{c.Semester.HtmlEncode()}</td>");
										html.AppendFormat($"<td>{c.InstituteAlias.HtmlEncode()}</td>");
										html.AppendFormat($"<td>{c.DepartmentAlias.HtmlEncode()}</td>");
										html.AppendFormat($"<td>{c.Class.HtmlEncode()}</td>");
										html.AppendFormat($"<td>{c.Title.HtmlEncode()}</td>");
										html.AppendFormat($"<td>{c.FacultyMemberName.ToNAIfNullOrEmpty().HtmlEncode()}</td>");
										html.AppendFormat($"<td>{c.MakMarksSheetChange.ActionTypeFullName.HtmlEncode()}</td>");
										html.AppendFormat($"<td>{c.MakMarksSheetChange.ActionDate.ToString(CultureInfo.CurrentCulture).HtmlEncode()}</td>");
										html.AppendFormat($"<td>{c.ChangedBy.HtmlEncode()}</td>");
									}
									html.Append("\n</tr>");
								});
						}
						html.Append("\n</tbody>");
					}
					html.Append("\n</table>");
				}
				html.Append("\n</body>");
			}
			html.Append("\n</html>");
			return html.ToString();
		}

		private static void SendMarksSheetChangesToFacultyMember(int facultyMemberID, CancellationToken cancellationToken)
		{
			cancellationToken.ThrowIfCancellationRequested();
			using (var aspireContext = new AspireContext(true))
			{
				var facultyMember = aspireContext.FacultyMembers.Where(fm => fm.FacultyMemberID == facultyMemberID).Select(fm => new
				{
					fm.FacultyMemberID,
					fm.Name,
					fm.Email
				}).SingleOrDefault();
				if (facultyMember?.Email.TryValidateEmailAddress() == null)
					return;
				var changes = (from oc in aspireContext.OfferedCourses
							   join msc in aspireContext.MarksSheetChanges on oc.OfferedCourseID equals msc.OfferedCourseID
							   join ulh in aspireContext.UserLoginHistories on msc.LoginSessionGuid equals ulh.LoginSessionGuid
							   where msc.FacultyMemberEmailID == null && oc.FacultyMemberID == facultyMember.FacultyMemberID
							   select new MarksSheetChangeInfo
							   {
								   MakMarksSheetChange = msc,
								   SemesterID = oc.SemesterID,
								   InstituteAlias = oc.Cours.AdmissionOpenProgram.Program.Institute.InstituteAlias,
								   DepartmentAlias = oc.Cours.AdmissionOpenProgram.Program.Department.DepartmentAlias,
								   ProgramAlias = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
								   SemesterNoEnum = (SemesterNos)oc.SemesterNo,
								   SectionEnum = (Sections)oc.Section,
								   ShiftEnum = (Shifts)oc.Shift,
								   Title = oc.Cours.Title,
								   UserTypeEnum = (UserTypes)ulh.UserType,
								   FacultyMemberName = ulh.FacultyMember.Name,
								   FacultyMemberEmail = ulh.FacultyMember.Email,
								   StaffMemberName = ulh.User.Name,
								   StaffMemberEmail = ulh.User.Email,
							   }).ToList();
				var messageHtml = $@"<p>Dear {facultyMember.Name.HtmlEncode()},</p>
<p>It is to bring in your kind notice through system-generated email that some changes have been made in exams record of your allocated courses. For details, please see the following activity log, however, detailed information can be obtained through CMS using “Change History” option in marks sheet.</p>";
				var emailBody = PrepareChanges(changes, messageHtml);
				var toList = new[] { (facultyMember.Name, facultyMember.Email.ValidateEmailAddress()) };
				var email = aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.MarksSheetChangesToFacultyMember, "Exam Marks Changes", emailBody, true, Email.Priorities.Normal, DateTime.Now, null, UserLoginHistory.SystemUserLoginHistoryID, toList);
				changes.ForEach(c => c.MakMarksSheetChange.FacultyMemberEmailID = email.EmailID);
				aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
			}
		}

		private static void SendMarksSheetChangesToManagement(int instituteID, int? departmentID, Guid managementEmailRecipient, CancellationToken cancellationToken)
		{
			cancellationToken.ThrowIfCancellationRequested();
			using (var aspireContext = new AspireContext(true))
			{
				var designation = Contact.Designations.DirectorExams.GetGuid();
				var directorExams = aspireContext.Contacts
					.Where(c => c.Designation == designation)
					.Select(c => new { c.Name, c.Email }).SingleOrDefault();

				designation = Contact.Designations.DirectorCampus.GetGuid();
				var directorCampus = aspireContext.Contacts
					.Where(c => c.InstituteID == instituteID && c.Designation == designation)
					.Select(c => new { c.Name, c.Email }).SingleOrDefault();

				designation = Contact.Designations.DeputyDirectorExams.GetGuid();
				var deputyDirectorExams = aspireContext.Contacts
					.Where(c => c.InstituteID == instituteID && c.Designation == designation)
					.Select(c => new { c.Name, c.Email }).SingleOrDefault();

				designation = Contact.Designations.AssistantDirectorExam.GetGuid();
				var assistantDirectorExam = aspireContext.Contacts
					.Where(c => c.InstituteID == instituteID && c.Designation == designation)
					.Select(c => new { c.Name, c.Email }).SingleOrDefault();

				designation = Contact.Designations.DeputyDirectorAcademics.GetGuid();
				var deputyDirectorAcademics = aspireContext.Contacts
					.Where(c => c.InstituteID == instituteID && c.Designation == designation)
					.Select(c => new { c.Name, c.Email }).SingleOrDefault();

				designation = Contact.Designations.Dean.GetGuid();
				var dean = aspireContext.Contacts
					.Where(c => c.InstituteID == instituteID && c.DepartmentID == departmentID && c.Designation == designation)
					.Select(c => new { c.Name, c.Email }).SingleOrDefault();

				designation = Contact.Designations.HeadOfDepartment.GetGuid();
				var hod = aspireContext.Contacts
					.Where(c => c.InstituteID == instituteID && c.DepartmentID == departmentID && c.Designation == designation)
					.Select(c => new { c.Name, c.Email }).SingleOrDefault();

				var changes = (from oc in aspireContext.OfferedCourses
							   join msc in aspireContext.MarksSheetChanges on oc.OfferedCourseID equals msc.OfferedCourseID
							   join ulh in aspireContext.UserLoginHistories on msc.LoginSessionGuid equals ulh.LoginSessionGuid
							   where msc.ManagementEmailRecipient == managementEmailRecipient && msc.ManagementEmailID == null
																&& oc.Cours.AdmissionOpenProgram.Program.InstituteID == instituteID
																&& oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID
							   select new MarksSheetChangeInfo
							   {
								   MakMarksSheetChange = msc,
								   SemesterID = oc.SemesterID,
								   InstituteAlias = oc.Cours.AdmissionOpenProgram.Program.Institute.InstituteAlias,
								   DepartmentAlias = oc.Cours.AdmissionOpenProgram.Program.Department.DepartmentAlias,
								   ProgramAlias = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
								   SemesterNoEnum = (SemesterNos)oc.SemesterNo,
								   SectionEnum = (Sections)oc.Section,
								   ShiftEnum = (Shifts)oc.Shift,
								   Title = oc.Cours.Title,
								   UserTypeEnum = (UserTypes)ulh.UserType,
								   FacultyMemberName = ulh.FacultyMember.Name,
								   FacultyMemberEmail = ulh.FacultyMember.Email,
								   StaffMemberName = ulh.User.Name,
								   StaffMemberEmail = ulh.User.Email,
							   }).ToList();

				var messageHtml = $@"<p>Dear All,</p>
<p>It is to bring in your kind notice through system-generated email that some changes have been made in exams record. For details, please see the following activity log, however, detailed information can be obtained through CMS using “Change History” option in marks sheet.</p>";
				var emailBody = PrepareChanges(changes, messageHtml);
				List<(string Email, string DisplayName)> toList;
				var managementEmailRecipientEnum = managementEmailRecipient.GetEnum<MarksSheetChanx.ManagementEmailRecipients>();
				switch (managementEmailRecipientEnum)
				{
					case MarksSheetChanx.ManagementEmailRecipients.BeforeSubmitToUniversity:
						toList = new[] { directorCampus, deputyDirectorExams, deputyDirectorAcademics, assistantDirectorExam, dean, hod }.Where(c => c != null).Select(c => (c.Name, c.Email)).ToList();
						break;
					case MarksSheetChanx.ManagementEmailRecipients.AfterSubmitToUniversity:
						toList = new[] { directorExams, directorCampus, deputyDirectorExams, deputyDirectorAcademics, assistantDirectorExam, dean, hod }.Where(c => c != null).Select(c => (c.Name, c.Email)).ToList();
						break;
					default:
						throw new NotImplementedEnumException(managementEmailRecipientEnum);
				}
				var email = aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.MarksSheetChangesToManagement, "Exam Marks Changes", emailBody, true, Email.Priorities.Normal, DateTime.Now, null, UserLoginHistory.SystemUserLoginHistoryID, toList);
				changes.ForEach(c => c.MakMarksSheetChange.ManagementEmailID = email.EmailID);
				aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
			}
		}

		private sealed class ProjectThesisInternshipChangeInfo
		{
			public ProjectThesisInternshipChanx ProjectThesisInternshipChange { get; set; }
			public string InstituteAlias { get; set; }
			public string DepartmentAlias { get; set; }
			public string ProgramAlias { get; set; }
			public short IntakeSemesterID { get; set; }
			public string IntakeSemester => this.IntakeSemesterID.ToSemesterString();
			public Guid RecordType { get; set; }
			public string RecordTypeFullName => this.RecordType.GetFullName();
			public short SemesterID { get; set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public int? RegistrationNo { get; set; }
			public string Enrollment { get; set; }
			public UserTypes UserTypeEnum { get; set; }
			public string StaffMemberName { get; set; }
			public string StaffMemberEmail { get; set; }
			public string ChangedBy
			{
				get
				{
					switch (this.UserTypeEnum)
					{
						case UserTypes.Staff:
							return $"{this.StaffMemberName} ({this.UserTypeEnum.ToFullName()}) <{this.StaffMemberEmail}>";
						default:
							throw new NotImplementedEnumException(this.UserTypeEnum);
					}
				}
			}
		}

		private static string PrepareChanges(List<ProjectThesisInternshipChangeInfo> changes, string messageHtml)
		{
			var html = new StringBuilder();
			html.Append("<html>");
			{
				html.Append("\n<head>");
				{
					html.Append(@"<style type=""text/css"">
body {
font-family: ""Helvetica Neue"", Helvetica, Arial, sans-serif;
font-size: 13px;
}
table {
border: 1px solid #ddd;
border-collapse: collapse;
border-spacing: 0;
color: rgb(51, 51, 51);
}
th {
border: 1px solid #ddd;
padding: 4px;
text-align: left;
font-weight: bold;
}
td {
border: 1px solid #ddd;
padding: 4px;
text-align: left;
}
</style>");
				}
				html.Append("\n</head>");
				html.Append("\n<body>");
				{
					html.Append(messageHtml);
					html.Append("\n<table>");
					{
						html.Append("\n<thead>");
						{
							html.Append("\n<tr>");
							{
								html.AppendFormat($"<th>{"Semester".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Institute".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Department".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Program".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Intake Semester".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Registration No.".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Enrollment".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Course".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Change Type".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Date".HtmlEncode()}</th>");
								html.AppendFormat($"<th>{"Changed By".HtmlEncode()}</th>");
							}
							html.Append("\n</tr>");
						}
						html.Append("\n</thead>");
						html.Append("\n<tbody>");
						{
							changes.OrderBy(c => c.ProgramAlias).ThenBy(c => c.IntakeSemesterID).ThenBy(c => c.SemesterID).ThenBy(c => c.ProjectThesisInternshipChange.ActionDate)
								.ForEach(c =>
								{
									html.Append("\n<tr>");
									{
										html.AppendFormat($"<td>{c.Semester.HtmlEncode()}</td>");
										html.AppendFormat($"<td>{c.InstituteAlias.HtmlEncode()}</td>");
										html.AppendFormat($"<td>{c.DepartmentAlias.HtmlEncode()}</td>");
										html.AppendFormat($"<td>{c.ProgramAlias.HtmlEncode()}</td>");
										html.AppendFormat($"<td>{c.IntakeSemester.HtmlEncode()}</td>");
										html.AppendFormat($"<td>{(c.RegistrationNo?.ToString()).ToNAIfNullOrEmpty().HtmlEncode()}</td>");
										html.AppendFormat($"<td>{c.Enrollment.HtmlEncode()}</td>");
										html.AppendFormat($"<td>{c.RecordTypeFullName.HtmlEncode()}</td>");
										html.AppendFormat($"<td>{c.ProjectThesisInternshipChange.ActionTypeFullName.HtmlEncode()}</td>");
										html.AppendFormat($"<td>{c.ProjectThesisInternshipChange.ActionDate.ToString(CultureInfo.CurrentCulture).HtmlEncode()}</td>");
										html.AppendFormat($"<td>{c.ChangedBy.HtmlEncode()}</td>");
									}
									html.Append("\n</tr>");
								});
						}
						html.Append("\n</tbody>");
					}
					html.Append("\n</table>");
				}
				html.Append("\n</body>");
			}
			html.Append("\n</html>");
			return html.ToString();
		}

		private static void SendProjectThesisInternshipChangesToManagement(int instituteID, int? departmentID, Guid managementEmailRecipient, CancellationToken cancellationToken)
		{
			cancellationToken.ThrowIfCancellationRequested();
			using (var aspireContext = new AspireContext(true))
			{
				var designation = Contact.Designations.DirectorExams.GetGuid();
				var directorExams = aspireContext.Contacts
					.Where(c => c.Designation == designation)
					.Select(c => new { c.Name, c.Email }).SingleOrDefault();

				designation = Contact.Designations.DirectorCampus.GetGuid();
				var directorCampus = aspireContext.Contacts
					.Where(c => c.InstituteID == instituteID && c.Designation == designation)
					.Select(c => new { c.Name, c.Email }).SingleOrDefault();

				designation = Contact.Designations.DeputyDirectorExams.GetGuid();
				var deputyDirectorExams = aspireContext.Contacts
					.Where(c => c.InstituteID == instituteID && c.Designation == designation)
					.Select(c => new { c.Name, c.Email }).SingleOrDefault();

				designation = Contact.Designations.AssistantDirectorExam.GetGuid();
				var assistantDirectorExam = aspireContext.Contacts
					.Where(c => c.InstituteID == instituteID && c.Designation == designation)
					.Select(c => new { c.Name, c.Email }).SingleOrDefault();

				designation = Contact.Designations.DeputyDirectorAcademics.GetGuid();
				var deputyDirectorAcademics = aspireContext.Contacts
					.Where(c => c.InstituteID == instituteID && c.Designation == designation)
					.Select(c => new { c.Name, c.Email }).SingleOrDefault();

				designation = Contact.Designations.Dean.GetGuid();
				var dean = aspireContext.Contacts
					.Where(c => c.InstituteID == instituteID && c.DepartmentID == departmentID && c.Designation == designation)
					.Select(c => new { c.Name, c.Email }).SingleOrDefault();

				designation = Contact.Designations.HeadOfDepartment.GetGuid();
				var hod = aspireContext.Contacts
					.Where(c => c.InstituteID == instituteID && c.DepartmentID == departmentID && c.Designation == designation)
					.Select(c => new { c.Name, c.Email }).SingleOrDefault();

				var changes = (from ptic in aspireContext.ProjectThesisInternshipChanges
							   join pti in aspireContext.ProjectThesisInternships on ptic.ProjectThesisInternshipID equals pti.ProjectThesisInternshipID
							   join s in aspireContext.Students on ptic.StudentID equals s.StudentID
							   join ulh in aspireContext.UserLoginHistories on ptic.LoginSessionGuid equals ulh.LoginSessionGuid
							   where ptic.ManagementEmailRecipient == managementEmailRecipient && ptic.ManagementEmailID == null
																&& s.AdmissionOpenProgram.Program.InstituteID == instituteID
																&& s.AdmissionOpenProgram.Program.DepartmentID == departmentID
							   select new ProjectThesisInternshipChangeInfo
							   {
								   ProjectThesisInternshipChange = ptic,
								   InstituteAlias = s.AdmissionOpenProgram.Program.Institute.InstituteAlias,
								   DepartmentAlias = s.AdmissionOpenProgram.Program.Department.DepartmentAlias,
								   ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
								   UserTypeEnum = (UserTypes)ulh.UserType,
								   StaffMemberName = ulh.User.Name,
								   StaffMemberEmail = ulh.User.Email,
								   IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
								   Enrollment = s.Enrollment,
								   RegistrationNo = s.RegistrationNo,
								   RecordType = pti.RecordType,
								   SemesterID = pti.SemesterID
							   }).ToList();

				var messageHtml = $@"<p>Dear All,</p>
<p>It is to bring in your kind notice through system-generated email that some changes have been made in exams record. For details, please see the following activity log, however, detailed information can be obtained through CMS using “Change History” option.</p>";
				var emailBody = PrepareChanges(changes, messageHtml);
				List<(string Email, string DisplayName)> toList;
				var managementEmailRecipientEnum = managementEmailRecipient.GetEnum<ProjectThesisInternshipChanx.ManagementEmailRecipients>();
				switch (managementEmailRecipientEnum)
				{
					case ProjectThesisInternshipChanx.ManagementEmailRecipients.AfterSubmitToHOD:
					case ProjectThesisInternshipChanx.ManagementEmailRecipients.AfterSubmitToCampus:
						toList = new[] { directorCampus, deputyDirectorExams, deputyDirectorAcademics, assistantDirectorExam, dean, hod }.Where(c => c != null).Select(c => (c.Name, c.Email)).ToList();
						break;
					case ProjectThesisInternshipChanx.ManagementEmailRecipients.AfterSubmitToUniversity:
						toList = new[] { directorExams, directorCampus, deputyDirectorExams, deputyDirectorAcademics, assistantDirectorExam, dean, hod }.Where(c => c != null).Select(c => (c.Name, c.Email)).ToList();
						break;
					default:
						throw new NotImplementedEnumException(managementEmailRecipientEnum);
				}
				var email = aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.ProjectThesisInternshipMarksChangesToManagement, "Exam Marks Changes", emailBody, true, Email.Priorities.Normal, DateTime.Now, null, UserLoginHistory.SystemUserLoginHistoryID, toList);
				changes.ForEach(c => c.ProjectThesisInternshipChange.ManagementEmailID = email.EmailID);
				aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
			}
		}
	}
}