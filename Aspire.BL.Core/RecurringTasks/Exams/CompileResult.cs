﻿using Aspire.BL.Core.Exams.Common.Transcripts;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Aspire.BL.Core.RecurringTasks.Exams
{
	public sealed class CompileResult : BaseTask
	{
		protected override Guid TaskGuid => new Guid("4CF233ED-D558-4E31-AF8D-41E06B45AA5C");

#if DEBUG
		protected override bool Enabled => false;
#else
		protected override bool Enabled => true;
#endif

		protected override TimeSpan PollingTimeSpan => TimeSpan.FromMinutes(1);

		protected override TimeSpan RetryTimeSpan => TimeSpan.FromMinutes(5);

		protected override DateTime RunOn => DateTime.Now.TruncateDay();

		protected override DateTime NextRunOn => this.RunOn.AddDays(1);

		protected override void PerformJob(CancellationToken cancellationToken)
		{
			List<int> studentIDs;
			short fromSemesterID = 20163;
			short toSemesterID = 20192;
			cancellationToken.ThrowIfCancellationRequested();
			using (var aspireContext = new AspireContext())
			{
				studentIDs = aspireContext.Students
					.Where(s => s.AdmissionOpenProgram.SemesterID >= fromSemesterID && s.StudentSemesters.Any(ss => ss.SemesterID <= toSemesterID))
					.Select(s => s.StudentID).ToList();
			}
			cancellationToken.ThrowIfCancellationRequested();
			studentIDs.ForEach(studentID => CompileIndividual(toSemesterID, studentID, cancellationToken));
		}

		private static void CompileIndividual(short toSemesterID, int studentID, CancellationToken cancellationToken)
		{
			cancellationToken.ThrowIfCancellationRequested();
			using (var aspireContext = new AspireContext(true))
			{
				aspireContext.CompileResult(toSemesterID, studentID, UserLoginHistory.SystemUserLoginHistoryID);
				aspireContext.CommitTransaction();
			}
		}
	}
}