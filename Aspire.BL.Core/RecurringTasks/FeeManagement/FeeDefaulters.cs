﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Aspire.BL.Core.RecurringTasks.FeeManagement
{
	public sealed class FeeDefaulters : BaseTask
	{
		protected override Guid TaskGuid => new Guid("1D860430-091C-46DD-9E5D-8466D2F9F3DA");

#if DEBUG
		protected override bool Enabled => false;
#else
		protected override bool Enabled => true;
#endif

		protected override TimeSpan PollingTimeSpan => TimeSpan.FromMinutes(1);

		protected override TimeSpan RetryTimeSpan => TimeSpan.FromMinutes(5);

		protected override DateTime RunOn => DateTime.Now.TruncateDay();

		protected override DateTime NextRunOn => this.RunOn.AddDays(1);

		protected override void PerformJob(CancellationToken cancellationToken)
		{
			List<FeeDefaulterJobSemester> feeDefaulterJobSemesters;
			using (var aspireContext = new AspireContext())
			{
				feeDefaulterJobSemesters = aspireContext.FeeDefaulterJobSemesters.Where(fdjs => fdjs.Status == (byte)FeeDefaulterJobSemester.Statuses.Active).ToList();
			}
			feeDefaulterJobSemesters.ForEach(feeDefaulterJobSemester =>
			{
				ProcessFeeDefaulters(feeDefaulterJobSemester, cancellationToken);
			});
		}

		private sealed class Student
		{
			public int StudentID { get; internal set; }
			public Model.Entities.Student.Statuses? StatusEnum { get; internal set; }
		}

		private static void ProcessFeeDefaulters(FeeDefaulterJobSemester feeDefaulterJobSemester, CancellationToken cancellationToken)
		{
			cancellationToken.ThrowIfCancellationRequested();
			List<Student> students;
			using (var aspireContext = new AspireContext())
			{
				students = aspireContext.RegisteredCourses
					.Where(rc => rc.DeletedDate == null)
					.Where(rc => rc.OfferedCours.SemesterID == feeDefaulterJobSemester.SemesterID)
					.Where(rc => rc.OfferedCours.Cours.AdmissionOpenProgram.Program.InstituteID == feeDefaulterJobSemester.InstituteID)
					.Where(rc => rc.Student.AdmissionOpenProgram.SemesterID != feeDefaulterJobSemester.SemesterID)
					.Select(rc => new Student
					{
						StudentID = rc.StudentID,
						StatusEnum = (Model.Entities.Student.Statuses?)rc.Student.Status,
					})
					.Distinct()
					.ToList();
			}

			foreach (var student in students)
				switch (student.StatusEnum)
				{
					case Model.Entities.Student.Statuses.AdmissionCancelled:
					case Model.Entities.Student.Statuses.Dropped:
					case Model.Entities.Student.Statuses.Expelled:
					case Model.Entities.Student.Statuses.Graduated:
					case Model.Entities.Student.Statuses.Left:
					case Model.Entities.Student.Statuses.ProgramChanged:
					case Model.Entities.Student.Statuses.Rusticated:
					case Model.Entities.Student.Statuses.TransferredToOtherCampus:
						break;
					case Model.Entities.Student.Statuses.Blocked:
					case Model.Entities.Student.Statuses.Deferred:
					case null:
						cancellationToken.ThrowIfCancellationRequested();
						Core.FeeManagement.Common.FeeDefaulters.ProcessIndividualFeeDefaulterAndSendEmails(student.StudentID);
						break;
					default:
						throw new NotImplementedEnumException(student.StatusEnum);
				}
		}
	}
}