﻿using Aspire.BL.Core.Logs.Common;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Linq;
using System.Threading;

namespace Aspire.BL.Core.RecurringTasks
{
	public abstract class BaseTask
	{
		protected string TaskName => this.GetType().Name;
		protected abstract Guid TaskGuid { get; }
		protected abstract bool Enabled { get; }
		protected abstract TimeSpan PollingTimeSpan { get; }
		protected abstract TimeSpan RetryTimeSpan { get; }
		protected abstract DateTime RunOn { get; }
		protected abstract DateTime NextRunOn { get; }

		protected abstract void PerformJob(CancellationToken cancellationToken);

		private RecurringTask GetLastRecurringTask(RecurringTask.Statuses statusEnum)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.RecurringTasks
					.Where(rt => rt.TaskGUID == this.TaskGuid && rt.Status == (byte)statusEnum)
					.OrderByDescending(rt => rt.StartTime)
					.FirstOrDefault();
			}
		}

		private void LogAttempt(DateTime startTime, DateTime finishTime, RecurringTask.Statuses statusEnum)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.RecurringTasks.Add(new RecurringTask
				{
					TaskGUID = this.TaskGuid,
					StartTime = startTime,
					FinishTime = finishTime,
					StatusEnum = statusEnum
				});
				aspireContext.SaveChangesAsSystemUser();
			}
		}

		private enum ExecutionStatuses
		{
			NotExecuted,
			Executed,
			AlreadyExecuted,
			Failed,
			Disabled
		}

		private void StartJob(CancellationToken cancellationToken)
		{
			ExecutionStatuses executionStatus = ExecutionStatuses.NotExecuted;
			while (true)
			{
				if (this.Enabled)
				{
					cancellationToken.ThrowIfCancellationRequested();
					if (this.RunOn > DateTime.Now)
						executionStatus = ExecutionStatuses.NotExecuted;
					else
					{
						var recurringTask = this.GetLastRecurringTask(RecurringTask.Statuses.Success);
						if (recurringTask != null && recurringTask.StartTime >= this.RunOn)
							executionStatus = ExecutionStatuses.AlreadyExecuted;
						else
						{
							var startTime = DateTime.Now;
							RecurringTask.Statuses statusEnum = RecurringTask.Statuses.Failed;
							try
							{
								this.PerformJob(cancellationToken);
								statusEnum = RecurringTask.Statuses.Success;
								executionStatus = ExecutionStatuses.Executed;
							}
							catch (OperationCanceledException)
							{
								statusEnum = RecurringTask.Statuses.CancelledSafely;
								throw;
							}
							catch (Exception exc)
							{
								statusEnum = RecurringTask.Statuses.Failed;
								ErrorHandler.LogException(null, exc, -1, this.GetType().FullName, null, null);
							}
							finally
							{
								this.LogAttempt(startTime, DateTime.Now, statusEnum);
							}
						}
					}
				}
				else
					executionStatus = ExecutionStatuses.Disabled;

				cancellationToken.ThrowIfCancellationRequested();
				switch (executionStatus)
				{
					case ExecutionStatuses.Disabled:
					case ExecutionStatuses.NotExecuted:
						Thread.Sleep(this.PollingTimeSpan);
						break;
					case ExecutionStatuses.Executed:
					case ExecutionStatuses.AlreadyExecuted:
						var waitTimeInSecondsUntilNextRun = (this.NextRunOn - DateTime.Now).TotalSeconds;
						if (waitTimeInSecondsUntilNextRun > 0)
							Thread.Sleep(TimeSpan.FromSeconds(waitTimeInSecondsUntilNextRun));
						break;
					case ExecutionStatuses.Failed:
						Thread.Sleep(this.RetryTimeSpan);
						break;
					default:
						throw new NotImplementedEnumException(executionStatus);
				}
			}
		}

		public static Action<CancellationToken>[] GetJobs()
		{
			return typeof(BaseTask).Assembly.GetTypes().Where(t => t.IsClass && !t.IsAbstract && t.IsSubclassOf(typeof(BaseTask)))
				.Select(t => (BaseTask)Activator.CreateInstance(t))
				.Select(t => new Action<CancellationToken>(t.StartJob)).ToArray();
		}
	}
}
