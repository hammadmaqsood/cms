﻿using Aspire.BL.Core.Logs.Common;
using Aspire.Lib.Helpers;
using Aspire.Lib.Helpers.Email;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;

namespace Aspire.BL.Core.RecurringTasks.SendEmails
{
	public sealed class AttendanceChangeLogs : BaseTask
	{
		public override Guid TaskGuid => new Guid("ACFCC571-B90D-4603-9950-ADBE9A7E16FB");

#if DEBUG
		public override bool Enabled => false;
#else
		public override bool Enabled => true;
#endif

		private static DateTime StartDateTime => new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 18, 0, 0);

		protected override bool PerformJob(CancellationToken cancellationToken)
		{
			if (this.Enabled == false)
				return false;
			if (StartDateTime > DateTime.Now)
				return true;

			var recurringTask = this.GetLastRecurringTask(RecurringTask.Statuses.Success);
			if (recurringTask != null && recurringTask.StartTime >= StartDateTime)
				return true;

			var startTime = DateTime.Now;
			RecurringTask.Statuses statusEnum;
			try
			{
				using (var aspireContext = new AspireContext())
				{
					aspireContext.Database.CommandTimeout = (int?)TimeSpan.FromMinutes(20).TotalSeconds;
					var instituteID = 1;
					var adminULoginHistories = aspireContext.UserLoginHistories
						.Where(h => h.UserRole.UserType == UserTypeConstants.MasterAdmin
									|| (h.UserRole.UserType == UserTypeConstants.Admin && h.UserRole.UserRoleInstitutes.Any(ri => ri.InstituteID == instituteID)))
						.Select(h => new
						{
							h.UserLoginHistoryID,
							h.UserName,
							h.UserType,
						});
					var lastMonth = DateTime.Today.AddMonths(-3);
					var tableName = nameof(aspireContext.OfferedCourseAttendanceDetails);
					var columnName = nameof(OfferedCourseAttendanceDetail.Hours);
					var logDetails = aspireContext.DMLLogDetails
						.Where(ld => ld.DMLLog.TransactionDate >= lastMonth)
						.Where(ld => ld.DMLLog.TableName == tableName)
						.Where(ld => ld.ColumnName == columnName)
						.Select(ld => new
						{
							ld.DMLLog.UserLoginHistoryID,
							ld.DMLLog.TransactionDate,
							ld.DMLLog.TransactionType,
							ld.DMLLog.TableName,
							ld.DMLLog.PrimaryKey,
							ld.ColumnName,
							ld.OldValue,
							ld.NewValue
						});

					var logs = from ld in logDetails
							   join h in adminULoginHistories on ld.UserLoginHistoryID equals h.UserLoginHistoryID
							   select new
							   {
								   h.UserLoginHistoryID,
								   h.UserName,
								   h.UserType,
								   ld.TransactionDate,
								   ld.TransactionType,
								   ld.TableName,
								   ld.PrimaryKey,
								   ld.ColumnName,
								   ld.OldValue,
								   ld.NewValue
							   };

					var students = aspireContext.OfferedCourseAttendanceDetails
						.Where(ocad => ocad.RegisteredCours.Student.AdmissionOpenProgram.Program.InstituteID == instituteID)
						.Select(ocad => new
						{
							ocad.OfferedCourseAttendanceDetailID,
							ocad.RegisteredCours.Student.Enrollment,
							ocad.RegisteredCours.Student.Name,
							ocad.RegisteredCours.OfferedCours.Cours.Title,
						});

					var resultQuery = from l in logs
									  join s in students on l.PrimaryKey equals s.OfferedCourseAttendanceDetailID into grp
									  from g in grp.DefaultIfEmpty()
									  orderby l.TransactionDate descending
									  select new
									  {
										  l.UserName,
										  l.UserType,
										  l.TransactionDate,
										  l.TransactionType,
										  l.TableName,
										  l.PrimaryKey,
										  l.ColumnName,
										  l.OldValue,
										  l.NewValue,
										  g.Enrollment,
										  g.Name,
										  g.Title,
									  };
					var result = resultQuery.OrderByDescending(r => r.TransactionDate).ToList();
					if (result.Any())
					{
						using (var excelMS = new MemoryStream())
						{
							result.ConvertToExcel(excelMS, "Attendance Change Log", new List<ExcelHelper.ExcelColumns>
							{
								new ExcelHelper.ExcelColumns {DisplayIndex = 0, HeaderText = "UserName", PropertyName = "UserName"},
								new ExcelHelper.ExcelColumns {DisplayIndex = 1, HeaderText = "TransactionDate", PropertyName = "TransactionDate"},
								new ExcelHelper.ExcelColumns {DisplayIndex = 2, HeaderText = "Enrollment", PropertyName = "Enrollment"},
								new ExcelHelper.ExcelColumns {DisplayIndex = 3, HeaderText = "Name", PropertyName = "Name"},
								new ExcelHelper.ExcelColumns {DisplayIndex = 4, HeaderText = "Title", PropertyName = "Title"},
								new ExcelHelper.ExcelColumns {DisplayIndex = 5, HeaderText = "Old Value", PropertyName = "OldValue"},
								new ExcelHelper.ExcelColumns {DisplayIndex = 6, HeaderText = "New Value", PropertyName = "NewValue"},
							});
							var subject = "Attendance Change Log";
							var body = $"Data Range: {lastMonth:D} to {DateTime.Today:D}.";
							List<string> recipientsTo;
							List<string> cc = null;
							List<string> bcc = null;
							var replyToList = new List<string> { "orangzeb@bahria.edu.pk" };
							if (this.LiveSite == false)
								recipientsTo = new[] { "orangzeb@bahria.edu.pk" }.ToList();
							else
							{
								recipientsTo = new[] { "administrator@bui.edu.pk" }.ToList();
								cc = new[]
								{
									"orangzeb@bahria.edu.pk"
								}.ToList();
							}
							var attachments = new List<Tuple<string, byte[]>>
							{
								new Tuple<string, byte[]>($"Attendance Log {lastMonth:yyyyMMdd}-{DateTime.Now:yyyyMMdd}.xlsx", excelMS.ToArray()),
							};
							Common.EmailEngine.QueueEmail(Priority.Lowest, DateTime.Now.AddDays(1), EmailTypes.AttendanceChangeLogs, recipientsTo, subject, body, false, replyToList, cc, bcc, attachments);
						}
					}
					statusEnum = RecurringTask.Statuses.Success;
				}
			}
			catch (Exception exc)
			{
				statusEnum = RecurringTask.Statuses.Failed;
				ErrorHandler.LogException(null, exc, -1, this.GetType().FullName, null, System.Net.IPAddress.Loopback.ToString());
			}
			this.LogAttempt(startTime, DateTime.Now, statusEnum);
			return true;
		}
	}
}
