﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Aspire.BL.Core.RecurringTasks.FeeManagement
{
	public sealed class LoginExpiry : BaseTask
	{
		protected override Guid TaskGuid => new Guid("A5A17006-A256-46C8-A7B6-86475FBD4FFD");

#if DEBUG
		protected override bool Enabled => true;
#else
		protected override bool Enabled => true;
#endif

		protected override TimeSpan PollingTimeSpan => TimeSpan.FromMinutes(1);

		protected override TimeSpan RetryTimeSpan => TimeSpan.FromMinutes(5);

		protected override DateTime RunOn => DateTime.Now.TruncateHour();

		protected override DateTime NextRunOn => this.RunOn.AddHours(1);

		protected override void PerformJob(CancellationToken cancellationToken)
		{
			while (true)
			{
				using (var aspireContext = new AspireContext(true))
				{
					var loginDate = DateTime.Now.AddHours(-8);
					var expiredUserLoginHistories = aspireContext.UserLoginHistories
						.Where(h => h.LoginSessionGuid != UserLoginHistory.SystemLoginSessionGuid)
						.Where(h => h.LoginStatus == (byte)UserLoginHistory.LoginStatuses.Success && h.LogOffDate == null)
						.Where(h => h.LoginDate < loginDate)
						.Take(1000)
						.ToList();
					if (!expiredUserLoginHistories.Any())
						return;
					expiredUserLoginHistories.ForEach(h =>
					{
						h.LogOffDate = DateTime.Now;
						h.LogOffReasonEnum = UserLoginHistory.LogOffReasons.SessionTimeout;
					});
					aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
				}
			}
		}
	}
}