﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Administration.Admin
{
	public static class Departments
	{
		private static Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Administration.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Administration.ManageDepartments, UserGroupPermission.PermissionValues.Allowed);
		}

		public static List<CustomListItem> GetDepartments(int? instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var departments = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Departments).Distinct();
				if (instituteID != null)
				{
					departments = departments.Where(d => d.InstituteID == instituteID);
					return departments.Select(d => new CustomListItem
					{
						ID = d.DepartmentID,
						Text = d.DepartmentAlias
					}).OrderBy(d => d.Text).ToList();
				}
				return departments.Select(d => new CustomListItem
				{
					ID = d.DepartmentID,
					Text = d.Institute.InstituteAlias + "/" + d.DepartmentAlias
				}).OrderBy(d => d.Text).ToList();
			}
		}

		public sealed class Department
		{
			internal Department()
			{
			}

			public string DepartmentAlias { get; internal set; }
			public int DepartmentID { get; internal set; }
			public string DepartmentName { get; internal set; }
			public string DepartmentShortName { get; internal set; }
			public string InstituteAlias { get; internal set; }
			public string FacultyAlias { get; internal set; }
		}

		public static List<Department> GetDepartments(int? instituteID, Guid? facultyID, string sortExpression, SortDirection sortDirection, int pageIndex, int pageSize, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var departments = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Departments).Distinct();
				if (instituteID != null)
					departments = departments.Where(d => d.InstituteID == instituteID);
				if (facultyID != null)
					departments = departments.Where(d => d.FacultyID == facultyID);
				virtualItemCount = departments.Count();
				var finalQuery = departments.Select(d => new Department
				{
					DepartmentID = d.DepartmentID,
					InstituteAlias = d.Institute.InstituteAlias,
					FacultyAlias = d.Faculty.FacultyAlias,
					DepartmentName = d.DepartmentName,
					DepartmentShortName = d.DepartmentShortName,
					DepartmentAlias = d.DepartmentAlias
				});
				switch (sortExpression)
				{
					case nameof(Department.InstituteAlias):
						finalQuery = finalQuery.OrderBy(sortDirection, d => d.InstituteAlias);
						break;
					case nameof(Department.FacultyAlias):
						finalQuery = finalQuery.OrderBy(sortDirection, d => d.FacultyAlias);
						break;
					case nameof(Department.DepartmentName):
						finalQuery = finalQuery.OrderBy(sortDirection, d => d.DepartmentName);
						break;
					case nameof(Department.DepartmentShortName):
						finalQuery = finalQuery.OrderBy(sortDirection, d => d.DepartmentShortName);
						break;
					case nameof(Department.DepartmentAlias):
						finalQuery = finalQuery.OrderBy(sortDirection, d => d.DepartmentAlias);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return finalQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList();
			}
		}

		public sealed class AddDepartmentResult
		{
			internal AddDepartmentResult()
			{
			}

			public enum Statuses
			{
				NameAlreadyExists,
				ShortNameAlreadyExists,
				AliasAlreadyExists,
				Success
			}

			public Statuses Status { get; internal set; }
			public int? DepartmentID { get; internal set; }
		}

		public static AddDepartmentResult AddDepartment(int instituteID, Guid facultyID, string departmentName, string departmentShortName, string departmentAlias, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var department = new Model.Entities.Department
				{
					InstituteID = instituteID,
					FacultyID = aspireContext.Faculties.Where(f => f.InstituteID == instituteID && f.FacultyID == facultyID).Select(f => new { f.FacultyID }).Single().FacultyID,
					DepartmentAlias = departmentAlias,
					DepartmentName = departmentName,
					DepartmentShortName = departmentShortName,
					StatusEnum = Model.Entities.Department.Statuses.Active
				}.Validate();
				if (aspireContext.Departments.Any(d => d.DepartmentName == department.DepartmentName && d.InstituteID == department.InstituteID))
					return new AddDepartmentResult { Status = AddDepartmentResult.Statuses.NameAlreadyExists };
				if (aspireContext.Departments.Any(d => d.DepartmentShortName == department.DepartmentShortName && d.InstituteID == department.InstituteID))
					return new AddDepartmentResult { Status = AddDepartmentResult.Statuses.ShortNameAlreadyExists };
				if (aspireContext.Departments.Any(d => d.DepartmentAlias == department.DepartmentAlias && d.InstituteID == department.InstituteID))
					return new AddDepartmentResult { Status = AddDepartmentResult.Statuses.AliasAlreadyExists };
				aspireContext.Departments.Add(department);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return new AddDepartmentResult { Status = AddDepartmentResult.Statuses.Success, DepartmentID = department.DepartmentID };
			}
		}

		public static Model.Entities.Department GetDepartment(int departmentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Departments).SingleOrDefault(d => d.DepartmentID == departmentID);
			}
		}

		public enum UpdateDepartmentStatuses
		{
			NoRecordFound,
			NameAlreadyExists,
			ShortNameAlreadyExists,
			AliasAlreadyExists,
			Success
		}

		public static UpdateDepartmentStatuses UpdateDepartment(int departmentID, Guid facultyID, string departmentName, string departmentShortName, string departmentAlias, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var department = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Departments).SingleOrDefault(d => d.DepartmentID == departmentID);
				if (department == null)
					return UpdateDepartmentStatuses.NoRecordFound;

				department.FacultyID = aspireContext.Faculties.Where(f => f.InstituteID == department.InstituteID && f.FacultyID == facultyID).Select(f => new { f.FacultyID }).Single().FacultyID;
				department.DepartmentName = departmentName;
				department.DepartmentShortName = departmentShortName;
				department.DepartmentAlias = departmentAlias;
				department.Validate();
				if (aspireContext.Departments.Any(d => d.InstituteID == department.InstituteID && d.DepartmentID != department.DepartmentID && d.DepartmentName == department.DepartmentName))
					return UpdateDepartmentStatuses.NameAlreadyExists;
				if (aspireContext.Departments.Any(d => d.InstituteID == department.InstituteID && d.DepartmentID != department.DepartmentID && d.DepartmentShortName == department.DepartmentShortName))
					return UpdateDepartmentStatuses.ShortNameAlreadyExists;
				if (aspireContext.Departments.Any(d => d.InstituteID == department.InstituteID && d.DepartmentID != department.DepartmentID && d.DepartmentAlias == department.DepartmentAlias))
					return UpdateDepartmentStatuses.AliasAlreadyExists;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateDepartmentStatuses.Success;
			}
		}

		public enum DeleteDepartmentStatuses
		{
			NoRecordFound,
			ChildRecordExistsExamInvigilators,
			ChildRecordExistsPrograms,
			ChildRecordExistsFacultyMembers,
			ChildRecordExistsUserRoleModules,
			Success
		}

		public static DeleteDepartmentStatuses DeleteDepartment(int departmentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var department = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Departments).SingleOrDefault(d => d.DepartmentID == departmentID);
				if (department == null)
					return DeleteDepartmentStatuses.NoRecordFound;
				if (department.ExamInvigilators.Any())
					return DeleteDepartmentStatuses.ChildRecordExistsExamInvigilators;
				if (department.Programs.Any())
					return DeleteDepartmentStatuses.ChildRecordExistsPrograms;
				if (department.FacultyMembers.Any())
					return DeleteDepartmentStatuses.ChildRecordExistsFacultyMembers;
				if (department.UserRoleModules.Any())
					return DeleteDepartmentStatuses.ChildRecordExistsUserRoleModules;

				aspireContext.Departments.Remove(department);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteDepartmentStatuses.Success;
			}
		}
	}
}
