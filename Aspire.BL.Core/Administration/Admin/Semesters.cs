﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Administration.Admin
{
	public static class Semesters
	{
		private static Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Administration.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Administration.ManageSemesters, UserGroupPermission.PermissionValues.Allowed);
		}

		public enum AddSemesterStatuses
		{
			AlreadyExists,
			Success,
		}

		public static AddSemesterStatuses AddSemester(short semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				if (aspireContext.Semesters.Any(s => s.SemesterID == semesterID))
					return AddSemesterStatuses.AlreadyExists;
				var semester = new Semester
				{
					SemesterID = semesterID,
					Year = Semester.GetYear(semesterID),
					SemesterTypeEnum = Semester.GetSemesterType(semesterID),
					Visible = true,
				};
				aspireContext.Semesters.Add(semester);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddSemesterStatuses.Success;
			}
		}

		public enum AddOrUpdateInstituteSemesterStatuses
		{
			InvalidDates,
			Success,
		}

		public static AddOrUpdateInstituteSemesterStatuses AddOrUpdateInstituteSemester(short semesterID, int instituteID, DateTime classesStartDate, DateTime? classesEndDate, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandAdminActiveSessionAndInstitute(loginSessionGuid, instituteID);
				aspireContext.DemandPermissions(loginSessionGuid);
				var instituteSemester = aspireContext.InstituteSemesters.SingleOrDefault(i => i.InstituteID == instituteID && i.SemesterID == semesterID);
				if (instituteSemester == null)
					instituteSemester = aspireContext.InstituteSemesters.Add(new Model.Entities.InstituteSemester
					{
						InstituteSemesterID = Guid.NewGuid(),
						InstituteID = instituteID,
						SemesterID = semesterID
					});
				classesStartDate = classesStartDate.Date;
				classesEndDate = classesEndDate?.Date;
				if (classesEndDate != null && classesStartDate > classesEndDate)
					return AddOrUpdateInstituteSemesterStatuses.InvalidDates;

				instituteSemester.ClassesStartDate = classesStartDate;
				instituteSemester.ClassesEndDate = classesEndDate;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddOrUpdateInstituteSemesterStatuses.Success;
			}
		}

		public static Model.Entities.InstituteSemester GetInstituteSemester(short semesterID, int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandAdminActiveSessionAndInstitute(loginSessionGuid, instituteID);
				return aspireContext.InstituteSemesters.SingleOrDefault(i => i.InstituteID == instituteID && i.SemesterID == semesterID);
			}
		}

		public sealed class InstituteSemester
		{
			internal InstituteSemester() { }
			public short SemesterID { get; set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public int InstituteID { get; set; }
			public string InstituteAlias { get; set; }
			public DateTime? ClassesStartDate { get; set; }
			public DateTime? ClassesEndDate { get; set; }
		}

		public static List<InstituteSemester> GetSemesters(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var institutes = aspireContext.Institutes.Select(i => new { i.InstituteID, i.InstituteAlias }).ToList();
				var semesters = aspireContext.Semesters.ToList();
				var instituteSemesters = aspireContext.InstituteSemesters.ToList();

				var instituteJSemesters = from s in semesters
										  from i in institutes
										  select new { s.SemesterID, i.InstituteID, i.InstituteAlias };
				var result = from ijs in instituteJSemesters
							 join iss in instituteSemesters on new { ijs.SemesterID, ijs.InstituteID } equals new { iss.SemesterID, iss.InstituteID } into grp
							 from iss in grp.DefaultIfEmpty()
							 select new InstituteSemester
							 {
								 InstituteID = ijs.InstituteID,
								 SemesterID = ijs.SemesterID,
								 InstituteAlias = ijs.InstituteAlias,
								 ClassesStartDate = iss?.ClassesStartDate,
								 ClassesEndDate = iss?.ClassesEndDate
							 };

				return result.OrderByDescending(s => s.SemesterID).ThenBy(s => s.InstituteID).ToList();
			}
		}

		public enum DeleteSemesterStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteSemesterStatuses DeleteSemester(short semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var semester = aspireContext.Semesters.SingleOrDefault(s => s.SemesterID == semesterID);
				if (semester == null)
					return DeleteSemesterStatuses.NoRecordFound;
				var query = aspireContext.Semesters.First(s => s.SemesterID == semesterID);
				if (query.InstituteSemesters.Any())
					return DeleteSemesterStatuses.ChildRecordExists;
				if (query.AdmissionOpenPrograms.Any())
					return DeleteSemesterStatuses.ChildRecordExists;
				if (query.Candidates.Any())
					return DeleteSemesterStatuses.ChildRecordExists;
				if (query.CandidateTemps.Any())
					return DeleteSemesterStatuses.ChildRecordExists;
				if (query.ClassAttendanceSettings.Any())
					return DeleteSemesterStatuses.ChildRecordExists;
				if (query.CourseRegistrationSettings.Any())
					return DeleteSemesterStatuses.ChildRecordExists;
				if (query.ExamConducts.Any())
					return DeleteSemesterStatuses.ChildRecordExists;
				if (query.ExamMarksSettings.Any())
					return DeleteSemesterStatuses.ChildRecordExists;
				if (query.OfferedCourses.Any())
					return DeleteSemesterStatuses.ChildRecordExists;
				if (query.StudentSemesters.Any())
					return DeleteSemesterStatuses.ChildRecordExists;
				if (query.SurveyConducts.Any())
					return DeleteSemesterStatuses.ChildRecordExists;
				aspireContext.Semesters.Remove(semester);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteSemesterStatuses.Success;
			}
		}
	}
}