﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Administration.Admin
{
	public static class Contacts
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Administration.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int instituteID, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID, AdminPermissions.Administration.ManageContacts, UserGroupPermission.PermissionValues.Allowed);
		}

		public static List<Contact> GetContacts(int? instituteID, Guid? facultyID, int? departmentID, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var contacts = !loginHistory.IsMasterAdmin
					? aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Contacts)
					: aspireContext.Contacts.AsQueryable();

				contacts = contacts
					.Where(c => instituteID == null || c.InstituteID == instituteID.Value)
					.Where(c => facultyID == null || c.FacultyID == facultyID.Value)
					.Where(c => departmentID == null || c.DepartmentID == departmentID.Value);
				virtualItemCount = contacts.Count();
				switch (sortExpression)
				{
					case nameof(Contact.Institute.InstituteAlias):
						contacts = contacts.OrderBy(sortDirection, c => c.Institute.InstituteAlias);
						break;
					case nameof(Contact.Department.DepartmentAlias):
						contacts = contacts.OrderBy(sortDirection, c => c.Department.DepartmentAlias);
						break;
					case nameof(Contact.Designation):
						contacts = contacts.OrderBy(sortDirection, c => c.Designation);
						break;
					case nameof(Contact.Name):
						contacts = contacts.OrderBy(sortDirection, c => c.Name);
						break;
					case nameof(Contact.Email):
						contacts = contacts.OrderBy(sortDirection, c => c.Email);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return contacts.Include(emp => emp.Institute).Include(c => c.Faculty).Include(c => c.Department).Skip(pageIndex * pageSize).Take(pageSize).ToList();
			}
		}

		public static Contact GetContact(Guid contactID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var contacts = !loginHistory.IsMasterAdmin
					? aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Contacts)
					: aspireContext.Contacts.AsQueryable();
				return contacts.SingleOrDefault(c => c.ContactID == contactID);
			}
		}

		public enum AddContactStatuses
		{
			Success,
		}

		public static AddContactStatuses AddContact(int? instituteID, Guid? facultyID, int? departmentID, Contact.Designations designationEnum, string name, string email, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				aspireContext.Contacts.Add(new Contact
				{
					ContactID = Guid.NewGuid(),
					InstituteID = instituteID,
					FacultyID = facultyID,
					DepartmentID = departmentID,
					Designation = designationEnum.GetGuid(),
					Name = name,
					Email = email
				}.Validate());

				if (instituteID == null)
				{
					var loginHistory = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				}
				else
				{
					var loginHistory = aspireContext.DemandPermissions(instituteID.Value, loginSessionGuid);
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				}
				return AddContactStatuses.Success;
			}
		}

		public enum UpdateContactStatuses
		{
			NoRecordFound,
			Success,
		}

		public static UpdateContactStatuses UpdateContact(Guid contactID, string name, string email, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var contact = aspireContext.Contacts.SingleOrDefault(c => c.ContactID == contactID);
				if (contact == null)
					return UpdateContactStatuses.NoRecordFound;
				contact.Name = name;
				contact.Email = email;
				contact.Validate();
				if (contact.InstituteID == null)
				{
					var loginHistory = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				}
				else
				{
					var loginHistory = aspireContext.DemandPermissions(contact.InstituteID.Value, loginSessionGuid);
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				}
				return UpdateContactStatuses.Success;
			}
		}

		public enum DeleteContactStatuses
		{
			NoRecordFound,
			Success,
		}

		public static DeleteContactStatuses DeleteContact(Guid contactID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var contact = aspireContext.Contacts.SingleOrDefault(c => c.ContactID == contactID);
				if (contact == null)
					return DeleteContactStatuses.NoRecordFound;
				aspireContext.Contacts.Remove(contact);
				if (contact.InstituteID == null)
				{
					var loginHistory = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				}
				else
				{
					var loginHistory = aspireContext.DemandPermissions(contact.InstituteID.Value, loginSessionGuid);
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				}
				return DeleteContactStatuses.Success;
			}
		}
	}
}