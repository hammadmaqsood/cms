﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Administration.Admin
{
	public static class Institutes
	{
		private static Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Administration.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Administration.ManageInstitutes, UserGroupPermission.PermissionValues.Allowed);
		}

		public static List<Institute> GetInstitutes(string sortExpression, SortDirection sortDirection, int pageIndex, int pageSize, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var institutes = aspireContext.GetAdminInstitutes(loginHistory);
				virtualItemCount = institutes.Count();
				switch (sortExpression)
				{
					case nameof(Institute.InstituteID):
						institutes = institutes.OrderBy(sortDirection, i => i.InstituteID);
						break;
					case nameof(Institute.InstituteName):
						institutes = institutes.OrderBy(sortDirection, i => i.InstituteName);
						break;
					case nameof(Institute.InstituteShortName):
						institutes = institutes.OrderBy(sortDirection, i => i.InstituteShortName);
						break;
					case nameof(Institute.InstituteAlias):
						institutes = institutes.OrderBy(sortDirection, i => i.InstituteAlias);
						break;
					case nameof(Institute.Phone):
						institutes = institutes.OrderBy(sortDirection, i => i.Phone);
						break;
					case nameof(Institute.Fax):
						institutes = institutes.OrderBy(sortDirection, i => i.Fax);
						break;
					case nameof(Institute.Website):
						institutes = institutes.OrderBy(sortDirection, i => i.Website);
						break;
					case nameof(Institute.InstituteCode):
						institutes = institutes.OrderBy(sortDirection, i => i.InstituteCode);
						break;
					case nameof(Institute.MailAddress):
						institutes = institutes.OrderBy(sortDirection, i => i.MailAddress);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}

				return institutes.Skip(pageIndex * pageSize).Take(pageSize).ToList();
			}
		}

		public sealed class AddInstituteResult
		{
			public enum Statuses
			{
				NameAlreadyExists,
				ShortNameAlreadyExists,
				AliasAlreadyExists,
				Success
			}

			public Statuses Status { get; internal set; }
			public int? InstituteID { get; internal set; }
		}

		public static AddInstituteResult AddInstitute(int instituteID, string instituteName, string instituteShortName, string instituteAlias, string instituteCode, string website, string address, string phone, string fax, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var institute = new Institute
				{
					InstituteID = instituteID,
					Fax = fax,
					InstituteAlias = instituteAlias,
					InstituteShortName = instituteShortName,
					InstituteCode = instituteCode,
					StatusEnum = Institute.Statuses.Active,
					Website = website,
					Phone = phone,
					MailAddress = address,
					InstituteName = instituteName
				}.Validate();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				if (aspireContext.Institutes.Any(i => i.InstituteName == institute.InstituteName))
					return new AddInstituteResult { Status = AddInstituteResult.Statuses.NameAlreadyExists };
				if (aspireContext.Institutes.Any(i => i.InstituteShortName == institute.InstituteShortName))
					return new AddInstituteResult { Status = AddInstituteResult.Statuses.ShortNameAlreadyExists };
				if (aspireContext.Institutes.Any(i => i.InstituteAlias == institute.InstituteAlias))
					return new AddInstituteResult { Status = AddInstituteResult.Statuses.AliasAlreadyExists };
				aspireContext.Institutes.Add(institute);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return new AddInstituteResult { InstituteID = institute.InstituteID, Status = AddInstituteResult.Statuses.Success };
			}
		}

		public static Institute GetInstitute(int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory).SingleOrDefault(i => i.InstituteID == instituteID);
			}
		}

		public enum UpdateInstituteStatuses
		{
			NoRecordFound,
			NameAlreadyExists,
			ShortNameAlreadyExists,
			AliasAlreadyExists,
			Success
		}

		public static UpdateInstituteStatuses UpdateInstitute(int instituteID, string instituteName, string instituteShortName, string instituteAlias, string instituteCode, string website, string address, string phone, string fax, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var institute = aspireContext.GetAdminInstitutes(loginHistory).SingleOrDefault(i => i.InstituteID == instituteID);
				if (institute == null)
					return UpdateInstituteStatuses.NoRecordFound;
				institute.InstituteName = instituteName;
				institute.InstituteShortName = instituteShortName;
				institute.InstituteAlias = instituteAlias;
				institute.InstituteCode = instituteCode;
				institute.Website = website;
				institute.MailAddress = address;
				institute.Phone = phone;
				institute.Fax = fax;
				institute.Validate();

				if (aspireContext.Institutes.Any(i => i.InstituteID != institute.InstituteID && i.InstituteName == institute.InstituteName))
					return UpdateInstituteStatuses.NameAlreadyExists;
				if (aspireContext.Institutes.Any(i => i.InstituteID != institute.InstituteID && i.InstituteShortName == institute.InstituteShortName))
					return UpdateInstituteStatuses.ShortNameAlreadyExists;
				if (aspireContext.Institutes.Any(i => i.InstituteID != institute.InstituteID && i.InstituteAlias == institute.InstituteAlias))
					return UpdateInstituteStatuses.AliasAlreadyExists;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateInstituteStatuses.Success;
			}
		}

		public static List<CustomListItem> GetAssignedInstitutesList(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandAdminActiveSession(loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory).Select(i => new CustomListItem
				{
					ID = i.InstituteID,
					Text = i.InstituteAlias
				}).OrderBy(g => g.Text).ToList();
			}
		}
	}
}
