﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Administration.Admin
{
	public static class Faculties
	{
		private static Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Administration.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Administration.ManageFaculties, UserGroupPermission.PermissionValues.Allowed);
		}

		public static List<CustomListItemGuid> GetFaculties(int? instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var faculties = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Faculties);
				if (instituteID != null)
				{
					faculties = faculties.Where(d => d.InstituteID == instituteID);
					return faculties.Select(f => new CustomListItemGuid
					{
						ID = f.FacultyID,
						Text = f.FacultyAlias
					}).OrderBy(d => d.Text).ToList();
				}
				return faculties.Select(f => new CustomListItemGuid
				{
					ID = f.FacultyID,
					Text = f.Institute.InstituteAlias + "/" + f.FacultyAlias
				}).OrderBy(d => d.Text).ToList();
			}
		}

		public sealed class Faculty
		{
			internal Faculty()
			{
			}

			public Guid FacultyID { get; internal set; }
			public string FacultyName { get; internal set; }
			public string FacultyShortName { get; internal set; }
			public string FacultyAlias { get; internal set; }
			public string InstituteAlias { get; internal set; }
		}

		public static List<Faculty> GetFaculties(int? instituteID, string sortExpression, SortDirection sortDirection, int pageIndex, int pageSize, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var faculties = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Faculties);
				if (instituteID != null)
					faculties = faculties.Where(p => p.InstituteID == instituteID);
				virtualItemCount = faculties.Count();
				switch (sortExpression)
				{
					case nameof(Faculty.InstituteAlias):
						faculties = faculties.OrderBy(sortDirection, d => d.Institute.InstituteAlias);
						break;
					case nameof(Faculty.FacultyAlias):
						faculties = faculties.OrderBy(sortDirection, d => d.FacultyAlias);
						break;
					case nameof(Faculty.FacultyShortName):
						faculties = faculties.OrderBy(sortDirection, d => d.FacultyShortName);
						break;
					case nameof(Faculty.FacultyName):
						faculties = faculties.OrderBy(sortDirection, d => d.FacultyName);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return faculties.Skip(pageIndex * pageSize).Take(pageSize).Select(f => new Faculty
				{
					FacultyID = f.FacultyID,
					FacultyName = f.FacultyName,
					FacultyShortName = f.FacultyShortName,
					FacultyAlias = f.FacultyAlias,
					InstituteAlias = f.Institute.InstituteAlias
				}).ToList();
			}
		}

		public sealed class AddFacultyResult
		{
			internal AddFacultyResult()
			{
			}

			public enum Statuses
			{
				NameAlreadyExists,
				ShortNameAlreadyExists,
				AliasAlreadyExists,
				Success
			}

			public Statuses Status { get; internal set; }
			public Model.Entities.Faculty Faculty { get; internal set; }
		}

		public static AddFacultyResult AddFaculty(int instituteID, string facultyName, string facultyShortName, string facultyAlias, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var faculty = new Model.Entities.Faculty
				{
					FacultyID = Guid.NewGuid(),
					InstituteID = instituteID,
					FacultyName = facultyName,
					FacultyShortName = facultyShortName,
					FacultyAlias = facultyAlias,
				}.Validate();
				if (aspireContext.Faculties.Any(f => f.FacultyName == faculty.FacultyName && f.InstituteID == instituteID))
					return new AddFacultyResult { Status = AddFacultyResult.Statuses.NameAlreadyExists };
				if (aspireContext.Faculties.Any(f => f.FacultyShortName == faculty.FacultyShortName && f.InstituteID == instituteID))
					return new AddFacultyResult { Status = AddFacultyResult.Statuses.ShortNameAlreadyExists };
				if (aspireContext.Faculties.Any(f => f.FacultyAlias == faculty.FacultyAlias && f.InstituteID == instituteID))
					return new AddFacultyResult { Status = AddFacultyResult.Statuses.AliasAlreadyExists };
				aspireContext.Faculties.Add(faculty);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return new AddFacultyResult { Status = AddFacultyResult.Statuses.Success, Faculty = faculty };
			}
		}

		public static Model.Entities.Faculty GetFaculty(Guid facultyID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Faculties).SingleOrDefault(f => f.FacultyID == facultyID);
			}
		}

		public enum UpdateFacultyStatuses
		{
			NoRecordFound,
			NameAlreadyExists,
			ShortNameAlreadyExists,
			AliasAlreadyExists,
			Success,
		}

		public static UpdateFacultyStatuses UpdateFaculty(Guid facultyID, string facultyName, string facultyShortName, string facultyAlias, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var faculty = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Faculties).SingleOrDefault(f => f.FacultyID == facultyID);
				if (faculty == null)
					return UpdateFacultyStatuses.NoRecordFound;
				faculty.FacultyName = facultyName;
				faculty.FacultyShortName = facultyShortName;
				faculty.FacultyAlias = facultyAlias;
				faculty.Validate();
				if (aspireContext.Faculties.Any(f => f.InstituteID == faculty.InstituteID && f.FacultyID != faculty.FacultyID && f.FacultyName == faculty.FacultyName))
					return UpdateFacultyStatuses.NameAlreadyExists;
				if (aspireContext.Faculties.Any(f => f.InstituteID == faculty.InstituteID && f.FacultyID != faculty.FacultyID && f.FacultyShortName == faculty.FacultyShortName))
					return UpdateFacultyStatuses.ShortNameAlreadyExists;
				if (aspireContext.Faculties.Any(f => f.InstituteID == faculty.InstituteID && f.FacultyID != faculty.FacultyID && f.FacultyAlias == faculty.FacultyAlias))
					return UpdateFacultyStatuses.AliasAlreadyExists;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateFacultyStatuses.Success;
			}
		}

		public enum DeleteFacultyStatuses
		{
			NoRecordFound,
			Success,
			ChildRecordExistsDepartments,
			ChildRecordExistsContacts
		}

		public static DeleteFacultyStatuses DeleteFaculty(Guid facultyID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var faculty = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Faculties).SingleOrDefault(f => f.FacultyID == facultyID);
				if (faculty == null)
					return DeleteFacultyStatuses.NoRecordFound;
				if (faculty.Departments.Any())
					return DeleteFacultyStatuses.ChildRecordExistsDepartments;
				if (faculty.Contacts.Any())
					return DeleteFacultyStatuses.ChildRecordExistsContacts;

				aspireContext.Faculties.Remove(faculty);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteFacultyStatuses.Success;
			}
		}
	}
}
