﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Administration.Admin
{
	public static class Programs
	{
		private static Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Administration.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Administration.ManagePrograms, UserGroupPermission.PermissionValues.Allowed);
		}

		public static List<Program> GetPrograms(int? instituteID, Guid? facultyID, int? departmentID, string sortExpression, SortDirection sortDirection, int pageIndex, int pageSize, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var programs = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Programs);
				if (instituteID != null)
					programs = programs.Where(p => p.InstituteID == instituteID);
				if (facultyID != null)
					programs = programs.Where(p => p.Department.FacultyID == facultyID);
				if (departmentID != null)
					programs = programs.Where(p => p.DepartmentID == departmentID);
				virtualItemCount = programs.Count();
				switch (sortExpression)
				{
					case nameof(Program.Institute.InstituteAlias):
						programs = programs.OrderBy(sortDirection, p => p.Institute.InstituteAlias);
						break;
					case nameof(Program.Department.Faculty.FacultyAlias):
						programs = programs.OrderBy(sortDirection, p => p.Department.Faculty.FacultyAlias);
						break;
					case nameof(Program.Department.DepartmentAlias):
						programs = programs.OrderBy(sortDirection, p => p.Department.DepartmentAlias);
						break;
					case nameof(Program.ProgramName):
						programs = programs.OrderBy(sortDirection, p => p.ProgramName);
						break;
					case nameof(Program.ProgramShortName):
						programs = programs.OrderBy(sortDirection, p => p.ProgramShortName);
						break;
					case nameof(Program.ProgramAlias):
						programs = programs.OrderBy(sortDirection, p => p.ProgramAlias);
						break;
					case nameof(Program.Duration):
						programs = programs.OrderBy(sortDirection, p => p.Duration);
						break;
					case nameof(Program.ProgramCode):
						programs = programs.OrderBy(sortDirection, p => p.ProgramCode);
						break;
					case nameof(Program.ProgramType):
						programs = programs.OrderBy(sortDirection, p => p.ProgramType);
						break;
					case nameof(Program.DegreeLevel):
						programs = programs.OrderBy(sortDirection, p => p.DegreeLevel);
						break;
					case nameof(Program.MaxAllowedCoursesPerSemester):
						programs = programs.OrderBy(sortDirection, p => p.MaxAllowedCoursesPerSemester);
						break;
					case nameof(Program.MaxAllowedCreditHoursPerSemester):
						programs = programs.OrderBy(sortDirection, p => p.MaxAllowedCreditHoursPerSemester);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				programs = programs.Skip(pageIndex * pageSize).Take(pageSize);
				return programs.Include(p => p.Department.Faculty).Include(p => p.Institute).ToList();
			}
		}

		public sealed class AddProgramResult
		{
			internal AddProgramResult()
			{
			}

			public enum Statuses
			{
				NoRecordFound,
				ShortNameAlreadyExists,
				AliasAlreadyExists,
				Success
			}

			public Statuses Status { get; internal set; }
			public int? ProgramID { get; internal set; }
		}

		public static AddProgramResult AddProgram(int instituteID, int? departmentID, string programName, string programShortName, string programAlias, ProgramDurations programDurationEnum, string programCode, DegreeLevels degreeLevelEnum, ProgramTypes programTypeEnum, DateTime? initiatedDate, int? defaultExamMarksPolicyID, int? defaultExamRemarksPolicyID, byte maxAllowedCoursesPerSemester, byte maxAllowedCreditHoursPerSemester, Guid loginSessionGuid)
		{
			if (maxAllowedCreditHoursPerSemester <= 0)
				throw new ArgumentException("must be greater than zero.", nameof(maxAllowedCreditHoursPerSemester));
			if (maxAllowedCoursesPerSemester <= 0)
				throw new ArgumentException("must be greater than zero.", nameof(maxAllowedCoursesPerSemester));
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var program = new Program
				{
					InstituteID = aspireContext.GetAdminInstitutes(loginHistory).Where(i => i.InstituteID == instituteID).Select(i => i.InstituteID).Single(),
					DepartmentID = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Departments).Where(d => d.InstituteID == instituteID && d.DepartmentID == departmentID).Select(d => (int?)d.DepartmentID).Single(),
					DegreeLevelEnum = degreeLevelEnum,
					DurationEnum = programDurationEnum,
					ProgramAlias = programAlias,
					ProgramName = programName,
					ProgramShortName = programShortName,
					ProgramTypeEnum = programTypeEnum,
					ProgramCode = programCode,
					InitiatedDate = initiatedDate,
					DefaultExamMarksPolicyID = aspireContext.ExamMarksPolicies.Where(emp => emp.InstituteID == instituteID && emp.ExamMarksPolicyID == defaultExamMarksPolicyID).Select(emp => (int?)emp.ExamMarksPolicyID).SingleOrDefault(),
					DefaultExamRemarksPolicyID = aspireContext.ExamRemarksPolicies.Where(erp => erp.InstituteID == instituteID && erp.ExamRemarksPolicyID == defaultExamRemarksPolicyID.Value).Select(erp => (int?)erp.ExamRemarksPolicyID).SingleOrDefault(),
					MaxAllowedCoursesPerSemester = maxAllowedCoursesPerSemester,
					MaxAllowedCreditHoursPerSemester = maxAllowedCreditHoursPerSemester,
					StatusEnum = Program.Statuses.Active,
				}.Validate();
				if (aspireContext.Programs.Any(p => p.InstituteID == program.InstituteID && p.ProgramShortName == program.ProgramShortName))
					return new AddProgramResult { Status = AddProgramResult.Statuses.ShortNameAlreadyExists };
				if (aspireContext.Programs.Any(p => p.InstituteID == program.InstituteID && p.ProgramAlias == program.ProgramAlias))
					return new AddProgramResult { Status = AddProgramResult.Statuses.AliasAlreadyExists };
				aspireContext.Programs.Add(program);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return new AddProgramResult { Status = AddProgramResult.Statuses.Success, ProgramID = program.ProgramID };
			}
		}

		public static Program GetProgram(int programID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Programs).SingleOrDefault(p => p.ProgramID == programID);
			}
		}

		public enum UpdateProgramStatuses
		{
			NoRecordFound,
			ShortNameAlreadyExists,
			AliasAlreadyExists,
			Success,
		}

		public static UpdateProgramStatuses UpdateProgram(int programID, int? departmentID, string programName, string programShortName, string programAlias, ProgramDurations programDurationEnum, string programCode, DegreeLevels degreeLevelEnum, ProgramTypes programTypeEnum, DateTime? initiatedDate, int? defaultExamMarksPolicyID, int? defaultExamRemarksPolicyID, byte maxAllowedCoursesPerSemester, byte maxAllowedCreditHoursPerSemester, Guid loginSessionGuid)
		{
			if (maxAllowedCreditHoursPerSemester <= 0)
				throw new ArgumentException("must be greater than zero.", nameof(maxAllowedCreditHoursPerSemester));
			if (maxAllowedCoursesPerSemester <= 0)
				throw new ArgumentException("must be greater than zero.", nameof(maxAllowedCoursesPerSemester));
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var program = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Programs).SingleOrDefault(p => p.ProgramID == programID);
				if (program == null)
					return UpdateProgramStatuses.NoRecordFound;
				program.DepartmentID = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Departments).Where(d => d.InstituteID == program.InstituteID && d.DepartmentID == departmentID).Select(d => (int?)d.DepartmentID).Single();
				program.ProgramName = programName;
				program.ProgramShortName = programShortName;
				program.ProgramAlias = programAlias;
				program.DurationEnum = programDurationEnum;
				program.ProgramCode = programCode;
				program.DegreeLevelEnum = degreeLevelEnum;
				program.ProgramTypeEnum = programTypeEnum;
				program.InitiatedDate = initiatedDate;
				program.DefaultExamMarksPolicyID = aspireContext.ExamMarksPolicies.Where(emp => emp.InstituteID == program.InstituteID && emp.ExamMarksPolicyID == defaultExamMarksPolicyID).Select(emp => (int?)emp.ExamMarksPolicyID).SingleOrDefault();
				program.DefaultExamRemarksPolicyID = aspireContext.ExamRemarksPolicies.Where(erp => erp.InstituteID == program.InstituteID && erp.ExamRemarksPolicyID == defaultExamRemarksPolicyID.Value).Select(erp => (int?)erp.ExamRemarksPolicyID).SingleOrDefault();
				program.MaxAllowedCoursesPerSemester = maxAllowedCoursesPerSemester;
				program.MaxAllowedCreditHoursPerSemester = maxAllowedCreditHoursPerSemester;
				program.Validate();
				if (aspireContext.Programs.Any(p => p.ProgramID != program.ProgramID && p.InstituteID == program.InstituteID && p.ProgramShortName == program.ProgramShortName))
					return UpdateProgramStatuses.ShortNameAlreadyExists;
				if (aspireContext.Programs.Any(p => p.ProgramID != program.ProgramID && p.InstituteID == program.InstituteID && p.ProgramAlias == program.ProgramAlias))
					return UpdateProgramStatuses.AliasAlreadyExists;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateProgramStatuses.Success;
			}
		}

		public static (List<CustomListItem> ExamMarksPolicies, List<CustomListItem> ExamRemarksPolicies) GetPolicies(int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var examMarksPolicies = aspireContext.GetAdminInstitutes(loginHistory).Where(i => i.InstituteID == instituteID).SelectMany(i => i.ExamMarksPolicies).Select(emp => new CustomListItem
				{
					ID = emp.ExamMarksPolicyID,
					Text = emp.ExamMarksPolicyName
				}).ToList();
				var examRemarksPolicies = aspireContext.GetAdminInstitutes(loginHistory).Where(i => i.InstituteID == instituteID).SelectMany(i => i.ExamRemarksPolicies).Select(erp => new CustomListItem
				{
					ID = erp.ExamRemarksPolicyID,
					Text = erp.ExamRemarksPolicyName
				}).ToList();
				return (examMarksPolicies, examRemarksPolicies);
			}
		}
	}
}
