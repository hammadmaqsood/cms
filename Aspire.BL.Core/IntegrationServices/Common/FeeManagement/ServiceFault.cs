using Aspire.BL.Core.Logs.Common;
using System;
using System.Runtime.Serialization;
using System.ServiceModel;
using Aspire.Model.Entities;

namespace Aspire.BL.Core.IntegrationServices.Common.FeeManagement
{
	[DataContract]
	public sealed class ServiceFault
	{
		[DataMember]
		public string Message { get; set; }

		internal enum FailureReasons
		{
			InvalidCredentials,
			IPAddressNotAuthorized,
			ChallanNoDoesNotExists,
			AlreadyPaidAtCampus,
			AlreadyPaidAtBank,
			LastDueDatePassed,
			InvalidAmount,
			InvalidAccountInformation,
			InvalidChallanNoFormat,
			BranchNameCannotBeNullOrEmpty,
			BranchAddressCannotBeNullOrEmpty,
			NotHandledException,
			DepositDateMustBeTodayDate,
			InstituteBankAccountNotFound,
		}

		internal FailureReasons FailureReason { get; set; }

		public ServiceFault() { }

		private ServiceFault(string message, FailureReasons failureReason) : this()
		{
			this.Message = message;
			this.FailureReason = failureReason;
		}

		private static FaultException<ServiceFault> Create(string message, FailureReasons failureReason)
		{
			return new FaultException<ServiceFault>(new ServiceFault(message, failureReason), message, new FaultCode("Bahria University"));
		}

		internal static FaultException<ServiceFault> InvalidCredentials()
		{
			return Create($"Invalid credentials.", FailureReasons.InvalidCredentials);
		}

		internal static FaultException<ServiceFault> IPAddressNotAuthorized(string ipAddress)
		{
			return Create($"IP Address {ipAddress} is not authorized.", FailureReasons.IPAddressNotAuthorized);
		}

		internal static FaultException<ServiceFault> ChallanNoDoesNotExists(decimal challanNo)
		{
			return Create($"ChallanNo {challanNo} doesn't exists.", FailureReasons.ChallanNoDoesNotExists);
		}

		internal static FaultException<ServiceFault> AlreadyPaidAtCampus(decimal challanNo, DateTime depositDate)
		{
			return Create($"ChallanNo {challanNo} is already paid on {depositDate:D} at Campus.", FailureReasons.AlreadyPaidAtCampus);
		}

		internal static FaultException<ServiceFault> AlreadyPaidAtBank(decimal challanNo, DateTime depositDate, string bankName)
		{
			return Create($"ChallanNo {challanNo} is already paid on {depositDate:F} at {bankName}", FailureReasons.AlreadyPaidAtBank);
		}

		internal static FaultException<ServiceFault> LastDueDatePassed(decimal challanNo, DateTime lastDueDate)
		{
			return Create($"Last Due Date for ChallanNo {challanNo} was {lastDueDate:D}.", FailureReasons.LastDueDatePassed);
		}

		internal static FaultException<ServiceFault> InvalidAmount(decimal challanNo, int amount)
		{
			return Create($"Amount Rs. {amount} is invalid for ChallanNo {challanNo}.", FailureReasons.InvalidAmount);
		}

		internal static FaultException<ServiceFault> InvalidAccountInformation(string accountNo, string accountTitle)
		{
			return Create($"Invalid Account Information has been provided. Account No: {accountNo}, Account Title: {accountTitle}.", FailureReasons.InvalidAccountInformation);
		}

		internal static FaultException<ServiceFault> InvalidChallanNoFormat(decimal challanNo)
		{
			return Create($"ChallanNo {challanNo} is not valid.", FailureReasons.InvalidChallanNoFormat);
		}

		internal static FaultException<ServiceFault> BranchNameCannotBeNullOrEmpty(short branchCode)
		{
			return Create($"Branch Name cannot be null or empty. Branch Code: {branchCode}.", FailureReasons.BranchNameCannotBeNullOrEmpty);
		}

		internal static FaultException<ServiceFault> BranchAddressCannotBeNullOrEmpty(short branchCode)
		{
			return Create($"Branch Address cannot be null or empty. Branch Code: {branchCode}.", FailureReasons.BranchAddressCannotBeNullOrEmpty);
		}

		internal static FaultException<ServiceFault> DepositDateMustBeTodayDate(DateTime depositDateTime)
		{
			return Create($"Deposit Date must be Today's Date: {depositDateTime}.", FailureReasons.DepositDateMustBeTodayDate);
		}

		internal static FaultException<ServiceFault> InstituteBankAccountNotFound(string instituteCode, InstituteBankAccount.FeeTypes feeTypeEnum)
		{
			return Create($"InstituteBankAccount not found. {nameof(instituteCode)}: {instituteCode}, {nameof(feeTypeEnum)}: {feeTypeEnum}", FailureReasons.InstituteBankAccountNotFound);
		}

		internal static void LogException(Type serviceType, int userLoginHistoryID, string ipAddress, Exception exc)
		{
			ErrorHandler.LogException(userLoginHistoryID, exc, -1, serviceType.FullName, null, ipAddress);
		}

		internal static FaultException<ServiceFault> NotHandledException(Type serviceType, int userLoginHistoryID, string ipAddress, Exception exc)
		{
			LogException(serviceType, userLoginHistoryID, ipAddress, exc);
			return Create("Try to complete the request again. The problem may only be temporary. If the problem persists and you need help immediately, contact Bahria University IT Directorate Islamabad.", FailureReasons.NotHandledException);
		}
	}
}