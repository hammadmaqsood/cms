﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.FeeManagement.Common;
using Aspire.BL.Core.FeeServiceMedical;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;
using System.Net;
using System.ServiceModel;

namespace Aspire.BL.Core.IntegrationServices.Common.FeeManagement
{
	internal static class ServiceHelpers
	{
		private static readonly EndpointAddress BUMDCServiceEndPoint = new EndpointAddress(Configurations.Current.AppSettings.BUMDCServiceURL);
		private static readonly BasicHttpBinding BUMDCServiceBinding = new BasicHttpBinding
		{
			Security = new BasicHttpSecurity { Mode = BasicHttpSecurityMode.Transport }
		};

		private static Core.FeeManagement.Common.PaymentFailureReasons ToPaymentFailureReason(this FeeServiceMedical.PaymentFailureReasons paymentFailureReason)
		{
			switch (paymentFailureReason)
			{
				case FeeServiceMedical.PaymentFailureReasons.None:
					return Core.FeeManagement.Common.PaymentFailureReasons.None;
				case FeeServiceMedical.PaymentFailureReasons.InvalidChallanNoFormat:
					return Core.FeeManagement.Common.PaymentFailureReasons.InvalidChallanNoFormat;
				case FeeServiceMedical.PaymentFailureReasons.ChallanNotFound:
				case FeeServiceMedical.PaymentFailureReasons.AdmissionHasNotOpened:
				case FeeServiceMedical.PaymentFailureReasons.AddmissionHasBeenClosed:
					return Core.FeeManagement.Common.PaymentFailureReasons.ChallanNotFound;
				case FeeServiceMedical.PaymentFailureReasons.FeeChallanAlreadyPaid:
					return Core.FeeManagement.Common.PaymentFailureReasons.FeeChallanAlreadyPaid;
				case FeeServiceMedical.PaymentFailureReasons.DueDateIsPassed:
					return Core.FeeManagement.Common.PaymentFailureReasons.DueDateIsPassed;
				case FeeServiceMedical.PaymentFailureReasons.AmountMismatched:
					return Core.FeeManagement.Common.PaymentFailureReasons.AmountMismatched;
				case FeeServiceMedical.PaymentFailureReasons.AmountMustBeGreaterThanZero:
					return Core.FeeManagement.Common.PaymentFailureReasons.AmountMustBeGreaterThanZero;
				case FeeServiceMedical.PaymentFailureReasons.InstituteBankAccountNotFound:
					return Core.FeeManagement.Common.PaymentFailureReasons.InstituteBankAccountNotFound;
				default:
					throw new ArgumentOutOfRangeException(nameof(paymentFailureReason), paymentFailureReason, null);
			}
		}

		internal static Helpers.RawFeeChallan GetRawFeeChallanDetails(string bankName, decimal fullChallanNo, bool ignoreDueDate, bool ignoreZeroOrNegativeAmount, InstituteBankAccount.AccountTypes[] accountTypeEnums, out Core.FeeManagement.Common.PaymentFailureReasons paymentFailureReason)
		{
			var challanNo = AspireFormats.ChallanNo.TryParse(fullChallanNo);
			if (challanNo == null)
			{
				paymentFailureReason = Core.FeeManagement.Common.PaymentFailureReasons.InvalidChallanNoFormat;
				return null;
			}
			if (challanNo.Value.InstituteCode != "99")
			{
				using (var aspireContext = new AspireContext())
				{
					paymentFailureReason = Core.FeeManagement.Common.PaymentFailureReasons.None;
					var rawFeeChallan = aspireContext.GetRawFeeChallanDetails(bankName, challanNo.Value, ignoreZeroOrNegativeAmount);
					if (rawFeeChallan != null)
						paymentFailureReason = aspireContext.CanPay(challanNo.Value, rawFeeChallan.Amount, ignoreDueDate, ignoreZeroOrNegativeAmount, bankName, accountTypeEnums);
					return rawFeeChallan;
				}
			}
			else
			{
				using (var client = new OnlineFeePaymentSoapClient(BUMDCServiceBinding, BUMDCServiceEndPoint))
				{
#if DEBUG
					ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, error) => true;
#endif
					var details = client.GetFeeChallanDetails(Configurations.Current.AppSettings.BUMDCServiceAuth, bankName, fullChallanNo, out var failureReason);
#if DEBUG
					ServicePointManager.ServerCertificateValidationCallback = null;
#endif
					if (details == null)
					{
						paymentFailureReason = Core.FeeManagement.Common.PaymentFailureReasons.ChallanNotFound;
						return null;
					}

					paymentFailureReason = failureReason.ToPaymentFailureReason();
					return new Helpers.RawFeeChallan
					{
						FullChallanNo = details.FullChallanNo,
						SemesterID = details.SemesterID,
						Name = details.Name,
						Amount = details.Amount,
						DueDate = details.DueDate,
						Paid = details.Paid,
						DepositDate = details.DepositDate,
						DepositedInBankName = details.DepositedInBankName,
						DepositedInAccountNo = details.DepositedInAccountNo,
						BankAccountNo = details.BankAccountNo,
						BankAccountTitle = details.BankAccountTitle,
						//InstituteCode = "99",
						//ProgramID = 0,
						InstituteID = 0,
						//AdmissionOpenProgramID = 0,
						//CandidateAppliedProgramID = null,
						//StudentFeeChallanID = null,
						ApplicationNo = null,
						Enrollment = null,
						AccountTransactionID = null,
						TransactionReferenceID = null,
						ServiceEnum = null,
						//DepartmentID = null,
						SystemDate = null,
						LastUpdatedByUserName = null,
						LastUpdatedByUserType = null,
					};
				}
			}
		}

		internal static Core.FeeManagement.Common.PaymentFailureReasons PayFeeChallan(AspireFormats.ChallanNo challanNo, string bankName, AccountTransaction.Services serviceEnum, AccountTransaction.TransactionChannels transactionChannelEnum, AccountTransaction.PaymentTypes paymentTypeEnum, InstituteBankAccount.AccountTypes[] accountTypeEnums, int challanAmount, string transactionReferenceID, string accountNo, string accountTitle, string branchCode, string branchName, DateTime transactionDate, string remarks, bool verified, int userLoginHistoryID)
		{
			lock (Locks.AccountTransaction)
			{
				if (challanNo.InstituteCode != "99")
				{
					using (var aspireContext = new AspireContext(true))
					{
						if (!aspireContext.Institutes.Any(i => i.InstituteCode == challanNo.InstituteCode))
							return Core.FeeManagement.Common.PaymentFailureReasons.ChallanNotFound;
						var instituteBankAccount = aspireContext.GetInstituteBankAccount(challanNo.InstituteCode, challanNo.FeeTypeEnum, bankName, accountTypeEnums);
						if (instituteBankAccount == null)
							return Core.FeeManagement.Common.PaymentFailureReasons.InstituteBankAccountNotFound;
						var accountTransaction = Helpers.CreateAccountTransaction(serviceEnum, transactionChannelEnum, paymentTypeEnum, challanNo, challanAmount, transactionReferenceID, accountNo, accountTitle, branchCode, branchName, transactionDate, instituteBankAccount.InstituteBankAccountID, remarks, verified, userLoginHistoryID);
						var paymentFailureReason = aspireContext.PayFee(accountTransaction, false, false);
						if (paymentFailureReason == Core.FeeManagement.Common.PaymentFailureReasons.None)
							aspireContext.CommitTransaction();
						else
							aspireContext.RollbackTransaction();
						return paymentFailureReason;
					}
				}
				else
				{
					using (var client = new OnlineFeePaymentSoapClient(BUMDCServiceBinding, BUMDCServiceEndPoint))
					{
						Services service;
						switch (serviceEnum)
						{
							case AccountTransaction.Services.Manual:
								throw new InvalidOperationException();
							case AccountTransaction.Services.BankAlfalah:
								service = Services.BankAlfalah;
								break;
							case AccountTransaction.Services.AlliedBank:
								service = Services.AlliedBank;
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(serviceEnum), serviceEnum, null);
						}

						TransactionChannels transactionChannel;
						switch (transactionChannelEnum)
						{
							case AccountTransaction.TransactionChannels.Campus:
								throw new InvalidOperationException();
							case AccountTransaction.TransactionChannels.ATM:
								transactionChannel = TransactionChannels.ATM;
								break;
							case AccountTransaction.TransactionChannels.MobileBanking:
								transactionChannel = TransactionChannels.MobileBanking;
								break;
							case AccountTransaction.TransactionChannels.Branch:
								transactionChannel = TransactionChannels.Branch;
								break;
							case AccountTransaction.TransactionChannels.InternetBanking:
								transactionChannel = TransactionChannels.InternetBanking;
								break;
							case AccountTransaction.TransactionChannels.ContactCenter:
								transactionChannel = TransactionChannels.ContactCenter;
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(transactionChannelEnum), transactionChannelEnum, null);
						}

						PaymentTypes paymentType;
						switch (paymentTypeEnum)
						{
							case AccountTransaction.PaymentTypes.Cash:
								paymentType = PaymentTypes.Cash;
								break;
							case AccountTransaction.PaymentTypes.Account:
								paymentType = PaymentTypes.Account;
								break;
							case AccountTransaction.PaymentTypes.DemandDraft:
								paymentType = PaymentTypes.DemandDraft;
								break;
							case AccountTransaction.PaymentTypes.PayOrder:
								paymentType = PaymentTypes.PayOrder;
								break;
							case AccountTransaction.PaymentTypes.Cheque:
								paymentType = PaymentTypes.Cheque;
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(paymentTypeEnum), paymentTypeEnum, null);
						}
#if DEBUG
						ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, error) => true;
#endif
						var failureReason = client.PayFeeChallan(Configurations.Current.AppSettings.BUMDCServiceAuth, challanNo.FullChallanNo, service, transactionChannel, paymentType, challanAmount, transactionReferenceID, accountNo, accountTitle, branchCode, branchName, transactionDate);
#if DEBUG
						ServicePointManager.ServerCertificateValidationCallback = null;
#endif
						return failureReason.ToPaymentFailureReason();
					}
				}
			}
		}
	}
}
