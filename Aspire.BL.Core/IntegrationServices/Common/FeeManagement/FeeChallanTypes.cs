﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.Common.FeeManagement
{
	internal enum FeeChallanTypes : byte
	{
		/// <summary>
		/// AdmissionProcessingFee type is used for Admission Processing Fee (e.g. Prospectus Fee).
		/// </summary>
		[EnumMember]
		AdmissionProcessingFee = Model.Entities.InstituteBankAccount.FeeTypes.AdmissionProcessingFee,
		/// <summary>
		/// NewAdmission type is used for non-enrolled students. (e.g. Candidate applying for admission)
		/// </summary>
		[EnumMember]
		NewAdmission = Model.Entities.InstituteBankAccount.FeeTypes.NewAdmissionFee,
		/// <summary>
		/// ExistingStudent type is used for Enrolled students.
		/// </summary>
		[EnumMember]
		ExistingStudent = Model.Entities.InstituteBankAccount.FeeTypes.StudentFee,
	}
}
