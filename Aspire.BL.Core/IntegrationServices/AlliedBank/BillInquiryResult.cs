using Aspire.Lib.Extensions;
using System;
using System.Globalization;

namespace Aspire.BL.Core.IntegrationServices.AlliedBank
{
	public sealed class BillInquiryResult
	{
		public enum ResponseCodes
		{
			ChallanNoCanBePaid = 0,
			InvalidChallanNo = 1,
			ChallanNoCannotBePaid = 2,
			UnknownBadTransaction = 3,
			InvalidData = 4,
			ProcessingFailed = 5
		}
		public enum BillStatuses
		{
			Unpaid = 'U',
			Paid = 'P',
			Blocked = 'B',
		}

		public ResponseCodes ResponseCode { get; private set; }
		public string ConsumerDetail { get; private set; }
		public BillStatuses BillStatus { get; private set; }
		public DateTime? DueDate { get; private set; }
		public int? AmountWithinDueDate { get; private set; }
		public int? AmountAfterDueDate { get; private set; }
		public DateTime? BillingMonth { get; private set; }
		public DateTime? DatePaid { get; private set; }
		public int? AmountPaid { get; private set; }
		public string TranAuthID { get; private set; }
		public string Reserved { get; private set; }

		#region Helpers

		internal static string ConfirmLength(string input, byte length, bool correctLength)
		{
			if (input == null)
				return string.Empty.PadRight(length, ' ');
			if (input.Length == length)
				return input;
			if (!correctLength)
				throw new ArgumentException(nameof(input));
			return input.Length < length ? input.PadRight(length, ' ') : input.Substring(0, length);
		}

		private const string DateFormat = "yyyyMMdd";
		private static DateTime? ParseDate(string input)
		{
			if (input?.Length != 8)
				throw new ArgumentException(nameof(input));
			if (string.IsNullOrWhiteSpace(input))
				return null;
			if (DateTime.TryParseExact(input, DateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out var date))
				return date;
			throw new ArgumentException(nameof(input));
		}

		private static int? ParseAmount(string amount)
		{
			if (amount?.Length != 14)
				throw new ArgumentException(nameof(amount));
			if (string.IsNullOrWhiteSpace(amount))
				return null;
			if (amount[0] == '+' || amount[0] != '-')
				if (amount.Substring(12, 2) == "00")
					return amount.Substring(0, 12).TryToInt() ?? throw new ArgumentException(nameof(amount));
			throw new ArgumentException(nameof(amount));
		}

		private static int? ParseAmountPaid(string amount)
		{
			if (amount?.Length != 12)
				throw new ArgumentException(nameof(amount));
			if (string.IsNullOrWhiteSpace(amount))
				return null;
			var _amount = amount.Substring(0, 10).TryToInt() ?? throw new ArgumentException(nameof(amount));
			if (_amount > 0)
				return _amount;
			throw new ArgumentException(nameof(amount));
		}

		private static string GetDateString(DateTime? dateTime, string format) => (dateTime?.ToString(format)) ?? string.Empty.PadRight(format.Length);

		public static string GetAmountString(int? amount) => amount == null
			? string.Empty.PadRight(14, ' ')
			: (amount < 0 ? $"{amount:D11}00" : $"+{amount:D11}00");

		public static string GetAmountPaidString(int? amount) => amount > 0 ? $"{amount:D10}00" : string.Empty.PadRight(12, ' ');

		#endregion

		public void SetResponseCode(ResponseCodes responseCode)
		{
			this.ResponseCode = responseCode;
		}

		public void SetResponseCode(string responseCode)
		{
			if (responseCode?.Length != 2)
				throw new ArgumentException();
			var code = (ResponseCodes)responseCode.ToInt();
			if (Enum.IsDefined(typeof(ResponseCodes), code))
				this.SetResponseCode(code);
			else
				throw new ArgumentException();
		}

		public void SetConsumerDetail(string consumerDetails, bool correctLength) => this.ConsumerDetail = ConfirmLength(consumerDetails, 30, correctLength);

		public void SetBillStatus(BillStatuses billStatus)
		{
			this.BillStatus = billStatus;
		}

		public void SetBillStatus(string billStatus)
		{
			if (billStatus?.Length != 1)
				throw new ArgumentException();
			var status = (BillStatuses)billStatus[0];
			if (Enum.IsDefined(typeof(BillStatuses), status))
				this.SetBillStatus(status);
			else
				throw new ArgumentException();
		}

		public void SetDueDate(DateTime? dueDate)
		{
			if (dueDate != dueDate?.Date)
				throw new ArgumentException(nameof(dueDate));
			this.DueDate = dueDate;
		}

		public void SetDueDate(string dueDate) => this.SetDueDate(ParseDate(dueDate));

		public void SetAmountWithInDueDate(int? amountWithInDueDate) => this.AmountWithinDueDate = amountWithInDueDate;

		public void SetAmountWithInDueDate(string amountWithInDueDate) => this.SetAmountWithInDueDate(ParseAmount(amountWithInDueDate));

		public void SetAmountAfterDueDate(int? amountAfterDueDate) => this.AmountAfterDueDate = amountAfterDueDate;

		public void SetAmountAfterDueDate(string amountAfterDueDate) => this.SetAmountAfterDueDate(ParseAmount(amountAfterDueDate));

		public void SetBillingMonth(DateTime? billingMonth)
		{
			if (billingMonth == null)
				this.BillingMonth = null;
			else if (billingMonth.Value == billingMonth.Value.TruncateMonth())
				this.BillingMonth = billingMonth;
			else
				throw new ArgumentException(nameof(billingMonth));
		}

		public void SetBillingMonth(string billingMonth)
		{
			if (billingMonth?.Length != 4)
				throw new ArgumentException(nameof(billingMonth));
			if (string.IsNullOrWhiteSpace(billingMonth))
				this.SetBillingMonth((DateTime?)null);
			else if (DateTime.TryParseExact(billingMonth, "yyMM", CultureInfo.InvariantCulture, DateTimeStyles.None, out var month))
				this.SetBillingMonth(month);
			else
				throw new ArgumentException(nameof(billingMonth));
		}

		public void SetDatePaid(DateTime? datePaid)
		{
			if (datePaid != datePaid?.Date)
				throw new ArgumentException(nameof(datePaid));
			this.DatePaid = datePaid;
		}

		public void SetDatePaid(string datePaid) => this.SetDatePaid(ParseDate(datePaid));

		public void SetAmountPaid(int? amountPaid) => this.AmountPaid = amountPaid;

		public void SetAmountPaid(string amountPaid) => this.SetAmountPaid(ParseAmount(amountPaid));

		public void SetTranAuthID(string tranAuthID, bool correctLength) => this.TranAuthID = ConfirmLength(tranAuthID, 6, correctLength);

		public void SetReserved(string reserved, bool correctLength) => this.Reserved = ConfirmLength(reserved, 200, correctLength);

		public override string ToString()
		{
			return string.Join("", new[]
			{
				((int)this.ResponseCode).ToString("00"),
				this.ConsumerDetail,
				((char)this.BillStatus).ToString(),
				GetDateString(this.DueDate,DateFormat),
				GetAmountString(this.AmountWithinDueDate),
				GetAmountString(this.AmountAfterDueDate),
				GetDateString(this.BillingMonth,"yyMM"),
				GetDateString(this.DatePaid,DateFormat),
				GetAmountPaidString(this.AmountPaid),
				this.TranAuthID,
				this.Reserved
			});
		}

		public static BillInquiryResult Parse(string resultString)
		{
			var result = new BillInquiryResult();
			var startIndex = 0;
			var length = 2;
			result.SetResponseCode(resultString.Substring(startIndex, length));
			startIndex += length;
			length = 30;
			result.SetConsumerDetail(resultString.Substring(startIndex, length), false);//2+30=32
			startIndex += length;
			length = 1;
			result.SetBillStatus(resultString.Substring(startIndex, length));
			startIndex += length;
			length = 8;
			result.SetDueDate(resultString.Substring(startIndex, length));
			startIndex += length;
			length = 14;
			result.SetAmountWithInDueDate(resultString.Substring(startIndex, length));
			startIndex += length;
			length = 14;
			result.SetAmountAfterDueDate(resultString.Substring(startIndex, length));
			startIndex += length;
			length = 4;
			result.SetBillingMonth(resultString.Substring(startIndex, length));
			startIndex += length;
			length = 8;
			result.SetDatePaid(resultString.Substring(startIndex, length));
			startIndex += length;
			length = 14;
			result.SetAmountPaid(resultString.Substring(startIndex, length));
			startIndex += length;
			length = 6;
			result.SetTranAuthID(resultString.Substring(startIndex, length), false);
			startIndex += length;
			length = 200;
			result.SetReserved(resultString.Substring(startIndex, length), false);
			return result;
		}

		public static BillInquiryResult New(ResponseCodes responseCode, BillStatuses billStatus)
		{
			var result = new BillInquiryResult();
			result.SetResponseCode(responseCode);
			result.SetConsumerDetail(null, true);
			result.SetBillStatus(billStatus);
			result.SetDueDate((DateTime?)null);
			result.SetAmountWithInDueDate((int?)null);
			result.SetAmountAfterDueDate((int?)null);
			result.SetBillingMonth((DateTime?)null);
			result.SetDatePaid((DateTime?)null);
			result.SetAmountPaid((int?)null);
			result.SetTranAuthID(null, true);
			result.SetReserved(null, true);
			return result;
		}

		public static BillInquiryResult New(ResponseCodes responseCode, string name, BillStatuses billStatus, DateTime dueDate, int amount, DateTime? depositDate, string transactionReferenceID)
		{
			var result = new BillInquiryResult();
			result.SetResponseCode(responseCode);
			result.SetConsumerDetail(name, true);
			result.SetBillStatus(billStatus);
			result.SetDueDate(dueDate);
			result.SetAmountWithInDueDate(amount);
			result.SetAmountAfterDueDate((int?)null);
			result.SetBillingMonth(new DateTime(dueDate.Year, dueDate.Month, 1));
			result.SetDatePaid(depositDate);
			result.SetAmountPaid(depositDate != null ? amount : (int?)null);
			result.SetTranAuthID(transactionReferenceID, true);
			result.SetReserved(null, true);
			return result;
		}
	}
}