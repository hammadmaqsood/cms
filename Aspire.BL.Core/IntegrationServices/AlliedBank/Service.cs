﻿using Aspire.BL.Core.FeeManagement.Common;
using Aspire.BL.Core.IntegrationServices.Common.FeeManagement;
using Aspire.BL.Core.LoginManagement;
using Aspire.BL.Core.Logs.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Aspire.BL.Core.IntegrationServices.AlliedBank
{
	public static class Service
	{
		public static string BankName => AspireConstants.AlliedBankName;

		private static IntegratedService.LoginHistory Authenticate(string username, string password, out string errorMessage)
		{
			var userNameGuid = username.TryToGuid();
			var passwordGuid = password.TryToGuid();
			if (userNameGuid == null || userNameGuid == Guid.Empty || userNameGuid == default(Guid))
			{
				errorMessage = "InvalidCredentials";
				return null;
			}
			if (passwordGuid == null || passwordGuid == Guid.Empty || passwordGuid == default(Guid))
			{
				errorMessage = "InvalidCredentials";
				return null;
			}
			var authenticated = IntegratedService.Authenticate(userNameGuid.Value, passwordGuid.Value, IntegratedServiceUser.ServiceTypes.AlliedBank);
			if (!authenticated)
			{
				errorMessage = "InvalidCredentials";
				return null;
			}

			var ipAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
			var loginHistory = IntegratedService.Login(userNameGuid.Value, IntegratedServiceUser.ServiceTypes.AlliedBank, ipAddress);
			if (loginHistory == null)
			{
				errorMessage = "IPAddressNotAuthorized";
				return null;
			}
			errorMessage = null;
			return loginHistory;
		}

		private static readonly InstituteBankAccount.AccountTypes[] AccountTypeEnums = { InstituteBankAccount.AccountTypes.Online, InstituteBankAccount.AccountTypes.ManualOnline };

		private static BillInquiryResult BillInquiry(string userName, string password, string consumerNumber, string bankMnemonic, string reserved, out IntegratedService.LoginHistory loginHistory, out string responseJSON)
		{
			loginHistory = null;
			try
			{
				loginHistory = Authenticate(userName, password, out var errorMessage);
				if (loginHistory == null)
				{
					responseJSON = errorMessage;
					return null;
				}

				var challanNo = AspireFormats.ChallanNo.TryParse(consumerNumber);
				if (challanNo == null)
				{
					responseJSON = "Invalid Challan No.";
					return BillInquiryResult.New(BillInquiryResult.ResponseCodes.InvalidData, BillInquiryResult.BillStatuses.Blocked);
				}

				if (!new int?[] { null, 0, 3, 8 }.Contains(bankMnemonic?.Length) || !new int?[] { null, 0, 200 }.Contains(reserved?.Length))
				{
					responseJSON = $"Invalid input for {nameof(bankMnemonic)} or {nameof(reserved)}";
					return BillInquiryResult.New(BillInquiryResult.ResponseCodes.InvalidData, BillInquiryResult.BillStatuses.Blocked);
				}

				var rawFeeChallan = ServiceHelpers.GetRawFeeChallanDetails(BankName, consumerNumber.ToDecimal(), false, false, AccountTypeEnums, out var paymentFailureReason);
				responseJSON = new
				{
					rawFeeChallan,
				}.ToJsonString();
				if (rawFeeChallan == null)
					return BillInquiryResult.New(BillInquiryResult.ResponseCodes.InvalidChallanNo, BillInquiryResult.BillStatuses.Blocked);
				switch (paymentFailureReason)
				{
					case PaymentFailureReasons.ChallanNotFound:
					case PaymentFailureReasons.InvalidChallanNoFormat:
						return BillInquiryResult.New(BillInquiryResult.ResponseCodes.InvalidChallanNo, BillInquiryResult.BillStatuses.Blocked);
					case PaymentFailureReasons.FeeChallanAlreadyPaid:
						return BillInquiryResult.New(BillInquiryResult.ResponseCodes.ChallanNoCannotBePaid, rawFeeChallan.Name, BillInquiryResult.BillStatuses.Paid, rawFeeChallan.DueDate.Date, rawFeeChallan.Amount, rawFeeChallan.DepositDate?.Date, rawFeeChallan.TransactionReferenceID);
					case PaymentFailureReasons.DueDateIsPassed:
					case PaymentFailureReasons.AmountMismatched:
					case PaymentFailureReasons.AmountMustBeGreaterThanZero:
					case PaymentFailureReasons.InstituteBankAccountNotFound:
						return BillInquiryResult.New(BillInquiryResult.ResponseCodes.ChallanNoCannotBePaid, rawFeeChallan.Name, BillInquiryResult.BillStatuses.Blocked, rawFeeChallan.DueDate.Date, rawFeeChallan.Amount, rawFeeChallan.DepositDate?.Date, rawFeeChallan.TransactionReferenceID);
					case PaymentFailureReasons.None:
						return BillInquiryResult.New(BillInquiryResult.ResponseCodes.ChallanNoCanBePaid, rawFeeChallan.Name, BillInquiryResult.BillStatuses.Unpaid, rawFeeChallan.DueDate.Date, rawFeeChallan.Amount, rawFeeChallan.DepositDate?.Date, rawFeeChallan.TransactionReferenceID);
					default:
						throw new NotImplementedEnumException(paymentFailureReason);
				}
			}
			catch (Exception exc)
			{
				responseJSON = new
				{
					exc.Source,
					exc.Message,
					exc.StackTrace,
					InnerException = exc.InnerException == null
						? null
						: new
						{
							exc.InnerException.Source,
							exc.InnerException.Message,
							exc.InnerException.StackTrace
						},
				}.ToJsonString();
				ErrorHandler.LogException(loginHistory?.UserLoginHistoryID, exc, -1, typeof(Service).FullName, null, loginHistory?.IPAddress ?? System.Net.IPAddress.Loopback.ToString());
				return BillInquiryResult.New(BillInquiryResult.ResponseCodes.ProcessingFailed, BillInquiryResult.BillStatuses.Blocked);
			}
		}

		public static string BillInquiry(string userName, string password, string consumerNumber, string bankMnemonic, string reserved)
		{
			var parameters = new { consumerNumber, bankMnemonic, reserved }.ToJsonString();
			var attemptDate = DateTime.Now;
			var billInquiryResult = BillInquiry(userName, password, consumerNumber, bankMnemonic, reserved, out var loginHistory, out var responseJSON);
			if (loginHistory != null)
				IntegrationBaseService.AddIntegrationServiceLog(typeof(Service), nameof(BillInquiry), attemptDate, loginHistory.UserLoginHistoryID, loginHistory.IPAddress, parameters, responseJSON);
			return billInquiryResult?.ToString();
		}

		private static AccountTransaction.TransactionChannels GetTransactionChannel(string channelID)
		{
			switch (channelID)
			{
				case "0001":
					return AccountTransaction.TransactionChannels.ATM;
				case "0105":
					return AccountTransaction.TransactionChannels.ContactCenter;
				case "0113":
					return AccountTransaction.TransactionChannels.MobileBanking;
				case "0118":
					return AccountTransaction.TransactionChannels.Branch;
				case "0056":
					return AccountTransaction.TransactionChannels.InternetBanking;
				default:
					throw new ArgumentOutOfRangeException(nameof(channelID), channelID, null);
			}
		}

		private static void DecodeReserved(string reserved, out AccountTransaction.PaymentTypes paymentTypeEnum, out AccountTransaction.TransactionChannels transactionChannelEnum, out string branchCode, out string branchName, out string accountNo, out string accountTitle)
		{
			var match = Regex.Match(reserved, @"^(?<AccountNoOrCash>[^\|]+)\|(?<ChannelID>\d{4})\|(?<BranchCode>\d{4})$");
			if (match.Success)
			{
				var accountNoOrCash = match.Groups["AccountNoOrCash"].Value;
				if (string.Equals(accountNoOrCash, "CASH", StringComparison.InvariantCultureIgnoreCase))
				{
					paymentTypeEnum = AccountTransaction.PaymentTypes.Cash;
					accountNo = null;
					accountTitle = null;
				}
				else
				{
					paymentTypeEnum = AccountTransaction.PaymentTypes.Account;
					accountNo = accountNoOrCash;
					accountTitle = null;
				}
				transactionChannelEnum = GetTransactionChannel(match.Groups["ChannelID"].Value);
				branchCode = match.Groups["BranchCode"].Value;
				branchName = null;
				return;
			}
			paymentTypeEnum = AccountTransaction.PaymentTypes.Cash;
			transactionChannelEnum = AccountTransaction.TransactionChannels.Branch;
			branchCode = null;
			branchName = null;
			accountNo = null;
			accountTitle = null;
		}

		private static string BillPayment(string userName, string password, string consumerNumber, string authId, string amount, string tranDate, string tranTime, string bankMnemonic, string reserved, out IntegratedService.LoginHistory loginHistory, out string responseJSON)
		{
			loginHistory = null;
			var invalidData = false;
			try
			{
				loginHistory = Authenticate(userName, password, out var errorMessage);
				if (loginHistory == null)
				{
					responseJSON = errorMessage;
					return errorMessage;
				}
				invalidData = true;
				var challanNo = AspireFormats.ChallanNo.TryParse(consumerNumber);
				if (challanNo == null)
					throw new ArgumentException($"Challan No. format is invalid. {nameof(consumerNumber)}: {consumerNumber}", nameof(consumerNumber));
				if (authId?.Length != 6)
					throw new ArgumentException($"authID must be 6 characters longs. {nameof(authId)}: {authId}", nameof(authId));
				if (amount.Length != 13)
					throw new ArgumentException($"amount must be 13 characters long. amount: {amount}", nameof(amount));
				if (!amount.EndsWith("00"))
					throw new ArgumentException($"amount minor digits (last 2) must be zeros. amount: {amount}", nameof(amount));
				var challanAmount = amount.Substring(0, amount.Length - 2).TrimStart('0').ToInt();
				if (tranDate.Length != 8 || !DateTime.TryParseExact(tranDate, "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.None, out var transactionDate))
					throw new ArgumentException($"Invalid tranDate. Value: {tranDate}", nameof(tranDate));
				if (tranTime.Length != 6 || !TimeSpan.TryParseExact(tranTime, "hhmmss", CultureInfo.CurrentCulture, TimeSpanStyles.None, out var transactionTime))
					throw new ArgumentException($"Invalid tranTime. Value: {tranTime}", nameof(tranTime));
				transactionDate = transactionDate.Add(transactionTime);
				if (!new int?[] { null, 0, 3, 8 }.Contains(bankMnemonic?.Length))
					throw new ArgumentException($"Length must be 3 or 8. bankMnemonic: {bankMnemonic}", nameof(bankMnemonic));
				DecodeReserved(reserved, out var paymentTypeEnum, out var transactionChannelEnum, out var branchCode, out var branchName, out var accountNo, out var accountTitle);
				invalidData = false;
				var paymentFailureReason = ServiceHelpers.PayFeeChallan(challanNo.Value, BankName, AccountTransaction.Services.AlliedBank, transactionChannelEnum, paymentTypeEnum, AccountTypeEnums, challanAmount, authId, accountNo, accountTitle, branchCode, branchName, transactionDate, null, true, loginHistory.UserLoginHistoryID);
				responseJSON = new { paymentFailureReason }.ToJsonString();
				switch (paymentFailureReason)
				{
					case PaymentFailureReasons.InvalidChallanNoFormat:
					case PaymentFailureReasons.AmountMismatched:
					case PaymentFailureReasons.AmountMustBeGreaterThanZero:
						return BillPaymentResult.InvalidData;
					case PaymentFailureReasons.ChallanNotFound:
						return BillPaymentResult.ChallanNoNotFound;
					case PaymentFailureReasons.FeeChallanAlreadyPaid:
						return BillPaymentResult.DuplicateTransaction;
					case PaymentFailureReasons.DueDateIsPassed:
					case PaymentFailureReasons.InstituteBankAccountNotFound:
						return BillPaymentResult.ProcessingFailed;
					case PaymentFailureReasons.None:
						return BillPaymentResult.Success;
					default:
						throw new NotImplementedEnumException(paymentFailureReason);
				}
			}
			catch (Exception exc)
			{
				responseJSON = new
				{
					exc.Source,
					exc.Message,
					exc.StackTrace,
					InnerException = exc.InnerException == null
						? null
						: new
						{
							exc.InnerException.Source,
							exc.InnerException.Message,
							exc.InnerException.StackTrace
						},
				}.ToJsonString();
				if (!invalidData)
					ErrorHandler.LogException(loginHistory?.UserLoginHistoryID, exc, -1, typeof(Service).FullName, null, loginHistory?.IPAddress ?? System.Net.IPAddress.Loopback.ToString());
				return invalidData ? BillPaymentResult.InvalidData : BillPaymentResult.UnknownError;
			}
		}

		public static string BillPayment(string userName, string password, string consumerNumber, string authId, string amount, string tranDate, string tranTime, string bankMnemonic, string reserved)
		{
			var parameters = new { consumerNumber, authId, amount, tranDate, tranTime, bankMnemonic, reserved }.ToJsonString();
			var attemptDate = DateTime.Now;
			var billPaymentResult = BillPayment(userName, password, consumerNumber, authId, amount, tranDate, tranTime, bankMnemonic, reserved, out var loginHistory, out var response);
			if (loginHistory != null)
				IntegrationBaseService.AddIntegrationServiceLog(typeof(Service), nameof(BillPayment), attemptDate, loginHistory.UserLoginHistoryID, loginHistory.IPAddress, parameters, response);
			return billPaymentResult;
		}

		private static string EchoMessage(string userName, string password, string ping, out IntegratedService.LoginHistory loginHistory, out string response)
		{
			loginHistory = null;
			try
			{
				loginHistory = Authenticate(userName, password, out var errorMessage);
				if (loginHistory == null)
				{
					response = errorMessage;
					return errorMessage;
				}
				response = ping == "ARE_YOU_ALIVE" ? ping : null;
				return response;
			}
			catch (Exception exc)
			{
				response = new
				{
					exc.Source,
					exc.Message,
					exc.StackTrace,
					InnerException = exc.InnerException == null
						? null
						: new
						{
							exc.InnerException.Source,
							exc.InnerException.Message,
							exc.InnerException.StackTrace
						},
				}.ToJsonString();
				ErrorHandler.LogException(loginHistory?.UserLoginHistoryID, exc, -1, typeof(Service).FullName, null, loginHistory?.IPAddress ?? System.Net.IPAddress.Loopback.ToString());
				return null;
			}
		}

		public static string EchoMessage(string userName, string password, string ping)
		{
			var parameters = new { ping }.ToJsonString();
			var attemptDate = DateTime.Now;
			var echoMessageResult = EchoMessage(userName, password, ping, out var loginHistory, out var response);
			if (loginHistory != null)
				IntegrationBaseService.AddIntegrationServiceLog(typeof(Service), nameof(EchoMessage), attemptDate, loginHistory.UserLoginHistoryID, loginHistory.IPAddress, parameters, response);
			return echoMessageResult;
		}
	}
}