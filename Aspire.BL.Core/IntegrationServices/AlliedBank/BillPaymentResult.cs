using Aspire.Lib.Extensions;
using System;

namespace Aspire.BL.Core.IntegrationServices.AlliedBank
{
	public sealed class BillPaymentResult
	{
		public enum ResponseCodes
		{
			Success = 0,
			ChallanNoNotFound = 1,
			UnknownError = 2,
			DuplicateTransaction = 3,
			InvalidData = 4,
			ProcessingFailed = 5,
		}

		public ResponseCodes ResponseCode { get; private set; }
		public string IdentificationParameter { get; private set; }
		public string Reserved { get; private set; }

		public void SetResponseCode(ResponseCodes responseCode)
		{
			this.ResponseCode = responseCode;
		}

		public void SetResponseCode(string responseCode)
		{
			if (responseCode?.Length != 2)
				throw new ArgumentException();
			var code = (ResponseCodes)responseCode.ToInt();
			if (Enum.IsDefined(typeof(ResponseCodes), code))
				this.SetResponseCode(code);
			else
				throw new ArgumentException();
		}

		private void SetIdentificationParameter(string identificationParameter, bool correctLength)
		{
			this.IdentificationParameter = BillInquiryResult.ConfirmLength(identificationParameter, 20, correctLength);
		}

		private void SetReserved(string reserved, bool correctLength)
		{
			this.Reserved = BillInquiryResult.ConfirmLength(reserved, 200, correctLength);
		}

		public override string ToString()
		{
			return $"{(int)this.ResponseCode:00}{this.IdentificationParameter}{this.Reserved}";
		}

		public static BillPaymentResult Parse(string billPaymentResultString)
		{
			var billPaymentResult = new BillPaymentResult();
			billPaymentResult.SetResponseCode(billPaymentResultString.Substring(0, 2));
			billPaymentResult.SetIdentificationParameter(billPaymentResultString.Substring(2, 20), false);
			billPaymentResult.SetReserved(billPaymentResultString.Substring(22, 200), false);
			return billPaymentResult;
		}

		private static BillPaymentResult New(ResponseCodes responseCode)
		{
			var billPaymentResult = new BillPaymentResult();
			billPaymentResult.SetResponseCode(responseCode);
			billPaymentResult.SetIdentificationParameter(null, true);
			billPaymentResult.SetReserved(null, true);
			return billPaymentResult;
		}

		public static string Success => New(ResponseCodes.Success).ToString();
		public static string ChallanNoNotFound => New(ResponseCodes.ChallanNoNotFound).ToString();
		public static string UnknownError => New(ResponseCodes.UnknownError).ToString();
		public static string DuplicateTransaction => New(ResponseCodes.DuplicateTransaction).ToString();
		public static string InvalidData => New(ResponseCodes.InvalidData).ToString();
		public static string ProcessingFailed => New(ResponseCodes.ProcessingFailed).ToString();
	}
}