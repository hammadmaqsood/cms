using Aspire.BL.Core.Logs.Common;
using System;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Aspire.BL.Core.IntegrationServices.BankAlfalah
{
	[DataContract]
	public sealed class ServiceFault
	{
		public enum FailureReasons
		{
			InvalidCredentials,
			IPAddressNotAuthorized,
			ChallanNoDoesNotExists,
			AlreadyPaidAtCampus,
			AlreadyPaidAtBank,
			LastDueDatePassed,
			InvalidAmount,
			InvalidAccountInformation,
			InvalidChallanNoFormat,
			BranchNameCannotBeNullOrEmpty,
			BranchAddressCannotBeNullOrEmpty,
			NotHandledException,
			DepositDateMustBeTodayDate
		}

		[DataMember]
		public string Message { get; set; }

		internal FailureReasons FailureReason { get; set; }

		public ServiceFault() { }

		internal ServiceFault(string message, FailureReasons failureReason) : this()
		{
			this.Message = message;
			this.FailureReason = failureReason;
		}

		private static FaultException<ServiceFault> Create(string message, FailureReasons failureReason)
		{
			return new FaultException<ServiceFault>(new ServiceFault(message, failureReason), message, new FaultCode("Bahria University"));
		}

		internal static FaultException<ServiceFault> ChallanNoDoesNotExists(decimal challanNo)
		{
			throw Create($"ChallanNo {challanNo} doesn't exists.", FailureReasons.ChallanNoDoesNotExists);
		}

		internal static FaultException<ServiceFault> AlreadyPaidAtBank(decimal challanNo)
		{
			throw Create($"ChallanNo {challanNo} is already paid.", FailureReasons.AlreadyPaidAtBank);
		}

		internal static FaultException<ServiceFault> LastDueDatePassed(decimal challanNo)
		{
			throw Create($"Last Due Date for ChallanNo {challanNo} is passed.", FailureReasons.LastDueDatePassed);
		}

		internal static FaultException<ServiceFault> InvalidAmount(decimal challanNo, int amount)
		{
			throw Create($"Amount Rs. {amount} is invalid for ChallanNo {challanNo}.", FailureReasons.InvalidAmount);
		}

		internal static FaultException<ServiceFault> InvalidAccountInformation()
		{
			throw Create($"Invalid Account Information has been provided.", FailureReasons.InvalidAccountInformation);
		}

		internal static FaultException<ServiceFault> InvalidChallanNoFormat(decimal challanNo)
		{
			throw Create($"ChallanNo {challanNo} is not valid.", FailureReasons.InvalidChallanNoFormat);
		}

		internal static void LogException(Type serviceType, int userLoginHistoryID, string ipAddress, Exception exc)
		{
			ErrorHandler.LogException(userLoginHistoryID, exc, -1, serviceType.FullName, null, ipAddress);
		}

		internal static Exception NotHandledException(Type serviceType, int userLoginHistoryID, string ipAddress, Exception exc)
		{
			LogException(serviceType, userLoginHistoryID, ipAddress, exc);
			throw Create("Try to complete the request again. The problem may only be temporary. If the problem persists and you need help immediately, contact Bahria University IT Directorate Islamabad.", FailureReasons.NotHandledException);
		}
	}
}