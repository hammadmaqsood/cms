﻿using Aspire.BL.Core.FeeManagement.Common;
using Aspire.BL.Core.IntegrationServices.Common.FeeManagement;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.ServiceModel;

namespace Aspire.BL.Core.IntegrationServices.BankAlfalah
{
	public class Service : IntegrationBaseService, IService
	{
		private static string BankName => AspireConstants.BankAlfalahName;

		public Service() : base(IntegratedServiceUser.ServiceTypes.BankAlfalah)
		{
		}

		public static void Configure(ServiceConfiguration config)
		{
			ConfigureService<IService>(config, IntegratedServiceUser.ServiceTypes.BankAlfalah);
		}

		public static readonly InstituteBankAccount.AccountTypes[] AccountTypeEnums = { InstituteBankAccount.AccountTypes.Online, InstituteBankAccount.AccountTypes.ManualOnline };

		public FeeChallan GetChallanDetails(decimal challanNo)
		{
			var parameters = new { challanNo }.ToJsonString();
			var attemptDate = DateTime.Now;
			string responseJSON = null;
			try
			{
				var rawFeeChallan = ServiceHelpers.GetRawFeeChallanDetails(BankName, challanNo, false, false, AccountTypeEnums, out var paymentFailureReason);
				var challan = FeeChallan.GetFeeChallan(rawFeeChallan);
				responseJSON = new { challan, paymentFailureReason }.ToJsonString();
				switch (paymentFailureReason)
				{
					case PaymentFailureReasons.None:
						return challan;
					case PaymentFailureReasons.ChallanNotFound:
						throw ServiceFault.ChallanNoDoesNotExists(challanNo);
					case PaymentFailureReasons.FeeChallanAlreadyPaid:
						throw ServiceFault.AlreadyPaidAtBank(challanNo);
					case PaymentFailureReasons.DueDateIsPassed:
						throw ServiceFault.LastDueDatePassed(challanNo);
					case PaymentFailureReasons.InvalidChallanNoFormat:
						throw ServiceFault.InvalidChallanNoFormat(challanNo);
					case PaymentFailureReasons.AmountMismatched:
					case PaymentFailureReasons.AmountMustBeGreaterThanZero:
					case PaymentFailureReasons.InstituteBankAccountNotFound:
						throw new InvalidOperationException(paymentFailureReason.ToString());
					default:
						throw new NotImplementedEnumException(paymentFailureReason);
				}
			}
			catch (FaultException<ServiceFault>)
			{
				throw;
			}
			catch (Exception exc)
			{
				throw ServiceFault.NotHandledException(typeof(Service), this.LoginHistory.UserLoginHistoryID, this.LoginHistory.IPAddress, exc);
			}
			finally
			{
				IntegrationBaseService.AddIntegrationServiceLog(typeof(Service), nameof(this.GetChallanDetails), attemptDate, this.LoginHistory.UserLoginHistoryID, this.LoginHistory.IPAddress, parameters, responseJSON);
			}
		}

		public void UpdateChallan(decimal challanNo, int amount, string accountNo, string accountTitle, short branchCode, string branchName, string branchAddress)
		{
			var parameters = new { challanNo, amount, accountNo, accountTitle, branchCode, branchName, branchAddress }.ToJsonString();
			var attemptDate = DateTime.Now;
			string responseJSON = null;
			try
			{
				var challan = AspireFormats.ChallanNo.TryParse(challanNo);
				if (challan == null)
					throw ServiceFault.InvalidChallanNoFormat(challanNo);

				var paymentFailureReason = ServiceHelpers.PayFeeChallan(challan.Value, BankName, AccountTransaction.Services.BankAlfalah, AccountTransaction.TransactionChannels.Branch, AccountTransaction.PaymentTypes.Cash, AccountTypeEnums, amount, challanNo.ToString(AspireFormats.ChallanNo.FullChallanNoDecimalFormat), null, null, branchCode.ToString(), branchName, DateTime.Now, null, true, this.LoginHistory.UserLoginHistoryID);
				responseJSON = new { paymentFailureReason }.ToJsonString();
				switch (paymentFailureReason)
				{
					case PaymentFailureReasons.None:
						break;
					case PaymentFailureReasons.InvalidChallanNoFormat:
						throw ServiceFault.InvalidChallanNoFormat(challanNo);
					case PaymentFailureReasons.ChallanNotFound:
						throw ServiceFault.ChallanNoDoesNotExists(challanNo);
					case PaymentFailureReasons.FeeChallanAlreadyPaid:
						throw ServiceFault.AlreadyPaidAtBank(challanNo);
					case PaymentFailureReasons.DueDateIsPassed:
						throw ServiceFault.LastDueDatePassed(challanNo);
					case PaymentFailureReasons.AmountMismatched:
					case PaymentFailureReasons.AmountMustBeGreaterThanZero:
						throw ServiceFault.InvalidAmount(challanNo, amount);
					case PaymentFailureReasons.InstituteBankAccountNotFound:
						throw ServiceFault.InvalidAccountInformation();
					default:
						throw new NotImplementedEnumException(paymentFailureReason);
				}
			}
			catch (FaultException<ServiceFault>)
			{
				throw;
			}
			catch (Exception exc)
			{
				responseJSON = exc.ToJsonString();
				throw;
			}
			finally
			{
				IntegrationBaseService.AddIntegrationServiceLog(typeof(Service), nameof(this.UpdateChallan), attemptDate, this.LoginHistory.UserLoginHistoryID, this.LoginHistory.IPAddress, parameters, responseJSON);
			}
		}
	}
}
