﻿using System;
using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.BankAlfalah
{
	[DataContract]
	public sealed class FeeChallan
	{
		[DataMember]
		public int Amount { get; set; }
		[DataMember]
		public string ApplicationNoOrEnrollment { get; set; }
		[DataMember]
		public string BankAlfalahAccountNo { get; set; }
		[DataMember]
		public string BankAlfalahAccountTitle { get; set; }
		[DataMember]
		public decimal ChallanNo { get; set; }
		[DataMember]
		public DateTime? DepositDate { get; set; }
		[DataMember]
		public string DepositedInAccountNo { get; set; }
		[DataMember]
		public string DepositedInAccountTitle { get; set; }
		[DataMember]
		public string DepositedInBankName { get; set; }
		[DataMember]
		public short? DepositedInBranchCode { get; set; }
		[DataMember]
		public string DepositedInBranchName { get; set; }
		[DataMember]
		public string DepositPlace { get; set; }
		[DataMember]
		public DateTime DueDate { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public bool Paid { get; set; }

		internal static FeeChallan GetFeeChallan(FeeManagement.Common.Helpers.RawFeeChallan rawFeeChallan)
		{
			if (rawFeeChallan == null)
				return null;
			return new FeeChallan
			{
				ChallanNo = rawFeeChallan.FullChallanNo,
				ApplicationNoOrEnrollment = rawFeeChallan.Enrollment ?? rawFeeChallan.ApplicationNo.ToString(),
				Name = rawFeeChallan.Name,
				Amount = rawFeeChallan.Amount,
				DueDate = rawFeeChallan.DueDate,
				Paid = rawFeeChallan.DepositDate != null,
				BankAlfalahAccountTitle = rawFeeChallan.BankAccountTitle,
				BankAlfalahAccountNo = rawFeeChallan.BankAccountNo,
				DepositDate = rawFeeChallan.DepositDate,
				DepositPlace = "Bank",
				DepositedInBankName = rawFeeChallan.DepositedInBankName,
				DepositedInBranchCode = null,
				DepositedInBranchName = null,
				DepositedInAccountNo = rawFeeChallan.DepositedInAccountNo,
				DepositedInAccountTitle = null,
			};
		}
	}
}