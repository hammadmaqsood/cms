using System.ServiceModel;

namespace Aspire.BL.Core.IntegrationServices.BankAlfalah
{
	[ServiceContract]
	public interface IService
	{
		[OperationContract]
		[FaultContract(typeof(ServiceFault))]
		FeeChallan GetChallanDetails(decimal challanNo);

		[OperationContract]
		[FaultContract(typeof(ServiceFault))]
		void UpdateChallan(decimal challanNo, int amount, string accountNo, string accountTitle, short branchCode, string branchName, string branchAddress);
	}
}