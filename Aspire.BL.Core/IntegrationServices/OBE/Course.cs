﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.OBE
{
	[DataContract]
	public sealed class Course
	{
		[DataMember] public int CourseID { get; set; }
		[DataMember] public int AdmissionOpenProgramID { get; set; }
		[DataMember] public string CourseCode { get; set; }
		[DataMember] public string Title { get; set; }
		[DataMember] public decimal CreditHours { get; set; }
		[DataMember] public decimal ContactHours { get; set; }
		[DataMember] public byte CourseCategory { get; set; }
	}
}