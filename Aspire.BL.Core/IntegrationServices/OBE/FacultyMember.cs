﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.OBE
{
	[DataContract]
	public sealed class FacultyMember
	{
		[DataMember] public int FacultyMemberID { get; set; }
		[DataMember] public int InstituteID { get; set; }
		[DataMember] public string Name { get; set; }
		[DataMember] public string UserName { get; set; }
		[DataMember] public string Email { get; set; }
		[DataMember] public int? DepartmentID { get; set; }
	}
}