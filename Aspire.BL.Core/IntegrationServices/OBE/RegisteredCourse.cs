﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.OBE
{
	[DataContract]
	public sealed class RegisteredCourse
	{
		[DataMember] public int RegisteredCourseID { get; set; }
		[DataMember] public int StudentID { get; set; }
		[DataMember] public int OfferedCourseID { get; set; }
		[DataMember] public int CourseID { get; set; }
		[DataMember] public byte? Status { get; set; }
		[DataMember] public byte? FreezedStatus { get; set; }
	}
}