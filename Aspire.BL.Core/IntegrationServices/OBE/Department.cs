﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.OBE
{
	[DataContract]
	public sealed class Department
	{
		[DataMember] public int DepartmentID { get; set; }
		[DataMember] public int InstituteID { get; set; }
		[DataMember] public string DepartmentName { get; set; }
		[DataMember] public string DepartmentShortName { get; set; }
		[DataMember] public string DepartmentAlias { get; set; }
	}
}