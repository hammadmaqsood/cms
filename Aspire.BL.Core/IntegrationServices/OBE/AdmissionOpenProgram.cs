﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.OBE
{
	[DataContract]
	public sealed class AdmissionOpenProgram
	{
		[DataMember] public int AdmissionOpenProgramID { get; set; }
		[DataMember] public short SemesterID { get; set; }
		[DataMember] public int ProgramID { get; set; }
	}
}