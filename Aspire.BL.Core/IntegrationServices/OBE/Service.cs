﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

namespace Aspire.BL.Core.IntegrationServices.OBE
{
	public sealed class Service : IntegrationBaseService, IService
	{
		public Service() : base(IntegratedServiceUser.ServiceTypes.OBE)
		{
		}

		public static void Configure(ServiceConfiguration config)
		{
			ConfigureService<IService>(config, IntegratedServiceUser.ServiceTypes.OBE);
		}

		private IQueryable<Model.Entities.Institute> GetInstitutes(AspireContext aspireContext)
		{
			if (this.LoginHistory.InstituteID == null)
				return aspireContext.Institutes.AsQueryable();
			return aspireContext.Institutes.Where(i => i.InstituteID == this.LoginHistory.InstituteID.Value);
		}

		private IQueryable<OfferedCours> GetOfferedCourses(AspireContext aspireContext, short offeredSemesterID)
		{
			if (this.LoginHistory.InstituteID == null)
				return aspireContext.OfferedCourses.Where(oc => oc.SemesterID == offeredSemesterID);
			return aspireContext.OfferedCourses
				.Where(oc => oc.SemesterID == offeredSemesterID)
				.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == this.LoginHistory.InstituteID.Value);
		}

		public List<Institute> GetInstitutes1()
		{
			using (var aspireContext = new AspireContext())
			{
				return this.GetInstitutes(aspireContext)
					.Select(i => new Institute
					{
						InstituteID = i.InstituteID,
						InstituteName = i.InstituteName,
						InstituteShortName = i.InstituteShortName,
						InstituteAlias = i.InstituteAlias,
						InstituteCode = i.InstituteCode,
					}).ToList();
			}
		}

		public List<Department> GetDepartments2()
		{
			using (var aspireContext = new AspireContext())
			{
				return this.GetInstitutes(aspireContext)
					.SelectMany(i => i.Departments)
					.Select(d => new Department
					{
						DepartmentID = d.DepartmentID,
						InstituteID = d.InstituteID,
						DepartmentAlias = d.DepartmentAlias,
						DepartmentName = d.DepartmentName,
						DepartmentShortName = d.DepartmentShortName,
					}).ToList();
			}
		}

		public List<Program> GetPrograms3()
		{
			using (var aspireContext = new AspireContext())
			{
				return this.GetInstitutes(aspireContext)
					.SelectMany(i => i.Programs)
					.Select(p => new Program
					{
						ProgramID = p.ProgramID,
						InstituteID = p.InstituteID,
						DepartmentID = p.DepartmentID,
						ProgramName = p.ProgramName,
						ProgramShortName = p.ProgramShortName,
						ProgramAlias = p.ProgramAlias,
						Duration = p.Duration,
					}).ToList();
			}
		}

		public List<AdmissionOpenProgram> GetAdmissionOpenPrograms4()
		{
			using (var aspireContext = new AspireContext())
			{
				return this.GetInstitutes(aspireContext)
					.SelectMany(i => i.Programs)
					.SelectMany(p => p.AdmissionOpenPrograms)
					.Select(aop => new AdmissionOpenProgram
					{
						AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
						SemesterID = aop.SemesterID,
						ProgramID = aop.ProgramID,
					}).ToList();
			}
		}

		public List<Student> GetStudents5(short registeredInSemesterID)
		{
			using (var aspireContext = new AspireContext())
			{
				return this.GetOfferedCourses(aspireContext, registeredInSemesterID)
					.SelectMany(oc => oc.RegisteredCourses)
					.Where(rc => rc.DeletedDate == null)
					.Select(rc => rc.Student)
					.Select(s => new Student
					{
						StudentID = s.StudentID,
						Enrollment = s.Enrollment,
						AdmissionOpenProgramID = s.AdmissionOpenProgramID,
						RegistrationNo = s.RegistrationNo,
						Name = s.Name,
						UniversityEmail = s.UniversityEmail,
						Status = s.Status,
					})
					.Distinct()
					.ToList();
			}
		}

		public List<StudentSemester> GetStudentSemesters6(short registeredInSemesterID)
		{
			using (var aspireContext = new AspireContext())
			{
				var studentSemesters = this.LoginHistory.InstituteID == null
					   ? aspireContext.StudentSemesters.Where(ss => ss.SemesterID == registeredInSemesterID)
					   : aspireContext.StudentSemesters.Where(ss => ss.Student.AdmissionOpenProgram.Program.InstituteID == this.LoginHistory.InstituteID.Value);
				return studentSemesters.Select(ss => new StudentSemester
				{
					StudentSemesterID = ss.StudentSemesterID,
					StudentID = ss.StudentID,
					SemesterID = ss.SemesterID,
					SemesterNo = ss.SemesterNo,
					Section = ss.Section,
					Shift = ss.Shift,
					Status = ss.Status,
					FreezedStatus = ss.FreezedStatus,
				}).ToList();
			}
		}

		public List<FacultyMember> GetFacultyMembers7(short registeredInSemesterID)
		{
			using (var aspireContext = new AspireContext())
			{
				return this.GetOfferedCourses(aspireContext, registeredInSemesterID)
					.Where(oc => oc.FacultyMemberID != null)
					.Select(oc => oc.FacultyMember)
					.Select(fm => new FacultyMember
					{
						FacultyMemberID = fm.FacultyMemberID,
						InstituteID = fm.InstituteID,
						Name = fm.Name,
						UserName = fm.UserName,
						Email = fm.Email,
						DepartmentID = fm.DepartmentID,
					})
					.Distinct()
					.ToList();
			}
		}

		public List<Course> GetCourses8(short registeredInSemesterID)
		{
			using (var aspireContext = new AspireContext())
			{
				return this.GetInstitutes(aspireContext)
					.SelectMany(i => i.Programs)
					.SelectMany(p => p.AdmissionOpenPrograms)
					.SelectMany(aop => aop.Courses)
					.Select(c => new Course
					{
						CourseID = c.CourseID,
						AdmissionOpenProgramID = c.AdmissionOpenProgramID,
						CourseCode = c.CourseCode,
						Title = c.Title,
						CreditHours = c.CreditHours,
						ContactHours = c.ContactHours,
						CourseCategory = c.CourseCategory,
					}).ToList();
			}
		}

		public List<OfferedCourse> GetOfferedCourses9(short registeredInSemesterID)
		{
			using (var aspireContext = new AspireContext())
			{
				return this.GetOfferedCourses(aspireContext, registeredInSemesterID)
					.Select(oc => new OfferedCourse
					{
						OfferedCourseID = oc.OfferedCourseID,
						CourseID = oc.CourseID,
						SemesterID = oc.SemesterID,
						FacultyMemberID = oc.FacultyMemberID,
						SemesterNo = oc.SemesterNo,
						Section = oc.Section,
						Shift = oc.Shift,
						Visiting = oc.Visiting,
						SpecialOffered = oc.SpecialOffered,
					}).ToList();
			}
		}

		public List<RegisteredCourse> GetRegisteredCourses10(short registeredInSemesterID)
		{
			using (var aspireContext = new AspireContext())
			{
				return this.GetOfferedCourses(aspireContext, registeredInSemesterID)
					.SelectMany(oc => oc.RegisteredCourses)
					.Where(rc => rc.DeletedDate == null)
					.Select(rc => new RegisteredCourse
					{
						RegisteredCourseID = rc.RegisteredCourseID,
						CourseID = rc.CourseID,
						FreezedStatus = rc.FreezedStatus,
						OfferedCourseID = rc.OfferedCourseID,
						Status = rc.Status,
						StudentID = rc.StudentID,
					}).ToList();
			}
		}
	}
}
