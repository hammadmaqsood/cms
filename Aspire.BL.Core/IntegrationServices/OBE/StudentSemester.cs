﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.OBE
{
	[DataContract]
	public sealed class StudentSemester
	{
		[DataMember] public int StudentSemesterID { get; set; }
		[DataMember] public int StudentID { get; set; }
		[DataMember] public short SemesterID { get; set; }
		[DataMember] public short SemesterNo { get; set; }
		[DataMember] public int Section { get; set; }
		[DataMember] public byte Shift { get; set; }
		[DataMember] public byte Status { get; set; }
		[DataMember] public byte? FreezedStatus { get; set; }
	}
}