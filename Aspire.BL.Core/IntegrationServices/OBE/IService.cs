using System.Collections.Generic;
using System.ServiceModel;

namespace Aspire.BL.Core.IntegrationServices.OBE
{
	[ServiceContract]
	public interface IService
	{
		[OperationContract]
		List<Institute> GetInstitutes1();
		[OperationContract]
		List<Department> GetDepartments2();
		[OperationContract]
		List<Program> GetPrograms3();
		[OperationContract]
		List<AdmissionOpenProgram> GetAdmissionOpenPrograms4();
		[OperationContract]
		List<Student> GetStudents5(short registeredInSemesterID);
		[OperationContract]
		List<StudentSemester> GetStudentSemesters6(short registeredInSemesterID);
		[OperationContract]
		List<FacultyMember> GetFacultyMembers7(short registeredInSemesterID);
		[OperationContract]
		List<Course> GetCourses8(short registeredInSemesterID);
		[OperationContract]
		List<OfferedCourse> GetOfferedCourses9(short registeredInSemesterID);
		[OperationContract]
		List<RegisteredCourse> GetRegisteredCourses10(short registeredInSemesterID);
	}
}