﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.OBE
{
	[DataContract]
	public sealed class Program
	{
		[DataMember] public int ProgramID { get; set; }
		[DataMember] public int InstituteID { get; set; }
		[DataMember] public int? DepartmentID { get; set; }
		[DataMember] public string ProgramName { get; set; }
		[DataMember] public string ProgramShortName { get; set; }
		[DataMember] public string ProgramAlias { get; set; }
		[DataMember] public byte Duration { get; set; }
	}
}