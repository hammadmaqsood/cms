﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.OBE
{
	[DataContract]
	public sealed class OfferedCourse
	{
		[DataMember] public int OfferedCourseID { get; set; }
		[DataMember] public int CourseID { get; set; }
		[DataMember] public short SemesterID { get; set; }
		[DataMember] public int? FacultyMemberID { get; set; }
		[DataMember] public short SemesterNo { get; set; }
		[DataMember] public int Section { get; set; }
		[DataMember] public byte Shift { get; set; }
		[DataMember] public bool Visiting { get; set; }
		[DataMember] public bool SpecialOffered { get; set; }
	}
}