﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.OBE
{
	[DataContract]
	public sealed class Institute
	{
		[DataMember] public int InstituteID { get; set; }
		[DataMember] public string InstituteName { get; set; }
		[DataMember] public string InstituteShortName { get; set; }
		[DataMember] public string InstituteAlias { get; set; }
		[DataMember] public string InstituteCode { get; set; }
	}
}