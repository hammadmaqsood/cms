﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.OBE
{
	[DataContract]
	public sealed class Student
	{
		[DataMember] public int StudentID { get; set; }
		[DataMember] public string Enrollment { get; set; }
		[DataMember] public int AdmissionOpenProgramID { get; set; }
		[DataMember] public int? RegistrationNo { get; internal set; }
		[DataMember] public string Name { get; internal set; }
		[DataMember] public string UniversityEmail { get; internal set; }
		[DataMember] public byte? Status { get; internal set; }
	}
}