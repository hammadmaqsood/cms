using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Aspire.BL.Core.IntegrationServices.CBT
{
	[ServiceContract]
	public interface IService
	{
		[OperationContract]
		List<Institute> GetInstitutes();
		[OperationContract]
		List<CandidateInfo> GetCandidatesList(int? instituteID, short semesterID);
		[OperationContract]
		UpdateAdmissionTestScoreStatuses UpdateAdmissionTestScore(Guid applicationGuid, DateTime entryTestDate, byte entryTestScorePercentage);
	}
}