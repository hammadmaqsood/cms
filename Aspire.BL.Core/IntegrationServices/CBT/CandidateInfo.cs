using System;
using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.CBT
{
	[DataContract]
	public class CandidateInfo
	{
		[DataMember]
		public int? ApplicationNo { get; set; }
		[DataMember]
		public Guid ApplicationGuid { get; set; }
		[DataMember]
		public short Semester { get; set; }
		[DataMember]
		public string Password { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public string FatherName { get; set; }
		[DataMember]
		public string Email { get; set; }
		[DataMember]
		public string ProgramAlias { get; set; }
		[DataMember]
		public decimal DegreeDuration { get; set; }
		[DataMember]
		public bool EntryTestPercentageEntered { get; set; }
	}
}