﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

namespace Aspire.BL.Core.IntegrationServices.CBT
{
	public class Service : IntegrationBaseService, IService
	{
		public Service() : base(IntegratedServiceUser.ServiceTypes.CBT)
		{
		}

		public static void Configure(ServiceConfiguration config)
		{
			ConfigureService<IService>(config, IntegratedServiceUser.ServiceTypes.CBT);
		}

		List<Institute> IService.GetInstitutes()
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.Institutes.Select(i => new Institute
				{
					InstituteID = i.InstituteID,
					InstituteName = i.InstituteName,
					InstituteAlias = i.InstituteAlias,
				}).OrderBy(i => i.InstituteID).ToList();
			}
		}

		List<CandidateInfo> IService.GetCandidatesList(int? instituteID, short semesterID)
		{
			this.CheckAuthorization();
			using (var aspireContext = new AspireContext())
			{
				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms
					.Where(aop => aop.SemesterID == semesterID)
					.Where(aop => aop.EntryTestType == (byte)AdmissionOpenProgram.EntryTestTypes.ComputerBasedTest);
				if (this.LoginHistory.InstituteID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.Program.InstituteID == this.LoginHistory.InstituteID.Value);
				if (instituteID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.Program.InstituteID == instituteID.Value);
				var candidateAppliedPrograms = aspireContext.CandidateAppliedPrograms.Where(cap => cap.AccountTransactionID != null);
				return (from cap in candidateAppliedPrograms
						join aop in admissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
						select new CandidateInfo
						{
							ApplicationNo = cap.ApplicationNo,
							ApplicationGuid = cap.ApplicationGuid,
							Semester = aop.SemesterID,
							Name = cap.Candidate.Name,
							Email = cap.Candidate.Email,
							ProgramAlias = aop.Program.ProgramAlias,
							Password = cap.Candidate.Password,
							DegreeDuration = aop.Program.Duration,
							EntryTestPercentageEntered = cap.EntryTestMarksPercentage != null,
							FatherName = cap.Candidate.FatherName,
						}).ToList();
			}
		}

		UpdateAdmissionTestScoreStatuses IService.UpdateAdmissionTestScore(Guid applicationGuid, DateTime entryTestDate, byte entryTestScorePercentage)
		{
			this.CheckAuthorization();
			var instituteID = this.LoginHistory.InstituteID;
			if (100 > entryTestScorePercentage)
				throw new ArgumentOutOfRangeException(nameof(entryTestScorePercentage), entryTestScorePercentage, null);
			this.CheckAuthorization();
			using (var aspireContext = new AspireContext(true))
			{
				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms
					.Where(aop => aop.EntryTestType == (byte)AdmissionOpenProgram.EntryTestTypes.ComputerBasedTest);
				if (this.LoginHistory.InstituteID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.Program.InstituteID == this.LoginHistory.InstituteID.Value);
				if (instituteID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.Program.InstituteID == instituteID.Value);
				var candidateAppliedProgram = (from cap in aspireContext.CandidateAppliedPrograms
											   join aop in admissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
											   where cap.ApplicationGuid == applicationGuid && cap.AccountTransactionID != null
											   select cap).SingleOrDefault();
				if (candidateAppliedProgram == null)
					return UpdateAdmissionTestScoreStatuses.NoRecordFound;
				if (candidateAppliedProgram.EntryTestMarksPercentage != null)
					return UpdateAdmissionTestScoreStatuses.ScoreAlreadySet;
				candidateAppliedProgram.EntryTestMarksPercentage = entryTestScorePercentage;
				candidateAppliedProgram.EntryTestDate = entryTestDate;
				aspireContext.SaveChangesAndCommitTransaction(this.LoginHistory.UserLoginHistoryID);
				return UpdateAdmissionTestScoreStatuses.Success;
			}
		}
	}
}
