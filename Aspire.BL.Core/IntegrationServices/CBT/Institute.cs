using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.CBT
{
	[DataContract]
	public class Institute
	{
		[DataMember]
		public int InstituteID { get; set; }
		[DataMember]
		public string InstituteAlias { get; internal set; }
		[DataMember]
		public string InstituteName { get; set; }
	}
}