using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.CBT
{
	[DataContract]
	public enum UpdateAdmissionTestScoreStatuses
	{
		[EnumMember]
		NoRecordFound,
		[EnumMember]
		Success,
		[EnumMember]
		ScoreAlreadySet,
	}
}