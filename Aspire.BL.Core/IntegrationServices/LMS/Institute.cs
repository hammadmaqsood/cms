﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.LMS
{
	[DataContract]
	public class Institute
	{
		[DataMember]
		public int InstituteID { get; set; }
		[DataMember]
		public string InstituteName { get; set; }
		[DataMember]
		public string InstituteShortName { get; set; }
		[DataMember]
		public string InstituteAlias { get; set; }
	}
}
