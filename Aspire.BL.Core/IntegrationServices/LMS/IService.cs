using System;
using System.ServiceModel;

namespace Aspire.BL.Core.IntegrationServices.LMS
{
	[ServiceContract]
	public interface IService
	{
		[OperationContract]
		User VerifySession(Guid key);
	}
}