﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.LMS
{
	[DataContract]
	public class RegisteredCourse
	{
		[DataMember]
		public int RegisteredCourseID { get; set; }
		[DataMember]
		public int StudentID { get; set; }
		[DataMember]
		public int OfferedCourseID { get; set; }
		[DataMember]
		public string Status { get; set; }
		[DataMember]
		public string FreezedStatus { get; set; }
	}
}