﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.LMS
{
	[DataContract]
	public class Semester
	{
		[DataMember]
		public short SemesterID { get; set; }
		[DataMember]
		public string SemesterType { get; set; }
		[DataMember]
		public short Year { get; set; }
		[DataMember]
		public string SemesterFullName { get; set; }
	}
}