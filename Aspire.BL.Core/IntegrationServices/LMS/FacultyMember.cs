﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.LMS
{
	[DataContract]
	public class FacultyMember
	{
		[DataMember]
		public int FacultyMemberID { get; set; }
		[DataMember]
		public int InstituteID { get; set; }
		[DataMember]
		public int? DepartmentID { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public string Email { get; set; }
	}
}