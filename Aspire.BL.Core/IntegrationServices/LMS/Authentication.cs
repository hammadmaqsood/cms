﻿using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.IntegrationServices.LMS
{
	public sealed class Authentication
	{
		public abstract class User
		{
			public abstract UserTypes UserTypeEnum { get; }
			public string Name { get; set; }
			public string Email { get; set; }
			public DateTime ExpiryDate { get; set; }
		}

		public sealed class Student : User
		{
			public override UserTypes UserTypeEnum => UserTypes.Student;
			public int StudentID { get; set; }
		}

		public sealed class FacultyMember : User
		{
			public override UserTypes UserTypeEnum => UserTypes.Faculty;
			public int FacultyMemberID { get; set; }
		}

		public sealed class Executive : User
		{
			public override UserTypes UserTypeEnum => UserTypes.Executive;
			public int UserID { get; set; }
		}

		private static readonly Dictionary<Guid, User> Users = new Dictionary<Guid, User>();

		private static readonly object UsersLock = new object();

		private static DateTime ExpiryDate => DateTime.Now.AddMinutes(20);

		private static void Cleanup()
		{
			lock (UsersLock)
			{
				Users.Where(u => u.Value.ExpiryDate <= DateTime.Now).Select(u => u.Key).ToList().ForEach(k => Users.Remove(k));
			}
		}

		public static Guid AddStudent(int studentID, string name, string email)
		{
			lock (UsersLock)
			{
				var key = Guid.NewGuid();
				Users.Add(key, new Student
				{
					StudentID = studentID,
					Name = name,
					Email = email,
					ExpiryDate = ExpiryDate,
				});
				return key;
			}
		}

		public static Guid AddFacultyMember(int facultyMemberID, string name, string email)
		{
			lock (UsersLock)
			{
				var key = Guid.NewGuid();
				Users.Add(key, new FacultyMember
				{
					FacultyMemberID = facultyMemberID,
					Name = name,
					Email = email,
					ExpiryDate = ExpiryDate
				});
				return key;
			}
		}

		public static Guid AddExecutive(int userID, string name, string email)
		{
			lock (UsersLock)
			{
				var key = Guid.NewGuid();
				Users.Add(key, new Executive
				{
					UserID = userID,
					Name = name,
					Email = email,
					ExpiryDate = ExpiryDate
				});
				return key;
			}
		}

		public static User Verify(Guid key)
		{
			try
			{
				lock (UsersLock)
				{
					if (Users.TryGetValue(key, out var user))
					{
						Users.Remove(key);
						if (user.ExpiryDate < DateTime.Now)
							return null;
						return user;
					}
					return null;
				}
			}
			finally
			{
				Cleanup();
			}
		}
	}
}