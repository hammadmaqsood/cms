﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.LMS
{
	[DataContract]
	public class Permission
	{
		[DataMember]
		public int InstituteID { get; set; }
		[DataMember]
		public int? DepartmentID { get; set; }
		[DataMember]
		public int? ProgramID { get; set; }
	}
}
