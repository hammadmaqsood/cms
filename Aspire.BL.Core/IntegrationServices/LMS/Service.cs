﻿using Aspire.BL.Core.LoginManagement;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.ServiceModel;

namespace Aspire.BL.Core.IntegrationServices.LMS
{
	public sealed class Service : IntegrationBaseService, IService
	{
		public Service() : base(IntegratedServiceUser.ServiceTypes.LMS)
		{
		}

		public static void Configure(ServiceConfiguration config)
		{
			ConfigureService<IService>(config, IntegratedServiceUser.ServiceTypes.LMS);
		}

		public User VerifySession(Guid key)
		{
			var user = Authentication.Verify(key);
			switch (user)
			{
				case Authentication.Student student:
					return new User { ID = student.StudentID, UserType = "Student" };
				case Authentication.FacultyMember facultyMember:
					return new User { ID = facultyMember.FacultyMemberID, UserType = "FacultyMember" };
				default:
					throw new ArgumentOutOfRangeException(nameof(user), user, null);
			}
		}

		private static IntegratedService.LoginHistory Authenticate(string username, string password)
		{
			var userNameGuid = username.TryToGuid();
			var passwordGuid = password.TryToGuid();
			if (userNameGuid == null || userNameGuid == Guid.Empty || userNameGuid == default(Guid))
				throw new SecurityException("InvalidCredentials");
			if (passwordGuid == null || passwordGuid == Guid.Empty || passwordGuid == default(Guid))
				throw new SecurityException("InvalidCredentials");
			var authenticated = IntegratedService.Authenticate(userNameGuid.Value, passwordGuid.Value, IntegratedServiceUser.ServiceTypes.LMS);
			if (!authenticated)
				throw new SecurityException("InvalidCredentials");
			var ipAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
			var loginHistory = IntegratedService.Login(userNameGuid.Value, IntegratedServiceUser.ServiceTypes.LMS, ipAddress);
			if (loginHistory == null)
				throw new SecurityException("IPAddressNotAuthorized");
			return loginHistory;
		}

		public static User VerifySession(Guid userName, Guid password, Guid key)
		{
			Authenticate(userName.ToString(), password.ToString());
			var user = Authentication.Verify(key);
			switch (user)
			{
				case Authentication.Student student:
					return new User
					{
						ID = student.StudentID,
						UserType = nameof(Authentication.Student),
						Permissions = new List<Permission>(),
						Name = student.Name,
						Email = student.Email,
					};
				case Authentication.FacultyMember facultyMember:
					return new User
					{
						ID = facultyMember.FacultyMemberID,
						UserType = nameof(Authentication.FacultyMember),
						Permissions = new List<Permission>(),
						Name = facultyMember.Name,
						Email = facultyMember.Email
					};
				case Authentication.Executive executive:
					using (var aspireContext = new AspireContext())
					{
						var permissions = aspireContext.Users.Where(u => u.UserID == executive.UserID && u.IsDeleted == false && u.Status == (byte)Model.Entities.User.Statuses.Active)
							.SelectMany(u => u.UserRoles1)
							.Where(r => r.UserType == (short)UserTypes.Executive && r.InstituteID != null && r.IsDeleted == false && r.Status == (byte)Model.Entities.UserRole.Statuses.Active)
							.SelectMany(r => r.UserRoleModules)
							.Where(m => m.Module == (byte)AspireModules.LMS)
							.Select(m => new Permission
							{
								InstituteID = m.UserRole.InstituteID.Value,
								DepartmentID = m.DepartmentID,
								ProgramID = m.ProgramID
							}).ToList();
						foreach (var permission in permissions.FindAll(p => p.DepartmentID == null && p.ProgramID == null))
							permissions.RemoveAll(p => p.InstituteID == permission.InstituteID && (p.DepartmentID != null || p.ProgramID != null));

						foreach (var permission in permissions.FindAll(p => p.DepartmentID != null && p.ProgramID == null))
							permissions.RemoveAll(p => p.InstituteID == permission.InstituteID && p.DepartmentID == permission.DepartmentID && p.ProgramID != null);

						return new User
						{
							ID = executive.UserID,
							UserType = nameof(Authentication.Executive),
							Permissions = permissions,
							Name = executive.Name,
							Email = executive.Email
						};
					}
				default:
					throw new ArgumentOutOfRangeException(nameof(user), user, null);
			}
		}

		public static string Ping(Guid userName, Guid password)
		{
			Authenticate(userName.ToString(), password.ToString());
			return "Success";
		}

		public static List<Semester> GetSemesters1(Guid userName, Guid password)
		{
			Authenticate(userName.ToString(), password.ToString());
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.Semesters
					.Where(s => s.SemesterID >= 20193)
					.OrderByDescending(s => s.SemesterID)
					.ToList().Select(s => new Semester
					{
						SemesterID = s.SemesterID,
						SemesterType = s.SemesterTypeEnum.ToFullName(),
						Year = s.Year,
						SemesterFullName = s.ToString(),
					}).ToList();
			}
		}

		public static List<Institute> GetInstitutes2(Guid userName, Guid password)
		{
			Authenticate(userName.ToString(), password.ToString());
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.Institutes.OrderBy(i => i.InstituteID).Select(i => new Institute
				{
					InstituteID = i.InstituteID,
					InstituteName = i.InstituteName,
					InstituteShortName = i.InstituteShortName,
					InstituteAlias = i.InstituteAlias,
				}).ToList();
			}
		}

		public static List<Department> GetDepartments3(Guid userName, Guid password)
		{
			Authenticate(userName.ToString(), password.ToString());
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.Departments.OrderBy(d => d.InstituteID).ThenBy(d => d.DepartmentID).Select(d => new Department
				{
					DepartmentID = d.DepartmentID,
					InstituteID = d.InstituteID,
					DepartmentName = d.DepartmentName,
					DepartmentShortName = d.DepartmentShortName,
					DepartmentAlias = d.DepartmentAlias
				}).ToList();
			}
		}

		public static List<Program> GetPrograms4(Guid userName, Guid password)
		{
			Authenticate(userName.ToString(), password.ToString());
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.Programs.OrderBy(p => p.InstituteID).ThenBy(p => p.DepartmentID).ThenBy(p => p.ProgramID).Select(p => new Program
				{
					ProgramID = p.ProgramID,
					InstituteID = p.InstituteID,
					DepartmentID = p.DepartmentID,
					ProgramName = p.ProgramName,
					ProgramShortName = p.ProgramShortName,
					ProgramAlias = p.ProgramAlias
				}).ToList();
			}
		}

		public static List<FacultyMember> GetFacultyMembers5(Guid userName, Guid password, short offeredSemesterID)
		{
			Authenticate(userName.ToString(), password.ToString());
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.OfferedCourses
					.Where(oc => oc.SemesterID == offeredSemesterID && oc.FacultyMemberID != null)
					.Select(oc => new FacultyMember
					{
						FacultyMemberID = oc.FacultyMember.FacultyMemberID,
						InstituteID = oc.FacultyMember.InstituteID,
						DepartmentID = oc.FacultyMember.DepartmentID,
						Name = oc.FacultyMember.Name,
						Email = oc.FacultyMember.Email
					})
					.OrderBy(fm => fm.InstituteID).ThenBy(fm => fm.FacultyMemberID)
					.Distinct()
					.ToList();
			}
		}

		public static List<Student> GetStudents6(Guid userName, Guid password, short offeredSemesterID)
		{
			Authenticate(userName.ToString(), password.ToString());
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.RegisteredCourses.Where(rc => rc.OfferedCours.SemesterID == offeredSemesterID && rc.DeletedDate == null)
					.Select(rc => new
					{
						rc.Student.StudentID,
						rc.Student.Enrollment,
						rc.Student.PersonalEmail,
						rc.Student.UniversityEmail,
						StudentStatus = (Model.Entities.Student.Statuses?)rc.Student.Status,
						rc.Student.Name,
						rc.Student.AdmissionOpenProgram.ProgramID,
						IntakeSemesterID = rc.Student.AdmissionOpenProgram.SemesterID
					}).Distinct().OrderBy(p => p.ProgramID).ThenByDescending(p => p.IntakeSemesterID).ThenBy(p => p.Enrollment).ToList().Select(s => new BL.Core.IntegrationServices.LMS.Student
					{
						StudentID = s.StudentID,
						Enrollment = s.Enrollment,
						PersonalEmail = s.PersonalEmail,
						UniversityEmail = s.UniversityEmail,
						StudentStatus = s.StudentStatus?.ToFullName(),
						Name = s.Name,
						ProgramID = s.ProgramID,
						IntakeSemesterID = s.IntakeSemesterID
					}).ToList();
			}
		}

		public static List<OfferedCourse> GetOfferedCourses7(Guid userName, Guid password, short offeredSemesterID)
		{
			Authenticate(userName.ToString(), password.ToString());
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.OfferedCourses.Where(oc => oc.SemesterID == offeredSemesterID)
					.Select(oc => new
					{
						oc.OfferedCourseID,
						oc.SemesterID,
						oc.Cours.AdmissionOpenProgram.ProgramID,
						oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						SemesterNoEnum = (SemesterNos)oc.SemesterNo,
						SectionEnum = (Sections)oc.Section,
						ShiftEnum = (Shifts)oc.Shift,
						oc.Cours.CourseCode,
						oc.Cours.Title,
						oc.Cours.CreditHours,
						oc.FacultyMemberID,
					}).OrderBy(oc => oc.ProgramAlias).ThenBy(oc => oc.SemesterNoEnum).ThenBy(oc => oc.SectionEnum).ThenBy(oc => oc.ShiftEnum).ToList().Select(oc => new OfferedCourse
					{
						OfferedCourseID = oc.OfferedCourseID,
						FacultyMemberID = oc.FacultyMemberID,
						SemesterID = oc.SemesterID,
						ProgramID = oc.ProgramID,
						Class = AspireFormats.GetClassName(oc.ProgramAlias, oc.SemesterNoEnum, oc.SectionEnum, oc.ShiftEnum),
						SemesterNo = oc.SemesterNoEnum.ToFullName(),
						Section = oc.SectionEnum.ToFullName(),
						Shift = oc.ShiftEnum.ToFullName(),
						CourseCode = oc.CourseCode,
						Title = oc.Title,
						CreditHours = oc.CreditHours
					}).ToList();
			}
		}

		public static List<RegisteredCourse> GetRegisteredCourses8(Guid userName, Guid password, short offeredSemesterID)
		{
			Authenticate(userName.ToString(), password.ToString());
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.RegisteredCourses.Where(rc => rc.OfferedCours.SemesterID == offeredSemesterID && rc.DeletedDate == null)
					.Select(rc => new
					{
						rc.RegisteredCourseID,
						rc.StudentID,
						rc.OfferedCourseID,
						StatusEnum = (RegisteredCours.Statuses?)rc.Status,
						FreezedStatusEnum = (FreezedStatuses?)rc.FreezedStatus
					}).OrderByDescending(rc => rc.OfferedCourseID).ThenBy(rc => rc.RegisteredCourseID).ToList().Select(rc => new RegisteredCourse
					{
						RegisteredCourseID = rc.RegisteredCourseID,
						StudentID = rc.StudentID,
						OfferedCourseID = rc.OfferedCourseID,
						Status = rc.StatusEnum?.ToShortName(),
						FreezedStatus = rc.FreezedStatusEnum?.ToShortName()
					}).ToList();
			}
		}
	}
}
