﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.LMS
{
	[DataContract]
	public class User
	{
		[DataMember]
		public int ID { get; set; }
		[DataMember]
		public string UserType { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public string Email { get; set; }
		[DataMember]
		public List<Permission> Permissions { get; set; }
	}
}
