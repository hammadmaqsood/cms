﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.LMS
{
	[DataContract]
	public class OfferedCourse
	{
		[DataMember]
		public int OfferedCourseID { get; set; }
		[DataMember]
		public int? FacultyMemberID { get; set; }
		[DataMember]
		public short SemesterID { get; set; }
		[DataMember]
		public int ProgramID { get; set; }
		[DataMember]
		public string Class { get; set; }
		[DataMember]
		public string SemesterNo { get; set; }
		[DataMember]
		public string Section { get; set; }
		[DataMember]
		public string Shift { get; set; }
		[DataMember]
		public string CourseCode { get; set; }
		[DataMember]
		public string Title { get; set; }
		[DataMember]
		public decimal CreditHours { get; set; }
	}
}