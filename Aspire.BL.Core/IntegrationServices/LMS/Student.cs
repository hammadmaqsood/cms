﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.LMS
{
	[DataContract]
	public class Student
	{
		[DataMember]
		public int StudentID { get; set; }
		[DataMember]
		public string Enrollment { get; set; }
		[DataMember]
		public string PersonalEmail { get; set; }
		[DataMember]
		public string UniversityEmail { get; set; }
		[DataMember]
		public string StudentStatus { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public short IntakeSemesterID { get; set; }
		[DataMember]
		public int ProgramID { get; set; }
	}
}