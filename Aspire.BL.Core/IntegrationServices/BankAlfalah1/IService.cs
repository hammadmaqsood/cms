using System;
using System.ServiceModel;

namespace Aspire.BL.Core.IntegrationServices.BankAlfalah1
{
	[ServiceContract]
	public interface IService
	{
		[OperationContract]
		FeeChallan GetChallanDetails(decimal challanNo);

		PayChallanResult PayChallan(decimal fullChallanNo, TransactionChannels transactionChannel, PaymentTypes paymentType, int amount, string transactionReferenceID, DateTime transactionDateTime, string accountNo, string accountTitle, string branchCode, string branchName);
	}
}