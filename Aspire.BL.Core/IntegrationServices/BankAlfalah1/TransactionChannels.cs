﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.BankAlfalah1
{
	[DataContract]
	public enum TransactionChannels
	{
		[EnumMember] ATM = 1,
		[EnumMember] MobileBanking = 2,
		[EnumMember] Branch = 3,
		[EnumMember] InternetBanking = 4,
	}
}