﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.BankAlfalah1
{
	[DataContract]
	public enum PaymentTypes
	{
		[EnumMember] Cash = 1,
		[EnumMember] Account = 2,
		[EnumMember] DemandDraft = 3,
		[EnumMember] PayOrder = 4,
		[EnumMember] Cheque = 5
	}
}