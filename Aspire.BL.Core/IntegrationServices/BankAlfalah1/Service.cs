﻿using Aspire.BL.Core.FeeManagement.Common;
using Aspire.BL.Core.IntegrationServices.Common.FeeManagement;
using Aspire.BL.Core.Logs.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.ServiceModel;

namespace Aspire.BL.Core.IntegrationServices.BankAlfalah1
{
	public class Service : IntegrationBaseService, IService
	{
		internal static string BankName => AspireConstants.BankAlfalahName;

		public Service() : base(IntegratedServiceUser.ServiceTypes.BankAlfalah)
		{
#if !DEBUG
			throw new System.NotImplementedException();
#endif
		}

		public static void Configure(ServiceConfiguration config)
		{
			ConfigureService<IService>(config, IntegratedServiceUser.ServiceTypes.BankAlfalah);
		}

		public static readonly InstituteBankAccount.AccountTypes[] AccountTypeEnums = { InstituteBankAccount.AccountTypes.Online, InstituteBankAccount.AccountTypes.ManualOnline };

		private FeeChallan GetChallanDetails(decimal fullChallanNo, out string responseJSON, out bool errorOccurred)
		{
			try
			{
				errorOccurred = false;
				var rawFeeChallan = ServiceHelpers.GetRawFeeChallanDetails(BankName, fullChallanNo, false, false, AccountTypeEnums, out var paymentFailureReason);
				if (rawFeeChallan == null)
				{
					responseJSON = null;
					return null;
				}
				var feeChallan = FeeChallan.GetFeeChallan(rawFeeChallan, paymentFailureReason);
				responseJSON = feeChallan.ToJsonString();
				return feeChallan;
			}
			catch (Exception exc)
			{
				errorOccurred = true;
				responseJSON = new
				{
					exc.Source,
					exc.Message,
					exc.StackTrace,
					InnerException = exc.InnerException == null
						? null
						: new
						{
							exc.InnerException.Source,
							exc.InnerException.Message,
							exc.InnerException.StackTrace
						},
				}.ToJsonString();
				ErrorHandler.LogException(this.LoginHistory.UserLoginHistoryID, exc, -1, typeof(Service).FullName, null, this.LoginHistory.IPAddress);
				return null;
			}
		}

		public FeeChallan GetChallanDetails(decimal fullChallanNo)
		{
			var parameters = new { fullChallanNo }.ToJsonString();
			var attemptDate = DateTime.Now;
			var feeChallan = GetChallanDetails(fullChallanNo, out var responseJSON, out var errorOccurred);
			AddIntegrationServiceLog(typeof(Service), nameof(this.GetChallanDetails), attemptDate, this.LoginHistory.UserLoginHistoryID, this.LoginHistory.IPAddress, parameters, responseJSON);
			if (errorOccurred)
				throw new FaultException("Unknown error occurred. Please contact IT Directorate of Bahria University.");
			return feeChallan;
		}

		private PayChallanResult.Statuses PayChallan(decimal fullChallanNo, TransactionChannels transactionChannel, PaymentTypes paymentType, int amount, string transactionReferenceID, DateTime transactionDateTime, string accountNo, string accountTitle, string branchCode, string branchName, out string responseJSON)
		{
			try
			{
				var challanNo = AspireFormats.ChallanNo.TryParse(fullChallanNo);
				if (challanNo == null)
				{
					responseJSON = new { PayChallanResult.Statuses.InvalidChallanNoFormat }.ToJsonString();
					return PayChallanResult.Statuses.InvalidChallanNoFormat;
				}
				if (amount <= 0)
				{
					responseJSON = new { PayChallanResult.Statuses.AmountMustBeGreaterThanZero }.ToJsonString();
					return PayChallanResult.Statuses.AmountMustBeGreaterThanZero;
				}
				if (transactionDateTime.Date != DateTime.Today || transactionDateTime > DateTime.Now.AddMinutes(10))
				{
					responseJSON = new { PayChallanResult.Statuses.TransactionDateTimeCannotBeFutureDateTime }.ToJsonString();
					return PayChallanResult.Statuses.TransactionDateTimeCannotBeFutureDateTime;
				}
				AccountTransaction.TransactionChannels transactionChannelEnum;
				AccountTransaction.PaymentTypes paymentTypeEnum;
				switch (transactionChannel)
				{
					case TransactionChannels.ATM:
						transactionChannelEnum = AccountTransaction.TransactionChannels.ATM;
						break;
					case TransactionChannels.MobileBanking:
						transactionChannelEnum = AccountTransaction.TransactionChannels.MobileBanking;
						break;
					case TransactionChannels.Branch:
						transactionChannelEnum = AccountTransaction.TransactionChannels.Branch;
						break;
					case TransactionChannels.InternetBanking:
						transactionChannelEnum = AccountTransaction.TransactionChannels.InternetBanking;
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(transactionChannel), transactionChannel, null);
				}
				switch (paymentType)
				{
					case PaymentTypes.Cash:
						paymentTypeEnum = AccountTransaction.PaymentTypes.Cash;
						break;
					case PaymentTypes.Account:
						paymentTypeEnum = AccountTransaction.PaymentTypes.Account;
						break;
					case PaymentTypes.DemandDraft:
						paymentTypeEnum = AccountTransaction.PaymentTypes.DemandDraft;
						break;
					case PaymentTypes.PayOrder:
						paymentTypeEnum = AccountTransaction.PaymentTypes.PayOrder;
						break;
					case PaymentTypes.Cheque:
						paymentTypeEnum = AccountTransaction.PaymentTypes.Cheque;
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(paymentType), paymentType, null);
				}
				var paymentFailureReason = ServiceHelpers.PayFeeChallan(challanNo.Value, BankName, AccountTransaction.Services.BankAlfalah, transactionChannelEnum, paymentTypeEnum, AccountTypeEnums, amount, transactionReferenceID, accountNo, accountTitle, branchCode, branchName, transactionDateTime, null, true, this.LoginHistory.UserLoginHistoryID);
				responseJSON = new { paymentFailureReason }.ToJsonString();
				switch (paymentFailureReason)
				{
					case PaymentFailureReasons.None:
						return PayChallanResult.Statuses.Success;
					case PaymentFailureReasons.InvalidChallanNoFormat:
						throw new InvalidOperationException();
					case PaymentFailureReasons.ChallanNotFound:
						return PayChallanResult.Statuses.ChallanNotFound;
					case PaymentFailureReasons.FeeChallanAlreadyPaid:
						return PayChallanResult.Statuses.FeeChallanAlreadyPaid;
					case PaymentFailureReasons.DueDateIsPassed:
						return PayChallanResult.Statuses.DueDateIsPassed;
					case PaymentFailureReasons.AmountMismatched:
						return PayChallanResult.Statuses.AmountMismatched;
					case PaymentFailureReasons.AmountMustBeGreaterThanZero:
						return PayChallanResult.Statuses.AmountMustBeGreaterThanZero;
					case PaymentFailureReasons.InstituteBankAccountNotFound:
						return PayChallanResult.Statuses.InstituteBankAccountNotFound;
					default:
						throw new ArgumentOutOfRangeException(nameof(paymentFailureReason), paymentFailureReason, null);
				}
			}
			catch (Exception exc)
			{
				responseJSON = new
				{
					exc.Source,
					exc.Message,
					exc.StackTrace,
					InnerException = exc.InnerException == null
					   ? null
					   : new
					   {
						   exc.InnerException.Source,
						   exc.InnerException.Message,
						   exc.InnerException.StackTrace
					   },
				}.ToJsonString();
				ErrorHandler.LogException(this.LoginHistory.UserLoginHistoryID, exc, -1, typeof(Service).FullName, null, this.LoginHistory?.IPAddress);
				return PayChallanResult.Statuses.UnknownError;
			}
		}

		private PayChallanResult PayChallan(string methodName, decimal fullChallanNo, TransactionChannels transactionChannel, PaymentTypes paymentType, int amount, string transactionReferenceID, DateTime transactionDateTime, string accountNo, string accountTitle, string branchCode, string branchName)
		{
			var attemptDate = DateTime.Now;
			var parameters = new { fullChallanNo, transactionChannel, paymentType, amount, transactionReferenceID, transactionDateTime, accountNo, accountTitle, branchCode, branchName }.ToJsonString();
			var status = PayChallan(fullChallanNo, transactionChannel, paymentType, amount, transactionReferenceID, transactionDateTime, accountNo, accountTitle, branchCode, branchName, out var responseJSON);
			AddIntegrationServiceLog(typeof(Service), methodName, attemptDate, this.LoginHistory.UserLoginHistoryID, this.LoginHistory.IPAddress, parameters, responseJSON);
			return PayChallanResult.New(status);
		}

		public PayChallanResult PayChallan(decimal fullChallanNo, TransactionChannels transactionChannel, PaymentTypes paymentType, int amount, string transactionReferenceID, DateTime transactionDateTime, string accountNo, string accountTitle, string branchCode, string branchName)
		{
			return PayChallan(nameof(this.PayChallan), fullChallanNo, transactionChannel, paymentType, amount, transactionReferenceID, transactionDateTime, null, null, branchCode, branchName);
		}
	}
}
