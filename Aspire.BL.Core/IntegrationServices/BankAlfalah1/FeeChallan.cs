﻿using Aspire.BL.Core.FeeManagement.Common;
using System;
using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.BankAlfalah1
{
	[DataContract]
	public sealed class FeeChallan
	{
		[DataContract]
		public enum Statuses
		{
			[EnumMember]
			CanBePaid,
			AlreadyPaid,
			DueDateIsPassed,
			AmountMustBeGreaterThanZero,
			BankAccountNotFound,
		}

		[DataMember]
		public decimal ChallanNo { get; set; }
		[DataMember]
		public string ApplicationNoOrEnrollment { get; set; }
		[DataMember]
		public string BankAlfalahAccountNo { get; set; }
		[DataMember]
		public string BankAlfalahAccountTitle { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public int Amount { get; set; }
		[DataMember]
		public DateTime DueDate { get; set; }
		[DataMember]
		public DateTime? DepositDate { get; set; }
		[DataMember]
		public Statuses Status { get; set; }
		[DataMember]
		public string Message { get; set; }

		internal static FeeChallan GetFeeChallan(Helpers.RawFeeChallan rawFeeChallan, PaymentFailureReasons paymentFailureReason)
		{
			if (rawFeeChallan == null)
				throw new ArgumentNullException(nameof(rawFeeChallan));
			var feeChallan = new FeeChallan
			{
				ChallanNo = rawFeeChallan.FullChallanNo,
				ApplicationNoOrEnrollment = rawFeeChallan.Enrollment ?? rawFeeChallan.ApplicationNo.ToString(),
				Name = rawFeeChallan.Name,
				Amount = rawFeeChallan.Amount,
				DueDate = rawFeeChallan.DueDate,
				BankAlfalahAccountTitle = rawFeeChallan.BankAccountTitle,
				BankAlfalahAccountNo = rawFeeChallan.BankAccountNo,
				DepositDate = rawFeeChallan.DepositDate,
			};
			switch (paymentFailureReason)
			{
				case PaymentFailureReasons.InvalidChallanNoFormat:
				case PaymentFailureReasons.ChallanNotFound:
				case PaymentFailureReasons.AmountMismatched:
					throw new InvalidOperationException();
				case PaymentFailureReasons.AmountMustBeGreaterThanZero:
					feeChallan.Status = Statuses.AmountMustBeGreaterThanZero;
					break;
				case PaymentFailureReasons.FeeChallanAlreadyPaid:
					feeChallan.Status = Statuses.AlreadyPaid;
					break;
				case PaymentFailureReasons.DueDateIsPassed:
					feeChallan.Status = Statuses.DueDateIsPassed;
					break;
				case PaymentFailureReasons.InstituteBankAccountNotFound:
					feeChallan.Status = Statuses.BankAccountNotFound;
					break;
				case PaymentFailureReasons.None:
					feeChallan.Status = Statuses.CanBePaid;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(paymentFailureReason), paymentFailureReason, null);
			}
			switch (feeChallan.Status)
			{
				case Statuses.CanBePaid:
					feeChallan.Message = "Fee Challan can be paid.";
					break;
				case Statuses.AlreadyPaid:
					feeChallan.Message = "Fee Challan is already paid.";
					break;
				case Statuses.DueDateIsPassed:
					feeChallan.Message = "Fee Challan cannot be paid because Due Date has been passed.";
					break;
				case Statuses.BankAccountNotFound:
					feeChallan.Message = "Fee Challan cannot be paid because Account Information is missing. Please contact IT Directorate of Bahria University.";
					break;
				case Statuses.AmountMustBeGreaterThanZero:
					feeChallan.Message = "Fee Challan cannot be paid because amount is zero or negative.";
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			return feeChallan;
		}
	}
}