﻿using System;
using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.BankAlfalah1
{
	[DataContract]
	public sealed class PayChallanResult
	{
		[DataContract]
		public enum Statuses
		{
			[EnumMember] Success,
			[EnumMember] InvalidChallanNoFormat,
			[EnumMember] AmountMustBeGreaterThanZero,
			[EnumMember] TransactionDateTimeCannotBeFutureDateTime,
			[EnumMember] ChallanNotFound,
			[EnumMember] FeeChallanAlreadyPaid,
			[EnumMember] DueDateIsPassed,
			[EnumMember] AmountMismatched,
			[EnumMember] InstituteBankAccountNotFound,
			[EnumMember] UnknownError,
		}

		[DataMember]
		public Statuses Status { get; set; }
		[DataMember]
		public string DetailedMessage { get; set; }

		internal static PayChallanResult New(Statuses status)
		{
			var getDetailedMessage = new Func<string>(() =>
			{
				switch (status)
				{
					case Statuses.Success:
						return "Success";
					case Statuses.InvalidChallanNoFormat:
						return "Format of the Challan No is not correct. It must be 11-12 digit long number.";
					case Statuses.AmountMustBeGreaterThanZero:
						return "Amount must be greater than zero.";
					case Statuses.TransactionDateTimeCannotBeFutureDateTime:
						return "Transaction cannot be performed for future date/time.";
					case Statuses.ChallanNotFound:
						return "Challan does not exists.";
					case Statuses.FeeChallanAlreadyPaid:
						return "Challan is already marked paid.";
					case Statuses.DueDateIsPassed:
						return "Challan cannot be paid because Due Date has been passed.";
					case Statuses.AmountMismatched:
						return "Challan cannot be paid because amount is mismatched with challan.";
					case Statuses.InstituteBankAccountNotFound:
						return "Account is not configured in Bahria University Application. Please contact IT Directorate of the Bahria University.";
					case Statuses.UnknownError:
						return "Unknown error occurred in Bahria University Application. Please try again, it may be temporary. If it persists please contact IT Directorate of the Bahria University.";
					default:
						throw new ArgumentOutOfRangeException(nameof(status), status, null);
				}
			});
			return new PayChallanResult
			{
				Status = status,
				DetailedMessage = getDetailedMessage(),
			};
		}
	}
}