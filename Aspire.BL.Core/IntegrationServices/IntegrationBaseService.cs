﻿using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.Xml;

namespace Aspire.BL.Core.IntegrationServices
{
	public abstract class IntegrationBaseService
	{
		internal LoginManagement.IntegratedService.LoginHistory LoginHistory { get; }
		private bool UserAuthenticated { get; }

		private IntegrationBaseService()
		{
		}

		protected internal IntegrationBaseService(IntegratedServiceUser.ServiceTypes serviceTypeEnum) : this()
		{
			var username = OperationContext.Current.ServiceSecurityContext.PrimaryIdentity.Name.ToGuid();
			var ipAddress = ((RemoteEndpointMessageProperty)OperationContext.Current.IncomingMessageProperties[RemoteEndpointMessageProperty.Name]).Address;
			var loginHistory = LoginManagement.IntegratedService.Login(username, serviceTypeEnum, ipAddress);
			if (loginHistory == null)
			{
				this.UserAuthenticated = false;
				this.LoginHistory = null;
				throw new SecurityAccessDeniedException($"IP Address \"{ipAddress}\" is not authorized.");
			}
			this.UserAuthenticated = true;
			this.LoginHistory = loginHistory;
		}

		protected void CheckAuthorization()
		{
			if (this.UserAuthenticated)
				return;
			throw new SecurityAccessDeniedException();
		}

		protected static void ConfigureService<T>(ServiceConfiguration config, IntegratedServiceUser.ServiceTypes serviceTypeEnum) where T : class
		{
			var contractType = typeof(T);
			if (!contractType.IsInterface)
				throw new InvalidOperationException("Type must be interface.");
			var binding = new WSHttpBinding
			{
				TransactionFlow = false,
				MaxReceivedMessageSize = int.MaxValue,
				ReaderQuotas = new XmlDictionaryReaderQuotas { MaxStringContentLength = int.MaxValue },
				Security = new WSHttpSecurity
				{
					Mode = SecurityMode.TransportWithMessageCredential,
					Transport = new HttpTransportSecurity { ClientCredentialType = HttpClientCredentialType.None },
					Message = new NonDualMessageSecurityOverHttp { ClientCredentialType = MessageCredentialType.UserName },
				},
			};

			config.AddServiceEndpoint(contractType, binding, string.Empty);
			config.Description.Behaviors.Add(new ServiceMetadataBehavior { HttpGetEnabled = false, HttpsGetEnabled = true, });
			config.Description.Behaviors.Add(new ServiceDebugBehavior { IncludeExceptionDetailInFaults = false });
			config.Description.Behaviors.Add(new ServiceCredentials
			{
				UserNameAuthentication =
				{
					CustomUserNamePasswordValidator = new ServiceUserNamePasswordValidator(serviceTypeEnum),
					UserNamePasswordValidationMode = UserNamePasswordValidationMode.Custom,
				},
			});
		}

		internal static void AddIntegrationServiceLog(Type serviceType, string methodName, DateTime attemptDate, int userLoginHistoryID, string ipAddress, string parameters, string response)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.IntegratedServiceLogs.Add(new IntegratedServiceLog
				{
					UserLoginHistoryID = userLoginHistoryID,
					Service = serviceType.FullName,
					MethodName = methodName,
					AttemptDate = attemptDate,
					Parameters = parameters,
					Response = response,
				});
				aspireContext.SaveChanges(userLoginHistoryID, false);
			}
		}
	}
}