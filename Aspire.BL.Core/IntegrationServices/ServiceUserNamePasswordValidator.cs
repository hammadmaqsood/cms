﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities;
using System;
using System.IdentityModel.Selectors;
using System.ServiceModel;
using System.Threading.Tasks;

namespace Aspire.BL.Core.IntegrationServices
{
	public class ServiceUserNamePasswordValidator : UserNamePasswordValidator
	{
		public IntegratedServiceUser.ServiceTypes ServiceType { get; }
		public ServiceUserNamePasswordValidator(IntegratedServiceUser.ServiceTypes serviceTypeEnum)
		{
			this.ServiceType = serviceTypeEnum;
		}

		public override void Validate(string userName, string password)
		{
			var userNameGuid = userName.TryToGuid();
			var passwordGuid = password.TryToGuid();
			if (userNameGuid == null || userNameGuid == Guid.Empty || userNameGuid == default(Guid))
				throw new ArgumentException("Invalid Username.", nameof(userName));
			if (passwordGuid == null || passwordGuid == Guid.Empty || passwordGuid == default(Guid))
				throw new ArgumentException("Invalid Password.", nameof(password));
			var authenticated = LoginManagement.IntegratedService.Authenticate(userNameGuid.Value, passwordGuid.Value, this.ServiceType);
			if (!authenticated)
				throw new FaultException("Unknown Username or Incorrect Password");
		}
	}
}
