﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.HR
{
	[DataContract]
	public class Semester
	{
		[DataMember] public short SemesterID { get; set; }
		[DataMember] public string SemesterName { get; set; }
	}
}
