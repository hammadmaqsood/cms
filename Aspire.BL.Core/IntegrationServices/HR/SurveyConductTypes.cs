﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.HR
{
	[DataContract]
	public enum SurveyConductTypes : byte
	{
		[EnumMember] BeforeMidTerm = Model.Entities.SurveyConduct.SurveyConductTypes.BeforeMidTerm,
		[EnumMember] BeforeFinalTerm = Model.Entities.SurveyConduct.SurveyConductTypes.BeforeFinalTerm,
	}
}