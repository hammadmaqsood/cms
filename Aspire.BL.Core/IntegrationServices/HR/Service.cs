﻿using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.ServiceModel;

namespace Aspire.BL.Core.IntegrationServices.HR
{
	public abstract class Service : IntegrationBaseService, IService
	{
		protected Service() : base(IntegratedServiceUser.ServiceTypes.HR)
		{
		}

		public static void Configure(ServiceConfiguration config)
		{
			ConfigureService<IService>(config, IntegratedServiceUser.ServiceTypes.HR);
		}

		public List<Semester> GetSemesters(string cnic)
		{
			cnic = cnic.TrimAndMakeItNullIfEmpty();
			if (cnic == null)
				throw new ArgumentNullException(nameof(cnic));
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.OfferedCourses
					.Where(oc => oc.FacultyMember.CNIC == cnic)
					.Where(oc => this.LoginHistory.InstituteID == null || oc.Cours.AdmissionOpenProgram.Program.InstituteID == this.LoginHistory.InstituteID.Value)
					.Select(oc => new
					{
						oc.SemesterID
					})
					.Distinct()
					.OrderByDescending(s => s.SemesterID)
					.ToList()
					.Select(s => new Semester
					{
						SemesterID = s.SemesterID,
						SemesterName = s.SemesterID.ToSemesterString()
					})
					.ToList();
			}
		}

		public List<Course> GetCourses(string cnic, short semesterID)
		{
			cnic = cnic.TrimAndMakeItNullIfEmpty();
			if (cnic == null)
				throw new ArgumentNullException(nameof(cnic));
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.OfferedCourses
					.Where(oc => oc.SemesterID == semesterID)
					.Where(oc => oc.FacultyMember.CNIC == cnic)
					.Where(oc => this.LoginHistory.InstituteID == null || oc.Cours.AdmissionOpenProgram.Program.InstituteID == this.LoginHistory.InstituteID.Value)
					.Select(oc => new
					{
						oc.OfferedCourseID,
						oc.Cours.Title,
						oc.Cours.AdmissionOpenProgram.Program.Institute.InstituteAlias,
						oc.Cours.AdmissionOpenProgram.Program.Department.DepartmentAlias,
						oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						oc.SemesterNo,
						oc.Section,
						oc.Shift,
						oc.Visiting,
						oc.FacultyMember.Name,
					})
					.ToList()
					.Select(oc => new Course
					{
						OfferedCourseID = oc.OfferedCourseID,
						Title = oc.Title,
						InstituteAlias = oc.InstituteAlias,
						DepartmentAlias = oc.DepartmentAlias,
						ProgramAlias = oc.ProgramAlias,
						SemesterNo = ((SemesterNos)oc.SemesterNo).ToFullName(),
						Section = ((Sections)oc.Section).ToFullName(),
						Shift = ((Shifts)oc.Shift).ToFullName(),
						Class = AspireFormats.GetClassName(oc.ProgramAlias, oc.SemesterNo, oc.Section, oc.Shift),
						Visiting = oc.Visiting.ToYesNo(),
						FacultyMemberName = oc.Name,
					}).ToList();
			}
		}

		public abstract byte[] GetTeacherEvaluation(string cnic, int offeredCourseID, SurveyConductTypes surveyConductTypeEnum);

		protected QualityAssurance.Executive.Reports.Survey.ReportDataSet GetTeacherEvaluation(string cnic, int offeredCourseID, SurveyConduct.SurveyConductTypes surveyConductType)
		{
			cnic = cnic.TrimAndMakeItNullIfEmpty();
			if (cnic == null)
				throw new ArgumentNullException(nameof(cnic));
			using (var aspireContext = new AspireContext())
			{
				aspireContext.Configuration.AutoDetectChangesEnabled = false;

				var offeredCourse = aspireContext.OfferedCourses
					.Where(oc => oc.OfferedCourseID == offeredCourseID)
					.Where(oc => oc.FacultyMember.CNIC == cnic)
					.Where(oc => this.LoginHistory.InstituteID == null || oc.Cours.AdmissionOpenProgram.Program.InstituteID == this.LoginHistory.InstituteID.Value)
					.Select(oc => new
					{
						oc.OfferedCourseID,
						oc.SemesterID,
						oc.Cours.Title,
						oc.Cours.AdmissionOpenProgram.Program.InstituteID,
						oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
						oc.Cours.AdmissionOpenProgram.Program.Department.DepartmentName,
						oc.Cours.AdmissionOpenProgram.ProgramID,
						oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						oc.SemesterNo,
						oc.Section,
						oc.Shift,
						oc.FacultyMember.FacultyMemberID,
						oc.FacultyMember.Name,
					})
					.SingleOrDefault();
				if (offeredCourse == null)
					return null;

				var survey = aspireContext.Surveys
					.Where(s => s.InstituteID == offeredCourse.InstituteID)
					.Where(s => s.SurveyType == Survey.SurveyTypeStudentCourseWise)
					.Where(s => s.SurveyName == "Teacher Evaluation Form (Survey Form-10)")
					.Include(s => s.SurveyQuestionGroups.Select(g => g.SurveyQuestions.Select(q => q.SurveyQuestionOptions)))
					.Include(s => s.Institute)
					.SingleOrDefault();
				if (survey == null)
					return null;

				var surveyConduct = aspireContext.SurveyConducts
					.Where(sc => sc.SurveyID == survey.SurveyID)
					.Where(sc => sc.SemesterID == offeredCourse.SemesterID && sc.SurveyConductType == (byte)surveyConductType)
					.Select(sc => new
					{
						sc.SurveyConductID
					}).SingleOrDefault();
				if (surveyConduct == null)
					return null;

				var surveyUserAnswers = aspireContext.RegisteredCourses
					.Where(rc => rc.OfferedCourseID == offeredCourse.OfferedCourseID && rc.DeletedDate == null)
					.SelectMany(rc => rc.SurveyUsers)
					.Where(su => su.SurveyConductID == surveyConduct.SurveyConductID)
					.SelectMany(su => su.SurveyUserAnswers)
					.AsNoTracking()
					.ToList();

				return new QualityAssurance.Executive.Reports.Survey.ReportDataSet(survey, surveyUserAnswers, offeredCourse.SemesterID, offeredCourse.DepartmentID, offeredCourse.DepartmentName, offeredCourse.ProgramID, offeredCourse.ProgramAlias, (SemesterNos)offeredCourse.SemesterNo, (Sections)offeredCourse.Section, (Shifts)offeredCourse.Shift, offeredCourse.FacultyMemberID, offeredCourse.Name, offeredCourse.OfferedCourseID, offeredCourse.Title);
			}
		}

		public List<TeacherEvaluation> GetTeacherEvaluations(string cnic, SurveyConductTypes? surveyConductTypeEnum, short? fromSemesterID, short? toSemesterID)
		{
			cnic = cnic.TrimAndMakeItNullIfEmpty();
			if (cnic == null)
				throw new ArgumentNullException(nameof(cnic));
			using (var aspireContext = new AspireContext())
			{
				aspireContext.Configuration.AutoDetectChangesEnabled = false;

				var offeredCourses = aspireContext.OfferedCourses
					.Where(oc => fromSemesterID == null || oc.SemesterID >= fromSemesterID.Value)
					.Where(oc => toSemesterID == null || oc.SemesterID <= toSemesterID.Value)
					.Where(oc => oc.FacultyMember.CNIC == cnic)
					.Where(oc => this.LoginHistory.InstituteID == null || oc.Cours.AdmissionOpenProgram.Program.InstituteID == this.LoginHistory.InstituteID.Value)
					.Select(oc => new
					{
						oc.FacultyMember.CNIC,
						oc.OfferedCourseID,
						oc.Visiting
					}).ToList();

				var surveyConductTypeEnums = Enum.GetValues(typeof(SurveyConductTypes)).Cast<SurveyConductTypes>()
					.Where(sc => surveyConductTypeEnum == null || sc == surveyConductTypeEnum.Value).ToList();

				var teacherEvaluations = new List<TeacherEvaluation>();
				foreach (var offeredCourse in offeredCourses)
					foreach (var surveyConductTypeEnum_ in surveyConductTypeEnums)
					{
						var result = this.GetTeacherEvaluation(cnic, offeredCourse.OfferedCourseID, (SurveyConduct.SurveyConductTypes)surveyConductTypeEnum_);
						teacherEvaluations.Add(new TeacherEvaluation
						{
							Class = result.PageHeader.Class,
							DepartmentAlias = result.PageHeader.DepartmentName,
							FacultyMemberName = result.PageHeader.FacultyMemberName,
							InstituteAlias = result.PageHeader.InstituteName,
							OfferedCourseID = result.PageHeader.OfferedCourseID.Value,
							ProgramAlias = result.PageHeader.ProgramAlias,
							SemesterNo = result.PageHeader.SemesterNoFullName,
							Section = result.PageHeader.SectionFullName,
							Shift = result.PageHeader.ShiftFullName,
							Title = result.PageHeader.Title,
							Visiting = offeredCourse.Visiting.ToYesNo(),
							Percentage = result.Summary.FirstOrDefault()?.GrandTotalPercentage,
						});
					}
				return teacherEvaluations;
			}
		}
	}
}
