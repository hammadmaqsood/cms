using System.Collections.Generic;
using System.ServiceModel;

namespace Aspire.BL.Core.IntegrationServices.HR
{
	[ServiceContract]
	public interface IService
	{
		[OperationContract]
		List<Semester> GetSemesters(string cnic);

		[OperationContract]
		List<Course> GetCourses(string cnic, short semesterID);

		[OperationContract]
		byte[] GetTeacherEvaluation(string cnic, int offeredCourseID, SurveyConductTypes surveyConductType);

		[OperationContract]
		List<TeacherEvaluation> GetTeacherEvaluations(string cnic, SurveyConductTypes? surveyConductTypeEnum, short? fromSemesterID, short? toSemesterID);
	}
}