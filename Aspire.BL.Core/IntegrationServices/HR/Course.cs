﻿using System.Runtime.Serialization;

namespace Aspire.BL.Core.IntegrationServices.HR
{
	[DataContract]
	public class Course
	{
		[DataMember] public int OfferedCourseID { get; set; }
		[DataMember] public string Title { get; set; }
		[DataMember] public string InstituteAlias { get; set; }
		[DataMember] public string DepartmentAlias { get; set; }
		[DataMember] public string ProgramAlias { get; set; }
		[DataMember] public string SemesterNo { get; set; }
		[DataMember] public string Section { get; set; }
		[DataMember] public string Shift { get; set; }
		[DataMember] public string Visiting { get; set; }
		[DataMember] public string FacultyMemberName { get; set; }
		[DataMember] public string Class { get; set; }
	}
}
