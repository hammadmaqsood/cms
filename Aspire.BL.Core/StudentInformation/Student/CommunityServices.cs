﻿using Aspire.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.StudentInformation.Student
{
	public static class CommunityServices
	{
		public static List<Model.Entities.StudentCommunityService> GetStudentCommunityServices(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.StudentCommunityServices.Where(scs => scs.StudentID == studentID).ToList();
			}
		}
	}
}
