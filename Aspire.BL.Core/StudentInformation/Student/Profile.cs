﻿using Aspire.BL.Core.Common.EmailsManagement;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FileServer;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.StudentInformation.Student
{
	public static class Profile
	{
		private static Aspire.BL.Core.Common.Permissions.Student.StudentLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid, SubUserTypes? subUserTypeEnum)
		{
			return aspireContext.DemandStudentActiveSession(loginSessionGuid, subUserTypeEnum);
		}

		public sealed class Student
		{
			internal Student() { }
			public string Enrollment { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string Name { get; internal set; }
			public string FatherName { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public byte DegreeDuration { get; internal set; }
			public ProgramDurations DegreeDurationEnum => (ProgramDurations)this.DegreeDuration;
			public string DegreeDurationFullName => this.DegreeDurationEnum.ToFullName();
			public string Mobile { get; internal set; }
			public string MobileNotVerified { get; internal set; }
			public string Phone { get; internal set; }
			public string PersonalEmail { get; internal set; }
			public string PersonalEmailNotVerified { get; internal set; }
			public string UniversityEmail { get; internal set; }
			public string CurrentAddress { get; internal set; }
			public string PermanentAddress { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public string IntakeSemester => this.IntakeSemesterID.ToSemesterString();
			public short MaxSemesterID { get; internal set; }
			public string MaxSemester => this.MaxSemesterID.ToSemesterString();
		}

		public static Student GetStudentInfo(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid, null);
				return aspireContext.Students.Where(std => std.StudentID == loginHistory.StudentID).Select(std => new Student
				{
					IntakeSemesterID = std.AdmissionOpenProgram.SemesterID,
					MaxSemesterID = std.AdmissionOpenProgram.MaxSemesterID,
					Enrollment = std.Enrollment,
					RegistrationNo = std.RegistrationNo,
					Name = std.Name,
					FatherName = std.FatherName,
					ProgramAlias = std.AdmissionOpenProgram.Program.ProgramAlias,
					DegreeDuration = std.AdmissionOpenProgram.Program.Duration,
					Mobile = std.Mobile,
					MobileNotVerified = std.MobileNotVerified,
					Phone = std.Phone,
					PersonalEmail = std.PersonalEmail,
					PersonalEmailNotVerified = std.PersonalEmailNotVerified,
					UniversityEmail = std.UniversityEmail,
					CurrentAddress = std.CurrentAddress,
					PermanentAddress = std.PermanentAddress,
				}).SingleOrDefault();
			}
		}

		public static bool UpdatePersonalEmail(string personalEmail, string pageURL, Guid loginSessionGuid)
		{
			Aspire.BL.Core.Common.Permissions.Student.StudentLoginHistory loginHistory;
			using (var aspireContext = new AspireContext(true))
			{
				loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid, SubUserTypes.None);
				var student = aspireContext.Students.Single(s => s.StudentID == loginHistory.StudentID);
				if (student.PersonalEmail == personalEmail)
					return false;
				student.PersonalEmailNotVerified = personalEmail.MustBeEmailAddress();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
			}
			return SendEmailConfirmationLinkToStudent(pageURL, loginSessionGuid);
		}

		public static bool SendEmailConfirmationLinkToStudent(string pageURL, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid, SubUserTypes.None);
				var student = aspireContext.Students.Where(s => s.StudentID == loginHistory.StudentID).Select(s => new
				{
					s.StudentID,
					s.Name,
					s.PersonalEmailNotVerified
				}).Single();
				if (string.IsNullOrWhiteSpace(student.PersonalEmailNotVerified))
					return false;
				var url = pageURL.AttachQueryParams("StudentID", student.StudentID, "PersonalEmail", student.PersonalEmailNotVerified, EncryptionModes.ConstantSaltNoneUserSessionNone);
				var subject = "Confirm your email address";
				var emailBody = Properties.Resources.ConfirmPersonalEmailForStudent;
				emailBody = emailBody.Replace("{NameHtmlEncoded}", student.Name.HtmlEncode());
				emailBody = emailBody.Replace("{EmailHtmlEncoded}", student.PersonalEmailNotVerified.HtmlEncode());
				emailBody = emailBody.Replace("{LinkHtmlAttributeEncoded}", url.HtmlAttributeEncode());
				emailBody = emailBody.Replace("{LinkHtmlEncoded}", url.HtmlEncode());
				var createdDate = DateTime.Now;
				var expiryDate = (DateTime?)null;
				var toList = new[] { (student.Name, student.PersonalEmailNotVerified) };
				aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.StudentConfirmPersonalEmail, subject, emailBody, true, Email.Priorities.Normal, createdDate, expiryDate, loginHistory.UserLoginHistoryID, toList: toList);
				aspireContext.CommitTransaction();
				return true;
			}
		}

		public static bool ConfirmPersonalEmail(int studentID, string personalEmail)
		{
			personalEmail.MustBeEmailAddress();
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.Students.SingleOrDefault(s => s.StudentID == studentID && s.PersonalEmailNotVerified == personalEmail);
				if (student == null)
					return false;
				student.PersonalEmail = student.PersonalEmailNotVerified;
				student.PersonalEmailNotVerified = null;
				aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
				return true;
			}
		}

		public static byte[] ReadStudentPicture(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStudentActiveSession(loginSessionGuid, SubUserTypes.None);
				return aspireContext.ReadStudentPicture(loginHistory.StudentID);
			}
		}
	}
}



