﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.StudentInformation.Student
{
	public static class GraduateDirectory
	{
		private static Core.Common.Permissions.Student.StudentLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStudentActiveSession(loginSessionGuid, SubUserTypes.None);
		}

		public enum SubmitGraduateDirectoryFormStatuses
		{
			NoRecordFound,
			AlreadySubmitted,
			ProjectsRecordNotEntered,
			WorkExperienceNotEntered,
			SkillNotEntered,
			ActivitiesNotEntered,
			Submitted,
		}

		public static SubmitGraduateDirectoryFormStatuses SubmitGraduateDirectoryForm(int graduateDirectoryFormID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var graduateDirectoryForm = aspireContext.GraduateDirectoryForms.SingleOrDefault(gdf => gdf.GraduateDirectoryFormID == graduateDirectoryFormID && gdf.StudentID == loginHistory.StudentID);
				if (graduateDirectoryForm == null)
					return SubmitGraduateDirectoryFormStatuses.NoRecordFound;
				if (graduateDirectoryForm.Submitted != null)
					return SubmitGraduateDirectoryFormStatuses.AlreadySubmitted;

				if (!graduateDirectoryForm.GraduateDirectoryFormWorkExperiences.Any())
					return SubmitGraduateDirectoryFormStatuses.WorkExperienceNotEntered;
				if (!graduateDirectoryForm.GraduateDirectoryFormProjects.Any())
					return SubmitGraduateDirectoryFormStatuses.ProjectsRecordNotEntered;
				if (!graduateDirectoryForm.GraduateDirectoryExtraCurricularActivities.Any())
					return SubmitGraduateDirectoryFormStatuses.ActivitiesNotEntered;
				if (!graduateDirectoryForm.GraduateDirectoryFormSkills.Any())
					return SubmitGraduateDirectoryFormStatuses.SkillNotEntered;
				graduateDirectoryForm.Submitted = DateTime.Now;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return SubmitGraduateDirectoryFormStatuses.Submitted;
			}
		}

		public static GraduateDirectoryForm AddAndGetGraduateDirectoryForm(short semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var graduateDirectoryForm = aspireContext.GraduateDirectoryForms
					.SingleOrDefault(gdf => gdf.SemesterID == semesterID && gdf.StudentID == loginHistory.StudentID);
				if (graduateDirectoryForm != null)
					return graduateDirectoryForm;
				graduateDirectoryForm = aspireContext.GraduateDirectoryForms.Add(new GraduateDirectoryForm
				{
					Submitted = null,
					StudentID = loginHistory.StudentID,
					SemesterID = semesterID,
				});
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return graduateDirectoryForm;
			}
		}

		public static (GraduateDirectoryForm graduateDirectoryForm, List<CustomListItem> proramMajors, string programAlias) GetGraduateDirectoryForm(int graduateDirectoryFormID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var graduateDirectoryForm = aspireContext.GraduateDirectoryForms
					.Include(gdf => gdf.GraduateDirectoryExtraCurricularActivities)
					.Include(gdf => gdf.GraduateDirectoryFormProjects)
					.Include(gdf => gdf.GraduateDirectoryFormSkills)
					.Include(gdf => gdf.GraduateDirectoryFormWorkExperiences)
					.Include(gdf => gdf.GraduateDirectoryFormVolunteerWorks)
					.Include(gdf => gdf.GraduateDirectoryFormWorkshops)
					.SingleOrDefault(gdf => gdf.GraduateDirectoryFormID == graduateDirectoryFormID && gdf.StudentID == loginHistory.StudentID);
				var student = aspireContext.Students.Where(s => s.StudentID == loginHistory.StudentID)
					.Select(s => new
					{
						s.AdmissionOpenProgram.Program.ProgramAlias,
						ProgramMajors = s.AdmissionOpenProgram.Program.ProgramMajors
							.Select(pm => new CustomListItem
							{
								ID = pm.ProgramMajorID,
								Text = pm.Majors
							})
							.ToList()
					}).Single();
				return (graduateDirectoryForm, student.ProgramMajors, student.ProgramAlias);
			}
		}

		private static (Core.Common.Permissions.Student.StudentLoginHistory loginHistory, bool noRecordFound, bool formSubmitted) GetGraduateDirectoryFormStatus(this AspireContext aspireContext, int graduateDirectoryFormID, Guid loginSessionGuid)
		{
			var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
			var graduateDirectoryForm = aspireContext.GraduateDirectoryForms
				.Where(gdf => gdf.GraduateDirectoryFormID == graduateDirectoryFormID && gdf.StudentID == loginHistory.StudentID)
				.Select(gdf => new { gdf.Submitted }).SingleOrDefault();
			if (graduateDirectoryForm == null)
				return (loginHistory, true, false);
			if (graduateDirectoryForm.Submitted != null)
				return (loginHistory, false, true);
			return (loginHistory, false, false);
		}

		#region Graduate Directory Work Experience

		public static GraduateDirectoryFormWorkExperience GetGraduateDirectoryFormWorkExperience(int graduateDirectoryFormWorkExperienceID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GraduateDirectoryFormWorkExperiences.SingleOrDefault(gdfwe => gdfwe.GraduateDirectoryFormWorkExperienceID == graduateDirectoryFormWorkExperienceID && gdfwe.GraduateDirectoryForm.StudentID == loginHistory.StudentID);
			}
		}

		public enum AddGraduateDirectoryFormWorkExperienceStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			Success,
		}

		public static AddGraduateDirectoryFormWorkExperienceStatuses AddGraduateDirectoryFormWorkExperience(int graduateDirectoryFormID, GraduateDirectoryFormWorkExperience.ExperienceTypes experienceTypeEnum, string designation, string organization, string duration, string roles, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return AddGraduateDirectoryFormWorkExperienceStatuses.NoRecordFound;
				if (formSubmitted)
					return AddGraduateDirectoryFormWorkExperienceStatuses.SubmittedFormCannotBeChanged;

				var graduateDirectoryFormWorkExperience = new GraduateDirectoryFormWorkExperience
				{
					GraduateDirectoryFormID = graduateDirectoryFormID,
					ExperienceTypeEnum = experienceTypeEnum,
					Designation = designation.TrimAndCannotBeEmpty(),
					Organization = organization.TrimAndCannotBeEmpty(),
					Duration = duration.TrimAndCannotBeEmpty(),
					Roles = roles.TrimAndCannotBeEmpty(),
				};
				aspireContext.GraduateDirectoryFormWorkExperiences.Add(graduateDirectoryFormWorkExperience);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddGraduateDirectoryFormWorkExperienceStatuses.Success;
			}
		}

		public enum UpdateGraduateDirectoryFormWorkExperienceStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			Success,
		}

		public static UpdateGraduateDirectoryFormWorkExperienceStatuses UpdateGraduateDirectoryFormWorkExperience(int graduateDirectoryFormWorkExperienceID, GraduateDirectoryFormWorkExperience.ExperienceTypes experienceTypeEnum, string designation, string organization, string duration, string roles, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var graduateDirectoryFormWorkExperience = aspireContext.GraduateDirectoryFormWorkExperiences.SingleOrDefault(gdfwe => gdfwe.GraduateDirectoryFormWorkExperienceID == graduateDirectoryFormWorkExperienceID);
				if (graduateDirectoryFormWorkExperience == null)
					return UpdateGraduateDirectoryFormWorkExperienceStatuses.NoRecordFound;
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryFormWorkExperience.GraduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return UpdateGraduateDirectoryFormWorkExperienceStatuses.NoRecordFound;
				if (formSubmitted)
					return UpdateGraduateDirectoryFormWorkExperienceStatuses.SubmittedFormCannotBeChanged;

				graduateDirectoryFormWorkExperience.ExperienceTypeEnum = experienceTypeEnum;
				graduateDirectoryFormWorkExperience.Designation = designation.TrimAndCannotBeEmpty();
				graduateDirectoryFormWorkExperience.Organization = organization.TrimAndCannotBeEmpty();
				graduateDirectoryFormWorkExperience.Duration = duration.TrimAndCannotBeEmpty();
				graduateDirectoryFormWorkExperience.Roles = duration.TrimAndCannotBeEmpty();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateGraduateDirectoryFormWorkExperienceStatuses.Success;
			}
		}

		public enum DeleteGraduateDirectoryFormWorkExperienceStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			Success,
		}

		public static DeleteGraduateDirectoryFormWorkExperienceStatuses DeleteGraduateDirectoryFormWorkExperience(int graduateDirectoryFormWorkExperienceID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var graduateDirectoryFormWorkExperience = aspireContext.GraduateDirectoryFormWorkExperiences.SingleOrDefault(gdfwe => gdfwe.GraduateDirectoryFormWorkExperienceID == graduateDirectoryFormWorkExperienceID);
				if (graduateDirectoryFormWorkExperience == null)
					return DeleteGraduateDirectoryFormWorkExperienceStatuses.NoRecordFound;
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryFormWorkExperience.GraduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return DeleteGraduateDirectoryFormWorkExperienceStatuses.NoRecordFound;
				if (formSubmitted)
					return DeleteGraduateDirectoryFormWorkExperienceStatuses.SubmittedFormCannotBeChanged;
				aspireContext.GraduateDirectoryFormWorkExperiences.Remove(graduateDirectoryFormWorkExperience);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteGraduateDirectoryFormWorkExperienceStatuses.Success;
			}
		}

		#endregion

		#region Graduate Directory Form Project

		public static GraduateDirectoryFormProject GetGraduateDirectoryFormProject(int graduateDirectoryFormProjectID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GraduateDirectoryFormProjects.SingleOrDefault(gdfp => gdfp.GraduateDirectoryFormProjectID == graduateDirectoryFormProjectID && gdfp.GraduateDirectoryForm.StudentID == loginHistory.StudentID);
			}
		}

		public enum AddGraduateDirectoryFormProjectStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			Success,
		}

		public static AddGraduateDirectoryFormProjectStatuses AddGraduateDirectoryFormProject(int graduateDirectoryFormID, short semesterID, GraduateDirectoryFormProject.ProjectTypes projectTypeEnum, string description, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return AddGraduateDirectoryFormProjectStatuses.NoRecordFound;
				if (formSubmitted)
					return AddGraduateDirectoryFormProjectStatuses.SubmittedFormCannotBeChanged;

				var graduateDirectoryFormProject = new GraduateDirectoryFormProject
				{
					GraduateDirectoryFormID = graduateDirectoryFormID,
					SemesterID = semesterID,
					ProjectTypeEnum = projectTypeEnum,
					Description = description.TrimAndCannotBeEmpty(),
				};
				aspireContext.GraduateDirectoryFormProjects.Add(graduateDirectoryFormProject);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddGraduateDirectoryFormProjectStatuses.Success;
			}
		}

		public enum UpdateGraduateDirectoryFormProjectStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			Success,
		}

		public static UpdateGraduateDirectoryFormProjectStatuses UpdateGraduateDirectoryFormProject(int graduateDirectoryFormProjectID, short semesterID, GraduateDirectoryFormProject.ProjectTypes projectTypeEnum, string description, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var graduateDirectoryFormProject = aspireContext.GraduateDirectoryFormProjects.SingleOrDefault(gdfwe => gdfwe.GraduateDirectoryFormProjectID == graduateDirectoryFormProjectID);
				if (graduateDirectoryFormProject == null)
					return UpdateGraduateDirectoryFormProjectStatuses.NoRecordFound;
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryFormProject.GraduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return UpdateGraduateDirectoryFormProjectStatuses.NoRecordFound;
				if (formSubmitted)
					return UpdateGraduateDirectoryFormProjectStatuses.SubmittedFormCannotBeChanged;

				graduateDirectoryFormProject.SemesterID = semesterID;
				graduateDirectoryFormProject.ProjectTypeEnum = projectTypeEnum;
				graduateDirectoryFormProject.Description = description.TrimAndCannotBeEmpty();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateGraduateDirectoryFormProjectStatuses.Success;
			}
		}

		public enum DeleteGraduateDirectoryFormProjectStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			Success,
		}

		public static DeleteGraduateDirectoryFormProjectStatuses DeleteGraduateDirectoryFormProject(int graduateDirectoryFormProjectID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var graduateDirectoryFormProject = aspireContext.GraduateDirectoryFormProjects.SingleOrDefault(gdfwe => gdfwe.GraduateDirectoryFormProjectID == graduateDirectoryFormProjectID);
				if (graduateDirectoryFormProject == null)
					return DeleteGraduateDirectoryFormProjectStatuses.NoRecordFound;
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryFormProject.GraduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return DeleteGraduateDirectoryFormProjectStatuses.NoRecordFound;
				if (formSubmitted)
					return DeleteGraduateDirectoryFormProjectStatuses.SubmittedFormCannotBeChanged;

				aspireContext.GraduateDirectoryFormProjects.Remove(graduateDirectoryFormProject);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteGraduateDirectoryFormProjectStatuses.Success;
			}
		}

		#endregion

		#region Graduate Directory Extra Curricular Activity

		public static GraduateDirectoryExtraCurricularActivity GetGraduateDirectoryExtraCurricularActivity(int graduateDirectoryExtraCurricularActivityID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GraduateDirectoryExtraCurricularActivities.SingleOrDefault(gdeca => gdeca.GraduateDirectoryExtraCurricularActivityID == graduateDirectoryExtraCurricularActivityID && gdeca.GraduateDirectoryForm.StudentID == loginHistory.StudentID);
			}
		}

		public enum AddGraduateDirectoryExtraCurricularActivityStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			Success,
		}

		public static AddGraduateDirectoryExtraCurricularActivityStatuses AddGraduateDirectoryExtraCurricularActivity(int graduateDirectoryFormID, string description, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return AddGraduateDirectoryExtraCurricularActivityStatuses.NoRecordFound;
				if (formSubmitted)
					return AddGraduateDirectoryExtraCurricularActivityStatuses.SubmittedFormCannotBeChanged;
				var graduateDirectoryExtraCurricularActivity = new GraduateDirectoryExtraCurricularActivity
				{
					GraduateDirectoryFormID = graduateDirectoryFormID,
					Description = description.TrimAndCannotBeEmpty(),
				};
				aspireContext.GraduateDirectoryExtraCurricularActivities.Add(graduateDirectoryExtraCurricularActivity);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddGraduateDirectoryExtraCurricularActivityStatuses.Success;
			}
		}

		public enum UpdateGraduateDirectoryExtraCurricularActivityStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			Success,
		}

		public static UpdateGraduateDirectoryExtraCurricularActivityStatuses UpdateGraduateDirectoryExtraCurricularActivity(int graduateDirectoryExtraCurricularActivityID, string description, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var graduateDirectoryExtraCurricularActivity = aspireContext.GraduateDirectoryExtraCurricularActivities.SingleOrDefault(gdeca => gdeca.GraduateDirectoryExtraCurricularActivityID == graduateDirectoryExtraCurricularActivityID);
				if (graduateDirectoryExtraCurricularActivity == null)
					return UpdateGraduateDirectoryExtraCurricularActivityStatuses.NoRecordFound;
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryExtraCurricularActivity.GraduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return UpdateGraduateDirectoryExtraCurricularActivityStatuses.NoRecordFound;
				if (formSubmitted)
					return UpdateGraduateDirectoryExtraCurricularActivityStatuses.SubmittedFormCannotBeChanged;

				graduateDirectoryExtraCurricularActivity.Description = description.TrimAndCannotBeEmpty();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateGraduateDirectoryExtraCurricularActivityStatuses.Success;
			}
		}

		public enum DeleteGraduateDirectoryExtraCurricularActivityStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			Success,
		}

		public static DeleteGraduateDirectoryExtraCurricularActivityStatuses DeleteGraduateDirectoryExtraCurricularActivity(int graduateDirectoryExtraCurricularActivityID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var graduateDirectoryExtraCurricularActivity = aspireContext.GraduateDirectoryExtraCurricularActivities.SingleOrDefault(gdeca => gdeca.GraduateDirectoryExtraCurricularActivityID == graduateDirectoryExtraCurricularActivityID);
				if (graduateDirectoryExtraCurricularActivity == null)
					return DeleteGraduateDirectoryExtraCurricularActivityStatuses.NoRecordFound;
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryExtraCurricularActivity.GraduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return DeleteGraduateDirectoryExtraCurricularActivityStatuses.NoRecordFound;
				if (formSubmitted)
					return DeleteGraduateDirectoryExtraCurricularActivityStatuses.SubmittedFormCannotBeChanged;

				aspireContext.GraduateDirectoryExtraCurricularActivities.Remove(graduateDirectoryExtraCurricularActivity);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteGraduateDirectoryExtraCurricularActivityStatuses.Success;
			}
		}

		#endregion

		#region Graduate Directory Form Skill

		public static GraduateDirectoryFormSkill GetGraduateDirectoryFormSkill(int graduateDirectoryFormSkillID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GraduateDirectoryFormSkills.SingleOrDefault(gdfs => gdfs.GraduateDirectorySkillID == graduateDirectoryFormSkillID && gdfs.GraduateDirectoryForm.StudentID == loginHistory.StudentID);
			}
		}

		public enum AddGraduateDirectorySkillStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			Success,
		}

		public static AddGraduateDirectorySkillStatuses AddGraduateDirectorySkill(int graduateDirectoryFormID, string description, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return AddGraduateDirectorySkillStatuses.NoRecordFound;
				if (formSubmitted)
					return AddGraduateDirectorySkillStatuses.SubmittedFormCannotBeChanged;
				var graduateDirectoryFormSkill = new GraduateDirectoryFormSkill
				{
					GraduateDirectoryFormID = graduateDirectoryFormID,
					Description = description,
				};
				aspireContext.GraduateDirectoryFormSkills.Add(graduateDirectoryFormSkill);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddGraduateDirectorySkillStatuses.Success;
			}
		}

		public enum UpdateGraduateDirectorySkillStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			Success,
		}

		public static UpdateGraduateDirectorySkillStatuses UpdateGraduateDirectorySkill(int graduateDirectorySkillID, string description, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var graduateDirectoryFormSkill = aspireContext.GraduateDirectoryFormSkills.SingleOrDefault(gdfs => gdfs.GraduateDirectorySkillID == graduateDirectorySkillID);
				if (graduateDirectoryFormSkill == null)
					return UpdateGraduateDirectorySkillStatuses.NoRecordFound;
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryFormSkill.GraduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return UpdateGraduateDirectorySkillStatuses.NoRecordFound;
				if (formSubmitted)
					return UpdateGraduateDirectorySkillStatuses.SubmittedFormCannotBeChanged;

				graduateDirectoryFormSkill.Description = description.TrimAndCannotBeEmpty();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateGraduateDirectorySkillStatuses.Success;
			}
		}

		public enum DeleteGraduateDirectoryFormSkillStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			Success,
		}

		public static DeleteGraduateDirectoryFormSkillStatuses DeleteGraduateDirectoryFormSkill(int graduateDirectoryFormSkillID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var graduateDirectoryFormSkill = aspireContext.GraduateDirectoryFormSkills.SingleOrDefault(gdfs => gdfs.GraduateDirectorySkillID == graduateDirectoryFormSkillID);
				if (graduateDirectoryFormSkill == null)
					return DeleteGraduateDirectoryFormSkillStatuses.NoRecordFound;
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryFormSkill.GraduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return DeleteGraduateDirectoryFormSkillStatuses.NoRecordFound;
				if (formSubmitted)
					return DeleteGraduateDirectoryFormSkillStatuses.SubmittedFormCannotBeChanged;
				aspireContext.GraduateDirectoryFormSkills.Remove(graduateDirectoryFormSkill);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteGraduateDirectoryFormSkillStatuses.Success;
			}
		}

		#endregion

		#region Graduate Directory Form Volunteer Work

		public static GraduateDirectoryFormVolunteerWork GetGraduateDirectoryFormVolunteerWork(int graduateDirectoryFormVolunteerWorkID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GraduateDirectoryFormVolunteerWorks.SingleOrDefault(gdfvw => gdfvw.GraduateDirectoryFormVolunteerWorkID == graduateDirectoryFormVolunteerWorkID && gdfvw.GraduateDirectoryForm.StudentID == loginHistory.StudentID);
			}
		}

		public enum AddGraduateDirectoryFormVolunteerWorkStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			FromDateMustBeLessThanOrEqualToToDate,
			Success,
		}

		public static AddGraduateDirectoryFormVolunteerWorkStatuses AddGraduateDirectoryFormVolunteerWork(int graduateDirectoryFormID, string organization, string description, DateTime fromDate, DateTime toDate, Guid loginSessionGuid)
		{
			if (fromDate > toDate)
				return AddGraduateDirectoryFormVolunteerWorkStatuses.FromDateMustBeLessThanOrEqualToToDate;
			using (var aspireContext = new AspireContext(true))
			{
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return AddGraduateDirectoryFormVolunteerWorkStatuses.NoRecordFound;
				if (formSubmitted)
					return AddGraduateDirectoryFormVolunteerWorkStatuses.SubmittedFormCannotBeChanged;
				var graduateDirectoryFormVolunteerWork = new GraduateDirectoryFormVolunteerWork
				{
					GraduateDirectoryFormID = graduateDirectoryFormID,
					Organization = organization.TrimAndCannotBeEmpty(),
					Description = description.TrimAndCannotBeEmpty(),
					FromDate = fromDate,
					ToDate = toDate
				};
				aspireContext.GraduateDirectoryFormVolunteerWorks.Add(graduateDirectoryFormVolunteerWork);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddGraduateDirectoryFormVolunteerWorkStatuses.Success;
			}
		}

		public enum UpdateGraduateDirectoryFormVolunteerWorkStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			FromDateMustBeLessThanOrEqualToToDate,
			Success,
		}

		public static UpdateGraduateDirectoryFormVolunteerWorkStatuses UpdateGraduateDirectoryFormVolunteerWork(int graduateDirectoryFormVolunteerWorkID, string organization, string description, DateTime fromDate, DateTime toDate, Guid loginSessionGuid)
		{
			if (fromDate > toDate)
				return UpdateGraduateDirectoryFormVolunteerWorkStatuses.FromDateMustBeLessThanOrEqualToToDate;
			using (var aspireContext = new AspireContext(true))
			{
				var graduateDirectoryFormVolunteerWork = aspireContext.GraduateDirectoryFormVolunteerWorks.SingleOrDefault(gdfvw => gdfvw.GraduateDirectoryFormVolunteerWorkID == graduateDirectoryFormVolunteerWorkID);
				if (graduateDirectoryFormVolunteerWork == null)
					return UpdateGraduateDirectoryFormVolunteerWorkStatuses.NoRecordFound;
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryFormVolunteerWork.GraduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return UpdateGraduateDirectoryFormVolunteerWorkStatuses.NoRecordFound;
				if (formSubmitted)
					return UpdateGraduateDirectoryFormVolunteerWorkStatuses.SubmittedFormCannotBeChanged;

				graduateDirectoryFormVolunteerWork.Organization = organization.TrimAndCannotBeEmpty();
				graduateDirectoryFormVolunteerWork.Description = description.TrimAndCannotBeEmpty();
				graduateDirectoryFormVolunteerWork.FromDate = fromDate;
				graduateDirectoryFormVolunteerWork.ToDate = toDate;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateGraduateDirectoryFormVolunteerWorkStatuses.Success;
			}
		}

		public enum DeleteGraduateDirectoryFormVolunteerWorkStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			Success,
		}

		public static DeleteGraduateDirectoryFormVolunteerWorkStatuses DeleteGraduateDirectoryFormVolunteerWork(int graduateDirectoryFormVolunteerWorkID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var graduateDirectoryFormVolunteerWork = aspireContext.GraduateDirectoryFormVolunteerWorks.SingleOrDefault(gdfvw => gdfvw.GraduateDirectoryFormVolunteerWorkID == graduateDirectoryFormVolunteerWorkID);
				if (graduateDirectoryFormVolunteerWork == null)
					return DeleteGraduateDirectoryFormVolunteerWorkStatuses.NoRecordFound;
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryFormVolunteerWork.GraduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return DeleteGraduateDirectoryFormVolunteerWorkStatuses.NoRecordFound;
				if (formSubmitted)
					return DeleteGraduateDirectoryFormVolunteerWorkStatuses.SubmittedFormCannotBeChanged;
				aspireContext.GraduateDirectoryFormVolunteerWorks.Remove(graduateDirectoryFormVolunteerWork);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteGraduateDirectoryFormVolunteerWorkStatuses.Success;
			}
		}

		#endregion

		#region Graduate Directory Form Workshop

		public static GraduateDirectoryFormWorkshop GetGraduateDirectoryFormWorkshop(int graduateDirectoryFormWorkshopID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GraduateDirectoryFormWorkshops.SingleOrDefault(gdfvw => gdfvw.GraduateDirectoryFormWorkshopID == graduateDirectoryFormWorkshopID && gdfvw.GraduateDirectoryForm.StudentID == loginHistory.StudentID);
			}
		}

		public enum AddGraduateDirectoryFormWorkshopStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			FromDateMustBeLessThanOrEqualToToDate,
			Success,
		}

		public static AddGraduateDirectoryFormWorkshopStatuses AddGraduateDirectoryFormWorkshop(int graduateDirectoryFormID, GraduateDirectoryFormWorkshop.Types typeEnum, string description, DateTime fromDate, DateTime toDate, Guid loginSessionGuid)
		{
			if (fromDate > toDate)
				return AddGraduateDirectoryFormWorkshopStatuses.FromDateMustBeLessThanOrEqualToToDate;
			using (var aspireContext = new AspireContext(true))
			{
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return AddGraduateDirectoryFormWorkshopStatuses.NoRecordFound;
				if (formSubmitted)
					return AddGraduateDirectoryFormWorkshopStatuses.SubmittedFormCannotBeChanged;
				var graduateDirectoryFormWorkshop = new GraduateDirectoryFormWorkshop
				{
					GraduateDirectoryFormID = graduateDirectoryFormID,
					TypeEnum = typeEnum,
					Description = description,
					FromDate = fromDate,
					ToDate = toDate
				};
				aspireContext.GraduateDirectoryFormWorkshops.Add(graduateDirectoryFormWorkshop);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddGraduateDirectoryFormWorkshopStatuses.Success;
			}
		}

		public enum UpdateGraduateDirectoryFormWorkshopStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			FromDateMustBeLessThanOrEqualToToDate,
			Success,
		}

		public static UpdateGraduateDirectoryFormWorkshopStatuses UpdateGraduateDirectoryFormWorkshop(int graduateDirectoryFormWorkshopID, GraduateDirectoryFormWorkshop.Types typeEnum, string description, DateTime fromDate, DateTime toDate, Guid loginSessionGuid)
		{
			if (fromDate > toDate)
				return UpdateGraduateDirectoryFormWorkshopStatuses.FromDateMustBeLessThanOrEqualToToDate;
			using (var aspireContext = new AspireContext(true))
			{
				var graduateDirectoryFormWorkshop = aspireContext.GraduateDirectoryFormWorkshops.SingleOrDefault(gdfvw => gdfvw.GraduateDirectoryFormWorkshopID == graduateDirectoryFormWorkshopID);
				if (graduateDirectoryFormWorkshop == null)
					return UpdateGraduateDirectoryFormWorkshopStatuses.NoRecordFound;
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryFormWorkshop.GraduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return UpdateGraduateDirectoryFormWorkshopStatuses.NoRecordFound;
				if (formSubmitted)
					return UpdateGraduateDirectoryFormWorkshopStatuses.SubmittedFormCannotBeChanged;

				graduateDirectoryFormWorkshop.TypeEnum = typeEnum;
				graduateDirectoryFormWorkshop.Description = description.TrimAndCannotBeEmpty();
				graduateDirectoryFormWorkshop.FromDate = fromDate;
				graduateDirectoryFormWorkshop.ToDate = toDate;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateGraduateDirectoryFormWorkshopStatuses.Success;
			}
		}

		public enum DeleteGraduateDirectoryFormWorkshopStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			Success,
		}

		public static DeleteGraduateDirectoryFormWorkshopStatuses DeleteGraduateDirectoryFormWorkshop(int graduateDirectoryFormWorkshopID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var graduateDirectoryFormWorkshop = aspireContext.GraduateDirectoryFormWorkshops.SingleOrDefault(gdfvw => gdfvw.GraduateDirectoryFormWorkshopID == graduateDirectoryFormWorkshopID);
				if (graduateDirectoryFormWorkshop == null)
					return DeleteGraduateDirectoryFormWorkshopStatuses.NoRecordFound;
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryFormWorkshop.GraduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return DeleteGraduateDirectoryFormWorkshopStatuses.NoRecordFound;
				if (formSubmitted)
					return DeleteGraduateDirectoryFormWorkshopStatuses.SubmittedFormCannotBeChanged;
				aspireContext.GraduateDirectoryFormWorkshops.Remove(graduateDirectoryFormWorkshop);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteGraduateDirectoryFormWorkshopStatuses.Success;
			}
		}

		#endregion

		public enum UpdateProgramMajorsStatuses
		{
			NoRecordFound,
			SubmittedFormCannotBeChanged,
			Success,
		}

		public static UpdateProgramMajorsStatuses UpdateProgramMajors(int graduateDirectoryFormID, int? programMajorID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var (loginHistory, noRecordFound, formSubmitted) = aspireContext.GetGraduateDirectoryFormStatus(graduateDirectoryFormID, loginSessionGuid);
				if (noRecordFound)
					return UpdateProgramMajorsStatuses.NoRecordFound;
				if (formSubmitted)
					return UpdateProgramMajorsStatuses.SubmittedFormCannotBeChanged;
				aspireContext.GraduateDirectoryForms.Single(gdf => gdf.GraduateDirectoryFormID == graduateDirectoryFormID).ProgramMajorID = programMajorID;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateProgramMajorsStatuses.Success;
			}
		}
	}
}
