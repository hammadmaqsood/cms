﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FileServer;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.StudentInformation.Common
{
	public static class StudentDocuments
	{
		public sealed class GetStudentInfoAndDocumentsResult
		{
			internal GetStudentInfoAndDocumentsResult() { }
			public int StudentID { get; internal set; }
			public int InstituteID { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public int ProgramID { get; internal set; }
			public int AdmissionOpenProgramID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public List<StudentAcademicRecord> StudentAcademicRecords { get; internal set; }
			public List<StudentDocument> StudentDocuments { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string FatherName { get; internal set; }
			public Model.Entities.Student.Statuses? StatusEnum { get; internal set; }
			public DateTime? StatusDate { get; internal set; }
			public string BlockedReason { get; internal set; }
			public Guid? StudentDocumentsStatus { get; internal set; }
			public string StudentDocumentsRemarks { get; internal set; }
			public DateTime? StudentDocumentsStatusDate { get; internal set; }
			public string StudentDocumentsLastUpdatedBy { get; internal set; }
		}

		public static GetStudentInfoAndDocumentsResult GetStudentInfoAndDocuments(int? studentID, string enrollment, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var result = aspireContext.Students.Where(s => s.StudentID == studentID || s.Enrollment == enrollment)
					.Select(s => new GetStudentInfoAndDocumentsResult
					{
						StudentID = s.StudentID,
						InstituteID = s.AdmissionOpenProgram.Program.InstituteID,
						DepartmentID = s.AdmissionOpenProgram.Program.DepartmentID,
						ProgramID = s.AdmissionOpenProgram.Program.ProgramID,
						AdmissionOpenProgramID = s.AdmissionOpenProgramID,
						Enrollment = s.Enrollment,
						RegistrationNo = s.RegistrationNo,
						FatherName = s.FatherName,
						ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
						Name = s.Name,
						StatusEnum = (Model.Entities.Student.Statuses?)s.Status,
						StatusDate = s.StatusDate,
						BlockedReason = s.BlockedReason,
						IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
						StudentAcademicRecords = s.StudentAcademicRecords.ToList(),
						StudentDocuments = s.StudentDocuments.ToList(),
						StudentDocumentsStatus = s.StudentDocumentsStatus,
						StudentDocumentsRemarks = s.StudentDocumentsRemarks,
						StudentDocumentsStatusDate = s.StudentDocumentsStatusDate,
						StudentDocumentsLastUpdatedBy = s.UserLoginHistory.User.Name
					})
					.SingleOrDefault();
				if (result == null)
					return null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Student:
						if (loginHistory.StudentID != result.StudentID)
							return null;
						break;
					case UserTypes.Staff:
						aspireContext.DemandStaffActiveSession(loginSessionGuid);
						aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.ViewStudentDocuments, UserGroupPermission.PermissionValues.Allowed, result.InstituteID, result.DepartmentID, result.ProgramID, result.AdmissionOpenProgramID);
						break;
					default:
						throw new NotImplementedEnumException(loginHistory.UserTypeEnum);
				}
				return result;
			}
		}

		public enum AddStudentDocumentStatuses
		{
			NoRecordFound,
			StudentCanNotAddDocumentAfterVerification,
			DuplicateDocumentNotAllowed,
			FileNotFound,
			TemporaryFileNotFound,
			DocumentIsNotValid,
			DocumentIsNotJpeg,
			Success
		}

		public static AddStudentDocumentStatuses AddStudentDocument(int studentID, StudentDocument.DocumentTypes documentTypeEnum, List<(StudentDocument.Pages PageEnum, Guid InstanceGuid, Guid TemporarySystemFileID)> documents, Guid loginSessionGuid)
		{
			var pages = documents.Select(d => d.PageEnum).Distinct().ToList();
			if (documents.Count != pages.Count)
				throw new ArgumentException(nameof(documents));
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var record = aspireContext.Students.Where(sd => sd.StudentID == studentID)
					.Select(s => new
					{
						Student = s,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.Program.ProgramID,
						s.AdmissionOpenProgramID
					})
					.SingleOrDefault();
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Student:
						if (loginHistory.StudentID != record.Student.StudentID)
							return AddStudentDocumentStatuses.NoRecordFound;
						if (record.Student.StudentDocumentsStatusEnum == Model.Entities.Student.StudentDocumentsStatuses.Verified)
							return AddStudentDocumentStatuses.StudentCanNotAddDocumentAfterVerification;
						break;
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
						aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.UploadStudentDocuments, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
						break;
					default:
						throw new NotImplementedEnumException(loginHistory.UserTypeEnum);
				}
				var documentType = documentTypeEnum.GetGuid();
				var documentAttribute = StudentDocument.GetDocumentAttribute(documentTypeEnum);
				if (!documents.All(d => documentAttribute.Pages.Select(p => p.Page).Contains(d.PageEnum)))
					throw new ArgumentException(nameof(documents));

				if (!documentAttribute.AllowMultiple && aspireContext.StudentDocuments.Any(sd => sd.StudentID == record.Student.StudentID && sd.DocumentType == documentType))
					return AddStudentDocumentStatuses.DuplicateDocumentNotAllowed;

				foreach (var document in documents)
				{
					var (systemFile, status, imageDpiX, imageDpiY, imageWidthPixels, imageHeightPixels) = aspireContext.AddStudentDocument(document.PageEnum.GetFullName(), document.InstanceGuid, document.TemporarySystemFileID, loginSessionGuid);
					switch (status)
					{
						case FileServer.StudentDocuments.AddStudentDocumentStatuses.TemporaryFileNotFound:
							return AddStudentDocumentStatuses.TemporaryFileNotFound;
						case FileServer.StudentDocuments.AddStudentDocumentStatuses.DocumentIsNotValid:
							return AddStudentDocumentStatuses.DocumentIsNotValid;
						case FileServer.StudentDocuments.AddStudentDocumentStatuses.DocumentIsNotJpeg:
							return AddStudentDocumentStatuses.DocumentIsNotJpeg;
						case FileServer.StudentDocuments.AddStudentDocumentStatuses.Success:
							var studentDocument = aspireContext.StudentDocuments.Add(new StudentDocument
							{
								StudentDocumentID = Guid.NewGuid(),
								StudentID = studentID,
								DocumentTypeEnum = documentTypeEnum,
								PageEnum = document.PageEnum,
								SystemFileID = systemFile?.SystemFileID ?? throw new InvalidOperationException(),
								Remarks = null,
								ImageDpiX = imageDpiX,
								ImageDpiY = imageDpiY,
								ImageHeightPixels = imageHeightPixels,
								ImageWidthPixels = imageWidthPixels,
								StatusEnum = StudentDocument.Statuses.VerificationPending
							});
							if (studentDocument.ImageDpiX < 150 || studentDocument.ImageDpiY < 150)
							{
								studentDocument.StatusEnum = StudentDocument.Statuses.Rejected;
								studentDocument.Remarks = "Horizontal/Vertical Resolution must be at least 150dpi. Please upload proper scanned document.";
							}
							aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
							break;
						default:
							throw new NotImplementedEnumException(status);
					}
				}
				record.Student.StudentDocumentsStatusEnum = Model.Entities.Student.StudentDocumentsStatuses.VerificationPending;
				record.Student.StudentDocumentsRemarks = null;
				record.Student.StudentDocumentsLastUpdatedByUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				record.Student.StudentDocumentsStatusDate = DateTime.Now;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddStudentDocumentStatuses.Success;
			}
		}

		public static StudentDocument GetStudentDocument(Guid studentDocumentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var result = aspireContext.StudentDocuments.Where(sd => sd.StudentDocumentID == studentDocumentID)
					.Select(sd => new
					{
						StudentDocument = sd,
						sd.Student.AdmissionOpenProgram.Program.InstituteID,
						sd.Student.AdmissionOpenProgram.Program.DepartmentID,
						sd.Student.AdmissionOpenProgram.Program.ProgramID,
						sd.Student.AdmissionOpenProgramID,
					})
					.SingleOrDefault();
				if (result == null)
					return null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Student:
						if (loginHistory.StudentID != result.StudentDocument.StudentID)
							return null;
						break;
					case UserTypes.Staff:
						aspireContext.DemandStaffActiveSession(loginSessionGuid);
						aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.ViewStudentDocuments, UserGroupPermission.PermissionValues.Allowed, result.InstituteID, result.DepartmentID, result.ProgramID, result.AdmissionOpenProgramID);
						break;
					default:
						throw new NotImplementedEnumException(loginHistory.UserTypeEnum);
				}

				return result.StudentDocument;
			}
		}

		public enum DeleteStudentDocumentStatuses
		{
			NoRecordFound,
			StudentCanNotDeleteDocumentAfterVerification,
			Success
		}

		public static DeleteStudentDocumentStatuses DeleteStudentDocument(int studentID, StudentDocument.DocumentTypes documentTypeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var documentType = documentTypeEnum.GetGuid();
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						Student = s,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.Program.ProgramID,
						s.AdmissionOpenProgramID,
						StudentDocuments = s.StudentDocuments.Where(sd => sd.DocumentType == documentType).ToList()
					})
					.SingleOrDefault();
				if (record == null)
					return DeleteStudentDocumentStatuses.NoRecordFound;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Student:
						if (loginHistory.StudentID != record.Student.StudentID)
							return DeleteStudentDocumentStatuses.NoRecordFound;
						if (record.Student.StudentDocumentsStatusEnum == Model.Entities.Student.StudentDocumentsStatuses.Verified)
							return DeleteStudentDocumentStatuses.StudentCanNotDeleteDocumentAfterVerification;
						break;
					case UserTypes.Staff:
						aspireContext.DemandStaffActiveSession(loginSessionGuid);
						aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.VerifyStudentDocuments, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
						break;
					default:
						throw new NotImplementedEnumException(loginHistory.UserTypeEnum);
				}

				record.StudentDocuments.ForEach(sd =>
				{
					aspireContext.DeleteSystemFile(sd.SystemFileID, loginHistory.LoginSessionGuid);
					aspireContext.StudentDocuments.Remove(sd);
				});

				record.Student.StudentDocumentsStatusEnum = Model.Entities.Student.StudentDocumentsStatuses.VerificationPending;
				record.Student.StudentDocumentsRemarks = null;
				record.Student.StudentDocumentsStatusDate = DateTime.Now;
				record.Student.StudentDocumentsLastUpdatedByUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteStudentDocumentStatuses.Success;
			}
		}

		public static (SystemFile systemFile, byte[] fileBytes)? ReadStudentDocument(Guid studentDocumentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var result = aspireContext.StudentDocuments.Where(sd => sd.StudentDocumentID == studentDocumentID)
					.Select(sd => new
					{
						sd.StudentID,
						sd.SystemFileID,
						sd.Student.AdmissionOpenProgram.Program.InstituteID,
						sd.Student.AdmissionOpenProgram.Program.DepartmentID,
						sd.Student.AdmissionOpenProgram.Program.ProgramID,
						sd.Student.AdmissionOpenProgramID
					})
					.SingleOrDefault();
				if (result == null)
					return null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Student:
						if (loginHistory.StudentID != result.StudentID)
							return null;
						break;
					case UserTypes.Staff:
						aspireContext.DemandStaffActiveSession(loginSessionGuid);
						aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.ViewStudentDocuments, UserGroupPermission.PermissionValues.Allowed, result.InstituteID, result.DepartmentID, result.ProgramID, result.AdmissionOpenProgramID);
						break;
					default:
						throw new NotImplementedEnumException(loginHistory.UserTypeEnum);
				}

				return aspireContext.ReadStudentDocument(result.SystemFileID);
			}
		}
	}
}