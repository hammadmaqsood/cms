﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.StudentInformation.Staff
{
	public static class StudentSearch
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class Student
		{
			internal Student() { }
			public int StudentID { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string Name { get; internal set; }
			public string Enrollment { get; internal set; }
			public string CNIC { get; internal set; }
			public string FatherName { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public byte Gender { get; internal set; }
			public string GenderEnumString => ((Genders)this.Gender).ToFullName();
			public int ProgramID { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public string IntakeSemester => this.IntakeSemesterID.ToSemesterString();
			public byte? Status { get; internal set; }
			public string StatusEnumString => ((Model.Entities.Student.Statuses?)this.Status)?.ToFullName();
		}

		public sealed class SearchStudentsResult
		{
			internal SearchStudentsResult() { }
			public int VirtualItemCount { get; internal set; }
			public List<Student> Students { get; internal set; }
		}

		public static SearchStudentsResult SearchStudents(string enrollment, int? registrationNo, string name, int? intakeSemesterID, int? programID, string cnic, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var students = aspireContext.Students.Where(s => s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);

				if (!string.IsNullOrWhiteSpace(enrollment))
					students = students.Where(s => s.Enrollment == enrollment);
				if (registrationNo != null)
					students = students.Where(s => s.RegistrationNo == registrationNo.Value);
				if (intakeSemesterID != null)
					students = students.Where(s => s.AdmissionOpenProgram.SemesterID == intakeSemesterID.Value);
				if (!string.IsNullOrWhiteSpace(name))
					students = students.Where(s => s.Name.Contains(name));
				if (!string.IsNullOrWhiteSpace(cnic))
					students = students.Where(s => s.CNIC.Contains(cnic));
				if (programID != null)
					students = students.Where(s => s.AdmissionOpenProgram.ProgramID == programID.Value);

				var virtualItemCount = students.Count();

				switch (sortExpression)
				{
					case nameof(Student.Enrollment):
						students = students.OrderBy(sortDirection, s => s.Enrollment);
						break;
					case nameof(Student.RegistrationNo):
						students = students.OrderBy(sortDirection, s => s.RegistrationNo);
						break;
					case nameof(Student.IntakeSemesterID):
					case nameof(Student.IntakeSemester):
						students = students.OrderBy(sortDirection, s => s.AdmissionOpenProgram.SemesterID);
						break;
					case nameof(Student.CNIC):
						students = students.OrderBy(sortDirection, s => s.CNIC);
						break;
					case nameof(Student.Name):
						students = students.OrderBy(sortDirection, s => s.Name);
						break;
					case nameof(Student.FatherName):
						students = students.OrderBy(sortDirection, s => s.FatherName);
						break;
					case nameof(Student.Gender):
						students = students.OrderBy(sortDirection, s => s.Gender);
						break;
					case nameof(Student.Status):
						students = students.OrderBy(sortDirection, s => s.Status);
						break;
					case nameof(Student.ProgramAlias):
						students = students.OrderBy(sortDirection, s => s.AdmissionOpenProgram.Program.ProgramAlias);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return new SearchStudentsResult
				{
					VirtualItemCount = virtualItemCount,
					Students = students.Skip(pageIndex * pageSize).Take(pageSize).Select(s => new Student
					{
						ProgramID = s.AdmissionOpenProgram.ProgramID,
						StudentID = s.StudentID,
						Enrollment = s.Enrollment,
						ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
						Name = s.Name,
						FatherName = s.FatherName,
						CNIC = s.CNIC,
						RegistrationNo = s.RegistrationNo,
						Gender = s.Gender,
						IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
						Status = s.Status,
					}).ToList(),
				};
			}
		}
	}
}
