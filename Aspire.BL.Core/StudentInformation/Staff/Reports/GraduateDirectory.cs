﻿using Aspire.BL.Core.Common.Permissions;
#if !DEBUG
using Aspire.BL.Core.FileServer;
#endif
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.StudentInformation.Staff.Reports
{
	public static class GraduateDirectory
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.ViewGraduateDirectory, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class GraduateDirectoryForm
		{
			public int StudentID { get; set; }
			public string Enrollment { get; set; }
			public string Name { get; set; }
			public short SemesterID { get; set; }
			public Guid? PictureSystemFolderID { get; set; }
			public byte[] Picture { get; internal set; }
			public string PersonalEmail { get; set; }
			public string UniversityEmail { get; set; }
			public string Mobile { get; set; }
			public string InstituteAlias { get; set; }
			public string InstituteShortName { get; set; }
			public string InstituteName { get; set; }
			public string DepartmentName { get; set; }
			public string DepartmentShortName { get; set; }
			public string DepartmentAlias { get; set; }
			public string ProgramAlias { get; set; }
			public string ProgramShortName { get; set; }
			public string ProgramName { get; set; }
			public byte Duration { get; set; }
			public short IntakeSemesterID { get; set; }
			public string Majors { get; set; }
			public List<GraduateDirectoryExtraCurricularActivity> GraduateDirectoryExtraCurricularActivities { get; set; }
			public List<GraduateDirectoryFormProject> GraduateDirectoryFormProjects { get; set; }
			public List<GraduateDirectoryFormSkill> GraduateDirectoryFormSkills { get; set; }
			public List<GraduateDirectoryFormVolunteerWork> GraduateDirectoryFormVolunteerWorks { get; set; }
			public List<GraduateDirectoryFormWorkExperience> GraduateDirectoryFormWorkExperiences { get; set; }
			public List<GraduateDirectoryFormWorkshop> GraduateDirectoryFormWorkshops { get; set; }
			public List<StudentAcademicRecord> StudentAcademicRecords { get; set; }

			public string Semester => (this.SemesterID).ToSemesterString();
			public string IntakeSemester => (this.IntakeSemesterID).ToSemesterString();
			public string GenderFullName => this.GenderEnum.ToFullName();
			public Genders GenderEnum { get; set; }


			public List<GraduateDirectoryForm> GetGraduateDirectoryForms()
			{
				return null;
			}

			public sealed class StudentAcademicRecord
			{
				public DegreeTypes DegreeTypeEnum { get; internal set; }
				public string DegreeTypeFullName => this.DegreeTypeEnum.ToFullName();
				public string BoardUniversity { get; internal set; }
				public string Institute { get; internal set; }
				public double ObtainedMarks { get; internal set; }
				public double TotalMarks { get; internal set; }
				public short PassingYear { get; internal set; }

				public List<StudentAcademicRecord> GetStudentAcademicRecords()
				{
					return null;
				}
			}

			public sealed class GraduateDirectoryFormWorkExperience
			{
				public Model.Entities.GraduateDirectoryFormWorkExperience.ExperienceTypes ExperienceTypeEnum { get; set; }
				public string ExperienceTypeFullName => this.ExperienceTypeEnum.ToFullName();
				public string Designation { get; set; }
				public string Organization { get; set; }
				public string Duration { get; set; }
				public string Roles { get; set; }

				public List<GraduateDirectoryFormWorkExperience> GetGraduateDirectoryFormWorkExperiences()
				{
					return null;
				}
			}

			public sealed class GraduateDirectoryFormProject
			{
				public Model.Entities.GraduateDirectoryFormProject.ProjectTypes ProjectTypeEnum { get; set; }
				public string ProjectTypeFullName => this.ProjectTypeEnum.ToFullName();
				public string Description { get; set; }

				public List<GraduateDirectoryFormProject> GetGraduateDirectoryFormProjects()
				{
					return null;
				}
			}

			public sealed class GraduateDirectoryFormVolunteerWork
			{
				public DateTime FromDate { get; set; }
				public DateTime ToDate { get; set; }
				public string Organization { get; set; }
				public string Description { get; set; }

				public List<GraduateDirectoryFormVolunteerWork> GetGraduateDirectoryFormVolunteerWorks()
				{
					return null;
				}
			}

			public sealed class GraduateDirectoryFormWorkshop
			{
				public DateTime FromDate { get; set; }
				public DateTime ToDate { get; set; }
				public string Description { get; set; }
				public Model.Entities.GraduateDirectoryFormWorkshop.Types TypeEnum { get; set; }
				public string TypeFullName => this.TypeEnum.ToFullName();

				public List<GraduateDirectoryFormWorkshop> GetGraduateDirectoryFormWorkshops()
				{
					return null;
				}
			}

			public sealed class GraduateDirectoryExtraCurricularActivity
			{
				public string Description { get; set; }

				public List<GraduateDirectoryExtraCurricularActivity> GetGraduateDirectoryExtraCurricularActivities()
				{
					return null;
				}
			}

			public sealed class GraduateDirectoryFormSkill
			{
				public string Description { get; set; }

				public List<GraduateDirectoryFormSkill> GetGraduateDirectoryFormSkills()
				{
					return null;
				}
			}
		}

		public class ReportDataSet
		{
			internal ReportDataSet() { }
			public List<GraduateDirectoryForm> GraduateDirectoryForms { get; internal set; }
		}

		public static ReportDataSet GetGraduateDirectoryForms(short? intakeSemesterID, int? departmentID, int? programID, short semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var graduateDirectoryForms = aspireContext.GraduateDirectoryForms
					.Where(gdf => gdf.Submitted != null && gdf.SemesterID == semesterID)
					.Where(gdf => intakeSemesterID == null || gdf.Student.AdmissionOpenProgram.SemesterID == intakeSemesterID.Value)
					.Where(gdf => gdf.Student.AdmissionOpenProgram.FinalSemesterID <= semesterID)
					.Where(gdf => gdf.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(gdf => departmentID == null || gdf.Student.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value)
					.Where(gdf => programID == null || gdf.Student.AdmissionOpenProgram.ProgramID == programID.Value)
					.Select(gdf => new GraduateDirectoryForm
					{
						StudentID = gdf.StudentID,
						Enrollment = gdf.Student.Enrollment,
						Name = gdf.Student.Name,
						SemesterID = gdf.SemesterID,
						GenderEnum = (Genders)gdf.Student.Gender,
						PictureSystemFolderID = gdf.Student.PictureSystemFolderID,
						PersonalEmail = gdf.Student.PersonalEmail,
						UniversityEmail = gdf.Student.UniversityEmail,
						Mobile = gdf.Student.Mobile,
						InstituteAlias = gdf.Student.AdmissionOpenProgram.Program.Institute.InstituteAlias,
						InstituteShortName = gdf.Student.AdmissionOpenProgram.Program.Institute.InstituteShortName,
						InstituteName = gdf.Student.AdmissionOpenProgram.Program.Institute.InstituteName,
						DepartmentName = gdf.Student.AdmissionOpenProgram.Program.Department.DepartmentName,
						DepartmentShortName = gdf.Student.AdmissionOpenProgram.Program.Department.DepartmentShortName,
						DepartmentAlias = gdf.Student.AdmissionOpenProgram.Program.Department.DepartmentAlias,
						ProgramAlias = gdf.Student.AdmissionOpenProgram.Program.ProgramAlias,
						ProgramShortName = gdf.Student.AdmissionOpenProgram.Program.ProgramShortName,
						ProgramName = gdf.Student.AdmissionOpenProgram.Program.ProgramName,
						Duration = gdf.Student.AdmissionOpenProgram.Program.Duration,
						IntakeSemesterID = gdf.Student.AdmissionOpenProgram.SemesterID,
						Majors = gdf.ProgramMajor.Majors,
						StudentAcademicRecords = gdf.Student.StudentAcademicRecords.Select(sar => new GraduateDirectoryForm.StudentAcademicRecord
						{
							DegreeTypeEnum = (DegreeTypes)sar.DegreeType,
							BoardUniversity = sar.BoardUniversity,
							Institute = sar.Institute,
							ObtainedMarks = sar.ObtainedMarks,
							TotalMarks = sar.TotalMarks,
							PassingYear = sar.PassingYear,
						}).ToList(),
						GraduateDirectoryFormWorkExperiences = gdf.GraduateDirectoryFormWorkExperiences.Select(gdfwe => new GraduateDirectoryForm.GraduateDirectoryFormWorkExperience
						{
							ExperienceTypeEnum = (GraduateDirectoryFormWorkExperience.ExperienceTypes)gdfwe.ExperienceType,
							Designation = gdfwe.Designation,
							Organization = gdfwe.Organization,
							Duration = gdfwe.Duration,
							Roles = gdfwe.Roles,
						}).ToList(),
						GraduateDirectoryFormProjects = gdf.GraduateDirectoryFormProjects.Select(gdfp => new GraduateDirectoryForm.GraduateDirectoryFormProject
						{
							ProjectTypeEnum = (GraduateDirectoryFormProject.ProjectTypes)gdfp.ProjectType,
							Description = gdfp.Description,
						}).ToList(),
						GraduateDirectoryFormVolunteerWorks = gdf.GraduateDirectoryFormVolunteerWorks.Select(gdfvw => new GraduateDirectoryForm.GraduateDirectoryFormVolunteerWork
						{
							FromDate = gdfvw.FromDate,
							ToDate = gdfvw.ToDate,
							Organization = gdfvw.Organization,
							Description = gdfvw.Description,
						}).ToList(),
						GraduateDirectoryFormWorkshops = gdf.GraduateDirectoryFormWorkshops.Select(gdfw => new GraduateDirectoryForm.GraduateDirectoryFormWorkshop
						{
							FromDate = gdfw.FromDate,
							ToDate = gdfw.ToDate,
							Description = gdfw.Description,
							TypeEnum = (GraduateDirectoryFormWorkshop.Types)gdfw.Type,
						}).ToList(),
						GraduateDirectoryExtraCurricularActivities = gdf.GraduateDirectoryExtraCurricularActivities.Select(gdfecw => new GraduateDirectoryForm.GraduateDirectoryExtraCurricularActivity
						{
							Description = gdfecw.Description,
						}).ToList(),
						GraduateDirectoryFormSkills = gdf.GraduateDirectoryFormSkills.Select(gdfs => new GraduateDirectoryForm.GraduateDirectoryFormSkill
						{
							Description = gdfs.Description,
						}).ToList()
					})
					.ToList();
#if !DEBUG
				foreach (var graduateDirectoryForm in graduateDirectoryForms)
					if (graduateDirectoryForm.PictureSystemFolderID != null)
						graduateDirectoryForm.Picture = aspireContext.ReadStudentPicture(graduateDirectoryForm.StudentID);
#endif
				return new ReportDataSet
				{
					GraduateDirectoryForms = graduateDirectoryForms
				};
			}
		}
	}
}
