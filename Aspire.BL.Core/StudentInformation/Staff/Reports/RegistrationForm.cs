﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FileServer;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.StudentInformation.Staff.Reports
{
	public static class RegistrationForm
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.Module, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Readonly);
		}

		public sealed class RegistrationFormPageHeader
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Title => $"APPLICATION / REGISTRATION FORM";
			public List<RegistrationFormPageHeader> GetPageHeaders()
			{
				return null;
			}
		}

		public sealed class StudentForRegistrationForm
		{
			public int StudentID { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public short MaxSemesterID { get; internal set; }
			public short? FinalSemesterID { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public int ProgramID { get; internal set; }
			public string ProgramShortName { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public byte Duration { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string PersonalEmail { get; internal set; }
			public string UniversityEmail { get; internal set; }
			public string CNIC { get; internal set; }
			public string PassportNo { get; internal set; }
			public byte Gender { get; internal set; }
			public byte Category { get; internal set; }
			public DateTime DOB { get; internal set; }
			public byte? BloodGroup { get; internal set; }
			public string Phone { get; internal set; }
			public string Mobile { get; internal set; }
			public string Nationality { get; internal set; }
			public string Country { get; internal set; }
			public string Province { get; internal set; }
			public string District { get; internal set; }
			public string Tehsil { get; internal set; }
			public string Domicile { get; internal set; }
			public byte? AreaType { get; internal set; }
			public string CurrentAddress { get; internal set; }
			public string PermanentAddress { get; internal set; }
			public string ServiceNo { get; internal set; }
			public string FatherName { get; internal set; }
			public string FatherCNIC { get; internal set; }
			public string FatherPassportNo { get; internal set; }
			public string FatherDesignation { get; internal set; }
			public string FatherDepartment { get; internal set; }
			public string FatherOrganization { get; internal set; }
			public string FatherServiceNo { get; internal set; }
			public string FatherOfficePhone { get; internal set; }
			public string FatherHomePhone { get; internal set; }
			public string FatherMobile { get; internal set; }
			public string FatherFax { get; internal set; }
			public byte? SponsoredBy { get; internal set; }
			public string SponsorName { get; internal set; }
			public string SponsorCNIC { get; internal set; }
			public string SponsorPassportNo { get; internal set; }
			public string SponsorRelationshipWithGuardian { get; internal set; }
			public string SponsorDesignation { get; internal set; }
			public string SponsorDepartment { get; internal set; }
			public string SponsorOrganization { get; internal set; }
			public string SponsorServiceNo { get; internal set; }
			public string SponsorPhoneHome { get; internal set; }
			public string SponsorMobile { get; internal set; }
			public string SponsorPhoneOffice { get; internal set; }
			public string SponsorFax { get; internal set; }
			public string EmergencyContactName { get; set; }
			public string EmergencyMobile { get; set; }
			public string EmergencyPhone { get; set; }
			public string EmergencyRelationship { get; set; }
			public string EmergencyAddress { get; set; }
			public string PhysicalDisability { get; set; }
			public byte AdmissionType { get; set; }
			public byte? ETSType { get; internal set; }
			public double? ETSObtained { get; internal set; }
			public double? ETSTotal { get; internal set; }
			public byte? EntryTestMarksPercentage { get; internal set; }
			public byte? Status { get; set; }
			public System.DateTime? StatusDate { get; set; }
			public byte[] Photo { get; internal set; }
			public List<StudentAcademicRecord> StudentAcademicRecords { get; internal set; }

			public string IntakeSemester => (this.IntakeSemesterID).ToSemesterString();
			public string MaxSemester => (this.MaxSemesterID).ToSemesterString();
			public string FinalSemester => (this.FinalSemesterID).ToSemesterString().ToNAIfNullOrEmpty();
			public string DOBShortFormat => this.DOB.ToString("d");
			public string GenderFullName => ((Genders)this.Gender).ToFullName();
			public string CategoryFullName => ((Categories)this.Category).ToFullName();
			public string BloodGroupFullName => ((BloodGroups?)this.BloodGroup)?.ToFullName();
			public string SponsorByFullName => ((Sponsorships?)this.SponsoredBy)?.ToFullName();
			public Model.Entities.Student.AdmissionTypes AdmissionTypeFullName => ((Model.Entities.Student.AdmissionTypes)this.AdmissionType);
			public string AreaTypeFullName => ((AreaTypes?)this.AreaType)?.ToFullName();
			public string ETSTypeFullName => ((ETSTypes?)this.ETSType)?.ToFullName();
			public int? ApplicationNo { get; internal set; }
			public short? ApplicationSemesterID { get; internal set; }
			public string ApplicationNoAndSemester => (this.ApplicationNo == null ? null : $"{this.ApplicationNo} - {this.ApplicationSemesterID.ToSemesterString()}").ToNAIfNullOrEmpty();
			public string Religion { get; internal set; }
			public string NextOfKin { get; internal set; }

			public sealed class StudentAcademicRecord
			{
				public int StudentID { get; internal set; }
				public byte DegreeType { get; internal set; }
				public double ObtainedMarks { get; internal set; }
				public double TotalMarks { get; internal set; }
				public double Percentage { get; internal set; }
				public bool IsCGPA { get; internal set; }
				public string Subjects { get; internal set; }
				public string Institute { get; internal set; }
				public string BoardUniversity { get; internal set; }
				public short PassingYear { get; internal set; }
				public byte? Status { get; internal set; }
				public string Remarks { get; internal set; }
				public string DegreeTypeFullName => ((DegreeTypes)this.DegreeType).ToFullName();

				public List<StudentAcademicRecord> GetList()
				{
					return null;
				}
			}

			public List<StudentForRegistrationForm> GetList()
			{
				return null;
			}
		}

		public class ReportDataSet
		{
			internal ReportDataSet() { }
			public RegistrationFormPageHeader PageHeader { get; internal set; }
			public List<StudentForRegistrationForm> StudentsForRegistrationForm { get; internal set; }
		}

		public static ReportDataSet GetStudentsForRegistrationForm(short semesterID, int? departmentID, int? programID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var students = aspireContext.Students
					.Where(s => s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(s => s.AdmissionOpenProgram.SemesterID == semesterID);
				if (departmentID != null)
					students = students.Where(s => s.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
				if (programID != null)
					students = students.Where(s => s.AdmissionOpenProgram.ProgramID == programID.Value);

				var studentsList = students.Select(s => new StudentForRegistrationForm
				{
					StudentID = s.StudentID,
					ApplicationNo = s.CandidateAppliedProgram.ApplicationNo,
					ApplicationSemesterID = s.CandidateAppliedProgram.AdmissionOpenProgram1.SemesterID,
					IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
					MaxSemesterID = s.AdmissionOpenProgram.MaxSemesterID,
					FinalSemesterID = s.AdmissionOpenProgram.FinalSemesterID,
					ProgramID = s.AdmissionOpenProgram.Program.ProgramID,
					ProgramShortName = s.AdmissionOpenProgram.Program.ProgramShortName,
					ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
					Duration = s.AdmissionOpenProgram.Program.Duration,
					RegistrationNo = s.RegistrationNo,
					Enrollment = s.Enrollment,
					Name = s.Name,
					Gender = s.Gender,
					Category = s.Category,
					CNIC = s.CNIC,
					PassportNo = s.PassportNo,
					Nationality = s.Nationality,
					DOB = s.DOB,
					Province = s.Province,
					District = s.District,
					Tehsil = s.Tehsil,
					Domicile = s.Domicile,
					PersonalEmail = s.PersonalEmail,
					UniversityEmail = s.UniversityEmail,
					CurrentAddress = s.CurrentAddress,
					Phone = s.Phone,
					Mobile = s.Mobile,
					PermanentAddress = s.PermanentAddress,
					Country = s.Country,
					FatherName = s.FatherName,
					FatherCNIC = s.FatherCNIC,
					FatherPassportNo = s.FatherPassportNo,
					FatherDesignation = s.FatherDesignation,
					FatherDepartment = s.FatherDepartment,
					FatherOrganization = s.FatherOrganization,
					SponsoredBy = s.SponsoredBy,
					SponsorName = s.SponsorName,
					SponsorCNIC = s.SponsorCNIC,
					SponsorPassportNo = s.SponsorPassportNo,
					SponsorDesignation = s.SponsorDesignation,
					SponsorDepartment = s.SponsorDepartment,
					SponsorOrganization = s.SponsorOrganization,
					ServiceNo = s.ServiceNo,
					SponsorPhoneHome = s.SponsorPhoneHome,
					EmergencyContactName = s.EmergencyContactName,
					EmergencyRelationship = s.EmergencyRelationship,
					EmergencyAddress = s.EmergencyAddress,
					EmergencyPhone = s.EmergencyPhone,
					EmergencyMobile = s.EmergencyMobile,
					PhysicalDisability = s.PhysicalDisability,
					AdmissionType = s.AdmissionType,
					ETSType = s.ETSType,
					ETSObtained = s.ETSObtained,
					ETSTotal = s.ETSTotal,
					EntryTestMarksPercentage = s.CandidateAppliedPrograms.Select(cap => cap.EntryTestMarksPercentage).FirstOrDefault(),
					Status = s.Status,
					StatusDate = s.StatusDate,
					Religion = s.Religion,
					NextOfKin = s.NextOfKin,
					StudentAcademicRecords = s.StudentAcademicRecords.Select(sar => new StudentForRegistrationForm.StudentAcademicRecord
					{
						StudentID = sar.StudentID,
						DegreeType = sar.DegreeType,
						ObtainedMarks = sar.ObtainedMarks,
						TotalMarks = sar.TotalMarks,
						Percentage = sar.Percentage,
						Institute = sar.Institute,
						BoardUniversity = sar.BoardUniversity,
						Subjects = sar.Subjects,
						PassingYear = sar.PassingYear,
						Status = sar.Status,
						IsCGPA = sar.IsCGPA,
						Remarks = sar.Remarks,
					}).ToList(),
				}).OrderBy(s => s.Enrollment).ToList();

				foreach (var student in studentsList)
					student.Photo =aspireContext.ReadStudentPicture(student.StudentID);

				return new ReportDataSet
				{
					PageHeader = new RegistrationFormPageHeader
					{
						SemesterID = semesterID,
						Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single(),
					},
					StudentsForRegistrationForm = studentsList,
				};
			}
		}
	}
}
