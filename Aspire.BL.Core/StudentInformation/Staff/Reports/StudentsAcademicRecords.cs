﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Aspire.Lib.Extensions;

namespace Aspire.BL.Core.StudentInformation.Staff.Reports
{
	public static class StudentsAcademicRecords
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class StudentAcademicPageHeader
		{
			public string Institute { get; internal set; }
			public short? SemesterID { get; internal set; }
			public DegreeTypes DegreeType { get; internal set; }
			public string Title => $"Student Academic Records ({this.SemesterID.ToSemesterString()})";
			public static List<StudentAcademicPageHeader> GetPageHeaders()
			{
				return null;
			}
		}

		public sealed class StudentAcademicRecord
		{
			public int StudentID { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public int ProgramID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string FatherName { get; internal set; }
			public byte Category { get; internal set; }
			public string Domicile { get; internal set; }
			public double TotalMarks { get; internal set; }
			public double ObtainedMarks { get; internal set; }
			public double Percentage { get; internal set; }
			public double? PercentageFilter { get; internal set; }
			public string Subjects { get; set; }
			public string Institute { get; set; }
			public string BoardUniversity { get; set; }
			public short PassingYear { get; set; }
			public string Remarks { get; internal set; }
			public byte DegreeType { get; internal set; }
			public byte? Status { get; internal set; }
			public byte? StudentAcademicStatus { get; internal set; }
			public System.DateTime? StatusDate { get; internal set; }
			public bool ForeignStudent { get; internal set; }

			public string StudentStatusFullName => ((Model.Entities.Student.Statuses?)this.Status)?.ToFullName() ?? string.Empty.ToNAIfNullOrEmpty();
			public string StudentAcademicStatusFullName => ((Model.Entities.StudentAcademicRecord.Statuses?)this.StudentAcademicStatus)?.ToString() ?? string.Empty.ToNAIfNullOrEmpty();
			public string DegreeTypeFullName => ((DegreeTypes)this.DegreeType).ToFullName().ToNAIfNullOrEmpty();
			public string CategoryFullName => ((Categories)this.Category).ToFullName().ToNAIfNullOrEmpty();
			public string RegistrationNoNA => this.RegistrationNo.ToString().ToNAIfNullOrEmpty();
			public string RemarksNA => this.Remarks.ToNAIfNullOrEmpty();
			public string ShortPercentage => this.Percentage.ToString("0.##");

			public static List<StudentAcademicRecord> GetStudentAcademicRecords()
			{
				return null;
			}
		}

		public class ReportDataSet
		{
			internal ReportDataSet() { }
			public StudentAcademicPageHeader PageHeader { get; internal set; }
			public List<StudentAcademicRecord> StudentsAcademicRecords { get; internal set; }
		}

		public static ReportDataSet GetStudentsAcademicRecords(short? semesterID, int? departmentID, int? programID, double? percentage, List<byte> degreeTypesList, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var studentAcademicRecords = aspireContext.StudentAcademicRecords
					.Where(sar => sar.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				if (semesterID != null)
					studentAcademicRecords = studentAcademicRecords.Where(sar => sar.Student.AdmissionOpenProgram.SemesterID == semesterID.Value);
				studentAcademicRecords = studentAcademicRecords.Where(sar => degreeTypesList.Contains(sar.DegreeType));
				if (departmentID != null)
					studentAcademicRecords = studentAcademicRecords.Where(sar => sar.Student.AdmissionOpenProgram.Program.DepartmentID == departmentID);
				if (programID != null)
					studentAcademicRecords = studentAcademicRecords.Where(sar => sar.Student.AdmissionOpenProgram.ProgramID == programID);
				if (percentage != null)
					studentAcademicRecords = studentAcademicRecords.Where(sar => sar.Percentage >= percentage.Value);
				var result = studentAcademicRecords.Select(sar => new StudentAcademicRecord
				{
					StudentID = sar.StudentID,
					RegistrationNo = sar.Student.RegistrationNo,
					ProgramID = sar.Student.AdmissionOpenProgram.ProgramID,
					ProgramAlias = sar.Student.AdmissionOpenProgram.Program.ProgramAlias,
					Enrollment = sar.Student.Enrollment,
					Name = sar.Student.Name,
					FatherName = sar.Student.FatherName,
					DegreeType = sar.DegreeType,
					Category = sar.Student.Category,
					Domicile = sar.Student.Domicile,
					TotalMarks = sar.TotalMarks,
					ObtainedMarks = sar.ObtainedMarks,
					Percentage = sar.Percentage,
					PercentageFilter = percentage ?? 0,
					Subjects = sar.Subjects,
					Institute = sar.Institute,
					BoardUniversity = sar.BoardUniversity,
					PassingYear = sar.PassingYear,
					Status = sar.Student.Status,
					StudentAcademicStatus = sar.Status,
					Remarks = sar.Remarks,
				}).ToList();

				return new ReportDataSet
				{
					PageHeader = new StudentAcademicPageHeader
					{
						SemesterID = semesterID,
						Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single(),
					},
					StudentsAcademicRecords = result,
				};
			}
		}

		public static List<CustomListItem> GetAdmissionOpenProgramsList(short semesterID, int? departmentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.AdmissionOpenPrograms.Where(aop => aop.SemesterID == semesterID && aop.Program.DepartmentID == departmentID).Select(aop => new CustomListItem
				{
					ID = aop.AdmissionOpenProgramID,
					Text = aop.Program.ProgramAlias,
				}).OrderByDescending(aop => aop.Text).ToList();
			}
		}
	}
}
