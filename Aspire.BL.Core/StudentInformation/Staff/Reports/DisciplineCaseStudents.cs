﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Aspire.Lib.Extensions;

namespace Aspire.BL.Core.StudentInformation.Staff.Reports
{
	public static class DisciplineCaseStudents
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class PageHeader
		{
			public string Institute { get; internal set; }
			public string Title => $"Student Discipline Case";

			public static List<PageHeader> GetPageHeaders()
			{
				return null;
			}

		}

		public sealed class DisciplineCaseStudent
		{
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public int DisciplineCaseID { get; internal set; }
			public string CaseFileNo { get; internal set; }
			public string Location { get; internal set; }
			public DateTime CaseDate { get; internal set; }
			public DateTime EntryDate { get; internal set; }
			public int DisciplineCasesStudentID { get; internal set; }
			public List<string> DisciplineCaseStudentActivities { get; internal set; }
			public string Mobile { get; internal set; }
			public string StudentStatement { get; internal set; }
			public byte? RemarksType { get; internal set; }
			public string Remarks { get; internal set; }
			public string OffenceActivities => string.Join(",", DisciplineCaseStudentActivities.Select(dcsa => dcsa));
			public string RemarksTypeEnumFullName => ((Model.Entities.DisciplineCasesStudent.RemarksTypes?)this.RemarksType).ToString().SplitCamelCasing();
			public string CaseDateString => this.CaseDate.ToString("dd-MMMM-yyyy hh:mm:ss tt");
			public string EntryDateString => this.EntryDate.ToString("dd-MMMM-yyyy");

			public static List<DisciplineCaseStudent> GetDisciplinceCaseStudents()
			{
				return null;
			}
		}

		public class ReportDataSet
		{
			internal ReportDataSet()
			{
			}

			public PageHeader PageHeader { get; internal set; }
			public List<DisciplineCaseStudent> DisciplineCaseStudents { get; internal set; }
		}

		public static ReportDataSet GetDisciplineCaseStudents(string caseFileNo, DateTime? offenceDateFrom, DateTime? offenceDateTo, string enrollment, int? offenceActivityID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var disciplineCasesStudents = aspireContext.DisciplineCasesStudents.Where(dcs => dcs.DisciplineCas.InstituteID == loginHistory.InstituteID);
				if (!string.IsNullOrEmpty(enrollment))
					disciplineCasesStudents = disciplineCasesStudents.Where(dcs => dcs.Student.Enrollment == enrollment);
				else if (!string.IsNullOrEmpty(caseFileNo))
					disciplineCasesStudents = disciplineCasesStudents.Where(dcs => dcs.DisciplineCas.CaseFileNo == caseFileNo);
				else if (offenceActivityID != null)
					disciplineCasesStudents = disciplineCasesStudents.Where(dcs => dcs.DisciplineCaseStudentActivities.Any(dcsa=>dcsa.DisciplineCaseActivityID==offenceActivityID));
				else
				{
					if (offenceDateFrom != null && offenceDateTo == null)
						disciplineCasesStudents = disciplineCasesStudents.Where(dcs => dcs.DisciplineCas.CaseDate >= offenceDateFrom);
					if (offenceDateTo != null && offenceDateFrom == null)
						disciplineCasesStudents = disciplineCasesStudents.Where(dcs => dcs.DisciplineCas.CaseDate <= offenceDateTo);
					if (offenceDateFrom != null && offenceDateTo != null)
						disciplineCasesStudents = disciplineCasesStudents.Where(dcs => dcs.DisciplineCas.CaseDate >= offenceDateFrom && dcs.DisciplineCas.CaseDate <= offenceDateTo);

				}
				var disciplineCaseStudents = disciplineCasesStudents.Select(dcs => new DisciplineCaseStudent
				{
					StudentID = dcs.StudentID,
					Enrollment = dcs.Student.Enrollment,
					Name = dcs.Student.Name,
					ProgramAlias = dcs.Student.AdmissionOpenProgram.Program.ProgramAlias,
					DisciplineCaseID = dcs.DisciplineCaseID,
					CaseFileNo = dcs.DisciplineCas.CaseFileNo,
					CaseDate = dcs.DisciplineCas.CaseDate,
					Location = dcs.DisciplineCas.Location,
					EntryDate = dcs.DisciplineCas.EntryDate,
					DisciplineCasesStudentID = dcs.DisciplineCasesStudentID,
					DisciplineCaseStudentActivities = dcs.DisciplineCaseStudentActivities.Select(dcsa => dcsa.DisciplineCaseActivity.ActivityName).ToList(),
					Mobile = dcs.Mobile,
					StudentStatement = dcs.StudentsStatement,
					RemarksType = dcs.RemarksType,
					Remarks = dcs.Remarks
				});
				return new ReportDataSet
				{
					PageHeader = new PageHeader
					{
						Institute = aspireContext.Institutes.Single(i => i.InstituteID == loginHistory.InstituteID).InstituteName,
					},
					DisciplineCaseStudents = disciplineCaseStudents.ToList(),
				};
			}
		}
	}
}
