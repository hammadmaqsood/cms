﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.StudentInformation.Staff.Reports
{
	public static class StudentInformation
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class PageHeader
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Title => $"Students Information Semester ({this.SemesterID.ToSemesterString()})";
			public static List<PageHeader> GetPageHeaders()
			{
				return null;
			}
		}

		public sealed class Student
		{
			public string InstituteAlias { get; internal set; }
			public int StudentID { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public int? ApplicationNo { get; internal set; }
			public string DepartmentName { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string ProgramShortName { get; internal set; }
			public byte Duration { get; internal set; }
			public string Enrollment { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string Name { get; internal set; }
			public byte Gender { get; internal set; }
			public string FatherName { get; internal set; }
			public string PersonalEmail { get; internal set; }
			public string UniversityEmail { get; internal set; }
			public DateTime DOB { get; internal set; }
			public byte? BloodGroup { get; internal set; }
			public string CNIC { get; internal set; }
			public byte Category { get; internal set; }
			public string Phone { get; internal set; }
			public string Mobile { get; internal set; }
			public string CurrentAddress { get; internal set; }
			public string PermanentAddress { get; internal set; }
			public string Nationality { get; internal set; }
			public string Country { get; internal set; }
			public string Province { get; internal set; }
			public string District { get; internal set; }
			public string Tehsil { get; internal set; }
			public string Domicile { get; internal set; }
			public byte? AreaType { get; internal set; }
			public string PhysicalDisability { get; internal set; }
			public bool? CreditTransfer { get; internal set; }
			public string PassportNo { get; internal set; }
			public string FatherCNIC { get; internal set; }
			public string FatherPassportNo { get; internal set; }
			public string FatherDesignation { get; internal set; }
			public string FatherDepartment { get; internal set; }
			public string FatherOrganization { get; internal set; }
			public string FatherServiceNo { get; internal set; }
			public string FatherOfficePhone { get; internal set; }
			public string FatherHomePhone { get; internal set; }
			public string FatherMobile { get; internal set; }
			public string FatherFax { get; internal set; }
			public byte? SponsoredBy { get; internal set; }
			public string SponsorName { get; internal set; }
			public string SponsorCNIC { get; internal set; }
			public string SponsorPassportNo { get; internal set; }
			public string SponsorRelationshipWithGuardian { get; internal set; }
			public string SponsorDesignation { get; internal set; }
			public string SponsorDepartment { get; internal set; }
			public string SponsorOrganization { get; internal set; }
			public string SponsorServiceNo { get; internal set; }
			public string SponsorPhoneHome { get; internal set; }
			public string SponsorMobile { get; internal set; }
			public string SponsorPhoneOffice { get; internal set; }
			public string SponsorFax { get; internal set; }
			public short? SemesterNo { get; internal set; }
			public int? Section { get; internal set; }
			public byte? Shift { get; internal set; }
			public DateTime? EntryTestDate { get; internal set; }
			public byte? EntryTestMarksPercentage { get; internal set; }
			public byte? ETSType { get; internal set; }
			public double? ETSObtained { get; internal set; }
			public double? ETSTotal { get; internal set; }
			public byte AdmissionType { get; internal set; }
			public byte? Status { get; internal set; }
			public string StudentAcademicStatusFullName { get; internal set; }
			public DateTime? StatusDate { get; internal set; }
			public string DegreeTypeFullName { get; internal set; }
			public double TotalMarksNational { get; internal set; }
			public double ObtainedMarksNational { get; internal set; }
			public string TotalMarksInternational { get; internal set; }
			public string ObtainedMarksInternational { get; internal set; }
			public string ShortPercentage { get; internal set; }
			public int? PercentageFilter { get; internal set; }
			public string RemarksNA { get; internal set; }
			public string Subjects { get; internal set; }
			public string Institute { get; internal set; }
			public string BoardUniversity { get; internal set; }
			public short PassingYear { get; internal set; }
			public FreezedStatuses? SemesterFreezeStatusEnum { get; set; }
			public bool? ForeignStudent { get; set; }

			public string GenderFullName => ((Genders)this.Gender).ToFullName();
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public string AreaTypeFullName => ((AreaTypes?)this.AreaType)?.ToFullName() ?? string.Empty.ToNAIfNullOrEmpty();
			public string SponsoredByFullName => ((Sponsorships?)this.SponsoredBy)?.ToFullName() ?? string.Empty.ToNAIfNullOrEmpty();
			public string CategoryFullName => ((Categories)this.Category).ToFullName();
			public string BloodGroupFullName => ((BloodGroups?)this.BloodGroup)?.ToFullName();
			public string StudentStatusFullName => ((Model.Entities.Student.Statuses?)this.Status)?.ToFullName() ?? string.Empty.ToNAIfNullOrEmpty();
			public string StatusDateOnly => this.StatusDate?.ToString("d");
			public string DOBDateOnly => this.DOB.ToString("d");
			public string EntryTestDateOnly => this.EntryTestDate?.ToString("d");
			public string DurationFullName => ((ProgramDurations)this.Duration).ToFullName();
			public string IntakeSemester => this.IntakeSemesterID.ToSemesterString();
			public string CreditTranferYesNo => this.CreditTransfer?.ToYesNo();
			public string SemesterFreezeStatusEnumFullName => this.SemesterFreezeStatusEnum?.ToFullName();

			public string DegreeNo { get; internal set; }
			public DateTime? DegreeIssueDate { get; internal set; }
			public int? DegreeGazetteNo { get; internal set; }
			public DateTime? DegreeIssuedToStudent { get; internal set; }

			public string TotalMarks
			{
				get
				{
					if (this.ForeignStudent != null)
					{
						if (!this.ForeignStudent.Value)
							return this.TotalMarksNational.ToString();
						else
							return this.TotalMarksInternational;
					}
					return this.TotalMarksNational.ToString();

				}
			}
			public string ObtainedMarks
			{
				get
				{
					if (this.ForeignStudent != null)
					{
						if (!this.ForeignStudent.Value)
							return this.ObtainedMarksNational.ToString();
						else
							return this.ObtainedMarksInternational;
					}
					return this.ObtainedMarksNational.ToString();
				}
			}


			#region CourseRegistrationDetails

			public string OfferedCourseCode { get; internal set; }
			public string OfferedTitle { get; internal set; }
			public decimal OfferedCreditHours { get; internal set; }
			public string RoadMapCourseCode { get; internal set; }
			public string RoadMapTitle { get; internal set; }
			public decimal RoadMapCreditHours { get; internal set; }
			public string OfferedProgramAlias { get; internal set; }
			public short? OfferedSemesterNo { get; internal set; }
			public int? OfferedSection { get; internal set; }
			public byte? OfferedShift { get; internal set; }
			public RegisteredCours.Statuses? RegisteredCourseStatusEnum { get; internal set; }
			public FreezedStatuses? RegisteredCourseFreezedStatusEnum { get; internal set; }
			public byte? Grade { get; internal set; }
			public decimal? GradePoints { get; internal set; }
			public decimal? Product { get; internal set; }

			public string OfferedClass => AspireFormats.GetClassName(this.OfferedProgramAlias, this.OfferedSemesterNo, this.OfferedSection, this.OfferedShift);
			public string RegisteredCourseStatusEnumFullName => this.RegisteredCourseStatusEnum?.ToFullName();
			public string RegisteredCourseFreezedStatusEnumFullName => this.RegisteredCourseFreezedStatusEnum?.ToFullName();

			#endregion

			public string EntryTestMarksComplete
			{
				get
				{
					if ((this.ETSType == null || this.ETSObtained == null || this.ETSTotal == null) && this.EntryTestMarksPercentage != null) return $"{this.EntryTestMarksPercentage}";
					if (this.ETSType != null && this.ETSObtained != null && this.ETSTotal != null)
					{
						var etsPercentage = (byte)Math.Round(this.ETSObtained.Value * 100d / this.ETSTotal.Value, MidpointRounding.AwayFromZero);
						var marks = this.EntryTestMarksPercentage ?? 0;
						if (this.EntryTestDate == null || this.EntryTestMarksPercentage == null)
							return $"{etsPercentage}";
						if (etsPercentage > marks)
							return $"{etsPercentage}";
						return $"{marks}";
					}
					else
					{
						return "N/A";
					}
				}
			}

			public static List<Student> GetStudentInformation()
			{
				return null;
			}
		}

		public class ReportDataSet
		{
			internal ReportDataSet() { }
			public PageHeader PageHeader { get; internal set; }
			public List<Student> StudentsInformation { get; internal set; }
		}

		public static ReportDataSet GetStudentsInformation(short semesterID, int? departmentID, int? programID, string studentStatusText, byte? studentStatus, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var studentsInformation = aspireContext.Students.Where(s => s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				studentsInformation = studentsInformation.Where(s => s.AdmissionOpenProgram.SemesterID == semesterID);
				if (departmentID != null)
					studentsInformation = studentsInformation.Where(s => s.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
				if (programID != null)
					studentsInformation = studentsInformation.Where(s => s.AdmissionOpenProgram.ProgramID == programID.Value);
				if (studentStatusText == "None")
					studentsInformation = studentsInformation.Where(s => s.Status == null);
				else
				{
					if (studentStatus != null)
						studentsInformation = studentsInformation.Where(s => s.Status == studentStatus);
				}
				var result = studentsInformation.Select(s => new Student
				{
					StudentID = s.StudentID,
					IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
					DepartmentName = s.AdmissionOpenProgram.Program.Department.DepartmentName,
					ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
					Duration = s.AdmissionOpenProgram.Program.Duration,
					Enrollment = s.Enrollment,
					RegistrationNo = s.RegistrationNo,
					Name = s.Name,
					FatherName = s.FatherName,
					PersonalEmail = s.PersonalEmail,
					UniversityEmail = s.UniversityEmail,
					DOB = s.DOB,
					Gender = s.Gender,
					CNIC = s.CNIC,
					Category = s.Category,
					Phone = s.Phone,
					Mobile = s.Mobile,
					CurrentAddress = s.CurrentAddress,
					PermanentAddress = s.PermanentAddress,
					Domicile = s.Domicile,
					PassportNo = s.PassportNo,
					EntryTestDate = aspireContext.CandidateAppliedPrograms.FirstOrDefault(cap => cap.CandidateAppliedProgramID == s.CandidateAppliedProgramID).EntryTestDate,/*s.CandidateAppliedPrograms.FirstOrDefault().EntryTestDate,*/
					EntryTestMarksPercentage = aspireContext.CandidateAppliedPrograms.FirstOrDefault(cap => cap.CandidateAppliedProgramID == s.CandidateAppliedProgramID).EntryTestMarksPercentage,
					ETSType = s.ETSType,
					ETSObtained = s.ETSObtained,
					ETSTotal = s.ETSTotal,
					Status = s.Status,
					StatusDate = s.StatusDate,
				}).OrderBy(o => o.Enrollment).ThenBy(t => t.RegistrationNo).ThenBy(t => t.DepartmentName).ThenBy(t => t.ProgramAlias).ToList();

				return new ReportDataSet
				{
					PageHeader = new PageHeader
					{
						SemesterID = semesterID,
						Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single()
					},
					StudentsInformation = result
				};
			}
		}

		public enum ReportTypes
		{
			StudentsInformation,
			StudentsInformationCourseRegistrationWise,
			StudentsInformationWithRegisteredCoursesDetails,
			StudentsAcademicRecords,
			StudentsInformationWithAcademicRecordsNational,
			StudentsInformationWithAcademicRecordsInternational,
		}

		public static List<Student> GetStudentsInformationAndAcademicRecords(short? semesterID, int? departmentID, int? programID, ReportTypes reportType, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var studentsInformation = aspireContext.Students.Where(s => s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				var candidateForeignAcadematicRecords = aspireContext.CandidateForeignAcademicRecords.AsEnumerable();
				if (semesterID != null)
				{
					studentsInformation = studentsInformation.Where(s => s.AdmissionOpenProgram.SemesterID == semesterID.Value);
					candidateForeignAcadematicRecords = candidateForeignAcadematicRecords.Where(cfar => cfar.Candidate.SemesterID == semesterID);
				}
				if (departmentID != null)
					studentsInformation = studentsInformation.Where(s => s.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
				if (programID != null)
					studentsInformation = studentsInformation.Where(s => s.AdmissionOpenProgram.ProgramID == programID.Value);

				var studentInfo = studentsInformation.Select(s => new
				{
					InstituteAlias = s.AdmissionOpenProgram.Program.Institute.InstituteAlias,
					StudentID = s.StudentID,
					IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
					DepartmentName = s.AdmissionOpenProgram.Program.Department.DepartmentName,
					ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
					Duration = s.AdmissionOpenProgram.Program.Duration,
					Enrollment = s.Enrollment,
					ApplicationNo = aspireContext.CandidateAppliedPrograms.FirstOrDefault(cap => cap.CandidateAppliedProgramID == s.CandidateAppliedProgramID).ApplicationNo,
					RegistrationNo = s.RegistrationNo,
					Name = s.Name,
					PersonalEmail = s.PersonalEmail,
					UniversityEmail = s.UniversityEmail,
					DOB = s.DOB,
					BloodGroup = s.BloodGroup,
					Gender = s.Gender,
					CNIC = s.CNIC,
					Category = s.Category,
					Phone = s.Phone,
					Mobile = s.Mobile,
					CurrentAddress = s.CurrentAddress,
					PermanentAddress = s.PermanentAddress,
					PassportNo = s.PassportNo,
					Nationality = s.Nationality,
					Country = s.Country,
					Province = s.Province,
					District = s.District,
					Tehsil = s.Tehsil,
					Domicile = s.Domicile,
					AreaType = s.AreaType,
					PhysicalDisability = s.PhysicalDisability,
					CreditTransfer = (bool?)aspireContext.CandidateAppliedPrograms.FirstOrDefault(cap => cap.CandidateAppliedProgramID == s.CandidateAppliedProgramID).CreditTransfer,
					FatherName = s.FatherName,
					FatherCNIC = s.FatherCNIC,
					FatherDesignation = s.FatherDesignation,
					FatherDepartment = s.FatherDepartment,
					FatherOrganization = s.FatherOrganization,
					FatherPassportNo = s.FatherPassportNo,
					FatherMobile = s.FatherMobile,
					FatherHomePhone = s.FatherHomePhone,
					SponsoredBy = s.SponsoredBy,
					SponsorName = s.SponsorName,
					SponsorCNIC = s.SponsorCNIC,
					SponsorDesignation = s.SponsorDesignation,
					SponsorDepartment = s.SponsorDepartment,
					SponsorOrganization = s.SponsorOrganization,
					SponsorRelationshipWithGuardian = s.SponsorRelationshipWithGuardian,
					SponsorPassportNo = s.SponsorPassportNo,
					SponsorMobile = s.SponsorMobile,
					SponsorPhoneHome = s.SponsorPhoneHome,
					SemesterNo = (short?)s.StudentSemesters.FirstOrDefault(ss => ss.SemesterID == s.AdmissionOpenProgram.SemesterID).SemesterNo,
					Section = (int?)s.StudentSemesters.FirstOrDefault(ss => ss.SemesterID == s.AdmissionOpenProgram.SemesterID).Section,
					Shift = (byte?)s.StudentSemesters.FirstOrDefault(ss => ss.SemesterID == s.AdmissionOpenProgram.SemesterID).Shift,
					EntryTestDate = aspireContext.CandidateAppliedPrograms.FirstOrDefault(cap => cap.CandidateAppliedProgramID == s.CandidateAppliedProgramID).EntryTestDate, /*s.CandidateAppliedPrograms.FirstOrDefault().EntryTestDate,*/
					EntryTestMarksPercentage = aspireContext.CandidateAppliedPrograms.FirstOrDefault(cap => cap.CandidateAppliedProgramID == s.CandidateAppliedProgramID).EntryTestMarksPercentage,
					ETSType = s.ETSType,
					ETSObtained = s.ETSObtained,
					ETSTotal = s.ETSTotal,
					Status = s.Status,
					StatusDate = s.StatusDate,
					DegreeNo = s.DegreeNo,
					DegreeIssueDate = s.DegreeIssueDate,
					DegreeGazetteNo = s.DegreeGazetteNo,
					DegreeIssuedToStudent = s.DegreeIssuedToStudent,
					CandidateID = s.CandidateAppliedProgramID != null ? s.CandidateAppliedProgram.CandidateID : -1,
					ForeignStudent = s.CandidateAppliedProgramID != null ? s.CandidateAppliedProgram.Candidate.ForeignStudent : false,
				}).ToList();
				var foreignStudents = studentInfo.Where(s => s?.ForeignStudent ?? false).AsEnumerable();

				var studentAcademicRecords = BL.Core.StudentInformation.Staff.Reports.StudentsAcademicRecords.GetStudentsAcademicRecords(semesterID, departmentID, programID, 0, Enum.GetValues(typeof(DegreeTypes)).Cast<byte>().ToList(), loginSessionGuid);
				var _sar = studentAcademicRecords.StudentsAcademicRecords;
				IEnumerable<Student> result;
				switch (reportType)
				{
					case ReportTypes.StudentsInformation:
						result = from s in studentInfo
								 select new Student
								 {
									 InstituteAlias = s.InstituteAlias,
									 IntakeSemesterID = s.IntakeSemesterID,
									 RegistrationNo = s.RegistrationNo,
									 ApplicationNo = s.ApplicationNo,
									 Enrollment = s.Enrollment,
									 Name = s.Name,
									 DepartmentName = s.DepartmentName,
									 ProgramAlias = s.ProgramAlias,
									 SemesterNo = s.SemesterNo,
									 Section = s.Section,
									 Shift = s.Shift,
									 Duration = s.Duration,
									 PersonalEmail = s.PersonalEmail,
									 UniversityEmail = s.UniversityEmail,
									 Gender = s.Gender,
									 CNIC = s.CNIC,
									 DOB = s.DOB,
									 BloodGroup = s.BloodGroup,
									 Category = s.Category,
									 Phone = s.Phone,
									 Mobile = s.Mobile,
									 CurrentAddress = s.CurrentAddress,
									 PermanentAddress = s.PermanentAddress,
									 Nationality = s.Nationality,
									 PassportNo = s.PassportNo,
									 Country = s.Country,
									 Province = s.Province,
									 District = s.District,
									 Tehsil = s.Tehsil,
									 Domicile = s.Domicile,
									 AreaType = s.AreaType,
									 PhysicalDisability = s.PhysicalDisability,
									 CreditTransfer = s.CreditTransfer,
									 FatherName = s.FatherName,
									 FatherCNIC = s.FatherCNIC,
									 FatherDesignation = s.FatherDesignation,
									 FatherDepartment = s.FatherDepartment,
									 FatherOrganization = s.FatherOrganization,
									 FatherPassportNo = s.FatherPassportNo,
									 FatherMobile = s.FatherMobile,
									 FatherHomePhone = s.FatherHomePhone,
									 SponsoredBy = s.SponsoredBy,
									 SponsorName = s.SponsorName,
									 SponsorCNIC = s.SponsorCNIC,
									 SponsorDesignation = s.SponsorDesignation,
									 SponsorDepartment = s.SponsorDepartment,
									 SponsorOrganization = s.SponsorOrganization,
									 SponsorRelationshipWithGuardian = s.SponsorRelationshipWithGuardian,
									 SponsorPassportNo = s.SponsorPassportNo,
									 SponsorMobile = s.SponsorMobile,
									 SponsorPhoneHome = s.SponsorPhoneHome,
									 EntryTestDate = s.EntryTestDate,
									 EntryTestMarksPercentage = s.EntryTestMarksPercentage,
									 ETSType = s.ETSType,
									 ETSObtained = s.ETSObtained,
									 ETSTotal = s.ETSTotal,
									 Status = s.Status,
									 StatusDate = s.StatusDate,
									 DegreeNo = s.DegreeNo,
									 DegreeIssueDate = s.DegreeIssueDate,
									 DegreeGazetteNo = s.DegreeGazetteNo,
									 DegreeIssuedToStudent = s.DegreeIssuedToStudent,
									 ForeignStudent = s.ForeignStudent,
								 };
						break;
					case ReportTypes.StudentsAcademicRecords:
						result = from sar in _sar
								 select new Student
								 {
									 Enrollment = sar.Enrollment,
									 DegreeTypeFullName = sar.DegreeTypeFullName,
									 ObtainedMarksNational = sar.ObtainedMarks,
									 TotalMarksNational = sar.TotalMarks,
									 ShortPercentage = sar.ShortPercentage,
									 Subjects = sar.Subjects,
									 Institute = sar.Institute,
									 BoardUniversity = sar.BoardUniversity,
									 PassingYear = sar.PassingYear,
									 RemarksNA = sar.RemarksNA,
									 StudentAcademicStatusFullName = sar.StudentAcademicStatusFullName,
									 ForeignStudent = sar.ForeignStudent,
								 };
						break;
					case ReportTypes.StudentsInformationWithAcademicRecordsNational:
						result = from sar in _sar
								 join s in studentInfo on sar.StudentID equals s.StudentID
								 select new Student
								 {
									 InstituteAlias = s.InstituteAlias,
									 IntakeSemesterID = s.IntakeSemesterID,
									 RegistrationNo = s.RegistrationNo,
									 ApplicationNo = s.ApplicationNo,
									 Enrollment = s.Enrollment,
									 Name = s.Name,
									 DepartmentName = s.DepartmentName,
									 ProgramAlias = s.ProgramAlias,
									 SemesterNo = s.SemesterNo,
									 Section = s.Section,
									 Shift = s.Shift,
									 Duration = s.Duration,
									 PersonalEmail = s.PersonalEmail,
									 UniversityEmail = s.UniversityEmail,
									 Gender = s.Gender,
									 CNIC = s.CNIC,
									 DOB = s.DOB,
									 BloodGroup = s.BloodGroup,
									 Category = s.Category,
									 Phone = s.Phone,
									 Mobile = s.Mobile,
									 CurrentAddress = s.CurrentAddress,
									 PermanentAddress = s.PermanentAddress,
									 Nationality = s.Nationality,
									 PassportNo = s.PassportNo,
									 Country = s.Country,
									 Province = s.Province,
									 District = s.District,
									 Tehsil = s.Tehsil,
									 Domicile = s.Domicile,
									 AreaType = s.AreaType,
									 PhysicalDisability = s.PhysicalDisability,
									 CreditTransfer = s.CreditTransfer,
									 FatherName = s.FatherName,
									 FatherCNIC = s.FatherCNIC,
									 FatherDesignation = s.FatherDesignation,
									 FatherDepartment = s.FatherDepartment,
									 FatherOrganization = s.FatherOrganization,
									 FatherPassportNo = s.FatherPassportNo,
									 FatherMobile = s.FatherMobile,
									 FatherHomePhone = s.FatherHomePhone,
									 SponsoredBy = s.SponsoredBy,
									 SponsorName = s.SponsorName,
									 SponsorCNIC = s.SponsorCNIC,
									 SponsorDesignation = s.SponsorDesignation,
									 SponsorDepartment = s.SponsorDepartment,
									 SponsorOrganization = s.SponsorOrganization,
									 SponsorRelationshipWithGuardian = s.SponsorRelationshipWithGuardian,
									 SponsorPassportNo = s.SponsorPassportNo,
									 SponsorMobile = s.SponsorMobile,
									 SponsorPhoneHome = s.SponsorPhoneHome,
									 EntryTestDate = s.EntryTestDate,
									 EntryTestMarksPercentage = s.EntryTestMarksPercentage,
									 ETSType = s.ETSType,
									 ETSObtained = s.ETSObtained,
									 ETSTotal = s.ETSTotal,
									 Status = s.Status,
									 StatusDate = s.StatusDate,
									 ForeignStudent = s.ForeignStudent,
									 DegreeTypeFullName = sar.DegreeTypeFullName,
									 ObtainedMarksNational = sar.ObtainedMarks,
									 TotalMarksNational = sar.TotalMarks,
									 ShortPercentage = sar.ShortPercentage,
									 Subjects = sar.Subjects,
									 Institute = sar.Institute,
									 BoardUniversity = sar.BoardUniversity,
									 PassingYear = sar.PassingYear,
									 RemarksNA = sar.RemarksNA,
									 StudentAcademicStatusFullName = sar.StudentAcademicStatusFullName,
									 DegreeNo = s.DegreeNo,
									 DegreeIssueDate = s.DegreeIssueDate,
									 DegreeGazetteNo = s.DegreeGazetteNo,
									 DegreeIssuedToStudent = s.DegreeIssuedToStudent,
								 };
						break;
					case ReportTypes.StudentsInformationWithAcademicRecordsInternational:
						result = from cfar in candidateForeignAcadematicRecords
								 join s in foreignStudents on cfar.CandidateID equals s.CandidateID
								 select new Student
								 {
									 InstituteAlias = s.InstituteAlias,
									 IntakeSemesterID = s.IntakeSemesterID,
									 RegistrationNo = s.RegistrationNo,
									 ApplicationNo = s.ApplicationNo,
									 Enrollment = s.Enrollment,
									 Name = s.Name,
									 DepartmentName = s.DepartmentName,
									 ProgramAlias = s.ProgramAlias,
									 SemesterNo = s.SemesterNo,
									 Section = s.Section,
									 Shift = s.Shift,
									 Duration = s.Duration,
									 PersonalEmail = s.PersonalEmail,
									 UniversityEmail = s.UniversityEmail,
									 Gender = s.Gender,
									 CNIC = s.CNIC,
									 DOB = s.DOB,
									 BloodGroup = s.BloodGroup,
									 Category = s.Category,
									 Phone = s.Phone,
									 Mobile = s.Mobile,
									 CurrentAddress = s.CurrentAddress,
									 PermanentAddress = s.PermanentAddress,
									 Nationality = s.Nationality,
									 PassportNo = s.PassportNo,
									 Country = s.Country,
									 Province = s.Province,
									 District = s.District,
									 Tehsil = s.Tehsil,
									 Domicile = s.Domicile,
									 AreaType = s.AreaType,
									 PhysicalDisability = s.PhysicalDisability,
									 CreditTransfer = s.CreditTransfer,
									 FatherName = s.FatherName,
									 FatherCNIC = s.FatherCNIC,
									 FatherDesignation = s.FatherDesignation,
									 FatherDepartment = s.FatherDepartment,
									 FatherOrganization = s.FatherOrganization,
									 FatherPassportNo = s.FatherPassportNo,
									 FatherMobile = s.FatherMobile,
									 FatherHomePhone = s.FatherHomePhone,
									 SponsoredBy = s.SponsoredBy,
									 SponsorName = s.SponsorName,
									 SponsorCNIC = s.SponsorCNIC,
									 SponsorDesignation = s.SponsorDesignation,
									 SponsorDepartment = s.SponsorDepartment,
									 SponsorOrganization = s.SponsorOrganization,
									 SponsorRelationshipWithGuardian = s.SponsorRelationshipWithGuardian,
									 SponsorPassportNo = s.SponsorPassportNo,
									 SponsorMobile = s.SponsorMobile,
									 SponsorPhoneHome = s.SponsorPhoneHome,
									 EntryTestDate = s.EntryTestDate,
									 EntryTestMarksPercentage = s.EntryTestMarksPercentage,
									 ETSType = s.ETSType,
									 ETSObtained = s.ETSObtained,
									 ETSTotal = s.ETSTotal,
									 Status = s.Status,
									 StatusDate = s.StatusDate,
									 ForeignStudent = s.ForeignStudent,
									 DegreeTypeFullName = cfar.DegreeName,
									 ObtainedMarksInternational = cfar.ObtainedMarks,
									 TotalMarksInternational = cfar.TotalMarks,
									 ShortPercentage = "",
									 Subjects = cfar.Subjects,
									 Institute = cfar.Institute,
									 BoardUniversity = "",
									 PassingYear = cfar.PassingYear,
									 RemarksNA = "",
									 StudentAcademicStatusFullName = "",
									 DegreeNo = s.DegreeNo,
									 DegreeIssueDate = s.DegreeIssueDate,
									 DegreeGazetteNo = s.DegreeGazetteNo,
									 DegreeIssuedToStudent = s.DegreeIssuedToStudent,
								 };
						break;
					case ReportTypes.StudentsInformationCourseRegistrationWise:
					case ReportTypes.StudentsInformationWithRegisteredCoursesDetails:
						throw new NotImplementedEnumException(reportType);
					default:
						throw new ArgumentOutOfRangeException(nameof(reportType), reportType, null);
				}
				return result.OrderBy(o => o.Enrollment).ThenBy(t => t.RegistrationNo).ThenBy(t => t.DepartmentName).ThenBy(t => t.ProgramAlias).ToList();
			}
		}

		public static List<Student> GetStudentsInformationCourseRegistrationWise(short? semesterID, int? departmentID, int? programID, short? semesterNo, int? section, byte? shift, ReportTypes reportType, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var studentsInformation = aspireContext.Students.Where(s => s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				var studentSemesters = aspireContext.StudentSemesters.AsQueryable();
				if (semesterID != null)
					studentSemesters = studentSemesters.Where(ss => ss.SemesterID == semesterID.Value);
				var registeredCourses = aspireContext.RegisteredCourses.Where(rc => rc.DeletedDate == null);
				if (semesterID != null)
					registeredCourses = registeredCourses.Where(rc => rc.OfferedCours.SemesterID == semesterID.Value);
				if (departmentID != null)
					studentsInformation = studentsInformation.Where(s => s.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
				if (programID != null)
					studentsInformation = studentsInformation.Where(s => s.AdmissionOpenProgram.ProgramID == programID.Value);
				if (semesterNo != null)
					studentSemesters = studentSemesters.Where(ss => ss.SemesterNo == semesterNo.Value);
				if (section != null)
					studentSemesters = studentSemesters.Where(ss => ss.Section == section.Value);
				if (shift != null)
					studentSemesters = studentSemesters.Where(ss => ss.Shift == shift.Value);

				var studentInfo = studentsInformation.Select(s => new
				{
					InstituteAlias = s.AdmissionOpenProgram.Program.Institute.InstituteAlias,
					StudentID = s.StudentID,
					IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
					DepartmentName = s.AdmissionOpenProgram.Program.Department.DepartmentName,
					ProgramShortName = s.AdmissionOpenProgram.Program.ProgramShortName,
					ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
					Duration = s.AdmissionOpenProgram.Program.Duration,
					Enrollment = s.Enrollment,
					ApplicationNo = aspireContext.CandidateAppliedPrograms.FirstOrDefault(cap => cap.CandidateAppliedProgramID == s.CandidateAppliedProgramID).ApplicationNo,
					RegistrationNo = s.RegistrationNo,
					Name = s.Name,
					PersonalEmail = s.PersonalEmail,
					UniversityEmail = s.UniversityEmail,
					DOB = s.DOB,
					BloodGroup = s.BloodGroup,
					Gender = s.Gender,
					CNIC = s.CNIC,
					Category = s.Category,
					Phone = s.Phone,
					Mobile = s.Mobile,
					CurrentAddress = s.CurrentAddress,
					PermanentAddress = s.PermanentAddress,
					PassportNo = s.PassportNo,
					Nationality = s.Nationality,
					Country = s.Country,
					Province = s.Province,
					District = s.District,
					Tehsil = s.Tehsil,
					Domicile = s.Domicile,
					AreaType = s.AreaType,
					PhysicalDisability = s.PhysicalDisability,
					CreditTransfer = (bool?)aspireContext.CandidateAppliedPrograms.FirstOrDefault(cap => cap.CandidateAppliedProgramID == s.CandidateAppliedProgramID).CreditTransfer,
					FatherName = s.FatherName,
					FatherCNIC = s.FatherCNIC,
					FatherDesignation = s.FatherDesignation,
					FatherDepartment = s.FatherDepartment,
					FatherOrganization = s.FatherOrganization,
					FatherPassportNo = s.FatherPassportNo,
					FatherMobile = s.FatherMobile,
					FatherHomePhone = s.FatherHomePhone,
					SponsoredBy = s.SponsoredBy,
					SponsorName = s.SponsorName,
					SponsorCNIC = s.SponsorCNIC,
					SponsorDesignation = s.SponsorDesignation,
					SponsorDepartment = s.SponsorDepartment,
					SponsorOrganization = s.SponsorOrganization,
					SponsorRelationshipWithGuardian = s.SponsorRelationshipWithGuardian,
					SponsorPassportNo = s.SponsorPassportNo,
					SponsorMobile = s.SponsorMobile,
					SponsorPhoneHome = s.SponsorPhoneHome,
					Status = s.Status,
				}).ToList();

				switch (reportType)
				{
					case ReportTypes.StudentsInformation:
					case ReportTypes.StudentsAcademicRecords:
					case ReportTypes.StudentsInformationWithAcademicRecordsNational:
					case ReportTypes.StudentsInformationWithAcademicRecordsInternational:
						return null;
					case ReportTypes.StudentsInformationCourseRegistrationWise:

						var result = from s in studentInfo
									 join ss in studentSemesters on s.StudentID equals ss.StudentID
									 join rc in registeredCourses on s.StudentID equals rc.StudentID
									 group new { s, ss } by new
									 {
										 InstituteAlias = s.InstituteAlias,
										 IntakeSemesterID = s.IntakeSemesterID,
										 RegistrationNo = s.RegistrationNo,
										 ApplicationNo = s.ApplicationNo,
										 Enrollment = s.Enrollment,
										 Name = s.Name,
										 DepartmentName = s.DepartmentName,
										 ProgramAlias = s.ProgramAlias,
										 ProgramShortName = s.ProgramShortName,
										 SemesterNo = ss.SemesterNo,
										 Section = ss.Section,
										 Shift = ss.Shift,
										 Duration = s.Duration,
										 PersonalEmail = s.PersonalEmail,
										 UniversityEmail = s.UniversityEmail,
										 Gender = s.Gender,
										 CNIC = s.CNIC,
										 DOB = s.DOB,
										 BloodGroup = s.BloodGroup,
										 Category = s.Category,
										 Phone = s.Phone,
										 Mobile = s.Mobile,
										 CurrentAddress = s.CurrentAddress,
										 PermanentAddress = s.PermanentAddress,
										 Nationality = s.Nationality,
										 PassportNo = s.PassportNo,
										 Country = s.Country,
										 Province = s.Province,
										 District = s.District,
										 Tehsil = s.Tehsil,
										 Domicile = s.Domicile,
										 AreaType = s.AreaType,
										 PhysicalDisability = s.PhysicalDisability,
										 CreditTransfer = s.CreditTransfer,
										 FatherName = s.FatherName,
										 FatherCNIC = s.FatherCNIC,
										 FatherDesignation = s.FatherDesignation,
										 FatherDepartment = s.FatherDepartment,
										 FatherOrganization = s.FatherOrganization,
										 FatherPassportNo = s.FatherPassportNo,
										 FatherMobile = s.FatherMobile,
										 FatherHomePhone = s.FatherHomePhone,
										 SponsoredBy = s.SponsoredBy,
										 SponsorName = s.SponsorName,
										 SponsorCNIC = s.SponsorCNIC,
										 SponsorDesignation = s.SponsorDesignation,
										 SponsorDepartment = s.SponsorDepartment,
										 SponsorOrganization = s.SponsorOrganization,
										 SponsorRelationshipWithGuardian = s.SponsorRelationshipWithGuardian,
										 SponsorPassportNo = s.SponsorPassportNo,
										 SponsorMobile = s.SponsorMobile,
										 SponsorPhoneHome = s.SponsorPhoneHome,
										 Status = s.Status,
										 SemesterFreezeStatusEnum = ss.FreezedStatusEnum,
									 } into g
									 select new Student
									 {
										 InstituteAlias = g.Key.InstituteAlias,
										 IntakeSemesterID = g.Key.IntakeSemesterID,
										 RegistrationNo = g.Key.RegistrationNo,
										 ApplicationNo = g.Key.ApplicationNo,
										 Enrollment = g.Key.Enrollment,
										 Name = g.Key.Name,
										 DepartmentName = g.Key.DepartmentName,
										 ProgramAlias = g.Key.ProgramAlias,
										 ProgramShortName = g.Key.ProgramShortName,
										 SemesterNo = g.Key.SemesterNo,
										 Section = g.Key.Section,
										 Shift = g.Key.Shift,
										 Duration = g.Key.Duration,
										 PersonalEmail = g.Key.PersonalEmail,
										 UniversityEmail = g.Key.UniversityEmail,
										 Gender = g.Key.Gender,
										 CNIC = g.Key.CNIC,
										 DOB = g.Key.DOB,
										 BloodGroup = g.Key.BloodGroup,
										 Category = g.Key.Category,
										 Phone = g.Key.Phone,
										 Mobile = g.Key.Mobile,
										 CurrentAddress = g.Key.CurrentAddress,
										 PermanentAddress = g.Key.PermanentAddress,
										 Nationality = g.Key.Nationality,
										 PassportNo = g.Key.PassportNo,
										 Country = g.Key.Country,
										 Province = g.Key.Province,
										 District = g.Key.District,
										 Tehsil = g.Key.Tehsil,
										 Domicile = g.Key.Domicile,
										 AreaType = g.Key.AreaType,
										 PhysicalDisability = g.Key.PhysicalDisability,
										 CreditTransfer = g.Key.CreditTransfer,
										 FatherName = g.Key.FatherName,
										 FatherCNIC = g.Key.FatherCNIC,
										 FatherDesignation = g.Key.FatherDesignation,
										 FatherDepartment = g.Key.FatherDepartment,
										 FatherOrganization = g.Key.FatherOrganization,
										 FatherPassportNo = g.Key.FatherPassportNo,
										 FatherMobile = g.Key.FatherMobile,
										 FatherHomePhone = g.Key.FatherHomePhone,
										 SponsoredBy = g.Key.SponsoredBy,
										 SponsorName = g.Key.SponsorName,
										 SponsorCNIC = g.Key.SponsorCNIC,
										 SponsorDesignation = g.Key.SponsorDesignation,
										 SponsorDepartment = g.Key.SponsorDepartment,
										 SponsorOrganization = g.Key.SponsorOrganization,
										 SponsorRelationshipWithGuardian = g.Key.SponsorRelationshipWithGuardian,
										 SponsorPassportNo = g.Key.SponsorPassportNo,
										 SponsorMobile = g.Key.SponsorMobile,
										 SponsorPhoneHome = g.Key.SponsorPhoneHome,
										 Status = g.Key.Status,
										 SemesterFreezeStatusEnum = g.Key.SemesterFreezeStatusEnum,
									 };
						return result.OrderBy(o => o.Enrollment).ToList();
					case ReportTypes.StudentsInformationWithRegisteredCoursesDetails:
						var resultWithCourses = from s in studentInfo
												join ss in studentSemesters on s.StudentID equals ss.StudentID
												join rc in registeredCourses on s.StudentID equals rc.StudentID
												select new Student
												{
													InstituteAlias = s.InstituteAlias,
													IntakeSemesterID = s.IntakeSemesterID,
													RegistrationNo = s.RegistrationNo,
													ApplicationNo = s.ApplicationNo,
													Enrollment = s.Enrollment,
													Name = s.Name,
													DepartmentName = s.DepartmentName,
													ProgramAlias = s.ProgramAlias,
													ProgramShortName = s.ProgramShortName,
													SemesterNo = ss.SemesterNo,
													Section = ss.Section,
													Shift = ss.Shift,
													Duration = s.Duration,
													PersonalEmail = s.PersonalEmail,
													UniversityEmail = s.UniversityEmail,
													Gender = s.Gender,
													CNIC = s.CNIC,
													DOB = s.DOB,
													BloodGroup = s.BloodGroup,
													Category = s.Category,
													Phone = s.Phone,
													Mobile = s.Mobile,
													CurrentAddress = s.CurrentAddress,
													PermanentAddress = s.PermanentAddress,
													Nationality = s.Nationality,
													PassportNo = s.PassportNo,
													Country = s.Country,
													Province = s.Province,
													District = s.District,
													Tehsil = s.Tehsil,
													Domicile = s.Domicile,
													AreaType = s.AreaType,
													PhysicalDisability = s.PhysicalDisability,
													CreditTransfer = s.CreditTransfer,
													FatherName = s.FatherName,
													FatherCNIC = s.FatherCNIC,
													FatherDesignation = s.FatherDesignation,
													FatherDepartment = s.FatherDepartment,
													FatherOrganization = s.FatherOrganization,
													FatherPassportNo = s.FatherPassportNo,
													FatherMobile = s.FatherMobile,
													FatherHomePhone = s.FatherHomePhone,
													SponsoredBy = s.SponsoredBy,
													SponsorName = s.SponsorName,
													SponsorCNIC = s.SponsorCNIC,
													SponsorDesignation = s.SponsorDesignation,
													SponsorDepartment = s.SponsorDepartment,
													SponsorOrganization = s.SponsorOrganization,
													SponsorRelationshipWithGuardian = s.SponsorRelationshipWithGuardian,
													SponsorPassportNo = s.SponsorPassportNo,
													SponsorMobile = s.SponsorMobile,
													SponsorPhoneHome = s.SponsorPhoneHome,
													Status = s.Status,
													SemesterFreezeStatusEnum = ss.FreezedStatusEnum,
													OfferedProgramAlias = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
													OfferedCourseCode = rc.OfferedCours.Cours.CourseCode,
													OfferedTitle = rc.OfferedCours.Cours.Title,
													OfferedCreditHours = rc.OfferedCours.Cours.CreditHours,
													RoadMapCourseCode = rc.Cours.CourseCode,
													RoadMapTitle = rc.Cours.Title,
													RoadMapCreditHours = rc.Cours.CreditHours,
													OfferedSemesterNo = rc.OfferedCours.SemesterNo,
													OfferedSection = rc.OfferedCours.Section,
													OfferedShift = rc.OfferedCours.Shift,
													RegisteredCourseStatusEnum = rc.StatusEnum,
													RegisteredCourseFreezedStatusEnum = rc.FreezeStatusEnum,
													Grade = rc.OfferedCours.MarksEntryLocked ? rc.Grade : null,
													GradePoints = rc.OfferedCours.MarksEntryLocked ? rc.GradePoints : null,
													Product = rc.OfferedCours.MarksEntryLocked ? rc.Product : null,
												};
						return resultWithCourses.OrderBy(o => o.Enrollment).ToList();
					default:
						throw new ArgumentOutOfRangeException(nameof(reportType), reportType, null);
				}

			}
		}
	}
}

