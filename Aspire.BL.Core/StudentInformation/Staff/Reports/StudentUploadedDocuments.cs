﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Aspire.BL.Core.StudentInformation.Staff.Reports
{
	public static class StudentUploadedDocuments
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.ViewStudentDocuments, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class StudentDocuments
		{
			internal StudentDocuments() { }
			public Guid StudentDocumentsID { get; internal set; }
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string Nationality { get; internal set; }
			public byte? StudentStatus { get; internal set; }
			public Guid? StudentDocumentsStatus { get; internal set; }
			public Model.Entities.Student.StudentDocumentsStatuses? StudentDocumentsStatusEnum => this.StudentDocumentsStatus?.GetEnum<Model.Entities.Student.StudentDocumentsStatuses>();
			public short SemesterID { get; internal set; }
			public Guid DocumentType { get; internal set; }
			public Model.Entities.StudentDocument.DocumentTypes DocumentTypeEnum => this.DocumentType.GetEnum<Model.Entities.StudentDocument.DocumentTypes>();
			public Guid DocumentPages { get; internal set; }
			public StudentDocument.Pages DocumentPagesEnum => this.DocumentPages.GetEnum<StudentDocument.Pages>();
			public string Remarks { get; internal set; }
			public Guid? DocumentsStatus { get; internal set; }
			public StudentDocument.Statuses? StatusEnum => this.DocumentsStatus?.GetEnum<StudentDocument.Statuses>();
		}

		public static DataTable GetUploadedDocuments(short semesterID, int programID, Model.Entities.Student.StudentDocumentsStatuses? studentDocumentsStatusesEnum, bool? pakistani, List<Model.Entities.StudentDocument.DocumentTypes> groupByFields, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.IncreaseCommandTimeout();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);

				var studentDocumentsStatus = studentDocumentsStatusesEnum?.GetGuid();
				var query = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID)
					.SelectMany(a => a.Programs).Where(p => p.ProgramID == programID)
					.SelectMany(b => b.AdmissionOpenPrograms).Where(aop => aop.SemesterID == semesterID)
					.SelectMany(c => c.Students).Where(s => studentDocumentsStatus == null || s.StudentDocumentsStatus == studentDocumentsStatus)
					.Where(s => pakistani == null || pakistani == true ? s.Nationality == "PAKISTANI" : s.Nationality != "PAKISTANI")
					.Select(s => s.StudentID);
				
				var studentDocuments = aspireContext.StudentDocuments.Where(sd => query.Contains(sd.StudentID)).Select(sd => new StudentDocuments
				{
					StudentID = sd.StudentID,
					Enrollment = sd.Student.Enrollment,
					Name = sd.Student.Name,
					ProgramAlias = sd.Student.AdmissionOpenProgram.Program.ProgramAlias,
					Nationality = sd.Student.Nationality,
					StudentStatus = sd.Student.Status,
					StudentDocumentsStatus = sd.Student.StudentDocumentsStatus,
					SemesterID = sd.Student.AdmissionOpenProgram.SemesterID,
					StudentDocumentsID = sd.StudentDocumentID,
					DocumentType = sd.DocumentType,
					DocumentPages = sd.Page,
					Remarks = sd.Remarks,
					DocumentsStatus = sd.Status,
				});

				#region GetGuidFromEnums								
				var acceptanceForm = StudentDocument.DocumentTypes.AcceptanceForm.GetGuid();
				var aLevelEquivalenceCertificateFromIBCC = StudentDocument.DocumentTypes.ALevelEquivalenceCertificateFromIBCC.GetGuid();
				var aLevelGeneralCertificateFromCambridge = StudentDocument.DocumentTypes.ALevelGeneralCertificateFromCambridge.GetGuid();
				var bachelors2YearsDegree = StudentDocument.DocumentTypes.Bachelors2YearsDegree.GetGuid();
				var bachelors2YearsMarksSheet = StudentDocument.DocumentTypes.Bachelors2YearsMarksSheet.GetGuid();
				var bachelors4YearsDegree = StudentDocument.DocumentTypes.Bachelors4YearsDegree.GetGuid();
				var bachelors4YearsMarksSheet = StudentDocument.DocumentTypes.Bachelors4YearsMarksSheet.GetGuid();
				var bachelorsEquivalenceCertificateFromHEC = StudentDocument.DocumentTypes.BachelorsEquivalenceCertificateFromHEC.GetGuid();
				var bdsDegree = StudentDocument.DocumentTypes.BDSDegree.GetGuid();
				var cnic = StudentDocument.DocumentTypes.CNIC.GetGuid();
				var confidentialForm = StudentDocument.DocumentTypes.ConfidentialForm.GetGuid();
				var consolidatedTranscript = StudentDocument.DocumentTypes.ConsolidatedTranscript.GetGuid();
				var domicile = StudentDocument.DocumentTypes.Domicile.GetGuid();
				var formB = StudentDocument.DocumentTypes.FormB.GetGuid();
				var gatScore = StudentDocument.DocumentTypes.GATScore.GetGuid();
				var houseJobCertificate = StudentDocument.DocumentTypes.HouseJobCertificate.GetGuid();
				var hsscCertificate = StudentDocument.DocumentTypes.HSSCCertificate.GetGuid();
				var hsscMarksSheet = StudentDocument.DocumentTypes.HSSCMarksSheet.GetGuid();
				var interimTranscript = StudentDocument.DocumentTypes.InterimTranscript.GetGuid();
				var mastersDegree = StudentDocument.DocumentTypes.MastersDegree.GetGuid();
				var mastersEquivalenceCertificateFromHEC = StudentDocument.DocumentTypes.MastersEquivalenceCertificateFromHEC.GetGuid();
				var mastersMarksSheet = StudentDocument.DocumentTypes.MastersMarksSheet.GetGuid();
				var mbbsDegree = StudentDocument.DocumentTypes.MBBSDegree.GetGuid();
				var medicalReport = StudentDocument.DocumentTypes.MedicalReport.GetGuid();
				var msMPhilDegree = StudentDocument.DocumentTypes.MSMPhilDegree.GetGuid();
				var msMPhilEquivalenceCertificateFromHEC = StudentDocument.DocumentTypes.MSMPhilEquivalenceCertificateFromHEC.GetGuid();
				var msMPhilMarksSheet = StudentDocument.DocumentTypes.MSMPhilMarksSheet.GetGuid();
				var oLevelEquivalenceCertificateFromIBCC = StudentDocument.DocumentTypes.OLevelEquivalenceCertificateFromIBCC.GetGuid();
				var oLevelGeneralCertificateFromCambridge = StudentDocument.DocumentTypes.OLevelGeneralCertificateFromCambridge.GetGuid();
				var passport = StudentDocument.DocumentTypes.Passport.GetGuid();
				var pmdCRegistrationForm = StudentDocument.DocumentTypes.PMDCRegistrationForm.GetGuid();
				var sat2Score = StudentDocument.DocumentTypes.SAT2Score.GetGuid();
				var sscCertificate = StudentDocument.DocumentTypes.SSCCertificate.GetGuid();
				var sscMarksSheet = StudentDocument.DocumentTypes.SSCMarksSheet.GetGuid();
				var undertaking = StudentDocument.DocumentTypes.Undertaking.GetGuid();
				#endregion

				#region GroupingDataQuery
				var uploadedData = studentDocuments.GroupBy(g => new
				{
					g.StudentID,
					g.Enrollment,
					g.Name,
					g.ProgramAlias,
					g.Nationality,
					g.StudentDocumentsStatus,
					g.StudentStatus,
					g.SemesterID,
				}).Select(s => new
				{
					s.Key,
					s.Key.Enrollment,
					s.Key.Name,
					s.Key.Nationality,
					acceptanceFormStatus = s.Where(gg => gg.DocumentType == acceptanceForm).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					aLevelEquivalenceCertificateFromIBCCStatus = s.Where(gg => gg.DocumentType == aLevelEquivalenceCertificateFromIBCC).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					aLevelGeneralCertificateFromCambridgeStatus = s.Where(gg => gg.DocumentType == aLevelGeneralCertificateFromCambridge).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					bachelors2YearsDegreeStatus = s.Where(gg => gg.DocumentType == bachelors2YearsDegree).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					bachelors2YearsMarksSheetStatus = s.Where(gg => gg.DocumentType == bachelors2YearsMarksSheet).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					bachelors4YearsDegreeStatus = s.Where(gg => gg.DocumentType == bachelors4YearsDegree).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					bachelors4YearsMarksSheetStatus = s.Where(gg => gg.DocumentType == bachelors4YearsMarksSheet).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					bachelorsEquivalenceCertificateFromHECStatus = s.Where(gg => gg.DocumentType == bachelorsEquivalenceCertificateFromHEC).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					bdsDegreeStatus = s.Where(gg => gg.DocumentType == bdsDegree).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					cnicStatus = s.Where(gg => gg.DocumentType == cnic).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					confidentialFormStatus = s.Where(gg => gg.DocumentType == confidentialForm).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					consolidatedTranscriptStatus = s.Where(gg => gg.DocumentType == consolidatedTranscript).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					domicileStatus = s.Where(gg => gg.DocumentType == domicile).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					formBStatus = s.Where(gg => gg.DocumentType == formB).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					gatScoreStatus = s.Where(gg => gg.DocumentType == gatScore).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					houseJobCertificateStatus = s.Where(gg => gg.DocumentType == houseJobCertificate).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					hsscCertificateStatus = s.Where(gg => gg.DocumentType == hsscCertificate).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					hsscMarksSheetStatus = s.Where(gg => gg.DocumentType == hsscMarksSheet).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					interimTranscriptStatus = s.Where(gg => gg.DocumentType == interimTranscript).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					mastersDegreeStatus = s.Where(gg => gg.DocumentType == mastersDegree).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					mastersEquivalenceCertificateFromHECStatus = s.Where(gg => gg.DocumentType == mastersEquivalenceCertificateFromHEC).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					mastersMarksSheetStatus = s.Where(gg => gg.DocumentType == mastersMarksSheet).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					mbbsDegreeStatus = s.Where(gg => gg.DocumentType == mbbsDegree).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					medicalReportStatus = s.Where(gg => gg.DocumentType == medicalReport).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					msMPhilDegreeStatus = s.Where(gg => gg.DocumentType == msMPhilDegree).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					msMPhilEquivalenceCertificateFromHECStatus = s.Where(gg => gg.DocumentType == msMPhilEquivalenceCertificateFromHEC).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					msMPhilMarksSheetStatus = s.Where(gg => gg.DocumentType == msMPhilMarksSheet).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					oLevelEquivalenceCertificateFromIBCCStatus = s.Where(gg => gg.DocumentType == oLevelEquivalenceCertificateFromIBCC).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					oLevelGeneralCertificateFromCambridgeStatus = s.Where(gg => gg.DocumentType == oLevelGeneralCertificateFromCambridge).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					passportStatus = s.Where(gg => gg.DocumentType == passport).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					pmdCRegistrationFormStatus = s.Where(gg => gg.DocumentType == pmdCRegistrationForm).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					sat2ScoreStatus = s.Where(gg => gg.DocumentType == sat2Score).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					sscCertificateStatus = s.Where(gg => gg.DocumentType == sscCertificate).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					sscMarksSheetStatus = s.Where(gg => gg.DocumentType == sscMarksSheet).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
					undertakingStatus = s.Where(gg => gg.DocumentType == undertaking).Select(sgg => sgg.DocumentsStatus).FirstOrDefault(),
				}).ToList().OrderBy(o => o.Key.Enrollment);
				#endregion

				#region GetSelectedColumns
				var groupByCNIC = groupByFields.Contains(StudentDocument.DocumentTypes.CNIC);
				var groupByPassport = groupByFields.Contains(StudentDocument.DocumentTypes.Passport);
				var groupByFormB = groupByFields.Contains(StudentDocument.DocumentTypes.FormB);
				var groupByDomicile = groupByFields.Contains(StudentDocument.DocumentTypes.Domicile);
				var groupBySSCMarksSheet = groupByFields.Contains(StudentDocument.DocumentTypes.SSCMarksSheet);
				var groupBySSCCertificate = groupByFields.Contains(StudentDocument.DocumentTypes.SSCCertificate);
				var groupByHSSCMarksSheet = groupByFields.Contains(StudentDocument.DocumentTypes.HSSCMarksSheet);
				var groupByHSSCCertificate = groupByFields.Contains(StudentDocument.DocumentTypes.HSSCCertificate);
				var groupByAcceptanceForm = groupByFields.Contains(StudentDocument.DocumentTypes.AcceptanceForm);
				var groupByALevelEquivalenceCertificateFromIBCC = groupByFields.Contains(StudentDocument.DocumentTypes.ALevelEquivalenceCertificateFromIBCC);
				var groupByALevelGeneralCertificateFromCambridge = groupByFields.Contains(StudentDocument.DocumentTypes.ALevelGeneralCertificateFromCambridge);
				var groupByBachelors2YearsDegree = groupByFields.Contains(StudentDocument.DocumentTypes.Bachelors2YearsDegree);
				var groupByBachelors2YearsMarksSheet = groupByFields.Contains(StudentDocument.DocumentTypes.Bachelors2YearsMarksSheet);
				var groupByBachelors4YearsDegree = groupByFields.Contains(StudentDocument.DocumentTypes.Bachelors4YearsDegree);
				var groupByBachelors4YearsMarksSheet = groupByFields.Contains(StudentDocument.DocumentTypes.Bachelors4YearsMarksSheet);
				var groupByBachelorsEquivalenceCertificateFromHEC = groupByFields.Contains(StudentDocument.DocumentTypes.BachelorsEquivalenceCertificateFromHEC);
				var groupByBDSDegree = groupByFields.Contains(StudentDocument.DocumentTypes.BDSDegree);
				var groupByConfidentialForm = groupByFields.Contains(StudentDocument.DocumentTypes.ConfidentialForm);
				var groupByConsolidatedTranscript = groupByFields.Contains(StudentDocument.DocumentTypes.ConsolidatedTranscript);
				var groupByGATScore = groupByFields.Contains(StudentDocument.DocumentTypes.GATScore);
				var groupByHouseJobCertificate = groupByFields.Contains(StudentDocument.DocumentTypes.HouseJobCertificate);
				var groupByInterimTranscript = groupByFields.Contains(StudentDocument.DocumentTypes.InterimTranscript);
				var groupByMastersDegree = groupByFields.Contains(StudentDocument.DocumentTypes.MastersDegree);
				var groupByMastersEquivalenceCertificateFromHEC = groupByFields.Contains(StudentDocument.DocumentTypes.MastersEquivalenceCertificateFromHEC);
				var groupByMastersMarksSheet = groupByFields.Contains(StudentDocument.DocumentTypes.MastersMarksSheet);
				var groupByMBBSDegree = groupByFields.Contains(StudentDocument.DocumentTypes.MBBSDegree);
				var groupByMedicalReport = groupByFields.Contains(StudentDocument.DocumentTypes.MedicalReport);
				var groupByMSMPhilDegree = groupByFields.Contains(StudentDocument.DocumentTypes.MSMPhilDegree);
				var groupByMSMPhilEquivalenceCertificateFromHEC = groupByFields.Contains(StudentDocument.DocumentTypes.MSMPhilEquivalenceCertificateFromHEC);
				var groupByMSMPhilMarksSheet = groupByFields.Contains(StudentDocument.DocumentTypes.MSMPhilMarksSheet);
				var groupByOLevelEquivalenceCertificateFromIBCC = groupByFields.Contains(StudentDocument.DocumentTypes.OLevelEquivalenceCertificateFromIBCC);
				var groupByOLevelGeneralCertificateFromCambridge = groupByFields.Contains(StudentDocument.DocumentTypes.OLevelGeneralCertificateFromCambridge);
				var groupByPMDCRegistrationForm = groupByFields.Contains(StudentDocument.DocumentTypes.PMDCRegistrationForm);
				var groupBySAT2Score = groupByFields.Contains(StudentDocument.DocumentTypes.SAT2Score);
				var groupByUndertaking = groupByFields.Contains(StudentDocument.DocumentTypes.Undertaking);
				#endregion

				#region AddColumnsInDataTable

				var table = new DataTable("UploadedDocuments");

				int? acceptanceFormColumnIndex = null;
				int? aLevelEquivalenceCertificateFromIBCCColumnIndex = null;
				int? aLevelGeneralCertificateFromCambridgeColumnIndex = null;
				int? bachelors2YearsDegreeColumnIndex = null;
				int? bachelors2YearsMarksSheetColumnIndex = null;
				int? bachelors4YearsDegreeColumnIndex = null;
				int? bachelors4YearsMarksSheetColumnIndex = null;
				int? bachelorsEquivalenceCertificateFromHECColumnIndex = null;
				int? bdsDegreeColumnIndex = null;
				int? cnicColumnIndex = null;
				int? confidentialFormColumnIndex = null;
				int? consolidatedTranscriptColumnIndex = null;
				int? domicileColumnIndex = null;
				int? formBColumnIndex = null;
				int? gatScoreColumnIndex = null;
				int? houseJobCertificateColumnIndex = null;
				int? hsscCertificateColumnIndex = null;
				int? hsscMarksSheetColumnIndex = null;
				int? interimTranscriptColumnIndex = null;
				int? mastersDegreeColumnIndex = null;
				int? mastersEquivalenceCertificateFromHECColumnIndex = null;
				int? mastersMarksSheetColumnIndex = null;
				int? mbbsDegreeColumnIndex = null;
				int? medicalReportColumnIndex = null;
				int? msMPhilDegreeColumnIndex = null;
				int? msMPhilEquivalenceCertificateFromHECColumnIndex = null;
				int? msMPhilMarksSheetColumnIndex = null;
				int? oLevelEquivalenceCertificateFromIBCCColumnIndex = null;
				int? oLevelGeneralCertificateFromCambridgeColumnIndex = null;
				int? passportColumnIndex = null;
				int? pmdCRegistrationFormColumnIndex = null;
				int? sat2ScoreColumnIndex = null;
				int? sscCertificateColumnIndex = null;
				int? sscMarksSheetColumnIndex = null;
				int? undertakingColumnIndex = null;

				var sortExpressions = new List<string>();
				table.Columns.Add("Enrollment", typeof(string));
				table.Columns.Add("Name", typeof(string));
				table.Columns.Add("Program", typeof(string));
				table.Columns.Add("Nationality", typeof(string));
				table.Columns.Add("Semester", typeof(string));
				table.Columns.Add("Student Status", typeof(string));
				table.Columns.Add("Documents Status", typeof(string));

				if (groupByAcceptanceForm)
				{
					acceptanceFormColumnIndex = table.Columns.Add("Acceptance Form", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[acceptanceFormColumnIndex.Value].ColumnName);
				}
				if (groupByALevelEquivalenceCertificateFromIBCC)
				{
					aLevelEquivalenceCertificateFromIBCCColumnIndex = table.Columns.Add("A-Level - Equivalence Certificate (From IBCC)", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[aLevelEquivalenceCertificateFromIBCCColumnIndex.Value].ColumnName);
				}
				if (groupByALevelGeneralCertificateFromCambridge)
				{
					aLevelGeneralCertificateFromCambridgeColumnIndex = table.Columns.Add("A-Level - General Certificate of Education (From Cambridge)", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[aLevelGeneralCertificateFromCambridgeColumnIndex.Value].ColumnName);
				}
				if (groupByBachelors2YearsDegree)
				{
					bachelors2YearsDegreeColumnIndex = table.Columns.Add("Bachelors (2 Years) Degree", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[bachelors2YearsDegreeColumnIndex.Value].ColumnName);
				}
				if (groupByBachelors2YearsMarksSheet)
				{
					bachelors2YearsMarksSheetColumnIndex = table.Columns.Add("Bachelors (2 Years) Marks Sheet/Transcript", typeof(string)).Ordinal; sortExpressions.Add(table.Columns[bachelors2YearsMarksSheetColumnIndex.Value].ColumnName);
				}
				if (groupByBachelors4YearsDegree)
				{
					bachelors4YearsDegreeColumnIndex = table.Columns.Add("Bachelors (4 Years) Degree", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[bachelors4YearsDegreeColumnIndex.Value].ColumnName);
				}
				if (groupByBachelors4YearsMarksSheet)
				{
					bachelors4YearsMarksSheetColumnIndex = table.Columns.Add("Bachelors (4 Years) Marks Sheet/Transcript", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[bachelors4YearsMarksSheetColumnIndex.Value].ColumnName);
				}
				if (groupByBachelorsEquivalenceCertificateFromHEC)
				{
					bachelorsEquivalenceCertificateFromHECColumnIndex = table.Columns.Add("Bachelors - HEC Equivalence Certificate for Foreign Degree", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[bachelorsEquivalenceCertificateFromHECColumnIndex.Value].ColumnName);
				}
				if (groupByBDSDegree)
				{
					bdsDegreeColumnIndex = table.Columns.Add("BDS Degree", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[bdsDegreeColumnIndex.Value].ColumnName);
				}
				if (groupByCNIC)
				{
					cnicColumnIndex = table.Columns.Add("CNIC", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[cnicColumnIndex.Value].ColumnName);
				}
				if (groupByConfidentialForm)
				{
					confidentialFormColumnIndex = table.Columns.Add("Confidential Form", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[confidentialFormColumnIndex.Value].ColumnName);
				}
				if (groupByConsolidatedTranscript)
				{
					consolidatedTranscriptColumnIndex = table.Columns.Add("Consolidated Transcript", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[consolidatedTranscriptColumnIndex.Value].ColumnName);
				}
				if (groupByDomicile)
				{
					domicileColumnIndex = table.Columns.Add("Domicile", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[domicileColumnIndex.Value].ColumnName);
				}
				if (groupByFormB)
				{
					formBColumnIndex = table.Columns.Add("Form B", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[formBColumnIndex.Value].ColumnName);
				}
				if (groupByGATScore)
				{
					gatScoreColumnIndex = table.Columns.Add("GAT Score", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[gatScoreColumnIndex.Value].ColumnName);
				}
				if (groupByHouseJobCertificate)
				{
					houseJobCertificateColumnIndex = table.Columns.Add("House Job Certificate", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[houseJobCertificateColumnIndex.Value].ColumnName);
				}
				if (groupByHSSCMarksSheet)
				{
					hsscMarksSheetColumnIndex = table.Columns.Add("HSSC Certificate", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[hsscMarksSheetColumnIndex.Value].ColumnName);
				}
				if (groupByHSSCCertificate)
				{
					hsscCertificateColumnIndex = table.Columns.Add("HSSC MarksSheet", typeof(string)).Ordinal;
					sortExpressions.Add(table.
						Columns[hsscCertificateColumnIndex.Value].ColumnName);
				}
				if (groupByInterimTranscript)
				{
					interimTranscriptColumnIndex = table.Columns.Add("Interim Transcript", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[interimTranscriptColumnIndex.Value].ColumnName);
				}
				if (groupByMastersDegree)
				{
					mastersDegreeColumnIndex = table.Columns.Add("Masters Degree", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[mastersDegreeColumnIndex.Value].ColumnName);
				}
				if (groupByMastersEquivalenceCertificateFromHEC)
				{
					mastersEquivalenceCertificateFromHECColumnIndex = table.Columns.Add("Masters - HEC Equivalence Certificate for Foreign Degree", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[mastersEquivalenceCertificateFromHECColumnIndex.Value].ColumnName);
				}
				if (groupByMastersMarksSheet)
				{
					mastersMarksSheetColumnIndex = table.Columns.Add("Masters Marks Sheet/Transcript", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[mastersMarksSheetColumnIndex.Value].ColumnName);
				}
				if (groupByMBBSDegree)
				{
					mbbsDegreeColumnIndex = table.Columns.Add("MBBS Degree", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[mbbsDegreeColumnIndex.Value].ColumnName);
				}
				if (groupByMedicalReport)
				{
					medicalReportColumnIndex = table.Columns.Add("Medical Report", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[medicalReportColumnIndex.Value].ColumnName);
				}
				if (groupByMSMPhilDegree)
				{
					msMPhilDegreeColumnIndex = table.Columns.Add("MS/M.Phil Degree", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[msMPhilDegreeColumnIndex.Value].ColumnName);
				}
				if (groupByMSMPhilEquivalenceCertificateFromHEC)
				{
					msMPhilEquivalenceCertificateFromHECColumnIndex = table.Columns.Add("MS/M.Phil - HEC Equivalence Certificate for Foreign Degree", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[msMPhilEquivalenceCertificateFromHECColumnIndex.Value].ColumnName);
				}
				if (groupByMSMPhilMarksSheet)
				{
					msMPhilMarksSheetColumnIndex = table.Columns.Add("MS/M.Phil Marks Sheet/Transcript", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[msMPhilMarksSheetColumnIndex.Value].ColumnName);
				}
				if (groupByOLevelEquivalenceCertificateFromIBCC)
				{
					oLevelEquivalenceCertificateFromIBCCColumnIndex = table.Columns.Add("O-Level - Equivalence Certificate (From IBCC)", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[oLevelEquivalenceCertificateFromIBCCColumnIndex.Value].ColumnName);
				}
				if (groupByOLevelGeneralCertificateFromCambridge)
				{
					oLevelGeneralCertificateFromCambridgeColumnIndex = table.Columns.Add("O-Level - General Certificate of Education (From Cambridge)", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[oLevelGeneralCertificateFromCambridgeColumnIndex.Value].ColumnName);
				}
				if (groupByPassport)
				{
					passportColumnIndex = table.Columns.Add("Passport", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[passportColumnIndex.Value].ColumnName);
				}
				if (groupByPMDCRegistrationForm)
				{
					pmdCRegistrationFormColumnIndex = table.Columns.Add("PMDC Registration Form", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[pmdCRegistrationFormColumnIndex.Value].ColumnName);
				}
				if (groupBySAT2Score)
				{
					sat2ScoreColumnIndex = table.Columns.Add("SAT-II Score", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[sat2ScoreColumnIndex.Value].ColumnName);
				}
				if (groupBySSCCertificate)
				{
					sscCertificateColumnIndex = table.Columns.Add("SSC Certificate", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[sscCertificateColumnIndex.Value].ColumnName);
				}
				if (groupBySSCMarksSheet)
				{
					sscMarksSheetColumnIndex = table.Columns.Add("SSC MarksSheet", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[sscMarksSheetColumnIndex.Value].ColumnName);
				}
				if (groupByUndertaking)
				{
					undertakingColumnIndex = table.Columns.Add("Undertaking", typeof(string)).Ordinal;
					sortExpressions.Add(table.Columns[undertakingColumnIndex.Value].ColumnName);
				}

				table.DefaultView.Sort = string.Join(",", sortExpressions.Select(e => $"[{e}] ASC"));
				#endregion

				#region AssignDataToColumns
				foreach (var record in uploadedData)
				{
					var recordSemesterID = record.Key.SemesterID;
					var row = table.Rows.Add(record.Key.Enrollment);
					row[0] = record.Key.Enrollment;
					row[1] = record.Key.Name;
					row[2] = record.Key.ProgramAlias;
					row[3] = record.Key.Nationality;
					row[4] = recordSemesterID.ToSemesterString();
					row[5] = record.Key.StudentStatus;
					row[6] = record.Key.StudentDocumentsStatus.Value.GetEnum<Model.Entities.Student.StudentDocumentsStatuses>().GetFullName();

					if (groupByAcceptanceForm && record.acceptanceFormStatus != null)
						row[acceptanceFormColumnIndex.Value] = record.acceptanceFormStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByALevelEquivalenceCertificateFromIBCC && record.aLevelEquivalenceCertificateFromIBCCStatus != null)
						row[aLevelEquivalenceCertificateFromIBCCColumnIndex.Value] = record.aLevelEquivalenceCertificateFromIBCCStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByALevelGeneralCertificateFromCambridge && record.aLevelGeneralCertificateFromCambridgeStatus != null)
						row[aLevelGeneralCertificateFromCambridgeColumnIndex.Value] = record.aLevelGeneralCertificateFromCambridgeStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByBachelors2YearsDegree && record.bachelors2YearsDegreeStatus != null)
						row[bachelors2YearsDegreeColumnIndex.Value] = record.bachelors2YearsDegreeStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByBachelors2YearsMarksSheet && record.bachelors2YearsMarksSheetStatus != null)
						row[bachelors2YearsMarksSheetColumnIndex.Value] = record.bachelors2YearsMarksSheetStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByBachelors4YearsDegree && record.bachelors4YearsDegreeStatus != null)
						row[bachelors4YearsDegreeColumnIndex.Value] = record.bachelors4YearsDegreeStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByBachelors4YearsMarksSheet && record.bachelors4YearsMarksSheetStatus != null)
						row[bachelors4YearsMarksSheetColumnIndex.Value] = record.bachelors4YearsMarksSheetStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByBachelorsEquivalenceCertificateFromHEC && record.bachelorsEquivalenceCertificateFromHECStatus != null)
						row[bachelorsEquivalenceCertificateFromHECColumnIndex.Value] = record.bachelorsEquivalenceCertificateFromHECStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByBDSDegree && record.bdsDegreeStatus != null)
						row[bdsDegreeColumnIndex.Value] = record.bdsDegreeStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByCNIC && record.cnicStatus != null)
						row[cnicColumnIndex.Value] = record.cnicStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByConfidentialForm && record.confidentialFormStatus != null)
						row[confidentialFormColumnIndex.Value] = record.confidentialFormStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByConsolidatedTranscript && record.consolidatedTranscriptStatus != null)
						row[consolidatedTranscriptColumnIndex.Value] = record.consolidatedTranscriptStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByDomicile && record.domicileStatus != null)
						row[domicileColumnIndex.Value] = record.domicileStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByFormB && record.formBStatus != null)
						row[formBColumnIndex.Value] = record.formBStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByGATScore && record.gatScoreStatus != null)
						row[gatScoreColumnIndex.Value] = record.gatScoreStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByHouseJobCertificate && record.houseJobCertificateStatus != null)
						row[houseJobCertificateColumnIndex.Value] = record.houseJobCertificateStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByHSSCCertificate && record.hsscCertificateStatus != null)
						row[hsscCertificateColumnIndex.Value] = record.hsscCertificateStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByHSSCMarksSheet && record.hsscMarksSheetStatus != null)
						row[hsscMarksSheetColumnIndex.Value] = record.hsscMarksSheetStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByInterimTranscript && record.interimTranscriptStatus != null)
						row[interimTranscriptColumnIndex.Value] = record.interimTranscriptStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByMastersDegree && record.mastersDegreeStatus != null)
						row[mastersDegreeColumnIndex.Value] = record.mastersDegreeStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByMastersEquivalenceCertificateFromHEC && record.mastersEquivalenceCertificateFromHECStatus != null) row[mastersEquivalenceCertificateFromHECColumnIndex.Value] = record.mastersEquivalenceCertificateFromHECStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByMastersMarksSheet && record.mastersMarksSheetStatus != null)
						row[mastersMarksSheetColumnIndex.Value] = record.mastersMarksSheetStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByMBBSDegree && record.mbbsDegreeStatus != null)
						row[mbbsDegreeColumnIndex.Value] = record.mbbsDegreeStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByMedicalReport && record.medicalReportStatus != null)
						row[medicalReportColumnIndex.Value] = record.medicalReportStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByMSMPhilDegree && record.msMPhilDegreeStatus != null)
						row[msMPhilDegreeColumnIndex.Value] = record.msMPhilDegreeStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByMSMPhilEquivalenceCertificateFromHEC && record.msMPhilEquivalenceCertificateFromHECStatus != null)
						row[msMPhilEquivalenceCertificateFromHECColumnIndex.Value] = record.msMPhilEquivalenceCertificateFromHECStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByMSMPhilMarksSheet && record.msMPhilMarksSheetStatus != null)
						row[msMPhilMarksSheetColumnIndex.Value] = record.msMPhilMarksSheetStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByOLevelEquivalenceCertificateFromIBCC && record.oLevelEquivalenceCertificateFromIBCCStatus != null)
						row[oLevelEquivalenceCertificateFromIBCCColumnIndex.Value] = record.oLevelEquivalenceCertificateFromIBCCStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByOLevelGeneralCertificateFromCambridge && record.oLevelGeneralCertificateFromCambridgeStatus != null)
						row[oLevelGeneralCertificateFromCambridgeColumnIndex.Value] = record.oLevelGeneralCertificateFromCambridgeStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByPassport && record.passportStatus != null)
						row[passportColumnIndex.Value] = record.passportStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByPMDCRegistrationForm && record.pmdCRegistrationFormStatus != null)
						row[pmdCRegistrationFormColumnIndex.Value] = record.pmdCRegistrationFormStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupBySAT2Score && record.sat2ScoreStatus != null)
						row[sat2ScoreColumnIndex.Value] = record.sat2ScoreStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupBySSCCertificate && record.sscCertificateStatus != null)
						row[sscCertificateColumnIndex.Value] = record.sscCertificateStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupBySSCMarksSheet && record.sscMarksSheetStatus != null)
						row[sscMarksSheetColumnIndex.Value] = record.sscMarksSheetStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					if (groupByUndertaking && record.undertakingStatus != null)
						row[undertakingColumnIndex.Value] = record.undertakingStatus.Value.GetEnum<StudentDocument.Statuses>().GetFullName();
					#endregion
				}

				return table;
			}
		}
	}
}
