﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using Aspire.BL.Entities.Common;

namespace Aspire.BL.Core.StudentInformation.Staff
{

	public static class DisciplineCase
	{
		public class DisciplineCasesStudent
		{
			public Aspire.Model.Entities.DisciplineCas DisciplineCase { get; internal set; }
			public int DisciplineCasesStudentID { get; internal set; }
			public int DisciplineCaseID { get; internal set; }
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public List<string> DisciplineCaseStudentActivities { get; internal set; }
			public byte? RemarksType { get; internal set; }
			public string Mobile { get; internal set; }
			public string StudentStatement { get; internal set; }
			public string Remarks { get; internal set; }

			public string RemarksTypeEnumFullName => ((Model.Entities.DisciplineCasesStudent.RemarksTypes?)this.RemarksType).ToString().SplitCamelCasing();
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandDisciplineCasePermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.ManageDisciplineCase, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.ManageStudentDisciplineCase, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandRemarksPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.ManageDisciplineCasesStudentRemarks, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public static Model.Entities.Student GetStudent(string enrollment, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var studentID = aspireContext.GetStudentIDFromEnrollment(loginHistory.InstituteID, enrollment);
				return aspireContext.Students
					.Include(s => s.AdmissionOpenProgram.Program)
					.SingleOrDefault(s => s.StudentID == studentID && s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
			}
		}

		public enum AddDisciplineCaseStatuses
		{
			AlreadyExists,
			Success,
		}

		public static AddDisciplineCaseStatuses AddDisciplineCase(DateTime caseDate, string location, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var activeUser = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var loginHistory = aspireContext.DemandDisciplineCasePermissions(loginSessionGuid, activeUser.InstituteID, null, null, null);
				var now = DateTime.Now;
				var caseFileNo = aspireContext.GetCaseFileNo(caseDate, loginHistory.InstituteID);
				if (aspireContext.DisciplineCases.Any(dc => dc.CaseFileNo == caseFileNo))
					return AddDisciplineCaseStatuses.AlreadyExists;
				var disciplineCase = new DisciplineCas
				{
					InstituteID = loginHistory.InstituteID,
					CaseFileNo = caseFileNo,
					CaseDate = caseDate,
					Location = location,
					EntryDate = now,
				}.Validate();
				aspireContext.DisciplineCases.Add(disciplineCase);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddDisciplineCaseStatuses.Success;
			}
		}

		public enum DeleteDisciplineCasesStatues
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteDisciplineCasesStatues DeleteDisciplineCase(int disciplineCaseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var activeUser = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var loginHistory = aspireContext.DemandDisciplineCasePermissions(loginSessionGuid, activeUser.InstituteID, null, null, null);
				var disciplineCase = aspireContext.DisciplineCases.SingleOrDefault(dc => dc.DisciplineCaseID == disciplineCaseID);
				if (disciplineCase == null)
					return DeleteDisciplineCasesStatues.NoRecordFound;
				if (disciplineCase.DisciplineCasesStudents.Any())
					return DeleteDisciplineCasesStatues.ChildRecordExists;
				aspireContext.DisciplineCases.Remove(disciplineCase);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteDisciplineCasesStatues.Success;
			}
		}

		public static List<DisciplineCas> GetDisciplineCases(int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var disciplineCases = aspireContext.DisciplineCases.Include(dc => dc.DisciplineCasesStudents).Where(dc => dc.InstituteID == loginHistory.InstituteID);
				virtualCount = disciplineCases.Count();
				switch (sortExpression)
				{
					case nameof(DisciplineCas.CaseFileNo):
						disciplineCases = disciplineCases.OrderBy(sortDirection, q => q.CaseFileNo);
						break;
					case nameof(DisciplineCas.CaseDate):
						disciplineCases = disciplineCases.OrderBy(sortDirection, q => q.CaseDate);
						break;
					case nameof(DisciplineCas.Location):
						disciplineCases = disciplineCases.OrderBy(sortDirection, q => q.Location);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}

				disciplineCases = disciplineCases.Skip(pageIndex * pageSize).Take(pageSize);
				return disciplineCases.ToList();
			}
		}

		public static DisciplineCas GetDisciplineCase(int disciplineCaseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.DisciplineCases.Single(dc => dc.InstituteID == loginHistory.InstituteID && dc.DisciplineCaseID == disciplineCaseID);
			}
		}

		public enum AddDisciplineCasesStudentStatues
		{
			NoRecordFound,
			Success,
			AlreadyExists,
		}

		public static AddDisciplineCasesStudentStatues AddDisciplineCasesStudent(int disciplineCaseID, string enrollment, string mobile, List<int> offenceActivities, string studentStatement, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var activeUser = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var studentID = aspireContext.Students.SingleOrDefault(s => s.Enrollment == enrollment && s.AdmissionOpenProgram.Program.InstituteID == activeUser.InstituteID)?.StudentID;
				if (studentID == null)
					return AddDisciplineCasesStudentStatues.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, activeUser.InstituteID, null, null, null);
				if (aspireContext.DisciplineCasesStudents.Any(dcs => dcs.StudentID == studentID && dcs.DisciplineCaseID == disciplineCaseID))
					return AddDisciplineCasesStudentStatues.AlreadyExists;
				var disciplineCaseStudent = new Model.Entities.DisciplineCasesStudent
				{
					DisciplineCaseID = disciplineCaseID,
					StudentID = studentID.Value,
					Mobile = mobile,
					StudentsStatement = studentStatement.TrimAndMakeItNullIfEmpty(),
				};
				aspireContext.DisciplineCasesStudents.Add(disciplineCaseStudent);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.AddOffenceActivitiesForDisciplinceCasesStudents(disciplineCaseStudent.DisciplineCasesStudentID, offenceActivities, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return AddDisciplineCasesStudentStatues.Success;
			}
		}


		public static List<CustomListItem> GetDisciplineCaseActivities(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.DisciplineCaseActivities.Where(dca => dca.InstituteID == loginHistory.InstituteID).Select(dca => new CustomListItem
				{
					ID = dca.DisciplineCaseActivityID,
					Text = dca.ActivityName,
				}).OrderBy(dca => dca.Text).ToList();
			}
		}

		public enum UpdateDisciplineCasesStudentStatues
		{
			NoRecordFound,
			Success,
		}
		public static UpdateDisciplineCasesStudentStatues UpdateDisciplineCasesStudent(int disciplineCasesStudentID, string mobile, string studentStatement, List<int> offenceActivities, Model.Entities.DisciplineCasesStudent.RemarksTypes? remarksTypesEnum, string remarks, bool sameforOthers, bool forRemarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var activeUser = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				Core.Common.Permissions.Staff.StaffGroupPermission loginHistory = null;
				if (!forRemarks)
					loginHistory = aspireContext.DemandPermissions(loginSessionGuid, activeUser.InstituteID, null, null, null);
				if (forRemarks)
					loginHistory = aspireContext.DemandRemarksPermissions(loginSessionGuid, activeUser.InstituteID, null, null, null);
				if (sameforOthers && !forRemarks)
					throw new InvalidOperationException();
				var disciplineCasesStudentsAll = aspireContext.DisciplineCasesStudents.AsQueryable();
				var disciplineCasesStudent = disciplineCasesStudentsAll.SingleOrDefault(dcs => dcs.DisciplineCasesStudentID == disciplineCasesStudentID);
				if (disciplineCasesStudent == null)
					return UpdateDisciplineCasesStudentStatues.NoRecordFound;
				if (forRemarks && sameforOthers)
				{
					if (remarksTypesEnum == null || string.IsNullOrEmpty(remarks))
						throw new InvalidOperationException();
					var disciplineCasesStudents = disciplineCasesStudentsAll.Where(dcs => dcs.DisciplineCaseID == disciplineCasesStudent.DisciplineCaseID && dcs.DisciplineCasesStudentID != disciplineCasesStudentID);
					foreach (var dcs in disciplineCasesStudents)
					{
						dcs.RemarksTypeEnum = remarksTypesEnum;
						dcs.Remarks = remarks.Trim();
					}
				}
				if (!disciplineCasesStudent.DisciplineCaseStudentActivities.Any())
					throw new InvalidDataException("Discipline Case Student Activities missing.");
				disciplineCasesStudent.Mobile = mobile;
				disciplineCasesStudent.StudentsStatement = studentStatement.TrimAndMakeItNullIfEmpty();
				disciplineCasesStudent.RemarksTypeEnum = remarksTypesEnum;
				disciplineCasesStudent.Remarks = remarks.Trim();
				aspireContext.DisciplineCaseStudentActivities.RemoveRange(disciplineCasesStudent.DisciplineCaseStudentActivities);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.AddOffenceActivitiesForDisciplinceCasesStudents(disciplineCasesStudent.DisciplineCasesStudentID, offenceActivities, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return UpdateDisciplineCasesStudentStatues.Success;
			}
		}

		public enum DeleteDisciplineCasesStudentStatues
		{
			NoRecordFound,
			RemarksIsExists,
			Success,
		}

		public static DeleteDisciplineCasesStudentStatues DeleteDisciplineCasesStudent(int disciplineCasesStudentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var activeUser = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, activeUser.InstituteID, null, null, null);
				var disciplineCasesStudent = aspireContext.DisciplineCasesStudents.SingleOrDefault(dcs => dcs.DisciplineCasesStudentID == disciplineCasesStudentID);
				if (disciplineCasesStudent == null)
					return DeleteDisciplineCasesStudentStatues.NoRecordFound;
				if (disciplineCasesStudent.RemarksType != null || !string.IsNullOrEmpty(disciplineCasesStudent.Remarks))
					return DeleteDisciplineCasesStudentStatues.RemarksIsExists;
				if (!disciplineCasesStudent.DisciplineCaseStudentActivities.Any())
					throw new InvalidDataException("Discipline Case Student Activities missing.");
				aspireContext.DisciplineCaseStudentActivities.RemoveRange(disciplineCasesStudent.DisciplineCaseStudentActivities);
				aspireContext.DisciplineCasesStudents.Remove(disciplineCasesStudent);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteDisciplineCasesStudentStatues.Success;
			}
		}

		public static List<DisciplineCasesStudent> GetDisciplineCasesStudents(int disciplineCaseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var disciplineCaseStudents = aspireContext.DisciplineCasesStudents.Where(dcs => dcs.DisciplineCaseID == disciplineCaseID).Select(dcs =>
					new DisciplineCasesStudent
					{
						DisciplineCase = dcs.DisciplineCas,
						DisciplineCasesStudentID = dcs.DisciplineCasesStudentID,
						DisciplineCaseID = dcs.DisciplineCaseID,
						StudentID = dcs.Student.StudentID,
						Enrollment = dcs.Student.Enrollment,
						Name = dcs.Student.Name,
						ProgramAlias = dcs.Student.AdmissionOpenProgram.Program.ProgramAlias,
						DisciplineCaseStudentActivities = dcs.DisciplineCaseStudentActivities.Select(dcsa => dcsa.DisciplineCaseActivity.ActivityName).ToList(),
						Mobile = dcs.Mobile,
						StudentStatement = dcs.StudentsStatement,
						RemarksType = dcs.RemarksType,
						Remarks = dcs.Remarks,
					});
				return disciplineCaseStudents.ToList();
			}
		}

		public static Model.Entities.DisciplineCasesStudent GetDisciplineCasesStudent(int disciplineCasesStudentID, bool forRemarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				if (forRemarks)
					aspireContext.DemandRemarksPermissions(loginSessionGuid, loginHistory.InstituteID, null, null, null);
				var disciplineCasesStudent = aspireContext.DisciplineCasesStudents.Include(dcs => dcs.Student).Include(dcs => dcs.Student.AdmissionOpenProgram.Program).Include(dcs => dcs.DisciplineCaseStudentActivities).SingleOrDefault(dcs => dcs.DisciplineCasesStudentID == disciplineCasesStudentID);
				return disciplineCasesStudent;
			}
		}

		private static string GetCaseFileNo(this AspireContext aspireContext, DateTime caseDate, int instituteID)
		{
			var caseDateDate = caseDate.Date;
			var nextCaseDateDate = caseDate.Date.AddDays(1);
			var count = aspireContext.DisciplineCases
				.Where(dc => dc.InstituteID == instituteID)
				.Count(dc => caseDateDate <= dc.CaseDate && dc.CaseDate < nextCaseDateDate);
			return $"SEC-{caseDateDate:yyyy}-{caseDateDate:MM}{caseDateDate:dd}-{(++count):000}";
		}

		private static void AddOffenceActivitiesForDisciplinceCasesStudents(this AspireContext aspireContext, int disciplineCasesStudentID, List<int> offenceActivities, int userLoginHistoryID)
		{
			foreach (var offenceActivityID in offenceActivities)
			{
				var disciplineCaseStudentActivity = new Model.Entities.DisciplineCaseStudentActivity
				{
					DisciplineCasesStudentID = disciplineCasesStudentID,
					DisciplineCaseActivityID = offenceActivityID,
				};
				aspireContext.DisciplineCaseStudentActivities.Add(disciplineCaseStudentActivity);
			}
			aspireContext.SaveChanges(userLoginHistoryID);
		}
	}
}

