﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.StudentInformation.Staff
{
	public static class StudentDocuments
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.ViewStudentDocuments, UserGroupPermission.PermissionValues.Allowed);
		}

		public enum UpdateStudentDocumentStatuses
		{
			NoRecordFound,
			LowResolutionIsNotAllowed,
			Success
		}

		public static UpdateStudentDocumentStatuses UpdateStudentDocument(Guid studentDocumentID, Model.Entities.StudentDocument.Statuses statusEnum, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.StudentDocuments.Where(sd => sd.StudentDocumentID == studentDocumentID)
					.Select(sd => new
					{
						StudentDocument = sd,
						sd.Student,
						sd.Student.AdmissionOpenProgram.Program.InstituteID,
						sd.Student.AdmissionOpenProgram.Program.DepartmentID,
						sd.Student.AdmissionOpenProgram.Program.ProgramID,
						sd.Student.AdmissionOpenProgramID,
					})
					.SingleOrDefault();
				if (record == null)
					return UpdateStudentDocumentStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.VerifyStudentDocuments, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				switch (statusEnum)
				{
					case Model.Entities.StudentDocument.Statuses.VerificationPending:
						record.StudentDocument.StatusEnum = statusEnum;
						record.StudentDocument.Remarks = remarks.MustBeNullOrEmpty();
						break;
					case Model.Entities.StudentDocument.Statuses.Rejected:
						record.StudentDocument.StatusEnum = statusEnum;
						record.StudentDocument.Remarks = remarks.CannotBeEmptyOrWhitespace();
						break;
					case Model.Entities.StudentDocument.Statuses.Verified:
						if (record.StudentDocument.ImageDpiX < 150 || record.StudentDocument.ImageDpiY < 150)
							return UpdateStudentDocumentStatuses.LowResolutionIsNotAllowed;
						record.StudentDocument.StatusEnum = statusEnum;
						record.StudentDocument.Remarks = remarks.MustBeNullOrEmpty();
						break;
					default:
						throw new NotImplementedEnumException(statusEnum);
				}

				var changesDone = aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				if (changesDone > 0)
				{
					record.Student.StudentDocumentsStatusEnum = Model.Entities.Student.StudentDocumentsStatuses.VerificationPending;
					record.Student.StudentDocumentsRemarks = null;
					record.Student.StudentDocumentsLastUpdatedByUserLoginHistoryID = loginHistory.UserLoginHistoryID;
					record.Student.StudentDocumentsStatusDate = DateTime.Now;
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateStudentDocumentStatuses.Success;
			}
		}

		public enum VerifyStudentDocumentsStatuses
		{
			NoRecordFound,
			AlreadyVerified,
			AllDocumentsAreNotVerified,
			NoDocumentsFound,
			Success
		}

		public static VerifyStudentDocumentsStatuses VerifyStudentDocuments(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						Student = s,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.Program.ProgramID,
						s.AdmissionOpenProgramID,
						StudentDocuments = s.StudentDocuments.Select(sd => new
						{
							sd.Status
						}).ToList()
					})
					.SingleOrDefault();
				if (record == null)
					return VerifyStudentDocumentsStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.VerifyStudentDocuments, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				if (!record.StudentDocuments.Any())
					return VerifyStudentDocumentsStatuses.NoDocumentsFound;
				if (record.StudentDocuments.Any(sd => sd.Status.GetEnum<Model.Entities.StudentDocument.Statuses>() != Model.Entities.StudentDocument.Statuses.Verified))
					return VerifyStudentDocumentsStatuses.AllDocumentsAreNotVerified;

				switch (record.Student.StudentDocumentsStatusEnum)
				{
					case Model.Entities.Student.StudentDocumentsStatuses.Verified:
						return VerifyStudentDocumentsStatuses.AlreadyVerified;
					case null:
					case Model.Entities.Student.StudentDocumentsStatuses.VerificationPending:
					case Model.Entities.Student.StudentDocumentsStatuses.Rejected:
						record.Student.StudentDocumentsStatusEnum = Model.Entities.Student.StudentDocumentsStatuses.Verified;
						record.Student.StudentDocumentsRemarks = null;
						record.Student.StudentDocumentsStatusDate = DateTime.Now;
						record.Student.StudentDocumentsLastUpdatedByUserLoginHistoryID = loginHistory.UserLoginHistoryID;
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return VerifyStudentDocumentsStatuses.Success;
					default:
						throw new NotImplementedEnumException(record.Student.StudentDocumentsStatusEnum);
				}
			}
		}

		public enum RejectStudentDocumentsStatuses
		{
			NoRecordFound,
			Success
		}

		public static RejectStudentDocumentsStatuses RejectStudentDocuments(int studentID, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						Student = s,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.Program.ProgramID,
						s.AdmissionOpenProgramID,
						StudentDocuments = s.StudentDocuments.Select(sd => new
						{
							sd.Status
						}).ToList()
					})
					.SingleOrDefault();
				if (record == null)
					return RejectStudentDocumentsStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.VerifyStudentDocuments, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				record.Student.StudentDocumentsStatusEnum = Model.Entities.Student.StudentDocumentsStatuses.Rejected;
				record.Student.StudentDocumentsRemarks = remarks.TrimAndCannotBeEmpty();
				record.Student.StudentDocumentsStatusDate = DateTime.Now;
				record.Student.StudentDocumentsLastUpdatedByUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return RejectStudentDocumentsStatuses.Success;
			}
		}

		public sealed class Student
		{
			internal Student() { }
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public Guid? StudentDocumentsStatus { get; internal set; }
			public DateTime? StudentDocumentsStatusDate { get; internal set; }
			public string StudentDocumentsLastUpdatedBy { get; internal set; }
			public List<StudentDocument> StudentDocuments { get; internal set; }
			public string DocumentsHtml => this.StudentDocuments?.OrderBy(sd => (int)sd.DocumentTypeEnum).ThenBy(sd => (int)sd.PageEnum).Select(sd =>
			{
				var statusHtml = $"{sd.DocumentType.GetFullName()} - {sd.Page.GetFullName()}".HtmlEncode();
				if (sd.Status.GetEnum<Model.Entities.StudentDocument.Statuses>() == Model.Entities.StudentDocument.Statuses.Verified)
					return $"<span class=\"text-success\">{statusHtml}</span>";
				return statusHtml;
			}).Join("<br />").TrimAndMakeItNullIfEmpty();

			public sealed class StudentDocument
			{
				internal StudentDocument() { }
				public Guid DocumentType { get; internal set; }
				public Model.Entities.StudentDocument.DocumentTypes DocumentTypeEnum => this.DocumentType.GetEnum<Model.Entities.StudentDocument.DocumentTypes>();
				public Guid Page { get; internal set; }
				public Model.Entities.StudentDocument.Pages PageEnum => this.DocumentType.GetEnum<Model.Entities.StudentDocument.Pages>();
				public Guid Status { get; internal set; }
			}
		}

		public static (List<Student> studentDocuments, int virtualItemCount) GetStudentDocuments(short? intakeSemesterID, int? departmentID, int? programID, Model.Entities.StudentDocument.DocumentTypes? missingDocumentTypeEnum, Model.Entities.StudentDocument.Pages? missingPageEnum, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var missingDocumentType = missingDocumentTypeEnum?.GetGuid();
				var missingPage = missingPageEnum?.GetGuid();
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var minIntakeSemesterID = (short)20191;
				var studentsQuery = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID)
					.SelectMany(i => i.Departments).Where(d => departmentID == null || d.DepartmentID == departmentID.Value)
					.SelectMany(d => d.Programs).Where(p => programID == null || p.ProgramID == programID.Value)
					.SelectMany(p => p.AdmissionOpenPrograms).Where(aop => aop.SemesterID >= minIntakeSemesterID && (intakeSemesterID == null || aop.SemesterID == intakeSemesterID.Value))
					.SelectMany(aop => aop.Students);
				if (missingDocumentType == null)
				{
					if (missingPage != null)
						throw new ArgumentException(nameof(missingPage));
				}
				else
				{
					if (missingPage == null)
						studentsQuery = studentsQuery.Where(s => !s.StudentDocuments.Any(sd => sd.DocumentType == missingDocumentType.Value));
					else
						studentsQuery = studentsQuery.Where(s => !s.StudentDocuments.Any(sd => sd.DocumentType == missingDocumentType.Value && sd.Page == missingPage.Value));
				}

				var students = studentsQuery.Select(s => new Student
				{
					StudentID = s.StudentID,
					Enrollment = s.Enrollment,
					Name = s.Name,
					ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
					IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
					StudentDocumentsStatus = s.StudentDocumentsStatus,
					StudentDocumentsStatusDate = s.StudentDocumentsStatusDate,
					StudentDocumentsLastUpdatedBy = s.UserLoginHistory.User.Name,
					StudentDocuments = s.StudentDocuments.Select(sd => new Student.StudentDocument
					{
						DocumentType = sd.DocumentType,
						Page = sd.Page,
						Status = sd.Status
					}).ToList()
				});
				var virtualItemCount = students.Count();
				switch (sortExpression)
				{
					case nameof(Student.Enrollment):
						students = students.OrderBy(sortDirection, s => s.Enrollment);
						break;
					case nameof(Student.Name):
						students = students.OrderBy(sortDirection, s => s.Name);
						break;
					case nameof(Student.ProgramAlias):
						students = students.OrderBy(sortDirection, s => s.ProgramAlias);
						break;
					case nameof(Student.IntakeSemesterID):
						students = students.OrderBy(sortDirection, s => s.IntakeSemesterID);
						break;
					case nameof(Student.StudentDocumentsStatus):
						students = students.OrderBy(sortDirection, s => s.StudentDocumentsStatus).ThenBy(sortDirection, s => s.StudentDocumentsStatusDate).ThenBy(sortDirection, s => s.StudentDocumentsLastUpdatedBy);
						break;
					case nameof(Student.StudentDocumentsStatusDate):
						students = students.OrderBy(sortDirection, s => s.StudentDocumentsStatusDate);
						break;
					case nameof(Student.StudentDocumentsLastUpdatedBy):
						students = students.OrderBy(sortDirection, s => s.StudentDocumentsLastUpdatedBy);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return (students.Skip(pageIndex * pageSize).Take(pageSize).ToList(), virtualItemCount);
			}
		}
	}
}