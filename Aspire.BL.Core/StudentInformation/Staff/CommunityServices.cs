﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.StudentInformation.Staff
{
	public static class CommunityServices
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.ManageCommunityServices, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		#region Community Services


		//public sealed class CommunityService
		//{
		//	internal CommunityService() { }
		//	public TYPE Type { get; set; }
		//}

		public enum AddCommunityServiceStatuses
		{
			NoRecordFound,
			Success,
		}

		public static AddCommunityServiceStatuses AddCommunityService(int studentID, short semesterID, string organization, string jobTitle, DateTime completedDate, byte creditHours, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => new
				{
					s.AdmissionOpenProgram.SemesterID,
					s.AdmissionOpenProgramID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.Program.InstituteID
				}).SingleOrDefault();
				if (student == null)
					return AddCommunityServiceStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				var studentCommunityService = new Aspire.Model.Entities.StudentCommunityService
				{
					StudentID = studentID,
					SemesterID = semesterID,
					Organization = organization,
					JobTitle = jobTitle,
					CompletedDate = completedDate,
					Hours = creditHours,
				};
				aspireContext.StudentCommunityServices.Add(studentCommunityService);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddCommunityServiceStatuses.Success;
			}
		}

		public enum UpdateCommunityServiceStatuses
		{
			NoRecordFound,
			Success,
		}

		public static UpdateCommunityServiceStatuses UpdateCommunityService(int? studentCommunityServiceID, short semesterID, string organization, string jobTitle, DateTime completedDate, byte creditHours, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var studentCommunityService = aspireContext.StudentCommunityServices.SingleOrDefault(scs => scs.StudentCommunityServiceID == studentCommunityServiceID);
				if (studentCommunityService == null)
					return UpdateCommunityServiceStatuses.NoRecordFound;

				var student = aspireContext.Students.Where(s => s.StudentID == studentCommunityService.StudentID).Select(s => new
				{
					s.AdmissionOpenProgram.SemesterID,
					s.AdmissionOpenProgramID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.Program.InstituteID
				}).SingleOrDefault();
				if (student == null)
					return UpdateCommunityServiceStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				studentCommunityService.SemesterID = semesterID;
				studentCommunityService.Organization = organization;
				studentCommunityService.JobTitle = jobTitle;
				studentCommunityService.CompletedDate = completedDate;
				studentCommunityService.Hours = creditHours;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateCommunityServiceStatuses.Success;
			}
		}

		public enum DeleteCommunityServiceStatuses
		{
			NoRecordFound,
			Success,
		}


		public static DeleteCommunityServiceStatuses DeleteStudentCommunityService(int studentCommunityServiceID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var studentCommunityService = aspireContext.StudentCommunityServices.SingleOrDefault(scs => scs.StudentCommunityServiceID == studentCommunityServiceID);
				if (studentCommunityService == null)
					return DeleteCommunityServiceStatuses.NoRecordFound;

				var student = aspireContext.Students.Where(s => s.StudentID == studentCommunityService.StudentID).Select(s => new
				{
					s.AdmissionOpenProgramID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.Program.InstituteID
				}).Single();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);

				aspireContext.StudentCommunityServices.Remove(studentCommunityService);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteCommunityServiceStatuses.Success;
			}
		}

		public static List<Model.Entities.StudentCommunityService> GetStudentCommunityServices(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.StudentCommunityServices.Where(scs => scs.StudentID == studentID).ToList();
			}
		}

		public static Model.Entities.StudentCommunityService GetCommunityService(int studentCommunityServiceID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.StudentCommunityServices.SingleOrDefault(scs => scs.StudentCommunityServiceID == studentCommunityServiceID);
			}
		}

		#endregion
	}
}
