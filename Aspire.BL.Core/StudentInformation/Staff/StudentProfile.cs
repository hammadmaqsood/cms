﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FileServer;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.StudentInformation.Staff
{
	public static class StudentProfile
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandReadPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.Module, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Readonly);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.ManageStudentProfile, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public static Model.Entities.Student GetStudent(string enrollment, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandReadPermissions(loginSessionGuid);
				return aspireContext.Students
					.Include(s => s.AdmissionOpenProgram.Program)
					.Include(s => s.CandidateAppliedProgram.Candidate)
					.Include(s => s.AdmissionOpenProgram1)
					.Include(s => s.StudentAcademicRecords)
					.Include(s => s.StudentCustomInfos.Select(sci => sci.UserLoginHistory.User))
					.SingleOrDefault(s => s.Enrollment == enrollment && s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
			}
		}

		public sealed class DeferredFromStudent
		{
			internal DeferredFromStudent()
			{
			}
			public int StudentID { get; internal set; }
			public int DeferredFromStudentID { get; internal set; }
			public string DeferredFromProgramAlias { get; internal set; }
			public short DeferredFromIntakeSemesterID { get; internal set; }
			public string DeferredFromEnrollment { get; internal set; }
		}

		public static DeferredFromStudent GetStudentDeferredFrom(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandReadPermissions(loginSessionGuid);
				var student = aspireContext.Students
					.Where(s => s.StudentID == studentID && s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID && s.DeferredFromStudentID != null)
					.Select(s => new
					{
						s.StudentID,
						s.DeferredFromStudentID,
					})
					.SingleOrDefault();
				if (student == null)
					return null;

				return aspireContext.Students.Where(s => s.StudentID == student.DeferredFromStudentID.Value)
					.Select(s => new DeferredFromStudent
					{
						StudentID = student.StudentID,
						DeferredFromStudentID = s.StudentID,
						DeferredFromEnrollment = s.Enrollment,
						DeferredFromProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
						DeferredFromIntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
					}).SingleOrDefault();
			}
		}

		public sealed class DeferredToStudent
		{
			internal DeferredToStudent()
			{
			}
			public int StudentID { get; internal set; }
			public int? DeferredToStudentID { get; internal set; }
			public string DeferredToProgramAlias { get; internal set; }
			public Shifts DeferredToShiftEnum { get; internal set; }
			public short DeferredToIntakeSemesterID { get; internal set; }
			public string DeferredToEnrollment { get; internal set; }
			public int DeferredToAdmissionOpenProgramID { get; set; }
		}

		public static DeferredToStudent GetStudentDeferredTo(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandReadPermissions(loginSessionGuid);
				var student = aspireContext.Students
					.Where(s => s.StudentID == studentID && s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(s => s.DeferredToStudentID != null || s.DeferredToAdmissionOpenProgramID != null)
					.Select(s => new
					{
						s.StudentID,
						s.DeferredToAdmissionOpenProgramID,
						DeferredToShiftEnum = (Shifts)s.DeferredToShift.Value,
						s.DeferredToStudentID
					})
					.SingleOrDefault();
				if (student == null)
					return null;

				if (student.DeferredToStudentID != null)
					return aspireContext.Students.Where(s => s.StudentID == student.DeferredToStudentID.Value)
						.Select(s => new DeferredToStudent
						{
							StudentID = student.StudentID,
							DeferredToShiftEnum = student.DeferredToShiftEnum,
							DeferredToStudentID = s.StudentID,
							DeferredToEnrollment = s.Enrollment,
							DeferredToProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
							DeferredToIntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
							DeferredToAdmissionOpenProgramID = s.AdmissionOpenProgramID,
						}).Single();
				return aspireContext.AdmissionOpenPrograms.Where(aop => aop.AdmissionOpenProgramID == student.DeferredToAdmissionOpenProgramID.Value)
					.Select(aop => new DeferredToStudent
					{
						StudentID = student.StudentID,
						DeferredToStudentID = null,
						DeferredToEnrollment = null,
						DeferredToProgramAlias = aop.Program.ProgramAlias,
						DeferredToIntakeSemesterID = aop.SemesterID,
						DeferredToAdmissionOpenProgramID = aop.AdmissionOpenProgramID,
						DeferredToShiftEnum = student.DeferredToShiftEnum
					}).Single();
			}
		}

		public enum UpdateStudentStatuses
		{
			StatusCannotBeChangedAfterDeferment,
			NoRecordFound,
			Success,
		}

		public static UpdateStudentStatuses UpdateStudent(Model.Entities.Student student, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == student.StudentID).Select(s => new
				{
					s.AdmissionOpenProgram.Program.InstituteID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgram.AdmissionOpenProgramID,
					Student = s,
				}).SingleOrDefault();
				if (record == null)
					return UpdateStudentStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				if (record.Student.StatusEnum != student.StatusEnum)
					if (record.Student.StatusEnum == Model.Entities.Student.Statuses.Deferred) //old status was deferred
						if (record.Student.DeferredToStudentID != null)
							return UpdateStudentStatuses.StatusCannotBeChangedAfterDeferment;

				record.Student.CopyValuesAndValidate(student);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateStudentStatuses.Success;
			}
		}

		public static List<StudentAcademicRecord> GetStudentAcademicRecords(int studentID, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandReadPermissions(loginSessionGuid);

				var studentAcademicRecords = aspireContext.StudentAcademicRecords.Where(sar => sar.StudentID == studentID && sar.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				switch (sortExpression)
				{
					case nameof(StudentAcademicRecord.Institute):
						studentAcademicRecords = studentAcademicRecords.OrderBy(sortDirection, sar => sar.Institute);
						break;
					case nameof(StudentAcademicRecord.BoardUniversity):
						studentAcademicRecords = studentAcademicRecords.OrderBy(sortDirection, sar => sar.BoardUniversity);
						break;
					case nameof(StudentAcademicRecord.DegreeType):
						studentAcademicRecords = studentAcademicRecords.OrderBy(sortDirection, sar => sar.DegreeType);
						break;
					case nameof(StudentAcademicRecord.IsCGPA):
						studentAcademicRecords = studentAcademicRecords.OrderBy(sortDirection, sar => sar.IsCGPA);
						break;
					case nameof(StudentAcademicRecord.ObtainedMarks):
						studentAcademicRecords = studentAcademicRecords.OrderBy(sortDirection, sar => sar.ObtainedMarks);
						break;
					case nameof(StudentAcademicRecord.PassingYear):
						studentAcademicRecords = studentAcademicRecords.OrderBy(sortDirection, sar => sar.PassingYear);
						break;
					case nameof(StudentAcademicRecord.Percentage):
						studentAcademicRecords = studentAcademicRecords.OrderBy(sortDirection, sar => sar.Percentage);
						break;
					case nameof(StudentAcademicRecord.Remarks):
						studentAcademicRecords = studentAcademicRecords.OrderBy(sortDirection, sar => sar.Remarks);
						break;
					case nameof(StudentAcademicRecord.Status):
						studentAcademicRecords = studentAcademicRecords.OrderBy(sortDirection, sar => sar.Status);
						break;
					case nameof(StudentAcademicRecord.Subjects):
						studentAcademicRecords = studentAcademicRecords.OrderBy(sortDirection, sar => sar.Subjects);
						break;
					case nameof(StudentAcademicRecord.TotalMarks):
						studentAcademicRecords = studentAcademicRecords.OrderBy(sortDirection, sar => sar.TotalMarks);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return studentAcademicRecords.ToList();
			}
		}

		public enum AddStudentAcademicRecordStatuses
		{
			NoRecordFound,
			Success,
		}

		public static AddStudentAcademicRecordStatuses AddStudentAcademicRecord(int studentID, DegreeTypes degreeTypeEnum, double totalMarks, double obtainedMarks, bool isCGPA, string subjects, string institute, string boardUniversity, short passingYear, string remarks, StudentAcademicRecord.Statuses? statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => new
				{
					s.AdmissionOpenProgram.Program.InstituteID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgram.AdmissionOpenProgramID,
				}).SingleOrDefault();
				if (record == null)
					return AddStudentAcademicRecordStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
				aspireContext.StudentAcademicRecords.Add(new StudentAcademicRecord
				{
					StudentID = studentID,
					DegreeTypeEnum = degreeTypeEnum,
					TotalMarks = totalMarks,
					ObtainedMarks = obtainedMarks,
					IsCGPA = isCGPA,
					Subjects = subjects,
					Institute = institute,
					BoardUniversity = boardUniversity,
					PassingYear = passingYear,
					Remarks = remarks,
					StatusEnum = statusEnum,
				}.Validate());
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddStudentAcademicRecordStatuses.Success;
			}
		}

		public enum UpdateStudentAcademicRecordStatuses
		{
			NoRecordFound,
			Success,
		}

		public static UpdateStudentAcademicRecordStatuses UpdateStudentAcademicRecord(int studentAcademicRecordID, double totalMarks, double obtainedMarks, bool isCGPA, string subjects, string institute, string boardUniversity, short passingYear, string remarks, StudentAcademicRecord.Statuses? statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.StudentAcademicRecords.Where(sar => sar.StudentAcademicRecordID == studentAcademicRecordID).Select(sar => new
				{
					sar.Student.AdmissionOpenProgram.Program.InstituteID,
					sar.Student.AdmissionOpenProgram.Program.DepartmentID,
					sar.Student.AdmissionOpenProgram.ProgramID,
					sar.Student.AdmissionOpenProgram.AdmissionOpenProgramID,
					StudentAcademicRecord = sar
				}).SingleOrDefault();
				if (record == null)
					return UpdateStudentAcademicRecordStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
				record.StudentAcademicRecord.TotalMarks = totalMarks;
				record.StudentAcademicRecord.ObtainedMarks = obtainedMarks;
				record.StudentAcademicRecord.IsCGPA = isCGPA;
				record.StudentAcademicRecord.Subjects = subjects;
				record.StudentAcademicRecord.Institute = institute;
				record.StudentAcademicRecord.BoardUniversity = boardUniversity;
				record.StudentAcademicRecord.PassingYear = passingYear;
				record.StudentAcademicRecord.StatusEnum = statusEnum;
				record.StudentAcademicRecord.Remarks = remarks;
				record.StudentAcademicRecord.Validate();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateStudentAcademicRecordStatuses.Success;
			}
		}

		public enum DeleteStudentAcademicRecordStatuses
		{
			NoRecordFound,
			Success,
		}

		public static DeleteStudentAcademicRecordStatuses DeleteStudentAcademicRecord(int studentAcademicRecordID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.StudentAcademicRecords.Where(sar => sar.StudentAcademicRecordID == studentAcademicRecordID).Select(sar => new
				{
					sar.Student.AdmissionOpenProgram.Program.InstituteID,
					sar.Student.AdmissionOpenProgram.Program.DepartmentID,
					sar.Student.AdmissionOpenProgram.ProgramID,
					sar.Student.AdmissionOpenProgram.AdmissionOpenProgramID,
					StudentAcademicRecord = sar
				}).SingleOrDefault();
				if (record == null)
					return DeleteStudentAcademicRecordStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
				aspireContext.StudentAcademicRecords.Remove(record.StudentAcademicRecord);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteStudentAcademicRecordStatuses.Success;
			}
		}

		public static StudentAcademicRecord GetStudentAcademicRecord(int studentAcademicRecordID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandReadPermissions(loginSessionGuid);
				return aspireContext.StudentAcademicRecords.SingleOrDefault(sar => sar.StudentAcademicRecordID == studentAcademicRecordID && sar.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
			}
		}

		#region Student Custom Information

		public sealed class StudentCustomInfo
		{
			internal StudentCustomInfo()
			{
			}

			public string Name { get; internal set; }
			public string Subject { get; internal set; }
			public string Description { get; internal set; }
			public string CreatedByUser { get; internal set; }
			public DateTime CreatedDate { get; internal set; }
		}

		public enum AddStudentCustomInformationStatuses
		{
			NoRecordFound,
			Success,
		}

		public static AddStudentCustomInformationStatuses AddStudentCustomInformation(int studentID, string subject, string description, DateTime createdDate, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => new
				{
					s.AdmissionOpenProgram.SemesterID,
					s.AdmissionOpenProgramID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.Program.InstituteID
				}).SingleOrDefault();
				if (student == null)
					return AddStudentCustomInformationStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				var studentCustomInformation = new Model.Entities.StudentCustomInfo
				{
					StudentID = studentID,
					Subject = subject,
					Description = description,
					CreatedDate = createdDate,
					CreatedByUserLoginHistoryID = loginHistory.UserLoginHistoryID,
				};
				aspireContext.StudentCustomInfos.Add(studentCustomInformation);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddStudentCustomInformationStatuses.Success;
			}
		}

		public static List<StudentCustomInfo> GetStudentCustomInformation(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandReadPermissions(loginSessionGuid);
				return aspireContext.StudentCustomInfos.Where(sci => sci.StudentID == studentID).Select(sci => new StudentCustomInfo
				{
					Name = sci.Student.Name,
					Subject = sci.Subject,
					Description = sci.Description,
					CreatedByUser = sci.UserLoginHistory.UserName,
					CreatedDate = sci.CreatedDate,
				}).OrderBy(sci => sci.CreatedDate).ToList();
			}
		}

		#endregion

		public sealed class Student
		{
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public byte? Status { get; internal set; }
			public string StatusFullName => ((Model.Entities.Student.Statuses?)this.Status)?.ToFullName();
			public List<AcademicRecord> AcademicRecords { get; internal set; }

			public sealed class AcademicRecord
			{
				internal AcademicRecord() { }
				public byte? DegreeType { get; internal set; }
				public string DegreeTypeFullName => ((DegreeTypes?)this.DegreeType)?.ToFullName();
				public double? ObtainedMarks { get; internal set; }
				public double? TotalMarks { get; internal set; }
				public double? Percentage { get; internal set; }
				public bool? IsCGPA { get; internal set; }
				public StudentAcademicRecord.Statuses? StatusEnum { get; internal set; }
				public string StatusFullName => this.StatusEnum?.ToString();
			}

			internal Student() { }
		}

		public static List<Student> GetStudentAcademicRecords(short intakeSemesterID, int programID, List<DegreeTypes> degreeTypeEnums, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandReadPermissions(loginSessionGuid);
				var degreeTypes = degreeTypeEnums.Cast<byte>().ToList();
				return aspireContext.Students
					.Where(s => s.AdmissionOpenProgram.SemesterID == intakeSemesterID && s.AdmissionOpenProgram.ProgramID == programID && s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Select(s => new Student
					{
						StudentID = s.StudentID,
						Enrollment = s.Enrollment,
						Name = s.Name,
						ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
						IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
						Status = s.Status,
						AcademicRecords = s.StudentAcademicRecords
							.Where(sar => degreeTypes.Contains(sar.DegreeType))
							.Select(sar => new Student.AcademicRecord
							{
								DegreeType = sar.DegreeType,
								ObtainedMarks = sar.ObtainedMarks,
								TotalMarks = sar.TotalMarks,
								Percentage = sar.Percentage,
								IsCGPA = sar.IsCGPA,
								StatusEnum = (StudentAcademicRecord.Statuses?)sar.Status
							}).ToList(),
					}).ToList();
			}
		}

		public enum UpdateResult
		{
			NoRecordFound,
			Success
		}

		public static UpdateResult UpdatePersonalInformation(int studentID, string personalEmail, string universityEmail, string phoneHome, string mobile, string cnic, string passportNo, DateTime dob, Genders genderEnum, Categories categoryEnum, BloodGroups? bloodGroupEnum, string nationality, string country, string province, string district, string tehsil, string domicile, AreaTypes? areaTypeEnum, string serviceNo, string religion, string nextOfKin, string currentAddress, string permanentAddress, string physicalDisability, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgram.AdmissionOpenProgramID,
						Student = s,
					}).SingleOrDefault();
				if (record == null)
					return UpdateResult.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				var student = record.Student;
				student.PersonalEmail = personalEmail.ToNullIfWhiteSpace();
				student.UniversityEmail = universityEmail.ToNullIfWhiteSpace();
				student.Phone = phoneHome.ToNullIfWhiteSpace();
				student.Mobile = mobile.ToNullIfWhiteSpace();
				student.CNIC = cnic.ToNullIfWhiteSpace();
				student.PassportNo = passportNo.ToNullIfWhiteSpace();
				student.DOB = dob;
				student.GenderEnum = genderEnum;
				student.CategoryEnum = categoryEnum;
				student.BloodGroupEnum = bloodGroupEnum;
				student.Nationality = nationality.ToNullIfWhiteSpace();
				student.Country = country.ToNullIfWhiteSpace();
				student.Province = province.ToNullIfWhiteSpace();
				student.District = district.ToNullIfWhiteSpace();
				student.Tehsil = tehsil.ToNullIfWhiteSpace();
				student.Domicile = domicile.ToNullIfWhiteSpace();
				student.AreaTypeEnum = areaTypeEnum;
				student.ServiceNo = serviceNo.ToNullIfWhiteSpace();
				student.Religion = religion.ToNullIfWhiteSpace();
				student.NextOfKin = nextOfKin.ToNullIfWhiteSpace();
				student.CurrentAddress = currentAddress.ToNullIfWhiteSpace();
				student.PermanentAddress = permanentAddress.ToNullIfWhiteSpace();
				student.PhysicalDisability = physicalDisability.ToNullIfWhiteSpace();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateResult.Success;
			}
		}

		public static UpdateResult UpdateEmergencyInformation(int studentID, string emergencyContactName, string emergencyMobile, string emergencyPhone, string emergencyRelationship, string emergencyAddress, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgram.AdmissionOpenProgramID,
						Student = s,
					}).SingleOrDefault();
				if (record == null)
					return UpdateResult.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				var student = record.Student;
				student.EmergencyContactName = emergencyContactName.ToNullIfWhiteSpace();
				student.EmergencyMobile = emergencyMobile.ToNullIfWhiteSpace();
				student.EmergencyPhone = emergencyPhone.ToNullIfWhiteSpace();
				student.EmergencyRelationship = emergencyRelationship.ToNullIfWhiteSpace();
				student.EmergencyAddress = emergencyAddress.ToNullIfWhiteSpace();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateResult.Success;
			}
		}

		public static UpdateResult UpdateFatherInformation(int studentID, string fatherName, string fatherCNIC, string fatherPassportNo, string fatherDesignation, string fatherDepartment, string fatherOrganization, string fatherServiceNo, string fatherMobile, string fatherOfficePhone, string fatherHomePhone, string fatherFax, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgram.AdmissionOpenProgramID,
						Student = s,
					}).SingleOrDefault();
				if (record == null)
					return UpdateResult.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				var student = record.Student;
				student.FatherName = fatherName.ToNullIfWhiteSpace();
				student.FatherCNIC = fatherCNIC.ToNullIfWhiteSpace();
				student.FatherPassportNo = fatherPassportNo.ToNullIfWhiteSpace();
				student.FatherDesignation = fatherDesignation.ToNullIfWhiteSpace();
				student.FatherDepartment = fatherDepartment.ToNullIfWhiteSpace();
				student.FatherOrganization = fatherOrganization.ToNullIfWhiteSpace();
				student.FatherServiceNo = fatherServiceNo.ToNullIfWhiteSpace();
				student.FatherMobile = fatherMobile.ToNullIfWhiteSpace();
				student.FatherOfficePhone = fatherOfficePhone.ToNullIfWhiteSpace();
				student.FatherHomePhone = fatherHomePhone.ToNullIfWhiteSpace();
				student.FatherFax = fatherFax.ToNullIfWhiteSpace();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateResult.Success;
			}
		}

		public static UpdateResult UpdateSponsorInformation(int studentID, Sponsorships sponsoredByEnum, string sponsorName, string sponsorCNIC, string sponsorPassportNo, string sponsorRelationshipWithGuardian, string sponsorDesignation, string sponsorDepartment, string sponsorOrganization, string sponsorServiceNo, string sponsorMobile, string sponsorPhoneOffice, string sponsorPhoneHome, string sponsorFax, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgram.AdmissionOpenProgramID,
						Student = s,
					}).SingleOrDefault();
				if (record == null)
					return UpdateResult.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				var student = record.Student;
				student.SponsoredByEnum = sponsoredByEnum;
				student.SponsorName = sponsorName.ToNullIfWhiteSpace();
				student.SponsorCNIC = sponsorCNIC.ToNullIfWhiteSpace();
				student.SponsorPassportNo = sponsorPassportNo.ToNullIfWhiteSpace();
				student.SponsorRelationshipWithGuardian = sponsorRelationshipWithGuardian.ToNullIfWhiteSpace();
				student.SponsorDesignation = sponsorDesignation.ToNullIfWhiteSpace();
				student.SponsorDepartment = sponsorDepartment.ToNullIfWhiteSpace();
				student.SponsorOrganization = sponsorOrganization.ToNullIfWhiteSpace();
				student.SponsorServiceNo = sponsorServiceNo.ToNullIfWhiteSpace();
				student.SponsorMobile = sponsorMobile.ToNullIfWhiteSpace();
				student.SponsorPhoneOffice = sponsorPhoneOffice.ToNullIfWhiteSpace();
				student.SponsorPhoneHome = sponsorPhoneHome.ToNullIfWhiteSpace();
				student.SponsorFax = sponsorFax.ToNullIfWhiteSpace();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateResult.Success;
			}
		}

		public static UpdateResult AddOrUpdateAdditionalInformation(int studentID, int? studentCustomInfoID, Model.Entities.StudentCustomInfo.Types typeEnum, DateTime referenceDate, string subject, string description, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.StudentID,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgram.AdmissionOpenProgramID,
						StudentCustomInfo = s.StudentCustomInfos.FirstOrDefault(sci => sci.StudentCustomInfoID == studentCustomInfoID),
					}).SingleOrDefault();
				if (record == null)
					return UpdateResult.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				if (studentCustomInfoID == null)
				{
					if (record.StudentCustomInfo != null)
						throw new InvalidOperationException();
					aspireContext.StudentCustomInfos.Add(new Model.Entities.StudentCustomInfo
					{
						StudentID = record.StudentID,
						ReferenceDate = referenceDate,
						TypeEnum = typeEnum,
						Subject = subject.TrimAndCannotBeEmpty(),
						Description = description.TrimAndCannotBeEmpty(),
						CreatedDate = DateTime.Now,
						CreatedByUserLoginHistoryID = loginHistory.UserLoginHistoryID
					});
				}
				else
				{
					if (record.StudentCustomInfo == null)
						return UpdateResult.NoRecordFound;
					record.StudentCustomInfo.TypeEnum = typeEnum;
					record.StudentCustomInfo.ReferenceDate = referenceDate.Date;
					record.StudentCustomInfo.Subject = subject.TrimAndCannotBeEmpty();
					record.StudentCustomInfo.Description = description.TrimAndCannotBeEmpty();
					record.StudentCustomInfo.CreatedDate = DateTime.Now;
					record.StudentCustomInfo.CreatedByUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateResult.Success;
			}
		}

		public static UpdateResult DeleteStudentCustomInfo(int studentID, int studentCustomInfoID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.StudentID,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgram.AdmissionOpenProgramID,
						StudentCustomInfo = s.StudentCustomInfos.FirstOrDefault(sci => sci.StudentCustomInfoID == studentCustomInfoID),
					}).SingleOrDefault();
				if (record == null)
					return UpdateResult.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
				if (record.StudentCustomInfo == null)
					return UpdateResult.NoRecordFound;
				aspireContext.StudentCustomInfos.Remove(record.StudentCustomInfo);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateResult.Success;
			}
		}

		public static UpdateResult DeleteStudentAcademicRecord(int studentID, int studentAcademicRecordID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.StudentID,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgram.AdmissionOpenProgramID,
						StudentAcademicRecord = s.StudentAcademicRecords.FirstOrDefault(sci => sci.StudentAcademicRecordID == studentAcademicRecordID),
					}).SingleOrDefault();
				if (record == null)
					return UpdateResult.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
				if (record.StudentAcademicRecord == null)
					return UpdateResult.NoRecordFound;
				aspireContext.StudentAcademicRecords.Remove(record.StudentAcademicRecord);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateResult.Success;
			}
		}

		public static UpdateResult AddOrUpdateStudentAcademicRecord(int studentID, int? studentAcademicRecordID, DegreeTypes degreeTypeEnum, bool isCGPA, string institute, string boardUniversity, string subjects, double totalMarks, double obtainedMarks, short passingYear, string remarks, StudentAcademicRecord.Statuses statusEnum, Guid loginSessionGuid)
		{
			if (totalMarks < obtainedMarks)
				throw new ArgumentException(nameof(totalMarks));
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.StudentID,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgram.AdmissionOpenProgramID,
						StudentAcademicRecord = s.StudentAcademicRecords.FirstOrDefault(sci => sci.StudentAcademicRecordID == studentAcademicRecordID),
					}).SingleOrDefault();
				if (record == null)
					return UpdateResult.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				if (studentAcademicRecordID == null)
				{
					if (record.StudentAcademicRecord != null)
						throw new InvalidOperationException();
					aspireContext.StudentAcademicRecords.Add(new StudentAcademicRecord
					{
						StudentID = record.StudentID,
						DegreeTypeEnum = degreeTypeEnum,
						IsCGPA = isCGPA,
						Institute = institute.TrimAndCannotBeEmpty(),
						BoardUniversity = boardUniversity.TrimAndCannotBeEmpty(),
						Subjects = subjects.TrimAndCannotBeEmpty(),
						TotalMarks = totalMarks,
						ObtainedMarks = obtainedMarks,
						PassingYear = passingYear,
						StatusEnum = statusEnum,
						Remarks = remarks.TrimAndMakeItNullIfEmpty(),
						Percentage = obtainedMarks * 100d / totalMarks,
						VerificationStatusEnum = StudentAcademicRecord.VerificationStatuses.None,
					}.Validate());
				}
				else
				{
					if (record.StudentAcademicRecord == null)
						return UpdateResult.NoRecordFound;

					record.StudentAcademicRecord.DegreeTypeEnum = degreeTypeEnum;
					record.StudentAcademicRecord.IsCGPA = isCGPA;
					record.StudentAcademicRecord.Institute = institute.TrimAndCannotBeEmpty();
					record.StudentAcademicRecord.BoardUniversity = boardUniversity.TrimAndCannotBeEmpty();
					record.StudentAcademicRecord.Subjects = subjects.TrimAndCannotBeEmpty();
					record.StudentAcademicRecord.TotalMarks = totalMarks;
					record.StudentAcademicRecord.ObtainedMarks = obtainedMarks;
					record.StudentAcademicRecord.PassingYear = passingYear;
					record.StudentAcademicRecord.StatusEnum = statusEnum;
					record.StudentAcademicRecord.Remarks = remarks.TrimAndMakeItNullIfEmpty();
					record.StudentAcademicRecord.Percentage = obtainedMarks * 100d / totalMarks;
					record.StudentAcademicRecord.Validate();
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateResult.Success;
			}
		}

		public static UpdateResult UpdateDegreeInformation(int studentID, int? finalTranscriptNo, DateTime? finalTranscriptIssueDate, decimal? finalCGPA, string degreeNo, int? degreeGazetteNo, DateTime? degreeIssueDate, DateTime? degreeIssuedToStudent, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgram.AdmissionOpenProgramID,
						Student = s,
					}).SingleOrDefault();
				if (record == null)
					return UpdateResult.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				var student = record.Student;
				student.FinalTranscriptNo = finalTranscriptNo;
				student.FinalTranscriptIssueDate = finalTranscriptIssueDate;
				student.FinalCGPA = finalCGPA;
				student.DegreeNo = degreeNo;
				student.DegreeGazetteNo = degreeGazetteNo;
				student.DegreeIssueDate = degreeIssueDate;
				student.DegreeIssuedToStudent = degreeIssuedToStudent;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateResult.Success;
			}
		}

		public static UpdateResult UpdateBasicInformation(int studentID, int? registrationNo, string name, Model.Entities.Student.Statuses? statusEnum, string blockedReason, int? deferredToAdmissionOpenProgramID, Shifts? deferredToShiftEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgram.AdmissionOpenProgramID,
						Student = s,
					}).SingleOrDefault();
				if (record == null)
					return UpdateResult.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				var student = record.Student;
				student.RegistrationNo = registrationNo;
				student.Name = name.TrimAndCannotEmptyAndMustBeUpperCase();
				if (student.DeferredToStudentID == null)
				{
					if (student.StatusEnum != statusEnum
						|| student.DeferredToAdmissionOpenProgramID != deferredToAdmissionOpenProgramID
						|| student.DeferredToShift != (byte?)deferredToShiftEnum)
						student.StatusDate = DateTime.Now;
					student.StatusEnum = statusEnum;
					switch (statusEnum)
					{
						case null:
						case Model.Entities.Student.Statuses.AdmissionCancelled:
						case Model.Entities.Student.Statuses.Blocked:
						case Model.Entities.Student.Statuses.Dropped:
						case Model.Entities.Student.Statuses.Expelled:
						case Model.Entities.Student.Statuses.Graduated:
						case Model.Entities.Student.Statuses.Left:
						case Model.Entities.Student.Statuses.ProgramChanged:
						case Model.Entities.Student.Statuses.Rusticated:
						case Model.Entities.Student.Statuses.TransferredToOtherCampus:
							if (deferredToAdmissionOpenProgramID != null)
								throw new ArgumentException("Must be null.", nameof(deferredToAdmissionOpenProgramID));
							if (deferredToShiftEnum != null)
								throw new ArgumentException("Must be null.", nameof(deferredToShiftEnum));
							student.DeferredToAdmissionOpenProgramID = null;
							student.DeferredToShift = null;
							break;
						case Model.Entities.Student.Statuses.Deferred:
							if ((deferredToAdmissionOpenProgramID == null && deferredToShiftEnum == null)
								|| (deferredToAdmissionOpenProgramID != null && deferredToShiftEnum != null))
							{
								if (deferredToAdmissionOpenProgramID != null)
									deferredToAdmissionOpenProgramID = aspireContext.AdmissionOpenPrograms
										.Where(aop => aop.AdmissionOpenProgramID == deferredToAdmissionOpenProgramID.Value)
										.Where(aop => aop.Program.InstituteID == loginHistory.InstituteID)
										.Select(aop => aop.AdmissionOpenProgramID)
										.Single();
								student.DeferredToAdmissionOpenProgramID = deferredToAdmissionOpenProgramID;
								student.DeferredToShift = (byte?)deferredToShiftEnum;
							}
							else
								throw new ArgumentException("deferredToAdmissionOpenProgramID and deferredToShiftEnum, both should be null or not null at a time.");
							break;
						default:
							throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
					}
				}

				if (statusEnum == Model.Entities.Student.Statuses.Blocked)
					student.BlockedReason = blockedReason.TrimAndCannotBeEmpty();
				else
					student.BlockedReason = blockedReason.TrimAndMakeItNullIfEmpty().MustBeNullOrEmpty();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateResult.Success;
			}
		}

		public enum UpdatePhotoStatuses
		{
			NoRecordFound,
			ResolutionMustBe400X400,
			InvalidImage,
			Success
		}

		public static UpdatePhotoStatuses UpdatePhoto(int studentID, byte[] bytes, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgram.AdmissionOpenProgramID,
						Student = s,
					}).SingleOrDefault();
				if (record == null)
					return UpdatePhotoStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
				var student = record.Student;
				var status = aspireContext.UpdateStudentPicture(record.Student.StudentID, bytes, false, loginSessionGuid);
				switch (status)
				{
					case StudentPictures.UpdateStudentPictureStatuses.Success:
						aspireContext.CommitTransaction();
						return UpdatePhotoStatuses.Success;
					case StudentPictures.UpdateStudentPictureStatuses.ResolutionMustBe400X400:
						return UpdatePhotoStatuses.ResolutionMustBe400X400;
					case StudentPictures.UpdateStudentPictureStatuses.InvalidImage:
						return UpdatePhotoStatuses.InvalidImage;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
		}

		public static byte[] ReadStudentPicture(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var studentExists = aspireContext.Students.Any(s => s.StudentID == studentID && s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				if (studentExists)
					return aspireContext.ReadStudentPicture(studentID);
				return null;
			}
		}

		public enum UpdateHECDegreeVerificationStampDateStatuses
		{
			NoRecordFound,
			Success,
		}

		public static UpdateHECDegreeVerificationStampDateStatuses UpdateHECDegreeVerificationStampDate(string enrollment, Dictionary<int, DateTime?> hecDegreeVerificationStampDates, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var studentID = aspireContext.GetStudentIDFromEnrollment(loginHistory.InstituteID, enrollment);
				var record = aspireContext.Students
					.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.StudentID,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgram.AdmissionOpenProgramID,
					}).SingleOrDefault();
				if (record == null)
					return UpdateHECDegreeVerificationStampDateStatuses.NoRecordFound;
				var loginHistoryPermission = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
				var studentAcademicRecords = aspireContext.StudentAcademicRecords.Where(sar => sar.StudentID == record.StudentID && sar.Student.AdmissionOpenProgram.Program.InstituteID == loginHistoryPermission.InstituteID).ToList();
				foreach (var studentAcademicRecord in studentAcademicRecords)
				{
					if (hecDegreeVerificationStampDates.TryGetValue(studentAcademicRecord.StudentAcademicRecordID, out var hecDegreeVerificationStampDate))
						studentAcademicRecord.HECDegreeVerificationStampDate = hecDegreeVerificationStampDate;
					else
						return UpdateHECDegreeVerificationStampDateStatuses.NoRecordFound;
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateHECDegreeVerificationStampDateStatuses.Success;
			}
		}

		public sealed class RegistrationInfo
		{
			public int RegistrationNo { get; set; }
			public string Enrollment { get; set; }
			public string Name { get; set; }
			public string Message { get; set; }
			public bool Success { get; set; }
		}

		public static void UpdateRegistrationNos(List<RegistrationInfo> data, Guid loginSessionGuid)
		{
			if (data?.Any() != true)
				throw new ArgumentNullException(nameof(data));
			data.SplitIntoGroups(50).ForEach(group =>
			{
				using (var aspireContext = new AspireContext(true))
				{
					var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.UpdateRegistrationNos, UserGroupPermission.PermissionValues.Allowed);
					var studentsQuery = aspireContext.Students.Where(s => s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
					foreach (var registrationInfo in group)
					{
						var student = studentsQuery.SingleOrDefault(s => s.Enrollment == registrationInfo.Enrollment);
						registrationInfo.Message = null;
						if (student == null)
						{
							registrationInfo.Message = $"{registrationInfo.Enrollment} doesn't exists in CMS.";
							registrationInfo.Success = false;
							continue;
						}
						registrationInfo.Message = "";
						registrationInfo.Success = true;
						if (student.Name != registrationInfo.Name)
							registrationInfo.Message += $" Name Mismatched (CMS=\"{student.Name}\", Excel=\"{registrationInfo.Name}\").";

						if (student.RegistrationNo != registrationInfo.RegistrationNo)
						{
							if (student.RegistrationNo != null)
								registrationInfo.Message += $" Registration No Changed (CMS={student.RegistrationNo}, Excel={registrationInfo.RegistrationNo}).";
							student.RegistrationNo = registrationInfo.RegistrationNo;
						}
						if (!string.IsNullOrEmpty(registrationInfo.Message))
							registrationInfo.Message = $"{student.Enrollment}: {registrationInfo.Message}";
						else
							registrationInfo.Message = null;
					}
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				}
			});
		}
	}
}