﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.CourseRegistration.Staff;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;

namespace Aspire.BL.Core.StudentInformation.Staff
{
	public static class DisciplineCaseActivities
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, int instituteID, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.StudentInformation.ManageDisciplineCaseActivities, UserGroupPermission.PermissionValues.Allowed);
		}

		public static List<DisciplineCaseActivity> GetDisciplineCaseActivities(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.DisciplineCaseActivities.Where(dca => dca.InstituteID == loginHistory.InstituteID).ToList();
			}
		}

		public enum AddDisciplineCaseActivityStatuses
		{
			AlreadyExists,
			Success,
		}

		public static AddDisciplineCaseActivityStatuses AddDisciplineCaseActivity(string activityName, int? fineAmount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var loginHistoryPermission = aspireContext.DemandPermissions(loginHistory.InstituteID, loginSessionGuid);
				activityName = activityName.TrimAndCannotBeEmpty();
				if (aspireContext.DisciplineCaseActivities.Any(dca => dca.ActivityName == activityName))
					return AddDisciplineCaseActivityStatuses.AlreadyExists;
				var disciplineCaseActivity = new DisciplineCaseActivity
				{
					InstituteID = loginHistoryPermission.InstituteID,
					ActivityName = activityName,
					FineAmount = fineAmount,
				};
				aspireContext.DisciplineCaseActivities.Add(disciplineCaseActivity);
				aspireContext.SaveChangesAndCommitTransaction(loginHistoryPermission.UserLoginHistoryID);
				return AddDisciplineCaseActivityStatuses.Success;
			}
		}

		public enum UpdateDisciplineCaseActivityStatuses
		{
			NoRecordFound,
			AlreadyExists,
			Success,
		}

		public static UpdateDisciplineCaseActivityStatuses UpdateDisciplineCaseActivity(int disciplineCaseActivityID, string activityName, int? fineAmount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				activityName = activityName.TrimAndCannotBeEmpty();
				var disciplineCaseActivities = aspireContext.DisciplineCaseActivities.AsQueryable();
				var disciplineCaseActivity = disciplineCaseActivities.SingleOrDefault(dca => dca.DisciplineCaseActivityID == disciplineCaseActivityID);
				if (disciplineCaseActivity == null)
					return UpdateDisciplineCaseActivityStatuses.NoRecordFound;
				if (disciplineCaseActivities.Any(dca => dca.ActivityName == activityName && dca.DisciplineCaseActivityID != disciplineCaseActivityID))
					return UpdateDisciplineCaseActivityStatuses.AlreadyExists;
				var loginHistory = aspireContext.DemandPermissions(disciplineCaseActivity.InstituteID, loginSessionGuid);
				disciplineCaseActivity.ActivityName = activityName;
				disciplineCaseActivity.FineAmount = fineAmount;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateDisciplineCaseActivityStatuses.Success;
			}
		}

		public static DisciplineCaseActivity GetDisciplineCaseActivity(int disciplineCaseActivityID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.DisciplineCaseActivities.SingleOrDefault(dca => dca.DisciplineCaseActivityID == disciplineCaseActivityID && dca.InstituteID == loginHistory.InstituteID);
			}
		}

		public enum DeleteDisciplineCaseActivityStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteDisciplineCaseActivityStatuses DeleteDisciplineCaseActivity(int disciplineCaseActivityID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var disciplineCaseActivity = aspireContext.DisciplineCaseActivities.SingleOrDefault(dca => dca.DisciplineCaseActivityID == disciplineCaseActivityID);
				if (disciplineCaseActivity == null)
					return DeleteDisciplineCaseActivityStatuses.NoRecordFound;
				var loginHistoryPermission = aspireContext.DemandPermissions(loginHistory.InstituteID, loginSessionGuid);
				if (disciplineCaseActivity.DisciplineCaseStudentActivities.Any())
					return DeleteDisciplineCaseActivityStatuses.ChildRecordExists;
				aspireContext.DisciplineCaseActivities.Remove(disciplineCaseActivity);
				aspireContext.SaveChangesAndCommitTransaction(loginHistoryPermission.UserLoginHistoryID);
				return DeleteDisciplineCaseActivityStatuses.Success;
			}
		}
	}
}
