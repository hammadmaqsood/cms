﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.EmailsManagement;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FileServer;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.StudentInformation.Admin
{
	public static class Students
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.StudentInformation.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.StudentInformation.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.StudentInformation.ManageStudentAccount, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.StudentInformation.ManageStudentAccount, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class Student
		{
			internal Student() { }
			public int StudentID { get; set; }
			public string UniversityEmail { get; set; }
			public string Enrollment { get; set; }
			public string InstituteAlias { get; set; }
			public string Name { get; set; }
			public string ProgramAlias { get; set; }
		}

		public static List<Student> GetStudents(int instituteID, int? departmentID, int? programID, string searchText, out int virtualItemCount, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				var query = aspireContext.Students.Where(q => q.AdmissionOpenProgram.Program.InstituteID == instituteID);
				if (departmentID != null)
					query = query.Where(q => q.AdmissionOpenProgram.Program.DepartmentID == departmentID);
				if (programID != null)
					query = query.Where(q => q.AdmissionOpenProgram.ProgramID == programID);
				searchText = searchText.TrimAndMakeItNullIfEmpty();
				if (searchText != null)
					query = query.Where(q => q.Name.Contains(searchText)
											|| q.Enrollment.Equals(searchText)
											|| q.UniversityEmail.Contains(searchText)
											|| q.PersonalEmail.Contains(searchText)
											|| q.CNIC.Contains(searchText)
											|| q.FatherName.Contains(searchText));

				virtualItemCount = query.Count();
				var students = query.Select(s => new Student
				{
					StudentID = s.StudentID,
					InstituteAlias = s.AdmissionOpenProgram.Program.Institute.InstituteAlias,
					ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
					Enrollment = s.Enrollment,
					Name = s.Name,
					UniversityEmail = s.UniversityEmail
				});

				switch (sortExpression)
				{
					case nameof(Student.InstituteAlias):
						students = students.OrderBy(sortDirection, q => q.InstituteAlias);
						break;
					case nameof(Student.ProgramAlias):
						students = students.OrderBy(sortDirection, q => q.ProgramAlias);
						break;
					case nameof(Student.Enrollment):
						students = students.OrderBy(sortDirection, q => q.Enrollment);
						break;
					case nameof(Student.Name):
						students = students.OrderBy(sortDirection, q => q.Name);
						break;
					case nameof(Student.UniversityEmail):
						students = students.OrderBy(sortDirection, q => q.UniversityEmail);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}

				return students.Skip(pageIndex * pageSize).Take(pageSize).ToList();
			}
		}

		public sealed class StudentInfo
		{
			internal StudentInfo() { }
			public string UniversityEmail { get; set; }
			public string PersonalEmail { get; set; }
			public string ParentsEmail { get; set; }
			public string Enrollment { get; set; }
			public string Name { get; set; }
			public string FatherName { get; set; }
			public string ProgramAlias { get; set; }
		}

		public static StudentInfo GetStudentInfo(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Programs)
					.SelectMany(p => p.AdmissionOpenPrograms)
					.SelectMany(aop => aop.Students)
					.Where(s => s.StudentID == studentID)
					.Select(s => new StudentInfo
					{
						ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
						Enrollment = s.Enrollment,
						Name = s.Name,
						FatherName = s.FatherName,
						PersonalEmail = s.PersonalEmail,
						UniversityEmail = s.UniversityEmail,
						ParentsEmail = s.ParentsEmail,
					}).SingleOrDefault();
			}
		}

		public enum UpdateParentEmailResult
		{
			Success,
			NoRecordFound,
		}

		public static UpdateParentEmailResult UpdateParentEmail(int studentID, string parentEmail, Guid loginSessionGuid)
		{
			parentEmail = parentEmail.TrimAndMakeItNullIfEmpty();
			if (parentEmail == null)
				throw new ArgumentNullException(nameof(parentEmail));
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						Student = s,
						s.AdmissionOpenProgram.Program.InstituteID
					}).SingleOrDefault();
				if (record == null)
					return UpdateParentEmailResult.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(record.InstituteID, loginSessionGuid);
				record.Student.ParentsEmail = parentEmail.ValidateEmailAddress();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateParentEmailResult.Success;
			}
		}

		public enum DisableParentAccountResult
		{
			NoRecordFound,
			AlreadyDisabled,
			Success,
		}

		public static DisableParentAccountResult DisableParentAccount(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						Student = s,
						s.AdmissionOpenProgram.Program.InstituteID
					}).SingleOrDefault();
				if (record == null)
					return DisableParentAccountResult.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(record.InstituteID, loginSessionGuid);
				if (string.IsNullOrWhiteSpace(record.Student.ParentsEmail))
					return DisableParentAccountResult.AlreadyDisabled;
				record.Student.ParentsEmail = null;
				record.Student.ParentsPassword = null;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DisableParentAccountResult.Success;
			}
		}

		public enum ResetStudentPasswordResult
		{
			NoRecordFound,
			EmailIsRequired,
			Success,
		}

		public static ResetStudentPasswordResult ResetStudentPassword(int studentID, string newPassword, PasswordChangeReasons reasonEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				switch (reasonEnum)
				{
					case PasswordChangeReasons.UserForgotHisPassword:
					case PasswordChangeReasons.ManuallySettingForNewUser:
					case PasswordChangeReasons.PasswordPotentiallyCompromised:
					case PasswordChangeReasons.Troubleshooting:
						break;
					case PasswordChangeReasons.ByUserItSelf:
					case PasswordChangeReasons.SetDuringRegistration:
					case PasswordChangeReasons.UsingResetLink:
						throw new ArgumentException(nameof(reasonEnum));
					default:
						throw new ArgumentOutOfRangeException(nameof(reasonEnum), reasonEnum, null);
				}
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						Student = s,
						s.AdmissionOpenProgram.Program.InstituteID
					}).SingleOrDefault();
				if (record == null)
					return ResetStudentPasswordResult.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(record.InstituteID, loginSessionGuid);
				if (string.IsNullOrWhiteSpace(record.Student.PersonalEmail) && string.IsNullOrWhiteSpace(record.Student.UniversityEmail))
					return ResetStudentPasswordResult.EmailIsRequired;
				var oldHashedPassword = record.Student.Password;
				record.Student.Password = newPassword.ValidatePassword().EncryptPassword();
				var now = DateTime.Now;
				aspireContext.PasswordChangeHistories.Add(new PasswordChangeHistory
				{
					ChangeReasonEnum = reasonEnum,
					StudentID = record.Student.StudentID,
					ChangedDate = now,
					UserLoginHistoryID = loginHistory.UserLoginHistoryID,
					OldPassword = oldHashedPassword,
					NewPassword = record.Student.Password,
					StudentIDOfParent = null,
					CandidateID = null,
					FacultyMemberID = null,
					UserID = null
				});
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				var adminUser = aspireContext.Users.Where(u => u.UserID == loginHistory.UserID).Select(u => new { u.Email, u.Name }).Single();
				aspireContext.SendPasswordHasBeenResetEmailToStudent(record.Student.Enrollment, record.Student.Name, record.Student.UniversityEmail, record.Student.PersonalEmail, newPassword, now, reasonEnum, loginHistory.UserLoginHistoryID, adminUser.Name, adminUser.Email);
				aspireContext.CommitTransaction();
				return ResetStudentPasswordResult.Success;
			}
		}

		private static void SendPasswordHasBeenResetEmailToStudent(this AspireContext aspireContext, string enrollment, string name, string universityEmail, string personalEmail, string password, DateTime changeDateTime, PasswordChangeReasons reasonEnum, int userLoginHistoryID, string adminName, string adminEmail)
		{
			if (string.IsNullOrWhiteSpace(universityEmail) && string.IsNullOrWhiteSpace(personalEmail))
				throw new ArgumentNullException(nameof(universityEmail));
			var subject = "Student Portal Password Reset by Admin";
			var emailBody = Properties.Resources.ResetStudentPasswordByAdmin;
			emailBody = emailBody.Replace("{EnrollmentHtmlEncoded}", enrollment.HtmlEncode());
			emailBody = emailBody.Replace("{NameHtmlEncoded}", name.HtmlEncode());
			emailBody = emailBody.Replace("{PasswordHtmlEncoded}", password.HtmlEncode());
			emailBody = emailBody.Replace("{Date}", changeDateTime.ToString("D").HtmlAttributeEncode());
			emailBody = emailBody.Replace("{Time}", changeDateTime.ToString("T").HtmlEncode());
			emailBody = emailBody.Replace("{ReasonHtmlEncoded}", reasonEnum.ToFullName().HtmlEncode());
			universityEmail = universityEmail.TrimAndMakeItNullIfEmpty();
			personalEmail = personalEmail.TrimAndMakeItNullIfEmpty();
			var createdDate = changeDateTime;
			var expiryDate = (DateTime?)null;
			(string, string)[] toList;
			if (universityEmail != null)
				toList = new[] { (name, universityEmail) };
			else
				toList = new[] { (name, personalEmail) };
			var replyToList = new[] { (adminName, adminEmail) };
			aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.StudentResetPasswordByAdmin, subject, emailBody, true, Email.Priorities.Normal, createdDate, expiryDate, userLoginHistoryID, toList: toList, replyToList: replyToList);
		}

		public enum ResetParentPasswordResult
		{
			NoRecordFound,
			EmailIsRequired,
			Success,
		}

		public static ResetParentPasswordResult ResetParentPassword(int studentID, string newPassword, PasswordChangeReasons reasonEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				switch (reasonEnum)
				{
					case PasswordChangeReasons.UserForgotHisPassword:
					case PasswordChangeReasons.ManuallySettingForNewUser:
					case PasswordChangeReasons.PasswordPotentiallyCompromised:
					case PasswordChangeReasons.Troubleshooting:
						break;
					case PasswordChangeReasons.ByUserItSelf:
					case PasswordChangeReasons.SetDuringRegistration:
					case PasswordChangeReasons.UsingResetLink:
						throw new ArgumentException(nameof(reasonEnum));
					default:
						throw new ArgumentOutOfRangeException(nameof(reasonEnum), reasonEnum, null);
				}
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						Student = s,
						s.AdmissionOpenProgram.Program.InstituteID
					}).SingleOrDefault();
				if (record == null)
					return ResetParentPasswordResult.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(record.InstituteID, loginSessionGuid);
				if (string.IsNullOrWhiteSpace(record.Student.ParentsEmail))
					return ResetParentPasswordResult.EmailIsRequired;
				var oldHashedPassword = record.Student.ParentsPassword;
				record.Student.ParentsPassword = newPassword.ValidatePassword().EncryptPassword();
				aspireContext.PasswordChangeHistories.Add(new PasswordChangeHistory
				{
					ChangeReasonEnum = reasonEnum,
					StudentIDOfParent = record.Student.StudentID,
					ChangedDate = DateTime.Now,
					UserLoginHistoryID = loginHistory.UserLoginHistoryID,
					UserID = null,
					FacultyMemberID = null,
					CandidateID = null,
					StudentID = null,
					OldPassword = oldHashedPassword,
					NewPassword = record.Student.ParentsPassword
				});
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.SendPasswordHasBeenResetByAdminEmailToParent(record.Student.Enrollment, record.Student.Name, record.Student.ParentsEmail, newPassword, DateTime.Now, reasonEnum, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return ResetParentPasswordResult.Success;
			}
		}

		private static void SendPasswordHasBeenResetByAdminEmailToParent(this AspireContext aspireContext, string enrollment, string name, string parentEmail, string password, DateTime changeDateTime, PasswordChangeReasons reasonEnum, int userLoginHistoryID)
		{
			var subject = "Student Portal (For Parents) Password Reset by Admin";
			var emailBody = Properties.Resources.ResetStudentParentPasswordByAdmin;
			emailBody = emailBody.Replace("{EnrollmentHtmlEncoded}", enrollment.HtmlEncode());
			emailBody = emailBody.Replace("{PasswordHtmlEncoded}", password.HtmlEncode());
			emailBody = emailBody.Replace("{Date}", changeDateTime.ToString("D").HtmlAttributeEncode());
			emailBody = emailBody.Replace("{Time}", changeDateTime.ToString("T").HtmlEncode());
			emailBody = emailBody.Replace("{ReasonHtmlEncoded}", reasonEnum.ToFullName().HtmlEncode());
			var createdDate = DateTime.Now;
			var expiryDate = (DateTime?)null;
			var toList = new[] { (string.Empty, parentEmail) };
			aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.StudentParentResetPasswordByAdmin, subject, emailBody, true, Email.Priorities.Normal, createdDate, expiryDate, userLoginHistoryID, toList: toList);
		}

		public enum SendPasswordResetLinkForParentResult
		{
			NoRecordFound,
			EmailIsRequired,
			Success,
		}

		public static SendPasswordResetLinkForParentResult SendPasswordResetLinkForParent(int studentID, string url, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						Student = s,
						s.AdmissionOpenProgram.Program.InstituteID
					}).SingleOrDefault();
				if (record == null)
					return SendPasswordResetLinkForParentResult.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(record.InstituteID, loginSessionGuid);
				if (string.IsNullOrWhiteSpace(record.Student.ParentsEmail))
					return SendPasswordResetLinkForParentResult.EmailIsRequired;
				record.Student.ParentsConfirmationCode = Guid.NewGuid();
				record.Student.ParentsConfirmationCodeExpiryDate = DateTime.Now.AddDays(3);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				var urlWithCode = $"{url}?PC={record.Student.ParentsConfirmationCode:N}";
				aspireContext.SendResetPasswordLinkForParentEmail(record.Student.Enrollment, record.Student.ParentsEmail, urlWithCode, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return SendPasswordResetLinkForParentResult.Success;
			}
		}

		private static void SendResetPasswordLinkForParentEmail(this AspireContext aspireContext, string enrollment, string parentEmail, string url, int userLoginHistoryID)
		{
			var subject = "Reset Password - Student Portal (For Parents)";
			var emailBody = Properties.Resources.ResetStudentParentPasswordEmailLink;
			emailBody = emailBody.Replace("{EnrollmentHtmlEncoded}", enrollment.HtmlEncode());
			emailBody = emailBody.Replace("{LinkHtmlAttributeEncoded}", url.HtmlAttributeEncode());
			emailBody = emailBody.Replace("{LinkHtmlEncoded}", url.HtmlEncode());
			var createdDate = DateTime.Now;
			var expiryDate = (DateTime?)null;
			var toList = new[] { (string.Empty, parentEmail) };
			aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.StudentParentResetPasswordLink, subject, emailBody, true, Email.Priorities.Normal, createdDate, expiryDate, userLoginHistoryID, toList: toList);
		}

		public static byte[] ReadStudentPicture(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandAdminActiveSession(loginSessionGuid);
				if (loginHistory.IsMasterAdmin)
					return aspireContext.ReadStudentPicture(studentID);
				var students = aspireContext.GetAdminInstitutes(loginHistory)
									.SelectMany(i => i.Programs)
									.SelectMany(i => i.AdmissionOpenPrograms)
									.SelectMany(i => i.Students);
				if (students.Any(s => s.StudentID == studentID))
					return aspireContext.ReadStudentPicture(studentID);
				return null;
			}
		}
	}
}
