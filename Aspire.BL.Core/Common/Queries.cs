﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.Common
{
	internal static class Queries
	{
		public static string GetEnrollmentFromStudentID(this AspireContext aspireContext, int studentID)
		{
			return aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => s.Enrollment).SingleOrDefault();
		}

		public static int? GetStudentIDFromEnrollment(this AspireContext aspireContext, int instituteID, string enrollment)
		{
			return aspireContext.Students.Where(s => s.AdmissionOpenProgram.Program.InstituteID == instituteID && s.Enrollment == enrollment).Select(s => (int?)s.StudentID).SingleOrDefault();
		}

		public static IQueryable<Institute> GetInstitutes(this AspireContext aspireContext, int userAdminRoleID)
		{
			var userAdminRoles = aspireContext.UserRoles.Where(r => r.UserRoleID == userAdminRoleID && r.IsDeleted == false);
			if (userAdminRoles.Any(r => r.UserType == UserTypeConstants.MasterAdmin))
				return aspireContext.Institutes.AsQueryable();
			return aspireContext.Institutes.Where(r => r.UserRoleInstitutes.Any(ri => ri.UserRoleID == userAdminRoleID));
		}

		public static int GetUserLoginHistoryID(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.UserLoginHistories.Where(h => h.LoginSessionGuid == loginSessionGuid).Select(h => h.UserLoginHistoryID).Single();
		}

		public static Guid GetLoginSessionGuid(this AspireContext aspireContext, int userLoginHistoryID)
		{
			return aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == userLoginHistoryID).Select(h => h.LoginSessionGuid).Single();
		}
	}
}
