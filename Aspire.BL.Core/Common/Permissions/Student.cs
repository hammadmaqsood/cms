﻿using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.Common.Permissions
{
	public static class Student
	{
		public sealed class StudentLoginHistory
		{
			internal StudentLoginHistory() { }
			public int UserLoginHistoryID { get; internal set; }
			public Guid LoginSessionGuid { get; internal set; }
			public SubUserTypes SubUserTypeEnum { get; internal set; }
			public int StudentID { get; internal set; }
		}

		internal static StudentLoginHistory DemandStudentActiveSession(this AspireContext aspireContext, Guid loginSessionGuid, SubUserTypes? subUserTypeEnum)
		{
			var studentLoginSession = aspireContext.ViewActiveStudentUserLogins.Where(h => h.LoginSessionGuid == loginSessionGuid).Select(h => new StudentLoginHistory
			{
				UserLoginHistoryID = h.UserLoginHistoryID,
				LoginSessionGuid = h.LoginSessionGuid,
				StudentID = h.StudentID,
				SubUserTypeEnum = (SubUserTypes)h.SubUserType,
			}).SingleOrDefault();
			if (studentLoginSession != null && (subUserTypeEnum == null || studentLoginSession.SubUserTypeEnum == subUserTypeEnum))
				return studentLoginSession;
			throw new AspireNotAuthorizedException(UserTypes.Student);
		}

		public static StudentLoginHistory DemandStudentActiveSession(Guid loginSessionGuid, SubUserTypes? subUserTypeEnum)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.DemandStudentActiveSession(loginSessionGuid, subUserTypeEnum);
			}
		}
	}
}