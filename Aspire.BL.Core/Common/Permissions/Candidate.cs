﻿using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.Common.Permissions
{
	public static class Candidate
	{
		public sealed class CandidateLoginHistory
		{
			internal CandidateLoginHistory() { }
			public int UserLoginHistoryID { get; internal set; }
			public Guid LoginSessionGuid { get; internal set; }
			public int CandidateID { get; internal set; }
			public short SemesterID { get; internal set; }
			public bool ForeignStudent { get; internal set; }
		}

		internal static CandidateLoginHistory DemandCandidateActiveSession(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			var loginHistory = aspireContext.ViewActiveCandidateUserLogins
				.Where(l => l.LoginSessionGuid == loginSessionGuid)
				.Select(l => new CandidateLoginHistory
				{
					UserLoginHistoryID = l.UserLoginHistoryID,
					LoginSessionGuid = l.LoginSessionGuid,
					CandidateID = l.CandidateID.Value,
					SemesterID = l.SemesterID,
					ForeignStudent = l.ForeignStudent
				}).SingleOrDefault();
			if (loginHistory != null)
				return loginHistory;
			throw new AspireNotAuthorizedException(UserTypes.Candidate);
		}
	}
}
