﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;
using AspireNotAuthorizedException = Aspire.Model.Entities.Common.AspireNotAuthorizedException;

namespace Aspire.BL.Core.Common.Permissions
{
	public static class Staff
	{
		#region Active Session

		public abstract class LoginHistory
		{
			public Guid LoginSessionGuid { get; internal set; }
			public int UserLoginHistoryID { get; internal set; }
			public int UserID { get; internal set; }
			public int InstituteID { get; internal set; }
		}

		public sealed class StaffLoginHistory : LoginHistory
		{
			internal StaffLoginHistory() { }
		}

		internal static StaffLoginHistory DemandStaffActiveSession(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			var staffLoginSession = aspireContext.ViewActiveStaffUserLogins
				.Where(h => h.LoginSessionGuid == loginSessionGuid)
				.Select(h => new StaffLoginHistory
				{
					UserLoginHistoryID = h.UserLoginHistoryID,
					LoginSessionGuid = h.LoginSessionGuid,
					UserID = h.UserID.Value,
					InstituteID = h.InstituteID.Value
				}).SingleOrDefault();
			return staffLoginSession ?? throw new AspireNotAuthorizedException(UserTypes.Staff);
		}

		#endregion

		#region Group Permissions

		public sealed class StaffGroupPermission : LoginHistory
		{
			internal StaffGroupPermission() { }
			public StaffPermissionType PermissionType { get; internal set; }
			public UserGroupPermission.PermissionValues[] PermissionValuesDemanded { get; internal set; }
			public UserGroupPermission.PermissionValues PermissionValueEnum { get; internal set; }
		}

		internal static UserGroupPermission.PermissionValues? GetStaffPermissionValue(this AspireContext aspireContext, Guid loginSessionGuid, StaffPermissionType permissionType)
		{
			var staffGroupPermission = aspireContext.ViewActiveStaffUserLoginGroupPermissions
				.Where(p => p.LoginSessionGuid == loginSessionGuid && p.Module == permissionType.Module && p.PermissionType == permissionType.PermissionType)
				.Select(p => new StaffGroupPermission
				{
					PermissionValueEnum = (UserGroupPermission.PermissionValues)p.PermissionValue
				}).SingleOrDefault();
			return staffGroupPermission?.PermissionValueEnum;
		}

		internal static StaffGroupPermission DemandStaffPermissions(this AspireContext aspireContext, Guid loginSessionGuid, StaffPermissionType permissionType, params UserGroupPermission.PermissionValues[] permissionValues)
		{
			if (permissionValues == null || permissionValues.Length == 0)
				throw new ArgumentNullException(nameof(permissionValues));
			var staffGroupPermission = aspireContext.ViewActiveStaffUserLoginGroupPermissions
				.Where(p => p.LoginSessionGuid == loginSessionGuid && p.Module == permissionType.Module && p.PermissionType == permissionType.PermissionType)
				.Select(p => new StaffGroupPermission
				{
					UserLoginHistoryID = p.UserLoginHistoryID,
					LoginSessionGuid = p.LoginSessionGuid,
					UserID = p.UserID.Value,
					InstituteID = p.InstituteID.Value,
					PermissionValueEnum = (UserGroupPermission.PermissionValues)p.PermissionValue
				}).SingleOrDefault();
			if (staffGroupPermission != null)
			{
				staffGroupPermission.PermissionType = permissionType;
				staffGroupPermission.PermissionValuesDemanded = permissionValues;
				if (permissionValues.Contains(staffGroupPermission.PermissionValueEnum))
					return staffGroupPermission;
				throw new AspireNotAuthorizedException(UserTypes.Staff, permissionType, permissionValues, staffGroupPermission.PermissionValueEnum);
			}
			throw new AspireNotAuthorizedException(UserTypes.Staff, permissionType, permissionValues, null);
		}

		internal static StaffGroupPermission TryDemandStaffPermissions(this AspireContext aspireContext, Guid loginSessionGuid, StaffPermissionType permissionType, params UserGroupPermission.PermissionValues[] permissionValues)
		{
			try
			{
				return aspireContext.DemandStaffPermissions(loginSessionGuid, permissionType, permissionValues);
			}
			catch (AspireNotAuthorizedException)
			{
				return null;
			}
		}

		public static UserGroupPermission.PermissionValues? GetStaffPermissionValue(Guid loginSessionGuid, StaffPermissionType permissionType)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.GetStaffPermissionValue(loginSessionGuid, permissionType);
			}
		}

		public static StaffGroupPermission DemandStaffPermissions(Guid loginSessionGuid, StaffPermissionType permissionType, params UserGroupPermission.PermissionValues[] permissionValues)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.DemandStaffPermissions(loginSessionGuid, permissionType, permissionValues);
			}
		}

		public static StaffGroupPermission TryDemandStaffPermissions(Guid loginSessionGuid, StaffPermissionType permissionType, params UserGroupPermission.PermissionValues[] permissionValues)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.TryDemandStaffPermissions(loginSessionGuid, permissionType, permissionValues);
			}
		}

		#endregion

		#region Module Permissions

		internal static StaffGroupPermission DemandStaffPermissions(this AspireContext aspireContext, Guid loginSessionGuid, StaffPermissionType permissionType, UserGroupPermission.PermissionValues[] permissionValues, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			if (permissionValues?.Any() != true)
				throw new ArgumentNullException(nameof(permissionValues));
			var permissions = aspireContext.ViewActiveStaffUserLoginGroupPermissionsModules
				.Where(p => p.LoginSessionGuid == loginSessionGuid && p.Module == permissionType.Module && p.PermissionType == permissionType.PermissionType && p.InstituteID == instituteID)
				.Select(p => new
				{
					p.UserLoginHistoryID,
					UserID = p.UserID.Value,
					PermissionValueEnum = (UserGroupPermission.PermissionValues)p.PermissionValue,
					p.DepartmentID,
					p.ProgramID,
					p.AdmissionOpenProgramID
				}).ToList();
			if (!permissions.Any())
				throw new AspireNotAuthorizedException(UserTypes.Staff, permissionType, permissionValues, null);

			var firstPermission = permissions.First();
			if (!permissionValues.Contains(firstPermission.PermissionValueEnum))
				throw new AspireNotAuthorizedException(UserTypes.Staff, permissionType, permissionValues, firstPermission.PermissionValueEnum);

			var staffGroupPermissions = new StaffGroupPermission
			{
				UserID = firstPermission.UserID,
				InstituteID = instituteID,
				LoginSessionGuid = loginSessionGuid,
				UserLoginHistoryID = firstPermission.UserLoginHistoryID,
				PermissionValueEnum = firstPermission.PermissionValueEnum,
				PermissionType = permissionType,
				PermissionValuesDemanded = permissionValues
			};

			if (permissions.Any(m => m.DepartmentID == null && m.ProgramID == null && m.AdmissionOpenProgramID == null))
				return staffGroupPermissions;

			if (departmentID != null)
			{
				if (permissions.Any(m => m.DepartmentID == departmentID.Value && m.ProgramID == null && m.AdmissionOpenProgramID == null))
					return staffGroupPermissions;

				if (programID != null)
				{
					if (permissions.Any(m => m.DepartmentID == departmentID.Value && m.ProgramID == programID.Value && m.AdmissionOpenProgramID == null))
						return staffGroupPermissions;

					if (admissionOpenProgramID != null)
					{
						if (permissions.Any(m => m.DepartmentID == departmentID.Value && m.ProgramID == programID.Value && m.AdmissionOpenProgramID == admissionOpenProgramID.Value))
							return staffGroupPermissions;
					}
				}
			}
			throw new AspireNotAuthorizedException(UserTypes.Staff, staffGroupPermissions.PermissionType, staffGroupPermissions.PermissionValuesDemanded, staffGroupPermissions.PermissionValueEnum);
		}

		public static StaffGroupPermission DemandStaffPermissions(Guid loginSessionGuid, StaffPermissionType permissionType, UserGroupPermission.PermissionValues[] permissionValues, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.DemandStaffPermissions(loginSessionGuid, permissionType, permissionValues, instituteID, departmentID, programID, admissionOpenProgramID);
			}
		}

		internal static StaffGroupPermission DemandStaffPermissions(this AspireContext aspireContext, Guid loginSessionGuid, StaffPermissionType permissionType, UserGroupPermission.PermissionValues permissionValue, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, permissionType, new[] { permissionValue }, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		#endregion

		internal static StaffGroupPermission TryDemandStaffPermissions(this AspireContext aspireContext, Guid loginSessionGuid, StaffPermissionType permissionType, UserGroupPermission.PermissionValues permissionValue, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			try
			{
				return aspireContext.DemandStaffPermissions(loginSessionGuid, permissionType, permissionValue, instituteID, departmentID, programID, admissionOpenProgramID);
			}
			catch (AspireNotAuthorizedException)
			{
				return null;
			}
		}

		internal static StaffGroupPermission TryDemandStaffPermissions(this AspireContext aspireContext, Guid loginSessionGuid, StaffPermissionType permissionType, UserGroupPermission.PermissionValues permissionValue)
		{
			try
			{
				return aspireContext.DemandStaffPermissions(loginSessionGuid, permissionType, permissionValue);
			}
			catch (AspireNotAuthorizedException)
			{
				return null;
			}
		}
	}
}