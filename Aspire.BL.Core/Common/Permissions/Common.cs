﻿using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.Common.Permissions
{
	public static class Common
	{
		public sealed class LoginHistory
		{
			internal LoginHistory() { }
			public int UserLoginHistoryID { get; internal set; }
			public Guid LoginSessionGuid { get; internal set; }
			public int? UserID { get; internal set; }
			public int? UserRoleID { get; internal set; }
			public int? CandidateID { get; internal set; }
			public int? FacultyMemberID { get; internal set; }
			public int? StudentID { get; internal set; }
			public UserTypes UserTypeEnum { get; internal set; }
			public SubUserTypes? SubUserTypeEnum { get; internal set; }
		}

		internal static LoginHistory DemandActiveUserSession(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			var loginHistory = aspireContext.ViewActiveUserLoginHistories
				.Where(h => h.LoginSessionGuid == loginSessionGuid).Select(h => new LoginHistory
				{
					UserLoginHistoryID = h.UserLoginHistoryID,
					LoginSessionGuid = h.LoginSessionGuid,
					UserTypeEnum = (UserTypes)h.UserType,
					SubUserTypeEnum = (SubUserTypes?)h.SubUserType,
					UserID = h.UserID,
					CandidateID = h.CandidateID,
					FacultyMemberID = h.FacultyMemberID,
					StudentID = h.StudentID,
					UserRoleID = h.UserRoleID,
				}).SingleOrDefault();
			return loginHistory ?? throw new AspireNotAuthorizedException();
		}
	}
}
