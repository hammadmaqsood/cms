﻿using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.Common.Permissions
{
	public static class Faculty
	{
		public sealed class FacultyLoginSession
		{
			internal FacultyLoginSession() { }
			public int UserLoginHistoryID { get; internal set; }
			public int FacultyMemberID { get; internal set; }
			public int InstituteID { get; internal set; }
		}

		internal static FacultyLoginSession DemandFacultyActiveSession(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			var facultyLoginSession = aspireContext.ViewActiveFacultyUserLogins.Where(s => s.LoginSessionGuid == loginSessionGuid)
				.Select(h => new FacultyLoginSession
				{
					UserLoginHistoryID = h.UserLoginHistoryID,
					FacultyMemberID = h.FacultyMemberID.Value,
					InstituteID = h.InstituteID,
				}).SingleOrDefault();
			return facultyLoginSession ?? throw new AspireNotAuthorizedException(UserTypes.Faculty);
		}
	}
}