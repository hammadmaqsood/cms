﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.Common.Permissions
{
	public static class Executive
	{
		#region Active Session

		public abstract class LoginHistory
		{
			public int UserLoginHistoryID { get; internal set; }
			public Guid LoginSessionGuid { get; internal set; }
			public int UserID { get; internal set; }
			public int UserRoleID { get; internal set; }
			public int UserGroupID { get; internal set; }
			public int InstituteID { get; internal set; }
		}

		public sealed class ExecutiveLoginHistory : LoginHistory
		{
			internal ExecutiveLoginHistory() { }
		}

		internal static ExecutiveLoginHistory DemandExecutiveActiveSession(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			var executiveLoginSession = aspireContext.ViewActiveExecutiveUserLogins
				.Where(h => h.LoginSessionGuid == loginSessionGuid)
				.Select(h => new ExecutiveLoginHistory
				{
					UserLoginHistoryID = h.UserLoginHistoryID,
					LoginSessionGuid = h.LoginSessionGuid,
					UserID = h.UserID.Value,
					UserRoleID = h.UserRoleID.Value,
					InstituteID = h.InstituteID.Value,
					UserGroupID = h.UserGroupID.Value,
				}).SingleOrDefault();
			return executiveLoginSession ?? throw new AspireNotAuthorizedException(UserTypes.Executive);
		}

		#endregion

		#region Group Permissions

		public sealed class ExecutiveGroupPermission : LoginHistory
		{
			internal ExecutiveGroupPermission() { }
			public ExecutivePermissionType PermissionType { get; internal set; }
			public UserGroupPermission.PermissionValues[] PermissionValuesDemanded { get; internal set; }
			public UserGroupPermission.PermissionValues PermissionValueEnum { get; internal set; }
		}

		internal static ExecutiveGroupPermission DemandExecutivePermissions(this AspireContext aspireContext, Guid loginSessionGuid, ExecutivePermissionType permissionType, params UserGroupPermission.PermissionValues[] demandedPermissionValues)
		{
			if (demandedPermissionValues?.Any() != true)
				throw new ArgumentNullException(nameof(demandedPermissionValues));
			var executiveGroupPermission = aspireContext.ViewActiveExecutiveUserLoginGroupPermissions
				.Where(p => p.LoginSessionGuid == loginSessionGuid && p.Module == permissionType.Module && p.PermissionType == permissionType.PermissionType)
				.Select(p => new ExecutiveGroupPermission
				{
					LoginSessionGuid = p.LoginSessionGuid,
					UserLoginHistoryID = p.UserLoginHistoryID,
					UserID = p.UserID.Value,
					UserRoleID = p.UserRoleID.Value,
					UserGroupID = p.UserGroupID.Value,
					InstituteID = p.InstituteID.Value,
					PermissionValueEnum = (UserGroupPermission.PermissionValues)p.PermissionValue,
				}).SingleOrDefault();
			if (executiveGroupPermission != null)
			{
				executiveGroupPermission.PermissionType = permissionType;
				executiveGroupPermission.PermissionValuesDemanded = demandedPermissionValues;
				if (demandedPermissionValues.Contains(executiveGroupPermission.PermissionValueEnum))
					return executiveGroupPermission;
				throw new AspireNotAuthorizedException(UserTypes.Executive, permissionType, demandedPermissionValues, executiveGroupPermission.PermissionValueEnum);
			}
			throw new AspireNotAuthorizedException(UserTypes.Executive, permissionType, demandedPermissionValues, null);
		}

		#endregion

		#region Module Permissions

		public sealed class ExecutiveModulePermissions
		{
			internal ExecutiveModulePermissions() { }
			public int InstituteID { get; internal set; }
			public int UserLoginHistoryID { get; internal set; }
			public Guid LoginSessionGuid { get; internal set; }
			public IQueryable<Department> Departments { get; internal set; }
			public IQueryable<Program> Programs { get; internal set; }
			public IQueryable<AdmissionOpenProgram> AdmissionOpenPrograms { get; internal set; }
		}

		internal static ExecutiveModulePermissions DemandExecutiveModulePermissions(this AspireContext aspireContext, ExecutiveGroupPermission executiveGroupPermissions)
		{
			var userRoleModules = aspireContext.UserRoleModules
				.Where(m => m.UserRoleID == executiveGroupPermissions.UserRoleID && m.Module == executiveGroupPermissions.PermissionType.Module)
				.Select(m => new { m.DepartmentID, m.ProgramID, m.AdmissionOpenProgramID })
				.Distinct()
				.ToList();
			var executiveModulePermissions = new ExecutiveModulePermissions
			{
				LoginSessionGuid = executiveGroupPermissions.LoginSessionGuid,
				UserLoginHistoryID = executiveGroupPermissions.UserLoginHistoryID,
				InstituteID = executiveGroupPermissions.InstituteID,
				Departments = aspireContext.Departments.Where(d => d.InstituteID == executiveGroupPermissions.InstituteID),
				Programs = aspireContext.Programs.Where(p => p.InstituteID == executiveGroupPermissions.InstituteID),
				AdmissionOpenPrograms = aspireContext.AdmissionOpenPrograms.Where(aop => aop.Program.InstituteID == executiveGroupPermissions.InstituteID)
			};

			if (!userRoleModules.Any())
			{
				executiveModulePermissions.Departments = executiveModulePermissions.Departments.Where(d => false);
				executiveModulePermissions.Programs = executiveModulePermissions.Programs.Where(p => false);
				executiveModulePermissions.AdmissionOpenPrograms = executiveModulePermissions.AdmissionOpenPrograms.Where(p => false);
				return executiveModulePermissions;
			}
			if (userRoleModules.Any(m => m.DepartmentID == null && m.ProgramID == null && m.AdmissionOpenProgramID == null))
				return executiveModulePermissions;

			var departmentIDs = userRoleModules.Where(m => m.DepartmentID != null && m.ProgramID == null && m.AdmissionOpenProgramID == null).Select(m => m.DepartmentID).Distinct().ToList();
			var programIDs = userRoleModules.Where(m => m.ProgramID != null && m.AdmissionOpenProgramID == null).Select(m => m.ProgramID.Value).Distinct().ToList();
			var admissionOpenProgramIDs = userRoleModules.Where(m => m.AdmissionOpenProgramID != null).Select(m => m.AdmissionOpenProgramID.Value).Distinct().ToList();

			executiveModulePermissions.Departments = executiveModulePermissions.Departments.Where(d => departmentIDs.Contains(d.DepartmentID));
			executiveModulePermissions.Programs = executiveModulePermissions.Programs.Where(p => departmentIDs.Contains(p.DepartmentID) || programIDs.Contains(p.ProgramID));
			executiveModulePermissions.AdmissionOpenPrograms = executiveModulePermissions.AdmissionOpenPrograms.Where(aop => departmentIDs.Contains(aop.Program.DepartmentID) || programIDs.Contains(aop.ProgramID) || admissionOpenProgramIDs.Contains(aop.AdmissionOpenProgramID));
			return executiveModulePermissions;
		}

		internal static ExecutiveModulePermissions DemandExecutiveModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid, ExecutivePermissionType permissionType, params UserGroupPermission.PermissionValues[] permissionValues)
		{
			var executiveGroupPermissions = aspireContext.DemandExecutivePermissions(loginSessionGuid, permissionType, permissionValues);
			return aspireContext.DemandExecutiveModulePermissions(executiveGroupPermissions);
		}

		internal static ExecutiveModulePermissions TryDemandExecutiveModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid, ExecutivePermissionType permissionType, params UserGroupPermission.PermissionValues[] permissionValues)
		{
			try
			{
				var executiveGroupPermissions = aspireContext.DemandExecutivePermissions(loginSessionGuid, permissionType, permissionValues);
				return aspireContext.DemandExecutiveModulePermissions(executiveGroupPermissions);
			}
			catch (AspireNotAuthorizedException)
			{
				return null;
			}
		}

		public static ExecutiveModulePermissions DemandExecutiveModulePermissions(Guid loginSessionGuid, ExecutivePermissionType permissionType, params UserGroupPermission.PermissionValues[] permissionValues)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, permissionType, permissionValues);
			}
		}

		public static ExecutiveModulePermissions TryDemandExecutiveModulePermissions(Guid loginSessionGuid, ExecutivePermissionType permissionType, params UserGroupPermission.PermissionValues[] permissionValues)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.TryDemandExecutiveModulePermissions(loginSessionGuid, permissionType, permissionValues);
			}
		}

		#endregion
	}
}