﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.Common.Permissions
{
	public static class Admin
	{
		public sealed class AdminLoginHistory
		{
			internal AdminLoginHistory() { }
			public Guid LoginSessionGuid { get; internal set; }
			public int UserLoginHistoryID { get; internal set; }
			public int UserID { get; internal set; }
			public int? UserGroupID { get; internal set; }
			public int UserRoleID { get; internal set; }
			public UserTypes UserTypeEnum { get; internal set; }
			public bool IsMasterAdmin => this.UserTypeEnum.IsMasterAdmin();
		}

		internal static AdminLoginHistory DemandAdminActiveSession(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			var loginHistory = aspireContext.ViewActiveAdminUserLogins
				.Where(l => l.LoginSessionGuid == loginSessionGuid)
				.Select(l => new AdminLoginHistory
				{
					UserLoginHistoryID = l.UserLoginHistoryID,
					LoginSessionGuid = l.LoginSessionGuid,
					UserID = l.UserID.Value,
					UserTypeEnum = (UserTypes)l.UserType,
					UserGroupID = l.UserGroupID,
					UserRoleID = l.UserRoleID.Value
				}).SingleOrDefault();
			return loginHistory ?? throw new AspireNotAuthorizedException(UserTypes.Admins);
		}

		internal static AdminLoginHistory DemandAdminActiveSessionAndInstitute(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID)
		{
			var loginHistory = aspireContext.DemandAdminActiveSession(loginSessionGuid);
			if (loginHistory.UserTypeEnum.IsMasterAdmin())
				return loginHistory;
			if (aspireContext.UserRoleInstitutes.Any(r => r.UserRoleID == loginHistory.UserRoleID && r.InstituteID == instituteID))
				return loginHistory;
			throw new AspireNotAuthorizedException(UserTypes.Admins);
		}

		internal static AdminLoginHistory DemandAdminPermissions(this AspireContext aspireContext, Guid loginSessionGuid, AdminPermissionType permissionType, params UserGroupPermission.PermissionValues[] demandedPermissionValues)
		{
			if (demandedPermissionValues?.Any() != true)
				throw new ArgumentNullException(nameof(demandedPermissionValues));
			var loginHistory = aspireContext.DemandAdminActiveSession(loginSessionGuid);
			if (loginHistory.IsMasterAdmin)
				return loginHistory;
			var userGroupPermission = aspireContext.UserGroupPermissions
				.Where(g => g.UserGroupID == loginHistory.UserGroupID.Value && g.Module == permissionType.Module && g.PermissionType == permissionType.PermissionType)
				.Select(p => new
				{
					PermissionValueEnum = (UserGroupPermission.PermissionValues)p.PermissionValue
				})
				.SingleOrDefault();
			if (userGroupPermission != null)
			{
				if (demandedPermissionValues.Contains(userGroupPermission.PermissionValueEnum))
					return loginHistory;
				throw new AspireNotAuthorizedException(UserTypes.Admins, permissionType, demandedPermissionValues, userGroupPermission.PermissionValueEnum);
			}
			throw new AspireNotAuthorizedException(UserTypes.Admins, permissionType, demandedPermissionValues, null);
		}

		internal static AdminLoginHistory DemandAdminPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, AdminPermissionType permissionType, params UserGroupPermission.PermissionValues[] demandedPermissionValues)
		{
			if (demandedPermissionValues?.Any() != true)
				throw new ArgumentNullException(nameof(demandedPermissionValues));
			var loginHistory = aspireContext.DemandAdminActiveSessionAndInstitute(loginSessionGuid, instituteID);
			if (loginHistory.IsMasterAdmin)
				return loginHistory;
			var userGroupPermission = aspireContext.UserGroupPermissions
				.Where(g => g.UserGroupID == loginHistory.UserGroupID.Value && g.Module == permissionType.Module && g.PermissionType == permissionType.PermissionType)
				.Select(p => new
				{
					PermissionValueEnum = (UserGroupPermission.PermissionValues)p.PermissionValue
				})
				.SingleOrDefault();
			if (userGroupPermission != null)
			{
				if (demandedPermissionValues.Contains(userGroupPermission.PermissionValueEnum))
					return loginHistory;
				throw new AspireNotAuthorizedException(UserTypes.Admins, permissionType, demandedPermissionValues, userGroupPermission.PermissionValueEnum);
			}
			throw new AspireNotAuthorizedException(UserTypes.Admins, permissionType, demandedPermissionValues, null);
		}

		public static AdminLoginHistory DemandAdminPermissions(Guid loginSessionGuid, AdminPermissionType permissionType, params UserGroupPermission.PermissionValues[] demandedPermissionValues)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.DemandAdminPermissions(loginSessionGuid, permissionType, demandedPermissionValues);
			}
		}

		internal static IQueryable<Institute> GetAdminInstitutes(this AspireContext aspireContext, AdminLoginHistory loginHistory)
		{
			if (loginHistory.IsMasterAdmin)
				return aspireContext.Institutes.AsQueryable();
			return aspireContext.UserRoleInstitutes.Where(r => r.UserRoleID == loginHistory.UserRoleID).Select(r => r.Institute);
		}

		internal static AdminLoginHistory DemandMasterAdminActiveSession(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			var loginHistory = aspireContext.DemandAdminActiveSession(loginSessionGuid);
			if (loginHistory.IsMasterAdmin)
				return loginHistory;
			throw new AspireNotAuthorizedException(UserTypes.MasterAdmin, null, null, null);
		}
	}
}
