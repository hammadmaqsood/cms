﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.Logs.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;

namespace Aspire.BL.Core.Common.EmailsManagement
{
	public static class EmailHelper
	{
		internal static Email AddEmailAndSaveChanges(this AspireContext aspireContext,
			Email.EmailTypes emailTypeEnum,
			string subject,
			string body,
			bool isBodyHtml,
			Email.Priorities priorityEnum,
			DateTime createdDate,
			DateTime? expiryDate,
			int userLoginHistoryID,
			IEnumerable<(string DisplayName, string EmailAddress)> toList,
			IEnumerable<(string DisplayName, string EmailAddress)> ccList = null,
			IEnumerable<(string DisplayName, string EmailAddress)> bccList = null,
			IEnumerable<(string DisplayName, string EmailAddress)> replyToList = null,
			IEnumerable<(string FileName, byte[] FileContents)> attachmentList = null)
		{
			var email = Email.CreateNew(emailTypeEnum, subject, body, isBodyHtml, priorityEnum, userLoginHistoryID, createdDate, expiryDate, null)
				.ToList(toList?.ToArray())
				.CcList(ccList?.ToArray())
				.BccList(bccList?.ToArray())
				.ReplyToList(replyToList?.ToArray())
				.AddAttachments(attachmentList?.ToArray());
			aspireContext.Emails.Add(email.Validate());
			aspireContext.SaveChanges(userLoginHistoryID, false);
			EmailEngine.EmailsQueue.Add(new EmailInfo(email.EmailID, email.CreatedDate, email.ExpiryDate, email.PriorityEnum));
			return email;
		}

		public static void AddEmailAndSaveChanges(
			Email.EmailTypes emailTypeEnum,
			string subject,
			string body,
			bool isBodyHtml,
			Email.Priorities priorityEnum,
			DateTime createdDate,
			DateTime? expiryDate,
			Guid loginSessionGuid,
			IEnumerable<(string DisplayName, string EmailAddress)> toList,
			IEnumerable<(string DisplayName, string EmailAddress)> ccList = null,
			IEnumerable<(string DisplayName, string EmailAddress)> bccList = null,
			IEnumerable<(string DisplayName, string EmailAddress)> replyToList = null,
			IEnumerable<(string FileName, byte[] FileContents)> attachmentList = null)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var userLoginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				aspireContext.AddEmailAndSaveChanges(emailTypeEnum, subject, body, isBodyHtml, priorityEnum, createdDate, expiryDate, userLoginHistory.UserLoginHistoryID, toList, ccList, bccList, replyToList, attachmentList);
				aspireContext.CommitTransaction();
			}
		}

		public static void QueuePendingEmails()
		{
			List<EmailInfo> pendingEmails;
			using (var aspireContext = new AspireContext())
			{
				var validStatuses = new[] { Email.Statuses.Error, Email.Statuses.Pending, Email.Statuses.SMTPServerNotAvailable }.Select(s => s.GetGuid()).ToList();
				pendingEmails = aspireContext.Emails.Where(e => validStatuses.Contains(e.Status)).Select(e => new
				{
					e.EmailID,
					e.CreatedDate,
					e.ExpiryDate,
					PriorityEnum = (Email.Priorities)e.Priority
				})
				.ToList()
				.Select(e => new EmailInfo(e.EmailID, e.CreatedDate, e.ExpiryDate, e.PriorityEnum))
				.ToList();
			}
			pendingEmails.ForEach(EmailEngine.EmailsQueue.Add);
		}

		public static class EmailEngine
		{
			private static readonly object LockForDictionary = new object();
			private static readonly Dictionary<Guid, object> EmailLocks = new Dictionary<Guid, object>();
			private static readonly Dictionary<Guid, object> SmtpServerLocks = new Dictionary<Guid, object>();
			private static readonly int RetryAttemptsOnError = 10;
			private static readonly TimeSpan SmtpServerBlockTime = TimeSpan.FromMinutes(1);

			private static object GetEmailLock(Guid emailID)
			{
				lock (LockForDictionary)
				{
					if (EmailLocks.ContainsKey(emailID))
						return EmailLocks[emailID];
					var @object = new object();
					EmailLocks.Add(emailID, @object);
					return @object;
				}
			}

			private static void ReleaseEmailLock(Guid emailID)
			{
				lock (LockForDictionary)
				{
					EmailLocks.Remove(emailID);
				}
			}

			private static object GetSmtpServerLock(Guid smtpServerID)
			{
				lock (LockForDictionary)
				{
					if (SmtpServerLocks.ContainsKey(smtpServerID))
						return SmtpServerLocks[smtpServerID];
					var @object = new object();
					SmtpServerLocks.Add(smtpServerID, @object);
					return @object;
				}
			}

			private static void ReleaseSmtpServerLock(Guid smtpServerID)
			{
				lock (LockForDictionary)
				{
					SmtpServerLocks.Remove(smtpServerID);
				}
			}

			private sealed class EmailMessage
			{
				public string FromDisplayName { get; set; }
				public MailMessage MailMessage { get; set; }
			}

			private static Email.Statuses? SendEmail(Guid emailID)
			{
				try
				{
					lock (GetEmailLock(emailID))
					{
						using (var aspireContext = new AspireContext(true))
						{
							var email = aspireContext.Emails
								.Include(e => e.EmailRecipients)
								.Include(e => e.EmailAttachments)
								.Include(e => e.EmailSendAttempts)
								.SingleOrDefault(e => e.EmailID == emailID);
							if (email == null)
								return null;
							var now = DateTime.Now;
							switch (email.StatusEnum)
							{
								case Email.Statuses.Success:
								case Email.Statuses.SuccessNoRecipient:
								case Email.Statuses.AttemptsExceeded:
								case Email.Statuses.Expired:
									return email.StatusEnum;
								case Email.Statuses.Error:
								case Email.Statuses.Pending:
								case Email.Statuses.SMTPServerNotAvailable:
									email.AttemptsCount++;
									if (email.ExpiryDate != null && email.ExpiryDate.Value < now)
									{
										email.StatusEnum = Email.Statuses.Expired;
										email.EmailSendAttempts.Add(new EmailSendAttempt
										{
											EmailSendAttemptID = Guid.NewGuid(),
											EmailID = email.EmailID,
											AttemptDate = now,
											StatusEnum = EmailSendAttempt.Statuses.Expired,
											SMTPServerID = null,
											ErrorMessage = null
										});
										aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
										return email.StatusEnum;
									}
									if (email.AttemptsCount > RetryAttemptsOnError)
									{
										email.StatusEnum = Email.Statuses.AttemptsExceeded;
										email.EmailSendAttempts.Add(new EmailSendAttempt
										{
											EmailSendAttemptID = Guid.NewGuid(),
											EmailID = email.EmailID,
											AttemptDate = now,
											StatusEnum = EmailSendAttempt.Statuses.AttemptsExceeded,
											SMTPServerID = null,
											ErrorMessage = null
										});
										aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
										return email.StatusEnum;
									}
									var active = SMTPServer.Statuses.Active.GetGuid();
									var smtpServer = aspireContext.SMTPServers.OrderBy(ss=> ss.BlockedUpTill).FirstOrDefault(ss => ss.Status == active && ss.BlockedUpTill == null || ss.BlockedUpTill.Value < now);
									if (smtpServer == null)
									{
										email.StatusEnum = Email.Statuses.SMTPServerNotAvailable;
										email.EmailSendAttempts.Add(new EmailSendAttempt
										{
											EmailSendAttemptID = Guid.NewGuid(),
											EmailID = email.EmailID,
											AttemptDate = now,
											StatusEnum = EmailSendAttempt.Statuses.SMTPServerNotAvailable,
											SMTPServerID = null,
											ErrorMessage = null
										});
										aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
										return email.StatusEnum;
									}

									lock (GetSmtpServerLock(smtpServer.SMTPServerID))
									{
										var mailMessage = new MailMessage
										{
											Subject = email.Subject,
											Body = email.Body,
											IsBodyHtml = email.IsBodyHtml
										};
										mailMessage.Headers.Add("X-Bahria-Application", $"<{Lib.Helpers.Configurations.Current.AppSettings.Application}>");
										mailMessage.Headers.Add("X-Bahria-EmailID", $"<{email.EmailID}>");
										email.EmailRecipients.ForEach(r =>
										{
											switch (r.RecipientTypeEnum)
											{
												case EmailRecipient.RecipientTypes.To:
													mailMessage.To.Add(new MailAddress(r.EmailAddress, r.DisplayName));
													break;
												case EmailRecipient.RecipientTypes.Cc:
													mailMessage.CC.Add(new MailAddress(r.EmailAddress, r.DisplayName));
													break;
												case EmailRecipient.RecipientTypes.Bcc:
													mailMessage.Bcc.Add(new MailAddress(r.EmailAddress, r.DisplayName));
													break;
												case EmailRecipient.RecipientTypes.ReplyTo:
													mailMessage.ReplyToList.Add(new MailAddress(r.EmailAddress, r.DisplayName));
													break;
												default:
													throw new NotImplementedEnumException(r.RecipientTypeEnum);
											}
										});
										email.EmailAttachments.ForEach(a => mailMessage.Attachments.Add(new Attachment(new MemoryStream(a.FileContents), a.FileName)));
										var emailSendAttempt = aspireContext.EmailSendAttempts.Add(new EmailSendAttempt
										{
											EmailSendAttemptID = Guid.NewGuid(),
											EmailID = email.EmailID,
											AttemptDate = now,
											StatusEnum = EmailSendAttempt.Statuses.SMTPServerNotAvailable,
											SMTPServerID = smtpServer.SMTPServerID,
											ErrorMessage = null
										});

										using (var smtpClient = new SmtpClient(smtpServer.SMTPServerAddress, smtpServer.Port))
										{
											smtpClient.EnableSsl = smtpServer.EnableSSL;
											smtpClient.Credentials = new NetworkCredential(smtpServer.FromEmailAddress, smtpServer.Password);
											mailMessage.From = new MailAddress(smtpServer.FromEmailAddress, email.FromDisplayName);
											smtpServer.CC?.Split(' ', ',', ';')?.Where(e => !string.IsNullOrWhiteSpace(e)).ForEach(e => mailMessage.CC.Add(e));
											smtpServer.BCC?.Split(' ', ',', ';')?.Where(e => !string.IsNullOrWhiteSpace(e)).ForEach(e => mailMessage.Bcc.Add(e));
											smtpServer.ReplyTo?.Split(' ', ',', ';')?.Where(e => !string.IsNullOrWhiteSpace(e)).ForEach(e => mailMessage.ReplyToList.Add(e));
#if DEBUG
											var index = 1;
											mailMessage.To?.ForEach(e => mailMessage.Headers.Add($"X-Bahria-Recipient-{index++}", $"<{e.Address}>"));
											mailMessage.To.Clear();
#endif
											try
											{
												if (mailMessage.To?.Any() == true || mailMessage.CC?.Any() == true || mailMessage.Bcc?.Any() == true)
												{
													smtpClient.Send(mailMessage);
													email.StatusEnum = Email.Statuses.Success;
													emailSendAttempt.StatusEnum = EmailSendAttempt.Statuses.Success;
												}
												else
												{
													email.StatusEnum = Email.Statuses.SuccessNoRecipient;
													emailSendAttempt.StatusEnum = EmailSendAttempt.Statuses.SuccessNoRecipient;
												}
											}
											catch (SmtpException exc)
											{
												email.StatusEnum = Email.Statuses.Error;
												emailSendAttempt.StatusEnum = EmailSendAttempt.Statuses.Error;
												emailSendAttempt.ErrorMessage = exc.Message;
												smtpServer.BlockedUpTill = DateTime.Now.Add(SmtpServerBlockTime);
											}
											aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
											return email.StatusEnum;
										}
									}
								default:
									throw new NotImplementedEnumException(email.StatusEnum);
							}
						}
					}
				}
				finally
				{
					ReleaseEmailLock(emailID);
				}
			}

			internal static readonly BlockingCollection<EmailInfo> EmailsQueue = new BlockingCollection<EmailInfo>(new EmailsPriorityQueue());

			private static bool AttemptSendingEmail(Guid emailID)
			{
				var status = SendEmail(emailID);
				switch (status)
				{
					case null:
					case Email.Statuses.Success:
					case Email.Statuses.SuccessNoRecipient:
					case Email.Statuses.Expired:
					case Email.Statuses.AttemptsExceeded:
					case Email.Statuses.Error:
						return true;
					case Email.Statuses.Pending:
						throw new InvalidOperationException("Status must be other than pending after attempt.");
					case Email.Statuses.SMTPServerNotAvailable:
						Thread.Sleep(TimeSpan.FromSeconds(10));
						return false;
					default:
						throw new NotImplementedEnumException(status);
				}
			}

			public static void StartEmailSending(CancellationToken cancellationToken)
			{
				foreach (var emailInfo in EmailsQueue.GetConsumingEnumerable(cancellationToken))
				{
					cancellationToken.ThrowIfCancellationRequested();
					try
					{
						while (!AttemptSendingEmail(emailInfo.EmailID)) { }
					}
					catch (Exception exc)
					{
						ErrorHandler.LogException(null, exc, -1, typeof(EmailEngine).Name, null, null);
					}
				}
			}

			private sealed class EmailsPriorityQueue : IProducerConsumerCollection<EmailInfo>
			{
				private static readonly Model.Entities.Email.Priorities[] Priorities = Enum.GetValues(typeof(Model.Entities.Email.Priorities)).Cast<Model.Entities.Email.Priorities>().OrderBy(e => e).ToArray();
				private readonly IReadOnlyDictionary<Model.Entities.Email.Priorities, ConcurrentQueue<EmailInfo>> Queues
					= Enum.GetValues(typeof(Model.Entities.Email.Priorities)).Cast<Model.Entities.Email.Priorities>().ToDictionary(p => p, p => new ConcurrentQueue<EmailInfo>());
				private int EmailsCount = 0;

				public bool TryAdd(EmailInfo emailInfo)
				{
					this.Queues[emailInfo.Priority].Enqueue(emailInfo);
					Interlocked.Increment(ref this.EmailsCount);
					return true;
				}

				public bool TryTake(out EmailInfo emailInfo)
				{
					foreach (var priority in Priorities)
						lock (this.Queues)
						{
							if (this.Queues[priority].TryDequeue(out emailInfo))
							{
								Interlocked.Decrement(ref this.EmailsCount);
								return true;
							}
						}
					emailInfo = null;
					return false;
				}

				public int Count => this.EmailsCount;

				void ICollection.CopyTo(Array array, int index)
				{
					this.CopyTo(array as EmailInfo[], index);
				}

				public void CopyTo(EmailInfo[] destination, int destStartingIndex)
				{
					if (destination == null) throw new ArgumentNullException();
					if (destStartingIndex < 0) throw new ArgumentOutOfRangeException();

					int remaining = destination.Length;
					EmailInfo[] temp = this.ToArray();
					for (int i = 0; i < destination.Length && i < temp.Length; i++)
						destination[i] = temp[i];
				}

				public EmailInfo[] ToArray()
				{
					lock (this.Queues)
					{
						return Priorities.SelectMany(p => this.Queues[0]).ToArray();
					}
				}

				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.GetEnumerator();
				}

				public IEnumerator<EmailInfo> GetEnumerator()
				{
					foreach (var emailInfo in Priorities.SelectMany(p => this.Queues[p]))
						yield return emailInfo;
				}

				public bool IsSynchronized => throw new NotSupportedException();

				public object SyncRoot => throw new NotSupportedException();
			}
		}
	}
}
