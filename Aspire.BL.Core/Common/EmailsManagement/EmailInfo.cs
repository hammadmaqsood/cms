﻿using Aspire.Model.Entities;
using System;

namespace Aspire.BL.Core.Common.EmailsManagement
{
	internal sealed class EmailInfo
	{
		public Email.Priorities Priority { get; }
		public Guid EmailID { get; }
		public DateTime CreatedDate { get; }
		public DateTime? ExpiryDate { get; }

		internal EmailInfo(Guid emailID, DateTime createdDate, DateTime? expiryDate, Email.Priorities priority)
		{
			this.Priority = priority;
			this.EmailID = emailID;
			this.CreatedDate = createdDate;
			this.ExpiryDate = expiryDate;
		}
	}
}
