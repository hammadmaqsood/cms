﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Common
{
	public static class Common
	{
		public sealed class Institute
		{
			internal Institute() { }
			public int InstituteID { get; internal set; }
			public string InstituteName { get; internal set; }
			public string InstituteShortName { get; internal set; }
			public string InstituteAlias { get; internal set; }
		}

		internal static Institute GetInstitute(this AspireContext aspireContext, int insituteID, Guid loginSessionGuid)
		{
			aspireContext.DemandActiveUserSession(loginSessionGuid);
			return aspireContext.Institutes
							.Where(i => i.InstituteID == insituteID)
							.Select(i => new Institute
							{
								InstituteID = i.InstituteID,
								InstituteName = i.InstituteName,
								InstituteShortName = i.InstituteShortName,
								InstituteAlias = i.InstituteAlias
							})
							.SingleOrDefault();
		}

		public static Institute GetInstitute(int insituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.GetInstitute(insituteID, loginSessionGuid);
			}
		}

		public sealed class Department
		{
			internal Department() { }
			public int DepartmentID { get; internal set; }
			public int InstituteID { get; internal set; }
			public string DepartmentName { get; internal set; }
			public string DepartmentShortName { get; internal set; }
			public string DepartmentAlias { get; internal set; }
		}

		internal static Department GetDepartment(this AspireContext aspireContext, int departmentID, Guid loginSessionGuid)
		{
			aspireContext.DemandActiveUserSession(loginSessionGuid);
			return aspireContext.Departments
							.Where(d => d.DepartmentID == departmentID)
							.Select(d => new Department
							{
								DepartmentID = d.DepartmentID,
								InstituteID = d.InstituteID,
								DepartmentName = d.DepartmentName,
								DepartmentShortName = d.DepartmentShortName,
								DepartmentAlias = d.DepartmentAlias,
							})
							.SingleOrDefault();
		}

		public static Department GetDepartment(int departmentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.GetDepartment(departmentID, loginSessionGuid);
			}
		}

		public sealed class Program
		{
			internal Program() { }
			public int ProgramID { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public string ProgramName { get; internal set; }
			public string ProgramShortName { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public ProgramDurations DegreeDuration { get; internal set; }
		}

		internal static Program GetProgram(this AspireContext aspireContext, int programID, Guid loginSessionGuid)
		{
			aspireContext.DemandActiveUserSession(loginSessionGuid);
			return aspireContext.Programs
							.Where(p => p.ProgramID == programID)
							.Select(p => new Program
							{
								ProgramID = p.ProgramID,
								DepartmentID = p.DepartmentID,
								ProgramName = p.ProgramName,
								ProgramShortName = p.ProgramShortName,
								ProgramAlias = p.ProgramAlias,
								DegreeDuration = (ProgramDurations)p.Duration,
							})
							.SingleOrDefault();
		}

		public static Program GetProgram(int programID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.GetProgram(programID, loginSessionGuid);
			}
		}

		private static readonly object IpAddressesLock = new object();
		private static DateTime? IPAddressesLastUpdatedOn = null;
		private static List<(System.Net.IPAddress IPAddress, int InstituteID)?> Addresses = null;

		public static int? GetFirstInstituteID(string inputIPAddress)
		{
			var inputIPAddressNet = inputIPAddress.TryToIPAddress();
			if (inputIPAddressNet == null)
				return null;
			lock (IpAddressesLock)
			{
				if (Addresses == null || IPAddressesLastUpdatedOn == null || IPAddressesLastUpdatedOn.Value.Add(TimeSpan.FromMinutes(3)) < DateTime.Now)
					using (var aspireContext = new AspireContext())
					{
						var addresses = aspireContext.IPAddresses.ToList();
						Addresses = new List<(System.Net.IPAddress IPAddress, int InstituteID)?>();
						addresses.ForEach(a =>
						{
							var ipV4Address = a.IPV4Address.TryToIPAddress();
							var ipV6Address = a.IPV6Address.TryToIPAddress();
							if (ipV4Address != null)
								Addresses.Add((ipV4Address, a.InstituteID));
							if (ipV6Address != null)
								Addresses.Add((ipV6Address, a.InstituteID));
						});
						IPAddressesLastUpdatedOn = DateTime.Now;
					}
				return Addresses.FirstOrDefault(a => a.Value.IPAddress.Equals(inputIPAddressNet))?.InstituteID;
			}
		}
	}
}
