﻿using Aspire.Model.Entities.Common;
using Newtonsoft.Json;
using System;

namespace Aspire.BL.Core.Common.JsonConverters
{
	public sealed class EnumFullNameConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			var underLyingType = Nullable.GetUnderlyingType(objectType);
			if (underLyingType != null)
				return underLyingType.IsEnum;
			return objectType.IsEnum;
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value == null)
				writer.WriteValue((string)null);
			else
				switch (value)
				{
					case ExamGrades examGradeEnum:
						writer.WriteValue(examGradeEnum.ToFullName());
						return;
					default:
						writer.WriteValue(value);
						return;
				}
		}
	}
}