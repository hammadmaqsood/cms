﻿using Aspire.Lib.Extensions;
using Newtonsoft.Json;
using System;

namespace Aspire.BL.Core.Common.JsonConverters
{
	public sealed class YesNoConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			var underLyingType = Nullable.GetUnderlyingType(objectType);
			if (underLyingType != null)
				return underLyingType == typeof(bool);
			return objectType == typeof(bool);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			writer.WriteValue(((bool?)value)?.ToYesNo());
		}
	}
}