﻿using Aspire.Model.Entities.Common;
using Newtonsoft.Json;
using System;

namespace Aspire.BL.Core.Common.JsonConverters
{
	public sealed class EnumGuidFullNameConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			var underLyingType = Nullable.GetUnderlyingType(objectType);
			if (underLyingType != null)
				return underLyingType == typeof(Guid) || underLyingType.IsEnum;
			return objectType == typeof(Guid) || objectType.IsEnum;
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			switch (value)
			{
				case null:
					writer.WriteValue((string)null);
					return;
				case Guid guidValue:
					writer.WriteValue(guidValue.GetFullName());
					return;
				default:
					writer.WriteValue(((Enum)value).GetFullName());
					return;
			}
		}
	}
}
