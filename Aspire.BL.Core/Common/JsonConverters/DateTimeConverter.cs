﻿using System;
using System.Globalization;
using Newtonsoft.Json;

namespace Aspire.BL.Core.Common.JsonConverters
{
	public sealed class DateTimeConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(DateTime?) || objectType == typeof(DateTime);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value == null)
				writer.WriteValue((DateTime?)null);
			writer.WriteValue(((DateTime)value).ToString(CultureInfo.CurrentCulture));
		}
	}
}