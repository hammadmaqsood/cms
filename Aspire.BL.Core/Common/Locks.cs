﻿using System;
using System.Collections.Concurrent;

namespace Aspire.BL.Core.Common
{
	public static class Locks
	{
		public static readonly object GenerateFeeChallan = new object();
		public static readonly object AccountTransaction = new object();
		public static readonly object FeeStructureTransaction = new object();
		public static readonly object ComplaintTransaction = new object();
		public static readonly object ApplyProgram = new object();
		public static readonly object ConfirmCBTSessionLock = new object();
		public static readonly object UserFeedback = new object();

		public enum Modules
		{
			ExamMarksSheetOfferedCourseID,
		}

		private static readonly ConcurrentDictionary<Modules, ConcurrentDictionary<int, object>> ModulesDictionary = new ConcurrentDictionary<Modules, ConcurrentDictionary<int, object>>
		{
			[Modules.ExamMarksSheetOfferedCourseID] = new ConcurrentDictionary<int, object>()
		};

		private static readonly object ReadLock = new object();

		public static object GetItemLock(Modules module, int key)
		{
			lock (ReadLock)
			{
				var dictionary = ModulesDictionary[module];
				if (dictionary.TryGetValue(key, out var lockObject))
					return lockObject;
				lockObject = new object();
				if (dictionary.TryAdd(key, lockObject))
					return lockObject;
				throw new InvalidOperationException();
			}
		}
	}
}
