using System;

namespace Aspire.BL.Core.Common
{
	internal class SortingNotImplementedException : Exception
	{
		public SortingNotImplementedException(string sortExpression) : base(sortExpression)
		{ }
	}
}