﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Common
{
	public static class Lists
	{
		public static List<short> GetSemestersList(bool? visible)
		{
			using (var aspireContext = new AspireContext())
			{
				if (visible == null)
					return aspireContext.Semesters.OrderByDescending(s => s.SemesterID).Select(s => s.SemesterID).ToList();
				if (visible == true)
					return aspireContext.Semesters.Where(s => s.Visible).OrderByDescending(s => s.SemesterID).Select(s => s.SemesterID).ToList();
				return aspireContext.Semesters.Where(s => !s.Visible).OrderByDescending(s => s.SemesterID).Select(s => s.SemesterID).ToList();
			}
		}

		public static List<short> GetScholarshipAnnouncedSemesters()
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.ScholarshipAnnouncements.Select(sa => sa.SemesterID).Distinct().ToList();
			}
		}

		public static List<short> GetScholarshipApprovalSemestersList()
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.ScholarshipApplicationApprovals.Select(saa => saa.SemesterID).Distinct().ToList();
			}
		}

		public static List<CustomListItem> GetAllActiveInstitutes(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandActiveUserSession(loginSessionGuid);
				return aspireContext.Institutes.Where(i => i.Status == Institute.StatusActive).Select(i => new CustomListItem
				{
					ID = i.InstituteID,
					Text = i.InstituteAlias
				}).OrderBy(g => g.Text).ToList();
			}
		}

		public static List<CustomListItem> GetBanksList()
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.Banks.Select(b => new CustomListItem
				{
					ID = b.BankID,
					Text = b.BankAlias + " (" + b.BankName + ")",
				}).OrderBy(b => b.Text).ToList();
			}
		}

		public static List<Country> GetAllCountries()
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.Countries.OrderBy(c => c.CountryName).ToList();
			}
		}

		public static List<Tehsil> GetAllTehsils()
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.Tehsils.OrderBy(t => t.ProvinceName).ThenBy(t => t.DistrictName).ThenBy(t => t.TehsilName).ToList();
			}
		}

		public static List<CustomListItem> GetInstitutesShortNamesList(Institute.Statuses? statusEnum)
		{
			using (var aspireContext = new AspireContext())
			{
				var institutes = aspireContext.Institutes.AsQueryable();
				if (statusEnum != null)
					institutes = institutes.Where(i => i.Status == (byte)statusEnum.Value);
				return institutes.Select(i => new CustomListItem
				{
					ID = i.InstituteID,
					Text = i.InstituteShortName
				}).OrderBy(g => g.Text).ToList();
			}
		}

		public static List<CustomListItemGuid> GetFacultiesList(int instituteID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.Faculties.Where(d => d.InstituteID == instituteID).Select(f => new CustomListItemGuid
				{
					ID = f.FacultyID,
					Text = f.FacultyAlias
				}).OrderBy(d => d.Text).ToList();
			}
		}

		public static List<CustomListItem> GetDepartmentsList(int instituteID, Guid? facultyID)
		{
			using (var aspireContext = new AspireContext())
			{
				var departments = aspireContext.Departments.Where(d => d.InstituteID == instituteID);
				if (facultyID != null)
					departments = departments.Where(d => d.FacultyID == facultyID.Value);
				return departments.Select(s => new CustomListItem
				{
					ID = s.DepartmentID,
					Text = s.DepartmentAlias
				}).OrderBy(d => d.Text).ToList();
			}
		}

		public static List<CustomListItem> GetProgramMajors(int programID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.ProgramMajors.Where(pm => pm.ProgramID == programID).Select(pm => new CustomListItem
				{
					ID = pm.ProgramMajorID,
					Text = pm.Majors
				}).OrderBy(pm => pm.Text).ToList();
			}
		}

		public static List<CustomListItem> GetAdmissionOpenProgramMajors(int admissionOpenProgramID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.AdmissionOpenPrograms.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID).SelectMany(aop => aop.Program.ProgramMajors).Select(pm => new CustomListItem
				{
					ID = pm.ProgramMajorID,
					Text = pm.Majors
				}).OrderBy(pm => pm.Text).ToList();
			}
		}

		public static List<CustomListItem> GetAdmissionOpenPrograms(short semesterID, int instituteID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.AdmissionOpenPrograms
					.Where(aop => aop.Program.InstituteID == instituteID && aop.SemesterID == semesterID)
					.Select(aop => new CustomListItem
					{
						ID = aop.AdmissionOpenProgramID,
						Text = aop.Program.ProgramAlias,
					})
				.OrderBy(aop => aop.Text).ToList();
			}
		}

		public static List<CustomListItem> GetAdmissionOpenPrograms(short semesterID, int instituteID, int? departmentID, bool considerNullDepartmentIDasAll)
		{
			using (var aspireContext = new AspireContext())
			{
				var programs = aspireContext.Programs.Where(pr => pr.InstituteID == instituteID);
				if (departmentID != null || considerNullDepartmentIDasAll == false)
					programs = programs.Where(pr => pr.DepartmentID == departmentID);
				return programs.SelectMany(p => p.AdmissionOpenPrograms).Where(aop => aop.SemesterID == semesterID)
							.Select(aop => new CustomListItem
							{
								ID = aop.AdmissionOpenProgramID,
								Text = aop.Program.ProgramAlias,
							})
							.OrderBy(aop => aop.Text).ToList();
			}
		}

		/// <summary>
		/// If departmentID is null  && considerNullDepartmentIDasAll then display all without where clause.
		/// } 
		/// </summary>
		/// <param name="instituteID"></param>
		/// <param name="departmentID"></param>
		/// <param name="considerNullDepartmentIDasAll"></param>
		/// <returns></returns>
		public static List<CustomListItem> GetPrograms(int instituteID, int? departmentID, bool considerNullDepartmentIDasAll)
		{
			using (var aspireContext = new AspireContext())
			{
				var programs = aspireContext.Programs.Where(pr => pr.InstituteID == instituteID);
				if (departmentID != null || considerNullDepartmentIDasAll == false)
					programs = programs.Where(pr => pr.DepartmentID == departmentID);
				return programs.Select(pr => new CustomListItem
				{
					ID = pr.ProgramID,
					Text = pr.ProgramAlias
				}).OrderBy(p => p.Text).ToList();
			}
		}

		/// <summary>
		/// If departmentID is null  && considerNullDepartmentIDasAll then display all without where clause.
		/// } 
		/// </summary>
		/// <param name="instituteID"></param>
		/// <param name="departmentID"></param>
		/// <param name="degreeLevelEnum"></param>
		/// <param name="considerNullDepartmentIDasAll"></param>
		/// <returns></returns>
		public static List<CustomListItem> GetPrograms(int instituteID, int? departmentID, DegreeLevels? degreeLevelEnum, bool considerNullDepartmentIDasAll)
		{
			using (var aspireContext = new AspireContext())
			{
				var programs = aspireContext.Programs.Where(pr => pr.InstituteID == instituteID);
				if (departmentID != null || considerNullDepartmentIDasAll == false)
					programs = programs.Where(pr => pr.DepartmentID == departmentID);
				if (degreeLevelEnum != null)
					programs = programs.Where(pr => pr.DegreeLevel == (byte)degreeLevelEnum.Value);
				return programs.Select(pr => new CustomListItem
				{
					ID = pr.ProgramID,
					Text = pr.ProgramAlias
				}).OrderBy(p => p.Text).ToList();
			}
		}

		/// <summary>
		/// If departmentID is null  && considerNullDepartmentIDasAll then display all without where clause.
		/// } 
		/// </summary>
		/// <param name="instituteID"></param>
		/// <param name="departmentID"></param>
		/// <param name="considerNullDepartmentIDasAll"></param>
		/// <returns></returns>
		public static List<CustomListItem> GetProgramsWithDepartment(int instituteID, int? departmentID, bool considerNullDepartmentIDasAll)
		{
			using (var aspireContext = new AspireContext())
			{
				var programs = aspireContext.Programs.Where(pr => pr.InstituteID == instituteID);
				if (departmentID != null || considerNullDepartmentIDasAll == false)
					programs = programs.Where(pr => pr.DepartmentID == departmentID);
				return programs.Select(p => new
				{
					p.ProgramID,
					p.ProgramAlias,
					p.Department.DepartmentAlias,
				}).ToList().Select(p => new CustomListItem
				{
					ID = p.ProgramID,
					Text = $"{p.DepartmentAlias} / {p.ProgramAlias}"
				}).OrderBy(p => p.Text).ToList();
			}
		}

		/// <summary>
		/// If departmentID is null  && considerNullDepartmentIDasAll then display all without where clause.
		/// } 
		/// </summary>
		/// <param name="instituteID"></param>
		/// <param name="departmentID"></param>
		/// <param name="degreeLevelEnum"></param>
		/// <param name="considerNullDepartmentIDasAll"></param>
		/// <returns></returns>
		public static List<CustomListItem> GetProgramsWithDepartment(int instituteID, int? departmentID, DegreeLevels? degreeLevelEnum, bool considerNullDepartmentIDasAll)
		{
			using (var aspireContext = new AspireContext())
			{
				var programs = aspireContext.Programs.Where(pr => pr.InstituteID == instituteID);
				if (departmentID != null || considerNullDepartmentIDasAll == false)
					programs = programs.Where(pr => pr.DepartmentID == departmentID);
				if (degreeLevelEnum != null)
					programs = programs.Where(pr => pr.DegreeLevel == (byte)degreeLevelEnum.Value);
				return programs.Select(p => new
				{
					p.ProgramID,
					p.ProgramAlias,
					p.Department.DepartmentAlias,
				}).ToList().Select(p => new CustomListItem
				{
					ID = p.ProgramID,
					Text = $"{p.DepartmentAlias} / {p.ProgramAlias}"
				}).OrderBy(p => p.Text).ToList();
			}
		}

		public static List<CustomListItem> GetFacultyMembersList(int instituteID, FacultyMember.Statuses? statusEnum)
		{
			using (var aspireContext = new AspireContext())
			{
				var facultyMembers = aspireContext.FacultyMembers.Where(fm => fm.InstituteID == instituteID);
				if (statusEnum != null)
					facultyMembers = facultyMembers.Where(fm => fm.Status == (byte)statusEnum.Value);
				return facultyMembers.Select(fm => new CustomListItem
				{
					ID = fm.FacultyMemberID,
					Text = fm.Name,
				}).OrderBy(fm => fm.Text).ToList();
			}
		}

		public static List<CustomListItem> GetFacultyMembersList(int instituteID, short offeredSemesterID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.OfferedCourses
					.Where(oc => oc.SemesterID == offeredSemesterID)
					.Select(oc => oc.FacultyMember)
					.Where(fm => fm.InstituteID == instituteID)
					.Select(fm => new CustomListItem
					{
						ID = fm.FacultyMemberID,
						Text = fm.Name,
					})
					.Distinct()
					.OrderBy(fm => fm.Text)
					.ToList();
			}
		}

		public static List<CustomListItem> GetRoomsForAttendance(int instituteID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.InstituteBuildings.Where(ibs => ibs.InstituteID == instituteID).SelectMany(r => r.Rooms).Select(r => new CustomListItem
				{
					Text = r.RoomName,
					ID = r.RoomID
				}).OrderBy(r => r.Text).ToList();
			}
		}

		public sealed class Student
		{
			internal Student() { }
			public string Enrollment { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string Name { get; internal set; }
			public string FatherName { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public string IntakeSemester => this.IntakeSemesterID.ToSemesterString();
			public byte? Status { get; set; }
			public string StatusFullName => ((Model.Entities.Student.Statuses?)this.Status)?.ToFullName();
		}

		public static List<Student> SearchStudents(string enrollment, int? registrationNo, string name, string fatherName, string cnic, short? intakeSemesterID, int? programID, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				IQueryable<Model.Entities.Student> studentsQuery;
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Admins:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
						var adminLoginHistory = aspireContext.DemandAdminActiveSession(loginSessionGuid);
						studentsQuery = aspireContext.GetAdminInstitutes(adminLoginHistory)
							 .SelectMany(i => i.Programs)
							 .SelectMany(p => p.AdmissionOpenPrograms)
							 .SelectMany(aop => aop.Students);
						break;
					case UserTypes.Executive:
						var executiveLoginHistory = aspireContext.DemandExecutiveActiveSession(loginSessionGuid);
						studentsQuery = aspireContext.Students.Where(s => s.AdmissionOpenProgram.Program.InstituteID == executiveLoginHistory.InstituteID);
						break;
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
						studentsQuery = aspireContext.Students.Where(s => s.AdmissionOpenProgram.Program.InstituteID == staffLoginHistory.InstituteID);
						break;
					case UserTypes.Faculty:
						var facultyLoginHistory = aspireContext.DemandFacultyActiveSession(loginSessionGuid);
						studentsQuery = aspireContext.Students.Where(s => s.AdmissionOpenProgram.Program.InstituteID == facultyLoginHistory.InstituteID);
						break;
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.Candidate:
					case UserTypes.Student:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
				studentsQuery = studentsQuery
					.Where(s => enrollment == null || s.Enrollment.Contains(enrollment))
					.Where(s => registrationNo == null || s.RegistrationNo == registrationNo.Value)
					.Where(s => name == null || s.Name.Contains(name))
					.Where(s => fatherName == null || s.FatherName.Contains(fatherName))
					.Where(s => cnic == null || s.CNIC.Contains(cnic))
					.Where(s => intakeSemesterID == null || s.AdmissionOpenProgram.SemesterID == intakeSemesterID.Value)
					.Where(s => programID == null || s.AdmissionOpenProgram.ProgramID == programID.Value);
				var query = studentsQuery
					.Select(s => new Student
					{
						Enrollment = s.Enrollment,
						RegistrationNo = s.RegistrationNo,
						Name = s.Name,
						FatherName = s.FatherName,
						ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
						IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
						Status = s.Status,
					});
				switch (sortExpression)
				{
					case nameof(Student.Enrollment):
						query = query.OrderBy(sortDirection, s => s.Enrollment);
						break;
					case nameof(Student.RegistrationNo):
						query = query.OrderBy(sortDirection, s => s.RegistrationNo);
						break;
					case nameof(Student.Name):
						query = query.OrderBy(sortDirection, s => s.Name);
						break;
					case nameof(Student.FatherName):
						query = query.OrderBy(sortDirection, s => s.FatherName);
						break;
					case nameof(Student.ProgramAlias):
						query = query.OrderBy(sortDirection, s => s.ProgramAlias);
						break;
					case nameof(Student.IntakeSemesterID):
					case nameof(Student.IntakeSemester):
						query = query.OrderBy(sortDirection, s => s.IntakeSemesterID);
						break;
					case nameof(Student.Status):
					case nameof(Student.StatusFullName):
						query = query.OrderBy(sortDirection, s => s.Status);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return query.Take(100).ToList();
			}
		}
	}
}
