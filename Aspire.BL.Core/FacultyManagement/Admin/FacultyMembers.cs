﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.EmailsManagement;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.FacultyManagement.Admin
{
	public static class FacultyMembers
	{
		private static Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.FacultyManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Common.Permissions.Admin.AdminLoginHistory DemandManageFacultyMemberAccountPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.FacultyManagement.ManageFacultyMemberAccount, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class FacultyMember
		{
			internal FacultyMember() { }
			public int FacultyMemberID { get; internal set; }
			public Model.Entities.FacultyMember.Titles? TitleEnum { get; internal set; }
			public string Name { get; internal set; }
			public string DisplayName { get; internal set; }
			public string DepartmentAlias { get; internal set; }
			public string Username { get; internal set; }
			public string Email { get; internal set; }
			public string CNIC { get; internal set; }
			public Genders GenderEnum { get; internal set; }
			public Model.Entities.FacultyMember.FacultyTypes FacultyTypeEnum { get; internal set; }
		}

		public static (List<FacultyMember> facultyMembers, int virtualItemCount) GetFacultyMembers(int instituteID, int? departmentID, Model.Entities.FacultyMember.Statuses? statusEnum, Model.Entities.FacultyMember.FacultyTypes? facultyTypeEnum, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

				var query = aspireContext.GetAdminInstitutes(loginHistory).Where(i => i.InstituteID == instituteID).SelectMany(i => i.FacultyMembers);
				if (departmentID != null)
					query = query.Where(fm => fm.DepartmentID == departmentID.Value);
				if (statusEnum != null)
					query = query.Where(fm => fm.Status == (byte)statusEnum.Value);
				if (facultyTypeEnum != null)
					query = query.Where(fm => fm.FacultyType == (byte)facultyTypeEnum.Value);
				if (!string.IsNullOrWhiteSpace(searchText))
					query = query.Where(fm => fm.Name.Contains(searchText) || fm.UserName.Contains(searchText) || fm.Email.Contains(searchText) || fm.CNIC.Contains(searchText));

				var virtualItemCount = query.Count();
				switch (sortExpression)
				{
					case nameof(FacultyMember.TitleEnum):
						query = query.OrderBy(sortDirection, c => c.Title);
						break;
					case nameof(FacultyMember.DisplayName):
						query = query.OrderBy(sortDirection, c => c.DisplayName);
						break;
					case nameof(FacultyMember.Name):
						query = query.OrderBy(sortDirection, c => c.Name);
						break;
					case nameof(FacultyMember.CNIC):
						query = query.OrderBy(sortDirection, c => c.CNIC);
						break;
					case nameof(FacultyMember.Email):
						query = query.OrderBy(sortDirection, c => c.Email);
						break;
					case nameof(FacultyMember.GenderEnum):
						query = query.OrderBy(sortDirection, c => c.Gender);
						break;
					case nameof(FacultyMember.FacultyTypeEnum):
						query = query.OrderBy(sortDirection, c => c.FacultyType);
						break;
					case nameof(FacultyMember.DepartmentAlias):
						query = query.OrderBy(sortDirection, c => c.Department.DepartmentAlias);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				var facultyMembers = query.Skip(pageIndex * pageSize).Take(pageSize).Select(fm => new FacultyMember
				{
					FacultyMemberID = fm.FacultyMemberID,
					DepartmentAlias = fm.Department.DepartmentAlias,
					Name = fm.Name,
					Email = fm.Email,
					CNIC = fm.CNIC,
					GenderEnum = (Genders)fm.Gender,
					FacultyTypeEnum = (Model.Entities.FacultyMember.FacultyTypes)fm.FacultyType,
					Username = fm.UserName,
					TitleEnum = (Model.Entities.FacultyMember.Titles?)fm.Title,
					DisplayName = fm.DisplayName,
				}).ToList();
				return (facultyMembers, virtualItemCount);
			}
		}

		public static bool SendPasswordResetLinkToFacultyMember(int facultyMemberID, string pageUrl, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandManageFacultyMemberAccountPermissions(loginSessionGuid);
				var facultyMember = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.FacultyMembers)
					.SingleOrDefault(fm => fm.FacultyMemberID == facultyMemberID);
				if (facultyMember == null)
					return false;
				var createdDate = DateTime.Now;
				var expiryDate = createdDate.AddDays(3);
				facultyMember.ConfirmationCode = Guid.NewGuid();
				facultyMember.ConfirmationCodeExpiryDate = expiryDate;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				var url = $"{pageUrl}?C={facultyMember.ConfirmationCode}";
				var subject = "Reset Bahria University Faculty Portal Password";
				var emailBody = Properties.Resources.FacultyMemberPasswordResetByAdmin;
				emailBody = emailBody.Replace("{NameHtmlEncoded}", facultyMember.Name.HtmlEncode());
				emailBody = emailBody.Replace("{LinkHtmlAttributeEncoded}", url.HtmlAttributeEncode());
				emailBody = emailBody.Replace("{LinkHtmlEncoded}", url.HtmlEncode());
				var adminUser = aspireContext.Users.Where(u => u.UserID == loginHistory.UserID).Select(u => new { u.Name, u.Email }).Single();
				var toList = new[] { (facultyMember.Name, facultyMember.Email) };
				var replyToList = new[] { (adminUser.Name, adminUser.Email) };
				aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.FacultyMemberPasswordResetByAdminEmail, subject, emailBody, true, Email.Priorities.Highest, createdDate, expiryDate, loginHistory.UserLoginHistoryID, toList: toList, replyToList: replyToList);
				aspireContext.CommitTransaction();
				return true;
			}
		}

		public static bool ResetFacultyMemberPassword(int facultyMemberID, string newPassword, PasswordChangeReasons reason, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				switch (reason)
				{
					case PasswordChangeReasons.UserForgotHisPassword:
					case PasswordChangeReasons.ManuallySettingForNewUser:
					case PasswordChangeReasons.PasswordPotentiallyCompromised:
					case PasswordChangeReasons.Troubleshooting:
						break;
					case PasswordChangeReasons.ByUserItSelf:
					case PasswordChangeReasons.SetDuringRegistration:
					case PasswordChangeReasons.UsingResetLink:
						throw new ArgumentException(nameof(reason));
					default:
						throw new NotImplementedEnumException(reason);
				}
				var loginHistory = aspireContext.DemandManageFacultyMemberAccountPermissions(loginSessionGuid);
				var facultyMember = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.FacultyMembers).SingleOrDefault(fm => fm.FacultyMemberID == facultyMemberID);
				if (facultyMember == null)
					return false;
				var oldHashedPassword = facultyMember.Password;
				facultyMember.Password = newPassword.ValidatePassword().EncryptPassword();
				var createdDate = DateTime.Now;
				aspireContext.PasswordChangeHistories.Add(new PasswordChangeHistory
				{
					ChangeReasonEnum = reason,
					FacultyMemberID = facultyMember.FacultyMemberID,
					ChangedDate = createdDate,
					UserLoginHistoryID = loginHistory.UserLoginHistoryID,
					OldPassword = oldHashedPassword,
					NewPassword = facultyMember.Password,
					UserID = null,
					CandidateID = null,
					StudentID = null,
					StudentIDOfParent = null
				});
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);

				var subject = "Faculty Portal Password Changed";
				var emailBody = Properties.Resources.FacultyMemberPasswordHasBeenResetByAdmin;
				emailBody = emailBody.Replace("{NameHtmlEncoded}", facultyMember.Name.HtmlEncode());
				emailBody = emailBody.Replace("{Date}", createdDate.ToString("D").HtmlEncode());
				emailBody = emailBody.Replace("{Time}", createdDate.ToString("T").HtmlEncode());
				emailBody = emailBody.Replace("{Reason}", reason.ToFullName().HtmlEncode());
				var expiryDate = (DateTime?)null;
				var adminUser = aspireContext.Users.Where(u => u.UserID == loginHistory.UserID).Select(u => new { u.Name, u.Email }).Single();
				var toList = new[] { (facultyMember.Name, facultyMember.Email) };
				var replyToList = new[] { (adminUser.Name, adminUser.Email) };
				aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.FacultyMemberPasswordHasBeenResetByAdmin, subject, emailBody, true, Email.Priorities.Highest, createdDate, expiryDate, loginHistory.UserLoginHistoryID, toList: toList, replyToList: replyToList);
				aspireContext.CommitTransaction();
				return true;
			}
		}

		public sealed class FacultyMemberAccount
		{
			internal FacultyMemberAccount() { }
			public int FacultyMemberID { get; internal set; }
			public Model.Entities.FacultyMember.Titles? TitleEnum { get; internal set; }
			public string Name { get; internal set; }
			public string DisplayName { get; internal set; }
			public string DepartmentAlias { get; internal set; }
			public string Username { get; internal set; }
			public string CNIC { get; internal set; }
			public string Email { get; internal set; }
			public Model.Entities.FacultyMember.FacultyTypes FacultyTypeEnum { get; internal set; }
			public short? OfferedSemesterID { get; internal set; }
		}

		public static (List<FacultyMemberAccount> facultyMemberAccount, int virtualItemCount) GetFacultyMembersAccounts(int instituteID, short offeredSemesterID, int? departmentID, Model.Entities.FacultyMember.FacultyTypes? facultyTypeEnum, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

				var query = aspireContext.GetAdminInstitutes(loginHistory).Where(i => i.InstituteID == instituteID)
					.SelectMany(i => i.FacultyMembers).Where(fm => fm.OfferedCourses.Any(oc => oc.SemesterID == offeredSemesterID));
				if (departmentID != null)
					query = query.Where(fm => fm.DepartmentID == departmentID.Value);
				if (facultyTypeEnum != null)
					query = query.Where(fm => fm.FacultyType == (byte)facultyTypeEnum.Value);
				if (!string.IsNullOrWhiteSpace(searchText))
					query = query.Where(fm => fm.Name.Contains(searchText) || fm.UserName.Contains(searchText) || fm.Email.Contains(searchText) || fm.CNIC.Contains(searchText));

				var virtualItemCount = query.Count();
				var finalQuery = query.Select(fm => new FacultyMemberAccount
				{
					FacultyMemberID = fm.FacultyMemberID,
					DepartmentAlias = fm.Department.DepartmentAlias,
					TitleEnum = (Model.Entities.FacultyMember.Titles)fm.Title,
					Name = fm.Name,
					CNIC = fm.CNIC,
					DisplayName = fm.DisplayName,
					Username = fm.UserName,
					Email = fm.Email,
					FacultyTypeEnum = (Model.Entities.FacultyMember.FacultyTypes)fm.FacultyType,
					OfferedSemesterID = offeredSemesterID
				});

				switch (sortExpression)
				{
					case nameof(FacultyMemberAccount.TitleEnum):
						finalQuery = finalQuery.OrderBy(sortDirection, fm => fm.TitleEnum);
						break;
					case nameof(FacultyMemberAccount.Name):
						finalQuery = finalQuery.OrderBy(sortDirection, fm => fm.Name);
						break;
					case nameof(FacultyMemberAccount.DisplayName):
						finalQuery = finalQuery.OrderBy(sortDirection, fm => fm.DisplayName);
						break;
					case nameof(FacultyMemberAccount.Username):
						finalQuery = finalQuery.OrderBy(sortDirection, fm => fm.Username);
						break;
					case nameof(FacultyMemberAccount.Email):
						finalQuery = finalQuery.OrderBy(sortDirection, fm => fm.Email);
						break;
					case nameof(FacultyMemberAccount.CNIC):
						finalQuery = finalQuery.OrderBy(sortDirection, fm => fm.CNIC);
						break;
					case nameof(FacultyMemberAccount.DepartmentAlias):
						finalQuery = finalQuery.OrderBy(sortDirection, fm => fm.DepartmentAlias);
						break;
					case nameof(FacultyMemberAccount.FacultyTypeEnum):
						finalQuery = finalQuery.OrderBy(sortDirection, fm => fm.FacultyTypeEnum);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return (finalQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList(), virtualItemCount);
			}
		}

		public static FacultyMemberAccount GetFacultyMemberAccount(int facultyMemberID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.FacultyMembers)
					.Where(fm => fm.FacultyMemberID == facultyMemberID)
					.Select(fm => new FacultyMemberAccount
					{

						FacultyMemberID = fm.FacultyMemberID,
						TitleEnum = (Model.Entities.FacultyMember.Titles)fm.Title,
						Name = fm.Name,
						DisplayName = fm.DisplayName,
						DepartmentAlias = fm.Department.DepartmentAlias,
						Email = fm.Email,
						Username = fm.UserName,
						CNIC = fm.CNIC,
						FacultyTypeEnum = (Model.Entities.FacultyMember.FacultyTypes)fm.FacultyType,
						OfferedSemesterID = null
					}).SingleOrDefault();
			}
		}

		public static Model.Entities.FacultyMember GetFacultyMember(int facultyMemberID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.FacultyMembers)
					.SingleOrDefault(fm => fm.FacultyMemberID == facultyMemberID);
			}
		}

		public enum AddFacultyMemberStatuses
		{
			NoRecordFound,
			CNICAlreadyExists,
			UsernameAlreadyExists,
			EmailAlreadyExists,
			DisplayNameIsRequiredBecauseNameAlreadyExists,
			DisplayNameMustBeUnique,
			Success
		}

		public static AddFacultyMemberStatuses AddFacultyMember(int instituteID, int departmentID, Model.Entities.FacultyMember.Titles titleEnum, string name, string displayName, string username, string email, Genders genderEnum, string cnic, string passportNo, Model.Entities.FacultyMember.FacultyTypes facultyTypeEnum, Model.Entities.FacultyMember.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandManageFacultyMemberAccountPermissions(loginSessionGuid);
				if (!aspireContext.GetAdminInstitutes(loginHistory).Any(i => i.InstituteID == instituteID))
					return AddFacultyMemberStatuses.NoRecordFound;
				if (!aspireContext.Departments.Any(d => d.DepartmentID == departmentID && d.InstituteID == instituteID))
					return AddFacultyMemberStatuses.NoRecordFound;

				name = name.TrimAndCannotEmptyAndMustBeUpperCase();
				cnic = cnic.MustBeCNIC();
				passportNo = passportNo.TrimAndMakeItNullIfEmpty();
				username = username.TrimAndCannotEmptyAndMustBeLowerCase();

				var facultyMembers = aspireContext.FacultyMembers.Where(fm => fm.InstituteID == instituteID);
				if (facultyMembers.Any(fm => fm.CNIC == cnic))
					return AddFacultyMemberStatuses.CNICAlreadyExists;
				if (facultyMembers.Any(fm => fm.UserName == username))
					return AddFacultyMemberStatuses.UsernameAlreadyExists;
				if (facultyMembers.Any(fm => fm.Email == email))
					return AddFacultyMemberStatuses.EmailAlreadyExists;
				if (facultyMembers.Any(fm => fm.Name == name))
				{
					displayName = displayName.TrimAndMakeItNullIfEmpty();
					if (displayName == null)
						return AddFacultyMemberStatuses.DisplayNameIsRequiredBecauseNameAlreadyExists;
					else
					{
						if (facultyMembers.Any(fm => fm.Name == displayName || fm.DisplayName == displayName))
							return AddFacultyMemberStatuses.DisplayNameMustBeUnique;
					}
				}
				else
				{
					displayName = null;
				}

				aspireContext.FacultyMembers.Add(new Model.Entities.FacultyMember
				{
					TitleEnum = titleEnum,
					Name = name,
					DisplayName = displayName,
					CNIC = cnic,
					Email = email,
					UserName = username,
					FacultyTypeEnum = facultyTypeEnum,
					DepartmentID = departmentID,
					InstituteID = instituteID,
					GenderEnum = genderEnum,
					PassportNo = passportNo,
					StatusEnum = statusEnum,
					ConfirmationCode = null,
					ConfirmationCodeExpiryDate = null
				});

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddFacultyMemberStatuses.Success;
			}
		}

		public enum UpdateFacultyMemberStatuses
		{
			NoRecordFound,
			CNICAlreadyExists,
			UsernameAlreadyExists,
			EmailAlreadyExists,
			DisplayNameIsRequiredBecauseNameAlreadyExists,
			DisplayNameMustBeUnique,
			Success
		}

		public static UpdateFacultyMemberStatuses UpdateFacultyMember(int facultyMemberID, int departmentID, Model.Entities.FacultyMember.Titles titleEnum, string name, string displayName, string username, string email, Genders genderEnum, string cnic, string passportNo, Model.Entities.FacultyMember.FacultyTypes facultyTypeEnum, Model.Entities.FacultyMember.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandManageFacultyMemberAccountPermissions(loginSessionGuid);
				var facultyMember = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.FacultyMembers).SingleOrDefault(fm => fm.FacultyMemberID == facultyMemberID);
				if (facultyMember == null)
					return UpdateFacultyMemberStatuses.NoRecordFound;
				if (!aspireContext.Departments.Any(d => d.DepartmentID == departmentID && d.InstituteID == facultyMember.InstituteID))
					return UpdateFacultyMemberStatuses.NoRecordFound;

				name = name.TrimAndCannotEmptyAndMustBeUpperCase();
				cnic = cnic.MustBeCNIC();
				passportNo = passportNo.TrimAndMakeItNullIfEmpty();
				username = username.TrimAndCannotEmptyAndMustBeLowerCase();

				var facultyMembers = aspireContext.FacultyMembers.Where(fm => fm.InstituteID == facultyMember.InstituteID && fm.FacultyMemberID != facultyMember.FacultyMemberID);
				if (facultyMembers.Any(fm => fm.CNIC == cnic))
					return UpdateFacultyMemberStatuses.CNICAlreadyExists;
				if (facultyMembers.Any(fm => fm.UserName == username))
					return UpdateFacultyMemberStatuses.UsernameAlreadyExists;
				if (facultyMembers.Any(fm => fm.Email == email))
					return UpdateFacultyMemberStatuses.EmailAlreadyExists;
				if (facultyMembers.Any(fm => fm.Name == name))
				{
					displayName = displayName.TrimAndMakeItNullIfEmpty();
					if (displayName == null)
						return UpdateFacultyMemberStatuses.DisplayNameIsRequiredBecauseNameAlreadyExists;
					else
					{
						if (facultyMembers.Any(fm => fm.Name == displayName || fm.DisplayName == displayName))
							return UpdateFacultyMemberStatuses.DisplayNameMustBeUnique;
					}
				}
				else
				{
					displayName = null;
				}

				facultyMember.TitleEnum = titleEnum;
				facultyMember.Name = name;
				facultyMember.DisplayName = displayName;
				facultyMember.CNIC = cnic;
				facultyMember.Email = email;
				facultyMember.UserName = username;
				facultyMember.FacultyTypeEnum = facultyTypeEnum;
				facultyMember.DepartmentID = departmentID;
				facultyMember.GenderEnum = genderEnum;
				facultyMember.PassportNo = passportNo;
				facultyMember.StatusEnum = statusEnum;

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateFacultyMemberStatuses.Success;
			}
		}

		public enum DeleteFacultyMemberStatuses
		{
			NoRecordFound,
			ChildRecordExistsOfferedCourses,
			ChildRecordExistsQASurveyUsers,
			ChildRecordExistsQASummaryFacultyEvaluations,
			ChildRecordExistsSurveyUsers,
			Success
		}

		public static DeleteFacultyMemberStatuses DeleteFacultyMember(int facultyMemberID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandManageFacultyMemberAccountPermissions(loginSessionGuid);
				var facultyMember = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.FacultyMembers)
					.FirstOrDefault(fm => fm.FacultyMemberID == facultyMemberID);
				if (facultyMember == null)
					return DeleteFacultyMemberStatuses.NoRecordFound;
				if (facultyMember.OfferedCourses.Any())
					return DeleteFacultyMemberStatuses.ChildRecordExistsOfferedCourses;
				if (facultyMember.QASurveyUsers.Any())
					return DeleteFacultyMemberStatuses.ChildRecordExistsQASurveyUsers;
				if (facultyMember.QASummaryFacultyEvaluations.Any())
					return DeleteFacultyMemberStatuses.ChildRecordExistsQASummaryFacultyEvaluations;
				if (facultyMember.SurveyUsers.Any())
					return DeleteFacultyMemberStatuses.ChildRecordExistsSurveyUsers;
				aspireContext.FacultyMemberPinCodes.RemoveRange(facultyMember.FacultyMemberPinCodes);
				aspireContext.PasswordChangeHistories.RemoveRange(facultyMember.PasswordChangeHistories);
				aspireContext.UserLoginHistories.RemoveRange(facultyMember.UserLoginHistories);
				aspireContext.FacultyMembers.Remove(facultyMember);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteFacultyMemberStatuses.Success;
			}
		}
	}
}
