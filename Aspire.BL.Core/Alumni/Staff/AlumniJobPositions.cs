﻿using System;
using System.Linq;
using System.Collections.Generic;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.BL.Core.Common.Permissions;

namespace Aspire.BL.Core.Alumni.Staff
{
	public static class AlumniJobPositions
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Alumni.Module, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			aspireContext.DemandModulePermissions(loginSessionGuid);
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Alumni.CanManageJobPositions, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class AlumniBasicInformation
		{
			internal AlumniBasicInformation() { }
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string Department { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public byte Duration { get; internal set; }
			public ProgramDurations DurationEnum => (ProgramDurations)this.Duration;
			public short IntakeSemesterID { get; internal set; }
			public string IntakeSemester => this.IntakeSemesterID.ToSemesterString();
			public short IntakeYear { get; internal set; }
			public short GraduationSemesterID { get; internal set; }
			public string GraduationSemester => this.GraduationSemesterID.ToSemesterString();
			public string GraduationYear => Semester.GetYear(GraduationSemesterID).ToString();

			public Aspire.Model.Entities.Student.Statuses? StatusEnum { get; internal set; }
			public DateTime? StatusDate { get; set; }
			public string BlockedReason { get; internal set; }
			public List<AlumniProfessionalInformation> ProfessionalInformation { get; set; }
		}

		public static AlumniBasicInformation GetAlumniBasicInformation(string enrollment, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var getAlumniBasicInformation = aspireContext.Students
					.Where(s => s.Enrollment == enrollment && s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Select(s => new AlumniBasicInformation
					{
						StudentID = s.StudentID,
						Enrollment = s.Enrollment,
						Name = s.Name,
						Department = s.AdmissionOpenProgram.Program.Department.DepartmentShortName,
						ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
						IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
						Duration = s.AdmissionOpenProgram.Program.Duration,
						IntakeYear = s.AdmissionOpenProgram.Semester.Year,
						GraduationSemesterID = s.StudentSemesters.OrderByDescending(ss => ss.SemesterID).Select(ss => ss.SemesterID).FirstOrDefault(),
						//GraduationYear = s.StudentSemesters.OrderByDescending(ss=>ss.SemesterID).Select(ss => ss.SemesterID)
						StatusEnum = (Model.Entities.Student.Statuses)s.Status,
						StatusDate = s.StatusDate,
						BlockedReason = s.BlockedReason,
					}).SingleOrDefault();

				if (getAlumniBasicInformation != null)
					getAlumniBasicInformation.ProfessionalInformation = aspireContext.GetAlumniProfessionalInformation(getAlumniBasicInformation.StudentID, loginSessionGuid);
				return getAlumniBasicInformation;
			}
		}

		public sealed class AlumniProfessionalInformation
		{
			internal AlumniProfessionalInformation() { }
			public int StudentID { get; internal set; }
			public Guid AlumniJobPositionID { get; internal set; }
			public Guid JobStatus { get; internal set; }
			public DateTime LastUpdatedOn { get; internal set; }
			public string Organization { get; internal set; }
			public string Designation { get; internal set; }
			public string University { get; internal set; }
			public string DegreeTitle { get; internal set; }
			public string Country { get; internal set; }
			public string City { get; internal set; }
			public string Mobile { get; internal set; }
			public string Phone { get; internal set; }
			public string PersonalEmail { get; internal set; }
			public string MailingAddress { get; internal set; }
			public AlumniJobPosition.JobStatuses JobStatusEnum => this.JobStatus.GetEnum<Model.Entities.AlumniJobPosition.JobStatuses>();
		}

		public static List<AlumniProfessionalInformation> GetAlumniProfessionalInformation(this AspireContext aspireContext, int studentID, Guid loginSessionGuid)
		{
			var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
			return aspireContext.AlumniJobPositions
				.Where(ajp => ajp.StudentID == studentID)
				.Where(ajp => ajp.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
				.Select(ajp => new AlumniProfessionalInformation
				{
					AlumniJobPositionID = ajp.AlumniJobPositionID,
					StudentID = ajp.StudentID,
					JobStatus = ajp.JobStatus,
					LastUpdatedOn = ajp.LastUpdatedOn,
					Organization = ajp.Organization,
					Designation = ajp.Designation,
					University = ajp.University,
					DegreeTitle = ajp.DegreeTitle,
					Country = ajp.Country,
					City = ajp.City,
					Mobile = ajp.Mobile,
					Phone = ajp.Phone,
					PersonalEmail = ajp.PersonalEmail,
					MailingAddress = ajp.MailingAddress,
				}).ToList();
		}

		public static AlumniProfessionalInformation GetAlumniProfessionalInformation(Guid alumniJobPositionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.AlumniJobPositions
					.Where(ajp => ajp.AlumniJobPositionID == alumniJobPositionID)
					.Where(ajp => ajp.InstituteID == loginHistory.InstituteID)
					.Select(ajp => new AlumniProfessionalInformation
					{
						AlumniJobPositionID = ajp.AlumniJobPositionID,
						StudentID = ajp.StudentID,
						JobStatus = ajp.JobStatus,
						LastUpdatedOn = ajp.LastUpdatedOn,
						Organization = ajp.Organization,
						Designation = ajp.Designation,
						University = ajp.University,
						DegreeTitle = ajp.DegreeTitle,
						Country = ajp.Country,
						City = ajp.City,
						Mobile = ajp.Mobile,
						Phone = ajp.Phone,
						PersonalEmail = ajp.PersonalEmail,
						MailingAddress = ajp.MailingAddress,
					}).SingleOrDefault();
			}
		}

		public static (string, string) GetAlumniPreviousDetails(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				
				var alumniPreviousDetails = aspireContext.AlumniJobPositions.Where(ajp=>ajp.StudentID == studentID && ajp.InstituteID == loginHistory.InstituteID)
					.OrderByDescending(ajp=>ajp.LastUpdatedOn)
					.Select(s => new
					{
						s.Mobile,
						s.MailingAddress,
					}).FirstOrDefault();

				if (alumniPreviousDetails != null)
					return (alumniPreviousDetails.Mobile, alumniPreviousDetails.MailingAddress);

				var alumniStudentDetails = aspireContext.Students.Where(s => s.StudentID == studentID && s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Select(s => new
					{
						s.Mobile,
						s.CurrentAddress
					}).SingleOrDefault();

				if (alumniStudentDetails == null)
					return (null, null);
				return (alumniStudentDetails.Mobile, alumniStudentDetails.CurrentAddress);
			}
		}

		public enum AddUpdateProfessionalInformationStatuses
		{
			NoRecordFound,
			Success
		}

		public static AddUpdateProfessionalInformationStatuses AddProfessionalInformation(int studentID, AlumniJobPosition.JobStatuses jobStatusEnum,
			DateTime? lastUpdatedOn, string organization, string designation, string university, string degreeTitle, string country, string city, string phone, string mobileNo,
			string personalEmail, string mailingAddress, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var student = aspireContext.Students
					.Where(s => s.StudentID == studentID)
					.Where(s => s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);

				if (student == null)
					return AddUpdateProfessionalInformationStatuses.NoRecordFound;

				var alumniJobPosition = new Aspire.Model.Entities.AlumniJobPosition
				{
					AlumniJobPositionID = Guid.NewGuid(),
					InstituteID = loginHistory.InstituteID,
					StudentID = studentID,
					JobStatusEnum = jobStatusEnum,
					LastUpdatedOn = lastUpdatedOn.Value.Date,
					Organization = organization,
					Designation = designation,
					University = university,
					DegreeTitle = degreeTitle,
					Country = country,
					City = city,
					Phone = phone,
					Mobile = mobileNo,
					PersonalEmail = personalEmail,
					MailingAddress = mailingAddress,
				};

				aspireContext.AlumniJobPositions.Add(alumniJobPosition);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				return AddUpdateProfessionalInformationStatuses.Success;
			}
		}

		public static AddUpdateProfessionalInformationStatuses UpdateProfessionalInformation(Guid alumniJobPositionID, AlumniJobPosition.JobStatuses jobStatusEnum,
			DateTime? lastUpdatedOn, string organization, string designation, string university, string degreeTitle, string country, string city, string phone, string mobileNo,
			string personalEmail, string mailingAddress, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var alumni = aspireContext.AlumniJobPositions
					.Where(ajp => ajp.AlumniJobPositionID == alumniJobPositionID)
					.Where(ajp => ajp.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID).SingleOrDefault();

				if (alumni == null)
					return AddUpdateProfessionalInformationStatuses.NoRecordFound;

				alumni.JobStatusEnum = jobStatusEnum;
				alumni.LastUpdatedOn = lastUpdatedOn.Value.Date;
				alumni.Organization = organization;
				alumni.Designation = designation;
				alumni.University = university;
				alumni.DegreeTitle = degreeTitle;
				alumni.Country = country;
				alumni.City = city;
				alumni.Phone = phone;
				alumni.Mobile = mobileNo;
				alumni.PersonalEmail = personalEmail;
				alumni.MailingAddress = mailingAddress;

				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				return AddUpdateProfessionalInformationStatuses.Success;
			}
		}
	}
}
