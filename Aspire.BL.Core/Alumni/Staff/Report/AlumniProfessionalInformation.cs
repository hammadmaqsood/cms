﻿using System;
using System.Linq;
using System.Collections.Generic;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.BL.Core.Common.Permissions;

namespace Aspire.BL.Core.Alumni.Staff
{
	public static class AlumniProfessionalInformation
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Alumni.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class ProfessionalInformation
		{
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string Department { get; internal set; }
			public int ProgramID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public string IntakeSemester => Semester.GetSemesterType(this.IntakeSemesterID).ToString();
			public short IntakeYear { get; internal set; }
			public short GraduationSemesterID { get; internal set; }
			public string GraduationSemester => Semester.GetSemesterType(this.GraduationSemesterID).ToString();
			public string GraduationYear => Semester.GetYear(GraduationSemesterID).ToString();
			public Guid AlumniJobPositionID { get; internal set; }
			public int InstituteID { get; internal set; }
			public Guid JobStatus { get; internal set; }
			public string JobStatusEnum => this.JobStatus.GetEnum<Model.Entities.AlumniJobPosition.JobStatuses>().GetFullName();
			public DateTime LastUpdatedOn { get; internal set; }
			public string LastUpdatedDate => this.LastUpdatedOn.ToString("d");
			public string Organization { get; internal set; }
			public string Designatioin { get; internal set; }
			public string University { get; internal set; }
			public string DegreeTitle { get; internal set; }
			public string Country { get; internal set; }
			public string City { get; internal set; }
			public string Mobile { get; internal set; }
			public string Phone { get; internal set; }
			public string PersonalEmail { get; internal set; }
			public string MailingAdress { get; internal set; }

			public static List<ProfessionalInformation> GetProfessionalInformation()
			{
				return null;
			}
		}

		public sealed class PageHeader
		{
			public string Institute { get; internal set; }
			public string Title { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Filters { get; internal set; }

			public static List<PageHeader> GetAlumniHeaderList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public PageHeader PageHeader { get; internal set; }
			public List<ProfessionalInformation> ProfessionalInformation { get; internal set; }
		}

		public static ReportDataSet GetProfessionalInformation(short? semesterID, int? admissionOpenProgramID, AlumniJobPosition.JobStatuses? jobStatusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var filtersList = new List<string>();

				var studentSemester = aspireContext.StudentSemesters;
				var jobPositions = aspireContext.AlumniJobPositions.AsQueryable();

				var admissionOpenPrograms = aspireContext.Programs.Where(p => p.InstituteID == loginHistory.InstituteID)
					.SelectMany(p => p.AdmissionOpenPrograms);

				if (semesterID != null)
				{
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.SemesterID == semesterID.Value);
					var semester = admissionOpenPrograms.Select(aop => aop.SemesterID).FirstOrDefault();
					filtersList.Add("Semester = " + semester.ToSemesterString());
				}

				if (admissionOpenProgramID != null)
				{
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID.Value);
					filtersList.Add("Program = " + admissionOpenPrograms.Select(aop => aop.Program.ProgramAlias).Single());
				}

				if (jobStatusEnum != null)
				{
					var jobStatus = jobStatusEnum.GetGuid();
					jobPositions = jobPositions.Where(jp => jp.JobStatus == jobStatus);
					filtersList.Add("Job Status = " + jobStatusEnum.Value.GetFullName());
				}

				var alumnies = (from ajp in jobPositions
									join aop in admissionOpenPrograms on ajp.Student.AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
									select new ProfessionalInformation
									{
										StudentID = ajp.StudentID,
										Enrollment = ajp.Student.Enrollment,
										Name = ajp.Student.Name,
										Department = ajp.Student.AdmissionOpenProgram.Program.Department.DepartmentShortName,
										IntakeSemesterID = ajp.Student.AdmissionOpenProgram.SemesterID,
										IntakeYear = ajp.Student.AdmissionOpenProgram.Semester.Year,
										GraduationSemesterID = ajp.Student.StudentSemesters.OrderByDescending(ss => ss.SemesterID).Select(ss => ss.SemesterID).FirstOrDefault(),
										ProgramID = aop.ProgramID,
										ProgramAlias = aop.Program.ProgramAlias,
										AlumniJobPositionID = ajp.AlumniJobPositionID,
										InstituteID = ajp.InstituteID,
										LastUpdatedOn = ajp.LastUpdatedOn,
										JobStatus = ajp.JobStatus,
										Organization = ajp.Organization,
										Designatioin = ajp.Designation,
										University = ajp.University,
										DegreeTitle = ajp.DegreeTitle,
										Country = ajp.Country,
										City = ajp.City,
										Mobile = ajp.Mobile,
										Phone = ajp.Phone,
										PersonalEmail = ajp.PersonalEmail,
										MailingAdress = ajp.MailingAddress
									}).OrderByDescending(c => c.LastUpdatedOn).ToList();

				var data = new List<ProfessionalInformation>();
				int? studentID = null;

				foreach (var alumni in alumnies)
				{
					if (studentID != alumni.StudentID)
						studentID = null;

					if (studentID == null)
					{
						data.Add(alumni);
						studentID = alumni.StudentID;
					}
				}

				return new ReportDataSet
				{
					PageHeader = new PageHeader
					{
						Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single(),
						Title = $"Alumni's Professional Information",
						Filters = string.Join(", ", filtersList),
					},
					ProfessionalInformation = data.OrderBy(d => d.ProgramAlias).ToList(),
				};
			}
		}


	}
}
