﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Complaints.Common
{
	public static class ComplaintComment
	{
		private static Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Complaints.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Complaints.ManageComplaints, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class Complaint
		{
			internal Complaint() { }
			public Guid ComplaintID { get; internal set; }
			public Guid ComplaintTypeID { get; internal set; }
			public int StudentID { get; internal set; }
			public int InstituteID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string ComplaintNo { get; internal set; }
			public string Description { get; internal set; }
			public DateTime RegisteredDate { get; internal set; }
			public Guid Status { get; internal set; }
			public DateTime StatusChangeDate { get; internal set; }
			public string ComplaintTypeName { get; internal set; }
			public string StatusEnumToFullName => this.Status.GetEnum<Model.Entities.Complaint.Statuses>().GetFullName();
		}

		public static List<Complaint> GetComplaints(int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualCount, Guid loginSessionGuid, string enrollment = null, string complaintNo = null, DateTime? complaintDateFrom = null, DateTime? complaintDateTo = null, Guid? status = null)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var complaints = aspireContext.Complaints.AsQueryable();
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
						complaints = complaints.Where(c => c.InstituteID == staffLoginHistory.InstituteID);
						if (!string.IsNullOrEmpty(enrollment))
							complaints = complaints.Where(c => c.Student.Enrollment == enrollment);
						if (!string.IsNullOrEmpty(complaintNo))
							complaints = complaints.Where(c => c.ComplaintNo == complaintNo);
						complaintDateFrom = complaintDateFrom?.Date;
						if (complaintDateFrom != null)
							complaints = complaints.Where(c => c.RegisteredDate >= complaintDateFrom);
						if (complaintDateTo != null)
						{
							complaintDateTo = complaintDateTo.Value.Date;
							complaintDateTo = complaintDateTo.Value.AddDays(1);
							complaints = complaints.Where(c => c.RegisteredDate < complaintDateTo);
						}
						if (status != null)
							complaints = complaints.Where(c => c.Status == status);
						break;
					case UserTypes.Student:
						complaints = complaints.Where(c => c.StudentID == loginHistory.StudentID);
						break;
					default:
						throw new NotImplementedEnumException(loginHistory.UserTypeEnum);
				}

				virtualCount = complaints.Count();
				switch (sortExpression)
				{
					case nameof(Complaint.Enrollment):
						complaints = complaints.OrderBy(sortDirection, q => q.Student.Enrollment);
						break;
					case nameof(Complaint.Name):
						complaints = complaints.OrderBy(sortDirection, q => q.Student.Name);
						break;
					case nameof(Complaint.ProgramAlias):
						complaints = complaints.OrderBy(sortDirection, q => q.Student.AdmissionOpenProgram.Program.ProgramAlias);
						break;
					case nameof(Complaint.ComplaintNo):
						complaints = complaints.OrderBy(sortDirection, q => q.ComplaintNo);
						break;
					case nameof(Complaint.ComplaintTypeName):
						complaints = complaints.OrderBy(sortDirection, q => q.ComplaintType.Name);
						break;
					case nameof(Complaint.Description):
						complaints = complaints.OrderBy(sortDirection, q => q.Description);
						break;
					case nameof(Complaint.RegisteredDate):
						complaints = complaints.OrderBy(sortDirection, q => q.RegisteredDate);
						break;
					case nameof(Complaint.Status):
						complaints = complaints.OrderBy(sortDirection, q => q.Status);
						break;
					case nameof(Complaint.StatusChangeDate):
						complaints = complaints.OrderBy(sortDirection, q => q.StatusChangeDate);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				complaints = complaints.Skip(pageIndex * pageSize).Take(pageSize);
				return complaints.Select(c => new Complaint
				{
					ComplaintID = c.ComplaintID,
					StudentID = c.StudentID,
					InstituteID = c.InstituteID,
					ComplaintTypeID = c.ComplaintTypeID,
					Enrollment = c.Student.Enrollment,
					Name = c.Student.Name,
					ProgramAlias = c.Student.AdmissionOpenProgram.Program.ProgramAlias,
					ComplaintNo = c.ComplaintNo,
					ComplaintTypeName = c.ComplaintType.Name,
					Description = c.Description,
					RegisteredDate = c.RegisteredDate,
					Status = c.Status,
					StatusChangeDate = c.StatusChangeDate,
				}).ToList();
			}
		}

		public enum ToggleComplaintStatusResults
		{
			NoRecordFound,
			Success,
		}

		public static ToggleComplaintStatusResults ToggleComplaintStatus(Guid complaintID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var complaint = aspireContext.Complaints.SingleOrDefault(c => c.ComplaintID == complaintID);
				if (complaint == null)
					return ToggleComplaintStatusResults.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, complaint.InstituteID, null, null, null);
				var statusEnum = complaint.Status.GetEnum();
				switch (statusEnum)
				{
					case Model.Entities.Complaint.Statuses.Pending:
						complaint.StatusEnum = Model.Entities.Complaint.Statuses.Resolved;
						break;
					case Model.Entities.Complaint.Statuses.Resolved:
						complaint.StatusEnum = Model.Entities.Complaint.Statuses.Pending;
						break;
					default:
						throw new NotImplementedEnumException(statusEnum);
				}
				complaint.StatusChangeDate = DateTime.Now;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return ToggleComplaintStatusResults.Success;
			}
		}

		public class ComplaintCommentResult : Aspire.Model.Entities.Complaint
		{
			public Guid ComplaintCommentID { get; internal set; }
			public string CommentedBy { get; internal set; }
			public DateTime CommentedDate { get; internal set; }
			public string Text { get; internal set; }

			public string ComplaintStatusFullName => this.Status.GetEnum<Statuses>().GetFullName();

		}
		public static List<ComplaintCommentResult> GetComplaintComments(Guid complaintID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistoryActive = aspireContext.DemandActiveUserSession(loginSessionGuid);
				int instituteID;
				switch (loginHistoryActive.UserTypeEnum)
				{

					case UserTypes.Staff:
						var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
						instituteID = loginHistory.InstituteID;
						break;
					//case UserTypes.Student:
					//	var
					//	break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				var complaintComments = aspireContext.ComplaintComments
					.Where(cc => cc.ComplaintID == complaintID && cc.Complaint.InstituteID == instituteID);
				return complaintComments.Select(cc => new ComplaintCommentResult
				{
					ComplaintID = cc.ComplaintID,
					ComplaintNo = cc.Complaint.ComplaintNo,
					Description = cc.Complaint.Description,
					RegisteredDate = cc.Complaint.RegisteredDate,
					Status = cc.Complaint.Status,
					ComplaintCommentID = cc.ComplaintCommentID,
					CommentedBy = cc.UserLoginHistory.UserName,
					Text = cc.Text,
					CommentedDate = cc.Date,
				}).ToList();
			}
		}
	}
}
