﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Complaints.Student
{
	public static class RegisterComplaints
	{
		public static List<CustomListItemGuid> GetComplaintTypes(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStudentActiveSession(loginSessionGuid, SubUserTypes.None);
				return aspireContext.Students.Where(s => s.StudentID == loginHistory.StudentID)
					.SelectMany(s => s.AdmissionOpenProgram.Program.Institute.ComplaintTypes)
					.Select(ct => new CustomListItemGuid
					{
						ID = ct.ComplaintTypeID,
						Text = ct.Name
					}).OrderBy(i => i.Text).ToList();
			}
		}

		public enum AddComplaintStatuses
		{
			NoRecordFound,
			AlreadyExists,
			Success,
		}

		public static AddComplaintStatuses AddComplaint(Guid complaintTypeID, string description, Guid loginSessionGuid)
		{
			lock (BL.Core.Common.Locks.ComplaintTransaction)
			{
				using (var aspireContext = new AspireContext(true))
				{
					var loginHistory = aspireContext.DemandStudentActiveSession(loginSessionGuid, SubUserTypes.None);
					var studentID = loginHistory.StudentID;
					var complaintType = aspireContext.Students.Where(s => s.StudentID == loginHistory.StudentID)
						.SelectMany(s => s.AdmissionOpenProgram.Program.Institute.ComplaintTypes)
						.Where(ct => ct.ComplaintTypeID == complaintTypeID)
						.Select(ct => new { ct.InstituteID })
						.SingleOrDefault();
					if (complaintType == null)
						return AddComplaintStatuses.NoRecordFound;

					var complaints = aspireContext.Complaints.Where(c => c.StudentID == studentID);
					var resolvedStatusGuid = Complaint.Statuses.Resolved.GetGuid();
					if (complaints.Any(c => c.ComplaintTypeID == complaintTypeID && c.Status != resolvedStatusGuid))
						return AddComplaintStatuses.AlreadyExists;
					var now = DateTime.Now;
					var complaintNo = aspireContext.GetComplaintNo(complaintType.InstituteID, now);
					var complaint = new Complaint
					{
						ComplaintID = Guid.NewGuid(),
						ComplaintTypeID = complaintTypeID,
						StudentID = studentID,
						InstituteID = complaintType.InstituteID,
						ComplaintNo = complaintNo,
						Description = description,
						RegisteredDate = now,
						StatusEnum = Complaint.Statuses.Pending,
						StatusChangeDate = now
					};
					aspireContext.Complaints.Add(complaint);
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
					return AddComplaintStatuses.Success;
				}
			}
		}

		private static string GetComplaintNo(this AspireContext aspireContext, int instituteID, DateTime now)
		{
			var nowDate = now.Date;
			var nextDate = now.Date.AddDays(1);
			var count = aspireContext.Complaints
				.Where(c => c.InstituteID == instituteID)
				.Count(c => nowDate <= c.RegisteredDate && c.RegisteredDate < nextDate);
			var countFormat= (++count).ToString("000");
			return $"SSC-{nowDate:yyyy}-{nowDate:MM}{nowDate:dd}-{countFormat}";
		}
	}
}
