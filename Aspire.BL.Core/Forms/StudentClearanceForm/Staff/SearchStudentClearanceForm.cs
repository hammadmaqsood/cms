﻿using System;
using System.Linq;
using System.Collections.Generic;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.BL.Core.Common.Permissions;

namespace Aspire.BL.Core.Forms.StudentClearanceForm.Staff
{
	public static class SearchStudentClearanceForm
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandStaffPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceForm, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class SearchClearanceForm
		{
			internal SearchClearanceForm() { }
			public Guid StudentClearanceFormID { get; internal set; }
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public DateTime? FormSubmissionDate { get; internal set; }
			public Guid FormStudentClearanceStatus { get; internal set; }
			public Model.Entities.FormStudentClearance.StudentClearanceStatuses FormStudentClearanceStatusEnum => this.FormStudentClearanceStatus.GetEnum<Model.Entities.FormStudentClearance.StudentClearanceStatuses>();
			public Guid FormStudentClearanceSSCStatus { get; internal set; }
			public Model.Entities.FormStudentClearance.StudentClearanceSSCStatuses FormStudentClearanceSSCStatusEnum => this.FormStudentClearanceSSCStatus.GetEnum<Model.Entities.FormStudentClearance.StudentClearanceSSCStatuses>();
		}

		public static List<SearchClearanceForm> StudentClearanceData(string searchText, FormStudentClearance.StudentClearanceSSCStatuses? sscStatusEnum,
			FormStudentClearance.StudentClearanceStatuses? formStatusEnum, FormStudentClearanceDepartmentStatus.Departments? departmentsEnum,
			FormStudentClearanceDepartmentStatus.DepartmentStatuses? departmentStatusEnum, int pageIndex, int pageSize, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid);

				var query = aspireContext.FormStudentClearances.AsQueryable();

				var formStudentClearance = Model.Entities.FormStudentClearance.StudentClearanceStatuses.NotInitiated.GetGuid();
				query = query.Where(q => q.Form.InstituteID == loginHistory.InstituteID)
					.Where(fsc => fsc.StudentClearanceStatus != formStudentClearance)
					.Where(fsc => searchText == null || fsc.Student.Enrollment.Contains(searchText) || fsc.Student.Name.Contains(searchText));

				if (sscStatusEnum != null)
				{
					var sscStatus = sscStatusEnum.GetGuid();
					query = query.Where(q => q.StudentClearanceSSCStatus == sscStatus);
				}
				if (formStatusEnum != null)
				{
					var formStatus = formStatusEnum.GetGuid();
					query = query.Where(q => q.StudentClearanceStatus == formStatus);
				}

				if (departmentsEnum != null)
				{
					var departments = departmentsEnum.GetGuid();
					query = query.Where(q => q.FormStudentClearanceDepartmentStatuses.Any(fscds => fscds.Department == departments));
				}
				if (departmentStatusEnum != null)
				{
					var departmentStatus = departmentStatusEnum.GetGuid();
					query = query.Where(q => q.FormStudentClearanceDepartmentStatuses.Any(fscds => fscds.DepartmentStatus == departmentStatus));
				}

				var result = query.Select(fsc => new SearchClearanceForm
				{
					StudentClearanceFormID = fsc.FormStudentClearanceID,
					StudentID = fsc.StudentID,
					Enrollment = fsc.Student.Enrollment,
					Name = fsc.Student.Name,
					FormSubmissionDate = fsc.Form.SubmissionDate,
					FormStudentClearanceStatus = fsc.StudentClearanceStatus,
					FormStudentClearanceSSCStatus = fsc.StudentClearanceSSCStatus,
				}).OrderByDescending(fsc => fsc.FormSubmissionDate).ToList();
				virtualItemCount = result.Count();
				return result.Skip(pageIndex * pageSize).Take(pageSize).ToList();
			}
		}
	}
}
