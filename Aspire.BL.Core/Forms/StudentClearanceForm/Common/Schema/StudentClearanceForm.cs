﻿using System;
using Aspire.Lib.Extensions;

namespace Aspire.BL.Core.Forms.StudentClearanceForm.Common.Schema
{
	[Serializable]
	public sealed class StudentClearanceForm
	{
		public string FullName { get; set; }
		public string CNIC { get; set; }
		public string Relationship { get; set; }
		public string StudentMobile { get; set; }
		public string Reason { get; set; }

		public string ToJson()
		{
			return this.ToJsonString();
		}

		public static StudentClearanceForm FromJson(string json)
		{
			return json.FromJsonString<StudentClearanceForm>();
		}

		public void UpdateRequiredInformation(string fullName, string cnic, string relationship, string studentMobile, string reason)
		{
			this.FullName = fullName;
			this.CNIC = cnic;
			this.Relationship = relationship;
			this.StudentMobile = studentMobile;
			this.Reason = reason;
		}
	}
}
