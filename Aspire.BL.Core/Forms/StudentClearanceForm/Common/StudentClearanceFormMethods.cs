﻿using System;
using System.Linq;
using System.Collections.Generic;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Helpers;
using Aspire.BL.Core.Common.EmailsManagement;

namespace Aspire.BL.Core.Forms.StudentClearanceForm.Common
{
	public static class StudentClearanceFormMethods
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandStaffPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceForm, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class AddClearanceFormResult
		{
			internal AddClearanceFormResult() { }

			public enum AddClearanceFormResultStatuses
			{
				NoRecordFound,
				AlreadyExist,
				Success,
			}

			public AddClearanceFormResultStatuses Status { get; internal set; }
		}

		public static AddClearanceFormResult AddClearanceForm(int studentID, int instituteID, string fullName, string cnic, string relationship, string mobile, string reason, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var studentInfo = aspireContext.Students
					.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgramID
					}).SingleOrDefault();

				if (studentInfo == null)
					return new AddClearanceFormResult { Status = AddClearanceFormResult.AddClearanceFormResultStatuses.NoRecordFound };

				var alreadyExist = aspireContext.FormStudentClearances.Where(ftd => ftd.StudentID == studentID)
					.Where(ftd => ftd.Form.InstituteID == instituteID)
					.Select(ftd => ftd.Data).SingleOrDefault();

				if (alreadyExist != null)
					return new AddClearanceFormResult { Status = AddClearanceFormResult.AddClearanceFormResultStatuses.AlreadyExist };

				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandStaffPermissions(loginSessionGuid);
						aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceFormCanInitiateOrEdit, UserGroupPermission.PermissionValues.Allowed);
						break;
					case UserTypes.Student:
						if (studentID != loginHistory.StudentID)
							throw new InvalidOperationException();
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(loginHistory.UserTypeEnum), loginHistory.UserTypeEnum, null);
				}

				var form = aspireContext.Forms.Add(new Model.Entities.Form
				{
					FormID = Guid.NewGuid(),
					StudentID = studentID,
					InstituteID = instituteID,
					FormTypeEnum = Form.FormTypes.StudentClearanceForm,
					SubmissionDate = null,
					FormStatusEnum = Form.FormStatuses.NotInitiated
				});
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);

				var data = new Common.Schema.StudentClearanceForm();
				var studentClearanceForm = aspireContext.FormStudentClearances.Add(new Model.Entities.FormStudentClearance
				{
					FormStudentClearanceID = Guid.NewGuid(),
					FormID = form.FormID,
					StudentID = studentID,
					Data = data.ToJson(),
					StudentClearanceStatusEnum = FormStudentClearance.StudentClearanceStatuses.NotInitiated,
					StudentClearanceStatusChangedDate = DateTime.Now,
					StudentClearanceSSCStatusEnum = FormStudentClearance.StudentClearanceSSCStatuses.Pending,
					StudentClearanceSSCStatusChangedDate = null,
					PersonalEmailConfirmationCode = Guid.NewGuid().ToString("N").Substring(0, 4),
					UniversityEmailConfirmationCode = Guid.NewGuid().ToString("N").Substring(0, 4)
				});
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);

				var result = AddOrEditRequiredInformation(studentClearanceForm.FormStudentClearanceID, fullName, cnic, relationship, mobile, reason, loginSessionGuid);

				if (result == AddOrEditRequiredInformationStatuses.NoRecordFound)
					return new AddClearanceFormResult { Status = AddClearanceFormResult.AddClearanceFormResultStatuses.NoRecordFound };

				return new AddClearanceFormResult { Status = AddClearanceFormResult.AddClearanceFormResultStatuses.Success };
			}
		}

		public enum AddOrEditRequiredInformationStatuses
		{
			NoRecordFound,
			alreadyExists,
			Success
		}

		public static AddOrEditRequiredInformationStatuses AddOrEditRequiredInformation(Guid formStudentClearanceID, string fullName, string cnic, string relationship, string mobile, string reason, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var result = aspireContext.FormStudentClearances.Where(ftd => ftd.FormStudentClearanceID == formStudentClearanceID).SingleOrDefault();

				if (result == null)
					return AddOrEditRequiredInformationStatuses.NoRecordFound;

				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceFormCanInitiateOrEdit, UserGroupPermission.PermissionValues.Allowed);
						break;
					case UserTypes.Student:
						break;
					default:
						throw new NotImplementedEnumException(loginHistory.UserTypeEnum);
				}

				var requiredInfo = Common.Schema.StudentClearanceForm.FromJson(result.Data);
				requiredInfo.UpdateRequiredInformation(fullName, cnic, relationship, mobile, reason);
				result.Data = requiredInfo.ToJson();

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddOrEditRequiredInformationStatuses.Success;
			}
		}

		public enum SubmitFormStatuses
		{
			NoRecordFound,
			alreadySubmitted,
			PasswordNotCorrect,
			UniversityEmailCodeNotCorrect,
			PersonalEmailCodeNotCorrect,
			Success
		}

		public static SubmitFormStatuses SubmitForm(Guid formStudentClearanceID, string password, string universityEmailCode, string personalEmailCode, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var clearanceForm = aspireContext.FormStudentClearances.Single(ftd => ftd.FormStudentClearanceID == formStudentClearanceID);

				if (clearanceForm == null)
					return SubmitFormStatuses.NoRecordFound;

				if (clearanceForm.StudentClearanceStatus != FormStudentClearance.StudentClearanceStatuses.NotInitiated.GetGuid())
					return SubmitFormStatuses.alreadySubmitted;

				var passwordCheck = clearanceForm.Student.Password;
				password = password.CannotBeEmptyOrWhitespace();
				if (password == null || !password.CheckPassword(clearanceForm.Student.Password))
					return SubmitFormStatuses.PasswordNotCorrect;

				if (universityEmailCode != clearanceForm.UniversityEmailConfirmationCode)
					return SubmitFormStatuses.UniversityEmailCodeNotCorrect;

				if (personalEmailCode != clearanceForm.PersonalEmailConfirmationCode)
					return SubmitFormStatuses.PersonalEmailCodeNotCorrect;

				clearanceForm.Form.SubmissionDate = DateTime.Now;
				clearanceForm.Form.FormStatusEnum = Form.FormStatuses.Initiated;
				clearanceForm.StudentClearanceStatusEnum = FormStudentClearance.StudentClearanceStatuses.Initiated;
				clearanceForm.StudentClearanceStatusChangedDate = DateTime.Now;
				clearanceForm.StudentClearanceSSCStatusEnum = FormStudentClearance.StudentClearanceSSCStatuses.Pending;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return SubmitFormStatuses.Success;
			}
		}

		public sealed class Emails
		{
			public enum EmailStatuses
			{
				UniversityEmailMissing,
				PersonalEmailMissing,
				Success
			}
			public EmailStatuses EmailStatus { get; set; }
			public string UniversityEmail { get; set; }
			public string PersoanlEmail { get; set; }
		}

		public static Emails SendEmailConfirmationCodeToStudent(Guid formStudentClearanceID, bool resendEmail, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var clearanceForm = aspireContext.FormStudentClearances.Where(fsc => fsc.FormStudentClearanceID == formStudentClearanceID).SingleOrDefault();

				if (string.IsNullOrWhiteSpace(clearanceForm.Student.UniversityEmail))
					return new Emails { EmailStatus = Emails.EmailStatuses.UniversityEmailMissing };

				if (string.IsNullOrWhiteSpace(clearanceForm.Student.PersonalEmail))
					return new Emails { EmailStatus = Emails.EmailStatuses.PersonalEmailMissing };

				if (resendEmail)
				{
					clearanceForm.UniversityEmailConfirmationCode = Guid.NewGuid().ToString("N").Substring(0, 4);
					clearanceForm.PersonalEmailConfirmationCode = Guid.NewGuid().ToString("N").Substring(0, 4);
					aspireContext.SaveChanges();
				}

				var now = DateTime.Now;
				var subject = "Confirmation Code for Student Clearance Form";
				var emailBody = Properties.Resources.StudentClearanceFormConfirmationCodes;
				emailBody = emailBody.Replace("{EnrollmentHtmlEncoded}", clearanceForm.Student.Enrollment.HtmlEncode());
				emailBody = emailBody.Replace("{NameHtmlEncoded}", clearanceForm.Student.Name.HtmlEncode());
				emailBody = emailBody.Replace("{ConfirmationCodeHtmlEncoded}", clearanceForm.UniversityEmailConfirmationCode);
				var toList = new[] { (clearanceForm.Student.Name, clearanceForm.Student.UniversityEmail) };
				aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.StudentClearanceFormConfirmationCode, subject, emailBody, true, Email.Priorities.Normal, now, now.AddHours(1), loginHistory.UserLoginHistoryID, toList: toList);

				emailBody = Properties.Resources.StudentClearanceFormConfirmationCodes;
				emailBody = emailBody.Replace("{EnrollmentHtmlEncoded}", clearanceForm.Student.Enrollment.HtmlEncode());
				emailBody = emailBody.Replace("{NameHtmlEncoded}", clearanceForm.Student.Name.HtmlEncode());
				emailBody = emailBody.Replace("{ConfirmationCodeHtmlEncoded}", clearanceForm.PersonalEmailConfirmationCode);
				toList = new[] { (clearanceForm.Student.Name, clearanceForm.Student.PersonalEmail) };
				aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.StudentClearanceFormConfirmationCode, subject, emailBody, true, Email.Priorities.Normal, now, now.AddHours(1), loginHistory.UserLoginHistoryID, toList: toList);

				aspireContext.CommitTransaction();
				return new Emails
				{
					UniversityEmail = clearanceForm.Student.UniversityEmail,
					PersoanlEmail = clearanceForm.Student.PersonalEmail,
					EmailStatus = Emails.EmailStatuses.Success
				};
			}
		}

		public enum SubmitSSCStatuses
		{
			NoRecordFound,
			Pending,
			NotAllowedToChangeWhenVerified,
			NotVerified,
			Success
		}

		public static SubmitSSCStatuses SubmitSSCStatus(Guid formStudentClearanceID, FormStudentClearance.StudentClearanceSSCStatuses formStudentClearanceSSCStatusesEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid);
				aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceFormCanChangeSSCStatus, UserGroupPermission.PermissionValues.Allowed);

				var clearanceForm = aspireContext.FormStudentClearances.Single(ftd => ftd.FormStudentClearanceID == formStudentClearanceID);

				if (clearanceForm == null)
					return SubmitSSCStatuses.NoRecordFound;

				if (clearanceForm.StudentClearanceSSCStatusEnum == FormStudentClearance.StudentClearanceSSCStatuses.Verified)
					return SubmitSSCStatuses.NotAllowedToChangeWhenVerified;

				var now = DateTime.Now;
				clearanceForm.StudentClearanceStatusChangedDate = now;
				clearanceForm.StudentClearanceSSCStatusChangedDate = now;
				clearanceForm.StudentClearanceSSCStatusEnum = formStudentClearanceSSCStatusesEnum;
				clearanceForm.StudentClearanceSSCStatusChangedByUserLoginHistoryID = loginHistory.UserLoginHistoryID;

				switch (formStudentClearanceSSCStatusesEnum)
				{
					case FormStudentClearance.StudentClearanceSSCStatuses.Pending:
						clearanceForm.Form.FormStatusEnum = Form.FormStatuses.Initiated;
						clearanceForm.StudentClearanceStatusEnum = FormStudentClearance.StudentClearanceStatuses.Initiated;
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return SubmitSSCStatuses.Pending;
					case FormStudentClearance.StudentClearanceSSCStatuses.NotVerified:
						clearanceForm.Form.FormStatusEnum = Form.FormStatuses.NotVerifiedBySSC;
						clearanceForm.StudentClearanceStatusEnum = FormStudentClearance.StudentClearanceStatuses.NotVerifiedBySSC;
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return SubmitSSCStatuses.NotVerified;
					case FormStudentClearance.StudentClearanceSSCStatuses.Verified:
						clearanceForm.Form.FormStatusEnum = Form.FormStatuses.InProcess;
						clearanceForm.StudentClearanceStatusEnum = FormStudentClearance.StudentClearanceStatuses.InProcess;
						break;
					default:
						throw new NotImplementedEnumException(formStudentClearanceSSCStatusesEnum);
				}

				var clearanceFormDepartments = clearanceForm.FormStudentClearanceDepartmentStatuses.Any(fscds => fscds.FormStudentClearanceID == formStudentClearanceID);
				if (clearanceFormDepartments)
					return SubmitSSCStatuses.Success;

				var departments = GetDepartmentsEnum();

				var female = clearanceForm.Student.Gender == (byte)Model.Entities.Common.Genders.Female;
				if (!female)
					departments.RemoveAll(d => d.Text == FormStudentClearanceDepartmentStatus.Departments.HostelWarden.ToString());

				foreach (var department in departments)
				{
					aspireContext.FormStudentClearanceDepartmentStatuses.Add(new Model.Entities.FormStudentClearanceDepartmentStatus
					{
						FormStudentClearanceDepartmentStatusID = Guid.NewGuid(),
						FormStudentClearanceID = clearanceForm.FormStudentClearanceID,
						UserLoginHistoryID = null,
						Department = department.Value,
						DepartmentStatusEnum = FormStudentClearanceDepartmentStatus.DepartmentStatuses.Pending,
						Remarks = null,
						DepartmentStatusChangedDate = DateTime.Now,
					});
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				}
				aspireContext.CommitTransaction();
				return SubmitSSCStatuses.Success;
			}
		}

		public class Departments
		{
			public string Text { get; set; }
			public Guid Value { get; set; }
		}

		public static List<Departments> GetDepartmentsEnum()
		{
			return Enum.GetValues(typeof(Aspire.Model.Entities.FormStudentClearanceDepartmentStatus.Departments))
						.Cast<Aspire.Model.Entities.FormStudentClearanceDepartmentStatus.Departments>()
						.Select(d => new Departments
						{
							Text = d.GetFullName(),
							Value = d.GetGuid(),
						}).ToList();
		}

		public enum UpdateStatuses
		{
			NoRecordFound,
			NotAuthorized,
			RemarksIfNotApproved,
			NotAllStatusesAreApproved,
			Success,
		}

		public static UpdateStatuses UpdateStatus(Guid formStudentClearanceDepartmentStatusID, Model.Entities.FormStudentClearanceDepartmentStatus.Departments departmentEnum, Model.Entities.FormStudentClearanceDepartmentStatus.DepartmentStatuses departmentStatusEnum, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid);
				var formStudentClearanceStatus = Model.Entities.FormStudentClearance.StudentClearanceStatuses.NotInitiated.GetGuid();
				var rows = aspireContext.FormStudentClearanceDepartmentStatuses.Where(fscds => fscds.FormStudentClearance.StudentClearanceStatus != formStudentClearanceStatus);
				var currentStatus = rows.Where(r => r.FormStudentClearanceDepartmentStatusID == formStudentClearanceDepartmentStatusID).SingleOrDefault();

				if (currentStatus == null)
					return UpdateStatuses.NoRecordFound;

				var formTypeDetailID = currentStatus.FormStudentClearanceID;
				var allStatuses = rows.Where(r => r.FormStudentClearanceID == formTypeDetailID).ToList();
				var permissions = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceForm, UserGroupPermission.PermissionValues.Allowed);
				switch (departmentEnum)
				{
					case FormStudentClearanceDepartmentStatus.Departments.MainLibrary:
						permissions = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceFormMainLibrarySignature, UserGroupPermission.PermissionValues.Allowed);
						break;
					case FormStudentClearanceDepartmentStatus.Departments.LawLibrary:
						permissions = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceFormLawLibrarySignature, UserGroupPermission.PermissionValues.Allowed);
						break;
					case FormStudentClearanceDepartmentStatus.Departments.LabsOrServerRoom:
						permissions = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceFormLabsServerRoomSignature, UserGroupPermission.PermissionValues.Allowed);
						break;
					case FormStudentClearanceDepartmentStatus.Departments.LaptopSection:
						permissions = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceFormLaptopSectionSignature, UserGroupPermission.PermissionValues.Allowed);
						break;
					case FormStudentClearanceDepartmentStatus.Departments.StudentAffairs:
						permissions = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceFormStudentAffairsSignature, UserGroupPermission.PermissionValues.Allowed);
						break;
					case FormStudentClearanceDepartmentStatus.Departments.HostelWarden:
						permissions = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceFormHostelWardenSignature, UserGroupPermission.PermissionValues.Allowed);
						break;
					case FormStudentClearanceDepartmentStatus.Departments.ExaminationCell:
						permissions = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceFormExamCellSignature, UserGroupPermission.PermissionValues.Allowed);
						break;
					case FormStudentClearanceDepartmentStatus.Departments.AdmissionOfficeFor1stSemesterOnly:
						permissions = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceFormAdmissionOfficeSignature, UserGroupPermission.PermissionValues.Allowed);
						break;
					case FormStudentClearanceDepartmentStatus.Departments.SecurityAndSports:
						permissions = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceFormSecurityAndSportsSignature, UserGroupPermission.PermissionValues.Allowed);
						break;
					case FormStudentClearanceDepartmentStatus.Departments.StudentAdvisor:
						permissions = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceFormStudentAdvisorSignature, UserGroupPermission.PermissionValues.Allowed);
						break;
					case FormStudentClearanceDepartmentStatus.Departments.AccountsSection:
						permissions = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceFormAccountsSectionSignature, UserGroupPermission.PermissionValues.Allowed);
						if (permissions == null)
							return UpdateStatuses.NotAuthorized;
						var department = FormStudentClearanceDepartmentStatus.Departments.AccountsSection.GetGuid();
						var pendingOrNotApproved = allStatuses.Where(s => s.Department != department && s.DepartmentStatus != FormStudentClearanceDepartmentStatus.DepartmentStatuses.Approved.GetGuid()).ToList();

						if (pendingOrNotApproved.Any())
							return UpdateStatuses.NotAllStatusesAreApproved;
						break;
					default:
						throw new NotImplementedEnumException(departmentEnum);
				}

				if (permissions == null)
					return UpdateStatuses.NotAuthorized;

				if (departmentStatusEnum == FormStudentClearanceDepartmentStatus.DepartmentStatuses.NotApproved && remarks.ToNullIfWhiteSpaceOrNA() == null)
					return UpdateStatuses.RemarksIfNotApproved;

				currentStatus.DepartmentStatusEnum = departmentStatusEnum;
				currentStatus.Remarks = remarks;
				currentStatus.DepartmentStatusChangedDate = DateTime.Now;
				currentStatus.UserLoginHistoryID = loginHistory.UserLoginHistoryID;

				if (currentStatus.FormStudentClearance.StudentClearanceStatusEnum == Model.Entities.FormStudentClearance.StudentClearanceStatuses.InProcess &&
					!allStatuses.Any(s => s.DepartmentStatusEnum == FormStudentClearanceDepartmentStatus.DepartmentStatuses.Pending ||
									  s.DepartmentStatusEnum == FormStudentClearanceDepartmentStatus.DepartmentStatuses.NotApproved))
				{
					currentStatus.FormStudentClearance.StudentClearanceStatusEnum = FormStudentClearance.StudentClearanceStatuses.Completed;
					currentStatus.FormStudentClearance.Form.FormStatusEnum = Form.FormStatuses.Completed;
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateStatuses.Success;
			}
		}
	}
}
