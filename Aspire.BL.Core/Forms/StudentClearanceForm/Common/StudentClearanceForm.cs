﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Forms.StudentClearanceForm.Common
{
	public static class StudentClearanceForm
	{
		private static Aspire.BL.Core.Common.Permissions.Staff.StaffGroupPermission DemandStaffPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Forms.StudentClearanceForm, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class StudentInformation
		{
			internal StudentInformation() { }
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public int InstituteID { get; internal set; }
			public string IntakeSemester => this.IntakeSemesterID.ToSemesterString();

			public StudentClearanceFormData ClearanceFormData { get; internal set; }
		}

		public static StudentInformation GetStudentFormData(int? studentID, string enrollment, Guid loginSessionGuid)
		{
			if (studentID == null && string.IsNullOrWhiteSpace(enrollment))
				throw new ArgumentNullException(nameof(studentID));
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var student = aspireContext.Students.AsQueryable();

				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Student:
						student = aspireContext.Students.Where(s => s.StudentID == loginHistory.StudentID.Value);
						break;
					case UserTypes.Staff:
						var login = aspireContext.DemandStaffPermissions(loginSessionGuid);
						student = aspireContext.Students.Where(s => s.AdmissionOpenProgram.Program.InstituteID == login.InstituteID);
						break;
					default:
						throw new NotImplementedEnumException(loginHistory.UserTypeEnum);
				}

				if (enrollment != null)
					student = aspireContext.Students.Where(s => s.Enrollment == enrollment);
				if (studentID != null)
					student = aspireContext.Students.Where(s => s.StudentID == studentID.Value);

				var formType = Form.FormTypes.StudentClearanceForm.GetGuid();
				return student.Select(s => new StudentInformation
				{
					StudentID = s.StudentID,
					Enrollment = s.Enrollment,
					Name = s.Name,
					ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
					IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
					InstituteID = s.AdmissionOpenProgram.Program.InstituteID,
					ClearanceFormData = s.FormStudentClearances.Where(fsc => fsc.Form.FormType == formType)
						.Select(fsc => new StudentClearanceFormData
						{
							FormStudentClearanceID = fsc.FormStudentClearanceID,
							FormID = fsc.FormID,
							StudentID = fsc.StudentID,
							RequiredInfo = fsc.Data,
							StudentClearanceStatusChangedDate = fsc.StudentClearanceStatusChangedDate,
							StudentClearanceStatus = fsc.StudentClearanceStatus,
							StudentClearanceSSCStatus = fsc.StudentClearanceSSCStatus,
							StudentClearanceSSCStatusChangedDate = fsc.StudentClearanceSSCStatusChangedDate,
							FormSubmissionDate = fsc.Form.SubmissionDate,
							ClearanceFormDepartmentStatuses = fsc.FormStudentClearanceDepartmentStatuses.Select(fscds => new ClearanceFormDepartmentStatuses
							{
								FormStudentClearanceDepartmentStatusID = fscds.FormStudentClearanceDepartmentStatusID,
								FormStudentClearanceID = fscds.FormStudentClearanceID,
								Departments = fscds.Department,
								Name = fscds.UserLoginHistory.User.Name,
								Designation = fscds.UserLoginHistory.User.JobTitle,
								DepartmentStatus = fscds.DepartmentStatus,
								Remarks = fscds.Remarks,
								DepartmentStatusChangedDate = fscds.DepartmentStatusChangedDate
							}).ToList()
						}).FirstOrDefault(),
				}).SingleOrDefault();
			}
		}

		public sealed class StudentClearanceFormData
		{
			internal StudentClearanceFormData() { }
			public Guid FormStudentClearanceID { get; internal set; }
			public Guid FormID { get; internal set; }
			public int StudentID { get; internal set; }
			public string RequiredInfo { get; internal set; }

			private Aspire.BL.Core.Forms.StudentClearanceForm.Common.Schema.StudentClearanceForm _studentClearanceForm;
			public Aspire.BL.Core.Forms.StudentClearanceForm.Common.Schema.StudentClearanceForm StudentClearanceForm
			{
				get
				{
					if (this._studentClearanceForm == null)
						this._studentClearanceForm = Aspire.BL.Core.Forms.StudentClearanceForm.Common.Schema.StudentClearanceForm.FromJson(this.RequiredInfo);
					return this._studentClearanceForm;
				}
			}

			public Guid StudentClearanceStatus { get; internal set; }
			public Model.Entities.FormStudentClearance.StudentClearanceStatuses StudentClearanceStatusEnum => this.StudentClearanceStatus.GetEnum<FormStudentClearance.StudentClearanceStatuses>();
			public DateTime? StudentClearanceStatusChangedDate { get; internal set; }
			public Guid StudentClearanceSSCStatus { get; internal set; }
			public FormStudentClearance.StudentClearanceSSCStatuses StudentClearanceSSCStatusEnum => this.StudentClearanceSSCStatus.GetEnum<FormStudentClearance.StudentClearanceSSCStatuses>();
			public DateTime? StudentClearanceSSCStatusChangedDate { get; internal set; }
			public DateTime? FormSubmissionDate { get; internal set; }
			public List<ClearanceFormDepartmentStatuses> ClearanceFormDepartmentStatuses { get; internal set; }
		}

		public class ClearanceFormDepartmentStatuses
		{
			internal ClearanceFormDepartmentStatuses() { }
			public Guid FormStudentClearanceDepartmentStatusID { get; internal set; }
			public Guid FormStudentClearanceID { get; internal set; }
			public Guid Departments { get; internal set; }
			public FormStudentClearanceDepartmentStatus.Departments DepartmentEnum => this.Departments.GetEnum<FormStudentClearanceDepartmentStatus.Departments>();
			public string Name { get; internal set; }
			public string Designation { get; internal set; }
			public Guid DepartmentStatus { get; internal set; }
			public FormStudentClearanceDepartmentStatus.DepartmentStatuses DepartmentStatusEnum => this.DepartmentStatus.GetEnum<FormStudentClearanceDepartmentStatus.DepartmentStatuses>();
			public string Remarks { get; internal set; }
			public DateTime? DepartmentStatusChangedDate { get; internal set; }
		}

		public static ClearanceFormDepartmentStatuses DepartmentStatus(Guid formStudentClearanceDepartmentStatusID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid);

				return aspireContext.FormStudentClearanceDepartmentStatuses.Where(fscds => fscds.FormStudentClearanceDepartmentStatusID == formStudentClearanceDepartmentStatusID)
					.Select(fscds => new ClearanceFormDepartmentStatuses
					{
						FormStudentClearanceDepartmentStatusID = fscds.FormStudentClearanceDepartmentStatusID,
						Departments = fscds.Department,
						DepartmentStatus = fscds.DepartmentStatus,
						Remarks = fscds.Remarks,
						DepartmentStatusChangedDate = fscds.DepartmentStatusChangedDate
					}).SingleOrDefault();
			}
		}
	}
}
