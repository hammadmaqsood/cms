﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.LibraryManagement.Staff
{
	public static class StudentLibraryDefaulter
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.LibraryManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.LibraryManagement.ManageStudentLibraryDefaulter, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class StudentLibraryDefaulterRecord
		{
			internal StudentLibraryDefaulterRecord()
			{

			}
			public int StudentLibraryDefaulterID { get; internal set; }
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string StudentName { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string Remarks { get; internal set; }
			public byte Status { get; internal set; }
			public System.DateTime LastUpdatedDate { get; internal set; }
			public int LastUpdatedByUserLoginHistoryID { get; internal set; }
			public string UpdatedByUser { get; internal set; }
			public string StatusEnumFullName => ((Aspire.Model.Entities.StudentLibraryDefaulter.Statuses)this.Status).ToString();
		}

		public enum AddStudentLibraryDefaulterStatuses
		{
			NoRecordFound,
			Added,
			ExistingUpdated,
			AlreadyExists
		}

		public static Dictionary<string, AddStudentLibraryDefaulterStatuses> AddStudentLibraryDefaulter(HashSet<string> enrollments, string remarks, Model.Entities.StudentLibraryDefaulter.Statuses statusEnum, bool updateExitingLibraryDefaulters, Guid loginSessionGuid)
		{
			var result = new Dictionary<string, AddStudentLibraryDefaulterStatuses>();
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistoryActive = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				remarks = remarks.ToNullIfWhiteSpace();
				foreach (var enrollment in enrollments)
				{
					var student = aspireContext.Students
						.Where(s => s.Enrollment == enrollment && s.AdmissionOpenProgram.Program.InstituteID == loginHistoryActive.InstituteID)
						.Select(s => new
						{
							s.StudentID,
							s.AdmissionOpenProgram.Program.InstituteID,
							s.AdmissionOpenProgram.Program.DepartmentID,
							s.AdmissionOpenProgram.ProgramID,
							s.AdmissionOpenProgramID,
							StudentLibraryDefaulter = s.StudentLibraryDefaulters.FirstOrDefault()
						}).SingleOrDefault();
					if (student == null)
					{
						result.Add(enrollment, AddStudentLibraryDefaulterStatuses.NoRecordFound);
						continue;
					}
					var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, loginHistoryActive.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
					if (student.StudentLibraryDefaulter == null)
					{
						aspireContext.StudentLibraryDefaulters.Add(new Model.Entities.StudentLibraryDefaulter
						{
							StudentID = student.StudentID,
							LastUpdatedByUserLoginHistoryID = loginHistoryActive.UserLoginHistoryID,
							LastUpdatedDate = DateTime.Now,
							Remarks = remarks,
							StatusEnum = statusEnum,
						});
						result.Add(enrollment, AddStudentLibraryDefaulterStatuses.Added);
					}
					else
					{
						if (updateExitingLibraryDefaulters)
						{
							student.StudentLibraryDefaulter.LastUpdatedByUserLoginHistoryID = loginHistoryActive.UserLoginHistoryID;
							student.StudentLibraryDefaulter.LastUpdatedDate = DateTime.Now;
							student.StudentLibraryDefaulter.Remarks = remarks;
							student.StudentLibraryDefaulter.StatusEnum = statusEnum;
							result.Add(enrollment, AddStudentLibraryDefaulterStatuses.ExistingUpdated);
						}
						else
							result.Add(enrollment, AddStudentLibraryDefaulterStatuses.AlreadyExists);
					}
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistoryActive.UserLoginHistoryID);
				return result;
			}
		}

		public static List<StudentLibraryDefaulterRecord> GetStudentLibraryDefaulters(string search, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var defaulters = aspireContext.StudentLibraryDefaulters.Where(s => s.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				if (!string.IsNullOrWhiteSpace(search))
					defaulters = defaulters.Where(s => s.Student.Enrollment.Contains(search) || s.Remarks.Contains(search));

				virtualItemCount = defaulters.Count();
				switch (sortExpression)
				{
					case nameof(StudentLibraryDefaulterRecord.Enrollment):
						defaulters = defaulters.OrderBy(sortDirection, d => d.Student.Enrollment);
						break;
					case nameof(StudentLibraryDefaulterRecord.StudentName):
						defaulters = defaulters.OrderBy(sortDirection, d => d.Student.Name);
						break;
					case nameof(StudentLibraryDefaulterRecord.ProgramAlias):
						defaulters = defaulters.OrderBy(sortDirection, d => d.Student.AdmissionOpenProgram.Program.ProgramAlias);
						break;
					case nameof(StudentLibraryDefaulterRecord.Remarks):
						defaulters = defaulters.OrderBy(sortDirection, d => d.Remarks);
						break;
					case nameof(StudentLibraryDefaulterRecord.Status):
					case nameof(StudentLibraryDefaulterRecord.StatusEnumFullName):
						defaulters = defaulters.OrderBy(sortDirection, d => d.Status);
						break;
					case nameof(StudentLibraryDefaulterRecord.LastUpdatedDate):
						defaulters = defaulters.OrderBy(sortDirection, d => d.LastUpdatedDate);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}

				return defaulters.Skip(pageIndex * pageSize).Take(pageSize).Select(sld => new StudentLibraryDefaulterRecord
				{
					StudentLibraryDefaulterID = sld.StudentLibraryDefaulterID,
					StudentID = sld.StudentID,
					Enrollment = sld.Student.Enrollment,
					StudentName = sld.Student.Name,
					ProgramAlias = sld.Student.AdmissionOpenProgram.Program.ProgramAlias,
					Remarks = sld.Remarks,
					Status = sld.Status,
					LastUpdatedDate = sld.LastUpdatedDate,
					UpdatedByUser = sld.UserLoginHistory.User.Name,
				}).ToList();
			}
		}

		public enum DeleteStudentLibraryDefaulterStatuses
		{
			NoRecordFound,
			Success,
		}

		public static DeleteStudentLibraryDefaulterStatuses DeleteStudentLibraryDefaulter(int studentLibraryDefaulterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				//var studentLibraryDefaulters = aspireContext.StudentLibraryDefaulters.SingleOrDefault(sld => sld.StudentLibraryDefaulterID == studentLibraryDefualterID);
				var student = aspireContext.StudentLibraryDefaulters.Where(sld => sld.StudentLibraryDefaulterID == studentLibraryDefaulterID).Select(sld => new
				{
					sld.Student.StudentID,
					sld.Student.AdmissionOpenProgram.Program.InstituteID,
					sld.Student.AdmissionOpenProgram.Program.DepartmentID,
					sld.Student.AdmissionOpenProgram.ProgramID,
					sld.Student.AdmissionOpenProgramID,
					sld.Student.AdmissionOpenProgram.SemesterID,
					sld.Student.AdmissionOpenProgram.MaxSemesterID,
					StudentLibraryDefaulter = sld,
				}).SingleOrDefault();
				if (student == null)
					return DeleteStudentLibraryDefaulterStatuses.NoRecordFound;
				if (student.StudentLibraryDefaulter == null)
					return DeleteStudentLibraryDefaulterStatuses.NoRecordFound;

				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				aspireContext.StudentLibraryDefaulters.Remove(student.StudentLibraryDefaulter);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteStudentLibraryDefaulterStatuses.Success;
			}
		}

		public static Aspire.Model.Entities.StudentLibraryDefaulter GetStudentLibraryDefaulter(int studentLibraryDefaulterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.StudentLibraryDefaulters.Include(sld => sld.Student).SingleOrDefault(sld => sld.StudentLibraryDefaulterID == studentLibraryDefaulterID);
			}
		}

		public enum UpdateStudentLibraryDefaulterStatuses
		{
			NoRecordFound,
			Success,
		}

		public static UpdateStudentLibraryDefaulterStatuses UpdateStudentLibraryDefaulter(int studentLibraryDefaulterID, string remarks, Model.Entities.StudentLibraryDefaulter.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistoryActive = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var studentLibraryDefaulter = aspireContext.StudentLibraryDefaulters.SingleOrDefault(sld => sld.StudentLibraryDefaulterID == studentLibraryDefaulterID);
				if (studentLibraryDefaulter == null)
					return UpdateStudentLibraryDefaulterStatuses.NoRecordFound;
				var student = aspireContext.Students.Where(s => s.StudentID == studentLibraryDefaulter.StudentID).Select(s => new
				{
					s.AdmissionOpenProgram.Program.InstituteID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgramID
				}).SingleOrDefault();
				if (student == null)
					return UpdateStudentLibraryDefaulterStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, loginHistoryActive.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				studentLibraryDefaulter.Remarks = remarks;
				studentLibraryDefaulter.StatusEnum = statusEnum;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateStudentLibraryDefaulterStatuses.Success;
			}
		}
	}
}
