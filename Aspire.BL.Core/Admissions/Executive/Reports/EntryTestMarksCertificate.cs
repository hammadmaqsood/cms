﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Executive.Reports
{
	public static class EntryTestMarksCertificate
	{
		private static Core.Common.Permissions.Executive.ExecutiveGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandExecutivePermissions(loginSessionGuid, ExecutivePermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class EntryTestMarksCertificatePageHeader
		{
			public string Institute { get; internal set; }
			public string DepartmentName { get; internal set; }
			public string Title => $"Entry Test Marks Certificate";
			public static List<EntryTestMarksCertificatePageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class EntryTestMarksCertificates
		{
			public int CandidateAppliedProgramID { get; internal set; }
			public int CandidateID { get; internal set; }
			public short SemesterID { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public string DepartmentName { get; internal set; }
			public int ProgramID { get; internal set; }
			public string CandidateName { get; internal set; }
			public string FatherName { get; internal set; }
			public int? ApplicationNo { get; internal set; }
			public byte? Gender { get; internal set; }

			public string ProgramShortName { get; internal set; }
			public DateTime? EntryTestDate { get; internal set; }
			public byte? ETSType { get; internal set; }
			public double? ETSObtained { get; internal set; }
			public double? ETSTotal { get; internal set; }
			public byte? EntryTestMarksPercentage { get; internal set; }
			public byte? EntryTestMarksPercentageEmpty
			{
				get
				{
					var marks = this.EntryTestMarksPercentage ?? 0;
					if (this.ETSType != null && this.ETSObtained != null && this.ETSTotal != null)
					{
						var etsPercentage = (byte)Math.Round(this.ETSObtained.Value * 100d / this.ETSTotal.Value, MidpointRounding.AwayFromZero);
						if (etsPercentage > marks)
							return etsPercentage;
					}
					return marks;
				}
			}
			public string Semester => this.SemesterID.ToSemesterString();
			public Genders? GenderEnum => (Genders?)this.Gender;
			public string Title
			{
				get
				{
					switch (this.GenderEnum)
					{
						case Genders.Male:
							return "Mr.";
						case Genders.Female:
							return "Ms.";
						case null:
							return string.Empty;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}

			public string EntryTestMarksPercentageComplete
			{
				get
				{
					if (this.ETSType == null || this.ETSObtained == null || this.ETSTotal == null && this.EntryTestMarksPercentage != null) return $"{this.EntryTestMarksPercentage}";
					if (this.ETSType != null && this.ETSObtained != null && this.ETSTotal != null)
					{
						if (this.EntryTestDate == null && this.EntryTestMarksPercentage == null)
							return $"{(byte)Math.Round(this.ETSObtained.Value * 100d / this.ETSTotal.Value, MidpointRounding.AwayFromZero)}";
						var marks = this.EntryTestMarksPercentage ?? 0;
						var etsPercentage = (byte)Math.Round(this.ETSObtained.Value * 100d / this.ETSTotal.Value, MidpointRounding.AwayFromZero);
						if (etsPercentage > marks)
							return $"{etsPercentage}";
						return $"{marks}";
					}
					else
					{
						return "N/A";
					}
				}
			}

			public static List<EntryTestMarksCertificates> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet()
			{
			}

			public EntryTestMarksCertificatePageHeader PageHeader { get; internal set; }
			public List<EntryTestMarksCertificates> EntryTestMarksCertificate { get; internal set; }
		}

		public static ReportDataSet GetEntryTestMarksCertificate(short intakeSemesterID, int? departmentID, int? programID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms.Where(aop => aop.Program.InstituteID == loginHistory.InstituteID && aop.SemesterID == intakeSemesterID);

				var departmentIDs = aspireContext.UserRoleModules
					.Where(urm => urm.UserRoleID == loginHistory.UserRoleID)
					.Where(urm => urm.Module == loginHistory.PermissionType.Module)
					.Select(urm => urm.DepartmentID).ToList();
				var programIDs = aspireContext.UserRoleModules
					.Where(urm => urm.UserRoleID == loginHistory.UserRoleID)
					.Where(urm => urm.Module == loginHistory.PermissionType.Module)
					.Select(urm => urm.ProgramID).ToList();
				var admissionOpenProgramIDs = aspireContext.UserRoleModules
					.Where(urm => urm.UserRoleID == loginHistory.UserRoleID)
					.Where(urm => urm.Module == loginHistory.PermissionType.Module)
					.Select(urm => urm.AdmissionOpenProgramID).ToList();

				if (departmentIDs.All(d => d != null))
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => departmentIDs.Contains(aop.Program.DepartmentID));
				if (programIDs.All(d => d != null))
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => programIDs.Contains(aop.ProgramID));
				if (admissionOpenProgramIDs.All(d => d != null))
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => admissionOpenProgramIDs.Contains(aop.AdmissionOpenProgramID));

				if (departmentID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.Program.DepartmentID == departmentID.Value);
				if (programID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.ProgramID == programID.Value);

				var query = from cap in aspireContext.CandidateAppliedPrograms
							join aop in admissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
							where cap.AccountTransactionID != null && (cap.EntryTestMarksPercentage != null || (cap.ETSType != null && cap.ETSObtained != null && cap.ETSTotal != null)) && cap.ApplicationNo != null
							select new EntryTestMarksCertificates
							{
								CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
								CandidateID = cap.CandidateID,
								SemesterID = aop.SemesterID,
								DepartmentID = aop.Program.DepartmentID,
								DepartmentName = aop.Program.Department.DepartmentName,
								ProgramID = aop.Program.ProgramID,
								CandidateName = cap.Candidate.Name,
								FatherName = cap.Candidate.FatherName,
								ApplicationNo = cap.ApplicationNo,
								Gender = cap.Candidate.Gender,
								ProgramShortName = aop.Program.ProgramShortName,
								EntryTestDate = cap.EntryTestDate,
								ETSType = cap.ETSType,
								ETSObtained = cap.ETSObtained,
								ETSTotal = cap.ETSTotal,
								EntryTestMarksPercentage = cap.EntryTestMarksPercentage,
							};

				return new ReportDataSet
				{
					PageHeader = new EntryTestMarksCertificatePageHeader
					{
						Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single(),
					},
					EntryTestMarksCertificate = query.ToList(),
				};
			}
		}
	}
}
