﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Candidate
{
	public static class ApplyForProgram
	{
		public sealed class CandidateAppliedProgramStatus
		{
			internal CandidateAppliedProgramStatus() { }
			public int CandidateAppliedProgramID { get; internal set; }
			public bool ForeignStudent { get; internal set; }
			public int AdmissionOpenProgramID { get; internal set; }
			public string ProgramShortName { get; internal set; }
			public string ProgramName { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public int InstituteID { get; internal set; }
			public string InstituteShortName { get; internal set; }
			public DateTime ApplyDate { get; internal set; }
			public DateTime? DepositDate { get; internal set; }
			public DateTime? AdmissionFeeLastDate { get; internal set; }
			public int? AdmittedAdmissionOpenProgramID { get; internal set; }
			public DateTime? EnrollmentGenerated { get; internal set; }
			public DateTime AdmissionClosingDate { get; internal set; }
			public DateTime? EntryTestDate { get; internal set; }
			public DateTime? InterviewsEndDate { get; internal set; }
			public DateTime? InterviewsStartDate { get; internal set; }

			public bool AdmissionProcessingFeeSubmitted => this.DepositDate != null;
			public bool CanDownloadAdmissionProcessingFeeChallan => this.AdmissionProcessingFeeSubmitted == false && this.AdmissionProcessingFeeDueDate >= DateTime.Now;
			public string AdmissionProcessingFeeStatus
			{
				get
				{
					if (this.AdmissionProcessingFeeSubmitted)
						return "Submitted";
					if (this.AdmissionProcessingFeeDueDate < DateTime.Now)
						return "Not Submitted (Admissions Closed)";
					return "Not Submitted";
				}
			}

			public bool CanDownloadAdmitSlip
			{
				get
				{
					if (this.AdmissionProcessingFeeSubmitted)
					{
						if (this.AdmittedAdmissionOpenProgramID == null)
						{
							if (this.EntryTestDate != null)
							{
								if (this.EntryTestDate >= DateTime.Now)
									return true;
							}
							else
							{
								if (this.InterviewsEndDate != null)
								{
									if (this.InterviewsEndDate.Value >= DateTime.Now)
										return true;
								}
								else //no end date specified
									return true;
							}
						}
					}
					return false;
				}
			}

			public string AdmitSlipStatus => this.CanDownloadAdmitSlip ? "" : string.Empty.ToNAIfNullOrEmpty();

			private bool InterviewCleared => (this.ForeignStudent || this.AdmissionProcessingFeeSubmitted) && this.AdmittedAdmissionOpenProgramID != null;

			public string InterviewStatus
			{
				get
				{
					if (this.ForeignStudent)
						return this.InterviewCleared ? "Yes" : "No";
					return this.InterviewCleared ? "Cleared" : string.Empty.ToNAIfNullOrEmpty();
				}
			}

			public string AdmissionFeeStatus
			{
				get
				{
					const string contactAccountOffice = "Contact Accounts Office to get Fee Challan.";
					if (!this.InterviewCleared)
						return string.Empty.ToNAIfNullOrEmpty();
					switch (this.StudentFees.Count)
					{
						case 0:
							return contactAccountOffice;
						case 1:
							var studentFee = this.StudentFees.Single();
							switch (studentFee.StatusEnum)
							{
								case Model.Entities.StudentFee.Statuses.NotPaid:
									return "Not Paid";
								case Model.Entities.StudentFee.Statuses.Paid:
									return "Paid";
								default:
									throw new ArgumentOutOfRangeException();
							}
						default:
							if (this.StudentFees.All(sf => sf.StatusEnum == Model.Entities.StudentFee.Statuses.Paid))
								return "Paid";
							return contactAccountOffice;
					}
				}
			}

			public bool CanDownloadAdmissionFeeChallan
			{
				get
				{
					if (this.StudentFees.Count == 1)
					{
						var studentFee = this.StudentFees.Single();
						switch (studentFee.StatusEnum)
						{
							case Model.Entities.StudentFee.Statuses.NotPaid:
								return true;
							case Model.Entities.StudentFee.Statuses.Paid:
								return false;
							default:
								throw new ArgumentOutOfRangeException();
						}
					}
					return false;
				}
			}

			public int? StudentFeeID => this.StudentFees.Count == 1 ? (int?)this.StudentFees.Single().StudentFeeID : null;

			public List<StudentFee> StudentFees { get; internal set; }
			public byte Category { get; internal set; }
			public Categories CategoryEnum => (Categories)this.Category;
			public DateTime AdmissionProcessingFeeDueDate { get; internal set; }

			public sealed class StudentFee
			{
				internal StudentFee() { }
				public int StudentFeeID { get; internal set; }
				public byte Status { get; internal set; }
				public Model.Entities.StudentFee.Statuses StatusEnum => (Model.Entities.StudentFee.Statuses)this.Status;
			}
		}

		public sealed class GetAppliedProgramsStatusResult
		{
			internal GetAppliedProgramsStatusResult()
			{
			}

			public byte CandidateStatus { get; internal set; }
			public Model.Entities.Candidate.Statuses CandidateStatusEnum => (Model.Entities.Candidate.Statuses)this.CandidateStatus;
			public List<CandidateAppliedProgramStatus> CandidateAppliedProgramStatuses { get; internal set; }
		}

		public static GetAppliedProgramsStatusResult GetAppliedProgramsStatus(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				var query = from cap in aspireContext.CandidateAppliedPrograms
							join aop in aspireContext.AdmissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
							where cap.CandidateID == loginHistory.CandidateID
							select new CandidateAppliedProgramStatus
							{
								CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
								ForeignStudent = cap.Candidate.ForeignStudent,
								AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
								ProgramShortName = aop.Program.ProgramShortName,
								ProgramName = aop.Program.ProgramName,
								SemesterID = aop.SemesterID,
								InstituteID = aop.Program.InstituteID,
								InstituteShortName = aop.Program.Institute.InstituteShortName,
								ApplyDate = cap.ApplyDate,
								DepositDate = cap.AccountTransaction.TransactionDate,
								AdmissionFeeLastDate = cap.AdmissionFeeLastDate,
								AdmittedAdmissionOpenProgramID = cap.AdmittedAdmissionOpenProgramID,
								EnrollmentGenerated = cap.EnrollmentGenerated,
								AdmissionClosingDate = aop.AdmissionClosingDate,
								AdmissionProcessingFeeDueDate = aop.AdmissionProcessingFeeDueDate,
								EntryTestDate = cap.EntryTestDate,
								InterviewsStartDate = aop.InterviewsStartDate,
								InterviewsEndDate = aop.InterviewsEndDate,
								Category = cap.Candidate.Category.Value,
								StudentFees = cap.StudentFees.Select(sf => new CandidateAppliedProgramStatus.StudentFee
								{
									StudentFeeID = sf.StudentFeeID,
									Status = sf.Status,
								}).ToList()
							};

				return new GetAppliedProgramsStatusResult
				{
					CandidateAppliedProgramStatuses = query.OrderBy(a => a.ApplyDate).ToList(),
					CandidateStatus = aspireContext.Candidates.Where(c => c.CandidateID == loginHistory.CandidateID).Select(c => c.Status).Single()
				};
			}
		}

		public sealed class GetAdmissionsOpenInstitutesResult
		{
			public Model.Entities.Candidate.Statuses CandidateStatus { get; internal set; }
			public sealed class Institute
			{
				internal Institute() { }
				public int InstituteID { get; internal set; }
				public string InstituteName { get; internal set; }
				public string InstituteShortName { get; internal set; }
				public string InstituteAlias { get; internal set; }
			}
			public List<Institute> Institutes { get; internal set; }
		}

		public static GetAdmissionsOpenInstitutesResult GetNotAppliedAdmissionsOpenInstitutes(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				return new GetAdmissionsOpenInstitutesResult
				{
					CandidateStatus = (Model.Entities.Candidate.Statuses)aspireContext.Candidates.Where(c => c.CandidateID == loginHistory.CandidateID).Select(c => c.Status).Single(),
					Institutes = aspireContext.GetNotAppliedAdmissionOpenPrograms(loginHistory.CandidateID).Select(aop => new GetAdmissionsOpenInstitutesResult.Institute
					{
						InstituteID = aop.Program.InstituteID,
						InstituteName = aop.Program.Institute.InstituteName,
						InstituteShortName = aop.Program.Institute.InstituteShortName,
						InstituteAlias = aop.Program.Institute.InstituteAlias
					}).Distinct().OrderBy(i => i.InstituteID).ToList()
				};
			}
		}

		public sealed class NotAppliedAdmissionOpenProgram
		{
			internal NotAppliedAdmissionOpenProgram() { }
			public int AdmissionOpenProgramID { get; internal set; }
			public int ProgramID { get; internal set; }
			public string ProgramName { get; internal set; }
			public string ProgramShortName { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public ProgramDurations ProgramDurationEnum { get; internal set; }
		}

		public static List<NotAppliedAdmissionOpenProgram> GetNotAppliedAdmissionsOpenPrograms(int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				return aspireContext.GetNotAppliedAdmissionOpenPrograms(loginHistory.CandidateID)
					.Where(p => p.Program.InstituteID == instituteID)
					.Select(aop => new NotAppliedAdmissionOpenProgram
					{
						AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
						ProgramID = aop.ProgramID,
						ProgramName = aop.Program.ProgramName,
						ProgramShortName = aop.Program.ProgramShortName,
						ProgramAlias = aop.Program.ProgramAlias,
						ProgramDurationEnum = (ProgramDurations)aop.Program.Duration
					}).ToList();
			}
		}

		public static (Model.Entities.AdmissionOpenProgram, List<(Guid ResearchThemeAreaID, string AreaName, string ResearchThemeName)>) GetAdmissionOpenProgram(int instituteID, int admissionOpenProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				var admissionOpenProgram = aspireContext.GetNotAppliedAdmissionOpenPrograms(loginHistory.CandidateID)
					.Where(aop => aop.Program.InstituteID == instituteID)
						.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID)
						.SingleOrDefault();
				if (admissionOpenProgram == null)
					return (null, null);
				var researchThemeIDs = aspireContext.ProgramResearchThemes.Where(rt => rt.ProgramID == admissionOpenProgram.ProgramID).Select(rt => (Guid)rt.ResearchThemeID);
				var researchThemeAreas = aspireContext.ResearchThemeAreas.Where(rta => researchThemeIDs.Contains(rta.ResearchThemeID)).ToList().Select(rta => ((Guid ResearchThemeAreaID, string AreaName, string ResearchThemeName))(rta.ResearchThemeAreaID, rta.AreaName, rta.ResearchTheme.ResearchThemeName)).ToList();
				return (admissionOpenProgram, researchThemeAreas);
			}
		}

		public static List<AdmissionCriteriaPreReq> GetAdmissionCriteriaPreReqs(int instituteID, int admissionOpenProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				return aspireContext.GetNotAppliedAdmissionOpenPrograms(loginHistory.CandidateID)
					.Where(aop => aop.Program.InstituteID == instituteID)
					.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID)
					.Select(aop => aop.AdmissionCriteria)
					.SelectMany(ac => ac.AdmissionCriteriaPreReqs)
					.Include(acpr => acpr.AdmissionCriteriaPreReqDegrees.Select(acprd => acprd.AdmissionCriteriaPreReqDegreeSubjects))
					.ToList();
			}
		}

		public sealed class PreReqDegreeType
		{
			public byte DegreeType { get; internal set; }
			public byte DisplaySequence { get; internal set; }
			public int AdmissionCriteriaPreReqDegreeID { get; internal set; }
		}

		public static List<PreReqDegreeType> GetAdmissionCriteriaPreReqDegreeTypes(int instituteID, int admissionOpenProgramID, int admissionCriteriaID, string preReqQualification, int admissionCriteriaPreReqID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				return aspireContext.GetNotAppliedAdmissionOpenPrograms(loginHistory.CandidateID)
					.Where(aop => aop.Program.InstituteID == instituteID)
					.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID)
					.Select(aop => aop.AdmissionCriteria)
					.Where(ac => ac.AdmissionCriteriaID == admissionCriteriaID)
					.SelectMany(ac => ac.AdmissionCriteriaPreReqs)
					.Where(acpr => acpr.PreReqQualification == preReqQualification)
					.Where(acpr => acpr.AdmissionCriteriaPreReqID == admissionCriteriaPreReqID)
					.SelectMany(acpr => acpr.AdmissionCriteriaPreReqDegrees)
					.Select(c => new PreReqDegreeType
					{
						AdmissionCriteriaPreReqDegreeID = c.AdmissionCriteriaPreReqDegreeID,
						DegreeType = c.DegreeType,
						DisplaySequence = c.DisplaySequence
					}).OrderBy(acpr => acpr.DisplaySequence).ToList();
			}
		}

		public static List<CustomListItem> GetAdmissionCriteriaPreReqDegreesDegreeTypes(int admissionCriteriaPreReqID, byte displaySequence, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				var degreeTypes = aspireContext.AdmissionCriteriaPreReqDegrees.Where(c => c.AdmissionCriteriaPreReqID == admissionCriteriaPreReqID && c.DisplaySequence == displaySequence).Select(c => new { c.AdmissionCriteriaPreReqDegreeID, c.DegreeType }).ToList();
				return degreeTypes.Select(c => new CustomListItem
				{
					ID = c.AdmissionCriteriaPreReqDegreeID,
					Text = ((DegreeTypes)c.DegreeType).ToFullName(),
				}).ToList();
			}
		}

		public static AdmissionCriteriaPreReqDegree GetAdmissionCriteriaPreReqDegree(int admissionCriteriaPreReqDegreeID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				return aspireContext.AdmissionCriteriaPreReqDegrees.Include(c => c.AdmissionCriteriaPreReqDegreeSubjects).SingleOrDefault(c => c.AdmissionCriteriaPreReqDegreeID == admissionCriteriaPreReqDegreeID);
			}
		}

		public enum ApplyForProgramStatuses
		{
			NoRecordFound,
			Choice1AdmissionNotOpened,
			Choice1AlreadyApplied,
			AdmissionCriteriaPreReqNotFound,
			ProfileInformationIsBlank,
			ProfileInformationPictureNotUploaded,
			Success
		}

		public static ApplyForProgramStatuses Apply(int admissionCriteriaPreReqID, int choice1AdmissionOpenProgramID, Guid? researchThemeAreaID, bool creditTransfer, Shifts shiftEnum, List<CandidateAppliedProgramAcademicRecord> academicRecords, Guid loginSessionGuid)
		{
			lock (BL.Core.Common.Locks.ApplyProgram)
				using (var aspireContext = new AspireContext(true))
				{
					var now = DateTime.Now;
					var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
					if (loginHistory.ForeignStudent)
						throw new InvalidOperationException();
					var candidate = aspireContext.Candidates.Single(c => c.CandidateID == loginHistory.CandidateID);
					switch (candidate.StatusEnum)
					{
						case Model.Entities.Candidate.Statuses.Blank:
						case Model.Entities.Candidate.Statuses.PictureUploaded:
							return ApplyForProgramStatuses.ProfileInformationIsBlank;
						case Model.Entities.Candidate.Statuses.InformationProvided:
							return ApplyForProgramStatuses.ProfileInformationPictureNotUploaded;
						case Model.Entities.Candidate.Statuses.Completed:
							break;
						default:
							throw new NotImplementedEnumException(candidate.StatusEnum);
					}

					var choice1AdmissionOpenProgram = aspireContext.AdmissionOpenPrograms
						.Where(aop => aop.AdmissionOpenProgramID == choice1AdmissionOpenProgramID)
						.Select(aop => new
						{
							aop.AdmissionOpenProgramID,
							aop.AdmissionProcessingFee,
							EntryTestTypeEnum = (AdmissionOpenProgram.EntryTestTypes)aop.EntryTestType,
							aop.EntryTestStartDate,
							aop.EntryTestEndDate,
							aop.EntryTestDuration,
							aop.AdmissionCriteriaID,
							AdmissionCriteriaPreReq = aop.AdmissionCriteria.AdmissionCriteriaPreReqs.Where(acpr => acpr.AdmissionCriteriaPreReqID == admissionCriteriaPreReqID)
								.Select(acpr => new
								{
									acpr.AdmissionCriteriaPreReqID,
									acpr.PreReqQualification,
									acpr.PreReqQualificationResultStatus,
									ResultAwaitedDegreeTypeEnum = (DegreeTypes?)acpr.ResultAwaitedDegreeType
								}).FirstOrDefault(),
							AlreadyApplied = aspireContext.CandidateAppliedPrograms.Any(cap => cap.CandidateID == candidate.CandidateID && cap.Choice1AdmissionOpenProgramID == choice1AdmissionOpenProgramID)
						}).SingleOrDefault();

					if (choice1AdmissionOpenProgram == null)
						return ApplyForProgramStatuses.NoRecordFound;
					if (choice1AdmissionOpenProgram.AlreadyApplied)
						return ApplyForProgramStatuses.Choice1AlreadyApplied;
					if (!aspireContext.GetNotAppliedAdmissionOpenPrograms(candidate.CandidateID).Any(aop => aop.AdmissionOpenProgramID == choice1AdmissionOpenProgramID))
						return ApplyForProgramStatuses.Choice1AdmissionNotOpened;

					var admissionCriteriaPreReqDegrees = aspireContext.AdmissionCriteriaPreReqDegrees
						.Include(acprd => acprd.AdmissionCriteriaPreReqDegreeSubjects)
						.Where(acpr => acpr.AdmissionCriteriaPreReqID == choice1AdmissionOpenProgram.AdmissionCriteriaPreReq.AdmissionCriteriaPreReqID)
						.OrderBy(acprd => acprd.DisplaySequence)
						.ToList();
					if (admissionCriteriaPreReqDegrees?.Any() != true)
						return ApplyForProgramStatuses.AdmissionCriteriaPreReqNotFound;
					var displaySequences = admissionCriteriaPreReqDegrees.Select(acprd => acprd.DisplaySequence).Distinct().OrderBy(s => s).ToList();
					if (academicRecords?.Count != displaySequences.Count)
						throw new ArgumentException("Academic Records count do not match with admission criteria.", nameof(academicRecords));

					var candidateAppliedProgramAcademicRecords = new List<CandidateAppliedProgramAcademicRecord>();
					var pAcademicRecords = academicRecords.ToList();

					ArgumentException GetException()
					{
						return new ArgumentException($"Academic Records do not match admission criteria. {nameof(choice1AdmissionOpenProgram.AdmissionCriteriaPreReq.AdmissionCriteriaPreReqID)}:{choice1AdmissionOpenProgram.AdmissionCriteriaPreReq.AdmissionCriteriaPreReqID}", nameof(academicRecords));
					}

					foreach (var displaySequence in displaySequences)
					{
						var selectedAdmissionCriteriaPreReqDegrees = admissionCriteriaPreReqDegrees.Where(acprd => acprd.DisplaySequence == displaySequence).ToList();
						var pAcademicRecord = pAcademicRecords.First();
						pAcademicRecords.Remove(pAcademicRecord);
						var selectedAdmissionCriteriaPreReqDegree = selectedAdmissionCriteriaPreReqDegrees.SingleOrDefault(acprd => acprd.DegreeTypeEnum == pAcademicRecord.DegreeTypeEnum);
						if (selectedAdmissionCriteriaPreReqDegree == null)
							throw GetException();

						var candidateAppliedProgramAcademicRecord = new CandidateAppliedProgramAcademicRecord
						{
							AdmissionCriteriaPreReqDegreeID = selectedAdmissionCriteriaPreReqDegree.AdmissionCriteriaPreReqDegreeID,
							DegreeTypeEnum = pAcademicRecord.DegreeTypeEnum,
							PassingYear = pAcademicRecord.PassingYear,
							Institute = pAcademicRecord.Institute.TrimAndCannotBeEmpty(),
							BoardUniversity = pAcademicRecord.Institute.TrimAndCannotBeEmpty(),
							Subjects = pAcademicRecord.Subjects.TrimAndCannotBeEmpty(),
							IsCGPA = pAcademicRecord.IsCGPA,
							ObtainedMarks = pAcademicRecord.ObtainedMarks,
							TotalMarks = pAcademicRecord.TotalMarks,
							Percentage = pAcademicRecord.Percentage
						}.Validate(selectedAdmissionCriteriaPreReqDegree, candidate.DOB ?? throw new InvalidOperationException());
						candidateAppliedProgramAcademicRecords.Add(candidateAppliedProgramAcademicRecord);
					}

					DateTime? entryTestDate;
					switch (choice1AdmissionOpenProgram.EntryTestTypeEnum)
					{
						case AdmissionOpenProgram.EntryTestTypes.None:
						case AdmissionOpenProgram.EntryTestTypes.ComputerBasedTest:
							entryTestDate = null;
							break;
						case AdmissionOpenProgram.EntryTestTypes.PaperBasedTest:
							entryTestDate = choice1AdmissionOpenProgram.EntryTestStartDate ?? throw new InvalidOperationException();
							break;
						default:
							throw new NotImplementedEnumException(choice1AdmissionOpenProgram.EntryTestTypeEnum);
					}

					var candidateAppliedProgram = new CandidateAppliedProgram
					{
						ChallanNo = aspireContext.GetNextAdmissionProcessingFeeChallanNo().Single() ?? throw new InvalidOperationException("Null is returned for Next Challan No."),
						CandidateID = candidate.CandidateID,
						AdmissionCriteriaPreReqID = admissionCriteriaPreReqID,
						ApplicationNo = null,
						ResearchThemeAreaID = researchThemeAreaID,
						Choice1AdmissionOpenProgramID = choice1AdmissionOpenProgramID,
						Choice2AdmissionOpenProgramID = null,
						Choice3AdmissionOpenProgramID = null,
						Choice4AdmissionOpenProgramID = null,
						Amount = choice1AdmissionOpenProgram.AdmissionProcessingFee,
						ShiftEnum = shiftEnum,
						CreditTransfer = creditTransfer,
						EntryTestDate = entryTestDate,
						TestDuration = choice1AdmissionOpenProgram.EntryTestDuration,
						PreReqDegree = choice1AdmissionOpenProgram.AdmissionCriteriaPreReq.PreReqQualification,
						ResultAwaitedDegreeTypeEnum = choice1AdmissionOpenProgram.AdmissionCriteriaPreReq.ResultAwaitedDegreeTypeEnum,
						StatusEnum = CandidateAppliedProgram.Statuses.None,
						ApplyDate = now,
						AccountTransactionID = null,
						ETSTypeEnum = null,
						ETSObtained = null,
						ETSTotal = null,
						InterviewDate = null,
						InterviewRemarks = null,
						AdmissionFeeLastDate = null,
						AdmittedAdmissionOpenProgramID = null,
						AdmittedShiftEnum = null,
						EnrollmentGenerated = null,
						StudentID = null,
						EntryTestMarksPercentage = null,
						CBTSessionLabID = null,
						StatusDate = null,
						DeferredDate = null,
						DeferredToAdmittedAdmissionOpenProgramID = null,
						DeferredToShift = null,
						DeferredToShiftEnum = null,
						CandidateAppliedProgramAcademicRecords = academicRecords
					};

					candidate.EditStatusEnum = Model.Entities.Candidate.EditStatuses.CanNotEditBasicInfo;
					candidate.CandidateAppliedPrograms.Add(candidateAppliedProgram);
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
					return ApplyForProgramStatuses.Success;
				}
		}

		public sealed class GetCBTSessionsResult
		{
			public int CandidateAppliedProgramID { get; internal set; }
			public int CandidateID { get; internal set; }
			public int InstituteID { get; internal set; }
			public string InstituteShortName { get; internal set; }
			public short SemesterID { get; internal set; }
			public string ProgramShortName { get; internal set; }
			public byte EntryTestType { get; internal set; }
			public int ApplicationNo { get; internal set; }
			public List<CBTSession> CBTSessions { get; internal set; }
			internal DateTime? EntryTestStartDate { get; set; }
			internal DateTime? EntryTestEndDate { get; set; }
			internal byte? EntryTestDuration { get; set; }
			internal DateTime ApplyDate { get; set; }

			public sealed class CBTSession
			{
				public int CBTSessionID { get; internal set; }
				public string InstituteShortName { get; internal set; }
				public DateTime StartTime { get; internal set; }
				public byte DurationInMinutes { get; internal set; }
				public DateTime EndTime => this.StartTime.AddMinutes(this.DurationInMinutes).ToString("d").ToDateTime("d");
				public string Timing => $"{this.StartTime:hh:mm tt} - {this.StartTime.AddMinutes(this.DurationInMinutes):hh:mm tt}";
				public int Seats { get; internal set; }
				public int AssignedSeats { get; internal set; }
			}
		}

		public static GetCBTSessionsResult GetCBTSessions(int candidateAppliedProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				if (loginHistory.ForeignStudent)
					throw new InvalidOperationException();

				var result = aspireContext.CandidateAppliedPrograms
					.Where(cap => cap.CandidateAppliedProgramID == candidateAppliedProgramID && cap.CandidateID == loginHistory.CandidateID)
					.Where(cap => cap.AccountTransactionID != null)
					.Where(cap => cap.CBTSessionLabID == null)
					.Select(cap => new GetCBTSessionsResult
					{
						CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
						CandidateID = cap.CandidateID,
						InstituteID = cap.AdmissionOpenProgram1.Program.InstituteID,
						InstituteShortName = cap.AdmissionOpenProgram1.Program.Institute.InstituteShortName,
						ProgramShortName = cap.AdmissionOpenProgram1.Program.ProgramShortName,
						SemesterID = cap.AdmissionOpenProgram1.SemesterID,
						ApplicationNo = cap.ApplicationNo.Value,
						EntryTestType = cap.AdmissionOpenProgram1.EntryTestType,
						EntryTestStartDate = cap.AdmissionOpenProgram1.EntryTestStartDate,
						EntryTestEndDate = cap.AdmissionOpenProgram1.EntryTestEndDate,
						EntryTestDuration = cap.AdmissionOpenProgram1.EntryTestDuration,
						ApplyDate = cap.ApplyDate,
					})
					.SingleOrDefault();
				if (result == null || (AdmissionOpenProgram.EntryTestTypes)result.EntryTestType != AdmissionOpenProgram.EntryTestTypes.ComputerBasedTest)
					return null;
				var cbtSessions = aspireContext.CBTSessions
				   .Where(s => s.InstituteID == result.InstituteID)
				   .Where(s => result.EntryTestStartDate <= DbFunctions.TruncateTime(s.StartTime) && DbFunctions.TruncateTime(s.StartTime) <= result.EntryTestEndDate)
				   .Where(s => s.DurationInMinutes >= result.EntryTestDuration)
				   .Where(s => s.StartTime > DateTime.Now)
				   .Select(s => new GetCBTSessionsResult.CBTSession
				   {
					   CBTSessionID = s.CBTSessionID,
					   InstituteShortName = s.Institute.InstituteShortName,
					   StartTime = s.StartTime,
					   DurationInMinutes = s.DurationInMinutes,
					   Seats = s.CBTSessionLabs.Sum(l => l.Computers),
					   AssignedSeats = s.CBTSessionLabs.SelectMany(l => l.CandidateAppliedPrograms).Count(),
				   }).ToList();

				result.CBTSessions = cbtSessions.Where(s =>
				{
					if (s.DurationInMinutes >= result.EntryTestDuration)
						if (s.Seats > s.AssignedSeats)
							if (result.EntryTestStartDate <= s.StartTime)
								if (s.EndTime <= result.EntryTestEndDate)
									return true;
					return false;
				}).OrderBy(s => s.StartTime).ToList();
				return result;
			}
		}

		public enum ConfirmCBTSessionStatuses
		{
			NoRecordFound,
			AlreadyAssignedToOtherAppliedProgram,
			AlreadyConfirmed,
			SessionFull,
			SessionNotAllowed,
			Confirmed,
		}

		public static ConfirmCBTSessionStatuses ConfirmCBTSession(int candidateAppliedProgramID, int cbtSessionID, Guid loginSessionGuid)
		{
			lock (BL.Core.Common.Locks.ConfirmCBTSessionLock)
			{
				using (var aspireContext = new AspireContext(true))
				{
					var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
					if (loginHistory.ForeignStudent)
						throw new InvalidOperationException();

					var result = aspireContext.CandidateAppliedPrograms
						.Where(cap => cap.CandidateAppliedProgramID == candidateAppliedProgramID && cap.CandidateID == loginHistory.CandidateID)
						.Where(cap => cap.AccountTransactionID != null)
						.Select(cap => new
						{
							CandidateAppliedProgram = cap,
							cap.AdmissionOpenProgram1.Program.InstituteID,
							cap.AdmissionOpenProgram1.EntryTestType,
							cap.AdmissionOpenProgram1.EntryTestStartDate,
							cap.AdmissionOpenProgram1.EntryTestEndDate,
							cap.AdmissionOpenProgram1.EntryTestDuration
						})
						.SingleOrDefault();
					if (result == null || (AdmissionOpenProgram.EntryTestTypes)result.EntryTestType != AdmissionOpenProgram.EntryTestTypes.ComputerBasedTest)
						return ConfirmCBTSessionStatuses.NoRecordFound;

					if (result.CandidateAppliedProgram.CBTSessionLabID != null)
						return ConfirmCBTSessionStatuses.AlreadyConfirmed;

					if (aspireContext.CandidateAppliedPrograms.Any(cap => cap.CandidateID == result.CandidateAppliedProgram.CandidateID && cap.CBTSessionLabID != null && cap.CBTSessionLab.CBTSessionID == cbtSessionID))
						return ConfirmCBTSessionStatuses.AlreadyAssignedToOtherAppliedProgram;

					var cbtSession = aspireContext.CBTSessions
						.Where(s => s.CBTSessionID == cbtSessionID)
						.Select(s => new
						{
							s.CBTSessionID,
							s.InstituteID,
							s.StartTime,
							s.DurationInMinutes,
							CBTSessionLab = s.CBTSessionLabs.Select(l => new
							{
								l.CBTSessionLabID,
								TotalSeats = l.Computers,
								AssignedSeats = l.CandidateAppliedPrograms.Count,
							}).ToList()
						}).SingleOrDefault();

					if (cbtSession == null)
						return ConfirmCBTSessionStatuses.NoRecordFound;

					if (cbtSession.InstituteID >= result.InstituteID)
						if (cbtSession.DurationInMinutes >= result.EntryTestDuration)
							if (result.EntryTestStartDate <= cbtSession.StartTime)
								if (cbtSession.StartTime.AddMinutes(cbtSession.DurationInMinutes) <= result.EntryTestEndDate?.AddHours(24)) // to include the end day. 
									foreach (var lab in cbtSession.CBTSessionLab)
										if (lab.AssignedSeats < lab.TotalSeats)
										{
											result.CandidateAppliedProgram.CBTSessionLabID = lab.CBTSessionLabID;
											aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
											return ConfirmCBTSessionStatuses.Confirmed;
										}
					return ConfirmCBTSessionStatuses.SessionNotAllowed;
				}
			}
		}
	}
}
