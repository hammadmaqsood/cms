﻿using Aspire.BL.Core.Admissions.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Candidate
{
	public static class Common
	{
		public sealed class Semester
		{
			internal Semester() { }
			public short SemesterID { get; internal set; }
			public bool LocalAllowed { get; internal set; }
			public bool ForeignerAllowed { get; internal set; }
		}

		public static List<Semester> GetAdmissionOpenSemesters()
		{
			using (var aspireContext = new AspireContext())
			{
				var now = DateTime.Now;
				return aspireContext.AdmissionOpenPrograms.FilterOpenAdmissions(null)
					.Select(aop => new Semester
					{
						SemesterID = aop.SemesterID,
						LocalAllowed = aop.AdmissionOpenDate <= now && now <= aop.AdmissionClosingDate,
						ForeignerAllowed = aop.AdmissionOpenDateForForeigners != null && aop.AdmissionClosingDateForForeigners != null && aop.AdmissionOpenDateForForeigners.Value <= now && now <= aop.AdmissionClosingDateForForeigners.Value
					}).Distinct().OrderByDescending(s => s).ToList();
			}
		}

		internal static IQueryable<short> GetCandidateLoginAllowedSemesterIDsQuery(this AspireContext aspireContext)
		{
			var now = DateTime.Now;
			return aspireContext.AdmissionOpenPrograms.Select(aop => new Semester
			{
				SemesterID = aop.SemesterID,
				LocalAllowed = now <= aop.LoginAllowedEndDate || (aop.AdmissionOpenDate <= now && now <= aop.AdmissionClosingDate) || now <= aop.AdmissionProcessingFeeDueDate,
				ForeignerAllowed = now <= aop.LoginAllowedEndDate || (aop.AdmissionOpenDateForForeigners != null && aop.AdmissionClosingDateForForeigners != null && aop.AdmissionOpenDateForForeigners.Value <= now && now <= aop.AdmissionClosingDateForForeigners.Value)
			})
			.Where(s => s.LocalAllowed || s.ForeignerAllowed)
			.Select(s => s.SemesterID)
			.Distinct()
			.OrderByDescending(s => s);
		}

		public static List<short> GetCandidateLoginAllowedSemesterIDs()
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.GetCandidateLoginAllowedSemesterIDsQuery().ToList();
			}
		}

		internal static IQueryable<AdmissionOpenProgram> GetNotAppliedAdmissionOpenPrograms(this AspireContext aspireContext, int candidateID)
		{
			var now = DateTime.Now;
			var candidate = aspireContext.Candidates.Where(c => c.CandidateID == candidateID).Select(c => new
			{
				c.ForeignStudent,
				c.SemesterID,
				AppliedAdmissionOpenProgramIDs = c.CandidateAppliedPrograms.Select(cap => cap.Choice1AdmissionOpenProgramID).ToList()
			}).Single();
			return aspireContext.AdmissionOpenPrograms.Include(aop => aop.Program).FilterOpenAdmissions(candidate.ForeignStudent)
				.Where(aop => aop.SemesterID == candidate.SemesterID)
				.Where(aop => !candidate.AppliedAdmissionOpenProgramIDs.Contains(aop.AdmissionOpenProgramID));
		}

		public sealed class OpenProgram
		{
			internal OpenProgram() { }
			public int InstituteID { get; internal set; }
			public string InstituteName { get; internal set; }
			public string InstituteShortName { get; internal set; }
			public string InstituteAlias { get; internal set; }
			public IEnumerable<Program> Programs { get; internal set; }

			public sealed class Program
			{
				internal Program() { }
				public int ProgramID { get; internal set; }
				public string ProgramName { get; internal set; }
				public string ProgramShortName { get; internal set; }
				public string ProgramAlias { get; internal set; }
				public DateTime AdmissionOpenDate { get; internal set; }
				public DateTime AdmissionClosingDate { get; internal set; }
				public DateTime? AdmissionOpenDateForForeigners { get; internal set; }
				public DateTime? AdmissionClosingDateForForeigners { get; internal set; }
				public DateTime AdmissionProcessingFeeDueDate { get; internal set; }
				public DateTime? InterviewsStartDate { get; internal set; }
				public DateTime? InterviewsEndDate { get; internal set; }
				public Shifts ShiftsEnum { get; internal set; }
				public short SemesterID { get; internal set; }
				public int AdmissionOpenProgramID { get; internal set; }
				public bool Applied { get; internal set; }
				public AdmissionOpenProgram.EntryTestTypes EntryTestTypeEnum { get; internal set; }
				public DateTime? EntryTestStartDate { get; internal set; }
				public DateTime? EntryTestEndDate { get; internal set; }
			}
		}

		public static List<OpenProgram> GetAllAdmissionOpenProgramsSchedule(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms
					.FilterOpenAdmissions(loginHistory.ForeignStudent)
					.Where(a => a.SemesterID == loginHistory.SemesterID).Select(aop => new
					{
						aop.AdmissionOpenProgramID,
						aop.ProgramID,
						aop.Program.ProgramName,
						aop.Program.ProgramShortName,
						aop.Program.ProgramAlias,
						aop.AdmissionOpenDate,
						aop.AdmissionClosingDate,
						aop.AdmissionOpenDateForForeigners,
						aop.AdmissionClosingDateForForeigners,
						aop.AdmissionProcessingFeeDueDate,
						EntryTestTypeEnum = (AdmissionOpenProgram.EntryTestTypes)aop.EntryTestType,
						aop.EntryTestStartDate,
						aop.EntryTestEndDate,
						aop.InterviewsStartDate,
						aop.InterviewsEndDate,
						ShiftsEnum = (Shifts)aop.Shifts,
						aop.SemesterID,
						aop.Program.InstituteID,
						aop.Program.Institute.InstituteName,
						aop.Program.Institute.InstituteShortName,
						aop.Program.Institute.InstituteAlias,
						Applied = aspireContext.CandidateAppliedPrograms.Any(c => c.CandidateID == loginHistory.CandidateID && c.Choice1AdmissionOpenProgramID == aop.AdmissionOpenProgramID)
					}).ToList();

				return admissionOpenPrograms.GroupBy(aop => new { aop.InstituteID, aop.InstituteName, aop.InstituteShortName, aop.InstituteAlias })
					.Select(g => new OpenProgram
					{
						InstituteID = g.Key.InstituteID,
						InstituteName = g.Key.InstituteName,
						InstituteShortName = g.Key.InstituteShortName,
						InstituteAlias = g.Key.InstituteAlias,
						Programs = g.Select(p => new OpenProgram.Program
						{
							AdmissionOpenProgramID = p.AdmissionOpenProgramID,
							ProgramID = p.ProgramID,
							ProgramName = p.ProgramName,
							ProgramShortName = p.ProgramShortName,
							ProgramAlias = p.ProgramAlias,
							SemesterID = p.SemesterID,
							AdmissionOpenDate = p.AdmissionOpenDate,
							AdmissionClosingDate = p.AdmissionClosingDate,
							AdmissionOpenDateForForeigners = p.AdmissionOpenDateForForeigners,
							AdmissionClosingDateForForeigners = p.AdmissionClosingDateForForeigners,
							AdmissionProcessingFeeDueDate = p.AdmissionProcessingFeeDueDate,
							Applied = p.Applied,
							EntryTestTypeEnum = p.EntryTestTypeEnum,
							EntryTestStartDate = p.EntryTestStartDate,
							EntryTestEndDate = p.EntryTestEndDate,
							InterviewsEndDate = p.InterviewsEndDate,
							InterviewsStartDate = p.InterviewsStartDate,
							ShiftsEnum = p.ShiftsEnum
						}).ToList()
					}).ToList();
			}
		}

		public sealed class EligibilityCriteria
		{
			internal EligibilityCriteria() { }
			public int InstituteID { get; internal set; }
			public string InstituteName { get; internal set; }
			public string InstituteShortName { get; internal set; }
			public string InstituteAlias { get; internal set; }
			public List<Program> Programs { get; internal set; }

			public sealed class Program
			{
				internal Program() { }
				public int AdmissionOpenProgramID { get; internal set; }
				public int ProgramID { get; internal set; }
				public string ProgramName { get; internal set; }
				public string ProgramShortName { get; internal set; }
				public string ProgramAlias { get; internal set; }
				public ProgramDurations ProgramDurationEnum { get; internal set; }
				public string EligibilityCriteriaStatement { get; internal set; }
			}
		}

		public static List<EligibilityCriteria> GetEligibilityCriteriaStatements(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				return aspireContext.AdmissionOpenPrograms
					.Where(aop => aop.SemesterID == loginHistory.SemesterID)
					.Where(aop => !loginHistory.ForeignStudent || aop.AdmissionClosingDateForForeigners != null)
					.GroupBy(aop => new
					{
						aop.Program.InstituteID,
						aop.Program.Institute.InstituteName,
						aop.Program.Institute.InstituteShortName,
						aop.Program.Institute.InstituteAlias,
					}).Select(g => new EligibilityCriteria
					{
						InstituteID = g.Key.InstituteID,
						InstituteName = g.Key.InstituteName,
						InstituteShortName = g.Key.InstituteShortName,
						InstituteAlias = g.Key.InstituteAlias,
						Programs = g.Select(aop => new EligibilityCriteria.Program
						{
							AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
							ProgramID = aop.ProgramID,
							ProgramName = aop.Program.ProgramName,
							ProgramShortName = aop.Program.ProgramShortName,
							ProgramAlias = aop.Program.ProgramAlias,
							ProgramDurationEnum = (ProgramDurations)aop.Program.Duration,
							EligibilityCriteriaStatement = aop.EligibilityCriteriaStatement
						}).ToList()
					}).ToList();
			}
		}
	}
}
