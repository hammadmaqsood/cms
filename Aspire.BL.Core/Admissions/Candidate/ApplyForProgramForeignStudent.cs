﻿using Aspire.BL.Core.Admissions.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FeeManagement.Common;
using Aspire.BL.Core.FileServer;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace Aspire.BL.Core.Admissions.Candidate
{
	public static class ApplyForProgramForeignStudent
	{
		public class AdmissionOpenProgram
		{
			internal AdmissionOpenProgram() { }
			public int AdmissionOpenProgramID { get; internal set; }
			public int ProgramID { get; internal set; }
			public int InstituteID { get; internal set; }
			public string InstituteName { get; internal set; }
			public string ProgramName { get; internal set; }
			public DegreeLevels DegreeLevelEnum { get; internal set; }
			public ProgramDurations DurationEnum { get; internal set; }
			public string EligibilityCriteriaStatement { get; internal set; }
			public Shifts ShiftEnumFlags { get; internal set; }
			public DateTime AdmissionOpenDateForForeigners { get; internal set; }
			public DateTime AdmissionClosingDateForForeigners { get; internal set; }
			public bool Applied { get; internal set; }
		}

		public sealed class GetAllInformationForApplyProgramResult
		{
			internal GetAllInformationForApplyProgramResult()
			{
			}

			public enum Statuses
			{
				NotForeignStudent,
				ProfileInformationNotCompleted,
				Success,
			}

			public Statuses Status { get; internal set; }
			public bool EditingAllowed
			{
				get
				{
					if (this.CandidateStatus == Model.Entities.Candidate.Statuses.Completed)
						if (!this.CandidateAppliedPrograms.Any())
							return true;
					return false;
				}
			}
			public bool CanApplyProgram
			{
				get
				{
					var documentTypes = new[] { CandidateForeignDocument.DocumentTypes.Passport, CandidateForeignDocument.DocumentTypes.StatementOfPurpose, CandidateForeignDocument.DocumentTypes.EnglishLanguageCertificate };
					if (this.CandidateStatus == Model.Entities.Candidate.Statuses.Completed)
						if (this.CandidateForeignAcademicRecords.Any())
							if (documentTypes.All(t => this.CandidateForeignDocuments.Any(d => d.DocumentTypeEnum == t)))
								return true;
					return false;
				}
			}
			public List<CandidateForeignAcademicRecord> CandidateForeignAcademicRecords { get; internal set; }
			public List<CandidateForeignDocument> CandidateForeignDocuments { get; internal set; }
			public List<AdmissionOpenProgram> AdmissionOpenPrograms { get; internal set; }
			public List<CandidateAppliedProgram> CandidateAppliedPrograms { get; internal set; }
			public Model.Entities.Candidate.Statuses CandidateStatus { get; internal set; }

			public sealed class CandidateAppliedProgram : AdmissionOpenProgram
			{
				internal CandidateAppliedProgram() { }

				public int CandidateAppliedProgramID { get; internal set; }
				public DateTime ApplyDate { get; internal set; }
				public Shifts ShiftEnum { get; internal set; }
				public bool CreditTransfer { get; internal set; }
				public string ResearchThemeAreaName { get; internal set; }
			}
		}

		public static GetAllInformationForApplyProgramResult GetAllInformationForApplyProgram(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				var result = new GetAllInformationForApplyProgramResult { };

				GetAllInformationForApplyProgramResult Result(GetAllInformationForApplyProgramResult.Statuses status)
				{
					result.Status = status;
					return result;
				}
				if (loginHistory.ForeignStudent == false)
					return Result(GetAllInformationForApplyProgramResult.Statuses.NotForeignStudent);

				var candidate = aspireContext.Candidates.Where(c => c.CandidateID == loginHistory.CandidateID).Select(c => new
				{
					c.CandidateID,
					StatusEnum = (Model.Entities.Candidate.Statuses)c.Status,
				}).Single();

				result.CandidateStatus = candidate.StatusEnum;
				if (candidate.StatusEnum != Model.Entities.Candidate.Statuses.Completed)
					return Result(GetAllInformationForApplyProgramResult.Statuses.ProfileInformationNotCompleted);

				result.CandidateForeignAcademicRecords = aspireContext.CandidateForeignAcademicRecords.Where(r => r.CandidateID == loginHistory.CandidateID).ToList();
				result.CandidateForeignDocuments = aspireContext.CandidateForeignDocuments.Where(r => r.CandidateID == loginHistory.CandidateID).ToList();

				result.AdmissionOpenPrograms = aspireContext.AdmissionOpenPrograms.FilterOpenAdmissions(loginHistory.ForeignStudent)
					.Where(aop => aop.SemesterID == loginHistory.SemesterID)
					.Select(aop => new AdmissionOpenProgram
					{
						AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
						InstituteID = aop.Program.Institute.InstituteID,
						InstituteName = aop.Program.Institute.InstituteName,
						ProgramName = aop.Program.ProgramName,
						DurationEnum = (ProgramDurations)aop.Program.Duration,
						EligibilityCriteriaStatement = aop.EligibilityCriteriaStatement,
						ShiftEnumFlags = (Shifts)aop.Shifts,
						AdmissionOpenDateForForeigners = aop.AdmissionOpenDateForForeigners.Value,
						AdmissionClosingDateForForeigners = aop.AdmissionClosingDateForForeigners.Value,
						Applied = aspireContext.CandidateAppliedPrograms.Any(cap => cap.CandidateID == loginHistory.CandidateID && cap.Choice1AdmissionOpenProgramID == aop.AdmissionOpenProgramID)
					}).ToList();

				var candidateAppliedProgramQuery = from cap in aspireContext.CandidateAppliedPrograms
												   join aop in aspireContext.AdmissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
												   where cap.CandidateID == loginHistory.CandidateID
												   select new GetAllInformationForApplyProgramResult.CandidateAppliedProgram
												   {
													   CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
													   AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
													   ProgramName = aop.Program.ProgramName,
													   InstituteID = aop.Program.InstituteID,
													   InstituteName = aop.Program.Institute.InstituteName,
													   AdmissionOpenDateForForeigners = aop.AdmissionOpenDateForForeigners.Value,
													   AdmissionClosingDateForForeigners = aop.AdmissionClosingDateForForeigners.Value,
													   DegreeLevelEnum = (DegreeLevels)aop.Program.DegreeLevel,
													   DurationEnum = (ProgramDurations)aop.Program.Duration,
													   EligibilityCriteriaStatement = aop.EligibilityCriteriaStatement,
													   ShiftEnumFlags = (Shifts)cap.Shift,
													   ApplyDate = cap.ApplyDate,
													   CreditTransfer = cap.CreditTransfer,
													   ShiftEnum = (Shifts)cap.Shift,
													   ResearchThemeAreaName = cap.ResearchThemeArea.AreaName,
												   };
				result.CandidateAppliedPrograms = candidateAppliedProgramQuery.ToList();
				return Result(GetAllInformationForApplyProgramResult.Statuses.Success);
			}
		}

		public static (AdmissionOpenProgram, List<(Guid ResearchThemeAreaID, string AreaName, string ResearchThemeName)>) GetAdmissionOpenProgram(int admissionOpenProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				var now = DateTime.Now;
				var admissionOpenProgram = aspireContext.AdmissionOpenPrograms.FilterOpenAdmissions(loginHistory.ForeignStudent)
					.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID)
					.Select(aop => new AdmissionOpenProgram
					{
						AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
						ProgramID = aop.ProgramID,
						InstituteID = aop.Program.Institute.InstituteID,
						InstituteName = aop.Program.Institute.InstituteName,
						ProgramName = aop.Program.ProgramName,
						DurationEnum = (ProgramDurations)aop.Program.Duration,
						DegreeLevelEnum = (DegreeLevels)aop.Program.DegreeLevel,
						EligibilityCriteriaStatement = aop.EligibilityCriteriaStatement,
						ShiftEnumFlags = (Shifts)aop.Shifts,
						AdmissionOpenDateForForeigners = aop.AdmissionOpenDateForForeigners.Value,
						AdmissionClosingDateForForeigners = aop.AdmissionClosingDateForForeigners.Value
					}).SingleOrDefault();
				if (admissionOpenProgram == null)
					return (null, null);
				var researchThemeIDs = aspireContext.ProgramResearchThemes.Where(rt => rt.ProgramID == admissionOpenProgram.ProgramID).Select(rt => (Guid)rt.ResearchThemeID);
				var researchThemeAreas = aspireContext.ResearchThemeAreas.Where(rta => researchThemeIDs.Contains(rta.ResearchThemeID)).ToList().Select(rta => ((Guid ResearchThemeAreaID, string AreaName, string ResearchThemeName))(rta.ResearchThemeAreaID, rta.AreaName, rta.ResearchTheme.ResearchThemeName)).ToList();
				return (admissionOpenProgram, researchThemeAreas);
			}
		}

		public enum AddCandidateForeignAcademicRecordStatuses
		{
			NoRecordFound,
			AccountNotValid,
			ErrorRecordLockedDueToProgramApplied,
			ErrorProfileInformationIsNotComplete,
			ErrorTemporaryFileNotFound,
			ErrorDocumentIsNotValid,
			Success,
		}

		public static AddCandidateForeignAcademicRecordStatuses AddCandidateForeignAcademicRecord(string degreeName, string institute, string subjects, string obtainedMarks, string totalMarks, short passingYear, Guid instanceGuid, Guid temporarySystemFileID, string fileName, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				if (loginHistory.ForeignStudent == false)
					return AddCandidateForeignAcademicRecordStatuses.AccountNotValid;

				var candidate = aspireContext.Candidates.Where(c => c.CandidateID == loginHistory.CandidateID).Select(c => new
				{
					StatusEnum = (Model.Entities.Candidate.Statuses)c.Status,
					CandidateAppliedProgramsFound = c.CandidateAppliedPrograms.Any(),
				}).Single();

				if (candidate.StatusEnum != Model.Entities.Candidate.Statuses.Completed)
					return AddCandidateForeignAcademicRecordStatuses.ErrorProfileInformationIsNotComplete;

				if (aspireContext.CandidateAppliedPrograms.Any(cap => cap.CandidateID == loginHistory.CandidateID))
					return AddCandidateForeignAcademicRecordStatuses.ErrorRecordLockedDueToProgramApplied;

				var (systemFile, status) = aspireContext.AddCandidateForeignAcademicRecordDocument(fileName, instanceGuid, temporarySystemFileID, loginSessionGuid);

				switch (status)
				{
					case CandidateForeignAcademicRecordDocuments.AddCandidateForeignAcademicRecordDocumentStatuses.TemporaryFileNotFound:
						return AddCandidateForeignAcademicRecordStatuses.ErrorTemporaryFileNotFound;
					case CandidateForeignAcademicRecordDocuments.AddCandidateForeignAcademicRecordDocumentStatuses.DocumentIsNotValid:
						return AddCandidateForeignAcademicRecordStatuses.ErrorDocumentIsNotValid;
					case CandidateForeignAcademicRecordDocuments.AddCandidateForeignAcademicRecordDocumentStatuses.Success:
						var candidateForeignAcademicRecord = new CandidateForeignAcademicRecord
						{
							CandidateForeignAcademicRecordID = Guid.NewGuid(),
							CandidateID = loginHistory.CandidateID,
							DegreeName = degreeName.TrimAndCannotBeEmpty(),
							Institute = institute.TrimAndCannotBeEmpty(),
							Subjects = subjects.TrimAndCannotBeEmpty(),
							ObtainedMarks = obtainedMarks.TrimAndCannotBeEmpty(),
							TotalMarks = totalMarks.TrimAndCannotBeEmpty(),
							PassingYear = passingYear,
							SystemFileID = systemFile.SystemFileID
						};
						aspireContext.CandidateForeignAcademicRecords.Add(candidateForeignAcademicRecord);
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return AddCandidateForeignAcademicRecordStatuses.Success;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
		}

		public static (SystemFile systemFile, byte[] fileBytes)? DownloadCandidateForeignAcademicRecord(Guid candidateForeignAcademicRecordID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				int? candidateID = null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						candidateID = aspireContext.CandidateForeignAcademicRecords.Single(cfar => cfar.CandidateForeignAcademicRecordID == candidateForeignAcademicRecordID).CandidateID;
						break;
					case UserTypes.Candidate:
						candidateID = loginHistory.CandidateID;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				var candidateForeignAcademicRecordDocument = aspireContext.CandidateForeignAcademicRecords
					.Where(cfar => cfar.CandidateID == candidateID && cfar.CandidateForeignAcademicRecordID == candidateForeignAcademicRecordID)
					.Select(cfar => new
					{
						cfar.SystemFileID
					}).SingleOrDefault();
				if (candidateForeignAcademicRecordDocument == null)
					return null;
				return aspireContext.ReadCandidateForeignAcademicRecordDocument(candidateForeignAcademicRecordDocument.SystemFileID);
			}
		}

		public enum DeleteCandidateForeignAcademicRecordStatuses
		{
			NoRecordFound,
			AccountNotValid,
			ErrorRecordLockedDueToProgramApplied,
			ErrorProfileInformationIsNotComplete,
			Success,
		}

		public static DeleteCandidateForeignAcademicRecordStatuses DeleteCandidateForeignAcademicRecord(Guid candidateForeignAcademicRecordID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				if (loginHistory.ForeignStudent == false)
					return DeleteCandidateForeignAcademicRecordStatuses.AccountNotValid;

				var candidate = aspireContext.Candidates.Where(c => c.CandidateID == loginHistory.CandidateID).Select(c => new
				{
					StatusEnum = (Model.Entities.Candidate.Statuses)c.Status,
					CandidateAppliedProgramsFound = c.CandidateAppliedPrograms.Any(),
					CandidateForeignAcademicRecord = c.CandidateForeignAcademicRecords.FirstOrDefault(cfar => cfar.CandidateForeignAcademicRecordID == candidateForeignAcademicRecordID)
				}).Single();

				if (candidate.StatusEnum != Model.Entities.Candidate.Statuses.Completed)
					return DeleteCandidateForeignAcademicRecordStatuses.ErrorProfileInformationIsNotComplete;

				if (aspireContext.CandidateAppliedPrograms.Any(cap => cap.CandidateID == loginHistory.CandidateID))
					return DeleteCandidateForeignAcademicRecordStatuses.ErrorRecordLockedDueToProgramApplied;
				if (candidate.CandidateForeignAcademicRecord == null)
					return DeleteCandidateForeignAcademicRecordStatuses.NoRecordFound;

				aspireContext.DeleteSystemFile(candidate.CandidateForeignAcademicRecord.SystemFileID, loginHistory.LoginSessionGuid);
				aspireContext.CandidateForeignAcademicRecords.Remove(candidate.CandidateForeignAcademicRecord);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteCandidateForeignAcademicRecordStatuses.Success;
			}
		}

		public enum AddCandidateForeignDocumentStatuses
		{
			NoRecordFound,
			AccountNotValid,
			ErrorRecordLockedDueToProgramApplied,
			ErrorProfileInformationIsNotComplete,
			ErrorTemporaryFileNotFound,
			ErrorDocumentIsNotValid,
			Success,
		}

		public static AddCandidateForeignDocumentStatuses AddCandidateForeignDocument(CandidateForeignDocument.DocumentTypes documentTypeEnum, string fileName, Guid instanceGuid, Guid temporarySystemFileID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				if (loginHistory.ForeignStudent == false)
					return AddCandidateForeignDocumentStatuses.AccountNotValid;

				var candidate = aspireContext.Candidates.Where(c => c.CandidateID == loginHistory.CandidateID).Select(c => new
				{
					StatusEnum = (Model.Entities.Candidate.Statuses)c.Status,
					CandidateAppliedProgramsFound = c.CandidateAppliedPrograms.Any(),
				}).Single();

				if (candidate.StatusEnum != Model.Entities.Candidate.Statuses.Completed)
					return AddCandidateForeignDocumentStatuses.ErrorProfileInformationIsNotComplete;

				if (aspireContext.CandidateAppliedPrograms.Any(cap => cap.CandidateID == loginHistory.CandidateID))
					return AddCandidateForeignDocumentStatuses.ErrorRecordLockedDueToProgramApplied;

				var (systemFile, status) = aspireContext.AddCandidateForeignDocument(fileName, instanceGuid, temporarySystemFileID, loginSessionGuid);

				switch (status)
				{
					case CandidateForeignAcademicRecordDocuments.AddCandidateForeignDocumentStatuses.TemporaryFileNotFound:
						return AddCandidateForeignDocumentStatuses.ErrorTemporaryFileNotFound;
					case CandidateForeignAcademicRecordDocuments.AddCandidateForeignDocumentStatuses.DocumentIsNotValid:
						return AddCandidateForeignDocumentStatuses.ErrorDocumentIsNotValid;
					case CandidateForeignAcademicRecordDocuments.AddCandidateForeignDocumentStatuses.Success:
						var candidateForeignDocument = new CandidateForeignDocument
						{
							CandidateForeignDocumentID = Guid.NewGuid(),
							CandidateID = loginHistory.CandidateID,
							DocumentTypeEnum = documentTypeEnum,
							SystemFileID = systemFile.SystemFileID
						};
						aspireContext.CandidateForeignDocuments.Add(candidateForeignDocument);
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return AddCandidateForeignDocumentStatuses.Success;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
		}

		public enum DeleteCandidateForeignDocumentStatuses
		{
			NoRecordFound,
			AccountNotValid,
			ErrorRecordLockedDueToProgramApplied,
			ErrorProfileInformationIsNotComplete,
			Success,
		}

		public static DeleteCandidateForeignDocumentStatuses DeleteCandidateForeignDocument(Guid candidateForeignDocumentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				if (loginHistory.ForeignStudent == false)
					return DeleteCandidateForeignDocumentStatuses.AccountNotValid;

				var candidate = aspireContext.Candidates.Where(c => c.CandidateID == loginHistory.CandidateID).Select(c => new
				{
					StatusEnum = (Model.Entities.Candidate.Statuses)c.Status,
					CandidateAppliedProgramsFound = c.CandidateAppliedPrograms.Any(),
					CandidateForeignDocument = c.CandidateForeignDocuments.FirstOrDefault(cfd => cfd.CandidateForeignDocumentID == candidateForeignDocumentID)
				}).Single();

				if (candidate.StatusEnum != Model.Entities.Candidate.Statuses.Completed)
					return DeleteCandidateForeignDocumentStatuses.ErrorProfileInformationIsNotComplete;

				if (aspireContext.CandidateAppliedPrograms.Any(cap => cap.CandidateID == loginHistory.CandidateID))
					return DeleteCandidateForeignDocumentStatuses.ErrorRecordLockedDueToProgramApplied;
				if (candidate.CandidateForeignDocument == null)
					return DeleteCandidateForeignDocumentStatuses.NoRecordFound;

				aspireContext.DeleteSystemFile(candidate.CandidateForeignDocument.SystemFileID, loginHistory.LoginSessionGuid);
				aspireContext.CandidateForeignDocuments.Remove(candidate.CandidateForeignDocument);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteCandidateForeignDocumentStatuses.Success;
			}
		}

		public static (SystemFile systemFile, byte[] fileBytes)? DownloadCandidateForeignDocument(Guid candidateForeignDocumentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				int? candidateID = null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						candidateID = aspireContext.CandidateForeignDocuments.Single(cfd => cfd.CandidateForeignDocumentID == candidateForeignDocumentID).CandidateID;
						break;
					case UserTypes.Candidate:
						candidateID = loginHistory.CandidateID;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				var candidateForeignDocument = aspireContext.CandidateForeignDocuments
					.Where(cfd => cfd.CandidateID == candidateID && cfd.CandidateForeignDocumentID == candidateForeignDocumentID)
					.Select(cfar => new
					{
						cfar.SystemFileID
					}).SingleOrDefault();
				if (candidateForeignDocument == null)
					return null;
				return aspireContext.ReadCandidateForeignDocument(candidateForeignDocument.SystemFileID);
			}
		}

		public enum ApplyStatuses
		{
			NoRecordFound,
			AlreadyApplied,
			Success,
			AccountIsNotValid,
			ProfileInformationIsBlank,
			ProfileInformationPictureNotUploaded,
			PassportNotUploaded,
			StatementOfPurposeNotUploaded,
			EnglishLanguageCertificateNotUploaded,
			ResearchPlanNotUploaded,
			AcademicRecordNotAdded
		}

		public static ApplyStatuses Apply(int choice1AdmissionOpenProgramID, Guid? researchThemeAreaID, Shifts shiftEnum, bool creditTransfer, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				if (!loginHistory.ForeignStudent)
					return ApplyStatuses.AccountIsNotValid;

				var record = aspireContext.Candidates
					.Where(c => c.CandidateID == loginHistory.CandidateID)
					.Select(c => new
					{
						Candidate = c,
						AlreadyApplied = c.CandidateAppliedPrograms.Any(cap => cap.Choice1AdmissionOpenProgramID == choice1AdmissionOpenProgramID),
						CandidateForeignAcademicRecordsAdded = c.CandidateForeignAcademicRecords.Any(),
						DocumentTypes = c.CandidateForeignDocuments.Select(cfd => cfd.DocumentType).ToList(),
					})
					.Single();

				var choice1AdmissionOpenProgram = aspireContext.AdmissionOpenPrograms
					.Where(aop => aop.AdmissionOpenProgramID == choice1AdmissionOpenProgramID && aop.SemesterID == record.Candidate.SemesterID)
					.Select(aop => new
					{
						aop.AdmissionOpenProgramID,
						DegreeLevelsEnum = (DegreeLevels)aop.Program.DegreeLevel,
						aop.AdmissionProcessingFee,
						aop.EntryTestType,
						aop.EntryTestStartDate,
						aop.EntryTestEndDate,
						aop.EntryTestDuration,
						aop.AdmissionCriteriaID,
					}).SingleOrDefault();
				if (choice1AdmissionOpenProgram == null)
					return ApplyStatuses.NoRecordFound;

				switch (record.Candidate.StatusEnum)
				{
					case Model.Entities.Candidate.Statuses.Blank:
					case Model.Entities.Candidate.Statuses.PictureUploaded:
						return ApplyStatuses.ProfileInformationIsBlank;
					case Model.Entities.Candidate.Statuses.InformationProvided:
						return ApplyStatuses.ProfileInformationPictureNotUploaded;
					case Model.Entities.Candidate.Statuses.Completed:
						break;
					default:
						throw new NotImplementedEnumException(record.Candidate.StatusEnum);
				}
				if (record.AlreadyApplied)
					return ApplyStatuses.AlreadyApplied;

				if (!record.CandidateForeignAcademicRecordsAdded)
					return ApplyStatuses.AcademicRecordNotAdded;
				if (!record.DocumentTypes.Any(t => t == CandidateForeignDocument.DocumentTypes.Passport.GetGuid()))
					return ApplyStatuses.PassportNotUploaded;
				if (!record.DocumentTypes.Any(t => t == CandidateForeignDocument.DocumentTypes.StatementOfPurpose.GetGuid()))
					return ApplyStatuses.StatementOfPurposeNotUploaded;
				if (!record.DocumentTypes.Any(t => t == CandidateForeignDocument.DocumentTypes.EnglishLanguageCertificate.GetGuid()))
					return ApplyStatuses.EnglishLanguageCertificateNotUploaded;
				if (choice1AdmissionOpenProgram.DegreeLevelsEnum == DegreeLevels.Doctorate && !record.DocumentTypes.Any(t => t == CandidateForeignDocument.DocumentTypes.ResearchPlan.GetGuid()))
					return ApplyStatuses.ResearchPlanNotUploaded;

				var candidateAppliedProgram = new CandidateAppliedProgram
				{
					CandidateID = loginHistory.CandidateID,
					ChallanNo = aspireContext.GetNextAdmissionProcessingFeeChallanNo().Single() ?? throw new InvalidOperationException("Null is returned for Next Challan No."),
					ApplicationNo = aspireContext.GetNextApplicationNo(choice1AdmissionOpenProgram.AdmissionOpenProgramID),
					ResearchThemeAreaID = researchThemeAreaID,
					Choice1AdmissionOpenProgramID = choice1AdmissionOpenProgramID,
					Amount = 0,
					ShiftEnum = shiftEnum,
					Shift = (byte)shiftEnum,
					CreditTransfer = creditTransfer,
					Status = (byte)CandidateAppliedProgram.Statuses.None,
					StatusEnum = CandidateAppliedProgram.Statuses.None,
					ApplyDate = DateTime.Now,
					AdmissionCriteriaPreReqID = null,
					Choice2AdmissionOpenProgramID = null,
					Choice3AdmissionOpenProgramID = null,
					Choice4AdmissionOpenProgramID = null,
					EntryTestDate = null,
					TestDuration = null,
					PreReqDegree = null,
					ResultAwaitedDegreeType = null,
					ResultAwaitedDegreeTypeEnum = null,
					AccountTransactionID = null,
					ETSType = null,
					ETSTypeEnum = null,
					ETSObtained = null,
					ETSTotal = null,
					InterviewDate = null,
					InterviewRemarks = null,
					AdmissionFeeLastDate = null,
					AdmittedAdmissionOpenProgramID = null,
					AdmittedShift = null,
					AdmittedShiftEnum = null,
					EnrollmentGenerated = null,
					StudentID = null,
					EntryTestMarksPercentage = null,
					CBTSessionLabID = null,
					StatusDate = null,
					DeferredDate = null,
					DeferredToAdmittedAdmissionOpenProgramID = null,
					DeferredToShift = null,
					DeferredToShiftEnum = null,
				};

				record.Candidate.EditStatusEnum = Model.Entities.Candidate.EditStatuses.CanNotEditBasicInfo;
				record.Candidate.CandidateAppliedPrograms.Add(candidateAppliedProgram);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return ApplyStatuses.Success;
			}
		}
	}
}
