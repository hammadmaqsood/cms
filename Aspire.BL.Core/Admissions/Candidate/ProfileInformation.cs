﻿using Aspire.BL.Core.Common.EmailsManagement;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Candidate
{
	public static class ProfileInformation
	{
		public sealed class GenerateResetPasswordCodeResult
		{
			public enum Statuses
			{
				AdmissionNotOpen,
				NoRecordFound,
				Success
			}

			internal GenerateResetPasswordCodeResult() { }
			public Statuses Status { get; internal set; }
			public Candidate CandidateInfo { get; internal set; }

			public sealed class Candidate
			{
				internal Candidate() { }
				public int CandidateID { get; internal set; }
				public Guid ConfirmationCode { get; internal set; }
				public string Name { get; internal set; }
				public short SemesterID { get; internal set; }
				public DateTime ConfirmationCodeExpiryDate { get; internal set; }
				public string Email { get; internal set; }
			}
		}

		public static GenerateResetPasswordCodeResult GenerateResetPasswordCode(string email, short semesterID, string pageURL)
		{
			email = email.ValidateEmailAddress().CannotBeEmptyOrWhitespace();
			using (var aspireContext = new AspireContext(true))
			{
				if (!aspireContext.GetCandidateLoginAllowedSemesterIDsQuery().Contains(semesterID))
					return new GenerateResetPasswordCodeResult { Status = GenerateResetPasswordCodeResult.Statuses.AdmissionNotOpen };
				var candidate = aspireContext.Candidates.SingleOrDefault(c => c.Email == email && c.SemesterID == semesterID);
				if (candidate == null)
					return new GenerateResetPasswordCodeResult { Status = GenerateResetPasswordCodeResult.Statuses.NoRecordFound };
				var createdDate = DateTime.Now;
				var expiryDate = createdDate.AddDays(1);
				var userLoginHistoryID = UserLoginHistory.SystemUserLoginHistoryID;

				candidate.ConfirmationCode = Guid.NewGuid();
				candidate.ConfirmationCodeExpiryDate = expiryDate;
				aspireContext.SaveChangesAsSystemUser();

				var confirmationLink = $"{pageURL}?C={candidate.ConfirmationCode}";
				var subject = "Reset Password";
				var emailBody = Properties.Resources.CandidateResetPasswordVerificationEmail;
				emailBody = emailBody.Replace("{LinkHtmlAttributeEncoded}", confirmationLink.HtmlAttributeEncode());
				emailBody = emailBody.Replace("{LinkHtmlEncoded}", confirmationLink.HtmlEncode());
				emailBody = emailBody.Replace("{NameHtmlEncoded}", candidate.Name.HtmlEncode());
				emailBody = emailBody.Replace("{SemesterHtmlEncoded}", candidate.SemesterID.ToSemesterString().HtmlEncode());
				emailBody = emailBody.Replace("{ConfirmationLinkExpiryDate}", expiryDate.ToString("F").HtmlEncode());
				var toList = new[] { (candidate.Name, candidate.Email) };
				aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.CandidateForgotPassword, subject, emailBody, true, Email.Priorities.Highest, createdDate, expiryDate, userLoginHistoryID, toList: toList);
				aspireContext.CommitTransaction();
				return new GenerateResetPasswordCodeResult
				{
					Status = GenerateResetPasswordCodeResult.Statuses.Success,
					CandidateInfo = new GenerateResetPasswordCodeResult.Candidate
					{
						CandidateID = candidate.CandidateID,
						SemesterID = candidate.SemesterID,
						Name = candidate.Name,
						Email = candidate.Email,
						ConfirmationCode = candidate.ConfirmationCode.Value,
						ConfirmationCodeExpiryDate = candidate.ConfirmationCodeExpiryDate.Value
					},
				};
			}
		}

		public sealed class GetCandidateInfoForResetPasswordResult
		{
			public enum Statuses
			{
				NoRecordFound,
				ConfirmationCodeExpired,
				LoginNotAllowedForSemester
			}

			public sealed class Candidate
			{
				internal Candidate()
				{
				}

				public int CandidateID { get; internal set; }
				public string Email { get; internal set; }
				public string Name { get; internal set; }
				public short SemesterID { get; internal set; }
				public DateTime? ConfirmationCodeExpiryDate { get; internal set; }
			}
			internal GetCandidateInfoForResetPasswordResult() { }
			public Statuses? Status { get; internal set; }
			public Candidate CandidateInfo { get; internal set; }
		}

		public static GetCandidateInfoForResetPasswordResult GetCandidateInfoForResetPassword(Guid confirmationCode)
		{
			using (var aspireContext = new AspireContext())
			{
				var candidate = aspireContext.Candidates.Where(c => c.ConfirmationCode == confirmationCode)
					.Select(c => new GetCandidateInfoForResetPasswordResult.Candidate
					{
						CandidateID = c.CandidateID,
						ConfirmationCodeExpiryDate = c.ConfirmationCodeExpiryDate.Value,
						Name = c.Name,
						Email = c.Email,
						SemesterID = c.SemesterID,
					}).SingleOrDefault();

				if (candidate == null)
					return new GetCandidateInfoForResetPasswordResult { Status = GetCandidateInfoForResetPasswordResult.Statuses.NoRecordFound };
				if (candidate.ConfirmationCodeExpiryDate < DateTime.Now)
					return new GetCandidateInfoForResetPasswordResult { Status = GetCandidateInfoForResetPasswordResult.Statuses.ConfirmationCodeExpired };

				if (!aspireContext.GetCandidateLoginAllowedSemesterIDsQuery().Contains(candidate.SemesterID))
					return new GetCandidateInfoForResetPasswordResult { Status = GetCandidateInfoForResetPasswordResult.Statuses.LoginNotAllowedForSemester };

				return new GetCandidateInfoForResetPasswordResult { Status = null, CandidateInfo = candidate };
			}
		}

		public sealed class ResetPasswordResult
		{
			public enum Statuses
			{
				NoRecordFound,
				ConfirmationCodeHasBeenExpired,
				LoginNotAllowedForSemester,
				Success
			}

			public sealed class Candidate
			{
				internal Candidate() { }

				public int CandidateID { get; internal set; }
				public string Email { get; internal set; }
				public string Name { get; internal set; }
			}

			internal ResetPasswordResult() { }
			public Statuses Status { get; internal set; }
			public Candidate CandidateInfo { get; internal set; }
		}

		public static ResetPasswordResult ResetPassword(Guid confirmationCode, string newPassword)
		{
			newPassword = newPassword.ValidatePassword();
			using (var aspireContext = new AspireContext(true))
			{
				var candidate = aspireContext.Candidates.SingleOrDefault(c => c.ConfirmationCode == confirmationCode);
				if (candidate == null)
					return new ResetPasswordResult { Status = ResetPasswordResult.Statuses.NoRecordFound };
				if (candidate.ConfirmationCodeExpiryDate < DateTime.Now)
					return new ResetPasswordResult { Status = ResetPasswordResult.Statuses.ConfirmationCodeHasBeenExpired };
				if (!aspireContext.GetCandidateLoginAllowedSemesterIDsQuery().Contains(candidate.SemesterID))
					return new ResetPasswordResult { Status = ResetPasswordResult.Statuses.LoginNotAllowedForSemester };

				var createdDate = DateTime.Now;
				candidate.ConfirmationCode = null;
				candidate.ConfirmationCodeExpiryDate = null;
				var oldHashedPassword = candidate.Password;
				candidate.Password = newPassword.EncryptPassword();
				aspireContext.PasswordChangeHistories.Add(new PasswordChangeHistory
				{
					CandidateID = candidate.CandidateID,
					UserLoginHistoryID = UserLoginHistory.SystemUserLoginHistoryID,
					ChangedDate = createdDate,
					ChangeReasonEnum = PasswordChangeReasons.UsingResetLink,
					FacultyMemberID = null,
					UserID = null,
					StudentID = null,
					StudentIDOfParent = null,
					OldPassword = oldHashedPassword,
					NewPassword = candidate.Password
				});
				aspireContext.SaveChangesAsSystemUser();

				var subject = "Password Changed";
				var emailBody = Properties.Resources.CandidatePasswordHasBeenReset;
				emailBody = emailBody.Replace("{NameHtmlEncoded}", candidate.Name.HtmlEncode());
				var expiryDate = (DateTime?)null;
				var toList = new[] { (candidate.Name, candidate.Email) };
				aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.CandidatePasswordHasBeenReset, subject, emailBody, true, Email.Priorities.Normal, createdDate, expiryDate, UserLoginHistory.SystemUserLoginHistoryID, toList: toList);
				aspireContext.CommitTransaction();

				return new ResetPasswordResult
				{
					Status = ResetPasswordResult.Statuses.Success,
					CandidateInfo = new ResetPasswordResult.Candidate
					{
						CandidateID = candidate.CandidateID,
						Name = candidate.Name,
						Email = candidate.Email,
					}
				};
			}
		}

		public static Model.Entities.Candidate GetCandidate(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				return aspireContext.Candidates.Single(c => c.CandidateID == loginHistory.CandidateID);
			}
		}

		public static void UpdateCandidate(Model.Entities.Candidate candidate, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				candidate.Validate();
				var candidateDB = aspireContext.Candidates.Single(c => c.CandidateID == loginHistory.CandidateID);
				candidateDB.CopyValues(candidate);
				switch (candidateDB.StatusEnum)
				{
					case Model.Entities.Candidate.Statuses.Blank:
						candidateDB.StatusEnum = Model.Entities.Candidate.Statuses.InformationProvided;
						break;
					case Model.Entities.Candidate.Statuses.PictureUploaded:
						candidateDB.StatusEnum |= Model.Entities.Candidate.Statuses.InformationProvided;
						break;
					case Model.Entities.Candidate.Statuses.InformationProvided:
						break;
					case Model.Entities.Candidate.Statuses.Completed:
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
			}
		}

		public enum ChangePasswordResult
		{
			Success,
			CurrentPasswordDonotMatch,
			PasswordMustBeDifferentFromOldPassword,
			NoRecordFound,
		}

		public static ChangePasswordResult ChangePassword(string oldPassword, string newPassword, Guid loginSessionGuid)
		{
			oldPassword = oldPassword.ValidatePassword();
			newPassword = newPassword.ValidatePassword();
			if (oldPassword == newPassword)
				return ChangePasswordResult.PasswordMustBeDifferentFromOldPassword;
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
				var candidate = aspireContext.Candidates.SingleOrDefault(c => c.CandidateID == loginHistory.CandidateID);
				if (candidate == null)
					return ChangePasswordResult.NoRecordFound;
				if (!oldPassword.CheckPassword(candidate.Password))
					return ChangePasswordResult.CurrentPasswordDonotMatch;
				{
					var oldHashedPassword = candidate.Password;
					candidate.Password = newPassword.EncryptPassword();
					candidate.PasswordChangeHistories.Add(new PasswordChangeHistory
					{
						CandidateID = loginHistory.CandidateID,
						ChangedDate = DateTime.Now,
						ChangeReasonEnum = PasswordChangeReasons.ByUserItSelf,
						UserLoginHistoryID = loginHistory.UserLoginHistoryID,
						StudentID = null,
						FacultyMemberID = null,
						StudentIDOfParent = null,
						UserID = null,
						OldPassword = oldHashedPassword,
						NewPassword = candidate.Password
					});
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
					return ChangePasswordResult.Success;
				}
			}
		}
	}
}