﻿using Aspire.BL.Core.Admissions.Common;
using Aspire.BL.Core.Common.EmailsManagement;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Candidate
{
	public static class Registration
	{
		public sealed class RegisterCandidateResult
		{
			public enum Statuses
			{
				AdmissionsNotOpen,
				EmailAlreadyRegistered,
				Success,
				SuccessAlreadyExists
			}

			internal RegisterCandidateResult() { }
			public Statuses Status { get; internal set; }
			public CandidateTemp CandidateTemp { get; set; }
		}

		public static RegisterCandidateResult RegisterCandidate(string email, string name, bool foreignStudent, short semesterID, string pageURL)
		{
			email = email.MustBeEmailAddress();
			name = name.TrimAndCannotEmptyAndMustBeUpperCase();
			using (var aspireContext = new AspireContext(true))
			{
				if (!aspireContext.AdmissionOpenPrograms.FilterOpenAdmissions(foreignStudent).Any(aop => aop.SemesterID == semesterID))
					return new RegisterCandidateResult { Status = RegisterCandidateResult.Statuses.AdmissionsNotOpen };

				if (aspireContext.Candidates.Any(c => c.Email == email && c.SemesterID == semesterID))
					return new RegisterCandidateResult { Status = RegisterCandidateResult.Statuses.EmailAlreadyRegistered };

				var candidateTemp = aspireContext.CandidateTemps.SingleOrDefault(c => c.Email == email && c.SemesterID == semesterID);
				var createdDate = DateTime.Now;
				var expiryDate = createdDate.AddDays(1);
				var userLoginHistoryID = UserLoginHistory.SystemUserLoginHistoryID;
				var alreadyExists = candidateTemp != null;
				if (candidateTemp != null)
				{
					candidateTemp.CodeExpiryDate = expiryDate;
					candidateTemp.ForeignStudent = foreignStudent;
					aspireContext.SaveChangesAsSystemUser();
				}
				else
				{
					candidateTemp = new CandidateTemp
					{
						Email = email,
						Name = name,
						CodeGeneratedDate = createdDate,
						CodeExpiryDate = expiryDate,
						VerificationCode = Guid.NewGuid(),
						SemesterID = semesterID,
						ForeignStudent = foreignStudent
					};
					candidateTemp = aspireContext.CandidateTemps.Add(candidateTemp.Validate());
					aspireContext.SaveChangesAsSystemUser();
				}

				var confirmationLink = $"{pageURL}?C={candidateTemp.VerificationCode}";
				var subject = "Registration Verification";
				var emailBody = Properties.Resources.CandidateRegistrationVerificationEmail;
				emailBody = emailBody.Replace("{LinkHtmlAttributeEncoded}", confirmationLink.HtmlAttributeEncode());
				emailBody = emailBody.Replace("{LinkHtmlEncoded}", confirmationLink.HtmlEncode());
				emailBody = emailBody.Replace("{NameHtmlEncoded}", candidateTemp.Name.HtmlEncode());
				emailBody = emailBody.Replace("{SemesterHtmlEncoded}", candidateTemp.SemesterID.ToSemesterString().HtmlEncode());
				emailBody = emailBody.Replace("{ConfirmationLinkExpiryDate}", candidateTemp.CodeExpiryDate.ToString("F").HtmlEncode());
				var toList = new[] { (candidateTemp.Name, candidateTemp.Email) };

				aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.CandidateRegistration, subject, emailBody, true, Email.Priorities.Highest, createdDate, expiryDate, userLoginHistoryID, toList: toList);
				aspireContext.CommitTransaction();
				return new RegisterCandidateResult
				{
					Status = alreadyExists ? RegisterCandidateResult.Statuses.SuccessAlreadyExists : RegisterCandidateResult.Statuses.Success,
					CandidateTemp = candidateTemp,
				};
			}
		}

		public static CandidateTemp GetCandidateTemp(Guid verificationCode)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.CandidateTemps.SingleOrDefault(c => c.VerificationCode == verificationCode);
			}
		}

		public sealed class ConfirmRegistrationResult
		{
			public enum Statuses
			{
				NoRecordFound,
				EmailAlreadyRegisteredAndConfirmed,
				AdmissionNotOpen,
				RegisteredSuccessfully,
			}

			internal ConfirmRegistrationResult()
			{
			}
			public Statuses Status { get; internal set; }
			public Model.Entities.Candidate Candidate { get; internal set; }
		}

		public static ConfirmRegistrationResult ConfirmRegistration(Guid confirmationCode, string name, string password)
		{
			name = name.TrimAndCannotEmptyAndMustBeUpperCase();
			password = password.CannotBeEmptyOrWhitespace();
			using (var aspireContext = new AspireContext(true))
			{
				var candidateTemp = aspireContext.CandidateTemps.SingleOrDefault(c => c.VerificationCode == confirmationCode && c.CodeExpiryDate >= DateTime.Now);
				if (candidateTemp == null)
					return new ConfirmRegistrationResult { Status = ConfirmRegistrationResult.Statuses.NoRecordFound };

				var email = candidateTemp.Email.ValidateEmailAddress().CannotBeEmptyOrWhitespace();

				if (aspireContext.Candidates.Any(c => c.SemesterID == candidateTemp.SemesterID && c.Email == email))
					throw new InvalidOperationException();

				if (!aspireContext.GetAdmissionOpenPrograms(candidateTemp.SemesterID).Any())
					return new ConfirmRegistrationResult { Status = ConfirmRegistrationResult.Statuses.AdmissionNotOpen };

				var createdDate = DateTime.Now;
				var candidate = aspireContext.Candidates.Add(new Model.Entities.Candidate
				{
					Email = candidateTemp.Email.MustBeEmailAddress(),
					Name = name.TrimAndCannotEmptyAndMustBeUpperCase(),
					SemesterID = candidateTemp.SemesterID,
					Password = password.EncryptPassword(),
					EditStatusEnum = Model.Entities.Candidate.EditStatuses.CanEdit,
					StatusEnum = Model.Entities.Candidate.Statuses.Blank,
					IsFatherAlive = true,
					ForeignStudent = candidateTemp.ForeignStudent
				});
				candidate.PasswordChangeHistories.Add(new PasswordChangeHistory
				{
					ChangedDate = createdDate,
					ChangeReasonEnum = PasswordChangeReasons.SetDuringRegistration,
					UserLoginHistoryID = UserLoginHistory.SystemUserLoginHistoryID,
					FacultyMemberID = null,
					UserID = null,
					StudentID = null,
					StudentIDOfParent = null,
					OldPassword = null,
					NewPassword = candidate.Password
				});
				aspireContext.CandidateTemps.Remove(candidateTemp);
				aspireContext.SaveChangesAsSystemUser();

				var emailBody = Properties.Resources.CandidateWelcomeEmail;
				emailBody = emailBody.Replace("{NameHtmlEncoded}", candidate.Name.HtmlEncode());
				var subject = "Welcome to Bahria University";
				var toList = new[] { (candidate.Name, candidate.Email) };
				var expiryDate = (DateTime?)null;
				aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.CandidateWelcomeEmail, subject, emailBody, true, Email.Priorities.Normal, createdDate, expiryDate, UserLoginHistory.SystemUserLoginHistoryID, toList: toList);
				aspireContext.CommitTransaction();
				return new ConfirmRegistrationResult
				{
					Status = ConfirmRegistrationResult.Statuses.RegisteredSuccessfully,
					Candidate = candidate
				};
			}
		}
	}
}
