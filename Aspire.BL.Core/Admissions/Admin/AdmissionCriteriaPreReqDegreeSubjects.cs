﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Admissions.Admin
{
	public static class AdmissionCriteriaPreReqDegreeSubjects
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Admissions.ManageAdmissionCriteria, UserGroupPermission.PermissionValues.Allowed);
		}

		#region Admission Criteria Pre Req Degree Subjects

		public enum AddAdmissionCriteriaPreReqDegreeSubjectStatuses
		{
			Success,
		}

		public static AddAdmissionCriteriaPreReqDegreeSubjectStatuses AddAdmissionCriteriaPreReqDegreeSubject(int admissionCriteriaPreReqDegreeID, string subjects, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var admissionCriteriaPreReqDegreeSubject = new AdmissionCriteriaPreReqDegreeSubject
				{
					AdmissionCriteriaPreReqDegreeID = admissionCriteriaPreReqDegreeID,
					Subjects = subjects,
				};
				admissionCriteriaPreReqDegreeSubject.Validate();
				aspireContext.AdmissionCriteriaPreReqDegreeSubjects.Add(admissionCriteriaPreReqDegreeSubject);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddAdmissionCriteriaPreReqDegreeSubjectStatuses.Success;
			}
		}

		public enum UpdateAdmissionCriteriaPreReqDegreeSubjectStatuses
		{
			NoRecordFound,
			Success,
		}

		public static UpdateAdmissionCriteriaPreReqDegreeSubjectStatuses UpdateAdmissionCriteriaPreReqDegreeSubject(int? admissionCriteriaPreReqDegSubjectID, string subjects, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var admissionCriteriaPreReqDegSubject = aspireContext.AdmissionCriteriaPreReqDegreeSubjects.SingleOrDefault(acPreReqDegSub => acPreReqDegSub.AdmissionCriteriaPreReqDegreeSubjectID == admissionCriteriaPreReqDegSubjectID);
				if (admissionCriteriaPreReqDegSubject == null)
					return UpdateAdmissionCriteriaPreReqDegreeSubjectStatuses.NoRecordFound;

				admissionCriteriaPreReqDegSubject.Subjects = subjects;
				admissionCriteriaPreReqDegSubject.Validate();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateAdmissionCriteriaPreReqDegreeSubjectStatuses.Success;
			}
		}

		public enum DeleteAdmissionCriteriaPreReqDegreeSubjectStatuses
		{
			NoRecordFound,
			Success,
		}

		public static DeleteAdmissionCriteriaPreReqDegreeSubjectStatuses DeleteAdmissionCriteriaPreReqDegreeSubject(int admissionCriteriaPreReqDegSubjectID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var admissionCriteriaPreReqDegSub = aspireContext.AdmissionCriteriaPreReqDegreeSubjects.SingleOrDefault(acPreReqDegSub => acPreReqDegSub.AdmissionCriteriaPreReqDegreeSubjectID == admissionCriteriaPreReqDegSubjectID);
				if (admissionCriteriaPreReqDegSub == null)
					return DeleteAdmissionCriteriaPreReqDegreeSubjectStatuses.NoRecordFound;
				aspireContext.AdmissionCriteriaPreReqDegreeSubjects.Remove(admissionCriteriaPreReqDegSub);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteAdmissionCriteriaPreReqDegreeSubjectStatuses.Success;
			}
		}

		public static AdmissionCriteriaPreReqDegreeSubject GetAdmissionCriteriaPreReqDegreeSubject(int admissionCriteriaPreReqDegreeSubjectID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.AdmissionCriteriaPreReqDegreeSubjects.SingleOrDefault(acPreReqDegSub => acPreReqDegSub.AdmissionCriteriaPreReqDegreeSubjectID == admissionCriteriaPreReqDegreeSubjectID);
			}
		}

		public static List<AdmissionCriteriaPreReqDegreeSubject> GetAdmissionCriteriaPreReqDegreeSubjects(int admissionCriteriaPreReqDegreeID, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = aspireContext.AdmissionCriteriaPreReqDegreeSubjects.AsQueryable();
				query = query.Where(q => q.AdmissionCriteriaPreReqDegreeID == admissionCriteriaPreReqDegreeID);
				switch (sortExpression)
				{
					case nameof(AdmissionCriteriaPreReqDegreeSubject.Subjects):
						query = query.OrderBy(sortDirection, q => q.Subjects);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return query.ToList();
			}
		}

		#endregion
	}
}
