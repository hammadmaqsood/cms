﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Admissions.Admin
{
	public static class AdmissionOpenPrograms
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Admissions.ManageAdmissionOpening, UserGroupPermission.PermissionValues.Allowed);
		}

		#region Admission Open Program

		public static Model.Entities.AdmissionOpenProgram.ValidationResults AddAdmissionOpenProgram(Model.Entities.AdmissionOpenProgram admissionOpenProgram, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var alreadyExists = aspireContext.AdmissionOpenPrograms.Any(aop => aop.SemesterID == admissionOpenProgram.SemesterID && aop.ProgramID == admissionOpenProgram.ProgramID);
				if (alreadyExists)
					return Model.Entities.AdmissionOpenProgram.ValidationResults.AlreadyExists;

				var validationResult = admissionOpenProgram.Validate();
				if (validationResult != Model.Entities.AdmissionOpenProgram.ValidationResults.Success)
					return validationResult;
				admissionOpenProgram.ExamSystemTypeEnum = ExamSystemTypes.SemesterSystem;
				aspireContext.AdmissionOpenPrograms.Add(admissionOpenProgram);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return validationResult;
			}
		}

		public static Model.Entities.AdmissionOpenProgram.ValidationResults UpdateAdmissionOpenProgram(Model.Entities.AdmissionOpenProgram admissionOpenProgram, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var dbAdmissionOpenProgram = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Programs).SelectMany(p => p.AdmissionOpenPrograms)
					.SingleOrDefault(aop => aop.AdmissionOpenProgramID == admissionOpenProgram.AdmissionOpenProgramID);
				if (dbAdmissionOpenProgram == null)
					return Model.Entities.AdmissionOpenProgram.ValidationResults.NoRecordFound;

				dbAdmissionOpenProgram.IsSummerRegularSemester = admissionOpenProgram.IsSummerRegularSemester;
				dbAdmissionOpenProgram.FinalSemesterID = admissionOpenProgram.FinalSemesterID;
				dbAdmissionOpenProgram.MaxSemesterID = admissionOpenProgram.MaxSemesterID;
				dbAdmissionOpenProgram.Shifts = admissionOpenProgram.Shifts;
				dbAdmissionOpenProgram.AdmissionCriteriaID = admissionOpenProgram.AdmissionCriteriaID;
				dbAdmissionOpenProgram.EligibilityCriteriaStatement = admissionOpenProgram.EligibilityCriteriaStatement;
				dbAdmissionOpenProgram.AdmissionOpenDate = admissionOpenProgram.AdmissionOpenDate;
				dbAdmissionOpenProgram.AdmissionClosingDate = admissionOpenProgram.AdmissionClosingDate;
				dbAdmissionOpenProgram.AdmissionProcessingFee = admissionOpenProgram.AdmissionProcessingFee;
				dbAdmissionOpenProgram.AdmissionProcessingFeeDueDate = admissionOpenProgram.AdmissionProcessingFeeDueDate;
				dbAdmissionOpenProgram.EntryTestType = admissionOpenProgram.EntryTestType;
				dbAdmissionOpenProgram.EntryTestDuration = admissionOpenProgram.EntryTestDuration;
				dbAdmissionOpenProgram.EntryTestStartDate = admissionOpenProgram.EntryTestStartDate;
				dbAdmissionOpenProgram.EntryTestEndDate = admissionOpenProgram.EntryTestEndDate;
				dbAdmissionOpenProgram.InterviewsStartDate = admissionOpenProgram.InterviewsStartDate;
				dbAdmissionOpenProgram.InterviewsEndDate = admissionOpenProgram.InterviewsEndDate;
				dbAdmissionOpenProgram.LoginAllowedEndDate = admissionOpenProgram.LoginAllowedEndDate;
				dbAdmissionOpenProgram.DefaultExamMarksPolicyID = admissionOpenProgram.DefaultExamMarksPolicyID;
				dbAdmissionOpenProgram.ExamRemarksPolicyID = admissionOpenProgram.ExamRemarksPolicyID;
				dbAdmissionOpenProgram.AdmissionOpenDateForForeigners = admissionOpenProgram.AdmissionOpenDateForForeigners;
				dbAdmissionOpenProgram.AdmissionClosingDateForForeigners = admissionOpenProgram.AdmissionClosingDateForForeigners;
				dbAdmissionOpenProgram.IntakeTargetStudentsCount = admissionOpenProgram.IntakeTargetStudentsCount;

				var validationResult = dbAdmissionOpenProgram.Validate();
				if (validationResult != Model.Entities.AdmissionOpenProgram.ValidationResults.Success)
					return validationResult;

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return validationResult;
			}
		}

		public enum DeleteAdmissionOpenProgramStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteAdmissionOpenProgramStatuses DeleteAdmissionOpenProgram(int admissionOpenProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var admissionOpenProgram = aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.Programs)
					.SelectMany(p => p.AdmissionOpenPrograms)
					.SingleOrDefault(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID);
				if (admissionOpenProgram == null)
					return DeleteAdmissionOpenProgramStatuses.NoRecordFound;
				var query = aspireContext.AdmissionOpenPrograms.First(aop => aop.AdmissionOpenProgramID == admissionOpenProgram.AdmissionOpenProgramID);
				if (query.AdmissionOpenProgramEquivalents.Any())
					return DeleteAdmissionOpenProgramStatuses.ChildRecordExists;
				if (query.CandidateAppliedPrograms.Any())
					return DeleteAdmissionOpenProgramStatuses.ChildRecordExists;
				if (query.CandidateAppliedPrograms1.Any())
					return DeleteAdmissionOpenProgramStatuses.ChildRecordExists;
				if (query.CandidateAppliedPrograms2.Any())
					return DeleteAdmissionOpenProgramStatuses.ChildRecordExists;
				if (query.CandidateAppliedPrograms3.Any())
					return DeleteAdmissionOpenProgramStatuses.ChildRecordExists;
				if (query.CandidateAppliedPrograms4.Any())
					return DeleteAdmissionOpenProgramStatuses.ChildRecordExists;
				if (query.CandidateAppliedPrograms5.Any())
					return DeleteAdmissionOpenProgramStatuses.ChildRecordExists;
				if (query.Students.Any())
					return DeleteAdmissionOpenProgramStatuses.ChildRecordExists;
				if (query.Courses.Any())
					return DeleteAdmissionOpenProgramStatuses.ChildRecordExists;
				aspireContext.AdmissionOpenPrograms.Remove(admissionOpenProgram);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteAdmissionOpenProgramStatuses.Success;
			}
		}

		public sealed class AdmissionOpenProgram
		{
			internal AdmissionOpenProgram() { }

			public int AdmissionOpenProgramID { get; set; }
			public string InstituteAlias { get; set; }
			public string ProgramAlias { get; set; }
			public ProgramDurations DurationEnum { get; set; }
			internal DateTime AdmissionOpenDate { get; set; }
			internal DateTime AdmissionCloseDate { get; set; }
			internal DateTime? AdmissionOpenDateForForeigner { get; set; }
			internal DateTime? AdmissionCloseDateForForeigner { get; set; }
			public string AdmissionOpenDates => this.AdmissionOpenDate.ConcatenateDates(this.AdmissionCloseDate, "g");
			public string AdmissionOpenDatesForForeigners => this.AdmissionOpenDateForForeigner.ConcatenateDates(this.AdmissionCloseDateForForeigner, "g").ToNAIfNullOrEmpty();
			public DateTime AdmissionProcessingFeeDueDate { get; set; }
			public Model.Entities.AdmissionOpenProgram.EntryTestTypes EntryTestTypeEnum { get; set; }
			public string EntryTestTypeFullName => this.EntryTestTypeEnum.ToFullName();
			internal DateTime? EntryTestStartDate { get; set; }
			internal DateTime? EntryTestEndDate { get; set; }
			public string EntryTestDates
			{
				get
				{
					switch (this.EntryTestTypeEnum)
					{
						case Model.Entities.AdmissionOpenProgram.EntryTestTypes.None:
							return string.Empty.ToNAIfNullOrEmpty();
						case Model.Entities.AdmissionOpenProgram.EntryTestTypes.ComputerBasedTest:
							return this.EntryTestStartDate.ConcatenateDates(this.EntryTestEndDate, "d", " to ");
						case Model.Entities.AdmissionOpenProgram.EntryTestTypes.PaperBasedTest:
							return this.EntryTestStartDate.Value.ToString("d");
						default:
							throw new ArgumentOutOfRangeException(nameof(this.EntryTestTypeEnum), this.EntryTestTypeEnum, null);
					}
				}
			}
			public Shifts ShiftsEnum { get; set; }
			public byte? EntryTestDuration { get; set; }
			internal DateTime? InterviewStartDate { get; set; }
			internal DateTime? InterviewEndDate { get; set; }
			public string InterviewDates => this.InterviewStartDate.ConcatenateDates(this.InterviewEndDate, "d");
			public DateTime LoginAllowedEndDate { get; set; }
			public short AdmissionProcessingFee { get; set; }
			public string EquivalentPrograms { get; set; }

			public string ShiftsFullName => this.ShiftsEnum.ToFullNames();
			public short SemesterID { get; set; }
			public short? FinalSemesterID { get; set; }
			public short MaxSemesterID { get; set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public string FinalSemester => this.FinalSemesterID.ToSemesterString().ToNAIfNullOrEmpty();
			public string MaxSemester => this.MaxSemesterID.ToSemesterString();
			public int IntakeTargetStudentsCount { get; set; }
		}

		public static List<AdmissionOpenProgram> GetAdmissionOpenPrograms(short? semesterID, int? instituteID, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid, out int virtualItemCount)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var admissionOpenPrograms = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Programs).SelectMany(p => p.AdmissionOpenPrograms);
				if (semesterID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.SemesterID == semesterID);
				if (instituteID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.Program.InstituteID == instituteID);

				virtualItemCount = admissionOpenPrograms.Count();
				switch (sortExpression)
				{
					case nameof(Program.ProgramAlias):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.Program.Institute.InstituteAlias).ThenBy(sortDirection, aop => aop.Program.ProgramAlias).ThenBy(sortDirection, aop => aop.Program.Duration);
						break;
					case nameof(AdmissionOpenProgram.AdmissionOpenDates):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.AdmissionOpenDate).ThenBy(sortDirection, aop => aop.AdmissionClosingDate);
						break;
					case nameof(AdmissionOpenProgram.AdmissionOpenDatesForForeigners):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.AdmissionOpenDateForForeigners).ThenBy(sortDirection, aop => aop.AdmissionClosingDateForForeigners);
						break;
					case nameof(AdmissionOpenProgram.AdmissionProcessingFeeDueDate):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.AdmissionProcessingFeeDueDate);
						break;
					case nameof(AdmissionOpenProgram.EntryTestTypeEnum):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.EntryTestType);
						break;
					case nameof(AdmissionOpenProgram.EntryTestDates):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.EntryTestStartDate).ThenBy(sortDirection, aop => aop.EntryTestEndDate);
						break;
					case nameof(AdmissionOpenProgram.EntryTestDuration):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.EntryTestDuration);
						break;
					case nameof(AdmissionOpenProgram.InterviewDates):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.InterviewsStartDate).ThenBy(sortDirection, aop => aop.InterviewsEndDate);
						break;
					case nameof(AdmissionOpenProgram.LoginAllowedEndDate):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.LoginAllowedEndDate);
						break;
					case nameof(AdmissionOpenProgram.AdmissionProcessingFee):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.AdmissionProcessingFee);
						break;
					case nameof(AdmissionOpenProgram.IntakeTargetStudentsCount):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.IntakeTargetStudentsCount);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);

				}
				admissionOpenPrograms = admissionOpenPrograms.Skip(pageIndex * pageSize).Take(pageSize);
				var aops = admissionOpenPrograms.Select(aop => new AdmissionOpenProgram
				{
					AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
					SemesterID = aop.SemesterID,
					InstituteAlias = aop.Program.Institute.InstituteAlias,
					ProgramAlias = aop.Program.ProgramAlias,
					DurationEnum = (ProgramDurations)aop.Program.Duration,
					AdmissionOpenDate = aop.AdmissionOpenDate,
					AdmissionCloseDate = aop.AdmissionClosingDate,
					AdmissionProcessingFeeDueDate = aop.AdmissionProcessingFeeDueDate,
					EntryTestTypeEnum = (Model.Entities.AdmissionOpenProgram.EntryTestTypes)aop.EntryTestType,
					EntryTestStartDate = aop.EntryTestStartDate,
					EntryTestEndDate = aop.EntryTestEndDate,
					ShiftsEnum = (Shifts)aop.Shifts,
					EntryTestDuration = aop.EntryTestDuration,
					InterviewStartDate = aop.InterviewsStartDate,
					InterviewEndDate = aop.InterviewsEndDate,
					LoginAllowedEndDate = aop.LoginAllowedEndDate,
					AdmissionProcessingFee = aop.AdmissionProcessingFee,
					MaxSemesterID = aop.MaxSemesterID,
					AdmissionOpenDateForForeigner = aop.AdmissionOpenDateForForeigners,
					AdmissionCloseDateForForeigner = aop.AdmissionClosingDateForForeigners,
					FinalSemesterID = aop.FinalSemesterID,
					IntakeTargetStudentsCount = aop.IntakeTargetStudentsCount
				}).ToList();

				foreach (var _aop in aops)
				{
					_aop.EquivalentPrograms = string.Empty;
					var query = from p in aspireContext.Programs
								join aop in aspireContext.AdmissionOpenPrograms on p.ProgramID equals aop.ProgramID
								join aope in aspireContext.AdmissionOpenProgramEquivalents on aop.AdmissionOpenProgramID equals aope.EquivalentAdmissionOpenProgramID
								where aope.AdmissionOpenProgramID == _aop.AdmissionOpenProgramID
								select new
								{
									p.ProgramAlias
								};
					if (query.Select(q => q.ProgramAlias).ToList().Count == 0)
						_aop.EquivalentPrograms = "Add";
					else
						_aop.EquivalentPrograms = string.Join(", ", query.Select(q => q.ProgramAlias).ToList());
				}
				return aops;
			}
		}

		public static (Model.Entities.AdmissionOpenProgram AdmissionOpenProgram, bool foreignStudentsFound)? GetAdmissionOpenProgram(int admissionOpenProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var admissionOpenProgram = aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.Programs)
					.SelectMany(p => p.AdmissionOpenPrograms)
					.Include(aop => aop.Program)
					.SingleOrDefault(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID);
				if (admissionOpenProgram == null)
					return null;
				return (admissionOpenProgram, aspireContext.CandidateAppliedPrograms.Any(cap => cap.Choice1AdmissionOpenProgramID == admissionOpenProgram.AdmissionOpenProgramID && cap.Candidate.ForeignStudent));
			}
		}

		public static Model.Entities.AdmissionOpenProgram GetLastAdmissionOpenProgram(int programID, short beforeIntakeSemesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.Programs)
					.Where(p => p.ProgramID == programID)
					.SelectMany(p => p.AdmissionOpenPrograms)
					.Where(aop => aop.SemesterID < beforeIntakeSemesterID)
					.OrderByDescending(aop => aop.SemesterID)
					.FirstOrDefault();
			}
		}

		public static List<CustomListItem> GetAdmissionCriteriaList(int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.AdmissionCriterias)
					.Where(c => c.InstituteID == instituteID)
					.Select(ac => new CustomListItem
					{
						ID = ac.AdmissionCriteriaID,
						Text = ac.CriteriaName
					})
					.OrderBy(ac => ac.Text)
					.ToList();
			}
		}

		public static (List<CustomListItem> ExamMarksPolicies, int? defaultExamMarksPolicyID, List<CustomListItem> ExamRemarksPolicies, int? defaultExamRemarksPolicyID) GetPolicies(int programID, short admissionOpenSemesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var program = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Programs).Where(p => p.ProgramID == programID).Select(p => new
				{
					p.InstituteID,
					p.DefaultExamMarksPolicyID,
					p.DefaultExamRemarksPolicyID,
				}).Single();
				var examMarksPolicies = aspireContext.ExamMarksPolicies
					.Where(emp => emp.InstituteID == program.InstituteID)
					.Where(emp => emp.ValidFromSemesterID <= admissionOpenSemesterID)
					.Where(emp => emp.ValidUpToSemesterID == null || admissionOpenSemesterID <= emp.ValidUpToSemesterID)
					.Select(emp => new CustomListItem
					{
						ID = emp.ExamMarksPolicyID,
						Text = emp.ExamMarksPolicyName
					}).ToList();
				var examRemarksPolicies = aspireContext.ExamRemarksPolicies
					.Where(emp => emp.InstituteID == program.InstituteID)
					.Where(emp => emp.ValidFromSemesterID <= admissionOpenSemesterID)
					.Where(emp => emp.ValidUpToSemesterID == null || admissionOpenSemesterID <= emp.ValidUpToSemesterID)
					.Select(erp => new CustomListItem
					{
						ID = erp.ExamRemarksPolicyID,
						Text = erp.ExamRemarksPolicyName
					}).ToList();
				return (examMarksPolicies, program.DefaultExamMarksPolicyID, examRemarksPolicies, program.DefaultExamRemarksPolicyID);
			}
		}

		#endregion

		#region Admission Open Program Equivalents

		public enum AddAdmissionOpenProgramEquivalentStatuses
		{
			AlreadyExists,
			AddWithWarning,
			Success,
		}

		public static AddAdmissionOpenProgramEquivalentStatuses AddEquivalentAdmissionOpenProgram(int admissionOpenProgramID, int equivalentAdmissionOpenProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				if (aspireContext.AdmissionOpenProgramEquivalents.Any(aope => aope.AdmissionOpenProgramID == admissionOpenProgramID && aope.EquivalentAdmissionOpenProgramID == equivalentAdmissionOpenProgramID))
					return AddAdmissionOpenProgramEquivalentStatuses.AlreadyExists;
				var admissionOpenProgramsQuery = aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.Programs)
					.SelectMany(p => p.AdmissionOpenPrograms)
					.Select(aop => new
					{
						aop.AdmissionOpenProgramID,
						aop.AdmissionOpenDate,
						aop.AdmissionClosingDate,
						aop.AdmissionProcessingFeeDueDate,
						aop.AdmissionProcessingFee
					});

				var admissionOpenProgram = admissionOpenProgramsQuery.Single(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID);
				var equivalentAdmissionOpenProgram = admissionOpenProgramsQuery.Single(aop => aop.AdmissionOpenProgramID == equivalentAdmissionOpenProgramID);
				var admissionOpenProgramEquivalent = new AdmissionOpenProgramEquivalent
				{
					AdmissionOpenProgramID = admissionOpenProgramID,
					EquivalentAdmissionOpenProgramID = equivalentAdmissionOpenProgramID,
				};
				aspireContext.AdmissionOpenProgramEquivalents.Add(admissionOpenProgramEquivalent);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				if (admissionOpenProgram.AdmissionOpenDate != equivalentAdmissionOpenProgram.AdmissionOpenDate || admissionOpenProgram.AdmissionClosingDate != equivalentAdmissionOpenProgram.AdmissionClosingDate || admissionOpenProgram.AdmissionProcessingFeeDueDate != equivalentAdmissionOpenProgram.AdmissionProcessingFeeDueDate || admissionOpenProgram.AdmissionProcessingFee != equivalentAdmissionOpenProgram.AdmissionProcessingFee)
					return AddAdmissionOpenProgramEquivalentStatuses.AddWithWarning;
				return AddAdmissionOpenProgramEquivalentStatuses.Success;
			}
		}

		public enum DeleteAdmissionOpenProgramEquivalentStatuses
		{
			NoRecordFound,
			Success,
		}

		public static DeleteAdmissionOpenProgramEquivalentStatuses DeleteAdmissionOpenProgramEquivalent(int admissionOpenProgramEquivalentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var admissionOpenProgramEquivalent = aspireContext.AdmissionOpenProgramEquivalents.SingleOrDefault(aope => aope.AdmissionOpenProgramEquivalentID == admissionOpenProgramEquivalentID);
				if (admissionOpenProgramEquivalent == null)
					return DeleteAdmissionOpenProgramEquivalentStatuses.NoRecordFound;
				aspireContext.AdmissionOpenProgramEquivalents.Remove(admissionOpenProgramEquivalent);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteAdmissionOpenProgramEquivalentStatuses.Success;
			}
		}

		public static List<CustomListItem> GetEquivalentAdmissionOpenPrograms(int admissionOpenProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = from p in aspireContext.Programs
							join aop in aspireContext.AdmissionOpenPrograms on p.ProgramID equals aop.ProgramID
							join aope in aspireContext.AdmissionOpenProgramEquivalents on aop.AdmissionOpenProgramID equals aope.EquivalentAdmissionOpenProgramID
							where aope.AdmissionOpenProgramID == admissionOpenProgramID
							select new CustomListItem
							{
								ID = aope.AdmissionOpenProgramEquivalentID,
								Text = p.ProgramAlias,
							};
				return query.ToList();
			}
		}

		public static List<CustomListItem> GetAdmissionOpenProgramsForEquivalency(int instituteID, short semesterID, int admissionOpenProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = aspireContext.AdmissionOpenPrograms.Where(aop => aop.Program.InstituteID == instituteID && aop.SemesterID == semesterID && aop.AdmissionOpenProgramID != admissionOpenProgramID).Select(aop => new CustomListItem
				{
					ID = aop.AdmissionOpenProgramID,
					Text = aop.Program.ProgramAlias,
				});
				return query.ToList();
			}
		}

		#endregion
	}
}
