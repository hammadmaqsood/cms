﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
namespace Aspire.BL.Core.Admissions.Admin
{
	public static class AdmissionCriterias
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Admissions.ManageAdmissionCriteria, UserGroupPermission.PermissionValues.Allowed);
		}

		#region Admission Criteria

		public sealed class AdmissionCriteria
		{
			internal AdmissionCriteria() { }

			public int AdmissionCriteriaID { get; set; }
			public int InstituteID { get; set; }
			public string CriteriaName { get; set; }
			public string InstituteAlias { get; set; }
		}


		public enum AddAdmissionCriteriaStatuses
		{
			AlreadyExists,
			Success,
		}

		public static AddAdmissionCriteriaStatuses AddAdmissionCriteria(int instituteID, string criteriaName, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				if (!aspireContext.GetAdminInstitutes(loginHistory).Any(i => i.InstituteID == instituteID))
					throw new InvalidOperationException();
				criteriaName = criteriaName.TrimAndCannotBeEmpty();
				if (aspireContext.AdmissionCriterias.Any(ac => ac.InstituteID == instituteID && ac.CriteriaName == criteriaName))
					return AddAdmissionCriteriaStatuses.AlreadyExists;
				var admissionCriteria = new Model.Entities.AdmissionCriteria
				{
					CriteriaName = criteriaName,
					InstituteID = instituteID,
				};
				aspireContext.AdmissionCriterias.Add(admissionCriteria);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddAdmissionCriteriaStatuses.Success;
			}
		}

		public enum UpdateAdmissionCriteriaStatuses
		{
			NoRecordFound,
			AlreadyExists,
			Success,
		}

		public static UpdateAdmissionCriteriaStatuses UpdateAdmissionCriteria(int admissionCriteriaID, string criteriaName, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var admissionCriteria = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.AdmissionCriterias).SingleOrDefault(ac => ac.AdmissionCriteriaID == admissionCriteriaID);
				if (admissionCriteria == null)
					return UpdateAdmissionCriteriaStatuses.NoRecordFound;
				admissionCriteria.CriteriaName = criteriaName.TrimAndCannotBeEmpty();
				if (aspireContext.AdmissionCriterias.Any(ac => ac.AdmissionCriteriaID != admissionCriteria.AdmissionCriteriaID && ac.InstituteID == admissionCriteria.InstituteID && ac.CriteriaName == admissionCriteria.CriteriaName))
					return UpdateAdmissionCriteriaStatuses.AlreadyExists;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateAdmissionCriteriaStatuses.Success;
			}
		}

		public enum DeleteAdmissionCriteriaStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteAdmissionCriteriaStatuses DeleteAdmissionCriteria(int admissionCriteriaID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var admissionCriteria = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.AdmissionCriterias).SingleOrDefault(ac => ac.AdmissionCriteriaID == admissionCriteriaID);
				if (admissionCriteria == null)
					return DeleteAdmissionCriteriaStatuses.NoRecordFound;
				var query = aspireContext.AdmissionCriterias.First(c => c.AdmissionCriteriaID == admissionCriteria.AdmissionCriteriaID);
				if (admissionCriteria.AdmissionCriteriaPreReqs.Any())
					return DeleteAdmissionCriteriaStatuses.ChildRecordExists;
				if (admissionCriteria.AdmissionOpenPrograms.Any())
					return DeleteAdmissionCriteriaStatuses.ChildRecordExists;
				aspireContext.AdmissionCriterias.Remove(admissionCriteria);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteAdmissionCriteriaStatuses.Success;
			}
		}

		public static List<AdmissionCriteria> GetAdmissionCriterias(int? instituteID, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(false))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var institutes = aspireContext.GetAdminInstitutes(loginHistory);
				if (instituteID != null)
					institutes = institutes.Where(i => i.InstituteID == instituteID.Value);
				var query = institutes.SelectMany(i => i.AdmissionCriterias);
				virtualItemCount = query.Count();
				switch (sortExpression)
				{
					case "InstituteAlias":
						query = query.OrderBy(sortDirection, q => q.Institute.InstituteAlias);
						break;
					case "CriteriaName":
						query = query.OrderBy(sortDirection, q => q.CriteriaName);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return query.Skip(pageIndex * pageSize).Take(pageSize).Select(c => new AdmissionCriteria
				{
					AdmissionCriteriaID = c.AdmissionCriteriaID,
					InstituteID = c.InstituteID,
					InstituteAlias = c.Institute.InstituteAlias,
					CriteriaName = c.CriteriaName
				}).ToList();
			}
		}

		public static Model.Entities.AdmissionCriteria GetAdmissionCriteria(int admissionCriteriaID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var admissionCriteria = aspireContext.AdmissionCriterias.SingleOrDefault(ac => ac.AdmissionCriteriaID == admissionCriteriaID);
				return admissionCriteria;
			}
		}

		#endregion

	}
}
