﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Admissions.Admin
{
	public static class AdmissionCriteriaPreReqs
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Admissions.ManageAdmissionCriteria, UserGroupPermission.PermissionValues.Allowed);
		}

		#region Admission Criteria Pre Reqs

		public enum AddAdmissionCriteriaPreReqStatuses
		{
			Success,
		}

		public static AddAdmissionCriteriaPreReqStatuses AddAdmissionCriteriaPreReq(int admissionCriteriaID, string preReqQualification, string preReqQualificationResultStatus, byte? resultAwaitedDegreeType, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var admissionCriteriaPreReq = new Model.Entities.AdmissionCriteriaPreReq
				{
					AdmissionCriteriaID = admissionCriteriaID,
					PreReqQualification = preReqQualification,
					PreReqQualificationResultStatus = preReqQualificationResultStatus,
					ResultAwaitedDegreeType = resultAwaitedDegreeType
				};
				admissionCriteriaPreReq.Validate();
				aspireContext.AdmissionCriteriaPreReqs.Add(admissionCriteriaPreReq);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddAdmissionCriteriaPreReqStatuses.Success;
			}
		}

		public enum UpdateAdmissionCriteriaPreReqStatuses
		{
			NoRecordFound,
			Success,
		}

		public static UpdateAdmissionCriteriaPreReqStatuses UpdateAdmissionCriteriaPreReq(int? admissionCriteriaPreReqID, string preReqQualification, string preReqQualificationResultStatus, byte? resultAwaitedDegreeType, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var admissionCriteriaPreReq = aspireContext.AdmissionCriteriaPreReqs.SingleOrDefault(acPreReq => acPreReq.AdmissionCriteriaPreReqID == admissionCriteriaPreReqID);
				if (admissionCriteriaPreReq == null)
					return UpdateAdmissionCriteriaPreReqStatuses.NoRecordFound;

				admissionCriteriaPreReq.PreReqQualification = preReqQualification;
				admissionCriteriaPreReq.PreReqQualificationResultStatus = preReqQualificationResultStatus;
				admissionCriteriaPreReq.ResultAwaitedDegreeType = resultAwaitedDegreeType;
				admissionCriteriaPreReq.Validate();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateAdmissionCriteriaPreReqStatuses.Success;
			}
		}

		public enum DeleteAdmissionCriteriaPreReqStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteAdmissionCriteriaPreReqStatuses DeleteAdmissionCriteriaPreReq(int admissionCriteriaPreReqID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var admissionCriteriaPreReq = aspireContext.AdmissionCriteriaPreReqs.SingleOrDefault(acPreReq => acPreReq.AdmissionCriteriaPreReqID == admissionCriteriaPreReqID);
				if (admissionCriteriaPreReq == null)
					return DeleteAdmissionCriteriaPreReqStatuses.NoRecordFound;
				if (admissionCriteriaPreReq.AdmissionCriteriaPreReqDegrees.Any())
					return DeleteAdmissionCriteriaPreReqStatuses.ChildRecordExists;
				aspireContext.AdmissionCriteriaPreReqs.Remove(admissionCriteriaPreReq);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteAdmissionCriteriaPreReqStatuses.Success;
			}
		}

		public static Model.Entities.AdmissionCriteriaPreReq GetAdmissionCriteriaPreReq(int admissionCriteriaPreReqID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.AdmissionCriteriaPreReqs.SingleOrDefault(acPreReq => acPreReq.AdmissionCriteriaPreReqID == admissionCriteriaPreReqID);
			}
		}

		public static List<Model.Entities.AdmissionCriteriaPreReq> GetAdmissionCriteriaPreReqs(int instituteID, int admissionCriteriaID, string sortExpression, SortDirection sortDirection)
		{
			using (var aspireContext = new AspireContext())
			{
				var queryGetAdmissionCriteriaPreReq = aspireContext.AdmissionCriteriaPreReqs.Where(q => q.AdmissionCriteriaID == admissionCriteriaID);
				switch (sortExpression)
				{
					case nameof(Model.Entities.AdmissionCriteriaPreReq.PreReqQualification):
						queryGetAdmissionCriteriaPreReq = queryGetAdmissionCriteriaPreReq.OrderBy(sortDirection, q => q.PreReqQualification);
						break;
					case nameof(Model.Entities.AdmissionCriteriaPreReq.PreReqQualificationResultStatus):
						queryGetAdmissionCriteriaPreReq = queryGetAdmissionCriteriaPreReq.OrderBy(sortDirection, q => q.PreReqQualificationResultStatus);
						break;
					case nameof(Model.Entities.AdmissionCriteriaPreReq.ResultAwaitedDegreeType):
						queryGetAdmissionCriteriaPreReq = queryGetAdmissionCriteriaPreReq.OrderBy(sortDirection, q => q.ResultAwaitedDegreeType);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return queryGetAdmissionCriteriaPreReq.ToList();
			}
		}

		#endregion
	}
}
