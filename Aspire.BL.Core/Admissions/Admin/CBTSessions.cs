﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Admissions.Admin
{
	public static class CBTSessions
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Admissions.ManageCBTSessions, UserGroupPermission.PermissionValues.Allowed);
		}

		public enum AddCBTSessionStatuses
		{
			Success,
		}

		public static AddCBTSessionStatuses AddCBTSession(int instituteID, DateTime startTime, byte durationInMinutes, Dictionary<int, byte> labs, Guid loginSessionGuid)
		{
			if (labs.Any(l => l.Value <= 0))
				throw new ArgumentException();
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				if (!aspireContext.GetAdminInstitutes(loginHistory).Any(i => i.InstituteID == instituteID))
					throw new InvalidOperationException();

				byte sequence = 0;
				var cbtSession = new Model.Entities.CBTSession
				{
					InstituteID = instituteID,
					StartTime = startTime,
					DurationInMinutes = durationInMinutes,
					CBTSessionLabs = labs.Select(lab => new CBTSessionLab
					{
						CBTLabID = lab.Key,
						Computers = lab.Value,
						Sequence = sequence++,
					}).ToList()
				};
				aspireContext.CBTSessions.Add(cbtSession);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddCBTSessionStatuses.Success;
			}
		}

		public enum UpdateCBTSessionStatuses
		{
			NoRecordFound,
			CannotUpdateChildRecordExists,
			Success,
		}

		public static UpdateCBTSessionStatuses UpdateCBTSession(int cbtSessionID, DateTime startTime, byte durationInMinutes, Dictionary<int, byte> labs, Guid loginSessionGuid)
		{
			if (labs.Any(l => l.Value <= 0))
				throw new ArgumentException();
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var cbtSession = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.CBTSessions).Include(s => s.CBTSessionLabs).SingleOrDefault(s => s.CBTSessionID == cbtSessionID);
				if (cbtSession == null)
					return UpdateCBTSessionStatuses.NoRecordFound;

				cbtSession.StartTime = startTime;
				cbtSession.DurationInMinutes = durationInMinutes;

				foreach (var cbtSessionLab in cbtSession.CBTSessionLabs.ToList())
					if (labs.All(l => l.Key != cbtSessionLab.CBTLabID))
					{
						if (aspireContext.CandidateAppliedPrograms.Any(cap => cap.CBTSessionLabID == cbtSessionLab.CBTSessionLabID))
							return UpdateCBTSessionStatuses.CannotUpdateChildRecordExists;
						aspireContext.CBTSessionLabs.Remove(cbtSessionLab);
					}
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);

				byte sequence = 0;
				foreach (var lab in labs)
				{
					var cbtLab = cbtSession.CBTSessionLabs.SingleOrDefault(l => l.CBTLabID == lab.Key);
					if (cbtLab == null)
						cbtSession.CBTSessionLabs.Add(new CBTSessionLab
						{
							CBTLabID = lab.Key,
							Computers = lab.Value,
							Sequence = sequence,
						});
					else
					{
						var assignedTotal = aspireContext.CandidateAppliedPrograms.Count(cap => cap.CBTSessionLabID == cbtLab.CBTSessionLabID);
						if (lab.Value > 0 && lab.Value >= assignedTotal)
							cbtLab.Computers = lab.Value;
						else
							return UpdateCBTSessionStatuses.CannotUpdateChildRecordExists;
						cbtLab.Sequence = sequence;
					}
					sequence++;
				}

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateCBTSessionStatuses.Success;
			}
		}

		public enum DeleteCBTSessionStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteCBTSessionStatuses DeleteCBTSession(int cbtSessionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var cbtSession = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.CBTSessions).Include(s => s.CBTSessionLabs).SingleOrDefault(l => l.CBTSessionID == cbtSessionID);
				if (cbtSession == null)
					return DeleteCBTSessionStatuses.NoRecordFound;
				if (aspireContext.CandidateAppliedPrograms.Any(cap => cap.CBTSessionLab.CBTSessionID == cbtSessionID))
					return DeleteCBTSessionStatuses.ChildRecordExists;
				aspireContext.CBTSessionLabs.RemoveRange(cbtSession.CBTSessionLabs);
				aspireContext.CBTSessions.Remove(cbtSession);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteCBTSessionStatuses.Success;
			}
		}

		public sealed class CBTSession
		{
			internal CBTSession() { }

			public string InstituteAlias { get; internal set; }
			public int Labs { get; internal set; }
			public int Computers { get; internal set; }
			public int CBTSessionID { get; internal set; }
			public DateTime StartTime { get; internal set; }
			public byte DurationInMinutes { get; internal set; }
			public int ConfirmedSeats { get; internal set; }
		}

		public static List<CBTSession> GetCBTSessions(int? instituteID, DateTime? sessionDate, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(false))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var institutes = aspireContext.GetAdminInstitutes(loginHistory);
				if (instituteID != null)
					institutes = institutes.Where(i => i.InstituteID == instituteID.Value);
				var query = institutes.SelectMany(i => i.CBTSessions);
				if (sessionDate != null)
				{
					sessionDate = sessionDate.Value.Date;
					query = query.Where(s => DbFunctions.TruncateTime(s.StartTime) == sessionDate.Value);
				}
				virtualItemCount = query.Count();

				var result = query.Select(q => new CBTSession
				{
					CBTSessionID = q.CBTSessionID,
					InstituteAlias = q.Institute.InstituteAlias,
					StartTime = q.StartTime,
					DurationInMinutes = q.DurationInMinutes,
					Computers = q.CBTSessionLabs.Sum(l => (int?)l.Computers) ?? 0,
					Labs = q.CBTSessionLabs.Count,
					ConfirmedSeats = q.CBTSessionLabs.SelectMany(l => l.CandidateAppliedPrograms).Count()
				});

				switch (sortExpression)
				{
					case nameof(CBTSession.InstituteAlias):
						result = result.OrderBy(sortDirection, q => q.InstituteAlias);
						break;
					case nameof(CBTSession.Labs):
						result = result.OrderBy(sortDirection, q => q.Labs);
						break;
					case nameof(CBTSession.StartTime):
						result = result.OrderBy(sortDirection, q => q.StartTime);
						break;
					case nameof(CBTSession.DurationInMinutes):
						result = result.OrderBy(sortDirection, q => q.DurationInMinutes);
						break;
					case nameof(CBTSession.Computers):
						result = result.OrderBy(sortDirection, q => q.Computers);
						break;
					case nameof(CBTSession.ConfirmedSeats):
						result = result.OrderBy(sortDirection, q => q.ConfirmedSeats);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return result.Skip(pageIndex * pageSize).Take(pageSize).ToList();
			}
		}

		public sealed class Session
		{
			internal Session() { }
			public int CBTSessionID { get; internal set; }
			public DateTime StartTime { get; internal set; }
			public byte DurationInMinutes { get; internal set; }
			public int InstituteID { get; internal set; }
			public int Computers => this.Labs.Sum(l => l.Computers);
			public List<Lab> Labs { get; internal set; }

			public sealed class Lab
			{
				internal Lab() { }
				public int CBTLabID { get; internal set; }
				public byte Computers { get; internal set; }
				public string LabName { get; internal set; }
			}
		}

		public static Session GetCBTSession(int cbtSessionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.CBTSessions)
					.Where(l => l.CBTSessionID == cbtSessionID)
					.Select(s => new Session
					{
						CBTSessionID = s.CBTSessionID,
						InstituteID = s.InstituteID,
						StartTime = s.StartTime,
						DurationInMinutes = s.DurationInMinutes,
						Labs = s.CBTSessionLabs.Select(l => new Session.Lab
						{
							CBTLabID = l.CBTLabID,
							Computers = l.Computers,
						}).ToList()
					})
					.SingleOrDefault();
			}
		}

		public static List<Session.Lab> GetCBTLabs(int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory)
					.Where(i => i.InstituteID == instituteID)
					.SelectMany(i => i.CBTLabs)
					.OrderBy(l => l.LabName)
					.Select(l => new Session.Lab
					{
						CBTLabID = l.CBTLabID,
						LabName = l.LabName,
						Computers = l.Computers,
					})
					.ToList();
			}
		}
	}
}
