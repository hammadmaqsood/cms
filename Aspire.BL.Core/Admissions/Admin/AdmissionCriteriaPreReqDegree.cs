﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Admissions.Admin
{
	public static class AdmissionCriteriaPreReqDegrees
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Admissions.ManageAdmissionCriteria, UserGroupPermission.PermissionValues.Allowed);
		}

		#region Admission Criteria Pre Req Degrees

		public enum AddAdmissionCriteriaPreReqDegreeStatuses
		{
			Success,
		}

		public static AddAdmissionCriteriaPreReqDegreeStatuses AddAdmissionCriteriaPreReqDegree(AdmissionCriteriaPreReqDegree admissionCriteriaPreReqDegree, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				admissionCriteriaPreReqDegree.Validate();
				admissionCriteriaPreReqDegree = aspireContext.AdmissionCriteriaPreReqDegrees.Add(admissionCriteriaPreReqDegree);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddAdmissionCriteriaPreReqDegreeStatuses.Success;
			}
		}

		public enum UpdateAdmissionCriteriaPreReqDegreeStatuses
		{
			NoRecordFound,
			Success,
		}

		public static UpdateAdmissionCriteriaPreReqDegreeStatuses UpdateAdmissionCriteriaPreReqDegree(AdmissionCriteriaPreReqDegree admissionCriteriaPreReqDegree, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var admissionCriteriaPreReqDegreeforEdit = aspireContext.AdmissionCriteriaPreReqDegrees.SingleOrDefault(acPreReqDeg => acPreReqDeg.AdmissionCriteriaPreReqDegreeID == admissionCriteriaPreReqDegree.AdmissionCriteriaPreReqDegreeID);
				if (admissionCriteriaPreReqDegreeforEdit == null)
					return UpdateAdmissionCriteriaPreReqDegreeStatuses.NoRecordFound;
				admissionCriteriaPreReqDegree.Validate();
				admissionCriteriaPreReqDegreeforEdit.DegreeType = admissionCriteriaPreReqDegree.DegreeType;
				admissionCriteriaPreReqDegreeforEdit.AnnualExamSystem = admissionCriteriaPreReqDegree.AnnualExamSystem;
				admissionCriteriaPreReqDegreeforEdit.SemesterSystem = admissionCriteriaPreReqDegree.SemesterSystem;
				admissionCriteriaPreReqDegreeforEdit.AnnualExamSystemMinimumPercentage = admissionCriteriaPreReqDegree.AnnualExamSystemMinimumPercentage;
				admissionCriteriaPreReqDegreeforEdit.SemesterSystemMinCGPAOutOf4 = admissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf4;
				admissionCriteriaPreReqDegreeforEdit.SemesterSystemMinCGPAOutOf5 = admissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf5;
				admissionCriteriaPreReqDegreeforEdit.DisplaySequence = admissionCriteriaPreReqDegree.DisplaySequence;
				admissionCriteriaPreReqDegreeforEdit.MeritListWeightage = admissionCriteriaPreReqDegree.MeritListWeightage;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateAdmissionCriteriaPreReqDegreeStatuses.Success;
			}
		}

		public enum DeleteAdmissionCriteriaPreReqDegreeStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteAdmissionCriteriaPreReqDegreeStatuses DeleteAdmissionCriteriaPreReqDegree(int admissionCriteriaPreReqDegreeID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var admissionCriteriaPreReqDegree = aspireContext.AdmissionCriteriaPreReqDegrees.SingleOrDefault(acPreReq => acPreReq.AdmissionCriteriaPreReqDegreeID == admissionCriteriaPreReqDegreeID);
				if (admissionCriteriaPreReqDegree == null)
					return DeleteAdmissionCriteriaPreReqDegreeStatuses.NoRecordFound;
				if (admissionCriteriaPreReqDegree.AdmissionCriteriaPreReqDegreeSubjects.Any())
					return DeleteAdmissionCriteriaPreReqDegreeStatuses.ChildRecordExists;
				aspireContext.AdmissionCriteriaPreReqDegrees.Remove(admissionCriteriaPreReqDegree);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteAdmissionCriteriaPreReqDegreeStatuses.Success;
			}
		}

		public static AdmissionCriteriaPreReqDegree GetAdmissionCriteriaPreReqDegree(int admissionCriteriaPreRegDegreeID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.AdmissionCriteriaPreReqDegrees.SingleOrDefault(d => d.AdmissionCriteriaPreReqDegreeID == admissionCriteriaPreRegDegreeID);
			}
		}

		public static List<AdmissionCriteriaPreReqDegree> GetAdmissionCriteriaPreReqDegrees(int admissionCriteriaPreReqID, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = aspireContext.AdmissionCriteriaPreReqDegrees.AsQueryable();
				query = query.Where(q => q.AdmissionCriteriaPreReqID == admissionCriteriaPreReqID);
				switch (sortExpression)
				{
					case nameof(AdmissionCriteriaPreReqDegree.DegreeType):
						query = query.OrderBy(sortDirection, q => q.DegreeType);
						break;
					case nameof(AdmissionCriteriaPreReqDegree.AnnualExamSystem):
						query = query.OrderBy(sortDirection, q => q.AnnualExamSystem);
						break;
					case nameof(AdmissionCriteriaPreReqDegree.SemesterSystem):
						query = query.OrderBy(sortDirection, q => q.SemesterSystem);
						break;
					case nameof(AdmissionCriteriaPreReqDegree.AnnualExamSystemMinimumPercentage):
						query = query.OrderBy(sortDirection, q => q.AnnualExamSystemMinimumPercentage);
						break;
					case nameof(AdmissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf4):
						query = query.OrderBy(sortDirection, q => q.SemesterSystemMinCGPAOutOf4);
						break;
					case nameof(AdmissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf5):
						query = query.OrderBy(sortDirection, q => q.SemesterSystemMinCGPAOutOf5);
						break;
					case nameof(AdmissionCriteriaPreReqDegree.DisplaySequence):
						query = query.OrderBy(sortDirection, q => q.DisplaySequence);
						break;
					case nameof(AdmissionCriteriaPreReqDegree.MeritListWeightage):
						query = query.OrderBy(sortDirection, q => q.MeritListWeightage);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return query.ToList();
			}
		}

		#endregion
	}
}