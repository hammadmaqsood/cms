﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Admin
{
	public static class CandidateInformationForCBT
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int instituteID, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID, AdminPermissions.Admissions.ExportCandidateInformationForCBT, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class CandidateDetail
		{
			internal CandidateDetail()
			{
			}
			public int CandidateAppliedProgramID { get; internal set; }
			public int InstituteID { get; internal set; }
			public string InstituteAlias { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Name { get; internal set; }
			public string Email { get; internal set; }
			public string Phone { get; internal set; }
			public string Mobile { get; internal set; }
			public int? ApplicationNo { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string Password { get; internal set; }
			public DateTime? DepositDate { get; internal set; }
			public DateTime ApplyDate { get; internal set; }
			public string CBTLabInstituteAlias { get; internal set; }
			public string LabName { get; internal set; }
			public DateTime? CBTLabStartTime { get; internal set; }
			public byte? CBTLabDurationInMinutes { get; internal set; }
			public int TestAttempt { get; internal set; }
			public short Year { get; internal set; }
			public byte SemesterType { get; internal set; }
			public string AccountTransactionRemarks { get; internal set; }

			public string Semester => this.SemesterID.ToSemesterString();

			public int? FinalApplicationNo
			{
				get
				{
					if (!string.IsNullOrEmpty(this.AccountTransactionRemarks))
					{
						return this.AccountTransactionRemarks.Contains("*Not in Use and Transferred*") ? 9999 : this.ApplicationNo;
					}
					return this.ApplicationNo;
				}
			}

			public string FinalName
			{
				get
				{
					if (!string.IsNullOrEmpty(this.AccountTransactionRemarks))
					{
						return this.AccountTransactionRemarks.Contains("*Not in Use and Transferred*") ? $"(TRANSFERRED)-{this.Name}" : this.Name;
					}
					return this.Name;
				}
			}
		}

		public static List<CandidateDetail> GetCandidateInformation(short semesterID, int instituteID, int? departmentID, int? admissionOpenProgramID, bool? paid, bool forCBT, List<int> applicationNonsList, int? applicationNo, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				var candidateAppliedPrograms = aspireContext.CandidateAppliedPrograms.AsQueryable();
				switch (paid)
				{
					case true:
						candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.AccountTransactionID != null);
						break;
					case null:
						break;
					default:
						candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.AccountTransactionID == null);
						break;
				}

				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms.Where(aop => aop.SemesterID == semesterID && aop.Program.InstituteID == instituteID);
				if (departmentID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.Program.DepartmentID == departmentID.Value);
				if (admissionOpenProgramID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID.Value);

				var query = from cap in candidateAppliedPrograms
							join aop in admissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
							select new CandidateDetail
							{
								InstituteID = aop.Program.InstituteID,
								SemesterID = aop.SemesterID,
								InstituteAlias = aop.Program.Institute.InstituteAlias,
								ApplicationNo = cap.ApplicationNo,
								Name = cap.Candidate.Name,
								ProgramAlias = aop.Program.ProgramAlias,
								Email = cap.Candidate.Email,
								Phone = cap.Candidate.Phone,
								Mobile = cap.Candidate.Mobile,
								Password = cap.Candidate.Password,
								DepositDate = cap.AccountTransaction.TransactionDate,
								ApplyDate=cap.ApplyDate,
								CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
								CBTLabInstituteAlias = cap.CBTSessionLab.CBTSession.Institute.InstituteAlias,
								LabName = cap.CBTSessionLab.CBTLab.LabName,
								CBTLabStartTime = (DateTime?)cap.CBTSessionLab.CBTSession.StartTime,
								CBTLabDurationInMinutes = (byte?)cap.CBTSessionLab.CBTSession.DurationInMinutes,
								TestAttempt = 0,
								Year = aop.Semester.Year,
								SemesterType = aop.Semester.SemesterType,
								AccountTransactionRemarks = cap.AccountTransaction.Remarks,
							};
				if (applicationNo != null)
				{
					query = query.Where(q => q.ApplicationNo == applicationNo);
				}
				else if (applicationNonsList.Any())
				{
					query = query.Where(q => applicationNonsList.Contains(q.ApplicationNo.Value));
				}
				var finalquery = forCBT ? query.ToList().OrderByDescending(o => o.FinalApplicationNo) : query.ToList().OrderBy(o => o.FinalApplicationNo);
				return finalquery.ToList();
			}
		}
	}
}
