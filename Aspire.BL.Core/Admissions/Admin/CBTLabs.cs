﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Admissions.Admin
{
	public static class CBTLabs
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Admissions.ManageCBTLabs, UserGroupPermission.PermissionValues.Allowed);
		}

		public enum AAddCBTLabStatuses
		{
			LabNameAlreadyExists,
			Success,
		}

		public static AAddCBTLabStatuses AddCBTLab(int instituteID, string testCentreName, string labName, byte computers, Model.Entities.CBTLab.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				if (!aspireContext.GetAdminInstitutes(loginHistory).Any(i => i.InstituteID == instituteID))
					throw new InvalidOperationException();
				labName = labName.TrimAndCannotBeEmpty();
				if (aspireContext.CBTLabs.Any(ac => ac.InstituteID == instituteID && ac.LabName == labName))
					return AAddCBTLabStatuses.LabNameAlreadyExists;
				var cbtLab = new Model.Entities.CBTLab
				{
					InstituteID = instituteID,
					TestCentreName = testCentreName,
					LabName = labName,
					Computers = computers,
					StatusEnum = statusEnum,
				};
				aspireContext.CBTLabs.Add(cbtLab);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AAddCBTLabStatuses.Success;
			}
		}

		public enum UpdateCBTLabStatuses
		{
			NoRecordFound,
			LabNameAlreadyExists,
			Success,
		}

		public static UpdateCBTLabStatuses UpdateCBTLab(int cbtLabID, string testCentreName, string labName, byte computers, Model.Entities.CBTLab.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var cbtLab = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.CBTLabs).SingleOrDefault(l => l.CBTLabID == cbtLabID);
				if (cbtLab == null)
					return UpdateCBTLabStatuses.NoRecordFound;
				cbtLab.TestCentreName = testCentreName.TrimAndCannotBeEmpty();
				cbtLab.LabName = labName.TrimAndCannotBeEmpty();
				if (aspireContext.CBTLabs.Any(l => l.CBTLabID != cbtLab.CBTLabID && l.InstituteID == cbtLab.InstituteID && l.LabName == cbtLab.LabName))
					return UpdateCBTLabStatuses.LabNameAlreadyExists;
				cbtLab.Computers = computers;
				cbtLab.StatusEnum = statusEnum;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateCBTLabStatuses.Success;
			}
		}

		public enum DeleteCBTLabStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteCBTLabStatuses DeleteCBTLab(int cbtLabID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var cbtLab = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.CBTLabs).SingleOrDefault(l => l.CBTLabID == cbtLabID);
				if (cbtLab == null)
					return DeleteCBTLabStatuses.NoRecordFound;
				if (cbtLab.CBTSessionLabs.Any())
					return DeleteCBTLabStatuses.ChildRecordExists;
				aspireContext.CBTLabs.Remove(cbtLab);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteCBTLabStatuses.Success;
			}
		}

		public sealed class CBTLab
		{
			internal CBTLab() { }

			public int CBTLabID { get; internal set; }
			public string InstituteAlias { get; internal set; }
			public string TestCentreName { get; internal set; }
			public string LabName { get; internal set; }
			public byte Status { get; internal set; }
			public string StatusFullName => ((Model.Entities.CBTLab.Statuses)this.Status).ToFullName();
			public int Computers { get; internal set; }
		}

		public static List<CBTLab> GetCBTLabs(int? instituteID, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(false))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var institutes = aspireContext.GetAdminInstitutes(loginHistory);
				if (instituteID != null)
					institutes = institutes.Where(i => i.InstituteID == instituteID.Value);
				var query = institutes.SelectMany(i => i.CBTLabs);
				virtualItemCount = query.Count();
				switch (sortExpression)
				{
					case nameof(CBTLab.InstituteAlias):
						query = query.OrderBy(sortDirection, q => q.Institute.InstituteAlias);
						break;
					case nameof(CBTLab.TestCentreName):
						query = query.OrderBy(sortDirection, q => q.TestCentreName);
						break;
					case nameof(CBTLab.LabName):
						query = query.OrderBy(sortDirection, q => q.LabName);
						break;
					case nameof(CBTLab.Computers):
						query = query.OrderBy(sortDirection, q => q.Computers);
						break;
					case nameof(CBTLab.Status):
					case nameof(CBTLab.StatusFullName):
						query = query.OrderBy(sortDirection, q => q.Status);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return query.Skip(pageIndex * pageSize).Take(pageSize).Select(c => new CBTLab
				{
					CBTLabID = c.CBTLabID,
					InstituteAlias = c.Institute.InstituteAlias,
					TestCentreName = c.TestCentreName,
					LabName = c.LabName,
					Status = c.Status,
					Computers = c.Computers,
				}).ToList();
			}
		}

		public static Model.Entities.CBTLab GetCBTLab(int cbtLabID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.CBTLabs).SingleOrDefault(l => l.CBTLabID == cbtLabID);
			}
		}
	}
}
