﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FileServer;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Common
{
	public static class ProfileInformation
	{
		public sealed class CandidateDetailedProfile
		{
			internal CandidateDetailedProfile() { }
			public Model.Entities.Candidate Candidate { get; internal set; }
			public List<CandidateForeignAcademicRecord> CandidateForeignAcademicRecords { get; internal set; }
			public List<CandidateForeignDocument> CandidateForeignDocuments { get; internal set; }

			public List<AppliedProgram> AppliedPrograms { get; internal set; }
			public List<CandidateAppliedProgramForeigner> CandidateAppliedProgramsForeigner { get; internal set; }

			public sealed class AppliedProgram
			{
				internal AppliedProgram() { }
				public CandidateAppliedProgram CandidateAppliedProgram { get; internal set; }
				public AdmissionOpenProgram AdmissionOpenProgram1 { get; internal set; }
				public AdmissionOpenProgram AdmissionOpenProgram2 { get; internal set; }
				public AdmissionOpenProgram AdmissionOpenProgram3 { get; internal set; }
				public AdmissionOpenProgram AdmissionOpenProgram4 { get; internal set; }
				public string EntryTestDateString
				{
					get
					{
						switch (this.AdmissionOpenProgram1.EntryTestTypeEnum)
						{
							case AdmissionOpenProgram.EntryTestTypes.None:
								return string.Empty.ToNAIfNullOrEmpty();
							case AdmissionOpenProgram.EntryTestTypes.ComputerBasedTest:
								if (this.CandidateAppliedProgram.EntryTestDate != null)
									return this.CandidateAppliedProgram.EntryTestDate.Value.ToString("F");
								else
									return this.AdmissionOpenProgram1.EntryTestStartDate != null ? $"Starts from {this.AdmissionOpenProgram1.EntryTestStartDate:D}" : null;
							case AdmissionOpenProgram.EntryTestTypes.PaperBasedTest:
								if (this.CandidateAppliedProgram.EntryTestDate != null)
									return this.CandidateAppliedProgram.EntryTestDate.Value.ToString("F");
								else
									return (this.AdmissionOpenProgram1.EntryTestStartDate?.ToString("F")).ToNAIfNullOrEmpty();
							default:
								throw new ArgumentOutOfRangeException();
						}
					}
				}
				public List<CandidateAppliedProgramAcademicRecord> AcademicRecords { get; internal set; }

			}
			public sealed class CandidateAppliedProgramForeigner : AdmissionOpenProgram
			{
				internal CandidateAppliedProgramForeigner() { }

				public int CandidateAppliedProgramID { get; internal set; }
				public DateTime ApplyDate { get; internal set; }
				public Shifts ShiftEnum { get; internal set; }
				public bool CreditTransfer { get; internal set; }
				public new int AdmissionOpenProgramID { get; internal set; }
				public int InstituteID { get; internal set; }
				public string InstituteName { get; internal set; }
				public string ProgramName { get; internal set; }
				public ProgramDurations DurationEnum { get; internal set; }
				public new string EligibilityCriteriaStatement { get; internal set; }
				public Shifts ShiftEnumFlags { get; internal set; }
				public new DateTime? AdmissionOpenDateForForeigners { get; internal set; }
				public new DateTime? AdmissionClosingDateForForeigners { get; internal set; }
				public bool Applied { get; internal set; }
				public string ResearchThemeAreaName { get; internal set; }
			}
		}


		public static CandidateDetailedProfile GetCandidateDetailedProfile(int candidateID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				int? instituteID = null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						instituteID = aspireContext.DemandStaffPermissions(loginHistory.LoginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed).InstituteID;
						break;
					case UserTypes.Candidate:
						var history = aspireContext.DemandCandidateActiveSession(loginSessionGuid);
						if (candidateID != history.CandidateID)
							return null;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				var candidateAppliedProgramsForeigner = from cap in aspireContext.CandidateAppliedPrograms
														join aop in aspireContext.AdmissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
														where cap.CandidateID == candidateID
														select new CandidateDetailedProfile.CandidateAppliedProgramForeigner
														{
															CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
															AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
															ProgramName = aop.Program.ProgramName,
															InstituteID = aop.Program.InstituteID,
															InstituteName = aop.Program.Institute.InstituteName,
															AdmissionOpenDateForForeigners = aop.AdmissionOpenDateForForeigners,
															AdmissionClosingDateForForeigners = aop.AdmissionClosingDateForForeigners,
															DurationEnum = (ProgramDurations)aop.Program.Duration,
															EligibilityCriteriaStatement = aop.EligibilityCriteriaStatement,
															ShiftEnumFlags = (Shifts)cap.Shift,
															ApplyDate = cap.ApplyDate,
															CreditTransfer = cap.CreditTransfer,
															ShiftEnum = (Shifts)cap.Shift,
															ResearchThemeAreaName = cap.ResearchThemeArea.AreaName,
														};
				var candidateDetailedProfile = new CandidateDetailedProfile
				{
					Candidate = aspireContext.Candidates.Include(c => c.CandidateAppliedPrograms.Select(cap => cap.AccountTransaction.InstituteBankAccount.Bank)).Include(c => c.CandidateAppliedPrograms.Select(cap => cap.ResearchThemeArea)).Single(c => c.CandidateID == candidateID),
					CandidateForeignAcademicRecords = aspireContext.Candidates.Single(c => c.CandidateID == candidateID).CandidateForeignAcademicRecords.ToList(),
					CandidateForeignDocuments = aspireContext.Candidates.Single(c => c.CandidateID == candidateID).CandidateForeignDocuments.OrderBy(o => o.DocumentTypeEnum).ToList(),
					AppliedPrograms = new List<CandidateDetailedProfile.AppliedProgram>(),
					CandidateAppliedProgramsForeigner = candidateAppliedProgramsForeigner.ToList(),
				};

				if (candidateDetailedProfile.Candidate.ForeignStudent) return candidateDetailedProfile;

				foreach (var candidateAppliedProgram in candidateDetailedProfile.Candidate.CandidateAppliedPrograms)
				{
					var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms.Include(aop => aop.Program.Institute);
					var appliedProgram = new CandidateDetailedProfile.AppliedProgram
					{
						CandidateAppliedProgram = candidateAppliedProgram,
						AdmissionOpenProgram1 = admissionOpenPrograms.First(aop => aop.AdmissionOpenProgramID == candidateAppliedProgram.Choice1AdmissionOpenProgramID),
						AdmissionOpenProgram2 = admissionOpenPrograms.FirstOrDefault(aop => aop.AdmissionOpenProgramID == candidateAppliedProgram.Choice2AdmissionOpenProgramID),
						AdmissionOpenProgram3 = admissionOpenPrograms.FirstOrDefault(aop => aop.AdmissionOpenProgramID == candidateAppliedProgram.Choice3AdmissionOpenProgramID),
						AdmissionOpenProgram4 = admissionOpenPrograms.FirstOrDefault(aop => aop.AdmissionOpenProgramID == candidateAppliedProgram.Choice4AdmissionOpenProgramID),
						AcademicRecords = aspireContext.CandidateAppliedProgramAcademicRecords.Where(capa => capa.CandidateAppliedProgramID == candidateAppliedProgram.CandidateAppliedProgramID).ToList(),
					};
					if (instituteID == null || appliedProgram.AdmissionOpenProgram1.Program.InstituteID == instituteID)
						candidateDetailedProfile.AppliedPrograms.Add(appliedProgram);
				}
				return candidateDetailedProfile;
			}
		}

		public static byte[] GetCandidateProfilePicture(int candidateID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
						var candidates = from cap in aspireContext.CandidateAppliedPrograms
										 join aop in aspireContext.AdmissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
										 where aop.Program.InstituteID == staffLoginHistory.InstituteID
										 select new { cap.CandidateID };
						if (candidates.Any(c => c.CandidateID == candidateID))
							return aspireContext.ReadCandidatePicture(candidateID);
						break;
					case UserTypes.Candidate:
						return aspireContext.ReadCandidatePicture(loginHistory.CandidateID ?? throw new InvalidOperationException());
					default:
						throw new NotImplementedEnumException(loginHistory.UserTypeEnum);
				}
			}
			return null;
		}

		public static Model.Entities.Candidate GetCandidate(int candidateID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
						var candidate = (from cap in aspireContext.CandidateAppliedPrograms
										 join aop in aspireContext.AdmissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
										 where cap.CandidateID == candidateID && aop.Program.InstituteID == staffLoginHistory.InstituteID
										 select cap.Candidate).FirstOrDefault();
						if (candidate != null)
							candidate.EditStatusEnum = Model.Entities.Candidate.EditStatuses.CanEdit;
						return candidate;
					case UserTypes.Candidate:
						return candidateID != loginHistory.CandidateID
							? null
							: aspireContext.Candidates.Single(c => c.CandidateID == loginHistory.CandidateID);
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Student:
					case UserTypes.Faculty:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		public static bool UpdateCandidate(Model.Entities.Candidate candidate, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				candidate.Validate();
				Model.Entities.Candidate candidateDB;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.ChangeProfileInformation, UserGroupPermission.PermissionValues.Allowed);
						candidateDB = (from cap in aspireContext.CandidateAppliedPrograms
									   join aop in aspireContext.AdmissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
									   where cap.CandidateID == candidate.CandidateID && aop.Program.InstituteID == staffLoginHistory.InstituteID
									   select cap.Candidate).FirstOrDefault();
						if (candidateDB == null)
							return false;

						var editStatus = candidateDB.EditStatusEnum;
						candidateDB.EditStatusEnum = Model.Entities.Candidate.EditStatuses.CanEdit;
						candidateDB.CopyValues(candidate);
						candidateDB.EditStatusEnum = editStatus;
						break;
					case UserTypes.Candidate:
						candidateDB = aspireContext.Candidates.Single(c => c.CandidateID == loginHistory.CandidateID.Value);
						candidateDB.CopyValues(candidate);
						break;
					default:
						throw new NotImplementedEnumException(loginHistory.UserTypeEnum);
				}

				switch (candidateDB.StatusEnum)
				{
					case Model.Entities.Candidate.Statuses.Blank:
						candidateDB.StatusEnum = Model.Entities.Candidate.Statuses.InformationProvided;
						break;
					case Model.Entities.Candidate.Statuses.PictureUploaded:
						candidateDB.StatusEnum = candidateDB.StatusEnum | Model.Entities.Candidate.Statuses.InformationProvided;
						break;
					case Model.Entities.Candidate.Statuses.InformationProvided:
						break;
					case Model.Entities.Candidate.Statuses.Completed:
						break;
					default:
						throw new NotImplementedEnumException(candidateDB.StatusEnum);
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return true;
			}
		}

		public enum UpdatePhotoStatuses
		{
			NoRecordFound,
			RecordCannotBeChanged,
			ResolutionMustBe400X400,
			InvalidImage,
			Success
		}

		public static UpdatePhotoStatuses UpdatePhoto(int candidateID, byte[] bytes, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
						var candidates = from cap in aspireContext.CandidateAppliedPrograms
										 join aop in aspireContext.AdmissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
										 where cap.CandidateID == candidateID && aop.Program.InstituteID == staffLoginHistory.InstituteID
										 select new { cap.CandidateID };
						if (!candidates.Any(c => c.CandidateID == candidateID))
							return UpdatePhotoStatuses.NoRecordFound;
						break;
					case UserTypes.Candidate:
						var candidate = aspireContext.Candidates.Where(c => c.CandidateID == loginHistory.CandidateID.Value && c.CandidateID == candidateID)
							.Select(c => new
							{
								EditStatusEnum = (Model.Entities.Candidate.EditStatuses)c.EditStatus
							}).SingleOrDefault();
						switch (candidate?.EditStatusEnum)
						{
							case null:
								return UpdatePhotoStatuses.NoRecordFound;
							case Model.Entities.Candidate.EditStatuses.CanNotEditBasicInfo:
							case Model.Entities.Candidate.EditStatuses.CanEditContactInformation:
							case Model.Entities.Candidate.EditStatuses.Readonly:
								return UpdatePhotoStatuses.RecordCannotBeChanged;
							case Model.Entities.Candidate.EditStatuses.CanEdit:
								break;
							default:
								throw new NotImplementedEnumException(candidate?.EditStatusEnum);
						}
						break;
					default:
						throw new NotImplementedEnumException(loginHistory.UserTypeEnum);
				}
				var status = aspireContext.UpdateCandidatePicture(candidateID, bytes, false, loginSessionGuid);
				switch (status)
				{
					case CandidatePictures.AddOrUpdateCandidatePngFormatPictureStatuses.Success:
						aspireContext.CommitTransaction();
						return UpdatePhotoStatuses.Success;
					case CandidatePictures.AddOrUpdateCandidatePngFormatPictureStatuses.ResolutionMustBe400X400:
						return UpdatePhotoStatuses.ResolutionMustBe400X400;
					case CandidatePictures.AddOrUpdateCandidatePngFormatPictureStatuses.InvalidImage:
						return UpdatePhotoStatuses.InvalidImage;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
		}
	}
}