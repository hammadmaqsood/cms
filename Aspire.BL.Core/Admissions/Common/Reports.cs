﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Common
{
	public static class Reports
	{
		public sealed class AdmissionProcessingFeeChallan
		{
			public sealed class BankAccount
			{
				public int InstituteBankAccountID { get; internal set; }
				public string BankName { get; internal set; }
				public string BankNameForDisplay { get; internal set; }
				internal string AccountTitle { get; set; }
				internal string AccountTitleForDisplay { get; set; }
				internal string AccountNo { get; set; }
				internal string AccountNoForDisplay { get; set; }
				internal string BranchCode { get; set; }
				internal string BranchName { get; set; }
				internal string BranchAddress { get; set; }
				internal string BranchForDisplay { get; set; }
				public string BankText => this.BankNameForDisplay.ToNullIfWhiteSpace() ?? this.BankName;
				public string AccountTitleText => this.AccountTitleForDisplay.ToNullIfWhiteSpace() ?? this.AccountTitle;
				public string AccountNoText => this.AccountNoForDisplay.ToNullIfWhiteSpace() ?? this.AccountNo;
				public string BranchText => this.BranchForDisplay.ToNullIfWhiteSpace() ?? $"{this.BranchCode} - {this.BranchName}";
				public string Instructions
				{
					get
					{
						switch (this.BankName)
						{
							case "Allied Bank":
								return AccountTransaction.AlliedBankInstructions;
							case "Bank Alfalah":
								return AccountTransaction.BankAlfalahInstructions;
							default:
								return $"Instructions are missing for {this.BankName}";
						}
					}
				}
				public List<BankAccount> GetList()
				{
					return null;
				}
			}

			public List<BankAccount> BankAccounts { get; internal set; }
			internal int InstituteID { get; set; }
			public string InstitutePhone { get; internal set; }
			public string InstituteName { get; internal set; }
			public int CandidateAppliedProgramID { get; internal set; }
			public int ChallanNo { get; internal set; }
			public string FullChallanNo => AspireFormats.ChallanNo.GetFullChallanNoString(this.InstituteCode, InstituteBankAccount.FeeTypes.AdmissionProcessingFee, this.ChallanNo);
			public string ChallanType => "Admission Processing Fee";
			public string Name { get; internal set; }
			public string FatherName { get; internal set; }
			public string ProgramShortName { get; internal set; }
			public byte Duration { get; internal set; }
			public string Program => this.ProgramShortName;
			public short Amount { get; internal set; }
			public string AmountInWords => ((int)this.Amount).ToWords() + " Rupees Only";
			public DateTime ApplyDate { get; internal set; }
			public DateTime DueDate => this.AdmissionProcessingFeeDueDate;
			internal short SemesterID { get; set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public string InstituteCode { get; internal set; }
			public DateTime AdmissionProcessingFeeDueDate { get; internal set; }
			public bool Paid { get; internal set; }

			public List<AdmissionProcessingFeeChallan> GetData()
			{
				return null;
			}
		}

		public static List<AdmissionProcessingFeeChallan> GetAdmissionProcessingFeeChallans(int candidateAppliedProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var candidateAppliedPrograms = aspireContext.CandidateAppliedPrograms.Where(cap => cap.CandidateAppliedProgramID == candidateAppliedProgramID && cap.Candidate.ForeignStudent == false);
				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms.AsQueryable();
				var now = DateTime.Now;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
						admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.Program.InstituteID == staffLoginHistory.InstituteID);
						break;
					case UserTypes.Candidate:
						admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.AdmissionProcessingFeeDueDate >= now);
						candidateAppliedPrograms = candidateAppliedPrograms
							.Where(cap => cap.CandidateID == loginHistory.CandidateID.Value)
							.Where(cap => cap.AccountTransactionID == null);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				var challans = (from cap in candidateAppliedPrograms
								join aop in admissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
								select new AdmissionProcessingFeeChallan
								{
									CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
									ChallanNo = cap.ChallanNo,
									SemesterID = aop.SemesterID,
									Name = cap.Candidate.Name,
									FatherName = cap.Candidate.FatherName,
									ProgramShortName = aop.Program.ProgramShortName,
									Duration = aop.Program.Duration,
									Amount = cap.Amount,
									AdmissionProcessingFeeDueDate = aop.AdmissionProcessingFeeDueDate,
									ApplyDate = cap.ApplyDate,
									InstituteID = aop.Program.InstituteID,
									InstituteCode = aop.Program.Institute.InstituteCode,
									InstituteName = aop.Program.Institute.InstituteShortName,
									InstitutePhone = aop.Program.Institute.Phone,
									Paid = cap.AccountTransactionID != null,
								}).ToList();
				if (challans.Any())
					foreach (var admissionProcessingFeeChallan in challans)
						admissionProcessingFeeChallan.BankAccounts = aspireContext.InstituteBankAccounts.Where(iba => iba.InstituteID == admissionProcessingFeeChallan.InstituteID && iba.FeeType == (byte)InstituteBankAccount.FeeTypes.AdmissionProcessingFee && iba.Active).Select(iba => new AdmissionProcessingFeeChallan.BankAccount
						{
							AccountNo = iba.AccountNo,
							AccountTitle = iba.AccountTitle,
							BankName = iba.Bank.BankName,
							BranchName = iba.BranchName,
							AccountTitleForDisplay = iba.AccountTitleForDisplay,
							AccountNoForDisplay = iba.AccountNoForDisplay,
							BranchForDisplay = iba.BranchForDisplay,
							InstituteBankAccountID = iba.InstituteBankAccountID,
							BranchCode = iba.BranchCode,
							BranchAddress = iba.BranchAddress,
							BankNameForDisplay = iba.Bank.BankNameForDisplay,
						}).ToList();
				return challans;
			}
		}

		public sealed class AdmitSlip
		{
			public byte[] Photo { get; internal set; }
			public int CandidateID { get; internal set; }
			public int CandidateAppliedProgramID { get; internal set; }
			public int ApplicationNo { get; internal set; }
			internal short SemesterID { get; set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public string Name { get; internal set; }
			public string FatherName { get; internal set; }
			public string CNIC { get; internal set; }
			public string InstituteShortName { get; internal set; }
			public string Program1 { get; internal set; }
			public string Program2 { get; internal set; }
			public string Program3 { get; internal set; }
			public string Program4 { get; internal set; }

			internal byte EntryTestType { get; set; }
			public AdmissionOpenProgram.EntryTestTypes EntryTestTypeEnum => (AdmissionOpenProgram.EntryTestTypes)this.EntryTestType;
			internal byte? EntryTestDuration { get; set; }
			internal DateTime? EntryTestStartDate { get; set; }
			internal DateTime? EntryTestEndDate { get; set; }
			internal string TestCentreInstitute { get; set; }
			internal string TestCentreName { get; set; }
			internal string TestCentreLabName { get; set; }
			internal DateTime? CBTSessionStartTime { get; set; }
			internal byte? CBTSessionDurationInMinutes { get; set; }
			internal string CBTSession => this.CBTSessionStartTime == null ? null : $"{this.CBTSessionStartTime:D}, Timing: {this.CBTSessionStartTime:hh:mm tt} - {(this.CBTSessionStartTime.Value.AddMinutes(this.CBTSessionDurationInMinutes ?? throw new InvalidOperationException())):hh:mm tt}";
			public string EntryTestDateTime
			{
				get
				{
					switch (this.EntryTestTypeEnum)
					{
						case AdmissionOpenProgram.EntryTestTypes.None:
							return string.Empty.ToNAIfNullOrEmpty();
						case AdmissionOpenProgram.EntryTestTypes.ComputerBasedTest:
							return this.CBTSession;
						case AdmissionOpenProgram.EntryTestTypes.PaperBasedTest:
							return $"{this.EntryTestStartDate:f}";
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
			public string TestCentre
			{
				get
				{
					switch (this.EntryTestTypeEnum)
					{
						case AdmissionOpenProgram.EntryTestTypes.None:
							return string.Empty.ToNAIfNullOrEmpty();
						case AdmissionOpenProgram.EntryTestTypes.ComputerBasedTest:
							return this.TestCentreName;
						case AdmissionOpenProgram.EntryTestTypes.PaperBasedTest:
							return this.TestCentreInstitute;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
			public string TestCentreLab
			{
				get
				{
					switch (this.EntryTestTypeEnum)
					{
						case AdmissionOpenProgram.EntryTestTypes.None:
						case AdmissionOpenProgram.EntryTestTypes.PaperBasedTest:
							return string.Empty.ToNAIfNullOrEmpty();
						case AdmissionOpenProgram.EntryTestTypes.ComputerBasedTest:
							return this.TestCentreLabName;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
			internal DateTime? InterviewsStartDate { get; set; }
			internal DateTime? InterviewsEndDate { get; set; }
			public string InterviewDates => this.InterviewsStartDate.ConcatenateDates(this.InterviewsEndDate, "dd-MMM-yyyy", " To ").ToNAIfNullOrEmpty();

			public bool IsValid => this.EntryTestTypeEnum != AdmissionOpenProgram.EntryTestTypes.ComputerBasedTest || this.CBTSessionStartTime != null;

			public List<AdmitSlip> GetData()
			{
				return null;
			}
		}

		public static List<AdmitSlip> GetAdmitSlip(int candidateID, int? candidateAppliedProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var candidateAppliedPrograms = aspireContext.CandidateAppliedPrograms
					.Where(cap => cap.CandidateID == candidateID && cap.Candidate.ForeignStudent == false)
					.Where(cap => cap.AccountTransactionID != null);
				if (candidateAppliedProgramID != null)
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.CandidateAppliedProgramID == candidateAppliedProgramID.Value);
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandStaffPermissions(loginHistory.LoginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
						candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.AdmissionOpenProgram1.Program.InstituteID == staffLoginHistory.InstituteID);
						break;
					case UserTypes.Candidate:
						candidateAppliedPrograms = candidateAppliedPrograms
							.Where(cap => cap.CandidateID == loginHistory.CandidateID.Value)
							.Where(cap => cap.EntryTestDate == null || cap.EntryTestDate >= DateTime.Now);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				var admitSlips = candidateAppliedPrograms.Select(cap => new AdmitSlip
				{
					CandidateID = cap.CandidateID,
					CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
					ApplicationNo = cap.ApplicationNo.Value,
					SemesterID = cap.AdmissionOpenProgram1.SemesterID,
					Name = cap.Candidate.Name,
					FatherName = cap.Candidate.FatherName,
					CNIC = cap.Candidate.CNIC,
					InstituteShortName = cap.AdmissionOpenProgram1.Program.Institute.InstituteShortName,
					Program1 = cap.AdmissionOpenProgram1.Program.ProgramShortName,
					Program2 = cap.AdmissionOpenProgram2.Program.ProgramShortName,
					Program3 = cap.AdmissionOpenProgram3.Program.ProgramShortName,
					Program4 = cap.AdmissionOpenProgram4.Program.ProgramShortName,
					EntryTestType = cap.AdmissionOpenProgram1.EntryTestType,
					EntryTestStartDate = cap.AdmissionOpenProgram1.EntryTestStartDate,
					EntryTestEndDate = cap.AdmissionOpenProgram1.EntryTestEndDate,
					EntryTestDuration = cap.AdmissionOpenProgram1.EntryTestDuration,
					InterviewsStartDate = cap.AdmissionOpenProgram1.InterviewsStartDate,
					InterviewsEndDate = cap.AdmissionOpenProgram1.InterviewsEndDate,
					TestCentreInstitute = cap.CBTSessionLab.CBTLab.Institute.InstituteShortName,
					TestCentreLabName = cap.CBTSessionLab.CBTLab.LabName,
					CBTSessionStartTime = cap.CBTSessionLab.CBTSession.StartTime,
					CBTSessionDurationInMinutes = cap.CBTSessionLab.CBTSession.DurationInMinutes,
					TestCentreName = cap.CBTSessionLab.CBTLab.TestCentreName,
				}).ToList();

				if (admitSlips.Any())
				{
					var photo = ProfileInformation.GetCandidateProfilePicture(candidateID, loginSessionGuid);
					foreach (var admitSlip in admitSlips)
					{
						admitSlip.Photo = photo;
						admitSlip.Program2 = admitSlip.Program2.ToNAIfNullOrEmpty();
						admitSlip.Program3 = admitSlip.Program3.ToNAIfNullOrEmpty();
						admitSlip.Program4 = admitSlip.Program4.ToNAIfNullOrEmpty();
					}
				}
				return admitSlips;
			}
		}
	}
}
