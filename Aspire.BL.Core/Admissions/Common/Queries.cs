﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Common
{
	internal static class Queries
	{
		internal static IQueryable<AdmissionOpenProgram> FilterOpenAdmissions(this IQueryable<AdmissionOpenProgram> admissionOpenPrograms, bool? foreignStudents)
		{
			var now = DateTime.Now;
			switch (foreignStudents)
			{
				case null:
					return admissionOpenPrograms.Where(aop => (aop.AdmissionOpenDate <= now && now <= aop.AdmissionClosingDate)
						|| (aop.AdmissionOpenDateForForeigners != null && aop.AdmissionClosingDateForForeigners != null && aop.AdmissionOpenDateForForeigners.Value <= now && now <= aop.AdmissionClosingDateForForeigners.Value));
				case true:
					return admissionOpenPrograms.Where(aop => aop.AdmissionOpenDateForForeigners != null && aop.AdmissionClosingDateForForeigners != null && aop.AdmissionOpenDateForForeigners.Value <= now && now <= aop.AdmissionClosingDateForForeigners.Value);
				default:
					return admissionOpenPrograms.Where(aop => aop.AdmissionOpenDate <= now && now <= aop.AdmissionClosingDate);
			}
		}

		internal static IQueryable<AdmissionOpenProgram> GetAdmissionOpenPrograms(this AspireContext aspireContext, short semesterID)
		{
			return aspireContext.AdmissionOpenPrograms.FilterOpenAdmissions(null).Where(aop => aop.SemesterID == semesterID);
		}
	}
}
