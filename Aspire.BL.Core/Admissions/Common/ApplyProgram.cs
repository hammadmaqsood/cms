﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Common
{
	public static class ApplyProgram
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissionsForStaff(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissionsForStaff(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.ChangeAppliedProgram, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		private static IQueryable<AdmissionOpenProgram> GetNotAppliedAdmissionOpenPrograms(this AspireContext aspireContext, int candidateID)
		{
			var now = DateTime.Now;
			return from c in aspireContext.Candidates
				   join aop in aspireContext.AdmissionOpenPrograms on c.SemesterID equals aop.SemesterID
				   where c.CandidateID == candidateID && aop.AdmissionOpenDate <= now && now <= aop.AdmissionClosingDate
						 && c.CandidateAppliedPrograms.All(cap => cap.Choice1AdmissionOpenProgramID != aop.AdmissionOpenProgramID)
				   select aop;
		}

		public sealed class GetAdmissionsOpenInstitutesResult
		{
			public Model.Entities.Candidate.Statuses CandidateStatus { get; internal set; }
			public List<CustomListItem> Institutes { get; internal set; }
		}

		public static GetAdmissionsOpenInstitutesResult GetNotAppliedAdmissionsOpenInstitutes(int candidateID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				IQueryable<AdmissionOpenProgram> admissionOpenPrograms;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandModulePermissionsForStaff(loginSessionGuid);
						admissionOpenPrograms = aspireContext.GetNotAppliedAdmissionOpenPrograms(candidateID).Where(aop => aop.Program.InstituteID == staffLoginHistory.InstituteID);
						break;
					case UserTypes.Candidate:
						if (candidateID != loginHistory.CandidateID)
							throw new ArgumentException();
						admissionOpenPrograms = aspireContext.GetNotAppliedAdmissionOpenPrograms(candidateID);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				return new GetAdmissionsOpenInstitutesResult
				{
					CandidateStatus = (Model.Entities.Candidate.Statuses)aspireContext.Candidates.Where(c => c.CandidateID == candidateID).Select(c => c.Status).Single(),
					Institutes = admissionOpenPrograms.Select(p => new CustomListItem
					{
						ID = p.Program.InstituteID,
						Text = p.Program.Institute.InstituteName,
					}).Distinct().OrderBy(i => i.Text).ToList()
				};
			}
		}

		public static List<CustomListItem> GetNotAppliedAdmissionsOpenPrograms(int candidateID, int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var admissionOpenPrograms = aspireContext.GetNotAppliedAdmissionOpenPrograms(candidateID);
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandModulePermissionsForStaff(loginSessionGuid);
						admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.Program.InstituteID == staffLoginHistory.InstituteID);
						break;
					case UserTypes.Candidate:
						if (candidateID != loginHistory.CandidateID)
							throw new InvalidOperationException();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				return admissionOpenPrograms.Where(p => p.Program.InstituteID == instituteID)
					.Select(p => new CustomListItem
					{
						ID = p.AdmissionOpenProgramID,
						Text = p.Program.ProgramShortName,
					}).OrderBy(o => o.Text).ToList();
			}
		}

		public sealed class GetAdmissionOpenProgramAndCriteriaResult
		{
			public sealed class OpenProgram
			{
				public int AdmissionCriteriaID { get; internal set; }
				public int AdmissionOpenProgramID { get; internal set; }
				public short AdmissionProcessingFee { get; internal set; }
				public string EligibilityCriteriaStatement { get; internal set; }
				public byte? EntryTestDuration { get; internal set; }
				public DateTime? InterviewsEndDate { get; internal set; }
				public DateTime? InterviewsStartDate { get; internal set; }
				public byte Shifts { get; internal set; }
				public IEnumerable<Shifts> ShiftFlags => ((Shifts)this.Shifts).GetFlags();
				public byte EntryTestType { get; internal set; }
				public AdmissionOpenProgram.EntryTestTypes EntryTestTypeEnum => (AdmissionOpenProgram.EntryTestTypes)this.EntryTestType;
				public DateTime? EntryTestStartDate { get; internal set; }
				public DateTime? EntryTestEndDate { get; internal set; }
				public short SemesterID { get; internal set; }
			}

			public OpenProgram AdmissionOpenProgram { get; internal set; }
			public List<CustomListItem> PreReqQualification { get; internal set; }
			public List<CustomListItem> Choices { get; internal set; }
		}

		public static GetAdmissionOpenProgramAndCriteriaResult GetAdmissionOpenProgramAndCriteria(int candidateID, int admissionOpenProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var admissionOpenPrograms = aspireContext.GetNotAppliedAdmissionOpenPrograms(candidateID);

				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandModulePermissionsForStaff(loginSessionGuid);
						admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.Program.InstituteID == staffLoginHistory.InstituteID);
						break;
					case UserTypes.Candidate:
						if (candidateID != loginHistory.CandidateID)
							throw new InvalidOperationException();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				var admissionOpenProgram = admissionOpenPrograms.Where(p => p.AdmissionOpenProgramID == admissionOpenProgramID)
					.Select(p => new GetAdmissionOpenProgramAndCriteriaResult.OpenProgram
					{
						AdmissionOpenProgramID = p.AdmissionOpenProgramID,
						SemesterID = p.SemesterID,
						AdmissionCriteriaID = p.AdmissionCriteriaID,
						EligibilityCriteriaStatement = p.EligibilityCriteriaStatement,
						AdmissionProcessingFee = p.AdmissionProcessingFee,
						EntryTestType = p.EntryTestType,
						EntryTestStartDate = p.EntryTestStartDate,
						EntryTestEndDate = p.EntryTestEndDate,
						EntryTestDuration = p.EntryTestDuration,
						InterviewsStartDate = p.InterviewsStartDate,
						InterviewsEndDate = p.InterviewsEndDate,
						Shifts = p.Shifts,
					}).SingleOrDefault();
				if (admissionOpenProgram == null)
					return null;

				return new GetAdmissionOpenProgramAndCriteriaResult
				{
					AdmissionOpenProgram = admissionOpenProgram,
					PreReqQualification = aspireContext.AdmissionCriteriaPreReqs
					.Where(c => c.AdmissionCriteriaID == admissionOpenProgram.AdmissionCriteriaID)
					.Select(c => new CustomListItem
					{
						ID = c.AdmissionCriteriaID,
						Text = c.PreReqQualification
					}).Distinct().OrderBy(q => q.ID).ToList(),
					Choices = (from aope in aspireContext.AdmissionOpenProgramEquivalents
							   join aop in aspireContext.GetAdmissionOpenPrograms(admissionOpenProgram.SemesterID) on aope.EquivalentAdmissionOpenProgramID equals aop.AdmissionOpenProgramID
							   where aope.AdmissionOpenProgramID == admissionOpenProgram.AdmissionOpenProgramID
							   select new CustomListItem
							   {
								   ID = aop.AdmissionOpenProgramID,
								   Text = aop.Program.ProgramShortName,
							   }).OrderBy(i => i.Text).ToList(),
				};
			}
		}

		public static List<CustomListItem> GetAdmissionCriteriaPreReqs(int admissionCriteriaID, string preReqQualification, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var admissionCriteriaPreReqs = aspireContext.AdmissionCriteriaPreReqs.Where(acpr => acpr.AdmissionCriteriaID == admissionCriteriaID);
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandModulePermissionsForStaff(loginSessionGuid);
						admissionCriteriaPreReqs = admissionCriteriaPreReqs
							.Where(acpr => acpr.AdmissionCriteria.InstituteID == staffLoginHistory.InstituteID);
						break;
					case UserTypes.Candidate:
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				return admissionCriteriaPreReqs.Where(c => c.AdmissionCriteriaID == admissionCriteriaID && c.PreReqQualification == preReqQualification)
					.Select(c => new CustomListItem
					{
						ID = c.AdmissionCriteriaPreReqID,
						Text = c.PreReqQualificationResultStatus
					}).OrderBy(i => i.Text).ToList();
			}
		}

		public sealed class PreReqDegreeType
		{
			public byte DegreeType { get; internal set; }
			public byte DisplaySequence { get; internal set; }
			public int AdmissionCriteriaPreReqDegreeID { get; internal set; }
		}

		public static List<PreReqDegreeType> GetAdmissionCriteriaPreReqDegreeTypes(int admissionCriteriaPreReqID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var admissionCriteriaPreReqDegrees = aspireContext.AdmissionCriteriaPreReqDegrees
					.Where(c => c.AdmissionCriteriaPreReqID == admissionCriteriaPreReqID);
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandModulePermissionsForStaff(loginSessionGuid);
						admissionCriteriaPreReqDegrees = admissionCriteriaPreReqDegrees
							.Where(acprd => acprd.AdmissionCriteriaPreReq.AdmissionCriteria.InstituteID == staffLoginHistory.InstituteID);
						break;
					case UserTypes.Candidate:
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				return admissionCriteriaPreReqDegrees.Select(c => new PreReqDegreeType
				{
					AdmissionCriteriaPreReqDegreeID = c.AdmissionCriteriaPreReqDegreeID,
					DegreeType = c.DegreeType,
					DisplaySequence = c.DisplaySequence,
				}).ToList();
			}
		}

		public static AdmissionCriteriaPreReqDegree GetAdmissionCriteriaPreReqDegree(int admissionCriteriaPreReqDegreeID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var admissionCriteriaPreReqDegrees = aspireContext.AdmissionCriteriaPreReqDegrees
					.Include(c => c.AdmissionCriteriaPreReqDegreeSubjects)
					.Where(c => c.AdmissionCriteriaPreReqDegreeID == admissionCriteriaPreReqDegreeID);
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandModulePermissionsForStaff(loginSessionGuid);
						admissionCriteriaPreReqDegrees = admissionCriteriaPreReqDegrees
							.Where(acprd => acprd.AdmissionCriteriaPreReq.AdmissionCriteria.InstituteID == staffLoginHistory.InstituteID);
						break;
					case UserTypes.Candidate:
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				return admissionCriteriaPreReqDegrees.SingleOrDefault();
			}
		}
	}
}
