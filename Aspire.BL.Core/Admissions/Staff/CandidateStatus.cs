﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Staff
{
	public static class CandidateStatus
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.ChangeCandidateStatus, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class CandidateAppliedProgram
		{
			internal CandidateAppliedProgram() { }
			public int CandidateAppliedProgramID { get; internal set; }
			public int ApplicationNo { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Name { get; internal set; }
			public string FatherName { get; internal set; }
			public int Choice1AdmissionOpenProgramID { get; internal set; }
			public int? AdmittedAdmissionOpenProgramID { get; internal set; }
			public byte? AdmittedShift { get; internal set; }
			public Shifts? AdmittedShiftEnum => (Shifts?)this.AdmittedShift;
			public bool EnrollmentGenerated { get; internal set; }
			public string Enrollment { get; internal set; }
			public bool FeeChallanGenerated { get; internal set; }
			public bool FeePaid { get; internal set; }
			public int CandidateID { get; internal set; }
			public string Choice1ProgramAlias { get; internal set; }
			public string AdmittedProgramAlias { get; internal set; }
			public byte Shift { get; internal set; }
			public Shifts ShiftEnum => (Shifts)this.Shift;
			public byte? Status { get; internal set; }
			public Model.Entities.CandidateAppliedProgram.Statuses? StatusEnum => (Model.Entities.CandidateAppliedProgram.Statuses?)this.Status;
			public DateTime? StatusDate { get; internal set; }
			public DateTime? DeferredDate { get; internal set; }
			public short? DeferredToSemesterID { get; internal set; }
			public int? DeferredToAdmittedAdmissionOpenProgramID { get; internal set; }
			public byte? DeferredToShift { get; internal set; }
		}

		public static CandidateAppliedProgram GetCandidateAppliedProgram(int applicationNo, short semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return (from cap in aspireContext.CandidateAppliedPrograms
						join aop in aspireContext.AdmissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
						where cap.ApplicationNo == applicationNo && aop.SemesterID == semesterID && aop.Program.InstituteID == loginHistory.InstituteID
							&& cap.AccountTransactionID != null
						select new CandidateAppliedProgram
						{
							CandidateID = cap.CandidateID,
							CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
							Name = cap.Candidate.Name,
							FatherName = cap.Candidate.FatherName,
							SemesterID = aop.SemesterID,
							ApplicationNo = cap.ApplicationNo.Value,
							Shift = cap.Shift,
							Choice1AdmissionOpenProgramID = cap.Choice1AdmissionOpenProgramID,
							Choice1ProgramAlias = aop.Program.ProgramAlias,
							AdmittedAdmissionOpenProgramID = cap.AdmittedAdmissionOpenProgramID,
							AdmittedProgramAlias = aspireContext.AdmissionOpenPrograms.Where(_aop => _aop.AdmissionOpenProgramID == cap.AdmittedAdmissionOpenProgramID).Select(_aop => _aop.Program.ProgramAlias).FirstOrDefault(),
							AdmittedShift = cap.Shift,
							EnrollmentGenerated = cap.StudentID != null && cap.EnrollmentGenerated != null,
							Enrollment = cap.Student.Enrollment,
							FeeChallanGenerated = cap.StudentFees.Any(),
							FeePaid = cap.StudentFees.Any(sf => sf.Status != StudentFee.StatusNotPaid),
							Status = cap.Status,
							StatusDate = cap.StatusDate,
							DeferredDate = cap.DeferredDate,
							DeferredToAdmittedAdmissionOpenProgramID = cap.DeferredToAdmittedAdmissionOpenProgramID,
							DeferredToShift = cap.DeferredToShift,
							DeferredToSemesterID = aspireContext.AdmissionOpenPrograms.Where(_aop => _aop.AdmissionOpenProgramID == cap.DeferredToAdmittedAdmissionOpenProgramID).Select(_aop => (short?)_aop.SemesterID).FirstOrDefault(),
						}).SingleOrDefault();
			}
		}

		public enum ChangeCandidateAppliedProgramStatusStatuses
		{
			NoRecordFound,
			InterviewNotCleared,
			FeeMustBePaidWhenDeferring,
			FeeMustBePaidWhenMakeItRefunded,
			RefundedCaseCannotBeDeferred,
			EnrollmentHasBeenGenerated,
			Success,
		}

		public static ChangeCandidateAppliedProgramStatusStatuses ChangeCandidateAppliedProgramStatus(int candidateAppliedProgramID, Model.Entities.CandidateAppliedProgram.Statuses statusEnum, DateTime? deferrredDate, int? deferredToAdmissionOpenProgramID, Shifts? deferredToShiftEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var candidateAppliedProgram = aspireContext.CandidateAppliedPrograms.SingleOrDefault(cap => cap.CandidateAppliedProgramID == candidateAppliedProgramID);
				if (candidateAppliedProgram == null)
					return ChangeCandidateAppliedProgramStatusStatuses.NoRecordFound;

				if (candidateAppliedProgram.AdmittedAdmissionOpenProgramID == null)
					return ChangeCandidateAppliedProgramStatusStatuses.InterviewNotCleared;

				var admittedAdmissionOpenProgram = aspireContext.AdmissionOpenPrograms
					.Where(aop => aop.AdmissionOpenProgramID == candidateAppliedProgram.AdmittedAdmissionOpenProgramID)
					.Select(aop => new
					{
						aop.SemesterID,
						aop.Program.InstituteID,
						aop.Program.DepartmentID,
						aop.ProgramID,
						aop.AdmissionOpenProgramID,
					}).Single();

				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, admittedAdmissionOpenProgram.InstituteID, admittedAdmissionOpenProgram.DepartmentID, admittedAdmissionOpenProgram.ProgramID, admittedAdmissionOpenProgram.AdmissionOpenProgramID);

				if (candidateAppliedProgram.EnrollmentGenerated != null)
					return ChangeCandidateAppliedProgramStatusStatuses.EnrollmentHasBeenGenerated;

				if (candidateAppliedProgram.StatusEnum != statusEnum)
				{
					candidateAppliedProgram.StatusEnum = statusEnum;
					candidateAppliedProgram.StatusDate = DateTime.Now;
				}
				if (candidateAppliedProgram.DeferredDate != deferrredDate)
					candidateAppliedProgram.DeferredDate = deferrredDate;
				if (candidateAppliedProgram.DeferredToAdmittedAdmissionOpenProgramID != deferredToAdmissionOpenProgramID)
					candidateAppliedProgram.DeferredToAdmittedAdmissionOpenProgramID = deferredToAdmissionOpenProgramID;
				if (candidateAppliedProgram.DeferredToShiftEnum != deferredToShiftEnum)
				{
					candidateAppliedProgram.DeferredToShiftEnum = deferredToShiftEnum;
					if (candidateAppliedProgram.DeferredToAdmittedAdmissionOpenProgramID == null)
						candidateAppliedProgram.DeferredToShift = null;
				}

				if (candidateAppliedProgram.DeferredToAdmittedAdmissionOpenProgramID != null)
				{
					if (candidateAppliedProgram.DeferredDate == null)
						throw new ArgumentNullException(nameof(deferrredDate));
					if (candidateAppliedProgram.DeferredToShift == null)
						throw new ArgumentNullException(nameof(deferredToShiftEnum));

					var deferredAdmissionOpenProgram = aspireContext.AdmissionOpenPrograms
						.Where(aop => aop.AdmissionOpenProgramID == candidateAppliedProgram.DeferredToAdmittedAdmissionOpenProgramID.Value)
						.Select(aop => new
						{
							aop.SemesterID
						}).Single();
					if (deferredAdmissionOpenProgram.SemesterID <= admittedAdmissionOpenProgram.SemesterID)
						throw new InvalidOperationException("Deferred semester cannot be current or previous semester.");
				}

				var feePaid = aspireContext.StudentFees.Any(sf => sf.CandidateAppliedProgramID == candidateAppliedProgram.CandidateAppliedProgramID && sf.Status != StudentFee.StatusNotPaid);
				if (candidateAppliedProgram.DeferredDate != null)
				{
					if (candidateAppliedProgram.AdmittedAdmissionOpenProgramID == null)
						return ChangeCandidateAppliedProgramStatusStatuses.InterviewNotCleared;
					if (feePaid == false)
						return ChangeCandidateAppliedProgramStatusStatuses.FeeMustBePaidWhenDeferring;
				}

				switch (candidateAppliedProgram.StatusEnum)
				{
					case Model.Entities.CandidateAppliedProgram.Statuses.None:
						break;
					case Model.Entities.CandidateAppliedProgram.Statuses.Refunded:
						if (feePaid == false)
							return ChangeCandidateAppliedProgramStatusStatuses.FeeMustBePaidWhenMakeItRefunded;
						if (candidateAppliedProgram.DeferredDate != null)
							return ChangeCandidateAppliedProgramStatusStatuses.RefundedCaseCannotBeDeferred;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return ChangeCandidateAppliedProgramStatusStatuses.Success;
			}
		}
	}
}