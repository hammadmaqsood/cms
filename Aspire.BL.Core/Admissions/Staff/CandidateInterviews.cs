﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FeeManagement.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Staff
{
	public static class CandidateInterviews
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.ManageInterviews, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class CandidateInterview
		{
			public sealed class AdmissionOpenProgram
			{
				internal AdmissionOpenProgram() { }
				public int AdmissionOpenProgramID { get; internal set; }
				public string ProgramAlias { get; internal set; }
				public byte Shifts { get; internal set; }
			}

			internal CandidateInterview() { }
			public int CandidateAppliedProgramID { get; internal set; }
			public bool ForeignStudent { get; internal set; }
			public int ApplicationNo { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Name { get; internal set; }
			public string FatherName { get; internal set; }
			public string CNIC { get; internal set; }
			public byte Shift { get; internal set; }
			public Shifts ShiftEnum => (Shifts)this.Shift;
			public byte Category { get; internal set; }
			public Categories CategoryEnum => (Categories)this.Category;

			public int Choice1AdmissionOpenProgramID { get; internal set; }
			public int? Choice2AdmissionOpenProgramID { get; internal set; }
			public int? Choice3AdmissionOpenProgramID { get; internal set; }
			public int? Choice4AdmissionOpenProgramID { get; internal set; }
			public int? AdmittedAdmissionOpenProgramID { get; internal set; }
			public byte? AdmittedShift { get; internal set; }
			public Shifts? AdmittedShiftEnum => (Shifts?)this.AdmittedShift;

			public List<AdmissionOpenProgram> AdmissionOpenPrograms { get; internal set; }
			public bool EnrollmentGenerated { get; internal set; }
			public bool FeeChallanGenerated { get; internal set; }
			public DateTime? DepositDate { get; internal set; }
			public int CandidateID { get; internal set; }
			public DateTime? InterviewDate { get; internal set; }
			public DateTime? AdmissionFeeLastDate { get; internal set; }
			public string InterviewRemarks { get; internal set; }
			public bool CanBeDeleted => (this.ForeignStudent || this.DepositDate != null) && this.InterviewDate != null && this.FeeChallanGenerated == false && this.EnrollmentGenerated == false;
			public bool CanBeEdited => (this.ForeignStudent || this.DepositDate != null) && this.EnrollmentGenerated == false;
		}

		public static CandidateInterview GetCandidateForInterview(int applicationNo, short semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return (from cap in aspireContext.CandidateAppliedPrograms
						join aop in aspireContext.AdmissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
						where cap.ApplicationNo == applicationNo && aop.SemesterID == semesterID && aop.Program.InstituteID == loginHistory.InstituteID
						select new CandidateInterview
						{
							CandidateID = cap.CandidateID,
							CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
							ForeignStudent = cap.Candidate.ForeignStudent,
							Name = cap.Candidate.Name,
							FatherName = cap.Candidate.FatherName,
							CNIC = cap.Candidate.CNIC,
							Category = cap.Candidate.Category.Value,
							SemesterID = aop.SemesterID,
							ApplicationNo = cap.ApplicationNo.Value,
							Shift = cap.Shift,
							Choice1AdmissionOpenProgramID = cap.Choice1AdmissionOpenProgramID,
							Choice2AdmissionOpenProgramID = cap.Choice2AdmissionOpenProgramID,
							Choice3AdmissionOpenProgramID = cap.Choice3AdmissionOpenProgramID,
							Choice4AdmissionOpenProgramID = cap.Choice4AdmissionOpenProgramID,
							AdmittedAdmissionOpenProgramID = cap.AdmittedAdmissionOpenProgramID,
							AdmittedShift = cap.AdmittedShift,
							EnrollmentGenerated = cap.StudentID != null && cap.EnrollmentGenerated != null,
							FeeChallanGenerated = cap.StudentFees.Any(),
							DepositDate = cap.AccountTransaction.TransactionDate,
							InterviewDate = cap.InterviewDate,
							AdmissionFeeLastDate = cap.AdmissionFeeLastDate,
							InterviewRemarks = cap.InterviewRemarks,
							AdmissionOpenPrograms = aspireContext.AdmissionOpenPrograms.Where(p => p.SemesterID == semesterID && p.Program.InstituteID == loginHistory.InstituteID).Select(p => new CandidateInterview.AdmissionOpenProgram
							{
								AdmissionOpenProgramID = p.AdmissionOpenProgramID,
								ProgramAlias = p.Program.ProgramAlias,
								Shifts = p.Shifts,
							}).ToList(),
						}).SingleOrDefault();
			}
		}

		public enum UpdateCandidateInterviewResults
		{
			Success,
			NoRecordFound,
			AdmissionProcessingFeeNotPaid,
			EnrollmentHasBeenGenerated,
			EntryTestRecordNotFound,
			AdmittedProgramMustBeDefinedBecauseFeeChallanHasBeenGenerated,
			FeeStructureNotfound,
			//InterviewRecordAlreadyExists,
			//FeeStructureNotfound,
			//EntryTestMarksPercentageIsNull,
		}

		public static UpdateCandidateInterviewResults UpdateCandidateInterview(int candidateAppliedProgramID, Shifts? admittedShiftEnum, int? admittedAdmissionOpenProgramID, DateTime interviewDate, DateTime? admissionFeeLastDate, string interviewRemarks, bool generateFeeChallan, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms.AsQueryable();
				var record = (from cap in aspireContext.CandidateAppliedPrograms
							  join aop in admissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
							  where cap.CandidateAppliedProgramID == candidateAppliedProgramID
							  select new
							  {
								  cap,
								  cap.Candidate.ForeignStudent,
								  aop.SemesterID,
								  Category = cap.Candidate.Category.Value,
								  aop.Program.InstituteID,
								  aop.Program.DepartmentID,
								  aop.ProgramID,
								  aop.AdmissionOpenProgramID,
								  aop.EntryTestType,
								  FeeGenerated = cap.StudentFees.Any(),
							  }).SingleOrDefault();
				if (record == null)
					return UpdateCandidateInterviewResults.NoRecordFound;
				if (record.cap.AccountTransactionID == null && !record.ForeignStudent)
					return UpdateCandidateInterviewResults.AdmissionProcessingFeeNotPaid;

				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
				var candidateAppliedProgram = record.cap;
				var semesterID = record.SemesterID;
				var categoryEnum = (Categories)record.Category;

				if (candidateAppliedProgram.EnrollmentGenerated != null)
					return UpdateCandidateInterviewResults.EnrollmentHasBeenGenerated;

				switch ((AdmissionOpenProgram.EntryTestTypes)record.EntryTestType)
				{
					case AdmissionOpenProgram.EntryTestTypes.None:
						break;
					case AdmissionOpenProgram.EntryTestTypes.ComputerBasedTest:
					case AdmissionOpenProgram.EntryTestTypes.PaperBasedTest:
						if (!record.ForeignStudent)
						{
							if (candidateAppliedProgram.EntryTestDate == null || candidateAppliedProgram.EntryTestMarksPercentage == null)
							{
								if (candidateAppliedProgram.ETSObtained == null)
									return UpdateCandidateInterviewResults.EntryTestRecordNotFound;
							}
						}
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				if (admittedAdmissionOpenProgramID == null)
				{
					if (record.FeeGenerated)
						return UpdateCandidateInterviewResults.AdmittedProgramMustBeDefinedBecauseFeeChallanHasBeenGenerated;
					candidateAppliedProgram.InterviewDate = interviewDate;
					candidateAppliedProgram.AdmittedAdmissionOpenProgramID = null;
					candidateAppliedProgram.AdmittedShiftEnum = null;
					candidateAppliedProgram.AdmissionFeeLastDate = null;
					candidateAppliedProgram.InterviewRemarks = interviewRemarks;
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				}
				else
				{
					var admittedAdmissionOpenProgram = admissionOpenPrograms.SingleOrDefault(aop => aop.AdmissionOpenProgramID == admittedAdmissionOpenProgramID
																				&& aop.SemesterID == semesterID
																				&& aop.Program.InstituteID == loginHistory.InstituteID);
					if (admittedAdmissionOpenProgram == null)
						return UpdateCandidateInterviewResults.NoRecordFound;

					candidateAppliedProgram.InterviewDate = interviewDate;
					candidateAppliedProgram.AdmittedAdmissionOpenProgramID = admittedAdmissionOpenProgram.AdmissionOpenProgramID;
					if (admittedShiftEnum == null)
						throw new ArgumentNullException(nameof(admittedShiftEnum));
					candidateAppliedProgram.AdmittedShiftEnum = admittedShiftEnum.Value;
					if (admissionFeeLastDate == null)
						throw new ArgumentNullException(nameof(admissionFeeLastDate));
					candidateAppliedProgram.AdmissionFeeLastDate = admissionFeeLastDate.Value;
					candidateAppliedProgram.InterviewRemarks = interviewRemarks;
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
					if (!categoryEnum.IsNaval() && record.FeeGenerated == false && generateFeeChallan)
					{
						#region Fee Challan Generation

						aspireContext.Entry(admittedAdmissionOpenProgram).Collection(aop => aop.FeeStructures).Query().Include(fs => fs.FeeStructureDetails.Select(fsd => fsd.FeeHead)).Load();
						if (!admittedAdmissionOpenProgram.FeeStructures.Any())
						{
							aspireContext.RollbackTransaction();
							return UpdateCandidateInterviewResults.FeeStructureNotfound;
						}
						var feeStructure = admittedAdmissionOpenProgram.FeeStructures.Single();
						var studentFee = new StudentFee
						{
							StudentFeeID = 0,
							StudentID = null,
							CreatedDate = DateTime.Now,
							CandidateAppliedProgramID = candidateAppliedProgramID,
							CreditHours = feeStructure.FirstSemesterCreditHours,
							NewAdmission = true,
							NoOfInstallmentsEnum = InstallmentNos.One,
							Remarks = null,
							SemesterID = admittedAdmissionOpenProgram.SemesterID,
							StatusEnum = StudentFee.Statuses.NotPaid,
							ConcessionAmount = 0,
							GrandTotalAmount = 0,
							TotalAmount = 0,
							FeeTypeEnum = StudentFee.FeeTypes.Challan,
							CreatedByUserLoginHistoryID = loginHistory.UserLoginHistoryID,
							ExemptWHT = false
						};
						foreach (var feeStructureDetail in feeStructure.FeeStructureDetails)
							if ((feeStructureDetail.SemesterNoEnum == null || feeStructureDetail.SemesterNoEnum == SemesterNos.One)
								&& (feeStructureDetail.CategoryEnum == null || feeStructureDetail.CategoryEnum == categoryEnum))
							{
								var studentFeeDetail = new StudentFeeDetail
								{
									StudentFeeDetailID = 0,
									FeeHeadID = feeStructureDetail.FeeHeadID,
								};
								switch (feeStructureDetail.FeeHead.FeeHeadTypeEnum)
								{
									case FeeHead.FeeHeadTypes.TutionFee:
										studentFeeDetail.TotalAmount = (int?)(feeStructureDetail.Amount * studentFee.CreditHours);
										studentFeeDetail.ConcessionAmount = null;
										break;
									case FeeHead.FeeHeadTypes.Arrears:
									case FeeHead.FeeHeadTypes.Custom:
										studentFeeDetail.TotalAmount = feeStructureDetail.Amount;
										studentFeeDetail.ConcessionAmount = null;
										break;
									default:
										throw new ArgumentOutOfRangeException();
								}
								studentFeeDetail.GrandTotalAmount = (studentFeeDetail.TotalAmount ?? 0) - (studentFeeDetail.ConcessionAmount ?? 0);
								switch (feeStructureDetail.FeeHead.RuleTypeEnum)
								{
									case FeeHead.RuleTypes.Credit:
										studentFee.TotalAmount += studentFeeDetail.TotalAmount ?? 0;
										studentFee.ConcessionAmount += studentFeeDetail.ConcessionAmount ?? 0;
										studentFee.GrandTotalAmount += studentFeeDetail.GrandTotalAmount;
										break;
									case FeeHead.RuleTypes.Debit:
										studentFee.TotalAmount -= studentFeeDetail.TotalAmount ?? 0;
										studentFee.ConcessionAmount -= studentFeeDetail.ConcessionAmount ?? 0;
										studentFee.GrandTotalAmount -= studentFeeDetail.GrandTotalAmount;
										break;
									default:
										throw new ArgumentOutOfRangeException();
								}
								studentFee.StudentFeeDetails.Add(studentFeeDetail);
							}
						studentFee.StudentFeeChallans.Add(new StudentFeeChallan
						{
							Amount = studentFee.GrandTotalAmount,
							DueDate = admissionFeeLastDate.Value,
							StatusEnum = StudentFeeChallan.Statuses.NotPaid,
							InstallmentNoEnum = InstallmentNos.One,
						});
						aspireContext.StudentFees.Add(studentFee.ValidateForAdd(feeStructure.FeeStructureDetails.Select(fsd => fsd.FeeHead).ToList()));
						aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
						aspireContext.CalculateAndGenerateFeeChallanForWithholdingTax(studentFee.StudentFeeID, loginHistory.UserLoginHistoryID);

						#endregion
					}
				}

				aspireContext.CommitTransaction();
				return UpdateCandidateInterviewResults.Success;
			}
		}

		public enum DeleteCandidateInterviewInfoResults
		{
			NoRecordFound,
			AdmissionProcessingFeeNotPaid,
			InterviewRecordNotExists,
			CannotDeleteFeeGenerated,
			CannotDeleteEnrollmentGenerated,
			Success,
		}

		public static DeleteCandidateInterviewInfoResults DeleteCandidateInterviewInfo(int candidateAppliedProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms.AsQueryable();
				var record = (from cap in aspireContext.CandidateAppliedPrograms
							  join aop in admissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
							  where cap.CandidateAppliedProgramID == candidateAppliedProgramID
							  select new
							  {
								  cap,
								  cap.Candidate.ForeignStudent,
								  cap.Candidate.SemesterID,
								  Category = cap.Candidate.Category.Value,
								  aop.Program.InstituteID,
								  aop.Program.DepartmentID,
								  aop.ProgramID,
								  aop.AdmissionOpenProgramID,
								  FeeGenerated = cap.StudentFees.Any(),
							  }).SingleOrDefault();
				if (record == null)
					return DeleteCandidateInterviewInfoResults.NoRecordFound;
				if (!record.ForeignStudent && record.cap.AccountTransactionID == null)
					return DeleteCandidateInterviewInfoResults.AdmissionProcessingFeeNotPaid;
				if (record.FeeGenerated)
					return DeleteCandidateInterviewInfoResults.CannotDeleteFeeGenerated;

				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
				var candidateAppliedProgram = record.cap;
				if (candidateAppliedProgram.EnrollmentGenerated != null)
					return DeleteCandidateInterviewInfoResults.CannotDeleteEnrollmentGenerated;

				if (candidateAppliedProgram.InterviewDate == null)
					return DeleteCandidateInterviewInfoResults.InterviewRecordNotExists;

				candidateAppliedProgram.InterviewDate = null;
				candidateAppliedProgram.AdmittedAdmissionOpenProgramID = null;
				candidateAppliedProgram.AdmittedShift = null;
				candidateAppliedProgram.AdmissionFeeLastDate = null;
				candidateAppliedProgram.InterviewRemarks = null;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteCandidateInterviewInfoResults.Success;
			}
		}

		public static Shifts? GetAdmissionOpenProgramShifts(int admissionOpenProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return (Shifts?)aspireContext.AdmissionOpenPrograms.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID && aop.Program.InstituteID == loginHistory.InstituteID).Select(aop => (byte?)aop.Shifts).SingleOrDefault();
			}
		}
	}
}