﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FileServer;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Staff
{
	public static class EnrollmentGeneration
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.GenerateEnrollments, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		internal static string GetEnrollmentFormat(this AspireContext aspireContext, int admissionOpenProgramID, Shifts shiftEnum)
		{
			var programInfo = aspireContext.AdmissionOpenPrograms.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID).Select(aop => new
			{
				aop.Program.Institute.InstituteCode,
				aop.Program.ProgramCode,
				aop.SemesterID,
				aop.Semester.Year,
				SemesterTypeEnum = (SemesterTypes)aop.Semester.SemesterType
			}).Single();
			if (string.IsNullOrWhiteSpace(programInfo.ProgramCode))
				return null;
			string shiftCode;
			switch (shiftEnum)
			{
				case Shifts.Morning:
					shiftCode = "1";
					break;
				case Shifts.Evening:
					shiftCode = "2";
					break;
				case Shifts.Weekend:
					shiftCode = "3";
					break;
				default:
					throw new NotImplementedEnumException(shiftEnum);
			}
			string semesterCode;
			switch (programInfo.SemesterTypeEnum)
			{
				case SemesterTypes.Spring:
					semesterCode = "1";
					break;
				case SemesterTypes.Summer:
					semesterCode = "3";
					break;
				case SemesterTypes.Fall:
					semesterCode = "2";
					break;
				default:
					throw new NotImplementedEnumException(programInfo.SemesterTypeEnum);
			}
			return $"{programInfo.InstituteCode}-{shiftCode}{programInfo.ProgramCode}{programInfo.Year.ToString().Substring(2)}{semesterCode}-";
		}

		internal static string GetMaxEnrollmentGenerated(this AspireContext aspireContext, int instituteID, string format)
		{
			if (string.IsNullOrWhiteSpace(format))
				return null;
			return aspireContext.Students
				.Where(s => s.AdmissionOpenProgram.Program.InstituteID == instituteID && s.Enrollment.StartsWith(format))
				.OrderByDescending(s => s.Enrollment).Select(s => s.Enrollment).FirstOrDefault();
		}

		public sealed class Candidate
		{
			internal Candidate() { }
			public int? CandidateAppliedProgramID { get; internal set; }
			public int? StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public int? ApplicationNo { get; internal set; }
			public string Name { get; internal set; }
			public string FatherName { get; internal set; }
			public byte? Gender { get; internal set; }
			public Genders? GenderEnum => (Genders?)this.Gender;
			public string GenderFullName => this.GenderEnum?.ToFullName();
			public string Remarks { get; internal set; }
		}

		public static List<Candidate> GetCandidatesForEnrollmentGeneration(bool deferred, bool deferredAfterEnrollmentGeneration, int admittedAdmissionOpenProgramID, Shifts shiftEnum, string sortExpression, out string enrollmentFormat, out string maxEnrollmentGenerated, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				enrollmentFormat = aspireContext.GetEnrollmentFormat(admittedAdmissionOpenProgramID, shiftEnum);
				maxEnrollmentGenerated = aspireContext.GetMaxEnrollmentGenerated(loginHistory.InstituteID, enrollmentFormat);

				IQueryable<Candidate> candidates;
				if (deferred)
				{
					if (!deferredAfterEnrollmentGeneration)
						candidates = aspireContext.CandidateAppliedPrograms
							.Where(cap => cap.DeferredToAdmittedAdmissionOpenProgramID == admittedAdmissionOpenProgramID && cap.DeferredToShift == (byte)shiftEnum)
							.Where(cap => cap.EnrollmentGenerated == null && cap.StudentID == null)
							.Where(cap => cap.StudentFees.Any(sf => sf.Status != StudentFee.StatusNotPaid))
							.Select(cap => new Candidate
							{
								CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
								ApplicationNo = cap.ApplicationNo.Value,
								Name = cap.Candidate.Name,
								FatherName = cap.Candidate.FatherName,
								Gender = cap.Candidate.Gender,
								Remarks = cap.Candidate.Remarks,
								Enrollment = null,
								StudentID = null
							});
					else
						candidates = aspireContext.Students
							.Where(s => s.DeferredToAdmissionOpenProgramID == admittedAdmissionOpenProgramID && s.DeferredToShift == (byte)shiftEnum)
							.Where(s => s.DeferredToStudentID == null)
							.Select(s => new Candidate
							{
								CandidateAppliedProgramID = null,
								ApplicationNo = null,
								Name = s.Name,
								FatherName = s.FatherName,
								Gender = s.Gender,
								Remarks = null,
								Enrollment = s.Enrollment,
								StudentID = s.StudentID,
							});

				}
				else
					candidates = aspireContext.CandidateAppliedPrograms
						.Where(cap => cap.AdmittedAdmissionOpenProgramID == admittedAdmissionOpenProgramID && cap.AdmittedShift == (byte)shiftEnum)
						.Where(cap => cap.DeferredToAdmittedAdmissionOpenProgramID == null)
						.Where(cap => cap.EnrollmentGenerated == null && cap.StudentID == null)
						.Where(cap => cap.StudentFees.Any(sf => sf.Status != StudentFee.StatusNotPaid))
						.Select(cap => new Candidate
						{
							CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
							ApplicationNo = cap.ApplicationNo.Value,
							Name = cap.Candidate.Name,
							FatherName = cap.Candidate.FatherName,
							Gender = cap.Candidate.Gender,
							Remarks = cap.Candidate.Remarks,
							Enrollment = null,
							StudentID = null
						});

				switch (sortExpression)
				{
					case nameof(Candidate.ApplicationNo):
						candidates = candidates.OrderBy(c => c.ApplicationNo);
						break;
					case nameof(Candidate.Name):
						candidates = candidates.OrderBy(c => c.Name);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return candidates.ToList();
			}
		}

		internal static string GetNewEnrollment(this AspireContext aspireContext, int admissionOpenProgramID, Shifts shiftEnum)
		{
			var format = aspireContext.GetEnrollmentFormat(admissionOpenProgramID, shiftEnum);
			if (string.IsNullOrWhiteSpace(format))
				return null;
			var admissionOpenProgram = aspireContext.AdmissionOpenPrograms
				.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID)
				.Select(aop => new
				{
					aop.Program.InstituteID,
				}).Single();
			var maxEnrollment = aspireContext.GetMaxEnrollmentGenerated(admissionOpenProgram.InstituteID, format);
			var rollNo = 0;
			if (maxEnrollment != null)
				rollNo = maxEnrollment.Replace(format, "").TrimStart('0').ToInt();
			rollNo++;
			if (rollNo.ToString().Length == 4 || rollNo < 0)
				throw new NotSupportedException("Roll No. must be at-most 3 digit long.");
			return format + rollNo.ToString().PadLeft(3, '0');

		}

		public enum EnrollmentGenerationStatus
		{
			Success,
			NoRecordFound,
			InterviewStatusNotFound,
			EnrollmentAlreadyGenerated,
			RollNoMustBeThreeDigitLong,
			EnrollmentAlreadyExists,
			AdmissionFeeNotPaid,
			Deferred,
			NotDeferred,
			SuccessWithPictureMigrationFailed,
			NewEnrollmentIsNotValid
		}

		private static EnrollmentGenerationStatus GenerateEnrollment(this AspireContext aspireContext, bool deferred, CandidateAppliedProgram candidateAppliedProgram, int userLoginHistoryID)
		{
			int admittedAdmissionOpenProgramID;
			Shifts admittedShiftEnum;
			if (deferred)
			{
				if (candidateAppliedProgram.AdmittedAdmissionOpenProgramID == null || candidateAppliedProgram.AdmittedShiftEnum == null || candidateAppliedProgram.AdmissionFeeLastDate == null)
					return EnrollmentGenerationStatus.InterviewStatusNotFound;
				if (candidateAppliedProgram.DeferredToAdmittedAdmissionOpenProgramID == null)
					return EnrollmentGenerationStatus.NotDeferred;
				admittedAdmissionOpenProgramID = candidateAppliedProgram.DeferredToAdmittedAdmissionOpenProgramID.Value;
				admittedShiftEnum = candidateAppliedProgram.DeferredToShiftEnum.Value;
			}
			else
			{
				if (candidateAppliedProgram.AdmittedAdmissionOpenProgramID == null || candidateAppliedProgram.AdmittedShiftEnum == null || candidateAppliedProgram.AdmissionFeeLastDate == null)
					return EnrollmentGenerationStatus.InterviewStatusNotFound;
				if (candidateAppliedProgram.DeferredToAdmittedAdmissionOpenProgramID != null)
					return EnrollmentGenerationStatus.Deferred;
				admittedAdmissionOpenProgramID = candidateAppliedProgram.AdmittedAdmissionOpenProgramID.Value;
				admittedShiftEnum = candidateAppliedProgram.AdmittedShiftEnum.Value;
			}

			if (candidateAppliedProgram.StudentFees.All(sf => sf.Status == StudentFee.StatusNotPaid))
				return EnrollmentGenerationStatus.AdmissionFeeNotPaid;
			if (candidateAppliedProgram.EnrollmentGenerated != null || candidateAppliedProgram.StudentID != null)
				return EnrollmentGenerationStatus.EnrollmentAlreadyGenerated;

			var admittedAdmissionOpenProgram = aspireContext.AdmissionOpenPrograms.Where(aop => aop.AdmissionOpenProgramID == admittedAdmissionOpenProgramID)
				.Select(aop => new
				{
					aop.Program.InstituteID,
					aop.Program.ProgramShortName,
					aop.SemesterID,
				}).Single();
			var newEnrollment = aspireContext.GetNewEnrollment(admittedAdmissionOpenProgramID, admittedShiftEnum);
			if (string.IsNullOrWhiteSpace(newEnrollment))
				return EnrollmentGenerationStatus.NewEnrollmentIsNotValid;
			if (aspireContext.Students.Any(s => s.AdmissionOpenProgram.Program.InstituteID == admittedAdmissionOpenProgram.InstituteID && s.Enrollment == newEnrollment))
				throw new InvalidOperationException(newEnrollment + " already exists.");
			var student = new Model.Entities.Student
			{
				Enrollment = newEnrollment.CannotBeEmptyOrWhitespace(),
				CandidateAppliedProgramID = candidateAppliedProgram.CandidateAppliedProgramID,
				AdmissionOpenProgramID = admittedAdmissionOpenProgramID,
				CreatedDate = DateTime.Now,
				RegistrationNo = null,
				MeritListScore = null,
				MeritListPosition = null,
				Name = candidateAppliedProgram.Candidate.Name,
				PersonalEmail = candidateAppliedProgram.Candidate.Email,
				UniversityEmail = null,
				Password = candidateAppliedProgram.Candidate.Password,
				ConfirmationCode = null,
				ConfirmationCodeExpiryDate = null,
				CNIC = candidateAppliedProgram.Candidate.CNIC,
				PassportNo = candidateAppliedProgram.Candidate.PassportNo,
				Gender = candidateAppliedProgram.Candidate.Gender.Value,
				Category = candidateAppliedProgram.Candidate.Category.Value,
				DOB = candidateAppliedProgram.Candidate.DOB ?? DateTime.Today,
				BloodGroup = candidateAppliedProgram.Candidate.BloodGroup,
				Phone = candidateAppliedProgram.Candidate.Phone,
				Mobile = candidateAppliedProgram.Candidate.Mobile,
				Nationality = candidateAppliedProgram.Candidate.Nationality,
				Country = candidateAppliedProgram.Candidate.Country,
				Province = candidateAppliedProgram.Candidate.Province,
				District = candidateAppliedProgram.Candidate.District,
				Tehsil = candidateAppliedProgram.Candidate.Tehsil,
				Domicile = candidateAppliedProgram.Candidate.Domicile,
				AreaType = candidateAppliedProgram.Candidate.AreaType,
				CurrentAddress = candidateAppliedProgram.Candidate.CurrentAddress,
				PermanentAddress = candidateAppliedProgram.Candidate.PermanentAddress,
				ServiceNo = candidateAppliedProgram.Candidate.ServiceNo,
				EmergencyContactName = candidateAppliedProgram.Candidate.EmergencyContactName,
				EmergencyMobile = candidateAppliedProgram.Candidate.EmergencyMobile,
				EmergencyPhone = candidateAppliedProgram.Candidate.EmergencyPhone,
				FatherName = candidateAppliedProgram.Candidate.FatherName,
				FatherCNIC = candidateAppliedProgram.Candidate.FatherCNIC,
				FatherPassportNo = candidateAppliedProgram.Candidate.FatherPassportNo,
				FatherDesignation = candidateAppliedProgram.Candidate.FatherDesignation,
				FatherDepartment = candidateAppliedProgram.Candidate.FatherDepartment,
				FatherOrganization = candidateAppliedProgram.Candidate.FatherOrganization,
				FatherServiceNo = candidateAppliedProgram.Candidate.SponsorServiceNo,
				FatherOfficePhone = candidateAppliedProgram.Candidate.FatherOfficePhone,
				FatherHomePhone = candidateAppliedProgram.Candidate.FatherHomePhone,
				FatherMobile = candidateAppliedProgram.Candidate.FatherMobile,
				FatherFax = candidateAppliedProgram.Candidate.FatherFax,
				SponsoredBy = candidateAppliedProgram.Candidate.SponsoredBy,
				SponsorName = candidateAppliedProgram.Candidate.SponsorName,
				SponsorCNIC = candidateAppliedProgram.Candidate.SponsorCNIC,
				SponsorPassportNo = candidateAppliedProgram.Candidate.SponsorPassportNo,
				SponsorRelationshipWithGuardian = candidateAppliedProgram.Candidate.SponsorRelationshipWithGuardian,
				SponsorDesignation = candidateAppliedProgram.Candidate.SponsorDesignation,
				SponsorDepartment = candidateAppliedProgram.Candidate.SponsorDepartment,
				SponsorOrganization = candidateAppliedProgram.Candidate.SponsorOrganization,
				SponsorServiceNo = candidateAppliedProgram.Candidate.SponsorServiceNo,
				SponsorPhoneHome = candidateAppliedProgram.Candidate.SponsorPhoneHome,
				SponsorMobile = candidateAppliedProgram.Candidate.SponsorMobile,
				SponsorPhoneOffice = candidateAppliedProgram.Candidate.SponsorPhoneOffice,
				SponsorFax = candidateAppliedProgram.Candidate.SponsorFax,
				ETSObtained = candidateAppliedProgram.ETSObtained,
				ETSTotal = candidateAppliedProgram.ETSTotal,
				ETSType = candidateAppliedProgram.ETSType,
				AdmissionTypeEnum = Model.Entities.Student.AdmissionTypes.Admission,
				EmergencyAddress = candidateAppliedProgram.Candidate.EmergencyAddress,
				EmergencyRelationship = candidateAppliedProgram.Candidate.EmergencyRelationship,
				PhysicalDisability = candidateAppliedProgram.Candidate.PhysicalDisability,
				StatusEnum = null,
				StatusDate = null,
				Religion = candidateAppliedProgram.Candidate.Religion,
				NextOfKin = candidateAppliedProgram.Candidate.NextOfKin,
			};

			foreach (var candidateAppliedProgramAcademicRecord in candidateAppliedProgram.CandidateAppliedProgramAcademicRecords)
				student.StudentAcademicRecords.Add(new StudentAcademicRecord
				{
					DegreeType = candidateAppliedProgramAcademicRecord.DegreeType,
					ObtainedMarks = candidateAppliedProgramAcademicRecord.ObtainedMarks,
					TotalMarks = candidateAppliedProgramAcademicRecord.TotalMarks,
					Percentage = candidateAppliedProgramAcademicRecord.ObtainedMarks * 100f / candidateAppliedProgramAcademicRecord.TotalMarks,
					IsCGPA = candidateAppliedProgramAcademicRecord.IsCGPA,
					Subjects = candidateAppliedProgramAcademicRecord.Subjects,
					Institute = candidateAppliedProgramAcademicRecord.Institute,
					BoardUniversity = candidateAppliedProgramAcademicRecord.BoardUniversity,
					PassingYear = candidateAppliedProgramAcademicRecord.PassingYear,
					Remarks = null,
					Status = null,
				});

			aspireContext.Students.Add(student.Validate());
			aspireContext.SaveChanges(userLoginHistoryID);

			candidateAppliedProgram.StudentID = student.StudentID;
			candidateAppliedProgram.EnrollmentGenerated = student.CreatedDate;
			candidateAppliedProgram.Candidate.EditStatusEnum = Model.Entities.Candidate.EditStatuses.Readonly;
			var studentFees = aspireContext.StudentFees.Where(sf => sf.CandidateAppliedProgramID == candidateAppliedProgram.CandidateAppliedProgramID).ToList();
			foreach (var studentFee in studentFees)
				studentFee.StudentID = student.StudentID;
			aspireContext.SaveChanges(userLoginHistoryID);

			var loginSessionGuid = aspireContext.GetLoginSessionGuid(userLoginHistoryID);
			var result = aspireContext.UpdateStudentPictureFromItsCandidateProfile(student.StudentID, loginSessionGuid);
			switch (result)
			{
				case StudentPictures.UpdateStudentPictureFromItsCandidateStatuses.CandidateNotFound:
					throw new InvalidOperationException("Candidate must exists.");
				case StudentPictures.UpdateStudentPictureFromItsCandidateStatuses.PictureIsInvalid:
					return EnrollmentGenerationStatus.SuccessWithPictureMigrationFailed;
				case StudentPictures.UpdateStudentPictureFromItsCandidateStatuses.PictureNotFound:
				case StudentPictures.UpdateStudentPictureFromItsCandidateStatuses.Success:
					return EnrollmentGenerationStatus.Success;
				default:
					throw new NotImplementedEnumException(result);
			}
		}

		private static EnrollmentGenerationStatus GenerateEnrollment(this AspireContext aspireContext, Model.Entities.Student oldStudent, int userLoginHistoryID)
		{
			if (oldStudent.DeferredToAdmissionOpenProgramID == null || oldStudent.DeferredToShift == null)
				return EnrollmentGenerationStatus.NotDeferred;
			if (oldStudent.DeferredToStudentID != null)
				return EnrollmentGenerationStatus.EnrollmentAlreadyGenerated;

			var admittedAdmissionOpenProgramID = oldStudent.DeferredToAdmissionOpenProgramID.Value;
			var admittedShiftEnum = (Shifts)oldStudent.DeferredToShift.Value;

			var admittedAdmissionOpenProgram = aspireContext.AdmissionOpenPrograms
				.Where(aop => aop.AdmissionOpenProgramID == admittedAdmissionOpenProgramID)
				.Select(aop => new
				{
					aop.Program.InstituteID,
					aop.Program.ProgramShortName,
					aop.SemesterID,
				}).Single();
			var newEnrollment = aspireContext.GetNewEnrollment(admittedAdmissionOpenProgramID, admittedShiftEnum);
			if (string.IsNullOrWhiteSpace(newEnrollment))
				return EnrollmentGenerationStatus.NewEnrollmentIsNotValid;
			if (aspireContext.Students.Any(s => s.AdmissionOpenProgram.Program.InstituteID == admittedAdmissionOpenProgram.InstituteID && s.Enrollment == newEnrollment))
				throw new InvalidOperationException(newEnrollment + " already exists.");
			var newStudent = new Model.Entities.Student
			{
				Enrollment = newEnrollment,
				CandidateAppliedProgramID = oldStudent.CandidateAppliedProgramID,
				AdmissionOpenProgramID = admittedAdmissionOpenProgramID,
				CreatedDate = DateTime.Now,
				RegistrationNo = oldStudent.RegistrationNo,
				MeritListScore = oldStudent.MeritListScore,
				MeritListPosition = oldStudent.MeritListPosition,
				Name = oldStudent.Name,
				PersonalEmail = oldStudent.PersonalEmail,
				UniversityEmail = null,
				Password = oldStudent.Password,
				ConfirmationCode = null,
				ConfirmationCodeExpiryDate = null,
				CNIC = oldStudent.CNIC,
				PassportNo = oldStudent.PassportNo,
				Gender = oldStudent.Gender,
				Category = oldStudent.Category,
				DOB = oldStudent.DOB,
				BloodGroup = oldStudent.BloodGroup,
				Phone = oldStudent.Phone,
				Mobile = oldStudent.Mobile,
				Nationality = oldStudent.Nationality,
				Country = oldStudent.Country,
				Province = oldStudent.Province,
				District = oldStudent.District,
				Tehsil = oldStudent.Tehsil,
				Domicile = oldStudent.Domicile,
				AreaType = oldStudent.AreaType,
				CurrentAddress = oldStudent.CurrentAddress,
				PermanentAddress = oldStudent.PermanentAddress,
				ServiceNo = oldStudent.ServiceNo,
				EmergencyContactName = oldStudent.EmergencyContactName,
				EmergencyMobile = oldStudent.EmergencyMobile,
				EmergencyPhone = oldStudent.EmergencyPhone,
				FatherName = oldStudent.FatherName,
				FatherCNIC = oldStudent.FatherCNIC,
				FatherPassportNo = oldStudent.FatherPassportNo,
				FatherDesignation = oldStudent.FatherDesignation,
				FatherDepartment = oldStudent.FatherDepartment,
				FatherOrganization = oldStudent.FatherOrganization,
				FatherServiceNo = oldStudent.SponsorServiceNo,
				FatherOfficePhone = oldStudent.FatherOfficePhone,
				FatherHomePhone = oldStudent.FatherHomePhone,
				FatherMobile = oldStudent.FatherMobile,
				FatherFax = oldStudent.FatherFax,
				SponsoredBy = oldStudent.SponsoredBy,
				SponsorName = oldStudent.SponsorName,
				SponsorCNIC = oldStudent.SponsorCNIC,
				SponsorPassportNo = oldStudent.SponsorPassportNo,
				SponsorRelationshipWithGuardian = oldStudent.SponsorRelationshipWithGuardian,
				SponsorDesignation = oldStudent.SponsorDesignation,
				SponsorDepartment = oldStudent.SponsorDepartment,
				SponsorOrganization = oldStudent.SponsorOrganization,
				SponsorServiceNo = oldStudent.SponsorServiceNo,
				SponsorPhoneHome = oldStudent.SponsorPhoneHome,
				SponsorMobile = oldStudent.SponsorMobile,
				SponsorPhoneOffice = oldStudent.SponsorPhoneOffice,
				SponsorFax = oldStudent.SponsorFax,
				ETSObtained = oldStudent.ETSObtained,
				ETSTotal = oldStudent.ETSTotal,
				ETSType = oldStudent.ETSType,
				AdmissionTypeEnum = Model.Entities.Student.AdmissionTypes.AdmissionDeferred,
				AdmissionType = Model.Entities.Student.AdmissionTypeAdmissionDeferred,
				EmergencyAddress = oldStudent.EmergencyAddress,
				EmergencyRelationship = oldStudent.EmergencyRelationship,
				PhysicalDisability = oldStudent.PhysicalDisability,
				Religion = oldStudent.Religion,
				NextOfKin = oldStudent.NextOfKin,
				StatusEnum = null,
				StatusDate = null,
				DeferredToAdmissionOpenProgramID = null,
				DeferredToShift = null,
				DeferredFromStudentID = oldStudent.StudentID,
				DeferredToStudentID = null,
				UniversityEmailPassword = null,
				AreaTypeEnum = oldStudent.AreaTypeEnum,
				CategoryEnum = oldStudent.CategoryEnum,
				BloodGroupEnum = oldStudent.BloodGroupEnum,
				ETSTypeEnum = oldStudent.ETSTypeEnum,
				FinalCGPA = null,
				FinalTranscriptIssueDate = null,
				FinalTranscriptNo = null,
				GenderEnum = oldStudent.GenderEnum,
				MigrationID = null,
				MobileNotVerified = null,
				MobileVerificationCode = null,
				PersonalEmailNotVerified = null,
				SponsoredByEnum = oldStudent.SponsoredByEnum,
				Status = null,
			};

			foreach (var oldStudentAcademicRecords in oldStudent.StudentAcademicRecords)
				newStudent.StudentAcademicRecords.Add(new StudentAcademicRecord
				{
					DegreeType = oldStudentAcademicRecords.DegreeType,
					DegreeTypeEnum = oldStudentAcademicRecords.DegreeTypeEnum,
					ObtainedMarks = oldStudentAcademicRecords.ObtainedMarks,
					TotalMarks = oldStudentAcademicRecords.TotalMarks,
					Percentage = oldStudentAcademicRecords.Percentage,
					IsCGPA = oldStudentAcademicRecords.IsCGPA,
					Subjects = oldStudentAcademicRecords.Subjects,
					Institute = oldStudentAcademicRecords.Institute,
					BoardUniversity = oldStudentAcademicRecords.BoardUniversity,
					PassingYear = oldStudentAcademicRecords.PassingYear,
					Remarks = oldStudentAcademicRecords.Remarks,
					Status = oldStudentAcademicRecords.Status,
					StatusEnum = oldStudentAcademicRecords.StatusEnum,
				});

			aspireContext.Students.Add(newStudent.Validate());
			aspireContext.SaveChanges(userLoginHistoryID);

			oldStudent.DeferredToStudentID = newStudent.StudentID;
			aspireContext.SaveChanges(userLoginHistoryID);

			var loginSessionGuid = aspireContext.GetLoginSessionGuid(userLoginHistoryID);
			var result = aspireContext.UpdateStudentPictureFromOtherStudentProfile(newStudent.StudentID, oldStudent.StudentID, loginSessionGuid);
			switch (result)
			{
				case StudentPictures.UpdateStudentPictureFromOtherStudentProfileStatuses.PictureNotFound:
				case StudentPictures.UpdateStudentPictureFromOtherStudentProfileStatuses.PictureIsInvalid:
					return EnrollmentGenerationStatus.SuccessWithPictureMigrationFailed;
				case StudentPictures.UpdateStudentPictureFromOtherStudentProfileStatuses.Success:
					return EnrollmentGenerationStatus.Success;
				default:
					throw new NotImplementedEnumException(result);
			}
		}

		public static Dictionary<int, EnrollmentGenerationStatus> GenerateEnrollments(bool deferred, bool deferredAfterEnrollmentGeneration, List<int> ids, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var dictionary = new Dictionary<int, EnrollmentGenerationStatus>();
				if (deferred && deferredAfterEnrollmentGeneration)
				{
					foreach (var studentID in ids)
					{
						var record = aspireContext.Students
							.Where(s => s.StudentID == studentID && s.DeferredToAdmissionOpenProgramID != null && s.DeferredToShift != null)
							.Select(s => new
							{
								s.AdmissionOpenProgram1.Program.InstituteID,
								s.AdmissionOpenProgram1.Program.DepartmentID,
								s.AdmissionOpenProgram1.ProgramID,
								Student = s
							}).SingleOrDefault();
						if (record == null)
							dictionary.Add(studentID, EnrollmentGenerationStatus.NoRecordFound);
						else
						{
							var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.Student.StudentID);
							dictionary.Add(record.Student.StudentID, aspireContext.GenerateEnrollment(record.Student, loginHistory.UserLoginHistoryID));
						}
					}
				}
				else
				{
					foreach (var candidateAppliedProgramID in ids)
					{
						if (deferred)
						{
							var record = (from cap in aspireContext.CandidateAppliedPrograms
										  join aop in aspireContext.AdmissionOpenPrograms on cap.DeferredToAdmittedAdmissionOpenProgramID equals aop.AdmissionOpenProgramID
										  where cap.CandidateAppliedProgramID == candidateAppliedProgramID && cap.StudentFees.Any(sf => sf.Status != StudentFee.StatusNotPaid)
										  select new
										  {
											  aop.Program.InstituteID,
											  aop.Program.DepartmentID,
											  aop.ProgramID,
											  aop.AdmissionOpenProgramID,
											  cap,
										  }).SingleOrDefault();
							if (record == null)
								dictionary.Add(candidateAppliedProgramID, EnrollmentGenerationStatus.NoRecordFound);
							else
							{
								var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
								dictionary.Add(record.cap.CandidateAppliedProgramID, aspireContext.GenerateEnrollment(deferred, record.cap, loginHistory.UserLoginHistoryID));
							}
						}
						else
						{
							var record = (from cap in aspireContext.CandidateAppliedPrograms
										  join aop in aspireContext.AdmissionOpenPrograms on cap.AdmittedAdmissionOpenProgramID equals aop.AdmissionOpenProgramID
										  where cap.CandidateAppliedProgramID == candidateAppliedProgramID && cap.StudentFees.Any(sf => sf.Status != StudentFee.StatusNotPaid)
												&& cap.DeferredToAdmittedAdmissionOpenProgramID == null
										  select new
										  {
											  aop.Program.InstituteID,
											  aop.Program.DepartmentID,
											  aop.ProgramID,
											  aop.AdmissionOpenProgramID,
											  cap,
										  }).SingleOrDefault();
							if (record == null)
								dictionary.Add(candidateAppliedProgramID, EnrollmentGenerationStatus.NoRecordFound);
							else
							{
								var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
								dictionary.Add(record.cap.CandidateAppliedProgramID, aspireContext.GenerateEnrollment(deferred, record.cap, loginHistory.UserLoginHistoryID));
							}
						}
					}
				}
				if (dictionary.Values.All(s => s == EnrollmentGenerationStatus.Success || s == EnrollmentGenerationStatus.SuccessWithPictureMigrationFailed))
					aspireContext.CommitTransaction();
				else
					aspireContext.RollbackTransaction();
				return dictionary;
			}
		}
	}
}
