﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Staff.Reports
{
	public static class CandidateDetailsReport
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Readonly);
		}
		public sealed class CandidateDetailReportPageHeader
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Title => $"APPLICATION FORM";
			public List<CandidateDetailReportPageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class CandidateDetail
		{
			public int InstituteID { get; internal set; }
			public string InstituteAlias { get; internal set; }
			public int CandidateID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Name { get; internal set; }
			public string Email { get; internal set; }
			public int? ApplicationNo { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string Password { get; internal set; }
			public DateTime? DepositDate { get; internal set; }
			public string CNIC { get; internal set; }
			public string PassportNo { get; internal set; }
			public byte? Gender { get; internal set; }
			public byte? Category { get; internal set; }
			public DateTime? DOB { get; internal set; }
			public byte? BloodGroup { get; internal set; }
			public string Phone { get; internal set; }
			public string Mobile { get; internal set; }
			public string Nationality { get; internal set; }
			public string Country { get; internal set; }
			public string Province { get; internal set; }
			public string District { get; internal set; }
			public string Tehsil { get; internal set; }
			public string Domicile { get; internal set; }
			public byte? AreaType { get; internal set; }
			public string Religion { get; internal set; }
			public string NextOfKin { get; internal set; }
			public string CurrentAddress { get; internal set; }
			public string PermanentAddress { get; internal set; }
			public string ServiceNo { get; internal set; }
			public string Remarks { get; internal set; }
			public string FatherName { get; internal set; }
			public string FatherCNIC { get; internal set; }
			public string FatherPassportNo { get; internal set; }
			public string FatherDesignation { get; internal set; }
			public string FatherDepartment { get; internal set; }
			public string FatherOrganization { get; internal set; }
			public string FatherServiceNo { get; internal set; }
			public string FatherOfficePhone { get; internal set; }
			public string FatherHomePhone { get; internal set; }
			public string FatherMobile { get; internal set; }
			public string FatherFax { get; internal set; }
			public byte? SponsoredBy { get; internal set; }
			public string SponsorName { get; internal set; }
			public string SponsorCNIC { get; internal set; }
			public string SponsorPassportNo { get; internal set; }
			public string SponsorRelationshipWithGuardian { get; internal set; }
			public string SponsorDesignation { get; internal set; }
			public string SponsorDepartment { get; internal set; }
			public string SponsorOrganization { get; internal set; }
			public string SponsorServiceNo { get; internal set; }
			public string SponsorPhoneHome { get; internal set; }
			public string SponsorMobile { get; internal set; }
			public string SponsorPhoneOffice { get; internal set; }
			public string SponsorFax { get; internal set; }
			public string EmergencyContactName { get; internal set; }
			public string EmergencyMobile { get; internal set; }
			public string EmergencyPhone { get; internal set; }
			public string EmergencyRelationship { get; internal set; }
			public string EmergencyAddress { get; internal set; }
			public string PhysicalDisability { get; internal set; }
			public string AnnualFamilyIncome { get; internal set; }
			public byte? SourceOfInformation { get; internal set; }
			public byte Status { get; internal set; }
			public byte EditStatus { get; internal set; }
			public byte[] Photo { get; internal set; }
			public List<CandidateAcademicRecord> CandidateAcademicRecords { get; internal set; }
			public List<CandidateAppliedProgram> CandidateAppliedPrograms { get; internal set; }

			public string DOBShortFormat => this.DOB?.ToString("d").ToNAIfNullOrEmpty();
			//public string DurationFullName => ((ProgramDurations)this.Duration).ToFullName();
			public string GenderFullName => ((Genders?)this.Gender)?.ToFullName().ToNAIfNullOrEmpty();
			public string CategoryFullName => ((Categories?)this.Category)?.ToFullName().ToNAIfNullOrEmpty();
			public string BloodGroupFullName => ((BloodGroups?)this.BloodGroup)?.ToFullName().ToNAIfNullOrEmpty();
			public string SponsorByFullName => ((Sponsorships?)this.SponsoredBy)?.ToFullName().ToNAIfNullOrEmpty();
			//public Model.Entities.Student.AdmissionTypes AdmissionTypeFullName => ((Model.Entities.Student.AdmissionTypes)this.AdmissionType);
			public string AreaTypeFullName => ((AreaTypes?)this.AreaType)?.ToFullName().ToNAIfNullOrEmpty();
			//public string ETSTypeFullName => ((ETSTypes?)this.ETSType)?.ToFullName();

			public sealed class CandidateAcademicRecord
			{
				public int CandidateAppliedProgramID { get; internal set; }
				public byte DegreeType { get; internal set; }
				public double ObtainedMarks { get; internal set; }
				public double TotalMarks { get; internal set; }
				public double Percentage { get; internal set; }
				public bool IsCGPA { get; internal set; }
				public string Subjects { get; internal set; }
				public string BoardOrInstitute { get; internal set; }
				public short PassingYear { get; internal set; }
				public byte? Status { get; internal set; }
				public string Remarks { get; internal set; }
				public string DegreeTypeFullName => ((DegreeTypes)this.DegreeType).ToFullName();

				public List<CandidateAcademicRecord> GetList()
				{
					return null;
				}
			}
			public List<CandidateDetail> GetList()
			{
				return null;
			}
		}

		public sealed class CandidateAppliedProgram
		{
			public int CandidateAppliedProgramID { get; internal set; }
			public int CandidateID { get; internal set; }
			public int? ApplicationNo { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public byte Duration { get; internal set; }
			public byte Shift { get; internal set; }
			public bool CreditTransfer { get; internal set; }
			public string PreReqDegree { get; internal set; }
			public byte? ResultAwaitedDegreeType { get; internal set; }
			public DateTime ApplyDate { get; internal set; }
			public DateTime? EntryTestDate { get; internal set; }
			public byte? TestDuration { get; internal set; }
			public short Amount { get; internal set; }
			public int? InstituteBankAccountID { get; internal set; }
			public DateTime? DepositDate { get; internal set; }
			public byte? DepositPlace { get; internal set; }
			public int? DepositBankBranchID { get; internal set; }
			public byte? FeeUpdatedByUserType { get; internal set; }
			public byte? ETSType { get; internal set; }
			public double? ETSObtained { get; internal set; }
			public double? ETSTotal { get; internal set; }
			public byte? EntryTestMarksPercentage { get; internal set; }
			public DateTime? InterviewDate { get; internal set; }
			public string InterviewRemarks { get; internal set; }
			public int? AdmittedAdmissionOpenProgramID { get; internal set; }
			public byte? AdmittedShift { get; internal set; }
			public DateTime? AdmissionFeeLastDate { get; internal set; }
			public DateTime? EnrollmentGenerated { get; internal set; }
			public int? StudentID { get; internal set; }
			public int? MigrationID { get; internal set; }
			public byte? Status { get; internal set; }
			public DateTime? StatusDate { get; internal set; }

			public string ShiftFullName => ((Shifts)this.Shift).ToFullName().ToNAIfNullOrEmpty();
			public string CreditTransferYesNo => this.CreditTransfer.ToYesNo();
			public string DurationFullName => ((ProgramDurations)this.Duration).ToFullName().ToNAIfNullOrEmpty();
			public string ResultAwaitedDegreeTypeFullName => ((DegreeTypes?)this.ResultAwaitedDegreeType)?.ToFullName().ToNAIfNullOrEmpty();
			public string ApplyDateShortFormat => this.ApplyDate.ToString("d").ToNAIfNullOrEmpty();
			public string EntryTestDateShortFormat => this.EntryTestDate?.ToString("d").ToNAIfNullOrEmpty();
			public string DepositDateShortFormat => this.DepositDate?.ToString("d").ToNAIfNullOrEmpty();
			public string InterviewDateShortFormat => this.InterviewDate?.ToString("d").ToNAIfNullOrEmpty();
			public string InterviewClearedYesNo => this.InterviewDate == null ? "No" : "Yes";
			public string EnrollmentGeneratedYesNo => this.EnrollmentGenerated == null ? "No" : "Yes";
			public string PayAdmissionProcessingFeeYesNo => this.DepositDate == null ? "No" : "Yes";
			public string EntryTestYesNo => this.EntryTestDate == null ? this.ETSType == null ? "No" : "Yes" : "Yes";
			public string ETSTypeFullName => ((ETSTypes?)this.ETSType)?.ToFullName().ToNAIfNullOrEmpty();

			public List<CandidateAppliedProgram> GetList()
			{
				return null;
			}
		}

		public class ReportDataSet
		{
			internal ReportDataSet() { }
			public CandidateDetailReportPageHeader PageHeader { get; internal set; }
			public List<CandidateDetail> CandidateDetails { get; internal set; }
		}

		public static List<CustomListItem> GetAdmissionOpenPrograms(int instituteID, short semesterID, int? departmentID)
		{
			using (var aspireContext = new AspireContext())
			{
				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms.AsQueryable();
				if (departmentID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.Program.DepartmentID == departmentID);
				return admissionOpenPrograms.Where(aop => aop.Program.InstituteID == instituteID && aop.SemesterID == semesterID && aop.Program.DepartmentID == departmentID).Select(p => new CustomListItem
				{
					ID = p.AdmissionOpenProgramID,
					Text = p.Program.ProgramAlias
				}).OrderBy(p => p.Text).ToList();
			}
		}

		public static ReportDataSet GetCandidatesDetails(short semesterID, int? departmentID, int? admissionOpenProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var candidateAppliedPrograms = aspireContext.CandidateAppliedPrograms.AsQueryable();
				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms.Where(aop => aop.SemesterID == semesterID && aop.Program.InstituteID == loginHistory.InstituteID).AsQueryable();
				if (departmentID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.Program.DepartmentID == departmentID);
				if (admissionOpenProgramID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID);

				var query = from cap in candidateAppliedPrograms
							join aop in admissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
							select new CandidateDetail
							{
								CandidateID = cap.Candidate.CandidateID,
								SemesterID = cap.Candidate.SemesterID,
								Name = cap.Candidate.Name,
								Email = cap.Candidate.Email,
								CNIC = cap.Candidate.CNIC,
								PassportNo = cap.Candidate.PassportNo,
								Gender = cap.Candidate.Gender,
								Category = cap.Candidate.Category,
								DOB = cap.Candidate.DOB,
								BloodGroup = cap.Candidate.BloodGroup,
								Phone = cap.Candidate.Phone,
								Mobile = cap.Candidate.Mobile,
								Nationality = cap.Candidate.Nationality,
								Country = cap.Candidate.Country,
								Province = cap.Candidate.Province,
								District = cap.Candidate.District,
								Tehsil = cap.Candidate.Tehsil,
								Domicile = cap.Candidate.Domicile,
								AreaType = cap.Candidate.AreaType,
								Religion = cap.Candidate.Religion,
								NextOfKin = cap.Candidate.NextOfKin,
								CurrentAddress = cap.Candidate.CurrentAddress,
								PermanentAddress = cap.Candidate.PermanentAddress,
								ServiceNo = cap.Candidate.ServiceNo,
								Remarks = cap.Candidate.Remarks,
								FatherName = cap.Candidate.FatherName,
								FatherCNIC = cap.Candidate.FatherCNIC,
								FatherPassportNo = cap.Candidate.FatherPassportNo,
								FatherDesignation = cap.Candidate.FatherDesignation,
								FatherDepartment = cap.Candidate.FatherDepartment,
								FatherOrganization = cap.Candidate.FatherOrganization,
								FatherServiceNo = cap.Candidate.FatherServiceNo,
								FatherOfficePhone = cap.Candidate.FatherOfficePhone,
								FatherHomePhone = cap.Candidate.FatherHomePhone,
								FatherMobile = cap.Candidate.FatherMobile,
								FatherFax = cap.Candidate.FatherFax,
								SponsoredBy = cap.Candidate.SponsoredBy,
								SponsorName = cap.Candidate.SponsorName,
								SponsorCNIC = cap.Candidate.SponsorCNIC,
								SponsorPassportNo = cap.Candidate.SponsorPassportNo,
								SponsorRelationshipWithGuardian = cap.Candidate.SponsorRelationshipWithGuardian,
								SponsorDesignation = cap.Candidate.SponsorDesignation,
								SponsorDepartment = cap.Candidate.SponsorDepartment,
								SponsorOrganization = cap.Candidate.SponsorOrganization,
								SponsorServiceNo = cap.Candidate.SponsorServiceNo,
								SponsorPhoneHome = cap.Candidate.SponsorPhoneHome,
								SponsorMobile = cap.Candidate.SponsorMobile,
								SponsorPhoneOffice = cap.Candidate.SponsorPhoneOffice,
								SponsorFax = cap.Candidate.SponsorFax,
								EmergencyContactName = cap.Candidate.EmergencyContactName,
								EmergencyMobile = cap.Candidate.EmergencyMobile,
								EmergencyPhone = cap.Candidate.EmergencyPhone,
								EmergencyRelationship = cap.Candidate.EmergencyRelationship,
								EmergencyAddress = cap.Candidate.EmergencyAddress,
								PhysicalDisability = cap.Candidate.PhysicalDisability,
								AnnualFamilyIncome = cap.Candidate.AnnualFamilyIncome,
								SourceOfInformation = cap.Candidate.SourceOfInformation,
								Status = cap.Candidate.Status,
							};

				var candidatesList = query.OrderBy(o => o.Name).ToList();
				foreach (var candidate in candidatesList)
					candidate.Photo = Common.ProfileInformation.GetCandidateProfilePicture(candidate.CandidateID, loginSessionGuid);

				return new ReportDataSet
				{
					PageHeader = new CandidateDetailReportPageHeader
					{
						SemesterID = semesterID,
						Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single(),
					},
					CandidateDetails = candidatesList,
				};
			}
		}

		public static List<CandidateAppliedProgram> GetCandidateAppliedPrograms(short semesterID, int candidateID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var candidateAppliedPrograms = aspireContext.CandidateAppliedPrograms.Where(cap => cap.CandidateID == candidateID).AsQueryable();
				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms.Where(aop => aop.SemesterID == semesterID && aop.Program.InstituteID == loginHistory.InstituteID).AsQueryable();

				var query = from cap in candidateAppliedPrograms
							join aop in admissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
							select new CandidateAppliedProgram
							{
								CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
								CandidateID = cap.CandidateID,
								ApplicationNo = cap.ApplicationNo,
								ProgramAlias = aop.Program.ProgramAlias,
								Duration = aop.Program.Duration,
								Shift = cap.Shift,
								CreditTransfer = cap.CreditTransfer,
								PreReqDegree = cap.PreReqDegree,
								ResultAwaitedDegreeType = cap.ResultAwaitedDegreeType,
								ApplyDate = cap.ApplyDate,
								EntryTestDate = cap.EntryTestDate,
								TestDuration = cap.TestDuration,
								Amount = cap.Amount,
								DepositDate = cap.AccountTransaction.TransactionDate,
								ETSType = cap.ETSType,
								ETSObtained = cap.ETSObtained,
								ETSTotal = cap.ETSTotal,
								EntryTestMarksPercentage = cap.EntryTestMarksPercentage,
								InterviewDate = cap.InterviewDate,
								InterviewRemarks = cap.InterviewRemarks,
								AdmissionFeeLastDate = cap.AdmissionFeeLastDate,
								EnrollmentGenerated = cap.EnrollmentGenerated,
								Status = cap.Status,
								StatusDate = cap.StatusDate,
							};
				return query.ToList();
			}
		}
	}
}
