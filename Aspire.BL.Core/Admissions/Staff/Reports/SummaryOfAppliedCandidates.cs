﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Staff.Reports
{
	public static class SummaryOfAppliedCandidates
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class AppliedCandidatesSummaryPageHeader
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Title => $"Program-Wise Applied Candidates Summary Online - ({this.SemesterID.ToSemesterString()})";
			public string Department { get; internal set; }
			public string AdmissionProcessingFee { get; internal set; }
			public string EntryTestCleared { get; internal set; }
			public string InterviewCleared { get; internal set; }
			public string EnrollmentGenerated { get; internal set; }
			public string Filters => $"Department: {this.Department}, Admission Processing Fee Paid: {this.AdmissionProcessingFee}, Entry Test Cleared: {this.EntryTestCleared}, Interview Cleared: {this.InterviewCleared}, Enrollment Generated: {this.EnrollmentGenerated}";
			public static List<AppliedCandidatesSummaryPageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class AppliedCandidatesSummary
		{
			public int CandidateAppliedProgramID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public byte Duration { get; internal set; }
			public DateTime ApplyDate { get; internal set; }
			public string ApplyDateString => this.ApplyDate.Date.ToString("dd-MM-yyyy");
			public string ProgramAliasAndDuration => $"{this.ProgramAlias} {((ProgramDurations)this.Duration).ToFullName()}";
			public static List<AppliedCandidatesSummary> GetList()
			{
				return null;
			}
		}

		public class ReportDataSet
		{
			public AppliedCandidatesSummaryPageHeader PageHeader { get; set; }
			public List<AppliedCandidatesSummary> AppliedCandidateSummary { get; set; }
		}
		public static ReportDataSet GetAppliedCandidatesSummary(short semesterID, int? departmentID, string departmentString, bool? admissionProcessingFee, bool? entryTestCleared, bool? interviewCleared, bool? enrollmentGenerated, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms.Where(aop => aop.Program.InstituteID == loginHistory.InstituteID && aop.SemesterID == semesterID);
				var candidateAppliedPrograms = aspireContext.CandidateAppliedPrograms.AsQueryable();
				var pageHeader = new AppliedCandidatesSummaryPageHeader
				{
					SemesterID = semesterID,
					Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single(),
				};

				if (departmentID != null)
				{
					pageHeader.Department = departmentString;
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.AdmissionOpenProgram1.Program.DepartmentID == departmentID);
				}
				else
					pageHeader.Department = "ALL";
				if (admissionProcessingFee != null)
				{
					if (admissionProcessingFee.Value)
					{
						pageHeader.AdmissionProcessingFee = "YES";
						candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.AccountTransactionID != null);
					}
					else
					{
						pageHeader.AdmissionProcessingFee = "NO";
						candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.AccountTransactionID == null);
					}
				}
				else
					pageHeader.AdmissionProcessingFee = "ALL";

				if (entryTestCleared != null)
				{
					if (entryTestCleared.Value)
					{
						pageHeader.EntryTestCleared = "YES";
						candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.ETSObtained != null || cap.EntryTestMarksPercentage != null);
					}
					else
					{
						pageHeader.EntryTestCleared = "NO";
						candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.ETSObtained == null && cap.EntryTestMarksPercentage == null);
					}
				}
				else
					pageHeader.EntryTestCleared = "ALL";

				if (interviewCleared != null)
				{
					if (interviewCleared.Value)
					{
						pageHeader.InterviewCleared = "YES";
						candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.InterviewDate != null);

					}
					else
					{
						pageHeader.InterviewCleared = "NO";
						candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.InterviewDate == null);
					}
				}
				else
					pageHeader.InterviewCleared = "ALL";

				if (enrollmentGenerated != null)
				{
					if (enrollmentGenerated.Value)
					{
						pageHeader.EnrollmentGenerated = "YES";
						candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.EnrollmentGenerated != null);

					}
					else
					{
						pageHeader.EnrollmentGenerated = "NO";
						candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.EnrollmentGenerated == null);
					}
				}
				else
					pageHeader.EnrollmentGenerated = "ALL";
				var query = from cap in candidateAppliedPrograms
							join aop in admissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
							select new AppliedCandidatesSummary
							{
								CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
								ProgramAlias = aop.Program.ProgramAlias,
								Duration = aop.Program.Duration,
								ApplyDate = cap.ApplyDate,
							};

				return new ReportDataSet
				{
					PageHeader = pageHeader,
					AppliedCandidateSummary = query.ToList(),
				};
			}
		}
	}
}
