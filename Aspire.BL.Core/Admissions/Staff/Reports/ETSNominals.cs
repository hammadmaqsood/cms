﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Staff.Reports
{
	public static class ETSNominals
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class Candidate
		{
			public string Name { get; set; }
			public string Program { get; set; }
			public byte ETSType { get; set; }
			public string ETSTypeFullName => ((ETSTypes)this.ETSType).ToFullName();
			public double ETSObtained { get; set; }
			public double ETSTotal { get; set; }
			public string Institute { get; set; }
			public int ApplicationNo { get; set; }
			public List<Candidate> GetList()
			{
				return null;
			}
		}
		public static List<Candidate> GetETSNominal(short semesterID, int? admissionOpenProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = from cap in aspireContext.CandidateAppliedPrograms
							join aop in aspireContext.AdmissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
							where aop.SemesterID == semesterID && aop.Program.InstituteID == loginHistory.InstituteID && cap.ApplicationNo != null && cap.AccountTransactionID != null
								  && (cap.ETSType != null && cap.ETSObtained != null && cap.ETSTotal != null)
								  && (admissionOpenProgramID == null || aop.AdmissionOpenProgramID == admissionOpenProgramID)
							select new Candidate
							{
								Institute = aop.Program.Institute.InstituteShortName,
								Program = aop.Program.ProgramAlias,
								ApplicationNo = cap.ApplicationNo.Value,
								Name = cap.Candidate.Name,
								ETSType = cap.ETSType.Value,
								ETSObtained = cap.ETSObtained.Value,
								ETSTotal = cap.ETSTotal.Value,
							};
				return query.ToList();
			}
		}
	}
}
