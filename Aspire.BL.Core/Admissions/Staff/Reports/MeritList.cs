﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Staff.Reports
{
	public static class MeritList
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class ReportSummary
		{
			public string InstituteName { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public string Criteria
			{
				get
				{
					var list = new List<string>();
					if (this.EntryTestMarksPercentageToExclusive != null && this.EntryTestMarksPercentageFromInclusive != null)
						throw new ArgumentException();
					if ((this.EntryTestMarksPercentageToExclusive == null && this.EntryTestMarksPercentageFromInclusive != null) || (this.EntryTestMarksPercentageToExclusive != null && this.EntryTestMarksPercentageFromInclusive == null))
					{
						if (this.EntryTestMarksPercentageFromInclusive != null)
							list.Add($"Entry Test Marks {this.EntryTestMarksPercentageFromInclusive}% and above");
						else if (this.EntryTestMarksPercentageToExclusive != null)
							list.Add($"Entry Test Marks below {this.EntryTestMarksPercentageFromInclusive}%");
					}

					if (this.ETSMarksPercentageToExclusive != null && this.ETSMarksPercentageFromInclusive != null)
						throw new ArgumentException();
					if ((this.ETSMarksPercentageToExclusive == null && this.ETSMarksPercentageFromInclusive != null) || (this.ETSMarksPercentageToExclusive != null && this.ETSMarksPercentageFromInclusive == null))
					{
						if (this.ETSMarksPercentageFromInclusive != null)
							list.Add($"ETS Marks {this.ETSMarksPercentageFromInclusive}% and above");
						else if (this.ETSMarksPercentageToExclusive != null)
							list.Add($"ETS Marks below {this.ETSMarksPercentageFromInclusive}%");
					}
					return list.Any()
						? string.Join("\n", list)
						: null;
				}
			}
			public double? EntryTestMarksPercentageFromInclusive { get; internal set; }
			public double? EntryTestMarksPercentageToExclusive { get; internal set; }
			public double? ETSMarksPercentageFromInclusive { get; internal set; }
			public double? ETSMarksPercentageToExclusive { get; internal set; }
			public string DepartmentShortName { get; internal set; }
			public string ProgramShortName { get; internal set; }
			public int TotalCandidates { get; internal set; }
			public DegreeLevels DegreeLevelEnum { get; internal set; }

			public List<ReportSummary> GetList()
			{
				return null;
			}
		}

		public sealed class Masters
		{
			public Masters() { }
			internal Masters(Candidate candidate, string entryTestDates)
			{
				this.ReportPageTitle = $@"{candidate.InstituteName} - Admissions ({candidate.SemesterID.ToSemesterString()})
Selection List of Candidates {candidate.ProgramShortName}
{entryTestDates}".Trim();
				this.HasErrors = true;
				this.IsPassed = false;
				this.DepartmentShortName = candidate.DepartmentShortName;
				this.ProgramShortName = candidate.ProgramShortName;
				this.AdmissionOpenProgramID = candidate.AdmissionOpenProgramID;
				this.AppNoB = candidate.ApplicationNo;
				this.IsNaval = candidate.IsNaval;
				this.NameC = candidate.Name;
				this.Mobile = candidate.Mobile;
				this.Email = candidate.Email;
				this.Enrollment = candidate.Enrollment;
				this.StudentStatusEnum = candidate.StudentStatusEnum;
				this.StudentProgramAlias = candidate.StudentProgramAlias;
				this.StudentIntakeSemesterID = candidate.StudentIntakeSemesterID;
				this.AppliedSemesterID = candidate.AppliedSemesterID;

				var academicRecordEligibility = candidate.AcademicRecords.OrderByDescending(c => c.DegreeTypeEnum).FirstOrDefault();
				if (academicRecordEligibility == null)
				{
					this.RemarksI = "Academic Record is missing.";
					return;
				}
				this.ExamPassedD = academicRecordEligibility.DegreeTypeEnum.ToFullName();
				this.EligibilityPercentage = academicRecordEligibility.Percentage;
				if (this.EligibilityPercentage < 0 || 100 < this.EligibilityPercentage)
				{
					this.EligibilityPercentage = null;
					this.RemarksI = "Invalid Eligibility Percentage";
					return;
				}

				if (candidate.EntryTestMarksPercentageAndETSPercentageIsNull)
				{
					this.RemarksI = "EntryTestPercentage/ETS Information is missing.";
					return;
				}

				if (candidate.EntryTestPassed)
				{
					if (candidate.ETSPassed)
					{
						if (candidate.EntryTestMarksPercentage >= candidate.ETSPercentage)
						{
							this.TestMarksPercentage = candidate.EntryTestMarksPercentage.Value;
							this.IsETSScorer = false;
						}
						else
						{
							this.TestMarksPercentage = candidate.ETSPercentage.Value;
							this.IsETSScorer = true;
						}
					}
					else
					{
						this.TestMarksPercentage = candidate.EntryTestMarksPercentage.Value;
						this.IsETSScorer = false;
					}
				}
				else
				{
					if (candidate.ETSPassed)
					{
						this.TestMarksPercentage = candidate.ETSPercentage.Value;
						this.IsETSScorer = true;
					}
					else
					{
						if (candidate.EntryTestMarksPercentage != null)
						{
							if (candidate.ETSPercentage != null)
							{
								if (candidate.EntryTestMarksPercentage.Value >= candidate.ETSPercentage.Value)
								{
									this.TestMarksPercentage = candidate.EntryTestMarksPercentage.Value;
									this.IsETSScorer = false;
								}
								else
								{
									this.TestMarksPercentage = candidate.ETSPercentage.Value;
									this.IsETSScorer = true;
								}
							}
							else
							{
								this.TestMarksPercentage = candidate.EntryTestMarksPercentage.Value;
								this.IsETSScorer = false;
							}
						}
						else
						{
							if (candidate.ETSPercentage != null)
							{
								this.TestMarksPercentage = candidate.ETSPercentage.Value;
								this.IsETSScorer = true;
							}
							else
							{
								this.TestMarksPercentage = null;
								this.IsETSScorer = false;
							}
						}
					}
				}

				if (this.TestMarksPercentage < 0 || 100 < this.TestMarksPercentage)
				{
					this.TestMarksPercentage = null;
					this.RemarksI = "Invalid Test Marks Percentage";
					return;
				}

				this.RemarksI = candidate.ResultAwaitedDegreeTypeEnum?.ToFullName();
				this.IsPassed = candidate.IsPassed;
				this.HasErrors = false;
			}

			public string DepartmentShortName { get; }
			public string ProgramShortName { get; }
			public int AdmissionOpenProgramID { get; }
			public string ReportPageTitle { get; }
			public bool IsNaval { get; }
			public bool IsETSScorer { get; }
			public bool HasErrors { get; }
			public string HasErrorsYesNo => this.HasErrors.ToYesNo();
			public int AppNoB { get; }
			public string NameC { get; }
			public string Mobile { get; }
			public string Email { get; }
			public string ExamPassedD { get; }
			private double? EligibilityPercentage { get; }
			public string EligibilityPercentageE => this.EligibilityPercentage?.ToString("###.00");
			private double? TestMarksPercentage { get; }
			public string TestMarksPercentageF => this.TestMarksPercentage?.ToString("####.00");
			private double? FinalSelection => this.EligibilityPercentage * 0.48 + this.TestMarksPercentage * 0.32;
			public string FinalSelectionG => this.FinalSelection?.ToString("###.00");
			public string TypeH => new[] { this.IsNaval ? "N" : null, this.IsETSScorer ? "E" : null }.Where(s => s != null).Join("+");
			public string RemarksI { get; }
			public string Enrollment { get; }
			public Model.Entities.Student.Statuses? StudentStatusEnum { get; }
			public string StudentStatusFullName => this.StudentStatusEnum?.ToFullName();
			public string StudentProgramAlias { get; }
			public short? StudentIntakeSemesterID { get; }
			public string StudentIntakeSemester => this.StudentIntakeSemesterID.ToSemesterString();
			public bool IsPassed { get; }
			public string IsPassedYesNo => this.IsPassed.ToYesNo();
			public short AppliedSemesterID { get; }
			public string AppliedSemester => this.AppliedSemesterID.ToSemesterString();

			public List<Masters> GetList()
			{
				return null;
			}
		}

		public sealed class Bachelors
		{
			public Bachelors() { }
			internal Bachelors(Candidate candidate, string entryTestDates)
			{
				this.ReportPageTitle = $@"{candidate.InstituteName} - Admissions ({candidate.SemesterID.ToSemesterString()})
Selection List of Candidates {candidate.ProgramShortName}
{entryTestDates}".Trim();
				this.HasErrors = true;
				this.IsPassed = false;
				this.DepartmentShortName = candidate.DepartmentShortName;
				this.ProgramShortName = candidate.ProgramShortName;
				this.AdmissionOpenProgramID = candidate.AdmissionOpenProgramID;
				this.AppNoB = candidate.ApplicationNo;
				this.IsNaval = candidate.IsNaval;
				this.NameC = candidate.Name;
				this.Mobile = candidate.Mobile;
				this.Email = candidate.Email;
				this.Enrollment = candidate.Enrollment;
				this.StudentStatusEnum = candidate.StudentStatusEnum;
				this.StudentProgramAlias = candidate.StudentProgramAlias;
				this.StudentIntakeSemesterID = candidate.StudentIntakeSemesterID;
				this.AppliedSemesterID = candidate.AppliedSemesterID;

				if (!candidate.AcademicRecords.Any())
				{
					this.RemarksJ = "Academic Record is missing.";
					return;
				}

				var academicRecordOLevelSSC = candidate.AcademicRecords.FirstOrDefault(ar => ar.DegreeTypeEnum == DegreeTypes.SSC || ar.DegreeTypeEnum == DegreeTypes.OLEVEL);
				if (academicRecordOLevelSSC == null)
				{
					this.RemarksJ = "SSC/O-Level Percentage is missing.";
					return;
				}
				this.OLevelSSCPercentage = academicRecordOLevelSSC.Percentage;
				if (this.OLevelSSCPercentage < 0 || 100 < this.OLevelSSCPercentage)
				{
					this.EligibilityPercentage = null;
					this.RemarksJ = "Invalid SSC/O-Level Percentage";
					return;
				}

				var academicRecordEligibility = candidate.AcademicRecords.OrderByDescending(c => c.DegreeTypeEnum).FirstOrDefault();
				if (academicRecordEligibility == null)
				{
					this.RemarksJ = "Eligibility Percentage is missing.";
					return;
				}
				this.ExamPassedD = academicRecordEligibility.DegreeTypeEnum.ToFullName();
				this.EligibilityPercentage = academicRecordEligibility.Percentage;
				if (this.EligibilityPercentage < 0 || 100 < this.EligibilityPercentage)
				{
					this.EligibilityPercentage = null;
					this.RemarksJ = "Invalid Eligibility Percentage";
					return;
				}

				if (candidate.EntryTestMarksPercentageAndETSPercentageIsNull)
				{
					this.RemarksJ = "EntryTestPercentage/ETS Information is missing.";
					return;
				}

				if (candidate.EntryTestPassed)
				{
					if (candidate.ETSPassed)
					{
						if (candidate.EntryTestMarksPercentage >= candidate.ETSPercentage)
						{
							this.TestMarksPercentage = candidate.EntryTestMarksPercentage.Value;
							this.IsETSScorer = false;
						}
						else
						{
							this.TestMarksPercentage = candidate.ETSPercentage.Value;
							this.IsETSScorer = true;
						}
					}
					else
					{
						this.TestMarksPercentage = candidate.EntryTestMarksPercentage.Value;
						this.IsETSScorer = false;
					}
				}
				else
				{
					if (candidate.ETSPassed)
					{
						this.TestMarksPercentage = candidate.ETSPercentage.Value;
						this.IsETSScorer = true;
					}
					else
					{
						if (candidate.EntryTestMarksPercentage != null)
						{
							if (candidate.ETSPercentage != null)
							{
								if (candidate.EntryTestMarksPercentage.Value >= candidate.ETSPercentage.Value)
								{
									this.TestMarksPercentage = candidate.EntryTestMarksPercentage.Value;
									this.IsETSScorer = false;
								}
								else
								{
									this.TestMarksPercentage = candidate.ETSPercentage.Value;
									this.IsETSScorer = true;
								}
							}
							else
							{
								this.TestMarksPercentage = candidate.EntryTestMarksPercentage.Value;
								this.IsETSScorer = false;
							}
						}
						else
						{
							if (candidate.ETSPercentage != null)
							{
								this.TestMarksPercentage = candidate.ETSPercentage.Value;
								this.IsETSScorer = true;
							}
							else
							{
								this.TestMarksPercentage = null;
								this.IsETSScorer = false;
							}
						}
					}
				}

				if (this.TestMarksPercentage < 0 || 100 < this.TestMarksPercentage)
				{
					this.TestMarksPercentage = null;
					this.RemarksJ = "Invalid Test Marks Percentage";
					return;
				}

				this.RemarksJ = candidate.ResultAwaitedDegreeTypeEnum?.ToFullName();
				this.IsPassed = candidate.IsPassed;
				this.HasErrors = false;
			}

			public string DepartmentShortName { get; }
			public string ProgramShortName { get; }
			public int AdmissionOpenProgramID { get; }
			public string ReportPageTitle { get; }
			public bool IsNaval { get; }
			public bool IsETSScorer { get; }
			public bool HasErrors { get; }
			public string HasErrorsYesNo => this.HasErrors.ToYesNo();
			public int AppNoB { get; }
			public string NameC { get; }
			public string Mobile { get; }
			public string Email { get; }
			public string ExamPassedD { get; }
			private double? OLevelSSCPercentage { get; }
			public string OLevelSSCPercentageE => this.OLevelSSCPercentage?.ToString("###.00");
			private double? EligibilityPercentage { get; }
			public string EligibilityPercentageF => this.EligibilityPercentage?.ToString("###.00");
			private double? TestMarksPercentage { get; }
			public string TestMarksPercentageG => this.TestMarksPercentage?.ToString("####.00");
			private double? FinalSelection => this.OLevelSSCPercentage * 0.1 + this.EligibilityPercentage * 0.4 + this.TestMarksPercentage * 0.5;
			public string FinalSelectionH => this.FinalSelection?.ToString("###.00");
			public string TypeI => new[] { this.IsNaval ? "N" : null, this.IsETSScorer ? "E" : null }.Where(s => s != null).Join("+");
			public string RemarksJ { get; }
			public string Enrollment { get; }
			public Model.Entities.Student.Statuses? StudentStatusEnum { get; }
			public string StudentStatusFullName => this.StudentStatusEnum?.ToFullName();
			public string StudentProgramAlias { get; }
			public short? StudentIntakeSemesterID { get; }
			public string StudentIntakeSemester => this.StudentIntakeSemesterID.ToSemesterString();
			public bool IsPassed { get; }
			public string IsPassedYesNo => this.IsPassed.ToYesNo();
			public short AppliedSemesterID { get; }
			public string AppliedSemester => this.AppliedSemesterID.ToSemesterString();

			public List<Bachelors> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			public List<Bachelors> DiplomaCandidates { get; }
			public List<Bachelors> BachelorsCandidates { get; }
			public List<Masters> MastersCandidates { get; }
			public List<Masters> DoctorateCandidates { get; }
			public List<ReportSummary> Summary { get; }

			internal ReportDataSet(List<Candidate> candidates, double? entryTestMarksPercentageFromInclusive, double? entryTestMarksPercentageToExclusive, double? etsMarksPercentageFromInclusive, double? etsMarksPercentageToExclusive, bool displayOnlyPassed)
			{
				var doctorateCandidates = new List<Candidate>();
				var mastersCandidates = new List<Candidate>();
				var bachelorsCandidates = new List<Candidate>();
				var diplomaCandidates = new List<Candidate>();

				foreach (var candidate in candidates)
				{
					if (candidate.EntryTestMarksPercentageAndETSPercentageIsNull && displayOnlyPassed)
						continue;
					candidate.EntryTestPassed = candidate.EntryTestMarksPercentage != null
						&& (entryTestMarksPercentageFromInclusive == null || entryTestMarksPercentageFromInclusive <= candidate.EntryTestMarksPercentage.Value)
						&& (entryTestMarksPercentageToExclusive == null || candidate.EntryTestMarksPercentage < entryTestMarksPercentageToExclusive);

					candidate.ETSPassed = candidate.ETSPercentage != null
						&& (etsMarksPercentageFromInclusive == null || etsMarksPercentageFromInclusive <= candidate.ETSPercentage.Value)
						&& (etsMarksPercentageToExclusive == null || candidate.ETSPercentage < etsMarksPercentageToExclusive);

					if (candidate.IsPassed || !displayOnlyPassed)
						switch (candidate.DegreeLevelEnum)
						{
							case DegreeLevels.Diploma:
								diplomaCandidates.Add(candidate);
								break;
							case DegreeLevels.Undergraduate:
								bachelorsCandidates.Add(candidate);
								break;
							case DegreeLevels.Masters:
							case DegreeLevels.Postgraduate:
								mastersCandidates.Add(candidate);
								break;
							case DegreeLevels.Doctorate:
								doctorateCandidates.Add(candidate);
								break;
							default:
								throw new NotImplementedEnumException(candidate.DegreeLevelEnum);
						}
				}

				this.DoctorateCandidates = doctorateCandidates
					.GroupBy(c => c.AdmissionOpenProgramID)
					.SelectMany(c =>
					{
						var entryTestDateFrom = c.Where(cc => cc.EntryTestDate != null).Min(cc => cc.EntryTestDate);
						var entryTestDateTo = c.Where(cc => cc.EntryTestDate != null).Max(cc => cc.EntryTestDate);
						var entryTestDates = entryTestDateFrom.ConcatenateDates(entryTestDateTo, "dd-MMM-yyyy", " to ", false);
						if (!string.IsNullOrWhiteSpace(entryTestDates))
						{
							if (entryTestDates.Length < 12)
								entryTestDates = $"(Test Date: {entryTestDates})";
							else
								entryTestDates = $"(Test Dates: {entryTestDates})";
						}
						return c.Select(cc => new Masters(cc, entryTestDates)).OrderByDescending(cc => cc.FinalSelectionG).ToList();
					}).ToList();
				this.MastersCandidates = mastersCandidates
					.GroupBy(c => c.AdmissionOpenProgramID)
					.SelectMany(c =>
					{
						var entryTestDateFrom = c.Where(cc => cc.EntryTestDate != null).Min(cc => cc.EntryTestDate);
						var entryTestDateTo = c.Where(cc => cc.EntryTestDate != null).Max(cc => cc.EntryTestDate);
						var entryTestDates = entryTestDateFrom.ConcatenateDates(entryTestDateTo, "dd-MMM-yyyy", "-", false);
						if (!string.IsNullOrWhiteSpace(entryTestDates))
						{
							if (entryTestDates.Length < 12)
								entryTestDates = $"(Test Date: {entryTestDates})";
							else
								entryTestDates = $"(Test Dates: {entryTestDates})";
						}
						return c.Select(cc => new Masters(cc, entryTestDates)).OrderByDescending(cc => cc.FinalSelectionG).ToList();
					}).ToList();
				this.BachelorsCandidates = bachelorsCandidates
					.GroupBy(c => c.AdmissionOpenProgramID)
					.SelectMany(c =>
					{
						var entryTestDateFrom = c.Where(cc => cc.EntryTestDate != null).Min(cc => cc.EntryTestDate);
						var entryTestDateTo = c.Where(cc => cc.EntryTestDate != null).Max(cc => cc.EntryTestDate);
						var entryTestDates = entryTestDateFrom.ConcatenateDates(entryTestDateTo, "dd-MMM-yyyy", "-", false);
						if (!string.IsNullOrWhiteSpace(entryTestDates))
						{
							if (entryTestDates.Length < 12)
								entryTestDates = $"(Test Date: {entryTestDates})";
							else
								entryTestDates = $"(Test Dates: {entryTestDates})";
						}
						return c.Select(cc => new Bachelors(cc, entryTestDates)).OrderByDescending(cc => cc.FinalSelectionH).ToList();
					}).ToList();
				this.DiplomaCandidates = diplomaCandidates
					.GroupBy(c => c.AdmissionOpenProgramID)
					.SelectMany(c =>
					{
						var entryTestDateFrom = c.Where(cc => cc.EntryTestDate != null).Min(cc => cc.EntryTestDate);
						var entryTestDateTo = c.Where(cc => cc.EntryTestDate != null).Max(cc => cc.EntryTestDate);
						var entryTestDates = entryTestDateFrom.ConcatenateDates(entryTestDateTo, "dd-MMM-yyyy", "-", false);
						if (!string.IsNullOrWhiteSpace(entryTestDates))
						{
							if (entryTestDates.Length < 12)
								entryTestDates = $"(Test Date: {entryTestDates})";
							else
								entryTestDates = $"(Test Dates: {entryTestDates})";
						}
						return c.Select(cc => new Bachelors(cc, entryTestDates)).OrderByDescending(cc => cc.FinalSelectionH).ToList();
					}).ToList();

				this.Summary = doctorateCandidates.Union(mastersCandidates).Union(bachelorsCandidates).Union(diplomaCandidates)
					.GroupBy(c => c.AdmissionOpenProgramID)
					.Select(c => new ReportSummary
					{
						InstituteName = c.First().InstituteName,
						DegreeLevelEnum = c.First().DegreeLevelEnum,
						SemesterID = c.First().SemesterID,
						DepartmentShortName = c.First().DepartmentShortName,
						ProgramShortName = c.First().ProgramShortName,
						EntryTestMarksPercentageFromInclusive = entryTestMarksPercentageFromInclusive,
						EntryTestMarksPercentageToExclusive = entryTestMarksPercentageToExclusive,
						TotalCandidates = c.Count(),
					}).ToList();
			}
		}

		internal sealed class Candidate
		{
			internal string Email { get; set; }
			internal int ApplicationNo { get; set; }
			internal int CandidateAppliedProgramID { get; set; }
			internal Categories CategoryEnum { get; set; }
			internal bool IsNaval => this.CategoryEnum.IsNaval();
			internal DateTime? EntryTestDate { get; set; }
			internal byte? EntryTestMarksPercentage { get; set; }
			internal string Name { get; set; }
			internal string Mobile { get; set; }
			internal DegreeTypes? ResultAwaitedDegreeTypeEnum { get; set; }
			internal List<AcademicRecord> AcademicRecords { get; set; }
			internal DegreeLevels DegreeLevelEnum { get; set; }
			internal byte? ETSType { get; set; }
			internal double? ETSObtained { get; set; }
			internal double? ETSTotal { get; set; }
			internal double? ETSPercentage
			{
				get
				{
					if (this.ETSType == null || this.ETSObtained == null || this.ETSTotal == null || this.ETSObtained.Value > this.ETSTotal.Value || this.ETSTotal.Value == 0)
						return null;
					return this.ETSObtained.Value * 100d / this.ETSTotal.Value;
				}
			}
			internal bool EntryTestMarksPercentageAndETSPercentageIsNull => this.EntryTestMarksPercentage == null && this.ETSPercentage == null;
			internal bool EntryTestPassed { get; set; }
			internal bool ETSPassed { get; set; }
			internal bool IsPassed => this.EntryTestPassed || this.ETSPassed;
			internal string ProgramShortName { get; set; }
			internal int AdmissionOpenProgramID { get; set; }
			internal short AppliedSemesterID { get; set; }
			internal string InstituteName { get; set; }
			internal string DepartmentShortName { get; set; }
			internal short SemesterID { get; set; }
			internal string Enrollment { get; set; }
			internal Model.Entities.Student.Statuses? StudentStatusEnum { get; set; }
			internal string StudentProgramAlias { get; set; }
			internal short? StudentIntakeSemesterID { get; set; }
		}

		internal sealed class AcademicRecord
		{
			public DegreeTypes DegreeTypeEnum { get; internal set; }
			public double Percentage { get; internal set; }
		}

		public static ReportDataSet Generate(short semesterID, int? departmentID, int? programID, List<int> applicationNos, DateTime? entryTestDateFrom, DateTime? entryTestDateTo, double? entryTestMarksPercentageFromInclusive, double? entryTestMarksPercentageToExclusive, double? etsMarksPercentageFromInclusive, double? etsMarksPercentageToExclusive, bool displayOnlyPassed, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var candidateAppliedPrograms = aspireContext.CandidateAppliedPrograms
					.Where(cap => cap.Candidate.SemesterID == semesterID && !cap.Candidate.ForeignStudent)
					.Where(cap => cap.AccountTransactionID != null)
					.Where(cap => cap.ApplicationNo != null);

				if (applicationNos != null && applicationNos.Any())
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => applicationNos.Contains(cap.ApplicationNo.Value));
				if (entryTestDateFrom != null)
				{
					entryTestDateFrom = entryTestDateFrom.Value.Date;
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.EntryTestDate >= entryTestDateFrom.Value);
				}
				if (entryTestDateTo != null)
				{
					entryTestDateTo = entryTestDateTo.Value.AddDays(1).Date;
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.EntryTestDate < entryTestDateTo.Value);
				}

				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms
					.Where(aop => aop.Program.InstituteID == loginHistory.InstituteID)
					.Where(aop => aop.SemesterID == semesterID);

				if (departmentID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.Program.DepartmentID == departmentID.Value);
				if (programID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.ProgramID == programID.Value);

				var candidates = from cap in candidateAppliedPrograms
								 join aop in admissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
								 select new Candidate
								 {
									 DegreeLevelEnum = (DegreeLevels)aop.Program.DegreeLevel,
									 ProgramShortName = aop.Program.ProgramShortName,
									 DepartmentShortName = aop.Program.Department.DepartmentShortName,
									 AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
									 AppliedSemesterID = aop.SemesterID,
									 InstituteName = aop.Program.Institute.InstituteName,
									 SemesterID = aop.SemesterID,
									 CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
									 ApplicationNo = cap.ApplicationNo.Value,
									 Name = cap.Candidate.Name,
									 Mobile = cap.Candidate.Mobile,
									 Email = cap.Candidate.Email,
									 CategoryEnum = (Categories)cap.Candidate.Category.Value,
									 ResultAwaitedDegreeTypeEnum = (DegreeTypes?)cap.ResultAwaitedDegreeType,
									 EntryTestMarksPercentage = cap.EntryTestMarksPercentage,
									 EntryTestDate = cap.EntryTestDate,
									 ETSType = cap.ETSType,
									 ETSObtained = cap.ETSObtained,
									 ETSTotal = cap.ETSTotal,
									 AcademicRecords = cap.CandidateAppliedProgramAcademicRecords.Select(a => new AcademicRecord
									 {
										 DegreeTypeEnum = (DegreeTypes)a.DegreeType,
										 Percentage = a.Percentage
									 }).ToList(),
									 Enrollment = cap.Student.Enrollment,
									 StudentStatusEnum = (Model.Entities.Student.Statuses?)cap.Student.Status,
									 StudentProgramAlias = cap.Student.AdmissionOpenProgram.Program.ProgramAlias,
									 StudentIntakeSemesterID = cap.Student.AdmissionOpenProgram.SemesterID,
									 EntryTestPassed = false,
									 ETSPassed = false
								 };

				return new ReportDataSet(candidates.ToList(), entryTestMarksPercentageFromInclusive, entryTestMarksPercentageToExclusive, etsMarksPercentageFromInclusive, etsMarksPercentageToExclusive, displayOnlyPassed);
			}
		}
	}
}
