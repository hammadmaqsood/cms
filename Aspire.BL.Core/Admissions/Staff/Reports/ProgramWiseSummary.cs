﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Staff.Reports
{
	public static class ProgramWiseSummary
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class ProgramSummary
		{
			public int ProgramID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public int? IntakeCount { get; internal set; }
			public int AppliedCount { get; internal set; }
			public int AdmissionProcessingFeePaidCount { get; internal set; }
			public int SemesterFeePaidCount { get; internal set; }

			public static List<ProgramSummary> GetProgramSummary()
			{
				return null;
			}
		}

		public sealed class PageHeader
		{
			public string Institute { get; internal set; }
			public string Title { get; internal set; }
			public string Filters { get; internal set; }

			public static List<PageHeader> GetProgramSummaryHeaderList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public PageHeader PageHeader { get; internal set; }
			public List<ProgramSummary> ProgramSummary { get; internal set; }
		}

		public static ReportDataSet GetProgramWiseSummery(short semesterID, int? admissionOpenProgramID, Shifts? shiftEnum, int? applicationNo, DateTime? applyDateFrom, DateTime? applyDateTo, DateTime? depositDateFrom, DateTime? depositDateTo, bool? foreignCandidate, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var filtersList = new List<string>();
				var candidateAppliedPrograms = aspireContext.CandidateAppliedPrograms.AsQueryable();
				var admissionOpenPrograms = aspireContext.Programs.Where(p => p.InstituteID == loginHistory.InstituteID)
					.SelectMany(p => p.AdmissionOpenPrograms)
					.Where(aop => aop.SemesterID == semesterID);

				if (admissionOpenProgramID != null)
				{
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID.Value);
					filtersList.Add("Program = " + admissionOpenPrograms.Select(aop => aop.Program.ProgramAlias).Single());
				}
				if (shiftEnum != null)
				{
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.Shift == (byte?)shiftEnum);
					filtersList.Add("Shift = " + shiftEnum.Value.ToFullName());
				}
				if (applicationNo != null)
				{
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.ApplicationNo == applicationNo && cap.AccountTransactionID != null);
					filtersList.Add("Application No. = " + applicationNo.Value);
				}
				if (applyDateFrom != null)
				{
					applyDateFrom = applyDateFrom.Value.Date;
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.ApplyDate >= applyDateFrom);
					filtersList.Add("Apply Date >= " + applyDateFrom);
				}
				if (applyDateTo != null)
				{
					applyDateTo = applyDateTo.Value.Date.AddDays(1);
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.ApplyDate < applyDateTo.Value);
					filtersList.Add("Apply Date <= " + applyDateTo.Value.AddDays(-1));
				}
				if (depositDateFrom != null)
				{
					depositDateFrom = depositDateFrom.Value.Date;
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.AccountTransactionID != null && cap.AccountTransaction.TransactionDate >= depositDateFrom.Value);
					filtersList.Add("Deposit Date >= " + depositDateFrom);
				}
				if (depositDateTo != null)
				{
					depositDateTo = depositDateTo.Value.Date.AddDays(1);
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.AccountTransactionID != null && cap.AccountTransaction.TransactionDate < depositDateTo);
					filtersList.Add("Deposit Date <= " + depositDateTo.Value.AddDays(-1));
				}
				if (foreignCandidate != null)
				{
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.Candidate.ForeignStudent == foreignCandidate);
					filtersList.Add("Foreign Candidate = " + foreignCandidate.Value.ToYesNo());
				}

				var getCandidates = (from cap in candidateAppliedPrograms
									 join aop in admissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
									 select new
									 {
										 aop.ProgramID,
										 aop.Program.ProgramAlias,
										 IntakeTarget = aop.IntakeTargetStudentsCount,
										 AccountTransactionDate = (DateTime?)cap.AccountTransaction.TransactionDate,
										 AdmissionFeeChallanPaid = cap.StudentFees.Any(sf => sf.FeeType == (byte)StudentFee.FeeTypes.Challan && sf.Status == (byte)StudentFee.Statuses.Paid),
										 //AdmissionFeeRefunded = cap.StudentFees.Any(sf => sf.FeeType == (byte)StudentFee.FeeTypes.Refund)
									 }).ToList();

				var getCandidatesData = getCandidates.GroupBy(s => new { s.ProgramID, s.ProgramAlias, s.IntakeTarget })
							  .Select(g => new ProgramSummary
							  {
								  ProgramAlias = g.Key.ProgramAlias,
								  IntakeCount = g.Key.IntakeTarget,
								  AppliedCount = g.Count(),
								  AdmissionProcessingFeePaidCount = g.Count(c => c.AccountTransactionDate != null),
								  SemesterFeePaidCount = g.Count(c => c.AdmissionFeeChallanPaid),
							  }).OrderBy(c => c.ProgramAlias).ToList();

				return new ReportDataSet
				{
					PageHeader = new PageHeader
					{
						Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single(),
						Title = $"Program Wise Summary ({semesterID.ToSemesterString()})",
						Filters = string.Join(", ", filtersList),
					},
					ProgramSummary = getCandidatesData,
				};
			}
		}


	}
}


