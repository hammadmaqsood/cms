﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Staff.Reports
{
	public static class GeneratedEnrollments
	{
		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public ReportHeader ReportHeader { get; internal set; }
			public List<Student> Students { get; internal set; }
		}

		public sealed class ReportHeader
		{
			public string InstituteName { get; internal set; }
			public string ProgramName { get; internal set; }
			internal short SemesterID { get; set; }
			public string Semester => this.SemesterID.ToSemesterString();

			public List<ReportHeader> GetList()
			{
				return null;
			}
		}

		public sealed class Student
		{
			public string Enrollment { get; internal set; }
			public int? ApplicationNo { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string Name { get; internal set; }
			public string FatherName { get; internal set; }
			internal byte Gender { get; set; }
			public string GenderFullName => ((Genders)this.Gender).ToFullName();
			internal byte Category { get; set; }
			public string CategoryFullName => ((Categories)this.Category).ToFullName();
			public string CandidateEmail { get; internal set; }
			public string CandidateMobile { get; internal set; }
			public DateTime? EnrollmentGeneratedDate { get; internal set; }
			internal short? Status { get; set; }
			public string StatusFullName => (((Model.Entities.Student.Statuses?)this.Status)?.ToFullName()).ToNAIfNullOrEmpty();
			public byte? AdmittedShift { get; internal set; }
			public string AdmittedShiftFullName => ((Shifts?)this.AdmittedShift)?.ToFullName();

			public List<Student> GetList()
			{
				return null;
			}
		}

		public static ReportDataSet GetGeneratedEnrollments(int admissionOpenProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
				var students = (from s in aspireContext.Students
								join cap in aspireContext.CandidateAppliedPrograms on s.CandidateAppliedProgramID equals cap.CandidateAppliedProgramID into joined
								from j in joined.DefaultIfEmpty()
								where s.AdmissionOpenProgramID == admissionOpenProgramID && s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID
								select new Student
								{
									Enrollment = s.Enrollment,
									ApplicationNo = j == null ? null : j.ApplicationNo,
									RegistrationNo = s.RegistrationNo ?? null,
									Name = s.Name,
									FatherName = s.FatherName,
									Gender = s.Gender,
									Category = s.Category,
									CandidateEmail = s.PersonalEmail,
									CandidateMobile = s.Mobile,
									EnrollmentGeneratedDate = s.CreatedDate,
									Status = s.Status,
									AdmittedShift = j == null ? null : j.AdmittedShift,
								}).ToList();

				return new ReportDataSet
				{
					Students = students,
					ReportHeader = aspireContext.AdmissionOpenPrograms.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID && aop.Program.InstituteID == loginHistory.InstituteID).Select(aop => new ReportHeader
					{
						InstituteName = aop.Program.Institute.InstituteName,
						ProgramName = aop.Program.ProgramName,
						SemesterID = aop.SemesterID,
					}).Single()
				};
			}
		}
	}
}
