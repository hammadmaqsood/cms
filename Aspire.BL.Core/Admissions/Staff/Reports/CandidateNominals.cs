﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Staff.Reports
{
	public static class CandidateNominals
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class Candidate
		{
			public string Institute { get; set; }
			public string Program { get; set; }
			public int? ApplicationNo { get; set; }
			public string Name { get; set; }
			public string FatherName { get; set; }
			public DateTime ApplyDate { get; set; }
			public DateTime? DepositDate { get; set; }
			public string BankAlias { get; set; }
			public string Mobile { get; set; }
			public string Phone { get; set; }
			public byte Gender { get; set; }
			public string Email { get; set; }
			public bool? ForeignCandidate { get; set; }

			public string CandidateName
			{
				get
				{
					switch ((Genders)this.Gender)
					{
						case Genders.Male:
							return $"{this.Name} S/O {this.FatherName}";
						case Genders.Female:
							return $"{this.Name} D/O {this.FatherName}";
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}

			public string MobilePhone => $"{this.Mobile},{this.Phone}";
			public string ForeignCandidateYesNo => this.ForeignCandidate?.ToYesNo();
			public string TransactionReferenceID { get; internal set; }

			public List<Candidate> GetCandidateList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public List<Candidate> Candidates { get; internal set; }
			public string Filters { get; internal set; }
		}

		public static ReportDataSet GetCandidateNominal(short semesterID, int? admissionOpenProgramID, Shifts? shiftEnum, int? applicationNo, bool? admissionProcessingFeeSubmitted, DateTime? applyDateFrom, DateTime? applyDateTo, DateTime? depositDateFrom, DateTime? depositDateTo, bool? foreignCandidate, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var filtersList = new List<string>();
				var candidateAppliedPrograms = aspireContext.CandidateAppliedPrograms.AsQueryable();
				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms.Where(aop => aop.SemesterID == semesterID && aop.Program.InstituteID == loginHistory.InstituteID);

				if (admissionOpenProgramID != null)
				{
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID);
					filtersList.Add("Program = " + admissionOpenPrograms.Select(aop => aop.Program.ProgramAlias).Single());
				}
				if (shiftEnum != null)
				{
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.AdmissionOpenProgram1.Shifts == (byte?)shiftEnum);
					filtersList.Add("Shift = " + shiftEnum.ToString().SplitCamelCasing());
				}
				if (applicationNo != null)
				{
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.ApplicationNo == applicationNo);
					filtersList.Add("Application No. = " + applicationNo);
				}

				if (admissionProcessingFeeSubmitted != null)
				{
					if (admissionProcessingFeeSubmitted.Value)
						candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.AccountTransactionID != null);
					else
						candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.AccountTransactionID == null);
					filtersList.Add("Admission Processing Fee Submitted = " + admissionProcessingFeeSubmitted.Value.ToYesNo());
				}

				if (applyDateFrom != null)
				{
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.ApplyDate >= applyDateFrom);
					filtersList.Add("Apply Date >= " + applyDateFrom);
				}

				if (applyDateTo != null)
				{
					applyDateTo = applyDateTo.Value.Date.AddDays(1);
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.ApplyDate <= applyDateTo);
					filtersList.Add("Apply Date <= " + applyDateTo.Value.AddDays(-1));
				}

				if (depositDateFrom != null)
				{
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.AccountTransactionID != null && cap.AccountTransaction.TransactionDate >= depositDateFrom);
					filtersList.Add("Deposit Date >= " + depositDateFrom);
				}

				if (depositDateTo != null)
				{
					depositDateTo = depositDateTo.Value.Date.AddDays(1);
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.AccountTransactionID != null && cap.AccountTransaction.TransactionDate < depositDateTo);
					filtersList.Add("Deposit Date <= " + depositDateTo.Value.AddDays(-1));
				}
				if (foreignCandidate != null)
				{
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.Candidate.ForeignStudent == foreignCandidate);
					filtersList.Add("Foreign Candidate = " + foreignCandidate?.ToYesNo());
				}

				var query = from cap in candidateAppliedPrograms
							join aop in admissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
							select new Candidate
							{
								Institute = aop.Program.Institute.InstituteShortName,
								Program = aop.Program.ProgramAlias,
								ApplicationNo = cap.ApplicationNo,
								Name = cap.Candidate.Name,
								Gender = cap.Candidate.Gender.Value,
								FatherName = cap.Candidate.FatherName,
								Email = cap.Candidate.Email,
								ForeignCandidate = foreignCandidate,
								ApplyDate = cap.ApplyDate,
								Mobile = cap.Candidate.Mobile,
								Phone = cap.Candidate.Phone,
								DepositDate = (DateTime?)cap.AccountTransaction.TransactionDate,
								BankAlias = cap.AccountTransaction.InstituteBankAccount.Bank.BankAlias,
								TransactionReferenceID = cap.AccountTransaction.TransactionReferenceID,
							};

				return new ReportDataSet
				{
					Filters = string.Join(", ", filtersList),
					Candidates = query.ToList(),
				};
			}
		}
	}
}
