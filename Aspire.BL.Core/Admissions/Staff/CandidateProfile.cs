﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Admissions.Staff
{
	public static class CandidateInfo
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class GetCandidateInformationResult
		{
			public sealed class Candidate
			{
				internal Candidate() { }
				public int CandidateAppliedProgramID { get; internal set; }
				public int ChallanNo { get; internal set; }
				public string FullChallanNo => AspireFormats.ChallanNo.GetFullChallanNoString(this.InstituteCode, InstituteBankAccount.FeeTypes.AdmissionProcessingFee, this.ChallanNo);
				public int CandidateID { get; internal set; }
				public short SemesterID { get; internal set; }
				public string Semester => this.SemesterID.ToSemesterString();
				public int? ApplicationNo { get; internal set; }
				public string Enrollment { get; internal set; }
				public string Name { get; internal set; }
				public string Email { get; internal set; }
				public string FatherName { get; internal set; }
				public string InstituteAlias { get; internal set; }
				public string InstituteCode { get; internal set; }
				public string ProgramAlias { get; internal set; }
				public string Nationality { get; internal set; }
				public string Country { get; internal set; }
				public bool ForeignStudent { get; internal set; }
			}

			internal GetCandidateInformationResult() { }
			public int VirtualItemCount { get; internal set; }
			public List<Candidate> Candidates { get; internal set; }
		}

		public static GetCandidateInformationResult GetCandidateInformation(short? semesterID, AspireFormats.ChallanNo? challanNo, int? applicationNo, string name, string fatherName, string email, bool? foreignStudent, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var candidateAppliedPrograms = aspireContext.CandidateAppliedPrograms.AsQueryable();
				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms.Where(aop => aop.Program.InstituteID == loginHistory.InstituteID);
				var candidates = aspireContext.Candidates.AsQueryable();

				if (semesterID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.SemesterID == semesterID);
				if (challanNo != null)
				{
					var challanID = challanNo.Value.ChallanID;
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.ChallanNo == challanID);
				}
				if (applicationNo != null)
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.ApplicationNo == applicationNo && (cap.Candidate.ForeignStudent || cap.AccountTransactionID != null));
				if (!string.IsNullOrWhiteSpace(name))
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.Candidate.Name.Contains(name));
				if (!string.IsNullOrWhiteSpace(fatherName))
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.Candidate.FatherName.Contains(fatherName));
				if (!string.IsNullOrWhiteSpace(email))
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.Candidate.Email.Contains(email));
				if (foreignStudent != null)
					candidateAppliedPrograms = candidateAppliedPrograms.Where(cap => cap.Candidate.ForeignStudent == foreignStudent.Value);

				var query = from c in candidates
							join cap in candidateAppliedPrograms on c.CandidateID equals cap.CandidateID
							join aop in admissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
							select new GetCandidateInformationResult.Candidate
							{
								CandidateID = cap.CandidateID,
								CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
								ChallanNo = cap.ChallanNo,
								SemesterID = aop.SemesterID,
								ApplicationNo = cap.AccountTransactionID != null || cap.Candidate.ForeignStudent ? cap.ApplicationNo : null,
								Name = cap.Candidate.Name,
								FatherName = cap.Candidate.FatherName,
								Email = cap.Candidate.Email,
								Nationality = cap.Candidate.Nationality,
								Country = cap.Candidate.Country,
								ProgramAlias = aop.Program.ProgramAlias,
								InstituteAlias = aop.Program.Institute.InstituteAlias,
								InstituteCode = aop.Program.Institute.InstituteCode,
								Enrollment = cap.Student.Enrollment,
								ForeignStudent = cap.Candidate.ForeignStudent
							};

				var virtualItemCount = query.Count();
				switch (sortExpression)
				{
					case nameof(GetCandidateInformationResult.Candidate.InstituteAlias):
						query = query.OrderBy(sortDirection, c => c.InstituteAlias);
						break;
					case nameof(GetCandidateInformationResult.Candidate.SemesterID):
						query = query.OrderBy(sortDirection, c => c.SemesterID);
						break;
					case nameof(GetCandidateInformationResult.Candidate.ApplicationNo):
						query = query.OrderBy(sortDirection, c => c.ApplicationNo);
						break;
					case nameof(GetCandidateInformationResult.Candidate.Name):
						query = query.OrderBy(sortDirection, c => c.Name);
						break;
					case nameof(GetCandidateInformationResult.Candidate.FatherName):
						query = query.OrderBy(sortDirection, c => c.FatherName);
						break;
					case nameof(GetCandidateInformationResult.Candidate.Enrollment):
						query = query.OrderBy(sortDirection, c => c.Enrollment);
						break;
					case nameof(GetCandidateInformationResult.Candidate.ProgramAlias):
						query = query.OrderBy(sortDirection, c => c.ProgramAlias);
						break;
					case nameof(GetCandidateInformationResult.Candidate.Email):
						query = query.OrderBy(sortDirection, c => c.Email);
						break;
					case nameof(GetCandidateInformationResult.Candidate.ChallanNo):
					case nameof(GetCandidateInformationResult.Candidate.FullChallanNo):
						query = query.OrderBy(sortDirection, c => c.ChallanNo);
						break;
					case nameof(GetCandidateInformationResult.Candidate.Nationality):
						query = query.OrderBy(sortDirection, c => c.Nationality);
						break;
					case nameof(GetCandidateInformationResult.Candidate.Country):
						query = query.OrderBy(sortDirection, c => c.Country);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return new GetCandidateInformationResult
				{
					VirtualItemCount = virtualItemCount,
					Candidates = query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
				};
			}
		}
	}
}
