﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FileServer;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Staff
{
	public static class StudentTransfer
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.StudentTransfer, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class Student
		{
			internal Student()
			{
			}

			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string InstituteAlias { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public string ProgramShortName { get; internal set; }
			public byte? Status { get; set; }
			public DateTime? StatusDate { get; internal set; }
			public int InstituteID { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string Name { get; internal set; }
			public Model.Entities.Student.Statuses? StatusEnum => (Model.Entities.Student.Statuses?)this.Status;
			public string StatusFullName => this.StatusEnum?.ToFullName();
			public List<TransferredStudent> TransferredStudents { get; internal set; }

			public sealed class TransferredStudent
			{
				internal TransferredStudent() { }
				public int TransferredStudentID { get; internal set; }
				public string FromEnrollment { get; internal set; }
				public int? FromRegistrationNo { get; internal set; }
				public string FromInstituteAlias { get; internal set; }
				public string FromProgramShortName { get; internal set; }
				public string Remarks { get; internal set; }
				public string ToEnrollment { get; internal set; }
				public int? ToRegistrationNo { get; internal set; }
				public int ToInstituteID { get; internal set; }
				public string ToInstituteAlias { get; internal set; }
				public string ToProgramShortName { get; internal set; }
				public short TransferSemesterID { get; internal set; }
				public string TransferSemester => this.TransferSemesterID.ToSemesterString();
				public DateTime TransferDate { get; internal set; }
				public bool CanManage { get; internal set; }
			}
		}

		private static List<int> GetTransferChain(this AspireContext aspireContext, List<int> studentIDs)
		{
			var transferredStudents = aspireContext.TransferredStudents
				.Where(ts => studentIDs.Contains(ts.TransferredFromStudentID) || studentIDs.Contains(ts.TransferredToStudentID))
				.Select(ts => new
				{
					ts.TransferredFromStudentID,
					ts.TransferredToStudentID,
				}).ToList();
			var newStudentIDs = transferredStudents.Select(ts => ts.TransferredFromStudentID)
				.Union(transferredStudents.Select(ts => ts.TransferredToStudentID))
				.Distinct()
				.OrderBy(i => i)
				.ToList();
			return newStudentIDs.SequenceEqual(studentIDs)
				? newStudentIDs
				: aspireContext.GetTransferChain(newStudentIDs);
		}

		internal sealed class TransferredStudentsDetail
		{
			public int TransferredStudentID { get; set; }
			public int TransferredFromStudentID { get; set; }
			public int TransferredToStudentID { get; set; }
			public short TransferSemesterID { get; set; }
			public string Remarks { get; set; }
			public string FromEnrollment { get; set; }
			public int? FromRegistrationNo { get; set; }
			public string FromName { get; set; }
			public string FromFatherName { get; set; }
			public short FromIntakeSemesterID { get; set; }
			public int FromInstituteID { get; internal set; }
			public string FromInstituteAlias { get; set; }
			public int? FromDepartmentID { get; internal set; }
			public int FromProgramID { get; internal set; }
			public string FromProgramShortName { get; set; }
			public int FromAdmissionOpenProgramID { get; internal set; }
			public string ToEnrollment { get; set; }
			public int? ToRegistrationNo { get; set; }
			public string ToName { get; set; }
			public string ToFatherName { get; set; }
			public short ToIntakeSemesterID { get; set; }
			public int ToInstituteID { get; internal set; }
			public string ToInstituteAlias { get; set; }
			public int? ToDepartmentID { get; internal set; }
			public int ToProgramID { get; internal set; }
			public string ToProgramShortName { get; set; }
			public int ToAdmissionOpenProgramID { get; internal set; }
			public DateTime TransferDate { get; set; }
		}

		private static IQueryable<TransferredStudentsDetail> GetTransferredStudentsQuery(this AspireContext aspireContext)
		{
			var studentsQuery = aspireContext.Students.Select(s => new
			{
				s.StudentID,
				s.Enrollment,
				s.RegistrationNo,
				s.Name,
				s.FatherName,
				IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
				s.AdmissionOpenProgram.Program.InstituteID,
				s.AdmissionOpenProgram.Program.Institute.InstituteAlias,
				s.AdmissionOpenProgram.Program.DepartmentID,
				s.AdmissionOpenProgram.Program.ProgramShortName,
				s.AdmissionOpenProgram.ProgramID,
				s.AdmissionOpenProgramID
			});

			return from tranStudent in aspireContext.TransferredStudents
				   join fromStudent in studentsQuery on tranStudent.TransferredFromStudentID equals fromStudent.StudentID
				   join toStudent in studentsQuery on tranStudent.TransferredToStudentID equals toStudent.StudentID
				   select new TransferredStudentsDetail
				   {
					   TransferredStudentID = tranStudent.TransferredStudentID,
					   TransferredFromStudentID = tranStudent.TransferredFromStudentID,
					   TransferredToStudentID = tranStudent.TransferredToStudentID,
					   TransferSemesterID = tranStudent.TransferSemesterID,
					   TransferDate = tranStudent.TransferDate,
					   Remarks = tranStudent.Remarks,
					   FromEnrollment = fromStudent.Enrollment,
					   FromRegistrationNo = fromStudent.RegistrationNo,
					   FromName = fromStudent.Name,
					   FromFatherName = fromStudent.FatherName,
					   FromIntakeSemesterID = fromStudent.IntakeSemesterID,
					   FromInstituteID = fromStudent.InstituteID,
					   FromInstituteAlias = fromStudent.InstituteAlias,
					   FromDepartmentID = fromStudent.DepartmentID,
					   FromProgramID = fromStudent.ProgramID,
					   FromProgramShortName = fromStudent.ProgramShortName,
					   FromAdmissionOpenProgramID = fromStudent.AdmissionOpenProgramID,
					   ToEnrollment = toStudent.Enrollment,
					   ToRegistrationNo = toStudent.RegistrationNo,
					   ToName = toStudent.Name,
					   ToFatherName = toStudent.FatherName,
					   ToIntakeSemesterID = toStudent.IntakeSemesterID,
					   ToInstituteID = toStudent.InstituteID,
					   ToInstituteAlias = toStudent.InstituteAlias,
					   ToDepartmentID = toStudent.DepartmentID,
					   ToProgramID = toStudent.ProgramID,
					   ToProgramShortName = toStudent.ProgramShortName,
					   ToAdmissionOpenProgramID = toStudent.AdmissionOpenProgramID
				   };
		}

		public static Student GetStudent(int instituteID, string enrollment, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(enrollment))
				throw new ArgumentNullException(nameof(enrollment));
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var student = aspireContext.Students
					.Where(s => s.Enrollment == enrollment && s.AdmissionOpenProgram.Program.InstituteID == instituteID)
					.Select(s => new Student
					{
						StudentID = s.StudentID,
						RegistrationNo = s.RegistrationNo,
						Name = s.Name,
						Enrollment = s.Enrollment,
						IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
						InstituteID = s.AdmissionOpenProgram.Program.InstituteID,
						ProgramShortName = s.AdmissionOpenProgram.Program.ProgramShortName,
						InstituteAlias = s.AdmissionOpenProgram.Program.Institute.InstituteAlias,
						Status = s.Status,
						StatusDate = s.StatusDate,
					}).SingleOrDefault();
				if (student == null)
					return null;
				var studentIDs = aspireContext.GetTransferChain(new List<int> { student.StudentID });

				student.TransferredStudents = aspireContext.GetTransferredStudentsQuery()
					.Where(ts => studentIDs.Contains(ts.TransferredFromStudentID) || studentIDs.Contains(ts.TransferredToStudentID))
					.Select(ts => new Student.TransferredStudent
					{
						CanManage = ts.ToInstituteID == loginHistory.InstituteID,
						FromEnrollment = ts.FromEnrollment,
						FromInstituteAlias = ts.FromInstituteAlias,
						FromProgramShortName = ts.FromProgramShortName,
						FromRegistrationNo = ts.FromRegistrationNo,
						Remarks = ts.Remarks,
						ToEnrollment = ts.ToEnrollment,
						ToInstituteAlias = ts.ToInstituteAlias,
						ToInstituteID = ts.ToInstituteID,
						ToProgramShortName = ts.ToProgramShortName,
						ToRegistrationNo = ts.ToRegistrationNo,
						TransferDate = ts.TransferDate,
						TransferredStudentID = ts.TransferredStudentID,
						TransferSemesterID = ts.TransferSemesterID
					})
					.OrderBy(ts => ts.TransferDate)
					.ToList();
				return student;
			}
		}

		internal static (string Enrollment, int? existingStudentID, bool AlreadyGenerated) GetNewEnrollment(this AspireContext aspireContext, int studentID, int admissionOpenProgramID, Shifts shiftEnum, Guid loginSessionGuid)
		{
			var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
			var studentIDs = aspireContext.GetTransferChain(new List<int> { studentID });
			var enrollmentAlreadyGenerated = aspireContext.Students
				.Where(s => studentIDs.Contains(s.StudentID) && s.AdmissionOpenProgramID == admissionOpenProgramID && s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
				.Select(s => new { s.StudentID, s.Enrollment })
				.SingleOrDefault();
			if (enrollmentAlreadyGenerated != null)
				return (enrollmentAlreadyGenerated.Enrollment, enrollmentAlreadyGenerated.StudentID, true);

			var newEnrollmentToBeGenerated = aspireContext.GetNewEnrollment(admissionOpenProgramID, shiftEnum);
			return (newEnrollmentToBeGenerated, null, false);
		}

		public static (string Enrollment, bool AlreadyGenerated) GetNewEnrollment(int studentID, int admissionOpenProgramID, Shifts shiftEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var (enrollment, _, alreadyGenerated) = aspireContext.GetNewEnrollment(studentID, admissionOpenProgramID, shiftEnum, loginSessionGuid);
				return (enrollment, alreadyGenerated);
			}
		}

		private static (string newEnrollment, int newStudentID) AddStudent(this AspireContext aspireContext, int existingStudentID, Model.Entities.Student.Statuses existingStudentNewStatus, int newAdmissionOpenProgramID, Shifts newShift, int userLoginHistoryID)
		{
			var existingStudent = aspireContext.Students.Include(s => s.StudentAcademicRecords)
				.Single(s => s.StudentID == existingStudentID);

			var newEnrollment = aspireContext.GetNewEnrollment(newAdmissionOpenProgramID, newShift);
			var newStudent = aspireContext.Students.Add(new Model.Entities.Student
			{
				Enrollment = newEnrollment,
				AdmissionOpenProgramID = newAdmissionOpenProgramID,
				CreatedDate = DateTime.Now,
				RegistrationNo = existingStudent.RegistrationNo,
				Name = existingStudent.Name,
				PersonalEmail = existingStudent.PersonalEmail,
				PersonalEmailNotVerified = existingStudent.PersonalEmailNotVerified,
				Password = existingStudent.Password,
				CNIC = existingStudent.CNIC,
				PassportNo = existingStudent.PassportNo,
				Gender = existingStudent.Gender,
				Category = existingStudent.Category,
				DOB = existingStudent.DOB,
				BloodGroup = existingStudent.BloodGroup,
				Phone = existingStudent.Phone,
				Mobile = existingStudent.Mobile,
				Nationality = existingStudent.Nationality,
				Country = existingStudent.Country,
				Province = existingStudent.Province,
				District = existingStudent.District,
				Tehsil = existingStudent.Tehsil,
				Domicile = existingStudent.Domicile,
				AreaType = existingStudent.AreaType,
				CurrentAddress = existingStudent.CurrentAddress,
				PermanentAddress = existingStudent.PermanentAddress,
				ServiceNo = existingStudent.ServiceNo,
				FatherName = existingStudent.FatherName,
				FatherCNIC = existingStudent.FatherCNIC,
				FatherPassportNo = existingStudent.FatherPassportNo,
				FatherDesignation = existingStudent.FatherDesignation,
				FatherDepartment = existingStudent.FatherDepartment,
				FatherOrganization = existingStudent.FatherOrganization,
				FatherServiceNo = existingStudent.FatherServiceNo,
				FatherOfficePhone = existingStudent.FatherOfficePhone,
				FatherHomePhone = existingStudent.FatherHomePhone,
				FatherMobile = existingStudent.FatherMobile,
				FatherFax = existingStudent.FatherFax,
				SponsoredBy = existingStudent.SponsoredBy,
				SponsorName = existingStudent.SponsorName,
				SponsorCNIC = existingStudent.SponsorCNIC,
				SponsorPassportNo = existingStudent.SponsorPassportNo,
				SponsorRelationshipWithGuardian = existingStudent.SponsorRelationshipWithGuardian,
				SponsorDesignation = existingStudent.SponsorDesignation,
				SponsorDepartment = existingStudent.SponsorDepartment,
				SponsorOrganization = existingStudent.SponsorOrganization,
				SponsorServiceNo = existingStudent.SponsorServiceNo,
				SponsorPhoneHome = existingStudent.SponsorPhoneHome,
				SponsorMobile = existingStudent.SponsorMobile,
				SponsorPhoneOffice = existingStudent.SponsorPhoneOffice,
				SponsorFax = existingStudent.SponsorFax,
				ETSType = existingStudent.ETSType,
				ETSObtained = existingStudent.ETSObtained,
				ETSTotal = existingStudent.ETSTotal,
				EmergencyContactName = existingStudent.EmergencyContactName,
				EmergencyMobile = existingStudent.EmergencyMobile,
				EmergencyPhone = existingStudent.EmergencyPhone,
				EmergencyRelationship = existingStudent.EmergencyRelationship,
				EmergencyAddress = existingStudent.EmergencyAddress,
				PhysicalDisability = existingStudent.PhysicalDisability,
				AdmissionType = (byte)Model.Entities.Student.AdmissionTypes.Transferred,
				Religion = existingStudent.Religion,
				NextOfKin = existingStudent.NextOfKin,
				ParentsEmail = existingStudent.ParentsEmail,
				ParentsPassword = existingStudent.ParentsPassword,
				CandidateAppliedProgramID = null,
				MeritListScore = null,
				MeritListPosition = null,
				UniversityEmail = null,
				ConfirmationCode = null,
				ConfirmationCodeExpiryDate = null,
				MobileNotVerified = null,
				MobileVerificationCode = null,
				Status = null,
				StatusDate = null,
				MigrationID = null,
				UniversityEmailPassword = null,
				FinalCGPA = null,
				FinalTranscriptNo = null,
				FinalTranscriptIssueDate = null,
				DeferredToAdmissionOpenProgramID = null,
				DeferredToShift = null,
				DeferredToStudentID = null,
				DeferredFromStudentID = null,
				ParentsConfirmationCode = null,
				ParentsConfirmationCodeExpiryDate = null,
				DegreeNo = null,
				DegreeIssueDate = null,
				DegreeIssuedToStudent = null,
				StudentAcademicRecords = existingStudent.StudentAcademicRecords.Select(sar => new StudentAcademicRecord
				{
					DegreeType = sar.DegreeType,
					ObtainedMarks = sar.ObtainedMarks,
					TotalMarks = sar.TotalMarks,
					Percentage = sar.Percentage,
					IsCGPA = sar.IsCGPA,
					Subjects = sar.Subjects,
					Institute = sar.Institute,
					BoardUniversity = sar.BoardUniversity,
					PassingYear = sar.PassingYear,
					Status = sar.Status,
					Remarks = sar.Remarks
				}).ToList()
			});

			existingStudent.StatusEnum = existingStudentNewStatus;
			existingStudent.StatusDate = newStudent.CreatedDate;

			aspireContext.SaveChanges(userLoginHistoryID);
			return (newStudent.Enrollment, newStudent.StudentID);
		}

		public sealed class TransferStudentResult
		{
			public enum Statuses
			{
				ErrorAlreadyEnrolled,
				ErrorAlreadyTransferred,
				ErrorStudentStatusMustBeTransferredToOtherCampus,
				ErrorStudentStatusMustBeProgramChanged,
				ErrorCannotTransferToPreviousIntakeSemesters,
				ErrorStudentCannotTransferToPreviousSemesters,
				Success,
			}

			internal TransferStudentResult(Statuses status)
			{
				this.Status = status;
			}

			public Statuses Status { get; }
			public string GeneratedEnrollment { get; internal set; }
		}

		public static TransferStudentResult TransferStudent(int studentID, int toAdmissionOpenProgramID, Shifts toShiftEnum, short transferSemesterID, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				aspireContext.DemandModulePermissions(loginSessionGuid);
				var student = aspireContext.Students
					.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.StudentID,
						s.Name,
						StatusEnum = (Model.Entities.Student.Statuses?)s.Status,
						IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgram.AdmissionOpenProgramID,
					}).SingleOrDefault();
				if (student == null)
					return null;

				var toAdmissionOpenProgram = aspireContext.AdmissionOpenPrograms
					.Where(aop => aop.AdmissionOpenProgramID == toAdmissionOpenProgramID)
					.Select(aop => new
					{
						aop.SemesterID,
						aop.Program.ProgramShortName,
						aop.Program.InstituteID,
						aop.Program.DepartmentID,
						aop.Program.ProgramID,
						aop.AdmissionOpenProgramID,
					})
					.SingleOrDefault();
				if (toAdmissionOpenProgram == null)
					return null;

				if (student.AdmissionOpenProgramID == toAdmissionOpenProgram.AdmissionOpenProgramID)
					return new TransferStudentResult(TransferStudentResult.Statuses.ErrorAlreadyEnrolled);

				var alreadyTransferred = aspireContext.GetTransferredStudentsQuery()
					.Any(ts => ts.TransferredFromStudentID == student.StudentID
								&& ts.TransferSemesterID == transferSemesterID
								&& ts.ToAdmissionOpenProgramID == toAdmissionOpenProgram.AdmissionOpenProgramID);
				if (alreadyTransferred)
					return new TransferStudentResult(TransferStudentResult.Statuses.ErrorAlreadyTransferred);

				if (toAdmissionOpenProgram.SemesterID < student.IntakeSemesterID)
					return new TransferStudentResult(TransferStudentResult.Statuses.ErrorCannotTransferToPreviousIntakeSemesters);

				var studentIDs = aspireContext.GetTransferChain(new List<int> { studentID });
				var transferredStudents = aspireContext.TransferredStudents
					.Where(ts => studentIDs.Contains(ts.TransferredFromStudentID) || studentIDs.Contains(ts.TransferredToStudentID));
				if (transferredStudents.Any(ts => ts.TransferSemesterID > transferSemesterID))
					return new TransferStudentResult(TransferStudentResult.Statuses.ErrorStudentCannotTransferToPreviousSemesters);

				if (student.InstituteID != toAdmissionOpenProgram.InstituteID)
				{
					if (student.StatusEnum != Model.Entities.Student.Statuses.TransferredToOtherCampus)
						return new TransferStudentResult(TransferStudentResult.Statuses.ErrorStudentStatusMustBeTransferredToOtherCampus);
				}
				else
				{
					if (student.StatusEnum != Model.Entities.Student.Statuses.ProgramChanged)
						return new TransferStudentResult(TransferStudentResult.Statuses.ErrorStudentStatusMustBeProgramChanged);
				}

				var (newEnrollment, existingStudentID, alreadyGenerated) = aspireContext.GetNewEnrollment(studentID, toAdmissionOpenProgramID, toShiftEnum, loginSessionGuid);
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, toAdmissionOpenProgram.InstituteID, toAdmissionOpenProgram.DepartmentID, toAdmissionOpenProgram.ProgramID, toAdmissionOpenProgram.AdmissionOpenProgramID);
				(string Enrollment, int StudentID) newStudent;

				if (alreadyGenerated == false)
				{
					var newStatus = student.InstituteID != toAdmissionOpenProgram.InstituteID
						? Model.Entities.Student.Statuses.TransferredToOtherCampus
						: Model.Entities.Student.Statuses.ProgramChanged;
					newStudent = aspireContext.AddStudent(studentID, newStatus, toAdmissionOpenProgram.AdmissionOpenProgramID, toShiftEnum, loginHistory.UserLoginHistoryID);
				}
				else
					newStudent = (newEnrollment, existingStudentID ?? throw new InvalidOperationException());

				aspireContext.TransferredStudents.Add(new TransferredStudent
				{
					TransferredFromStudentID = studentID,
					TransferredToStudentID = newStudent.StudentID,
					TransferDate = DateTime.Now,
					TransferSemesterID = transferSemesterID,
					Remarks = remarks.TrimAndMakeItNullIfEmpty()
				});
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);

				if (alreadyGenerated == false)
				{
					var status = aspireContext.UpdateStudentPictureFromOtherStudentProfile(newStudent.StudentID, student.StudentID, loginSessionGuid);
					switch (status)
					{
						case StudentPictures.UpdateStudentPictureFromOtherStudentProfileStatuses.PictureNotFound:
						case StudentPictures.UpdateStudentPictureFromOtherStudentProfileStatuses.PictureIsInvalid:
						case StudentPictures.UpdateStudentPictureFromOtherStudentProfileStatuses.Success:
							break;
						default:
							throw new NotImplementedEnumException(status);
					}
				}

				return new TransferStudentResult(TransferStudentResult.Statuses.Success)
				{
					GeneratedEnrollment = newStudent.Enrollment
				};
			}
		}

		public sealed class TransferredStudentDetails
		{
			internal TransferredStudentDetails() { }
			public int TransferredStudentID { get; internal set; }
			public short TransferSemesterID { get; internal set; }
			public string Enrollment { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string Name { get; internal set; }
			public string FromInstituteAlias { get; internal set; }
			public short FromIntakeSemesterID { get; internal set; }
			public string ToInstituteAlias { get; internal set; }
			public string ToProgramShortName { get; internal set; }
			public string FromProgramShortName { get; internal set; }
			public short ToIntakeSemesterID { get; internal set; }
			public List<TransferredStudentCourseMapping> TransferredStudentCourseMappings { get; set; }
			public List<RegisteredCourse> RegisteredCourses { get; set; }
			public List<RoadmapCourse> RoadmapCourses { get; set; }

			public sealed class RegisteredCourse
			{
				internal RegisteredCourse() { }
				public int RegisteredCourseID { get; internal set; }
				public short OfferedSemesterID { get; internal set; }
				public string InstituteAlias { get; internal set; }
				public string ProgramAlias { get; internal set; }
				public string CourseCode { get; internal set; }
				public string Title { get; internal set; }
				public decimal CreditHours { get; internal set; }
			}

			public sealed class RoadmapCourse
			{
				internal RoadmapCourse() { }
				public int CourseID { get; internal set; }
				public string CourseCode { get; internal set; }
				public string Title { get; internal set; }
				public string Majors { get; internal set; }
				public decimal CreditHours { get; internal set; }
			}
		}

		public static TransferredStudentDetails GetTransferredStudentDetails(int transferredStudentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var transferredStudent = aspireContext.GetTransferredStudentsQuery()
					.Where(ts => ts.TransferredStudentID == transferredStudentID)
					.Select(ts => new
					{
						ts.TransferredStudentID,
						ts.TransferSemesterID,
						ts.TransferredToStudentID,
						ts.ToEnrollment,
						ts.ToRegistrationNo,
						ts.ToName,
						ts.ToAdmissionOpenProgramID,
						ts.ToDepartmentID,
						ts.ToProgramID,
						ts.ToInstituteID,
						ts.FromInstituteAlias,
						ts.FromProgramShortName,
						ts.FromIntakeSemesterID,
						ts.ToInstituteAlias,
						ts.ToProgramShortName,
						ts.ToIntakeSemesterID,
					}).SingleOrDefault();
				if (transferredStudent == null)
					return null;

				aspireContext.DemandModulePermissions(loginSessionGuid, transferredStudent.ToInstituteID, transferredStudent.ToDepartmentID, transferredStudent.ToProgramID, transferredStudent.ToAdmissionOpenProgramID);

				var studentIDs = aspireContext.GetTransferChain(new List<int> { transferredStudent.TransferredToStudentID });

				return new TransferredStudentDetails
				{
					TransferredStudentID = transferredStudent.TransferredStudentID,
					TransferSemesterID = transferredStudent.TransferSemesterID,
					Enrollment = transferredStudent.ToEnrollment,
					RegistrationNo = transferredStudent.ToRegistrationNo,
					Name = transferredStudent.ToName,
					FromInstituteAlias = transferredStudent.FromInstituteAlias,
					FromProgramShortName = transferredStudent.FromProgramShortName,
					FromIntakeSemesterID = transferredStudent.FromIntakeSemesterID,
					ToInstituteAlias = transferredStudent.ToInstituteAlias,
					ToProgramShortName = transferredStudent.ToProgramShortName,
					ToIntakeSemesterID = transferredStudent.ToIntakeSemesterID,
					TransferredStudentCourseMappings = aspireContext.TransferredStudents
						.Where(ts => studentIDs.Contains(ts.TransferredFromStudentID) || studentIDs.Contains(ts.TransferredToStudentID))
						.SelectMany(ts => ts.TransferredStudentCourseMappings).ToList(),
					RegisteredCourses = aspireContext.RegisteredCourses
						.Where(rc => rc.DeletedDate == null)
						.Where(rc => studentIDs.Contains(rc.StudentID))
						.Select(rc => new TransferredStudentDetails.RegisteredCourse
						{
							RegisteredCourseID = rc.RegisteredCourseID,
							InstituteAlias = rc.Cours.AdmissionOpenProgram.Program.Institute.InstituteAlias,
							ProgramAlias = rc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
							OfferedSemesterID = rc.OfferedCours.SemesterID,
							CourseCode = rc.Cours.CourseCode,
							Title = rc.Cours.Title,
							CreditHours = rc.Cours.CreditHours
						}).ToList(),
					RoadmapCourses = aspireContext.Courses
						.Where(c => c.AdmissionOpenProgramID == transferredStudent.ToAdmissionOpenProgramID)
						.Select(c => new TransferredStudentDetails.RoadmapCourse
						{
							CourseID = c.CourseID,
							CourseCode = c.CourseCode,
							Title = c.Title,
							CreditHours = c.CreditHours,
							Majors = c.ProgramMajor.Majors
						}).ToList()
				};
			}
		}



		public static bool UpdateCourseMappings(int transferredStudentID, Dictionary<int, (int CourseID, TransferredStudentCourseMapping.Statuses StatusEnum)?> courseMappings, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var transferredStudent = aspireContext.GetTransferredStudentsQuery()
					.Where(ts => ts.TransferredStudentID == transferredStudentID)
					.Select(ts => new
					{
						ts.TransferredStudentID,
						ts.TransferSemesterID,
						ts.TransferredToStudentID,
						ts.ToEnrollment,
						ts.ToRegistrationNo,
						ts.ToName,
						ts.ToAdmissionOpenProgramID,
						ts.ToDepartmentID,
						ts.ToProgramID,
						ts.ToInstituteID,
						ts.FromInstituteAlias,
						ts.FromProgramShortName,
						ts.FromIntakeSemesterID,
						ts.ToInstituteAlias,
						ts.ToProgramShortName,
						ts.ToIntakeSemesterID,
						TransferredStudentCourseMappings = aspireContext.TransferredStudentCourseMappings.Where(tscm => tscm.TransferredStudentID == ts.TransferredStudentID).ToList()
					}).SingleOrDefault();
				if (transferredStudent == null)
					return false;

				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, transferredStudent.ToInstituteID, transferredStudent.ToDepartmentID, transferredStudent.ToProgramID, transferredStudent.ToAdmissionOpenProgramID);

				var mappingsToBeDeleted = transferredStudent.TransferredStudentCourseMappings.Where(tscm => !courseMappings.ContainsKey(tscm.FromRegisteredCourseID));
				aspireContext.TransferredStudentCourseMappings.RemoveRange(mappingsToBeDeleted);

				foreach (var courseMapping in courseMappings)
				{
					var transferredStudentCourseMapping = transferredStudent.TransferredStudentCourseMappings.SingleOrDefault(tscm => tscm.FromRegisteredCourseID == courseMapping.Key);
					if (transferredStudentCourseMapping != null)
					{
						if (courseMapping.Value != null)
						{
							transferredStudentCourseMapping.ToCourseID = courseMapping.Value.Value.CourseID;
							transferredStudentCourseMapping.StatusEnum = courseMapping.Value.Value.StatusEnum;
						}
						else
							aspireContext.TransferredStudentCourseMappings.Remove(transferredStudentCourseMapping);
					}
					else
					{
						if (courseMapping.Value != null)
							aspireContext.TransferredStudentCourseMappings.Add(new TransferredStudentCourseMapping
							{
								TransferredStudentID = transferredStudent.TransferredStudentID,
								FromRegisteredCourseID = courseMapping.Key,
								ToCourseID = courseMapping.Value.Value.CourseID,
								StatusEnum = courseMapping.Value.Value.StatusEnum
							});
					}
				}

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return true;
			}
		}
	}
}
