﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Admissions.Staff
{
	public static class AdmissionProcessingFee
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.ManageAdmissionProcessingFee, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class GetBankBranchesResult
		{
			public sealed class BankBranch
			{
				internal BankBranch()
				{
				}

				public string BranchName { get; internal set; }
				public short BranchCodeNumber { get; internal set; }
				public string BranchCode => this.BranchCodeNumber.ToString().PadLeft(4, '0');
				public string BankName { get; internal set; }
				public string BankAlias { get; internal set; }
				public string BranchAddress { get; internal set; }
			}

			internal GetBankBranchesResult()
			{
			}

			public int VirtualItemCount { get; internal set; }
			public List<BankBranch> BankBranches { get; internal set; }
		}

		public static GetBankBranchesResult GetBankBranches(int? bankID, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var bankBranches = aspireContext.InstituteBankAccounts.Where(iba => iba.InstituteID == loginHistory.InstituteID && iba.FeeType == InstituteBankAccount.FeeTypeAdmissionProcessingFee)
					.Select(iba => iba.BankBranch.Bank).SelectMany(b => b.BankBranches).AsQueryable();
				if (bankID != null)
					bankBranches = bankBranches.Where(b => b.BankID == bankID);
				if (!string.IsNullOrWhiteSpace(searchText))
				{
					var branchCode = searchText.TryToShort();
					bankBranches = bankBranches.Where(b => b.BranchName.Contains(searchText) || b.BranchAddress.Contains(searchText) || branchCode == null || b.BranchCode == branchCode);
				}
				var virtualItemCount = bankBranches.Count();
				switch (sortExpression)
				{
					case nameof(GetBankBranchesResult.BankBranch.BranchCode):
						bankBranches = bankBranches.OrderBy(sortDirection, b => b.BranchCode);
						break;
					case nameof(GetBankBranchesResult.BankBranch.BranchName):
						bankBranches = bankBranches.OrderBy(sortDirection, b => b.BranchName);
						break;
					case nameof(GetBankBranchesResult.BankBranch.BranchAddress):
						bankBranches = bankBranches.OrderBy(sortDirection, b => b.BranchAddress);
						break;
					case nameof(GetBankBranchesResult.BankBranch.BankAlias):
						bankBranches = bankBranches.OrderBy(sortDirection, b => b.Bank.BankAlias);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return new GetBankBranchesResult
				{
					VirtualItemCount = virtualItemCount,
					BankBranches = bankBranches.Skip(pageIndex * pageSize).Take(pageSize).Select(b => new GetBankBranchesResult.BankBranch
					{
						BranchCodeNumber = b.BranchCode,
						BranchName = b.BranchName,
						BankName = b.Bank.BankName,
						BankAlias = b.Bank.BankAlias,
						BranchAddress = b.BranchAddress,
					}).ToList(),
				};
			}
		}

		public static List<CustomListItem> GetBanksForAdmissionProcessingFeeList(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.InstituteBankAccounts.Where(iba => iba.InstituteID == loginHistory.InstituteID && iba.FeeType == InstituteBankAccount.FeeTypeAdmissionProcessingFee)
					.Select(iba => new CustomListItem
					{
						ID = iba.BankBranch.Bank.BankID,
						Text = iba.BankBranch.Bank.BankAlias,
					}).OrderBy(i => i.Text).ToList();
			}
		}

		public sealed class CandidateAppliedProgram
		{
			internal CandidateAppliedProgram() { }
			public int CandidateID { get; internal set; }
			public int CandidateAppliedProgramID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Name { get; internal set; }
			public string FatherName { get; internal set; }
			public string Mobile { get; internal set; }
			public byte Category { get; internal set; }
			public Categories CategoryEnum => (Categories)this.Category;
			public short Amount { get; internal set; }
			public DateTime ApplyDate { get; internal set; }
			public int? ApplicationNo { get; internal set; }
			public int? BankID { get; internal set; }
			public int? InstituteBankAccountID { get; internal set; }
			public int? DepositBankBranchID { get; internal set; }
			public byte? DepositPlace { get; internal set; }
			public DepositPlaces? DepositPlaceEnum => (DepositPlaces?)this.DepositPlace;
			public DateTime? DepositDate { get; internal set; }
			public string Program { get; internal set; }
			public string Institute { get; internal set; }
			public string Phone { get; internal set; }
			public DateTime? EnrollmentGenerated { get; internal set; }
			public bool FeeChallanGenerated { get; internal set; }
			public string InstituteCode { get; internal set; }
			public string FullChallanNo => AspireFormats.GetFullChallanNo(this.InstituteCode, InstituteBankAccount.FeeTypes.AdmissionProcessingFee, this.CandidateAppliedProgramID);
		}

		public static CandidateAppliedProgram GetCandidateAppliedProgramForAdmissionProcessingFee(decimal fullChallanNo, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				int? candidateAppliedProgramID;
				var challanNo = AspireFormats.DecodeFullChallanNo(fullChallanNo, false);
				if (challanNo != null)
				{
					if (challanNo.Item2 != InstituteBankAccount.FeeTypes.AdmissionProcessingFee || challanNo.Item1.ToInt() != loginHistory.InstituteID)
						return null;
					candidateAppliedProgramID = challanNo.Item3;
				}
				else
				{
					candidateAppliedProgramID = fullChallanNo.ToString(CultureInfo.InvariantCulture).TryToInt();
					if (candidateAppliedProgramID == null)
						return null;
				}

				return (from cap in aspireContext.CandidateAppliedPrograms
						join aop in aspireContext.AdmissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
						where cap.CandidateAppliedProgramID == candidateAppliedProgramID && aop.Program.InstituteID == loginHistory.InstituteID
						select new CandidateAppliedProgram
						{
							CandidateID = cap.CandidateID,
							CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
							InstituteCode = aop.Program.Institute.InstituteCode,
							SemesterID = aop.SemesterID,
							Name = cap.Candidate.Name,
							FatherName = cap.Candidate.FatherName,
							Mobile = cap.Candidate.Mobile,
							Phone = cap.Candidate.Phone,
							Category = cap.Candidate.Category.Value,
							ApplicationNo = cap.ApplicationNo,
							Amount = cap.Amount,
							BankID = cap.BankBranch.Bank.BankID,
							InstituteBankAccountID = cap.InstituteBankAccountID,
							Program = aop.Program.ProgramAlias,
							Institute = aop.Program.Institute.InstituteShortName,
							DepositBankBranchID = cap.DepositBankBranchID,
							ApplyDate = cap.ApplyDate,
							DepositPlace = cap.DepositPlace,
							DepositDate = cap.DepositDate,
							FeeChallanGenerated = cap.StudentFees.Any(),
							EnrollmentGenerated = cap.EnrollmentGenerated,
						}).SingleOrDefault();
			}
		}

		internal static int GetNextApplicationNo(this AspireContext aspireContext, int candidateAppliedProgramID)
		{
			var candidateAppliedProgram = aspireContext.CandidateAppliedPrograms
				.Where(cap => cap.CandidateAppliedProgramID == candidateAppliedProgramID)
				.Select(cap => new
				{
					cap.AdmissionOpenProgram1.Program.InstituteID,
					cap.AdmissionOpenProgram1.SemesterID,
				})
				.Single();

			var maxApplicationNo = aspireContext.CandidateAppliedPrograms
				.Where(cap => cap.AdmissionOpenProgram1.SemesterID == candidateAppliedProgram.SemesterID && cap.ApplicationNo != null)
				.Where(cap => cap.AdmissionOpenProgram1.Program.InstituteID == candidateAppliedProgram.InstituteID)
				.Max(cap => cap.ApplicationNo) ?? 0;

			return maxApplicationNo + 1;
		}

		public enum UpdateAdmissionProcessingFeeStatusResults
		{
			Success,
			NoRecordFound,
			CannotUpdateAdmissionProcessingFee,
			CannotBeUnpaidEnrollmentHasBeenGenerated,
			CannotBeUnpaidFeeChallanHasBeenGenerated,
			DepositDateMustBeGreaterThanOrEqualToApplyDate,
			InvalidBranchCode,
			AlreadyPaid
		}

		private static UpdateAdmissionProcessingFeeStatusResults UpdateAdmissionProcessingFeeStatus(this AspireContext aspireContext, AdmissionProcessingFeeChallan admissionProcessingFeeChallan, bool import, Guid loginSessionGuid)
		{
			aspireContext.DemandTransaction();
			var candidateAppliedProgramID = admissionProcessingFeeChallan.CandidateAppliedProgramID;
			var fee = (from cap in aspireContext.CandidateAppliedPrograms
					   join aop in aspireContext.AdmissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
					   where cap.CandidateAppliedProgramID == candidateAppliedProgramID
					   select new
					   {
						   cap,
						   aop.Program.InstituteID,
						   aop.Program.DepartmentID,
						   aop.ProgramID,
						   aop.AdmissionOpenProgramID,
						   aop.Program.Institute.InstituteCode,
					   }).SingleOrDefault();

			if (fee == null || fee.InstituteCode.ToByte() != admissionProcessingFeeChallan.InstituteCode.ToByte())
				return UpdateAdmissionProcessingFeeStatusResults.NoRecordFound;
			var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, fee.InstituteID, fee.DepartmentID, fee.ProgramID, fee.AdmissionOpenProgramID);
			var candidateAppliedProgram = fee.cap;

			if (!(candidateAppliedProgram.FeeUpdatedByUserTypeEnum == null || candidateAppliedProgram.FeeUpdatedByUserTypeEnum == FeeUpdatedByUserTypes.Staff))
				return UpdateAdmissionProcessingFeeStatusResults.CannotUpdateAdmissionProcessingFee;

			if (admissionProcessingFeeChallan.Paid == false)
			{
				if (import)
					throw new InvalidOperationException();
				if (candidateAppliedProgram.DepositDate != null)
				{
					if (candidateAppliedProgram.EnrollmentGenerated != null)
						return UpdateAdmissionProcessingFeeStatusResults.CannotBeUnpaidEnrollmentHasBeenGenerated;
					if (aspireContext.StudentFees.Any(sf => sf.CandidateAppliedProgramID == candidateAppliedProgram.CandidateAppliedProgramID))
						return UpdateAdmissionProcessingFeeStatusResults.CannotBeUnpaidFeeChallanHasBeenGenerated;
				}
				if (admissionProcessingFeeChallan.Amount != null && admissionProcessingFeeChallan.Amount != candidateAppliedProgram.Amount)
					candidateAppliedProgram.Amount = admissionProcessingFeeChallan.Amount.Value;
				candidateAppliedProgram.CBTSessionLabID = null;
				candidateAppliedProgram.FeeUpdatedByUserTypeEnum = null;
				candidateAppliedProgram.DepositDate = null;
				candidateAppliedProgram.DepositPlace = null;
				candidateAppliedProgram.DepositBankBranchID = null;
				candidateAppliedProgram.InstituteBankAccountID = null;
			}
			else
			{
				if (import && candidateAppliedProgram.DepositDate != null)
					return UpdateAdmissionProcessingFeeStatusResults.AlreadyPaid;
				if (admissionProcessingFeeChallan.DepositDate == null)
					throw new InvalidOperationException();
				if (admissionProcessingFeeChallan.DepositDate.Value < candidateAppliedProgram.ApplyDate.Date)
					return UpdateAdmissionProcessingFeeStatusResults.DepositDateMustBeGreaterThanOrEqualToApplyDate;
				if (admissionProcessingFeeChallan.Amount != null && import)
					throw new InvalidOperationException("Amount cannot be changed through import.");

				switch (admissionProcessingFeeChallan.DepositPlaceEnum)
				{
					case null:
						throw new ArgumentNullException();
					case DepositPlaces.Bank:
						if (admissionProcessingFeeChallan.DepositBankBranchID == null && admissionProcessingFeeChallan.BranchCode == null)
							throw new InvalidOperationException("DepositBankBranchID cannot be null when deposit place is bank.");
						if (admissionProcessingFeeChallan.InstituteBankAccountID == null)
							throw new InvalidOperationException("InstituteBankAccountID cannot be null when deposit place is bank.");
						int? depositBankBranchID;
						if (admissionProcessingFeeChallan.DepositBankBranchID != null)
							depositBankBranchID = admissionProcessingFeeChallan.DepositBankBranchID.Value;
						else
						{
							depositBankBranchID = aspireContext.InstituteBankAccounts
								.Where(iba => iba.InstituteBankAccountID == admissionProcessingFeeChallan.InstituteBankAccountID.Value)
								.SelectMany(iba => iba.BankBranch.Bank.BankBranches)
								.Where(bb => bb.BranchCode == admissionProcessingFeeChallan.BranchCode).Select(bb => (int?)bb.BankBranchID).SingleOrDefault();
							if (depositBankBranchID == null)
								return UpdateAdmissionProcessingFeeStatusResults.InvalidBranchCode;
						}
						if (admissionProcessingFeeChallan.Amount != null && admissionProcessingFeeChallan.Amount != candidateAppliedProgram.Amount)
							candidateAppliedProgram.Amount = admissionProcessingFeeChallan.Amount.Value;
						candidateAppliedProgram.DepositBankBranchID = depositBankBranchID.Value;
						candidateAppliedProgram.DepositDate = admissionProcessingFeeChallan.DepositDate.Value;
						candidateAppliedProgram.FeeUpdatedByUserTypeEnum = admissionProcessingFeeChallan.FeeUpdatedByUserTypeEnum;
						candidateAppliedProgram.InstituteBankAccountID = admissionProcessingFeeChallan.InstituteBankAccountID.Value;
						candidateAppliedProgram.DepositPlaceEnum = admissionProcessingFeeChallan.DepositPlaceEnum.Value;
						break;
					case DepositPlaces.Campus:
						if (import)
							throw new InvalidOperationException("Import cannot be performed for Campus.");
						if (admissionProcessingFeeChallan.DepositBankBranchID != null)
							throw new InvalidOperationException("DepositBankBranchID must be null when deposit place is campus.");
						if (admissionProcessingFeeChallan.InstituteBankAccountID == null)
							throw new InvalidOperationException("InstituteBankAccountID cannot be null when deposit place is campus.");
						if (admissionProcessingFeeChallan.Amount != null && admissionProcessingFeeChallan.Amount != candidateAppliedProgram.Amount)
							candidateAppliedProgram.Amount = admissionProcessingFeeChallan.Amount.Value;
						candidateAppliedProgram.DepositBankBranchID = null;
						candidateAppliedProgram.DepositDate = admissionProcessingFeeChallan.DepositDate.Value;
						candidateAppliedProgram.FeeUpdatedByUserTypeEnum = admissionProcessingFeeChallan.FeeUpdatedByUserTypeEnum;
						candidateAppliedProgram.InstituteBankAccountID = admissionProcessingFeeChallan.InstituteBankAccountID.Value;
						candidateAppliedProgram.DepositPlaceEnum = admissionProcessingFeeChallan.DepositPlaceEnum.Value;
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(admissionProcessingFeeChallan.DepositPlaceEnum), admissionProcessingFeeChallan.DepositPlaceEnum, null);
				}

				if (candidateAppliedProgram.ApplicationNo == null)
					candidateAppliedProgram.ApplicationNo = aspireContext.GetNextApplicationNo(candidateAppliedProgram.CandidateAppliedProgramID);
			}
			aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
			return UpdateAdmissionProcessingFeeStatusResults.Success;
		}

		public static UpdateAdmissionProcessingFeeStatusResults UpdateAdmissionProcessingFeeStatus(decimal fullChallanNo, short amount, DateTime? depositDate, DepositPlaces? depositPlaceEnum, int? depositBankBranchID, int? instituteBankAccountID, FeeUpdatedByUserTypes feeUpdatedByUserTypeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var result = aspireContext.UpdateAdmissionProcessingFeeStatus(new AdmissionProcessingFeeChallan
				{
					DepositDate = depositDate,
					InstituteBankAccountID = instituteBankAccountID,
					DepositBankBranchID = depositBankBranchID,
					BranchCode = null,
					DepositPlaceEnum = depositPlaceEnum,
					FeeUpdatedByUserTypeEnum = feeUpdatedByUserTypeEnum,
					Paid = depositDate != null,
					FullChallanNo = fullChallanNo,
					Amount = amount,
				}, false, loginSessionGuid);

				switch (result)
				{
					case UpdateAdmissionProcessingFeeStatusResults.Success:
						aspireContext.CommitTransaction();
						break;
					case UpdateAdmissionProcessingFeeStatusResults.NoRecordFound:
					case UpdateAdmissionProcessingFeeStatusResults.CannotUpdateAdmissionProcessingFee:
					case UpdateAdmissionProcessingFeeStatusResults.CannotBeUnpaidEnrollmentHasBeenGenerated:
					case UpdateAdmissionProcessingFeeStatusResults.CannotBeUnpaidFeeChallanHasBeenGenerated:
					case UpdateAdmissionProcessingFeeStatusResults.InvalidBranchCode:
					case UpdateAdmissionProcessingFeeStatusResults.DepositDateMustBeGreaterThanOrEqualToApplyDate:
					case UpdateAdmissionProcessingFeeStatusResults.AlreadyPaid:
						aspireContext.RollbackTransaction();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				return result;
			}
		}

		public class CandidateAppliedProgramForImportFee
		{
			internal CandidateAppliedProgramForImportFee() { }
			public int CandidateAppliedProgramID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Name { get; internal set; }
			public string FatherName { get; internal set; }
			public short Amount { get; internal set; }
			public DateTime? DepositDate { get; internal set; }
			public string Program { get; internal set; }
			public string Institute { get; internal set; }
			public string BankAlias { get; internal set; }
			public short? BranchCode { get; internal set; }
			internal string InstituteCode { get; set; }
			public string FullChallanNo => AspireFormats.GetFullChallanNo(this.InstituteCode, InstituteBankAccount.FeeTypes.AdmissionProcessingFee, this.CandidateAppliedProgramID);
		}

		public static List<CandidateAppliedProgramForImportFee> GetCandidateAppliedProgramsForImportFees(List<decimal> fullChallanNos, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var instituteCode = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteCode).Single().PadLeft(2, '0');
				var candidateAppliedProgramIDs = fullChallanNos.Select(c => AspireFormats.DecodeFullChallanNo(c, false)).Where(c => c != null && c.Item1 == instituteCode && c.Item2 == InstituteBankAccount.FeeTypes.AdmissionProcessingFee)
					.Select(c => c.Item3).ToList();

				return (from cap in aspireContext.CandidateAppliedPrograms
						join aop in aspireContext.AdmissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
						where candidateAppliedProgramIDs.Contains(cap.CandidateAppliedProgramID) && aop.Program.InstituteID == loginHistory.InstituteID
						select new CandidateAppliedProgramForImportFee
						{
							CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
							SemesterID = aop.SemesterID,
							Name = cap.Candidate.Name,
							FatherName = cap.Candidate.FatherName,
							Amount = cap.Amount,
							Program = aop.Program.ProgramAlias,
							Institute = aop.Program.Institute.InstituteAlias,
							InstituteCode = aop.Program.Institute.InstituteCode,
							DepositDate = cap.DepositDate,
							BankAlias = cap.BankBranch.Bank.BankAlias,
							BranchCode = cap.BankBranch.BranchCode,
						}).ToList();
			}
		}

		public sealed class AdmissionProcessingFeeChallan
		{
			public DepositPlaces? DepositPlaceEnum { get; set; }
			public decimal FullChallanNo { get; set; }
			public int? CandidateAppliedProgramID => AspireFormats.DecodeFullChallanNo(this.FullChallanNo)?.Item3;
			public string InstituteCode => AspireFormats.DecodeFullChallanNo(this.FullChallanNo)?.Item1;
			public DateTime? DepositDate { get; set; }
			public short? BranchCode { get; set; }
			public int? DepositBankBranchID { get; set; }
			public int? InstituteBankAccountID { get; set; }
			public bool Paid { get; set; }
			public FeeUpdatedByUserTypes FeeUpdatedByUserTypeEnum { get; set; }
			public short? Amount { get; internal set; }
		}

		public static Dictionary<AdmissionProcessingFeeChallan, UpdateAdmissionProcessingFeeStatusResults> UpdateAdmissionProcessingFeeStatus(List<AdmissionProcessingFeeChallan> admissionProcessingFeeChallans, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var results = new Dictionary<AdmissionProcessingFeeChallan, UpdateAdmissionProcessingFeeStatusResults>();
				foreach (var admissionProcessingFeeChallan in admissionProcessingFeeChallans)
				{
					var result = aspireContext.UpdateAdmissionProcessingFeeStatus(admissionProcessingFeeChallan, true, loginSessionGuid);
					results.Add(admissionProcessingFeeChallan, result);
				}
				aspireContext.CommitTransaction();
				return results;
			}
		}

		public static List<CustomListItem> GetBankBranchesList(int instituteBankAccountID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var branches = aspireContext.InstituteBankAccounts
					.Where(iba => iba.InstituteBankAccountID == instituteBankAccountID && iba.InstituteID == loginHistory.InstituteID)
					.SelectMany(s => s.BankBranch.Bank.BankBranches).ToList();
				return branches.Select(b => new CustomListItem
				{
					ID = b.BankBranchID,
					Text = $"{b.BranchCode.ToString().PadLeft(4, '0')} - {b.BranchName}"
				}).ToList();
			}
		}
	}
}