﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.Admissions.Staff
{
	public static class ETSScorers
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.ManageETSScore, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.PermissionValues.Special);
		}

		public sealed class ETSInformation
		{
			internal ETSInformation() { }
			public int CandidateAppliedProgramID { get; internal set; }
			public short SemesterID { get; internal set; }
			public int? ApplicationNo { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public byte? ETSType { get; internal set; }
			public ETSTypes? ETSTypeEnum => (ETSTypes?)this.ETSType;
			public double? ETSObtained { get; internal set; }
			public double? ETSTotal { get; internal set; }
			public DateTime? EnrollmentGenerated { get; internal set; }
		}

		public static ETSInformation GetCandidateETSScore(short semesterID, int applicationNo, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return (from cap in aspireContext.CandidateAppliedPrograms
						join aop in aspireContext.AdmissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
						where aop.Program.InstituteID == loginHistory.InstituteID && aop.SemesterID == semesterID
							  && cap.ApplicationNo == applicationNo && cap.AccountTransactionID != null
						select new ETSInformation
						{
							CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
							SemesterID = cap.Candidate.SemesterID,
							ApplicationNo = cap.ApplicationNo,
							Name = cap.Candidate.Name,
							ProgramAlias = aop.Program.ProgramAlias,
							ETSType = cap.ETSType,
							ETSObtained = cap.ETSObtained,
							ETSTotal = cap.ETSTotal,
							EnrollmentGenerated = cap.EnrollmentGenerated,
						}).SingleOrDefault();
			}
		}

		public enum UpdateETSScoreStatuses
		{
			NoRecordFound,
			EnrollmentGenerated,
			Success,
			EntryTestMarksEntered,
			EntryTestScoreExists,
			EntryTestEndDatePassed
		}

		public static UpdateETSScoreStatuses UpdateETSScore(int candidateAppliedProgramID, ETSTypes? etsTypeEnum, double? etsObtained, double? etsTotal, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var candidateAppliedProgram = (from cap in aspireContext.CandidateAppliedPrograms
											   join aop in aspireContext.AdmissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
											   where aop.Program.InstituteID == loginHistory.InstituteID && cap.CandidateAppliedProgramID == candidateAppliedProgramID
											   select cap).SingleOrDefault();

				if (candidateAppliedProgram == null)
					return UpdateETSScoreStatuses.NoRecordFound;
				if (candidateAppliedProgram.EntryTestMarksPercentage != null)
					return UpdateETSScoreStatuses.EntryTestMarksEntered;
				if (candidateAppliedProgram.EnrollmentGenerated != null)
					return UpdateETSScoreStatuses.EnrollmentGenerated;

				if (loginHistory.PermissionValueEnum == UserGroupPermission.PermissionValues.Allowed)
				{
					var admissionOpenProgram = aspireContext.AdmissionOpenPrograms
						.Where(aop => aop.AdmissionOpenProgramID == candidateAppliedProgram.Choice1AdmissionOpenProgramID)
						.Select(aop => new
						{
							aop.EntryTestEndDate,
							EntryTestScoreExists = aop.CandidateAppliedPrograms1.Any(cap => cap.EntryTestMarksPercentage != null),
							ETSScoreExists = aop.CandidateAppliedPrograms1.Any(cap => cap.ETSObtained != null),
						}).Single();

					if (admissionOpenProgram.EntryTestScoreExists)
						return UpdateETSScoreStatuses.EntryTestScoreExists;
					if (admissionOpenProgram.ETSScoreExists)
					{
						if (admissionOpenProgram.EntryTestEndDate < DateTime.Today)
							return UpdateETSScoreStatuses.EntryTestEndDatePassed;
					}
				}

				candidateAppliedProgram.ETSTypeEnum = etsTypeEnum;
				candidateAppliedProgram.ETSObtained = etsObtained;
				candidateAppliedProgram.ETSTotal = etsTotal;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateETSScoreStatuses.Success;
			}
		}
	}
}
