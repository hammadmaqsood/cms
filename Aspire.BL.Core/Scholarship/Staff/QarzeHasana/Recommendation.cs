﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Scholarship.Staff.QarzeHasana
{
	public static class Recommendation
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.QarzeHasanaModule, UserGroupPermission.PermissionValues.Allowed);
		}

		private static void DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, StaffPermissions.Scholarship permissionType)
		{
			aspireContext.DemandStaffPermissions(loginSessionGuid, permissionType, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class IndividualRecommendation
		{
			internal IndividualRecommendation() { }
			public int ScholarshipApplicationID { get; internal set; }
			public short AnnouncementSemesterID { get; set; }
			public short IntakeSemesterID { get; set; }
			public string Enrollment { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string Name { get; internal set; }
			public List<StudentSemester> StudentSemesters { get; internal set; }
			public StudentSemester CurrentStudentSemester => this.StudentSemesters.SingleOrDefault(ss => ss.SemesterID == this.AnnouncementSemesterID);
			public StudentSemester LastStudentSemester => this.StudentSemesters.OrderByDescending(ss => ss.SemesterID).FirstOrDefault(ss => ss.SemesterID < this.AnnouncementSemesterID);
			public bool IsFirstSemester => this.IntakeSemesterID == this.AnnouncementSemesterID;
			public int? CampusRecommendation { get; internal set; }
			public string CampusRemarks { get; internal set; }
			public int? CSCRecommendation { get; internal set; }
			public string CSCRemarks { get; internal set; }
			public int? FinalRecommendation { get; internal set; }
			public double? PreviousDegreePercentage { get; internal set; }
			public int? TuitionFee { get; internal set; }
			//public ScholarshipApplication.ApprovalStatuses ApprovalStatusEnum { get; internal set; }
			public string LastResult
			{
				get
				{
					if (this.IsFirstSemester)
						return $"{this.PreviousDegreePercentage:N2}%";
					if (this.LastStudentSemester != null)
						return $"GPA: {this.LastStudentSemester.GPA:N2}, CGPA: {this.LastStudentSemester.CGPA:N2}";
					return string.Empty.ToNAIfNullOrEmpty();
				}
			}
		}

		public static (List<IndividualRecommendation> recommendations, short semesterID) GetRecommendations(int scholarshipAnnouncementID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var recommendations = aspireContext.ScholarshipAnnouncements
					.Where(sa => sa.ScholarshipAnnouncementID == scholarshipAnnouncementID && sa.Scholarship.InstituteID == loginHistory.InstituteID)
					.SelectMany(sa => sa.ScholarshipApplications)
					.Where(sap => sap.Status == (byte)ScholarshipApplication.Statuses.Submitted)
					.Select(sap => new IndividualRecommendation
					{
						ScholarshipApplicationID = sap.ScholarshipApplicationID,
						AnnouncementSemesterID = sap.ScholarshipAnnouncement.SemesterID,
						Enrollment = sap.Student.Enrollment,
						IntakeSemesterID = sap.Student.AdmissionOpenProgram.SemesterID,
						Name = sap.Student.Name,
						ProgramAlias = sap.Student.AdmissionOpenProgram.Program.ProgramAlias,
						StudentSemesters = sap.Student.StudentSemesters.ToList(),
						PreviousDegreePercentage = sap.Student.StudentAcademicRecords
							.OrderByDescending(sar => sar.DegreeType)
							.Select(sar => (double?)sar.Percentage)
							.FirstOrDefault(),
					})
					.OrderBy(sap => sap.Enrollment)
					.ToList();
				var semesterID = aspireContext.ScholarshipAnnouncements.Where(sa => sa.ScholarshipAnnouncementID == scholarshipAnnouncementID).Select(sa => sa.SemesterID).Single();
				return (recommendations, semesterID);
			}
		}
		/*
		public sealed class Application
		{
			public int ScholarshipApplicationID { get; set; }
			public int? TuitionFee { get; set; }
			public int? CampusRecommendation { get; set; }
			public string CampusRemarks { get; set; }
			public int? CSCRecommendation { get; set; }
			public string CSCRemarks { get; set; }
			public int? FinalRecommendation { get; set; }
			public ScholarshipApplication.ApprovalStatuses ApprovalStatusEnum { get; set; }
		}
		*/
		public enum UpdateScholarshipApplicationResult
		{
			Success,
			NoRecordFound
		}

		/*
		public static UpdateScholarshipApplicationResult UpdateScholarshipApplications(int scholarshipAnnouncementID, List<Application> applications, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

				var scholarshipAnnouncement = aspireContext.ScholarshipAnnouncements
					.Where(sa => sa.ScholarshipAnnouncementID == scholarshipAnnouncementID && sa.Scholarship.InstituteID == loginHistory.InstituteID)
					.Select(sa => new { sa.ScholarshipAnnouncementID }).SingleOrDefault();
				if (scholarshipAnnouncement == null)
					return UpdateScholarshipApplicationResult.NoRecordFound;

				foreach (var application in applications)
				{
					var scholarshipApplication = aspireContext.ScholarshipApplications
						.Where(sapp => sapp.ScholarshipAnnouncementID == scholarshipAnnouncement.ScholarshipAnnouncementID)
						.Where(sapp => sapp.Status == (byte)ScholarshipApplication.Statuses.Submitted)
						.SingleOrDefault(sapp => sapp.ScholarshipApplicationID == application.ScholarshipApplicationID);
					if (scholarshipApplication == null)
						return UpdateScholarshipApplicationResult.NoRecordFound;
					/*
					if (scholarshipApplication.TuitionFee != application.TuitionFee
						|| scholarshipApplication.CampusRecommendation != application.CampusRecommendation
						|| scholarshipApplication.CampusRemarks.ToNullIfWhiteSpace() != application.CampusRemarks.ToNullIfWhiteSpace())
					{
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.Scholarship.CanManageQarzeHasanaCampusRecommendations);
						scholarshipApplication.TuitionFee = application.TuitionFee;
						scholarshipApplication.CampusRecommendation = application.CampusRecommendation;
						scholarshipApplication.CampusRemarks = application.CampusRemarks;
					}

					if (scholarshipApplication.CSCRecommendation != application.CSCRecommendation
						|| scholarshipApplication.CSCRemarks.ToNullIfWhiteSpace() != application.CSCRemarks.ToNullIfWhiteSpace())
					{
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.Scholarship.CanManageQarzeHasanaDSARecommendations);
						scholarshipApplication.CSCRecommendation = application.CSCRecommendation;
						scholarshipApplication.CSCRemarks = application.CSCRemarks;
					}

					if (scholarshipApplication.FinalRecommendation != application.FinalRecommendation
						|| scholarshipApplication.ApprovalStatusEnum != application.ApprovalStatusEnum)
					{
						aspireContext.DemandPermissions(loginSessionGuid, StaffPermissions.Scholarship.CanManageQarzeHasanaFinalRecommendations);
						scholarshipApplication.FinalRecommendation = application.FinalRecommendation;
						switch (application.ApprovalStatusEnum)
						{
							case ScholarshipApplication.ApprovalStatuses.Pending:
							case ScholarshipApplication.ApprovalStatuses.Rejected:
								scholarshipApplication.ApprovalStatusEnum = application.ApprovalStatusEnum;
								break;
							case ScholarshipApplication.ApprovalStatuses.Approved:
								if (scholarshipApplication.TuitionFee != null && scholarshipApplication.FinalRecommendation != null)
									scholarshipApplication.ApprovalStatusEnum = ScholarshipApplication.ApprovalStatuses.Approved;
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}
					}
					
	}

	aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateScholarshipApplicationResult.Success;
			}
		}
		*/
	}
}
