﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Report
{
	public static class ApplicationList
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.QarzeHasanaModule, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class PageHeader
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public ScholarshipApplicationApproval.ApprovalStatuses? ApprovalStatusEnum { get; internal set; }
			public string Title { get; internal set; }

			public static List<PageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class QarzeHasanaApplication
		{
			public int ScholarshipApplicationID { get; internal set; }
			public int ScholarshipApplicationApprovalID { get; set; }
			public short AnnouncementSemesterID { get; set; }
			public short IntakeSemesterID { get; set; }
			public string Enrollment { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string Name { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.StudentSemester?.SemesterNo, null, null);
			public int? CampusRecommendation { get; internal set; }
			public string CampusRemarks { get; internal set; }
			public int? CSCRecommendation { get; internal set; }
			public string CSCRemarks { get; internal set; }
			public int? FinalRecommendation { get; internal set; }
			public int? TuitionFee { get; internal set; }
			public ScholarshipApplicationApproval.ApprovalStatuses ApprovalStatusEnum { get; internal set; }
			public string Status => this.ApprovalStatusEnum.ToFullName();
			public StudentSemester StudentSemester { get; internal set; }
			public short ApprovalSemesterID { get; internal set; }
			public bool IsFirstSemester => this.IntakeSemesterID == this.ApprovalSemesterID;
			public double? PreviousDegreePercentage { get; internal set; }
			public string Data { get; internal set; }

			public string GetLastBUExamGPA => this.StudentSemester?.BUExamGPA?.FormatGPA();
			public string GetLastBUExamCGPA => this.StudentSemester?.BUExamCGPA?.FormatGPA();
			public string GetLastPercentage
			{
				get
				{
					if (this.IsFirstSemester && this.PreviousDegreePercentage != null)
						return $"{this.PreviousDegreePercentage:N2}";
					return null;
				}
			}

			public static List<QarzeHasanaApplication> GetList()
			{
				return null;
			}

			public string ScholarshipIntakeSemester => this.AnnouncementSemesterID.ToSemesterString();

			public int GetApprovedAmount(Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses approvalStatus)
			{
				return approvalStatus != Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Rejected
						&& this.FinalRecommendation != null && this.TuitionFee != null
					? (int)Math.Round(this.TuitionFee.Value * this.FinalRecommendation.Value / 100.0, MidpointRounding.AwayFromZero)
					: 0;
			}
			public int Amount => this.GetApprovedAmount(this.ApprovalStatusEnum);

			public string GetRemarks
			{
				get
				{
					if (this.CampusRemarks.ToNullIfWhiteSpace() == null && this.CSCRemarks.ToNullIfWhiteSpace() == null)
						return $"N/A";
					if (this.CampusRemarks.ToNullIfWhiteSpace() != null && this.CSCRemarks.ToNullIfWhiteSpace() == null)
						return $"Campus: {this.CampusRemarks}";
					if (this.CampusRemarks.ToNullIfWhiteSpace() == null && this.CSCRemarks.ToNullIfWhiteSpace() != null)
						return $"CSC: {this.CSCRemarks}";
					return $"Campus: {this.CampusRemarks},\nCSC: {CSCRemarks}";
				}
			}

			public bool SetApprovalData
			{
				get
				{
					if (this.Data == null)
						return false;

					var approvalData = BL.Core.Scholarship.Common.Schemas.ScholarshipApplicationApproval.FromJson(this.Data);
					CampusRecommendation = approvalData.CampusRecommendation;
					CampusRemarks = approvalData.CampusRemarks;
					CSCRecommendation = approvalData.CSCRecommendation;
					CSCRemarks = approvalData.CSCRemarks;
					FinalRecommendation = approvalData.FinalRecommendation;
					TuitionFee = approvalData.TuitionFee;
					return true;
				}
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public PageHeader PageHeader { get; internal set; }
			public List<QarzeHasanaApplication> Applications { get; internal set; }
			public UserGroupPermission.PermissionValues PermissionValue { get; internal set; }
		}

		private static string ListTitle(short semesterID, ScholarshipApplicationApproval.ApprovalStatuses? approvalStatusEnum, ApplicationTypes? applicationTypes)
		{
			if (approvalStatusEnum == null && applicationTypes == null)
				return $"{Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana.ToFullName()} ({semesterID.ToSemesterString()})";
			if (approvalStatusEnum != null && applicationTypes == null)
				return $"{Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana.ToFullName()} ({semesterID.ToSemesterString()}) ({approvalStatusEnum.Value.ToFullName()})";
			if (approvalStatusEnum == null && applicationTypes != null)
				return $"{Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana.ToFullName()} ({semesterID.ToSemesterString()}) ({applicationTypes})";
			return $"{Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana.ToFullName()} ({semesterID.ToSemesterString()}) ({applicationTypes}) ({approvalStatusEnum.Value.ToFullName()})";
		}

		private static string SemesterWiseListTitle(short announcementSemesterID, short approvalSemesterID, ScholarshipApplicationApproval.ApprovalStatuses? approvalStatusEnum)
		{
			if (approvalStatusEnum == null && announcementSemesterID == approvalSemesterID)
				return $"{Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana.ToFullName()} ({announcementSemesterID.ToSemesterString()}) (New) ";
			if (approvalStatusEnum == null && announcementSemesterID != approvalSemesterID)
				return $"{Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana.ToFullName()} ({approvalSemesterID.ToSemesterString()}) (Existing)";
			if (approvalStatusEnum != null && announcementSemesterID == approvalSemesterID)
				return $"{Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana.ToFullName()} ({approvalSemesterID.ToSemesterString()}) (New) ({approvalStatusEnum.Value.ToFullName()})";
			return $"{Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana.ToFullName()} ({approvalSemesterID.ToSemesterString()}) (Existing) ({approvalStatusEnum.Value.ToFullName()})";
		}
		public enum ListTypes
		{
			QarzeHasnaList,
			SemesterWiseList
		}

		public enum ApplicationTypes
		{
			New,
			Existing
		}

		public static ReportDataSet GetQarzeHasanaLists(short? announcementSemesterID, short approvalSemesterID, ScholarshipApplicationApproval.ApprovalStatuses? approvalStatusEnum, ApplicationTypes? applicationType, ListTypes listTypes, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var approvalStatus = (byte?)approvalStatusEnum;

				var applicationsQuery = aspireContext.ScholarshipApplicationApprovals
					.Where(saa => saa.ScholarshipApplication.ScholarshipAnnouncement.Scholarship.InstituteID == loginHistory.InstituteID)
					.Where(saa => saa.SemesterID == approvalSemesterID)
					.Where(saa => approvalStatus == null || saa.ApprovalStatus == approvalStatus.Value);

				string title = string.Empty;
				switch (listTypes)
				{
					case ListTypes.QarzeHasnaList:
						applicationsQuery = applicationsQuery.Where(a => applicationType == null || (applicationType == ApplicationTypes.New && a.ScholarshipApplication.ScholarshipAnnouncement.SemesterID == approvalSemesterID)
											|| (applicationType == ApplicationTypes.Existing && a.ScholarshipApplication.ScholarshipAnnouncement.SemesterID != approvalSemesterID));
						title = ListTitle(approvalSemesterID, approvalStatusEnum, applicationType);

						break;
					case ListTypes.SemesterWiseList:
						applicationsQuery = applicationsQuery.Where(a => a.ScholarshipApplication.ScholarshipAnnouncement.SemesterID == announcementSemesterID);
						title = SemesterWiseListTitle(announcementSemesterID.Value, approvalSemesterID, approvalStatusEnum);
						break;
					default:
						throw new NotImplementedEnumException(listTypes);
				}

				var applications = applicationsQuery.Select(saa => new QarzeHasanaApplication
				{
					ScholarshipApplicationID = saa.ScholarshipApplicationID,
					ScholarshipApplicationApprovalID = saa.ScholarshipApplicationApprovalID,
					Data = saa.Data,
					AnnouncementSemesterID = saa.ScholarshipApplication.ScholarshipAnnouncement.SemesterID,
					IntakeSemesterID = saa.ScholarshipApplication.Student.AdmissionOpenProgram.SemesterID,
					Enrollment = saa.ScholarshipApplication.Student.Enrollment,
					Name = saa.ScholarshipApplication.Student.Name,
					ProgramAlias = saa.ScholarshipApplication.Student.AdmissionOpenProgram.Program.ProgramAlias,
					StudentSemester = saa.ScholarshipApplication.Student.StudentSemesters.Where(ss => ss.Semester.SemesterType != (byte)SemesterTypes.Summer && ss.SemesterID < approvalSemesterID).OrderByDescending(ss => ss.SemesterID).FirstOrDefault(),
					PreviousDegreePercentage = saa.ScholarshipApplication.Student.StudentAcademicRecords
						.OrderByDescending(sar => sar.DegreeType)
						.Select(sar => (double?)sar.Percentage)
						.FirstOrDefault(),
					ApprovalStatusEnum = (ScholarshipApplicationApproval.ApprovalStatuses)saa.ApprovalStatus,
					ApprovalSemesterID = saa.SemesterID,
				})
				.OrderBy(saa => saa.AnnouncementSemesterID)
				.ThenBy(saa => saa.ProgramAlias)
				.ThenBy(tb => tb.Enrollment)
				.ToList()
				.FindAll(a => a.SetApprovalData);

				return new ReportDataSet
				{
					PageHeader = new PageHeader
					{
						Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single(),
						SemesterID = approvalSemesterID,
						ApprovalStatusEnum = approvalStatusEnum,
						Title = title,
					},
					Applications = applications
				};
			}
		}
	}
}

