﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.Scholarship.Staff.QarzeHasana
{
	public static class ViewApplications
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.QarzeHasanaModule, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class ScholarshipAnnouncement
		{
			internal ScholarshipAnnouncement() { }
			public int StudentID { get; internal set; }
			public int? ScholarshipAnnouncementID { get; internal set; }
			public int? ScholarshipApplicationID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public ScholarshipApplication.Statuses? ScholarshipApplicationStatusEnum { get; internal set; }
			public string ScholarshipApplicationStatusFullName => this.ScholarshipApplicationStatusEnum.ToFullName();
			public Model.Entities.Scholarship.ScholarshipTypes ScholarshipTypeEnum { get; internal set; }
			public string ScholarshipTypeFullName => this.ScholarshipTypeEnum.ToFullName();
			public string Scholarships => $"{this.ScholarshipTypeFullName} ({this.Semester})";
			public ScholarshipApplication.DocumentsStatuses? DocumentsStatusEnum { get; internal set; }
			public string ScolarshipIntakeSemester
			{
				get
				{
					if (this.LastScholarshipApplicationApproval != null)
						return this.SemesterID.ToSemesterString();
					return "N/A";
				}
			}
			public ScholarshipApplicationApproval LastScholarshipApplicationApproval { get; internal set; }
			public sealed class ScholarshipApplicationApproval
			{
				internal ScholarshipApplicationApproval() { }
				public short SemesterID { get; internal set; }
				public byte ApprovalStatus { get; internal set; }
			}

			public string LastStatus
			{
				get
				{
					if (this.LastScholarshipApplicationApproval != null)
						return $"({this.LastScholarshipApplicationApproval?.SemesterID.ToSemesterString()}) ({(Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses)this.LastScholarshipApplicationApproval?.ApprovalStatus})";
					return $"N/A";
				}
			}

			public bool StatusButtonVisible
			{
				get
				{
					return this.ScholarshipApplicationID != null && this.LastScholarshipApplicationApproval != null;
				}
			}

			public bool ActionTypeForDoc
			{
				get
				{
					switch (ScholarshipApplicationStatusEnum)
					{
						case null:
						case Model.Entities.ScholarshipApplication.Statuses.InProgress:
							return false;
						case Model.Entities.ScholarshipApplication.Statuses.Submitted:
							return true;
						default:
							throw new NotImplementedEnumException(ScholarshipApplicationStatusEnum);
					}
				}
			}
		}

		public static ScholarshipApplicationApproval GetFirstScholarshipApplication(int scholarshipApplicationID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var firstScholarshipApplication = aspireContext.ScholarshipApplicationApprovals
					.Where(saa => saa.ScholarshipApplicationID == scholarshipApplicationID)
					.OrderBy(saa => saa.SemesterID).FirstOrDefault();
				if (firstScholarshipApplication == null)
					return null;
				return firstScholarshipApplication;
			}
		}

		public static List<ScholarshipAnnouncement> GetScholarshipApplications(string enrollment, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var studentInfo = aspireContext.Students
					.Where(s => s.Enrollment == enrollment)
					.Select(s => new
					{
						s.StudentID,
						s.AdmissionOpenProgram.Program.InstituteID
					}).SingleOrDefault();
				if (studentInfo == null)
					return null;

				var currentSemesterID = aspireContext.StudentSemesters.Where(ss => ss.Semester.SemesterType != (byte)SemesterTypes.Summer).OrderByDescending(ss => ss.SemesterID).Select(ss => ss.SemesterID).First();

				var applications = aspireContext.ScholarshipAnnouncements.Include(sa => sa.ScholarshipApplications)
					.Where(sa => sa.Scholarship.InstituteID == loginHistory.InstituteID)
					.Where(sa => sa.SemesterID == currentSemesterID ||
								 sa.ScholarshipApplications.Any(sapp => sapp.StudentID == studentInfo.StudentID)) 
					.Select(sa => new ScholarshipAnnouncement
					 {
						 StudentID = studentInfo.StudentID,
						 ScholarshipAnnouncementID = sa.ScholarshipAnnouncementID,
						 SemesterID = sa.SemesterID,
						 ScholarshipTypeEnum = (Model.Entities.Scholarship.ScholarshipTypes)sa.Scholarship.ScholarshipType,
						 ScholarshipApplicationStatusEnum = sa.ScholarshipApplications.Where(sapp => sapp.StudentID == studentInfo.StudentID).Select(sapp => (ScholarshipApplication.Statuses?)sapp.Status).FirstOrDefault(),
						 DocumentsStatusEnum = sa.ScholarshipApplications.Where(sapp => sapp.StudentID == studentInfo.StudentID).Select(sapp => (ScholarshipApplication.DocumentsStatuses?)sapp.DocumentsStatus).FirstOrDefault(),
						 ScholarshipApplicationID = sa.ScholarshipApplications.Where(sapp => sapp.StudentID == studentInfo.StudentID).Select(sapp => (int?)sapp.ScholarshipApplicationID).FirstOrDefault(),
						 LastScholarshipApplicationApproval = sa.ScholarshipApplications.Where(sapp => sapp.StudentID == studentInfo.StudentID)
							.Select(sap => sap.ScholarshipApplicationApprovals.OrderByDescending(saa => saa.SemesterID)
							   .Select(saa => new ScholarshipAnnouncement.ScholarshipApplicationApproval
							   {
								   SemesterID = saa.SemesterID,
								   ApprovalStatus = saa.ApprovalStatus,
							   }).FirstOrDefault()
						).FirstOrDefault(),
					 }).OrderByDescending(a => a.SemesterID).ToList();

				return applications;
			}
		}

		public enum UpdateDocumentsStatus
		{
			NoRecordFound,
			InProgress,
			Success
		}

		public static UpdateDocumentsStatus ToggleDocumentsStatus(int scholarshipApplicationID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var scholarshipApplication = aspireContext.ScholarshipApplications.SingleOrDefault(sa => sa.ScholarshipApplicationID == scholarshipApplicationID);
				if (scholarshipApplication == null)
					return UpdateDocumentsStatus.NoRecordFound;

				if (scholarshipApplication.StatusEnum == ScholarshipApplication.Statuses.InProgress)
					return UpdateDocumentsStatus.InProgress;

				switch (scholarshipApplication.DocumentStatusesEnum)
				{
					case ScholarshipApplication.DocumentsStatuses.NotSubmitted:
						scholarshipApplication.DocumentStatusesEnum = ScholarshipApplication.DocumentsStatuses.Submitted;
						break;
					case ScholarshipApplication.DocumentsStatuses.Submitted:
						scholarshipApplication.DocumentStatusesEnum = ScholarshipApplication.DocumentsStatuses.NotSubmitted;
						break;
					default:
						throw new NotImplementedEnumException(scholarshipApplication.DocumentStatusesEnum);
				}

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateDocumentsStatus.Success;
			}
		}
	}
}
