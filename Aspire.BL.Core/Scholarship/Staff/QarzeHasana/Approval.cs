﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Scholarship.Staff.QarzeHasana
{
	public static class Approval
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.QarzeHasanaModule, UserGroupPermission.PermissionValues.Allowed);
		}

		private static void DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, StaffPermissions.Scholarship permissionType)
		{
			aspireContext.DemandStaffPermissions(loginSessionGuid, permissionType, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class ScholarshipApplicantSummary
		{
			internal ScholarshipApplicantSummary() { }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public List<StudentSemester> StudentSemesters { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.StudentSemesters.FirstOrDefault()?.SemesterNo);
			public short AnnouncementSemesterID { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public bool IsFirstSemester => this.IntakeSemesterID == this.CurrentApprovalSemesterID;
			public double? PreviousDegreePercentage { get; internal set; }
			public sealed class StudentSemester
			{
				internal StudentSemester() { }
				public int StudentID { get; internal set; }
				public short SemesterNo { get; internal set; }
				public short SemesterID { get; internal set; }
				public decimal? GPA { get; internal set; }
				public decimal? CGPA { get; internal set; }
				public decimal? BUExamGPA { get; internal set; }
				public decimal? BUExamCGPA { get; internal set; }
			}

			public string LastResult
			{
				get
				{
					var currentSemesterResult = this.StudentSemesters.Where(ss => !Semester.IsSummer(ss.SemesterID) && ss.SemesterID < this.CurrentApprovalSemesterID).OrderByDescending(ss => ss.SemesterID).FirstOrDefault();
					var lastSemesterResult = this.StudentSemesters.Where(ss => !Semester.IsSummer(ss.SemesterID) && ss.SemesterID < this.CurrentApprovalSemesterID).OrderByDescending(ss => ss.SemesterID).Skip(1).FirstOrDefault();

					if (currentSemesterResult != null && currentSemesterResult.GPA != null && currentSemesterResult.CGPA != null)
						return $"GPA: {currentSemesterResult.GPA:N2}, CGPA: {currentSemesterResult.CGPA:N2} ({currentSemesterResult.SemesterID.ToSemesterString()})";

					if (lastSemesterResult != null && lastSemesterResult.GPA != null && lastSemesterResult.CGPA != null)
						return $"GPA: {lastSemesterResult.GPA:N2}, CGPA: {lastSemesterResult.CGPA:N2} ({lastSemesterResult.SemesterID.ToSemesterString()})";

					if (this.IsFirstSemester && this.PreviousDegreePercentage != null)
						return $"{this.PreviousDegreePercentage:N2}%";
					return $"GPA: N/A, CGPA: N/A";
				}
			}
			public short FinalSemester { get; set; }
			public int ScholarshipApplicationID { get; internal set; }
			public int StudentID { get; internal set; }
			public List<ApplicationApproval> ApplicationApprovals { get; internal set; }
			public short CurrentApprovalSemesterID { get; internal set; }
			public ScholarshipApplicationApproval.ApprovalStatuses ApplicationApprovalStatusEnum { get; internal set; }
		}

		public static ScholarshipApplicantSummary GetScholarshipApplicantSummary(int? scholarshipApplicationID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var scholarshipApplicantSummary = aspireContext.ScholarshipApplications
					.Where(sap => sap.ScholarshipApplicationID == scholarshipApplicationID
					&& sap.ScholarshipAnnouncement.Scholarship.InstituteID == loginHistory.InstituteID)
				.Select(sas => new ScholarshipApplicantSummary
				{
					ScholarshipApplicationID = sas.ScholarshipApplicationID,
					Enrollment = sas.Student.Enrollment,
					StudentID = sas.StudentID,
					Name = sas.Student.Name,
					ProgramAlias = sas.Student.AdmissionOpenProgram.Program.ProgramAlias,
					AnnouncementSemesterID = sas.ScholarshipAnnouncement.SemesterID,
					IntakeSemesterID = sas.Student.AdmissionOpenProgram.SemesterID,
					StudentSemesters = sas.Student.StudentSemesters.OrderByDescending(ss => ss.SemesterID).Select(ss => new ScholarshipApplicantSummary.StudentSemester
					{
						StudentID = ss.StudentID,
						SemesterID = ss.SemesterID,
						GPA = ss.BUExamGPA,
						CGPA = ss.BUExamCGPA,
						SemesterNo = ss.SemesterNo,
					}).ToList(),
					CurrentApprovalSemesterID = sas.ScholarshipApplicationApprovals.OrderByDescending(saap => saap.SemesterID).Select(saap => saap.SemesterID).FirstOrDefault(),
					PreviousDegreePercentage = sas.Student.StudentAcademicRecords
						.OrderByDescending(sar => sar.DegreeType)
						.Select(sar => (double?)sar.Percentage)
						.FirstOrDefault(),
					FinalSemester = (short)sas.Student.AdmissionOpenProgram.FinalSemesterID,
				}).SingleOrDefault();

				if (scholarshipApplicantSummary != null)
					scholarshipApplicantSummary.ApplicationApprovals = aspireContext.GetApplicationApprovals(scholarshipApplicantSummary.ScholarshipApplicationID, loginSessionGuid);
				return scholarshipApplicantSummary;
			}
		}

		public static ScholarshipApplicantSummary GetScholarshipApplicationSummaryAndStatusForStudent(int? scholarshipApplicationID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				if (scholarshipApplicationID == null)
					return null;
				var loginHistory = aspireContext.DemandStudentActiveSession(loginSessionGuid, SubUserTypes.None);

				var scholarshipApplicantSummary = aspireContext.ScholarshipApplications
					.Where(sap => sap.ScholarshipApplicationID == scholarshipApplicationID)
				.Select(sas => new ScholarshipApplicantSummary
				{
					ScholarshipApplicationID = sas.ScholarshipApplicationID,
					Enrollment = sas.Student.Enrollment,
					StudentID = sas.StudentID,
					Name = sas.Student.Name,
					ProgramAlias = sas.Student.AdmissionOpenProgram.Program.ProgramAlias,
					AnnouncementSemesterID = sas.ScholarshipAnnouncement.SemesterID,
					IntakeSemesterID = sas.Student.AdmissionOpenProgram.SemesterID,
					StudentSemesters = sas.Student.StudentSemesters.OrderByDescending(ss => ss.SemesterID).Select(ss => new ScholarshipApplicantSummary.StudentSemester
					{
						StudentID = ss.StudentID,
						SemesterID = ss.SemesterID,
						GPA = ss.BUExamGPA,
						CGPA = ss.BUExamCGPA,
						SemesterNo = ss.SemesterNo,
					}).ToList(),
					CurrentApprovalSemesterID = sas.ScholarshipApplicationApprovals.OrderByDescending(saap => saap.SemesterID).Select(saap => saap.SemesterID).FirstOrDefault(),
					PreviousDegreePercentage = sas.Student.StudentAcademicRecords
						.OrderByDescending(sar => sar.DegreeType)
						.Select(sar => (double?)sar.Percentage)
						.FirstOrDefault(),
					FinalSemester = (short)sas.Student.AdmissionOpenProgram.FinalSemesterID,
				}).SingleOrDefault();

				if (scholarshipApplicantSummary != null)
					scholarshipApplicantSummary.ApplicationApprovals = aspireContext.GetApplicationApprovals(scholarshipApplicantSummary.ScholarshipApplicationID, loginSessionGuid);
				return scholarshipApplicantSummary;
			}
		}

		public sealed class ApplicationApproval
		{
			internal ApplicationApproval() { }
			public int ScholarshipApplicationApprovalID { get; internal set; }
			public int ScholarshipApplicationID { get; internal set; }
			public ScholarshipApplicationApproval.ApprovalStatuses ApplicationApprovalStatusEnum { get; internal set; }
			public ScholarshipApplicationApproval.ChequeStatuses ChequeStatusEnum { get; internal set; }
			public string Data { get; internal set; }
			public short SemesterID { get; internal set; }

			private Common.Schemas.ScholarshipApplicationApproval _scholarshipApplicationApproval;
			public Common.Schemas.ScholarshipApplicationApproval ScholarshipApplicationApproval
			{
				get
				{
					if (this._scholarshipApplicationApproval == null)
						this._scholarshipApplicationApproval = Common.Schemas.ScholarshipApplicationApproval.FromJson(this.Data);
					return this._scholarshipApplicationApproval;
				}
			}
			public bool ActionTypeForDeleteBtn
			{
				get
				{
					switch (this.ApplicationApprovalStatusEnum)
					{
						case Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Pending:
							return true;
						case Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Approved:
						case Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Rejected:
							return false;
						default:
							return false;
					}
				}
			}

			public bool ActionTypeForChequeBtn
			{
				get
				{
					switch (this.ApplicationApprovalStatusEnum)
					{
						case Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Pending:
						case Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Rejected:
							return false;
						case Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Approved:
							return true;
						default:
							return false;
					}
				}
			}

			public string ApprovalStatuses
			{
				get
				{
					switch (this.ApplicationApprovalStatusEnum)
					{
						case Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Approved:
							switch (this.ChequeStatusEnum)
							{
								case Model.Entities.ScholarshipApplicationApproval.ChequeStatuses.No:
									return "Pending";
								case Model.Entities.ScholarshipApplicationApproval.ChequeStatuses.Yes:
									return "Approved";
								default:
									return "Pending";
							}
						case Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Pending:
							return "Pending";
						case Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Rejected:
							return "Rejected";
						default:
							return "Pending";
					}
				}
			}
		}

		public static List<ApplicationApproval> GetApplicationApprovals(this AspireContext aspireContext, int? scholarshipApplicationID, Guid loginSessionGuid)
		{
			return aspireContext.ScholarshipApplicationApprovals
				.Where(sap => sap.ScholarshipApplicationID == scholarshipApplicationID)
				.Select(saa => new ApplicationApproval
				{
					ScholarshipApplicationApprovalID = saa.ScholarshipApplicationApprovalID,
					ScholarshipApplicationID = saa.ScholarshipApplicationID,
					Data = saa.Data,
					ApplicationApprovalStatusEnum = (ScholarshipApplicationApproval.ApprovalStatuses)saa.ApprovalStatus,
					ChequeStatusEnum = (ScholarshipApplicationApproval.ChequeStatuses)saa.ChequeStatus,
					SemesterID = saa.SemesterID,
				}).ToList();
		}

		public enum ChequeStatuses
		{
			NoRecordFound,
			Success
		}

		public static ChequeStatuses ToggleChequeStatus(int ScholarshipApplicationApprovalID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.CanManageQarzeHasnaStudentsFeeDetails, UserGroupPermission.PermissionValues.Allowed);
				var scholarshipApplications = aspireContext.ScholarshipApplicationApprovals
					.Where(sap => sap.ScholarshipApplicationApprovalID == ScholarshipApplicationApprovalID).FirstOrDefault();

				if (scholarshipApplications == null)
					return ChequeStatuses.NoRecordFound;

				switch (scholarshipApplications.ChequeStatusesEnum)
				{
					case ScholarshipApplicationApproval.ChequeStatuses.Yes:
						scholarshipApplications.ChequeStatusesEnum = ScholarshipApplicationApproval.ChequeStatuses.No;
						break;
					case ScholarshipApplicationApproval.ChequeStatuses.No:
						scholarshipApplications.ChequeStatusesEnum = ScholarshipApplicationApproval.ChequeStatuses.Yes;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return ChequeStatuses.Success;
			}
		}

		public static int? GetTutionFee(int? scholarshipApplicationID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.GetTutionFeeData(null, null, scholarshipApplicationID.Value, loginSessionGuid)
					.Where(f => f.ScholarshipApplicationID == scholarshipApplicationID)
					.OrderByDescending(f => f.ApprovalSemesterID)
					.Select(s => s.CalculatedTutionFee).FirstOrDefault();
			}
		}

		#region BulkOperation

		public sealed class ApplicationForApproval
		{
			internal ApplicationForApproval() { }
			public int ScholarshipApplicationID { get; internal set; }
			public short AnnouncementSemesterID { get; internal set; }
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
		}

		public static List<ApplicationForApproval> GetDataForBulkApprovals(short approvalSemesterID, int? scholarshipAnnouncementID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

				var scholarshipApplicationQuery = aspireContext.ScholarshipApplications
					.Where(sa => sa.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				if (scholarshipAnnouncementID != null)
					scholarshipApplicationQuery = scholarshipApplicationQuery.Where(sa => sa.ScholarshipAnnouncementID == scholarshipAnnouncementID);

				scholarshipApplicationQuery = scholarshipApplicationQuery
					.Where(sa => sa.ScholarshipApplicationApprovals.Any(saa => saa.SemesterID == sa.ScholarshipAnnouncement.SemesterID && saa.ApprovalStatus == (byte)ScholarshipApplicationApproval.ApprovalStatuses.Approved))
					.Where(sa => sa.Student.AdmissionOpenProgram.FinalSemesterID >= approvalSemesterID)
					.Where(sa => !sa.ScholarshipApplicationApprovals.Any(saa => saa.SemesterID >= approvalSemesterID));

				return scholarshipApplicationQuery.Select(sa => new ApplicationForApproval
				{
					ScholarshipApplicationID = sa.ScholarshipApplicationID,
					AnnouncementSemesterID = sa.ScholarshipAnnouncement.SemesterID,
					StudentID = sa.StudentID,
					Enrollment = sa.Student.Enrollment,
					Name = sa.Student.Name,
					ProgramAlias = sa.Student.AdmissionOpenProgram.Program.ProgramAlias
				}).OrderByDescending(sa => sa.AnnouncementSemesterID).ThenBy(sa => sa.Enrollment).ToList();
			}
		}

		public sealed class ApplicationGenerationStatusesResults
		{
			public enum ApplicationGenerationStatuses
			{
				Success,
				NoRecordAdded,
			}
			public ApplicationGenerationStatuses Status { get; internal set; }
			public int Count { get; internal set; }
		}

		public static ApplicationGenerationStatusesResults GenerateApplications(List<int> scholarshipApplicationIDs, short newSemesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				if (scholarshipApplicationIDs?.Any() != true)
					return new ApplicationGenerationStatusesResults { Status = ApplicationGenerationStatusesResults.ApplicationGenerationStatuses.NoRecordAdded };

				int count = 0;
				foreach (var scholarshipApplicationID in scholarshipApplicationIDs)
				{
					var record = aspireContext.ScholarshipApplicationApprovals
						.Where(saa => saa.ScholarshipApplicationID == scholarshipApplicationID)
						.Select(saa => new
						{
							ScholarshipApplicationApproval = saa,
							ScholarshipIntakeSemesterID = saa.ScholarshipApplication.ScholarshipAnnouncement.SemesterID,
							StudentIntakeSemesterID = saa.ScholarshipApplication.Student.AdmissionOpenProgram.SemesterID,
							saa.ScholarshipApplication.Student.AdmissionOpenProgram.Program.InstituteID,
							saa.ScholarshipApplication.Student.AdmissionOpenProgram.Program.DepartmentID,
							saa.ScholarshipApplication.Student.AdmissionOpenProgram.ProgramID,
							saa.ScholarshipApplication.Student.AdmissionOpenProgramID,
							saa.SemesterID,
						}).FirstOrDefault();

					if (record?.ScholarshipApplicationApproval == null)
						return new ApplicationGenerationStatusesResults { Status = ApplicationGenerationStatusesResults.ApplicationGenerationStatuses.NoRecordAdded };

					var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.CanManageQarzeHasanaDSARecommendations, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

					if (record.ScholarshipIntakeSemesterID != newSemesterID && record.ScholarshipApplicationApproval.ApprovalStatusEnum != Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Approved)
						continue;

					var getlastApproval = aspireContext.ScholarshipApplicationApprovals.Where(saa => saa.ScholarshipApplicationID == record.ScholarshipApplicationApproval.ScholarshipApplicationID).OrderByDescending(saa => saa.SemesterID).FirstOrDefault();

					var getLastApprovalData = BL.Core.Scholarship.Common.Schemas.ScholarshipApplicationApproval.FromJson(getlastApproval.Data);

					var approvalData = new BL.Core.Scholarship.Common.Schemas.ScholarshipApplicationApproval
					{
						CampusRecommendation = null,
						CampusRemarks = null,
						CSCRecommendation = null,
						CSCRemarks = null,
						TuitionFee = null,
						FinalRecommendation = getLastApprovalData.FinalRecommendation
					};

					var scholarshipApplicationApproval = new Model.Entities.ScholarshipApplicationApproval
					{
						ScholarshipApplicationID = scholarshipApplicationID,
						SemesterID = newSemesterID,
						ApprovalDate = DateTime.Now,
						Data = approvalData.ToJson(),
						ChequeStatusesEnum = ScholarshipApplicationApproval.ChequeStatuses.No,
						ApprovalStatusEnum = ScholarshipApplicationApproval.ApprovalStatuses.Pending,
					};
					aspireContext.ScholarshipApplicationApprovals.Add(scholarshipApplicationApproval);
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
					count++;
				}
				aspireContext.CommitTransaction();
				return new ApplicationGenerationStatusesResults
				{
					Status = ApplicationGenerationStatusesResults.ApplicationGenerationStatuses.Success,
					Count = count
				};
			}
		}

		public static readonly decimal MinimumAllowedGPACGAP = 2.75m;

		public sealed class ApplicationForRejection
		{
			internal ApplicationForRejection() { }
			public int ScholarshipApplicationID { get; internal set; }
			public short AnnouncementSemesterID { get; internal set; }
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short ApprovalSemesterID { get; internal set; }
			public ScholarshipApplicationApproval.ApprovalStatuses ApprovalStatusEnum { get; internal set; }
			public List<StudentSemester> StudentSemesters { get; internal set; }
			private short PreviousSemesterID => Semester.PreviousRegularSemester(this.ApprovalSemesterID, false);
			public StudentSemester PreviousStudentSemester => this.StudentSemesters.SingleOrDefault(ss => ss.SemesterID == this.PreviousSemesterID);

			public sealed class StudentSemester
			{
				internal StudentSemester() { }
				public short SemesterID { get; internal set; }
				public decimal? BUExamGPA { get; internal set; }
				public decimal? BUExamCGPA { get; internal set; }
			}

			public bool ValidForRejection(short semesterID)
			{
				if (this.PreviousStudentSemester != null && !IsFirstSemester(semesterID))
				{
					if (this.PreviousStudentSemester.BUExamGPA == null || this.PreviousStudentSemester.BUExamCGPA == null || this.PreviousStudentSemester.BUExamGPA.Value >= MinimumAllowedGPACGAP || this.PreviousStudentSemester.BUExamCGPA.Value >= MinimumAllowedGPACGAP)
						return false;
					return true;
				}
				return false;
			}

			public bool IsFirstSemester(short semesterID)
			{
				return this.AnnouncementSemesterID == semesterID;
			}
		}

		public static List<ApplicationForRejection> GetDataForBulkRejection(short approvalSemesterID, int? scholarshipAnnouncementID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

				var scholarshipApplicationApprovalsQuery = aspireContext.ScholarshipApplicationApprovals
					.Where(saa => saa.ScholarshipApplication.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(saa => saa.SemesterID == approvalSemesterID)
					.Where(saa => saa.ApprovalStatus == (byte)ScholarshipApplicationApproval.ApprovalStatuses.Pending);
				if (scholarshipAnnouncementID != null)
					scholarshipApplicationApprovalsQuery = scholarshipApplicationApprovalsQuery.Where(saa => saa.ScholarshipApplication.ScholarshipAnnouncementID == scholarshipAnnouncementID);

				return scholarshipApplicationApprovalsQuery.Select(saa => new ApplicationForRejection
				{
					ScholarshipApplicationID = saa.ScholarshipApplicationID,
					AnnouncementSemesterID = saa.ScholarshipApplication.ScholarshipAnnouncement.SemesterID,
					StudentID = saa.ScholarshipApplication.StudentID,
					Enrollment = saa.ScholarshipApplication.Student.Enrollment,
					Name = saa.ScholarshipApplication.Student.Name,
					ProgramAlias = saa.ScholarshipApplication.Student.AdmissionOpenProgram.Program.ProgramAlias,
					ApprovalSemesterID = saa.SemesterID,
					ApprovalStatusEnum = (ScholarshipApplicationApproval.ApprovalStatuses)saa.ApprovalStatus,
					StudentSemesters = saa.ScholarshipApplication.Student.StudentSemesters
						.Where(ss => ss.SemesterID < approvalSemesterID).OrderByDescending(ss => ss.SemesterID)
						.Select(ss => new ApplicationForRejection.StudentSemester
						{
							SemesterID = ss.SemesterID,
							BUExamGPA = ss.BUExamGPA,
							BUExamCGPA = ss.BUExamCGPA
						}).ToList()
				}).OrderByDescending(saa => saa.AnnouncementSemesterID).ThenBy(saa => saa.Enrollment)
				.ToList().FindAll(r => r.ValidForRejection(approvalSemesterID));
			}
		}

		public sealed class ApplicationRejectionStatusesResults
		{
			public enum ApplicationRejectionStatuses
			{
				Success,
				NoRecordAdded,
			}
			public ApplicationRejectionStatuses Status { get; internal set; }
			public int Count { get; internal set; }
		}

		public static ApplicationRejectionStatusesResults RejectApplications(List<int> scholarshipApplicationIDs, short rejectionSemesterID, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				if (scholarshipApplicationIDs?.Any() != true)
					throw new ArgumentNullException(nameof(scholarshipApplicationIDs));
				var now = DateTime.Now;

				int count = 0;
				foreach (var scholarshipApplicationID in scholarshipApplicationIDs)
				{
					var record = aspireContext.ScholarshipApplicationApprovals
						.Where(saa => saa.ScholarshipApplicationID == scholarshipApplicationID && saa.SemesterID == rejectionSemesterID)
						.Where(saa => saa.ApprovalStatus == (byte)ScholarshipApplicationApproval.ApprovalStatuses.Pending)
						.Select(saa => new
						{
							ScholarshipApplicationApproval = saa,
							ScholarshipIntakeSemesterID = saa.ScholarshipApplication.ScholarshipAnnouncement.SemesterID,
							StudentIntakeSemesterID = saa.ScholarshipApplication.Student.AdmissionOpenProgram.SemesterID,
							saa.ScholarshipApplication.Student.AdmissionOpenProgram.Program.InstituteID,
							saa.ScholarshipApplication.Student.AdmissionOpenProgram.Program.DepartmentID,
							saa.ScholarshipApplication.Student.AdmissionOpenProgram.ProgramID,
							saa.ScholarshipApplication.Student.AdmissionOpenProgramID,
							StudentSemesters = saa.ScholarshipApplication.Student.StudentSemesters.Where(ss => ss.SemesterID < rejectionSemesterID).ToList(),
							AcademicRecords = saa.ScholarshipApplication.Student.StudentAcademicRecords.ToList()
						}).SingleOrDefault();
					if (record?.ScholarshipApplicationApproval == null)
						return new ApplicationRejectionStatusesResults { Status = ApplicationRejectionStatusesResults.ApplicationRejectionStatuses.NoRecordAdded };

					var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.CanManageQarzeHasanaCampusRecommendations, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

					if (record.ScholarshipIntakeSemesterID == rejectionSemesterID)
						continue;
					else
					{
						var studentSemester = record.StudentSemesters.OrderByDescending(ss => ss.SemesterID).FirstOrDefault(ss => !Semester.IsSummer(ss.SemesterID));
						if (studentSemester == null)
							continue;
						if (studentSemester.BUExamGPA == null || studentSemester.BUExamCGPA == null || studentSemester.BUExamGPA.Value >= MinimumAllowedGPACGAP || studentSemester.BUExamCGPA.Value >= MinimumAllowedGPACGAP)
							continue;
					}
					var data = BL.Core.Scholarship.Common.Schemas.ScholarshipApplicationApproval.FromJson(record.ScholarshipApplicationApproval.Data);
					if (data == null)
						continue;
					data.CampusRemarks = remarks;
					record.ScholarshipApplicationApproval.Data = data.ToJson();
					record.ScholarshipApplicationApproval.ApprovalStatusEnum = ScholarshipApplicationApproval.ApprovalStatuses.Rejected;
					record.ScholarshipApplicationApproval.ApprovalDate = now;
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
					count++;
				}

				aspireContext.CommitTransaction();
				return new ApplicationRejectionStatusesResults
				{
					Status = ApplicationRejectionStatusesResults.ApplicationRejectionStatuses.Success,
					Count = count
				};
			}
		}

		public sealed class ApplicationsForUpdaetFee
		{
			internal ApplicationsForUpdaetFee() { }
			internal ScholarshipApplicationApproval ScholarshipApplicationApproval { get; set; }
			public int ScholarshipApplicationID { get; internal set; } //=> this.ScholarshipApplicationApproval.ScholarshipApplicationID;
			public short AnnouncementSemesterID { get; internal set; } //=> this.ScholarshipApplicationApproval.ScholarshipApplication.ScholarshipAnnouncement.SemesterID;
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short ApprovalSemesterID { get; internal set; }
			public string Data => this.ScholarshipApplicationApproval.Data;// { get; internal set; }
			public int? CalculatedTutionFee { get; internal set; }

			private Common.Schemas.ScholarshipApplicationApproval _scholarshipApplicationApprovalData;
			public Common.Schemas.ScholarshipApplicationApproval ScholarshipApplicationApprovalData
			{
				get
				{
					if (this._scholarshipApplicationApprovalData == null)
						this._scholarshipApplicationApprovalData = Common.Schemas.ScholarshipApplicationApproval.FromJson(this.Data);
					return this._scholarshipApplicationApprovalData;
				}
			}
		}

		private static IQueryable<ApplicationsForUpdaetFee> GetTutionFeeData(this AspireContext aspireContext, short? approvalSemesterID, int? scholarshipAnnouncementID, int? scholarshipApplicationID, Guid loginSessionGuid)
		{
			var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

			var scholarshipApplicationApprovalsQuery = aspireContext.ScholarshipApplicationApprovals
							.Where(saa => saa.ScholarshipApplication.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
							.Where(saa => saa.SemesterID == approvalSemesterID.Value)
							.Where(saa => saa.ApprovalStatus == (byte)ScholarshipApplicationApproval.ApprovalStatuses.Pending);
			
			if (scholarshipAnnouncementID != null)
				scholarshipApplicationApprovalsQuery = scholarshipApplicationApprovalsQuery.Where(saa => saa.ScholarshipApplication.ScholarshipAnnouncementID == scholarshipAnnouncementID);

			if (scholarshipApplicationID != null)
				scholarshipApplicationApprovalsQuery = aspireContext.ScholarshipApplicationApprovals
							.Where(saa => saa.ScholarshipApplication.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
							.Where(saa => saa.ScholarshipApplicationID == scholarshipApplicationID);

			return scholarshipApplicationApprovalsQuery.Select(saa => new ApplicationsForUpdaetFee
			{
				ScholarshipApplicationApproval = saa,
				ScholarshipApplicationID = saa.ScholarshipApplicationID,
				AnnouncementSemesterID = saa.ScholarshipApplication.ScholarshipAnnouncement.SemesterID,
				StudentID = saa.ScholarshipApplication.StudentID,
				Enrollment = saa.ScholarshipApplication.Student.Enrollment,
				Name = saa.ScholarshipApplication.Student.Name,
				ProgramAlias = saa.ScholarshipApplication.Student.AdmissionOpenProgram.Program.ProgramAlias,
				ApprovalSemesterID = saa.SemesterID,
				CalculatedTutionFee = saa.ScholarshipApplication.Student.StudentFees
					.Where(sf => sf.FeeType == (byte)StudentFee.FeeTypes.Challan)
					.Where(sf => sf.SemesterID == saa.SemesterID)
					.SelectMany(sf => sf.StudentFeeDetails)
					.Where(sfd => sfd.FeeHead.FeeHeadType == (byte)FeeHead.FeeHeadTypes.TutionFee)
					.Sum(sfd => (int?)sfd.GrandTotalAmount)
			});
		}

		public static List<ApplicationsForUpdaetFee> GetDataToUpdateFee(short approvalSemesterID, int? scholarshipAnnouncementID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.GetTutionFeeData(approvalSemesterID, scholarshipAnnouncementID, null, loginSessionGuid)
					.OrderByDescending(saa => saa.AnnouncementSemesterID).ThenBy(saa => saa.Enrollment)
					.ToList();
			}
		}

		public sealed class UpdateFeeStatusesResults
		{
			public enum UpdatedFeeStatuses
			{
				Success,
				NoRecordAdded,
			}
			public UpdatedFeeStatuses Status { get; internal set; }
			public int Count { get; internal set; }
		}

		public static UpdateFeeStatusesResults UpdateFee(List<int> scholarshipApplicationIDs, short feeAddedSemesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.CanManageQarzeHasnaStudentsFeeDetails, UserGroupPermission.PermissionValues.Allowed);
				if (scholarshipApplicationIDs?.Any() != true)
					throw new ArgumentNullException(nameof(scholarshipApplicationIDs));
				var dateTime = DateTime.Now;
				int count = 0;

				var SummerSemester = aspireContext.StudentSemesters.Where(ss => ss.SemesterID == feeAddedSemesterID && ss.Semester.SemesterType == (byte)SemesterTypes.Summer).FirstOrDefault();
				if (SummerSemester != null)
					return null;

				var feeData = aspireContext.GetTutionFeeData(feeAddedSemesterID, null, null, loginSessionGuid)
					.Where(s => scholarshipApplicationIDs.Contains(s.ScholarshipApplicationID))
					.ToList();

				foreach (var fee in feeData)
				{
					var data = BL.Core.Scholarship.Common.Schemas.ScholarshipApplicationApproval.FromJson(fee.ScholarshipApplicationApproval.Data);
					data.TuitionFee = fee.CalculatedTutionFee;
					fee.ScholarshipApplicationApproval.Data = data.ToJson();
					fee.ScholarshipApplicationApproval.ApprovalDate = dateTime;
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
					count++;
				}

				aspireContext.CommitTransaction();
				return new UpdateFeeStatusesResults
				{
					Status = UpdateFeeStatusesResults.UpdatedFeeStatuses.Success,
					Count = count
				};
			}
		}
		#endregion
	}
}