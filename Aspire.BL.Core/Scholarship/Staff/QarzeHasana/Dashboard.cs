﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Scholarship.Staff.QarzeHasana
{
	public static class Dashboard
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.QarzeHasanaModule, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class ScholarshipAnnouncement
		{
			internal ScholarshipAnnouncement() { }
			public int ScholarshipAnnouncementID { get; internal set; }
			public short SemesterID { get; internal set; }
			public int AppliedApplications { get; internal set; }
			public int InProgressApplications { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public Model.Entities.Scholarship.ScholarshipTypes ScholarshipTypeEnum { get; internal set; }
			public DateTime OpeningDate { get; internal set; }
			public DateTime? ClosingDate { get; internal set; }
			public List<Applicant> Applicants { get; internal set; }
		}

		public static ScholarshipAnnouncement ScholarshipAnnouncementSummary(int? scholarshipAnnouncementID, FeeAddedStatuses? feeAdded, CampusRemarksAddedStatuses? campusRemarksAdded, CSCRemarksAddedStatuses? cscRemarksAdded, ScholarshipApplicationApproval.ApprovalStatuses? approvalStatusEnum, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

				var scholarshipAnnouncement = aspireContext.ScholarshipAnnouncements
						.Where(san => san.ScholarshipAnnouncementID == scholarshipAnnouncementID
							&& san.Scholarship.InstituteID == loginHistory.InstituteID)
						.Select(san => new ScholarshipAnnouncement
						{
							ScholarshipAnnouncementID = san.ScholarshipAnnouncementID,
							SemesterID = san.SemesterID,
							OpeningDate = san.StartDate,
							ClosingDate = san.EndDate,
							ScholarshipTypeEnum = (Model.Entities.Scholarship.ScholarshipTypes)san.Scholarship.ScholarshipType,
							AppliedApplications = san.ScholarshipApplications.Count(sap => sap.ScholarshipAnnouncementID == san.ScholarshipAnnouncementID && sap.Status == (byte)ScholarshipApplication.Statuses.Submitted),
							InProgressApplications = san.ScholarshipApplications.Count(sap => sap.ScholarshipAnnouncementID == san.ScholarshipAnnouncementID && sap.Status == (byte)ScholarshipApplication.Statuses.InProgress),
						}).SingleOrDefault();

				scholarshipAnnouncement.Applicants = aspireContext.GetApplicantsData(scholarshipAnnouncementID.Value, feeAdded, campusRemarksAdded, cscRemarksAdded, approvalStatusEnum, searchText, pageIndex, pageSize, sortExpression, sortDirection, out virtualItemCount, loginSessionGuid);

				return scholarshipAnnouncement;
			}
		}

		public sealed class Applicant
		{
			internal Applicant() { }
			public int ScholarshipAnnouncementID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string Name { get; internal set; }
			public ScholarshipApplicationApproval LastScholarshipApplicationApproval { get; internal set; }
			public Model.Entities.Scholarship.ScholarshipTypes ScholarshipTypeEnum { get; internal set; }
			public int ScholarshipApplicationID { get; internal set; }
			public int StudentID { get; internal set; }
			public List<StudentSemester> StudentSemesters { get; internal set; }
			public short SemesterID { get; internal set; }
			public short AnnouncementSemesterID { get; internal set; }
			public short IntakeSemesterID { get; set; }
			public bool IsFirstSemester => this.IntakeSemesterID == this.LastScholarshipApplicationApproval.SemesterID;
			public double? PreviousDegreePercentage { get; internal set; }
			public short FinalSemesterID { get; internal set; }
			public bool IsFinalSemester
			{
				get
				{
					if (this.SemesterID <= this.FinalSemesterID)
						return true;
					return false;
				}
			}

			public sealed class ScholarshipApplicationApproval
			{
				internal ScholarshipApplicationApproval() { }
				public short SemesterID { get; internal set; }
				public byte ApprovalStatus { get; internal set; }
				public string Data { get; internal set; }
			}

			public sealed class StudentSemester
			{
				internal StudentSemester() { }
				public short SemesterID { get; internal set; }
				public decimal? CGPA { get; internal set; }
			}

			public string LastStatus
			{
				get
				{
					var currentSemesterResult = this.StudentSemesters.Where(ss => !Semester.IsSummer(ss.SemesterID) && ss.SemesterID < LastScholarshipApplicationApproval.SemesterID).OrderByDescending(ss => ss.SemesterID).FirstOrDefault();
					var lastSemesterResult = this.StudentSemesters.Where(ss => !Semester.IsSummer(ss.SemesterID) && ss.SemesterID < LastScholarshipApplicationApproval.SemesterID).OrderByDescending(ss => ss.SemesterID).Skip(1).FirstOrDefault();

					if (currentSemesterResult != null && currentSemesterResult.CGPA != null)
						return $"({this.LastScholarshipApplicationApproval.SemesterID.ToSemesterString()}) {(Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses)this.LastScholarshipApplicationApproval.ApprovalStatus} ({currentSemesterResult.CGPA:N2})";

					if (lastSemesterResult != null && lastSemesterResult.CGPA != null)
						return $"({this.LastScholarshipApplicationApproval.SemesterID.ToSemesterString()}) {(Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses)this.LastScholarshipApplicationApproval.ApprovalStatus} ({lastSemesterResult.CGPA:N2})";

					if (this.IsFirstSemester && this.PreviousDegreePercentage != null)
						return $"({this.LastScholarshipApplicationApproval.SemesterID.ToSemesterString()}) {(Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses)this.LastScholarshipApplicationApproval.ApprovalStatus} ({this.PreviousDegreePercentage:N2}%)";
					return $"({this.LastScholarshipApplicationApproval.SemesterID.ToSemesterString()}) {(Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses)this.LastScholarshipApplicationApproval.ApprovalStatus} (N/A)";
				}
			}

			public string FormData { get; internal set; }
			public bool? _isOldApplication;
			public bool IsOldApplication
			{
				get
				{
					if (this._isOldApplication == null)
					{
						var applicationData = Aspire.BL.Core.Scholarship.Common.Schemas.QarzeHasana.FromJson(this.FormData);
						return applicationData.AdditionalDetails == "Old Data. Added from bulk operation";
					}
					return this._isOldApplication == true;
				}
			}

			public bool FeeAdded
			{
				get
				{
					var applicationApprovalData = Common.Schemas.ScholarshipApplicationApproval.FromJson(this.LastScholarshipApplicationApproval.Data);
					return applicationApprovalData.TuitionFee != null;
				}
			}
			public bool CampusRemarksAdded
			{
				get
				{
					var applicationApprovalData = Common.Schemas.ScholarshipApplicationApproval.FromJson(this.LastScholarshipApplicationApproval.Data);
					return applicationApprovalData.CampusRemarks.ToNullIfWhiteSpace() != null;
				}
			}
			public bool CSCRemarksAdded
			{
				get
				{
					var applicationApprovalData = Common.Schemas.ScholarshipApplicationApproval.FromJson(this.LastScholarshipApplicationApproval.Data);
					return applicationApprovalData.CSCRemarks.ToNullIfWhiteSpace() != null;
				}
			}
		}

		public enum FeeAddedStatuses
		{
			Yes,
			No
		}
		public enum CampusRemarksAddedStatuses
		{
			Yes,
			No
		}
		public enum CSCRemarksAddedStatuses
		{
			Yes,
			No
		}

		public static List<Applicant> GetApplicantsData(this AspireContext aspireContext, int scholarshipAnnouncementID, FeeAddedStatuses? feeAdded, CampusRemarksAddedStatuses? campusRemarksAdded, CSCRemarksAddedStatuses? cscRemarksAdded, ScholarshipApplicationApproval.ApprovalStatuses? approvalStatusEnum, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
			searchText = searchText.ToNullIfWhiteSpace();
			var approvalStatus = (byte?)approvalStatusEnum;

			var applicantsQuery = aspireContext.ScholarshipApplications
				.Where(sap => sap.ScholarshipAnnouncement.Scholarship.InstituteID == loginHistory.InstituteID)
				.Where(sap => sap.Status == (byte)ScholarshipApplication.Statuses.Submitted)
				.Where(sap => sap.ScholarshipAnnouncementID == scholarshipAnnouncementID)
				.Where(sap => searchText == null || sap.Student.Enrollment.Contains(searchText) || sap.Student.Name.Contains(searchText)
					|| sap.Student.AdmissionOpenProgram.Program.ProgramAlias.Contains(searchText))
				.Select(sap => new Applicant
				{
					ScholarshipAnnouncementID = sap.ScholarshipAnnouncementID,
					Enrollment = sap.Student.Enrollment,
					ProgramAlias = sap.Student.AdmissionOpenProgram.Program.ProgramAlias,
					Name = sap.Student.Name,
					ScholarshipTypeEnum = (Model.Entities.Scholarship.ScholarshipTypes)sap.ScholarshipAnnouncement.Scholarship.ScholarshipType,
					ScholarshipApplicationID = sap.ScholarshipApplicationID,
					StudentID = sap.StudentID,
					StudentSemesters = sap.Student.StudentSemesters.OrderByDescending(ss => ss.SemesterID).Select(ss => new Applicant.StudentSemester
					{
						SemesterID = ss.SemesterID,
						CGPA = ss.BUExamCGPA,
					}).ToList(),
					SemesterID = sap.ScholarshipAnnouncement.SemesterID,
					AnnouncementSemesterID = sap.ScholarshipAnnouncement.SemesterID,
					IntakeSemesterID = sap.Student.AdmissionOpenProgram.SemesterID,
					FinalSemesterID = (short)sap.Student.AdmissionOpenProgram.FinalSemesterID,
					PreviousDegreePercentage = sap.Student.StudentAcademicRecords.OrderByDescending(sar => sar.DegreeType)
									.Select(sar => (double?)sar.Percentage).FirstOrDefault(),
					LastScholarshipApplicationApproval = sap.ScholarshipApplicationApprovals.OrderByDescending(saa => saa.SemesterID)
									.Select(sa => new Applicant.ScholarshipApplicationApproval
									{
										SemesterID = sa.SemesterID,
										ApprovalStatus = sa.ApprovalStatus,
										Data = sa.Data
									}).FirstOrDefault(),
					FormData = sap.FormData
				});

			var applicants = applicantsQuery.ToList();

			if (feeAdded != null)
				if (feeAdded == FeeAddedStatuses.Yes)
					applicants = applicants.FindAll(a => a.FeeAdded);
				else
					applicants = applicants.FindAll(a => !a.FeeAdded);

			if (campusRemarksAdded != null)
				if (campusRemarksAdded == CampusRemarksAddedStatuses.Yes)
					applicants = applicants.FindAll(a => a.CampusRemarksAdded);
				else
					applicants = applicants.FindAll(a => !a.CampusRemarksAdded);

			if (cscRemarksAdded != null)
				if (cscRemarksAdded == CSCRemarksAddedStatuses.Yes)
					applicants = applicants.FindAll(a => a.CSCRemarksAdded);
				else
					applicants = applicants.FindAll(a => !a.CSCRemarksAdded);

			if (approvalStatus != null)
				applicants = applicants.Where(a => a.LastScholarshipApplicationApproval.ApprovalStatus == approvalStatus).ToList();

			virtualItemCount = applicants.Count();
			List<Applicant> finalQuery;
			switch (sortExpression)
			{
				case nameof(Applicant.Enrollment):
					finalQuery = applicants.OrderBy(sortDirection, r => r.Enrollment).ToList();
					break;
				case nameof(Applicant.Name):
					finalQuery = applicants.OrderBy(sortDirection, r => r.Name).ToList();
					break;
				case nameof(Applicant.ProgramAlias):
					finalQuery = applicants.OrderBy(sortDirection, r => r.ProgramAlias).ToList();
					break;
				case nameof(Applicant.FinalSemesterID):
					finalQuery = applicants.OrderBy(sortDirection, r => r.FinalSemesterID).ToList();
					break;
				case nameof(Applicant.FeeAdded):
					finalQuery = applicants.OrderBy(sortDirection, r => r.FeeAdded).ToList();
					break;
				default:
					throw new SortingNotImplementedException(sortExpression);
			}
			return finalQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList();
		}

		#region BulkAddApplications

		public enum AddApplicationStatuses
		{
			NoRecordFound,
			permissionsError,
			AlreadyApplied,
			MaxSemesterExceeded,
			Error,
			Success,
		}

		public static AddApplicationStatuses AddApplicationsData(string textData, int announcementID, short semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				int sNo = 1, rowsAdded = 0, alreadyApplied = 0, MaxSemester = 0, submittedApplications = 0;
				string enrollment;
				int approvalPercentage = 0;

				string[] LineSplit = textData.Split('\n');

				foreach (string item in LineSplit)
				{
					var rowData = item;
					string[] data = rowData.Split('\t');
					enrollment = data[0];
					approvalPercentage = data[1].ToInt();

					var student = aspireContext.Students.Where(s => s.Enrollment == enrollment && s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
						.Select(s => new
						{
							s.StudentID,
							s.AdmissionOpenProgram.Program.InstituteID,
						}).SingleOrDefault();

					if (student == null)
						return AddApplicationStatuses.NoRecordFound;

					var resultApplications = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.AddScholarshipApplication(student.StudentID, announcementID, null, semesterID, loginSessionGuid);

					switch (resultApplications.Status)
					{
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.AddScholarshipApplicationResult.Statuses.NotAuthorizedPerson:
							break;
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.AddScholarshipApplicationResult.Statuses.NoRecordFound:
							break;
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.AddScholarshipApplicationResult.Statuses.AlreadyApplied:
							alreadyApplied++;
							continue;
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.AddScholarshipApplicationResult.Statuses.MaxSemesterExceeded:
							MaxSemester++;
							continue;
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.AddScholarshipApplicationResult.Statuses.Success:
							rowsAdded++;
							int? applicationID = resultApplications.ScholarshipApplication.ScholarshipApplicationID;

							var familyMember = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.AddOrUpdateFamilyMember(applicationID ?? throw new InvalidOperationException(), null, 1, null, "N/A", loginSessionGuid);
							var familyInformation = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.UpdateFamilyInformation(applicationID ?? throw new InvalidOperationException(), 0, 0, loginSessionGuid);
							var familyIncome = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.UpdateFamilyIncome(applicationID ?? throw new InvalidOperationException(), null, null, null, null, null, 1, loginSessionGuid);
							var otherIncome = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.AddOrUpdateOtherIncome(applicationID ?? throw new InvalidOperationException(), null, "N/A", 1, "N/A", loginSessionGuid);
							var familyExpenditures = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.UpdateFamilyExpenditures(applicationID ?? throw new InvalidOperationException(), null, null, null, null, null, null, null, null, 0, 0, "Old Data. Added from bulk operation", loginSessionGuid);

							string submittedResult = SubmittedResult(applicationID, loginSessionGuid);
							if (submittedResult == "true")
								submittedApplications++;
							else if (submittedResult == "StudentInformationIsMissing")
							{
								var studentInfo = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.UpdateStudentInfo(applicationID ?? throw new InvalidOperationException(), student.StudentID, "N/A", "0300-0000000", "N/A", loginSessionGuid);
								string againSubmitResult = SubmittedResult(applicationID, loginSessionGuid);
								if (againSubmitResult == "true")
									submittedApplications++;
							}
							else
							{
								sNo++;
								return AddApplicationStatuses.Error;
							}

							break;
						default:
							throw new ArgumentOutOfRangeException(nameof(resultApplications.Status), resultApplications.Status, null);
					}

					//--------------------------------------------addScholarshipApplicationApprovals--------------------------------

					int studentID = student.StudentID;
					var approval = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.GetApprovalForMigratingData(studentID);
					if (approval == null)
						continue;

					byte approvalStatus = 2;
					int? ScholarshipApplicationApprovalID = approval.ScholarshipApplicationApprovalID;
					int? ScholarshipApplicationID = approval.ScholarshipApplicationID;

					var resultApproval = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.UpdateApplicationApproval(ScholarshipApplicationApprovalID ?? throw new InvalidOperationException(), ScholarshipApplicationID ?? throw new InvalidOperationException(),
						0, null, null, null, null, approvalPercentage, (Aspire.Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses)approvalStatus, loginSessionGuid);

					switch (resultApproval)
					{
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.UpdateApplicationApprovalStatuses.NoRecordFound:
							break;
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.UpdateApplicationApprovalStatuses.TutionFeeAndApprovalEmpty:
							//sNo++;
							break;
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.UpdateApplicationApprovalStatuses.NotAllowedChangesInApprovedOrRejectedApplication:
							//changesNotAllowed++;
							//sNo++;
							break;
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.UpdateApplicationApprovalStatuses.Success:
							//rowsUpdated++;
							break;
						default:
							throw new ArgumentOutOfRangeException(nameof(resultApproval), resultApproval, null);
					}
				}
				return AddApplicationStatuses.Success;
			}
		}

		private static string SubmittedResult(int? scholarshipApplicationID, Guid staffLoginSessionGuid)
		{
			var result = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplication(scholarshipApplicationID ?? throw new InvalidOperationException(), staffLoginSessionGuid);
			if (result.Errors != null)
			{
				foreach (var error in result.Errors)
					switch (error)
					{
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.NotAuthorizedForChange:
							return "NotAuthorizedForChange";
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.NoRecordFound:
							return "NoRecordFound";
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.AlreadySubmitted:
							return "AlreadySubmitted";
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.StudentInformationIsMissing:
							return "StudentInformationIsMissing";
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.FamilyMembersAreMissing:
							return "FamilyMembersAreMissing";
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.FamilyInformationIsMissing:
							return "FamilyInformationIsMissing";
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.FamilyIncomeIsMissing:
							return "FamilyIncomeIsMissing";
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.FamilyExpendituresAreMissing:
							return "FamilyExpendituresAreMissing";
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.Success:
							throw new InvalidOperationException();
						default:
							throw new ArgumentOutOfRangeException(nameof(error), error, null);
					}
				return "false";
			}
			return "true";
		}

		#endregion
	}
}

