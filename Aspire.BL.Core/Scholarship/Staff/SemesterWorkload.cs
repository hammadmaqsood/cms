﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Scholarship.Staff
{
	public static class SemesterWorkload
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.ManageSemesterWorkload, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, null);
		}

		public sealed class ScholarshipWorkload
		{
			internal ScholarshipWorkload()
			{
			}

			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public int ProgramID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterNo { get; internal set; }
			public SemesterNos SemesterNoEnum => (SemesterNos)this.SemesterNo;
			public string SeemsnoNoFullName => this.SemesterNoEnum.ToFullName();
			public decimal OfferedCreditHours { get; internal set; }
			public decimal? CreditHours { get; internal set; }
		}

		public static List<ScholarshipWorkload> GetScholarshipWorkloads(short semesterID, int? programID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = aspireContext.OfferedCourses
					.Where(oc => oc.SemesterID == semesterID)
					.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				if (programID != null)
					query = query.Where(oc => oc.Cours.AdmissionOpenProgram.ProgramID == programID.Value);

				var workload = query.GroupBy(oc => new
				{
					oc.SemesterID,
					oc.Cours.AdmissionOpenProgram.ProgramID,
					oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					oc.SemesterNo,
					oc.Section,
				}).Select(oc => new
				{
					oc.Key.SemesterID,
					oc.Key.ProgramID,
					oc.Key.ProgramAlias,
					oc.Key.SemesterNo,
					oc.Key.Section,
					CreditHours = oc.Sum(c => c.Cours.CreditHours)
				}).ToList();

				workload = workload.GroupBy(oc => new
				{
					oc.SemesterID,
					oc.ProgramID,
					oc.ProgramAlias,
					oc.SemesterNo,
				})
				.SelectMany(oc => oc.Where(c => c.Section == oc.Min(occ => occ.Section)))
				.ToList();

				var scholarshipWorkloadsQuery = aspireContext.ScholarshipWorkloads
					.Where(sw => sw.SemesterID == semesterID)
					.Where(sw => sw.Program.InstituteID == loginHistory.InstituteID);
				if (programID != null)
					scholarshipWorkloadsQuery = scholarshipWorkloadsQuery.Where(sw => sw.ProgramID == programID.Value);
				var scholarshipWorkloads = scholarshipWorkloadsQuery.Select(sw => new
				{
					sw.SemesterID,
					sw.ProgramID,
					sw.Program.ProgramAlias,
					sw.SemesterNo,
					sw.CreditHours
				}).ToList();

				return scholarshipWorkloads
					.Select(sw => new
					{
						sw.SemesterID,
						sw.ProgramID,
						sw.ProgramAlias,
						sw.SemesterNo
					})
					.Union(workload.Select(w => new
					{
						w.SemesterID,
						w.ProgramID,
						w.ProgramAlias,
						w.SemesterNo
					}))
					.Distinct()
					.Select(w => new ScholarshipWorkload
					{
						SemesterID = w.SemesterID,
						ProgramID = w.ProgramID,
						ProgramAlias = w.ProgramAlias,
						SemesterNo = w.SemesterNo,
						OfferedCreditHours = workload
												 .Where(sw => sw.SemesterID == w.SemesterID && sw.ProgramID == w.ProgramID && sw.SemesterNo == w.SemesterNo)
												 .Select(sw => (decimal?)sw.CreditHours)
												 .FirstOrDefault() ?? 0M,
						CreditHours = scholarshipWorkloads
							.Where(sw => sw.SemesterID == w.SemesterID && sw.ProgramID == w.ProgramID && sw.SemesterNo == w.SemesterNo)
							.Select(sw => (decimal?)sw.CreditHours)
							.FirstOrDefault(),
					}).ToList();
			}
		}

		public enum UpdateWorkloadsStatuses
		{
			NoRecordFound,
			Success,
		}

		public static UpdateWorkloadsStatuses UpdateWorkloads(short semesterID, Dictionary<int, Dictionary<SemesterNos, decimal?>> workloads, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var scholarshipWorkloads = aspireContext.ScholarshipWorkloads
					.Where(sw => sw.Program.InstituteID == loginHistory.InstituteID && sw.SemesterID == semesterID)
					.ToList();
				var programs = aspireContext.Programs
					.Where(p => p.InstituteID == loginHistory.InstituteID)
					.Select(p => new
					{
						p.InstituteID,
						p.DepartmentID,
						p.ProgramID,
					}).ToList();

				foreach (var program in workloads)
				{
					var prog = programs.SingleOrDefault(p => p.ProgramID == program.Key);
					if (prog == null)
						return UpdateWorkloadsStatuses.NoRecordFound;
					aspireContext.DemandPermissions(loginSessionGuid, prog.InstituteID, prog.DepartmentID, prog.ProgramID);
					foreach (var semester in program.Value)
					{
						var scholarshipWorkload = scholarshipWorkloads.SingleOrDefault(sw => sw.ProgramID == program.Key && sw.SemesterNo == (short)semester.Key);
						if (semester.Value == null || semester.Value.Value == 0)
						{
							if (scholarshipWorkload != null)
								aspireContext.ScholarshipWorkloads.Remove(scholarshipWorkload);
						}
						else
						{
							if (scholarshipWorkload != null)
								scholarshipWorkload.CreditHours = semester.Value.Value;
							else
								aspireContext.ScholarshipWorkloads.Add(new Model.Entities.ScholarshipWorkload
								{
									ProgramID = program.Key,
									SemesterID = semesterID,
									CreditHours = semester.Value.Value,
									SemesterNo = (short)semester.Key,
								});
						}
					}
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateWorkloadsStatuses.Success;
			}
		}

		public sealed class OfferedCourse
		{
			internal OfferedCourse() { }
			internal class Class
			{
				public short SemesterNo { get; set; }
				public int Section { get; set; }
				public byte Shift { get; set; }
			}

			public int OfferedCourseID { get; set; }
			public short SemesterID { get; set; }
			public string CourseCode { get; set; }
			public string Title { get; set; }
			public decimal CreditHours { get; set; }
			public string Majors { get; set; }

			public string Program { get; set; }
			public short SemesterNo { get; set; }
			public int Section { get; set; }
			public byte Shift { get; set; }
			internal List<Class> Classes { get; set; }
			public string ClassNamesHtmlEncoded
			{
				get
				{
					var classes = new List<string> { AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift).HtmlEncode() };
					if (this.Classes != null)
						classes.AddRange(this.Classes.Select(c => $"<small><em>{AspireFormats.GetClassName(this.Program, (short)c.SemesterNo, c.Section, c.Shift).HtmlEncode()}</em></small>"));
					return string.Join("<br />", classes);
				}
			}
		}

		public static List<OfferedCourse> GetOfferedCourses(int programID, short semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.OfferedCourses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(oc => oc.Cours.AdmissionOpenProgram.ProgramID == programID)
					.Where(oc => oc.SemesterID == semesterID)
					.Select(oc => new OfferedCourse
					{
						SemesterID = oc.SemesterID,
						Program = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						SemesterNo = oc.SemesterNo,
						Section = oc.Section,
						Shift = oc.Shift,
						CreditHours = oc.Cours.CreditHours,
						Title = oc.Cours.Title,
						CourseCode = oc.Cours.CourseCode,
						Majors = oc.Cours.ProgramMajor.Majors,
						OfferedCourseID = oc.OfferedCourseID,
						Classes = oc.OfferedCourseClasses.Select(occ => new OfferedCourse.Class
						{
							SemesterNo = occ.SemesterNo,
							Section = occ.Section,
							Shift = occ.Shift
						}).ToList()
					}).ToList().OrderBy(oc => oc.ClassNamesHtmlEncoded).ToList();
			}
		}
	}
}