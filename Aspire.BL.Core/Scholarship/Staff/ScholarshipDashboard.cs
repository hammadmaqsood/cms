﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Scholarship.Staff
{
	public static class ScholarshipDashboard
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class ScholarshipAnnouncement
		{
			internal ScholarshipAnnouncement() { }
			public int ScholarshipAnnouncementID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public int AppliedApplications { get; internal set; }
			public int InProgressApplications { get; internal set; }
			public int Total { get; internal set; }
			public Model.Entities.Scholarship.ScholarshipTypes ScholarshipTypeEnum { get; internal set; }
			public string ScholarshipTypeFullName => this.ScholarshipTypeEnum.ToFullName();
			public DateTime OpeningDate { get; internal set; }
			public DateTime? ClosingDate { get; internal set; }
		}

		public static List<ScholarshipAnnouncement> GetScholarshipApplications(short semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var scholarshipAnnouncement = aspireContext.ScholarshipAnnouncements
					.Where(sa => sa.Scholarship.InstituteID == loginHistory.InstituteID && sa.SemesterID == semesterID)
					.Select(sa => new ScholarshipAnnouncement
					{
						ScholarshipAnnouncementID = sa.ScholarshipAnnouncementID,
						SemesterID = sa.SemesterID,
						OpeningDate = sa.StartDate,
						ClosingDate = sa.EndDate,
						ScholarshipTypeEnum = (Model.Entities.Scholarship.ScholarshipTypes)sa.Scholarship.ScholarshipType,
						AppliedApplications = sa.ScholarshipApplications.Count(sapp => sapp.ScholarshipAnnouncementID == sa.ScholarshipAnnouncementID && sapp.Status == (byte)Model.Entities.ScholarshipApplication.Statuses.Submitted),
						InProgressApplications = sa.ScholarshipApplications.Count(sapp => sapp.ScholarshipAnnouncementID == sa.ScholarshipAnnouncementID && sapp.Status == (byte)Model.Entities.ScholarshipApplication.Statuses.InProgress),
						Total = sa.ScholarshipApplications.Count(sapp => sapp.ScholarshipAnnouncementID == sa.ScholarshipAnnouncementID),
					}).ToList();
				return scholarshipAnnouncement;
			}
		}
	}
}
