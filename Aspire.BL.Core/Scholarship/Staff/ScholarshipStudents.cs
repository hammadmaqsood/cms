﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Scholarship.Staff
{
	public static class ScholarshipStudents
	{
		private static Aspire.BL.Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Aspire.BL.Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.CompileMerit, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, null);
		}

		public enum CalculateAllResults
		{
			Success,
			AlreadyCalculated,
		}

		private static readonly object ScholarshipLock = new object();
		public static CalculateAllResults CalculateAll(short semesterID, bool overwrite, Guid loginSessionGuid)
		{
			lock (ScholarshipLock)
			{
				using (var aspireContext = new AspireContext(true))
				{
					var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
					loginHistory = aspireContext.DemandPermissions(loginSessionGuid, loginHistory.InstituteID, null, null);

					var scholarshipStudentsQuery = aspireContext.ScholarshipStudents.Where(ss => ss.SemesterID == semesterID && ss.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
					if (scholarshipStudentsQuery.Any())
					{
						if (overwrite == false)
							return CalculateAllResults.AlreadyCalculated;
						aspireContext.ScholarshipStudents.RemoveRange(scholarshipStudentsQuery);
					}

					var studentSemesters = aspireContext.StudentSemesters
						.Where(s => s.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
						.Select(ss => new
						{
							ss.StudentID,
							ss.Student.AdmissionOpenProgram.ProgramID,
							ss.SemesterID,
							ss.Student.Enrollment,
							ss.Student.Name,
							StudentProgramShortName = ss.Student.AdmissionOpenProgram.Program.ProgramShortName,
							StudentProgramAlias = ss.Student.AdmissionOpenProgram.Program.ProgramAlias,
							StudentSemesterSemesterNo = ss.SemesterNo,
						}).ToList();

					var registeredCourses = aspireContext.RegisteredCourses
						.Where(rc => rc.DeletedDate == null)
						.Where(rc => rc.OfferedCours.SemesterID == semesterID)
						.Where(rc => rc.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
						.Select(rc => new
						{
							rc.StudentID,
							rc.OfferedCours.SemesterID,
							OfferedCourseProgramAlias = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
							OfferedCourseSemesterNo = rc.OfferedCours.SemesterNo,
							rc.Cours.CreditHours,
							rc.Total,
							rc.Grade,
							rc.GradePoints,
							rc.Product,
						}).ToList();

					var joined = from rc in registeredCourses
								 join ss in studentSemesters on new { rc.StudentID, rc.SemesterID } equals new { ss.StudentID, ss.SemesterID }
								 select new
								 {
									 ss.StudentID,
									 ss.ProgramID,
									 ss.SemesterID,
									 ss.Enrollment,
									 ss.Name,
									 ss.StudentProgramShortName,
									 ss.StudentProgramAlias,
									 ss.StudentSemesterSemesterNo,
									 rc.OfferedCourseProgramAlias,
									 rc.OfferedCourseSemesterNo,
									 rc.CreditHours,
									 rc.Total,
									 rc.Grade,
									 rc.GradePoints,
									 rc.Product,
								 };

					var scholarshipStudents = joined.GroupBy(j => new
					{
						j.StudentID,
						j.SemesterID,
					}).Select(g =>
					{
						var rCourses = g.ToList();
						var rCoursesGraded = rCourses.Where(rc => rc.Grade != null).ToList();
						var rCoursesNotGraded = rCourses.Where(rc => rc.Grade == null).ToList();

						decimal? percentage;
						short? semesterNo;
						string offeredClasses;
						bool regular;
						decimal? gpa;
						decimal? gpaCreditHours;
						var registeredCreditHours = rCourses.Sum(rc => rc.CreditHours);
						var registeredCoursesNotGraded = rCoursesNotGraded.Count;

						if (rCoursesGraded.Any())
						{
							percentage = rCoursesGraded.Sum(rc => rc.Total ?? 0) * 1M / rCoursesGraded.Count;
							gpaCreditHours = rCoursesGraded.Sum(rc => rc.CreditHours);
							if (gpaCreditHours == 0)
								gpa = 0;
							else
								gpa = rCoursesGraded.Sum(rc => rc.Product ?? 0) / gpaCreditHours;
						}
						else
						{
							percentage = null;
							gpaCreditHours = null;
							gpa = null;
						}

						var classes = rCourses.GroupBy(rc => new { rc.OfferedCourseProgramAlias, rc.OfferedCourseSemesterNo, GradeAwarded = rc.Grade != null })
							.Select(rc => new
							{
								rc.Key.OfferedCourseProgramAlias,
								rc.Key.OfferedCourseSemesterNo,
								rc.Key.GradeAwarded,
								Class = AspireFormats.GetClassName(rc.Key.OfferedCourseProgramAlias, rc.Key.OfferedCourseSemesterNo, null, null),
							})
							.ToList();
						switch (classes.Count(c => c.GradeAwarded))
						{
							case 1:
								semesterNo = classes.Single(c => c.GradeAwarded).OfferedCourseSemesterNo;
								offeredClasses = classes.Single(c => c.GradeAwarded).Class;
								regular = true;
								break;
							default:
								semesterNo = null;
								offeredClasses = string.Join(", ", classes.Select(c => c.Class).Distinct());
								regular = false;
								break;
						}

						var scholarshipStudent = new Model.Entities.ScholarshipStudent
						{
							StudentID = g.Key.StudentID,
							SemesterID = g.Key.SemesterID,
							RegisteredCourses = rCourses.Count,
							SemesterNo = semesterNo,
							Regular = regular,
							Percentage = percentage,
							Excluded = false,
							OfferedClasses = offeredClasses,
							GPA = gpa,
							GPACreditHours = gpaCreditHours,
							RegisteredCoursesNotGraded = registeredCoursesNotGraded,
							RegisteredCreditHours = registeredCreditHours,
							Position = null,
							Remarks = null,
							WorkloadCreditHours = null,
						};
						return new { rCourses.First().ProgramID, ScholarshipStudent = scholarshipStudent };
					}).ToList();

					var scholarshipWorkloads = aspireContext.ScholarshipWorkloads
						.Where(sw => sw.SemesterID == semesterID && sw.Program.InstituteID == loginHistory.InstituteID)
						.Select(sw => new
						{
							sw.ProgramID,
							sw.SemesterID,
							sw.CreditHours,
						}).ToList();

					(from ss in scholarshipStudents
					 join sw in scholarshipWorkloads on new { ss.ProgramID, ss.ScholarshipStudent.SemesterID } equals new { sw.ProgramID, sw.SemesterID }
					 select new { ss, sw }).ForEach(x => x.ss.ScholarshipStudent.WorkloadCreditHours = x.sw.CreditHours);
					aspireContext.ScholarshipStudents.AddRange(scholarshipStudents.Select(ss => ss.ScholarshipStudent));
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
					aspireContext.UpdatePositions(semesterID, loginHistory.UserLoginHistoryID);
					aspireContext.CommitTransaction();
					return CalculateAllResults.Success;
				}
			}
		}

		private static void UpdatePositions(this AspireContext aspireContext, short semesterID, int userLoginHistoryID)
		{
			var scholarshipStudents = aspireContext.ScholarshipStudents
				.Where(ss => ss.SemesterID == semesterID)
				.Select(ss => new
				{
					ss.Student.AdmissionOpenProgram.ProgramID,
					ScholarshipStudent = ss,
				}).ToList();

			scholarshipStudents.ForEach(ss => ss.ScholarshipStudent.Position = null);

			scholarshipStudents = scholarshipStudents
					.Where(ss => ss.ScholarshipStudent.Excluded == false)
					.Where(ss => ss.ScholarshipStudent.GPA != null)
					.Where(ss => ss.ScholarshipStudent.Percentage != null)
					.Where(ss => ss.ScholarshipStudent.WorkloadCreditHours != null)
					.Where(ss => ss.ScholarshipStudent.GPACreditHours != null)
					.Where(ss => ss.ScholarshipStudent.WorkloadCreditHours <= ss.ScholarshipStudent.GPACreditHours)
					.ToList();

			scholarshipStudents.GroupBy(ss => new
			{
				ss.ProgramID,
				ss.ScholarshipStudent.SemesterNo,
			})
				.Select(g => new
				{
					g.Key.ProgramID,
					g.Key.SemesterNo,
					ScholarshipStudents = g.ToList()
				})
				.ForEach(x =>
				{
					var position = 1;
					x.ScholarshipStudents
						.GroupBy(g => new
						{
							g.ScholarshipStudent.GPA,
							Percentage = g.ScholarshipStudent.GPA == 4m && g.ScholarshipStudent.Percentage != null ? 100M : g.ScholarshipStudent.Percentage
						})
						.OrderByDescending(g => g.Key.GPA)
						.ThenByDescending(g => g.Key.Percentage)
						.ForEach(g =>
						{
							var pos = position++;
							g.ForEach(s => s.ScholarshipStudent.Position = pos);
						});
				});

			aspireContext.SaveChanges(userLoginHistoryID);
		}

		public sealed class RegisteredCourse
		{
			public string Enrollment { get; internal set; }
			public string Title { get; internal set; }
			public string Major { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public byte? Total { get; internal set; }
			public byte? Grade { get; internal set; }
			public string GradeFullName => ((ExamGrades?)this.Grade)?.ToFullName();
			public byte? Status { get; internal set; }
			public byte? FreezedStatus { get; internal set; }
			public string StatusFullName => AspireFormats.GetRegisteredCourseStatusString(this.Status, this.FreezedStatus);
			public decimal? GradePoints { get; internal set; }
			public decimal? Product { get; internal set; }
			public int RegisteredCourseID { get; internal set; }
		}

		public sealed class ScholarshipStudent
		{
			internal ScholarshipStudent()
			{
			}
			public int ScholarshipStudentID { get; internal set; }
			public int StudentID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public int RegisteredCoursesCount { get; internal set; }
			public short? SemesterNo { get; internal set; }
			public string SemesterNoFullName => ((SemesterNos?)this.SemesterNo)?.ToFullName();
			public bool Regular { get; internal set; }
			public string RegularYesNo => this.Regular.ToYesNo();
			public decimal? Percentage { get; internal set; }
			public bool Excluded { get; internal set; }
			public string ExcludedYesNo => this.Excluded.ToYesNo();
			public string OfferedClasses { get; internal set; }
			public decimal? GPA { get; internal set; }
			public decimal? GPACreditHours { get; internal set; }
			public int RegisteredCoursesNotGraded { get; internal set; }
			public decimal? WorkloadCreditHours { get; internal set; }
			public decimal RegisteredCreditHours { get; internal set; }
			public int? Position { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string Name { get; internal set; }
			public List<RegisteredCourse> RegisteredCoursesList { get; internal set; }
			public string Enrollment { get; internal set; }
			public int ProgramID { get; internal set; }
			public string Remarks { get; internal set; }
		}

		private static IOrderedQueryable<ScholarshipStudent> Order<TU>(IQueryable<ScholarshipStudent> students, Expression<Func<ScholarshipStudent, TU>> keySelector, SortDirection sortDirection, bool thenBy)
		{
			return thenBy
						   ? ((IOrderedQueryable<ScholarshipStudent>)students).ThenBy(sortDirection, keySelector)
						   : students.OrderBy(sortDirection, keySelector);
		}

		public static List<ScholarshipStudent> GetScholarshipStudents(short semesterID, int? programID, SemesterNos? semesterNo, decimal? gpa, bool? regular, int? position, string enrollment, string orderBy1, SortDirection sortDirection1, string orderBy2, SortDirection sortDirection2, string orderBy3, SortDirection sortDirection3, string orderBy4, SortDirection sortDirection4, string orderBy5, SortDirection sortDirection5, int pageIndex, int pageSize, out int virtualItemsCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				enrollment = enrollment.TrimAndMakeItNullIfEmpty();
				var scholarshipStudentsQuery = aspireContext.ScholarshipStudents
					.Where(ss => ss.SemesterID == semesterID)
					.Where(ss => ss.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(ss => programID == null || ss.Student.AdmissionOpenProgram.ProgramID == programID.Value)
					.Where(ss => semesterNo == null || ss.SemesterNo == (short)semesterNo.Value)
					.Where(ss => gpa == null || ss.GPA >= gpa.Value)
					.Where(ss => regular == null || ss.Regular == regular.Value)
					.Where(ss => position == null || ss.Position == position.Value)
					.Where(ss => enrollment == null || ss.Student.Enrollment == enrollment)
					.Select(ss => new ScholarshipStudent
					{
						ScholarshipStudentID = ss.ScholarshipStudentID,
						StudentID = ss.StudentID,
						Enrollment = ss.Student.Enrollment,
						Name = ss.Student.Name,
						ProgramID = ss.Student.AdmissionOpenProgram.ProgramID,
						ProgramAlias = ss.Student.AdmissionOpenProgram.Program.ProgramAlias,
						SemesterID = ss.SemesterID,
						RegisteredCoursesCount = ss.RegisteredCourses,
						SemesterNo = ss.SemesterNo,
						Regular = ss.Regular,
						Percentage = ss.Percentage,
						Excluded = ss.Excluded,
						OfferedClasses = ss.OfferedClasses,
						GPA = ss.GPA,
						GPACreditHours = ss.GPACreditHours,
						RegisteredCoursesNotGraded = ss.RegisteredCoursesNotGraded,
						RegisteredCreditHours = ss.RegisteredCreditHours,
						WorkloadCreditHours = ss.WorkloadCreditHours,
						Position = ss.Position,
						Remarks = ss.Remarks,
						RegisteredCoursesList = ss.Student.RegisteredCourses
							.Where(rc => rc.DeletedDate == null && rc.OfferedCours.SemesterID == ss.SemesterID)
							.Select(rc => new RegisteredCourse
							{
								Enrollment = rc.Student.Enrollment,
								RegisteredCourseID = rc.RegisteredCourseID,
								Title = rc.Cours.Title,
								Major = rc.Cours.ProgramMajor.Majors,
								CreditHours = rc.Cours.CreditHours,
								Total = rc.Total,
								Grade = rc.Grade,
								GradePoints = rc.GradePoints,
								Product = rc.Product,
								Status = rc.Status,
								FreezedStatus = rc.FreezedStatus,
							}).ToList()
					});
				virtualItemsCount = scholarshipStudentsQuery.Count();
				var sort = new Func<IQueryable<ScholarshipStudent>, string, SortDirection, bool, IOrderedQueryable<ScholarshipStudent>>(delegate (IQueryable<ScholarshipStudent> students, string orderBy, SortDirection sortDirection, bool thenBy)
				 {
					 switch (orderBy)
					 {
						 case nameof(ScholarshipStudent.ProgramAlias):
							 return Order(students, s => s.ProgramAlias, sortDirection, thenBy);
						 case nameof(ScholarshipStudent.SemesterNo):
							 return Order(students, s => s.SemesterNo, sortDirection, thenBy);
						 case nameof(ScholarshipStudent.GPA):
							 return Order(students, s => s.GPA, sortDirection, thenBy);
						 case nameof(ScholarshipStudent.Percentage):
							 return Order(students, s => s.Percentage, sortDirection, thenBy);
						 case nameof(ScholarshipStudent.Enrollment):
							 return Order(students, s => s.Enrollment, sortDirection, thenBy);
						 default:
							 throw new SortingNotImplementedException(orderBy);
					 }
				 });

				scholarshipStudentsQuery = sort(scholarshipStudentsQuery, orderBy1, sortDirection1, false);
				scholarshipStudentsQuery = sort(scholarshipStudentsQuery, orderBy2, sortDirection2, true);
				scholarshipStudentsQuery = sort(scholarshipStudentsQuery, orderBy3, sortDirection3, true);
				scholarshipStudentsQuery = sort(scholarshipStudentsQuery, orderBy4, sortDirection4, true);
				scholarshipStudentsQuery = sort(scholarshipStudentsQuery, orderBy5, sortDirection5, true);
				return scholarshipStudentsQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList();
			}
		}

		public static ScholarshipStudent GetScholarshipStudent(int scholarshipStudentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.ScholarshipStudents
					.Where(ss => ss.ScholarshipStudentID == scholarshipStudentID)
					.Where(ss => ss.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Select(ss => new ScholarshipStudent
					{
						ScholarshipStudentID = ss.ScholarshipStudentID,
						StudentID = ss.StudentID,
						Enrollment = ss.Student.Enrollment,
						Name = ss.Student.Name,
						ProgramID = ss.Student.AdmissionOpenProgram.ProgramID,
						ProgramAlias = ss.Student.AdmissionOpenProgram.Program.ProgramAlias,
						SemesterID = ss.SemesterID,
						RegisteredCoursesCount = ss.RegisteredCourses,
						SemesterNo = ss.SemesterNo,
						Regular = ss.Regular,
						Percentage = ss.Percentage,
						Excluded = ss.Excluded,
						OfferedClasses = ss.OfferedClasses,
						GPA = ss.GPA,
						GPACreditHours = ss.GPACreditHours,
						RegisteredCoursesNotGraded = ss.RegisteredCoursesNotGraded,
						WorkloadCreditHours = ss.WorkloadCreditHours,
						RegisteredCreditHours = ss.RegisteredCreditHours,
						Position = ss.Position,
						Remarks = ss.Remarks,
						RegisteredCoursesList = ss.Student.RegisteredCourses
							.Where(rc => rc.DeletedDate == null && rc.OfferedCours.SemesterID == ss.SemesterID)
							.Select(rc => new RegisteredCourse
							{
								Enrollment = rc.Student.Enrollment,
								RegisteredCourseID = rc.RegisteredCourseID,
								Title = rc.Cours.Title,
								Major = rc.Cours.ProgramMajor.Majors,
								CreditHours = rc.Cours.CreditHours,
								Total = rc.Total,
								Grade = rc.Grade,
								GradePoints = rc.GradePoints,
								Product = rc.Product,
								Status = rc.Status,
								FreezedStatus = rc.FreezedStatus,
							}).ToList(),
					}).SingleOrDefault();
			}
		}

		public enum UpdateScholarshipStudentsStatuses
		{
			NoRecordFound,
			Success,
		}

		public static UpdateScholarshipStudentsStatuses UpdateScholarshipStudents(int scholarshipStudentID, bool excluded, string remarks, Guid loginSessionGuid)
		{
			lock (ScholarshipLock)
			{
				using (var aspireContext = new AspireContext(true))
				{
					var record = aspireContext.ScholarshipStudents.Where(ss => ss.ScholarshipStudentID == scholarshipStudentID)
						.Select(ss => new
						{
							ScholarshipStudent = ss,
							ss.Student.AdmissionOpenProgram.Program.InstituteID,
							ss.Student.AdmissionOpenProgram.Program.DepartmentID,
							ss.Student.AdmissionOpenProgram.ProgramID,
							ss.Student.AdmissionOpenProgramID,
						}).SingleOrDefault();
					if (record == null)
						return UpdateScholarshipStudentsStatuses.NoRecordFound;
					var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID);
					record.ScholarshipStudent.Excluded = excluded;
					record.ScholarshipStudent.Remarks = remarks.TrimAndMakeItNullIfEmpty();
					if (excluded)
						record.ScholarshipStudent.Position = null;
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
					aspireContext.UpdatePositions(record.ScholarshipStudent.SemesterID, loginHistory.UserLoginHistoryID);
					aspireContext.CommitTransaction();
					return UpdateScholarshipStudentsStatuses.Success;
				}
			}
		}
	}
}