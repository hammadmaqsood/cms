﻿using System;
using System.Linq;
using System.Data.Entity;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.BL.Core.Common.Permissions;

namespace Aspire.BL.Core.Scholarship.Common.QarzeHasana
{
	public static class ScholarshipApplicationApproval
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.QarzeHasanaModule, UserGroupPermission.PermissionValues.Allowed);
		}

		public enum AddApplicationApprovalStatuses
		{
			AlreadyExists,
			MaxSemestersExceeded,
			FirstScholarshipApplicationIsPendingOrRejected,
			Pending,
			PrevousSemesterSelected,
			TutionFeeAndApprovalEmpty,
			Success,
		}

		public sealed class GetAddApplicationApprovalResult
		{
			public AddApplicationApprovalStatuses Status { get; set; }
		}

		internal static GetAddApplicationApprovalResult GetAddApplicationApproval(this AspireContext aspireContext, int scholarshipApplicationID,
				int semesterID, int? tutionFee, int? campusRecommendation, string campusRemarks, int? CSCRecommendation,
				string CSCRemarks, int? finalRecommendation, Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses approvalStatusesEnum)
		{
			var getLastScholarshipApplicationApproval = GetLastScholarshipApplicationApproval(scholarshipApplicationID);
			var getFirstScholarshipApplicationApproval = GetFirstScholarshipApplicationApproval(scholarshipApplicationID);

			if (getFirstScholarshipApplicationApproval.ApprovalStatusEnum != Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Approved)
				return new GetAddApplicationApprovalResult { Status = AddApplicationApprovalStatuses.FirstScholarshipApplicationIsPendingOrRejected };

			if (getLastScholarshipApplicationApproval.SemesterID == semesterID)
				return new GetAddApplicationApprovalResult { Status = AddApplicationApprovalStatuses.AlreadyExists };

			if (getLastScholarshipApplicationApproval.ScholarshipApplication.Student.AdmissionOpenProgram.FinalSemesterID < semesterID)
				return new GetAddApplicationApprovalResult { Status = AddApplicationApprovalStatuses.MaxSemestersExceeded };

			if (getLastScholarshipApplicationApproval.SemesterID > semesterID)
				return new GetAddApplicationApprovalResult { Status = AddApplicationApprovalStatuses.PrevousSemesterSelected };

			if (approvalStatusesEnum == Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Approved)
				if (tutionFee == null || finalRecommendation == null)
					return new GetAddApplicationApprovalResult { Status = AddApplicationApprovalStatuses.TutionFeeAndApprovalEmpty };

			var applicationApproval = new Schemas.ScholarshipApplicationApproval();
			applicationApproval.UpdateApplicationApproval(tutionFee, campusRecommendation, campusRemarks, CSCRecommendation, CSCRemarks, finalRecommendation);

			var scholarshipApplicationApproval = new Model.Entities.ScholarshipApplicationApproval
			{
				ScholarshipApplicationID = scholarshipApplicationID,
				SemesterID = (short)semesterID,
				Data = applicationApproval.ToJson(),
				ApprovalDate = DateTime.Now,
				ApprovalStatusEnum = approvalStatusesEnum,
				ChequeStatus = (byte)Model.Entities.ScholarshipApplicationApproval.ChequeStatuses.No,
			};
			aspireContext.ScholarshipApplicationApprovals.Add(scholarshipApplicationApproval);
			return new GetAddApplicationApprovalResult { Status = AddApplicationApprovalStatuses.Success };
		}

		public static AddApplicationApprovalStatuses AddApplicationApproval(int scholarshipApplicationID, int? tutionFee,
			int? campusRecommendation, string campusRemarks, int? CSCRecommendation, string CSCRemarks, int? finalRecommendation,
			Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses approvalStatusesEnum, int semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.CanManageQarzeHasanaDSARecommendations, UserGroupPermission.PermissionValues.Allowed);

				var getApplicationApprovalForAdd = aspireContext.GetAddApplicationApproval(scholarshipApplicationID, semesterID, tutionFee,
					campusRecommendation, campusRemarks, CSCRecommendation, CSCRemarks, finalRecommendation, approvalStatusesEnum);

				switch (getApplicationApprovalForAdd.Status)
				{
					case AddApplicationApprovalStatuses.AlreadyExists:
					case AddApplicationApprovalStatuses.MaxSemestersExceeded:
					case AddApplicationApprovalStatuses.Pending:
					case AddApplicationApprovalStatuses.PrevousSemesterSelected:
					case AddApplicationApprovalStatuses.TutionFeeAndApprovalEmpty:
					case AddApplicationApprovalStatuses.FirstScholarshipApplicationIsPendingOrRejected:
						return getApplicationApprovalForAdd.Status;
					case AddApplicationApprovalStatuses.Success:
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return AddApplicationApprovalStatuses.Success;
					default:
						throw new NotImplementedEnumException(getApplicationApprovalForAdd.Status);
				}
			}
		}

		public enum UpdateApplicationApprovalStatuses
		{
			NoRecordFound,
			TutionFeeAndApprovalEmpty,
			NotAllowedChangesInApprovedOrRejectedApplication,
			Success,
		}

		private sealed class GetApplicationApprovalForEditResult
		{
			public UpdateApplicationApprovalStatuses Status { get; set; }
			public Model.Entities.ScholarshipApplicationApproval ApplicationApproval { get; set; }
		}

		private static GetApplicationApprovalForEditResult GetApplicationApprovalForEdit(this AspireContext aspireContext, int scholarshipApplicationApprovalID, int? tutionFee, int? campusRecommendation, string campusRemarks, int? CSCRecommendation,
			string CSCRemarks, int? finalRecommendation, Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses approvalStatusesEnum, Guid loginSessionGuid)
		{
			var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

			var studentInfo = aspireContext.ScholarshipApplicationApprovals
				.Where(sa => sa.ScholarshipApplicationApprovalID == scholarshipApplicationApprovalID)
				.Select(sa => new
				{
					ScholarshipApplicationApproval = sa,
					sa.ScholarshipApplication.Student.AdmissionOpenProgram.Program.InstituteID,
					sa.ScholarshipApplication.Student.AdmissionOpenProgram.Program.DepartmentID,
					sa.ScholarshipApplication.Student.AdmissionOpenProgram.ProgramID,
					sa.ScholarshipApplication.Student.AdmissionOpenProgramID,
				}).SingleOrDefault();

			if (studentInfo == null)
				return new GetApplicationApprovalForEditResult { Status = UpdateApplicationApprovalStatuses.NoRecordFound };

			if (studentInfo.ScholarshipApplicationApproval.ApprovalStatus != (byte)Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Pending
				&& approvalStatusesEnum != Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Pending)
				return new GetApplicationApprovalForEditResult { Status = UpdateApplicationApprovalStatuses.NotAllowedChangesInApprovedOrRejectedApplication };

			var applicationApproval = Schemas.ScholarshipApplicationApproval.FromJson(studentInfo.ScholarshipApplicationApproval.Data);

			var feeManagement = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.CanManageQarzeHasnaStudentsFeeDetails, UserGroupPermission.PermissionValues.Allowed, studentInfo.InstituteID, studentInfo.DepartmentID, studentInfo.ProgramID, studentInfo.AdmissionOpenProgramID);
			var campusManagement = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.CanManageQarzeHasanaCampusRecommendations, UserGroupPermission.PermissionValues.Allowed, studentInfo.InstituteID, studentInfo.DepartmentID, studentInfo.ProgramID, studentInfo.AdmissionOpenProgramID);
			var CSCManagement = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.CanManageQarzeHasanaFinalRecommendations, UserGroupPermission.PermissionValues.Allowed, studentInfo.InstituteID, studentInfo.DepartmentID, studentInfo.ProgramID, studentInfo.AdmissionOpenProgramID);
			var DSA = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.CanManageQarzeHasanaDSARecommendations, UserGroupPermission.PermissionValues.Allowed, studentInfo.InstituteID, studentInfo.DepartmentID, studentInfo.ProgramID, studentInfo.AdmissionOpenProgramID);

			if (CSCManagement != null || DSA != null)
			{
				if (approvalStatusesEnum == Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Approved && (finalRecommendation == null || tutionFee == null))
					return new GetApplicationApprovalForEditResult { Status = UpdateApplicationApprovalStatuses.TutionFeeAndApprovalEmpty };

				if (feeManagement != null && campusManagement != null && CSCManagement != null && DSA != null)
					applicationApproval.UpdateApplicationApproval(tutionFee, campusRecommendation, campusRemarks, CSCRecommendation, CSCRemarks, finalRecommendation);
				else
					applicationApproval.UpdateApplicationApproval(applicationApproval.TuitionFee, applicationApproval.CampusRecommendation, applicationApproval.CampusRemarks, CSCRecommendation, CSCRemarks, finalRecommendation);
				studentInfo.ScholarshipApplicationApproval.ApprovalStatus = (byte)approvalStatusesEnum;
			}
			if (feeManagement != null && applicationApproval.TuitionFee != tutionFee && approvalStatusesEnum == Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Pending)
			{
				applicationApproval.UpdateApplicationApproval(tutionFee, applicationApproval.CampusRecommendation, applicationApproval.CampusRemarks, applicationApproval.CSCRecommendation, applicationApproval.CSCRemarks, applicationApproval.FinalRecommendation);
			}
			if (campusManagement != null && (applicationApproval.CampusRemarks != campusRemarks || applicationApproval.CampusRecommendation != campusRecommendation) && approvalStatusesEnum == Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Pending)
			{
				applicationApproval.UpdateApplicationApproval(applicationApproval.TuitionFee, campusRecommendation, campusRemarks, applicationApproval.CSCRecommendation, applicationApproval.CSCRemarks, applicationApproval.FinalRecommendation);
			}			

			studentInfo.ScholarshipApplicationApproval.Data = applicationApproval.ToJson();
			return new GetApplicationApprovalForEditResult
			{
				ApplicationApproval = studentInfo.ScholarshipApplicationApproval,
				Status = UpdateApplicationApprovalStatuses.Success,
			};
		}

		public static UpdateApplicationApprovalStatuses UpdateApplicationApproval(int applicationApprovalID, int scholarshipApplicationID,
			int? tuitionFee, int? campusRecommendation, string campusRemarks, int? CSCRecommendation, string CSCRemarks, int? finalRecommendation,
			Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses approvalStatusesEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getApplicationApprovalForEdit = aspireContext.GetApplicationApprovalForEdit(applicationApprovalID, tuitionFee, campusRecommendation, campusRemarks, CSCRecommendation, CSCRemarks, finalRecommendation, approvalStatusesEnum, loginSessionGuid);
				switch (getApplicationApprovalForEdit.Status)
				{
					case UpdateApplicationApprovalStatuses.NoRecordFound:
					case UpdateApplicationApprovalStatuses.TutionFeeAndApprovalEmpty:
					case UpdateApplicationApprovalStatuses.NotAllowedChangesInApprovedOrRejectedApplication:
						return getApplicationApprovalForEdit.Status;
					case UpdateApplicationApprovalStatuses.Success:
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return UpdateApplicationApprovalStatuses.Success;
					default:
						throw new NotImplementedEnumException(getApplicationApprovalForEdit.Status);
				}
			}
		}

		public enum DeleteApplicationApprovalStatuses
		{
			NoRecordFound,
			FieldsAreNotEmpty,
			NotAllowedToDeleteFirstSemesterApplication,
			Success,
		}

		public static DeleteApplicationApprovalStatuses DeleteApplicationApproval(int applicationApprovalID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.CanManageQarzeHasanaDSARecommendations, UserGroupPermission.PermissionValues.Allowed);
				var scholarshipApplicationApproval = aspireContext.ScholarshipApplicationApprovals
							.Where(sa => sa.ScholarshipApplicationApprovalID == applicationApprovalID).SingleOrDefault();

				if (scholarshipApplicationApproval == null)
					return DeleteApplicationApprovalStatuses.NoRecordFound;

				var data = Schemas.ScholarshipApplicationApproval.FromJson(scholarshipApplicationApproval.Data);

				if (data.CSCRecommendation != null || (data.CSCRemarks.ToNullIfWhiteSpace() != null) || data.CampusRecommendation != null
					|| (data.CampusRemarks.ToNullIfWhiteSpace() != null) || data.FinalRecommendation != null || data.TuitionFee != null)
					return DeleteApplicationApprovalStatuses.FieldsAreNotEmpty;

				var firstScholarshipApplicationResults = GetFirstScholarshipApplicationApproval(scholarshipApplicationApproval.ScholarshipApplicationID);
				if (firstScholarshipApplicationResults.SemesterID == scholarshipApplicationApproval.SemesterID)
					return DeleteApplicationApprovalStatuses.NotAllowedToDeleteFirstSemesterApplication;

				aspireContext.ScholarshipApplicationApprovals.Remove(scholarshipApplicationApproval);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteApplicationApprovalStatuses.Success;
			}
		}

		public enum TextBoxEnabledStatus
		{
			EnabledTextBoxForFirstScholarshipApplication,
			DisableTextBox,
		}

		public static TextBoxEnabledStatus TextBoxEnableDisable(int scholarsipApplicationID, short SemesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getFirstScholarshipApplicationApproval = GetFirstScholarshipApplicationApproval(scholarsipApplicationID);
				if (getFirstScholarshipApplicationApproval.SemesterID == SemesterID)
					return TextBoxEnabledStatus.EnabledTextBoxForFirstScholarshipApplication;
				return TextBoxEnabledStatus.DisableTextBox;
			}
		}

		public sealed class ApplicationApprovalInfoResult
		{
			public int ScholarshipApplicationApprovalID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Data { get; internal set; }
			public Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses ApprovalStatusEnum { get; internal set; }
			public Model.Entities.ScholarshipApplicationApproval.ChequeStatuses ChequeStatusEnum { get; internal set; }
		}

		public static ApplicationApprovalInfoResult GetApplicationApprovalInfo(int scholarshipApplicationApprovalID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				return aspireContext.ScholarshipApplicationApprovals
					.Where(saa => saa.ScholarshipApplicationApprovalID == scholarshipApplicationApprovalID)
					.Select(sa => new ApplicationApprovalInfoResult
					{
						SemesterID = sa.SemesterID,
						Data = sa.Data,
						ApprovalStatusEnum = (Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses)sa.ApprovalStatus,
						ChequeStatusEnum = (Model.Entities.ScholarshipApplicationApproval.ChequeStatuses)sa.ChequeStatus,
					}).SingleOrDefault();
			}
		}

		public static Model.Entities.ScholarshipApplicationApproval GetFirstScholarshipApplicationApproval(int scholarshipApplicationID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.ScholarshipApplicationApprovals
					.Where(saa => saa.ScholarshipApplicationID == scholarshipApplicationID)
					.OrderBy(saa => saa.SemesterID).First();
			}
		}

		public static Model.Entities.ScholarshipApplicationApproval GetLastScholarshipApplicationApproval(int scholarshipApplicationID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.ScholarshipApplicationApprovals
					.Where(saa => saa.ScholarshipApplicationID == scholarshipApplicationID)
					.Include(s => s.ScholarshipApplication.Student.AdmissionOpenProgram)
					.OrderByDescending(saa => saa.SemesterID).First();
			}
		}

		public static Aspire.Model.Entities.ScholarshipApplicationApproval GetApprovalForMigratingData(int studentID)
		{
			using (var aspireContext = new AspireContext())
			{
				var Approval = aspireContext.ScholarshipApplicationApprovals.Where(saa => saa.ScholarshipApplication.StudentID == studentID).Where(saa =>
					saa.ScholarshipApplication.ScholarshipApplicationID == saa.ScholarshipApplicationID).FirstOrDefault();
				if (Approval != null)
					return Approval;
				return null;
			}
		}
	}
}
