﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.Scholarship.Common.QarzeHasana.Report
{
	public static class QarzeHasanaForm
	{
		public class PageHeader
		{
			public string Institute { get; internal set; }
			public string Title => $"{Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana.ToFullName()} ({this.SemesterID.ToSemesterString()})";
			public short SemesterID { get; internal set; }

			public static List<PageHeader> GetList()
			{
				return null;
			}
		}

		public class ReportDataSet
		{
			internal ReportDataSet()
			{
			}

			public Schemas.QarzeHasana QarzeHasanaForm { get; internal set; }
			public Student StudentInfo { get; internal set; }

			public sealed class Student
			{
				public int StudentID { get; internal set; }
				public string Enrollment { get; internal set; }
				public short AnnouncementSemesterID { get; set; }
				public short IntakeSemesterID { get; set; }
				public string ReportTitle => $"{Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana.ToFullName()} ({this.AnnouncementSemesterID.ToSemesterString()})";
				public string InstituteName { get; internal set; }
				public int? RegistrationNo { get; internal set; }
				public string Name { get; internal set; }
				public string FatherName { get; internal set; }
				public string Phone { get; internal set; }
				public string Mobile { get; internal set; }
				public string UniversityEmail { get; internal set; }
				public string PersonalEmail { get; internal set; }
				public string Email
				{
					get
					{
						if (this.UniversityEmail != null && this.PersonalEmail != null)
							return $"{this.UniversityEmail}, {this.PersonalEmail}";
						return $"{this.UniversityEmail}{this.PersonalEmail}";
					}
				}
				public string CurrentAddress { get; internal set; }
				public string ProgramAlias { get; internal set; }
				internal StudentSemester StudentSemester { get; set; }
				public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.StudentSemester.SemesterNo);
				public StudentSemester LastStudentSemester { get; internal set; }
				public bool IsFirstSemester => this.IntakeSemesterID == this.AnnouncementSemesterID;
				public double? PreviousDegreePercentage { get; internal set; }
				public string LastResult
				{
					get
					{
						if (this.StudentSemester != null && this.StudentSemester.BUExamGPA != null && this.StudentSemester.BUExamCGPA != null)
							return $"GPA: {this.StudentSemester.BUExamGPA:N2}, CGPA: {this.StudentSemester.BUExamCGPA:N2} ({StudentSemester.SemesterID.ToSemesterString()})";

						if (this.LastStudentSemester != null && this.LastStudentSemester.BUExamGPA != null && this.LastStudentSemester.BUExamCGPA != null)
							return $"GPA: {this.LastStudentSemester.BUExamGPA:N2}, CGPA: {this.LastStudentSemester.BUExamCGPA:N2} ({LastStudentSemester.SemesterID.ToSemesterString()})";

						if (this.IsFirstSemester)
							return $"{this.PreviousDegreePercentage:N2}%";
						return $"GPA: N/A, CGPA: N/A";
					}
				}
				public static List<Student> GetList()
				{
					return null;
				}
			}
		}

		public static ReportDataSet GetScholarshipApplicationQarzeHasana(int scholarshipApplicationID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var scholarshipApplicationQuery = aspireContext.ScholarshipApplications
					.Include(sa => sa.ScholarshipAnnouncement.Scholarship.Institute)
					.Where(sa => sa.ScholarshipApplicationID == scholarshipApplicationID);
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.QarzeHasanaModule, UserGroupPermission.PermissionValues.Allowed);
						scholarshipApplicationQuery = scholarshipApplicationQuery.Where(sa => sa.ScholarshipAnnouncement.Scholarship.InstituteID == staffLoginHistory.InstituteID);
						break;
					case UserTypes.Student:
						scholarshipApplicationQuery = scholarshipApplicationQuery.Where(sa => sa.StudentID == loginHistory.StudentID.Value);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				var scholarshipApplication = scholarshipApplicationQuery.SingleOrDefault();
				if (scholarshipApplication == null)
					return null;

				var qarzeHasana = Schemas.QarzeHasana.FromJson(scholarshipApplication.FormData);
				var validateErrors = qarzeHasana.ValidateForSubmission().ToList();
				if (validateErrors.Any())
					return null;

				var studentInfo = aspireContext.Students
					.Where(s => s.StudentID == scholarshipApplication.StudentID)
					.Select(s => new ReportDataSet.Student
					{
						StudentID = s.StudentID,
						InstituteName = s.AdmissionOpenProgram.Program.Institute.InstituteName,
						AnnouncementSemesterID = scholarshipApplication.ScholarshipAnnouncement.SemesterID,
						IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
						PreviousDegreePercentage = s.StudentAcademicRecords
							.OrderByDescending(sar => sar.DegreeType)
							.Select(sar => (double?)sar.Percentage)
							.FirstOrDefault(),
						Enrollment = s.Enrollment,
						Name = s.Name,
						UniversityEmail = s.UniversityEmail,
						PersonalEmail = s.PersonalEmail,
						FatherName = s.FatherName,
						RegistrationNo = s.RegistrationNo,
						ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
						StudentSemester = s.StudentSemesters.FirstOrDefault(ss => ss.SemesterID == scholarshipApplication.ScholarshipAnnouncement.SemesterID && ss.Semester.SemesterType != (byte)SemesterTypes.Summer),
						LastStudentSemester = s.StudentSemesters.OrderByDescending(ss => ss.SemesterID).FirstOrDefault(ss => ss.SemesterID < scholarshipApplication.ScholarshipAnnouncement.SemesterID && ss.Semester.SemesterType != (byte)SemesterTypes.Summer),
						CurrentAddress = s.CurrentAddress,
						Mobile = s.Mobile,
						Phone = s.Phone
					}).Single();

				return new ReportDataSet
				{
					StudentInfo = studentInfo,
					QarzeHasanaForm = qarzeHasana
				};
			}
		}
	}
}