﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.Scholarship.Common.QarzeHasana
{
	public static class ScholarshipApplication
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.QarzeHasanaModule, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandStaffPermissions(this AspireContext aspireContext, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID, Guid loginSessionGuid)
		{
			return aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.CanEditQarzeHasanaApplicationForm, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public enum UpdateScholarshipApplicationStatuses
		{
			NoRecordFound,
			AlreadySubmitted,
			NotAuthorizedForChange,
			ScholarshipsNotOpened,
			Success,
		}

		private sealed class GetScholarshipApplicationForEditResult
		{
			public UpdateScholarshipApplicationStatuses Status { get; set; }
			public Model.Entities.ScholarshipApplication ScholarshipApplication { get; set; }
		}

		private static GetScholarshipApplicationForEditResult GetScholarshipApplicationForEdit(this AspireContext aspireContext, int scholarshipApplicationID, out UserTypes userTypeEnum, out int userLoginHistoryID, Guid loginSessionGuid)
		{
			var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
			userTypeEnum = loginHistory.UserTypeEnum;
			userLoginHistoryID = loginHistory.UserLoginHistoryID;
			switch (loginHistory.UserTypeEnum)
			{
				case UserTypes.Staff:
					var studentInfo = aspireContext.ScholarshipApplications
						.Where(sa => sa.ScholarshipApplicationID == scholarshipApplicationID)
						.Select(sa => new
						{
							ScholarshipAnnouncement = sa,
							sa.Student.AdmissionOpenProgram.Program.InstituteID,
							sa.Student.AdmissionOpenProgram.Program.DepartmentID,
							sa.Student.AdmissionOpenProgram.ProgramID,
							sa.Student.AdmissionOpenProgramID,
						}).SingleOrDefault();

					if (studentInfo != null)
					{
						var permissions = aspireContext.DemandStaffPermissions(studentInfo.InstituteID, studentInfo.DepartmentID, studentInfo.ProgramID, studentInfo.AdmissionOpenProgramID, loginSessionGuid);
						if (permissions == null)
							return new GetScholarshipApplicationForEditResult { Status = UpdateScholarshipApplicationStatuses.NotAuthorizedForChange };
					}
					else
						return new GetScholarshipApplicationForEditResult { Status = UpdateScholarshipApplicationStatuses.NoRecordFound };

					return new GetScholarshipApplicationForEditResult
					{
						ScholarshipApplication = studentInfo.ScholarshipAnnouncement,
						Status = UpdateScholarshipApplicationStatuses.Success,
					};

				case UserTypes.Student:
					var scholarshipApplication = aspireContext.ScholarshipApplications.SingleOrDefault(sa => sa.ScholarshipApplicationID == scholarshipApplicationID && sa.StudentID == loginHistory.StudentID.Value);
					if (scholarshipApplication == null)
						return new GetScholarshipApplicationForEditResult { Status = UpdateScholarshipApplicationStatuses.NoRecordFound };
					if (!aspireContext.IsScholarshipAnnouncementOpen(scholarshipApplication.ScholarshipAnnouncementID, loginHistory.UserTypeEnum))
						return new GetScholarshipApplicationForEditResult { Status = UpdateScholarshipApplicationStatuses.ScholarshipsNotOpened };
					if (scholarshipApplication.StatusEnum == Model.Entities.ScholarshipApplication.Statuses.InProgress)
						return new GetScholarshipApplicationForEditResult
						{
							ScholarshipApplication = scholarshipApplication,
							Status = UpdateScholarshipApplicationStatuses.Success,
						};
					return new GetScholarshipApplicationForEditResult
					{
						ScholarshipApplication = scholarshipApplication,
						Status = UpdateScholarshipApplicationStatuses.AlreadySubmitted,
					};
				default:
					throw new ArgumentOutOfRangeException(nameof(loginHistory.UserTypeEnum), loginHistory.UserTypeEnum, null);
			}
		}

		public sealed class AddScholarshipApplicationResult
		{
			internal AddScholarshipApplicationResult() { }

			public enum Statuses
			{
				NoRecordFound,
				AlreadyApplied,
				MaxSemesterExceeded,
				ScholarshipsNotOpened,
				NotAuthorizedPerson,
				Success,
			}

			public Statuses Status { get; internal set; }
			public Model.Entities.ScholarshipApplication ScholarshipApplication { get; internal set; }
		}

		public static AddScholarshipApplicationResult AddScholarshipApplication(int studentID, int scholarshipAnnouncementID, string phone, short? semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var studentInfo = aspireContext.Students
					.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgramID,
						s.Phone,
						s.Mobile,
						s.CurrentAddress,
					}).SingleOrDefault();
				if (studentInfo == null)
					return new AddScholarshipApplicationResult { Status = AddScholarshipApplicationResult.Statuses.NoRecordFound };

				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						var permissions = aspireContext.DemandStaffPermissions(studentInfo.InstituteID, studentInfo.DepartmentID, studentInfo.ProgramID, studentInfo.AdmissionOpenProgramID, loginSessionGuid);
						if (permissions == null)
							return new AddScholarshipApplicationResult { Status = AddScholarshipApplicationResult.Statuses.NotAuthorizedPerson };
						break;
					case UserTypes.Student:
						if (studentID != loginHistory.StudentID)
							throw new InvalidOperationException();
						if (!aspireContext.IsScholarshipAnnouncementOpen(scholarshipAnnouncementID, loginHistory.UserTypeEnum))
							return new AddScholarshipApplicationResult { Status = AddScholarshipApplicationResult.Statuses.ScholarshipsNotOpened };
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(loginHistory.UserTypeEnum), loginHistory.UserTypeEnum, null);
				}

				var qarzeHasana = new Schemas.QarzeHasana();
				qarzeHasana.UpdateStudentInfo(phone, studentInfo.Mobile, studentInfo.CurrentAddress);
				var schema = qarzeHasana.ToJson();
				var result = aspireContext.AddScholarshipApplication(studentID, scholarshipAnnouncementID, schema, semesterID, loginHistory.LoginSessionGuid);
				switch (result.Status)
				{
					case ScholarshipApplications.AddScholarshipApplicationResult.Statuses.AlreadyApplied:
						return new AddScholarshipApplicationResult { Status = AddScholarshipApplicationResult.Statuses.AlreadyApplied };
					case ScholarshipApplications.AddScholarshipApplicationResult.Statuses.MaxSemesterExceeded:
						return new AddScholarshipApplicationResult { Status = AddScholarshipApplicationResult.Statuses.MaxSemesterExceeded };
					case ScholarshipApplications.AddScholarshipApplicationResult.Statuses.ScholarshipsNotOpened:
						return new AddScholarshipApplicationResult { Status = AddScholarshipApplicationResult.Statuses.ScholarshipsNotOpened };
					case ScholarshipApplications.AddScholarshipApplicationResult.Statuses.Success:
						aspireContext.CommitTransaction();
						return new AddScholarshipApplicationResult
						{
							ScholarshipApplication = result.ScholarshipApplication,
							Status = AddScholarshipApplicationResult.Statuses.Success
						};
					default:
						throw new ArgumentOutOfRangeException(nameof(result.Status), result.Status, null);
				}
			}
		}

		public static UpdateScholarshipApplicationStatuses UpdateStudentInfo(int scholarshipApplicationID, int studentID, string phone, string mobile, string residentialAddress, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getScholarshipApplicationForEdit = aspireContext.GetScholarshipApplicationForEdit(scholarshipApplicationID, out _, out _, loginSessionGuid);
				switch (getScholarshipApplicationForEdit.Status)
				{
					case UpdateScholarshipApplicationStatuses.NoRecordFound:
					case UpdateScholarshipApplicationStatuses.AlreadySubmitted:
					case UpdateScholarshipApplicationStatuses.NotAuthorizedForChange:
					case UpdateScholarshipApplicationStatuses.ScholarshipsNotOpened:
						return getScholarshipApplicationForEdit.Status;
					case UpdateScholarshipApplicationStatuses.Success:
						var qarzeHasana = Schemas.QarzeHasana.FromJson(getScholarshipApplicationForEdit.ScholarshipApplication.FormData);
						qarzeHasana.UpdateStudentInfo(phone, mobile, residentialAddress);
						getScholarshipApplicationForEdit.ScholarshipApplication.FormData = qarzeHasana.ToJson();
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return UpdateScholarshipApplicationStatuses.Success;
					default:
						throw new ArgumentOutOfRangeException(nameof(getScholarshipApplicationForEdit.Status), getScholarshipApplicationForEdit.Status, null);
				}
			}
		}

		public static UpdateScholarshipApplicationStatuses UpdateFamilyExpenditures(int scholarshipApplicationID, double? childrenFee, double? electricityBill, double? loan, double? houseRent, double? telephoneBill, double? medicalExpenses, double? taxesPaid, double? otherExpenses, double educationalExpensesFamilyCanPay, double requiredEducationalExpenses, string additionalDetails, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getScholarshipApplicationForEdit = aspireContext.GetScholarshipApplicationForEdit(scholarshipApplicationID, out _, out _, loginSessionGuid);
				switch (getScholarshipApplicationForEdit.Status)
				{
					case UpdateScholarshipApplicationStatuses.NoRecordFound:
					case UpdateScholarshipApplicationStatuses.AlreadySubmitted:
					case UpdateScholarshipApplicationStatuses.NotAuthorizedForChange:
					case UpdateScholarshipApplicationStatuses.ScholarshipsNotOpened:
						return getScholarshipApplicationForEdit.Status;
					case UpdateScholarshipApplicationStatuses.Success:
						var qarzeHasana = Schemas.QarzeHasana.FromJson(getScholarshipApplicationForEdit.ScholarshipApplication.FormData);
						qarzeHasana.UpdateFamilyExpenditures(childrenFee, electricityBill, loan, houseRent, telephoneBill, medicalExpenses, taxesPaid, otherExpenses, educationalExpensesFamilyCanPay, requiredEducationalExpenses, additionalDetails);
						getScholarshipApplicationForEdit.ScholarshipApplication.FormData = qarzeHasana.ToJson();
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return UpdateScholarshipApplicationStatuses.Success;
					default:
						throw new ArgumentOutOfRangeException(nameof(getScholarshipApplicationForEdit.Status), getScholarshipApplicationForEdit.Status, null);
				}
			}
		}

		public static UpdateScholarshipApplicationStatuses UpdateFamilyInformation(int scholarshipApplicationID, byte noOfFamilyMembersStudyingAtBU, byte siblingsStudyingAtOthers, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getScholarshipApplicationForEdit = aspireContext.GetScholarshipApplicationForEdit(scholarshipApplicationID, out _, out _, loginSessionGuid);
				switch (getScholarshipApplicationForEdit.Status)
				{
					case UpdateScholarshipApplicationStatuses.NoRecordFound:
					case UpdateScholarshipApplicationStatuses.AlreadySubmitted:
					case UpdateScholarshipApplicationStatuses.NotAuthorizedForChange:
					case UpdateScholarshipApplicationStatuses.ScholarshipsNotOpened:
						return getScholarshipApplicationForEdit.Status;
					case UpdateScholarshipApplicationStatuses.Success:
						var qarzeHasana = Schemas.QarzeHasana.FromJson(getScholarshipApplicationForEdit.ScholarshipApplication.FormData);
						qarzeHasana.UpdateFamilyInformation(noOfFamilyMembersStudyingAtBU, siblingsStudyingAtOthers);
						getScholarshipApplicationForEdit.ScholarshipApplication.FormData = qarzeHasana.ToJson();
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return UpdateScholarshipApplicationStatuses.Success;
					default:
						throw new ArgumentOutOfRangeException(nameof(getScholarshipApplicationForEdit.Status), getScholarshipApplicationForEdit.Status, null);
				}
			}
		}

		public static UpdateScholarshipApplicationStatuses UpdateFamilyIncome(int scholarshipApplicationID, double? fatherIncome, double? motherIncome, double? spouseIncome, double? selfIncome, double? siblingIncome, byte personsDependingOnParentsIncludingYou, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getScholarshipApplicationForEdit = aspireContext.GetScholarshipApplicationForEdit(scholarshipApplicationID, out _, out _, loginSessionGuid);
				switch (getScholarshipApplicationForEdit.Status)
				{
					case UpdateScholarshipApplicationStatuses.NoRecordFound:
					case UpdateScholarshipApplicationStatuses.AlreadySubmitted:
					case UpdateScholarshipApplicationStatuses.NotAuthorizedForChange:
					case UpdateScholarshipApplicationStatuses.ScholarshipsNotOpened:
						return getScholarshipApplicationForEdit.Status;
					case UpdateScholarshipApplicationStatuses.Success:
						var qarzeHasana = Schemas.QarzeHasana.FromJson(getScholarshipApplicationForEdit.ScholarshipApplication.FormData);
						qarzeHasana.UpdateFamilyIncome(fatherIncome, motherIncome, spouseIncome, selfIncome, siblingIncome, personsDependingOnParentsIncludingYou);
						getScholarshipApplicationForEdit.ScholarshipApplication.FormData = qarzeHasana.ToJson();
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return UpdateScholarshipApplicationStatuses.Success;
					default:
						throw new ArgumentOutOfRangeException(nameof(getScholarshipApplicationForEdit.Status), getScholarshipApplicationForEdit.Status, null);
				}
			}
		}

		public static UpdateScholarshipApplicationStatuses AddOrUpdateFamilyMember(int scholarshipApplicationID, int? familyMemberID, double age, string occupation, string relation, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getScholarshipApplicationForEdit = aspireContext.GetScholarshipApplicationForEdit(scholarshipApplicationID, out _, out _, loginSessionGuid);
				switch (getScholarshipApplicationForEdit.Status)
				{
					case UpdateScholarshipApplicationStatuses.NoRecordFound:
					case UpdateScholarshipApplicationStatuses.AlreadySubmitted:
					case UpdateScholarshipApplicationStatuses.NotAuthorizedForChange:
					case UpdateScholarshipApplicationStatuses.ScholarshipsNotOpened:
						return getScholarshipApplicationForEdit.Status;
					case UpdateScholarshipApplicationStatuses.Success:
						var qarzeHasana = Schemas.QarzeHasana.FromJson(getScholarshipApplicationForEdit.ScholarshipApplication.FormData);
						if (familyMemberID == null)
							qarzeHasana.AddFamilyMember(new Schemas.QarzeHasana.FamilyMember
							{
								Age = age,
								Occupation = occupation,
								Relation = relation,
							});
						else
						{
							var familyMember = qarzeHasana.GetFamilyMember(familyMemberID.Value);
							if (familyMember == null)
								return UpdateScholarshipApplicationStatuses.NoRecordFound;
							familyMember.Age = age;
							familyMember.Occupation = occupation;
							familyMember.Relation = relation;
							familyMember.Validate();
						}
						getScholarshipApplicationForEdit.ScholarshipApplication.FormData = qarzeHasana.ToJson();
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return UpdateScholarshipApplicationStatuses.Success;
					default:
						throw new ArgumentOutOfRangeException(nameof(getScholarshipApplicationForEdit.Status), getScholarshipApplicationForEdit.Status, null);
				}
			}
		}

		public static UpdateScholarshipApplicationStatuses DeleteFamilyMember(int scholarshipApplicationID, int familyMemberID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getScholarshipApplicationForEdit = aspireContext.GetScholarshipApplicationForEdit(scholarshipApplicationID, out _, out _, loginSessionGuid);
				switch (getScholarshipApplicationForEdit.Status)
				{
					case UpdateScholarshipApplicationStatuses.NoRecordFound:
					case UpdateScholarshipApplicationStatuses.AlreadySubmitted:
					case UpdateScholarshipApplicationStatuses.NotAuthorizedForChange:
					case UpdateScholarshipApplicationStatuses.ScholarshipsNotOpened:
						return getScholarshipApplicationForEdit.Status;
					case UpdateScholarshipApplicationStatuses.Success:
						var qarzeHasana = Schemas.QarzeHasana.FromJson(getScholarshipApplicationForEdit.ScholarshipApplication.FormData);
						var deleted = qarzeHasana.DeleteFamilyMember(familyMemberID);
						if (!deleted)
							return UpdateScholarshipApplicationStatuses.NoRecordFound;
						getScholarshipApplicationForEdit.ScholarshipApplication.FormData = qarzeHasana.ToJson();
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return UpdateScholarshipApplicationStatuses.Success;
					default:
						throw new ArgumentOutOfRangeException(nameof(getScholarshipApplicationForEdit.Status), getScholarshipApplicationForEdit.Status, null);
				}
			}
		}

		public static UpdateScholarshipApplicationStatuses AddOrUpdateFamilyAsset(int scholarshipApplicationID, int? familyAssetID, string assetType, string ownedBy, string locality, string area, double? priceValue, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getScholarshipApplicationForEdit = aspireContext.GetScholarshipApplicationForEdit(scholarshipApplicationID, out _, out _, loginSessionGuid);
				switch (getScholarshipApplicationForEdit.Status)
				{
					case UpdateScholarshipApplicationStatuses.NoRecordFound:
					case UpdateScholarshipApplicationStatuses.AlreadySubmitted:
					case UpdateScholarshipApplicationStatuses.NotAuthorizedForChange:
					case UpdateScholarshipApplicationStatuses.ScholarshipsNotOpened:
						return getScholarshipApplicationForEdit.Status;
					case UpdateScholarshipApplicationStatuses.Success:
						var qarzeHasana = Schemas.QarzeHasana.FromJson(getScholarshipApplicationForEdit.ScholarshipApplication.FormData);
						if (familyAssetID == null)
							qarzeHasana.AddFamilyAsset(new Schemas.QarzeHasana.FamilyAsset
							{
								AssetType = assetType,
								OwnedBy = ownedBy,
								Locality = locality,
								Area = area,
								PriceValue = priceValue
							});
						else
						{
							var familyAsset = qarzeHasana.GetFamilyAsset(familyAssetID.Value);
							if (familyAsset == null)
								return UpdateScholarshipApplicationStatuses.NoRecordFound;
							familyAsset.AssetType = assetType;
							familyAsset.OwnedBy = ownedBy;
							familyAsset.Locality = locality;
							familyAsset.Area = area;
							familyAsset.PriceValue = priceValue;
							familyAsset.Validate();
						}
						getScholarshipApplicationForEdit.ScholarshipApplication.FormData = qarzeHasana.ToJson();
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return UpdateScholarshipApplicationStatuses.Success;
					default:
						throw new ArgumentOutOfRangeException(nameof(getScholarshipApplicationForEdit.Status), getScholarshipApplicationForEdit.Status, null);
				}
			}
		}

		public static UpdateScholarshipApplicationStatuses DeleteFamilyAsset(int scholarshipApplicationID, int familyAssetID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getScholarshipApplicationForEdit = aspireContext.GetScholarshipApplicationForEdit(scholarshipApplicationID, out _, out _, loginSessionGuid);
				switch (getScholarshipApplicationForEdit.Status)
				{
					case UpdateScholarshipApplicationStatuses.NoRecordFound:
					case UpdateScholarshipApplicationStatuses.AlreadySubmitted:
					case UpdateScholarshipApplicationStatuses.NotAuthorizedForChange:
					case UpdateScholarshipApplicationStatuses.ScholarshipsNotOpened:
						return getScholarshipApplicationForEdit.Status;
					case UpdateScholarshipApplicationStatuses.Success:
						var qarzeHasana = Schemas.QarzeHasana.FromJson(getScholarshipApplicationForEdit.ScholarshipApplication.FormData);
						var deleted = qarzeHasana.DeleteFamilyAsset(familyAssetID);
						if (!deleted)
							return UpdateScholarshipApplicationStatuses.NoRecordFound;
						getScholarshipApplicationForEdit.ScholarshipApplication.FormData = qarzeHasana.ToJson();
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return UpdateScholarshipApplicationStatuses.Success;
					default:
						throw new ArgumentOutOfRangeException(nameof(getScholarshipApplicationForEdit.Status), getScholarshipApplicationForEdit.Status, null);
				}
			}
		}

		public static UpdateScholarshipApplicationStatuses AddOrUpdateVehicle(int scholarshipApplicationID, int? vehicleID, string make, string model, string cc, string ownedBy, double? priceValue, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getScholarshipApplicationForEdit = aspireContext.GetScholarshipApplicationForEdit(scholarshipApplicationID, out _, out _, loginSessionGuid);
				switch (getScholarshipApplicationForEdit.Status)
				{
					case UpdateScholarshipApplicationStatuses.NoRecordFound:
					case UpdateScholarshipApplicationStatuses.AlreadySubmitted:
					case UpdateScholarshipApplicationStatuses.NotAuthorizedForChange:
					case UpdateScholarshipApplicationStatuses.ScholarshipsNotOpened:
						return getScholarshipApplicationForEdit.Status;
					case UpdateScholarshipApplicationStatuses.Success:
						var qarzeHasana = Schemas.QarzeHasana.FromJson(getScholarshipApplicationForEdit.ScholarshipApplication.FormData);
						if (vehicleID == null)
							qarzeHasana.AddVehicle(new Schemas.QarzeHasana.Vehicle
							{
								Make = make,
								Model = model,
								CC = cc,
								OwnedBy = ownedBy,
								PriceValue = priceValue
							});
						else
						{
							var vehicle = qarzeHasana.GetVehicle(vehicleID.Value);
							if (vehicle == null)
								return UpdateScholarshipApplicationStatuses.NoRecordFound;
							vehicle.Make = make;
							vehicle.Model = model;
							vehicle.CC = cc;
							vehicle.OwnedBy = ownedBy;
							vehicle.PriceValue = priceValue;
							vehicle.Validate();
						}
						getScholarshipApplicationForEdit.ScholarshipApplication.FormData = qarzeHasana.ToJson();
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return UpdateScholarshipApplicationStatuses.Success;
					default:
						throw new ArgumentOutOfRangeException(nameof(getScholarshipApplicationForEdit.Status), getScholarshipApplicationForEdit.Status, null);
				}
			}
		}

		public static UpdateScholarshipApplicationStatuses DeleteVehicle(int scholarshipApplicationID, int vehicleID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getScholarshipApplicationForEdit = aspireContext.GetScholarshipApplicationForEdit(scholarshipApplicationID, out _, out _, loginSessionGuid);
				switch (getScholarshipApplicationForEdit.Status)
				{
					case UpdateScholarshipApplicationStatuses.NoRecordFound:
					case UpdateScholarshipApplicationStatuses.AlreadySubmitted:
					case UpdateScholarshipApplicationStatuses.NotAuthorizedForChange:
					case UpdateScholarshipApplicationStatuses.ScholarshipsNotOpened:
						return getScholarshipApplicationForEdit.Status;
					case UpdateScholarshipApplicationStatuses.Success:
						var qarzeHasana = Schemas.QarzeHasana.FromJson(getScholarshipApplicationForEdit.ScholarshipApplication.FormData);
						var deleted = qarzeHasana.DeleteVehicle(vehicleID);
						if (!deleted)
							return UpdateScholarshipApplicationStatuses.NoRecordFound;
						getScholarshipApplicationForEdit.ScholarshipApplication.FormData = qarzeHasana.ToJson();
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return UpdateScholarshipApplicationStatuses.Success;
					default:
						throw new ArgumentOutOfRangeException(nameof(getScholarshipApplicationForEdit.Status), getScholarshipApplicationForEdit.Status, null);
				}
			}
		}

		public static UpdateScholarshipApplicationStatuses AddOrUpdateSaving(int scholarshipApplicationID, int? savingID, string ownedBy, string type, double priceValue, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getScholarshipApplicationForEdit = aspireContext.GetScholarshipApplicationForEdit(scholarshipApplicationID, out _, out _, loginSessionGuid);
				switch (getScholarshipApplicationForEdit.Status)
				{
					case UpdateScholarshipApplicationStatuses.NoRecordFound:
					case UpdateScholarshipApplicationStatuses.AlreadySubmitted:
					case UpdateScholarshipApplicationStatuses.NotAuthorizedForChange:
					case UpdateScholarshipApplicationStatuses.ScholarshipsNotOpened:
						return getScholarshipApplicationForEdit.Status;
					case UpdateScholarshipApplicationStatuses.Success:
						var qarzeHasana = Schemas.QarzeHasana.FromJson(getScholarshipApplicationForEdit.ScholarshipApplication.FormData);
						if (savingID == null)
							qarzeHasana.AddSaving(new Schemas.QarzeHasana.Saving
							{
								OwnedBy = ownedBy,
								Type = type,
								PriceValue = priceValue
							});
						else
						{
							var saving = qarzeHasana.GetSaving(savingID.Value);
							if (saving == null)
								return UpdateScholarshipApplicationStatuses.NoRecordFound;
							saving.OwnedBy = ownedBy;
							saving.Type = type;
							saving.PriceValue = priceValue;
							saving.Validate();
						}
						getScholarshipApplicationForEdit.ScholarshipApplication.FormData = qarzeHasana.ToJson();
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return UpdateScholarshipApplicationStatuses.Success;
					default:
						throw new ArgumentOutOfRangeException(nameof(getScholarshipApplicationForEdit.Status), getScholarshipApplicationForEdit.Status, null);
				}
			}
		}

		public static UpdateScholarshipApplicationStatuses DeleteSaving(int scholarshipApplicationID, int savingID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getScholarshipApplicationForEdit = aspireContext.GetScholarshipApplicationForEdit(scholarshipApplicationID, out _, out _, loginSessionGuid);
				switch (getScholarshipApplicationForEdit.Status)
				{
					case UpdateScholarshipApplicationStatuses.NoRecordFound:
					case UpdateScholarshipApplicationStatuses.AlreadySubmitted:
					case UpdateScholarshipApplicationStatuses.NotAuthorizedForChange:
					case UpdateScholarshipApplicationStatuses.ScholarshipsNotOpened:
						return getScholarshipApplicationForEdit.Status;
					case UpdateScholarshipApplicationStatuses.Success:
						var qarzeHasana = Schemas.QarzeHasana.FromJson(getScholarshipApplicationForEdit.ScholarshipApplication.FormData);
						var deleted = qarzeHasana.DeleteSaving(savingID);
						if (!deleted)
							return UpdateScholarshipApplicationStatuses.NoRecordFound;
						getScholarshipApplicationForEdit.ScholarshipApplication.FormData = qarzeHasana.ToJson();
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return UpdateScholarshipApplicationStatuses.Success;
					default:
						throw new ArgumentOutOfRangeException(nameof(getScholarshipApplicationForEdit.Status), getScholarshipApplicationForEdit.Status, null);
				}
			}
		}

		public static UpdateScholarshipApplicationStatuses AddOrUpdateOtherIncome(int scholarshipApplicationID, int? otherIncomeID, string incomeType, double priceValue, string description, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getScholarshipApplicationForEdit = aspireContext.GetScholarshipApplicationForEdit(scholarshipApplicationID, out _, out _, loginSessionGuid);
				switch (getScholarshipApplicationForEdit.Status)
				{
					case UpdateScholarshipApplicationStatuses.NoRecordFound:
					case UpdateScholarshipApplicationStatuses.AlreadySubmitted:
					case UpdateScholarshipApplicationStatuses.NotAuthorizedForChange:
					case UpdateScholarshipApplicationStatuses.ScholarshipsNotOpened:
						return getScholarshipApplicationForEdit.Status;
					case UpdateScholarshipApplicationStatuses.Success:
						var qarzeHasana = Schemas.QarzeHasana.FromJson(getScholarshipApplicationForEdit.ScholarshipApplication.FormData);
						if (otherIncomeID == null)
							qarzeHasana.AddOtherIncome(new Schemas.QarzeHasana.OtherIncome
							{
								IncomeType = incomeType,
								PriceValue = priceValue,
								Description = description
							});
						else
						{
							var otherIncome = qarzeHasana.GetOtherIncome(otherIncomeID.Value);
							if (otherIncome == null)
								return UpdateScholarshipApplicationStatuses.NoRecordFound;
							otherIncome.IncomeType = incomeType;
							otherIncome.PriceValue = priceValue;
							otherIncome.Description = description;
							otherIncome.Validate();
						}
						getScholarshipApplicationForEdit.ScholarshipApplication.FormData = qarzeHasana.ToJson();
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return UpdateScholarshipApplicationStatuses.Success;
					default:
						throw new ArgumentOutOfRangeException(nameof(getScholarshipApplicationForEdit.Status), getScholarshipApplicationForEdit.Status, null);
				}
			}
		}

		public static UpdateScholarshipApplicationStatuses DeleteOtherIncome(int scholarshipApplicationID, int otherIncomeID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getScholarshipApplicationForEdit = aspireContext.GetScholarshipApplicationForEdit(scholarshipApplicationID, out _, out _, loginSessionGuid);
				switch (getScholarshipApplicationForEdit.Status)
				{
					case UpdateScholarshipApplicationStatuses.NoRecordFound:
					case UpdateScholarshipApplicationStatuses.AlreadySubmitted:
					case UpdateScholarshipApplicationStatuses.NotAuthorizedForChange:
					case UpdateScholarshipApplicationStatuses.ScholarshipsNotOpened:
						return getScholarshipApplicationForEdit.Status;
					case UpdateScholarshipApplicationStatuses.Success:
						var qarzeHasana = Schemas.QarzeHasana.FromJson(getScholarshipApplicationForEdit.ScholarshipApplication.FormData);
						var deleted = qarzeHasana.DeleteOtherIncome(otherIncomeID);
						if (!deleted)
							return UpdateScholarshipApplicationStatuses.NoRecordFound;
						getScholarshipApplicationForEdit.ScholarshipApplication.FormData = qarzeHasana.ToJson();
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return UpdateScholarshipApplicationStatuses.Success;
					default:
						throw new ArgumentOutOfRangeException(nameof(getScholarshipApplicationForEdit.Status), getScholarshipApplicationForEdit.Status, null);
				}
			}
		}

		public sealed class GetApplicationInfoResult
		{
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public Model.Entities.ScholarshipApplication ScholarshipApplication { get; internal set; }
			public short AnnouncementSemesterID { get; set; }
			public short IntakeSemesterID { get; set; }
			public int? RegistrationNo { get; internal set; }
			public string Name { get; internal set; }
			public string FatherName { get; internal set; }
			public DateTime DOB { get; internal set; }
			public byte Gender { get; internal set; }
			public string GenderFullName => ((Genders)this.Gender).ToFullName();
			public string DepartmentName { get; internal set; }
			public string Phone { get; internal set; }
			public string Mobile { get; internal set; }
			public string UniversityEmail { get; internal set; }
			public string CurrentAddress { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public SemesterNos? SemesterNo { get; internal set; }
			public Sections? Section { get; internal set; }
			public Shifts? Shift { get; internal set; }
			public string Domicile { get; internal set; }
			public StudentSemester StudentSemester { get; internal set; }
			public StudentSemester LastStudentSemester { get; internal set; }
			public bool IsFirstSemester => this.IntakeSemesterID == this.AnnouncementSemesterID;
			public double? PreviousDegreePercentage { get; internal set; }
			public string LastResult
			{
				get
				{
					if (this.StudentSemester != null && this.StudentSemester.BUExamGPA != null && this.StudentSemester.BUExamCGPA != null)
						return $"GPA: {this.StudentSemester.BUExamGPA:N2}, CGPA: {this.StudentSemester.BUExamCGPA:N2} ({StudentSemester.SemesterID.ToSemesterString()})";

					if (this.LastStudentSemester != null && this.LastStudentSemester.BUExamGPA != null && this.LastStudentSemester.BUExamCGPA != null)
						return $"GPA: {this.LastStudentSemester.BUExamGPA:N2}, CGPA: {this.LastStudentSemester.BUExamCGPA:N2} ({LastStudentSemester.SemesterID.ToSemesterString()})";

					if (this.IsFirstSemester)
						return $"{this.PreviousDegreePercentage:N2}%";
					return $"GPA: N/A, CGPA: N/A";
				}
			}
			public string PersonalEmail { get; internal set; }
			public string Email
			{
				get
				{
					if (this.UniversityEmail != null && this.PersonalEmail != null)
						return $"{this.UniversityEmail}, {this.PersonalEmail}";
					return $"{this.UniversityEmail}{this.PersonalEmail}";
				}
			}
		}

		public static GetApplicationInfoResult GetApplicationInfo(int scholarshipAnnouncementID, int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var scholarshipAnnouncement = aspireContext.ScholarshipAnnouncements.Where(sa => sa.ScholarshipAnnouncementID == scholarshipAnnouncementID).Select(sa => new
				{
					sa.SemesterID,
					sa.Scholarship.InstituteID,
				}).SingleOrDefault();
				if (scholarshipAnnouncement == null)
					return null;
				var studentInfo = aspireContext.Students
					.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.Enrollment,
						s.Name,
						s.RegistrationNo,
						s.AdmissionOpenProgram.Program.ProgramAlias,
						s.AdmissionOpenProgram.Program.Department.DepartmentName,
						s.FatherName,
						s.DOB,
						s.Gender,
						s.Phone,
						s.Mobile,
						s.UniversityEmail,
						s.PersonalEmail,
						s.CurrentAddress,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgramID,
						s.Domicile,
						s.AdmissionOpenProgram.IsSummerRegularSemester,
						s.AdmissionOpenProgram.SemesterID,
						ScholarshipApplication = s.ScholarshipApplications.FirstOrDefault(sapp => sapp.ScholarshipAnnouncementID == scholarshipAnnouncementID),
						PreviousDegreePercentage = s.StudentAcademicRecords
							.OrderByDescending(sar => sar.DegreeType)
							.Select(sar => (double?)sar.Percentage)
							.FirstOrDefault(),
						StudentSemester = s.StudentSemesters.FirstOrDefault(ss => ss.SemesterID == scholarshipAnnouncement.SemesterID && ss.Semester.SemesterType != (byte)SemesterTypes.Summer),
						LastStudentSemester = s.StudentSemesters.OrderByDescending(ss => ss.SemesterID).FirstOrDefault(ss => ss.SemesterID < scholarshipAnnouncement.SemesterID && ss.Semester.SemesterType != (byte)SemesterTypes.Summer),
					}).SingleOrDefault();

				if (studentInfo == null)
					return null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
						if (staffLoginHistory.InstituteID != scholarshipAnnouncement.InstituteID || staffLoginHistory.InstituteID != studentInfo.InstituteID)
							return null;
						break;
					case UserTypes.Student:
						if (loginHistory.StudentID != studentID)
							return null;
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(loginHistory.UserTypeEnum), loginHistory.UserTypeEnum, null);
				}

				return new GetApplicationInfoResult
				{
					StudentID = studentID,
					Enrollment = studentInfo.Enrollment,
					RegistrationNo = studentInfo.RegistrationNo,
					ProgramAlias = studentInfo.ProgramAlias,
					SemesterNo = studentInfo.StudentSemester?.SemesterNoEnum,
					Section = studentInfo.StudentSemester?.SectionEnum,
					Shift = studentInfo.StudentSemester?.ShiftEnum,
					Name = studentInfo.Name,
					FatherName = studentInfo.FatherName,
					DOB = studentInfo.DOB,
					Gender = studentInfo.Gender,
					DepartmentName = studentInfo.DepartmentName,
					Phone = studentInfo.Phone,
					Mobile = studentInfo.Mobile,
					UniversityEmail = studentInfo.UniversityEmail,
					PersonalEmail = studentInfo.PersonalEmail,
					CurrentAddress = studentInfo.CurrentAddress,
					Domicile = studentInfo.Domicile,
					ScholarshipApplication = studentInfo.ScholarshipApplication,
					AnnouncementSemesterID = scholarshipAnnouncement.SemesterID,
					IntakeSemesterID = studentInfo.SemesterID,
					PreviousDegreePercentage = studentInfo.PreviousDegreePercentage,
					StudentSemester = studentInfo.StudentSemester,
					LastStudentSemester = studentInfo.LastStudentSemester,
				};
			}
		}

		public sealed class SubmitApplicationResult
		{
			internal SubmitApplicationResult() { }

			public enum Error
			{
				NoRecordFound,
				AlreadySubmitted,
				StudentInformationIsMissing,
				FamilyMembersAreMissing,
				FamilyInformationIsMissing,
				FamilyExpendituresAreMissing,
				FamilyIncomeIsMissing,
				NotAuthorizedForChange,
				ScholarshipsNotOpened,
				Success,
			}
			public List<Error> Errors { get; internal set; }
		}

		public static SubmitApplicationResult SubmitApplication(int scholarshipApplicationID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var getScholarshipApplicationForEdit = aspireContext.GetScholarshipApplicationForEdit(scholarshipApplicationID, out _, out _, loginSessionGuid);
				var errors = new List<SubmitApplicationResult.Error>();
				switch (getScholarshipApplicationForEdit.Status)
				{
					case UpdateScholarshipApplicationStatuses.NoRecordFound:
						errors.Add(SubmitApplicationResult.Error.NoRecordFound);
						return new SubmitApplicationResult { Errors = errors };
					case UpdateScholarshipApplicationStatuses.AlreadySubmitted:
						errors.Add(SubmitApplicationResult.Error.AlreadySubmitted);
						return new SubmitApplicationResult { Errors = errors };
					case UpdateScholarshipApplicationStatuses.NotAuthorizedForChange:
						errors.Add(SubmitApplicationResult.Error.NotAuthorizedForChange);
						return new SubmitApplicationResult { Errors = errors };
					case UpdateScholarshipApplicationStatuses.ScholarshipsNotOpened:
						errors.Add(SubmitApplicationResult.Error.ScholarshipsNotOpened);
						return new SubmitApplicationResult { Errors = errors };

					case UpdateScholarshipApplicationStatuses.Success:
						var qarzeHasana = Schemas.QarzeHasana.FromJson(getScholarshipApplicationForEdit.ScholarshipApplication.FormData);
						var validationErrors = qarzeHasana.ValidateForSubmission().ToList();
						if (validationErrors.Any())
						{
							foreach (var validationError in validationErrors)
								switch (validationError)
								{
									case Schemas.QarzeHasana.ValidationErrors.StudentInformationIsMissing:
										errors.Add(SubmitApplicationResult.Error.StudentInformationIsMissing);
										break;
									case Schemas.QarzeHasana.ValidationErrors.FamilyMembersAreMissing:
										errors.Add(SubmitApplicationResult.Error.FamilyMembersAreMissing);
										break;
									case Schemas.QarzeHasana.ValidationErrors.FamilyInformationIsMissing:
										errors.Add(SubmitApplicationResult.Error.FamilyInformationIsMissing);
										break;
									case Schemas.QarzeHasana.ValidationErrors.FamilyIncomeIsMissing:
										errors.Add(SubmitApplicationResult.Error.FamilyIncomeIsMissing);
										break;
									case Schemas.QarzeHasana.ValidationErrors.FamilyExpendituresAreMissing:
										errors.Add(SubmitApplicationResult.Error.FamilyExpendituresAreMissing);
										break;
									default:
										throw new ArgumentOutOfRangeException(nameof(validationError), validationError, null);
								}
							return new SubmitApplicationResult { Errors = errors };
						}
						aspireContext.SubmitScholarshipApplication(getScholarshipApplicationForEdit.ScholarshipApplication.StudentID, scholarshipApplicationID, loginSessionGuid, loginHistory.UserTypeEnum);
						aspireContext.CommitTransaction();
						return new SubmitApplicationResult { Errors = null };
					default:
						throw new ArgumentOutOfRangeException(nameof(getScholarshipApplicationForEdit.Status), getScholarshipApplicationForEdit.Status, null);
				}
			}
		}

		public enum DeleteStatuses
		{
			NoRecordFound,
			Success,
		}

		public static DeleteStatuses DeleteApplication(int scholarshipAnnouncementID, int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Scholarship.CanAddAndDeleteQarzeHasanaApplicationForm, UserGroupPermission.PermissionValues.Allowed);

				var scholarshipApplication = aspireContext.ScholarshipApplications.Where(sa => sa.ScholarshipAnnouncementID == scholarshipAnnouncementID && sa.StudentID == studentID)
					.Include(sa => sa.ScholarshipApplicationApprovals)
					.SingleOrDefault();
				if (scholarshipApplication == null)
					return DeleteStatuses.NoRecordFound;

				aspireContext.ScholarshipApplicationApprovals.RemoveRange(scholarshipApplication.ScholarshipApplicationApprovals);
				aspireContext.ScholarshipApplications.Remove(scholarshipApplication);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteStatuses.Success;
			}
		}
	}
}
