﻿using Aspire.BL.Core.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.Scholarship.Common
{
	internal static class ScholarshipApplications
	{
		public sealed class AddScholarshipApplicationResult
		{
			public enum Statuses
			{
				AlreadyApplied,
				MaxSemesterExceeded,
				ScholarshipsNotOpened,
				Success,
			}
			internal AddScholarshipApplicationResult() { }
			public Statuses Status { get; internal set; }
			public ScholarshipApplication ScholarshipApplication { get; internal set; }
		}

		internal static bool IsScholarshipApplicationAlreadyApplied(this AspireContext aspireContext, int studentID)
		{
			var scholarshipApplication = aspireContext.ScholarshipApplications.Where(sa => sa.StudentID == studentID).OrderByDescending(sa => sa.ScholarshipApplicationID).FirstOrDefault();
			if (scholarshipApplication == null)
				return false;

			var firstApplication = aspireContext.ScholarshipApplicationApprovals.Where(saa => saa.ScholarshipApplicationID == scholarshipApplication.ScholarshipApplicationID).OrderBy(saa => saa.SemesterID).FirstOrDefault();
			if (firstApplication != null)
				if (firstApplication.ApprovalStatusEnum != Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Rejected)
					return true;
			return false;
		}

		internal static bool IsMaxSemesterExceededForScholarship(this AspireContext aspireContext, int studentID, short semesterID)
		{
			var scholarshipApplication = aspireContext.Students.Where(s => s.StudentID == studentID).FirstOrDefault(s => s.AdmissionOpenProgram.FinalSemesterID >= semesterID);
			if (scholarshipApplication == null)
				return true;
			return false;
		}

		internal static bool IsScholarshipAnnouncementOpen(this AspireContext aspireContext, int scholarshipAnnouncementID, UserTypes userTypeEnum)
		{
			switch (userTypeEnum)
			{
				case UserTypes.Staff:
					return true;
				case UserTypes.Student:
					var today = DateTime.Today;
					return aspireContext.ScholarshipAnnouncements
						.Where(sa => sa.ScholarshipAnnouncementID == scholarshipAnnouncementID)
						.Where(sa => sa.StartDate <= today)
						.Where(sa => sa.EndDate == null || today <= sa.EndDate)
						.Any(sa => sa.Status == (byte)ScholarshipAnnouncement.Statuses.Active);
				default:
					throw new ArgumentOutOfRangeException(nameof(userTypeEnum), userTypeEnum, null);
			}
		}

		internal static AddScholarshipApplicationResult AddScholarshipApplication(this AspireContext aspireContext, int studentID, int scholarshipAnnouncementID, string formData, short? semesterID, Guid loginSessionGuid)
		{
			if (aspireContext.IsScholarshipApplicationAlreadyApplied(studentID))
				return new AddScholarshipApplicationResult { Status = AddScholarshipApplicationResult.Statuses.AlreadyApplied };

			if (aspireContext.IsMaxSemesterExceededForScholarship(studentID, semesterID ?? throw new InvalidOperationException()))
				return new AddScholarshipApplicationResult { Status = AddScholarshipApplicationResult.Statuses.MaxSemesterExceeded };

			var studentInstituteID = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => s.AdmissionOpenProgram.Program.InstituteID).Single();
			var scholarshipApplication = aspireContext.ScholarshipApplications.Add(new ScholarshipApplication
			{
				ScholarshipAnnouncementID = aspireContext.ScholarshipAnnouncements.Where(sa => sa.ScholarshipAnnouncementID == scholarshipAnnouncementID && sa.Scholarship.InstituteID == studentInstituteID).Select(sa => sa.ScholarshipAnnouncementID).Single(),
				StudentID = studentID,
				StatusEnum = ScholarshipApplication.Statuses.InProgress,
				FormData = formData,
				SubmissionDate = null,
				DocumentStatusesEnum = ScholarshipApplication.DocumentsStatuses.NotSubmitted,
			});
			var userLoginHistoryID = aspireContext.GetUserLoginHistoryID(loginSessionGuid);
			aspireContext.SaveChanges(userLoginHistoryID);
			return new AddScholarshipApplicationResult
			{
				Status = AddScholarshipApplicationResult.Statuses.Success,
				ScholarshipApplication = scholarshipApplication,
			};
		}

		internal static AddScholarshipApplicationResult SubmitScholarshipApplication(this AspireContext aspireContext, int studentID, int scholarshipApplicationID, Guid loginSessionGuid, UserTypes userTypeEnum)
		{
			var scholarshipApplication = aspireContext.ScholarshipApplications.Single(sa => sa.ScholarshipApplicationID == scholarshipApplicationID && sa.StudentID == studentID);

			switch (userTypeEnum)
			{
				case UserTypes.Staff:
					if (scholarshipApplication.StatusEnum != ScholarshipApplication.Statuses.Submitted)
						scholarshipApplication.SubmissionDate = DateTime.Now;
					break;
				case UserTypes.Student:
					scholarshipApplication.SubmissionDate = DateTime.Now;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(userTypeEnum), userTypeEnum, null);
			}

			if (scholarshipApplication.StatusEnum != ScholarshipApplication.Statuses.Submitted)
			{
				scholarshipApplication.StatusEnum = ScholarshipApplication.Statuses.Submitted;
				var applicationApproval = new Schemas.ScholarshipApplicationApproval();
				var scholarshipApplicationApproval = new Model.Entities.ScholarshipApplicationApproval
				{
					ScholarshipApplicationID = scholarshipApplication.ScholarshipApplicationID,
					SemesterID = scholarshipApplication.ScholarshipAnnouncement.SemesterID,
					ApprovalDate = DateTime.Now,
					Data = applicationApproval.ToJson(),
					ApprovalStatusEnum = ScholarshipApplicationApproval.ApprovalStatuses.Pending,
					ChequeStatusesEnum = ScholarshipApplicationApproval.ChequeStatuses.No,
				};
				scholarshipApplication.ScholarshipApplicationApprovals.Add(scholarshipApplicationApproval);
			}

			var userLoginHistoryID = aspireContext.GetUserLoginHistoryID(loginSessionGuid);
			aspireContext.SaveChanges(userLoginHistoryID);
			return new AddScholarshipApplicationResult
			{
				Status = AddScholarshipApplicationResult.Statuses.Success,
			};
		}
	}
}
