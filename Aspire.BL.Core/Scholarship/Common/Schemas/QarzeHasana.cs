﻿using Aspire.Lib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Scholarship.Common.Schemas
{
	[Serializable]
	public sealed class QarzeHasana
	{
		#region Properties
		public string Phone { get; set; }
		public string Mobile { get; set; }
		public string ResidentialAddress { get; set; }
		public byte? NoOfFamilyMembersStudyingAtBU { get; set; }
		public byte? SiblingsStudyingAtOthers { get; set; }
		public double? FatherIncome { get; set; }
		public double? MotherIncome { get; set; }
		public double? SpouseIncome { get; set; }
		public double? SelfIncome { get; set; }
		public double? SiblingIncome { get; set; }
		public byte? PersonsDependingOnParentsIncludingYou { get; set; }
		public double? ChildrenFee { get; set; }
		public double? ElectricityBill { get; set; }
		public double? TelephoneBill { get; set; }
		public double? MedicalExpenses { get; set; }
		public double? TaxesPaid { get; set; }
		public double? OtherExpenses { get; set; }
		public double? Loan { get; set; }
		public double? HouseRent { get; set; }
		public double? RequiredEducationalExpenses { get; set; }
		public double? EducationalExpensesFamilyCanPay { get; set; }
		public string AdditionalDetails { get; set; }
		public string PhoneWithNullCheck => this.Phone.ToNAIfNullOrEmpty();
		public string FatherIncomeWithNullCheck => this.FatherIncome?.ToString("N0") ?? this.FatherIncome.ToString().ToNAIfNullOrEmpty();
		public string MotherIncomeWithNullCheck => this.MotherIncome?.ToString("N0") ?? this.MotherIncome.ToString().ToNAIfNullOrEmpty();
		public string SpouseIncomeWithNullCheck => this.SpouseIncome?.ToString("N0") ?? this.SpouseIncome.ToString().ToNAIfNullOrEmpty();
		public string SelfIncomeWithNullCheck => this.SelfIncome?.ToString("N0") ?? this.SelfIncome.ToString().ToNAIfNullOrEmpty();
		public string SiblingIncomeWithNullCheck => this.SiblingIncome?.ToString("N0") ?? this.SiblingIncome.ToString().ToNAIfNullOrEmpty();
		public string LoanWithNullCheck => this.Loan?.ToString("N0") ?? this.Loan.ToString().ToNAIfNullOrEmpty();
		public string HouseRentWithNullCheck => this.HouseRent?.ToString("N0") ?? this.HouseRent.ToString().ToNAIfNullOrEmpty();
		public string TaxesPaidWithNullCheck => this.TaxesPaid?.ToString("N0") ?? this.TaxesPaid.ToString().ToNAIfNullOrEmpty();
		public string OtherExpensesWithNullCheck => this.OtherExpenses?.ToString("N0") ?? this.OtherExpenses.ToString().ToNAIfNullOrEmpty();
		public string AdditionalDetailsWithNullCheck => this.AdditionalDetails.ToNAIfNullOrEmpty();
		public List<FamilyMember> FamilyMembers { get; set; }
		public List<FamilyAsset> FamilyAssets { get; set; }
		public List<Vehicle> Vehicles { get; set; }
		public List<Saving> Savings { get; set; }
		public List<OtherIncome> OtherIncomes { get; set; }

		#endregion

		#region Classes for lists to bind with grid

		[Serializable]
		public sealed class FamilyMember
		{
			public int FamilyMemberID { get; set; }
			public string Relation { get; set; }
			public string Occupation { get; set; }
			public string OccupationWithNullCheck => this.Occupation.ToNAIfNullOrEmpty();
			public double Age { get; set; }
			public FamilyMember Validate()
			{
				this.Relation = this.Relation.TrimAndCannotBeEmpty();
				if (this.Age < 0 || this.Age > 120)
					throw new InvalidOperationException("Invalid Age value. Must be <= 120");
				return this;
			}

			public List<FamilyMember> GetList()
			{
				return null;
			}
		}

		[Serializable]
		public sealed class FamilyAsset
		{
			public int FamilyAssetID { get; set; }
			public string AssetType { get; set; }
			public string OwnedBy { get; set; }
			public string Locality { get; set; }
			public string Area { get; set; }
			public double? PriceValue { get; set; }
			public string PriceValueWithNullCheck => this.PriceValue?.ToString("N0") ?? this.PriceValue.ToString().ToNAIfNullOrEmpty();

			public FamilyAsset Validate()
			{
				this.AssetType = this.AssetType.TrimAndCannotBeEmpty();
				this.OwnedBy = this.OwnedBy.TrimAndCannotBeEmpty();
				this.Locality = this.Locality.TrimAndCannotBeEmpty();
				this.Area = this.Area.TrimAndCannotBeEmpty();
				if (this.PriceValue != null && this.PriceValue < 0)
					throw new InvalidOperationException("Price value cannot be negative.");
				return this;
			}

			public List<FamilyAsset> GetList()
			{
				return null;
			}
		}

		[Serializable]
		public class Vehicle
		{
			public int VehicleID { get; set; }
			public string Make { get; set; }
			public string Model { get; set; }
			public string CC { get; set; }
			public string OwnedBy { get; set; }
			public double? PriceValue { get; set; }
			public string PriceValueWithNullCheck => this.PriceValue?.ToString("N0") ?? this.PriceValue.ToString().ToNAIfNullOrEmpty();

			public Vehicle Validate()
			{
				this.Make = this.Make.TrimAndCannotBeEmpty();
				this.Model = this.Model.TrimAndCannotBeEmpty();
				this.CC = this.CC.TrimAndCannotBeEmpty();
				this.OwnedBy = this.OwnedBy.TrimAndCannotBeEmpty();
				if (this.PriceValue != null && this.PriceValue < 0)
					throw new InvalidOperationException("Price value can not be negative.");
				return this;
			}

			public List<Vehicle> GetList()
			{
				return null;
			}
		}

		[Serializable]
		public class Saving
		{
			public int SavingID { get; set; }
			public string OwnedBy { get; set; }
			public string Type { get; set; }
			public double? PriceValue { get; set; }

			public Saving Validate()
			{
				this.OwnedBy = this.OwnedBy.TrimAndCannotBeEmpty();
				this.Type = this.Type.TrimAndCannotBeEmpty();
				this.OwnedBy = this.OwnedBy.TrimAndCannotBeEmpty();
				if (this.PriceValue != null && this.PriceValue < 0)
					throw new InvalidOperationException("Price value can not be negative.");
				return this;
			}

			public List<Saving> GetList()
			{
				return null;
			}
		}

		[Serializable]
		public class OtherIncome
		{
			public int OtherIncomeID { get; set; }
			public string IncomeType { get; set; }
			public double? PriceValue { get; set; }
			public string Description { get; set; }
			public string DescriptionWithNullCheck => this.Description.ToNAIfNullOrEmpty();

			public OtherIncome Validate()
			{
				this.IncomeType = this.IncomeType.TrimAndCannotBeEmpty();
				if (this.PriceValue != null && this.PriceValue < 0)
					throw new InvalidOperationException("Price value can not be negative.");
				this.Description = this.Description.Trim();
				return this;
			}

			public List<OtherIncome> GetList()
			{
				return null;
			}
		}

		#endregion

		#region Methods for Family Members

		internal void AddFamilyMember(FamilyMember familyMember)
		{
			if (this.FamilyMembers == null)
				this.FamilyMembers = new List<FamilyMember>();
			this.FamilyMembers.Add(familyMember.Validate());
			this.RebuildIDs();
		}

		private void RebuildIDs()
		{
			var index = 0;
			this.FamilyMembers?.ForEach(fm => fm.FamilyMemberID = index++);
			index = 0;
			this.FamilyAssets?.ForEach(fa => fa.FamilyAssetID = index++);
			index = 0;
			this.Vehicles?.ForEach(v => v.VehicleID = index++);
			index = 0;
			this.Savings?.ForEach(s => s.SavingID = index++);
			index = 0;
			this.OtherIncomes?.ForEach(oi => oi.OtherIncomeID = index++);
		}

		public FamilyMember GetFamilyMember(int familyMemberID)
		{
			return this.FamilyMembers?.SingleOrDefault(fm => fm.FamilyMemberID == familyMemberID);
		}

		internal bool DeleteFamilyMember(int familyMemberID)
		{
			var familyMember = this.GetFamilyMember(familyMemberID);
			if (familyMember == null)
				return false;
			this.FamilyMembers.Remove(familyMember);
			this.RebuildIDs();
			return true;
		}

		#endregion

		#region Methods for Family Assets

		internal void AddFamilyAsset(FamilyAsset familyAsset)
		{
			if (this.FamilyAssets == null)
				this.FamilyAssets = new List<FamilyAsset>();
			this.FamilyAssets.Add(familyAsset.Validate());
			this.RebuildIDs();
		}

		public FamilyAsset GetFamilyAsset(int familyAssetID)
		{
			return this.FamilyAssets?.SingleOrDefault(fm => fm.FamilyAssetID == familyAssetID);
		}

		internal bool DeleteFamilyAsset(int familyAssetID)
		{
			var familyAsset = this.GetFamilyAsset(familyAssetID);
			if (familyAsset == null)
				return false;
			this.FamilyAssets.Remove(familyAsset);
			this.RebuildIDs();
			return true;
		}

		#endregion

		#region Methods for Vehicles

		internal void AddVehicle(Vehicle vehicle)
		{
			if (this.Vehicles == null)
				this.Vehicles = new List<Vehicle>();
			this.Vehicles.Add(vehicle.Validate());
			this.RebuildIDs();
		}

		public Vehicle GetVehicle(int vehicleID)
		{
			return this.Vehicles?.SingleOrDefault(v => v.VehicleID == vehicleID);
		}

		internal bool DeleteVehicle(int vehicleID)
		{
			var vehicle = this.GetVehicle(vehicleID);
			if (vehicle == null)
				return false;
			this.Vehicles.Remove(vehicle);
			this.RebuildIDs();
			return true;
		}

		#endregion

		#region Methods for Savings

		internal void AddSaving(Saving saving)
		{
			if (this.Savings == null)
				this.Savings = new List<Saving>();
			this.Savings.Add(saving.Validate());
			this.RebuildIDs();
		}

		public Saving GetSaving(int savingID)
		{
			return this.Savings?.SingleOrDefault(s => s.SavingID == savingID);
		}

		internal bool DeleteSaving(int savingID)
		{
			var saving = this.GetSaving(savingID);
			if (saving == null)
				return false;
			this.Savings.Remove(saving);
			this.RebuildIDs();
			return true;
		}

		#endregion

		#region Methods for Family Other Incomes

		internal void AddOtherIncome(OtherIncome otherIncome)
		{
			if (this.OtherIncomes == null)
				this.OtherIncomes = new List<OtherIncome>();
			this.OtherIncomes.Add(otherIncome.Validate());
			this.RebuildIDs();
		}

		public OtherIncome GetOtherIncome(int otherIncomeID)
		{
			return this.OtherIncomes?.SingleOrDefault(oi => oi.OtherIncomeID == otherIncomeID);
		}

		internal bool DeleteOtherIncome(int otherIncomeID)
		{
			var otherIncome = this.GetOtherIncome(otherIncomeID);
			if (otherIncome == null)
				return false;
			this.OtherIncomes.Remove(otherIncome);
			this.RebuildIDs();
			return true;
		}

		#endregion

		public string ToJson()
		{
			return this.ToJsonString();
		}

		public static QarzeHasana FromJson(string json)
		{
			return json.FromJsonString<QarzeHasana>();
		}

		#region Update methods

		public void UpdateStudentInfo(string phone, string mobile, string residentialAddress)
		{
			if (phone != null)
				this.Phone = phone.Trim();
			if (mobile != null)
				this.Mobile = mobile.TrimAndCannotBeEmpty();
			if (residentialAddress != null)
				this.ResidentialAddress = residentialAddress.Trim();
		}

		public void UpdateFamilyInformation(byte noOfFamilyMembersStudyingAtBU, byte siblingsStudyingAtOthers)
		{
			this.NoOfFamilyMembersStudyingAtBU = noOfFamilyMembersStudyingAtBU;
			this.SiblingsStudyingAtOthers = siblingsStudyingAtOthers;
		}

		public void UpdateFamilyIncome(double? fatherIncome, double? motherIncome, double? spouseIncome, double? selfIncome, double? siblingIncome, byte personsDependingOnParentsIncludingYou)
		{
			this.FatherIncome = fatherIncome;
			this.MotherIncome = motherIncome;
			this.SpouseIncome = spouseIncome;
			this.SelfIncome = selfIncome;
			this.SiblingIncome = siblingIncome;
			this.PersonsDependingOnParentsIncludingYou = personsDependingOnParentsIncludingYou;
		}

		public void UpdateFamilyExpenditures(double? childrenFee, double? electricityBill, double? loan, double? houseRent, double? telephoneBill, double? medicalExpenses, double? taxesPaid, double? otherExpenses, double educationalExpensesFamilyCanPay, double requiredEducationalExpenses, string additionalDetails)
		{
			this.ChildrenFee = childrenFee;
			this.ElectricityBill = electricityBill;
			this.Loan = loan;
			this.HouseRent = houseRent;
			this.TelephoneBill = telephoneBill;
			this.MedicalExpenses = medicalExpenses;
			this.TaxesPaid = taxesPaid;
			this.OtherExpenses = otherExpenses;
			this.EducationalExpensesFamilyCanPay = educationalExpensesFamilyCanPay;
			this.RequiredEducationalExpenses = requiredEducationalExpenses;
			this.AdditionalDetails = additionalDetails;
		}

		#endregion

		#region Validation on submit

		public enum ValidationErrors
		{
			StudentInformationIsMissing,
			FamilyMembersAreMissing,
			FamilyInformationIsMissing,
			FamilyExpendituresAreMissing,
			FamilyIncomeIsMissing,
		}

		public IEnumerable<ValidationErrors> ValidateForSubmission()
		{
			if (string.IsNullOrWhiteSpace(this.Mobile) || string.IsNullOrWhiteSpace(this.ResidentialAddress))
				yield return ValidationErrors.StudentInformationIsMissing;

			if (this.NoOfFamilyMembersStudyingAtBU == null || this.SiblingsStudyingAtOthers == null)
				yield return ValidationErrors.FamilyInformationIsMissing;

			if (this.FatherIncome == null && this.MotherIncome == null && this.SelfIncome == null && this.SiblingIncome == null && this.OtherIncomes == null)
				yield return ValidationErrors.FamilyIncomeIsMissing;

			if (this.EducationalExpensesFamilyCanPay == null || this.RequiredEducationalExpenses == null)
				yield return ValidationErrors.FamilyExpendituresAreMissing;

			if (this.FamilyMembers?.Any() != true)
				yield return ValidationErrors.FamilyMembersAreMissing;
		}

		#endregion

		public static List<QarzeHasana> GetList()
		{
			return null;
		}
	}
}
