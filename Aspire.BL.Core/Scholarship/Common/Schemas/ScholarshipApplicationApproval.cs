﻿using Aspire.Lib.Extensions;
using System;

namespace Aspire.BL.Core.Scholarship.Common.Schemas
{
	[Serializable]
	public sealed class ScholarshipApplicationApproval
	{
		public int Count { get; set; }
		public int? TuitionFee { get; set; }
		public int? CampusRecommendation { get; set; }
		public string CampusRemarks { get; set; }
		public int? CSCRecommendation { get; set; }
		public string CSCRemarks { get; set; }
		public int? FinalRecommendation { get; set; }
		public string GetApprovedAmount(Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses approvalStatus)
		{
			return approvalStatus == Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Approved
					&& this.FinalRecommendation != null && this.TuitionFee != null
					? $"Rs. {(int)Math.Round(this.TuitionFee.Value * this.FinalRecommendation.Value / 100.0, MidpointRounding.AwayFromZero)}"
					: string.Empty.ToNAIfNullOrEmpty();
		}

		public string ToJson()
		{
			return this.ToJsonString();
		}

		public static ScholarshipApplicationApproval FromJson(string json)
		{
			return json.FromJsonString<ScholarshipApplicationApproval>();
		}

		public void UpdateApplicationApproval(int? tuitionFee, int? campusRecommendation, string campusRemarks, int? CSCRecommendation, string CSCRemarks, int? finalRecommendation)
		{
			this.TuitionFee = tuitionFee;
			this.CampusRecommendation = campusRecommendation;
			this.CampusRemarks = campusRemarks;
			this.CSCRecommendation = CSCRecommendation;
			this.CSCRemarks = CSCRemarks;
			this.FinalRecommendation = finalRecommendation;
		}
	}
}
