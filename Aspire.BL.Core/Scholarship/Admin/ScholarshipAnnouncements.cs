﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Scholarship.Admin
{
	public static class ScholarshipAnnouncements
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Scholarship.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Scholarship.ManageControlPanel, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.Scholarship.ManageControlPanel, UserGroupPermission.PermissionValues.Allowed);
		}

		public enum AddScholarshipAnnouncementResults
		{
			AlreadyExists,
			Success,
		}

		private static Model.Entities.Scholarship CheckScholarshipExists(int instituteID, Model.Entities.Scholarship.ScholarshipTypes scholarshipTypeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				var result = aspireContext.Scholarships.Where(s => s.InstituteID == instituteID && s.ScholarshipType == (byte)scholarshipTypeEnum).FirstOrDefault();
				if (result == null)
					return null;
				return result;
			}
		}

		public static AddScholarshipAnnouncementResults AddScholarshipAnnouncement(int instituteID, Model.Entities.Scholarship.ScholarshipTypes scholarshipTypeEnum, short semesterID, DateTime startDate, DateTime? endDate, Model.Entities.ScholarshipAnnouncement.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);

				var scholarship = CheckScholarshipExists(instituteID, scholarshipTypeEnum, loginSessionGuid);
				if (scholarship == null)
				{
					var newScholarship = new Model.Entities.Scholarship
					{
						InstituteID = instituteID,
						ScholarshipType = (byte)scholarshipTypeEnum,
					};
					aspireContext.Scholarships.Add(newScholarship);
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
					scholarship = CheckScholarshipExists(instituteID, scholarshipTypeEnum, loginSessionGuid);
				}
				
				var announcementAlreadyExists = aspireContext.ScholarshipAnnouncements.Any(sca => sca.Scholarship.InstituteID == instituteID && sca.Scholarship.ScholarshipType == (byte)scholarshipTypeEnum && sca.ScholarshipID == scholarship.ScholarshipID && sca.SemesterID == semesterID);
				if (announcementAlreadyExists)
					return AddScholarshipAnnouncementResults.AlreadyExists;

				var scholarshipAnnouncement = new Model.Entities.ScholarshipAnnouncement
				{
					ScholarshipID = scholarship.ScholarshipID,
					SemesterID = semesterID,
					StartDate = startDate,
					EndDate = endDate,
					StatusEnum = statusEnum,
					ScholarshipAnnouncementPrograms = new List<ScholarshipAnnouncementProgram>
					{
						new ScholarshipAnnouncementProgram
						{
							ProgramID=null,
							AdmissionOpenProgramID = null,
						}
					}
				};
				aspireContext.ScholarshipAnnouncements.Add(scholarshipAnnouncement.Validate());
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				return AddScholarshipAnnouncementResults.Success;
			}
		}

		public enum UpdateScholarshipAnnouncementResults
		{
			NoRecordFound,
			Success
		}

		public static UpdateScholarshipAnnouncementResults UpdateScholarshipAnnouncement(int scholarshipAnnouncementID, DateTime startDate, DateTime? endDate, Model.Entities.ScholarshipAnnouncement.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var scholarshipAnnouncement = aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.Scholarships)
					.SelectMany(s => s.ScholarshipAnnouncements)
					.SingleOrDefault(sa => sa.ScholarshipAnnouncementID == scholarshipAnnouncementID);
				if (scholarshipAnnouncement == null)
					return UpdateScholarshipAnnouncementResults.NoRecordFound;

				scholarshipAnnouncement.StartDate = startDate;
				scholarshipAnnouncement.EndDate = endDate;
				scholarshipAnnouncement.StatusEnum = statusEnum;
				scholarshipAnnouncement.Validate();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateScholarshipAnnouncementResults.Success;
			}
		}

		public sealed class ScholarshipAnnouncement
		{
			internal ScholarshipAnnouncement() { }
			public int ScholarshipAnnouncementID { get; internal set; }
			public int InstituteID { get; internal set; }
			public int ScholarshipID { get; internal set; }
			public string InstituteAlias { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public DateTime StartDate { get; internal set; }
			public DateTime? EndDate { get; internal set; }
			public Model.Entities.ScholarshipAnnouncement.Statuses StatusEnum { get; internal set; }
			public string StatusFullName => this.StatusEnum.ToString().SplitCamelCasing();
			public Model.Entities.Scholarship.ScholarshipTypes ScholarshipTypeEnum { get; internal set; }
			public string ScholarshipTypeFullName => this.ScholarshipTypeEnum.ToFullName();
		}

		public static List<ScholarshipAnnouncement> GetScholarshipAnnouncements(int? instituteID, Model.Entities.Scholarship.ScholarshipTypes? scholarshipTypeEnum, short? semesterID, Model.Entities.Scholarship.Statuses? statusEnum, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.Scholarships)
					.Where(s => instituteID == null || s.InstituteID == instituteID.Value)
					.Where(s => (byte?)scholarshipTypeEnum == null || s.ScholarshipType == (byte)scholarshipTypeEnum.Value)
					.SelectMany(s => s.ScholarshipAnnouncements)
					.Where(sa => (byte?)statusEnum == null || sa.Status == (byte)statusEnum.Value)
					.Where(s => semesterID == null || s.SemesterID == semesterID.Value);
				virtualItemCount = query.Count();
				switch (sortExpression)
				{
					case nameof(ScholarshipAnnouncement.InstituteAlias):
						query = query.OrderBy(sortDirection, q => q.Scholarship.Institute.InstituteAlias);
						break;
					case nameof(ScholarshipAnnouncement.ScholarshipTypeEnum):
					case nameof(ScholarshipAnnouncement.ScholarshipTypeFullName):
						query = query.OrderBy(sortDirection, q => q.Scholarship.ScholarshipType);
						break;
					case nameof(ScholarshipAnnouncement.SemesterID):
					case nameof(ScholarshipAnnouncement.Semester):
						query = query.OrderBy(sortDirection, q => q.SemesterID);
						break;
					case nameof(ScholarshipAnnouncement.StartDate):
						query = query.OrderBy(sortDirection, q => q.StartDate);
						break;
					case nameof(ScholarshipAnnouncement.EndDate):
						query = query.OrderBy(sortDirection, q => q.EndDate);
						break;
					case nameof(ScholarshipAnnouncement.StatusEnum):
					case nameof(ScholarshipAnnouncement.StatusFullName):
						query = query.OrderBy(sortDirection, q => q.Status);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}

				return query.Skip(pageIndex * pageSize).Take(pageSize)
				   .Select(sca => new ScholarshipAnnouncement
				   {
					   ScholarshipAnnouncementID = sca.ScholarshipAnnouncementID,
					   InstituteAlias = sca.Scholarship.Institute.InstituteAlias,
					   ScholarshipTypeEnum = (Model.Entities.Scholarship.ScholarshipTypes)sca.Scholarship.ScholarshipType,
					   SemesterID = sca.SemesterID,
					   StartDate = sca.StartDate,
					   EndDate = sca.EndDate,
					   StatusEnum = (Model.Entities.ScholarshipAnnouncement.Statuses)sca.Status
				   }).ToList();
			}
		}

		public static ScholarshipAnnouncement GetScholarshipAnnouncement(int scholarshipAnnouncementID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.Scholarships)
					.SelectMany(s => s.ScholarshipAnnouncements)
					.Select(s => new ScholarshipAnnouncement
					{
						InstituteID = s.Scholarship.InstituteID,
						ScholarshipID = s.Scholarship.ScholarshipID,
						SemesterID = s.SemesterID,
						StartDate = s.StartDate,
						EndDate = s.EndDate,
						StatusEnum = (Model.Entities.ScholarshipAnnouncement.Statuses)s.Status,
						ScholarshipAnnouncementID = s.ScholarshipAnnouncementID
					})
					.SingleOrDefault(sa => sa.ScholarshipAnnouncementID == scholarshipAnnouncementID);
			}
		}

		public static Model.Entities.ScholarshipAnnouncement ToggleScholarshipAnnouncementStatus(int scholarshipAnnouncementID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var scholarshipAnnouncement = aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.Scholarships)
					.SelectMany(s => s.ScholarshipAnnouncements)
					.SingleOrDefault(sa => sa.ScholarshipAnnouncementID == scholarshipAnnouncementID);
				if (scholarshipAnnouncement == null)
					return null;

				switch (scholarshipAnnouncement.StatusEnum)
				{
					case Model.Entities.ScholarshipAnnouncement.Statuses.Active:
						scholarshipAnnouncement.StatusEnum = Model.Entities.ScholarshipAnnouncement.Statuses.Inactive;
						break;
					case Model.Entities.ScholarshipAnnouncement.Statuses.Inactive:
						scholarshipAnnouncement.StatusEnum = Model.Entities.ScholarshipAnnouncement.Statuses.Active;
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(scholarshipAnnouncement.StatusEnum), scholarshipAnnouncement.StatusEnum, null);
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return scholarshipAnnouncement;
			}
		}

		public enum DeleteScholarshipAnnouncementStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteScholarshipAnnouncementStatuses DeleteScholarshipAnnouncement(int scholarshipAnnouncementID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var scholarshipAnnouncement = aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.Scholarships)
					.SelectMany(s => s.ScholarshipAnnouncements)
					.Include(s => s.ScholarshipAnnouncementPrograms)
					.SingleOrDefault(sa => sa.ScholarshipAnnouncementID == scholarshipAnnouncementID);
				if (scholarshipAnnouncement == null)
					return DeleteScholarshipAnnouncementStatuses.NoRecordFound;
				if (scholarshipAnnouncement.ScholarshipApplications.Any())
					return DeleteScholarshipAnnouncementStatuses.ChildRecordExists;
				aspireContext.ScholarshipAnnouncementPrograms.RemoveRange(scholarshipAnnouncement.ScholarshipAnnouncementPrograms);
				aspireContext.ScholarshipAnnouncements.Remove(scholarshipAnnouncement);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteScholarshipAnnouncementStatuses.Success;
			}
		}

		public static List<CustomListItem> GetScholarships(int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory)
						.SelectMany(i => i.Scholarships)
						.Where(s => s.InstituteID == instituteID)
						.Select(s => new
						{
							s.ScholarshipID,
							ScholarshipTypeEnum = (Model.Entities.Scholarship.ScholarshipTypes)s.ScholarshipType
						})
						.ToList()
						.Select(s => new CustomListItem
						{
							ID = s.ScholarshipID,
							Text = s.ScholarshipTypeEnum.ToFullName()
						})
						.OrderBy(s => s.Text)
						.ToList();
			}
		}
	}
}
