﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Scholarship.Student
{
	public static class ScholarshipAnnouncements
	{
		public sealed class ScholarshipAnnouncement
		{
			internal ScholarshipAnnouncement() { }
			public int ScholarshipAnnouncementID { get; internal set; }
			public int? ScholarshipApplicationID { get; internal set; }
			public short SemesterID { get; internal set; }
			public Model.Entities.Scholarship.ScholarshipTypes ScholarshipTypeEnum { get; internal set; }
			public Model.Entities.ScholarshipApplication.Statuses? ScholarshipApplicationStatusEnum { get; internal set; }
			public DateTime StartDate { get; internal set; }
			public DateTime? EndDate { get; internal set; }

			public bool ActionTypeForApprovalStatusBtn
			{
				get
				{
					switch (this.ScholarshipApplicationStatusEnum)
					{
						case null:
						case Model.Entities.ScholarshipApplication.Statuses.InProgress:
							return false;
						case Model.Entities.ScholarshipApplication.Statuses.Submitted:
							return true;
						default:
							throw new NotImplementedEnumException(this.ScholarshipApplicationStatusEnum);
					}
				}
			}

			public bool ActionTypeForFormBtn
			{
				get
				{
					switch (this.ScholarshipApplicationStatusEnum)
					{
						case Model.Entities.ScholarshipApplication.Statuses.InProgress:
						case Model.Entities.ScholarshipApplication.Statuses.Submitted:
							return true;
						case null:
							var dateToday = DateTime.Today;
							if (EndDate == null || dateToday <= this.EndDate)
								return true;
							return false;
						default:
							throw new NotImplementedEnumException(this.ScholarshipApplicationStatusEnum);
					}
				}
			}

			public Model.Entities.ScholarshipApplication.Statuses? ScholarshipApplicationStatus
			{
				get
				{
					switch (this.ScholarshipApplicationStatusEnum)
					{
						case Model.Entities.ScholarshipApplication.Statuses.InProgress:
							return Model.Entities.ScholarshipApplication.Statuses.InProgress;
						case Model.Entities.ScholarshipApplication.Statuses.Submitted:
							return Model.Entities.ScholarshipApplication.Statuses.Submitted;
						case null:
							var dateToday = DateTime.Today;
							if (dateToday <= this.EndDate || EndDate == null)
								return Model.Entities.ScholarshipApplication.Statuses.NotApplied;
							return Model.Entities.ScholarshipApplication.Statuses.DateOver;
						default:
							throw new NotImplementedEnumException(this.ScholarshipApplicationStatusEnum);
					}
				}
			}
		}

		public static List<ScholarshipAnnouncement> GetAnnouncedScholarships(int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStudentActiveSession(loginSessionGuid, SubUserTypes.None);
				var studentInfo = aspireContext.Students
					.Where(s => s.StudentID == loginHistory.StudentID)
					.Select(s => new
					{
						s.StudentID,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgramID,
						s.RegisteredCourses,
					}).SingleOrDefault();
				if (studentInfo == null)
					return null;

				var scholarshipAnnouncements = aspireContext.ScholarshipAnnouncements
					.Where(san => san.Scholarship.InstituteID == instituteID)
					.Where(san => san.Status == (byte)Model.Entities.ScholarshipAnnouncement.Statuses.Active)
					.Where(san => san.EndDate >= DateTime.Today || san.ScholarshipApplications.Any(sap => sap.StudentID == loginHistory.StudentID
								  && sap.Status == (byte)Model.Entities.ScholarshipApplication.Statuses.Submitted))
					.Select(sa => new
					{
						sa.ScholarshipAnnouncementID,
						sa.SemesterID,
						sa.StartDate,
						sa.EndDate,
						ScholarshipTypeEnum = (Model.Entities.Scholarship.ScholarshipTypes)sa.Scholarship.ScholarshipType,
						ScholarshipApplication = sa.ScholarshipApplications.Where(sap => sap.StudentID == loginHistory.StudentID).Select(sapp => new
						{
							ScholarshipApplicationStatusEnum = (Model.Entities.ScholarshipApplication.Statuses?)sapp.Status,
							sapp.ScholarshipApplicationID,
						}).FirstOrDefault(),
					}).ToList();

				return scholarshipAnnouncements.Select(sa => new ScholarshipAnnouncement
				{
					ScholarshipAnnouncementID = sa.ScholarshipAnnouncementID,
					SemesterID = sa.SemesterID,
					ScholarshipTypeEnum = sa.ScholarshipTypeEnum,
					ScholarshipApplicationStatusEnum = sa.ScholarshipApplication?.ScholarshipApplicationStatusEnum,
					ScholarshipApplicationID = sa.ScholarshipApplication?.ScholarshipApplicationID,
					StartDate = sa.StartDate,
					EndDate = sa.EndDate,
				}).OrderByDescending(sa => sa.SemesterID).ToList();
			}
		}
	}
}
