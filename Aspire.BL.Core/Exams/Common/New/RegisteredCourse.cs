﻿using Aspire.BL.Core.Exams.Common.New.Converters;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.New
{
	[JsonObject(MemberSerialization.OptIn)]
	internal class RegisteredCourse : IRegisteredCourse
	{
		public static IReadOnlyList<IRegisteredCourse> GetAllRegisteredCourses(IOfferedCourse offeredCourse, AspireContext aspireContext)
		{
			var registeredCourses = aspireContext.RegisteredCourses.Where(rc => rc.OfferedCourseID == offeredCourse.OfferedCourseID && rc.DeletedDate == null)
				.Select(rc => new RegisteredCourse
				{
					RegisteredCourseID = rc.RegisteredCourseID,
					StudentID = rc.StudentID,
					Enrollment = rc.Student.Enrollment,
					RegistrationNo = rc.Student.RegistrationNo,
					Name = rc.Student.Name,
					ProgramAlias = rc.Student.AdmissionOpenProgram.Program.ProgramAlias,
					ProgramDurationEnum = (ProgramDurations)rc.Student.AdmissionOpenProgram.Program.Duration,
					IsSummerRegularSemester = rc.Student.AdmissionOpenProgram.IsSummerRegularSemester,
					StudentStatusEnum = (Model.Entities.Student.Statuses?)rc.Student.Status,
					StatusEnum = (RegisteredCours.Statuses?)rc.Status,
					FreezeStatusEnum = (FreezedStatuses?)rc.FreezedStatus,
					SummerGradeCapping = rc.Cours.SummerGradeCapping,
					StudentAdmissionOpenProgramID = rc.Student.AdmissionOpenProgramID,
					RoadmapAdmissionOpenProgramID = rc.Cours.AdmissionOpenProgramID,
					CourseCode = rc.Cours.CourseCode,
					Title = rc.Cours.Title,
					CreditHours = rc.Cours.CreditHours,
					CourseCategoryEnum = (CourseCategories)rc.Cours.CourseCategory,
					RoadmapSemesterID = rc.Cours.AdmissionOpenProgram.SemesterID,
					Assignments = rc.Assignments,
					Quizzes = rc.Quizzes,
					Internals = rc.Internals,
					Mid = rc.Mid,
					Final = rc.Final,
					Total = rc.Total,
					GradeEnum = (ExamGrades?)rc.Grade,
					GradePoints = rc.GradePoints,
					Product = rc.Product,
					ExamMarksPolicyChecksumMD5 = rc.ExamMarksPolicyChecksumMD5,
					ExamMarksPolicyID = rc.ExamMarksPolicyID,
					MarksEntryLocked = rc.MarksEntryLocked,
					MarksEntryLockedDate = rc.MarksEntryLockedDate,
					ExamMarksPolicyCourseCategoryEnum = (CourseCategories?)rc.ExamMarksPolicyCourseCategory
				})
				.ToList();

			var examMarksPolicies = ExamMarksPolicy.GetAllExamMarksPolicies(offeredCourse, aspireContext);
			registeredCourses.ForEach(rc =>
			{
				rc.OfferedCourse = offeredCourse;
				rc.ExamMarksPolicyToBeApplied = examMarksPolicies.SingleOrDefault(emp => emp.AdmissionOpenProgramID == rc.RoadmapAdmissionOpenProgramID);
			});

			switch (offeredCourse.MarksSheet.MarksEntryTypeEnum)
			{
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					var registeredCourseExamMarks = aspireContext.RegisteredCourseExamMarks
						.Where(rcem => rcem.RegisteredCours.OfferedCourseID == offeredCourse.OfferedCourseID && rcem.RegisteredCours.DeletedDate == null)
						.Select(rcem => new
						{
							rcem.RegisteredCourseExamMarkID,
							rcem.RegisteredCourseID,
							rcem.OfferedCourseExamID,
							rcem.ExamMarks,
						}).ToList();

					registeredCourses.ForEach(rc =>
					{
						rc.OfferedCourseExams = offeredCourse.OfferedCourseExams.Select(oce =>
							{
								var registeredCourseExamMark = registeredCourseExamMarks
									.Where(rcem => rcem.RegisteredCourseID == rc.RegisteredCourseID && rcem.OfferedCourseExamID == oce.OfferedCourseExamID)
									.Select(rcem => new RegisteredCourseExamMark(rc, oce, rcem.ExamMarks))
									.Cast<IRegisteredCourseExamMark>()
									.SingleOrDefault();
								return new ExamMarks(oce, registeredCourseExamMark);
							}).ToList();
					});
					break;
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
					registeredCourses.ForEach(rc => rc.OfferedCourseExams = new List<ExamMarks>());
					break;
				default:
					throw new NotImplementedEnumException(offeredCourse.MarksSheet.MarksEntryTypeEnum);
			}
			return registeredCourses;
		}

		public IOfferedCourse OfferedCourse { get; private set; }

		[JsonProperty]
		public int RegisteredCourseID { get; private set; }

		[JsonProperty]
		public int StudentAdmissionOpenProgramID { get; private set; }

		[JsonProperty]
		public int RoadmapAdmissionOpenProgramID { get; private set; }

		[JsonProperty("Credit Hours")]
		public decimal CreditHours { get; private set; }

		[JsonProperty]
		public int StudentID { get; private set; }

		[JsonProperty("Enrollment")]
		public string Enrollment { get; private set; }

		[JsonProperty("Registration No.")]
		public int? RegistrationNo { get; private set; }

		[JsonProperty("Student Name")]
		public string Name { get; private set; }

		[JsonProperty("Student Status")]
		[JsonConverter(typeof(EnumFullNameConverter))]
		public Model.Entities.Student.Statuses? StudentStatusEnum { get; private set; }

		[JsonProperty("Program")]
		public string ProgramAlias { get; private set; }

		[JsonProperty("Program Duration")]
		[JsonConverter(typeof(EnumFullNameConverter))]
		public ProgramDurations ProgramDurationEnum { get; private set; }

		[JsonProperty("Course Code")]
		public string CourseCode { get; private set; }

		[JsonProperty("Course Title")]
		public string Title { get; private set; }

		[JsonProperty("Is Summer Regular Semester")]
		[JsonConverter(typeof(YesNoConverter))]
		public bool IsSummerRegularSemester { get; private set; }

		[JsonProperty("Roadmap Semester")]
		[JsonConverter(typeof(SemesterIDConverter))]
		public short RoadmapSemesterID { get; private set; }

		[JsonProperty("Course Category")]
		[JsonConverter(typeof(EnumFullNameConverter))]
		public CourseCategories CourseCategoryEnum { get; private set; }

		[JsonProperty("Summer Grade Capping")]
		[JsonConverter(typeof(YesNoConverter))]
		public bool SummerGradeCapping { get; private set; }

		[JsonProperty("Registered Course Status")]
		[JsonConverter(typeof(EnumFullNameConverter))]
		public RegisteredCours.Statuses? StatusEnum { get; private set; }

		[JsonProperty("Registered Course Freeze Status")]
		[JsonConverter(typeof(EnumFullNameConverter))]
		public FreezedStatuses? FreezeStatusEnum { get; private set; }

		public decimal? Assignments { get; private set; }

		public byte? AssignmentsTotal => this.ExamMarksPolicyToBeApplied?.GetMaxAssignments(this);

		[JsonProperty("Assignments")]
		public string AssignmentsString => this.OfferedCourse.MarksSheet.Display.GetMarksString(this.Assignments, this.AssignmentsTotal);

		public decimal? Quizzes { get; private set; }

		public byte? QuizzesTotal => this.ExamMarksPolicyToBeApplied?.GetMaxQuizzes(this);

		[JsonProperty("Quizzes")]
		public string QuizzesString => this.OfferedCourse.MarksSheet.Display.GetMarksString(this.Quizzes, this.QuizzesTotal);

		public decimal? Internals { get; private set; }

		public byte? InternalsTotal => this.ExamMarksPolicyToBeApplied?.GetMaxInternals(this);

		[JsonProperty("Internals")]
		public string InternalsString => this.OfferedCourse.MarksSheet.Display.GetMarksString(this.Internals, this.InternalsTotal);

		public decimal? Mid { get; private set; }
		public byte? MidTotal => this.ExamMarksPolicyToBeApplied?.GetMaxMid(this);

		[JsonProperty("Mid")]
		public string MidString => this.OfferedCourse.MarksSheet.Display.GetMarksString(this.Mid, this.MidTotal);

		public decimal? Final { get; private set; }

		public byte? FinalTotal => this.ExamMarksPolicyToBeApplied?.GetMaxFinal(this);

		[JsonProperty("Final")]
		public string FinalString => this.OfferedCourse.MarksSheet.Display.GetMarksString(this.Final, this.FinalTotal);

		public byte? Total { get; private set; }

		public byte TotalTotal => 100;

		[JsonProperty("Total")]
		public string TotalString => this.Total == null ? "-" : this.OfferedCourse.MarksSheet.Display.GetMarksString(this.Total.Value, this.TotalTotal);

		public ExamGrades? GradeEnum { get; private set; }

		[JsonProperty("Grade")]
		public string GradeString => AspireFormats.GetGradeString(this.GradeEnum, this.StatusEnum);

		[JsonProperty("Grade Points")]
		public decimal? GradePoints { get; private set; }

		[JsonProperty("Product")]
		public decimal? Product { get; private set; }

		[JsonProperty("Status")]
		public string StatusString => AspireFormats.GetRegisteredCourseStatusString(this.StatusEnum, this.FreezeStatusEnum);

		public string StatusHtml => AspireFormats.GetRegisteredCourseStatusHtml(this.StatusEnum, this.FreezeStatusEnum);

		[JsonProperty("Marks Entry Locked")]
		[JsonConverter(typeof(YesNoConverter))]
		public bool MarksEntryLocked { get; private set; }

		[JsonProperty("Marks Entry Locked On")]
		[JsonConverter(typeof(DateTimeConverter))]
		public DateTime? MarksEntryLockedDate { get; private set; }

		[JsonProperty]
		public int? ExamMarksPolicyID { get; private set; }

		[JsonProperty("Exam Marks Policy Checksum MD5")]
		public string ExamMarksPolicyChecksumMD5 { get; private set; }

		[JsonProperty("Exam Marks Policy Course Category")]
		[JsonConverter(typeof(EnumFullNameConverter))]
		public CourseCategories? ExamMarksPolicyCourseCategoryEnum { get; private set; }

		public IExamMarksPolicy ExamMarksPolicyToBeApplied { get; private set; }

		public IReadOnlyList<IExamMarks> OfferedCourseExams { get; private set; }

		[JsonProperty("Individual Assignments")]
		public IReadOnlyList<IExamMarks> OfferedCourseExamAssignments => this.OfferedCourseExams.Where(ie => ie.OfferedCourseExam.ExamTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Assignment).OrderBy(ie => ie.OfferedCourseExam.ExamNo).ToList();

		[JsonProperty("Individual Quizzes")]
		public IReadOnlyList<IExamMarks> OfferedCourseExamQuizzes => this.OfferedCourseExams.Where(ie => ie.OfferedCourseExam.ExamTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Quiz).OrderBy(ie => ie.OfferedCourseExam.ExamNo).ToList();

		public string RegisteredCourseInfo => $"Code: {this.CourseCode}, Title: {this.Title}, Category: {this.CourseCategoryEnum.ToFullName()}, Roadmap: {this.RoadmapSemesterID.ToSemesterString()}, Program: {this.ProgramAlias}";

		public (List<string> errors, List<string> warnings) Validate()
		{
			var errors = new List<string>();
			var warnings = new List<string>();
			(List<string> errors, List<string> warnings) Error(string errorMessage)
			{
				errors.Add(errorMessage);
				return (errors.Distinct().ToList(), warnings.Distinct().ToList());
			}
			if (this.CourseCategoryEnum != this.OfferedCourse.CourseCategoryEnum)
				return Error($"Registered Course Category do not match with offered course's category \"{this.OfferedCourse.CourseCategoryEnum.ToFullName()}\". Please contact respective Course Coordinator to resolve this issue. Registered Course: {this.RegisteredCourseInfo}.");

			if (this.ExamMarksPolicyToBeApplied == null)
				return Error($"Exam Marks Policy is missing. Registered Course: {this.RegisteredCourseInfo}.");

			switch (this.CourseCategoryEnum)
			{
				case CourseCategories.Course:
					if (this.ExamMarksPolicyToBeApplied.CourseMarksEntryTypeEnum != this.OfferedCourse.ExamMarksPolicy.CourseMarksEntryTypeEnum)
						return Error($"Marks Entry Type for {this.CourseCategoryEnum.ToFullName()} in Exam Marks Policy do not match with \"{this.OfferedCourse.ExamMarksPolicy.CourseMarksEntryTypeEnum.ToFullName()}\". Registered Course: {this.RegisteredCourseInfo}. Offered Course: {this.OfferedCourse.OfferedCourseInfo}.");
					break;
				case CourseCategories.Lab:
					if (this.ExamMarksPolicyToBeApplied.LabMarksEntryTypeEnum != this.OfferedCourse.ExamMarksPolicy.LabMarksEntryTypeEnum)
						return Error($"Marks Entry Type for {this.CourseCategoryEnum.ToFullName()} in Exam Marks Policy do not match with \"{this.OfferedCourse.ExamMarksPolicy.CourseMarksEntryTypeEnum.ToFullName()}\". Registered Course: {this.RegisteredCourseInfo}. Offered Course: {this.OfferedCourse.OfferedCourseInfo}.");
					break;
				case CourseCategories.Internship:
				case CourseCategories.Project:
				case CourseCategories.Thesis:
					throw new InvalidOperationException();
				default:
					throw new NotImplementedEnumException(this.CourseCategoryEnum);
			}

			if (this.ExamMarksPolicyID != null)
			{
				if (this.ExamMarksPolicyToBeApplied.ExamMarksPolicyID != this.ExamMarksPolicyID.Value)
					warnings.Add($"Exam Marks Policy has been changed after marks entry. Registered Course: {this.RegisteredCourseInfo}. Offered Course: {this.OfferedCourse.OfferedCourseInfo}.");
				if (this.ExamMarksPolicyToBeApplied.ChecksumMD5 != this.ExamMarksPolicyChecksumMD5)
					warnings.Add($"Values of Exam Marks Policy have been changed. Registered Course: {this.RegisteredCourseInfo}. Offered Course: {this.OfferedCourse.OfferedCourseInfo}.");
				if (this.CourseCategoryEnum != this.ExamMarksPolicyCourseCategoryEnum)
					warnings.Add($"Course Category has been changed after marks entry. Registered Course: {this.RegisteredCourseInfo}. Offered Course: {this.OfferedCourse.OfferedCourseInfo}.");
			}
			return (errors.Distinct().ToList(), warnings.Distinct().ToList());
		}

		private IEnumerable<string> MarksEntryNotAllowedAsPerStatusesReasons
		{
			get
			{
				switch (this.StudentStatusEnum)
				{
					case Model.Entities.Student.Statuses.AdmissionCancelled:
					case Model.Entities.Student.Statuses.Blocked:
					case Model.Entities.Student.Statuses.Deferred:
					case Model.Entities.Student.Statuses.Dropped:
					case Model.Entities.Student.Statuses.Expelled:
					case Model.Entities.Student.Statuses.Graduated:
					case Model.Entities.Student.Statuses.Left:
					case Model.Entities.Student.Statuses.ProgramChanged:
					case Model.Entities.Student.Statuses.Rusticated:
					case Model.Entities.Student.Statuses.TransferredToOtherCampus:
						if (this.OfferedCourse.MarksSheet.IsFacultyMember)
							yield return $"Marks Entry is not allowed because student's status is \"{this.StudentStatusEnum.Value.ToFullName()}\".";
						break;
					case null:
						break;
					default:
						throw new NotImplementedEnumException(this.StudentStatusEnum);
				}
				if (this.FreezeStatusEnum != null)
					yield return $"Marks Entry is not allowed because student has freeze the {this.OfferedCourse.OfferedSemesterID.ToSemesterString()} semester. Status: \"{this.FreezeStatusEnum.Value.ToFullName()}\".";
				switch (this.StatusEnum)
				{
					case RegisteredCours.Statuses.Incomplete:
					case RegisteredCours.Statuses.AttendanceDefaulter:
					case null:
						break;
					case RegisteredCours.Statuses.Deferred:
						if (!this.OfferedCourse.MarksSheet.IsStaff)
							yield return $"Marks Entry is not allowed because registered course's status is \"{this.StatusEnum.Value.ToFullName()}\".";
						break;
					case RegisteredCours.Statuses.WithdrawWithFee:
					case RegisteredCours.Statuses.WithdrawWithoutFee:
					case RegisteredCours.Statuses.UnfairMeans:
						yield return $"Marks Entry is not allowed because registered course's status is \"{this.StatusEnum.Value.ToFullName()}\".";
						break;
					default:
						throw new NotImplementedEnumException(this.StatusEnum);
				}
			}
		}

		public IEnumerable<string> MarksEntryNotAllowedReasons
		{
			get
			{
				if (this.MarksEntryLocked)
					yield return "Marks Entry is locked for this record.";
				foreach (var reason in this.MarksEntryNotAllowedAsPerStatusesReasons)
					yield return reason;
			}
		}

		public bool MarksEntryAllowed => this.MarksEntryNotAllowedReasons?.Any() != true;

		public IEnumerable<string> CompilationNotAllowedReasons => this.MarksEntryNotAllowedAsPerStatusesReasons;

		public bool CompilationAllowed => this.CompilationNotAllowedReasons?.Any() != true;

		public IEnumerable<string> CanNotUnlockMarksEntryReasons => this.OfferedCourse.MarksSheet.MarksSubmissionFlow.CanNotUnlockMarksEntryForRegisteredCourseReasons(this);

		public bool CanUnlockMarksEntry => !this.CanNotUnlockMarksEntryReasons.Any();

		public decimal? GetCompiledAssignmentsMarks(OfferedCours.ExamCompilationTypes examCompilationType)
		{
			return this.OfferedCourse.MarksSheet.GetCompiledAssignmentsMarks(this, examCompilationType);
		}

		public decimal? GetCompiledQuizzesMarks(OfferedCours.ExamCompilationTypes examCompilationType)
		{
			return this.OfferedCourse.MarksSheet.GetCompiledQuizzesMarks(this, examCompilationType);
		}

		public decimal? GetCompiledInternalsMarks(OfferedCours.ExamCompilationTypes examCompilationType)
		{
			return this.OfferedCourse.MarksSheet.GetCompiledInternalsMarks(this, examCompilationType);
		}

		public void SetTotalsAndGrades(RegisteredCours registeredCourse)
		{
			this.OfferedCourse.MarksSheet.SetTotalsAndGrades(this, registeredCourse);
			registeredCourse.ExamMarksPolicyID = this.ExamMarksPolicyToBeApplied.ExamMarksPolicyID;
			registeredCourse.ExamMarksPolicyChecksumMD5 = this.ExamMarksPolicyToBeApplied.ChecksumMD5;
			registeredCourse.ExamMarksPolicyCourseCategory = (byte)this.CourseCategoryEnum;
		}
	}
}