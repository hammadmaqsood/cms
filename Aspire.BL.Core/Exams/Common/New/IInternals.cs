﻿using Aspire.Model.Entities;
using System.Collections.Generic;

namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IInternals
	{
		IEnumerable<string> CanNotUnlockInternalsReasons { get; }
		bool CanUnlockInternals { get; }
		IEnumerable<string> CanNotCompileInternalsReasons { get; }
		bool CanCompileInternals { get; }
		bool IsInternalsLocked { get; }
		decimal? GetCompiledInternalsMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType);
		(IEnumerable<string> compileFailureReasons, bool success) CompileInternals(OfferedCours.ExamCompilationTypes examCompilationTypeEnum, bool submit);
		(IEnumerable<string> unlockFailureReasons, bool success) UnlockInternals();
	}
}
