﻿using System.Collections.Generic;

namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IFinal
	{
		IEnumerable<string> CanNotEditFinalReasons { get; }
		bool CanEditFinal { get; }
		IEnumerable<string> CanNotUnlockFinalReasons { get; }
		bool CanUnlockFinal { get; }
		bool IsFinalLocked { get; }
		IReadOnlyList<IStudentMarks> GetFinalMarks();
		(IEnumerable<string> updateFailureReasons, bool success) UpdateFinal(IReadOnlyDictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)> marks, bool submit);
		(IEnumerable<string> unlockFailureReasons, bool success) UnlockFinal();
	}
}
