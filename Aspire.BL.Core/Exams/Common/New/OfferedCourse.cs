﻿using Aspire.BL.Core.Exams.Common.New.Converters;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.New
{
	[JsonObject(MemberSerialization.OptIn)]
	internal sealed class OfferedCourse : IOfferedCourse
	{
		internal OfferedCourse(IMarksSheet marksSheet, AspireContext aspireContext, int offeredCourseID)
		{
			this.MarksSheet = marksSheet;
			var offeredCourse = aspireContext.OfferedCourses.Where(oc => oc.OfferedCourseID == offeredCourseID)
				.Select(oc => new
				{
					oc.OfferedCourseID,
					oc.Cours.AdmissionOpenProgram.Program.InstituteID,
					oc.Cours.AdmissionOpenProgram.Program.Institute.InstituteName,
					oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
					oc.Cours.AdmissionOpenProgram.ProgramID,
					oc.Cours.AdmissionOpenProgramID,
					RoadmapSemesterID = oc.Cours.AdmissionOpenProgram.SemesterID,
					OfferedSemesterID = oc.SemesterID,
					oc.Cours.CourseID,
					oc.Cours.CourseCode,
					oc.Cours.Title,
					oc.Cours.CreditHours,
					oc.Cours.ContactHours,
					oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNoEnum = (SemesterNos)oc.SemesterNo,
					SectionEnum = (Sections)oc.Section,
					ShiftEnum = (Shifts)oc.Shift,
					oc.FacultyMemberID,
					FacultyMemberName = oc.FacultyMember.Name,
					FacultyMemberEmailAddress = oc.FacultyMember.Email,
					CourseCategoryEnum = (CourseCategories)oc.Cours.CourseCategory,
					CourseTypeEnum = (CourseTypes)oc.Cours.CourseType,
					oc.Visiting,
					oc.SpecialOffered,
					AssignmentsExamCompilationTypeEnum = (OfferedCours.ExamCompilationTypes?)oc.AssignmentsExamCompilationType,
					oc.AssignmentsMarksSubmitted,
					oc.AssignmentsMarksSubmittedDate,
					QuizzesExamCompilationTypeEnum = (OfferedCours.ExamCompilationTypes?)oc.QuizzesExamCompilationType,
					oc.QuizzesMarksSubmitted,
					oc.QuizzesMarksSubmittedDate,
					InternalsExamCompilationTypeEnum = (OfferedCours.ExamCompilationTypes?)oc.InternalsExamCompilationType,
					oc.InternalsMarksSubmitted,
					oc.InternalsMarksSubmittedDate,
					oc.MidMarksSubmitted,
					oc.MidMarksSubmittedDate,
					oc.FinalMarksSubmitted,
					oc.FinalMarksSubmittedDate,
					oc.MarksEntryCompleted,
					oc.MarksEntryLocked,
					oc.MarksEntryLockedDate,
					oc.SubmittedToHODDate,
					oc.SubmittedToCampusDate,
					oc.SubmittedToUniversityDate,
				}).Single();

			switch (offeredCourse.CourseCategoryEnum)
			{
				case CourseCategories.Course:
				case CourseCategories.Lab:
					{
						this.OfferedCourseID = offeredCourse.OfferedCourseID;
						this.InstituteID = offeredCourse.InstituteID;
						this.InstituteName = offeredCourse.InstituteName;
						this.DepartmentID = offeredCourse.DepartmentID;
						this.ProgramID = offeredCourse.ProgramID;
						this.AdmissionOpenProgramID = offeredCourse.AdmissionOpenProgramID;
						this.OfferedSemesterID = offeredCourse.OfferedSemesterID;
						this.RoadmapSemesterID = offeredCourse.RoadmapSemesterID;
						this.CourseID = offeredCourse.CourseID;
						this.CourseCode = offeredCourse.CourseCode;
						this.Title = offeredCourse.Title;
						this.CreditHours = offeredCourse.CreditHours;
						this.ContactHours = offeredCourse.ContactHours;
						this.ProgramAlias = offeredCourse.ProgramAlias;
						this.SemesterNoEnum = offeredCourse.SemesterNoEnum;
						this.SectionEnum = offeredCourse.SectionEnum;
						this.ShiftEnum = offeredCourse.ShiftEnum;
						this.FacultyMemberID = offeredCourse.FacultyMemberID;
						this.FacultyMemberName = offeredCourse.FacultyMemberName;
						this.CourseCategoryEnum = offeredCourse.CourseCategoryEnum;
						this.CourseTypeEnum = offeredCourse.CourseTypeEnum;
						this.Visiting = offeredCourse.Visiting;
						this.SpecialOffered = offeredCourse.SpecialOffered;

						this.AssignmentsExamCompilationTypeEnum = offeredCourse.AssignmentsExamCompilationTypeEnum;
						this.AssignmentsMarksSubmitted = offeredCourse.AssignmentsMarksSubmitted;
						this.AssignmentsMarksSubmittedDate = offeredCourse.AssignmentsMarksSubmittedDate;
						this.QuizzesExamCompilationTypeEnum = offeredCourse.QuizzesExamCompilationTypeEnum;
						this.QuizzesMarksSubmitted = offeredCourse.QuizzesMarksSubmitted;
						this.QuizzesMarksSubmittedDate = offeredCourse.QuizzesMarksSubmittedDate;
						this.InternalsExamCompilationTypeEnum = offeredCourse.InternalsExamCompilationTypeEnum;
						this.InternalsMarksSubmitted = offeredCourse.InternalsMarksSubmitted;
						this.InternalsMarksSubmittedDate = offeredCourse.InternalsMarksSubmittedDate;
						this.MidMarksSubmitted = offeredCourse.MidMarksSubmitted;
						this.MidMarksSubmittedDate = offeredCourse.MidMarksSubmittedDate;
						this.FinalMarksSubmitted = offeredCourse.FinalMarksSubmitted;
						this.FinalMarksSubmittedDate = offeredCourse.FinalMarksSubmittedDate;
						this.MarksEntryCompleted = offeredCourse.MarksEntryCompleted;
						this.MarksEntryLocked = offeredCourse.MarksEntryLocked;
						this.MarksEntryLockedDate = offeredCourse.MarksEntryLockedDate;
						this.SubmittedToHODDate = offeredCourse.SubmittedToHODDate;
						this.SubmittedToCampusDate = offeredCourse.SubmittedToCampusDate;
						this.SubmittedToUniversityDate = offeredCourse.SubmittedToUniversityDate;

						this.OfferedCourseExams = OfferedCourseExam.GetAllOfferedCourseExams(this, aspireContext);
						this.OfferedCourseExamAssignments = this.OfferedCourseExams.Where(oce => oce.ExamTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Assignment).ToList();
						this.OfferedCourseExamQuizzes = this.OfferedCourseExams.Where(oce => oce.ExamTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Quiz).ToList();
						this.RegisteredCourses = RegisteredCourse.GetAllRegisteredCourses(this, aspireContext);
						this.ExamMarksPolicy = New.ExamMarksPolicy.GetExamMarksPolicy(aspireContext, offeredCourse.AdmissionOpenProgramID, offeredCourse.OfferedSemesterID);
					}
					break;
				case CourseCategories.Internship:
				case CourseCategories.Project:
				case CourseCategories.Thesis:
					throw new NotSupportedException($"{offeredCourse.CourseCategoryEnum} not supported.");
				default:
					throw new NotImplementedEnumException(offeredCourse.CourseCategoryEnum);
			}
		}

		public IMarksSheet MarksSheet { get; }

		[JsonProperty]
		public int OfferedCourseID { get; }

		public int InstituteID { get; }

		public string InstituteName { get; }

		public int? DepartmentID { get; }

		public int ProgramID { get; }

		public int AdmissionOpenProgramID { get; }

		[JsonProperty("Roadmap Semester")]
		[JsonConverter(typeof(SemesterIDConverter))]
		public short RoadmapSemesterID { get; }

		[JsonProperty("Offered Semester")]
		[JsonConverter(typeof(SemesterIDConverter))]
		public short OfferedSemesterID { get; }

		[JsonProperty]
		public int CourseID { get; }

		[JsonProperty("Course Code")]
		public string CourseCode { get; }

		[JsonProperty]
		public string Title { get; }

		[JsonProperty("Credit Hours")]
		public decimal CreditHours { get; }

		[JsonProperty("Contact Hours")]
		public decimal ContactHours { get; }

		public string ProgramAlias { get; }

		public SemesterNos SemesterNoEnum { get; }

		public Sections SectionEnum { get; }

		public Shifts ShiftEnum { get; }

		[JsonProperty]
		public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNoEnum, this.SectionEnum, this.ShiftEnum);

		[JsonProperty]
		public int? FacultyMemberID { get; }

		[JsonProperty("Faculty Member Name")]
		public string FacultyMemberName { get; }

		[JsonProperty("Course Category")]
		[JsonConverter(typeof(EnumFullNameConverter))]
		public CourseCategories CourseCategoryEnum { get; }

		[JsonProperty("Course Type")]
		[JsonConverter(typeof(EnumFullNameConverter))]
		public CourseTypes CourseTypeEnum { get; }

		[JsonProperty("Visiting Course")]
		[JsonConverter(typeof(YesNoConverter))]
		public bool Visiting { get; }

		[JsonProperty("Special Offered")]
		[JsonConverter(typeof(YesNoConverter))]
		public bool SpecialOffered { get; }

		[JsonProperty("Assignments Compilation Type")]
		[JsonConverter(typeof(EnumFullNameConverter))]
		public OfferedCours.ExamCompilationTypes? AssignmentsExamCompilationTypeEnum { get; }

		private static string GetMarksSubmittedString(bool submitted, DateTime? submittedOn)
		{
			return submitted && submittedOn != null ? submittedOn.Value.ToString(CultureInfo.CurrentCulture) : submitted.ToYesNo();
		}

		private static string GetMarksSubmittedString(DateTime? submittedOn)
		{
			return submittedOn != null ? submittedOn.Value.ToString(CultureInfo.CurrentCulture) : false.ToYesNo();
		}

		[JsonProperty("Assignments Marks Submitted")]
		[JsonConverter(typeof(YesNoConverter))]
		public bool AssignmentsMarksSubmitted { get; }

		[JsonProperty("Assignments Marks Submitted On")]
		[JsonConverter(typeof(DateTimeConverter))]
		public DateTime? AssignmentsMarksSubmittedDate { get; }

		public string AssignmentsMarksSubmittedString => GetMarksSubmittedString(this.AssignmentsMarksSubmitted, this.AssignmentsMarksSubmittedDate);

		[JsonProperty("Quizzes Compilation Type")]
		[JsonConverter(typeof(EnumFullNameConverter))]
		public OfferedCours.ExamCompilationTypes? QuizzesExamCompilationTypeEnum { get; }

		[JsonProperty("Quizzes Marks Submitted")]
		[JsonConverter(typeof(YesNoConverter))]
		public bool QuizzesMarksSubmitted { get; }

		[JsonProperty("Quizzes Marks Submitted On")]
		[JsonConverter(typeof(DateTimeConverter))]
		public DateTime? QuizzesMarksSubmittedDate { get; }

		public string QuizzesMarksSubmittedString => GetMarksSubmittedString(this.QuizzesMarksSubmitted, this.QuizzesMarksSubmittedDate);

		[JsonProperty("Internals Compilation Type")]
		[JsonConverter(typeof(EnumFullNameConverter))]
		public OfferedCours.ExamCompilationTypes? InternalsExamCompilationTypeEnum { get; }

		[JsonProperty("Internals Marks Submitted")]
		[JsonConverter(typeof(YesNoConverter))]
		public bool InternalsMarksSubmitted { get; }

		[JsonProperty("Internals Marks Submitted On")]
		[JsonConverter(typeof(DateTimeConverter))]
		public DateTime? InternalsMarksSubmittedDate { get; }

		public string InternalsMarksSubmittedString => GetMarksSubmittedString(this.InternalsMarksSubmitted, this.InternalsMarksSubmittedDate);

		[JsonProperty("Mid Marks Submitted")]
		[JsonConverter(typeof(YesNoConverter))]
		public bool MidMarksSubmitted { get; }

		[JsonProperty("Mid Marks Submitted On")]
		[JsonConverter(typeof(DateTimeConverter))]
		public DateTime? MidMarksSubmittedDate { get; }

		public string MidMarksSubmittedString => GetMarksSubmittedString(this.MidMarksSubmitted, this.MidMarksSubmittedDate);

		[JsonProperty("Final Marks Submitted")]
		[JsonConverter(typeof(YesNoConverter))]
		public bool FinalMarksSubmitted { get; }

		[JsonProperty("Final Marks Submitted On")]
		[JsonConverter(typeof(DateTimeConverter))]
		public DateTime? FinalMarksSubmittedDate { get; }

		public string FinalMarksSubmittedString => GetMarksSubmittedString(this.FinalMarksSubmitted, this.FinalMarksSubmittedDate);

		[JsonProperty("Marks Entry Completed")]
		[JsonConverter(typeof(YesNoConverter))]
		public bool MarksEntryCompleted { get; }

		public string MarksEntryCompletedString => this.MarksEntryCompleted.ToYesNo();

		[JsonProperty("Marks Entry Locked")]
		[JsonConverter(typeof(YesNoConverter))]
		public bool MarksEntryLocked { get; }

		[JsonProperty("Marks Entry Locked On")]
		[JsonConverter(typeof(DateTimeConverter))]
		public DateTime? MarksEntryLockedDate { get; }

		public string MarksEntryLockedString => GetMarksSubmittedString(this.MarksEntryLocked, this.MarksEntryLockedDate);

		[JsonProperty("Submitted To HOD On")]
		[JsonConverter(typeof(DateTimeConverter))]
		public DateTime? SubmittedToHODDate { get; }

		public string SubmittedToHODString => GetMarksSubmittedString(this.SubmittedToHODDate);

		[JsonProperty("Submitted To Campus On")]
		[JsonConverter(typeof(DateTimeConverter))]
		public DateTime? SubmittedToCampusDate { get; }

		public string SubmittedToCampusString => GetMarksSubmittedString(this.SubmittedToCampusDate);

		[JsonProperty("Submitted To BUHO Exams On")]
		[JsonConverter(typeof(DateTimeConverter))]
		public DateTime? SubmittedToUniversityDate { get; }

		public string SubmittedToUniversityString => GetMarksSubmittedString(this.SubmittedToUniversityDate);

		public IExamMarksPolicy ExamMarksPolicy { get; }

		[JsonProperty]
		private int? ExamMarksPolicyID => this.ExamMarksPolicy?.ExamMarksPolicyID;

		public IReadOnlyList<IOfferedCourseExam> OfferedCourseExams { get; }

		[JsonProperty("Assignments")]
		public IReadOnlyList<IOfferedCourseExam> OfferedCourseExamAssignments { get; }

		[JsonProperty("Quizzes")]
		public IReadOnlyList<IOfferedCourseExam> OfferedCourseExamQuizzes { get; }

		[JsonProperty("Exam Marks Policies")]
		private IReadOnlyList<IExamMarksPolicy> ExamMarksPolicies => this.RegisteredCourses.Select(rc => rc.ExamMarksPolicyToBeApplied).Where(emp => emp != null).GroupBy(emp => emp.ExamMarksPolicyID).Select(g => g.First()).ToList();

		[JsonProperty("Registered Courses")]
		public IReadOnlyList<IRegisteredCourse> RegisteredCourses { get; }

		public string OfferedCourseInfo => $"Code: {this.CourseCode}, Title: {this.Title}, Roadmap: {this.RoadmapSemesterID.ToSemesterString()}, Program: {this.ProgramAlias}, Offered Semester: {this.OfferedSemesterID.ToSemesterString()}, Offered Class: {AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNoEnum, this.SectionEnum, this.ShiftEnum)}";
	}
}
