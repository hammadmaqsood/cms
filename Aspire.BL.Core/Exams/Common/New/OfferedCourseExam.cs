﻿using Aspire.BL.Core.Exams.Common.New.Converters;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.New
{
	[JsonObject(MemberSerialization.OptIn)]
	internal sealed class OfferedCourseExam : IOfferedCourseExam
	{
		public static IReadOnlyList<IOfferedCourseExam> GetAllOfferedCourseExams(IOfferedCourse offeredCourse, AspireContext aspireContext)
		{
			switch (offeredCourse.MarksSheet.MarksEntryTypeEnum)
			{
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					var offeredCourseExams = aspireContext.OfferedCourseExams
						.Where(oce => oce.OfferedCourseID == offeredCourse.OfferedCourseID)
						.Where(oce => oce.ExamType == (byte)Model.Entities.OfferedCourseExam.ExamTypes.Assignment || oce.ExamType == (byte)Model.Entities.OfferedCourseExam.ExamTypes.Quiz)
						.Select(oce => new OfferedCourseExam
						{
							OfferedCourseExamID = oce.OfferedCourseExamID,
							ExamDate = oce.ExamDate,
							ExamNo = oce.ExamNo,
							ExamTypeEnum = (Model.Entities.OfferedCourseExam.ExamTypes)oce.ExamType,
							TotalMarks = oce.TotalMarks,
							Remarks = oce.Remarks,
							Locked = oce.Locked,
							LockedDate = oce.LockedDate
						})
						.ToList();
					offeredCourseExams.ForEach(oce => oce.OfferedCourse = offeredCourse);
					return offeredCourseExams;
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
					return new IOfferedCourseExam[] { };
				default:
					throw new NotImplementedEnumException(offeredCourse.MarksSheet.MarksEntryTypeEnum);
			}
		}

		public IOfferedCourse OfferedCourse { get; private set; }

		[JsonProperty]
		public int OfferedCourseExamID { get; private set; }

		[JsonProperty("Type")]
		[JsonConverter(typeof(EnumFullNameConverter))]
		public Model.Entities.OfferedCourseExam.ExamTypes ExamTypeEnum { get; private set; }

		[JsonProperty("Date")]
		[JsonConverter(typeof(DateTimeConverter))]
		public DateTime ExamDate { get; private set; }

		[JsonProperty("No.")]
		public byte ExamNo { get; private set; }

		[JsonProperty]
		[JsonConverter(typeof(MarksConverter))]
		public decimal TotalMarks { get; private set; }

		[JsonProperty("Remarks")]
		public string Remarks { get; private set; }

		[JsonProperty("Submitted")]
		[JsonConverter(typeof(YesNoConverter))]
		public bool Locked { get; private set; }

		[JsonProperty("Submitted On")]
		[JsonConverter(typeof(DateTimeConverter))]
		public DateTime? LockedDate { get; private set; }

		public IEnumerable<string> CanNotEditAssignmentReasons => this.OfferedCourse.MarksSheet.CanNotEditOrDeleteAssignmentReasons(this.OfferedCourseExamID);

		public bool CanEditAssignment => this.CanNotEditAssignmentReasons?.Any() != true;

		public IEnumerable<string> CanNotEditQuizReasons => this.OfferedCourse.MarksSheet.CanNotEditOrDeleteQuizReasons(this.OfferedCourseExamID);

		public bool CanEditQuiz => this.CanNotEditQuizReasons?.Any() != true;

		public (List<string> errors, List<string> warnings) Validate()
		{
			var warnings = new List<string>();
			if (this.TotalMarks <= 0)
				warnings.Add($"Total Marks can not be zero or less than zero. {this.ExamTypeEnum}-{this.ExamNo}, Offered Course: {this.OfferedCourse.OfferedCourseInfo}.");
			return (new List<string>(), warnings.Distinct().ToList());
		}
	}
}
