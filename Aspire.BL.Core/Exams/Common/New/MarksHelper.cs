﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.ExecutiveInformation.Executive.Exams;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

// ReSharper disable PossibleMultipleEnumeration

namespace Aspire.BL.Core.Exams.Common.New
{
	public static class MarksHelper
	{
		private static (IMarksSheet marksSheet, string errorMessage)? GetMarksSheet(this AspireContext aspireContext, int offeredCourseID, Guid loginSessionGuid)
		{
			var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
			var offeredCourse = aspireContext.OfferedCourses
				.Where(oc => oc.OfferedCourseID == offeredCourseID)
				.Select(oc => new
				{
					oc.Cours.AdmissionOpenProgram.Program.InstituteID,
					oc.Cours.AdmissionOpenProgram.Program.Institute.InstituteName,
					oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
					oc.Cours.AdmissionOpenProgram.ProgramID,
					oc.Cours.AdmissionOpenProgramID,
					CourseCategoryEnum = (CourseCategories)oc.Cours.CourseCategory,
					oc.FacultyMemberID,
					oc.OfferedCourseID,
					oc.SemesterID,
				}).SingleOrDefault();
			if (offeredCourse == null)
				return null;

			var examMarksPolicy = aspireContext.AdmissionOpenProgramExamMarksPolicies
				.Where(aopemp => aopemp.AdmissionOpenProgramID == offeredCourse.AdmissionOpenProgramID && aopemp.SemesterID == offeredCourse.SemesterID)
				.Select(aopemp => new
				{
					CourseMarksEntryTypeEnum = (Model.Entities.ExamMarksPolicy.MarksEntryTypes)aopemp.ExamMarksPolicy.CourseMarksEntryType,
					LabMarksEntryTypeEnum = (Model.Entities.ExamMarksPolicy.MarksEntryTypes)aopemp.ExamMarksPolicy.LabMarksEntryType
				}).SingleOrDefault();
			if (examMarksPolicy == null)
				return (null, "Exam Marks Policy not found for Offered Course.");

			switch (loginHistory.UserTypeEnum)
			{
				case UserTypes.Staff:
					var staffGroupPermission = aspireContext.DemandStaffPermissions(loginHistory.LoginSessionGuid, StaffPermissions.Exams.Module, new[] { UserGroupPermission.PermissionValues.Allowed }, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID);
					switch (offeredCourse.CourseCategoryEnum)
					{
						case CourseCategories.Course:
							switch (examMarksPolicy.CourseMarksEntryTypeEnum)
							{
								case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
									return (new MarksEntryTypes.StaffAssignmentsQuizzesMidFinalMarksSheet(aspireContext, offeredCourse.OfferedCourseID, staffGroupPermission.UserLoginHistoryID, staffGroupPermission.LoginSessionGuid), null);
								case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
									return (new MarksEntryTypes.StaffAssignmentCompileAssignmentsQuizCompileQuizzesMidFinalMarksSheet(aspireContext, offeredCourse.OfferedCourseID, staffGroupPermission.UserLoginHistoryID, staffGroupPermission.LoginSessionGuid), null);
								case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
									return (new MarksEntryTypes.StaffAssignmentQuizCompileInternalsMidFinalMarksSheet(aspireContext, offeredCourse.OfferedCourseID, staffGroupPermission.UserLoginHistoryID, staffGroupPermission.LoginSessionGuid), null);
								case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
									return (new MarksEntryTypes.StaffFinalMarksSheet(aspireContext, offeredCourse.OfferedCourseID, staffGroupPermission.UserLoginHistoryID, staffGroupPermission.LoginSessionGuid), null);
								default:
									throw new NotImplementedEnumException(examMarksPolicy.CourseMarksEntryTypeEnum);
							}
						case CourseCategories.Lab:
							switch (examMarksPolicy.LabMarksEntryTypeEnum)
							{
								case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
									return (new MarksEntryTypes.StaffAssignmentsQuizzesMidFinalMarksSheet(aspireContext, offeredCourse.OfferedCourseID, staffGroupPermission.UserLoginHistoryID, staffGroupPermission.LoginSessionGuid), null);
								case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
									return (new MarksEntryTypes.StaffAssignmentCompileAssignmentsQuizCompileQuizzesMidFinalMarksSheet(aspireContext, offeredCourse.OfferedCourseID, staffGroupPermission.UserLoginHistoryID, staffGroupPermission.LoginSessionGuid), null);
								case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
									return (new MarksEntryTypes.StaffAssignmentQuizCompileInternalsMidFinalMarksSheet(aspireContext, offeredCourse.OfferedCourseID, staffGroupPermission.UserLoginHistoryID, staffGroupPermission.LoginSessionGuid), null);
								case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
									return (new MarksEntryTypes.StaffFinalMarksSheet(aspireContext, offeredCourse.OfferedCourseID, staffGroupPermission.UserLoginHistoryID, staffGroupPermission.LoginSessionGuid), null);
								default:
									throw new NotImplementedEnumException(examMarksPolicy.CourseMarksEntryTypeEnum);
							}
						case CourseCategories.Internship:
						case CourseCategories.Project:
						case CourseCategories.Thesis:
							return (null, $"Marks Entry is not allowed for Course Category \"{offeredCourse.CourseCategoryEnum}\". Please contact Exam Section.");
						default:
							throw new NotImplementedEnumException(offeredCourse.CourseCategoryEnum);
					}
				case UserTypes.Faculty:
					if (offeredCourse.FacultyMemberID != loginHistory.FacultyMemberID)
						return null;
					switch (offeredCourse.CourseCategoryEnum)
					{
						case CourseCategories.Course:
							switch (examMarksPolicy.CourseMarksEntryTypeEnum)
							{
								case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
									return (new MarksEntryTypes.FacultyMemberAssignmentsQuizzesMidFinalMarksSheet(aspireContext, offeredCourse.OfferedCourseID, loginHistory.UserLoginHistoryID, loginHistory.LoginSessionGuid), null);
								case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
									return (new MarksEntryTypes.FacultyMemberAssignmentCompileAssignmentsQuizCompileQuizzesMidFinalMarksSheet(aspireContext, offeredCourse.OfferedCourseID, loginHistory.UserLoginHistoryID, loginHistory.LoginSessionGuid), null);
								case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
									return (new MarksEntryTypes.FacultyMemberAssignmentQuizCompileInternalsMidFinalMarksSheet(aspireContext, offeredCourse.OfferedCourseID, loginHistory.UserLoginHistoryID, loginHistory.LoginSessionGuid), null);
								case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
									return (new MarksEntryTypes.FacultyMemberFinalMarksSheet(aspireContext, offeredCourse.OfferedCourseID, loginHistory.UserLoginHistoryID, loginHistory.LoginSessionGuid), null);
								default:
									throw new NotImplementedEnumException(examMarksPolicy.CourseMarksEntryTypeEnum);
							}
						case CourseCategories.Lab:
							switch (examMarksPolicy.LabMarksEntryTypeEnum)
							{
								case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
									return (new MarksEntryTypes.FacultyMemberAssignmentsQuizzesMidFinalMarksSheet(aspireContext, offeredCourse.OfferedCourseID, loginHistory.UserLoginHistoryID, loginHistory.LoginSessionGuid), null);
								case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
									return (new MarksEntryTypes.FacultyMemberAssignmentCompileAssignmentsQuizCompileQuizzesMidFinalMarksSheet(aspireContext, offeredCourse.OfferedCourseID, loginHistory.UserLoginHistoryID, loginHistory.LoginSessionGuid), null);
								case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
									return (new MarksEntryTypes.FacultyMemberAssignmentQuizCompileInternalsMidFinalMarksSheet(aspireContext, offeredCourse.OfferedCourseID, loginHistory.UserLoginHistoryID, loginHistory.LoginSessionGuid), null);
								case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
									return (new MarksEntryTypes.FacultyMemberFinalMarksSheet(aspireContext, offeredCourse.OfferedCourseID, loginHistory.UserLoginHistoryID, loginHistory.LoginSessionGuid), null);
								default:
									throw new NotImplementedEnumException(examMarksPolicy.CourseMarksEntryTypeEnum);
							}
						case CourseCategories.Internship:
						case CourseCategories.Project:
						case CourseCategories.Thesis:
							return (null, $"Marks Entry is not allowed for Course Category \"{offeredCourse.CourseCategoryEnum}\". Please contact Exam Section.");
						default:
							throw new NotImplementedEnumException(offeredCourse.CourseCategoryEnum);
					}
				default:
					throw new NotImplementedEnumException(loginHistory.UserTypeEnum);
			}
		}

		public static (IMarksSheet marksSheet, string errorMessage)? GetMarksSheet(int offeredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.GetMarksSheet(offeredCourseID, loginSessionGuid);
			}
		}

		private static (IMarksSheet marksSheet, IEnumerable<string> errorMessages)? GetValidatedMarksSheet(this AspireContext aspireContext, int offeredCourseID, Guid loginSessionGuid)
		{
			var result = aspireContext.GetMarksSheet(offeredCourseID, loginSessionGuid);
			if (result == null)
				return null;
			if (result.Value.errorMessage != null)
				return ((IMarksSheet)null, new[] { result.Value.errorMessage });
			var (errors, _) = result.Value.marksSheet.Validate();
			return (result.Value.marksSheet, errors);
		}

		public static (IEnumerable<string> addFailureReasons, int? offeredCourseExamID)? AddOfferedCourseExam(int offeredCourseID, Model.Entities.OfferedCourseExam.ExamTypes examTypeEnum, DateTime examDate, byte examNo, decimal totalMarks, string remarks, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, null);
					IEnumerable<string> addFailureReasons; int? offeredCourseExamID;
					switch (examTypeEnum)
					{
						case Model.Entities.OfferedCourseExam.ExamTypes.Assignment:
							(addFailureReasons, offeredCourseExamID) = result.Value.marksSheet.AddAssignment(examDate, examNo, totalMarks, remarks);
							break;
						case Model.Entities.OfferedCourseExam.ExamTypes.Quiz:
							(addFailureReasons, offeredCourseExamID) = result.Value.marksSheet.AddQuiz(examDate, examNo, totalMarks, remarks);
							break;
						default:
							throw new NotImplementedEnumException(examTypeEnum);
					}
					if (!addFailureReasons.Any() && offeredCourseExamID != null)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return (addFailureReasons, offeredCourseExamID);
				}
			}
		}

		public static (IEnumerable<string> deleteFailureReasons, bool success)? DeleteOfferedCourseExam(int offeredCourseID, Model.Entities.OfferedCourseExam.ExamTypes examTypeEnum, int offeredCourseExamID, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, false);
					IEnumerable<string> deleteFailureReasons;
					bool success;
					switch (examTypeEnum)
					{
						case Model.Entities.OfferedCourseExam.ExamTypes.Assignment:
							(deleteFailureReasons, success) = result.Value.marksSheet.DeleteAssignment(offeredCourseExamID);
							break;
						case Model.Entities.OfferedCourseExam.ExamTypes.Quiz:
							(deleteFailureReasons, success) = result.Value.marksSheet.DeleteQuiz(offeredCourseExamID);
							break;
						default:
							throw new NotImplementedEnumException(examTypeEnum);
					}
					if (!deleteFailureReasons.Any() && success)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return (deleteFailureReasons, success);
				}
			}
		}

		public static (IEnumerable<string> updateFailureReasons, bool success)? UpdateOfferedCourseExam(int offeredCourseID, Model.Entities.OfferedCourseExam.ExamTypes examTypeEnum, int offeredCourseExamID, DateTime examDate, byte examNo, string remarks, IReadOnlyDictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)> marks, bool submit, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, false);
					IEnumerable<string> updateFailureReasons;
					bool success;
					switch (examTypeEnum)
					{
						case Model.Entities.OfferedCourseExam.ExamTypes.Assignment:
							(updateFailureReasons, success) = result.Value.marksSheet.UpdateAssignment(offeredCourseExamID, examDate, examNo, remarks, marks, submit);
							break;
						case Model.Entities.OfferedCourseExam.ExamTypes.Quiz:
							(updateFailureReasons, success) = result.Value.marksSheet.UpdateQuiz(offeredCourseExamID, examDate, examNo, remarks, marks, submit);
							break;
						default:
							throw new NotImplementedEnumException(examTypeEnum);
					}
					if (!updateFailureReasons.Any() && success)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return (updateFailureReasons, success);
				}
			}
		}

		public static (IEnumerable<string> compileFailureReasons, bool success)? CompileExamMarks(int offeredCourseID, CompileExamMarksTypes compileExamMarksType, OfferedCours.ExamCompilationTypes examCompilationType, bool submit, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, false);
					IEnumerable<string> compileFailureReasons;
					bool success;
					switch (compileExamMarksType)
					{
						case CompileExamMarksTypes.Assignments:
							(compileFailureReasons, success) = result.Value.marksSheet.CompileAssignments(examCompilationType, submit);
							break;
						case CompileExamMarksTypes.Quizzes:
							(compileFailureReasons, success) = result.Value.marksSheet.CompileQuizzes(examCompilationType, submit);
							break;
						case CompileExamMarksTypes.Internals:
							(compileFailureReasons, success) = result.Value.marksSheet.CompileInternals(examCompilationType, submit);
							break;
						default:
							throw new NotImplementedEnumException(compileExamMarksType);
					}
					if (!compileFailureReasons.Any() && success)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return (compileFailureReasons, success);
				}
			}
		}

		public static (IEnumerable<string> updateFailureReasons, bool success)? UpdateExamMarks(int offeredCourseID, ExamMarksTypes examMarksTypeEnum, IReadOnlyDictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)> marks, bool submit, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, false);
					IEnumerable<string> updateFailureReasons;
					bool success;
					switch (examMarksTypeEnum)
					{
						case ExamMarksTypes.Assignments:
							(updateFailureReasons, success) = result.Value.marksSheet.UpdateAssignments(marks, submit);
							break;
						case ExamMarksTypes.Quizzes:
							(updateFailureReasons, success) = result.Value.marksSheet.UpdateQuizzes(marks, submit);
							break;
						case ExamMarksTypes.Internals:
							throw new InvalidOperationException();
						case ExamMarksTypes.Mid:
							(updateFailureReasons, success) = result.Value.marksSheet.UpdateMid(marks, submit);
							break;
						case ExamMarksTypes.Final:
							(updateFailureReasons, success) = result.Value.marksSheet.UpdateFinal(marks, submit);
							break;
						default:
							throw new NotImplementedEnumException(examMarksTypeEnum);
					}
					if (!updateFailureReasons.Any() && success)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return (updateFailureReasons, success);
				}
			}
		}

		public static (IEnumerable<string> unlockFailureReasons, bool success)? UnlockExamMarks(int offeredCourseID, ExamMarksTypes examMarksTypeEnum, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, false);
					IEnumerable<string> unlockFailureReasons;
					bool success;
					switch (examMarksTypeEnum)
					{
						case ExamMarksTypes.Assignments:
							(unlockFailureReasons, success) = result.Value.marksSheet.UnlockAssignments();
							break;
						case ExamMarksTypes.Quizzes:
							(unlockFailureReasons, success) = result.Value.marksSheet.UnlockQuizzes();
							break;
						case ExamMarksTypes.Internals:
							(unlockFailureReasons, success) = result.Value.marksSheet.UnlockInternals();
							break;
						case ExamMarksTypes.Mid:
							(unlockFailureReasons, success) = result.Value.marksSheet.UnlockMid();
							break;
						case ExamMarksTypes.Final:
							(unlockFailureReasons, success) = result.Value.marksSheet.UnlockFinal();
							break;
						default:
							throw new NotImplementedEnumException(examMarksTypeEnum);
					}
					if (!unlockFailureReasons.Any() && success)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return (unlockFailureReasons, success);
				}
			}
		}

		public static (IEnumerable<string> unlockFailureReasons, bool success)? UnlockOfferedCourseExamMarks(int offeredCourseID, int offeredCourseExamID, Model.Entities.OfferedCourseExam.ExamTypes examTypeEnum, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, false);
					IEnumerable<string> unlockFailureReasons;
					bool success;
					switch (examTypeEnum)
					{
						case Model.Entities.OfferedCourseExam.ExamTypes.Assignment:
							(unlockFailureReasons, success) = result.Value.marksSheet.UnlockAssignment(offeredCourseExamID);
							break;
						case Model.Entities.OfferedCourseExam.ExamTypes.Quiz:
							(unlockFailureReasons, success) = result.Value.marksSheet.UnlockQuiz(offeredCourseExamID);
							break;
						default:
							throw new NotImplementedEnumException(examTypeEnum);
					}
					if (!unlockFailureReasons.Any() && success)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return (unlockFailureReasons, success);
				}
			}
		}

		public static (IEnumerable<string> compileFailureReasons, bool success)? CompileTotalAndGrades(int offeredCourseID, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, false);
					var (compileFailureReasons, success) = result.Value.marksSheet.CompileTotalAndGrades();
					if (!compileFailureReasons.Any() && success)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return (compileFailureReasons, success);
				}
			}
		}

		public static (IEnumerable<string> lockFailureReasons, bool success)? LockMarksEntry(int offeredCourseID, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, false);
					var (lockFailureReasons, success) = result.Value.marksSheet.LockMarksEntry();
					if (!lockFailureReasons.Any() && success)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return (lockFailureReasons, success);
				}
			}
		}

		public static (IEnumerable<string> unlockFailureReasons, bool success)? UnlockMarksEntry(int offeredCourseID, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, false);
					var (unlockFailureReasons, success) = result.Value.marksSheet.UnlockMarksEntry();
					if (!unlockFailureReasons.Any() && success)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return (unlockFailureReasons, success);
				}
			}
		}

		public static (IEnumerable<string> submitToHODFailureReasons, bool success)? SubmitToHOD(int offeredCourseID, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, false);
					var (submitToHODFailureReasons, success) = result.Value.marksSheet.SubmitToHOD();
					if (!submitToHODFailureReasons.Any() && success)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return (submitToHODFailureReasons, success);
				}
			}
		}

		public static (IEnumerable<string> revertSubmitToHODFailureReasons, bool success)? RevertSubmitToHOD(int offeredCourseID, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, false);
					var (revertSubmitToHODFailureReasons, success) = result.Value.marksSheet.RevertSubmitToHOD();
					if (!revertSubmitToHODFailureReasons.Any() && success)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return (revertSubmitToHODFailureReasons, success);
				}
			}
		}

		public static (IEnumerable<string> submitToCampusFailureReasons, bool success)? SubmitToCampus(int offeredCourseID, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, false);
					var (submitToCampusFailureReasons, success) = result.Value.marksSheet.SubmitToCampus();
					if (!submitToCampusFailureReasons.Any() && success)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return (submitToCampusFailureReasons, success);
				}
			}
		}

		public static (IEnumerable<string> revertSubmitToCampusFailureReasons, bool success)? RevertSubmitToCampus(int offeredCourseID, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, false);
					var (revertSubmitToCampusFailureReasons, success) = result.Value.marksSheet.RevertSubmitToCampus();
					if (!revertSubmitToCampusFailureReasons.Any() && success)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return (revertSubmitToCampusFailureReasons, success);
				}
			}
		}

		public static (IEnumerable<string> submitToUniversityFailureReasons, bool success)? SubmitToUniversity(int offeredCourseID, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, false);
					var (submitToUniversityFailureReasons, success) = result.Value.marksSheet.SubmitToUniversity();
					if (!submitToUniversityFailureReasons.Any() && success)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return (submitToUniversityFailureReasons, success);
				}
			}
		}

		public static (IEnumerable<string> revertSubmitToUniversityFailureReasons, bool success)? RevertSubmitToUniversity(int offeredCourseID, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, false);
					var (revertSubmitToUniversityFailureReasons, success) = result.Value.marksSheet.RevertSubmitToUniversity();
					if (!revertSubmitToUniversityFailureReasons.Any() && success)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return (revertSubmitToUniversityFailureReasons, success);
				}
			}
		}

		[Serializable]
		public sealed class MarksSheetChange
		{
			public Guid MarksSheetChangesID { get; set; }
			public DateTime ActionDate { get; set; }
			public Guid ActionType { get; set; }
			public string ActionTypeFullName => this.ActionType.GetFullName();
			public string ChangedByUserName { get; set; }
			public UserTypes ChangeByUserTypeEnum { get; set; }
			public string ChangedBy => $"{this.ChangedByUserName} ({this.ChangeByUserTypeEnum.ToFullName()})";
			public string OldMarksSheetJSON { get; set; }
			public string NewMarksSheetJSON { get; set; }
		}

		public static (List<MarksSheetChange> MarksSheetChanges, int VirtualItemCount)? GetMarksSheetChanges(int offeredCourseID, int pageIndex, int pageSize, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var offeredCourse = aspireContext.OfferedCourses
					.Where(oc => oc.OfferedCourseID == offeredCourseID)
					.Select(oc => new
					{
						oc.Cours.AdmissionOpenProgram.Program.InstituteID,
						oc.Cours.AdmissionOpenProgram.Program.Institute.InstituteName,
						oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
						oc.Cours.AdmissionOpenProgram.ProgramID,
						oc.Cours.AdmissionOpenProgramID,
						CourseCategoryEnum = (CourseCategories)oc.Cours.CourseCategory,
						oc.FacultyMemberID,
						oc.OfferedCourseID,
						oc.SemesterID,
					}).SingleOrDefault();
				if (offeredCourse == null)
					return null;
				var marksSheetChangesQuery = from msc in aspireContext.MarksSheetChanges
											 join ulh in aspireContext.UserLoginHistories on msc.LoginSessionGuid equals ulh.LoginSessionGuid
											 where msc.OfferedCourseID == offeredCourse.OfferedCourseID
											 orderby msc.ActionDate descending
											 select new MarksSheetChange
											 {
												 MarksSheetChangesID = msc.MarksSheetChangesID,
												 ChangedByUserName = ulh.UserName,
												 ChangeByUserTypeEnum = (UserTypes)ulh.UserType,
												 ActionDate = msc.ActionDate,
												 ActionType = msc.ActionType
											 };

				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandStaffPermissions(loginHistory.LoginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID);
						break;
					case UserTypes.Faculty:
						if (offeredCourse.FacultyMemberID != loginHistory.FacultyMemberID)
							return null;
						break;
					default:
						throw new NotImplementedEnumException(loginHistory.UserTypeEnum);
				}
				return (marksSheetChangesQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList(), marksSheetChangesQuery.Count());
			}
		}

		public static List<MarksSheetChange> GetMarksSheetChange(Guid? leftMarksSheetChangeID, Guid rightMarksSheetChangeID, int offeredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var offeredCourse = aspireContext.OfferedCourses
					.Where(oc => oc.OfferedCourseID == offeredCourseID)
					.Select(oc => new
					{
						oc.Cours.AdmissionOpenProgram.Program.InstituteID,
						oc.Cours.AdmissionOpenProgram.Program.Institute.InstituteName,
						oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
						oc.Cours.AdmissionOpenProgram.ProgramID,
						oc.Cours.AdmissionOpenProgramID,
						CourseCategoryEnum = (CourseCategories)oc.Cours.CourseCategory,
						oc.FacultyMemberID,
						oc.OfferedCourseID,
						oc.SemesterID,
					}).SingleOrDefault();
				if (offeredCourse == null)
					return null;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						aspireContext.DemandStaffPermissions(loginHistory.LoginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID);
						break;
					case UserTypes.Faculty:
						if (offeredCourse.FacultyMemberID != loginHistory.FacultyMemberID)
							return null;
						break;
					default:
						throw new NotImplementedEnumException(loginHistory.UserTypeEnum);
				}
				var marksSheetChangesQuery = from msc in aspireContext.MarksSheetChanges
											 join ulh in aspireContext.UserLoginHistories on msc.LoginSessionGuid equals ulh.LoginSessionGuid
											 where msc.OfferedCourseID == offeredCourse.OfferedCourseID && (msc.MarksSheetChangesID == leftMarksSheetChangeID || msc.MarksSheetChangesID == rightMarksSheetChangeID)
											 orderby msc.ActionDate descending
											 select new MarksSheetChange
											 {
												 MarksSheetChangesID = msc.MarksSheetChangesID,
												 ChangedByUserName = ulh.UserName,
												 ChangeByUserTypeEnum = (UserTypes)ulh.UserType,
												 ActionDate = msc.ActionDate,
												 ActionType = msc.ActionType,
												 OldMarksSheetJSON = msc.OldMarksSheetJSON,
												 NewMarksSheetJSON = msc.NewMarksSheetJSON
											 };
				return marksSheetChangesQuery.ToList();
			}
		}

		public static (IEnumerable<string> unlockFailureReasons, bool success)? UnlockMarksEntryForRegisteredCourse(int offeredCourseID, int registeredCourseID, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, false);
					var registeredCourse = result.Value.marksSheet.OfferedCourse.RegisteredCourses.SingleOrDefault(rc => rc.RegisteredCourseID == registeredCourseID);
					if (registeredCourse == null)
						return null;
					var (unlockFailureReasons, success) = result.Value.marksSheet.UnlockMarksEntryForRegisteredCourse(registeredCourse);
					if (!unlockFailureReasons.Any() && success)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return (unlockFailureReasons, success);
				}
			}
		}

		public sealed class MarksSubmission
		{
			internal MarksSubmission() { }
			public short OfferedSemesterID { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public string DepartmentAlias { get; internal set; }
			public int ProgramID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public int Total { get; internal set; }
			public int MarksEntryCompleted { get; internal set; }
			public int SubmittedToHOD { get; internal set; }
			public int SubmittedToCampus { get; internal set; }
			public int SubmittedToUniversity { get; internal set; }
		}

		public static List<MarksSubmission> GetMarksSubmissionStatuses(short semesterID, int? departmentID, bool? markEntryCompleted, bool? submittedToHOD, bool? submittedToCampus, bool? submittedToUniversity, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
				var offeredCourses = aspireContext.OfferedCourses
					.Where(oc => oc.Cours.CourseCategory == (byte)CourseCategories.Course || oc.Cours.CourseCategory == (byte)CourseCategories.Lab)
					.Where(oc => oc.SemesterID == semesterID)
					.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				if (departmentID != null)
					offeredCourses = offeredCourses.Where(oc => departmentID == null || oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
				if (markEntryCompleted != null)
					offeredCourses = offeredCourses.Where(oc => oc.MarksEntryCompleted == markEntryCompleted.Value);
				if (submittedToHOD != null)
					offeredCourses = submittedToHOD.Value
						? offeredCourses.Where(oc => oc.SubmittedToHODDate != null)
						: offeredCourses.Where(oc => oc.SubmittedToHODDate == null);
				if (submittedToCampus != null)
					offeredCourses = submittedToCampus.Value
						? offeredCourses.Where(oc => oc.SubmittedToCampusDate != null)
						: offeredCourses.Where(oc => oc.SubmittedToCampusDate == null);
				if (submittedToUniversity != null)
					offeredCourses = submittedToUniversity.Value
						? offeredCourses.Where(oc => oc.SubmittedToUniversityDate != null)
						: offeredCourses.Where(oc => oc.SubmittedToUniversityDate == null);

				var result = offeredCourses
					.GroupBy(oc => new
					{
						OfferedSemesterID = oc.SemesterID,
						oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
						oc.Cours.AdmissionOpenProgram.Program.Department.DepartmentAlias,
						oc.Cours.AdmissionOpenProgram.ProgramID,
						oc.Cours.AdmissionOpenProgram.Program.ProgramAlias
					})
					.Select(g => new MarksSubmission
					{
						OfferedSemesterID = g.Key.OfferedSemesterID,
						DepartmentID = g.Key.DepartmentID,
						DepartmentAlias = g.Key.DepartmentAlias,
						ProgramID = g.Key.ProgramID,
						ProgramAlias = g.Key.ProgramAlias,
						Total = g.Count(),
						MarksEntryCompleted = g.Count(gg => gg.MarksEntryCompleted),
						SubmittedToHOD = g.Count(gg => gg.SubmittedToHODDate != null),
						SubmittedToCampus = g.Count(gg => gg.SubmittedToCampusDate != null),
						SubmittedToUniversity = g.Count(gg => gg.SubmittedToUniversityDate != null)
					});

				switch (sortExpression)
				{
					case nameof(MarksSubmission.OfferedSemesterID):
						return result.OrderBy(sortDirection, r => r.OfferedSemesterID).ToList();
					case nameof(MarksSubmission.DepartmentID):
						return result.OrderBy(sortDirection, r => r.DepartmentID).ToList();
					case nameof(MarksSubmission.DepartmentAlias):
						return result.OrderBy(sortDirection, r => r.DepartmentAlias).ToList();
					case nameof(MarksSubmission.ProgramID):
						return result.OrderBy(sortDirection, r => r.ProgramID).ToList();
					case nameof(MarksSubmission.ProgramAlias):
						return result.OrderBy(sortDirection, r => r.ProgramAlias).ToList();
					case nameof(MarksSubmission.Total):
						return result.OrderBy(sortDirection, r => r.Total).ToList();
					case nameof(MarksSubmission.MarksEntryCompleted):
						return result.OrderBy(sortDirection, r => r.MarksEntryCompleted).ToList();
					case nameof(MarksSubmission.SubmittedToHOD):
						return result.OrderBy(sortDirection, r => r.SubmittedToHOD).ToList();
					case nameof(MarksSubmission.SubmittedToCampus):
						return result.OrderBy(sortDirection, r => r.SubmittedToCampus).ToList();
					case nameof(MarksSubmission.SubmittedToUniversity):
						return result.OrderBy(sortDirection, r => r.SubmittedToUniversity).ToList();
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
			}
		}

		public sealed class OfferedCourse
		{
			internal OfferedCourse() { }
			public int OfferedCourseID { get; internal set; }
			internal string ProgramAlias { get; set; }
			internal short SemesterNo { get; set; }
			internal int Section { get; set; }
			internal byte Shift { get; set; }
			public string ClassName => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public string Title { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public bool MarksEntryCompleted { get; internal set; }
			public bool SubmittedToHOD { get; internal set; }
			public bool SubmittedToCampus { get; internal set; }
			public bool SubmittedToUniversity { get; internal set; }
		}

		public static List<OfferedCourse> GetOfferedCourses(short semesterID, int? departmentID, bool? markEntryCompleted, bool? submittedToHOD, bool? submittedToCampus, bool? submittedToUniversity, List<int> programIDs, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
				var offeredCourses = aspireContext.OfferedCourses
					.Where(oc => oc.Cours.CourseCategory == (byte)CourseCategories.Course || oc.Cours.CourseCategory == (byte)CourseCategories.Lab)
					.Where(oc => oc.SemesterID == semesterID)
					.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				if (departmentID != null)
					offeredCourses = offeredCourses.Where(oc => departmentID == null || oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
				if (markEntryCompleted != null)
					offeredCourses = offeredCourses.Where(oc => oc.MarksEntryCompleted == markEntryCompleted.Value);
				if (submittedToHOD != null)
					offeredCourses = submittedToHOD.Value
						? offeredCourses.Where(oc => oc.SubmittedToHODDate != null)
						: offeredCourses.Where(oc => oc.SubmittedToHODDate == null);
				if (submittedToCampus != null)
					offeredCourses = submittedToCampus.Value
						? offeredCourses.Where(oc => oc.SubmittedToCampusDate != null)
						: offeredCourses.Where(oc => oc.SubmittedToCampusDate == null);
				if (submittedToUniversity != null)
					offeredCourses = submittedToUniversity.Value
						? offeredCourses.Where(oc => oc.SubmittedToUniversityDate != null)
						: offeredCourses.Where(oc => oc.SubmittedToUniversityDate == null);
				if (programIDs != null)
					offeredCourses = offeredCourses.Where(oc => programIDs.Contains(oc.Cours.AdmissionOpenProgram.ProgramID));
				return offeredCourses.Select(oc => new OfferedCourse
				{
					OfferedCourseID = oc.OfferedCourseID,
					ProgramAlias = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = oc.SemesterNo,
					Section = oc.Section,
					Shift = oc.Shift,
					Title = oc.Cours.Title,
					FacultyMemberName = oc.FacultyMember.Name,
					MarksEntryCompleted = oc.MarksEntryCompleted,
					SubmittedToHOD = oc.SubmittedToHODDate != null,
					SubmittedToCampus = oc.SubmittedToCampusDate != null,
					SubmittedToUniversity = oc.SubmittedToUniversityDate != null
				}).ToList().OrderBy(oc => oc.ClassName).ThenBy(oc => oc.Title).ToList();
			}
		}

		public class AwardListRow
		{
			public int OfferedCourseID { get; set; }
			public string OfferedTitle { get; set; }
			public string FacultyMemberName { get; set; }
			public string OfferedCreditHours { get; set; }
			public string OfferedContactHours { get; set; }
			public string OfferedCourseCategory { get; set; }
			public string OfferedClass { get; set; }
			public string OfferedSemester { get; set; }
			public bool SchemeAssignmentCompileAssignmentsQuizCompileQuizzesMidFinal { get; set; }
			public bool SchemeAssignmentQuizCompileInternalsMidFinal { get; set; }
			public bool SchemeAssignmentsQuizzesMidFinal { get; set; }
			public bool SchemeFinal { get; set; }
			public string AssignmentsMarksSubmittedString { get; set; }
			public string QuizzesMarksSubmittedString { get; set; }
			public string InternalsMarksSubmittedString { get; set; }
			public string MidMarksSubmittedString { get; set; }
			public string FinalMarksSubmittedString { get; set; }
			public string MarksEntryCompletedString { get; set; }
			public string MarksEntryLockedString { get; set; }
			public string SubmittedToHODString { get; set; }
			public string SubmittedToCampusString { get; set; }
			public string SubmittedToUniversityString { get; set; }
			public string RegisteredCourseCourseCode { get; set; }
			public string RegisteredCourseTitle { get; set; }
			public int RegisteredCourseAdmissionOpenProgramID { get; set; }
			public string RegisteredCourseProgram { get; set; }
			public string RegisteredCourseCreditHours { get; set; }
			public string RegisteredCourseCategory { get; set; }
			public int RegisteredCourseID { get; set; }
			public int StudentID { get; set; }
			public string Enrollment { get; set; }
			public string Name { get; set; }
			public int? RegistrationNo { get; set; }
			public string Program { get; set; }
			public string StudentStatus { get; set; }
			public string AssignmentsString { get; set; }
			public byte? AssignmentsTotal { get; set; }
			public string QuizzesString { get; set; }
			public byte? QuizzesTotal { get; set; }
			public string InternalsString { get; set; }
			public byte? InternalsTotal { get; set; }
			public string MidString { get; set; }
			public byte? MidTotal { get; set; }
			public string FinalString { get; set; }
			public byte? FinalTotal { get; set; }
			public string TotalString { get; set; }
			public byte TotalTotal { get; set; }
			public string GradeString { get; set; }
			public string StatusString { get; set; }
			public string Special { get; set; }
			public string Visiting { get; set; }
			public string Institute { get; set; }
			public string ReportTitle { get; set; }
			public string OfferedCourseCode { get; set; }
			public string RegisteredCourseRoadmapSemester { get; set; }
			public string RegisteredCourseGradingSchemeHtml { get; set; }
			public decimal? Product { get; set; }
			public short RegisteredCourseRoadmapSemesterID { get; set; }

			public List<AwardListRow> GetAwardListRows()
			{
				return null;
			}
		}

		public static (IEnumerable<string> errors, List<AwardListRow> awardListRows)? GetAwardList(int offeredCourseID, Guid loginSessionGuid)
		{
			lock (Locks.GetItemLock(Locks.Modules.ExamMarksSheetOfferedCourseID, offeredCourseID))
			{
				using (var aspireContext = new AspireContext(false))
				{
					var result = aspireContext.GetValidatedMarksSheet(offeredCourseID, loginSessionGuid);
					if (result == null)
						return null;
					if (result.Value.errorMessages.Any())
						return (result.Value.errorMessages, null);

					var marksSheet = result.Value.marksSheet;
					var awardListRows = marksSheet.OfferedCourse.RegisteredCourses.Select(rc => new AwardListRow
					{
						OfferedCourseID = rc.OfferedCourse.OfferedCourseID,
						Institute = rc.OfferedCourse.InstituteName,
						ReportTitle = $"Award List ({rc.OfferedCourse.OfferedSemesterID.ToSemesterString()})",
						OfferedTitle = rc.OfferedCourse.Title,
						OfferedCourseCode = rc.OfferedCourse.CourseCode,
						FacultyMemberName = rc.OfferedCourse.FacultyMemberName,
						OfferedCreditHours = rc.OfferedCourse.CreditHours.ToCreditHoursFullName(),
						OfferedContactHours = rc.OfferedCourse.ContactHours.ToCreditHoursFullName(),
						OfferedCourseCategory = rc.OfferedCourse.CourseCategoryEnum.ToFullName(),
						OfferedClass = AspireFormats.GetClassName(rc.OfferedCourse.ProgramAlias, rc.OfferedCourse.SemesterNoEnum, rc.OfferedCourse.SectionEnum, rc.OfferedCourse.ShiftEnum),
						OfferedSemester = rc.OfferedCourse.OfferedSemesterID.ToSemesterString(),
						Visiting = rc.OfferedCourse.Visiting.ToYesNo(),
						Special = rc.OfferedCourse.SpecialOffered.ToYesNo(),
						SchemeAssignmentCompileAssignmentsQuizCompileQuizzesMidFinal = rc.OfferedCourse.MarksSheet.MarksEntryTypeEnum == Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal,
						SchemeAssignmentQuizCompileInternalsMidFinal = rc.OfferedCourse.MarksSheet.MarksEntryTypeEnum == Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal,
						SchemeAssignmentsQuizzesMidFinal = rc.OfferedCourse.MarksSheet.MarksEntryTypeEnum == Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal,
						SchemeFinal = rc.OfferedCourse.MarksSheet.MarksEntryTypeEnum == Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final,
						AssignmentsMarksSubmittedString = rc.OfferedCourse.AssignmentsMarksSubmittedString,
						QuizzesMarksSubmittedString = rc.OfferedCourse.QuizzesMarksSubmittedString,
						InternalsMarksSubmittedString = rc.OfferedCourse.InternalsMarksSubmittedString,
						MidMarksSubmittedString = rc.OfferedCourse.MidMarksSubmittedString,
						FinalMarksSubmittedString = rc.OfferedCourse.FinalMarksSubmittedString,
						MarksEntryCompletedString = rc.OfferedCourse.MarksEntryCompletedString,
						MarksEntryLockedString = rc.OfferedCourse.MarksEntryLockedString,
						SubmittedToHODString = rc.OfferedCourse.SubmittedToHODString,
						SubmittedToCampusString = rc.OfferedCourse.SubmittedToCampusString,
						SubmittedToUniversityString = rc.OfferedCourse.SubmittedToUniversityString,
						RegisteredCourseCourseCode = rc.CourseCode,
						RegisteredCourseTitle = rc.Title,
						RegisteredCourseAdmissionOpenProgramID = rc.StudentAdmissionOpenProgramID,
						RegisteredCourseRoadmapSemesterID = rc.RoadmapSemesterID,
						RegisteredCourseProgram = rc.ProgramAlias,
						RegisteredCourseCreditHours = rc.CreditHours.ToCreditHoursFullName(),
						RegisteredCourseCategory = rc.CourseCategoryEnum.ToFullName(),
						RegisteredCourseRoadmapSemester = rc.RoadmapSemesterID.ToSemesterString(),
						RegisteredCourseID = rc.RegisteredCourseID,
						StudentID = rc.StudentID,
						Enrollment = rc.Enrollment,
						Name = rc.Name,
						RegistrationNo = rc.RegistrationNo,
						Program = rc.ProgramAlias,
						StudentStatus = rc.StudentStatusEnum?.ToFullName(),
						AssignmentsString = rc.Assignments.ToExamMarksString(),
						AssignmentsTotal = rc.AssignmentsTotal,
						QuizzesString = rc.Quizzes.ToExamMarksString(),
						QuizzesTotal = rc.QuizzesTotal,
						InternalsString = rc.Internals.ToExamMarksString(),
						InternalsTotal = rc.InternalsTotal,
						MidString = rc.Mid.ToExamMarksString(),
						MidTotal = rc.MidTotal,
						FinalString = rc.Final.ToExamMarksString(),
						FinalTotal = rc.FinalTotal,
						TotalString = rc.Total.ToString(),
						TotalTotal = rc.TotalTotal,
						GradeString = rc.GradeString,
						Product = rc.Product,
						StatusString = rc.StatusString,
						RegisteredCourseGradingSchemeHtml = rc.ExamMarksPolicyToBeApplied.GetGradingSchemeHtml()
					}).ToList();

					return (null, awardListRows);
				}
			}
		}
	}
}