﻿namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IOptions
	{
		bool AddAssignment { get; }
		bool CanAddAssignment { get; }
		string CanNotAddAssignmentReasons { get; }
		bool AddQuiz { get; }
		bool CanAddQuiz { get; }
		string CanNotAddQuizReasons { get; }
		bool Assignments { get; }
		bool CanEditAssignments { get; }
		string CanNotEditAssignmentsReasons { get; }
		bool Quizzes { get; }
		bool CanEditQuizzes { get; }
		string CanNotEditQuizzesReasons { get; }
		bool Mid { get; }
		bool CanEditMid { get; }
		string CanNotEditMidReasons { get; }
		bool CompileAssignments { get; }
		bool CanCompileAssignments { get; }
		string CanNotCompileAssignmentsReasons { get; }
		bool CompileQuizzes { get; }
		bool CanCompileQuizzes { get; }
		string CanNotCompileQuizzesReasons { get; }
		bool CompileInternals { get; }
		bool CanCompileInternals { get; }
		string CanNotCompileInternalsReasons { get; }
		bool Final { get; }
		bool CanEditFinal { get; }
		string CanNotEditFinalReasons { get; }
		bool CompileTotalAndGrades { get; }
		bool CanCompileTotalAndGrades { get; }
		string CanNotCompileTotalAndGradesReasons { get; }
		bool LockMarksEntry { get; }
		bool CanLockMarksEntry { get; }
		string CanNotLockMarksEntryReasons { get; }
		bool UnlockMarksEntry { get; }
		bool CanUnlockMarksEntry { get; }
		string CanNotUnlockMarksEntryReasons { get; }
		bool SubmitToHOD { get; }
		bool CanSubmitToHOD { get; }
		string CanNotSubmitToHODReasons { get; }
		bool SubmitToCampus { get; }
		bool CanSubmitToCampus { get; }
		string CanNotSubmitToCampusReasons { get; }
		bool SubmitToUniversity { get; }
		bool CanSubmitToUniversity { get; }
		string CanNotSubmitToUniversityReasons { get; }
		bool RevertSubmitToHOD { get; }
		bool CanRevertSubmitToHOD { get; }
		string CanNotRevertSubmitToHODReasons { get; }
		bool RevertSubmitToCampus { get; }
		bool CanRevertSubmitToCampus { get; }
		string CanNotRevertSubmitToCampusReasons { get; }
		bool RevertSubmitToUniversity { get; }
		bool CanRevertSubmitToUniversity { get; }
		string CanNotRevertSubmitToUniversityReasons { get; }
	}
}