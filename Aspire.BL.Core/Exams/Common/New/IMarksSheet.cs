﻿using System;

namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IMarksSheet : IValidatable, IAssignment, IAssignments, IQuiz, IQuizzes, IInternals, IMid, IFinal, IMarksSubmissionFlow
	{
		bool IsFacultyMember { get; }
		bool IsStaff { get; }
		int UserLoginHistoryID { get; }
		Guid LoginSessionGuid { get; }
		Model.Entities.ExamMarksPolicy.MarksEntryTypes MarksEntryTypeEnum { get; }
		IOptions Options { get; }
		IDisplay Display { get; }
		IOfferedCourse OfferedCourse { get; }
		IMarksSubmissionFlow MarksSubmissionFlow { get; }
		string ToJson();
	}
}