﻿using System;
using System.Collections.Generic;

namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IQuiz
	{
		IEnumerable<string> CanNotAddQuizReasons { get; }
		bool CanAddQuiz { get; }
		IEnumerable<string> CanNotEditOrDeleteQuizReasons(int offeredCourseExamID);
		bool CanEditOrDeleteQuiz(int offeredCourseExamID);
		IEnumerable<string> CanNotUnlockQuizReasons(int offeredCourseExamID);
		bool CanUnlockQuiz(int offeredCourseExamID);
		IReadOnlyList<IStudentMarks> GetQuizMarks(int offeredCourseExamID);
		(IEnumerable<string> addFailureReasons, int? offeredCourseExamID) AddQuiz(DateTime examDate, byte examNo, decimal totalMarks, string remarks);
		(IEnumerable<string> updateFailureReasons, bool success) UpdateQuiz(int offeredCourseExamID, DateTime examDate, byte examNo, string remarks, IReadOnlyDictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)> marks, bool submit);
		(IEnumerable<string> deleteFailureReasons, bool success) DeleteQuiz(int offeredCourseExamID);
		(IEnumerable<string> unlockFailureReasons, bool success) UnlockQuiz(int offeredCourseExamID);
	}
}
