﻿using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;

namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IOfferedCourse
	{
		IMarksSheet MarksSheet { get; }
		int OfferedCourseID { get; }
		int InstituteID { get; }
		string InstituteName { get; }
		int? DepartmentID { get; }
		int ProgramID { get; }
		int AdmissionOpenProgramID { get; }
		short RoadmapSemesterID { get; }
		short OfferedSemesterID { get; }
		int CourseID { get; }
		string CourseCode { get; }
		string Title { get; }
		decimal CreditHours { get; }
		decimal ContactHours { get; }
		string ProgramAlias { get; }
		SemesterNos SemesterNoEnum { get; }
		Sections SectionEnum { get; }
		Shifts ShiftEnum { get; }
		string Class { get; }
		int? FacultyMemberID { get; }
		string FacultyMemberName { get; }
		CourseCategories CourseCategoryEnum { get; }
		CourseTypes CourseTypeEnum { get; }
		bool Visiting { get; }
		bool SpecialOffered { get; }
		OfferedCours.ExamCompilationTypes? AssignmentsExamCompilationTypeEnum { get; }
		bool AssignmentsMarksSubmitted { get; }
		DateTime? AssignmentsMarksSubmittedDate { get; }
		string AssignmentsMarksSubmittedString { get; }
		OfferedCours.ExamCompilationTypes? QuizzesExamCompilationTypeEnum { get; }
		bool QuizzesMarksSubmitted { get; }
		DateTime? QuizzesMarksSubmittedDate { get; }
		string QuizzesMarksSubmittedString { get; }
		OfferedCours.ExamCompilationTypes? InternalsExamCompilationTypeEnum { get; }
		bool InternalsMarksSubmitted { get; }
		DateTime? InternalsMarksSubmittedDate { get; }
		string InternalsMarksSubmittedString { get; }
		bool MidMarksSubmitted { get; }
		DateTime? MidMarksSubmittedDate { get; }
		string MidMarksSubmittedString { get; }
		bool FinalMarksSubmitted { get; }
		DateTime? FinalMarksSubmittedDate { get; }
		string FinalMarksSubmittedString { get; }
		bool MarksEntryCompleted { get; }
		string MarksEntryCompletedString { get; }
		bool MarksEntryLocked { get; }
		DateTime? MarksEntryLockedDate { get; }
		string MarksEntryLockedString { get; }
		DateTime? SubmittedToHODDate { get; }
		string SubmittedToHODString { get; }
		DateTime? SubmittedToCampusDate { get; }
		string SubmittedToCampusString { get; }
		DateTime? SubmittedToUniversityDate { get; }
		string SubmittedToUniversityString { get; }
		IExamMarksPolicy ExamMarksPolicy { get; }
		IReadOnlyList<IRegisteredCourse> RegisteredCourses { get; }
		IReadOnlyList<IOfferedCourseExam> OfferedCourseExams { get; }
		IReadOnlyList<IOfferedCourseExam> OfferedCourseExamAssignments { get; }
		IReadOnlyList<IOfferedCourseExam> OfferedCourseExamQuizzes { get; }
		string OfferedCourseInfo { get; }
	}
}
