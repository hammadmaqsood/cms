﻿using System.Collections.Generic;

namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IMid
	{
		IEnumerable<string> CanNotEditMidReasons { get; }
		bool CanEditMid { get; }
		IEnumerable<string> CanNotUnlockMidReasons { get; }
		bool CanUnlockMid { get; }
		bool IsMidLocked { get; }
		IReadOnlyList<IStudentMarks> GetMidMarks();
		(IEnumerable<string> updateFailureReasons, bool success) UpdateMid(IReadOnlyDictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)> marks, bool submit);
		(IEnumerable<string> unlockFailureReasons, bool success) UnlockMid();
	}
}
