﻿using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;

namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IRegisteredCourse : IValidatable
	{
		IOfferedCourse OfferedCourse { get; }
		int RegisteredCourseID { get; }
		int StudentAdmissionOpenProgramID { get; }
		int RoadmapAdmissionOpenProgramID { get; }
		int StudentID { get; }
		string Enrollment { get; }
		int? RegistrationNo { get; }
		string Name { get; }
		Model.Entities.Student.Statuses? StudentStatusEnum { get; }
		string CourseCode { get; }
		string Title { get; }
		decimal CreditHours { get; }
		string ProgramAlias { get; }
		ProgramDurations ProgramDurationEnum { get; }
		bool IsSummerRegularSemester { get; }
		short RoadmapSemesterID { get; }
		CourseCategories CourseCategoryEnum { get; }
		bool SummerGradeCapping { get; }
		RegisteredCours.Statuses? StatusEnum { get; }
		string StatusString { get; }
		string StatusHtml { get; }
		FreezedStatuses? FreezeStatusEnum { get; }
		decimal? Assignments { get; }
		byte? AssignmentsTotal { get; }
		string AssignmentsString { get; }
		decimal? Quizzes { get; }
		byte? QuizzesTotal { get; }
		string QuizzesString { get; }
		decimal? Internals { get; }
		byte? InternalsTotal { get; }
		string InternalsString { get; }
		decimal? Mid { get; }
		byte? MidTotal { get; }
		string MidString { get; }
		decimal? Final { get; }
		byte? FinalTotal { get; }
		string FinalString { get; }
		byte? Total { get; }
		byte TotalTotal { get; }
		string TotalString { get; }
		ExamGrades? GradeEnum { get; }
		string GradeString { get; }
		decimal? GradePoints { get; }
		decimal? Product { get; }
		bool MarksEntryLocked { get; }
		DateTime? MarksEntryLockedDate { get; }
		int? ExamMarksPolicyID { get; }
		CourseCategories? ExamMarksPolicyCourseCategoryEnum { get; }
		string ExamMarksPolicyChecksumMD5 { get; }
		IReadOnlyList<IExamMarks> OfferedCourseExams { get; }
		IReadOnlyList<IExamMarks> OfferedCourseExamAssignments { get; }
		IReadOnlyList<IExamMarks> OfferedCourseExamQuizzes { get; }
		IExamMarksPolicy ExamMarksPolicyToBeApplied { get; }
		string RegisteredCourseInfo { get; }
		IEnumerable<string> MarksEntryNotAllowedReasons { get; }
		bool MarksEntryAllowed { get; }
		IEnumerable<string> CompilationNotAllowedReasons { get; }
		bool CompilationAllowed { get; }
		IEnumerable<string> CanNotUnlockMarksEntryReasons { get; }
		bool CanUnlockMarksEntry { get; }
		decimal? GetCompiledAssignmentsMarks(OfferedCours.ExamCompilationTypes examCompilationType);
		decimal? GetCompiledQuizzesMarks(OfferedCours.ExamCompilationTypes examCompilationType);
		decimal? GetCompiledInternalsMarks(OfferedCours.ExamCompilationTypes examCompilationType);
		void SetTotalsAndGrades(RegisteredCours registeredCourse);
	}
}