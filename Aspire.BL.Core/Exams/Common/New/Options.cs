﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.New
{
	internal sealed class Options : IOptions
	{
		public Options(IMarksSheet marksSheet)
		{
			this.MarksSheet = marksSheet;
		}

		private static string ConvertToHtml(IEnumerable<string> input)
		{
			return input.FirstOrDefault()?.HtmlEncode();
			//return input.Select(i => $"<li>{i.HtmlEncode()}</li>").Join("");
		}

		private IMarksSheet MarksSheet { get; }

		public bool AddAssignment
		{
			get
			{
				switch (this.MarksSheet.MarksEntryTypeEnum)
				{
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
						return true;
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
						return false;
					default:
						throw new NotImplementedEnumException(this.MarksSheet.MarksEntryTypeEnum);
				}
			}
		}

		public bool CanAddAssignment => this.MarksSheet.CanAddAssignment;

		public string CanNotAddAssignmentReasons => ConvertToHtml(this.MarksSheet.CanNotAddAssignmentReasons);

		public bool AddQuiz
		{
			get
			{
				switch (this.MarksSheet.MarksEntryTypeEnum)
				{
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
						return true;
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
						return false;
					default:
						throw new NotImplementedEnumException(this.MarksSheet.MarksEntryTypeEnum);
				}
			}
		}

		public bool CanAddQuiz => this.MarksSheet.CanAddQuiz;

		public string CanNotAddQuizReasons => ConvertToHtml(this.MarksSheet.CanNotAddQuizReasons);

		public bool Assignments
		{
			get
			{
				switch (this.MarksSheet.MarksEntryTypeEnum)
				{
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
						return false;
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
						return true;
					default:
						throw new NotImplementedEnumException(this.MarksSheet.MarksEntryTypeEnum);
				}
			}
		}

		public bool CanEditAssignments => this.MarksSheet.CanEditAssignments;

		public string CanNotEditAssignmentsReasons => ConvertToHtml(this.MarksSheet.CanNotEditAssignmentsReasons);

		public bool Quizzes
		{
			get
			{
				switch (this.MarksSheet.MarksEntryTypeEnum)
				{
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
						return false;
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
						return true;
					default:
						throw new NotImplementedEnumException(this.MarksSheet.MarksEntryTypeEnum);
				}
			}
		}

		public bool CanEditQuizzes => this.MarksSheet.CanEditQuizzes;

		public string CanNotEditQuizzesReasons => ConvertToHtml(this.MarksSheet.CanNotEditQuizzesReasons);

		public bool Mid
		{
			get
			{
				switch (this.MarksSheet.MarksEntryTypeEnum)
				{
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
						return true;
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
						return false;
					default:
						throw new NotImplementedEnumException(this.MarksSheet.MarksEntryTypeEnum);
				}
			}
		}

		public bool CanEditMid => this.MarksSheet.CanEditMid;

		public string CanNotEditMidReasons => ConvertToHtml(this.MarksSheet.CanNotEditMidReasons);

		public bool CompileAssignments
		{
			get
			{
				switch (this.MarksSheet.MarksEntryTypeEnum)
				{
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
						return true;
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
						return false;
					default:
						throw new NotImplementedEnumException(this.MarksSheet.MarksEntryTypeEnum);
				}
			}
		}

		public bool CanCompileAssignments => this.MarksSheet.CanCompileAssignments;

		public string CanNotCompileAssignmentsReasons => ConvertToHtml(this.MarksSheet.CanNotCompileAssignmentsReasons);

		public bool CompileQuizzes
		{
			get
			{
				switch (this.MarksSheet.MarksEntryTypeEnum)
				{
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
						return true;
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
						return false;
					default:
						throw new NotImplementedEnumException(this.MarksSheet.MarksEntryTypeEnum);
				}
			}
		}

		public bool CanCompileQuizzes => this.MarksSheet.CanCompileQuizzes;

		public string CanNotCompileQuizzesReasons => ConvertToHtml(this.MarksSheet.CanNotCompileQuizzesReasons);

		public bool CompileInternals
		{
			get
			{
				switch (this.MarksSheet.MarksEntryTypeEnum)
				{
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
						return true;
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
						return false;
					default:
						throw new NotImplementedEnumException(this.MarksSheet.MarksEntryTypeEnum);
				}
			}
		}

		public bool CanCompileInternals => this.MarksSheet.CanCompileInternals;

		public string CanNotCompileInternalsReasons => ConvertToHtml(this.MarksSheet.CanNotCompileInternalsReasons);

		public bool Final
		{
			get
			{
				switch (this.MarksSheet.MarksEntryTypeEnum)
				{
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
					case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
						return true;
					default:
						throw new NotImplementedEnumException(this.MarksSheet.MarksEntryTypeEnum);
				}
			}
		}

		public bool CanEditFinal => this.MarksSheet.CanEditFinal;

		public string CanNotEditFinalReasons => ConvertToHtml(this.MarksSheet.CanNotEditFinalReasons);

		public bool CompileTotalAndGrades => true;

		public bool CanCompileTotalAndGrades => this.MarksSheet.CanCompileGrades;

		public string CanNotCompileTotalAndGradesReasons => ConvertToHtml(this.MarksSheet.CanNotCompileGradesReasons);

		public bool LockMarksEntry => !this.MarksSheet.OfferedCourse.MarksEntryLocked && this.MarksSheet.IsStaff;

		public bool CanLockMarksEntry => this.MarksSheet.CanLockMarksEntry;

		public string CanNotLockMarksEntryReasons => ConvertToHtml(this.MarksSheet.CanNotLockMarksEntryReasons);

		public bool UnlockMarksEntry => this.MarksSheet.OfferedCourse.MarksEntryLocked && this.MarksSheet.IsStaff;

		public bool CanUnlockMarksEntry => this.MarksSheet.CanUnlockMarksEntry;

		public string CanNotUnlockMarksEntryReasons => ConvertToHtml(this.MarksSheet.CanNotUnlockMarksEntryReasons);

		public bool SubmitToHOD => this.MarksSheet.IsFacultyMember;

		public bool CanSubmitToHOD => this.MarksSheet.CanSubmitToHOD;

		public string CanNotSubmitToHODReasons => ConvertToHtml(this.MarksSheet.CanNotSubmitToHODReasons);

		public bool SubmitToCampus => this.MarksSheet.IsStaff;

		public bool CanSubmitToCampus => this.MarksSheet.CanSubmitToCampus;

		public string CanNotSubmitToCampusReasons => ConvertToHtml(this.MarksSheet.CanNotSubmitToCampusReasons);

		public bool SubmitToUniversity => this.MarksSheet.IsStaff;

		public bool CanSubmitToUniversity => this.MarksSheet.CanSubmitToUniversity;

		public string CanNotSubmitToUniversityReasons => ConvertToHtml(this.MarksSheet.CanNotSubmitToUniversityReasons);

		public bool RevertSubmitToHOD => this.MarksSheet.IsStaff;

		public bool CanRevertSubmitToHOD => this.MarksSheet.CanRevertSubmitToHOD;

		public string CanNotRevertSubmitToHODReasons => ConvertToHtml(this.MarksSheet.CanNotRevertSubmitToHODReasons);

		public bool RevertSubmitToCampus => this.MarksSheet.IsStaff;

		public bool CanRevertSubmitToCampus => this.MarksSheet.CanRevertSubmitToCampus;

		public string CanNotRevertSubmitToCampusReasons => ConvertToHtml(this.MarksSheet.CanNotRevertSubmitToCampusReasons);

		public bool RevertSubmitToUniversity => this.MarksSheet.IsStaff;

		public bool CanRevertSubmitToUniversity => this.MarksSheet.CanRevertSubmitToUniversity;

		public string CanNotRevertSubmitToUniversityReasons => ConvertToHtml(this.MarksSheet.CanNotRevertSubmitToUniversityReasons);
	}
}