﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.New
{
	[JsonObject(MemberSerialization.OptIn)]
	internal sealed class RegisteredCourseExamMark : IRegisteredCourseExamMark
	{
		internal RegisteredCourseExamMark(IRegisteredCourse registeredCourse, IOfferedCourseExam offeredCourseExam, decimal? examMarks)
		{
			this.OfferedCourseExam = offeredCourseExam;
			this.ExamMarks = examMarks;
		}

		private IOfferedCourseExam OfferedCourseExam { get; }

		[JsonProperty]
		public decimal? ExamMarks { get; }

		public (List<string> errors, List<string> warnings) Validate()
		{
			var warnings = new List<string>();
			if (this.ExamMarks > this.OfferedCourseExam.TotalMarks)
				warnings.Add($"Marks must be less than or equal to total marks. Marks: {this.OfferedCourseExam.ExamTypeEnum}-{this.OfferedCourseExam.ExamNo}.");
			return (new List<string>(), warnings.Distinct().ToList());
		}
	}
}
