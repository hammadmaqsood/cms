﻿using Aspire.Model.Entities.Common;

namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IExamMarksPolicy
	{
		int AdmissionOpenProgramID { get; }
		int ExamMarksPolicyID { get; }
		ExamGrades? SummerCappingExamGradeEnum { get; }
		Model.Entities.ExamMarksPolicy.MarksEntryTypes CourseMarksEntryTypeEnum { get; }
		Model.Entities.ExamMarksPolicy.MarksEntryTypes LabMarksEntryTypeEnum { get; }
		string ChecksumMD5 { get; }
		byte? GetMaxAssignments(IRegisteredCourse registeredCourse);
		byte? GetMaxQuizzes(IRegisteredCourse registeredCourse);
		byte? GetMaxInternals(IRegisteredCourse registeredCourse);
		byte? GetMaxMid(IRegisteredCourse registeredCourse);
		byte GetMaxFinal(IRegisteredCourse registeredCourse);
		(byte? Total, ExamGrades ExamGradeEnum, decimal GradePoints, decimal Product)? GetExamGradeAndGradePoints(decimal? final, decimal total, IRegisteredCourse registeredCourse);
		string GetGradingSchemeHtml();
	}
}
