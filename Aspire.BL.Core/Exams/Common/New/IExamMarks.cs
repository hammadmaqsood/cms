﻿namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IExamMarks
	{
		IOfferedCourseExam OfferedCourseExam { get; }
		IRegisteredCourseExamMark RegisteredCourseExamMark { get; }
	}
}
