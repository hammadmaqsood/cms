﻿using Aspire.Model.Entities;
using System.Collections.Generic;

namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IAssignments
	{
		IEnumerable<string> CanNotEditAssignmentsReasons { get; }
		bool CanEditAssignments { get; }
		IEnumerable<string> CanNotUnlockAssignmentsReasons { get; }
		bool CanUnlockAssignments { get; }
		IEnumerable<string> CanNotCompileAssignmentsReasons { get; }
		bool CanCompileAssignments { get; }
		bool IsAssignmentsLocked { get; }
		IReadOnlyList<IStudentMarks> GetAssignmentsMarks();
		decimal? GetCompiledAssignmentsMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType);
		(IEnumerable<string> updateFailureReasons, bool success) UpdateAssignments(IReadOnlyDictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)> marks, bool submit);
		(IEnumerable<string> compileFailureReasons, bool success) CompileAssignments(OfferedCours.ExamCompilationTypes examCompilationTypeEnum, bool submit);
		(IEnumerable<string> unlockFailureReasons, bool success) UnlockAssignments();
	}
}
