﻿using System.Collections.Generic;

namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IValidatable
	{
		(List<string> errors, List<string> warnings) Validate();
	}
}
