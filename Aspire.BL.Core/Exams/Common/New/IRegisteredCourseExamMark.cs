﻿namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IRegisteredCourseExamMark : IValidatable
	{
		decimal? ExamMarks { get; }
	}
}
