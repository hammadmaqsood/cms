﻿using System;
using System.Collections.Generic;

namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IOfferedCourseExam : IValidatable
	{
		IOfferedCourse OfferedCourse { get; }
		int OfferedCourseExamID { get; }
		Model.Entities.OfferedCourseExam.ExamTypes ExamTypeEnum { get; }
		DateTime ExamDate { get; }
		byte ExamNo { get; }
		decimal TotalMarks { get; }
		string Remarks { get; }
		bool Locked { get; }
		DateTime? LockedDate { get; }
		IEnumerable<string> CanNotEditAssignmentReasons { get; }
		bool CanEditAssignment { get; }
		IEnumerable<string> CanNotEditQuizReasons { get; }
		bool CanEditQuiz { get; }
	}
}