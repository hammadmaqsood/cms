﻿namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IDisplay
	{
		bool MultiRowHeader { get; }
		bool AssignmentVisible { get; }
		bool EmptyAssignmentVisible { get; }
		bool AssignmentsVisible { get; }
		bool CompileAssignmentsVisible { get; }
		int AssignmentsHeaderColSpan { get; }
		bool QuizVisible { get; }
		bool EmptyQuizVisible { get; }
		bool QuizzesVisible { get; }
		bool CompileQuizzesVisible { get; }
		int QuizzesHeaderColSpan { get; }
		bool InternalsVisible { get; }
		bool MidVisible { get; }
		string GetMarksString(decimal? obtainedMarks, decimal? totalMarks);
	}
}
