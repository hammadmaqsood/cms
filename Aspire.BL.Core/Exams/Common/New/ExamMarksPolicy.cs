﻿using Aspire.BL.Core.Exams.Common.New.Converters;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.New
{
	[JsonObject(MemberSerialization.OptIn)]
	internal sealed class ExamMarksPolicy : IExamMarksPolicy
	{
		[JsonObject(MemberSerialization.OptIn)]
		private sealed class Grade
		{
			[JsonProperty("Maximum")]
			private int Max { get; set; }
			[JsonProperty("Minimum")]
			private int Min { get; set; }
			[JsonProperty("Grade Points")]
			private decimal GradePoints { get; set; }

			public static Grade Create(int? min, int? max, decimal? gradePoints)
			{
				if (min == null || max == null || gradePoints == null)
					return null;
				return new Grade { Min = min.Value, Max = max.Value, GradePoints = gradePoints.Value };
			}
		}

		[JsonObject(MemberSerialization.OptIn)]
		private sealed class Marks
		{
			[JsonProperty("Marks Entry Type")]
			[JsonConverter(typeof(EnumFullNameConverter))]
			public Model.Entities.ExamMarksPolicy.MarksEntryTypes MarksEntryTypeEnum { get; set; }
			[JsonProperty("Assignments")]
			public byte? Assignments { get; set; }
			[JsonProperty("Quizzes")]
			public byte? Quizzes { get; set; }
			[JsonProperty("Internals")]
			public byte? Internal { get; set; }
			[JsonProperty("Mid")]
			public byte? Mid { get; set; }
			[JsonProperty("Final")]
			public byte Final { get; set; }
		}

		internal static List<IExamMarksPolicy> GetAllExamMarksPolicies(IOfferedCourse offeredCourse, AspireContext aspireContext)
		{
			var query = from rc in aspireContext.RegisteredCourses
						join c in aspireContext.Courses on rc.CourseID equals c.CourseID
						join aopemp in aspireContext.AdmissionOpenProgramExamMarksPolicies on c.AdmissionOpenProgramID equals aopemp.AdmissionOpenProgramID
						join emp in aspireContext.ExamMarksPolicies on aopemp.ExamMarksPolicyID equals emp.ExamMarksPolicyID
						where rc.OfferedCourseID == offeredCourse.OfferedCourseID && rc.DeletedDate == null && aopemp.SemesterID == offeredCourse.OfferedSemesterID
						select new ExamMarksPolicy
						{
							AdmissionOpenProgramID = aopemp.AdmissionOpenProgramID,
							ExamMarksPolicyID = emp.ExamMarksPolicyID,
							AMin = emp.AMin,
							AMax = emp.AMax,
							AMinusMin = emp.AMinusMin,
							AMinusMax = emp.AMinusMax,
							BPlusMin = emp.BPlusMin,
							BPlusMax = emp.BPlusMax,
							BMin = emp.BMin,
							BMax = emp.BMax,
							BMinusMin = emp.BMinusMin,
							BMinusMax = emp.BMinusMax,
							CPlusMin = emp.CPlusMin,
							CPlusMax = emp.CPlusMax,
							CMin = emp.CMin,
							CMax = emp.CMax,
							CMinusMin = emp.CMinusMin,
							CMinusMax = emp.CMinusMax,
							DPlusMin = emp.DPlusMin,
							DPlusMax = emp.DPlusMax,
							DMin = emp.DMin,
							DMax = emp.DMax,
							FMax = emp.FMax,
							AGradePoint = emp.AGradePoint,
							AMinusGradePoint = emp.AMinusGradePoint,
							BPlusGradePoint = emp.BPlusGradePoint,
							BGradePoint = emp.BGradePoint,
							BMinusGradePoint = emp.BMinusGradePoint,
							CPlusGradePoint = emp.CPlusGradePoint,
							CGradePoint = emp.CGradePoint,
							CMinusGradePoint = emp.CMinusGradePoint,
							DPlusGradePoint = emp.DPlusGradePoint,
							DGradePoint = emp.DGradePoint,
							FGradePoint = emp.FGradePoint,
							SummerCappingExamGradeEnum = (ExamGrades)emp.SummerCappingExamGrade,
							CourseMarksEntryTypeEnum = (Model.Entities.ExamMarksPolicy.MarksEntryTypes)emp.CourseMarksEntryType,
							CourseAssignments = emp.CourseAssignments,
							CourseQuizzes = emp.CourseQuizzes,
							CourseInternals = emp.CourseInternals,
							CourseMid = emp.CourseMid,
							CourseFinal = emp.CourseFinal,
							LabMarksEntryTypeEnum = (Model.Entities.ExamMarksPolicy.MarksEntryTypes)emp.LabMarksEntryType,
							LabAssignments = emp.LabAssignments,
							LabQuizzes = emp.LabQuizzes,
							LabInternals = emp.LabInternals,
							LabMid = emp.LabMid,
							LabFinal = emp.LabFinal,
							ChecksumMD5 = emp.ChecksumMD5
						};
			return query.Distinct().ToList().Cast<IExamMarksPolicy>().ToList();
		}

		internal static IExamMarksPolicy GetExamMarksPolicy(AspireContext aspireContext, int admissionOpenProgramID, short semesterID)
		{
			return aspireContext.AdmissionOpenProgramExamMarksPolicies
				.Where(aopemp => aopemp.AdmissionOpenProgramID == admissionOpenProgramID && aopemp.SemesterID == semesterID)
				.Select(aopemp => aopemp.ExamMarksPolicy)
				.Select(emp => new ExamMarksPolicy
				{
					AdmissionOpenProgramID = admissionOpenProgramID,
					ExamMarksPolicyID = emp.ExamMarksPolicyID,
					AMin = emp.AMin,
					AMax = emp.AMax,
					AMinusMin = emp.AMinusMin,
					AMinusMax = emp.AMinusMax,
					BPlusMin = emp.BPlusMin,
					BPlusMax = emp.BPlusMax,
					BMin = emp.BMin,
					BMax = emp.BMax,
					BMinusMin = emp.BMinusMin,
					BMinusMax = emp.BMinusMax,
					CPlusMin = emp.CPlusMin,
					CPlusMax = emp.CPlusMax,
					CMin = emp.CMin,
					CMax = emp.CMax,
					CMinusMin = emp.CMinusMin,
					CMinusMax = emp.CMinusMax,
					DPlusMin = emp.DPlusMin,
					DPlusMax = emp.DPlusMax,
					DMin = emp.DMin,
					DMax = emp.DMax,
					FMax = emp.FMax,
					AGradePoint = emp.AGradePoint,
					AMinusGradePoint = emp.AMinusGradePoint,
					BPlusGradePoint = emp.BPlusGradePoint,
					BGradePoint = emp.BGradePoint,
					BMinusGradePoint = emp.BMinusGradePoint,
					CPlusGradePoint = emp.CPlusGradePoint,
					CGradePoint = emp.CGradePoint,
					CMinusGradePoint = emp.CMinusGradePoint,
					DPlusGradePoint = emp.DPlusGradePoint,
					DGradePoint = emp.DGradePoint,
					FGradePoint = emp.FGradePoint,
					SummerCappingExamGradeEnum = (ExamGrades?)emp.SummerCappingExamGrade,
					CourseMarksEntryTypeEnum = (Model.Entities.ExamMarksPolicy.MarksEntryTypes)emp.CourseMarksEntryType,
					CourseAssignments = emp.CourseAssignments,
					CourseQuizzes = emp.CourseQuizzes,
					CourseInternals = emp.CourseInternals,
					CourseMid = emp.CourseMid,
					CourseFinal = emp.CourseFinal,
					LabMarksEntryTypeEnum = (Model.Entities.ExamMarksPolicy.MarksEntryTypes)emp.LabMarksEntryType,
					LabAssignments = emp.LabAssignments,
					LabQuizzes = emp.LabQuizzes,
					LabInternals = emp.LabInternals,
					LabMid = emp.LabMid,
					LabFinal = emp.LabFinal,
					ChecksumMD5 = emp.ChecksumMD5
				}).SingleOrDefault();
		}

		private ExamMarksPolicy() { }

		public int AdmissionOpenProgramID { get; private set; }

		[JsonProperty]
		public int ExamMarksPolicyID { get; private set; }

		private byte? AMin { get; set; }

		private byte? AMax { get; set; }

		private decimal? AGradePoint { get; set; }

		private byte? AMinusMin { get; set; }

		private byte? AMinusMax { get; set; }

		private decimal? AMinusGradePoint { get; set; }

		private byte? BPlusMin { get; set; }

		private byte? BPlusMax { get; set; }

		private decimal? BPlusGradePoint { get; set; }

		private byte? BMin { get; set; }

		private byte? BMax { get; set; }

		private decimal? BGradePoint { get; set; }

		private byte? BMinusMin { get; set; }

		private byte? BMinusMax { get; set; }

		private decimal? BMinusGradePoint { get; set; }

		private byte? CPlusMin { get; set; }

		private byte? CPlusMax { get; set; }

		private decimal? CPlusGradePoint { get; set; }

		private byte? CMin { get; set; }

		private byte? CMax { get; set; }

		private decimal? CGradePoint { get; set; }

		private byte? CMinusMin { get; set; }

		private byte? CMinusMax { get; set; }

		private decimal? CMinusGradePoint { get; set; }

		private byte? DPlusMin { get; set; }

		private byte? DPlusMax { get; set; }

		private decimal? DPlusGradePoint { get; set; }

		private byte? DMin { get; set; }

		private byte? DMax { get; set; }

		private decimal? DGradePoint { get; set; }

		private byte FMax { get; set; }

		private decimal FGradePoint { get; set; }

		[JsonProperty("A")]
		private Grade A => Grade.Create(this.AMin, this.AMax, this.AGradePoint);

		[JsonProperty("A-")]
		private Grade AMinus => Grade.Create(this.AMinusMin, this.AMinusMax, this.AMinusGradePoint);

		[JsonProperty("B+")]
		private Grade BPlus => Grade.Create(this.BPlusMin, this.BPlusMax, this.BPlusGradePoint);

		[JsonProperty("B")]
		private Grade B => Grade.Create(this.BMin, this.BMax, this.BGradePoint);

		[JsonProperty("B-")]
		private Grade BMinus => Grade.Create(this.BMinusMin, this.BMinusMax, this.BMinusGradePoint);

		[JsonProperty("C+")]
		private Grade CPlus => Grade.Create(this.CPlusMin, this.CPlusMax, this.CPlusGradePoint);

		[JsonProperty("C")]
		private Grade C => Grade.Create(this.CMin, this.CMax, this.CGradePoint);

		[JsonProperty("C-")]
		private Grade CMinus => Grade.Create(this.CMinusMin, this.CMinusMax, this.CMinusGradePoint);

		[JsonProperty("D+")]
		private Grade DPlus => Grade.Create(this.DPlusMin, this.DPlusMax, this.DPlusGradePoint);

		[JsonProperty("D")]
		private Grade D => Grade.Create(this.DMin, this.DMin, this.DGradePoint);

		[JsonProperty("F")]
		private Grade F => Grade.Create(0, this.FMax, this.FGradePoint);

		[JsonProperty("Summer Capped Grade")]
		[JsonConverter(typeof(EnumFullNameConverter))]
		public ExamGrades? SummerCappingExamGradeEnum { get; private set; }

		public Model.Entities.ExamMarksPolicy.MarksEntryTypes CourseMarksEntryTypeEnum { get; private set; }

		private byte? CourseAssignments { get; set; }

		private byte? CourseQuizzes { get; set; }

		private byte? CourseInternals { get; set; }

		private byte? CourseMid { get; set; }

		private byte CourseFinal { get; set; }

		[JsonProperty("Course")]
		private Marks Course => new Marks
		{
			MarksEntryTypeEnum = this.CourseMarksEntryTypeEnum,
			Assignments = this.CourseAssignments,
			Quizzes = this.CourseQuizzes,
			Internal = this.CourseInternals,
			Mid = this.CourseMid,
			Final = this.CourseFinal
		};

		public Model.Entities.ExamMarksPolicy.MarksEntryTypes LabMarksEntryTypeEnum { get; private set; }

		private byte? LabAssignments { get; set; }

		private byte? LabQuizzes { get; set; }

		private byte? LabInternals { get; set; }

		private byte? LabMid { get; set; }

		private byte LabFinal { get; set; }

		[JsonProperty("Lab")]
		private Marks Lab => new Marks
		{
			MarksEntryTypeEnum = this.LabMarksEntryTypeEnum,
			Assignments = this.LabAssignments,
			Quizzes = this.LabQuizzes,
			Internal = this.LabInternals,
			Mid = this.LabMid,
			Final = this.LabFinal
		};

		[JsonProperty("Checksum MD5")]
		public string ChecksumMD5 { get; private set; }

		public byte? GetMaxAssignments(IRegisteredCourse registeredCourse)
		{
			switch (registeredCourse.OfferedCourse.MarksSheet.MarksEntryTypeEnum)
			{
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
					switch (registeredCourse.CourseCategoryEnum)
					{
						case CourseCategories.Course:
							return this.CourseAssignments;
						case CourseCategories.Lab:
							return this.LabAssignments;
						case CourseCategories.Internship:
							throw new InvalidOperationException();
						case CourseCategories.Project:
							throw new InvalidOperationException();
						case CourseCategories.Thesis:
							throw new InvalidOperationException();
						default:
							throw new NotImplementedEnumException(registeredCourse.CourseCategoryEnum);
					}
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
					switch (registeredCourse.CourseCategoryEnum)
					{
						case CourseCategories.Course:
						case CourseCategories.Lab:
							return null;
						case CourseCategories.Internship:
							throw new InvalidOperationException();
						case CourseCategories.Project:
							throw new InvalidOperationException();
						case CourseCategories.Thesis:
							throw new InvalidOperationException();
						default:
							throw new NotImplementedEnumException(registeredCourse.CourseCategoryEnum);
					}
				default:
					throw new NotImplementedEnumException(registeredCourse.OfferedCourse.MarksSheet.MarksEntryTypeEnum);
			}
		}

		public byte? GetMaxQuizzes(IRegisteredCourse registeredCourse)
		{
			switch (registeredCourse.OfferedCourse.MarksSheet.MarksEntryTypeEnum)
			{
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
					switch (registeredCourse.CourseCategoryEnum)
					{
						case CourseCategories.Course:
							return this.CourseQuizzes;
						case CourseCategories.Lab:
							return this.LabQuizzes;
						case CourseCategories.Internship:
							throw new InvalidOperationException();
						case CourseCategories.Project:
							throw new InvalidOperationException();
						case CourseCategories.Thesis:
							throw new InvalidOperationException();
						default:
							throw new NotImplementedEnumException(registeredCourse.CourseCategoryEnum);
					}
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
					switch (registeredCourse.CourseCategoryEnum)
					{
						case CourseCategories.Course:
						case CourseCategories.Lab:
							return null;
						case CourseCategories.Internship:
							throw new InvalidOperationException();
						case CourseCategories.Project:
							throw new InvalidOperationException();
						case CourseCategories.Thesis:
							throw new InvalidOperationException();
						default:
							throw new NotImplementedEnumException(registeredCourse.CourseCategoryEnum);
					}
				default:
					throw new NotImplementedEnumException(registeredCourse.OfferedCourse.MarksSheet.MarksEntryTypeEnum);
			}
		}

		public byte? GetMaxInternals(IRegisteredCourse registeredCourse)
		{
			switch (registeredCourse.OfferedCourse.MarksSheet.MarksEntryTypeEnum)
			{
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					switch (registeredCourse.CourseCategoryEnum)
					{
						case CourseCategories.Course:
							return this.CourseInternals;
						case CourseCategories.Lab:
							return this.LabInternals;
						case CourseCategories.Internship:
							throw new InvalidOperationException();
						case CourseCategories.Project:
							throw new InvalidOperationException();
						case CourseCategories.Thesis:
							throw new InvalidOperationException();
						default:
							throw new NotImplementedEnumException(registeredCourse.CourseCategoryEnum);
					}
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
					switch (registeredCourse.CourseCategoryEnum)
					{
						case CourseCategories.Course:
						case CourseCategories.Lab:
							return null;
						case CourseCategories.Internship:
							throw new InvalidOperationException();
						case CourseCategories.Project:
							throw new InvalidOperationException();
						case CourseCategories.Thesis:
							throw new InvalidOperationException();
						default:
							throw new NotImplementedEnumException(registeredCourse.CourseCategoryEnum);
					}
				default:
					throw new NotImplementedEnumException(registeredCourse.OfferedCourse.MarksSheet.MarksEntryTypeEnum);
			}
		}

		public byte? GetMaxMid(IRegisteredCourse registeredCourse)
		{
			switch (registeredCourse.OfferedCourse.MarksSheet.MarksEntryTypeEnum)
			{
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					switch (registeredCourse.CourseCategoryEnum)
					{
						case CourseCategories.Course:
							return this.CourseMid;
						case CourseCategories.Lab:
							return this.LabMid;
						case CourseCategories.Internship:
							throw new InvalidOperationException();
						case CourseCategories.Project:
							throw new InvalidOperationException();
						case CourseCategories.Thesis:
							throw new InvalidOperationException();
						default:
							throw new NotImplementedEnumException(registeredCourse.CourseCategoryEnum);
					}
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
					switch (registeredCourse.CourseCategoryEnum)
					{
						case CourseCategories.Course:
						case CourseCategories.Lab:
							return null;
						case CourseCategories.Internship:
							throw new InvalidOperationException();
						case CourseCategories.Project:
							throw new InvalidOperationException();
						case CourseCategories.Thesis:
							throw new InvalidOperationException();
						default:
							throw new NotImplementedEnumException(registeredCourse.CourseCategoryEnum);
					}
				default:
					throw new NotImplementedEnumException(registeredCourse.OfferedCourse.MarksSheet.MarksEntryTypeEnum);
			}
		}

		public byte GetMaxFinal(IRegisteredCourse registeredCourse)
		{
			switch (registeredCourse.OfferedCourse.MarksSheet.MarksEntryTypeEnum)
			{
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
					switch (registeredCourse.CourseCategoryEnum)
					{
						case CourseCategories.Course:
							return this.CourseFinal;
						case CourseCategories.Lab:
							return this.LabFinal;
						case CourseCategories.Internship:
							throw new InvalidOperationException();
						case CourseCategories.Project:
							throw new InvalidOperationException();
						case CourseCategories.Thesis:
							throw new InvalidOperationException();
						default:
							throw new NotImplementedEnumException(registeredCourse.CourseCategoryEnum);
					}
				default:
					throw new NotImplementedEnumException(registeredCourse.OfferedCourse.MarksSheet.MarksEntryTypeEnum);
			}
		}

		private IEnumerable<(byte? Min, byte? Max, ExamGrades Grade, decimal? GradePoint)?> _grades;

		private IEnumerable<(byte? Min, byte? Max, ExamGrades Grade, decimal? GradePoint)?> Grades
		{
			get
			{
				if (this._grades == null)
				{
					IEnumerable<(byte? Min, byte? Max, ExamGrades Grade, decimal? GradePoint)?> GetGrades()
					{
						yield return (this.AMin, this.AMax, ExamGrades.A, this.AGradePoint);
						yield return (this.AMinusMin, this.AMinusMax, ExamGrades.AMinus, this.AMinusGradePoint);
						yield return (this.BPlusMin, this.BPlusMax, ExamGrades.BPlus, this.BPlusGradePoint);
						yield return (this.BMin, this.BMax, ExamGrades.B, this.BGradePoint);
						yield return (this.BMinusMin, this.BMinusMax, ExamGrades.BMinus, this.BMinusGradePoint);
						yield return (this.CPlusMin, this.CPlusMax, ExamGrades.CPlus, this.CPlusGradePoint);
						yield return (this.CMin, this.CMax, ExamGrades.C, this.CGradePoint);
						yield return (this.CMinusMin, this.CMinusMax, ExamGrades.CMinus, this.CMinusGradePoint);
						yield return (this.DPlusMin, this.DPlusMax, ExamGrades.DPlus, this.DPlusGradePoint);
						yield return (this.DMin, this.DMax, ExamGrades.D, this.DGradePoint);
						yield return ((byte?)0, this.FMax, ExamGrades.F, this.FGradePoint);
					}
					this._grades = GetGrades().Where(g => g != null && g.Value.Min != null && g.Value.Max != null).ToList();
				}
				return this._grades;
			}
		}

		public (byte? Total, ExamGrades ExamGradeEnum, decimal GradePoints, decimal Product)? GetExamGradeAndGradePoints(decimal? final, decimal total, IRegisteredCourse registeredCourse)
		{
			switch (registeredCourse.StudentStatusEnum)
			{
				case Model.Entities.Student.Statuses.AdmissionCancelled:
				case Model.Entities.Student.Statuses.Deferred:
				case Model.Entities.Student.Statuses.Dropped:
				case Model.Entities.Student.Statuses.Expelled:
				case Model.Entities.Student.Statuses.Graduated:
				case Model.Entities.Student.Statuses.Left:
				case Model.Entities.Student.Statuses.ProgramChanged:
				case Model.Entities.Student.Statuses.Rusticated:
					return null;
				case Model.Entities.Student.Statuses.TransferredToOtherCampus:
				case Model.Entities.Student.Statuses.Blocked:
				case null:
					switch (registeredCourse.FreezeStatusEnum)
					{
						case FreezedStatuses.FreezedWithFee:
						case FreezedStatuses.FreezedWithoutFee:
							return null;
						case null:
							switch (registeredCourse.StatusEnum)
							{
								case RegisteredCours.Statuses.AttendanceDefaulter:
								case RegisteredCours.Statuses.UnfairMeans:
									return (null, ExamGrades.F, this.FGradePoint, this.FGradePoint * registeredCourse.CreditHours);
								case RegisteredCours.Statuses.Incomplete:
								case RegisteredCours.Statuses.WithdrawWithFee:
								case RegisteredCours.Statuses.WithdrawWithoutFee:
								case RegisteredCours.Statuses.Deferred:
									return null;
								case null:
									if (total < 0)
										throw new InvalidOperationException($"Total must be from 0 to {registeredCourse.TotalTotal}. {nameof(registeredCourse.RegisteredCourseID)}: {registeredCourse.RegisteredCourseID}, Total: {total}.");
									var totalOutput = (byte)Math.Round(total, MidpointRounding.AwayFromZero);
									if (registeredCourse.TotalTotal < totalOutput)
										throw new InvalidOperationException($"Total must be from 0 to {registeredCourse.TotalTotal}. {nameof(registeredCourse.RegisteredCourseID)}: {registeredCourse.RegisteredCourseID}, Total: {totalOutput}.");
									if (final == null)
										return (totalOutput, ExamGrades.F, this.FGradePoint, this.FGradePoint * registeredCourse.CreditHours);
									else
									{
										var grade = this.Grades?.SingleOrDefault(g => g?.Min <= totalOutput && totalOutput <= g.Value.Max);
										if (grade == null)
											throw new InvalidOperationException($"Grade not found. Total: {totalOutput}, {nameof(this.ExamMarksPolicyID)}: {this.ExamMarksPolicyID}");
										if (this.SummerCappingExamGradeEnum != null)
											if (registeredCourse.SummerGradeCapping && Semester.IsSummer(registeredCourse.OfferedCourse.OfferedSemesterID) && !registeredCourse.IsSummerRegularSemester)
											{
												var cappedGrade = this.Grades.SingleOrDefault(g => g?.Grade == this.SummerCappingExamGradeEnum.Value);
												if (cappedGrade == null)
													throw new InvalidOperationException($"Summer Capped Grade not found. Grade: {this.SummerCappingExamGradeEnum.Value.ToFullName()}, {nameof(this.ExamMarksPolicyID)}: {this.ExamMarksPolicyID}");
												if (ExamGradeComparer.GreaterThanOrEqualTo(grade.Value.Grade, cappedGrade.Value.Grade))
													grade = cappedGrade;
											}
										if (grade.Value.GradePoint == null)
											throw new InvalidOperationException("GradePoint cannot be null.");
										return (totalOutput, grade.Value.Grade, grade.Value.GradePoint.Value, grade.Value.GradePoint.Value * registeredCourse.CreditHours);
									}
								default:
									throw new NotImplementedEnumException(registeredCourse.StatusEnum);
							}
						default:
							throw new NotImplementedEnumException(registeredCourse.FreezeStatusEnum);
					}
				default:
					throw new NotImplementedEnumException(registeredCourse.StudentStatusEnum);
			}
		}

		public string GetGradingSchemeHtml()
		{
			return this.Grades.Select(g => $"<strong>{g.Value.Grade.ToFullName()}</strong>: {g.Value.Min?.ToString().HtmlEncode()}-{g.Value.Max?.ToString().HtmlEncode()}").Join(", ".HtmlEncode());
		}
	}
}
