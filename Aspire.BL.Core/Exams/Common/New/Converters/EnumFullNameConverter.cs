﻿using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Newtonsoft.Json;
using System;

namespace Aspire.BL.Core.Exams.Common.New.Converters
{
	public sealed class EnumFullNameConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			var underLyingType = Nullable.GetUnderlyingType(objectType);
			if (underLyingType != null)
				return underLyingType.IsEnum;
			return objectType.IsEnum;
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			switch (value)
			{
				case CourseCategories courseCategory:
					writer.WriteValue(courseCategory.ToFullName());
					break;
				case CourseTypes courseType:
					writer.WriteValue(courseType.ToFullName());
					break;
				case OfferedCours.ExamCompilationTypes examCompilationType:
					writer.WriteValue(examCompilationType.ToFullName());
					break;
				case Model.Entities.OfferedCourseExam.ExamTypes examType:
					writer.WriteValue(examType.ToFullName());
					break;
				case Model.Entities.Student.Statuses status:
					writer.WriteValue(status.ToFullName());
					break;
				case ProgramDurations programDuration:
					writer.WriteValue(programDuration.ToFullName());
					break;
				case ExamGrades examGrade:
					writer.WriteValue(examGrade.ToFullName());
					break;
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes marksEntryType:
					writer.WriteValue(marksEntryType.ToFullName());
					break;
				default:
					writer.WriteValue(value);
					break;
			}
		}
	}
}
