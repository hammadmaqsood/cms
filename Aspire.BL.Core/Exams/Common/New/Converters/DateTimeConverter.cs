﻿using Newtonsoft.Json;
using System;

namespace Aspire.BL.Core.Exams.Common.New.Converters
{
	public sealed class DateTimeConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(DateTime?) || objectType == typeof(DateTime);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value is DateTime?)
				writer.WriteValue(((DateTime?)value)?.ToString());
			else if (value is DateTime dateTime)
				writer.WriteValue(dateTime.ToString());
			else
				writer.WriteValue(value);
		}
	}
}
