﻿using Aspire.Lib.Extensions;
using Newtonsoft.Json;
using System;

namespace Aspire.BL.Core.Exams.Common.New.Converters
{
	public sealed class YesNoConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(bool?) || objectType == typeof(bool);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value is bool?)
				writer.WriteValue(((bool?)value)?.ToYesNo());
			else if (value is bool boolean)
				writer.WriteValue(boolean.ToString());
			else
				writer.WriteValue(value);
		}
	}
}
