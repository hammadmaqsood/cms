﻿using Aspire.Model.Entities.Common;
using Newtonsoft.Json;
using System;

namespace Aspire.BL.Core.Exams.Common.New.Converters
{
	public sealed class SemesterIDConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return typeof(short).IsAssignableFrom(objectType);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			writer.WriteValue(((short)value).ToSemesterString());
		}
	}
}
