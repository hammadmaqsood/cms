﻿using Aspire.Model.Entities.Common;
using Newtonsoft.Json;
using System;

namespace Aspire.BL.Core.Exams.Common.New.Converters
{
	public sealed class MarksConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(decimal?) || objectType == typeof(decimal);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value is decimal?)
				writer.WriteValue(((decimal?)value)?.ToExamMarksString());
			else if (value is decimal marks)
				writer.WriteValue(marks.ToExamMarksString());
			else
				writer.WriteValue(value);
		}
	}
}
