﻿using Aspire.Model.Entities;
using System.Collections.Generic;

namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IMarksSubmissionFlow
	{
		IEnumerable<string> PendingMarksEntry { get; }
		IEnumerable<string> CanNotCompileGradesReasons { get; }
		bool CanCompileGrades { get; }
		IEnumerable<string> CanNotLockMarksEntryReasons { get; }
		bool CanLockMarksEntry { get; }
		IEnumerable<string> CanNotUnlockMarksEntryReasons { get; }
		bool CanUnlockMarksEntry { get; }
		void SetTotalsAndGrades(IRegisteredCourse iRegisteredCourse, RegisteredCours registeredCourse);
		(IEnumerable<string> compileFailureReasons, bool success) CompileTotalAndGrades();
		(IEnumerable<string> lockFailureReasons, bool success) LockMarksEntry();
		(IEnumerable<string> unlockFailureReasons, bool success) UnlockMarksEntry();
		IEnumerable<string> CanNotUnlockMarksEntryForRegisteredCourseReasons(IRegisteredCourse registeredCourse);
		(IEnumerable<string> unlockFailureReasons, bool success) UnlockMarksEntryForRegisteredCourse(IRegisteredCourse registeredCourse);
		IEnumerable<string> CanNotSubmitToHODReasons { get; }
		bool CanSubmitToHOD { get; }
		IEnumerable<string> CanNotSubmitToCampusReasons { get; }
		bool CanSubmitToCampus { get; }
		IEnumerable<string> CanNotSubmitToUniversityReasons { get; }
		bool CanSubmitToUniversity { get; }
		IEnumerable<string> CanNotRevertSubmitToHODReasons { get; }
		bool CanRevertSubmitToHOD { get; }
		IEnumerable<string> CanNotRevertSubmitToCampusReasons { get; }
		bool CanRevertSubmitToCampus { get; }
		IEnumerable<string> CanNotRevertSubmitToUniversityReasons { get; }
		bool CanRevertSubmitToUniversity { get; }
		(IEnumerable<string> submitToHODFailureReasons, bool success) SubmitToHOD();
		(IEnumerable<string> submitToCampusFailureReasons, bool success) SubmitToCampus();
		(IEnumerable<string> submitToUniversityFailureReasons, bool success) SubmitToUniversity();
		(IEnumerable<string> revertSubmitToHODFailureReasons, bool success) RevertSubmitToHOD();
		(IEnumerable<string> revertSubmitToCampusFailureReasons, bool success) RevertSubmitToCampus();
		(IEnumerable<string> revertSubmitToUniversityFailureReasons, bool success) RevertSubmitToUniversity();
	}
}
