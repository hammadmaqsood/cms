﻿using System;
using System.Collections.Generic;

namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IAssignment
	{
		IEnumerable<string> CanNotAddAssignmentReasons { get; }
		bool CanAddAssignment { get; }
		IEnumerable<string> CanNotEditOrDeleteAssignmentReasons(int offeredCourseExamID);
		bool CanEditOrDeleteAssignment(int offeredCourseExamID);
		IEnumerable<string> CanNotUnlockAssignmentReasons(int offeredCourseExamID);
		bool CanUnlockAssignment(int offeredCourseExamID);
		IReadOnlyList<IStudentMarks> GetAssignmentMarks(int offeredCourseExamID);
		(IEnumerable<string> addFailureReasons, int? offeredCourseExamID) AddAssignment(DateTime examDate, byte examNo, decimal totalMarks, string remarks);
		(IEnumerable<string> updateFailureReasons, bool success) UpdateAssignment(int offeredCourseExamID, DateTime examDate, byte examNo, string remarks, IReadOnlyDictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)> marks, bool submit);
		(IEnumerable<string> deleteFailureReasons, bool success) DeleteAssignment(int offeredCourseExamID);
		(IEnumerable<string> unlockFailureReasons, bool success) UnlockAssignment(int offeredCourseExamID);
	}
}
