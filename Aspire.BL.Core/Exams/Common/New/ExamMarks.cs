﻿using Newtonsoft.Json;

namespace Aspire.BL.Core.Exams.Common.New
{
	[JsonObject(MemberSerialization.OptIn)]
	internal sealed class ExamMarks : IExamMarks
	{
		public ExamMarks(IOfferedCourseExam offeredCourseExam, IRegisteredCourseExamMark registeredCourseExamMark)
		{
			this.OfferedCourseExam = offeredCourseExam;
			this.RegisteredCourseExamMark = registeredCourseExamMark;
		}

		public IOfferedCourseExam OfferedCourseExam { get; }

		[JsonProperty]
		public int OfferedCourseExamID => this.OfferedCourseExam.OfferedCourseExamID;

		[JsonProperty("Marks")]
		public IRegisteredCourseExamMark RegisteredCourseExamMark { get; }
	}
}