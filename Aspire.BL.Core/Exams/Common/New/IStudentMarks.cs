﻿namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IStudentMarks
	{
		IRegisteredCourse RegisteredCourse { get; }
		decimal Min { get; }
		decimal Max { get; }
		decimal? MarksObtained { get; }
	}
}