﻿namespace Aspire.BL.Core.Exams.Common.New
{
	internal sealed class StudentMarks : IStudentMarks
	{
		public StudentMarks(IRegisteredCourse registeredCourse, decimal min, decimal max, decimal? marksObtainedInDB)
		{
			this.RegisteredCourse = registeredCourse;
			this.Min = min;
			this.Max = max;
			this.MarksObtained = marksObtainedInDB;
		}

		public IRegisteredCourse RegisteredCourse { get; }
		public decimal Min { get; }
		public decimal Max { get; }
		public decimal? MarksObtained { get; }
	}
}