﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;

namespace Aspire.BL.Core.Exams.Common.New.MarksEntryTypes
{
	internal sealed class FacultyMemberAssignmentsQuizzesMidFinalMarksSheet : BaseAssignmentsQuizzesMidFinalMarksSheet, IFacultyMemberMarksSheet
	{
		internal FacultyMemberAssignmentsQuizzesMidFinalMarksSheet(AspireContext aspireContext, int offeredCourseID, int userLoginHistoryID, Guid loginSessionGuid) : base(aspireContext, offeredCourseID, userLoginHistoryID, loginSessionGuid)
		{
			this.ExamMarksTypeEnum = FacultyMemberHelperMethods.GetExamMarksSettings(aspireContext, this);
			this.Options = new Options(this);
		}

		public override string ReloadAndToJson(out MarksSheetChanx.ManagementEmailRecipients? managementEmailRecipients)
		{
			var marksSheet = new FacultyMemberAssignmentsQuizzesMidFinalMarksSheet(this.AspireContext, this.OfferedCourse.OfferedCourseID, this.UserLoginHistoryID, this.LoginSessionGuid);
			managementEmailRecipients = marksSheet.ManagementEmailRecipients;
			return marksSheet.ToJson();
		}

		public ExamMarksTypes? ExamMarksTypeEnum { get; }

		public override IOptions Options { get; }

		#region IAssignments

		public override IEnumerable<string> CanNotEditAssignmentsReasons => FacultyMemberHelperMethods.CanNotEditAssignmentsQuizzesMidFinalReasons(this, ExamMarksTypes.Assignments);

		public override IEnumerable<string> CanNotUnlockAssignmentsReasons => FacultyMemberHelperMethods.CanNotUnlockMarksReasons;

		#endregion

		#region IQuizzes

		public override IEnumerable<string> CanNotEditQuizzesReasons => FacultyMemberHelperMethods.CanNotEditAssignmentsQuizzesMidFinalReasons(this, ExamMarksTypes.Quizzes);

		public override IEnumerable<string> CanNotUnlockQuizzesReasons => FacultyMemberHelperMethods.CanNotUnlockMarksReasons;

		#endregion

		#region IMid

		public override IEnumerable<string> CanNotEditMidReasons => FacultyMemberHelperMethods.CanNotEditAssignmentsQuizzesMidFinalReasons(this, ExamMarksTypes.Mid);

		public override IEnumerable<string> CanNotUnlockMidReasons => FacultyMemberHelperMethods.CanNotUnlockMarksReasons;

		#endregion

		#region IFinal

		public override IEnumerable<string> CanNotEditFinalReasons => FacultyMemberHelperMethods.CanNotEditAssignmentsQuizzesMidFinalReasons(this, ExamMarksTypes.Final);

		public override IEnumerable<string> CanNotUnlockFinalReasons => FacultyMemberHelperMethods.CanNotUnlockMarksReasons;

		#endregion

		#region IMarksSubmissionFlow

		public override IEnumerable<string> CanNotCompileGradesReasons => FacultyMemberHelperMethods.CanNotCompileGradesReasons(this);

		public override IEnumerable<string> CanNotLockMarksEntryReasons => FacultyMemberHelperMethods.CanNotLockMarksEntryReasons(this);

		public override IEnumerable<string> CanNotUnlockMarksEntryReasons => FacultyMemberHelperMethods.CanNotUnlockMarksReasons;

		public override IEnumerable<string> CanNotSubmitToHODReasons => FacultyMemberHelperMethods.CanNotSubmitToHODReasons(this);

		public override IEnumerable<string> CanNotSubmitToCampusReasons => FacultyMemberHelperMethods.CanNotSubmitToCampusReasons;

		public override IEnumerable<string> CanNotSubmitToUniversityReasons => FacultyMemberHelperMethods.CanNotSubmitToUniversityReasons;

		public override IEnumerable<string> CanNotRevertSubmitToHODReasons => FacultyMemberHelperMethods.CanNotRevertSubmitToHODReasons;

		public override IEnumerable<string> CanNotRevertSubmitToCampusReasons => FacultyMemberHelperMethods.CanNotRevertSubmitToCampusReasons;

		public override IEnumerable<string> CanNotRevertSubmitToUniversityReasons => FacultyMemberHelperMethods.CanNotRevertSubmitToUniversityReasons;

		#endregion
	}
}