﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.New.MarksEntryTypes
{
	internal abstract class BaseAssignmentQuizCompileInternalsMidFinalMarksSheet : BaseMarksSheet
	{
		protected BaseAssignmentQuizCompileInternalsMidFinalMarksSheet(AspireContext aspireContext, int offeredCourseID, int userLoginHistoryID, Guid loginSessionGuid) : base(aspireContext, offeredCourseID, userLoginHistoryID, loginSessionGuid)
		{
			this.Display = New.Display.CreateAssignmentQuizCompileInternalsMidFinal(this);
		}

		public sealed override Model.Entities.ExamMarksPolicy.MarksEntryTypes MarksEntryTypeEnum => Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal;

		public override IDisplay Display { get; }

		#region IAssignment

		public abstract override IEnumerable<string> CanNotAddAssignmentReasons { get; }

		public abstract override IEnumerable<string> CanNotEditOrDeleteAssignmentReasons(int offeredCourseExamID);

		public abstract override IEnumerable<string> CanNotUnlockAssignmentReasons(int offeredCourseExamID);

		public sealed override IReadOnlyList<IStudentMarks> GetAssignmentMarks(int offeredCourseExamID)
		{
			var offeredCourseExam = this.OfferedCourse.OfferedCourseExamAssignments.SingleOrDefault(oce => oce.OfferedCourseExamID == offeredCourseExamID);
			if (offeredCourseExam == null)
				return null;
			return MarksSheetHelperMethods.GetMarksList(offeredCourseExam);
		}

		#endregion

		#region IAssignments

		public sealed override IEnumerable<string> CanNotEditAssignmentsReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Assignments);

		public sealed override IEnumerable<string> CanNotUnlockAssignmentsReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Assignments);

		public sealed override IEnumerable<string> CanNotCompileAssignmentsReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Assignments);

		public sealed override IReadOnlyList<IStudentMarks> GetAssignmentsMarks()
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Assignments).First());
		}

		public sealed override decimal? GetCompiledAssignmentsMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Assignments).First());
		}

		#endregion

		#region IQuiz

		public abstract override IEnumerable<string> CanNotAddQuizReasons { get; }

		public abstract override IEnumerable<string> CanNotEditOrDeleteQuizReasons(int offeredCourseExamID);

		public abstract override IEnumerable<string> CanNotUnlockQuizReasons(int offeredCourseExamID);

		public sealed override IReadOnlyList<IStudentMarks> GetQuizMarks(int offeredCourseExamID)
		{
			var offeredCourseExam = this.OfferedCourse.OfferedCourseExamQuizzes.SingleOrDefault(oce => oce.OfferedCourseExamID == offeredCourseExamID);
			if (offeredCourseExam == null)
				return null;
			return MarksSheetHelperMethods.GetMarksList(offeredCourseExam);
		}

		#endregion

		#region IQuizzes

		public sealed override IEnumerable<string> CanNotEditQuizzesReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Quizzes);

		public sealed override IEnumerable<string> CanNotUnlockQuizzesReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Quizzes);

		public sealed override IEnumerable<string> CanNotCompileQuizzesReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Quizzes);

		public sealed override IReadOnlyList<IStudentMarks> GetQuizzesMarks()
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Quizzes).First());
		}

		public sealed override decimal? GetCompiledQuizzesMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Quizzes).First());
		}

		#endregion

		#region IInternals

		public abstract override IEnumerable<string> CanNotUnlockInternalsReasons { get; }

		public abstract override IEnumerable<string> CanNotCompileInternalsReasons { get; }

		public sealed override decimal? GetCompiledInternalsMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType)
		{
			var totalMarks = registeredCourse.InternalsTotal
				?? throw new InvalidOperationException($"{nameof(registeredCourse.InternalsTotal)} cannot be null.  {nameof(registeredCourse.RegisteredCourseID)}:{registeredCourse.RegisteredCourseID}, {nameof(registeredCourse.ExamMarksPolicyID)}:{registeredCourse.ExamMarksPolicyID}.");
			return MarksSheetHelperMethods.GetCompiledMarksPercentage(registeredCourse, CompileExamMarksTypes.Internals, examCompilationType, totalMarks);
		}

		#endregion

		#region IMid

		public abstract override IEnumerable<string> CanNotEditMidReasons { get; }

		public abstract override IEnumerable<string> CanNotUnlockMidReasons { get; }

		public sealed override IReadOnlyList<IStudentMarks> GetMidMarks()
		{
			return MarksSheetHelperMethods.GetMarksList(this.OfferedCourse, ExamMarksTypes.Mid);
		}

		#endregion

		#region IFinal

		public abstract override IEnumerable<string> CanNotEditFinalReasons { get; }

		public abstract override IEnumerable<string> CanNotUnlockFinalReasons { get; }

		#endregion

		#region IMarksSubmissionFlow

		public sealed override IEnumerable<string> PendingMarksEntry
		{
			get
			{
				//if (!this.OfferedCourse.OfferedCourseExams.Any(oce => oce.ExamTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Assignment || oce.ExamTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Quiz) || this.OfferedCourse.InternalsExamCompilationTypeEnum == null)
				//	yield return $"{ExamMarksTypes.Internals.ToFullName()} Marks are not yet submitted.";
				//if (this.OfferedCourse.RegisteredCourses.All(rc => rc.Mid == null))
				//	yield return $"{ExamMarksTypes.Mid.ToFullName()} Marks are not yet submitted.";
				//if (this.OfferedCourse.RegisteredCourses.All(rc => rc.Final == null))
				//	yield return $"{ExamMarksTypes.Final.ToFullName()} Marks are not yet submitted.";
				if (this.OfferedCourse.InternalsMarksSubmittedDate == null)
					yield return $"{ExamMarksTypes.Internals.ToFullName()} Marks are not yet submitted.";
				if (this.OfferedCourse.MidMarksSubmittedDate == null)
					yield return $"{ExamMarksTypes.Mid.ToFullName()} Marks are not yet submitted.";
				if (this.OfferedCourse.FinalMarksSubmittedDate == null)
					yield return $"{ExamMarksTypes.Final.ToFullName()} Marks are not yet submitted.";
			}
		}

		public abstract override IEnumerable<string> CanNotCompileGradesReasons { get; }

		public abstract override IEnumerable<string> CanNotLockMarksEntryReasons { get; }

		public sealed override void SetTotalsAndGrades(IRegisteredCourse iRegisteredCourse, RegisteredCours registeredCourse)
		{
			if (!iRegisteredCourse.CompilationAllowed)
			{
				registeredCourse.Internals = null;
				registeredCourse.Total = null;
				registeredCourse.Grade = null;
				registeredCourse.GradePoints = null;
				registeredCourse.Product = null;
			}
			else
			{
				registeredCourse.Internals = iRegisteredCourse.GetCompiledInternalsMarks(this.OfferedCourse.InternalsExamCompilationTypeEnum ?? throw new InvalidOperationException());
				var total = (registeredCourse.Internals ?? 0) + (registeredCourse.Mid ?? 0) + (registeredCourse.Final ?? 0);
				var grade = iRegisteredCourse.ExamMarksPolicyToBeApplied.GetExamGradeAndGradePoints(registeredCourse.Final, total, iRegisteredCourse);
				registeredCourse.Total = grade?.Total;
				registeredCourse.GradeEnum = grade?.ExamGradeEnum;
				registeredCourse.GradePoints = grade?.GradePoints;
				registeredCourse.Product = grade?.GradePoints * iRegisteredCourse.CreditHours;
			}
		}

		public abstract override IEnumerable<string> CanNotUnlockMarksEntryReasons { get; }

		public abstract override IEnumerable<string> CanNotSubmitToHODReasons { get; }

		public abstract override IEnumerable<string> CanNotSubmitToCampusReasons { get; }

		public abstract override IEnumerable<string> CanNotSubmitToUniversityReasons { get; }

		public abstract override IEnumerable<string> CanNotRevertSubmitToHODReasons { get; }

		public abstract override IEnumerable<string> CanNotRevertSubmitToCampusReasons { get; }

		public abstract override IEnumerable<string> CanNotRevertSubmitToUniversityReasons { get; }

		#endregion
	}
}