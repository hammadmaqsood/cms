﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.New.MarksEntryTypes
{
	internal abstract class BaseAssignmentCompileAssignmentsQuizCompileQuizzesMidFinalMarksSheet : BaseMarksSheet
	{
		protected BaseAssignmentCompileAssignmentsQuizCompileQuizzesMidFinalMarksSheet(AspireContext aspireContext, int offeredCourseID, int userLoginHistoryID, Guid loginSessionGuid) : base(aspireContext, offeredCourseID, userLoginHistoryID, loginSessionGuid)
		{
			this.Display = New.Display.CreateAssignmentCompileAssignmentsQuizCompileQuizzesMidFinal(this);
		}

		public sealed override Model.Entities.ExamMarksPolicy.MarksEntryTypes MarksEntryTypeEnum => Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal;

		public sealed override IDisplay Display { get; }

		#region IAssignment

		public abstract override IEnumerable<string> CanNotAddAssignmentReasons { get; }

		public abstract override IEnumerable<string> CanNotEditOrDeleteAssignmentReasons(int offeredCourseExamID);

		public abstract override IEnumerable<string> CanNotUnlockAssignmentReasons(int offeredCourseExamID);

		public sealed override IReadOnlyList<IStudentMarks> GetAssignmentMarks(int offeredCourseExamID)
		{
			var offeredCourseExam = this.OfferedCourse.OfferedCourseExamAssignments.SingleOrDefault(oce => oce.OfferedCourseExamID == offeredCourseExamID);
			if (offeredCourseExam == null)
				return null;
			return MarksSheetHelperMethods.GetMarksList(offeredCourseExam);
		}

		#endregion

		#region IAssignments

		public sealed override IEnumerable<string> CanNotEditAssignmentsReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Assignments);

		public abstract override IEnumerable<string> CanNotUnlockAssignmentsReasons { get; }

		public abstract override IEnumerable<string> CanNotCompileAssignmentsReasons { get; }

		public sealed override IReadOnlyList<IStudentMarks> GetAssignmentsMarks()
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Assignments).First());
		}

		public sealed override decimal? GetCompiledAssignmentsMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType)
		{
			var totalMarks = registeredCourse.AssignmentsTotal
				?? throw new InvalidOperationException($"{nameof(registeredCourse.AssignmentsTotal)} cannot be null.  {nameof(registeredCourse.RegisteredCourseID)}:{registeredCourse.RegisteredCourseID}, {nameof(registeredCourse.ExamMarksPolicyID)}:{registeredCourse.ExamMarksPolicyID}.");
			return MarksSheetHelperMethods.GetCompiledMarksPercentage(registeredCourse, CompileExamMarksTypes.Assignments, examCompilationType, totalMarks);
		}

		#endregion

		#region IQuiz

		public abstract override IEnumerable<string> CanNotAddQuizReasons { get; }

		public abstract override IEnumerable<string> CanNotEditOrDeleteQuizReasons(int offeredCourseExamID);

		public abstract override IEnumerable<string> CanNotUnlockQuizReasons(int offeredCourseExamID);

		public sealed override IReadOnlyList<IStudentMarks> GetQuizMarks(int offeredCourseExamID)
		{
			var offeredCourseExam = this.OfferedCourse.OfferedCourseExamQuizzes.SingleOrDefault(oce => oce.OfferedCourseExamID == offeredCourseExamID);
			if (offeredCourseExam == null)
				return null;
			return MarksSheetHelperMethods.GetMarksList(offeredCourseExam);
		}

		#endregion

		#region IQuizzes

		public sealed override IEnumerable<string> CanNotEditQuizzesReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Quizzes);

		public abstract override IEnumerable<string> CanNotUnlockQuizzesReasons { get; }

		public abstract override IEnumerable<string> CanNotCompileQuizzesReasons { get; }

		public sealed override IReadOnlyList<IStudentMarks> GetQuizzesMarks()
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Quizzes).First());
		}

		public sealed override decimal? GetCompiledQuizzesMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType)
		{
			var totalMarks = registeredCourse.QuizzesTotal
				?? throw new InvalidOperationException($"{nameof(registeredCourse.QuizzesTotal)} cannot be null.  {nameof(registeredCourse.RegisteredCourseID)}:{registeredCourse.RegisteredCourseID}, {nameof(registeredCourse.ExamMarksPolicyID)}:{registeredCourse.ExamMarksPolicyID}.");
			return MarksSheetHelperMethods.GetCompiledMarksPercentage(registeredCourse, CompileExamMarksTypes.Quizzes, examCompilationType, totalMarks);
		}

		#endregion

		#region IInternals

		public sealed override IEnumerable<string> CanNotUnlockInternalsReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Internals);

		public sealed override IEnumerable<string> CanNotCompileInternalsReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Internals);

		public sealed override decimal? GetCompiledInternalsMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Internals).First());
		}

		#endregion

		#region IMid

		public abstract override IEnumerable<string> CanNotEditMidReasons { get; }

		public abstract override IEnumerable<string> CanNotUnlockMidReasons { get; }

		public sealed override IReadOnlyList<IStudentMarks> GetMidMarks()
		{
			return MarksSheetHelperMethods.GetMarksList(this.OfferedCourse, ExamMarksTypes.Mid);
		}

		#endregion

		#region IFinal

		public abstract override IEnumerable<string> CanNotEditFinalReasons { get; }

		public abstract override IEnumerable<string> CanNotUnlockFinalReasons { get; }

		#endregion

		#region IMarksSubmissionFlow

		public sealed override IEnumerable<string> PendingMarksEntry
		{
			get
			{
				//if (!this.OfferedCourse.OfferedCourseExamAssignments.Any() || this.OfferedCourse.AssignmentsExamCompilationTypeEnum == null)
				//	yield return $"{ExamMarksTypes.Assignments.ToFullName()} Marks are not yet submitted.";
				//if (!this.OfferedCourse.OfferedCourseExamQuizzes.Any() || this.OfferedCourse.QuizzesExamCompilationTypeEnum == null)
				//	yield return $"{ExamMarksTypes.Quizzes.ToFullName()} Marks are not yet submitted.";
				//if (this.OfferedCourse.RegisteredCourses.All(rc => rc.Mid == null))
				//	yield return $"{ExamMarksTypes.Mid.ToFullName()} Marks are not yet submitted.";
				//if (this.OfferedCourse.RegisteredCourses.All(rc => rc.Final == null))
				//	yield return $"{ExamMarksTypes.Final.ToFullName()} Marks are not yet submitted.";
				if (this.OfferedCourse.AssignmentsMarksSubmittedDate == null)
					yield return $"{ExamMarksTypes.Assignments.ToFullName()} Marks are not yet submitted.";
				if (this.OfferedCourse.QuizzesMarksSubmittedDate == null)
					yield return $"{ExamMarksTypes.Quizzes.ToFullName()} Marks are not yet submitted.";
				if (this.OfferedCourse.MidMarksSubmittedDate == null)
					yield return $"{ExamMarksTypes.Mid.ToFullName()} Marks are not yet submitted.";
				if (this.OfferedCourse.FinalMarksSubmittedDate == null)
					yield return $"{ExamMarksTypes.Final.ToFullName()} Marks are not yet submitted.";
			}
		}

		public abstract override IEnumerable<string> CanNotCompileGradesReasons { get; }

		public abstract override IEnumerable<string> CanNotLockMarksEntryReasons { get; }

		public sealed override void SetTotalsAndGrades(IRegisteredCourse iRegisteredCourse, RegisteredCours registeredCourse)
		{
			if (!iRegisteredCourse.CompilationAllowed)
			{
				registeredCourse.Assignments = null;
				registeredCourse.Quizzes = null;
				registeredCourse.Total = null;
				registeredCourse.Grade = null;
				registeredCourse.GradePoints = null;
				registeredCourse.Product = null;
			}
			else
			{
				registeredCourse.Assignments = iRegisteredCourse.GetCompiledAssignmentsMarks(this.OfferedCourse.AssignmentsExamCompilationTypeEnum ?? throw new InvalidOperationException());
				registeredCourse.Quizzes = iRegisteredCourse.GetCompiledQuizzesMarks(this.OfferedCourse.QuizzesExamCompilationTypeEnum ?? throw new InvalidOperationException());
				var total = (registeredCourse.Assignments ?? 0) + (registeredCourse.Quizzes ?? 0) + (registeredCourse.Mid ?? 0) + (registeredCourse.Final ?? 0);
				var grade = iRegisteredCourse.ExamMarksPolicyToBeApplied.GetExamGradeAndGradePoints(registeredCourse.Final, total, iRegisteredCourse);
				registeredCourse.Total = grade?.Total;
				registeredCourse.GradeEnum = grade?.ExamGradeEnum;
				registeredCourse.GradePoints = grade?.GradePoints;
				registeredCourse.Product = grade?.GradePoints * iRegisteredCourse.CreditHours;
			}
		}

		public abstract override IEnumerable<string> CanNotUnlockMarksEntryReasons { get; }

		public abstract override IEnumerable<string> CanNotSubmitToHODReasons { get; }

		public abstract override IEnumerable<string> CanNotSubmitToCampusReasons { get; }

		public abstract override IEnumerable<string> CanNotSubmitToUniversityReasons { get; }

		public abstract override IEnumerable<string> CanNotRevertSubmitToHODReasons { get; }

		public abstract override IEnumerable<string> CanNotRevertSubmitToCampusReasons { get; }

		public abstract override IEnumerable<string> CanNotRevertSubmitToUniversityReasons { get; }

		#endregion
	}
}