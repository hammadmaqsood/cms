﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;

namespace Aspire.BL.Core.Exams.Common.New.MarksEntryTypes
{
	internal sealed class StaffAssignmentsQuizzesMidFinalMarksSheet : BaseAssignmentsQuizzesMidFinalMarksSheet, IStaffMarksSheet
	{
		internal StaffAssignmentsQuizzesMidFinalMarksSheet(AspireContext aspireContext, int offeredCourseID, int userLoginHistoryID, Guid loginSessionGuid) : base(aspireContext, offeredCourseID, userLoginHistoryID, loginSessionGuid)
		{
			(this.UniversityLevelMarksEntryAllowed, this.CanUnlockCampusLevelMarksEntry, this.CampusLevelMarksEntryAllowed, this.DepartmentLevelMarksEntryAllowed, this.IsPermittedToRevertSubmitToHOD, this.IsPermittedToRevertSubmitToCampus, this.IsPermittedToRevertSubmitToUniversity) = StaffHelperMethods.GetPermissions(aspireContext, loginSessionGuid, this);
			this.Options = new Options(this);
		}

		public override string ReloadAndToJson(out MarksSheetChanx.ManagementEmailRecipients? managementEmailRecipients)
		{
			var marksSheet = new StaffAssignmentsQuizzesMidFinalMarksSheet(this.AspireContext, this.OfferedCourse.OfferedCourseID, this.UserLoginHistoryID, this.LoginSessionGuid);
			managementEmailRecipients = marksSheet.ManagementEmailRecipients;
			return marksSheet.ToJson();
		}

		public override IOptions Options { get; }

		public bool UniversityLevelMarksEntryAllowed { get; }

		public bool CanUnlockCampusLevelMarksEntry { get; }

		public bool CampusLevelMarksEntryAllowed { get; }

		public bool DepartmentLevelMarksEntryAllowed { get; }

		public bool IsPermittedToRevertSubmitToHOD { get; }

		public bool IsPermittedToRevertSubmitToCampus { get; }

		public bool IsPermittedToRevertSubmitToUniversity { get; }

		#region IAssignments

		public override IEnumerable<string> CanNotUnlockAssignmentsReasons => StaffHelperMethods.CanNotUnlockAssignmentsQuizzesInternalsMidFinalReasons(this, ExamMarksTypes.Assignments);

		public override IEnumerable<string> CanNotEditAssignmentsReasons => StaffHelperMethods.CanNotEditAssignmentsQuizzesMidFinalReasons(this, ExamMarksTypes.Assignments);

		#endregion

		#region IQuizzes

		public override IEnumerable<string> CanNotUnlockQuizzesReasons => StaffHelperMethods.CanNotUnlockAssignmentsQuizzesInternalsMidFinalReasons(this, ExamMarksTypes.Quizzes);

		public override IEnumerable<string> CanNotEditQuizzesReasons => StaffHelperMethods.CanNotEditAssignmentsQuizzesMidFinalReasons(this, ExamMarksTypes.Quizzes);

		#endregion

		#region IMid

		public override IEnumerable<string> CanNotEditMidReasons => StaffHelperMethods.CanNotEditAssignmentsQuizzesMidFinalReasons(this, ExamMarksTypes.Mid);

		public override IEnumerable<string> CanNotUnlockMidReasons => StaffHelperMethods.CanNotUnlockAssignmentsQuizzesInternalsMidFinalReasons(this, ExamMarksTypes.Mid);

		#endregion

		#region IFinal

		public override IEnumerable<string> CanNotEditFinalReasons => StaffHelperMethods.CanNotEditAssignmentsQuizzesMidFinalReasons(this, ExamMarksTypes.Final);

		public override IEnumerable<string> CanNotUnlockFinalReasons => StaffHelperMethods.CanNotUnlockAssignmentsQuizzesInternalsMidFinalReasons(this, ExamMarksTypes.Final);

		#endregion

		#region IMarksSubmissionFlow

		public override IEnumerable<string> CanNotCompileGradesReasons => StaffHelperMethods.CanNotCompileGradesReasons(this);

		public override IEnumerable<string> CanNotLockMarksEntryReasons => StaffHelperMethods.CanNotLockMarksEntryReasons(this);

		public override IEnumerable<string> CanNotUnlockMarksEntryReasons => StaffHelperMethods.CanNotUnlockMarksEntryReasons(this);

		public override IEnumerable<string> CanNotSubmitToHODReasons => StaffHelperMethods.CanNotSubmitToHODReasons(this);

		public override IEnumerable<string> CanNotSubmitToCampusReasons => StaffHelperMethods.CanNotSubmitToCampusReasons(this);

		public override IEnumerable<string> CanNotSubmitToUniversityReasons => StaffHelperMethods.CanNotSubmitToUniversityReasons(this);

		public override IEnumerable<string> CanNotRevertSubmitToHODReasons => StaffHelperMethods.CanNotRevertSubmitToHODReasons(this);

		public override IEnumerable<string> CanNotRevertSubmitToCampusReasons => StaffHelperMethods.CanNotRevertSubmitToCampusReasons(this);

		public override IEnumerable<string> CanNotRevertSubmitToUniversityReasons => StaffHelperMethods.CanNotRevertSubmitToUniversityReasons(this);

		#endregion
	}
}