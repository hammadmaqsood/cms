﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.New.MarksEntryTypes
{
	internal abstract class BaseFinalMarksSheet : BaseMarksSheet
	{
		protected BaseFinalMarksSheet(AspireContext aspireContext, int offeredCourseID, int userLoginHistoryID, Guid loginSessionGuid) : base(aspireContext, offeredCourseID, userLoginHistoryID, loginSessionGuid)
		{
			this.Display = New.Display.CreateFinal(this);
		}

		public sealed override Model.Entities.ExamMarksPolicy.MarksEntryTypes MarksEntryTypeEnum => Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final;

		public override IDisplay Display { get; }

		#region IAssignment

		public sealed override IEnumerable<string> CanNotAddAssignmentReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(Model.Entities.OfferedCourseExam.ExamTypes.Assignment);

		public sealed override IEnumerable<string> CanNotEditOrDeleteAssignmentReasons(int offeredCourseExamID)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(Model.Entities.OfferedCourseExam.ExamTypes.Assignment).First());
		}

		public sealed override IEnumerable<string> CanNotUnlockAssignmentReasons(int offeredCourseExamID)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(Model.Entities.OfferedCourseExam.ExamTypes.Assignment).First());
		}

		public sealed override IReadOnlyList<IStudentMarks> GetAssignmentMarks(int offeredCourseExamID)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(Model.Entities.OfferedCourseExam.ExamTypes.Assignment).First());
		}

		#endregion

		#region IAssignments

		public sealed override IEnumerable<string> CanNotEditAssignmentsReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Assignments);

		public sealed override IEnumerable<string> CanNotUnlockAssignmentsReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Assignments);

		public sealed override IEnumerable<string> CanNotCompileAssignmentsReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Assignments);

		public sealed override IReadOnlyList<IStudentMarks> GetAssignmentsMarks()
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Assignments).First());
		}

		public sealed override decimal? GetCompiledAssignmentsMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Assignments).First());
		}

		#endregion

		#region IQuiz

		public sealed override IEnumerable<string> CanNotAddQuizReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(Model.Entities.OfferedCourseExam.ExamTypes.Quiz);

		public sealed override IEnumerable<string> CanNotEditOrDeleteQuizReasons(int offeredCourseExamID)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(Model.Entities.OfferedCourseExam.ExamTypes.Quiz).First());
		}

		public sealed override IEnumerable<string> CanNotUnlockQuizReasons(int offeredCourseExamID)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(Model.Entities.OfferedCourseExam.ExamTypes.Quiz).First());
		}

		public sealed override IReadOnlyList<IStudentMarks> GetQuizMarks(int offeredCourseExamID)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(Model.Entities.OfferedCourseExam.ExamTypes.Quiz).First());
		}

		#endregion

		#region IQuizzes

		public sealed override IEnumerable<string> CanNotEditQuizzesReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Quizzes);

		public sealed override IEnumerable<string> CanNotUnlockQuizzesReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Quizzes);

		public sealed override IEnumerable<string> CanNotCompileQuizzesReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Quizzes);

		public sealed override IReadOnlyList<IStudentMarks> GetQuizzesMarks()
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Quizzes).First());
		}

		public sealed override decimal? GetCompiledQuizzesMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Quizzes).First());
		}

		#endregion

		#region IInternals

		public sealed override IEnumerable<string> CanNotUnlockInternalsReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Internals);

		public sealed override IEnumerable<string> CanNotCompileInternalsReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Internals);

		public sealed override decimal? GetCompiledInternalsMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Internals).First());
		}

		#endregion

		#region IMid

		public sealed override IEnumerable<string> CanNotEditMidReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Mid);

		public sealed override IEnumerable<string> CanNotUnlockMidReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Mid);

		public sealed override IReadOnlyList<IStudentMarks> GetMidMarks()
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Mid).First());
		}

		#endregion

		#region IFinal

		public abstract override IEnumerable<string> CanNotEditFinalReasons { get; }

		public abstract override IEnumerable<string> CanNotUnlockFinalReasons { get; }

		#endregion

		#region IMarksSubmissionFlow

		public sealed override IEnumerable<string> PendingMarksEntry
		{
			get
			{
				//if (this.OfferedCourse.RegisteredCourses.All(rc => rc.Final == null))
				//	yield return $"{ExamMarksTypes.Final.ToFullName()} Marks are not yet submitted.";
				if (this.OfferedCourse.FinalMarksSubmittedDate == null)
					yield return $"{ExamMarksTypes.Final.ToFullName()} Marks are not yet submitted.";
			}
		}

		public abstract override IEnumerable<string> CanNotCompileGradesReasons { get; }

		public abstract override IEnumerable<string> CanNotLockMarksEntryReasons { get; }

		public sealed override void SetTotalsAndGrades(IRegisteredCourse iRegisteredCourse, RegisteredCours registeredCourse)
		{
			if (!iRegisteredCourse.CompilationAllowed)
			{
				registeredCourse.Total = null;
				registeredCourse.Grade = null;
				registeredCourse.GradePoints = null;
				registeredCourse.Product = null;
			}
			else
			{
				var total = registeredCourse.Final ?? 0;
				var grade = iRegisteredCourse.ExamMarksPolicyToBeApplied.GetExamGradeAndGradePoints(registeredCourse.Final, total, iRegisteredCourse);
				registeredCourse.Total = grade?.Total;
				registeredCourse.GradeEnum = grade?.ExamGradeEnum;
				registeredCourse.GradePoints = grade?.GradePoints;
				registeredCourse.Product = grade?.GradePoints * iRegisteredCourse.CreditHours;
			}
		}

		public abstract override IEnumerable<string> CanNotUnlockMarksEntryReasons { get; }

		public abstract override IEnumerable<string> CanNotSubmitToHODReasons { get; }

		public abstract override IEnumerable<string> CanNotSubmitToCampusReasons { get; }

		public abstract override IEnumerable<string> CanNotSubmitToUniversityReasons { get; }

		public abstract override IEnumerable<string> CanNotRevertSubmitToHODReasons { get; }

		public abstract override IEnumerable<string> CanNotRevertSubmitToCampusReasons { get; }

		public abstract override IEnumerable<string> CanNotRevertSubmitToUniversityReasons { get; }

		#endregion
	}
}