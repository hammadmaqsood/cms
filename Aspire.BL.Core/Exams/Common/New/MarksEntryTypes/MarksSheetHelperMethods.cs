﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.New.MarksEntryTypes
{
	internal static class MarksSheetHelperMethods
	{
		internal static IEnumerable<string> ToEnumerable(this string input)
		{
			yield return input;
		}

		public static List<RegisteredCours> GetRegisteredCourses(AspireContext aspireContext, IMarksSheet marksSheet)
		{
			return aspireContext.RegisteredCourses.Where(rc => rc.OfferedCourseID == marksSheet.OfferedCourse.OfferedCourseID && rc.DeletedDate == null).ToList();
		}

		public static IReadOnlyList<IStudentMarks> GetMarksList(IOfferedCourse offeredCourse, ExamMarksTypes examMarksTypeEnum)
		{
			switch (examMarksTypeEnum)
			{
				case ExamMarksTypes.Assignments:
					return offeredCourse.RegisteredCourses.Select(rc => (IStudentMarks)new StudentMarks(rc, 0, rc.AssignmentsTotal ?? throw new InvalidOperationException($"{examMarksTypeEnum} Total Marks can not be null."), rc.Assignments)).ToList();
				case ExamMarksTypes.Quizzes:
					return offeredCourse.RegisteredCourses.Select(rc => (IStudentMarks)new StudentMarks(rc, 0, rc.QuizzesTotal ?? throw new InvalidOperationException($"{examMarksTypeEnum} Total Marks can not be null."), rc.Quizzes)).ToList();
				case ExamMarksTypes.Internals:
					return offeredCourse.RegisteredCourses.Select(rc => (IStudentMarks)new StudentMarks(rc, 0, rc.InternalsTotal ?? throw new InvalidOperationException($"{examMarksTypeEnum} Total Marks can not be null."), rc.Internals)).ToList();
				case ExamMarksTypes.Mid:
					return offeredCourse.RegisteredCourses.Select(rc => (IStudentMarks)new StudentMarks(rc, 0, rc.MidTotal ?? throw new InvalidOperationException($"{examMarksTypeEnum} Total Marks can not be null."), rc.Mid)).ToList();
				case ExamMarksTypes.Final:
					return offeredCourse.RegisteredCourses.Select(rc => (IStudentMarks)new StudentMarks(rc, 0, rc.FinalTotal ?? throw new InvalidOperationException($"{examMarksTypeEnum} Total Marks can not be null."), rc.Final)).ToList();
				default:
					throw new NotImplementedEnumException(examMarksTypeEnum);
			}
		}

		public static IReadOnlyList<IStudentMarks> GetMarksList(IOfferedCourseExam offeredCourseExam)
		{
			switch (offeredCourseExam.ExamTypeEnum)
			{
				case Model.Entities.OfferedCourseExam.ExamTypes.Assignment:
					return offeredCourseExam.OfferedCourse.RegisteredCourses.Select(rc => (IStudentMarks)new StudentMarks(rc, 0, offeredCourseExam.TotalMarks, rc.OfferedCourseExamAssignments.Single(ocea => ocea.OfferedCourseExam == offeredCourseExam).RegisteredCourseExamMark?.ExamMarks)).ToList();
				case Model.Entities.OfferedCourseExam.ExamTypes.Quiz:
					return offeredCourseExam.OfferedCourse.RegisteredCourses.Select(rc => (IStudentMarks)new StudentMarks(rc, 0, offeredCourseExam.TotalMarks, rc.OfferedCourseExamQuizzes.Single(ocea => ocea.OfferedCourseExam == offeredCourseExam).RegisteredCourseExamMark?.ExamMarks)).ToList();
				default:
					throw new NotImplementedEnumException(offeredCourseExam.ExamTypeEnum);
			}
		}

		public static IEnumerable<string> ExamMarksPolicyDoesNotSupport(Model.Entities.OfferedCourseExam.ExamTypes examTypeEnum)
		{
			yield return $"Assigned Exam Marks Policy does not support {examTypeEnum.ToFullName()} Marks.";
		}

		public static IEnumerable<string> ExamMarksPolicyDoesNotSupport(ExamMarksTypes examMarksTypeEnum)
		{
			yield return $"Assigned Exam Marks Policy does not support {examMarksTypeEnum.ToFullName()} Marks.";
		}

		public static IEnumerable<string> ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes compileExamMarksTypeEnum)
		{
			yield return $"Assigned Exam Marks Policy does not support compilation of {compileExamMarksTypeEnum.ToFullName()}.";
		}

		public static decimal? GetCompiledMarksPercentage(IRegisteredCourse registeredCourse, CompileExamMarksTypes compileExamMarksType, OfferedCours.ExamCompilationTypes examCompilationType, decimal totalMarks)
		{
			IReadOnlyList<IExamMarks> offeredCourseExams;
			switch (compileExamMarksType)
			{
				case CompileExamMarksTypes.Assignments:
					offeredCourseExams = registeredCourse.OfferedCourseExamAssignments;
					break;
				case CompileExamMarksTypes.Quizzes:
					offeredCourseExams = registeredCourse.OfferedCourseExamQuizzes;
					break;
				case CompileExamMarksTypes.Internals:
					offeredCourseExams = registeredCourse.OfferedCourseExams;
					break;
				default:
					throw new NotImplementedEnumException(compileExamMarksType);
			}
			switch (examCompilationType)
			{
				case OfferedCours.ExamCompilationTypes.OverallAverage:
					if (!offeredCourseExams.Any())
						return null;
					var total = offeredCourseExams.Sum(oce => oce.OfferedCourseExam.TotalMarks);
					if (total == 0)
						return 0;
					return offeredCourseExams.Sum(oce => (oce.RegisteredCourseExamMark?.ExamMarks) ?? 0) * totalMarks / total;
				default:
					throw new NotImplementedEnumException(examCompilationType);
			}
		}
	}
}
