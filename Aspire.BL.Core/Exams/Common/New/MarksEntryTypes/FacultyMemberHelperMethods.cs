﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.New.MarksEntryTypes
{
	internal static class FacultyMemberHelperMethods
	{
		public static ExamMarksTypes? GetExamMarksSettings(AspireContext aspireContext, IFacultyMemberMarksSheet facultyMemberMarksSheet)
		{
			var today = DateTime.Today;
			var examMarksSettings = aspireContext.ExamMarksSettings.Where(ems => ems.ProgramID == facultyMemberMarksSheet.OfferedCourse.ProgramID && ems.SemesterID == facultyMemberMarksSheet.OfferedCourse.OfferedSemesterID)
				.Where(ems => ems.Status == (byte)ExamMarksSetting.Statuses.Active)
				.Where(ems => ems.FromDate <= today && today <= ems.ToDate)
				.Select(ems => new
				{
					ExamMarksTypeEnum = (ExamMarksTypes)ems.ExamMarksTypes
				})
				.SingleOrDefault();
			return examMarksSettings?.ExamMarksTypeEnum;
		}

		public static IEnumerable<string> CanNotAddAssignmentQuizReasons(IFacultyMemberMarksSheet facultyMemberMarksSheet, Model.Entities.OfferedCourseExam.ExamTypes examTypeEnum, ExamMarksTypes examMarksTypeEnum)
		{
			var offeredCourse = facultyMemberMarksSheet.OfferedCourse;
			if (offeredCourse.SubmittedToUniversityDate != null)
				yield return $"{examTypeEnum.ToFullName()} Marks cannot be added because Marks have been submitted to BUHO Exams.";
			if (offeredCourse.SubmittedToCampusDate != null)
				yield return $"{examTypeEnum.ToFullName()} Marks cannot be added because Marks have been submitted to Campus.";
			if (offeredCourse.SubmittedToHODDate != null)
				yield return $"{examTypeEnum.ToFullName()} Marks cannot be added because Marks have been submitted to HOD.";
			if (offeredCourse.MarksEntryLocked)
				yield return $"{examTypeEnum.ToFullName()} Marks cannot be added because Marks Entry has been locked.";

			switch (examMarksTypeEnum)
			{
				case ExamMarksTypes.Assignments:
					if (offeredCourse.AssignmentsMarksSubmitted)
						yield return $"{examTypeEnum.ToFullName()} Marks cannot be added because {examMarksTypeEnum.ToFullName()} Marks have been submitted.";
					break;
				case ExamMarksTypes.Quizzes:
					if (offeredCourse.QuizzesMarksSubmitted)
						yield return $"{examTypeEnum.ToFullName()} Marks cannot be added because {examMarksTypeEnum.ToFullName()} Marks have been submitted.";
					break;
				case ExamMarksTypes.Internals:
					if (offeredCourse.InternalsMarksSubmitted)
						yield return $"{examTypeEnum.ToFullName()} Marks cannot be added because {examMarksTypeEnum.ToFullName()} Marks have been submitted.";
					break;
				case ExamMarksTypes.Mid:
				case ExamMarksTypes.Final:
					throw new InvalidOperationException();
				default:
					throw new NotImplementedEnumException(examMarksTypeEnum);
			}

			if (facultyMemberMarksSheet.ExamMarksTypeEnum?.HasFlag(examMarksTypeEnum) != true)
				yield return $"{examTypeEnum.ToFullName()} Marks cannot be added because {examMarksTypeEnum.ToFullName()} Marks Entry is not open now. Contact Exam Section.";
		}

		public static IEnumerable<string> CanNotEditOrDeleteAssignmentOrQuizReasons(int offeredCourseExamID, IFacultyMemberMarksSheet facultyMemberMarksSheet, Model.Entities.OfferedCourseExam.ExamTypes examTypeEnum, ExamMarksTypes examMarksTypeEnum)
		{
			var offeredCourse = facultyMemberMarksSheet.OfferedCourse;
			if (offeredCourse.SubmittedToUniversityDate != null)
				yield return $"{examTypeEnum.ToFullName()} Marks cannot be edited/deleted because Marks have been submitted to BUHO Exams.";
			if (offeredCourse.SubmittedToCampusDate != null)
				yield return $"{examTypeEnum.ToFullName()} Marks cannot be edited/deleted because Marks have been submitted to Campus.";
			if (offeredCourse.SubmittedToHODDate != null)
				yield return $"{examTypeEnum.ToFullName()} Marks cannot be edited/deleted because Marks have been submitted to HOD.";
			if (offeredCourse.MarksEntryLocked)
				yield return $"{examTypeEnum.ToFullName()} Marks cannot be edited/deleted because Marks Entry has been locked.";
			switch (examMarksTypeEnum)
			{
				case ExamMarksTypes.Assignments:
					if (offeredCourse.AssignmentsMarksSubmitted)
						yield return $"{examTypeEnum.ToFullName()} Marks cannot be edited/deleted because {examMarksTypeEnum.ToFullName()} Marks have been submitted.";
					break;
				case ExamMarksTypes.Quizzes:
					if (offeredCourse.QuizzesMarksSubmitted)
						yield return $"{examTypeEnum.ToFullName()} Marks cannot be edited/deleted because {examMarksTypeEnum.ToFullName()} Marks have been submitted.";
					break;
				case ExamMarksTypes.Internals:
					if (offeredCourse.InternalsMarksSubmitted)
						yield return $"{examTypeEnum.ToFullName()} Marks cannot be edited/deleted because {examMarksTypeEnum.ToFullName()} Marks have been submitted.";
					break;
				case ExamMarksTypes.Mid:
				case ExamMarksTypes.Final:
					throw new InvalidOperationException();
				default:
					throw new NotImplementedEnumException(examMarksTypeEnum);
			}
			var offeredCourseExam = offeredCourse.OfferedCourseExams.Single(ocea => ocea.OfferedCourseExamID == offeredCourseExamID && ocea.ExamTypeEnum == examTypeEnum);
			if (offeredCourseExam.Locked)
				yield return $"{examTypeEnum.ToFullName()} Marks cannot be edited/deleted because {examTypeEnum.ToFullName()} Marks have been submitted.";

			if (facultyMemberMarksSheet.ExamMarksTypeEnum?.HasFlag(examMarksTypeEnum) != true)
				yield return $"{examTypeEnum.ToFullName()} Marks cannot be edited/deleted because {examMarksTypeEnum.ToFullName()} Marks Entry is not open now. Contact Exam Section.";
		}

		public static IEnumerable<string> CanNotCompileAssignmentsQuizzesInternalsReasons(IFacultyMemberMarksSheet facultyMemberMarksSheet, ExamMarksTypes examMarksTypeEnum)
		{
			var offeredCourse = facultyMemberMarksSheet.OfferedCourse;
			if (offeredCourse.SubmittedToUniversityDate != null)
				yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be compiled because Marks have been submitted to BUHO Exams.";
			if (offeredCourse.SubmittedToCampusDate != null)
				yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be compiled because Marks have been submitted to Campus.";
			if (offeredCourse.SubmittedToHODDate != null)
				yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be compiled because Marks have been submitted to HOD.";
			if (offeredCourse.MarksEntryLocked)
				yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be compiled because Marks Entry has been locked.";
			switch (examMarksTypeEnum)
			{
				case ExamMarksTypes.Assignments:
					if (offeredCourse.AssignmentsMarksSubmitted)
						yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be compiled because {examMarksTypeEnum.ToFullName()} Marks have been submitted.";
					if (!offeredCourse.OfferedCourseExamAssignments.Any())
						yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be compiled because no {Model.Entities.OfferedCourseExam.ExamTypes.Assignment.ToFullName()} has been added yet.";
					break;
				case ExamMarksTypes.Quizzes:
					if (offeredCourse.QuizzesMarksSubmitted)
						yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be compiled because {examMarksTypeEnum.ToFullName()} Marks have been submitted.";
					if (!offeredCourse.OfferedCourseExamQuizzes.Any())
						yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be compiled because no {Model.Entities.OfferedCourseExam.ExamTypes.Quiz.ToFullName()} has been added yet.";
					break;
				case ExamMarksTypes.Internals:
					if (offeredCourse.InternalsMarksSubmitted)
						yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be compiled because {examMarksTypeEnum.ToFullName()} Marks have been submitted.";
					if (!offeredCourse.OfferedCourseExams.Any(oce => oce.ExamTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Assignment || oce.ExamTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Quiz))
						yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be compiled because no {Model.Entities.OfferedCourseExam.ExamTypes.Assignment.ToFullName()}/{Model.Entities.OfferedCourseExam.ExamTypes.Quiz.ToFullName()} has been added yet.";
					break;
				case ExamMarksTypes.Mid:
				case ExamMarksTypes.Final:
					throw new InvalidOperationException();
				default:
					throw new NotImplementedEnumException(examMarksTypeEnum);
			}
			if (facultyMemberMarksSheet.ExamMarksTypeEnum?.HasFlag(examMarksTypeEnum) != true && facultyMemberMarksSheet.ExamMarksTypeEnum?.HasFlag(ExamMarksTypes.Final) != true)
				yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be compiled because {examMarksTypeEnum.ToFullName()} Marks Entry is not open now. Contact Exam Section.";
		}

		public static IEnumerable<string> CanNotEditAssignmentsQuizzesMidFinalReasons(IFacultyMemberMarksSheet facultyMemberMarksSheet, ExamMarksTypes examMarksTypeEnum)
		{
			var offeredCourse = facultyMemberMarksSheet.OfferedCourse;
			if (offeredCourse.SubmittedToUniversityDate != null)
				yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be edited because Marks have been submitted to BUHO Exams.";
			if (offeredCourse.SubmittedToCampusDate != null)
				yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be edited because Marks have been submitted to Campus.";
			if (offeredCourse.SubmittedToHODDate != null)
				yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be edited because Marks have been submitted to HOD.";
			if (offeredCourse.MarksEntryLocked)
				yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be edited because Marks Entry has been locked.";
			switch (examMarksTypeEnum)
			{
				case ExamMarksTypes.Assignments:
					if (offeredCourse.AssignmentsMarksSubmitted)
						yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be edited because {examMarksTypeEnum.ToFullName()} Marks have been submitted.";
					break;
				case ExamMarksTypes.Quizzes:
					if (offeredCourse.QuizzesMarksSubmitted)
						yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be edited because {examMarksTypeEnum.ToFullName()} Marks have been submitted.";
					break;
				case ExamMarksTypes.Internals:
					throw new InvalidOperationException();
				case ExamMarksTypes.Mid:
					if (offeredCourse.MidMarksSubmitted)
						yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be edited because {examMarksTypeEnum.ToFullName()} Marks have been submitted.";
					break;
				case ExamMarksTypes.Final:
					if (offeredCourse.FinalMarksSubmitted)
						yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be edited because {examMarksTypeEnum.ToFullName()} Marks have been submitted.";
					break;
				default:
					throw new NotImplementedEnumException(examMarksTypeEnum);
			}
			if (facultyMemberMarksSheet.ExamMarksTypeEnum?.HasFlag(examMarksTypeEnum) != true)
				yield return $"{examMarksTypeEnum.ToFullName()} Marks cannot be edited because {examMarksTypeEnum.ToFullName()} Marks Entry is not open now. Contact Exam Section.";
		}

		public static IEnumerable<string> CanNotCompileGradesReasons(IFacultyMemberMarksSheet facultyMemberMarksSheet)
		{
			var offeredCourse = facultyMemberMarksSheet.OfferedCourse;
			if (offeredCourse.SubmittedToUniversityDate != null)
				yield return "Total/Grades cannot be compiled because Marks have been submitted to BUHO Exams.";
			if (offeredCourse.SubmittedToCampusDate != null)
				yield return "Total/Grades cannot be compiled because Marks have been submitted to Campus.";
			if (offeredCourse.SubmittedToHODDate != null)
				yield return "Total/Grades cannot be compiled because Marks have been submitted to HOD.";
			if (offeredCourse.MarksEntryLocked)
				yield return "Total/Grades cannot be compiled because Marks Entry has been locked.";
			foreach (var pendingMarksEntry in offeredCourse.MarksSheet.PendingMarksEntry)
				yield return pendingMarksEntry;
			if (facultyMemberMarksSheet.ExamMarksTypeEnum?.HasFlag(ExamMarksTypes.Final) != true)
				yield return $"Total/Grades cannot be compiled because {ExamMarksTypes.Final.ToFullName()} Marks Entry is not open now. Contact Exam Section.";
		}

		public static IEnumerable<string> CanNotLockMarksEntryReasons(IFacultyMemberMarksSheet facultyMemberMarksSheet)
		{
			var offeredCourse = facultyMemberMarksSheet.OfferedCourse;
			if (offeredCourse.MarksEntryLocked)
				yield return "Marks Entry is already locked.";
			else
				yield return "Faculty Member is not authorized to Lock Marks Entry.";
		}

		public static IEnumerable<string> CanNotSubmitToHODReasons(IFacultyMemberMarksSheet facultyMemberMarksSheet)
		{
			var offeredCourse = facultyMemberMarksSheet.OfferedCourse;
			if (offeredCourse.SubmittedToHODDate != null)
				yield return "Marks are already submitted to HOD.";
			else
			{
				if (offeredCourse.SubmittedToUniversityDate != null)
					yield return "Marks cannot be submitted to HOD because Marks have been submitted to BUHO Exams.";
				if (offeredCourse.SubmittedToCampusDate != null)
					yield return "Marks cannot be submitted to HOD because Marks have been submitted to Campus.";
				if (offeredCourse.MarksEntryCompleted == false)
					yield return "Marks cannot be submitted to HOD because Total/Grades are not yet compiled.";
				if (facultyMemberMarksSheet.ExamMarksTypeEnum?.HasFlag(ExamMarksTypes.Final) != true)
					yield return $"Marks cannot be submitted to HOD because {ExamMarksTypes.Final.ToFullName()} Marks Entry is not open now. Contact Exam Section.";
			}
		}

		public static IEnumerable<string> CanNotUnlockMarksReasons
		{
			get
			{
				yield return "Faculty Member is not authorized for unlocking Marks Entry.";
			}
		}

		public static IEnumerable<string> CanNotSubmitToCampusReasons
		{
			get
			{
				yield return "Faculty Member is not authorized for submitting Marks to Campus.";
			}
		}

		public static IEnumerable<string> CanNotSubmitToUniversityReasons
		{
			get
			{
				yield return "Faculty Member is not authorized for submitting Marks to BUHO Exams.";
			}
		}

		public static IEnumerable<string> CanNotRevertSubmitToHODReasons
		{
			get
			{
				yield return "Faculty Member is not authorized for reverting Submission To HOD.";
			}
		}

		public static IEnumerable<string> CanNotRevertSubmitToCampusReasons
		{
			get
			{
				yield return "Faculty Member is not authorized for reverting Submission To Campus.";
			}
		}

		public static IEnumerable<string> CanNotRevertSubmitToUniversityReasons
		{
			get
			{
				yield return "Faculty Member is not authorized for reverting Submission to BUHO Exams.";
			}
		}
	}
}
