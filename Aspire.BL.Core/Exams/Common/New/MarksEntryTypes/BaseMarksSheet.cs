﻿using Aspire.BL.Core.Exams.Common.New.Converters;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.New.MarksEntryTypes
{
	[JsonObject(MemberSerialization.OptIn)]
	internal abstract class BaseMarksSheet : IMarksSheet, IAspireContext
	{
		protected BaseMarksSheet(AspireContext aspireContext, int offeredCourseID, int userLoginHistoryID, Guid loginSessionGuid)
		{
			this.OfferedCourse = new OfferedCourse(this, aspireContext, offeredCourseID);
			this.UserLoginHistoryID = userLoginHistoryID;
			this.LoginSessionGuid = loginSessionGuid;
			this.AspireContext = aspireContext;
		}

		public AspireContext AspireContext { get; }

		protected MarksSheetChanx.ManagementEmailRecipients? ManagementEmailRecipients
		{
			get
			{
				if (this.OfferedCourse.SubmittedToUniversityDate != null)
					return MarksSheetChanx.ManagementEmailRecipients.AfterSubmitToUniversity;
				if (this.OfferedCourse.SubmittedToCampusDate != null)
					return MarksSheetChanx.ManagementEmailRecipients.BeforeSubmitToUniversity;
				if (this.OfferedCourse.SubmittedToHODDate != null)
					return MarksSheetChanx.ManagementEmailRecipients.BeforeSubmitToUniversity;
				return null;
			}
		}

		public abstract string ReloadAndToJson(out MarksSheetChanx.ManagementEmailRecipients? managementEmailRecipients);

		[JsonProperty]
		public IOfferedCourse OfferedCourse { get; }

		public abstract IDisplay Display { get; }

		public abstract IOptions Options { get; }

		public IMarksSubmissionFlow MarksSubmissionFlow => this;

		public int UserLoginHistoryID { get; }

		public Guid LoginSessionGuid { get; }

		public bool IsFacultyMember => this is IFacultyMemberMarksSheet;

		public bool IsStaff => this is IStaffMarksSheet;

		[JsonProperty("Marks Entry Type Enum")]
		[JsonConverter(typeof(EnumFullNameConverter))]
		public abstract Model.Entities.ExamMarksPolicy.MarksEntryTypes MarksEntryTypeEnum { get; }

		#region IAssignment

		public abstract IEnumerable<string> CanNotAddAssignmentReasons { get; }

		public bool CanAddAssignment => !this.CanNotAddAssignmentReasons.Any();

		public abstract IEnumerable<string> CanNotEditOrDeleteAssignmentReasons(int offeredCourseExamID);

		public bool CanEditOrDeleteAssignment(int offeredCourseExamID)
		{
			return !this.CanNotEditOrDeleteAssignmentReasons(offeredCourseExamID).Any();
		}

		public abstract IEnumerable<string> CanNotUnlockAssignmentReasons(int offeredCourseExamID);

		public bool CanUnlockAssignment(int offeredCourseExamID)
		{
			return !this.CanNotUnlockAssignmentReasons(offeredCourseExamID).Any();
		}

		public abstract IReadOnlyList<IStudentMarks> GetAssignmentMarks(int offeredCourseExamID);

		public (IEnumerable<string> addFailureReasons, int? offeredCourseExamID) AddAssignment(DateTime examDate, byte examNo, decimal totalMarks, string remarks)
		{
			var examTypeEnum = Model.Entities.OfferedCourseExam.ExamTypes.Assignment;
			if (!this.CanAddAssignment)
				return (this.CanNotAddAssignmentReasons, null);
			return MarksEntryHelper.AddOfferedCourseExam(this, examTypeEnum, examDate, examNo, totalMarks, remarks);
		}

		public (IEnumerable<string> deleteFailureReasons, bool success) DeleteAssignment(int offeredCourseExamID)
		{
			var examTypeEnum = Model.Entities.OfferedCourseExam.ExamTypes.Assignment;
			if (this.OfferedCourse.OfferedCourseExamAssignments.All(oce => oce.OfferedCourseExamID != offeredCourseExamID))
				return (new[] { $"No {examTypeEnum.ToFullName()} record found." }, false);
			if (!this.CanEditOrDeleteAssignment(offeredCourseExamID))
				return (this.CanNotEditOrDeleteAssignmentReasons(offeredCourseExamID), false);
			return MarksEntryHelper.DeleteOfferedCourseExam(this, examTypeEnum, offeredCourseExamID);
		}

		public (IEnumerable<string> updateFailureReasons, bool success) UpdateAssignment(int offeredCourseExamID, DateTime examDate, byte examNo, string remarks, IReadOnlyDictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)> marks, bool submit)
		{
			var examTypeEnum = Model.Entities.OfferedCourseExam.ExamTypes.Assignment;
			if (this.OfferedCourse.OfferedCourseExamAssignments.All(oce => oce.OfferedCourseExamID != offeredCourseExamID))
				return (new[] { $"No {examTypeEnum.ToFullName()} record found." }, false);
			if (!this.CanEditOrDeleteAssignment(offeredCourseExamID))
				return (this.CanNotEditOrDeleteAssignmentReasons(offeredCourseExamID), false);
			return MarksEntryHelper.UpdateOfferedCourseExam(this, examTypeEnum, offeredCourseExamID, examDate, examNo, remarks, marks, submit);
		}

		public (IEnumerable<string> unlockFailureReasons, bool success) UnlockAssignment(int offeredCourseExamID)
		{
			var examTypeEnum = Model.Entities.OfferedCourseExam.ExamTypes.Assignment;
			if (this.OfferedCourse.OfferedCourseExamAssignments.All(oce => oce.OfferedCourseExamID != offeredCourseExamID))
				return (new[] { $"No {examTypeEnum.ToFullName()} record found." }, false);
			if (!this.CanUnlockAssignment(offeredCourseExamID))
				return (this.CanNotUnlockAssignmentReasons(offeredCourseExamID), false);
			return MarksEntryHelper.UnlockOfferedCourseExam(this, examTypeEnum, offeredCourseExamID);
		}

		#endregion

		#region IAssignments

		public abstract IEnumerable<string> CanNotEditAssignmentsReasons { get; }

		public bool CanEditAssignments => !this.CanNotEditAssignmentsReasons.Any();

		public abstract IEnumerable<string> CanNotUnlockAssignmentsReasons { get; }

		public bool CanUnlockAssignments => !this.CanNotUnlockAssignmentsReasons.Any();

		public abstract IEnumerable<string> CanNotCompileAssignmentsReasons { get; }

		public bool CanCompileAssignments => !this.CanNotCompileAssignmentsReasons.Any();

		public bool IsAssignmentsLocked => this.OfferedCourse.AssignmentsMarksSubmitted;

		public abstract IReadOnlyList<IStudentMarks> GetAssignmentsMarks();

		public abstract decimal? GetCompiledAssignmentsMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType);

		public (IEnumerable<string> updateFailureReasons, bool success) UpdateAssignments(IReadOnlyDictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)> marks, bool submit)
		{
			var examMarksTypeEnum = ExamMarksTypes.Assignments;
			if (!this.CanEditAssignments)
				return (this.CanNotEditAssignmentsReasons, false);
			return MarksEntryHelper.UpdateExamMarks(this, examMarksTypeEnum, marks, submit);
		}

		public (IEnumerable<string> compileFailureReasons, bool success) CompileAssignments(OfferedCours.ExamCompilationTypes examCompilationTypeEnum, bool submit)
		{
			var compileExamMarksTypeEnum = CompileExamMarksTypes.Assignments;
			if (!this.CanCompileAssignments)
				return (this.CanNotCompileAssignmentsReasons, false);
			return MarksEntryHelper.CompileExamMarks(this, compileExamMarksTypeEnum, examCompilationTypeEnum, submit);
		}

		public (IEnumerable<string> unlockFailureReasons, bool success) UnlockAssignments()
		{
			var examMarksTypeEnum = ExamMarksTypes.Assignments;
			if (!this.CanUnlockAssignments)
				return (this.CanNotUnlockAssignmentsReasons, false);
			return MarksEntryHelper.UnlockExamMarks(this, examMarksTypeEnum);
		}

		#endregion

		#region IQuiz

		public abstract IEnumerable<string> CanNotAddQuizReasons { get; }

		public bool CanAddQuiz => !this.CanNotAddQuizReasons.Any();

		public abstract IEnumerable<string> CanNotEditOrDeleteQuizReasons(int offeredCourseExamID);

		public bool CanEditOrDeleteQuiz(int offeredCourseExamID)
		{
			return !this.CanNotEditOrDeleteQuizReasons(offeredCourseExamID).Any();
		}

		public abstract IEnumerable<string> CanNotUnlockQuizReasons(int offeredCourseExamID);

		public bool CanUnlockQuiz(int offeredCourseExamID)
		{
			return !this.CanNotUnlockQuizReasons(offeredCourseExamID).Any();
		}

		public abstract IReadOnlyList<IStudentMarks> GetQuizMarks(int offeredCourseExamID);

		public (IEnumerable<string> addFailureReasons, int? offeredCourseExamID) AddQuiz(DateTime examDate, byte examNo, decimal totalMarks, string remarks)
		{
			var examTypeEnum = Model.Entities.OfferedCourseExam.ExamTypes.Quiz;
			if (!this.CanAddQuiz)
				return (this.CanNotAddQuizReasons, null);
			return MarksEntryHelper.AddOfferedCourseExam(this, examTypeEnum, examDate, examNo, totalMarks, remarks);
		}

		public (IEnumerable<string> deleteFailureReasons, bool success) DeleteQuiz(int offeredCourseExamID)
		{
			var examTypeEnum = Model.Entities.OfferedCourseExam.ExamTypes.Quiz;
			if (this.OfferedCourse.OfferedCourseExamQuizzes.All(oce => oce.OfferedCourseExamID != offeredCourseExamID))
				return (new[] { $"No {examTypeEnum.ToFullName()} record found." }, false);
			if (!this.CanEditOrDeleteQuiz(offeredCourseExamID))
				return (this.CanNotEditOrDeleteQuizReasons(offeredCourseExamID), false);
			return MarksEntryHelper.DeleteOfferedCourseExam(this, examTypeEnum, offeredCourseExamID);
		}

		public (IEnumerable<string> updateFailureReasons, bool success) UpdateQuiz(int offeredCourseExamID, DateTime examDate, byte examNo, string remarks, IReadOnlyDictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)> marks, bool submit)
		{
			var examTypeEnum = Model.Entities.OfferedCourseExam.ExamTypes.Quiz;
			if (this.OfferedCourse.OfferedCourseExamQuizzes.All(oce => oce.OfferedCourseExamID != offeredCourseExamID))
				return (new[] { $"No {examTypeEnum.ToFullName()} record found." }, false);
			if (!this.CanEditOrDeleteQuiz(offeredCourseExamID))
				return (this.CanNotEditOrDeleteQuizReasons(offeredCourseExamID), false);
			return MarksEntryHelper.UpdateOfferedCourseExam(this, examTypeEnum, offeredCourseExamID, examDate, examNo, remarks, marks, submit);
		}

		public (IEnumerable<string> unlockFailureReasons, bool success) UnlockQuiz(int offeredCourseExamID)
		{
			var examTypeEnum = Model.Entities.OfferedCourseExam.ExamTypes.Quiz;
			if (this.OfferedCourse.OfferedCourseExamQuizzes.All(oce => oce.OfferedCourseExamID != offeredCourseExamID))
				return (new[] { $"No {examTypeEnum.ToFullName()} record found." }, false);
			if (!this.CanUnlockQuiz(offeredCourseExamID))
				return (this.CanNotUnlockQuizReasons(offeredCourseExamID), false);
			return MarksEntryHelper.UnlockOfferedCourseExam(this, examTypeEnum, offeredCourseExamID);
		}

		#endregion

		#region IQuizzes

		public abstract IEnumerable<string> CanNotEditQuizzesReasons { get; }

		public bool CanEditQuizzes => !this.CanNotEditQuizzesReasons.Any();

		public abstract IEnumerable<string> CanNotUnlockQuizzesReasons { get; }

		public bool CanUnlockQuizzes => !this.CanNotUnlockQuizzesReasons.Any();

		public abstract IEnumerable<string> CanNotCompileQuizzesReasons { get; }

		public bool CanCompileQuizzes => !this.CanNotCompileQuizzesReasons.Any();

		public bool IsQuizzesLocked => this.OfferedCourse.QuizzesMarksSubmitted;

		public abstract IReadOnlyList<IStudentMarks> GetQuizzesMarks();

		public abstract decimal? GetCompiledQuizzesMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType);

		public (IEnumerable<string> updateFailureReasons, bool success) UpdateQuizzes(IReadOnlyDictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)> marks, bool submit)
		{
			var examMarksTypeEnum = ExamMarksTypes.Quizzes;
			if (!this.CanEditQuizzes)
				return (this.CanNotEditQuizzesReasons, false);
			return MarksEntryHelper.UpdateExamMarks(this, examMarksTypeEnum, marks, submit);
		}

		public (IEnumerable<string> compileFailureReasons, bool success) CompileQuizzes(OfferedCours.ExamCompilationTypes examCompilationTypeEnum, bool submit)
		{
			var compileExamMarksTypeEnum = CompileExamMarksTypes.Quizzes;
			if (!this.CanCompileQuizzes)
				return (this.CanNotCompileQuizzesReasons, false);
			return MarksEntryHelper.CompileExamMarks(this, compileExamMarksTypeEnum, examCompilationTypeEnum, submit);
		}

		public (IEnumerable<string> unlockFailureReasons, bool success) UnlockQuizzes()
		{
			var examMarksTypeEnum = ExamMarksTypes.Quizzes;
			if (!this.CanUnlockQuizzes)
				return (this.CanNotUnlockQuizzesReasons, false);
			return MarksEntryHelper.UnlockExamMarks(this, examMarksTypeEnum);
		}

		#endregion

		#region IInternals

		public abstract IEnumerable<string> CanNotUnlockInternalsReasons { get; }

		public bool CanUnlockInternals => !this.CanNotUnlockInternalsReasons.Any();

		public abstract IEnumerable<string> CanNotCompileInternalsReasons { get; }

		public bool CanCompileInternals => !this.CanNotCompileInternalsReasons.Any();

		public bool IsInternalsLocked => this.OfferedCourse.InternalsMarksSubmitted;

		public abstract decimal? GetCompiledInternalsMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType);

		public (IEnumerable<string> compileFailureReasons, bool success) CompileInternals(OfferedCours.ExamCompilationTypes examCompilationTypeEnum, bool submit)
		{
			var compileExamMarksTypeEnum = CompileExamMarksTypes.Internals;
			if (!this.CanCompileInternals)
				return (this.CanNotCompileInternalsReasons, false);
			return MarksEntryHelper.CompileExamMarks(this, compileExamMarksTypeEnum, examCompilationTypeEnum, submit);
		}

		public (IEnumerable<string> unlockFailureReasons, bool success) UnlockInternals()
		{
			var examMarksTypeEnum = ExamMarksTypes.Internals;
			if (!this.CanUnlockInternals)
				return (this.CanNotUnlockInternalsReasons, false);
			return MarksEntryHelper.UnlockExamMarks(this, examMarksTypeEnum);
		}

		#endregion

		#region IMid

		public abstract IEnumerable<string> CanNotEditMidReasons { get; }

		public bool CanEditMid => !this.CanNotEditMidReasons.Any();

		public abstract IEnumerable<string> CanNotUnlockMidReasons { get; }

		public bool CanUnlockMid => !this.CanNotUnlockMidReasons.Any();

		public bool IsMidLocked => this.OfferedCourse.MidMarksSubmitted;

		public abstract IReadOnlyList<IStudentMarks> GetMidMarks();

		public (IEnumerable<string> updateFailureReasons, bool success) UpdateMid(IReadOnlyDictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)> marks, bool submit)
		{
			var examMarksTypeEnum = ExamMarksTypes.Mid;
			if (!this.CanEditMid)
				return (this.CanNotEditMidReasons, false);
			return MarksEntryHelper.UpdateExamMarks(this, examMarksTypeEnum, marks, submit);
		}

		public (IEnumerable<string> unlockFailureReasons, bool success) UnlockMid()
		{
			var examMarksTypeEnum = ExamMarksTypes.Mid;
			if (!this.CanUnlockMid)
				return (this.CanNotUnlockMidReasons, false);
			return MarksEntryHelper.UnlockExamMarks(this, examMarksTypeEnum);
		}

		#endregion

		#region IFinal

		public abstract IEnumerable<string> CanNotEditFinalReasons { get; }

		public bool CanEditFinal => !this.CanNotEditFinalReasons.Any();

		public abstract IEnumerable<string> CanNotUnlockFinalReasons { get; }

		public bool CanUnlockFinal => !this.CanNotUnlockFinalReasons.Any();

		public bool IsFinalLocked => this.OfferedCourse.FinalMarksSubmitted;

		public IReadOnlyList<IStudentMarks> GetFinalMarks()
		{
			return MarksSheetHelperMethods.GetMarksList(this.OfferedCourse, ExamMarksTypes.Final);
		}

		public (IEnumerable<string> updateFailureReasons, bool success) UpdateFinal(IReadOnlyDictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)> marks, bool submit)
		{
			var examMarksTypeEnum = ExamMarksTypes.Final;
			if (!this.CanEditFinal)
				return (this.CanNotEditFinalReasons, false);
			return MarksEntryHelper.UpdateExamMarks(this, examMarksTypeEnum, marks, submit);
		}

		public (IEnumerable<string> unlockFailureReasons, bool success) UnlockFinal()
		{
			var examMarksTypeEnum = ExamMarksTypes.Final;
			if (!this.CanUnlockFinal)
				return (this.CanNotUnlockFinalReasons, false);
			return MarksEntryHelper.UnlockExamMarks(this, examMarksTypeEnum);
		}

		#endregion

		#region IMarksSubmissionFlow

		public abstract IEnumerable<string> PendingMarksEntry { get; }

		public abstract IEnumerable<string> CanNotCompileGradesReasons { get; }

		public bool CanCompileGrades => !this.CanNotCompileGradesReasons.Any();

		public abstract void SetTotalsAndGrades(IRegisteredCourse iRegisteredCourse, RegisteredCours registeredCourse);

		public (IEnumerable<string> compileFailureReasons, bool success) CompileTotalAndGrades()
		{
			if (!this.CanCompileGrades)
				return (this.CanNotCompileGradesReasons, false);
			return MarksEntryHelper.CompileTotalAndGrades(this);
		}

		public (IEnumerable<string> lockFailureReasons, bool success) LockMarksEntry()
		{
			if (!this.CanLockMarksEntry)
				return (this.CanNotLockMarksEntryReasons, false);
			return MarksEntryHelper.LockMarksEntry(this);
		}

		public (IEnumerable<string> unlockFailureReasons, bool success) UnlockMarksEntry()
		{
			if (!this.CanUnlockMarksEntry)
				return (this.CanNotUnlockMarksEntryReasons, false);
			return MarksEntryHelper.UnlockMarksEntry(this);
		}

		public abstract IEnumerable<string> CanNotLockMarksEntryReasons { get; }

		public bool CanLockMarksEntry => !this.CanNotLockMarksEntryReasons.Any();

		public abstract IEnumerable<string> CanNotUnlockMarksEntryReasons { get; }

		public bool CanUnlockMarksEntry => !this.CanNotUnlockMarksEntryReasons.Any();

		public IEnumerable<string> CanNotUnlockMarksEntryForRegisteredCourseReasons(IRegisteredCourse registeredCourse)
		{
			if (!registeredCourse.MarksEntryLocked)
				return "Registered Course Marks are already unlocked.".ToEnumerable();
			if (this.IsFacultyMember)
				return "Faculty Members are not authorized to Unlock Marks Entry.".ToEnumerable();

			if (this.IsStaff)
			{
				var staffMarksSheet = (IStaffMarksSheet)this;
				if (staffMarksSheet.OfferedCourse.SubmittedToUniversityDate != null)
					return "Registered Course Marks cannot be unlocked because Marks have been submitted to BUHO Exams.".ToEnumerable();
				if (staffMarksSheet.OfferedCourse.SubmittedToCampusDate != null)
				{
					if (staffMarksSheet.CanUnlockCampusLevelMarksEntry)
						return Enumerable.Empty<string>();
					return "Registered Course Marks cannot be unlock because Marks have been submitted to Campus and you do not have permissions to Unlock this record.".ToEnumerable();
				}
				if (staffMarksSheet.OfferedCourse.SubmittedToHODDate != null)
				{
					if (staffMarksSheet.CanUnlockCampusLevelMarksEntry)
						return Enumerable.Empty<string>();
					return "Registered Course Marks cannot be unlock because Marks have been submitted to HOD and you do not have permissions to Unlock this record.".ToEnumerable();
				}
				return "Registered Course Marks cannot be unlock because Marks are not yet submitted to HOD/Campus.".ToEnumerable();
			}
			throw new InvalidOperationException();
		}

		public (IEnumerable<string> unlockFailureReasons, bool success) UnlockMarksEntryForRegisteredCourse(IRegisteredCourse registeredCourse)
		{
			if (!registeredCourse.CanUnlockMarksEntry)
				return (registeredCourse.CanNotUnlockMarksEntryReasons, false);
			return MarksEntryHelper.UnlockMarksEntryForRegisteredCourse(registeredCourse);
		}

		public abstract IEnumerable<string> CanNotSubmitToHODReasons { get; }

		public bool CanSubmitToHOD => !this.CanNotSubmitToHODReasons.Any();

		public abstract IEnumerable<string> CanNotSubmitToCampusReasons { get; }

		public bool CanSubmitToCampus => !this.CanNotSubmitToCampusReasons.Any();

		public abstract IEnumerable<string> CanNotSubmitToUniversityReasons { get; }

		public bool CanSubmitToUniversity => !this.CanNotSubmitToUniversityReasons.Any();

		public abstract IEnumerable<string> CanNotRevertSubmitToHODReasons { get; }

		public bool CanRevertSubmitToHOD => !this.CanNotRevertSubmitToHODReasons.Any();

		public abstract IEnumerable<string> CanNotRevertSubmitToCampusReasons { get; }

		public bool CanRevertSubmitToCampus => !this.CanNotRevertSubmitToCampusReasons.Any();

		public abstract IEnumerable<string> CanNotRevertSubmitToUniversityReasons { get; }

		public bool CanRevertSubmitToUniversity => !this.CanNotRevertSubmitToUniversityReasons.Any();

		public (IEnumerable<string> submitToHODFailureReasons, bool success) SubmitToHOD()
		{
			if (!this.CanSubmitToHOD)
				return (this.CanNotSubmitToHODReasons, false);
			return MarksEntryHelper.SubmitToHOD(this);
		}

		public (IEnumerable<string> submitToCampusFailureReasons, bool success) SubmitToCampus()
		{
			if (!this.CanSubmitToCampus)
				return (this.CanNotSubmitToCampusReasons, false);
			return MarksEntryHelper.SubmitToCampus(this);
		}

		public (IEnumerable<string> submitToUniversityFailureReasons, bool success) SubmitToUniversity()
		{
			if (!this.CanSubmitToUniversity)
				return (this.CanNotSubmitToUniversityReasons, false);
			return MarksEntryHelper.SubmitToUniversity(this);
		}

		public (IEnumerable<string> revertSubmitToHODFailureReasons, bool success) RevertSubmitToHOD()
		{
			if (!this.CanRevertSubmitToHOD)
				return (this.CanNotRevertSubmitToHODReasons, false);
			return MarksEntryHelper.RevertSubmitToHOD(this);
		}

		public (IEnumerable<string> revertSubmitToCampusFailureReasons, bool success) RevertSubmitToCampus()
		{
			if (!this.CanRevertSubmitToCampus)
				return (this.CanNotRevertSubmitToCampusReasons, false);
			return MarksEntryHelper.RevertSubmitToCampus(this);
		}

		public (IEnumerable<string> revertSubmitToUniversityFailureReasons, bool success) RevertSubmitToUniversity()
		{
			if (!this.CanRevertSubmitToUniversity)
				return (this.CanNotRevertSubmitToUniversityReasons, false);
			return MarksEntryHelper.RevertSubmitToUniversity(this);
		}

		#endregion

		public string ToJson()
		{
			return JsonConvert.SerializeObject(this, Formatting.Indented, new JsonSerializerSettings
			{
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore
			});
		}

		public (List<string> errors, List<string> warnings) Validate()
		{
			var errors = new List<string>();
			var warnings = new List<string>();
			(List<string> errors, List<string> warnings) Error(string errorMessage)
			{
				errors.Add(errorMessage);
				return (errors.Distinct().ToList(), warnings.Distinct().ToList());
			}
			switch (this.OfferedCourse.CourseCategoryEnum)
			{
				case CourseCategories.Course:
				case CourseCategories.Lab:
					break;
				case CourseCategories.Internship:
				case CourseCategories.Project:
				case CourseCategories.Thesis:
					return Error($"{this.OfferedCourse.CourseCategoryEnum.ToFullName()} Not supported. Offered Course: {this.OfferedCourse.OfferedCourseInfo}");
				default:
					throw new NotImplementedEnumException(this.OfferedCourse.CourseCategoryEnum);
			}
			if (!this.OfferedCourse.RegisteredCourses.Any())
				return Error($"No student registration found. Offered Course: {this.OfferedCourse.OfferedCourseInfo}.");
			if (this.OfferedCourse.ExamMarksPolicy == null)
				return Error($"Exam Marks Policy is missing. Offered Course: {this.OfferedCourse.OfferedCourseInfo}.");

			this.OfferedCourse.OfferedCourseExams.ForEach(oce =>
			{
				var (errorsOfferedCourseExams, warningsOfferedCourseExams) = oce.Validate();
				errors.AddRange(errorsOfferedCourseExams);
				warnings.AddRange(warningsOfferedCourseExams);
			});
			this.OfferedCourse.RegisteredCourses.ForEach(rc =>
			{
				var (errorsRegisteredCourses, warningsRegisteredCourses) = rc.Validate();
				errors.AddRange(errorsRegisteredCourses);
				warnings.AddRange(warningsRegisteredCourses);
			});
			return (errors.Distinct().ToList(), warnings.Distinct().ToList());
		}
	}
}
