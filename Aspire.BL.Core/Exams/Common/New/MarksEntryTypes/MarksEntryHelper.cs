﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.New.MarksEntryTypes
{
	internal static class MarksEntryHelper
	{
		public static (IEnumerable<string> addFailureReasons, int? offeredCourseExamID) AddOfferedCourseExam(IMarksSheet marksSheet, Model.Entities.OfferedCourseExam.ExamTypes examTypeEnum, DateTime examDate, byte examNo, decimal totalMarks, string remarks)
		{
			var aspireContext = ((IAspireContext)marksSheet).AspireContext;
			var now = DateTime.Now;
			var oldMarksSheetJSON = marksSheet.ToJson();
			var addFailureReasons = new List<string>();
			MarksSheetChanx.ActionTypes actionTypeEnum;
			switch (examTypeEnum)
			{
				case Model.Entities.OfferedCourseExam.ExamTypes.Assignment:
					if (marksSheet.OfferedCourse.OfferedCourseExamAssignments.Any(oce => oce.ExamNo == examNo))
					{
						addFailureReasons.Add($"{examTypeEnum.ToFullName()} No. {examNo} already exists.");
						return (addFailureReasons, null);
					}
					actionTypeEnum = MarksSheetChanx.ActionTypes.AddAssignment;
					break;
				case Model.Entities.OfferedCourseExam.ExamTypes.Quiz:
					if (marksSheet.OfferedCourse.OfferedCourseExamQuizzes.Any(oce => oce.ExamNo == examNo))
					{
						addFailureReasons.Add($"{examTypeEnum.ToFullName()} No. {examNo} already exists.");
						return (addFailureReasons, null);
					}
					actionTypeEnum = MarksSheetChanx.ActionTypes.AddQuiz;
					break;
				default:
					throw new NotImplementedEnumException(examTypeEnum);
			}

			if (totalMarks <= 0)
			{
				addFailureReasons.Add("Total Marks must be greater than zero.");
				return (addFailureReasons, null);
			}

			var offeredCourseExam = aspireContext.OfferedCourseExams.Add(new Model.Entities.OfferedCourseExam
			{
				OfferedCourseID = marksSheet.OfferedCourse.OfferedCourseID,
				ExamDate = examDate,
				ExamNo = examNo,
				ExamTypeEnum = examTypeEnum,
				TotalMarks = totalMarks,
				Remarks = remarks.TrimAndMakeItNullIfEmpty(),
				Locked = marksSheet.IsStaff,
				LockedDate = now,
				Important = false
			});
			var offeredCourse = aspireContext.OfferedCourses.Single(oc => oc.OfferedCourseID == marksSheet.OfferedCourse.OfferedCourseID);
			if (offeredCourse.MarksEntryLocked)
				throw new InvalidOperationException("Mark Entry is Locked.");
			bool clearAssignments = false, clearQuizzes = false, clearInternals = false;
			switch (marksSheet.MarksEntryTypeEnum)
			{
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
					clearAssignments = examTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Assignment;
					clearQuizzes = examTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Quiz;
					break;
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					clearInternals = examTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Assignment || examTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Quiz;
					break;
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
					throw new InvalidOperationException();
				default:
					throw new NotImplementedEnumException(marksSheet.MarksEntryTypeEnum);
			}

			offeredCourse.MarksEntryCompleted = false;
			if (clearAssignments)
			{
				offeredCourse.AssignmentsMarksSubmitted = false;
				offeredCourse.AssignmentsMarksSubmittedDate = null;
			}
			if (clearQuizzes)
			{
				offeredCourse.QuizzesMarksSubmitted = false;
				offeredCourse.QuizzesMarksSubmittedDate = null;
			}
			if (clearInternals)
			{
				offeredCourse.InternalsMarksSubmitted = false;
				offeredCourse.InternalsMarksSubmittedDate = null;
			}

			var registeredCourses = MarksSheetHelperMethods.GetRegisteredCourses(aspireContext, marksSheet);
			registeredCourses.ForEach(rc =>
			{
				if (clearAssignments)
					rc.Assignments = null;
				if (clearQuizzes)
					rc.Quizzes = null;
				if (clearInternals)
					rc.Internals = null;
				rc.Total = null;
				rc.Grade = null;
				rc.GradePoints = null;
				rc.Product = null;
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			var newMarksSheetJSON = ((IAspireContext)marksSheet).ReloadAndToJson(out var managementEmailRecipientsEnum);
			aspireContext.MarksSheetChanges.Add(new MarksSheetChanx
			{
				MarksSheetChangesID = Guid.NewGuid(),
				ActionTypeEnum = actionTypeEnum,
				ActionDate = now,
				LoginSessionGuid = marksSheet.LoginSessionGuid,
				OfferedCourseID = marksSheet.OfferedCourse.OfferedCourseID,
				OldMarksSheetJSON = oldMarksSheetJSON,
				NewMarksSheetJSON = newMarksSheetJSON,
				ManagementEmailRecipientsEnum = managementEmailRecipientsEnum
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			return (new string[] { }, offeredCourseExam.OfferedCourseExamID);
		}

		public static (IEnumerable<string> deleteFailureReasons, bool success) DeleteOfferedCourseExam(IMarksSheet marksSheet, Model.Entities.OfferedCourseExam.ExamTypes examTypeEnum, int offeredCourseExamID)
		{
			var aspireContext = ((IAspireContext)marksSheet).AspireContext;
			var now = DateTime.Now;
			var oldMarksSheetJSON = marksSheet.ToJson();
			var offeredCourseExam = marksSheet.OfferedCourse.OfferedCourseExams.Single(oce => oce.OfferedCourseExamID == offeredCourseExamID && oce.ExamTypeEnum == examTypeEnum);
			var clearAssignments = false;
			var clearQuizzes = false;
			var clearInternals = false;
			MarksSheetChanx.ActionTypes actionTypeEnum;
			switch (examTypeEnum)
			{
				case Model.Entities.OfferedCourseExam.ExamTypes.Assignment:
					actionTypeEnum = MarksSheetChanx.ActionTypes.DeleteAssignment;
					break;
				case Model.Entities.OfferedCourseExam.ExamTypes.Quiz:
					actionTypeEnum = MarksSheetChanx.ActionTypes.DeleteQuiz;
					break;
				default:
					throw new NotImplementedEnumException(examTypeEnum);
			}
			switch (marksSheet.MarksEntryTypeEnum)
			{
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
					clearAssignments = examTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Assignment;
					clearQuizzes = examTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Quiz;
					break;
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					clearInternals = examTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Assignment || examTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Quiz;
					break;
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
					throw new InvalidOperationException();
				default:
					throw new NotImplementedEnumException(marksSheet.MarksEntryTypeEnum);
			}

			var offeredCourseExamDB = aspireContext.OfferedCourseExams
				.Include(oce => oce.RegisteredCourseExamMarks)
				.Include(oce => oce.OfferedCours)
				.Single(oce => oce.OfferedCourseExamID == offeredCourseExam.OfferedCourseExamID && oce.OfferedCourseID == offeredCourseExam.OfferedCourse.OfferedCourseID);

			var offeredCourse = offeredCourseExamDB.OfferedCours;

			if (offeredCourse.MarksEntryLocked)
				throw new InvalidOperationException("Mark Entry is Locked.");
			aspireContext.RegisteredCourseExamMarks.RemoveRange(offeredCourseExamDB.RegisteredCourseExamMarks);
			aspireContext.OfferedCourseExams.Remove(offeredCourseExamDB);

			offeredCourse.MarksEntryCompleted = false;
			if (clearAssignments)
			{
				offeredCourse.AssignmentsMarksSubmitted = false;
				offeredCourse.AssignmentsMarksSubmittedDate = null;
			}
			if (clearQuizzes)
			{
				offeredCourse.QuizzesMarksSubmitted = false;
				offeredCourse.QuizzesMarksSubmittedDate = null;
			}
			if (clearInternals)
			{
				offeredCourse.InternalsMarksSubmitted = false;
				offeredCourse.InternalsMarksSubmittedDate = null;
			}

			var registeredCourses = MarksSheetHelperMethods.GetRegisteredCourses(aspireContext, marksSheet);
			registeredCourses.ForEach(rc =>
			{
				if (clearAssignments)
					rc.Assignments = null;
				if (clearQuizzes)
					rc.Quizzes = null;
				if (clearInternals)
					rc.Internals = null;
				rc.Total = null;
				rc.Grade = null;
				rc.GradePoints = null;
				rc.Product = null;
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			var newMarksSheetJSON = ((IAspireContext)marksSheet).ReloadAndToJson(out var managementEmailRecipientsEnum);
			aspireContext.MarksSheetChanges.Add(new MarksSheetChanx
			{
				MarksSheetChangesID = Guid.NewGuid(),
				ActionTypeEnum = actionTypeEnum,
				ActionDate = now,
				LoginSessionGuid = marksSheet.LoginSessionGuid,
				OfferedCourseID = marksSheet.OfferedCourse.OfferedCourseID,
				OldMarksSheetJSON = oldMarksSheetJSON,
				NewMarksSheetJSON = newMarksSheetJSON,
				ManagementEmailRecipientsEnum = managementEmailRecipientsEnum
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			return (new string[] { }, true);
		}

		public static (IEnumerable<string> updateFailureReasons, bool success) UpdateOfferedCourseExam(IMarksSheet marksSheet, Model.Entities.OfferedCourseExam.ExamTypes examTypeEnum, int offeredCourseExamID, DateTime examDate, byte examNo, string remarks, IReadOnlyDictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)> marks, bool submit)
		{
			var aspireContext = ((IAspireContext)marksSheet).AspireContext;
			var now = DateTime.Now;
			var oldMarksSheetJSON = marksSheet.ToJson();
			var updateFailureReasons = new List<string>();
			var offeredCourseExam = marksSheet.OfferedCourse.OfferedCourseExams.Single(oce => oce.OfferedCourseExamID == offeredCourseExamID && oce.ExamTypeEnum == examTypeEnum);

			var clearAssignments = false;
			var clearQuizzes = false;
			var clearInternals = false;
			MarksSheetChanx.ActionTypes actionTypeEnum;
			switch (examTypeEnum)
			{
				case Model.Entities.OfferedCourseExam.ExamTypes.Assignment:
					actionTypeEnum = MarksSheetChanx.ActionTypes.UpdateAssignment;
					break;
				case Model.Entities.OfferedCourseExam.ExamTypes.Quiz:
					actionTypeEnum = MarksSheetChanx.ActionTypes.UpdateQuiz;
					break;
				default:
					throw new NotImplementedEnumException(examTypeEnum);
			}
			switch (marksSheet.MarksEntryTypeEnum)
			{
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
					clearAssignments = examTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Assignment;
					clearQuizzes = examTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Quiz;
					break;
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					clearInternals = examTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Assignment || examTypeEnum == Model.Entities.OfferedCourseExam.ExamTypes.Quiz;
					break;
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
					throw new InvalidOperationException();
				default:
					throw new NotImplementedEnumException(marksSheet.MarksEntryTypeEnum);
			}

			var offeredCourseExamInDB = aspireContext.OfferedCourseExams
				.Include(oce => oce.OfferedCours)
				.Include(oce => oce.RegisteredCourseExamMarks)
				.Single(oce => oce.OfferedCourseExamID == offeredCourseExamID && oce.OfferedCourseID == offeredCourseExam.OfferedCourse.OfferedCourseID);
			if (offeredCourseExamInDB.OfferedCours.MarksEntryLocked)
				throw new InvalidOperationException("Mark Entry is Locked.");
			var registeredCourses = MarksSheetHelperMethods.GetRegisteredCourses(aspireContext, marksSheet);

			var marksInDB = registeredCourses.Select(rc => new
			{
				rc.RegisteredCourseID,
				offeredCourseExamInDB.RegisteredCourseExamMarks.SingleOrDefault(rcem => rcem.RegisteredCourseID == rc.RegisteredCourseID)?.ExamMarks
			}).OrderBy(rc => rc.RegisteredCourseID).ToList();
			var oldMarks = marks.Select(sm => new
			{
				RegisteredCourseID = sm.Key,
				ExamMarks = sm.Value.marksObtainedInDB
			}).OrderBy(rc => rc.RegisteredCourseID).ToList();

			if (!marksInDB.Select(rc => rc.RegisteredCourseID).SequenceEqual(oldMarks.Select(rc => rc.RegisteredCourseID)))
			{
				updateFailureReasons.Add("Course Registration has been updated in other session. Please try again.");
				return (updateFailureReasons, false);
			}

			if (!marksInDB.SequenceEqual(oldMarks))
			{
				updateFailureReasons.Add("Marks have been updated in other session. Please try again.");
				return (updateFailureReasons, false);
			}

			if (aspireContext.OfferedCourseExams.Any(oce => oce.OfferedCourseExamID != offeredCourseExamInDB.OfferedCourseExamID && oce.OfferedCourseID == offeredCourseExamInDB.OfferedCourseID && oce.ExamType == offeredCourseExamInDB.ExamType && oce.ExamNo == offeredCourseExamInDB.ExamNo))
			{
				updateFailureReasons.Add($"{offeredCourseExamInDB.ExamTypeEnum.ToFullName() } No. {offeredCourseExamInDB.ExamNo} already exists.");
				return (updateFailureReasons, false);
			}

			offeredCourseExamInDB.ExamDate = examDate;
			offeredCourseExamInDB.ExamNo = examNo;
			offeredCourseExamInDB.Remarks = remarks.TrimAndMakeItNullIfEmpty();
			offeredCourseExamInDB.LockedDate = now;
			if (offeredCourseExamInDB.Locked && !submit)
				throw new InvalidOperationException();
			if (submit)
				offeredCourseExamInDB.Locked = true;

			foreach (var sMarks in marks)
			{
				var iRegisteredCourse = marksSheet.OfferedCourse.RegisteredCourses.Single(rc => rc.RegisteredCourseID == sMarks.Key);
				var registeredCourseExamMark = offeredCourseExamInDB.RegisteredCourseExamMarks.SingleOrDefault(rcem => rcem.RegisteredCourseID == sMarks.Key);
				if (iRegisteredCourse.MarksEntryAllowed)
				{
					if (registeredCourseExamMark == null)
						registeredCourseExamMark = aspireContext.RegisteredCourseExamMarks.Add(new Model.Entities.RegisteredCourseExamMark
						{
							RegisteredCourseID = sMarks.Key,
							OfferedCourseExamID = offeredCourseExamInDB.OfferedCourseExamID
						});
					registeredCourseExamMark.ExamMarks = sMarks.Value.marksObtainedNew;
				}
				else
				{
					if (registeredCourseExamMark?.ExamMarks != sMarks.Value.marksObtainedNew)
						return (iRegisteredCourse.MarksEntryNotAllowedReasons, false);
				}
			}

			offeredCourseExamInDB.OfferedCours.MarksEntryCompleted = false;
			if (clearAssignments)
				offeredCourseExamInDB.OfferedCours.AssignmentsMarksSubmitted = false;
			if (clearQuizzes)
				offeredCourseExamInDB.OfferedCours.QuizzesMarksSubmitted = false;
			if (clearInternals)
				offeredCourseExamInDB.OfferedCours.InternalsMarksSubmitted = false;
			registeredCourses.ForEach(rc =>
			{
				if (clearAssignments)
					rc.Assignments = null;
				if (clearQuizzes)
					rc.Quizzes = null;
				if (clearInternals)
					rc.Internals = null;
				rc.Total = null;
				rc.Grade = null;
				rc.GradePoints = null;
				rc.Product = null;
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			var newMarksSheetJSON = ((IAspireContext)marksSheet).ReloadAndToJson(out var managementEmailRecipientsEnum);
			aspireContext.MarksSheetChanges.Add(new MarksSheetChanx
			{
				MarksSheetChangesID = Guid.NewGuid(),
				ActionTypeEnum = actionTypeEnum,
				ActionDate = now,
				LoginSessionGuid = marksSheet.LoginSessionGuid,
				OfferedCourseID = marksSheet.OfferedCourse.OfferedCourseID,
				OldMarksSheetJSON = oldMarksSheetJSON,
				NewMarksSheetJSON = newMarksSheetJSON,
				ManagementEmailRecipientsEnum = managementEmailRecipientsEnum
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			return (new string[] { }, true);
		}

		public static (IEnumerable<string> compileFailureReasons, bool success) CompileExamMarks(IMarksSheet marksSheet, CompileExamMarksTypes compileExamMarksType, OfferedCours.ExamCompilationTypes examCompilationType, bool submit)
		{
			var aspireContext = ((IAspireContext)marksSheet).AspireContext;
			var now = DateTime.Now;
			var oldMarksSheetJSON = marksSheet.ToJson();
			var registeredCourses = MarksSheetHelperMethods.GetRegisteredCourses(aspireContext, marksSheet);
			var offeredCourse = aspireContext.OfferedCourses.Single(oc => oc.OfferedCourseID == marksSheet.OfferedCourse.OfferedCourseID);
			if (offeredCourse.MarksEntryLocked)
				throw new InvalidOperationException("Mark Entry is Locked.");
			MarksSheetChanx.ActionTypes actionTypeEnum;
			switch (compileExamMarksType)
			{
				case CompileExamMarksTypes.Assignments:
					offeredCourse.AssignmentsExamCompilationTypeEnum = examCompilationType;
					if (offeredCourse.AssignmentsMarksSubmitted && !submit)
						throw new InvalidOperationException();
					offeredCourse.AssignmentsMarksSubmitted = submit;
					offeredCourse.AssignmentsMarksSubmittedDate = now;
					if (submit)
					{
						aspireContext.OfferedCourseExams
							.Where(oce => oce.OfferedCourseID == offeredCourse.OfferedCourseID && !oce.Locked)
							.Where(oce => oce.ExamType == (byte)Model.Entities.OfferedCourseExam.ExamTypes.Assignment)
							.ToList()
							.ForEach(oce =>
							{
								oce.Locked = true;
								oce.LockedDate = now;
							});
					}
					actionTypeEnum = MarksSheetChanx.ActionTypes.CompileAssignments;
					break;
				case CompileExamMarksTypes.Quizzes:
					offeredCourse.QuizzesExamCompilationTypeEnum = examCompilationType;
					if (offeredCourse.QuizzesMarksSubmitted && !submit)
						throw new InvalidOperationException();
					offeredCourse.QuizzesMarksSubmitted = submit;
					offeredCourse.QuizzesMarksSubmittedDate = now;
					if (submit)
					{
						aspireContext.OfferedCourseExams
							.Where(oce => oce.OfferedCourseID == offeredCourse.OfferedCourseID && !oce.Locked)
							.Where(oce => oce.ExamType == (byte)Model.Entities.OfferedCourseExam.ExamTypes.Quiz)
							.ToList()
							.ForEach(oce =>
							{
								oce.Locked = true;
								oce.LockedDate = now;
							});
					}
					actionTypeEnum = MarksSheetChanx.ActionTypes.CompileQuizzes;
					break;
				case CompileExamMarksTypes.Internals:
					offeredCourse.InternalsExamCompilationTypeEnum = examCompilationType;
					if (offeredCourse.InternalsMarksSubmitted && !submit)
						throw new InvalidOperationException();
					offeredCourse.InternalsMarksSubmitted = submit;
					offeredCourse.InternalsMarksSubmittedDate = now;
					if (submit)
					{
						aspireContext.OfferedCourseExams
							.Where(oce => oce.OfferedCourseID == offeredCourse.OfferedCourseID && !oce.Locked)
							.Where(oce => oce.ExamType == (byte)Model.Entities.OfferedCourseExam.ExamTypes.Assignment || oce.ExamType == (byte)Model.Entities.OfferedCourseExam.ExamTypes.Quiz)
							.ToList()
							.ForEach(oce =>
							{
								oce.Locked = true;
								oce.LockedDate = now;
							});
					}
					actionTypeEnum = MarksSheetChanx.ActionTypes.CompileInternals;
					break;
				default:
					throw new NotImplementedEnumException(compileExamMarksType);
			}

			offeredCourse.MarksEntryCompleted = false;
			registeredCourses.ForEach(rc =>
			{
				var iRegisteredCourse = marksSheet.OfferedCourse.RegisteredCourses.Single(irc => irc.RegisteredCourseID == rc.RegisteredCourseID);
				switch (compileExamMarksType)
				{
					case CompileExamMarksTypes.Assignments:
						rc.Assignments = iRegisteredCourse.CompilationAllowed ? iRegisteredCourse.GetCompiledAssignmentsMarks(examCompilationType) : null;
						break;
					case CompileExamMarksTypes.Quizzes:
						rc.Quizzes = iRegisteredCourse.CompilationAllowed ? iRegisteredCourse.GetCompiledQuizzesMarks(examCompilationType) : null;
						break;
					case CompileExamMarksTypes.Internals:
						rc.Internals = iRegisteredCourse.CompilationAllowed ? iRegisteredCourse.GetCompiledInternalsMarks(examCompilationType) : null;
						break;
					default:
						throw new NotImplementedEnumException(compileExamMarksType);
				}
				rc.Total = null;
				rc.Grade = null;
				rc.GradePoints = null;
				rc.Product = null;
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			var newMarksSheetJSON = ((IAspireContext)marksSheet).ReloadAndToJson(out var managementEmailRecipientsEnum);
			aspireContext.MarksSheetChanges.Add(new MarksSheetChanx
			{
				MarksSheetChangesID = Guid.NewGuid(),
				ActionTypeEnum = actionTypeEnum,
				ActionDate = now,
				LoginSessionGuid = marksSheet.LoginSessionGuid,
				OfferedCourseID = marksSheet.OfferedCourse.OfferedCourseID,
				OldMarksSheetJSON = oldMarksSheetJSON,
				NewMarksSheetJSON = newMarksSheetJSON,
				ManagementEmailRecipientsEnum = managementEmailRecipientsEnum
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			return (new string[] { }, true);
		}

		public static (IEnumerable<string> unlockFailureReasons, bool success) UnlockOfferedCourseExam(IMarksSheet marksSheet, Model.Entities.OfferedCourseExam.ExamTypes examTypeEnum, int offeredCourseExamID)
		{
			var aspireContext = ((IAspireContext)marksSheet).AspireContext;
			var now = DateTime.Now;
			var oldMarksSheetJSON = marksSheet.ToJson();
			MarksSheetChanx.ActionTypes actionTypeEnum;
			switch (examTypeEnum)
			{
				case Model.Entities.OfferedCourseExam.ExamTypes.Assignment:
					actionTypeEnum = MarksSheetChanx.ActionTypes.UnlockAssignment;
					break;
				case Model.Entities.OfferedCourseExam.ExamTypes.Quiz:
					actionTypeEnum = MarksSheetChanx.ActionTypes.UnlockQuiz;
					break;
				default:
					throw new NotImplementedEnumException(examTypeEnum);
			}
			var offeredCourseExamInDB = aspireContext.OfferedCourseExams.Include(oce => oce.OfferedCours).Single(oce => oce.OfferedCourseExamID == offeredCourseExamID && oce.ExamType == (byte)examTypeEnum && oce.OfferedCourseID == marksSheet.OfferedCourse.OfferedCourseID);
			if (offeredCourseExamInDB.OfferedCours.MarksEntryLocked)
				throw new InvalidOperationException("Mark Entry is Locked.");
			offeredCourseExamInDB.Locked = false;
			offeredCourseExamInDB.OfferedCours.MarksEntryCompleted = false;
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			var newMarksSheetJSON = ((IAspireContext)marksSheet).ReloadAndToJson(out var managementEmailRecipientsEnum);
			aspireContext.MarksSheetChanges.Add(new MarksSheetChanx
			{
				MarksSheetChangesID = Guid.NewGuid(),
				ActionTypeEnum = actionTypeEnum,
				ActionDate = now,
				LoginSessionGuid = marksSheet.LoginSessionGuid,
				OfferedCourseID = marksSheet.OfferedCourse.OfferedCourseID,
				OldMarksSheetJSON = oldMarksSheetJSON,
				NewMarksSheetJSON = newMarksSheetJSON,
				ManagementEmailRecipientsEnum = managementEmailRecipientsEnum
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			return (new string[] { }, true);
		}

		public static (IEnumerable<string> updateFailureReasons, bool success) UpdateExamMarks(IMarksSheet marksSheet, ExamMarksTypes examMarksTypeEnum, IReadOnlyDictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)> marks, bool submit)
		{
			var aspireContext = ((IAspireContext)marksSheet).AspireContext;
			var now = DateTime.Now;
			var oldMarksSheetJSON = marksSheet.ToJson();
			var registeredCourses = MarksSheetHelperMethods.GetRegisteredCourses(aspireContext, marksSheet);
			List<(int RegisteredCourseID, decimal? ExamMarks)> marksInDB;
			List<(int RegisteredCourseID, decimal? ExamMarks)> oldMarks = marks.Select(sm => (sm.Key, sm.Value.marksObtainedInDB)).OrderBy(m => m.Key).ToList();

			MarksSheetChanx.ActionTypes actionTypeEnum;
			switch (examMarksTypeEnum)
			{
				case ExamMarksTypes.Assignments:
					marksInDB = registeredCourses.Select(rc => (rc.RegisteredCourseID, rc.Assignments)).OrderBy(rc => rc.RegisteredCourseID).ToList();
					actionTypeEnum = MarksSheetChanx.ActionTypes.UpdateAssignments;
					break;
				case ExamMarksTypes.Quizzes:
					marksInDB = registeredCourses.Select(rc => (rc.RegisteredCourseID, rc.Quizzes)).OrderBy(rc => rc.RegisteredCourseID).ToList();
					actionTypeEnum = MarksSheetChanx.ActionTypes.UpdateQuizzes;
					break;
				case ExamMarksTypes.Internals:
					throw new InvalidOperationException();
				case ExamMarksTypes.Mid:
					marksInDB = registeredCourses.Select(rc => (rc.RegisteredCourseID, rc.Mid)).OrderBy(rc => rc.RegisteredCourseID).ToList();
					actionTypeEnum = MarksSheetChanx.ActionTypes.UpdateMid;
					break;
				case ExamMarksTypes.Final:
					marksInDB = registeredCourses.Select(rc => (rc.RegisteredCourseID, rc.Final)).OrderBy(rc => rc.RegisteredCourseID).ToList();
					actionTypeEnum = MarksSheetChanx.ActionTypes.UpdateFinal;
					break;
				default:
					throw new NotImplementedEnumException(examMarksTypeEnum);
			}
			if (!marksInDB.Select(rc => rc.RegisteredCourseID).SequenceEqual(oldMarks.Select(rc => rc.RegisteredCourseID)))
				return (new[] { "Course Registration has been updated in other session. Please try again." }, false);

			if (!marksInDB.SequenceEqual(oldMarks))
				return (new[] { "Marks have been updated in other session. Please try again." }, false);

			var offeredCourse = aspireContext.OfferedCourses.Single(oc => oc.OfferedCourseID == marksSheet.OfferedCourse.OfferedCourseID);
			if (offeredCourse.MarksEntryLocked)
				throw new InvalidOperationException("Mark Entry is Locked.");

			foreach (var sMarks in marks)
			{
				var iRegisteredCourse = marksSheet.OfferedCourse.RegisteredCourses.Single(rc => rc.RegisteredCourseID == sMarks.Key);
				var registeredCourseDB = registeredCourses.Single(rc => rc.RegisteredCourseID == sMarks.Key);
				switch (examMarksTypeEnum)
				{
					case ExamMarksTypes.Assignments:
						if (iRegisteredCourse.MarksEntryAllowed)
							registeredCourseDB.Assignments = sMarks.Value.marksObtainedNew;
						else if (registeredCourseDB.Assignments != sMarks.Value.marksObtainedNew)
							return (iRegisteredCourse.MarksEntryNotAllowedReasons, false);
						break;
					case ExamMarksTypes.Quizzes:
						if (iRegisteredCourse.MarksEntryAllowed)
							registeredCourseDB.Quizzes = sMarks.Value.marksObtainedNew;
						else if (registeredCourseDB.Quizzes != sMarks.Value.marksObtainedNew)
							return (iRegisteredCourse.MarksEntryNotAllowedReasons, false);
						break;
					case ExamMarksTypes.Internals:
						throw new InvalidOperationException();
					case ExamMarksTypes.Mid:
						if (iRegisteredCourse.MarksEntryAllowed)
							registeredCourseDB.Mid = sMarks.Value.marksObtainedNew;
						else if (registeredCourseDB.Mid != sMarks.Value.marksObtainedNew)
							return (iRegisteredCourse.MarksEntryNotAllowedReasons, false);
						break;
					case ExamMarksTypes.Final:
						if (iRegisteredCourse.MarksEntryAllowed)
							registeredCourseDB.Final = sMarks.Value.marksObtainedNew;
						else if (registeredCourseDB.Final != sMarks.Value.marksObtainedNew)
							return (iRegisteredCourse.MarksEntryNotAllowedReasons, false);
						break;
					default:
						throw new NotImplementedEnumException(examMarksTypeEnum);
				}
			}

			offeredCourse.MarksEntryCompleted = false;
			switch (examMarksTypeEnum)
			{
				case ExamMarksTypes.Assignments:
					if (offeredCourse.AssignmentsMarksSubmitted && !submit)
						throw new InvalidOperationException();
					offeredCourse.AssignmentsMarksSubmitted = submit;
					offeredCourse.AssignmentsMarksSubmittedDate = now;
					break;
				case ExamMarksTypes.Quizzes:
					if (offeredCourse.QuizzesMarksSubmitted && !submit)
						throw new InvalidOperationException();
					offeredCourse.QuizzesMarksSubmitted = submit;
					offeredCourse.QuizzesMarksSubmittedDate = now;
					break;
				case ExamMarksTypes.Internals:
					throw new InvalidOperationException();
				case ExamMarksTypes.Mid:
					if (offeredCourse.MidMarksSubmitted && !submit)
						throw new InvalidOperationException();
					offeredCourse.MidMarksSubmitted = submit;
					offeredCourse.MidMarksSubmittedDate = now;
					break;
				case ExamMarksTypes.Final:
					if (offeredCourse.FinalMarksSubmitted && !submit)
						throw new InvalidOperationException();
					offeredCourse.FinalMarksSubmitted = submit;
					offeredCourse.FinalMarksSubmittedDate = now;
					break;
				default:
					throw new NotImplementedEnumException(examMarksTypeEnum);
			}

			registeredCourses.ForEach(rc =>
			{
				rc.Total = null;
				rc.Grade = null;
				rc.GradePoints = null;
				rc.Product = null;
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			var newMarksSheetJSON = ((IAspireContext)marksSheet).ReloadAndToJson(out var managementEmailRecipientsEnum);
			aspireContext.MarksSheetChanges.Add(new MarksSheetChanx
			{
				MarksSheetChangesID = Guid.NewGuid(),
				ActionTypeEnum = actionTypeEnum,
				ActionDate = now,
				LoginSessionGuid = marksSheet.LoginSessionGuid,
				OfferedCourseID = marksSheet.OfferedCourse.OfferedCourseID,
				OldMarksSheetJSON = oldMarksSheetJSON,
				NewMarksSheetJSON = newMarksSheetJSON,
				ManagementEmailRecipientsEnum = managementEmailRecipientsEnum
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			return (new string[] { }, true);
		}

		public static (IEnumerable<string> unlockFailureReasons, bool success) UnlockExamMarks(IMarksSheet marksSheet, ExamMarksTypes examMarksTypeEnum)
		{
			var aspireContext = ((IAspireContext)marksSheet).AspireContext;
			var now = DateTime.Now;
			var oldMarksSheetJSON = marksSheet.ToJson();
			var offeredCourse = aspireContext.OfferedCourses.Single(oc => oc.OfferedCourseID == marksSheet.OfferedCourse.OfferedCourseID);
			if (offeredCourse.MarksEntryLocked)
				throw new InvalidOperationException("Mark Entry is Locked.");
			offeredCourse.MarksEntryCompleted = false;
			MarksSheetChanx.ActionTypes actionTypeEnum;
			switch (examMarksTypeEnum)
			{
				case ExamMarksTypes.Assignments:
					if (!offeredCourse.AssignmentsMarksSubmitted)
						throw new InvalidOperationException();
					offeredCourse.AssignmentsMarksSubmitted = false;
					actionTypeEnum = MarksSheetChanx.ActionTypes.UnlockAssignments;
					break;
				case ExamMarksTypes.Quizzes:
					if (!offeredCourse.QuizzesMarksSubmitted)
						throw new InvalidOperationException();
					offeredCourse.QuizzesMarksSubmitted = false;
					actionTypeEnum = MarksSheetChanx.ActionTypes.UnlockQuizzes;
					break;
				case ExamMarksTypes.Internals:
					if (!offeredCourse.InternalsMarksSubmitted)
						throw new InvalidOperationException();
					offeredCourse.InternalsMarksSubmitted = false;
					actionTypeEnum = MarksSheetChanx.ActionTypes.UnlockInternals;
					break;
				case ExamMarksTypes.Mid:
					if (!offeredCourse.MidMarksSubmitted)
						throw new InvalidOperationException();
					offeredCourse.MidMarksSubmitted = false;
					actionTypeEnum = MarksSheetChanx.ActionTypes.UnlockMid;
					break;
				case ExamMarksTypes.Final:
					if (!offeredCourse.FinalMarksSubmitted)
						throw new InvalidOperationException();
					offeredCourse.FinalMarksSubmitted = false;
					actionTypeEnum = MarksSheetChanx.ActionTypes.UnlockFinal;
					break;
				default:
					throw new NotImplementedEnumException(examMarksTypeEnum);
			}
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			var newMarksSheetJSON = ((IAspireContext)marksSheet).ReloadAndToJson(out var managementEmailRecipientsEnum);
			aspireContext.MarksSheetChanges.Add(new MarksSheetChanx
			{
				MarksSheetChangesID = Guid.NewGuid(),
				ActionTypeEnum = actionTypeEnum,
				ActionDate = now,
				LoginSessionGuid = marksSheet.LoginSessionGuid,
				OfferedCourseID = marksSheet.OfferedCourse.OfferedCourseID,
				OldMarksSheetJSON = oldMarksSheetJSON,
				NewMarksSheetJSON = newMarksSheetJSON,
				ManagementEmailRecipientsEnum = managementEmailRecipientsEnum
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			return (new string[] { }, true);
		}

		private static void LockMarksEntry(AspireContext aspireContext, IMarksSheet marksSheet, OfferedCours offeredCourse, DateTime now, List<RegisteredCours> registeredCourses)
		{
			var offeredCourseExams = aspireContext.OfferedCourseExams.Where(oce => oce.OfferedCourseID == offeredCourse.OfferedCourseID && !oce.Locked && (oce.ExamType == (byte)Model.Entities.OfferedCourseExam.ExamTypes.Assignment || oce.ExamType == (byte)Model.Entities.OfferedCourseExam.ExamTypes.Quiz));
			switch (marksSheet.MarksEntryTypeEnum)
			{
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
					{
						offeredCourseExams.ToList().ForEach(oce =>
						{
							oce.LockedDate = (oce.Locked ? oce.LockedDate : now) ?? now;
							oce.Locked = true;
						});

						if (!offeredCourse.AssignmentsMarksSubmitted)
						{
							offeredCourse.AssignmentsMarksSubmitted = true;
							offeredCourse.AssignmentsMarksSubmittedDate = now;
						}

						offeredCourse.AssignmentsMarksSubmittedDate = offeredCourse.AssignmentsMarksSubmittedDate ?? now;

						if (!offeredCourse.QuizzesMarksSubmitted)
						{
							offeredCourse.QuizzesMarksSubmitted = true;
							offeredCourse.QuizzesMarksSubmittedDate = now;
						}

						offeredCourse.QuizzesMarksSubmittedDate = offeredCourse.QuizzesMarksSubmittedDate ?? now;

						if (!offeredCourse.MidMarksSubmitted)
						{
							offeredCourse.MidMarksSubmitted = true;
							offeredCourse.MidMarksSubmittedDate = now;
						}

						offeredCourse.MidMarksSubmittedDate = offeredCourse.MidMarksSubmittedDate ?? now;

						if (!offeredCourse.FinalMarksSubmitted)
						{
							offeredCourse.FinalMarksSubmitted = true;
							offeredCourse.FinalMarksSubmittedDate = now;
						}

						offeredCourse.FinalMarksSubmittedDate = offeredCourse.FinalMarksSubmittedDate ?? now;
					}
					break;
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					{
						offeredCourseExams.ToList().ForEach(oce =>
						{
							oce.LockedDate = (oce.Locked ? oce.LockedDate : now) ?? now;
							oce.Locked = true;
						});

						if (!offeredCourse.InternalsMarksSubmitted)
						{
							offeredCourse.InternalsMarksSubmitted = true;
							offeredCourse.InternalsMarksSubmittedDate = now;
						}

						offeredCourse.InternalsMarksSubmittedDate = offeredCourse.InternalsMarksSubmittedDate ?? now;

						if (!offeredCourse.MidMarksSubmitted)
						{
							offeredCourse.MidMarksSubmitted = true;
							offeredCourse.MidMarksSubmittedDate = now;
						}

						offeredCourse.MidMarksSubmittedDate = offeredCourse.MidMarksSubmittedDate ?? now;

						if (!offeredCourse.FinalMarksSubmitted)
						{
							offeredCourse.FinalMarksSubmitted = true;
							offeredCourse.FinalMarksSubmittedDate = now;
						}

						offeredCourse.FinalMarksSubmittedDate = offeredCourse.FinalMarksSubmittedDate ?? now;
					}
					break;
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
					{
						if (!offeredCourse.AssignmentsMarksSubmitted)
						{
							offeredCourse.AssignmentsMarksSubmitted = true;
							offeredCourse.AssignmentsMarksSubmittedDate = now;
						}

						offeredCourse.AssignmentsMarksSubmittedDate = offeredCourse.AssignmentsMarksSubmittedDate ?? now;

						if (!offeredCourse.QuizzesMarksSubmitted)
						{
							offeredCourse.QuizzesMarksSubmitted = true;
							offeredCourse.QuizzesMarksSubmittedDate = now;
						}

						offeredCourse.QuizzesMarksSubmittedDate = offeredCourse.QuizzesMarksSubmittedDate ?? now;

						if (!offeredCourse.MidMarksSubmitted)
						{
							offeredCourse.MidMarksSubmitted = true;
							offeredCourse.MidMarksSubmittedDate = now;
						}

						offeredCourse.MidMarksSubmittedDate = offeredCourse.MidMarksSubmittedDate ?? now;

						if (!offeredCourse.FinalMarksSubmitted)
						{
							offeredCourse.FinalMarksSubmitted = true;
							offeredCourse.FinalMarksSubmittedDate = now;
						}

						offeredCourse.FinalMarksSubmittedDate = offeredCourse.FinalMarksSubmittedDate ?? now;
					}
					break;
				case Model.Entities.ExamMarksPolicy.MarksEntryTypes.Final:
					{
						if (!offeredCourse.FinalMarksSubmitted)
						{
							offeredCourse.FinalMarksSubmitted = true;
							offeredCourse.FinalMarksSubmittedDate = now;
						}

						offeredCourse.FinalMarksSubmittedDate = offeredCourse.FinalMarksSubmittedDate ?? now;
					}
					break;
				default:
					throw new NotImplementedEnumException(marksSheet.MarksEntryTypeEnum);
			}

			offeredCourse.MarksEntryLockedDate = (offeredCourse.MarksEntryLocked ? offeredCourse.MarksEntryLockedDate : now) ?? now;
			offeredCourse.MarksEntryLocked = true;
			registeredCourses.ForEach(rc =>
			{
				rc.MarksEntryLockedDate = (rc.MarksEntryLocked ? rc.MarksEntryLockedDate : now) ?? now;
				rc.MarksEntryLocked = true;
			});
		}

		private static (IEnumerable<string> compileFailureReasons, bool success) CompileTotalAndGrades(IMarksSheet marksSheet, bool lockMarksEntry)
		{
			var aspireContext = ((IAspireContext)marksSheet).AspireContext;
			var now = DateTime.Now;
			var oldMarksSheetJSON = marksSheet.ToJson();
			var offeredCourse = aspireContext.OfferedCourses.Single(oc => oc.OfferedCourseID == marksSheet.OfferedCourse.OfferedCourseID);
			if (offeredCourse.MarksEntryLocked)
				throw new InvalidOperationException("Mark Entry is Locked.");
			offeredCourse.MarksEntryCompleted = true;
			var registeredCourses = MarksSheetHelperMethods.GetRegisteredCourses(aspireContext, marksSheet);
			foreach (var registeredCourse in registeredCourses)
				marksSheet.OfferedCourse.RegisteredCourses.Single(irc => irc.RegisteredCourseID == registeredCourse.RegisteredCourseID).SetTotalsAndGrades(registeredCourse);
			if (lockMarksEntry)
				LockMarksEntry(aspireContext, marksSheet, offeredCourse, now, registeredCourses);
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			var newMarksSheetJSON = ((IAspireContext)marksSheet).ReloadAndToJson(out var managementEmailRecipientsEnum);
			aspireContext.MarksSheetChanges.Add(new MarksSheetChanx
			{
				MarksSheetChangesID = Guid.NewGuid(),
				ActionTypeEnum = lockMarksEntry ? MarksSheetChanx.ActionTypes.LockMarksEntry : MarksSheetChanx.ActionTypes.CompileTotalAndGrades,
				ActionDate = now,
				LoginSessionGuid = marksSheet.LoginSessionGuid,
				OfferedCourseID = marksSheet.OfferedCourse.OfferedCourseID,
				OldMarksSheetJSON = oldMarksSheetJSON,
				NewMarksSheetJSON = newMarksSheetJSON,
				ManagementEmailRecipientsEnum = managementEmailRecipientsEnum
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			return (new string[] { }, true);
		}

		public static (IEnumerable<string> compileFailureReasons, bool success) CompileTotalAndGrades(IMarksSheet marksSheet)
		{
			return CompileTotalAndGrades(marksSheet, false);
		}

		public static (IEnumerable<string> lockFailureReasons, bool success) LockMarksEntry(IMarksSheet marksSheet)
		{
			return CompileTotalAndGrades(marksSheet, true);
		}

		public static (IEnumerable<string> unlockFailureReasons, bool success) UnlockMarksEntry(IMarksSheet marksSheet)
		{
			var aspireContext = ((IAspireContext)marksSheet).AspireContext;
			var now = DateTime.Now;
			var oldMarksSheetJSON = marksSheet.ToJson();
			var offeredCourse = aspireContext.OfferedCourses.Single(oc => oc.OfferedCourseID == marksSheet.OfferedCourse.OfferedCourseID);
			if (!offeredCourse.MarksEntryLocked)
				throw new InvalidOperationException("Mark Entry is not locked.");
			var registeredCourses = MarksSheetHelperMethods.GetRegisteredCourses(aspireContext, marksSheet);
			registeredCourses.ForEach(rc =>
			{
				rc.MarksEntryLocked = false;
				rc.MarksEntryLockedDate = null;
			});
			offeredCourse.MarksEntryLocked = false;
			offeredCourse.MarksEntryCompleted = false;
			offeredCourse.MarksEntryLockedDate = null;
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			var newMarksSheetJSON = ((IAspireContext)marksSheet).ReloadAndToJson(out var managementEmailRecipientsEnum);
			aspireContext.MarksSheetChanges.Add(new MarksSheetChanx
			{
				MarksSheetChangesID = Guid.NewGuid(),
				ActionTypeEnum = MarksSheetChanx.ActionTypes.UnlockMarksEntry,
				ActionDate = now,
				LoginSessionGuid = marksSheet.LoginSessionGuid,
				OfferedCourseID = marksSheet.OfferedCourse.OfferedCourseID,
				OldMarksSheetJSON = oldMarksSheetJSON,
				NewMarksSheetJSON = newMarksSheetJSON,
				ManagementEmailRecipientsEnum = managementEmailRecipientsEnum
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			return (new string[] { }, true);
		}

		public static (IEnumerable<string> unlockFailureReasons, bool success) UnlockMarksEntryForRegisteredCourse(IRegisteredCourse registeredCourse)
		{
			var marksSheet = registeredCourse.OfferedCourse.MarksSheet;
			var aspireContext = ((IAspireContext)marksSheet).AspireContext;
			var now = DateTime.Now;
			var oldMarksSheetJSON = marksSheet.ToJson();
			var offeredCourse = aspireContext.OfferedCourses.Single(oc => oc.OfferedCourseID == marksSheet.OfferedCourse.OfferedCourseID);
			var registeredCourses = MarksSheetHelperMethods.GetRegisteredCourses(aspireContext, marksSheet);
			registeredCourses.ForEach(rc =>
			{
				if (rc.RegisteredCourseID == registeredCourse.RegisteredCourseID)
				{
					rc.MarksEntryLocked = false;
					rc.MarksEntryLockedDate = null;
				}
			});
			offeredCourse.MarksEntryLocked = false;
			offeredCourse.MarksEntryCompleted = false;
			offeredCourse.MarksEntryLockedDate = null;
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			var newMarksSheetJSON = ((IAspireContext)marksSheet).ReloadAndToJson(out var managementEmailRecipientsEnum);
			aspireContext.MarksSheetChanges.Add(new MarksSheetChanx
			{
				MarksSheetChangesID = Guid.NewGuid(),
				ActionTypeEnum = MarksSheetChanx.ActionTypes.UnlockMarksEntryForRegisteredCourse,
				ActionDate = now,
				LoginSessionGuid = marksSheet.LoginSessionGuid,
				OfferedCourseID = marksSheet.OfferedCourse.OfferedCourseID,
				OldMarksSheetJSON = oldMarksSheetJSON,
				NewMarksSheetJSON = newMarksSheetJSON,
				ManagementEmailRecipientsEnum = managementEmailRecipientsEnum
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			return (new string[] { }, true);
		}

		public static (IEnumerable<string> submitToHODFailureReasons, bool success) SubmitToHOD(IMarksSheet marksSheet)
		{
			var aspireContext = ((IAspireContext)marksSheet).AspireContext;
			var now = DateTime.Now;
			var oldMarksSheetJSON = marksSheet.ToJson();
			var offeredCourse = aspireContext.OfferedCourses.Single(oc => oc.OfferedCourseID == marksSheet.OfferedCourse.OfferedCourseID);
			if (offeredCourse.SubmittedToHODDate != null)
				throw new InvalidOperationException($"Marks already submitted to HOD. {nameof(offeredCourse.OfferedCourseID)}: {offeredCourse.OfferedCourseID}.");
			if (offeredCourse.SubmittedToCampusDate != null)
				throw new InvalidOperationException($"Marks already submitted to Campus. {nameof(offeredCourse.OfferedCourseID)}: {offeredCourse.OfferedCourseID}.");
			if (offeredCourse.SubmittedToUniversityDate != null)
				throw new InvalidOperationException($"Marks already submitted to BUHO Exams. {nameof(offeredCourse.OfferedCourseID)}: {offeredCourse.OfferedCourseID}.");

			if (!offeredCourse.MarksEntryCompleted)
				throw new InvalidOperationException($"Mark Entry must be Completed. {nameof(offeredCourse.OfferedCourseID)}: {offeredCourse.OfferedCourseID}.");
			if (offeredCourse.MarksEntryLocked)
				throw new InvalidOperationException($"Mark Entry must be unlocked. {nameof(offeredCourse.OfferedCourseID)}: {offeredCourse.OfferedCourseID}.");

			var registeredCourses = MarksSheetHelperMethods.GetRegisteredCourses(aspireContext, marksSheet);
			LockMarksEntry(aspireContext, marksSheet, offeredCourse, now, registeredCourses);

			offeredCourse.SubmittedToHODDate = now;
			offeredCourse.SubmittedToHODByUserLoginHistoryID = marksSheet.UserLoginHistoryID;
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			var newMarksSheetJSON = ((IAspireContext)marksSheet).ReloadAndToJson(out _);
			aspireContext.MarksSheetChanges.Add(new MarksSheetChanx
			{
				MarksSheetChangesID = Guid.NewGuid(),
				ActionTypeEnum = MarksSheetChanx.ActionTypes.SubmitToHOD,
				ActionDate = now,
				LoginSessionGuid = marksSheet.LoginSessionGuid,
				OfferedCourseID = marksSheet.OfferedCourse.OfferedCourseID,
				OldMarksSheetJSON = oldMarksSheetJSON,
				NewMarksSheetJSON = newMarksSheetJSON,
				ManagementEmailRecipientsEnum = null
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			return (new string[] { }, true);
		}

		public static (IEnumerable<string> submitToCampusFailureReasons, bool success) SubmitToCampus(IMarksSheet marksSheet)
		{
			var aspireContext = ((IAspireContext)marksSheet).AspireContext;
			var now = DateTime.Now;
			var oldMarksSheetJSON = marksSheet.ToJson();
			var offeredCourse = aspireContext.OfferedCourses.Single(oc => oc.OfferedCourseID == marksSheet.OfferedCourse.OfferedCourseID);
			if (offeredCourse.SubmittedToCampusDate != null)
				throw new InvalidOperationException($"Marks already submitted to Campus. {nameof(offeredCourse.OfferedCourseID)}: {offeredCourse.OfferedCourseID}.");
			if (offeredCourse.SubmittedToUniversityDate != null)
				throw new InvalidOperationException($"Marks already submitted to BUHO Exams. {nameof(offeredCourse.OfferedCourseID)}: {offeredCourse.OfferedCourseID}.");

			if (offeredCourse.SubmittedToHODDate != null)
			{
				if (!offeredCourse.MarksEntryCompleted)
					throw new InvalidOperationException($"Mark Entry must be Completed. {nameof(offeredCourse.OfferedCourseID)}: {offeredCourse.OfferedCourseID}");
				if (!offeredCourse.MarksEntryLocked)
					throw new InvalidOperationException($"Mark Entry must be Locked. {nameof(offeredCourse.OfferedCourseID)}: {offeredCourse.OfferedCourseID}");
			}
			else
			{
				offeredCourse.MarksEntryCompleted = false;
				offeredCourse.MarksEntryLocked = true;
				offeredCourse.MarksEntryLockedDate = now;
			}

			var registeredCourses = MarksSheetHelperMethods.GetRegisteredCourses(aspireContext, marksSheet);
			registeredCourses.ForEach(rc =>
			{
				if (!rc.MarksEntryLocked)
				{
					rc.MarksEntryLocked = true;
					rc.MarksEntryLockedDate = now;
				}
			});

			offeredCourse.SubmittedToCampusDate = now;
			offeredCourse.SubmittedToCampusByUserLoginHistoryID = marksSheet.UserLoginHistoryID;
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			var newMarksSheetJSON = ((IAspireContext)marksSheet).ReloadAndToJson(out _);
			aspireContext.MarksSheetChanges.Add(new MarksSheetChanx
			{
				MarksSheetChangesID = Guid.NewGuid(),
				ActionTypeEnum = MarksSheetChanx.ActionTypes.SubmitToCampus,
				ActionDate = now,
				LoginSessionGuid = marksSheet.LoginSessionGuid,
				OfferedCourseID = marksSheet.OfferedCourse.OfferedCourseID,
				OldMarksSheetJSON = oldMarksSheetJSON,
				NewMarksSheetJSON = newMarksSheetJSON,
				ManagementEmailRecipientsEnum = null
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			return (new string[] { }, true);
		}

		public static (IEnumerable<string> submitToUniversityFailureReasons, bool success) SubmitToUniversity(IMarksSheet marksSheet)
		{
			var aspireContext = ((IAspireContext)marksSheet).AspireContext;
			var now = DateTime.Now;
			var oldMarksSheetJSON = marksSheet.ToJson();
			var offeredCourse = aspireContext.OfferedCourses.Single(oc => oc.OfferedCourseID == marksSheet.OfferedCourse.OfferedCourseID);
			if (offeredCourse.SubmittedToUniversityDate != null)
				throw new InvalidOperationException($"Mark Entry already submitted to BUHO Exams. {nameof(offeredCourse.OfferedCourseID)}: {offeredCourse.OfferedCourseID}.");

			if (!offeredCourse.MarksEntryLocked || !offeredCourse.MarksEntryCompleted)
			{
				offeredCourse.MarksEntryCompleted = false;
				offeredCourse.MarksEntryLocked = true;
				offeredCourse.MarksEntryLockedDate = now;
			}

			var registeredCourses = MarksSheetHelperMethods.GetRegisteredCourses(aspireContext, marksSheet);
			registeredCourses.ForEach(rc =>
			{
				if (rc.MarksEntryLocked == false)
				{
					rc.MarksEntryLocked = true;
					rc.MarksEntryLockedDate = now;
				}
			});

			offeredCourse.SubmittedToUniversityDate = now;
			offeredCourse.SubmittedToUniversityByUserLoginHistoryID = marksSheet.UserLoginHistoryID;
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			var newMarksSheetJSON = ((IAspireContext)marksSheet).ReloadAndToJson(out _);
			aspireContext.MarksSheetChanges.Add(new MarksSheetChanx
			{
				MarksSheetChangesID = Guid.NewGuid(),
				ActionTypeEnum = MarksSheetChanx.ActionTypes.SubmitToUniversity,
				ActionDate = now,
				LoginSessionGuid = marksSheet.LoginSessionGuid,
				OfferedCourseID = marksSheet.OfferedCourse.OfferedCourseID,
				OldMarksSheetJSON = oldMarksSheetJSON,
				NewMarksSheetJSON = newMarksSheetJSON,
				ManagementEmailRecipientsEnum = null
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			return (new string[] { }, true);
		}

		public static (IEnumerable<string> revertSubmitToHODFailureReasons, bool success) RevertSubmitToHOD(IMarksSheet marksSheet)
		{
			var aspireContext = ((IAspireContext)marksSheet).AspireContext;
			var now = DateTime.Now;
			var oldMarksSheetJSON = marksSheet.ToJson();
			var offeredCourse = aspireContext.OfferedCourses.Single(oc => oc.OfferedCourseID == marksSheet.OfferedCourse.OfferedCourseID);
			if (offeredCourse.SubmittedToUniversityDate != null)
				throw new InvalidOperationException($"Marks submitted to BUHO Exams. {nameof(offeredCourse.OfferedCourseID)}: {offeredCourse.OfferedCourseID}.");
			if (offeredCourse.SubmittedToCampusDate != null)
				throw new InvalidOperationException($"Marks submitted to Campus. {nameof(offeredCourse.OfferedCourseID)}: {offeredCourse.OfferedCourseID}.");
			if (offeredCourse.SubmittedToHODDate == null)
				throw new InvalidOperationException($"Marks not submitted to HOD. {nameof(offeredCourse.OfferedCourseID)}: {offeredCourse.OfferedCourseID}.");

			var registeredCourses = MarksSheetHelperMethods.GetRegisteredCourses(aspireContext, marksSheet);
			LockMarksEntry(aspireContext, marksSheet, offeredCourse, now, registeredCourses);

			offeredCourse.MarksEntryCompleted = false;
			offeredCourse.MarksEntryLocked = false;
			offeredCourse.MarksEntryLockedDate = null;
			offeredCourse.SubmittedToHODDate = null;
			offeredCourse.SubmittedToHODByUserLoginHistoryID = null;
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			var newMarksSheetJSON = ((IAspireContext)marksSheet).ReloadAndToJson(out _);
			aspireContext.MarksSheetChanges.Add(new MarksSheetChanx
			{
				MarksSheetChangesID = Guid.NewGuid(),
				ActionTypeEnum = MarksSheetChanx.ActionTypes.RevertSubmitToHOD,
				ActionDate = now,
				LoginSessionGuid = marksSheet.LoginSessionGuid,
				OfferedCourseID = marksSheet.OfferedCourse.OfferedCourseID,
				OldMarksSheetJSON = oldMarksSheetJSON,
				NewMarksSheetJSON = newMarksSheetJSON,
				ManagementEmailRecipientsEnum = MarksSheetChanx.ManagementEmailRecipients.BeforeSubmitToUniversity
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			return (new string[] { }, true);
		}

		public static (IEnumerable<string> revertSubmitToCampusFailureReasons, bool success) RevertSubmitToCampus(IMarksSheet marksSheet)
		{
			var aspireContext = ((IAspireContext)marksSheet).AspireContext;
			var now = DateTime.Now;
			var oldMarksSheetJSON = marksSheet.ToJson();
			var offeredCourse = aspireContext.OfferedCourses.Single(oc => oc.OfferedCourseID == marksSheet.OfferedCourse.OfferedCourseID);
			if (offeredCourse.SubmittedToUniversityDate != null)
				throw new InvalidOperationException($"Marks already submitted to BUHO Exams. {nameof(offeredCourse.OfferedCourseID)}: {offeredCourse.OfferedCourseID}.");
			if (offeredCourse.SubmittedToCampusDate == null)
				throw new InvalidOperationException($"Marks not submitted to Campus. {nameof(offeredCourse.OfferedCourseID)}: {offeredCourse.OfferedCourseID}.");

			offeredCourse.MarksEntryCompleted = false;
			if (!offeredCourse.MarksEntryLocked)
			{
				offeredCourse.MarksEntryLocked = true;
				offeredCourse.MarksEntryLockedDate = now;
			}

			var registeredCourses = MarksSheetHelperMethods.GetRegisteredCourses(aspireContext, marksSheet);
			registeredCourses.ForEach(rc =>
			{
				if (!rc.MarksEntryLocked)
				{
					rc.MarksEntryLocked = true;
					rc.MarksEntryLockedDate = now;
				}
			});

			offeredCourse.SubmittedToCampusDate = null;
			offeredCourse.SubmittedToCampusByUserLoginHistoryID = null;
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			var newMarksSheetJSON = ((IAspireContext)marksSheet).ReloadAndToJson(out _);
			aspireContext.MarksSheetChanges.Add(new MarksSheetChanx
			{
				MarksSheetChangesID = Guid.NewGuid(),
				ActionTypeEnum = MarksSheetChanx.ActionTypes.RevertSubmitToCampus,
				ActionDate = now,
				LoginSessionGuid = marksSheet.LoginSessionGuid,
				OfferedCourseID = marksSheet.OfferedCourse.OfferedCourseID,
				OldMarksSheetJSON = oldMarksSheetJSON,
				NewMarksSheetJSON = newMarksSheetJSON,
				ManagementEmailRecipientsEnum = MarksSheetChanx.ManagementEmailRecipients.BeforeSubmitToUniversity
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			return (new string[] { }, true);
		}

		public static (IEnumerable<string> revertSubmitToUniversityFailureReasons, bool success) RevertSubmitToUniversity(IMarksSheet marksSheet)
		{
			var aspireContext = ((IAspireContext)marksSheet).AspireContext;
			var now = DateTime.Now;
			var oldMarksSheetJSON = marksSheet.ToJson();
			var offeredCourse = aspireContext.OfferedCourses.Single(oc => oc.OfferedCourseID == marksSheet.OfferedCourse.OfferedCourseID);
			if (offeredCourse.SubmittedToUniversityDate == null)
				throw new InvalidOperationException($"Mark Entry not submitted to BUHO Exams. {nameof(offeredCourse.OfferedCourseID)}: {offeredCourse.OfferedCourseID}.");

			offeredCourse.MarksEntryCompleted = false;
			if (!offeredCourse.MarksEntryLocked)
			{
				offeredCourse.MarksEntryLocked = true;
				offeredCourse.MarksEntryLockedDate = now;
			}

			var registeredCourses = MarksSheetHelperMethods.GetRegisteredCourses(aspireContext, marksSheet);
			registeredCourses.ForEach(rc =>
			{
				if (!rc.MarksEntryLocked)
				{
					rc.MarksEntryLocked = true;
					rc.MarksEntryLockedDate = now;
				}
			});

			offeredCourse.SubmittedToUniversityDate = null;
			offeredCourse.SubmittedToUniversityByUserLoginHistoryID = null;
			if (offeredCourse.SubmittedToCampusDate == null)
			{
				offeredCourse.SubmittedToCampusDate = now;
				offeredCourse.SubmittedToCampusByUserLoginHistoryID = marksSheet.UserLoginHistoryID;
			}
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			var newMarksSheetJSON = ((IAspireContext)marksSheet).ReloadAndToJson(out _);
			aspireContext.MarksSheetChanges.Add(new MarksSheetChanx
			{
				MarksSheetChangesID = Guid.NewGuid(),
				ActionTypeEnum = MarksSheetChanx.ActionTypes.RevertSubmitToUniversity,
				ActionDate = now,
				LoginSessionGuid = marksSheet.LoginSessionGuid,
				OfferedCourseID = marksSheet.OfferedCourse.OfferedCourseID,
				OldMarksSheetJSON = oldMarksSheetJSON,
				NewMarksSheetJSON = newMarksSheetJSON,
				ManagementEmailRecipientsEnum = MarksSheetChanx.ManagementEmailRecipients.AfterSubmitToUniversity
			});
			aspireContext.SaveChanges(marksSheet.UserLoginHistoryID);
			return (new string[] { }, true);
		}
	}
}
