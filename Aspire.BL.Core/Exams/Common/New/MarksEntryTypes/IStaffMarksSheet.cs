﻿namespace Aspire.BL.Core.Exams.Common.New.MarksEntryTypes
{
	internal interface IStaffMarksSheet : IMarksSheet
	{
		bool UniversityLevelMarksEntryAllowed { get; }
		bool CanUnlockCampusLevelMarksEntry { get; }
		bool CampusLevelMarksEntryAllowed { get; }
		bool DepartmentLevelMarksEntryAllowed { get; }
		bool IsPermittedToRevertSubmitToHOD { get; }
		bool IsPermittedToRevertSubmitToCampus { get; }
		bool IsPermittedToRevertSubmitToUniversity { get; }
	}
}
