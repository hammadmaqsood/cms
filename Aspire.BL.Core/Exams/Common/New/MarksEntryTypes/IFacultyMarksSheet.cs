﻿using Aspire.Model.Entities.Common;

namespace Aspire.BL.Core.Exams.Common.New.MarksEntryTypes
{
	internal interface IFacultyMemberMarksSheet : IMarksSheet
	{
		ExamMarksTypes? ExamMarksTypeEnum { get; }
	}
}
