﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.New.MarksEntryTypes
{
	internal static class StaffHelperMethods
	{
		public static (bool UniversityLevelMarksEntryAllowed, bool CanUnlockCampusLevelMarksEntryAllowed, bool CampusLevelMarksEntryAllowed, bool departmentLevelMarksEntryAllowed, bool canRevertSubmitToHOD, bool canRevertSubmitToCampus, bool canRevertSubmitToUniversity) GetPermissions(AspireContext aspireContext, Guid loginSessionGuid, IStaffMarksSheet staffMarksSheet)
		{
			var universityLevelMarksEntry = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.UniversityLevelMarksEntry, UserGroupPermission.PermissionValues.Allowed, staffMarksSheet.OfferedCourse.InstituteID, staffMarksSheet.OfferedCourse.DepartmentID, staffMarksSheet.OfferedCourse.ProgramID, staffMarksSheet.OfferedCourse.AdmissionOpenProgramID);
			var canUnlockCampusLevelMarksEntry = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.CanUnlockCampusLevelMarksEntry, UserGroupPermission.PermissionValues.Allowed, staffMarksSheet.OfferedCourse.InstituteID, staffMarksSheet.OfferedCourse.DepartmentID, staffMarksSheet.OfferedCourse.ProgramID, staffMarksSheet.OfferedCourse.AdmissionOpenProgramID);
			var campusLevelMarksEntry = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.CampusLevelMarksEntry, UserGroupPermission.PermissionValues.Allowed, staffMarksSheet.OfferedCourse.InstituteID, staffMarksSheet.OfferedCourse.DepartmentID, staffMarksSheet.OfferedCourse.ProgramID, staffMarksSheet.OfferedCourse.AdmissionOpenProgramID);
			var departmentLevelMarksEntry = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.DepartmentLevelMarksEntry, UserGroupPermission.PermissionValues.Allowed, staffMarksSheet.OfferedCourse.InstituteID, staffMarksSheet.OfferedCourse.DepartmentID, staffMarksSheet.OfferedCourse.ProgramID, staffMarksSheet.OfferedCourse.AdmissionOpenProgramID);
			var canRevertSubmitToHOD = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.CanRevertSubmitToHOD, UserGroupPermission.PermissionValues.Allowed, staffMarksSheet.OfferedCourse.InstituteID, staffMarksSheet.OfferedCourse.DepartmentID, staffMarksSheet.OfferedCourse.ProgramID, staffMarksSheet.OfferedCourse.AdmissionOpenProgramID)?.PermissionValueEnum == UserGroupPermission.PermissionValues.Allowed;
			var canRevertSubmitToCampus = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.CanRevertSubmitToCampus, UserGroupPermission.PermissionValues.Allowed, staffMarksSheet.OfferedCourse.InstituteID, staffMarksSheet.OfferedCourse.DepartmentID, staffMarksSheet.OfferedCourse.ProgramID, staffMarksSheet.OfferedCourse.AdmissionOpenProgramID)?.PermissionValueEnum == UserGroupPermission.PermissionValues.Allowed;
			var canRevertSubmitToUniversity = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.CanRevertSubmitToUniversity, UserGroupPermission.PermissionValues.Allowed, staffMarksSheet.OfferedCourse.InstituteID, staffMarksSheet.OfferedCourse.DepartmentID, staffMarksSheet.OfferedCourse.ProgramID, staffMarksSheet.OfferedCourse.AdmissionOpenProgramID)?.PermissionValueEnum == UserGroupPermission.PermissionValues.Allowed;
			var universityLevelMarksEntryAllowed = universityLevelMarksEntry?.PermissionValueEnum == UserGroupPermission.PermissionValues.Allowed;
			var campusLevelMarksEntryAllowed = campusLevelMarksEntry?.PermissionValueEnum == UserGroupPermission.PermissionValues.Allowed;
			var canUnlockCampusLevelMarksEntryAllowed = canUnlockCampusLevelMarksEntry?.PermissionValueEnum == UserGroupPermission.PermissionValues.Allowed;
			var departmentLevelMarksEntryAllowed = departmentLevelMarksEntry?.PermissionValueEnum == UserGroupPermission.PermissionValues.Allowed;
			return (universityLevelMarksEntryAllowed, canUnlockCampusLevelMarksEntryAllowed, campusLevelMarksEntryAllowed, departmentLevelMarksEntryAllowed, canRevertSubmitToHOD, canRevertSubmitToCampus, canRevertSubmitToUniversity);
		}

		private static IEnumerable<string> EmptyReasons => Enumerable.Empty<string>();

		public static IEnumerable<string> CanNotAddAssignmentQuizReasons(IStaffMarksSheet staffMarksSheet, Model.Entities.OfferedCourseExam.ExamTypes examTypeEnum)
		{
			var offeredCourse = staffMarksSheet.OfferedCourse;
			if (offeredCourse.MarksEntryLocked)
				return $"You are not authorized to add {examTypeEnum.ToFullName()} Marks because Marks Entry has been locked.".ToEnumerable();
			if (offeredCourse.SubmittedToUniversityDate != null)
			{
				return !staffMarksSheet.UniversityLevelMarksEntryAllowed
					? $"You are not authorized to add {examTypeEnum.ToFullName()} Marks because Marks have been submitted to BUHO Exams and you do not have University level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			if (staffMarksSheet.CanUnlockCampusLevelMarksEntry)
				return EmptyReasons;
			if (offeredCourse.SubmittedToCampusDate != null)
			{
				return !staffMarksSheet.CampusLevelMarksEntryAllowed
					? $"You are not authorized to add {examTypeEnum.ToFullName()} Marks because Marks have been submitted to Campus and you do not have Campus level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			if (offeredCourse.SubmittedToHODDate != null)
			{
				return !staffMarksSheet.CampusLevelMarksEntryAllowed
					? $"You are not authorized to add {examTypeEnum.ToFullName()} Marks because Marks have been submitted to HOD and you do not have Campus level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			return $"You are not authorized to add {examTypeEnum.ToFullName()} Marks because Marks are not yet submitted to HOD/Campus.".ToEnumerable();
		}

		public static IEnumerable<string> CanNotEditOrDeleteAssignmentQuizReasons(IStaffMarksSheet staffMarksSheet, Model.Entities.OfferedCourseExam.ExamTypes examTypeEnum)
		{
			var offeredCourse = staffMarksSheet.OfferedCourse;
			if (offeredCourse.MarksEntryLocked)
				return $"You are not authorized to edit/delete {examTypeEnum.ToFullName()} Marks because Marks Entry has been locked.".ToEnumerable();
			if (offeredCourse.SubmittedToUniversityDate != null)
			{
				return !staffMarksSheet.UniversityLevelMarksEntryAllowed
					? $"You are not authorized to edit/delete {examTypeEnum.ToFullName()} Marks because Marks have been submitted to BUHO Exams and you do not have University level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			if (staffMarksSheet.CanUnlockCampusLevelMarksEntry)
				return EmptyReasons;
			if (offeredCourse.SubmittedToCampusDate != null)
			{
				return !staffMarksSheet.CampusLevelMarksEntryAllowed
					? $"You are not authorized to edit/delete {examTypeEnum.ToFullName()} Marks because Marks have been submitted to Campus and you do not have Campus level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			if (offeredCourse.SubmittedToHODDate != null)
			{
				return !staffMarksSheet.CampusLevelMarksEntryAllowed
					? $"You are not authorized to edit/delete {examTypeEnum.ToFullName()} Marks because Marks have been submitted to HOD and you do not have Campus level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			return $"You are not authorized to edit/delete {examTypeEnum.ToFullName()} Marks because Marks are not yet submitted to HOD/Campus.".ToEnumerable();
		}

		public static IEnumerable<string> CanNotUnlockAssignmentQuizReasons(int offeredCourseExamID, IStaffMarksSheet staffMarksSheet, Model.Entities.OfferedCourseExam.ExamTypes examTypeEnum)
		{
			var offeredCourse = staffMarksSheet.OfferedCourse;
			if (offeredCourse.MarksEntryLocked)
				return $"{examTypeEnum.ToFullName()} Marks cannot be unlocked because Marks Entry has been locked.".ToEnumerable();
			var offeredCourseExam = offeredCourse.OfferedCourseExams.Single(oce => oce.OfferedCourseExamID == offeredCourseExamID && oce.ExamTypeEnum == examTypeEnum);
			if (!offeredCourseExam.Locked)
				return $"{examTypeEnum.ToFullName()} No. {offeredCourseExam.ExamNo} is not locked.".ToEnumerable();
			if (offeredCourse.SubmittedToUniversityDate != null)
				return $"{examTypeEnum.ToFullName()} Marks cannot be unlocked because Marks have been submitted to BUHO Exams.".ToEnumerable();
			if (staffMarksSheet.CanUnlockCampusLevelMarksEntry)
				return EmptyReasons;
			if (offeredCourse.SubmittedToCampusDate != null)
				return $"{examTypeEnum.ToFullName()} Marks cannot be unlocked because Marks have been submitted to Campus.".ToEnumerable();
			if (offeredCourse.SubmittedToHODDate != null)
				return $"{examTypeEnum.ToFullName()} Marks cannot be unlocked because Marks have been submitted to HOD.".ToEnumerable();

			return $"{examTypeEnum.ToFullName()} Marks cannot be unlocked because you are not authorized to unlock Marks Entry.".ToEnumerable();
		}

		public static IEnumerable<string> CanNotUnlockAssignmentsQuizzesInternalsMidFinalReasons(IStaffMarksSheet staffMarksSheet, ExamMarksTypes examMarksTypeEnum)
		{
			var offeredCourse = staffMarksSheet.OfferedCourse;
			if (offeredCourse.MarksEntryLocked)
				return $"{examMarksTypeEnum.ToFullName()} Marks cannot be unlocked because Marks Entry has been locked.".ToEnumerable();
			switch (examMarksTypeEnum)
			{
				case ExamMarksTypes.Assignments:
					if (!offeredCourse.AssignmentsMarksSubmitted)
						return $"{examMarksTypeEnum.ToFullName()} marks are not locked.".ToEnumerable();
					break;
				case ExamMarksTypes.Quizzes:
					if (!offeredCourse.QuizzesMarksSubmitted)
						return $"{examMarksTypeEnum.ToFullName()} marks are not locked.".ToEnumerable();
					break;
				case ExamMarksTypes.Internals:
					if (!offeredCourse.InternalsMarksSubmitted)
						return $"{examMarksTypeEnum.ToFullName()} marks are not locked.".ToEnumerable();
					break;
				case ExamMarksTypes.Mid:
					if (!offeredCourse.MidMarksSubmitted)
						return $"{examMarksTypeEnum.ToFullName()} marks are not locked.".ToEnumerable();
					break;
				case ExamMarksTypes.Final:
					if (!offeredCourse.FinalMarksSubmitted)
						return $"{examMarksTypeEnum.ToFullName()} marks are not locked.".ToEnumerable();
					break;
				default:
					throw new NotImplementedEnumException(examMarksTypeEnum);
			}
			if (offeredCourse.SubmittedToUniversityDate != null)
				return $"{examMarksTypeEnum.ToFullName()} Marks cannot be unlocked because Marks have been submitted to BUHO Exams.".ToEnumerable();
			if (staffMarksSheet.CanUnlockCampusLevelMarksEntry)
				return EmptyReasons;
			if (offeredCourse.SubmittedToCampusDate != null)
				return $"{examMarksTypeEnum.ToFullName()} Marks cannot be unlocked because Marks have been submitted to Campus.".ToEnumerable();
			if (offeredCourse.SubmittedToHODDate != null)
				return $"{examMarksTypeEnum.ToFullName()} Marks cannot be unlocked because Marks have been submitted to HOD.".ToEnumerable();

			return $"{examMarksTypeEnum.ToFullName()} Marks cannot be unlocked because you are not authorized to unlock Marks Entry.".ToEnumerable();
		}

		public static IEnumerable<string> CanNotCompileAssignmentsQuizzesInternalsReasons(IStaffMarksSheet staffMarksSheet, CompileExamMarksTypes compileExamMarksTypeEnum)
		{
			var offeredCourse = staffMarksSheet.OfferedCourse;
			if (offeredCourse.MarksEntryLocked)
				return $"You are not authorized to compile {compileExamMarksTypeEnum.ToFullName()} Marks because Marks Entry has been locked.".ToEnumerable();
			if (offeredCourse.SubmittedToUniversityDate != null)
			{
				return !staffMarksSheet.UniversityLevelMarksEntryAllowed
					? $"You are not authorized to compile {compileExamMarksTypeEnum.ToFullName()} Marks because Marks have been submitted to BUHO Exams and you do not have University level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			if (staffMarksSheet.CanUnlockCampusLevelMarksEntry)
				return EmptyReasons;
			if (offeredCourse.SubmittedToCampusDate != null)
			{
				return !staffMarksSheet.CampusLevelMarksEntryAllowed
					? $"You are not authorized to compile {compileExamMarksTypeEnum.ToFullName()} Marks because Marks have been submitted to Campus and you do not have Campus level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			if (offeredCourse.SubmittedToHODDate != null)
			{
				return !staffMarksSheet.CampusLevelMarksEntryAllowed
					? $"You are not authorized to compile {compileExamMarksTypeEnum.ToFullName()} Marks because Marks have been submitted to HOD and you do not have Campus level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			return $"You are not authorized to compile {compileExamMarksTypeEnum.ToFullName()} Marks because Marks are not yet submitted to HOD/Campus.".ToEnumerable();
		}

		public static IEnumerable<string> CanNotEditAssignmentsQuizzesMidFinalReasons(IStaffMarksSheet staffMarksSheet, ExamMarksTypes examMarksTypeEnum)
		{
			var offeredCourse = staffMarksSheet.OfferedCourse;
			if (offeredCourse.MarksEntryLocked)
				return $"You are not authorized to edit {examMarksTypeEnum.ToFullName()} Marks because Marks Entry has been locked.".ToEnumerable();
			if (offeredCourse.SubmittedToUniversityDate != null)
			{
				return !staffMarksSheet.UniversityLevelMarksEntryAllowed
					? $"You are not authorized to edit {examMarksTypeEnum.ToFullName()} Marks because Marks have been submitted to BUHO Exams and you do not have University level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			if (staffMarksSheet.CanUnlockCampusLevelMarksEntry)
				return EmptyReasons;
			if (offeredCourse.SubmittedToCampusDate != null)
			{
				return !staffMarksSheet.CampusLevelMarksEntryAllowed
					? $"You are not authorized to edit {examMarksTypeEnum.ToFullName()} Marks because Marks have been submitted to Campus and you do not have Campus level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			if (offeredCourse.SubmittedToHODDate != null)
			{
				return !staffMarksSheet.CampusLevelMarksEntryAllowed
					? $"You are not authorized to edit {examMarksTypeEnum.ToFullName()} Marks because Marks have been submitted to HOD and you do not have Campus level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			return $"You are not authorized to edit {examMarksTypeEnum.ToFullName()} Marks because Marks are not yet submitted to HOD/Campus.".ToEnumerable();
		}

		public static IEnumerable<string> CanNotCompileGradesReasons(IStaffMarksSheet staffMarksSheet)
		{
			var offeredCourse = staffMarksSheet.OfferedCourse;
			if (offeredCourse.MarksEntryLocked)
				return "You are not authorized to compile Total/Grades because Marks Entry has been locked.".ToEnumerable();
			if (offeredCourse.MarksSheet.PendingMarksEntry.Any())
				return offeredCourse.MarksSheet.PendingMarksEntry;
			if (offeredCourse.SubmittedToUniversityDate != null)
			{
				return !staffMarksSheet.UniversityLevelMarksEntryAllowed
					? "You are not authorized to compile Total/Grades because Marks have been submitted to BUHO Exams and you do not have University level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			if (staffMarksSheet.CanUnlockCampusLevelMarksEntry)
				return EmptyReasons;
			if (offeredCourse.SubmittedToCampusDate != null)
			{
				return !staffMarksSheet.CampusLevelMarksEntryAllowed
					? "You are not authorized to compile Total/Grades because Marks have been submitted to Campus and you do not have Campus level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			if (offeredCourse.SubmittedToHODDate != null)
			{
				return !staffMarksSheet.CampusLevelMarksEntryAllowed
					? "You are not authorized to compile Total/Grades because Marks have been submitted to HOD and you do not have Campus level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			return "You are not authorized to compile Total/Grades because Marks are not yet submitted to HOD/Campus.".ToEnumerable();
		}

		public static IEnumerable<string> CanNotLockMarksEntryReasons(IStaffMarksSheet staffMarksSheet)
		{
			var offeredCourse = staffMarksSheet.OfferedCourse;
			if (offeredCourse.MarksEntryLocked)
				return "Marks Entry is already locked.".ToEnumerable();
			if (offeredCourse.MarksSheet.PendingMarksEntry.Any())
				return offeredCourse.MarksSheet.PendingMarksEntry;
			if (offeredCourse.SubmittedToUniversityDate != null)
			{
				return !staffMarksSheet.UniversityLevelMarksEntryAllowed
					? "You are not authorized to lock Marks Entry because Marks have been submitted to BUHO Exams and you do not have University level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			if (staffMarksSheet.CanUnlockCampusLevelMarksEntry)
				return EmptyReasons;
			if (offeredCourse.SubmittedToCampusDate != null)
			{
				return !staffMarksSheet.CampusLevelMarksEntryAllowed
					? "You are not authorized to lock Marks Entry because Marks have been submitted to Campus and you do not have Campus level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			if (offeredCourse.SubmittedToHODDate != null)
			{
				return !staffMarksSheet.CampusLevelMarksEntryAllowed
					? "You are not authorized to lock Marks Entry because Marks have been submitted to HOD and you do not have Campus level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			return "You are not authorized to lock Marks Entry because Marks are not yet submitted to HOD/Campus.".ToEnumerable();
		}

		public static IEnumerable<string> CanNotUnlockMarksEntryReasons(IStaffMarksSheet staffMarksSheet)
		{
			var offeredCourse = staffMarksSheet.OfferedCourse;
			if (!offeredCourse.MarksEntryLocked)
				return "Marks Entry is not locked.".ToEnumerable();
			if (offeredCourse.SubmittedToUniversityDate != null)
			{
				return !staffMarksSheet.UniversityLevelMarksEntryAllowed
					? "You are not authorized to unlock Marks Entry because Marks have been submitted to BUHO Exams and you do not have University level permissions.".ToEnumerable()
					: EmptyReasons;
			}
			if (staffMarksSheet.CanUnlockCampusLevelMarksEntry)
				return EmptyReasons;
			if (offeredCourse.SubmittedToHODDate != null && offeredCourse.SubmittedToCampusDate == null)
				return "Marks Entry cannot be unlock because Marks have been submitted to HoD and HoD cannot edit any marks. Submit it to Campus first.".ToEnumerable();
			if (staffMarksSheet.CanUnlockCampusLevelMarksEntry)
				return EmptyReasons;
			return "You are not authorized to unlock Marks Entry because you do not have permissions to Unlock Marks Entry at Campus Level.".ToEnumerable();
		}

		public static IEnumerable<string> CanNotSubmitToHODReasons(IStaffMarksSheet staffMarksSheet)
		{
			if (staffMarksSheet.OfferedCourse.SubmittedToHODDate != null)
				return "Marks are already submitted to HOD.".ToEnumerable();
			if (staffMarksSheet.CanUnlockCampusLevelMarksEntry)
				return EmptyReasons;
			return "Only Faculty Member can submit marks to HOD.".ToEnumerable();
		}

		public static IEnumerable<string> CanNotSubmitToCampusReasons(IStaffMarksSheet staffMarksSheet)
		{
			if (staffMarksSheet.OfferedCourse.SubmittedToCampusDate != null)
				return "Marks are already submitted to Campus.".ToEnumerable();
			if (staffMarksSheet.OfferedCourse.SubmittedToUniversityDate != null)
				return "Marks cannot be submitted to Campus because Marks are already submitted to BUHO Exams.".ToEnumerable();
			if (staffMarksSheet.CanUnlockCampusLevelMarksEntry)
				return EmptyReasons;
			if (staffMarksSheet.DepartmentLevelMarksEntryAllowed)
				return EmptyReasons;
			return "You are not authorized to Submit Marks to Campus because you do not have a Department Level Permissions.".ToEnumerable();
		}

		public static IEnumerable<string> CanNotSubmitToUniversityReasons(IStaffMarksSheet staffMarksSheet)
		{
			if (staffMarksSheet.OfferedCourse.SubmittedToUniversityDate != null)
				return "Marks are already submitted to BUHO Exams.".ToEnumerable();
			if (staffMarksSheet.CanUnlockCampusLevelMarksEntry)
				return EmptyReasons;
			if (staffMarksSheet.CampusLevelMarksEntryAllowed)
				return EmptyReasons;
			return "You are not authorized to Submit Marks to BUHO Exams because you do not have a Campus Level Permissions.".ToEnumerable();
		}

		public static IEnumerable<string> CanNotRevertSubmitToHODReasons(IStaffMarksSheet staffMarksSheet)
		{
			if (staffMarksSheet.OfferedCourse.SubmittedToHODDate == null)
				return "Marks are not submitted to HOD.".ToEnumerable();
			if (staffMarksSheet.OfferedCourse.SubmittedToUniversityDate != null)
				return "Marks Submission to HOD cannot be reverted because Marks have been submitted to BUHO Exams.".ToEnumerable();
			if (staffMarksSheet.OfferedCourse.SubmittedToCampusDate != null)
				return "Marks Submission to HOD cannot be reverted because Marks have been submitted to Campus.".ToEnumerable();
			if (staffMarksSheet.IsPermittedToRevertSubmitToHOD)
				return EmptyReasons;
			return "You are not authorized to revert submission of Marks to HOD.".ToEnumerable();
		}

		public static IEnumerable<string> CanNotRevertSubmitToCampusReasons(IStaffMarksSheet staffMarksSheet)
		{
			if (staffMarksSheet.OfferedCourse.SubmittedToCampusDate == null)
				return "Marks are not submitted to Campus.".ToEnumerable();
			if (staffMarksSheet.OfferedCourse.SubmittedToUniversityDate != null)
				return "Marks Submission to Campus cannot be reverted because Marks have been submitted to BUHO Exams.".ToEnumerable();
			if (staffMarksSheet.IsPermittedToRevertSubmitToCampus)
				return EmptyReasons;
			return "You are not authorized to revert submission of Marks to Campus.".ToEnumerable();
		}

		public static IEnumerable<string> CanNotRevertSubmitToUniversityReasons(IStaffMarksSheet staffMarksSheet)
		{
			if (staffMarksSheet.OfferedCourse.SubmittedToUniversityDate == null)
				return "Marks are not submitted to BUHO Exams.".ToEnumerable();
			if (staffMarksSheet.IsPermittedToRevertSubmitToUniversity)
				return EmptyReasons;
			return "You are not authorized to revert submission of Marks to BUHO Exams.".ToEnumerable();
		}
	}
}
