﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.New.MarksEntryTypes
{
	internal abstract class BaseAssignmentsQuizzesMidFinalMarksSheet : BaseMarksSheet
	{
		protected BaseAssignmentsQuizzesMidFinalMarksSheet(AspireContext aspireContext, int offeredCourseID, int userLoginHistoryID, Guid loginSessionGuid) : base(aspireContext, offeredCourseID, userLoginHistoryID, loginSessionGuid)
		{
			this.Display = New.Display.CreateAssignmentsQuizzesMidFinal(this);
		}

		public sealed override Model.Entities.ExamMarksPolicy.MarksEntryTypes MarksEntryTypeEnum => Model.Entities.ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal;

		public override IDisplay Display { get; }

		#region IAssignment

		public sealed override IEnumerable<string> CanNotAddAssignmentReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(Model.Entities.OfferedCourseExam.ExamTypes.Assignment);

		public sealed override IEnumerable<string> CanNotEditOrDeleteAssignmentReasons(int offeredCourseExamID)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(Model.Entities.OfferedCourseExam.ExamTypes.Assignment).First());
		}

		public sealed override IEnumerable<string> CanNotUnlockAssignmentReasons(int offeredCourseExamID)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(Model.Entities.OfferedCourseExam.ExamTypes.Assignment).First());
		}

		public sealed override IReadOnlyList<IStudentMarks> GetAssignmentMarks(int offeredCourseExamID)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(Model.Entities.OfferedCourseExam.ExamTypes.Assignment).First());
		}

		#endregion

		#region IAssignments

		public abstract override IEnumerable<string> CanNotEditAssignmentsReasons { get; }

		public abstract override IEnumerable<string> CanNotUnlockAssignmentsReasons { get; }

		public sealed override IEnumerable<string> CanNotCompileAssignmentsReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Assignments);

		public sealed override IReadOnlyList<IStudentMarks> GetAssignmentsMarks()
		{
			return MarksSheetHelperMethods.GetMarksList(this.OfferedCourse, ExamMarksTypes.Assignments);
		}

		public sealed override decimal? GetCompiledAssignmentsMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Assignments).First());
		}

		#endregion

		#region IQuiz

		public sealed override IEnumerable<string> CanNotAddQuizReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(Model.Entities.OfferedCourseExam.ExamTypes.Quiz);

		public sealed override IEnumerable<string> CanNotEditOrDeleteQuizReasons(int offeredCourseExamID)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(Model.Entities.OfferedCourseExam.ExamTypes.Quiz).First());
		}

		public sealed override IEnumerable<string> CanNotUnlockQuizReasons(int offeredCourseExamID)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(Model.Entities.OfferedCourseExam.ExamTypes.Quiz).First());
		}

		public sealed override IReadOnlyList<IStudentMarks> GetQuizMarks(int offeredCourseExamID)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(Model.Entities.OfferedCourseExam.ExamTypes.Quiz).First());
		}

		#endregion

		#region IQuizzes

		public abstract override IEnumerable<string> CanNotEditQuizzesReasons { get; }

		public abstract override IEnumerable<string> CanNotUnlockQuizzesReasons { get; }

		public sealed override IEnumerable<string> CanNotCompileQuizzesReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Quizzes);

		public sealed override IReadOnlyList<IStudentMarks> GetQuizzesMarks()
		{
			return MarksSheetHelperMethods.GetMarksList(this.OfferedCourse, ExamMarksTypes.Quizzes);
		}

		public sealed override decimal? GetCompiledQuizzesMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Quizzes).First());
		}

		#endregion

		#region IInternals

		public sealed override IEnumerable<string> CanNotUnlockInternalsReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(ExamMarksTypes.Internals);

		public sealed override IEnumerable<string> CanNotCompileInternalsReasons => MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Internals);

		public sealed override decimal? GetCompiledInternalsMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType)
		{
			throw new InvalidOperationException(MarksSheetHelperMethods.ExamMarksPolicyDoesNotSupport(CompileExamMarksTypes.Internals).First());
		}

		#endregion

		#region IMid

		public abstract override IEnumerable<string> CanNotEditMidReasons { get; }

		public abstract override IEnumerable<string> CanNotUnlockMidReasons { get; }

		public sealed override IReadOnlyList<IStudentMarks> GetMidMarks()
		{
			return MarksSheetHelperMethods.GetMarksList(this.OfferedCourse, ExamMarksTypes.Mid);
		}

		#endregion

		#region IFinal

		public abstract override IEnumerable<string> CanNotEditFinalReasons { get; }

		public abstract override IEnumerable<string> CanNotUnlockFinalReasons { get; }

		#endregion

		#region IMarksSubmissionFlow

		public sealed override IEnumerable<string> PendingMarksEntry
		{
			get
			{
				if (this.OfferedCourse.AssignmentsMarksSubmittedDate == null)
					yield return $"{ExamMarksTypes.Assignments.ToFullName()} Marks are not yet submitted.";
				if (this.OfferedCourse.QuizzesMarksSubmittedDate == null)
					yield return $"{ExamMarksTypes.Quizzes.ToFullName()} Marks are not yet submitted.";
				if (this.OfferedCourse.MidMarksSubmittedDate == null)
					yield return $"{ExamMarksTypes.Mid.ToFullName()} Marks are not yet submitted.";
				if (this.OfferedCourse.FinalMarksSubmittedDate == null)
					yield return $"{ExamMarksTypes.Final.ToFullName()} Marks are not yet submitted.";
			}
		}

		public abstract override IEnumerable<string> CanNotCompileGradesReasons { get; }

		public abstract override IEnumerable<string> CanNotLockMarksEntryReasons { get; }

		public sealed override void SetTotalsAndGrades(IRegisteredCourse iRegisteredCourse, RegisteredCours registeredCourse)
		{
			if (!iRegisteredCourse.CompilationAllowed)
			{
				registeredCourse.Total = null;
				registeredCourse.Grade = null;
				registeredCourse.GradePoints = null;
				registeredCourse.Product = null;
			}
			else
			{
				var total = (registeredCourse.Assignments ?? 0) + (registeredCourse.Quizzes ?? 0) + (registeredCourse.Mid ?? 0) + (registeredCourse.Final ?? 0);
				var grade = iRegisteredCourse.ExamMarksPolicyToBeApplied.GetExamGradeAndGradePoints(registeredCourse.Final, total, iRegisteredCourse);
				registeredCourse.Total = grade?.Total;
				registeredCourse.GradeEnum = grade?.ExamGradeEnum;
				registeredCourse.GradePoints = grade?.GradePoints;
				registeredCourse.Product = grade?.GradePoints * iRegisteredCourse.CreditHours;
			}
		}

		public abstract override IEnumerable<string> CanNotUnlockMarksEntryReasons { get; }

		public abstract override IEnumerable<string> CanNotSubmitToHODReasons { get; }

		public abstract override IEnumerable<string> CanNotSubmitToCampusReasons { get; }

		public abstract override IEnumerable<string> CanNotSubmitToUniversityReasons { get; }

		public abstract override IEnumerable<string> CanNotRevertSubmitToHODReasons { get; }

		public abstract override IEnumerable<string> CanNotRevertSubmitToCampusReasons { get; }

		public abstract override IEnumerable<string> CanNotRevertSubmitToUniversityReasons { get; }

		#endregion
	}
}