﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;

namespace Aspire.BL.Core.Exams.Common.New.MarksEntryTypes
{
	internal sealed class FacultyMemberAssignmentQuizCompileInternalsMidFinalMarksSheet : BaseAssignmentQuizCompileInternalsMidFinalMarksSheet, IFacultyMemberMarksSheet
	{
		internal FacultyMemberAssignmentQuizCompileInternalsMidFinalMarksSheet(AspireContext aspireContext, int offeredCourseID, int userLoginHistoryID, Guid loginSessionGuid) : base(aspireContext, offeredCourseID, userLoginHistoryID, loginSessionGuid)
		{
			this.ExamMarksTypeEnum = FacultyMemberHelperMethods.GetExamMarksSettings(aspireContext, this);
			this.Options = new Options(this);
		}

		public override string ReloadAndToJson(out MarksSheetChanx.ManagementEmailRecipients? managementEmailRecipients)
		{
			var marksSheet = new FacultyMemberAssignmentQuizCompileInternalsMidFinalMarksSheet(this.AspireContext, this.OfferedCourse.OfferedCourseID, this.UserLoginHistoryID, this.LoginSessionGuid);
			managementEmailRecipients = marksSheet.ManagementEmailRecipients;
			return marksSheet.ToJson();
		}

		public ExamMarksTypes? ExamMarksTypeEnum { get; }

		public override IOptions Options { get; }

		#region IAssignment

		public override IEnumerable<string> CanNotAddAssignmentReasons => FacultyMemberHelperMethods.CanNotAddAssignmentQuizReasons(this, Model.Entities.OfferedCourseExam.ExamTypes.Assignment, ExamMarksTypes.Internals);

		public override IEnumerable<string> CanNotEditOrDeleteAssignmentReasons(int offeredCourseExamID)
		{
			return FacultyMemberHelperMethods.CanNotEditOrDeleteAssignmentOrQuizReasons(offeredCourseExamID, this, Model.Entities.OfferedCourseExam.ExamTypes.Assignment, ExamMarksTypes.Internals);
		}

		public override IEnumerable<string> CanNotUnlockAssignmentReasons(int offeredCourseExamID)
		{
			return FacultyMemberHelperMethods.CanNotUnlockMarksReasons;
		}

		#endregion

		#region IQuiz

		public override IEnumerable<string> CanNotAddQuizReasons => FacultyMemberHelperMethods.CanNotAddAssignmentQuizReasons(this, Model.Entities.OfferedCourseExam.ExamTypes.Quiz, ExamMarksTypes.Internals);

		public override IEnumerable<string> CanNotEditOrDeleteQuizReasons(int offeredCourseExamID)
		{
			return FacultyMemberHelperMethods.CanNotEditOrDeleteAssignmentOrQuizReasons(offeredCourseExamID, this, Model.Entities.OfferedCourseExam.ExamTypes.Quiz, ExamMarksTypes.Internals);
		}

		public override IEnumerable<string> CanNotUnlockQuizReasons(int offeredCourseExamID)
		{
			return FacultyMemberHelperMethods.CanNotUnlockMarksReasons;
		}

		#endregion

		#region IInternals

		public override IEnumerable<string> CanNotUnlockInternalsReasons => FacultyMemberHelperMethods.CanNotUnlockMarksReasons;

		public override IEnumerable<string> CanNotCompileInternalsReasons => FacultyMemberHelperMethods.CanNotCompileAssignmentsQuizzesInternalsReasons(this, ExamMarksTypes.Internals);

		#endregion

		#region IMid

		public override IEnumerable<string> CanNotEditMidReasons => FacultyMemberHelperMethods.CanNotEditAssignmentsQuizzesMidFinalReasons(this, ExamMarksTypes.Mid);

		public override IEnumerable<string> CanNotUnlockMidReasons => FacultyMemberHelperMethods.CanNotUnlockMarksReasons;

		#endregion

		#region IFinal

		public override IEnumerable<string> CanNotEditFinalReasons => FacultyMemberHelperMethods.CanNotEditAssignmentsQuizzesMidFinalReasons(this, ExamMarksTypes.Final);

		public override IEnumerable<string> CanNotUnlockFinalReasons => FacultyMemberHelperMethods.CanNotUnlockMarksReasons;

		#endregion

		#region IMarksSubmissionFlow

		public override IEnumerable<string> CanNotCompileGradesReasons => FacultyMemberHelperMethods.CanNotCompileGradesReasons(this);

		public override IEnumerable<string> CanNotLockMarksEntryReasons => FacultyMemberHelperMethods.CanNotLockMarksEntryReasons(this);

		public override IEnumerable<string> CanNotUnlockMarksEntryReasons => FacultyMemberHelperMethods.CanNotUnlockMarksReasons;

		public override IEnumerable<string> CanNotSubmitToHODReasons => FacultyMemberHelperMethods.CanNotSubmitToHODReasons(this);

		public override IEnumerable<string> CanNotSubmitToCampusReasons => FacultyMemberHelperMethods.CanNotSubmitToCampusReasons;

		public override IEnumerable<string> CanNotSubmitToUniversityReasons => FacultyMemberHelperMethods.CanNotSubmitToUniversityReasons;

		public override IEnumerable<string> CanNotRevertSubmitToHODReasons => FacultyMemberHelperMethods.CanNotRevertSubmitToHODReasons;

		public override IEnumerable<string> CanNotRevertSubmitToCampusReasons => FacultyMemberHelperMethods.CanNotRevertSubmitToCampusReasons;

		public override IEnumerable<string> CanNotRevertSubmitToUniversityReasons => FacultyMemberHelperMethods.CanNotRevertSubmitToUniversityReasons;

		#endregion
	}
}