﻿using Aspire.Model.Entities.Common;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.New
{
	internal sealed class Display : IDisplay
	{
		public bool MultiRowHeader { get; private set; }
		public bool AssignmentVisible { get; private set; }
		public bool EmptyAssignmentVisible { get; private set; }
		public bool AssignmentsVisible { get; private set; }
		public bool CompileAssignmentsVisible { get; private set; }
		public int AssignmentsHeaderColSpan { get; private set; }
		public bool QuizVisible { get; private set; }
		public bool EmptyQuizVisible { get; private set; }
		public bool QuizzesVisible { get; private set; }
		public bool CompileQuizzesVisible { get; private set; }
		public int QuizzesHeaderColSpan { get; private set; }
		public bool InternalsVisible { get; private set; }
		public bool MidVisible { get; private set; }

		public string GetMarksString(decimal? obtainedMarks, decimal? totalMarks)
		{
			if (obtainedMarks == null)
				return totalMarks == null ? "Absent/TotalMarksMissing" : $"Absent/{totalMarks.Value.ToExamMarksString()}";
			return totalMarks == null
				? $"{obtainedMarks.Value.ToExamMarksString()}/TotalMarksMissing"
				: $"{obtainedMarks.Value.ToExamMarksString()}/{totalMarks.Value.ToExamMarksString()}";
		}

		public static IDisplay CreateAssignmentCompileAssignmentsQuizCompileQuizzesMidFinal(IMarksSheet marksSheet)
		{
			return new Display
			{
				MultiRowHeader = true,
				AssignmentVisible = true,
				EmptyAssignmentVisible = false,
				AssignmentsVisible = false,
				CompileAssignmentsVisible = true,
				AssignmentsHeaderColSpan = marksSheet.OfferedCourse.OfferedCourseExamAssignments?.Any() == true ? marksSheet.OfferedCourse.OfferedCourseExamAssignments.Count + 1 : 1,
				QuizVisible = true,
				EmptyQuizVisible = false,
				QuizzesVisible = false,
				CompileQuizzesVisible = true,
				QuizzesHeaderColSpan = marksSheet.OfferedCourse.OfferedCourseExamQuizzes?.Any() == true ? marksSheet.OfferedCourse.OfferedCourseExamQuizzes.Count + 1 : 1,
				InternalsVisible = false,
				MidVisible = true,
			};
		}

		public static IDisplay CreateAssignmentQuizCompileInternalsMidFinal(IMarksSheet marksSheet)
		{
			return new Display
			{
				MultiRowHeader = true,
				AssignmentVisible = true,
				EmptyAssignmentVisible = marksSheet.OfferedCourse.OfferedCourseExamAssignments?.Any() != true,
				AssignmentsVisible = false,
				CompileAssignmentsVisible = false,
				AssignmentsHeaderColSpan = marksSheet.OfferedCourse.OfferedCourseExamAssignments?.Any() == true ? marksSheet.OfferedCourse.OfferedCourseExamAssignments.Count : 1,
				QuizVisible = true,
				EmptyQuizVisible = marksSheet.OfferedCourse.OfferedCourseExamQuizzes?.Any() != true,
				QuizzesVisible = false,
				CompileQuizzesVisible = false,
				QuizzesHeaderColSpan = marksSheet.OfferedCourse.OfferedCourseExamQuizzes?.Any() == true ? marksSheet.OfferedCourse.OfferedCourseExamQuizzes.Count : 1,
				InternalsVisible = true,
				MidVisible = true,
			};
		}

		public static IDisplay CreateAssignmentsQuizzesMidFinal(IMarksSheet marksSheet)
		{
			return new Display
			{
				MultiRowHeader = false,
				AssignmentVisible = false,
				EmptyAssignmentVisible = false,
				AssignmentsVisible = true,
				CompileAssignmentsVisible = false,
				AssignmentsHeaderColSpan = 1,
				QuizVisible = false,
				EmptyQuizVisible = false,
				QuizzesVisible = true,
				CompileQuizzesVisible = false,
				QuizzesHeaderColSpan = 1,
				InternalsVisible = false,
				MidVisible = true
			};
		}

		public static IDisplay CreateFinal(IMarksSheet marksSheet)
		{
			return new Display
			{
				MultiRowHeader = false,
				AssignmentVisible = false,
				EmptyAssignmentVisible = false,
				AssignmentsVisible = false,
				CompileAssignmentsVisible = false,
				AssignmentsHeaderColSpan = 1,
				QuizVisible = false,
				EmptyQuizVisible = false,
				QuizzesVisible = false,
				CompileQuizzesVisible = false,
				QuizzesHeaderColSpan = 1,
				InternalsVisible = false,
				MidVisible = false
			};
		}
	}
}