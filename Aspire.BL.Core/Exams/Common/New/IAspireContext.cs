﻿using Aspire.Model.Context;
using Aspire.Model.Entities;

namespace Aspire.BL.Core.Exams.Common.New
{
	internal interface IAspireContext
	{
		AspireContext AspireContext { get; }
		string ReloadAndToJson(out MarksSheetChanx.ManagementEmailRecipients? managementEmailRecipients);
	}
}