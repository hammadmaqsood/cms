﻿using Aspire.Model.Entities;
using System.Collections.Generic;

namespace Aspire.BL.Core.Exams.Common.New
{
	public interface IQuizzes
	{
		IEnumerable<string> CanNotEditQuizzesReasons { get; }
		bool CanEditQuizzes { get; }
		IEnumerable<string> CanNotUnlockQuizzesReasons { get; }
		bool CanUnlockQuizzes { get; }
		IEnumerable<string> CanNotCompileQuizzesReasons { get; }
		bool CanCompileQuizzes { get; }
		bool IsQuizzesLocked { get; }
		IReadOnlyList<IStudentMarks> GetQuizzesMarks();
		decimal? GetCompiledQuizzesMarks(IRegisteredCourse registeredCourse, OfferedCours.ExamCompilationTypes examCompilationType);
		(IEnumerable<string> updateFailureReasons, bool success) UpdateQuizzes(IReadOnlyDictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)> marks, bool submit);
		(IEnumerable<string> compileFailureReasons, bool success) CompileQuizzes(OfferedCours.ExamCompilationTypes examCompilationTypeEnum, bool submit);
		(IEnumerable<string> unlockFailureReasons, bool success) UnlockQuizzes();
	}
}
