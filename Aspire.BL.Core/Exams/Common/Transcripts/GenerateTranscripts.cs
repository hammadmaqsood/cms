﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.Transcripts
{
	public static class GenerateTranscripts
	{
		public static Transcript GetTranscript(this AspireContext aspireContext, int studentID, short? compileUpToSemesterID)
		{
			var transcriptInput = aspireContext.Students
				.Where(s => s.StudentID == studentID)
				.Select(s => new TranscriptInput
				{
					StudentID = s.StudentID,
					RegistrationNo = s.RegistrationNo,
					Enrollment = s.Enrollment,
					Name = s.Name,
					FatherName = s.FatherName,
					StatusEnum = (Model.Entities.Student.Statuses?)s.Status,
					ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
					ProgramName = s.AdmissionOpenProgram.Program.ProgramName,
					ProgramShortName = s.AdmissionOpenProgram.Program.ProgramShortName,
					IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
					ProgramDurationEnum = (ProgramDurations)s.AdmissionOpenProgram.Program.Duration,
					IsSummerRegularSemester = s.AdmissionOpenProgram.IsSummerRegularSemester,
					ExamRemarksPolicy = s.AdmissionOpenProgram.ExamRemarksPolicy,
					ExamRemarksPolicyDetails = s.AdmissionOpenProgram.ExamRemarksPolicy.ExamRemarksPolicyDetails.ToList(),
					StudentSemesters = s.StudentSemesters.Select(ss => new TranscriptInput.StudentSemester
					{
						SemesterID = ss.SemesterID,
						StatusEnum = (StudentSemester.Statuses)ss.Status,
						FreezeStatusEnum = (FreezedStatuses?)ss.FreezedStatus,
						GPA = ss.GPA,
						CGPA = ss.CGPA,
						ExamRemarksTypeEnum = (ExamRemarksTypes?)ss.ExamRemarksType,
					}).ToList(),
					RegisteredCourses = s.RegisteredCourses
						.Where(rc => rc.DeletedDate == null)
						.Select(rc => new TranscriptInput.RegisteredCourse
						{
							RegisteredCourseID = rc.RegisteredCourseID,
							RoadmapCourseID = rc.CourseID,
							OfferedSemesterID = rc.OfferedCours.SemesterID,
							RoadmapCourseCode = rc.Cours.CourseCode,
							RoadmapCourseTitle = rc.Cours.Title,
							RoadmapCreditHours = rc.Cours.CreditHours,
							RoadmapCourseTypeEnum = (CourseTypes)rc.Cours.CourseType,
							RoadmapCourseCategoryEnum = (CourseCategories)rc.Cours.CourseCategory,
							RoadmapInstituteAlias = rc.Cours.AdmissionOpenProgram.Program.Institute.InstituteAlias,
							RoadmapProgramAlias = rc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
							FreezeStatusEnum = (FreezedStatuses?)rc.FreezedStatus,
							GradeEnum = (ExamGrades?)rc.Grade,
							GradePoints = rc.GradePoints,
							Product = rc.Product,
							StatusEnum = (RegisteredCours.Statuses?)rc.Status,
						})
						.ToList(),
					ExemptedCourses = s.StudentExemptedCourses
						.Select(sec => new TranscriptInput.ExemptedCourse
						{
							StudentExemptedCourseID = sec.StudentExemptedCourseID,
							ExemptedCourseID = sec.CourseID,
							ExemptedCourseCode = sec.Cours.CourseCode,
							ExemptedCourseTitle = sec.Cours.Title,
							ExemptedCreditHours = sec.Cours.CreditHours,
							ExemptedCourseTypeEnum = (CourseTypes)sec.Cours.CourseType,
							ExemptedCourseCategoryEnum = (CourseCategories)sec.Cours.CourseCategory,
							SubstitutedCourseID = sec.SubstitutedCourseID,
							SubstitutedCourseCode = sec.Cours1.CourseCode,
							SubstitutedTitle = sec.Cours1.Title,
							SubstitutedCreditHours = sec.Cours1.CreditHours,
							SubstitutedCourseTypeEnum = (CourseTypes)sec.Cours1.CourseType,
							SubstitutedCourseCategoryEnum = (CourseCategories)sec.Cours1.CourseCategory,
						}).ToList(),
					CreditsTransferredCourses = s.StudentCreditsTransfers
						.SelectMany(sct => sct.StudentCreditsTransferredCourses)
						.Where(sctc => sctc.EquivalentCourseID != null && sctc.EquivalentGrade != null)
						.Select(sctc => new TranscriptInput.CreditsTransferredCourse
						{
							CourseID = sctc.Cours.CourseID,
							CourseCode = sctc.Cours.CourseCode,
							Title = sctc.Cours.Title,
							CreditHours = sctc.Cours.CreditHours,
							EquivalentGradeEnum = (ExamGrades)sctc.EquivalentGrade.Value,
							CourseTypeEnum = (CourseTypes)sctc.Cours.CourseType,
							CourseCategoryEnum = (CourseCategories)sctc.Cours.CourseCategory,
						})
						.ToList()
				}).Single();

			var transferredStudent = aspireContext.TransferredStudents
				.Where(ts => ts.TransferredToStudentID == studentID)
				.OrderByDescending(ts => ts.TransferSemesterID)
				.ThenByDescending(ts => ts.TransferDate)
				.FirstOrDefault();
			if (transferredStudent != null)
			{
				transcriptInput.InternalTransferredCourses = aspireContext.TransferredStudentCourseMappings
						.Where(tscm => tscm.TransferredStudentID == transferredStudent.TransferredStudentID)
						.Select(tscm => new TranscriptInput.InternalTransferredCourse
						{
							TransferredStudentCourseMappingID = tscm.TransferredStudentCourseMappingID,
							RegisteredCourseID = tscm.FromRegisteredCourseID,
							OfferedSemesterID = tscm.RegisteredCours.OfferedCours.SemesterID,
							RegisteredCourseRoadmapInstituteAlias = tscm.RegisteredCours.Cours.AdmissionOpenProgram.Program.Institute.InstituteAlias,
							RegisteredCourseRoadmapProgramAlias = tscm.RegisteredCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
							RegisteredCourseRoadmapIntakeSemesterID = tscm.RegisteredCours.Cours.AdmissionOpenProgram.SemesterID,
							RegisteredCourseRoadmapCourseID = tscm.RegisteredCours.CourseID,
							RegisteredCourseRoadmapCourseCode = tscm.RegisteredCours.Cours.CourseCode,
							RegisteredCourseRoadmapTitle = tscm.RegisteredCours.Cours.Title,
							RegisteredCourseRoadmapCreditHours = tscm.RegisteredCours.Cours.CreditHours,
							RegisteredCourseGradeEnum = (ExamGrades?)tscm.RegisteredCours.Grade,
							RegisteredCourseGradePoints = tscm.RegisteredCours.GradePoints,
							RegisteredCourseProduct = tscm.RegisteredCours.Product,
							RegisteredCourseStatusEnum = (RegisteredCours.Statuses?)tscm.RegisteredCours.Status,
							RegisteredCourseFreezeStatusEnum = (FreezedStatuses?)tscm.RegisteredCours.FreezedStatus,
							RoadmapCourseID = tscm.ToCourseID,
							RoadmapCourseCode = tscm.Cours.CourseCode,
							RoadmapTitle = tscm.Cours.Title,
							RoadmapCreditHours = tscm.Cours.CreditHours,
							RoadmapMajors = tscm.Cours.ProgramMajor.Majors,
							RoadmapCourseTypeEnum = (CourseTypes)tscm.Cours.CourseType,
							RoadmapCourseCategoryEnum = (CourseCategories)tscm.Cours.CourseCategory
						}).ToList();
			}

			return new Transcript(transcriptInput, compileUpToSemesterID);
		}

		public enum CompileResultStatuses
		{
			NoRecordFound,
			PreFall2016NotSupported,
			NoChangesFound,
			Compiled
		}

		public static (CompileResultStatuses status, Transcript transcript) CompileResult(this AspireContext aspireContext, short? compileUpToSemesterID, int studentID, int loginHistoryID)
		{
			var transcript = aspireContext.GetTranscript(studentID, compileUpToSemesterID);
			if (transcript.IntakeSemesterID < 20163)
				return (CompileResultStatuses.PreFall2016NotSupported, transcript);
			if (transcript.Semesters == null)
				return (CompileResultStatuses.NoRecordFound, transcript);

			if (!transcript.Semesters.Any(s => s.GPA.FormatGPA() != s.DatabaseGPA.FormatGPA() || s.CGPA.FormatGPA() != s.DatabaseCGPA.FormatGPA() || s.ExamRemarksTypeEnum != s.DatabaseExamRemarksTypeEnum))
				return (CompileResultStatuses.NoChangesFound, transcript);

			var now = DateTime.Now;
			var studentSemesters = aspireContext.StudentSemesters.Where(s => s.StudentID == studentID).ToList();
			if (transcript.Semesters != null)
				foreach (var semester in transcript.Semesters)
				{
					var studentSemester = studentSemesters.SingleOrDefault(ss => ss.SemesterID == semester.SemesterID);
					if (studentSemester != null)
					{
						studentSemester.GPA = semester.GPA;
						studentSemester.CGPA = semester.CGPA;
						studentSemester.ExamRemarksTypeEnum = semester.ExamRemarksTypeEnum;
						studentSemester.CGPACalculationDate = now;
					}
				}
			aspireContext.SaveChanges(loginHistoryID);
			return (CompileResultStatuses.Compiled, transcript);
		}
	}
}
