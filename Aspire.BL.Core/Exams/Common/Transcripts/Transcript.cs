﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common.Transcripts
{
	public sealed class Transcript
	{
		private TranscriptInput Input { get; }
		public bool? IsExamRemarkPolicyValid { get; }

		internal Transcript(TranscriptInput transcriptInput, short? compileUpToSemesterID)
		{
			this.Input = transcriptInput;
			switch (this.Input.StatusEnum)
			{
				case Model.Entities.Student.Statuses.AdmissionCancelled:
					this._errors.Add("Admission has been canceled.");
					return;
				case Model.Entities.Student.Statuses.Left:
				case Model.Entities.Student.Statuses.Blocked:
				case Model.Entities.Student.Statuses.Deferred:
				case Model.Entities.Student.Statuses.Dropped:
				case Model.Entities.Student.Statuses.Expelled:
				case Model.Entities.Student.Statuses.Graduated:
				case Model.Entities.Student.Statuses.ProgramChanged:
				case Model.Entities.Student.Statuses.Rusticated:
				case Model.Entities.Student.Statuses.TransferredToOtherCampus:
				case null:
					var programDetails = $"Program: {this.Input.ProgramAlias}, Intake Semester: {this.Input.IntakeSemesterID.ToSemesterString()}.";
					var examRemarkPolicyErrorMessage = ValidateExamRemarksPolicy(this.Input.ExamRemarksPolicy, this.IntakeSemesterID);
					if (examRemarkPolicyErrorMessage != null)
						this._errors.Add($"{examRemarkPolicyErrorMessage} {programDetails}");
					this.IsExamRemarkPolicyValid = examRemarkPolicyErrorMessage == null;

					var semesterIDs = transcriptInput.StudentSemesters.Select(ss => ss.SemesterID);
					if (transcriptInput.InternalTransferredCourses?.Any() == true)
						semesterIDs = semesterIDs.Union(transcriptInput.InternalTransferredCourses.Select(itc => itc.OfferedSemesterID));
					semesterIDs = semesterIDs.Distinct()
						.Where(s => compileUpToSemesterID == null || s <= compileUpToSemesterID.Value)
						.OrderBy(s => s);
					this.Semesters = semesterIDs
						.Select(semesterID => new Semester(semesterID, this))
						.ToList();

					this.CompileRemarks();
					break;
				default:
					throw new NotImplementedEnumException(this.Input.StatusEnum);
			}
		}

		private static string ValidateExamRemarksPolicy(ExamRemarksPolicy examRemarksPolicy, short intakeSemesterID)
		{
			if (examRemarksPolicy == null)
				return "Exam Remarks Policy not found.";
			if (intakeSemesterID < examRemarksPolicy.ValidFromSemesterID
				|| (examRemarksPolicy.ValidUpToSemesterID != null && examRemarksPolicy.ValidUpToSemesterID < intakeSemesterID))
				return "Exam Remarks Policy is not valid for this student.";
			return examRemarksPolicy.IsValid;
		}

		private void CompileRemarks()
		{
			if (this.IsExamRemarkPolicyValid == null)
				throw new InvalidOperationException();

			if (this.IsExamRemarkPolicyValid == false)
				return;

			var firstSemesterRule = this.Input.ExamRemarksPolicyDetails.SingleOrDefault(erpd => erpd.FirstSemester);
			if (firstSemesterRule != null)
			{
				var firstSemester = this.Semesters.SingleOrDefault(s => s.SemesterID == this.IntakeSemesterID);
				if (firstSemester != null && firstSemester.CGPA < firstSemesterRule.LessThanCGPA)
				{
					this.Semesters.ForEach(s => s.ExamRemarksTypeEnum = firstSemesterRule.ExamRemarksTypeEnum);
					return;
				}
			}

			var examRemarksPolicyDetails = this.Input.ExamRemarksPolicyDetails.Where(erpd => erpd.FirstSemester == false).ToList();
			var rulesProcessed = new List<ExamRemarksPolicyDetail>();

			ExamRemarksPolicyDetail GetCurrentRule()
				=> examRemarksPolicyDetails.Except(rulesProcessed).OrderBy(erpd => erpd.Sequence).FirstOrDefault()
					?? throw new InvalidOperationException($"Rules not found further. StudentID: {this.StudentID}.");

			ExamRemarksTypes? lastExamRemarksType = null;
			foreach (var semester in this.Semesters)
			{
				if (lastExamRemarksType == ExamRemarksTypes.Drop)
					semester.ExamRemarksTypeEnum = ExamRemarksTypes.Drop;
				else
				{
					if (this.Input.IsSummerRegularSemester || !Model.Entities.Semester.IsSummer(semester.SemesterID))
					{
						var currentRule = GetCurrentRule();
						if (semester.CGPA < currentRule.LessThanCGPA)
						{
							semester.ExamRemarksTypeEnum = currentRule.ExamRemarksTypeEnum;
							rulesProcessed.Add(currentRule);
							lastExamRemarksType = semester.ExamRemarksTypeEnum;
						}
					}
				}
			}
		}

		private readonly List<string> _errors = new List<string>();
		private readonly List<string> _warnings = new List<string>();
		public IEnumerable<string> Errors => this._errors.AsReadOnly();
		public IEnumerable<string> Warnings => this._warnings.AsReadOnly();
		public int StudentID => this.Input.StudentID;
		public int? RegistrationNo => this.Input.RegistrationNo;
		public string Enrollment => this.Input.Enrollment;
		public string Name => this.Input.Name;
		public string FatherName => this.Input.FatherName;
		public string ProgramAlias => this.Input.ProgramAlias;
		public string ProgramShortName => this.Input.ProgramShortName;
		public string ProgramName => this.Input.ProgramName;
		public short IntakeSemesterID => this.Input.IntakeSemesterID;
		public IReadOnlyList<Semester> Semesters { get; }

		public sealed class Semester
		{
			public enum SemesterStatuses
			{
				Included,
				SemesterFreezeWithFee,
				SemesterFreezeWithoutFee,
				ErrorMultipleRegistrationOfSameCourseFound,
				ErrorInvalidGPA,
				ErrorInvalidCGPA
			}

			public enum GPAStatuses
			{
				Error,
				Included,
				ExcludedDueToDifferentSemester,
				ExcludedSemesterFreezeWithFee,
				ExcludedSemesterFreezeWithoutFee,
				ExcludedStatusDeferred,
				ExcludedStatusIncomplete,
				ExcludedStatusWithdraw,
				ExcludedStatusWithdrawWithFee,
				ExcludedDueToInvalidGradePoints
			}

			public enum CGPAStatuses
			{
				Error,
				Included,
				ExcludedStatusDeferred,
				ExcludedStatusIncomplete,
				ExcludedDueToRepeat,
				ExcludedDueToNullGrade,
				ExcludedSemesterFreezeWithFee,
				ExcludedSemesterFreezeWithoutFee,
				ExcludedStatusWithdraw,
				ExcludedStatusWithdrawWithFee,
				ExcludedDueToInvalidGradePoints
			}

			private Transcript Transcript { get; }
			public short SemesterID { get; }
			public SemesterStatuses SemesterStatus { get; private set; }
			public List<Course> Courses { get; set; }
			public decimal? GPA { get; private set; }
			public decimal? CGPA { get; private set; }
			public ExamRemarksTypes? ExamRemarksTypeEnum { get; internal set; }
			public decimal? DatabaseGPA { get; private set; }
			public decimal? DatabaseCGPA { get; private set; }
			public ExamRemarksTypes? DatabaseExamRemarksTypeEnum { get; internal set; }
			public string ExamRemarksTypeFullName => this.Transcript.IsExamRemarkPolicyValid == true ? this.ExamRemarksTypeEnum?.ToFullName() : "Error";

			public sealed class Course
			{
				public enum CourseTypes
				{
					RegisteredCourse,
					InternalTransfer
				}

				public CourseTypes CourseType { get; set; }
				public int CourseIndex { get; set; }

				#region Student Roadmap
				public int StudentRoadmapCourseID { get; set; }
				public string StudentRoadmapCourseCode { get; set; }
				public string StudentRoadmapTitle { get; set; }
				public decimal StudentRoadmapCreditHours { get; set; }
				public Model.Entities.Common.CourseTypes StudentRoadmapCourseTypeEnum { get; set; }
				public CourseCategories StudentRoadmapCourseCategoryEnum { get; set; }
				#endregion

				#region Registered Course
				public int RegisteredCourseID { get; set; }
				public short OfferedSemesterID { get; set; }
				public string RegisteredCourseInstituteAlias { get; set; }
				public string RegisteredCourseProgramAlias { get; set; }
				public int RegisteredCourseRoadmapCourseID { get; set; }
				public string RegisteredCourseRoadmapCourseCode { get; set; }
				public string RegisteredCourseRoadmapCourseTitle { get; set; }
				public decimal RegisteredCourseRoadmapCourseCreditHours { get; set; }
				public ExamGrades? RegisteredCourseGradeEnum { get; set; }
				public decimal? RegisteredCourseGradePoints { get; set; }
				public decimal? RegisteredCourseProduct { get; set; }
				public RegisteredCours.Statuses? RegisteredCourseStatusEnum { get; set; }
				public FreezedStatuses? RegisteredCourseFreezeStatusEnum { get; set; }
				#endregion

				public GPAStatuses GPAStatus { get; set; }
				public CGPAStatuses CGPAStatus { get; set; }
			}

			internal Semester(short semesterID, Transcript transcript)
			{
				this.Transcript = transcript;
				this.SemesterID = semesterID;

				this.DatabaseGPA = transcript.Input.StudentSemesters.SingleOrDefault(ss => ss.SemesterID == semesterID)?.GPA;
				this.DatabaseCGPA = transcript.Input.StudentSemesters.SingleOrDefault(ss => ss.SemesterID == semesterID)?.CGPA;
				this.DatabaseExamRemarksTypeEnum = transcript.Input.StudentSemesters.SingleOrDefault(ss => ss.SemesterID == semesterID)?.ExamRemarksTypeEnum;

				var courseIndex = 0;
				this.Courses = this.Transcript.Input.RegisteredCourses
					.Where(rc => rc.OfferedSemesterID <= this.SemesterID)
					.OrderBy(rc => rc.OfferedSemesterID).ThenBy(rc => rc.RoadmapCourseTitle)
					.Select(rc => new Course
					{
						CourseIndex = courseIndex++,
						CourseType = Course.CourseTypes.RegisteredCourse,
						RegisteredCourseID = rc.RegisteredCourseID,
						OfferedSemesterID = rc.OfferedSemesterID,
						RegisteredCourseInstituteAlias = rc.RoadmapInstituteAlias,
						RegisteredCourseProgramAlias = rc.RoadmapProgramAlias,
						RegisteredCourseRoadmapCourseID = rc.RoadmapCourseID,
						RegisteredCourseRoadmapCourseCode = rc.RoadmapCourseCode,
						RegisteredCourseRoadmapCourseTitle = rc.RoadmapCourseTitle,
						RegisteredCourseRoadmapCourseCreditHours = rc.RoadmapCreditHours,
						RegisteredCourseGradeEnum = rc.GradeEnum,
						RegisteredCourseGradePoints = rc.GradePoints,
						RegisteredCourseProduct = rc.Product,
						RegisteredCourseStatusEnum = rc.StatusEnum,
						RegisteredCourseFreezeStatusEnum = rc.FreezeStatusEnum,
						StudentRoadmapCourseID = rc.RoadmapCourseID,
						StudentRoadmapCourseCode = rc.RoadmapCourseCode,
						StudentRoadmapTitle = rc.RoadmapCourseTitle,
						StudentRoadmapCreditHours = rc.RoadmapCreditHours,
						StudentRoadmapCourseTypeEnum = rc.RoadmapCourseTypeEnum,
						StudentRoadmapCourseCategoryEnum = rc.RoadmapCourseCategoryEnum,
						GPAStatus = GPAStatuses.Error,
						CGPAStatus = CGPAStatuses.Error
					})
					.OrderBy(rc => rc.OfferedSemesterID)
					.ToList();

				this.Transcript.Input.InternalTransferredCourses?.ForEach(itc =>
				{
					if (itc.OfferedSemesterID <= this.SemesterID)
						this.Courses.Add(new Course
						{
							CourseIndex = courseIndex++,
							CourseType = Course.CourseTypes.InternalTransfer,
							RegisteredCourseID = itc.RegisteredCourseID,
							OfferedSemesterID = itc.OfferedSemesterID,
							RegisteredCourseInstituteAlias = itc.RegisteredCourseRoadmapInstituteAlias,
							RegisteredCourseProgramAlias = itc.RegisteredCourseRoadmapProgramAlias,
							RegisteredCourseRoadmapCourseID = itc.RegisteredCourseRoadmapCourseID,
							RegisteredCourseRoadmapCourseCode = itc.RegisteredCourseRoadmapCourseCode,
							RegisteredCourseRoadmapCourseTitle = itc.RegisteredCourseRoadmapTitle,
							RegisteredCourseRoadmapCourseCreditHours = itc.RegisteredCourseRoadmapCreditHours,
							RegisteredCourseGradeEnum = itc.RegisteredCourseGradeEnum,
							RegisteredCourseGradePoints = itc.RegisteredCourseGradePoints,
							RegisteredCourseProduct = itc.RegisteredCourseProduct,
							RegisteredCourseStatusEnum = itc.RegisteredCourseStatusEnum,
							RegisteredCourseFreezeStatusEnum = itc.RegisteredCourseFreezeStatusEnum,
							StudentRoadmapCourseID = itc.RoadmapCourseID,
							StudentRoadmapCourseCode = itc.RoadmapCourseCode,
							StudentRoadmapTitle = itc.RoadmapTitle,
							StudentRoadmapCreditHours = itc.RoadmapCreditHours,
							StudentRoadmapCourseCategoryEnum = itc.RoadmapCourseCategoryEnum,
							StudentRoadmapCourseTypeEnum = itc.RoadmapCourseTypeEnum,
							GPAStatus = GPAStatuses.Error,
							CGPAStatus = CGPAStatuses.Error,
						});
				});

				this.Compile();
			}

			private void Compile()
			{
				var studentSemester = this.Transcript.Input.StudentSemesters.SingleOrDefault(ss => ss.SemesterID == this.SemesterID);
				switch (studentSemester?.FreezeStatusEnum)
				{
					case FreezedStatuses.FreezedWithFee:
						this.SemesterStatus = SemesterStatuses.SemesterFreezeWithFee;
						return;
					case FreezedStatuses.FreezedWithoutFee:
						this.SemesterStatus = SemesterStatuses.SemesterFreezeWithoutFee;
						return;
					case null:
						this.Courses.ForEach(this.ProcessCourseForGPA);
						this.ProcessCoursesForCGPA();
						this.SemesterStatus = SemesterStatuses.Included;
						(this.GPA, this.CGPA) = this.GetGPAAndCGPA();

						if (this.GPA < 0 || 4 < this.GPA)
						{
							this.GPA = null;
							this.CGPA = null;
							this.SemesterStatus = SemesterStatuses.ErrorInvalidGPA;
							return;
						}

						if (this.CGPA < 0 || 4 < this.CGPA)
						{
							this.CGPA = null;
							this.SemesterStatus = SemesterStatuses.ErrorInvalidCGPA;
							return;
						}
						return;
					default:
						throw new NotImplementedEnumException(studentSemester.FreezeStatusEnum);
				}
			}

			private void ProcessCourseForGPA(Course course)
			{
				var sameCoursesCount = this.Courses.Count(c => c.StudentRoadmapCourseID == course.StudentRoadmapCourseID && c.OfferedSemesterID == course.OfferedSemesterID);
				if (sameCoursesCount > 1)
				{
					this.SemesterStatus = SemesterStatuses.ErrorMultipleRegistrationOfSameCourseFound;
					return;
				}

				if (course.OfferedSemesterID != this.SemesterID)
				{
					course.GPAStatus = GPAStatuses.ExcludedDueToDifferentSemester;
					return;
				}

				switch (course.RegisteredCourseFreezeStatusEnum)
				{
					case FreezedStatuses.FreezedWithFee:
						course.GPAStatus = GPAStatuses.ExcludedSemesterFreezeWithFee;
						return;
					case FreezedStatuses.FreezedWithoutFee:
						course.GPAStatus = GPAStatuses.ExcludedSemesterFreezeWithoutFee;
						return;
					case null:
						switch (course.RegisteredCourseStatusEnum)
						{
							case RegisteredCours.Statuses.Deferred:
								course.GPAStatus = GPAStatuses.ExcludedStatusDeferred;
								return;
							case RegisteredCours.Statuses.Incomplete:
								course.GPAStatus = GPAStatuses.ExcludedStatusIncomplete;
								return;
							case RegisteredCours.Statuses.AttendanceDefaulter:
								course.RegisteredCourseGradeEnum = ExamGrades.F;
								course.RegisteredCourseGradePoints = 0;
								course.RegisteredCourseProduct = 0;
								course.GPAStatus = GPAStatuses.Included;
								return;
							case RegisteredCours.Statuses.WithdrawWithFee:
								course.GPAStatus = GPAStatuses.ExcludedStatusWithdraw;
								return;
							case RegisteredCours.Statuses.WithdrawWithoutFee:
								course.GPAStatus = GPAStatuses.ExcludedStatusWithdrawWithFee;
								return;
							case RegisteredCours.Statuses.UnfairMeans:
							case null:
								if (course.RegisteredCourseGradePoints != null && 0 <= course.RegisteredCourseGradePoints && course.RegisteredCourseGradePoints <= 4)
								{
									course.GPAStatus = GPAStatuses.Included;
									if (course.RegisteredCourseGradeEnum == null)
									{
										course.RegisteredCourseGradePoints = 0;
										course.RegisteredCourseProduct = 0;
									}
								}
								else
									course.GPAStatus = GPAStatuses.ExcludedDueToInvalidGradePoints;
								return;
							default:
								throw new NotImplementedEnumException(course.RegisteredCourseStatusEnum);
						}
					default:
						throw new NotImplementedEnumException(course.RegisteredCourseFreezeStatusEnum);
				}
			}

			private void ProcessCoursesForCGPA()
			{
				this.Courses.GroupBy(c => new
				{
					c.StudentRoadmapCourseID
				}).Select(g => new
				{
					g.Key.StudentRoadmapCourseID,
					Courses = g.ToList()
				}).ForEach(g =>
				{
					if (g.Courses.Count == 1)
					{
						var course = g.Courses.Single();
						switch (course.GPAStatus)
						{
							case GPAStatuses.Error:
								course.CGPAStatus = CGPAStatuses.Error;
								return;
							case GPAStatuses.Included:
							case GPAStatuses.ExcludedDueToDifferentSemester:
								course.CGPAStatus = CGPAStatuses.Included;
								return;
							case GPAStatuses.ExcludedSemesterFreezeWithFee:
								course.CGPAStatus = CGPAStatuses.ExcludedSemesterFreezeWithFee;
								return;
							case GPAStatuses.ExcludedSemesterFreezeWithoutFee:
								course.CGPAStatus = CGPAStatuses.ExcludedSemesterFreezeWithoutFee;
								return;
							case GPAStatuses.ExcludedStatusDeferred:
								course.CGPAStatus = CGPAStatuses.ExcludedStatusDeferred;
								return;
							case GPAStatuses.ExcludedStatusIncomplete:
								course.CGPAStatus = CGPAStatuses.ExcludedStatusIncomplete;
								return;
							case GPAStatuses.ExcludedStatusWithdraw:
								course.CGPAStatus = CGPAStatuses.ExcludedStatusWithdraw;
								return;
							case GPAStatuses.ExcludedStatusWithdrawWithFee:
								course.CGPAStatus = CGPAStatuses.ExcludedStatusWithdrawWithFee;
								return;
							case GPAStatuses.ExcludedDueToInvalidGradePoints:
								course.CGPAStatus = CGPAStatuses.ExcludedDueToInvalidGradePoints;
								return;
							default:
								throw new NotImplementedEnumException(course.GPAStatus);
						}
					}
					else
					{
						var included = false;
						foreach (var course in g.Courses.OrderByDescending(c => c.RegisteredCourseGradeEnum ?? ExamGrades.F, ExamGradeComparer.New()))
						{
							switch (course.GPAStatus)
							{
								case GPAStatuses.Error:
									course.CGPAStatus = CGPAStatuses.Error;
									break;
								case GPAStatuses.ExcludedSemesterFreezeWithFee:
									course.CGPAStatus = CGPAStatuses.ExcludedSemesterFreezeWithFee;
									break;
								case GPAStatuses.ExcludedSemesterFreezeWithoutFee:
									course.CGPAStatus = CGPAStatuses.ExcludedSemesterFreezeWithoutFee;
									break;
								case GPAStatuses.ExcludedStatusDeferred:
									course.CGPAStatus = CGPAStatuses.ExcludedStatusDeferred;
									break;
								case GPAStatuses.ExcludedStatusIncomplete:
									course.CGPAStatus = CGPAStatuses.ExcludedStatusIncomplete;
									break;
								case GPAStatuses.ExcludedStatusWithdraw:
									course.CGPAStatus = CGPAStatuses.ExcludedStatusWithdraw;
									break;
								case GPAStatuses.ExcludedStatusWithdrawWithFee:
									course.CGPAStatus = CGPAStatuses.ExcludedStatusWithdrawWithFee;
									break;
								case GPAStatuses.ExcludedDueToInvalidGradePoints:
									course.CGPAStatus = CGPAStatuses.ExcludedDueToInvalidGradePoints;
									break;
								case GPAStatuses.Included:
								case GPAStatuses.ExcludedDueToDifferentSemester:
									if (included)
									{
										switch (course.RegisteredCourseGradeEnum)
										{
											case ExamGrades.A:
											case ExamGrades.BPlus:
											case ExamGrades.B:
											case ExamGrades.CPlus:
											case ExamGrades.C:
											case ExamGrades.D:
											case ExamGrades.F:
											case ExamGrades.AMinus:
											case ExamGrades.BMinus:
											case ExamGrades.CMinus:
											case ExamGrades.DPlus:
												course.CGPAStatus = CGPAStatuses.ExcludedDueToRepeat;
												break;
											case null:
												course.CGPAStatus = CGPAStatuses.ExcludedDueToNullGrade;
												break;
											default:
												throw new NotImplementedEnumException(course.RegisteredCourseGradeEnum);
										}
									}
									else
									{
										course.CGPAStatus = CGPAStatuses.Included;
										included = true;
									}
									break;
								default:
									throw new NotImplementedEnumException(course.GPAStatus);
							}
						}

						if (g.Courses.Any(c => c.CGPAStatus == CGPAStatuses.Error))
							g.Courses.ForEach(c => c.CGPAStatus = CGPAStatuses.Error);
					}
				});
			}

			private (decimal? GPA, decimal? CGPA) GetGPAAndCGPA()
			{
				var gpaCourseFound = false;
				var cgpaCourseFound = false;
				decimal sumOfCreditHoursForGPA = 0;
				decimal sumOfProductForGPA = 0;
				decimal sumOfCreditHoursForCGPA = 0;
				decimal sumOfProductForCGPA = 0;
				foreach (var course in this.Courses)
				{
					switch (course.GPAStatus)
					{
						case GPAStatuses.Included:
							gpaCourseFound = true;
							sumOfCreditHoursForGPA += course.StudentRoadmapCreditHours;
							sumOfProductForGPA += (course.StudentRoadmapCreditHours * course.RegisteredCourseGradePoints) ?? 0;
							break;
						case GPAStatuses.ExcludedStatusDeferred:
						case GPAStatuses.ExcludedStatusIncomplete:
						case GPAStatuses.ExcludedDueToDifferentSemester:
						case GPAStatuses.ExcludedSemesterFreezeWithFee:
						case GPAStatuses.ExcludedSemesterFreezeWithoutFee:
						case GPAStatuses.ExcludedStatusWithdraw:
						case GPAStatuses.ExcludedStatusWithdrawWithFee:
						case GPAStatuses.ExcludedDueToInvalidGradePoints:
							break;
						case GPAStatuses.Error:
							return (null, null);
						default:
							throw new NotImplementedEnumException(course.CGPAStatus);
					}
					switch (course.CGPAStatus)
					{
						case CGPAStatuses.Included:
							cgpaCourseFound = true;
							sumOfCreditHoursForCGPA += course.StudentRoadmapCreditHours;
							sumOfProductForCGPA += (course.StudentRoadmapCreditHours * course.RegisteredCourseGradePoints) ?? 0;
							break;
						case CGPAStatuses.ExcludedStatusDeferred:
						case CGPAStatuses.ExcludedStatusIncomplete:
						case CGPAStatuses.ExcludedDueToRepeat:
						case CGPAStatuses.ExcludedDueToNullGrade:
						case CGPAStatuses.ExcludedSemesterFreezeWithFee:
						case CGPAStatuses.ExcludedSemesterFreezeWithoutFee:
						case CGPAStatuses.ExcludedStatusWithdraw:
						case CGPAStatuses.ExcludedStatusWithdrawWithFee:
						case CGPAStatuses.ExcludedDueToInvalidGradePoints:
							break;
						case CGPAStatuses.Error:
							return (null, null);
						default:
							throw new NotImplementedEnumException(course.CGPAStatus);
					}
				}

				decimal? gpa = null;
				decimal? cgpa = null;

				if (gpaCourseFound)
				{
					if (sumOfCreditHoursForGPA < 0)
						throw new InvalidOperationException("Sum of CreditHours cannot be less than zero.");
					gpa = sumOfCreditHoursForGPA == 0 ? (decimal?)null : sumOfProductForGPA / sumOfCreditHoursForGPA;
				}
				if (cgpaCourseFound)
				{
					if (sumOfCreditHoursForCGPA < 0)
						throw new InvalidOperationException("Sum of CreditHours cannot be less than zero.");
					cgpa = sumOfCreditHoursForCGPA == 0 ? (decimal?)null : sumOfProductForCGPA / sumOfCreditHoursForCGPA;
				}

				if (gpa == null)
					cgpa = null;
				return (gpa, cgpa);
			}
		}
	}
}
