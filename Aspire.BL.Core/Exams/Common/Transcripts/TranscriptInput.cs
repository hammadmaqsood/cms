﻿using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System.Collections.Generic;

namespace Aspire.BL.Core.Exams.Common.Transcripts
{
	internal sealed class TranscriptInput
	{
		public int StudentID { get; set; }
		public int? RegistrationNo { get; set; }
		public string Enrollment { get; set; }
		public string Name { get; set; }
		public string FatherName { get; set; }
		public Model.Entities.Student.Statuses? StatusEnum { get; set; }
		public string ProgramAlias { get; set; }
		public string ProgramShortName { get; set; }
		public string ProgramName { get; set; }
		public short IntakeSemesterID { get; set; }
		public ProgramDurations ProgramDurationEnum { get; set; }
		public bool IsSummerRegularSemester { get; set; }
		public ExamRemarksPolicy ExamRemarksPolicy { get; set; }
		public List<ExamRemarksPolicyDetail> ExamRemarksPolicyDetails { get; set; }
		public List<StudentSemester> StudentSemesters { get; set; }
		public List<RegisteredCourse> RegisteredCourses { get; set; }
		public List<ExemptedCourse> ExemptedCourses { get; set; }
		public List<CreditsTransferredCourse> CreditsTransferredCourses { get; set; }
		public List<InternalTransferredCourse> InternalTransferredCourses { get; set; }

		internal sealed class StudentSemester
		{
			public short SemesterID { get; set; }
			public Model.Entities.StudentSemester.Statuses StatusEnum { get; set; }
			public FreezedStatuses? FreezeStatusEnum { get; set; }
			public decimal? GPA { get; set; }
			public decimal? CGPA { get; set; }
			public ExamRemarksTypes? ExamRemarksTypeEnum { get; set; }
		}

		internal sealed class RegisteredCourse
		{
			public int RegisteredCourseID { get; set; }
			public short OfferedSemesterID { get; set; }
			public int RoadmapCourseID { get; set; }
			public string RoadmapInstituteAlias { get; set; }
			public string RoadmapProgramAlias { get; set; }
			public string RoadmapCourseCode { get; set; }
			public string RoadmapCourseTitle { get; set; }
			public decimal RoadmapCreditHours { get; set; }
			public CourseTypes RoadmapCourseTypeEnum { get; set; }
			public CourseCategories RoadmapCourseCategoryEnum { get; set; }
			public ExamGrades? GradeEnum { get; set; }
			public decimal? GradePoints { get; set; }
			public decimal? Product { get; set; }
			public RegisteredCours.Statuses? StatusEnum { get; set; }
			public FreezedStatuses? FreezeStatusEnum { get; set; }
		}

		internal sealed class ExemptedCourse
		{
			public int StudentExemptedCourseID { get; set; }
			public int ExemptedCourseID { get; set; }
			public string ExemptedCourseCode { get; set; }
			public string ExemptedCourseTitle { get; set; }
			public decimal ExemptedCreditHours { get; set; }
			public CourseTypes ExemptedCourseTypeEnum { get; set; }
			public CourseCategories ExemptedCourseCategoryEnum { get; set; }
			public int? SubstitutedCourseID { get; set; }
			public string SubstitutedCourseCode { get; set; }
			public string SubstitutedTitle { get; set; }
			public decimal? SubstitutedCreditHours { get; set; }
			public CourseTypes? SubstitutedCourseTypeEnum { get; set; }
			public CourseCategories? SubstitutedCourseCategoryEnum { get; set; }
		}

		internal sealed class CreditsTransferredCourse
		{
			public int CourseID { get; set; }
			public string CourseCode { get; set; }
			public string Title { get; set; }
			public decimal CreditHours { get; set; }
			public ExamGrades EquivalentGradeEnum { get; set; }
			public CourseTypes CourseTypeEnum { get; set; }
			public CourseCategories CourseCategoryEnum { get; set; }
		}

		internal class InternalTransferredCourse
		{
			public int TransferredStudentCourseMappingID { get; set; }
			public int RegisteredCourseID { get; set; }
			public short OfferedSemesterID { get; set; }
			public string RegisteredCourseRoadmapInstituteAlias { get; set; }
			public string RegisteredCourseRoadmapProgramAlias { get; set; }
			public short RegisteredCourseRoadmapIntakeSemesterID { get; set; }
			public int RegisteredCourseRoadmapCourseID { get; set; }
			public string RegisteredCourseRoadmapCourseCode { get; set; }
			public string RegisteredCourseRoadmapTitle { get; set; }
			public decimal RegisteredCourseRoadmapCreditHours { get; set; }
			public ExamGrades? RegisteredCourseGradeEnum { get; set; }
			public decimal? RegisteredCourseGradePoints { get; set; }
			public decimal? RegisteredCourseProduct { get; set; }
			public RegisteredCours.Statuses? RegisteredCourseStatusEnum { get; set; }
			public FreezedStatuses? RegisteredCourseFreezeStatusEnum { get; set; }
			public int RoadmapCourseID { get; set; }
			public string RoadmapCourseCode { get; set; }
			public string RoadmapTitle { get; set; }
			public decimal RoadmapCreditHours { get; set; }
			public string RoadmapMajors { get; set; }
			public CourseTypes RoadmapCourseTypeEnum { get; set; }
			public CourseCategories RoadmapCourseCategoryEnum { get; set; }
		}
	}
}