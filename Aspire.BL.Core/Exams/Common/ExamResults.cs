﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Common
{
	public static class ExamResults
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissionsStaff(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Executive.ExecutiveGroupPermission DemandModulePermissionsExecutive(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandExecutivePermissions(loginSessionGuid, ExecutivePermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}
		public sealed class ExamResultPageHeader
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Title => $"Provisional Result Card";
			public static List<ExamResultPageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class ExamResultStudentInformation
		{
			public string Enrollment { get; set; }
			public int StudentID { get; set; }
			public int? RegistrationNo { get; set; }
			public string Name { get; set; }
			public string FatherName { get; set; }
			public string Program { get; set; }
			public int ProgramID { get; set; }
			public int Duration { get; set; }
			public short MaxAllowedSemesterID { get; set; }
			public int AdmissionOpenProgramID { get; set; }
			public short SemesterID { get; set; }
			public string Nationality { get; set; }
			public byte? Status { get; set; }
			public byte? StudentSemesterStatus { get; set; }
			public byte? FreezedStatus { get; set; }
			public Aspire.Model.Entities.Student.Statuses? StatusEnum => (Aspire.Model.Entities.Student.Statuses?)this.Status;
			public DateTime? StatusDate { get; set; }
			public string Remarks { get; set; }
			public string ProgramAlias { get; set; }
			public short SemesterNo { get; set; }
			public int Section { get; set; }
			public byte Shift { get; set; }

			public string IntakeSemesterID => this.SemesterID.ToSemesterString();
			public string StatusEnumFullName => (this.StatusEnum?.ToFullName()).ToNAIfNullOrEmpty();
			//public string RemarksNA => this.Remarks.ToNAIfNullOrEmpty();
			//public string CompleteStatus => (AspireFormats.GetRegisteredCourseStatusString(this.StudentSemesterStatus, this.FreezedStatus)).ToNAIfNullOrEmpty();
			//	public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			//	public string DurationEnumFullName => ((ProgramDurations)this.Duration).ToFullName();
			//	public string MaxAllowedSemester => this.MaxAllowedSemesterID.ToSemesterString();
			public static List<ExamResultStudentInformation> GetList()
			{
				return null;
			}
		}

		public sealed class ExamResult
		{
			public short SemesterID { get; internal set; }
			public int CourseID { get; internal set; }
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public string Teacher { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public decimal? Assignments { get; internal set; }
			public decimal? Quizzes { get; internal set; }
			public decimal? Internals { get; internal set; }
			public decimal? Mid { get; internal set; }
			public decimal? Final { get; internal set; }
			public byte? Total { get; internal set; }
			public byte? Grade { get; internal set; }
			public decimal? GradePoints { get; internal set; }
			public decimal? Product { get; internal set; }
			public decimal? GPA { get; internal set; }
			public decimal? CGPA { get; internal set; }
			public string Remarks { get; internal set; }
			public decimal? BUExamGPA { get; internal set; }
			public decimal? BUExamCGPA { get; internal set; }
			public byte? BUExamResultRemarks { get; internal set; }
			public byte? Status { get; internal set; }
			public byte? FreezedStatus { get; internal set; }
			public byte? StudentFeeStatus { get; internal set; }
			public decimal? TotalHours { get; internal set; }
			public decimal? PresentHours { get; internal set; }
			public decimal AbsentHours => this.TotalHours - this.PresentHours ?? 0;
			public decimal Percentage => this.TotalHours == 0 ? 0 : (this.PresentHours ?? 0) * 100 / (this.TotalHours ?? 1);

			public string Semester => this.SemesterID.ToSemesterString();
			public string StringCreditHours => this.CreditHours.ToCreditHoursFullName();
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public string StatusComplete => AspireFormats.GetRegisteredCourseStatusString(this.Status, this.FreezedStatus).ToNAIfNullOrEmpty();
			public string StudentFeeStatusFullName => ((StudentFee.Statuses?)this.StudentFeeStatus)?.ToFullName() ?? StudentFee.Statuses.NotPaid.ToFullName();
			public string GradeEnumFullName => ((ExamGrades?)this.Grade)?.ToFullName();
			public string AssignmentsString => (this.Assignments)?.ToString("0.##");
			public string QuizzesString => (this.Quizzes)?.ToString("0.##");
			public string InternalString => (this.Internals)?.ToString("0.##");
			public string MidString => (this.Mid)?.ToString("0.##");
			public string FinalString => (this.Final)?.ToString("0.##");
			public string ProductString => this.Product?.ToString("0.##");
			public string PercentageShort => this.Percentage.ToString("0.##");
			public string StringGPA => this.GPA?.ToString("0.##") ?? string.Empty.ToNAIfNullOrEmpty();
			public string StringCGPA => this.CGPA?.ToString("0.##") ?? string.Empty.ToNAIfNullOrEmpty();
			public string RemarksNA => this.Remarks ?? string.Empty.ToNAIfNullOrEmpty();
			public string StringBUExamGPA => this.BUExamGPA?.ToString("0.##") ?? this.StringGPA;
			public string StringBUExamCGPA => this.BUExamCGPA?.ToString("0.##") ?? this.StringCGPA;
			public string StringBUExamResultRemarks => ((StudentSemester.ResultRemarksTypes?)this.BUExamResultRemarks).ToString().ToNAIfNullOrEmpty();

			public List<ExamResult> GetList()
			{
				return null;
			}

		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public ExamResultPageHeader PageHeader { get; internal set; }
			public ExamResultStudentInformation StudentInformation { get; internal set; }
			public List<ExamResult> ExamResults { get; internal set; }
		}

		public static ReportDataSet GetExamResult(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				int instituteID = 0;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Executive:
						var executiveLoginHistory = aspireContext.DemandModulePermissionsExecutive(loginSessionGuid);
						instituteID = executiveLoginHistory.InstituteID;
						break;
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandModulePermissionsStaff(loginSessionGuid);
						instituteID = staffLoginHistory.InstituteID;
						break;
					case UserTypes.Student:
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				var studentInformation = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => new ExamResultStudentInformation
				{
					StudentID = s.StudentID,
					Enrollment = s.Enrollment,
					RegistrationNo = s.RegistrationNo,
					Name = s.Name,
					FatherName = s.FatherName,
					ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
					Duration = s.AdmissionOpenProgram.Program.Duration,
					MaxAllowedSemesterID = s.AdmissionOpenProgram.MaxSemesterID,
					ProgramID = s.AdmissionOpenProgram.Program.ProgramID,
					SemesterID = s.AdmissionOpenProgram.SemesterID,
					AdmissionOpenProgramID = s.AdmissionOpenProgramID,
					Nationality = s.Nationality,
					Status = s.Status,
					StatusDate = s.StatusDate,
				}).SingleOrDefault();

				var registeredCourses = aspireContext.RegisteredCourses
					.Where(rc => rc.StudentID == studentID && rc.DeletedDate == null)
					.Select(rc => new
					{
						rc.OfferedCours.SemesterID,
						rc.RegisteredCourseID,
						rc.StudentID,
						rc.OfferedCourseID,
						rc.CourseID,
						rc.Cours.CourseCode,
						rc.Cours.Title,
						rc.Cours.CreditHours,
						rc.OfferedCours.FacultyMember.Name,
						rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						rc.OfferedCours.SemesterNo,
						rc.OfferedCours.Section,
						rc.OfferedCours.Shift,
						rc.Assignments,
						rc.Quizzes,
						rc.Internals,
						rc.Mid,
						rc.Final,
						rc.Total,
						rc.Grade,
						rc.GradePoints,
						rc.Product,
						rc.Status,
						rc.FreezedStatus,
						StudentFeeStatus = rc.StudentFee.Status,
						rc.OfferedCours.MarksEntryLocked,
						TotalHours = rc.OfferedCours.OfferedCourseAttendances.Sum(oca => (decimal?)oca.Hours) ?? 0,
						PresentHours = rc.OfferedCours.OfferedCourseAttendances.SelectMany(oca => oca.OfferedCourseAttendanceDetails).Where(ocad => ocad.RegisteredCourseID == rc.RegisteredCourseID).Sum(ocad => (decimal?)ocad.Hours) ?? 0,
						GPA = rc.Student.StudentSemesters.FirstOrDefault(ss => ss.SemesterID == rc.OfferedCours.SemesterID).GPA,
						CGPA = rc.Student.StudentSemesters.FirstOrDefault(ss => ss.SemesterID == rc.OfferedCours.SemesterID).CGPA,
						Remarks = rc.Student.StudentSemesters.FirstOrDefault(ss => ss.SemesterID == rc.OfferedCours.SemesterID).Remarks,
						BUExamGPA = rc.Student.StudentSemesters.FirstOrDefault(ss => ss.SemesterID == rc.OfferedCours.SemesterID).BUExamGPA,
						BUExamCGPA = rc.Student.StudentSemesters.FirstOrDefault(ss => ss.SemesterID == rc.OfferedCours.SemesterID).BUExamCGPA,
						BUExamResultRemarks = rc.Student.StudentSemesters.FirstOrDefault(ss => ss.SemesterID == rc.OfferedCours.SemesterID).BUExamResultRemarks,
					}).AsQueryable();

				var result = registeredCourses.Select(rc => new ExamResult
				{
					SemesterID = rc.SemesterID,
					CourseID = rc.CourseID,
					CourseCode = rc.CourseCode,
					Title = rc.Title,
					CreditHours = rc.CreditHours,
					Teacher = rc.Name,
					ProgramAlias = rc.ProgramAlias,
					SemesterNo = rc.SemesterNo,
					Section = rc.Section,
					Shift = rc.Shift,
					Assignments = rc.MarksEntryLocked ? rc.Assignments : null,
					Quizzes = rc.MarksEntryLocked ? rc.Quizzes : null,
					Internals = rc.MarksEntryLocked ? rc.Internals : null,
					Mid = rc.Mid,
					Final = rc.MarksEntryLocked ? rc.Final : null,
					Total = rc.MarksEntryLocked ? rc.Total : null,
					Grade = rc.MarksEntryLocked ? rc.Grade : null,
					GradePoints = rc.MarksEntryLocked ? rc.GradePoints : null,
					Product = rc.MarksEntryLocked ? rc.Product : null,
					GPA = rc.MarksEntryLocked ? rc.GPA : null,
					CGPA = rc.MarksEntryLocked ? rc.CGPA : null,
					Remarks = rc.MarksEntryLocked ? rc.Remarks : null,
					BUExamGPA = rc.MarksEntryLocked ? rc.BUExamGPA : null,
					BUExamCGPA = rc.MarksEntryLocked ? rc.BUExamCGPA : null,
					BUExamResultRemarks = rc.MarksEntryLocked ? rc.BUExamResultRemarks : null,
					Status = rc.Status,
					FreezedStatus = rc.FreezedStatus,
					StudentFeeStatus = rc.StudentFeeStatus,
					TotalHours = rc.TotalHours,
					PresentHours = rc.PresentHours,
				}).OrderByDescending(o => o.SemesterID).ToList();

				return new ReportDataSet
				{
					PageHeader = new ExamResultPageHeader
					{
						Institute = aspireContext.Institutes.Where(i => i.InstituteID == instituteID).Select(i => i.InstituteName).Single(),
					},
					StudentInformation = studentInformation,
					ExamResults = result,
				};
			}
		}
	}
}
