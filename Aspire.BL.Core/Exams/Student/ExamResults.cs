﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Student
{
	public static class ExamResults
	{
		public sealed class StudentSemester
		{
			internal StudentSemester() { }
			public short SemesterID { get; internal set; }
			public decimal? GPA { get; internal set; }
			public decimal? CGPA { get; internal set; }
			public Model.Entities.StudentSemester.ResultRemarksTypes? ExamRemarksTypeEnum { get; internal set; }
			public decimal? BUExamGPA { get; internal set; }
			public decimal? BUExamCGPA { get; internal set; }
			public Model.Entities.StudentSemester.ResultRemarksTypes? BUExamResultRemarksEnum { get; internal set; }
			public List<RegisteredCourse> RegisteredCourses { get; internal set; }

			public sealed class RegisteredCourse
			{
				internal RegisteredCourse()
				{
				}

				public string CourseCode { get; internal set; }
				public string Title { get; internal set; }
				public string Majors { get; internal set; }
				public decimal CreditHours { get; internal set; }
				public ExamGrades? GradeEnum { get; internal set; }
				public decimal? GradePoints { get; internal set; }
				public decimal? Product { get; internal set; }
			}
		}

		public static List<StudentSemester> GetExamResult(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStudentActiveSession(loginSessionGuid, null);
				var validStudentStatuses = new[]
				{
					Model.Entities.Student.Statuses.Deferred,
					Model.Entities.Student.Statuses.Graduated,
					Model.Entities.Student.Statuses.Dropped,
					Model.Entities.Student.Statuses.ProgramChanged,
					Model.Entities.Student.Statuses.TransferredToOtherCampus,
				}.Select(s => (byte?)s).ToList();

				return aspireContext.StudentSemesters
					.Where(ss => ss.StudentID == loginHistory.StudentID)
					.Where(ss => ss.Student.Status == null || validStudentStatuses.Contains(ss.Student.Status))
					.Select(ss => new StudentSemester
					{
						SemesterID = ss.SemesterID,
						BUExamGPA = ss.BUExamGPA,
						BUExamCGPA = ss.BUExamCGPA,
						BUExamResultRemarksEnum = (Model.Entities.StudentSemester.ResultRemarksTypes)ss.BUExamResultRemarks,
						GPA = ss.GPA,
						CGPA = ss.CGPA,
						ExamRemarksTypeEnum = (Model.Entities.StudentSemester.ResultRemarksTypes)ss.ExamRemarksType,
						RegisteredCourses = aspireContext.RegisteredCourses
							.Where(rc => rc.DeletedDate == null)
							.Where(rc => rc.StudentID == ss.StudentID && rc.OfferedCours.SemesterID == ss.SemesterID)
							.Select(rc => new StudentSemester.RegisteredCourse
							{
								CourseCode = rc.Cours.CourseCode,
								Title = rc.Cours.Title,
								CreditHours = rc.Cours.CreditHours,
								GradeEnum = rc.OfferedCours.MarksEntryLocked && rc.OfferedCours.MarksEntryCompleted && rc.OfferedCours.SubmittedToUniversityDate != null ? (ExamGrades?)rc.Grade : null,
								GradePoints = rc.OfferedCours.MarksEntryLocked && rc.OfferedCours.MarksEntryCompleted && rc.OfferedCours.SubmittedToUniversityDate != null ? rc.GradePoints : null,
								Product = rc.OfferedCours.MarksEntryLocked && rc.OfferedCours.MarksEntryCompleted && rc.OfferedCours.SubmittedToUniversityDate != null ? rc.Product : null
							}).ToList()
					})
					.ToList();
			}
		}
	}
}
