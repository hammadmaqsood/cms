﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Staff
{
	public static class ExportData
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class RegisteredCourse
		{
			internal RegisteredCourse() { }

			public decimal? Assignments { get; internal set; }
			public string CourseCode { get; internal set; }
			public int CourseID { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public string Enrollment { get; internal set; }
			public decimal? Final { get; internal set; }
			public byte? FreezedStatus { get; internal set; }
			public FreezedStatuses? FreezedStatusEnum => (FreezedStatuses?)this.FreezedStatus;
			public string FreezedStatusFullName => this.FreezedStatusEnum?.ToFullName();
			public byte? Grade { get; internal set; }
			public ExamGrades? GradeEnum => (ExamGrades?)this.Grade;
			public string GradeFullName
			{
				get
				{
					switch (this.StatusEnum)
					{
						case null:
						case RegisteredCours.Statuses.AttendanceDefaulter:
						case RegisteredCours.Statuses.Deferred:
						case RegisteredCours.Statuses.Incomplete:
						case RegisteredCours.Statuses.UnfairMeans:
							return this.GradeEnum?.ToFullName();
						case RegisteredCours.Statuses.WithdrawWithFee:
						case RegisteredCours.Statuses.WithdrawWithoutFee:
							return "W";
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
			public decimal? GradePoints { get; internal set; }
			public decimal? Product { get; internal set; }
			public string InstituteAlias { get; internal set; }
			public decimal? Internals { get; set; }
			public decimal? Mid { get; internal set; }
			public string Name { get; internal set; }
			public int OfferedCourseID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public decimal? Quizzes { get; internal set; }
			public int RegisteredCourseID { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public int Section { get; internal set; }
			public Sections SectionEnum => (Sections)this.Section;
			public string SectionFullName => this.SectionEnum.ToFullName();
			public short SemesterID { get; internal set; }
			public short Year => Model.Entities.Semester.GetYear(this.SemesterID);
			public SemesterTypes SemesterTypeEnum => Model.Entities.Semester.GetSemesterType(this.SemesterID);
			public byte SemesterType => (byte)this.SemesterTypeEnum;
			public string Semester => this.SemesterID.ToSemesterString();
			public short SemesterNo { get; internal set; }
			public SemesterNos SemesterNoEnum => (SemesterNos)this.SemesterNo;
			public string SemesterNoFullName => this.SemesterNoEnum.ToFullName();
			public byte Shift { get; internal set; }
			public Shifts ShiftEnum => (Shifts)this.Shift;
			public string ShiftFullName => this.ShiftEnum.ToFullName();
			public byte? Status { get; internal set; }
			public RegisteredCours.Statuses? StatusEnum => (RegisteredCours.Statuses?)this.Status;
			public string StatusFullName => this.StatusEnum?.ToFullName();
			public string FullStatus => AspireFormats.GetRegisteredCourseStatusString(this.Status, this.FreezedStatus);
			public byte? StudentFeeStatus { get; internal set; }
			public StudentFee.Statuses? StudentFeeStatusEnum => (StudentFee.Statuses?)this.StudentFeeStatus;
			public string StudentFeeStatusFullName => this.StudentFeeStatusEnum?.ToFullName();
			public int Attendance => (this.StudentFeeStatusEnum == null || this.StudentFeeStatusEnum == StudentFee.Statuses.NotPaid) ? 0 : 1;
			public string Title { get; internal set; }
			public byte? Total { get; internal set; }
		}

		public static List<RegisteredCourse> GetData(short? semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.RegisteredCourses
					.Where(rc => rc.OfferedCours.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(rc => semesterID == null || rc.OfferedCours.SemesterID == semesterID)
					.Where(rc => rc.OfferedCours.MarksEntryLocked && rc.OfferedCours.MarksEntryCompleted && rc.OfferedCours.SubmittedToUniversityDate != null)
					.Where(rc => rc.DeletedDate == null)
					.Select(rc => new RegisteredCourse
					{
						RegisteredCourseID = rc.RegisteredCourseID,
						RegistrationNo = rc.Student.RegistrationNo,
						Name = rc.Student.Name,
						Enrollment = rc.Student.Enrollment,
						OfferedCourseID = rc.OfferedCourseID,
						CourseCode = rc.Cours.CourseCode,
						Title = rc.Cours.Title,
						ProgramAlias = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						CourseID = rc.CourseID,
						Section = rc.OfferedCours.Section,
						SemesterNo = rc.OfferedCours.SemesterNo,
						Shift = rc.OfferedCours.Shift,
						CreditHours = rc.Cours.CreditHours,
						SemesterID = rc.OfferedCours.SemesterID,
						Status = rc.Status,
						FreezedStatus = rc.FreezedStatus,
						StudentFeeStatus = rc.StudentFee.Status,
						GradePoints = rc.GradePoints,
						Product = rc.Product,
						Assignments = rc.Assignments,
						Quizzes = rc.Quizzes,
						Internals = rc.Internals,
						Mid = rc.Mid,
						Final = rc.Final,
						Total = rc.Total,
						Grade = rc.Grade,
						InstituteAlias = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.Institute.InstituteAlias,
					}).ToList();
			}
		}

		public sealed class ThesisInternshipRecord
		{
			internal ThesisInternshipRecord() { }
			public string InstituteAlias { get; internal set; }
			public string Enrollment { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public string CreditHoursFullName => this.CreditHours.ToCreditHoursFullName();
			public DateTime? DefenceDate { get; internal set; }
			public DateTime? FromDate { get; internal set; }
			public DateTime? ToDate { get; internal set; }
			public string Title { get; internal set; }
			public byte Grade { get; internal set; }
			public string GradeFullName => ((ExamGrades)this.Grade).ToFullName();
			public string Organization { get; internal set; }
			public ThesisInternship.RecordTypes RecordTypeEnum { get; internal set; }
			public string RecordTypeFullName => this.RecordTypeEnum.ToString();
			public DateTime? SubmissionDate { get; internal set; }
			public decimal ObtainedMarks { get; internal set; }
			public decimal TotalMarks => 100;
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
		}

		public static List<ThesisInternshipRecord> GetThesisProjectData(short? semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.ThesisInternships
					.Where(ti => ti.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(ti => semesterID == null || ti.SemesterID == semesterID.Value)
					.Select(ti => new ThesisInternshipRecord
					{
						InstituteAlias = ti.Student.AdmissionOpenProgram.Program.Institute.InstituteAlias,
						Enrollment = ti.Student.Enrollment,
						RegistrationNo = ti.Student.RegistrationNo,
						ProgramAlias = ti.Student.AdmissionOpenProgram.Program.ProgramAlias,
						CreditHours = ti.CreditHours,
						DefenceDate = ti.DefenceDate,
						FromDate = ti.FromDate,
						ToDate = ti.ToDate,
						Title = ti.Title,
						Grade = ti.Grade,
						Organization = ti.Organization,
						RecordTypeEnum = (ThesisInternship.RecordTypes)ti.RecordType,
						SubmissionDate = ti.SubmissionDate,
						ObtainedMarks = ti.Total,
						SemesterID = ti.SemesterID
					}).OrderBy(ti => ti.Enrollment).ToList();
			}
		}

		public sealed class StudentSemesterResult
		{
			internal StudentSemesterResult() { }
			public string InstituteAlias { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string ProgramDuration { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public string IntakeSemester { get; internal set; }
			public string Enrollment { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string Name { get; internal set; }
			public string FatherName { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester { get; internal set; }
			public string GPA { get; internal set; }
			public string CGPA { get; internal set; }
			public string Remarks { get; internal set; }
		}

		public static List<StudentSemesterResult> GetCGPARecords(short fromIntakeSemesterID, short uptoOfferedSemesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.StudentSemesters
								  .Where(ss => ss.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
								  .Where(ss => ss.Student.AdmissionOpenProgram.SemesterID >= fromIntakeSemesterID)
								  .Where(ss => ss.SemesterID <= uptoOfferedSemesterID)
								  .Select(ss => new
								  {
									  ss.Student.AdmissionOpenProgram.Program.InstituteID,
									  ss.Student.AdmissionOpenProgram.Program.Institute.InstituteAlias,
									  ss.Student.AdmissionOpenProgram.Program.ProgramAlias,
									  ProgramDuration = (ProgramDurations)ss.Student.AdmissionOpenProgram.Program.Duration,
									  IntakeSemesterID = ss.Student.AdmissionOpenProgram.SemesterID,
									  ss.Student.Enrollment,
									  ss.Student.RegistrationNo,
									  ss.Student.Name,
									  ss.Student.FatherName,
									  ss.SemesterID,
									  ss.GPA,
									  ss.CGPA,
									  ExamRemarksTypeEnum = (ExamRemarksTypes?)ss.ExamRemarksType,
								  })
								  .OrderBy(ss => ss.InstituteID)
								  .ThenBy(ss => ss.Enrollment)
								  .ThenBy(ss => ss.SemesterID)
								  .ToList()
								  .Select(ss => new StudentSemesterResult
								  {
									  InstituteAlias = ss.InstituteAlias,
									  ProgramAlias = ss.ProgramAlias,
									  ProgramDuration = ss.ProgramDuration.ToFullName(),
									  IntakeSemesterID = ss.IntakeSemesterID,
									  IntakeSemester = ss.IntakeSemesterID.ToSemesterString(),
									  Enrollment = ss.Enrollment,
									  RegistrationNo = ss.RegistrationNo,
									  Name = ss.Name,
									  FatherName = ss.FatherName,
									  SemesterID = ss.SemesterID,
									  Semester = ss.SemesterID.ToSemesterString(),
									  GPA = ss.GPA.FormatGPA(),
									  CGPA = ss.CGPA.FormatGPA(),
									  Remarks = ss.ExamRemarksTypeEnum?.ToFullName()
								  }).ToList();
			}
		}
	}
}