﻿using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Entities;

namespace Aspire.BL.Core.Exams.Staff
{
	public static class ExamResults
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissionsStaff(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Executive.ExecutiveGroupPermission DemandModulePermissionsExecutive(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandExecutivePermissions(loginSessionGuid, ExecutivePermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}
		public sealed class ExamResult
		{
			internal ExamResult()
			{
			}

			public short SemesterID { get; internal set; }
			public int CourseID { get; internal set; }
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public string Teacher { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public decimal? Assignments { get; internal set; }
			public decimal? Quizzes { get; internal set; }
			public decimal? Mid { get; internal set; }
			public decimal? Final { get; internal set; }
			public byte? Total { get; internal set; }
			public byte? Grade { get; internal set; }
			public decimal? GradePoint { get; internal set; }
			public decimal? GPA { get; internal set; }
			public decimal? CGPA { get; internal set; }
			public string Remarks { get; internal set; }
			public byte? Status { get; internal set; }
			public byte? FreezedStatus { get; internal set; }
			public byte? StudentFeeStatus { get; internal set; }
			public decimal? TotalHours { get; internal set; }
			public decimal? PresentHours { get; internal set; }
			public decimal AbsentHours => this.TotalHours - this.PresentHours ?? 0;
			public decimal Percentage => this.TotalHours == 0 ? 0 : (this.PresentHours ?? 0) * 100 / (this.TotalHours ?? 1);

			public string Semester => this.SemesterID.ToSemesterString();
			public string StringCreditHours => this.CreditHours.ToCreditHoursFullName();
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public string StatusComplete => AspireFormats.GetRegisteredCourseStatusString(this.Status, this.FreezedStatus).ToNAIfNullOrEmpty();
			public string StudentFeeStatusFullName => ((StudentFee.Statuses?)this.StudentFeeStatus)?.ToFullName() ?? StudentFee.Statuses.NotPaid.ToFullName();
			public string GradeEnumFullName => ((ExamGrades?)this.Grade)?.ToFullName();
			public string AssignmentsString => (this.Assignments)?.ToString("0.##");
			public string QuizzesString => (this.Quizzes)?.ToString("0.##");
			public string MidString => (this.Mid)?.ToString("0.##");
			public string FinalString => (this.Final)?.ToString("0.##");
			public string Product => (this.CreditHours * this.GradePoint)?.ToString("0.##");
			public string PercentageShort => this.Percentage.ToString("0.##");

		}

		public static List<ExamResult> GetExamResult(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Executive:
						var executiveLoginHistory = aspireContext.DemandModulePermissionsExecutive(loginSessionGuid);
						break;
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandModulePermissionsStaff(loginSessionGuid);
						break;
					case UserTypes.Student:
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				var registeredCourses = aspireContext.RegisteredCourses
					.Where(rc => rc.StudentID == studentID && rc.DeletedDate == null)
					.Select(rc => new
					{
						rc.OfferedCours.SemesterID,
						rc.RegisteredCourseID,
						rc.StudentID,
						rc.OfferedCourseID,
						rc.CourseID,
						rc.Cours.CourseCode,
						rc.Cours.Title,
						rc.Cours.CreditHours,
						rc.OfferedCours.FacultyMember.Name,
						rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						rc.OfferedCours.SemesterNo,
						rc.OfferedCours.Section,
						rc.OfferedCours.Shift,
						rc.Assignments,
						rc.Quizzes,
						rc.Mid,
						rc.Final,
						rc.Total,
						rc.Grade,
						rc.GradePoint,
						rc.Status,
						rc.FreezedStatus,
						StudentFeeStatus = rc.StudentFee.Status,
						TotalHours = rc.OfferedCours.OfferedCourseAttendances.Sum(oca => (decimal?)oca.Hours)??0,
						PresentHours = rc.OfferedCours.OfferedCourseAttendances.SelectMany(oca => oca.OfferedCourseAttendanceDetails).Where(ocad=>ocad.RegisteredCourseID==rc.RegisteredCourseID).Sum(ocad=> (decimal?)ocad.Hours)??0,
						GPA = rc.Student.StudentSemesters.FirstOrDefault(ss => ss.SemesterID == rc.OfferedCours.SemesterID).GPA,
						CGPA = rc.Student.StudentSemesters.FirstOrDefault(ss => ss.SemesterID == rc.OfferedCours.SemesterID).CGPA,
						//Remarks=rc.Student.StudentSemesters.Where(ss=>ss.SemesterID==rc.OfferedCours.SemesterID).Select(ss=>ss.ResultRemarks).FirstOrDefault(),
					}).AsQueryable();

				return registeredCourses.Select(rc => new ExamResult
				{
					SemesterID = rc.SemesterID,
					CourseID = rc.CourseID,
					CourseCode = rc.CourseCode,
					Title = rc.Title,
					CreditHours = rc.CreditHours,
					Teacher = rc.Name,
					ProgramAlias = rc.ProgramAlias,
					SemesterNo = rc.SemesterNo,
					Section = rc.Section,
					Shift = rc.Shift,
					Assignments = rc.Assignments,
					Quizzes = rc.Quizzes,
					Mid = rc.Mid,
					Final = rc.Final,
					Total = rc.Total,
					Grade = rc.Grade,
					GradePoint = rc.GradePoint,
					GPA = rc.GPA,
					CGPA = rc.CGPA,
					Status = rc.Status,
					FreezedStatus = rc.FreezedStatus,
					StudentFeeStatus = rc.StudentFeeStatus,
					TotalHours = rc.TotalHours,
					PresentHours = rc.PresentHours,
				}).OrderBy(o => o.SemesterID).ToList();
			}
		}
	}
}
