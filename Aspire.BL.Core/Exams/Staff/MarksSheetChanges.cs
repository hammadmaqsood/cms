﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Exams.Staff
{
	public static class MarksSheetChanges
	{
		public sealed class MarksSheetChange
		{
			internal MarksSheetChange()
			{
			}

			public int OfferedCourseID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Title { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public Guid MarksSheetChangeID { get; internal set; }
			public DateTime ActionDate { get; internal set; }
			public Guid ActionType { get; internal set; }
			public string ChangedByUserName { get; internal set; }
			public UserTypes ChangedByUserType { get; internal set; }
		}

		public static List<MarksSheetChange> GetMarksSheetChanges(short semesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int? facultyMemberID, string searchText, int pageIndex, int pageSize, out int virtualItemCount, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);

				var offeredCoursesQuery = aspireContext.OfferedCourses
					.Where(oc => oc.SemesterID == semesterID)
					.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);

				if (departmentID != null)
					offeredCoursesQuery = offeredCoursesQuery.Where(c => c.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID);
				if (programID != null)
					offeredCoursesQuery = offeredCoursesQuery.Where(c => c.Cours.AdmissionOpenProgram.ProgramID == programID);
				if (semesterNoEnum != null)
					offeredCoursesQuery = offeredCoursesQuery.Where(c => c.SemesterNo == (short)semesterNoEnum.Value);
				if (sectionEnum != null)
					offeredCoursesQuery = offeredCoursesQuery.Where(c => c.Section == (int)sectionEnum.Value);
				if (shiftEnum != null)
					offeredCoursesQuery = offeredCoursesQuery.Where(c => c.Shift == (byte)shiftEnum.Value);
				if (facultyMemberID != null)
					offeredCoursesQuery = offeredCoursesQuery.Where(c => c.FacultyMemberID == facultyMemberID);

				var marksSheetChangesQuery = from msc in aspireContext.MarksSheetChanges
											 join ulh in aspireContext.UserLoginHistories on msc.LoginSessionGuid equals ulh.LoginSessionGuid
											 join oc in offeredCoursesQuery on msc.OfferedCourseID equals oc.OfferedCourseID
											 select new MarksSheetChange
											 {
												 OfferedCourseID = oc.OfferedCourseID,
												 Title = oc.Cours.Title,
												 ProgramAlias = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
												 SemesterID = oc.SemesterID,
												 SemesterNo = oc.SemesterNo,
												 Section = oc.Section,
												 Shift = oc.Shift,
												 FacultyMemberName = oc.FacultyMember.Name,
												 MarksSheetChangeID = msc.MarksSheetChangesID,
												 ActionDate = msc.ActionDate,
												 ActionType = msc.ActionType,
												 ChangedByUserName = ulh.UserName,
												 ChangedByUserType = (UserTypes)ulh.UserType
											 };

				if (!string.IsNullOrWhiteSpace(searchText))
					marksSheetChangesQuery = marksSheetChangesQuery.Where(msc => msc.ChangedByUserName == searchText);

				virtualItemCount = marksSheetChangesQuery.Count();
				switch (sortExpression)
				{
					case nameof(MarksSheetChange.SemesterID):
						marksSheetChangesQuery = marksSheetChangesQuery.OrderBy(sortDirection, c => c.SemesterID);
						break;
					case nameof(MarksSheetChange.Title):
						marksSheetChangesQuery = marksSheetChangesQuery.OrderBy(sortDirection, c => c.Title);
						break;
					case nameof(MarksSheetChange.FacultyMemberName):
						marksSheetChangesQuery = marksSheetChangesQuery.OrderBy(sortDirection, c => c.FacultyMemberName);
						break;
					case nameof(MarksSheetChange.ChangedByUserName):
						marksSheetChangesQuery = marksSheetChangesQuery.OrderBy(sortDirection, c => c.ChangedByUserName);
						break;
					case nameof(MarksSheetChange.ChangedByUserType):
						marksSheetChangesQuery = marksSheetChangesQuery.OrderBy(sortDirection, c => c.ChangedByUserType);
						break;
					case nameof(MarksSheetChange.ActionDate):
						marksSheetChangesQuery = marksSheetChangesQuery.OrderBy(sortDirection, c => c.ActionDate);
						break;
					case nameof(MarksSheetChange.ActionType):
						marksSheetChangesQuery = marksSheetChangesQuery.OrderBy(sortDirection, c => c.ActionType);
						break;
					case nameof(MarksSheetChange.Class):
						marksSheetChangesQuery = marksSheetChangesQuery
							.OrderBy(sortDirection, c => c.ProgramAlias)
							.ThenBy(sortDirection, c => c.SemesterNo)
							.ThenBy(sortDirection, c => c.Section)
							.ThenBy(sortDirection, c => c.Shift);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return marksSheetChangesQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList();
			}
		}
	}
}