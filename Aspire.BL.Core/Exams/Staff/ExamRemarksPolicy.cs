﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.Exams.Staff
{
	public static class ExamRemarksPolicy
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.ManageExamRemarksPolicies, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class GetExamRemarksPoliciesForCrossTabGridResult
		{
			internal GetExamRemarksPoliciesForCrossTabGridResult()
			{
			}

			public List<Model.Entities.ExamRemarksPolicy> ExamRemarksPolicies { get; internal set; }
			public List<Program> Programs { get; internal set; }
			public List<short> SemesterIDs { get; internal set; }

			public sealed class Program
			{
				internal Program()
				{
				}
				public int ProgramID { get; internal set; }
				public string ProgramAlias { get; internal set; }
				public List<AdmissionOpenProgram> AdmissionOpenPrograms { get; internal set; }

				public sealed class AdmissionOpenProgram
				{
					internal AdmissionOpenProgram() { }
					public int AdmissionOpenProgramID { get; internal set; }
					public short IntakeSemesterID { get; internal set; }
					public int Students { get; internal set; }
					public int? ExamRemarksPolicyID { get; internal set; }
				}
			}

			public sealed class CrossTabRow
			{
				public int ProgramID { get; internal set; }
				public string ProgramAlias { get; internal set; }
				public List<Policy> Policies { get; internal set; }

				public sealed class Policy
				{
					internal Policy() { }
					public short IntakeSemesterID { get; internal set; }
					public int? AdmissionOpenProgramID { get; internal set; }
					public Program Program { get; internal set; }
					public Model.Entities.ExamRemarksPolicy ExamRemarksPolicy { get; internal set; }
					public int? Students { get; internal set; }
					public bool Valid => this.ExamRemarksPolicy != null
										&& this.ExamRemarksPolicy.ValidFromSemesterID <= this.IntakeSemesterID
										&& (this.ExamRemarksPolicy.ValidUpToSemesterID == null
											|| this.IntakeSemesterID <= this.ExamRemarksPolicy.ValidUpToSemesterID.Value);
				}
			}

			private List<CrossTabRow> _crossTabRows;
			public List<CrossTabRow> CrossTabRows
			{
				get
				{
					if (this._crossTabRows != null)
						return this._crossTabRows;
					this.SemesterIDs = this.Programs.SelectMany(p => p.AdmissionOpenPrograms).Select(aop => aop.IntakeSemesterID).Distinct().OrderByDescending(s => s).ToList();
					return this._crossTabRows = this.Programs
						.OrderBy(p => p.ProgramAlias)
						.Select(p => new CrossTabRow
						{
							ProgramID = p.ProgramID,
							ProgramAlias = p.ProgramAlias,
							Policies = this.SemesterIDs.Select(s =>
							{
								var admissionOpenProgram = p.AdmissionOpenPrograms.SingleOrDefault(aop => aop.IntakeSemesterID == s);
								return new CrossTabRow.Policy
								{
									IntakeSemesterID = s,
									AdmissionOpenProgramID = admissionOpenProgram?.AdmissionOpenProgramID,
									Program = p,
									Students = admissionOpenProgram?.Students,
									ExamRemarksPolicy = this.ExamRemarksPolicies.SingleOrDefault(erp => erp.ExamRemarksPolicyID == admissionOpenProgram?.ExamRemarksPolicyID),
								};
							}).ToList()
						}).ToList();
				}
			}
		}

		public static GetExamRemarksPoliciesForCrossTabGridResult GetExamRemarksPoliciesForCrossTabGrid(int? departmentID, int? programID, short? intakeSemesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return new GetExamRemarksPoliciesForCrossTabGridResult
				{
					ExamRemarksPolicies = aspireContext.ExamRemarksPolicies.Include(erp => erp.ExamRemarksPolicyDetails).Where(erp => erp.InstituteID == loginHistory.InstituteID).ToList(),
					Programs = aspireContext.Institutes
						.Where(i => i.InstituteID == loginHistory.InstituteID)
						.SelectMany(i => i.Programs)
						.Where(p => departmentID == null || p.DepartmentID == departmentID.Value)
						.Where(p => programID == null || p.ProgramID == programID.Value)
						.Select(p => new GetExamRemarksPoliciesForCrossTabGridResult.Program
						{
							ProgramID = p.ProgramID,
							ProgramAlias = p.ProgramAlias,
							AdmissionOpenPrograms = p.AdmissionOpenPrograms
								.Where(aop => intakeSemesterID == null || aop.SemesterID == intakeSemesterID.Value)
								.Select(aop => new GetExamRemarksPoliciesForCrossTabGridResult.Program.AdmissionOpenProgram
								{
									AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
									IntakeSemesterID = aop.SemesterID,
									ExamRemarksPolicyID = aop.ExamRemarksPolicyID,
									Students = aop.Students.Count
								}).ToList(),
						}).ToList()
				};
			}
		}

		public static List<Model.Entities.ExamRemarksPolicy> GetExamRemarksPolicies(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.ExamRemarksPolicies
					.Include(erp => erp.ExamRemarksPolicyDetails)
					.Where(erp => erp.InstituteID == loginHistory.InstituteID)
					.OrderBy(erp => erp.ExamRemarksPolicyName)
					.ToList();
			}
		}

		private static bool? AssignUnAssignExamRemarksPolicy(this AspireContext aspireContext, int programID, short intakeSemesterID, int? examRemarksPolicyID, Guid loginSessionGuid)
		{
			var record = aspireContext.AdmissionOpenPrograms
				.Where(aop => aop.ProgramID == programID && aop.SemesterID == intakeSemesterID)
				.Select(aop => new
				{
					AdmissionOpenProgram = aop,
					aop.Program.DepartmentID,
					aop.Program.InstituteID
				}).SingleOrDefault();
			if (record == null)
				return null;
			var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.AdmissionOpenProgram.ProgramID, record.AdmissionOpenProgram.AdmissionOpenProgramID);
			if (examRemarksPolicyID == null)
			{
				if (record.AdmissionOpenProgram.ExamRemarksPolicyID != null)
				{
					record.AdmissionOpenProgram.ExamRemarksPolicyID = null;
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
					return true;
				}
				return false;
			}

			var examRemarksPolicy = aspireContext.ExamRemarksPolicies
				.Where(erp => erp.ExamRemarksPolicyID == examRemarksPolicyID && erp.InstituteID == loginHistory.InstituteID)
				.Where(erp => erp.ValidFromSemesterID <= record.AdmissionOpenProgram.SemesterID)
				.Where(erp => erp.ValidUpToSemesterID == null || record.AdmissionOpenProgram.SemesterID <= erp.ValidUpToSemesterID)
				.Select(erp => new
				{
					erp.ExamRemarksPolicyID,
				}).SingleOrDefault();

			if (record.AdmissionOpenProgram.ExamRemarksPolicyID != examRemarksPolicy?.ExamRemarksPolicyID)
			{
				record.AdmissionOpenProgram.ExamRemarksPolicyID = examRemarksPolicy?.ExamRemarksPolicyID;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				return true;
			}

			return false;
		}

		public sealed class AssignUnAssignExamRemarksPolicyResult
		{
			public int TotalRecords { get; }
			public int ValidRecords { get; }
			public int UpdatedRecords { get; }

			internal AssignUnAssignExamRemarksPolicyResult(IReadOnlyCollection<bool?> results)
			{
				this.TotalRecords = results.Count;
				this.ValidRecords = results.Count(r => r != null);
				this.UpdatedRecords = results.Count(r => r == true);
			}
		}

		public static AssignUnAssignExamRemarksPolicyResult AssignUnAssignExamRemarksPolicy(List<int> programIDs, short intakeSemesterIDFrom, short intakeSemesterIDTo, int? examRemarksPolicyID, Guid loginSessionGuid)
		{
			if (intakeSemesterIDFrom > intakeSemesterIDTo)
				throw new ArgumentException(nameof(intakeSemesterIDFrom));
			var results = new List<bool?>();
			using (var aspireContext = new AspireContext(true))
			{
				foreach (var programID in programIDs)
				{
					var intakeSemesterID = intakeSemesterIDFrom;
					while (intakeSemesterID <= intakeSemesterIDTo)
					{
						results.Add(aspireContext.AssignUnAssignExamRemarksPolicy(programID, intakeSemesterID, examRemarksPolicyID, loginSessionGuid));
						intakeSemesterID = Semester.AddSemesters(intakeSemesterID, 1, true);
					}
				}

				var result = new AssignUnAssignExamRemarksPolicyResult(results.AsReadOnly());
				if (result.UpdatedRecords > 0)
					aspireContext.CommitTransaction();
				return result;
			}
		}
	}
}
