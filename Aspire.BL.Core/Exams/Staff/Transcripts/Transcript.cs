﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.Exams.Common.Transcripts;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.Exams.Staff.Transcripts
{
	public static class Transcript
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.CompileCGPAAndRemarks, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public static Common.Transcripts.Transcript GetTranscript(short? compileUpToSemesterID, int? studentID, string enrollment, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				if (studentID == null)
					studentID = aspireContext.GetStudentIDFromEnrollment(loginHistory.InstituteID, enrollment);
				if (studentID == null)
					return null;
				return aspireContext.GetTranscript(studentID.Value, compileUpToSemesterID);
			}
		}

		public static GenerateTranscripts.CompileResultStatuses CompileResult(short? compileUpToSemesterID, int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.Students
					.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.StudentID,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgramID,
					})
					.SingleOrDefault();
				if (student == null)
					return GenerateTranscripts.CompileResultStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				var (status, _) = aspireContext.CompileResult(compileUpToSemesterID, student.StudentID, loginHistory.UserLoginHistoryID);
				switch (status)
				{
					case GenerateTranscripts.CompileResultStatuses.NoRecordFound:
					case GenerateTranscripts.CompileResultStatuses.PreFall2016NotSupported:
					case GenerateTranscripts.CompileResultStatuses.NoChangesFound:
						break;
					case GenerateTranscripts.CompileResultStatuses.Compiled:
						aspireContext.CommitTransaction();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				return status;
			}
		}
	}
}
