﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Staff
{
	public static class ExamMarksPolicy
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.ManageExamMarksPolicies, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class GetExamMarksPoliciesForCrossTabGridResult
		{
			internal GetExamMarksPoliciesForCrossTabGridResult()
			{
			}

			public List<Model.Entities.ExamMarksPolicy> ExamMarksPolicies { get; internal set; }
			public List<short> SemesterIDs { get; internal set; }
			public List<Program> Programs { get; internal set; }

			public sealed class Program
			{
				internal Program()
				{
				}
				public int ProgramID { get; internal set; }
				public string ProgramAlias { get; internal set; }
				public List<AdmissionOpenProgram> AdmissionOpenPrograms { get; internal set; }

				public sealed class AdmissionOpenProgram
				{
					internal AdmissionOpenProgram() { }
					public int AdmissionOpenProgramID { get; internal set; }
					public short IntakeSemesterID { get; internal set; }
					public short? FinalSemesterID { get; internal set; }
					public short? MaxSemesterID { get; internal set; }
					public List<AdmissionOpenProgramExamMarksPolicy> AdmissionOpenProgramExamMarksPolicies { get; internal set; }
				}
			}

			public sealed class CrossTabRow
			{
				public int ProgramID { get; internal set; }
				public string ProgramAlias { get; internal set; }
				public short IntakeSemesterID { get; internal set; }
				public int AdmissionOpenProgramID { get; internal set; }
				public List<Policy> Policies { get; internal set; }

				public sealed class Policy
				{
					internal Policy() { }
					public short SemesterID { get; internal set; }
					public Model.Entities.ExamMarksPolicy ExamMarksPolicy { get; internal set; }
					public bool RegistrationFound { get; internal set; }
					public int AdmissionOpenProgramID { get; internal set; }
					public int ProgramID { get; internal set; }
					public string ProgramAlias { get; internal set; }
					public short IntakeSemesterID { get; internal set; }
					public string Tooltip => $"{this.ProgramAlias} Intake: {this.IntakeSemesterID.ToSemesterString()}, Semester: {this.SemesterID.ToSemesterString()}";
					public string CellCssClass
					{
						get
						{
							if (this.SemesterID < this.IntakeSemesterID)
								return "danger";
							if (this.ExamMarksPolicy != null)
							{
								if (this.SemesterID < this.ExamMarksPolicy.ValidFromSemesterID)
									return "danger";
								if (this.ExamMarksPolicy.ValidUpToSemesterID != null && this.ExamMarksPolicy.ValidUpToSemesterID < this.SemesterID)
									return "danger";
								if (this.RegistrationFound == false)
									return "warning";
								return "success";
							}
							if (this.RegistrationFound)
								return "danger";
							return "";
						}
					}
				}
			}

			private List<CrossTabRow> _crossTabRows;
			public List<CrossTabRow> CrossTabRows
			{
				get
				{
					if (this._crossTabRows != null)
						return this._crossTabRows;
					return this._crossTabRows = this.Programs
						.SelectMany(p => p.AdmissionOpenPrograms
							.OrderByDescending(aop => aop.IntakeSemesterID)
							.Select(aop => new CrossTabRow
							{
								ProgramID = p.ProgramID,
								ProgramAlias = p.ProgramAlias,
								AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
								IntakeSemesterID = aop.IntakeSemesterID,
								Policies = this.SemesterIDs
									.Select(s =>
									{
										var examMarksPolicyID = aop.AdmissionOpenProgramExamMarksPolicies.SingleOrDefault(aopemp => aopemp.SemesterID == s)?.ExamMarksPolicyID;
										return new CrossTabRow.Policy
										{
											ProgramID = p.ProgramID,
											ProgramAlias = p.ProgramAlias,
											AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
											IntakeSemesterID = aop.IntakeSemesterID,
											SemesterID = s,
											RegistrationFound = this.RegisteredCourses.Any(rc => rc.AdmissionOpenProgramID == aop.AdmissionOpenProgramID && rc.SemesterID == s),
											ExamMarksPolicy = this.ExamMarksPolicies.SingleOrDefault(ep => ep.ExamMarksPolicyID == examMarksPolicyID),
										};
									}).ToList(),
							})).Where(aop => aop.Policies.Any(p => p.RegistrationFound)).ToList();
				}
			}

			internal List<RegisteredCourse> RegisteredCourses { get; set; }

			internal sealed class RegisteredCourse
			{
				public int AdmissionOpenProgramID { get; internal set; }
				public short SemesterID { get; internal set; }
			}
		}

		public static GetExamMarksPoliciesForCrossTabGridResult GetExamMarksPoliciesForCrossTabGrid(int? departmentID, int? programID, short? intakeSemesterID, short? semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var result = new GetExamMarksPoliciesForCrossTabGridResult
				{
					ExamMarksPolicies = aspireContext.ExamMarksPolicies.Where(emp => emp.InstituteID == loginHistory.InstituteID).ToList(),
					Programs = aspireContext.Institutes
						.Where(i => i.InstituteID == loginHistory.InstituteID)
						.SelectMany(i => i.Programs)
						.Where(p => departmentID == null || p.DepartmentID == departmentID.Value)
						.Where(p => programID == null || p.ProgramID == programID.Value)
						.Select(p => new GetExamMarksPoliciesForCrossTabGridResult.Program
						{
							ProgramID = p.ProgramID,
							ProgramAlias = p.ProgramAlias,
							AdmissionOpenPrograms = p.AdmissionOpenPrograms
								.Where(aop => intakeSemesterID == null || aop.SemesterID == intakeSemesterID.Value)
								.Where(aop => semesterID == null || aop.SemesterID <= semesterID.Value)
								.Select(aop => new GetExamMarksPoliciesForCrossTabGridResult.Program.AdmissionOpenProgram
								{
									AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
									IntakeSemesterID = aop.SemesterID,
									FinalSemesterID = aop.FinalSemesterID,
									MaxSemesterID = aop.MaxSemesterID,
									AdmissionOpenProgramExamMarksPolicies = aop.AdmissionOpenProgramExamMarksPolicies
										.Where(aopep => semesterID == null || aopep.SemesterID == semesterID.Value)
										.ToList(),
								}).ToList(),
						}).ToList(),
					RegisteredCourses = aspireContext.RegisteredCourses
						.Where(rc => rc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
						.Select(rc => new GetExamMarksPoliciesForCrossTabGridResult.RegisteredCourse
						{
							AdmissionOpenProgramID = rc.Cours.AdmissionOpenProgramID,
							SemesterID = rc.OfferedCours.SemesterID
						}).Distinct().ToList(),
					SemesterIDs = aspireContext.Semesters.Select(s => s.SemesterID)
						.Where(s => intakeSemesterID == null || intakeSemesterID <= s)
						.Where(s => semesterID == null || s == semesterID.Value)
						.ToList()
				};
				var minIntakeSemesterID = result.Programs.SelectMany(p => p.AdmissionOpenPrograms).Min(aop => (short?)aop.IntakeSemesterID);
				if (minIntakeSemesterID == null)
					result.SemesterIDs.Clear();
				else
					result.SemesterIDs = result.SemesterIDs.Where(s => s >= minIntakeSemesterID).OrderByDescending(s => s).ToList();
				return result;
			}
		}

		public static List<Model.Entities.ExamMarksPolicy> GetExamMarksPolicies(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.ExamMarksPolicies.Where(emp => emp.InstituteID == loginHistory.InstituteID).OrderBy(emp => emp.ExamMarksPolicyName).ToList();
			}
		}

		private static bool? AssignUnAssignExamMarksPolicy(this AspireContext aspireContext, int programID, short intakeSemesterID, short semesterID, int? examMarksPolicyID, Guid loginSessionGuid)
		{
			if (intakeSemesterID > semesterID)
				throw new ArgumentException("Must be less than or equal to semesterID.", nameof(intakeSemesterID));
			var admissionOpenProgram = aspireContext.AdmissionOpenPrograms.Where(aop => aop.ProgramID == programID && aop.SemesterID == intakeSemesterID)
				.Select(aop => new
				{
					aop.AdmissionOpenProgramID,
					aop.SemesterID,
					aop.ProgramID,
					aop.Program.DepartmentID,
					aop.Program.InstituteID,
					AdmissionOpenProgramExamMarksPolicy = aop.AdmissionOpenProgramExamMarksPolicies.FirstOrDefault(aopemp => aopemp.SemesterID == semesterID),
				}).SingleOrDefault();
			if (admissionOpenProgram == null)
				return null;
			var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, admissionOpenProgram.InstituteID, admissionOpenProgram.DepartmentID, admissionOpenProgram.ProgramID, admissionOpenProgram.AdmissionOpenProgramID);
			if (examMarksPolicyID == null)
			{
				if (admissionOpenProgram.AdmissionOpenProgramExamMarksPolicy != null)
				{
					aspireContext.AdmissionOpenProgramExamMarksPolicies.Remove(admissionOpenProgram.AdmissionOpenProgramExamMarksPolicy);
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
					return true;
				}
				return false;
			}
			var examMarksPolicy = aspireContext.ExamMarksPolicies
			   .Where(emp => emp.ExamMarksPolicyID == examMarksPolicyID && emp.InstituteID == loginHistory.InstituteID)
			   .Select(emp => new
			   {
				   emp.ExamMarksPolicyID,
				   emp.ValidFromSemesterID,
				   emp.ValidUpToSemesterID
			   }).Single();

			if (admissionOpenProgram.SemesterID < examMarksPolicy.ValidFromSemesterID)
				return false;
			if (examMarksPolicy.ValidUpToSemesterID != null && examMarksPolicy.ValidUpToSemesterID.Value < admissionOpenProgram.SemesterID)
				return false;

			var admissionOpenProgramExamMarksPolicy = admissionOpenProgram.AdmissionOpenProgramExamMarksPolicy;
			if (admissionOpenProgramExamMarksPolicy != null)
			{
				if (admissionOpenProgramExamMarksPolicy.ExamMarksPolicyID == examMarksPolicy.ExamMarksPolicyID)
					return false;
				admissionOpenProgramExamMarksPolicy.ExamMarksPolicyID = examMarksPolicy.ExamMarksPolicyID;
			}
			else
				aspireContext.AdmissionOpenProgramExamMarksPolicies.Add(new AdmissionOpenProgramExamMarksPolicy
				{
					AdmissionOpenProgramID = admissionOpenProgram.AdmissionOpenProgramID,
					SemesterID = semesterID,
					ExamMarksPolicyID = examMarksPolicy.ExamMarksPolicyID
				});
			aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
			return true;
		}

		public sealed class AssignUnAssignExamMarksPolicyResult
		{
			public int TotalRecords { get; }
			public int ValidRecords { get; }
			public int UpdatedRecords { get; }

			internal AssignUnAssignExamMarksPolicyResult(IReadOnlyCollection<bool?> results)
			{
				this.TotalRecords = results.Count;
				this.ValidRecords = results.Count(r => r != null);
				this.UpdatedRecords = results.Count(r => r == true);
			}
		}

		public static AssignUnAssignExamMarksPolicyResult AssignUnAssignExamMarksPolicy(List<int> programIDs, short intakeSemesterIDFrom, short intakeSemesterIDTo, short semesterIDFrom, short semesterIDTo, int? examMarksPolicyID, Guid loginSessionGuid)
		{
			if (intakeSemesterIDFrom > intakeSemesterIDTo || semesterIDFrom > semesterIDTo)
				throw new ArgumentException();
			var results = new List<bool?>();
			using (var aspireContext = new AspireContext(true))
			{
				foreach (var programID in programIDs)
				{
					var intakeSemesterID = intakeSemesterIDFrom;
					while (intakeSemesterID <= intakeSemesterIDTo)
					{
						var semesterID = semesterIDFrom;
						while (semesterID <= semesterIDTo)
						{
							if (intakeSemesterID <= semesterID)
								results.Add(aspireContext.AssignUnAssignExamMarksPolicy(programID, intakeSemesterID, semesterID, examMarksPolicyID, loginSessionGuid));
							semesterID = Semester.AddSemesters(semesterID, 1, true);
						}
						intakeSemesterID = Semester.AddSemesters(intakeSemesterID, 1, true);
					}
				}

				var result = new AssignUnAssignExamMarksPolicyResult(results);
				if (result.UpdatedRecords > 0)
					aspireContext.CommitTransaction();
				return result;
			}
		}

		public static uint CopyExamMarksPolicyFromPreviousSemester(int? departmentID, int? programID, short? intakeSemesterID, short semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var previousSemesterID = Semester.PreviousSemester(semesterID);
				var programs = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID)
					.SelectMany(i => i.Programs)
					.Where(p => departmentID == null || p.DepartmentID == departmentID)
					.Where(p => programID == null || p.ProgramID == programID)
					.SelectMany(p => p.AdmissionOpenPrograms)
					.Where(aop => intakeSemesterID == null || aop.SemesterID == intakeSemesterID)
					.SelectMany(aopemp => aopemp.AdmissionOpenProgramExamMarksPolicies)
					.Where(aopemp => aopemp.SemesterID == previousSemesterID)
					.Select(aopemp => new
					{
						aopemp.AdmissionOpenProgramID,
						aopemp.AdmissionOpenProgram.ProgramID,
						IntakeSemesterID = aopemp.AdmissionOpenProgram.SemesterID,
						aopemp.AdmissionOpenProgram.MaxSemesterID,
						aopemp.ExamMarksPolicyID,
					}).ToList();
				uint count = 0;
				foreach (var program in programs)
				{
					if (program.MaxSemesterID < semesterID)
						continue;
					if (aspireContext.AdmissionOpenProgramExamMarksPolicies.Any(aopemp => aopemp.AdmissionOpenProgramID == program.AdmissionOpenProgramID && aopemp.SemesterID == semesterID))
						continue;
					var assigned = aspireContext.AssignUnAssignExamMarksPolicy(program.ProgramID, program.IntakeSemesterID, semesterID, program.ExamMarksPolicyID, loginSessionGuid);
					count = assigned == true ? count + 1 : count;
				}
				if (count > 0)
					aspireContext.CommitTransaction();
				return count;
			}
		}
	}
}
