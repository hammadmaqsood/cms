﻿using Aspire.BL.Core.Common.JsonConverters;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.Exams.Staff
{
	public static class ProjectThesisInternships
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.UpdateMarks, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}


		[JsonObject(MemberSerialization.OptIn)]
		internal sealed class ProjectThesisInternshipJsonObject
		{
			[JsonProperty]
			public Guid ProjectThesisInternshipID { get; set; }

			[JsonProperty]
			public int StudentID { get; set; }

			[JsonProperty]
			public string Enrollment { get; set; }

			[JsonProperty("Semester")]
			[JsonConverter(typeof(SemesterIDConverter))]
			public short SemesterID { get; set; }

			[JsonProperty]
			public int? RegisteredCourseID { get; set; }

			[JsonProperty("Registered Course Code")]
			public string CourseCode { get; set; }

			[JsonProperty("Registered Course Title")]
			public string CourseTitle { get; set; }

			[JsonProperty("Registered Course Credit Hours")]
			public decimal? CourseCreditHours { get; set; }

			[JsonProperty("Type")]
			[JsonConverter(typeof(EnumGuidFullNameConverter))]
			public Guid RecordType { get; set; }

			[JsonProperty("Credit Hours")]
			[JsonConverter(typeof(CreditHoursConverter))]
			public decimal CreditHours { get; set; }

			[JsonProperty("Title")]
			public string Title { get; set; }

			[JsonProperty("Organization/Company")]
			public string Organization { get; set; }

			[JsonProperty("From")]
			[JsonConverter(typeof(DateTimeConverter))]
			public DateTime? FromDate { get; set; }

			[JsonProperty("To")]
			[JsonConverter(typeof(DateTimeConverter))]
			public DateTime? ToDate { get; set; }

			[JsonProperty("Report Submission Date")]
			[JsonConverter(typeof(DateTimeConverter))]
			public DateTime? ReportSubmissionDate { get; set; }

			[JsonProperty("Defence/Viva Date")]
			[JsonConverter(typeof(DateTimeConverter))]
			public DateTime? DefenceVivaDate { get; set; }

			[JsonProperty("Completion Date")]
			[JsonConverter(typeof(DateTimeConverter))]
			public DateTime? CompletionDate { get; set; }

			[JsonProperty("Remarks")]
			public string Remarks { get; set; }

			[JsonProperty("Total Marks")]
			[JsonConverter(typeof(ExamMarksConverter))]
			public decimal Total { get; set; }

			[JsonProperty("Grade")]
			[JsonConverter(typeof(EnumFullNameConverter))]
			public ExamGrades GradeEnum { get; set; }

			[JsonProperty("Grade Points")]
			[JsonConverter(typeof(GradePointsConverter))]
			public decimal GradePoints { get; set; }

			[JsonProperty("Product")]
			[JsonConverter(typeof(GradePointsConverter))]
			public decimal Product { get; set; }

			[JsonProperty("Marks Entry Locked")]
			[JsonConverter(typeof(YesNoConverter))]
			public bool MarksEntryLocked { get; set; }

			[JsonProperty("Submitted To HoD")]
			[JsonConverter(typeof(DateTimeConverter))]
			public DateTime? SubmittedToHODDate { get; set; }

			[JsonProperty("Submitted To HoD By")]
			public string SubmittedToHODBy { get; set; }

			[JsonProperty("Submitted To Campus")]
			[JsonConverter(typeof(DateTimeConverter))]
			public DateTime? SubmittedToCampusDate { get; set; }

			[JsonProperty("Submitted To Campus By")]
			public string SubmittedToCampusBy { get; set; }

			[JsonProperty("Submitted To BUHO Exams")]
			[JsonConverter(typeof(DateTimeConverter))]
			public DateTime? SubmittedToUniversityDate { get; set; }

			[JsonProperty("Submitted To BUHO Exams By")]
			public string SubmittedToUniversityBy { get; set; }

			[JsonProperty("Detailed Marks")]
			public List<ProjectThesisInternshipMarksJsonObject> Marks { get; set; }

			[JsonProperty("Examiners")]
			public List<ProjectThesisInternshipExaminersJsonObject> Examiners { get; set; }

			[JsonObject(MemberSerialization.OptIn)]
			internal sealed class ProjectThesisInternshipMarksJsonObject
			{
				[JsonProperty("Marks Title")]
				public string MarksTitle { get; set; }

				[JsonProperty("Obtained Marks")]
				[JsonConverter(typeof(ExamMarksConverter))]
				public decimal ObtainedMarks { get; set; }

				[JsonProperty("Total Marks")]
				[JsonConverter(typeof(ExamMarksConverter))]
				public decimal TotalMarks { get; set; }
			}

			[JsonObject(MemberSerialization.OptIn)]
			internal sealed class ProjectThesisInternshipExaminersJsonObject
			{
				[JsonProperty("Type")]
				[JsonConverter(typeof(EnumGuidFullNameConverter))]
				public Guid ExaminerType { get; set; }

				[JsonProperty("Name")]
				public string ExaminerName { get; set; }
			}
		}

		private static string GetJson(this AspireContext aspireContext, Guid projectThesisInternshipID)
		{
			var jsonObject = aspireContext.ProjectThesisInternships
				.Where(pti => pti.ProjectThesisInternshipID == projectThesisInternshipID)
				.Select(pti => new ProjectThesisInternshipJsonObject
				{
					ProjectThesisInternshipID = pti.ProjectThesisInternshipID,
					StudentID = pti.StudentID,
					Enrollment = pti.Student.Enrollment,
					RegisteredCourseID = pti.RegisteredCourseID,
					CourseCode = pti.RegisteredCours.Cours.CourseCode,
					CourseTitle = pti.RegisteredCours.Cours.Title,
					CourseCreditHours = pti.RegisteredCours.Cours.CreditHours,
					SemesterID = pti.SemesterID,
					RecordType = pti.RecordType,
					CreditHours = pti.CreditHours,
					Title = pti.Title,
					Organization = pti.Organization,
					FromDate = pti.FromDate,
					ToDate = pti.ToDate,
					ReportSubmissionDate = pti.ReportSubmissionDate,
					DefenceVivaDate = pti.DefenceVivaDate,
					CompletionDate = pti.CompletionDate,
					Remarks = pti.Remarks,
					Total = pti.Total,
					GradeEnum = (ExamGrades)pti.Grade,
					GradePoints = pti.GradePoints,
					MarksEntryLocked = pti.MarksEntryLocked,
					SubmittedToHODDate = pti.SubmittedToHODDate,
					SubmittedToHODBy = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == pti.SubmittedToHODByUserLoginHistoryID).Select(h => h.User.Name).FirstOrDefault(),
					SubmittedToCampusDate = pti.SubmittedToCampusDate,
					SubmittedToCampusBy = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == pti.SubmittedToCampusByUserLoginHistoryID).Select(h => h.User.Name).FirstOrDefault(),
					SubmittedToUniversityDate = pti.SubmittedToUniversityDate,
					SubmittedToUniversityBy = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == pti.SubmittedToUniversityByUserLoginHistoryID).Select(h => h.User.Name).FirstOrDefault(),
					Marks = pti.ProjectThesisInternshipMarks.OrderBy(ptim => ptim.DisplayIndex).Select(ptim => new ProjectThesisInternshipJsonObject.ProjectThesisInternshipMarksJsonObject
					{
						MarksTitle = ptim.MarksTitle,
						ObtainedMarks = ptim.ObtainedMarks,
						TotalMarks = ptim.TotalMarks
					}).ToList(),
					Examiners = pti.ProjectThesisInternshipExaminers.Select(ptim => new ProjectThesisInternshipJsonObject.ProjectThesisInternshipExaminersJsonObject
					{
						ExaminerType = ptim.ExaminerType,
						ExaminerName = ptim.ExaminerName
					}).ToList()
				}).SingleOrDefault();
			if (jsonObject == null)
				return null;
			return JsonConvert.SerializeObject(jsonObject, Formatting.Indented);
		}

		public static List<ProjectThesisInternship> GetProjectThesisInternships(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.ProjectThesisInternships.Include(pti => pti.RegisteredCours.Cours).Where(ti => ti.StudentID == studentID && ti.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID).ToList();
			}
		}

		public sealed class GetStudentInfoResult
		{
			internal GetStudentInfoResult() { }
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string FatherName { get; internal set; }
			public string Name { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public Model.Entities.Student.Statuses? StatusEnum { get; internal set; }
			public DateTime? StatusDate { get; internal set; }
			public List<GetProjectThesisInternshipResult.Marks> ProjectThesisInternshipMarks => Enumerable.Range(0, 10).Select(i => new GetProjectThesisInternshipResult.Marks()).ToList();
			public List<GetProjectThesisInternshipResult.Examiner> ProjectThesisInternshipExaminers => Enumerable.Range(0, 5).Select(i => new GetProjectThesisInternshipResult.Examiner()).ToList();
		}

		public static GetStudentInfoResult GetStudentInfo(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.Students
					.Where(s => s.StudentID == studentID && s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Select(s => new GetStudentInfoResult
					{
						StudentID = s.StudentID,
						Enrollment = s.Enrollment,
						Name = s.Name,
						ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
						StatusEnum = (Model.Entities.Student.Statuses?)s.Status,
						StatusDate = s.StatusDate,
						FatherName = s.FatherName,
						IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
						RegistrationNo = s.RegistrationNo
					}).SingleOrDefault();
			}
		}

		public sealed class GetProjectThesisInternshipResult
		{
			public sealed class Marks
			{
				internal Marks() { }
				public string MarksTitle { get; internal set; }
				public decimal? ObtainedMarks { get; internal set; }
				public decimal? TotalMarks { get; internal set; }
			}

			public sealed class Examiner
			{
				internal Examiner() { }
				public Guid? ExaminerType { get; internal set; }
				public string ExaminerName { get; internal set; }
			}

			internal GetProjectThesisInternshipResult() { }
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string FatherName { get; internal set; }
			public string Name { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public Model.Entities.Student.Statuses? StatusEnum { get; internal set; }
			public DateTime? StatusDate { get; internal set; }
			public int? RegisteredCourseID { get; internal set; }
			public string CourseCode { get; internal set; }
			public string CourseTitle { get; internal set; }
			public decimal? CourseCreditHours { get; internal set; }
			public Guid ProjectThesisInternshipID { get; internal set; }
			public short SemesterID { get; internal set; }
			public Guid RecordType { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public string Title { get; internal set; }
			public string Organization { get; internal set; }
			public DateTime? FromDate { get; internal set; }
			public DateTime? ToDate { get; internal set; }
			public DateTime? ReportSubmissionDate { get; internal set; }
			public DateTime? DefenceVivaDate { get; internal set; }
			public DateTime? CompletionDate { get; internal set; }
			public string Remarks { get; internal set; }
			public decimal Total { get; internal set; }
			public byte Grade { get; internal set; }
			public decimal GradePoints { get; internal set; }
			public decimal Product { get; internal set; }
			public bool MarksEntryLocked { get; internal set; }
			public int? SubmittedToHODByUserLoginHistoryID { get; internal set; }
			public DateTime? SubmittedToHODDate { get; internal set; }
			public int? SubmittedToCampusByUserLoginHistoryID { get; internal set; }
			public DateTime? SubmittedToCampusDate { get; internal set; }
			public int? SubmittedToUniversityByUserLoginHistoryID { get; internal set; }
			public DateTime? SubmittedToUniversityDate { get; internal set; }
			public List<Marks> ProjectThesisInternshipMarks { get; internal set; }
			public List<Examiner> ProjectThesisInternshipExaminers { get; internal set; }
		}

		public static GetProjectThesisInternshipResult GetProjectThesisInternship(int studentID, Guid projectThesisInternshipID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var result = aspireContext.ProjectThesisInternships
						.Where(pti => pti.ProjectThesisInternshipID == projectThesisInternshipID && pti.StudentID == studentID)
						.Where(pti => pti.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
						.Select(pti => new GetProjectThesisInternshipResult
						{
							StudentID = pti.Student.StudentID,
							Enrollment = pti.Student.Enrollment,
							Name = pti.Student.Name,
							ProgramAlias = pti.Student.AdmissionOpenProgram.Program.ProgramAlias,
							StatusEnum = (Model.Entities.Student.Statuses?)pti.Student.Status,
							FatherName = pti.Student.FatherName,
							IntakeSemesterID = pti.Student.AdmissionOpenProgram.SemesterID,
							RegistrationNo = pti.Student.RegistrationNo,
							StatusDate = pti.Student.StatusDate,
							ProjectThesisInternshipID = pti.ProjectThesisInternshipID,
							RegisteredCourseID = pti.RegisteredCourseID,
							CourseCode = pti.RegisteredCours.Cours.CourseCode,
							CourseTitle = pti.RegisteredCours.Cours.Title,
							CourseCreditHours = pti.RegisteredCours.Cours.CreditHours,
							SemesterID = pti.SemesterID,
							RecordType = pti.RecordType,
							CreditHours = pti.CreditHours,
							Title = pti.Title,
							Organization = pti.Organization,
							FromDate = pti.FromDate,
							ToDate = pti.ToDate,
							ReportSubmissionDate = pti.ReportSubmissionDate,
							DefenceVivaDate = pti.DefenceVivaDate,
							CompletionDate = pti.CompletionDate,
							Remarks = pti.Remarks,
							Total = pti.Total,
							Grade = pti.Grade,
							GradePoints = pti.GradePoints,
							Product = pti.Product,
							MarksEntryLocked = pti.MarksEntryLocked,
							SubmittedToHODDate = pti.SubmittedToHODDate,
							SubmittedToHODByUserLoginHistoryID = pti.SubmittedToHODByUserLoginHistoryID,
							SubmittedToCampusDate = pti.SubmittedToCampusDate,
							SubmittedToCampusByUserLoginHistoryID = pti.SubmittedToCampusByUserLoginHistoryID,
							SubmittedToUniversityDate = pti.SubmittedToUniversityDate,
							SubmittedToUniversityByUserLoginHistoryID = pti.SubmittedToUniversityByUserLoginHistoryID,
							ProjectThesisInternshipExaminers = pti.ProjectThesisInternshipExaminers.Select(ptie => new GetProjectThesisInternshipResult.Examiner
							{
								ExaminerType = ptie.ExaminerType,
								ExaminerName = ptie.ExaminerName
							}).ToList(),
							ProjectThesisInternshipMarks = pti.ProjectThesisInternshipMarks.OrderBy(ptie => ptie.DisplayIndex).Select(ptim => new GetProjectThesisInternshipResult.Marks
							{
								MarksTitle = ptim.MarksTitle,
								ObtainedMarks = ptim.ObtainedMarks,
								TotalMarks = ptim.TotalMarks
							}).ToList()
						}).SingleOrDefault();
				if (result == null)
					return null;
				var count = 10 - (result.ProjectThesisInternshipMarks?.Count ?? 0);
				result.ProjectThesisInternshipMarks = result.ProjectThesisInternshipMarks ?? new List<GetProjectThesisInternshipResult.Marks>();
				result.ProjectThesisInternshipMarks.AddRange(Enumerable.Range(0, count).Select(m => new GetProjectThesisInternshipResult.Marks()));

				count = 5 - (result.ProjectThesisInternshipExaminers?.Count ?? 0);
				result.ProjectThesisInternshipExaminers = result.ProjectThesisInternshipExaminers ?? new List<GetProjectThesisInternshipResult.Examiner>();
				result.ProjectThesisInternshipExaminers.AddRange(Enumerable.Range(0, count).Select(m => new GetProjectThesisInternshipResult.Examiner()));
				return result;
			}
		}

		public enum AddProjectThesisInternshipStatuses
		{
			NoRecordFound,
			ExamMarksPolicyNotFound,
			Success,
		}

		private static void AddProjectThesisInternshipChange(this AspireContext aspireContext, ProjectThesisInternshipChanx.ActionTypes actionTypeEnum, int studentID, Guid projectThesisInternshipID, DateTime now, string oldJSON, ProjectThesisInternshipChanx.ManagementEmailRecipients? managementEmailRecipientEnum, Guid loginSessionGuid)
		{
			aspireContext.ProjectThesisInternshipChanges.Add(new ProjectThesisInternshipChanx
			{
				LoginSessionGuid = loginSessionGuid,
				StudentID = studentID,
				ProjectThesisInternshipID = projectThesisInternshipID,
				ActionDate = now,
				ActionTypeEnum = actionTypeEnum,
				OldJSON = oldJSON,
				NewJSON = aspireContext.GetJson(projectThesisInternshipID),
				ProjectThesisInternshipChangeID = Guid.NewGuid(),
				ManagementEmailRecipientsEnum = managementEmailRecipientEnum,
				ManagementEmailID = null
			});
		}

		public static (AddProjectThesisInternshipStatuses status, Guid? projectThesisInternshipID) AddProjectThesisInternship(int studentID, ProjectThesisInternship.RecordTypes recordTypeEnum, short semesterID, decimal creditHours, string title, string organization, DateTime? fromDate, DateTime? toDate, DateTime? reportSubmissionDate, DateTime? defenceVivaDate, DateTime? completionDate, string remarks, List<ProjectThesisInternshipMark> projectThesisInternshipMarks, List<ProjectThesisInternshipExaminer> projectThesisInternshipExaminers, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var now = DateTime.Now;
				var student = aspireContext.Students
					.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.StudentID,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgramID
					}).SingleOrDefault();
				if (student == null)
					return (AddProjectThesisInternshipStatuses.NoRecordFound, null);
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.DepartmentLevelMarksEntryOfProjectThesisInternship, UserGroupPermission.PermissionValues.Allowed, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);

				var admissionOpenProgramExamMarksPolicy = aspireContext.Students.Where(s => s.StudentID == student.StudentID)
					.SelectMany(s => s.AdmissionOpenProgram.AdmissionOpenProgramExamMarksPolicies)
					.Include(aopemp => aopemp.ExamMarksPolicy)
					.SingleOrDefault(aopemp => aopemp.SemesterID == semesterID);
				if (admissionOpenProgramExamMarksPolicy == null)
				{
					aspireContext.RollbackTransaction();
					return (AddProjectThesisInternshipStatuses.ExamMarksPolicyNotFound, null);
				}

				byte displayIndex = 0;
				var projectThesisInternship = aspireContext.ProjectThesisInternships.Add(new ProjectThesisInternship
				{
					ProjectThesisInternshipID = Guid.NewGuid(),
					StudentID = studentID,
					RecordTypeEnum = recordTypeEnum,
					SemesterID = semesterID,
					CreditHours = creditHours,
					Title = title,
					Organization = organization,
					FromDate = fromDate,
					ToDate = toDate,
					ReportSubmissionDate = reportSubmissionDate,
					DefenceVivaDate = defenceVivaDate,
					CompletionDate = completionDate,
					Remarks = remarks,
					MarksEntryLocked = false,
					SubmittedToHODDate = null,
					SubmittedToHODByUserLoginHistoryID = null,
					SubmittedToCampusDate = null,
					SubmittedToCampusByUserLoginHistoryID = null,
					SubmittedToUniversityDate = null,
					SubmittedToUniversityByUserLoginHistoryID = null,
					ProjectThesisInternshipMarks = projectThesisInternshipMarks.OrderBy(m => m.DisplayIndex).Select(m => new ProjectThesisInternshipMark
					{
						ProjectThesisInternshipMarkID = Guid.NewGuid(),
						DisplayIndex = displayIndex++,
						MarksTitle = m.MarksTitle,
						ObtainedMarks = m.ObtainedMarks,
						TotalMarks = m.TotalMarks
					}).ToList(),
					ProjectThesisInternshipExaminers = projectThesisInternshipExaminers.Select(e => new ProjectThesisInternshipExaminer
					{
						ProjectThesisInternshipExaminerID = Guid.NewGuid(),
						ExaminerName = e.ExaminerName,
						ExaminerType = e.ExaminerType
					}).ToList()
				}.Validate(admissionOpenProgramExamMarksPolicy.ExamMarksPolicy));
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.AddProjectThesisInternshipChange(ProjectThesisInternshipChanx.ActionTypes.Added, projectThesisInternship.StudentID, projectThesisInternship.ProjectThesisInternshipID, now, null, null, loginHistory.LoginSessionGuid);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return (AddProjectThesisInternshipStatuses.Success, projectThesisInternship.ProjectThesisInternshipID);
			}
		}

		public enum UpdateProjectThesisInternshipStatuses
		{
			NoRecordFound,
			ExamMarksPolicyNotFound,
			MarksCanNotBeEditWhileSubmittedToHOD,
			MarksEntryIsLocked,
			SemesterCannotBeChangedWhenAttachedToRegisteredCourse,
			CreditHoursCannotBeChangedWhenAttachedToRegisteredCourse,
			Success
		}

		public static UpdateProjectThesisInternshipStatuses UpdateProjectThesisInternship(Guid projectThesisInternshipID, short semesterID, decimal creditHours, string title, string organization, DateTime? fromDate, DateTime? toDate, DateTime? reportSubmissionDate, DateTime? defenceVivaDate, DateTime? completionDate, string remarks, List<ProjectThesisInternshipMark> projectThesisInternshipMarks, List<ProjectThesisInternshipExaminer> projectThesisInternshipExaminers, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.ProjectThesisInternships
				   .Where(pti => pti.ProjectThesisInternshipID == projectThesisInternshipID)
				   .Select(pti => new
				   {
					   ProjectThesisInternship = pti,
					   pti.Student.StudentID,
					   pti.Student.AdmissionOpenProgram.Program.InstituteID,
					   pti.Student.AdmissionOpenProgram.Program.DepartmentID,
					   pti.Student.AdmissionOpenProgram.ProgramID,
					   pti.Student.AdmissionOpenProgramID
				   }).SingleOrDefault();
				if (record == null)
					return UpdateProjectThesisInternshipStatuses.NoRecordFound;

				aspireContext.DemandModulePermissions(loginSessionGuid);
				Core.Common.Permissions.Staff.StaffGroupPermission loginHistory;
				ProjectThesisInternshipChanx.ManagementEmailRecipients? managementEmailRecipientEnum;
				if (record.ProjectThesisInternship.SubmittedToUniversityDate != null)
				{
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.UniversityLevelMarksEntry, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
					managementEmailRecipientEnum = ProjectThesisInternshipChanx.ManagementEmailRecipients.AfterSubmitToUniversity;
				}
				else if (record.ProjectThesisInternship.SubmittedToCampusDate != null)
				{
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.CampusLevelMarksEntry, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
					managementEmailRecipientEnum = ProjectThesisInternshipChanx.ManagementEmailRecipients.AfterSubmitToCampus;
				}
				else if (record.ProjectThesisInternship.SubmittedToHODDate != null)
				{
					return UpdateProjectThesisInternshipStatuses.MarksCanNotBeEditWhileSubmittedToHOD;
				}
				else if (record.ProjectThesisInternship.MarksEntryLocked)
				{
					return UpdateProjectThesisInternshipStatuses.MarksEntryIsLocked;
				}
				else
				{
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.DepartmentLevelMarksEntryOfProjectThesisInternship, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
					managementEmailRecipientEnum = null;
				}

				var admissionOpenProgramExamMarksPolicy = aspireContext.Students.Where(s => s.StudentID == record.StudentID)
					.SelectMany(s => s.AdmissionOpenProgram.AdmissionOpenProgramExamMarksPolicies)
					.Include(aopemp => aopemp.ExamMarksPolicy)
					.SingleOrDefault(aopemp => aopemp.SemesterID == semesterID);
				if (admissionOpenProgramExamMarksPolicy == null)
				{
					aspireContext.RollbackTransaction();
					return UpdateProjectThesisInternshipStatuses.ExamMarksPolicyNotFound;
				}

				var projectThesisInternship = record.ProjectThesisInternship;
				var now = DateTime.Now;
				var oldJSON = aspireContext.GetJson(projectThesisInternshipID);
				aspireContext.Entry(projectThesisInternship).Collection(pti => pti.ProjectThesisInternshipMarks).Load();
				aspireContext.Entry(projectThesisInternship).Collection(pti => pti.ProjectThesisInternshipExaminers).Load();

				if (projectThesisInternship.RegisteredCourseID == null)
					projectThesisInternship.SemesterID = semesterID;
				else if (projectThesisInternship.SemesterID != semesterID)
					return UpdateProjectThesisInternshipStatuses.SemesterCannotBeChangedWhenAttachedToRegisteredCourse;

				if (projectThesisInternship.RegisteredCourseID == null)
					projectThesisInternship.CreditHours = creditHours;
				else if (projectThesisInternship.CreditHours != creditHours)
					return UpdateProjectThesisInternshipStatuses.CreditHoursCannotBeChangedWhenAttachedToRegisteredCourse;

				projectThesisInternship.Title = title;
				projectThesisInternship.Organization = organization;
				projectThesisInternship.FromDate = fromDate;
				projectThesisInternship.ToDate = toDate;
				projectThesisInternship.ReportSubmissionDate = reportSubmissionDate;
				projectThesisInternship.DefenceVivaDate = defenceVivaDate;
				projectThesisInternship.CompletionDate = completionDate;
				projectThesisInternship.Remarks = remarks;

				aspireContext.ProjectThesisInternshipMarks.RemoveRange(projectThesisInternship.ProjectThesisInternshipMarks);
				byte displayIndex = 0;
				foreach (var projectThesisInternshipMark in projectThesisInternshipMarks)
					projectThesisInternship.ProjectThesisInternshipMarks.Add(new ProjectThesisInternshipMark
					{
						ProjectThesisInternshipMarkID = Guid.NewGuid(),
						DisplayIndex = displayIndex++,
						MarksTitle = projectThesisInternshipMark.MarksTitle,
						ObtainedMarks = projectThesisInternshipMark.ObtainedMarks,
						TotalMarks = projectThesisInternshipMark.TotalMarks
					});

				aspireContext.ProjectThesisInternshipExaminers.RemoveRange(projectThesisInternship.ProjectThesisInternshipExaminers);
				foreach (var projectThesisInternshipExaminer in projectThesisInternshipExaminers)
					projectThesisInternship.ProjectThesisInternshipExaminers.Add(new ProjectThesisInternshipExaminer
					{
						ProjectThesisInternshipExaminerID = Guid.NewGuid(),
						ExaminerType = projectThesisInternshipExaminer.ExaminerType,
						ExaminerName = projectThesisInternshipExaminer.ExaminerName
					});

				projectThesisInternship.Validate(admissionOpenProgramExamMarksPolicy.ExamMarksPolicy);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.AddProjectThesisInternshipChange(ProjectThesisInternshipChanx.ActionTypes.Updated, projectThesisInternship.StudentID, projectThesisInternship.ProjectThesisInternshipID, now, oldJSON, managementEmailRecipientEnum, loginHistory.LoginSessionGuid);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateProjectThesisInternshipStatuses.Success;
			}
		}

		public enum DeleteProjectThesisInternshipStatuses
		{
			NoRecordFound,
			SubmittedToUniversity,
			MarksCanNotBeEditWhileSubmittedToHOD,
			MarksEntryIsLocked,
			Success
		}

		public static DeleteProjectThesisInternshipStatuses DeleteProjectThesisInternship(Guid projectThesisInternshipID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.ProjectThesisInternships
				   .Where(pti => pti.ProjectThesisInternshipID == projectThesisInternshipID)
				   .Select(pti => new
				   {
					   ProjectThesisInternship = pti,
					   pti.Student.StudentID,
					   pti.Student.AdmissionOpenProgram.Program.InstituteID,
					   pti.Student.AdmissionOpenProgram.Program.DepartmentID,
					   pti.Student.AdmissionOpenProgram.ProgramID,
					   pti.Student.AdmissionOpenProgramID
				   }).SingleOrDefault();
				if (record == null)
					return DeleteProjectThesisInternshipStatuses.NoRecordFound;

				aspireContext.DemandModulePermissions(loginSessionGuid);
				Core.Common.Permissions.Staff.StaffGroupPermission loginHistory;
				ProjectThesisInternshipChanx.ManagementEmailRecipients? managementEmailRecipientEnum;
				if (record.ProjectThesisInternship.MarksEntryLocked)
					return DeleteProjectThesisInternshipStatuses.MarksEntryIsLocked;

				if (record.ProjectThesisInternship.SubmittedToUniversityDate != null)
				{
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.UniversityLevelMarksEntry, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
					managementEmailRecipientEnum = ProjectThesisInternshipChanx.ManagementEmailRecipients.AfterSubmitToUniversity;
				}
				else if (record.ProjectThesisInternship.SubmittedToCampusDate != null)
				{
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.CampusLevelMarksEntry, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
					managementEmailRecipientEnum = ProjectThesisInternshipChanx.ManagementEmailRecipients.AfterSubmitToCampus;
				}
				else if (record.ProjectThesisInternship.SubmittedToHODDate != null)
				{
					return DeleteProjectThesisInternshipStatuses.MarksCanNotBeEditWhileSubmittedToHOD;
				}
				else
				{
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.DepartmentLevelMarksEntryOfProjectThesisInternship, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
					managementEmailRecipientEnum = null;
				}

				var projectThesisInternship = record.ProjectThesisInternship;
				var now = DateTime.Now;
				var oldJSON = aspireContext.GetJson(projectThesisInternshipID);
				aspireContext.Entry(projectThesisInternship).Collection(pti => pti.ProjectThesisInternshipMarks).Load();
				aspireContext.Entry(projectThesisInternship).Collection(pti => pti.ProjectThesisInternshipExaminers).Load();

				aspireContext.ProjectThesisInternshipMarks.RemoveRange(projectThesisInternship.ProjectThesisInternshipMarks);
				aspireContext.ProjectThesisInternshipExaminers.RemoveRange(projectThesisInternship.ProjectThesisInternshipExaminers);
				aspireContext.ProjectThesisInternships.Remove(projectThesisInternship);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.AddProjectThesisInternshipChange(ProjectThesisInternshipChanx.ActionTypes.Deleted, projectThesisInternship.StudentID, projectThesisInternship.ProjectThesisInternshipID, now, oldJSON, managementEmailRecipientEnum, loginHistory.LoginSessionGuid);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteProjectThesisInternshipStatuses.Success;
			}
		}

		public enum LockMarksEntryStatuses
		{
			NoRecordFound,
			MarksEntryIsAlreadyLocked,
			RegisteredCourseIsNotAttached,
			Success
		}

		public static LockMarksEntryStatuses LockMarksEntry(Guid projectThesisInternshipID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.ProjectThesisInternships
					.Where(pti => pti.ProjectThesisInternshipID == projectThesisInternshipID)
					.Select(pti => new
					{
						ProjectThesisInternship = pti,
						pti.Student.AdmissionOpenProgram.Program.InstituteID,
						pti.Student.AdmissionOpenProgram.Program.DepartmentID,
						pti.Student.AdmissionOpenProgram.ProgramID,
						pti.Student.AdmissionOpenProgramID
					}).SingleOrDefault();
				if (record == null)
					return LockMarksEntryStatuses.NoRecordFound;
				if (record.ProjectThesisInternship.MarksEntryLocked)
					return LockMarksEntryStatuses.MarksEntryIsAlreadyLocked;

				aspireContext.DemandModulePermissions(loginSessionGuid);
				Core.Common.Permissions.Staff.StaffGroupPermission loginHistory;
				if (record.ProjectThesisInternship.SubmittedToUniversityDate != null)
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.UniversityLevelMarksEntry, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
				else if (record.ProjectThesisInternship.SubmittedToCampusDate != null)
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.CampusLevelMarksEntry, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
				else if (record.ProjectThesisInternship.SubmittedToHODDate != null)
					throw new InvalidOperationException("HOD cannot change marks.");
				else
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.DepartmentLevelMarksEntryOfProjectThesisInternship, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				switch (record.ProjectThesisInternship.RecordTypeEnum)
				{
					case ProjectThesisInternship.RecordTypes.Internship:
						break;
					case ProjectThesisInternship.RecordTypes.Project:
					case ProjectThesisInternship.RecordTypes.Thesis:
						if (record.ProjectThesisInternship.RegisteredCourseID == null)
							return LockMarksEntryStatuses.RegisteredCourseIsNotAttached;
						break;
					default:
						throw new NotImplementedEnumException(record.ProjectThesisInternship.RecordTypeEnum);
				}
				var now = DateTime.Now;
				var oldJSON = aspireContext.GetJson(record.ProjectThesisInternship.ProjectThesisInternshipID);
				record.ProjectThesisInternship.MarksEntryLocked = true;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.AddProjectThesisInternshipChange(ProjectThesisInternshipChanx.ActionTypes.LockMarksEntry, record.ProjectThesisInternship.StudentID, record.ProjectThesisInternship.ProjectThesisInternshipID, now, oldJSON, null, loginHistory.LoginSessionGuid);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return LockMarksEntryStatuses.Success;
			}
		}

		public enum UnlockMarksEntryStatuses
		{
			NoRecordFound,
			MarksEntryIsAlreadyUnlocked,
			CannotUnlockBecauseMarksSubmittedToHOD,
			Success
		}

		public static UnlockMarksEntryStatuses UnlockMarksEntry(Guid projectThesisInternshipID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.ProjectThesisInternships
					.Where(pti => pti.ProjectThesisInternshipID == projectThesisInternshipID)
					.Select(pti => new
					{
						ProjectThesisInternship = pti,
						pti.Student.AdmissionOpenProgram.Program.InstituteID,
						pti.Student.AdmissionOpenProgram.Program.DepartmentID,
						pti.Student.AdmissionOpenProgram.ProgramID,
						pti.Student.AdmissionOpenProgramID
					}).SingleOrDefault();
				if (record == null)
					return UnlockMarksEntryStatuses.NoRecordFound;
				if (!record.ProjectThesisInternship.MarksEntryLocked)
					return UnlockMarksEntryStatuses.MarksEntryIsAlreadyUnlocked;

				aspireContext.DemandModulePermissions(loginSessionGuid);
				Core.Common.Permissions.Staff.StaffGroupPermission loginHistory;
				if (record.ProjectThesisInternship.SubmittedToUniversityDate != null)
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.UniversityLevelMarksEntry, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
				else if (record.ProjectThesisInternship.SubmittedToCampusDate != null)
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.UnlockMarksEntry, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
				else if (record.ProjectThesisInternship.SubmittedToHODDate != null)
					return UnlockMarksEntryStatuses.CannotUnlockBecauseMarksSubmittedToHOD;
				else
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.UnlockMarksEntry, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				var now = DateTime.Now;
				var oldJSON = aspireContext.GetJson(record.ProjectThesisInternship.ProjectThesisInternshipID);
				record.ProjectThesisInternship.MarksEntryLocked = false;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.AddProjectThesisInternshipChange(ProjectThesisInternshipChanx.ActionTypes.UnlockMarksEntry, record.ProjectThesisInternship.StudentID, record.ProjectThesisInternship.ProjectThesisInternshipID, now, oldJSON, null, loginHistory.LoginSessionGuid);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UnlockMarksEntryStatuses.Success;
			}
		}

		public enum SubmitToHODStatuses
		{
			NoRecordFound,
			MarksEntryIsNotLocked,
			MarksAreAlreadySubmittedToUniversity,
			MarksAreAlreadySubmittedToCampus,
			MarksAreAlreadySubmittedToHOD,
			Success
		}

		public static SubmitToHODStatuses SubmitToHOD(Guid projectThesisInternshipID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.ProjectThesisInternships
					.Where(pti => pti.ProjectThesisInternshipID == projectThesisInternshipID)
					.Select(pti => new
					{
						ProjectThesisInternship = pti,
						pti.Student.AdmissionOpenProgram.Program.InstituteID,
						pti.Student.AdmissionOpenProgram.Program.DepartmentID,
						pti.Student.AdmissionOpenProgram.ProgramID,
						pti.Student.AdmissionOpenProgramID
					}).SingleOrDefault();
				if (record == null)
					return SubmitToHODStatuses.NoRecordFound;

				if (record.ProjectThesisInternship.SubmittedToUniversityDate != null)
					return SubmitToHODStatuses.MarksAreAlreadySubmittedToUniversity;
				if (record.ProjectThesisInternship.SubmittedToCampusDate != null)
					return SubmitToHODStatuses.MarksAreAlreadySubmittedToCampus;
				if (record.ProjectThesisInternship.SubmittedToHODDate != null)
					return SubmitToHODStatuses.MarksAreAlreadySubmittedToHOD;

				if (!record.ProjectThesisInternship.MarksEntryLocked)
					return SubmitToHODStatuses.MarksEntryIsNotLocked;

				if (!(record.ProjectThesisInternship.MarksEntryLocked
					&& record.ProjectThesisInternship.SubmittedToHODDate == null
					&& record.ProjectThesisInternship.SubmittedToCampusDate == null
					&& record.ProjectThesisInternship.SubmittedToUniversityDate == null))
					throw new InvalidOperationException($"{nameof(record.ProjectThesisInternship.ProjectThesisInternshipID)}: {record.ProjectThesisInternship.ProjectThesisInternshipID}");

				aspireContext.DemandModulePermissions(loginSessionGuid);
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.DepartmentLevelMarksEntryOfProjectThesisInternship, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				var now = DateTime.Now;
				var oldJSON = aspireContext.GetJson(record.ProjectThesisInternship.ProjectThesisInternshipID);
				record.ProjectThesisInternship.SubmittedToHODDate = now;
				record.ProjectThesisInternship.SubmittedToHODByUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.AddProjectThesisInternshipChange(ProjectThesisInternshipChanx.ActionTypes.SubmitToHOD, record.ProjectThesisInternship.StudentID, record.ProjectThesisInternship.ProjectThesisInternshipID, now, oldJSON, null, loginHistory.LoginSessionGuid);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return SubmitToHODStatuses.Success;
			}
		}

		public enum SubmitToCampusStatuses
		{
			NoRecordFound,
			MarksAreAlreadySubmittedToUniversity,
			MarksAreAlreadySubmittedToCampus,
			Success
		}

		public static SubmitToCampusStatuses SubmitToCampus(Guid projectThesisInternshipID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.ProjectThesisInternships
					.Where(pti => pti.ProjectThesisInternshipID == projectThesisInternshipID)
					.Select(pti => new
					{
						ProjectThesisInternship = pti,
						pti.Student.AdmissionOpenProgram.Program.InstituteID,
						pti.Student.AdmissionOpenProgram.Program.DepartmentID,
						pti.Student.AdmissionOpenProgram.ProgramID,
						pti.Student.AdmissionOpenProgramID
					}).SingleOrDefault();
				if (record == null)
					return SubmitToCampusStatuses.NoRecordFound;

				if (record.ProjectThesisInternship.SubmittedToUniversityDate != null)
					return SubmitToCampusStatuses.MarksAreAlreadySubmittedToUniversity;
				if (record.ProjectThesisInternship.SubmittedToCampusDate != null)
					return SubmitToCampusStatuses.MarksAreAlreadySubmittedToCampus;

				if (!(record.ProjectThesisInternship.MarksEntryLocked
					&& record.ProjectThesisInternship.SubmittedToCampusDate == null
					&& record.ProjectThesisInternship.SubmittedToUniversityDate == null))
					throw new InvalidOperationException($"{nameof(record.ProjectThesisInternship.ProjectThesisInternshipID)}: {record.ProjectThesisInternship.ProjectThesisInternshipID}");

				aspireContext.DemandModulePermissions(loginSessionGuid);
				var loginHistory = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.DepartmentLevelMarksEntry, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID)
					?? aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.CampusLevelMarksEntry, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				var now = DateTime.Now;
				var oldJSON = aspireContext.GetJson(record.ProjectThesisInternship.ProjectThesisInternshipID);
				record.ProjectThesisInternship.SubmittedToCampusDate = now;
				record.ProjectThesisInternship.SubmittedToCampusByUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.AddProjectThesisInternshipChange(ProjectThesisInternshipChanx.ActionTypes.SubmitToCampus, record.ProjectThesisInternship.StudentID, record.ProjectThesisInternship.ProjectThesisInternshipID, now, oldJSON, null, loginHistory.LoginSessionGuid);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return SubmitToCampusStatuses.Success;
			}
		}

		public enum SubmitToUniversityStatuses
		{
			NoRecordFound,
			MarksAreAlreadySubmittedToUniversity,
			MarksAreNotSubmittedToCampus,
			Success
		}

		public static SubmitToUniversityStatuses SubmitToUniversity(Guid projectThesisInternshipID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.ProjectThesisInternships
					.Where(pti => pti.ProjectThesisInternshipID == projectThesisInternshipID)
					.Select(pti => new
					{
						ProjectThesisInternship = pti,
						pti.Student.AdmissionOpenProgram.Program.InstituteID,
						pti.Student.AdmissionOpenProgram.Program.DepartmentID,
						pti.Student.AdmissionOpenProgram.ProgramID,
						pti.Student.AdmissionOpenProgramID
					}).SingleOrDefault();
				if (record == null)
					return SubmitToUniversityStatuses.NoRecordFound;

				if (record.ProjectThesisInternship.SubmittedToUniversityDate != null)
					return SubmitToUniversityStatuses.MarksAreAlreadySubmittedToUniversity;
				if (record.ProjectThesisInternship.SubmittedToCampusDate == null)
					return SubmitToUniversityStatuses.MarksAreNotSubmittedToCampus;

				if (!(record.ProjectThesisInternship.MarksEntryLocked
					&& record.ProjectThesisInternship.SubmittedToCampusDate != null
					&& record.ProjectThesisInternship.SubmittedToUniversityDate == null))
					throw new InvalidOperationException($"{nameof(record.ProjectThesisInternship.ProjectThesisInternshipID)}: {record.ProjectThesisInternship.ProjectThesisInternshipID}");

				aspireContext.DemandModulePermissions(loginSessionGuid);
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.CampusLevelMarksEntry, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				var now = DateTime.Now;
				var oldJSON = aspireContext.GetJson(record.ProjectThesisInternship.ProjectThesisInternshipID);
				record.ProjectThesisInternship.SubmittedToUniversityDate = now;
				record.ProjectThesisInternship.SubmittedToUniversityByUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.AddProjectThesisInternshipChange(ProjectThesisInternshipChanx.ActionTypes.SubmitToUniversity, record.ProjectThesisInternship.StudentID, record.ProjectThesisInternship.ProjectThesisInternshipID, now, oldJSON, null, loginHistory.LoginSessionGuid);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return SubmitToUniversityStatuses.Success;
			}
		}

		public sealed class ProjectThesisInternshipChange
		{
			public Guid ProjectThesisInternshipChangeID { get; internal set; }
			public int StudentID { get; internal set; }
			public DateTime ActionDate { get; internal set; }
			public Guid ActionType { get; internal set; }
			public string ActionTypeFullName => this.ActionType.GetFullName();
			public string ChangedByUserName { get; internal set; }
			public UserTypes ChangeByUserTypeEnum { get; internal set; }
			public string ChangedBy => $"{this.ChangedByUserName} ({this.ChangeByUserTypeEnum.ToFullName()})";
			public string OldJSON { get; internal set; }
			public string NewJSON { get; internal set; }
		}

		public static (List<ProjectThesisInternshipChange> ProjectThesisInternshipChanges, int VirtualItemCount)? GetProjectThesisInternshipChanges(int studentID, int pageIndex, int pageSize, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var student = aspireContext.Students
					.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.StudentID,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.Institute.InstituteName,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgramID
					}).SingleOrDefault();
				if (student == null)
					return null;
				aspireContext.DemandStaffPermissions(loginHistory.LoginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				var marksSheetChangesQuery = from ptic in aspireContext.ProjectThesisInternshipChanges
											 join ulh in aspireContext.UserLoginHistories on ptic.LoginSessionGuid equals ulh.LoginSessionGuid
											 where ptic.StudentID == student.StudentID
											 orderby ptic.ActionDate descending
											 select new ProjectThesisInternshipChange
											 {
												 ProjectThesisInternshipChangeID = ptic.ProjectThesisInternshipChangeID,
												 StudentID = ptic.StudentID,
												 ChangedByUserName = ulh.UserName,
												 ChangeByUserTypeEnum = (UserTypes)ulh.UserType,
												 ActionDate = ptic.ActionDate,
												 ActionType = ptic.ActionType
											 };

				return (marksSheetChangesQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList(), marksSheetChangesQuery.Count());
			}
		}

		public static ProjectThesisInternshipChange GetProjectThesisInternshipChange(Guid projectThesisInternshipChangeID, int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				var student = aspireContext.Students
					.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						s.StudentID,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.Institute.InstituteName,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgramID
					}).SingleOrDefault();
				if (student == null)
					return null;
				aspireContext.DemandStaffPermissions(loginHistory.LoginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				var marksSheetChangesQuery = from ptic in aspireContext.ProjectThesisInternshipChanges
											 join ulh in aspireContext.UserLoginHistories on ptic.LoginSessionGuid equals ulh.LoginSessionGuid
											 where ptic.StudentID == student.StudentID && ptic.ProjectThesisInternshipChangeID == projectThesisInternshipChangeID
											 orderby ptic.ActionDate descending
											 select new ProjectThesisInternshipChange
											 {
												 ProjectThesisInternshipChangeID = ptic.ProjectThesisInternshipChangeID,
												 StudentID = ptic.StudentID,
												 ChangedByUserName = ulh.UserName,
												 ChangeByUserTypeEnum = (UserTypes)ulh.UserType,
												 ActionDate = ptic.ActionDate,
												 ActionType = ptic.ActionType,
												 OldJSON = ptic.OldJSON,
												 NewJSON = ptic.NewJSON
											 };

				return marksSheetChangesQuery.SingleOrDefault();
			}
		}

		public static List<CustomListItem> GetRegisteredCourses(Guid projectThesisInternshipID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var projectThesisInternship = aspireContext.ProjectThesisInternships
					.Where(pti => pti.ProjectThesisInternshipID == projectThesisInternshipID && pti.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Select(pti => new
					{
						pti.StudentID,
						pti.SemesterID
					})
					.SingleOrDefault();
				var registeredCourse = aspireContext.RegisteredCourses
					.Where(rc => rc.DeletedDate == null)
					.Where(rc => rc.StudentID == projectThesisInternship.StudentID && rc.OfferedCours.SemesterID == projectThesisInternship.SemesterID)
					.Select(rc => new
					{
						rc.RegisteredCourseID,
						rc.Cours.Title,
						rc.Cours.CreditHours,
						rc.Cours.CourseCode,
						CourseCategoryEnum = (CourseCategories)rc.Cours.CourseCategory,
					}).ToList();

				return registeredCourse.Select(rc => new CustomListItem
				{
					Text = $"{rc.Title} [Credits: {rc.CreditHours.ToCreditHoursFullName()}] [Category: {rc.CourseCategoryEnum.ToFullName()}]",
					ID = rc.RegisteredCourseID
				}).OrderBy(rc => rc.Text).ToList();
			}
		}

		public enum AttachRegisteredCoursesStatuses
		{
			NoRecordFound,
			MarksCanNotBeEditWhileSubmittedToHOD,
			MarksEntryIsLocked,
			RegisteredCourseCategoryMustBeInternship,
			RegisteredCourseCategoryMustBeProject,
			RegisteredCourseCategoryMustBeThesis,
			RegisteredCourseCreditHoursDoNotMatch,
			NoChangeFound,
			RegisteredCourseAttached,
			RegisteredCourseDetached,
		}

		public static AttachRegisteredCoursesStatuses AttachRegisteredCourses(Guid projectThesisInternshipID, int? registeredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.ProjectThesisInternships
				   .Where(pti => pti.ProjectThesisInternshipID == projectThesisInternshipID)
				   .Select(pti => new
				   {
					   ProjectThesisInternship = pti,
					   pti.Student.StudentID,
					   pti.Student.AdmissionOpenProgram.Program.InstituteID,
					   pti.Student.AdmissionOpenProgram.Program.DepartmentID,
					   pti.Student.AdmissionOpenProgram.ProgramID,
					   pti.Student.AdmissionOpenProgramID
				   }).SingleOrDefault();
				if (record == null)
					return AttachRegisteredCoursesStatuses.NoRecordFound;

				aspireContext.DemandModulePermissions(loginSessionGuid);
				Core.Common.Permissions.Staff.StaffGroupPermission loginHistory;
				ProjectThesisInternshipChanx.ManagementEmailRecipients? managementEmailRecipientEnum;
				if (record.ProjectThesisInternship.MarksEntryLocked)
					return AttachRegisteredCoursesStatuses.MarksEntryIsLocked;
				if (record.ProjectThesisInternship.SubmittedToUniversityDate != null)
				{
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.UniversityLevelMarksEntry, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
					managementEmailRecipientEnum = ProjectThesisInternshipChanx.ManagementEmailRecipients.AfterSubmitToUniversity;
				}
				else if (record.ProjectThesisInternship.SubmittedToCampusDate != null)
				{
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.CampusLevelMarksEntry, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
					managementEmailRecipientEnum = ProjectThesisInternshipChanx.ManagementEmailRecipients.AfterSubmitToCampus;
				}
				else if (record.ProjectThesisInternship.SubmittedToHODDate != null)
				{
					return AttachRegisteredCoursesStatuses.MarksCanNotBeEditWhileSubmittedToHOD;
				}
				else
				{
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.DepartmentLevelMarksEntryOfProjectThesisInternship, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
					managementEmailRecipientEnum = null;
				}

				var projectThesisInternship = record.ProjectThesisInternship;
				var now = DateTime.Now;
				var oldJSON = aspireContext.GetJson(projectThesisInternshipID);
				ProjectThesisInternshipChanx.ActionTypes actionTypEnum;
				if (registeredCourseID == null)
				{
					if (record.ProjectThesisInternship.RegisteredCourseID == null)
						return AttachRegisteredCoursesStatuses.NoChangeFound;
					actionTypEnum = ProjectThesisInternshipChanx.ActionTypes.RegisteredCourseDetached;
					projectThesisInternship.RegisteredCourseID = null;
				}
				else
				{
					actionTypEnum = ProjectThesisInternshipChanx.ActionTypes.RegisteredCourseAttached;
					var registeredCourse = aspireContext.RegisteredCourses
						.Where(rc => rc.DeletedDate == null)
						.Where(rc => rc.StudentID == projectThesisInternship.StudentID && rc.OfferedCours.SemesterID == projectThesisInternship.SemesterID)
						.Where(rc => rc.RegisteredCourseID == registeredCourseID)
						.Select(rc => new
						{
							rc.RegisteredCourseID,
							rc.Cours.CreditHours,
							CourseCategoryEnum = (CourseCategories)rc.Cours.CourseCategory,
						}).SingleOrDefault();
					if (registeredCourse == null)
						return AttachRegisteredCoursesStatuses.NoRecordFound;
					switch (record.ProjectThesisInternship.RecordTypeEnum)
					{
						case ProjectThesisInternship.RecordTypes.Internship:
							if (registeredCourse.CourseCategoryEnum != CourseCategories.Internship)
								return AttachRegisteredCoursesStatuses.RegisteredCourseCategoryMustBeInternship;
							break;
						case ProjectThesisInternship.RecordTypes.Project:
							if (registeredCourse.CourseCategoryEnum != CourseCategories.Project)
								return AttachRegisteredCoursesStatuses.RegisteredCourseCategoryMustBeProject;
							break;
						case ProjectThesisInternship.RecordTypes.Thesis:
							if (registeredCourse.CourseCategoryEnum != CourseCategories.Thesis)
								return AttachRegisteredCoursesStatuses.RegisteredCourseCategoryMustBeThesis;
							break;
						default:
							throw new NotImplementedEnumException(record.ProjectThesisInternship.RecordTypeEnum);
					}
					if (registeredCourse.CreditHours != record.ProjectThesisInternship.CreditHours)
						return AttachRegisteredCoursesStatuses.RegisteredCourseCreditHoursDoNotMatch;
					if (record.ProjectThesisInternship.RegisteredCourseID == registeredCourse.RegisteredCourseID)
						return AttachRegisteredCoursesStatuses.NoChangeFound;
					record.ProjectThesisInternship.RegisteredCourseID = registeredCourse.RegisteredCourseID;
				}

				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.AddProjectThesisInternshipChange(actionTypEnum, projectThesisInternship.StudentID, projectThesisInternship.ProjectThesisInternshipID, now, oldJSON, managementEmailRecipientEnum, loginHistory.LoginSessionGuid);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return record.ProjectThesisInternship.RegisteredCourseID == null
					? AttachRegisteredCoursesStatuses.RegisteredCourseDetached
					: AttachRegisteredCoursesStatuses.RegisteredCourseAttached;
			}
		}
	}
}
