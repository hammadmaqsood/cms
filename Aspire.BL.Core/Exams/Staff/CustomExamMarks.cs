﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Staff
{
	public static class CustomExamMarks
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.UpdateMarks, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class RegisteredCourse
		{
			internal RegisteredCourse()
			{
			}
			public int RegisteredCourseID { get; internal set; }

			public string RoadmapCourseCode { get; internal set; }
			public string RoadmapTitle { get; internal set; }
			public decimal RoadmapCreditHours { get; internal set; }
			public string RoadmapCreditHoursFullName => this.RoadmapCreditHours.ToCreditHoursFullName();

			public short OfferedSemesterID { get; internal set; }
			public string OfferedSemester => this.OfferedSemesterID.ToSemesterString();
			public string OfferedCourseCode { get; internal set; }
			public string OfferedTitle { get; internal set; }
			public string OfferedProgram { get; internal set; }
			public short OfferedSemesterNo { get; internal set; }
			public SemesterNos OfferedSemesterNoEnum => (SemesterNos)this.OfferedSemesterNo;
			public string OfferedSemesterNoFullName => this.OfferedSemesterNoEnum.ToFullName();
			public int OfferedSection { get; internal set; }
			public Sections OfferedSectionEnum => (Sections)this.OfferedSection;
			public string OfferedSectionFullName => this.OfferedSectionEnum.ToFullName();
			public byte OfferedShift { get; internal set; }
			public Shifts OfferedShiftEnum => (Shifts)this.OfferedShift;
			public string OfferedShiftFullName => this.OfferedShiftEnum.ToFullName();
			public string OfferedClass => AspireFormats.GetClassName(this.OfferedProgram, this.OfferedSemesterNo, this.OfferedSection, this.OfferedShift);
			public string FacultyMemberName { get; internal set; }
			public byte? Status { get; internal set; }
			public byte? FreezedStatus { get; internal set; }
			public string FullStatus => AspireFormats.GetRegisteredCourseStatusString(this.Status, this.FreezedStatus);
		}

		public static List<RegisteredCourse> GetRegisteredCourses(int studentID, short? semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var registeredCourses = aspireContext.RegisteredCourses
					.Where(rc => rc.StudentID == studentID && rc.DeletedDate == null && rc.Cours.CustomMarks)
					.Where(rc => rc.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				if (semesterID != null)
					registeredCourses = registeredCourses.Where(rc => rc.OfferedCours.SemesterID == semesterID);

				return registeredCourses.Select(rc => new RegisteredCourse
				{
					FacultyMemberName = rc.OfferedCours.FacultyMember.Name,
					FreezedStatus = rc.FreezedStatus,
					OfferedProgram = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					OfferedSection = rc.OfferedCours.Section,
					OfferedSemesterNo = rc.OfferedCours.SemesterNo,
					OfferedSemesterID = rc.OfferedCours.SemesterID,
					OfferedShift = rc.OfferedCours.Shift,
					OfferedTitle = rc.OfferedCours.Cours.Title,
					OfferedCourseCode = rc.OfferedCours.Cours.CourseCode,
					RegisteredCourseID = rc.RegisteredCourseID,
					RoadmapCourseCode = rc.Cours.CourseCode,
					RoadmapTitle = rc.Cours.Title,
					RoadmapCreditHours = rc.Cours.CreditHours,
					Status = rc.Status,
				}).OrderBy(rc => rc.OfferedSemesterID).ThenBy(rc => rc.RoadmapTitle).ToList();
			}
		}
	}
}
