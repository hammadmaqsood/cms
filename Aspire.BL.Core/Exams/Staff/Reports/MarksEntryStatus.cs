﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Staff.Reports
{
	public static class MarksEntryStatus
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class OfferedCourse
		{
			private static string ToYesNo(bool submitted, bool entered) => submitted ? "Yes" : (entered ? "Saved" : "No");

			public bool AssignmentsMarksEntered { get; internal set; }
			public bool AssignmentsMarksSubmitted { get; internal set; }
			public string AssignmentsMarksSubmittedString => ToYesNo(this.AssignmentsMarksSubmitted, this.AssignmentsMarksEntered);
			public int? DepartmentID { get; internal set; }
			public string DepartmentName { get; internal set; }
			public int? FacultyMemberID { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public bool FinalMarksSubmitted { get; internal set; }
			public bool FinalMarksEntered { get; internal set; }
			public string FinalMarksSubmittedString => ToYesNo(this.FinalMarksSubmitted, this.FinalMarksEntered);
			public bool MarksEntryLocked { get; internal set; }
			public string MarksEntryLockedString => this.MarksEntryLocked.ToYesNo();
			public bool MidMarksSubmitted { get; internal set; }
			public bool MidMarksEntered { get; internal set; }
			public string MidMarksSubmittedString => ToYesNo(this.MidMarksSubmitted, this.MidMarksEntered);
			public string ProgramAlias { get; internal set; }
			public int ProgramID { get; internal set; }
			public string ProgramName { get; internal set; }
			public bool QuizzesMarksSubmitted { get; internal set; }
			public bool QuizzesMarksEntered { get; internal set; }
			public string QuizzesMarksSubmittedString => ToYesNo(this.QuizzesMarksSubmitted, this.QuizzesMarksEntered);
			public int Section { get; internal set; }
			public short SemesterNo { get; internal set; }
			public byte Shift { get; internal set; }
			public string Title { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public List<OfferedCourse> GetList()
			{
				return null;
			}
		}

		public sealed class PageHeader
		{
			public ExamMarksTypes? ExamMarksTypes { get; internal set; }
			public short SemesterID { get; internal set; }
			public string ReportTitle => this.ExamMarksTypes == null
				? $"Marks Entry Status ({this.SemesterID.ToSemesterString()})"
				: $"Marks Entry Status ({this.SemesterID.ToSemesterString()}), Defaulter Types: {this.ExamMarksTypes.Value.ToFullNames()}";

			public string InstituteName { get; internal set; }

			public List<PageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet()
			{
			}

			public PageHeader PageHeader { get; internal set; }
			public List<OfferedCourse> OfferedCourses { get; internal set; }
		}

		public static ReportDataSet GetMarksEntryDefaulters(short semesterID, int? departmentID, int? programID, ExamMarksTypes? examMarksTypes, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var offeredCourses = aspireContext.OfferedCourses
					.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(oc => oc.SemesterID == semesterID);
				if (departmentID != null)
					offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
				if (programID != null)
					offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.ProgramID == programID.Value);

				var examMarksTypesEnums = examMarksTypes?.GetFlags().Cast<byte>().ToList();
				if (examMarksTypesEnums != null)
					offeredCourses = offeredCourses.Where(oc =>
						(examMarksTypesEnums.Contains((byte)ExamMarksTypes.Assignments) && !oc.AssignmentsMarksSubmitted)
						|| (examMarksTypesEnums.Contains((byte)ExamMarksTypes.Quizzes) && !oc.QuizzesMarksSubmitted)
						|| (examMarksTypesEnums.Contains((byte)ExamMarksTypes.Mid) && !oc.MidMarksSubmitted)
						|| (examMarksTypesEnums.Contains((byte)ExamMarksTypes.Final) && !oc.FinalMarksSubmitted));

				var query = offeredCourses.Select(oc => new OfferedCourse
				{
					DepartmentID = oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
					DepartmentName = oc.Cours.AdmissionOpenProgram.Program.Department.DepartmentName,
					ProgramID = oc.Cours.AdmissionOpenProgram.ProgramID,
					ProgramAlias = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					Title = oc.Cours.Title,
					SemesterNo = oc.SemesterNo,
					Section = oc.Section,
					Shift = oc.Shift,
					AssignmentsMarksSubmitted = oc.AssignmentsMarksSubmitted,
					AssignmentsMarksEntered = oc.RegisteredCourses.Any(rc => rc.DeletedDate == null && rc.Assignments != null),
					QuizzesMarksSubmitted = oc.QuizzesMarksSubmitted,
					QuizzesMarksEntered = oc.RegisteredCourses.Any(rc => rc.DeletedDate == null && rc.Quizzes != null),
					MidMarksSubmitted = oc.MidMarksSubmitted,
					MidMarksEntered = oc.RegisteredCourses.Any(rc => rc.DeletedDate == null && rc.Mid != null),
					FinalMarksSubmitted = oc.FinalMarksSubmitted,
					FinalMarksEntered = oc.RegisteredCourses.Any(rc => rc.DeletedDate == null && rc.Final != null),
					MarksEntryLocked = oc.MarksEntryLocked,
					FacultyMemberID = oc.FacultyMemberID,
					FacultyMemberName = oc.FacultyMember.Name,
				});

				return new ReportDataSet
				{
					OfferedCourses = query.ToList(),
					PageHeader = new PageHeader
					{
						ExamMarksTypes = examMarksTypes,
						SemesterID = semesterID,
						InstituteName = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).SingleOrDefault()
					},
				};
			}
		}
	}
}
