﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Staff.Reports
{
	public static class ProjectThesisInternshipAwardList
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class ProjectThesisInternship
		{
			public int InstituteID { get; internal set; }
			public string InstituteAlias { get; internal set; }
			public string InstituteName { get; internal set; }
			public string CourseCode { get; internal set; }
			public decimal? CreditHours { get; internal set; }
			public string CourseTitle { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public int AdmissionOpenProgramID { get; internal set; }
			public int ProgramID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public string IntakeSemester => this.IntakeSemesterID.ToSemesterString();
			public int? RegistrationNo { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string Title { get; internal set; }
			public string Organization { get; internal set; }
			public DateTime? FromDate { get; internal set; }
			public DateTime? ToDate { get; internal set; }
			public DateTime? ReportSubmissionDate { get; internal set; }
			public DateTime? DefenceVivaDate { get; internal set; }
			public DateTime? CompletionDate { get; internal set; }
			public decimal Total { get; internal set; }
			public ExamGrades GradeEnum { get; internal set; }
			public string GradeFullName => this.GradeEnum.ToFullName();
			internal List<Examiner> ExaminersList { get; set; }
			public string Examiners => this.ExaminersList.Select(e => $"{e.ExaminerName} ({e.ExaminerTypeFullName})").Join(", ");

			public static List<ProjectThesisInternship> GetProjectThesisInternshipList()
			{
				return null;
			}

			public sealed class Examiner
			{
				public Guid ExaminerType { get; internal set; }
				public string ExaminerTypeFullName => this.ExaminerType.GetFullName();
				public string ExaminerName { get; internal set; }

				public static List<Examiner> GetExaminerList()
				{
					return null;
				}
			}
		}

		public static List<ProjectThesisInternship> GetData(short semesterID, int? departmentID, int? programID, Model.Entities.ProjectThesisInternship.RecordTypes recordTypeEnum, DateTime? completionDateFrom, DateTime? completionDateTo, bool? marksEntryLocked, bool? submittedToHOD, bool? submittedToCampus, bool? submittedToUniversity, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var recordTypeGuid = recordTypeEnum.GetGuid();
				var projectThesisInternships = aspireContext.ProjectThesisInternships
					.Where(pti => pti.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(pti => pti.SemesterID == semesterID)
					.Where(pti => pti.RecordType == recordTypeGuid);
				if (departmentID != null)
					projectThesisInternships = projectThesisInternships.Where(pti => pti.Student.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
				if (programID != null)
					projectThesisInternships = projectThesisInternships.Where(pti => pti.Student.AdmissionOpenProgram.ProgramID == programID.Value);
				if (completionDateFrom != null)
					projectThesisInternships = projectThesisInternships.Where(pti => completionDateFrom.Value <= pti.CompletionDate);
				if (completionDateTo != null)
					projectThesisInternships = projectThesisInternships.Where(pti => pti.CompletionDate < completionDateTo.Value);
				if (marksEntryLocked != null)
					projectThesisInternships = projectThesisInternships.Where(pti => pti.MarksEntryLocked == marksEntryLocked.Value);
				if (submittedToHOD == true)
					projectThesisInternships = projectThesisInternships.Where(pti => pti.SubmittedToHODDate != null);
				else if (submittedToHOD == false)
					projectThesisInternships = projectThesisInternships.Where(pti => pti.SubmittedToHODDate == null);
				if (submittedToCampus == true)
					projectThesisInternships = projectThesisInternships.Where(pti => pti.SubmittedToCampusDate != null);
				else if (submittedToCampus == false)
					projectThesisInternships = projectThesisInternships.Where(pti => pti.SubmittedToCampusDate == null);
				if (submittedToUniversity == true)
					projectThesisInternships = projectThesisInternships.Where(pti => pti.SubmittedToUniversityDate != null);
				else if (submittedToUniversity == false)
					projectThesisInternships = projectThesisInternships.Where(pti => pti.SubmittedToUniversityDate == null);

				return projectThesisInternships.Select(pti => new ProjectThesisInternship
				{
					InstituteID = pti.Student.AdmissionOpenProgram.Program.InstituteID,
					InstituteAlias = pti.Student.AdmissionOpenProgram.Program.Institute.InstituteAlias,
					InstituteName = pti.Student.AdmissionOpenProgram.Program.Institute.InstituteName,
					CourseCode = pti.RegisteredCours.Cours.CourseCode,
					CreditHours = pti.RegisteredCours.Cours.CreditHours,
					CourseTitle = pti.RegisteredCours.Cours.Title,
					SemesterID = pti.SemesterID,
					AdmissionOpenProgramID = pti.Student.AdmissionOpenProgramID,
					ProgramID = pti.Student.AdmissionOpenProgram.ProgramID,
					ProgramAlias = pti.Student.AdmissionOpenProgram.Program.ProgramAlias,
					IntakeSemesterID = pti.Student.AdmissionOpenProgram.SemesterID,
					RegistrationNo = pti.Student.RegistrationNo,
					Enrollment = pti.Student.Enrollment,
					Name = pti.Student.Name,
					Title = pti.Title,
					Organization = pti.Organization,
					FromDate = pti.FromDate,
					ToDate = pti.ToDate,
					ReportSubmissionDate = pti.ReportSubmissionDate,
					DefenceVivaDate = pti.DefenceVivaDate,
					CompletionDate = pti.CompletionDate,
					Total = pti.Total,
					GradeEnum = (ExamGrades)pti.Grade,
					ExaminersList = pti.ProjectThesisInternshipExaminers.Select(ptim => new ProjectThesisInternship.Examiner
					{
						ExaminerType = ptim.ExaminerType,
						ExaminerName = ptim.ExaminerName
					}).ToList()
				}).OrderBy(pti => pti.ProgramAlias).ThenBy(pti => pti.IntakeSemesterID).ToList();
			}
		}
	}
}
