﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Staff.Reports
{
	public static class SemesterFreeze
	{
		private static BL.Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class SemesterFreezePageHeader
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Title => $"Semester Freeze - {this.SemesterID.ToSemesterString()}";
			public static List<SemesterFreezePageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class Student
		{
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string Name { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public string DepartmentAlias { get; internal set; }
			public int ProgramID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public byte? StudentStatus { get; internal set; }
			public string StudentStatusFullName => ((Model.Entities.Student.Statuses?)this.StudentStatus)?.ToFullName();
			public byte? FreezeStatus { get; internal set; }
			public string FreezeStatusFullName => ((Model.Entities.Common.FreezedStatuses?)this.FreezeStatus)?.ToFullName();
			public DateTime? FreezeDate { get; internal set; }

			public static List<Student> GetSemesterFreezeStudentsList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public SemesterFreezePageHeader PageHeader { get; internal set; }
			public List<Student> Students { get; internal set; }
		}

		public static ReportDataSet GetSemesterFreezeList(short semesterID, int? departmentID, int? programID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var studentsSemesters = aspireContext.StudentSemesters
					.Where(ss => ss.SemesterID == semesterID)
					.Where(ss => ss.FreezedStatus != null)
					.Where(ss => ss.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				if (departmentID != null)
					studentsSemesters = studentsSemesters.Where(ss => ss.Student.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
				if (programID != null)
					studentsSemesters = studentsSemesters.Where(ss => ss.Student.AdmissionOpenProgram.ProgramID == programID.Value);

				return new ReportDataSet
				{
					PageHeader = new SemesterFreezePageHeader
					{
						SemesterID = semesterID,
						Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single(),
					},
					Students = studentsSemesters.Select(ss => new Student
					{
						StudentID = ss.StudentID,
						Enrollment = ss.Student.Enrollment,
						RegistrationNo = ss.Student.RegistrationNo,
						Name = ss.Student.Name,
						ProgramID = ss.Student.AdmissionOpenProgram.ProgramID,
						ProgramAlias = ss.Student.AdmissionOpenProgram.Program.ProgramAlias,
						DepartmentID = ss.Student.AdmissionOpenProgram.Program.DepartmentID,
						DepartmentAlias = ss.Student.AdmissionOpenProgram.Program.Department.DepartmentAlias,
						StudentStatus = ss.Student.Status,
						FreezeStatus = ss.FreezedStatus.Value,
						FreezeDate = ss.FreezedDate.Value, 
					}).ToList()
				};
			}
		}
	}
}
