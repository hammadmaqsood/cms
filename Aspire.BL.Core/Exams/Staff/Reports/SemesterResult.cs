﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.Exams.Staff.Reports
{
	public static class SemesterResult
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.CampusLevelMarksEntry, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class RegisteredCourse
		{
			public int RegisteredCourseID { get; internal set; }
			public int OfferedCourseID { get; internal set; }
			public int? RegisteredCourseDepartmentID { get; internal set; }
			public string RegisteredCourseDepartmentAlias { get; internal set; }
			public int RegisteredCourseProgramID { get; internal set; }
			public string RegisteredCourseProgramAlias { get; internal set; }
			public string Enrollment { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string Name { get; internal set; }
			public int RegisteredCourseAdmissionOpenProgramID { get; internal set; }
			public short RegisteredCourseRoadmapSemesterID { get; internal set; }
			public string RegisteredCourseRoadmapSemester => this.RegisteredCourseRoadmapSemesterID.ToSemesterString();
			public string RegisteredCourseCode { get; internal set; }
			public string RegisteredCourseTitle { get; internal set; }
			public decimal RegisteredCourseCreditHours { get; internal set; }
			public string RegisteredCourseCreditHoursFullName => this.RegisteredCourseCreditHours.ToCreditHoursFullName();
			public string OfferedProgramAlias { get; internal set; }
			public SemesterNos OfferedSemesterNoEnum { get; internal set; }
			public Sections OfferedSectionEnum { get; internal set; }
			public Shifts OfferedShiftEnum { get; internal set; }
			public string OfferedClass => AspireFormats.GetClassName(this.OfferedProgramAlias, this.OfferedSemesterNoEnum, this.OfferedSectionEnum, this.OfferedShiftEnum);
			public string FacultyMemberName { get; internal set; }
			public string OfferedTitle { get; internal set; }
			public decimal OfferedCreditHours { get; internal set; }
			public string OfferedCreditHoursFullName => this.OfferedCreditHours.ToCreditHoursFullName();
			public Model.Entities.Student.Statuses? StudentStatusEnum { get; internal set; }
			public string StudentStatusFullName => this.StudentStatusEnum?.ToFullName();
			public ExamGrades? RegisteredCourseGradeEnum { get; internal set; }
			public RegisteredCours.Statuses? RegisteredCourseStatusEnum { get; internal set; }
			public FreezedStatuses? RegisteredCourseFreezeStatusEnum { get; internal set; }
			public string RegisteredCourseStatusString => AspireFormats.GetRegisteredCourseStatusString(this.RegisteredCourseStatusEnum, this.RegisteredCourseFreezeStatusEnum);
			public int? OfferedDepartmentID { get; set; }
			public string OfferedDepartmentAlias { get; set; }
			public int OfferedProgramID { get; set; }
			public bool FeeDefaulter { get; internal set; }
			public string FeeDefaulterYesNo => this.FeeDefaulter.ToYesNo();
			public int StudentID { get; internal set; }
			public int RegisteredCourseCourseID { get; internal set; }
			public int OfferedCourseCourseID { get; internal set; }
			public string RegisteredCourseGradeString => AspireFormats.GetGradeString(this.RegisteredCourseGradeEnum, this.RegisteredCourseStatusEnum);

			public List<RegisteredCourse> GetList()
			{
				return null;
			}
		}

		public sealed class PageHeader
		{
			public short SemesterID { get; internal set; }
			public string ReportTitle => $"Semester Result {this.SemesterID.ToSemesterString()}";
			public string InstituteName { get; internal set; }

			public List<PageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet()
			{
			}

			public PageHeader PageHeader { get; internal set; }
			public List<RegisteredCourse> RegisteredCourses { get; internal set; }
		}

		public static ReportDataSet GetSemesterResult(short semesterID, int? departmentID, int? programID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var offeredCourses = aspireContext.OfferedCourses
					.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(oc => oc.SemesterID == semesterID)
					.Where(oc => oc.MarksEntryCompleted && oc.SubmittedToUniversityDate != null);
				if (departmentID != null)
					offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
				if (programID != null)
					offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.ProgramID == programID.Value);

				var registeredCourses = offeredCourses.SelectMany(oc => oc.RegisteredCourses).Where(rc => rc.DeletedDate == null);

				var query = registeredCourses.Select(rc => new RegisteredCourse
				{
					RegisteredCourseID = rc.RegisteredCourseID,
					StudentID = rc.StudentID,
					Name = rc.Student.Name,
					Enrollment = rc.Student.Enrollment,
					RegistrationNo = rc.Student.RegistrationNo,
					StudentStatusEnum = (Model.Entities.Student.Statuses?)rc.Student.Status,
					OfferedCourseID = rc.OfferedCourseID,
					OfferedCourseCourseID = rc.OfferedCours.CourseID,
					OfferedDepartmentID = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.DepartmentID,
					OfferedDepartmentAlias = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.Department.DepartmentAlias,
					OfferedProgramID = rc.OfferedCours.Cours.AdmissionOpenProgram.ProgramID,
					OfferedProgramAlias = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					OfferedSemesterNoEnum = (SemesterNos)rc.OfferedCours.SemesterNo,
					OfferedSectionEnum = (Sections)rc.OfferedCours.Section,
					OfferedShiftEnum = (Shifts)rc.OfferedCours.Shift,
					OfferedTitle = rc.OfferedCours.Cours.Title,
					OfferedCreditHours = rc.OfferedCours.Cours.CreditHours,
					FacultyMemberName = rc.OfferedCours.FacultyMember.Name,
					RegisteredCourseCourseID = rc.CourseID,
					RegisteredCourseCode = rc.Cours.CourseCode,
					RegisteredCourseTitle = rc.Cours.Title,
					RegisteredCourseCreditHours = rc.Cours.CreditHours,
					RegisteredCourseDepartmentAlias = rc.Cours.AdmissionOpenProgram.Program.Department.DepartmentAlias,
					RegisteredCourseDepartmentID = rc.Cours.AdmissionOpenProgram.Program.DepartmentID,
					RegisteredCourseProgramAlias = rc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					RegisteredCourseProgramID = rc.Cours.AdmissionOpenProgram.ProgramID,
					RegisteredCourseAdmissionOpenProgramID = rc.Cours.AdmissionOpenProgramID,
					RegisteredCourseRoadmapSemesterID = rc.Cours.AdmissionOpenProgram.SemesterID,
					RegisteredCourseGradeEnum = (ExamGrades?)rc.Grade,
					RegisteredCourseStatusEnum = (RegisteredCours.Statuses?)rc.Status,
					RegisteredCourseFreezeStatusEnum = (FreezedStatuses?)rc.FreezedStatus,
					FeeDefaulter = rc.FeeDefaulter
				});

				return new ReportDataSet
				{
					RegisteredCourses = query.ToList(),
					PageHeader = new PageHeader
					{
						SemesterID = semesterID,
						InstituteName = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).SingleOrDefault()
					},
				};
			}
		}
	}
}
