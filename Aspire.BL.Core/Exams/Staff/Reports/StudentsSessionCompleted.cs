﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Aspire.Lib.Extensions;

namespace Aspire.BL.Core.Exams.Staff.Reports
{
	public static class StudentsSessionCompleted
	{
		private static BL.Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class StudentsSessionCompletedPageHeader
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Title => $"Session Completed in Semester ({this.SemesterID.ToSemesterString()})";
			public static List<StudentsSessionCompletedPageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class StudentSessionCompleted
		{
			public short IntakeSemesterID { get; internal set; }
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string Name { get; internal set; }
			public int ProgramID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public byte Duration { get; internal set; }
			public short? SemesterNo { get; internal set; }
			public int? Section { get; internal set; }
			public byte? Shift { get; internal set; }
			public byte? StudentStatus { get; internal set; }
			public decimal? CGPA { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);

			public string DurationFullName => ((ProgramDurations)this.Duration).ToShortName();
			public string IntakeSemester => this.IntakeSemesterID.ToSemesterString();
			public string CGPAShort => this.CGPA?.ToString("0.##");
			public string StudentStatusFullName => ((Model.Entities.Student.Statuses?)this.StudentStatus)?.ToFullName() ?? string.Empty.ToNAIfNullOrEmpty();

			public static List<StudentSessionCompleted> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public StudentsSessionCompletedPageHeader PageHeader { get; internal set; }
			public List<StudentSessionCompleted> StudentsSessionCompleted { get; internal set; }
		}

		public static ReportDataSet GetStudentsSessionCompleted(short semesterID, int? departmentID, int? programID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var studentsSemesters = aspireContext.StudentSemesters.Where(ss => ss.SemesterID == semesterID).AsQueryable();
				var admissionOpenProgram = aspireContext.AdmissionOpenPrograms.Where(aop => aop.Program.InstituteID == loginHistory.InstituteID).AsQueryable();
				if (departmentID != null)
					admissionOpenProgram = admissionOpenProgram.Where(aop => aop.Program.DepartmentID == departmentID);
				if (programID != null)
					admissionOpenProgram = admissionOpenProgram.Where(aop => aop.ProgramID == programID);
				var studentsSessionCompleted = from s in studentsSemesters
											   join aop in admissionOpenProgram on s.Student.AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
											   where aop.FinalSemesterID == semesterID
											   select new StudentSessionCompleted
											   {
												   IntakeSemesterID = aop.SemesterID,
												   Enrollment = s.Student.Enrollment,
												   RegistrationNo = s.Student.RegistrationNo,
												   Name = s.Student.Name,
												   ProgramAlias = aop.Program.ProgramAlias,
												   Duration = aop.Program.Duration,
												   SemesterNo = s.SemesterNo,
												   Shift = s.Shift,
												   Section = s.Section,
												   CGPA = s.Student.FinalCGPA,
												   StudentStatus = s.Student.Status,
											   };
				studentsSessionCompleted = studentsSessionCompleted.OrderBy(o => o.Enrollment);
				return new ReportDataSet
				{
					PageHeader = new StudentsSessionCompletedPageHeader
					{
						SemesterID = semesterID,
						Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single(),
					},
					StudentsSessionCompleted = studentsSessionCompleted.ToList(),
				};
			}
		}
	}
}
