﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Exams.Staff
{
	public static class OfferedCourses
	{
		public sealed class OfferedCourse
		{
			internal OfferedCourse()
			{
			}

			public int OfferedCourseID { get; internal set; }
			public int CourseID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Title { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string Teacher { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public CourseCategories CategoryEnum { get; internal set; }
			public bool MarksEntryCompleted { get; internal set; }
			public DateTime? SubmittedToHOD { get; internal set; }
			public DateTime? SubmittedToCampus { get; internal set; }
			public DateTime? SubmittedToUniversity { get; internal set; }
			public bool MarksSheetVisible => this.CategoryEnum == CourseCategories.Course || this.CategoryEnum == CourseCategories.Lab;
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
		}

		public static List<OfferedCourse> GetOfferedCourses(short semesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int? facultyMemberID, bool? marksEntryCompleted, bool? submittedToHOD, bool? submittedToCampus, bool? submittedToUniversity, int pageIndex, int pageSize, out int virtualItemCount, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);

				var courses = aspireContext.OfferedCourses
					.Where(oc => oc.SemesterID == semesterID)
					.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);

				if (departmentID != null)
					courses = courses.Where(c => c.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID);
				if (programID != null)
					courses = courses.Where(c => c.Cours.AdmissionOpenProgram.ProgramID == programID);
				if (semesterNoEnum != null)
				{
					var semesterNo = (short)semesterNoEnum;
					courses = courses.Where(c => c.SemesterNo == semesterNo);
				}
				if (sectionEnum != null)
				{
					var section = (int)sectionEnum;
					courses = courses.Where(c => c.Section == section);
				}
				if (shiftEnum != null)
				{
					var shift = (byte)shiftEnum;
					courses = courses.Where(c => c.Shift == shift);
				}
				if (facultyMemberID != null)
					courses = courses.Where(c => c.FacultyMemberID == facultyMemberID);
				if (marksEntryCompleted != null)
					courses = courses.Where(c => c.MarksEntryCompleted == marksEntryCompleted.Value);
				if (submittedToHOD != null)
					courses = submittedToHOD == true
						? courses.Where(c => c.SubmittedToHODDate != null)
						: courses.Where(c => c.SubmittedToHODDate == null);
				if (submittedToCampus != null)
					courses = submittedToCampus == true
						? courses.Where(c => c.SubmittedToCampusDate != null)
						: courses.Where(c => c.SubmittedToCampusDate == null);
				if (submittedToUniversity != null)
					courses = submittedToUniversity == true
						? courses.Where(c => c.SubmittedToUniversityDate != null)
						: courses.Where(c => c.SubmittedToUniversityDate == null);

				virtualItemCount = courses.Count();
				switch (sortExpression)
				{
					case nameof(OfferedCourse.SemesterID):
						courses = courses.OrderBy(sortDirection, c => c.SemesterID);
						break;
					case nameof(OfferedCourse.Title):
						courses = courses.OrderBy(sortDirection, c => c.Cours.Title);
						break;
					case nameof(OfferedCourse.Teacher):
						courses = courses.OrderBy(sortDirection, c => c.FacultyMember.Name);
						break;
					case nameof(OfferedCourse.CategoryEnum):
						courses = courses.OrderBy(sortDirection, c => c.Cours.CourseCategory);
						break;
					case nameof(OfferedCourse.Class):
						courses = courses
							.OrderBy(sortDirection, c => c.Cours.AdmissionOpenProgram.Program.ProgramAlias)
							.ThenBy(sortDirection, c => c.SemesterNo)
							.ThenBy(sortDirection, c => c.Section)
							.ThenBy(sortDirection, c => c.Shift);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				courses = courses.Skip(pageIndex * pageSize).Take(pageSize);
				return courses.Select(c => new OfferedCourse
				{
					OfferedCourseID = c.OfferedCourseID,
					CourseID = c.Cours.CourseID,
					Title = c.Cours.Title,
					ProgramAlias = c.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterID = c.SemesterID,
					SemesterNo = c.SemesterNo,
					Section = c.Section,
					Shift = c.Shift,
					Teacher = c.FacultyMember.Name,
					CategoryEnum = (CourseCategories)c.Cours.CourseCategory,
					MarksEntryCompleted = c.MarksEntryCompleted,
					SubmittedToHOD = c.SubmittedToHODDate,
					SubmittedToCampus = c.SubmittedToCampusDate,
					SubmittedToUniversity = c.SubmittedToUniversityDate
				}).ToList();
			}
		}
	}
}