﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Exams.Admin
{
	public static class ExamMarksPolicy
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Exams.ManageExamMarksPolicies, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.Exams.ManageExamMarksPolicies, UserGroupPermission.PermissionValues.Allowed);
		}

		public static List<Model.Entities.ExamMarksPolicy> GetExamMarksPolicies(int? instituteID, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				var examMarksPolicies = aspireContext.GetAdminInstitutes(loginHistory)
					.Where(i => instituteID == null || i.InstituteID == instituteID.Value)
					.SelectMany(i => i.ExamMarksPolicies);
				virtualItemCount = examMarksPolicies.Count();
				switch (sortExpression)
				{
					case nameof(Model.Entities.ExamMarksPolicy.ExamMarksPolicyName):
						examMarksPolicies = examMarksPolicies.OrderBy(sortDirection, emp => emp.ExamMarksPolicyName);
						break;
					case nameof(Model.Entities.ExamMarksPolicy.ExamMarksPolicyDescription):
						examMarksPolicies = examMarksPolicies.OrderBy(sortDirection, emp => emp.ExamMarksPolicyDescription);
						break;
					case nameof(Institute.InstituteAlias):
						examMarksPolicies = examMarksPolicies.OrderBy(sortDirection, emp => emp.Institute.InstituteAlias);
						break;
					case nameof(Model.Entities.ExamMarksPolicy.ValidFromSemesterID):
						examMarksPolicies = examMarksPolicies.OrderBy(sortDirection, emp => emp.ValidFromSemesterID).ThenBy(sortDirection, emp => emp.ValidUpToSemesterID);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return examMarksPolicies.Include(emp => emp.Institute).Skip(pageIndex * pageSize).Take(pageSize).ToList();
			}
		}

		public static Model.Entities.ExamMarksPolicy GetExamMarksPolicy(int examMarksPolicyID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.ExamMarksPolicies)
					.Include(emp => emp.Institute)
					.SingleOrDefault(emp => emp.ExamMarksPolicyID == examMarksPolicyID);
			}
		}

		public enum AddExamMarksPolicyStatuses
		{
			NameAlreadyExists,
			RecordNotValid,
			Success,
		}

		public static AddExamMarksPolicyStatuses AddExamMarksPolicy(Model.Entities.ExamMarksPolicy examMarksPolicy, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(examMarksPolicy.InstituteID, loginSessionGuid);
				examMarksPolicy.ExamMarksPolicyName = examMarksPolicy.ExamMarksPolicyName.TrimAndCannotBeEmpty();
				examMarksPolicy.ExamMarksPolicyDescription = examMarksPolicy.ExamMarksPolicyDescription.TrimAndMakeItNullIfEmpty();
				var alreadyExists = aspireContext.ExamMarksPolicies.Any(emp => emp.InstituteID == examMarksPolicy.InstituteID && emp.ExamMarksPolicyName == examMarksPolicy.ExamMarksPolicyName);
				if (alreadyExists)
					return AddExamMarksPolicyStatuses.NameAlreadyExists;
				var errorMessage = examMarksPolicy.Validate();
				if (errorMessage != null)
					return AddExamMarksPolicyStatuses.RecordNotValid;
				examMarksPolicy.ChecksumMD5 = examMarksPolicy.CalculateChecksumMD5();
				aspireContext.ExamMarksPolicies.Add(examMarksPolicy);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddExamMarksPolicyStatuses.Success;
			}
		}

		public enum UpdateExamMarksPolicyStatuses
		{
			NoRecordFound,
			NameAlreadyExists,
			RecordNotValid,
			Success,
		}

		public static UpdateExamMarksPolicyStatuses UpdateExamMarksPolicy(Model.Entities.ExamMarksPolicy examMarksPolicy, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var dbExamMarksPolicy = aspireContext.ExamMarksPolicies.SingleOrDefault(emp => emp.ExamMarksPolicyID == examMarksPolicy.ExamMarksPolicyID);
				if (dbExamMarksPolicy == null)
					return UpdateExamMarksPolicyStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(dbExamMarksPolicy.InstituteID, loginSessionGuid);
				examMarksPolicy.ExamMarksPolicyName = examMarksPolicy.ExamMarksPolicyName.TrimAndCannotBeEmpty();
				examMarksPolicy.ExamMarksPolicyDescription = examMarksPolicy.ExamMarksPolicyDescription.TrimAndMakeItNullIfEmpty();
				var alreadyExists = aspireContext.ExamMarksPolicies
					.Where(emp => emp.ExamMarksPolicyID != examMarksPolicy.ExamMarksPolicyID)
					.Any(emp => emp.InstituteID == examMarksPolicy.InstituteID && emp.ExamMarksPolicyName == examMarksPolicy.ExamMarksPolicyName);
				if (alreadyExists)
					return UpdateExamMarksPolicyStatuses.NameAlreadyExists;

				dbExamMarksPolicy.ExamMarksPolicyName = examMarksPolicy.ExamMarksPolicyName;
				dbExamMarksPolicy.ValidFromSemesterID = examMarksPolicy.ValidFromSemesterID;
				dbExamMarksPolicy.ValidUpToSemesterID = examMarksPolicy.ValidUpToSemesterID;
				dbExamMarksPolicy.ExamMarksPolicyDescription = examMarksPolicy.ExamMarksPolicyDescription;
				dbExamMarksPolicy.CourseMarksEntryType = examMarksPolicy.CourseMarksEntryType;
				dbExamMarksPolicy.LabMarksEntryType = examMarksPolicy.LabMarksEntryType;
				dbExamMarksPolicy.CourseAssignments = examMarksPolicy.CourseAssignments;
				dbExamMarksPolicy.CourseQuizzes = examMarksPolicy.CourseQuizzes;
				dbExamMarksPolicy.CourseInternals = examMarksPolicy.CourseInternals;
				dbExamMarksPolicy.CourseMid = examMarksPolicy.CourseMid;
				dbExamMarksPolicy.CourseFinal = examMarksPolicy.CourseFinal;
				dbExamMarksPolicy.LabAssignments = examMarksPolicy.LabAssignments;
				dbExamMarksPolicy.LabQuizzes = examMarksPolicy.LabQuizzes;
				dbExamMarksPolicy.LabInternals = examMarksPolicy.LabInternals;
				dbExamMarksPolicy.LabMid = examMarksPolicy.LabMid;
				dbExamMarksPolicy.LabFinal = examMarksPolicy.LabFinal;
				dbExamMarksPolicy.AMin = examMarksPolicy.AMin;
				dbExamMarksPolicy.AMax = examMarksPolicy.AMax;
				dbExamMarksPolicy.AMinusMin = examMarksPolicy.AMinusMin;
				dbExamMarksPolicy.AMinusMax = examMarksPolicy.AMinusMax;
				dbExamMarksPolicy.BPlusMin = examMarksPolicy.BPlusMin;
				dbExamMarksPolicy.BPlusMax = examMarksPolicy.BPlusMax;
				dbExamMarksPolicy.BMin = examMarksPolicy.BMin;
				dbExamMarksPolicy.BMax = examMarksPolicy.BMax;
				dbExamMarksPolicy.BMinusMin = examMarksPolicy.BMinusMin;
				dbExamMarksPolicy.BMinusMax = examMarksPolicy.BMinusMax;
				dbExamMarksPolicy.CPlusMin = examMarksPolicy.CPlusMin;
				dbExamMarksPolicy.CPlusMax = examMarksPolicy.CPlusMax;
				dbExamMarksPolicy.CMin = examMarksPolicy.CMin;
				dbExamMarksPolicy.CMax = examMarksPolicy.CMax;
				dbExamMarksPolicy.CMinusMin = examMarksPolicy.CMinusMin;
				dbExamMarksPolicy.CMinusMax = examMarksPolicy.CMinusMax;
				dbExamMarksPolicy.DPlusMin = examMarksPolicy.DPlusMin;
				dbExamMarksPolicy.DPlusMax = examMarksPolicy.DPlusMax;
				dbExamMarksPolicy.DMin = examMarksPolicy.DMin;
				dbExamMarksPolicy.DMax = examMarksPolicy.DMax;
				dbExamMarksPolicy.FMax = examMarksPolicy.FMax;
				dbExamMarksPolicy.AGradePoint = examMarksPolicy.AGradePoint;
				dbExamMarksPolicy.AMinusGradePoint = examMarksPolicy.AMinusGradePoint;
				dbExamMarksPolicy.BPlusGradePoint = examMarksPolicy.BPlusGradePoint;
				dbExamMarksPolicy.BGradePoint = examMarksPolicy.BGradePoint;
				dbExamMarksPolicy.BMinusGradePoint = examMarksPolicy.BMinusGradePoint;
				dbExamMarksPolicy.CPlusGradePoint = examMarksPolicy.CPlusGradePoint;
				dbExamMarksPolicy.CGradePoint = examMarksPolicy.CGradePoint;
				dbExamMarksPolicy.CMinusGradePoint = examMarksPolicy.CMinusGradePoint;
				dbExamMarksPolicy.DPlusGradePoint = examMarksPolicy.DPlusGradePoint;
				dbExamMarksPolicy.DGradePoint = examMarksPolicy.DGradePoint;
				dbExamMarksPolicy.FGradePoint = examMarksPolicy.FGradePoint;
				dbExamMarksPolicy.SummerCappingExamGrade = examMarksPolicy.SummerCappingExamGrade;

				var errorMessage = dbExamMarksPolicy.Validate();
				if (errorMessage != null)
					return UpdateExamMarksPolicyStatuses.RecordNotValid;
				dbExamMarksPolicy.ChecksumMD5 = dbExamMarksPolicy.CalculateChecksumMD5();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateExamMarksPolicyStatuses.Success;
			}
		}

		public enum DeleteExamMarksPolicyStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteExamMarksPolicyStatuses DeleteExamMarksPolicy(int examMarksPolicyID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var examMarksPolicy = aspireContext.ExamMarksPolicies.SingleOrDefault(emp => emp.ExamMarksPolicyID == examMarksPolicyID);
				if (examMarksPolicy == null)
					return DeleteExamMarksPolicyStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(examMarksPolicy.InstituteID, loginSessionGuid);
				if (examMarksPolicy.AdmissionOpenProgramExamMarksPolicies.Any())
					return DeleteExamMarksPolicyStatuses.ChildRecordExists;
				if (examMarksPolicy.AdmissionOpenPrograms.Any())
					return DeleteExamMarksPolicyStatuses.ChildRecordExists;
				if (examMarksPolicy.Programs.Any())
					return DeleteExamMarksPolicyStatuses.ChildRecordExists;
				aspireContext.ExamMarksPolicies.Remove(examMarksPolicy);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteExamMarksPolicyStatuses.Success;
			}
		}
	}
}