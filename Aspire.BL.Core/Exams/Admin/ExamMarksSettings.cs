﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Exams.Admin
{
	public static class ExamMarksSettings
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int instituteID, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID, AdminPermissions.Exams.ManageControlPanel, UserGroupPermission.PermissionValues.Allowed);
		}

		public enum AddOrUpdateExamMarksSettingStatuses
		{
			Added,
			Updated,
		}

		private static AddOrUpdateExamMarksSettingStatuses AddOrUpdateExamMarksSetting(this AspireContext aspireContext, short semesterID, int programID, ExamMarksTypes examMarksTypesEnumFlags, DateTime fromDate, DateTime toDate, Model.Entities.ExamMarksSetting.Statuses statusEnum, Core.Common.Permissions.Admin.AdminLoginHistory loginHistory)
		{
			var examMarksSetting = aspireContext.ExamMarksSettings.SingleOrDefault(ems => ems.SemesterID == semesterID && ems.ProgramID == programID);
			AddOrUpdateExamMarksSettingStatuses status;
			if (examMarksSetting == null)
			{
				status = AddOrUpdateExamMarksSettingStatuses.Added;
				examMarksSetting = new Model.Entities.ExamMarksSetting
				{
					ProgramID = programID,
					SemesterID = semesterID,
				};
				aspireContext.ExamMarksSettings.Add(examMarksSetting);
			}
			else
				status = AddOrUpdateExamMarksSettingStatuses.Updated;

			examMarksSetting.ExamMarksTypesEnum = examMarksTypesEnumFlags;
			examMarksSetting.FromDate = fromDate;
			examMarksSetting.ToDate = toDate;
			examMarksSetting.StatusEnum = statusEnum;
			aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
			return status;
		}

		public sealed class AddOrUpdateExamMarksSettingSettingsResult
		{
			public enum Statuses
			{
				None,
				NoRecordFound,
				InvalidDateRange,
				Success,
			}

			public Statuses Status { get; internal set; }
			public uint TotalRecords { get; internal set; }
			public uint SuccessAdd { get; internal set; }
			public uint SuccessUpdate { get; internal set; }
		}

		public static AddOrUpdateExamMarksSettingSettingsResult AddOrUpdateExamMarksSettingSettings(short semesterID, int instituteID, List<int> programIDs, ExamMarksTypes examMarksTypesEnumFlags, DateTime fromDate, DateTime toDate, Model.Entities.ExamMarksSetting.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var addOrUpdateExamMarksSettingSettingsResult = new AddOrUpdateExamMarksSettingSettingsResult();
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				if (fromDate.Date > toDate.Date)
				{
					addOrUpdateExamMarksSettingSettingsResult.Status = AddOrUpdateExamMarksSettingSettingsResult.Statuses.InvalidDateRange;
					return addOrUpdateExamMarksSettingSettingsResult;
				}

				var validListProgramIDs = aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.Programs)
					.Where(p => programIDs.Contains(p.ProgramID))
					.Select(p => p.ProgramID).ToList();

				foreach (var programID in validListProgramIDs)
				{
					addOrUpdateExamMarksSettingSettingsResult.TotalRecords++;
					var result = aspireContext.AddOrUpdateExamMarksSetting(semesterID, programID, examMarksTypesEnumFlags, fromDate, toDate, statusEnum, loginHistory);
					switch (result)
					{
						case AddOrUpdateExamMarksSettingStatuses.Added:
							addOrUpdateExamMarksSettingSettingsResult.SuccessAdd++;
							break;
						case AddOrUpdateExamMarksSettingStatuses.Updated:
							addOrUpdateExamMarksSettingSettingsResult.SuccessUpdate++;
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
				aspireContext.CommitTransaction();
				addOrUpdateExamMarksSettingSettingsResult.Status = AddOrUpdateExamMarksSettingSettingsResult.Statuses.Success;
				return addOrUpdateExamMarksSettingSettingsResult;
			}
		}

		public enum DeleteExamMarksSettingStatuses
		{
			NoRecordFound,
			Success,
		}

		public static DeleteExamMarksSettingStatuses DeleteExamMarksSetting(int examMarksSettingID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var examMarksSetting = aspireContext.ExamMarksSettings.Include(ems => ems.Program).SingleOrDefault(ems => ems.ExamMarksSettingID == examMarksSettingID);
				if (examMarksSetting == null)
					return DeleteExamMarksSettingStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(examMarksSetting.Program.InstituteID, loginSessionGuid);
				aspireContext.ExamMarksSettings.Remove(examMarksSetting);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteExamMarksSettingStatuses.Success;
			}
		}

		public static Model.Entities.ExamMarksSetting ToggleExamMarksSettingStatus(int examMarksSettingID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var examMarksSetting = aspireContext.ExamMarksSettings.Include(ems => ems.Program).SingleOrDefault(ems => ems.ExamMarksSettingID == examMarksSettingID);
				if (examMarksSetting == null)
					return null;
				var loginHistory = aspireContext.DemandPermissions(examMarksSetting.Program.InstituteID, loginSessionGuid);

				switch (examMarksSetting.StatusEnum)
				{
					case Model.Entities.ExamMarksSetting.Statuses.Active:
						examMarksSetting.StatusEnum = Model.Entities.ExamMarksSetting.Statuses.Inactive;
						break;
					case Model.Entities.ExamMarksSetting.Statuses.Inactive:
						examMarksSetting.StatusEnum = Model.Entities.ExamMarksSetting.Statuses.Active;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return examMarksSetting;
			}
		}

		public static Model.Entities.ExamMarksSetting GetExamMarksSetting(int examMarksSettingID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.Programs)
					.SelectMany(p => p.ExamMarksSettings)
					.Include(ems => ems.Program)
					.SingleOrDefault(ems => ems.ExamMarksSettingID == examMarksSettingID);
			}
		}

		public sealed class ExamMarksSetting
		{
			public int ExamMarksSettingID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public string InstituteAlias { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string DepartmentAlias { get; set; }
			public byte ExamMarksType { get; internal set; }
			public string ExamMarksTypesFullNames => ((ExamMarksTypes)this.ExamMarksType).ToFullNames();
			public DateTime FromDate { get; internal set; }
			public DateTime ToDate { get; internal set; }
			public byte Status { get; internal set; }
			public Model.Entities.ExamMarksSetting.Statuses StatusEnum => (Model.Entities.ExamMarksSetting.Statuses)this.Status;
			public string StatusFullName => this.StatusEnum.ToFullName();
		}

		public static List<ExamMarksSetting> GetExamMarksSettings(short? semesterID, int? instituteID, int? departmentID, int? programID, Model.Entities.ExamMarksSetting.Statuses? statusEnum, DateTime? fromDate, DateTime? toDate, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				fromDate = fromDate?.Date;
				toDate = toDate?.Date.AddDays(1);
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.Programs)
					.SelectMany(p => p.ExamMarksSettings)
					.Where(q => semesterID == null || q.SemesterID == semesterID.Value)
					.Where(q => instituteID == null || q.Program.InstituteID == instituteID.Value)
					.Where(q => departmentID == null || q.Program.DepartmentID == departmentID.Value)
					.Where(q => programID == null || q.ProgramID == programID.Value)
					.Where(q => (byte?)statusEnum == null || q.Status == (byte)statusEnum.Value)
					.Where(q => fromDate == null || fromDate.Value <= q.FromDate)
					.Where(q => toDate == null || q.ToDate < toDate.Value);
				virtualItemCount = query.Count();
				switch (sortExpression)
				{
					case nameof(ExamMarksSetting.SemesterID):
					case nameof(ExamMarksSetting.Semester):
						query = query.OrderBy(sortDirection, q => q.SemesterID);
						break;
					case nameof(ExamMarksSetting.InstituteAlias):
						query = query.OrderBy(sortDirection, q => q.Program.Institute.InstituteAlias);
						break;
					case nameof(ExamMarksSetting.DepartmentAlias):
						query = query.OrderBy(sortDirection, q => q.Program.Department.DepartmentAlias);
						break;
					case nameof(ExamMarksSetting.ProgramAlias):
						query = query.OrderBy(sortDirection, q => q.Program.ProgramAlias);
						break;
					case nameof(ExamMarksSetting.ExamMarksType):
					case nameof(ExamMarksSetting.ExamMarksTypesFullNames):
						query = query.OrderBy(sortDirection, q => q.ExamMarksTypes);
						break;
					case nameof(ExamMarksSetting.FromDate):
						query = query.OrderBy(sortDirection, q => q.FromDate);
						break;
					case nameof(ExamMarksSetting.ToDate):
						query = query.OrderBy(sortDirection, q => q.ToDate);
						break;
					case nameof(ExamMarksSetting.Status):
					case nameof(ExamMarksSetting.StatusFullName):
						query = query.OrderBy(sortDirection, q => q.Status);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return query.Skip(pageIndex * pageSize).Take(pageSize).Select(q => new ExamMarksSetting
				{
					ExamMarksSettingID = q.ExamMarksSettingID,
					SemesterID = q.SemesterID,
					InstituteAlias = q.Program.Institute.InstituteAlias,
					ProgramAlias = q.Program.ProgramAlias,
					DepartmentAlias = q.Program.Department.DepartmentAlias,
					ExamMarksType = q.ExamMarksTypes,
					FromDate = q.FromDate,
					ToDate = q.ToDate,
					Status = q.Status
				}).ToList();
			}
		}
	}
}