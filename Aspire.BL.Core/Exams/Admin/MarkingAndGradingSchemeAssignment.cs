﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Exams.Admin
{
	public static class ManageMarkingAndGradingSchemeAssignment
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int instituteID, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID, AdminPermissions.Exams.ManageMarkingAndGradingSchemeAssignment, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class GetAdmissionOpenProgramsResult
		{
			public sealed class AdmissionOpenProgram
			{
				internal AdmissionOpenProgram() { }
				public int AdmissionOpenProgramID { get; internal set; }
				public string ProgramAlias { get; internal set; }
				public short IntakeSemesterID { get; internal set; }
				public string IntakeSemester => this.IntakeSemesterID.ToSemesterString();
				public int ExamMarkingSchemeID { get; internal set; }
				public string MarkingSchemeName { get; internal set; }
				public int ExamMarksGradingSchemeID { get; internal set; }
				public string GradingSchemeName { get; internal set; }
				public DegreeLevels DegreeLevelEnum { get; internal set; }
			}

			internal GetAdmissionOpenProgramsResult()
			{
			}
			public List<AdmissionOpenProgram> AdmissionOpenPrograms { get; internal set; }
			public List<CustomListItem> ExamMarksGradingSchemes { get; internal set; }
			public List<CustomListItem> ExamMarkingSchemes { get; internal set; }
			public int VirtualItemCount { get; internal set; }
		}

		public static GetAdmissionOpenProgramsResult GetAdmissionOpenPrograms(int instituteID, int? departmentID, DegreeLevels? degreeLevelEnum, int? programID, short? intakeSemesterID, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				var admissionOpenPrograms = aspireContext.GetAdminInstitutes(loginHistory).Where(i => i.InstituteID == instituteID)
					.SelectMany(i => i.Departments).Where(d => departmentID == null || d.DepartmentID == departmentID.Value)
					.SelectMany(d => d.Programs)
					.Where(p => (byte?)degreeLevelEnum == null || p.DegreeLevel == (byte)degreeLevelEnum.Value)
					.Where(p => programID == null || p.ProgramID == programID.Value)
					.SelectMany(p => p.AdmissionOpenPrograms).Where(aop => intakeSemesterID == null || aop.SemesterID == intakeSemesterID.Value);

				var result = new GetAdmissionOpenProgramsResult
				{
					VirtualItemCount = admissionOpenPrograms.Count(),
				};

				switch (sortExpression)
				{
					case nameof(GetAdmissionOpenProgramsResult.AdmissionOpenProgram.ProgramAlias):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.Program.ProgramAlias);
						break;
					case nameof(GetAdmissionOpenProgramsResult.AdmissionOpenProgram.DegreeLevelEnum):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.Program.DegreeLevel);
						break;
					case nameof(GetAdmissionOpenProgramsResult.AdmissionOpenProgram.GradingSchemeName):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.ExamMarksGradingScheme.GradingSchemeName);
						break;
					case nameof(GetAdmissionOpenProgramsResult.AdmissionOpenProgram.MarkingSchemeName):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.ExamMarkingScheme.MarkingSchemeName);
						break;
					case nameof(GetAdmissionOpenProgramsResult.AdmissionOpenProgram.IntakeSemester):
					case nameof(GetAdmissionOpenProgramsResult.AdmissionOpenProgram.IntakeSemesterID):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.SemesterID);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				result.AdmissionOpenPrograms = admissionOpenPrograms.Select(aop => new GetAdmissionOpenProgramsResult.AdmissionOpenProgram
				{
					AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
					ProgramAlias = aop.Program.ProgramAlias,
					DegreeLevelEnum = (DegreeLevels)aop.Program.DegreeLevel,
					IntakeSemesterID = aop.SemesterID,
					ExamMarkingSchemeID = aop.ExamMarkingSchemeID,
					MarkingSchemeName = aop.ExamMarkingScheme.MarkingSchemeName,
					ExamMarksGradingSchemeID = aop.ExamMarksGradingSchemeID,
					GradingSchemeName = aop.ExamMarksGradingScheme.GradingSchemeName
				}).ToList();

				if (result.AdmissionOpenPrograms.Any())
				{
					result.ExamMarkingSchemes = aspireContext.ExamMarkingSchemes.Where(ems => ems.InstituteID == instituteID).Select(ems => new CustomListItem
					{
						ID = ems.ExamMarkingSchemeID,
						Text = ems.MarkingSchemeName,
					}).OrderBy(i => i.Text).ToList();
					result.ExamMarksGradingSchemes = aspireContext.ExamMarksGradingSchemes.Where(emgs => emgs.InstituteID == instituteID).Select(emgs => new CustomListItem
					{
						ID = emgs.ExamMarksGradingSchemeID,
						Text = emgs.GradingSchemeName,
					}).OrderBy(i => i.Text).ToList();
				}
				return result;
			}
		}

		public sealed class AdmissionOpenProgram
		{
			public int AdmissionOpenProgramID { get; set; }
			public int OldExamMarkingSchemeID { get; set; }
			public int OldExamMarksGradingSchemeID { get; set; }
			public int NewExamMarkingSchemeID { get; set; }
			public int NewExamMarksGradingSchemeID { get; set; }
		}

		public static uint UpdateMarksAndGradingSchemes(List<AdmissionOpenProgram> admissionOpenPrograms, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				uint updated = 0;
				foreach (var admissionOpenProgram in admissionOpenPrograms)
				{
					var dbRecord = aspireContext.AdmissionOpenPrograms
						.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgram.AdmissionOpenProgramID)
						.Where(aop => aop.ExamMarkingSchemeID == admissionOpenProgram.OldExamMarkingSchemeID)
						.Where(aop => aop.ExamMarksGradingSchemeID == admissionOpenProgram.OldExamMarksGradingSchemeID)
						.Select(aop => new
						{
							AdmissionOpenProgram = aop,
							aop.Program.InstituteID
						}).SingleOrDefault();
					if (dbRecord == null)
						continue;
					var loginHistory = aspireContext.DemandPermissions(dbRecord.InstituteID, loginSessionGuid);
					dbRecord.AdmissionOpenProgram.ExamMarkingSchemeID = admissionOpenProgram.NewExamMarkingSchemeID;
					dbRecord.AdmissionOpenProgram.ExamMarksGradingSchemeID = admissionOpenProgram.NewExamMarksGradingSchemeID;
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
					updated++;
				}
				if (updated > 0)
					aspireContext.CommitTransaction();
				return updated;
			}
		}
	}
}
