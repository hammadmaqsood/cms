﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Exams.Admin
{
	public static class ExamRemarksPolicy
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Exams.ManageExamMarksPolicies, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.Exams.ManageExamMarksPolicies, UserGroupPermission.PermissionValues.Allowed);
		}

		public static List<Model.Entities.ExamRemarksPolicy> GetExamRemarksPolicies(int? instituteID, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				var examRemarksPolicies = aspireContext.GetAdminInstitutes(loginHistory)
					.Where(i => instituteID == null || i.InstituteID == instituteID.Value)
					.SelectMany(i => i.ExamRemarksPolicies);
				virtualItemCount = examRemarksPolicies.Count();
				switch (sortExpression)
				{
					case nameof(Model.Entities.ExamRemarksPolicy.ExamRemarksPolicyName):
						examRemarksPolicies = examRemarksPolicies.OrderBy(sortDirection, erp => erp.ExamRemarksPolicyName);
						break;
					case nameof(Model.Entities.ExamRemarksPolicy.ExamRemarksPolicyDescription):
						examRemarksPolicies = examRemarksPolicies.OrderBy(sortDirection, erp => erp.ExamRemarksPolicyDescription);
						break;
					case nameof(Institute.InstituteAlias):
						examRemarksPolicies = examRemarksPolicies.OrderBy(sortDirection, erp => erp.Institute.InstituteAlias);
						break;
					case nameof(Model.Entities.ExamRemarksPolicy.ValidFromSemesterID):
						examRemarksPolicies = examRemarksPolicies.OrderBy(sortDirection, erp => erp.ValidFromSemesterID).ThenBy(sortDirection, erp => erp.ValidUpToSemesterID);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return examRemarksPolicies.Include(erp => erp.Institute).Include(erp => erp.ExamRemarksPolicyDetails).Skip(pageIndex * pageSize).Take(pageSize).ToList();
			}
		}

		public static Model.Entities.ExamRemarksPolicy GetExamRemarksPolicy(int examRemarksPolicyID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.ExamRemarksPolicies)
					.Include(emp => emp.Institute)
					.Include(emp => emp.ExamRemarksPolicyDetails)
					.SingleOrDefault(erp => erp.ExamRemarksPolicyID == examRemarksPolicyID);
			}
		}

		public enum AddExamRemarksPolicyStatuses
		{
			NameAlreadyExists,
			Success,
		}

		public static AddExamRemarksPolicyStatuses AddExamRemarksPolicy(Model.Entities.ExamRemarksPolicy examRemarksPolicy, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(examRemarksPolicy.InstituteID, loginSessionGuid);
				examRemarksPolicy.ExamRemarksPolicyName = examRemarksPolicy.ExamRemarksPolicyName.TrimAndCannotBeEmpty();
				examRemarksPolicy.ExamRemarksPolicyDescription = examRemarksPolicy.ExamRemarksPolicyDescription.TrimAndMakeItNullIfEmpty();
				var alreadyExists = aspireContext.ExamRemarksPolicies.Any(erp => erp.InstituteID == examRemarksPolicy.InstituteID && erp.ExamRemarksPolicyName == examRemarksPolicy.ExamRemarksPolicyName);
				if (alreadyExists)
					return AddExamRemarksPolicyStatuses.NameAlreadyExists;
				aspireContext.ExamRemarksPolicies.Add(examRemarksPolicy);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddExamRemarksPolicyStatuses.Success;
			}
		}

		public enum UpdateExamRemarksPolicyStatuses
		{
			NoRecordFound,
			NameAlreadyExists,
			Success,
		}

		public static UpdateExamRemarksPolicyStatuses UpdateExamRemarksPolicy(Model.Entities.ExamRemarksPolicy examRemarksPolicy, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var dbExamRemarksPolicy = aspireContext.ExamRemarksPolicies.SingleOrDefault(erp => erp.ExamRemarksPolicyID == examRemarksPolicy.ExamRemarksPolicyID);
				if (dbExamRemarksPolicy == null)
					return UpdateExamRemarksPolicyStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(dbExamRemarksPolicy.InstituteID, loginSessionGuid);
				dbExamRemarksPolicy.ExamRemarksPolicyName = examRemarksPolicy.ExamRemarksPolicyName.TrimAndCannotBeEmpty();
				dbExamRemarksPolicy.ExamRemarksPolicyDescription = examRemarksPolicy.ExamRemarksPolicyDescription.TrimAndMakeItNullIfEmpty();
				dbExamRemarksPolicy.ValidFromSemesterID = examRemarksPolicy.ValidFromSemesterID;
				dbExamRemarksPolicy.ValidUpToSemesterID = examRemarksPolicy.ValidUpToSemesterID;
				var alreadyExists = aspireContext.ExamRemarksPolicies
					.Where(erp => erp.ExamRemarksPolicyID != examRemarksPolicy.ExamRemarksPolicyID)
					.Any(erp => erp.InstituteID == examRemarksPolicy.InstituteID && erp.ExamRemarksPolicyName == examRemarksPolicy.ExamRemarksPolicyName);
				if (alreadyExists)
					return UpdateExamRemarksPolicyStatuses.NameAlreadyExists;

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateExamRemarksPolicyStatuses.Success;
			}
		}

		public enum DeleteExamRemarksPolicyStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteExamRemarksPolicyStatuses DeleteExamRemarksPolicy(int examRemarksPolicyID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var examRemarksPolicy = aspireContext.ExamRemarksPolicies.Include(erp => erp.ExamRemarksPolicyDetails).SingleOrDefault(erp => erp.ExamRemarksPolicyID == examRemarksPolicyID);
				if (examRemarksPolicy == null)
					return DeleteExamRemarksPolicyStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(examRemarksPolicy.InstituteID, loginSessionGuid);
				if (examRemarksPolicy.Programs.Any())
					return DeleteExamRemarksPolicyStatuses.ChildRecordExists;
				if (examRemarksPolicy.AdmissionOpenPrograms.Any())
					return DeleteExamRemarksPolicyStatuses.ChildRecordExists;
				aspireContext.ExamRemarksPolicyDetails.RemoveRange(examRemarksPolicy.ExamRemarksPolicyDetails);
				aspireContext.ExamRemarksPolicies.Remove(examRemarksPolicy);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteExamRemarksPolicyStatuses.Success;
			}
		}

		private static void Order(this IEnumerable<ExamRemarksPolicyDetail> examRemarksPolicyDetails)
		{
			byte sequence = 0;
			foreach (var examRemarksPolicyDetail in examRemarksPolicyDetails.OrderByDescending(erpd => erpd.FirstSemester).ThenBy(erpd => erpd.Sequence))
			{
				if (examRemarksPolicyDetail.FirstSemester)
					examRemarksPolicyDetail.Sequence = 1;
				else
					examRemarksPolicyDetail.Sequence = ++sequence;
			}
		}

		public enum AddExamRemarksPolicyDetailStatuses
		{
			NoRecordFound,
			FirstSemesterRecordAlreadyExists,
			RemarksAlreadyExists,
			Success
		}

		public static AddExamRemarksPolicyDetailStatuses AddExamRemarksPolicyDetail(int examRemarksPolicyID, ExamRemarksTypes examRemarksTypeEnum, decimal lessThanCGPA, bool firstSemester, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var examRemarksPolicy = aspireContext.ExamRemarksPolicies.Include(erp => erp.ExamRemarksPolicyDetails).SingleOrDefault(erp => erp.ExamRemarksPolicyID == examRemarksPolicyID);
				if (examRemarksPolicy == null)
					return AddExamRemarksPolicyDetailStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(examRemarksPolicy.InstituteID, loginSessionGuid);

				if (firstSemester)
				{
					if (examRemarksPolicy.ExamRemarksPolicyDetails.Any(erp => erp.FirstSemester))
						return AddExamRemarksPolicyDetailStatuses.FirstSemesterRecordAlreadyExists;
				}
				else
				{
					if (examRemarksPolicy.ExamRemarksPolicyDetails.Any(erp => erp.FirstSemester == false && erp.ExamRemarksTypeEnum == examRemarksTypeEnum))
						return AddExamRemarksPolicyDetailStatuses.RemarksAlreadyExists;
				}
				examRemarksPolicy.ExamRemarksPolicyDetails.Add(new ExamRemarksPolicyDetail
				{
					ExamRemarksPolicyID = examRemarksPolicyID,
					ExamRemarksTypeEnum = examRemarksTypeEnum,
					FirstSemester = firstSemester,
					LessThanCGPA = lessThanCGPA,
					Sequence = byte.MaxValue
				});
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ExamRemarksPolicyDetails.Where(erpd => erpd.ExamRemarksPolicyID == examRemarksPolicy.ExamRemarksPolicyID).ToList().Order();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddExamRemarksPolicyDetailStatuses.Success;
			}
		}

		public enum DeleteExamRemarksPolicyDetailStatuses
		{
			NoRecordFound,
			Success
		}

		public static DeleteExamRemarksPolicyDetailStatuses DeleteExamRemarksPolicyDetail(int examRemarksPolicyDetailID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var examRemarksPolicyDetail = aspireContext.ExamRemarksPolicyDetails.Include(erpd => erpd.ExamRemarksPolicy).SingleOrDefault(erpd => erpd.ExamRemarksPolicyDetailID == examRemarksPolicyDetailID);
				if (examRemarksPolicyDetail == null)
					return DeleteExamRemarksPolicyDetailStatuses.NoRecordFound;
				var examRemarksPolicy = examRemarksPolicyDetail.ExamRemarksPolicy;
				var loginHistory = aspireContext.DemandPermissions(examRemarksPolicy.InstituteID, loginSessionGuid);
				aspireContext.ExamRemarksPolicyDetails.Remove(examRemarksPolicyDetail);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				var examRemarksPolicyDetails = aspireContext.ExamRemarksPolicyDetails.Where(erpd => erpd.ExamRemarksPolicyID == examRemarksPolicy.ExamRemarksPolicyID);
				examRemarksPolicyDetails.Order();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteExamRemarksPolicyDetailStatuses.Success;
			}
		}
	}
}