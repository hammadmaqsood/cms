﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Exams.Admin
{
	public static class ExamMarkingSchemes
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Exams.ManageExamMarkingSchemes, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.Exams.ManageExamMarkingSchemes, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class GetExamMarkingSchemesResult
		{
			public sealed class Scheme
			{
				internal Scheme() { }
				public int ExamMarkingSchemeID { get; internal set; }
				public string InstituteAlias { get; internal set; }
				public string MarkingSchemeName { get; internal set; }
				public ExamMarkingScheme.MarkingSchemeTypes MarkingSchemeTypeEnum { get; internal set; }
				public string MarkingSchemeTypeFullName => this.MarkingSchemeTypeEnum.ToFullName();
			}

			internal GetExamMarkingSchemesResult()
			{
			}

			public List<Scheme> Schemes { get; internal set; }
			public int VirtualItemCount { get; internal set; }
		}

		public static GetExamMarkingSchemesResult GetExamMarkingSchemes(int? instituteID, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				var query = aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.ExamMarkingSchemes)
					.Where(i => instituteID == null || i.InstituteID == instituteID.Value);
				var result = new GetExamMarkingSchemesResult
				{
					VirtualItemCount = query.Count(),
				};
				switch (sortExpression)
				{
					case nameof(ExamMarkingScheme.MarkingSchemeName):
						query = query.OrderBy(sortDirection, ems => ems.MarkingSchemeName);
						break;
					case nameof(Institute.InstituteAlias):
						query = query.OrderBy(sortDirection, ems => ems.Institute.InstituteAlias);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				result.Schemes = query.Select(ems => new GetExamMarkingSchemesResult.Scheme
				{
					ExamMarkingSchemeID = ems.ExamMarkingSchemeID,
					MarkingSchemeName = ems.MarkingSchemeName,
					InstituteAlias = ems.Institute.InstituteAlias,
					//MarkingSchemeTypeEnum = (ExamMarkingScheme.MarkingSchemeTypes)ems.MarkingSchemeType,
				}).ToList();
				return result;
			}
		}

		public enum DeleteExamMarkingSchemeStatuses
		{
			NoRecordFound,
			Success,
			ChildRecordExistsAdmissionOpenPrograms,
			ChildRecordExistsExamMarkingSchemeDetails,
			ChildRecordExistsPrograms
		}

		public static DeleteExamMarkingSchemeStatuses DeleteExamMarkingScheme(int examMarkingSchemeID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var examMarkingScheme = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.ExamMarkingSchemes).SingleOrDefault(ems => ems.ExamMarkingSchemeID == examMarkingSchemeID);
				if (examMarkingScheme == null)
					return DeleteExamMarkingSchemeStatuses.NoRecordFound;
				if (examMarkingScheme.AdmissionOpenPrograms.Any())
					return DeleteExamMarkingSchemeStatuses.ChildRecordExistsAdmissionOpenPrograms;
				if (examMarkingScheme.ExamMarkingSchemeDetails.Any())
					return DeleteExamMarkingSchemeStatuses.ChildRecordExistsExamMarkingSchemeDetails;
				if (examMarkingScheme.Programs.Any())
					return DeleteExamMarkingSchemeStatuses.ChildRecordExistsPrograms;
				aspireContext.ExamMarkingSchemes.Remove(examMarkingScheme);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteExamMarkingSchemeStatuses.Success;
			}
		}

		public static ExamMarkingScheme GetExamMarkingScheme(int examMarkingSchemeID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.ExamMarkingSchemes).Include(ems => ems.ExamMarkingSchemeDetails).SingleOrDefault(ems => ems.ExamMarkingSchemeID == examMarkingSchemeID);
			}
		}

		public sealed class AddExamMarkingSchemeResult
		{
			public enum Statuses
			{
				NameAlreadyExists,
				Success,
			}

			internal AddExamMarkingSchemeResult()
			{
			}
			public int? ExamMarkingSchemeID { get; internal set; }
			public Statuses Status { get; internal set; }
		}

		public static AddExamMarkingSchemeResult AddExamMarkingScheme(int instituteID, string markingSchemeName, ExamMarkingScheme.MarkingSchemeTypes markingSchemeTypeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				markingSchemeName = markingSchemeName.TrimAndCannotBeEmpty();
				if (aspireContext.ExamMarkingSchemes.Any(ems => ems.InstituteID == instituteID && ems.MarkingSchemeName == markingSchemeName))
					return new AddExamMarkingSchemeResult { Status = AddExamMarkingSchemeResult.Statuses.NameAlreadyExists };
				var examMarkingScheme = aspireContext.ExamMarkingSchemes.Add(new ExamMarkingScheme
				{
					InstituteID = instituteID,
					MarkingSchemeName = markingSchemeName,
					//MarkingSchemeTypeEnum = markingSchemeTypeEnum
				});
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return new AddExamMarkingSchemeResult
				{
					ExamMarkingSchemeID = examMarkingScheme.ExamMarkingSchemeID,
					Status = AddExamMarkingSchemeResult.Statuses.Success
				};
			}
		}

		public enum UpdateExamMarkingSchemeStatuses
		{
			NoRecordFound,
			NameAlreadyExists,
			Success,
		}

		public static UpdateExamMarkingSchemeStatuses UpdateExamMarkingScheme(int examMarkingSchemeID, string markingSchemeName, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var examMarkingScheme = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.ExamMarkingSchemes).SingleOrDefault(ems => ems.ExamMarkingSchemeID == examMarkingSchemeID);
				if (examMarkingScheme == null)
					return UpdateExamMarkingSchemeStatuses.NoRecordFound;
				markingSchemeName = markingSchemeName.TrimAndCannotBeEmpty();
				if (aspireContext.ExamMarkingSchemes.Any(ems => ems.ExamMarkingSchemeID != examMarkingScheme.ExamMarkingSchemeID && ems.InstituteID == examMarkingScheme.InstituteID && ems.MarkingSchemeName == markingSchemeName))
					return UpdateExamMarkingSchemeStatuses.NameAlreadyExists;
				examMarkingScheme.MarkingSchemeName = markingSchemeName;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateExamMarkingSchemeStatuses.Success;
			}
		}

		public enum AddExamMarkingSchemeDetailStatuses
		{
			AlreadyExists,
			NoRecordFound,
			Success,
		}

		public static AddExamMarkingSchemeDetailStatuses AddExamMarkingSchemeDetail(int examMarkingSchemeID, CourseCategories courseCategoryEnum, ExamMarksTypes examMarksTypeEnum, decimal minMarks, decimal maxMarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var examMarkingScheme = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.ExamMarkingSchemes).SingleOrDefault(ems => ems.ExamMarkingSchemeID == examMarkingSchemeID);
				if (examMarkingScheme == null)
					return AddExamMarkingSchemeDetailStatuses.NoRecordFound;
				var courseCategory = (byte)courseCategoryEnum;
				var examMarksType = (byte)examMarksTypeEnum;
				if (examMarkingScheme.ExamMarkingSchemeDetails.Any(emsd => emsd.CourseCategory == courseCategory && emsd.ExamMarksType == examMarksType))
					return AddExamMarkingSchemeDetailStatuses.AlreadyExists;
				aspireContext.ExamMarkingSchemeDetails.Add(new ExamMarkingSchemeDetail
				{
					ExamMarkingSchemeID = examMarkingSchemeID,
					CourseCategoryEnum = courseCategoryEnum,
					ExamMarksTypeEnum = examMarksTypeEnum,
					MinMarks = minMarks,
					MaxMarks = maxMarks,
				});
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddExamMarkingSchemeDetailStatuses.Success;
			}
		}

		public enum UpdateExamMarkingSchemeDetailStatuses
		{
			NoRecordFound,
			Success,
		}

		public static UpdateExamMarkingSchemeDetailStatuses UpdateExamMarkingSchemeDetail(int examMarkingSchemeDetailID, decimal minMarks, decimal maxMarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var examMarkingSchemeDetail = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.ExamMarkingSchemes)
					.SelectMany(ems => ems.ExamMarkingSchemeDetails).SingleOrDefault(ems => ems.ExamMarkingSchemeDetailID == examMarkingSchemeDetailID);
				if (examMarkingSchemeDetail == null)
					return UpdateExamMarkingSchemeDetailStatuses.NoRecordFound;
				examMarkingSchemeDetail.MinMarks = minMarks;
				examMarkingSchemeDetail.MaxMarks = maxMarks;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateExamMarkingSchemeDetailStatuses.Success;
			}
		}

		public enum DeleteExamMarkingSchemeDetailStatuses
		{
			NoRecordFound,
			Success,
		}

		public static DeleteExamMarkingSchemeDetailStatuses DeleteExamMarkingSchemeDetail(int examMarkingSchemeDetailID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var examMarkingSchemeDetail = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.ExamMarkingSchemes)
					.SelectMany(ems => ems.ExamMarkingSchemeDetails).SingleOrDefault(ems => ems.ExamMarkingSchemeDetailID == examMarkingSchemeDetailID);
				if (examMarkingSchemeDetail == null)
					return DeleteExamMarkingSchemeDetailStatuses.NoRecordFound;
				aspireContext.ExamMarkingSchemeDetails.Remove(examMarkingSchemeDetail);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteExamMarkingSchemeDetailStatuses.Success;
			}
		}

		public static ExamMarkingSchemeDetail GetExamMarkingSchemeDetail(int examMarkingSchemeDetailID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.ExamMarkingSchemes)
					.SelectMany(ems => ems.ExamMarkingSchemeDetails).SingleOrDefault(ems => ems.ExamMarkingSchemeDetailID == examMarkingSchemeDetailID);
			}
		}
	}
}
