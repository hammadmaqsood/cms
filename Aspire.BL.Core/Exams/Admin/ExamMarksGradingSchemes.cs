﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.Exams.Admin
{
	public static class ExamMarksGradingSchemes
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.Exams.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.Exams.ManageExamMarksGradingSchemes, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.Exams.ManageExamMarksGradingSchemes, UserGroupPermission.PermissionValues.Allowed);
		}

		public static List<ExamMarksGradingScheme> GetExamMarksGradingSchemes(int? instituteID, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				var query = aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.ExamMarksGradingSchemes).Include(e => e.Institute)
					.Where(emgs => instituteID == null || emgs.InstituteID == instituteID.Value);
				virtualItemCount = query.Count();
				switch (sortExpression)
				{
					case nameof(ExamMarksGradingScheme.GradingSchemeName):
						query = query.OrderBy(sortDirection, q => q.GradingSchemeName);
						break;
					case nameof(Institute.InstituteAlias):
						query = query.OrderBy(sortDirection, q => q.Institute.InstituteAlias);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return query.Skip(pageIndex * pageSize).Take(pageSize).ToList();
			}
		}

		public static ExamMarksGradingScheme GetExamMarksGradingScheme(int examMarksGradingSchemeID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.ExamMarksGradingSchemes).SingleOrDefault(emgs => emgs.ExamMarksGradingSchemeID == examMarksGradingSchemeID);
			}
		}

		public static ExamMarksGradingScheme AddExamMarksGradingScheme(int instituteID, string gradingSchemeName, out bool alreadyExists, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				gradingSchemeName = gradingSchemeName.Trim();
				alreadyExists = aspireContext.ExamMarksGradingSchemes.Any(e => e.InstituteID == instituteID && e.GradingSchemeName == gradingSchemeName);
				if (alreadyExists)
					return null;
				var examMarksGradingScheme = new ExamMarksGradingScheme
				{
					InstituteID = instituteID,
					GradingSchemeName = gradingSchemeName,
				};
				aspireContext.ExamMarksGradingSchemes.Add(examMarksGradingScheme);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return examMarksGradingScheme;
			}
		}

		public static ExamMarksGradingScheme UpdateExamMarksGradingScheme(int examMarksGradingSchemeID, string gradingSchemeName, out bool alreadyExists, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				gradingSchemeName = gradingSchemeName.Trim();
				var examMarksGradingScheme = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.ExamMarksGradingSchemes).SingleOrDefault(e => e.ExamMarksGradingSchemeID == examMarksGradingSchemeID);
				if (examMarksGradingScheme == null)
				{
					alreadyExists = false;
					return null;
				}
				alreadyExists = aspireContext.ExamMarksGradingSchemes.Any(e => e.ExamMarksGradingSchemeID != examMarksGradingSchemeID && e.InstituteID == examMarksGradingScheme.InstituteID && e.GradingSchemeName == gradingSchemeName);
				if (alreadyExists)
					return null;
				examMarksGradingScheme.GradingSchemeName = gradingSchemeName;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return examMarksGradingScheme;
			}
		}

		public static bool DeleteExamMarksGradingScheme(int examMarksGradingSchemeID, out bool isChildExists, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var examMarksGradingScheme = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.ExamMarksGradingSchemes).SingleOrDefault(e => e.ExamMarksGradingSchemeID == examMarksGradingSchemeID);
				if (examMarksGradingScheme == null)
				{
					isChildExists = false;
					return false;
				}
				isChildExists = examMarksGradingScheme.ExamMarksGradingSchemeGradeRanges.Any();
				if (isChildExists)
					return false;
				isChildExists = examMarksGradingScheme.AdmissionOpenPrograms.Any();
				if (isChildExists)
					return false;
				isChildExists = examMarksGradingScheme.Programs.Any();
				if (isChildExists)
					return false;
				aspireContext.ExamMarksGradingSchemes.Remove(examMarksGradingScheme);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return true;
			}
		}

		public enum AddExamMarksGradingSchemeGradeRangesStatuses
		{
			NoRecordFound,
			AlreadyExists,
			Success
		}

		public static AddExamMarksGradingSchemeGradeRangesStatuses AddExamMarksGradingSchemeGradeRanges(int examMarksGradingSchemeID, bool regularSemester, ExamGrades gradeEnum, decimal minMarks, decimal maxMarks, decimal gradePoints, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var examMarksGradingScheme = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.ExamMarksGradingSchemes).SingleOrDefault(e => e.ExamMarksGradingSchemeID == examMarksGradingSchemeID);
				if (examMarksGradingScheme == null)
					return AddExamMarksGradingSchemeGradeRangesStatuses.NoRecordFound;
				var alreadyExists = examMarksGradingScheme.ExamMarksGradingSchemeGradeRanges.Any(e => e.RegularSemester == regularSemester && e.Grade == (byte)gradeEnum && e.ExamMarksGradingSchemeID == examMarksGradingSchemeID);
				if (alreadyExists)
					return AddExamMarksGradingSchemeGradeRangesStatuses.AlreadyExists;
				var examMarksGradingSchemeGradeRange = new ExamMarksGradingSchemeGradeRanx
				{
					RegularSemester = regularSemester,
					GradeEnum = gradeEnum,
					MinMarks = minMarks,
					MaxMarks = maxMarks,
					GradePoints = gradePoints,
				};
				examMarksGradingScheme.ExamMarksGradingSchemeGradeRanges.Add(examMarksGradingSchemeGradeRange);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddExamMarksGradingSchemeGradeRangesStatuses.Success;
			}
		}

		public enum DeleteExamMarksGradingSchemesGradeRangeStatuses
		{
			NoRecordFound,
			Success
		}

		public static DeleteExamMarksGradingSchemesGradeRangeStatuses DeleteExamMarksGradingSchemesGradeRange(int examMarksGradingSchemesGradeRangeID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var examMarksGradingSchemesGradeRange = aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.ExamMarksGradingSchemes)
					.SelectMany(emgs => emgs.ExamMarksGradingSchemeGradeRanges)
					.SingleOrDefault(e => e.ExamMarksGradingSchemesGradeRangeID == examMarksGradingSchemesGradeRangeID);
				if (examMarksGradingSchemesGradeRange == null)
					return DeleteExamMarksGradingSchemesGradeRangeStatuses.NoRecordFound;
				aspireContext.ExamMarksGradingSchemeGradeRanges.Remove(examMarksGradingSchemesGradeRange);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteExamMarksGradingSchemesGradeRangeStatuses.Success;
			}
		}

		public static List<ExamMarksGradingSchemeGradeRanx> GetExamMarksGradingSchemeGradeRanges(int? examMarksGradingSchemeID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.ExamMarksGradingSchemes)
					.SelectMany(emgs => emgs.ExamMarksGradingSchemeGradeRanges)
					.Where(e => e.ExamMarksGradingSchemeID == examMarksGradingSchemeID)
					.OrderByDescending(emgsr => emgsr.RegularSemester)
					.ThenByDescending(emgsr => emgsr.MinMarks)
					.ToList();
			}
		}
	}
}
