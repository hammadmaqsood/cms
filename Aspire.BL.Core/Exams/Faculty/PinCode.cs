﻿using Aspire.BL.Core.Common.EmailsManagement;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Linq;

namespace Aspire.BL.Core.Exams.Faculty
{
	public static class PinCode
	{
		public static string GeneratePinCode(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandFacultyActiveSession(loginSessionGuid);
				var facultyMember = aspireContext.FacultyMembers.Where(fm => fm.FacultyMemberID == loginHistory.FacultyMemberID)
					.Select(fm => new
					{
						fm.Name,
						fm.Email
					})
					.Single();
				if (string.IsNullOrWhiteSpace(facultyMember.Email))
					return null;

				var createdDate = DateTime.Now;
				var expiryDate = createdDate.AddMinutes(5);
				var facultyMemberPinCode = aspireContext.FacultyMemberPinCodes.Add(new FacultyMemberPinCode
				{
					ExpiryDate = expiryDate,
					FacultyMemberID = loginHistory.FacultyMemberID,
					PinCode = Guid.NewGuid(),
					UserLoginHistoryID = null,
				});
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);

				var subject = "Pin Code For Exam Marks Entry";
				var emailBody = Properties.Resources.FacultyMemberPinCode;
				emailBody = emailBody.Replace("{NameHtmlEncoded}", facultyMember.Name.HtmlEncode());
				emailBody = emailBody.Replace("{PinCodeHtmlEncoded}", facultyMemberPinCode.PinCode.ToString("N").Substring(0, 8).ToLower());
				emailBody = emailBody.Replace("{ExpiryDateHtmlEncoded}", facultyMemberPinCode.ExpiryDate.ToString("F"));
				aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.FacultyMemberPinCode, subject, emailBody, true, Email.Priorities.Highest, createdDate, expiryDate, loginHistory.UserLoginHistoryID, toList: new[] { (facultyMember.Name, facultyMember.Email) });
				aspireContext.CommitTransaction();
				return facultyMember.Email;
			}
		}

		public static bool VerifyPinCode(Guid loginSessionGuid, string pinCode)
		{
			if (pinCode.Length != 8)
				throw new ArgumentOutOfRangeException(nameof(pinCode), pinCode, null);
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandFacultyActiveSession(loginSessionGuid);
				var facultyMemberPinCode = aspireContext.FacultyMemberPinCodes
					.Where(fm => fm.UserLoginHistoryID == null && fm.ExpiryDate >= DateTime.Now)
					.FirstOrDefault(fm => fm.FacultyMemberID == loginHistory.FacultyMemberID && fm.PinCode.ToString().StartsWith(pinCode));
				if (facultyMemberPinCode == null)
					return false;
				facultyMemberPinCode.UserLoginHistoryID = loginHistory.UserLoginHistoryID;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return true;
			}
		}
	}
}
