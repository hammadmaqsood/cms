﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FeeManagement.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.FeeManagement.Student
{
	public static class StudentFees
	{
		private static Core.Common.Permissions.Student.StudentLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid, SubUserTypes? subUserTypeEnum)
		{
			return aspireContext.DemandStudentActiveSession(loginSessionGuid, subUserTypeEnum);
		}

		public static (List<StudentFeeChallan> studentFeeChallans, List<FeeDefaulters.FeeDefaulterReason> reasons) GetStudentFeeChallans(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid, null);
				var reasonsList = new List<FeeDefaulters.FeeDefaulterReason>();
				var student = aspireContext.Students.Where(s => s.StudentID == loginHistory.StudentID)
					.Select(s => new
					{
						s.AdmissionOpenProgram.Program.Institute.InstituteCode,
						s.AdmissionOpenProgram.Program.InstituteID,
						IntakeSemesterID = s.AdmissionOpenProgram.SemesterID
					}).Single();
				var feeDefaulterJobSemesters = aspireContext.FeeDefaulterJobSemesters
					.Where(fdjs => fdjs.SemesterID >= student.IntakeSemesterID && fdjs.InstituteID == student.InstituteID && fdjs.Status == (byte)FeeDefaulterJobSemester.Statuses.Active)
					.ToList();
				foreach (var feeDefaulterJobSemester in feeDefaulterJobSemesters)
				{
					var reasons = aspireContext.GetFeeDefaulterReasons(loginHistory.StudentID, student.InstituteCode, student.InstituteID, feeDefaulterJobSemester.SemesterID);
					if (reasons != null)
						reasonsList.AddRange(reasons);
				}

				var studentFeeChallans = aspireContext.StudentFeeChallans.Include(sfc => sfc.StudentFee).Include(sfc => sfc.AccountTransaction)
					.Where(sfc => sfc.StudentFee.StudentID == loginHistory.StudentID && sfc.StudentFee.FeeType == StudentFee.FeeTypeChallan)
					.OrderByDescending(sfc => sfc.StudentFee.SemesterID).ThenBy(sfc => sfc.StudentFeeID).ThenBy(sfc => sfc.StudentFeeChallanID)
					.ToList();
				return (studentFeeChallans, reasonsList);
			}
		}

		public sealed class AddStudentFeeResults
		{
			public enum Statuses
			{
				ContactAccountsOfficeArrearsFound,
				ContactAccountsOfficeArrearsHeadFoundInFeeStructure,
				ContactAccountsOfficeForeignerStudent,
				ContactAccountsOfficeLastSemesterFeeNotFound,
				ContactAccountsOfficeNoPreviousStudentFeeFound,
				ContactAccountsOfficeZeroCreditHourFound,
				ContactAccountsOfficeStudentStatusIssue,
				ContactAccountsOfficeStudentSemesterStatusIssue,
				CoursesNotRegistered,
				FeeStructureDetailsNotFound,
				FeeStructureDetailsTutionFeeNotFound,
				StudentFeeAlreadyGenerated,
				FeeDueDateNotFound,
				FeeDueDatePassed,
				Success,
			}

			internal AddStudentFeeResults() { }
			public List<int> StudentFeeIDs { get; internal set; }
			public Statuses Status { get; internal set; }
		}

		private static readonly object AddStudentFeeLock = new object();
		public static AddStudentFeeResults AddStudentFee(short offeredSemesterID, Guid loginSessionGuid)
		{
			lock (AddStudentFeeLock)
			{
				using (var aspireContext = new AspireContext(true))
				{
					var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid, SubUserTypes.None);

					var student = aspireContext.Students
						.Where(s => s.StudentID == loginHistory.StudentID)
						.Select(s => new
						{
							s.StudentID,
							s.AdmissionOpenProgram.IsSummerRegularSemester,
							s.AdmissionOpenProgramID,
							s.AdmissionOpenProgram.Program.InstituteID,
							s.Category,
							s.Nationality,
							s.Status,
							StudentSemester = s.StudentSemesters.Where(ss => ss.SemesterID == offeredSemesterID).Select(ss => new
							{
								ss.SemesterNo,
								ss.Status,
								ss.FreezedStatus,
							}).FirstOrDefault(),
							FeeDueDate = s.AdmissionOpenProgram.Program.FeeDueDates.FirstOrDefault(fdd => fdd.SemesterID == offeredSemesterID),
						}).Single();

					if (!string.Equals(student.Nationality, "Pakistani", StringComparison.CurrentCultureIgnoreCase))
						return new AddStudentFeeResults { Status = AddStudentFeeResults.Statuses.ContactAccountsOfficeForeignerStudent };

					switch ((Model.Entities.Student.Statuses?)student.Status)
					{
						case Model.Entities.Student.Statuses.AdmissionCancelled:
						case Model.Entities.Student.Statuses.Blocked:
						case Model.Entities.Student.Statuses.Deferred:
						case Model.Entities.Student.Statuses.Dropped:
						case Model.Entities.Student.Statuses.Expelled:
						case Model.Entities.Student.Statuses.Graduated:
						case Model.Entities.Student.Statuses.Left:
						case Model.Entities.Student.Statuses.ProgramChanged:
						case Model.Entities.Student.Statuses.Rusticated:
						case Model.Entities.Student.Statuses.TransferredToOtherCampus:
							return new AddStudentFeeResults { Status = AddStudentFeeResults.Statuses.ContactAccountsOfficeStudentStatusIssue };
						case null:
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}

					var registeredCourses = aspireContext.RegisteredCourses
						.Include(rc => rc.Cours)
						.Where(rc => rc.StudentID == student.StudentID && rc.DeletedDate == null && rc.OfferedCours.SemesterID == offeredSemesterID && rc.StudentFeeID == null)
						.ToList();
					if (!registeredCourses.Any())
						return new AddStudentFeeResults { Status = AddStudentFeeResults.Statuses.CoursesNotRegistered };

					switch ((StudentSemester.Statuses?)student.StudentSemester.Status)
					{
						case null:
						case StudentSemester.Statuses.NotRegistered:
							return new AddStudentFeeResults { Status = AddStudentFeeResults.Statuses.ContactAccountsOfficeStudentSemesterStatusIssue };
						case StudentSemester.Statuses.Registered:
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}

					switch ((FreezedStatuses?)student.StudentSemester.FreezedStatus)
					{
						case FreezedStatuses.FreezedWithFee:
						case FreezedStatuses.FreezedWithoutFee:
							return new AddStudentFeeResults { Status = AddStudentFeeResults.Statuses.ContactAccountsOfficeStudentSemesterStatusIssue };
						case null:
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}

					if (registeredCourses.Any(rc => rc.Cours.CreditHours == 0))
						return new AddStudentFeeResults { Status = AddStudentFeeResults.Statuses.ContactAccountsOfficeZeroCreditHourFound };

					var studentFees = aspireContext.StudentFees
						.Where(sf => sf.StudentID == student.StudentID && sf.SemesterID <= offeredSemesterID && sf.FeeType == StudentFee.FeeTypeChallan)
						.Select(sf => new
						{
							sf.SemesterID,
							sf.Status,
						})
						.ToList();
					if (!studentFees.Any())
						return new AddStudentFeeResults { Status = AddStudentFeeResults.Statuses.ContactAccountsOfficeNoPreviousStudentFeeFound };

					if (studentFees.Any(sf => sf.SemesterID == offeredSemesterID))
						return new AddStudentFeeResults { Status = AddStudentFeeResults.Statuses.StudentFeeAlreadyGenerated };

					var previousSemesterID = Semester.PreviousSemester(offeredSemesterID);
					var previousRegularSemesterID = Semester.PreviousRegularSemester(offeredSemesterID, student.IsSummerRegularSemester);

					var previousSemesterStudentFees = studentFees.FindAll(sf => sf.SemesterID == previousSemesterID);
					var previousRegularSemesterStudentFees = studentFees.FindAll(sf => sf.SemesterID == previousRegularSemesterID);

					if (previousSemesterID != previousRegularSemesterID && previousSemesterStudentFees.Any())
					{
						if (previousSemesterStudentFees.Any(sf => sf.Status != StudentFee.StatusPaid))
							return new AddStudentFeeResults { Status = AddStudentFeeResults.Statuses.ContactAccountsOfficeArrearsFound };
					}
					else
					{
						if (previousRegularSemesterStudentFees.Any())
						{
							if (previousRegularSemesterStudentFees.Any(sf => sf.Status != StudentFee.StatusPaid))
								return new AddStudentFeeResults { Status = AddStudentFeeResults.Statuses.ContactAccountsOfficeArrearsFound };
						}
						else
							return new AddStudentFeeResults { Status = AddStudentFeeResults.Statuses.ContactAccountsOfficeLastSemesterFeeNotFound };
					}

					var feeHeads = aspireContext.FeeHeads.Where(fh => fh.InstituteID == student.InstituteID).ToList();
					var feeStructureDetails = aspireContext.FeeStructureDetails
						.Where(fsd => fsd.FeeStructure.AdmissionOpenProgramID == student.AdmissionOpenProgramID)
						.Where(fsd => fsd.Category == null || fsd.Category == student.Category)
						.Where(fsd => fsd.SemesterNo == null || fsd.SemesterNo == student.StudentSemester.SemesterNo)
						.Where(fsd => fsd.FeeHead.Visible)
						.Where(fsd => fsd.Amount != 0)
						.Select(fsd => new
						{
							fsd.FeeHeadID,
							fsd.Amount,
							fsd.FeeHead.FeeHeadType,
							fsd.FeeHead.DisplayIndex,
							fsd.FeeHead.RuleType,
						}).ToList();

					if (!feeStructureDetails.Any())
						return new AddStudentFeeResults { Status = AddStudentFeeResults.Statuses.FeeStructureDetailsNotFound };
					if (feeStructureDetails.All(fsd => fsd.FeeHeadType != (byte)FeeHead.FeeHeadTypes.TutionFee))
						return new AddStudentFeeResults { Status = AddStudentFeeResults.Statuses.FeeStructureDetailsTutionFeeNotFound };

					if (student.FeeDueDate == null)
						return new AddStudentFeeResults { Status = AddStudentFeeResults.Statuses.FeeDueDateNotFound };
					if (student.FeeDueDate.DueDate < DateTime.Today)
						return new AddStudentFeeResults { Status = AddStudentFeeResults.Statuses.FeeDueDatePassed };

					var studentFee = aspireContext.StudentFees.Add(new StudentFee
					{
						StudentID = loginHistory.StudentID,
						CandidateAppliedProgramID = null,
						CreatedDate = DateTime.Now,
						NewAdmission = false,
						NoOfInstallmentsEnum = InstallmentNos.One,
						StatusEnum = StudentFee.Statuses.NotPaid,
						CreditHours = registeredCourses.Sum(rc => rc.Cours.CreditHours),
						Remarks = null,
						SemesterID = offeredSemesterID,
						FeeTypeEnum = StudentFee.FeeTypes.Challan,
						CreatedByUserLoginHistoryID = loginHistory.UserLoginHistoryID,
						TotalAmount = 0,
						ConcessionAmount = 0,
						GrandTotalAmount = 0,
						ExemptWHT = false
					});
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);

					foreach (var feeStructureDetail in feeStructureDetails)
					{
						var studentFeeDetail = new StudentFeeDetail
						{
							FeeHeadID = feeStructureDetail.FeeHeadID,
							ConcessionAmount = null,
						};
						studentFee.StudentFeeDetails.Add(studentFeeDetail);
						switch ((FeeHead.FeeHeadTypes)feeStructureDetail.FeeHeadType)
						{
							case FeeHead.FeeHeadTypes.Arrears:
								return new AddStudentFeeResults { Status = AddStudentFeeResults.Statuses.ContactAccountsOfficeArrearsHeadFoundInFeeStructure };
							case FeeHead.FeeHeadTypes.TutionFee:
								studentFeeDetail.TotalAmount = (int)Math.Ceiling(studentFee.CreditHours * feeStructureDetail.Amount);
								break;
							case FeeHead.FeeHeadTypes.Custom:
							case FeeHead.FeeHeadTypes.WithholdingTax:
								studentFeeDetail.TotalAmount = feeStructureDetail.Amount;
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}
						studentFeeDetail.GrandTotalAmount = (studentFeeDetail.TotalAmount) ?? (0 - studentFeeDetail.ConcessionAmount ?? 0);
						switch ((FeeHead.RuleTypes)feeStructureDetail.RuleType)
						{
							case FeeHead.RuleTypes.Credit:
								studentFee.TotalAmount = studentFee.TotalAmount + studentFeeDetail.TotalAmount.Value;
								break;
							case FeeHead.RuleTypes.Debit:
								studentFee.TotalAmount = studentFee.TotalAmount - studentFeeDetail.TotalAmount.Value;
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}
					}
					studentFee.GrandTotalAmount = studentFee.TotalAmount - studentFee.ConcessionAmount;
					studentFee.StudentFeeChallans.Add(new StudentFeeChallan
					{
						Amount = studentFee.GrandTotalAmount,
						InstallmentNoEnum = InstallmentNos.One,
						StatusEnum = StudentFeeChallan.Statuses.NotPaid,
						DueDate = student.FeeDueDate.DueDate,
						AccountTransactionID = null,
					});
					studentFee.ValidateForAdd(feeHeads);
					foreach (var registeredCourse in registeredCourses)
						registeredCourse.StudentFeeID = studentFee.StudentFeeID;
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
					var studentFeeIDs = new List<int> { studentFee.StudentFeeID };
					var studentFeeForTax = aspireContext.CalculateAndGenerateFeeChallanForWithholdingTax(studentFee.StudentFeeID, loginHistory.UserLoginHistoryID);
					if (studentFeeForTax != null)
						studentFeeIDs.Add(studentFeeForTax.StudentFeeID);
					aspireContext.ProcessIndividualFeeDefaulter(loginHistory.StudentID, loginHistory.UserLoginHistoryID);
					aspireContext.CommitTransaction();
					return new AddStudentFeeResults
					{
						StudentFeeIDs = studentFeeIDs,
						Status = AddStudentFeeResults.Statuses.Success
					};
				}
			}
		}
	}
}
