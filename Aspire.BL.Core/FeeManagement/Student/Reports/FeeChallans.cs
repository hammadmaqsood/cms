﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FeeManagement.Common.Reports;
using Aspire.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.FeeManagement.Student.Reports
{
	public static class FeeChallans
	{
		public static List<Common.Reports.FeeChallans.FeeChallan> GetFeeChallans(List<int> studentFeeIDs, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStudentActiveSession(loginSessionGuid, null);
				studentFeeIDs = aspireContext.StudentFees
					.Where(sf => studentFeeIDs.Contains(sf.StudentFeeID) && sf.StudentID == loginHistory.StudentID)
					.Select(sf => sf.StudentFeeID)
					.ToList();
				return aspireContext.GetFeeChallans(studentFeeIDs);
			}
		}
	}
}
