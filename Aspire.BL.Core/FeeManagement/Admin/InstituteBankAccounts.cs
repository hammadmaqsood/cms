﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.FeeManagement.Admin
{
	public static class InstituteBankAccounts
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.FeeManagement.ManageBankAccounts, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.FeeManagement.ManageBankAccounts, UserGroupPermission.PermissionValues.Allowed);
		}

		private static bool AreInstituteBankAccountConsistentForAccountTypes(this AspireContext aspireContext)
		{
			return aspireContext.InstituteBankAccounts
					   .Where(iba => iba.Active)
					   .Where(iba => iba.AccountType == (byte)Model.Entities.InstituteBankAccount.AccountTypes.Online || iba.AccountType == (byte)Model.Entities.InstituteBankAccount.AccountTypes.ManualOnline)
					   .GroupBy(iba => new
					   {
						   iba.InstituteID,
						   iba.FeeType,
						   iba.BankID,
					   })
					   .Any(iba => iba.Count() > 1) == false;
		}

		public enum AddInstituteBankAccountsStatuses
		{
			AlreadyExists,
			Success,
			NotConsistent
		}

		public static AddInstituteBankAccountsStatuses AddInstituteBankAccount(int instituteID, Model.Entities.InstituteBankAccount.FeeTypes feeTypeEnum, int bankID, string accountTitle, string accountTitleForDisplay, string accountNo, string accountNoForDisplay, string branchCode, string branchName, string branchAddress, string branchForDisplay, Model.Entities.InstituteBankAccount.AccountTypes accountTypeEnum, bool active, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				if (aspireContext.InstituteBankAccounts.Any(iba => iba.InstituteID == instituteID && iba.BankID == bankID && iba.FeeType == (byte)feeTypeEnum && iba.AccountNo == accountNo))
					return AddInstituteBankAccountsStatuses.AlreadyExists;
				var instituteBankAccount = new Model.Entities.InstituteBankAccount
				{
					InstituteID = instituteID,
					AccountTitle = accountTitle,
					AccountNo = accountNo,
					FeeTypeEnum = feeTypeEnum,
					AccountTypeEnum = accountTypeEnum,
					Active = active,
					BranchForDisplay = branchForDisplay,
					AccountTitleForDisplay = accountTitleForDisplay,
					AccountNoForDisplay = accountNoForDisplay,
					BankID = bankID,
					BranchName = branchName,
					BranchAddress = branchAddress,
					BranchCode = branchCode,
				};
				aspireContext.InstituteBankAccounts.Add(instituteBankAccount.Validate());
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);

				if (aspireContext.AreInstituteBankAccountConsistentForAccountTypes())
				{
					aspireContext.CommitTransaction();
					return AddInstituteBankAccountsStatuses.Success;
				}

				aspireContext.RollbackTransaction();
				return AddInstituteBankAccountsStatuses.NotConsistent;
			}
		}

		public sealed class InstituteBankAccount
		{
			internal InstituteBankAccount()
			{
			}
			public int InstituteBankAccountID { get; internal set; }
			public byte FeeType { get; internal set; }
			public string FeeTypeFullName => ((Model.Entities.InstituteBankAccount.FeeTypes)this.FeeType).ToFullName();
			public string BankAlias { get; internal set; }
			public string AccountNo { get; internal set; }
			public string AccountNoForDisplay { get; internal set; }
			public string AccountTitleForDisplay { get; internal set; }
			public string BranchCode { get; internal set; }
			public string BranchName { get; internal set; }
			public string BranchAddress { get; internal set; }
			public string BranchForDisplay { get; internal set; }
			public bool Active { get; internal set; }
			public string ActiveYesNo => this.Active.ToYesNo();
			public byte AccountType { get; internal set; }
			public string AccountTypeFullName => ((Model.Entities.InstituteBankAccount.AccountTypes)this.AccountType).ToFullName();
			public string AccountTitle { get; internal set; }
			public string InstituteAlias { get; internal set; }
		}

		public static List<InstituteBankAccount> GetInstituteBankAccounts(int? instituteID, Model.Entities.InstituteBankAccount.FeeTypes? feeTypeEnum, Model.Entities.InstituteBankAccount.AccountTypes? accountTypeEnum, bool? active, string sortExpression, SortDirection sortDirection, int pageIndex, int pageSize, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				var query = aspireContext.InstituteBankAccounts
					.Where(q => instituteID == null || q.InstituteID == instituteID.Value)
					.Where(q => (byte?)feeTypeEnum == null || q.FeeType == (byte)feeTypeEnum.Value)
					.Where(q => (byte?)accountTypeEnum == null || q.AccountType == (byte)accountTypeEnum.Value)
					.Where(q => active == null || q.Active == active.Value);

				virtualItemCount = query.Count();
				switch (sortExpression)
				{
					case nameof(InstituteBankAccount.InstituteAlias):
						query = query.OrderBy(sortDirection, iba => iba.Institute.InstituteAlias);
						break;
					case nameof(InstituteBankAccount.FeeType):
					case nameof(InstituteBankAccount.FeeTypeFullName):
						query = query.OrderBy(sortDirection, iba => iba.FeeType);
						break;
					case nameof(InstituteBankAccount.BankAlias):
						query = query.OrderBy(sortDirection, iba => iba.Bank.BankAlias);
						break;
					case nameof(InstituteBankAccount.AccountNo):
						query = query.OrderBy(sortDirection, iba => iba.AccountNo);
						break;
					case nameof(InstituteBankAccount.AccountNoForDisplay):
						query = query.OrderBy(sortDirection, iba => iba.AccountNoForDisplay);
						break;
					case nameof(InstituteBankAccount.AccountTitle):
						query = query.OrderBy(sortDirection, iba => iba.AccountTitle);
						break;
					case nameof(InstituteBankAccount.AccountTitleForDisplay):
						query = query.OrderBy(sortDirection, iba => iba.AccountTitleForDisplay);
						break;
					case nameof(InstituteBankAccount.BranchCode):
						query = query.OrderBy(sortDirection, iba => iba.BranchCode);
						break;
					case nameof(InstituteBankAccount.BranchName):
						query = query.OrderBy(sortDirection, iba => iba.BranchName);
						break;
					case nameof(InstituteBankAccount.BranchAddress):
						query = query.OrderBy(sortDirection, iba => iba.BranchAddress);
						break;
					case nameof(InstituteBankAccount.BranchForDisplay):
						query = query.OrderBy(sortDirection, iba => iba.BranchForDisplay);
						break;
					case nameof(InstituteBankAccount.AccountType):
					case nameof(InstituteBankAccount.AccountTypeFullName):
						query = query.OrderBy(sortDirection, iba => iba.FeeType);
						break;
					case nameof(InstituteBankAccount.Active):
					case nameof(InstituteBankAccount.ActiveYesNo):
						query = query.OrderBy(sortDirection, iba => iba.Active);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}

				return query.Skip(pageIndex * pageSize).Take(pageSize).Select(iba => new InstituteBankAccount
				{
					InstituteBankAccountID = iba.InstituteBankAccountID,
					InstituteAlias = iba.Institute.InstituteAlias,
					FeeType = iba.FeeType,
					BankAlias = iba.Bank.BankAlias,
					AccountNo = iba.AccountNo,
					AccountNoForDisplay = iba.AccountNoForDisplay,
					AccountTitle = iba.AccountTitle,
					AccountTitleForDisplay = iba.AccountTitleForDisplay,
					BranchCode = iba.BranchCode,
					BranchName = iba.BranchName,
					BranchAddress = iba.BranchAddress,
					BranchForDisplay = iba.BranchForDisplay,
					AccountType = iba.AccountType,
					Active = iba.Active,
				}).ToList();
			}
		}

		public enum DeleteInstituteBankAccountStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteInstituteBankAccountStatuses DeleteInstituteBankAccount(int instituteBankAccountID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var instituteBankAccount = aspireContext.InstituteBankAccounts.SingleOrDefault(iba => iba.InstituteBankAccountID == instituteBankAccountID);
				if (instituteBankAccount == null)
					return DeleteInstituteBankAccountStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(instituteBankAccount.InstituteID, loginSessionGuid);
				if (aspireContext.AccountTransactions.Any(at => at.InstituteBankAccountID == instituteBankAccountID))
					return DeleteInstituteBankAccountStatuses.ChildRecordExists;
				aspireContext.InstituteBankAccounts.Remove(instituteBankAccount);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteInstituteBankAccountStatuses.Success;
			}
		}

		public static Model.Entities.InstituteBankAccount GetInstituteBankAccount(int instituteBankAccountID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var instituteBankAccount = aspireContext.InstituteBankAccounts.SingleOrDefault(iba => iba.InstituteBankAccountID == instituteBankAccountID);
				if (instituteBankAccount != null)
					aspireContext.DemandModulePermissions(instituteBankAccount.InstituteID, loginSessionGuid);
				return instituteBankAccount;
			}
		}

		public enum UpdateInstituteBankAccountStatuses
		{
			NoRecordFound,
			Success,
			NotConsistent
		}

		public static UpdateInstituteBankAccountStatuses UpdateInstituteBankAccount(int instituteBankAccountID, string accountTitle, string accountTitleForDisplay, string accountNo, string accountNoForDisplay, string branchCode, string branchName, string branchAddress, string branchForDisplay, Model.Entities.InstituteBankAccount.AccountTypes accountTypeEnum, bool active, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var instituteBankAccount = aspireContext.InstituteBankAccounts.SingleOrDefault(iba => iba.InstituteBankAccountID == instituteBankAccountID);
				if (instituteBankAccount == null)
					return UpdateInstituteBankAccountStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(instituteBankAccount.InstituteID, loginSessionGuid);
				instituteBankAccount.AccountNo = accountNo;
				instituteBankAccount.AccountNoForDisplay = accountNoForDisplay;
				instituteBankAccount.AccountTitle = accountTitle;
				instituteBankAccount.AccountTitleForDisplay = accountTitleForDisplay;
				instituteBankAccount.BranchCode = branchCode;
				instituteBankAccount.BranchName = branchName;
				instituteBankAccount.BranchAddress = branchAddress;
				instituteBankAccount.BranchForDisplay = branchForDisplay;
				instituteBankAccount.AccountTypeEnum = accountTypeEnum;
				instituteBankAccount.Active = active;
				instituteBankAccount.Validate();
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);

				if (aspireContext.AreInstituteBankAccountConsistentForAccountTypes())
				{
					aspireContext.CommitTransaction();
					return UpdateInstituteBankAccountStatuses.Success;
				}

				aspireContext.RollbackTransaction();
				return UpdateInstituteBankAccountStatuses.NotConsistent;
			}
		}
	}
}
