﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.FeeManagement.Admin
{
	public static class FeeHeads
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.FeeManagement.ManageFeeHeads, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.FeeManagement.ManageFeeHeads, UserGroupPermission.PermissionValues.Allowed);
		}


		private static void ReOrderFeeHeads(this AspireContext aspireContext, int instituteID, int userLoginHistoryID)
		{
			var feeHeads = aspireContext.FeeHeads.Where(fh => fh.InstituteID == instituteID).ToList();
			var displayIndex = 0;
			feeHeads.Where(fh => fh.Visible).OrderBy(fh => fh.DisplayIndex).ForEach(fh => fh.DisplayIndex = displayIndex++);
			displayIndex = 0;
			feeHeads.Where(fh => !fh.Visible).OrderBy(fh => fh.DisplayIndex).ForEach(fh => fh.DisplayIndex = displayIndex++);
			aspireContext.SaveChanges(userLoginHistoryID);
		}

		public enum AddFeeHeadStatuses
		{
			HeadNameAlreadyExists,
			HeadShortNameAlreadyExists,
			HeadAliasAlreadyExists,
			TutionFeeAlreadyExists,
			ArrearsAlreadyExists,
			WithholdingTaxAlreadyExists,
			Success,
		}

		public static AddFeeHeadStatuses AddFeeHead(int instituteID, FeeHead.FeeHeadTypes feeHeadTypeEnum, string headName, string headShortName, string headAlias, FeeHead.RuleTypes ruleTypeEnum, bool refundable, bool concession, bool visible, bool withholdingTaxApplicable, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				var feeHead = new FeeHead
				{
					InstituteID = instituteID,
					Visible = visible,
					Concession = concession,
					DisplayIndex = byte.MaxValue,
					FeeHeadTypeEnum = feeHeadTypeEnum,
					HeadAlias = headAlias,
					HeadShortName = headShortName,
					HeadName = headName,
					Refundable = refundable,
					RuleTypeEnum = ruleTypeEnum,
					WithholdingTaxApplicable = feeHeadTypeEnum != FeeHead.FeeHeadTypes.WithholdingTax && withholdingTaxApplicable,
				}.Validate();
				var feeHeads = aspireContext.FeeHeads.Where(q => q.InstituteID == feeHead.InstituteID);
				if (feeHeads.Any(q => q.HeadName == feeHead.HeadName))
					return AddFeeHeadStatuses.HeadNameAlreadyExists;
				if (feeHeads.Any(q => q.HeadShortName == feeHead.HeadShortName))
					return AddFeeHeadStatuses.HeadShortNameAlreadyExists;
				if (feeHeads.Any(q => q.HeadAlias == feeHead.HeadAlias))
					return AddFeeHeadStatuses.HeadAliasAlreadyExists;
				if (feeHeads.Any(q => q.FeeHeadType != (byte)FeeHead.FeeHeadTypes.Custom && q.FeeHeadType == feeHead.FeeHeadType))
					switch (feeHeadTypeEnum)
					{
						case FeeHead.FeeHeadTypes.TutionFee:
							return AddFeeHeadStatuses.TutionFeeAlreadyExists;
						case FeeHead.FeeHeadTypes.Arrears:
							return AddFeeHeadStatuses.ArrearsAlreadyExists;
						case FeeHead.FeeHeadTypes.WithholdingTax:
							return AddFeeHeadStatuses.WithholdingTaxAlreadyExists;
						case FeeHead.FeeHeadTypes.Custom:
							throw new InvalidOperationException();
						default:
							throw new ArgumentOutOfRangeException(nameof(feeHeadTypeEnum), feeHeadTypeEnum, null);
					}

				aspireContext.FeeHeads.Add(feeHead);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ReOrderFeeHeads(feeHead.InstituteID, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return AddFeeHeadStatuses.Success;
			}
		}

		public enum UpdateFeeHeadStatus
		{
			NoRecordFound,
			HeadNameAlreadyExists,
			HeadShortNameAlreadyExists,
			HeadAliasAlreadyExists,
			Success,
		}

		public static UpdateFeeHeadStatus UpdateFeeHead(int feeHeadID, string headName, string headShortName, string headAlias, FeeHead.RuleTypes ruleTypeEnum, bool refundable, int displayIndex, bool concession, bool visible, bool withholdingTaxApplicable, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var feeHead = aspireContext.FeeHeads.SingleOrDefault(fh => fh.FeeHeadID == feeHeadID);
				if (feeHead == null)
					return UpdateFeeHeadStatus.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(feeHead.InstituteID, loginSessionGuid);
				feeHead.HeadName = headName;
				feeHead.HeadShortName = headShortName;
				feeHead.HeadAlias = headAlias;
				feeHead.RuleTypeEnum = ruleTypeEnum;
				feeHead.Refundable = refundable;
				feeHead.Visible = visible;
				feeHead.Concession = concession;
				feeHead.WithholdingTaxApplicable = feeHead.FeeHeadTypeEnum != FeeHead.FeeHeadTypes.WithholdingTax && withholdingTaxApplicable;
				feeHead.Validate();

				if (aspireContext.FeeHeads.Any(fh => fh.InstituteID == feeHead.InstituteID && fh.HeadName == feeHead.HeadName && fh.FeeHeadID != feeHeadID))
					return UpdateFeeHeadStatus.HeadNameAlreadyExists;
				if (aspireContext.FeeHeads.Any(fh => fh.InstituteID == feeHead.InstituteID && fh.HeadShortName == feeHead.HeadShortName && fh.FeeHeadID != feeHeadID))
					return UpdateFeeHeadStatus.HeadShortNameAlreadyExists;
				if (aspireContext.FeeHeads.Any(fh => fh.InstituteID == feeHead.InstituteID && fh.HeadAlias == feeHead.HeadAlias && fh.FeeHeadID != feeHeadID))
					return UpdateFeeHeadStatus.HeadAliasAlreadyExists;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ReOrderFeeHeads(feeHead.InstituteID, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return UpdateFeeHeadStatus.Success;
			}
		}

		public static FeeHead GetFeeHead(int feeHeadID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.FeeHeads).SingleOrDefault(fh => fh.FeeHeadID == feeHeadID);
			}
		}

		public enum DeleteFeeHeadStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success,
		}

		public static DeleteFeeHeadStatuses DeleteFeeHead(int feeHeadID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				var feeHead = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.FeeHeads).SingleOrDefault(fh => fh.FeeHeadID == feeHeadID);
				if (feeHead == null)
					return DeleteFeeHeadStatuses.NoRecordFound;
				if (feeHead.FeeStructureDetails.Any())
					return DeleteFeeHeadStatuses.ChildRecordExists;
				var instituteID = feeHead.InstituteID;
				aspireContext.FeeHeads.Remove(feeHead);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ReOrderFeeHeads(instituteID, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return DeleteFeeHeadStatuses.Success;
			}
		}

		public static List<FeeHead> GetFeeHeads(int? instituteID, FeeHead.FeeHeadTypes? feeHeadTypeEnum, FeeHead.RuleTypes? ruleTypeEnum, bool? refundable, bool? concession, bool? visible, bool? withholdingTaxAppliable, string searchText, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				var feeHeads = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.FeeHeads).Include(fh => fh.Institute);
				if (instituteID != null)
					feeHeads = feeHeads.Where(fh => fh.InstituteID == instituteID.Value);
				if (feeHeadTypeEnum != null)
					feeHeads = feeHeads.Where(fh => fh.FeeHeadType == (byte)feeHeadTypeEnum.Value);
				if (ruleTypeEnum != null)
					feeHeads = feeHeads.Where(fh => fh.RuleType == (byte)ruleTypeEnum.Value);
				if (refundable != null)
					feeHeads = feeHeads.Where(fh => fh.Refundable == refundable.Value);
				if (concession != null)
					feeHeads = feeHeads.Where(fh => fh.Concession == concession.Value);
				if (visible != null)
					feeHeads = feeHeads.Where(fh => fh.Visible == visible.Value);
				if (withholdingTaxAppliable != null)
					feeHeads = feeHeads.Where(fh => fh.WithholdingTaxApplicable == withholdingTaxAppliable.Value);
				if (!string.IsNullOrWhiteSpace(searchText))
					feeHeads = feeHeads.Where(fh => fh.HeadName.Contains(searchText) || fh.HeadShortName.Contains(searchText) || fh.HeadAlias.Contains(searchText));
				return feeHeads.OrderByDescending(fh => fh.Visible).ThenBy(fh => fh.DisplayIndex).ThenBy(fh => fh.Institute.InstituteAlias).ToList();
			}
		}

		public enum ShiftFeeHeadStatuses
		{
			NoRecordFound,
			Success,
		}

		public static ShiftFeeHeadStatuses ShiftFeeHeadUp(int feeHeadID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var feeHeadSelected = aspireContext.FeeHeads.SingleOrDefault(fh => fh.FeeHeadID == feeHeadID);
				if (feeHeadSelected == null)
					return ShiftFeeHeadStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(feeHeadSelected.InstituteID, loginSessionGuid);
				if (feeHeadSelected.DisplayIndex == 0)
					return ShiftFeeHeadStatuses.Success;

				var feeHeadsQuery = aspireContext.FeeHeads.Where(fh => fh.InstituteID == feeHeadSelected.InstituteID && fh.Visible == feeHeadSelected.Visible);
				var feeHeadToMoveDown = feeHeadsQuery.SingleOrDefault(q => q.DisplayIndex == feeHeadSelected.DisplayIndex - 1);
				if (feeHeadToMoveDown != null)
				{
					feeHeadSelected.DisplayIndex = feeHeadSelected.DisplayIndex - 1;
					feeHeadToMoveDown.DisplayIndex = feeHeadToMoveDown.DisplayIndex + 1;
				}
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ReOrderFeeHeads(feeHeadSelected.InstituteID, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return ShiftFeeHeadStatuses.Success;
			}
		}

		public static ShiftFeeHeadStatuses ShiftFeeHeadDown(int feeHeadID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var feeHeadSelected = aspireContext.FeeHeads.SingleOrDefault(fh => fh.FeeHeadID == feeHeadID);
				if (feeHeadSelected == null)
					return ShiftFeeHeadStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(feeHeadSelected.InstituteID, loginSessionGuid);

				var feeHeadsQuery = aspireContext.FeeHeads.Where(fh => fh.InstituteID == feeHeadSelected.InstituteID && fh.Visible == feeHeadSelected.Visible);
				var feeHeadToMoveUp = feeHeadsQuery.SingleOrDefault(q => q.DisplayIndex == feeHeadSelected.DisplayIndex + 1);
				if (feeHeadToMoveUp != null)
				{
					feeHeadSelected.DisplayIndex = feeHeadSelected.DisplayIndex + 1;
					feeHeadToMoveUp.DisplayIndex = feeHeadToMoveUp.DisplayIndex - 1;
				}
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ReOrderFeeHeads(feeHeadSelected.InstituteID, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return ShiftFeeHeadStatuses.Success;
			}
		}
	}
}