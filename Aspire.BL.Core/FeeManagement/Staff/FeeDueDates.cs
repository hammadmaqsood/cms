﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.FeeManagement.Staff
{
	public static class FeeDueDates
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.ManageFeeDueDates, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, null);
		}

		public enum AddOrUpdateFeeDueDatesStatuses
		{
			Added,
			Updated,
		}

		private static AddOrUpdateFeeDueDatesStatuses AddOrUpdateFeeDueDates(this AspireContext aspireContext, short semesterID, int programID, DateTime dueDate, Core.Common.Permissions.Staff.StaffLoginHistory loginHistory)
		{
			var program = aspireContext.Programs.Where(p => p.ProgramID == programID).Select(p => new { p.InstituteID, p.DepartmentID, p.ProgramID }).Single();
			aspireContext.DemandPermissions(loginHistory.LoginSessionGuid, program.InstituteID, program.DepartmentID, program.ProgramID);
			var feeDueDate = aspireContext.FeeDueDates.SingleOrDefault(fdd => fdd.SemesterID == semesterID && fdd.ProgramID == programID && fdd.Program.InstituteID == loginHistory.InstituteID);
			AddOrUpdateFeeDueDatesStatuses status;
			if (feeDueDate == null)
			{
				status = AddOrUpdateFeeDueDatesStatuses.Added;
				feeDueDate = new Model.Entities.FeeDueDate
				{
					ProgramID = programID,
					SemesterID = semesterID,
				};
				aspireContext.FeeDueDates.Add(feeDueDate);
			}
			else
				status = AddOrUpdateFeeDueDatesStatuses.Updated;

			feeDueDate.DueDate = dueDate;
			aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
			return status;
		}

		public sealed class AddOrUpdateFeeDueDatesResult
		{
			public enum Statuses
			{
				None,
				NoRecordFound,
				Success,
			}

			public Statuses Status { get; internal set; }
			public uint TotalRecords { get; internal set; }
			public uint SuccessAdd { get; internal set; }
			public uint SuccessUpdate { get; internal set; }
		}

		public static AddOrUpdateFeeDueDatesResult AddOrUpdateFeeDueDates(short semesterID, List<int> programIDs, DateTime dueDate, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var addOrUpdateFeeDueDatesResult = new AddOrUpdateFeeDueDatesResult();
				var loginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);

				var validListProgramIDs = aspireContext.Programs
					.Where(p => p.InstituteID == loginHistory.InstituteID)
					.Where(p => programIDs.Contains(p.ProgramID))
					.Select(p => p.ProgramID).ToList();

				foreach (var programID in validListProgramIDs)
				{
					addOrUpdateFeeDueDatesResult.TotalRecords++;
					var result = aspireContext.AddOrUpdateFeeDueDates(semesterID, programID, dueDate, loginHistory);
					switch (result)
					{
						case AddOrUpdateFeeDueDatesStatuses.Added:
							addOrUpdateFeeDueDatesResult.SuccessAdd++;
							break;
						case AddOrUpdateFeeDueDatesStatuses.Updated:
							addOrUpdateFeeDueDatesResult.SuccessUpdate++;
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
				aspireContext.CommitTransaction();
				addOrUpdateFeeDueDatesResult.Status = AddOrUpdateFeeDueDatesResult.Statuses.Success;
				return addOrUpdateFeeDueDatesResult;
			}
		}

		public enum DeleteFeeDueDateStatuses
		{
			NoRecordFound,
			Success,
		}

		public static DeleteFeeDueDateStatuses DeleteFeeDueDate(int feeDueDateID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var feeDueDate = aspireContext.FeeDueDates.Include(ems => ems.Program).SingleOrDefault(fdd => fdd.FeeDueDateID == feeDueDateID);
				if (feeDueDate == null)
					return DeleteFeeDueDateStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, feeDueDate.Program.InstituteID, feeDueDate.Program.DepartmentID, feeDueDate.ProgramID);
				aspireContext.FeeDueDates.Remove(feeDueDate);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteFeeDueDateStatuses.Success;
			}
		}

		public static Model.Entities.FeeDueDate GetFeeDueDate(int feeDueDateID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.Programs
					.Where(p => p.InstituteID == loginHistory.InstituteID)
					.SelectMany(p => p.FeeDueDates)
					.Include(fdd => fdd.Program)
					.SingleOrDefault(fdd => fdd.FeeDueDateID == feeDueDateID);
			}
		}

		public sealed class FeeDueDate
		{
			public int FeeDueDateID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public string DepartmentAlias { get; set; }
			public string ProgramAlias { get; internal set; }
			public DateTime DueDate { get; internal set; }
		}

		public static List<FeeDueDate> GetFeeDueDates(short? semesterID, int? departmentID, int? programID, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = aspireContext.Programs
						.Where(p => p.InstituteID == loginHistory.InstituteID)
					.SelectMany(p => p.FeeDueDates)
					.Where(q => semesterID == null || q.SemesterID == semesterID.Value)
					.Where(q => departmentID == null || q.Program.DepartmentID == departmentID.Value)
					.Where(q => programID == null || q.ProgramID == programID.Value);
				virtualItemCount = query.Count();
				switch (sortExpression)
				{
					case nameof(FeeDueDate.SemesterID):
					case nameof(FeeDueDate.Semester):
						query = query.OrderBy(sortDirection, q => q.SemesterID);
						break;
					case nameof(FeeDueDate.DepartmentAlias):
						query = query.OrderBy(sortDirection, q => q.Program.Department.DepartmentAlias);
						break;
					case nameof(FeeDueDate.ProgramAlias):
						query = query.OrderBy(sortDirection, q => q.Program.ProgramAlias);
						break;
					case nameof(FeeDueDate.DueDate):
						query = query.OrderBy(sortDirection, q => q.DueDate);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return query.Skip(pageIndex * pageSize).Take(pageSize).Select(q => new FeeDueDate
				{
					FeeDueDateID = q.FeeDueDateID,
					SemesterID = q.SemesterID,
					ProgramAlias = q.Program.ProgramAlias,
					DepartmentAlias = q.Program.Department.DepartmentAlias,
					DueDate = q.DueDate,
				}).ToList();
			}
		}
	}
}