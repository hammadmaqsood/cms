﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.CourseRegistration.Staff;
using Aspire.BL.Core.FeeManagement.Common;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.FeeManagement.Staff
{
	public static class StudentFees
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.ManageFeeChallan, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class FeeInformation
		{
			public sealed class Student
			{
				internal Student() { }
				public int StudentID { get; internal set; }
				public int AdmissionOpenProgramID { get; internal set; }
				public string Enrollment { get; internal set; }
				public string Program { get; internal set; }
				public int? RegistrationNo { get; internal set; }
				public string Name { get; internal set; }
				public byte Category { get; internal set; }
				public Categories CategoryEnum => (Categories)this.Category;
				public string CategoryFullName => this.CategoryEnum.ToFullName();
				public short IntakeSemesterID { get; internal set; }
				public string IntakeSemester => this.IntakeSemesterID.ToSemesterString();
				public string Nationality { get; internal set; }
				public byte? Status { get; internal set; }
				public Model.Entities.Student.Statuses? StatusEnum => (Model.Entities.Student.Statuses?)this.Status;
				public DateTime? StatusDate { get; internal set; }
				public short? SemesterNo { get; internal set; }
				public SemesterNos? SemesterNoEnum => (SemesterNos?)this.SemesterNo;
				public string SemesterNoFullName => this.SemesterNoEnum?.ToFullName();

				public bool FeeChallanAlreadyGenerated { get; internal set; }
			}

			public sealed class Candidate
			{
				internal Candidate() { }
				public int CandidateID { get; internal set; }
				public int? StudentID { get; set; }
				public string Enrollment { get; set; }
				public int CandidateAppliedProgramID { get; internal set; }
				public int AdmissionOpenProgramID { get; internal set; }
				public int ApplicationNo { get; internal set; }
				public string Program { get; internal set; }
				public string Name { get; internal set; }
				public byte Category { get; internal set; }
				public Categories CategoryEnum => (Categories)this.Category;
				public string CategoryFullName => this.CategoryEnum.ToFullName();
				public short IntakeSemesterID { get; internal set; }
				public string IntakeSemester => this.IntakeSemesterID.ToSemesterString();
				public string Nationality { get; internal set; }
				public bool FeeChallanAlreadyGenerated { get; internal set; }
			}

			internal FeeInformation() { }
			public Student StudentInfo { get; internal set; }
			public Candidate CandidateInfo { get; internal set; }
			public FeeStructure FeeStructure { get; internal set; }
			public List<FeeHead> FeeHeads { get; internal set; }
			public List<CustomListItem> ConcessionList { get; internal set; }
			public List<CourseRegistration.Staff.CourseRegistration.RegisteredCourse> RegisteredCourses { get; internal set; }
			public int? Arrears { get; internal set; }

			public double CreditHours
			{
				get
				{
					if (this.RegisteredCourses == null)
						return 0d;
					return this.RegisteredCourses.Sum(rc => (double?)rc.RoadmapCreditHours) ?? 0d;
				}
			}
		}

		public static FeeInformation GetStudentFeeForGeneratingFeeChallan(short semesterID, string enrollment, int? applicationNo, Guid loginSessionGuid)
		{
			if (enrollment == null && applicationNo == null)
				throw new ArgumentException();
			if (enrollment != null && applicationNo != null)
				throw new ArgumentException();
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var feeInformation = new FeeInformation();
				if (enrollment != null)
				{
					feeInformation.CandidateInfo = null;
					feeInformation.StudentInfo = aspireContext.Students
						.Where(s => s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID && s.Enrollment == enrollment)
						.Select(s => new FeeInformation.Student
						{
							StudentID = s.StudentID,
							AdmissionOpenProgramID = s.AdmissionOpenProgramID,
							Enrollment = s.Enrollment,
							RegistrationNo = s.RegistrationNo,
							Name = s.Name,
							Category = s.Category,
							IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
							Nationality = s.Nationality,
							Program = s.AdmissionOpenProgram.Program.ProgramAlias,
							Status = s.Status,
							StatusDate = s.StatusDate,
							FeeChallanAlreadyGenerated = s.StudentFees.Any(sf => sf.SemesterID == semesterID),
						}).SingleOrDefault();
					if (feeInformation.StudentInfo == null)
						return null;

					feeInformation.StudentInfo.SemesterNo = aspireContext.StudentSemesters.Where(ss => ss.StudentID == feeInformation.StudentInfo.StudentID && ss.SemesterID == semesterID).Select(ss => (short?)ss.SemesterNo).SingleOrDefault();
					feeInformation.RegisteredCourses = aspireContext.GetStudentRegisteredCourses(feeInformation.StudentInfo.StudentID, null, semesterID, null);
					var previousStudentFee = aspireContext.StudentFees.Where(sf => sf.StudentID == feeInformation.StudentInfo.StudentID && sf.SemesterID < semesterID).OrderByDescending(sf => sf.SemesterID).FirstOrDefault();
					if (previousStudentFee != null)
						feeInformation.Arrears = previousStudentFee.StudentFeeChallans.Where(sfc => sfc.Status == StudentFeeChallan.StatusNotPaid).Sum(sfc => (int?)sfc.Amount);
				}
				else
				{
					feeInformation.CandidateInfo = (from cap in aspireContext.CandidateAppliedPrograms
													join aop in aspireContext.AdmissionOpenPrograms on cap.AdmittedAdmissionOpenProgramID equals aop.AdmissionOpenProgramID
													where aop.SemesterID == semesterID && cap.ApplicationNo == applicationNo && aop.Program.InstituteID == loginHistory.InstituteID
													select new FeeInformation.Candidate
													{
														CandidateID = cap.CandidateID,
														StudentID = cap.StudentID,
														Enrollment = cap.Student.Enrollment,
														ApplicationNo = cap.ApplicationNo.Value,
														CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
														AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
														Name = cap.Candidate.Name,
														Category = cap.Candidate.Category.Value,
														IntakeSemesterID = aop.SemesterID,
														Nationality = cap.Candidate.Nationality,
														Program = aop.Program.ProgramAlias,
														FeeChallanAlreadyGenerated = cap.StudentFees.Any(sf => sf.SemesterID == semesterID),
													}).SingleOrDefault();
					if (feeInformation.CandidateInfo == null)
						return null;
					feeInformation.StudentInfo = null;
					feeInformation.RegisteredCourses = null;
				}
				var admissionOpenProgramID = feeInformation.StudentInfo?.AdmissionOpenProgramID ?? feeInformation.CandidateInfo.AdmissionOpenProgramID;
				feeInformation.FeeStructure = aspireContext.FeeStructures.Include(fs => fs.FeeStructureDetails.Select(s => s.FeeHead))
					.Include(fs => fs.AdmissionOpenProgram.Program)
					.SingleOrDefault(fs => fs.AdmissionOpenProgramID == admissionOpenProgramID);

				feeInformation.FeeHeads = aspireContext.FeeHeads.Where(fh => fh.InstituteID == loginHistory.InstituteID && fh.Visible).ToList();
				feeInformation.ConcessionList = aspireContext.FeeConcessionTypes
					.Where(fct => fct.InstituteID == loginHistory.InstituteID && fct.Visible)
					.Select(fct => new CustomListItem
					{
						ID = fct.FeeConcessionTypeID,
						Text = fct.ConcessionName,
					}).OrderBy(fct => fct.Text).ToList();

				return feeInformation;
			}
		}

		public sealed class AddStudentFeeResult
		{
			public enum Statuses
			{
				NoRecordFound,
				EnrollmentGenerated,
				Success
			}
			internal AddStudentFeeResult() { }
			public List<int> StudentFeeIDs { get; internal set; }
			public Statuses Status { get; internal set; }
		}

		public static AddStudentFeeResult AddStudentFee(StudentFee studentFee, List<int> registeredCourseIDs, Guid loginSessionGuid)
		{
			lock (Core.Common.Locks.GenerateFeeChallan)
			{
				using (var aspireContext = new AspireContext(true))
				{
					List<StudentFee> studentFeeList;
					Core.Common.Permissions.Staff.StaffGroupPermission loginHistory;
					if (studentFee.StudentID != null)
					{
						var student = aspireContext.Students.Where(s => s.StudentID == studentFee.StudentID.Value).Select(s => new
						{
							s.AdmissionOpenProgram.Program.InstituteID,
							s.AdmissionOpenProgram.Program.DepartmentID,
							s.AdmissionOpenProgram.ProgramID,
							s.AdmissionOpenProgramID
						}).SingleOrDefault();
						if (student == null)
							return new AddStudentFeeResult { Status = AddStudentFeeResult.Statuses.NoRecordFound };
						loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
						studentFeeList = aspireContext.AddStudentFee(studentFee, registeredCourseIDs, student.InstituteID, loginHistory.UserLoginHistoryID);
						aspireContext.ProcessIndividualFeeDefaulter(studentFee.StudentID ?? throw new InvalidOperationException(), loginHistory.UserLoginHistoryID);
					}
					else if (studentFee.CandidateAppliedProgramID != null)
					{
						var candidate = (from cap in aspireContext.CandidateAppliedPrograms
										 join aop in aspireContext.AdmissionOpenPrograms on cap.AdmittedAdmissionOpenProgramID equals aop.AdmissionOpenProgramID
										 where cap.CandidateAppliedProgramID == studentFee.CandidateAppliedProgramID.Value
										 select new
										 {
											 aop.Program.InstituteID,
											 aop.Program.DepartmentID,
											 aop.ProgramID,
											 aop.AdmissionOpenProgramID,
											 cap.StudentID,
										 }).SingleOrDefault();
						if (candidate == null)
							return new AddStudentFeeResult { Status = AddStudentFeeResult.Statuses.NoRecordFound };
						if (candidate.StudentID != null)
							return new AddStudentFeeResult { Status = AddStudentFeeResult.Statuses.EnrollmentGenerated };
						loginHistory = aspireContext.DemandPermissions(loginSessionGuid, candidate.InstituteID, candidate.DepartmentID, candidate.ProgramID, candidate.AdmissionOpenProgramID);
						studentFeeList = aspireContext.AddStudentFee(studentFee, registeredCourseIDs, candidate.InstituteID, loginHistory.UserLoginHistoryID);
					}
					else
						throw new ArgumentException();

					aspireContext.CommitTransaction();
					return new AddStudentFeeResult { Status = AddStudentFeeResult.Statuses.Success, StudentFeeIDs = studentFeeList.Select(sf => sf.StudentFeeID).ToList() };
				}
			}
		}

		public static StudentFeeChallan GetStudentFeeChallan(int studentFeeChallanID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var studentFeeChallan = aspireContext.StudentFeeChallans.Include(sfc => sfc.StudentFee.StudentFeeDetails.Select(sfd => sfd.FeeHead)).SingleOrDefault(sfc => sfc.StudentFeeChallanID == studentFeeChallanID);
				if (studentFeeChallan == null)
					return null;
				if (studentFeeChallan.StudentFee.StudentID != null)
				{
					var student = aspireContext.Students.Where(s => s.StudentID == studentFeeChallan.StudentFee.StudentID.Value)
						.Select(s => new
						{
							s.AdmissionOpenProgram.Program.InstituteID,
						}).Single();
					if (student.InstituteID == loginHistory.InstituteID)
						return studentFeeChallan;
				}
				else if (studentFeeChallan.StudentFee.CandidateAppliedProgramID != null)
				{
					var candidate = (from cap in aspireContext.CandidateAppliedPrograms
									 join aop in aspireContext.AdmissionOpenPrograms on cap.AdmittedAdmissionOpenProgramID equals aop.AdmissionOpenProgramID
									 where cap.CandidateAppliedProgramID == studentFeeChallan.StudentFee.CandidateAppliedProgramID.Value
									 select new
									 {
										 aop.Program.InstituteID,
									 }).Single();
					if (candidate.InstituteID == loginHistory.InstituteID)
						return studentFeeChallan;
				}
				else
					throw new InvalidOperationException();
				return null;
			}
		}

		public enum UpdateDueDateAndRemarksStatuses
		{
			NoRecordFound,
			CannotUpdateDueDateBecauseFeePaid,
			Success
		}

		public static UpdateDueDateAndRemarksStatuses UpdateDueDateAndRemarks(int studentFeeChallanID, bool exemptWHT, DateTime dueDate, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var studentFeeChallan = aspireContext.StudentFeeChallans.Include(sfc => sfc.StudentFee.StudentFeeDetails.Select(sfd => sfd.FeeHead)).SingleOrDefault(sfc => sfc.StudentFeeChallanID == studentFeeChallanID);
				if (studentFeeChallan == null)
					return UpdateDueDateAndRemarksStatuses.NoRecordFound;
				Core.Common.Permissions.Staff.StaffGroupPermission loginHistory;
				if (studentFeeChallan.StudentFee.StudentID != null)
				{
					var student = aspireContext.Students.Where(s => s.StudentID == studentFeeChallan.StudentFee.StudentID.Value)
						.Select(s => new
						{
							s.AdmissionOpenProgram.Program.InstituteID,
							s.AdmissionOpenProgram.Program.DepartmentID,
							s.AdmissionOpenProgram.Program.ProgramID,
							s.AdmissionOpenProgramID,
						}).Single();
					loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				}
				else if (studentFeeChallan.StudentFee.CandidateAppliedProgramID != null)
				{
					var candidate = (from cap in aspireContext.CandidateAppliedPrograms
									 join aop in aspireContext.AdmissionOpenPrograms on cap.AdmittedAdmissionOpenProgramID equals aop.AdmissionOpenProgramID
									 where cap.CandidateAppliedProgramID == studentFeeChallan.StudentFee.CandidateAppliedProgramID.Value
									 select new
									 {
										 aop.Program.InstituteID,
										 aop.Program.DepartmentID,
										 aop.Program.ProgramID,
										 aop.AdmissionOpenProgramID,
									 }).Single();
					loginHistory = aspireContext.DemandPermissions(loginSessionGuid, candidate.InstituteID, candidate.DepartmentID, candidate.ProgramID, candidate.AdmissionOpenProgramID);
				}
				else
					throw new InvalidOperationException();

				switch (studentFeeChallan.StatusEnum)
				{
					case StudentFeeChallan.Statuses.NotPaid:
						studentFeeChallan.DueDate = dueDate;
						studentFeeChallan.StudentFee.Remarks = remarks.ToNullIfWhiteSpace();
						break;
					case StudentFeeChallan.Statuses.Paid:
						if (studentFeeChallan.DueDate != dueDate)
							return UpdateDueDateAndRemarksStatuses.CannotUpdateDueDateBecauseFeePaid;
						studentFeeChallan.StudentFee.Remarks = remarks.ToNullIfWhiteSpace();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				if (studentFeeChallan.StudentFee.StudentFeeDetails.Any(sfd => sfd.FeeHead.FeeHeadTypeEnum == FeeHead.FeeHeadTypes.WithholdingTax))
					studentFeeChallan.StudentFee.ExemptWHT = true;
				else
					studentFeeChallan.StudentFee.ExemptWHT = exemptWHT;

				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				var studentID = studentFeeChallan.StudentFee.StudentID;
				if (studentID != null)
					aspireContext.ProcessIndividualFeeDefaulter(studentID.Value, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return UpdateDueDateAndRemarksStatuses.Success;
			}
		}

		public enum DeleteStudentFeeStatuses
		{
			NoRecordFound,
			CannotDeleteFeePaid,
			Success,
		}

		public static DeleteStudentFeeStatuses DeleteStudentFee(int studentFeeID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var studentFee = aspireContext.StudentFees
					.Include(sf => sf.StudentFeeDetails.Select(sfd => sfd.FeeHead))
					.Include(sf => sf.StudentFeeChallans)
					.Include(sf => sf.StudentFeeConcessionTypes)
					.Include(sf => sf.RegisteredCourses)
					.SingleOrDefault(sf => sf.StudentFeeID == studentFeeID);
				if (studentFee == null)
					return DeleteStudentFeeStatuses.NoRecordFound;

				(int instituteID, int? departmentID, int programID, int admissionOpenProgramID) ids;
				Core.Common.Permissions.Staff.StaffGroupPermission loginHistory;
				if (studentFee.StudentID != null)
				{
					var student = aspireContext.Students.Where(s => s.StudentID == studentFee.StudentID)
						.Select(s => new
						{
							s.AdmissionOpenProgram.Program.InstituteID,
							s.AdmissionOpenProgram.Program.DepartmentID,
							s.AdmissionOpenProgram.ProgramID,
							s.AdmissionOpenProgramID,
						}).Single();
					ids = (student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				}
				else if (studentFee.CandidateAppliedProgramID != null)
				{
					var candidate = aspireContext.CandidateAppliedPrograms
						.Where(cap => cap.CandidateAppliedProgramID == studentFee.CandidateAppliedProgramID.Value)
						.Select(cap => new
						{
							cap.AdmissionOpenProgram.Program.InstituteID,
							cap.AdmissionOpenProgram.Program.DepartmentID,
							cap.AdmissionOpenProgram.ProgramID,
							cap.AdmissionOpenProgram.AdmissionOpenProgramID,
						}).Single();
					ids = (candidate.InstituteID, candidate.DepartmentID, candidate.ProgramID, candidate.AdmissionOpenProgramID);
				}
				else
					throw new InvalidOperationException();

				switch (studentFee.StatusEnum)
				{
					case StudentFee.Statuses.Paid:
						if (!studentFee.IsZero)
							return DeleteStudentFeeStatuses.CannotDeleteFeePaid;
						loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.ManageFeeChallan, UserGroupPermission.PermissionValues.Allowed, ids.instituteID, ids.departmentID, ids.programID, ids.admissionOpenProgramID);
						break;
					case StudentFee.Statuses.NotPaid:
						switch (studentFee.FeeTypeEnum)
						{
							case StudentFee.FeeTypes.Attendance:
								if (studentFee.StudentFeeDetails.Any())
									throw new InvalidOperationException($"StudentFeeDetails found for StudentFee of type Attendance. StudentFeeID: {studentFee.StudentFeeID}.");
								loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.ManageFeeChallan, UserGroupPermission.PermissionValues.Allowed, ids.instituteID, ids.departmentID, ids.programID, ids.admissionOpenProgramID);
								break;
							case StudentFee.FeeTypes.Refund:
							case StudentFee.FeeTypes.Challan:
								if (studentFee.StudentFeeChallans.Any(sfc => sfc.StatusEnum == StudentFeeChallan.Statuses.Paid || sfc.AccountTransactionID != null))
									throw new InvalidOperationException($"StudentFee Statuses are inconsistent? StudentFeeID: {studentFee.StudentFeeID}.");
								if (studentFee.StudentFeeDetails.Any(sfd => sfd.FeeHead.FeeHeadTypeEnum == FeeHead.FeeHeadTypes.WithholdingTax))
									loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.CanDeleteFeeChallanContainingWithHoldingTax, UserGroupPermission.PermissionValues.Allowed, ids.instituteID, ids.departmentID, ids.programID, ids.admissionOpenProgramID);
								else
									loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.ManageFeeChallan, UserGroupPermission.PermissionValues.Allowed, ids.instituteID, ids.departmentID, ids.programID, ids.admissionOpenProgramID);
								break;
							default:
								throw new NotImplementedEnumException(studentFee.FeeTypeEnum);
						}
						break;
					default:
						throw new NotImplementedEnumException(studentFee.StatusEnum);
				}

				var studentID = studentFee.StudentID;
				aspireContext.StudentFeeDetails.RemoveRange(studentFee.StudentFeeDetails);
				aspireContext.StudentFeeConcessionTypes.RemoveRange(studentFee.StudentFeeConcessionTypes);
				aspireContext.StudentFeeChallans.RemoveRange(studentFee.StudentFeeChallans);
				studentFee.RegisteredCourses.ForEach(rc => rc.StudentFeeID = null);
				aspireContext.StudentFees.Remove(studentFee);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				if (studentID != null)
					aspireContext.ProcessIndividualFeeDefaulter(studentID.Value, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return DeleteStudentFeeStatuses.Success;
			}
		}

		public static (List<StudentFeeChallan> StudentFeeChallans, List<Common.StudentFees.WithHoldingTax> WithHoldingTaxes) GetStudentFeeChallans(int? studentID, int? candidateAppliedProgramID, short? semesterID, Guid loginSessionGuid)
		{
			if (studentID == null && candidateAppliedProgramID == null)
				throw new ArgumentException();
			if (studentID != null && candidateAppliedProgramID != null)
				throw new ArgumentException();
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var studentFeeChallansQuery = aspireContext.StudentFeeChallans.AsQueryable();
				if (semesterID != null)
					studentFeeChallansQuery = studentFeeChallansQuery.Where(sfc => sfc.StudentFee.SemesterID == semesterID.Value);
				if (studentID != null)
					studentFeeChallansQuery = studentFeeChallansQuery.Where(sfc => sfc.StudentFee.StudentID == studentID.Value && sfc.StudentFee.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				if (candidateAppliedProgramID != null)
					studentFeeChallansQuery = (from cap in aspireContext.CandidateAppliedPrograms
											   join aop in aspireContext.AdmissionOpenPrograms on cap.AdmittedAdmissionOpenProgramID equals aop.AdmissionOpenProgramID
											   where cap.CandidateAppliedProgramID == candidateAppliedProgramID.Value && aop.Program.InstituteID == loginHistory.InstituteID
											   select cap).SelectMany(cap => cap.StudentFees).SelectMany(sf => sf.StudentFeeChallans);
				var studentFeeChallans = studentFeeChallansQuery
					.Include(sfc => sfc.StudentFee.StudentFeeDetails.Select(sfd => sfd.FeeHead))
					.Include(sfc => sfc.AccountTransaction)
					.OrderByDescending(sfc => sfc.StudentFee.StudentFeeID)
					.ToList();

				var withholdingTaxes = aspireContext.GetWithHoldingTaxes(studentID, candidateAppliedProgramID, null);
				return (studentFeeChallans, withholdingTaxes);
			}
		}

		public sealed class CandidateInfo
		{
			internal CandidateInfo() { }
			public int CandidateAppliedProgramID { get; internal set; }
			public int ApplicationNo { get; internal set; }
			public string Name { get; internal set; }
			public string FatherName { get; internal set; }
			public string Program { get; internal set; }
			public byte? Category { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Nationality { get; internal set; }

			public string CategoryEnumFullName => ((Categories?)this.Category)?.ToFullName();
		}

		public static CandidateInfo GetCandidateInfo(int applicationNo, short semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var q = from cap in aspireContext.CandidateAppliedPrograms
						join aop in aspireContext.AdmissionOpenPrograms on cap.AdmittedAdmissionOpenProgramID equals aop.AdmissionOpenProgramID
						where cap.ApplicationNo == applicationNo && aop.SemesterID == semesterID && aop.Program.InstituteID == loginHistory.InstituteID
						select new CandidateInfo
						{
							CandidateAppliedProgramID = cap.CandidateAppliedProgramID,
							ApplicationNo = cap.ApplicationNo.Value,
							Name = cap.Candidate.Name,
							FatherName = cap.Candidate.FatherName,
							Category = cap.Candidate.Category,
							Nationality = cap.Candidate.Nationality,
							SemesterID = aop.SemesterID,
							Program = aop.Program.ProgramAlias,
						};
				return q.SingleOrDefault();
			}
		}

		public static StudentSemester GetStudentCurrentSemester(string enrollment, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.StudentSemesters.OrderByDescending(ss => ss.SemesterID).FirstOrDefault(ss => ss.Student.Enrollment == enrollment && ss.Status == StudentSemester.StatusRegistered);
			}
		}

		public static List<FeeDefaulters.FeeDefaulterReason> ProcessFeeDefaulter(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var result = aspireContext.ProcessIndividualFeeDefaulter(studentID, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return result;
			}
		}

		public sealed class StudentFeeInfo
		{
			internal StudentFeeInfo() { }
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public int? RegistrationNo { get; set; }
			public string Name { get; internal set; }
			public Categories CategoryEnum { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public string Nationality { get; internal set; }
			public List<int> StudentFeeChallanIDs { get; internal set; }
			public List<RegisteredCourse> RegisteredCourses { get; set; }
			public List<short> OfferedSemesterIDs => this.RegisteredCourses.Select(rc => rc.OfferedSemesterID).Distinct().OrderByDescending(s => s).ToList();
			public string FatherName { get; internal set; }
			public Model.Entities.Student.Statuses? StatusEnum { get; internal set; }
			public string BlockedReason { get; internal set; }
			public DateTime? StatusDate { get; internal set; }

			public sealed class RegisteredCourse
			{
				internal RegisteredCourse() { }
				public int RegisteredCourseID { get; set; }
				public short OfferedSemesterID { get; set; }
				public string Title { get; set; }
				public decimal CreditHours { get; set; }
				public string ProgramAlias { get; set; }
				public short SemesterNo { get; set; }
				public int Section { get; set; }
				public byte Shift { get; set; }
				public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
				public byte? Status { get; set; }
				public byte? FreezeStatus { get; set; }
				public string StatusHtml => AspireFormats.GetRegisteredCourseStatusHtml(this.Status, this.FreezeStatus);
				public bool FeeDefaulter { get; internal set; }
				public string FeeDefaulterYesNo => this.FeeDefaulter.ToYesNo();
				public int? StudentFeeChallanID { get; internal set; }
				public int? StudentFeeID { get; internal set; }
				public bool? NewAdmission { get; internal set; }
				public string InstituteCode { get; internal set; }
				public string FullChallanNo => this.StudentFeeChallanID == null ? null : AspireFormats.ChallanNo.GetFullChallanNoString(this.InstituteCode, this.NewAdmission == true, this.StudentFeeChallanID.Value);
			}
		}

		public static StudentFeeInfo GetStudentFeeInfo(string enrollment, short? offeredSemesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.Students
					.Where(s => s.Enrollment == enrollment)
					.Where(s => s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Select(s => new StudentFeeInfo
					{
						StudentID = s.StudentID,
						Enrollment = s.Enrollment,
						RegistrationNo = s.RegistrationNo,
						Name = s.Name,
						FatherName = s.FatherName,
						CategoryEnum = (Categories)s.Category,
						ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
						IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
						Nationality = s.Nationality,
						StatusEnum = (Model.Entities.Student.Statuses?)s.Status,
						BlockedReason = s.BlockedReason,
						StatusDate = s.StatusDate,
						StudentFeeChallanIDs = s.StudentFees.SelectMany(sf => sf.StudentFeeChallans).Select(sfc => sfc.StudentFeeChallanID).ToList(),
						RegisteredCourses = s.RegisteredCourses
							.Where(rc => rc.DeletedDate == null && (offeredSemesterID == null || rc.OfferedCours.SemesterID == offeredSemesterID))
							.Select(rc => new StudentFeeInfo.RegisteredCourse
							{
								RegisteredCourseID = rc.RegisteredCourseID,
								OfferedSemesterID = rc.OfferedCours.SemesterID,
								Title = rc.Cours.Title,
								CreditHours = rc.Cours.CreditHours,
								InstituteCode = rc.Student.AdmissionOpenProgram.Program.Institute.InstituteCode,
								ProgramAlias = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
								SemesterNo = rc.OfferedCours.SemesterNo,
								Section = rc.OfferedCours.Section,
								Shift = rc.OfferedCours.Shift,
								Status = rc.Status,
								FreezeStatus = rc.FreezedStatus,
								FeeDefaulter = rc.FeeDefaulter,
								NewAdmission = rc.StudentFee.NewAdmission,
								StudentFeeChallanID = rc.StudentFee.StudentFeeChallans.Select(sfc => (int?)sfc.StudentFeeChallanID).FirstOrDefault(),
								StudentFeeID = rc.StudentFeeID,
							})
							.OrderByDescending(rc => rc.OfferedSemesterID).ThenBy(rc => rc.Title)
							.ToList(),
					})
					.SingleOrDefault();
			}
		}
	}
}