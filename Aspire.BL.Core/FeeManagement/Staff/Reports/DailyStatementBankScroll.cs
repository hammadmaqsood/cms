﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.FeeManagement.Staff.Reports
{
	public static class DailyStatementBankScroll
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public ReportHeader ReportHeader { get; internal set; }
			public List<Detail> Details { get; internal set; }
		}

		public sealed class ReportHeader
		{
			public string InstituteName { get; set; }
			public string ReportName { get; internal set; }
			public string Semester { get; internal set; }

			public List<ReportHeader> GetDailyStatementBankScrollReportHeader()
			{
				return null;
			}
		}

		public sealed class Detail
		{
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public DateTime DepositDate { get; internal set; }
			public string MonthName => this.DepositDate.ToString("MMM-yyyy");
			public int Month => this.DepositDate.ToString("yyyyMM").ToInt();
			public int Amount { get; internal set; }
			public int InstituteBankAccountID { get; internal set; }
			public string BankAlias { get; internal set; }
			public string AccountNo { get; internal set; }
			public string AccountTitle { get; internal set; }

			public List<Detail> GetDailyStatementBankScrollDetail()
			{
				return null;
			}
		}

		public static ReportDataSet GetDailyStatementBankScroll(short? semesterID, DateTime? depositDateFrom, DateTime? depositDateTo, int? instituteBankAccountID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.IncreaseCommandTimeout();
				depositDateFrom = depositDateFrom?.Date;
				depositDateTo = depositDateTo?.Date.AddDays(1);
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var summary = aspireContext.StudentFeeChallans
					.Where(sfc => sfc.StudentFee.FeeType == StudentFee.FeeTypeChallan)
					.Where(sfc => sfc.Status == StudentFeeChallan.StatusPaid)
					.Where(sfc => sfc.AccountTransaction.InstituteBankAccount.Institute.InstituteID == loginHistory.InstituteID)
					.Where(sfc => semesterID == null || sfc.StudentFee.SemesterID == semesterID.Value)
					.Where(sfc => instituteBankAccountID == null || sfc.AccountTransaction.InstituteBankAccountID == instituteBankAccountID.Value)
					.Where(sfc => depositDateFrom == null || depositDateFrom.Value <= sfc.AccountTransaction.TransactionDate)
					.Where(sfc => depositDateTo == null || sfc.AccountTransaction.TransactionDate < depositDateTo.Value)
					.GroupBy(sfc => new
					{
						sfc.AccountTransaction.InstituteBankAccountID,
						sfc.AccountTransaction.InstituteBankAccount.Bank.BankAlias,
						sfc.AccountTransaction.InstituteBankAccount.AccountNo,
						sfc.AccountTransaction.InstituteBankAccount.AccountTitle,
						sfc.StudentFee.SemesterID,
						DepositDate = System.Data.Entity.DbFunctions.TruncateTime(sfc.AccountTransaction.TransactionDate).Value,
					})
					.Select(g => new Detail
					{
						InstituteBankAccountID = g.Key.InstituteBankAccountID,
						BankAlias = g.Key.BankAlias,
						AccountNo = g.Key.AccountNo,
						AccountTitle = g.Key.AccountTitle,
						SemesterID = g.Key.SemesterID,
						DepositDate = g.Key.DepositDate,
						Amount = g.Sum(c => c.Amount),
					})
					.ToList();

				return new ReportDataSet
				{
					ReportHeader = new ReportHeader
					{
						InstituteName = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteShortName).SingleOrDefault(),
						ReportName = "Daily Statement of Bank Scroll",
					},
					Details = summary
				};
			}
		}
	}
}
