﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FeeManagement.Common.Reports;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.FeeManagement.Staff.Reports
{
	public static class FeeChallans
	{
		public static List<FeeManagement.Common.Reports.FeeChallans.FeeChallan> GetFeeChallans(List<int> studentFeeIDs, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed);
				var studentFees = aspireContext.StudentFees
					.Where(sf => studentFeeIDs.Contains(sf.StudentFeeID))
					.Select(sf => new { sf.StudentID, sf.CandidateAppliedProgramID })
					.ToList();

				foreach (var studentFee in studentFees)
				{
					if (studentFee.StudentID != null)
					{
						var studentFound = aspireContext.Students
							.Where(s => s.StudentID == studentFee.StudentID.Value)
							.Any(s => s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
						if (studentFound == false)
							return null;
					}
					else if (studentFee.CandidateAppliedProgramID != null)
					{
						var candidateFound = aspireContext.CandidateAppliedPrograms
							.Where(cap => cap.CandidateAppliedProgramID == studentFee.CandidateAppliedProgramID.Value)
							.Any(cap => cap.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
						if (candidateFound == false)
							return null;
					}
					else
						throw new InvalidOperationException();
				}

				return aspireContext.GetFeeChallans(studentFeeIDs);
			}
		}
	}
}
