﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.FeeManagement.Staff.Reports
{
	public sealed class FeeReportParameters
	{
		public int InstituteID { get; set; }

		#region StudentFee

		public byte FeeType { get; set; }
		public short? SemesterID { get; set; }
		public bool? NewAdmission { get; set; }
		public decimal? CreditHours { get; set; }
		public ComparisonOperators CreditHoursOperator { get; set; }
		public byte? StudentFeeStatus { get; set; }
		public DateTime? CreatedFromDate { get; set; }
		public DateTime? CreatedToDate { get; set; }
		public List<int> FeeConcessionTypeIDs { get; set; }
		public LogicalOperators FeeConcessionTypeIDsLogicalOperator { get; set; }
		public string Remarks { get; set; }
		public bool? ExemptWHT { get; set; }

		#endregion

		#region Student, Candidate

		public List<byte> Categories { get; set; }
		public int? DepartmentID { get; set; }
		public int? ProgramID { get; set; }
		public short? SemesterNo { get; set; }
		public int? Section { get; set; }
		public byte? Shift { get; set; }

		#endregion

		#region StudentFeeChallans

		public byte? FeeChallanStatus { get; set; }
		public DateTime? DueFromDate { get; set; }
		public DateTime? DueToDate { get; set; }
		public List<int> FeeHeadIDs { get; set; }

		#endregion

		#region AccountTransactions

		public int? InstituteBankAccountID { get; set; }
		public byte? Service { get; set; }
		public DateTime? TransactionDateFrom { get; set; }
		public DateTime? TransactionDateTo { get; set; }

		#endregion

		public string GetFiltersText(string department, string program, string instituteBankAccountNo, List<string> feeConcessionTypes, List<string> feeHeads)
		{
			var filters = string.Empty;

			if (this.NewAdmission != null)
				filters += Environment.NewLine + $"New Admission = {this.NewAdmission.Value.ToYesNo()}";
			if (this.DepartmentID != null)
				filters += Environment.NewLine + $"Department = {department}";
			if (this.ProgramID != null)
				filters += Environment.NewLine + $"Program = {program}";
			if (this.SemesterNo != null)
				filters += Environment.NewLine + $"Semester No. = {((SemesterNos)this.SemesterNo).ToFullName()}";
			if (this.Section != null)
				filters += Environment.NewLine + $"Section = {((Sections)this.Section).ToFullName()}";
			if (this.Shift != null)
				filters += Environment.NewLine + $"Shift = {((Shifts)this.Shift).ToFullName()}";

			var dateFormat = "d";
			if (this.CreatedFromDate != null || this.CreatedToDate != null)
				filters += Environment.NewLine + $"Create Date = {this.CreatedFromDate.ConcatenateDates(this.CreatedToDate, dateFormat)}";

			if (this.DueFromDate != null || this.DueToDate != null)
				filters += Environment.NewLine + $"Due Date = {this.DueFromDate.ConcatenateDates(this.DueToDate, dateFormat)}";

			if (this.TransactionDateFrom != null || this.TransactionDateTo != null)
				filters += Environment.NewLine + $"Deposit Date = {this.TransactionDateFrom.ConcatenateDates(this.TransactionDateTo, dateFormat)}";

			if (this.ExemptWHT != null)
				filters += Environment.NewLine + $"Exempt WHT = {this.ExemptWHT.Value.ToYesNo()}";

			if (this.Categories != null && this.Categories.Any())
				filters += Environment.NewLine + $"Categories = ({string.Join(" or ", this.Categories.Select(c => ((Categories)c).ToFullName()))})";

			if (this.CreditHours != null)
				switch (this.CreditHoursOperator)
				{
					case ComparisonOperators.Equal:
						filters += Environment.NewLine + $"Credit Hours = {this.CreditHours}";
						break;
					case ComparisonOperators.GreaterThan:
						filters += Environment.NewLine + $"Credit Hours > {this.CreditHours}";
						break;
					case ComparisonOperators.GreaterThanEqual:
						filters += Environment.NewLine + $"Credit Hours >= {this.CreditHours}";
						break;
					case ComparisonOperators.LessThan:
						filters += Environment.NewLine + $"Credit Hours < {this.CreditHours}";
						break;
					case ComparisonOperators.LessThanEqual:
						filters += Environment.NewLine + $"Credit Hours <= {this.CreditHours}";
						break;
					case ComparisonOperators.NotEqual:
						filters += Environment.NewLine + $"Credit Hours != {this.CreditHours}";
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

			if (this.FeeConcessionTypeIDs != null && this.FeeConcessionTypeIDs.Any())
				switch (this.FeeConcessionTypeIDsLogicalOperator)
				{
					case LogicalOperators.And:
						filters += Environment.NewLine + $"Fee Concessions = ({string.Join(" and ", feeConcessionTypes)})";
						break;
					case LogicalOperators.Or:
						filters += Environment.NewLine + $"Fee Concessions = ({string.Join(" or ", feeConcessionTypes)})";
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

			if (this.InstituteBankAccountID != null)
				filters += Environment.NewLine + $"Institute Account No. = {instituteBankAccountNo}";

			if (this.Service != null)
				filters += Environment.NewLine + $"Service = {((AccountTransaction.Services)this.Service).ToFullName()}";

			if (this.FeeChallanStatus != null)
				filters += Environment.NewLine + $"Fee Challan Status = {((StudentFeeChallan.Statuses)this.FeeChallanStatus).ToFullName()}";

			if (this.StudentFeeStatus != null)
				filters += Environment.NewLine + $"Fee Status = {((StudentFee.Statuses)this.StudentFeeStatus).ToFullName()}";
			if (!string.IsNullOrEmpty(this.Remarks))
				filters += Environment.NewLine + $"Remarks = {this.Remarks}";

			if (this.FeeHeadIDs?.Any() == true)
				filters += Environment.NewLine + $"Fee Heads = {string.Join(",", feeHeads)}";

			return filters.Trim().ToNullIfWhiteSpace();
		}

		public FeeReportParameters ConvertDatesToLocalTime()
		{
			this.CreatedFromDate = this.CreatedFromDate?.ToLocalTime();
			this.CreatedToDate = this.CreatedToDate?.ToLocalTime();
			this.TransactionDateFrom = this.TransactionDateFrom?.ToLocalTime();
			this.TransactionDateTo = this.TransactionDateTo?.ToLocalTime();
			this.DueFromDate = this.DueFromDate?.ToLocalTime();
			this.DueToDate = this.DueToDate?.ToLocalTime();
			return this;
		}
	}
}
