﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.FeeManagement.Staff.Reports
{
	public static class Scholarship
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public ReportHeader ReportHeader { get; internal set; }
			public List<Detail> Details { get; internal set; }
		}

		public sealed class ReportHeader
		{
			public string InstituteName { get; set; }
			public string ReportName { get; internal set; }
			public string Semester { get; internal set; }

			public List<ReportHeader> GetScholarshipHeader()
			{
				return null;
			}
		}

		public sealed class Detail
		{
			public int ChallanNo { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public int ScholarshipAmount { get; internal set; }
			public int ConcessionAmount { get; internal set; }
			public int TotalAmount { get; internal set; }
			public int GrandTotalAmount { get; internal set; }
			public string NewAdmission { get; internal set; }
			public DateTime? DepositDate { get; internal set; }
			public string Status { get; internal set; }
			public string Concessions { get; internal set; }
			public string LastDegree { get; internal set; }

			public List<Detail> GetScholarshipDetail()
			{
				return null;
			}
		}

		public static ReportDataSet GetScholarships(short semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.IncreaseCommandTimeout();
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var scholarshipHeadName = "Scholarship Amount";

				string LastDegree(DegreeTypes degreeTypeEnum, double obtained, double total, bool isCGPA)
				{
					return isCGPA
						? (total > 0 ? $"{degreeTypeEnum.ToFullName()}\n{obtained:#.##}/{total:#.##} = {(obtained * 100d / total):#.##}%" : "Error")
						: $"{degreeTypeEnum.ToFullName()}\n{obtained:#.##}/{total:#.##}";
				}
				var data = aspireContext.StudentFees
					.Where(sf => sf.SemesterID == semesterID && sf.Status == StudentFee.StatusPaid)
					.Where(sf => sf.ConcessionAmount != 0 || sf.StudentFeeDetails.Any(sfd => sfd.GrandTotalAmount != 0 && sfd.FeeHead.HeadName == scholarshipHeadName))
					.Select(sf => new
					{
						StudentFeeChallanID = sf.StudentFeeChallans.Select(sfc => sfc.StudentFeeChallanID).FirstOrDefault(),
						DepositDate = sf.StudentFeeChallans.Select(sfc => (DateTime?)sfc.AccountTransaction.TransactionDate).FirstOrDefault(),
						Student = new
						{
							sf.Student.Enrollment,
							sf.Student.Name,
							sf.Student.AdmissionOpenProgram.Program.ProgramAlias,
							InstituteID = (int?)sf.Student.AdmissionOpenProgram.Program.InstituteID,
							AcademicRecord = sf.Student.StudentAcademicRecords.Select(sar => new
							{
								DegreeTypeEnum = (DegreeTypes)sar.DegreeType,
								sar.IsCGPA,
								sar.ObtainedMarks,
								sar.TotalMarks,
							}).OrderByDescending(sar => sar.DegreeTypeEnum).FirstOrDefault()
						},
						Candidate = new
						{
							sf.CandidateAppliedProgram.ApplicationNo,
							sf.CandidateAppliedProgram.Candidate.Name,
							sf.CandidateAppliedProgram.AdmissionOpenProgram.Program.ProgramAlias,
							InstituteID = (int?)sf.CandidateAppliedProgram.AdmissionOpenProgram.Program.InstituteID,
							AcademicRecord = sf.CandidateAppliedProgram.CandidateAppliedProgramAcademicRecords.Select(capar => new
							{
								DegreeTypeEnum = (DegreeTypes)capar.DegreeType,
								capar.IsCGPA,
								capar.ObtainedMarks,
								capar.TotalMarks,
							}).OrderByDescending(sar => sar.DegreeTypeEnum).FirstOrDefault()
						},
						ScholarshipAmount = sf.StudentFeeDetails.Where(sfd => sfd.FeeHead.HeadName == scholarshipHeadName && sfd.GrandTotalAmount != 0).Select(sfd => sfd.GrandTotalAmount).FirstOrDefault(),
						sf.ConcessionAmount,
						sf.TotalAmount,
						sf.GrandTotalAmount,
						sf.NewAdmission,
						Status = (StudentFee.Statuses)sf.Status,
						ConcessionNames = sf.StudentFeeConcessionTypes.Select(sfct => sfct.FeeConcessionType.ConcessionName).ToList(),
					})
					.ToList()
					.Where(f => f.Student?.InstituteID == loginHistory.InstituteID || f.Candidate?.InstituteID == loginHistory.InstituteID)
					.Select(f => new Detail
					{
						ChallanNo = f.StudentFeeChallanID,
						Enrollment = f.Student?.Enrollment ?? f.Candidate?.ApplicationNo.ToString(),
						Name = f.Student?.Name ?? f.Candidate?.Name,
						ProgramAlias = f.Student?.ProgramAlias ?? f.Candidate?.ProgramAlias,
						ScholarshipAmount = f.ScholarshipAmount,
						ConcessionAmount = f.ConcessionAmount,
						TotalAmount = f.TotalAmount,
						GrandTotalAmount = f.GrandTotalAmount,
						NewAdmission = f.NewAdmission.ToYesNo(),
						DepositDate = f.DepositDate,
						Status = f.Status.ToFullName(),
						Concessions = string.Join("\n", f.ConcessionNames),
						LastDegree = f.Student?.AcademicRecord != null
							? LastDegree(f.Student.AcademicRecord.DegreeTypeEnum, f.Student.AcademicRecord.ObtainedMarks, f.Student.AcademicRecord.TotalMarks, f.Student.AcademicRecord.IsCGPA)
							: (f.Candidate?.AcademicRecord != null
							? LastDegree(f.Candidate.AcademicRecord.DegreeTypeEnum, f.Candidate.AcademicRecord.ObtainedMarks, f.Candidate.AcademicRecord.TotalMarks, f.Candidate.AcademicRecord.IsCGPA)
							: null)
					}).ToList();

				return new ReportDataSet
				{
					ReportHeader = new ReportHeader
					{
						InstituteName = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteShortName).SingleOrDefault(),
						ReportName = $"Scholarship Report ({semesterID.ToSemesterString()})",
					},
					Details = data
				};
			}
		}
	}
}
