﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.FeeManagement.Staff.Reports
{
	public static class ProgramWiseSummary
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public DetailedScroll.ReportHeader ReportHeader { get; internal set; }
			public List<DetailedScroll.FeeChallan> FeeChallans { get; internal set; }
		}

		public sealed class ReportHeader
		{
			public string InstituteName { get; set; }
			public string ReportName { get; internal set; }
			public string Semester { get; internal set; }
			public string Filters { get; internal set; }

			public List<DetailedScroll.ReportHeader> GetList()
			{
				return null;
			}
		}

		internal sealed class Detail
		{
			public Detail(FeeHead feeHead, List<StudentFeeDetail> studentFeeDetails)
			{
				this.FeeHead = feeHead;
				this.TotalAmount = studentFeeDetails.Sum(sfd => sfd.TotalAmount ?? 0);
				this.ConcessionAmount = studentFeeDetails.Sum(sfd => sfd.ConcessionAmount ?? 0);
				this.GrandTotalAmount = studentFeeDetails.Sum(sfd => sfd.GrandTotalAmount);
				if (this.TotalAmount - this.ConcessionAmount != this.GrandTotalAmount)
					throw new InvalidOperationException();
				switch (feeHead.RuleTypeEnum)
				{
					case FeeHead.RuleTypes.Credit:
						this.TotalAmountForCalculation = this.GrandTotalAmount;
						break;
					case FeeHead.RuleTypes.Debit:
						this.TotalAmountForCalculation = -this.GrandTotalAmount;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			public Model.Entities.FeeHead FeeHead { get; }
			public int ConcessionAmount { get; }
			public int GrandTotalAmount { get; }
			public int TotalAmount { get; }
			internal int TotalAmountForCalculation { get; }
		}

		internal sealed class Program
		{
			public Program(int? departmentID, string departmentName, int programID, string programAlias, IReadOnlyCollection<StudentFee> studentFees)
			{
				this.DepartmentID = departmentID;
				this.DepartmentName = departmentName;
				this.ProgramID = programID;
				this.ProgramAlias = programAlias;
				var listStudents = new List<Tuple<int?, int?>>();
				studentFees.Select(sf => new { sf.StudentID, sf.CandidateAppliedProgramID }).ForEach(x =>
				{
					if (listStudents.Any(l => x.StudentID != null && l.Item1 == x.StudentID))
						return;
					if (listStudents.Any(l => x.CandidateAppliedProgramID != null && l.Item2 == x.CandidateAppliedProgramID))
						return;
					listStudents.Add(new Tuple<int?, int?>(x.StudentID, x.CandidateAppliedProgramID));
				});
				this.TotalStudents = listStudents.Count;
				var listPaidStudents = new List<Tuple<int?, int?>>();
				studentFees.Where(sf => sf.StatusEnum != StudentFee.Statuses.NotPaid).Select(sf => new { sf.StudentID, sf.CandidateAppliedProgramID }).ForEach(x =>
				  {
					  if (listPaidStudents.Any(l => x.StudentID != null && l.Item1 == x.StudentID))
						  return;
					  if (listPaidStudents.Any(l => x.CandidateAppliedProgramID != null && l.Item2 == x.CandidateAppliedProgramID))
						  return;
					  listPaidStudents.Add(new Tuple<int?, int?>(x.StudentID, x.CandidateAppliedProgramID));
				  });
				this.TotalStudentsFeePaid = listPaidStudents.Count;
				this.TotalAmount = studentFees.Sum(sf => sf.TotalAmount);
				this.ConcessionAmount = studentFees.Sum(sf => sf.ConcessionAmount);
				this.GrandTotalAmount = studentFees.Sum(sf => sf.GrandTotalAmount);
				this.PaymentReceived = studentFees.SelectMany(sf => sf.StudentFeeChallans).Where(sfc => sfc.StatusEnum == StudentFeeChallan.Statuses.Paid).Select(sfc => sfc.Amount).Sum();
				this.DefaulterAmount = studentFees.SelectMany(sf => sf.StudentFeeChallans).Where(sfc => sfc.StatusEnum == StudentFeeChallan.Statuses.NotPaid).Select(sfc => sfc.Amount).Sum();
				this.Details = studentFees
					.SelectMany(sf => sf.StudentFeeDetails)
					.GroupBy(sfd => sfd.FeeHead)
					.Select(sfd => new Detail(sfd.Key, sfd.ToList())).ToList();

				var studentFeeTotalAmount = studentFees.Sum(sf => sf.TotalAmount);
				var studentFeeConcessionAmount = studentFees.Sum(sf => sf.ConcessionAmount);
				var studentFeeGrandTotalAmount = studentFees.Sum(sf => sf.GrandTotalAmount);

				if (studentFeeGrandTotalAmount != studentFeeTotalAmount - studentFeeConcessionAmount)
					throw new InvalidOperationException();
				if (studentFeeGrandTotalAmount != studentFees.SelectMany(sf => sf.StudentFeeChallans).Sum(sfc => sfc.Amount))
					throw new InvalidOperationException();
				if (studentFeeGrandTotalAmount != this.Details.Sum(d => d.TotalAmountForCalculation))
					throw new InvalidOperationException();
			}

			public string ProgramAlias { get; }
			public int TotalStudents { get; }
			public int TotalAmount { get; }
			public int ConcessionAmount { get; }
			public int GrandTotalAmount { get; }
			public int TotalStudentsFeePaid { get; }
			public int PaymentReceived { get; }
			public int? DepartmentID { get; }
			public string DepartmentName { get; }
			public int ProgramID { get; }
			public int DefaulterAmount { get; }
			public List<Detail> Details { get; }
		}

		public sealed class SummaryRow
		{
			public int? DepartmentID { get; internal set; }
			public string DepartmentName { get; internal set; }
			public int ProgramID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public int DepartmentWiseTotalStudents { get; internal set; }
			public int DepartmentWiseTotalAmount { get; internal set; }
			public int DepartmentWiseConcessionAmount { get; internal set; }
			public int DepartmentWiseGrandTotalAmount { get; internal set; }
			public int DepartmentWisePaymentReceived { get; internal set; }
			public int DepartmentWiseTotalStudentsFeePaid { get; internal set; }
			public int DepartmentWiseDefaulterAmount { get; internal set; }
			public int ProgramWiseTotalStudents { get; internal set; }
			public int ProgramWiseTotalAmount { get; internal set; }
			public int ProgramWiseConcessionAmount { get; internal set; }
			public int ProgramWiseGrandTotalAmount { get; internal set; }
			public int ProgramWisePaymentReceived { get; internal set; }
			public int ProgramWiseTotalStudentsFeePaid { get; internal set; }
			public int ProgramWiseDefaulterAmount { get; internal set; }
			public int FeeHeadID { get; internal set; }
			public string HeadName { get; internal set; }
			public string HeadNameString => this.RuleTypeEnum == FeeHead.RuleTypes.Credit
				? $"+{this.HeadName}"
				: $"-{this.HeadName}";
			public int DisplayIndex { get; internal set; }
			public FeeHead.RuleTypes RuleTypeEnum { get; internal set; }
			public int HeadTotalAmount { get; internal set; }
			public int HeadConcessionAmount { get; internal set; }
			public int HeadGrandTotalAmount { get; internal set; }
			public int ReportWiseTotalStudents { get; internal set; }
			public int ReportWiseTotalAmount { get; internal set; }
			public int ReportWiseConcessionAmount { get; internal set; }
			public int ReportWiseGrandTotalAmount { get; internal set; }
			public int ReportWisePaymentReceived { get; internal set; }
			public int ReportWiseTotalStudentsFeePaid { get; internal set; }
			public int ReportWiseDefaulterAmount { get; internal set; }

			public List<SummaryRow> GetList()
			{
				return null;
			}
		}

		public sealed class PageHeader
		{
			public string InstituteName { get; internal set; }
			public string Filters { get; internal set; }
			public string ReportTitle => $"Program-wise Summary";

			public List<PageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataset
		{
			public PageHeader PageHeader { get; internal set; }
			public List<SummaryRow> SummaryRows { get; }

			internal ReportDataset(List<Program> programs)
			{
				this.SummaryRows = programs
					.GroupBy(p => new
					{
						p.DepartmentID,
						p.DepartmentName
					})
					.Select(d => new
					{
						Programs = d.Select(p => new
						{
							SummaryRows = p.Details.Select(dd => new SummaryRow
							{
								DepartmentID = d.Key.DepartmentID,
								DepartmentName = d.Key.DepartmentName,
								ReportWiseTotalStudents = programs.Sum(x => x.TotalStudents),
								ReportWiseTotalAmount = programs.Sum(x => x.TotalAmount),
								ReportWiseConcessionAmount = programs.Sum(x => x.ConcessionAmount),
								ReportWiseGrandTotalAmount = programs.Sum(x => x.GrandTotalAmount),
								ReportWisePaymentReceived = programs.Sum(x => x.PaymentReceived),
								ReportWiseTotalStudentsFeePaid = programs.Sum(x => x.TotalStudentsFeePaid),
								ReportWiseDefaulterAmount = programs.Sum(x => x.DefaulterAmount),
								DepartmentWiseTotalStudents = d.Sum(x => x.TotalStudents),
								DepartmentWiseTotalAmount = d.Sum(x => x.TotalAmount),
								DepartmentWiseConcessionAmount = d.Sum(x => x.ConcessionAmount),
								DepartmentWiseGrandTotalAmount = d.Sum(x => x.GrandTotalAmount),
								DepartmentWisePaymentReceived = d.Sum(x => x.PaymentReceived),
								DepartmentWiseTotalStudentsFeePaid = d.Sum(x => x.TotalStudentsFeePaid),
								DepartmentWiseDefaulterAmount = d.Sum(x => x.DefaulterAmount),
								ProgramID = p.ProgramID,
								ProgramAlias = p.ProgramAlias,
								ProgramWiseTotalStudents = p.TotalStudents,
								ProgramWiseTotalAmount = p.TotalAmount,
								ProgramWiseConcessionAmount = p.ConcessionAmount,
								ProgramWiseGrandTotalAmount = p.GrandTotalAmount,
								ProgramWisePaymentReceived = p.PaymentReceived,
								ProgramWiseTotalStudentsFeePaid = p.TotalStudentsFeePaid,
								ProgramWiseDefaulterAmount = p.DefaulterAmount,
								RuleTypeEnum = dd.FeeHead.RuleTypeEnum,
								HeadName = dd.FeeHead.HeadName,
								DisplayIndex = dd.FeeHead.DisplayIndex,
								FeeHeadID = dd.FeeHead.FeeHeadID,
								HeadTotalAmount = dd.TotalAmount,
								HeadConcessionAmount = dd.ConcessionAmount,
								HeadGrandTotalAmount = dd.GrandTotalAmount,
							}),
						}),
					}).SelectMany(d => d.Programs.SelectMany(p => p.SummaryRows)).ToList();
			}
		}

		public static ReportDataset GetProgramWiseFeeSummary(short semesterID, int? departmentID, int? programID, bool? newAdmission, DateTime? depositDateFrom, DateTime? depositDateTo, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var studentFeesQuery = aspireContext.StudentFees
					.Where(sf => sf.SemesterID == semesterID && sf.FeeType == StudentFee.FeeTypeChallan);
				if (depositDateFrom != null)
					studentFeesQuery = studentFeesQuery.Where(sf => sf.StudentFeeChallans.Any(sfc => sfc.AccountTransactionID != null && depositDateFrom.Value <= sfc.AccountTransaction.TransactionDate));
				if (depositDateTo != null)
				{
					var toDate = depositDateTo.Value.Date.AddDays(1);
					studentFeesQuery = studentFeesQuery.Where(sf => sf.StudentFeeChallans.Any(sfc => sfc.AccountTransactionID != null && sfc.AccountTransaction.TransactionDate < toDate));
				}
				if (newAdmission != null)
					studentFeesQuery = studentFeesQuery.Where(sf => sf.NewAdmission == newAdmission.Value);

				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms.Where(aop => aop.Program.InstituteID == loginHistory.InstituteID);
				if (departmentID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.Program.DepartmentID == departmentID.Value);
				if (programID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.ProgramID == programID.Value);

				var studentsQuery = admissionOpenPrograms.SelectMany(aop => aop.Students)
					.Select(s => new
					{
						s.StudentID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.Program.Department.DepartmentName,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgram.Program.ProgramAlias,
					});

				var candidateAppliedProgramsQuery = from cap in aspireContext.CandidateAppliedPrograms
													join aop in admissionOpenPrograms on cap.AdmittedAdmissionOpenProgramID equals aop.AdmissionOpenProgramID
													select new
													{
														cap.CandidateAppliedProgramID,
														aop.Program.DepartmentID,
														aop.Program.Department.DepartmentName,
														aop.ProgramID,
														aop.Program.ProgramAlias,
													};

				studentFeesQuery = studentFeesQuery
					.Include(sf => sf.StudentFeeChallans)
					.Include(sf => sf.StudentFeeDetails.Select(sfd => sfd.FeeHead))
					.Where(sf => studentsQuery.Any(s => s.StudentID == sf.StudentID)
								 || candidateAppliedProgramsQuery.Any(cap => cap.CandidateAppliedProgramID == sf.CandidateAppliedProgramID));

				var studentFees = studentFeesQuery.ToList();
				var students = studentsQuery.ToList();
				var candidateAppliedPrograms = candidateAppliedProgramsQuery.ToList();

				var studentFeesResult = from sf in studentFees
										join s in students on sf.StudentID equals s.StudentID
										where sf.StudentID != null
										select new
										{
											StudentFee = sf,
											s.DepartmentID,
											s.DepartmentName,
											s.ProgramID,
											s.ProgramAlias,
										};
				var candidateFeesResult = from sf in studentFees
										  join cap in candidateAppliedPrograms on sf.CandidateAppliedProgramID equals cap.CandidateAppliedProgramID
										  where sf.StudentID == null
										  select new
										  {
											  StudentFee = sf,
											  cap.DepartmentID,
											  cap.DepartmentName,
											  cap.ProgramID,
											  cap.ProgramAlias,
										  };

				var programs = studentFeesResult.Union(candidateFeesResult).GroupBy(sf => new
				{
					sf.DepartmentID,
					sf.DepartmentName,
					sf.ProgramID,
					sf.ProgramAlias,
				}).Select(p => new Program(p.Key.DepartmentID, p.Key.DepartmentName, p.Key.ProgramID, p.Key.ProgramAlias, p.Select(x => x.StudentFee).ToList())).ToList();

				var filters = new Dictionary<string, string>
				{
					{"Semester = ", semesterID.ToSemesterString()},
				};
				if (departmentID != null)
					filters.Add("Department = ", aspireContext.Departments.Where(d => d.DepartmentID == departmentID.Value).Select(d => d.DepartmentAlias).Single());
				if (programID != null)
					filters.Add("Program = ", aspireContext.Programs.Where(p => p.ProgramID == programID.Value).Select(p => p.ProgramAlias).Single());
				if (newAdmission != null)
					filters.Add("New Admission = ", newAdmission?.ToYesNo());
				if (depositDateFrom != null)
					filters.Add("Deposit Date >= ", depositDateFrom.Value.ToShortDateString());
				if (depositDateTo != null)
					filters.Add("Deposit Date <= ", depositDateTo.Value.ToShortDateString());

				return new ReportDataset(programs)
				{
					PageHeader = new PageHeader
					{
						InstituteName = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single(),
						Filters = string.Join(", ", filters.Select(f => $"{f.Key}{f.Value}")),
					}
				};
			}
		}
	}
}
