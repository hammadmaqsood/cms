﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.FeeManagement.Staff.Reports
{
	public static class DetailedScroll
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public ReportHeader ReportHeader { get; internal set; }
			public List<FeeChallan> FeeChallans { get; internal set; }
		}

		public sealed class ReportHeader
		{
			public string InstituteName { get; set; }
			public string ReportName { get; internal set; }
			public string Semester { get; internal set; }
			public string Filters { get; internal set; }

			public List<ReportHeader> GetList()
			{
				return null;
			}
		}

		public sealed class FeeChallan
		{
			public int StudentFeeID { get; internal set; }
			internal int? ApplicationNo { get; set; }
			internal string Enrollment { get; set; }
			public string EnrollmentOrApplicationNo => this.Enrollment ?? this.ApplicationNo.ToString();
			public string Name { get; internal set; }
			internal string ProgramAlias { get; set; }
			internal int? Section { get; set; }
			internal short? SemesterNo { get; set; }
			internal byte? Shift { get; set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public DateTime? DepositDate { get; internal set; }
			internal byte NoOfInstallments { get; set; }
			public int StudentFeeGrandTotalAmount { get; internal set; }

			public int StudentFeeChallanID { get; internal set; }
			public int StudentFeeChallanAmount { get; internal set; }
			public byte InstallmentNo { get; internal set; }

			public int? StudentFeeDetailID { get; internal set; }
			public int? FeeHeadID { get; internal set; }
			public int? FeeHeadDisplayIndex { get; internal set; }
			internal string HeadShortName { get; set; }
			public string DeposittedInAccountNo { get; internal set; }
			public string DeposittedInBankAlias { get; internal set; }
			public string HeadName
			{
				get
				{
					switch (this.RuleTypeEnum)
					{
						case FeeHead.RuleTypes.Credit:
							return $"+{this.HeadShortName}";
						case FeeHead.RuleTypes.Debit:
							return $"-{this.HeadShortName}";
						case null:
							return null;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}

			internal byte? RuleType { get; set; }
			public FeeHead.RuleTypes? RuleTypeEnum => (FeeHead.RuleTypes?)this.RuleType;
			public int? StudentFeeDetailGrandTotalAmount { get; internal set; }
			public int? StudentFeeDetailGrandTotalAmountCalculated
			{
				get
				{
					switch (this.RuleTypeEnum)
					{
						case FeeHead.RuleTypes.Credit:
							return +(this.StudentFeeDetailGrandTotalAmount ?? 0);
						case FeeHead.RuleTypes.Debit:
							return -(this.StudentFeeDetailGrandTotalAmount ?? 0);
						case null:
							return null;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}

			internal bool CanBeDisplayed(FeeReportParameters parameters)
			{
				return true;
			}

			public List<FeeChallan> GetList()
			{
				return null;
			}
		}

		internal sealed class Challan
		{
			public int StudentFeeID { get; internal set; }
			public short SemesterID { get; internal set; }
			public byte FeeType { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public byte NoOfInstallments { get; internal set; }
			public bool NewAdmission { get; internal set; }
			public string Remarks { get; internal set; }
			public int GrandTotalAmount { get; internal set; }
			public DateTime CreatedDate { get; internal set; }
			public byte Status { get; internal set; }
			public bool ExemptWHT { get; internal set; }
			public StudentInfo Student { get; internal set; }
			public CandidateInfo Candidate { get; internal set; }
			public List<StudentFeeDetail> StudentFeeDetails { get; internal set; }
			public List<StudentFeeChallanInfo> StudentFeeChallans { get; internal set; }
			public StudentFeeChallanInfo StudentFeeChallan => this.StudentFeeChallans.Single();
			public List<int> StudentFeeConcessionTypeIDs { get; internal set; }
			public string Name => this.Student?.Name ?? this.Candidate.Name;
			public string ProgramAlias => this.Student?.ProgramAlias ?? this.Candidate.ProgramAlias;
			public byte? Category => this.Student?.Category ?? this.Candidate.Category;
			public int? InstituteID => this.Student?.InstituteID ?? this.Candidate.InstituteID;
			public int? DepartmentID => this.Student?.DepartmentID ?? this.Candidate.DepartmentID;
			public int? ProgramID => this.Student?.ProgramID ?? this.Candidate.ProgramID;
			public short? SemesterNo => this.Student?.StudentSemester?.SemesterNo;
			public int? Section => this.Student?.StudentSemester?.Section;
			public byte? Shift => this.Student?.StudentSemester?.Shift;

			public sealed class StudentInfo
			{
				public string Enrollment { get; internal set; }
				public string Name { get; internal set; }
				public SemesterInfo StudentSemester { get; internal set; }
				public string ProgramAlias { get; internal set; }
				public int? InstituteID { get; internal set; }
				public int? DepartmentID { get; internal set; }
				public int? ProgramID { get; internal set; }
				public byte? Category { get; internal set; }

				public sealed class SemesterInfo
				{
					public short SemesterNo { get; internal set; }
					public int Section { get; internal set; }
					public byte Shift { get; internal set; }
				}
			}

			public sealed class CandidateInfo
			{
				public string Name { get; internal set; }
				public byte? Category { get; internal set; }
				public string ProgramAlias { get; internal set; }
				public int? InstituteID { get; internal set; }
				public int? ProgramID { get; internal set; }
				public int? DepartmentID { get; internal set; }
				public int? ApplicationNo { get; internal set; }
			}

			public sealed class StudentFeeDetail
			{
				public int FeeHeadID { get; internal set; }
				public string HeadShortName { get; internal set; }
				public int GrandTotalAmount { get; internal set; }
				public int StudentFeeDetailID { get; internal set; }
				public int FeeHeadDisplayIndex { get; internal set; }
				public byte RuleType { get; internal set; }
			}

			public sealed class StudentFeeChallanInfo
			{
				public int Amount { get; internal set; }
				public DateTime DueDate { get; internal set; }
				public byte Status { get; internal set; }
				public byte InstallmentNo { get; internal set; }
				public int StudentFeeChallanID { get; internal set; }
				public int? InstituteBankAccountID { get; internal set; }
				public byte? Service { get; internal set; }
				public byte? TransactionChannel { get; internal set; }
				public string BranchCode { get; internal set; }
				public string TransactionReferenceID { get; internal set; }
				public byte? PaymentType { get; internal set; }
				public string AccountTitle { get; internal set; }
				public string BranchName { get; internal set; }
				public DateTime? TransactionDate { get; internal set; }
				public int? DeposittedInBankID { get; internal set; }
				public string DeposittedInBankAlias { get; internal set; }
				public string DeposittedInAccountNo { get; internal set; }
			}

			public bool IsValid(FeeReportParameters parameters)
			{
				if (parameters.FeeType != this.FeeType)
					return false;
				if (parameters.NewAdmission != null && parameters.NewAdmission != this.NewAdmission)
					return false;
				if (parameters.SemesterID != null && parameters.SemesterID != this.SemesterID)
					return false;
				if (parameters.CreditHours != null)
					switch (parameters.CreditHoursOperator)
					{
						case ComparisonOperators.Equal:
							if (this.CreditHours != parameters.CreditHours)
								return false;
							break;
						case ComparisonOperators.GreaterThan:
							if (!(this.CreditHours > parameters.CreditHours))
								return false;
							break;
						case ComparisonOperators.GreaterThanEqual:
							if (!(this.CreditHours >= parameters.CreditHours))
								return false;
							break;
						case ComparisonOperators.LessThan:
							if (!(this.CreditHours < parameters.CreditHours))
								return false;
							break;
						case ComparisonOperators.LessThanEqual:
							if (!(this.CreditHours <= parameters.CreditHours))
								return false;
							break;
						case ComparisonOperators.NotEqual:
							if (this.CreditHours == parameters.CreditHours)
								return false;
							break;
						default:
							throw new NotImplementedEnumException(parameters.CreditHoursOperator);
					}
				if (parameters.StudentFeeStatus != null && this.Status != parameters.StudentFeeStatus)
					return false;
				if (parameters.CreatedFromDate != null && !(parameters.CreatedFromDate.Value.Date <= this.CreatedDate.Date))
					return false;
				if (parameters.CreatedToDate != null && !(this.CreatedDate.Date <= parameters.CreatedToDate.Value.Date))
					return false;
				if (parameters.ExemptWHT != null && parameters.ExemptWHT.Value != this.ExemptWHT)
					return false;
				if (parameters.FeeConcessionTypeIDs.Any())
				{
					if (!this.StudentFeeConcessionTypeIDs.Any())
						return false;
					switch (parameters.FeeConcessionTypeIDsLogicalOperator)
					{
						case LogicalOperators.And:
							if (!parameters.FeeConcessionTypeIDs.All(id => this.StudentFeeConcessionTypeIDs.Contains(id)))
								return false;
							break;
						case LogicalOperators.Or:
							if (!parameters.FeeConcessionTypeIDs.Any(id => this.StudentFeeConcessionTypeIDs.Contains(id)))
								return false;
							break;
						default:
							throw new NotImplementedEnumException(parameters.FeeConcessionTypeIDsLogicalOperator);
					}
				}
				if (!string.IsNullOrWhiteSpace(parameters.Remarks) && !this.Remarks.ToLower().Contains(parameters.Remarks.ToLower()))
					return false;

				if (parameters.Categories.Any() && !parameters.Categories.Cast<byte?>().Contains(this.Category))
					return false;

				if (parameters.InstituteID != this.InstituteID)
					return false;
				if (parameters.DepartmentID != null && parameters.DepartmentID != this.DepartmentID)
					return false;
				if (parameters.ProgramID != null && parameters.ProgramID != this.ProgramID)
					return false;

				if (parameters.SemesterNo != null && parameters.SemesterNo != this.SemesterNo)
					return false;
				if (parameters.Section != null && parameters.Section != this.Section)
					return false;
				if (parameters.Shift != null && parameters.Shift != this.Shift)
					return false;

				if (!this.StudentFeeDetails.Any(sfd => parameters.FeeHeadIDs.Contains(sfd.FeeHeadID)))
					return false;

				this.StudentFeeChallans = this.StudentFeeChallans.Where(sfc =>
				{
					if (parameters.Service != null && parameters.Service != sfc.Service)
						return false;
					if (parameters.FeeChallanStatus != null && parameters.FeeChallanStatus != sfc.Status)
						return false;
					if (parameters.TransactionDateFrom != null && !(sfc.TransactionDate != null && parameters.TransactionDateFrom.Value.Date <= sfc.TransactionDate.Value.Date))
						return false;
					if (parameters.TransactionDateTo != null && !(sfc.TransactionDate != null && sfc.TransactionDate.Value.Date <= parameters.TransactionDateTo.Value.Date))
						return false;
					if (parameters.DueFromDate != null && !(parameters.DueFromDate.Value.Date <= sfc.DueDate.Date))
						return false;
					if (parameters.DueToDate != null && !(sfc.DueDate.Date <= parameters.DueToDate.Value.Date))
						return false;
					if (parameters.InstituteBankAccountID != null && parameters.InstituteBankAccountID != sfc.InstituteBankAccountID)
						return false;
					return true;
				}).ToList();

				if (!this.StudentFeeChallans.Any())
					return false;

				return true;
			}

			public IEnumerable<FeeChallan> ToFeeChallans()
			{
				return this.StudentFeeDetails.Select(sfd => new FeeChallan
				{
					DepositDate = this.StudentFeeChallan.TransactionDate,
					ProgramAlias = this.ProgramAlias,
					Enrollment = this.Student?.Enrollment,
					Name = this.Name,
					ApplicationNo = this.Candidate?.ApplicationNo,
					Section = this.Section,
					SemesterNo = this.SemesterNo,
					Shift = this.Shift,
					StudentFeeID = this.StudentFeeID,
					NoOfInstallments = this.NoOfInstallments,
					StudentFeeChallanID = this.StudentFeeChallan.StudentFeeChallanID,
					FeeHeadID = sfd.FeeHeadID,
					InstallmentNo = this.StudentFeeChallan.InstallmentNo,
					HeadShortName = sfd.HeadShortName,
					FeeHeadDisplayIndex = sfd.FeeHeadDisplayIndex,
					RuleType = sfd.RuleType,
					StudentFeeChallanAmount = this.StudentFeeChallan.Amount,
					StudentFeeDetailID = sfd.StudentFeeDetailID,
					StudentFeeDetailGrandTotalAmount = sfd.GrandTotalAmount,
					StudentFeeGrandTotalAmount = this.GrandTotalAmount,
					DeposittedInBankAlias = this.StudentFeeChallan.DeposittedInBankAlias,
					DeposittedInAccountNo = this.StudentFeeChallan.DeposittedInAccountNo
				});
			}
		}

		public static ReportDataSet GetDetailedScroll(FeeReportParameters parameters, string filtersText, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.IncreaseCommandTimeout();
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				if (parameters.InstituteID != loginHistory.InstituteID)
					return null;
				var challans = aspireContext.StudentFees.AsNoTracking()
					.Where(sf => parameters.SemesterID == null || sf.SemesterID == parameters.SemesterID.Value)
					.Where(sf => sf.FeeType == parameters.FeeType)
					.Select(sf => new Challan
					{
						StudentFeeID = sf.StudentFeeID,
						SemesterID = sf.SemesterID,
						FeeType = sf.FeeType,
						CreditHours = sf.CreditHours,
						NoOfInstallments = sf.NoOfInstallments,
						NewAdmission = sf.NewAdmission,
						Remarks = sf.Remarks,
						GrandTotalAmount = sf.GrandTotalAmount,
						CreatedDate = sf.CreatedDate,
						Status = sf.Status,
						ExemptWHT = sf.ExemptWHT,
						Student = new Challan.StudentInfo
						{
							Enrollment = sf.Student.Enrollment,
							Name = sf.Student.Name,
							Category = sf.Student.Category,
							ProgramAlias = sf.Student.AdmissionOpenProgram.Program.ProgramAlias,
							InstituteID = sf.Student.AdmissionOpenProgram.Program.InstituteID,
							DepartmentID = sf.Student.AdmissionOpenProgram.Program.DepartmentID,
							ProgramID = sf.Student.AdmissionOpenProgram.Program.ProgramID,
							StudentSemester = sf.Student.StudentSemesters.Where(ss => ss.Status == StudentSemester.StatusRegistered && ss.SemesterID == parameters.SemesterID)
								.Select(ss => new Challan.StudentInfo.SemesterInfo
								{
									SemesterNo = ss.SemesterNo,
									Section = ss.Section,
									Shift = ss.Shift
								}).FirstOrDefault()
						},
						Candidate = new Challan.CandidateInfo
						{
							ApplicationNo = sf.CandidateAppliedProgram.ApplicationNo,
							Name = sf.CandidateAppliedProgram.Candidate.Name,
							Category = sf.CandidateAppliedProgram.Candidate.Category,
							ProgramAlias = sf.CandidateAppliedProgram.AdmissionOpenProgram.Program.ProgramAlias,
							InstituteID = sf.CandidateAppliedProgram.AdmissionOpenProgram.Program.InstituteID,
							DepartmentID = sf.CandidateAppliedProgram.AdmissionOpenProgram.Program.DepartmentID,
							ProgramID = sf.CandidateAppliedProgram.AdmissionOpenProgram.Program.ProgramID,
						},
						StudentFeeDetails = sf.StudentFeeDetails.Select(sfd => new Challan.StudentFeeDetail
						{
							FeeHeadID = sfd.FeeHead.FeeHeadID,
							HeadShortName = sfd.FeeHead.HeadShortName,
							GrandTotalAmount = sfd.GrandTotalAmount,
							StudentFeeDetailID = sfd.StudentFeeDetailID,
							FeeHeadDisplayIndex = sfd.FeeHead.DisplayIndex,
							RuleType = sfd.FeeHead.RuleType,
						}).ToList(),
						StudentFeeChallans = sf.StudentFeeChallans.Select(sfc => new Challan.StudentFeeChallanInfo
						{
							StudentFeeChallanID = sfc.StudentFeeChallanID,
							Amount = sfc.Amount,
							InstituteBankAccountID = sfc.AccountTransaction.InstituteBankAccountID,
							DeposittedInBankID = sfc.AccountTransaction.InstituteBankAccount.BankID,
							DeposittedInBankAlias = sfc.AccountTransaction.InstituteBankAccount.Bank.BankAlias,
							DeposittedInAccountNo = sfc.AccountTransaction.InstituteBankAccount.AccountNo,
							Service = sfc.AccountTransaction.Service,
							TransactionChannel = sfc.AccountTransaction.TransactionChannel,
							TransactionDate = sfc.AccountTransaction.TransactionDate,
							BranchCode = sfc.AccountTransaction.BranchCode,
							BranchName = sfc.AccountTransaction.BranchName,
							TransactionReferenceID = sfc.AccountTransaction.TransactionReferenceID,
							PaymentType = sfc.AccountTransaction.PaymentType,
							AccountTitle = sfc.AccountTransaction.AccountTitle,
							DueDate = sfc.DueDate,
							InstallmentNo = sfc.InstallmentNo,
							Status = sfc.Status,
						}).ToList(),
						StudentFeeConcessionTypeIDs = sf.StudentFeeConcessionTypes.Select(sfct => sfct.FeeConcessionTypeID).ToList(),
					})
					.ToList()
					.Where(c => c.IsValid(parameters))
					.SelectMany(c => c.ToFeeChallans())
					.ToList();

				return new ReportDataSet
				{
					FeeChallans = challans,
					ReportHeader = new ReportHeader
					{
						InstituteName = aspireContext.Institutes.Where(i => i.InstituteID == parameters.InstituteID).Select(i => i.InstituteName).Single(),
						Semester = parameters.SemesterID.ToSemesterString() ?? "All",
						ReportName = "Detailed Scroll",
						Filters = filtersText
					}
				};
			}
		}
	}
}
