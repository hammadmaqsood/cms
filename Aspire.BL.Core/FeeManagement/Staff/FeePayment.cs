﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FeeManagement.Common;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.FeeManagement.Staff
{
	public static class FeePayment
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandFeeModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandAdmissionModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandManageFeePaymentPermissions(this AspireContext aspireContext, int instituteID, int? departmentID, int programID, int admissionOpenProgramID, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.ManageFeePayment, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandManageFeePaymentOfLockedRecordsPermissions(this AspireContext aspireContext, int instituteID, int? departmentID, int programID, int admissionOpenProgramID, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.ManageFeePaymentOfLockedRecords, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandManageAdmissionProcessingFeePermissions(this AspireContext aspireContext, int instituteID, int? departmentID, int programID, int admissionOpenProgramID, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.ManageAdmissionProcessingFee, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandManageAdmissionProcessingFeeForLockedRecordsPermissions(this AspireContext aspireContext, int instituteID, int? departmentID, int programID, int admissionOpenProgramID, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.ManageAdmissionProcessingFeeForLockedRecords, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public static Helpers.RawFeeChallan GetRawFeeChallanDetails(AspireFormats.ChallanNo challanNo, out AccountTransaction accountTransaction, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var rawFeeChallan = aspireContext.GetRawFeeChallanDetails(challanNo, false);
				if (rawFeeChallan != null)
				{
					accountTransaction = rawFeeChallan.AccountTransactionID != null
						? aspireContext.AccountTransactions.Single(at => at.AccountTransactionID == rawFeeChallan.AccountTransactionID)
						: null;
					switch (challanNo.FeeTypeEnum)
					{
						case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
							var loginHistory = aspireContext.DemandAdmissionModulePermissions(loginSessionGuid);
							if (loginHistory.InstituteID == rawFeeChallan.InstituteID)
								return rawFeeChallan;
							break;
						case InstituteBankAccount.FeeTypes.NewAdmissionFee:
						case InstituteBankAccount.FeeTypes.StudentFee:
							loginHistory = aspireContext.DemandFeeModulePermissions(loginSessionGuid);
							if (loginHistory.InstituteID == rawFeeChallan.InstituteID)
								return rawFeeChallan;
							if (rawFeeChallan.Amount == 0 && rawFeeChallan.Paid && rawFeeChallan.AccountTransactionID == null)
								return null;
							break;
						default:
							throw new NotImplementedEnumException(challanNo.FeeTypeEnum);
					}
				}

				accountTransaction = null;
				return null;
			}
		}

		public enum DeleteAccountTransactionStatuses
		{
			NoRecordFound,
			ChallanIsNotPaid,
			CannotDeleteVerifiedAccountTransaction,
			Success,
		}

		private static (bool recordFound, int? accountTransactionID) CanManageFeePermissions(this AspireContext aspireContext, AspireFormats.ChallanNo challanNo, Guid loginSessionGuid)
		{
			switch (challanNo.FeeTypeEnum)
			{
				case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
					var candidateAppliedProgram = aspireContext.CandidateAppliedPrograms
						.Where(cap => cap.ChallanNo == challanNo.ChallanID)
						.Select(cap => new
						{
							cap.AccountTransactionID,
							AccountTransactionVerified = (bool?)cap.AccountTransaction.Verified,
							cap.Choice1AdmissionOpenProgramID,
							cap.AdmissionOpenProgram1.Program.Institute.InstituteCode,
							cap.AdmissionOpenProgram1.Program.InstituteID,
							cap.AdmissionOpenProgram1.Program.DepartmentID,
							cap.AdmissionOpenProgram1.Program.ProgramID,
							cap.AdmissionOpenProgram1.AdmissionOpenProgramID,
						})
						.SingleOrDefault();
					if (candidateAppliedProgram == null)
						return (false, null);
					aspireContext.DemandManageAdmissionProcessingFeePermissions(candidateAppliedProgram.InstituteID, candidateAppliedProgram.DepartmentID, candidateAppliedProgram.ProgramID, candidateAppliedProgram.AdmissionOpenProgramID, loginSessionGuid);
					if (candidateAppliedProgram.AccountTransactionID != null && candidateAppliedProgram.AccountTransactionVerified == true)
						aspireContext.DemandManageAdmissionProcessingFeeForLockedRecordsPermissions(candidateAppliedProgram.InstituteID, candidateAppliedProgram.DepartmentID, candidateAppliedProgram.ProgramID, candidateAppliedProgram.AdmissionOpenProgramID, loginSessionGuid);
					return (true, candidateAppliedProgram.AccountTransactionID);
				case InstituteBankAccount.FeeTypes.NewAdmissionFee:
				case InstituteBankAccount.FeeTypes.StudentFee:
					var studentFeeChallan = aspireContext.StudentFeeChallans
						.Where(sfc => sfc.StudentFeeChallanID == challanNo.ChallanID)
						.Select(sfc => new
						{
							sfc.AccountTransactionID,
							AccountTransactionVerified = (bool?)sfc.AccountTransaction.Verified,
							sfc.StudentFeeChallanID,
							sfc.StudentFee.StudentID,
							sfc.StudentFee.CandidateAppliedProgramID,
						}).SingleOrDefault();
					if (studentFeeChallan == null)
						return (false, null);
					if (studentFeeChallan.StudentID != null)
					{
						var student = aspireContext.Students.Where(s => s.StudentID == studentFeeChallan.StudentID.Value)
							.Select(s => new
							{
								s.AdmissionOpenProgram.Program.InstituteID,
								s.AdmissionOpenProgram.Program.DepartmentID,
								s.AdmissionOpenProgram.ProgramID,
								s.AdmissionOpenProgram.AdmissionOpenProgramID,
							}).Single();
						aspireContext.DemandManageFeePaymentPermissions(student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID, loginSessionGuid);
						if (studentFeeChallan.AccountTransactionID != null && studentFeeChallan.AccountTransactionVerified == true)
							aspireContext.DemandManageFeePaymentOfLockedRecordsPermissions(student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID, loginSessionGuid);
					}
					else if (studentFeeChallan.CandidateAppliedProgramID != null)
					{
						var candidateAppliedProgram1 = aspireContext.CandidateAppliedPrograms.Where(cap => cap.CandidateAppliedProgramID == studentFeeChallan.CandidateAppliedProgramID.Value)
							.Select(cap => new
							{
								cap.AdmissionOpenProgram.Program.InstituteID,
								cap.AdmissionOpenProgram.Program.DepartmentID,
								cap.AdmissionOpenProgram.ProgramID,
								cap.AdmissionOpenProgram.AdmissionOpenProgramID,
							}).Single();
						aspireContext.DemandManageFeePaymentPermissions(candidateAppliedProgram1.InstituteID, candidateAppliedProgram1.DepartmentID, candidateAppliedProgram1.ProgramID, candidateAppliedProgram1.AdmissionOpenProgramID, loginSessionGuid);
						if (studentFeeChallan.AccountTransactionID != null && studentFeeChallan.AccountTransactionVerified == true)
							aspireContext.DemandManageFeePaymentOfLockedRecordsPermissions(candidateAppliedProgram1.InstituteID, candidateAppliedProgram1.DepartmentID, candidateAppliedProgram1.ProgramID, candidateAppliedProgram1.AdmissionOpenProgramID, loginSessionGuid);
					}
					else
						throw new InvalidOperationException();

					return (true, studentFeeChallan.AccountTransactionID);
				default:
					throw new NotImplementedEnumException(challanNo.FeeTypeEnum);
			}
		}

		public static DeleteAccountTransactionStatuses DeleteAccountTransaction(AspireFormats.ChallanNo challanNo, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var (recordFound, accountTransactionID) = aspireContext.CanManageFeePermissions(challanNo, loginSessionGuid);
				if (!recordFound || accountTransactionID == null)
					return DeleteAccountTransactionStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var accountTransaction = aspireContext.AccountTransactions.Single(at => at.AccountTransactionID == accountTransactionID.Value);
				switch (challanNo.FeeTypeEnum)
				{
					case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
						var candidateAppliedProgram = aspireContext.CandidateAppliedPrograms.Single(cap => cap.AccountTransactionID == accountTransaction.AccountTransactionID);
						candidateAppliedProgram.AccountTransactionID = null;
						candidateAppliedProgram.CBTSessionLabID = null;
						aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
						aspireContext.AccountTransactions.Remove(accountTransaction);
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return DeleteAccountTransactionStatuses.Success;
					case InstituteBankAccount.FeeTypes.NewAdmissionFee:
					case InstituteBankAccount.FeeTypes.StudentFee:
						var studentFeeChallan = aspireContext.StudentFeeChallans
							.Include(sfc => sfc.StudentFee)
							.Single(sfc => sfc.AccountTransactionID == accountTransaction.AccountTransactionID);
						studentFeeChallan.AccountTransactionID = null;
						studentFeeChallan.StatusEnum = StudentFeeChallan.Statuses.NotPaid;
						switch (studentFeeChallan.StudentFee.StatusEnum)
						{
							case StudentFee.Statuses.Paid:
								studentFeeChallan.StudentFee.StatusEnum = StudentFee.Statuses.NotPaid;
								break;
							case StudentFee.Statuses.NotPaid:
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}
						aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
						aspireContext.AccountTransactions.Remove(accountTransaction);
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return DeleteAccountTransactionStatuses.Success;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		public static List<CustomListItem> GetInstituteBankAccounts(AccountTransaction.Services serviceEnum, InstituteBankAccount.FeeTypes feeTypeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var instituteBankAccounts = aspireContext.InstituteBankAccounts
					.Where(iba => iba.InstituteID == loginHistory.InstituteID)
					.Where(iba => iba.FeeType == (byte)feeTypeEnum);
				switch (serviceEnum)
				{
					case AccountTransaction.Services.Manual:
						instituteBankAccounts = instituteBankAccounts
							.Where(iba => iba.AccountType == (byte)InstituteBankAccount.AccountTypes.Manual || iba.AccountType == (byte)InstituteBankAccount.AccountTypes.ManualOnline);
						break;
					case AccountTransaction.Services.BankAlfalah:
						instituteBankAccounts = instituteBankAccounts
							.Where(iba => iba.Active)
							.Where(iba => iba.Bank.BankName == AspireConstants.BankAlfalahName)
							.Where(iba => iba.AccountType == (byte)InstituteBankAccount.AccountTypes.Online || iba.AccountType == (byte)InstituteBankAccount.AccountTypes.ManualOnline);
						break;
					case AccountTransaction.Services.AlliedBank:
						instituteBankAccounts = instituteBankAccounts
							.Where(iba => iba.Active)
							.Where(iba => iba.Bank.BankName == AspireConstants.AlliedBankName)
							.Where(iba => iba.AccountType == (byte)InstituteBankAccount.AccountTypes.Online || iba.AccountType == (byte)InstituteBankAccount.AccountTypes.ManualOnline);
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(serviceEnum), serviceEnum, null);
				}

				return instituteBankAccounts
					.Select(iba => new
					{
						iba.InstituteBankAccountID,
						iba.AccountNo,
						iba.AccountTitle,
						iba.Bank.BankName,
					})
					.ToList()
					.Select(iba => new CustomListItem
					{
						ID = iba.InstituteBankAccountID,
						Text = $"{iba.BankName} - {iba.AccountNo} - {iba.AccountTitle}",
					})
					.OrderBy(i => i.Text)
					.ToList();
			}
		}

		public enum PayFeeStatuses
		{
			NoRecordFound,
			AlreadyPaid,
			CannotUpdateVerifiedAccountTransaction,
			Added,
			Updated,
		}

		public static PayFeeStatuses PayFee(AspireFormats.ChallanNo challanNo, int? accountTransactionID, int amount, AccountTransaction.Services serviceEnum, AccountTransaction.TransactionChannels transactionChannelEnum, AccountTransaction.PaymentTypes paymentTypeEnum, int instituteBankAccountID, string transactionReferenceID, DateTime transactionDate, string accountNo, string accountTitle, string branchCode, string branchName, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var (recordFound, aTransactionID) = aspireContext.CanManageFeePermissions(challanNo, loginSessionGuid);
				if (recordFound == false)
					return PayFeeStatuses.NoRecordFound;
				if (accountTransactionID == null)
				{
					if (aTransactionID != null)
						return PayFeeStatuses.AlreadyPaid;
					var accountTransaction = Helpers.CreateAccountTransaction(serviceEnum, transactionChannelEnum, paymentTypeEnum, challanNo, amount, transactionReferenceID, accountNo, accountTitle, branchCode, branchName, transactionDate, instituteBankAccountID, remarks, false, loginHistory.UserLoginHistoryID);
					var payFailureReason = aspireContext.PayFee(accountTransaction, true, false);
					switch (payFailureReason)
					{
						case PaymentFailureReasons.None:
							aspireContext.CommitTransaction();
							return PayFeeStatuses.Added;
						case PaymentFailureReasons.ChallanNotFound:
							aspireContext.RollbackTransaction();
							return PayFeeStatuses.NoRecordFound;
						case PaymentFailureReasons.FeeChallanAlreadyPaid:
							aspireContext.RollbackTransaction();
							return PayFeeStatuses.AlreadyPaid;
						case PaymentFailureReasons.InvalidChallanNoFormat:
						case PaymentFailureReasons.DueDateIsPassed:
						case PaymentFailureReasons.AmountMismatched:
						case PaymentFailureReasons.AmountMustBeGreaterThanZero:
						case PaymentFailureReasons.InstituteBankAccountNotFound:
							aspireContext.RollbackTransaction();
							throw new InvalidOperationException();
						default:
							throw new NotImplementedEnumException(payFailureReason);
					}
				}
				else
				{
					if (accountTransactionID.Value != aTransactionID)
						return PayFeeStatuses.NoRecordFound;
					var accountTransaction = aspireContext.AccountTransactions.Single(at => at.AccountTransactionID == accountTransactionID.Value);
					if (accountTransaction.Amount != amount)
						throw new InvalidOperationException();
					accountTransaction.ServiceEnum = serviceEnum;
					accountTransaction.TransactionChannelEnum = transactionChannelEnum;
					accountTransaction.PaymentTypeEnum = paymentTypeEnum;
					accountTransaction.InstituteBankAccountID = instituteBankAccountID;
					accountTransaction.TransactionReferenceID = transactionReferenceID;
					accountTransaction.TransactionDate = transactionDate;
					accountTransaction.AccountNo = accountNo;
					accountTransaction.AccountTitle = accountTitle;
					accountTransaction.BranchCode = branchCode;
					accountTransaction.BranchName = branchName;
					accountTransaction.Remarks = remarks;
					accountTransaction.UserLoginHistoryID = loginHistory.UserLoginHistoryID;
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
					return PayFeeStatuses.Updated;
				}
			}
		}

		public enum SwapChallanNosStatuses
		{
			NoRecordFound,
			NotImplementedFeature,
			CandidateIDNotSame,
			BothChallansAreSame,
			BothChallansArePaid,
			BothChallansAreNotPaid,
			AmountsAreNotSame,
			InstitutesMustBeSameDueToBankAccounts,
			Success,
		}

		private static SwapChallanNosStatuses SwapAdmissionProcessingFeeChallanNos(AspireFormats.ChallanNo challanNo1, AspireFormats.ChallanNo challanNo2, Guid loginSessionGuid)
		{
			if (challanNo1.FeeTypeEnum != InstituteBankAccount.FeeTypes.AdmissionProcessingFee || challanNo2.FeeTypeEnum != InstituteBankAccount.FeeTypes.AdmissionProcessingFee)
				return SwapChallanNosStatuses.NoRecordFound;
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.Admissions.SwapAdmissionProcessingFeeChallanNos, UserGroupPermission.PermissionValues.Allowed);
				var record1 = aspireContext.CandidateAppliedPrograms
					.Where(cap => cap.ChallanNo == challanNo1.ChallanID && cap.AdmissionOpenProgram1.Program.Institute.InstituteCode == challanNo1.InstituteCode)
					.Select(cap => new
					{
						CandidateAppliedProgram = cap,
						cap.AccountTransaction,
						cap.AdmissionOpenProgram1.Program.Institute.InstituteID,
					})
					.SingleOrDefault();
				var record2 = aspireContext.CandidateAppliedPrograms
					.Where(cap => cap.ChallanNo == challanNo2.ChallanID && cap.AdmissionOpenProgram1.Program.Institute.InstituteCode == challanNo2.InstituteCode)
					.Select(cap => new
					{
						CandidateAppliedProgram = cap,
						cap.AccountTransaction,
						cap.AdmissionOpenProgram1.Program.Institute.InstituteID,
					})
					.SingleOrDefault();
				if (record1 == null || record2 == null)
					return SwapChallanNosStatuses.NoRecordFound;
				if (record1.CandidateAppliedProgram.CandidateID != record2.CandidateAppliedProgram.CandidateID)
					return SwapChallanNosStatuses.CandidateIDNotSame;
				if (record1.CandidateAppliedProgram.CandidateAppliedProgramID == record2.CandidateAppliedProgram.CandidateAppliedProgramID)
					return SwapChallanNosStatuses.BothChallansAreSame;
				if (record1.CandidateAppliedProgram.ChallanNo == record2.CandidateAppliedProgram.ChallanNo)
					return SwapChallanNosStatuses.BothChallansAreSame;
				if (challanNo1.InstituteCode != challanNo2.InstituteCode)
					return SwapChallanNosStatuses.InstitutesMustBeSameDueToBankAccounts;
				if (record1.CandidateAppliedProgram.Amount != record2.CandidateAppliedProgram.Amount)
					return SwapChallanNosStatuses.AmountsAreNotSame;
				if (record1.AccountTransaction != null && record1.AccountTransaction.Amount != record1.CandidateAppliedProgram.Amount)
					throw new InvalidOperationException($"Amounts are not same. {nameof(record1.CandidateAppliedProgram.CandidateAppliedProgramID)}: {record1.CandidateAppliedProgram.CandidateAppliedProgramID}.");
				if (record2.AccountTransaction != null && record2.AccountTransaction.Amount != record2.CandidateAppliedProgram.Amount)
					throw new InvalidOperationException($"Amounts are not same. {nameof(record2.CandidateAppliedProgram.CandidateAppliedProgramID)}: {record2.CandidateAppliedProgram.CandidateAppliedProgramID}.");
				if (record1.CandidateAppliedProgram.AccountTransactionID != null && record2.CandidateAppliedProgram.AccountTransactionID != null)
					return SwapChallanNosStatuses.BothChallansArePaid;
				if (record1.CandidateAppliedProgram.AccountTransactionID == null && record2.CandidateAppliedProgram.AccountTransactionID == null)
					return SwapChallanNosStatuses.BothChallansAreNotPaid;

				var remarks = $"\nChallan Nos swapped for CandidateAppliedProgramIDs: {record1.CandidateAppliedProgram.CandidateAppliedProgramID}, {record2.CandidateAppliedProgram.CandidateAppliedProgramID}. ChallanNos: {challanNo1.ToString()},{challanNo2.ToString()}";
				if (record1.CandidateAppliedProgram.AccountTransactionID != null && record2.CandidateAppliedProgram.AccountTransactionID == null)
				{
					record1.CandidateAppliedProgram.AccountTransactionID = null;
					record1.CandidateAppliedProgram.CBTSessionLabID = null;
					record2.CandidateAppliedProgram.AccountTransactionID = record1.AccountTransaction.AccountTransactionID;
					record2.CandidateAppliedProgram.CBTSessionLabID = null;
					if (record2.CandidateAppliedProgram.ApplicationNo == null)
						record2.CandidateAppliedProgram.ApplicationNo = aspireContext.GetNextApplicationNo(record2.CandidateAppliedProgram.Choice1AdmissionOpenProgramID);
					record1.AccountTransaction.Remarks += remarks;
				}
				else if (record1.CandidateAppliedProgram.AccountTransactionID == null && record2.CandidateAppliedProgram.AccountTransactionID != null)
				{
					record1.CandidateAppliedProgram.AccountTransactionID = record2.AccountTransaction.AccountTransactionID;
					record1.CandidateAppliedProgram.CBTSessionLabID = null;
					if (record1.CandidateAppliedProgram.ApplicationNo == null)
						record1.CandidateAppliedProgram.ApplicationNo = aspireContext.GetNextApplicationNo(record1.CandidateAppliedProgram.Choice1AdmissionOpenProgramID);
					record2.CandidateAppliedProgram.AccountTransactionID = null;
					record2.CandidateAppliedProgram.CBTSessionLabID = null;
					record2.AccountTransaction.Remarks += remarks;
				}
				else
					throw new InvalidOperationException();
				record1.CandidateAppliedProgram.ChallanNo = aspireContext.GetNextAdmissionProcessingFeeChallanNo().SingleOrDefault() ?? throw new InvalidOperationException("Error occurred while generating challan No.");
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				record2.CandidateAppliedProgram.ChallanNo = challanNo1.ChallanID;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				record1.CandidateAppliedProgram.ChallanNo = challanNo2.ChallanID;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return SwapChallanNosStatuses.Success;
			}
		}

		public static SwapChallanNosStatuses SwapChallanNos(AspireFormats.ChallanNo challanNo1, AspireFormats.ChallanNo challanNo2, Guid loginSessionGuid)
		{
			switch (challanNo1.FeeTypeEnum)
			{
				case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
					return SwapAdmissionProcessingFeeChallanNos(challanNo1, challanNo2, loginSessionGuid);
				case InstituteBankAccount.FeeTypes.NewAdmissionFee:
				case InstituteBankAccount.FeeTypes.StudentFee:
					return SwapChallanNosStatuses.NotImplementedFeature;
				default:
					throw new NotImplementedEnumException(challanNo1.FeeTypeEnum);
			}
		}
	}
}