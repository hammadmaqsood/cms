﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.FeeManagement.Staff
{
	public static class FeeConcessionTypes
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.ManageFeeConcessionTypes, UserGroupPermission.PermissionValues.Allowed);
		}

		public enum AddFeeConcessionTypeStatuses
		{
			NameAlreadyExists,
			Success,
		}

		public static AddFeeConcessionTypeStatuses AddFeeConcessionType(string concessionName, bool visible, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var feeConcessionType = new FeeConcessionType
				{
					InstituteID = loginHistory.InstituteID,
					ConcessionName = concessionName.TrimAndCannotBeEmpty(),
					Visible = visible,
				};
				if (aspireContext.FeeConcessionTypes.Any(fct => fct.ConcessionName == feeConcessionType.ConcessionName && fct.InstituteID == loginHistory.InstituteID))
					return AddFeeConcessionTypeStatuses.NameAlreadyExists;

				aspireContext.FeeConcessionTypes.Add(feeConcessionType);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddFeeConcessionTypeStatuses.Success;
			}
		}

		public enum UpdateFeeConcessionTypeStatuses
		{
			NoRecordFound,
			NameAlreadyExists,
			Success,
		}

		public static UpdateFeeConcessionTypeStatuses UpdateFeeConcessionType(int feeConcessionTypeID, string concessionName, bool visible, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var feeConcessionType = aspireContext.FeeConcessionTypes.SingleOrDefault(fct => fct.FeeConcessionTypeID == feeConcessionTypeID && fct.InstituteID == loginHistory.InstituteID);
				if (feeConcessionType == null)
					return UpdateFeeConcessionTypeStatuses.NoRecordFound;
				concessionName = concessionName.TrimAndCannotBeEmpty();
				if (aspireContext.FeeConcessionTypes.Any(fct => fct.FeeConcessionTypeID != feeConcessionType.FeeConcessionTypeID && fct.ConcessionName == concessionName && fct.InstituteID == feeConcessionType.InstituteID))
					return UpdateFeeConcessionTypeStatuses.NameAlreadyExists;
				feeConcessionType.ConcessionName = concessionName.TrimAndCannotBeEmpty();
				feeConcessionType.Visible = visible;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateFeeConcessionTypeStatuses.Success;
			}
		}

		public sealed class GetFeeConcessionsResult
		{
			internal GetFeeConcessionsResult()
			{
			}
			public int VirtualItemsCount { get; internal set; }
			public List<FeeConcessionType> FeeConcessionTypes { get; internal set; }
		}

		public static GetFeeConcessionsResult GetFeeConcessions(int pageIndex, int pageSize, string sortProperty, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = aspireContext.FeeConcessionTypes.Where(fct => fct.InstituteID == loginHistory.InstituteID);
				switch (sortProperty)
				{
					case nameof(FeeConcessionType.ConcessionName):
						query = query.OrderBy(sortDirection, c => c.ConcessionName);
						break;
					case nameof(FeeConcessionType.Visible):
						query = query.OrderBy(sortDirection, c => c.Visible);
						break;
					default:
						throw new SortingNotImplementedException(sortProperty);
				}
				return new GetFeeConcessionsResult
				{
					VirtualItemsCount = query.Count(),
					FeeConcessionTypes = query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
				};
			}
		}

		public static FeeConcessionType GetFeeConcessionType(int feeConcessionTypeID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				return aspireContext.FeeConcessionTypes.SingleOrDefault(fct => fct.FeeConcessionTypeID == feeConcessionTypeID && fct.InstituteID == loginHistory.InstituteID);
			}
		}

		public enum DeleteFeeConcessionTypeStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success
		}

		public static DeleteFeeConcessionTypeStatuses DeleteFeeConcessionType(int feeConcessionTypeID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var feeConcessionType = aspireContext.FeeConcessionTypes.FirstOrDefault(fct => fct.FeeConcessionTypeID == feeConcessionTypeID && fct.InstituteID == loginHistory.InstituteID);
				if (feeConcessionType == null)
					return DeleteFeeConcessionTypeStatuses.NoRecordFound;
				if (feeConcessionType.StudentFeeConcessionTypes.Any())
					return DeleteFeeConcessionTypeStatuses.ChildRecordExists;
				aspireContext.FeeConcessionTypes.Remove(feeConcessionType);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteFeeConcessionTypeStatuses.Success;
			}
		}

		public static List<CustomListItem> GetFeeConcessionTypesList(bool? visible, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var feeConcessionTypes = aspireContext.FeeConcessionTypes.Where(fct => fct.InstituteID == loginHistory.InstituteID);
				if (visible != null)
					feeConcessionTypes = feeConcessionTypes.Where(fct => fct.Visible == visible.Value);
				return feeConcessionTypes.Select(fct => new CustomListItem
				{
					ID = fct.FeeConcessionTypeID,
					Text = fct.ConcessionName,
				}).OrderBy(fct => fct.Text).ToList();
			}
		}
	}
}
