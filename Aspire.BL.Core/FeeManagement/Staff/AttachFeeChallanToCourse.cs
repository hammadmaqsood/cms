﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FeeManagement.Common;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.FeeManagement.Staff
{
	public static class AttachFeeChallanToCourse
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.ManageFeePayment, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class Student
		{
			internal Student() { }
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public short SemesterID { get; internal set; }
			public string IntakeSemester => this.SemesterID.ToSemesterString();
			public int RegisteredCourses { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public bool FeeChallanExists { get; internal set; }
			public string FeeChallanExistsYesNo => this.FeeChallanExists.ToYesNo();
			public bool CanSelect => this.FeeChallanExists && this.RegisteredCourses > 0;
		}

		public static List<Student> GetStudentNotPaidRegisteredCourses(short semesterID, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = aspireContext.StudentSemesters
					.Where(ss => ss.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(ss => ss.Student.AdmissionOpenProgram.SemesterID == semesterID)
					.Where(ss => ss.SemesterID == semesterID)
					.Select(ss => new Student
					{
						StudentID = ss.StudentID,
						Enrollment = ss.Student.Enrollment,
						Name = ss.Student.Name,
						ProgramAlias = ss.Student.AdmissionOpenProgram.Program.ProgramAlias,
						SemesterNo = ss.SemesterNo,
						Section = ss.Section,
						Shift = ss.Shift,
						SemesterID = ss.SemesterID,
						RegisteredCourses = ss.Student.RegisteredCourses.Count(rc => rc.OfferedCours.SemesterID == semesterID && rc.StudentFeeID == null && rc.DeletedDate == null),
						FeeChallanExists = ss.Student.StudentFees.Any(sf => sf.SemesterID == semesterID && sf.FeeType != StudentFee.FeeTypeRefund),
					}).Where(s => s.RegisteredCourses > 0);

				switch (sortExpression)
				{
					case nameof(Student.Enrollment):
						query = query.OrderBy(sortDirection, s => s.Enrollment);
						break;
					case nameof(Student.Name):
						query = query.OrderBy(sortDirection, s => s.Name);
						break;
					case nameof(Student.Class):
						query = query.OrderBy(sortDirection, s => s.ProgramAlias).ThenBy(sortDirection, s => s.SemesterNo).ThenBy(sortDirection, s => s.Section).ThenBy(sortDirection, s => s.Shift);
						break;
					case nameof(Student.FeeChallanExists):
						query = query.OrderBy(sortDirection, s => s.FeeChallanExists);
						break;
					case nameof(Student.RegisteredCourses):
						query = query.OrderBy(sortDirection, s => s.RegisteredCourses);
						break;
					case nameof(Student.SemesterID):
					case nameof(Student.IntakeSemester):
						query = query.OrderBy(sortDirection, s => s.SemesterID);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return query.ToList();
			}
		}

		public enum AttachStudentFeeIDToFirstSemesterRegisteredCoursesStatuses
		{
			Success,
			NoRecordFound,
		}

		public static AttachStudentFeeIDToFirstSemesterRegisteredCoursesStatuses AttachStudentFeeIDToFirstSemesterRegisteredCourses(short semesterID, List<int> studentIDs, Guid loginSessionGuid)
		{
			foreach (var studentID in studentIDs)
			{
				var result = AttachStudentFeeIDToFirstSemesterRegisteredCourses(semesterID, studentID, loginSessionGuid);
				switch (result)
				{
					case AttachStudentFeeIDToFirstSemesterRegisteredCoursesStatuses.Success:
						break;
					case AttachStudentFeeIDToFirstSemesterRegisteredCoursesStatuses.NoRecordFound:
						return result;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			return AttachStudentFeeIDToFirstSemesterRegisteredCoursesStatuses.Success;
		}

		private static AttachStudentFeeIDToFirstSemesterRegisteredCoursesStatuses AttachStudentFeeIDToFirstSemesterRegisteredCourses(short semesterID, int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.Students.Where(s => s.StudentID == studentID && s.AdmissionOpenProgram.SemesterID == semesterID)
					.Select(s => new
					{
						s.StudentID,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgramID,
						RegisteredCourses = s.RegisteredCourses.Where(rc => rc.OfferedCours.SemesterID == semesterID && rc.StudentFeeID == null).ToList(),
						StudentFees = s.StudentFees.Where(sf => sf.SemesterID == semesterID && sf.FeeType != StudentFee.FeeTypeRefund).Select(sf => new
						{
							sf.StudentFeeID,
							sf.FeeType,
							sf.Status,
						}).ToList(),
					}).SingleOrDefault();
				if (student == null || !student.RegisteredCourses.Any() || !student.StudentFees.Any())
					return AttachStudentFeeIDToFirstSemesterRegisteredCoursesStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);

				var studentFeesFeeChallan = student.StudentFees.Where(sf => sf.FeeType == StudentFee.FeeTypeChallan);
				var studentFee = studentFeesFeeChallan.FirstOrDefault(sf => sf.Status == StudentFee.StatusPaid)
								 ?? studentFeesFeeChallan.FirstOrDefault(sf => sf.Status == StudentFee.StatusNotPaid)
								 ?? student.StudentFees.FirstOrDefault();
				if (studentFee == null)
					return AttachStudentFeeIDToFirstSemesterRegisteredCoursesStatuses.NoRecordFound;
				foreach (var registeredCourse in student.RegisteredCourses)
					registeredCourse.StudentFeeID = studentFee.StudentFeeID;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ProcessIndividualFeeDefaulter(student.StudentID, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return AttachStudentFeeIDToFirstSemesterRegisteredCoursesStatuses.Success;
			}
		}

		public enum AttachRegisteredCourseToStudentFeeStatuses
		{
			NoRecordFound,
			AlreadyAttached,
			Success,
		}

		public static AttachRegisteredCourseToStudentFeeStatuses AttachRegisteredCourseToStudentFee(int registeredCourseID, int studentFeeID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.RegisteredCourses
					.Where(rc => rc.RegisteredCourseID == registeredCourseID && rc.DeletedDate == null)
					.Select(rc => new
					{
						RegisteredCourse = rc,
						rc.OfferedCours.SemesterID,
						rc.Student.AdmissionOpenProgram.Program.InstituteID,
						rc.Student.AdmissionOpenProgram.Program.DepartmentID,
						rc.Student.AdmissionOpenProgram.ProgramID,
						rc.Student.AdmissionOpenProgramID,
					})
					.SingleOrDefault();
				if (record == null)
					return AttachRegisteredCourseToStudentFeeStatuses.NoRecordFound;
				if (record.RegisteredCourse.StudentFeeID != null)
					return AttachRegisteredCourseToStudentFeeStatuses.AlreadyAttached;
				var _studentFeeID = aspireContext.StudentFees
					.Where(sf => sf.StudentFeeID == studentFeeID && sf.SemesterID == record.SemesterID)
					.Select(sf => (int?)sf.StudentFeeID)
					.SingleOrDefault();
				if (_studentFeeID == null)
					return AttachRegisteredCourseToStudentFeeStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
				record.RegisteredCourse.StudentFeeID = _studentFeeID;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ProcessIndividualFeeDefaulter(record.RegisteredCourse.StudentID, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return AttachRegisteredCourseToStudentFeeStatuses.Success;
			}
		}

		public enum DetachRegisteredCourseToStudentFeeStatuses
		{
			NoRecordFound,
			AlreadyDetached,
			Success,
		}

		public static DetachRegisteredCourseToStudentFeeStatuses DetachRegisteredCourseToStudentFee(int registeredCourseID, int studentFeeID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.RegisteredCourses
					.Where(rc => rc.RegisteredCourseID == registeredCourseID && rc.DeletedDate == null)
					.Select(rc => new
					{
						RegisteredCourse = rc,
						rc.Student.AdmissionOpenProgram.Program.InstituteID,
						rc.Student.AdmissionOpenProgram.Program.DepartmentID,
						rc.Student.AdmissionOpenProgram.ProgramID,
						rc.Student.AdmissionOpenProgramID,
					})
					.SingleOrDefault();
				if (record == null)
					return DetachRegisteredCourseToStudentFeeStatuses.NoRecordFound;
				if (record.RegisteredCourse.StudentFeeID == null)
					return DetachRegisteredCourseToStudentFeeStatuses.AlreadyDetached;
				if (record.RegisteredCourse.StudentFeeID != studentFeeID)
					return DetachRegisteredCourseToStudentFeeStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);
				record.RegisteredCourse.StudentFeeID = null;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ProcessIndividualFeeDefaulter(record.RegisteredCourse.StudentID, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return DetachRegisteredCourseToStudentFeeStatuses.Success;
			}
		}

		public sealed class GetRegisteredCourseForFeeAttachmentResult
		{
			internal GetRegisteredCourseForFeeAttachmentResult() { }
			public int RegisteredCourseID { get; internal set; }
			public string Title { get; internal set; }
			public short OfferedSemesterID { get; internal set; }
			internal List<StudentFeeChallan> _studentFeeChallans { get; set; }
			public List<CustomListItem> StudentFeeChallans => this._studentFeeChallans.Select(sfc => new CustomListItem
			{
				ID = sfc.StudentFeeID,
				Text = $"{sfc.FullChallanNo} - Rs. {sfc.Amount}"
			}).ToList();
			public int StudentID { get; internal set; }

			internal sealed class StudentFeeChallan
			{
				public string InstituteCode { get; internal set; }
				public int StudentFeeID { get; internal set; }
				public int StudentFeeChallanID { get; internal set; }
				public bool NewAdmission { get; internal set; }
				public int Amount { get; internal set; }
				public string FullChallanNo => AspireFormats.ChallanNo.GetFullChallanNoString(this.InstituteCode, this.NewAdmission, this.StudentFeeChallanID);
			}
		}

		public static GetRegisteredCourseForFeeAttachmentResult GetRegisteredCourseForFeeAttachment(int registeredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var record = aspireContext.RegisteredCourses
					.Where(rc => rc.RegisteredCourseID == registeredCourseID && rc.DeletedDate == null && rc.StudentFeeID == null)
					.Where(rc => rc.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Select(rc => new GetRegisteredCourseForFeeAttachmentResult
					{
						RegisteredCourseID = rc.RegisteredCourseID,
						StudentID = rc.StudentID,
						OfferedSemesterID = rc.OfferedCours.SemesterID,
						Title = rc.Cours.Title,
					})
					.SingleOrDefault();
				if (record != null)
					record._studentFeeChallans = aspireContext.StudentFees
						.Where(sf => sf.StudentID == record.StudentID)
						.Where(sf => sf.SemesterID == record.OfferedSemesterID)
						.Where(sf => sf.FeeType == (byte)StudentFee.FeeTypes.Challan)
						.SelectMany(sf => sf.StudentFeeChallans)
						.Select(sfc => new GetRegisteredCourseForFeeAttachmentResult.StudentFeeChallan
						{
							InstituteCode = sfc.StudentFee.Student.AdmissionOpenProgram.Program.Institute.InstituteCode,
							NewAdmission = sfc.StudentFee.NewAdmission,
							StudentFeeID = sfc.StudentFeeID,
							StudentFeeChallanID = sfc.StudentFeeChallanID,
							Amount = sfc.Amount,
						}).ToList();
				return record;
			}
		}
	}
}