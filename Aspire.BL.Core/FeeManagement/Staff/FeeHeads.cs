﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.FeeManagement.Staff
{
	public static class FeeHeads
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public static List<FeeHead> GetFeeHeads(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.FeeHeads
					.Where(fh => fh.InstituteID == loginHistory.InstituteID)
					.OrderBy(fh => fh.DisplayIndex)
					.ToList();
			}
		}

		public static List<CustomListItem> GetFeeHeadsList(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.FeeHeads
					.Where(fh => fh.InstituteID == loginHistory.InstituteID)
					.OrderBy(fh => fh.DisplayIndex)
					.Select(fh => new CustomListItem
					{
						Text = fh.HeadAlias,
						ID = fh.FeeHeadID,
					}).ToList();
			}
		}
	}
}
