﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.EmailsManagement;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.FeeManagement.Staff
{
	public static class FeeStructures
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.FeeManagement.ManageFeeStructure, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public enum AddFeeStructureStatuses
		{
			AlreadyExists,
			Success,
			NoRecordFound
		}

		public static AddFeeStructureStatuses AddFeeStructure(int admissionOpenProgramID, byte creditHours, Guid loginSessionGuid)
		{
			lock (Locks.FeeStructureTransaction)
			{
				using (var aspireContext = new AspireContext(true))
				{
					var admissionOpenProgram = aspireContext.AdmissionOpenPrograms.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID).Select(aop => new
					{
						aop.Program.InstituteID,
						aop.Program.DepartmentID,
						aop.ProgramID,
						aop.AdmissionOpenProgramID,
						FeeStructureExists = aop.FeeStructures.Any(),
					}).SingleOrDefault();
					if (admissionOpenProgram == null)
						return AddFeeStructureStatuses.NoRecordFound;
					var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, admissionOpenProgram.InstituteID, admissionOpenProgram.DepartmentID, admissionOpenProgram.ProgramID, admissionOpenProgram.AdmissionOpenProgramID);
					if (admissionOpenProgram.FeeStructureExists)
						return AddFeeStructureStatuses.AlreadyExists;

					var feeStructure = new FeeStructure
					{
						AdmissionOpenProgramID = admissionOpenProgramID,
						FirstSemesterCreditHours = creditHours,
					};
					aspireContext.FeeStructures.Add(feeStructure);
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
					return AddFeeStructureStatuses.Success;
				}
			}
		}

		public enum EditFeeStructureStatuses
		{
			NoRecordFound,
			Success
		}

		public static EditFeeStructureStatuses EditFeeStructure(int feeStructureID, byte creditHours, Guid loginSessionGuid)
		{
			lock (Locks.FeeStructureTransaction)
			{
				using (var aspireContext = new AspireContext(true))
				{
					var admissionOpenProgram = aspireContext.FeeStructures.Where(fs => fs.FeeStructureID == feeStructureID).Select(fs => new
					{
						fs.AdmissionOpenProgram.Program.InstituteID,
						fs.AdmissionOpenProgram.Program.DepartmentID,
						fs.AdmissionOpenProgram.ProgramID,
						fs.AdmissionOpenProgram.AdmissionOpenProgramID,
						FeeStructure = fs,
					}).SingleOrDefault();
					if (admissionOpenProgram == null)
						return EditFeeStructureStatuses.NoRecordFound;
					var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, admissionOpenProgram.InstituteID, admissionOpenProgram.DepartmentID, admissionOpenProgram.ProgramID, admissionOpenProgram.AdmissionOpenProgramID);

					admissionOpenProgram.FeeStructure.FirstSemesterCreditHours = creditHours;
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
					return EditFeeStructureStatuses.Success;
				}
			}
		}

		public static FeeStructure GetFeeStructure(int admissionOpenProgramID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.FeeStructures.Include(fs => fs.FeeStructureDetails.Select(s => s.FeeHead))
					.Include(fs => fs.AdmissionOpenProgram.Program)
					.SingleOrDefault(fs => fs.AdmissionOpenProgramID == admissionOpenProgramID && fs.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
			}
		}

		public enum AddFeeStructureDetailStatuses
		{
			NoRecordFound,
			AlreadyExists,
			Success
		}

		public static AddFeeStructureDetailStatuses AddFeeStructureDetail(int feeStructureID, int feeHeadID, SemesterNos? semesterNoEnum, Categories? categoryEnum, int amount, Guid loginSessionGuid)
		{
			lock (Locks.FeeStructureTransaction)
			{
				using (var aspireContext = new AspireContext(true))
				{
					var admissionOpenProgram = aspireContext.FeeStructures.Where(fs => fs.FeeStructureID == feeStructureID).Select(fs => new
					{
						fs.AdmissionOpenProgram.Program.InstituteID,
						fs.AdmissionOpenProgram.Program.DepartmentID,
						fs.AdmissionOpenProgram.ProgramID,
						fs.AdmissionOpenProgram.AdmissionOpenProgramID,
					}).SingleOrDefault();
					if (admissionOpenProgram == null)
						return AddFeeStructureDetailStatuses.NoRecordFound;
					var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, admissionOpenProgram.InstituteID, admissionOpenProgram.DepartmentID, admissionOpenProgram.ProgramID, admissionOpenProgram.AdmissionOpenProgramID);
					var feeStructureDetail = new Model.Entities.FeeStructureDetail
					{
						FeeStructureID = feeStructureID,
						FeeHeadID = aspireContext.FeeHeads.Where(fh => fh.FeeHeadID == feeHeadID && fh.InstituteID == loginHistory.InstituteID).Select(fh => fh.FeeHeadID).Single(),
						SemesterNoEnum = semesterNoEnum,
						CategoryEnum = categoryEnum,
						Amount = amount,
					};
					aspireContext.FeeStructureDetails.Add(feeStructureDetail);
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);

					var semesterNoEnums = Enum.GetValues(typeof(SemesterNos)).Cast<SemesterNos>().ToList();
					var categoryEnums = Enum.GetValues(typeof(Categories)).Cast<Categories>().ToList();
					var feeStructureDetails = aspireContext.FeeStructureDetails.Where(fsd => fsd.FeeStructureID == feeStructureID && fsd.FeeHeadID == feeHeadID).ToList();

					foreach (var semesterNo in semesterNoEnums)
						foreach (var category in categoryEnums)
						{
							var count = feeStructureDetails.Count(fsd => (fsd.SemesterNoEnum == null || fsd.SemesterNoEnum == semesterNo) && (fsd.CategoryEnum == null || fsd.CategoryEnum == category));
							if (count > 1)
							{
								aspireContext.RollbackTransaction();
								return AddFeeStructureDetailStatuses.AlreadyExists;
							}
						}

					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
					return AddFeeStructureDetailStatuses.Success;
				}
			}
		}

		public enum UpdateFeeStructureDetailStatuses
		{
			NoRecordFound,
			Success
		}

		public static UpdateFeeStructureDetailStatuses UpdateFeeStructureDetail(int feeStructureDetailID, int amount, Guid loginSessionGuid)
		{
			lock (Locks.FeeStructureTransaction)
			{
				using (var aspireContext = new AspireContext(true))
				{
					var admissionOpenProgram = aspireContext.FeeStructureDetails.Where(fsd => fsd.FeeStructureDetailID == feeStructureDetailID).Select(fsd => new
					{
						fsd.FeeStructure.AdmissionOpenProgram.Program.InstituteID,
						fsd.FeeStructure.AdmissionOpenProgram.Program.DepartmentID,
						fsd.FeeStructure.AdmissionOpenProgram.ProgramID,
						fsd.FeeStructure.AdmissionOpenProgram.AdmissionOpenProgramID,
						FeeStructureDetail = fsd,
					}).SingleOrDefault();
					if (admissionOpenProgram == null)
						return UpdateFeeStructureDetailStatuses.NoRecordFound;
					var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, admissionOpenProgram.InstituteID, admissionOpenProgram.DepartmentID, admissionOpenProgram.ProgramID, admissionOpenProgram.AdmissionOpenProgramID);
					admissionOpenProgram.FeeStructureDetail.Amount = amount;
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
					return UpdateFeeStructureDetailStatuses.Success;
				}
			}
		}

		public enum DeleteFeeStructureDetailStatuses
		{
			NoRecordFound,
			Success,
		}

		public static DeleteFeeStructureDetailStatuses DeleteFeeStructureDetail(int feeStructureDetailID, Guid loginSessionGuid)
		{
			lock (Locks.FeeStructureTransaction)
			{
				using (var aspireContext = new AspireContext(true))
				{
					var admissionOpenProgram = aspireContext.FeeStructureDetails.Where(fsd => fsd.FeeStructureDetailID == feeStructureDetailID).Select(fsd => new
					{
						fsd.FeeStructure.AdmissionOpenProgram.Program.InstituteID,
						fsd.FeeStructure.AdmissionOpenProgram.Program.DepartmentID,
						fsd.FeeStructure.AdmissionOpenProgram.ProgramID,
						fsd.FeeStructure.AdmissionOpenProgram.AdmissionOpenProgramID,
						FeeStructureDetail = fsd,
					}).SingleOrDefault();
					if (admissionOpenProgram == null)
						return DeleteFeeStructureDetailStatuses.NoRecordFound;
					var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, admissionOpenProgram.InstituteID, admissionOpenProgram.DepartmentID, admissionOpenProgram.ProgramID, admissionOpenProgram.AdmissionOpenProgramID);
					aspireContext.FeeStructureDetails.Remove(admissionOpenProgram.FeeStructureDetail);
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
					return DeleteFeeStructureDetailStatuses.Success;
				}
			}
		}

		public static Model.Entities.FeeStructureDetail GetFeeStructureDetail(int feeStructureDetailID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.FeeStructureDetails.Include(fds => fds.FeeHead).SingleOrDefault(fsd => fsd.FeeStructureDetailID == feeStructureDetailID && fsd.FeeStructure.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
			}
		}

		public static List<FeeHead> GetFeeHeadsForIncrement(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.FeeHeads.Where(fh => fh.InstituteID == loginHistory.InstituteID)
					.Where(fh => fh.FeeHeadType == (byte)FeeHead.FeeHeadTypes.TutionFee)
					.OrderBy(fh => fh.HeadAlias)
					.ToList();
			}
		}

		public class FeeStructureDetail
		{
			internal FeeStructureDetail() { }
			public int FeeStructureDetailID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string DepartmentAlias { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public Categories? CategoryEnum { get; internal set; }
			public int FeeHeadID { get; internal set; }
			public string HeadAlias { get; internal set; }
			public SemesterNos? SemesterNoEnum { get; internal set; }
			public int Amount { get; internal set; }
			public double IncrementPercentage { get; internal set; }
			public int IncrementAmount => (int)Math.Round(this.Amount * this.IncrementPercentage / 100d, 0, MidpointRounding.AwayFromZero);
			public int AmountAfterIncrement => this.Amount + this.IncrementAmount;
		}

		private static IQueryable<Model.Entities.FeeStructureDetail> GetFeeStructureDetailsQuery(this AspireContext aspireContext, short upToSemesterID, int? departmentID, int? programID, Categories? categoryEnum, SemesterNos? semesterNoEnum, int feeHeadID, Guid loginSessionGuid)
		{
			var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
			var feeStructureDetailsQuery = aspireContext.FeeStructureDetails
				.Where(fsd => fsd.FeeStructure.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
				.Where(fsd => fsd.FeeHeadID == feeHeadID)
				.Where(fsd => fsd.FeeStructure.AdmissionOpenProgram.SemesterID <= upToSemesterID);
			if (departmentID != null)
				feeStructureDetailsQuery = feeStructureDetailsQuery.Where(fsd => fsd.FeeStructure.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
			if (programID != null)
				feeStructureDetailsQuery = feeStructureDetailsQuery.Where(fsd => fsd.FeeStructure.AdmissionOpenProgram.ProgramID == programID.Value);
			if (categoryEnum != null)
				feeStructureDetailsQuery = feeStructureDetailsQuery.Where(fsd => fsd.Category == null || fsd.Category == (byte)categoryEnum.Value);
			if (semesterNoEnum != null)
				feeStructureDetailsQuery = feeStructureDetailsQuery.Where(fsd => fsd.SemesterNo == null || fsd.SemesterNo == (byte)semesterNoEnum.Value);
			return feeStructureDetailsQuery;
		}

		public static List<FeeStructureDetail> GetFeeStructureDetails(short upToSemesterID, int? departmentID, int? programID, Categories? categoryEnum, SemesterNos? semesterNoEnum, int feeHeadID, int incrementPercentage, string sortExpression, SortDirection sortdirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var feeStructureDetailsQuery = aspireContext.GetFeeStructureDetailsQuery(upToSemesterID, departmentID, programID, categoryEnum, semesterNoEnum, feeHeadID, loginSessionGuid)
					.Select(fsd => new FeeStructureDetail
					{
						FeeStructureDetailID = fsd.FeeStructureDetailID,
						SemesterID = fsd.FeeStructure.AdmissionOpenProgram.SemesterID,
						DepartmentAlias = fsd.FeeStructure.AdmissionOpenProgram.Program.Department.DepartmentAlias,
						ProgramAlias = fsd.FeeStructure.AdmissionOpenProgram.Program.ProgramAlias,
						CategoryEnum = (Categories?)fsd.Category,
						FeeHeadID = fsd.FeeHeadID,
						HeadAlias = fsd.FeeHead.HeadAlias,
						Amount = fsd.Amount,
						SemesterNoEnum = (SemesterNos?)fsd.SemesterNo,
						IncrementPercentage = incrementPercentage
					});
				switch (sortExpression)
				{
					case nameof(FeeStructureDetail.SemesterID):
						return feeStructureDetailsQuery.OrderBy(sortdirection, fsd => fsd.SemesterID).ToList();
					case nameof(FeeStructureDetail.DepartmentAlias):
						return feeStructureDetailsQuery.OrderBy(sortdirection, fsd => fsd.DepartmentAlias).ToList();
					case nameof(FeeStructureDetail.ProgramAlias):
						return feeStructureDetailsQuery.OrderBy(sortdirection, fsd => fsd.ProgramAlias).ToList();
					case nameof(FeeStructureDetail.CategoryEnum):
						return feeStructureDetailsQuery.OrderBy(sortdirection, fsd => fsd.CategoryEnum).ToList();
					case nameof(FeeStructureDetail.HeadAlias):
						return feeStructureDetailsQuery.OrderBy(sortdirection, fsd => fsd.HeadAlias).ToList();
					case nameof(FeeStructureDetail.Amount):
						return feeStructureDetailsQuery.OrderBy(sortdirection, fsd => fsd.Amount).ToList();
					case nameof(FeeStructureDetail.SemesterNoEnum):
						return feeStructureDetailsQuery.OrderBy(sortdirection, fsd => fsd.SemesterNoEnum).ToList();
					case nameof(FeeStructureDetail.IncrementPercentage):
						return feeStructureDetailsQuery.OrderBy(sortdirection, fsd => fsd.IncrementPercentage).ToList();
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
			}
		}

		public enum UpdateFeeStructureDetailsStatuses
		{
			Success,
			NoRecordFound
		}

		public static (UpdateFeeStructureDetailsStatuses status, int recordsUpdated) UpdateFeeStructureDetails(short upToSemesterID, int? departmentID, int? programID, Categories? categoryEnum, SemesterNos? semesterNoEnum, int feeHeadID, int incrementPercentage, Guid loginSessionGuid)
		{
			lock (Locks.FeeStructureTransaction)
			{
				using (var aspireContext = new AspireContext(true))
				{
					var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
					aspireContext.DemandPermissions(loginSessionGuid, loginHistory.InstituteID, null, null, null);
					var feeStructureDetailsQuery = aspireContext.GetFeeStructureDetailsQuery(upToSemesterID, departmentID, programID, categoryEnum, semesterNoEnum, feeHeadID, loginSessionGuid);

					var records = feeStructureDetailsQuery
						.Select(fsd => new
						{
							fsd.FeeStructure.AdmissionOpenProgram.SemesterID,
							fsd.FeeStructure.AdmissionOpenProgram.Program.Department.DepartmentAlias,
							fsd.FeeStructure.AdmissionOpenProgram.Program.ProgramAlias,
							fsd.FeeHead.HeadAlias,
							IncrementPercentage = incrementPercentage,
							FeeStructureDetail = fsd
						}).ToList();

					var feeStructureDetails = new List<FeeStructureDetail>();
					foreach (var record in records)
					{
						var feeStructureDetail = new FeeStructureDetail
						{
							FeeStructureDetailID = record.FeeStructureDetail.FeeStructureDetailID,
							Amount = record.FeeStructureDetail.Amount,
							CategoryEnum = record.FeeStructureDetail.CategoryEnum,
							DepartmentAlias = record.DepartmentAlias,
							ProgramAlias = record.ProgramAlias,
							FeeHeadID = record.FeeStructureDetail.FeeHeadID,
							HeadAlias = record.HeadAlias,
							IncrementPercentage = record.IncrementPercentage,
							SemesterID = record.SemesterID,
							SemesterNoEnum = record.FeeStructureDetail.SemesterNoEnum
						};
						feeStructureDetails.Add(feeStructureDetail);
						record.FeeStructureDetail.Amount = feeStructureDetail.AmountAfterIncrement;
					}

					var createdDate = DateTime.Now;
					var html = new StringBuilder();
					html.Append("<html>");
					{
						html.Append("\n<head>");
						{
							html.Append(@"<style type=""text/css"">
body {
font-family: ""Helvetica Neue"", Helvetica, Arial, sans-serif;
font-size: 13px;
}
table {
border: 1px solid #ddd;
border-collapse: collapse;
border-spacing: 0;
color: rgb(51, 51, 51);
}
th {
border: 1px solid #ddd;
padding: 4px;
text-align: left;
font-weight: bold;
}
td {
border: 1px solid #ddd;
padding: 4px;
text-align: left;
}
</style>");
						}
						html.Append("\n</head>");
						html.Append("<body>");
						{
							html.Append($"Changes made on {createdDate:F}.");
							html.Append("<table>");
							{
								html.Append("<thead>");
								{
									html.Append("<tr>");
									{
										html.Append($"<th>{"#".HtmlEncode()}</th>");
										html.Append($"<th>{"FeeStructureDetailID".HtmlEncode()}</th>");
										html.Append($"<th>{"Intake Semester".HtmlEncode()}</th>");
										html.Append($"<th>{"Department".HtmlEncode()}</th>");
										html.Append($"<th>{"Program".HtmlEncode()}</th>");
										html.Append($"<th>{"Category".HtmlEncode()}</th>");
										html.Append($"<th>{"Semester No.".HtmlEncode()}</th>");
										html.Append($"<th>{"Fee Head".HtmlEncode()}</th>");
										html.Append($"<th>{"Amount".HtmlEncode()}</th>");
										html.Append($"<th>{"Increment".HtmlEncode()}</th>");
										html.Append($"<th>{"Amount After Increment".HtmlEncode()}</th>");
									}
									html.Append("</tr>");
								}
								html.Append("</thead>");
								html.Append("<tbody>");
								{
									var index = 1;
									foreach (var feeStructureDetail in feeStructureDetails.OrderByDescending(fsd => fsd.SemesterID).ThenBy(fsd => fsd.DepartmentAlias).ThenBy(fsd => fsd.ProgramAlias))
									{
										html.Append("<tr>");
										{
											html.Append($"<td>{index++}</td>");
											html.Append($"<td>{($"{feeStructureDetail.FeeStructureDetailID}").HtmlEncode()}</td>");
											html.Append($"<td>{($"{feeStructureDetail.SemesterID.ToSemesterString()}").HtmlEncode()}</td>");
											html.Append($"<td>{($"{feeStructureDetail.DepartmentAlias}".ToNAIfNullOrEmpty()).HtmlEncode()}</td>");
											html.Append($"<td>{($"{feeStructureDetail.ProgramAlias}").HtmlEncode()}</td>");
											html.Append($"<td>{($"{feeStructureDetail.CategoryEnum?.ToFullName()}" ?? "All").HtmlEncode()}</td>");
											html.Append($"<td>{($"{feeStructureDetail.SemesterNoEnum?.ToFullName()}" ?? "All").HtmlEncode()}</td>");
											html.Append($"<td>{($"{feeStructureDetail.HeadAlias}").HtmlEncode()}</td>");
											html.Append($"<td>{($"Rs. {feeStructureDetail.Amount}").HtmlEncode()}</td>");
											html.Append($"<td>{($"{feeStructureDetail.IncrementPercentage}%").HtmlEncode()}</td>");
											html.Append($"<td>{($"Rs. {feeStructureDetail.AmountAfterIncrement}").HtmlEncode()}</td>");
										}
										html.Append("</tr>");
									}
								}
								html.Append("</tbody>");
							}
							html.Append("</table>");
						}
						html.Append("</body>");
					}
					html.Append("</html>");

					if (feeStructureDetails.Any())
					{
						var subject = "Fee Structure Updated";
						var emailBody = html.ToString();
						var user = aspireContext.Users.Where(u => u.UserID == loginHistory.UserID).Select(u => new { u.Email, u.Name }).Single();
						var toList = new[] { (user.Name, user.Email), ("Aurang Zeb", "orangzeb@bahria.edu.pk") };
						aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.FeeStructureUpdated, subject, emailBody, true, Email.Priorities.Highest, createdDate, null, loginHistory.UserLoginHistoryID, toList: toList);
						aspireContext.CommitTransaction();
						return (UpdateFeeStructureDetailsStatuses.Success, feeStructureDetails.Count);
					}
					return (UpdateFeeStructureDetailsStatuses.NoRecordFound, feeStructureDetails.Count);
				}
			}
		}
	}
}
