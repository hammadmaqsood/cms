﻿using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.FeeManagement.Common.Reports
{
	public static class FeeChallans
	{
		public sealed class FeeChallan
		{
			public int InstituteID { get; internal set; }
			public string InstitutePhone { get; internal set; }
			public string InstituteName { get; internal set; }
			public int StudentFeeID { get; internal set; }
			public int StudentFeeChallanID { get; internal set; }
			internal string InstituteCode { get; set; }
			internal bool NewAdmission { get; set; }
			public string FullChallanNo => AspireFormats.ChallanNo.GetFullChallanNoString(this.InstituteCode, this.NewAdmission ? InstituteBankAccount.FeeTypes.NewAdmissionFee : InstituteBankAccount.FeeTypes.StudentFee, this.StudentFeeChallanID);
			public int? CandidateAppliedProgramID { get; internal set; }
			public int? StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public int? ApplicationNo { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public string IntakeSemester => this.IntakeSemesterID.ToSemesterString();
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public byte Gender { get; internal set; }
			public Genders GenderEnum => (Genders)this.Gender;
			public string Name { get; internal set; }
			public string NameFatherName
			{
				get
				{
					switch (this.GenderEnum)
					{
						case Genders.Male:
							return $"{this.Name} S/O {this.FatherName}";
						case Genders.Female:
							return $"{this.Name} D/O {this.FatherName}";
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
			public string FatherName { get; internal set; }
			public string Program { get; internal set; }
			public short? SemesterNo { get; internal set; }
			public int? Section { get; internal set; }
			public byte? Shift { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift);
			public string Nationality { get; internal set; }
			public byte Category { get; internal set; }
			public string CategoryFullName => ((Categories)this.Category).ToFullName();
			public DateTime CreatedDate { get; internal set; }
			public int GrandTotalAmount { get; internal set; }
			public int Amount { get; internal set; }
			public string AmountInWords => this.Amount.ToWords();
			public DateTime DueDate { get; internal set; }
			public DateTime? DepositDate { get; internal set; }
			public byte StudentFeeChallanStatus { get; internal set; }
			public bool Paid => ((StudentFeeChallan.Statuses)this.StudentFeeChallanStatus) == StudentFeeChallan.Statuses.Paid;
			public byte NoOfInstallments { get; internal set; }
			public byte InstallmentNo { get; internal set; }
			public string Remarks { get; internal set; }
			public bool RemarksVisible => !string.IsNullOrWhiteSpace(this.Remarks);
			public int? TutionFeePerCreditHour { get; internal set; }
			internal int AdmissionOpenProgramID { get; set; }
			public string EnrollmentApplicationNoLabel => this.StudentID != null ? "Enrollment" : "Application No.";
			public string EnrollmentApplicationNo => this.StudentID != null ? this.Enrollment : this.ApplicationNo?.ToString();
			public string ProgramClassLabel => this.SemesterNo != null ? "Class" : "Program";

			public string SemesterIntakeSemesterLabel => this.StudentID != null ? "Semester" : "Intake Semester";
			public string SemesterIntakeSemester => this.StudentID != null ? this.Semester : this.IntakeSemester;

			public string RegistrationNoLabel => this.StudentID != null ? "Registration No." : string.Empty;
			public string RegistrationNoValue => this.StudentID != null ? (this.RegistrationNo?.ToString()).ToNAIfNullOrEmpty() : string.Empty;

			public List<FeeDetail> FeeDetails { get; internal set; }
			public List<RegisteredCourse> RegisteredCourses { get; internal set; }
			public List<BankAccount> BankAccounts { get; internal set; }
			public List<Installment> Installments { get; internal set; }
			public List<string> Concessions { get; internal set; }
			public bool ConcessionsVisible => this.Concessions != null && this.Concessions.Any();
			public bool RegisteredCoursesVisible => this.RegisteredCourses != null && this.RegisteredCourses.Any();
			public string ConcessionsValue => this.ConcessionsVisible ? string.Join(", ", this.Concessions) : string.Empty;
			public int? CreatedByULoginHistoryID { get; internal set; }
			internal short? CreatedByUserType { get; set; }
			internal UserTypes? CreatedByUserTypeEnum => (UserTypes?)this.CreatedByUserType;
			internal string CreatedByName { get; set; }
			public string CreatedBy => $"{this.CreatedByName} ({this.CreatedByUserTypeEnum?.ToFullName()})";
			public byte FeeType { get; internal set; }
			public StudentFee.FeeTypes FeeTypeEnum => (StudentFee.FeeTypes)this.FeeType;
			internal bool ExemptWHT { get; set; }
			public bool BankAndCampusCopyVisible => this.FeeTypeEnum == StudentFee.FeeTypes.Challan && !this.Paid;
			public bool InstituteBankAccountVisible => this.FeeTypeEnum == StudentFee.FeeTypes.Challan;

			public sealed class RegisteredCourse
			{
				public string CourseCode { get; internal set; }
				public string Title { get; internal set; }
				public decimal CreditHours { get; internal set; }
				public string CreditHoursString => this.CreditHours.ToCreditHoursFullName();
				public string Program { get; internal set; }
				public short SemesterNo { get; internal set; }
				public int Section { get; internal set; }
				public byte Shift { get; internal set; }
				public short SemesterID { get; internal set; }
				public string Semester => this.SemesterID.ToSemesterString();
				public string Class => AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift);

				public List<RegisteredCourse> GetList()
				{
					return null;
				}
			}

			public sealed class FeeDetail
			{
				public string FeeHeadName { get; internal set; }
				public bool Refundable { get; internal set; }
				public int? TotalAmount { get; internal set; }
				public string TotalAmountString => this.TotalAmount?.ToString();
				public int? ConcessionAmount { get; internal set; }
				public string ConcessionAmountString => this.ConcessionAmount?.ToString();
				public int GrandTotalAmount { get; internal set; }
				public int FullGrandTotalAmount { get; internal set; }
				public string FullGrandTotalAmountString => this.FullGrandTotalAmount.ToString();
				public int FullTotalAmount { get; internal set; }
				public string FullTotalAmountString => this.FullTotalAmount.ToString();
				public int FullConcessionAmount { get; internal set; }
				public string FullConcessionAmountString => this.FullConcessionAmount.ToString();
				public byte RuleType { get; internal set; }
				public bool WithholdingTaxApplicable { get; internal set; }
				public FeeHead.RuleTypes RuleTypeEnum => (FeeHead.RuleTypes)this.RuleType;
				public string FeeHeadDisplayName
				{
					get
					{
						string ruleType;
						switch (this.RuleTypeEnum)
						{
							case FeeHead.RuleTypes.Credit:
								ruleType = "+";
								break;
							case FeeHead.RuleTypes.Debit:
								ruleType = "-";
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}

						var refundable = this.Refundable ? " (Refundable)" : "";
						var whtApplicable = this.WithholdingTaxApplicable ? " (Withholding Tax applicable)" : "";
						return $"{ruleType}{this.FeeHeadName} {whtApplicable}{refundable}".Replace("  ", " ");
					}
				}

				public List<FeeDetail> GetList()
				{
					return null;
				}
			}

			public sealed class BankAccount
			{
				public int InstituteBankAccountID { get; internal set; }
				public string BankName { get; internal set; }
				public string BankNameForDisplay { get; internal set; }
				internal string AccountTitle { get; set; }
				internal string AccountTitleForDisplay { get; set; }
				internal string AccountNo { get; set; }
				internal string AccountNoForDisplay { get; set; }
				internal string BranchCode { get; set; }
				internal string BranchName { get; set; }
				internal string BranchAddress { get; set; }
				internal string BranchForDisplay { get; set; }
				public string BankNameText => this.BankNameForDisplay.ToNullIfWhiteSpace() ?? this.BankName;
				public string AccountTitleText => this.AccountTitleForDisplay.ToNullIfWhiteSpace() ?? this.AccountTitle;
				public string AccountNoText => this.AccountNoForDisplay.ToNullIfWhiteSpace() ?? this.AccountNo;
				public string BranchText => this.BranchForDisplay.ToNullIfWhiteSpace() ?? $"{this.BranchCode} - {this.BranchName}";
				public string Instructions
				{
					get
					{
						switch (this.BankName)
						{
							case "Allied Bank":
								return AccountTransaction.AlliedBankInstructions;
							case "Bank Alfalah":
								return AccountTransaction.BankAlfalahInstructions;
							default:
								return $"Instructions are missing for {this.BankName}";
						}
					}
				}
				internal bool Active { get; set; }

				public List<BankAccount> GetList()
				{
					return null;
				}
			}

			public sealed class Installment
			{
				public int StudentFeeChallanID { get; internal set; }
				public int Amount { get; internal set; }
				public DateTime DueDate { get; internal set; }
				public byte InstallmentNo { get; internal set; }

				public List<Installment> GetList()
				{
					return null;
				}
			}

			public List<FeeChallan> GetList()
			{
				return null;
			}
		}

		internal static List<FeeChallan> GetFeeChallans(this AspireContext aspireContext, List<int> studentFeeIDs)
		{
			var studentFeeChallans = aspireContext.StudentFeeChallans
				.Where(sfc => studentFeeIDs.Contains(sfc.StudentFeeID))
				.Where(sfc => !(sfc.Amount == 0 && sfc.Status == (byte)StudentFeeChallan.Statuses.Paid && sfc.AccountTransactionID == null));
			var studentFee = studentFeeChallans.Select(sfc => new
			{
				sfc.StudentFeeID,
				sfc.StudentFee.StudentID,
				sfc.StudentFee.CandidateAppliedProgramID,
				sfc.StudentFee.NewAdmission,
			}).FirstOrDefault();
			if (studentFee == null)
				return null;

			List<FeeChallan> feeChallans;

			var feeType = studentFee.NewAdmission ? (byte)InstituteBankAccount.FeeTypes.NewAdmissionFee : (byte)InstituteBankAccount.FeeTypes.StudentFee;
			if (studentFee.StudentID != null)
			{
				feeChallans = studentFeeChallans.Select(sfc => new FeeChallan
				{
					InstituteID = sfc.StudentFee.Student.AdmissionOpenProgram.Program.InstituteID,
					InstituteCode = sfc.StudentFee.Student.AdmissionOpenProgram.Program.Institute.InstituteCode,
					AdmissionOpenProgramID = sfc.StudentFee.Student.AdmissionOpenProgramID,
					NewAdmission = sfc.StudentFee.NewAdmission,
					StudentID = sfc.StudentFee.StudentID,
					Enrollment = sfc.StudentFee.Student.Enrollment,
					SemesterID = sfc.StudentFee.SemesterID,
					Program = sfc.StudentFee.Student.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = sfc.StudentFee.Student.StudentSemesters.Where(ss => ss.SemesterID == sfc.StudentFee.SemesterID).Select(ss => (short?)ss.SemesterNo).FirstOrDefault(),
					Section = sfc.StudentFee.Student.StudentSemesters.Where(ss => ss.SemesterID == sfc.StudentFee.SemesterID).Select(ss => (int?)ss.Section).FirstOrDefault(),
					Shift = sfc.StudentFee.Student.StudentSemesters.Where(ss => ss.SemesterID == sfc.StudentFee.SemesterID).Select(ss => (byte?)ss.Shift).FirstOrDefault(),
					StudentFeeChallanID = sfc.StudentFeeChallanID,
					StudentFeeID = sfc.StudentFeeID,
					CandidateAppliedProgramID = sfc.StudentFee.CandidateAppliedProgramID,
					Remarks = sfc.StudentFee.Remarks,
					Name = sfc.StudentFee.Student.Name,
					Amount = sfc.Amount,
					ApplicationNo = sfc.StudentFee.CandidateAppliedProgram.ApplicationNo,
					Category = sfc.StudentFee.Student.Category,
					CreatedDate = sfc.StudentFee.CreatedDate,
					DueDate = sfc.DueDate,
					DepositDate = sfc.AccountTransaction.TransactionDate,
					FatherName = sfc.StudentFee.Student.FatherName,
					Gender = sfc.StudentFee.Student.Gender,
					InstallmentNo = sfc.InstallmentNo,
					GrandTotalAmount = sfc.StudentFee.GrandTotalAmount,
					InstituteName = sfc.StudentFee.Student.AdmissionOpenProgram.Program.Institute.InstituteName,
					InstitutePhone = sfc.StudentFee.Student.AdmissionOpenProgram.Program.Institute.Phone,
					IntakeSemesterID = sfc.StudentFee.Student.AdmissionOpenProgram.SemesterID,
					Nationality = sfc.StudentFee.Student.Nationality,
					NoOfInstallments = sfc.StudentFee.NoOfInstallments,
					RegistrationNo = sfc.StudentFee.Student.RegistrationNo,
					StudentFeeChallanStatus = sfc.Status,
					CreatedByULoginHistoryID = sfc.StudentFee.CreatedByUserLoginHistoryID,
					CreatedByUserType = sfc.StudentFee.UserLoginHistory.UserType,
					FeeType = sfc.StudentFee.FeeType,
					ExemptWHT = sfc.StudentFee.ExemptWHT,
					RegisteredCourses = aspireContext.RegisteredCourses.Where(rc => rc.StudentFeeID == sfc.StudentFeeID && rc.DeletedDate == null).Select(rc => new FeeChallan.RegisteredCourse
					{
						Program = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						SemesterNo = rc.OfferedCours.SemesterNo,
						Section = rc.OfferedCours.Section,
						Shift = rc.OfferedCours.Shift,
						CreditHours = rc.Cours.CreditHours,
						CourseCode = rc.Cours.CourseCode,
						Title = rc.Cours.Title,
						SemesterID = rc.OfferedCours.SemesterID,
					}).ToList(),
					Concessions = sfc.StudentFee.StudentFeeConcessionTypes.Select(sfct => sfct.FeeConcessionType.ConcessionName).ToList(),
					Installments = sfc.StudentFee.StudentFeeChallans.Select(sfcc => new FeeChallan.Installment
					{
						StudentFeeChallanID = sfcc.StudentFeeChallanID,
						Amount = sfcc.Amount,
						InstallmentNo = sfcc.InstallmentNo,
						DueDate = sfcc.DueDate,
					}).ToList(),
					BankAccounts = sfc.StudentFee.Student.AdmissionOpenProgram.Program.Institute.InstituteBankAccounts
						.Where(iba => iba.FeeType == feeType && ((sfc.Status == StudentFeeChallan.StatusPaid && iba.InstituteBankAccountID == sfc.AccountTransaction.InstituteBankAccountID) || (sfc.Status == StudentFeeChallan.StatusNotPaid && iba.Active)))
						.Select(iba => new FeeChallan.BankAccount
						{
							InstituteBankAccountID = iba.InstituteBankAccountID,
							AccountNo = iba.AccountNo,
							AccountTitle = iba.AccountTitle,
							BankName = iba.Bank.BankName,
							BankNameForDisplay = iba.Bank.BankNameForDisplay,
							BranchName = iba.BranchName,
							BranchCode = iba.BranchCode,
							BranchAddress = iba.BranchAddress,
							AccountTitleForDisplay = iba.AccountTitleForDisplay,
							AccountNoForDisplay = iba.AccountNoForDisplay,
							BranchForDisplay = iba.BranchForDisplay,
							Active = iba.Active,
						}).ToList(),
					FeeDetails = sfc.StudentFee.StudentFeeDetails.Select(fd => new FeeChallan.FeeDetail
					{
						FullGrandTotalAmount = sfc.StudentFee.GrandTotalAmount,
						FullTotalAmount = sfc.StudentFee.TotalAmount,
						FullConcessionAmount = sfc.StudentFee.ConcessionAmount,
						GrandTotalAmount = fd.GrandTotalAmount,
						Refundable = fd.FeeHead.Refundable,
						RuleType = fd.FeeHead.RuleType,
						WithholdingTaxApplicable = !sfc.StudentFee.ExemptWHT && fd.FeeHead.WithholdingTaxApplicable,
						ConcessionAmount = fd.ConcessionAmount,
						FeeHeadName = fd.FeeHead.HeadName,
						TotalAmount = fd.TotalAmount
					}).ToList()
				}).ToList();
			}
			else
			{
				if (studentFee.CandidateAppliedProgramID == null)
					throw new InvalidOperationException();
				var candidates = from cap in aspireContext.CandidateAppliedPrograms
								 join aop in aspireContext.AdmissionOpenPrograms on cap.AdmittedAdmissionOpenProgramID equals aop.AdmissionOpenProgramID
								 select new
								 {
									 cap.CandidateAppliedProgramID,
									 aop.AdmissionOpenProgramID,
									 aop.Program.InstituteID,
									 aop.Program.ProgramAlias,
									 cap.Candidate.Name,
									 cap.Candidate.FatherName,
									 cap.Candidate.Gender,
									 cap.Candidate.Nationality,
									 cap.Candidate.Category,
									 cap.ApplicationNo,
									 aop.Program.Institute.InstituteName,
									 aop.Program.Institute.InstituteCode,
									 aop.Program.Institute.Phone,
									 aop.SemesterID,
									 BankAccounts = aop.Program.Institute.InstituteBankAccounts.Where(iba => iba.FeeType == feeType).Select(iba => new FeeChallan.BankAccount
									 {
										 InstituteBankAccountID = iba.InstituteBankAccountID,
										 AccountNo = iba.AccountNo,
										 AccountTitle = iba.AccountTitle,
										 BankName = iba.Bank.BankName,
										 BankNameForDisplay = iba.Bank.BankNameForDisplay,
										 BranchName = iba.BranchName,
										 BranchCode = iba.BranchCode,
										 BranchAddress = iba.BranchAddress,
										 AccountTitleForDisplay = iba.AccountTitleForDisplay,
										 AccountNoForDisplay = iba.AccountNoForDisplay,
										 BranchForDisplay = iba.BranchForDisplay,
										 Active = iba.Active,
									 }),
								 };
				feeChallans = (from sfc in studentFeeChallans
							   join c in candidates on sfc.StudentFee.CandidateAppliedProgramID equals c.CandidateAppliedProgramID
							   select new FeeChallan
							   {
								   InstituteID = c.InstituteID,
								   InstituteCode = c.InstituteCode,
								   AdmissionOpenProgramID = c.AdmissionOpenProgramID,
								   NewAdmission = sfc.StudentFee.NewAdmission,
								   StudentID = sfc.StudentFee.StudentID,
								   Enrollment = null,
								   SemesterID = sfc.StudentFee.SemesterID,
								   Program = c.ProgramAlias,
								   SemesterNo = null,
								   Section = null,
								   Shift = null,
								   StudentFeeChallanID = sfc.StudentFeeChallanID,
								   StudentFeeID = sfc.StudentFeeID,
								   CandidateAppliedProgramID = sfc.StudentFee.CandidateAppliedProgramID,
								   Remarks = sfc.StudentFee.Remarks,
								   Name = c.Name,
								   Amount = sfc.Amount,
								   ApplicationNo = c.ApplicationNo,
								   Category = c.Category.Value,
								   CreatedDate = sfc.StudentFee.CreatedDate,
								   DueDate = sfc.DueDate,
								   DepositDate = sfc.AccountTransaction.TransactionDate,
								   FatherName = c.FatherName,
								   Gender = c.Gender.Value,
								   InstallmentNo = sfc.InstallmentNo,
								   GrandTotalAmount = sfc.StudentFee.GrandTotalAmount,
								   InstituteName = c.InstituteName,
								   InstitutePhone = c.Phone,
								   IntakeSemesterID = c.SemesterID,
								   Nationality = c.Nationality,
								   NoOfInstallments = sfc.StudentFee.NoOfInstallments,
								   RegistrationNo = null,
								   StudentFeeChallanStatus = sfc.Status,
								   CreatedByULoginHistoryID = sfc.StudentFee.CreatedByUserLoginHistoryID,
								   CreatedByUserType = sfc.StudentFee.UserLoginHistory.UserType,
								   Concessions = sfc.StudentFee.StudentFeeConcessionTypes.Select(sfct => sfct.FeeConcessionType.ConcessionName).ToList(),
								   FeeType = sfc.StudentFee.FeeType,
								   ExemptWHT = sfc.StudentFee.ExemptWHT,
								   Installments = sfc.StudentFee.StudentFeeChallans.Select(sfcc => new FeeChallan.Installment
								   {
									   StudentFeeChallanID = sfcc.StudentFeeChallanID,
									   Amount = sfcc.Amount,
									   InstallmentNo = sfcc.InstallmentNo,
									   DueDate = sfcc.DueDate,
								   }).ToList(),
								   BankAccounts = c.BankAccounts.Where(iba => (sfc.Status == StudentFeeChallan.StatusPaid && iba.InstituteBankAccountID == sfc.AccountTransaction.InstituteBankAccountID) || (sfc.Status == StudentFeeChallan.StatusNotPaid && iba.Active)).ToList(),
								   FeeDetails = sfc.StudentFee.StudentFeeDetails.Select(fd => new FeeChallan.FeeDetail
								   {
									   FullGrandTotalAmount = sfc.StudentFee.GrandTotalAmount,
									   FullTotalAmount = sfc.StudentFee.TotalAmount,
									   FullConcessionAmount = sfc.StudentFee.ConcessionAmount,
									   GrandTotalAmount = fd.GrandTotalAmount,
									   Refundable = fd.FeeHead.Refundable,
									   RuleType = fd.FeeHead.RuleType,
									   WithholdingTaxApplicable = !sfc.StudentFee.ExemptWHT && fd.FeeHead.WithholdingTaxApplicable,
									   ConcessionAmount = fd.ConcessionAmount,
									   FeeHeadName = fd.FeeHead.HeadName,
									   TotalAmount = fd.TotalAmount
								   }).ToList()
							   }).ToList();
			}

			foreach (var feeChallan in feeChallans.Where(fc => fc.CreatedByULoginHistoryID != null))
				switch ((UserTypes?)feeChallan.CreatedByUserType)
				{
					case UserTypes.Staff:
						feeChallan.CreatedByName = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == feeChallan.CreatedByULoginHistoryID.Value).Select(h => h.User.Name).Single();
						break;
					case UserTypes.Candidate:
						feeChallan.CreatedByName = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == feeChallan.CreatedByULoginHistoryID.Value).Select(h => h.Candidate.Name).Single();
						break;
					case UserTypes.Student:
						feeChallan.CreatedByName = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == feeChallan.CreatedByULoginHistoryID.Value).Select(h => h.Student.Name).Single();
						break;
					case UserTypes.ManualSQL:
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Executive:
					case UserTypes.Faculty:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.Admins:
					case UserTypes.Any:
					case null:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}

			foreach (var feeChallan in feeChallans)
			{
				var tutionFeePerCreditHour = aspireContext.FeeStructures
					.Where(fs => fs.AdmissionOpenProgramID == feeChallan.AdmissionOpenProgramID)
					.SelectMany(fs => fs.FeeStructureDetails)
					.Where(fsd => fsd.FeeHead.FeeHeadType == (byte)FeeHead.FeeHeadTypes.TutionFee)
					.Where(fsd => fsd.Category == feeChallan.Category || fsd.Category == null)
					.Select(fsd => fsd.Amount).SingleOrDefault();
				feeChallan.TutionFeePerCreditHour = tutionFeePerCreditHour;
			}

			return feeChallans;
		}
	}
}