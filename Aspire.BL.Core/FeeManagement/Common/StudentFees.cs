﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.FeeManagement.Common
{
	public static class StudentFees
	{
		internal static List<StudentFee> AddStudentFee(this AspireContext aspireContext, StudentFee studentFee, List<int> registeredCourseIDs, int instituteID, int userLoginHistoryID)
		{
			if (studentFee.StudentID == null && studentFee.CandidateAppliedProgramID == null)
				throw new ArgumentException();
			if (studentFee.StudentID != null && studentFee.CandidateAppliedProgramID != null)
				throw new ArgumentException();
			if (studentFee.StudentID == null && studentFee.CandidateAppliedProgramID != null && registeredCourseIDs != null && registeredCourseIDs.Any())
				throw new ArgumentException(null, nameof(registeredCourseIDs));

			if (studentFee.CandidateAppliedProgramID != null && studentFee.StudentID == null)
			{
				var studentID = aspireContext.CandidateAppliedPrograms
					.Where(cap => cap.CandidateAppliedProgramID == studentFee.CandidateAppliedProgramID.Value)
					.Select(cap => cap.StudentID)
					.Single();
				if (studentID != null)
					throw new InvalidOperationException("Fee Challan can not be generated for Application No. whose enrollment have been generated.");
			}

			var feeHeads = aspireContext.FeeHeads.Where(fh => fh.InstituteID == instituteID && fh.Visible).ToList();
			studentFee.NewAdmission = studentFee.StudentID == null
									  || aspireContext.Students
										  .Where(s => s.StudentID == studentFee.StudentID)
										  .Select(s => (short?)s.AdmissionOpenProgram.SemesterID)
										  .SingleOrDefault() == studentFee.SemesterID;
			if (studentFee.CandidateAppliedProgram != null || studentFee.Student != null || (studentFee.RegisteredCourses != null && studentFee.RegisteredCourses.Any()))
				throw new InvalidOperationException();
			studentFee.CreatedByUserLoginHistoryID = userLoginHistoryID;
			studentFee.ValidateForAdd(feeHeads);
			aspireContext.StudentFees.Add(studentFee);
			aspireContext.SaveChanges(userLoginHistoryID);

			if (registeredCourseIDs != null && registeredCourseIDs.Any())
			{
				var registeredCourses = aspireContext.RegisteredCourses
					.Where(rc => registeredCourseIDs.Contains(rc.RegisteredCourseID))
					.Where(rc => rc.DeletedDate == null)
					.Where(rc => rc.StudentID == studentFee.StudentID && rc.OfferedCours.SemesterID == studentFee.SemesterID && rc.StudentFeeID == null)
					.ToList();
				if (registeredCourses.Count != registeredCourseIDs.Count)
					throw new InvalidOperationException("Registered Courses count mismatched.");
				foreach (var registeredCourse in registeredCourses)
					registeredCourse.StudentFeeID = studentFee.StudentFeeID;
				aspireContext.SaveChanges(userLoginHistoryID);
			}

			var studentFeeForTax = aspireContext.CalculateAndGenerateFeeChallanForWithholdingTax(studentFee.StudentFeeID, userLoginHistoryID);
			return studentFeeForTax == null
				? new List<StudentFee> { studentFee }
				: new List<StudentFee> { studentFee, studentFeeForTax };
		}

		public sealed class WHTChallan
		{
			internal WHTChallan() { }
			public int? StudentID { get; internal set; }
			public int? CandidateAppliedProgramID { get; internal set; }
			public DateTime CreatedDate { get; internal set; }
			public DateTime DueDate { get; internal set; }
			public bool NewAdmission { get; internal set; }
			public short SemesterID { get; internal set; }
			public int FeeHeadID { get; internal set; }
			public int InstituteID { get; internal set; }
			public int TaxableAmount { get; internal set; }
			public int TaxGiven { get; internal set; }
			public double WithholdingTaxPercentage { get; internal set; }
			public int ApplicableOnAmount { get; internal set; }
			public int TotalWhtTax => (int)Math.Ceiling(this.TaxableAmount * this.WithholdingTaxPercentage / 100.0d);
			public int RemainingWhtTax => this.TotalWhtTax - this.TaxGiven;
			public string Remarks
			{
				get
				{
					var remarks = $"Generated automatically for the Withholding Tax. Total Taxable Amount: {this.TaxableAmount}. Total WHT: {this.TotalWhtTax}.";
					if (this.TaxGiven > 0)
						remarks += $"Remaining WHT: {this.RemainingWhtTax}.";
					return remarks;
				}
			}
		}

		public sealed class WithHoldingTax
		{
			public int FinancialYear { get; internal set; }
			public DateTime MinDate => GetRange(this.FinancialYear).Min;
			public DateTime MaxDateExclusive => GetRange(this.FinancialYear).MaxExclusive;
			public int TaxableAmount { get; internal set; }
			public int Given { get; internal set; }
			public int Tax
			{
				get
				{
					if (this.WithholdingTaxSetting == null)
						return 0;
					if (this.TaxableAmount < this.WithholdingTaxSetting.ApplicableOnAmount)
						return 0;
					return (int)Math.Round(this.TaxableAmount * this.WithholdingTaxSetting.WithholdingTaxPercentage / 100d, MidpointRounding.AwayFromZero);
				}
			}
			public int Remaining => this.Tax - this.Given;
			public WithholdingTaxSetting WithholdingTaxSetting { get; internal set; }
		}

		public static int GetFinancialYear(DateTime date)
		{
			return date.Month <= 6 ? date.Year - 1 : date.Year;
		}

		public static (DateTime Min, DateTime MaxExclusive) GetRange(int financialYear)
		{
			return (new DateTime(financialYear, 7, 1), new DateTime(financialYear + 1, 7, 1));
		}

		public static (DateTime Min, DateTime MaxExclusive) GetRange(DateTime date)
		{
			return GetRange(GetFinancialYear(date));
		}

		internal static List<WithHoldingTax> GetWithHoldingTaxes(this AspireContext aspireContext, int? studentID, int? candidateAppliedProgramID, DateTime? createdDate)
		{
			var studentFees = aspireContext.StudentFees
				.Where(sf => (studentID != null && sf.StudentID == studentID.Value) || (candidateAppliedProgramID != null && sf.CandidateAppliedProgramID == candidateAppliedProgramID.Value))
				.Where(sf => sf.FeeType == (byte)StudentFee.FeeTypes.Challan);
			if (createdDate != null)
			{
				var (min, maxExclusive) = GetRange(createdDate.Value);
				studentFees = studentFees.Where(sf => min <= sf.CreatedDate && sf.CreatedDate < maxExclusive);
			}

			var studentFeeDetails = studentFees.SelectMany(sf => sf.StudentFeeDetails)
				.Select(sfd => new
				{
					sfd.StudentFeeDetailID,
					sfd.StudentFee.CreatedDate,
					sfd.FeeHeadID,
					sfd.FeeHead.InstituteID,
					sfd.StudentFee.ExemptWHT,
					sfd.FeeHead.WithholdingTaxApplicable,
					FeeHeadTypeEnum = (FeeHead.FeeHeadTypes)sfd.FeeHead.FeeHeadType,
					RuleTypeEnum = (FeeHead.RuleTypes)sfd.FeeHead.RuleType,
					sfd.GrandTotalAmount,
				}).ToList();

			var instituteID = studentFeeDetails.FirstOrDefault()?.InstituteID;
			if (instituteID == null)
				return null;
			var withholdingTaxSettings = aspireContext.WithholdingTaxSettings.Where(s => s.InstituteID == instituteID).ToList();

			return studentFeeDetails.GroupBy(sfd => new
			{
				FinanicalYear = GetFinancialYear(sfd.CreatedDate)
			}).Select(g =>
			{
				var withHoldingTax = new WithHoldingTax
				{
					FinancialYear = g.Key.FinanicalYear,
					WithholdingTaxSetting = withholdingTaxSettings.SingleOrDefault(wht => wht.FinancialYear == g.Key.FinanicalYear),
					Given = 0,
					TaxableAmount = 0,
				};
				foreach (var detail in g)
				{
					switch (detail.FeeHeadTypeEnum)
					{
						case FeeHead.FeeHeadTypes.Arrears:
						case FeeHead.FeeHeadTypes.TutionFee:
						case FeeHead.FeeHeadTypes.Custom:
							if (!detail.ExemptWHT && detail.WithholdingTaxApplicable)
							{
								switch (detail.RuleTypeEnum)
								{
									case FeeHead.RuleTypes.Credit:
										withHoldingTax.TaxableAmount = withHoldingTax.TaxableAmount + detail.GrandTotalAmount;
										break;
									case FeeHead.RuleTypes.Debit:
										withHoldingTax.TaxableAmount = withHoldingTax.TaxableAmount - detail.GrandTotalAmount;
										break;
									default:
										throw new NotImplementedEnumException(detail.RuleTypeEnum);
								}
							}
							break;
						case FeeHead.FeeHeadTypes.WithholdingTax:
							if (detail.WithholdingTaxApplicable)
								throw new InvalidOperationException($"WithholdingTax type head cannot be itself WithholdingTaxApplicable. {nameof(detail.StudentFeeDetailID)}: {detail.StudentFeeDetailID}.");
							switch (detail.RuleTypeEnum)
							{
								case FeeHead.RuleTypes.Credit:
									withHoldingTax.Given = withHoldingTax.Given + detail.GrandTotalAmount;
									break;
								case FeeHead.RuleTypes.Debit:
									withHoldingTax.Given = withHoldingTax.Given - detail.GrandTotalAmount;
									break;
								default:
									throw new NotImplementedEnumException(detail.RuleTypeEnum);
							}
							break;
						default:
							throw new NotImplementedEnumException(detail.FeeHeadTypeEnum);
					}
				}
				return withHoldingTax;
			}).ToList();
		}

		internal static StudentFee CalculateAndGenerateFeeChallanForWithholdingTax(this AspireContext aspireContext, int studentFeeID, int userLoginHistoryID)
		{
			var oldStudentFee = aspireContext.StudentFees.Where(sf => sf.StudentFeeID == studentFeeID).Select(sf => new
			{
				sf.StudentID,
				sf.CandidateAppliedProgramID,
				sf.CreatedDate,
				sf.NewAdmission,
				sf.SemesterID,
				sf.ExemptWHT,
				IsWithholdingTaxFeeChallan = sf.StudentFeeDetails.Any(sfd => sfd.FeeHead.FeeHeadType == (byte)FeeHead.FeeHeadTypes.WithholdingTax),
				FeeTypeEnum = (StudentFee.FeeTypes)sf.FeeType,
				StudentFeeChallans = sf.StudentFeeChallans.ToList(),
			}).Single();
			switch (oldStudentFee.FeeTypeEnum)
			{
				case StudentFee.FeeTypes.Attendance:
				case StudentFee.FeeTypes.Refund:
					return null;
				case StudentFee.FeeTypes.Challan:
					if (oldStudentFee.IsWithholdingTaxFeeChallan || oldStudentFee.ExemptWHT)
						return null;
					break;
				default:
					throw new NotImplementedEnumException(oldStudentFee.FeeTypeEnum);
			}
			int instituteID;
			if (oldStudentFee.StudentID != null)
				instituteID = aspireContext.Students.Where(s => s.StudentID == oldStudentFee.StudentID.Value).Select(s => s.AdmissionOpenProgram.Program.InstituteID).Single();
			else if (oldStudentFee.CandidateAppliedProgramID != null)
				instituteID = aspireContext.CandidateAppliedPrograms.Where(cap => cap.CandidateAppliedProgramID == oldStudentFee.CandidateAppliedProgramID.Value).Select(cap => cap.AdmissionOpenProgram.Program.InstituteID).Single();
			else
				throw new InvalidOperationException();

			DateTime minDate;
			DateTime maxDate;
			short financialYear;
			if (oldStudentFee.CreatedDate.Month <= 6)
			{
				minDate = new DateTime(oldStudentFee.CreatedDate.Year - 1, 7, 1);
				maxDate = new DateTime(oldStudentFee.CreatedDate.Year, 7, 1);
				financialYear = (short)minDate.Year;
			}
			else
			{
				minDate = new DateTime(oldStudentFee.CreatedDate.Year, 7, 1);
				maxDate = new DateTime(oldStudentFee.CreatedDate.Year + 1, 7, 1);
				financialYear = (short)minDate.Year;
			}

			var withholdingTaxSetting = aspireContext.WithholdingTaxSettings.SingleOrDefault(s => s.InstituteID == instituteID && s.FinancialYear == financialYear);
			if (withholdingTaxSetting == null)
				throw new InvalidOperationException($"{nameof(aspireContext.WithholdingTaxSettings)} not found for year {financialYear} and InstituteID {instituteID}.");

			if (withholdingTaxSetting.WithholdingTaxPercentage < 0)
				throw new InvalidOperationException($"WithholdingTaxSetting percentage {withholdingTaxSetting.WithholdingTaxPercentage} is invalid.");
			if (withholdingTaxSetting.WithholdingTaxPercentage <= 0)
				return null;

			var withHoldingTaxFeeHead = aspireContext.FeeHeads.SingleOrDefault(fh => fh.InstituteID == instituteID && fh.FeeHeadType == (byte)FeeHead.FeeHeadTypes.WithholdingTax);
			if (withHoldingTaxFeeHead == null)
				throw new InvalidOperationException($"Fee Head of type {FeeHead.FeeHeadTypes.WithholdingTax} not found for InstituteID {instituteID}.");

			var studentFeeDetails = aspireContext.StudentFees
				.Where(sf => (oldStudentFee.StudentID != null && sf.StudentID == oldStudentFee.StudentID) || (oldStudentFee.CandidateAppliedProgramID != null && sf.CandidateAppliedProgramID == oldStudentFee.CandidateAppliedProgramID))
				.Where(sf => minDate <= sf.CreatedDate && sf.CreatedDate < maxDate)
				.Where(sf => sf.FeeType == (byte)StudentFee.FeeTypes.Challan)
				.SelectMany(sf => sf.StudentFeeDetails)
				.Select(sfd => new
				{
					sfd.FeeHeadID,
					sfd.GrandTotalAmount,
					FeeHeadTypeEnum = (FeeHead.FeeHeadTypes)sfd.FeeHead.FeeHeadType,
					RuleTypeEnum = (FeeHead.RuleTypes)sfd.FeeHead.RuleType,
					sfd.FeeHead.WithholdingTaxApplicable,
					sfd.StudentFee.ExemptWHT
				}).ToList();

			var taxableAmount = 0;
			var taxGiven = 0;
			foreach (var studentFeeDetail in studentFeeDetails)
			{
				if (studentFeeDetail.FeeHeadTypeEnum == FeeHead.FeeHeadTypes.WithholdingTax)
					taxGiven += studentFeeDetail.GrandTotalAmount;
				if (studentFeeDetail.WithholdingTaxApplicable && !studentFeeDetail.ExemptWHT)
				{
					if (studentFeeDetail.FeeHeadTypeEnum == FeeHead.FeeHeadTypes.WithholdingTax)
						throw new InvalidOperationException($"WithholdingTaxApplicable is not valid for FeeHead. FeeHeadID: {studentFeeDetail.FeeHeadID}.");
					switch (studentFeeDetail.RuleTypeEnum)
					{
						case FeeHead.RuleTypes.Credit:
							taxableAmount = taxableAmount + studentFeeDetail.GrandTotalAmount;
							break;
						case FeeHead.RuleTypes.Debit:
							taxableAmount = taxableAmount - studentFeeDetail.GrandTotalAmount;
							break;
						default:
							throw new NotImplementedEnumException(studentFeeDetail.RuleTypeEnum);
					}
				}
			}

			var whtChallan = new WHTChallan
			{
				ApplicableOnAmount = withholdingTaxSetting.ApplicableOnAmount,
				TaxableAmount = taxableAmount,
				WithholdingTaxPercentage = withholdingTaxSetting.WithholdingTaxPercentage,
				TaxGiven = taxGiven,
				StudentID = oldStudentFee.StudentID,
				CandidateAppliedProgramID = oldStudentFee.CandidateAppliedProgramID,
				CreatedDate = oldStudentFee.CreatedDate,
				DueDate = oldStudentFee.StudentFeeChallans.Single().DueDate,
				FeeHeadID = withHoldingTaxFeeHead.FeeHeadID,
				NewAdmission = oldStudentFee.NewAdmission,
				SemesterID = oldStudentFee.SemesterID,
				InstituteID = instituteID
			};

			return aspireContext.GenerateFeeChallanForWithholdingTax(whtChallan, userLoginHistoryID);
		}

		private static StudentFee GenerateFeeChallanForWithholdingTax(this AspireContext aspireContext, WHTChallan whtChallan, int userLoginHistoryID)
		{
			if (whtChallan.TaxableAmount < whtChallan.ApplicableOnAmount)
				return null;

			if (whtChallan.RemainingWhtTax <= 0)
				return null;

			var studentFee = new StudentFee
			{
				StudentID = whtChallan.StudentID,
				CandidateAppliedProgramID = whtChallan.CandidateAppliedProgramID,
				CreatedByUserLoginHistoryID = userLoginHistoryID,
				CreatedDate = whtChallan.CreatedDate,
				CreditHours = 0,
				FeeTypeEnum = StudentFee.FeeTypes.Challan,
				TotalAmount = whtChallan.RemainingWhtTax,
				ConcessionAmount = 0,
				GrandTotalAmount = whtChallan.RemainingWhtTax,
				NewAdmission = whtChallan.NewAdmission,
				SemesterID = whtChallan.SemesterID,
				NoOfInstallmentsEnum = InstallmentNos.One,
				Remarks = whtChallan.Remarks,
				StatusEnum = StudentFee.Statuses.NotPaid,
				StudentFeeConcessionTypes = new List<StudentFeeConcessionType>(),
				StudentFeeDetails = new List<StudentFeeDetail>
				{
					new StudentFeeDetail
					{
						FeeHeadID = whtChallan.FeeHeadID,
						TotalAmount = whtChallan.RemainingWhtTax,
						ConcessionAmount = null,
						GrandTotalAmount = whtChallan.RemainingWhtTax
					}
				},
				StudentFeeChallans = new List<StudentFeeChallan>
				{
					new StudentFeeChallan
					{
						Amount = whtChallan.RemainingWhtTax,
						DueDate = whtChallan.DueDate,
						InstallmentNoEnum = InstallmentNos.One,
						AccountTransactionID = null,
						StatusEnum = StudentFeeChallan.Statuses.NotPaid,
					}
				},
				ExemptWHT = true
			};
			return aspireContext.AddStudentFee(studentFee, null, whtChallan.InstituteID, userLoginHistoryID).Single();
		}
	}
}