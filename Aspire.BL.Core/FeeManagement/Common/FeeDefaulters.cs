﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.EmailsManagement;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.FeeManagement.Common
{
	public static class FeeDefaulters
	{
		//		public sealed class FeeDefaulterWarning
		//		{
		//			private string FullChallanNo { get; }
		//			private DateTime? DueDate { get; }
		//			private short SemesterID { get; }

		//			public string ToString(bool forStudent)
		//			{
		//				return forStudent
		//? $"You will be considered fee defaulter if you are unable to pay the challan before the Due Date. Challan No.: {this.FullChallanNo}, Semester: {this.SemesterID.ToSemesterString()}, Due Date: {this.DueDate:D}."
		//: $"Student will be considered fee defaulter if he is unable to pay the challan before the Due Date. Challan No.: {this.FullChallanNo}, Semester: {this.SemesterID.ToSemesterString()}, Due Date: {this.DueDate:D}.";
		//			}

		//			internal FeeDefaulterWarning(short semesterID, string fullChallanNo, DateTime dueDate)
		//			{
		//				this.SemesterID = semesterID;
		//				this.FullChallanNo = fullChallanNo.CannotBeEmptyOrWhitespace();
		//				this.DueDate = dueDate;
		//			}

		//			internal static FeeDefaulterWarning NewDueDatePassed(short semesterID, string fullChallanNo, DateTime dueDate)
		//			{
		//				return new FeeDefaulterWarning(semesterID, fullChallanNo, dueDate);
		//			}
		//		}

		public sealed class FeeDefaulterReason
		{
			private enum ReasonTypes
			{
				FeeChallanNotGeneratedForCourse,
				DueDatePassed,
				DueDateNear,
			}

			private ReasonTypes ReasonTypeEnum { get; }
			private string CourseTitle { get; }
			private string FullChallanNo { get; }
			private DateTime? DueDate { get; }
			private short SemesterID { get; }
			public short FeeDefaulterJobSemesterID { get; }
			public bool IsWarning { get; }
			public DateTime? ClassesStartDate { get; }

			public string ToString(bool forStudent)
			{
				switch (this.ReasonTypeEnum)
				{
					case ReasonTypes.FeeChallanNotGeneratedForCourse:
						return $"Fee Challan is not generated for the \"{this.CourseTitle}\" course of {this.SemesterID.ToSemesterString()} semester.";
					case ReasonTypes.DueDatePassed:
						return forStudent
							? $"You are considered as Defaulter because you are unable to pay the challan before the Due Date. Challan No.: {this.FullChallanNo}, Semester: {this.SemesterID.ToSemesterString()}, Due Date: {this.DueDate:D}."
							: $"Student is considered as Defaulter because he is unable to pay the challan before the Due Date. Challan No.: {this.FullChallanNo}, Semester: {this.SemesterID.ToSemesterString()}, Due Date: {this.DueDate:D}.";
					case ReasonTypes.DueDateNear:
						return forStudent
							? $"You will be considered as Defaulter if you are unable to pay the challan before the Due Date. Challan No.: {this.FullChallanNo}, Semester: {this.SemesterID.ToSemesterString()}, Due Date: {this.DueDate:D}."
							: $"Student will be considered as Defaulter if he is unable to pay the challan before the Due Date. Challan No.: {this.FullChallanNo}, Semester: {this.SemesterID.ToSemesterString()}, Due Date: {this.DueDate:D}.";
					default:
						throw new NotImplementedEnumException(this.ReasonTypeEnum);
				}
			}

			private FeeDefaulterReason(ReasonTypes reasonTypeEnum, short feeDefaulterJobSemesterID, short semesterID, string courseTitle, string fullChallanNo, DateTime? dueDate, DateTime? classesStartDate, bool isWarning)
			{
				this.ReasonTypeEnum = reasonTypeEnum;
				this.FeeDefaulterJobSemesterID = feeDefaulterJobSemesterID;
				this.SemesterID = semesterID;
				this.CourseTitle = courseTitle;
				this.FullChallanNo = fullChallanNo;
				this.DueDate = dueDate;
				this.IsWarning = isWarning;
				this.ClassesStartDate = classesStartDate;
			}

			internal static FeeDefaulterReason NewFeeChallanNotGeneratedForCourse(short feeDefaulterJobSemesterID, short semesterID, string courseTitle, DateTime? classesStartDate, bool isWarning)
			{
				return new FeeDefaulterReason(ReasonTypes.FeeChallanNotGeneratedForCourse, feeDefaulterJobSemesterID, semesterID, courseTitle.TrimAndCannotBeEmpty(), null, null, classesStartDate, isWarning);
			}

			internal static FeeDefaulterReason NewDueDatePassed(short feeDefaulterJobSemesterID, short semesterID, string fullChallanNo, DateTime dueDate, DateTime? classesStartDate, bool isWarning)
			{
				return new FeeDefaulterReason(ReasonTypes.DueDatePassed, feeDefaulterJobSemesterID, semesterID, null, fullChallanNo ?? throw new ArgumentNullException(nameof(fullChallanNo)), dueDate, classesStartDate, isWarning);
			}

			internal static FeeDefaulterReason NewDueDateNear(short feeDefaulterJobSemesterID, short semesterID, string fullChallanNo, DateTime dueDate, DateTime? classesStartDate)
			{
				return new FeeDefaulterReason(ReasonTypes.DueDateNear, feeDefaulterJobSemesterID, semesterID, null, fullChallanNo ?? throw new ArgumentNullException(nameof(fullChallanNo)), dueDate, classesStartDate, true);
			}
		}

		internal static List<FeeDefaulterReason> GetFeeDefaulterReasons(this AspireContext aspireContext, int studentID, string instituteCode, int instituteID, short feeDefaulterJobSemesterID)
		{
			var reasons = new List<FeeDefaulterReason>();
			var registeredCourses = aspireContext.RegisteredCourses
				.Where(rc => rc.DeletedDate == null)
				.Where(rc => rc.OfferedCours.SemesterID == feeDefaulterJobSemesterID)
				.Where(rc => rc.StudentID == studentID)
				.Select(rc => new
				{
					OfferedSemesterID = rc.OfferedCours.SemesterID,
					rc.Cours.Title,
					rc.StudentFeeID,
				})
				.ToList();
			var instituteSemester = aspireContext.InstituteSemesters
				.Where(@is => @is.SemesterID == feeDefaulterJobSemesterID && @is.InstituteID == instituteID)
				.Select(@is => new { @is.ClassesStartDate })
				.SingleOrDefault();

			var classesStartDate = instituteSemester?.ClassesStartDate;
			var ignoreReasons = classesStartDate != null && DateTime.Today < classesStartDate.Value.AddDays(14);

			var studentFees = aspireContext.StudentFees.Include(sf => sf.StudentFeeChallans).Where(sf => sf.StudentID == studentID).ToList();

			foreach (var registeredCourse in registeredCourses)
				if (registeredCourse.StudentFeeID == null)
					reasons.Add(FeeDefaulterReason.NewFeeChallanNotGeneratedForCourse(feeDefaulterJobSemesterID, registeredCourse.OfferedSemesterID, registeredCourse.Title, classesStartDate, ignoreReasons));

			foreach (var studentFee in studentFees)
				switch (studentFee.FeeTypeEnum)
				{
					case StudentFee.FeeTypes.Refund:
						break;
					case StudentFee.FeeTypes.Challan:
						switch (studentFee.StatusEnum)
						{
							case StudentFee.Statuses.Paid:
								break;
							case StudentFee.Statuses.NotPaid:
								var studentFeeChallan = studentFee.StudentFeeChallans.SingleOrDefault();
								if (studentFeeChallan != null)
									switch (studentFeeChallan.StatusEnum)
									{
										case StudentFeeChallan.Statuses.NotPaid:
											if (DateTime.Today <= studentFeeChallan.DueDate)
											{
												if (DateTime.Today.AddDays(3) >= studentFeeChallan.DueDate)
													reasons.Add(FeeDefaulterReason.NewDueDateNear(feeDefaulterJobSemesterID, studentFeeChallan.StudentFee.SemesterID, studentFeeChallan.GetFullChallanNo(instituteCode), studentFeeChallan.DueDate, classesStartDate));
											}
											else
												reasons.Add(FeeDefaulterReason.NewDueDatePassed(feeDefaulterJobSemesterID, studentFeeChallan.StudentFee.SemesterID, studentFeeChallan.GetFullChallanNo(instituteCode), studentFeeChallan.DueDate, classesStartDate, ignoreReasons));
											break;
										case StudentFeeChallan.Statuses.Paid:
											break;
										default:
											throw new NotImplementedEnumException(studentFeeChallan.StatusEnum);
									}
								break;
							default:
								throw new NotImplementedEnumException(studentFee.StatusEnum);
						}
						break;
					case StudentFee.FeeTypes.Attendance:
						break;
					default:
						throw new NotImplementedEnumException(studentFee.FeeTypeEnum);
				}
			return reasons;
		}

		private static List<FeeDefaulterReason> ProcessIndividualFeeDefaulter(this AspireContext aspireContext, int studentID, int userLoginHistoryID, bool sendEmailAndCommitTransaction)
		{
			var reasonsList = new List<FeeDefaulterReason>();
			var student = aspireContext.Students
				.Where(s => s.StudentID == studentID)
				.Select(s => new
				{
					s.StudentID,
					s.UniversityEmail,
					s.PersonalEmail,
					s.Enrollment,
					s.Name,
					s.AdmissionOpenProgram.Program.Institute.InstituteCode,
					s.AdmissionOpenProgram.Program.InstituteID,
					IntakeSemesterID = s.AdmissionOpenProgram.SemesterID
				}).Single();
			var feeDefaulterJobSemesters = aspireContext.FeeDefaulterJobSemesters
				.Where(fdjs => fdjs.SemesterID >= student.IntakeSemesterID && fdjs.InstituteID == student.InstituteID && fdjs.Status == (byte)FeeDefaulterJobSemester.Statuses.Active)
				.ToList();
			foreach (var feeDefaulterJobSemester in feeDefaulterJobSemesters)
			{
				var reasons = aspireContext.GetFeeDefaulterReasons(studentID, student.InstituteCode, student.InstituteID, feeDefaulterJobSemester.SemesterID);
				var feeDefaulter = reasons.Any(r => !r.IsWarning);
				aspireContext.RegisteredCourses
					.Where(rc => rc.StudentID == studentID && rc.DeletedDate == null && rc.OfferedCours.SemesterID == feeDefaulterJobSemester.SemesterID)
					.ToList()
					.ForEach(rc => rc.FeeDefaulter = feeDefaulter);
				aspireContext.SaveChanges(userLoginHistoryID);
				reasonsList.AddRange(reasons);
			}

			if (sendEmailAndCommitTransaction)
			{
				if (reasonsList.Any())
				{
					reasonsList = reasonsList.Where(r => !(r.FeeDefaulterJobSemesterID == student.IntakeSemesterID && (r.ClassesStartDate == null || r.ClassesStartDate.Value <= DateTime.Today))).ToList();
					var reasons = reasonsList.FindAll(r => !r.IsWarning);
					if (reasons.Any())
						aspireContext.SendDefaulterEmail(student.StudentID, student.UniversityEmail, student.PersonalEmail, student.Enrollment, student.Name, reasons, userLoginHistoryID);
					else
					{
						var warnings = reasonsList.FindAll(r => r.IsWarning);
						if (warnings.Any())
							aspireContext.SendDefaulterWarningEmail(student.UniversityEmail, student.PersonalEmail, student.Enrollment, student.Name, warnings, userLoginHistoryID);
					}
				}
				aspireContext.CommitTransaction();
			}

			return reasonsList;
		}

		internal static List<FeeDefaulterReason> ProcessIndividualFeeDefaulter(this AspireContext aspireContext, int studentID, int userLoginHistoryID)
		{
			return aspireContext.ProcessIndividualFeeDefaulter(studentID, userLoginHistoryID, false);
		}

		internal static List<FeeDefaulterReason> ProcessIndividualFeeDefaulterAndSendEmails(int studentID)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var userLoginHistoryID = aspireContext.GetUserLoginHistoryID(UserLoginHistory.SystemLoginSessionGuid);
				return aspireContext.ProcessIndividualFeeDefaulter(studentID, userLoginHistoryID, true);
			}
		}

		private static void SendDefaulterEmail(this AspireContext aspireContext, int studentID, string universityEmail, string personalEmail, string enrollment, string studentName, IEnumerable<FeeDefaulterReason> reasons, int userLoginHistoryID)
		{
			var emailBody = Properties.Resources.FeeDefaulter
				.Replace("{StudentNameHttpEncoded}", studentName.HtmlEncode())
				.Replace("{EnrollmentHttpEncoded}", enrollment.HtmlEncode())
				.Replace("{ReasonsHtml}", $"<ul>{reasons.Select(r => $"<li>{r.ToString(true).HtmlEncode()}</li>").Distinct().Join("")}</ul>");
			var subject = "Fee Defaulter";
			var checksum = new { subject, emailBody }.ToJsonString().ComputeHashMD5();
			var createdDate = DateTime.Now;
			var today = createdDate.Date;
			if (aspireContext.StudentFeeDefaulterEmails.Any(sfde => sfde.StudentID == studentID && sfde.EmailSentDate == today && sfde.EmailMd5Checksum == checksum))
				return;
			var expiryDate = today.AddDays(1);
			personalEmail = personalEmail.TryValidateEmailAddress();
			universityEmail = universityEmail.TryValidateEmailAddress();
			var toList = new List<(string, string)>();
			if (personalEmail != null)
				toList.Add((studentName, personalEmail));
			if (universityEmail != null)
				toList.Add((studentName, universityEmail));
			if (!toList.Any())
				return;
			aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.FeeDefaulter, subject, emailBody, true, Email.Priorities.High, createdDate, expiryDate, userLoginHistoryID, toList: toList);
			aspireContext.StudentFeeDefaulterEmails.Add(new StudentFeeDefaulterEmail
			{
				StudentFeeDefaulterEmailID = Guid.NewGuid(),
				EmailMd5Checksum = checksum,
				EmailSentDate = today,
				StudentID = studentID
			});
			aspireContext.SaveChangesAsSystemUser();
		}

		private static void SendDefaulterWarningEmail(this AspireContext aspireContext, string universityEmail, string personalEmail, string enrollment, string studentName, IEnumerable<FeeDefaulterReason> warnings, int userLoginHistoryID)
		{
			var emailBody = Properties.Resources.FeeDefaulterWarning
				.Replace("{StudentNameHttpEncoded}", studentName.HtmlEncode())
				.Replace("{EnrollmentHttpEncoded}", enrollment.HtmlEncode())
				.Replace("{ReasonsHtml}", $"<ul>{warnings.Select(r => $"<li>{r.ToString(true).HtmlEncode()}</li>").Distinct().Join("")}</ul>");
			var subject = "Warning - Fee Defaulter";
			var createdDate = DateTime.Now;
			var expiryDate = createdDate.AddHours(12);
			personalEmail = personalEmail.TryValidateEmailAddress();
			universityEmail = universityEmail.TryValidateEmailAddress();
			var toList = new List<(string, string)>();
			if (personalEmail != null)
				toList.Add((studentName, personalEmail));
			if (universityEmail != null)
				toList.Add((studentName, universityEmail));
			if (!toList.Any())
				return;
			aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.FeeDefaulter, subject, emailBody, true, Email.Priorities.Normal, createdDate, expiryDate, userLoginHistoryID, toList: toList);
		}
	}
}
