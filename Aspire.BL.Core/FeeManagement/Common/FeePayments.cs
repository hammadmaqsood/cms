﻿using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.FeeManagement.Common
{
	public static class FeePayments
	{
		public sealed class PayFeeResult
		{
			public AccountTransaction AccountTransaction { get; }
			public AccountTransactionErrors AccountTransactionError { get; }

			private PayFeeResult(AccountTransaction accountTransaction, AccountTransactionErrors accountTransactionError)
			{
				this.AccountTransaction = accountTransaction;
				this.AccountTransactionError = accountTransactionError;
			}

			public static PayFeeResult Success(AccountTransaction accountTransaction)
			{
				if (accountTransaction == null)
					throw new ArgumentNullException(nameof(accountTransaction));
				return new PayFeeResult(accountTransaction, AccountTransactionErrors.None);
			}

			public static PayFeeResult Error(AccountTransactionErrors accountTransactionError)
			{
				if (accountTransactionError == AccountTransactionErrors.None)
					throw new InvalidOperationException();
				return new PayFeeResult(null, accountTransactionError);
			}
		}

		private static AccountTransactionErrors CanPayAdmissionProcessingFee(this AspireContext aspireContext, int candidateAppliedProgramID, int amount, bool ignoreDueDate)
		{
			var candidateAppliedProgram = aspireContext.CandidateAppliedPrograms
				.Where(cap => cap.CandidateAppliedProgramID == candidateAppliedProgramID)
				.Select(cap => new
				{
					cap.AccountTransactionID,
					cap.AdmissionOpenProgram1.AdmissionProcessingFeeDueDate,
					cap.Amount,
				}).SingleOrDefault();
			if (candidateAppliedProgram == null)
				return AccountTransactionErrors.InvalidChallanNo;

			if (candidateAppliedProgram.AccountTransactionID != null)
				return AccountTransactionErrors.FeeChallanAlreadyPaid;

			if (candidateAppliedProgram.AdmissionProcessingFeeDueDate.Date < DateTime.Today && !ignoreDueDate)
				return AccountTransactionErrors.AdmissionProcessingFeeDueDateIsPassed;

			if (candidateAppliedProgram.Amount != amount)
				return AccountTransactionErrors.AdmissionProcessingFeeAmountMismatched;

			return AccountTransactionErrors.None;
		}

		private static AccountTransactionErrors CanPayStudentFee(this AspireContext aspireContext, int studentFeeChallanID, bool newAdmission, int amount, bool ignoreDueDate)
		{
			var studentFeeChallan = aspireContext.StudentFeeChallans
				.Where(sfc => sfc.StudentFeeChallanID == studentFeeChallanID)
				.Where(sfc => sfc.StudentFee.NewAdmission == newAdmission)
				.Select(sfc => new
				{
					sfc.AccountTransactionID,
					sfc.Amount,
					sfc.DueDate,
					sfc.StudentFee.StudentID,
					sfc.StudentFee.CandidateAppliedProgramID,
					sfc.StudentFee.NewAdmission,
				}).SingleOrDefault();
			if (studentFeeChallan == null)
				return AccountTransactionErrors.InvalidChallanNo;

			if (studentFeeChallan.AccountTransactionID != null)
				return AccountTransactionErrors.FeeChallanAlreadyPaid;

			if (studentFeeChallan.DueDate.Date < DateTime.Today && !ignoreDueDate)
				return newAdmission ? AccountTransactionErrors.NewAdmissionFeeDueDateIsPassed : AccountTransactionErrors.StudentFeeDueDateIsPassed;

			if (studentFeeChallan.Amount != amount)
				return newAdmission ? AccountTransactionErrors.NewAdmissionFeeAmountMismatched : AccountTransactionErrors.StudentFeeAmountMismatched;

			return AccountTransactionErrors.None;
		}

		private static PayFeeResult PayAdmissionProcessingFee(this AspireContext aspireContext, string instituteCode, int candidateAppliedProgramID, AccountTransaction accountTransaction, bool ignoreDueDate, bool ignoreAccountType)
		{
			var error = aspireContext.Validate(accountTransaction, instituteCode, ignoreAccountType);
			if (error != AccountTransactionErrors.None)
				return PayFeeResult.Error(error);
			var accountTransactionError = aspireContext.CanPayAdmissionProcessingFee(candidateAppliedProgramID, accountTransaction.Amount, ignoreDueDate);
			if (accountTransactionError != AccountTransactionErrors.None)
				return PayFeeResult.Error(accountTransactionError);
			var candidateAppliedProgram = aspireContext.CandidateAppliedPrograms.Single(cap => cap.CandidateAppliedProgramID == candidateAppliedProgramID);
			aspireContext.AccountTransactions.Add(accountTransaction);
			aspireContext.SaveChanges(accountTransaction.UserLoginHistoryID);
			candidateAppliedProgram.AccountTransactionID = accountTransaction.AccountTransactionID;
			aspireContext.SaveChanges(accountTransaction.UserLoginHistoryID);
			return PayFeeResult.Success(accountTransaction);
		}

		private static PayFeeResult PayStudentFee(this AspireContext aspireContext, string instituteCode, int studentFeeChallanID, bool newAdmission, AccountTransaction accountTransaction, bool ignoreDueDate, bool ignoreAccountType)
		{
			var error = aspireContext.Validate(accountTransaction, instituteCode, ignoreAccountType);
			if (error != AccountTransactionErrors.None)
				return PayFeeResult.Error(error);
			var accountTransactionError = aspireContext.CanPayStudentFee(studentFeeChallanID, newAdmission, accountTransaction.Amount, ignoreDueDate);
			if (accountTransactionError != AccountTransactionErrors.None)
				return PayFeeResult.Error(accountTransactionError);
			var studentFeeChallan = aspireContext.StudentFeeChallans.Single(sfc => sfc.StudentFeeChallanID == studentFeeChallanID);
			aspireContext.AccountTransactions.Add(accountTransaction);
			aspireContext.SaveChanges(accountTransaction.UserLoginHistoryID);
			studentFeeChallan.AccountTransactionID = accountTransaction.AccountTransactionID;
			aspireContext.SaveChanges(accountTransaction.UserLoginHistoryID);
			return PayFeeResult.Success(accountTransaction);
		}

		private static readonly object PayFeeLock = new object();

		private static PayFeeResult PayFee(this AspireContext aspireContext, AccountTransaction accountTransaction, bool ignoreDueDate, bool ignoreAccountType)
		{
			var challanNo= AspireFormats.ChallanNo.TryParse(accountTransaction.FullChallanNo);
			if (challanNo == null)
				return PayFeeResult.Error(AccountTransactionErrors.ChallanNoFormatIsWrong);
			switch (challanNo.Value.FeeTypeEnum)
			{
				case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
					return aspireContext.PayAdmissionProcessingFee(challanNo.Value.InstituteCode, challanNo.Value.ChallanID, accountTransaction, ignoreDueDate, ignoreAccountType);
				case InstituteBankAccount.FeeTypes.NewAdmissionFee:
					return aspireContext.PayStudentFee(challanNo.Value.InstituteCode, challanNo.Value.ChallanID, true, accountTransaction, ignoreDueDate, ignoreAccountType);
				case InstituteBankAccount.FeeTypes.StudentFee:
					return aspireContext.PayStudentFee(challanNo.Value.InstituteCode, challanNo.Value.ChallanID, false, accountTransaction, ignoreDueDate, ignoreAccountType);
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private static AccountTransactionErrors Validate(this AspireContext aspireContext, AccountTransaction accountTransaction, string instituteCode, bool ignoreAccountType)
		{
			var challanNo= AspireFormats.ChallanNo.TryParse(accountTransaction.FullChallanNo);
			if (challanNo == null)
				return AccountTransactionErrors.ChallanNoFormatIsWrong;
			var instituteBankAccount = aspireContext.InstituteBankAccounts
				.Where(iba => iba.InstituteBankAccountID == accountTransaction.InstituteBankAccountID)
				.Where(iba => iba.Institute.InstituteCode == instituteCode)
				.Select(iba => new
				{
					iba.Active,
					AccountTypeEnum = (InstituteBankAccount.AccountTypes)iba.AccountType,
					FeeTypeEnum = (InstituteBankAccount.FeeTypes)iba.FeeType,
				})
				.SingleOrDefault();
			if (instituteBankAccount == null)
				return AccountTransactionErrors.InstituteBankAccountIDNotFound;
			if (instituteBankAccount.FeeTypeEnum != challanNo.Value.FeeTypeEnum)
				return AccountTransactionErrors.InstituteBankAccountFeeTypeDoesNotMatch;
			switch (accountTransaction.ServiceEnum)
			{
				case AccountTransaction.Services.Campus:
					accountTransaction.ValidateForCampus();
					break;
				case AccountTransaction.Services.BankAlfalah:
					if (!instituteBankAccount.AccountTypeEnum.HasFlag(InstituteBankAccount.AccountTypes.Online) && !ignoreAccountType)
						return AccountTransactionErrors.AccountTypeIsNotOnline;
					accountTransaction.ValidateForBankAlfalah();
					break;
				case AccountTransaction.Services.AlliedBank:
					if (!instituteBankAccount.AccountTypeEnum.HasFlag(InstituteBankAccount.AccountTypes.Online) && !ignoreAccountType)
						return AccountTransactionErrors.AccountTypeIsNotOnline;
					accountTransaction.ValidateForAlliedBank();
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(accountTransaction.ServiceEnum), accountTransaction.ServiceEnum, null);
			}
			return AccountTransactionErrors.None;
		}

		private static AccountTransactionErrors ValidateForBankAlfalah(this AccountTransaction accountTransaction)
		{
			if (accountTransaction.ServiceEnum != AccountTransaction.Services.BankAlfalah)
				throw new InvalidOperationException();
			switch (accountTransaction.TransactionChannelEnum)
			{
				case AccountTransaction.TransactionChannels.Manual:
				case AccountTransaction.TransactionChannels.ContactCenter:
					throw new NotSupportedException();
				case AccountTransaction.TransactionChannels.ATM:
				case AccountTransaction.TransactionChannels.MobileBanking:
				case AccountTransaction.TransactionChannels.InternetBanking:
					switch (accountTransaction.PaymentTypeEnum)
					{
						case AccountTransaction.PaymentTypes.Cash:
							throw new InvalidOperationException();
						case AccountTransaction.PaymentTypes.Account:
							break;
						default:
							throw new ArgumentOutOfRangeException(nameof(accountTransaction.PaymentTypeEnum), accountTransaction.PaymentTypeEnum, null);
					}
					break;
				case AccountTransaction.TransactionChannels.Branch:
					switch (accountTransaction.PaymentTypeEnum)
					{
						case AccountTransaction.PaymentTypes.Cash:
							break;
						case AccountTransaction.PaymentTypes.Account:
							throw new InvalidOperationException();
						default:
							throw new ArgumentOutOfRangeException(nameof(accountTransaction.PaymentTypeEnum), accountTransaction.PaymentTypeEnum, null);
					}
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(accountTransaction.PaymentTypeEnum), accountTransaction.PaymentTypeEnum, null);
			}
			if (accountTransaction.Amount <= 0)
				return AccountTransactionErrors.AmountMustBeGreaterThanZero;
			accountTransaction.TransactionReferenceID = accountTransaction.TransactionReferenceID.TrimAndCannotBeEmpty();
			accountTransaction.TransactionDate.Date.CannotBeFutureDate();
			accountTransaction.AccountNo = accountTransaction.AccountNo.TrimAndMakeItNullIfEmpty();
			accountTransaction.AccountTitle = accountTransaction.AccountTitle.TrimAndMakeItNullIfEmpty();
			accountTransaction.BranchCode = accountTransaction.BranchCode.TrimAndMakeItNullIfEmpty();
			accountTransaction.BranchName = accountTransaction.BranchName.TrimAndMakeItNullIfEmpty();
			accountTransaction.Remarks = accountTransaction.Remarks.TrimAndMakeItNullIfEmpty();
			return AccountTransactionErrors.None;
		}

		private static AccountTransactionErrors ValidateForAlliedBank(this AccountTransaction accountTransaction)
		{
			if (accountTransaction.ServiceEnum != AccountTransaction.Services.AlliedBank)
				throw new InvalidOperationException();
			switch (accountTransaction.TransactionChannelEnum)
			{
				case AccountTransaction.TransactionChannels.Manual:
					throw new NotSupportedException();
				case AccountTransaction.TransactionChannels.ContactCenter:
				case AccountTransaction.TransactionChannels.ATM:
				case AccountTransaction.TransactionChannels.MobileBanking:
				case AccountTransaction.TransactionChannels.InternetBanking:
					switch (accountTransaction.PaymentTypeEnum)
					{
						case AccountTransaction.PaymentTypes.Cash:
							throw new InvalidOperationException();
						case AccountTransaction.PaymentTypes.Account:
							break;
						default:
							throw new ArgumentOutOfRangeException(nameof(accountTransaction.PaymentTypeEnum), accountTransaction.PaymentTypeEnum, null);
					}
					break;
				case AccountTransaction.TransactionChannels.Branch:
					switch (accountTransaction.PaymentTypeEnum)
					{
						case AccountTransaction.PaymentTypes.Cash:
							break;
						case AccountTransaction.PaymentTypes.Account:
							throw new InvalidOperationException();
						default:
							throw new ArgumentOutOfRangeException(nameof(accountTransaction.PaymentTypeEnum), accountTransaction.PaymentTypeEnum, null);
					}
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(accountTransaction.PaymentTypeEnum), accountTransaction.PaymentTypeEnum, null);
			}
			if (accountTransaction.Amount <= 0)
				return AccountTransactionErrors.AmountMustBeGreaterThanZero;
			accountTransaction.TransactionReferenceID = accountTransaction.TransactionReferenceID.TrimAndCannotBeEmpty();
			accountTransaction.TransactionDate.Date.CannotBeFutureDate();
			accountTransaction.AccountNo = accountTransaction.AccountNo.TrimAndMakeItNullIfEmpty();
			accountTransaction.AccountTitle = accountTransaction.AccountTitle.TrimAndMakeItNullIfEmpty();
			accountTransaction.BranchCode = accountTransaction.BranchCode.TrimAndMakeItNullIfEmpty();
			accountTransaction.BranchName = accountTransaction.BranchName.TrimAndMakeItNullIfEmpty();
			accountTransaction.Remarks = accountTransaction.Remarks.TrimAndMakeItNullIfEmpty();
			return AccountTransactionErrors.None;
		}

		private static AccountTransactionErrors ValidateForCampus(this AccountTransaction accountTransaction)
		{
			if (accountTransaction.ServiceEnum != AccountTransaction.Services.BankAlfalah)
				throw new InvalidOperationException();
			switch (accountTransaction.TransactionChannelEnum)
			{
				case AccountTransaction.TransactionChannels.Manual:
					break;
				case AccountTransaction.TransactionChannels.ContactCenter:
				case AccountTransaction.TransactionChannels.ATM:
				case AccountTransaction.TransactionChannels.MobileBanking:
				case AccountTransaction.TransactionChannels.InternetBanking:
				case AccountTransaction.TransactionChannels.Branch:
					throw new NotSupportedException();
				default:
					throw new ArgumentOutOfRangeException(nameof(accountTransaction.PaymentTypeEnum), accountTransaction.PaymentTypeEnum, null);
			}
			accountTransaction.TransactionReferenceID = accountTransaction.TransactionReferenceID.TrimAndMakeItNullIfEmpty();
			accountTransaction.TransactionDate.Date.CannotBeFutureDate();
			accountTransaction.AccountNo = accountTransaction.AccountNo.TrimAndMakeItNullIfEmpty();
			accountTransaction.AccountTitle = accountTransaction.AccountTitle.TrimAndMakeItNullIfEmpty();
			accountTransaction.BranchCode = accountTransaction.BranchCode.TrimAndMakeItNullIfEmpty();
			accountTransaction.BranchName = accountTransaction.BranchName.TrimAndMakeItNullIfEmpty();
			accountTransaction.Remarks = accountTransaction.Remarks.TrimAndMakeItNullIfEmpty();
			return AccountTransactionErrors.None;
		}

		public static PayFeeResult PayFee(AccountTransaction accountTransaction, bool ignoreDueDate, bool ignoreAccountType, Guid loginSessionGuid)
		{
			//lock (PayFeeLock)
			{
				using (var aspireContext = new AspireContext(true))
				{
					var result = aspireContext.PayFee(accountTransaction, ignoreDueDate, ignoreAccountType);
					if (result == null)
						throw new InvalidOperationException();

					if (result.AccountTransactionError == AccountTransactionErrors.None)
						aspireContext.CommitTransaction();
					else
						aspireContext.RollbackTransaction();
					return result;
				}
			}
		}
	}
}
