namespace Aspire.BL.Core.FeeManagement.Common
{
	internal enum PaymentFailureReasons
	{
		None,
		InvalidChallanNoFormat,
		ChallanNotFound,
		FeeChallanAlreadyPaid,
		DueDateIsPassed,
		AmountMismatched,
		AmountMustBeGreaterThanZero,
		InstituteBankAccountNotFound
	}
}