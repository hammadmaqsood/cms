﻿using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.FeeManagement.Common
{
	public static class Helpers
	{
		public sealed class RawFeeChallan
		{
			public decimal FullChallanNo { get; internal set; }
			public bool NewAdmission { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Name { get; internal set; }
			public string Enrollment { get; internal set; }
			public int? ApplicationNo { get; internal set; }
			public int Amount { get; internal set; }
			public DateTime DueDate { get; internal set; }
			public DateTime? DepositDate { get; internal set; }
			public bool Paid { get; internal set; }
			public string BankAccountTitle { get; internal set; }
			public string BankAccountNo { get; internal set; }
			public string DepositedInBankName { get; internal set; }
			public string DepositedInAccountNo { get; internal set; }
			public int? AccountTransactionID { get; internal set; }
			public string TransactionReferenceID { get; internal set; }
			public AccountTransaction.Services? ServiceEnum { get; internal set; }
			public int InstituteID { get; internal set; }
			//public string InstituteCode { get; internal set; }
			//public int? DepartmentID { get; internal set; }
			//public int ProgramID { get; internal set; }
			//public int AdmissionOpenProgramID { get; internal set; }
			public DateTime? SystemDate { get; internal set; }
			public string LastUpdatedByUserName { get; internal set; }
			public UserTypes? LastUpdatedByUserType { get; internal set; }
		}

		private static PaymentFailureReasons CanPayAdmissionProcessingFee(this AspireContext aspireContext, AspireFormats.ChallanNo challanNo, int amount, bool ignoreDueDate, bool ignoreZeroOrNegativeAmount)
		{
			var candidateAppliedProgram = aspireContext.CandidateAppliedPrograms
				.Where(cap => cap.ChallanNo == challanNo.ChallanID)
				.Select(cap => new
				{
					cap.AccountTransactionID,
					cap.AdmissionOpenProgram1.AdmissionProcessingFeeDueDate,
					cap.Amount,
				}).SingleOrDefault();
			if (candidateAppliedProgram == null)
				return PaymentFailureReasons.ChallanNotFound;

			if (candidateAppliedProgram.AccountTransactionID != null)
				return PaymentFailureReasons.FeeChallanAlreadyPaid;

			if (!ignoreDueDate)
				if (candidateAppliedProgram.AdmissionProcessingFeeDueDate.Date < DateTime.Today)
					return PaymentFailureReasons.DueDateIsPassed;

			if (candidateAppliedProgram.Amount != amount)
				return PaymentFailureReasons.AmountMismatched;

			if (!ignoreZeroOrNegativeAmount && candidateAppliedProgram.Amount <= 0)
				return PaymentFailureReasons.AmountMustBeGreaterThanZero;

			return PaymentFailureReasons.None;
		}

		private static PaymentFailureReasons CanPayAdmissionProcessingFee(this AspireContext aspireContext, AspireFormats.ChallanNo challanNo, int amount, bool ignoreDueDate, bool ignoreZeroOrNegativeAmount, string bankName, InstituteBankAccount.AccountTypes[] accountTypeEnums)
		{
			var paymentFailureReason = aspireContext.CanPayAdmissionProcessingFee(challanNo, amount, ignoreDueDate, ignoreZeroOrNegativeAmount);
			if (paymentFailureReason != PaymentFailureReasons.None)
				return paymentFailureReason;
			var instituteBankAccount = aspireContext.GetInstituteBankAccount(challanNo.InstituteCode, challanNo.FeeTypeEnum, bankName, accountTypeEnums);
			if (instituteBankAccount == null)
				return PaymentFailureReasons.InstituteBankAccountNotFound;
			return paymentFailureReason;
		}

		private static PaymentFailureReasons CanPayStudentFee(this AspireContext aspireContext, AspireFormats.ChallanNo challanNo, int amount, bool ignoreDueDate, bool ignoreZeroOrNegativeAmount)
		{
			var studentFeeChallan = aspireContext.StudentFeeChallans
				.Where(sfc => sfc.StudentFeeChallanID == challanNo.ChallanID)
				.Select(sfc => new
				{
					sfc.AccountTransactionID,
					sfc.Amount,
					sfc.DueDate,
				}).SingleOrDefault();
			if (studentFeeChallan == null)
				return PaymentFailureReasons.ChallanNotFound;

			if (studentFeeChallan.AccountTransactionID != null)
				return PaymentFailureReasons.FeeChallanAlreadyPaid;

			if (!ignoreDueDate)
				if (studentFeeChallan.DueDate.Date < DateTime.Today)
					return PaymentFailureReasons.DueDateIsPassed;

			if (studentFeeChallan.Amount != amount)
				return PaymentFailureReasons.AmountMismatched;

			if (!ignoreZeroOrNegativeAmount && studentFeeChallan.Amount <= 0)
				return PaymentFailureReasons.AmountMustBeGreaterThanZero;

			return PaymentFailureReasons.None;
		}

		private static PaymentFailureReasons CanPayStudentFee(this AspireContext aspireContext, AspireFormats.ChallanNo challanNo, int amount, bool ignoreDueDate, bool ignoreZeroOrNegativeAmount, string bankName, InstituteBankAccount.AccountTypes[] accountTypeEnums)
		{
			var paymentFailureReason = aspireContext.CanPayStudentFee(challanNo, amount, ignoreDueDate, ignoreZeroOrNegativeAmount);
			if (paymentFailureReason != PaymentFailureReasons.None)
				return paymentFailureReason;
			var instituteBankAccount = aspireContext.GetInstituteBankAccount(challanNo.InstituteCode, challanNo.FeeTypeEnum, bankName, accountTypeEnums);
			if (instituteBankAccount == null)
				return PaymentFailureReasons.InstituteBankAccountNotFound;
			return paymentFailureReason;
		}

		internal static PaymentFailureReasons CanPay(this AspireContext aspireContext, AspireFormats.ChallanNo challanNo, int amount, bool ignoreDueDate, bool ignoreZeroOrNegativeAmount, string bankName, InstituteBankAccount.AccountTypes[] accountTypeEnums)
		{
			switch (challanNo.FeeTypeEnum)
			{
				case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
					return aspireContext.CanPayAdmissionProcessingFee(challanNo, amount, ignoreDueDate, ignoreZeroOrNegativeAmount, bankName, accountTypeEnums);
				case InstituteBankAccount.FeeTypes.NewAdmissionFee:
				case InstituteBankAccount.FeeTypes.StudentFee:
					return aspireContext.CanPayStudentFee(challanNo, amount, ignoreDueDate, ignoreZeroOrNegativeAmount, bankName, accountTypeEnums);
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		internal static RawFeeChallan GetRawFeeChallanDetails(this AspireContext aspireContext, AspireFormats.ChallanNo challanNo, bool ignoreZeroOrNegativeAmount)
		{
			var institute = aspireContext.Institutes.Where(i => i.InstituteCode == challanNo.InstituteCode)
				.Select(i => new
				{
					i.InstituteID
				}).SingleOrDefault();
			if (institute == null)
				return null;

			var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms
				.Where(aop => aop.Program.InstituteID == institute.InstituteID)
				.Select(aop => new
				{
					aop.SemesterID,
					aop.AdmissionOpenProgramID,
					aop.AdmissionProcessingFeeDueDate,
					aop.ProgramID,
					aop.Program.DepartmentID,
					aop.Program.InstituteID,
					aop.Program.Institute.InstituteCode,
				});

			switch (challanNo.FeeTypeEnum)
			{
				case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
					var admissionProcessingFeeQuery = from c in aspireContext.Candidates
													  join cap in aspireContext.CandidateAppliedPrograms on c.CandidateID equals cap.CandidateID
													  join aop in admissionOpenPrograms on cap.Choice1AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
													  where cap.ChallanNo == challanNo.ChallanID && (ignoreZeroOrNegativeAmount || cap.Amount > 0)
													  select new RawFeeChallan
													  {
														  FullChallanNo = challanNo.FullChallanNo,
														  SemesterID = aop.SemesterID,
														  ApplicationNo = cap.AccountTransactionID != null ? cap.ApplicationNo : null,
														  Enrollment = null,
														  Name = c.Name,
														  Amount = cap.Amount,
														  DueDate = aop.AdmissionProcessingFeeDueDate,
														  AccountTransactionID = cap.AccountTransactionID,
														  Paid = cap.AccountTransactionID != null,
														  DepositDate = cap.AccountTransaction.TransactionDate,
														  DepositedInBankName = cap.AccountTransaction.InstituteBankAccount.Bank.BankName,
														  DepositedInAccountNo = cap.AccountTransaction.InstituteBankAccount.AccountNo,
														  TransactionReferenceID = cap.AccountTransaction.TransactionReferenceID,
														  ServiceEnum = (AccountTransaction.Services?)cap.AccountTransaction.Service,
														  BankAccountNo = null,
														  BankAccountTitle = null,
														  InstituteID = aop.InstituteID,
														  //InstituteCode = aop.InstituteCode,
														  //ProgramID = aop.ProgramID,
														  //DepartmentID = aop.DepartmentID,
														  //AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
														  SystemDate = cap.AccountTransaction.SystemDate,
														  LastUpdatedByUserName = cap.AccountTransaction.UserLoginHistory.User.Name,
														  LastUpdatedByUserType = (UserTypes?)cap.AccountTransaction.UserLoginHistory.UserType,
														  NewAdmission = false
													  };
					return admissionProcessingFeeQuery.SingleOrDefault();
				case InstituteBankAccount.FeeTypes.StudentFee:
				case InstituteBankAccount.FeeTypes.NewAdmissionFee:
					var existingStudentQuery = from s in aspireContext.Students
											   join aop in admissionOpenPrograms on s.AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
											   join sf in aspireContext.StudentFees on s.StudentID equals sf.StudentID
											   join sfc in aspireContext.StudentFeeChallans on sf.StudentFeeID equals sfc.StudentFeeID
											   where sfc.StudentFeeChallanID == challanNo.ChallanID
													 && sf.NewAdmission == challanNo.NewAdmission
													 && sf.FeeType == StudentFee.FeeTypeChallan
													 && (ignoreZeroOrNegativeAmount || sfc.Amount > 0)
											   select new RawFeeChallan
											   {
												   FullChallanNo = challanNo.FullChallanNo,
												   SemesterID = sf.SemesterID,
												   ApplicationNo = null,
												   Enrollment = s.Enrollment,
												   Name = s.Name,
												   Amount = sfc.Amount,
												   DueDate = sfc.DueDate,
												   DepositDate = sfc.AccountTransaction.TransactionDate,
												   AccountTransactionID = sfc.AccountTransactionID,
												   Paid = sfc.Status == StudentFeeChallan.StatusPaid,
												   DepositedInBankName = sfc.AccountTransaction.InstituteBankAccount.Bank.BankName,
												   DepositedInAccountNo = sfc.AccountTransaction.InstituteBankAccount.AccountNo,
												   TransactionReferenceID = sfc.AccountTransaction.TransactionReferenceID,
												   ServiceEnum = (AccountTransaction.Services?)sfc.AccountTransaction.Service,
												   InstituteID = aop.InstituteID,
												   //InstituteCode = aop.InstituteCode,
												   //ProgramID = aop.ProgramID,
												   //DepartmentID = aop.DepartmentID,
												   //AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
												   BankAccountNo = null,
												   BankAccountTitle = null,
												   SystemDate = sfc.AccountTransaction.SystemDate,
												   LastUpdatedByUserName = sfc.AccountTransaction.UserLoginHistory.User.Name,
												   LastUpdatedByUserType = (UserTypes?)sfc.AccountTransaction.UserLoginHistory.UserType,
												   NewAdmission = sfc.StudentFee.NewAdmission
											   };
					var newAdmissionQuery = from c in aspireContext.Candidates
											join cap in aspireContext.CandidateAppliedPrograms on c.CandidateID equals cap.CandidateID
											join aop in admissionOpenPrograms on cap.AdmittedAdmissionOpenProgramID equals aop.AdmissionOpenProgramID
											join sf in aspireContext.StudentFees on cap.CandidateAppliedProgramID equals sf.CandidateAppliedProgramID
											join sfc in aspireContext.StudentFeeChallans on sf.StudentFeeID equals sfc.StudentFeeID
											where sfc.StudentFeeChallanID == challanNo.ChallanID
												  && challanNo.NewAdmission == true && sf.NewAdmission
												  && sf.FeeType == StudentFee.FeeTypeChallan
												  && (ignoreZeroOrNegativeAmount || sfc.Amount > 0)
											select new RawFeeChallan
											{
												FullChallanNo = challanNo.FullChallanNo,
												SemesterID = sf.SemesterID,
												ApplicationNo = cap.ApplicationNo,
												Enrollment = null,
												Name = c.Name,
												Amount = sfc.Amount,
												DueDate = sfc.DueDate,
												DepositDate = sfc.AccountTransaction.TransactionDate,
												AccountTransactionID = sfc.AccountTransactionID,
												Paid = sfc.Status == StudentFeeChallan.StatusPaid,
												DepositedInBankName = sfc.AccountTransaction.InstituteBankAccount.Bank.BankName,
												DepositedInAccountNo = sfc.AccountTransaction.InstituteBankAccount.AccountNo,
												TransactionReferenceID = sfc.AccountTransaction.TransactionReferenceID,
												ServiceEnum = (AccountTransaction.Services?)sfc.AccountTransaction.Service,
												InstituteID = aop.InstituteID,
												//InstituteCode = aop.InstituteCode,
												//ProgramID = aop.ProgramID,
												//DepartmentID = aop.DepartmentID,
												//AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
												BankAccountNo = null,
												BankAccountTitle = null,
												SystemDate = sfc.AccountTransaction.SystemDate,
												LastUpdatedByUserName = sfc.AccountTransaction.UserLoginHistory.User.Name,
												LastUpdatedByUserType = (UserTypes?)sfc.AccountTransaction.UserLoginHistory.UserType,
												NewAdmission = sfc.StudentFee.NewAdmission
											};
					return existingStudentQuery.SingleOrDefault() ?? newAdmissionQuery.SingleOrDefault();
				default:
					throw new ArgumentOutOfRangeException(nameof(challanNo.FeeTypeEnum), challanNo.FeeTypeEnum, null);
			}
		}

		private static RawFeeChallan GetRawFeeChallanDetails(this AspireContext aspireContext, AspireFormats.ChallanNo challanNo, int instituteBankAccountID, InstituteBankAccount.AccountTypes[] accountTypeEnums, bool ignoreZeroOrNegativeAmount)
		{
			var rawFeeChallan = aspireContext.GetRawFeeChallanDetails(challanNo, ignoreZeroOrNegativeAmount);
			if (rawFeeChallan == null)
				return null;

			var instituteBankAccount = aspireContext.InstituteBankAccounts
				.Where(iba => iba.InstituteBankAccountID == instituteBankAccountID)
				.Where(iba => iba.Institute.InstituteCode == challanNo.InstituteCode)
				.Where(iba => iba.FeeType == challanNo.FeeTypeByte)
				.Where(iba => iba.Active)
				.Select(iba => new
				{
					iba.AccountTitle,
					iba.AccountNo,
					AccountTypeEnum = (InstituteBankAccount.AccountTypes)iba.AccountType,
				}).ToList()
				.SingleOrDefault(iba => accountTypeEnums.Contains(iba.AccountTypeEnum));
			if (instituteBankAccount == null)
				return null;
			rawFeeChallan.BankAccountNo = instituteBankAccount.AccountNo;
			rawFeeChallan.BankAccountTitle = instituteBankAccount.AccountTitle;
			return rawFeeChallan;
		}

		internal static RawFeeChallan GetRawFeeChallanDetails(this AspireContext aspireContext, string bankName, AspireFormats.ChallanNo challanNo, bool ignoreZeroOrNegativeAmount)
		{
			var accountTypeEnums = new[] { InstituteBankAccount.AccountTypes.Online, InstituteBankAccount.AccountTypes.ManualOnline };
			var instituteBankAccount = aspireContext.InstituteBankAccounts
				.Where(iba => iba.Bank.BankName == bankName)
				.Where(iba => iba.Institute.InstituteCode == challanNo.InstituteCode)
				.Where(iba => iba.FeeType == challanNo.FeeTypeByte)
				.Where(iba => iba.Active)
				.Select(iba => new
				{
					iba.InstituteBankAccountID,
					AccountTypeEnum = (InstituteBankAccount.AccountTypes)iba.AccountType,
				}).ToList()
				.SingleOrDefault(iba => accountTypeEnums.Contains(iba.AccountTypeEnum));
			if (instituteBankAccount == null)
				return null;
			return aspireContext.GetRawFeeChallanDetails(challanNo, instituteBankAccount.InstituteBankAccountID, accountTypeEnums, ignoreZeroOrNegativeAmount);
		}

		internal static InstituteBankAccount GetInstituteBankAccount(this AspireContext aspireContext, string instituteCode, InstituteBankAccount.FeeTypes feeTypeEnum, string bankName, InstituteBankAccount.AccountTypes[] accountTypeEnums)
		{
			var accountTypes = accountTypeEnums.Cast<byte>().ToArray();
			return aspireContext.InstituteBankAccounts
				.Where(iba => iba.Institute.InstituteCode == instituteCode
							&& iba.FeeType == (byte)feeTypeEnum
							&& iba.Active
							&& iba.Bank.BankName == bankName).ToList()
				.SingleOrDefault(iba => accountTypes.Contains(iba.AccountType));
		}

		internal static int GetNextApplicationNo(this AspireContext aspireContext, int admissionOpenProgramID)
		{
			var admissionOpenProgram = aspireContext.AdmissionOpenPrograms
				.Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID)
				.Select(aop => new
				{
					aop.Program.InstituteID,
					aop.SemesterID,
				})
				.Single();

			var maxApplicationNo = aspireContext.CandidateAppliedPrograms
										.Where(cap => cap.ApplicationNo != null)
										.Where(cap => cap.AdmissionOpenProgram1.SemesterID == admissionOpenProgram.SemesterID)
										.Where(cap => cap.AdmissionOpenProgram1.Program.InstituteID == admissionOpenProgram.InstituteID)
										.Max(cap => cap.ApplicationNo) ?? 0;

			return maxApplicationNo + 1;
		}

		private static PaymentFailureReasons PayAdmissionProcessingFee(this AspireContext aspireContext, AccountTransaction accountTransaction, bool ignoreDueDate, bool ignoreZeroOrNegativeAmount)
		{
			if (accountTransaction == null)
				throw new ArgumentNullException(nameof(accountTransaction));
			var challanNo = AspireFormats.ChallanNo.Parse(accountTransaction.FullChallanNo);
			var paymentFailureReason = aspireContext.CanPayAdmissionProcessingFee(challanNo, accountTransaction.Amount, ignoreDueDate, ignoreZeroOrNegativeAmount);
			if (paymentFailureReason != PaymentFailureReasons.None)
				return paymentFailureReason;
			aspireContext.AccountTransactions.Add(accountTransaction);
			aspireContext.SaveChanges(accountTransaction.UserLoginHistoryID);
			var candidateAppliedProgram = aspireContext.CandidateAppliedPrograms.Single(cap => cap.ChallanNo == challanNo.ChallanID);
			candidateAppliedProgram.AccountTransactionID = accountTransaction.AccountTransactionID;
			if (candidateAppliedProgram.ApplicationNo == null)
				candidateAppliedProgram.ApplicationNo = aspireContext.GetNextApplicationNo(candidateAppliedProgram.Choice1AdmissionOpenProgramID);
			aspireContext.SaveChanges(accountTransaction.UserLoginHistoryID);
			return PaymentFailureReasons.None;
		}

		private static PaymentFailureReasons PayStudentFee(this AspireContext aspireContext, AccountTransaction accountTransaction, bool ignoreDueDate, bool ignoreZeroOrNegativeAmount)
		{
			if (accountTransaction == null)
				throw new ArgumentNullException(nameof(accountTransaction));
			var challanNo = AspireFormats.ChallanNo.Parse(accountTransaction.FullChallanNo);
			var paymentFailureReason = aspireContext.CanPayStudentFee(challanNo, accountTransaction.Amount, ignoreDueDate, ignoreZeroOrNegativeAmount);
			if (paymentFailureReason != PaymentFailureReasons.None)
				return paymentFailureReason;
			aspireContext.AccountTransactions.Add(accountTransaction);
			aspireContext.SaveChanges(accountTransaction.UserLoginHistoryID);
			var studentFeeChallan = aspireContext.StudentFeeChallans.Include(sfc => sfc.StudentFee).Single(sfc => sfc.StudentFeeChallanID == challanNo.ChallanID);
			studentFeeChallan.AccountTransactionID = accountTransaction.AccountTransactionID;
			studentFeeChallan.StatusEnum = StudentFeeChallan.Statuses.Paid;
			studentFeeChallan.StudentFee.StatusEnum = StudentFee.Statuses.Paid;
			aspireContext.SaveChanges(accountTransaction.UserLoginHistoryID);
			return PaymentFailureReasons.None;
		}

		private static bool IsInstituteBankAccountIDValid(this AspireContext aspireContext, AccountTransaction accountTransaction)
		{
			var challanNo = AspireFormats.ChallanNo.Parse(accountTransaction.FullChallanNo);
			return aspireContext.InstituteBankAccounts
					.Where(iba => iba.Institute.InstituteCode == challanNo.InstituteCode)
					.Where(iba => iba.FeeType == challanNo.FeeTypeByte)
				.Any(iba => iba.InstituteBankAccountID == accountTransaction.InstituteBankAccountID);
		}

		internal static PaymentFailureReasons PayFee(this AspireContext aspireContext, AccountTransaction accountTransaction, bool ignoreDueDate, bool ignoreZeroOrNegativeAmount)
		{
			var challanNo = AspireFormats.ChallanNo.TryParse(accountTransaction.FullChallanNo);
			if (challanNo == null)
				return PaymentFailureReasons.InvalidChallanNoFormat;
			accountTransaction.ValidateServiceChannelPaymentType();
			if (!aspireContext.IsInstituteBankAccountIDValid(accountTransaction))
				return PaymentFailureReasons.InstituteBankAccountNotFound;
			switch (challanNo.Value.FeeTypeEnum)
			{
				case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
					return aspireContext.PayAdmissionProcessingFee(accountTransaction, ignoreDueDate, ignoreZeroOrNegativeAmount);
				case InstituteBankAccount.FeeTypes.NewAdmissionFee:
				case InstituteBankAccount.FeeTypes.StudentFee:
					return aspireContext.PayStudentFee(accountTransaction, ignoreDueDate, ignoreZeroOrNegativeAmount);
				default:
					throw new ArgumentOutOfRangeException(nameof(challanNo.Value.FeeTypeEnum), challanNo.Value.FeeTypeEnum, null);
			}
		}

		public static AccountTransaction CreateAccountTransaction(AccountTransaction.Services serviceEnum, AccountTransaction.TransactionChannels transactionChannelEnum, AccountTransaction.PaymentTypes paymentTypeEnum, AspireFormats.ChallanNo challanNo, int amount, string transactionReferenceID, string accountNo, string accountTitle, string branchCode, string branchName, DateTime transactionDate, int instituteBankAccountID, string remarks, bool verified, int userLoginHistoryID)
		{
			return new AccountTransaction
			{
				FullChallanNo = challanNo.FullChallanNo,
				Amount = amount,
				TransactionReferenceID = transactionReferenceID.TrimAndMakeItNullIfEmpty() ?? challanNo.ToString(),
				AccountNo = accountNo.TrimAndMakeItNullIfEmpty(),
				AccountTitle = accountTitle.TrimAndMakeItNullIfEmpty(),
				BranchCode = branchCode.TrimAndMakeItNullIfEmpty(),
				BranchName = branchName.TrimAndMakeItNullIfEmpty(),
				TransactionChannelEnum = transactionChannelEnum,
				PaymentTypeEnum = paymentTypeEnum,
				TransactionDate = transactionDate,
				UserLoginHistoryID = userLoginHistoryID,
				InstituteBankAccountID = instituteBankAccountID,
				ServiceEnum = serviceEnum,
				Remarks = remarks.TrimAndMakeItNullIfEmpty(),
				Verified = verified,
				SystemDate = DateTime.Now,
			}.ValidateServiceChannelPaymentType();
		}
	}
}