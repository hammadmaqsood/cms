﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.ExamSeatingPlan.Common;
using Aspire.BL.Core.ExamSeatingPlan.Staff;
using Aspire.BL.Entities.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Faculty
{
	public static class ExamEnvelopesSlip
	{
		public static List<CustomListItem> GetExamConducts(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandFacultyActiveSession(loginSessionGuid);

				return aspireContext.ExamConducts
									.Where(ec => ec.InstituteID == loginHistory.InstituteID)
									.Where(ec => ec.VisibleToFacultyMembers)
									.Select(ec => new
									{
										ec.ExamConductID,
										ec.SemesterID,
										ExamTypeEnum = (Model.Entities.ExamConduct.ExamTypes)ec.ExamType,
										ec.Name
									}).ToList()
									.OrderByDescending(ec => ec.SemesterID).ThenByDescending(ec => ec.ExamTypeEnum).ThenByDescending(ec => ec.Name)
									.Select(ec => new CustomListItem
									{
										ID = ec.ExamConductID,
										Text = $"{ec.SemesterID.ToSemesterString()} - {ec.ExamTypeEnum.ToFullName()} - {ec.Name}"
									}).ToList();
			}
		}

		public sealed class TeacherCoursesSlipDetail
		{
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);

			public DateTime Date { get; internal set; }
			public int ExamDateID { get; internal set; }
			public int ExamOfferedRoomID { get; internal set; }
			public int ExamSessionID { get; internal set; }
			public int? FacultyMemberID { get; internal set; }
			public string Name { get; internal set; }
			public int OfferedCourseID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string RoomName { get; internal set; }
			public int Section { get; internal set; }
			public short SemesterNo { get; internal set; }
			public short Sequence { get; internal set; }
			public string SessionName { get; internal set; }
			public byte Shift { get; internal set; }
			public int Students { get; internal set; }
			public string Title { get; internal set; }
			public string CourseInfo => $"{this.Title}{Environment.NewLine}{this.Class}";
			public string SessionInfo => $"{this.Date:d}{Environment.NewLine}{this.SessionName}";

			public static List<TeacherCoursesSlipDetail> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public List<TeacherCoursesSlipDetail> CoursesSlipDetails { get; internal set; }
		}

		public static ReportDataSet GetExamEnvelopesSlips(int examConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandFacultyActiveSession(loginSessionGuid);

				if (!aspireContext.ExamConducts.Any(ec => ec.ExamConductID == examConductID && ec.InstituteID == loginHistory.InstituteID))
					return null;

				var examDateSheets = aspireContext.GetExamDateSheets(examConductID, null)
					.Where(eds => eds.OfferedCours.FacultyMemberID == loginHistory.FacultyMemberID)
					.Select(eds => new
					{
						eds.OfferedCourseID,
						eds.OfferedCours.Cours.Title,
						eds.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						eds.OfferedCours.SemesterNo,
						eds.OfferedCours.Section,
						eds.OfferedCours.Shift,
						eds.OfferedCours.FacultyMemberID,
						eds.OfferedCours.FacultyMember.Name,
						eds.ExamDateAndSession.ExamDateID,
						eds.ExamDateAndSession.ExamDate.Date,
						eds.ExamDateAndSession.ExamSessionID,
						eds.ExamDateAndSession.ExamSession.SessionName,
					});
				var examSeats = aspireContext.GetValidVisibleExamSeats(examConductID, null, null)
					.Where(es => es.RegisteredCours.OfferedCours.FacultyMemberID == loginHistory.FacultyMemberID)
					.Select(es => new
					{
						es.RegisteredCours.OfferedCourseID,
						es.ExamOfferedRoomID,
						es.ExamOfferedRoom.ExamRoom.RoomName,
						es.ExamOfferedRoom.Sequence,
					});

				var details = from eds in examDateSheets
							  join es in examSeats on eds.OfferedCourseID equals es.OfferedCourseID
							  group new { eds, es } by new
							  {
								  eds.OfferedCourseID,
								  eds.Title,
								  eds.ProgramAlias,
								  eds.SemesterNo,
								  eds.Section,
								  eds.Shift,
								  eds.FacultyMemberID,
								  eds.Name,
								  es.ExamOfferedRoomID,
								  es.RoomName,
								  es.Sequence,
								  eds.ExamDateID,
								  eds.ExamSessionID,
								  eds.Date,
								  eds.SessionName,
							  } into grp
							  select new TeacherCoursesSlipDetail
							  {
								  OfferedCourseID = grp.Key.OfferedCourseID,
								  Title = grp.Key.Title,
								  ProgramAlias = grp.Key.ProgramAlias,
								  SemesterNo = grp.Key.SemesterNo,
								  Section = grp.Key.Section,
								  Shift = grp.Key.Shift,
								  FacultyMemberID = grp.Key.FacultyMemberID,
								  Name = grp.Key.Name,
								  ExamDateID = grp.Key.ExamDateID,
								  Date = grp.Key.Date,
								  ExamSessionID = grp.Key.ExamSessionID,
								  SessionName = grp.Key.SessionName,
								  ExamOfferedRoomID = grp.Key.ExamOfferedRoomID,
								  RoomName = grp.Key.RoomName,
								  Sequence = grp.Key.Sequence,
								  Students = grp.Count(),
							  };

				return new ReportDataSet { CoursesSlipDetails = details.ToList() };
			}
		}
	}
}

