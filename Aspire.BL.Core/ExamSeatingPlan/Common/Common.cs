﻿using Aspire.Model.Context;
using Aspire.Model.Entities;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Common
{
	public static class Common
	{
		internal static IQueryable<ExamDateSheet> GetExamDateSheets(this AspireContext aspireContext, int examConductID, int? examDateAndSessionID)
		{
			var examDateSheets = aspireContext.ExamDateSheets.Where(eds => eds.ExamDateAndSession.ExamConductID == examConductID);
			if (examDateAndSessionID != null)
				examDateSheets = examDateSheets.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID);
			return examDateSheets;
		}
	}
}
