﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FileServer;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Common
{
	public static class ExamSeats
	{
		public sealed class RegisteredCourseExamSeat
		{
			#region Student

			public string InstituteName { get; internal set; }
			public string ReportTitle => $"Exam Admit Slip ({this.OfferedSemesterID.ToSemesterString()} {this.ExamTypeEnum?.ToFullName()})";
			public int StudentID { get; internal set; }
			internal short IntakeSemesterID { get; set; }
			public byte[] Picture { get; internal set; }
			public string Enrollment { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string Name { get; internal set; }
			internal byte? StudentStatus { get; set; }
			private Model.Entities.Student.Statuses? StudentStatusEnum => (Model.Entities.Student.Statuses?)this.StudentStatus;
			internal string StudentProgramAlias { get; set; }
			internal bool LibraryDefaulter { get; set; }
			private bool StudentOk
			{
				get
				{
					if (this.LibraryDefaulter)
						return false;
					switch (this.StudentStatusEnum)
					{
						case Model.Entities.Student.Statuses.AdmissionCancelled:
						case Model.Entities.Student.Statuses.Blocked:
						case Model.Entities.Student.Statuses.Deferred:
						case Model.Entities.Student.Statuses.Dropped:
						case Model.Entities.Student.Statuses.Expelled:
						case Model.Entities.Student.Statuses.Graduated:
						case Model.Entities.Student.Statuses.Left:
						case Model.Entities.Student.Statuses.ProgramChanged:
						case Model.Entities.Student.Statuses.Rusticated:
						case Model.Entities.Student.Statuses.TransferredToOtherCampus:
							return false;
						case null:
							return true;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
			private IEnumerable<string> Statuses => new[]
			{
				this.LibraryDefaulter ? "Library Defaulter" : null,
				this.StudentStatusEnum?.ToFullName(),
				this.StudentSemesterFreezedStatusEnum?.ToFullName()
			}.Where(s => !string.IsNullOrWhiteSpace(s));
			public string StudentStatusDisplay => string.Join(",", this.Statuses);
			#endregion

			#region Student Semester
			internal short StudentSemesterSemesterNo { get; set; }
			internal int StudentSemesterSection { get; set; }
			internal byte StudentSemesterShift { get; set; }
			public string StudentClass => AspireFormats.GetClassName(this.StudentProgramAlias, this.StudentSemesterSemesterNo, this.StudentSemesterSection, this.StudentSemesterShift);
			internal byte? StudentSemesterFreezedStatus { get; set; }
			internal FreezedStatuses? StudentSemesterFreezedStatusEnum => (FreezedStatuses?)this.StudentSemesterFreezedStatus;
			public bool GraduateFormRequiredAndNotSubmitted { get; internal set; }
			private bool StudentSemesterOk
			{
				get
				{
					if (this.GraduateFormRequiredAndNotSubmitted)
						return false;
					switch (this.StudentSemesterFreezedStatusEnum)
					{
						case FreezedStatuses.FreezedWithFee:
						case FreezedStatuses.FreezedWithoutFee:
							return false;
						case null:
							return true;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
			#endregion

			#region OfferedCourse
			public int OfferedCourseID { get; internal set; }
			public short OfferedSemesterID { get; internal set; }
			public string Title { get; internal set; }
			internal string OfferedCourseProgramAlias { get; set; }
			internal int OfferedCourseSection { get; set; }
			internal short OfferedCourseSemesterNo { get; set; }
			internal byte OfferedCourseShift { get; set; }
			public string OfferedClass => AspireFormats.GetClassName(this.OfferedCourseProgramAlias, this.OfferedCourseSemesterNo, this.OfferedCourseSection, this.OfferedCourseShift);
			#endregion

			#region RegisteredCourse
			public int RegisteredCourseID { get; internal set; }
			internal byte? RegisteredCourseStatus { get; set; }
			private RegisteredCours.Statuses? RegisteredCourseStatusEnum => (RegisteredCours.Statuses?)this.RegisteredCourseStatus;
			internal byte? RegisteredCourseFreezedStatus { get; set; }
			private FreezedStatuses? RegisteredCourseFreezedStatusEnum => (FreezedStatuses?)this.RegisteredCourseFreezedStatus ?? this.StudentSemesterFreezedStatusEnum;
			private bool CourseOk
			{
				get
				{
					if (this.FeeOk == false || this.SurveysOk == false)
						return false;
					switch (this.RegisteredCourseStatusEnum)
					{
						case RegisteredCours.Statuses.AttendanceDefaulter:
						case RegisteredCours.Statuses.Deferred:
						case RegisteredCours.Statuses.Incomplete:
						case RegisteredCours.Statuses.WithdrawWithFee:
						case RegisteredCours.Statuses.WithdrawWithoutFee:
						case RegisteredCours.Statuses.UnfairMeans:
							return false;
						case null:
							switch (this.RegisteredCourseFreezedStatusEnum)
							{
								case FreezedStatuses.FreezedWithFee:
								case FreezedStatuses.FreezedWithoutFee:
									return false;
								case null:
									return true;
								default:
									throw new ArgumentOutOfRangeException();
							}
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
			public string RegisteredCourseStatusDisplay => AspireFormats.GetRegisteredCourseStatusHtml(this.RegisteredCourseStatus, this.RegisteredCourseFreezedStatus).ToNAIfNullOrEmpty();
			#endregion

			#region DateSheet
			public int? ExamConductID { get; internal set; }
			internal byte? ExamType { get; set; }
			internal ExamConduct.ExamTypes? ExamTypeEnum => (ExamConduct.ExamTypes?)this.ExamType;
			public DateTime? Date { get; internal set; }
			public string SessionName { get; internal set; }
			public TimeSpan? StartTime { get; internal set; }
			public byte? TotalTimeInMinutes { get; internal set; }
			public string Timing => this.Date != null ? $"{this.StartTime?.ToTime12Format()}" : null;
			public string Timing24 => this.Date != null ? $"{this.StartTime?.ToTime24Format()}" : null;
			#endregion

			#region Exam Seat
			internal string RoomName { get; set; }
			internal byte? Row { get; set; }
			internal byte? Column { get; set; }
			private bool SeatDisplay => this.StudentOk && this.StudentSemesterOk && this.CourseOk;
			public string RoomNameDisplay => (this.SeatDisplay ? this.RoomName : null).ToNAIfNullOrEmpty();
			public string RowDisplay => (this.SeatDisplay ? this.Row?.ToString() : null).ToNAIfNullOrEmpty();
			public string ColumnDisplay => (this.SeatDisplay ? this.Column.ToString() : null).ToNAIfNullOrEmpty();
			#endregion

			#region Fee
			internal bool FeeDefaulter { get; set; }
			internal bool BlockFeeDefaulters { get; set; }
			private bool FeeOk => this.BlockFeeDefaulters == false || !this.FeeDefaulter;
			public string FeeDisplay => this.FeeDefaulter.ToYesNo();
			#endregion

			#region Surveys
			internal List<int> CompletedSurveyConductIDsStudentWise { get; set; }
			internal List<int> CompletedSurveyConductIDsCourseWise { get; set; }
			internal List<int> RequiredSurveyConductIDsStudentWise { get; set; }
			internal List<int> RequiredSurveyConductIDsCourseWise { get; set; }
			private bool StudentWiseSurveysOk => true;//!this.RequiredSurveyConductIDsStudentWise.Any() || this.RequiredSurveyConductIDsStudentWise.All(id => this.CompletedSurveyConductIDsStudentWise.Contains(id));
			private bool CourseWiseSurveysOk => !this.RequiredSurveyConductIDsCourseWise.Any() || this.RequiredSurveyConductIDsCourseWise.All(id => this.CompletedSurveyConductIDsCourseWise.Contains(id));
			internal bool SurveysOk => this.StudentWiseSurveysOk && this.CourseWiseSurveysOk;
			public string SurveysDisplay => this.SurveysOk ? "Completed" : "Not Completed";
			#endregion

			public List<RegisteredCourseExamSeat> GetList()
			{
				return null;
			}
		}

		internal static List<RegisteredCourseExamSeat> GetStudentSlips(this AspireContext aspireContext, int studentID, short semesterID, ExamConduct.ExamTypes examTypeEnum, bool loadPicture)
		{
			var examType = (byte)examTypeEnum;
			var student = aspireContext.Students.Where(s => s.StudentID == studentID)
				.Select(s => new
				{
					s.StudentID,
					s.AdmissionOpenProgram.Program.InstituteID,
					s.AdmissionOpenProgram.SemesterID,
					s.AdmissionOpenProgram.FinalSemesterID,
					s.AdmissionOpenProgram.Program.Duration,
					s.AdmissionOpenProgram.IsSummerRegularSemester,
				}).Single();

			var examConducts = aspireContext.ExamConducts.Where(ec => ec.InstituteID == student.InstituteID && ec.SemesterID == semesterID && ec.ExamType == examType && ec.VisibleToStudents)
				.Select(ec => new
				{
					ec.ExamConductID,
					ec.ExamType,
				}).ToList();
			if (!examConducts.Any())
				return null;

			bool graduateFormRequiredAndNotSubmitted;
			switch (examTypeEnum)
			{
				case ExamConduct.ExamTypes.Mid:
					graduateFormRequiredAndNotSubmitted = false;
					break;
				case ExamConduct.ExamTypes.Final:
					var submitted = aspireContext.GraduateDirectoryForms.Any(gdf => gdf.StudentID == studentID && gdf.SemesterID == semesterID && gdf.Submitted != null);
					if (submitted)
						graduateFormRequiredAndNotSubmitted = false;
					else if (student.FinalSemesterID != null)
						graduateFormRequiredAndNotSubmitted = student.FinalSemesterID <= semesterID;
					else
						graduateFormRequiredAndNotSubmitted = student.SemesterID.GetLastSemester((ProgramDurations)student.Duration, student.IsSummerRegularSemester) <= semesterID;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(examTypeEnum), examTypeEnum, null);
			}

			var registeredCourses = (from ss in aspireContext.StudentSemesters
									 join rc in aspireContext.RegisteredCourses on ss.StudentID equals rc.StudentID
									 where rc.StudentID == studentID && ss.SemesterID == semesterID && rc.OfferedCours.SemesterID == semesterID && rc.DeletedDate == null
									 select new RegisteredCourseExamSeat
									 {
										 StudentID = ss.Student.StudentID,
										 InstituteName = ss.Student.AdmissionOpenProgram.Program.Institute.InstituteName,
										 IntakeSemesterID = ss.Student.AdmissionOpenProgram.SemesterID,
										 GraduateFormRequiredAndNotSubmitted = graduateFormRequiredAndNotSubmitted,
										 Enrollment = ss.Student.Enrollment,
										 RegistrationNo = ss.Student.RegistrationNo,
										 Name = ss.Student.Name,
										 StudentProgramAlias = ss.Student.AdmissionOpenProgram.Program.ProgramAlias,
										 StudentStatus = ss.Student.Status,
										 LibraryDefaulter = ss.Student.StudentLibraryDefaulters.Any(d => d.Status == StudentLibraryDefaulter.StatusBlocked),
										 StudentSemesterFreezedStatus = ss.FreezedStatus,
										 StudentSemesterSemesterNo = ss.SemesterNo,
										 StudentSemesterSection = ss.Section,
										 StudentSemesterShift = ss.Shift,
										 RegisteredCourseID = rc.RegisteredCourseID,
										 RegisteredCourseFreezedStatus = rc.FreezedStatus,
										 RegisteredCourseStatus = rc.Status,
										 FeeDefaulter = rc.FeeDefaulter,
										 Title = rc.OfferedCours.Cours.Title,
										 OfferedCourseID = rc.OfferedCourseID,
										 OfferedSemesterID = rc.OfferedCours.SemesterID,
										 OfferedCourseProgramAlias = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
										 OfferedCourseSemesterNo = rc.OfferedCours.SemesterNo,
										 OfferedCourseSection = rc.OfferedCours.Section,
										 OfferedCourseShift = rc.OfferedCours.Shift,
										 CompletedSurveyConductIDsStudentWise = aspireContext.SurveyUsers.Where(su => su.StudentID == rc.StudentID && su.RegisteredCourseID == null)
											 .Select(su => su.SurveyConductID).Distinct().ToList(),
										 CompletedSurveyConductIDsCourseWise = aspireContext.SurveyUsers.Where(su => su.RegisteredCourseID == rc.RegisteredCourseID)
											 .Select(su => su.SurveyConductID).Distinct().ToList(),
									 }).OrderBy(c => c.Title).ToList();

			List<byte> surveyConductTypes;
			switch (examTypeEnum)
			{
				case ExamConduct.ExamTypes.Mid:
					surveyConductTypes = new List<byte>
					{
						(byte)SurveyConduct.SurveyConductTypes.BeforeMidTerm
					};
					break;
				case ExamConduct.ExamTypes.Final:
					surveyConductTypes = new List<byte>
					{
						(byte)SurveyConduct.SurveyConductTypes.BeforeMidTerm,
						(byte)SurveyConduct.SurveyConductTypes.BeforeFinalTerm
					};
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(examTypeEnum), examTypeEnum, null);
			}
			var surveysRequired = aspireContext.SurveyConducts
				.Where(sc => sc.SemesterID == semesterID && sc.Status == SurveyConduct.StatusActive && sc.Survey.InstituteID == student.InstituteID)
				.Where(sc => surveyConductTypes.Contains(sc.SurveyConductType))
				.Where(s => s.Survey.SurveyType == Survey.SurveyTypeStudentCourseWise || s.Survey.SurveyType == Survey.SurveyTypeStudentWise)
				.Select(sct => new
				{
					sct.SurveyConductID,
					sct.Survey.SurveyType,
				}).ToList();

			var examConductIDs = examConducts.Select(ec => ec.ExamConductID).ToList();
			var offeredCourseIDs = registeredCourses.Select(rc => rc.OfferedCourseID).ToList();
			var registeredCourseIDs = registeredCourses.Select(rc => rc.RegisteredCourseID).ToList();
			var examDateSheets = aspireContext.ExamDateSheets.Where(eds => examConductIDs.Contains(eds.ExamDateAndSession.ExamConductID) && offeredCourseIDs.Contains(eds.OfferedCourseID))
				.Select(eds => new
				{
					eds.OfferedCourseID,
					eds.ExamDateAndSession.ExamConductID,
					eds.ExamDateAndSession.ExamConduct.ExamType,
					eds.ExamDateAndSession.ExamDate.Date,
					eds.ExamDateAndSession.ExamSession.SessionName,
					eds.ExamDateAndSession.ExamSession.StartTime,
					eds.ExamDateAndSession.ExamSession.TotalTimeInMinutes,
				}).ToList();
			var examSeats = aspireContext.ExamSeats.Where(es => registeredCourseIDs.Contains(es.RegisteredCourseID) && examConductIDs.Contains(es.ExamOfferedRoom.ExamConductID))
				.Select(es => new
				{
					es.RegisteredCourseID,
					es.ExamOfferedRoom.ExamRoom.RoomName,
					es.Row,
					es.Column,
					es.ExamOfferedRoom.ExamConduct.BlockFeeDefaulters,
				}).ToList();

			var studentWiseSurveysRequired = surveysRequired.Where(s => s.SurveyType == Survey.SurveyTypeStudentWise).Select(s => s.SurveyConductID).ToList();
			var courseWiseSurveysRequired = surveysRequired.Where(s => s.SurveyType == Survey.SurveyTypeStudentCourseWise).Select(s => s.SurveyConductID).ToList();

			foreach (var registeredCourse in registeredCourses)
			{
				registeredCourse.RequiredSurveyConductIDsStudentWise = studentWiseSurveysRequired;
				registeredCourse.RequiredSurveyConductIDsCourseWise = courseWiseSurveysRequired;

				var examDateSheet = examDateSheets.SingleOrDefault(eds => eds.OfferedCourseID == registeredCourse.OfferedCourseID);
				if (examDateSheet == null)
					continue;

				registeredCourse.ExamConductID = examDateSheet.ExamConductID;
				registeredCourse.ExamType = examDateSheet.ExamType;
				registeredCourse.Date = examDateSheet.Date;
				registeredCourse.SessionName = examDateSheet.SessionName;
				registeredCourse.StartTime = examDateSheet.StartTime;
				registeredCourse.TotalTimeInMinutes = examDateSheet.TotalTimeInMinutes;

				var examSeat = examSeats.SingleOrDefault(es => es.RegisteredCourseID == registeredCourse.RegisteredCourseID);
				if (examSeat == null)
					continue;

				registeredCourse.RoomName = examSeat.RoomName;
				registeredCourse.Row = examSeat.Row;
				registeredCourse.Column = examSeat.Column;
				registeredCourse.BlockFeeDefaulters = examSeat.BlockFeeDefaulters;
			}
			if (loadPicture && registeredCourses.Any())
			{
				var bytes = aspireContext.ReadStudentPicture(student.StudentID);
				registeredCourses.ForEach(rc => rc.Picture = bytes);
			}
			return registeredCourses;
		}

		public static List<BL.Core.ExamSeatingPlan.Common.ExamSeats.RegisteredCourseExamSeat> GetStudentSlips(string enrollment, short semesterID, ExamConduct.ExamTypes examTypeEnum, bool loadPicture, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						var staffLoginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.ExecuteSeatingPlan, UserGroupPermission.PermissionValues.Allowed);
						var studentID = aspireContext.Students.Where(s => s.Enrollment == enrollment && s.AdmissionOpenProgram.Program.InstituteID == staffLoginHistory.InstituteID)
							.Select(s => (int?)s.StudentID).SingleOrDefault();
						if (studentID == null)
							return null;
						return aspireContext.GetStudentSlips(studentID.Value, semesterID, examTypeEnum, loadPicture);
					case UserTypes.Student:
						return aspireContext.GetStudentSlips(loginHistory.StudentID.Value, semesterID, examTypeEnum, loadPicture);
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		public sealed class ExamSeatsFiltersCustomListItems
		{
			internal ExamSeatsFiltersCustomListItems() { }
			public List<CustomListItem> Semesters { get; internal set; }
			public List<CustomListItem> ExamTypes { get; internal set; }
		}

		public static ExamSeatsFiltersCustomListItems GetSemestersExamTypes(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandActiveUserSession(loginSessionGuid);
				int instituteID;
				switch (loginHistory.UserTypeEnum)
				{
					case UserTypes.Staff:
						instituteID = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed).InstituteID;
						break;
					case UserTypes.Student:
						instituteID = aspireContext.Students.Where(s => s.StudentID == loginHistory.StudentID.Value).Select(s => s.AdmissionOpenProgram.Program.InstituteID).Single();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				var record = aspireContext.ExamConducts
					.Where(ec => ec.InstituteID == instituteID && ec.VisibleToStudents)
					.Select(ec => new
					{
						ec.SemesterID,
						ec.ExamType,
					}).Distinct().ToList();

				var examSeatsFilterCustomListItems = new ExamSeatsFiltersCustomListItems
				{
					Semesters = record.GroupBy(g => new { g.SemesterID }).Select(s => new CustomListItem
					{
						ID = s.Key.SemesterID,
						Text = s.Key.SemesterID.ToSemesterString(),
					}).OrderByDescending(i => i.ID).ToList(),

					ExamTypes = record.GroupBy(g => new { g.ExamType }).Select(et => new CustomListItem
					{
						ID = et.Key.ExamType,
						Text = ((ExamConduct.ExamTypes)et.Key.ExamType).ToFullName(),
					}).ToList(),
				};
				return examSeatsFilterCustomListItems;
			}
		}
	}
}