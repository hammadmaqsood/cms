﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff
{
	public static class ExamInvigilatorDuties
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.ManageExamInvigilatorDuties, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class GetExamInvigilatorDutiesResult
		{
			public sealed class ExamInvigilatorDuty
			{
				internal ExamInvigilatorDuty() { }
				public short Sequence { get; internal set; }
				public int ExamOfferedRoomID { get; internal set; }
				public string BuildingName { get; internal set; }
				public string RoomName { get; internal set; }
				public int TotalSeats { get; internal set; }
				public int Students { get; internal set; }
				public List<ExamInvigilator> ExamInvigilators { get; internal set; }
				public byte RoomUsageType { get; internal set; }
				public string RoomUsageTypeFullName => ((ExamOfferedRoom.RoomUsageTypes)this.RoomUsageType).ToFullName();

				private ExamInvigilator GetExamInvigilator(Model.Entities.ExamInvigilatorDuty.InvigilatorTypes type)
				{
					return this.ExamInvigilators?.Find(ei => ei.InvigilatorTypeEnum == type);
				}

				public int? ExamInvigilatorIDOne => this.GetExamInvigilator(Model.Entities.ExamInvigilatorDuty.InvigilatorTypes.InvigilatorOne)?.ExamInvigilatorID;
				public int? ExamInvigilatorIDTwo => this.GetExamInvigilator(Model.Entities.ExamInvigilatorDuty.InvigilatorTypes.InvigilatorTwo)?.ExamInvigilatorID;
				public string ExamInvigilatorOneName => (this.GetExamInvigilator(Model.Entities.ExamInvigilatorDuty.InvigilatorTypes.InvigilatorOne)?.Name).ToNAIfNullOrEmpty();
				public string ExamInvigilatorTwoName => (this.GetExamInvigilator(Model.Entities.ExamInvigilatorDuty.InvigilatorTypes.InvigilatorTwo)?.Name).ToNAIfNullOrEmpty();
				public bool ExamInvigilatorOneDeleteAllowed => this.GetExamInvigilator(Model.Entities.ExamInvigilatorDuty.InvigilatorTypes.InvigilatorOne)?.AttendanceStatusEnum == Model.Entities.ExamInvigilatorDuty.AttendanceStatuses.Absent;
				public bool ExamInvigilatorTwoDeleteAllowed => this.GetExamInvigilator(Model.Entities.ExamInvigilatorDuty.InvigilatorTypes.InvigilatorTwo)?.AttendanceStatusEnum == Model.Entities.ExamInvigilatorDuty.AttendanceStatuses.Absent;
				public sealed class ExamInvigilator
				{
					internal ExamInvigilator() { }
					public int? ExamInvigilatorID { get; internal set; }
					public string Name { get; internal set; }
					public byte InvigilatorType { get; internal set; }
					public Model.Entities.ExamInvigilatorDuty.InvigilatorTypes InvigilatorTypeEnum => (Model.Entities.ExamInvigilatorDuty.InvigilatorTypes)this.InvigilatorType;
					public byte AttendanceStatus { get; internal set; }
					public Model.Entities.ExamInvigilatorDuty.AttendanceStatuses AttendanceStatusEnum => (Model.Entities.ExamInvigilatorDuty.AttendanceStatuses)this.AttendanceStatus;
				}
			}

			public sealed class ExamInvigilator
			{
				internal ExamInvigilator() { }
				public bool Available { get; internal set; }
				public int ExamInvigilatorID { get; internal set; }
				public string Name { get; internal set; }
			}

			internal GetExamInvigilatorDutiesResult() { }
			public List<ExamInvigilatorDuty> ExamInvigilatorDuties { get; internal set; }
			public List<ExamInvigilator> ExamInvigilators { get; internal set; }
		}

		public static GetExamInvigilatorDutiesResult GetExamInvigilatorDuties(int examDateAndSessionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var examDateAndSession = aspireContext.ExamDateAndSessions.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID && eds.ExamConduct.InstituteID == loginHistory.InstituteID)
					.Select(eds => new
					{
						eds.ExamConductID,
						eds.ExamDateAndSessionID,
						eds.ExamSessionID,
					}).SingleOrDefault();
				if (examDateAndSession == null)
					return null;

				var examSeats = aspireContext.GetValidExamSeats(examDateAndSession.ExamConductID, examDateAndSession.ExamDateAndSessionID, null)
					.GroupBy(es => es.ExamOfferedRoomID).Select(es => new
					{
						ExamOfferedRoomID = es.Key,
						Students = es.Count()
					}).ToList();

				var examOfferedRooms = aspireContext.ExamOfferedRooms.Where(eor => eor.ExamConductID == examDateAndSession.ExamConductID)
					.OrderBy(eor => eor.Sequence)
					.Select(eor => new GetExamInvigilatorDutiesResult.ExamInvigilatorDuty
					{
						ExamOfferedRoomID = eor.ExamOfferedRoomID,
						Sequence = eor.Sequence,
						BuildingName = eor.ExamRoom.ExamBuilding.BuildingName,
						RoomName = eor.ExamRoom.RoomName,
						TotalSeats = eor.Rows * eor.Columns,
						RoomUsageType = eor.RoomUsageType,
						Students = 0,
						ExamInvigilators = eor.ExamInvigilatorDuties.Where(eid => eid.ExamDateAndSessionID == examDateAndSession.ExamDateAndSessionID)
							.Select(eid => new GetExamInvigilatorDutiesResult.ExamInvigilatorDuty.ExamInvigilator
							{
								ExamInvigilatorID = eid.ExamInvigilatorID,
								Name = eid.ExamInvigilator.Name,
								InvigilatorType = eid.InvigilatorType,
								AttendanceStatus = eid.AttendanceStatus
							}).ToList()
					}).ToList();

				(from eor in examOfferedRooms
				 join es in examSeats on eor.ExamOfferedRoomID equals es.ExamOfferedRoomID
				 select new { eor, es }).ForEach(x => x.eor.Students = x.es.Students);

				//foreach (var examInvigilatorDuty in examOfferedRooms)
				//	examInvigilatorDuty.Students = examSeats.Find(es => es.ExamOfferedRoomID == examInvigilatorDuty.ExamOfferedRoomID)?.Students ?? 0;

				var invigilators = aspireContext.ExamInvigilators
					.Where(ei => ei.Status == ExamInvigilator.StatusActive)
					.Where(ei => ei.ExamInvigilatorDuties.All(eid => eid.ExamDateAndSessionID != examDateAndSession.ExamDateAndSessionID))
					.Where(ei => ei.InstituteID == loginHistory.InstituteID)
					.Select(ei => new GetExamInvigilatorDutiesResult.ExamInvigilator
					{
						ExamInvigilatorID = ei.ExamInvigilatorID,
						Name = ei.Name,
						Available = ei.ExamInvigilatorExamSessions.Where(eies => eies.ExamSessionID == examDateAndSession.ExamSessionID).Select(eies => eies.Available).FirstOrDefault()
					}).ToList();

				return new GetExamInvigilatorDutiesResult
				{
					ExamInvigilatorDuties = examOfferedRooms,
					ExamInvigilators = invigilators,
				};
			}
		}

		public enum AddExamInvigilatorDutyStatuses
		{
			NoRecordFound,
			Success,
			DutyAlreadyAssignedToInvigilator,
			DutyAlreadyAssignedInRoom
		}

		private static AddExamInvigilatorDutyStatuses AddExamInvigilatorDuty(this AspireContext aspireContext, int examOfferedRoomID, int examDateAndSessionID, int examInvigilatorID, Model.Entities.ExamInvigilatorDuty.InvigilatorTypes invigilatorTypeEnum, Core.Common.Permissions.Staff.StaffGroupPermission loginHistory)
		{
			var recordExists = (from eor in aspireContext.ExamOfferedRooms
								join eds in aspireContext.ExamDateAndSessions on eor.ExamConductID equals eds.ExamConductID
								join ei in aspireContext.ExamInvigilators on eds.ExamConduct.InstituteID equals ei.InstituteID
								where eor.ExamOfferedRoomID == examOfferedRoomID && eds.ExamDateAndSessionID == examDateAndSessionID && ei.ExamInvigilatorID == examInvigilatorID
									  && ei.InstituteID == loginHistory.InstituteID
								select 1).Any();

			if (!recordExists)
				return AddExamInvigilatorDutyStatuses.NoRecordFound;

			var examInvigilatorDuties = aspireContext.ExamInvigilatorDuties.Where(eid => eid.ExamDateAndSessionID == examDateAndSessionID);
			if (examInvigilatorDuties.Any(eid => eid.ExamInvigilatorID == examInvigilatorID))
				return AddExamInvigilatorDutyStatuses.DutyAlreadyAssignedToInvigilator;

			if (examInvigilatorDuties.Any(eid => eid.ExamOfferedRoomID == examOfferedRoomID && eid.InvigilatorType == (byte)invigilatorTypeEnum))
				return AddExamInvigilatorDutyStatuses.DutyAlreadyAssignedInRoom;

			aspireContext.ExamInvigilatorDuties.Add(new Model.Entities.ExamInvigilatorDuty
			{
				AttendanceStatusEnum = Model.Entities.ExamInvigilatorDuty.AttendanceStatuses.Absent,
				ExamDateAndSessionID = examDateAndSessionID,
				ExamInvigilatorID = examInvigilatorID,
				ExamOfferedRoomID = examOfferedRoomID,
				InvigilatorTypeEnum = invigilatorTypeEnum,
			});
			aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
			return AddExamInvigilatorDutyStatuses.Success;
		}

		public static AddExamInvigilatorDutyStatuses AddExamInvigilatorDuty(int examOfferedRoomID, int examDateAndSessionID, int examInvigilatorID, Model.Entities.ExamInvigilatorDuty.InvigilatorTypes invigilatorTypeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var result = aspireContext.AddExamInvigilatorDuty(examOfferedRoomID, examDateAndSessionID, examInvigilatorID, invigilatorTypeEnum, loginHistory);
				switch (result)
				{
					case AddExamInvigilatorDutyStatuses.NoRecordFound:
						break;
					case AddExamInvigilatorDutyStatuses.Success:
						aspireContext.CommitTransaction();
						break;
					case AddExamInvigilatorDutyStatuses.DutyAlreadyAssignedToInvigilator:
						break;
					case AddExamInvigilatorDutyStatuses.DutyAlreadyAssignedInRoom:
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				return result;
			}
		}

		public enum DeleteDutyStatus
		{
			Success,
			RecordNotFound,
			CannotDeletePresentInvigilator
		}

		public enum DeleteExamInvigilatorDutyStatuses
		{
			NoRecordFound,
			Success,
			CannotDeleteAttendanceIsPresent
		}

		public static DeleteExamInvigilatorDutyStatuses DeleteExamInvigilatorDuty(int examInvigilatorID, int examDateAndSessionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var examInvigilatorDuty = aspireContext.ExamInvigilatorDuties.SingleOrDefault(eid => eid.ExamDateAndSessionID == examDateAndSessionID && eid.ExamInvigilatorID == examInvigilatorID && eid.ExamInvigilator.InstituteID == loginHistory.InstituteID);
				if (examInvigilatorDuty == null)
					return DeleteExamInvigilatorDutyStatuses.NoRecordFound;
				switch (examInvigilatorDuty.AttendanceStatusEnum)
				{
					case Model.Entities.ExamInvigilatorDuty.AttendanceStatuses.Absent:
						break;
					case Model.Entities.ExamInvigilatorDuty.AttendanceStatuses.Present:
						return DeleteExamInvigilatorDutyStatuses.CannotDeleteAttendanceIsPresent;
					default:
						throw new ArgumentOutOfRangeException();
				}
				aspireContext.ExamInvigilatorDuties.Remove(examInvigilatorDuty);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteExamInvigilatorDutyStatuses.Success;
			}
		}

		public enum AutoAssignAllDutiesStatus
		{
			NoRecordFound,
			InvigilatorsNotAvailable,
			Success
		}

		public static AutoAssignAllDutiesStatus AutoAssignAllDuties(int examDateAndSessionID, out uint assignedDutiesCount, Guid loginSessionGuid)
		{
			assignedDutiesCount = 0;
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var examDateAndSession = aspireContext.ExamDateAndSessions
					.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID && eds.ExamConduct.InstituteID == loginHistory.InstituteID).Select(eds => new
					{
						eds.ExamDateAndSessionID,
						eds.ExamSessionID,
						eds.ExamConductID,
					}).SingleOrDefault();
				if (examDateAndSession == null)
				{
					assignedDutiesCount = 0;
					aspireContext.RollbackTransaction();
					return AutoAssignAllDutiesStatus.NoRecordFound;
				}

				var examOfferedRooms = aspireContext.ExamOfferedRooms.Where(eor => eor.ExamConductID == examDateAndSession.ExamConductID)
					.Select(eor => new
					{
						eor.ExamOfferedRoomID,
						InvigilatorTypes = eor.ExamInvigilatorDuties.Where(eid => eid.ExamDateAndSessionID == examDateAndSession.ExamDateAndSessionID)
							.Select(eid => eid.InvigilatorType).ToList()
					}).ToList();

				var availableExamInvigilatorIDs = aspireContext.ExamInvigilators
					.Where(ei => ei.InstituteID == loginHistory.InstituteID && ei.Status == ExamInvigilator.StatusActive)
					.Where(ei => ei.ExamInvigilatorExamSessions.Any(eies => eies.ExamSessionID == examDateAndSession.ExamSessionID && eies.Available))
					.Where(ei => ei.ExamInvigilatorDuties.All(eid => eid.ExamDateAndSessionID != examDateAndSession.ExamDateAndSessionID))
					.Select(ei => ei.ExamInvigilatorID).ToList()
					.OrderBy(ei => Guid.NewGuid()).ToList();

				var invigilatorTypes = Enum.GetValues(typeof(Model.Entities.ExamInvigilatorDuty.InvigilatorTypes)).Cast<Model.Entities.ExamInvigilatorDuty.InvigilatorTypes>().ToArray();
				foreach (var examOfferedRoom in examOfferedRooms)
				{
					foreach (var invigilatorType in invigilatorTypes)
					{
						if (examOfferedRoom.InvigilatorTypes.Contains((byte)invigilatorType))
							continue;
						var examInvigilatorID = availableExamInvigilatorIDs.Cast<int?>().FirstOrDefault();
						if (examInvigilatorID == null)
						{
							aspireContext.CommitTransaction();
							return AutoAssignAllDutiesStatus.InvigilatorsNotAvailable;
						}
						availableExamInvigilatorIDs.Remove(examInvigilatorID.Value);
						var result = aspireContext.AddExamInvigilatorDuty(examOfferedRoom.ExamOfferedRoomID, examDateAndSession.ExamDateAndSessionID, examInvigilatorID.Value, invigilatorType, loginHistory);
						switch (result)
						{
							case AddExamInvigilatorDutyStatuses.Success:
								assignedDutiesCount++;
								break;
							case AddExamInvigilatorDutyStatuses.NoRecordFound:
							case AddExamInvigilatorDutyStatuses.DutyAlreadyAssignedToInvigilator:
							case AddExamInvigilatorDutyStatuses.DutyAlreadyAssignedInRoom:
								assignedDutiesCount = 0;
								aspireContext.RollbackTransaction();
								throw new InvalidOperationException();
							default:
								throw new ArgumentOutOfRangeException();
						}
					}
				}
				aspireContext.CommitTransaction();
				return AutoAssignAllDutiesStatus.Success;
			}
		}

		public sealed class ExamInvigilatorDuty
		{
			internal ExamInvigilatorDuty()
			{
			}

			public int ExamInvigilatorDutyID { get; internal set; }
			public byte AttendanceStatus { get; internal set; }
			public DateTime ExamDate { get; internal set; }
			public string SessionName { get; internal set; }
			public string ExamDateAndSession => AspireFormats.GetExamDateAndSessionString(this.ExamDate, this.SessionName);
			public string InvigilatorName { get; internal set; }
			public string RoomName { get; internal set; }
		}

		public static List<ExamInvigilatorDuty> GetExamInvigilatorDutiesForAttendance(int examDateAndSessionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.ExamInvigilatorDuties.Where(eid => eid.ExamDateAndSessionID == examDateAndSessionID && eid.ExamDateAndSession.ExamConduct.InstituteID == loginHistory.InstituteID).Select(eid => new ExamInvigilatorDuty
				{
					ExamInvigilatorDutyID = eid.ExamInvigilatorDutyID,
					InvigilatorName = eid.ExamInvigilator.Name,
					RoomName = eid.ExamOfferedRoom.ExamRoom.RoomName,
					ExamDate = eid.ExamDateAndSession.ExamDate.Date,
					SessionName = eid.ExamDateAndSession.ExamSession.SessionName,
					AttendanceStatus = eid.AttendanceStatus,
				}).OrderBy(eid => eid.InvigilatorName).ToList();
			}
		}

		public enum MarkExamInvigilatorDutiesAttendanceStatuses
		{
			NoRecordFound,
			Success
		}

		public static MarkExamInvigilatorDutiesAttendanceStatuses MarkExamInvigilatorDutiesAttendance(int examDateAndSessionID, Dictionary<int, Model.Entities.ExamInvigilatorDuty.AttendanceStatuses> attendance, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var examInvigilatorDuties = aspireContext.ExamInvigilatorDuties.Where(eid => eid.ExamDateAndSessionID == examDateAndSessionID && eid.ExamDateAndSession.ExamConduct.InstituteID == loginHistory.InstituteID).ToList();
				if (attendance.Count != examInvigilatorDuties.Count)
					return MarkExamInvigilatorDutiesAttendanceStatuses.NoRecordFound;
				foreach (var examInvigilatorDuty in examInvigilatorDuties)
				{
					Model.Entities.ExamInvigilatorDuty.AttendanceStatuses attendanceStatus;
					if (!attendance.TryGetValue(examInvigilatorDuty.ExamInvigilatorDutyID, out attendanceStatus))
						return MarkExamInvigilatorDutiesAttendanceStatuses.NoRecordFound;
					examInvigilatorDuty.AttendanceStatusEnum = attendanceStatus;
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return MarkExamInvigilatorDutiesAttendanceStatuses.Success;
			}
		}

		public enum ClearAllAssignedDutiesStatuses
		{
			AttendanceMarked,
			Success,
		}

		public static Dictionary<ClearAllAssignedDutiesStatuses, int> ClearAllAssignedDuties(int examDateAndSessionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var examInvigilatorDuties = aspireContext.ExamInvigilatorDuties
					.Where(eid => eid.ExamDateAndSessionID == examDateAndSessionID && eid.ExamInvigilator.InstituteID == loginHistory.InstituteID)
					.ToList();
				var result = new Dictionary<ClearAllAssignedDutiesStatuses, int>();
				foreach (var examInvigilatorDuty in examInvigilatorDuties)
				{
					switch (examInvigilatorDuty.AttendanceStatusEnum)
					{
						case Model.Entities.ExamInvigilatorDuty.AttendanceStatuses.Absent:
							aspireContext.ExamInvigilatorDuties.Remove(examInvigilatorDuty);
							if (result.ContainsKey(ClearAllAssignedDutiesStatuses.Success))
								result[ClearAllAssignedDutiesStatuses.Success]++;
							else
								result[ClearAllAssignedDutiesStatuses.Success] = 1;
							break;
						case Model.Entities.ExamInvigilatorDuty.AttendanceStatuses.Present:
							if (result.ContainsKey(ClearAllAssignedDutiesStatuses.AttendanceMarked))
								result[ClearAllAssignedDutiesStatuses.AttendanceMarked]++;
							else
								result[ClearAllAssignedDutiesStatuses.AttendanceMarked] = 1;
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return result;
			}
		}

		public enum ClearAllAssignedDutiesOfInvigilatorStatuses
		{
			Success,
			NoRecordFound,
			NotAllDeletedAttendanceRecordExists
		}

		public static ClearAllAssignedDutiesOfInvigilatorStatuses ClearAllAssignedDutiesOfInvigilator(int examConductID, int examInvigilatorID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var examInvigilatorDuties = aspireContext.ExamInvigilatorDuties
					.Where(eid => eid.ExamInvigilatorID == examInvigilatorID)
					.Where(eid => eid.ExamDateAndSession.ExamConductID == examConductID && eid.ExamInvigilator.InstituteID == loginHistory.InstituteID)
					.ToList();
				if (!examInvigilatorDuties.Any())
					return ClearAllAssignedDutiesOfInvigilatorStatuses.NoRecordFound;

				var result = ClearAllAssignedDutiesOfInvigilatorStatuses.Success;
				foreach (var examInvigilatorDuty in examInvigilatorDuties)
					switch (examInvigilatorDuty.AttendanceStatusEnum)
					{
						case Model.Entities.ExamInvigilatorDuty.AttendanceStatuses.Absent:
							aspireContext.ExamInvigilatorDuties.Remove(examInvigilatorDuty);
							break;
						case Model.Entities.ExamInvigilatorDuty.AttendanceStatuses.Present:
							result = ClearAllAssignedDutiesOfInvigilatorStatuses.NotAllDeletedAttendanceRecordExists;
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return result;
			}
		}

		public static uint AutoAssignAllDutiesToInvigilator(int examConductID, int examInvigilatorID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);

				var availableExamSessions = aspireContext.ExamInvigilatorExamSessions
					.Where(eies => eies.ExamInvigilatorID == examInvigilatorID && eies.Available)
					.Where(eies => eies.ExamSession.ExamConductID == examConductID && eies.ExamInvigilator.InstituteID == loginHistory.InstituteID)
					.Select(eies => eies.ExamSession);

				var examDateAndSessionIDs = availableExamSessions.SelectMany(es => es.ExamDateAndSessions)
					.Select(eds => eds.ExamDateAndSessionID).ToList();

				var allExamOfferedRoomIDs = aspireContext.ExamOfferedRooms
					.Where(eor => eor.ExamConductID == examConductID)
					.Select(eor => eor.ExamOfferedRoomID).ToList();

				var invigilatorTypes = Enum.GetValues(typeof(Model.Entities.ExamInvigilatorDuty.InvigilatorTypes)).Cast<byte>().ToList();

				var examInvigilatorDuties = aspireContext.ExamInvigilatorDuties
					.Where(eid => eid.ExamOfferedRoom.ExamConductID == examConductID)
					.Select(eid => new
					{
						eid.ExamDateAndSessionID,
						eid.ExamOfferedRoomID,
						eid.ExamInvigilatorID,
						eid.InvigilatorType,
					}).ToList();

				uint counter = 0;
				foreach (var examDateAndSessionID in examDateAndSessionIDs)
				{
					var dutyAssigned = false;
					var dateAndSessionDuties = examInvigilatorDuties.Where(eid => eid.ExamDateAndSessionID == examDateAndSessionID).ToList();
					if (dateAndSessionDuties.Any(eds => eds.ExamInvigilatorID == examInvigilatorID))
						continue;
					foreach (var examOfferedRoomID in allExamOfferedRoomIDs)
					{
						var roomDuties = dateAndSessionDuties.Where(eds => eds.ExamOfferedRoomID == examOfferedRoomID).ToList();
						foreach (var invigilatorType in invigilatorTypes)
						{
							if (roomDuties.Any(eid => eid.InvigilatorType == invigilatorType))
								continue;
							aspireContext.ExamInvigilatorDuties.Add(new Model.Entities.ExamInvigilatorDuty
							{
								ExamDateAndSessionID = examDateAndSessionID,
								AttendanceStatusEnum = Model.Entities.ExamInvigilatorDuty.AttendanceStatuses.Absent,
								ExamInvigilatorID = examInvigilatorID,
								ExamOfferedRoomID = examOfferedRoomID,
								InvigilatorType = invigilatorType,
							});
							dutyAssigned = true;
							break;
						}
						if (dutyAssigned)
							break;
					}
					if (dutyAssigned)
						counter++;
				}
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);

				var wrongExists = aspireContext.ExamInvigilatorDuties
									.Where(eid => eid.ExamDateAndSession.ExamConductID == examConductID)
									.GroupBy(eid => new
									{
										eid.ExamOfferedRoomID,
										eid.ExamDateAndSessionID,
										eid.InvigilatorType
									}).Any(eid => eid.Count() != 1);

				if (wrongExists)
					throw new InvalidOperationException();
				aspireContext.CommitTransaction();
				return counter;
			}
		}

		public static uint AutoAssignAllDutiesToInvigilator(int examConductID, Guid loginSessionGuid)
		{
			List<int> examDateAndSessionIDs;
			using (var aspireContext = new AspireContext())
			{
				examDateAndSessionIDs = aspireContext.ExamDateAndSessions
					.Where(eds => eds.ExamConductID == examConductID)
					.Select(eds => eds.ExamDateAndSessionID)
					.ToList();
			}
			uint assignedDutiesCount = 0;
			foreach (var examDateAndSessionID in examDateAndSessionIDs)
			{
				AutoAssignAllDuties(examDateAndSessionID, out var duties, loginSessionGuid);
				assignedDutiesCount += duties;
			}
			return assignedDutiesCount;
		}
	}
}