﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff
{
	public static class ExamInvigilators
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.ManageExamInvigilators, UserGroupPermission.PermissionValues.Allowed);
		}

		public static List<CustomListItem> GetExamInvigilatorsList(Model.Entities.ExamInvigilator.Statuses? statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var examInvigilators = aspireContext.ExamInvigilators.Where(ei => ei.InstituteID == loginHistory.InstituteID);
				if (statusEnum != null)
					examInvigilators = examInvigilators.Where(ei => ei.Status == (byte)statusEnum);
				return examInvigilators.Select(ei => new CustomListItem
				{
					ID = ei.ExamInvigilatorID,
					Text = ei.Name,
				}).OrderBy(eds => eds.Text).ToList();
			}
		}

		public enum ToggleInvigilatorStatusResult
		{
			Success,
			NoRecordFound,
		}

		public static ToggleInvigilatorStatusResult ToggleInvigilatorStatus(int invigilatorID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var examInvigilator = aspireContext.ExamInvigilators.SingleOrDefault(ei => ei.ExamInvigilatorID == invigilatorID && ei.InstituteID == loginHistory.InstituteID);
				if (examInvigilator == null)
					return ToggleInvigilatorStatusResult.NoRecordFound;
				switch (examInvigilator.StatusEnum)
				{
					case Model.Entities.ExamInvigilator.Statuses.Active:
						examInvigilator.StatusEnum = Model.Entities.ExamInvigilator.Statuses.Inactive;
						break;
					case Model.Entities.ExamInvigilator.Statuses.Inactive:
						examInvigilator.StatusEnum = Model.Entities.ExamInvigilator.Statuses.Active;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return ToggleInvigilatorStatusResult.Success;
			}
		}

		public static Model.Entities.ExamInvigilator GetExamInvigilator(int examInvigilatorID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.ExamInvigilators.SingleOrDefault(ei => ei.ExamInvigilatorID == examInvigilatorID && ei.InstituteID == loginHistory.InstituteID);
			}
		}

		public sealed class ExamInvigilator
		{
			internal ExamInvigilator() { }
			public int ExamInvigilatorID { get; internal set; }
			public string InstituteName { get; internal set; }
			public string DepartmentName { get; internal set; }
			public string Designation { get; internal set; }
			public string CNIC { get; internal set; }
			public string Bank { get; internal set; }
			public string Name { get; internal set; }
			public string AccountNo { get; internal set; }
			public byte Status { get; set; }
			public string StatusFullName => ((Model.Entities.ExamInvigilator.Statuses)this.Status).ToFullName();
		}

		public sealed class GetExamInvigilatorsResult
		{
			public int VirtualItemCount { get; internal set; }
			public List<ExamInvigilator> ExamInvigilators { get; internal set; }
		}

		public static GetExamInvigilatorsResult GetExamInvigilators(int? departmentID, int? examDesignationID, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new InvalidOperationException(nameof(sortExpression));

			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = aspireContext.ExamInvigilators.Where(ei => ei.InstituteID == loginHistory.InstituteID);
				if (departmentID != null)
					query = query.Where(ei => ei.DepartmentID == departmentID.Value);
				if (examDesignationID != null)
					query = query.Where(ei => ei.ExamDesignationID == examDesignationID.Value);
				if (!string.IsNullOrWhiteSpace(searchText))
					query = query.Where(ei => ei.Name.Contains(searchText) || ei.Department.DepartmentName.Contains(searchText) || ei.ExamDesignation.Designation.Contains(searchText));

				var invigilators = query.Select(ei => new ExamInvigilator
				{
					ExamInvigilatorID = ei.ExamInvigilatorID,
					DepartmentName = ei.Department.DepartmentName,
					Designation = ei.ExamDesignation.Designation,
					Name = ei.Name,
					CNIC = ei.CNIC,
					Bank = ei.Bank,
					AccountNo = ei.AccountNo,
					Status = ei.Status
				});
				var virtualItemCount = query.Count();
				switch (sortExpression)
				{
					case nameof(ExamInvigilator.Name):
						invigilators = (sortDirection == SortDirection.Ascending) ? invigilators.OrderBy(inv => inv.Name) : invigilators.OrderByDescending(inv => inv.Name);
						break;
					case nameof(ExamInvigilator.Designation):
						invigilators = (sortDirection == SortDirection.Ascending) ? invigilators.OrderBy(inv => inv.Designation) : invigilators.OrderByDescending(inv => inv.Designation);
						break;
					case nameof(ExamInvigilator.DepartmentName):
						invigilators = (sortDirection == SortDirection.Ascending) ? invigilators.OrderBy(inv => inv.DepartmentName) : invigilators.OrderByDescending(inv => inv.DepartmentName);
						break;
					case nameof(ExamInvigilator.Bank):
						invigilators = (sortDirection == SortDirection.Ascending) ? invigilators.OrderBy(inv => inv.Bank) : invigilators.OrderByDescending(inv => inv.Bank);
						break;
					case nameof(ExamInvigilator.CNIC):
						invigilators = (sortDirection == SortDirection.Ascending) ? invigilators.OrderBy(inv => inv.CNIC) : invigilators.OrderByDescending(inv => inv.CNIC);
						break;
					case nameof(ExamInvigilator.Status):
						invigilators = (sortDirection == SortDirection.Ascending) ? invigilators.OrderBy(inv => inv.Status) : invigilators.OrderByDescending(inv => inv.Status);
						break;
					case nameof(ExamInvigilator.AccountNo):
						invigilators = (sortDirection == SortDirection.Ascending) ? invigilators.OrderBy(inv => inv.AccountNo) : invigilators.OrderByDescending(inv => inv.AccountNo);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				invigilators = invigilators.Skip(pageIndex * pageSize).Take(pageSize);
				return new GetExamInvigilatorsResult
				{
					VirtualItemCount = virtualItemCount,
					ExamInvigilators = invigilators.ToList()
				};
			}
		}

		public enum AddExamInvigilatorStatuses
		{
			CNICAlreadyExists,
			Success
		}

		public static AddExamInvigilatorStatuses AddExamInvigilator(int? departmentID, int examDesignationID, string name, string cnic, string bank, string accountNumber, Model.Entities.ExamInvigilator.Statuses status, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				if (aspireContext.ExamInvigilators.Any(ei => ei.CNIC == cnic && ei.InstituteID == loginHistory.InstituteID))
					return AddExamInvigilatorStatuses.CNICAlreadyExists;
				aspireContext.ExamInvigilators.Add(new Model.Entities.ExamInvigilator
				{
					InstituteID = loginHistory.InstituteID,
					DepartmentID = aspireContext.Departments.Where(d => d.DepartmentID == departmentID && d.InstituteID == loginHistory.InstituteID).Select(d => (int?)d.DepartmentID).SingleOrDefault(),
					ExamDesignationID = aspireContext.ExamDesignations.Where(d => d.ExamDesignationID == examDesignationID && d.InstituteID == loginHistory.InstituteID).Select(d => d.ExamDesignationID).SingleOrDefault(),
					Name = name,
					CNIC = cnic,
					Bank = bank,
					AccountNo = accountNumber,
					StatusEnum = status,
				}.Validate());
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddExamInvigilatorStatuses.Success;
			}
		}

		public enum UpdateExamInvigilatorStatuses
		{
			Success,
			NoRecordFound,
			CNICAlreadyExists
		}

		public static UpdateExamInvigilatorStatuses UpdateExamInvigilator(int examInvigilatorID, int? departmentID, int examDesignationID, string name, string cnic, string bank, string accountNumber, Model.Entities.ExamInvigilator.Statuses status, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var examInvigilators = aspireContext.ExamInvigilators.Where(ei => ei.InstituteID == loginHistory.InstituteID);
				var examInvigilator = examInvigilators.SingleOrDefault(ei => ei.ExamInvigilatorID == examInvigilatorID);
				if (examInvigilator == null)
					return UpdateExamInvigilatorStatuses.NoRecordFound;

				examInvigilator.DepartmentID = departmentID;
				examInvigilator.ExamDesignationID = examDesignationID;
				examInvigilator.Name = name;
				examInvigilator.CNIC = cnic;
				examInvigilator.Bank = bank;
				examInvigilator.AccountNo = accountNumber;
				examInvigilator.StatusEnum = status;
				examInvigilator.Validate();
				if (examInvigilators.Any(ei => ei.ExamInvigilatorID != examInvigilatorID && ei.CNIC == examInvigilator.CNIC))
					return UpdateExamInvigilatorStatuses.CNICAlreadyExists;

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateExamInvigilatorStatuses.Success;
			}
		}

		public enum DeleteExamInvigilatorStatuses
		{
			Success,
			NoRecordFound,
			CannotDeleteChildRecordExistsExamInvigilatorDuties,
			CannotDeleteChildRecordExistsExamInvigilatorExamSessions
		}

		public static DeleteExamInvigilatorStatuses DeleteExamInvigilator(int examInvigilatorID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var examInvigilator = aspireContext.ExamInvigilators.SingleOrDefault(ei => ei.InstituteID == loginHistory.InstituteID && ei.ExamInvigilatorID == examInvigilatorID);
				if (examInvigilator == null)
					return DeleteExamInvigilatorStatuses.NoRecordFound;
				if (examInvigilator.ExamInvigilatorDuties.Any())
					return DeleteExamInvigilatorStatuses.CannotDeleteChildRecordExistsExamInvigilatorDuties;
				if (examInvigilator.ExamInvigilatorExamSessions.Any(eies => eies.Available))
					return DeleteExamInvigilatorStatuses.CannotDeleteChildRecordExistsExamInvigilatorExamSessions;

				aspireContext.ExamInvigilatorExamSessions.RemoveRange(examInvigilator.ExamInvigilatorExamSessions.Where(eies => !eies.Available));
				aspireContext.ExamInvigilators.Remove(examInvigilator);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteExamInvigilatorStatuses.Success;
			}
		}

		public sealed class ExamInvigilatorExamSessions
		{
			internal ExamInvigilatorExamSessions() { }

			public string DepartmentName { get; internal set; }
			public string Designation { get; internal set; }
			public int ExamInvigilatorID { get; internal set; }
			public List<int> ExamSessionIDsList { get; internal set; }
			public string Name { get; internal set; }
		}

		public sealed class GetExamInvigilatorExamSessionsResult
		{
			internal GetExamInvigilatorExamSessionsResult() { }
			public List<ExamInvigilatorExamSessions> ExamInvigilatorAndSessions { get; internal set; }
			public List<CustomListItem> ExamSessions { get; internal set; }
		}

		public static GetExamInvigilatorExamSessionsResult GetExamInvigilatorExamSessions(int examConductID, int? departmentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var examInvigilators = aspireContext.ExamInvigilators
					.Where(ei => ei.InstituteID == loginHistory.InstituteID && ei.Status == Model.Entities.ExamInvigilator.StatusActive);
				if (departmentID != null)
					examInvigilators = examInvigilators.Where(ei => ei.DepartmentID == departmentID.Value);

				return new GetExamInvigilatorExamSessionsResult
				{
					ExamInvigilatorAndSessions = examInvigilators.Select(ei => new ExamInvigilatorExamSessions
					{
						ExamInvigilatorID = ei.ExamInvigilatorID,
						DepartmentName = ei.Department.DepartmentName,
						Designation = ei.ExamDesignation.Designation,
						Name = ei.Name,
						ExamSessionIDsList = ei.ExamInvigilatorExamSessions
							.Where(eies => eies.Available && eies.ExamSession.ExamConductID == examConductID)
							.Select(eies => eies.ExamSessionID).ToList()
					}).OrderBy(i => i.Name).ToList(),
					ExamSessions = aspireContext.ExamSessions.Where(es => es.ExamConductID == examConductID && es.ExamConduct.InstituteID == loginHistory.InstituteID).Select(es => new CustomListItem
					{
						ID = es.ExamSessionID,
						Text = es.SessionName,
					}).OrderBy(i => i.Text).ToList()
				};
			}
		}

		public enum UpdateExamInvigilatorExamSessionsStatuses
		{
			NoRecordFound,
			Success
		}

		public static UpdateExamInvigilatorExamSessionsStatuses UpdateExamInvigilatorExamSessions(int examConductID, int? departmentID, Dictionary<int, List<int>> examSessions, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var examInvigilators = aspireContext.ExamInvigilators
					.Where(ei => ei.InstituteID == loginHistory.InstituteID && ei.Status == Model.Entities.ExamInvigilator.StatusActive)
					.Where(ei => departmentID == null || ei.DepartmentID == departmentID.Value)
					.Select(ei => new
					{
						ei.ExamInvigilatorID,
						ei.DepartmentID,
						ExamInvigilatorExamSessions = ei.ExamInvigilatorExamSessions.Where(eies => eies.ExamSession.ExamConductID == examConductID).ToList()
					}).ToList();

				foreach (var examInvigilator in examInvigilators)
				{
					if (!examSessions.TryGetValue(examInvigilator.ExamInvigilatorID, out var newSessions))
						return UpdateExamInvigilatorExamSessionsStatuses.NoRecordFound;

					foreach (var examInvigilatorExamSession in examInvigilator.ExamInvigilatorExamSessions)
						examInvigilatorExamSession.Available = false;
					foreach (var examSessionID in newSessions)
					{
						var examInvigilatorExamSession = examInvigilator.ExamInvigilatorExamSessions.SingleOrDefault(eies => eies.ExamSessionID == examSessionID);
						if (examInvigilatorExamSession != null)
							examInvigilatorExamSession.Available = true;
						else
							aspireContext.ExamInvigilatorExamSessions.Add(new ExamInvigilatorExamSession
							{
								ExamInvigilatorID = examInvigilator.ExamInvigilatorID,
								Available = true,
								ExamSessionID = examSessionID
							});
					}
				}

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateExamInvigilatorExamSessionsStatuses.Success;
			}
		}
	}
}
