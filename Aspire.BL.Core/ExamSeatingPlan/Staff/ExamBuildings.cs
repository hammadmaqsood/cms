﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff
{
	public static class ExamBuildings
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, params UserGroupPermission.PermissionValues[] permissionValues)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.ManageExamBuildings, permissionValues);
		}

		public enum AddExamBuildingStatuses
		{
			NameAlreadyExists,
			Success
		}

		public static AddExamBuildingStatuses AddExamBuilding(string buildingName, ExamBuilding.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				buildingName = buildingName.TrimAndCannotBeEmpty();
				if (aspireContext.ExamBuildings.Any(eb => eb.InstituteID == loginHistory.InstituteID && eb.BuildingName == buildingName))
					return AddExamBuildingStatuses.NameAlreadyExists;
				var examBuilding = new ExamBuilding
				{
					InstituteID = loginHistory.InstituteID,
					BuildingName = buildingName,
					StatusEnum = statusEnum,
				};
				aspireContext.ExamBuildings.Add(examBuilding);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddExamBuildingStatuses.Success;
			}
		}

		public enum UpdateExamBuildingStatuses
		{
			NoRecordFound,
			NameAlreadyExists,
			Success
		}

		public static UpdateExamBuildingStatuses UpdateExamBuilding(int examBuildingID, string buildingName, ExamBuilding.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				buildingName = buildingName.TrimAndCannotBeEmpty();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var examBuilding = aspireContext.ExamBuildings.SingleOrDefault(eb => eb.ExamBuildingID == examBuildingID && eb.InstituteID == loginHistory.InstituteID);
				if (examBuilding == null)
					return UpdateExamBuildingStatuses.NoRecordFound;
				if (aspireContext.ExamBuildings.Any(eb => eb.ExamBuildingID != examBuildingID && eb.BuildingName == buildingName && eb.InstituteID == loginHistory.InstituteID))
					return UpdateExamBuildingStatuses.NameAlreadyExists;
				examBuilding.BuildingName = buildingName;
				examBuilding.StatusEnum = statusEnum;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				return UpdateExamBuildingStatuses.Success;
			}
		}

		public enum DeleteExamBuildingStatuses
		{
			NoRecordFound,
			CannotDeleteChildRecordExistsExamRooms,
			Success,
		}

		public static DeleteExamBuildingStatuses DeleteExamBuilding(int examBuildingID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var examBuilding = aspireContext.ExamBuildings.FirstOrDefault(eb => eb.ExamBuildingID == examBuildingID && eb.InstituteID == loginHistory.InstituteID);
				if (examBuilding == null)
					return DeleteExamBuildingStatuses.NoRecordFound;
				if (examBuilding.ExamRooms.Any())
					return DeleteExamBuildingStatuses.CannotDeleteChildRecordExistsExamRooms;
				aspireContext.ExamBuildings.Remove(examBuilding);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteExamBuildingStatuses.Success;
			}
		}

		public static ExamBuilding GetExamBuilding(int examBuildingID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.ExamBuildings.SingleOrDefault(eb => eb.ExamBuildingID == examBuildingID && eb.InstituteID == loginHistory.InstituteID);
			}
		}

		public static List<ExamBuilding> GetExamBuildings(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.ExamBuildings.OrderBy(eb => eb.BuildingName).Where(eb => eb.InstituteID == loginHistory.InstituteID).ToList();
			}
		}

		public enum UpdateExamBuildingStatusResults
		{
			NoRecordFound,
			Success
		}

		public static UpdateExamBuildingStatusResults UpdateExamBuildingStatus(int examBuildingID, ExamBuilding.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var examBuilding = aspireContext.ExamBuildings.SingleOrDefault(eb => eb.ExamBuildingID == examBuildingID && eb.InstituteID == loginHistory.InstituteID);
				if (examBuilding == null)
					return UpdateExamBuildingStatusResults.NoRecordFound;
				examBuilding.StatusEnum = statusEnum;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateExamBuildingStatusResults.Success;
			}
		}

		public static List<CustomListItem> GetExamBuildingsList(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.ExamBuildings.Where(eb => eb.InstituteID == loginHistory.InstituteID).Select(eb => new CustomListItem
				{
					Text = eb.BuildingName,
					ID = eb.ExamBuildingID
				}).OrderBy(eb => eb.Text).ToList();
			}
		}
	}
}
