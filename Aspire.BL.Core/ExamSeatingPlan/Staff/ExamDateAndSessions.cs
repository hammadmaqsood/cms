using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff
{
	public static class ExamDateAndSessions
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, params UserGroupPermission.PermissionValues[] permissionValues)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.ManageExamDateAndSessions, permissionValues);
		}

		public sealed class ExamDateAndSession
		{
			internal ExamDateAndSession()
			{
			}

			public int ExamDateAndSessionID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public byte ExamType { get; internal set; }
			public string ExamTypeFullName => ((ExamConduct.ExamTypes)this.ExamType).ToFullName();
			public string ExamConductName { get; internal set; }
			public DateTime Date { get; internal set; }
			public string SessionName { get; internal set; }
			public TimeSpan StartTime { get; internal set; }
			public string StartTimeString
			{
				get
				{
					if (this.StartTime.Hours > 12)
						return $"{this.StartTime.Add(TimeSpan.FromHours(-12)):hh\\:mm} PM";
					return $"{this.StartTime:hh\\:mm} AM";
				}
			}
			public byte TotalTimeInMinutes { get; internal set; }
			public string Timing => $"{this.StartTime:hh\\:mm} - {this.StartTime.Add(TimeSpan.FromMinutes(this.TotalTimeInMinutes)):hh\\:mm}";
			public byte Shift { get; internal set; }
			public string ShiftFullName => ((Shifts)this.Shift).ToFullName();
		}

		public static List<ExamDateAndSession> GetExamDateAndSessions(int examConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var examDateAndSessions = aspireContext.ExamDateAndSessions.Where(es => es.ExamConductID == examConductID && es.ExamConduct.InstituteID == loginHistory.InstituteID).OrderBy(eds => eds.ExamDate.Date).ThenBy(eds => eds.ExamSession.Sequence);
				return examDateAndSessions.Select(eds => new ExamDateAndSession
				{
					ExamDateAndSessionID = eds.ExamDateAndSessionID,
					SemesterID = eds.ExamConduct.SemesterID,
					ExamType = eds.ExamConduct.ExamType,
					ExamConductName = eds.ExamConduct.Name,
					Date = eds.ExamDate.Date,
					SessionName = eds.ExamSession.SessionName,
					StartTime = eds.ExamSession.StartTime,
					TotalTimeInMinutes = eds.ExamSession.TotalTimeInMinutes,
					Shift = eds.ExamSession.Shift,
				}).ToList();
			}
		}

		public static List<CustomListItem> GetExamDateAndSessionsList(int examConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandStaffActiveSession(loginSessionGuid);
				return aspireContext.ExamDateAndSessions.Where(ec => ec.ExamConductID == examConductID).OrderBy(c => c.ExamDate.Date).ThenBy(c => c.ExamSession.Sequence).Select(c => new
				{
					c.ExamDateAndSessionID,
					c.ExamDate.Date,
					c.ExamSession.SessionName,
				}).ToList().Select(i => new CustomListItem
				{
					ID = i.ExamDateAndSessionID,
					Text = AspireFormats.GetExamDateAndSessionString(i.Date, i.SessionName),
				}).ToList();
			}
		}

		public enum AddExamDateAndSessionStatuses
		{
			Success,
			NameAlreadyExists,
		}

		public static AddExamDateAndSessionStatuses AddExamDateAndSession(int examConductID, int examDateID, int examSessionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				if (aspireContext.ExamDateAndSessions.Any(eds => eds.ExamConductID == examConductID && eds.ExamDateID == examDateID && eds.ExamSessionID == examSessionID))
					return AddExamDateAndSessionStatuses.NameAlreadyExists;
				var examDateAndSession = new Model.Entities.ExamDateAndSession
				{
					ExamConductID = aspireContext.ExamConducts.Where(ec => ec.ExamConductID == examConductID && ec.InstituteID == loginHistory.InstituteID).Select(ec => ec.ExamConductID).Single(),
					ExamDateID = aspireContext.ExamDates.Where(ed => ed.ExamConductID == examConductID && ed.ExamDateID == examDateID).Select(ed => ed.ExamDateID).Single(),
					ExamSessionID = aspireContext.ExamSessions.Where(es => es.ExamConductID == examConductID && es.ExamSessionID == examSessionID).Select(es => es.ExamSessionID).Single(),
				};
				aspireContext.ExamDateAndSessions.Add(examDateAndSession);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddExamDateAndSessionStatuses.Success;
			}
		}

		public enum DeleteExamDateAndSessionStatuses
		{
			NoRecordFound,
			Success,
			ChildRecordExistsExamDateSheets,
			ChildRecordExistsExamInvigilatorDuties
		}

		public static DeleteExamDateAndSessionStatuses DeleteExamDateAndSession(int examDateAndSessionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var examDateAndSession = aspireContext.ExamDateAndSessions.FirstOrDefault(eds => eds.ExamDateAndSessionID == examDateAndSessionID && eds.ExamConduct.InstituteID == loginHistory.InstituteID);
				if (examDateAndSession == null)
					return DeleteExamDateAndSessionStatuses.NoRecordFound;
				if (examDateAndSession.ExamDateSheets.Any())
					return DeleteExamDateAndSessionStatuses.ChildRecordExistsExamDateSheets;
				if (examDateAndSession.ExamInvigilatorDuties.Any())
					return DeleteExamDateAndSessionStatuses.ChildRecordExistsExamInvigilatorDuties;
				aspireContext.ExamDateAndSessions.Remove(examDateAndSession);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteExamDateAndSessionStatuses.Success;
			}
		}
	}
}