﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.ExamSeatingPlan.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff
{
	public static class IndividualSeating
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.IndividualSeating, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class RegisteredCourse
		{
			internal RegisteredCourse() { }

			public DateTime Date { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public int RegisteredCourseID { get; internal set; }
			public int Section { get; internal set; }
			public short SemesterNo { get; internal set; }
			public string SessionName { get; internal set; }
			public byte Shift { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public byte Sequence { get; internal set; }
			public string Title { get; internal set; }
		}

		public sealed class GetNotSeatedRegisteredCoursesResult
		{
			public int VirtualItemCount { get; internal set; }
			public List<RegisteredCourse> NotSeatedRegisteredCourses { get; internal set; }
		}

		public static GetNotSeatedRegisteredCoursesResult GetNotSeatedRegisteredCourses(int examConductID, int? offeredCourseID, string enrollment, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var examConduct = aspireContext.ExamConducts.Where(ec => ec.ExamConductID == examConductID && ec.InstituteID == loginHistory.InstituteID)
					.Select(ec => new
					{
						ec.SemesterID,
						ec.InstituteID,

					}).SingleOrDefault();
				if (examConduct == null)
					return null;
				var seatedRegisteredCourseIDs = aspireContext.ExamSeats
					.Where(es => es.ExamOfferedRoom.ExamConductID == examConductID && es.RegisteredCours.DeletedDate == null)
					.Select(es => es.RegisteredCourseID);

				var notSeatedRegisteredCourses = aspireContext
					.GetValidRegisteredCourses(examConductID, null)
					.FilterValidRegisteredCoursesForSeatingPlan()
					.Where(rc => !seatedRegisteredCourseIDs.Contains(rc.RegisteredCourseID));
				if (offeredCourseID != null)
					notSeatedRegisteredCourses = notSeatedRegisteredCourses.Where(rc => rc.OfferedCourseID == offeredCourseID.Value);
				if (!string.IsNullOrWhiteSpace(enrollment))
					notSeatedRegisteredCourses = notSeatedRegisteredCourses.Where(rc => rc.Student.Enrollment == enrollment);
				var examDateSheets = aspireContext.ExamDateSheets.Where(eds => eds.ExamDateAndSession.ExamConductID == examConductID);

				var notSeated = from rc in notSeatedRegisteredCourses
								join eds in examDateSheets on rc.OfferedCourseID equals eds.OfferedCourseID
								select new RegisteredCourse
								{
									RegisteredCourseID = rc.RegisteredCourseID,
									Enrollment = rc.Student.Enrollment,
									Name = rc.Student.Name,
									ProgramAlias = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
									SemesterNo = rc.OfferedCours.SemesterNo,
									Section = rc.OfferedCours.Section,
									Shift = rc.OfferedCours.Shift,
									Date = eds.ExamDateAndSession.ExamDate.Date,
									SessionName = eds.ExamDateAndSession.ExamSession.SessionName,
									Sequence = eds.ExamDateAndSession.ExamSession.Sequence,
									Title = eds.OfferedCours.Cours.Title,
								};
				var virtualItemCount = notSeated.Count();

				switch (sortExpression)
				{
					case nameof(RegisteredCourse.Enrollment):
						notSeated = notSeated.OrderBy(sortDirection, rc => rc.Enrollment);
						break;
					case nameof(RegisteredCourse.Name):
						notSeated = notSeated.OrderBy(sortDirection, rc => rc.Name);
						break;
					case nameof(RegisteredCourse.Class):
						notSeated = notSeated.OrderBy(sortDirection, rc => rc.ProgramAlias).ThenBy(sortDirection, rc => rc.SemesterNo).ThenBy(sortDirection, rc => rc.Section).ThenBy(sortDirection, rc => rc.Shift);
						break;
					case nameof(RegisteredCourse.Date):
						notSeated = notSeated.OrderBy(sortDirection, rc => rc.Date);
						break;
					case nameof(RegisteredCourse.SessionName):
						notSeated = notSeated.OrderBy(sortDirection, rc => rc.Sequence);
						break;
					case nameof(RegisteredCourse.Title):
						notSeated = notSeated.OrderBy(sortDirection, rc => rc.Title);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}

				notSeated = notSeated.Skip(pageIndex * pageSize).Take(pageSize);
				return new GetNotSeatedRegisteredCoursesResult
				{
					VirtualItemCount = virtualItemCount,
					NotSeatedRegisteredCourses = notSeated.ToList(),
				};
			}
		}

		public sealed class FreeSeat
		{
			internal FreeSeat() { }
			public int ExamOfferedRoomID { get; internal set; }
			public string BuildingName { get; internal set; }
			public string RoomName { get; internal set; }
			public string RoomUsageType { get; internal set; }
			public bool ClassFellows { get; internal set; }
			public int FreeSeats { get; internal set; }
		}

		public static List<FreeSeat> GetFreeSeats(int examConductID, int registeredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var registeredCourse = aspireContext
					.GetValidRegisteredCourses(examConductID, null)
					.FilterValidRegisteredCoursesForSeatingPlan()
					.Where(rc => rc.RegisteredCourseID == registeredCourseID)
					.Select(rc => new
					{
						rc.StudentID,
						rc.RegisteredCourseID,
						rc.OfferedCourseID,
						rc.OfferedCours.SemesterID,
					}).SingleOrDefault();
				if (registeredCourse == null)
					return null;

				var examDateSheet = aspireContext.GetExamDateSheets(examConductID, null)
					.Where(eds => eds.OfferedCourseID == registeredCourse.OfferedCourseID && eds.ExamDateAndSession.ExamConduct.InstituteID == loginHistory.InstituteID)
					.Select(eds => new { eds.ExamDateAndSessionID }).SingleOrDefault();
				if (examDateSheet == null)
					return null;

				var papers = aspireContext
					.GetExamDateSheets(examConductID, examDateSheet.ExamDateAndSessionID)
					.SelectMany(eds => eds.OfferedCours.RegisteredCourses)
					.FilterValidRegisteredCoursesForSeatingPlan()
					.Count(rc => rc.StudentID == registeredCourse.StudentID);
				if (papers <= 0)
					return null;

				var roomUsageType = papers == 1 ? ExamOfferedRoom.RoomUsageTypeSinglePaper : ExamOfferedRoom.RoomUsageTypeMultiplePapers;

				var allSeats = aspireContext.GetAllExamOfferedRoomSeats(examConductID, roomUsageType).Select(os => new
				{
					os.ExamOfferedRoomID,
					os.Row,
					os.Column
				}).ToList();
				var seats = aspireContext.GetExamSeats(examConductID, examDateSheet.ExamDateAndSessionID, (ExamOfferedRoom.RoomUsageTypes)roomUsageType)
					.Select(es => new
					{
						es.ExamOfferedRoomID,
						es.Row,
						es.Column,
					}).ToList();

				var classFellowExamOfferedRoomIDs = aspireContext
					.GetExamSeats(examConductID, examDateSheet.ExamDateAndSessionID, (ExamOfferedRoom.RoomUsageTypes)roomUsageType)
					.Where(es => es.RegisteredCours.OfferedCourseID == registeredCourse.OfferedCourseID)
					.Select(es => es.ExamOfferedRoomID).Distinct().ToList();

				var freeSeats = allSeats.Except(seats)
					.GroupBy(fs => fs.ExamOfferedRoomID)
					.Select(fs => new
					{
						ExamOfferedRoomID = fs.Key,
						FreeSeats = fs.Count(),
					}).ToList();

				var examOfferedRooms = aspireContext.ExamOfferedRooms
					.Where(eor => eor.ExamConductID == examConductID)
					.Select(eor => new
					{
						eor.ExamOfferedRoomID,
						eor.ExamRoom.RoomName,
						eor.ExamRoom.ExamBuilding.BuildingName,
						eor.RoomUsageType,
					}).ToList();

				return (from fs in freeSeats
						join eor in examOfferedRooms on fs.ExamOfferedRoomID equals eor.ExamOfferedRoomID
						select new FreeSeat
						{
							ExamOfferedRoomID = fs.ExamOfferedRoomID,
							BuildingName = eor.BuildingName,
							RoomName = eor.RoomName,
							RoomUsageType = ((ExamOfferedRoom.RoomUsageTypes)eor.RoomUsageType).ToFullName(),
							ClassFellows = classFellowExamOfferedRoomIDs.Contains(fs.ExamOfferedRoomID),
							FreeSeats = fs.FreeSeats,
						}
					).ToList();
			}
		}
	}
}