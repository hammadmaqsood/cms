﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff
{
	public static class ExamConducts
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, params UserGroupPermission.PermissionValues[] permissionValues)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.ManageExamConducts, permissionValues);
		}

		public static List<ExamConduct> GetExamConducts(short? semesterID, ExamConduct.ExamTypes? examTypeEnum, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var examConducts = aspireContext.ExamConducts.Where(ec => ec.InstituteID == loginHistory.InstituteID);
				if (semesterID != null)
					examConducts = examConducts.Where(q => q.SemesterID == semesterID);
				if (examTypeEnum != null)
				{
					var examType = (byte)examTypeEnum.Value;
					examConducts = examConducts.Where(q => q.ExamType == examType);
				}
				virtualItemCount = examConducts.Count();
				switch (sortExpression)
				{
					case nameof(ExamConduct.SemesterID):
						examConducts = examConducts.OrderBy(sortDirection, q => q.SemesterID);
						break;
					case nameof(ExamConduct.ExamType):
						examConducts = examConducts.OrderBy(sortDirection, q => q.ExamType);
						break;
					case nameof(ExamConduct.Name):
						examConducts = examConducts.OrderBy(sortDirection, q => q.Name);
						break;
					case nameof(ExamConduct.BlockFeeDefaulters):
						examConducts = examConducts.OrderBy(sortDirection, q => q.BlockFeeDefaulters);
						break;
					case nameof(ExamConduct.VisibleToStudents):
						examConducts = examConducts.OrderBy(sortDirection, q => q.VisibleToStudents);
						break;
					case nameof(ExamConduct.VisibleToFacultyMembers):
						examConducts = examConducts.OrderBy(sortDirection, q => q.VisibleToFacultyMembers);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return examConducts.Skip(pageIndex * pageSize).Take(pageSize).ToList();
			}
		}

		public static ExamConduct GetExamConduct(int examConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.ExamConducts.SingleOrDefault(ec => ec.ExamConductID == examConductID && ec.InstituteID == loginHistory.InstituteID);
			}
		}

		public enum AddExamConductStatuses
		{
			NameAlreadyExist,
			Success
		}

		public static AddExamConductStatuses AddExamConduct(short semesterID, ExamConduct.ExamTypes examTypeEnum, string name, bool blockFeeDefaulters, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				name = name.TrimAndCannotBeEmpty();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var examType = (byte)examTypeEnum;
				var alreadyExists = aspireContext.ExamConducts.Any(ec => ec.InstituteID == loginHistory.InstituteID && ec.SemesterID == semesterID && ec.ExamType == examType && ec.Name == name);
				if (alreadyExists)
					return AddExamConductStatuses.NameAlreadyExist;
				var examConduct = new ExamConduct
				{
					InstituteID = loginHistory.InstituteID,
					SemesterID = semesterID,
					ExamType = examType,
					Name = name,
					BlockFeeDefaulters = blockFeeDefaulters,
					VisibleToStudents = false,
					VisibleToFacultyMembers = false
				};
				aspireContext.ExamConducts.Add(examConduct);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddExamConductStatuses.Success;
			}
		}

		public enum UpdateExamConductStatuses
		{
			NoRecordFound,
			NameAlreadyExists,
			CanNotVisibleToStudentsDueToNoExamDatesFound,
			CanNotVisibleToStudentsDueToExamsFinished,
			CanNotVisibleToStudentsDueToTeacherEvaluationSurveyBeforeMidNotOpen,
			CanNotVisibleToStudentsDueToTeacherEvaluationSurveyBeforeFinalNotOpen,
			CanNotVisibleToStudentsDueToCourseEvaluationSurveyBeforeFinalNotOpen,
			CanNotVisibleToStudentsDueToGraduateSurveyBeforeFinalNotOpen,
			Success
		}

		public static UpdateExamConductStatuses UpdateExamConduct(int examConductID, string name, bool blockFeeDefaulters, bool visibleToStudents, bool visibleToFacultyMembers, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				name = name.TrimAndCannotBeEmpty();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var examConduct = aspireContext.ExamConducts.SingleOrDefault(ec => ec.ExamConductID == examConductID && ec.InstituteID == loginHistory.InstituteID);
				if (examConduct == null)
					return UpdateExamConductStatuses.NoRecordFound;

				var alreadyExists = aspireContext.ExamConducts.Any(ec => ec.ExamConductID != examConduct.ExamConductID && ec.InstituteID == loginHistory.InstituteID && ec.SemesterID == examConduct.SemesterID && ec.ExamType == examConduct.ExamType && ec.Name == name);
				if (alreadyExists)
					return UpdateExamConductStatuses.NameAlreadyExists;

				examConduct.Name = name;
				examConduct.BlockFeeDefaulters = blockFeeDefaulters;
				if (visibleToStudents)
				{
					var maxExamDate = aspireContext.ExamDateAndSessions
						.Where(eds => eds.ExamConductID == examConductID)
						.Select(eds => eds.ExamDate)
						.Where(eds => eds.Date >= DateTime.Today)
						.OrderByDescending(ed => ed.Date)
						.Select(ed => (DateTime?)ed.Date)
						.FirstOrDefault();
					if (maxExamDate == null)
						return UpdateExamConductStatuses.CanNotVisibleToStudentsDueToNoExamDatesFound;
					if (maxExamDate.Value < DateTime.Today)
						return UpdateExamConductStatuses.CanNotVisibleToStudentsDueToExamsFinished;

					var minExamDate = aspireContext.ExamDateAndSessions
						.Where(eds => eds.ExamConductID == examConductID)
						.Select(eds => eds.ExamDate)
						.Where(eds => eds.Date >= DateTime.Today)
						.OrderBy(ed => ed.Date)
						.Select(ed => ed.Date)
						.First();

					var surveyConducts = aspireContext.SurveyConducts
						.Where(sc => sc.Survey.InstituteID == loginHistory.InstituteID)
						.Where(sc => sc.SemesterID == examConduct.SemesterID)
						.Where(sc => sc.Status == SurveyConduct.StatusActive)
						.Where(sc => sc.OpenDate <= minExamDate)
						.Where(sc => sc.CloseDate == null || maxExamDate.Value <= sc.CloseDate.Value);

					var teacherEvaluationBeforeMid = surveyConducts
						.Where(sc => sc.SurveyConductType == SurveyConduct.SurveyConductTypeBeforeMidTerm)
						.Where(sc => sc.Survey.SurveyType == Survey.SurveyTypeStudentCourseWise)
						.Any(sc => sc.Survey.SurveyName == "Teacher Evaluation Form (Survey Form-10)");
					var teacherEvaluationBeforeFinal = surveyConducts
						.Where(sc => sc.SurveyConductType == SurveyConduct.SurveyConductTypeBeforeFinalTerm)
						.Where(sc => sc.Survey.SurveyType == Survey.SurveyTypeStudentCourseWise)
						.Any(sc => sc.Survey.SurveyName == "Teacher Evaluation Form (Survey Form-10)");
					var courseEvaluationBeforeFinal = surveyConducts
						.Where(sc => sc.SurveyConductType == SurveyConduct.SurveyConductTypeBeforeFinalTerm)
						.Where(sc => sc.Survey.SurveyType == Survey.SurveyTypeStudentCourseWise)
						.Any(sc => sc.Survey.SurveyName == "Student Course Evaluation Questionnaire (Survey Form-1)");
					var graduatingStudentsBeforeFinal = surveyConducts
						.Where(sc => sc.SurveyConductType == SurveyConduct.SurveyConductTypeBeforeFinalTerm)
						.Where(sc => sc.Survey.SurveyType == Survey.SurveyTypeStudentWise)
						.Any(sc => sc.Survey.SurveyName == "Survey of Graduating Students (Survey Form-3)");

					switch (examConduct.ExamTypeEnum)
					{
						case ExamConduct.ExamTypes.Mid:
							if (!teacherEvaluationBeforeMid)
								return UpdateExamConductStatuses.CanNotVisibleToStudentsDueToTeacherEvaluationSurveyBeforeMidNotOpen;
							break;
						case ExamConduct.ExamTypes.Final:
							if (!teacherEvaluationBeforeMid)
								return UpdateExamConductStatuses.CanNotVisibleToStudentsDueToTeacherEvaluationSurveyBeforeMidNotOpen;
							if (!graduatingStudentsBeforeFinal)
								return UpdateExamConductStatuses.CanNotVisibleToStudentsDueToGraduateSurveyBeforeFinalNotOpen;
							if (!courseEvaluationBeforeFinal)
								return UpdateExamConductStatuses.CanNotVisibleToStudentsDueToCourseEvaluationSurveyBeforeFinalNotOpen;
							if (!teacherEvaluationBeforeFinal)
								return UpdateExamConductStatuses.CanNotVisibleToStudentsDueToTeacherEvaluationSurveyBeforeFinalNotOpen;
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					examConduct.VisibleToStudents = true;
				}
				else
				{
					examConduct.VisibleToStudents = false;
				}
				examConduct.VisibleToFacultyMembers = visibleToFacultyMembers;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateExamConductStatuses.Success;
			}
		}

		public enum DeleteExamConductStatuses
		{
			NoRecordFound,
			ChildRecordExistsExamDateAndSessions,
			ChildRecordExistsExamDates,
			ChildRecordExistsExamOfferedRooms,
			ChildRecordExistsExamSessions,
			Success,
		}

		public static DeleteExamConductStatuses DeleteExamConduct(int examConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var examConduct = aspireContext.ExamConducts.FirstOrDefault(ec => ec.ExamConductID == examConductID && ec.InstituteID == loginHistory.InstituteID);
				if (examConduct == null)
					return DeleteExamConductStatuses.NoRecordFound;
				if (examConduct.ExamDateAndSessions.Any())
					return DeleteExamConductStatuses.ChildRecordExistsExamDateAndSessions;
				if (examConduct.ExamDates.Any())
					return DeleteExamConductStatuses.ChildRecordExistsExamDates;
				if (examConduct.ExamOfferedRooms.Any())
					return DeleteExamConductStatuses.ChildRecordExistsExamOfferedRooms;
				if (examConduct.ExamSessions.Any())
					return DeleteExamConductStatuses.ChildRecordExistsExamSessions;

				aspireContext.ExamConducts.Remove(examConduct);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteExamConductStatuses.Success;
			}
		}

		public static List<CustomListItem> GetExamConductsList(short semesterID, ExamConduct.ExamTypes examTypeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var examType = (byte)examTypeEnum;
				return aspireContext.ExamConducts.Where(ec => ec.InstituteID == loginHistory.InstituteID && ec.SemesterID == semesterID && ec.ExamType == examType).Select(c => new CustomListItem
				{
					ID = c.ExamConductID,
					Text = c.Name,
				}).ToList();
			}
		}
	}
}
