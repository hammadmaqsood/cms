﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.ExamSeatingPlan.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff
{
	public static class ExecuteSeatingPlan
	{
		private static BL.Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static BL.Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.ExecuteSeatingPlan, UserGroupPermission.PermissionValues.Allowed);
		}

		private static BL.Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissionsIndividualSeating(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.IndividualSeating, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class Summary
		{
			internal Summary()
			{
			}

			public short SemesterID { get; set; }
			public string Semester => this.SemesterID.ToSemesterString();

			public byte ExamType { get; internal set; }
			public string ExamConductName { get; internal set; }
			public int ExamDateAndSessionID { get; internal set; }
			public int ExamConductID { get; internal set; }
			public DateTime Date { get; internal set; }
			public string SessionName { get; internal set; }
			public int Rooms { get; internal set; }
			public int TotalSeats { get; internal set; }
			public int RegisteredCourses { get; internal set; }
			public int Seats { get; internal set; }
			public int FreeSeats => this.TotalSeats - this.Seats;
			public List<string> Statuses { get; set; }
			public string StatusFullName => this.Statuses != null ? string.Join("<br />", this.Statuses.Select(s => s.HtmlEncode())) : null;
		}

		public static List<Summary> GetSummary(int examConductID, ExamOfferedRoom.RoomUsageTypes roomUsageTypeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var examDateAndSessions = aspireContext.ExamDateAndSessions.Where(eds => eds.ExamConductID == examConductID && eds.ExamConduct.InstituteID == loginHistory.InstituteID)
					.OrderBy(eds => eds.ExamDate.Date).ThenBy(eds => eds.ExamSession.Sequence).Select(eds => new Summary
					{
						ExamConductID = eds.ExamConductID,
						ExamDateAndSessionID = eds.ExamDateAndSessionID,
						SemesterID = eds.ExamConduct.SemesterID,
						ExamConductName = eds.ExamConduct.Name,
						ExamType = eds.ExamConduct.ExamType,
						Date = eds.ExamDate.Date,
						SessionName = eds.ExamSession.SessionName,
					}).ToList();
				var examOfferedRoomsSeats = aspireContext.GetExamOfferedRooms(examConductID, roomUsageTypeEnum).Select(r => r.Rows * r.Columns).ToList();
				var roomsCount = examOfferedRoomsSeats.Count;
				var seatsCount = roomsCount > 0 ? examOfferedRoomsSeats.Sum() : 0;
				foreach (var examDateAndSession in examDateAndSessions)
				{
					examDateAndSession.Rooms = roomsCount;
					examDateAndSession.TotalSeats = seatsCount;
					switch (roomUsageTypeEnum)
					{
						case ExamOfferedRoom.RoomUsageTypes.SinglePaper:
							examDateAndSession.RegisteredCourses = aspireContext.GetValidRegisteredCourses(examDateAndSession.ExamConductID, examDateAndSession.ExamDateAndSessionID).Select(rc => rc.StudentID)
								.GroupBy(studentID => studentID).Count(s => s.Count() == 1);
							break;
						case ExamOfferedRoom.RoomUsageTypes.MultiplePapers:
							examDateAndSession.RegisteredCourses = examDateAndSession.RegisteredCourses = aspireContext.GetValidRegisteredCourses(examDateAndSession.ExamConductID, examDateAndSession.ExamDateAndSessionID).Select(rc => rc.StudentID)
								.GroupBy(studentID => studentID).Where(s => s.Count() > 1).Sum(s => (int?)s.Count()) ?? 0;
							break;
						default:
							throw new ArgumentOutOfRangeException(nameof(roomUsageTypeEnum), roomUsageTypeEnum, null);
					}
					examDateAndSession.Seats = aspireContext.GetExamSeats(examDateAndSession.ExamConductID, examDateAndSession.ExamDateAndSessionID, roomUsageTypeEnum).Select(s => new { s.ExamOfferedRoomID, s.Row, s.Column }).Distinct().Count();
				}

				return examDateAndSessions;
			}
		}

		public enum ExecutionOrders
		{
			DepartmentWise,
			ProgramWise,
			Random,
		}

		private class RegisteredCourse
		{
			public int? RegisteredCourseID { get; set; }
			public int? DepartmentID { get; set; }
			public int? ProgramID { get; set; }
			public int? AdmissionOpenProgramID { get; set; }
			public int? OfferedCourseID { get; set; }
			public string OfferedTitle { get; set; }
			public int? CourseID { get; set; }
			public short? SemesterNo { get; set; }
			public int? Section { get; set; }
			public byte? Shift { get; set; }
			public int? StudentID { get; set; }
			public bool IsEmpty { get; set; }

			protected void Clear()
			{
				this.StudentID = null;
				this.RegisteredCourseID = null;
				this.AdmissionOpenProgramID = null;
				this.DepartmentID = null;
				this.ProgramID = null;
				this.AdmissionOpenProgramID = null;
				this.OfferedCourseID = null;
				this.OfferedTitle = null;
				this.CourseID = null;
				this.SemesterNo = null;
				this.Section = null;
				this.Shift = null;
				this.IsEmpty = true;
			}

			public void SetValues(RegisteredCourse registeredCourse)
			{
				if (this.IsEmpty == false)
					throw new InvalidOperationException("Only set when its cleared.");
				this.StudentID = registeredCourse.StudentID.Value;
				this.RegisteredCourseID = registeredCourse.RegisteredCourseID.Value;
				this.AdmissionOpenProgramID = registeredCourse.AdmissionOpenProgramID.Value;
				this.DepartmentID = registeredCourse.DepartmentID;
				this.ProgramID = registeredCourse.ProgramID.Value;
				this.AdmissionOpenProgramID = registeredCourse.AdmissionOpenProgramID.Value;
				this.OfferedCourseID = registeredCourse.OfferedCourseID.Value;
				this.OfferedTitle = registeredCourse.OfferedTitle;
				this.CourseID = registeredCourse.CourseID.Value;
				this.SemesterNo = registeredCourse.SemesterNo.Value;
				this.Section = registeredCourse.Section.Value;
				this.Shift = registeredCourse.Shift.Value;
				this.IsEmpty = false;
			}
		}

		private sealed class Seat : RegisteredCourse
		{
			public Seat()
			{
				base.Clear();
			}

			public int ExamOfferedRoomID { get; set; }
			public byte Row { get; set; }
			public byte Column { get; set; }
		}

		public enum ClearSeatingPlanResult
		{
			NoRecordFound,
			Success
		}

		public static ClearSeatingPlanResult ClearSeatingPlan(int examConductID, ExamOfferedRoom.RoomUsageTypes roomUsageTypeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				aspireContext.Configuration.AutoDetectChangesEnabled = false;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var roomUsageType = (byte)roomUsageTypeEnum;
				var seats = aspireContext.ExamSeats
					.Where(es => es.ExamOfferedRoom.ExamConductID == examConductID && es.ExamOfferedRoom.ExamConduct.InstituteID == loginHistory.InstituteID)
					.Where(es => es.ExamOfferedRoom.RoomUsageType == roomUsageType);
				aspireContext.ExamSeats.RemoveRange(seats);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return ClearSeatingPlanResult.Success;
			}
		}

		public enum ExecuteResult
		{
			NoRecordFound,
			AlreadyGenerated,
			SeatsNotAvailable,
			Success,
		}

		public static Dictionary<int, ExecuteExamDateAndSessionResult> ExecutePlan(int examConductID, ExamOfferedRoom.RoomUsageTypes roomUsageTypeEnum, ExecutionOrders executionOrderEnum, Guid loginSessionGuid)
		{
			List<int> examDateAndSessionIDs;
			using (var aspireContext = new AspireContext())
			{
				examDateAndSessionIDs = aspireContext.ExamDateAndSessions
					.Where(eds => eds.ExamConductID == examConductID)
					.Select(eds => eds.ExamDateAndSessionID).ToList();
			}
			var results = new Dictionary<int, ExecuteExamDateAndSessionResult>();

			Parallel.ForEach(examDateAndSessionIDs, examDateAndSessionID =>
			{
				var result = ExecuteExamDateAndSession(examDateAndSessionID, roomUsageTypeEnum, executionOrderEnum, loginSessionGuid);
				lock (results)
					results.Add(examDateAndSessionID, result);
			});

			return results;
		}

		public enum ExecuteExamDateAndSessionResult
		{
			NoRecordFound,
			NoRegisteredCourseFound,
			AlreadyGenerated,
			SeatsNotAvailable,
			Success,
		}

		public static ExecuteExamDateAndSessionResult ExecuteExamDateAndSession(int examDateAndSessionID, ExamOfferedRoom.RoomUsageTypes roomUsageTypeEnum, ExecutionOrders executionOrderEnum, Guid loginSessionGuid)
		{
			switch (roomUsageTypeEnum)
			{
				case ExamOfferedRoom.RoomUsageTypes.SinglePaper:
					break;
				case ExamOfferedRoom.RoomUsageTypes.MultiplePapers:
					switch (executionOrderEnum)
					{
						case ExecutionOrders.DepartmentWise:
						case ExecutionOrders.ProgramWise:
							throw new ArgumentException("For Multiple Papers execution order must be Random.");
						case ExecutionOrders.Random:
							break;
						default:
							throw new ArgumentOutOfRangeException(nameof(executionOrderEnum), executionOrderEnum, null);
					}
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(roomUsageTypeEnum), roomUsageTypeEnum, null);
			}
			using (var aspireContext = new AspireContext(true))
			{
				aspireContext.IncreaseCommandTimeout();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var examDateAndSession = aspireContext.ExamDateAndSessions
					.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID && eds.ExamConduct.InstituteID == loginHistory.InstituteID)
					.Select(eds => new
					{
						eds.ExamConductID,
					}).SingleOrDefault();
				if (examDateAndSession == null)
					return ExecuteExamDateAndSessionResult.NoRecordFound;
				var roomUsageType = (byte)roomUsageTypeEnum;

				var alreadyExecuted = aspireContext.ExamDateSheets
									.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID)
									.SelectMany(eds => eds.OfferedCours.RegisteredCourses)
									.SelectMany(rc => rc.ExamSeats)
									.Any(es => es.ExamOfferedRoom.ExamConductID == examDateAndSession.ExamConductID && es.ExamOfferedRoom.RoomUsageType == roomUsageType);
				if (alreadyExecuted)
					return ExecuteExamDateAndSessionResult.AlreadyGenerated;

				var allSeats = aspireContext.GetAllExamOfferedRoomSeats(examDateAndSession.ExamConductID, roomUsageType).Select(s => new Seat
				{
					ExamOfferedRoomID = s.ExamOfferedRoomID,
					Row = s.Row,
					Column = s.Column,
				}).ToList();

				var validRegisteredCourses = aspireContext.GetValidRegisteredCourses(examDateAndSession.ExamConductID, examDateAndSessionID).Select(rc => new RegisteredCourse
				{
					RegisteredCourseID = rc.RegisteredCourseID,
					StudentID = rc.StudentID,
					AdmissionOpenProgramID = rc.OfferedCours.Cours.AdmissionOpenProgramID,
					DepartmentID = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.DepartmentID,
					ProgramID = rc.OfferedCours.Cours.AdmissionOpenProgram.ProgramID,
					OfferedCourseID = rc.OfferedCourseID,
					OfferedTitle = rc.OfferedCours.Cours.Title,
					CourseID = rc.OfferedCours.CourseID,
					SemesterNo = rc.OfferedCours.SemesterNo,
					Section = rc.OfferedCours.Section,
					Shift = rc.OfferedCours.Shift,
				}).ToList();

				switch (roomUsageTypeEnum)
				{
					case ExamOfferedRoom.RoomUsageTypes.SinglePaper:
						validRegisteredCourses = validRegisteredCourses.GroupBy(rc => rc.StudentID).Where(rc => rc.Count() == 1).SelectMany(rc => rc.ToList()).ToList();
						var executeExamDateAndSessionSinglePaperResult = aspireContext.ExecuteExamDateAndSessionSinglePaper(validRegisteredCourses, executionOrderEnum, allSeats, loginHistory.UserLoginHistoryID);
						switch (executeExamDateAndSessionSinglePaperResult)
						{
							case ExecuteExamDateAndSessionSinglePaperResults.SeatsNotAvailable:
								aspireContext.RollbackTransaction();
								return ExecuteExamDateAndSessionResult.SeatsNotAvailable;
							case ExecuteExamDateAndSessionSinglePaperResults.Success:
								aspireContext.CommitTransaction();
								return ExecuteExamDateAndSessionResult.Success;
							case ExecuteExamDateAndSessionSinglePaperResults.NoRegisteredCourseFound:
								aspireContext.RollbackTransaction();
								return ExecuteExamDateAndSessionResult.NoRegisteredCourseFound;
							default:
								throw new ArgumentOutOfRangeException();
						}
					case ExamOfferedRoom.RoomUsageTypes.MultiplePapers:
						validRegisteredCourses = validRegisteredCourses.GroupBy(rc => rc.StudentID).Where(rc => rc.Count() > 1).SelectMany(rc => rc.ToList()).ToList();
						var executeExamDateAndSessionMultiplePaperResult = aspireContext.ExecuteExamDateAndSessionMultiplePaper(validRegisteredCourses, allSeats, loginHistory.UserLoginHistoryID);
						switch (executeExamDateAndSessionMultiplePaperResult)
						{
							case ExecuteExamDateAndSessionMultiplePaperResults.SeatsNotAvailable:
								aspireContext.RollbackTransaction();
								return ExecuteExamDateAndSessionResult.SeatsNotAvailable;
							case ExecuteExamDateAndSessionMultiplePaperResults.Success:
								aspireContext.CommitTransaction();
								return ExecuteExamDateAndSessionResult.Success;
							case ExecuteExamDateAndSessionMultiplePaperResults.NoRegisteredCourseFound:
								aspireContext.RollbackTransaction();
								return ExecuteExamDateAndSessionResult.NoRegisteredCourseFound;
							default:
								throw new ArgumentOutOfRangeException();
						}
					default:
						throw new ArgumentOutOfRangeException(nameof(roomUsageTypeEnum), roomUsageTypeEnum, null);
				}
			}
		}

		private enum ExecuteExamDateAndSessionSinglePaperResults
		{
			NoRegisteredCourseFound,
			SeatsNotAvailable,
			Success,
		}

		private static ExecuteExamDateAndSessionSinglePaperResults ExecuteExamDateAndSessionSinglePaper(this AspireContext aspireContext, List<RegisteredCourse> validRegisteredCourses, ExecutionOrders executionOrderEnum, List<Seat> allSeats, int userLoginHistoryID)
		{
			if (!validRegisteredCourses.Any())
				return ExecuteExamDateAndSessionSinglePaperResults.NoRegisteredCourseFound;
			var dictionaryDepartments = new Dictionary<int, Guid>();
			var dictionaryPrograms = new Dictionary<int, Guid>();
			var offeredCourses = validRegisteredCourses.GroupBy(rc => new { rc.OfferedCourseID, rc.DepartmentID, rc.ProgramID })
				.Select(rc =>
				{
					Guid? departmentGuid;
					Guid? programGuid;
					var departmentID = rc.Key.DepartmentID;
					var programID = rc.Key.ProgramID;
					if (departmentID == null)
						departmentGuid = null;
					else if (dictionaryDepartments.ContainsKey(departmentID.Value))
						departmentGuid = dictionaryDepartments[departmentID.Value];
					else
					{
						departmentGuid = Guid.NewGuid();
						dictionaryDepartments.Add(departmentID.Value, departmentGuid.Value);
					}
					if (programID == null)
						programGuid = null;
					else if (dictionaryPrograms.ContainsKey(programID.Value))
						programGuid = dictionaryPrograms[programID.Value];
					else
					{
						programGuid = Guid.NewGuid();
						dictionaryPrograms.Add(programID.Value, programGuid.Value);
					}
					return new
					{
						rc.Key.OfferedCourseID,
						rc.Key.DepartmentID,
						DepartmentGuid = departmentGuid,
						ProgramGuid = programGuid,
						rc.Key.ProgramID,
						RandomGuid = Guid.NewGuid(),
						Students = rc.Count()
					};
				});

			switch (executionOrderEnum)
			{
				case ExecutionOrders.DepartmentWise:
					offeredCourses = offeredCourses.OrderBy(oc => oc.DepartmentGuid).ThenBy(oc => oc.ProgramGuid);
					break;
				case ExecutionOrders.ProgramWise:
					offeredCourses = offeredCourses.OrderBy(oc => oc.ProgramGuid);
					break;
				case ExecutionOrders.Random:
					offeredCourses = offeredCourses.OrderBy(oc => oc.RandomGuid);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(executionOrderEnum), executionOrderEnum, null);
			}

			foreach (var offeredCourse in offeredCourses.ToList())
			{
				var registeredCourses = validRegisteredCourses.Where(rc => rc.OfferedCourseID == offeredCourse.OfferedCourseID).OrderBy(rc => Guid.NewGuid()).ToList();
				var result = registeredCourses.SeatOfferedCourseStudentsSinglePaper(allSeats);
				switch (result)
				{
					case SeatOfferedCourseStudentsSinglePaperResults.SeatsNotAvailable:
						return ExecuteExamDateAndSessionSinglePaperResults.SeatsNotAvailable;
					case SeatOfferedCourseStudentsSinglePaperResults.Success:
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}

			var allocatedSeats = allSeats.FindAll(s => !s.IsEmpty);
			aspireContext.ExamSeats.AddRange(allocatedSeats.Select(s => new ExamSeat
			{
				RegisteredCourseID = s.RegisteredCourseID.Value,
				ExamOfferedRoomID = s.ExamOfferedRoomID,
				Row = s.Row,
				Column = s.Column,
			}));
			aspireContext.SaveChanges(userLoginHistoryID);
			return ExecuteExamDateAndSessionSinglePaperResults.Success;
		}

		private static bool ValidateSeat(this Seat seat, RegisteredCourse registeredCourse, List<Seat> allSeats)
		{
			var selectedSeats = allSeats.Where(s => s.IsEmpty == false)
				.Where(s => s.ExamOfferedRoomID == seat.ExamOfferedRoomID)
				.Where(s => s.Column == seat.Column - 1 || s.Column == seat.Column + 1);
			return !selectedSeats.Any(s =>
			s.OfferedCourseID == registeredCourse.OfferedCourseID
			|| s.CourseID == registeredCourse.CourseID
			|| (s.ProgramID == registeredCourse.ProgramID && s.SemesterNo == registeredCourse.SemesterNo)
			|| s.OfferedTitle == registeredCourse.OfferedTitle);
		}

		private enum SeatOfferedCourseStudentsSinglePaperResults
		{
			SeatsNotAvailable,
			Success
		}

		private sealed class RoomColumn
		{
			public int ExamOfferedRoomID { get; set; }
			public byte Column { get; set; }
		}

		private static SeatOfferedCourseStudentsSinglePaperResults SeatOfferedCourseStudentsSinglePaper(this List<RegisteredCourse> registeredCourses, List<Seat> allSeats)
		{
			var freeSeats = allSeats.Where(s => s.IsEmpty).ToList();
			var freeSeatEnumerator = freeSeats.GetEnumerator();
			var validColumns = new List<RoomColumn>();
			var invalidColumns = new List<RoomColumn>();
			foreach (var registeredCourse in registeredCourses)
			{
				var seated = false;
				while (freeSeatEnumerator.MoveNext())
				{
					var seat = freeSeatEnumerator.Current;
					if (seat == null)
						throw new InvalidOperationException();
					if (validColumns.Any(c => c.ExamOfferedRoomID == seat.ExamOfferedRoomID && c.Column == seat.Column))
					{
						seat.SetValues(registeredCourse);
						seated = true;
						break;
					}
					else if (invalidColumns.Any(c => c.ExamOfferedRoomID == seat.ExamOfferedRoomID && c.Column == seat.Column))
						continue;
					else
					{
						var validationResult = seat.ValidateSeat(registeredCourse, allSeats);
						if (validationResult)
						{
							seat.SetValues(registeredCourse);
							validColumns.Add(new RoomColumn { Column = seat.Column, ExamOfferedRoomID = seat.ExamOfferedRoomID });
							seated = true;
							break;
						}
						else
							invalidColumns.Add(new RoomColumn { Column = seat.Column, ExamOfferedRoomID = seat.ExamOfferedRoomID });
					}
				}
				if (seated == false)
					return SeatOfferedCourseStudentsSinglePaperResults.SeatsNotAvailable;
			}
			return SeatOfferedCourseStudentsSinglePaperResults.Success;
		}

		private enum ExecuteExamDateAndSessionMultiplePaperResults
		{
			NoRegisteredCourseFound,
			SeatsNotAvailable,
			Success
		}

		private static ExecuteExamDateAndSessionMultiplePaperResults ExecuteExamDateAndSessionMultiplePaper(this AspireContext aspireContext, List<RegisteredCourse> validRegisteredCourses, List<Seat> allSeats, int userLoginHistoryID)
		{
			if (!validRegisteredCourses.Any())
				return ExecuteExamDateAndSessionMultiplePaperResults.NoRegisteredCourseFound;
			var students = validRegisteredCourses.GroupBy(rc => rc.StudentID).Select(rc => new
			{
				StudentID = rc.Key,
				Papers = rc.Count(),
			}).OrderByDescending(s => s.Papers);

			var freeSeats = allSeats.Where(s => s.IsEmpty).ToList();

			foreach (var student in students.ToList())
			{
				var registeredCourses = validRegisteredCourses.FindAll(rc => rc.StudentID == student.StudentID);
				var freeSeat = freeSeats.FirstOrDefault();
				if (freeSeat == null)
					return ExecuteExamDateAndSessionMultiplePaperResults.SeatsNotAvailable;
				aspireContext.ExamSeats.AddRange(registeredCourses.Select(rc => new ExamSeat
				{
					RegisteredCourseID = rc.RegisteredCourseID.Value,
					ExamOfferedRoomID = freeSeat.ExamOfferedRoomID,
					Row = freeSeat.Row,
					Column = freeSeat.Column,
				}));
				freeSeats.Remove(freeSeat);
			}
			aspireContext.SaveChanges(userLoginHistoryID);
			return ExecuteExamDateAndSessionMultiplePaperResults.Success;
		}

		public enum ValidatePlanStatuses
		{
			SameSeatAssignedToMultipleStudents,
			MultipleSeatsAssignedToSameStudent,
			SingleTypeRoomAssignedToStudentHavingClashes,
			MultipleTypeRoomAssignedToStudentHavingNoClash,
		}

		public static Dictionary<int, Dictionary<ValidatePlanStatuses, int>> ValidatePlan(int examConductID, out List<string> validationIssues, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var results = new Dictionary<int, Dictionary<ValidatePlanStatuses, int>>();
				var examDateSheets = aspireContext.ExamDateAndSessions
						.Where(eds => eds.ExamConductID == examConductID && eds.ExamConduct.InstituteID == loginHistory.InstituteID)
						.SelectMany(eds => eds.ExamDateSheets)
						.Select(eds => new
						{
							eds.ExamDateAndSessionID,
							eds.OfferedCourseID,
							eds.OfferedCours.Cours.Title,
						}).ToList();

				var registeredCourses = aspireContext.RegisteredCourses.FilterValidRegisteredCoursesForSeatingPlan();
				var examSeats = (from es in aspireContext.ExamSeats
								 join rc in registeredCourses on es.RegisteredCourseID equals rc.RegisteredCourseID
								 where es.ExamOfferedRoom.ExamConductID == examConductID
								 select new
								 {
									 es.ExamOfferedRoomID,
									 es.ExamOfferedRoom.RoomUsageType,
									 es.Row,
									 es.Column,
									 es.RegisteredCours.OfferedCourseID,
									 es.RegisteredCourseID,
									 es.RegisteredCours.StudentID,
									 es.RegisteredCours.Student.Enrollment,
								 }).ToList();

				var seats = (from eds in examDateSheets
							 join es in examSeats on eds.OfferedCourseID equals es.OfferedCourseID
							 select new
							 {
								 eds.OfferedCourseID,
								 eds.ExamDateAndSessionID,
								 es.ExamOfferedRoomID,
								 RoomUsageTypeEnum = (ExamOfferedRoom.RoomUsageTypes)es.RoomUsageType,
								 es.Row,
								 es.Column,
								 es.RegisteredCourseID,
								 es.StudentID,
								 eds.Title,
								 es.Enrollment
							 }).ToList();

				validationIssues = new List<string>();
				var examDateAndSessionIDs = examDateSheets.Select(es => es.ExamDateAndSessionID).Distinct().ToList();
				foreach (var examDateAndSessionID in examDateAndSessionIDs)
				{
					var result = new Dictionary<ValidatePlanStatuses, int>();
					results.Add(examDateAndSessionID, result);

					var selectedSeats = seats.Where(es => es.ExamDateAndSessionID == examDateAndSessionID).ToList();
					var sameSeatAssignedToMultipleStudents = selectedSeats.GroupBy(s => new
					{
						s.ExamOfferedRoomID,
						s.Row,
						s.Column
					}).Select(g => new
					{
						g.Key,
						Students = g.Select(s => s.StudentID).Distinct().Count(),
						Courses = g.Select(s => new
						{
							s.Enrollment,
							s.Title
						})
					}).Where(s => s.Students > 1).ToList();
					if (sameSeatAssignedToMultipleStudents.Count > 0)
					{
						result.Add(ValidatePlanStatuses.SameSeatAssignedToMultipleStudents, sameSeatAssignedToMultipleStudents.Count);
						foreach (var sameSeatAssignedToMultipleStudent in sameSeatAssignedToMultipleStudents)
							foreach (var course in sameSeatAssignedToMultipleStudent.Courses)
								validationIssues.Add($"{ValidatePlanStatuses.SameSeatAssignedToMultipleStudents}	{course.Enrollment}	{course.Title}");
					}

					var multipleSeatsAssignedToSameStudent = selectedSeats.GroupBy(s => s.StudentID)
						.Select(g => new
						{
							g.Key,
							Seats = g.Select(s => new
							{
								s.ExamOfferedRoomID,
								s.Row,
								s.Column
							}).Distinct().Count(),
							g.First().Enrollment,
						}).Where(s => s.Seats > 1).ToList();
					if (multipleSeatsAssignedToSameStudent.Count > 0)
					{
						result.Add(ValidatePlanStatuses.MultipleSeatsAssignedToSameStudent, multipleSeatsAssignedToSameStudent.Count);
						validationIssues.AddRange(multipleSeatsAssignedToSameStudent.Select(s => $"{ValidatePlanStatuses.MultipleSeatsAssignedToSameStudent}	{s.Enrollment}"));
					}

					var query = selectedSeats.GroupBy(s => s.StudentID)
						.Select(g => new
						{
							Seats = g.Count(),
							RoomUsageTypeEnums = g.Select(s => s.RoomUsageTypeEnum),
							g.First().Enrollment,
						}).ToList();

					var singleTypeRoomAssignedToStudentHavingClashes = query.Where(s => s.Seats > 1 && s.RoomUsageTypeEnums.Contains(ExamOfferedRoom.RoomUsageTypes.SinglePaper)).ToList();
					if (singleTypeRoomAssignedToStudentHavingClashes.Count > 0)
					{
						result.Add(ValidatePlanStatuses.SingleTypeRoomAssignedToStudentHavingClashes, singleTypeRoomAssignedToStudentHavingClashes.Count);
						validationIssues.AddRange(singleTypeRoomAssignedToStudentHavingClashes.Select(s => $"{ValidatePlanStatuses.SingleTypeRoomAssignedToStudentHavingClashes}	{s.Enrollment}"));
					}

					var multipleTypeRoomAssignedToStudentHavingNoClash = query.Where(s => s.Seats == 1 && s.RoomUsageTypeEnums.Contains(ExamOfferedRoom.RoomUsageTypes.MultiplePapers)).ToList();

					if (multipleTypeRoomAssignedToStudentHavingNoClash.Count > 0)
					{
						result.Add(ValidatePlanStatuses.MultipleTypeRoomAssignedToStudentHavingNoClash, multipleTypeRoomAssignedToStudentHavingNoClash.Count);
						validationIssues.AddRange(multipleTypeRoomAssignedToStudentHavingNoClash.Select(s => $"{ValidatePlanStatuses.MultipleTypeRoomAssignedToStudentHavingNoClash}	{s.Enrollment}"));
					}
				}

				return results;
			}
		}

		public sealed class IndividualSeatResult
		{
			internal IndividualSeatResult() { }
			public Statuses Status { get; internal set; }
			public string Enrollment { get; internal set; }
			public string OfferedTitle { get; internal set; }
			public int? OfferedCourseID { get; internal set; }
			public enum Statuses
			{
				Success,
				NoRecordFound,
				SeatAlreadyAllocated,
				SeatCannotBeAllocatedInTheSelectedRoom,
				FreeSeatsNotFound
			}

			public IndividualSeatResult SetStatus(Statuses status)
			{
				this.Status = status;
				return this;
			}
		}

		public static IndividualSeatResult IndividualSeat(int registeredCourseID, int examOfferedRoomID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				aspireContext.IncreaseCommandTimeout();
				var loginHistory = aspireContext.DemandPermissionsIndividualSeating(loginSessionGuid);
				var registeredCourse = aspireContext.RegisteredCourses
					.FilterValidRegisteredCoursesForSeatingPlan()
					.Where(rc => rc.RegisteredCourseID == registeredCourseID && rc.DeletedDate == null)
					.Select(rc => new
					{
						rc.OfferedCourseID,
						rc.OfferedCours.CourseID,
						rc.Student.Enrollment,
						OfferedTitle = rc.OfferedCours.Cours.Title,
						rc.OfferedCours.Cours.AdmissionOpenProgram.ProgramID,
						rc.OfferedCours.SemesterNo,
						rc.StudentID,
					}).SingleOrDefault();
				var result = new IndividualSeatResult();

				if (registeredCourse == null)
					return result.SetStatus(IndividualSeatResult.Statuses.NoRecordFound);

				result.Enrollment = registeredCourse.Enrollment;
				result.OfferedTitle = registeredCourse.OfferedTitle;
				result.OfferedCourseID = registeredCourse.OfferedCourseID;

				var examOfferedRoom = aspireContext.ExamOfferedRooms
					.Where(eor => eor.ExamOfferedRoomID == examOfferedRoomID && eor.ExamConduct.InstituteID == loginHistory.InstituteID)
					.Select(eor => new
					{
						eor.ExamConductID,
						eor.RoomUsageType,
					}).SingleOrDefault();
				if (examOfferedRoom == null)
					return result.SetStatus(IndividualSeatResult.Statuses.NoRecordFound);

				if (aspireContext.ExamSeats.Any(es => es.RegisteredCourseID == registeredCourseID && es.ExamOfferedRoom.ExamConductID == examOfferedRoom.ExamConductID))
					return result.SetStatus(IndividualSeatResult.Statuses.SeatAlreadyAllocated);

				var examDateSheet = aspireContext.ExamDateSheets
					 .Where(eds => eds.OfferedCourseID == registeredCourse.OfferedCourseID && eds.ExamDateAndSession.ExamConductID == examOfferedRoom.ExamConductID)
					 .Select(eds => new
					 {
						 eds.ExamDateAndSessionID
					 }).SingleOrDefault();
				if (examDateSheet == null)
					return result.SetStatus(IndividualSeatResult.Statuses.NoRecordFound);

				var registeredCourseIDs = aspireContext
					.GetValidRegisteredCourses(examOfferedRoom.ExamConductID, examDateSheet.ExamDateAndSessionID)
					.FilterValidRegisteredCoursesForSeatingPlan()
					.Where(rc => rc.StudentID == registeredCourse.StudentID).Select(rc => rc.RegisteredCourseID).ToList();

				var examSeatsAllocatedBefore = aspireContext.ExamSeats
					.Where(es => es.ExamOfferedRoom.ExamConductID == examOfferedRoom.ExamConductID && registeredCourseIDs.Contains(es.RegisteredCourseID)).ToList();

				if (examSeatsAllocatedBefore.Any())
				{
					aspireContext.ExamSeats.RemoveRange(examSeatsAllocatedBefore);
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				}

				var roomOfferedSeats = aspireContext.GetAllExamOfferedRoomSeats(examOfferedRoom.ExamConductID, examOfferedRoom.RoomUsageType)
					.Where(s => s.ExamOfferedRoomID == examOfferedRoomID).Select(s => new
					{
						s.Row,
						s.Column
					}).ToList();

				var allocatedSeats = aspireContext.GetExamSeats(examOfferedRoom.ExamConductID, examDateSheet.ExamDateAndSessionID, (ExamOfferedRoom.RoomUsageTypes?)examOfferedRoom.RoomUsageType)
									.Where(es => es.ExamOfferedRoomID == examOfferedRoomID).Select(s => new { s.Row, s.Column }).ToList();

				var freeSeats = roomOfferedSeats.Except(allocatedSeats).OrderBy(fs => fs.Column).ThenBy(fs => fs.Row).GroupBy(fs => fs.Column).Select(fs => fs.First()).ToList();
				var roomUsageTypeEnum = (ExamOfferedRoom.RoomUsageTypes)examOfferedRoom.RoomUsageType;
				switch (roomUsageTypeEnum)
				{
					case ExamOfferedRoom.RoomUsageTypes.SinglePaper:
						{
							var examDateSheets = aspireContext.GetExamDateSheets(examOfferedRoom.ExamConductID, examDateSheet.ExamDateAndSessionID);
							var examSeats = aspireContext.ExamSeats.Where(es => es.ExamOfferedRoomID == examOfferedRoomID);
							var allocatedSeatsInRoom = (from eds in examDateSheets
														join es in examSeats on eds.OfferedCourseID equals es.RegisteredCours.OfferedCourseID
														select new
														{
															es.ExamOfferedRoomID,
															es.Column,
															eds.OfferedCourseID,
															OfferedTitle = eds.OfferedCours.Cours.Title,
															eds.OfferedCours.CourseID,
															eds.OfferedCours.Cours.AdmissionOpenProgram.ProgramID,
															eds.OfferedCours.SemesterNo,
														}).Distinct().ToList();

							foreach (var freeSeat in freeSeats)
							{
								var seats = allocatedSeatsInRoom.Where(s => s.Column == freeSeat.Column - 1 || s.Column == freeSeat.Column + 1);

								var leftRightExists = seats.Any(s => s.OfferedCourseID == registeredCourse.OfferedCourseID
								|| s.CourseID == registeredCourse.CourseID
								|| (s.ProgramID == registeredCourse.ProgramID && s.SemesterNo == registeredCourse.SemesterNo)
								|| s.OfferedTitle == registeredCourse.OfferedTitle);

								if (leftRightExists)
									continue;
								aspireContext.ExamSeats.Add(new ExamSeat
								{
									RegisteredCourseID = registeredCourseID,
									ExamOfferedRoomID = examOfferedRoomID,
									Row = freeSeat.Row,
									Column = freeSeat.Column,
								});
								aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
								return result.SetStatus(IndividualSeatResult.Statuses.Success);
							}
							return result.SetStatus(IndividualSeatResult.Statuses.SeatCannotBeAllocatedInTheSelectedRoom);
						}
					case ExamOfferedRoom.RoomUsageTypes.MultiplePapers:
						{
							var freeSeat = freeSeats.FirstOrDefault();
							if (freeSeat == null)
								return result.SetStatus(IndividualSeatResult.Statuses.FreeSeatsNotFound);
							aspireContext.ExamSeats.AddRange(registeredCourseIDs.Select(rcID => new ExamSeat
							{
								RegisteredCourseID = rcID,
								ExamOfferedRoomID = examOfferedRoomID,
								Row = freeSeat.Row,
								Column = freeSeat.Column,
							}).ToList());
							aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
							return result.SetStatus(IndividualSeatResult.Statuses.Success);
						}
					default:
						throw new NotImplementedEnumException(roomUsageTypeEnum);
				}
			}
		}

		public enum DeleteExamSeatStatuses
		{
			NoRecordFound,
			Success
		}

		internal static DeleteExamSeatStatuses DeleteExamSeat(this AspireContext aspireContext, int examSeatID, Core.Common.Permissions.Staff.StaffGroupPermission loginHistory)
		{
			var examSeat = aspireContext.ExamSeats
				.Where(es => es.ExamSeatID == examSeatID && es.ExamOfferedRoom.ExamConduct.InstituteID == loginHistory.InstituteID)
				.Select(es => new
				{
					es.RegisteredCourseID,
					es.RegisteredCours.StudentID,
					es.RegisteredCours.OfferedCourseID,
					es.ExamOfferedRoom.ExamConductID,
				}).SingleOrDefault();
			if (examSeat == null)
				return DeleteExamSeatStatuses.NoRecordFound;

			var examDateAndSessionID = aspireContext.ExamDateSheets
				.Where(eds => eds.OfferedCourseID == examSeat.OfferedCourseID && eds.ExamDateAndSession.ExamConductID == examSeat.ExamConductID)
				.Select(eds => (int?)eds.ExamDateAndSessionID)
				.SingleOrDefault();
			if (examDateAndSessionID == null)
				throw new InvalidOperationException("Seat exists without date sheet???");

			var allocatedExamSeats = aspireContext.ExamDateSheets
				.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID.Value)
				.SelectMany(eds => eds.OfferedCours.RegisteredCourses)
				.Where(rc => rc.StudentID == examSeat.StudentID)
				.SelectMany(rc => rc.ExamSeats)
				.Where(es => es.ExamOfferedRoom.ExamConductID == examSeat.ExamConductID)
				.ToList();

			aspireContext.ExamSeats.RemoveRange(allocatedExamSeats);
			aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
			return DeleteExamSeatStatuses.Success;
		}

		public enum DeleteExamSeatsForOfferedCourseIDStatuses
		{
			NoRecordFound,
			Success,
		}

		public static DeleteExamSeatsForOfferedCourseIDStatuses DeleteExamSeatsForOfferedCourseID(int offeredCourseID, int examConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var examDateSheet = aspireContext.ExamDateSheets
					.Where(eds => eds.OfferedCourseID == offeredCourseID && eds.ExamDateAndSession.ExamConductID == examConductID && eds.ExamDateAndSession.ExamConduct.InstituteID == loginHistory.InstituteID)
					.Select(eds => new
					{
						eds.OfferedCourseID,
					}).SingleOrDefault();
				if (examDateSheet == null)
					return DeleteExamSeatsForOfferedCourseIDStatuses.NoRecordFound;

				var examSeats = aspireContext.ExamSeats
					.Where(es => es.RegisteredCours.OfferedCourseID == examDateSheet.OfferedCourseID && es.ExamOfferedRoom.ExamConductID == examConductID)
					.Select(es => new
					{
						es.ExamSeatID,
					}).ToList();

				foreach (var examSeat in examSeats)
				{
					var status = aspireContext.DeleteExamSeat(examSeat.ExamSeatID, loginHistory);
					switch (status)
					{
						case DeleteExamSeatStatuses.NoRecordFound:
						//aspireContext.RollbackTransaction();
						//return DeleteExamSeatsForOfferedCourseIDStatuses.NoRecordFound;
						case DeleteExamSeatStatuses.Success:
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
				aspireContext.CommitTransaction();
				return DeleteExamSeatsForOfferedCourseIDStatuses.Success;
			}
		}
	}
}