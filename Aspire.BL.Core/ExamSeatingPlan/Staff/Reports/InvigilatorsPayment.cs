﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
	public static class InvigilatorsPayment
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class InvigilatorsPaymentPageHeader
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public byte ExamType { get; internal set; }
			public string Title => $"Invigilators Payment ({this.SemesterID.ToSemesterString()} {((ExamConduct.ExamTypes)this.ExamType).ToFullName()} Exam)";
			public static List<InvigilatorsPaymentPageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class InvigilatorsPaymentDetail
		{
			public string AccountNo { get; internal set; }
			public string CNIC { get; internal set; }
			public string Designation { get; internal set; }
			public int Duties { get; internal set; }
			public int ExamInvigilatorID { get; internal set; }
			public int ExamSessionID { get; internal set; }
			public short InvigilationPayRate { get; internal set; }
			public string Name { get; internal set; }
			public byte Sequence { get; internal set; }
			public string SessionName { get; internal set; }
			public byte Shift { get; internal set; }
			public int Payment => this.Duties * this.InvigilationPayRate;

			public static List<InvigilatorsPaymentDetail> GetList()
			{
				return null;
			}
		}


		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public InvigilatorsPaymentPageHeader PageHeader { get; internal set; }
			public List<InvigilatorsPaymentDetail> PaymentDetails { get; internal set; }
		}

		public static ReportDataSet GetInvigilatorsPayment(int examConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var pageHeader = aspireContext.ExamConducts.Where(ec => ec.ExamConductID == examConductID && ec.InstituteID == loginHistory.InstituteID).Select(ec => new InvigilatorsPaymentPageHeader
				{
					Institute = ec.Institute.InstituteName,
					ExamType = ec.ExamType,
					SemesterID = ec.SemesterID,
				}).SingleOrDefault();

				var details = aspireContext.ExamInvigilatorDuties.Where(eid => eid.ExamDateAndSession.ExamConductID == examConductID && eid.AttendanceStatus == ExamInvigilatorDuty.AttendanceStatusPresent).GroupBy(eid => new
				{
					eid.ExamInvigilatorID,
					eid.ExamInvigilator.Name,
					eid.ExamInvigilator.ExamDesignation.Designation,
					eid.ExamInvigilator.AccountNo,
					eid.ExamInvigilator.CNIC,
					eid.ExamDateAndSession.ExamSessionID,
					eid.ExamDateAndSession.ExamSession.Shift,
					eid.ExamDateAndSession.ExamSession.SessionName,
					eid.ExamDateAndSession.ExamSession.InvigilationPayRate,
					eid.ExamDateAndSession.ExamSession.Sequence,
				}).Select(eid => new InvigilatorsPaymentDetail
				{
					ExamInvigilatorID = eid.Key.ExamInvigilatorID,
					Name = eid.Key.Name,
					Designation = eid.Key.Designation,
					AccountNo = eid.Key.AccountNo,
					CNIC = eid.Key.CNIC,
					ExamSessionID = eid.Key.ExamSessionID,
					Shift = eid.Key.Shift,
					SessionName = eid.Key.SessionName,
					InvigilationPayRate = eid.Key.InvigilationPayRate,
					Sequence = eid.Key.Sequence,
					Duties = eid.Count(),
				});
				return new ReportDataSet { PageHeader = pageHeader, PaymentDetails = details.ToList() };
			}
		}
	}
}
