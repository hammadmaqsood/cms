﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
	public static class ExaminerEnvelope
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class ExaminerEnvelopeDetail
		{
			public int OfferedCourseID { get; internal set; }
			public string TeacherName { get; internal set; }
			public string CourseTitle { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public string SessionName { get; internal set; }
			public DateTime Date { get; internal set; }
			public string RoomName { get; internal set; }
			public int Sequence { get; internal set; }
			public byte ExamType { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Session => $"{this.SessionName}\n{this.Date:d}\n{this.SemesterID.ToSemesterString()} {((ExamConduct.ExamTypes)this.ExamType).ToFullName()}";

			public static List<ExaminerEnvelopeDetail> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public List<ExaminerEnvelopeDetail> ExaminerEnvelopeDetails { get; set; }
		}

		public static ReportDataSet GetExaminerEnvelope(int examDateAndSessionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var pageHeader = aspireContext.ExamDateAndSessions.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID && eds.ExamConduct.InstituteID == loginHistory.InstituteID).Select(eds => new
				{
					eds.ExamDateAndSessionID,
					eds.ExamConductID,
					eds.ExamConduct.ExamType,
					eds.ExamDate.Date,
					eds.ExamSession.SessionName,
					eds.ExamConduct.SemesterID,
				}).SingleOrDefault();
				if (pageHeader == null)
					return null;

				var examinerEnvelopeDetails = aspireContext.GetValidExamSeats(pageHeader.ExamConductID, examDateAndSessionID, null).Select(es => new ExaminerEnvelopeDetail
				{
					OfferedCourseID = es.RegisteredCours.OfferedCourseID,
					CourseTitle = es.RegisteredCours.OfferedCours.Cours.Title,
					Date = pageHeader.Date,
					ProgramAlias = es.RegisteredCours.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					RoomName = es.ExamOfferedRoom.ExamRoom.RoomName,
					SemesterNo = es.RegisteredCours.OfferedCours.SemesterNo,
					Section = es.RegisteredCours.OfferedCours.Section,
					Shift = es.RegisteredCours.OfferedCours.Shift,
					Sequence = es.ExamOfferedRoom.Sequence,
					TeacherName = es.RegisteredCours.OfferedCours.FacultyMember.Name,
					SessionName = pageHeader.SessionName,
					SemesterID = pageHeader.SemesterID,
					ExamType = pageHeader.ExamType,
				}).Distinct().ToList();

				return new ReportDataSet { ExaminerEnvelopeDetails = examinerEnvelopeDetails };
			}
		}
	}
}
