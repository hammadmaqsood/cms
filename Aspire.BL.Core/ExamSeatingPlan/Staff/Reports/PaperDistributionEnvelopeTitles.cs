﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
	public static class PaperDistributionEnvelopeTitles
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}
		public class EnvelopeTitlesDataset
		{
			public int RoomID { get; internal set; }
			public string RoomName { get; internal set; }
			public int Sequence { get; internal set; }
			public int Students { get; internal set; }
			public int OfferedCourseID { get; internal set; }
			public string SessionName { get; internal set; }
			public DateTime ExamDate { get; internal set; }
			public string CourseTitle { get; internal set; }
			public string Major { get; internal set; }
			public string ProgramName { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramName, this.SemesterNo, this.Section, this.Shift);
			public string TeacherName { get; internal set; }

			public List<EnvelopeTitlesDataset> GetEnvelopeDataset() { return null; }
		}

		public class EnvelopeTitlePageHeader
		{
			public string InstituteName { get; internal set; }
			public string ReportTitle { get; internal set; }
			public List<EnvelopeTitlePageHeader> GetEnvelopeTitlesPageHeader()
			{
				return null;
			}
		}

		public class EnvelopeTitlesReportDataset
		{
			public List<EnvelopeTitlesDataset> EnvelopeTitlesDataset { get; internal set; }
			public EnvelopeTitlePageHeader EnvelopeTitlePageHeader { get; internal set; }
		}

		public static EnvelopeTitlesReportDataset GetEnvelopeTitles(int examDateAndSessionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var pageHeader = new EnvelopeTitlePageHeader
				{
					InstituteName = aspireContext.Institutes.Where(ins => ins.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).SingleOrDefault(),
					ReportTitle = "Paper Distribution Envelope Titles",
				};

				var examDateSheets = aspireContext.ExamDateSheets
					.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID)
					.Where(eds => eds.ExamDateAndSession.ExamConduct.InstituteID == loginHistory.InstituteID)
					.Select(eds => new
					{
						eds.OfferedCourseID,
						eds.ExamDateAndSession.ExamConductID,
						eds.ExamDateAndSession.ExamSession.SessionName,
						eds.ExamDateAndSession.ExamDate.Date,
						eds.OfferedCours.Cours.Title,
						eds.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						eds.OfferedCours.SemesterNo,
						eds.OfferedCours.Section,
						eds.OfferedCours.Shift,
						eds.OfferedCours.Cours.ProgramMajor.Majors,
						Teacher = eds.OfferedCours.FacultyMember.Name,
					});

				var examSeats = aspireContext.ExamSeats.Where(es => es.RegisteredCours.DeletedDate == null)
					.Select(es => new
					{
						es.RegisteredCours.OfferedCourseID,
						es.ExamOfferedRoom.ExamConductID,
						es.ExamOfferedRoomID,
						es.ExamOfferedRoom.ExamRoom.RoomName,
						es.ExamOfferedRoom.Sequence,
					});

				var dataSet = from es in examSeats
							  join eds in examDateSheets on new { es.OfferedCourseID, es.ExamConductID } equals new { eds.OfferedCourseID, eds.ExamConductID }
							  select new EnvelopeTitlesDataset
							  {
								  OfferedCourseID = eds.OfferedCourseID,
								  CourseTitle = eds.Title,
								  ProgramName = eds.ProgramAlias,
								  SemesterNo = eds.SemesterNo,
								  Section = eds.Section,
								  Shift = eds.Shift,
								  TeacherName = eds.Teacher,
								  RoomName = es.RoomName,
								  Sequence = es.Sequence,
								  RoomID = es.ExamOfferedRoomID,
								  Major = eds.Majors,
								  ExamDate = eds.Date,
								  SessionName = eds.SessionName
							  };
				return new EnvelopeTitlesReportDataset { EnvelopeTitlesDataset = dataSet.Distinct().ToList(), EnvelopeTitlePageHeader = pageHeader };
			}
		}
	}
}

