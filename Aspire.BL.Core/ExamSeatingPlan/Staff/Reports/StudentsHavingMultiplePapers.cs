﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
	public static class StudentsHavingMultiplePapers
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class StudentsHavingMultiplePapersPageHeader
		{
			public int ExamDateAndSessionID { get; internal set; }
			public int ExamConductID { get; internal set; }
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public byte ExamType { get; internal set; }
			public TimeSpan StartTime { get; internal set; }
			public byte TotalTimeInMinutes { get; internal set; }
			public string Timing => $"{this.StartTime.ToTime12Format()} To {this.StartTime.Add(TimeSpan.FromMinutes(this.TotalTimeInMinutes)).ToTime12Format()}";
			public DateTime Date { get; internal set; }
			public string SessionName { get; internal set; }
			public string Title => $"Students Having Multiple Papers ({this.SemesterID.ToSemesterString()} {((ExamConduct.ExamTypes)this.ExamType).ToFullName()} Exam), Date: {this.Date:D} Timing: {this.Timing}";

			public List<StudentsHavingMultiplePapersPageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class StudentsHavingMultiplePapersStudentDetail
		{
			public int OfferedCourseID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string StudentName { get; internal set; }
			public string CourseTitle { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public string DepartmentName { get; internal set; }
			public string Program { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift);

			public List<StudentsHavingMultiplePapersStudentDetail> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			public StudentsHavingMultiplePapersPageHeader PageHeader { get; internal set; }
			public List<StudentsHavingMultiplePapersStudentDetail> StudentDetails { get; internal set; }
		}

		public static ReportDataSet GetData(int examDateAndSessionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var pageHeader = aspireContext.ExamDateAndSessions.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID && eds.ExamConduct.InstituteID == loginHistory.InstituteID).Select(eds => new StudentsHavingMultiplePapersPageHeader
				{
					ExamDateAndSessionID = eds.ExamDateAndSessionID,
					ExamConductID = eds.ExamConductID,
					SemesterID = eds.ExamConduct.SemesterID,
					Institute = eds.ExamConduct.Institute.InstituteName,
					ExamType = eds.ExamConduct.ExamType,
					Date = eds.ExamDate.Date,
					StartTime = eds.ExamSession.StartTime,
					SessionName = eds.ExamSession.SessionName,
					TotalTimeInMinutes = eds.ExamSession.TotalTimeInMinutes,
				}).SingleOrDefault();
				if (pageHeader == null)
					return null;

				var multiPaperStudentIDs = aspireContext.GetValidRegisteredCourses(pageHeader.ExamConductID, pageHeader.ExamDateAndSessionID)
					.GroupBy(rc => rc.StudentID).Where(g => g.Count() > 1).Select(g => g.Key);
				var validRegisteredCourses = aspireContext.GetValidRegisteredCourses(pageHeader.ExamConductID, pageHeader.ExamDateAndSessionID)
					.Where(rc => multiPaperStudentIDs.Contains(rc.StudentID))
					.Select(rc => new StudentsHavingMultiplePapersStudentDetail
					{
						OfferedCourseID = rc.OfferedCourseID,
						Enrollment = rc.Student.Enrollment,
						StudentName = rc.Student.Name,
						CourseTitle = rc.OfferedCours.Cours.Title,
						DepartmentID = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.DepartmentID,
						DepartmentName = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.Department.DepartmentName,
						Program = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						SemesterNo = rc.OfferedCours.SemesterNo,
						Section = rc.OfferedCours.Section,
						Shift = rc.OfferedCours.Shift,
					}).ToList();

				return new ReportDataSet
				{
					PageHeader = pageHeader,
					StudentDetails = validRegisteredCourses
				};
			}
		}
	}
}
