﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
    public static class PapersList
    {
        private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
        {
            return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
        }
        public class PapersListPageHeader
        {
            public string InstituteName { get;internal set; }
            public short SemesterID { get; internal set; }
            public byte ExamType { get; internal set; }
            public DateTime ExamDate { get;internal set; }
            public string SessionName { get;internal set; }
            public TimeSpan StartTime { get;internal set; }
            public int TotalTimeInMinutes { get;internal set; }
            public string TimeRange => $" {this.StartTime} To {this.StartTime.Add(TimeSpan.FromMinutes(this.TotalTimeInMinutes))}";
            public string ReportTitle => $"Invigilator's Duties ({this.SemesterID.ToSemesterString()} {((ExamConduct.ExamTypes)this.ExamType).ToFullName()} Exam), Timing: {this.TimeRange}".ToUpper();
            public List<PapersListPageHeader> GetPapersListPageHeader() { return null; }
        }

        public class PapersListDataset
        {
            public int RoomID { get;internal set; }
            public string RoomName { get;internal set; }
            public int OfferedCourseID { get;internal set; }
            public string CourseTitle { get;internal set; }
            public string TeacherName { get;internal set; }
            public int Sequence { get;internal set; }
            public int Students { get;internal set; }
            public string ProgramName { get;internal set; }
            public short SemesterNo { get;internal set; }
            public int Section { get;internal set; }
            public byte Shift { get;internal set; }
            public string Class => AspireFormats.GetClassName(this.ProgramName, this.SemesterNo, this.Section, this.Shift);
            public DateTime ExamDate { get;internal set; }
            public string SessionName { get;internal set; }

            public List<PapersListDataset> GetPapersListDataset() { return null; }
        }

        public class PapersListReportDataset
        {
            public PapersListPageHeader PageHeader { get;internal set; }
            public List<PapersListDataset> Dataset { get;internal set; }
        }


        public static PapersListReportDataset GetPapersList(int examConductID, int examDateAndSessionID, Guid loginSessionGuid)
        {
            using (var aspireContext = new AspireContext())
            {
                var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

                var pageHeader = aspireContext.ExamDateAndSessions.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID && eds.ExamConduct.InstituteID == loginHistory.InstituteID).Select(eds => new PapersListPageHeader
                {
                    InstituteName = eds.ExamConduct.Institute.InstituteName,
                    SemesterID = eds.ExamConduct.SemesterID,
                    ExamType = eds.ExamConduct.ExamType,
                    ExamDate = eds.ExamDate.Date,
                    SessionName = eds.ExamSession.SessionName,
                    StartTime = eds.ExamSession.StartTime,
                    TotalTimeInMinutes = eds.ExamSession.TotalTimeInMinutes,
                }).SingleOrDefault();

                var examDateSheet = aspireContext.ExamDateSheets.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID).Select(eds => new
                {
                    eds.OfferedCourseID,
                    eds.ExamDateAndSession.ExamSession.SessionName,
                    eds.ExamDateAndSession.ExamDate.Date,
                    eds.OfferedCours.Cours.CourseCode,
                    eds.OfferedCours.Cours.Title,
                    eds.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
                    eds.OfferedCours.SemesterNo,
                    eds.OfferedCours.Section,
                    eds.OfferedCours.Shift,
                    Teacher = eds.OfferedCours.FacultyMember.Name,
                });

                var examSeats = aspireContext.ExamSeats.Where(es => es.ExamOfferedRoom.ExamConductID == examConductID).Select(es => new
                {
                    es.RegisteredCours.OfferedCourseID,
                    es.ExamOfferedRoomID,
                    es.ExamOfferedRoom.ExamRoom.RoomName,
                    es.ExamOfferedRoom.Sequence,
                });

                var dataSet = from es in examSeats
                              join eds in examDateSheet on es.OfferedCourseID equals eds.OfferedCourseID
                              select new PapersListDataset
                              {
                                  RoomID = es.ExamOfferedRoomID,
                                  RoomName = es.RoomName,
                                  OfferedCourseID = eds.OfferedCourseID,
                                  CourseTitle = eds.Title,
                                  ProgramName = eds.ProgramAlias,
                                  SemesterNo = eds.SemesterNo,
                                  Section = eds.Section,
                                  Shift = eds.Shift,
                                  TeacherName = eds.Teacher,
                                  SessionName = eds.SessionName,
                                  ExamDate = eds.Date,
                                  Sequence = es.Sequence,
                                  Students = 0, //todo Make the count of student in each room.
                              };
                return new PapersListReportDataset { Dataset = dataSet.Distinct().ToList(), PageHeader = pageHeader };
            }
        }

    }

}
