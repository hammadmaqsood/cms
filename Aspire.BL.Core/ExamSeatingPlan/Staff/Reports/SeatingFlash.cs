﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
	public static class SeatingFlash
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public class SeatingFlashPageHeader
		{
			public string Institute { get;internal set; }
			public short SemesterID { get; internal set; }
			public byte ExamType { get; internal set; }
			public DateTime ExamDate { get;internal set; }
			public string SessionName { get;internal set; }
			public TimeSpan StartTime { get;internal set; }
			public int TotalTimeInMinutes { get;internal set; }
			public string TimeRange => $" {this.StartTime.ToString(@"hh\:mm")} To {this.StartTime.Add(TimeSpan.FromMinutes(this.TotalTimeInMinutes)).ToString(@"hh\:mm")}";
			public string Title => $"Flash Seating Plan ({this.SemesterID.ToSemesterString()} {((ExamConduct.ExamTypes)this.ExamType).ToFullName()} Exam) Date: {this.ExamDate.ToString("D")} Timing: {this.TimeRange}".ToUpper();
			public List<SeatingFlashPageHeader> GetPageHeader()
			{
				return null;
			}
		}

		public class SeatingFlashDataset
		{
			public int OfferedCourseID { get;internal set; }
			public int OfferedRoomID { get;internal set; }
			public string CourseTitle { get;internal set; }
			public string CourseCode { get;internal set; }
			public string ProgramAlias { get;internal set; }
			public short SemesterNo { get;internal set; }
			public int Section { get;internal set; }
			public byte Shift { get;internal set; }
			public string Teacher { get;internal set; }
			public string DepartmentName { get; internal set; }
			public string RoomName { get;internal set; }
			public short Sequence { get;internal set; }
			public int Students { get;internal set; }

			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);

			public List<SeatingFlashDataset> GetList()
			{
				return null;
			}
		}


		public class ReportDataset
		{
			public SeatingFlashPageHeader PageHeader { get;internal set; }
			public List<SeatingFlashDataset> SeatingFlash { get;internal set; }
		}

		public static ReportDataset GetSeatingFlash(int examDateAndSessionID, int examConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

				var pageHeader = aspireContext.ExamDateAndSessions.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID && eds.ExamConduct.InstituteID == loginHistory.InstituteID).Select(eds => new SeatingFlashPageHeader
				{
					Institute = eds.ExamConduct.Institute.InstituteName,
					SemesterID = eds.ExamConduct.SemesterID,
					ExamType = eds.ExamConduct.ExamType,
					ExamDate = eds.ExamDate.Date,
					SessionName = eds.ExamSession.SessionName,
					StartTime = eds.ExamSession.StartTime,
					TotalTimeInMinutes = eds.ExamSession.TotalTimeInMinutes,
				}).SingleOrDefault();
				if (pageHeader == null)
					return null;

				var edsOfferedCourseIDs = aspireContext.ExamDateSheets.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID && eds.ExamDateAndSession.ExamConduct.InstituteID == loginHistory.InstituteID).Select(eds => eds.OfferedCourseID);
				var query = aspireContext.ExamSeats.Where(es => es.ExamOfferedRoom.ExamConductID == examConductID &&
																edsOfferedCourseIDs.Contains(es.RegisteredCours.OfferedCourseID) &&
																es.ExamOfferedRoom.ExamConduct.InstituteID == loginHistory.InstituteID);

				var dataSet = query.GroupBy(es => new
				{
					es.RegisteredCours.OfferedCourseID,
					es.ExamOfferedRoomID,
					es.RegisteredCours.OfferedCours.Cours.CourseCode,
					es.RegisteredCours.OfferedCours.Cours.Title,
					es.RegisteredCours.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					es.RegisteredCours.OfferedCours.SemesterNo,
					es.RegisteredCours.OfferedCours.Section,
					es.RegisteredCours.OfferedCours.Shift,
					Teacher = es.RegisteredCours.OfferedCours.FacultyMember.Name,
					es.ExamOfferedRoom.ExamRoom.RoomName,
					es.ExamOfferedRoom.Sequence,
				}).Select(grp => new SeatingFlashDataset
				{
					OfferedCourseID = grp.Key.OfferedCourseID,
					CourseCode = grp.Key.CourseCode,
					CourseTitle = grp.Key.Title,
					ProgramAlias = grp.Key.ProgramAlias,
					SemesterNo = grp.Key.SemesterNo,
					Section = grp.Key.Section,
					Shift = grp.Key.Shift,
					Teacher = grp.Key.Teacher,
					RoomName = grp.Key.RoomName,
					Sequence = grp.Key.Sequence,
					Students = grp.Count(),
				});

				return new ReportDataset { PageHeader = pageHeader, SeatingFlash = dataSet.ToList() };
			}
		}
	}
}
