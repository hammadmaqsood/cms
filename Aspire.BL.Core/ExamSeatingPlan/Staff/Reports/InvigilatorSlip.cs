﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
	public static class InvigilatorSlip
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}
		public sealed class InvigilatorSlipPageHeader
		{
			public string Institute { get; internal set; }
			public int ExamConductID { get; internal set; }
			public short SemesterID { get; internal set; }
			public byte ExamType { get; internal set; }
			public string Title => $"Invigilator's Duties Slip ({this.SemesterID.ToSemesterString()} {((ExamConduct.ExamTypes)this.ExamType).ToFullName()} Exam)";

			public List<InvigilatorSlipPageHeader> GetInvigilatorsSlipsPageHeader()
			{
				return null;
			}
		}

		public sealed class InvigilatorSlipDetail
		{
			public int? DepartmentID { get; internal set; }
			public string DepartmentName { get; internal set; }
			public DateTime ExamDate { get; internal set; }
			public int ExamInvigilatorID { get; internal set; }
			public string InvigilatorName { get; internal set; }
			public string RoomName { get; internal set; }
			public byte Sequence { get; internal set; }
			public string SessionName { get; internal set; }
			public TimeSpan StartTime { get; internal set; }
			public byte TotalTimeInMinutes { get; internal set; }
			public string Timing => $"{this.StartTime.ToTime24Format()}";
			public string Session => $"{this.SessionName}{Environment.NewLine}{this.Timing}";
			public bool DisplayRoomName { get; internal set; }
			public string Duty => this.DisplayRoomName ? this.RoomName : "P";
			public int ExamSessionID { get; internal set; }

			public List<InvigilatorSlipDetail> GetList()
			{
				return null;
			}
		}


		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public InvigilatorSlipPageHeader PageHeader { get; internal set; }
			public List<InvigilatorSlipDetail> SlipDetails { get; internal set; }
		}

		public static ReportDataSet GetInvigilatorsSlips(int examConductID, int? examInvigilatorID, bool displayRoomName, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var pageHeader = aspireContext.ExamConducts.Where(ec => ec.ExamConductID == examConductID && ec.InstituteID == loginHistory.InstituteID).Select(ec => new InvigilatorSlipPageHeader
				{
					Institute = ec.Institute.InstituteName,
					ExamConductID = ec.ExamConductID,
					SemesterID = ec.SemesterID,
					ExamType = ec.ExamType,
				}).SingleOrDefault();
				if (pageHeader == null)
					return null;

				var examInvigilatorDuties = aspireContext.ExamInvigilatorDuties.Where(eid => eid.ExamDateAndSession.ExamConductID == pageHeader.ExamConductID);
				if (examInvigilatorID != null)
					examInvigilatorDuties = examInvigilatorDuties.Where(eid => eid.ExamInvigilatorID == examInvigilatorID.Value);
				var slips = examInvigilatorDuties.Select(eid => new InvigilatorSlipDetail
				{
					ExamInvigilatorID = eid.ExamInvigilatorID,
					DepartmentID = eid.ExamInvigilator.DepartmentID,
					DepartmentName = eid.ExamInvigilator.Department.DepartmentName,
					InvigilatorName = eid.ExamInvigilator.Name,
					ExamDate = eid.ExamDateAndSession.ExamDate.Date,
					ExamSessionID = eid.ExamDateAndSession.ExamSessionID,
					StartTime = eid.ExamDateAndSession.ExamSession.StartTime,
					TotalTimeInMinutes = eid.ExamDateAndSession.ExamSession.TotalTimeInMinutes,
					SessionName = eid.ExamDateAndSession.ExamSession.SessionName,
					RoomName = eid.ExamOfferedRoom.ExamRoom.RoomName,
					Sequence = eid.ExamDateAndSession.ExamSession.Sequence,
					DisplayRoomName = displayRoomName,
				});

				return new ReportDataSet { PageHeader = pageHeader, SlipDetails = slips.ToList() };
			}
		}
	}
}
