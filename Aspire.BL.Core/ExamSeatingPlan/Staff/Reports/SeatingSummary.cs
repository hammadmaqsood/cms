﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
	public static class SeatingSummary
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, Model.Entities.Common.StaffPermissions.ExamSeatingPlan.Module, Model.Entities.UserGroupPermission.PermissionValues.Allowed);
		}

		public class SeatingSummaryPageHeader
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public byte ExamType { get; internal set; }
			public DateTime ExamDate { get; internal set; }
			public string SessionName { get; internal set; }
			public TimeSpan StartTime { get; internal set; }
			public int TotalTimeInMinutes { get; internal set; }
			public string Timing => $"{this.StartTime.ToTime12Format()} To {this.StartTime.Add(TimeSpan.FromMinutes(this.TotalTimeInMinutes)).ToTime12Format()}";
			public string Title => $"Seating Plan Summary ({this.SemesterID.ToSemesterString()} {((ExamConduct.ExamTypes)this.ExamType).ToFullName()} Exam), Date: {this.ExamDate:D}, Timing: {this.Timing}";
			public int ExamConductID { get; internal set; }

			public List<SeatingSummaryPageHeader> GetPageHeader()
			{
				return null;
			}
		}

		public sealed class SeatingSummaryDetail
		{
			public int OfferedCourseID { get; internal set; }
			public string CourseTitle { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public string DepartmentName { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public string Teacher { get; internal set; }
			public int ExamOfferedRoomID { get; internal set; }
			public string RoomName { get; internal set; }
			public short Sequence { get; internal set; }
			public int Students { get; internal set; }
			public bool DisplayTeacherName { get; internal set; }
			public string CourseInfo => this.DisplayTeacherName
				? $"{this.CourseTitle}{Environment.NewLine}{this.Class}{Environment.NewLine}{this.Teacher}"
				: $"{this.CourseTitle}{Environment.NewLine}{this.Class}";

			public List<SeatingSummaryDetail> GetList()
			{
				return null;
			}
		}


		public sealed class ReportDataset
		{
			internal ReportDataset() { }
			public SeatingSummaryPageHeader PageHeader { get; internal set; }
			public List<SeatingSummaryDetail> SummaryDetails { get; internal set; }
		}

		public static ReportDataset GetSeatingSummary(int examDateAndSessionID, bool displayTeacherNames, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var pageHeader = aspireContext.ExamDateAndSessions.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID && eds.ExamConduct.InstituteID == loginHistory.InstituteID).Select(eds => new SeatingSummaryPageHeader
				{
					ExamConductID = eds.ExamConductID,
					Institute = eds.ExamConduct.Institute.InstituteName,
					SemesterID = eds.ExamConduct.SemesterID,
					ExamType = eds.ExamConduct.ExamType,
					ExamDate = eds.ExamDate.Date,
					SessionName = eds.ExamSession.SessionName,
					StartTime = eds.ExamSession.StartTime,
					TotalTimeInMinutes = eds.ExamSession.TotalTimeInMinutes,
				}).SingleOrDefault();
				if (pageHeader == null)
					return null;

				var validSeats = aspireContext.GetValidVisibleExamSeats(pageHeader.ExamConductID, examDateAndSessionID, null).GroupBy(s => new
				{
					s.RegisteredCours.OfferedCourseID,
					s.RegisteredCours.OfferedCours.Cours.AdmissionOpenProgram.Program.DepartmentID,
					s.RegisteredCours.OfferedCours.Cours.AdmissionOpenProgram.Program.Department.DepartmentName,
					s.RegisteredCours.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					s.RegisteredCours.OfferedCours.SemesterNo,
					s.RegisteredCours.OfferedCours.Section,
					s.RegisteredCours.OfferedCours.Shift,
					CourseTitle = s.RegisteredCours.OfferedCours.Cours.Title,
					Teacher = s.RegisteredCours.OfferedCours.FacultyMember.Name,
					s.ExamOfferedRoomID,
					s.ExamOfferedRoom.ExamRoom.RoomName,
					s.ExamOfferedRoom.Sequence,
				}).Select(s => new SeatingSummaryDetail
				{
					OfferedCourseID = s.Key.OfferedCourseID,
					DepartmentID = s.Key.DepartmentID,
					DepartmentName = s.Key.DepartmentName,
					ProgramAlias = s.Key.ProgramAlias,
					SemesterNo = s.Key.SemesterNo,
					Section = s.Key.Section,
					Shift = s.Key.Shift,
					CourseTitle = s.Key.CourseTitle,
					RoomName = s.Key.RoomName,
					ExamOfferedRoomID = s.Key.ExamOfferedRoomID,
					Sequence = s.Key.Sequence,
					Teacher = s.Key.Teacher,
					Students = s.Count(),
					DisplayTeacherName = displayTeacherNames
				});

				return new ReportDataset { PageHeader = pageHeader, SummaryDetails = validSeats.ToList() };
			}
		}
	}
}
