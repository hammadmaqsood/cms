﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
	public class PageHeader
	{
		public string InstituteName { get;internal set; }
		public string Title { get;internal set; }
	}
	public class ReportSetInvigilatorsAttendance
	{
		public string InvigilatorName { get;internal set; }
		public string SessionName { get;internal set; }
		public DateTime ExamDate { get;internal set; }
		public string Attendance { get;internal set; }
		public int Sequence { get;internal set; }

		public List<ReportSetInvigilatorsAttendance> GetInvigilatorsAttendanceEntity()
		{
			return null;
		}
	}

	public class ReportDataSet
	{
		public PageHeader pageHeader { get;internal set; }
		public ReportSetInvigilatorsAttendance attendances { get;internal set; }
	}

	public static class InvigilatorsAttandance
	{
		
	}
}
