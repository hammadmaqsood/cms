﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
	public static class InvigilatorDutiesDatewise
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class InvigilatorDutiesDatewisePageHeader
		{
			public string Institute { get; internal set; }
			public int ExamDateID { get; internal set; }
			public DateTime ExamDate { get; internal set; }
			public string Title => $"Invigilator Duties: {this.ExamDate:D}";

			public List<InvigilatorDutiesDatewisePageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class InvigilatorDutiesDatewiseInvigilatorDuty
		{
			public int ExamInvigilatorID { get; internal set; }
			public string InvigilatorName { get; internal set; }
			public int? ExamSessionID { get; internal set; }
			public string SessionName { get; internal set; }
			public int? Sequence { get; internal set; }
			public bool HasDuty { get; internal set; }
			public byte Status { get; internal set; }
			public string StatusFullName => ((ExamInvigilator.Statuses)this.Status).ToFullName();

			public static List<InvigilatorDutiesDatewiseInvigilatorDuty> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			internal ReportDataSet() { }
			public InvigilatorDutiesDatewisePageHeader PageHeader { get; set; }
			public List<InvigilatorDutiesDatewiseInvigilatorDuty> InvigilatorDuties { get; set; }
		}

		public static ReportDataSet GetInvigilatorDutiesDatewise(int examDateID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

				var pageHeader = aspireContext.ExamDates.Where(ed => ed.ExamDateID == examDateID && ed.ExamConduct.InstituteID == loginHistory.InstituteID).Select(ed => new InvigilatorDutiesDatewisePageHeader
				{
					Institute = ed.ExamConduct.Institute.InstituteName,
					ExamDateID = ed.ExamDateID,
					ExamDate = ed.Date,
				}).SingleOrDefault();
				if (pageHeader == null)
					return null;

				var duties = (from ei in aspireContext.ExamInvigilators
							  join eid in aspireContext.ExamInvigilatorDuties.Where(d => d.ExamDateAndSession.ExamDateID == pageHeader.ExamDateID) on ei.ExamInvigilatorID equals eid.ExamInvigilatorID into grp
							  from g in grp.DefaultIfEmpty()
							  select new InvigilatorDutiesDatewiseInvigilatorDuty
							  {
								  ExamInvigilatorID = ei.ExamInvigilatorID,
								  Status = ei.Status,
								  InvigilatorName = ei.Name,
								  ExamSessionID = g.ExamDateAndSession.ExamSessionID,
								  Sequence = g.ExamDateAndSession.ExamSession.Sequence,
								  SessionName = g.ExamDateAndSession.ExamSession.SessionName,
								  HasDuty = g != null,
							  }
							).ToList();

				return new ReportDataSet { PageHeader = pageHeader, InvigilatorDuties = duties };
			}
		}

		public static List<CustomListItem> GetExamDates(int examConductID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.ExamDates.Where(ed => ed.ExamConductID == examConductID).Select(ed => new
				{
					ed.ExamDateID,
					ed.Date,
				}).OrderBy(o => o.Date).ToList().Select(s => new CustomListItem
				{
					ID = s.ExamDateID,
					Text = s.Date.ToString("ddd, dd-MMM-yyyy"),
				}).ToList();
			}
		}
	}
}
