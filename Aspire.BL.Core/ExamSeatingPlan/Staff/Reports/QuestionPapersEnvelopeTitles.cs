﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Aspire.Lib.Extensions;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
	public static class QuestionPapersEnvelopeTitles
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class QuestionPapersEnvelopeTitle
		{
			public int OfferedCourseID { get; internal set; }
			public int ExamOfferedRoomID { get; internal set; }
			public string CourseTitle { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public string Teacher { get; internal set; }
			public string RoomName { get; internal set; }
			public string SessionName { get; internal set; }
			public DateTime ExamDate { get; internal set; }
			public short Sequence { get; internal set; }
			public int Students { get; internal set; }
			public int ExamDateAndSessionID { get; internal set; }
			public TimeSpan StartTime { get; internal set; }
			public byte TotalTimeInMinutes { get; internal set; }
			public string Timing => $"{this.StartTime.ToTime12Format()} To {this.StartTime.Add(TimeSpan.FromMinutes(this.TotalTimeInMinutes)).ToTime12Format()}";

			public List<QuestionPapersEnvelopeTitle> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataset
		{
			internal ReportDataset() { }
			public List<QuestionPapersEnvelopeTitle> EnvelopeTitles { get; internal set; }
		}

		public static ReportDataset GetData(int examDateAndSessionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

				var examDateAndSession = aspireContext.ExamDateAndSessions.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID && eds.ExamConduct.InstituteID == loginHistory.InstituteID).Select(eds => new
				{
					eds.ExamConductID,
					eds.ExamDateAndSessionID,
					eds.ExamDate.Date,
					eds.ExamSession.SessionName,
					eds.ExamSession.StartTime,
					eds.ExamSession.TotalTimeInMinutes,
				}).SingleOrDefault();
				if (examDateAndSession == null)
					return null;

				var envelopeTitles = aspireContext.GetExamSeats(examDateAndSession.ExamConductID, examDateAndSession.ExamDateAndSessionID, null).Select(es => new
				{
					examDateAndSession.ExamDateAndSessionID,
					examDateAndSession.StartTime,
					examDateAndSession.TotalTimeInMinutes,
					examDateAndSession.Date,
					examDateAndSession.SessionName,
					es.RegisteredCours.OfferedCourseID,
					es.ExamOfferedRoomID,
					es.ExamOfferedRoom.ExamRoom.RoomName,
					es.RegisteredCours.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					es.RegisteredCours.OfferedCours.SemesterNo,
					es.RegisteredCours.OfferedCours.Section,
					es.RegisteredCours.OfferedCours.Shift,
					es.RegisteredCours.OfferedCours.Cours.Title,
					es.RegisteredCours.OfferedCours.FacultyMember.Name,
					es.ExamOfferedRoom.Sequence,
				}).GroupBy(es => es).Select(g => new QuestionPapersEnvelopeTitle
				{
					ExamDateAndSessionID = g.Key.ExamDateAndSessionID,
					StartTime = g.Key.StartTime,
					TotalTimeInMinutes = g.Key.TotalTimeInMinutes,
					OfferedCourseID = g.Key.OfferedCourseID,
					ExamOfferedRoomID = g.Key.ExamOfferedRoomID,
					SessionName = g.Key.SessionName,
					ProgramAlias = g.Key.ProgramAlias,
					ExamDate = g.Key.Date,
					Sequence = g.Key.Sequence,
					Shift = g.Key.Shift,
					SemesterNo = g.Key.SemesterNo,
					RoomName = g.Key.RoomName,
					Section = g.Key.Section,
					CourseTitle = g.Key.Title,
					Teacher = g.Key.Name,
					Students = g.Count(),
				});

				return new ReportDataset { EnvelopeTitles = envelopeTitles.ToList() };
			}
		}
	}
}

