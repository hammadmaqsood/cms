﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
	public static class PaperReceiving
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class PaperReceivingPageHeader
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public byte ExamType { get; internal set; }
			public DateTime ExamDate { get; internal set; }
			public string SessionName { get; internal set; }
			public TimeSpan StartTime { get; internal set; }
			public int TotalTimeInMinutes { get; internal set; }
			public string Timing => $"{this.StartTime.ToTime12Format()} To {this.StartTime.Add(TimeSpan.FromMinutes(this.TotalTimeInMinutes)).ToTime12Format()}";
			public string Title => $"Paper Receiving ({this.SemesterID.ToSemesterString()} {((ExamConduct.ExamTypes)this.ExamType).ToFullName()} Exam), Date: {this.ExamDate:D}, Timing: {this.Timing}";
			public int ExamConductID { get; internal set; }

			public List<PaperReceivingPageHeader> GetPageHeader()
			{
				return null;
			}
		}

		public sealed class PaperReceivingDetail
		{
			public int OfferedCourseID { get; internal set; }
			public int? TeacherID { get; internal set; }
			public string TeacherName { get; internal set; }
			public string CourseTitle { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public int Students { get; internal set; }

			public List<PaperReceivingDetail> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataset
		{
			internal ReportDataset() { }
			public PaperReceivingPageHeader PageHeader { get; internal set; }
			public List<PaperReceivingDetail> Details { get; internal set; }
		}

		public static ReportDataset GetPaperReceiving(int examDateAndSessionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

				var pageHeader = aspireContext.ExamDateAndSessions.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID && eds.ExamConduct.InstituteID == loginHistory.InstituteID).Select(eds => new PaperReceivingPageHeader
				{
					ExamConductID = eds.ExamConductID,
					Institute = eds.ExamConduct.Institute.InstituteName,
					SemesterID = eds.ExamConduct.SemesterID,
					ExamType = eds.ExamConduct.ExamType,
					ExamDate = eds.ExamDate.Date,
					SessionName = eds.ExamSession.SessionName,
					StartTime = eds.ExamSession.StartTime,
					TotalTimeInMinutes = eds.ExamSession.TotalTimeInMinutes,
				}).SingleOrDefault();
				if (pageHeader == null)
					return null;

				var examSeats = aspireContext.GetValidVisibleExamSeats(pageHeader.ExamConductID, examDateAndSessionID, null);

				var details = examSeats.GroupBy(es => new
				{
					es.RegisteredCours.OfferedCourseID,
					es.RegisteredCours.OfferedCours.Cours.Title,
					es.RegisteredCours.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					es.RegisteredCours.OfferedCours.SemesterNo,
					es.RegisteredCours.OfferedCours.Section,
					es.RegisteredCours.OfferedCours.Shift,
					es.RegisteredCours.OfferedCours.FacultyMember.Name,
				}).Select(es => new PaperReceivingDetail
				{
					OfferedCourseID = es.Key.OfferedCourseID,
					CourseTitle = es.Key.Title,
					TeacherName = es.Key.Name,
					ProgramAlias = es.Key.ProgramAlias,
					SemesterNo = es.Key.SemesterNo,
					Section = es.Key.Section,
					Shift = es.Key.Shift,
					Students = es.Count(),
				});
				return new ReportDataset { PageHeader = pageHeader, Details = details.ToList() };
			}
		}
	}
}
