﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
	public static class InvigilatorsAttendance
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class InvigilatorsAttendancePageHeader
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public byte ExamType { get; internal set; }
			public string Title => $"Invigilators Attendance ({this.SemesterID.ToSemesterString()} {((ExamConduct.ExamTypes)this.ExamType).ToFullName()} Exam)";
			public static List<InvigilatorsAttendancePageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class InvigilatorsAttendanceDetail
		{
			public byte AttendanceStatus { get; internal set; }
			public string Attendance
			{
				get
				{
					switch ((ExamInvigilatorDuty.AttendanceStatuses)this.AttendanceStatus)
					{
						case ExamInvigilatorDuty.AttendanceStatuses.Absent:
							return "✗";
						case ExamInvigilatorDuty.AttendanceStatuses.Present:
							return "✓";
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}

			public DateTime ExamDate { get; internal set; }
			public int ExamDateID { get; internal set; }
			public int ExamInvigilatorID { get; internal set; }
			public int ExamSessionID { get; internal set; }
			public string Name { get; internal set; }
			public byte Sequence { get; internal set; }
			public string SessionName { get; internal set; }

			public static List<InvigilatorsAttendanceDetail> GetList()
			{
				return null;
			}
		}


		public sealed class ReportDataSet
		{
			internal ReportDataSet()
			{
			}

			public InvigilatorsAttendancePageHeader PageHeader { get; internal set; }
			public List<InvigilatorsAttendanceDetail> AttendanceDetails { get; internal set; }
		}

		public static ReportDataSet GetInvigilatorsAttendance(int examConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var pageHeader = aspireContext.ExamConducts.Where(ec => ec.ExamConductID == examConductID && ec.InstituteID == loginHistory.InstituteID).Select(ec => new InvigilatorsAttendancePageHeader
				{
					Institute = ec.Institute.InstituteName,
					ExamType = ec.ExamType,
					SemesterID = ec.SemesterID,
				}).SingleOrDefault();

				var attendance = aspireContext.ExamInvigilatorDuties.Where(eid => eid.ExamDateAndSession.ExamConductID == examConductID).Select(eid => new InvigilatorsAttendanceDetail
				{
					ExamInvigilatorID = eid.ExamInvigilatorID,
					Name = eid.ExamInvigilator.Name,
					ExamDateID = eid.ExamDateAndSession.ExamDateID,
					ExamDate = eid.ExamDateAndSession.ExamDate.Date,
					ExamSessionID = eid.ExamDateAndSession.ExamSessionID,
					SessionName = eid.ExamDateAndSession.ExamSession.SessionName,
					Sequence = eid.ExamDateAndSession.ExamSession.Sequence,
					AttendanceStatus = eid.AttendanceStatus,
				});
				return new ReportDataSet { PageHeader = pageHeader, AttendanceDetails = attendance.ToList() };
			}
		}
	}
}
