﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
	public static class DateSheet
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class DateSheetPageHeader
		{
			public int ExamDateAndSessionID { get; internal set; }
			public int ExamConductID { get; internal set; }
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public byte ExamType { get; internal set; }
			public string Title => $"Date Sheet ({this.SemesterID.ToSemesterString()} {((ExamConduct.ExamTypes)this.ExamType).ToFullName()} Exam)".ToUpper();

			public List<DateSheetPageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class DateSheetDataSet
		{
			public int OfferedCourseID { get; internal set; }
			public int? TeacherID { get; internal set; }
			public string TeacherName { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public string DepartmentName { get; internal set; }
			public string CourseTitle { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public string SessionName { get; internal set; }
			public TimeSpan? Time { get; internal set; }
			public DateTime? ExamDate { get; internal set; }
			public int TotalStudents { get; internal set; }
			public int? ValidStudents { get; internal set; }
			public int? StudentClashes { get; internal set; }
			public int? ExamDateAndSessionID { get; internal set; }

			public static List<DateSheetDataSet> GetDateSheet()
			{
				return null;
			}
		}

		public class ReportDataSet
		{
			internal ReportDataSet() { }
			public DateSheetPageHeader PageHeader { get; set; }
			public List<DateSheetDataSet> DateSheet { get; set; }
		}

		public static ReportDataSet GetDateSheet(int examConductID, int? departmentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var pageHeader = aspireContext.ExamConducts
					.Where(ec => ec.ExamConductID == examConductID && ec.InstituteID == loginHistory.InstituteID)
					.Select(ec => new DateSheetPageHeader
					{
						ExamConductID = ec.ExamConductID,
						SemesterID = ec.SemesterID,
						Institute = ec.Institute.InstituteName,
						ExamType = ec.ExamType,
					}).SingleOrDefault();
				if (pageHeader == null)
					return null;

				var offeredCourses = aspireContext.OfferedCourses
					.Where(oc => oc.SemesterID == pageHeader.SemesterID)
					.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				if (departmentID != null)
					offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);

				var examDateSheets = aspireContext.ExamDateSheets.Where(eds => eds.ExamDateAndSession.ExamConductID == examConductID);
				var result = (from oc in offeredCourses
							  join eds in examDateSheets on oc.OfferedCourseID equals eds.OfferedCourseID into grp
							  from g in grp.DefaultIfEmpty()
							  select new DateSheetDataSet
							  {
								  OfferedCourseID = oc.OfferedCourseID,
								  TeacherID = oc.FacultyMemberID,
								  TeacherName = oc.FacultyMember.Name,
								  DepartmentID = oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
								  DepartmentName = oc.Cours.AdmissionOpenProgram.Program.Department.DepartmentName,
								  CourseTitle = oc.Cours.Title,
								  ProgramAlias = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
								  SemesterNo = oc.SemesterNo,
								  Section = oc.Section,
								  Shift = oc.Shift,
								  SessionName = g.ExamDateAndSession.ExamSession.SessionName,
								  Time = g.ExamDateAndSession.ExamSession.StartTime,
								  ExamDate = g.ExamDateAndSession.ExamDate.Date,
								  ExamDateAndSessionID = g.ExamDateAndSessionID,
								  TotalStudents = oc.RegisteredCourses.Count(rc => rc.DeletedDate == null),
							  }).ToList();

				var validRegisteredCoursesForSeatingPlan = aspireContext.RegisteredCourses
					.Where(rc => rc.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(rc => rc.OfferedCours.SemesterID == pageHeader.SemesterID)
					.FilterValidRegisteredCoursesForSeatingPlan()
					.Select(rc => new
					{
						rc.StudentID,
						rc.OfferedCourseID,
					});

				var registeredCourses = (from rc in validRegisteredCoursesForSeatingPlan
										 join eds in examDateSheets on rc.OfferedCourseID equals eds.OfferedCourseID into grp
										 from g in grp.DefaultIfEmpty()
										 select new
										 {
											 rc.StudentID,
											 rc.OfferedCourseID,
											 ExamDateAndSessionID = (int?)g.ExamDateAndSessionID,
										 }).ToList();

				foreach (var ds in result)
				{
					var validStudents = registeredCourses.Where(rc => rc.OfferedCourseID == ds.OfferedCourseID).ToList();
					ds.ValidStudents = validStudents.Count();
					if (ds.ExamDateAndSessionID != null)
					{
						ds.StudentClashes = registeredCourses
							.Where(rc => rc.OfferedCourseID != ds.OfferedCourseID)
							.Where(rc => rc.ExamDateAndSessionID == ds.ExamDateAndSessionID)
							.Count(rc => validStudents.Any(s => s.StudentID == rc.StudentID));
					}
				}

				return new ReportDataSet { PageHeader = pageHeader, DateSheet = result };
			}
		}
	}
}

