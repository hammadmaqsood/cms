﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
	public static class StudentAttendanceSheets
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class StudentAttendanceSheetsPageHeader
		{
			public int ExamDateAndSessionID { get; internal set; }
			public int ExamConductID { get; internal set; }
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public byte ExamType { get; internal set; }
			public TimeSpan StartTime { get; internal set; }
			public byte TotalTimeInMinutes { get; internal set; }
			public string Timing => $"{this.StartTime.ToTime12Format()} To {this.StartTime.Add(TimeSpan.FromMinutes(this.TotalTimeInMinutes)).ToTime12Format()}";
			public DateTime Date { get; internal set; }
			public string SessionName { get; internal set; }
			public string Title => $"Attendance Sheet ({this.SemesterID.ToSemesterString()} {((ExamConduct.ExamTypes)this.ExamType).ToFullName()} Exam), Timing: {this.Timing}";

			public List<StudentAttendanceSheetsPageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class StudentAttendanceSheetsStudentDetail
		{
			public int OfferedCourseID { get; internal set; }
			public int RoomID { get; internal set; }
			public short RoomSequence { get; internal set; }
			public string RoomName { get; internal set; }
			public byte Row { get; internal set; }
			public byte Column { get; internal set; }
			public string Enrollment { get; internal set; }
			public string StudentName { get; internal set; }
			public string CourseTitle { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public string Program { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift);
			public byte? StudentFeeStatus { get; internal set; }
			private int StudentInfoLength => this.LegalPage ? 18 : 12;
			private string StudentNameTrimmed => this.StudentName.Length > this.StudentInfoLength ? this.StudentName.Substring(0, this.StudentInfoLength) : this.StudentName;
			private string ClassTrimmed => this.Class.Length > this.StudentInfoLength ? this.Class.Substring(0, this.StudentInfoLength) : this.Class;
			private string CourseTitleTrimmed => this.CourseTitle.Length > this.StudentInfoLength ? this.CourseTitle.Substring(0, this.StudentInfoLength) : this.CourseTitle;
			public string StudentInfo => $"{this.Enrollment}\n{this.StudentNameTrimmed}\n{this.CourseTitleTrimmed}";
			internal bool LegalPage { get; set; }
			public bool FeeDefaulter
			{
				get
				{
					switch ((StudentFee.Statuses?)this.StudentFeeStatus)
					{
						case null:
						case StudentFee.Statuses.NotPaid:
							return false;
						case StudentFee.Statuses.Paid:
							return true;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
			public int? PaperSerialNo { get; internal set; }
			public int? FacultyMemberID { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public int ProgramID { get; internal set; }

			public List<StudentAttendanceSheetsStudentDetail> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataSet
		{
			public StudentAttendanceSheetsPageHeader PageHeader { get; internal set; }
			public List<StudentAttendanceSheetsStudentDetail> StudentDetails { get; internal set; }
		}

		public static ReportDataSet GetData(int examDateAndSessionID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int? facultyMemberID, ExamOfferedRoom.RoomUsageTypes roomUsageTypeEnum, bool legalPage, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var pageHeader = aspireContext.ExamDateAndSessions
					.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID && eds.ExamConduct.InstituteID == loginHistory.InstituteID)
					.Select(eds => new StudentAttendanceSheetsPageHeader
					{
						ExamDateAndSessionID = eds.ExamDateAndSessionID,
						ExamConductID = eds.ExamConductID,
						SemesterID = eds.ExamConduct.SemesterID,
						Institute = eds.ExamConduct.Institute.InstituteName,
						ExamType = eds.ExamConduct.ExamType,
						Date = eds.ExamDate.Date,
						StartTime = eds.ExamSession.StartTime,
						SessionName = eds.ExamSession.SessionName,
						TotalTimeInMinutes = eds.ExamSession.TotalTimeInMinutes,
					}).SingleOrDefault();
				if (pageHeader == null)
					return null;
				var validExamSeats = aspireContext.GetValidVisibleExamSeats(pageHeader.ExamConductID, pageHeader.ExamDateAndSessionID, roomUsageTypeEnum);
				var studentDetailsQuery = validExamSeats.Select(es => new StudentAttendanceSheetsStudentDetail
				{
					LegalPage = legalPage,
					OfferedCourseID = es.RegisteredCours.OfferedCourseID,
					Enrollment = es.RegisteredCours.Student.Enrollment,
					StudentName = es.RegisteredCours.Student.Name,
					CourseTitle = es.RegisteredCours.OfferedCours.Cours.Title,
					FacultyMemberID = es.RegisteredCours.OfferedCours.FacultyMemberID,
					FacultyMemberName = es.RegisteredCours.OfferedCours.FacultyMember.Name,
					DepartmentID = es.RegisteredCours.OfferedCours.Cours.AdmissionOpenProgram.Program.DepartmentID,
					ProgramID = es.RegisteredCours.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramID,
					Program = es.RegisteredCours.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = es.RegisteredCours.OfferedCours.SemesterNo,
					Section = es.RegisteredCours.OfferedCours.Section,
					Shift = es.RegisteredCours.OfferedCours.Shift,
					StudentFeeStatus = es.RegisteredCours.StudentFee.Status,
					RoomSequence = es.ExamOfferedRoom.Sequence,
					RoomID = es.ExamOfferedRoom.ExamRoomID,
					RoomName = es.ExamOfferedRoom.ExamRoom.RoomName,
					Row = es.Row,
					Column = es.Column,
					PaperSerialNo = null,
				});
				if (departmentID != null)
					studentDetailsQuery = studentDetailsQuery.Where(s => s.DepartmentID == departmentID.Value);
				if (programID != null)
					studentDetailsQuery = studentDetailsQuery.Where(s => s.ProgramID == programID.Value);
				if (semesterNoEnum != null)
					studentDetailsQuery = studentDetailsQuery.Where(s => s.SemesterNo == (short)semesterNoEnum.Value);
				if (sectionEnum != null)
					studentDetailsQuery = studentDetailsQuery.Where(s => s.Section == (int)sectionEnum.Value);
				if (shiftEnum != null)
					studentDetailsQuery = studentDetailsQuery.Where(s => s.Shift == (byte)shiftEnum.Value);
				if (facultyMemberID != null)
					studentDetailsQuery = studentDetailsQuery.Where(s => s.FacultyMemberID == facultyMemberID.Value);
				var studentDetails = studentDetailsQuery.ToList();
				var offeredCourses = studentDetails.OrderBy(g => g.Class).ThenBy(g => g.CourseTitle).Select(rc => rc.OfferedCourseID).Distinct().ToList();
				var paperSerialNo = 1;
				foreach (var offeredCourseID in offeredCourses)
				{
					studentDetails.FindAll(rc => rc.OfferedCourseID == offeredCourseID).ForEach(rc => rc.PaperSerialNo = paperSerialNo);
					paperSerialNo++;
				}

				return new ReportDataSet
				{
					PageHeader = pageHeader,
					StudentDetails = studentDetails
				};
			}
		}
	}
}
