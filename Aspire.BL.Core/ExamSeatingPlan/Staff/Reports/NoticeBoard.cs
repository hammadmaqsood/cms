﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
    public static class NoticeBoard
    {
        private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
        {
            return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
        }
        public class NoticeBoardDataset
        {
            public int RoomID { get;internal set; }
            public string RoomName { get;internal set; }
            public int RowNumber { get;internal set; }
            public int ColumnNumber { get;internal set; }
            public string Enrollment { get;internal set; }
            public string StudentName { get;internal set; }
            public string CourseTitle { get;internal set; }
            public int Attendance { get;internal set; }
            public int OfferedCourseID { get;internal set; }

            public string ProgramName { get;internal set; }
            public short SemesterNo { get;internal set; }
            public int Section { get;internal set; }
            public byte Shift { get;internal set; }
            public string Class => AspireFormats.GetClassName(this.ProgramName, this.SemesterNo, this.Section, this.Shift);

            public string Remarks { get;internal set; }
            public List<NoticeBoardDataset> GetNoticeBoardDataset() { return null; }
        }

        public class NoticeBoardPageHeader
        {
            public string InstituteName { get;internal set; }
            public short SemesterID { get; internal set; }
            public byte ExamType { get; internal set; }
            public DateTime ExamDate { get;internal set; }
            public string SessionName { get;internal set; }
            public TimeSpan StartTime { get;internal set; }
            public int TotalTimeInMinutes { get;internal set; }
            public string TimeRange => $" {this.StartTime} To {this.StartTime.Add(TimeSpan.FromMinutes(this.TotalTimeInMinutes))}";
            public string ReportTitle => $"Invigilator's Duties ({this.SemesterID.ToSemesterString()} {((ExamConduct.ExamTypes)this.ExamType).ToFullName()} Exam), Timing: {this.TimeRange}".ToUpper();
            public List<NoticeBoardPageHeader> GetNoticeBoardPageHeader() { return null; }
        }
        public class ReportDataset
        {
            public List<NoticeBoardDataset> NoticeBoardDataset { get;internal set; }
            public NoticeBoardPageHeader NoticeBoardPageHeader { get;internal set; }
        }

        public static ReportDataset GetNoticeBoard(int examDateAndSessionID, Guid loginSessionGuid)
        {
            using (var aspireContext = new AspireContext())
            {
                var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
                var pageHeader = aspireContext.ExamDateAndSessions.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID && eds.ExamConduct.InstituteID == loginHistory.InstituteID).Select(eds => new NoticeBoardPageHeader
                {
                    InstituteName = eds.ExamConduct.Institute.InstituteName,
                    SemesterID = eds.ExamConduct.SemesterID,
                    ExamType = eds.ExamConduct.ExamType,
                    ExamDate = eds.ExamDate.Date,
                    SessionName = eds.ExamSession.SessionName,
                    StartTime = eds.ExamSession.StartTime,
                    TotalTimeInMinutes = eds.ExamSession.TotalTimeInMinutes,
                }).SingleOrDefault();

                var examConductID = aspireContext.ExamDateAndSessions.Single(eds => eds.ExamDateAndSessionID == examDateAndSessionID).ExamConductID;

                var dateSheetOfferedCourseIDs = aspireContext.ExamDateSheets.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID).Select(eds => eds.OfferedCourseID);

                var dataset = aspireContext.ExamSeats.Where(es => dateSheetOfferedCourseIDs.Contains(es.RegisteredCours.OfferedCourseID) && es.ExamOfferedRoom.ExamConductID == examConductID).Select(es => new NoticeBoardDataset
                {
                    RoomID = es.ExamOfferedRoom.ExamRoomID,
                    RoomName = es.ExamOfferedRoom.ExamRoom.RoomName,
                    RowNumber = es.Row,
                    ColumnNumber = es.Column,
                    Enrollment = es.RegisteredCours.Student.Enrollment,
                    StudentName = es.RegisteredCours.Student.Name,
                    OfferedCourseID = es.RegisteredCours.OfferedCourseID,
                    CourseTitle = es.RegisteredCours.OfferedCours.Cours.Title,
                    ProgramName = es.RegisteredCours.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
                    SemesterNo = es.RegisteredCours.OfferedCours.SemesterNo,
                    Section = es.RegisteredCours.OfferedCours.Section,
                    Shift = es.RegisteredCours.OfferedCours.Shift,
                    Remarks = es.RegisteredCours.OfferedCours.Remarks
                }).ToList();

                return new ReportDataset { NoticeBoardDataset = dataset, NoticeBoardPageHeader = pageHeader };
            }
        }
    }
}
