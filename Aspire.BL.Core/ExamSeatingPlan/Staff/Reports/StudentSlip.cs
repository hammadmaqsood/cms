﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.ExamSeatingPlan.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
	#region StudentSlip

	public static class StudentSlip
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public static List<ExamSeatingPlan.Common.ExamSeats.RegisteredCourseExamSeat> GetStudentSlips(int examConductID, string enrollment, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var examConduct = aspireContext.ExamConducts
					.Where(ec => ec.ExamConductID == examConductID && ec.InstituteID == loginHistory.InstituteID)
					.Select(ec => new
					{
						ec.ExamConductID,
						ec.SemesterID,
						ec.ExamType,
					}).SingleOrDefault();
				if (examConduct == null)
					return null;
				var student = aspireContext.Students
					.Where(s => s.Enrollment == enrollment && s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Select(s => new
					{
						s.StudentID
					}).SingleOrDefault();

				if (student == null)
					return null;
				return aspireContext.GetStudentSlips(student.StudentID, examConduct.SemesterID, (ExamConduct.ExamTypes)examConduct.ExamType, true);
			}
		}
	}

	#endregion
}

