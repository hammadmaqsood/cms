﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.ExamSeatingPlan.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
	public static class QuestionPaperReceiving
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class QuestionPaperReceivingPageHeader
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public byte ExamType { get; internal set; }
			public string Title => $"Question Papers Received ({this.SemesterID.ToSemesterString()} {((ExamConduct.ExamTypes)this.ExamType).ToFullName()} Exam)";
			public List<QuestionPaperReceivingPageHeader> GetPageHeader()
			{
				return null;
			}
		}

		public sealed class QuestionPaperReceivingDetail
		{
			public int OfferedCourseID { get; internal set; }
			public string TeacherName { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public string DepartmentName { get; internal set; }
			public string CourseTitle { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public string SessionName { get; internal set; }
			public DateTime ExamDate { get; internal set; }
			public string ExamDateAndSession => AspireFormats.GetExamDateAndSessionString(this.ExamDate, this.SessionName);
			public DateTime? QuestionPaperReceviedDate { get; internal set; }
			public string QuestionPaperReceviedDateString => (this.QuestionPaperReceviedDate?.ToShortDateString()) ?? "Pending";

			public List<QuestionPaperReceivingDetail> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataset
		{
			internal ReportDataset() { }
			public QuestionPaperReceivingPageHeader PageHeader { get; internal set; }
			public List<QuestionPaperReceivingDetail> ReceivingDetails { get; internal set; }
		}

		public static ReportDataset GetQuestionPaperReceiving(int examConductID, int? departmentID, bool? questionPaperReceived, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

				var pageHeader = aspireContext.ExamConducts.Where(ec => ec.ExamConductID == examConductID && ec.InstituteID == loginHistory.InstituteID).Select(ec => new QuestionPaperReceivingPageHeader
				{
					Institute = ec.Institute.InstituteName,
					SemesterID = ec.SemesterID,
					ExamType = ec.ExamType,
				}).SingleOrDefault();
				if (pageHeader == null)
					return null;

				var examDateSheets = aspireContext.GetExamDateSheets(examConductID, null);
				if (departmentID != null)
					examDateSheets = examDateSheets.Where(eds => eds.OfferedCours.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
				if (questionPaperReceived != null)
					examDateSheets = examDateSheets.Where(eds => eds.QuestionPaperReceivedDate.HasValue == questionPaperReceived.Value);

				var dataSet = examDateSheets.Select(eds => new QuestionPaperReceivingDetail
				{
					OfferedCourseID = eds.OfferedCourseID,
					TeacherName = eds.OfferedCours.FacultyMember.Name,
					DepartmentID = eds.OfferedCours.Cours.AdmissionOpenProgram.Program.DepartmentID,
					DepartmentName = eds.OfferedCours.Cours.AdmissionOpenProgram.Program.Department.DepartmentName,
					CourseTitle = eds.OfferedCours.Cours.Title,
					ProgramAlias = eds.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = eds.OfferedCours.SemesterNo,
					Section = eds.OfferedCours.Section,
					Shift = eds.OfferedCours.Shift,
					SessionName = eds.ExamDateAndSession.ExamSession.SessionName,
					ExamDate = eds.ExamDateAndSession.ExamDate.Date,
					QuestionPaperReceviedDate = eds.QuestionPaperReceivedDate,
				});

				return new ReportDataset { PageHeader = pageHeader, ReceivingDetails = dataSet.ToList() };
			}
		}
	}
}
