﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff.Reports
{
	public static class InvigilationDuties
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class InvigilationDutiesPageHeader
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public byte ExamType { get; internal set; }
			public DateTime ExamDate { get; internal set; }
			public string SessionName { get; internal set; }
			public TimeSpan StartTime { get; internal set; }
			public int TotalTimeInMinutes { get; internal set; }
			public string TimeRange => $" {this.StartTime.ToTime12Format()} To {this.StartTime.Add(TimeSpan.FromMinutes(this.TotalTimeInMinutes)).ToTime12Format()}";
			public string Title => $"Invigilation Duties ({this.SemesterID.ToSemesterString()} {((ExamConduct.ExamTypes)this.ExamType).ToFullName()} Exam)";
			public string DateAndTime => $"Date: {this.ExamDate:D}, Timing: {this.TimeRange}";

			public int ExamConductID { get; internal set; }
			public int ExamDateAndSessionID { get; internal set; }

			public List<InvigilationDutiesPageHeader> GetList()
			{
				return null;
			}
		}

		public sealed class InvigilationDuty
		{
			public int ExamOfferedRoomID { get; internal set; }
			public string RoomName { get; internal set; }
			public string InvigilatorName1 { get; internal set; }
			public string InvigilatorName2 { get; internal set; }
			public int Sequence { get; internal set; }
			public int ExamBuildingID { get; internal set; }
			public string BuildingName { get; internal set; }
			public int Students { get; internal set; }

			public List<InvigilationDuty> GetList()
			{
				return null;
			}
		}

		public sealed class ReportDataset
		{
			internal ReportDataset() { }
			public InvigilationDutiesPageHeader PageHeader { get; internal set; }
			public List<InvigilationDuty> InvigilationDuties { get; internal set; }
		}

		public static ReportDataset GetInvigilatorDuties(int examDateAndSessionID, bool sorted, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

				var pageHeader = aspireContext.ExamDateAndSessions
					.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID && eds.ExamConduct.InstituteID == loginHistory.InstituteID)
					.Select(eds => new InvigilationDutiesPageHeader
					{
						ExamConductID = eds.ExamConductID,
						ExamDateAndSessionID = eds.ExamDateAndSessionID,
						Institute = eds.ExamConduct.Institute.InstituteName,
						SemesterID = eds.ExamConduct.SemesterID,
						ExamType = eds.ExamConduct.ExamType,
						ExamDate = eds.ExamDate.Date,
						SessionName = eds.ExamSession.SessionName,
						StartTime = eds.ExamSession.StartTime,
						TotalTimeInMinutes = eds.ExamSession.TotalTimeInMinutes,
					}).SingleOrDefault();
				if (pageHeader == null)
					return null;

				var validExamSeats = aspireContext.GetValidExamSeats(pageHeader.ExamConductID, pageHeader.ExamDateAndSessionID, null)
					.GroupBy(es => es.ExamOfferedRoomID)
					.Select(es => new { ExamOfferedRoomID = es.Key, Seats = es.Count() }).ToList();

				var duties = (from eor in aspireContext.ExamOfferedRooms.Where(r => r.ExamConductID == pageHeader.ExamConductID)
							  join eid in aspireContext.ExamInvigilatorDuties.Where(d => d.ExamDateAndSessionID == pageHeader.ExamDateAndSessionID) on eor.ExamOfferedRoomID equals eid.ExamOfferedRoomID into grp
							  from g in grp.DefaultIfEmpty()
							  select new
							  {
								  eor.ExamOfferedRoomID,
								  eor.ExamRoom.RoomName,
								  eor.ExamRoom.ExamBuildingID,
								  eor.ExamRoom.ExamBuilding.BuildingName,
								  eor.Sequence,
								  InvigilatorName = g != null ? g.ExamInvigilator.Name : null,
								  InvigilatorType = g != null ? g.InvigilatorType : (byte?)null,
							  }).ToList();

				var invigilatorDuties = duties.GroupBy(d => d.ExamOfferedRoomID).Select(d =>
				{
					var invigilatorType1 = d.SingleOrDefault(i => i.InvigilatorType == ExamInvigilatorDuty.InvigilatorTypeInvigilatorOne);
					var invigilatorType2 = d.SingleOrDefault(i => i.InvigilatorType == ExamInvigilatorDuty.InvigilatorTypeInvigilatorTwo);
					var examOfferedRoom = (invigilatorType1 ?? invigilatorType2) ?? d.First();
					return new InvigilationDuty
					{
						ExamOfferedRoomID = examOfferedRoom.ExamOfferedRoomID,
						Students = validExamSeats.SingleOrDefault(s => s.ExamOfferedRoomID == d.Key)?.Seats ?? 0,
						Sequence = examOfferedRoom.Sequence,
						BuildingName = examOfferedRoom.BuildingName,
						RoomName = examOfferedRoom.RoomName,
						ExamBuildingID = examOfferedRoom.ExamBuildingID,
						InvigilatorName1 = invigilatorType1?.InvigilatorName,
						InvigilatorName2 = invigilatorType2?.InvigilatorName,
					};
				}).ToList();

				if (sorted)
					invigilatorDuties = invigilatorDuties.Select(i => new[]
					{
						new InvigilationDuty
						{
							ExamOfferedRoomID = i.ExamOfferedRoomID,
							ExamBuildingID = i.ExamBuildingID,
							BuildingName = i.BuildingName,
							RoomName = i.RoomName,
							InvigilatorName1 = i.InvigilatorName1,
							Sequence = i.Sequence,
							Students = i.Students,
						},
						new InvigilationDuty
						{
							ExamOfferedRoomID = i.ExamOfferedRoomID,
							ExamBuildingID = i.ExamBuildingID,
							BuildingName = i.BuildingName,
							RoomName = i.RoomName,
							InvigilatorName1 = i.InvigilatorName2,
							Sequence = i.Sequence,
							Students = i.Students,
						},
					}).SelectMany(i => i).Where(i => i.InvigilatorName1 != null).OrderBy(i => i.InvigilatorName1).ToList();

				return new ReportDataset { PageHeader = pageHeader, InvigilationDuties = invigilatorDuties };
			}
		}
	}
}
