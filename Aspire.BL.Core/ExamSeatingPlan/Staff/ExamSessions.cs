using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff
{
	public static class ExamSessions
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.ManageExamSessions, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class ExamSession
		{
			internal ExamSession()
			{
			}

			public int ExamSessionID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public byte ExamType { get; internal set; }
			public string ExamTypeFullName => ((ExamConduct.ExamTypes)this.ExamType).ToFullName();
			public string ExamConductName { get; internal set; }
			public string SessionName { get; internal set; }
			public TimeSpan StartTime { get; internal set; }
			public string StartTimeString
			{
				get
				{
					if (this.StartTime.Hours > 12)
						return $"{this.StartTime.Add(TimeSpan.FromHours(-12)):hh\\:mm} PM";
					return $"{this.StartTime:hh\\:mm} AM";
				}
			}
			public byte TotalTimeInMinutes { get; internal set; }
			public string Timing => $"{this.StartTime:hh\\:mm} - {this.StartTime.Add(TimeSpan.FromMinutes(this.TotalTimeInMinutes)):hh\\:mm}";
			public byte Shift { get; internal set; }
			public string ShiftFullName => ((Shifts)this.Shift).ToFullName();
			public short InvigilationPayRate { get; internal set; }
		}

		public static List<ExamSession> GetExamSessions(int examConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var instituteID = loginHistory.InstituteID;
				var examSessions = aspireContext.ExamSessions.Where(es => es.ExamConductID == examConductID && es.ExamConduct.InstituteID == instituteID).OrderBy(es => es.Sequence);
				return examSessions.Select(es => new ExamSession
				{
					ExamSessionID = es.ExamSessionID,
					SemesterID = es.ExamConduct.SemesterID,
					ExamType = es.ExamConduct.ExamType,
					ExamConductName = es.ExamConduct.Name,
					SessionName = es.SessionName,
					StartTime = es.StartTime,
					TotalTimeInMinutes = es.TotalTimeInMinutes,
					Shift = es.Shift,
					InvigilationPayRate = es.InvigilationPayRate,
				}).ToList();
			}
		}

		public static Model.Entities.ExamSession GetExamSession(int examSessionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.ExamSessions.Include(es => es.ExamConduct).SingleOrDefault(es => es.ExamSessionID == examSessionID && es.ExamConduct.InstituteID == loginHistory.InstituteID);
			}
		}

		public enum AddExamSessionStatuses
		{
			Success,
			NameAlreadyExists,
		}

		public static void ReOrderExamSessions(this AspireContext aspireContext, int examConductID, int userLoginHistoryID)
		{
			byte sequence = 0;
			foreach (var examSession in aspireContext.ExamSessions.Where(ec => ec.ExamConductID == examConductID).OrderBy(ec => ec.Sequence))
				examSession.Sequence = sequence++;
			aspireContext.SaveChanges(userLoginHistoryID);
		}

		public static AddExamSessionStatuses AddExamSession(int examConductID, string sessionName, TimeSpan startTime, byte totalTime, short payRate, Shifts shiftEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var instituteID = loginHistory.InstituteID;
				sessionName = sessionName.TrimAndCannotBeEmpty();
				if (aspireContext.ExamSessions.Any(es => es.ExamConductID == examConductID && es.SessionName == sessionName))
					return AddExamSessionStatuses.NameAlreadyExists;

				var examSession = new Model.Entities.ExamSession
				{
					ExamConductID = aspireContext.ExamConducts.Where(ec => ec.ExamConductID == examConductID && ec.InstituteID == instituteID).Select(ec => ec.ExamConductID).Single(),
					SessionName = sessionName,
					StartTime = startTime,
					TotalTimeInMinutes = totalTime,
					InvigilationPayRate = payRate,
					ShiftEnum = shiftEnum,
					Sequence = byte.MaxValue,
				};

				aspireContext.ExamSessions.Add(examSession);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ReOrderExamSessions(examSession.ExamConductID, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return AddExamSessionStatuses.Success;
			}
		}

		public enum UpdateExamSessionStatuses
		{
			NoRecordFound,
			Success,
			NameAlreadyExists
		}

		public static UpdateExamSessionStatuses UpdateExamSession(int examSessionID, string sessionName, TimeSpan startTime, byte totalTime, short payRate, Shifts shiftEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				sessionName = sessionName.TrimAndCannotBeEmpty();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var instituteID = loginHistory.InstituteID;
				var examSession = aspireContext.ExamSessions.SingleOrDefault(es => es.ExamSessionID == examSessionID && es.ExamConduct.InstituteID == instituteID);
				if (examSession == null)
					return UpdateExamSessionStatuses.NoRecordFound;
				var alreadyExists = aspireContext.ExamSessions.Any(es => es.ExamSessionID != examSessionID && es.ExamConductID == examSession.ExamConductID && es.SessionName == sessionName);
				if (alreadyExists)
					return UpdateExamSessionStatuses.NameAlreadyExists;
				examSession.TotalTimeInMinutes = totalTime;
				examSession.SessionName = sessionName;
				examSession.StartTime = startTime;
				examSession.ShiftEnum = shiftEnum;
				examSession.InvigilationPayRate = payRate;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateExamSessionStatuses.Success;
			}
		}

		public enum DeleteExamSessionStatuses
		{
			NoRecordFound,
			Success,
			ChildRecordExistsExamDateAndSessions
		}

		public static DeleteExamSessionStatuses DeleteExamSession(int examSessionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var instituteID = loginHistory.InstituteID;
				var examSession = aspireContext.ExamSessions.FirstOrDefault(es => es.ExamSessionID == examSessionID && es.ExamConduct.InstituteID == instituteID);
				if (examSession == null)
					return DeleteExamSessionStatuses.NoRecordFound;
				if (examSession.ExamDateAndSessions.Any())
					return DeleteExamSessionStatuses.ChildRecordExistsExamDateAndSessions;
				var examConductID = examSession.ExamConductID;
				aspireContext.ExamSessions.Remove(examSession);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ReOrderExamSessions(examConductID, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return DeleteExamSessionStatuses.Success;
			}
		}

		public enum MoveExamSessionStatuses
		{
			NoRecordFound,
			AlreadyAtTop,
			AlreadyAtBottom,
			Success,
		}

		public static MoveExamSessionStatuses MoveExamSessionUp(int examSessionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var instituteID = loginHistory.InstituteID;
				var examSession = aspireContext.ExamSessions.SingleOrDefault(es => es.ExamSessionID == examSessionID && es.ExamConduct.InstituteID == instituteID);
				if (examSession == null)
					return MoveExamSessionStatuses.NoRecordFound;
				if (examSession.Sequence == 0)
					return MoveExamSessionStatuses.AlreadyAtTop;

				var upperExamSessions = aspireContext.ExamSessions.SingleOrDefault(es => es.ExamConductID == examSession.ExamConductID && es.Sequence == examSession.Sequence - 1);
				if (upperExamSessions == null)
					return MoveExamSessionStatuses.NoRecordFound;
				examSession.Sequence--;
				upperExamSessions.Sequence++;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return MoveExamSessionStatuses.Success;
			}
		}

		public static MoveExamSessionStatuses MoveExamSessionDown(int examSessionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var instituteID = loginHistory.InstituteID;
				var examSession = aspireContext.ExamSessions.SingleOrDefault(es => es.ExamSessionID == examSessionID && es.ExamConduct.InstituteID == instituteID);
				if (examSession == null)
					return MoveExamSessionStatuses.NoRecordFound;
				if (examSession.Sequence == byte.MaxValue)
					return MoveExamSessionStatuses.AlreadyAtBottom;

				var upperExamSessions = aspireContext.ExamSessions.SingleOrDefault(es => es.ExamConductID == examSession.ExamConductID && es.Sequence == examSession.Sequence + 1);
				if (upperExamSessions == null)
					return MoveExamSessionStatuses.AlreadyAtBottom;
				examSession.Sequence++;
				upperExamSessions.Sequence--;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return MoveExamSessionStatuses.Success;
			}
		}

		public static List<CustomListItem> GetExamSessionsList(int examConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var instituteID = loginHistory.InstituteID;
				return aspireContext.ExamSessions.Where(es => es.ExamConductID == examConductID && es.ExamConduct.InstituteID == instituteID).OrderBy(es => es.Sequence).Select(es => new CustomListItem
				{
					ID = es.ExamSessionID,
					Text = es.SessionName,
				}).ToList();
			}
		}
	}
}