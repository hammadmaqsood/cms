﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff
{
	public static class ExamDateSheets
	{
		public sealed class ExamDateSheet
		{
			internal ExamDateSheet()
			{
			}

			public int OfferedCourseID { get; internal set; }
			public string Title { get; internal set; }
			public CourseCategories CourseCategoryEnum { get; internal set; }
			public string Teacher { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public SemesterNos SemesterNoEnum { get; internal set; }
			public Sections SectionEnum { get; internal set; }
			public Shifts ShiftEnum { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNoEnum, this.SectionEnum, this.ShiftEnum);
			public List<Student> Students { get; internal set; }
			public DateSheet ExDateSheet { get; internal set; }
			public int RegisteredStudents { get; internal set; }
			public int Rooms { get; internal set; }
			public int Seats { get; internal set; }
			public int StudentsHavingClash { get; internal set; }

			public sealed class DateSheet
			{
				internal DateSheet() { }
				public int ExamDateSheetID { get; internal set; }
				public int ExamDateAndSessionID { get; internal set; }
				public DateTime Date { get; internal set; }
				public string SessionName { get; internal set; }
				public string ExamDateAndSession => AspireFormats.GetExamDateAndSessionString(this.Date, this.SessionName);
				public DateTime? QuestionPaperReceivedDate { get; internal set; }
				public DateTime? AnswerSheetsReceivedByExaminerDate { get; internal set; }
				public int? AnswerSheetsReceivedByExaminerCount { get; internal set; }
				public DateTime? AnswerSheetsReceivedByExamCellDate { get; internal set; }
				public int? AnswerSheetsReceivedByExamCellCount { get; internal set; }
			}

			public sealed class Student
			{
				internal Student() { }
				public int RegisteredCourseID { get; internal set; }
				public int? ExamOfferedRoomID { get; internal set; }
			}
		}

		public static List<ExamDateSheet> GetExamDateSheets(int examConductID, bool calculateClashes, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int? facultyMemberID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
				if (loginHistory == null)
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.Module, UserGroupPermission.PermissionValues.Allowed);
				var examConduct = aspireContext.ExamConducts.SingleOrDefault(oc => oc.ExamConductID == examConductID && oc.InstituteID == loginHistory.InstituteID);
				if (examConduct == null)
					return null;

				var offeredCoursesQuery = aspireContext.OfferedCourses
					.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(oc => oc.SemesterID == examConduct.SemesterID);

				if (facultyMemberID == null)
				{
					if (departmentID != null)
						offeredCoursesQuery = offeredCoursesQuery.Where(oc => oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
					if (programID != null)
						offeredCoursesQuery = offeredCoursesQuery.Where(oc => oc.Cours.AdmissionOpenProgram.ProgramID == programID.Value);
					if (semesterNoEnum != null)
						offeredCoursesQuery = offeredCoursesQuery.Where(oc => oc.SemesterNo == (short)semesterNoEnum.Value);
					if (sectionEnum != null)
						offeredCoursesQuery = offeredCoursesQuery.Where(oc => oc.Section == (int)sectionEnum.Value);
					if (shiftEnum != null)
						offeredCoursesQuery = offeredCoursesQuery.Where(oc => oc.Shift == (byte)shiftEnum.Value);
				}
				else
					offeredCoursesQuery = offeredCoursesQuery.Where(oc => oc.FacultyMemberID == facultyMemberID.Value);

				var validRegisteredCourses = aspireContext.RegisteredCourses.FilterValidRegisteredCoursesForSeatingPlan();

				var examDateSheets = offeredCoursesQuery.Select(oc => new ExamDateSheet
				{
					OfferedCourseID = oc.OfferedCourseID,
					ProgramAlias = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNoEnum = (SemesterNos)oc.SemesterNo,
					SectionEnum = (Sections)oc.Section,
					ShiftEnum = (Shifts)oc.Shift,
					Title = oc.Cours.Title,
					CourseCategoryEnum = (CourseCategories)oc.Cours.CourseCategory,
					Teacher = oc.FacultyMember.Name,
					ExDateSheet = oc.ExamDateSheets.Where(eds => eds.ExamDateAndSession.ExamConduct.ExamType == examConduct.ExamType).Select(eds => new ExamDateSheet.DateSheet
					{
						ExamDateSheetID = eds.ExamDateSheetID,
						ExamDateAndSessionID = eds.ExamDateAndSessionID,
						Date = eds.ExamDateAndSession.ExamDate.Date,
						SessionName = eds.ExamDateAndSession.ExamSession.SessionName,
						QuestionPaperReceivedDate = eds.QuestionPaperReceivedDate,
						AnswerSheetsReceivedByExaminerDate = eds.AnswerSheetsReceivedByExaminerDate,
						AnswerSheetsReceivedByExaminerCount = eds.AnswerSheetsReceivedByExaminerCount,
						AnswerSheetsReceivedByExamCellDate = eds.AnswerSheetsReceivedByExamCellDate,
						AnswerSheetsReceivedByExamCellCount = eds.AnswerSheetsReceivedByExamCellCount,
					}).FirstOrDefault(),
					Students = validRegisteredCourses.Where(rc => rc.OfferedCourseID == oc.OfferedCourseID).Select(rc => new ExamDateSheet.Student
					{
						RegisteredCourseID = rc.RegisteredCourseID,
						ExamOfferedRoomID = rc.ExamSeats.Where(es => es.ExamOfferedRoom.ExamConductID == examConduct.ExamConductID).Select(es => (int?)es.ExamOfferedRoomID).FirstOrDefault()
					}).ToList(),
					RegisteredStudents = 0,
					Rooms = 0,
					Seats = 0,
					StudentsHavingClash = 0
				}).ToList();

				examDateSheets.ForEach(eds =>
				{
					eds.RegisteredStudents = eds.Students.Count();
					eds.Rooms = eds.Students.Select(s => s.ExamOfferedRoomID).Where(r => r != null).Distinct().Count();
					eds.Seats = eds.Students.Count(s => s.ExamOfferedRoomID != null);
				});

				if (calculateClashes)
				{
					var registeredCoursesForClashesQuery = offeredCoursesQuery.SelectMany(oc => oc.RegisteredCourses)
						.Select(rc => rc.Student)
						.SelectMany(rc => rc.RegisteredCourses)
						.FilterValidRegisteredCoursesForSeatingPlan()
						.Select(rc => new
						{
							rc.StudentID,
							rc.OfferedCourseID
						});

					var examDateSheetsForClashesQuery = aspireContext.ExamDateSheets
						.Where(eds => eds.ExamDateAndSession.ExamConduct.SemesterID == examConduct.SemesterID && eds.ExamDateAndSession.ExamConduct.ExamType == examConduct.ExamType)
						.Select(eds => new
						{
							eds.OfferedCourseID,
							eds.ExamDateAndSessionID
						});

					var allClashesData = (from rc in registeredCoursesForClashesQuery
										  join eds in examDateSheetsForClashesQuery on rc.OfferedCourseID equals eds.OfferedCourseID
										  select new
										  {
											  eds.OfferedCourseID,
											  eds.ExamDateAndSessionID,
											  rc.StudentID
										  }).Distinct().ToList();

					var studentClashes = allClashesData.GroupBy(c => new { c.ExamDateAndSessionID, c.StudentID })
										.Where(g => g.Count() > 1)
										.Select(g => new
										{
											g.Key.StudentID,
											g.Key.ExamDateAndSessionID,
											OfferedCourseIDs = g.Select(gg => gg.OfferedCourseID)
										}).ToList();

					examDateSheets.ForEach(eds =>
					{
						eds.StudentsHavingClash = studentClashes.Count(c => c.OfferedCourseIDs.Contains(eds.OfferedCourseID));
					});
				}

				return examDateSheets;
			}
		}

		public sealed class OfferedCourse
		{
			internal OfferedCourse() { }
			public int OfferedCourseID { get; internal set; }
			public int? ExamDateSheetID { get; internal set; }
			public int? ExamDateAndSessionID { get; internal set; }
			public string Teacher { get; internal set; }
			public short SemesterID { get; internal set; }
			public DateTime? QuestionPaperReceivedDate { get; internal set; }
			public DateTime? AnswerSheetsReceivedByExaminerDate { get; internal set; }
			public int? AnswerSheetsReceivedByExaminerCount { get; internal set; }
			public DateTime? AnswerSheetsReceivedByExamCellDate { get; internal set; }
			public int? AnswerSheetsReceivedByExamCellCount { get; internal set; }
			public string ExamConductName { get; internal set; }
			public byte ExamType { get; internal set; }
			public ExamConduct.ExamTypes ExamTypeEnum => (ExamConduct.ExamTypes)this.ExamType;
			public string ProgramAlias { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public string Title { get; internal set; }
		}

		public static OfferedCourse GetOfferedCourseDetail(int examConductID, int offeredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
				if (loginHistory == null)
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.Module, UserGroupPermission.PermissionValues.Allowed);
				var examConducts = aspireContext.ExamConducts.Where(ec => ec.ExamConductID == examConductID && ec.InstituteID == loginHistory.InstituteID);
				var offeredCourse = examConducts.SelectMany(ec => ec.ExamDateAndSessions).SelectMany(eds => eds.ExamDateSheets).Where(eds => eds.OfferedCourseID == offeredCourseID).Select(eds => new OfferedCourse
				{
					OfferedCourseID = eds.OfferedCourseID,
					Title = eds.OfferedCours.Cours.Title,
					SemesterID = eds.OfferedCours.SemesterID,
					ProgramAlias = eds.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = eds.OfferedCours.SemesterNo,
					Section = eds.OfferedCours.Section,
					Shift = eds.OfferedCours.Shift,
					Teacher = eds.OfferedCours.FacultyMember.Name,
					ExamDateSheetID = eds.ExamDateSheetID,
					ExamDateAndSessionID = eds.ExamDateAndSessionID,
					ExamConductName = eds.ExamDateAndSession.ExamConduct.Name,
					ExamType = eds.ExamDateAndSession.ExamConduct.ExamType,
					QuestionPaperReceivedDate = eds.QuestionPaperReceivedDate,
					AnswerSheetsReceivedByExaminerDate = eds.AnswerSheetsReceivedByExaminerDate,
					AnswerSheetsReceivedByExaminerCount = eds.AnswerSheetsReceivedByExaminerCount,
					AnswerSheetsReceivedByExamCellDate = eds.AnswerSheetsReceivedByExamCellDate,
					AnswerSheetsReceivedByExamCellCount = eds.AnswerSheetsReceivedByExamCellCount,
				}).SingleOrDefault();
				if (offeredCourse != null)
					return offeredCourse;

				return (from ec in examConducts
						from oc in aspireContext.OfferedCourses
						where oc.OfferedCourseID == offeredCourseID && oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID
						select new OfferedCourse
						{
							Title = oc.Cours.Title,
							Teacher = oc.FacultyMember.Name,
							ProgramAlias = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
							SemesterNo = oc.SemesterNo,
							Section = oc.Section,
							Shift = oc.Shift,
							OfferedCourseID = oc.OfferedCourseID,
							SemesterID = oc.SemesterID,
							ExamConductName = ec.Name,
							ExamType = ec.ExamType,
							QuestionPaperReceivedDate = null,
							ExamDateSheetID = null,
							ExamDateAndSessionID = null,
							AnswerSheetsReceivedByExaminerDate = null,
							AnswerSheetsReceivedByExaminerCount = null,
							AnswerSheetsReceivedByExamCellDate = null,
							AnswerSheetsReceivedByExamCellCount = null
						}).SingleOrDefault();
			}
		}

		public enum UpdateExamDateSheetStatuses
		{
			NoRecordFound,
			NoChangeFound,
			CannotUpdateDueToChildRecordExistsExamSeats,
			CannotUpdateDueToQuestionPaperReceived,
			CannotUpdateDueToAnswerSheetsReceivedByExaminer,
			CannotUpdateDueToAnswerSheetsReceivedByExamCell,
			DateSheetExistsInOtherExamConduct,
			InvalidData,
			Success,
		}

		public static UpdateExamDateSheetStatuses UpdateExamDateSheet(int offeredCourseID, bool forCoordinators, int examConductID, int? examDateAndSessionID, DateTime? questionPaperReceivedDate, DateTime? answerSheetsReceivedByExaminerDate, int? answerSheetsReceivedByExaminerCount, DateTime? answerSheetsReceivedByExamCellDate, int? answerSheetsReceivedByExamCellCount, Guid loginSessionGuid)
		{
			if (examDateAndSessionID == null && questionPaperReceivedDate != null)
				throw new ArgumentException("Must be null.", nameof(questionPaperReceivedDate));
			using (var aspireContext = new AspireContext(true))
			{
				var offeredCourse = aspireContext.OfferedCourses
					.Where(oc => oc.OfferedCourseID == offeredCourseID)
					.Select(oc => new
					{
						oc.OfferedCourseID,
						oc.Cours.AdmissionOpenProgram.Program.InstituteID,
						oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
						oc.Cours.AdmissionOpenProgram.ProgramID,
						oc.Cours.AdmissionOpenProgramID,
					})
					.SingleOrDefault();
				if (offeredCourse == null)
					return UpdateExamDateSheetStatuses.NoRecordFound;

				var loginHistory = forCoordinators
					? aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.ManageExamDateSheet, UserGroupPermission.PermissionValues.Allowed, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID)
					: aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.ManageExamDateSheet, UserGroupPermission.PermissionValues.Allowed, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID);

				var examConduct = aspireContext.ExamConducts
					.Where(ec => ec.ExamConductID == examConductID && ec.InstituteID == offeredCourse.InstituteID)
					.Select(ec => new
					{
						ec.ExamConductID,
						ec.SemesterID,
						ExamTypeEnum = (ExamConduct.ExamTypes)ec.ExamType
					})
					.SingleOrDefault();
				if (examConduct == null)
					return UpdateExamDateSheetStatuses.NoRecordFound;

				var examDateSheet = aspireContext.ExamDateSheets.Include(eds => eds.ExamDateAndSession)
					.Where(eds => eds.OfferedCourseID == offeredCourseID)
					.Where(eds => eds.ExamDateAndSession.ExamConduct.SemesterID == examConduct.SemesterID)
					.SingleOrDefault(eds => eds.ExamDateAndSession.ExamConduct.ExamType == (byte)examConduct.ExamTypeEnum);

				if (examDateAndSessionID != null)
				{
					var examDateAndSession = aspireContext.ExamDateAndSessions
						.Where(eds => eds.ExamDateAndSessionID == examDateAndSessionID.Value && eds.ExamConductID == examConductID)
						.Select(eds => new
						{
							eds.ExamDateAndSessionID
						}).SingleOrDefault();
					if (examDateAndSession == null)
						return UpdateExamDateSheetStatuses.NoRecordFound;
				}

				if (examDateSheet == null)
				{
					if (examDateAndSessionID == null)
						return UpdateExamDateSheetStatuses.NoChangeFound;

					examDateSheet = aspireContext.ExamDateSheets.Add(new Model.Entities.ExamDateSheet
					{
						OfferedCourseID = offeredCourseID,
						ExamDateAndSessionID = examDateAndSessionID.Value,
						QuestionPaperReceivedDate = null,
						AnswerSheetsReceivedByExaminerDate = null,
						AnswerSheetsReceivedByExaminerCount = null,
						AnswerSheetsReceivedByExamCellDate = null,
						AnswerSheetsReceivedByExamCellCount = null
					});
					if (!forCoordinators)
					{
						examDateSheet.QuestionPaperReceivedDate = questionPaperReceivedDate;
						examDateSheet.AnswerSheetsReceivedByExaminerDate = answerSheetsReceivedByExaminerDate;
						examDateSheet.AnswerSheetsReceivedByExaminerCount = answerSheetsReceivedByExaminerCount;
						examDateSheet.AnswerSheetsReceivedByExamCellDate = answerSheetsReceivedByExamCellDate;
						examDateSheet.AnswerSheetsReceivedByExamCellCount = answerSheetsReceivedByExamCellCount;
					}
				}
				else
				{
					var examSeatsExists = aspireContext.ExamSeats
						.Where(es => es.RegisteredCours.OfferedCourseID == offeredCourseID)
						.Any(es => es.ExamOfferedRoom.ExamConduct.ExamType == (byte)examConduct.ExamTypeEnum);

					if (examDateAndSessionID == examDateSheet.ExamDateAndSessionID)
					{
						if (forCoordinators)
							return UpdateExamDateSheetStatuses.NoChangeFound;
						if (questionPaperReceivedDate == examDateSheet.QuestionPaperReceivedDate
							&& answerSheetsReceivedByExaminerDate == examDateSheet.AnswerSheetsReceivedByExaminerDate
							&& answerSheetsReceivedByExaminerCount == examDateSheet.AnswerSheetsReceivedByExaminerCount
							&& answerSheetsReceivedByExamCellDate == examDateSheet.AnswerSheetsReceivedByExamCellDate
							&& answerSheetsReceivedByExamCellCount == examDateSheet.AnswerSheetsReceivedByExamCellCount)
							return UpdateExamDateSheetStatuses.NoChangeFound;
						examDateSheet.QuestionPaperReceivedDate = questionPaperReceivedDate;
						examDateSheet.AnswerSheetsReceivedByExaminerDate = answerSheetsReceivedByExaminerDate;
						examDateSheet.AnswerSheetsReceivedByExaminerCount = answerSheetsReceivedByExaminerCount;
						examDateSheet.AnswerSheetsReceivedByExamCellDate = answerSheetsReceivedByExamCellDate;
						examDateSheet.AnswerSheetsReceivedByExamCellCount = answerSheetsReceivedByExamCellCount;
					}
					else
					{
						if (examSeatsExists)
							return UpdateExamDateSheetStatuses.CannotUpdateDueToChildRecordExistsExamSeats;
						if (examDateSheet.QuestionPaperReceivedDate != null)
							return UpdateExamDateSheetStatuses.CannotUpdateDueToQuestionPaperReceived;
						if (examDateSheet.AnswerSheetsReceivedByExaminerDate != null || examDateSheet.AnswerSheetsReceivedByExaminerCount != null)
							return UpdateExamDateSheetStatuses.CannotUpdateDueToAnswerSheetsReceivedByExaminer;
						if (examDateSheet.AnswerSheetsReceivedByExamCellDate != null || examDateSheet.AnswerSheetsReceivedByExamCellCount != null)
							return UpdateExamDateSheetStatuses.CannotUpdateDueToAnswerSheetsReceivedByExamCell;
						if (examDateAndSessionID == null)
							aspireContext.ExamDateSheets.Remove(examDateSheet);
						else
						{
							examDateSheet.ExamDateAndSessionID = examDateAndSessionID.Value;
							examDateSheet.QuestionPaperReceivedDate = questionPaperReceivedDate;
							examDateSheet.AnswerSheetsReceivedByExaminerDate = answerSheetsReceivedByExaminerDate;
							examDateSheet.AnswerSheetsReceivedByExaminerCount = answerSheetsReceivedByExaminerCount;
							examDateSheet.AnswerSheetsReceivedByExamCellDate = answerSheetsReceivedByExamCellDate;
							examDateSheet.AnswerSheetsReceivedByExamCellCount = answerSheetsReceivedByExamCellCount;
						}
					}
				}
				if (examDateAndSessionID != null)
				{
					var validationResult = examDateSheet.Validate();
					switch (validationResult)
					{
						case Model.Entities.ExamDateSheet.ValidationResult.Success:
							break;
						case Model.Entities.ExamDateSheet.ValidationResult.InvalidData:
							return UpdateExamDateSheetStatuses.InvalidData;
						default:
							throw new NotImplementedEnumException(validationResult);
					}
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateExamDateSheetStatuses.Success;
			}
		}

		public sealed class RegisteredCourse
		{
			internal RegisteredCourse() { }

			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public int Section { get; internal set; }
			public short SemesterNo { get; internal set; }
			public byte Shift { get; internal set; }
			public string Title { get; internal set; }
			public string Teacher { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public int ExamDateAndSessionID { get; internal set; }
			public DateTime Date { get; internal set; }
			public string SessionName { get; internal set; }
			public byte Sequence { get; internal set; }
			public int StudentID { get; internal set; }
			public int OfferedCourseID { get; internal set; }
		}

		public sealed class GetClashesResult
		{
			internal GetClashesResult() { }
			public int VirtualItemCount { get; internal set; }
			public List<RegisteredCourse> RegisteredCourses { get; internal set; }
		}

		public static GetClashesResult GetClashes(int examConductID, int? offeredCourseID, int clashes, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.ManageExamDateSheet, UserGroupPermission.PermissionValues.Allowed)
									?? aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.ManageExamDateSheet, UserGroupPermission.PermissionValues.Allowed);

				var registeredCourses = aspireContext.GetValidRegisteredCourses(examConductID, null);
				var examDateSheets = aspireContext.ExamDateSheets.Where(eds => eds.ExamDateAndSession.ExamConductID == examConductID && eds.ExamDateAndSession.ExamConduct.InstituteID == loginHistory.InstituteID);

				if (offeredCourseID != null)
				{
					var examDateSheet = aspireContext.ExamDateSheets.Where(eds => eds.OfferedCourseID == offeredCourseID.Value && eds.ExamDateAndSession.ExamConductID == examConductID).Select(eds => new
					{
						eds.ExamDateAndSession.ExamDateAndSessionID
					}).SingleOrDefault();
					if (examDateSheet == null)
						return null;
					examDateSheets = examDateSheets.Where(eds => eds.ExamDateAndSessionID == examDateSheet.ExamDateAndSessionID);

					var studentIDs = aspireContext.RegisteredCourses.Where(rc => rc.OfferedCourseID == offeredCourseID.Value && rc.DeletedDate == null).Select(rc => rc.StudentID);
					registeredCourses = registeredCourses.Where(p => studentIDs.Contains(p.StudentID) && p.OfferedCourseID != offeredCourseID.Value);
					clashes--;
				}

				var studentPapers = from rc in registeredCourses
									join eds in examDateSheets on rc.OfferedCourseID equals eds.OfferedCourseID
									select new RegisteredCourse
									{
										StudentID = rc.StudentID,
										Enrollment = rc.Student.Enrollment,
										OfferedCourseID = rc.OfferedCourseID,
										Name = rc.Student.Name,
										ProgramAlias = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
										SemesterNo = rc.OfferedCours.SemesterNo,
										Section = rc.OfferedCours.Section,
										Shift = rc.OfferedCours.Shift,
										Title = rc.OfferedCours.Cours.Title,
										Teacher = rc.OfferedCours.FacultyMember.Name,
										ExamDateAndSessionID = eds.ExamDateAndSessionID,
										Date = eds.ExamDateAndSession.ExamDate.Date,
										SessionName = eds.ExamDateAndSession.ExamSession.SessionName,
										Sequence = eds.ExamDateAndSession.ExamSession.Sequence,
									};

				var multiplePapers = studentPapers.GroupBy(p => new { p.StudentID, p.ExamDateAndSessionID }).Where(p => p.Count() >= clashes).SelectMany(p => p.ToList());

				var virtualItemCount = multiplePapers.Count();

				switch (sortExpression)
				{
					case nameof(RegisteredCourse.Class):
						multiplePapers = multiplePapers.OrderBy(sortDirection, p => p.ProgramAlias).ThenBy(sortDirection, p => p.SemesterNo).ThenBy(sortDirection, p => p.Section).ThenBy(sortDirection, p => p.Shift);
						break;
					case nameof(RegisteredCourse.Enrollment):
						multiplePapers = multiplePapers.OrderBy(sortDirection, p => p.Enrollment);
						break;
					case nameof(RegisteredCourse.Name):
						multiplePapers = multiplePapers.OrderBy(sortDirection, p => p.Name);
						break;
					case nameof(RegisteredCourse.Date):
						multiplePapers = multiplePapers.OrderBy(sortDirection, p => p.Date);
						break;
					case nameof(RegisteredCourse.SessionName):
						multiplePapers = multiplePapers.OrderBy(sortDirection, p => p.Sequence);
						break;
					case nameof(RegisteredCourse.Teacher):
						multiplePapers = multiplePapers.OrderBy(sortDirection, p => p.Teacher);
						break;
					case nameof(RegisteredCourse.Title):
						multiplePapers = multiplePapers.OrderBy(sortDirection, p => p.Title);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}

				return new GetClashesResult
				{
					VirtualItemCount = virtualItemCount,
					RegisteredCourses = multiplePapers.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
				};
			}
		}
	}
}
