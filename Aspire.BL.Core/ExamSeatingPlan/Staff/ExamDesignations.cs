﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff
{
	public static class ExamDesignations
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.ManageExamDesignations, UserGroupPermission.PermissionValues.Allowed);
		}

		public static Model.Entities.ExamDesignation GetExamDesignation(int examDesignationID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.ExamDesignations.SingleOrDefault(eds => eds.ExamDesignationID == examDesignationID && eds.InstituteID == loginHistory.InstituteID);
			}
		}

		public static List<Model.Entities.ExamDesignation> GetExamDesignations(string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));

			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var designations = aspireContext.ExamDesignations.Where(eds => eds.InstituteID == loginHistory.InstituteID);
				switch (sortExpression)
				{
					case nameof(ExamDesignation.Designation):
						designations = sortDirection == SortDirection.Ascending ? designations.OrderBy(eds => eds.Designation) : designations.OrderByDescending(eds => eds.Designation);
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(sortExpression));
				}
				return designations.ToList();
			}
		}

		public sealed class AddExamDesignationResult
		{
			public enum Statuses
			{
				NameAlreadyExists,
				Success
			}

			internal AddExamDesignationResult()
			{
			}

			public Statuses Status { get; internal set; }
			public ExamDesignation ExamDesignation { get; internal set; }
		}

		public static AddExamDesignationResult AddExamDesignation(string designation, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var examDesignation = new ExamDesignation
				{
					InstituteID = loginHistory.InstituteID,
					Designation = designation,
				}.Validate();
				if (aspireContext.ExamDesignations.Any(eds => eds.Designation == examDesignation.Designation && eds.InstituteID == examDesignation.InstituteID))
					return new AddExamDesignationResult { Status = AddExamDesignationResult.Statuses.NameAlreadyExists };
				aspireContext.ExamDesignations.Add(examDesignation);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return new AddExamDesignationResult { Status = AddExamDesignationResult.Statuses.Success, ExamDesignation = examDesignation };
			}
		}

		public enum UpdateExamDesignationStatuses
		{
			Success,
			RecordNotFound,
			ChildRecordsExistExamInvigilators,
			NameAlreadyExists
		}

		public static UpdateExamDesignationStatuses UpdateExamDesignation(int examDesignationID, string designation, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var examDesignation = aspireContext.ExamDesignations.SingleOrDefault(ed => ed.ExamDesignationID == examDesignationID && ed.InstituteID == loginHistory.InstituteID);
				if (examDesignation == null)
					return UpdateExamDesignationStatuses.RecordNotFound;
				if (examDesignation.ExamInvigilators.Any())
					return UpdateExamDesignationStatuses.ChildRecordsExistExamInvigilators;

				examDesignation.Designation = designation;
				examDesignation.Validate();
				if (aspireContext.ExamDesignations.Any(eds => eds.ExamDesignationID != examDesignation.ExamDesignationID && eds.Designation == examDesignation.Designation && eds.InstituteID == examDesignation.InstituteID))
					return UpdateExamDesignationStatuses.NameAlreadyExists;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateExamDesignationStatuses.Success;
			}
		}

		public enum DeleteExamDesignationStatuses
		{
			Success,
			RecordNotFound,
			ChildRecordsExistExamInvigilators,
		}

		public static DeleteExamDesignationStatuses DeleteExamDesignation(int examDesignationID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var examDesignation = aspireContext.ExamDesignations.SingleOrDefault(ed => ed.ExamDesignationID == examDesignationID && ed.InstituteID == loginHistory.InstituteID);
				if (examDesignation == null)
					return DeleteExamDesignationStatuses.RecordNotFound;
				if (examDesignation.ExamInvigilators.Any())
					return DeleteExamDesignationStatuses.ChildRecordsExistExamInvigilators;

				aspireContext.ExamDesignations.Remove(examDesignation);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteExamDesignationStatuses.Success;
			}
		}

		public static List<CustomListItem> GetExamDesignationsList(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.ExamDesignations.Where(eds => eds.InstituteID == loginHistory.InstituteID).Select(eds => new CustomListItem
				{
					ID = eds.ExamDesignationID,
					Text = eds.Designation,
				}).OrderBy(eds => eds.Text).ToList();
			}
		}
	}
}