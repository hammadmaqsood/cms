﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff
{
	public static class DeleteExamSeats
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.DeleteSeat, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class StudentInfo
		{
			internal StudentInfo() { }
			public string Enrollment { get; internal set; }
			public int StudentID { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string Name { get; internal set; }
			public string FatherName { get; internal set; }
			public byte? StudentStatus { get; internal set; }
			public DateTime? StudentStatusDate { get; internal set; }
			public string StudentStatusFullName => (((Model.Entities.Student.Statuses?)this.StudentStatus)?.ToFullName()).ToNAIfNullOrEmpty();
			public byte? SemesterFreezedStatus { get; internal set; }
			public DateTime? SemesterFreezedDate { get; internal set; }
			public string SemesterFreezedStatusFullName => ((FreezedStatuses?)this.SemesterFreezedStatus)?.ToFullName().ToNAIfNullOrEmpty();
			public short SemesterID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short? SemesterNo { get; internal set; }
			public int? Section { get; internal set; }
			public byte? Shift { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
		}

		public sealed class Seat
		{
			internal Seat() { }
			public int? ExamSeatID { get; internal set; }
			public byte? FeeStatus { get; internal set; }
			public string Title { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public string SessionName { get; internal set; }
			public DateTime? ExamDate { get; internal set; }
			public string Session => AspireFormats.GetExamDateAndSessionString(this.ExamDate, this.SessionName);
			public string RoomName { get; internal set; }
			public byte? Row { get; internal set; }
			public byte? Column { get; internal set; }
			public byte? Status { get; internal set; }
			public byte? FreezedStatus { get; internal set; }
			public string CompleteStatus => AspireFormats.GetRegisteredCourseStatusString(this.Status, this.FreezedStatus).ToNAIfNullOrEmpty();
			public int RegisteredCourseID { get; internal set; }
			public int OfferedCourseID { get; internal set; }

			public string FeeStatusFullName => (((StudentFee.Statuses?)this.FeeStatus)?.ToFullName()).ToNAIfNullOrEmpty();
		}

		public sealed class GetStudentExamSeatInfoResult
		{
			internal GetStudentExamSeatInfoResult() { }
			public StudentInfo StudentInfo { get; internal set; }
			public List<Seat> Seats { get; internal set; }
		}

		public static GetStudentExamSeatInfoResult GetStudentExamSeatInfo(string enrollment, short semesterID, int? examConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

				var studentInfo = (from s in aspireContext.Students
								   join ss in aspireContext.StudentSemesters on s.StudentID equals ss.StudentID into grp
								   where s.Enrollment == enrollment && s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID
								   from g in grp.DefaultIfEmpty()
								   where g.SemesterID == semesterID
								   select new StudentInfo
								   {
									   StudentID = s.StudentID,
									   Enrollment = s.Enrollment,
									   RegistrationNo = s.RegistrationNo,
									   Name = s.Name,
									   FatherName = s.FatherName,
									   ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
									   StudentStatus = s.Status,
									   StudentStatusDate = s.StatusDate,
									   SemesterID = semesterID,
									   SemesterNo = g == null ? (short?)null : g.SemesterNo,
									   Section = g == null ? (int?)null : g.Section,
									   Shift = g == null ? (byte?)null : g.Shift,
									   SemesterFreezedDate = g == null ? null : g.FreezedDate,
									   SemesterFreezedStatus = g == null ? (byte?)null : g.Status,
								   }).SingleOrDefault();
				if (studentInfo == null)
					return null;

				var seats = aspireContext.RegisteredCourses.Where(rc => rc.StudentID == studentInfo.StudentID && rc.OfferedCours.SemesterID == studentInfo.SemesterID).Select(rc => new Seat
				{
					RegisteredCourseID = rc.RegisteredCourseID,
					OfferedCourseID = rc.OfferedCourseID,
					ProgramAlias = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					Section = rc.OfferedCours.Section,
					Shift = rc.OfferedCours.Shift,
					SemesterNo = rc.OfferedCours.SemesterNo,
					Title = rc.OfferedCours.Cours.Title,
					FeeStatus = rc.StudentFee.Status,
					FreezedStatus = rc.FreezedStatus,
					Status = rc.Status,
					RoomName = rc.ExamSeats.Where(es => es.ExamOfferedRoom.ExamConductID == examConductID).Select(es => es.ExamOfferedRoom.ExamRoom.RoomName).FirstOrDefault(),
					Row = rc.ExamSeats.Where(es => es.ExamOfferedRoom.ExamConductID == examConductID).Select(es => (byte?)es.Row).FirstOrDefault(),
					Column = rc.ExamSeats.Where(es => es.ExamOfferedRoom.ExamConductID == examConductID).Select(es => (byte?)es.Column).FirstOrDefault(),
					ExamSeatID = rc.ExamSeats.Where(es => es.ExamOfferedRoom.ExamConductID == examConductID).Select(es => es.ExamSeatID).FirstOrDefault(),
				}).ToList();

				var offeredCourseIDs = seats.Select(s => s.OfferedCourseID).ToList();

				if (examConductID != null)
				{
					var dateSheets = aspireContext.ExamDateSheets
						.Where(eds => eds.ExamDateAndSession.ExamConduct.SemesterID == semesterID && eds.ExamDateAndSession.ExamConductID == examConductID && eds.ExamDateAndSession.ExamConduct.InstituteID == loginHistory.InstituteID)
						.Where(eds => offeredCourseIDs.Contains(eds.OfferedCourseID))
						.Select(eds => new
						{
							eds.OfferedCourseID,
							eds.ExamDateAndSession.ExamDate.Date,
							eds.ExamDateAndSession.ExamSession.SessionName,
						}).ToList();
					foreach (var dateSheet in dateSheets)
						seats.Where(s => s.OfferedCourseID == dateSheet.OfferedCourseID).ForEach(s =>
						{
							s.ExamDate = dateSheet.Date;
							s.SessionName = dateSheet.SessionName;
						});
				}

				return new GetStudentExamSeatInfoResult
				{
					StudentInfo = studentInfo,
					Seats = seats,
				};
			}
		}

		public static ExecuteSeatingPlan.DeleteExamSeatStatuses DeleteExamSeat(int examSeatID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var status = aspireContext.DeleteExamSeat(examSeatID, loginHistory);
				switch (status)
				{
					case ExecuteSeatingPlan.DeleteExamSeatStatuses.NoRecordFound:
						aspireContext.RollbackTransaction();
						break;
					case ExecuteSeatingPlan.DeleteExamSeatStatuses.Success:
						aspireContext.CommitTransaction();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				return status;
			}
		}
	}
}
