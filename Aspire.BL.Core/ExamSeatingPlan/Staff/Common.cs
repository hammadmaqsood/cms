﻿using Aspire.BL.Core.ExamSeatingPlan.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff
{
	internal static class Common
	{
		internal static IQueryable<RegisteredCours> GetRegisteredCourses(this AspireContext aspireContext, int examConductID, int? examDateAndSessionID)
		{
			return aspireContext.GetExamDateSheets(examConductID, examDateAndSessionID)
				.SelectMany(eds => eds.OfferedCours.RegisteredCourses)
				.Where(rc => rc.DeletedDate == null);
		}

		internal static IQueryable<RegisteredCours> FilterValidRegisteredCoursesForSeatingPlan(this IQueryable<RegisteredCours> registeredCourses)
		{
			var validStatusesForSeatingPlan = new[]
			{
				Student.Statuses.Deferred,
			}.Cast<byte>().ToList();
			var validRegisteredCourseStatuses = new[]
			{
				RegisteredCours.Statuses.Deferred,
			}.Cast<byte>().ToList();
			return registeredCourses
				.Where(rc => rc.DeletedDate == null)
				.Where(rc => (rc.Status == null || validRegisteredCourseStatuses.Contains(rc.Status.Value))
							 && (rc.FreezedStatus == null)
							 && (rc.Student.Status == null || validStatusesForSeatingPlan.Contains(rc.Student.Status.Value)));
		}

		internal static IQueryable<RegisteredCours> GetValidRegisteredCourses(this AspireContext aspireContext, int examConductID, int? examDateAndSessionID)
		{
			return aspireContext.GetRegisteredCourses(examConductID, examDateAndSessionID)
				.FilterValidRegisteredCoursesForSeatingPlan();
		}

		internal static IQueryable<RegisteredCours> GetValidRegisteredCourses(this AspireContext aspireContext, short semesterID, int instituteID)
		{
			return aspireContext.OfferedCourses
				.Where(oc => oc.SemesterID == semesterID && oc.Cours.AdmissionOpenProgram.Program.InstituteID == instituteID)
				.SelectMany(oc => oc.RegisteredCourses)
				.Where(rc => rc.DeletedDate == null)
				.FilterValidRegisteredCoursesForSeatingPlan();
		}

		internal static IQueryable<ExamSeat> GetExamSeats(this IQueryable<RegisteredCours> registeredCourses, int examConductID, ExamOfferedRoom.RoomUsageTypes? roomUsageTypeEnum)
		{
			var examSeats = registeredCourses.SelectMany(rc => rc.ExamSeats).Where(es => es.ExamOfferedRoom.ExamConductID == examConductID);
			if (roomUsageTypeEnum != null)
			{
				var roomUsageType = (byte)roomUsageTypeEnum;
				examSeats = examSeats.Where(es => es.ExamOfferedRoom.RoomUsageType == roomUsageType);
			}
			return examSeats;
		}

		internal static IQueryable<ExamSeat> GetValidExamSeats(this AspireContext aspireContext, int examConductID, int? examDateAndSessionID, ExamOfferedRoom.RoomUsageTypes? roomUsageTypeEnum)
		{
			return aspireContext
				.GetValidRegisteredCourses(examConductID, examDateAndSessionID)
				.FilterValidRegisteredCoursesForSeatingPlan()
				.GetExamSeats(examConductID, roomUsageTypeEnum);
		}

		internal static IQueryable<ExamSeat> GetValidVisibleExamSeats(this AspireContext aspireContext, int examConductID, int? examDateAndSessionID, ExamOfferedRoom.RoomUsageTypes? roomUsageTypeEnum)
		{
			return aspireContext.GetValidRegisteredCourses(examConductID, examDateAndSessionID)
				.GetExamSeats(examConductID, roomUsageTypeEnum)
				.Where(es => es.ExamOfferedRoom.ExamConduct.BlockFeeDefaulters == false || es.RegisteredCours.FeeDefaulter == false);
		}

		internal static IQueryable<ExamSeat> GetExamSeats(this AspireContext aspireContext, int examConductID, int? examDateAndSessionID, ExamOfferedRoom.RoomUsageTypes? roomUsageTypeEnum)
		{
			return aspireContext.GetRegisteredCourses(examConductID, examDateAndSessionID).GetExamSeats(examConductID, roomUsageTypeEnum);
		}

		internal static IQueryable<ExamOfferedRoom> GetExamOfferedRooms(this AspireContext aspireContext, int examConductID, ExamOfferedRoom.RoomUsageTypes? roomUsageTypeEnum)
		{
			var examOfferedRooms = aspireContext.ExamOfferedRooms.Where(eor => eor.ExamConductID == examConductID);

			if (roomUsageTypeEnum != null)
			{
				var roomUsageType = (byte)roomUsageTypeEnum;
				examOfferedRooms = examOfferedRooms.Where(eor => eor.RoomUsageType == roomUsageType);
			}
			return examOfferedRooms;
		}
	}
}
