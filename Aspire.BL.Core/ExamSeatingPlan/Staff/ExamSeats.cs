﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff
{
	public static class ExamSeats
	{
		private static BL.Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class ExtraExamSeat
		{
			internal ExtraExamSeat() { }

			public string Enrollment { get; internal set; }
			public DateTime ExamDate { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string SessionName { get; internal set; }
			public int StudentID { get; internal set; }
			public string Title { get; internal set; }
		}

		public sealed class GetExtraSeatsResult
		{
			public int VirtualItemCount { get; internal set; }
			public List<ExtraExamSeat> ExtraExamSeats { get; internal set; }
		}

		public static GetExtraSeatsResult GetExtraSeats(int examConductID, string sortExpression, SortDirection sortDirection, int pageIndex, int pageSize, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				if (!aspireContext.ExamConducts.Any(c => c.ExamConductID == examConductID && c.InstituteID == loginHistory.InstituteID))
					return null;

				var extraExamSeats = aspireContext.GetExamSeats(examConductID, null, null)
					.Except(aspireContext.GetValidExamSeats(examConductID, null, null))
					.Select(es => new ExtraExamSeat
					{
						StudentID = es.RegisteredCours.Student.StudentID,
						Enrollment = es.RegisteredCours.Student.Enrollment,
						Name = es.RegisteredCours.Student.Name,
						ProgramAlias = es.RegisteredCours.Student.AdmissionOpenProgram.Program.ProgramAlias,
						Title = es.RegisteredCours.OfferedCours.Cours.Title,
						ExamDate = es.RegisteredCours.OfferedCours.ExamDateSheets.FirstOrDefault(eds => eds.ExamDateAndSession.ExamConductID == examConductID).ExamDateAndSession.ExamDate.Date,
						SessionName = es.RegisteredCours.OfferedCours.ExamDateSheets.FirstOrDefault(eds => eds.ExamDateAndSession.ExamConductID == examConductID).ExamDateAndSession.ExamSession.SessionName,
					});

				var result = new GetExtraSeatsResult
				{
					VirtualItemCount = extraExamSeats.Count(),
				};

				switch (sortExpression)
				{
					case nameof(ExtraExamSeat.Enrollment):
						extraExamSeats = sortDirection == SortDirection.Ascending ? extraExamSeats.OrderBy(es => es.Enrollment) : extraExamSeats.OrderByDescending(es => es.Enrollment);
						break;
					case nameof(ExtraExamSeat.Name):
						extraExamSeats = sortDirection == SortDirection.Ascending ? extraExamSeats.OrderBy(es => es.Name) : extraExamSeats.OrderByDescending(es => es.Name);
						break;
					case nameof(ExtraExamSeat.ProgramAlias):
						extraExamSeats = sortDirection == SortDirection.Ascending ? extraExamSeats.OrderBy(es => es.ProgramAlias) : extraExamSeats.OrderByDescending(es => es.ProgramAlias);
						break;
					case nameof(ExtraExamSeat.Title):
						extraExamSeats = sortDirection == SortDirection.Ascending ? extraExamSeats.OrderBy(es => es.Title) : extraExamSeats.OrderByDescending(es => es.Title);
						break;
					case nameof(ExtraExamSeat.ExamDate):
						extraExamSeats = sortDirection == SortDirection.Ascending ? extraExamSeats.OrderBy(es => es.ExamDate) : extraExamSeats.OrderByDescending(es => es.ExamDate);
						break;
					case nameof(ExtraExamSeat.SessionName):
						extraExamSeats = sortDirection == SortDirection.Ascending ? extraExamSeats.OrderBy(es => es.SessionName) : extraExamSeats.OrderByDescending(es => es.SessionName);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}

				result.ExtraExamSeats = extraExamSeats.Skip(pageIndex * pageSize).Take(pageSize).ToList();
				return result;
			}
		}

		public enum ClearExtraSeatsResult
		{
			NoRecordFound,
			Success
		}

		public static ClearExtraSeatsResult ClearExtraSeats(int examConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.ClearExtraSeats, UserGroupPermission.PermissionValues.Allowed);
				if (!aspireContext.ExamConducts.Any(c => c.ExamConductID == examConductID && c.InstituteID == loginHistory.InstituteID))
					return ClearExtraSeatsResult.NoRecordFound;

				var examSeatsToBeDeleted = aspireContext.GetExamSeats(examConductID, null, null).Except(aspireContext.GetValidExamSeats(examConductID, null, null));
				aspireContext.ExamSeats.RemoveRange(examSeatsToBeDeleted);

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID, true);
				return ClearExtraSeatsResult.Success;
			}
		}
	}
}
