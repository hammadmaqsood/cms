using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff
{
	public static class ExamDates
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, params UserGroupPermission.PermissionValues[] permissionValues)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.ManageExamDates, permissionValues);
		}

		public sealed class ExamDate
		{
			internal ExamDate()
			{
			}

			public int ExamDateID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public byte ExamType { get; internal set; }
			public string ExamTypeFullName => ((ExamConduct.ExamTypes)this.ExamType).ToFullName();
			public string ExamConductName { get; internal set; }
			public DateTime Date { get; internal set; }
		}

		public static List<ExamDate> GetExamDates(int examConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var instituteID = loginHistory.InstituteID;
				var examDates = aspireContext.ExamDates.Where(es => es.ExamConductID == examConductID && es.ExamConduct.InstituteID == instituteID).OrderBy(es => es.Date);
				return examDates.Select(es => new ExamDate
				{
					ExamDateID = es.ExamDateID,
					SemesterID = es.ExamConduct.SemesterID,
					ExamType = es.ExamConduct.ExamType,
					ExamConductName = es.ExamConduct.Name,
					Date = es.Date,
				}).ToList();
			}
		}

		public enum AddExamDateStatuses
		{
			Success,
			NameAlreadyExists,
		}

		public static AddExamDateStatuses AddExamDate(int examConductID, DateTime date, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				date = date.Date;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var instituteID = loginHistory.InstituteID;
				if (aspireContext.ExamDates.Any(es => es.ExamConductID == examConductID && es.Date == date))
					return AddExamDateStatuses.NameAlreadyExists;

				var examDate = new Model.Entities.ExamDate
				{
					ExamConductID = aspireContext.ExamConducts.Where(ec => ec.ExamConductID == examConductID && ec.InstituteID == instituteID).Select(ec => ec.ExamConductID).Single(),
					Date = date,
				};

				aspireContext.ExamDates.Add(examDate);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return AddExamDateStatuses.Success;
			}
		}

		public enum DeleteExamDateStatuses
		{
			NoRecordFound,
			Success,
			ChildRecordExistsExamDateAndSessions
		}

		public static DeleteExamDateStatuses DeleteExamDate(int examDateID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var instituteID = loginHistory.InstituteID;
				var examDate = aspireContext.ExamDates.FirstOrDefault(es => es.ExamDateID == examDateID && es.ExamConduct.InstituteID == instituteID);
				if (examDate == null)
					return DeleteExamDateStatuses.NoRecordFound;
				if (examDate.ExamDateAndSessions.Any())
					return DeleteExamDateStatuses.ChildRecordExistsExamDateAndSessions;
				aspireContext.ExamDates.Remove(examDate);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteExamDateStatuses.Success;
			}
		}

		public static List<CustomListItem> GetExamDatesList(int examConductID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var instituteID = loginHistory.InstituteID;
				return aspireContext.ExamDates.Where(ed => ed.ExamConductID == examConductID && ed.ExamConduct.InstituteID == instituteID).OrderBy(ed => ed.Date).ToList().Select(ed => new CustomListItem
				{
					ID = ed.ExamDateID,
					Text = ed.Date.ToString("D"),
				}).ToList();
			}
		}
	}
}