using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff
{
	public static class ExamRooms
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.ManageExamRooms, UserGroupPermission.PermissionValues.Allowed);
		}

		public enum AddExamRoomStatuses
		{
			NameAlreadyExists,
			Success
		}

		private static void ReorderExamRooms(this AspireContext aspireContext, int examBuildingID, int userLoginHistoryID)
		{
			byte sequence = 0;
			foreach (var examRoom in aspireContext.ExamRooms.Where(er => er.ExamBuildingID == examBuildingID).OrderBy(er => er.Sequence).ToList())
				examRoom.Sequence = sequence++;
			aspireContext.SaveChanges(userLoginHistoryID);
		}

		public static AddExamRoomStatuses AddExamRoom(int examBuildingID, string roomName, byte rows, byte columns, Model.Entities.ExamRoom.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				roomName = roomName.TrimAndCannotBeEmpty();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var instituteID = loginHistory.InstituteID;
				if (aspireContext.ExamRooms.Any(er => er.ExamBuildingID == examBuildingID && er.ExamBuilding.InstituteID == instituteID && er.RoomName == roomName))
					return AddExamRoomStatuses.NameAlreadyExists;
				var examRoom = new Model.Entities.ExamRoom
				{
					ExamBuildingID = aspireContext.ExamBuildings.Where(eb => eb.ExamBuildingID == examBuildingID && eb.InstituteID == instituteID).Select(eb => eb.ExamBuildingID).Single(),
					RoomName = roomName,
					Rows = rows,
					Columns = columns,
					StatusEnum = statusEnum,
					Sequence = byte.MaxValue
				};
				aspireContext.ExamRooms.Add(examRoom);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ReorderExamRooms(examRoom.ExamBuildingID, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return AddExamRoomStatuses.Success;
			}
		}

		public enum UpdateExamRoomStatuses
		{
			NoRecordFound,
			NameAlreadyExists,
			Success
		}

		public static UpdateExamRoomStatuses UpdateExamRoom(int examRoomID, string roomName, byte rows, byte columns, Model.Entities.ExamRoom.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				roomName = roomName.TrimAndCannotBeEmpty();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var instituteID = loginHistory.InstituteID;
				var examRoom = aspireContext.ExamRooms.SingleOrDefault(er => er.ExamRoomID == examRoomID && er.ExamBuilding.InstituteID == instituteID);
				if (examRoom == null)
					return UpdateExamRoomStatuses.NoRecordFound;
				if (aspireContext.ExamRooms.Any(er => er.ExamRoomID != examRoom.ExamRoomID && er.ExamBuildingID == examRoom.ExamBuildingID && er.RoomName == roomName))
					return UpdateExamRoomStatuses.NameAlreadyExists;
				examRoom.RoomName = roomName;
				examRoom.Rows = rows;
				examRoom.Columns = columns;
				examRoom.StatusEnum = statusEnum;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateExamRoomStatuses.Success;
			}
		}

		public enum DeleteExamRoomStatuses
		{
			NoRecordFound,
			CannotDeleteChildRecordExistsExamOfferedRooms,
			Success
		}

		public static DeleteExamRoomStatuses DeleteExamRoom(int examRoomID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var instituteID = loginHistory.InstituteID;
				var examRoom = aspireContext.ExamRooms.FirstOrDefault(er => er.ExamRoomID == examRoomID && er.ExamBuilding.InstituteID == instituteID);
				if (examRoom == null)
					return DeleteExamRoomStatuses.NoRecordFound;
				if (examRoom.ExamOfferedRooms.Any())
					return DeleteExamRoomStatuses.CannotDeleteChildRecordExistsExamOfferedRooms;
				var examBuildingID = examRoom.ExamBuildingID;
				aspireContext.ExamRooms.Remove(examRoom);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ReorderExamRooms(examBuildingID, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return DeleteExamRoomStatuses.Success;
			}
		}

		public enum MoveExamRoomStatuses
		{
			NoRecordFound,
			AlreadyAtTop,
			AlreadyAtBottom,
			Success,
		}

		public static MoveExamRoomStatuses MoveExamRoomUp(int examRoomID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var instituteID = loginHistory.InstituteID;
				var examRoom = aspireContext.ExamRooms.SingleOrDefault(er => er.ExamRoomID == examRoomID && er.ExamBuilding.InstituteID == instituteID);
				if (examRoom == null)
					return MoveExamRoomStatuses.NoRecordFound;
				if (examRoom.Sequence == 0)
					return MoveExamRoomStatuses.AlreadyAtTop;
				var examRoomUpper = aspireContext.ExamRooms.SingleOrDefault(er => er.ExamBuildingID == examRoom.ExamBuildingID && er.Sequence == examRoom.Sequence - 1);
				if (examRoomUpper == null)
					return MoveExamRoomStatuses.NoRecordFound;
				examRoomUpper.Sequence++;
				examRoom.Sequence--;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return MoveExamRoomStatuses.Success;
			}
		}

		public static MoveExamRoomStatuses MoveExamRoomDown(int examRoomID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var instituteID = loginHistory.InstituteID;
				var examRoom = aspireContext.ExamRooms.SingleOrDefault(er => er.ExamRoomID == examRoomID && er.ExamBuilding.InstituteID == instituteID);
				if (examRoom == null)
					return MoveExamRoomStatuses.NoRecordFound;
				if (examRoom.Sequence == byte.MaxValue)
					return MoveExamRoomStatuses.AlreadyAtBottom;
				var examRoomBelow = aspireContext.ExamRooms.SingleOrDefault(er => er.ExamBuildingID == examRoom.ExamBuildingID && er.Sequence == examRoom.Sequence + 1);
				if (examRoomBelow == null)
					return MoveExamRoomStatuses.AlreadyAtBottom;
				examRoomBelow.Sequence--;
				examRoom.Sequence++;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return MoveExamRoomStatuses.Success;
			}
		}

		public sealed class ExamRoom
		{
			internal ExamRoom()
			{
			}

			public int ExamRoomID { get; internal set; }
			public int ExamBuildingID { get; internal set; }
			public string BuildingName { get; internal set; }
			public string RoomName { get; internal set; }
			public byte Rows { get; internal set; }
			public byte Columns { get; internal set; }
			public byte Status { get; internal set; }
			public string StatusFullName => ((Model.Entities.ExamRoom.Statuses)this.Status).ToFullName();
			public int TotalSeats => this.Rows * this.Columns;
		}

		public static List<ExamRoom> GetExamRooms(int? examBuildingID, Model.Entities.ExamRoom.Statuses? statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var instituteID = loginHistory.InstituteID;
				var examRooms = aspireContext.ExamRooms.Where(er => er.ExamBuilding.InstituteID == instituteID);
				if (examBuildingID != null)
					examRooms = examRooms.Where(er => er.ExamBuildingID == examBuildingID);
				if (statusEnum != null)
				{
					var status = (byte)statusEnum;
					examRooms = examRooms.Where(er => er.Status == status);
				}
				return examRooms.OrderBy(er => er.ExamBuilding.BuildingName).ThenBy(er => er.Sequence).Select(er => new ExamRoom
				{
					ExamRoomID = er.ExamRoomID,
					ExamBuildingID = er.ExamBuildingID,
					BuildingName = er.ExamBuilding.BuildingName,
					RoomName = er.RoomName,
					Rows = er.Rows,
					Columns = er.Columns,
					Status = er.Status,
				}).ToList();
			}
		}

		public static Model.Entities.ExamRoom GetExamRoom(int examRoomID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var instituteID = loginHistory.InstituteID;
				return aspireContext.ExamRooms.SingleOrDefault(er => er.ExamRoomID == examRoomID && er.ExamBuilding.InstituteID == instituteID);
			}
		}

		public enum UpdateRoomsSequenceStatuses
		{
			RoomsHaveBeenUpdatedInOtherSession,
			Success
		}

		public static UpdateRoomsSequenceStatuses UpdateRoomsSequence(List<int> examRoomIDs, Guid loginSessionGuid)
		{
			if (examRoomIDs == null || !examRoomIDs.Any())
				throw new ArgumentNullException(nameof(examRoomIDs));
			if (examRoomIDs.Distinct().Count() != examRoomIDs.Count)
				throw new ArgumentException(nameof(examRoomIDs));
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var sequence = 0;
				var rooms = examRoomIDs.Select(id => new
				{
					Sequence = sequence++,
					ExamRoom = aspireContext.ExamRooms.SingleOrDefault(er => er.ExamRoomID == id && er.ExamBuilding.InstituteID == loginHistory.InstituteID)
				}).ToList();

				if (rooms.Any(r => r.ExamRoom == null))
					return UpdateRoomsSequenceStatuses.RoomsHaveBeenUpdatedInOtherSession;

				var examBuildingIDs = rooms.Select(r => r.ExamRoom.ExamBuildingID).Distinct().ToList();
				foreach (var examBuildingID in examBuildingIDs)
				{
					var sequencedRooms = rooms.Where(r => r.ExamRoom.ExamBuildingID == examBuildingID).Select(r => r.ExamRoom.ExamRoomID).ToList();
					var buildingExamRooms = aspireContext.ExamRooms.Where(er => er.ExamBuildingID == examBuildingID).ToList();
					foreach (var examRoom in buildingExamRooms)
					{
						if (!sequencedRooms.Contains(examRoom.ExamRoomID))
							return UpdateRoomsSequenceStatuses.RoomsHaveBeenUpdatedInOtherSession;
						examRoom.Sequence = (byte)sequencedRooms.IndexOf(examRoom.ExamRoomID);
					}
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateRoomsSequenceStatuses.Success;
			}
		}
	}
}