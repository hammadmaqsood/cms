using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.ExamSeatingPlan.Staff
{
	public static class ExamOfferedRooms
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, params UserGroupPermission.PermissionValues[] permissionValues)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.ExamSeatingPlan.ManageExamOfferedRooms, permissionValues);
		}

		public sealed class ExamOfferedRoom
		{
			internal ExamOfferedRoom()
			{
			}

			public int? ExamOfferedRoomID { get; internal set; }
			public short? SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public byte? ExamType { get; internal set; }
			public string ExamTypeFullName => ((ExamConduct.ExamTypes?)this.ExamType)?.ToFullName();
			public string ExamConductName { get; internal set; }
			public string BuildingName { get; internal set; }
			public int ExamRoomID { get; internal set; }
			public string RoomName { get; internal set; }
			public byte Rows { get; internal set; }
			public byte Columns { get; internal set; }
			public int TotalSeats => this.Rows * this.Columns;
			public byte? RoomUsageType { get; internal set; }
			public string RoomUsageTypeFullName => ((Model.Entities.ExamOfferedRoom.RoomUsageTypes?)this.RoomUsageType)?.ToFullName();
		}

		public static List<ExamOfferedRoom> GetExamOfferedRooms(int examConductID, bool includeOffered, bool includeNotOffered, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var instituteID = loginHistory.InstituteID;
				if (!aspireContext.ExamConducts.Any(ec => ec.ExamConductID == examConductID && ec.InstituteID == instituteID))
					return null;

				var result = new List<ExamOfferedRoom>();
				if (includeOffered)
					result.AddRange(aspireContext.ExamOfferedRooms.Where(eor => eor.ExamConductID == examConductID).OrderBy(er => er.Sequence).Select(eor => new ExamOfferedRoom
					{
						ExamOfferedRoomID = eor.ExamOfferedRoomID,
						SemesterID = eor.ExamConduct.SemesterID,
						ExamType = eor.ExamConduct.ExamType,
						ExamConductName = eor.ExamConduct.Name,
						BuildingName = eor.ExamRoom.ExamBuilding.BuildingName,
						ExamRoomID = eor.ExamRoomID,
						RoomName = eor.ExamRoom.RoomName,
						Rows = eor.Rows,
						Columns = eor.Columns,
						RoomUsageType = eor.RoomUsageType,
					}));
				if (includeNotOffered)
					result.AddRange(aspireContext.ExamRooms.Where(er => er.Status == ExamRoom.StatusActive && er.ExamBuilding.InstituteID == instituteID && er.ExamOfferedRooms.All(eor => eor.ExamConductID != examConductID)).OrderBy(er => er.ExamBuilding.BuildingName).ThenBy(er => er.Sequence).Select(er => new ExamOfferedRoom
					{
						ExamOfferedRoomID = null,
						SemesterID = null,
						ExamType = null,
						ExamConductName = null,
						BuildingName = er.ExamBuilding.BuildingName,
						ExamRoomID = er.ExamRoomID,
						RoomName = er.RoomName,
						Rows = er.Rows,
						Columns = er.Columns,
						RoomUsageType = null,
					}));
				return result;
			}
		}

		public enum DeleteExamOfferedRoomStatuses
		{
			NoRecordFound,
			CannotDeleteChildRecordExists,
			Success,
		}

		private static bool ChildExists(this Model.Entities.ExamOfferedRoom examOfferedRoom)
		{
			if (examOfferedRoom.ExamSeats.Any())
				return true;
			if (examOfferedRoom.ExamInvigilatorDuties.Any())
				return true;
			return false;
		}

		public static DeleteExamOfferedRoomStatuses DeleteExamOfferedRoom(int examOfferedRoomID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var instituteID = loginHistory.InstituteID;
				var examOfferedRoom = aspireContext.ExamOfferedRooms.FirstOrDefault(eor => eor.ExamOfferedRoomID == examOfferedRoomID && eor.ExamRoom.ExamBuilding.InstituteID == instituteID);
				if (examOfferedRoom == null)
					return DeleteExamOfferedRoomStatuses.NoRecordFound;
				if (examOfferedRoom.ChildExists())
					return DeleteExamOfferedRoomStatuses.CannotDeleteChildRecordExists;
				aspireContext.ExamOfferedRooms.Remove(examOfferedRoom);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteExamOfferedRoomStatuses.Success;
			}
		}

		public enum ClearAllSeatsOfExamOfferedRoomStatuses
		{
			NoRecordFound,
			Success
		}

		public static ClearAllSeatsOfExamOfferedRoomStatuses ClearAllSeatsOfExamOfferedRoom(int examOfferedRoomID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var instituteID = loginHistory.InstituteID;
				var examOfferedRoom = aspireContext.ExamOfferedRooms.Include(eor => eor.ExamSeats)
					.SingleOrDefault(eor => eor.ExamOfferedRoomID == examOfferedRoomID && eor.ExamRoom.ExamBuilding.InstituteID == instituteID);
				if (examOfferedRoom == null)
					return ClearAllSeatsOfExamOfferedRoomStatuses.NoRecordFound;
				aspireContext.ExamSeats.RemoveRange(examOfferedRoom.ExamSeats);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return ClearAllSeatsOfExamOfferedRoomStatuses.Success;
			}
		}

		public enum ToggleStatuses
		{
			NoRecordFound,
			CannotUpdateChildRecordExists,
			Success,
		}

		public static ToggleStatuses ToggleRoomUsageType(int examOfferedRoomID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var instituteID = loginHistory.InstituteID;
				var examOfferedRoom = aspireContext.ExamOfferedRooms.SingleOrDefault(eor => eor.ExamOfferedRoomID == examOfferedRoomID && eor.ExamRoom.ExamBuilding.InstituteID == instituteID);
				if (examOfferedRoom == null)
					return ToggleStatuses.NoRecordFound;
				if (examOfferedRoom.ChildExists())
					return ToggleStatuses.CannotUpdateChildRecordExists;
				switch (examOfferedRoom.RoomUsageTypeEnum)
				{
					case Model.Entities.ExamOfferedRoom.RoomUsageTypes.SinglePaper:
						examOfferedRoom.RoomUsageTypeEnum = Model.Entities.ExamOfferedRoom.RoomUsageTypes.MultiplePapers;
						break;
					case Model.Entities.ExamOfferedRoom.RoomUsageTypes.MultiplePapers:
						examOfferedRoom.RoomUsageTypeEnum = Model.Entities.ExamOfferedRoom.RoomUsageTypes.SinglePaper;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return ToggleStatuses.Success;
			}
		}

		public enum DecrementStatuses
		{
			NoRecordFound,
			CannotUpdateChildRecordExists,
			CannotUpdateRowOrColumnCannotBeLessThanOne,
			Success
		}

		public static DecrementStatuses DecrementRows(int examOfferedRoomID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var instituteID = loginHistory.InstituteID;
				var examOfferedRoom = aspireContext.ExamOfferedRooms.SingleOrDefault(eor => eor.ExamOfferedRoomID == examOfferedRoomID && eor.ExamRoom.ExamBuilding.InstituteID == instituteID);
				if (examOfferedRoom == null)
					return DecrementStatuses.NoRecordFound;
				if (examOfferedRoom.ChildExists())
					return DecrementStatuses.CannotUpdateChildRecordExists;
				if (examOfferedRoom.Rows > 1)
					examOfferedRoom.Rows--;
				else
					return DecrementStatuses.CannotUpdateRowOrColumnCannotBeLessThanOne;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DecrementStatuses.Success;
			}
		}

		public enum IncrementStatuses
		{
			NoRecordFound,
			CannotUpdateChildRecordExists,
			CannotUpdateRowOrColumnCannotBeGreaterThenMax,
			Success,
		}

		public static IncrementStatuses IncrementRows(int examOfferedRoomID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var instituteID = loginHistory.InstituteID;
				var examOfferedRoom = aspireContext.ExamOfferedRooms.SingleOrDefault(eor => eor.ExamOfferedRoomID == examOfferedRoomID && eor.ExamRoom.ExamBuilding.InstituteID == instituteID);
				if (examOfferedRoom == null)
					return IncrementStatuses.NoRecordFound;
				if (examOfferedRoom.Rows < byte.MaxValue)
					examOfferedRoom.Rows++;
				else
					return IncrementStatuses.CannotUpdateRowOrColumnCannotBeGreaterThenMax;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return IncrementStatuses.Success;
			}
		}

		public static DecrementStatuses DecrementColumns(int examOfferedRoomID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var instituteID = loginHistory.InstituteID;
				var examOfferedRoom = aspireContext.ExamOfferedRooms.SingleOrDefault(eor => eor.ExamOfferedRoomID == examOfferedRoomID && eor.ExamRoom.ExamBuilding.InstituteID == instituteID);
				if (examOfferedRoom == null)
					return DecrementStatuses.NoRecordFound;
				if (examOfferedRoom.ChildExists())
					return DecrementStatuses.CannotUpdateChildRecordExists;
				if (examOfferedRoom.Columns > 1)
					examOfferedRoom.Columns--;
				else
					return DecrementStatuses.CannotUpdateRowOrColumnCannotBeLessThanOne;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DecrementStatuses.Success;
			}
		}

		public static IncrementStatuses IncrementColumns(int examOfferedRoomID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var instituteID = loginHistory.InstituteID;
				var examOfferedRoom = aspireContext.ExamOfferedRooms.SingleOrDefault(eor => eor.ExamOfferedRoomID == examOfferedRoomID && eor.ExamRoom.ExamBuilding.InstituteID == instituteID);
				if (examOfferedRoom == null)
					return IncrementStatuses.NoRecordFound;
				if (examOfferedRoom.Columns < byte.MaxValue)
					examOfferedRoom.Columns++;
				else
					return IncrementStatuses.CannotUpdateRowOrColumnCannotBeGreaterThenMax;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return IncrementStatuses.Success;
			}
		}

		public enum AddExamOfferedRoomStatuses
		{
			AlreadyExists,
			Success,
			NoRecordFoundExamRoom,
			NoRecordFoundExamConduct
		}

		private static AddExamOfferedRoomStatuses AddExamOfferedRoom(this AspireContext aspireContext, int instituteID, int examConductID, int examRoomID, int userLoginHistoryID)
		{
			if (aspireContext.ExamOfferedRooms.Any(eor => eor.ExamConductID == examConductID && eor.ExamRoomID == examRoomID))
				return AddExamOfferedRoomStatuses.AlreadyExists;
			var examConduct = aspireContext.ExamConducts.SingleOrDefault(ec => ec.ExamConductID == examConductID && ec.InstituteID == instituteID);
			if (examConduct == null)
				return AddExamOfferedRoomStatuses.NoRecordFoundExamConduct;
			var examRoom = aspireContext.ExamRooms.SingleOrDefault(er => er.ExamRoomID == examRoomID && er.ExamBuilding.InstituteID == instituteID);
			if (examRoom == null)
				return AddExamOfferedRoomStatuses.NoRecordFoundExamRoom;

			var max = aspireContext.ExamOfferedRooms.Where(eor => eor.ExamConductID == examConduct.ExamConductID).Max(eor => (short?)eor.Sequence) ?? 0;
			aspireContext.ExamOfferedRooms.Add(new Model.Entities.ExamOfferedRoom
			{
				ExamRoomID = examRoom.ExamRoomID,
				ExamConductID = examConduct.ExamConductID,
				Rows = examRoom.Rows,
				Columns = examRoom.Columns,
				Sequence = ++max,
				RoomUsageTypeEnum = Model.Entities.ExamOfferedRoom.RoomUsageTypes.SinglePaper,
			});
			aspireContext.SaveChanges(userLoginHistoryID);
			return AddExamOfferedRoomStatuses.Success;
		}

		public static AddExamOfferedRoomStatuses AddExamOfferedRoom(int examConductID, int examRoomID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var instituteID = loginHistory.InstituteID;
				var result = aspireContext.AddExamOfferedRoom(instituteID, examConductID, examRoomID, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return result;
			}
		}

		public enum UpdateRoomsSequenceStatuses
		{
			RoomsHaveBeenUpdatedInOtherSession,
			Success
		}

		public static UpdateRoomsSequenceStatuses UpdateRoomsSequence(List<int> examOfferedRoomIDs, Guid loginSessionGuid)
		{
			if (examOfferedRoomIDs.Distinct().Count() != examOfferedRoomIDs.Count)
				throw new ArgumentException(nameof(examOfferedRoomIDs));
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, UserGroupPermission.PermissionValues.Allowed);
				var instituteID = loginHistory.InstituteID;
				var list = examOfferedRoomIDs.ToList();
				var examConductID = aspireContext.ExamOfferedRooms.Where(eor => examOfferedRoomIDs.Contains(eor.ExamOfferedRoomID)).Select(eor => eor.ExamConductID).Distinct().Single();

				foreach (var examOfferedRoom in aspireContext.ExamOfferedRooms.Where(eor => eor.ExamConductID == examConductID && eor.ExamConduct.InstituteID == instituteID).ToList())
				{
					if (examOfferedRoomIDs.Contains(examOfferedRoom.ExamOfferedRoomID))
					{
						examOfferedRoom.Sequence = (short)examOfferedRoomIDs.IndexOf(examOfferedRoom.ExamOfferedRoomID);
						list.Remove(examOfferedRoom.ExamOfferedRoomID);
					}
					else
						return UpdateRoomsSequenceStatuses.RoomsHaveBeenUpdatedInOtherSession;
				}
				if (list.Any())
					return UpdateRoomsSequenceStatuses.RoomsHaveBeenUpdatedInOtherSession;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateRoomsSequenceStatuses.Success;
			}
		}
	}
}