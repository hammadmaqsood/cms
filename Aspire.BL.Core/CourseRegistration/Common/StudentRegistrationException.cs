using System;

namespace Aspire.BL.Core.CourseRegistration.Common
{
	internal sealed class StudentRegistrationException : Exception
	{
		public readonly StudentRegistrationStatus Status;
		public StudentRegistrationException(StudentRegistrationStatus status)
		{
			this.Status = status;
		}
	}
}