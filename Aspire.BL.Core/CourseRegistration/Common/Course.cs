using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.CourseRegistration.Common
{
	public sealed class Course
	{
		internal CoursesRegister CoursesRegister { get; }
		public int OfferedCourseID { get; }
		public bool RequestOnly { get; }
		public int? RoadmapCourseID { get; private set; }
		public RegisteredCours.BypassedCheckTypes BypassedChecks { get; set; }
		public CourseRegistrationStatus? Status { get; private set; }
		internal bool AddedToRequestedList { get; private set; }
		internal OfferedCourse OfferedCourse { get; private set; }
		internal RoadmapCourse RoadmapCourse { get; private set; }
		internal AspireContext AspireContext => this.CoursesRegister.AspireContext;

		private Course() { }

		internal Course(int offeredCourseID, int? roadmapCourseID, CoursesRegister coursesRegister, bool requestOnly) : this()
		{
			this.CoursesRegister = coursesRegister;
			this.OfferedCourseID = offeredCourseID;
			this.RoadmapCourseID = roadmapCourseID;
			this.RequestOnly = requestOnly;
			this.BypassedChecks = RegisteredCours.BypassedCheckTypes.None;
		}

		internal void Initialize()
		{
			this.OfferedCourse = this.AspireContext.OfferedCourses
				.Where(oc => oc.OfferedCourseID == this.OfferedCourseID && oc.Cours.AdmissionOpenProgram.Program.InstituteID == this.CoursesRegister.StudentInstituteID)
				.Select(oc => new OfferedCourse
				{
					CourseID = oc.CourseID,
					Title = oc.Cours.Title,
					AdmissionOpenProgramID = oc.Cours.AdmissionOpenProgramID,
					MaxClassStrength = oc.MaxClassStrength,
					RegisteredStrength = oc.RegisteredCourses.Count(rc => rc.DeletedDate == null),
				}).SingleOrDefault();

			if (this.OfferedCourse == null)
			{
				this.Status = CourseRegistrationStatus.OfferedCourseNotFound;
				return;
			}

			if (this.OfferedCourse.MaxClassStrength <= this.OfferedCourse.RegisteredStrength)
			{
				this.Status = CourseRegistrationStatus.ClassFull;
				return;
			}

			if (this.RoadmapCourseID == null)
			{
				switch (this.CoursesRegister.UserType)
				{
					case CoursesRegister.UserTypes.Student:
						break;
					case CoursesRegister.UserTypes.Staff:
						throw new InvalidOperationException("Equivalent Course must be specified by Staff.");
					default:
						throw new NotImplementedEnumException(this.CoursesRegister.UserType);
				}
				if (this.CoursesRegister.StudentAdmissionOpenProgramID == this.OfferedCourse.AdmissionOpenProgramID)
					this.RoadmapCourseID = this.OfferedCourse.CourseID;
				else
				{
					var equivalentRoadmapCourseID = (from ce in this.AspireContext.CourseEquivalents
													 join c in this.AspireContext.Courses on ce.EquivalentCourseID equals c.CourseID
													 where ce.CourseID == this.OfferedCourse.CourseID && c.AdmissionOpenProgramID == this.CoursesRegister.StudentAdmissionOpenProgramID
													 select (int?)c.CourseID).SingleOrDefault();
					if (equivalentRoadmapCourseID != null)
						this.RoadmapCourseID = equivalentRoadmapCourseID.Value;
				}
			}
			if (this.RoadmapCourseID != null)
			{
				if (this.OfferedCourse.AdmissionOpenProgramID == this.CoursesRegister.StudentAdmissionOpenProgramID
					&& this.RoadmapCourseID != this.OfferedCourse.CourseID)
					throw new InvalidOperationException($"Equivalent courses are not allowed with in the same roadmap. CourseID: {this.RoadmapCourseID}, OfferedCourseID: {this.OfferedCourseID}, StudentID: {this.CoursesRegister.StudentID}.");

				this.RoadmapCourse = this.AspireContext.Courses
					.Where(c => c.CourseID == this.RoadmapCourseID.Value && c.AdmissionOpenProgramID == this.CoursesRegister.StudentAdmissionOpenProgramID)
					.Select(c => new RoadmapCourse
					{
						CourseID = c.CourseID,
						Title = c.Title,
						CreditHours = c.CreditHours,
						CourseCategoryEnum = (CourseCategories)c.CourseCategory,
						PreReqs = c.CoursePreReqs.Select(p => new RoadmapCoursePreReq
						{
							CoursePreReqID = p.CoursePreReqID,
							CourseID1 = p.PreReqCourseID1,
							Title1 = this.AspireContext.Courses.Where(cc => cc.CourseID == p.PreReqCourseID1).Select(cc => cc.Title).FirstOrDefault(),
							CourseID2 = p.PreReqCourseID2,
							Title2 = this.AspireContext.Courses.Where(cc => p.PreReqCourseID2 != null && cc.CourseID == p.PreReqCourseID2.Value).Select(cc => cc.Title).FirstOrDefault(),
							CourseID3 = p.PreReqCourseID3,
							Title3 = this.AspireContext.Courses.Where(cc => p.PreReqCourseID3 != null && cc.CourseID == p.PreReqCourseID3.Value).Select(cc => cc.Title).FirstOrDefault(),
						}).ToList(),
					}).SingleOrDefault();
				if (this.RoadmapCourse == null)
					throw new InvalidOperationException($"Course does not belong to student's roadmap. . CourseID: {this.RoadmapCourseID}, OfferedCourseID: {this.OfferedCourseID}, StudentID: {this.CoursesRegister.StudentID}.");
			}
			if (this.RoadmapCourse == null)
				this.Status = CourseRegistrationStatus.RoadmapCourseNotFound;
		}

		internal void RequestCourseIfRequired()
		{
			this.AddedToRequestedList = false;
			if (this.CoursesRegister.RequestIfRegistrationFailed || this.RequestOnly)
			{
				if (this.AspireContext.RequestedCourses.Any(rc => rc.StudentID == this.CoursesRegister.StudentID && rc.OfferedCourseID == this.OfferedCourseID))
					return;
				this.AspireContext.RequestedCourses.Add(new RequestedCours
				{
					StudentID = this.CoursesRegister.StudentID,
					OfferedCourseID = this.OfferedCourseID,
					RequestDate = DateTime.Now,
				});
				this.AspireContext.SaveChanges(this.CoursesRegister.UserLoginHistoryID);
				this.AddedToRequestedList = true;
			}
		}

		private void RegisterCourseStep1CheckSemesterWorkload()
		{
			var summary = this.AspireContext.RegisteredCourses
				.Where(rc => rc.StudentID == this.CoursesRegister.StudentID && rc.DeletedDate == null && rc.OfferedCours.SemesterID == this.CoursesRegister.OfferedSemesterID)
				.Select(rc => new
				{
					rc.StudentID,
					rc.Cours.CreditHours
				}).GroupBy(rc => new
				{
					rc.StudentID
				}).Select(g => new
				{
					Courses = g.Count(),
					CreditHours = g.Sum(r => r.CreditHours)
				}).SingleOrDefault();
			var registeredCourses = summary?.Courses ?? 0;
			var registeredCreditHours = summary?.CreditHours ?? 0;
			if (this.CoursesRegister.MaxAllowedCoursesPerSemester < registeredCourses + 1)
			{
				switch (this.CoursesRegister.UserType)
				{
					case CoursesRegister.UserTypes.Student:
						throw new CourseRegistrationException(CourseRegistrationStatus.MaxAllowedCoursesPerSemesterConstraintFailed);
					case CoursesRegister.UserTypes.Staff:
						if (!this.CoursesRegister.ByPassChecks.HasFlag(RegisteredCours.BypassedCheckTypes.MaxAllowedCoursesPerSemester))
							throw new CourseRegistrationException(CourseRegistrationStatus.MaxAllowedCoursesPerSemesterConstraintFailed);
						break;
					default:
						throw new NotImplementedEnumException(this.CoursesRegister.UserType);
				}
			}
			if (this.CoursesRegister.MaxAllowedCreditHoursPerSemester < registeredCreditHours + this.RoadmapCourse.CreditHours)
			{
				switch (this.CoursesRegister.UserType)
				{
					case CoursesRegister.UserTypes.Student:
						throw new CourseRegistrationException(CourseRegistrationStatus.MaxAllowedCreditHoursPerSemesterConstraintFailed);
					case CoursesRegister.UserTypes.Staff:
						if (!this.CoursesRegister.ByPassChecks.HasFlag(RegisteredCours.BypassedCheckTypes.MaxAllowedCreditHoursPerSemester))
							throw new CourseRegistrationException(CourseRegistrationStatus.MaxAllowedCreditHoursPerSemesterConstraintFailed);
						break;
					default:
						throw new NotImplementedEnumException(this.CoursesRegister.UserType);
				}
			}
		}

		private void RegisterCourseStep2IsCourseAlreadyRegistered()
		{
			var registeredCoursesQuery = this.AspireContext.RegisteredCourses.Where(rc => rc.StudentID == this.CoursesRegister.StudentID && rc.DeletedDate == null);
			if (registeredCoursesQuery.Any(rc => rc.OfferedCourseID == this.OfferedCourseID))
				throw new CourseRegistrationException(CourseRegistrationStatus.AlreadyRegisteredInOfferedCourseID);

			if (registeredCoursesQuery.Any(rc => rc.OfferedCours.SemesterID == this.CoursesRegister.OfferedSemesterID && rc.CourseID == this.RoadmapCourse.CourseID))
				throw new CourseRegistrationException(CourseRegistrationStatus.AlreadyRegisteredCourseIDInCurrentSemester);
		}

		private void RegisterCourseStep3CheckPreRequisites()
		{
			this.RoadmapCourse.CheckPreRequisites(this);
		}

		private void RegisterCourseStep4CheckImprovements()
		{
			switch (this.RoadmapCourse.CourseCategoryEnum)
			{
				case CourseCategories.Project:
				case CourseCategories.Thesis:
				case CourseCategories.Internship:
					return;
				case CourseCategories.Course:
				case CourseCategories.Lab:
					var previousRegisteredCourses = this.AspireContext.RegisteredCourses
						.Where(rc => rc.DeletedDate == null && rc.StudentID == this.CoursesRegister.StudentID)
						.Where(rc => rc.CourseID == this.RoadmapCourse.CourseID && rc.OfferedCours.SemesterID < this.CoursesRegister.OfferedSemesterID)
						.Select(rc => new { GradeEnum = (ExamGrades?)rc.Grade, StatusEnum = (RegisteredCours.Statuses?)rc.Status }).ToList();
					var improvementCase = false;

					foreach (var previousRegisteredCourse in previousRegisteredCourses)
					{
						if (previousRegisteredCourse.GradeEnum != null)
						{
							if (ExamGradeComparer.GreaterThan(previousRegisteredCourse.GradeEnum.Value, this.CoursesRegister.MaxExamGradeToBeImproved))
								switch (previousRegisteredCourse.GradeEnum.Value)
								{
									case ExamGrades.A:
										throw new CourseRegistrationException(CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeA);
									case ExamGrades.AMinus:
										throw new CourseRegistrationException(CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeAMinus);
									case ExamGrades.BPlus:
										throw new CourseRegistrationException(CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeBPlus);
									case ExamGrades.B:
										throw new CourseRegistrationException(CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeB);
									case ExamGrades.BMinus:
										throw new CourseRegistrationException(CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeBMinus);
									case ExamGrades.CPlus:
										throw new CourseRegistrationException(CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeCPlus);
									case ExamGrades.C:
										throw new CourseRegistrationException(CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeC);
									case ExamGrades.CMinus:
										throw new CourseRegistrationException(CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeCMinus);
									case ExamGrades.DPlus:
										throw new CourseRegistrationException(CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeDPlus);
									case ExamGrades.D:
										throw new CourseRegistrationException(CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeD);
									case ExamGrades.F:
										throw new InvalidOperationException("F Grade can not be MaxExamGradeToBeImproved.");
									default:
										throw new NotImplementedEnumException(previousRegisteredCourse.GradeEnum);
								}
							else
								improvementCase = true;
						}
						else
						{
							switch (previousRegisteredCourse.StatusEnum)
							{
								case null:
								case RegisteredCours.Statuses.Incomplete:
								case RegisteredCours.Statuses.Deferred:
								case RegisteredCours.Statuses.WithdrawWithFee:
								case RegisteredCours.Statuses.WithdrawWithoutFee:
									break;
								case RegisteredCours.Statuses.AttendanceDefaulter:
								case RegisteredCours.Statuses.UnfairMeans:
									improvementCase = true;
									break;
								default:
									throw new NotImplementedEnumException(previousRegisteredCourse.StatusEnum);
							}
						}
					}

					if (Semester.IsSummer(this.CoursesRegister.OfferedSemesterID) && !this.CoursesRegister.StudentIsSummerRegularSemester)
						if (!improvementCase)
						{
							if (this.CoursesRegister.ByPassChecks.HasFlag(RegisteredCours.BypassedCheckTypes.ImprovementCheck))
							{
								if (this.CoursesRegister.CanByPassImprovementCheck)
								{
									this.BypassedChecks |= RegisteredCours.BypassedCheckTypes.ImprovementCheck;
									return;
								}
							}
							throw new CourseRegistrationException(CourseRegistrationStatus.OnlyImprovementsAllowed);
						}
					break;
				default:
					throw new NotImplementedEnumException(this.RoadmapCourse.CourseCategoryEnum);
			}
		}

		private void RegisterCourseStep5ValidateTimeBarConstraints()
		{
			if (!this.CoursesRegister.TimeBarredCase)
				return;
			var timeBarredStudent = this.AspireContext.TimeBarredStudents.Single(tbs => tbs.StudentID == this.CoursesRegister.StudentID && tbs.MinSemesterID <= this.CoursesRegister.OfferedSemesterID && this.CoursesRegister.OfferedSemesterID <= tbs.MaxSemesterID);
			if (!timeBarredStudent.CourseCategoriesAllowedEnum.HasFlag(this.RoadmapCourse.CourseCategoryEnum))
				switch (this.RoadmapCourse.CourseCategoryEnum)
				{
					case CourseCategories.Course:
						throw new CourseRegistrationException(CourseRegistrationStatus.TimeBarredCourseCategoryNotAllowedCourse);
					case CourseCategories.Internship:
						throw new CourseRegistrationException(CourseRegistrationStatus.TimeBarredCourseCategoryNotAllowedInternship);
					case CourseCategories.Lab:
						throw new CourseRegistrationException(CourseRegistrationStatus.TimeBarredCourseCategoryNotAllowedLab);
					case CourseCategories.Project:
						throw new CourseRegistrationException(CourseRegistrationStatus.TimeBarredCourseCategoryNotAllowedProject);
					case CourseCategories.Thesis:
						throw new CourseRegistrationException(CourseRegistrationStatus.TimeBarredCourseCategoryNotAllowedThesis);
					default:
						throw new NotImplementedEnumException(this.RoadmapCourse.CourseCategoryEnum);
				}
			if (timeBarredStudent.MaxCreditHours == null)
				return;
			var creditHours = this.AspireContext.RegisteredCourses.Where(rc => rc.StudentID == this.CoursesRegister.StudentID && rc.DeletedDate == null && timeBarredStudent.MinSemesterID <= rc.OfferedCours.SemesterID && rc.OfferedCours.SemesterID <= timeBarredStudent.MaxSemesterID).Select(rc => rc.Cours.CreditHours).ToList();
			var registeredCreditHours = creditHours.Any() ? creditHours.Sum(i => i) : 0;
			registeredCreditHours += this.RoadmapCourse.CreditHours;
			if (registeredCreditHours <= timeBarredStudent.MaxCreditHours)
				return;
			throw new CourseRegistrationException(CourseRegistrationStatus.TimeBarredCourseMaxCreditHoursCheckDoesNotMeet);
		}

		private void RegisterCourseStep6AddRegisterCourse()
		{
			this.AspireContext.RegisteredCourses.Add(new RegisteredCours
			{
				StudentID = this.CoursesRegister.StudentID,
				OfferedCourseID = this.OfferedCourseID,
				CourseID = this.RoadmapCourse.CourseID,
				RegistrationDate = DateTime.Now,
				RegisteredByUserLoginHistoryID = this.CoursesRegister.UserLoginHistoryID,
				BypassedChecksEnum = this.BypassedChecks,
				BypassedChecks = (byte)this.BypassedChecks,
				MarksRevised = false,
				DeletedByUserLoginHistoryID = null,
				DeletedDate = null,
				Assignments = null,
				Quizzes = null,
				Internals = null,
				Mid = null,
				Final = null,
				Total = null,
				Grade = null,
				GradeEnum = null,
				GradePoints = null,
				Product = null,
				Status = null,
				StatusEnum = null,
				StatusDate = null,
				FreezedStatus = null,
				FreezeStatusEnum = null,
				FreezedDate = null,
				Remarks = null,
				FeeDefaulter = true,
				StudentFeeID = null,
				MarksEntryLocked = false,
				MarksEntryLockedDate = null
			});
			var requestedCourses = this.AspireContext.RequestedCourses
				.Where(rc => rc.StudentID == this.CoursesRegister.StudentID && rc.OfferedCours.SemesterID == this.CoursesRegister.OfferedSemesterID);
			requestedCourses = requestedCourses
				.Where(rc => rc.OfferedCours.CourseID == this.RoadmapCourse.CourseID || rc.OfferedCours.CourseID == this.OfferedCourse.CourseID);
			this.AspireContext.RequestedCourses.RemoveRange(requestedCourses);
			this.AspireContext.SaveChanges(this.CoursesRegister.UserLoginHistoryID);
		}

		internal void Register()
		{
			if (this.Status != null)
				return;
			try
			{
				if (this.RequestOnly)
				{
					this.Status = CourseRegistrationStatus.Success;
					this.RequestCourseIfRequired();
				}
				else
				{
					if (this.RoadmapCourse == null)
						throw new InvalidOperationException("RoadmapCourse can not be null over here.");

					this.RegisterCourseStep1CheckSemesterWorkload();
					this.RegisterCourseStep2IsCourseAlreadyRegistered();
					this.RegisterCourseStep3CheckPreRequisites();
					this.RegisterCourseStep4CheckImprovements();
					this.RegisterCourseStep5ValidateTimeBarConstraints();
					this.RegisterCourseStep6AddRegisterCourse();
				}
				this.Status = CourseRegistrationStatus.Success;
			}
			catch (CourseRegistrationException exc)
			{
				if (exc.Status == CourseRegistrationStatus.Success)
					throw new InvalidOperationException($"Status cannot be {exc.Status.ToString()}.");
				this.Status = exc.Status;
				this.RequestCourseIfRequired();
			}
		}
	}
}