﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.CourseRegistration.Common
{
	public static class ExtensionMethods
	{
		public sealed class StudentClass
		{
			public int AdmissionOpenProgramID { get; internal set; }
			public int ProgramID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterID { get; internal set; }
			public short SemesterNo { get; internal set; }
			public SemesterNos SemesterNoEnum => (SemesterNos)this.SemesterNo;
			public string SemesterNoFullName => this.SemesterNoEnum.ToFullName();
			public int Section { get; internal set; }
			public Sections SectionEnum => (Sections)this.Section;
			public string SectionFullName => this.SectionEnum.ToFullName();
			public byte Shift { get; internal set; }
			public Shifts ShiftEnum => (Shifts)this.Shift;
			public string ShiftFullName => this.ShiftEnum.ToFullName();
			public string ClassName => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNoEnum, this.SectionEnum, this.ShiftEnum);
		}

		internal static StudentClass GetStudentClass(this AspireContext aspireContext, int studentID, short semesterID)
		{
			var studentSemester = aspireContext.StudentSemesters.Where(s => s.StudentID == studentID && s.SemesterID <= semesterID).OrderByDescending(s => s.SemesterID).Select(s => new
			{
				IntakeSemesterID = s.Student.AdmissionOpenProgram.SemesterID,
				s.Student.AdmissionOpenProgramID,
				s.Student.AdmissionOpenProgram.IsSummerRegularSemester,
				s.Student.AdmissionOpenProgram.Program.Duration,
				s.Student.AdmissionOpenProgram.ProgramID,
				s.Student.AdmissionOpenProgram.Program.ProgramAlias,
				s.SemesterID,
				s.SemesterNo,
				s.Section,
				s.Shift,
			}).FirstOrDefault();
			if (studentSemester == null)
				return null;

			var nextSemesterNoEnum = (SemesterNos)studentSemester.SemesterNo;
			if (studentSemester.SemesterID != semesterID)
				nextSemesterNoEnum = nextSemesterNoEnum.NextSemesterNo(semesterID, Semester.GetSemesterType(studentSemester.IntakeSemesterID), (ProgramDurations)studentSemester.Duration, studentSemester.IsSummerRegularSemester);
			return new StudentClass
			{
				AdmissionOpenProgramID = studentSemester.AdmissionOpenProgramID,
				ProgramID = studentSemester.ProgramID,
				ProgramAlias = studentSemester.ProgramAlias,
				SemesterID = studentSemester.SemesterID,
				SemesterNo = (short)nextSemesterNoEnum,
				Section = studentSemester.Section,
				Shift = studentSemester.Shift,
			};
		}

		public class OfferedCourse
		{
			internal OfferedCourse() { }
			internal class Class
			{
				public short SemesterNo { get; set; }
				public int Section { get; set; }
				public byte Shift { get; set; }
			}

			public int OfferedCourseID { get; set; }
			public short SemesterID { get; set; }
			public int AdmissionOpenProgramID { get; set; }
			public int CourseID { get; set; }
			public string CourseCode { get; set; }
			public string Title { get; set; }
			public decimal CreditHours { get; set; }
			public int? ProgramMajorID { get; set; }
			public string Majors { get; set; }

			public string Program { get; set; }
			public short SemesterNo { get; set; }
			public int Section { get; set; }
			public byte Shift { get; set; }
			public bool SpecialOffered { get; set; }
			internal List<Class> Classes { get; set; }
			public string ClassNamesHtmlEncoded
			{
				get
				{
					var classes = new List<string> { AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift).HtmlEncode() };
					if (this.Classes != null)
						classes.AddRange(this.Classes.Select(c => $"<small><em>{AspireFormats.GetClassName(this.Program, c.SemesterNo, c.Section, c.Shift).HtmlEncode()}</em></small>"));
					return string.Join("<br />", classes);
				}
			}

			public short? EquivalentSemesterID { get; set; }
			public int? EquivalentCourseID { get; set; }
			public string EquivalentCourseCode { get; set; }
			public string EquivalentTitle { get; set; }
			public decimal? EquivalentCreditHours { get; set; }
			public int? EquivalentProgramMajorID { get; set; }
			public string EquivalentMajors { get; set; }
			public Guid? EquivalentStatus { get; set; }
			public int ProgramID { get; set; }
			public byte Status { get; set; }
			public OfferedCours.Statuses StatusEnum => (OfferedCours.Statuses)this.Status;

			internal void AddClass(short semesterNo, int section, byte shift)
			{
				if (this.Classes == null)
					this.Classes = new List<Class> { new Class { SemesterNo = semesterNo, Section = section, Shift = shift } };
				else
					this.Classes.Add(new Class { SemesterNo = semesterNo, Section = section, Shift = shift });
			}

			public bool ContainsClass(StudentClass studentClass)
			{
				if (this.ProgramID == studentClass.ProgramID && this.SemesterNo == studentClass.SemesterNo && this.Section == studentClass.Section && this.Shift == studentClass.Shift)
					return true;
				if (this.Classes != null && this.Classes.Any(c => this.ProgramID == studentClass.ProgramID && c.SemesterNo == studentClass.SemesterNo && c.Section == studentClass.Section && c.Shift == studentClass.Shift))
					return true;
				return false;
			}
		}

		internal static List<OfferedCourse> GetOfferedCourses(this AspireContext aspireContext, int instituteID, int admissionOpenProgramID, int offeredSemesterID)
		{
			var eqCourses = from ce in aspireContext.CourseEquivalents
							join c in aspireContext.Courses on ce.EquivalentCourseID equals c.CourseID
							where c.AdmissionOpenProgramID == admissionOpenProgramID && c.AdmissionOpenProgram.Program.InstituteID == instituteID
							select new
							{
								ce.CourseID,
								ce.EquivalentCourseID,
								EquivalentSemesterID = c.AdmissionOpenProgram.SemesterID,
								EquivalentCourseCode = c.CourseCode,
								EquivalentTitle = c.Title,
								EquivalentCreditHours = c.CreditHours,
								EquivalentProgramMajorID = (int?)c.ProgramMajor.ProgramMajorID,
								EquivalentMajors = c.ProgramMajor.Majors,
								EquivalentStatus = c.Status
							};

			var offeredCourses = (from oc in aspireContext.OfferedCourses.Where(oc => oc.SemesterID == offeredSemesterID && oc.Cours.AdmissionOpenProgram.Program.InstituteID == instituteID)
								  join eqc in eqCourses on oc.CourseID equals eqc.CourseID into grp
								  from g in grp.DefaultIfEmpty()
								  select new OfferedCourse
								  {
									  OfferedCourseID = oc.OfferedCourseID,
									  AdmissionOpenProgramID = oc.Cours.AdmissionOpenProgramID,
									  SemesterID = oc.SemesterID,
									  ProgramID = oc.Cours.AdmissionOpenProgram.ProgramID,
									  CourseID = oc.CourseID,
									  CourseCode = oc.Cours.CourseCode,
									  Title = oc.Cours.Title,
									  CreditHours = oc.Cours.CreditHours,
									  ProgramMajorID = oc.Cours.ProgramMajor.ProgramMajorID,
									  Majors = oc.Cours.ProgramMajor.Majors,
									  Program = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
									  SemesterNo = oc.SemesterNo,
									  Section = oc.Section,
									  Shift = oc.Shift,
									  SpecialOffered = oc.SpecialOffered,
									  Status = oc.Status,
									  EquivalentSemesterID = g == null ? (short?)null : g.EquivalentSemesterID,
									  EquivalentCourseID = g == null ? (int?)null : g.EquivalentCourseID,
									  EquivalentCourseCode = g == null ? null : g.EquivalentCourseCode,
									  EquivalentTitle = g == null ? null : g.EquivalentTitle,
									  EquivalentCreditHours = g == null ? (decimal?)null : g.EquivalentCreditHours,
									  EquivalentProgramMajorID = g == null ? null : g.EquivalentProgramMajorID,
									  EquivalentMajors = g == null ? null : g.EquivalentMajors,
									  EquivalentStatus = g == null ? (Guid?)null : g.EquivalentStatus
								  }).ToList();
			var offeredClasses = aspireContext.OfferedCourseClasses.Where(occ => occ.OfferedCours.SemesterID == offeredSemesterID && occ.OfferedCours.Cours.AdmissionOpenProgram.Program.InstituteID == instituteID).Select(occ => new
			{
				occ.OfferedCourseID,
				occ.SemesterNo,
				occ.Section,
				occ.Shift,
			}).ToList();

			foreach (var offeredClass in offeredClasses)
				offeredCourses.Find(oc => oc.OfferedCourseID == offeredClass.OfferedCourseID).AddClass(offeredClass.SemesterNo, offeredClass.Section, offeredClass.Shift);

			var ocs = offeredCourses.FindAll(oc => oc.AdmissionOpenProgramID == admissionOpenProgramID && oc.EquivalentCourseID == null);
			foreach (var oc in ocs)
			{
				oc.EquivalentCourseID = oc.CourseID;
				oc.EquivalentCourseCode = oc.CourseCode;
				oc.EquivalentTitle = oc.Title;
				oc.EquivalentCreditHours = oc.CreditHours;
				oc.EquivalentProgramMajorID = oc.ProgramMajorID;
				oc.EquivalentMajors = oc.Majors;
			}
			return offeredCourses;
		}

		internal static List<OfferedCourse> GetOfferedCourses(this AspireContext aspireContext, int instituteID, int admissionOpenProgramID, int offeredSemesterID, StudentClass studentClass)
		{
			var offeredCourses = aspireContext.GetOfferedCourses(instituteID, admissionOpenProgramID, offeredSemesterID);
			if (studentClass != null)
				offeredCourses = offeredCourses.FindAll(oc => oc.ContainsClass(studentClass) && oc.EquivalentCourseID != null);
			return offeredCourses;
		}

		internal static short? GetRegistrationOpenSemesterID(this AspireContext aspireContext, int programID, CourseRegistrationSetting.Types typeEnum)
		{
			var today = DateTime.Today;
			var type = typeEnum.ToByte();
			return aspireContext.CourseRegistrationSettings.Where(s => s.ProgramID == programID && s.Type == type && s.Status == CourseRegistrationSetting.StatusActive && s.FromDate <= today && today <= s.ToDate).Select(s => (short?)s.SemesterID).SingleOrDefault();
		}

		public static IEnumerable<string> GetErrorMessages(this CoursesRegister coursesRegister)
		{
			switch (coursesRegister.Status)
			{
				case StudentRegistrationStatus.Success:
					break;
				case StudentRegistrationStatus.UniversityRegistrationNoNotAssigned:
					yield return "University Registration No. is not yet assigned. Contact Admissions Office.";
					break;
				case StudentRegistrationStatus.AdmissionCanceled:
					yield return "Admission has been canceled.";
					break;
				case StudentRegistrationStatus.AdmissionHasBeenCanceledPreviousRegularSemesterNotRegistered:
					yield return "Admission has been canceled because no course has been registered in previous regular semester.";
					break;
				case StudentRegistrationStatus.CannotRegisterBeforeIntakeSemester:
					yield return "Cannot register before intake semester.";
					break;
				case StudentRegistrationStatus.CourseRegistrationBlockedByAnyDepartment:
					yield return "Course Registration has been blocked by any department.";
					break;
				case StudentRegistrationStatus.Dropped:
					yield return "Student status is dropped.";
					break;
				case StudentRegistrationStatus.Expelled:
					yield return "Student status is expelled.";
					break;
				case StudentRegistrationStatus.Graduated:
					yield return "Student status is graduated.";
					break;
				case StudentRegistrationStatus.Left:
					yield return "Student status is left.";
					break;
				case StudentRegistrationStatus.OfferedSemesterIsFreeze:
					yield return $"Student has freeze the {coursesRegister.OfferedSemesterID.ToSemesterString()} Semester.";
					break;
				case StudentRegistrationStatus.ProgramChanged:
					yield return $"Student has change the program.";
					break;
				case StudentRegistrationStatus.Blocked:
					yield return $"Student status is blocked.";
					break;
				case StudentRegistrationStatus.Relegated:
					yield return $"Student has been relegated.";
					break;
				case StudentRegistrationStatus.Rusticated:
					yield return $"Student has been rusticated.";
					break;
				case StudentRegistrationStatus.TimeBarred:
					yield return $"Student cannot register any course because of time-bar.";
					break;
				case StudentRegistrationStatus.TimeBarredNotAuthorized:
					yield return $"You are not authorized to register course for time-barred students.";
					break;
				case StudentRegistrationStatus.TransferredToOtherCampus:
					yield return $"Student has been transferred to other campus.";
					break;
				case StudentRegistrationStatus.SummerSemesterNotAllowedForStudent:
					yield return $"Student cannot register in non-regular semester.";
					break;
				case StudentRegistrationStatus.ChangeInStudentSectionShiftNotAuthorized:
					yield return $"You are not authorized to change the class of student.";
					break;
				case StudentRegistrationStatus.OnChanceContactCoordinator:
					yield return $"Contact Coordinator. Result Remarks: Chance.";
					break;
				case StudentRegistrationStatus.OnProbationContactCoordinator:
					yield return $"Contact Coordinator. Result Remarks: Probation.";
					break;
				case StudentRegistrationStatus.CourseRegistrationIsNotOpen:
					yield return $"Course registration has been closed or not yet opened.";
					break;
				case StudentRegistrationStatus.StudentCannotRegisterForFirstTimeContactCoordinator:
					yield return $"First semester cannot be registered by student. Contact your Course Coordinator.";
					break;
				case StudentRegistrationStatus.TimeBarredNotAuthorizedToStudents:
					yield return $"Student cannot registered his courses when he time barred. Contact your Course Coordinator if you are allowed for course registration.";
					break;
				case null:
					throw new InvalidOperationException();
				default:
					throw new NotImplementedEnumException(coursesRegister.Status);
			}

			if (coursesRegister.Status == StudentRegistrationStatus.Success)
				foreach (var course in coursesRegister.Courses)
				{
					if (course.Status == null)
						throw new InvalidOperationException();

					switch (course.Status.Value)
					{
						case CourseRegistrationStatus.Success:
							if (course.AddedToRequestedList)
								yield return $"{course.RoadmapCourse.Title} has been added to requested list. Contact your course coordinator for further process.";
							break;
						case CourseRegistrationStatus.AlreadyRegisteredCourseIDInCurrentSemester:
							yield return $"{course.RoadmapCourse.Title} is already registered.";
							break;
						case CourseRegistrationStatus.AlreadyRegisteredInOfferedCourseID:
							yield return $"{course.RoadmapCourse.Title} is already registered.";
							break;
						case CourseRegistrationStatus.OnlyImprovementsAllowed:
							yield return $"Only improvement cases are allowed.";
							break;
						case CourseRegistrationStatus.PreRequisitesNotCleared:
							yield return $"{course.RoadmapCourse.Title} cannot be registered. Reason: Pre-Requisites are not cleared.";
							break;
						case CourseRegistrationStatus.EquivalentCourseNotFound:
							yield return $"Equivalent course for {course.OfferedCourse.Title} not found in the student road-map.";
							break;
						case CourseRegistrationStatus.OfferedCourseNotFound:
							yield return $"Offered course not found.";
							break;
						case CourseRegistrationStatus.RoadmapCourseNotFound:
							yield return $"Road-map course not found.";
							break;
						case CourseRegistrationStatus.NotAuthorizedToBypassPreRequisites:
							yield return $"You are not authorized to bypass pre-requisites check.";
							break;
						case CourseRegistrationStatus.TimeBarredCourseCategoryNotAllowedCourse:
							yield return $"This Time-barred Student is not allowed to register course of category \"Course\".";
							break;
						case CourseRegistrationStatus.TimeBarredCourseCategoryNotAllowedInternship:
							yield return $"This Time-barred Student is not allowed to register course of category \"Internship\".";
							break;
						case CourseRegistrationStatus.TimeBarredCourseCategoryNotAllowedLab:
							yield return $"This Time-barred Student is not allowed to register course of category \"Lab\".";
							break;
						case CourseRegistrationStatus.TimeBarredCourseCategoryNotAllowedProject:
							yield return $"This Time-barred Student is not allowed to register course of category \"Project\".";
							break;
						case CourseRegistrationStatus.TimeBarredCourseCategoryNotAllowedThesis:
							yield return $"This Time-barred Student is not allowed to register course of category \"Thesis\".";
							break;
						case CourseRegistrationStatus.TimeBarredCourseMaxCreditHoursCheckDoesNotMeet:
							yield return $"This Time-barred Student cannot register because of maximum credit hours allowed check.";
							break;
						case CourseRegistrationStatus.ClassFull:
							yield return $"\"{course.OfferedCourse.Title}\" cannot be registered because maximum students limit \"{course.OfferedCourse.MaxClassStrength}\" in class reached.";
							break;
						case CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeA:
							yield return $"\"{course.RoadmapCourse.Title}\" cannot be registered. Reason: Cannot improve from Grade A.";
							break;
						case CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeAMinus:
							yield return $"\"{course.RoadmapCourse.Title}\" cannot be registered. Reason: Cannot improve from Grade A-.";
							break;
						case CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeBPlus:
							yield return $"\"{course.RoadmapCourse.Title}\" cannot be registered. Reason: Cannot improve from Grade B+.";
							break;
						case CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeB:
							yield return $"\"{course.RoadmapCourse.Title}\" cannot be registered. Reason: Cannot improve from Grade B.";
							break;
						case CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeBMinus:
							yield return $"\"{course.RoadmapCourse.Title}\" cannot be registered. Reason: Cannot improve from Grade B-.";
							break;
						case CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeCPlus:
							yield return $"\"{course.RoadmapCourse.Title}\" cannot be registered. Reason: Cannot improve from Grade C+.";
							break;
						case CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeC:
							yield return $"\"{course.RoadmapCourse.Title}\" cannot be registered. Reason: Cannot improve from Grade C.";
							break;
						case CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeCMinus:
							yield return $"\"{course.RoadmapCourse.Title}\" cannot be registered. Reason: Cannot improve from Grade C-.";
							break;
						case CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeDPlus:
							yield return $"\"{course.RoadmapCourse.Title}\" cannot be registered. Reason: Cannot improve from Grade D+.";
							break;
						case CourseRegistrationStatus.AlreadyClearedAndCannotImproveFromGradeD:
							yield return $"\"{course.RoadmapCourse.Title}\" cannot be registered. Reason: Cannot improve from Grade D.";
							break;
						case CourseRegistrationStatus.MaxAllowedCoursesPerSemesterConstraintFailed:
							yield return $"Course cannot be registered because maximum allowed courses have already been registered.";
							break;
						case CourseRegistrationStatus.MaxAllowedCreditHoursPerSemesterConstraintFailed:
							yield return $"Course cannot be registered because maximum allowed credit hours have already been registered.";
							break;
						default:
							throw new NotImplementedEnumException(course.Status);
					}
				}
		}

		public static IEnumerable<string> GetWarningMessages(this CoursesRegister coursesRegister)
		{
			if (coursesRegister.Status == StudentRegistrationStatus.Success)
				foreach (var course in coursesRegister.Courses)
					foreach (RegisteredCours.BypassedCheckTypes byPassCheckType in Enum.GetValues(course.BypassedChecks.GetType()))
					{
						if (course.BypassedChecks.HasFlag(byPassCheckType))
							switch (byPassCheckType)
							{
								case RegisteredCours.BypassedCheckTypes.None:
									break;
								case RegisteredCours.BypassedCheckTypes.PreRequisites:
									yield return $"Pre-requisites of {course.RoadmapCourse.Title} are not cleared and this check has been bypassed.";
									break;
								case RegisteredCours.BypassedCheckTypes.ImprovementCheck:
									yield return $"{course.RoadmapCourse.Title} is not an improvement case and this check has been bypassed.";
									break;
								case RegisteredCours.BypassedCheckTypes.RegistrationOpenCheck:
									yield return $"Registration is not opened now and this check has been bypassed.";
									break;
								case RegisteredCours.BypassedCheckTypes.MaxAllowedCoursesPerSemester:
									yield return $"Check for maximum allowed courses for Registeration has been bypassed.";
									break;
								case RegisteredCours.BypassedCheckTypes.MaxAllowedCreditHoursPerSemester:
									yield return $"Check for maximum allowed credit hours for Registeration has been bypassed.";
									break;
								default:
									throw new NotImplementedEnumException(byPassCheckType);
							}
					}
		}

		public static IEnumerable<string> GetSuccessMessages(this CoursesRegister coursesRegister)
		{
			if (coursesRegister.Status == StudentRegistrationStatus.Success)
			{
				if (!coursesRegister.Courses.Any())
					yield return $"Student Class has been updated.";
				else
				{
					foreach (var course in coursesRegister.Courses)
						if (course.AddedToRequestedList)
							yield return $"{course.OfferedCourse.Title} has been added to request list. Contact course coordinator for registering this course.";
						else if (course.Status == CourseRegistrationStatus.Success)
							yield return $"{course.RoadmapCourse.Title} has been registered.";
				}
			}
		}
	}
}
