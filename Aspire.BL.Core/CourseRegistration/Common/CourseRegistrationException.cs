using System;

namespace Aspire.BL.Core.CourseRegistration.Common
{
	internal sealed class CourseRegistrationException : Exception
	{
		public readonly CourseRegistrationStatus Status;
		public CourseRegistrationException(CourseRegistrationStatus status)
		{
			this.Status = status;
		}
	}
}