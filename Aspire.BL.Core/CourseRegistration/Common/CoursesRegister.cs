﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Aspire.BL.Core.CourseRegistration.Common
{
	public sealed class CoursesRegister
	{
		internal enum UserTypes
		{
			Student,
			Staff
		}

		private Guid LoginSessionGuid { get; }
		public int StudentID { get; }
		public short OfferedSemesterID { get; }
		private SemesterNos? SemesterNoEnum { get; }
		private Sections? SectionEnum { get; }
		private Shifts? ShiftEnum { get; }
		public RegisteredCours.BypassedCheckTypes ByPassChecks { get; }
		internal bool RequestIfRegistrationFailed { get; }
		public StudentRegistrationStatus? Status { get; private set; }
		private readonly List<Course> _courses;
		public IReadOnlyList<Course> Courses => this._courses.AsReadOnly();
		internal int UserLoginHistoryID { get; private set; }
		internal UserTypes UserType { get; }
		internal int StudentInstituteID { get; private set; }
		private int? StudentRegistrationNo { get; set; }
		internal int StudentAdmissionOpenProgramID { get; private set; }
		internal bool IsStudentCreditsTransferredExists { get; private set; }
		public bool IsStudentExemptedCoursesExists { get; private set; }
		internal bool StudentIsSummerRegularSemester { get; private set; }
		private short StudentMaxSemesterID { get; set; }
		private short StudentIntakeSemesterID { get; set; }
		private Model.Entities.Student.Statuses? StudentStatusEnum { get; set; }
		internal byte MaxAllowedCoursesPerSemester { get; private set; }
		internal byte MaxAllowedCreditHoursPerSemester { get; private set; }
		internal bool TimeBarredCase { get; private set; }
		private bool IsStudentFirstSemester => this.OfferedSemesterID == this.StudentIntakeSemesterID;
		private bool StudentCanRegisterInNonRegularSemester => false;
		internal ExamGrades MaxExamGradeToBeImproved => ExamGrades.CPlus;
		internal AspireContext AspireContext { get; }
		private bool? _canByPassPreRequisites;
		internal bool CanByPassPreRequisites
		{
			get
			{
				switch (this.UserType)
				{
					case UserTypes.Student:
						return false;
					case UserTypes.Staff:
						if (this._canByPassPreRequisites != null)
							return this._canByPassPreRequisites.Value;
						this._canByPassPreRequisites = this.AspireContext.TryDemandStaffPermissions(this.LoginSessionGuid, StaffPermissions.CourseRegistration.CanByPassPreRequisitesCheck, UserGroupPermission.PermissionValues.Allowed)?.PermissionValueEnum == UserGroupPermission.PermissionValues.Allowed;
						return this._canByPassPreRequisites.Value;
					default:
						throw new NotImplementedEnumException(this.UserType);
				}
			}
		}
		private bool? _canByPassImprovementCheck;
		internal bool CanByPassImprovementCheck
		{
			get
			{
				switch (this.UserType)
				{
					case UserTypes.Student:
						return false;
					case UserTypes.Staff:
						if (this._canByPassImprovementCheck != null)
							return this._canByPassImprovementCheck.Value;
						this._canByPassImprovementCheck = this.AspireContext.TryDemandStaffPermissions(this.LoginSessionGuid, StaffPermissions.CourseRegistration.CanByPassImprovementCheck, UserGroupPermission.PermissionValues.Allowed)?.PermissionValueEnum == UserGroupPermission.PermissionValues.Allowed;
						return this._canByPassImprovementCheck.Value;
					default:
						throw new NotImplementedEnumException(this.UserType);
				}
			}
		}

		private StudentSemester _currentStudentSemester;
		private StudentSemester CurrentStudentSemester
			=> this._currentStudentSemester
				?? (this._currentStudentSemester
					= this.AspireContext.StudentSemesters.SingleOrDefault(ss => ss.StudentID == this.StudentID && ss.SemesterID == this.OfferedSemesterID));

		private short PreviousRegularSemesterID => Semester.PreviousRegularSemester(this.OfferedSemesterID, this.StudentIsSummerRegularSemester);
		private StudentSemester _previousRegularStudentSemester;
		private StudentSemester PreviousRegularStudentSemester
			=> this._previousRegularStudentSemester
				?? (this._previousRegularStudentSemester
					= this.AspireContext.StudentSemesters.SingleOrDefault(ss => ss.StudentID == this.StudentID && ss.SemesterID == this.PreviousRegularSemesterID));

		#region Constructor Related

		private CoursesRegister(AspireContext aspireContext, UserTypes userType, int studentID, short offeredSemesterID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, bool requestIfRegistrationFailed, RegisteredCours.BypassedCheckTypes byPassChecks, Guid loginSessionGuid)
		{
			this.UserType = userType;
			this.LoginSessionGuid = loginSessionGuid;
			this.StudentID = studentID;
			this.OfferedSemesterID = offeredSemesterID;
			this.SemesterNoEnum = semesterNoEnum;
			this.SectionEnum = sectionEnum;
			this.ShiftEnum = shiftEnum;
			this.RequestIfRegistrationFailed = requestIfRegistrationFailed;
			this.ByPassChecks = byPassChecks;
			this._courses = new List<Course>();
			this.AspireContext = aspireContext;
		}

		internal static CoursesRegister CreateForStudent(AspireContext aspireContext, int studentID, short offeredSemesterID, Guid loginSessionGuid)
		{
			var studentSemester = aspireContext.StudentSemesters.Where(ss => ss.StudentID == studentID && ss.SemesterID <= offeredSemesterID && ss.Status == StudentSemester.StatusRegistered).Select(ss => new
			{
				IntakeSemesterID = ss.Student.AdmissionOpenProgram.SemesterID,
				ss.Student.AdmissionOpenProgram.IsSummerRegularSemester,
				ss.Student.AdmissionOpenProgram.Program.Duration,
				ss.SemesterID,
				ss.SemesterNo,
				ss.Section,
				ss.Shift,
			}).OrderByDescending(o => o.SemesterID).FirstOrDefault();
			if (studentSemester == null)
				throw new StudentRegistrationException(StudentRegistrationStatus.StudentCannotRegisterForFirstTimeContactCoordinator);
			var semesterNoEnum = (SemesterNos)studentSemester.SemesterNo;
			var sectionEnum = (Sections)studentSemester.Section;
			var shiftEnum = (Shifts)studentSemester.Shift;
			if (studentSemester.SemesterID != offeredSemesterID)
				semesterNoEnum = semesterNoEnum.NextSemesterNo(offeredSemesterID, Semester.GetSemesterType(studentSemester.IntakeSemesterID), (ProgramDurations)studentSemester.Duration, studentSemester.IsSummerRegularSemester);
			return new CoursesRegister(aspireContext, UserTypes.Student, studentID, offeredSemesterID, semesterNoEnum, sectionEnum, shiftEnum, true, RegisteredCours.BypassedCheckTypes.None, loginSessionGuid);
		}

		internal static CoursesRegister CreateForStaffToRegisterCourse(AspireContext aspireContext, int studentID, short offeredSemesterID, SemesterNos semesterNoEnum, Sections sectionEnum, Shifts shiftEnum, int offeredCourseID, int equivalentCourseID, RegisteredCours.BypassedCheckTypes byPassChecks, Guid loginSessionGuid)
		{
			var courseRegister = new CoursesRegister(aspireContext, UserTypes.Staff, studentID, offeredSemesterID, semesterNoEnum, sectionEnum, shiftEnum, false, byPassChecks, loginSessionGuid);
			courseRegister.AddCourse(offeredCourseID, equivalentCourseID, false);
			return courseRegister;
		}

		internal static CoursesRegister CreateForStaffToChangeClass(AspireContext aspireContext, int studentID, short offeredSemesterID, SemesterNos semesterNoEnum, Sections sectionEnum, Shifts shiftEnum, Guid loginSessionGuid)
		{
			return new CoursesRegister(aspireContext, UserTypes.Staff, studentID, offeredSemesterID, semesterNoEnum, sectionEnum, shiftEnum, false, RegisteredCours.BypassedCheckTypes.RegistrationOpenCheck, loginSessionGuid);
		}

		public void AddCourse(int offeredCourseID, int? equivalentCourseID, bool requestOnly)
		{
			this._courses.Add(new Course(offeredCourseID, equivalentCourseID, this, requestOnly));
		}

		#endregion

		#region Process

		private void Initialize()
		{
			var student = this.AspireContext.Students
				.Where(s => s.StudentID == this.StudentID)
				.Select(s => new
				{
					StudentStatusEnum = (Model.Entities.Student.Statuses?)s.Status,
					s.RegistrationNo,
					s.AdmissionOpenProgram.Program.InstituteID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgramID,
					s.AdmissionOpenProgram.SemesterID,
					s.AdmissionOpenProgram.MaxSemesterID,
					s.AdmissionOpenProgram.Program.MaxAllowedCoursesPerSemester,
					s.AdmissionOpenProgram.Program.MaxAllowedCreditHoursPerSemester,
					ProgramDuration = (ProgramDurations)s.AdmissionOpenProgram.Program.Duration,
					s.AdmissionOpenProgram.IsSummerRegularSemester,
					CreditsTransfered = s.StudentCreditsTransfers.SelectMany(sct => sct.StudentCreditsTransferredCourses).Any(c => c.EquivalentGrade != null),
					ExemptedCourses = s.StudentExemptedCourses.Any()
				}).SingleOrDefault();
			if (student == null)
				throw new DataException("Student record not found.");

			var courseRegistrationSettingsQuery = this.AspireContext.CourseRegistrationSettings
				.Where(crs => crs.ProgramID == student.ProgramID)
				.Where(crs => crs.Status == CourseRegistrationSetting.StatusActive)
				.Where(crs => crs.SemesterID == this.OfferedSemesterID)
				.Where(crs => crs.FromDate <= DateTime.Today && DateTime.Today <= crs.ToDate);

			switch (this.UserType)
			{
				case UserTypes.Student:
					var studentLoginHistory = this.AspireContext.DemandStudentActiveSession(this.LoginSessionGuid, SubUserTypes.None);
					this.UserLoginHistoryID = studentLoginHistory.UserLoginHistoryID;
					if (!courseRegistrationSettingsQuery.Any(crs => crs.Type == CourseRegistrationSetting.TypeStudent))
						throw new StudentRegistrationException(StudentRegistrationStatus.CourseRegistrationIsNotOpen);
					break;
				case UserTypes.Staff:
					var staffLoginHistory = this.AspireContext.DemandStaffPermissions(this.LoginSessionGuid, StaffPermissions.CourseRegistration.RegisterCourse, UserGroupPermission.PermissionValues.Allowed, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
					this.UserLoginHistoryID = staffLoginHistory.UserLoginHistoryID;
					if (!courseRegistrationSettingsQuery.Any(crs => crs.Type == CourseRegistrationSetting.TypeCoordinator))
					{
						if (this.ByPassChecks.HasFlag(RegisteredCours.BypassedCheckTypes.RegistrationOpenCheck))
							this.AspireContext.DemandStaffPermissions(this.LoginSessionGuid, StaffPermissions.CourseRegistration.CanByPassRegistrationOpenCheck, UserGroupPermission.PermissionValues.Allowed);
						else
							throw new StudentRegistrationException(StudentRegistrationStatus.CourseRegistrationIsNotOpen);
					}
					break;
				default:
					throw new NotImplementedEnumException(this.UserType);
			}

			this.StudentInstituteID = student.InstituteID;
			this.StudentRegistrationNo = student.RegistrationNo;
			this.StudentAdmissionOpenProgramID = student.AdmissionOpenProgramID;
			this.StudentIntakeSemesterID = student.SemesterID;
			this.StudentMaxSemesterID = student.MaxSemesterID;
			this.StudentIsSummerRegularSemester = student.IsSummerRegularSemester;
			this.IsStudentCreditsTransferredExists = student.CreditsTransfered;
			this.IsStudentExemptedCoursesExists = student.ExemptedCourses;
			this.StudentStatusEnum = student.StudentStatusEnum;
			this.MaxAllowedCoursesPerSemester = student.MaxAllowedCoursesPerSemester;
			this.MaxAllowedCreditHoursPerSemester = student.MaxAllowedCreditHoursPerSemester;

			this.Courses?.ForEach(c => c.Initialize());
		}

		private void CheckStudentStatusStep1StudentStatus()
		{
			switch (this.StudentStatusEnum)
			{
				case null:
					break;
				case Model.Entities.Student.Statuses.TransferredToOtherCampus:
					throw new StudentRegistrationException(StudentRegistrationStatus.TransferredToOtherCampus);
				case Model.Entities.Student.Statuses.AdmissionCancelled:
					throw new StudentRegistrationException(StudentRegistrationStatus.AdmissionCanceled);
				case Model.Entities.Student.Statuses.Dropped:
					throw new StudentRegistrationException(StudentRegistrationStatus.Dropped);
				case Model.Entities.Student.Statuses.Graduated:
					throw new StudentRegistrationException(StudentRegistrationStatus.Graduated);
				case Model.Entities.Student.Statuses.Expelled:
					throw new StudentRegistrationException(StudentRegistrationStatus.Expelled);
				case Model.Entities.Student.Statuses.Rusticated:
					throw new StudentRegistrationException(StudentRegistrationStatus.Rusticated);
				case Model.Entities.Student.Statuses.Left:
					throw new StudentRegistrationException(StudentRegistrationStatus.Left);
				case Model.Entities.Student.Statuses.ProgramChanged:
					throw new StudentRegistrationException(StudentRegistrationStatus.ProgramChanged);
				case Model.Entities.Student.Statuses.Blocked:
					throw new StudentRegistrationException(StudentRegistrationStatus.Blocked);
				case Model.Entities.Student.Statuses.Deferred:
					//These are valid for registration status checking.
					break;
				default:
					throw new NotImplementedEnumException(this.StudentStatusEnum);
			}

			if (this.StudentIntakeSemesterID != this.OfferedSemesterID && this.StudentRegistrationNo == null)
				throw new StudentRegistrationException(StudentRegistrationStatus.UniversityRegistrationNoNotAssigned);
		}

		private void CheckStudentStatusStep2ValidateOfferedSemester()
		{
			if (this.OfferedSemesterID < this.StudentIntakeSemesterID)
				throw new StudentRegistrationException(StudentRegistrationStatus.CannotRegisterBeforeIntakeSemester);
		}

		private void CheckStudentStatusStep3CheckTimeBar()
		{
			if (this.OfferedSemesterID <= this.StudentMaxSemesterID)
			{
				this.TimeBarredCase = false;
				return;
			}

			var timeBarredStudentExists = this.AspireContext.TimeBarredStudents
				.Any(tbs => tbs.StudentID == this.StudentID && tbs.MinSemesterID <= this.OfferedSemesterID && this.OfferedSemesterID <= tbs.MaxSemesterID);
			if (!timeBarredStudentExists)
				throw new StudentRegistrationException(StudentRegistrationStatus.TimeBarred);

			switch (this.UserType)
			{
				case UserTypes.Student:
					throw new StudentRegistrationException(StudentRegistrationStatus.TimeBarredNotAuthorizedToStudents);
				case UserTypes.Staff:
					var canRegisterTimeBarredStudents = this.AspireContext.TryDemandStaffPermissions(this.LoginSessionGuid, StaffPermissions.CourseRegistration.CanRegisterTimeBarredStudents, UserGroupPermission.PermissionValues.Allowed)?.PermissionValueEnum == UserGroupPermission.PermissionValues.Allowed;
					if (!canRegisterTimeBarredStudents)
						throw new StudentRegistrationException(StudentRegistrationStatus.TimeBarredNotAuthorized);
					this.TimeBarredCase = true;
					break;
				default:
					throw new NotImplementedEnumException(this.UserType);
			}
		}

		private void CheckStudentStatusStep4CheckSemesterFreezeStatus()
		{
			if (this.CurrentStudentSemester?.FreezedStatus != null)
				throw new StudentRegistrationException(StudentRegistrationStatus.OfferedSemesterIsFreeze);
		}

		private void CheckStudentStatusStep5CheckPreviousSemesterRegistration()
		{
			if (this.IsStudentFirstSemester)
				return;
			if (this.PreviousRegularStudentSemester != null)
				if (this.PreviousRegularStudentSemester.FreezedStatus != null
					|| this.PreviousRegularStudentSemester.StatusEnum == StudentSemester.Statuses.Registered)
					return;
			var canceledAdmissionResolved = this.AspireContext.CancelledAdmissions.Any(ca => ca.StudentID == this.StudentID && ca.SemesterID == this.PreviousRegularSemesterID && ca.Status == CancelledAdmission.StatusResolved);
			if (canceledAdmissionResolved)
				return;
			if (this.TimeBarredCase)
				return;
			throw new StudentRegistrationException(StudentRegistrationStatus.AdmissionHasBeenCanceledPreviousRegularSemesterNotRegistered);
		}

		private void CheckStudentStatusStep6CanRegisterOnline()
		{
			switch (this.UserType)
			{
				case UserTypes.Staff:
					return;
				case UserTypes.Student:
					if (this.IsStudentFirstSemester)
						throw new StudentRegistrationException(StudentRegistrationStatus.StudentCannotRegisterForFirstTimeContactCoordinator);
					var studentSemesters = this.AspireContext.StudentSemesters.Where(ss => ss.StudentID == this.StudentID && ss.Status == StudentSemester.StatusRegistered);
					var examRemarksTypeEnum = (ExamRemarksTypes?)studentSemesters.Where(ss => ss.SemesterID < this.OfferedSemesterID).OrderByDescending(ss => ss.SemesterID).Select(ss => ss.ExamRemarksType).FirstOrDefault();
					switch (examRemarksTypeEnum)
					{
						case null:
						case ExamRemarksTypes.None:
						case ExamRemarksTypes.Qualified:
							break;
						case ExamRemarksTypes.Relegated:
							throw new StudentRegistrationException(StudentRegistrationStatus.Relegated);
						case ExamRemarksTypes.Probation:
						case ExamRemarksTypes.Probation1:
						case ExamRemarksTypes.Probation2:
						case ExamRemarksTypes.Probation3:
							throw new StudentRegistrationException(StudentRegistrationStatus.OnProbationContactCoordinator);
						case ExamRemarksTypes.Chance:
							throw new StudentRegistrationException(StudentRegistrationStatus.OnChanceContactCoordinator);
						case ExamRemarksTypes.Drop:
							throw new StudentRegistrationException(StudentRegistrationStatus.Dropped);
						default:
							throw new NotImplementedEnumException(examRemarksTypeEnum);
					}

					if (Semester.GetSemesterType(this.OfferedSemesterID) == SemesterTypes.Summer)
						if (this.StudentIsSummerRegularSemester == false)
							if (this.StudentCanRegisterInNonRegularSemester == false)
								throw new StudentRegistrationException(StudentRegistrationStatus.SummerSemesterNotAllowedForStudent);
					break;
				default:
					throw new NotImplementedEnumException(this.UserType);
			}
		}

		private void AddOrUpdateStudentSemester()
		{
			void DemandPermissionsForChangeInSectionAndShift()
			{
				switch (this.UserType)
				{
					case UserTypes.Staff:
						var canChangeStudentSectionShift = this.AspireContext.TryDemandStaffPermissions(this.LoginSessionGuid, StaffPermissions.CourseRegistration.CanChangeStudentSectionShift, UserGroupPermission.PermissionValues.Allowed)?.PermissionValueEnum == UserGroupPermission.PermissionValues.Allowed;
						if (!canChangeStudentSectionShift)
							throw new StudentRegistrationException(StudentRegistrationStatus.ChangeInStudentSectionShiftNotAuthorized);
						break;
					case UserTypes.Student:
						throw new StudentRegistrationException(StudentRegistrationStatus.ChangeInStudentSectionShiftNotAuthorized);
					default:
						throw new NotImplementedEnumException(this.UserType);
				}
			}
			var registeredCoursesFound = this.AspireContext.RegisteredCourses
				.Any(rc => rc.StudentID == this.StudentID && rc.OfferedCours.SemesterID == this.OfferedSemesterID && rc.DeletedDate == null);
			var statusEnum = registeredCoursesFound ? StudentSemester.Statuses.Registered : StudentSemester.Statuses.NotRegistered;
			if (this.CurrentStudentSemester == null)
			{
				var studentSemester = this.AspireContext.StudentSemesters.Add(new StudentSemester
				{
					StudentID = this.StudentID,
					SemesterID = this.OfferedSemesterID,
					SemesterNoEnum = this.SemesterNoEnum ?? throw new InvalidOperationException("SemesterNo cannot be null."),
					StatusEnum = statusEnum,
				});
				if (this.PreviousRegularStudentSemester == null)
				{
					studentSemester.SectionEnum = this.SectionEnum ?? throw new InvalidOperationException("Section cannot be null.");
					studentSemester.ShiftEnum = this.ShiftEnum ?? throw new InvalidOperationException("Shift cannot be null.");
				}
				else
				{
					if (this.PreviousRegularStudentSemester.SectionEnum != this.SectionEnum || this.PreviousRegularStudentSemester.ShiftEnum != this.ShiftEnum)
						DemandPermissionsForChangeInSectionAndShift();
					studentSemester.SectionEnum = this.SectionEnum ?? throw new InvalidOperationException("Section cannot be null.");
					studentSemester.ShiftEnum = this.ShiftEnum ?? throw new InvalidOperationException("Shift cannot be null.");
				}
			}
			else
			{
				if (this.CurrentStudentSemester.SectionEnum != this.SectionEnum || this.CurrentStudentSemester.ShiftEnum != this.ShiftEnum)
					DemandPermissionsForChangeInSectionAndShift();
				this.CurrentStudentSemester.SemesterNoEnum = this.SemesterNoEnum ?? throw new InvalidOperationException("SemesterNo cannot be null.");
				this.CurrentStudentSemester.SectionEnum = this.SectionEnum ?? throw new InvalidOperationException("Section cannot be null.");
				this.CurrentStudentSemester.ShiftEnum = this.ShiftEnum ?? throw new InvalidOperationException("Section cannot be null.");
				this.CurrentStudentSemester.StatusEnum = statusEnum;
			}
		}

		internal void Register()
		{
			try
			{
				this.Initialize();

				this.CheckStudentStatusStep1StudentStatus();
				this.CheckStudentStatusStep2ValidateOfferedSemester();
				this.CheckStudentStatusStep3CheckTimeBar();
				this.CheckStudentStatusStep4CheckSemesterFreezeStatus();
				this.CheckStudentStatusStep5CheckPreviousSemesterRegistration();
				this.CheckStudentStatusStep6CanRegisterOnline();
				this.Courses?.ForEach(c => c.Register());
				this.AddOrUpdateStudentSemester();
				this.AspireContext.SaveChanges(this.UserLoginHistoryID);
				this.Status = StudentRegistrationStatus.Success;
			}
			catch (StudentRegistrationException exc)
			{
				this.Status = exc.Status;
			}
		}

		#endregion
	}
}