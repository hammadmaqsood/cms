namespace Aspire.BL.Core.CourseRegistration.Common
{
	internal sealed class OfferedCourse
	{
		public int AdmissionOpenProgramID { get; internal set; }
		public int CourseID { get; internal set; }
		public byte MaxClassStrength { get; internal set; }
		public string Title { get; internal set; }
		public int RegisteredStrength { get; internal set; }
	}
}