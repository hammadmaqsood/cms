using Aspire.Model.Entities.Common;
using System.Collections.Generic;
using System.Linq;
using Aspire.Model.Entities;

namespace Aspire.BL.Core.CourseRegistration.Common
{
	internal sealed class RoadmapCourse
	{
		public int CourseID { get; set; }
		public decimal CreditHours { get; internal set; }
		public CourseCategories CourseCategoryEnum { get; internal set; }
		public List<RoadmapCoursePreReq> PreReqs { get; internal set; }
		public object Title { get; internal set; }

		public void CheckPreRequisites(Course course)
		{
			if (this.PreReqs?.Any() != true)
				return;

			foreach (var roadmapCoursePreReq in this.PreReqs)
				if (roadmapCoursePreReq.ArePreRequisitesCleared(course))
					return;
			if (course.CoursesRegister.ByPassChecks.HasFlag(RegisteredCours.BypassedCheckTypes.PreRequisites))
			{
				if (course.CoursesRegister.CanByPassPreRequisites)
				{
					course.BypassedChecks = course.BypassedChecks | RegisteredCours.BypassedCheckTypes.PreRequisites;
					return;
				}
			}

			throw new CourseRegistrationException(CourseRegistrationStatus.PreRequisitesNotCleared);
		}
	}
}