using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.CourseRegistration.Common
{
	internal sealed class RoadmapCoursePreReq
	{
		public int CoursePreReqID { get; set; }
		public int CourseID1 { get; set; }
		public string Title1 { get; set; }
		public int? CourseID2 { get; set; }
		public string Title2 { get; set; }
		public int? CourseID3 { get; set; }
		public string Title3 { get; set; }

		public bool ArePreRequisitesCleared(Course course)
		{
			if (IsPreRequisiteCleared(course, this.CourseID1))
			{
				if (this.CourseID2 == null)
				{
					if (this.CourseID3 != null)
						throw new InvalidOperationException($"CourseID3 must be null. {nameof(this.CoursePreReqID)}: {this.CoursePreReqID}");
					return true;
				}
				if (IsPreRequisiteCleared(course, this.CourseID2.Value))
				{
					if (this.CourseID3 == null)
						return true;
					if (IsPreRequisiteCleared(course, this.CourseID3.Value))
						return true;
				}
			}

			return false;
		}

		private static bool IsPreRequisiteCleared(Course course, int courseIDofPreReq)
		{
			var examGrades = course.AspireContext.RegisteredCourses
				.Where(rc => rc.StudentID == course.CoursesRegister.StudentID && rc.OfferedCours.SemesterID < course.CoursesRegister.OfferedSemesterID && rc.CourseID == courseIDofPreReq && rc.Grade != null && rc.DeletedDate == null)
				.Select(rc => (ExamGrades)rc.Grade.Value);
			var clearedGrades = new[]
			{
				ExamGrades.A,
				ExamGrades.AMinus,
				ExamGrades.BPlus,
				ExamGrades.B,
				ExamGrades.BMinus,
				ExamGrades.CPlus,
				ExamGrades.C,
				ExamGrades.CMinus,
				ExamGrades.DPlus,
				ExamGrades.D
			}.ToList();
			if (examGrades.Any(g => clearedGrades.Contains(g)))
				return true;

			if (course.CoursesRegister.IsStudentCreditsTransferredExists)
			{
				examGrades = course.AspireContext.StudentCreditsTransferredCourses
					.Where(sctc => sctc.StudentCreditsTransfer.StudentID == course.CoursesRegister.StudentID && sctc.EquivalentCourseID == courseIDofPreReq && sctc.EquivalentGrade != null)
					.Select(sctc => (ExamGrades)sctc.EquivalentGrade.Value);
				if (examGrades.Any(g => clearedGrades.Contains(g)))
					return true;
			}

			if (course.CoursesRegister.IsStudentExemptedCoursesExists)
			{
				if (course.AspireContext.StudentExemptedCourses.Any(sec => sec.StudentID == course.CoursesRegister.StudentID && sec.CourseID == courseIDofPreReq))
					return true;
			}

			return false;
		}
	}
}