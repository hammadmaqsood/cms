using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.CourseRegistration.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.CourseRegistration.Staff
{
	public static class CourseRegistration
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static bool IsAuthorizedForCourseRegistration(this AspireContext aspireContext, int studentID, Guid loginSessionGuid, out Core.Common.Permissions.Staff.StaffGroupPermission staffGroupPermission)
		{
			var student = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => new
			{
				s.AdmissionOpenProgram.Program.InstituteID,
				s.AdmissionOpenProgram.Program.DepartmentID,
				s.AdmissionOpenProgram.ProgramID,
				s.AdmissionOpenProgramID,
			}).Single();

			staffGroupPermission = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.RegisterCourse, UserGroupPermission.PermissionValues.Allowed, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
			return staffGroupPermission != null;
		}

		private static bool IsRegistrationOpenForCoordinators(this AspireContext aspireContext, int studentID, short offeredSemesterID, Core.Common.Permissions.Staff.StaffGroupPermission staffGroupPermission)
		{
			if (staffGroupPermission.PermissionType.PermissionTypeEnum != UserGroupPermission.PermissionTypes.RegisterCourse)
				return false;
			if (staffGroupPermission.PermissionValueEnum == UserGroupPermission.PermissionValues.Allowed)
			{
				var student = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => new
				{
					s.AdmissionOpenProgram.ProgramID,
				}).SingleOrDefault();
				if (student == null)
					throw new DataException("Student record not found.");
				var courseRegistrationSettingsQuery = aspireContext.CourseRegistrationSettings
					.Where(crs => crs.ProgramID == student.ProgramID)
					.Where(crs => crs.Status == CourseRegistrationSetting.StatusActive)
					.Where(crs => crs.SemesterID == offeredSemesterID)
					.Where(crs => crs.FromDate <= DateTime.Today && DateTime.Today <= crs.ToDate);
				return courseRegistrationSettingsQuery.Any();
			}
			return false;
		}

		public sealed class RegisteredCourse
		{
			internal RegisteredCourse()
			{
			}
			public int RegisteredCourseID { get; internal set; }

			public string RoadmapCourseCode { get; internal set; }
			public string RoadmapTitle { get; internal set; }
			public string RoadmapMajors { get; internal set; }
			public string RoadmapMajorsNA => this.RoadmapMajors.ToNAIfNullOrEmpty();
			public decimal RoadmapCreditHours { get; internal set; }
			public string RoadmapCreditHoursFullName => this.RoadmapCreditHours.ToCreditHoursFullName();

			public short OfferedSemesterID { get; internal set; }
			public string OfferedSemester => this.OfferedSemesterID.ToSemesterString();
			public string OfferedCourseCode { get; internal set; }
			public string OfferedTitle { get; internal set; }
			public decimal OfferedCreditHours { get; internal set; }
			public string OfferedCreditHoursFullName => this.OfferedCreditHours.ToCreditHoursFullName();
			public decimal OfferedContactHours { get; internal set; }
			public string OfferedContactHoursFullName => this.OfferedContactHours.ToCreditHoursFullName();
			public string OfferedProgram { get; internal set; }
			public short OfferedSemesterNo { get; internal set; }
			public SemesterNos OfferedSemesterNoEnum => (SemesterNos)this.OfferedSemesterNo;
			public string OfferedSemesterNoFullName => this.OfferedSemesterNoEnum.ToFullName();
			public int OfferedSection { get; internal set; }
			public Sections OfferedSectionEnum => (Sections)this.OfferedSection;
			public string OfferedSectionFullName => this.OfferedSectionEnum.ToFullName();
			public byte OfferedShift { get; internal set; }
			public Shifts OfferedShiftEnum => (Shifts)this.OfferedShift;
			public string OfferedShiftFullName => this.OfferedShiftEnum.ToFullName();
			public string OfferedClass => AspireFormats.GetClassName(this.OfferedProgram, this.OfferedSemesterNo, this.OfferedSection, this.OfferedShift);

			public string FacultyMemberName { get; internal set; }
			public byte? Status { get; internal set; }
			public RegisteredCours.Statuses? StatusEnum => (RegisteredCours.Statuses?)this.Status;
			public string StatusFullName => this.StatusEnum?.ToFullName();
			public byte? FreezedStatus { get; internal set; }
			public FreezedStatuses? FreezedStatusEnum => (FreezedStatuses?)this.FreezedStatus;
			public string FreezedStatusFullName => this.FreezedStatusEnum?.ToFullName();
			public DateTime? FreezedDate { get; internal set; }
			public bool SpecialOffered { get; internal set; }
			public string SpecialOfferedYesNo => this.SpecialOffered.ToYesNo();
			public DateTime? StatusDate { get; internal set; }
			public bool FeeDefaulter { get; internal set; }
			public int? StudentFeeID { get; internal set; }
			public byte? FeeType { get; internal set; }
			public StudentFee.FeeTypes? FeeTypeEnum => (StudentFee.FeeTypes?)this.FeeType;
			public string FeeTypeEnumFullName => this.FeeTypeEnum?.ToFullName();
			public DateTime RegistrationDate { get; internal set; }
			public string RegisteredByUserName { get; internal set; }
			public string RegisteredByStudentName { get; internal set; }
			public short? RegisteredByUserType { get; internal set; }
			public UserTypes? RegisteredByUserTypeEnum => (UserTypes?)this.RegisteredByUserType;
			public RegisteredCours.BypassedCheckTypes BypassedChecksEnum { get; internal set; }
			public bool MarksEntryLocked { get; internal set; }
			public string Remarks { get; internal set; }
			public string RegisteredBy
			{
				get
				{
					switch (this.RegisteredByUserTypeEnum)
					{
						case UserTypes.Staff:
							return this.RegisteredByUserName;
						case UserTypes.Student:
							return this.RegisteredByStudentName;
						case null:
							return string.Empty.ToNAIfNullOrEmpty();
						case UserTypes.ManualSQL:
						case UserTypes.Anonymous:
						case UserTypes.System:
						case UserTypes.MasterAdmin:
						case UserTypes.Admin:
						case UserTypes.Executive:
						case UserTypes.Candidate:
						case UserTypes.Faculty:
						case UserTypes.Alumni:
						case UserTypes.IntegratedService:
						case UserTypes.Admins:
						case UserTypes.Any:
							throw new InvalidOperationException($"Course cannot be registered by UserType: {this.RegisteredByUserTypeEnum}");
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
		}

		internal static List<RegisteredCourse> GetStudentRegisteredCourses(this AspireContext aspireContext, int? studentID, int? instituteID, short? semesterID, int? registeredCourseID)
		{
			var registeredCourses = aspireContext.RegisteredCourses.Where(rc => rc.DeletedDate == null);
			if (studentID != null)
				registeredCourses = registeredCourses.Where(rc => rc.StudentID == studentID.Value);
			if (semesterID != null)
				registeredCourses = registeredCourses.Where(rc => rc.OfferedCours.SemesterID == semesterID);
			if (registeredCourseID != null)
				registeredCourses = registeredCourses.Where(rc => rc.RegisteredCourseID == registeredCourseID.Value);
			if (instituteID != null)
				registeredCourses = registeredCourses.Where(rc => rc.Student.AdmissionOpenProgram.Program.InstituteID == instituteID.Value);

			var query = from rc in registeredCourses
						join h in aspireContext.UserLoginHistories on rc.RegisteredByUserLoginHistoryID equals h.UserLoginHistoryID into grp
						from g in grp.DefaultIfEmpty()
						select new RegisteredCourse
						{
							FacultyMemberName = rc.OfferedCours.FacultyMember.Name,
							FreezedDate = rc.FreezedDate,
							FreezedStatus = rc.FreezedStatus,
							OfferedProgram = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
							OfferedSection = rc.OfferedCours.Section,
							OfferedSemesterNo = rc.OfferedCours.SemesterNo,
							OfferedSemesterID = rc.OfferedCours.SemesterID,
							OfferedShift = rc.OfferedCours.Shift,
							OfferedTitle = rc.OfferedCours.Cours.Title,
							OfferedCourseCode = rc.OfferedCours.Cours.CourseCode,
							OfferedCreditHours = rc.OfferedCours.Cours.CreditHours,
							OfferedContactHours = rc.OfferedCours.Cours.ContactHours,
							RegisteredCourseID = rc.RegisteredCourseID,
							RoadmapCourseCode = rc.Cours.CourseCode,
							RoadmapTitle = rc.Cours.Title,
							RoadmapCreditHours = rc.Cours.CreditHours,
							RoadmapMajors = rc.Cours.ProgramMajor.Majors,
							SpecialOffered = rc.OfferedCours.SpecialOffered,
							Status = rc.Status,
							StatusDate = rc.StatusDate,
							StudentFeeID = rc.StudentFeeID,
							FeeType = rc.StudentFee.FeeType,
							RegistrationDate = rc.RegistrationDate,
							RegisteredByUserType = g.UserType,
							RegisteredByUserName = g.User.Name,
							RegisteredByStudentName = g.Student.Name,
							Remarks = rc.Remarks,
							BypassedChecksEnum = (RegisteredCours.BypassedCheckTypes)rc.BypassedChecks,
							MarksEntryLocked = rc.OfferedCours.MarksEntryLocked,
							FeeDefaulter = rc.FeeDefaulter
						};
			return query.OrderBy(rc => rc.OfferedSemesterID).ThenBy(rc => rc.RoadmapTitle).ToList();
		}

		internal static RegisteredCourse GetStudentRegisteredCourse(this AspireContext aspireContext, int registeredCourseID, int? instituteID)
		{
			return aspireContext.GetStudentRegisteredCourses(null, instituteID, null, registeredCourseID).SingleOrDefault();
		}

		public sealed class RequestedCourse
		{
			internal RequestedCourse()
			{
			}
			public int RequestedCourseID { get; internal set; }
			public int OfferedCourseID { get; internal set; }
			public short OfferedSemesterID { get; internal set; }
			public string OfferedCourseCode { get; internal set; }
			public string OfferedTitle { get; internal set; }
			public decimal OfferedCreditHours { get; internal set; }
			public string OfferedCreditHoursFullName => this.OfferedCreditHours.ToCreditHoursFullName();
			public string OfferedProgram { get; internal set; }
			public short OfferedSemesterNo { get; internal set; }
			public int OfferedSection { get; internal set; }
			public byte OfferedShift { get; internal set; }
			public string OfferedClass => AspireFormats.GetClassName(this.OfferedProgram, this.OfferedSemesterNo, this.OfferedSection, this.OfferedShift);
			public bool SpecialOffered { get; internal set; }
			public string SpecialOfferedYesNo => this.SpecialOffered.ToYesNo();
			public DateTime RequestDate { get; internal set; }
		}

		private static List<RequestedCourse> GetStudentRequestedCourses(this AspireContext aspireContext, int studentID, short semesterID)
		{
			var query = aspireContext.RequestedCourses.Where(rc => rc.StudentID == studentID && rc.OfferedCours.SemesterID == semesterID);
			return query.Select(rc => new RequestedCourse
			{
				RequestedCourseID = rc.RequestedCourseID,
				OfferedCourseID = rc.OfferedCourseID,
				OfferedProgram = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
				OfferedSection = rc.OfferedCours.Section,
				OfferedSemesterNo = rc.OfferedCours.SemesterNo,
				OfferedSemesterID = rc.OfferedCours.SemesterID,
				OfferedShift = rc.OfferedCours.Shift,
				OfferedTitle = rc.OfferedCours.Cours.Title,
				OfferedCourseCode = rc.OfferedCours.Cours.CourseCode,
				OfferedCreditHours = rc.OfferedCours.Cours.CreditHours,
				SpecialOffered = rc.OfferedCours.SpecialOffered,
				RequestDate = rc.RequestDate,
			}).OrderBy(rc => rc.OfferedTitle).ToList();
		}

		public sealed class RegisteredCourseResult
		{
			internal RegisteredCourseResult()
			{
			}
			public string InstituteAlias { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string Enrollment { get; internal set; }
			public int RegisteredCourseID { get; internal set; }
			public string RoadmapCourseCode { get; internal set; }
			public string RoadmapTitle { get; internal set; }
			public string RoadmapMajors { get; internal set; }
			public decimal RoadmapCreditHours { get; internal set; }
			public string RoadmapCreditHoursFullName => this.RoadmapCreditHours.ToCreditHoursFullName();
			public byte? Grade { get; set; }
			public string GradeFullName => ((ExamGrades?)this.Grade)?.ToFullName();

			public short OfferedSemesterID { get; internal set; }
			public string OfferedSemester => this.OfferedSemesterID.ToSemesterString();

			public byte? Status { get; internal set; }
			public byte? FreezeStatus { get; internal set; }
			public string CompleteStatus => AspireFormats.GetRegisteredCourseStatusString(this.Status, this.FreezeStatus);
		}

		private static void GetAllStudentIDs(this AspireContext aspireContext, List<int> studentIDsList, int studentID)
		{
			if (studentIDsList.Contains(studentID))
				return;
			studentIDsList.Add(studentID);
			var transferredStudents = aspireContext.TransferredStudents
				.Where(ts => ts.TransferredFromStudentID == studentID || ts.TransferredToStudentID == studentID)
				.Select(ts => new { ts.TransferredToStudentID, ts.TransferredFromStudentID }).ToList();
			foreach (var transferredStudent in transferredStudents)
			{
				if (!studentIDsList.Contains(transferredStudent.TransferredFromStudentID))
					aspireContext.GetAllStudentIDs(studentIDsList, transferredStudent.TransferredFromStudentID);
				if (!studentIDsList.Contains(transferredStudent.TransferredToStudentID))
					aspireContext.GetAllStudentIDs(studentIDsList, transferredStudent.TransferredToStudentID);
			}
		}

		private static List<int> GetAllStudentIDs(this AspireContext aspireContext, int studentID)
		{
			var studentIDs = new List<int>();
			aspireContext.GetAllStudentIDs(studentIDs, studentID);
			return studentIDs;
		}

		private static List<RegisteredCourseResult> GetStudentPreviousResult(this AspireContext aspireContext, int studentID, short offeredSemesterID)
		{
			var studentIDs = aspireContext.GetAllStudentIDs(studentID);
			return aspireContext.RegisteredCourses
				.Where(rc => studentIDs.Contains(rc.StudentID) && rc.OfferedCours.SemesterID < offeredSemesterID && rc.DeletedDate == null)
				.Select(rc => new RegisteredCourseResult
				{
					RegisteredCourseID = rc.RegisteredCourseID,
					Enrollment = rc.Student.Enrollment,
					OfferedSemesterID = rc.OfferedCours.SemesterID,
					Status = rc.Status,
					FreezeStatus = rc.FreezedStatus,
					RoadmapCourseCode = rc.Cours.CourseCode,
					RoadmapCreditHours = rc.Cours.CreditHours,
					RoadmapMajors = rc.Cours.ProgramMajor.Majors,
					RoadmapTitle = rc.Cours.Title,
					Grade = rc.OfferedCours.MarksEntryLocked ? rc.Grade : null,
					InstituteAlias = rc.Student.AdmissionOpenProgram.Program.Institute.InstituteAlias,
					ProgramAlias = rc.Student.AdmissionOpenProgram.Program.ProgramAlias,
				}).OrderByDescending(rc => rc.OfferedSemesterID).ThenBy(rc => rc.RoadmapTitle).ToList();
		}

		public sealed class CourseRegistrationInfo
		{
			internal CourseRegistrationInfo()
			{
			}
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string Name { get; internal set; }
			public string FatherName { get; internal set; }
			public string Program { get; internal set; }
			public int InstituteID { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public Model.Entities.Student.Statuses? StatusEnum { get; internal set; }
			public string BlockedReason { get; internal set; }
			public DateTime? StatusDate { get; internal set; }
			public List<StudentSemester> StudentSemesters { get; internal set; }
			public bool CanStudentRegisterCourseOnline { get; internal set; }
			public int ProgramID { get; internal set; }
			public int AdmissionOpenProgramID { get; internal set; }
			public short OfferedSemesterID { get; internal set; }
			public List<RegisteredCourse> RegisteredCourses { get; internal set; }
			public List<RequestedCourse> RequestedCourses { get; internal set; }
			public List<RegisteredCourseResult> RegisteredCourseResults { get; internal set; }
			public byte MaxAllowedCoursesPerSemester { get; internal set; }
			public byte MaxAllowedCreditHoursPerSemester { get; internal set; }
		}

		public static CourseRegistrationInfo GetCourseRegistrationInfo(string enrollment, short offeredSemesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

				var courseRegistrationInfo = aspireContext.Students.Where(s => s.Enrollment == enrollment && s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID).Select(s => new CourseRegistrationInfo
				{
					StudentID = s.StudentID,
					Enrollment = s.Enrollment,
					RegistrationNo = s.RegistrationNo,
					Name = s.Name,
					FatherName = s.FatherName,
					AdmissionOpenProgramID = s.AdmissionOpenProgramID,
					ProgramID = s.AdmissionOpenProgram.ProgramID,
					Program = s.AdmissionOpenProgram.Program.ProgramAlias,
					InstituteID = s.AdmissionOpenProgram.Program.InstituteID,
					IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
					StatusEnum = (Model.Entities.Student.Statuses?)s.Status,
					BlockedReason = s.BlockedReason,
					StatusDate = s.StatusDate,
					OfferedSemesterID = offeredSemesterID,
					StudentSemesters = s.StudentSemesters.OrderBy(ss => ss.SemesterID).ToList(),
					CanStudentRegisterCourseOnline = s.CanStudentRegisterCourseOnline == null || s.CanStudentRegisterCourseOnline.Value,
					MaxAllowedCoursesPerSemester = s.AdmissionOpenProgram.Program.MaxAllowedCoursesPerSemester,
					MaxAllowedCreditHoursPerSemester = s.AdmissionOpenProgram.Program.MaxAllowedCreditHoursPerSemester,
				}).SingleOrDefault();
				if (courseRegistrationInfo == null)
					return null;
				courseRegistrationInfo.RegisteredCourses = aspireContext.GetStudentRegisteredCourses(courseRegistrationInfo.StudentID, null, offeredSemesterID, null);
				courseRegistrationInfo.RequestedCourses = aspireContext.GetStudentRequestedCourses(courseRegistrationInfo.StudentID, offeredSemesterID);
				courseRegistrationInfo.RegisteredCourseResults = aspireContext.GetStudentPreviousResult(courseRegistrationInfo.StudentID, offeredSemesterID);
				return courseRegistrationInfo;
			}
		}

		public static List<ExtensionMethods.OfferedCourse> GetOfferedCourses(int admissionOpenProgramID, short offeredSemesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.Module, UserGroupPermission.PermissionValues.Allowed);
				return aspireContext.GetOfferedCourses(loginHistory.InstituteID, admissionOpenProgramID, offeredSemesterID);
			}
		}

		public sealed class Course
		{
			internal Course()
			{
			}
			public string Program { get; internal set; }
			public short SemesterID { get; internal set; }
			public int CourseID { get; internal set; }
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public string CreditHoursFullName => this.CreditHours.ToCreditHoursFullName();
			public byte CourseCategory { get; internal set; }
			public CourseCategories CourseCategoryEnum => (CourseCategories)this.CourseCategory;
			public string CourseCategoryFullName => this.CourseCategoryEnum.ToFullName();
			public byte CourseType { get; internal set; }
			public CourseTypes CourseTypeEnum => (CourseTypes)this.CourseType;
			public string CourseTypeFullName => this.CourseTypeEnum.ToFullName();
			public short SemesterNo { get; internal set; }
			public SemesterNos SemesterNoEnum => (SemesterNos)this.SemesterNo;
			public string SemesterNoFullName => this.SemesterNoEnum.ToFullName();
			public string Remarks { get; internal set; }
			public string Majors { get; internal set; }
			public Guid Status { get; internal set; }
		}

		public static List<Course> GetStudentRoadmapCourses(int studentID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.Students.Where(s => s.StudentID == studentID).SelectMany(s => s.AdmissionOpenProgram.Courses).Select(c => new Course
				{
					CourseID = c.CourseID,
					SemesterID = c.AdmissionOpenProgram.SemesterID,
					Program = c.AdmissionOpenProgram.Program.ProgramAlias,
					CourseCode = c.CourseCode,
					Title = c.Title,
					CreditHours = c.CreditHours,
					Majors = c.ProgramMajor.Majors,
					SemesterNo = c.SemesterNo,
					CourseCategory = c.CourseCategory,
					CourseType = c.CourseType,
					Remarks = c.Remarks,
					Status = c.Status
				}).OrderBy(c => c.Title).ToList();
			}
		}

		public static List<RegisteredCourse> GetStudentRegisteredCourses(int studentID, short? semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				if (!aspireContext.Students.Any(s => s.StudentID == studentID && s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID))
					return null;
				return aspireContext.GetStudentRegisteredCourses(studentID, null, semesterID, null);
			}
		}

		public static RegisteredCourse GetStudentRegisteredCourse(int registeredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GetStudentRegisteredCourse(registeredCourseID, loginHistory.InstituteID);
			}
		}

		public static CoursesRegister RegisterCourse(int studentID, short offeredSemesterID, SemesterNos semesterNoEnum, Sections sectionEnum, Shifts shiftEnum, int offeredCourseID, int equivalentCourseID, RegisteredCours.BypassedCheckTypes byPassChecks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var coursesRegister = CoursesRegister.CreateForStaffToRegisterCourse(aspireContext, studentID, offeredSemesterID, semesterNoEnum, sectionEnum, shiftEnum, offeredCourseID, equivalentCourseID, byPassChecks, loginSessionGuid);
				coursesRegister.Register();
				aspireContext.CommitTransaction();
				return coursesRegister;
			}
		}

		public static CoursesRegister ChangeStudentClass(int studentID, short offeredSemesterID, SemesterNos semesterNoEnum, Sections sectionEnum, Shifts shiftEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				if (!aspireContext.IsAuthorizedForCourseRegistration(studentID, loginSessionGuid, out _))
					return null;
				var coursesRegister = CoursesRegister.CreateForStaffToChangeClass(aspireContext, studentID, offeredSemesterID, semesterNoEnum, sectionEnum, shiftEnum, loginSessionGuid);
				coursesRegister.Register();
				aspireContext.CommitTransaction();
				return coursesRegister;
			}
		}

		public enum DeleteRegisteredCourseStatuses
		{
			NoRecordFound,
			NotAuthorized,
			Success,
			RegistrationClosed,
			CannotDeleteExamMarksExists,
			CannotDeleteAttendanceRecordExists,
			CannotDeleteStatusExists,
			CannotDeleteSemesterFreeze,
			CannotDeleteFeeGenerated,
			CannotDeleteSurveyRecordExists
		}

		public static DeleteRegisteredCourseStatuses DeleteRegisteredCourse(int registeredCourseID, bool deleteWithAttendance, bool bypassRegistrationOpenCheck, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.RegisteredCourses.Where(rc => rc.RegisteredCourseID == registeredCourseID)
					.Select(rc => new
					{
						RegisteredCourse = rc,
						OffferedSemesterID = rc.OfferedCours.SemesterID,
						rc.StudentID,
					}).SingleOrDefault();
				if (record == null)
					return DeleteRegisteredCourseStatuses.NoRecordFound;
				var registeredCourse = record.RegisteredCourse;

				if (!aspireContext.IsAuthorizedForCourseRegistration(registeredCourse.StudentID, loginSessionGuid, out var staffGroupPermission))
					return DeleteRegisteredCourseStatuses.NotAuthorized;

				if (!aspireContext.IsRegistrationOpenForCoordinators(record.StudentID, record.OffferedSemesterID, staffGroupPermission))
				{
					if (bypassRegistrationOpenCheck)
					{
						var permissions = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.CanByPassRegistrationOpenCheck, UserGroupPermission.PermissionValues.Allowed);
						if (permissions?.PermissionValueEnum != UserGroupPermission.PermissionValues.Allowed)
							return DeleteRegisteredCourseStatuses.RegistrationClosed;
					}
					else
						return DeleteRegisteredCourseStatuses.RegistrationClosed;
				}

				if (registeredCourse.Status != null)
					return DeleteRegisteredCourseStatuses.CannotDeleteStatusExists;
				if (registeredCourse.Assignments != null || registeredCourse.Quizzes != null || registeredCourse.Internals != null || registeredCourse.Mid != null || registeredCourse.Final != null || registeredCourse.Total != null || registeredCourse.Grade != null || registeredCourse.GradePoints != null || registeredCourse.Product != null)
					return DeleteRegisteredCourseStatuses.CannotDeleteExamMarksExists;
				if (registeredCourse.FreezedDate != null || registeredCourse.FreezedStatus != null)
					return DeleteRegisteredCourseStatuses.CannotDeleteSemesterFreeze;
				if (registeredCourse.RegisteredCourseExamMarks.Any(rc => rc.ExamMarks != null))
					return DeleteRegisteredCourseStatuses.CannotDeleteExamMarksExists;

				var surveyExists = registeredCourse.SurveyUsers.Any();
				var attendanceExists = registeredCourse.OfferedCourseAttendanceDetails.Any();
				if (deleteWithAttendance)
				{
					if (surveyExists || attendanceExists)
						staffGroupPermission = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.CanDeleteCourseRegistrationWithAttendance, UserGroupPermission.PermissionValues.Allowed);
					else
						staffGroupPermission = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.CanDeleteCourseRegistration, UserGroupPermission.PermissionValues.Allowed);
				}
				else
				{
					if (surveyExists)
						return DeleteRegisteredCourseStatuses.CannotDeleteSurveyRecordExists;
					if (attendanceExists)
						return DeleteRegisteredCourseStatuses.CannotDeleteAttendanceRecordExists;
					staffGroupPermission = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.CanDeleteCourseRegistration, UserGroupPermission.PermissionValues.Allowed);
				}
				registeredCourse.DeletedDate = DateTime.Now;
				registeredCourse.DeletedByUserLoginHistoryID = staffGroupPermission.UserLoginHistoryID;
				aspireContext.SaveChanges(staffGroupPermission.UserLoginHistoryID);
				aspireContext.UpdateStudentSemester(record.StudentID, record.OffferedSemesterID, staffGroupPermission);
				aspireContext.CommitTransaction();
				return DeleteRegisteredCourseStatuses.Success;
			}
		}

		private static void UpdateStudentSemester(this AspireContext aspireContext, int studentID, short semesterID, Core.Common.Permissions.Staff.StaffGroupPermission staffGroupPermission)
		{
			var studentSemester = aspireContext.StudentSemesters.SingleOrDefault(ss => ss.StudentID == studentID && ss.SemesterID == semesterID);
			if (aspireContext.RegisteredCourses.Any(rc => rc.StudentID == studentID && rc.OfferedCours.SemesterID == semesterID && rc.DeletedDate == null))
			{
				if (studentSemester == null)
					throw new InvalidOperationException("StudentSemester must exist.");
				if (studentSemester.StatusEnum != StudentSemester.Statuses.Registered)
					throw new InvalidOperationException("StudentSemester status must be Registered.");
			}
			else
			{
				if (studentSemester != null)
				{
					studentSemester.StatusEnum = StudentSemester.Statuses.NotRegistered;
					aspireContext.SaveChanges(staffGroupPermission.UserLoginHistoryID);
				}
			}
		}

		public enum DeleteRequestedCourseStatuses
		{
			NoRecordFound,
			NotAuthorized,
			Success
		}

		public static DeleteRequestedCourseStatuses DeleteRequestedCourse(int requestedCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var requestedCourse = aspireContext.RequestedCourses.SingleOrDefault(rc => rc.RequestedCourseID == requestedCourseID);
				if (requestedCourse == null)
					return DeleteRequestedCourseStatuses.NoRecordFound;

				if (!aspireContext.IsAuthorizedForCourseRegistration(requestedCourse.StudentID, loginSessionGuid, out var staffGroupPermission))
					return DeleteRequestedCourseStatuses.NotAuthorized;

				aspireContext.RequestedCourses.Remove(requestedCourse);
				aspireContext.SaveChangesAndCommitTransaction(staffGroupPermission.UserLoginHistoryID);
				return DeleteRequestedCourseStatuses.Success;
			}
		}

		public sealed class Student
		{
			internal Student()
			{ }
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public byte? Status { get; internal set; }
			public SemesterNos? SemesterNoEnum => this._StudentSemester?.SemesterNo;
			public Sections? SectionEnum => this._StudentSemester?.Section;
			public Shifts? ShiftEnum => this._StudentSemester?.Shift;
			public short? SemesterNo => (short?)this.SemesterNoEnum;
			public int? Section => (int?)this.SectionEnum;
			public byte? Shift => (byte?)this.ShiftEnum;

			public sealed class StudentSemester
			{
				public SemesterNos SemesterNo { get; internal set; }
				public Sections Section { get; internal set; }
				public Shifts Shift { get; internal set; }
			}

			internal StudentSemester _StudentSemester { get; set; }
			public string StatusFullName => ((Model.Entities.Student.Statuses?)this.Status)?.ToFullName();
			public List<RegisteredCourse> RegisteredCourses { get; internal set; }
			public short IntakeSemesterID { get; internal set; }

			public string CountHtml => this.RegisteredCourses
				.GroupBy(rc => rc.ProgramMajorID)
				.Select(g =>
				{
					var majors = g.First().Majors;
					return string.IsNullOrWhiteSpace(majors)
						? g.Count().ToString()
						: $"{majors.HtmlAttributeEncode()}: {g.Count().ToString().HtmlEncode()}";
				})
				.Join(" + ");

			public sealed class RegisteredCourse
			{
				internal RegisteredCourse() { }
				public int RegisteredCourseID { get; internal set; }
				public int? ProgramMajorID { get; internal set; }
				public string Majors { get; internal set; }
			}
		}

		public static List<Student> GetStudents(short semesterID, int programID, int? programMajorID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var students = aspireContext.Students
					.Where(s => s.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(s => s.AdmissionOpenProgram.SemesterID == semesterID)
					.Where(s => s.AdmissionOpenProgram.ProgramID == programID);

				var result = students.Select(s => new Student
				{
					StudentID = s.StudentID,
					Enrollment = s.Enrollment,
					Name = s.Name,
					IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
					ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
					Status = s.Status,
					RegisteredCourses = s.RegisteredCourses
						.Where(rc => rc.OfferedCours.SemesterID == semesterID && rc.DeletedDate == null)
						.Select(rc => new Student.RegisteredCourse
						{
							RegisteredCourseID = rc.RegisteredCourseID,
							ProgramMajorID = rc.Cours.ProgramMajor.ProgramMajorID,
							Majors = rc.Cours.ProgramMajor.Majors,
						}).ToList(),
					_StudentSemester = s.StudentSemesters
						 .Where(ss => ss.SemesterID == semesterID)
						 .Select(ss => new Student.StudentSemester
						 {
							 SemesterNo = (SemesterNos)ss.SemesterNo,
							 Section = (Sections)ss.Section,
							 Shift = (Shifts)ss.Shift,
						 })
						 .FirstOrDefault()
				}).OrderBy(o => o.Enrollment).ToList();
				return result
					.Where(r => semesterNoEnum == null || r.SemesterNo == null || r.SemesterNoEnum == semesterNoEnum.Value)
					.Where(r => sectionEnum == null || r.Section == null || r.SectionEnum == sectionEnum.Value)
					.Where(r => shiftEnum == null || r.Shift == null || r.ShiftEnum == shiftEnum.Value)
					.Where(r => programMajorID == null || !r.RegisteredCourses.Any() || r.RegisteredCourses.Any(rc => rc.ProgramMajorID == programMajorID.Value))
					.OrderBy(r => r.RegisteredCourses.Any()).ThenBy(r => r.Enrollment)
					.ToList();
			}
		}

		public sealed class StudentRegistration
		{
			public int StudentID { get; }
			public string Enrollment { get; }
			public SemesterNos SemesterNo { get; }
			public Sections Section { get; }
			public Shifts Shift { get; }
			public int? ProgramMajorID { get; }
			public CoursesRegister CoursesRegister { get; internal set; }

			public StudentRegistration(int studentID, string enrollment, SemesterNos semesterNo, Sections section, Shifts shift, int? programMajorID)
			{
				this.StudentID = studentID;
				this.Enrollment = enrollment;
				this.SemesterNo = semesterNo;
				this.Section = section;
				this.Shift = shift;
				this.ProgramMajorID = programMajorID;
				this.CoursesRegister = null;
			}
		}

		public static List<StudentRegistration> RegisterFirstSemesterCourses(List<StudentRegistration> students, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				foreach (var studentRegistration in students)
				{
					var student = aspireContext.Students
						.Where(s => s.StudentID == studentRegistration.StudentID)
						.Select(s => new
						{
							s.StudentID,
							s.AdmissionOpenProgramID,
							IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
							s.AdmissionOpenProgram.Program.InstituteID,
							s.AdmissionOpenProgram.Program.ProgramAlias,
							s.AdmissionOpenProgram.ProgramID
						}).SingleOrDefault();
					if (student == null)
					{
						studentRegistration.CoursesRegister = null;
						continue;
					}

					var offeredCourses = aspireContext.GetOfferedCourses(loginHistory.InstituteID, student.AdmissionOpenProgramID, student.IntakeSemesterID, new ExtensionMethods.StudentClass
					{
						SemesterID = student.IntakeSemesterID,
						ProgramID = student.ProgramID,
						ProgramAlias = student.ProgramAlias,
						AdmissionOpenProgramID = student.AdmissionOpenProgramID,
						SemesterNo = (short)studentRegistration.SemesterNo,
						Section = (int)studentRegistration.Section,
						Shift = (byte)studentRegistration.Shift,
					});

					CoursesRegister coursesRegister = null;
					using (var aspireContextInner = new AspireContext(true))
					{
						foreach (var offeredCourse in offeredCourses)
						{
							if (offeredCourse.EquivalentCourseID != null && (offeredCourse.EquivalentProgramMajorID == null || offeredCourse.EquivalentProgramMajorID.Value == studentRegistration.ProgramMajorID))
							{
								if (coursesRegister == null)
									coursesRegister = CoursesRegister.CreateForStaffToRegisterCourse(aspireContextInner, student.StudentID, student.IntakeSemesterID, studentRegistration.SemesterNo, studentRegistration.Section, studentRegistration.Shift, offeredCourse.OfferedCourseID, offeredCourse.EquivalentCourseID.Value, RegisteredCours.BypassedCheckTypes.None, loginSessionGuid);
								else
									coursesRegister.AddCourse(offeredCourse.OfferedCourseID, offeredCourse.EquivalentCourseID.Value, false);
							}
						}
						if (coursesRegister != null)
						{
							coursesRegister.Register();
							aspireContextInner.CommitTransaction();
						}
						studentRegistration.CoursesRegister = coursesRegister;
					}
				}
				return students;
			}
		}

		public enum ToggleCanStudentRegisterCourseOnlineStatuses
		{
			NoRecordFound,
			SuccessAllowed,
			SuccessNotAllowed
		}

		public static ToggleCanStudentRegisterCourseOnlineStatuses ToggleCanStudentRegisterCourseOnlineStatus(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.Students.Where(s => s.StudentID == studentID)
					.Select(s => new
					{
						Student = s,
						s.AdmissionOpenProgram.Program.InstituteID,
						s.AdmissionOpenProgram.Program.DepartmentID,
						s.AdmissionOpenProgram.ProgramID,
						s.AdmissionOpenProgramID
					}).SingleOrDefault();
				if (record == null)
					return ToggleCanStudentRegisterCourseOnlineStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.RegisterCourse, UserGroupPermission.PermissionValues.Allowed, record.InstituteID, record.DepartmentID, record.ProgramID, record.AdmissionOpenProgramID);

				record.Student.CanStudentRegisterCourseOnline = !(record.Student.CanStudentRegisterCourseOnline ?? true);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return record.Student.CanStudentRegisterCourseOnline == true ? ToggleCanStudentRegisterCourseOnlineStatuses.SuccessAllowed : ToggleCanStudentRegisterCourseOnlineStatuses.SuccessNotAllowed;
			}
		}

		public sealed class StudentWiseRegisteredCoursesSummary
		{
			internal StudentWiseRegisteredCoursesSummary() { }
			public short SemesterID { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public SemesterNos SemesterNoEnum { get; internal set; }
			public Sections SectionEnum { get; internal set; }
			public Shifts ShiftEnum { get; internal set; }
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public int Courses { get; internal set; }
			public decimal CreditHours { get; internal set; }
		}

		public static List<StudentWiseRegisteredCoursesSummary> GetStudentWiseRegisteredCoursesSummary(short offeredSemesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int? creditHours, int? registeredCourses, int pageIndex, int pageSize, out int virtualItemCount, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

				var registeredCoursesQuery = from p in aspireContext.Programs
											 join aop in aspireContext.AdmissionOpenPrograms on p.ProgramID equals aop.ProgramID
											 join s in aspireContext.Students on aop.AdmissionOpenProgramID equals s.AdmissionOpenProgramID
											 join rc in aspireContext.RegisteredCourses on s.StudentID equals rc.StudentID
											 join c in aspireContext.Courses on rc.CourseID equals c.CourseID
											 join oc in aspireContext.OfferedCourses on rc.OfferedCourseID equals oc.OfferedCourseID
											 join ss in aspireContext.StudentSemesters on s.StudentID equals ss.StudentID
											 where p.InstituteID == loginHistory.InstituteID
												&& oc.SemesterID == offeredSemesterID && ss.SemesterID == offeredSemesterID
												&& rc.DeletedDate == null
												&& (departmentID == null || p.DepartmentID == departmentID.Value)
												&& (programID == null || p.ProgramID == programID.Value)
												&& (semesterNoEnum == null || ss.SemesterNo == (short)semesterNoEnum.Value)
												&& (sectionEnum == null || ss.Section == (int)sectionEnum.Value)
												&& (shiftEnum == null || ss.Shift == (byte)shiftEnum.Value)
											 select new
											 {
												 s.StudentID,
												 s.Enrollment,
												 rc.RegisteredCourseID,
												 oc.SemesterID,
												 p.ProgramAlias,
												 SemesterNoEnum = (SemesterNos)ss.SemesterNo,
												 SectionEnum = (Sections)ss.Section,
												 ShiftEnum = (Shifts)ss.Shift,
												 c.CreditHours
											 };
				var summaryQuery = registeredCoursesQuery.GroupBy(rc => new
				{
					rc.StudentID,
					rc.Enrollment,
					rc.SemesterID,
					rc.ProgramAlias,
					rc.SemesterNoEnum,
					rc.SectionEnum,
					rc.ShiftEnum
				})
				.Select(g => new StudentWiseRegisteredCoursesSummary
				{
					SemesterID = g.Key.SemesterID,
					ProgramAlias = g.Key.ProgramAlias,
					SemesterNoEnum = g.Key.SemesterNoEnum,
					SectionEnum = g.Key.SectionEnum,
					ShiftEnum = g.Key.ShiftEnum,
					Enrollment = g.Key.Enrollment,
					Courses = g.Count(),
					CreditHours = g.Sum(gg => gg.CreditHours)
				});

				if (creditHours != null)
					summaryQuery = summaryQuery.Where(rc => rc.CreditHours >= creditHours.Value);
				if (registeredCourses != null)
					summaryQuery = summaryQuery.Where(rc => rc.Courses >= registeredCourses.Value);
				virtualItemCount = summaryQuery.Count();
				switch (sortExpression)
				{
					case nameof(StudentWiseRegisteredCoursesSummary.SemesterID):
						summaryQuery = summaryQuery.OrderBy(sortDirection, rc => rc.SemesterID);
						break;
					case nameof(StudentWiseRegisteredCoursesSummary.ProgramAlias):
						summaryQuery = summaryQuery.OrderBy(sortDirection, rc => rc.ProgramAlias);
						break;
					case nameof(StudentWiseRegisteredCoursesSummary.SemesterNoEnum):
						summaryQuery = summaryQuery.OrderBy(sortDirection, rc => rc.SemesterNoEnum);
						break;
					case nameof(StudentWiseRegisteredCoursesSummary.SectionEnum):
						summaryQuery = summaryQuery.OrderBy(sortDirection, rc => rc.SectionEnum);
						break;
					case nameof(StudentWiseRegisteredCoursesSummary.ShiftEnum):
						summaryQuery = summaryQuery.OrderBy(sortDirection, rc => rc.ShiftEnum);
						break;
					case nameof(StudentWiseRegisteredCoursesSummary.Enrollment):
						summaryQuery = summaryQuery.OrderBy(sortDirection, rc => rc.Enrollment);
						break;
					case nameof(StudentWiseRegisteredCoursesSummary.Courses):
						summaryQuery = summaryQuery.OrderBy(sortDirection, rc => rc.Courses);
						break;
					case nameof(StudentWiseRegisteredCoursesSummary.CreditHours):
						summaryQuery = summaryQuery.OrderBy(sortDirection, rc => rc.CreditHours);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return summaryQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList();
			}
		}
	}
}