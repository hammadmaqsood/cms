﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.CourseRegistration.Staff
{
	public static class ExemptedCourses
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.ManageExemptedCourses, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class StudentExemptedCourse
		{
			internal StudentExemptedCourse() { }
			public int StudentExemptedCourseID { get; internal set; }
			public int CourseID { get; internal set; }
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public string CreditHoursFullName => this.CreditHours.ToCreditHoursFullName();
			public int? SubstitutedCourseID { get; internal set; }
			public string SubstitutedCourseCode { get; internal set; }
			public string SubstitutedTitle { get; internal set; }
			public decimal? SubstitutedCreditHours { get; internal set; }
			public string SubstitutedCreditHoursFullName => this.SubstitutedCreditHours?.ToCreditHoursFullName();
			public DateTime ExemptionDate { get; internal set; }
			public string Remarks { get; internal set; }
		}

		public static List<StudentExemptedCourse> GetStudentExemptedCourses(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var exemptedCourses = from c in aspireContext.Courses
									  join sec in aspireContext.StudentExemptedCourses on c.CourseID equals sec.CourseID
									  where sec.StudentID == studentID && sec.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID
									  select new
									  {
										  sec.StudentExemptedCourseID,
										  sec.StudentID,
										  sec.CourseID,
										  sec.SubstitutedCourseID,
										  sec.ExemptionDate,
										  sec.Remarks,
										  c.Title,
										  c.CourseCode,
										  c.CreditHours,
									  };
				var exemptedCoursesWithSubstition = from sec in exemptedCourses
													join c in aspireContext.Courses on sec.SubstitutedCourseID equals c.CourseID into grp
													from g in grp.DefaultIfEmpty()
													orderby g.Title
													select new StudentExemptedCourse
													{
														StudentExemptedCourseID = sec.StudentExemptedCourseID,
														CourseID = sec.CourseID,
														CreditHours = sec.CreditHours,
														CourseCode = sec.CourseCode,
														Remarks = sec.Remarks,
														SubstitutedCourseID = sec.SubstitutedCourseID,
														Title = sec.Title,
														ExemptionDate = sec.ExemptionDate,
														SubstitutedCreditHours = g.CreditHours,
														SubstitutedCourseCode = g.CourseCode,
														SubstitutedTitle = g.Title,
													};
				return exemptedCoursesWithSubstition.OrderBy(c => c.Title).ToList();
			}
		}

		public static StudentExemptedCours GetStudentExemptedCourse(int exemptedCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.StudentExemptedCourses.SingleOrDefault(ec => ec.StudentExemptedCourseID == exemptedCourseID && ec.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
			}
		}

		public enum AddStudentExemptedCourseStatuses
		{
			CourseAlreadyExists,
			NoRecordFound,
			Success
		}

		public static AddStudentExemptedCourseStatuses AddStudentExemptedCourse(int studentID, int courseID, DateTime exemptionDate, int? substitutedCourseID, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => new
				{
					s.StudentID,
					s.AdmissionOpenProgram.Program.InstituteID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgramID,
				}).SingleOrDefault();
				if (student == null)
					return AddStudentExemptedCourseStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);

				if (aspireContext.StudentExemptedCourses.Any(sec => sec.StudentID == student.StudentID && sec.CourseID == courseID))
					return AddStudentExemptedCourseStatuses.CourseAlreadyExists;
				if (!aspireContext.Courses.Any(c => c.AdmissionOpenProgramID == student.AdmissionOpenProgramID && c.CourseID == courseID))
					return AddStudentExemptedCourseStatuses.NoRecordFound;

				aspireContext.StudentExemptedCourses.Add(new StudentExemptedCours
				{
					CourseID = courseID,
					ExemptionDate = exemptionDate,
					Remarks = remarks.TrimAndMakeItNullIfEmpty(),
					StudentID = student.StudentID,
					SubstitutedCourseID = substitutedCourseID,
				});
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddStudentExemptedCourseStatuses.Success;
			}
		}

		public enum UpdateStudentExemptedCourseStatuses
		{
			NoRecordFound,
			Success
		}

		public static UpdateStudentExemptedCourseStatuses UpdateStudentExemptedCourse(int studentExemptedCourseID, DateTime exemptionDate, int? substitutedCourseID, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.StudentExemptedCourses.Where(sec => sec.StudentExemptedCourseID == studentExemptedCourseID).Select(sec => new
				{
					StudentExemptedCourse = sec,
					sec.StudentID,
					sec.Student.AdmissionOpenProgram.Program.InstituteID,
					sec.Student.AdmissionOpenProgram.Program.DepartmentID,
					sec.Student.AdmissionOpenProgram.ProgramID,
					sec.Student.AdmissionOpenProgramID,
				}).SingleOrDefault();
				if (student == null)
					return UpdateStudentExemptedCourseStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);

				student.StudentExemptedCourse.ExemptionDate = exemptionDate;
				student.StudentExemptedCourse.SubstitutedCourseID = substitutedCourseID;
				student.StudentExemptedCourse.Remarks = remarks;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateStudentExemptedCourseStatuses.Success;
			}
		}

		public enum DeleteStudentExemptedCourseStatuses
		{
			NoRecordFound,
			Success
		}

		public static DeleteStudentExemptedCourseStatuses DeleteStudentExemptedCourse(int studentExemptedCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.StudentExemptedCourses.Where(sec => sec.StudentExemptedCourseID == studentExemptedCourseID).Select(sec => new
				{
					StudentExemptedCourse = sec,
					sec.StudentID,
					sec.Student.AdmissionOpenProgram.Program.InstituteID,
					sec.Student.AdmissionOpenProgram.Program.DepartmentID,
					sec.Student.AdmissionOpenProgram.ProgramID,
					sec.Student.AdmissionOpenProgramID,
				}).SingleOrDefault();
				if (student == null)
					return DeleteStudentExemptedCourseStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);

				aspireContext.StudentExemptedCourses.Remove(student.StudentExemptedCourse);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteStudentExemptedCourseStatuses.Success;
			}
		}
	}
}