﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.BL.Core.CourseRegistration.Staff
{
	public static class SemesterFreeze
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.ManageSemesterFreezed, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public enum UpdateStudentSemesterFreezedStatusResults
		{
			NoRecordFound,
			Success,
		}

		public static UpdateStudentSemesterFreezedStatusResults UpdateStudentSemesterFreezedStatus(int studentSemesterID, FreezedStatuses? freezedStatusEnum, DateTime? freezedDate, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var studentSemester = aspireContext.StudentSemesters.SingleOrDefault(ss => ss.StudentSemesterID == studentSemesterID);
				if (studentSemester == null)
					return UpdateStudentSemesterFreezedStatusResults.NoRecordFound;
				var student = aspireContext.Students.Where(s => s.StudentID == studentSemester.StudentID).Select(s => new
				{
					s.AdmissionOpenProgramID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.Program.InstituteID
				}).Single();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);

				if (freezedStatusEnum != null && freezedDate == null)
					throw new ArgumentNullException(nameof(freezedDate));
				if (freezedStatusEnum == null && freezedDate != null)
					throw new ArgumentNullException(nameof(freezedStatusEnum));

				studentSemester.FreezedStatusEnum = freezedStatusEnum;
				studentSemester.FreezedDate = freezedDate;
				studentSemester.Remarks = remarks;

				if (studentSemester.StatusEnum == StudentSemester.Statuses.Registered)
				{
					var registeredCourses = aspireContext.RegisteredCourses.Where(rc => rc.StudentID == studentSemester.StudentID && rc.OfferedCours.SemesterID == studentSemester.SemesterID).ToList();
					foreach (var registeredCourse in registeredCourses)
					{
						registeredCourse.FreezeStatusEnum = studentSemester.FreezedStatusEnum;
						registeredCourse.FreezedDate = studentSemester.FreezedDate;
					}
				}

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateStudentSemesterFreezedStatusResults.Success;
			}
		}
	}
}
