using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.CourseRegistration.Staff
{
	public static class TimeBarredStudents
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.ManageTimeBarredStudents, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public enum AddTimeBarredStudentStatuses
		{
			NoRecordFound,
			InvalidTimeBar,
			Success,
		}

		public static AddTimeBarredStudentStatuses AddTimeBarredStudent(int studentID, short minSemesterID, short maxSemesterID, CourseCategories courseCategoriesAllowedEnum, decimal? maxCreditHours, string remarks, Guid loginSessionGuid)
		{
			if (minSemesterID > maxSemesterID)
				throw new ArgumentException("Invalid Semester Range", nameof(maxSemesterID));
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => new
				{
					s.StudentID,
					s.AdmissionOpenProgram.Program.InstituteID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgramID,
					s.AdmissionOpenProgram.SemesterID,
					s.AdmissionOpenProgram.MaxSemesterID,
					TimeBarredStudents = s.TimeBarredStudents.ToList(),
				}).SingleOrDefault();
				if (student == null)
					return AddTimeBarredStudentStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);

				if (minSemesterID <= student.MaxSemesterID || maxSemesterID <= student.MaxSemesterID)
					return AddTimeBarredStudentStatuses.InvalidTimeBar;

				var min = minSemesterID;
				while (min <= maxSemesterID)
				{
					if (student.TimeBarredStudents.Any(tbs => tbs.MinSemesterID <= min && min <= tbs.MaxSemesterID))
						return AddTimeBarredStudentStatuses.InvalidTimeBar;
					min = Semester.AddSemesters(min, 1, true);
				}

				var timeBarredStudent = new TimeBarredStudent
				{
					StudentID = studentID,
					MinSemesterID = minSemesterID,
					MaxSemesterID = maxSemesterID,
					CourseCategoriesAllowedEnum = courseCategoriesAllowedEnum,
					MaxCreditHours = maxCreditHours,
					Remarks = remarks.CannotBeEmptyOrWhitespace(),
				};
				aspireContext.TimeBarredStudents.Add(timeBarredStudent);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddTimeBarredStudentStatuses.Success;
			}
		}

		public enum DeleteTimeBarredStudentStatuses
		{
			Success,
			NoRecordFound,
			ChildRecordExists,
		}

		public static DeleteTimeBarredStudentStatuses DeleteTimeBarredStudent(int timeBarredStudentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.TimeBarredStudents.Where(tbs => tbs.TimeBarredStudentID == timeBarredStudentID).Select(tbs => new
				{
					tbs.Student.StudentID,
					tbs.Student.AdmissionOpenProgram.Program.InstituteID,
					tbs.Student.AdmissionOpenProgram.Program.DepartmentID,
					tbs.Student.AdmissionOpenProgram.ProgramID,
					tbs.Student.AdmissionOpenProgramID,
					tbs.Student.AdmissionOpenProgram.SemesterID,
					tbs.Student.AdmissionOpenProgram.MaxSemesterID,
					TimeBarredStudent = tbs,
				}).SingleOrDefault();
				if (student == null)
					return DeleteTimeBarredStudentStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);

				if (aspireContext.StudentSemesters.Any(ss => ss.StudentID == student.StudentID && student.TimeBarredStudent.MinSemesterID <= ss.SemesterID && ss.SemesterID <= student.TimeBarredStudent.MaxSemesterID && ss.Status == StudentSemester.StatusRegistered))
					return DeleteTimeBarredStudentStatuses.ChildRecordExists;
				aspireContext.TimeBarredStudents.Remove(student.TimeBarredStudent);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteTimeBarredStudentStatuses.Success;
			}
		}

		public static List<Model.Entities.TimeBarredStudent> GetTimeBarredStudent(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.TimeBarredStudents.Where(tbs => tbs.StudentID == studentID && tbs.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID).OrderBy(tbs => tbs.MinSemesterID).ToList();
			}
		}

		public enum UpdateTimeBarredStudentStatuses
		{
			NoRecordFound,
			InvalidTimeBar,
			Success,
			CannotUpdateChildRecordExists,
		}

		public static UpdateTimeBarredStudentStatuses UpdateTimeBarredStudent(int timeBarredStudentID, short minSemesterID, short maxSemesterID, CourseCategories courseCategoriesAllowedEnum, decimal? maxCreditHours, string remarks, Guid loginSessionGuid)
		{
			if (minSemesterID > maxSemesterID)
				throw new ArgumentException("Invalid Semester Range", nameof(maxSemesterID));
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.TimeBarredStudents.Where(tbs => tbs.TimeBarredStudentID == timeBarredStudentID).Select(tbs => new
				{
					tbs.Student.StudentID,
					tbs.Student.AdmissionOpenProgram.Program.InstituteID,
					tbs.Student.AdmissionOpenProgram.Program.DepartmentID,
					tbs.Student.AdmissionOpenProgram.ProgramID,
					tbs.Student.AdmissionOpenProgramID,
					tbs.Student.AdmissionOpenProgram.SemesterID,
					tbs.Student.AdmissionOpenProgram.MaxSemesterID,
					TimeBarredStudents = tbs.Student.TimeBarredStudents.ToList(),
				}).SingleOrDefault();
				if (student == null)
					return UpdateTimeBarredStudentStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);

				if (minSemesterID <= student.MaxSemesterID || maxSemesterID <= student.MaxSemesterID)
					return UpdateTimeBarredStudentStatuses.InvalidTimeBar;

				var min = minSemesterID;
				while (min <= maxSemesterID)
				{
					if (student.TimeBarredStudents.Any(tbs => tbs.TimeBarredStudentID != timeBarredStudentID && tbs.MinSemesterID <= min && min <= tbs.MaxSemesterID))
						return UpdateTimeBarredStudentStatuses.InvalidTimeBar;
					min = Semester.AddSemesters(min, 1, true);
				}

				var timeBarredStudent = student.TimeBarredStudents.Single(tbs => tbs.TimeBarredStudentID == timeBarredStudentID);

				var registeredCourses = aspireContext.RegisteredCourses
						.Where(rc => rc.StudentID == timeBarredStudent.StudentID && timeBarredStudent.MinSemesterID <= rc.OfferedCours.SemesterID && rc.OfferedCours.SemesterID <= timeBarredStudent.MaxSemesterID)
						.Select(rc => new
						{
							rc.OfferedCours.SemesterID,
							rc.Cours.CourseCategory,
							rc.Cours.CreditHours
						}).ToList();

				if (registeredCourses.Any())
				{
					if (registeredCourses.Any(rc => rc.SemesterID < minSemesterID || maxSemesterID < rc.SemesterID))
						return UpdateTimeBarredStudentStatuses.CannotUpdateChildRecordExists;
					if (maxCreditHours != null)
						if (registeredCourses.Sum(rc => rc.CreditHours) > maxCreditHours)
							return UpdateTimeBarredStudentStatuses.CannotUpdateChildRecordExists;
					if (registeredCourses.Any(rc => !courseCategoriesAllowedEnum.HasFlag((CourseCategories)rc.CourseCategory)))
						return UpdateTimeBarredStudentStatuses.CannotUpdateChildRecordExists;
				}

				timeBarredStudent.MinSemesterID = minSemesterID;
				timeBarredStudent.MaxSemesterID = maxSemesterID;
				timeBarredStudent.MaxCreditHours = maxCreditHours;
				timeBarredStudent.CourseCategoriesAllowedEnum = courseCategoriesAllowedEnum;
				timeBarredStudent.Remarks = remarks.CannotBeEmptyOrWhitespace();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateTimeBarredStudentStatuses.Success;
			}
		}

		public static TimeBarredStudent GetTimeBarredofStudent(int timeBarredStudentID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.TimeBarredStudents.SingleOrDefault(tbs => tbs.TimeBarredStudentID == timeBarredStudentID);
			}
		}
	}
}
