using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.CourseRegistration.Staff
{
	public static class StudentCreditsTransfers
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.ManageStudentCreditTransfer, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class StudentCreditsTransfer
		{
			public int StudentCreditsTransferID { get; internal set; }
			public string CommitteeMembers { get; internal set; }
			public string InstituteName { get; internal set; }
			public string Remarks { get; internal set; }
			public DateTime TransferredDate { get; internal set; }
			public short TransferredSemesterID { get; internal set; }
			public List<Course> Courses { get; internal set; }

			public sealed class Course
			{
				public int StudentCreditsTransferredCourseID { get; internal set; }
				public string CourseTitle { get; internal set; }
				public string Grade { get; internal set; }
				public string CourseCode { get; internal set; }
				public string Title { get; internal set; }
				public string CourseInfo => (this.Title == null ? null : $"[{this.CourseCode}] {this.Title}").ToNAIfNullOrEmpty();
				public byte? EquivalentGrade { get; internal set; }
				public string EquivalentGradeFullName => (((ExamGrades?)this.EquivalentGrade)?.ToFullName()).ToNAIfNullOrEmpty();
			}
		}

		public static StudentCreditsTransfer GetStudentCreditsTransfer(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.StudentCreditsTransfers
					.Where(s => s.StudentID == studentID && s.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Select(s => new StudentCreditsTransfer
					{
						StudentCreditsTransferID = s.StudentCreditsTransferID,
						InstituteName = s.InstituteName,
						TransferredDate = s.TransferredDate,
						TransferredSemesterID = s.TransferredSemesterID,
						CommitteeMembers = s.CommitteeMembers,
						Remarks = s.Remarks,
						Courses = s.StudentCreditsTransferredCourses.Select(c => new StudentCreditsTransfer.Course
						{
							StudentCreditsTransferredCourseID = c.StudentCreditsTransferredCourseID,
							CourseTitle = c.CourseTitle,
							Grade = c.Grade,
							CourseCode = c.Cours.CourseCode,
							Title = c.Cours.Title,
							EquivalentGrade = c.EquivalentGrade,
						}).OrderBy(c => c.Title).ToList()
					}).SingleOrDefault();
			}
		}

		public enum AddStudentCreditsTransferStatus
		{
			RecordAlreadyExists,
			Success
		}

		public static AddStudentCreditsTransferStatus AddStudentCreditsTransfer(int studentID, string instituteName, DateTime transferredDate, short transferredSemesterID, string committeeMembers, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => new
				{
					s.AdmissionOpenProgram.Program.InstituteID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgramID,
					StudentCreditsTransferExists = s.StudentCreditsTransfers.Any(),
				}).SingleOrDefault();
				if (student == null)
					return AddStudentCreditsTransferStatus.RecordAlreadyExists;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				if (student.StudentCreditsTransferExists)
					return AddStudentCreditsTransferStatus.RecordAlreadyExists;
				var studentCreditsTransfer = new Model.Entities.StudentCreditsTransfer
				{
					StudentID = studentID,
					InstituteName = instituteName,
					TransferredDate = transferredDate,
					TransferredSemesterID = transferredSemesterID,
					CommitteeMembers = committeeMembers,
					Remarks = remarks,
				}.Validate();
				aspireContext.StudentCreditsTransfers.Add(studentCreditsTransfer);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddStudentCreditsTransferStatus.Success;
			}
		}

		public enum UpdateStudentCreditsTransferStatuses
		{
			NoRecordFound,
			Success
		}

		public static UpdateStudentCreditsTransferStatuses UpdateStudentCreditsTransfer(int studentCreditsTransferID, string instituteName, DateTime transferredDate, short transferredSemesterID, string committeeMembers, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.StudentCreditsTransfers
					.Where(sct => sct.StudentCreditsTransferID == studentCreditsTransferID)
					.Select(sct => new
					{
						sct.Student.AdmissionOpenProgram.Program.InstituteID,
						sct.Student.AdmissionOpenProgram.Program.DepartmentID,
						sct.Student.AdmissionOpenProgram.ProgramID,
						sct.Student.AdmissionOpenProgramID,
						StudentCreditsTransfer = sct,
					}).SingleOrDefault();
				if (student == null)
					return UpdateStudentCreditsTransferStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);

				student.StudentCreditsTransfer.InstituteName = instituteName;
				student.StudentCreditsTransfer.TransferredDate = transferredDate;
				student.StudentCreditsTransfer.TransferredSemesterID = transferredSemesterID;
				student.StudentCreditsTransfer.CommitteeMembers = committeeMembers;
				student.StudentCreditsTransfer.Remarks = remarks;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateStudentCreditsTransferStatuses.Success;
			}
		}

		public enum DeleteStudentCreditsTransferStatuses
		{
			NoRecordFound,
			ChildRecordExists,
			Success
		}

		public static DeleteStudentCreditsTransferStatuses DeleteStudentCreditsTransfer(int studentCreditsTransferID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.StudentCreditsTransfers
									.Where(sct => sct.StudentCreditsTransferID == studentCreditsTransferID)
									.Select(sct => new
									{
										StudentCreditsTransfer = sct,
										sct.Student.AdmissionOpenProgram.Program.InstituteID,
										sct.Student.AdmissionOpenProgram.Program.DepartmentID,
										sct.Student.AdmissionOpenProgram.ProgramID,
										sct.Student.AdmissionOpenProgramID,
										ChildRecordExists = sct.StudentCreditsTransferredCourses.Any()
									})
									.SingleOrDefault();
				if (student == null)
					return DeleteStudentCreditsTransferStatuses.NoRecordFound;

				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				if (student.ChildRecordExists)
					return DeleteStudentCreditsTransferStatuses.ChildRecordExists;
				aspireContext.StudentCreditsTransfers.Remove(student.StudentCreditsTransfer);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteStudentCreditsTransferStatuses.Success;
			}
		}

		public enum AddStudentCreditsTransferredCourseStatuses
		{
			NoRecordFound,
			EquivalentCourseAlreadyExists,
			CourseTitleAlreadyExists,
			ExamMarksPolicyNotFound,
			EquivalentGradeNotAllowedAsPerExamPolicy,
			Success
		}

		public static AddStudentCreditsTransferredCourseStatuses AddStudentCreditsTransferredCourse(int studentCreditsTransferID, string courseTitle, string grade, int? equivalentCourseID, ExamGrades? equivalentGradeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.StudentCreditsTransfers
					.Where(sct => sct.StudentCreditsTransferID == studentCreditsTransferID)
					.Select(sct => new
					{
						sct.Student.AdmissionOpenProgram.Program.InstituteID,
						sct.Student.AdmissionOpenProgram.Program.DepartmentID,
						sct.Student.AdmissionOpenProgram.ProgramID,
						sct.Student.AdmissionOpenProgramID,
						sct.TransferredSemesterID,
						StudentCreditsTransferedCourses = sct.StudentCreditsTransferredCourses.Select(c => new
						{
							c.CourseTitle,
							c.EquivalentCourseID,
						}).ToList(),
					}).SingleOrDefault();
				if (student == null)
					return AddStudentCreditsTransferredCourseStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				var studentCreditsTransferredCourse = new StudentCreditsTransferredCours
				{
					StudentCreditsTransferID = studentCreditsTransferID,
					CourseTitle = courseTitle,
					Grade = grade,
					EquivalentGradeEnum = equivalentGradeEnum,
					EquivalentCourseID = equivalentCourseID,
				}.Validate();

				if (student.StudentCreditsTransferedCourses.Any(c => c.CourseTitle == studentCreditsTransferredCourse.CourseTitle))
					return AddStudentCreditsTransferredCourseStatuses.CourseTitleAlreadyExists;

				if (studentCreditsTransferredCourse.EquivalentCourseID != null && studentCreditsTransferredCourse.EquivalentGradeEnum != null)
				{
					if (!aspireContext.Courses.Any(c => c.AdmissionOpenProgramID == student.AdmissionOpenProgramID && c.CourseID == studentCreditsTransferredCourse.EquivalentCourseID.Value))
						throw new InvalidOperationException("Invalid Roadmap Course.");
					if (student.StudentCreditsTransferedCourses.Any(sctc => sctc.EquivalentCourseID == studentCreditsTransferredCourse.EquivalentCourseID))
						return AddStudentCreditsTransferredCourseStatuses.EquivalentCourseAlreadyExists;

					var examMarksPolicy = aspireContext.AdmissionOpenProgramExamMarksPolicies
						.Where(aopemp => aopemp.AdmissionOpenProgramID == student.AdmissionOpenProgramID && aopemp.SemesterID == student.TransferredSemesterID)
						.Select(aopemp => aopemp.ExamMarksPolicy)
						.SingleOrDefault();
					if (examMarksPolicy == null)
						return AddStudentCreditsTransferredCourseStatuses.ExamMarksPolicyNotFound;

					if (!examMarksPolicy.IsExamGradeValidForUse(studentCreditsTransferredCourse.EquivalentGradeEnum.Value))
						return AddStudentCreditsTransferredCourseStatuses.EquivalentGradeNotAllowedAsPerExamPolicy;
				}

				aspireContext.StudentCreditsTransferredCourses.Add(studentCreditsTransferredCourse);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddStudentCreditsTransferredCourseStatuses.Success;
			}
		}

		public enum UpdateStudentCreditsTransferredCourseStatuses
		{
			NoRecordFound,
			EquivalentCourseAlreadyExists,
			CourseTitleAlreadyExists,
			ExamMarksPolicyNotFound,
			EquivalentGradeNotAllowedAsPerExamPolicy,
			Success
		}

		public static UpdateStudentCreditsTransferredCourseStatuses UpdateStudentCreditsTransferredCourse(int studentCreditsTransferredCourseID, string courseTitle, string grade, int? equivalentCourseID, ExamGrades? equivalentGradeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.StudentCreditsTransferredCourses
					.Where(sctc => sctc.StudentCreditsTransferredCourseID == studentCreditsTransferredCourseID)
					.Select(sctc => new
					{
						sctc.StudentCreditsTransfer.Student.AdmissionOpenProgram.Program.InstituteID,
						sctc.StudentCreditsTransfer.Student.AdmissionOpenProgram.Program.DepartmentID,
						sctc.StudentCreditsTransfer.Student.AdmissionOpenProgram.ProgramID,
						sctc.StudentCreditsTransfer.Student.AdmissionOpenProgramID,
						sctc.StudentCreditsTransfer.TransferredSemesterID,
						StudentCreditsTransferredCourse = sctc,
						StudentCreditsTransferredCourses = sctc.StudentCreditsTransfer.StudentCreditsTransferredCourses
							.Where(cc => cc.StudentCreditsTransferredCourseID != sctc.StudentCreditsTransferredCourseID)
							.Select(cc => new
							{
								cc.CourseTitle,
								cc.EquivalentCourseID,
							}).ToList(),
					}).SingleOrDefault();
				if (student == null)
					return UpdateStudentCreditsTransferredCourseStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);

				var studentCreditsTransferredCourse = student.StudentCreditsTransferredCourse;
				studentCreditsTransferredCourse.CourseTitle = courseTitle;
				studentCreditsTransferredCourse.Grade = grade;
				studentCreditsTransferredCourse.EquivalentCourseID = equivalentCourseID;
				studentCreditsTransferredCourse.EquivalentGradeEnum = equivalentGradeEnum;
				studentCreditsTransferredCourse.Validate();

				if (student.StudentCreditsTransferredCourses.Any(c => c.CourseTitle == studentCreditsTransferredCourse.CourseTitle))
					return UpdateStudentCreditsTransferredCourseStatuses.CourseTitleAlreadyExists;

				if (studentCreditsTransferredCourse.EquivalentCourseID != null && studentCreditsTransferredCourse.EquivalentGradeEnum != null)
				{
					if (!aspireContext.Courses.Any(c => c.AdmissionOpenProgramID == student.AdmissionOpenProgramID && c.CourseID == studentCreditsTransferredCourse.EquivalentCourseID.Value))
						throw new InvalidOperationException("Invalid Roadmap Course.");
					if (student.StudentCreditsTransferredCourses.Any(sctc => sctc.EquivalentCourseID == studentCreditsTransferredCourse.EquivalentCourseID))
						return UpdateStudentCreditsTransferredCourseStatuses.EquivalentCourseAlreadyExists;

					var examMarksPolicy = aspireContext.AdmissionOpenProgramExamMarksPolicies
						.Where(aopemp => aopemp.AdmissionOpenProgramID == student.AdmissionOpenProgramID && aopemp.SemesterID == student.TransferredSemesterID)
						.Select(aopemp => aopemp.ExamMarksPolicy)
						.SingleOrDefault();
					if (examMarksPolicy == null)
						return UpdateStudentCreditsTransferredCourseStatuses.ExamMarksPolicyNotFound;

					if (!examMarksPolicy.IsExamGradeValidForUse(studentCreditsTransferredCourse.EquivalentGradeEnum.Value))
						return UpdateStudentCreditsTransferredCourseStatuses.EquivalentGradeNotAllowedAsPerExamPolicy;
				}

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateStudentCreditsTransferredCourseStatuses.Success;
			}
		}

		public enum DeleteStudentCreditsTransferredCourseStatuses
		{
			NoRecordFound,
			Success
		}

		public static DeleteStudentCreditsTransferredCourseStatuses DeleteStudentCreditsTransferredCourse(int studentCreditsTransferredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.StudentCreditsTransferredCourses
					.Where(sct => sct.StudentCreditsTransferredCourseID == studentCreditsTransferredCourseID)
					.Select(sct => new
					{
						sct.StudentCreditsTransfer.Student.AdmissionOpenProgram.Program.InstituteID,
						sct.StudentCreditsTransfer.Student.AdmissionOpenProgram.Program.DepartmentID,
						sct.StudentCreditsTransfer.Student.AdmissionOpenProgram.ProgramID,
						sct.StudentCreditsTransfer.Student.AdmissionOpenProgramID,
						StudentCreditsTransferedCourse = sct,
					}).SingleOrDefault();

				if (student?.StudentCreditsTransferedCourse == null)
					return DeleteStudentCreditsTransferredCourseStatuses.NoRecordFound;

				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				aspireContext.StudentCreditsTransferredCourses.Remove(student.StudentCreditsTransferedCourse);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteStudentCreditsTransferredCourseStatuses.Success;
			}
		}

		public static StudentCreditsTransferredCours GetStudentCreditsTransferCourse(int studentCreditsTransferredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.StudentCreditsTransferredCourses.Where(c => c.StudentCreditsTransfer.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID).SingleOrDefault(stc => stc.StudentCreditsTransferredCourseID == studentCreditsTransferredCourseID);
			}
		}
	}
}