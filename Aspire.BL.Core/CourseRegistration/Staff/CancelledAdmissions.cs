﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.CourseRegistration.Staff
{
	public static class CancelledAdmissions
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.ManageCanceledAdmissions, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public sealed class CancelledAdmission
		{
			internal CancelledAdmission()
			{

			}

			public int CancelledAdmissionID { get; internal set; }
			public short SemesterID { get; internal set; }
			public byte Status { get; internal set; }
			public string Remarks { get; internal set; }

			public string Semester => this.SemesterID.ToSemesterString();
			public Model.Entities.CancelledAdmission.Statuses StatusEnum => (Model.Entities.CancelledAdmission.Statuses)this.Status;
		}

		public enum AddCancelledAdmissionStatuses
		{
			NoRecordFound,
			AlreadyExists,
			InvalidSemester,
			Success,
		}

		public static AddCancelledAdmissionStatuses AddCancelledAdmission(int studentID, short semesterID, Model.Entities.CancelledAdmission.Statuses statusEnum, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => new
				{
					s.StudentID,
					s.AdmissionOpenProgram.Program.InstituteID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgramID,
					s.AdmissionOpenProgram.SemesterID,
					s.AdmissionOpenProgram.MaxSemesterID,
					TimeBarredStudents = s.TimeBarredStudents.ToList(),
				}).SingleOrDefault();
				if (student == null)
					return AddCancelledAdmissionStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				if (aspireContext.CancelledAdmissions.Any(ca => ca.StudentID == studentID && ca.SemesterID == semesterID))
					return AddCancelledAdmissionStatuses.AlreadyExists;
				var studentSemeterIDs = aspireContext.StudentSemesters.Where(ss => ss.StudentID == studentID).Select(ss => ss.SemesterID).ToList();
				if (studentSemeterIDs.Contains(semesterID))
					return AddCancelledAdmissionStatuses.InvalidSemester;
				var cancelledAdmission = new Model.Entities.CancelledAdmission
				{
					StudentID = studentID,
					SemesterID = semesterID,
					StatusEnum = statusEnum,
					Remarks = remarks,
				};
				aspireContext.CancelledAdmissions.Add(cancelledAdmission.Validate());
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddCancelledAdmissionStatuses.Success;
			}
		}

		public enum UpdateCancelledAdmissionStatuses
		{
			NoRecordFound,
			Success,
		}

		public static UpdateCancelledAdmissionStatuses UpdateCancelledAdmission(int cancelledAdmissionID, int studentID, Model.Entities.CancelledAdmission.Statuses statusEnum, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => new
				{
					s.StudentID,
					s.AdmissionOpenProgram.Program.InstituteID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgramID,
					s.AdmissionOpenProgram.SemesterID,
					s.AdmissionOpenProgram.MaxSemesterID,
					TimeBarredStudents = s.TimeBarredStudents.ToList(),
				}).SingleOrDefault();
				if (student == null)
					return UpdateCancelledAdmissionStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				var cancelledAdmission = aspireContext.CancelledAdmissions.SingleOrDefault(ca => ca.CancelledAdmissionID == cancelledAdmissionID);
				if (cancelledAdmission == null)
					return UpdateCancelledAdmissionStatuses.NoRecordFound;
				cancelledAdmission.StatusEnum = statusEnum;
				cancelledAdmission.Remarks = remarks;
				cancelledAdmission.Validate();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateCancelledAdmissionStatuses.Success;
			}
		}

		public static Model.Entities.CancelledAdmission GetCancelledAdmission(int cancelledAdmissionID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.CancelledAdmissions.SingleOrDefault(ca => ca.CancelledAdmissionID == cancelledAdmissionID);
			}
		}

		public enum DeleteCancelledAdmissionStatuses
		{
			NoRecordFound,
			Success,
		}

		public static DeleteCancelledAdmissionStatuses DeleteCanCelledAdmission(int cancelledAdmissionID, int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => new
				{
					s.StudentID,
					s.AdmissionOpenProgram.Program.InstituteID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgramID,
					s.AdmissionOpenProgram.SemesterID,
					s.AdmissionOpenProgram.MaxSemesterID,
					TimeBarredStudents = s.TimeBarredStudents.ToList(),
				}).SingleOrDefault();
				if (student == null)
					return DeleteCancelledAdmissionStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				var cancelledAdmission = aspireContext.CancelledAdmissions.SingleOrDefault(ca => ca.CancelledAdmissionID == cancelledAdmissionID);
				if (cancelledAdmission == null)
					return DeleteCancelledAdmissionStatuses.NoRecordFound;
				aspireContext.CancelledAdmissions.Remove(cancelledAdmission);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteCancelledAdmissionStatuses.Success;
			}
		}

		public static List<CancelledAdmission> GetCancelledAdmissions(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = aspireContext.CancelledAdmissions.Where(ca => ca.StudentID == studentID);
				return query.Select(q => new CancelledAdmission
				{
					CancelledAdmissionID = q.CancelledAdmissionID,
					SemesterID = q.SemesterID,
					Status = q.Status,
					Remarks = q.Remarks,
				}).OrderByDescending(o => o.SemesterID).ToList();
			}
		}

		public sealed class StudentSemester
		{
			internal StudentSemester()
			{

			}

			public short SemesterID { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public byte Status { get; internal set; }
			public byte? FreezedStatus { get; internal set; }
			public string Remarks { get; internal set; }

			public string Semester => this.SemesterID.ToSemesterString();
			public string Class => AspireFormats.GetClassName(this.SemesterNo, this.Section, this.Shift);
			public string StatusFullName => ((Model.Entities.StudentSemester.Statuses)this.Status).ToFullName();
			public string FreezedStatusFullName => ((FreezedStatuses?)this.FreezedStatus)?.ToFullName();
		}

		public static List<StudentSemester> GetStudentSemesters(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = aspireContext.StudentSemesters.OrderBy(s => s.SemesterID).Where(s => s.StudentID == studentID && s.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				return query.Select(q => new StudentSemester
				{
					SemesterID = q.SemesterID,
					SemesterNo = q.SemesterNo,
					Section = q.Section,
					Shift = q.Shift,
					Status = q.Status,
					FreezedStatus = q.FreezedStatus,
					Remarks = q.Remarks,
				}).ToList();
			}
		}
	}
}
