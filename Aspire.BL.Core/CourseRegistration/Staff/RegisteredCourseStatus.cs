﻿using Aspire.BL.Core.Common.EmailsManagement;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aspire.BL.Core.CourseRegistration.Staff
{
	public static class RegisteredCourseStatus
	{
		//private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		//{
		//	return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.Module, UserGroupPermission.PermissionValues.Allowed);
		//}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.ManageRegisteredCourseStatus, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public enum UpdateRegisteredCourseStatusResult
		{
			NoRecordFound,
			MarksEntryLocked,
			Success
		}

		public static UpdateRegisteredCourseStatusResult UpdateRegisteredCourseStatus(int registeredCourseID, RegisteredCours.Statuses? statusEnum, DateTime? statusDate, string remarks, Guid loginSessionGuid)
		{
			if (statusEnum == null && statusDate != null)
				throw new ArgumentNullException(nameof(statusEnum));
			if (statusEnum != null && statusDate == null)
				throw new ArgumentNullException(nameof(statusDate));
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.RegisteredCourses
					.Where(rc => rc.RegisteredCourseID == registeredCourseID && rc.DeletedDate == null)
					.Select(rc => new
					{
						rc.Student.AdmissionOpenProgram.Program.InstituteID,
						rc.Student.AdmissionOpenProgram.Program.DepartmentID,
						rc.Student.AdmissionOpenProgram.ProgramID,
						rc.Student.AdmissionOpenProgramID,
						RegisteredCourse = rc,
						rc.OfferedCours.MarksEntryLocked
					}).SingleOrDefault();
				if (student == null)
					return UpdateRegisteredCourseStatusResult.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				if (student.MarksEntryLocked)
					return UpdateRegisteredCourseStatusResult.MarksEntryLocked;
				student.RegisteredCourse.StatusEnum = statusEnum;
				student.RegisteredCourse.StatusDate = statusEnum == null ? (DateTime?)null : statusDate.Value;
				switch (student.RegisteredCourse.StatusEnum)
				{
					case RegisteredCours.Statuses.AttendanceDefaulter:
					case RegisteredCours.Statuses.Incomplete:
					case RegisteredCours.Statuses.WithdrawWithFee:
					case RegisteredCours.Statuses.WithdrawWithoutFee:
						student.RegisteredCourse.GradeEnum = null;
						student.RegisteredCourse.GradePoints = null;
						student.RegisteredCourse.Product = null;
						break;
					case RegisteredCours.Statuses.UnfairMeans:
						student.RegisteredCourse.GradeEnum = ExamGrades.F;
						student.RegisteredCourse.GradePoints = 0;
						student.RegisteredCourse.Product = 0;
						break;
					case RegisteredCours.Statuses.Deferred:
					case null:
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				student.RegisteredCourse.Remarks = remarks.ToNullIfWhiteSpace();
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateRegisteredCourseStatusResult.Success;
			}
		}

		public static void UpdateRegisteredCourseStatusToAttendanceDefaulterAndEmailResult(short offeredSemesterID, string registeredCourseIDs, Guid loginSessionGuid)
		{
			if (string.IsNullOrEmpty(registeredCourseIDs))
				throw new ArgumentNullException();
			var result = new List<Tuple<string, string>>();
			using (var aspireContext = new AspireContext(true))
			{
				var staffLoginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var user = aspireContext.Users.Where(u => u.UserID == staffLoginHistory.UserID).Select(u => new
				{
					u.Email,
					u.Name
				}).Single();
				foreach (var line in registeredCourseIDs.Split('\n'))
				{
					var registeredCourseID = line.TryToInt();
					if (registeredCourseID == null)
					{
						result.Add(new Tuple<string, string>(line, "Invalid RegisteredCourseID."));
						continue;
					}
					var student = aspireContext.RegisteredCourses
						.Where(rc => rc.RegisteredCourseID == registeredCourseID && rc.DeletedDate == null)
						.Where(rc => rc.OfferedCours.SemesterID == offeredSemesterID)
						.Select(rc => new
						{
							rc.Student.AdmissionOpenProgram.Program.InstituteID,
							rc.Student.AdmissionOpenProgram.Program.DepartmentID,
							rc.Student.AdmissionOpenProgram.ProgramID,
							rc.Student.AdmissionOpenProgramID,
							RegisteredCourse = rc,
						}).SingleOrDefault();
					if (student == null)
					{
						result.Add(new Tuple<string, string>(line, "No record found."));
						continue;
					}
					var loginHistory = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.ManageRegisteredCourseStatus, UserGroupPermission.PermissionValues.Allowed, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
					if (loginHistory == null)
					{
						result.Add(new Tuple<string, string>(line, "Not Authorized to update this record."));
						continue;
					}
					switch (student.RegisteredCourse.StatusEnum)
					{
						case RegisteredCours.Statuses.AttendanceDefaulter:
						case RegisteredCours.Statuses.Deferred:
						case RegisteredCours.Statuses.Incomplete:
						case RegisteredCours.Statuses.WithdrawWithFee:
						case RegisteredCours.Statuses.WithdrawWithoutFee:
						case RegisteredCours.Statuses.UnfairMeans:
							result.Add(new Tuple<string, string>(line, $"Status is {student.RegisteredCourse.StatusEnum.Value.ToFullName()}."));
							continue;
						case null:
							student.RegisteredCourse.StatusEnum = RegisteredCours.Statuses.AttendanceDefaulter;
							student.RegisteredCourse.StatusDate = DateTime.Now;
							result.Add(new Tuple<string, string>(line, "Success"));
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
				aspireContext.SaveChanges(staffLoginHistory.UserLoginHistoryID);

				var csv = result.Select(d => new
				{
					RegisteredCourseID = d.Item1,
					Message = d.Item2
				}).ConvertToCSVFormat();

				var createdDate = DateTime.Now;
				var expiryDate = (DateTime?)null;
				var toList = new[] { (user.Name, user.Email) };
				var subject = $"Bulk Operation Attendance Defaulter";
				var emailBody = $"Please find the attachment for results.";
				var attachments = new[] { ($"Result Attendance Defaulter {offeredSemesterID} {createdDate:yyyyMMddhhmmss}.csv", Encoding.ASCII.GetBytes(csv)) };
				aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.BulkUpdateRegisteredCourseStatusToAttendanceDefaulter, subject, emailBody, false, Email.Priorities.Normal, createdDate, expiryDate, staffLoginHistory.UserLoginHistoryID, toList: toList, ccList: null, bccList: null, replyToList: null, attachmentList: attachments);
				aspireContext.CommitTransaction();
			}
		}
	}
}
