﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.CourseRegistration.Staff
{
	public static class AttendanceSummary
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class Summary
		{
			public sealed class AttendaceDetail
			{
				internal AttendaceDetail() { }
				public int OfferedCourseAttendanceID { get; internal set; }
				public int OfferedCourseID { get; internal set; }
				public DateTime ClassDate { get; internal set; }
				public byte ClassType { get; internal set; }
				public string ClassTypeFullName => ((OfferedCourseAttendance.ClassTypes)this.ClassType).ToString();
				public decimal TotalHours { get; internal set; }
				public decimal PresentHours { get; internal set; }
				public decimal AbsentHours => this.TotalHours - this.PresentHours;
				public decimal Percentage => this.TotalHours == 0 ? 0 : this.PresentHours * 100 / this.TotalHours;
				public string RoomName { get; internal set; }
				public string Remarks { get; internal set; }
				public string TopicsCovered { get; internal set; }
			}

			internal Summary() { }
			public CourseRegistration.RegisteredCourse RegisteredCourse { get; internal set; }
			public List<AttendaceDetail> AttendaceDetails { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public StudentSemester StudentSemester { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public decimal ClassesTotalHours { get; set; }
			public decimal PresentTotalHours { get; set; }
			public decimal PresentTotalPercentage { get; set; }
			public decimal AbsentTotalHours { get; set; }
		}

		public static Summary GetStudentAttendanceDetails(int registeredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);

				var registeredCourse = aspireContext.RegisteredCourses.Where(rc => rc.RegisteredCourseID == registeredCourseID && rc.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID).Select(rc => new
				{
					rc.StudentID,
					rc.RegisteredCourseID,
					rc.OfferedCourseID,
					rc.Student.Enrollment,
					rc.Student.Name,
					rc.Student.RegistrationNo,
					rc.Student.AdmissionOpenProgram.Program.ProgramAlias,
					StudentSemester = rc.Student.StudentSemesters.FirstOrDefault(ss => ss.SemesterID == rc.OfferedCours.SemesterID),
				}).SingleOrDefault();
				if (registeredCourse == null)
					return null;
				var summary = new Summary
				{
					Enrollment = registeredCourse.Enrollment,
					Name = registeredCourse.Name,
					StudentSemester = registeredCourse.StudentSemester,
					ProgramAlias = registeredCourse.ProgramAlias,
					RegistrationNo = registeredCourse.RegistrationNo,
					RegisteredCourse = aspireContext.GetStudentRegisteredCourse(registeredCourse.RegisteredCourseID, loginHistory.InstituteID),
					AttendaceDetails = aspireContext.OfferedCourseAttendances.Where(oca => oca.OfferedCourseID == registeredCourse.OfferedCourseID).Select(oca => new Summary.AttendaceDetail
					{
						OfferedCourseID = oca.OfferedCourseID,
						ClassDate = oca.ClassDate,
						ClassType = oca.ClassType,
						OfferedCourseAttendanceID = oca.OfferedCourseAttendanceID,
						Remarks = oca.Remarks,
						TopicsCovered = oca.TopicsCovered,
						RoomName = oca.Room.RoomName,
						TotalHours = oca.Hours,
						PresentHours = oca.OfferedCourseAttendanceDetails.Where(ocad => ocad.RegisteredCourseID == registeredCourse.RegisteredCourseID).Select(ocad => (decimal?)ocad.Hours).FirstOrDefault() ?? 0,
					}).OrderBy(a => a.ClassDate).ThenBy(a => a.OfferedCourseAttendanceID).ToList()
				};
				summary.ClassesTotalHours = summary.AttendaceDetails.Sum(d => d.TotalHours);
				summary.PresentTotalHours = summary.AttendaceDetails.Sum(d => d.PresentHours);
				summary.AbsentTotalHours = summary.AttendaceDetails.Sum(d => d.AbsentHours);
				summary.PresentTotalPercentage = summary.ClassesTotalHours == 0 ? 0 : (summary.PresentTotalHours * 100) / summary.ClassesTotalHours;
				return summary;
			}
		}
	}
}
