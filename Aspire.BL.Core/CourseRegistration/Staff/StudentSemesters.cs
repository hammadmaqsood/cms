﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.CourseRegistration.Staff
{
	public static class StudentSemesters
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseRegistration.ManageStudentSemesters, UserGroupPermission.PermissionValues.Allowed, instituteID, departmentID, programID, admissionOpenProgramID);
		}

		public static List<StudentSemester> GetStudentSemesters(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.StudentSemesters.OrderBy(s => s.SemesterID).Where(s => s.StudentID == studentID && s.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID).ToList();
			}
		}

		public static StudentSemester GetStudentSemester(int studentSemesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.StudentSemesters.SingleOrDefault(ss => ss.StudentSemesterID == studentSemesterID && ss.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
			}
		}

		public static StudentSemester GetStudentSemester(int studentID, short semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.StudentSemesters.SingleOrDefault(ss => ss.StudentID == studentID && ss.SemesterID == semesterID && ss.Student.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
			}
		}

		public enum AddStudentSemesterStatuses
		{
			NoRecordFound,
			Success,
			RecordAlreadyExists,
			CannotAddSemesterBeforeIntake
		}

		public static AddStudentSemesterStatuses AddStudentSemester(int studentID, short semesterID, SemesterNos semesterNoEnum, Sections sectionEnum, Shifts shiftEnum, decimal? buExamGPA, decimal? buExamCGPA, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var student = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => new
				{
					s.AdmissionOpenProgram.SemesterID,
					s.AdmissionOpenProgramID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.Program.InstituteID
				}).SingleOrDefault();
				if (student == null)
					return AddStudentSemesterStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);
				if (aspireContext.StudentSemesters.Any(ss => ss.StudentID == studentID && ss.SemesterID == semesterID))
					return AddStudentSemesterStatuses.RecordAlreadyExists;
				if (semesterID < student.SemesterID)
					return AddStudentSemesterStatuses.CannotAddSemesterBeforeIntake;
				var semester = new StudentSemester
				{
					StudentID = studentID,
					SemesterID = semesterID,
					SemesterNoEnum = semesterNoEnum,
					SectionEnum = sectionEnum,
					ShiftEnum = shiftEnum,
					Remarks = remarks,
					StatusEnum = aspireContext.RegisteredCourses.Any(rc => rc.StudentID == studentID && rc.DeletedDate == null && rc.OfferedCours.SemesterID == semesterID)
						? StudentSemester.Statuses.Registered
						: StudentSemester.Statuses.NotRegistered,
					BUExamGPA = null,
					BUExamCGPA = null
				};
				var canManageGPACGPA = aspireContext.GetStaffPermissionValue(loginSessionGuid, StaffPermissions.CourseRegistration.ManageStudentGPACGPA) == UserGroupPermission.PermissionValues.Allowed;
				if (canManageGPACGPA)
				{
					semester.BUExamGPA = buExamGPA;
					semester.BUExamCGPA = buExamCGPA;
				}
				aspireContext.StudentSemesters.Add(semester);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddStudentSemesterStatuses.Success;
			}
		}

		public enum UpdateStudentSemesterStatuses
		{
			NoRecordFound,
			Success,
		}

		public static UpdateStudentSemesterStatuses UpdateStudentSemester(int studentSemesterID, SemesterNos semesterNoEnum, Sections sectionEnum, Shifts shiftEnum, decimal? buExamGPA, decimal? buExamCGPA, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var studentSemester = aspireContext.StudentSemesters.SingleOrDefault(ss => ss.StudentSemesterID == studentSemesterID);
				if (studentSemester == null)
					return UpdateStudentSemesterStatuses.NoRecordFound;

				var student = aspireContext.Students.Where(s => s.StudentID == studentSemester.StudentID).Select(s => new
				{
					s.AdmissionOpenProgramID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.Program.InstituteID
				}).Single();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);

				studentSemester.SemesterNoEnum = semesterNoEnum;
				studentSemester.SectionEnum = sectionEnum;
				studentSemester.ShiftEnum = shiftEnum;
				var canManageGPACGPA = aspireContext.GetStaffPermissionValue(loginSessionGuid, StaffPermissions.CourseRegistration.ManageStudentGPACGPA) == UserGroupPermission.PermissionValues.Allowed;
				if (canManageGPACGPA)
				{
					studentSemester.BUExamGPA = buExamGPA;
					studentSemester.BUExamCGPA = buExamCGPA;
				}
				studentSemester.Remarks = remarks;
				studentSemester.StatusEnum = aspireContext.RegisteredCourses.Any(rc => rc.StudentID == studentSemester.StudentID && rc.DeletedDate == null && rc.OfferedCours.SemesterID == studentSemester.SemesterID)
					? StudentSemester.Statuses.Registered
					: StudentSemester.Statuses.NotRegistered;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateStudentSemesterStatuses.Success;
			}
		}

		public enum DeleteStudentSemesterStatuses
		{
			NoRecordFound,
			SemesterIsFreezed,
			RegisteredCoursesFound,
			Success,
		}

		public static DeleteStudentSemesterStatuses DeleteStudentSemester(int studentSemesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var studentSemester = aspireContext.StudentSemesters.SingleOrDefault(ss => ss.StudentSemesterID == studentSemesterID);
				if (studentSemester == null)
					return DeleteStudentSemesterStatuses.NoRecordFound;

				var student = aspireContext.Students.Where(s => s.StudentID == studentSemester.StudentID).Select(s => new
				{
					s.AdmissionOpenProgramID,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.Program.InstituteID
				}).Single();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid, student.InstituteID, student.DepartmentID, student.ProgramID, student.AdmissionOpenProgramID);

				if (studentSemester.StatusEnum == StudentSemester.Statuses.Registered)
					return DeleteStudentSemesterStatuses.RegisteredCoursesFound;
				if (studentSemester.FreezedStatus != null)
					return DeleteStudentSemesterStatuses.SemesterIsFreezed;
				aspireContext.StudentSemesters.Remove(studentSemester);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteStudentSemesterStatuses.Success;
			}
		}
	}
}