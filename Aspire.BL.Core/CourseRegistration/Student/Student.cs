﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.CourseRegistration.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.CourseRegistration.Student
{
	public static class CourseRegistration
	{
		public static CoursesRegister RegisterCourses(int studentID, short offeredSemesterID, List<int> offeredCourseIDs, bool requestOnly, Guid loginSessionGuid)
		{
			if (offeredCourseIDs == null || !offeredCourseIDs.Any())
				throw new ArgumentNullException(nameof(offeredCourseIDs));
			using (var aspireContext = new AspireContext(true))
			{
				var userLoginHistory = aspireContext.DemandStudentActiveSession(loginSessionGuid, SubUserTypes.None);
				var coursesRegister = CoursesRegister.CreateForStudent(aspireContext, studentID, offeredSemesterID, userLoginHistory.LoginSessionGuid);
				offeredCourseIDs.ForEach(id => coursesRegister.AddCourse(id, null, requestOnly));
				coursesRegister.Register();
				aspireContext.CommitTransaction();
				return coursesRegister;
			}
		}

		public sealed class OfferedCourses
		{
			public sealed class OfferedCourse : ExtensionMethods.OfferedCourse
			{
				internal OfferedCourse() { }
				public bool Registered { get; internal set; }
				public bool Requested { get; internal set; }
			}

			public ExtensionMethods.StudentClass StudentClass { get; internal set; }
			public short OfferedSemesterID { get; internal set; }
			public List<OfferedCourse> Courses { get; internal set; }
		}

		public static OfferedCourses GetAllOfferedCourses(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStudentActiveSession(loginSessionGuid, SubUserTypes.None);
				var studentID = loginHistory.StudentID;
				var student = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => new
				{
					s.StudentID,
					s.AdmissionOpenProgramID,
					s.AdmissionOpenProgram.IsSummerRegularSemester,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgram.Program.InstituteID,
					IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
				}).Single();
				var semesterID = aspireContext.GetRegistrationOpenSemesterID(student.ProgramID, CourseRegistrationSetting.Types.Student);
				if (semesterID == null || semesterID < student.IntakeSemesterID)
					return null;
				var offeredSemesterID = semesterID.Value;
				var offeredCourses = new OfferedCourses
				{
					OfferedSemesterID = offeredSemesterID,
					StudentClass = aspireContext.GetStudentClass(studentID, offeredSemesterID),
					Courses = new List<OfferedCourses.OfferedCourse>(),
				};

				var allOfferedCourses = aspireContext.GetOfferedCourses(student.InstituteID, student.AdmissionOpenProgramID, offeredSemesterID);
				var registeredCourses = aspireContext.RegisteredCourses.Where(rc => rc.StudentID == studentID && rc.DeletedDate == null && rc.OfferedCours.SemesterID == offeredSemesterID).Select(rc => new
				{
					rc.OfferedCourseID,
					rc.CourseID
				}).ToList();
				var requestedCourses = aspireContext.RequestedCourses.Where(rc => rc.StudentID == studentID && rc.OfferedCours.SemesterID == offeredSemesterID).Select(rc => rc.OfferedCourseID).ToList();

				foreach (var offeredCourse in allOfferedCourses)
				{
					var oCourse = new OfferedCourses.OfferedCourse
					{
						Program = offeredCourse.Program,
						Status = offeredCourse.Status,
						AdmissionOpenProgramID = offeredCourse.AdmissionOpenProgramID,
						SemesterID = offeredCourse.SemesterID,
						EquivalentCourseID = offeredCourse.EquivalentCourseID,
						OfferedCourseID = offeredCourse.OfferedCourseID,
						CourseID = offeredCourse.CourseID,
						CourseCode = offeredCourse.CourseCode,
						EquivalentCourseCode = offeredCourse.EquivalentCourseCode,
						EquivalentCreditHours = offeredCourse.EquivalentCreditHours,
						EquivalentMajors = offeredCourse.EquivalentMajors,
						EquivalentSemesterID = offeredCourse.EquivalentSemesterID,
						EquivalentTitle = offeredCourse.EquivalentTitle,
						ProgramID = offeredCourse.ProgramID,
						CreditHours = offeredCourse.CreditHours,
						Majors = offeredCourse.Majors,
						Section = offeredCourse.Section,
						Shift = offeredCourse.Shift,
						SemesterNo = offeredCourse.SemesterNo,
						SpecialOffered = offeredCourse.SpecialOffered,
						Title = offeredCourse.Title,
					};
					offeredCourse.Classes?.ForEach(c => oCourse.AddClass(c.SemesterNo, c.Section, c.Shift));
					offeredCourses.Courses.Add(oCourse);
					if (registeredCourses.Any(rc => rc.OfferedCourseID == offeredCourse.OfferedCourseID || rc.CourseID == offeredCourse.CourseID))
					{
						oCourse.Registered = true;
						oCourse.Requested = false;
					}
					else
					{
						oCourse.Registered = false;
						oCourse.Requested = requestedCourses.Contains(offeredCourse.OfferedCourseID);
					}
				}
				return offeredCourses;
			}
		}

		public sealed class RegisteredCourse
		{
			internal RegisteredCourse() { }
			public int RegisteredCourseID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Title { get; internal set; }
			public string TeacherName { get; internal set; }

			public string Program { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }

			public string ClassName => AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift);

			public string RegisteredCourseCode { get; internal set; }
			public string RegisteredTitle { get; internal set; }
			public decimal RegisteredCreditHours { get; internal set; }
			public string RegisteredMajors { get; internal set; }
			public byte? Status { get; internal set; }
			public RegisteredCours.Statuses? StatusEnum => (RegisteredCours.Statuses?)this.Status;
			public string StatusFullName => this.StatusEnum?.ToFullName();
			public byte? FeeStatus { get; internal set; }
			public StudentFee.Statuses? FeeStatusEnum => (StudentFee.Statuses?)this.FeeStatus;
			public string FeeStatusFullName => this.FeeStatusEnum?.ToFullName();
		}

		public static List<RegisteredCourse> GetRegisteredCourses(int studentID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.RegisteredCourses.Where(rc => rc.StudentID == studentID && rc.DeletedDate == null).OrderBy(rc => rc.OfferedCours.SemesterID).Select(rc => new RegisteredCourse
				{
					RegisteredCourseID = rc.RegisteredCourseID,
					SemesterID = rc.OfferedCours.SemesterID,
					Title = rc.OfferedCours.Cours.Title,
					Program = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = rc.OfferedCours.SemesterNo,
					Section = rc.OfferedCours.Section,
					Shift = rc.OfferedCours.Shift,
					RegisteredCreditHours = rc.Cours.CreditHours,
					RegisteredCourseCode = rc.Cours.CourseCode,
					RegisteredMajors = rc.Cours.ProgramMajor.Majors,
					RegisteredTitle = rc.Cours.Title,
					Status = rc.Status,
					TeacherName = rc.OfferedCours.FacultyMember.Name,
					FeeStatus = rc.StudentFee.Status
				}).ToList();
			}
		}
	}
}
