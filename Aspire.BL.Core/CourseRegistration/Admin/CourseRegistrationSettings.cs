﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.CourseRegistration.Admin;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.CourseRegistration.Admin
{
	public static class CourseRegistrationSettings
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.CourseRegistration.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int instituteID, Guid loginSessionGuid)
		{
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID, AdminPermissions.CourseRegistration.ManageCourseRegistrationSettings, UserGroupPermission.PermissionValues.Allowed);
		}

		public enum AddOrUpdateCourseRegistrationSettingStatuses
		{
			Added,
			Updated,
		}

		private static AddOrUpdateCourseRegistrationSettingStatuses AddOrUpdateCourseRegistrationSetting(this AspireContext aspireContext, short semesterID, int programID, Model.Entities.CourseRegistrationSetting.Types typeEnum, DateTime fromDate, DateTime toDate, Model.Entities.CourseRegistrationSetting.Statuses statusEnum, Core.Common.Permissions.Admin.AdminLoginHistory loginHistory)
		{
			var courseRegistrationSetting = aspireContext.CourseRegistrationSettings
				.SingleOrDefault(crs => crs.SemesterID == semesterID && crs.ProgramID == programID && crs.Type == (byte)typeEnum);
			AddOrUpdateCourseRegistrationSettingStatuses status;
			if (courseRegistrationSetting == null)
			{
				status = AddOrUpdateCourseRegistrationSettingStatuses.Added;
				courseRegistrationSetting = new Model.Entities.CourseRegistrationSetting
				{
					ProgramID = programID,
					TypeEnum = typeEnum,
					SemesterID = semesterID,
				};
				aspireContext.CourseRegistrationSettings.Add(courseRegistrationSetting);
			}
			else
				status = AddOrUpdateCourseRegistrationSettingStatuses.Updated;

			courseRegistrationSetting.FromDate = fromDate;
			courseRegistrationSetting.ToDate = toDate;
			courseRegistrationSetting.StatusEnum = statusEnum;
			aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);

			switch (statusEnum)
			{
				case Model.Entities.CourseRegistrationSetting.Statuses.Active:
					aspireContext.CourseRegistrationSettings.Where(csr => csr.CourseRegistrationSettingID != courseRegistrationSetting.CourseRegistrationSettingID)
						.Where(csr => csr.ProgramID == courseRegistrationSetting.ProgramID && csr.Status == Model.Entities.CourseRegistrationSetting.StatusActive && csr.Type == courseRegistrationSetting.Type)
						.ToList()
						.ForEach(csr => csr.StatusEnum = Model.Entities.CourseRegistrationSetting.Statuses.Inactive);
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
					break;
				case Model.Entities.CourseRegistrationSetting.Statuses.Inactive:
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
			}
			return status;
		}

		public sealed class AddOrUpdateCourseRegistrationSettingsResult
		{
			public enum Statuses
			{
				None,
				NoRecordFound,
				InvalidDateRange,
				Success,
			}

			public Statuses Status { get; internal set; }
			public int TotalRecords { get; internal set; }
			public int SuccessAdd { get; internal set; }
			public int SuccessUpdate { get; internal set; }
		}

		public static AddOrUpdateCourseRegistrationSettingsResult AddOrUpdateCourseRegistrationSettings(short semesterID, int instituteID, List<int> programIDs, Model.Entities.CourseRegistrationSetting.Types? typeEnum, DateTime fromDate, DateTime toDate, Model.Entities.CourseRegistrationSetting.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var courseRegistrationSettingResult = new AddOrUpdateCourseRegistrationSettingsResult();
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				if (fromDate.Date > toDate.Date)
				{
					courseRegistrationSettingResult.Status = AddOrUpdateCourseRegistrationSettingsResult.Statuses.InvalidDateRange;
					return courseRegistrationSettingResult;
				}

				var listProgramIDs = aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.Programs)
					.Where(p => programIDs.Contains(p.ProgramID))
					.Select(p => p.ProgramID).ToList();
				var typeEnums = typeEnum != null
					? new List<Model.Entities.CourseRegistrationSetting.Types> { typeEnum.Value }
					: Enum.GetValues(typeof(Model.Entities.CourseRegistrationSetting.Types)).OfType<Model.Entities.CourseRegistrationSetting.Types>().ToList();

				foreach (var programID in listProgramIDs)
				{
					foreach (var type in typeEnums)
					{
						courseRegistrationSettingResult.TotalRecords++;
						var result = aspireContext.AddOrUpdateCourseRegistrationSetting(semesterID, programID, type, fromDate, toDate, statusEnum, loginHistory);
						switch (result)
						{
							case AddOrUpdateCourseRegistrationSettingStatuses.Added:
								courseRegistrationSettingResult.SuccessAdd++;
								break;
							case AddOrUpdateCourseRegistrationSettingStatuses.Updated:
								courseRegistrationSettingResult.SuccessUpdate++;
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}
					}
				}
				aspireContext.CommitTransaction();
				courseRegistrationSettingResult.Status = AddOrUpdateCourseRegistrationSettingsResult.Statuses.Success;
				return courseRegistrationSettingResult;
			}
		}

		public enum DeleteCourseRegistrationSettingStatuses
		{
			NoRecordFound,
			Success,
		}

		public static DeleteCourseRegistrationSettingStatuses DeleteCourseRegistrationSetting(int courseRegistrationSettingID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var courseRegistrationSetting = aspireContext.CourseRegistrationSettings.Include(csr => csr.Program).SingleOrDefault(crs => crs.CourseRegistrationSettingID == courseRegistrationSettingID);
				if (courseRegistrationSetting == null)
					return DeleteCourseRegistrationSettingStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(courseRegistrationSetting.Program.InstituteID, loginSessionGuid);
				aspireContext.CourseRegistrationSettings.Remove(courseRegistrationSetting);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteCourseRegistrationSettingStatuses.Success;
			}
		}

		public static Model.Entities.CourseRegistrationSetting ToggleCourseRegistrationSettingStatus(int courseRegistrationSettingID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var courseRegistrationSetting = aspireContext.CourseRegistrationSettings.Include(csr => csr.Program).SingleOrDefault(crs => crs.CourseRegistrationSettingID == courseRegistrationSettingID);
				if (courseRegistrationSetting == null)
					return null;
				var loginHistory = aspireContext.DemandPermissions(courseRegistrationSetting.Program.InstituteID, loginSessionGuid);

				switch (courseRegistrationSetting.StatusEnum)
				{
					case Model.Entities.CourseRegistrationSetting.Statuses.Active:
						courseRegistrationSetting.StatusEnum = Model.Entities.CourseRegistrationSetting.Statuses.Inactive;
						break;
					case Model.Entities.CourseRegistrationSetting.Statuses.Inactive:
						courseRegistrationSetting.StatusEnum = Model.Entities.CourseRegistrationSetting.Statuses.Active;
						aspireContext.CourseRegistrationSettings.Where(csr => csr.CourseRegistrationSettingID != courseRegistrationSetting.CourseRegistrationSettingID)
							.Where(csr => csr.ProgramID == courseRegistrationSetting.ProgramID && csr.Status == Model.Entities.CourseRegistrationSetting.StatusActive && csr.Type == courseRegistrationSetting.Type)
							.ToList()
							.ForEach(csr => csr.StatusEnum = Model.Entities.CourseRegistrationSetting.Statuses.Inactive);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return courseRegistrationSetting;
			}
		}

		public static Model.Entities.CourseRegistrationSetting GetCourseRegistrationSetting(int courseRegistrationSettingID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.Programs)
					.SelectMany(p => p.CourseRegistrationSettings)
					.Include(csr => csr.Program)
					.SingleOrDefault(crs => crs.CourseRegistrationSettingID == courseRegistrationSettingID);
			}
		}

		public sealed class CourseRegistrationSetting
		{
			public int CourseRegistrationSettingID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public string InstituteAlias { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string DepartmentAlias { get; set; }
			public byte Type { get; internal set; }
			public string TypeFullName => ((Model.Entities.CourseRegistrationSetting.Types)this.Type).ToString();
			public DateTime FromDate { get; internal set; }
			public DateTime ToDate { get; internal set; }
			public byte Status { get; internal set; }
			public string StatusFullName => ((Model.Entities.CourseRegistrationSetting.Statuses)this.Status).ToString();
		}

		public static List<CourseRegistrationSetting> GetCourseRegistrationSettings(short? semesterID, int? instituteID, int? departmentID, int? programID, Model.Entities.CourseRegistrationSetting.Types? typeEnum, Model.Entities.CourseRegistrationSetting.Statuses? statusEnum, DateTime? fromDate, DateTime? toDate, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var query = aspireContext.GetAdminInstitutes(loginHistory)
					.SelectMany(i => i.Programs)
					.SelectMany(p => p.CourseRegistrationSettings);
				if (semesterID != null)
					query = query.Where(q => q.SemesterID == semesterID);
				if (instituteID != null)
					query = query.Where(q => q.Program.InstituteID == instituteID);
				if (departmentID != null)
					query = query.Where(q => q.Program.DepartmentID == departmentID);
				if (programID != null)
					query = query.Where(q => q.ProgramID == programID);
				if (typeEnum != null)
					query = query.Where(q => q.Type == (byte)typeEnum);
				if (statusEnum != null)
					query = query.Where(q => q.Status == (byte)statusEnum);
				if (fromDate != null)
					query = query.Where(q => fromDate.Value <= q.FromDate);
				if (toDate != null)
					query = query.Where(q => q.ToDate <= toDate.Value);

				switch (sortExpression)
				{
					case nameof(CourseRegistrationSetting.SemesterID):
					case nameof(CourseRegistrationSetting.Semester):
						query = query.OrderBy(sortDirection, q => q.SemesterID);
						break;
					case nameof(CourseRegistrationSetting.InstituteAlias):
						query = query.OrderBy(sortDirection, q => q.Program.Institute.InstituteAlias);
						break;
					case nameof(CourseRegistrationSetting.DepartmentAlias):
						query = query.OrderBy(sortDirection, q => q.Program.Department.DepartmentAlias);
						break;
					case nameof(CourseRegistrationSetting.ProgramAlias):
						query = query.OrderBy(sortDirection, q => q.Program.ProgramAlias);
						break;
					case nameof(CourseRegistrationSetting.Type):
					case nameof(CourseRegistrationSetting.TypeFullName):
						query = query.OrderBy(sortDirection, q => q.Type);
						break;
					case nameof(CourseRegistrationSetting.FromDate):
						query = query.OrderBy(sortDirection, q => q.FromDate);
						break;
					case nameof(CourseRegistrationSetting.ToDate):
						query = query.OrderBy(sortDirection, q => q.ToDate);
						break;
					case nameof(CourseRegistrationSetting.Status):
					case nameof(CourseRegistrationSetting.StatusFullName):
						query = query.OrderBy(sortDirection, q => q.Status);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}

				virtualItemCount = query.Count();
				query = query.Skip(pageIndex * pageSize).Take(pageSize);

				return query.Select(q => new CourseRegistrationSetting
				{
					CourseRegistrationSettingID = q.CourseRegistrationSettingID,
					SemesterID = q.SemesterID,
					InstituteAlias = q.Program.Institute.InstituteAlias,
					ProgramAlias = q.Program.ProgramAlias,
					DepartmentAlias = q.Program.Department.DepartmentAlias,
					Type = q.Type,
					FromDate = q.FromDate,
					ToDate = q.ToDate,
					Status = q.Status
				}).ToList();
			}
		}
	}
}