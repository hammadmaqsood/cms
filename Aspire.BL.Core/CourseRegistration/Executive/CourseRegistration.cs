﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.CourseRegistration.Executive
{
	public static class CourseRegistration
	{
		private static Core.Common.Permissions.Executive.ExecutiveModulePermissions DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.CourseRegistration.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class RegisteredCourse
		{
			internal RegisteredCourse()
			{
			}
			public int RegisteredCourseID { get; internal set; }

			public string RoadmapCourseCode { get; internal set; }
			public string RoadmapTitle { get; internal set; }
			public string RoadmapMajors { get; internal set; }
			public decimal RoadmapCreditHours { get; internal set; }
			public string RoadmapCreditHoursFullName => this.RoadmapCreditHours.ToCreditHoursFullName();

			public short OfferedSemesterID { get; internal set; }
			public string OfferedTitle { get; internal set; }
			public string OfferedProgram { get; internal set; }
			public short OfferedSemesterNo { get; internal set; }
			public int OfferedSection { get; internal set; }
			public byte OfferedShift { get; internal set; }
			public string OfferedClass => AspireFormats.GetClassName(this.OfferedProgram, this.OfferedSemesterNo, this.OfferedSection, this.OfferedShift);

			public string FacultyMemberName { get; internal set; }
			public byte? Status { get; internal set; }
			public byte? FreezedStatus { get; internal set; }
			public string RegisteredCourseStatus => AspireFormats.GetRegisteredCourseStatusString(this.Status, this.FreezedStatus);
			public byte? StudentFeeStatus { get; internal set; }
			public string StudentFeeStatusFullName => ((StudentFee.Statuses?)this.StudentFeeStatus ?? StudentFee.Statuses.NotPaid).ToFullName();
		}

		public sealed class CourseRegistrationInfo
		{
			internal CourseRegistrationInfo()
			{
			}
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public string Name { get; internal set; }
			public string FatherName { get; internal set; }
			public string Program { get; internal set; }
			public short IntakeSemesterID { get; internal set; }
			public byte? Status { get; internal set; }
			public string StatusFullName => (((Model.Entities.Student.Statuses?)this.Status)?.ToFullName()).ToNAIfNullOrEmpty();
			public DateTime? StatusDate { get; internal set; }
			public List<StudentSemester> StudentSemesters { get; internal set; }
			public List<RegisteredCourse> RegisteredCourses { get; internal set; }
		}

		public static CourseRegistrationInfo GetCourseRegistrationInfo(int studentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext
					.DemandModulePermissions(loginSessionGuid)
					.Programs
					.SelectMany(p => p.AdmissionOpenPrograms)
					.SelectMany(aop => aop.Students)
					.Where(s => s.StudentID == studentID)
					.Select(s => new CourseRegistrationInfo
					{
						StudentID = s.StudentID,
						Enrollment = s.Enrollment,
						RegistrationNo = s.RegistrationNo,
						Name = s.Name,
						FatherName = s.FatherName,
						Program = s.AdmissionOpenProgram.Program.ProgramAlias,
						IntakeSemesterID = s.AdmissionOpenProgram.SemesterID,
						Status = s.Status,
						StatusDate = s.StatusDate,
						StudentSemesters = s.StudentSemesters.Where(ss => ss.Status == (byte)StudentSemester.Statuses.Registered).ToList(),
						RegisteredCourses = s.RegisteredCourses.Where(rc => rc.DeletedDate == null).Select(rc => new RegisteredCourse
						{
							FacultyMemberName = rc.OfferedCours.FacultyMember.Name,
							FreezedStatus = rc.FreezedStatus,
							OfferedProgram = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
							OfferedSection = rc.OfferedCours.Section,
							OfferedSemesterNo = rc.OfferedCours.SemesterNo,
							OfferedSemesterID = rc.OfferedCours.SemesterID,
							OfferedShift = rc.OfferedCours.Shift,
							OfferedTitle = rc.OfferedCours.Cours.Title,
							RegisteredCourseID = rc.RegisteredCourseID,
							RoadmapCourseCode = rc.Cours.CourseCode,
							RoadmapTitle = rc.Cours.Title,
							RoadmapCreditHours = rc.Cours.CreditHours,
							RoadmapMajors = rc.Cours.ProgramMajor.Majors,
							Status = rc.Status,
							StudentFeeStatus = rc.StudentFee.Status,
						}).ToList()
					}).SingleOrDefault();
			}
		}
	}
}
