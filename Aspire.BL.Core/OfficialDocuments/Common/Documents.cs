﻿using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System.Linq;

namespace Aspire.BL.Core.OfficialDocuments.Common
{
	public static class Documents
	{
		public static IQueryable<Model.Entities.OfficialDocument> GetOfficialDocuments(this AspireContext aspireContext, int instituteID, UserTypes userTypeEnum, Model.Entities.OfficialDocument.Statuses? statusEnum)
		{
			var officialDocumentsQuery = aspireContext.OfficialDocuments
				.Where(od => od.InstituteID == null || od.InstituteID == instituteID)
				.Where(od => (od.UserTypes & (short)userTypeEnum) == (short)userTypeEnum);
			if (statusEnum != null)
			{
				var status = statusEnum.Value.GetGuid();
				officialDocumentsQuery = officialDocumentsQuery.Where(d => d.Status == status);
			}
			return officialDocumentsQuery.OrderBy(od => od.DocumentName);
		}
	}
}
