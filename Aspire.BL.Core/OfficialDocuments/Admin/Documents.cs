﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FileServer;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.OfficialDocuments.Admin
{
	public static class Documents
	{
		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.OfficialDocuments.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.OfficialDocuments.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Core.Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, AdminPermissions.OfficialDocuments permissions, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, permissions, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, permissions, UserGroupPermission.PermissionValues.Allowed);
		}

		private static BL.Core.Common.Permissions.Admin.AdminLoginHistory DemandManageDocumentPermissions(this AspireContext aspireContext, int? instituteID, UserTypes userTypeEnum, Guid loginSessionGuid)
		{
			if (!ValidUserTypes.Contains(userTypeEnum))
				throw new InvalidOperationException();
			switch (userTypeEnum)
			{
				case UserTypes.Executive:
					return aspireContext.DemandPermissions(AdminPermissions.OfficialDocuments.ManageExecutiveDocuments, instituteID, loginSessionGuid);
				case UserTypes.Staff:
					return aspireContext.DemandPermissions(AdminPermissions.OfficialDocuments.ManageStaffDocuments, instituteID, loginSessionGuid);
				case UserTypes.Student:
					return aspireContext.DemandPermissions(AdminPermissions.OfficialDocuments.ManageStudentDocuments, instituteID, loginSessionGuid);
				case UserTypes.Faculty:
					return aspireContext.DemandPermissions(AdminPermissions.OfficialDocuments.ManageFacultyDocuments, instituteID, loginSessionGuid);
				case UserTypes.Anonymous:
				case UserTypes.System:
				case UserTypes.MasterAdmin:
				case UserTypes.Admin:
				case UserTypes.Candidate:
				case UserTypes.Alumni:
				case UserTypes.IntegratedService:
				case UserTypes.ManualSQL:
				case UserTypes.Admins:
				case UserTypes.Any:
				default:
					throw new NotImplementedEnumException(userTypeEnum);
			}
		}

		public sealed class OfficialDocument
		{
			internal OfficialDocument()
			{
			}

			public int OfficialDocumentID { get; internal set; }
			public string DocumentName { get; internal set; }
			public string ProvidedBy { get; internal set; }
			public DateTime UploadedOn { get; internal set; }
			public int? InstituteID { get; internal set; }
			public string InstituteAlias { get; internal set; }
			public UserTypes UserTypeFlags { get; internal set; }
			public Guid Status { get; internal set; }
		}

		public static (List<OfficialDocument> officialDocuments, int virtualItemsCount) GetOfficialDocuments(int? instituteID, UserTypes? userTypeEnum, Model.Entities.OfficialDocument.Statuses? statusEnum, string searchText, int pageIndex, int pageSize, SortDirection sortDirection, string sortExpression, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				var adminInstituteIDs = aspireContext.GetAdminInstitutes(loginHistory).Select(i => i.InstituteID);
				var officialDocumentsQuery = aspireContext.OfficialDocuments.Where(od => od.InstituteID == null || adminInstituteIDs.Contains(od.InstituteID.Value));
				if (instituteID != null)
					officialDocumentsQuery = officialDocumentsQuery.Where(od => od.InstituteID == null || od.InstituteID == instituteID.Value);
				if (userTypeEnum != null)
					officialDocumentsQuery = officialDocumentsQuery.Where(od => (od.UserTypes & (short)userTypeEnum.Value) == (short)userTypeEnum.Value);
				if (statusEnum != null)
				{
					var status = statusEnum.GetGuid();
					officialDocumentsQuery = officialDocumentsQuery.Where(od => od.Status == status);
				}
				if (!string.IsNullOrWhiteSpace(searchText))
					officialDocumentsQuery = officialDocumentsQuery.Where(od => od.DocumentName.Contains(searchText));
				var virtualItemsCount = officialDocumentsQuery.Count();

				var finalQuery = officialDocumentsQuery.Select(od => new OfficialDocument
				{
					OfficialDocumentID = od.OfficialDocumentID,
					InstituteID = od.InstituteID,
					InstituteAlias = od.Institute.InstituteAlias,
					UserTypeFlags = (UserTypes)od.UserTypes,
					DocumentName = od.DocumentName,
					UploadedOn = od.UploadedOn,
					ProvidedBy = od.ProvidedBy,
					Status = od.Status
				});

				switch (sortExpression)
				{
					case nameof(OfficialDocument.DocumentName):
						finalQuery = finalQuery.OrderBy(sortDirection, od => od.DocumentName);
						break;
					case nameof(OfficialDocument.InstituteAlias):
						finalQuery = finalQuery.OrderBy(sortDirection, od => od.InstituteAlias);
						break;
					case nameof(OfficialDocument.ProvidedBy):
						finalQuery = finalQuery.OrderBy(sortDirection, od => od.ProvidedBy);
						break;
					case nameof(OfficialDocument.UserTypeFlags):
						finalQuery = finalQuery.OrderBy(sortDirection, od => od.UserTypeFlags);
						break;
					case nameof(OfficialDocument.InstituteID):
						finalQuery = finalQuery.OrderBy(sortDirection, od => od.InstituteID);
						break;
					case nameof(OfficialDocument.UploadedOn):
						finalQuery = finalQuery.OrderBy(sortDirection, od => od.UploadedOn);
						break;
					case nameof(OfficialDocument.Status):
						finalQuery = finalQuery.OrderBy(sortDirection, od => od.Status);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}

				return (finalQuery.Skip(pageIndex * pageSize).Take(pageSize).ToList(), virtualItemsCount);
			}
		}

		public enum AddOfficialDocumentStatuses
		{
			FileNotFound,
			Success
		}

		public static readonly UserTypes[] ValidUserTypes = { UserTypes.Executive, UserTypes.Staff, UserTypes.Faculty, UserTypes.Student };

		public static AddOfficialDocumentStatuses AddOfficialDocument(int? instituteID, UserTypes userTypeFlags, string documentName, string providedBy, Guid instanceGuid, Guid temporarySystemFileID, Model.Entities.OfficialDocument.Statuses statusEnum, Guid loginSessionGuid)
		{
			documentName = documentName.TrimAndCannotBeEmpty();
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				var userTypeEnums = userTypeFlags.GetFlags().ToArray();
				if (userTypeEnums.Any() != true)
					throw new ArgumentNullException(nameof(userTypeFlags));
				foreach (var userTypeEnum in userTypeEnums)
					aspireContext.DemandManageDocumentPermissions(instituteID, userTypeEnum, loginSessionGuid);

				var file = aspireContext.ReadTemporarySystemFile(instanceGuid, temporarySystemFileID, loginSessionGuid);
				if (file?.systemFile.SystemFolderID != SystemFolders.DefaultSystemFolderIDs[SystemFolder.FolderTypes.Temporary])
					return AddOfficialDocumentStatuses.FileNotFound;

				var (systemFile, status) = aspireContext.AddOfficialDocument(documentName, file.Value.systemFile.FileExtensionEnum, file.Value.fileBytes, loginSessionGuid);
				switch (status)
				{
					case FileServer.OfficialDocuments.AddOfficialDocumentStatuses.Success:
						aspireContext.OfficialDocuments.Add(new Model.Entities.OfficialDocument
						{
							SystemFileID = systemFile.SystemFileID,
							DocumentName = documentName.TrimAndCannotBeEmpty(),
							InstituteID = instituteID,
							UserTypes = (short)userTypeFlags,
							ProvidedBy = providedBy.TrimAndCannotBeEmpty(),
							UploadedOn = systemFile.CreatedDate,
							StatusEnum = statusEnum
						});
						aspireContext.DeleteTemporarySystemFile(instanceGuid, temporarySystemFileID, loginSessionGuid);
						aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
						return AddOfficialDocumentStatuses.Success;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
		}

		public enum UpdateOfficialDocumentStatuses
		{
			NoRecordFound,
			Success
		}

		public static UpdateOfficialDocumentStatuses UpdateOfficialDocument(int officialDocumentID, UserTypes userTypeFlags, string documentName, string providedBy, Model.Entities.OfficialDocument.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var officialDocument = aspireContext.OfficialDocuments.SingleOrDefault(od => od.OfficialDocumentID == officialDocumentID);
				if (officialDocument == null)
					return UpdateOfficialDocumentStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandModulePermissions(officialDocument.InstituteID, loginSessionGuid);
				var userTypeEnums = ((UserTypes)officialDocument.UserTypes).GetFlags().ToArray();
				if (userTypeEnums.Any() != true)
					throw new ArgumentNullException(nameof(userTypeEnums));
				foreach (var userTypeEnum in userTypeEnums)
					aspireContext.DemandManageDocumentPermissions(officialDocument.InstituteID, userTypeEnum, loginSessionGuid);

				userTypeEnums = userTypeFlags.GetFlags().ToArray();
				if (userTypeEnums.Any() != true)
					throw new ArgumentNullException(nameof(userTypeFlags));
				foreach (var userTypeEnum in userTypeEnums)
					aspireContext.DemandManageDocumentPermissions(officialDocument.InstituteID, userTypeEnum, loginSessionGuid);

				officialDocument.UserTypes = (short)userTypeFlags;
				officialDocument.DocumentName = documentName.TrimAndCannotBeEmpty();
				officialDocument.ProvidedBy = providedBy.TrimAndCannotBeEmpty();
				officialDocument.StatusEnum = statusEnum;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateOfficialDocumentStatuses.Success;
			}
		}

		public enum DeleteOfficialDocumentStatuses
		{
			NoRecordFound,
			Success
		}

		public static DeleteOfficialDocumentStatuses DeleteOfficialDocument(int officialDocumentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var officialDocument = aspireContext.OfficialDocuments.SingleOrDefault(od => od.OfficialDocumentID == officialDocumentID);
				if (officialDocument == null)
					return DeleteOfficialDocumentStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandModulePermissions(officialDocument.InstituteID, loginSessionGuid);
				var userTypeEnums = ((UserTypes)officialDocument.UserTypes).GetFlags().ToArray();
				if (userTypeEnums.Any() != true)
					throw new ArgumentNullException(nameof(userTypeEnums));
				foreach (var userTypeEnum in userTypeEnums)
					aspireContext.DemandManageDocumentPermissions(officialDocument.InstituteID, userTypeEnum, loginSessionGuid);

				aspireContext.DeleteSystemFile(officialDocument.SystemFileID, loginSessionGuid);
				aspireContext.OfficialDocuments.Remove(officialDocument);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteOfficialDocumentStatuses.Success;
			}
		}

		public static Model.Entities.OfficialDocument GetOfficialDocument(int officialDocumentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(false))
			{
				var officialDocument = aspireContext.OfficialDocuments.SingleOrDefault(od => od.OfficialDocumentID == officialDocumentID);
				if (officialDocument == null)
					return null;
				aspireContext.DemandModulePermissions(officialDocument.InstituteID, loginSessionGuid);
				return officialDocument;
			}
		}

		public static (Model.Entities.OfficialDocument OfficialDocument, SystemFile SystemFile, byte[] FileBytes)? ReadOfficialDocument(int officialDocumentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(false))
			{
				var officialDocument = aspireContext.OfficialDocuments.SingleOrDefault(od => od.OfficialDocumentID == officialDocumentID);
				if (officialDocument == null)
					return null;
				aspireContext.DemandModulePermissions(officialDocument.InstituteID, loginSessionGuid);
				var doc = aspireContext.ReadSystemFile(officialDocument.SystemFileID);
				if (doc == null)
					throw new InvalidOperationException("File does not exists.");
				return (officialDocument, doc.Value.systemFile, doc.Value.fileBytes);
			}
		}
	}
}