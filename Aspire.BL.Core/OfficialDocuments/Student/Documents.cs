﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FileServer;
using Aspire.BL.Core.OfficialDocuments.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.OfficialDocuments.Student
{
	public static class Documents
	{
		public static List<OfficialDocument> GetOfficialDocuments(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStudentActiveSession(loginSessionGuid, null);
				var student = aspireContext.Students.Where(s => s.StudentID == loginHistory.StudentID).Select(s => new
				{
					s.AdmissionOpenProgram.Program.InstituteID
				}).Single();
				return aspireContext.GetOfficialDocuments(student.InstituteID, UserTypes.Student, OfficialDocument.Statuses.Active).ToList();
			}
		}

		public static (OfficialDocument OfficaDocument, SystemFile SystemFile, byte[] FileBytes)? ReadOfficialDocuments(int officialDocumentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStudentActiveSession(loginSessionGuid, null);
				var student = aspireContext.Students.Where(s => s.StudentID == loginHistory.StudentID).Select(s => new
				{
					s.AdmissionOpenProgram.Program.InstituteID
				}).Single();
				var officialDocument = aspireContext.GetOfficialDocuments(student.InstituteID, UserTypes.Student, OfficialDocument.Statuses.Active).SingleOrDefault(od => od.OfficialDocumentID == officialDocumentID);
				if (officialDocument == null)
					return null;
				var doc = aspireContext.ReadSystemFile(officialDocument.SystemFileID);
				if (doc == null)
					throw new InvalidOperationException("File does not exists.");
				return (officialDocument, doc.Value.systemFile, doc.Value.fileBytes);
			}
		}
	}
}
