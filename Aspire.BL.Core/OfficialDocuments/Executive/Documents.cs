﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Core.FileServer;
using Aspire.BL.Core.OfficialDocuments.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.OfficialDocuments.Executive
{
	public static class Documents
	{
		public static List<Model.Entities.OfficialDocument> GetOfficialDocuments(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandExecutiveActiveSession(loginSessionGuid);
				//aspireContext.DemandExecutivePermissions(loginSessionGuid, ExecutivePermissions.OfficialDocuments.Module, UserGroupPermission.PermissionValues.Allowed);
				return aspireContext.GetOfficialDocuments(loginHistory.InstituteID, UserTypes.Executive, OfficialDocument.Statuses.Active).ToList();
			}
		}

		public static (OfficialDocument officialDocument, SystemFile SystemFile, byte[] FileBytes)? ReadOfficialDocuments(int officialDocumentID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandExecutiveActiveSession(loginSessionGuid);
				var officialDocument = aspireContext.GetOfficialDocuments(loginHistory.InstituteID, UserTypes.Executive, OfficialDocument.Statuses.Active).SingleOrDefault(od => od.OfficialDocumentID == officialDocumentID);
				if (officialDocument == null)
					return null;
				var doc = aspireContext.ReadSystemFile(officialDocument.SystemFileID);
				if (doc == null)
					throw new InvalidOperationException("File does not exists.");
				return (officialDocument, doc.Value.systemFile, doc.Value.fileBytes);
			}
		}
	}
}
