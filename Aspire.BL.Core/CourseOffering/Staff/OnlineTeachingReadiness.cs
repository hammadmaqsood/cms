﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.CourseOffering.Staff
{
	public static class OnlineTeachingReadiness
	{
		public sealed class OfferedCourse
		{
			internal OfferedCourse()
			{
			}

			public int OfferedCourseID { get; internal set; }
			public int CourseID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Title { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string Teacher { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public bool SubmittedByClusterHead { get; internal set; }
			public bool SubmittedByHOD { get; internal set; }
			public bool SubmittedByDirector { get; internal set; }
			public bool SubmittedByDG { get; internal set; }
			public Guid? ReadinessType { get; internal set; }
		}

		public static (List<OfferedCourse> offeredCourses, int virtualItemCount) GetOfferedCourses(short semesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int? facultyMemberID, Model.Entities.OnlineTeachingReadiness.ReadinessTypes? readinessTypeEnum, bool? submittedByClusterHead, bool? submittedByHOD, bool? submittedByDirector, bool? submittedByDG, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var readinessType = readinessTypeEnum?.GetGuid();
				var offeredCourses = from oc in aspireContext.OfferedCourses
									 join c in aspireContext.Courses on oc.CourseID equals c.CourseID
									 join aop in aspireContext.AdmissionOpenPrograms on c.AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
									 join p in aspireContext.Programs on aop.ProgramID equals p.ProgramID
									 join otr in aspireContext.OnlineTeachingReadinesses on oc.OfferedCourseID equals otr.OfferedCourseID into grp
									 from g in grp.DefaultIfEmpty()
									 where oc.SemesterID == semesterID && p.InstituteID == loginHistory.InstituteID
										&& (departmentID == null || p.DepartmentID == departmentID.Value)
										&& (programID == null || p.ProgramID == programID.Value)
										&& ((short?)semesterNoEnum == null || oc.SemesterNo == (short)semesterNoEnum.Value)
										&& ((int?)sectionEnum == null || oc.Section == (int)sectionEnum.Value)
										&& ((byte?)shiftEnum == null || oc.Shift == (byte)shiftEnum.Value)
										&& (facultyMemberID == null || oc.FacultyMemberID == facultyMemberID.Value)
										&& (readinessType == null || (g != null && g.ReadinessType == readinessType.Value))
									 select new OfferedCourse
									 {
										 OfferedCourseID = oc.OfferedCourseID,
										 CourseID = c.CourseID,
										 Title = c.Title,
										 ProgramAlias = p.ProgramAlias,
										 SemesterID = oc.SemesterID,
										 SemesterNo = oc.SemesterNo,
										 Section = oc.Section,
										 Shift = oc.Shift,
										 Teacher = oc.FacultyMember.Name,
										 ReadinessType = g != null ? g.ReadinessType : (Guid?)null,
										 SubmittedByClusterHead = g != null ? g.SubmittedByClusterHeadDate != null : false,
										 SubmittedByHOD = g != null ? g.SubmittedByHODDate != null : false,
										 SubmittedByDirector = g != null ? g.SubmittedByDirectorDate != null : false,
										 SubmittedByDG = g != null ? g.SubmittedByDGDate != null : false
									 };

				if (submittedByClusterHead != null)
					offeredCourses = offeredCourses.Where(oc => oc.SubmittedByClusterHead == submittedByClusterHead.Value);
				if (submittedByHOD != null)
					offeredCourses = offeredCourses.Where(oc => oc.SubmittedByHOD == submittedByHOD.Value);
				if (submittedByDirector != null)
					offeredCourses = offeredCourses.Where(oc => oc.SubmittedByDirector == submittedByDirector.Value);
				if (submittedByDG != null)
					offeredCourses = offeredCourses.Where(oc => oc.SubmittedByDG == submittedByDG.Value);

				var virtualItemCount = offeredCourses.Count();
				switch (sortExpression)
				{
					case nameof(OfferedCourse.SemesterID):
						offeredCourses = offeredCourses.OrderBy(sortDirection, c => c.SemesterID);
						break;
					case nameof(OfferedCourse.Title):
						offeredCourses = offeredCourses.OrderBy(sortDirection, c => c.Title);
						break;
					case nameof(OfferedCourse.Teacher):
						offeredCourses = offeredCourses.OrderBy(sortDirection, c => c.Teacher);
						break;
					case nameof(OfferedCourse.SubmittedByClusterHead):
						offeredCourses = offeredCourses.OrderBy(sortDirection, c => c.SubmittedByClusterHead);
						break;
					case nameof(OfferedCourse.SubmittedByHOD):
						offeredCourses = offeredCourses.OrderBy(sortDirection, c => c.SubmittedByHOD);
						break;
					case nameof(OfferedCourse.SubmittedByDirector):
						offeredCourses = offeredCourses.OrderBy(sortDirection, c => c.SubmittedByDirector);
						break;
					case nameof(OfferedCourse.SubmittedByDG):
						offeredCourses = offeredCourses.OrderBy(sortDirection, c => c.SubmittedByDG);
						break;
					case nameof(OfferedCourse.ReadinessType):
						offeredCourses = offeredCourses.OrderBy(sortDirection, c => c.ReadinessType);
						break;
					case nameof(OfferedCourse.Class):
						offeredCourses = offeredCourses
							.OrderBy(sortDirection, c => c.ProgramAlias)
							.ThenBy(sortDirection, c => c.SemesterNo)
							.ThenBy(sortDirection, c => c.Section)
							.ThenBy(sortDirection, c => c.Shift);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return (offeredCourses.Skip(pageIndex * pageSize).Take(pageSize).ToList(), virtualItemCount);
			}
		}

		public sealed class OnlineTeachingReadinessDetail
		{
			internal OnlineTeachingReadinessDetail() { }
			public int OfferedCourseID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public string FacultyMemberName { get; internal set; }
			public string FacultyMemberDepartmentAlias { get; internal set; }
			public FacultyMember.FacultyTypes? FacultyTypeEnum { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public decimal ContactHours { get; internal set; }
			public bool Visiting { get; internal set; }
			public string InstituteAlias { get; internal set; }
			public string DepartmentAlias { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public Model.Entities.OnlineTeachingReadiness OnlineTeachingReadiness { get; internal set; }
			public string SubmittedByClusterHeadUserName { get; set; }
			public string SubmittedByHODUserName { get; set; }
			public string SubmittedByDirectorUserName { get; set; }
			public string SubmittedByDGUserName { get; set; }
		}

		public static List<OnlineTeachingReadinessDetail> GetOnlineTeachingReadinessDetails(short semesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int? facultyMemberID, Model.Entities.OnlineTeachingReadiness.ReadinessTypes? readinessTypeEnum, bool? submittedByClusterHead, bool? submittedByHOD, bool? submittedByDirector, bool? submittedByDG, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffActiveSession(loginSessionGuid);
				var readinessType = readinessTypeEnum?.GetGuid();
				var offeredCoursesQuery = from oc in aspireContext.OfferedCourses
										  join c in aspireContext.Courses on oc.CourseID equals c.CourseID
										  join aop in aspireContext.AdmissionOpenPrograms on c.AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
										  join p in aspireContext.Programs on aop.ProgramID equals p.ProgramID
										  join d in aspireContext.Departments on p.DepartmentID equals d.DepartmentID
										  join i in aspireContext.Institutes on p.InstituteID equals i.InstituteID
										  join otr in aspireContext.OnlineTeachingReadinesses on oc.OfferedCourseID equals otr.OfferedCourseID
										  where oc.SemesterID == semesterID && p.InstituteID == loginHistory.InstituteID
											 && (departmentID == null || p.DepartmentID == departmentID.Value)
											 && (programID == null || p.ProgramID == programID.Value)
											 && ((short?)semesterNoEnum == null || oc.SemesterNo == (short)semesterNoEnum.Value)
											 && ((int?)sectionEnum == null || oc.Section == (int)sectionEnum.Value)
											 && ((byte?)shiftEnum == null || oc.Shift == (byte)shiftEnum.Value)
											 && (facultyMemberID == null || oc.FacultyMemberID == facultyMemberID.Value)
											 && (readinessType == null || otr.ReadinessType == readinessType.Value)
											 && (submittedByClusterHead == null || (submittedByClusterHead.Value && otr.SubmittedByClusterHeadDate != null) || (!submittedByClusterHead.Value && otr.SubmittedByClusterHeadDate == null))
											 && (submittedByHOD == null || (submittedByHOD.Value && otr.SubmittedByHODDate != null) || (!submittedByHOD.Value && otr.SubmittedByHODDate == null))
											 && (submittedByDirector == null || (submittedByDirector.Value && otr.SubmittedByDirectorDate != null) || (!submittedByDirector.Value && otr.SubmittedByDirectorDate == null))
											 && (submittedByDG == null || (submittedByDG.Value && otr.SubmittedByDGDate != null) || (!submittedByDG.Value && otr.SubmittedByDGDate == null))
										  select new OnlineTeachingReadinessDetail
										  {
											  OfferedCourseID = oc.OfferedCourseID,
											  Title = c.Title,
											  ProgramAlias = p.ProgramAlias,
											  SemesterID = oc.SemesterID,
											  SemesterNo = oc.SemesterNo,
											  Section = oc.Section,
											  Shift = oc.Shift,
											  FacultyMemberName = oc.FacultyMember.Name,
											  FacultyMemberDepartmentAlias = oc.FacultyMember.Department.DepartmentAlias,
											  CreditHours = c.CreditHours,
											  ContactHours = c.ContactHours,
											  CourseCode = c.CourseCode,
											  DepartmentAlias = d.DepartmentAlias,
											  FacultyTypeEnum = (FacultyMember.FacultyTypes?)oc.FacultyMember.FacultyType,
											  Visiting = oc.Visiting,
											  InstituteAlias = i.InstituteAlias,
											  OnlineTeachingReadiness = otr,
										  };

				var result = offeredCoursesQuery.ToList();
				var userLoginHistoryIDs = result.SelectMany(oc => new[]
				{
					oc.OnlineTeachingReadiness.SubmittedByClusterHeadUserLoginHistoryID,
					oc.OnlineTeachingReadiness.SubmittedByHODUserLoginHistoryID,
					oc.OnlineTeachingReadiness.SubmittedByDirectorUserLoginHistoryID,
					oc.OnlineTeachingReadiness.SubmittedByDGUserLoginHistoryID
				}).Where(u => u != null).Select(u => u.Value).Distinct().ToList();
				var userNames = (from u in aspireContext.Users
								 join h in aspireContext.UserLoginHistories on u.UserID equals h.UserID
								 where userLoginHistoryIDs.Contains(h.UserLoginHistoryID)
								 select new { h.UserLoginHistoryID, u.Name }).ToList();

				var query = from oc in result
							from u in userNames
							where oc.OnlineTeachingReadiness.SubmittedByClusterHeadUserLoginHistoryID == u.UserLoginHistoryID
								|| oc.OnlineTeachingReadiness.SubmittedByHODUserLoginHistoryID == u.UserLoginHistoryID
								|| oc.OnlineTeachingReadiness.SubmittedByDirectorUserLoginHistoryID == u.UserLoginHistoryID
								|| oc.OnlineTeachingReadiness.SubmittedByDGUserLoginHistoryID == u.UserLoginHistoryID
							select new { oc, u };

				query.ForEach(q =>
				{
					if (q.oc.OnlineTeachingReadiness.SubmittedByClusterHeadUserLoginHistoryID == q.u.UserLoginHistoryID)
						q.oc.SubmittedByClusterHeadUserName = q.u.Name;
					if (q.oc.OnlineTeachingReadiness.SubmittedByHODUserLoginHistoryID == q.u.UserLoginHistoryID)
						q.oc.SubmittedByHODUserName = q.u.Name;
					if (q.oc.OnlineTeachingReadiness.SubmittedByDirectorUserLoginHistoryID == q.u.UserLoginHistoryID)
						q.oc.SubmittedByDirectorUserName = q.u.Name;
					if (q.oc.OnlineTeachingReadiness.SubmittedByDGUserLoginHistoryID == q.u.UserLoginHistoryID)
						q.oc.SubmittedByDGUserName = q.u.Name;
				});

				return result;
			}
		}

		public sealed class OfferedCourseDetails
		{
			internal OfferedCourseDetails() { }
			public int OfferedCourseID { get; internal set; }
			public int CourseID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public string FacultyMemberDepartmentAlias { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public DateTime? SubmittedToHOD { get; internal set; }
			public DateTime? SubmittedToCampus { get; internal set; }
			public DateTime? SubmittedToUniversity { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public int InstituteID { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public int ProgramID { get; internal set; }
			public int AdmissionOpenProgramID { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public decimal ContactHours { get; internal set; }
			public bool Visiting { get; internal set; }
			public FacultyMember.FacultyTypes? FacultyTypeEnum { get; internal set; }
			public Model.Entities.OnlineTeachingReadiness OnlineTeachingReadiness { get; internal set; }
			public Dictionary<int, string> SubmittedByUsers { get; internal set; }
		}

		public static OfferedCourseDetails GetOfferedCourse(int offeredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.Module, Model.Entities.UserGroupPermission.PermissionValues.Allowed);
				var offeredCourse = aspireContext.OfferedCourses.Where(oc => oc.OfferedCourseID == offeredCourseID)
					.Select(oc => new OfferedCourseDetails
					{
						OfferedCourseID = oc.OfferedCourseID,
						ProgramAlias = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						InstituteID = oc.Cours.AdmissionOpenProgram.Program.InstituteID,
						DepartmentID = oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
						ProgramID = oc.Cours.AdmissionOpenProgram.ProgramID,
						AdmissionOpenProgramID = oc.Cours.AdmissionOpenProgramID,
						SemesterID = oc.SemesterID,
						SemesterNo = oc.SemesterNo,
						Section = oc.Section,
						Shift = oc.Shift,
						CourseID = oc.CourseID,
						FacultyMemberName = oc.FacultyMember.Name,
						FacultyMemberDepartmentAlias = oc.FacultyMember.Department.DepartmentAlias,
						FacultyTypeEnum = (Model.Entities.FacultyMember.FacultyTypes?)oc.FacultyMember.FacultyType,
						Title = oc.Cours.Title,
						CourseCode = oc.Cours.CourseCode,
						CreditHours = oc.Cours.CreditHours,
						ContactHours = oc.Cours.ContactHours,
						Visiting = oc.Visiting,
						OnlineTeachingReadiness = oc.OnlineTeachingReadinesses.FirstOrDefault()
					}).SingleOrDefault();
				if (offeredCourse == null)
					return null;
				aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanViewOnlineTeachingReadinessForm, Model.Entities.UserGroupPermission.PermissionValues.Allowed, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID);
				if (offeredCourse.OnlineTeachingReadiness != null)
				{
					var userLoginHistoryIDs = new[] {
						offeredCourse.OnlineTeachingReadiness.SubmittedByClusterHeadUserLoginHistoryID,
						offeredCourse.OnlineTeachingReadiness.SubmittedByHODUserLoginHistoryID,
						offeredCourse.OnlineTeachingReadiness.SubmittedByDirectorUserLoginHistoryID,
						offeredCourse.OnlineTeachingReadiness.SubmittedByDGUserLoginHistoryID
					}.Where(u => u != null).ToList();
					if (userLoginHistoryIDs.Any())
					{
						var query = from h in aspireContext.UserLoginHistories
									join r in aspireContext.UserRoles on h.UserRoleID equals r.UserRoleID
									join u in aspireContext.Users on r.UserID equals u.UserID
									where userLoginHistoryIDs.Contains(h.UserLoginHistoryID)
									select new { h.UserLoginHistoryID, u.Name };
						offeredCourse.SubmittedByUsers = query.ToList().ToDictionary(u => u.UserLoginHistoryID, u => u.Name);
					}
				}
				return offeredCourse;
			}
		}

		public enum UpdateOnlineTeachingReadinessStatuses
		{
			NoRecordFound,
			Saved,
			AlreadySubmittedByDG,
			AlreadySubmittedByDirector,
			AlreadySubmittedByClusterHead,
			AlreadySubmittedByHOD,
			MustBeSubmittedByClusterHeadFirst,
			MustBeSubmittedByHODFirst,
			MustBeSubmittedByDirectorFirst,
			UpdateFailedReasonSubmittedByDG,
		}

		public static UpdateOnlineTeachingReadinessStatuses UpdateOnlineTeachingReadiness(Model.Entities.OnlineTeachingReadiness onlineTeachingReadiness, bool submitAsClusterHead, bool submitAsHOD, bool submitAsDirector, bool submitAsDG, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var now = DateTime.Now;
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.Module, Model.Entities.UserGroupPermission.PermissionValues.Allowed);
				var offeredCourse = aspireContext.OfferedCourses.Where(oc => oc.OfferedCourseID == onlineTeachingReadiness.OfferedCourseID)
					.Select(oc => new
					{
						oc.OfferedCourseID,
						oc.Cours.AdmissionOpenProgram.Program.InstituteID,
						oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
						oc.Cours.AdmissionOpenProgram.ProgramID,
						oc.Cours.AdmissionOpenProgramID
					})
					.SingleOrDefault();
				if (offeredCourse == null)
					return UpdateOnlineTeachingReadinessStatuses.NoRecordFound;
				aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanViewOnlineTeachingReadinessForm, UserGroupPermission.PermissionValues.Allowed, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID);

				var onlineTeachingReadinessDB = aspireContext.OnlineTeachingReadinesses.SingleOrDefault(otr => otr.OfferedCourseID == offeredCourse.OfferedCourseID)
					?? aspireContext.OnlineTeachingReadinesses.Add(new Model.Entities.OnlineTeachingReadiness
					{
						OnlineTeachingReadinessID = Guid.NewGuid(),
						OfferedCourseID = offeredCourse.OfferedCourseID
					});

				onlineTeachingReadinessDB.ReadinessTypeEnum = onlineTeachingReadiness.ReadinessTypeEnum;

				var notReady = onlineTeachingReadinessDB.ReadinessTypeEnum == Model.Entities.OnlineTeachingReadiness.ReadinessTypes.NotReady;
				onlineTeachingReadinessDB.NotReadyAlreadyDeliveredFaceToFaceContactHours = notReady ? onlineTeachingReadiness.NotReadyAlreadyDeliveredFaceToFaceContactHours : null;
				onlineTeachingReadinessDB.NotReadyRemainingContactHours = notReady ? onlineTeachingReadiness.NotReadyRemainingContactHours : null;
				onlineTeachingReadinessDB.NotReadyTotalContactHours = notReady ? onlineTeachingReadiness.NotReadyTotalContactHours : null;
				onlineTeachingReadinessDB.NotReadyTheoreticalContactHours = notReady ? onlineTeachingReadiness.NotReadyTheoreticalContactHours : null;
				onlineTeachingReadinessDB.NotReadyLabContactHours = notReady ? onlineTeachingReadiness.NotReadyLabContactHours : null;
				onlineTeachingReadinessDB.NotReadyFieldStudyContactHours = notReady ? onlineTeachingReadiness.NotReadyFieldStudyContactHours : null;
				onlineTeachingReadinessDB.NotReadyOtherContactHours = notReady ? onlineTeachingReadiness.NotReadyOtherContactHours : null;
				onlineTeachingReadinessDB.NotReadyReasons = notReady ? onlineTeachingReadiness.NotReadyReasons : null;

				var partiallyReady = onlineTeachingReadinessDB.ReadinessTypeEnum == Model.Entities.OnlineTeachingReadiness.ReadinessTypes.PartiallyReady;
				onlineTeachingReadinessDB.PartiallyReadyAlreadyDeliveredFaceToFaceContactHours = partiallyReady ? onlineTeachingReadiness.PartiallyReadyAlreadyDeliveredFaceToFaceContactHours : null;
				onlineTeachingReadinessDB.PartiallyReadyToBeDeliveredOnlineContactHours = partiallyReady ? onlineTeachingReadiness.PartiallyReadyToBeDeliveredOnlineContactHours : null;
				onlineTeachingReadinessDB.PartiallyReadyTotalContactHours = partiallyReady ? onlineTeachingReadiness.PartiallyReadyTotalContactHours : null;
				onlineTeachingReadinessDB.PartiallyReadyCoveredOnlineTheoreticalContactHours = partiallyReady ? onlineTeachingReadiness.PartiallyReadyCoveredOnlineTheoreticalContactHours : null;
				onlineTeachingReadinessDB.PartiallyReadyCoveredOnlineLabContactHours = partiallyReady ? onlineTeachingReadiness.PartiallyReadyCoveredOnlineLabContactHours : null;
				onlineTeachingReadinessDB.PartiallyReadyCoveredOnlineFieldStudyContactHours = partiallyReady ? onlineTeachingReadiness.PartiallyReadyCoveredOnlineFieldStudyContactHours : null;
				onlineTeachingReadinessDB.PartiallyReadyCoveredOnlineOtherContactHours = partiallyReady ? onlineTeachingReadiness.PartiallyReadyCoveredOnlineOtherContactHours : null;
				onlineTeachingReadinessDB.PartiallyReadyCanNotCoveredOnlineTheoreticalContactHours = partiallyReady ? onlineTeachingReadiness.PartiallyReadyCanNotCoveredOnlineTheoreticalContactHours : null;
				onlineTeachingReadinessDB.PartiallyReadyCanNotCoveredOnlineLabContactHours = partiallyReady ? onlineTeachingReadiness.PartiallyReadyCanNotCoveredOnlineLabContactHours : null;
				onlineTeachingReadinessDB.PartiallyReadyCanNotCoveredOnlineFieldStudyContactHours = partiallyReady ? onlineTeachingReadiness.PartiallyReadyCanNotCoveredOnlineFieldStudyContactHours : null;
				onlineTeachingReadinessDB.PartiallyReadyCanNotCoveredOnlineOtherContactHours = partiallyReady ? onlineTeachingReadiness.PartiallyReadyCanNotCoveredOnlineOtherContactHours : null;
				onlineTeachingReadinessDB.PartiallyReadyQuestionsList = partiallyReady ? onlineTeachingReadiness.PartiallyReadyQuestionsList : null;
				onlineTeachingReadinessDB.PartiallyReadyRemainingContentsCoveredInSemesterID = partiallyReady ? onlineTeachingReadiness.PartiallyReadyRemainingContentsCoveredInSemesterID : null;
				onlineTeachingReadinessDB.PartiallyReadyReasons = partiallyReady ? onlineTeachingReadiness.PartiallyReadyReasons : null;

				var fullyReady = onlineTeachingReadinessDB.ReadinessTypeEnum == Model.Entities.OnlineTeachingReadiness.ReadinessTypes.FullyReady;
				onlineTeachingReadinessDB.FullyReadyAlreadyDeliveredFaceToFaceContactHours = fullyReady ? onlineTeachingReadiness.FullyReadyAlreadyDeliveredFaceToFaceContactHours : null;
				onlineTeachingReadinessDB.FullyReadyFieldStudyContactHours = fullyReady ? onlineTeachingReadiness.FullyReadyFieldStudyContactHours : null;
				onlineTeachingReadinessDB.FullyReadyLabContactHours = fullyReady ? onlineTeachingReadiness.FullyReadyLabContactHours : null;
				onlineTeachingReadinessDB.FullyReadyOtherContactHours = fullyReady ? onlineTeachingReadiness.FullyReadyOtherContactHours : null;
				onlineTeachingReadinessDB.FullyReadyQuestions = fullyReady ? onlineTeachingReadiness.FullyReadyQuestions : null;
				onlineTeachingReadinessDB.FullyReadyQuestionsList = fullyReady ? onlineTeachingReadiness.FullyReadyQuestionsList : null;
				onlineTeachingReadinessDB.FullyReadyTheoreticalContactHours = fullyReady ? onlineTeachingReadiness.FullyReadyTheoreticalContactHours : null;
				onlineTeachingReadinessDB.FullyReadyToBeDeliveredOnlineContactHours = fullyReady ? onlineTeachingReadiness.FullyReadyToBeDeliveredOnlineContactHours : null;
				onlineTeachingReadinessDB.FullyReadyTotalContactHours = fullyReady ? onlineTeachingReadiness.FullyReadyTotalContactHours : null;

				var submit = new[] { submitAsClusterHead, submitAsHOD, submitAsDirector, submitAsDG }.Count(c => c == true);
				if (submit > 1)
					throw new ArgumentException("Multiple submit not allowed.");

				if (submitAsClusterHead)
				{
					if (onlineTeachingReadinessDB.SubmittedByClusterHeadDate != null)
						return UpdateOnlineTeachingReadinessStatuses.AlreadySubmittedByClusterHead;
					aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanSubmitOnlineTeachingReadinessFormAsClusterHead, UserGroupPermission.PermissionValues.Allowed, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID);
					onlineTeachingReadinessDB.SubmittedByClusterHeadDate = now;
					onlineTeachingReadinessDB.SubmittedByClusterHeadUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				}
				else if (submitAsHOD)
				{
					if (onlineTeachingReadinessDB.SubmittedByHODDate != null)
						return UpdateOnlineTeachingReadinessStatuses.AlreadySubmittedByHOD;
					if (onlineTeachingReadinessDB.SubmittedByClusterHeadDate == null)
						return UpdateOnlineTeachingReadinessStatuses.MustBeSubmittedByClusterHeadFirst;
					aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanSubmitOnlineTeachingReadinessFormAsHOD, UserGroupPermission.PermissionValues.Allowed, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID);
					onlineTeachingReadinessDB.SubmittedByHODDate = now;
					onlineTeachingReadinessDB.SubmittedByHODUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				}
				else if (submitAsDirector)
				{
					if (onlineTeachingReadinessDB.SubmittedByDirectorDate != null)
						return UpdateOnlineTeachingReadinessStatuses.AlreadySubmittedByDirector;
					if (onlineTeachingReadinessDB.SubmittedByHODDate == null)
						return UpdateOnlineTeachingReadinessStatuses.MustBeSubmittedByHODFirst;
					aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanSubmitOnlineTeachingReadinessFormAsDirector, UserGroupPermission.PermissionValues.Allowed, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID);
					onlineTeachingReadinessDB.SubmittedByDirectorDate = now;
					onlineTeachingReadinessDB.SubmittedByDirectorUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				}
				else if (submitAsDG)
				{
					if (onlineTeachingReadinessDB.SubmittedByDGDate != null)
						return UpdateOnlineTeachingReadinessStatuses.AlreadySubmittedByDG;
					if (onlineTeachingReadinessDB.SubmittedByDirectorDate == null)
						return UpdateOnlineTeachingReadinessStatuses.MustBeSubmittedByDirectorFirst;
					if (onlineTeachingReadinessDB.SubmittedByHODDate == null)
						return UpdateOnlineTeachingReadinessStatuses.MustBeSubmittedByHODFirst;
					aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanSubmitOnlineTeachingReadinessFormAsDirector, UserGroupPermission.PermissionValues.Allowed, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID);
					onlineTeachingReadinessDB.SubmittedByDGDate = now;
					onlineTeachingReadinessDB.SubmittedByDGUserLoginHistoryID = loginHistory.UserLoginHistoryID;
				}
				else
				{
					if (onlineTeachingReadinessDB.SubmittedByDGDate != null)
						return UpdateOnlineTeachingReadinessStatuses.UpdateFailedReasonSubmittedByDG;
					if (onlineTeachingReadinessDB.SubmittedByDirectorDate != null)
						aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanSubmitOnlineTeachingReadinessFormAsDG, UserGroupPermission.PermissionValues.Allowed, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID);
					else if (onlineTeachingReadinessDB.SubmittedByHODDate != null)
						aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanSubmitOnlineTeachingReadinessFormAsDirector, UserGroupPermission.PermissionValues.Allowed, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID);
					else if (onlineTeachingReadinessDB.SubmittedByClusterHeadDate != null)
						aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanSubmitOnlineTeachingReadinessFormAsHOD, UserGroupPermission.PermissionValues.Allowed, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID);
					else
						aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanSubmitOnlineTeachingReadinessFormAsClusterHead, UserGroupPermission.PermissionValues.Allowed, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID);
				}

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateOnlineTeachingReadinessStatuses.Saved;
			}
		}

		public static (int updated, int total) SubmitOnlineTeachingReadiness(List<int> offeredCourseIDs, bool submitAsHOD, bool submitAsDirector, bool submitAsDG, Guid loginSessionGuid)
		{
			if (new[] { submitAsHOD, submitAsDirector, submitAsDG }.Count(c => c == true) != 1)
				throw new ArgumentException();
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanViewOnlineTeachingReadinessForm, UserGroupPermission.PermissionValues.Allowed);
				var offeredCoursesQuery = from oc in aspireContext.OfferedCourses
										  join c in aspireContext.Courses on oc.CourseID equals c.CourseID
										  join aop in aspireContext.AdmissionOpenPrograms on c.AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
										  join p in aspireContext.Programs on aop.ProgramID equals p.ProgramID
										  join otr in aspireContext.OnlineTeachingReadinesses on oc.OfferedCourseID equals otr.OfferedCourseID
										  where p.InstituteID == loginHistory.InstituteID && offeredCourseIDs.Contains(oc.OfferedCourseID)
										  select new
										  {
											  OnlineTeachingReadiness = otr,
											  p.InstituteID,
											  p.DepartmentID,
											  p.ProgramID,
											  aop.AdmissionOpenProgramID,
										  };
				var offeredCourses = offeredCoursesQuery.ToList();
				var total = offeredCourses.Count;
				var updated = 0;
				var now = DateTime.Now;

				offeredCoursesQuery.GroupBy(oc => new
				{
					oc.InstituteID,
					oc.DepartmentID,
					oc.ProgramID,
					oc.AdmissionOpenProgramID
				}).ForEach(g =>
				{
					if (submitAsHOD)
						aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanSubmitOnlineTeachingReadinessFormAsHOD, UserGroupPermission.PermissionValues.Allowed, g.Key.InstituteID, g.Key.DepartmentID, g.Key.ProgramID, g.Key.AdmissionOpenProgramID);
					else if (submitAsDirector)
						aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanSubmitOnlineTeachingReadinessFormAsDirector, UserGroupPermission.PermissionValues.Allowed, g.Key.InstituteID, g.Key.DepartmentID, g.Key.ProgramID, g.Key.AdmissionOpenProgramID);
					else if (submitAsDG)
						aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanSubmitOnlineTeachingReadinessFormAsDG, UserGroupPermission.PermissionValues.Allowed, g.Key.InstituteID, g.Key.DepartmentID, g.Key.ProgramID, g.Key.AdmissionOpenProgramID);
					else
						throw new InvalidOperationException();
					g.Select(gg => gg.OnlineTeachingReadiness).ForEach(otr =>
					{
						if (submitAsHOD)
						{
							if (otr.SubmittedByClusterHeadDate != null && otr.SubmittedByHODDate == null && otr.SubmittedByDirectorDate == null && otr.SubmittedByDGDate == null)
							{
								otr.SubmittedByHODDate = now;
								otr.SubmittedByHODUserLoginHistoryID = loginHistory.UserLoginHistoryID;
								updated++;
							}
						}
						else if (submitAsDirector)
						{
							if (otr.SubmittedByClusterHeadDate != null && otr.SubmittedByHODDate != null && otr.SubmittedByDirectorDate == null && otr.SubmittedByDGDate == null)
							{
								otr.SubmittedByDirectorDate = now;
								otr.SubmittedByDirectorUserLoginHistoryID = loginHistory.UserLoginHistoryID;
								updated++;
							}
						}
						else if (submitAsDG)
						{
							if (otr.SubmittedByClusterHeadDate != null && otr.SubmittedByHODDate != null && otr.SubmittedByDirectorDate != null && otr.SubmittedByDGDate == null)
							{
								otr.SubmittedByDGDate = now;
								otr.SubmittedByDGUserLoginHistoryID = loginHistory.UserLoginHistoryID;
								updated++;
							}
						}
						else
							throw new InvalidOperationException();
					});
				});

				if (updated > 0)
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return (updated, total);
			}
		}

		public static (int updated, int total) UndoSubmitOnlineTeachingReadiness(List<int> offeredCourseIDs, bool undoSubmitAsHOD, bool undoSubmitAsDirector, bool undoSubmitAsDG, Guid loginSessionGuid)
		{
			if (new[] { undoSubmitAsHOD, undoSubmitAsDirector, undoSubmitAsDG }.Count(c => c == true) != 1)
				throw new ArgumentException();
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanViewOnlineTeachingReadinessForm, UserGroupPermission.PermissionValues.Allowed);
				var offeredCoursesQuery = from oc in aspireContext.OfferedCourses
										  join c in aspireContext.Courses on oc.CourseID equals c.CourseID
										  join aop in aspireContext.AdmissionOpenPrograms on c.AdmissionOpenProgramID equals aop.AdmissionOpenProgramID
										  join p in aspireContext.Programs on aop.ProgramID equals p.ProgramID
										  join otr in aspireContext.OnlineTeachingReadinesses on oc.OfferedCourseID equals otr.OfferedCourseID
										  where p.InstituteID == loginHistory.InstituteID && offeredCourseIDs.Contains(oc.OfferedCourseID)
										  select new
										  {
											  OnlineTeachingReadiness = otr,
											  p.InstituteID,
											  p.DepartmentID,
											  p.ProgramID,
											  aop.AdmissionOpenProgramID,
										  };
				var offeredCourses = offeredCoursesQuery.ToList();
				var total = offeredCourses.Count;
				var updated = 0;
				var now = DateTime.Now;

				offeredCoursesQuery.GroupBy(oc => new
				{
					oc.InstituteID,
					oc.DepartmentID,
					oc.ProgramID,
					oc.AdmissionOpenProgramID
				}).ForEach(g =>
				{
					if (undoSubmitAsHOD)
						aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanUndoSubmitOnlineTeachingReadinessFormAsHOD, UserGroupPermission.PermissionValues.Allowed, g.Key.InstituteID, g.Key.DepartmentID, g.Key.ProgramID, g.Key.AdmissionOpenProgramID);
					else if (undoSubmitAsDirector)
						aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanUndoSubmitOnlineTeachingReadinessFormAsDirector, UserGroupPermission.PermissionValues.Allowed, g.Key.InstituteID, g.Key.DepartmentID, g.Key.ProgramID, g.Key.AdmissionOpenProgramID);
					else if (undoSubmitAsDG)
						aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanUndoSubmitOnlineTeachingReadinessFormAsDG, UserGroupPermission.PermissionValues.Allowed, g.Key.InstituteID, g.Key.DepartmentID, g.Key.ProgramID, g.Key.AdmissionOpenProgramID);
					else
						throw new InvalidOperationException();
					g.Select(gg => gg.OnlineTeachingReadiness).ForEach(otr =>
					{
						if (undoSubmitAsHOD)
						{
							if (otr.SubmittedByHODDate != null && otr.SubmittedByDirectorDate == null && otr.SubmittedByDGDate == null)
							{
								otr.SubmittedByHODDate = null;
								otr.SubmittedByHODUserLoginHistoryID = null;
								updated++;
							}
						}
						else if (undoSubmitAsDirector)
						{
							if (otr.SubmittedByDirectorDate != null && otr.SubmittedByDGDate == null)
							{
								otr.SubmittedByDirectorDate = null;
								otr.SubmittedByDirectorUserLoginHistoryID = null;
								updated++;
							}
						}
						else if (undoSubmitAsDG)
						{
							if (otr.SubmittedByDGDate != null)
							{
								otr.SubmittedByDGDate = null;
								otr.SubmittedByDGUserLoginHistoryID = null;
								updated++;
							}
						}
						else
							throw new InvalidOperationException();
					});
				});

				if (updated > 0)
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return (updated, total);
			}
		}
	}
}