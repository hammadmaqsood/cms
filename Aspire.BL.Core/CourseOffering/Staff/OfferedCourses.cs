﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.CourseOffering.Staff
{
	public static class OfferedCourses
	{
		private static Common.Permissions.Staff.StaffGroupPermission DemandPermissions(this AspireContext aspireContext, StaffPermissions.CourseOffering courseOffering, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, courseOffering, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandPermissions(StaffPermissions.CourseOffering.Module, loginSessionGuid);
		}

		public sealed class Course
		{
			internal Course()
			{
			}

			public int CourseID { get; internal set; }
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public short SemesterNo { get; internal set; }
			public SemesterNos SemesterNoEnum => (SemesterNos)this.SemesterNo;
			public string Majors { get; internal set; }
			public Guid Status { get; internal set; }
		}

		public static List<Course> GetRoadmapCoursesForOffering(int admissionOpenProgramID, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var courses = aspireContext.Courses.Where(c => c.AdmissionOpenProgramID == admissionOpenProgramID && c.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				switch (sortExpression)
				{
					case nameof(Cours.CourseCode):
						courses = sortDirection == SortDirection.Ascending ? courses.OrderBy(c => c.CourseCode) : courses.OrderByDescending(c => c.CourseCode);
						break;
					case nameof(Cours.Title):
						courses = sortDirection == SortDirection.Ascending ? courses.OrderBy(c => c.Title) : courses.OrderByDescending(c => c.Title);
						break;
					case nameof(Cours.CreditHours):
						courses = sortDirection == SortDirection.Ascending ? courses.OrderBy(c => c.CreditHours) : courses.OrderByDescending(c => c.CreditHours);
						break;
					case nameof(Cours.SemesterNo):
						courses = sortDirection == SortDirection.Ascending ? courses.OrderBy(c => c.SemesterNo) : courses.OrderByDescending(c => c.SemesterNo);
						break;
					case nameof(ProgramMajor.Majors):
						courses = sortDirection == SortDirection.Ascending ? courses.OrderBy(c => c.ProgramMajor.Majors) : courses.OrderByDescending(c => c.ProgramMajor.Majors);
						break;
					case nameof(Cours.StatusEnum):
						courses = sortDirection == SortDirection.Ascending ? courses.OrderBy(c => c.Status) : courses.OrderByDescending(c => c.Status);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return courses.Select(c => new Course
				{
					CourseID = c.CourseID,
					CourseCode = c.CourseCode,
					Title = c.Title,
					CreditHours = c.CreditHours,
					SemesterNo = c.SemesterNo,
					Majors = c.ProgramMajor.Majors,
					Status = c.Status
				}).ToList();
			}
		}

		public sealed class OfferedCourse
		{
			internal OfferedCourse()
			{
			}

			public int OfferedCourseID { get; internal set; }
			public int CourseID { get; internal set; }
			public int RegisteredStrength { get; internal set; }
			public short SemesterID { get; internal set; }
			public bool SpecialOffered { get; internal set; }
			public bool VisitingCourse { get; internal set; }
			public byte MaxClassStrength { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public SemesterNos SemesterNoEnum { get; internal set; }
			public Sections SectionEnum { get; internal set; }
			public Shifts ShiftEnum { get; internal set; }
			public OfferedCours.Statuses StatusEnum { get; internal set; }
			public string Remarks { get; internal set; }
			public int? FacultyMemberID { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public string Major { get; internal set; }
			public string Program { get; internal set; }
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public short CourseSemesterID { get; internal set; }
			public bool AttendanceRecordExists { get; internal set; }
		}

		public static OfferedCourse GetOfferedCourse(int offeredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var coursesOffered = aspireContext.OfferedCourses.Where(oc => oc.OfferedCourseID == offeredCourseID && oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);

				return coursesOffered.Select(co => new OfferedCourse
				{
					OfferedCourseID = co.OfferedCourseID,
					SemesterID = co.SemesterID,
					CourseCode = co.Cours.CourseCode,
					Title = co.Cours.Title,
					CreditHours = co.Cours.CreditHours,
					Major = co.Cours.ProgramMajor.Majors,
					SemesterNoEnum = (SemesterNos)co.SemesterNo,
					ShiftEnum = (Shifts)co.Shift,
					SectionEnum = (Sections)co.Section,
					MaxClassStrength = co.MaxClassStrength,
					SpecialOffered = co.SpecialOffered,
					VisitingCourse = co.Visiting,
					Remarks = co.Remarks,
					StatusEnum = (OfferedCours.Statuses)co.Status,
					Program = co.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					CourseSemesterID = co.Cours.AdmissionOpenProgram.SemesterID,
					FacultyMemberName = co.FacultyMember.Name,
					FacultyMemberID = co.FacultyMemberID,
					RegisteredStrength = co.RegisteredCourses.Count(rc => rc.DeletedDate == null),
					CourseID = co.CourseID,
					AttendanceRecordExists = co.OfferedCourseAttendances.Any()
				}).SingleOrDefault();
			}
		}

		public sealed class OfferCourseStatus
		{
			internal OfferCourseStatus()
			{
			}

			public string Title { get; internal set; }
			public int Added { get; internal set; }
			public int AlreadyExists { get; internal set; }
		}

		private static bool IsCourseAlreadyOffered(this AspireContext aspireContext, short offeredSemesterID, int courseID, SemesterNos semesterNoEnum, Sections sectionEnum, Shifts shiftEnum, int? exceptOfferedCourseID)
		{
			var query = aspireContext.OfferedCourses
				.Where(oc => oc.SemesterID == offeredSemesterID && oc.CourseID == courseID)
				.Where(oc => (oc.SemesterNo == (short)semesterNoEnum && oc.Section == (int)sectionEnum && oc.Shift == (byte)shiftEnum)
							|| oc.OfferedCourseClasses.Any(occ => occ.SemesterNo == (short)semesterNoEnum && occ.Section == (int)sectionEnum && occ.Shift == (byte)shiftEnum));
			return exceptOfferedCourseID != null
				? query.Any(oc => oc.OfferedCourseID != exceptOfferedCourseID.Value)
				: query.Any();
		}

		public static List<OfferCourseStatus> OfferCourses(List<int> courseIDs, short offeredSemesterID, SemesterNos semesterNoEnum, Sections sectionsEnum, Shifts shiftsEnum, bool visiting, bool specialOffered, byte maxClassStrength, OfferedCours.Statuses statusEnum, string remarks, Guid loginSessionGuid)
		{
			if (semesterNoEnum.GetFlags().Count() > 1)
				throw new ArgumentException(@"Only one value of SemesterNo is allowed.", nameof(semesterNoEnum));
			using (var aspireContext = new AspireContext(true))
			{
				var result = new List<OfferCourseStatus>();
				var courses = aspireContext.Courses.Where(c => courseIDs.Contains(c.CourseID))
					.Select(c => new
					{
						c.CourseID,
						c.Title,
						c.AdmissionOpenProgram.Program.InstituteID,
						c.AdmissionOpenProgram.Program.DepartmentID,
						c.AdmissionOpenProgram.ProgramID,
						c.AdmissionOpenProgramID,
					}).ToList();

				foreach (var course in courses)
				{
					var staffGroupPermission = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.ManageOfferedCourses, UserGroupPermission.PermissionValues.Allowed, course.InstituteID, course.DepartmentID, course.ProgramID, course.AdmissionOpenProgramID);

					var status = new OfferCourseStatus
					{
						Title = course.Title,
						Added = 0,
						AlreadyExists = 0,
					};
					result.Add(status);
					foreach (var sectionEnum in sectionsEnum.GetFlags())
						foreach (var shiftEnum in shiftsEnum.GetFlags())
						{
							if (aspireContext.IsCourseAlreadyOffered(offeredSemesterID, course.CourseID, semesterNoEnum, sectionEnum, shiftEnum, null))
							{
								status.AlreadyExists++;
								continue;
							}
							var offeredCours = new OfferedCours
							{
								CourseID = course.CourseID,
								SemesterID = offeredSemesterID,
								SemesterNoEnum = semesterNoEnum,
								SectionEnum = sectionEnum,
								ShiftEnum = shiftEnum,
								MaxClassStrength = maxClassStrength,
								SpecialOffered = specialOffered,
								StatusEnum = statusEnum,
								Visiting = visiting,
								Remarks = remarks,
								FacultyMemberID = null,
								AssignmentsMarksSubmitted = false,
								QuizzesMarksSubmitted = false,
								InternalsMarksSubmitted = false,
								MidMarksSubmitted = false,
								FinalMarksSubmitted = false,
								MarksEntryLocked = false,
								AssignmentsExamCompilationType = null,
								QuizzesExamCompilationType = null,
								InternalsExamCompilationType = null,
								AssignmentsMarksSubmittedDate = null,
								QuizzesMarksSubmittedDate = null,
								InternalsMarksSubmittedDate = null,
								MidMarksSubmittedDate = null,
								FinalMarksSubmittedDate = null,
								MarksEntryLockedDate = null,
							};
							aspireContext.OfferedCourses.Add(offeredCours);
							aspireContext.SaveChanges(staffGroupPermission.UserLoginHistoryID);
							status.Added = status.Added + 1;
						}
				}
				aspireContext.CommitTransaction();
				return result;
			}
		}

		public enum UpdateOfferedCourseStatuses
		{
			NoRecordFound,
			Success,
			MarksEntryLocked,
			AlreadyOffered,
			MaxClassStrengthCannotBeLessThanRegisteredStudents
		}

		public static UpdateOfferedCourseStatuses UpdateOfferedCourse(int offeredCourseID, SemesterNos semesterNoEnum, Sections sectionEnum, Shifts shiftEnum, bool specialOffered, bool visitingCourse, OfferedCours.Statuses statusEnum, byte maxClassStrength, string remarks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var oc = aspireContext.OfferedCourses
					.Where(c => c.OfferedCourseID == offeredCourseID)
					.Select(c => new
					{
						OfferedCourse = c,
						c.Cours.AdmissionOpenProgram.Program.InstituteID,
						c.Cours.AdmissionOpenProgram.Program.DepartmentID,
						c.Cours.AdmissionOpenProgram.ProgramID,
						c.Cours.AdmissionOpenProgramID,
						RegisteredStudents = c.RegisteredCourses.Count(rc => rc.DeletedDate == null)
					}).SingleOrDefault();
				if (oc == null)
					return UpdateOfferedCourseStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.ManageOfferedCourses, UserGroupPermission.PermissionValues.Allowed, oc.InstituteID, oc.DepartmentID, oc.ProgramID, oc.AdmissionOpenProgramID);
				if (oc.OfferedCourse.MarksEntryLocked)
					return UpdateOfferedCourseStatuses.MarksEntryLocked;
				if (oc.RegisteredStudents > maxClassStrength)
					return UpdateOfferedCourseStatuses.MaxClassStrengthCannotBeLessThanRegisteredStudents;

				if (aspireContext.IsCourseAlreadyOffered(oc.OfferedCourse.SemesterID, oc.OfferedCourse.CourseID, semesterNoEnum, sectionEnum, shiftEnum, oc.OfferedCourse.OfferedCourseID))
					return UpdateOfferedCourseStatuses.AlreadyOffered;

				oc.OfferedCourse.SemesterNoEnum = semesterNoEnum;
				oc.OfferedCourse.SectionEnum = sectionEnum;
				oc.OfferedCourse.ShiftEnum = shiftEnum;
				oc.OfferedCourse.SpecialOffered = specialOffered;
				oc.OfferedCourse.Visiting = visitingCourse;
				oc.OfferedCourse.StatusEnum = statusEnum;
				oc.OfferedCourse.MaxClassStrength = maxClassStrength;
				oc.OfferedCourse.Remarks = remarks;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				return UpdateOfferedCourseStatuses.Success;
			}
		}

		public enum DeleteOfferedCourseStatuses
		{
			NoRecordFound,
			Success,
			CannotDeleteChildRecordExistsRegisteredCourses,
			CannotDeleteChildRecordExistsExamDateSheets,
			CannotDeleteChildRecordExistsAttendance,
			CannotDeleteChildRecordExistsOfferedCourseExams,
			CannotDeleteChildRecordExistsRequestedCourses,
			CannotDeleteChildRecordExistsExamsMarksSubmitted
		}

		public static DeleteOfferedCourseStatuses DeleteOfferedCourse(int offeredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var offeredCourse = aspireContext.OfferedCourses.FirstOrDefault(c => c.OfferedCourseID == offeredCourseID);
				if (offeredCourse == null)
					return DeleteOfferedCourseStatuses.NoRecordFound;

				var oc = aspireContext.OfferedCourses.Where(c => c.OfferedCourseID == offeredCourseID).Select(c => new
				{
					c.Cours.AdmissionOpenProgram.Program.InstituteID,
					c.Cours.AdmissionOpenProgram.Program.DepartmentID,
					c.Cours.AdmissionOpenProgram.ProgramID,
					c.Cours.AdmissionOpenProgramID,
				}).Single();
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.ManageOfferedCourses, UserGroupPermission.PermissionValues.Allowed, oc.InstituteID, oc.DepartmentID, oc.ProgramID, oc.AdmissionOpenProgramID);

				if (offeredCourse.AssignmentsMarksSubmitted || offeredCourse.FinalMarksSubmitted || offeredCourse.MarksEntryLocked || offeredCourse.MidMarksSubmitted || offeredCourse.QuizzesMarksSubmitted)
					return DeleteOfferedCourseStatuses.CannotDeleteChildRecordExistsExamsMarksSubmitted;
				if (offeredCourse.ExamDateSheets.Any())
					return DeleteOfferedCourseStatuses.CannotDeleteChildRecordExistsExamDateSheets;
				if (offeredCourse.OfferedCourseAttendances.Any())
					return DeleteOfferedCourseStatuses.CannotDeleteChildRecordExistsAttendance;
				if (offeredCourse.OfferedCourseExams.Any())
					return DeleteOfferedCourseStatuses.CannotDeleteChildRecordExistsOfferedCourseExams;
				if (offeredCourse.RequestedCourses.Any())
					return DeleteOfferedCourseStatuses.CannotDeleteChildRecordExistsRequestedCourses;
				if (offeredCourse.RegisteredCourses.Any(rc => rc.DeletedDate == null))
					return DeleteOfferedCourseStatuses.CannotDeleteChildRecordExistsRegisteredCourses;

				aspireContext.RegisteredCourses.RemoveRange(offeredCourse.RegisteredCourses.Where(rc => rc.DeletedDate != null));
				aspireContext.OfferedCourseClasses.RemoveRange(aspireContext.OfferedCourseClasses.Where(occ => occ.OfferedCourseID == offeredCourse.OfferedCourseID));
				aspireContext.OfferedCourses.Remove(offeredCourse);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteOfferedCourseStatuses.Success;
			}
		}

		public enum AssignFacultyMemberStatuses
		{
			NoRecordFound,
			MarksEntryLocked,
			Success
		}

		public static AssignFacultyMemberStatuses AssignFacultyMember(int offeredCourseID, int? facultyMemberID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var offeredCourse = aspireContext.OfferedCourses.FirstOrDefault(c => c.OfferedCourseID == offeredCourseID);
				if (offeredCourse == null)
					return AssignFacultyMemberStatuses.NoRecordFound;

				var oc = aspireContext.OfferedCourses.Where(c => c.OfferedCourseID == offeredCourseID).Select(c => new
				{
					c.Cours.AdmissionOpenProgram.Program.InstituteID,
					c.Cours.AdmissionOpenProgram.Program.DepartmentID,
					c.Cours.AdmissionOpenProgram.ProgramID,
					c.Cours.AdmissionOpenProgramID,
				}).Single();

				var loginHistory = aspireContext.TryDemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.ManageOfferedCourses, UserGroupPermission.PermissionValues.Allowed, oc.InstituteID, oc.DepartmentID, oc.ProgramID, oc.AdmissionOpenProgramID);
				if (loginHistory == null)
					loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.AssignTeacher, UserGroupPermission.PermissionValues.Allowed, oc.InstituteID, oc.DepartmentID, oc.ProgramID, oc.AdmissionOpenProgramID);

				if (offeredCourse.MarksEntryLocked)
					return AssignFacultyMemberStatuses.MarksEntryLocked;
				offeredCourse.FacultyMemberID = facultyMemberID;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AssignFacultyMemberStatuses.Success;
			}
		}

		public static List<OfferedCourseClass> GetOfferedCourseClasses(int offeredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.Module, UserGroupPermission.PermissionValues.Allowed);
				return aspireContext.OfferedCourseClasses.Where(oc => oc.OfferedCourseID == offeredCourseID && oc.OfferedCours.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID).ToList();
			}
		}

		public enum AddOfferedCourseClassStatuses
		{
			NoRecordFound,
			AlreadyPrimaryClass,
			ClassAlreadyExists,
			CourseAlreadyOfferedToClass,
			Success
		}

		public static AddOfferedCourseClassStatuses AddOfferedCourseClass(int offeredCourseID, SemesterNos semesterNoEnum, Sections sectionEnum, Shifts shiftEnum, bool special, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var offeredCourse = aspireContext.OfferedCourses.Where(oc => oc.OfferedCourseID == offeredCourseID)
					.Select(oc => new
					{
						oc.Cours.AdmissionOpenProgram.Program.InstituteID,
						oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
						oc.Cours.AdmissionOpenProgram.ProgramID,
						oc.Cours.AdmissionOpenProgramID,
						oc.OfferedCourseID,
						oc.SemesterID,
						oc.CourseID,
						oc.SemesterNo,
						oc.Section,
						oc.Shift
					}).SingleOrDefault();
				if (offeredCourse == null)
					return AddOfferedCourseClassStatuses.NoRecordFound;

				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.ManageOfferedCourses, UserGroupPermission.PermissionValues.Allowed, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID);

				if (offeredCourse.SemesterNo == (short)semesterNoEnum && offeredCourse.Section == (int)sectionEnum && offeredCourse.Shift == (byte)shiftEnum)
					return AddOfferedCourseClassStatuses.AlreadyPrimaryClass;

				if (aspireContext.IsCourseAlreadyOffered(offeredCourse.SemesterID, offeredCourse.CourseID, semesterNoEnum, sectionEnum, shiftEnum, null))
					return AddOfferedCourseClassStatuses.CourseAlreadyOfferedToClass;

				aspireContext.OfferedCourseClasses.Add(new OfferedCourseClass
				{
					OfferedCourseID = offeredCourse.OfferedCourseID,
					SemesterNoEnum = semesterNoEnum,
					SectionEnum = sectionEnum,
					ShiftEnum = shiftEnum,
					SpecialOffered = special
				});
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
			}
			return AddOfferedCourseClassStatuses.Success;
		}

		public enum DeleteOfferedCourseClassStatuses
		{
			NoRecordFound,
			MarksEntryLocked,
			Success
		}

		public static DeleteOfferedCourseClassStatuses DeleteOfferedCourseClass(int offeredCourseClassID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var offeredCourseClass = aspireContext.OfferedCourseClasses.Include(occ => occ.OfferedCours).SingleOrDefault(occ => occ.OfferedCourseClassID == offeredCourseClassID);
				if (offeredCourseClass == null)
					return DeleteOfferedCourseClassStatuses.NoRecordFound;

				var oc = aspireContext.OfferedCourses.Where(c => c.OfferedCourseID == offeredCourseClass.OfferedCourseID).Select(c => new
				{
					c.Cours.AdmissionOpenProgram.Program.InstituteID,
					c.Cours.AdmissionOpenProgram.Program.DepartmentID,
					c.Cours.AdmissionOpenProgram.ProgramID,
					c.Cours.AdmissionOpenProgramID,
				}).Single();
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.ManageOfferedCourses, UserGroupPermission.PermissionValues.Allowed, oc.InstituteID, oc.DepartmentID, oc.ProgramID, oc.AdmissionOpenProgramID);

				if (offeredCourseClass.OfferedCours.MarksEntryLocked)
					return DeleteOfferedCourseClassStatuses.MarksEntryLocked;
				aspireContext.OfferedCourseClasses.Remove(offeredCourseClass);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteOfferedCourseClassStatuses.Success;
			}
		}

		public sealed class OfferedCourseForGrid
		{
			internal OfferedCourseForGrid()
			{
			}

			public int OfferedCourseID { get; internal set; }
			public int RegisteredStrength { get; internal set; }
			public bool SpecialOffered { get; internal set; }
			public bool Visiting { get; internal set; }
			public byte MaxClassStrength { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public SemesterNos SemesterNoEnum { get; internal set; }
			public Sections SectionEnum { get; internal set; }
			public Shifts ShiftEnum { get; internal set; }
			public OfferedCours.Statuses StatusEnum { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public string Major { get; internal set; }
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public short CourseSemesterID { get; internal set; }
			public List<OfferedCourseClass> OfferedCourseClasses { get; set; }
			public string Remarks { get; internal set; }
			public bool MarksEntryLocked { get; internal set; }
			public Guid Status { get; internal set; }
			public string PrimaryClassName => AspireFormats.GetClassName(this.SemesterNoEnum, this.SectionEnum, this.ShiftEnum);
			public IEnumerable<string> SecondaryClassNames => this.OfferedCourseClasses.Select(c => c.ClassName);

			public sealed class OfferedCourseClass
			{
				internal OfferedCourseClass()
				{
				}

				public SemesterNos SemesterNoEnum { get; internal set; }
				public Sections SectionEnum { get; internal set; }
				public Shifts ShiftEnum { get; internal set; }
				public string ClassName => AspireFormats.GetClassName(this.SemesterNoEnum, this.SectionEnum, this.ShiftEnum);
			}
		}

		public static List<OfferedCourseForGrid> GetOfferedCourses(short semesterID, int programID, SemesterNos? semesterNoEnum, Sections? sectionFlags, Shifts? shiftFlags, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.Module, UserGroupPermission.PermissionValues.Allowed);
				var semesterNo = (short?)semesterNoEnum;
				var sections = sectionFlags?.GetFlags().Cast<int>().ToList();
				var shifts = shiftFlags?.GetFlags().Cast<byte>().ToList();
				var offeredCourses = aspireContext.OfferedCourses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				offeredCourses = offeredCourses.Where(oc => oc.SemesterID == semesterID);
				offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.ProgramID == programID);
				if (semesterNo != null)
					offeredCourses = offeredCourses.Where(oc => oc.SemesterNo == semesterNo.Value);
				if (sections?.Any() == true)
					offeredCourses = offeredCourses.Where(oc => sections.Contains(oc.Section));
				if (shifts?.Any() == true)
					offeredCourses = offeredCourses.Where(oc => shifts.Contains(oc.Shift));
				var result = offeredCourses.Select(oc => new OfferedCourseForGrid
				{
					OfferedCourseID = oc.OfferedCourseID,
					CourseSemesterID = oc.Cours.AdmissionOpenProgram.SemesterID,
					CourseCode = oc.Cours.CourseCode,
					Title = oc.Cours.Title,
					CreditHours = oc.Cours.CreditHours,
					Major = oc.Cours.ProgramMajor.Majors,
					SemesterNoEnum = (SemesterNos)oc.SemesterNo,
					SectionEnum = (Sections)oc.Section,
					ShiftEnum = (Shifts)oc.Shift,
					FacultyMemberName = oc.FacultyMember.Name,
					MaxClassStrength = oc.MaxClassStrength,
					RegisteredStrength = oc.RegisteredCourses.Count(rc => rc.DeletedDate == null),
					SpecialOffered = oc.SpecialOffered,
					Visiting = oc.Visiting,
					Remarks = oc.Remarks,
					StatusEnum = (OfferedCours.Statuses)oc.Status,
					MarksEntryLocked = oc.MarksEntryLocked,
					Status = oc.Cours.Status,
					OfferedCourseClasses = oc.OfferedCourseClasses.Select(occ => new OfferedCourseForGrid.OfferedCourseClass
					{
						SemesterNoEnum = (SemesterNos)occ.SemesterNo,
						SectionEnum = (Sections)occ.Section,
						ShiftEnum = (Shifts)occ.Shift
					}).ToList()
				}).ToList();
				switch (sortExpression)
				{
					case nameof(OfferedCourseForGrid.Title):
						return result.OrderBy(sortDirection, oc => oc.Title).ToList();
					case nameof(OfferedCourseForGrid.CreditHours):
						return result.OrderBy(sortDirection, oc => oc.CreditHours).ToList();
					case nameof(OfferedCourseForGrid.PrimaryClassName):
						return result.OrderBy(sortDirection, oc => oc.PrimaryClassName).ToList();
					case nameof(OfferedCourseForGrid.FacultyMemberName):
						return result.OrderBy(sortDirection, oc => oc.FacultyMemberName).ToList();
					case nameof(OfferedCourseForGrid.MaxClassStrength):
						return result.OrderBy(sortDirection, oc => oc.MaxClassStrength).ToList();
					case nameof(OfferedCourseForGrid.RegisteredStrength):
						return result.OrderBy(sortDirection, oc => oc.RegisteredStrength).ToList();
					case nameof(OfferedCourseForGrid.Remarks):
						return result.OrderBy(sortDirection, oc => oc.Remarks).ToList();
					case nameof(OfferedCourseForGrid.Major):
						return result.OrderBy(sortDirection, oc => oc.Major).ToList();
					case nameof(OfferedCourseForGrid.Status):
						return result.OrderBy(sortDirection, oc => oc.Status).ToList();
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
			}
		}

		public static (List<CustomListItem> admissionOpenPrograms, string programAlias, int? selectedAdmissionOpenProgramID) GetAdmissionOpenPrograms(int programID, short offeredSemesterID, SemesterNos semesterNoEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.ManageOfferedCourses, UserGroupPermission.PermissionValues.Allowed);
				var admissionOpenPrograms = aspireContext.AdmissionOpenPrograms
					.Where(aop => aop.SemesterID <= offeredSemesterID && aop.ProgramID == programID && aop.Program.InstituteID == loginHistory.InstituteID)
					.OrderByDescending(aop => aop.SemesterID)
					.Select(aop => new
					{
						aop.Program.ProgramAlias,
						aop.AdmissionOpenProgramID,
						aop.IsSummerRegularSemester,
						aop.SemesterID,
						aop.FinalSemesterID
					})
					.ToList();
				var items = new List<CustomListItem>();
				int? selectedAdmissionOpenProgramID = null;
				foreach (var admissionOpenProgram in admissionOpenPrograms)
				{
					var semesterNo = 1;
					var intakeSemesterID = admissionOpenProgram.SemesterID;

					while (intakeSemesterID < offeredSemesterID)
					{
						semesterNo++;
						intakeSemesterID = Semester.AddSemesters(intakeSemesterID, 1, admissionOpenProgram.IsSummerRegularSemester);
					}

					items.Add(new CustomListItem { ID = admissionOpenProgram.AdmissionOpenProgramID, Text = $"{admissionOpenProgram.SemesterID.ToSemesterString()} - Sem.#: {semesterNo}" });
					if (semesterNo.ToString() == semesterNoEnum.ToFullName())
						selectedAdmissionOpenProgramID = admissionOpenProgram.AdmissionOpenProgramID;
				}

				return (items, admissionOpenPrograms.FirstOrDefault()?.ProgramAlias, selectedAdmissionOpenProgramID);
			}
		}

		public sealed class Student
		{
			internal Student() { }
			public int StudentID { get; internal set; }
			public string Enrollment { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public DateTime? RequestedDate { get; internal set; }
			public string PersonalEmail { get; internal set; }
			public string UniversityEmail { get; internal set; }
		}

		public static (List<Student> RegisteredStudents, List<Student> RequestedStudents) GetStudents(int offeredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.Module, UserGroupPermission.PermissionValues.Allowed);
				var offeredCourses = aspireContext.OfferedCourses
					.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(oc => oc.OfferedCourseID == offeredCourseID);

				return
				(
					offeredCourses.SelectMany(oc => oc.RegisteredCourses).Where(rc => rc.DeletedDate == null).Select(rc => new Student
					{
						StudentID = rc.StudentID,
						Enrollment = rc.Student.Enrollment,
						Name = rc.Student.Name,
						ProgramAlias = rc.Student.AdmissionOpenProgram.Program.ProgramAlias,
						PersonalEmail = rc.Student.PersonalEmail,
						UniversityEmail = rc.Student.UniversityEmail,
						RequestedDate = null,
					}).OrderBy(s => s.Enrollment).ToList(),
					offeredCourses.SelectMany(oc => oc.RequestedCourses).Select(rc => new Student
					{
						StudentID = rc.StudentID,
						Enrollment = rc.Student.Enrollment,
						Name = rc.Student.Name,
						ProgramAlias = rc.Student.AdmissionOpenProgram.Program.ProgramAlias,
						RequestedDate = rc.RequestDate,
						PersonalEmail = rc.Student.PersonalEmail,
						UniversityEmail = rc.Student.UniversityEmail,
					}).OrderBy(s => s.Enrollment).ToList()
				);
			}
		}

		public sealed class ImportRegisteredCoursesResult
		{
			public enum Statuses
			{
				NoRecordFound,
				FacultyMemberMismatched,
				AttendanceRecordExists,
				OfferedCourseExamsExists,
				AssignmentsMarksSubmitted,
				QuizzesMarksSubmitted,
				InternalsMarksSubmitted,
				MidMarksSubmitted,
				FinalMarksSubmitted,
				MarksEntryLocked,
				Success
			}

			public int OfferedCourseID { get; internal set; }
			public string Title { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public SemesterNos SemesterNoEnum { get; internal set; }
			public Sections SectionEnum { get; internal set; }
			public Shifts ShiftEnum { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNoEnum, this.SectionEnum, this.ShiftEnum);
			public Statuses Status { get; internal set; }
			public int? RegisteredCoursesCount { get; internal set; }

			internal ImportRegisteredCoursesResult SetStatus(Statuses status)
			{
				this.Status = status;
				return this;
			}
		}

		private static ImportRegisteredCoursesResult ImportRegisteredCourses(this AspireContext aspireContext, int targetOfferedCourseID, int sourceOfferedCourseID, Guid loginSessionGuid)
		{
			var targetOfferedCourse = aspireContext.OfferedCourses
				.Where(oc => oc.OfferedCourseID == targetOfferedCourseID)
				.Select(oc => new
				{
					oc.OfferedCourseID,
					oc.Cours.AdmissionOpenProgram.Program.InstituteID,
					oc.FacultyMemberID,
					oc.SemesterID,
				}).Single();
			var sourceOfferedCourse = aspireContext.OfferedCourses
				.Where(oc => oc.OfferedCourseID == sourceOfferedCourseID)
				.Where(oc => oc.SemesterID == targetOfferedCourse.SemesterID)
				.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == targetOfferedCourse.InstituteID)
				.Select(oc => new
				{
					ImportRegisteredCoursesResult = new ImportRegisteredCoursesResult
					{
						OfferedCourseID = oc.OfferedCourseID,
						Title = oc.Cours.Title,
						ProgramAlias = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						SemesterNoEnum = (SemesterNos)oc.SemesterNo,
						SectionEnum = (Sections)oc.Section,
						ShiftEnum = (Shifts)oc.Shift,
						Status = ImportRegisteredCoursesResult.Statuses.Success,
					},
					oc.Cours.AdmissionOpenProgram.Program.InstituteID,
					oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
					oc.Cours.AdmissionOpenProgram.ProgramID,
					oc.Cours.AdmissionOpenProgramID,
					OfferedCourseAttendancesExists = oc.OfferedCourseAttendances.Any(),
					OfferedCourseExamsExists = oc.OfferedCourseExams.Any(),
					oc.FacultyMemberID,
					oc.AssignmentsMarksSubmitted,
					oc.QuizzesMarksSubmitted,
					oc.InternalsMarksSubmitted,
					oc.MidMarksSubmitted,
					oc.FinalMarksSubmitted,
					oc.MarksEntryLocked,
					RegisteredCourses = oc.RegisteredCourses.Where(rc => rc.DeletedDate == null),
				}).SingleOrDefault();
			if (sourceOfferedCourse == null)
				return new ImportRegisteredCoursesResult { Status = ImportRegisteredCoursesResult.Statuses.NoRecordFound };
			var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanMergeClasses, UserGroupPermission.Allowed, sourceOfferedCourse.InstituteID, sourceOfferedCourse.DepartmentID, sourceOfferedCourse.ProgramID, sourceOfferedCourse.AdmissionOpenProgramID);
			if (targetOfferedCourse.FacultyMemberID != sourceOfferedCourse.FacultyMemberID)
				return sourceOfferedCourse.ImportRegisteredCoursesResult.SetStatus(ImportRegisteredCoursesResult.Statuses.FacultyMemberMismatched);
			if (sourceOfferedCourse.OfferedCourseExamsExists)
				return sourceOfferedCourse.ImportRegisteredCoursesResult.SetStatus(ImportRegisteredCoursesResult.Statuses.OfferedCourseExamsExists);
			if (sourceOfferedCourse.AssignmentsMarksSubmitted)
				return sourceOfferedCourse.ImportRegisteredCoursesResult.SetStatus(ImportRegisteredCoursesResult.Statuses.AssignmentsMarksSubmitted);
			if (sourceOfferedCourse.QuizzesMarksSubmitted)
				return sourceOfferedCourse.ImportRegisteredCoursesResult.SetStatus(ImportRegisteredCoursesResult.Statuses.QuizzesMarksSubmitted);
			if (sourceOfferedCourse.InternalsMarksSubmitted)
				return sourceOfferedCourse.ImportRegisteredCoursesResult.SetStatus(ImportRegisteredCoursesResult.Statuses.InternalsMarksSubmitted);
			if (sourceOfferedCourse.MidMarksSubmitted)
				return sourceOfferedCourse.ImportRegisteredCoursesResult.SetStatus(ImportRegisteredCoursesResult.Statuses.MidMarksSubmitted);
			if (sourceOfferedCourse.FinalMarksSubmitted)
				return sourceOfferedCourse.ImportRegisteredCoursesResult.SetStatus(ImportRegisteredCoursesResult.Statuses.FinalMarksSubmitted);
			if (sourceOfferedCourse.MarksEntryLocked)
				return sourceOfferedCourse.ImportRegisteredCoursesResult.SetStatus(ImportRegisteredCoursesResult.Statuses.MarksEntryLocked);

			foreach (var registeredCours in sourceOfferedCourse.RegisteredCourses)
			{
				registeredCours.OfferedCourseID = targetOfferedCourse.OfferedCourseID;
				var registeredCourseRemark = $"Class merged from \"{sourceOfferedCourse.ImportRegisteredCoursesResult.Title}\" @ {sourceOfferedCourse.ImportRegisteredCoursesResult.Class}.";
				if (registeredCours.Remarks == null)
					registeredCours.Remarks = registeredCourseRemark;
				else
					registeredCours.Remarks += "\n" + registeredCourseRemark;
			}
			aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
			sourceOfferedCourse.ImportRegisteredCoursesResult.RegisteredCoursesCount = sourceOfferedCourse.RegisteredCourses.Count();
			return sourceOfferedCourse.ImportRegisteredCoursesResult.SetStatus(ImportRegisteredCoursesResult.Statuses.Success);
		}

		public enum ImportRegisteredCoursesStatuses
		{
			NoRecordFound,
			OfferedCourseAttendancesExists,
			OfferedCourseExamsExists,
			AssignmentsMarksSubmitted,
			QuizzesMarksSubmitted,
			InternalsMarksSubmitted,
			MidMarksSubmitted,
			FinalMarksSubmitted,
			MarksEntryLocked,
			Failed,
			Success,
		}

		public static (ImportRegisteredCoursesStatuses Status, List<ImportRegisteredCoursesResult> ImportResults) ImportRegisteredCourses(int targetOfferedCourseID, List<int> sourceOfferedCourseIDs, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var result = new List<ImportRegisteredCoursesResult>();
				var offeredCourse = aspireContext.OfferedCourses
					.Where(oc => oc.OfferedCourseID == targetOfferedCourseID)
					.Select(oc => new
					{
						oc.OfferedCourseID,
						oc.Cours.AdmissionOpenProgram.Program.InstituteID,
						oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
						oc.Cours.AdmissionOpenProgram.ProgramID,
						oc.Cours.AdmissionOpenProgramID,
						OfferedCourseAttendancesExists = oc.OfferedCourseAttendances.Any(),
						OfferedCourseExamsExists = oc.OfferedCourseExams.Any(),
						oc.FacultyMemberID,
						oc.AssignmentsMarksSubmitted,
						oc.QuizzesMarksSubmitted,
						oc.InternalsMarksSubmitted,
						oc.MidMarksSubmitted,
						oc.FinalMarksSubmitted,
						oc.MarksEntryLocked,
					}).SingleOrDefault();
				if (offeredCourse == null)
					return (ImportRegisteredCoursesStatuses.NoRecordFound, null);
				var loginHistory = aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanMergeClasses, UserGroupPermission.Allowed, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID);
				if (offeredCourse.OfferedCourseAttendancesExists)
					return (ImportRegisteredCoursesStatuses.OfferedCourseAttendancesExists, null);
				if (offeredCourse.OfferedCourseExamsExists)
					return (ImportRegisteredCoursesStatuses.OfferedCourseExamsExists, null);
				if (offeredCourse.AssignmentsMarksSubmitted)
					return (ImportRegisteredCoursesStatuses.AssignmentsMarksSubmitted, null);
				if (offeredCourse.QuizzesMarksSubmitted)
					return (ImportRegisteredCoursesStatuses.QuizzesMarksSubmitted, null);
				if (offeredCourse.InternalsMarksSubmitted)
					return (ImportRegisteredCoursesStatuses.InternalsMarksSubmitted, null);
				if (offeredCourse.MidMarksSubmitted)
					return (ImportRegisteredCoursesStatuses.MidMarksSubmitted, null);
				if (offeredCourse.FinalMarksSubmitted)
					return (ImportRegisteredCoursesStatuses.FinalMarksSubmitted, null);
				if (offeredCourse.MarksEntryLocked)
					return (ImportRegisteredCoursesStatuses.MarksEntryLocked, null);

				foreach (var sourceOfferedCourseID in sourceOfferedCourseIDs)
				{
					var individualResult = aspireContext.ImportRegisteredCourses(offeredCourse.OfferedCourseID, sourceOfferedCourseID, loginSessionGuid);
					result.Add(individualResult);
				}
				if (result.All(r => r.Status == ImportRegisteredCoursesResult.Statuses.Success))
				{
					aspireContext.CommitTransaction();
					return (ImportRegisteredCoursesStatuses.Success, result);
				}
				else
				{
					aspireContext.RollbackTransaction();
					return (ImportRegisteredCoursesStatuses.Failed, result);
				}
			}
		}
	}
}
