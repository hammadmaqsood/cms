﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.CourseOffering.Staff.Reports
{
	public static class OfferedCourses
	{
		private static Core.Common.Permissions.Staff.StaffGroupPermission DemandModulePermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class OfferedCoursesPageHeader
		{
			public string Institute { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Title => $"Courses Offered ({this.SemesterID.ToSemesterString()})";
			public static List<OfferedCoursesPageHeader> GetPageHeaders()
			{
				return null;
			}
		}

		public sealed class OfferedCoursesDataSet
		{
			public int OfferedCourseID { get; internal set; }
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public string CreditHoursFullName => this.CreditHours.ToCreditHoursFullName();
			public int? DepartmentID { get; internal set; }
			public string DepartmentName { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public string FacultyMemberNameNA => this.FacultyMemberName.ToNAIfNullOrEmpty();
			public int RegisteredStrength { get; internal set; }
			public int MaxClassStrength { get; internal set; }
			public Guid Status { get; internal set; }
			public string StatusFullName => this.Status.GetFullName();
			public short RoadmapSemesterID { get; internal set; }
			public bool Visiting { get; internal set; }
			public bool SpecialOffered { get; internal set; }
			public string Roadmap => this.RoadmapSemesterID.ToSemesterString();
			public string VisitingYesNo => this.Visiting.ToYesNo();
			public string SpecialOfferedYesNo => this.SpecialOffered.ToYesNo();
			public List<Class> Classes { get; internal set; }
			public string ClassesNames
			{
				get
				{
					var list = new List<string> { AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift) };
					list.AddRange(this.Classes.Select(@class => AspireFormats.GetClassName(this.ProgramAlias, @class.SemesterNo, @class.Section, @class.Shift)));
					return string.Join("\n", list);
				}
			}

			public sealed class Class
			{
				internal Class() { }
				public short SemesterNo { get; internal set; }
				public int Section { get; internal set; }
				public byte Shift { get; internal set; }
			}

			public static List<OfferedCoursesDataSet> GetOfferedCourses()
			{
				return null;
			}
		}

		public class ReportDataSet
		{
			internal ReportDataSet() { }
			public OfferedCoursesPageHeader PageHeader { get; internal set; }
			public List<OfferedCoursesDataSet> OfferedCourses { get; internal set; }
		}

		public static ReportDataSet GetOfferedCourses(short semesterID, int? departmentID, bool? visiting, bool? specialOffered, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(loginSessionGuid);
				var offeredCourses = aspireContext.OfferedCourses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID);
				offeredCourses = offeredCourses.Where(oc => oc.SemesterID == semesterID);
				if (departmentID != null)
					offeredCourses = offeredCourses.Where(oc => oc.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID);
				if (visiting != null)
					offeredCourses = offeredCourses.Where(oc => oc.Visiting == visiting);
				if (specialOffered != null)
					offeredCourses = offeredCourses.Where(oc => oc.SpecialOffered == specialOffered);
				var result = offeredCourses.Select(oc => new OfferedCoursesDataSet
				{
					OfferedCourseID = oc.OfferedCourseID,
					RoadmapSemesterID = oc.Cours.AdmissionOpenProgram.SemesterID,
					CourseCode = oc.Cours.CourseCode,
					Title = oc.Cours.Title,
					CreditHours = oc.Cours.CreditHours,
					ProgramAlias = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = oc.SemesterNo,
					Section = oc.Section,
					Shift = oc.Shift,
					FacultyMemberName = oc.FacultyMember.Name,
					Visiting = oc.Visiting,
					SpecialOffered = oc.SpecialOffered,
					RegisteredStrength = oc.RegisteredCourses.Count(rc => rc.DeletedDate == null),
					DepartmentID = oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
					DepartmentName = oc.Cours.AdmissionOpenProgram.Program.Department.DepartmentName,
					MaxClassStrength = oc.MaxClassStrength,
					Status = oc.Cours.Status,
					Classes = oc.OfferedCourseClasses.Select(occ => new OfferedCoursesDataSet.Class
					{
						SemesterNo = occ.SemesterNo,
						Section = occ.Section,
						Shift = occ.Shift,
					}).ToList()
				}).OrderBy(o => o.ProgramAlias).ThenBy(t => t.SemesterNo).ThenBy(t => t.Section).ThenBy(t => t.Shift).ThenBy(t => t.Title).ToList();

				return new ReportDataSet
				{
					PageHeader = new OfferedCoursesPageHeader
					{
						SemesterID = semesterID,
						Institute = aspireContext.Institutes.Where(i => i.InstituteID == loginHistory.InstituteID).Select(i => i.InstituteName).Single()
					},
					OfferedCourses = result
				};
			}
		}
	}
}
