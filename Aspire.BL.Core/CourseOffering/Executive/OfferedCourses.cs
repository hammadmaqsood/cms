﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Aspire.Model.Entities;

namespace Aspire.BL.Core.CourseOffering.Executive
{
	public static class OfferedCourses
	{
		private static Common.Permissions.Executive.ExecutiveLoginHistory DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandExecutiveActiveSession(loginSessionGuid);
		}

		public sealed class OfferedCourse
		{
			internal OfferedCourse() { }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public int OfferedCourseID { get; internal set; }
			public int CourseID { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public string Title { get; internal set; }
			public string Program { get; internal set; }
			public short SemesterNo { get; internal set; }
			public SemesterNos SemesterNoEnum => (SemesterNos)this.SemesterNo;
			public int Section { get; internal set; }
			public Sections SectionEnum => (Sections)this.Section;
			public byte Shift { get; internal set; }
			public Shifts ShiftEnum => (Shifts)this.Shift;
			public string Class => AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift);
			public int? FacultyMemberID { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public string DepartmentName { get; internal set; }
			public int ProgramID { get; internal set; }
			public string CNIC { get; internal set; }
			public string CNICNA => this.CNIC.ToNAIfNullOrEmpty();
			public Guid? ReadinessType { get; internal set; }
			public DateTime? SubmittedByClusterHeadDate { get; internal set; }
			public string SubmittedByClusterHead { get; internal set; }
			public string ReadinessTypeFullName => this.ReadinessType?.GetFullName();
		}

		public static List<OfferedCourse> GetOfferedCourses(short semesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int? facultyMemberID, OnlineTeachingReadiness.ReadinessTypes? readinessTypeEnum, bool? isFromOnlineTeachingReadinessForm, int pageIndex, int pageSize, out int virtualItemCount, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			if (string.IsNullOrWhiteSpace(sortExpression))
				throw new ArgumentNullException(nameof(sortExpression));
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var courses = aspireContext.OfferedCourses.Where(oc => oc.SemesterID == semesterID && oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(oc => oc.RegisteredCourses.Any(rc => rc.DeletedDate == null));

				if (departmentID != null)
					courses = courses.Where(c => c.Cours.AdmissionOpenProgram.Program.DepartmentID == departmentID.Value);
				if (programID != null)
					courses = courses.Where(c => c.Cours.AdmissionOpenProgram.ProgramID == programID.Value);
				if (semesterNoEnum != null)
				{
					var semesterNo = (short)semesterNoEnum;
					courses = courses.Where(c => c.SemesterNo == semesterNo);
				}
				if (sectionEnum != null)
				{
					var section = (int)sectionEnum;
					courses = courses.Where(c => c.Section == section);
				}
				if (shiftEnum != null)
				{
					var shift = (byte)shiftEnum;
					courses = courses.Where(c => c.Shift == shift);
				}
				if (facultyMemberID != null)
					courses = courses.Where(c => c.FacultyMemberID == facultyMemberID.Value);

				if (readinessTypeEnum != null)
				{
					var readinessTypeGuid = readinessTypeEnum.GetGuid();
					courses = courses.Where(c => c.OnlineTeachingReadinesses.Any(otr => otr.ReadinessType == readinessTypeGuid));
					if (isFromOnlineTeachingReadinessForm != null && isFromOnlineTeachingReadinessForm.Value)
						courses = courses.Where(oc => oc.OnlineTeachingReadinesses.Any(otr => otr.SubmittedByDGDate != null));
				}

				virtualItemCount = courses.Count();
				switch (sortExpression)
				{
					case nameof(OfferedCourse.SemesterID):
					case nameof(OfferedCourse.Semester):
						courses = courses.OrderBy(sortDirection, c => c.SemesterID);
						break;
					case nameof(OfferedCourse.Title):
						courses = courses.OrderBy(sortDirection, c => c.Cours.Title);
						break;
					case nameof(OfferedCourse.Program):
						courses = courses.OrderBy(sortDirection, c => c.Cours.AdmissionOpenProgram.Program.ProgramAlias);
						break;
					case nameof(OfferedCourse.DepartmentName):
						courses = courses.OrderBy(sortDirection, c => c.Cours.AdmissionOpenProgram.Program.Department.DepartmentName);
						break;
					case nameof(OfferedCourse.SemesterNo):
					case nameof(OfferedCourse.SemesterNoEnum):
						courses = courses.OrderBy(sortDirection, c => c.SemesterNo);
						break;
					case nameof(OfferedCourse.Section):
					case nameof(OfferedCourse.SectionEnum):
						courses = courses.OrderBy(sortDirection, c => c.Section);
						break;
					case nameof(OfferedCourse.Shift):
					case nameof(OfferedCourse.ShiftEnum):
						courses = courses.OrderBy(sortDirection, c => c.Shift);
						break;
					case nameof(OfferedCourse.CNIC):
					case nameof(OfferedCourse.CNICNA):
						courses = courses.OrderBy(sortDirection, c => c.FacultyMember.CNIC);
						break;
					case nameof(OfferedCourse.Class):
						courses = courses.OrderBy(sortDirection, c => c.Cours.AdmissionOpenProgram.Program.ProgramAlias)
							.ThenBy(sortDirection, c => c.SemesterNo)
							.ThenBy(sortDirection, c => c.Section)
							.ThenBy(sortDirection, c => c.Shift);
						break;
					case nameof(OfferedCourse.FacultyMemberName):
						courses = courses.OrderBy(sortDirection, c => c.FacultyMember.Name);
						break;
					case nameof(OfferedCourse.ReadinessType):
						courses = courses.OrderBy(sortDirection, c => c.OnlineTeachingReadinesses.Select(otr => otr.ReadinessType).FirstOrDefault());
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				courses = courses.Skip(pageIndex * pageSize).Take(pageSize);
				return courses.Select(c => new OfferedCourse
				{
					OfferedCourseID = c.OfferedCourseID,
					CourseID = c.Cours.CourseID,
					Title = c.Cours.Title,
					DepartmentID = c.Cours.AdmissionOpenProgram.Program.DepartmentID,
					DepartmentName = c.Cours.AdmissionOpenProgram.Program.Department.DepartmentName,
					ProgramID = c.Cours.AdmissionOpenProgram.ProgramID,
					Program = c.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterID = c.SemesterID,
					SemesterNo = c.SemesterNo,
					Section = c.Section,
					Shift = c.Shift,
					FacultyMemberName = c.FacultyMember.Name,
					FacultyMemberID = c.FacultyMemberID,
					CNIC = c.FacultyMember.CNIC,
					ReadinessType = c.OnlineTeachingReadinesses.Where(otr => otr.OfferedCourseID == c.OfferedCourseID).Select(otr => (Guid?)otr.ReadinessType).FirstOrDefault(),
					SubmittedByClusterHeadDate = c.OnlineTeachingReadinesses.Where(otr => otr.OfferedCourseID == c.OfferedCourseID).Select(otr => (DateTime?)otr.SubmittedByClusterHeadDate).FirstOrDefault(),
					SubmittedByClusterHead = c.OnlineTeachingReadinesses.Where(otr => otr.OfferedCourseID == c.OfferedCourseID).Select(otr => (string)otr.UserLoginHistory.User.Name).FirstOrDefault(),
				}).ToList();
			}
		}
	}
}
