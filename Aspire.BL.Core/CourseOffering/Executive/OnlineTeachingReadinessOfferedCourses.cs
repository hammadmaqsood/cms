﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.CourseOffering.Executive
{
	public static class OnlineTeachingReadinessOfferedCourses
	{
		private static Common.Permissions.Executive.ExecutiveModulePermissions DemandPermissions(this AspireContext aspireContext, Guid loginSessionGuid)
		{
			return aspireContext.DemandExecutiveModulePermissions(loginSessionGuid, ExecutivePermissions.CourseOffering.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		[Flags]
		public enum OnlineTeachingReadinessOfferedCoursesFields
		{
			Department = 1 << 0,
			Program = 1 << 1,
			SemesterNo = 1 << 2,
			Section = 1 << 3,
			Shift = 1 << 4,
		}

		//public static DataTable GetOnlineTeachingReadinessCourses(short semesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields groupByFields, Guid loginSessionGuid)
		//{
		//	using (var aspireContext = new AspireContext())
		//	{
		//		aspireContext.IncreaseCommandTimeout();
		//		var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
		//		var programs = loginHistory.Programs;

		//		if (departmentID != null)
		//			programs = programs.Where(p => p.DepartmentID == departmentID.Value);

		//		if (programID != null)
		//			programs = programs.Where(p => p.ProgramID == programID.Value);

		//		var offeredCourses = programs.SelectMany(p => p.AdmissionOpenPrograms).SelectMany(aop => aop.Courses).SelectMany(c => c.OfferedCourses);
		//		offeredCourses = offeredCourses.Where(oc => oc.SemesterID == semesterID);

		//		if (semesterNoEnum != null)
		//			offeredCourses = offeredCourses.Where(oc => oc.SemesterNo == (short)semesterNoEnum);

		//		if (sectionEnum != null)
		//			offeredCourses = offeredCourses.Where(oc => oc.Section == (int)sectionEnum);

		//		if (shiftEnum != null)
		//			offeredCourses = offeredCourses.Where(oc => oc.Shift == (byte)shiftEnum);

		//		var groupByDepartmentID = groupByFields.HasFlag(OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields.Department);
		//		var groupByProgramID = groupByFields.HasFlag(OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields.Program);
		//		var groupBySemesterNo = groupByFields.HasFlag(OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields.SemesterNo);
		//		var groupBySection = groupByFields.HasFlag(OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields.Section);
		//		var groupByShift = groupByFields.HasFlag(OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields.Shift);

		//		var groupedResult = offeredCourses.GroupBy(oc => new
		//		{
		//			oc.SemesterID,
		//			DepartmentID = groupByDepartmentID ? oc.Cours.AdmissionOpenProgram.Program.DepartmentID : null,
		//			DepartmentName = groupByDepartmentID ? oc.Cours.AdmissionOpenProgram.Program.Department.DepartmentName : null,
		//			ProgramID = groupByProgramID ? oc.Cours.AdmissionOpenProgram.ProgramID : (int?)null,
		//			ProgramAlias = groupByProgramID ? oc.Cours.AdmissionOpenProgram.Program.ProgramAlias : null,
		//			SemesterNo = groupBySemesterNo ? oc.SemesterNo : (short?)null,
		//			Section = groupBySection ? oc.Section : (int?)null,
		//			Shift = groupByShift ? oc.Shift : (byte?)null,
		//		});

		//		dynamic result;
		//		var fullReadyGuid = OnlineTeachingReadiness.ReadinessTypes.FullyReady.GetGuid();
		//		var partialReadyGuid = OnlineTeachingReadiness.ReadinessTypes.PartiallyReady.GetGuid();
		//		var notReadyGuid = OnlineTeachingReadiness.ReadinessTypes.NotReady.GetGuid();
		//		result = groupedResult.Select(oc => new
		//		{
		//			oc.Key,
		//			OfferedCourses = oc.Count(),
		//			FullyReady = oc.SelectMany(t => t.OnlineTeachingReadinesses).Count(otr => otr.ReadinessType == fullReadyGuid),
		//			PartialReady = oc.SelectMany(t => t.OnlineTeachingReadinesses).Count(otr => otr.ReadinessType == partialReadyGuid),
		//			NotReady = oc.SelectMany(t => t.OnlineTeachingReadinesses).Count(otr => otr.ReadinessType == notReadyGuid),
		//		}).ToList();
		//		var table = new DataTable("OnlineTeachingReadinessOfferedCourses");
		//		int? departmentIDColumnIndex = null;
		//		int? departmentColumnIndex = null;
		//		int? programIDColumnIndex = null;
		//		int? programColumnIndex = null;
		//		int? semesterNoColumnIndex = null;
		//		int? sectionColumnIndex = null;
		//		int? shiftColumnIndex = null;

		//		var sortExpressions = new List<string>();
		//		table.Columns.Add("Semester", typeof(string));
		//		if (groupByDepartmentID)
		//		{
		//			departmentIDColumnIndex = table.Columns.Add("DepartmentID", typeof(int)).Ordinal;
		//			departmentColumnIndex = table.Columns.Add("Department", typeof(string)).Ordinal;
		//			sortExpressions.Add(table.Columns[departmentColumnIndex.Value].ColumnName);

		//		}

		//		if (groupByProgramID)
		//		{
		//			programIDColumnIndex = table.Columns.Add("ProgramID", typeof(int)).Ordinal;
		//			programColumnIndex = table.Columns.Add("Program", typeof(string)).Ordinal;
		//			sortExpressions.Add(table.Columns[programColumnIndex.Value].ColumnName);
		//		}

		//		if (groupBySemesterNo)
		//		{
		//			semesterNoColumnIndex = table.Columns.Add("Semester No.", typeof(string)).Ordinal;
		//			sortExpressions.Add(table.Columns[semesterNoColumnIndex.Value].ColumnName);
		//		}

		//		if (groupBySection)
		//		{
		//			sectionColumnIndex = table.Columns.Add("Section", typeof(string)).Ordinal;
		//			sortExpressions.Add(table.Columns[sectionColumnIndex.Value].ColumnName);
		//		}

		//		if (groupByShift)
		//		{
		//			shiftColumnIndex = table.Columns.Add("Shift", typeof(string)).Ordinal;
		//			sortExpressions.Add(table.Columns[shiftColumnIndex.Value].ColumnName);
		//		}
		//		var offeredCoursesColumnIndex = table.Columns.Add("Offered Courses", typeof(int)).Ordinal;
		//		var fullyReadyColumnIndex = table.Columns.Add("Fully Ready", typeof(int)).Ordinal;
		//		var partiallyReadyColumnIndex = table.Columns.Add("Partially Ready", typeof(int)).Ordinal;
		//		var notReadyColumnIndex = table.Columns.Add("Not Ready", typeof(int)).Ordinal;
		//		table.DefaultView.Sort = string.Join(",", sortExpressions.Select(e => $"[{e}] ASC"));

		//		foreach (var record in result)
		//		{
		//			var recordSemesterID = (short)record.Key.SemesterID;
		//			var row = table.Rows.Add(recordSemesterID.ToSemesterString());
		//			row[0] = recordSemesterID.ToSemesterString();
		//			if (groupByDepartmentID)
		//			{
		//				row[departmentIDColumnIndex.Value] = (record.Key.DepartmentID);
		//				row[departmentColumnIndex.Value] = ((string)record.Key.DepartmentName).ToNAIfNullOrEmpty();
		//			}
		//			if (groupByProgramID)
		//			{
		//				row[programIDColumnIndex.Value] = (record.Key.ProgramID);
		//				row[programColumnIndex.Value] = ((string)record.Key.ProgramAlias).ToNAIfNullOrEmpty();
		//			}
		//			if (groupBySemesterNo)
		//				row[semesterNoColumnIndex.Value] = ((SemesterNos)record.Key.SemesterNo).ToFullName();
		//			if (groupBySection)
		//				row[sectionColumnIndex.Value] = ((Sections)record.Key.Section).ToFullName();
		//			if (groupByShift)
		//				row[shiftColumnIndex.Value] = ((Shifts)record.Key.Shift).ToFullName();
		//			row[offeredCoursesColumnIndex] = record.OfferedCourses;
		//			row[fullyReadyColumnIndex] = record.FullyReady;
		//			row[partiallyReadyColumnIndex] = record.PartialReady;
		//			row[notReadyColumnIndex] = record.NotReady;

		//		}
		//		return table;
		//	}
		//}

		public static GridView GetOnlineTeachingReadinessOfferedCoursesGrid(short semesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields groupByFields, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.IncreaseCommandTimeout();
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var programs = loginHistory.Programs;

				if (departmentID != null)
					programs = programs.Where(p => p.DepartmentID == departmentID.Value);

				if (programID != null)
					programs = programs.Where(p => p.ProgramID == programID.Value);

				var offeredCourses = programs.SelectMany(p => p.AdmissionOpenPrograms).SelectMany(aop => aop.Courses).SelectMany(c => c.OfferedCourses.Where(oc => oc.RegisteredCourses.Any(rc => rc.DeletedDate == null)));
				offeredCourses = offeredCourses.Where(oc => oc.SemesterID == semesterID);

				if (semesterNoEnum != null)
					offeredCourses = offeredCourses.Where(oc => oc.SemesterNo == (short)semesterNoEnum);

				if (sectionEnum != null)
					offeredCourses = offeredCourses.Where(oc => oc.Section == (int)sectionEnum);

				if (shiftEnum != null)
					offeredCourses = offeredCourses.Where(oc => oc.Shift == (byte)shiftEnum);

				var groupByDepartmentID = groupByFields.HasFlag(OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields.Department);
				var groupByProgramID = groupByFields.HasFlag(OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields.Program);
				var groupBySemesterNo = groupByFields.HasFlag(OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields.SemesterNo);
				var groupBySection = groupByFields.HasFlag(OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields.Section);
				var groupByShift = groupByFields.HasFlag(OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields.Shift);

				var fullReadyGuid = OnlineTeachingReadiness.ReadinessTypes.FullyReady.GetGuid();
				var partialReadyGuid = OnlineTeachingReadiness.ReadinessTypes.PartiallyReady.GetGuid();
				var notReadyGuid = OnlineTeachingReadiness.ReadinessTypes.NotReady.GetGuid();

				var result = offeredCourses.GroupBy(oc => new
				{
					oc.SemesterID,
					DepartmentID = groupByDepartmentID ? oc.Cours.AdmissionOpenProgram.Program.DepartmentID : null,
					DepartmentName = groupByDepartmentID ? oc.Cours.AdmissionOpenProgram.Program.Department.DepartmentName : null,
					ProgramID = groupByProgramID ? oc.Cours.AdmissionOpenProgram.ProgramID : (int?)null,
					ProgramAlias = groupByProgramID ? oc.Cours.AdmissionOpenProgram.Program.ProgramAlias : null,
					SemesterNo = groupBySemesterNo ? oc.SemesterNo : (short?)null,
					Section = groupBySection ? oc.Section : (int?)null,
					Shift = groupByShift ? oc.Shift : (byte?)null,
				}).Select(oc => new
				{
					SemesterID = oc.Key.SemesterID,
					DepartmentID = groupByDepartmentID ? oc.Key.DepartmentID : null,
					DepartmentName = groupByDepartmentID ? oc.Key.DepartmentName : null,
					ProgramID = groupByProgramID ? oc.Key.ProgramID : (int?)null,
					ProgramAlias = groupByProgramID ? oc.Key.ProgramAlias : null,
					SemesterNo = groupBySemesterNo ? oc.Key.SemesterNo : (short?)null,
					Section = groupBySection ? oc.Key.Section : (int?)null,
					Shift = groupByShift ? oc.Key.Shift : (byte?)null,
					OfferedCourses = oc.Count(),
					FullyReady = oc.SelectMany(t => t.OnlineTeachingReadinesses).Count(otr => otr.ReadinessType == fullReadyGuid && otr.SubmittedByDGDate != null),
					PartialReady = oc.SelectMany(t => t.OnlineTeachingReadinesses).Count(otr => otr.ReadinessType == partialReadyGuid && otr.SubmittedByDGDate != null),
					NotReady = oc.SelectMany(t => t.OnlineTeachingReadinesses).Count(otr => otr.ReadinessType == notReadyGuid && otr.SubmittedByDGDate != null),
				}).ToList();
				GridView gvOnlineTeachingReadiness = new GridView();
				gvOnlineTeachingReadiness.AutoGenerateColumns = false;
				gvOnlineTeachingReadiness.ShowFooter = true;
				gvOnlineTeachingReadiness.CssClass = "table-bordered table-striped table-hover table AspireGridView sortable";
				gvOnlineTeachingReadiness.EmptyDataText = "No Record Found";
				gvOnlineTeachingReadiness.Caption = "DG certified courses are considered only";
				var table = new DataTable("OnlineTeachingReadinessOfferedCourses");
				int serialNoColumnIndex = -1;
				int semesterIDColumnIndex = -1;
				int departmentIDColumnIndex = -1;
				int programIDColumnIndex = -1;
				int semesterNoValueColumnIndex = -1;
				int sectionValueColumnIndex = -1;
				int shiftValueColumnIndex = -1;

				int? departmentColumnIndex = null;
				int? programColumnIndex = null;
				int? semesterNoColumnIndex = null;
				int? sectionColumnIndex = null;
				int? shiftColumnIndex = null;
				var sortExpressions = new List<string>();

				serialNoColumnIndex = table.Columns.Add("SerialNo", typeof(int)).Ordinal;
				gvOnlineTeachingReadiness.Columns.Add(new BoundField() { HeaderText = "#", DataField = "SerialNo" });
				table.Columns.Add("Semester", typeof(string));
				gvOnlineTeachingReadiness.Columns.Add(new BoundField() { HeaderText = "Semester", DataField = "Semester" });

				semesterIDColumnIndex = table.Columns.Add("SemesterID", typeof(short)).Ordinal;
				departmentIDColumnIndex = table.Columns.Add("DepartmentID", Nullable.GetUnderlyingType(typeof(int?))).Ordinal;
				programIDColumnIndex = table.Columns.Add("ProgramID", Nullable.GetUnderlyingType(typeof(int?))).Ordinal;
				semesterNoValueColumnIndex = table.Columns.Add("SemesterNoValue", Nullable.GetUnderlyingType(typeof(short?))).Ordinal;
				sectionValueColumnIndex = table.Columns.Add("SectionValue", Nullable.GetUnderlyingType(typeof(int?))).Ordinal;
				shiftValueColumnIndex = table.Columns.Add("ShiftValue", Nullable.GetUnderlyingType(typeof(byte?))).Ordinal;

				if (groupByDepartmentID)
				{
					departmentColumnIndex = table.Columns.Add("DepartmentName", typeof(string)).Ordinal;
					gvOnlineTeachingReadiness.Columns.Add(new BoundField() { HeaderText = "Department", DataField = "DepartmentName" });
					sortExpressions.Add(table.Columns[departmentColumnIndex.Value].ColumnName);
				}
				if (groupByProgramID)
				{
					programColumnIndex = table.Columns.Add("ProgramAlias", typeof(string)).Ordinal;
					gvOnlineTeachingReadiness.Columns.Add(new BoundField() { HeaderText = "Program", DataField = "ProgramAlias" });
					sortExpressions.Add(table.Columns[programColumnIndex.Value].ColumnName);
				}
				if (groupBySemesterNo)
				{
					semesterNoColumnIndex = table.Columns.Add("SemesterNo", typeof(string)).Ordinal;
					gvOnlineTeachingReadiness.Columns.Add(new BoundField() { HeaderText = "Semester No.", DataField = "SemesterNo" });
					sortExpressions.Add(table.Columns[semesterNoColumnIndex.Value].ColumnName);
				}

				if (groupBySection)
				{
					sectionColumnIndex = table.Columns.Add("Section", typeof(string)).Ordinal;
					gvOnlineTeachingReadiness.Columns.Add(new BoundField() { HeaderText = "Section", DataField = "Section" });
					sortExpressions.Add(table.Columns[sectionColumnIndex.Value].ColumnName);
				}

				if (groupByShift)
				{
					shiftColumnIndex = table.Columns.Add("Shift", typeof(string)).Ordinal;
					gvOnlineTeachingReadiness.Columns.Add(new BoundField() { HeaderText = "Shift", DataField = "Shift" });
					sortExpressions.Add(table.Columns[shiftColumnIndex.Value].ColumnName);
				}
				var offeredCoursesColumnIndex = table.Columns.Add("OfferedCourses", typeof(int)).Ordinal;
				gvOnlineTeachingReadiness.Columns.Add(new TemplateField { HeaderText = "Offered Courses" });
				var fullyReadyColumnIndex = table.Columns.Add("FullyReady", typeof(int)).Ordinal;
				gvOnlineTeachingReadiness.Columns.Add(new TemplateField { HeaderText = "Fully Ready" });
				var partiallyReadyColumnIndex = table.Columns.Add("PartiallyReady", typeof(int)).Ordinal;
				gvOnlineTeachingReadiness.Columns.Add(new TemplateField { HeaderText = "Partially Ready" });
				var notReadyColumnIndex = table.Columns.Add("NotReady", typeof(int)).Ordinal;
				gvOnlineTeachingReadiness.Columns.Add(new TemplateField { HeaderText = "Not Ready" });
				int serialNo = 0;
				foreach (var record in result)
				{
					serialNo++;
					var recordSemesterID = (short)record.SemesterID;
					var row = table.Rows.Add();
					row[serialNoColumnIndex] = serialNo;
					row[1] = recordSemesterID.ToSemesterString(); row[semesterIDColumnIndex] = record.SemesterID;
					if (groupByDepartmentID)
					{
						row[departmentIDColumnIndex] = record.DepartmentID;
						row[departmentColumnIndex.Value] = ((string)record.DepartmentName).ToNAIfNullOrEmpty();
					}
					if (groupByProgramID)
					{
						row[programIDColumnIndex] = record.ProgramID;
						row[programColumnIndex.Value] = ((string)record.ProgramAlias).ToNAIfNullOrEmpty();
					}
					if (groupBySemesterNo)
					{
						row[semesterNoValueColumnIndex] = record.SemesterNo;
						row[semesterNoColumnIndex.Value] = ((SemesterNos)record.SemesterNo).ToFullName();
					}
					if (groupBySection)
					{
						row[sectionValueColumnIndex] = record.Section;
						row[sectionColumnIndex.Value] = ((Sections)record.Section).ToFullName();
					}
					if (groupByShift)
					{
						row[shiftValueColumnIndex] = record.Shift;
						row[shiftColumnIndex.Value] = ((Shifts)record.Shift).ToFullName();
					}
					row[offeredCoursesColumnIndex] = record.OfferedCourses;
					row[fullyReadyColumnIndex] = record.FullyReady;
					row[partiallyReadyColumnIndex] = record.PartialReady;
					row[notReadyColumnIndex] = record.NotReady;
				}
				gvOnlineTeachingReadiness.DataSource = table;
				return gvOnlineTeachingReadiness;
			}
		}

		public sealed class OfferedCourseDetails
		{
			internal OfferedCourseDetails() { }
			public int OfferedCourseID { get; internal set; }
			public int CourseID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string CourseCode { get; internal set; }
			public string Title { get; internal set; }
			public short SemesterNo { get; internal set; }
			public int Section { get; internal set; }
			public byte Shift { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public string FacultyMemberDepartmentAlias { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public DateTime? SubmittedToHOD { get; internal set; }
			public DateTime? SubmittedToCampus { get; internal set; }
			public DateTime? SubmittedToUniversity { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
			public int InstituteID { get; internal set; }
			public int? DepartmentID { get; internal set; }
			public int ProgramID { get; internal set; }
			public int AdmissionOpenProgramID { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public decimal ContactHours { get; internal set; }
			public bool Visiting { get; internal set; }
			public FacultyMember.FacultyTypes? FacultyTypeEnum { get; internal set; }
			public Model.Entities.OnlineTeachingReadiness OnlineTeachingReadiness { get; internal set; }
			public Dictionary<int, string> SubmittedByUsers { get; internal set; }
		}

		public static OfferedCourseDetails GetOfferedCourse(int offeredCourseID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandPermissions(loginSessionGuid);
				var offeredCourse = aspireContext.OfferedCourses.Where(oc => oc.OfferedCourseID == offeredCourseID)
					.Select(oc => new OfferedCourseDetails
					{
						OfferedCourseID = oc.OfferedCourseID,
						ProgramAlias = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						InstituteID = oc.Cours.AdmissionOpenProgram.Program.InstituteID,
						DepartmentID = oc.Cours.AdmissionOpenProgram.Program.DepartmentID,
						ProgramID = oc.Cours.AdmissionOpenProgram.ProgramID,
						AdmissionOpenProgramID = oc.Cours.AdmissionOpenProgramID,
						SemesterID = oc.SemesterID,
						SemesterNo = oc.SemesterNo,
						Section = oc.Section,
						Shift = oc.Shift,
						CourseID = oc.CourseID,
						FacultyMemberName = oc.FacultyMember.Name,
						FacultyMemberDepartmentAlias = oc.FacultyMember.Department.DepartmentAlias,
						FacultyTypeEnum = (Model.Entities.FacultyMember.FacultyTypes?)oc.FacultyMember.FacultyType,
						Title = oc.Cours.Title,
						CourseCode = oc.Cours.CourseCode,
						CreditHours = oc.Cours.CreditHours,
						ContactHours = oc.Cours.ContactHours,
						Visiting = oc.Visiting,
						OnlineTeachingReadiness = oc.OnlineTeachingReadinesses.FirstOrDefault()
					}).SingleOrDefault();
				if (offeredCourse == null)
					return null;
				//aspireContext.DemandStaffPermissions(loginSessionGuid, StaffPermissions.CourseOffering.CanViewOnlineTeachingReadinessForm, Model.Entities.UserGroupPermission.PermissionValues.Allowed, offeredCourse.InstituteID, offeredCourse.DepartmentID, offeredCourse.ProgramID, offeredCourse.AdmissionOpenProgramID);
				if (offeredCourse.OnlineTeachingReadiness != null)
				{
					var userLoginHistoryIDs = new[] {
						offeredCourse.OnlineTeachingReadiness.SubmittedByClusterHeadUserLoginHistoryID,
						offeredCourse.OnlineTeachingReadiness.SubmittedByHODUserLoginHistoryID,
						offeredCourse.OnlineTeachingReadiness.SubmittedByDirectorUserLoginHistoryID,
						offeredCourse.OnlineTeachingReadiness.SubmittedByDGUserLoginHistoryID
					}.Where(u => u != null).ToList();
					if (userLoginHistoryIDs.Any())
					{
						var query = from h in aspireContext.UserLoginHistories
									join r in aspireContext.UserRoles on h.UserRoleID equals r.UserRoleID
									join u in aspireContext.Users on r.UserID equals u.UserID
									where userLoginHistoryIDs.Contains(h.UserLoginHistoryID)
									select new { h.UserLoginHistoryID, u.Name };
						offeredCourse.SubmittedByUsers = query.ToList().ToDictionary(u => u.UserLoginHistoryID, u => u.Name);
					}
				}
				return offeredCourse;
			}
		}
	}
}