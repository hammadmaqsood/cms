﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Core.CourseOffering.Faculty
{
	public static class OfferedCourses
	{
		public static List<short> GetOfferedSemesters(Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandFacultyActiveSession(loginSessionGuid);
				return aspireContext.OfferedCourses
					.Where(oc => oc.FacultyMemberID == loginHistory.FacultyMemberID)
					.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Select(oc => oc.SemesterID).Distinct()
					.OrderByDescending(s => s)
					.ToList();
			}
		}

		public sealed class OfferedCourse
		{
			internal OfferedCourse() { }
			public int OfferedCourseID { get; internal set; }
			public short SemesterID { get; internal set; }
			public string Semester => this.SemesterID.ToSemesterString();
			public string Title { get; internal set; }
			public decimal CreditHours { get; internal set; }
			public string CreditHoursFullName => this.CreditHours.ToCreditHoursFullName();
			public decimal ContactHours { get; internal set; }
			public string ContactHoursFullName => this.ContactHours.ToCreditHoursFullName();
			public string Program { get; internal set; }
			public SemesterNos SemesterNoEnum { get; internal set; }
			public Sections SectionEnum { get; internal set; }
			public Shifts ShiftEnum { get; internal set; }
			public string Class => AspireFormats.GetClassName(this.Program, this.SemesterNoEnum, this.SectionEnum, this.ShiftEnum);
			public bool Visiting { get; internal set; }
			public bool SpecialOffered { get; internal set; }
		}

		public static List<OfferedCourse> GetOfferedCourses(short? semesterID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandFacultyActiveSession(loginSessionGuid);
				return aspireContext.OfferedCourses
					.Where(oc => oc.FacultyMemberID == loginHistory.FacultyMemberID)
					.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == loginHistory.InstituteID)
					.Where(oc => semesterID == null || oc.SemesterID == semesterID.Value)
					.Select(oc => new OfferedCourse
					{
						OfferedCourseID = oc.OfferedCourseID,
						SemesterID = oc.SemesterID,
						Title = oc.Cours.Title,
						CreditHours = oc.Cours.CreditHours,
						ContactHours = oc.Cours.ContactHours,
						Program = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						SemesterNoEnum = (SemesterNos)oc.SemesterNo,
						SectionEnum = (Sections)oc.Section,
						ShiftEnum = (Shifts)oc.Shift,
						Visiting = oc.Visiting,
						SpecialOffered = oc.SpecialOffered
					}).ToList().OrderByDescending(oc => oc.SemesterID).OrderBy(oc => oc.Title).ThenBy(oc => oc.Class).ToList();
			}
		}
	}
}
