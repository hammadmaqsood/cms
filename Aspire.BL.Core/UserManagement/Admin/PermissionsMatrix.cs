﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.UserManagement.Admin
{
	public static class PermissionsMatrix
	{
		private static Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.UserManagement.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.UserManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		public sealed class Permission
		{
			internal Permission() { }
			public AspireModules ModuleEnum { get; internal set; }
			public string ModuleFullName => this.ModuleEnum.ToFullName();
			public UserGroupPermission.PermissionTypes? PermissionTypeEnum { get; internal set; }
			public string PermissionTypeFullName => this.PermissionTypeEnum?.ToFullName();
			public UserGroupPermission.PermissionValues? PermissionValueEnum { get; internal set; }
			public string PermissionValueFullName => this.PermissionValueEnum?.ToFullName();
			public int? UserGroupID { get; internal set; }
			public string GroupName { get; internal set; }
			public int? InstituteID { get; internal set; }
			public string InstituteAlias { get; internal set; }
			public int? UserID { get; internal set; }
			public int? UserRoleID { get; internal set; }
			public string Name { get; internal set; }
			public string JobTitle { get; internal set; }
			public bool? RoleExpired { get; internal set; }
			public User.Statuses? StatusEnum { get; internal set; }
			public string StatusFullName => this.StatusEnum?.ToFullName();
		}

		public static List<Permission> GetPermissionsMatrix(UserTypes userTypeEnum, int? instituteID, AspireModules? moduleEnum, UserGroupPermission.PermissionValues? permissionValueEnum, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				IQueryable<UserGroup> userGroupsQuery;
				IQueryable<UserRole> userRolesQuery = aspireContext.Users.Where(u => u.IsDeleted == false).SelectMany(u => u.UserRoles);
				switch (userTypeEnum)
				{
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.MasterAdmin:
					case UserTypes.Student:
					case UserTypes.Candidate:
					case UserTypes.Faculty:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					case UserTypes.Admin:
						if (loginHistory.IsMasterAdmin)
						{
							userGroupsQuery = aspireContext.UserGroups.Where(g => g.UserType == (byte)userTypeEnum && g.InstituteID == null && g.IsDeleted == false);
							userRolesQuery = userRolesQuery.Where(ur => ur.UserType == (byte)userTypeEnum);
							if (instituteID != null)
								userRolesQuery = userRolesQuery.Where(ur => ur.UserRoleInstitutes.Any(uri => uri.InstituteID == instituteID.Value));
						}
						else
						{
							userGroupsQuery = aspireContext.UserGroups.Where(g => false);
							userRolesQuery = userRolesQuery.Where(ur => false);
						}
						break;
					case UserTypes.Executive:
					case UserTypes.Staff:
						userGroupsQuery = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.UserGroups)
							.Where(g => g.UserType == (byte)userTypeEnum && g.IsDeleted == false);
						userRolesQuery = userRolesQuery.Where(ur => ur.UserType == (byte)userTypeEnum);
						if (instituteID != null)
						{
							userGroupsQuery = userGroupsQuery.Where(r => r.InstituteID == instituteID.Value);
							userRolesQuery = userRolesQuery.Where(r => r.InstituteID == instituteID.Value);
						}
						break;
					default:
						throw new NotImplementedEnumException(userTypeEnum);
				}

				var userGroups = userGroupsQuery.Select(g => new
				{
					g.UserGroupID,
					g.GroupName,
					g.InstituteID,
					g.Institute.InstituteAlias,
				}).ToList();

				var userGroupPermissions = userGroupsQuery.SelectMany(ug => ug.UserGroupPermissions)
					.Where(ugp => permissionValueEnum == null || ugp.PermissionValue == (byte)permissionValueEnum.Value).ToList();

				var userRoles = (from u in aspireContext.Users
								 join ur in userRolesQuery on u.UserID equals ur.UserID
								 select new
								 {
									 ur.UserGroupID,
									 ur.UserID,
									 u.Name,
									 u.JobTitle,
									 StatusEnum = (User.Statuses)u.Status,
									 ur.UserRoleID,
									 RoleExpired = ur.ExpiryDate <= DateTime.Now
								 }).ToList();

				var permissions = new List<Permission>();
				var permissionsList = userTypeEnum.GetPermissionsList();
				var modules = Enum.GetValues(typeof(AspireModules)).Cast<AspireModules>().Where(m => moduleEnum == null || m == moduleEnum.Value);
				foreach (var module in modules)
				{
					var permissionTypes = permissionsList.Where(p => p.ModuleEnum == module).ToList();
					if (!permissionTypes.Any())
						permissions.Add(new Permission { ModuleEnum = module });
					else
						foreach (var permissionType in permissionTypes)
						{
							var possiblePermissionValues = permissionType.PossiblePermissionValues.Where(ppv => permissionValueEnum == null || ppv == permissionValueEnum).ToList();
							if (!possiblePermissionValues.Any())
								permissions.Add(new Permission { ModuleEnum = module, PermissionTypeEnum = permissionType.PermissionTypeEnum });
							else
								foreach (var possiblePermissionValue in possiblePermissionValues)
								{
									var userGroupIDs = userGroupPermissions.Where(ugp => ugp.ModuleEnum == module && ugp.PermissionTypeEnum == permissionType.PermissionTypeEnum && ugp.PermissionValueEnum == possiblePermissionValue)
										.Select(ugp => ugp.UserGroupID).ToList();
									var groups = userGroups.Where(g => userGroupIDs.Contains(g.UserGroupID)).ToList();
									if (!groups.Any())
										permissions.Add(new Permission { ModuleEnum = module, PermissionTypeEnum = permissionType.PermissionTypeEnum, PermissionValueEnum = possiblePermissionValue });
									else
										foreach (var group in groups)
										{
											var groupUserRole = userRoles.Where(r => r.UserGroupID == group.UserGroupID).ToList();
											if (!groupUserRole.Any())
												permissions.Add(new Permission { ModuleEnum = module, PermissionTypeEnum = permissionType.PermissionTypeEnum, PermissionValueEnum = possiblePermissionValue, UserGroupID = group.UserGroupID, GroupName = group.GroupName, InstituteID = group.InstituteID, InstituteAlias = group.InstituteAlias });
											else
												foreach (var userRole in groupUserRole)
													permissions.Add(new Permission
													{
														ModuleEnum = module,
														PermissionTypeEnum = permissionType.PermissionTypeEnum,
														PermissionValueEnum = possiblePermissionValue,
														UserGroupID = group.UserGroupID,
														GroupName = group.GroupName,
														InstituteID = group.InstituteID,
														InstituteAlias = group.InstituteAlias,
														UserRoleID = userRole.UserRoleID,
														UserID = userRole.UserID,
														Name = userRole.Name,
														StatusEnum = userRole.StatusEnum,
														JobTitle = userRole.JobTitle,
														RoleExpired = userRole.RoleExpired
													});
										}
								}
						}
				}

				switch (sortExpression)
				{
					case nameof(Permission.ModuleFullName):
						return permissions.OrderBy(sortDirection, p => p.ModuleEnum)
							.ThenBy(sortDirection, p => p.PermissionTypeEnum)
							.ThenBy(sortDirection, p => p.PermissionValueEnum)
							.ToList();
					case nameof(Permission.GroupName):
						return permissions.OrderBy(sortDirection, p => p.GroupName)
							.ThenBy(sortDirection, p => p.UserGroupID)
							.ThenBy(sortDirection, p => p.ModuleEnum)
							.ThenBy(sortDirection, p => p.PermissionTypeEnum)
							.ThenBy(sortDirection, p => p.PermissionValueEnum)
							.ToList();
					case nameof(Permission.InstituteAlias):
						return permissions.OrderBy(sortDirection, p => p.InstituteAlias)
							.ThenBy(sortDirection, p => p.GroupName)
							.ThenBy(sortDirection, p => p.UserGroupID)
							.ThenBy(sortDirection, p => p.ModuleEnum)
							.ThenBy(sortDirection, p => p.PermissionTypeEnum)
							.ThenBy(sortDirection, p => p.PermissionValueEnum)
							.ToList();
					case nameof(Permission.Name):
						return permissions.OrderBy(sortDirection, p => p.Name)
							.ThenBy(sortDirection, p => p.UserRoleID)
							.ThenBy(sortDirection, p => p.GroupName)
							.ThenBy(sortDirection, p => p.ModuleEnum)
							.ThenBy(sortDirection, p => p.PermissionTypeEnum)
							.ThenBy(sortDirection, p => p.PermissionValueEnum)
							.ToList();
					case nameof(Permission.JobTitle):
						return permissions.OrderBy(sortDirection, p => p.JobTitle)
							.ThenBy(sortDirection, p => p.UserRoleID)
							.ThenBy(sortDirection, p => p.GroupName)
							.ThenBy(sortDirection, p => p.ModuleEnum)
							.ThenBy(sortDirection, p => p.PermissionTypeEnum)
							.ThenBy(sortDirection, p => p.PermissionValueEnum)
							.ToList();
					case nameof(Permission.StatusFullName):
						return permissions.OrderBy(sortDirection, p => p.StatusEnum)
							.ThenBy(sortDirection, p => p.UserRoleID)
							.ThenBy(sortDirection, p => p.GroupName)
							.ThenBy(sortDirection, p => p.ModuleEnum)
							.ThenBy(sortDirection, p => p.PermissionTypeEnum)
							.ThenBy(sortDirection, p => p.PermissionValueEnum)
							.ToList();
					case nameof(Permission.RoleExpired):
						return permissions.OrderBy(sortDirection, p => p.RoleExpired)
							.ThenBy(sortDirection, p => p.UserRoleID)
							.ThenBy(sortDirection, p => p.GroupName)
							.ThenBy(sortDirection, p => p.ModuleEnum)
							.ThenBy(sortDirection, p => p.PermissionTypeEnum)
							.ThenBy(sortDirection, p => p.PermissionValueEnum)
							.ToList();
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
			}
		}
	}
}
