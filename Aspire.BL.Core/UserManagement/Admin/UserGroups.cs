﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.UserManagement.Admin
{
	public static class UserGroups
	{
		private static Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.UserManagement.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.UserManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.UserManagement.ManageUserGroups, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.UserManagement.ManageUserGroups, UserGroupPermission.PermissionValues.Allowed);
		}

		private static IQueryable<UserGroup> ValidUserGroups(this AspireContext aspireContext)
		{
			return aspireContext.UserGroups.Where(g => g.IsDeleted == false);
		}

		private static List<UserGroupPermission> Validate(this List<UserGroupPermission> userGroupPermissionsList, UserTypes userTypeEnum)
		{
			var permissionsList = userTypeEnum.GetPermissionsList();
			foreach (var permissionType in permissionsList)
			{
				var userGroupPermission = userGroupPermissionsList.Find(p => p.Module == permissionType.Module && p.PermissionType == permissionType.PermissionType);
				if (userGroupPermission == null)
					throw new InvalidOperationException("Invalid set of User Group Permissions. Permissions not found.");
				if (!permissionType.PossiblePermissionValues.Contains(userGroupPermission.PermissionValueEnum))
					throw new InvalidOperationException("Invalid set of User Group Permissions. Invalid Permission Value.");
			}
			if (userGroupPermissionsList.Count != userGroupPermissionsList.Select(p => new { p.Module, p.PermissionType }).Distinct().Count())
				throw new InvalidOperationException("Invalid Permissions Set. Duplicate found.");
			foreach (var userGroupPermission in userGroupPermissionsList)
			{
				if (userGroupPermission.ModuleName != userGroupPermission.ModuleEnum.ToString())
					throw new InvalidOperationException("Invalid ModuleName");
				if (userGroupPermission.PermissionTypeName != userGroupPermission.PermissionTypeEnum.ToString())
					throw new InvalidOperationException("Invalid PermissionTypeName");
				if (userGroupPermission.PermissionValueName != userGroupPermission.PermissionValueEnum.ToString())
					throw new InvalidOperationException("Invalid PermissionValueName");
			}
			foreach (var userGroupPermission in userGroupPermissionsList)
			{
				UserGroupPermission.PermissionTypes modulePermissionTypeEnum;
				switch (userGroupPermission.ModuleEnum)
				{
					case AspireModules.None:
						throw new InvalidOperationException();
					case AspireModules.Administration:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.AdministrationModule;
						break;
					case AspireModules.Admissions:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.AdmissionsModule;
						break;
					case AspireModules.ClassAttendance:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.ClassAttendanceModule;
						break;
					case AspireModules.CourseOffering:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.CourseOfferingModule;
						break;
					case AspireModules.CourseRegistration:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.CourseRegistrationModule;
						break;
					case AspireModules.Courses:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.CoursesModule;
						break;
					case AspireModules.Exams:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.ExamsModule;
						break;
					case AspireModules.ExamSeatingPlan:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.ExamSeatingPlanModule;
						break;
					case AspireModules.FacultyManagement:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.FacultyManagementModule;
						break;
					case AspireModules.FeeManagement:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.FeeManagementModule;
						break;
					case AspireModules.QualityAssurance:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.QualityAssuranceModule;
						break;
					case AspireModules.StudentInformation:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.StudentInformationModule;
						break;
					case AspireModules.UserManagement:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.UserManagementModule;
						break;
					case AspireModules.Scholarship:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.ScholarshipModule;
						break;
					case AspireModules.Pgp:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.PgpModule;
						break;
					case AspireModules.LibraryManagement:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.LibraryManagementModule;
						break;
					case AspireModules.OfficialDocuments:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.OfficialDocumentsModule;
						break;
					case AspireModules.Complaints:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.ComplaintsModule;
						break;
					case AspireModules.LMS:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.LMSModule;
						break;
					case AspireModules.Feedbacks:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.FeedbacksModule;
						break;
					case AspireModules.Alumni:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.AlumniModule;
						break;
					case AspireModules.Forms:
						modulePermissionTypeEnum = UserGroupPermission.PermissionTypes.Forms;
						break;
					case AspireModules.Logs:
					case AspireModules.FileServer:
					case AspireModules.TimeTable:
						throw new InvalidOperationException();
					default:
						throw new NotImplementedEnumException(userGroupPermission.ModuleEnum);
				}
				var moduleUserGroupPermission = userGroupPermissionsList.Single(ugp => ugp.ModuleEnum == userGroupPermission.ModuleEnum && ugp.PermissionTypeEnum == modulePermissionTypeEnum);
				if (userGroupPermission != moduleUserGroupPermission && moduleUserGroupPermission.PermissionValueEnum != UserGroupPermission.PermissionValues.Allowed)
					userGroupPermission.PermissionValueEnum = UserGroupPermission.PermissionValues.Denied;
			}
			return userGroupPermissionsList;
		}

		public class AddUserGroupResult
		{
			public enum Statuses
			{
				GroupNameAlreadyExists,
				Success
			}

			internal AddUserGroupResult() { }
			public Statuses Status { get; internal set; }
			public UserGroup UserGroup { get; internal set; }
		}

		public static AddUserGroupResult AddUserGroup(UserTypes userTypeEnum, string groupName, int? instituteID, List<UserGroupPermission> userGroupPermissionsList, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				groupName = groupName.TrimAndCannotBeEmpty();
				var userType = userTypeEnum.ToShort();
				UserGroup userGroup;
				int userLoginHistoryID;
				switch (userTypeEnum)
				{
					case UserTypes.Admin:
						userLoginHistoryID = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid).UserLoginHistoryID;
						if (instituteID != null)
							throw new ArgumentException("Must be null.", nameof(instituteID));
						if (aspireContext.ValidUserGroups().Any(g => g.GroupName == groupName && g.UserType == userType))
							return new AddUserGroupResult { Status = AddUserGroupResult.Statuses.GroupNameAlreadyExists };
						userGroup = new UserGroup
						{
							UserTypeEnum = userTypeEnum,
							InstituteID = null,
							GroupName = groupName,
						};
						break;
					case UserTypes.Staff:
						if (instituteID == null)
							throw new ArgumentNullException(nameof(instituteID));
						userLoginHistoryID = aspireContext.DemandPermissions(instituteID.Value, loginSessionGuid).UserLoginHistoryID;
						if (aspireContext.ValidUserGroups().Any(g => g.GroupName == groupName && g.UserType == userType && g.InstituteID == instituteID.Value))
							return new AddUserGroupResult { Status = AddUserGroupResult.Statuses.GroupNameAlreadyExists };
						userGroup = new UserGroup
						{
							UserTypeEnum = userTypeEnum,
							InstituteID = instituteID.Value,
							GroupName = groupName,
						};
						break;
					case UserTypes.Executive:
						if (instituteID == null)
							throw new ArgumentNullException(nameof(instituteID));
						userLoginHistoryID = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid).UserLoginHistoryID;
						if (aspireContext.ValidUserGroups().Any(g => g.GroupName == groupName && g.UserType == userType && g.InstituteID == instituteID.Value))
							return new AddUserGroupResult { Status = AddUserGroupResult.Statuses.GroupNameAlreadyExists };
						userGroup = new UserGroup
						{
							UserTypeEnum = userTypeEnum,
							InstituteID = instituteID.Value,
							GroupName = groupName,
						};
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(userTypeEnum), userTypeEnum, null);
				}
				userGroup.UserGroupPermissions = userGroupPermissionsList.Validate(userGroup.UserTypeEnum);
				aspireContext.UserGroups.Add(userGroup);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return new AddUserGroupResult { Status = AddUserGroupResult.Statuses.Success, UserGroup = userGroup };
			}
		}

		public class UpdateUserGroupResult
		{
			public enum Statuses
			{
				GroupNameAlreadyExists,
				Success,
				NoRecordFound
			}

			internal UpdateUserGroupResult() { }
			public Statuses Status { get; internal set; }
			public UserGroup UserGroup { get; internal set; }
		}

		public static UpdateUserGroupResult UpdateUserGroup(int userGroupID, string groupName, List<UserGroupPermission> userGroupPermissionsList, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				int userLoginHistoryID;
				groupName = groupName.TrimAndCannotBeEmpty();
				var userGroup = aspireContext.ValidUserGroups().Include(g => g.UserGroupPermissions).SingleOrDefault(g => g.UserGroupID == userGroupID);
				if (userGroup == null)
					return new UpdateUserGroupResult { Status = UpdateUserGroupResult.Statuses.NoRecordFound };
				switch (userGroup.UserTypeEnum)
				{
					case UserTypes.Admin:
						userLoginHistoryID = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid).UserLoginHistoryID;
						if (aspireContext.ValidUserGroups().Any(g => g.UserGroupID != userGroup.UserGroupID && g.GroupName == groupName && g.UserType == userGroup.UserType))
							return new UpdateUserGroupResult { Status = UpdateUserGroupResult.Statuses.GroupNameAlreadyExists };
						userGroup.GroupName = groupName;
						break;
					case UserTypes.Staff:
						userLoginHistoryID = aspireContext.DemandPermissions(userGroup.InstituteID, loginSessionGuid).UserLoginHistoryID;
						if (aspireContext.ValidUserGroups().Any(g => g.UserGroupID != userGroup.UserGroupID && g.GroupName == groupName && g.UserType == userGroup.UserType && g.InstituteID == userGroup.InstituteID))
							return new UpdateUserGroupResult { Status = UpdateUserGroupResult.Statuses.GroupNameAlreadyExists };
						userGroup.GroupName = groupName;
						break;
					case UserTypes.Executive:
						userLoginHistoryID = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid).UserLoginHistoryID;
						if (aspireContext.ValidUserGroups().Any(g => g.UserGroupID != userGroup.UserGroupID && g.GroupName == groupName && g.UserType == userGroup.UserType && g.InstituteID == userGroup.InstituteID))
							return new UpdateUserGroupResult { Status = UpdateUserGroupResult.Statuses.GroupNameAlreadyExists };
						userGroup.GroupName = groupName;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				userGroupPermissionsList = userGroupPermissionsList.Validate(userGroup.UserTypeEnum);

				foreach (var userGroupPermission in userGroup.UserGroupPermissions.ToList())
				{
					var newUserGroupPermission = userGroupPermissionsList.Find(g => g.Module == userGroupPermission.Module && g.PermissionType == userGroupPermission.PermissionType);
					if (newUserGroupPermission == null)
						aspireContext.UserGroupPermissions.Remove(userGroupPermission);
					else
					{
						userGroupPermission.PermissionValue = newUserGroupPermission.PermissionValue;
						userGroupPermission.PermissionValueName = newUserGroupPermission.PermissionValueName;
						userGroupPermissionsList.Remove(newUserGroupPermission);
					}
				}
				userGroupPermissionsList.ForEach(p => p.UserGroupID = userGroup.UserGroupID);
				aspireContext.UserGroupPermissions.AddRange(userGroupPermissionsList);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return new UpdateUserGroupResult { Status = UpdateUserGroupResult.Statuses.Success, UserGroup = userGroup };
			}
		}

		public enum DeleteUserGroupStatus
		{
			NoRecordFound,
			CannotDeleteChildRecordExists,
			Success,
		}

		public static DeleteUserGroupStatus DeleteUserGroup(int userGroupID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var userGroup = aspireContext.ValidUserGroups().Include(g => g.UserGroupPermissions).Include(g => g.UserGroupMenuLinks).FirstOrDefault(g => g.UserGroupID == userGroupID);
				if (userGroup == null)
					return DeleteUserGroupStatus.NoRecordFound;
				int userLoginHistoryID;
				if (userGroup.UserTypeEnum.IsAdmin())
					userLoginHistoryID = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid).UserLoginHistoryID;
				else if (userGroup.UserTypeEnum.IsStaff())
					userLoginHistoryID = aspireContext.DemandPermissions(userGroup.InstituteID, loginSessionGuid).UserLoginHistoryID;
				else
					throw new InvalidOperationException();

				if (userGroup.UserRoles.Any(r => r.IsDeleted == false))
					return DeleteUserGroupStatus.CannotDeleteChildRecordExists;

				if (userGroup.UserRoles.Any())
					userGroup.IsDeleted = true;
				else
				{
					aspireContext.UserGroupPermissions.RemoveRange(userGroup.UserGroupPermissions);
					aspireContext.UserGroupMenuLinks.RemoveRange(userGroup.UserGroupMenuLinks);
					aspireContext.UserGroups.Remove(userGroup);
				}
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return DeleteUserGroupStatus.Success;
			}
		}

		public sealed class CustomUserGroup
		{
			internal CustomUserGroup() { }
			public string GroupName { get; internal set; }
			public string InstituteAlias { get; internal set; }
			public int Members { get; internal set; }
			public int UserGroupID { get; internal set; }
			public short UserType { get; internal set; }
			public UserTypes UserTypeEnum => (UserTypes)this.UserType;
		}

		public static List<CustomUserGroup> GetUserGroups(int? instituteID, UserTypes? usertypeEnum, string searchText, int pageIndex, int pageSize, out int virtualItemCount, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(instituteID, loginSessionGuid);
				IQueryable<UserGroup> userGroups;
				if (loginHistory.IsMasterAdmin)
					userGroups = aspireContext.ValidUserGroups().AsQueryable();
				else
					userGroups = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.UserGroups).Where(g => g.IsDeleted == false && g.UserType == UserTypeConstants.Staff);

				if (instituteID != null)
					userGroups = userGroups.Where(g => g.InstituteID == instituteID);
				var usertype = (short?)usertypeEnum;
				if (usertype != null)
					userGroups = userGroups.Where(g => g.UserType == usertype);
				if (!string.IsNullOrWhiteSpace(searchText))
					userGroups = userGroups.Where(g => g.GroupName.Contains(searchText));
				virtualItemCount = userGroups.Count();
				switch (sortExpression)
				{
					case nameof(UserGroup.GroupName):
						userGroups = userGroups.OrderBy(sortDirection, g => g.GroupName);
						break;
					case nameof(UserGroup.UserType):
						userGroups = userGroups.OrderBy(sortDirection, g => g.UserType);
						break;
					case nameof(Institute.InstituteAlias):
						userGroups = userGroups.OrderBy(sortDirection, g => g.Institute.InstituteAlias);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				userGroups = userGroups.Skip(pageIndex * pageSize).Take(pageSize);
				return userGroups.Select(g => new CustomUserGroup
				{
					UserGroupID = g.UserGroupID,
					GroupName = g.GroupName,
					InstituteAlias = g.Institute.InstituteAlias,
					UserType = g.UserType,
					Members = g.UserRoles.Count(r => r.IsDeleted == false && r.User1.IsDeleted == false),
				}).ToList();
			}
		}

		public enum AddUserGroupMenuLinkStatuses
		{
			NoRecordFound,
			Success
		}

		private static IQueryable<UserGroup> GetUserGroups(this AspireContext aspireContext, Common.Permissions.Admin.AdminLoginHistory loginHistory)
		{
			if (loginHistory.IsMasterAdmin)
				return aspireContext.ValidUserGroups();
			return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.UserGroups).Where(g => g.IsDeleted == false && g.UserType == UserTypeConstants.Staff);
		}

		public static AddUserGroupMenuLinkStatuses AddUserGroupMenuLink(int userGroupID, Guid? aspirePageGuid, string linkText, string linkUrl, int? parentUserGroupMenuLinkID, bool visible, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var userGroup = aspireContext.GetUserGroups(loginHistory).SingleOrDefault(g => g.UserGroupID == userGroupID);
				if (userGroup == null)
					return AddUserGroupMenuLinkStatuses.NoRecordFound;

				var userGroupMenuLink = aspireContext.UserGroupMenuLinks.Add(new UserGroupMenuLink
				{
					UserGroupID = userGroupID,
					AspirePageGuid = aspirePageGuid,
					DisplayIndex = byte.MaxValue,
					LinkText = linkText?.Trim(),
					LinkUrl = linkUrl,
					ParentUserGroupMenuLinkID = userGroup.UserGroupMenuLinks.SingleOrDefault(g => g.UserGroupMenuLinkID == parentUserGroupMenuLinkID)?.UserGroupMenuLinkID,
					Visible = visible,
				});

				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ReorderUserGroupMenuLink(userGroupID, userGroupMenuLink.ParentUserGroupMenuLinkID);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddUserGroupMenuLinkStatuses.Success;
			}
		}

		private static void ReorderUserGroupMenuLink(this AspireContext aspireContext, int userGroupID, int? parentUserGroupLinkID)
		{
			var links = aspireContext.UserGroupMenuLinks.Where(m => m.UserGroupID == userGroupID && m.ParentUserGroupMenuLinkID == parentUserGroupLinkID).OrderBy(m => m.DisplayIndex).ToList();
			if (!links.Any())
				return;
			byte displayIndex = 0;
			foreach (var userGroupMenuLink in links)
				userGroupMenuLink.DisplayIndex = displayIndex++;
		}

		public enum DeleteUserGroupMenuLinkStatuses
		{
			NoRecordFound,
			CannotDeleteChildRecordExists,
			Success
		}

		private static void RemoveUserGroupMenuLinks(this AspireContext aspireContext, UserGroupMenuLink userGroupMenuLink)
		{
			aspireContext.UserGroupMenuLinks
				.Where(ugml => ugml.UserGroupID == userGroupMenuLink.UserGroupID && ugml.ParentUserGroupMenuLinkID == userGroupMenuLink.UserGroupMenuLinkID)
				.ToList()
				.ForEach(aspireContext.RemoveUserGroupMenuLinks);
			aspireContext.UserGroupMenuLinks.Remove(userGroupMenuLink);
		}

		public static DeleteUserGroupMenuLinkStatuses DeleteUserGroupMenuLink(int userGroupMenuLinkID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var userGroupMenuLink = aspireContext.GetUserGroups(loginHistory).SelectMany(g => g.UserGroupMenuLinks).SingleOrDefault(g => g.UserGroupMenuLinkID == userGroupMenuLinkID);
				if (userGroupMenuLink == null)
					return DeleteUserGroupMenuLinkStatuses.NoRecordFound;
				var userGroupID = userGroupMenuLink.UserGroupID;
				var parentUserGroupMenuLinkID = userGroupMenuLink.ParentUserGroupMenuLinkID;
				aspireContext.RemoveUserGroupMenuLinks(userGroupMenuLink);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ReorderUserGroupMenuLink(userGroupID, parentUserGroupMenuLinkID);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteUserGroupMenuLinkStatuses.Success;
			}
		}

		public enum MoveUserGroupMenuLinkStatuses
		{
			NoRecordFound,
			AlreadyAtTop,
			Success,
			AlreadyAtBottom
		}

		public static MoveUserGroupMenuLinkStatuses MoveUpUserGroupMenuLink(int userGroupMenuLinkID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var userGroupMenuLink = aspireContext.GetUserGroups(loginHistory).SelectMany(g => g.UserGroupMenuLinks).SingleOrDefault(g => g.UserGroupMenuLinkID == userGroupMenuLinkID);
				if (userGroupMenuLink == null)
					return MoveUserGroupMenuLinkStatuses.NoRecordFound;

				var lowerUserGroupMenuLink = aspireContext.UserGroupMenuLinks.SingleOrDefault(m => m.UserGroupMenuLinkID == userGroupMenuLinkID);
				if (lowerUserGroupMenuLink == null || lowerUserGroupMenuLink.DisplayIndex <= byte.MinValue)
					return MoveUserGroupMenuLinkStatuses.AlreadyAtTop;

				var upperUserGroupMenuLink = aspireContext.UserGroupMenuLinks.SingleOrDefault(m => m.DisplayIndex == lowerUserGroupMenuLink.DisplayIndex - 1 && m.UserGroupID == lowerUserGroupMenuLink.UserGroupID && m.ParentUserGroupMenuLinkID == lowerUserGroupMenuLink.ParentUserGroupMenuLinkID);
				if (upperUserGroupMenuLink != null)
					upperUserGroupMenuLink.DisplayIndex++;
				lowerUserGroupMenuLink.DisplayIndex--;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ReorderUserGroupMenuLink(lowerUserGroupMenuLink.UserGroupID, lowerUserGroupMenuLink.ParentUserGroupMenuLinkID);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return MoveUserGroupMenuLinkStatuses.Success;
			}
		}

		public static MoveUserGroupMenuLinkStatuses MoveDownUserGroupMenuLink(int userGroupMenuLinkID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var userGroupMenuLink = aspireContext.GetUserGroups(loginHistory).SelectMany(g => g.UserGroupMenuLinks).SingleOrDefault(g => g.UserGroupMenuLinkID == userGroupMenuLinkID);
				if (userGroupMenuLink == null)
					return MoveUserGroupMenuLinkStatuses.NoRecordFound;

				var upperUserGroupMenuLink = aspireContext.UserGroupMenuLinks.SingleOrDefault(m => m.UserGroupMenuLinkID == userGroupMenuLinkID);
				if (upperUserGroupMenuLink == null || upperUserGroupMenuLink.DisplayIndex >= byte.MaxValue)
					return MoveUserGroupMenuLinkStatuses.AlreadyAtBottom;

				var lowerUserGroupMenuLink = aspireContext.UserGroupMenuLinks.SingleOrDefault(m => m.DisplayIndex == upperUserGroupMenuLink.DisplayIndex + 1 && m.UserGroupID == upperUserGroupMenuLink.UserGroupID && m.ParentUserGroupMenuLinkID == upperUserGroupMenuLink.ParentUserGroupMenuLinkID);
				if (lowerUserGroupMenuLink == null)
					return MoveUserGroupMenuLinkStatuses.AlreadyAtBottom;

				lowerUserGroupMenuLink.DisplayIndex--;
				upperUserGroupMenuLink.DisplayIndex++;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.ReorderUserGroupMenuLink(upperUserGroupMenuLink.UserGroupID, upperUserGroupMenuLink.ParentUserGroupMenuLinkID);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return MoveUserGroupMenuLinkStatuses.Success;
			}
		}

		public enum ResetToDefaultUserGroupMenuLinksStatuses
		{
			NoRecordFound,
			Success,
		}

		public static ResetToDefaultUserGroupMenuLinksStatuses ResetToDefaultUserGroupMenuLinks(int userGroupID, List<UserGroupMenuLink> userGroupMenuLinks, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var userGroup = aspireContext.GetUserGroups(loginHistory).SingleOrDefault(g => g.UserGroupID == userGroupID);
				if (userGroup == null)
					return ResetToDefaultUserGroupMenuLinksStatuses.NoRecordFound;

				aspireContext.UserGroupMenuLinks.RemoveRange(userGroup.UserGroupMenuLinks);
				userGroupMenuLinks.ForEach(l => l.UserGroupMenuLinkID = 0);
				userGroupMenuLinks.ForEach(l => l.ParentUserGroupMenuLinkID = null);
				foreach (var userGroupMenuLink in userGroupMenuLinks)
					userGroup.UserGroupMenuLinks.Add(userGroupMenuLink);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return ResetToDefaultUserGroupMenuLinksStatuses.Success;
			}
		}

		public enum ToggleVisibilityUserGroupMenuLinkStatuses
		{
			NoRecordFound,
			Success
		}

		public static ToggleVisibilityUserGroupMenuLinkStatuses ToggleVisibilityUserGroupMenuLink(int userGroupMenuLinkID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var userGroupMenuLink = aspireContext.GetUserGroups(loginHistory).SelectMany(g => g.UserGroupMenuLinks).SingleOrDefault(g => g.UserGroupMenuLinkID == userGroupMenuLinkID);
				if (userGroupMenuLink == null)
					return ToggleVisibilityUserGroupMenuLinkStatuses.NoRecordFound;

				userGroupMenuLink.Visible = !userGroupMenuLink.Visible;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return ToggleVisibilityUserGroupMenuLinkStatuses.Success;
			}
		}

		public sealed class GetUserGroupWithMenuLinksResult
		{
			public enum Statuses
			{
				Success,
				NoRecordFound,
			}
			internal GetUserGroupWithMenuLinksResult() { }
			public Statuses Status { get; }
			public string InstituteAlias { get; }
			public UserGroup UserGroup { get; }

			public GetUserGroupWithMenuLinksResult(Statuses status)
			{
				if (status == Statuses.Success)
					throw new ArgumentException(nameof(status));
				this.Status = status;
			}

			public GetUserGroupWithMenuLinksResult(UserGroup userGroup, string instituteAlias)
			{
				this.UserGroup = userGroup;
				this.InstituteAlias = instituteAlias;
				this.Status = Statuses.Success;
			}
		}

		public static Model.Entities.UserGroup GetUserGroup(int userGroupID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				if (loginHistory.IsMasterAdmin)
					return aspireContext.ValidUserGroups().SingleOrDefault(g => g.UserGroupID == userGroupID);
				return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.UserGroups).SingleOrDefault(g => g.UserGroupID == userGroupID);
			}
		}

		// Temporary Add this function to Get group for adding Groups by script of Create Users.
		public static UserGroup GetUserGroup(int instituteID, UserTypes userTypeEnum, string groupName, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				if (loginHistory.IsMasterAdmin)
					return aspireContext.ValidUserGroups().SingleOrDefault(g => g.InstituteID == instituteID && g.UserType == (short)userTypeEnum && g.GroupName == groupName);
				return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.UserGroups).SingleOrDefault(g => g.InstituteID == instituteID && g.UserType == (short)userTypeEnum && g.GroupName == groupName);
			}
		}

		public static List<Model.Entities.UserGroupPermission> GetUserGroupPermissions(int userGroupID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var userGroup = aspireContext.ValidUserGroups().Where(g => g.UserGroupID == userGroupID).Select(g => new { g.InstituteID, g.UserType, UserGroupPermissions = g.UserGroupPermissions.ToList() }).SingleOrDefault();
				if (userGroup == null)
					return null;
				switch ((UserTypes)userGroup.UserType)
				{
					case UserTypes.Admin:
						aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
						break;
					case UserTypes.Executive:
					case UserTypes.Staff:
						aspireContext.DemandModulePermissions(userGroup.InstituteID, loginSessionGuid);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				return userGroup.UserGroupPermissions;
			}
		}

		public static GetUserGroupWithMenuLinksResult GetUserGroupWithMenuLinks(int userGroupID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				var userGroup = aspireContext.GetUserGroups(loginHistory).SingleOrDefault(g => g.UserGroupID == userGroupID);
				if (userGroup == null)
					return new GetUserGroupWithMenuLinksResult(GetUserGroupWithMenuLinksResult.Statuses.NoRecordFound);
				var instituteAlias = userGroup.Institute?.InstituteAlias;
				aspireContext.Entry(userGroup).Collection(g => g.UserGroupMenuLinks).Load();
				return new GetUserGroupWithMenuLinksResult(userGroup, instituteAlias);
			}
		}

		public static List<CustomListItem> GetUserGroupsList(UserTypes userTypeEnum, int? instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var userType = (short)userTypeEnum;
				var userGroups = aspireContext.ValidUserGroups().Where(g => g.UserType == userType);

				switch (userTypeEnum)
				{
					case UserTypes.Admin:
						if (instituteID != null)
							throw new ArgumentException("Must be null.", nameof(instituteID));
						aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
						break;
					case UserTypes.Executive:
						if (instituteID == null)
							throw new ArgumentNullException(nameof(instituteID));
						aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
						userGroups = userGroups.Where(g => g.InstituteID == instituteID.Value);
						break;
					case UserTypes.Staff:
						if (instituteID == null)
							throw new ArgumentNullException(nameof(instituteID));
						aspireContext.DemandModulePermissions(instituteID.Value, loginSessionGuid);
						userGroups = userGroups.Where(g => g.InstituteID == instituteID.Value);
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(userTypeEnum), userTypeEnum, null);
				}
				return userGroups.Select(g => new CustomListItem
				{
					ID = g.UserGroupID,
					Text = g.GroupName
				}).OrderBy(g => g.Text).ToList();
			}
		}

		public static void ValidateAndCorrectUserGroupPermissions()
		{
			using (var aspireContext = new AspireContext(true))
			{
				var permissionsToBeAdded = new List<UserGroupPermission>();
				var permissionsToBeDeleted = new List<UserGroupPermission>();
				var userGroupPermissions = aspireContext.UserGroupPermissions.ToList();
				foreach (var userGroupPermission in userGroupPermissions.Where(ugp => !ugp.TryValidate()))
				{
					permissionsToBeDeleted.Add(userGroupPermission);
					permissionsToBeAdded.Add(userGroupPermission.Clone());
				}
				if (permissionsToBeDeleted.Any())
				{
					aspireContext.UserGroupPermissions.RemoveRange(permissionsToBeDeleted);
					aspireContext.SaveChangesAsSystemUser();
				}
				if (permissionsToBeAdded.Any())
				{
					aspireContext.UserGroupPermissions.AddRange(permissionsToBeAdded);
					aspireContext.SaveChangesAsSystemUser();
				}
				if (permissionsToBeDeleted.Any() || permissionsToBeAdded.Any())
					aspireContext.CommitTransaction();
			}
		}
	}
}

