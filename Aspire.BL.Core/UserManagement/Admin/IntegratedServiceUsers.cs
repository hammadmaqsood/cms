﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.UserManagement.Admin
{
	public static class IntegratedServiceUsers
	{
		public sealed class IntegratedServiceUser
		{
			internal IntegratedServiceUser() { }
			public int IntegratedServiceUserID { get; internal set; }
			public string InstituteAlias { get; internal set; }
			public string ServiceName { get; internal set; }
			public byte ServiceType { get; internal set; }
			public Guid UserName { get; internal set; }
			public string IPAddress { get; internal set; }
			public DateTime CreatedDate { get; internal set; }
			public DateTime ExpiryDate { get; internal set; }
			public byte Status { get; internal set; }
			public Model.Entities.IntegratedServiceUser.Statuses StatusEnum => (Model.Entities.IntegratedServiceUser.Statuses)this.Status;
			public string StatusFullName => this.StatusEnum.ToFullName();
			public Model.Entities.IntegratedServiceUser.ServiceTypes ServiceTypeEnum => (Model.Entities.IntegratedServiceUser.ServiceTypes)this.ServiceType;
			public string ServiceTypeFullName => this.ServiceTypeEnum.ToFullName();
			public int? InstituteID { get; internal set; }
			public string Institute => this.InstituteAlias ?? "All";
		}

		public enum AddIntegratedServiceUserStatuses
		{
			Success,
			AlreadyExists,
		}

		public static AddIntegratedServiceUserStatuses AddIntegratedServiceUser(string serviceName, Model.Entities.IntegratedServiceUser.ServiceTypes serviceTypeEnum, int? instituteID, Guid userName, Guid password, IPAddress ipAddress, DateTime expiryDate, Model.Entities.IntegratedServiceUser.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				var integratedServiceUser = new Model.Entities.IntegratedServiceUser
				{
					InstituteID = instituteID,
					StatusEnum = statusEnum,
					CreatedDate = DateTime.Now,
					ExpiryDate = expiryDate,
					IPAddress = ipAddress.ToString(),
					IsDeleted = false,
					Password = password.CannotBeDefaultOrEmpty().ToString().EncryptPassword(),
					ServiceName = serviceName,
					ServiceTypeEnum = serviceTypeEnum,
					UserName = userName,
				}.Validate(true);
				if (aspireContext.IntegratedServiceUsers.Any(isu => isu.UserName == integratedServiceUser.UserName && isu.IsDeleted == false))
					return AddIntegratedServiceUserStatuses.AlreadyExists;
				aspireContext.IntegratedServiceUsers.Add(integratedServiceUser);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddIntegratedServiceUserStatuses.Success;
			}
		}

		public enum UpdateIntegratedServiceUserStatuses
		{
			NoRecordFound,
			UsernameAlreadyExists,
			Success,
		}

		public static UpdateIntegratedServiceUserStatuses UpdateIntegratedServiceUser(int integratedServiceUserID, Guid? password, string serviceName, IPAddress ipAddress, DateTime expiryDate, Model.Entities.IntegratedServiceUser.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				var integratedServiceUser = aspireContext.IntegratedServiceUsers.SingleOrDefault(isu => isu.IntegratedServiceUserID == integratedServiceUserID && isu.IsDeleted == false);
				if (integratedServiceUser == null)
					return UpdateIntegratedServiceUserStatuses.NoRecordFound;

				if (password != null)
					integratedServiceUser.Password = password.Value.CannotBeDefaultOrEmpty().ToString().EncryptPassword();
				integratedServiceUser.ServiceName = serviceName;
				integratedServiceUser.IPAddress = ipAddress.ToString();
				integratedServiceUser.ExpiryDate = expiryDate;
				integratedServiceUser.StatusEnum = statusEnum;
				integratedServiceUser.Validate(false);

				if (aspireContext.IntegratedServiceUsers.Any(u => u.UserName == integratedServiceUser.UserName && u.IntegratedServiceUserID != integratedServiceUser.IntegratedServiceUserID && u.IsDeleted == false))
					return UpdateIntegratedServiceUserStatuses.UsernameAlreadyExists;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateIntegratedServiceUserStatuses.Success;
			}
		}

		public static UpdateIntegratedServiceUserStatuses ToggleIntegratedServiceUserStatus(int integratedServiceUserID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				var integratedServiceUser = aspireContext.IntegratedServiceUsers.SingleOrDefault(isu => isu.IntegratedServiceUserID == integratedServiceUserID && isu.IsDeleted == false);
				if (integratedServiceUser == null)
					return UpdateIntegratedServiceUserStatuses.NoRecordFound;

				switch (integratedServiceUser.StatusEnum)
				{
					case Model.Entities.IntegratedServiceUser.Statuses.Active:
						integratedServiceUser.StatusEnum = Model.Entities.IntegratedServiceUser.Statuses.Inactive;
						break;
					case Model.Entities.IntegratedServiceUser.Statuses.Inactive:
						integratedServiceUser.StatusEnum = Model.Entities.IntegratedServiceUser.Statuses.Active;
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(integratedServiceUser.StatusEnum), integratedServiceUser.StatusEnum, null);
				}

				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateIntegratedServiceUserStatuses.Success;
			}
		}

		public enum DeleteIntegratedServiceUserStatuses
		{
			NoRecordFound,
			Success,
		}

		public static DeleteIntegratedServiceUserStatuses DeleteIntegratedServiceUser(int integratedServiceUserID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				var integratedServiceUser = aspireContext.IntegratedServiceUsers.SingleOrDefault(isu => isu.IntegratedServiceUserID == integratedServiceUserID && isu.IsDeleted == false);
				if (integratedServiceUser == null)
					return DeleteIntegratedServiceUserStatuses.NoRecordFound;
				integratedServiceUser.IsDeleted = true;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteIntegratedServiceUserStatuses.Success;
			}
		}

		public static IntegratedServiceUser GetIntegratedServiceUser(int integratedServiceUserID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				return aspireContext.IntegratedServiceUsers.Where(isu => isu.IntegratedServiceUserID == integratedServiceUserID && isu.IsDeleted == false).Select(isu => new IntegratedServiceUser
				{
					Status = isu.Status,
					InstituteID = isu.InstituteID,
					InstituteAlias = isu.Institute.InstituteAlias,
					CreatedDate = isu.CreatedDate,
					UserName = isu.UserName,
					ExpiryDate = isu.ExpiryDate,
					IntegratedServiceUserID = isu.IntegratedServiceUserID,
					IPAddress = isu.IPAddress,
					ServiceName = isu.ServiceName,
					ServiceType = isu.ServiceType,
				}).SingleOrDefault();
			}
		}

		public static List<IntegratedServiceUser> GetIntegratedServiceUsers(int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid, out int virtualItemCount)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				var query = aspireContext.IntegratedServiceUsers.Where(isu => isu.IsDeleted == false);
				switch (sortExpression)
				{
					case "InstituteAlias":
						query = SortDirection.Ascending == sortDirection ? query.OrderBy(q => q.Institute.InstituteAlias) : query.OrderByDescending(q => q.Institute.InstituteAlias);
						break;
					case "ServiceName":
						query = SortDirection.Ascending == sortDirection ? query.OrderBy(q => q.ServiceName) : query.OrderByDescending(q => q.ServiceName);
						break;
					case "ServiceType":
						query = SortDirection.Ascending == sortDirection ? query.OrderBy(q => q.ServiceType) : query.OrderByDescending(q => q.ServiceType);
						break;
					case "UserName":
						query = SortDirection.Ascending == sortDirection ? query.OrderBy(q => q.UserName) : query.OrderByDescending(q => q.UserName);
						break;
					case "IPAddress":
						query = SortDirection.Ascending == sortDirection ? query.OrderBy(q => q.IPAddress) : query.OrderByDescending(q => q.IPAddress);
						break;
					case "CreatedDate":
						query = SortDirection.Ascending == sortDirection ? query.OrderBy(q => q.CreatedDate) : query.OrderByDescending(q => q.CreatedDate);
						break;
					case "ExpiryDate":
						query = SortDirection.Ascending == sortDirection ? query.OrderBy(q => q.ExpiryDate) : query.OrderByDescending(q => q.ExpiryDate);
						break;
					case "Status":
						query = SortDirection.Ascending == sortDirection ? query.OrderBy(q => q.Status) : query.OrderByDescending(q => q.Status);
						break;

				}
				virtualItemCount = query.Count();
				query = query.Skip(pageIndex * pageSize).Take(pageSize);
				return query.Select(isu => new IntegratedServiceUser
				{
					IntegratedServiceUserID = isu.IntegratedServiceUserID,
					InstituteID = isu.InstituteID,
					ServiceName = isu.ServiceName,
					InstituteAlias = isu.Institute.InstituteAlias,
					UserName = isu.UserName,
					CreatedDate = isu.CreatedDate,
					ExpiryDate = isu.ExpiryDate,
					IPAddress = isu.IPAddress,
					ServiceType = isu.ServiceType,
					Status = isu.Status,
				}).ToList();
			}
		}
	}
}
