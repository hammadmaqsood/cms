﻿using Aspire.BL.Core.Common;
using Aspire.BL.Core.Common.EmailsManagement;
using Aspire.BL.Core.Common.Permissions;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security;
using System.Web.Security;
using System.Web.UI.WebControls;

namespace Aspire.BL.Core.UserManagement.Admin
{
	public static class Users
	{
		private static Common.Permissions.Admin.AdminLoginHistory DemandModulePermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.UserManagement.Module, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.UserManagement.Module, UserGroupPermission.PermissionValues.Allowed);
		}

		private static Common.Permissions.Admin.AdminLoginHistory DemandPermissions(this AspireContext aspireContext, int? instituteID, Guid loginSessionGuid)
		{
			if (instituteID == null)
				return aspireContext.DemandAdminPermissions(loginSessionGuid, AdminPermissions.UserManagement.ManageUsers, UserGroupPermission.PermissionValues.Allowed);
			return aspireContext.DemandAdminPermissions(loginSessionGuid, instituteID.Value, AdminPermissions.UserManagement.ManageUsers, UserGroupPermission.PermissionValues.Allowed);
		}

		private static bool CanModifyUser(this AspireContext aspireContext, Model.Entities.User user, Common.Permissions.Admin.AdminLoginHistory loginHistory)
		{
			if (loginHistory.IsMasterAdmin)
				return true;

			var userRoles = aspireContext.UserRoles.Where(r => r.UserID == user.UserID && r.IsDeleted == false);
			if (userRoles.Any(r => r.UserType == UserTypeConstants.Admin || r.UserType == UserTypeConstants.Executive))
				return false;

			var instituteIDs = aspireContext.GetAdminInstitutes(loginHistory).Select(i => i.InstituteID);
			if (userRoles.Any(r => r.UserType == UserTypeConstants.Staff && !instituteIDs.Contains(r.InstituteID.Value)))
				return false;
			return true;
		}

		public sealed class User
		{
			public sealed class UserRole
			{
				internal UserRole()
				{
				}

				public int UserRoleID { get; internal set; }
				public int? GroupID { get; internal set; }
				public short UserType { get; internal set; }
				public UserTypes UserTypeEnum => (UserTypes)this.UserType;
				public int? InstituteID { get; internal set; }
				public string InstituteAlias { get; internal set; }
				public string GroupName { get; internal set; }
				public DateTime ExpiryDate { get; internal set; }
				public string AssignedByUser { get; internal set; }
				public DateTime CreatedDate { get; internal set; }
				public byte Status { get; internal set; }
				public string StatusFullName => ((Model.Entities.UserRole.Statuses)this.Status).ToFullName();
			}

			public sealed class UserRolesModule
			{
				internal UserRolesModule()
				{
				}

				public int UserRoleModuleID { get; internal set; }
				public short UserType { get; internal set; }
				public string UserTypeFullName => ((UserTypes)this.UserType).ToFullName();
				public byte Module { get; internal set; }
				public string AspireModuleFullName => ((AspireModules)this.Module).ToFullName();
				public string Institute { get; internal set; }
				public string Department { get; internal set; }
				public string Program { get; internal set; }
				public int? AdmissionOpenProgramID { get; internal set; }
				public short? IntakeSemesterID { get; internal set; }
				public string IntakeSemester => this.IntakeSemesterID.ToSemesterString();
			}

			internal User()
			{
			}

			public int UserID { get; internal set; }
			public string Username { get; internal set; }
			public string Name { get; internal set; }
			public string Email { get; internal set; }
			public string Mobile { get; internal set; }
			public string JobTitle { get; internal set; }
			public byte Status { get; internal set; }
			public DateTime? ConfirmationCodeExpiryDate { get; internal set; }
			public Model.Entities.User.Statuses StatusEnum => (Model.Entities.User.Statuses)this.Status;
			public bool AccountActivatedByUser { get; internal set; }
			public bool AccountLocked { get; internal set; }
			public DateTime? AccountLockedDate { get; internal set; }
			public bool CanModify { get; internal set; }
			public List<UserRole> Roles { get; internal set; }
			public List<UserRolesModule> RoleModules { get; internal set; }
		}

		public static User GetUser(int userID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				var instituteIDs = aspireContext.GetAdminInstitutes(loginHistory).Select(i => i.InstituteID);
				var _user = aspireContext.Users.Single(u => u.UserID == userID && u.IsDeleted == false);
				if (_user == null)
					return null;

				var user = new User
				{
					UserID = _user.UserID,
					Username = _user.Username,
					Name = _user.Name,
					Email = _user.Email,
					Mobile = _user.Mobile,
					JobTitle = _user.JobTitle,
					AccountActivatedByUser = _user.AccountActivatedByUser,
					AccountLocked = _user.AccountLocked,
					AccountLockedDate = _user.AccountLockedDate,
					Status = _user.Status,
					ConfirmationCodeExpiryDate = _user.ConfirmationCodeExpiryDate,
					CanModify = aspireContext.CanModifyUser(_user, loginHistory),
				};

				var userRoles = aspireContext.UserRoles.Where(r => r.UserID == _user.UserID && r.IsDeleted == false);
				var staffUserRoles = userRoles.Where(r => r.UserType == UserTypeConstants.Staff && instituteIDs.Contains(r.InstituteID.Value));
				user.Roles = staffUserRoles.Select(r => new User.UserRole
				{
					UserType = UserTypeConstants.Staff,
					UserRoleID = r.UserRoleID,
					InstituteID = r.InstituteID.Value,
					InstituteAlias = r.Institute.InstituteAlias,
					GroupName = r.UserGroup.GroupName,
					GroupID = r.UserGroup.UserGroupID,
					ExpiryDate = r.ExpiryDate,
					AssignedByUser = r.User.Name,
					CreatedDate = r.CreatedDate,
					Status = r.Status,
				}).ToList();

				user.RoleModules = staffUserRoles.SelectMany(r => r.UserRoleModules).Select(rm => new User.UserRolesModule
				{
					UserType = rm.UserRole.UserType,
					UserRoleModuleID = rm.UserRoleModuleID,
					Module = rm.Module,
					Institute = rm.UserRole.Institute.InstituteAlias,
					Department = rm.Department.DepartmentAlias,
					Program = rm.Program.ProgramAlias,
					AdmissionOpenProgramID = rm.AdmissionOpenProgramID,
					IntakeSemesterID = rm.AdmissionOpenProgram.SemesterID,
				}).ToList();

				if (loginHistory.IsMasterAdmin)
				{
					var adminRoles = userRoles.Where(r => r.UserType == UserTypeConstants.Admin);
					user.Roles.AddRange(adminRoles.SelectMany(r => r.UserRoleInstitutes).Select(r => new User.UserRole
					{
						UserType = UserTypeConstants.Admin,
						UserRoleID = r.UserRoleID,
						GroupName = r.UserRole.UserGroup.GroupName,
						GroupID = r.UserRole.UserGroup.UserGroupID,
						ExpiryDate = r.UserRole.ExpiryDate,
						AssignedByUser = r.UserRole.User.Name,
						CreatedDate = r.UserRole.CreatedDate,
						Status = r.UserRole.Status,
						InstituteAlias = r.Institute.InstituteAlias,
					}).ToList());

					user.Roles.AddRange(userRoles.Where(r => r.UserType == UserTypeConstants.MasterAdmin).Select(r => new User.UserRole
					{
						UserType = UserTypeConstants.MasterAdmin,
						UserRoleID = r.UserRoleID,
						GroupName = null,
						GroupID = null,
						ExpiryDate = r.ExpiryDate,
						AssignedByUser = r.User.Name,
						CreatedDate = r.CreatedDate,
						Status = r.Status,
						InstituteAlias = null,
						InstituteID = null,
					}).ToList());

					var executiveRoles = userRoles.Where(r => r.UserType == UserTypeConstants.Executive);
					user.Roles.AddRange(executiveRoles.Select(er => new User.UserRole
					{
						UserType = er.UserGroup.UserType,
						UserRoleID = er.UserRoleID,
						GroupName = er.UserGroup.GroupName,
						GroupID = er.UserGroup.UserGroupID,
						ExpiryDate = er.ExpiryDate,
						AssignedByUser = er.User.Name,
						CreatedDate = er.CreatedDate,
						Status = er.Status,
						InstituteID = er.InstituteID,
						InstituteAlias = er.Institute.InstituteAlias,
					}).ToList());

					user.RoleModules.AddRange(executiveRoles.SelectMany(r => r.UserRoleModules).Select(rm => new User.UserRolesModule
					{
						UserRoleModuleID = rm.UserRoleModuleID,
						Module = rm.Module,
						Institute = rm.UserRole.Institute.InstituteAlias,
						Department = rm.Department.DepartmentAlias,
						Program = rm.Program.ProgramAlias,
						AdmissionOpenProgramID = rm.AdmissionOpenProgramID,
						IntakeSemesterID = rm.AdmissionOpenProgram.SemesterID,
						UserType = rm.UserRole.UserType,
					}).ToList());
				}

				return user;
			}
		}

		public sealed class AddUserResult
		{
			public enum Statuses
			{
				UserNameAlreadyExists,
				EmailAlreadyExists,
				Success
			}

			public Statuses Status { get; }
			public int? UserID { get; }

			internal AddUserResult(Statuses status)
			{
				this.Status = status;
			}

			internal AddUserResult(int userID)
			{
				this.UserID = userID;
				this.Status = Statuses.Success;
			}
		}

		public static AddUserResult AddUser(string userName, string name, string email, string jobTitle, string mobile, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var userLoginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var user = new Aspire.Model.Entities.User
				{
					Name = name.TrimAndCannotBeEmpty(),
					Email = email.TrimAndCannotBeEmpty().ValidateEmailAddress(),
					JobTitle = jobTitle.TrimAndCannotBeEmpty(),
					Mobile = mobile.Trim().ValidateMobileNumber(),
					Username = userName.TrimAndCannotBeEmpty(),
					Password = "NotActivatedYet",
					StatusEnum = Model.Entities.User.Statuses.Inactive,
					AccountActivatedByUser = false,
					AccountLocked = false,
					AccountLockedDate = null,
					CreatedOn = DateTime.Now,
					CreatedByUserID = userLoginHistory.UserID,
					PasswordChangeOn = null,
					PasswordChangeOnNextLogin = false,
					ConfirmationCode = Guid.NewGuid(),
					ConfirmationCodeExpiryDate = DateTime.Today.AddDays(3),
					IsDeleted = false,
				};
				if (aspireContext.Users.Any(u => u.Username == user.Username && u.IsDeleted == false))
					return new AddUserResult(AddUserResult.Statuses.UserNameAlreadyExists);
				if (aspireContext.Users.Any(u => u.Email == user.Email && u.IsDeleted == false))
					return new AddUserResult(AddUserResult.Statuses.EmailAlreadyExists);
				aspireContext.Users.Add(user);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistory.UserLoginHistoryID);
				return new AddUserResult(user.UserID);
			}
		}

		public enum UpdateUserStatuses
		{
			NoRecordFound,
			UserNameAlreadyExists,
			OnlyMasterAdminCanEdit,
			CannotInactiveYourAccount,
			EmailAlreadyExists,
			Success
		}

		public static UpdateUserStatuses UpdateUser(int userID, string userName, string name, string email, string jobTitle, string mobile, Model.Entities.User.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var userLoginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var user = aspireContext.Users.SingleOrDefault(u => u.UserID == userID && u.IsDeleted == false);
				if (user == null)
					return UpdateUserStatuses.NoRecordFound;

				if (!aspireContext.CanModifyUser(user, userLoginHistory))
					return UpdateUserStatuses.OnlyMasterAdminCanEdit;

				if (statusEnum == Model.Entities.User.Statuses.Inactive && userID == userLoginHistory.UserID)
					return UpdateUserStatuses.CannotInactiveYourAccount;

				userName = userName.TrimAndCannotBeEmpty();
				if (aspireContext.Users.Any(u => u.Username == userName && u.UserID != userID && u.IsDeleted == false))
					return UpdateUserStatuses.UserNameAlreadyExists;
				if (aspireContext.Users.Any(u => u.Email == email && u.UserID != userID && u.IsDeleted == false))
					return UpdateUserStatuses.EmailAlreadyExists;
				user.Username = userName;
				user.Name = name.TrimAndCannotBeEmpty();
				email = email.MustBeEmailAddress();
				if (user.Email != email)
				{
					user.Email = email;
					if (user.AccountActivatedByUser)
						user.Password = Membership.GeneratePassword(10, 5).EncryptPassword();
				}
				user.JobTitle = jobTitle.TrimAndCannotBeEmpty();
				user.Mobile = mobile.Trim().ValidateMobileNumber();
				user.StatusEnum = statusEnum;
				if (user.StatusEnum == Model.Entities.User.Statuses.Active)
					if (user.UserRoles1.All(r => r.IsDeleted))
						user.StatusEnum = Model.Entities.User.Statuses.Inactive;

				aspireContext.SaveChangesAndCommitTransaction(userLoginHistory.UserLoginHistoryID);
				return UpdateUserStatuses.Success;
			}
		}

		public enum DeleteUserStatus
		{
			NoRecordFound,
			CannotDeleteYourself,
			Success
		}

		public static DeleteUserStatus DeleteUser(int userID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var userLoginHistory = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				var user = aspireContext.Users.FirstOrDefault(u => u.UserID == userID && u.IsDeleted == false);
				if (user == null)
					return DeleteUserStatus.NoRecordFound;

				if (user.UserID == userLoginHistory.UserID)
					return DeleteUserStatus.CannotDeleteYourself;

				user.IsDeleted = true;
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistory.UserLoginHistoryID);
				return DeleteUserStatus.Success;
			}
		}

		public enum DeleteUserRoleStatus
		{
			NoRecordFound,
			Success,
			CannotDeleteYourself,
			NotAuthorizedToDeleteTheSelectedRole
		}

		public static DeleteUserRoleStatus DeleteUserRole(int userRoleID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				Common.Permissions.Admin.AdminLoginHistory loginHistory;
				var userRole = aspireContext.UserRoles.SingleOrDefault(r => r.UserRoleID == userRoleID);
				if (userRole == null)
					return DeleteUserRoleStatus.NoRecordFound;
				switch (userRole.UserTypeEnum)
				{
					case UserTypes.Admin:
					case UserTypes.MasterAdmin:
					case UserTypes.Executive:
						loginHistory = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
						if (loginHistory.UserRoleID == userRole.UserRoleID)
							return DeleteUserRoleStatus.CannotDeleteYourself;
						break;
					case UserTypes.Staff:
						loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
						if (!aspireContext.CanModifyUser(userRole.User1, loginHistory))
							return DeleteUserRoleStatus.NotAuthorizedToDeleteTheSelectedRole;
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(userRole.UserTypeEnum), userRole.UserTypeEnum, null);
				}
				userRole.IsDeleted = true;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				if (userRole.User1.UserRoles1.All(r => r.IsDeleted))
					userRole.User1.StatusEnum = Model.Entities.User.Statuses.Inactive;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteUserRoleStatus.Success;
			}
		}

		public static UserRole GetUserAdminRole(int userRoleID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var userLoginHistory = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				return aspireContext.UserRoles.Include(r => r.UserRoleInstitutes).SingleOrDefault(r => r.UserRoleID == userRoleID && (r.UserType == UserTypeConstants.Admin || r.UserType == UserTypeConstants.MasterAdmin) && r.IsDeleted == false && r.User1.IsDeleted == false);
			}
		}

		public static UserRole GetUserRole(int userRoleID, UserTypes userTypeEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				var userType = (short)userTypeEnum;
				return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.UserRoles).SingleOrDefault(r => r.UserRoleID == userRoleID && r.IsDeleted == false && r.UserType == userType && r.User1.IsDeleted == false);
			}
		}

		// Temporary Add this function to Get User Roles for adding User role modules by script of Create Users.
		public static UserRole GetUserRole(int userID, UserTypes userTypeEnum, int instituteID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				return aspireContext.UserRoles.SingleOrDefault(ur => ur.UserID == userID && ur.UserType == (short)userTypeEnum && ur.InstituteID == instituteID);
			}
		}

		private static void MakeStatusActiveIfNoRolesFound(this AspireContext aspireContext, Model.Entities.User user) // Sir I think the name of function should be MakeStatusActiveIfRolesFound
		{
			if (user.StatusEnum == Model.Entities.User.Statuses.Active)
				return;
			if (!user.UserRoles1.Any(r => r.IsDeleted == false))
				return;
			user.StatusEnum = Model.Entities.User.Statuses.Active;
		}

		public enum AddUserRoleStatuses
		{
			RoleAlreadyExists,
			MasterAdminRoleCannotBeAddedWhenAdminRolesExists,
			NoRecordFound,
			Success,
			AdminRoleCannotBeAddedWhenMasterAdminRolesExists
		}

		public static AddUserRoleStatuses AddUserMasterAdminRole(int userID, DateTime expiryDate, UserRole.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var userLoginHistory = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				var user = aspireContext.Users.SingleOrDefault(u => u.UserID == userID && u.IsDeleted == false);
				if (user == null)
					return AddUserRoleStatuses.NoRecordFound;
				if (aspireContext.UserRoles.Any(r => r.UserID == userID && r.IsDeleted == false && r.UserType == UserTypeConstants.MasterAdmin))
					return AddUserRoleStatuses.RoleAlreadyExists;
				if (aspireContext.UserRoles.Any(r => r.UserID == userID && r.IsDeleted == false && r.UserType == UserTypeConstants.Admin))
					return AddUserRoleStatuses.MasterAdminRoleCannotBeAddedWhenAdminRolesExists;
				user.UserRoles1.Add(new UserRole
				{
					UserTypeEnum = UserTypes.MasterAdmin,
					UserGroupID = null,
					ExpiryDate = expiryDate,
					StatusEnum = statusEnum,
					IsDeleted = false,
					CreatedDate = DateTime.Now,
					AssignedByUserID = userLoginHistory.UserID,
					InstituteID = null,
					IsDefault = false,
				});
				aspireContext.MakeStatusActiveIfNoRolesFound(user);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistory.UserLoginHistoryID);
				return AddUserRoleStatuses.Success;
			}
		}

		public static AddUserRoleStatuses AddUserAdminRole(int userID, int userGroupID, List<int> instituteIDs, DateTime expiryDate, UserRole.Statuses statusEnum, Guid loginSessionGuid)
		{
			if (instituteIDs == null || instituteIDs.Count == 0)
				throw new ArgumentNullException(nameof(instituteIDs));
			using (var aspireContext = new AspireContext(true))
			{
				var userLoginHistory = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				var user = aspireContext.Users.SingleOrDefault(u => u.UserID == userID && u.IsDeleted == false);
				if (user == null)
					return AddUserRoleStatuses.NoRecordFound;
				var userRole = aspireContext.UserRoles.SingleOrDefault(r => r.UserID == userID && r.IsDeleted == false && (r.UserType == UserTypeConstants.MasterAdmin || r.UserType == UserTypeConstants.Admin));
				if (userRole != null)
				{
					if (userRole.UserTypeEnum.IsMasterAdmin())
						return AddUserRoleStatuses.AdminRoleCannotBeAddedWhenMasterAdminRolesExists;
					return AddUserRoleStatuses.RoleAlreadyExists;
				}
				user.UserRoles1.Add(new UserRole
				{
					UserGroupID = aspireContext.UserGroups.Where(g => g.UserGroupID == userGroupID && g.InstituteID == null && g.UserType == UserTypeConstants.Admin).Select(g => g.UserGroupID).Single(),
					UserRoleInstitutes = instituteIDs.Select(i => new UserRoleInstitute
					{
						InstituteID = i,
					}).ToList(),
					ExpiryDate = expiryDate,
					StatusEnum = statusEnum,
					AssignedByUserID = userLoginHistory.UserID,
					CreatedDate = DateTime.Now,
					IsDeleted = false,
					InstituteID = null,
					IsDefault = false,
					UserTypeEnum = UserTypes.Admin,
				});
				aspireContext.MakeStatusActiveIfNoRolesFound(user);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistory.UserLoginHistoryID);
				return AddUserRoleStatuses.Success;
			}
		}

		public static AddUserRoleStatuses AddUserRole(UserTypes userTypeEnum, int userID, int userGroupID, int instituteID, DateTime expiryDate, UserRole.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				if (!userTypeEnum.IsStaff() && !userTypeEnum.IsExecutive())
					throw new ArgumentOutOfRangeException(nameof(userTypeEnum), userTypeEnum, null);
				var userLoginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				var user = aspireContext.Users.SingleOrDefault(u => u.UserID == userID && u.IsDeleted == false);
				if (user == null)
					return AddUserRoleStatuses.NoRecordFound;
				var userType = (short)userTypeEnum;
				var userRole = aspireContext.UserRoles.SingleOrDefault(r => r.UserID == userID && r.UserType == userType && r.InstituteID == instituteID && r.IsDeleted == false);
				if (userRole != null)
					return AddUserRoleStatuses.RoleAlreadyExists;
				user.UserRoles1.Add(new UserRole
				{
					UserTypeEnum = userTypeEnum,
					UserGroupID = aspireContext.UserGroups.Where(g => g.UserGroupID == userGroupID && g.InstituteID == instituteID && g.UserType == userType).Select(g => g.UserGroupID).Single(),
					InstituteID = instituteID,
					ExpiryDate = expiryDate,
					StatusEnum = statusEnum,
					IsDeleted = false,
					IsDefault = false,
					AssignedByUserID = userLoginHistory.UserID,
					CreatedDate = DateTime.Now,
				});

				aspireContext.MakeStatusActiveIfNoRolesFound(user);
				aspireContext.SaveChanges(userLoginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return AddUserRoleStatuses.Success;
			}
		}

		public enum UpdateUserRoleStatuses
		{
			NoRecordFound,
			Success
		}

		public static UpdateUserRoleStatuses UpdateUserMasterAdminRole(int userAdminRoleID, DateTime expiryDate, UserRole.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var userLoginHistory = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				var userAdminRole = aspireContext.UserRoles.SingleOrDefault(ur => ur.UserRoleID == userAdminRoleID && ur.IsDeleted == false && ur.UserType == UserTypeConstants.MasterAdmin);
				if (userAdminRole == null)
					return UpdateUserRoleStatuses.NoRecordFound;
				userAdminRole.ExpiryDate = expiryDate;
				userAdminRole.StatusEnum = statusEnum;
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistory.UserLoginHistoryID);
				return UpdateUserRoleStatuses.Success;
			}
		}

		private static IQueryable<UserRole> GetUserRoles(this AspireContext aspireContext, UserTypes userTypeEnum)
		{
			var userType = (short)userTypeEnum;
			return aspireContext.UserRoles.Where(r => r.UserType == userType && r.IsDeleted == false && r.User1.IsDeleted == false);
		}

		private static UserRole GetUserRole(this AspireContext aspireContext, UserTypes userTypeEnum, int userRoleID)
		{
			return aspireContext.GetUserRoles(userTypeEnum).SingleOrDefault(r => r.UserRoleID == userRoleID);
		}

		public static UpdateUserRoleStatuses UpdateUserAdminRole(int userRoleID, int userGroupID, List<int> instituteIDs, DateTime expiryDate, UserRole.Statuses statusEnum, Guid loginSessionGuid)
		{
			if (instituteIDs == null || instituteIDs.Count == 0)
				throw new ArgumentNullException(nameof(instituteIDs));
			using (var aspireContext = new AspireContext(true))
			{
				var userLoginHistory = aspireContext.DemandMasterAdminActiveSession(loginSessionGuid);
				var userAdminRole = aspireContext.GetUserRoles(UserTypes.Admin).Include(r => r.UserRoleInstitutes).SingleOrDefault(r => r.UserRoleID == userRoleID);
				if (userAdminRole == null)
					return UpdateUserRoleStatuses.NoRecordFound;

				userAdminRole.UserGroupID = aspireContext.UserGroups.Where(g => g.UserGroupID == userGroupID && g.InstituteID == null && g.UserType == UserTypeConstants.Admin).Select(g => g.UserGroupID).Single();
				userAdminRole.ExpiryDate = expiryDate;
				userAdminRole.StatusEnum = statusEnum;

				aspireContext.UserRoleInstitutes.RemoveRange(userAdminRole.UserRoleInstitutes.Where(r => !instituteIDs.Contains(r.InstituteID)));

				foreach (var instituteID in instituteIDs)
					if (userAdminRole.UserRoleInstitutes.All(i => i.InstituteID != instituteID))
						userAdminRole.UserRoleInstitutes.Add(new UserRoleInstitute
						{
							InstituteID = instituteID
						});
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistory.UserLoginHistoryID);
				return UpdateUserRoleStatuses.Success;
			}
		}

		public static UpdateUserRoleStatuses UpdateUserRole(UserTypes userTypeEnum, int userRoleID, int userGroupID, DateTime expiryDate, UserRole.Statuses statusEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				if (!userTypeEnum.IsStaff() && !userTypeEnum.IsExecutive())
					throw new ArgumentOutOfRangeException(nameof(userTypeEnum), userTypeEnum, null);
				var userType = (short)userTypeEnum;
				var userRole = aspireContext.GetAdminInstitutes(loginHistory).SelectMany(r => r.UserRoles).SingleOrDefault(r => r.UserRoleID == userRoleID && r.UserType == userType && r.IsDeleted == false && r.User1.IsDeleted == false);
				if (userRole == null)
					return UpdateUserRoleStatuses.NoRecordFound;
				userRole.UserGroupID = aspireContext.UserGroups.Where(g => g.UserGroupID == userGroupID && g.InstituteID == userRole.InstituteID && g.UserType == userType).Select(g => g.UserGroupID).Single();
				userRole.ExpiryDate = expiryDate;
				userRole.StatusEnum = statusEnum;
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return UpdateUserRoleStatuses.Success;
			}
		}

		public enum SendAccountActivationEmailToUserResult
		{
			Success,
			NoRecordFound,
			UserStatusIsNotActive,
			NotAuthorized
		}

		public static SendAccountActivationEmailToUserResult SendAccountActivationEmailToUser(int userID, string pageUrl, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(null, loginSessionGuid);
				var user = aspireContext.Users.SingleOrDefault(u => u.UserID == userID && u.IsDeleted == false);
				if (user == null)
					return SendAccountActivationEmailToUserResult.NoRecordFound;

				if (user.StatusEnum != Model.Entities.User.Statuses.Active)
					return SendAccountActivationEmailToUserResult.UserStatusIsNotActive;

				if (!aspireContext.CanModifyUser(user, loginHistory))
					return SendAccountActivationEmailToUserResult.NotAuthorized;

				var createdDate = DateTime.Now;
				var expiryDate = createdDate.AddDays(3);

				user.ConfirmationCode = Guid.NewGuid();
				user.ConfirmationCodeExpiryDate = expiryDate;
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);

				var url = $"{pageUrl}?C={user.ConfirmationCode.ToString().UrlEncode()}";
				var emailBody = Properties.Resources.UserAccountActivationEmailLink;
				emailBody = emailBody.Replace("{NameHtmlEncoded}", user.Name.HtmlEncode());
				emailBody = emailBody.Replace("{LinkHtmlAttributeEncoded}", url.HtmlAttributeEncode());
				emailBody = emailBody.Replace("{LinkHtmlEncoded}", url.HtmlEncode());

				var subject = "Activate Account";
				var loggedInUser = aspireContext.Users.Where(u => u.UserID == loginHistory.UserID).Select(u => new { u.Name, u.Email }).Single();
				var toList = new[] { (user.Name, user.Email) };
				var replyToList = new[] { (loggedInUser.Name, loggedInUser.Email) };
				aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.UserAccountActivation, subject, emailBody, true, Email.Priorities.Highest, createdDate, expiryDate, loginHistory.UserLoginHistoryID, toList: toList, replyToList: replyToList);
				aspireContext.CommitTransaction();
				return SendAccountActivationEmailToUserResult.Success;
			}
		}

		public class GetUserByConfirmationCodeResult
		{
			public enum Statuses
			{
				Success,
				NoRecordFound,
				ConfirmationCodeExpired,
				StatusIsNotActive
			}

			public Statuses Status { get; }
			public string Email { get; }
			public string Name { get; }
			public string Username { get; }

			internal GetUserByConfirmationCodeResult(Statuses status)
			{
				if (status == Statuses.Success)
					throw new ArgumentException(nameof(status));
				this.Status = status;
			}

			internal GetUserByConfirmationCodeResult(string username, string name, string email)
			{
				this.Status = Statuses.Success;
				this.Username = username;
				this.Name = name;
				this.Email = email;
			}
		}

		public static GetUserByConfirmationCodeResult GetUserByConfirmationCode(Guid confirmationCode)
		{
			using (var aspireContext = new AspireContext())
			{
				var user = aspireContext.Users.Where(u => u.ConfirmationCode == confirmationCode && u.IsDeleted == false).Select(u => new
				{
					u.UserID,
					u.Username,
					u.Name,
					u.Email,
					u.ConfirmationCodeExpiryDate,
					StatusEnum = (Model.Entities.User.Statuses)u.Status,
				}).SingleOrDefault();
				if (user == null)
					return new GetUserByConfirmationCodeResult(GetUserByConfirmationCodeResult.Statuses.NoRecordFound);
				if (user.StatusEnum != Model.Entities.User.Statuses.Active)
					return new GetUserByConfirmationCodeResult(GetUserByConfirmationCodeResult.Statuses.StatusIsNotActive);
				if (user.ConfirmationCodeExpiryDate < DateTime.Now)
					return new GetUserByConfirmationCodeResult(GetUserByConfirmationCodeResult.Statuses.ConfirmationCodeExpired);
				return new GetUserByConfirmationCodeResult(user.Username, user.Name, user.Email);
			}
		}

		public sealed class ResetPasswordResult
		{
			public enum Statuses
			{
				NoRecordFound,
				ConfirmationCodeExpired,
				Success,
				StatusIsNotActive
			}

			public List<UserTypes> UserTypes { get; internal set; }
			public Statuses Status { get; internal set; }
		}

		public static ResetPasswordResult ResetPassword(Guid confirmationCode, SecureString password)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var user = aspireContext.Users.SingleOrDefault(u => u.ConfirmationCode == confirmationCode && u.IsDeleted == false);
				if (user == null)
					return new ResetPasswordResult { Status = ResetPasswordResult.Statuses.NoRecordFound };
				if (user.StatusEnum != Model.Entities.User.Statuses.Active)
					return new ResetPasswordResult { Status = ResetPasswordResult.Statuses.StatusIsNotActive };
				if (user.ConfirmationCodeExpiryDate < DateTime.Now)
					return new ResetPasswordResult { Status = ResetPasswordResult.Statuses.ConfirmationCodeExpired };
				var now = DateTime.Now;
				var oldHashedPassword = user.Password;
				user.Password = password.ToUnSecureString().ValidatePassword().EncryptPassword();
				user.PasswordChangeOn = now;
				user.ConfirmationCode = null;
				user.ConfirmationCodeExpiryDate = null;
				user.PasswordChangeOn = now;

				if (user.AccountActivatedByUser == false)
					user.AccountActivatedByUser = true;

				if (user.PasswordChangeOnNextLogin == true)
					user.PasswordChangeOnNextLogin = false;

				var userTypes = aspireContext.UserRoles.Where(r => r.UserID == user.UserID && r.IsDeleted == false)
					.Where(r => r.Status == (byte)UserRole.Statuses.Active && r.ExpiryDate > DateTime.Now)
					.Select(r => r.UserType).ToList().Cast<UserTypes>().ToList();

				aspireContext.PasswordChangeHistories.Add(new PasswordChangeHistory
				{
					ChangeReasonEnum = PasswordChangeReasons.UsingResetLink,
					UserID = user.UserID,
					ChangedDate = now,
					UserLoginHistoryID = UserLoginHistory.SystemUserLoginHistoryID,
					StudentID = null,
					CandidateID = null,
					FacultyMemberID = null,
					StudentIDOfParent = null,
					OldPassword = oldHashedPassword,
					NewPassword = user.Password
				});

				aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
				return new ResetPasswordResult { Status = ResetPasswordResult.Statuses.Success, UserTypes = userTypes };
			}
		}

		public sealed class CustomUser
		{
			public int UserID { get; internal set; }
			public string Username { get; internal set; }
			public string Name { get; internal set; }
			public string Email { get; internal set; }
			public string JobTitle { get; internal set; }
			public byte Status { get; internal set; }
			public bool AccountActivatedByUser { get; internal set; }
			public Model.Entities.User.Statuses StatusEnum => (Model.Entities.User.Statuses)this.Status;
			public List<short> UserTypes { get; internal set; }
			public string CreatedBy { get; internal set; }
			public DateTime? CreatedOn { get; internal set; }
			public List<DateTime> ExpiryDates { get; internal set; }
			public string Roles => string.Join(", ", this.UserTypes.OrderBy(r => r).Select(r => ((UserTypes)r).ToFullName())).ToNAIfNullOrEmpty();
			public List<string> GroupNames { get; set; }
			public string Groups => string.Join(", ", this.GroupNames.Where(g => g != null)).ToNAIfNullOrEmpty();
			public string StringExpiryDates => string.Join(", ", this.ExpiryDates.Select(ed => ed.ToString("D"))).ToNAIfNullOrEmpty();
		}

		public static List<CustomUser> GetUsers(int? instituteID, UserTypes? userTypeEnum, int? userGroupID, Model.Entities.User.Statuses? statusEnum, string searchText, int pageIndex, int pageSize, out int virtualItemCount, string sortExpression, SortDirection sortDirection, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				var users = aspireContext.Users.Where(u => u.IsDeleted == false);
				if (userTypeEnum != null)
				{
					var userType = userTypeEnum.Value.ToShort();
					users = users.Where(u => u.UserRoles1.Any(r => r.UserType == userType && r.IsDeleted == false));
				}
				if (userGroupID != null)
					users = users.Where(u => u.UserRoles1.Any(r => r.UserGroupID == userGroupID && r.IsDeleted == false));
				if (statusEnum != null)
				{
					var status = statusEnum.Value.ToByte();
					users = users.Where(u => u.Status == status);
				}
				if (!string.IsNullOrWhiteSpace(searchText))
					users = users.Where(u => u.Username.Contains(searchText) || u.Name.Contains(searchText) || u.Email.Contains(searchText) || u.JobTitle.Contains(searchText));

				var institutes = aspireContext.GetAdminInstitutes(loginHistory);
				if (instituteID != null)
					institutes = institutes.Where(i => i.InstituteID == instituteID.Value);

				var masterAdminUserIDs = aspireContext.UserRoles.Where(r => r.UserType == UserTypeConstants.MasterAdmin).Select(r => r.UserID);
				var staffUserIDs = institutes.SelectMany(i => i.UserRoles).Where(r => r.UserType == UserTypeConstants.Staff && r.IsDeleted == false).Select(r => r.UserID);
				var executiveUserIDs = institutes.SelectMany(i => i.UserRoles).Where(r => r.UserType == UserTypeConstants.Executive && r.IsDeleted == false).Select(r => r.UserID);
				var adminUserIDs = institutes.SelectMany(i => i.UserRoleInstitutes).Where(r => r.UserRole.UserType == UserTypeConstants.Admin && r.UserRole.IsDeleted == false).Select(r => r.UserRole.UserID);
				var userIDsWithNoRole = aspireContext.Users.Where(u => u.UserRoles1.All(r => r.IsDeleted)).Select(u => u.UserID);

				if (loginHistory.IsMasterAdmin)
					users = users.Where(u => staffUserIDs.Contains(u.UserID) || adminUserIDs.Contains(u.UserID) || userIDsWithNoRole.Contains(u.UserID) || masterAdminUserIDs.Contains(u.UserID) || executiveUserIDs.Contains(u.UserID));
				else
					users = users.Where(u => staffUserIDs.Contains(u.UserID) || userIDsWithNoRole.Contains(u.UserID));

				virtualItemCount = users.Count();
				switch (sortExpression)
				{
					case nameof(Model.Entities.User.Username):
						users = sortDirection == SortDirection.Ascending ? users.OrderBy(u => u.Username) : users.OrderByDescending(u => u.Username);
						break;
					case nameof(Model.Entities.User.Name):
						users = sortDirection == SortDirection.Ascending ? users.OrderBy(u => u.Name) : users.OrderByDescending(u => u.Name);
						break;
					case nameof(Model.Entities.User.Email):
						users = sortDirection == SortDirection.Ascending ? users.OrderBy(u => u.Email) : users.OrderByDescending(u => u.Email);
						break;
					case nameof(Model.Entities.User.JobTitle):
						users = sortDirection == SortDirection.Ascending ? users.OrderBy(u => u.JobTitle) : users.OrderByDescending(u => u.JobTitle);
						break;
					case nameof(Model.Entities.User.Status):
						users = sortDirection == SortDirection.Ascending ? users.OrderBy(u => u.Status) : users.OrderByDescending(u => u.Status);
						break;
					case nameof(Model.Entities.User.CreatedOn):
						users = sortDirection == SortDirection.Ascending ? users.OrderBy(u => u.CreatedOn) : users.OrderByDescending(u => u.CreatedOn);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				users = users.Skip(pageIndex * pageSize).Take(pageSize);

				if (loginHistory.IsMasterAdmin)
					return users.Select(u => new CustomUser
					{
						UserID = u.UserID,
						Username = u.Username,
						Name = u.Name,
						Email = u.Email,
						JobTitle = u.JobTitle,
						Status = u.Status,
						AccountActivatedByUser = u.AccountActivatedByUser,
						UserTypes = u.UserRoles1.Where(r => r.IsDeleted == false).Select(r => r.UserType).Distinct().ToList(),
						GroupNames = u.UserRoles1.Where(r => r.IsDeleted == false).Select(r => r.UserGroup.GroupName).Distinct().ToList(),
						ExpiryDates = u.UserRoles1.Where(r => r.IsDeleted == false).Select(r => r.ExpiryDate).Distinct().ToList(),
						CreatedOn = u.CreatedOn,
						CreatedBy = aspireContext.Users.Where(cu => cu.UserID == u.CreatedByUserID).Select(cu => cu.Name).FirstOrDefault(),


					}).ToList();
				return users.Select(u => new CustomUser
				{
					UserID = u.UserID,
					Username = u.Username,
					Name = u.Name,
					Email = u.Email,
					JobTitle = u.JobTitle,
					Status = u.Status,
					AccountActivatedByUser = u.AccountActivatedByUser,
					UserTypes = u.UserRoles1.Where(r => r.IsDeleted == false && r.UserType == UserTypeConstants.Staff).Select(r => r.UserType).Distinct().ToList(),
					GroupNames = u.UserRoles1.Where(r => r.IsDeleted == false && r.UserType == UserTypeConstants.Staff).Select(r => r.UserGroup.GroupName).Distinct().ToList(),
					ExpiryDates = u.UserRoles1.Where(r => r.IsDeleted == false).Select(r => r.ExpiryDate).Distinct().ToList(),
					CreatedOn = u.CreatedOn,
					CreatedBy = aspireContext.Users.Where(cu => cu.UserID == u.CreatedByUserID).Select(cu => cu.Name).FirstOrDefault(),
				}).ToList();
			}
		}

		public sealed class GetUserGroupWithMenuLinksResult
		{
			public enum Statuses
			{
				Success,
				NoRecordFound,
			}

			public Statuses Status { get; }
			public string InstituteAlias { get; }
			public UserGroup UserGroup { get; }

			public GetUserGroupWithMenuLinksResult(Statuses status)
			{
				if (status == Statuses.Success)
					throw new ArgumentException(nameof(status));
				this.Status = status;
			}

			public GetUserGroupWithMenuLinksResult(UserGroup userGroup, string instituteAlias)
			{
				this.UserGroup = userGroup;
				this.InstituteAlias = instituteAlias;
				this.Status = Statuses.Success;
			}
		}

		public enum AddUserRoleModuleStatuses
		{
			Success,
			AlreadyExists,
			NoRecordFound
		}

		public static AddUserRoleModuleStatuses AddUserRoleModule(int userRoleID, AspireModules aspireModule, int instituteID, int? departmentID, int? programID, int? admissionOpenProgramID, Guid loginSessionGuid)
		{
			if (aspireModule == AspireModules.None)
				throw new ArgumentException(nameof(aspireModule));
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandPermissions(instituteID, loginSessionGuid);
				var userRoles = aspireContext.UserRoles.Where(r => r.UserRoleID == userRoleID && r.InstituteID == instituteID && r.IsDeleted == false && r.User1.IsDeleted == false);
				if (!userRoles.Any())
					return AddUserRoleModuleStatuses.NoRecordFound;

				var module = (byte)aspireModule;
				var userRoleModules = userRoles.SelectMany(r => r.UserRoleModules).Where(m => m.Module == module).ToList();
				var institutes = aspireContext.Institutes.Where(i => i.InstituteID == instituteID);
				if (departmentID != null)
				{
					var departments = institutes.SelectMany(i => i.Departments).Where(d => d.DepartmentID == departmentID.Value);
					if (!departments.Any())
						return AddUserRoleModuleStatuses.NoRecordFound;
					if (programID != null)
					{
						var programs = departments.SelectMany(d => d.Programs).Where(p => p.ProgramID == programID.Value);
						if (!programs.Any())
							return AddUserRoleModuleStatuses.NoRecordFound;
						if (admissionOpenProgramID != null)
						{
							var admissionOpenPrograms = programs.SelectMany(p => p.AdmissionOpenPrograms).Where(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID.Value);
							if (!admissionOpenPrograms.Any())
								return AddUserRoleModuleStatuses.NoRecordFound;
						}
					}
					else
					{
						if (admissionOpenProgramID != null)
							throw new ArgumentException(nameof(admissionOpenProgramID));
					}
				}
				else
				{
					if (programID != null)
						throw new ArgumentException(nameof(programID));
					if (admissionOpenProgramID != null)
						throw new ArgumentException(nameof(admissionOpenProgramID));
				}

				if (departmentID != null)
					if (userRoleModules.Any(m => m.DepartmentID == null || (m.DepartmentID == departmentID.Value && m.ProgramID == null && m.AdmissionOpenProgramID == null)))
						return AddUserRoleModuleStatuses.AlreadyExists;
				if (programID != null)
					if (userRoleModules.Any(m => m.DepartmentID == departmentID.Value && (m.ProgramID == null || (m.ProgramID == programID.Value && m.AdmissionOpenProgramID == null))))
						return AddUserRoleModuleStatuses.AlreadyExists;
				if (admissionOpenProgramID != null)
					if (userRoleModules.Any(m => m.DepartmentID == departmentID.Value && m.ProgramID == programID.Value && (m.AdmissionOpenProgramID == null || m.AdmissionOpenProgramID == admissionOpenProgramID.Value)))
						return AddUserRoleModuleStatuses.AlreadyExists;

				if (aspireContext.UserRoleModules.Any(urm => urm.UserRoleID == userRoleID && urm.Module == module && urm.DepartmentID == departmentID && urm.ProgramID == programID && urm.AdmissionOpenProgramID == admissionOpenProgramID))
					return AddUserRoleModuleStatuses.AlreadyExists;

				var userRoleModule = new UserRoleModule
				{
					UserRoleID = userRoleID,
					ModuleEnum = aspireModule,
					ModuleNameEnum = aspireModule,
					DepartmentID = departmentID,
					ProgramID = programID,
					AdmissionOpenProgramID = admissionOpenProgramID,
				}.Validate();
				aspireContext.UserRoleModules.Add(userRoleModule);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return AddUserRoleModuleStatuses.Success;
			}
		}

		public enum DeleteUserStaffRoleModuleStatuses
		{
			NoRecordFound,
			Success
		}

		public static DeleteUserStaffRoleModuleStatuses DeleteUserRoleModule(int userRolesModuleID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var userRolesModule = aspireContext.UserRoleModules.Where(urm => urm.UserRoleModuleID == userRolesModuleID).Select(usrm => new
				{
					usrm.UserRole.InstituteID,
				}).SingleOrDefault();
				if (userRolesModule == null)
					return DeleteUserStaffRoleModuleStatuses.NoRecordFound;
				var loginHistory = aspireContext.DemandPermissions(userRolesModule.InstituteID, loginSessionGuid);
				aspireContext.UserRoleModules.Remove(aspireContext.UserRoleModules.Single(urm => urm.UserRoleModuleID == userRolesModuleID));
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return DeleteUserStaffRoleModuleStatuses.Success;
			}
		}

		public static List<CustomListItem> GetAdmissionOpenProgramsList(int programID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext())
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				return aspireContext.GetAdminInstitutes(loginHistory).SelectMany(i => i.Programs).SelectMany(p => p.AdmissionOpenPrograms).Where(aop => aop.ProgramID == programID).Select(aop => new
				{
					aop.AdmissionOpenProgramID,
					aop.SemesterID,
				}).OrderByDescending(aop => aop.SemesterID).ToList().Select(a => new CustomListItem
				{
					ID = a.AdmissionOpenProgramID,
					Text = a.SemesterID.ToSemesterString(),
				}).ToList();
			}
		}

		public static void ValidateAndCorrectUserRoleModules()
		{
			using (var aspireContext = new AspireContext(true))
			{
				var userRoleModulesToBeAdded = new List<UserRoleModule>();
				var userRoleModulesToBeDeleted = new List<UserRoleModule>();
				var userRoleModules = aspireContext.UserRoleModules.Where(urm => urm.UserRole.IsDeleted == false && urm.UserRole.User1.IsDeleted == false).ToList();
				foreach (var userGroupPermission in userRoleModules.Where(urm => !urm.TryValidate()))
				{
					userRoleModulesToBeDeleted.Add(userGroupPermission);
					userRoleModulesToBeAdded.Add(userGroupPermission.Clone());
				}
				if (userRoleModulesToBeDeleted.Any())
				{
					aspireContext.UserRoleModules.RemoveRange(userRoleModulesToBeDeleted);
					aspireContext.SaveChangesAsSystemUser();
				}
				if (userRoleModulesToBeAdded.Any())
				{
					aspireContext.UserRoleModules.AddRange(userRoleModulesToBeAdded);
					aspireContext.SaveChangesAsSystemUser();
				}
				if (userRoleModulesToBeDeleted.Any() || userRoleModulesToBeAdded.Any())
					aspireContext.CommitTransaction();
			}
		}

		public enum ResetUserPasswordStatuses
		{
			NoRecordFound,
			Success,
		}

		public static ResetUserPasswordStatuses ResetUserPassword(int userID, string newPassword, PasswordChangeReasons passwordChangeReasonEnum, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				switch (passwordChangeReasonEnum)
				{
					case PasswordChangeReasons.UserForgotHisPassword:
					case PasswordChangeReasons.ManuallySettingForNewUser:
					case PasswordChangeReasons.PasswordPotentiallyCompromised:
					case PasswordChangeReasons.Troubleshooting:
						break;
					case PasswordChangeReasons.ByUserItSelf:
					case PasswordChangeReasons.SetDuringRegistration:
					case PasswordChangeReasons.UsingResetLink:
						throw new ArgumentException(nameof(passwordChangeReasonEnum));
					default:
						throw new ArgumentOutOfRangeException(nameof(passwordChangeReasonEnum), passwordChangeReasonEnum, null);
				}
				var user = aspireContext.Users.SingleOrDefault(u => u.UserID == userID && u.IsDeleted == false);
				if (user == null)
					return ResetUserPasswordStatuses.NoRecordFound;
				var oldHashedPassword = user.Password;
				user.Password = newPassword.ValidatePassword().EncryptPassword();
				var now = DateTime.Now;
				aspireContext.PasswordChangeHistories.Add(new PasswordChangeHistory
				{
					ChangeReasonEnum = passwordChangeReasonEnum,
					UserID = user.UserID,
					ChangedDate = now,
					UserLoginHistoryID = loginHistory.UserLoginHistoryID,
					OldPassword = oldHashedPassword,
					NewPassword = user.Password,
					StudentIDOfParent = null,
					FacultyMemberID = null,
					CandidateID = null,
					StudentID = null
				});
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				aspireContext.SendPasswordHasBeenResetEmailToUser(user.Email, now, user.Name, passwordChangeReasonEnum, loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return ResetUserPasswordStatuses.Success;
			}
		}

		public enum ResetUserPasswordByLinkStatuses
		{
			NoRecordFound,
			Success,
		}

		public static ResetUserPasswordByLinkStatuses SendPasswordResetLinkToUser(int userID, string passwordResetPageUrl, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				var user = aspireContext.Users.SingleOrDefault(u => u.UserID == userID && u.IsDeleted == false);
				if (user == null)
					return ResetUserPasswordByLinkStatuses.NoRecordFound;
				user.ConfirmationCode = Guid.NewGuid();
				user.ConfirmationCodeExpiryDate = DateTime.Now.AddDays(3);
				aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				var adminUser = aspireContext.Users.Where(u => u.UserID == loginHistory.UserID).Select(u => new
				{
					u.Name,
					u.Email
				}).Single();
				aspireContext.SendPasswordResetLinkEmailToUser(user.ConfirmationCode.Value, user.Email, user.Name, passwordResetPageUrl, (adminUser.Name, adminUser.Email), loginHistory.UserLoginHistoryID);
				aspireContext.CommitTransaction();
				return ResetUserPasswordByLinkStatuses.Success;
			}
		}

		public static Model.Entities.User UpdatePasswordResetLinkValidity(int userID, Guid loginSessionGuid)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var loginHistory = aspireContext.DemandModulePermissions(null, loginSessionGuid);
				var user = aspireContext.Users.SingleOrDefault(u => u.UserID == userID && u.IsDeleted == false);
				if (user == null)
					return null;
				user.ConfirmationCode = Guid.NewGuid();
				user.ConfirmationCodeExpiryDate = DateTime.Now.AddDays(3);
				aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				return user;
			}
		}

		private static void SendPasswordHasBeenResetEmailToUser(this AspireContext aspireContext, string emailAddress, DateTime changeDateTime, string name, PasswordChangeReasons passwordChangeReasonEnum, int userLoginHistoryID)
		{
			var emailBody = Properties.Resources.UserPasswordHasBeenResetByAdmin;
			emailBody = emailBody.Replace("{NameHtmlEncoded}", name.HtmlEncode());
			emailBody = emailBody.Replace("{Date}", changeDateTime.ToString("D").HtmlAttributeEncode());
			emailBody = emailBody.Replace("{Time}", changeDateTime.ToString("T").HtmlEncode());
			emailBody = emailBody.Replace("{Reason}", passwordChangeReasonEnum.ToFullName().HtmlEncode());
			var subject = "User Password Changed";
			var createdDate = DateTime.Now;
			var expiryDate = createdDate.AddDays(7);
			aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.UserPasswordHasBeenReset, subject, emailBody, true, Email.Priorities.Normal, createdDate, expiryDate, userLoginHistoryID, toList: new[] { (name, emailAddress) });
		}

		private static void SendPasswordResetLinkEmailToUser(this AspireContext aspireContext, Guid confirmationCode, string emailAddress, string name, string pageUrl, (string displayName, string email) replyTo, int userLoginHistoryID)
		{
			var url = $"{pageUrl}?C={confirmationCode.ToString("N").UrlEncode()}";
			var emailBody = Properties.Resources.UserPasswordResetByAdminEmailLink;
			emailBody = emailBody.Replace("{NameHtmlEncoded}", name.HtmlEncode());
			emailBody = emailBody.Replace("{LinkHtmlAttributeEncoded}", url.HtmlAttributeEncode());
			emailBody = emailBody.Replace("{LinkHtmlEncoded}", url.HtmlEncode());
			var subject = "Reset Password";
			var createdDate = DateTime.Now;
			var expiryDate = createdDate.AddDays(7);
			aspireContext.AddEmailAndSaveChanges(Email.EmailTypes.UserResetPasswordLinkByAdmin, subject, emailBody, true, Email.Priorities.Highest, createdDate, expiryDate, userLoginHistoryID, toList: new[] { (name, emailAddress) }, replyToList: new[] { replyTo });
		}
	}
}

