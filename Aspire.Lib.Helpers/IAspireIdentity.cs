﻿using System;

namespace Aspire.Lib.Helpers
{
	public interface IAspireIdentity
	{
		Guid LoginSessionGuid { get; }
	}
}