﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security;
using System.Web.Hosting;

namespace Aspire.Lib.Helpers
{
	public static class EmailHelper
	{
		private static readonly NetworkCredential Credential;
		private static readonly NetworkCredential Credential1;
		private static readonly NetworkCredential Credential2;
		private static readonly NetworkCredential Credential3;
		private static readonly NetworkCredential Credential4;
		private static readonly NetworkCredential Credential5;
		private const string DefaultFromDisplayName = "Bahria University";
		private const string BackupEmailBCC = "alerts@bahria.edu.pk";
		private static readonly string[] FromEmailAddresses = {
			"alerts@bahria.edu.pk",
			"alerts1@bahria.edu.pk",
			"alerts2@bahria.edu.pk",
			"alerts3@bahria.edu.pk",
			"alerts4@bahria.edu.pk",
			"alerts5@bahria.edu.pk",
		};

		static EmailHelper()
		{
			var pass = new SecureString();
			@"HK8\auY6H1+m\&.CKnyT".ToCharArray().ToList().ForEach(p => pass.AppendChar(p));
			pass.MakeReadOnly();
			Credential = new NetworkCredential(FromEmailAddresses[0], pass);
			Credential1 = new NetworkCredential(FromEmailAddresses[1], pass);
			Credential2 = new NetworkCredential(FromEmailAddresses[2], pass);
			Credential3 = new NetworkCredential(FromEmailAddresses[3], pass);
			Credential4 = new NetworkCredential(FromEmailAddresses[4], pass);
			Credential5 = new NetworkCredential(FromEmailAddresses[5], pass);
		}

		private static int SmtpClientIndex => DateTime.Now.Minute % 6;

		private static MailAddress GetFromMailAddress(int smtpClientIndex, string fromDisplayName)
		{
			switch (smtpClientIndex)
			{
				case 0:
					return new MailAddress(FromEmailAddresses[0], fromDisplayName);
				case 1:
					return new MailAddress(FromEmailAddresses[1], fromDisplayName);
				case 2:
					return new MailAddress(FromEmailAddresses[2], fromDisplayName);
				case 3:
					return new MailAddress(FromEmailAddresses[3], fromDisplayName);
				case 4:
					return new MailAddress(FromEmailAddresses[4], fromDisplayName);
				case 5:
				default:
					return new MailAddress(FromEmailAddresses[5], fromDisplayName);
			}
		}

		private static SmtpClient GetSmtpClient(int smtpClientIndex)
		{
			var smtpClient = new SmtpClient("smtp.office365.com", 587) { EnableSsl = true };
			switch (smtpClientIndex)
			{
				case 0:
					smtpClient.Credentials = Credential;
					break;
				case 1:
					smtpClient.Credentials = Credential1;
					break;
				case 2:
					smtpClient.Credentials = Credential2;
					break;
				case 3:
					smtpClient.Credentials = Credential3;
					break;
				case 4:
					smtpClient.Credentials = Credential4;
					break;
				case 5:
				default:
					smtpClient.Credentials = Credential5;
					break;
			}
			return smtpClient;
		}

		private static void SendMail(MailMessage mailMessage, string fromDisplayName = DefaultFromDisplayName)
		{
			var smtpClientIndex = SmtpClientIndex;
			mailMessage.From = GetFromMailAddress(smtpClientIndex, fromDisplayName);
#if DEBUG
			var safeSendList = new[]
			{
				"orangzeb@bahria.edu.pk".ToLower(),
				"orangzeb@gmail.com".ToLower(),
				"orangzeb006@yahoo.com".ToLower(),
				"hammad@bahria.edu.pk".ToLower(),
				"hammad7531@gmail.com".ToLower(),
				"asim.abbas@bahria.edu.pk".ToLower(),
				"01-244072-002@student.bahria.edu.pk".ToLower(),
			};

			if (mailMessage.To.Any(e => !safeSendList.Contains(e.Address.ToLower())))
				throw new InvalidOperationException("Email will be sent only to safe list while debugging.");
#endif
			using (var smtpClient = GetSmtpClient(smtpClientIndex))
			{
				try
				{
					if (!mailMessage.Bcc.Contains(mailMessage.From))
						mailMessage.Bcc.Add(mailMessage.From);
					mailMessage.Bcc.Add(BackupEmailBCC);
#if DEBUG
					mailMessage.Subject = $"DEBUG: {mailMessage.Subject}";
#endif
					smtpClient.Send(mailMessage);
				}
				catch (Exception exc)
				{
					throw;
				}
			}
		}

		public static void SendInBackground(List<string> receiversList, string subject, string body, bool isBodyHtml, List<string> replyToList = null, List<string> ccList = null, List<string> bccList = null, List<Tuple<string, byte[]>> attachments = null, string fromDisplayName = DefaultFromDisplayName)
		{
			HostingEnvironment.QueueBackgroundWorkItem(ct =>
			{
				using (var mailMessage = new MailMessage())
				{
					var emailSeparator = ",";
					mailMessage.To.Add(string.Join(emailSeparator, receiversList));
					if (ccList != null)
						mailMessage.CC.Add(string.Join(emailSeparator, ccList));
					if (bccList != null)
						mailMessage.Bcc.Add(string.Join(emailSeparator, bccList));
					if (replyToList != null)
						mailMessage.ReplyToList.Add(string.Join(emailSeparator, replyToList));

					mailMessage.Subject = subject;
					mailMessage.Body = body;
					mailMessage.IsBodyHtml = isBodyHtml;
					if (attachments != null)
						foreach (var attachment in attachments)
						{
							var ms = new MemoryStream(attachment.Item2);
							mailMessage.Attachments.Add(new Attachment(ms, attachment.Item1));
						}
					SendMail(mailMessage, fromDisplayName);
					foreach (var attachment in mailMessage.Attachments)
					{
						attachment.ContentStream.Dispose();
						attachment.Dispose();
					}
				}
			});
		}

		public static void SendInBackground(string receiver, string subject, string body, bool isBodyHtml, List<string> replyToList = null, List<string> ccList = null, List<string> bccList = null, List<Tuple<string, byte[]>> attachments = null, string fromDisplayName = DefaultFromDisplayName)
		{
			SendInBackground(new List<string> { receiver }, subject, body, isBodyHtml, replyToList, ccList, bccList, attachments, fromDisplayName);
		}
	}
}
