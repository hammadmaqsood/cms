﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aspire.Lib.Helpers
{
	public static class QueryStringHelper
	{
		#region AttachQueryParams

		private const string ParameterName = "v";

		private static string ToQueryString(this Dictionary<string, object> parameters)
		{
			if (parameters == null || !parameters.Any())
				return null;
			return string.Join("&", parameters
				.Where(p => !string.IsNullOrEmpty(p.Value?.ToString()))
				.Select(p => $"{HttpUtility.UrlEncode(p.Key)}={HttpUtility.UrlEncode(p.Value?.ToString())}"));
		}

		private static string AttachQueryParam(this string pagePath, string encryptedQueryParamString)
		{
			if (string.IsNullOrEmpty(encryptedQueryParamString))
				return pagePath;
			if (!pagePath.EndsWith("?"))
				pagePath += "?";
			return $"{pagePath}{ParameterName}={HttpUtility.UrlEncode(encryptedQueryParamString)}";
		}

		public const EncryptionModes DefaultEncryptionMode = EncryptionModes.ConstantSaltNoneUserSession;

		public static string AttachQueryParams(this string pagePath, Dictionary<string, object> parameters, EncryptionModes encryptionMode = DefaultEncryptionMode)
		{
			if (parameters == null || parameters.Count == 0)
				return pagePath;
			var queryString = parameters.ToQueryString();
			if (string.IsNullOrEmpty(queryString))
				return pagePath;
			var encryptedQueryString = queryString.Encrypt(encryptionMode);
			return pagePath.AttachQueryParam(encryptedQueryString);
		}

		public static string AttachQueryParam(this string pagePath, string parameterName, object parameterValue, EncryptionModes encryptionMode = DefaultEncryptionMode)
		{
			return pagePath.AttachQueryParams(new Dictionary<string, object> { { parameterName, parameterValue } }, encryptionMode);
		}

		public static string AttachQueryParams(this string pagePath, string parameter1Name, object parameter1Value, string parameter2Name, object parameter2Value, EncryptionModes encryptionMode = DefaultEncryptionMode)
		{
			return pagePath.AttachQueryParams(new Dictionary<string, object> { { parameter1Name, parameter1Value }, { parameter2Name, parameter2Value } }, encryptionMode);
		}

		public static string AttachQueryParams(this string pagePath, string parameter1Name, object parameter1Value, string parameter2Name, object parameter2Value, string parameter3Name, object parameter3Value, EncryptionModes encryptionMode = DefaultEncryptionMode)
		{
			return pagePath.AttachQueryParams(new Dictionary<string, object> { { parameter1Name, parameter1Value }, { parameter2Name, parameter2Value }, { parameter3Name, parameter3Value } }, encryptionMode);
		}

		public static string AttachQueryParams(this string pagePath, string parameter1Name, object parameter1Value, string parameter2Name, object parameter2Value, string parameter3Name, object parameter3Value, string parameter4Name, object parameter4Value, EncryptionModes encryptionMode = DefaultEncryptionMode)
		{
			return pagePath.AttachQueryParams(new Dictionary<string, object> { { parameter1Name, parameter1Value }, { parameter2Name, parameter2Value }, { parameter3Name, parameter3Value }, { parameter4Name, parameter4Value } }, encryptionMode);
		}

		public static string AttachQueryParams(this string pagePath, string parameter1Name, object parameter1Value, string parameter2Name, object parameter2Value, string parameter3Name, object parameter3Value, string parameter4Name, object parameter4Value, string parameter5Name, object parameter5Value, EncryptionModes encryptionMode = DefaultEncryptionMode)
		{
			return pagePath.AttachQueryParams(new Dictionary<string, object> { { parameter1Name, parameter1Value }, { parameter2Name, parameter2Value }, { parameter3Name, parameter3Value }, { parameter4Name, parameter4Value }, { parameter5Name, parameter5Value } }, encryptionMode);
		}

		public static string AttachQueryParams(this string pagePath, string parameter1Name, object parameter1Value, string parameter2Name, object parameter2Value, string parameter3Name, object parameter3Value, string parameter4Name, object parameter4Value, string parameter5Name, object parameter5Value, string parameter6Name, object parameter6Value, EncryptionModes encryptionMode = DefaultEncryptionMode)
		{
			return pagePath.AttachQueryParams(new Dictionary<string, object> { { parameter1Name, parameter1Value }, { parameter2Name, parameter2Value }, { parameter3Name, parameter3Value }, { parameter4Name, parameter4Value }, { parameter5Name, parameter5Value }, { parameter6Name, parameter6Value } }, encryptionMode);
		}

		public static string AttachQueryParams(this string pagePath, string parameter1Name, object parameter1Value, string parameter2Name, object parameter2Value, string parameter3Name, object parameter3Value, string parameter4Name, object parameter4Value, string parameter5Name, object parameter5Value, string parameter6Name, object parameter6Value, string parameter7Name, object parameter7Value, EncryptionModes encryptionMode = DefaultEncryptionMode)
		{
			return pagePath.AttachQueryParams(new Dictionary<string, object> { { parameter1Name, parameter1Value }, { parameter2Name, parameter2Value }, { parameter3Name, parameter3Value }, { parameter4Name, parameter4Value }, { parameter5Name, parameter5Value }, { parameter6Name, parameter6Value }, { parameter7Name, parameter7Value } }, encryptionMode);
		}

		public static string AttachQueryParams(this string pagePath, string parameter1Name, object parameter1Value, string parameter2Name, object parameter2Value, string parameter3Name, object parameter3Value, string parameter4Name, object parameter4Value, string parameter5Name, object parameter5Value, string parameter6Name, object parameter6Value, string parameter7Name, object parameter7Value, string parameter8Name, object parameter8Value, EncryptionModes encryptionMode = DefaultEncryptionMode)
		{
			return pagePath.AttachQueryParams(new Dictionary<string, object> { { parameter1Name, parameter1Value }, { parameter2Name, parameter2Value }, { parameter3Name, parameter3Value }, { parameter4Name, parameter4Value }, { parameter5Name, parameter5Value }, { parameter6Name, parameter6Value }, { parameter7Name, parameter7Value }, { parameter8Name, parameter8Value } }, encryptionMode);
		}

		public static string AttachQueryParams(this string pagePath, string parameter1Name, object parameter1Value, string parameter2Name, object parameter2Value, string parameter3Name, object parameter3Value, string parameter4Name, object parameter4Value, string parameter5Name, object parameter5Value, string parameter6Name, object parameter6Value, string parameter7Name, object parameter7Value, string parameter8Name, object parameter8Value, string parameter9Name, object parameter9Value, EncryptionModes encryptionMode = DefaultEncryptionMode)
		{
			return pagePath.AttachQueryParams(new Dictionary<string, object> { { parameter1Name, parameter1Value }, { parameter2Name, parameter2Value }, { parameter3Name, parameter3Value }, { parameter4Name, parameter4Value }, { parameter5Name, parameter5Value }, { parameter6Name, parameter6Value }, { parameter7Name, parameter7Value }, { parameter8Name, parameter8Value }, { parameter9Name, parameter9Value } }, encryptionMode);
		}

		public static string AttachQueryParams(this string pagePath, string parameter1Name, object parameter1Value, string parameter2Name, object parameter2Value, string parameter3Name, object parameter3Value, string parameter4Name, object parameter4Value, string parameter5Name, object parameter5Value, string parameter6Name, object parameter6Value, string parameter7Name, object parameter7Value, string parameter8Name, object parameter8Value, string parameter9Name, object parameter9Value, string parameter10Name, object parameter10Value, EncryptionModes encryptionMode = DefaultEncryptionMode)
		{
			return pagePath.AttachQueryParams(new Dictionary<string, object> { { parameter1Name, parameter1Value }, { parameter2Name, parameter2Value }, { parameter3Name, parameter3Value }, { parameter4Name, parameter4Value }, { parameter5Name, parameter5Value }, { parameter6Name, parameter6Value }, { parameter7Name, parameter7Value }, { parameter8Name, parameter8Value }, { parameter9Name, parameter9Value }, { parameter10Name, parameter10Value } }, encryptionMode);
		}

		public static string AttachQueryParams(this string pagePath, string parameter1Name, object parameter1Value, string parameter2Name, object parameter2Value, string parameter3Name, object parameter3Value, string parameter4Name, object parameter4Value, string parameter5Name, object parameter5Value, string parameter6Name, object parameter6Value, string parameter7Name, object parameter7Value, string parameter8Name, object parameter8Value, string parameter9Name, object parameter9Value, string parameter10Name, object parameter10Value, string parameter11Name, object parameter11Value, EncryptionModes encryptionMode = DefaultEncryptionMode)
		{
			return pagePath.AttachQueryParams(new Dictionary<string, object> { { parameter1Name, parameter1Value }, { parameter2Name, parameter2Value }, { parameter3Name, parameter3Value }, { parameter4Name, parameter4Value }, { parameter5Name, parameter5Value }, { parameter6Name, parameter6Value }, { parameter7Name, parameter7Value }, { parameter8Name, parameter8Value }, { parameter9Name, parameter9Value }, { parameter10Name, parameter10Value }, { parameter11Name, parameter11Value } }, encryptionMode);
		}

		public static string AttachQueryParams(this string pagePath, EncryptionModes encryptionMode, params (string Name, object Value)[] parameters)
		{
			var parametersDictionary = parameters?.ToDictionary(p => p.Name, p => p.Value);
			return pagePath.AttachQueryParams(parametersDictionary, encryptionMode);
		}

		public static string AttachQueryParams(this string pagePath, params (string Name, object Value)[] parameters)
		{
			var parametersDictionary = parameters?.ToDictionary(p => p.Name, p => p.Value);
			return pagePath.AttachQueryParams(parametersDictionary);
		}

		public static string AttachQueryParam(this string pagePath, (string Name, object Value) parameter, EncryptionModes encryptionMode = DefaultEncryptionMode)
		{
			return pagePath.AttachQueryParams(encryptionMode, parameter);
		}

		#endregion

		#region GetParameterValue

		public static Dictionary<string, string> GetAllParameters(this HttpRequest request)
		{
			try
			{
				var decryptedQueryString = request.Params[ParameterName].Decrypt();
				if (decryptedQueryString != null)
					return decryptedQueryString
						.Split('&')
						.Select(p => p.Split('='))
						.ToDictionary(kv => HttpUtility.UrlDecode(kv[0]), kv => HttpUtility.UrlDecode(kv[1]));
			}
			catch (AspireEncryptionException exc)
			{
				if (!exc.UserSessionMismatched)
					throw;
			}
			catch
			{
				// ignored
			}
			return new Dictionary<string, string>();
		}

		public static string GetParameterValue(this HttpRequest request, string paramName)
		{
			request.GetAllParameters().TryGetValue(paramName, out var value);
			return value;
		}

		public static T? GetParameterValue<T>(this HttpRequest request, string paramName) where T : struct
		{
			var val = request.GetParameterValue(paramName);
			if (string.IsNullOrEmpty(val))
				return null;
			var typeofT = typeof(T);
			if (typeofT == typeof(Guid))
				return (T)(object)Guid.Parse(val);
			if (typeofT.IsEnum)
				return (T)Enum.Parse(typeofT, val);
			if (typeofT == typeof(TimeSpan))
			{
				if (TimeSpan.TryParse(val, out var timeSpan))
					return (T)(object)timeSpan;
				return null;
			}
			return (T?)Convert.ChangeType(val, typeof(T));
		}

		#endregion
	}
}
