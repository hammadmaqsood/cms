﻿using System;
using System.ComponentModel;
using System.Net;
using System.Runtime.InteropServices;
#pragma warning disable 414
#pragma warning disable 169

namespace Aspire.Lib.Helpers
{
	public sealed class NetworkDrive : IDisposable
	{
		private readonly string _networkName;

		public NetworkDrive(string networkName, NetworkCredential credentials)
		{
			this._networkName = networkName;

			var netResource = new NetResource
			{
				Scope = ResourceScope.GlobalNetwork,
				ResourceType = ResourceType.Disk,
				DisplayType = ResourceDisplaytype.Share,
				RemoteName = networkName
			};

			var userName = string.IsNullOrEmpty(credentials.Domain)
				? credentials.UserName
				: $@"{credentials.Domain}\{credentials.UserName}";

			var result = WNetAddConnection2(netResource, credentials.Password, userName, 0);

			if (result != 0)
				throw new Win32Exception(result, "Error connecting to remote share.");
		}

		~NetworkDrive()
		{
			this.Dispose(false);
		}

		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			if (disposing)
				WNetCancelConnection2(this._networkName, 0, true);
		}

		[DllImport("mpr.dll")]
		private static extern int WNetAddConnection2(NetResource netResource, string password, string username, int flags);

		[DllImport("mpr.dll")]
		private static extern int WNetCancelConnection2(string name, int flags, bool force);

		[StructLayout(LayoutKind.Sequential)]
		private class NetResource
		{
			public ResourceScope Scope;
			public ResourceType ResourceType;
			public ResourceDisplaytype DisplayType;
			public int Usage;
			public string LocalName;
			public string RemoteName;
			public string Comment;
			public string Provider;
		}

		private enum ResourceScope
		{
			GlobalNetwork = 2,
		}

		private enum ResourceType
		{
			Disk = 1,
		}

		private enum ResourceDisplaytype
		{
			Share = 0x03,
		}
	}
}
