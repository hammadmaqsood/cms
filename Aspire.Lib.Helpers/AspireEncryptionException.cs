using System;

namespace Aspire.Lib.Helpers
{
	public class AspireEncryptionException : Exception
	{
		public bool UserSessionMismatched { get; }
		public AspireEncryptionException(bool userSessionMismatched, string message) : base(message)
		{
			this.UserSessionMismatched = false;
		}
	}
}