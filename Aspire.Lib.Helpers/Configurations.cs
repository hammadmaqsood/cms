﻿using System;
using System.Configuration;

namespace Aspire.Lib.Helpers
{
	public static class Configurations
	{
		public interface IAppSettings
		{
			bool DEBUG { get; }
			string Application { get; }
			bool IsDevelopmentPC { get; }
			string RecaptchaPublic { get; }
			string RecaptchaPrivate { get; }
			string AspireFileDirectory { get; }
			string FileServerBasePath { get; }
			Guid BUMDCServiceAuth { get; }
			string BUMDCServiceURL { get; }
			string PernService { get; }
			string PernClientID { get; }
		}

		public interface IConnectionStrings
		{
			string AspireContext { get; }
		}

		public interface IConfigurations
		{
			IAppSettings AppSettings { get; }
			IConnectionStrings ConnectionStrings { get; }
		}

		private sealed class WebConfigurations : IConfigurations
		{
			private sealed class WebConfigurationsAppSettings : IAppSettings
			{
				public bool DEBUG
				{
					get
					{
#if DEBUG
						return true;
#else
						return false;
#endif
					}
				}
				public string Application { get; } = ConfigurationManager.AppSettings[nameof(Application)];
				public bool IsDevelopmentPC { get; } = ConfigurationManager.AppSettings[nameof(IsDevelopmentPC)]?.ToLower() == true.ToString().ToLower();
				public string RecaptchaPublic { get; } = ConfigurationManager.AppSettings[nameof(RecaptchaPublic)];
				public string RecaptchaPrivate { get; } = ConfigurationManager.AppSettings[nameof(RecaptchaPrivate)];
				public string AspireFileDirectory { get; } = ConfigurationManager.AppSettings[nameof(AspireFileDirectory)];
				public string FileServerBasePath { get; } = ConfigurationManager.AppSettings[nameof(FileServerBasePath)];
				public Guid BUMDCServiceAuth { get; } = Guid.Parse(ConfigurationManager.AppSettings[nameof(BUMDCServiceAuth)]);
				public string BUMDCServiceURL { get; } = ConfigurationManager.AppSettings[nameof(BUMDCServiceURL)];
				public string PernService { get; } = ConfigurationManager.AppSettings[nameof(PernService)];
				public string PernClientID { get; } = ConfigurationManager.AppSettings[nameof(PernClientID)];
			}

			private sealed class WebConfigurationsConnectionStrings : IConnectionStrings
			{
				public string AspireContext { get; } = ConfigurationManager.ConnectionStrings[nameof(AspireContext)].ConnectionString;
			}

			public IAppSettings AppSettings { get; } = new WebConfigurationsAppSettings();

			public IConnectionStrings ConnectionStrings { get; } = new WebConfigurationsConnectionStrings();
		}

		public static IConfigurations Current { get; } = new WebConfigurations();
	}
}
