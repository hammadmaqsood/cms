﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Aspire.Lib.Helpers
{
	public static class ExcelHelper
	{
		public enum ExcelFormat
		{
			Xls,
			Xlsx,
		}

		private static string GetExcelVersion(this ExcelFormat excelFormat)
		{
			switch (excelFormat)
			{
				case ExcelFormat.Xls:
					return "8.0";
				case ExcelFormat.Xlsx:
					return "12.0";
				default:
					throw new ArgumentOutOfRangeException(nameof(excelFormat), excelFormat, null);
			}
		}

		public static string GetOledbConnectionString(string fileName, ExcelFormat excelFormat = ExcelFormat.Xls, bool hdr = true, int imex = 1)
		{
			return new OleDbConnectionStringBuilder
			{
				{"Provider", "Microsoft.ACE.OLEDB.12.0"},
				{"Data Source", fileName},
				{"Extended Properties", $"Excel {excelFormat.GetExcelVersion()};HDR={(hdr ? "YES" : "NO")};IMEX={imex}"},
			}.ConnectionString;
		}

		public static bool TestConnection(string connectionString, out string errorMessage)
		{
			try
			{
				using (var oledbConn = new OleDbConnection(connectionString))
				{
					oledbConn.Open();
					oledbConn.Close();
					errorMessage = null;
					return true;
				}
			}

			catch (Exception exc)
			{
				errorMessage = exc.Message;
				return false;
			}
		}

		public static List<string> GetAllSheetNames(string connectionString)
		{
			using (var oledbConn = new OleDbConnection(connectionString))
			{
				oledbConn.Open();
				using (var dataTable = oledbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null))
				{
					return dataTable?.AsEnumerable().Select(r => (string)r["TABLE_NAME"]).ToList();
				}
			}
		}

		public static DataTable GetData(string connectionString, string sheetName)
		{
			using (var oledbAdapter = new OleDbDataAdapter($"SELECT * FROM [{sheetName}]", connectionString))
			{
				var dataTable = new DataTable();
				oledbAdapter.Fill(dataTable);
				return dataTable;
			}
		}

		public sealed class ExcelColumns
		{
			public int DisplayIndex { get; set; }
			public string HeaderText { get; set; }
			public string PropertyName { get; set; }
			public string Format { get; set; }
		}

		private static List<ExcelColumns> GetDefault<T>(this List<T> list)
		{
			var displayIndex = 0;
			return typeof(T).GetProperties().Select(p => new ExcelColumns
			{
				Format = null,
				PropertyName = p.Name,
				DisplayIndex = displayIndex++,
				HeaderText = p.Name,
			}).ToList();
		}

		private static List<ExcelColumns> GetDefault(this DataTable dataTable)
		{
			var displayIndex = 0;
			return dataTable.Columns.Cast<DataColumn>().Select(c => new ExcelColumns
			{
				Format = null,
				PropertyName = c.ColumnName,
				DisplayIndex = displayIndex++,
				HeaderText = c.Caption ?? c.ColumnName,
			}).ToList();
		}

		public static void ConvertToExcel<T>(this List<T> list, Stream writeToStream, string sheetName = null, List<ExcelColumns> columns = null)
		{
			using (var document = SpreadsheetDocument.Create(writeToStream, SpreadsheetDocumentType.Workbook))
			{
				var workbookPart = document.AddWorkbookPart();
				var stylesheet = workbookPart.AddNewPart<WorkbookStylesPart>();
				var fonts = new Fonts(new Font(), new Font(new Bold()));
				var fills = new Fills(new Fill());
				var borders = new Borders(new Border());

				OpenXmlElement[] cellFormats =
				{
					new CellFormat {FontId = 0, FillId = 0, BorderId = 0},//Normal Cell
					new CellFormat {FontId = 1},//Bold Cell
				};
				stylesheet.Stylesheet = new Stylesheet(fonts, fills, borders, new CellFormats(cellFormats));
				stylesheet.Stylesheet.Save();

				var stringCellStyleIndex = UInt32Value.FromUInt32(0);
				var headerCellStyleIndex = UInt32Value.FromUInt32(1);

				columns = (columns ?? list.GetDefault()).OrderBy(c => c.DisplayIndex).ToList();

				var sheetData = new SheetData();
				var propertyInfos = typeof(T).GetProperties();
				var headerRow = sheetData.AppendChild(new Row());
				foreach (var excelColumn in columns)
					headerRow.AppendChild(new Cell
					{
						DataType = CellValues.String,
						StyleIndex = headerCellStyleIndex,
						CellValue = new CellValue(excelColumn.HeaderText),
					});

				foreach (var row in list)
				{
					var dataRow = sheetData.AppendChild(new Row());
					foreach (var excelColumn in columns)
					{
						var cell = dataRow.AppendChild(new Cell());
						var propInfo = propertyInfos.Single(p => p.Name == excelColumn.PropertyName);
						var obj = propInfo.GetValue(row);
						switch (Type.GetTypeCode(propInfo.PropertyType))
						{
							case TypeCode.Empty:
							case TypeCode.Object:
							case TypeCode.DBNull:
							case TypeCode.Char:
							case TypeCode.String:
							case TypeCode.Boolean:
								cell.StyleIndex = stringCellStyleIndex;
								cell.DataType = new EnumValue<CellValues>(CellValues.String);
								cell.CellValue = obj != null ? new CellValue(obj.ToString()) : new CellValue();
								break;
							case TypeCode.SByte:
							case TypeCode.Byte:
							case TypeCode.Int16:
							case TypeCode.UInt16:
							case TypeCode.Int32:
							case TypeCode.UInt32:
							case TypeCode.Int64:
							case TypeCode.UInt64:
							case TypeCode.Single:
							case TypeCode.Double:
							case TypeCode.Decimal:
								cell.StyleIndex = stringCellStyleIndex;
								cell.DataType = new EnumValue<CellValues>(CellValues.Number);
								if (obj != null)
									cell.CellValue = string.IsNullOrWhiteSpace(excelColumn.Format)
										? new CellValue(Convert.ToDecimal(obj).ToString(CultureInfo.InvariantCulture))
										: new CellValue(Convert.ToDecimal(obj).ToString(excelColumn.Format, CultureInfo.InvariantCulture));
								else
									cell.CellValue = new CellValue();
								break;
							case TypeCode.DateTime:
								cell.StyleIndex = stringCellStyleIndex;
								cell.DataType = new EnumValue<CellValues>(CellValues.String);
								if (obj != null)
									cell.CellValue = string.IsNullOrWhiteSpace(excelColumn.Format)
										? new CellValue(Convert.ToDateTime(obj).ToString(CultureInfo.InvariantCulture))
										: new CellValue(Convert.ToDateTime(obj).ToString(excelColumn.Format, CultureInfo.InvariantCulture));
								else
									cell.CellValue = new CellValue();
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}
					}
				}

				workbookPart.Workbook = new Workbook();
				var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
				worksheetPart.Worksheet = new Worksheet(sheetData);
				workbookPart.Workbook.AppendChild(new Sheets(new Sheet { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = sheetName ?? "Sheet1" }));
				workbookPart.Workbook.Save();
				document.Close();
			}
		}

		public static void ConvertToExcel(this DataTable dataTable, Stream writeToStream, string sheetName = null, List<ExcelColumns> columns = null)
		{
			using (var document = SpreadsheetDocument.Create(writeToStream, SpreadsheetDocumentType.Workbook))
			{
				var workbookPart = document.AddWorkbookPart();
				var stylesheet = workbookPart.AddNewPart<WorkbookStylesPart>();
				var fonts = new Fonts(new Font(), new Font(new Bold()));
				var fills = new Fills(new Fill());
				var borders = new Borders(new Border());

				OpenXmlElement[] cellFormats =
				{
					new CellFormat {FontId = 0, FillId = 0, BorderId = 0},//Normal Cell
					new CellFormat {FontId = 1},//Bold Cell
				};
				stylesheet.Stylesheet = new Stylesheet(fonts, fills, borders, new CellFormats(cellFormats));
				stylesheet.Stylesheet.Save();

				var stringCellStyleIndex = UInt32Value.FromUInt32(0);
				var headerCellStyleIndex = UInt32Value.FromUInt32(1);

				columns = (columns ?? dataTable.GetDefault()).OrderBy(c => c.DisplayIndex).ToList();

				var sheetData = new SheetData();
				var headerRow = sheetData.AppendChild(new Row());
				foreach (var excelColumn in columns)
					headerRow.AppendChild(new Cell
					{
						DataType = CellValues.String,
						StyleIndex = headerCellStyleIndex,
						CellValue = new CellValue(excelColumn.HeaderText),
					});

				foreach (var row in dataTable.AsEnumerable())
				{
					var dataRow = sheetData.AppendChild(new Row());
					foreach (var excelColumn in columns)
					{
						var cell = dataRow.AppendChild(new Cell());
						var obj = row[excelColumn.PropertyName];
						switch (Type.GetTypeCode(dataTable.Columns[excelColumn.PropertyName].DataType))
						{
							case TypeCode.Empty:
							case TypeCode.Object:
							case TypeCode.DBNull:
							case TypeCode.Char:
							case TypeCode.String:
							case TypeCode.Boolean:
								cell.StyleIndex = stringCellStyleIndex;
								cell.DataType = new EnumValue<CellValues>(CellValues.String);
								cell.CellValue = obj != DBNull.Value ? new CellValue(obj.ToString()) : new CellValue("");
								break;
							case TypeCode.SByte:
							case TypeCode.Byte:
							case TypeCode.Int16:
							case TypeCode.UInt16:
							case TypeCode.Int32:
							case TypeCode.UInt32:
							case TypeCode.Int64:
							case TypeCode.UInt64:
							case TypeCode.Single:
							case TypeCode.Double:
							case TypeCode.Decimal:
								cell.StyleIndex = stringCellStyleIndex;
								cell.DataType = new EnumValue<CellValues>(CellValues.Number);
								if (obj != DBNull.Value)
									cell.CellValue = string.IsNullOrWhiteSpace(excelColumn.Format)
										? new CellValue(Convert.ToDecimal(obj).ToString(CultureInfo.InvariantCulture))
										: new CellValue(Convert.ToDecimal(obj).ToString(excelColumn.Format, CultureInfo.InvariantCulture));
								else
									cell.CellValue = new CellValue("");
								break;
							case TypeCode.DateTime:
								cell.StyleIndex = stringCellStyleIndex;
								cell.DataType = new EnumValue<CellValues>(CellValues.String);
								if (obj != DBNull.Value)
									cell.CellValue = string.IsNullOrWhiteSpace(excelColumn.Format)
										? new CellValue(Convert.ToDateTime(obj).ToString(CultureInfo.InvariantCulture))
										: new CellValue(Convert.ToDateTime(obj).ToString(excelColumn.Format, CultureInfo.InvariantCulture));
								else
									cell.CellValue = new CellValue("");
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}
					}
				}

				workbookPart.Workbook = new Workbook();
				var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
				worksheetPart.Worksheet = new Worksheet(sheetData);
				workbookPart.Workbook.AppendChild(new Sheets(new Sheet { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = sheetName ?? dataTable.TableName ?? "Sheet1" }));
				workbookPart.Workbook.Save();
				document.Close();
			}
		}
	}
}