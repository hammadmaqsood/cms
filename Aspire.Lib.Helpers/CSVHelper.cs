﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Lib.Helpers
{
	public static class CSVHelper
	{
		private static readonly string NewLine = Environment.NewLine;

		private static string GetElementForCSV(object obj)
		{
			var value = obj?.ToString() ?? string.Empty;
			return value.IndexOfAny(new[] { '\"', ',', '\n', '\r' }) >= 0
				? $"\"{value.Replace("\"", "\"\"")}\""
				: $@"{value}";
		}

		public static string ConvertToCSVFormat(this IEnumerable<IEnumerable<object>> list)
		{
			return string.Join(NewLine, list.Select(row => string.Join(",", row.Select(GetElementForCSV))));
		}

		public sealed class CSVColumnMapping
		{
			public int ColumnIndex { get; set; }
			public string ColumnName { get; set; }
			public string PropertyName { get; set; }
			public Func<object, string> Format { get; set; }
		}

		public static string ConvertToCSVFormat<T>(this IEnumerable<T> list, IEnumerable<CSVColumnMapping> mappings) where T : class
		{
			var propertyInfos = typeof(T).GetProperties();
			var selectedProperties = mappings.Select(mapping =>
			{
				var propertyInfo = propertyInfos.SingleOrDefault(p => p.Name == mapping.PropertyName);
				if (propertyInfo == null)
					throw new InvalidOperationException($"Property \"{mapping.PropertyName}\" not found.");
				return new { Mapping = mapping, PropertyInfo = propertyInfo };
			}).OrderBy(p => p.Mapping.ColumnIndex).ToList();

			IEnumerable<object> GetHeaderRow() => selectedProperties.Select(p => p.Mapping.ColumnName);

			IEnumerable<object> GetDataRow(object item)
			{
				foreach (var property in selectedProperties)
				{
					var value = property.PropertyInfo.GetValue(item);
					if (property.Mapping.Format != null)
						yield return property.Mapping.Format(value);
					else
						yield return value;
				}
			}

			IEnumerable<IEnumerable<object>> GetRows()
			{
				yield return GetHeaderRow();
				foreach (var item in list)
					yield return GetDataRow(item);
			}

			return GetRows().ConvertToCSVFormat();
		}

		public static string ConvertToCSVFormat<T>(this IEnumerable<T> list) where T : class
		{
			var columnIndex = 0;
			var mappings = typeof(T).GetProperties().Select(p => new CSVColumnMapping
			{
				PropertyName = p.Name,
				ColumnName = p.Name,
				ColumnIndex = columnIndex++,
				Format = null
			});
			return list.ConvertToCSVFormat(mappings);
		}

		private static string ToNullIfEmptyRequired(this string input, bool convertEmptyStringsToNull)
		{
			return convertEmptyStringsToNull && string.IsNullOrEmpty(input) ? null : input;
		}

		public static IEnumerable<IEnumerable<string>> ParseCSV(this string input, bool convertEmptyStringsToNull = false)
		{
			var rows = new List<List<string>>();
			if (!string.IsNullOrEmpty(input))
			{
				var cells = new List<string>();
				var currentCell = string.Empty;

				string GetCellValue()
				{
					if (currentCell == null)
						throw new InvalidOperationException();
					if (currentCell.Length > 1 && currentCell.StartsWith("\"") && currentCell.EndsWith("\""))
						return currentCell.Substring(1, currentCell.Length - 2).Replace("\"\"", "\"").ToNullIfEmptyRequired(convertEmptyStringsToNull);
					if (currentCell.StartsWith("\""))
						return currentCell.Substring(1).Replace("\"\"", "\"").ToNullIfEmptyRequired(convertEmptyStringsToNull);
					return currentCell.ToNullIfEmptyRequired(convertEmptyStringsToNull);
				}
				foreach (var currentChar in input)
				{
					switch (currentChar)
					{
						case ',':
							if (currentCell.StartsWith("\""))
							{
								var temp = currentCell.Substring(1).Replace("\"\"", " ");
								if (temp.Length > 1 && temp.EndsWith("\""))
								{
									cells.Add(GetCellValue());
									currentCell = string.Empty;
								}
								else
									currentCell += currentChar;
							}
							else
							{
								cells.Add(GetCellValue());
								currentCell = string.Empty;
							}
							break;
						case '\r':
							break;
						case '\n':
							if (currentCell.StartsWith("\""))
							{
								var temp = currentCell.Substring(1).Replace("\"\"", " ");
								if (temp.Length > 1 && temp.EndsWith("\""))
								{
									cells.Add(GetCellValue());
									rows.Add(cells);
									currentCell = string.Empty;
									cells = new List<string>();
								}
								else
									currentCell += currentChar;
							}
							else
							{
								cells.Add(GetCellValue());
								rows.Add(cells);
								currentCell = string.Empty;
								cells = new List<string>();
							}
							break;
						case '\"':
							currentCell += currentChar;
							break;
						default:
							currentCell += currentChar;
							break;
					}
				}

				if (cells.Count == 0 && currentCell == string.Empty)
					return rows;

				cells.Add(GetCellValue());
				rows.Add(cells);
				return rows;
			}
			return rows;
		}
	}
}