﻿using Aspire.Lib.Extensions.Exceptions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;

namespace Aspire.Lib.Helpers
{
	public static class ExcelFileHelper
	{
		public enum OledbProviders
		{
			Oledb12
		}

		public static string GetProviderName(this OledbProviders oledbProvider)
		{
			switch (oledbProvider)
			{
				case OledbProviders.Oledb12:
					return "Microsoft.ACE.OLEDB.12.0";
				default:
					throw new NotImplementedEnumException(oledbProvider);
			}
		}

		public static string GetExcelVersion(this string fileName)
		{
			if (fileName.EndsWith(".xls", StringComparison.InvariantCultureIgnoreCase))
				return "8.0";
			if (fileName.EndsWith(".xlsx", StringComparison.InvariantCultureIgnoreCase))
				return "12.0";
			throw new NotSupportedException(fileName);
		}

		public static string GetOledbConnectionString(OledbProviders oledbProvider, string fileName, string fileExtension, bool hdr = true, uint imex = 1)
		{
			return new OleDbConnectionStringBuilder
			{
				{"Provider", oledbProvider.GetProviderName()},
				{"Data Source", fileName},
				{"Extended Properties", $"Excel {fileExtension.GetExcelVersion()};HDR={(hdr ? "YES" : "NO")};IMEX={imex}"},
			}.ConnectionString;
		}

		public static bool TestConnection(string connectionString, out string errorMessage)
		{
			try
			{
				using (var oledbConn = new OleDbConnection(connectionString))
				{
					oledbConn.Open();
					errorMessage = null;
					return true;
				}
			}

			catch (Exception exc)
			{
				errorMessage = exc.Message;
				return false;
			}
		}

		public static List<string> GetSheetNames(string connectionString)
		{
			using (var oledbConn = new OleDbConnection(connectionString))
			{
				oledbConn.Open();
				using (var dataTable = oledbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null))
				{
					return dataTable?.AsEnumerable().Select(r => (string)r["TABLE_NAME"]).ToList();
				}
			}
		}

		public static DataTable GetSheetData(string connectionString, string sheetName)
		{
			using (var oledbConnection = new OleDbConnection(connectionString))
			using (var oledbCommand = oledbConnection.CreateCommand())
			{
				oledbCommand.CommandText = $"SELECT * FROM [{sheetName.Replace("]", "]]")}]";
				oledbCommand.Parameters.AddWithValue("@TableName", sheetName);
				oledbConnection.Open();
				var dataTable = new DataTable(sheetName);
				using (var oledbDataReader = oledbCommand.ExecuteReader())
				{
					dataTable.Load(oledbDataReader);
				}
				return dataTable;
			}
		}
	}
}
