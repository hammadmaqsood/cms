﻿namespace Aspire.Lib.Helpers
{
	public enum EncryptionModes
	{
		/// <summary>
		/// Every times output will be different and User Session will not be validation.
		/// </summary>
		ConstantSaltNoneUserSessionNone,
		/// <summary>
		/// Every times output will be different and User Session will be validated.
		/// </summary>
		ConstantSaltNoneUserSession,
		/// <summary>
		/// Every times output will be same and User Session will not be validation.
		/// </summary>
		ConstantSaltUserSessionNone,
		/// <summary>
		/// Every times output will be same and User Session will be validated.
		/// </summary>
		ConstantSaltUserSession,
	}
}