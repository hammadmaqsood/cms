﻿using Aspire.Lib.PasswordEncryptor;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Aspire.Lib.Helpers
{
	public static class EncryptionHelper
	{
		public static string EncryptPassword(this string password)
		{
			return PasswordHash.CreateHash(password);
		}

		public static bool CheckPassword(this string plainPassword, string hashedPassword)
		{
			try
			{
				return PasswordHash.ValidatePassword(plainPassword, hashedPassword);
			}
			catch
			{
				return false;
			}
		}

		private static readonly byte[] Key = { 0xD9, 0x11, 0x1D, 0x1E, 0xC8, 0xB4, 0x71, 0x4B, 0x44, 0x1, 0x6B, 0x92, 0x20, 0x8D, 0xD7, 0xFC, 0x47, 0xDB, 0x36, 0xE9, 0xF5, 0x42, 0x98, 0x82, 0x3C, 0x22, 0xDD, 0x86, 0x7C, 0x9B, 0xFA, 0x5A, };
		private static readonly byte[] IV = { 0x3D, 0x7F, 0x60, 0x7E, 0x22, 0xDE, 0x8, 0x26, 0x1B, 0x7B, 0xE9, 0x3C, 0x5D, 0x7B, 0x11, 0xD2, };

		private static readonly AesManaged AesManaged = new AesManaged
		{
			KeySize = Key.Length * 8,
			Key = Key,
			IV = IV,
			Mode = CipherMode.CBC,
			Padding = PaddingMode.PKCS7,
		};

		private static readonly ICryptoTransform Encryptor = AesManaged.CreateEncryptor();
		private static readonly ICryptoTransform Decryptor = AesManaged.CreateDecryptor();
		private static readonly string GuidFormat = "D";
		private static readonly int GuidLength = Guid.Empty.ToString(GuidFormat).Length;

		private static byte[] _Encrypt(this string plainText)
		{
			if (plainText == null)
				throw new ArgumentNullException(nameof(plainText));
			using (var memoryStream = new MemoryStream())
			{
				using (var cryptoStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write))
				{
					var rawPlainText = Encoding.Unicode.GetBytes(plainText);
					cryptoStream.Write(rawPlainText, 0, rawPlainText.Length);
				}
				return memoryStream.ToArray();
			}
		}

		private static string _Decrypt(this byte[] cipherText)
		{
			using (var memoryStream = new MemoryStream())
			{
				using (var cryptoStream = new CryptoStream(memoryStream, Decryptor, CryptoStreamMode.Write))
				{
					cryptoStream.Write(cipherText, 0, cipherText.Length);
				}
				return Encoding.Unicode.GetString(memoryStream.ToArray());
			}
		}

		private static Guid? CurrentUserSessionGuid
		{
			get
			{
				var identity = HttpContext.Current.Session["$AspireIdentity$"] as IAspireIdentity;
				return identity?.LoginSessionGuid;
			}
		}

		public static string Encrypt(this string plainText, EncryptionModes encryptionModes = EncryptionModes.ConstantSaltNoneUserSession)
		{
			Guid salt;
			Guid session;
			switch (encryptionModes)
			{
				case EncryptionModes.ConstantSaltNoneUserSessionNone:
					salt = Guid.NewGuid();
					session = Guid.Empty;
					break;
				case EncryptionModes.ConstantSaltNoneUserSession:
					salt = Guid.NewGuid();
					session = CurrentUserSessionGuid ?? throw new InvalidOperationException("User Session not found.");
					break;
				case EncryptionModes.ConstantSaltUserSessionNone:
					salt = Guid.Empty;
					session = Guid.Empty;
					break;
				case EncryptionModes.ConstantSaltUserSession:
					salt = Guid.Empty;
					session = CurrentUserSessionGuid ?? throw new InvalidOperationException("User Session not found.");
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(encryptionModes), encryptionModes, null);
			}
			var text = salt.ToString(GuidFormat) + session.ToString(GuidFormat) + plainText;
			return Convert.ToBase64String(text._Encrypt());
		}

		public static string Decrypt(this string cipherText)
		{
			try
			{
				if (string.IsNullOrEmpty(cipherText))
					return cipherText;
				var cipherBytes = Convert.FromBase64String(cipherText);
				var decrypted = cipherBytes._Decrypt();
				var saltRemovedDecrypted = decrypted.Substring(EncryptionHelper.GuidLength);
				var sessionGuidText = saltRemovedDecrypted.Substring(0, EncryptionHelper.GuidLength);
				var sessionGuid = Guid.ParseExact(sessionGuidText, GuidFormat);
				var decryptedText = saltRemovedDecrypted.Substring(EncryptionHelper.GuidLength);
				if (sessionGuid == Guid.Empty)
					return decryptedText;
				if (CurrentUserSessionGuid == null)
					throw new AspireEncryptionException(true, "User Session not found.");
				if (sessionGuid != CurrentUserSessionGuid.Value)
					throw new AspireEncryptionException(true, "User Session mismatched.");
				return decryptedText;
			}
			catch (Exception exc)
			{
				throw new AspireEncryptionException(false, $"{exc.Message}, Input: [{cipherText}]");
			}
		}

		public static bool TryDecrypt(this string cipherText, out string decryptedText)
		{
			try
			{
				decryptedText = cipherText.Decrypt();
				return true;
			}
			catch (Exception)
			{
				decryptedText = null;
				return false;
			}
		}
	}
}