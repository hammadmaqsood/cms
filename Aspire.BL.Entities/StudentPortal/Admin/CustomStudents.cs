﻿using Aspire.Model.Entities.Common;
using System;

namespace Aspire.BL.Entities.StudentPortal.Admin
{
	public class CustomStudents
	{
		public int StudentID { get; set; }
		public string PersonalEmail { get; set; }
		public string UniversityEmail { get; set; }
		public string Enrollment { get; set; }
		public byte Gender { get; set; }
		public string InstituteAlias { get; set; }
		public string Name { get; set; }
		public string FatherName { get; set; }
		public string ProgramAlias { get; set; }
		public short SemesterID { get; set; }
		public Guid ConfirmationCode { get; set; }
		public DateTime ConfirmationCodeExpiryDate { get; set; }

		public string Semester => this.SemesterID.ToSemesterString();
		public string GenderFullName => ((Genders)this.Gender).ToFullName();
		public string Email => this.UniversityEmail ?? this.PersonalEmail;
	}
}