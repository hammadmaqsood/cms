﻿namespace Aspire.BL.Entities.QualityAssurance.Common
{
	public enum SurveySubmissionResults
	{
		Success,
		AlreadyExists,
		NotOpenedYet,
		SurveyClosed,
		NoRecordFound
	}
}
