﻿namespace Aspire.BL.Entities.QualityAssurance.Common
{
	public enum SurveyStatus
	{
		Closed,
		NoRecordFound,
		NotYetOpened,
		Open,
		AlreadyCompleted
	}
}