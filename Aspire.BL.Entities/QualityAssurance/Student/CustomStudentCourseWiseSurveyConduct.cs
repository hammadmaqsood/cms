using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;

namespace Aspire.BL.Entities.QualityAssurance.Student
{
	public class CustomStudentSurveyConduct
	{
		public int SurveyConductID { get; set; }
		public int StudentID { get; set; }
		public int? RegisteredCourseID { get; set; }
		public short SemesterID { get; set; }
		public string SurveyName { get; set; }
		public string Title { get; set; }
		public string FacultyMemberName { get; set; }
		public string Program { get; set; }
		public short? SemesterNo { get; set; }
		public int? Section { get; set; }
		public byte? Shift { get; set; }
		public byte SurveyType { get; set; }
		public Survey.SurveyTypes SurveyTypeEnum => (Survey.SurveyTypes)this.SurveyType;

		public string Semester => this.SemesterID.ToSemesterString();
		public string ClassName => this.Program != null ? AspireFormats.GetClassName(this.Program, this.SemesterNo.Value, this.Section.Value, this.Shift.Value) : null;
	}
}