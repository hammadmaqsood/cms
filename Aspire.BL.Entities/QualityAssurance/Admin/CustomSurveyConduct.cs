﻿using System;
using Aspire.Lib.Extensions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;

namespace Aspire.BL.Entities.QualityAssurance.Admin
{
	public class CustomSurveyConduct
	{
		public int SurveyConductID { get; set; }
		public string InstituteAlias { get; set; }
		public short SemesterID { get; set; }
		public string SurveyName { get; set; }
		public byte Type { get; set; }
		public DateTime OpenDate { get; set; }
		public DateTime? CloseDate { get; set; }
		public byte Status { get; set; }

		public string SemesterFullName => this.SemesterID.ToSemesterString();
		public string TypeFullName => ((SurveyConduct.SurveyConductTypes)this.Type).ToString().SplitCamelCasing();
		public string StatusEnumFullName => ((SurveyConduct.Statuses) this.Status).ToString();
	}
}
