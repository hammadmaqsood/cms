﻿using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;

namespace Aspire.BL.Entities.ClassAttendance
{
	public class MarkClassAttendance
	{
		public class Student
		{
			public int? OfferedCourseAttendanceID { get; }
			public int RegisteredCourseID { get; }
			public decimal Hours { get; }
			public bool FeeDefaulter { get; }
			public OfferedCourseAttendance.HoursValues HoursValue => this.Hours.ToHoursValue();

			public Student(int registeredCourseID, int? offeredCourseAttendanceID, OfferedCourseAttendance.HoursValues hours, bool feeDefaulter)
			{
				this.RegisteredCourseID = registeredCourseID;
				this.OfferedCourseAttendanceID = offeredCourseAttendanceID;
				this.Hours = hours.ToDecimal();
				this.FeeDefaulter = feeDefaulter;
			}
		}

		public int? OfferedCourseAttendanceID { get; set; }
		public int OfferedCourseID { get; set; }
		public DateTime ClassDate { get; set; }
		public byte ClassType { get; set; }
		public decimal Hours { get; }
		public OfferedCourseAttendance.HoursValues HoursValue => this.Hours.ToHoursValue();

		public int RoomID { get; set; }
		public string Remarks { get; set; }
		public string TopicsCovered { get; set; }
		private readonly List<Student> _students = new List<Student>();
		public IReadOnlyCollection<Student> Students => this._students.AsReadOnly();

		public MarkClassAttendance(OfferedCourseAttendance.HoursValues hours)
		{
			this.Hours = hours.ToDecimal();
		}

		public void AddStudent(int registeredCourseID, OfferedCourseAttendance.HoursValues hours, bool feeDefaulter)
		{
			if (!this.HoursValue.GetSmallerOrEqualHourValue().Contains(hours))
				throw new InvalidOperationException();
			this._students.Add(new Student(registeredCourseID, this.OfferedCourseAttendanceID, hours, feeDefaulter));
		}
	}
}
