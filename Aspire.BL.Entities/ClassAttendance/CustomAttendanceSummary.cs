using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;

namespace Aspire.BL.Entities.ClassAttendance
{
	public class CustomAttendanceSummary
	{
		public int OfferedCourseID { get; set; }
		public short SemesterID { get; set; }
		public string CourseCode { get; set; }
		public string Title { get; set; }
		public decimal ContactHours { get; set; }
		public decimal CreditHours { get; set; }
		public string Program { get; set; }
		public short SemesterNo { get; set; }
		public int Section { get; set; }
		public byte Shift { get; set; }
		public string TeacherName { get; set; }
		public string ClassName => AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift);
		public decimal TotalHours { get; set; }

		public class Detail
		{
			public int RegisteredCourseID { get; set; }
			public int OfferedCourseID { get; set; }
			public int StudentID { get; set; }
			public string Enrollment { get; set; }
			public string Name { get; set; }
			public string Program { get; set; }
			public decimal TotalHours { get; set; }
			public decimal PresentHours { get; set; }
			public decimal AbsentHours => this.TotalHours - this.PresentHours;
			public decimal Percentage => this.TotalHours != 0 ? this.PresentHours * 100 / this.TotalHours : 100;
			public byte AllowedPercentage { get; set; }
			public bool Danger => this.Percentage < this.AllowedPercentage;
			public byte? FeeStatus { get; set; }
			public bool FeePaid
			{
				get
				{
					switch ((StudentFee.Statuses?)this.FeeStatus)
					{
						case null:
						case StudentFee.Statuses.NotPaid:
							return false;
						case StudentFee.Statuses.Paid:
							return true;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
		}

		public List<Detail> Summary { get; set; }
	}
}