﻿using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;

namespace Aspire.BL.Entities.ClassAttendance
{
	public class CustomViewClassAttendance
	{
		public int InstituteID { get; set; }
		public int OfferedCourseID { get; set; }
		public short SemesterID { get; set; }
		public string CourseCode { get; set; }
		public string Title { get; set; }
		public decimal ContactHours { get; set; }
		public decimal CreditHours { get; set; }
		public string Program { get; set; }
		public short SemesterNo { get; set; }
		public int Section { get; set; }
		public byte Shift { get; set; }
		public string TeacherName { get; set; }
		public string ClassName => AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift);

		public class Detail
		{
			public int InstituteID { get; set; }
			public int OfferedCourseAttendanceID { get; set; }
			public int OfferedCourseID { get; set; }
			public DateTime ClassDate { get; set; }
			public byte ClassType { get; set; }
			public decimal Hours { get; set; }
			public string HoursValueFullName => this.Hours.ToHoursValue().ToFullName(false);
			public string RoomName { get; set; }
			public string Remarks { get; set; }
			public string TopicsCovered { get; set; }
			public DateTime MarkedDate { get; set; }
			public string ClassTypeFullName => ((OfferedCourseAttendance.ClassTypes)this.ClassType).ToString();
		}

		public List<Detail> Details { get; set; }
	}
}
