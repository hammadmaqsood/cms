﻿using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;

namespace Aspire.BL.Entities.ClassAttendance
{
	public class CustomOfferedCourseAttendance
	{
		public int OfferedCourseID { get; set; }
		public int ProgramID { get; set; }
		public string Program { get; set; }
		public short OfferedSemesterID { get; set; }
		public int CourseID { get; set; }
		public string CourseCode { get; set; }
		public string Title { get; set; }
		public decimal ContactHours { get; set; }
		public decimal CreditHours { get; set; }
		public short SemesterNo { get; set; }
		public int Section { get; set; }
		public byte Shift { get; set; }
		public string Remarks { get; set; }
		public string TopicsCovered { get; set; }
		public string Teacher { get; set; }
		public short CourseSemesterID { get; set; }
		public string ClassName => AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift);
		public int? OfferedCourseAttendanceID { get; set; }
		public decimal? Hours { get; set; }
		public OfferedCourseAttendance.HoursValues? HoursValue => this.Hours?.ToHoursValue();
		public DateTime? ClassDate { get; set; }
		public byte? ClassType { get; set; }
		public OfferedCourseAttendance.ClassTypes? ClassTypeEnum => (OfferedCourseAttendance.ClassTypes?)this.ClassType;
		public int? RoomID { get; set; }
	}
}

