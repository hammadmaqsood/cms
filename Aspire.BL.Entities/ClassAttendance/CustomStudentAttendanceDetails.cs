﻿using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;

namespace Aspire.BL.Entities.ClassAttendance
{
	public class CustomStudentAttendanceDetails
	{
		public int RegisteredCourseID { get; set; }
		public int StudentID { get; set; }
		public string Enrollment { get; set; }
		public int? RegistrationNo { get; set; }
		public string Name { get; set; }
		public string StudentProgram { get; set; }
		public short StudentSemesterNo { get; set; }
		public int StudentSection { get; set; }
		public byte StudentShift { get; set; }
		public string StudentClassName => AspireFormats.GetClassName(this.StudentProgram, this.StudentSemesterNo, this.StudentSection, this.StudentShift);

		public int OfferedCourseID { get; set; }
		public short SemesterID { get; set; }
		public string CourseCode { get; set; }
		public string Title { get; set; }
		public decimal ContactHours { get; set; }
		public decimal CreditHours { get; set; }
		public string OfferedCourseProgram { get; set; }
		public short OfferedCourseSemesterNo { get; set; }
		public int OfferedCourseSection { get; set; }
		public byte OfferedCourseShift { get; set; }
		public string TeacherName { get; set; }
		public string OfferedCourseClassName => AspireFormats.GetClassName(this.OfferedCourseProgram, this.OfferedCourseSemesterNo, this.OfferedCourseSection, this.OfferedCourseShift);

		public class Detail
		{
			public int OfferedCourseAttendanceID { get; set; }
			public int OfferedCourseID { get; set; }
			public DateTime ClassDate { get; set; }
			public byte ClassType { get; set; }
			public string ClassTypeFullName => ((OfferedCourseAttendance.ClassTypes)this.ClassType).ToString();
			public decimal TotalHours { get; set; }
			public decimal PresentHours { get; set; }
			public decimal AbsentHours => this.TotalHours - this.PresentHours;
			public decimal Percentage => this.TotalHours == 0 ? 0 : this.PresentHours * 100 / this.TotalHours;
			public string RoomName { get; set; }
			public string Remarks { get; set; }
			public string TopicsCovered { get; set; }
		}

		public decimal ClassesTotalHours { get; set; }
		public List<Detail> Details { get; set; }
		public decimal PresentTotalHours { get; set; }
		public decimal PresentTotalPercentage { get; set; }
		public decimal AbsentTotalHours { get; set; }
	}
}
