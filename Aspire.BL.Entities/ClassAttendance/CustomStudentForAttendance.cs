﻿using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;

namespace Aspire.BL.Entities.ClassAttendance
{
	public class CustomOfferedCourseAttendanceDetails
	{
		public int? OfferedCourseAttendaceDetailID { get; set; }
		public int RegisteredCourseID { get; set; }
		public int StudentID { get; set; }
		public string Enrollment { get; set; }
		public string Name { get; set; }
		public string Program { get; set; }
		public decimal? Hours { get; set; }
		public OfferedCourseAttendance.HoursValues? HoursValue
		{
			get => this.Hours?.ToHoursValue();
			set => this.Hours = value?.ToDecimal();
		}
		public byte? FeeStatus { get; set; }
		public bool FeeDefaulter { get; set; }
		public bool FeePaid
		{
			get
			{
				switch ((StudentFee.Statuses?)this.FeeStatus)
				{
					case null:
					case StudentFee.Statuses.NotPaid:
						return false;
					case StudentFee.Statuses.Paid:
						return true;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
	}
}
