namespace Aspire.BL.Entities.FacultyManagement.Faculty
{
	public class CustomStudentExam
	{
		public int RegisteredCourseID { get; set; }
		public string Enrollment { get; set; }
		public string Name { get; set; }
		public string Program { get; set; }
		public decimal Min { get; set; }
		public decimal Max { get; set; }
		public decimal? MarksObtained { get; set; }
		public bool EditingAllowed { get; set; }
	}
}