﻿using Aspire.Model.Entities.Common;

namespace Aspire.BL.Entities.FacultyManagement.Admin
{
	public class CustomOfferedCourse
	{
		public int OfferedCourseID { get; set; }
		public short SemesterID { get; set; }
		public string Semester => this.SemesterID.ToSemesterString();
		public string CourseCode { get; set; }
		public string Title { get; set; }
		public decimal CreditHours { get; set; }
		public string CreditHoursFullName => this.CreditHours.ToCreditHoursFullName();
		public decimal ContactHours { get; set; }
		public string ContactHoursFullName => this.ContactHours.ToCreditHoursFullName();
		public string Program { get; set; }
		public short SemesterNo { get; set; }
		public int Section { get; set; }
		public byte Shift { get; set; }
		public string ClassName => AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift);
		public string Teacher { get; set; }
		public bool MarksEntryLocked { get; set; }
		public bool AssignmentsMarksSubmitted { get; set; }
		public bool QuizzesMarksSubmitted { get; set; }
		public bool MidMarksSubmitted { get; set; }
		public bool FinalMarksSubmitted { get; set; }
		public byte CourseCategory { get; set; }
		public CourseCategories CourseCategoryEnum => (CourseCategories)this.CourseCategory;
		public byte CourseType { get; set; }
		public CourseTypes CourseTypeEnum => (CourseTypes)this.CourseType;
	}
}
