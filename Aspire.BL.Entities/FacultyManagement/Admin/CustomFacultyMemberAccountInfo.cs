using Aspire.Model.Entities;

namespace Aspire.BL.Entities.FacultyManagement.Admin
{
	public class CustomFacultyMemberAccountInfo
	{
		public int FacultyMemberID { get; set; }
		public string Name { get; set; }
		public byte? Title { get; set; }
		public FacultyMember.Titles? TitleEnum => (FacultyMember.Titles?)this.Title;
		public string DisplayName { get; set; }
		public string DepartmentName { get; set; }
		public string Username { get; set; }
		public string Email { get; set; }
		public string FullName => this.TitleEnum == null ? this.Name : $"{this.TitleEnum.ToString().ToUpper()}. {this.Name}";
		public string FullDisplayName => string.IsNullOrWhiteSpace(this.DisplayName) ? this.FullName : this.DisplayName;
	}
}