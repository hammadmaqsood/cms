﻿namespace Aspire.BL.Entities.FacultyManagement.Admin
{
	public enum AddFacultyMemberStatus
	{
		UsernameExists,
		Success,
		CNICAlreadyExists,
		SpecifyDisplayName,
	}
}