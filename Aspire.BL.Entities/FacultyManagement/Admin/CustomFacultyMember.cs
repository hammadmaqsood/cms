﻿using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;

namespace Aspire.BL.Entities.FacultyManagement.Admin
{
	public class CustomFacultyMember
	{
		public int FacultyMemberID { get; set; }
		public string Name { get; set; }
		public byte? Title { get; set; }
		public FacultyMember.Titles? TitleEnum => (FacultyMember.Titles?)this.Title;
		public string DisplayName { get; set; }
		public string DepartmentName { get; set; }
		public string Username { get; set; }
		public string Email { get; set; }
		public byte Gender { get; set; }
		public byte Type { get; set; }
		public string FullName => this.TitleEnum == null ? this.Name : $"{this.TitleEnum.ToString().ToUpper()}. {this.Name}";
		public string FullDisplayName => string.IsNullOrWhiteSpace(this.DisplayName) ? this.FullName : this.DisplayName;
	}
}