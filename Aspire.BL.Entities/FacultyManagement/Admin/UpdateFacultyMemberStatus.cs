﻿namespace Aspire.BL.Entities.FacultyManagement.Admin
{
	public enum UpdateFacultyMemberStatus
	{
		UsernameExists,
		Success,
		CNICAlreadyExists,
		SpecifyDisplayName,
		NoRecordFound
	}
}
