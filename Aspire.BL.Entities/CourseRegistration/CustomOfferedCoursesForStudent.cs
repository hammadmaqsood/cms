using Aspire.Lib.Extensions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Entities.CourseRegistration
{
	public class CustomOfferedCoursesForStudent
	{
		private class Class
		{
			public short SemesterNo { get; set; }
			public int Section { get; set; }
			public byte Shift { get; set; }
		}

		public int OfferedCourseID { get; set; }
		public short SemesterID { get; set; }
		public int AdmissionOpenProgramID { get; set; }
		public int CourseID { get; set; }
		public string CourseCode { get; set; }
		public string Title { get; set; }
		public decimal CreditHours { get; set; }
		public string Majors { get; set; }

		public string Program { get; set; }
		public short SemesterNo { get; set; }
		public int Section { get; set; }
		public byte Shift { get; set; }
		public bool SpecialOffered { get; set; }
		private List<Class> Classes { get; set; }
		public string ClassNamesHtmlEncoded
		{
			get
			{
				var classes = new List<string> { AspireFormats.GetClassName(this.Program, this.SemesterNo, this.Section, this.Shift).HtmlEncode() };
				if (this.Classes != null)
					classes.AddRange(this.Classes.Select(c => $"<small><em>{AspireFormats.GetClassName(this.Program, c.SemesterNo, c.Section, c.Shift).HtmlEncode()}</em></small>"));
				return string.Join("<br />", classes);
			}
		}
		public short? EquivalentSemesterID { get; set; }
		public int? EquivalentCourseID { get; set; }
		public string EquivalentCourseCode { get; set; }
		public string EquivalentTitle { get; set; }
		public decimal? EquivalentCreditHours { get; set; }
		public string EquivalentMajors { get; set; }
		public int ProgramID { get; set; }
		public byte Status { get; set; }
		public OfferedCours.Statuses StatusEnum => (OfferedCours.Statuses)this.Status;
		public bool Registered { get; set; }
		public bool CanRegister { get; set; }
		public bool Requested { get; set; }
		public bool CanRequest { get; set; }

		public void AddClass(short semesterNo, int section, byte shift)
		{
			var @class = new Class { SemesterNo = semesterNo, Section = section, Shift = shift, };
			if (this.Classes == null)
				this.Classes = new List<Class> { @class };
			else
				this.Classes.Add(@class);
		}

		public bool ContainsClass(StudentClass studentClass)
		{
			if (this.ProgramID == studentClass.ProgramID && this.SemesterNo == studentClass.SemesterNo && this.Section == studentClass.Section && this.Shift == studentClass.Shift)
				return true;
			if (this.Classes != null && this.Classes.Any(c => this.ProgramID == studentClass.ProgramID && c.SemesterNo == studentClass.SemesterNo && c.Section == studentClass.Section && c.Shift == studentClass.Shift))
				return true;
			return false;
		}
	}
}