namespace Aspire.BL.Entities.CourseRegistration
{
	public enum CourseRegistrationStatus
	{
		AlreadyClearedAndCannotImproveFromGradeA,
		AlreadyClearedAndCannotImproveFromGradeB,
		AlreadyClearedAndCannotImproveFromGradeBPlus,
		AlreadyRegisteredCourseIDInCurrentSemester,
		AlreadyRegisteredInOfferedCourseID,
		OnlyImprovementsAllowed,
		PreRequisitesNotCleared,
		Success,
	}
}