﻿using System;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;

namespace Aspire.BL.Entities.CourseRegistration.Admin
{
	public class CustomCourseRegistrationSetting
	{
		public int CourseRegistrationSettingID { get; set; }
		public short SemesterID { get; set; }
		public string InstituteAlias { get; set; }
		public string ProgramAlias { get; set; }
		public byte Type { get; set; }
		public DateTime FromDate { get; set; }
		public DateTime ToDate { get; set; }
		public byte Status { get; set; }

		public string TypeFullName => ((CourseRegistrationSetting.Types)this.Type).ToString();
		public string StatusFullName => ((CourseRegistrationSetting.Statuses)this.Status).ToString();
		public string SemesterFullName => this.SemesterID.ToSemesterString();

		public string DepartmentAlias { get; set; }
	}
}
