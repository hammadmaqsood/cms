using System.Collections.Generic;

namespace Aspire.BL.Entities.CourseRegistration
{
	public class CoursesRegister
	{
		public class Course
		{
			public int OfferedCourseID { get; set; }
			public int CourseID { get; set; }
			public bool ByPassPreRequisites { get; set; }
			public string Title { get; set; }
			public CourseRegistrationStatus Status { get; set; }
		}
		public StudentRegistrationStatus Status { get; set; }
		public int StudentID { get; set; }
		public short OfferedSemesterID { get; set; }
		public short SemesterNo { get; set; }
		public int Section { get; set; }
		public byte Shift { get; set; }
		public bool CanChangeStudentSection { get; set; }
		public List<Course> Courses { get; set; }

		public void Validate()
		{
		}
	}
}