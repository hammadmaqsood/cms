using System;

namespace Aspire.BL.Entities.CourseRegistration
{
	public class CourseRegistrationException : Exception
	{
		public readonly CourseRegistrationStatus Status;
		public CourseRegistrationException(CourseRegistrationStatus status)
		{
			this.Status = status;
		}
	}
}