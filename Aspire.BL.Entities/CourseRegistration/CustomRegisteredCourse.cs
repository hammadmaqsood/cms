﻿using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.ComponentModel;

namespace Aspire.BL.Entities.CourseRegistration
{
	public class CustomRegisteredCourse
	{
		public int RegisteredCourseID { get; set; }
		public int OfferedCourseID { get; set; }

		public int RoadmapCourseID { get; set; }
		public string RoadmapCourseCode { get; set; }
		public string RoadmapTitle { get; set; }
		public string RoadmapMajors { get; set; }
		public decimal RoadmapCreditHours { get; set; }

		public short OfferedSemesterID { get; set; }
		public int Offered_CourseID { get; set; }
		public string OfferedCourseCode { get; set; }
		public string OfferedTitle { get; set; }
		public decimal OfferedCreditHours { get; set; }
		public string OfferedProgram { get; set; }
		public short OfferedSemesterNo { get; set; }
		public int OfferedSection { get; set; }
		public byte OfferedShift { get; set; }
		public string OfferedClass => AspireFormats.GetClassName(OfferedProgram, OfferedSemesterNo, this.OfferedSection, this.OfferedShift);

		public string FacultyMemberName { get; set; }
		public byte? Status { get; set; }

		public string StatusFullName
		{
			get
			{
				switch (this.StatusEnum)
				{
					case RegisteredCours.Statuses.Incomplete:
						return "Incomplete";
					case RegisteredCours.Statuses.WithdrawWithFee:
						return "Withdraw With Fee";
					case RegisteredCours.Statuses.WithdrawWithoutFee:
						return "Withdraw Without Fee";
					case RegisteredCours.Statuses.AttendanceDefaulter:
						return "Attendance Defaulter";
					case null:
						return null;
					default:
						throw new InvalidEnumArgumentException(nameof(this.StatusEnum));
				}
			}
		}

		public DateTime? StatusDate { get; set; }
		public RegisteredCours.Statuses? StatusEnum => (RegisteredCours.Statuses?)this.Status;
		public byte? FreezedStatus { get; set; }
		public FreezedStatuses? FreezedStatusEnum => (FreezedStatuses?)this.FreezedStatus;
		public DateTime? FreezedDate { get; set; }
		public bool SpecialOffered { get; set; }


	}
}
