﻿using Aspire.Model.Entities.Common;

namespace Aspire.BL.Entities.CourseRegistration.Staff
{
	public class CustomRegisteredCourses
	{
		public int RegisteredCourseID { get; set; }
		public short SemesterID { get; set; }
		public string CourseCode { get; set; }
		public string Title { get; set; }
		public decimal CreditHours { get; set; }
		public byte? Grade { get; set; }
		public byte? Status { get; set; }
		public byte? FreezedStatus { get; set; }
		public string Semester => this.SemesterID.ToSemesterString();
		public string GradeFullName => ((ExamGrades) (this.Grade)).ToFullName();
		public string CompleteStatus => AspireFormats.GetRegisteredCourseStatusString(this.Status, this.FreezedStatus);
	}
}
