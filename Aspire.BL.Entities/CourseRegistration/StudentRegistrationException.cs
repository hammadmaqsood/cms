using System;

namespace Aspire.BL.Entities.CourseRegistration
{
	public class StudentRegistrationException : Exception
	{
		public readonly StudentRegistrationStatus Status;
		public StudentRegistrationException(StudentRegistrationStatus status)
		{
			this.Status = status;
		}
	}
}