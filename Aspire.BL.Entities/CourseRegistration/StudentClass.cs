﻿using Aspire.Model.Entities.Common;

namespace Aspire.BL.Entities.CourseRegistration
{
	public class StudentClass
	{
		public int AdmissionOpenProgramID { get; set; }
		public int ProgramID { get; set; }
		public string ProgramAlias { get; set; }
		public short SemesterID { get; set; }
		public short SemesterNo { get; set; }
		public int Section { get; set; }
		public byte Shift { get; set; }
		public SemesterNos SemesterNoEnum
		{
			get { return (SemesterNos)this.SemesterNo; }
			set { this.SemesterNo = (short)value; }
		}
		public Sections SectionEnum
		{
			get { return (Sections)this.Section; }
			set { this.Section = (int)value; }
		}
		public Shifts ShiftEnum
		{
			get { return (Shifts)this.Shift; }
			set { this.Shift = (byte)value; }
		}

		public string ClassName => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
	}
}