﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;

namespace Aspire.BL.Entities.Common
{
	public class BasicStudentInformation
	{
		public string Enrollment { get; set; }
		public int StudentID { get; set; }
		public int? RegistrationNo { get; set; }
		public string Name { get; set; }
		public string FatherName { get; set; }
		public string Program { get; set; }
		public int ProgramID { get; set; }
		public int Duration { get; set; }
		public short MaxAllowedSemesterID { get; set; }
		public int AdmissionOpenProgramID { get; set; }
		public short SemesterID { get; set; }
		public string Nationality { get; set; }
		public Student.Statuses? StatusEnum { get; set; }
		public string BlockedReason { get; set; }
		public byte? StudentSemesterStatus { get; set; }
		public byte? FreezedStatus { get; set; }
		public DateTime? StatusDate { get; set; }
		public string Remarks { get; set; }
		public string ProgramAlias { get; set; }
		public short SemesterNo { get; set; }
		public int Section { get; set; }
		public byte Shift { get; set; }

		public string RemarksNA => this.Remarks.ToNAIfNullOrEmpty();
		public string StatusEnumFullName => (this.StatusEnum?.ToFullName()).ToNAIfNullOrEmpty();
		public string CompleteStatus => (AspireFormats.GetRegisteredCourseStatusString(this.StudentSemesterStatus, this.FreezedStatus)).ToNAIfNullOrEmpty();
		public string Class => AspireFormats.GetClassName(this.ProgramAlias, this.SemesterNo, this.Section, this.Shift);
		public string DurationEnumFullName => ((ProgramDurations)this.Duration).ToFullName();
		public string MaxAllowedSemester => this.MaxAllowedSemesterID.ToSemesterString();
		public int? CandidateAppliedProgramID { get; set; }
		public int? ApplicationNo { get; set; }
		public short? AppliedSemesterID { get; set; }
		public int? CandidateID { get; set; }
		public int? BURegistrationNo { get; set; }
	}
}
