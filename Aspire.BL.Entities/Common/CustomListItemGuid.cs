﻿using System;

namespace Aspire.BL.Entities.Common
{
	public class CustomListItemGuid
	{
		public Guid ID { get; set; }
		public string Text { get; set; }
	}
}
