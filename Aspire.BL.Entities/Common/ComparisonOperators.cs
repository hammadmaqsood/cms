namespace Aspire.BL.Entities.Common
{
	public enum ComparisonOperators
	{
		Equal,
		GreaterThan,
		GreaterThanEqual,
		LessThan,
		LessThanEqual,
		NotEqual,
	}
}