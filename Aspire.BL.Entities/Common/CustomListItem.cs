﻿namespace Aspire.BL.Entities.Common
{
	public class CustomListItem
	{
		public int ID { get; set; }
		public string Text { get; set; }
	}
}
