﻿namespace Aspire.BL.Entities.Common
{
	public enum ReportFormats
	{
		Excel, PDF, Word, Image
	}
}