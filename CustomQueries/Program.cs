﻿using Aspire.BL.Core.Exams.Common.Transcripts;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.WebSockets;
using Aspire.BL.Core.Admissions.Staff;
using Aspire.BL.Core.FeeManagement.Staff;
using Aspire.BL.QualityAssurance;
using File = System.IO.File;
using Student = Aspire.Model.Entities.Student;

namespace CustomQueries
{
	public static class Program
	{
		public static string ConnectionStringExcel = string.Empty;
		public static DateTime ExpiryDate = DateTime.Now.AddMonths(4);

		public enum ColumnIndexes : int
		{
			Institute = 0,
			IntakeSemester = 1,
			RegistrationNo = 2,
			Enrollment = 3,
			Name = 4,
			Department = 5,
			Code = 6,
			DegDuration = 7,
			PersonalEmail = 8,
			Gender = 9,
			CNIC = 10,
			DOB = 11,
			CurrentAddress = 12,
			PermanentAddress = 13,
			Nationality = 14,
			CreditTransfer = 15,
			FatherName = 16,
			SponsorBy = 17,
		}
		private static void Main()
		{
			#region PNSL Data Import Script
			//1. 
			//ImportAdmissionOpenProgram();
			//2.
			//ImportDataPNSLInCandidate();
			#endregion
			//ExportCdrNasirStudents();
			return;
			using (var aspireContext = new AspireContext())
			{
				var offeredSemesterID = 20201;
				var instituteID = 1;
				var data = aspireContext.OfferedCourses
					.Where(oc => oc.SemesterID == offeredSemesterID)
					.Where(oc => oc.Cours.AdmissionOpenProgram.Program.InstituteID == instituteID)
					.Select(oc => new
					{
						oc.OfferedCourseID,
						oc.FacultyMemberID,
						oc.Cours.Title,
						oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						oc.Cours.AdmissionOpenProgram.Program.Department.DepartmentAlias,
						oc.SemesterNo,
						oc.Section,
						oc.Shift,
						FacultyMemberName = oc.FacultyMember.Name,
						VisitingCourse = oc.Visiting,
						FacultyType = (FacultyMember.FacultyTypes?)oc.FacultyMember.FacultyType,
						FacultyMemberDepartment = oc.FacultyMember.Department.DepartmentAlias,
						Students = oc.RegisteredCourses.Count(rc => rc.DeletedDate == null)
					})
					.Where(oc => oc.DepartmentAlias == "COMPUTER ENGINEERING" || oc.FacultyMemberDepartment == "COMPUTER ENGINEERING")
					.ToList().Select(oc => new
					{
						oc.OfferedCourseID,
						oc.FacultyMemberID,
						oc.FacultyMemberName,
						oc.FacultyMemberDepartment,
						FacultyType = oc.FacultyType?.ToFullName(),
						oc.DepartmentAlias,
						oc.ProgramAlias,
						Class = AspireFormats.GetClassName(oc.ProgramAlias, oc.SemesterNo, oc.Section, oc.Shift),
						oc.Title,
						VisitingCourse = oc.VisitingCourse.ToYesNo(),
						oc.Students
					}).ToList();

				data.SaveFile("BCE");
			}
			Console.WriteLine("Press Enter to exit.");
			Console.ReadLine();
		}

		public static List<string> GetExcelFileSheets(string filePath, out string connectionStringExcel)
		{
			var connectionString = ExcelHelper.GetOledbConnectionString(filePath, ExcelHelper.ExcelFormat.Xlsx, false);
			connectionStringExcel = connectionString;
			string errorMessage;
			if (!ExcelHelper.TestConnection(connectionString, out errorMessage))
				return null;
			var sheets = ExcelHelper.GetAllSheetNames(connectionString);
			return sheets;
		}

		private static void ImportAdmissionOpenProgram()
		{
			string FilePath = @"E:\Documents Files\PNSL Data Transfer\AdmissionOpenPrograms.xlsx";
			int instituteID = 7;
			var loginInformation = Aspire.BL.Core.LoginManagement.Admin.Login("hammad@bahria.edu.pk", "asdf@123456", "Console", "39.50.104.134");
			var excelSheet = GetExcelFileSheets(FilePath, out ConnectionStringExcel);
			DataTable excelDataTable = ExcelHelper.GetData(ConnectionStringExcel, excelSheet[0].ToString());
			int i = 0;
			foreach (var row in excelDataTable.AsEnumerable())
			{
				i++;
				if (i != 1)
				{
					var dataRow = new[] { row[0], row[1] };
					string semesterString = dataRow[0].ToString();
					string programAlias = dataRow[1].ToString();
					var semesterID = GetSemesterIDFromSemesterString(semesterString);
					var (validationResults, admissionOpenProgram, admissionCriteriaID) = AddAdmissionOpenProgram(i, instituteID, semesterID.Value, programAlias, loginInformation.LoginHistory.LoginSessionGuid);
					switch (validationResults)
					{
						case AdmissionOpenProgram.ValidationResults.None:
							throw new InvalidOperationException();
						case AdmissionOpenProgram.ValidationResults.AlreadyExists:
							Console.WriteLine($"{i}. Admission open program is already exists.");
							break;
						case AdmissionOpenProgram.ValidationResults.Success:
							Console.WriteLine($"{i}. Admission open program is added successfully.");
							break;
						case AdmissionOpenProgram.ValidationResults.InvalidDateSequence:
							Console.WriteLine($"{i}. Invalid date sequence.");
							break;
						case AdmissionOpenProgram.ValidationResults.InvalidAdmissionOpenDates:
							Console.WriteLine($"{i}. Invalid admission open and close dates.");
							break;
						case AdmissionOpenProgram.ValidationResults.InvalidInterviewDates:
							Console.WriteLine($"{i}. Invalid interview start and end dates.");
							break;
						case AdmissionOpenProgram.ValidationResults.NoRecordFound:
							Console.WriteLine($"{i}. Not Record Found");
							break;
						case AdmissionOpenProgram.ValidationResults.InvalidAdmissionProcessingDueDate:
							Console.WriteLine($"{i}. Admission Processing Due Date cannot be less than Admission Closing date.");
							break;
						case AdmissionOpenProgram.ValidationResults.InvalidEntryTestDates:
							Console.WriteLine($"{i}. Invalid Entry Test start and end dates.");
							break;
						case AdmissionOpenProgram.ValidationResults.InvalidFinalSemester:
							Console.WriteLine($"{i}. Final Semester is not valid.");
							break;
						case AdmissionOpenProgram.ValidationResults.InvalidMaxSemester:
							Console.WriteLine($"{i}. Max Semester is not valid.");
							break;
						case AdmissionOpenProgram.ValidationResults.InvalidAdmissionOpenDatesForForeigners:
							Console.WriteLine($"{i}. Admission Open Dates for International Students are not valid.");
							break;
						default:
							throw new NotImplementedEnumException(validationResults);
					}
				}
			}

			Console.ReadKey();
		}

		private static void ImportDataPNSLInCandidate()
		{
			string FilePath = @"E:\Documents Files\PNSL Data Transfer\StudentInformationTest.xlsx";
			int instituteID = 7;
			var loginInformation = Aspire.BL.Core.LoginManagement.Staff.Login("hammad@bahria.edu.pk", "asdf@123456", "Console", "39.50.104.134");
			var excelSheet = GetExcelFileSheets(FilePath, out ConnectionStringExcel);
			DataTable excelDataTable = ExcelHelper.GetData(ConnectionStringExcel, excelSheet[0].ToString());
			int i = 0;
			foreach (var row in excelDataTable.AsEnumerable())
			{
				i++;
				if (i != 1)
				{
					var dataRow = new[] { row[ColumnIndexes.Institute.ToInt()], row[ColumnIndexes.IntakeSemester.ToInt()], row[ColumnIndexes.RegistrationNo.ToInt()], row[ColumnIndexes.Enrollment.ToInt()], row[ColumnIndexes.Name.ToInt()], row[ColumnIndexes.Department.ToInt()], row[ColumnIndexes.Code.ToInt()], row[ColumnIndexes.DegDuration.ToInt()], row[ColumnIndexes.PersonalEmail.ToInt()], row[ColumnIndexes.Gender.ToInt()], row[ColumnIndexes.CNIC.ToInt()], row[ColumnIndexes.DOB.ToInt()], row[ColumnIndexes.CurrentAddress.ToInt()], row[ColumnIndexes.PermanentAddress.ToInt()], row[ColumnIndexes.Nationality.ToInt()], row[ColumnIndexes.CreditTransfer.ToInt()], row[ColumnIndexes.FatherName.ToInt()], row[ColumnIndexes.SponsorBy.ToInt()] };
					string semesterString = dataRow[ColumnIndexes.IntakeSemester.ToInt()].ToString();
					string programAlias = dataRow[ColumnIndexes.Code.ToInt()].ToString();
					var name = dataRow[ColumnIndexes.Name.ToInt()].ToString();
					var email = string.IsNullOrEmpty(dataRow[ColumnIndexes.PersonalEmail.ToInt()].ToString()) ? $"dummy{i}@dummyemail.com" : dataRow[ColumnIndexes.PersonalEmail.ToInt()].ToString();
					var password = "asdf@123456".ValidatePassword().EncryptPassword();
					var cnic = dataRow[ColumnIndexes.CNIC.ToInt()].ToString();
					var gender = dataRow[ColumnIndexes.Gender.ToInt()].ToString().ToByte();
					var dob = dataRow[ColumnIndexes.DOB.ToInt()].ToString().ToDateTime();
					var nationality = dataRow[ColumnIndexes.Nationality.ToInt()].ToString();
					var country = !string.IsNullOrEmpty(nationality) && nationality == "PAKISTANI" ? "Pakistan" : null;
					var currentAddress = dataRow[ColumnIndexes.CurrentAddress.ToInt()].ToString();
					var permanentAddress = dataRow[ColumnIndexes.PermanentAddress.ToInt()].ToString();
					var fatherName = dataRow[ColumnIndexes.FatherName.ToInt()].ToString();
					var semesterID = GetSemesterIDFromSemesterString(semesterString);
					var candidates = new List<Aspire.Model.Entities.Candidate>();
					Aspire.Model.Entities.Candidate candidate;
					if (semesterID == null)
					{
						Console.WriteLine($"{i}. Invalid semester string.");
						continue;
					}
					var (admissionOpenProgramID, admissionCriteriaID) = GetAdmissionOpenProgram(i, instituteID, semesterID.Value, programAlias, loginInformation.LoginHistory.LoginSessionGuid);
					if (admissionOpenProgramID != null && admissionCriteriaID != null)
					{
						candidate = new Candidate
						{
							SemesterID = semesterID.Value,
							Name = name.TrimAndCannotEmptyAndMustBeUpperCase(),
							Email = email,
							Password = password,
							CNIC = cnic,
							Gender = gender,
							CategoryEnum = Categories.Others,
							DOB = dob,
							Nationality = nationality,
							Country = country,
							CurrentAddress = currentAddress,
							PermanentAddress = permanentAddress,
							IsFatherAlive = true,
							FatherName = fatherName,
							StatusEnum = Aspire.Model.Entities.Candidate.Statuses.Completed,
							EditStatus = (byte)Candidate.EditStatuses.Readonly,
							ForeignStudent = false,
						};
						var candidateStatusA = AddCandidateAndApplyForProgram(i, candidate, instituteID, admissionOpenProgramID.Value, admissionCriteriaID.Value, loginInformation.LoginHistory);
						switch (candidateStatusA)
						{
							case CandidateAddStatus.Success:
								Console.WriteLine($"{i}. Candidate has been applied successfully.");
								break;
							case CandidateAddStatus.Error:
								Console.WriteLine($"{i}. Candidate record has error.");
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}
					}
				}
			}
			Console.WriteLine("File records compeleted.");
			Console.ReadKey();
		}

		private static (int?, int?) GetAdmissionOpenProgram(int index, int instituteID, short semesterID, string programAlias, Guid loginSessionGuid)
		{
			Aspire.Model.Entities.Program program;
			int? admissionCriteriaID = null;
			using (var aspireContext = new AspireContext())
			{
				program = aspireContext.Programs.Single(p => p.ProgramAlias == programAlias && p.InstituteID == instituteID);
				var admissionOpenProgramID = aspireContext.AdmissionOpenPrograms.Where(aop => aop.ProgramID == program.ProgramID && aop.SemesterID == semesterID).Select(aop => (int?)aop.AdmissionOpenProgramID).SingleOrDefault();
				admissionCriteriaID = aspireContext.AdmissionCriterias.Where(ac => ac.InstituteID == instituteID && ac.CriteriaName == "BBA [4 Years] & BS (SCM)").Select(ac => (int?)ac.AdmissionCriteriaID).SingleOrDefault();
				if (admissionOpenProgramID == null)
				{
					Console.WriteLine($"{index}. Admission Open Program is not found.");
					return (null, null);
				}
				if (admissionCriteriaID == null)
				{
					Console.WriteLine($"{index}. Admission criteria is not found.");
					return (null, null);
				}
				return (admissionOpenProgramID.Value, admissionCriteriaID.Value);
			}
		}

		private static bool FeeRecordAlreadyExists(int index, int instituteID, short semesterID, int candidateAppliedProgramID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.StudentFees.Any(sf => sf.SemesterID == semesterID && sf.CandidateAppliedProgramID == candidateAppliedProgramID && sf.NewAdmission && sf.GrandTotalAmount == 0);
			}
		}

		private static short? GetSemesterIDFromSemesterString(string semesterString)
		{
			if (!semesterString.Contains("-"))
				return null;
			var semesterStringSplit = semesterString.Split('-');
			var semester = semesterStringSplit[0];
			var year = semesterStringSplit[1];
			SemesterTypes semesterType;
			switch (semester)
			{
				case nameof(SemesterTypes.Spring):
					semesterType = SemesterTypes.Spring;
					break;
				case nameof(SemesterTypes.Fall):
					semesterType = SemesterTypes.Fall;
					break;
				case nameof(SemesterTypes.Summer):
					semesterType = SemesterTypes.Summer;
					break;
				default:
					throw new ArgumentException();
			}
			return Semester.GetSemesterID(Int16.Parse(year), semesterType);
		}

		private static (AdmissionOpenProgram.ValidationResults, int?, int?) AddAdmissionOpenProgram(int index, int instituteID, short semesterID, string programAlias, Guid loginSessionGuid)
		{
			Aspire.Model.Entities.Program program;
			int? admissionCriteriaID = null;
			using (var aspireContext = new AspireContext())
			{
				program = aspireContext.Programs.Single(p => p.ProgramAlias == programAlias && p.InstituteID == instituteID);
				var admissionOpenProgramID = aspireContext.AdmissionOpenPrograms.Where(aop => aop.ProgramID == program.ProgramID && aop.SemesterID == semesterID).Select(aop => (int?)aop.AdmissionOpenProgramID).SingleOrDefault();
				admissionCriteriaID = aspireContext.AdmissionCriterias.Where(ac => ac.InstituteID == instituteID && ac.CriteriaName == "BBA [4 Years] & BS (SCM)").Select(ac => (int?)ac.AdmissionCriteriaID).SingleOrDefault();
				if (admissionCriteriaID == null)
				{
					Console.WriteLine($"{index}. Admission criteria is not found.");
					return (AdmissionOpenProgram.ValidationResults.NoRecordFound, null, null);
				}
				if (admissionCriteriaID != null && admissionOpenProgramID != null)
					return (AdmissionOpenProgram.ValidationResults.AlreadyExists, admissionOpenProgramID.Value, admissionCriteriaID.Value);
			}
			var now = DateTime.Now;
			var year = Semester.GetYear(semesterID);
			var semesterType = Semester.GetSemesterType(semesterID);
			short finalSemesterID = semesterID;
			short maxSemesterID = semesterID;
			switch (program.DurationEnum)
			{
				case ProgramDurations.ThreePointFiveYears:
					switch (semesterType)
					{
						case SemesterTypes.Spring:
							finalSemesterID = Semester.GetSemesterID((short)(year + 3), SemesterTypes.Spring);
							maxSemesterID = Semester.GetSemesterID((short)(year + 5), SemesterTypes.Summer);
							break;
						case SemesterTypes.Summer:
							finalSemesterID = Semester.GetSemesterID((short)(year + 2), SemesterTypes.Summer);
							maxSemesterID = Semester.GetSemesterID((short)(year + 5), SemesterTypes.Fall);
							break;
						case SemesterTypes.Fall:
							finalSemesterID = Semester.GetSemesterID((short)(year + 3), SemesterTypes.Fall);
							maxSemesterID = Semester.GetSemesterID((short)(year + 6), SemesterTypes.Spring);
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
				case ProgramDurations.FourYears:
					switch (semesterType)
					{
						case SemesterTypes.Spring:
							finalSemesterID = Semester.GetSemesterID((short)(year + 3), SemesterTypes.Fall);
							maxSemesterID = Semester.GetSemesterID((short)(year + 5), SemesterTypes.Fall);
							break;
						case SemesterTypes.Summer:
							finalSemesterID = Semester.GetSemesterID((short)(year + 2), SemesterTypes.Fall);
							maxSemesterID = Semester.GetSemesterID((short)(year + 6), SemesterTypes.Spring);
							break;
						case SemesterTypes.Fall:
							finalSemesterID = Semester.GetSemesterID((short)(year + 4), SemesterTypes.Spring);
							maxSemesterID = Semester.GetSemesterID((short)(year + 6), SemesterTypes.Summer);
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(program.DurationEnum), program.DurationEnum, null);
			}
			var admissionOpenProgram = new AdmissionOpenProgram
			{
				ProgramID = program.ProgramID,
				SemesterID = semesterID,
				IsSummerRegularSemester = semesterType == SemesterTypes.Summer,
				FinalSemesterID = finalSemesterID,
				MaxSemesterID = maxSemesterID,
				ShiftsEnum = Shifts.Morning,
				AdmissionCriteriaID = admissionCriteriaID.Value,
				EligibilityCriteriaStatement = "N/A",
				AdmissionOpenDate = now,
				AdmissionClosingDate = now,
				AdmissionProcessingFee = 2000,
				AdmissionProcessingFeeDueDate = now,
				EntryTestTypeEnum = AdmissionOpenProgram.EntryTestTypes.ComputerBasedTest,
				EntryTestDuration = 120,
				EntryTestStartDate = now,
				EntryTestEndDate = now,
				InterviewsStartDate = now,
				InterviewsEndDate = now,
				LoginAllowedEndDate = now,
				AdmissionOpenDateForForeigners = now,
				AdmissionClosingDateForForeigners = now,
				IntakeTargetStudentsCount = 0,
			};
			var validationResults = Aspire.BL.Core.Admissions.Admin.AdmissionOpenPrograms.AddAdmissionOpenProgram(admissionOpenProgram, loginSessionGuid);
			return (validationResults, admissionOpenProgram.AdmissionOpenProgramID, admissionCriteriaID);
		}

		public enum CandidateAddStatus
		{
			Success,
			Error,
		}

		private static CandidateAddStatus AddCandidateAndApplyForProgram(int index, Candidate candidate, int instituteID, int admissionOpenProgramID, int admissionCriteriaID, Aspire.BL.Core.LoginManagement.UserRole.LoginHistory loginHistory)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var now = DateTime.Now;
				var candidateDb = aspireContext.Candidates.SingleOrDefault(c => c.SemesterID == candidate.SemesterID && c.Name == candidate.Name && c.Email == candidate.Email);
				if (candidateDb == null)
				{
					aspireContext.Candidates.Add(candidate);
					aspireContext.SaveChanges(loginHistory.UserLoginHistoryID);
				}
				var admissionCriteriaPreReqs = aspireContext.AdmissionCriteriaPreReqs.SingleOrDefault(acpr => acpr.AdmissionCriteriaID == admissionCriteriaID);
				if (admissionCriteriaPreReqs == null)
				{
					aspireContext.RollbackTransaction();
					Console.WriteLine($"{index} Admission Criteria PreReqs is not found.");
					return CandidateAddStatus.Error;
				}
				var candidateID = candidateDb?.CandidateID ?? candidate.CandidateID;
				var candidateAppliedProgramFile = new CandidateAppliedProgram
				{
					ChallanNo = aspireContext.GetNextAdmissionProcessingFeeChallanNo().Single() ?? throw new InvalidOperationException("Null is returned for Next Challan No."),
					CandidateID = candidateID,
					AdmissionCriteriaPreReqID = admissionCriteriaPreReqs.AdmissionCriteriaPreReqID,
					ApplicationNo = null,
					Choice1AdmissionOpenProgramID = admissionOpenProgramID,
					ShiftEnum = Shifts.Morning,
					CreditTransfer = false,
					PreReqDegree = admissionCriteriaPreReqs.PreReqQualification,
					ApplyDate = now,
					EntryTestDate = now,
					TestDuration = 120,
					Amount = 2000,
					EntryTestMarksPercentage = 50,
					StatusEnum = CandidateAppliedProgram.Statuses.None,
					AccountTransactionID = null,
				};
				var candidateAppliedProgramDb = aspireContext.CandidateAppliedPrograms.SingleOrDefault(cap => cap.CandidateID == candidateID && cap.AdmissionOpenProgram1.AdmissionOpenProgramID == admissionOpenProgramID);
				if (candidateAppliedProgramDb == null)
				{
					aspireContext.CandidateAppliedPrograms.Add(candidateAppliedProgramFile);
					aspireContext.SaveChangesAndCommitTransaction(loginHistory.UserLoginHistoryID);
				}
				string fullChallanNo = string.Empty;
				Aspire.Model.Entities.CandidateAppliedProgram candidateAppliedProgram;
				if (candidateAppliedProgramDb == null)
					fullChallanNo = AspireFormats.ChallanNo.GetFullChallanNoString("07", InstituteBankAccount.FeeTypes.AdmissionProcessingFee, candidateAppliedProgramFile.ChallanNo);
				else
					fullChallanNo = candidateAppliedProgramDb.FullChallanNo;
				candidateAppliedProgram = candidateAppliedProgramDb ?? candidateAppliedProgramFile;
				var accountTransactionID = candidateAppliedProgram.AccountTransactionID;
				var instituteBankAccountID = aspireContext.InstituteBankAccounts.Where(iba => iba.InstituteID == instituteID && iba.Active && iba.AccountType == (byte)InstituteBankAccount.AccountTypes.ManualOnline && iba.FeeType == (byte)InstituteBankAccount.FeeTypes.AdmissionProcessingFee).Select(iba => iba.InstituteBankAccountID).Single();
				var paymentStatuses = Aspire.BL.Core.FeeManagement.Staff.FeePayment.PayFee(AspireFormats.ChallanNo.Parse(fullChallanNo), accountTransactionID, candidateAppliedProgramFile.Amount, AccountTransaction.Services.AlliedBank, AccountTransaction.TransactionChannels.Branch, AccountTransaction.PaymentTypes.Cash, instituteBankAccountID, string.Empty, now, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, loginHistory.LoginSessionGuid);
				switch (paymentStatuses)
				{
					case Aspire.BL.Core.FeeManagement.Staff.FeePayment.PayFeeStatuses.NoRecordFound:
						Console.WriteLine($"{index}. {AspireFormats.ChallanNo.Parse(candidateAppliedProgramFile.ChallanNo)} is not found.");
						return CandidateAddStatus.Error;
					case Aspire.BL.Core.FeeManagement.Staff.FeePayment.PayFeeStatuses.Added:
					case Aspire.BL.Core.FeeManagement.Staff.FeePayment.PayFeeStatuses.AlreadyPaid:
					case Aspire.BL.Core.FeeManagement.Staff.FeePayment.PayFeeStatuses.Updated:
						Console.WriteLine($"{index}. Payment Transaction has been updated/Added/Already paid.");
						var resultInterview = Aspire.BL.Core.Admissions.Staff.CandidateInterviews.UpdateCandidateInterview(candidateAppliedProgram.CandidateAppliedProgramID, Shifts.Morning, admissionOpenProgramID, now, now.AddDays(1), "Interview cleared by system", false, loginHistory.LoginSessionGuid);
						switch (resultInterview)
						{
							case CandidateInterviews.UpdateCandidateInterviewResults.Success:
								Console.WriteLine($"{index} Interview record has been updated.");
								var feeStructureResult = Aspire.BL.Core.FeeManagement.Staff.FeeStructures.AddFeeStructure(admissionOpenProgramID, 0, loginHistory.LoginSessionGuid);
								switch (feeStructureResult)
								{
									case Aspire.BL.Core.FeeManagement.Staff.FeeStructures.AddFeeStructureStatuses.AlreadyExists:
									case Aspire.BL.Core.FeeManagement.Staff.FeeStructures.AddFeeStructureStatuses.Success:
										Console.WriteLine($"{index}. Fee structure already exists / Added.");
										var studentFee = new StudentFee
										{
											StudentFeeID = 0,
											SemesterID = candidate.SemesterID,
											StudentID = null,
											CandidateAppliedProgramID = candidateAppliedProgram.CandidateAppliedProgramID,
											FeeTypeEnum = StudentFee.FeeTypes.Challan,
											CreatedDate = now,
											CreditHours = 0,
											NewAdmission = true,
											NoOfInstallmentsEnum = InstallmentNos.One,
											Remarks = "Generate by system",
											TotalAmount = 0,
											ConcessionAmount = 0,
											GrandTotalAmount = 0,
											StatusEnum = StudentFee.Statuses.Paid,
											ExemptWHT = false,
										};
										studentFee.StudentFeeChallans.Add(new StudentFeeChallan
										{
											Amount = 0,
											DueDate = now.AddDays(1),
											InstallmentNoEnum = InstallmentNos.One,
											StatusEnum = StudentFeeChallan.Statuses.Paid,
										});
										if (!FeeRecordAlreadyExists(index, instituteID, candidate.SemesterID, candidateAppliedProgram.CandidateAppliedProgramID))
										{
											var studentFeeResult = Aspire.BL.Core.FeeManagement.Staff.StudentFees.AddStudentFee(studentFee, new List<int>(), loginHistory.LoginSessionGuid);
											switch (studentFeeResult.Status)
											{
												case StudentFees.AddStudentFeeResult.Statuses.NoRecordFound:
													Console.WriteLine($"{index} No record found in student fee.");
													return CandidateAddStatus.Error;
												case StudentFees.AddStudentFeeResult.Statuses.Success:
													Console.WriteLine($"{index} Success upto student fee.");
													break;
												case StudentFees.AddStudentFeeResult.Statuses.EnrollmentGenerated:
													Console.WriteLine($"{index} Fee Challan can not be generated for Application No. whose enrollment have been generated.");
													return CandidateAddStatus.Error;
												default:
													throw new NotImplementedEnumException(studentFeeResult.Status);
											}
										}
										else
										{
											Console.WriteLine($"{index}. fee already exists.");
										}
										break;
									case Aspire.BL.Core.FeeManagement.Staff.FeeStructures.AddFeeStructureStatuses.NoRecordFound:
										Console.WriteLine($"{index} No record found in fee structure.");
										return CandidateAddStatus.Error;
									default:
										throw new ArgumentOutOfRangeException();
								}
								break;
							case CandidateInterviews.UpdateCandidateInterviewResults.NoRecordFound:
								Console.WriteLine($"{index} No record found in interview.");
								return CandidateAddStatus.Error;
							case CandidateInterviews.UpdateCandidateInterviewResults.FeeStructureNotfound:
								Console.WriteLine($"{index} Fee Structure not found.");
								return CandidateAddStatus.Error;
							case CandidateInterviews.UpdateCandidateInterviewResults.AdmissionProcessingFeeNotPaid:
								Console.WriteLine($"{index} Admission processing fee is not paid.");
								return CandidateAddStatus.Error;
							case CandidateInterviews.UpdateCandidateInterviewResults.EnrollmentHasBeenGenerated:
								Console.WriteLine($"{index} Cannot update because Enrollment has been generated.");
								return CandidateAddStatus.Error;
							case CandidateInterviews.UpdateCandidateInterviewResults.EntryTestRecordNotFound:
								Console.WriteLine($"{index}  Entry Test record not found.");
								return CandidateAddStatus.Error;
							case CandidateInterviews.UpdateCandidateInterviewResults.AdmittedProgramMustBeDefinedBecauseFeeChallanHasBeenGenerated:
								Console.WriteLine($"{index} Cannot update because Fee Challan has been generated. If you want to reject admission then fee challan must be deleted.");
								return CandidateAddStatus.Error;
							default:
								throw new ArgumentOutOfRangeException();
						}
						break;
					case Aspire.BL.Core.FeeManagement.Staff.FeePayment.PayFeeStatuses.CannotUpdateVerifiedAccountTransaction:
						Console.WriteLine($"{index}. Verified Payment Transaction cannot be updated.");
						return CandidateAddStatus.Error;
					default:
						throw new NotImplementedEnumException(paymentStatuses);
				}
				return CandidateAddStatus.Success;
			}
		}

		private static void RevertMarksSubmission()
		{
			List<int> offeredCourseIDs;
			var loginSessionGuid = new Guid("9B940C09-EB09-430B-A7A7-7A04EA66D28E");
			using (var aspireContext = new AspireContext(false))
			{
				var offeredSemesterID = 20193;
				offeredCourseIDs = aspireContext.OfferedCourses.Where(oc => oc.SemesterID == offeredSemesterID && oc.Cours.AdmissionOpenProgram.Program.InstituteID == 2)
					.Where(oc => oc.SubmittedToCampusDate != null)
					.Select(oc => oc.OfferedCourseID)
					.ToList();
			}
			var index = 0;
			offeredCourseIDs.ForEach(oc =>
			{
				var record = Aspire.BL.Core.Exams.Common.New.MarksHelper.GetMarksSheet(oc, loginSessionGuid);
				var marksSheet = record.Value.marksSheet;
				Console.WriteLine($"{++index:##}{marksSheet.OfferedCourse.Title}");
				Aspire.BL.Core.Exams.Common.New.MarksHelper.RevertSubmitToCampus(marksSheet.OfferedCourse.OfferedCourseID, loginSessionGuid);
			});
		}

		public static void ImportOldFeeChallanDetails()
		{
			var now = DateTime.Now;
			List<int> records;
			using (var aspireContext = new AspireContext())
			{
				records = aspireContext.StudentFees
								  .Where(sf => sf.MigrationID != null)
								  .Where(sf => !sf.StudentFeeDetails.Any())
								  .Where(sf => !sf.StudentFeeChallans.Any())
								  //.Where(sf => sf.Status == (byte)StudentFee.Statuses.Paid)
								  .Select(sf => sf.StudentFeeID).ToList();
			}
			records.ForEach(r =>
			{
				ImportOldStudentFeeDetails(now, r);
			});
		}

		private static void ImportOldStudentFeeDetails(DateTime now, int studentFeeID)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var record = aspireContext.StudentFees
									.Where(sf => sf.MigrationID != null)
									.Where(sf => !sf.StudentFeeDetails.Any())
									.Where(sf => !sf.StudentFeeChallans.Any())
									.Where(sf => sf.StudentFeeID == studentFeeID)
									.Select(sf => new
									{
										StudentFee = sf,
										sf.StudentFeeChallans,
										StudentInstituteID = (int?)sf.Student.AdmissionOpenProgram.Program.InstituteID,
										CandidateInstituteID = (int?)sf.CandidateAppliedProgram.AdmissionOpenProgram1.Program.InstituteID
									})
									.Single();
				if (record.StudentFee.GrandTotalAmount == 0)
					return;

				var instituteID = record.StudentInstituteID ?? record.CandidateInstituteID ?? throw new InvalidOperationException();
				var feeHeads = aspireContext.FeeHeads.Where(i => i.InstituteID == instituteID).ToList();
				string databaseName;
				string schemaName;
				switch (instituteID)
				{
					case 1:
						databaseName = "OldCMSData";
						schemaName = "ISB.CAS";
						break;
					case 2:
						databaseName = "KHI_LHR_NCMPR_IPP_OldCMSData";
						schemaName = "KHI.CAS";
						break;
					case 3:
						databaseName = "KHI_LHR_NCMPR_IPP_OldCMSData";
						schemaName = "LHR.CAS";
						break;
					case 4:
						databaseName = "KHI_LHR_NCMPR_IPP_OldCMSData";
						schemaName = "NCMPR.CAS";
						break;
					case 5:
						databaseName = "KHI_LHR_NCMPR_IPP_OldCMSData";
						schemaName = "IPP.CAS";
						break;
					default:
						throw new InvalidOperationException();
				}
				int? tutionFee;
				int? admissionFee;
				int? cautionMoney;
				int? degreeFee;
				DateTime issueDate;
				DateTime dueDate;
				DateTime? recDate;
				bool install;
				bool paid;
				int? lessAdjAmount;
				int? arrears;
				bool newAdm;
				int? fine;
				int? laptopFee;
				var remarks = $"Old record left in migration. Migrated on {now:F}";
				int? misc;
				int? scholarshipAmount;
				var connBuilder = new SqlConnectionStringBuilder(aspireContext.Database.Connection.ConnectionString)
				{
				};
				using (var conn = new SqlConnection(connBuilder.ConnectionString))
				{
					conn.Open();
					var sqlCommand = conn.CreateCommand();
					sqlCommand.CommandText = $"select * from [{schemaName}].FEE_CHALAN WHERE TOTAL_FEE = {record.StudentFee.GrandTotalAmount} AND CHALAN_NO={record.StudentFee.MigrationID.Value}";
					sqlCommand.Connection.ChangeDatabase(databaseName);
					using (var reader = sqlCommand.ExecuteReader())
					{
						if (reader.Read())
						{
							var ordinal = reader.GetOrdinal("TUTION_FEE");
							tutionFee = !reader.IsDBNull(ordinal) ? (int)reader.GetDecimal(ordinal) : (int?)null;
							ordinal = reader.GetOrdinal("ADMISSION_FEE");
							admissionFee = !reader.IsDBNull(ordinal) ? (int)reader.GetDecimal(ordinal) : (int?)null;
							ordinal = reader.GetOrdinal("CAUTION_MONEY");
							cautionMoney = !reader.IsDBNull(ordinal) ? (int)reader.GetDecimal(ordinal) : (int?)null;
							ordinal = reader.GetOrdinal("DEGREE_FEE");
							degreeFee = !reader.IsDBNull(ordinal) ? (int)reader.GetDecimal(ordinal) : (int?)null;
							ordinal = reader.GetOrdinal("ISSUE_DATE");
							issueDate = reader.GetDateTime(ordinal);
							ordinal = reader.GetOrdinal("DUE_DATE");
							dueDate = reader.GetDateTime(ordinal);
							ordinal = reader.GetOrdinal("REC_DATE");
							recDate = !reader.IsDBNull(ordinal) ? reader.GetDateTime(ordinal) : (DateTime?)null;
							ordinal = reader.GetOrdinal("INSTALL");
							install = reader.GetDecimal(ordinal) == 1;
							ordinal = reader.GetOrdinal("PAID");
							paid = reader.GetDecimal(ordinal) == 1;
							ordinal = reader.GetOrdinal("LESS_ADJ_AMOUNT");
							lessAdjAmount = !reader.IsDBNull(ordinal) ? (int)reader.GetDecimal(ordinal) : (int?)null;
							ordinal = reader.GetOrdinal("NEW_ADM");
							newAdm = reader.GetDecimal(ordinal) == 1;
							ordinal = reader.GetOrdinal("FINE");
							fine = !reader.IsDBNull(ordinal) ? (int)reader.GetDecimal(ordinal) : (int?)null;
							ordinal = reader.GetOrdinal("ARREARS");
							arrears = !reader.IsDBNull(ordinal) ? (int)reader.GetDecimal(ordinal) : (int?)null;
							ordinal = reader.GetOrdinal("LAPTOP_FEE");
							laptopFee = !reader.IsDBNull(ordinal) ? (int)reader.GetDecimal(ordinal) : (int?)null;
							ordinal = reader.GetOrdinal("MISC");
							misc = !reader.IsDBNull(ordinal) ? (int)reader.GetDecimal(ordinal) : (int?)null;
							ordinal = reader.GetOrdinal("SCHOLERSHIP_AMOUNT");
							scholarshipAmount = !reader.IsDBNull(ordinal) ? (int)reader.GetDecimal(ordinal) : (int?)null;
							if (reader.Read())
								throw new InvalidOperationException("Duplication found.");
						}
						else
							throw new InvalidOperationException("No record found.");
					}

				}

				AddStudentFeeDetails(record.StudentFee, feeHeads.Find(h => h.HeadName == "Tution Fee").FeeHeadID, tutionFee);
				AddStudentFeeDetails(record.StudentFee, feeHeads.Find(h => h.HeadName == "Admission Fee").FeeHeadID, admissionFee);
				AddStudentFeeDetails(record.StudentFee, feeHeads.Find(h => h.HeadName == "Caution Money").FeeHeadID, cautionMoney);
				AddStudentFeeDetails(record.StudentFee, feeHeads.Find(h => h.HeadName == "Degree Fee").FeeHeadID, degreeFee);
				AddStudentFeeDetails(record.StudentFee, feeHeads.Find(h => h.HeadName == "Less Adjusted Amount").FeeHeadID, lessAdjAmount);
				AddStudentFeeDetails(record.StudentFee, feeHeads.Find(h => h.HeadName == "Arrears").FeeHeadID, arrears);
				AddStudentFeeDetails(record.StudentFee, feeHeads.Find(h => h.HeadName == "Fine").FeeHeadID, fine);
				AddStudentFeeDetails(record.StudentFee, feeHeads.Find(h => h.HeadName == "Laptop Fee").FeeHeadID, laptopFee);
				AddStudentFeeDetails(record.StudentFee, feeHeads.Find(h => h.HeadName == "Miscellaneous").FeeHeadID, misc);
				AddStudentFeeDetails(record.StudentFee, feeHeads.Find(h => h.HeadName == "Scholarship Amount").FeeHeadID, scholarshipAmount);
				var studentFeeChallan = new StudentFeeChallan
				{
					DueDate = dueDate,
					Amount = record.StudentFee.GrandTotalAmount,
					InstallmentNoEnum = InstallmentNos.One,
					StatusEnum = StudentFeeChallan.Statuses.NotPaid,
					AccountTransactionID = null
				};
				record.StudentFee.StudentFeeChallans.Add(studentFeeChallan);
				record.StudentFee.Validate(feeHeads);
				aspireContext.SaveChangesAsSystemUser();

				if (record.StudentFee.CandidateAppliedProgramID != null && record.StudentFee.StudentID == null)
				{
					var candidateAppliedProgram = aspireContext.CandidateAppliedPrograms.Single(cap => cap.CandidateAppliedProgramID == record.StudentFee.CandidateAppliedProgramID.Value);
					if (candidateAppliedProgram.AdmittedAdmissionOpenProgramID == null)
					{
						candidateAppliedProgram.AdmittedAdmissionOpenProgramID = candidateAppliedProgram.Choice1AdmissionOpenProgramID;
						candidateAppliedProgram.InterviewDate = now;
						candidateAppliedProgram.InterviewRemarks = remarks;
						candidateAppliedProgram.AdmittedShiftEnum = candidateAppliedProgram.ShiftEnum;
					}
				}

				if (paid)
				{
					string accountNo;
					string accountTitle;
					string branchCode;
					string branchName;
					int instituteBankAccountID;
					switch (instituteID)
					{
						case 1:
							accountNo = newAdm ? "0010005881820080" : "0010005881820080";
							accountTitle = newAdm ? "BAHRIA UNIVERSITY" : "BAHRIA UNIVERSITY";
							branchCode = newAdm ? "680" : "680";
							branchName = newAdm ? "HALL NO. 5, SHOP NO. 125-126" : "HALL NO. 5, SHOP NO. 125-126";
							instituteBankAccountID = newAdm ? 2 : 3;
							break;
						case 2:
							accountNo = newAdm ? "MISSING" : "MISSING";
							accountTitle = newAdm ? "BAHRIA UNIVERSITY" : "BAHRIA UNIVERSITY";
							branchCode = newAdm ? "MISSING" : "MISSING";
							branchName = newAdm ? "MISSING" : "MISSING";
							instituteBankAccountID = newAdm ? 8 : 9;
							break;
						case 3:
							accountNo = newAdm ? "MISSING" : "MISSING";
							accountTitle = newAdm ? "BAHRIA UNIVERSITY" : "BAHRIA UNIVERSITY";
							branchCode = newAdm ? "MISSING" : "MISSING";
							branchName = newAdm ? "MISSING" : "MISSING";
							instituteBankAccountID = newAdm ? 14 : 15;
							break;
						case 4:
							accountNo = newAdm ? "MISSING" : "MISSING";
							accountTitle = newAdm ? "BAHRIA UNIVERSITY" : "BAHRIA UNIVERSITY";
							branchCode = newAdm ? "MISSING" : "MISSING";
							branchName = newAdm ? "MISSING" : "MISSING";
							instituteBankAccountID = newAdm ? 53 : 21;
							break;
						case 5:
							accountNo = newAdm ? "MISSING" : "MISSING";
							accountTitle = newAdm ? "BAHRIA UNIVERSITY" : "BAHRIA UNIVERSITY";
							branchCode = newAdm ? "MISSING" : "MISSING";
							branchName = newAdm ? "MISSING" : "MISSING";
							instituteBankAccountID = newAdm ? 35 : 36;
							break;
						default:
							throw new InvalidOperationException();
					}

					var fullChallanNo = studentFeeChallan.GetFullChallanNo("0" + instituteID);

					studentFeeChallan.AccountTransaction = new AccountTransaction
					{
						AccountNo = accountNo,
						AccountTitle = accountTitle,
						BranchCode = branchCode,
						BranchName = branchName,
						InstituteBankAccountID = instituteBankAccountID,
						Amount = record.StudentFee.GrandTotalAmount,
						FullChallanNo = fullChallanNo.ToDecimal(),
						TransactionReferenceID = fullChallanNo,
						PaymentTypeEnum = AccountTransaction.PaymentTypes.Cash,
						ServiceEnum = AccountTransaction.Services.Manual,
						SystemDate = now,
						TransactionChannelEnum = AccountTransaction.TransactionChannels.Branch,
						TransactionDate = recDate ?? throw new InvalidOperationException(),
						Verified = false,
						Remarks = remarks,
						UserLoginHistoryID = UserLoginHistory.SystemUserLoginHistoryID,
					}.ValidateServiceChannelPaymentType();

				}
				aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
				Console.WriteLine("Full Challan: " + record.StudentFee.StudentFeeChallans.Single().GetFullChallanNo("0" + instituteID));
			}
		}

		private static void AddStudentFeeDetails(StudentFee studentFee, int feeHeadID, int? amount)
		{
			if (amount != null && amount != 0)
				studentFee.StudentFeeDetails.Add(new StudentFeeDetail
				{
					FeeHeadID = feeHeadID,
					ConcessionAmount = null,
					TotalAmount = amount.Value,
					GrandTotalAmount = amount.Value
				});
		}

		public static void ExportInstitutes()
		{
			using (var aspireContext = new AspireContext(true))
			{
				var instituteID = 1;
				var query = aspireContext.Institutes.Where(i => i.InstituteID == instituteID).Select(i => new
				{
					i.InstituteID,
					i.InstituteName,
					IsDeleted = 0,
				}).ToList();
				//var feeStructures = query.ToList();
				//feeStructures.ForEach(fsd => fsd.Amount = (int)Math.Round(fsd.Amount + (fsd.Amount * 5d / 100d), MidpointRounding.AwayFromZero));
				//aspireContext.SaveChangesAsSystemUserAndCommitTransaction(true);
				query.SaveFile("Institutes");
			}
		}

		public static void ExportDepartments()
		{
			using (var aspireContext = new AspireContext(true))
			{
				var instituteID = 1;
				var query = aspireContext.Departments.Where(d => d.InstituteID == instituteID).Select(d => new
				{
					d.DepartmentID,
					d.InstituteID,
					d.DepartmentName,
					IsDeleted = 0,
				}).ToList();
				//var feeStructures = query.ToList();
				//feeStructures.ForEach(fsd => fsd.Amount = (int)Math.Round(fsd.Amount + (fsd.Amount * 5d / 100d), MidpointRounding.AwayFromZero));
				//aspireContext.SaveChangesAsSystemUserAndCommitTransaction(true);
				query.SaveFile("Departments");
			}
		}

		public static void ExportPrograms()
		{
			using (var aspireContext = new AspireContext(true))
			{
				var instituteID = 1;
				var query = aspireContext.Programs.Where(p => p.InstituteID == instituteID).Select(p => new
				{
					p.ProgramID,
					p.DepartmentID,
					p.InstituteID,
					p.ProgramName,
					IsDeleted = 0,
				}).ToList();
				//var feeStructures = query.ToList();
				//feeStructures.ForEach(fsd => fsd.Amount = (int)Math.Round(fsd.Amount + (fsd.Amount * 5d / 100d), MidpointRounding.AwayFromZero));
				//aspireContext.SaveChangesAsSystemUserAndCommitTransaction(true);
				query.SaveFile("Programs");
			}
		}

		public static void ExportStudents()
		{
			using (var aspireContext = new AspireContext(true))
			{
				var instituteID = 1;
				var semesterID = 20191;
				var query = aspireContext.RegisteredCourses.Where(s => s.Student.AdmissionOpenProgram.Program.InstituteID == instituteID && s.DeletedDate == null && s.OfferedCours.SemesterID == semesterID).Select(s => new
				{
					s.StudentID,
					s.Student.Enrollment,
					s.Student.PersonalEmail,
					s.Student.UniversityEmail,
					s.Student.Status,
					s.Student.Name,
					s.Student.AdmissionOpenProgram.ProgramID,
					s.Student.AdmissionOpenProgram.SemesterID,
					IsDeleted = 0,
				}).Distinct().ToList();
				//var feeStructures = query.ToList();
				//feeStructures.ForEach(fsd => fsd.Amount = (int)Math.Round(fsd.Amount + (fsd.Amount * 5d / 100d), MidpointRounding.AwayFromZero));
				//aspireContext.SaveChangesAsSystemUserAndCommitTransaction(true);
				query.SaveFile("Students");
			}
		}

		public static void ExportRegisteredCourses()
		{
			using (var aspireContext = new AspireContext(true))
			{
				var instituteID = 1;
				var semesterID = 20191;
				var query = aspireContext.RegisteredCourses.Where(r => r.Student.AdmissionOpenProgram.Program.InstituteID == instituteID && r.DeletedDate == null && r.OfferedCours.SemesterID == semesterID).Select(r => new
				{
					r.RegisteredCourseID,
					r.StudentID,
					r.OfferedCourseID,
					r.Status,
					r.FreezedStatus,
					IsDeleted = 0,
				}).ToList();
				//var feeStructures = query.ToList();
				//feeStructures.ForEach(fsd => fsd.Amount = (int)Math.Round(fsd.Amount + (fsd.Amount * 5d / 100d), MidpointRounding.AwayFromZero));
				//aspireContext.SaveChangesAsSystemUserAndCommitTransaction(true);
				query.SaveFile("RegisteredCourses");
			}
		}

		public static void ExportFacultyMembers()
		{
			using (var aspireContext = new AspireContext(true))
			{
				var instituteID = 1;
				var semesterID = 20191;
				var query = aspireContext.OfferedCourses.Where(f => f.FacultyMember.InstituteID == instituteID && f.SemesterID == semesterID).Select(f => new
				{
					f.FacultyMemberID,
					FacultyMemberName = f.FacultyMember.Name,
					f.FacultyMember.Email,
					IsDeleted = 0,
				}).Distinct().ToList();
				//var feeStructures = query.ToList();
				//feeStructures.ForEach(fsd => fsd.Amount = (int)Math.Round(fsd.Amount + (fsd.Amount * 5d / 100d), MidpointRounding.AwayFromZero));
				//aspireContext.SaveChangesAsSystemUserAndCommitTransaction(true);
				query.SaveFile("FacultyMembers");
			}
		}

		public static void ExportOfferedCourses()
		{
			using (var aspireContext = new AspireContext(true))
			{
				var instituteID = 1;
				var semesterID = 20191;
				var query = aspireContext.OfferedCourses.Where(f => f.Cours.AdmissionOpenProgram.Program.InstituteID == instituteID && f.SemesterID == semesterID).Select(f => new
				{
					f.OfferedCourseID,
					f.SemesterID,
					f.Cours.AdmissionOpenProgram.ProgramID,
					f.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					f.Cours.AdmissionOpenProgram.Program.ProgramShortName,
					f.SemesterNo,
					f.Section,
					f.Shift,
					f.Cours.Title,
					f.Cours.CreditHours,
					f.FacultyMemberID,

				}).ToList().Select(f => new
				{
					f.OfferedCourseID,
					f.SemesterID,
					Class = $"{f.ProgramAlias}-{((SemesterNos)f.SemesterNo).ToFullName()}-{((Shifts)f.Shift).ToFullName()}",
					f.ProgramID,
					f.ProgramShortName,
					f.SemesterNo,
					f.Section,
					f.Shift,
					f.Title,
					f.CreditHours,
					f.FacultyMemberID,
					IsDeleted = 0,
					CourseOutlineFile = "N/A",
				}).ToList();
				//var feeStructures = query.ToList();
				//feeStructures.ForEach(fsd => fsd.Amount = (int)Math.Round(fsd.Amount + (fsd.Amount * 5d / 100d), MidpointRounding.AwayFromZero));
				//aspireContext.SaveChangesAsSystemUserAndCommitTransaction(true);
				query.SaveFile("OfferedCourses");
			}
		}

		private static void ThesisMigration()
		{
			List<ThesisInternship> thesisInternships;
			using (var aspireContext = new AspireContext(true))
			{
				thesisInternships = aspireContext.ThesisInternships.Include(ti => ti.ThesisInternshipMarks).ToList();
			}

			var loginSessionGuid = new Guid("55C8A814-94E2-4075-9E13-6BAC57E144CF");
			//var userLoginHistoryID = 7809471;
			var count = 0;
			thesisInternships.ForEach(ti =>
			{
				count++;
				string title = null;
				string organization = null;
				DateTime? fromDate = null;
				DateTime? toDate = null;
				DateTime? reportSubmissionDate = null;
				DateTime? defenceVivaDate = null;
				DateTime? completionDate = null;
				var remarks = (ti.Remarks ?? string.Empty).Trim();

				void AppendRemarks(string name, string value)
				{
					if (!string.IsNullOrWhiteSpace(value))
						remarks = $"{remarks}\n{name}:{value}".Trim();
				}

				AppendRemarks("Title", ti.Title);
				AppendRemarks("Organization", ti.Organization);
				AppendRemarks("From Date", ti.FromDate?.ToShortDateString());
				AppendRemarks("To Date", ti.ToDate?.ToShortDateString());
				AppendRemarks("Defence Date", ti.DefenceDate?.ToShortDateString());
				AppendRemarks("Submission Date", ti.SubmissionDate?.ToShortDateString());
				AppendRemarks("Total", ti.Total.ToString(CultureInfo.CurrentCulture));
				AppendRemarks("Grade", ti.GradeFullName);

				var defaultDate = new DateTime(1990, 1, 1);
				ProjectThesisInternship.RecordTypes recordTypeEnum;
				switch (ti.RecordTypeEnum)
				{
					case ThesisInternship.RecordTypes.Internship:
						recordTypeEnum = ProjectThesisInternship.RecordTypes.Internship;
						organization = ti.Organization.ToNAIfNullOrEmpty();
						fromDate = ti.FromDate ?? defaultDate;
						toDate = ti.ToDate ?? defaultDate;
						reportSubmissionDate = ti.DefenceDate ?? defaultDate;
						defenceVivaDate = ti.DefenceDate ?? defaultDate;
						completionDate = ti.DefenceDate ?? defaultDate;
						break;
					case ThesisInternship.RecordTypes.Thesis:
						recordTypeEnum = ProjectThesisInternship.RecordTypes.Thesis;
						title = ti.Title.ToNAIfNullOrEmpty();
						organization = ti.Organization.ToNAIfNullOrEmpty();
						reportSubmissionDate = ti.DefenceDate ?? defaultDate;
						defenceVivaDate = ti.DefenceDate ?? defaultDate;
						completionDate = ti.SubmissionDate ?? defaultDate;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				using (var aspireContext = new AspireContext(true))
				{
					var projectThesisInternship = aspireContext.ProjectThesisInternships.Add(new ProjectThesisInternship
					{
						ProjectThesisInternshipID = Guid.NewGuid(),
						StudentID = ti.StudentID,
						RegisteredCourseID = null,
						SemesterID = ti.SemesterID,
						RecordTypeEnum = recordTypeEnum,
						CreditHours = ti.CreditHours,
						Title = title,
						Organization = organization,
						FromDate = fromDate,
						ToDate = toDate,
						ReportSubmissionDate = reportSubmissionDate,
						DefenceVivaDate = defenceVivaDate,
						CompletionDate = completionDate,
						Remarks = remarks,
						Total = ti.Total,
						GradeEnum = ti.GradeEnum,
						GradePoints = ti.CreditHours > 0 ? ti.GradePoint / ti.CreditHours : ti.GradePoint,
						Product = ti.GradePoint,
						MarksEntryLocked = ti.MarksEntryLocked,
						ProjectThesisInternshipMarks = ti.ThesisInternshipMarks.Select(tim => new ProjectThesisInternshipMark
						{
							ProjectThesisInternshipMarkID = Guid.NewGuid(),
							DisplayIndex = tim.DisplayIndex,
							MarksTitle = tim.MarksTitle,
							ObtainedMarks = tim.ObtainedMarks,
							TotalMarks = tim.TotalMarks
						}).ToList()
					});
					aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
					Console.WriteLine(count);
				}
			});

			Console.WriteLine("Press Enter to exit.");
			Console.ReadLine();
		}

		private static void TestingCSVParsing()
		{
			var input = @"Col1,Col 2,""Col""""3"",""Col,4"",""""""Col5"""""",""""""Col"""",
End""""""
1,2,3,,,
1,2,3,4,5,
";
			IEnumerable<IEnumerable<object>> result = input.ParseCSV();
			var csv = result.ConvertToCSVFormat();
			if (csv == ((IEnumerable<IEnumerable<object>>)csv.ParseCSV()).ConvertToCSVFormat())
			{

			}
		}

		public static void ExportFeeStructure()
		{
			using (var aspireContext = new AspireContext(true))
			{
				var instituteID = 1;
				var semesterID = 20191;
				var query = aspireContext.FeeStructureDetails
					.Where(fsd => fsd.FeeHead.InstituteID == instituteID)
					.Where(fsd => fsd.FeeStructure.AdmissionOpenProgram.SemesterID <= semesterID)
					.Where(fsd => fsd.FeeHead.FeeHeadType == (byte)FeeHead.FeeHeadTypes.TutionFee);
				var data = query
					.Select(fsd => new
					{
						fsd.FeeStructureDetailID,
						fsd.FeeStructure.AdmissionOpenProgram.Program.ProgramAlias,
						fsd.FeeStructure.AdmissionOpenProgram.SemesterID,
						fsd.FeeHead.HeadAlias,
						fsd.SemesterNo,
						fsd.Category,
						fsd.Amount,
						Increase = fsd.Amount * 5d / 100d,
					})
					.ToList()
					.Select(fsd => new
					{
						fsd.FeeStructureDetailID,
						fsd.ProgramAlias,
						Semester = fsd.SemesterID.ToSemesterString(),
						fsd.HeadAlias,
						SemesterNo = ((SemesterNos?)fsd.SemesterNo)?.ToFullName(),
						Category = ((Categories?)fsd.Category)?.ToFullName(),
						fsd.Amount,
						Increase = (int)Math.Round(fsd.Increase, MidpointRounding.AwayFromZero),
						NewAmount = (int)Math.Round(fsd.Amount + fsd.Increase, MidpointRounding.AwayFromZero)
					}).ToList();

				//var feeStructures = query.ToList();
				//feeStructures.ForEach(fsd => fsd.Amount = (int)Math.Round(fsd.Amount + (fsd.Amount * 5d / 100d), MidpointRounding.AwayFromZero));
				//aspireContext.SaveChangesAsSystemUserAndCommitTransaction(true);
				data.SaveFile("FeeStructure");
			}
		}

		public static void ExportWHT()
		{
			using (var aspireContext = new AspireContext())
			{
				var createdDate = DateTime.Now.AddDays(-21);
				var studentFees = aspireContext.StudentFees
							.Where(sf => sf.CreatedDate >= createdDate && sf.StudentFeeDetails.Any(sfd => sfd.FeeHead.FeeHeadType == (byte)FeeHead.FeeHeadTypes.WithholdingTax))
							.Select(sf => new
							{
								sf.Student.Enrollment,
								sf.CreatedDate,
								sf.GrandTotalAmount,
								sf.Remarks
							})
							.OrderBy(sf => sf.Enrollment).ToList();
				studentFees.SaveFile("WHT");
			}
		}

		public static void MissingGrades()
		{
		}

		private static void UpdateGrade(int registeredCourseID, ExamGrades? gradeEnum, decimal? gradePoints, decimal? product)
		{
			if (gradeEnum != null && gradePoints != null && product != null)
				using (var aspireContext = new AspireContext(true))
				{
					var registeredCourse = aspireContext.RegisteredCourses.Single(rc => rc.RegisteredCourseID == registeredCourseID && rc.DeletedDate == null && rc.Final != null && rc.Total == null && rc.Grade == null && rc.Product == null && rc.GradePoints == null);
					registeredCourse.GradeEnum = gradeEnum;
					registeredCourse.GradePoints = gradePoints;
					registeredCourse.Product = product;
					registeredCourse.Remarks = registeredCourse.Remarks?.Trim() + "\nGrade re-calculated by system because grades were null.";
					aspireContext.SaveChangesAsSystemUserAndCommitTransaction();
				}
		}

		internal class RC
		{
			public int RegisteredCourseID { get; set; }
			public int OfferedCourseID { get; set; }
			public string OfferedSemester { get; set; }
			public string Enrollment { get; set; }
			public string Status { get; set; }
			public string Title { get; set; }
			public string Class { get; set; }
			public decimal? Assignments { get; set; }
			public decimal? Quizzes { get; set; }
			public decimal? Internals { get; set; }
			public decimal? Mid { get; set; }
			public decimal? Final { get; set; }
			public byte? Total { get; set; }
			public ExamGrades? Grade { get; set; }
			public decimal? Product { get; set; }
			public decimal? GradePoints { get; set; }
		}

		private static void ForSalmanPrograms()
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.Students.GroupBy(s => new
				{
					s.AdmissionOpenProgram.Program.InstituteID,
					s.AdmissionOpenProgram.Program.Institute.InstituteName,
					s.AdmissionOpenProgram.Program.Institute.InstituteShortName,
					s.AdmissionOpenProgram.Program.Institute.InstituteAlias,
					s.AdmissionOpenProgram.Program.DepartmentID,
					s.AdmissionOpenProgram.Program.Department.DepartmentName,
					s.AdmissionOpenProgram.Program.Department.DepartmentShortName,
					s.AdmissionOpenProgram.Program.Department.DepartmentAlias,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgram.Program.ProgramName,
					s.AdmissionOpenProgram.Program.ProgramShortName,
					s.AdmissionOpenProgram.Program.ProgramAlias,
					s.AdmissionOpenProgram.Program.Duration,
					s.AdmissionOpenProgram.SemesterID
				}).Select(g => new
				{
					g.Key.InstituteName,
					g.Key.InstituteShortName,
					g.Key.InstituteAlias,
					g.Key.DepartmentName,
					g.Key.DepartmentShortName,
					g.Key.DepartmentAlias,
					g.Key.ProgramName,
					g.Key.ProgramShortName,
					g.Key.ProgramAlias,
					g.Key.Duration,
					g.Key.SemesterID,
					Students = g.Count()
				}).ToList()
					.Select(p => new
					{
						p.InstituteName,
						p.InstituteShortName,
						p.InstituteAlias,
						p.DepartmentName,
						p.DepartmentShortName,
						p.DepartmentAlias,
						p.ProgramName,
						p.ProgramShortName,
						p.ProgramAlias,
						Duration = ((ProgramDurations)p.Duration).ToFullName(),
						IntakeSemester = p.SemesterID.ToSemesterString(),
						Students = p.Students
					}).ToList()
					.SaveFile("AllPrograms", "Salman");

				aspireContext.Tehsils.Select(t => new { t.ProvinceName, t.DistrictName, t.TehsilName }).ToList().SaveFile("All Tehsils", "Salman");
			}
		}

		private static void ExportFBR()
		{
			using (var aspireContext = new AspireContext())
			{
				new[] { 2016 }.ForEach(year =>
				{
					var fromDate = new DateTime(year, 7, 1);
					var toDate = fromDate.AddYears(1);
					aspireContext.StudentFeeChallans
						.Where(sfc => sfc.StudentFee.Status == (int)StudentFee.Statuses.Paid && sfc.StudentFee.FeeType == (int)StudentFee.FeeTypes.Challan)
						.Where(sfc => sfc.AccountTransaction.TransactionDate >= fromDate && sfc.AccountTransaction.TransactionDate < toDate)
						.Where(sfc => sfc.StudentFee.StudentID != null)
						.GroupBy(sfc => new
						{
							sfc.StudentFee.StudentID,
							sfc.StudentFee.Student.Enrollment,
							sfc.StudentFee.Student.AdmissionOpenProgram.Program.Institute.InstituteAlias,
							sfc.StudentFee.Student.Name,
							sfc.StudentFee.Student.CNIC,
							sfc.StudentFee.Student.FatherName,
							sfc.StudentFee.Student.FatherCNIC,
							sfc.StudentFee.Student.CurrentAddress,
							sfc.StudentFee.Student.PermanentAddress,
						})
						.Select(sfc => new
						{
							sfc.Key.InstituteAlias,
							sfc.Key.Enrollment,
							sfc.Key.Name,
							sfc.Key.CNIC,
							sfc.Key.FatherName,
							sfc.Key.FatherCNIC,
							sfc.Key.CurrentAddress,
							sfc.Key.PermanentAddress,
							Amount = sfc.Sum(sfcc => sfcc.Amount)
						})
						.ToList()
						.GroupBy(sfc => new { sfc.InstituteAlias })
						.ForEach(sfci =>
						{
							sfci.OrderBy(s => s.Enrollment).ToList()
								.SaveFile($"{sfci.Key.InstituteAlias} {fromDate:dd-MMM-yyyy} to {toDate.AddDays(-1):dd-MMM-yyyy}",
									"Student Fees",
									new List<CSVHelper.CSVColumnMapping>
									{
										new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Enrollment#", PropertyName = "Enrollment"},
										new CSVHelper.CSVColumnMapping {ColumnIndex = 1, ColumnName = "Student Name", PropertyName = "Name"},
										new CSVHelper.CSVColumnMapping {ColumnIndex = 2, ColumnName = "Student CNIC", PropertyName = "CNIC"},
										new CSVHelper.CSVColumnMapping {ColumnIndex = 3, ColumnName = "Father Name", PropertyName = "FatherName"},
										new CSVHelper.CSVColumnMapping {ColumnIndex = 4, ColumnName = "Father CNIC", PropertyName = "FatherCNIC"},
										new CSVHelper.CSVColumnMapping {ColumnIndex = 5, ColumnName = "Current Address", PropertyName = "CurrentAddress"},
										new CSVHelper.CSVColumnMapping {ColumnIndex = 6, ColumnName = "Permanent Address", PropertyName = "PermanentAddress"},
										new CSVHelper.CSVColumnMapping {ColumnIndex = 7, ColumnName = "Fee Amount Deposited", PropertyName = "Amount"},
									});
						});
				});
			}
		}

		private static void ExportStudentInfoForPEC(string programAlias, short intakeSemesterID, int instituteID)
		{
			using (var aspireContext = new AspireContext())
			{
				aspireContext.Students
					.Where(s => s.AdmissionOpenProgram.Program.ProgramAlias == programAlias && s.AdmissionOpenProgram.Program.InstituteID == instituteID)
					.Where(s => s.AdmissionOpenProgram.SemesterID == intakeSemesterID)
					.Select(s => new
					{
						s.Enrollment,
						s.Name,
						s.FatherName,
						StatusEnum = (Student.Statuses?)s.Status,
						s.CNIC,
						s.CurrentAddress,
						s.PermanentAddress,
						s.UniversityEmail,
						s.PersonalEmail,
						s.Mobile,
						s.Phone,
						GenderEnum = (Genders)s.Gender,
						s.DOB,
						CategoryEnum = (Categories)s.Category,
						s.Religion,
						s.Country,
						s.Nationality,
						s.Domicile,
						s.Province,
						s.District,
						s.Tehsil,
						s.NextOfKin,
						StudentAcademicRecord = s.StudentAcademicRecords.OrderByDescending(sar => sar.DegreeType).FirstOrDefault()
					})
					.OrderBy(s => s.Enrollment)
					.ToList()
					.Select(s => new
					{
						s.Enrollment,
						s.Name,
						s.FatherName,
						Status = s.StatusEnum?.ToFullName(),
						s.CNIC,
						s.CurrentAddress,
						s.PermanentAddress,
						s.UniversityEmail,
						s.PersonalEmail,
						s.Mobile,
						s.Phone,
						Gender = s.GenderEnum.ToFullName(),
						s.DOB,
						Category = s.CategoryEnum.ToFullName(),
						s.Religion,
						s.Country,
						s.Nationality,
						s.Domicile,
						s.Province,
						s.District,
						s.Tehsil,
						s.NextOfKin,
						Degree = s.StudentAcademicRecord?.DegreeTypeEnum.ToFullName(),
						Subjects = s.StudentAcademicRecord?.Subjects,
						Institute = s.StudentAcademicRecord?.Institute,
						BoardUniversity = s.StudentAcademicRecord?.BoardUniversity,
						PassingYear = s.StudentAcademicRecord?.PassingYear,
						ObtainedMarks = s.StudentAcademicRecord?.ObtainedMarks.ToString("#.##"),
						TotalMarks = s.StudentAcademicRecord?.TotalMarks.ToString("#.##"),
						Percentage = s.StudentAcademicRecord?.Percentage.ToString("#.##")
					}).ToList()
					.SaveFile($"{programAlias}{intakeSemesterID}");
			}
		}

		private static void ExportResult()
		{
			using (var aspireContext = new AspireContext())
			{
				var fromSemesterID = 20163;
				var toSemesterID = 20182;

				aspireContext.StudentSemesters
				   .Where(ss => ss.Student.AdmissionOpenProgram.SemesterID >= fromSemesterID)
				   .Where(ss => ss.SemesterID <= toSemesterID)
				   .Select(ss => new
				   {
					   ss.Student.AdmissionOpenProgram.Program.InstituteID,
					   ss.Student.AdmissionOpenProgram.Program.Institute.InstituteAlias,
					   ss.Student.AdmissionOpenProgram.Program.ProgramAlias,
					   ProgramDuration = (ProgramDurations)ss.Student.AdmissionOpenProgram.Program.Duration,
					   IntakeSemesterID = ss.Student.AdmissionOpenProgram.SemesterID,
					   ss.Student.Enrollment,
					   ss.Student.RegistrationNo,
					   ss.Student.Name,
					   ss.Student.FatherName,
					   ss.SemesterID,
					   ss.GPA,
					   ss.CGPA,
					   ExamRemarksTypeEnum = (ExamRemarksTypes?)ss.ExamRemarksType,
				   })
				   .OrderBy(ss => ss.InstituteID)
				   .ThenBy(ss => ss.Enrollment)
				   .ThenBy(ss => ss.SemesterID)
				   .ToList()
				   .Select(ss => new
				   {
					   ss.InstituteAlias,
					   ss.ProgramAlias,
					   ProgramDuration = ss.ProgramDuration.ToFullName(),
					   ss.IntakeSemesterID,
					   IntakeSemester = ss.IntakeSemesterID.ToSemesterString(),
					   ss.Enrollment,
					   ss.RegistrationNo,
					   ss.Name,
					   ss.FatherName,
					   ss.SemesterID,
					   Semester = ss.SemesterID.ToSemesterString(),
					   GPA = ss.GPA.FormatGPA(),
					   CGPA = ss.CGPA.FormatGPA(),
					   Remarks = ss.ExamRemarksTypeEnum?.ToFullName()
				   })
				   .ToList().SaveFile("Exam Result");
			}
		}

		private static void Compile()
		{
			var logFileName = $"{DateTime.Now:yyyyMMddHHmmss}.log";
			var stringBuilder = new StringBuilder();
			void log(string message)
			{
				stringBuilder.AppendLine(message);
				Console.WriteLine(message);
			}
			try
			{
				using (var aspireContext = new AspireContext())
				{
					var students = aspireContext.Students
						.Where(s => s.AdmissionOpenProgram.SemesterID >= 20163 && s.AdmissionOpenProgram.SemesterID <= 20182)
						.Where(s => s.AdmissionOpenProgram.ExamRemarksPolicy.ExamRemarksPolicyDetails.Any())
						.Select(s => new
						{
							s.StudentID,
							s.AdmissionOpenProgram.Program.InstituteID,
							s.Enrollment,
						})
						.OrderByDescending(s => s.InstituteID).ThenBy(s => s.Enrollment)
						.ToList();
					var count = students.Count;

					var index = 0;
					foreach (var student in students)
					{
						using (var aspireContext1 = new AspireContext())
						{
							++index;
							var result = aspireContext1.CompileResult(20182, student.StudentID, UserLoginHistory.SystemUserLoginHistoryID);
							if (result.status == GenerateTranscripts.CompileResultStatuses.PreFall2016NotSupported)
								log($"{index:D5}/{count} : {student.Enrollment} {result.status}\n");
							else
							{
								var lastSemester = result.transcript.Semesters.LastOrDefault();
								log($"{index:D5}/{count} : {student.Enrollment} {lastSemester?.CGPA.FormatGPA()}, {lastSemester?.ExamRemarksTypeFullName}\n");
								if (result.transcript.Errors.Any())
									log(result.transcript.Errors.First());
							}
						}
					}
				}
			}
			catch (Exception exc)
			{
				log(exc.ToString());
			}

			File.WriteAllText(logFileName, stringBuilder.ToString());

			Console.ReadLine();
		}

		private static void CorrectGradePoints()
		{
			using (var aspireContext = new AspireContext())
			{
				var admissionOpenProgramExamMarksPolicies = aspireContext.AdmissionOpenProgramExamMarksPolicies.Include(aopemp => aopemp.ExamMarksPolicy).ToList();
				var registeredCourses = aspireContext.Students
					.Where(s => s.AdmissionOpenProgram.SemesterID >= 20163 && s.AdmissionOpenProgram.SemesterID <= 20182)
					.SelectMany(s => s.RegisteredCourses)
					.Where(rc => rc.DeletedDate == null && rc.Grade != null)
					.Select(rc => new
					{
						rc.RegisteredCourseID,
						rc.Total,
						GradeEnum = (ExamGrades)rc.Grade.Value,
						rc.GradePoints,
						rc.Product,
						rc.OfferedCours.SemesterID,
						rc.Cours.AdmissionOpenProgramID,
						rc.Cours.CreditHours,
						rc.Cours.AdmissionOpenProgram.IsSummerRegularSemester,
					})
					.OrderByDescending(rc => rc.RegisteredCourseID)
					.ToList();

				var count = registeredCourses.Count;
				var updated = 0;
				var gradeNotNullTotalNull = 0;
				var invalidGrade = 0;
				var processed = 0;
				var invalidMarkingPolicies = new List<(int, short)>();
				foreach (var registeredCourse in registeredCourses)
				{
					if (registeredCourse.Total == null)
					{
						if (registeredCourse.GradeEnum == ExamGrades.F)
						{
							if (registeredCourse.GradePoints != 0 || registeredCourse.Product != 0)
							{
								updated++;
								UpdateGradePoints(registeredCourse.RegisteredCourseID, 0, 0);
							}
						}
						else
							gradeNotNullTotalNull++;
					}
					else
					{
						var examMarksPolicy = admissionOpenProgramExamMarksPolicies.SingleOrDefault(aopemp => aopemp.AdmissionOpenProgramID == registeredCourse.AdmissionOpenProgramID && aopemp.SemesterID == registeredCourse.SemesterID)?.ExamMarksPolicy;
						if (examMarksPolicy != null)
						{
							var applyGradeCapping = !registeredCourse.IsSummerRegularSemester && Semester.IsSummer(registeredCourse.SemesterID);
							var (examGradeEnum, gradePoints) = examMarksPolicy.GetExamGradeAndGradePoints(registeredCourse.Total.Value, applyGradeCapping);
							var product = registeredCourse.CreditHours * gradePoints;
							if (registeredCourse.GradeEnum != examGradeEnum)
							{
								invalidGrade++;
								invalidMarkingPolicies.Add((registeredCourse.AdmissionOpenProgramID, registeredCourse.SemesterID));
							}
							else if (registeredCourse.GradePoints != gradePoints || registeredCourse.Product != product)
							{
								updated++;
								UpdateGradePoints(registeredCourse.RegisteredCourseID, gradePoints, product);
							}
						}
					}

					processed++;
					Console.WriteLine($"Count: {count},Processed: {processed}, Updated: {updated}, InvalidGrade: {invalidGrade}, TotalNull: {gradeNotNullTotalNull}");
				}

				var errors = invalidMarkingPolicies.Select(imp => new { imp.Item1, imp.Item2 }).Distinct().Count();
				Console.WriteLine("Invalid Marking Policy found: " + errors);
			}



			Console.ReadLine();
		}

		public static void UpdateGradePoints(int registeredCourseID, decimal gradePoints, decimal product)
		{
			using (var aspireContext = new AspireContext())
			{
				var registeredCourse = aspireContext.RegisteredCourses
					.SingleOrDefault(rc => rc.RegisteredCourseID == registeredCourseID && (rc.GradePoints != gradePoints || rc.Product != product));
				if (registeredCourse != null)
				{
					registeredCourse.GradePoints = gradePoints;
					registeredCourse.Product = product;
					aspireContext.SaveChanges(UserLoginHistory.SystemUserLoginHistoryID);
				}
			}
		}

		private static void ExportCdrNasirStudents()
		{
			using (var aspireContext = new AspireContext())
			{
				var students = aspireContext.RegisteredCourses.Where(rc => rc.DeletedDate == null)
					.Where(rc => rc.OfferedCours.Cours.AdmissionOpenProgram.Program.InstituteID == 1)
					.Where(rc => rc.OfferedCours.SemesterID == 20201 && rc.OfferedCours.FacultyMember.UserName == "gyro")
					.Select(rc => new
					{
						rc.OfferedCours.Cours.Title,
						rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						rc.OfferedCours.SemesterNo,
						rc.OfferedCours.Section,
						rc.OfferedCours.Shift,
						rc.Student.Enrollment,
						rc.Student.Name
					}).ToList().Select(rc => new
					{
						rc.Title,
						Class = AspireFormats.GetClassName(rc.ProgramAlias, rc.SemesterNo, rc.Section, rc.Shift),
						rc.Enrollment,
						rc.Name
					}).ToList();
				students.SaveFile("Cdr. Nasir.xls");
			}
		}

		internal static void SaveFile<T>(this List<T> data, string fileName, string directoryName = null, List<CSVHelper.CSVColumnMapping> mappings = null) where T : class
		{
			var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Export");
			if (directoryName != null)
				path = Path.Combine(path, directoryName);
			if (!Directory.Exists(path))
				Directory.CreateDirectory(path);
			path = Path.Combine(path, fileName + $" {DateTime.Now:yyyyMMddHHmmss}");
			path = Path.ChangeExtension(path, "csv");
			if (mappings != null)
				File.WriteAllText(path, data.ConvertToCSVFormat(mappings));
			else
				File.WriteAllText(path, data.ConvertToCSVFormat());
			Process.Start(path);
		}
	}
}
/*			using (var aspireContext = new AspireContext())
			{
				var registeredCourses = aspireContext.RegisteredCourses
					.Where(rc => rc.DeletedDate == null)
					.Where(rc => rc.GradePoints != null && rc.Product == null)
					.Where(rc => rc.Student.AdmissionOpenProgram.SemesterID >= 20163)
					.Select(rc => new
					{
						rc.OfferedCours.SemesterID,
						RegisteredCourse = rc,
						rc.Cours.CreditHours
					})
					.ToList()
					.Select(rc =>
					{
						var calculatedGradePoints = GetGradePoints(rc.RegisteredCourse.GradeEnum);
						var calculatedProduct = calculatedGradePoints * rc.CreditHours;
						if (rc.RegisteredCourse.GradePoints == calculatedProduct && rc.RegisteredCourse.GradePoints != null)
							return new
							{
								rc.SemesterID,
								rc.RegisteredCourse,
								calculatedGradePoints,
								calculatedProduct
							};
						return null;
					})
					.Where(rc => rc != null)
					.OrderByDescending(rc => rc.SemesterID)
					.ToList();

				Console.WriteLine("Total: " + registeredCourses.Count);
				var index = 0;
				registeredCourses.ForEach(rc =>
				{
					rc.RegisteredCourse.GradePoints = rc.calculatedGradePoints;
					rc.RegisteredCourse.Product = rc.calculatedProduct;
					index++;
					if (index % 1000 == 0)
					{
						Console.WriteLine("Done: " + index);
						aspireContext.SaveChangesAsSystemUser(false);
					}
				});
				aspireContext.SaveChangesAsSystemUser(false);
			}
*/
