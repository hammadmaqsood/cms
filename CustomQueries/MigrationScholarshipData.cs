﻿using Aspire.Lib.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web.Script.Serialization;

namespace CustomQueries
{
	class MigrationScholarshipData
	{
		public static string ConnectionStringExcel = string.Empty;

		public enum ApplicationsColumnIndexes : int
		{
			StudentID = 0,
			ScholarshipAnnouncementID = 1,
			Enrollment = 2,
		}

		public static void MigrateScholarshipApplicationsData(string filePath, Guid staffLoginSessionGuid)
		{
			var excelSheet = GetExcelFileSheets(filePath, out ConnectionStringExcel);
			DataTable excelDataTable = ExcelHelper.GetData(ConnectionStringExcel, excelSheet[0].ToString());

			int sNo = 1, rowsAdded = 0, alreadyApplied = 0, MaxSemester = 0, submittedApplications = 0;

			foreach (var row in excelDataTable.AsEnumerable().Skip(1))
			{
				var dataRow = new[] { row[ApplicationsColumnIndexes.StudentID.ToInt()], row[ApplicationsColumnIndexes.ScholarshipAnnouncementID.ToInt()], row[ApplicationsColumnIndexes.Enrollment.ToInt()] };

				var studentIDFromExcel = dataRow[ApplicationsColumnIndexes.StudentID.ToInt()].ToString().TrimAndCannotBeEmpty();
				var scholarshipAnnouncementIDFromExcel = dataRow[ApplicationsColumnIndexes.ScholarshipAnnouncementID.ToInt()].ToString().TrimAndCannotBeEmpty();
				var enrollment = dataRow[ApplicationsColumnIndexes.Enrollment.ToInt()].ToString().TrimAndCannotBeEmpty();

				int studentID = studentIDFromExcel.ToInt();
				int scholarshipAnnouncementID = scholarshipAnnouncementIDFromExcel.ToInt();
				short semesterID = 20191;
				var result = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.AddScholarshipApplication(studentID, scholarshipAnnouncementID, null, semesterID, staffLoginSessionGuid);

				switch (result.Status)
				{
					case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.AddScholarshipApplicationResult.Statuses.NotAuthorizedPerson:
						Console.WriteLine("You are not authorized to Add applications.");
						return;
					case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.AddScholarshipApplicationResult.Statuses.NoRecordFound:
						Console.WriteLine($"{sNo}. No Record Found. StudentID: {studentID}, Enrollment: {enrollment}");
						sNo++;
						break;
					case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.AddScholarshipApplicationResult.Statuses.AlreadyApplied:
						alreadyApplied++;
						Console.WriteLine($"{sNo}. Already Applied Applications. StudentID: {studentID}, Enrollment: {enrollment}");
						sNo++;
						break;
					case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.AddScholarshipApplicationResult.Statuses.MaxSemesterExceeded:
						MaxSemester++;
						Console.WriteLine($"{sNo}. Maximum Semester Exceeded Applications. StudentID: {studentID}, Enrollment: {enrollment}");
						sNo++;
						break;
					case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.AddScholarshipApplicationResult.Statuses.Success:
						rowsAdded++;
						int? applicationID = result.ScholarshipApplication.ScholarshipApplicationID;

						var familyMember = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.AddOrUpdateFamilyMember(applicationID ?? throw new InvalidOperationException(), null, 1, null, "N/A", staffLoginSessionGuid);
						var familyInformation = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.UpdateFamilyInformation(applicationID ?? throw new InvalidOperationException(), 0, 0, staffLoginSessionGuid);
						var familyIncome = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.UpdateFamilyIncome(applicationID ?? throw new InvalidOperationException(), null, null, null, null, null, 1, staffLoginSessionGuid);
						var otherIncome = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.AddOrUpdateOtherIncome(applicationID ?? throw new InvalidOperationException(), null, "N/A", 1, "N/A", staffLoginSessionGuid);
						var familyExpenditures = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.UpdateFamilyExpenditures(applicationID ?? throw new InvalidOperationException(), null, null, null, null, null, null, null, null, 0, 0, "Old Data. Imported from Excel.", staffLoginSessionGuid);

						string submittedResult = SubmittedResult(applicationID, staffLoginSessionGuid);
						if (submittedResult == "true")
							submittedApplications++;
						else if (submittedResult == "StudentInformationIsMissing")
						{
							var studentInfo = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.UpdateStudentInfo(applicationID ?? throw new InvalidOperationException(), studentID, "N/A", "0300-0000000", "N/A", staffLoginSessionGuid);
							string againSubmitResult = SubmittedResult(applicationID, staffLoginSessionGuid);
							if (againSubmitResult == "true")
								submittedApplications++;
						}
						else
						{
							Console.WriteLine($"{sNo}. Error while submition. StudentID: {studentID}, Enrollment: {enrollment} Error: {submittedResult}");
							sNo++;
						}
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(result.Status), result.Status, null);
				}
			}

			Console.WriteLine($"\nAlready Applied Applications: {alreadyApplied}");
			Console.WriteLine($"Maximum Semester Exceeded Applications: {MaxSemester}");
			Console.WriteLine($"Rows Added: {rowsAdded}");
			Console.WriteLine($"Submitted Applications: {submittedApplications}");
		}

		private static string SubmittedResult(int? scholarshipApplicationID, Guid staffLoginSessionGuid)
		{
			var result = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplication(scholarshipApplicationID ?? throw new InvalidOperationException(), staffLoginSessionGuid);
			if (result.Errors != null)
			{
				foreach (var error in result.Errors)
					switch (error)
					{
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.NotAuthorizedForChange:
							return "NotAuthorizedForChange";
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.NoRecordFound:
							return "NoRecordFound";
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.AlreadySubmitted:
							return "AlreadySubmitted";
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.StudentInformationIsMissing:
							return "StudentInformationIsMissing";
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.FamilyMembersAreMissing:
							return "FamilyMembersAreMissing";
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.FamilyInformationIsMissing:
							return "FamilyInformationIsMissing";
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.FamilyIncomeIsMissing:
							return "FamilyIncomeIsMissing";
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.FamilyExpendituresAreMissing:
							return "FamilyExpendituresAreMissing";
						case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.SubmitApplicationResult.Error.Success:
							throw new InvalidOperationException();
						default:
							throw new ArgumentOutOfRangeException(nameof(error), error, null);
					}
				return "false";
			}
			return "true";
		}

		//------------------------------------------Updating Approval Data-----------------------------------------------------------------------

		public enum ApprovalColumnIndexes : int
		{
			Data = 0,
			ApprovalStatus = 1,
			StudentID = 2,
		}

		public static void UpdateApprovalData(string filePath, Guid staffLoginSessionGuid)
		{
			int sNo = 1, rowsUpdated = 0, changesNotAllowed = 0;
			var excelSheet = GetExcelFileSheets(filePath, out ConnectionStringExcel);
			DataTable excelDataTable = ExcelHelper.GetData(ConnectionStringExcel, excelSheet[1].ToString());

			foreach (var row in excelDataTable.AsEnumerable().Skip(1))
			{
				var dataRow = new[]
				{
					row[ApprovalColumnIndexes.Data.ToInt()], row[ApprovalColumnIndexes.ApprovalStatus.ToInt()], row[ApprovalColumnIndexes.StudentID.ToInt()]
				};

				var dataFromExcel = dataRow[ApprovalColumnIndexes.Data.ToInt()].ToString().TrimAndCannotBeEmpty();
				var approvalStatusFromExcel = dataRow[ApprovalColumnIndexes.ApprovalStatus.ToInt()].ToString().TrimAndCannotBeEmpty();
				var studentIDFromExcel = dataRow[ApprovalColumnIndexes.StudentID.ToInt()].ToString().TrimAndCannotBeEmpty();

				int studentID = studentIDFromExcel.ToInt();
				var approval = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.GetApprovalForMigratingData(studentID);
				if (approval == null)
					continue;

				byte approvalStatus = approvalStatusFromExcel.ToByte();
				int? ScholarshipApplicationApprovalID = approval.ScholarshipApplicationApprovalID;
				int? ScholarshipApplicationID = approval.ScholarshipApplicationID;

				var deserializedData = new JavaScriptSerializer().Deserialize<ApprovalData>(dataFromExcel);

				var tuitionFee = deserializedData.TuitionFee;
				var CSCRemarks = deserializedData.CSCRemarks;
				var finalRecommendation = deserializedData.FinalRecommendation;
				var campusRecommendation = "";
				var campusRemarks = "";
				var CSCRecommendation = "";

				var result = Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.UpdateApplicationApproval(ScholarshipApplicationApprovalID ?? throw new InvalidOperationException(), ScholarshipApplicationID ?? throw new InvalidOperationException(),
					tuitionFee.ToNullableInt(), campusRecommendation.ToNullableInt(), campusRemarks, CSCRecommendation.ToNullableInt(), CSCRemarks, finalRecommendation.ToNullableInt(), (Aspire.Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses)approvalStatus, staffLoginSessionGuid);

				switch (result)
				{
					case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.UpdateApplicationApprovalStatuses.NoRecordFound:
						Console.WriteLine($"You are not authorized to Add applications.");
						return;
					case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.UpdateApplicationApprovalStatuses.TutionFeeAndApprovalEmpty:
						Console.WriteLine($"{sNo}. Tution Fee and Approval % should not empty for Approved Application.");
						sNo++;
						break;
					case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.UpdateApplicationApprovalStatuses.NotAllowedChangesInApprovedOrRejectedApplication:
						changesNotAllowed++;
						Console.WriteLine($"{sNo}. Changes are not Allowed. StudentID: {studentID}");
						sNo++;
						break;
					case Aspire.BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.UpdateApplicationApprovalStatuses.Success:
						rowsUpdated++;
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(result), result, null);
				}
			}

			Console.WriteLine($"\nChanges Not Allowed Applications: {changesNotAllowed}");
			Console.WriteLine($"No of Updated Applications: {rowsUpdated}\n");
		}

		private class ApprovalData
		{
			public string TuitionFee { get; set; }
			public string CSCRemarks { get; set; }
			public string FinalRecommendation { get; set; }
		}

		private static List<string> GetExcelFileSheets(string filePath, out string connectionStringExcel)
		{
			var connectionString = ExcelHelper.GetOledbConnectionString(filePath, ExcelHelper.ExcelFormat.Xlsx, false);
			connectionStringExcel = connectionString;
			if (!ExcelHelper.TestConnection(connectionString, out string errorMessage))
				return null;
			var sheets = ExcelHelper.GetAllSheetNames(connectionString);
			return sheets;
		}
	}

	#region ExcelHelper
	public static class ExcelHelper
	{
		public enum ExcelFormat
		{
			Xls,
			Xlsx,
		}

		private static string GetExcelVersion(this ExcelFormat excelFormat)
		{
			switch (excelFormat)
			{
				case ExcelFormat.Xls:
					return "8.0";
				case ExcelFormat.Xlsx:
					return "12.0";
				default:
					throw new ArgumentOutOfRangeException(nameof(excelFormat), excelFormat, null);
			}
		}

		public static string GetOledbConnectionString(string fileName, ExcelFormat excelFormat = ExcelFormat.Xlsx, bool hdr = true, int imex = 1)
		{
			return new OleDbConnectionStringBuilder
			{
				{"Provider", "Microsoft.ACE.OLEDB.12.0"},
				{"Data Source", fileName},
				{"Extended Properties", $"Excel {excelFormat.GetExcelVersion()};HDR={(hdr ? "YES" : "NO")};IMEX={imex}"},
			}.ConnectionString;
		}

		public static bool TestConnection(string connectionString, out string errorMessage)
		{
			try
			{
				using (var oledbConn = new OleDbConnection(connectionString))
				{
					oledbConn.Open();
					oledbConn.Close();
					errorMessage = null;
					return true;
				}
			}

			catch (Exception exc)
			{
				errorMessage = exc.Message;
				return false;
			}
		}

		public static List<string> GetAllSheetNames(string connectionString)
		{
			using (var oledbConn = new OleDbConnection(connectionString))
			{
				oledbConn.Open();
				using (var dataTable = oledbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null))
				{
					return dataTable?.AsEnumerable().Select(r => (string)r["TABLE_NAME"]).ToList();
				}
			}
		}

		public static DataTable GetData(string connectionString, string sheetName)
		{
			using (var oledbAdapter = new OleDbDataAdapter($"SELECT * FROM [{sheetName}]", connectionString))
			{
				var dataTable = new DataTable();
				oledbAdapter.Fill(dataTable);
				return dataTable;
			}
		}

		public static List<string> GetExcelFileSheets(string filePath, out string connectionStringExcel)
		{
			var connectionString = ExcelHelper.GetOledbConnectionString(filePath, ExcelHelper.ExcelFormat.Xlsx, false);
			connectionStringExcel = connectionString;
			if (!ExcelHelper.TestConnection(connectionString, out string errorMessage))
				return null;
			var sheets = ExcelHelper.GetAllSheetNames(connectionString);
			return sheets;
		}
	}
	#endregion
}
