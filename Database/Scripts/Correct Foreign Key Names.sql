﻿DECLARE FK CURSOR FOR
SELECT sch.name AS schema_name, obj.name AS FK_NAME, tab1.name AS [table], col1.name AS [column], tab2.name AS referenced_table, col2.name AS referenced_column
FROM            sys.foreign_key_columns AS fkc INNER JOIN
                         sys.objects AS obj ON obj.object_id = fkc.constraint_object_id INNER JOIN
                         sys.tables AS tab1 ON tab1.object_id = fkc.parent_object_id INNER JOIN
                         sys.schemas AS sch ON tab1.schema_id = sch.schema_id INNER JOIN
                         sys.columns AS col1 ON col1.column_id = fkc.parent_column_id AND col1.object_id = tab1.object_id INNER JOIN
                         sys.tables AS tab2 ON tab2.object_id = fkc.referenced_object_id INNER JOIN
                         sys.columns AS col2 ON col2.column_id = fkc.referenced_column_id AND col2.object_id = tab2.object_id;
DECLARE @FK_NAME SYSNAME;
DECLARE @SCHEMA SYSNAME;
DECLARE @TABLE SYSNAME;
DECLARE @COLUMN SYSNAME;
DECLARE @REF_TABLE SYSNAME;
DECLARE @REF_COLUMN SYSNAME;
OPEN FK;
FETCH NEXT FROM FK INTO @SCHEMA, @FK_NAME, @TABLE, @COLUMN, @REF_TABLE, @REF_COLUMN;
WHILE @@FETCH_STATUS = 0  
BEGIN
	DECLARE @SUGGESTED_NAME SYSNAME = 'FK_' + @TABLE + '_' + @REF_TABLE + '_' + @COLUMN;
	IF @SUGGESTED_NAME COLLATE SQL_Latin1_General_CP1_CS_AS <> @FK_NAME
	BEGIN
		PRINT QUOTENAME(@SCHEMA) + '.' + QUOTENAME(@FK_NAME) + '=> ' + @SUGGESTED_NAME;
		DECLARE @OLD_FK_NAME SYSNAME = QUOTENAME(@SCHEMA) + '.' + QUOTENAME(@FK_NAME);
		EXEC sp_rename @OLD_FK_NAME, @SUGGESTED_NAME;
	END
	IF @TABLE = @REF_TABLE AND @COLUMN = @REF_COLUMN
	BEGIN
		DECLARE @SQL VARCHAR(MAX) = 'ALTER TABLE ' + QUOTENAME(@SCHEMA) + '.' + QUOTENAME(@TABLE) + ' DROP CONSTRAINT ' + QUOTENAME(@FK_NAME) + ';'
		EXECUTE (@SQL);
		PRINT 'FOREIGN KEY ' + QUOTENAME(@FK_NAME) + ' DELETED BECAUSE SELF JOIN TO SAME COLUMN.';
	END
	FETCH NEXT FROM FK INTO @SCHEMA, @FK_NAME, @TABLE, @COLUMN, @REF_TABLE, @REF_COLUMN;
END
CLOSE FK;
DEALLOCATE FK;

DECLARE PK CURSOR FOR 
SELECT TC.TABLE_SCHEMA, TC.TABLE_NAME, TC.CONSTRAINT_NAME
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS TC JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE CCU ON TC.CONSTRAINT_NAME = CCU.CONSTRAINT_NAME
WHERE TC.CONSTRAINT_TYPE = 'PRIMARY KEY' AND TC.CONSTRAINT_NAME<>'PK_'+TC.TABLE_NAME;
OPEN PK;
DECLARE @PK_NAME SYSNAME;
FETCH NEXT FROM PK INTO @SCHEMA, @TABLE, @PK_NAME;
WHILE @@FETCH_STATUS = 0  
BEGIN
	SET @SUGGESTED_NAME = 'PK_' + @TABLE;
	EXEC SP_RENAME @OBJNAME = @PK_NAME, @NEWNAME = @SUGGESTED_NAME, @OBJTYPE = 'OBJECT';
	PRINT 'PK RENAMED TABLE: ' + QUOTENAME(@TABLE);
	FETCH NEXT FROM PK INTO @SCHEMA, @TABLE, @PK_NAME;
END
CLOSE PK;
DEALLOCATE PK;
