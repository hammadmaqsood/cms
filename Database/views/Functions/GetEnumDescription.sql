﻿CREATE FUNCTION [views].[GetEnumDescription](@EntityName NVARCHAR(128), @EnumType VARCHAR(100), @Value BIGINT, @EnumFlag BIT)
RETURNS VARCHAR(100)
AS
BEGIN
	IF @Value IS NULL
		RETURN NULL;
	DECLARE @Fullname VARCHAR(1000);
	IF @EntityName IS NULL
		SELECT @Fullname = ISNULL([FullName], [Name]) FROM [dbo].[EnumValues]
		WHERE TableName IS NULL AND [EnumType] = @EnumType AND [Value] = @Value;
	ELSE
	BEGIN
		SELECT @Fullname = ISNULL([FullName], [Name]) FROM [dbo].[EnumValues]
		WHERE [TableName] = @EntityName AND [EnumType] = @EnumType AND [Value] = @Value;
		IF @Fullname IS NULL
			SELECT @Fullname = ISNULL([FullName], [Name]) FROM [dbo].[EnumValues]
			WHERE TableName IS NULL AND [EnumType] = @EnumType AND [Value] = @Value;
	END
	IF @FullName IS NULL
	BEGIN
		IF @EnumFlag = 0
			RETURN CAST('Entity: ' +  + ISNULL(@EntityName,'NULL') + ', EnumType: ' + ISNULL(@EnumType,'NULL') + ', EnumValue: ' + CONVERT(VARCHAR(100), @Value) + ', IsEnumFlag: ' + CONVERT(VARCHAR(1), @EnumFlag) + ' missing.' AS INT);
		DECLARE @EValue BIGINT;
		DECLARE @EName VARCHAR(100);
		DECLARE @EFullName VARCHAR(1000);
		DECLARE CF CURSOR FOR SELECT [Value], [Name], [FullName] FROM [dbo].[EnumValues] WHERE ((@EntityName IS NULL AND TableName IS NULL) OR TableName = @EntityName) AND [EnumType] = @EnumType;
		OPEN CF;
		FETCH CF INTO @EValue, @EName, @EFullName;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF (@EValue & @Value) = @EValue
				SET @Fullname = ISNULL(@Fullname,'') + ', ' + @EFullName;
			FETCH CF INTO @EValue, @EName, @EFullName;
		END;
		CLOSE CF;
		DEALLOCATE CF;
		IF @Fullname IS NULL
		BEGIN
			DECLARE CF CURSOR FOR SELECT [Value], [Name], [FullName] FROM [dbo].[EnumValues] WHERE TableName IS NULL AND [EnumType] = @EnumType;
			OPEN CF;
			FETCH CF INTO @EValue, @EName, @EFullName;
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF (@EValue & @Value) = @EValue
					SET @Fullname = ISNULL(@Fullname,'') + ', ' + @EFullName;
				FETCH CF INTO @EValue, @EName, @EFullName;
			END;
			CLOSE CF;
			DEALLOCATE CF;
		END
		IF @FullName IS NULL
			RETURN CAST('Entity: ' +  + ISNULL(@EntityName,'NULL') + ', EnumType: ' + ISNULL(@EnumType,'NULL') + ', EnumValue: ' + CONVERT(VARCHAR(100), @Value) + ', IsEnumFlag: ' + CONVERT(VARCHAR(1), @EnumFlag) + ' missing.' AS INT);
		SET @FullName = SUBSTRING(@FullName, 2, LEN(@FullName) - 1);
	END
	RETURN @FullName;
END