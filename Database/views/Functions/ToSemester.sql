﻿CREATE FUNCTION [views].[ToSemester](@SemesterID SMALLINT)
RETURNS VARCHAR(100)
AS
BEGIN
	DECLARE @Year CHAR(5) = CONVERT(CHAR(4), @SemesterID / 10);
	DECLARE @Semester CHAR(1) = CONVERT(CHAR(1),@SemesterID % 10);
	IF @Semester = '1'
		RETURN 'Spring-' + @Year;
	ELSE IF @Semester = '2'
		RETURN 'Summer-' + @Year;
	IF @Semester = '3'
		RETURN 'Fall-' + @Year;
	RETURN CONVERT(VARCHAR(100), @SemesterID);
END