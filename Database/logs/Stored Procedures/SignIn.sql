﻿CREATE PROCEDURE [logs].[SignIn]
AS
BEGIN
	BEGIN TRY
		DECLARE @ORIGINAL_LOGIN SYSNAME = ORIGINAL_LOGIN();
		DECLARE @UserLoginHistoryID INT = 1;--Initially System Used for Insert
		DECLARE @LoginSessionGuid UNIQUEIDENTIFIER = NEWID();
		DECLARE @UserType dbo.UserType = 2048;--Manual SQL
		DECLARE @SubUserType dbo.Enum = 0;
		DECLARE @UserID INT = NULL;
		DECLARE @UserRoleID INT = NULL;
		DECLARE @CandidateID INT = NULL;
		DECLARE @StudentID INT = NULL;
		DECLARE @FacultyMemberID INT = NULL;
		DECLARE @IntegratedServiceUserID INT = NULL;
		DECLARE @UserName varchar(50) = @ORIGINAL_LOGIN;
		DECLARE @IPAddress varchar = CONVERT(VARCHAR(50), CONNECTIONPROPERTY('client_net_address'));
		DECLARE @UserAgent varchar = NULL;
		DECLARE @LoginStatus dbo.Enum = 1;
		DECLARE @LoginDate datetime = SYSDATETIME();
		DECLARE @LoginFailureReason dbo.Enum = NULL;
		DECLARE @LogOffDate datetime = NULL;
		DECLARE @LogOffReason dbo.Enum = NULL;
		DECLARE @ImpersonatedByUserLoginHistoryID INT = NULL;

		EXEC sys.sp_set_session_context @key = 'Logging', @value = N'1', @readonly = 1;
		EXEC sys.sp_set_session_context @key = 'UserLoginHistoryID', @value = @UserLoginHistoryID, @readonly = 0;
		INSERT INTO [dbo].[UserLoginHistory]([LoginSessionGuid], [UserType], [SubUserType], [UserID], [UserRoleID], [CandidateID], [StudentID], [FacultyMemberID], [IntegratedServiceUserID], [UserName], [IPAddress], [UserAgent], [LoginStatus], [LoginDate], [LoginFailureReason], [LogOffDate], [LogOffReason], [ImpersonatedByUserLoginHistoryID]) 
		VALUES(@LoginSessionGuid, @UserType, @SubUserType, @UserID, @UserRoleID, @CandidateID, @StudentID, @FacultyMemberID, @IntegratedServiceUserID, @UserName, @IPAddress, @UserAgent, @LoginStatus, @LoginDate, @LoginFailureReason, @LogOffDate, @LogOffReason, @ImpersonatedByUserLoginHistoryID);
		SET @UserLoginHistoryID = SCOPE_IDENTITY();
		EXEC sys.sp_set_session_context @key = 'UserLoginHistoryID', @value = @UserLoginHistoryID, @readonly = 1;
		PRINT FORMATMESSAGE('Generated UserLoginHistoryID: %i', @UserLoginHistoryID);
	END TRY
	BEGIN CATCH
		PRINT FORMATMESSAGE('Generated UserLoginHistoryID: ' + CONVERT(VARCHAR(10), SESSION_CONTEXT(N'UserLoginHistoryID')));
		THROW;
	END CATCH
END