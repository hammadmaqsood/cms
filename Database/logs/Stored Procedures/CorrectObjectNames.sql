﻿
CREATE PROCEDURE [logs].[CorrectObjectNames]
AS
BEGIN
	DECLARE @SUGGESTED_NAME SYSNAME;
	DECLARE @SCHEMA SYSNAME;
	DECLARE @TABLE SYSNAME;
	DECLARE @CONSTRAINT_NAME SYSNAME;
	DECLARE @OLD_FULL_NAME NVARCHAR(300);
	DECLARE @SQL NVARCHAR(MAX);

	EXECUTE('DROP TABLE IF EXISTS [dbo].[__RefactorLog]');

	BEGIN--Correct Primary Key Constraint Names
		DECLARE PK CURSOR FOR
			SELECT TC.TABLE_SCHEMA, TC.TABLE_NAME, TC.CONSTRAINT_NAME
			FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS TC JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE CCU ON TC.CONSTRAINT_NAME = CCU.CONSTRAINT_NAME
			WHERE TC.CONSTRAINT_TYPE = 'PRIMARY KEY';
		OPEN PK;
		FETCH NEXT FROM PK INTO @SCHEMA, @TABLE, @CONSTRAINT_NAME;
		WHILE @@FETCH_STATUS = 0  
		BEGIN
			SET @OLD_FULL_NAME = CASE @SCHEMA WHEN 'dbo' THEN QUOTENAME(@CONSTRAINT_NAME) ELSE QUOTENAME(@SCHEMA) + '.' + QUOTENAME(@CONSTRAINT_NAME) END;
			SET @SUGGESTED_NAME = 'PK_' + @TABLE;
			IF @CONSTRAINT_NAME COLLATE SQL_Latin1_General_CP1_CS_AS <> @SUGGESTED_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
			BEGIN
				PRINT 'RENAMING ' + @OLD_FULL_NAME + ' => ' + @SUGGESTED_NAME;
				EXEC SP_RENAME @OBJNAME = @OLD_FULL_NAME, @NEWNAME = @SUGGESTED_NAME, @OBJTYPE = 'OBJECT';
			END
			FETCH NEXT FROM PK INTO @SCHEMA, @TABLE, @CONSTRAINT_NAME;
		END
		CLOSE PK;
		DEALLOCATE PK;
	END

	BEGIN--Correct Foreign Key Constraint Names
		DECLARE FK CURSOR FOR
		SELECT sch.name AS schema_name, obj.name AS FK_NAME, tab1.name AS [table], col1.name AS [column], tab2.name AS referenced_table, col2.name AS referenced_column
		FROM sys.foreign_key_columns AS fkc INNER JOIN
								 sys.objects AS obj ON obj.object_id = fkc.constraint_object_id INNER JOIN
								 sys.tables AS tab1 ON tab1.object_id = fkc.parent_object_id INNER JOIN
								 sys.schemas AS sch ON tab1.schema_id = sch.schema_id INNER JOIN
								 sys.columns AS col1 ON col1.column_id = fkc.parent_column_id AND col1.object_id = tab1.object_id INNER JOIN
								 sys.tables AS tab2 ON tab2.object_id = fkc.referenced_object_id INNER JOIN
								 sys.columns AS col2 ON col2.column_id = fkc.referenced_column_id AND col2.object_id = tab2.object_id;
		DECLARE @COLUMN SYSNAME;
		DECLARE @REF_TABLE SYSNAME;
		DECLARE @REF_COLUMN SYSNAME;
		OPEN FK;
		FETCH NEXT FROM FK INTO @SCHEMA, @CONSTRAINT_NAME, @TABLE, @COLUMN, @REF_TABLE, @REF_COLUMN;
		WHILE @@FETCH_STATUS = 0  
		BEGIN
			SET @OLD_FULL_NAME = CASE @SCHEMA WHEN 'dbo' THEN QUOTENAME(@CONSTRAINT_NAME) ELSE QUOTENAME(@SCHEMA) + '.' + QUOTENAME(@CONSTRAINT_NAME) END;
			SET @SUGGESTED_NAME = 'FK_' + @TABLE + '_' + @REF_TABLE + '_' + @COLUMN;
			IF @TABLE = @REF_TABLE AND @COLUMN = @REF_COLUMN
			BEGIN
				PRINT 'DROPPING FOREIGN KEY ' + @OLD_FULL_NAME + ' BECAUSE OF SELF JOIN TO SAME COLUMN.';
				SET @SQL = 'ALTER TABLE ' + QUOTENAME(@SCHEMA) + '.' + QUOTENAME(@TABLE) + ' DROP CONSTRAINT ' + QUOTENAME(@CONSTRAINT_NAME) + ';';
				PRINT @SQL;
				EXECUTE (@SQL);
			END
			ELSE
			BEGIN
				IF @CONSTRAINT_NAME COLLATE SQL_Latin1_General_CP1_CS_AS <> @SUGGESTED_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
				BEGIN
					PRINT 'RENAMING ' + @OLD_FULL_NAME + ' => ' + @SUGGESTED_NAME;
					EXEC SP_RENAME @OBJNAME = @OLD_FULL_NAME, @NEWNAME = @SUGGESTED_NAME, @OBJTYPE = 'OBJECT';
				END
			END
			FETCH NEXT FROM FK INTO @SCHEMA, @CONSTRAINT_NAME, @TABLE, @COLUMN, @REF_TABLE, @REF_COLUMN;
		END
		CLOSE FK;
		DEALLOCATE FK;
	END

	BEGIN--Remove Default Value Constraints
		DECLARE DC CURSOR FOR
			SELECT SCHEMA_NAME(t.schema_id) AS schema_name, t.name AS table_name, DF.NAME AS constraint_name
			FROM sys.default_constraints df
				INNER JOIN sys.tables t ON df.parent_object_id = t.object_id
				INNER JOIN sys.columns c ON c.object_id = df.parent_object_id AND df.parent_column_id = c.column_id;
		OPEN DC;
		FETCH NEXT FROM DC INTO @SCHEMA, @TABLE, @CONSTRAINT_NAME;
		WHILE @@FETCH_STATUS = 0  
		BEGIN
			SET @OLD_FULL_NAME = QUOTENAME(@CONSTRAINT_NAME);
			PRINT 'DROPPING DEFAULT CONSTRAINT ' + @OLD_FULL_NAME;
			SET @SQL = 'ALTER TABLE ' + QUOTENAME(@SCHEMA) + '.' + QUOTENAME(@TABLE) + ' DROP CONSTRAINT ' + @OLD_FULL_NAME + ';';
			PRINT @SQL;
			EXEC(@SQL);
			FETCH NEXT FROM DC INTO @SCHEMA, @TABLE, @CONSTRAINT_NAME;
		END
		CLOSE DC;
		DEALLOCATE DC;
	END

	BEGIN--Correct Unique Key Constraint Names
		DECLARE UK CURSOR FOR
			SELECT DISTINCT i.object_id, i.index_id, SCHEMA_NAME(t.schema_id) AS schema_name, i.name as constraint_name, t.name as table_name
			FROM	sys.indexes AS i INNER JOIN
					sys.index_columns AS ic ON i.index_id = ic.index_id AND i.object_id = ic.object_id INNER JOIN
					sys.columns AS c ON ic.column_id = c.column_id AND ic.object_id = c.object_id INNER JOIN
					sys.tables AS t ON c.object_id = t.object_id
			WHERE (i.is_unique_constraint = 1);
		DECLARE @INDEX_ID INT;
		DECLARE @OBJECT_ID INT;
		OPEN UK;
		FETCH NEXT FROM UK INTO @OBJECT_ID, @INDEX_ID, @SCHEMA, @CONSTRAINT_NAME, @TABLE;
		WHILE @@FETCH_STATUS = 0  
		BEGIN
			SET @OLD_FULL_NAME = CASE @SCHEMA WHEN 'dbo' THEN QUOTENAME(@CONSTRAINT_NAME) ELSE QUOTENAME(@SCHEMA) + '.' + QUOTENAME(@CONSTRAINT_NAME) END;
			SET @SUGGESTED_NAME = 'UK_' + @TABLE;
			DECLARE UKC CURSOR FOR
				SELECT c.name
				FROM	sys.indexes AS i INNER JOIN
						sys.index_columns AS ic ON i.index_id = ic.index_id AND i.object_id = ic.object_id INNER JOIN
						sys.columns AS c ON ic.column_id = c.column_id AND ic.object_id = c.object_id INNER JOIN
						sys.tables AS t ON c.object_id = t.object_id
				WHERE i.object_id = @OBJECT_ID AND i.index_id = @INDEX_ID
				ORDER BY ic.key_ordinal;
			OPEN UKC;
			FETCH UKC INTO @COLUMN;
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @SUGGESTED_NAME = @SUGGESTED_NAME + '_' + @COLUMN;				
				FETCH UKC INTO @COLUMN;
			END;
			CLOSE UKC;
			DEALLOCATE UKC;
			IF @CONSTRAINT_NAME COLLATE SQL_Latin1_General_CP1_CS_AS <> @SUGGESTED_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
			BEGIN
				PRINT 'RENAMING ' + @OLD_FULL_NAME + ' => ' + @SUGGESTED_NAME;
				EXEC SP_RENAME @OBJNAME = @OLD_FULL_NAME, @NEWNAME = @SUGGESTED_NAME, @OBJTYPE = 'OBJECT';
			END
			FETCH NEXT FROM UK INTO @OBJECT_ID, @INDEX_ID, @SCHEMA, @CONSTRAINT_NAME, @TABLE;
		END
		CLOSE UK;
		DEALLOCATE UK;
	END
	
	BEGIN--Correct CHECK Constraint Names
		DECLARE CK CURSOR FOR
			SELECT SCHEMA_NAME(objects.schema_id) as schema_name, objects.name table_name, check_constraints.name, check_constraints.definition
			FROM sys.check_constraints INNER JOIN sys.objects ON sys.check_constraints.parent_object_id= sys.objects.object_id;
		OPEN CK;
		DECLARE @DEFINITION NVARCHAR(MAX);
		FETCH NEXT FROM CK INTO @SCHEMA, @TABLE, @CONSTRAINT_NAME, @DEFINITION;
		WHILE @@FETCH_STATUS = 0  
		BEGIN
			SET @OLD_FULL_NAME = CASE @SCHEMA WHEN 'dbo' THEN QUOTENAME(@CONSTRAINT_NAME) ELSE QUOTENAME(@SCHEMA) + '.' + QUOTENAME(@CONSTRAINT_NAME) END;
			SET @SUGGESTED_NAME = 'CK_' + @TABLE + ' ' + @DEFINITION;
			IF @CONSTRAINT_NAME COLLATE SQL_Latin1_General_CP1_CS_AS <> @SUGGESTED_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
			BEGIN
				PRINT 'RENAMING ' + @OLD_FULL_NAME + ' => ' + @SUGGESTED_NAME;
				EXEC SP_RENAME @OBJNAME = @OLD_FULL_NAME, @NEWNAME = @SUGGESTED_NAME, @OBJTYPE = 'OBJECT';
			END
			FETCH NEXT FROM CK INTO @SCHEMA, @TABLE, @CONSTRAINT_NAME, @DEFINITION;
		END
		CLOSE CK;
		DEALLOCATE CK;
	END
END