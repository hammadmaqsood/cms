﻿CREATE PROCEDURE [logs].[RemoveLogTriggerAndTable]
	@TABLE_SCHEMA SYSNAME,
	@TABLE_NAME SYSNAME,
	@TRIGGER_NAME SYSNAME OUT
AS
BEGIN
	SET @TRIGGER_NAME = QUOTENAME(@TABLE_SCHEMA) + '.' + QUOTENAME('Trg' + @TABLE_NAME + 'Audit');
	DECLARE @SQL NVARCHAR(MAX);

	SET @SQL = '
IF OBJECT_ID(''' + @TRIGGER_NAME + ''', ''TR'') IS NOT NULL
BEGIN
	DROP TRIGGER ' + @TRIGGER_NAME + ';
	DROP TABLE [logs].' + QUOTENAME(@TABLE_NAME) + ';
END	';
	PRINT (@SQL);
	EXEC(@SQL);
END