﻿CREATE PROCEDURE [logs].[CreateViews]
AS
BEGIN
	SET NOCOUNT ON;  
	DECLARE CTABLES CURSOR FOR SELECT TABLE_SCHEMA, TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_TYPE = 'BASE TABLE' ORDER BY TABLE_SCHEMA, TABLE_NAME;
	OPEN CTABLES;
	DECLARE @EXCLUDED_DATA_TYPES TABLE(DATA_TYPE SYSNAME);
	DECLARE @COLUMN_MAPPINGS TABLE(TABLE_SCHEMA SYSNAME, TABLE_NAME SYSNAME, COLUMN_NAME SYSNAME, ENUM_TYPE SYSNAME, IS_ENUM_FLAG BIT);
	INSERT INTO @EXCLUDED_DATA_TYPES(DATA_TYPE)
	VALUES
		('Remarks'),
		('Address'),
		('Url'),
		('PersonName'),
		('CreditHours'),
		('Email'),
		('EncryptedHashed'),
		('CNIC'),
		('Phone'),
		('ExamMarks'),
		('GPA'),
		('FileName');
	INSERT INTO @COLUMN_MAPPINGS(TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME, ENUM_TYPE, IS_ENUM_FLAG)
	VALUES
		('dbo', 'AdmissionCriteriaPreReqDegrees', 'DegreeType', 'DegreeTypes', 0),
		('dbo', 'AdmissionOpenPrograms', 'Shifts', 'Shifts', 1),
		('dbo', 'AdmissionOpenPrograms', 'EntryTestType', 'EntryTestTypes', 0),
		('dbo', 'AdmissionOpenPrograms', 'ExamSystemType', 'ExamSystemTypes', 0),
		('dbo', 'CancelledAdmissions', 'Status', 'Statuses', 0),
		('dbo', 'CandidateAppliedProgramAcademicRecords', 'DegreeType', 'DegreeTypes', 0),
		('dbo', 'CandidateAppliedPrograms', 'Shift', 'Shifts', 0),
		('dbo', 'CandidateAppliedPrograms', 'ResultAwaitedDegreeType', 'DegreeTypes', 0),
		('dbo', 'CandidateAppliedPrograms', 'DepositPlace', 'DepositPlaces', 0),
		('dbo', 'CandidateAppliedPrograms', 'FeeUpdatedByUserType', 'FeeUpdatedByUserTypes', 0),
		('dbo', 'CandidateAppliedPrograms', 'ETSType', 'ETSTypes', 0),
		('dbo', 'CandidateAppliedPrograms', 'AdmittedShift', 'Shifts', 0),
		('dbo', 'CandidateAppliedPrograms', 'Status', 'Statuses', 0),
		('dbo', 'CandidateAppliedPrograms', 'DeferredToShift', 'Shifts', 0),
		('dbo', 'Candidates', 'Gender', 'Genders', 0),
		('dbo', 'Candidates', 'Category', 'Categories', 0),
		('dbo', 'Candidates', 'BloodGroup', 'BloodGroups', 0),
		('dbo', 'Candidates', 'AreaType', 'AreaTypes', 0),
		('dbo', 'Candidates', 'SponsoredBy', 'Sponsorships', 0),
		('dbo', 'Candidates', 'SourceOfInformation', 'SourceOfInformations', 0),
		('dbo', 'Candidates', 'Status', 'Statuses', 1),
		('dbo', 'Candidates', 'EditStatus', 'EditStatuses', 0),
		('dbo', 'CBTLabs', 'Status', 'Statuses', 0),
		('dbo', 'ClassAttendanceSettings', 'Status', 'Statuses', 0),
		('dbo', 'CourseRegistrationSettings', 'Type', 'Types', 0),
		('dbo', 'Courses', 'CourseCategory', 'CourseCategories', 0),
		('dbo', 'Courses', 'CourseType', 'CourseTypes', 0),
		('dbo', 'Courses', 'SemesterNo', 'SemesterNos', 0),
		('dbo', 'CustomFields', 'FieldType', 'FieldTypes', 0),
		('dbo', 'CustomFields', 'Status', 'Statuses', 0),
		('dbo', 'CustomFields', 'RelationshipType', 'RelationshipTypes', 0),
		('dbo', 'Departments', 'Status', 'Statuses', 0),
		('dbo', 'ExamBuildings', 'Status', 'Statuses', 0),
		('dbo', 'ExamConducts', 'ExamType', 'ExamTypes', 0),
		('dbo', 'ExamInvigilatorDuties', 'InvigilatorType', 'InvigilatorTypes', 0),
		('dbo', 'ExamInvigilatorDuties', 'AttendanceStatus', 'AttendanceStatuses', 0),
		('dbo', 'ExamInvigilators', 'Status', 'Statuses', 0),
		('dbo', 'ExamMarkingSchemeDetails', 'ExamMarksType', 'ExamMarksTypes', 0),
		('dbo', 'ExamMarkingSchemeDetails', 'CourseCategory', 'CourseCategories', 0),
		('dbo', 'ExamMarksGradingSchemeGradeRanges', 'Grade', 'ExamGrades', 0),
		('dbo', 'ExamMarksSettings', 'ExamMarksTypes', 'ExamMarksTypes', 1),
		('dbo', 'ExamOfferedRooms', 'RoomUsageType', 'RoomUsageTypes', 0),
		('dbo', 'ExamRooms', 'Status', 'Statuses', 0),
		('dbo', 'ExamSessions', 'Shift', 'Shifts', 0),
		('dbo', 'FacultyMembers', 'Title', 'Titles', 0),
		('dbo', 'FacultyMembers', 'Gender', 'Genders', 0),
		('dbo', 'FacultyMembers', 'FacultyType', 'FacultyTypes', 0),
		('dbo', 'FacultyMembers', 'Status', 'Statuses', 0),
		('dbo', 'FeeHeads', 'FeeHeadType', 'FeeHeadTypes', 0),
		('dbo', 'FeeHeads', 'RuleType', 'RuleTypes', 0),
		('dbo', 'FeeStructureDetails', 'SemesterNo', 'SemesterNos', 0),
		('dbo', 'FeeStructureDetails', 'Category', 'Categories', 0),
		('dbo', 'Files', 'FileType', 'FileTypes', 1),
		('dbo', 'GraduateDirectoryFormProjects', 'ProjectType', 'ProjectTypes', 0),
		('dbo', 'InstituteBankAccounts', 'FeeType', 'FeeTypes', 0),
		('dbo', 'InstituteBankAccounts', 'AccountType', 'AccountTypes', 1),
		('dbo', 'Institutes', 'Status', 'Statuses', 0),
		('dbo', 'IntegratedServiceUsers', 'ServiceType', 'ServiceTypes', 0),
		('dbo', 'IntegratedServiceUsers', 'Status', 'Statuses', 0),
		('dbo', 'OfferedCourseAttendances', 'ClassType', 'ClassTypes', 0),
		('dbo', 'OfferedCourseClasses', 'SemesterNo', 'SemesterNos', 0),
		('dbo', 'OfferedCourseClasses', 'Section', 'Sections', 0),
		('dbo', 'OfferedCourseClasses', 'Shift', 'Shifts', 0),
		('dbo', 'OfferedCourseExams', 'ExamType', 'ExamTypes', 0),
		('dbo', 'OfferedCourses', 'SemesterNo', 'SemesterNos', 0),
		('dbo', 'OfferedCourses', 'Section', 'Sections', 0),
		('dbo', 'OfferedCourses', 'Shift', 'Shifts', 0),
		('dbo', 'OfferedCourses', 'Status', 'Statuses', 0),
		('dbo', 'OfferedCourses', 'AssignmentsExamCompilationType', 'ExamCompilationTypes', 0),
		('dbo', 'OfferedCourses', 'QuizzesExamCompilationType', 'ExamCompilationTypes', 0),
		('dbo', 'PasswordChangeHistories', 'ChangeReason', 'PasswordChangeReasons', 0),
		('dbo', 'PgpSupervisors', 'SupervisorType', 'SupervisorTypes', 1),
		('dbo', 'PgpSupervisors', 'Status', 'Statuses', 0),
		('dbo', 'PgpThesis', 'Status', 'Statuses', 1),
		('dbo', 'PgpThesisSupervisors', 'ThesisSupervisorType', 'ThesisSupervisorTypes', 1),
		('dbo', 'PgpThesisTitles', 'Status', 'Statuses', 0),
		('dbo', 'Programs', 'Duration', 'ProgramDurations', 0),
		('dbo', 'Programs', 'DegreeLevel', 'DegreeLevels', 0),
		('dbo', 'Programs', 'ProgramType', 'ProgramTypes', 0),
		('dbo', 'Programs', 'Status', 'Statuses', 0),
		('dbo', 'QASurveyConducts', 'SurveyStatus', 'SurveyStatuses', 0),
		('dbo', 'QASurveyUsers', 'Status', 'Statuses', 0),
		('dbo', 'RecurringTasks', 'Status', 'Statuses', 0),
		('dbo', 'RegisteredCourses', 'Status', 'Statuses', 0),
		('dbo', 'RegisteredCourses', 'FreezedStatus', 'FreezedStatuses', 0),
		('dbo', 'RegisteredCourses', 'Grade', 'ExamGrades', 0),
		('dbo', 'ScholarshipStudents', 'SemesterNo', 'SemesterNos', 0),
		('dbo', 'ScholarshipWorkloads', 'SemesterNo', 'SemesterNos', 0),
		('dbo', 'Semesters', 'SemesterType', 'SemesterTypes', 0),
		('dbo', 'StudentAcademicRecords', 'DegreeType', 'DegreeTypes', 0),
		('dbo', 'StudentAcademicRecords', 'Status', 'Statuses', 0),
		('dbo', 'StudentAcademicRecords', 'VerificationStatus', 'VerificationStatuses', 1),
		('dbo', 'StudentCreditsTransferedCourses', 'EquivalentGrade', 'ExamGrades', 0),
		('dbo', 'StudentFeeChallans', 'InstallmentNo', 'InstallmentNos', 0),
		('dbo', 'StudentFeeChallans', 'Status', 'Statuses', 0),
		('dbo', 'StudentFeeChallans', 'DepositPlace', 'DepositPlaces', 0),
		('dbo', 'StudentFeeChallans', 'FeeUpdatedByUserType', 'FeeUpdatedByUserTypes', 0),
		('dbo', 'StudentFees', 'FeeType', 'FeeTypes', 0),
		('dbo', 'StudentFees', 'NoOfInstallments', 'InstallmentNos', 0),
		('dbo', 'StudentFees', 'Status', 'Statuses', 0),
		('dbo', 'StudentLibraryDefaulters', 'Status', 'Statuses', 0),
		('dbo', 'Students', 'Gender', 'Genders', 0),
		('dbo', 'Students', 'Category', 'Categories', 0),
		('dbo', 'Students', 'BloodGroup', 'BloodGroups', 0),
		('dbo', 'Students', 'AreaType', 'AreaTypes', 0),
		('dbo', 'Students', 'SponsoredBy', 'Sponsorships', 0),
		('dbo', 'Students', 'ETSType', 'ETSTypes', 0),
		('dbo', 'Students', 'AdmissionType', 'AdmissionTypes', 0),
		('dbo', 'Students', 'Status', 'Statuses', 0),
		('dbo', 'Students', 'DeferredToShift', 'Shifts', 0),
		('dbo', 'StudentSemesters', 'SemesterNo', 'SemesterNos', 0),
		('dbo', 'StudentSemesters', 'Section', 'Sections', 0),
		('dbo', 'StudentSemesters', 'Shift', 'Shifts', 0),
		('dbo', 'StudentSemesters', 'Status', 'Statuses', 0),
		('dbo', 'StudentSemesters', 'FreezedStatus', 'FreezedStatuses', 0),
		('dbo', 'StudentSemesters', 'ResultRemarks', 'ResultRemarksTypes', 0),
		('dbo', 'SurveyConducts', 'SurveyConductType', 'SurveyConductTypes', 0),
		('dbo', 'SurveyConducts', 'Status', 'Statuses', 0),
		('dbo', 'SurveyQuestions', 'QuestionType', 'QuestionTypes', 0),
		('dbo', 'Surveys', 'SurveyType', 'SurveyTypes', 0),
		('dbo', 'SystemFolders', 'FolderType', 'FolderTypes', 0),
		('dbo', 'ThesisInternships', 'RecordType', 'RecordTypes', 0),
		('dbo', 'ThesisInternships', 'Grade', 'ExamGrades', 0),
		('dbo', 'TimeBarredStudents', 'CourseCategoriesAllowed', 'CourseCategories', 1),
		('dbo', 'TransferredStudents', 'TransferType', 'TransferTypes', 0),
		('dbo', 'UserGroups', 'UserType', 'UserTypes', 0),
		('dbo', 'UserLoginHistory', 'UserType', 'UserTypes', 0),
		('dbo', 'UserLoginHistory', 'LoginStatus', 'LoginStatuses', 0),
		('dbo', 'UserLoginHistory', 'LoginFailureReason', 'LoginFailureReasons', 0),
		('dbo', 'UserLoginHistory', 'LogOffReason', 'LogOffReasons', 0),
		('dbo', 'UserRoleModules', 'Module', 'AspireModules', 0),
		('dbo', 'UserRoles', 'UserType', 'UserTypes', 0),
		('dbo', 'UserRoles', 'Status', 'Statuses', 0),
		('dbo', 'Users', 'Status', 'Statuses', 0),
		('dbo', 'WebExceptionEmailRecipients', 'Status', 'Statuses', 0);
 	DECLARE @TABLE_SCHEMA SYSNAME;
	DECLARE @TABLE_NAME SYSNAME;
	FETCH NEXT FROM CTABLES INTO @TABLE_SCHEMA, @TABLE_NAME;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		--PRINT '--' + @TABLE_NAME;
		DECLARE @SCRIPT VARCHAR(MAX) = 'CREATE OR ALTER VIEW [views].' + QUOTENAME(@TABLE_NAME) + ' AS SELECT ';
		DECLARE CCOLUMNS CURSOR FOR SELECT COLUMN_NAME, DATA_TYPE, DOMAIN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @TABLE_SCHEMA AND TABLE_NAME = @TABLE_NAME ORDER BY ORDINAL_POSITION;
		DECLARE @COLUMN_NAME SYSNAME;
		DECLARE @DOMAIN_NAME SYSNAME;
		DECLARE @DATA_TYPE SYSNAME;
		OPEN CCOLUMNS;
		FETCH NEXT FROM CCOLUMNS INTO @COLUMN_NAME, @DATA_TYPE, @DOMAIN_NAME;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			DECLARE @CSCRIPT VARCHAR(MAX) = NULL;
			SET @SCRIPT = @SCRIPT + QUOTENAME(@COLUMN_NAME) + ', ';
			IF @DATA_TYPE = 'bit'
				SET @CSCRIPT = '[views].[ToYesNo]('+QUOTENAME(@COLUMN_NAME)+') AS ' + QUOTENAME(@COLUMN_NAME + 'YesNo') + ', ';
			ELSE IF @DOMAIN_NAME IS NOT NULL
			BEGIN
				IF @DOMAIN_NAME = 'SemesterID'
					SET @CSCRIPT = '[views].[ToSemester]('+QUOTENAME(@COLUMN_NAME)+') AS ' + QUOTENAME(REPLACE(@COLUMN_NAME,'SemesterID','Semester')) + ', ';
				ELSE IF EXISTS(SELECT 1 FROM @EXCLUDED_DATA_TYPES WHERE DATA_TYPE = @DOMAIN_NAME)
					SET @CSCRIPT = NULL;
				ELSE
				BEGIN
					DECLARE @ENTITY_NAME SYSNAME = (SELECT EntityName FROM [dbo].[TableNames] WHERE TableSchema = @TABLE_SCHEMA AND TableName = @TABLE_NAME);
					DECLARE @IS_ENUM_FLAG BIT = NULL;
					DECLARE @ENUM_TYPE SYSNAME = NULL;
					SELECT @ENUM_TYPE = ENUM_TYPE, @IS_ENUM_FLAG = IS_ENUM_FLAG FROM @COLUMN_MAPPINGS WHERE TABLE_SCHEMA = @TABLE_SCHEMA AND TABLE_NAME = @TABLE_NAME AND COLUMN_NAME = @COLUMN_NAME;
					IF @ENUM_TYPE IS NOT NULL AND @IS_ENUM_FLAG IS NOT NULL
					BEGIN
						SET @CSCRIPT = '[views].[GetEnumDescription](''' + @ENTITY_NAME + ''', ''' + @ENUM_TYPE + ''', ' + @COLUMN_NAME + ',' + CONVERT(CHAR(1), @IS_ENUM_FLAG) + ') AS ' + QUOTENAME(@COLUMN_NAME + 'FullName') + ', ';
					END
					ELSE
						PRINT '		(''' + @TABLE_SCHEMA +''', ''' + @TABLE_NAME + ''', ''' + @COLUMN_NAME + ''', ''' + @DOMAIN_NAME + '_Missing'', 0),';
				END
			END
			IF @CSCRIPT IS NOT NULL
				SET @SCRIPT = @SCRIPT + @CSCRIPT;
			FETCH NEXT FROM CCOLUMNS INTO @COLUMN_NAME, @DATA_TYPE, @DOMAIN_NAME;
		END
		SET @SCRIPT = SUBSTRING(@SCRIPT, 1 , LEN(@SCRIPT) -1) + ' FROM ' + QUOTENAME(@TABLE_SCHEMA) + '.' + QUOTENAME(@TABLE_NAME) + ';';
		CLOSE CCOLUMNS;
		DEALLOCATE CCOLUMNS;
		--PRINT 'EXECUTE(''' + REPLACE(@SCRIPT, '''', '''''') + ''')';
		PRINT 'SELECT TOP (1000) * FROM views.' + @TABLE_NAME + ';';
		EXECUTE(@SCRIPT);
		FETCH NEXT FROM CTABLES INTO @TABLE_SCHEMA, @TABLE_NAME;
	END
	CLOSE CTABLES;
	DEALLOCATE CTABLES;
END