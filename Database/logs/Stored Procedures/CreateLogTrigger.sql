﻿CREATE PROCEDURE [logs].[CreateLogTrigger]
	@TABLE_SCHEMA SYSNAME,
	@TABLE_NAME SYSNAME
AS
BEGIN
	DECLARE @TRIGGER_NAME SYSNAME = QUOTENAME(@TABLE_SCHEMA) + '.' + QUOTENAME('Trg' + @TABLE_NAME + 'Audit');
	DECLARE @SQL NVARCHAR(MAX);
	DECLARE @LOG_SCHEMA NVARCHAR(MAX) = 'logs';
	DECLARE @TAB SYSNAME = QUOTENAME(@TABLE_SCHEMA) + '.' + QUOTENAME(@TABLE_NAME);
	DECLARE @LOG_TAB SYSNAME = QUOTENAME(@LOG_SCHEMA) + '.' + QUOTENAME(@TABLE_NAME);
	DECLARE @InsertSQL NVARCHAR(MAX) = N'INSERT INTO ' + @LOG_TAB + '([UserLoginHistoryID$], [TransactionDate], [TransactionType]';
	DECLARE @UpdateSQL NVARCHAR(MAX) = N'INSERT INTO ' + @LOG_TAB + '([UserLoginHistoryID$], [TransactionDate], [TransactionType]';
	DECLARE @DeleteSQL NVARCHAR(MAX) = N'INSERT INTO ' + @LOG_TAB + '([UserLoginHistoryID$], [TransactionDate], [TransactionType]';
	DECLARE @InsertSelectSQL NVARCHAR(MAX) = N'SELECT @UserLoginHistoryID, @TransactionDate, @TransactionType';
	DECLARE @UpdateSelectSQL NVARCHAR(MAX) = N'SELECT @UserLoginHistoryID, @TransactionDate, @TransactionType';
	DECLARE @DeleteSelectSQL NVARCHAR(MAX) = N'SELECT @UserLoginHistoryID, @TransactionDate, @TransactionType';

	DECLARE COLUMNS_CURSOR CURSOR FOR SELECT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @TABLE_SCHEMA AND TABLE_NAME = @TABLE_NAME ORDER BY ORDINAL_POSITION;
	OPEN COLUMNS_CURSOR;
	DECLARE @COLUMN_NAME SYSNAME;
	DECLARE @DATA_TYPE SYSNAME;
	FETCH NEXT FROM COLUMNS_CURSOR INTO @COLUMN_NAME, @DATA_TYPE;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @InsertSQL = @InsertSQL + ', ' + QUOTENAME(@COLUMN_NAME);
		SET @UpdateSQL = @UpdateSQL + ', ' + QUOTENAME(@COLUMN_NAME);
		SET @DeleteSQL = @DeleteSQL + ', ' + QUOTENAME(@COLUMN_NAME);
		SET @InsertSelectSQL = @InsertSelectSQL + ', ' + QUOTENAME(@COLUMN_NAME);
		SET @UpdateSelectSQL = @UpdateSelectSQL + ', ' + QUOTENAME(@COLUMN_NAME);
		SET @DeleteSelectSQL = @DeleteSelectSQL + ', ' + QUOTENAME(@COLUMN_NAME);
		FETCH NEXT FROM COLUMNS_CURSOR INTO @COLUMN_NAME, @DATA_TYPE;
	END
	CLOSE COLUMNS_CURSOR;

	SET @InsertSQL = @InsertSQL + ')' + CHAR(10) + '		'+ @InsertSelectSQL + ' FROM INSERTED;';
	SET @UpdateSQL = @UpdateSQL + ')' + CHAR(10) + '		'+ @UpdateSelectSQL + ' FROM INSERTED;';
	SET @DeleteSQL = @DeleteSQL + ')' + CHAR(10) + '		'+ @DeleteSelectSQL + ' FROM DELETED;';

	SET @SQL = 'IF OBJECT_ID(''' + @TRIGGER_NAME + ''', ''TR'') IS NOT NULL DROP TRIGGER ' + @TRIGGER_NAME + ';';
	PRINT (@SQL);
	EXEC(@SQL);

	SET @SQL = 'CREATE TRIGGER ' + @TRIGGER_NAME + ' ON ' + @TAB + ' FOR INSERT,DELETE,UPDATE
AS 
DECLARE
	@INSERT CHAR = ''I'',
	@UPDATE CHAR = ''U'',
	@DELETE CHAR = ''D'',
	@TransactionType CHAR,
	@TransactionDate DATETIME = GETDATE(),
	@UserLoginHistoryID INT = CAST(CONVERT(VARBINARY(4), CONTEXT_INFO()) AS INT)
BEGIN
	IF EXISTS(SELECT 1 FROM INSERTED)
	BEGIN
		IF EXISTS(SELECT 1 FROM DELETED) 
			SET @TransactionType = @UPDATE;
		ELSE 
			SET @TransactionType = @INSERT;
	END
	ELSE SET @TransactionType = @DELETE;

	IF @TransactionType = @INSERT
		' + @InsertSQL + '
	ELSE IF @TransactionType = @UPDATE
		' + @UpdateSQL + '
	ELSE IF @TransactionType = @DELETE
		' + @DeleteSQL + '
';

	SET @SQL = @SQL + 'END';
	PRINT @SQL
	EXEC (@SQL);
END