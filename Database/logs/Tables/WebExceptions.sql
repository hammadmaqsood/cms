﻿CREATE TABLE [logs].[WebExceptions] (
    [WebExceptionID]     INT            IDENTITY (1, 1) NOT NULL,
    [UserLoginHistoryID] INT            NULL,
    [PageURL]            VARCHAR (MAX)  NOT NULL,
    [ExceptionMessage]   VARCHAR (MAX)  NULL,
    [YSOD]               VARCHAR (MAX)  NULL,
    [ExceptionDate]      DATETIME       NOT NULL,
    [ErrorCode]          INT            NOT NULL,
    [IPAddress]          VARCHAR (50)   NULL,
    [UserAgent]          VARCHAR (1024) NULL,
    CONSTRAINT [PK_WebExceptions] PRIMARY KEY CLUSTERED ([WebExceptionID] DESC),
    CONSTRAINT [FK_WebExceptions_UserLoginHistory_UserLoginHistoryID] FOREIGN KEY ([UserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID])
);

