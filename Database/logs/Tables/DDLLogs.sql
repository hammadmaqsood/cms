﻿CREATE TABLE [logs].[DDLLogs] (
    [DDLLogID]      INT                IDENTITY (1, 1) NOT NULL,
    [EventData]     XML                NOT NULL,
    CONSTRAINT [PK_DDLLogs] PRIMARY KEY CLUSTERED ([DDLLogID] DESC)
);





