﻿CREATE TABLE [logs].[UsageHistory] (
    [UsageHistoryID]     BIGINT           IDENTITY (1, 1) NOT NULL,
    [UserLoginHistoryID] INT              NULL,
    [PageGUID]           UNIQUEIDENTIFIER NOT NULL,
    [IsPostback]         BIT              NOT NULL,
    [StartDate]          DATETIME         NOT NULL,
    [EndDate]            DATETIME         NOT NULL,
    CONSTRAINT [PK_UsageHistory] PRIMARY KEY CLUSTERED ([UsageHistoryID] ASC),
    CONSTRAINT [FK_UsageHistory_UserLoginHistory_UserLoginHistoryID] FOREIGN KEY ([UserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID])
);





