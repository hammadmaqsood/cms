﻿CREATE TABLE [logs].[EmailAttachments] (
    [EmailAttachmentID] UNIQUEIDENTIFIER NOT NULL,
    [EmailID]           UNIQUEIDENTIFIER NOT NULL,
    [FileName]          VARCHAR (MAX)    NOT NULL,
    [FileContents]      VARBINARY (MAX)  NOT NULL,
    CONSTRAINT [PK_EmailAttachments] PRIMARY KEY CLUSTERED ([EmailAttachmentID] ASC),
    CONSTRAINT [FK_EmailAttachments_Emails_EmailID] FOREIGN KEY ([EmailID]) REFERENCES [logs].[Emails] ([EmailID])
);





