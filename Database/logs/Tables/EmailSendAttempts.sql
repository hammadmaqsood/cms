﻿CREATE TABLE [logs].[EmailSendAttempts] (
    [EmailSendAttemptID] UNIQUEIDENTIFIER NOT NULL,
    [EmailID]            UNIQUEIDENTIFIER NOT NULL,
    [SMTPServerID]       UNIQUEIDENTIFIER NULL,
    [Status]             [dbo].[EnumGuid] NOT NULL,
    [AttemptDate]        DATETIME         NOT NULL,
    [ErrorMessage]       VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_EmailSendAttempts] PRIMARY KEY CLUSTERED ([EmailSendAttemptID] ASC),
    CONSTRAINT [FK_EmailSendAttempts_Emails_EmailID] FOREIGN KEY ([EmailID]) REFERENCES [logs].[Emails] ([EmailID]),
    CONSTRAINT [FK_EmailSendAttempts_SMTPServers_SMTPServerID] FOREIGN KEY ([SMTPServerID]) REFERENCES [logs].[SMTPServers] ([SMTPServerID])
);











