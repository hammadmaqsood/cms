﻿CREATE TABLE [logs].[DMLLogDetails] (
    [DMLLogDetailID] INT           IDENTITY (1, 1) NOT NULL,
    [DMLLogID]       INT           NOT NULL,
    [ColumnName]     VARCHAR (128) NOT NULL,
    [OldValue]       VARCHAR (MAX) NULL,
    [NewValue]       VARCHAR (MAX) NULL,
    CONSTRAINT [PK_DMLLogDetails] PRIMARY KEY CLUSTERED ([DMLLogDetailID] ASC),
    CONSTRAINT [FK_DMLLogDetails_DMLLogs_DMLLogID] FOREIGN KEY ([DMLLogID]) REFERENCES [logs].[DMLLogs] ([DMLLogID])
);





