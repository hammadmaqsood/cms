﻿CREATE TABLE [logs].[StudentFeeDefaulterEmails] (
    [StudentFeeDefaulterEmailID] UNIQUEIDENTIFIER NOT NULL,
    [StudentID]                  INT              NOT NULL,
    [EmailSentDate]              DATE             NOT NULL,
    [EmailMd5Checksum]           CHAR (32)        NOT NULL,
    CONSTRAINT [PK_StudentFeeDefaulterEmails] PRIMARY KEY CLUSTERED ([StudentFeeDefaulterEmailID] ASC),
    CONSTRAINT [FK_StudentFeeDefaulterEmails_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [UK_StudentFeeDefaulterEmails_StudentID_EmailSentDate_EmailMd5Checksum] UNIQUE NONCLUSTERED ([StudentID] ASC, [EmailSentDate] ASC, [EmailMd5Checksum] ASC)
);

