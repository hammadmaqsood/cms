﻿CREATE TABLE [logs].[FeePaymentLogs] (
    [FeePaymentLogID]    INT            IDENTITY (1, 1) NOT NULL,
    [UserLoginHistoryID] INT            NOT NULL,
    [AttemptDate]        DATETIME       NOT NULL,
    [ChallanNo]          DECIMAL (12)   NOT NULL,
    [Amount]             INT            NOT NULL,
    [AccountNo]          VARCHAR (1000) NULL,
    [AccountTitle]       VARCHAR (1000) NULL,
    [BranchCode]         SMALLINT       NOT NULL,
    [BranchName]         VARCHAR (1000) NULL,
    [BranchAddress]      VARCHAR (1000) NULL,
    [Response]           VARCHAR (1000) NULL,
    [Success]            BIT            NOT NULL,
    CONSTRAINT [PK_FeePaymentLogs] PRIMARY KEY CLUSTERED ([FeePaymentLogID] DESC),
    CONSTRAINT [FK_FeePaymentLogs_UserLoginHistory_UserLoginHistoryID] FOREIGN KEY ([UserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID])
);





