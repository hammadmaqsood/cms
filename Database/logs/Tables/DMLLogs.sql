﻿CREATE TABLE [logs].[DMLLogs] (
    [DMLLogID]           INT              IDENTITY (1, 1) NOT NULL,
    [UserLoginHistoryID] INT              NOT NULL,
    [TransactionType]    CHAR (1)         NOT NULL,
    [TableName]          VARCHAR (128)    NOT NULL,
    [PrimaryKeyInt]      INT              NULL,
    [TransactionDate]    DATETIME         NOT NULL,
    [ActionType]         SMALLINT         NULL,
    [PrimaryKeyGuid]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_DMLLogs] PRIMARY KEY CLUSTERED ([DMLLogID] DESC),
    CONSTRAINT [CK_DMLLogs ([PrimaryKeyInt]] IS NOT NULL AND [PrimaryKeyGuid]] IS NULL OR [PrimaryKeyInt]] IS NULL AND [PrimaryKeyGuid]] IS NOT NULL] CHECK ([PrimaryKeyInt] IS NOT NULL AND [PrimaryKeyGuid] IS NULL OR [PrimaryKeyInt] IS NULL AND [PrimaryKeyGuid] IS NOT NULL)
);







