﻿CREATE TABLE [logs].[SMTPServers] (
    [SMTPServerID]      [dbo].[EnumGuid]  NOT NULL,
    [FromEmailAddress]  [dbo].[Email]     NOT NULL,
    [Password]          [dbo].[Encrypted] NOT NULL,
    [SMTPServerAddress] VARCHAR (MAX)     NOT NULL,
    [Port]              INT               NOT NULL,
    [EnableSSL]         BIT               NOT NULL,
    [Status]            [dbo].[EnumGuid]  NOT NULL,
    [CC]                VARCHAR (MAX)     NULL,
    [BCC]               VARCHAR (MAX)     NULL,
    [ReplyTo]           VARCHAR (MAX)     NULL,
    [BlockedUpTill]     DATETIME          NULL,
    CONSTRAINT [PK_SMTPServers] PRIMARY KEY CLUSTERED ([SMTPServerID] ASC)
);



