﻿CREATE TABLE [logs].[MarksSheetChanges] (
    [MarksSheetChangesID]      UNIQUEIDENTIFIER NOT NULL,
    [LoginSessionGuid]         UNIQUEIDENTIFIER NOT NULL,
    [OfferedCourseID]          INT              NOT NULL,
    [ActionType]               [dbo].[EnumGuid] NOT NULL,
    [ActionDate]               DATETIME         NOT NULL,
    [OldMarksSheetJSON]        VARCHAR (MAX)    NOT NULL,
    [NewMarksSheetJSON]        VARCHAR (MAX)    NOT NULL,
    [ManagementEmailRecipient] [dbo].[EnumGuid] NULL,
    [FacultyMemberEmailID]     UNIQUEIDENTIFIER NULL,
    [ManagementEmailID]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MarksSheetChanges] PRIMARY KEY CLUSTERED ([MarksSheetChangesID] ASC),
    CONSTRAINT [FK_MarksSheetChanges_Emails_FacultyMemberEmailID] FOREIGN KEY ([FacultyMemberEmailID]) REFERENCES [logs].[Emails] ([EmailID]),
    CONSTRAINT [FK_MarksSheetChanges_Emails_ManagementEmailID] FOREIGN KEY ([ManagementEmailID]) REFERENCES [logs].[Emails] ([EmailID])
);

