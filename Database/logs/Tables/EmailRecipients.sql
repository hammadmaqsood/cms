﻿CREATE TABLE [logs].[EmailRecipients] (
    [EmailRecipientID] UNIQUEIDENTIFIER NOT NULL,
    [EmailID]          UNIQUEIDENTIFIER NOT NULL,
    [DisplayName]      VARCHAR (MAX)    NULL,
    [EmailAddress]     VARCHAR (MAX)    NOT NULL,
    [RecipientType]    [dbo].[EnumGuid] NOT NULL,
    CONSTRAINT [PK_EmailRecipients] PRIMARY KEY CLUSTERED ([EmailRecipientID] ASC),
    CONSTRAINT [FK_EmailRecipients_Emails_EmailID] FOREIGN KEY ([EmailID]) REFERENCES [logs].[Emails] ([EmailID])
);



