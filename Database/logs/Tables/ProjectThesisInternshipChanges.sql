﻿CREATE TABLE [logs].[ProjectThesisInternshipChanges] (
    [ProjectThesisInternshipChangeID] UNIQUEIDENTIFIER NOT NULL,
    [LoginSessionGuid]                UNIQUEIDENTIFIER NOT NULL,
    [StudentID]                       INT              NOT NULL,
    [ProjectThesisInternshipID]       UNIQUEIDENTIFIER NOT NULL,
    [ActionType]                      [dbo].[EnumGuid] NOT NULL,
    [ActionDate]                      DATETIME         NOT NULL,
    [OldJSON]                         VARCHAR (MAX)    NULL,
    [NewJSON]                         VARCHAR (MAX)    NULL,
    [ManagementEmailRecipient]        [dbo].[EnumGuid] NULL,
    [ManagementEmailID]               UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ProjectThesisInternshipChanges] PRIMARY KEY CLUSTERED ([ProjectThesisInternshipChangeID] ASC),
    CONSTRAINT [FK_ProjectThesisInternshipChanges_Emails_ManagementEmailID] FOREIGN KEY ([ManagementEmailID]) REFERENCES [logs].[Emails] ([EmailID])
);

