﻿CREATE TABLE [logs].[Emails] (
    [EmailID]                     UNIQUEIDENTIFIER NOT NULL,
    [CreatedByUserLoginHistoryID] INT              NOT NULL,
    [EmailType]                   [dbo].[EnumGuid] NOT NULL,
    [Priority]                    TINYINT          NOT NULL,
    [FromDisplayName]             VARCHAR (MAX)    NOT NULL,
    [Subject]                     VARCHAR (MAX)    NOT NULL,
    [Body]                        VARCHAR (MAX)    NOT NULL,
    [IsBodyHtml]                  BIT              NOT NULL,
    [CreatedDate]                 DATETIME         NOT NULL,
    [ExpiryDate]                  DATETIME         NULL,
    [AttemptsCount]               INT              NOT NULL,
    [Status]                      [dbo].[EnumGuid] NOT NULL,
    CONSTRAINT [PK_Emails] PRIMARY KEY CLUSTERED ([EmailID] ASC),
    CONSTRAINT [FK_Emails_UserLoginHistory_CreatedByUserLoginHistoryID] FOREIGN KEY ([CreatedByUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID])
);







