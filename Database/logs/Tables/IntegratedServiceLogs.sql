﻿CREATE TABLE [logs].[IntegratedServiceLogs] (
    [IntegratedServiceLogID] BIGINT        IDENTITY (1, 1) NOT NULL,
    [UserLoginHistoryID]     INT           NOT NULL,
    [Service]                VARCHAR (100) NOT NULL,
    [MethodName]             VARCHAR (100) NOT NULL,
    [AttemptDate]            DATETIME      NOT NULL,
    [Parameters]             VARCHAR (MAX) NOT NULL,
    [Response]               VARCHAR (MAX) NULL,
    [Success]                BIT           NOT NULL,
    CONSTRAINT [PK_IntegratedServiceLogs] PRIMARY KEY CLUSTERED ([IntegratedServiceLogID] DESC),
    CONSTRAINT [FK_IntegratedServiceLogs_UserLoginHistory_UserLoginHistoryID] FOREIGN KEY ([UserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID])
);

