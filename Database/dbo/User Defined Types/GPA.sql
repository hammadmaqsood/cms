﻿CREATE TYPE [dbo].[GPA]
    FROM DECIMAL (18, 17) NOT NULL;

