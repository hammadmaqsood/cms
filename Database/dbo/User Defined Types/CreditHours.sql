﻿CREATE TYPE [dbo].[CreditHours]
    FROM DECIMAL (9, 2) NOT NULL;

