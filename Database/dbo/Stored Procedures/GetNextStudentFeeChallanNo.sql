﻿CREATE PROCEDURE [dbo].[GetNextStudentFeeChallanNo]
AS
BEGIN
	SELECT NEXT VALUE FOR [dbo].[StudentFeeChallanNo];
END