﻿CREATE PROCEDURE [dbo].[GetNextAdmissionProcessingFeeChallanNo]
AS
BEGIN
	SELECT NEXT VALUE FOR [dbo].[AdmissionProcessingFeeChallanNo];
END