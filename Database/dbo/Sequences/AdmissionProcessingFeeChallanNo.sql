﻿CREATE SEQUENCE [dbo].[AdmissionProcessingFeeChallanNo]
    AS INT
    START WITH 150746
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 999999999
    NO CACHE;

