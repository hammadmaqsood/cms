﻿CREATE VIEW [dbo].[ViewActiveExecutiveUserLoginGroupPermissions] WITH SCHEMABINDING AS
SELECT
	[ViewActiveExecutiveUserLogins].[UserLoginHistoryID],	[ViewActiveExecutiveUserLogins].[LoginSessionGuid],	[ViewActiveExecutiveUserLogins].[UserType],	[ViewActiveExecutiveUserLogins].[UserID],
	[ViewActiveExecutiveUserLogins].[UserRoleID],	[ViewActiveExecutiveUserLogins].[InstituteID],	[ViewActiveExecutiveUserLogins].[UserGroupID],	[UserGroupPermissions].[Module],	[UserGroupPermissions].[PermissionType],	[UserGroupPermissions].[PermissionValue]
FROM [dbo].[ViewActiveExecutiveUserLogins]
	INNER JOIN [dbo].[UserGroupPermissions] ON [ViewActiveExecutiveUserLogins].[UserGroupID] = [UserGroupPermissions].[UserGroupID]
GO