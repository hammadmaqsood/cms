﻿CREATE VIEW [dbo].[ViewActiveStudentUserLogins] WITH SCHEMABINDING AS
SELECT 
	[ViewActiveUserLoginHistory].[UserLoginHistoryID],	[ViewActiveUserLoginHistory].[LoginSessionGuid],	[ViewActiveUserLoginHistory].[UserType],	[ViewActiveUserLoginHistory].[SubUserType],	[ViewActiveUserLoginHistory].[UserID],
	[Students].[StudentID],	[Students].[Enrollment]
FROM [dbo].[Students]
	INNER JOIN [dbo].[ViewActiveUserLoginHistory] ON [Students].[StudentID] = [ViewActiveUserLoginHistory].[StudentID];
GO