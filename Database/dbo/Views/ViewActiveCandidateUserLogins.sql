﻿CREATE VIEW dbo.ViewActiveCandidateUserLogins WITH SCHEMABINDING AS
SELECT
	dbo.ViewActiveUserLoginHistory.UserLoginHistoryID,
	dbo.ViewActiveUserLoginHistory.LoginSessionGuid,
	dbo.ViewActiveUserLoginHistory.CandidateID,
	dbo.Candidates.SemesterID,
	dbo.Candidates.ForeignStudent
FROM dbo.ViewActiveUserLoginHistory
	INNER JOIN dbo.Candidates ON dbo.ViewActiveUserLoginHistory.CandidateID = dbo.Candidates.CandidateID
GO