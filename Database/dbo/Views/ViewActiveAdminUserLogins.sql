﻿CREATE VIEW [dbo].[ViewActiveAdminUserLogins] WITH SCHEMABINDING AS
SELECT
	[ViewActiveUserRoleLoginHistory].[UserLoginHistoryID],	[ViewActiveUserRoleLoginHistory].[LoginSessionGuid],	[ViewActiveUserRoleLoginHistory].[UserType],	[ViewActiveUserRoleLoginHistory].[UserID],
	[ViewActiveUserRoleLoginHistory].[UserRoleID],	[ViewActiveUserRoleLoginHistory].[UserGroupID]
FROM [dbo].[ViewActiveUserRoleLoginHistory]
WHERE [ViewActiveUserRoleLoginHistory].[UserType] IN (4,8)
GO