﻿CREATE VIEW [dbo].[ViewActiveStaffUserLoginGroupPermissionsModules] WITH SCHEMABINDING AS
SELECT [ViewActiveStaffUserLoginGroupPermissions].[UserLoginHistoryID],[ViewActiveStaffUserLoginGroupPermissions].[LoginSessionGuid],[ViewActiveStaffUserLoginGroupPermissions].[UserType],
[ViewActiveStaffUserLoginGroupPermissions].[UserID],[ViewActiveStaffUserLoginGroupPermissions].[UserRoleID],[ViewActiveStaffUserLoginGroupPermissions].[InstituteID],
[ViewActiveStaffUserLoginGroupPermissions].[UserGroupID],[ViewActiveStaffUserLoginGroupPermissions].[Module],[ViewActiveStaffUserLoginGroupPermissions].[PermissionType],
[ViewActiveStaffUserLoginGroupPermissions].[PermissionValue],[UserRoleModules].[UserRoleModuleID],[UserRoleModules].[DepartmentID],[UserRoleModules].[ProgramID],[UserRoleModules].[AdmissionOpenProgramID]
FROM [dbo].[ViewActiveStaffUserLoginGroupPermissions]
	INNER JOIN [dbo].[UserRoleModules] ON [ViewActiveStaffUserLoginGroupPermissions].[Module] = [UserRoleModules].[Module] AND [ViewActiveStaffUserLoginGroupPermissions].[UserRoleID] = [UserRoleModules].[UserRoleID]
GO
