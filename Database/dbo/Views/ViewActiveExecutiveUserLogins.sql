﻿CREATE VIEW [dbo].[ViewActiveExecutiveUserLogins] WITH SCHEMABINDING AS
SELECT 
	[ViewActiveUserRoleLoginHistory].[UserLoginHistoryID],	[ViewActiveUserRoleLoginHistory].[LoginSessionGuid],	[ViewActiveUserRoleLoginHistory].[UserType],	[ViewActiveUserRoleLoginHistory].[UserID],
	[ViewActiveUserRoleLoginHistory].[UserRoleID],	[ViewActiveUserRoleLoginHistory].[InstituteID],	[ViewActiveUserRoleLoginHistory].[UserGroupID]
FROM [dbo].[ViewActiveUserRoleLoginHistory]
WHERE [ViewActiveUserRoleLoginHistory].[UserType] = 16
GO