﻿CREATE VIEW [dbo].[ViewActiveUserRoleLoginHistory] WITH SCHEMABINDING AS
SELECT
	[ViewActiveUserLoginHistory].[UserLoginHistoryID],	[ViewActiveUserLoginHistory].[LoginSessionGuid],	[ViewActiveUserLoginHistory].[UserType],	[ViewActiveUserLoginHistory].[UserID],
	[ViewActiveUserLoginHistory].[UserRoleID],	[UserRoles].[InstituteID],
	[UserRoles].[UserGroupID]
FROM [dbo].[Users]
	INNER JOIN [dbo].[ViewActiveUserLoginHistory] ON Users.UserID = ViewActiveUserLoginHistory.UserID
	INNER JOIN [dbo].[UserRoles] ON Users.UserID = UserRoles.UserID AND ViewActiveUserLoginHistory.UserRoleID = UserRoles.UserRoleID
WHERE ([Users].[Status] = 1)
	AND ([Users].[IsDeleted] = 0)
	AND ([UserRoles].[Status] = 1)
	AND ([UserRoles].[IsDeleted] = 0)
	AND (CONVERT(DATE,[UserRoles].[ExpiryDate] ) > CONVERT(DATE,GETDATE()))
GO
