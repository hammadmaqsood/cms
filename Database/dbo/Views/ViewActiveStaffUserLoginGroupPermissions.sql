﻿CREATE VIEW [dbo].[ViewActiveStaffUserLoginGroupPermissions] WITH SCHEMABINDING AS
SELECT
	[ViewActiveStaffUserLogins].[UserLoginHistoryID],	[ViewActiveStaffUserLogins].[LoginSessionGuid],	[ViewActiveStaffUserLogins].[UserType],	[ViewActiveStaffUserLogins].[UserID],
	[ViewActiveStaffUserLogins].[UserRoleID],	[ViewActiveStaffUserLogins].[InstituteID],	[ViewActiveStaffUserLogins].[UserGroupID],	[UserGroupPermissions].[Module],	[UserGroupPermissions].[PermissionType],	[UserGroupPermissions].[PermissionValue]
FROM [dbo].[ViewActiveStaffUserLogins]
	INNER JOIN [dbo].[UserGroupPermissions] ON [ViewActiveStaffUserLogins].[UserGroupID] = [UserGroupPermissions].[UserGroupID]
GO