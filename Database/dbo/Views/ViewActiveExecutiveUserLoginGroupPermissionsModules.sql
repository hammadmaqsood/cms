﻿CREATE VIEW [dbo].[ViewActiveExecutiveUserLoginGroupPermissionsModules] WITH SCHEMABINDING AS
SELECT [ViewActiveExecutiveUserLoginGroupPermissions].[UserLoginHistoryID],[ViewActiveExecutiveUserLoginGroupPermissions].[LoginSessionGuid],[ViewActiveExecutiveUserLoginGroupPermissions].[UserType],
[ViewActiveExecutiveUserLoginGroupPermissions].[UserID],[ViewActiveExecutiveUserLoginGroupPermissions].[UserRoleID],[ViewActiveExecutiveUserLoginGroupPermissions].[InstituteID],
[ViewActiveExecutiveUserLoginGroupPermissions].[UserGroupID],[ViewActiveExecutiveUserLoginGroupPermissions].[Module],[ViewActiveExecutiveUserLoginGroupPermissions].[PermissionType],
[ViewActiveExecutiveUserLoginGroupPermissions].[PermissionValue],[UserRoleModules].[UserRoleModuleID],[UserRoleModules].[DepartmentID],[UserRoleModules].[ProgramID],[UserRoleModules].[AdmissionOpenProgramID]
FROM [dbo].[ViewActiveExecutiveUserLoginGroupPermissions]
	INNER JOIN [dbo].[UserRoleModules] ON [ViewActiveExecutiveUserLoginGroupPermissions].[Module] = [UserRoleModules].[Module] AND [ViewActiveExecutiveUserLoginGroupPermissions].[UserRoleID] = [UserRoleModules].[UserRoleID]
GO
