﻿CREATE VIEW [dbo].[ViewActiveUserLoginHistory] WITH SCHEMABINDING AS
SELECT 
	[UserLoginHistoryID],
	[LoginSessionGuid],
	[UserType],	[SubUserType],	[UserID],	[UserRoleID],	[CandidateID],	[StudentID],	[FacultyMemberID]FROM [dbo].[UserLoginHistory]
WHERE ([LoginStatus] = 1) AND ([LogOffDate] IS NULL)
GO
