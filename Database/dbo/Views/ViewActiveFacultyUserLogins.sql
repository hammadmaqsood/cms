﻿CREATE VIEW [dbo].[ViewActiveFacultyUserLogins] WITH SCHEMABINDING AS
SELECT 
	[ViewActiveUserLoginHistory].[UserLoginHistoryID],	[ViewActiveUserLoginHistory].[LoginSessionGuid],	[ViewActiveUserLoginHistory].[UserType],	[ViewActiveUserLoginHistory].[UserID],
	[ViewActiveUserLoginHistory].[FacultyMemberID],	[FacultyMembers].[InstituteID]FROM [dbo].[FacultyMembers]
	INNER JOIN [dbo].[ViewActiveUserLoginHistory] ON [FacultyMembers].[FacultyMemberID] = [ViewActiveUserLoginHistory].[FacultyMemberID];
GO