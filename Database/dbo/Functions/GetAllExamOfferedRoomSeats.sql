﻿CREATE FUNCTION [dbo].[GetAllExamOfferedRoomSeats](@ExamConductID INT, @RoomUsageType TINYINT)
RETURNS 
@TABLE TABLE 
(
	ExamOfferedRoomID INT NOT NULL,
	[Row] TINYINT NOT NULL,
	[Column] TINYINT NOT NULL
)
AS
BEGIN
	DECLARE @ExamOfferedRoomID INT;
	DECLARE @Rows INT;
	DECLARE @Columns INT;
	DECLARE ExamOfferedRoomsCursor CURSOR FOR 
		SELECT [ExamOfferedRoomID], [Rows], [Columns]
		FROM ExamOfferedRooms
		WHERE ExamConductID = @ExamConductID AND RoomUsageType = @RoomUsageType
		ORDER BY [Sequence];

	OPEN ExamOfferedRoomsCursor;

	FETCH NEXT FROM ExamOfferedRoomsCursor INTO @ExamOfferedRoomID, @Rows, @Columns;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		DECLARE @Column INT = 1;
		WHILE @Column <= @Columns
		BEGIN
			DECLARE @Row INT = 1;
			WHILE @Row <= @Rows
			BEGIN
				INSERT INTO @TABLE VALUES(@ExamOfferedRoomID, @Row, @Column);
			SET @Row = @Row + 1;
			END
			SET @Column = @Column + 1;
		END
		FETCH NEXT FROM ExamOfferedRoomsCursor INTO @ExamOfferedRoomID, @Rows, @Columns;
	END 
	CLOSE ExamOfferedRoomsCursor;
	DEALLOCATE ExamOfferedRoomsCursor;
	--DELETE FROM @TABLE FROM @TABLE T INNER JOIN [dbo].[ExamSeats] ES ON T.[ExamOfferedRoomID] = ES.[ExamOfferedRoomID];
	RETURN;
END