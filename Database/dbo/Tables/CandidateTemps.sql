﻿CREATE TABLE [dbo].[CandidateTemps] (
    [CandidateTempID]   INT                IDENTITY (1, 1) NOT NULL,
    [SemesterID]        SMALLINT           NOT NULL,
    [Name]              [dbo].[PersonName] NOT NULL,
    [Email]             [dbo].[Email]      NOT NULL,
    [VerificationCode]  UNIQUEIDENTIFIER   NOT NULL,
    [CodeGeneratedDate] DATETIME           NOT NULL,
    [CodeExpiryDate]    DATETIME           NOT NULL,
    [ForeignStudent]    BIT                NOT NULL,
    CONSTRAINT [PK_CandidateTemps] PRIMARY KEY CLUSTERED ([CandidateTempID] ASC),
    CONSTRAINT [FK_CandidateTemps_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID])
);


