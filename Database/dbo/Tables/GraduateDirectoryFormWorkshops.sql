﻿CREATE TABLE [dbo].[GraduateDirectoryFormWorkshops] (
    [GraduateDirectoryFormWorkshopID] INT            IDENTITY (1, 1) NOT NULL,
    [GraduateDirectoryFormID]         INT            NOT NULL,
    [Description]                     VARCHAR (1000) NOT NULL,
    [Type]                            [dbo].[Enum]   NOT NULL,
    [FromDate]                        DATE           NOT NULL,
    [ToDate]                          DATE           NOT NULL,
    CONSTRAINT [PK_GraduateDirectoryFormWorkshops] PRIMARY KEY CLUSTERED ([GraduateDirectoryFormWorkshopID] ASC),
    CONSTRAINT [CK_GraduateDirectoryFormWorkshops ([FromDate]]<=[ToDate]])] CHECK ([FromDate]<=[ToDate]),
    CONSTRAINT [FK_GraduateDirectoryFormWorkshops_GraduateDirectoryForms_GraduateDirectoryFormID] FOREIGN KEY ([GraduateDirectoryFormID]) REFERENCES [dbo].[GraduateDirectoryForms] ([GraduateDirectoryFormID])
);

