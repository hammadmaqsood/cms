﻿CREATE TABLE [dbo].[ExamInvigilatorExamSessions] (
    [ExamInvigilatorExamSessionID] INT IDENTITY (1, 1) NOT NULL,
    [ExamInvigilatorID]            INT NOT NULL,
    [ExamSessionID]                INT NOT NULL,
    [Available]                    BIT NOT NULL,
    CONSTRAINT [PK_ExamInvigilatorExamSessions] PRIMARY KEY CLUSTERED ([ExamInvigilatorExamSessionID] ASC),
    CONSTRAINT [FK_ExamInvigilatorExamSessions_ExamInvigilators_ExamInvigilatorID] FOREIGN KEY ([ExamInvigilatorID]) REFERENCES [dbo].[ExamInvigilators] ([ExamInvigilatorID]),
    CONSTRAINT [FK_ExamInvigilatorExamSessions_ExamSessions_ExamSessionID] FOREIGN KEY ([ExamSessionID]) REFERENCES [dbo].[ExamSessions] ([ExamSessionID]),
    CONSTRAINT [UK_ExamInvigilatorExamSessions_ExamInvigilatorID_ExamSessionID] UNIQUE NONCLUSTERED ([ExamInvigilatorID] ASC, [ExamSessionID] ASC)
);









