﻿CREATE TABLE [dbo].[ResearchThemeAreas] (
    [ResearchThemeAreaID] UNIQUEIDENTIFIER NOT NULL,
    [ResearchThemeID]     UNIQUEIDENTIFIER NOT NULL,
    [AreaName]            VARCHAR (500)    NOT NULL,
    CONSTRAINT [PK_ResearchThemeAreas] PRIMARY KEY CLUSTERED ([ResearchThemeAreaID] ASC),
    CONSTRAINT [FK_ResearchThemeAreas_ResearchThemes_ResearchThemeID] FOREIGN KEY ([ResearchThemeID]) REFERENCES [dbo].[ResearchThemes] ([ResearchThemeID])
);

