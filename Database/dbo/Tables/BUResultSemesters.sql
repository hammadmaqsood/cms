﻿CREATE TABLE [dbo].[BUResultSemesters] (
    [BUResultSemesterID] INT                IDENTITY (1, 1) NOT NULL,
    [BUResultID]         INT                NOT NULL,
    [SemesterID]         [dbo].[SemesterID] NOT NULL,
    [GPA]                [dbo].[GPA]        NULL,
    [CGPA]               [dbo].[GPA]        NULL,
    [Remarks]            VARCHAR (100)      NULL,
    CONSTRAINT [PK_BUResultSemesters] PRIMARY KEY CLUSTERED ([BUResultSemesterID] ASC),
    CONSTRAINT [FK_BUResultSemesters_BUResults_BUResultID] FOREIGN KEY ([BUResultID]) REFERENCES [dbo].[BUResults] ([BUResultID]),
    CONSTRAINT [UK_BUResultSemesters_BUResultID_SemesterID] UNIQUE NONCLUSTERED ([BUResultID] ASC, [SemesterID] ASC)
);








