﻿CREATE TABLE [dbo].[OfferedCourseAttendanceDetails] (
    [OfferedCourseAttendanceDetailID] INT            IDENTITY (1, 1) NOT NULL,
    [OfferedCourseAttendanceID]       INT            NOT NULL,
    [RegisteredCourseID]              INT            NOT NULL,
    [Hours]                           DECIMAL (2, 1) NOT NULL,
    [MigrationID]                     INT            NULL,
    [FeeDefaulter]                    BIT            NULL,
    CONSTRAINT [PK_OfferedCourseAttendanceDetails] PRIMARY KEY CLUSTERED ([OfferedCourseAttendanceDetailID] ASC),
    CONSTRAINT [FK_OfferedCourseAttendanceDetails_OfferedCourseAttendances_OfferedCourseAttendanceID] FOREIGN KEY ([OfferedCourseAttendanceID]) REFERENCES [dbo].[OfferedCourseAttendances] ([OfferedCourseAttendanceID]),
    CONSTRAINT [FK_OfferedCourseAttendanceDetails_RegisteredCourses_RegisteredCourseID] FOREIGN KEY ([RegisteredCourseID]) REFERENCES [dbo].[RegisteredCourses] ([RegisteredCourseID])
);
