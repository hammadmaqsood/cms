﻿CREATE TABLE [dbo].[UserRoleModules] (
    [UserRoleModuleID]       INT          IDENTITY (1, 1) NOT NULL,
    [UserRoleID]             INT          NOT NULL,
    [Module]                 [dbo].[Enum] NOT NULL,
    [ModuleName]             VARCHAR (50) NOT NULL,
    [DepartmentID]           INT          NULL,
    [ProgramID]              INT          NULL,
    [AdmissionOpenProgramID] INT          NULL,
    CONSTRAINT [PK_UserRoleModules] PRIMARY KEY CLUSTERED ([UserRoleModuleID] ASC),
    CONSTRAINT [FK_UserRoleModules_AdmissionOpenPrograms_AdmissionOpenProgramID] FOREIGN KEY ([AdmissionOpenProgramID]) REFERENCES [dbo].[AdmissionOpenPrograms] ([AdmissionOpenProgramID]),
    CONSTRAINT [FK_UserRoleModules_Departments_DepartmentID] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Departments] ([DepartmentID]),
    CONSTRAINT [FK_UserRoleModules_Programs_ProgramID] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[Programs] ([ProgramID]),
    CONSTRAINT [FK_UserRoleModules_UserRoles_UserRoleID] FOREIGN KEY ([UserRoleID]) REFERENCES [dbo].[UserRoles] ([UserRoleID])
);





