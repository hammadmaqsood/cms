﻿CREATE TABLE [dbo].[SystemFolders] (
    [SystemFolderID]               UNIQUEIDENTIFIER           NOT NULL,
    [FolderName]                   [dbo].[FileName]           NOT NULL,
    [FolderType]                   [dbo].[EnumString]               NOT NULL,
    [CreatedDate]                  DATETIME                   NOT NULL,
    [CreatedByUserLoginHistoryID]  INT                        NOT NULL,
    [DeletedDate]                 DATETIME                   NULL,
    [DeletedByUserLoginHistoryID]  INT                        NULL,
	CONSTRAINT [PK_SystemFolders] PRIMARY KEY CLUSTERED ([SystemFolderID] ASC)
);



