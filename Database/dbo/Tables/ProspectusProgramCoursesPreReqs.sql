﻿CREATE TABLE [dbo].[ProspectusProgramCoursesPreReqs] (
    [ProspectusProgramCoursesPreReqID] UNIQUEIDENTIFIER NOT NULL,
    [ProspectusProgramCourseID]        UNIQUEIDENTIFIER NOT NULL,
    [PreReq1ProspectusProgramCourseID] UNIQUEIDENTIFIER NOT NULL,
    [PreReq2ProspectusProgramCourseID] UNIQUEIDENTIFIER NULL,
    [PreReq3ProspectusProgramCourseID] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ProspectusProgramCoursesPreReqs] PRIMARY KEY CLUSTERED ([ProspectusProgramCoursesPreReqID] ASC),
    CONSTRAINT [CK_ProspectusProgramCoursesPreReqs ([PreReq3ProspectusProgramCourseID]] IS NULL OR [PreReq2ProspectusProgramCourseID]] IS NOT NULL] CHECK ([PreReq3ProspectusProgramCourseID] IS NULL OR [PreReq2ProspectusProgramCourseID] IS NOT NULL),
    CONSTRAINT [FK_ProspectusProgramCoursesPreReqs_ProspectusProgramCourses_PreReq1ProspectusProgramCourseID] FOREIGN KEY ([PreReq1ProspectusProgramCourseID]) REFERENCES [dbo].[ProspectusProgramCourses] ([ProspectusProgramCourseID]),
    CONSTRAINT [FK_ProspectusProgramCoursesPreReqs_ProspectusProgramCourses_PreReq2ProspectusProgramCourseID] FOREIGN KEY ([PreReq2ProspectusProgramCourseID]) REFERENCES [dbo].[ProspectusProgramCourses] ([ProspectusProgramCourseID]),
    CONSTRAINT [FK_ProspectusProgramCoursesPreReqs_ProspectusProgramCourses_PreReq3ProspectusProgramCourseID] FOREIGN KEY ([PreReq3ProspectusProgramCourseID]) REFERENCES [dbo].[ProspectusProgramCourses] ([ProspectusProgramCourseID]),
    CONSTRAINT [FK_ProspectusProgramCoursesPreReqs_ProspectusProgramCourses_ProspectusProgramCourseID] FOREIGN KEY ([ProspectusProgramCourseID]) REFERENCES [dbo].[ProspectusProgramCourses] ([ProspectusProgramCourseID])
);

