﻿CREATE TABLE [dbo].[SurveyQuestions] (
    [SurveyQuestionID]      INT           IDENTITY (1, 1) NOT NULL,
    [SurveyQuestionGroupID] INT           NOT NULL,
    [QuestionNo]            VARCHAR (50)  NOT NULL,
    [QuestionText]          VARCHAR (MAX) NOT NULL,
    [QuestionType]          [dbo].[Enum]  NOT NULL,
    [DisplayIndex]          TINYINT       NOT NULL,
    CONSTRAINT [PK_SurveyQuestions] PRIMARY KEY CLUSTERED ([SurveyQuestionID] ASC),
    CONSTRAINT [FK_SurveyQuestions_SurveyQuestionGroups_SurveyQuestionGroupID] FOREIGN KEY ([SurveyQuestionGroupID]) REFERENCES [dbo].[SurveyQuestionGroups] ([SurveyQuestionGroupID])
);












GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MCQ, Text', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SurveyQuestions', @level2type = N'COLUMN', @level2name = N'QuestionType';


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SurveyQuestions]
    ON [dbo].[SurveyQuestions]([SurveyQuestionGroupID] ASC, [DisplayIndex] ASC);

