﻿CREATE TABLE [dbo].[ExamRooms] (
    [ExamRoomID]     INT           IDENTITY (1, 1) NOT NULL,
    [ExamBuildingID] INT           NOT NULL,
    [RoomName]       VARCHAR (100) NOT NULL,
    [Rows]           TINYINT       NOT NULL,
    [Columns]        TINYINT       NOT NULL,
    [Status]         [dbo].[Enum]  NOT NULL,
    [Sequence]       TINYINT       NOT NULL,
    [MigrationID]    INT           NULL,
    CONSTRAINT [PK_ExamRooms] PRIMARY KEY CLUSTERED ([ExamRoomID] ASC),
    CONSTRAINT [FK_ExamRooms_ExamBuildings_ExamBuildingID] FOREIGN KEY ([ExamBuildingID]) REFERENCES [dbo].[ExamBuildings] ([ExamBuildingID]),
    CONSTRAINT [UK_ExamRooms_ExamBuildingID_RoomName] UNIQUE NONCLUSTERED ([ExamBuildingID] ASC, [RoomName] ASC)
);







