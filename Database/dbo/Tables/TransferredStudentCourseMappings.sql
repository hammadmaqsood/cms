﻿CREATE TABLE [dbo].[TransferredStudentCourseMappings] (
    [TransferredStudentCourseMappingID] INT                IDENTITY (1, 1) NOT NULL,
    [TransferredStudentID]              INT                NOT NULL,
    [FromRegisteredCourseID]            INT                NOT NULL,
    [ToCourseID]                        INT                NOT NULL,
    [Status]                            [dbo].[Enum] NOT NULL,
    CONSTRAINT [PK_TransferredStudentCourseMappings] PRIMARY KEY CLUSTERED ([TransferredStudentCourseMappingID] ASC),
    CONSTRAINT [FK_TransferredStudentCourseMappings_Courses_ToCourseID] FOREIGN KEY ([ToCourseID]) REFERENCES [dbo].[Courses] ([CourseID]),
    CONSTRAINT [FK_TransferredStudentCourseMappings_RegisteredCourses_FromRegisteredCourseID] FOREIGN KEY ([FromRegisteredCourseID]) REFERENCES [dbo].[RegisteredCourses] ([RegisteredCourseID]),
    CONSTRAINT [FK_TransferredStudentCourseMappings_TransferredStudents_TransferredStudentID] FOREIGN KEY ([TransferredStudentID]) REFERENCES [dbo].[TransferredStudents] ([TransferredStudentID]),
    CONSTRAINT [UK_TransferredStudentCourseMappings_TransferredStudentID_FromRegisteredCourseID] UNIQUE NONCLUSTERED ([TransferredStudentID] ASC, [FromRegisteredCourseID] ASC)
);











