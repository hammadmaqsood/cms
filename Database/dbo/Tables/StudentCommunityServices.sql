﻿CREATE TABLE [dbo].[StudentCommunityServices] (
    [StudentCommunityServiceID] INT                IDENTITY (1, 1) NOT NULL,
    [StudentID]                 INT                NOT NULL,
    [Organization]              VARCHAR (500)      NULL,
    [JobTitle]                  VARCHAR (500)      NULL,
    [CompletedDate]             DATE               NOT NULL,
    [SemesterID]                [dbo].[SemesterID] NOT NULL,
    [Hours]                     TINYINT            NOT NULL,
    CONSTRAINT [PK_StudentCommunityServices] PRIMARY KEY CLUSTERED ([StudentCommunityServiceID] ASC),
    CONSTRAINT [FK_StudentCommunityServices_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID]),
    CONSTRAINT [FK_StudentCommunityServices_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID])
);



