﻿CREATE TABLE [dbo].[IntegratedServiceUsers] (
    [IntegratedServiceUserID] INT                     IDENTITY (1, 1) NOT NULL,
    [ServiceName]             VARCHAR (100)           NOT NULL,
    [ServiceType]             [dbo].[Enum]            NOT NULL,
    [InstituteID]             INT                     NULL,
    [UserName]                UNIQUEIDENTIFIER        NOT NULL,
    [Password]                [dbo].[EncryptedHashed] NOT NULL,
    [IPAddress]               VARCHAR (39)            NOT NULL,
    [CreatedDate]             DATETIME                NOT NULL,
    [ExpiryDate]              DATETIME                NOT NULL,
    [Status]                  [dbo].[Enum]            NOT NULL,
    [IsDeleted]               BIT                     NOT NULL,
    CONSTRAINT [PK_IntegratedServiceUsers] PRIMARY KEY CLUSTERED ([IntegratedServiceUserID] ASC),
    CONSTRAINT [FK_IntegratedServiceUsers_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);





