﻿CREATE TABLE [dbo].[AlumniJobPositions] (
    [AlumniJobPositionID] UNIQUEIDENTIFIER NOT NULL,
    [StudentID]           INT              NOT NULL,
    [InstituteID]         INT              NOT NULL,
    [JobStatus]           [dbo].[EnumGuid] NOT NULL,
    [LastUpdatedOn]       DATE             NOT NULL,
    [Organization]        VARCHAR (1000)   NULL,
    [Designation]         VARCHAR (1000)   NULL,
    [University]          VARCHAR (1000)   NULL,
    [DegreeTitle]         VARCHAR (1000)   NULL,
    [Country]             VARCHAR (1000)   NULL,
    [City]                VARCHAR (1000)   NULL,
    [Mobile]              [dbo].[Phone]    NULL,
    [Phone]               [dbo].[Phone]    NULL,
    [PersonalEmail]       [dbo].[Email]    NULL,
    [MailingAddress]      [dbo].[Address]  NULL,
    CONSTRAINT [PK_AlumniJobPositions] PRIMARY KEY CLUSTERED ([AlumniJobPositionID] ASC),
    CONSTRAINT [FK_AlumniJobPositions_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID]),
    CONSTRAINT [FK_AlumniJobPositions_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID])
);



