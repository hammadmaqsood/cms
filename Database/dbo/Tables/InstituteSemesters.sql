﻿CREATE TABLE [dbo].[InstituteSemesters] (
    [InstituteSemesterID] UNIQUEIDENTIFIER   NOT NULL,
    [InstituteID]         INT                NOT NULL,
    [SemesterID]          [dbo].[SemesterID] NOT NULL,
    [ClassesStartDate]    DATE               NOT NULL,
    [ClassesEndDate]      DATE               NULL,
    CONSTRAINT [PK_InstituteSemesters] PRIMARY KEY CLUSTERED ([InstituteSemesterID] ASC),
    CONSTRAINT [FK_InstituteSemesters_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID]),
    CONSTRAINT [FK_InstituteSemesters_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID])
);

