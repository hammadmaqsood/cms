﻿CREATE TABLE [dbo].[IPAddresses] (
    [IPAddressID] INT          IDENTITY (1, 1) NOT NULL,
    [InstituteID] INT          NOT NULL,
    [IPV4Address] VARCHAR (50) NULL,
    [IPV6Address] VARCHAR (50) NULL,
    CONSTRAINT [PK_IPAddresses] PRIMARY KEY CLUSTERED ([IPAddressID] ASC),
    CONSTRAINT [FK_IPAddresses_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);
