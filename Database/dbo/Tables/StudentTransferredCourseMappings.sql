﻿CREATE TABLE [dbo].[StudentTransferredCourseMappings] (
    [StudentTransferredCourseMappingID] INT IDENTITY (1, 1) NOT NULL,
    [StudentTransferID]                 INT NOT NULL,
    [TransferredFromCourseID]           INT NOT NULL,
    [TransferredToCourseID]             INT NOT NULL,
    CONSTRAINT [PK_StudentTransferredCourses] PRIMARY KEY CLUSTERED ([StudentTransferredCourseMappingID] ASC),
    CONSTRAINT [FK_StudentTransferredCourses_StudentTransfers] FOREIGN KEY ([StudentTransferID]) REFERENCES [dbo].[StudentTransfers] ([StudentTransferID])
);

