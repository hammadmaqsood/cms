﻿CREATE TABLE [dbo].[GraduateDirectoryExtraCurricularActivities] (
    [GraduateDirectoryExtraCurricularActivityID] INT            IDENTITY (1, 1) NOT NULL,
    [GraduateDirectoryFormID]                    INT            NOT NULL,
    [Description]                                VARCHAR (1000) NOT NULL,
    CONSTRAINT [PK_GraduateDirectoryExtraCurricularActivities] PRIMARY KEY CLUSTERED ([GraduateDirectoryExtraCurricularActivityID] ASC),
    CONSTRAINT [FK_GraduateDirectoryExtraCurricularActivities_GraduateDirectoryForms_GraduateDirectoryFormID] FOREIGN KEY ([GraduateDirectoryFormID]) REFERENCES [dbo].[GraduateDirectoryForms] ([GraduateDirectoryFormID])
);


