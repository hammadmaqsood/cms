﻿CREATE TABLE [dbo].[QASummaryFacultyEvaluations] (
    [QASummaryFacultyEvaluationID]      INT             NOT NULL,
    [SurveyConductID]                   INT             NOT NULL,
    [FacultyMemberID]                   INT             NOT NULL,
    [DepartmentID]                      INT             NOT NULL,
    [MidAverage]                        DECIMAL (5, 2)  NOT NULL,
    [FinalFinalAverage]                 DECIMAL (5, 2)  NOT NULL,
    [TotalAverage]                      DECIMAL (5, 2)  NOT NULL,
    [CompilationDate]                   DATETIME        NOT NULL,
    [HODRemarks]                        [dbo].[Remarks] NULL,
    [HODRemarksDate]                    DATETIME        NULL,
    [HODRemarksUserLoginHistoryID]      INT             NULL,
    [DirectorRemarks]                   [dbo].[Remarks] NULL,
    [DirectorRemarksDate]               DATETIME        NULL,
    [DirectorRemarksUserLoginHistoryID] INT             NULL,
    [DGRemarks]                         [dbo].[Remarks] NULL,
    [DGRemarksDate]                     DATETIME        NULL,
    [DGRemarksUserLoginHistoryID]       INT             NULL,
    CONSTRAINT [PK_QASummaryFacultyEvaluations] PRIMARY KEY CLUSTERED ([QASummaryFacultyEvaluationID] ASC),
    CONSTRAINT [FK_QASummaryFacultyEvaluations_Departments_DepartmentID] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Departments] ([DepartmentID]),
    CONSTRAINT [FK_QASummaryFacultyEvaluations_FacultyMembers_FacultyMemberID] FOREIGN KEY ([FacultyMemberID]) REFERENCES [dbo].[FacultyMembers] ([FacultyMemberID]),
    CONSTRAINT [FK_QASummaryFacultyEvaluations_SurveyConducts_SurveyConductID] FOREIGN KEY ([SurveyConductID]) REFERENCES [dbo].[SurveyConducts] ([SurveyConductID]),
    CONSTRAINT [FK_QASummaryFacultyEvaluations_UserLoginHistory_DGRemarksUserLoginHistoryID] FOREIGN KEY ([DGRemarksUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID]),
    CONSTRAINT [FK_QASummaryFacultyEvaluations_UserLoginHistory_DirectorRemarksUserLoginHistoryID] FOREIGN KEY ([DirectorRemarksUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID]),
    CONSTRAINT [FK_QASummaryFacultyEvaluations_UserLoginHistory_HODRemarksUserLoginHistoryID] FOREIGN KEY ([HODRemarksUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID])
);


