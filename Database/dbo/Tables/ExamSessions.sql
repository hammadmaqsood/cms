﻿CREATE TABLE [dbo].[ExamSessions] (
    [ExamSessionID]       INT           IDENTITY (1, 1) NOT NULL,
    [ExamConductID]       INT           NOT NULL,
    [SessionName]         VARCHAR (50)  NOT NULL,
    [StartTime]           TIME (7)      NOT NULL,
    [TotalTimeInMinutes]  TINYINT       NOT NULL,
    [InvigilationPayRate] SMALLINT      NOT NULL,
    [Shift]               [dbo].[Shift] NOT NULL,
    [Sequence]            TINYINT       NOT NULL,
    CONSTRAINT [PK_ExamSessions] PRIMARY KEY CLUSTERED ([ExamSessionID] ASC),
    CONSTRAINT [FK_ExamSessions_ExamConducts_ExamConductID] FOREIGN KEY ([ExamConductID]) REFERENCES [dbo].[ExamConducts] ([ExamConductID])
);








GO



GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'In Minutes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ExamSessions', @level2type = N'COLUMN', @level2name = N'TotalTimeInMinutes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Start Date with Time', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ExamSessions', @level2type = N'COLUMN', @level2name = N'StartTime';

