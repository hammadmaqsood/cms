﻿CREATE TABLE [dbo].[SurveyQuestionOptions] (
    [SurveyQuestionOptionID] INT           IDENTITY (1, 1) NOT NULL,
    [SurveyQuestionID]       INT           NOT NULL,
    [OptionText]             VARCHAR (MAX) NOT NULL,
    [Weightage]              FLOAT (53)    NULL,
    CONSTRAINT [PK_SurveyQuestionOptions] PRIMARY KEY CLUSTERED ([SurveyQuestionOptionID] ASC),
    CONSTRAINT [FK_SurveyQuestionOptions_SurveyQuestions_SurveyQuestionID] FOREIGN KEY ([SurveyQuestionID]) REFERENCES [dbo].[SurveyQuestions] ([SurveyQuestionID])
);







