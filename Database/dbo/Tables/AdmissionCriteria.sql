﻿CREATE TABLE [dbo].[AdmissionCriteria] (
    [AdmissionCriteriaID] INT          IDENTITY (1, 1) NOT NULL,
    [InstituteID]         INT          NOT NULL,
    [CriteriaName]        VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_AdmissionCriteria] PRIMARY KEY CLUSTERED ([AdmissionCriteriaID] ASC),
    CONSTRAINT [FK_AdmissionCriteria_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);






