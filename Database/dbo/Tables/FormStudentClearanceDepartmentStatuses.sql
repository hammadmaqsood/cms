﻿CREATE TABLE [dbo].[FormStudentClearanceDepartmentStatuses] (
    [FormStudentClearanceDepartmentStatusID] UNIQUEIDENTIFIER NOT NULL,
    [FormStudentClearanceID]                 UNIQUEIDENTIFIER NOT NULL,
    [UserLoginHistoryID]                     INT              NULL,
    [Department]                             [dbo].[EnumGuid] NOT NULL,
    [DepartmentStatus]                       [dbo].[EnumGuid] NOT NULL,
    [DepartmentStatusChangedDate]            DATETIME         NULL,
    [Remarks]                                [dbo].[Remarks]  NULL,
    CONSTRAINT [PK_FormStudentClearanceDepartmentStatuses] PRIMARY KEY CLUSTERED ([FormStudentClearanceDepartmentStatusID] ASC),
    CONSTRAINT [FK_FormStudentClearanceDepartmentStatuses_FormStudentClearances_FormStudentClearanceID] FOREIGN KEY ([FormStudentClearanceID]) REFERENCES [dbo].[FormStudentClearances] ([FormStudentClearanceID]),
    CONSTRAINT [FK_FormStudentClearanceDepartmentStatuses_UserLoginHistory_UserLoginHistoryID] FOREIGN KEY ([UserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID])
);