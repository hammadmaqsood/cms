﻿CREATE TABLE [dbo].[FeeStructureDetails] (
    [FeeStructureDetailID] INT                IDENTITY (1, 1) NOT NULL,
    [FeeStructureID]       INT                NOT NULL,
    [FeeHeadID]            INT                NOT NULL,
    [SemesterNo]           [dbo].[SemesterNo] NULL,
    [Category]             [dbo].[Category]   NULL,
    [Amount]               INT                NOT NULL,
    CONSTRAINT [PK_FeeStructureDetails] PRIMARY KEY CLUSTERED ([FeeStructureDetailID] ASC),
    CONSTRAINT [FK_FeeStructureDetails_FeeHeads_FeeHeadID] FOREIGN KEY ([FeeHeadID]) REFERENCES [dbo].[FeeHeads] ([FeeHeadID]),
    CONSTRAINT [FK_FeeStructureDetails_FeeStructures_FeeStructureID] FOREIGN KEY ([FeeStructureID]) REFERENCES [dbo].[FeeStructures] ([FeeStructureID])
);




GO