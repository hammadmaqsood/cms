﻿CREATE TABLE [dbo].[ScholarshipAnnouncements] (
    [ScholarshipAnnouncementID] INT                IDENTITY (1, 1) NOT NULL,
    [ScholarshipID]             INT                NOT NULL,
    [SemesterID]                [dbo].[SemesterID] NOT NULL,
    [StartDate]                 DATETIME           NOT NULL,
    [EndDate]                   DATETIME           NULL,
    [Status]                    [dbo].[Enum]       NOT NULL,
    CONSTRAINT [PK_ScholarshipAnnouncements] PRIMARY KEY CLUSTERED ([ScholarshipAnnouncementID] ASC),
    CONSTRAINT [CK_ScholarshipAnnouncements ([StartDate]]<=isnull([EndDate]],[StartDate]]))] CHECK ([StartDate]<=isnull([EndDate],[StartDate])),
    CONSTRAINT [FK_ScholarshipAnnouncements_Scholarships_ScholarshipID] FOREIGN KEY ([ScholarshipID]) REFERENCES [dbo].[Scholarships] ([ScholarshipID]),
    CONSTRAINT [FK_ScholarshipAnnouncements_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID])
);



