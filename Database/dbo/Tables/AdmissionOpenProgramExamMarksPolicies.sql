﻿CREATE TABLE [dbo].[AdmissionOpenProgramExamMarksPolicies] (
    [AdmissionOpenProgramExamMarksPolicyID] INT                IDENTITY (1, 1) NOT NULL,
    [AdmissionOpenProgramID]                INT                NOT NULL,
    [ExamMarksPolicyID]                     INT                NOT NULL,
    [SemesterID]                            [dbo].[SemesterID] NOT NULL,
    [VerificationDate]                      DATETIME           NULL,
    [VerifiedByUserLoginHistoryID]          INT                NULL,
    CONSTRAINT [PK_AdmissionOpenProgramExamMarksPolicies] PRIMARY KEY CLUSTERED ([AdmissionOpenProgramExamMarksPolicyID] ASC),
    CONSTRAINT [FK_AdmissionOpenProgramExamMarksPolicies_AdmissionOpenPrograms_AdmissionOpenProgramID] FOREIGN KEY ([AdmissionOpenProgramID]) REFERENCES [dbo].[AdmissionOpenPrograms] ([AdmissionOpenProgramID]),
    CONSTRAINT [FK_AdmissionOpenProgramExamMarksPolicies_ExamMarksPolicies_ExamMarksPolicyID] FOREIGN KEY ([ExamMarksPolicyID]) REFERENCES [dbo].[ExamMarksPolicies] ([ExamMarksPolicyID]),
    CONSTRAINT [FK_AdmissionOpenProgramExamMarksPolicies_UserLoginHistory_VerifiedByUserLoginHistoryID] FOREIGN KEY ([VerifiedByUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID]),
    CONSTRAINT [UK_AdmissionOpenProgramExamMarksPolicies_AdmissionOpenProgramID_SemesterID] UNIQUE NONCLUSTERED ([AdmissionOpenProgramID] ASC, [SemesterID] ASC)
);



