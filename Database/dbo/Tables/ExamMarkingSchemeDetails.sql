﻿CREATE TABLE [dbo].[ExamMarkingSchemeDetails] (
    [ExamMarkingSchemeDetailID] INT               IDENTITY (1, 1) NOT NULL,
    [ExamMarkingSchemeID]       INT               NOT NULL,
    [ExamMarksType]             [dbo].[Enum]      NOT NULL,
    [CourseCategory]            [dbo].[Enum]      NOT NULL,
    [MinMarks]                  [dbo].[ExamMarks] NOT NULL,
    [MaxMarks]                  [dbo].[ExamMarks] NOT NULL,
    CONSTRAINT [PK_ExamMarkingSchemeDetails] PRIMARY KEY CLUSTERED ([ExamMarkingSchemeDetailID] ASC),
    CONSTRAINT [CK_ExamMarkingSchemeDetails ([MinMarks]]<=[MaxMarks]])] CHECK ([MinMarks]<=[MaxMarks]),
    CONSTRAINT [FK_ExamMarkingSchemeDetails_ExamMarkingSchemes_ExamMarkingSchemeID] FOREIGN KEY ([ExamMarkingSchemeID]) REFERENCES [dbo].[ExamMarkingSchemes] ([ExamMarkingSchemeID]),
    CONSTRAINT [UK_ExamMarkingSchemeDetails_ExamMarkingSchemeID_ExamMarksType_CourseCategory] UNIQUE NONCLUSTERED ([ExamMarkingSchemeID] ASC, [ExamMarksType] ASC, [CourseCategory] ASC)
);















