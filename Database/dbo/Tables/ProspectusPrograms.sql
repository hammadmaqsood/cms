﻿CREATE TABLE [dbo].[ProspectusPrograms] (
    [ProspectusProgramID] UNIQUEIDENTIFIER NOT NULL,
    [ProspectusID]        UNIQUEIDENTIFIER NOT NULL,
    [ProgramName]         VARCHAR (100)    NOT NULL,
    [Duration]            [dbo].[Enum]     NOT NULL,
    CONSTRAINT [PK_ProspectusPrograms] PRIMARY KEY CLUSTERED ([ProspectusProgramID] ASC),
    CONSTRAINT [FK_ProspectusPrograms_Prospectuses_ProspectusID] FOREIGN KEY ([ProspectusID]) REFERENCES [dbo].[Prospectuses] ([ProspectusID])
);

