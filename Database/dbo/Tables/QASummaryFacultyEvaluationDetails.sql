﻿CREATE TABLE [dbo].[QASummaryFacultyEvaluationDetails] (
    [QASummaryFacultyEvaluationDetailID] INT            NOT NULL,
    [QASummaryFacultyEvaluationID]       INT            NOT NULL,
    [OfferedCourseID]                    INT            NOT NULL,
    [Mid]                                DECIMAL (5, 2) NULL,
    [Final]                              DECIMAL (5, 2) NULL,
    CONSTRAINT [PK_QASummaryFacultyEvaluationDetails] PRIMARY KEY CLUSTERED ([QASummaryFacultyEvaluationDetailID] ASC),
    CONSTRAINT [FK_QASummaryFacultyEvaluationDetails_OfferedCourses_OfferedCourseID] FOREIGN KEY ([OfferedCourseID]) REFERENCES [dbo].[OfferedCourses] ([OfferedCourseID]),
    CONSTRAINT [FK_QASummaryFacultyEvaluationDetails_QASummaryFacultyEvaluations_QASummaryFacultyEvaluationID] FOREIGN KEY ([QASummaryFacultyEvaluationID]) REFERENCES [dbo].[QASummaryFacultyEvaluations] ([QASummaryFacultyEvaluationID])
);


