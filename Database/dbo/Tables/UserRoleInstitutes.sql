﻿CREATE TABLE [dbo].[UserRoleInstitutes] (
    [UserRoleInstituteID] INT IDENTITY (1, 1) NOT NULL,
    [UserRoleID]          INT NOT NULL,
    [InstituteID]         INT NOT NULL,
    CONSTRAINT [PK_UserRoleInstitutes] PRIMARY KEY CLUSTERED ([UserRoleInstituteID] ASC),
    CONSTRAINT [FK_UserRoleInstitutes_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID]),
    CONSTRAINT [FK_UserRoleInstitutes_UserRoles_UserRoleID] FOREIGN KEY ([UserRoleID]) REFERENCES [dbo].[UserRoles] ([UserRoleID]),
    CONSTRAINT [UK_UserRoleInstitutes_UserRoleID_InstituteID] UNIQUE NONCLUSTERED ([UserRoleID] ASC, [InstituteID] ASC)
);








GO
