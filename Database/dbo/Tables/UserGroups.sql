﻿CREATE TABLE [dbo].[UserGroups] (
    [UserGroupID] INT              IDENTITY (1, 1) NOT NULL,
    [InstituteID] INT              NULL,
    [UserType]    [dbo].[UserType] NOT NULL,
    [GroupName]   VARCHAR (100)    NOT NULL,
    [IsDeleted]   BIT              NOT NULL,
    CONSTRAINT [PK_UserGroups] PRIMARY KEY CLUSTERED ([UserGroupID] ASC),
    CONSTRAINT [FK_UserGroups_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);




GO
