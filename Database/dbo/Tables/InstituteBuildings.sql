﻿CREATE TABLE [dbo].[InstituteBuildings] (
    [InstituteBuildingID] INT          IDENTITY (1, 1) NOT NULL,
    [InstituteID]         INT          NOT NULL,
    [BuildingName]        VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_InstituteBuildings] PRIMARY KEY CLUSTERED ([InstituteBuildingID] ASC),
    CONSTRAINT [FK_InstituteBuildings_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);



