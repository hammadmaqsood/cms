﻿CREATE TABLE [dbo].[Students] (
    [StudentID]                                       INT                     IDENTITY (1, 1) NOT NULL,
    [Enrollment]                                      VARCHAR (15)            NOT NULL,
    [CandidateAppliedProgramID]                       INT                     NULL,
    [AdmissionOpenProgramID]                          INT                     NOT NULL,
    [CreatedDate]                                     DATETIME                NULL,
    [RegistrationNo]                                  INT                     NULL,
    [MeritListScore]                                  FLOAT (53)              NULL,
    [MeritListPosition]                               FLOAT (53)              NULL,
    [Name]                                            [dbo].[PersonName]      NOT NULL,
    [PersonalEmail]                                   [dbo].[Email]           NULL,
    [PersonalEmailNotVerified]                        [dbo].[Email]           NULL,
    [UniversityEmail]                                 [dbo].[Email]           NULL,
    [Password]                                        [dbo].[EncryptedHashed] NULL,
    [ConfirmationCode]                                UNIQUEIDENTIFIER        NULL,
    [ConfirmationCodeExpiryDate]                      DATETIME                NULL,
    [CNIC]                                            [dbo].[CNIC]            NULL,
    [PassportNo]                                      VARCHAR (50)            NULL,
    [Gender]                                          [dbo].[Gender]          NOT NULL,
    [Category]                                        [dbo].[Category]        NOT NULL,
    [DOB]                                             DATE                    NOT NULL,
    [BloodGroup]                                      [dbo].[Enum]            NULL,
    [Phone]                                           [dbo].[Phone]           NULL,
    [Mobile]                                          [dbo].[Phone]           NULL,
    [MobileNotVerified]                               [dbo].[Phone]           NULL,
    [MobileVerificationCode]                          INT                     NULL,
    [Nationality]                                     VARCHAR (50)            NULL,
    [Country]                                         VARCHAR (50)            NULL,
    [Province]                                        VARCHAR (50)            NULL,
    [District]                                        VARCHAR (50)            NULL,
    [Tehsil]                                          VARCHAR (50)            NULL,
    [Domicile]                                        VARCHAR (50)            NULL,
    [AreaType]                                        [dbo].[Enum]            NULL,
    [CurrentAddress]                                  [dbo].[Address]         NULL,
    [PermanentAddress]                                [dbo].[Address]         NULL,
    [ServiceNo]                                       VARCHAR (50)            NULL,
    [FatherName]                                      [dbo].[PersonName]      NOT NULL,
    [FatherCNIC]                                      [dbo].[CNIC]            NULL,
    [FatherPassportNo]                                VARCHAR (50)            NULL,
    [FatherDesignation]                               VARCHAR (100)           NULL,
    [FatherDepartment]                                VARCHAR (100)           NULL,
    [FatherOrganization]                              VARCHAR (100)           NULL,
    [FatherServiceNo]                                 VARCHAR (50)            NULL,
    [FatherOfficePhone]                               [dbo].[Phone]           NULL,
    [FatherHomePhone]                                 [dbo].[Phone]           NULL,
    [FatherMobile]                                    [dbo].[Phone]           NULL,
    [FatherFax]                                       [dbo].[Phone]           NULL,
    [SponsoredBy]                                     [dbo].[Enum]            NULL,
    [SponsorName]                                     VARCHAR (100)           NULL,
    [SponsorCNIC]                                     [dbo].[CNIC]            NULL,
    [SponsorPassportNo]                               VARCHAR (50)            NULL,
    [SponsorRelationshipWithGuardian]                 VARCHAR (50)            NULL,
    [SponsorDesignation]                              VARCHAR (50)            NULL,
    [SponsorDepartment]                               VARCHAR (50)            NULL,
    [SponsorOrganization]                             VARCHAR (50)            NULL,
    [SponsorServiceNo]                                VARCHAR (50)            NULL,
    [SponsorPhoneHome]                                VARCHAR (50)            NULL,
    [SponsorMobile]                                   VARCHAR (50)            NULL,
    [SponsorPhoneOffice]                              VARCHAR (50)            NULL,
    [SponsorFax]                                      VARCHAR (50)            NULL,
    [ETSType]                                         [dbo].[Enum]            NULL,
    [ETSObtained]                                     FLOAT (53)              NULL,
    [ETSTotal]                                        FLOAT (53)              NULL,
    [EmergencyContactName]                            VARCHAR (200)           NULL,
    [EmergencyMobile]                                 VARCHAR (200)           NULL,
    [EmergencyPhone]                                  VARCHAR (200)           NULL,
    [EmergencyRelationship]                           VARCHAR (200)           NULL,
    [EmergencyAddress]                                [dbo].[Address]         NULL,
    [PhysicalDisability]                              VARCHAR (200)           NULL,
    [AdmissionType]                                   [dbo].[Enum]            NOT NULL,
    [Status]                                          [dbo].[Enum]            NULL,
    [StatusDate]                                      DATETIME                NULL,
    [MigrationID]                                     VARCHAR (50)            NULL,
    [UniversityEmailPassword]                         VARCHAR (50)            NULL,
    [FinalCGPA]                                       [dbo].[GPA]             NULL,
    [FinalTranscriptNo]                               INT                     NULL,
    [FinalTranscriptIssueDate]                        DATETIME                NULL,
    [Religion]                                        VARCHAR (100)           NULL,
    [NextOfKin]                                       VARCHAR (100)           NULL,
    [DeferredToAdmissionOpenProgramID]                INT                     NULL,
    [DeferredToShift]                                 [dbo].[Shift]           NULL,
    [DeferredToStudentID]                             INT                     NULL,
    [DeferredFromStudentID]                           INT                     NULL,
    [ParentsEmail]                                    [dbo].[Email]           NULL,
    [ParentsPassword]                                 [dbo].[Password]        NULL,
    [ParentsConfirmationCode]                         UNIQUEIDENTIFIER        NULL,
    [ParentsConfirmationCodeExpiryDate]               DATETIME                NULL,
    [DegreeNo]                                        VARCHAR (50)            NULL,
    [DegreeIssueDate]                                 DATE                    NULL,
    [DegreeIssuedToStudent]                           DATE                    NULL,
    [DegreeGazetteNo]                                 INT                     NULL,
    [PictureSystemFolderID]                           UNIQUEIDENTIFIER        NULL,
    [BlockedReason]                                   [dbo].[Remarks]         NULL,
    [StudentDocumentsStatus]                          [dbo].[EnumGuid]        NULL,
    [StudentDocumentsRemarks]                         [dbo].[Remarks]         NULL,
    [StudentDocumentsStatusDate]                      DATETIME                NULL,
    [StudentDocumentsLastUpdatedByUserLoginHistoryID] INT                     NULL,
    [CanStudentRegisterCourseOnline]                  BIT                     NULL,
    CONSTRAINT [PK_Students] PRIMARY KEY CLUSTERED ([StudentID] ASC),
    CONSTRAINT [CK_Students (len(ltrim(rtrim([Enrollment]])))>(0))] CHECK (len(ltrim(rtrim([Enrollment])))>(0)),
    CONSTRAINT [FK_Students_AdmissionOpenPrograms_AdmissionOpenProgramID] FOREIGN KEY ([AdmissionOpenProgramID]) REFERENCES [dbo].[AdmissionOpenPrograms] ([AdmissionOpenProgramID]),
    CONSTRAINT [FK_Students_AdmissionOpenPrograms_DeferredToAdmissionOpenProgramID] FOREIGN KEY ([DeferredToAdmissionOpenProgramID]) REFERENCES [dbo].[AdmissionOpenPrograms] ([AdmissionOpenProgramID]),
    CONSTRAINT [FK_Students_CandidateAppliedPrograms_CandidateAppliedProgramID] FOREIGN KEY ([CandidateAppliedProgramID]) REFERENCES [dbo].[CandidateAppliedPrograms] ([CandidateAppliedProgramID]),
    CONSTRAINT [FK_Students_Students_DeferredFromStudentID] FOREIGN KEY ([DeferredFromStudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [FK_Students_Students_DeferredToStudentID] FOREIGN KEY ([DeferredToStudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [FK_Students_SystemFolders_PictureSystemFolderID] FOREIGN KEY ([PictureSystemFolderID]) REFERENCES [dbo].[SystemFolders] ([SystemFolderID]),
    CONSTRAINT [FK_Students_UserLoginHistory_StudentDocumentsLastUpdatedByUserLoginHistoryID] FOREIGN KEY ([StudentDocumentsLastUpdatedByUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID])
);












































GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Student Service No. If Naval Employee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Students', @level2type = N'COLUMN', @level2name = N'ServiceNo';

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'This table contains the information about the student. e.g. personal, contacts, addresses, etc',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Students',
    @level2type = NULL,
    @level2name = NULL
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Primary Key',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Students',
    @level2type = N'COLUMN',
    @level2name = N'StudentID'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Enrollment of Student. Unique inside Institute.',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Students',
    @level2type = N'COLUMN',
    @level2name = N'Enrollment'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Created Date',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Students',
    @level2type = N'COLUMN',
    @level2name = N'CreatedDate'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Registration No. of Student',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Students',
    @level2type = N'COLUMN',
    @level2name = N'RegistrationNo'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Score (Percentage) obtained in Admission Merit.',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Students',
    @level2type = N'COLUMN',
    @level2name = N'MeritListScore'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Position in Admission''s Merit List',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Students',
    @level2type = N'COLUMN',
    @level2name = N'MeritListPosition'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Name of Student',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Students',
    @level2type = N'COLUMN',
    @level2name = N'Name'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Personal Email of Student',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Students',
    @level2type = N'COLUMN',
    @level2name = N'PersonalEmail'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'If student want to change the personal email then it will kept in this field until it is not verified by student.',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Students',
    @level2type = N'COLUMN',
    @level2name = N'PersonalEmailNotVerified'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'University Email address. Usually from student.bahria.edu.pk domain.',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Students',
    @level2type = N'COLUMN',
    @level2name = N'UniversityEmail'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Password for the Student Profile. It is hashed encrypted.',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Students',
    @level2type = N'COLUMN',
    @level2name = N'Password'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Confirmation Code used for verification purpose.',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Students',
    @level2type = N'COLUMN',
    @level2name = N'ConfirmationCode'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Expiry Date of Confirmation Code.',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Students',
    @level2type = N'COLUMN',
    @level2name = N'ConfirmationCodeExpiryDate'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'CNIC of Student',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Students',
    @level2type = N'COLUMN',
    @level2name = N'CNIC'
GO