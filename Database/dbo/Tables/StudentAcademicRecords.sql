﻿CREATE TABLE [dbo].[StudentAcademicRecords] (
    [StudentAcademicRecordID]        INT                IDENTITY (1, 1) NOT NULL,
    [StudentID]                      INT                NOT NULL,
    [DegreeType]                     [dbo].[Enum]       NOT NULL,
    [ObtainedMarks]                  FLOAT (53)         NOT NULL,
    [TotalMarks]                     FLOAT (53)         NOT NULL,
    [Percentage]                     FLOAT (53)         NOT NULL,
    [IsCGPA]                         BIT                NOT NULL,
    [Subjects]                       VARCHAR (500)      NOT NULL,
    [Institute]                      VARCHAR (500)      NOT NULL,
    [BoardUniversity]                VARCHAR (500)      NOT NULL,
    [PassingYear]                    SMALLINT           NOT NULL,
    [Status]                         [dbo].[Enum]       NULL,
    [Remarks]                        [dbo].[Remarks]    NULL,
    [SystemFolderID]                 UNIQUEIDENTIFIER   NULL,
    [VerificationStatus]             [dbo].[EnumFlags8] NULL,
    [VerifiedByUserLoginHistoryID]   INT                NULL,
    [VerificationDate]               DATETIME           NULL,
    [HECDegreeVerificationStampDate] DATE               NULL, 
    CONSTRAINT [PK_StudentAcademicRecords] PRIMARY KEY CLUSTERED ([StudentAcademicRecordID] ASC),
    CONSTRAINT [CK_StudentAcademicRecords ([ObtainedMarks]]<=[TotalMarks]])] CHECK ([ObtainedMarks]<=[TotalMarks]),
    CONSTRAINT [FK_StudentAcademicRecords_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [FK_StudentAcademicRecords_SystemFolders_SystemFolderID] FOREIGN KEY ([SystemFolderID]) REFERENCES [dbo].[SystemFolders] ([SystemFolderID]),
    CONSTRAINT [FK_StudentAcademicRecords_UserLoginHistory_VerifiedByUserLoginHistoryID] FOREIGN KEY ([VerifiedByUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID])
);

















GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'SSC, HSSC, etc',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'StudentAcademicRecords',
    @level2type = N'COLUMN',
    @level2name = N'DegreeType'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Documents Verified, Not Verified, Pending, etc',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'StudentAcademicRecords',
    @level2type = N'COLUMN',
    @level2name = N'Status'