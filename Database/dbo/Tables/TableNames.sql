﻿CREATE TABLE [dbo].[TableNames] (
    [TableID]     INT            NOT NULL,
    [TableSchema] NVARCHAR (128) NOT NULL,
    [TableName]   NVARCHAR (128) NOT NULL,
    [EntityName]  NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_TableNames] PRIMARY KEY CLUSTERED ([TableID] ASC),
    CONSTRAINT [UK_TableNames_TableSchema_TableName] UNIQUE NONCLUSTERED ([TableSchema] ASC, [TableName] ASC)
);







