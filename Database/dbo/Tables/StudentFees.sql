﻿CREATE TABLE [dbo].[StudentFees] (
    [StudentFeeID]                INT                 IDENTITY (1, 1) NOT NULL,
    [StudentID]                   INT                 NULL,
    [CandidateAppliedProgramID]   INT                 NULL,
    [SemesterID]                  [dbo].[SemesterID]  NOT NULL,
    [FeeType]                     [dbo].[Enum]        NOT NULL,
    [CreditHours]                 [dbo].[CreditHours] NOT NULL,
    [NoOfInstallments]            [dbo].[Enum]        NOT NULL,
    [NewAdmission]                BIT                 NOT NULL,
    [Remarks]                     [dbo].[Remarks]     NULL,
    [TotalAmount]                 INT                 NOT NULL,
    [ConcessionAmount]            INT                 NOT NULL,
    [GrandTotalAmount]            INT                 NOT NULL,
    [CreatedDate]                 DATETIME            NOT NULL,
    [CreatedByUserLoginHistoryID] INT                 NULL,
    [Status]                      [dbo].[Enum]        NOT NULL,
    [MigrationID]                 INT                 NULL,
    [ExemptWHT]                   BIT                 NOT NULL,
    CONSTRAINT [PK_StudentFees] PRIMARY KEY CLUSTERED ([StudentFeeID] ASC),
    CONSTRAINT [CK_StudentFees (([TotalAmount]]-[ConcessionAmount]])=[GrandTotalAmount]])] CHECK (([TotalAmount]-[ConcessionAmount])=[GrandTotalAmount]),
    CONSTRAINT [CK_StudentFees ([CandidateAppliedProgramID]] IS NOT NULL OR [StudentID]] IS NOT NULL)] CHECK ([CandidateAppliedProgramID] IS NOT NULL OR [StudentID] IS NOT NULL),
    CONSTRAINT [FK_StudentFees_CandidateAppliedPrograms_CandidateAppliedProgramID] FOREIGN KEY ([CandidateAppliedProgramID]) REFERENCES [dbo].[CandidateAppliedPrograms] ([CandidateAppliedProgramID]),
    CONSTRAINT [FK_StudentFees_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [FK_StudentFees_UserLoginHistory_CreatedByUserLoginHistoryID] FOREIGN KEY ([CreatedByUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID])
);
