﻿CREATE TABLE [dbo].[ExamMarkingSchemes] (
    [ExamMarkingSchemeID] INT           IDENTITY (1, 1) NOT NULL,
    [InstituteID]         INT           NOT NULL,
    [MarkingSchemeName]   VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_ExamMarkingSchemes] PRIMARY KEY CLUSTERED ([ExamMarkingSchemeID] ASC),
    CONSTRAINT [FK_ExamMarkingSchemes_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);








GO


