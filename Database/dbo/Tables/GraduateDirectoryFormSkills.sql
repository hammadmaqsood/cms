﻿CREATE TABLE [dbo].[GraduateDirectoryFormSkills] (
    [GraduateDirectorySkillID] INT            IDENTITY (1, 1) NOT NULL,
    [GraduateDirectoryFormID]  INT            NOT NULL,
    [Description]              VARCHAR (1000) NOT NULL,
    CONSTRAINT [PK_GraduateDirectoryFormSkills] PRIMARY KEY CLUSTERED ([GraduateDirectorySkillID] ASC),
    CONSTRAINT [FK_GraduateDirectoryFormSkills_GraduateDirectoryForms_GraduateDirectoryFormID] FOREIGN KEY ([GraduateDirectoryFormID]) REFERENCES [dbo].[GraduateDirectoryForms] ([GraduateDirectoryFormID])
);





