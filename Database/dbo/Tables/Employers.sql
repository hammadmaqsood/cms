﻿CREATE TABLE [dbo].[Employers] (
    [EmployerID]     INT                IDENTITY (1, 1) NOT NULL,
    [InstituteID]    INT                NOT NULL,
    [Name]           [dbo].[PersonName] NOT NULL,
    [Email]          [dbo].[Email]      NOT NULL,
    [CompanyName]    [dbo].[PersonName] NOT NULL,
    [CompanyAddress] [dbo].[Address]    NULL,
    CONSTRAINT [PK_Employers] PRIMARY KEY CLUSTERED ([EmployerID] ASC),
    CONSTRAINT [FK_Employers_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);

