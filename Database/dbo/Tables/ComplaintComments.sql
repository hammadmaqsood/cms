﻿CREATE TABLE [dbo].[ComplaintComments] (
    [ComplaintCommentID] UNIQUEIDENTIFIER NOT NULL,
    [ComplaintID]        UNIQUEIDENTIFIER NOT NULL,
    [UserLoginHistoryID] INT              NOT NULL,
    [Date]               DATETIME         NOT NULL,
    [Text]               VARCHAR (1000)   NOT NULL,
    [Type]               [dbo].[EnumGuid] NOT NULL,
    [Status]             [dbo].[EnumGuid] NULL,
    CONSTRAINT [PK_ComplaintComments] PRIMARY KEY CLUSTERED ([ComplaintCommentID] ASC),
    CONSTRAINT [FK_ComplaintComments_Complaints_ComplaintID] FOREIGN KEY ([ComplaintID]) REFERENCES [dbo].[Complaints] ([ComplaintID]),
    CONSTRAINT [FK_ComplaintComments_UserLoginHistory_UserLoginHistoryID] FOREIGN KEY ([UserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID])
);



