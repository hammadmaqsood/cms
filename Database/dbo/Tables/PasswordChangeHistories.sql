﻿CREATE TABLE [dbo].[PasswordChangeHistories] (
    [PasswordChangeHistoryID] INT              IDENTITY (1, 1) NOT NULL,
    [UserID]                  INT              NULL,
    [CandidateID]             INT              NULL,
    [StudentID]               INT              NULL,
    [StudentIDOfParent]       INT              NULL,
    [FacultyMemberID]         INT              NULL,
    [ChangedDate]             DATETIME         NOT NULL,
    [UserLoginHistoryID]      INT              NOT NULL,
    [ChangeReason]            [dbo].[Enum]     NOT NULL,
    [OldPassword]             [dbo].[Password] NULL,
    [NewPassword]             [dbo].[Password] NULL,
    CONSTRAINT [PK_PasswordChangeHistories] PRIMARY KEY CLUSTERED ([PasswordChangeHistoryID] ASC),
    CONSTRAINT [FK_PasswordChangeHistories_Candidates_CandidateID] FOREIGN KEY ([CandidateID]) REFERENCES [dbo].[Candidates] ([CandidateID]),
    CONSTRAINT [FK_PasswordChangeHistories_FacultyMembers_FacultyMemberID] FOREIGN KEY ([FacultyMemberID]) REFERENCES [dbo].[FacultyMembers] ([FacultyMemberID]),
    CONSTRAINT [FK_PasswordChangeHistories_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [FK_PasswordChangeHistories_Students_StudentIDOfParent] FOREIGN KEY ([StudentIDOfParent]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [FK_PasswordChangeHistories_UserLoginHistory_UserLoginHistoryID] FOREIGN KEY ([UserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID]),
    CONSTRAINT [FK_PasswordChangeHistories_Users_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);





