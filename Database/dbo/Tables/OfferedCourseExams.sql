﻿CREATE TABLE [dbo].[OfferedCourseExams] (
    [OfferedCourseExamID] INT               IDENTITY (1, 1) NOT NULL,
    [OfferedCourseID]     INT               NOT NULL,
    [ExamType]            [dbo].[Enum]      NOT NULL,
    [ExamDate]            DATE              NOT NULL,
    [ExamNo]              TINYINT           NOT NULL,
    [Important]           BIT               NOT NULL,
    [TotalMarks]          [dbo].[ExamMarks] NOT NULL,
    [Remarks]             VARCHAR (MAX)     NULL,
    [Locked]              BIT               NOT NULL,
    [LockedDate]          DATETIME          NULL,
    CONSTRAINT [PK_OfferedCourseExams] PRIMARY KEY CLUSTERED ([OfferedCourseExamID] ASC),
    CONSTRAINT [CK_OfferedCourseExams ([TotalMarks]]<>(0))] CHECK ([TotalMarks]<>(0)),
    CONSTRAINT [FK_OfferedCourseExams_OfferedCourses_OfferedCourseID] FOREIGN KEY ([OfferedCourseID]) REFERENCES [dbo].[OfferedCourses] ([OfferedCourseID])
);



















