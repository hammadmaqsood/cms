﻿CREATE TABLE [dbo].[QAStudentOnlineTeachingSurveys] (
    [QAStudentOnlineTeachingSurveyID] UNIQUEIDENTIFIER    NOT NULL,
    [StudentID]                       INT                 NOT NULL,
    [SemesterID]                      [dbo].[SemesterID]  NOT NULL,
    [SubmittedDate]                   DATETIME            NULL,
    [Q11WhatsappNo]                   [dbo].[Mobile]      NULL,
    [Q12]                             [dbo].[EnumFlags32] NULL,
    [Q13]                             [dbo].[EnumFlags32] NULL,
    [Q14]                             [dbo].[EnumFlags32] NULL,
    [Q15]                             VARCHAR (50)        NULL,
    [Q16]                             [dbo].[EnumFlags32] NULL,
    [Q17]                             [dbo].[EnumFlags32] NULL,
    [Q18]                             [dbo].[EnumFlags32] NULL,
    [Q19]                             [dbo].[EnumFlags32] NULL,
    [Q20]                             [dbo].[EnumFlags32] NULL,
    [Q21]                             [dbo].[EnumFlags32] NULL,
    [Q21Other]                        VARCHAR (1000)      NULL,
    CONSTRAINT [PK_QAStudentOnlineTeachingSurveys] PRIMARY KEY CLUSTERED ([QAStudentOnlineTeachingSurveyID] ASC),
    CONSTRAINT [FK_QAStudentOnlineTeachingSurveys_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID]),
    CONSTRAINT [FK_QAStudentOnlineTeachingSurveys_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID])
);

