﻿CREATE TABLE [dbo].[PgpThesisSupervisors] (
    [PgpThesisSupervisorID] INT                IDENTITY (1, 1) NOT NULL,
    [PgpThesiID]            INT                NOT NULL,
    [PgpSupervisorID]       INT                NOT NULL,
    [ThesisSupervisorType]  [dbo].[EnumFlags8] NOT NULL,
    [AssignedDate]          DATETIME           NOT NULL,
    [Status]                INT                NOT NULL,
    CONSTRAINT [PK_PgpThesisSupervisors] PRIMARY KEY CLUSTERED ([PgpThesisSupervisorID] ASC),
    CONSTRAINT [FK_PgpThesisSupervisors_PgpSupervisors_PgpSupervisorID] FOREIGN KEY ([PgpSupervisorID]) REFERENCES [dbo].[PgpSupervisors] ([PgpSupervisorID]),
    CONSTRAINT [FK_PgpThesisSupervisors_PgpThesis_PgpThesiID] FOREIGN KEY ([PgpThesiID]) REFERENCES [dbo].[PgpThesis] ([PgpThesiID])
);

