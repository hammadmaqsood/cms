﻿CREATE TABLE [dbo].[InstituteBankAccounts] (
    [InstituteBankAccountID] INT                IDENTITY (1, 1) NOT NULL,
    [InstituteID]            INT                NOT NULL,
    [BankID]                 INT                NOT NULL,
    [FeeType]                [dbo].[Enum]       NOT NULL,
    [AccountType]            [dbo].[EnumFlags8] NOT NULL,
    [AccountTitle]           VARCHAR (200)      NOT NULL,
    [AccountTitleForDisplay] VARCHAR (500)      NULL,
    [AccountNo]              VARCHAR (100)      NOT NULL,
    [AccountNoForDisplay]    VARCHAR (500)      NULL,
    [BranchCode]             VARCHAR (10)       NULL,
    [BranchName]             [dbo].[Address]    NULL,
    [BranchAddress]          [dbo].[Address]    NULL,
    [BranchForDisplay]       [dbo].[Address]    NULL,
    [Active]                 BIT                NOT NULL,
    CONSTRAINT [PK_InstituteBankAccounts] PRIMARY KEY CLUSTERED ([InstituteBankAccountID] ASC),
    CONSTRAINT [FK_InstituteBankAccounts_Banks_BankID] FOREIGN KEY ([BankID]) REFERENCES [dbo].[Banks] ([BankID]),
    CONSTRAINT [FK_InstituteBankAccounts_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);















