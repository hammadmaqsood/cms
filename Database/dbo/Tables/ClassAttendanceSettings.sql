﻿CREATE TABLE [dbo].[ClassAttendanceSettings] (
    [ClassAttendanceSettingID] INT                IDENTITY (1, 1) NOT NULL,
    [SemesterID]               [dbo].[SemesterID] NOT NULL,
    [ProgramID]                INT                NOT NULL,
    [FromDate]                 DATE               NOT NULL,
    [ToDate]                   DATE               NOT NULL,
    [CanMarkPastDays]          TINYINT            NOT NULL,
    [CanEditPastDays]          TINYINT            NOT NULL,
    [Status]                   [dbo].[Enum]       NOT NULL,
    CONSTRAINT [PK_ClassAttendanceSettings] PRIMARY KEY CLUSTERED ([ClassAttendanceSettingID] ASC),
    CONSTRAINT [CK_ClassAttendanceSettings ([FromDate]]<=[ToDate]])] CHECK ([FromDate]<=[ToDate]),
    CONSTRAINT [FK_ClassAttendanceSettings_Programs_ProgramID] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[Programs] ([ProgramID]),
    CONSTRAINT [FK_ClassAttendanceSettings_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID])
);





