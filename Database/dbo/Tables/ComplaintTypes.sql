﻿CREATE TABLE [dbo].[ComplaintTypes] (
    [ComplaintTypeID] [dbo].[EnumGuid] NOT NULL,
    [InstituteID]     INT              NOT NULL,
    [Name]            VARCHAR (50)     NOT NULL,
    CONSTRAINT [PK_ComplaintTypes] PRIMARY KEY CLUSTERED ([ComplaintTypeID] ASC),
    CONSTRAINT [FK_ComplaintTypes_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);

