﻿CREATE TABLE [dbo].[CourseRegistrationSettings] (
    [CourseRegistrationSettingID] INT                IDENTITY (1, 1) NOT NULL,
    [SemesterID]                  [dbo].[SemesterID] NOT NULL,
    [ProgramID]                   INT                NOT NULL,
    [Type]                        [dbo].[Enum]       NOT NULL,
    [FromDate]                    DATE               NOT NULL,
    [ToDate]                      DATE               NOT NULL,
    [Status]                      TINYINT            NOT NULL,
    CONSTRAINT [PK_CourseRegistrationSettings] PRIMARY KEY CLUSTERED ([CourseRegistrationSettingID] ASC),
    CONSTRAINT [CK_CourseRegistrationSettings ([FromDate]]<=[ToDate]])] CHECK ([FromDate]<=[ToDate]),
    CONSTRAINT [FK_CourseRegistrationSettings_Programs_ProgramID] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[Programs] ([ProgramID]),
    CONSTRAINT [FK_CourseRegistrationSettings_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID])
);







