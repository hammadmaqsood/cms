﻿CREATE TABLE [dbo].[CandidateForeignAcademicRecords] (
    [CandidateForeignAcademicRecordID] UNIQUEIDENTIFIER NOT NULL,
    [CandidateID]                      INT              NOT NULL,
    [DegreeName]                       VARCHAR (500)    NOT NULL,
    [ObtainedMarks]                    VARCHAR (100)    NOT NULL,
    [TotalMarks]                       VARCHAR (100)    NOT NULL,
    [Subjects]                         VARCHAR (500)    NOT NULL,
    [Institute]                        VARCHAR (500)    NOT NULL,
    [PassingYear]                      SMALLINT         NOT NULL,
    [SystemFileID]                     UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_CandidateForeignAcademicRecords] PRIMARY KEY CLUSTERED ([CandidateForeignAcademicRecordID] ASC),
    CONSTRAINT [FK_CandidateForeignAcademicRecords_Candidates_CandidateID] FOREIGN KEY ([CandidateID]) REFERENCES [dbo].[Candidates] ([CandidateID]),
    CONSTRAINT [FK_CandidateForeignAcademicRecords_SystemFiles_SystemFileID] FOREIGN KEY ([SystemFileID]) REFERENCES [dbo].[SystemFiles] ([SystemFileID])
);
