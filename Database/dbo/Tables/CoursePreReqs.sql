﻿CREATE TABLE [dbo].[CoursePreReqs] (
    [CoursePreReqID]  INT IDENTITY (1, 1) NOT NULL,
    [CourseID]        INT NOT NULL,
    [PreReqCourseID1] INT NOT NULL,
    [PreReqCourseID2] INT NULL,
    [PreReqCourseID3] INT NULL,
    [MigrationID]     INT NULL,
    CONSTRAINT [PK_CoursePreReqs] PRIMARY KEY CLUSTERED ([CoursePreReqID] ASC),
    CONSTRAINT [CK_CoursePreReqs ([CourseID]]<>[PreReqCourseID1]])] CHECK ([CourseID]<>[PreReqCourseID1]),
    CONSTRAINT [CK_CoursePreReqs ([CourseID]]<>[PreReqCourseID2]])] CHECK ([CourseID]<>[PreReqCourseID2]),
    CONSTRAINT [CK_CoursePreReqs ([CourseID]]<>[PreReqCourseID3]])] CHECK ([CourseID]<>[PreReqCourseID3]),
    CONSTRAINT [FK_CoursePreReqs_Courses_CourseID] FOREIGN KEY ([CourseID]) REFERENCES [dbo].[Courses] ([CourseID]),
    CONSTRAINT [FK_CoursePreReqs_Courses_PreReqCourseID1] FOREIGN KEY ([PreReqCourseID1]) REFERENCES [dbo].[Courses] ([CourseID]),
    CONSTRAINT [FK_CoursePreReqs_Courses_PreReqCourseID2] FOREIGN KEY ([PreReqCourseID2]) REFERENCES [dbo].[Courses] ([CourseID]),
    CONSTRAINT [FK_CoursePreReqs_Courses_PreReqCourseID3] FOREIGN KEY ([PreReqCourseID3]) REFERENCES [dbo].[Courses] ([CourseID])
);




