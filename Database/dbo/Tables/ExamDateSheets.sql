﻿CREATE TABLE [dbo].[ExamDateSheets] (
    [ExamDateSheetID]                     INT  IDENTITY (1, 1) NOT NULL,
    [OfferedCourseID]                     INT  NOT NULL,
    [ExamDateAndSessionID]                INT  NOT NULL,
    [QuestionPaperReceivedDate]           DATE NULL,
    [AnswerSheetsReceivedByExaminerDate]  DATE NULL,
    [AnswerSheetsReceivedByExaminerCount] INT  NULL,
    [AnswerSheetsReceivedByExamCellDate]  DATE NULL,
    [AnswerSheetsReceivedByExamCellCount] INT  NULL,
    CONSTRAINT [PK_ExamDateSheets] PRIMARY KEY CLUSTERED ([ExamDateSheetID] ASC),
    CONSTRAINT [FK_ExamDateSheets_ExamDateAndSessions_ExamDateAndSessionID] FOREIGN KEY ([ExamDateAndSessionID]) REFERENCES [dbo].[ExamDateAndSessions] ([ExamDateAndSessionID]),
    CONSTRAINT [FK_ExamDateSheets_OfferedCourses_OfferedCourseID] FOREIGN KEY ([OfferedCourseID]) REFERENCES [dbo].[OfferedCourses] ([OfferedCourseID]),
    CONSTRAINT [UK_ExamDateSheets_OfferedCourseID_ExamDateAndSessionID] UNIQUE NONCLUSTERED ([OfferedCourseID] ASC, [ExamDateAndSessionID] ASC)
);









