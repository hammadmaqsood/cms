﻿CREATE TABLE [dbo].[OfferedCourseAttendances] (
    [OfferedCourseAttendanceID] INT             IDENTITY (1, 1) NOT NULL,
    [OfferedCourseID]           INT             NOT NULL,
    [ClassDate]                 DATETIME        NOT NULL,
    [ClassType]                 [dbo].[Enum]    NOT NULL,
    [Hours]                     DECIMAL (2, 1)  NOT NULL,
    [RoomID]                    INT             NOT NULL,
    [Remarks]                   [dbo].[Remarks] NULL,
    [MarkedDate]                DATETIME        NOT NULL,
    [MarkedByUserID]            INT             NULL,
    [MigrationID]               INT             NULL,
    [TopicsCovered]             VARCHAR(2000)   NULL, 
    CONSTRAINT [PK_OfferedCourseAttendances] PRIMARY KEY CLUSTERED ([OfferedCourseAttendanceID] ASC),
    CONSTRAINT [FK_OfferedCourseAttendances_OfferedCourses_OfferedCourseID] FOREIGN KEY ([OfferedCourseID]) REFERENCES [dbo].[OfferedCourses] ([OfferedCourseID]),
    CONSTRAINT [FK_OfferedCourseAttendances_Rooms_RoomID] FOREIGN KEY ([RoomID]) REFERENCES [dbo].[Rooms] ([RoomID])
);










GO
CREATE NONCLUSTERED INDEX [IX_OfferedCourseAttendances_OfferedCourseID]
    ON [dbo].[OfferedCourseAttendances]([OfferedCourseID] ASC);

