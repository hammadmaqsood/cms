﻿CREATE TABLE [dbo].[Rooms] (
    [RoomID]              INT          IDENTITY (1, 1) NOT NULL,
    [InstituteBuildingID] INT          NOT NULL,
    [RoomName]            VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Rooms] PRIMARY KEY CLUSTERED ([RoomID] ASC),
    CONSTRAINT [FK_Rooms_InstituteBuildings_InstituteBuildingID] FOREIGN KEY ([InstituteBuildingID]) REFERENCES [dbo].[InstituteBuildings] ([InstituteBuildingID])
);



