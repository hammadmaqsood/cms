﻿CREATE TABLE [dbo].[CBTSessionLabs] (
    [CBTSessionLabID] INT     IDENTITY (1, 1) NOT NULL,
    [CBTSessionID]    INT     NOT NULL,
    [CBTLabID]        INT     NOT NULL,
    [Computers]       TINYINT NOT NULL,
    [Sequence]        TINYINT NOT NULL,
    CONSTRAINT [PK_CBTSessionLabs] PRIMARY KEY CLUSTERED ([CBTSessionLabID] ASC),
    CONSTRAINT [FK_CBTSessionLabs_CBTLabs_CBTLabID] FOREIGN KEY ([CBTLabID]) REFERENCES [dbo].[CBTLabs] ([CBTLabID]),
    CONSTRAINT [FK_CBTSessionLabs_CBTSessions_CBTSessionID] FOREIGN KEY ([CBTSessionID]) REFERENCES [dbo].[CBTSessions] ([CBTSessionID]),
    CONSTRAINT [UK_CBTSessionLabs_CBTSessionID_CBTLabID] UNIQUE NONCLUSTERED ([CBTSessionID] ASC, [CBTLabID] ASC)
);





