﻿CREATE TABLE [dbo].[RequestedCourses] (
    [RequestedCourseID] INT  IDENTITY (1, 1) NOT NULL,
    [StudentID]         INT  NOT NULL,
    [OfferedCourseID]   INT  NOT NULL,
    [RequestDate]       DATE NOT NULL,
    CONSTRAINT [PK_RequestedCourses] PRIMARY KEY CLUSTERED ([RequestedCourseID] ASC),
    CONSTRAINT [FK_RequestedCourses_OfferedCourses_OfferedCourseID] FOREIGN KEY ([OfferedCourseID]) REFERENCES [dbo].[OfferedCourses] ([OfferedCourseID]),
    CONSTRAINT [FK_RequestedCourses_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID])
);









