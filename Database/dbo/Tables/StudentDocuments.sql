﻿CREATE TABLE [dbo].[StudentDocuments] (
    [StudentDocumentID] UNIQUEIDENTIFIER NOT NULL,
    [StudentID]         INT              NOT NULL,
    [SystemFileID]      UNIQUEIDENTIFIER NOT NULL,
    [DocumentType]      [dbo].[EnumGuid] NOT NULL,
    [Page]              [dbo].[EnumGuid] NOT NULL,
    [ImageWidthPixels]  INT              NOT NULL,
    [ImageHeightPixels] INT              NOT NULL,
    [ImageDpiX]         FLOAT (53)       NOT NULL,
    [ImageDpiY]         FLOAT (53)       NOT NULL,
    [Remarks]           [dbo].[Remarks]  NULL,
    [Status]            [dbo].[EnumGuid] NOT NULL,
    CONSTRAINT [PK_StudentDocuments] PRIMARY KEY CLUSTERED ([StudentDocumentID] ASC),
    CONSTRAINT [FK_StudentDocuments_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [FK_StudentDocuments_SystemFiles_SystemFileID] FOREIGN KEY ([SystemFileID]) REFERENCES [dbo].[SystemFiles] ([SystemFileID])
);