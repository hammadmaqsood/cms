﻿CREATE TABLE [dbo].[CandidateAppliedProgramAcademicRecords] (
    [CandidateAppliedProgramAcademicRecordID] INT           IDENTITY (1, 1) NOT NULL,
    [CandidateAppliedProgramID]               INT           NOT NULL,
    [AdmissionCriteriaPreReqDegreeID]         INT           NOT NULL,
    [DegreeType]                              [dbo].[Enum]  NOT NULL,
    [ObtainedMarks]                           FLOAT (53)    NOT NULL,
    [TotalMarks]                              FLOAT (53)    NOT NULL,
    [Percentage]                              FLOAT (53)    NOT NULL,
    [IsCGPA]                                  BIT           NOT NULL,
    [Subjects]                                VARCHAR (500) NOT NULL,
    [Institute]                               VARCHAR (500) NOT NULL,
    [BoardUniversity]                         VARCHAR (500) NOT NULL,
    [PassingYear]                             SMALLINT      NOT NULL,
    CONSTRAINT [PK_CandidateAppliedProgramAcademicRecords] PRIMARY KEY CLUSTERED ([CandidateAppliedProgramAcademicRecordID] ASC),
    CONSTRAINT [FK_CandidateAppliedProgramAcademicRecords_AdmissionCriteriaPreReqDegrees_AdmissionCriteriaPreReqDegreeID] FOREIGN KEY ([AdmissionCriteriaPreReqDegreeID]) REFERENCES [dbo].[AdmissionCriteriaPreReqDegrees] ([AdmissionCriteriaPreReqDegreeID]),
    CONSTRAINT [FK_CandidateAppliedProgramAcademicRecords_CandidateAppliedPrograms_CandidateAppliedProgramID] FOREIGN KEY ([CandidateAppliedProgramID]) REFERENCES [dbo].[CandidateAppliedPrograms] ([CandidateAppliedProgramID])
);






