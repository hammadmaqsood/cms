﻿CREATE TABLE [dbo].[GraduateDirectoryForms] (
    [GraduateDirectoryFormID] INT                IDENTITY (1, 1) NOT NULL,
    [StudentID]               INT                NOT NULL,
    [SemesterID]              [dbo].[SemesterID] NOT NULL,
    [Submitted]               DATETIME           NULL,
    [ProgramMajorID]          INT                NULL,
    CONSTRAINT [PK_GraduateDirectoryForms] PRIMARY KEY CLUSTERED ([GraduateDirectoryFormID] ASC),
    CONSTRAINT [FK_GraduateDirectoryForms_ProgramMajors_ProgramMajorID] FOREIGN KEY ([ProgramMajorID]) REFERENCES [dbo].[ProgramMajors] ([ProgramMajorID]),
    CONSTRAINT [FK_GraduateDirectoryForms_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID]),
    CONSTRAINT [FK_GraduateDirectoryForms_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID])
);







