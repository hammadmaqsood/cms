﻿CREATE TABLE [dbo].[DisciplineCaseActivities] (
    [DisciplineCaseActivityID] INT           IDENTITY (1, 1) NOT NULL,
    [InstituteID]              INT           NOT NULL,
    [ActivityName]             VARCHAR (100) NOT NULL,
    [FineAmount]               INT           NULL,
    CONSTRAINT [PK_DisciplineCaseActivities] PRIMARY KEY CLUSTERED ([DisciplineCaseActivityID] ASC),
    CONSTRAINT [FK_DisciplineCaseActivities_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);


