﻿CREATE TABLE [dbo].[Users] (
    [UserID]                     INT                     IDENTITY (1, 1) NOT NULL,
    [Username]                   [dbo].[Email]           NOT NULL,
    [Name]                       [dbo].[PersonName]      NOT NULL,
    [Password]                   [dbo].[EncryptedHashed] NOT NULL,
    [Email]                      [dbo].[Email]           NOT NULL,
    [ConfirmationCode]           UNIQUEIDENTIFIER        NULL,
    [ConfirmationCodeExpiryDate] DATETIME                NULL,
    [PasswordChangeOnNextLogin]  BIT                     NULL,
    [PasswordChangeOn]           DATETIME                NULL,
    [Mobile]                     VARCHAR (50)            NULL,
    [JobTitle]                   VARCHAR (50)            NULL,
    [CreatedOn]                  DATETIME                NULL,
    [CreatedByUserID]            INT                     NULL,
    [AccountActivatedByUser]     BIT                     NOT NULL,
    [AccountLocked]              BIT                     NOT NULL,
    [AccountLockedDate]          DATETIME                NULL,
    [Status]                     [dbo].[Enum]            NOT NULL,
    [IsDeleted]                  BIT                     NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([UserID] ASC)
);






GO
