﻿CREATE TABLE [dbo].[ExamMarksSettings] (
    [ExamMarksSettingID] INT                IDENTITY (1, 1) NOT NULL,
    [SemesterID]         [dbo].[SemesterID] NOT NULL,
    [ProgramID]          INT                NOT NULL,
    [FromDate]           DATE               NOT NULL,
    [ToDate]             DATE               NOT NULL,
    [ExamMarksTypes]     [dbo].[EnumFlags8] NOT NULL,
    [Status]             TINYINT            NOT NULL,
    CONSTRAINT [PK_ExamMarksSettings] PRIMARY KEY CLUSTERED ([ExamMarksSettingID] ASC),
    CONSTRAINT [FK_ExamMarksSettings_Programs_ProgramID] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[Programs] ([ProgramID]),
    CONSTRAINT [FK_ExamMarksSettings_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID]),
    CONSTRAINT [UK_ExamMarksSettings_ProgramID_SemesterID] UNIQUE NONCLUSTERED ([ProgramID] ASC, [SemesterID] ASC)
);









