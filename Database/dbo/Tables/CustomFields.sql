﻿CREATE TABLE [dbo].[CustomFields] (
    [CustomFieldID]        INT              IDENTITY (1, 1) NOT NULL,
    [CustomFieldGUID]      UNIQUEIDENTIFIER NOT NULL,
    [InstituteID]          INT              NULL,
    [Label]                VARCHAR (100)    NOT NULL,
    [FieldType]            [dbo].[Enum]     NOT NULL,
    [ValidationExpression] VARCHAR (1000)   NULL,
    [Status]               [dbo].[Enum]     NOT NULL,
    [RelationshipType]     [dbo].[Enum]     NOT NULL,
    [Readonly]             BIT              NULL,
    [MaxLength]            INT              NULL,
    [DisplayIndex]         INT              NOT NULL,
    CONSTRAINT [PK_CustomFields] PRIMARY KEY CLUSTERED ([CustomFieldID] ASC),
    CONSTRAINT [FK_CustomFields_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID]),
    CONSTRAINT [UK_CustomFields_CustomFieldGUID] UNIQUE NONCLUSTERED ([CustomFieldGUID] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Custom Fields for student table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CustomFields';

