﻿CREATE TABLE [dbo].[Complaints] (
    [ComplaintID]      UNIQUEIDENTIFIER NOT NULL,
    [ComplaintTypeID]  UNIQUEIDENTIFIER NOT NULL,
    [StudentID]        INT              NOT NULL,
    [InstituteID]      INT              NOT NULL,
    [ComplaintNo]      VARCHAR (50)     NOT NULL,
    [Description]      NVARCHAR (MAX)   NOT NULL,
    [RegisteredDate]   DATETIME         NOT NULL,
    [Status]           [dbo].[EnumGuid] NOT NULL,
    [StatusChangeDate] DATETIME         NOT NULL,
    CONSTRAINT [PK_Complaints] PRIMARY KEY CLUSTERED ([ComplaintID] ASC),
    CONSTRAINT [FK_Complaints_ComplaintTypes_ComplaintTypeID] FOREIGN KEY ([ComplaintTypeID]) REFERENCES [dbo].[ComplaintTypes] ([ComplaintTypeID]),
    CONSTRAINT [FK_Complaints_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID]),
    CONSTRAINT [FK_Complaints_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID])
);

