﻿CREATE TABLE [dbo].[AdmissionCriteriaPreReqDegrees] (
    [AdmissionCriteriaPreReqDegreeID]   INT          IDENTITY (1, 1) NOT NULL,
    [AdmissionCriteriaPreReqID]         INT          NOT NULL,
    [DegreeType]                        [dbo].[Enum] NOT NULL,
    [AnnualExamSystem]                  BIT          NOT NULL,
    [SemesterSystem]                    BIT          NOT NULL,
    [AnnualExamSystemMinimumPercentage] INT          NULL,
    [SemesterSystemMinCGPAOutOf4]       FLOAT (53)   NULL,
    [SemesterSystemMinCGPAOutOf5]       FLOAT (53)   NULL,
    [DisplaySequence]                   TINYINT      NOT NULL,
    [MeritListWeightage]                TINYINT      NOT NULL,
    CONSTRAINT [PK_AdmissionCriteriaPreReqDegrees] PRIMARY KEY CLUSTERED ([AdmissionCriteriaPreReqDegreeID] ASC),
    CONSTRAINT [FK_AdmissionCriteriaPreReqDegrees_AdmissionCriteriaPreReqs_AdmissionCriteriaPreReqID] FOREIGN KEY ([AdmissionCriteriaPreReqID]) REFERENCES [dbo].[AdmissionCriteriaPreReqs] ([AdmissionCriteriaPreReqID])
);




