﻿CREATE TABLE [dbo].[AdmissionOpenProgramEquivalents] (
    [AdmissionOpenProgramEquivalentID] INT IDENTITY (1, 1) NOT NULL,
    [AdmissionOpenProgramID]           INT NOT NULL,
    [EquivalentAdmissionOpenProgramID] INT NOT NULL,
    CONSTRAINT [PK_AdmissionOpenProgramEquivalents] PRIMARY KEY CLUSTERED ([AdmissionOpenProgramEquivalentID] ASC),
    CONSTRAINT [FK_AdmissionOpenProgramEquivalents_AdmissionOpenPrograms_AdmissionOpenProgramID] FOREIGN KEY ([AdmissionOpenProgramID]) REFERENCES [dbo].[AdmissionOpenPrograms] ([AdmissionOpenProgramID]),
    CONSTRAINT [FK_AdmissionOpenProgramEquivalents_AdmissionOpenPrograms_EquivalentAdmissionOpenProgramID] FOREIGN KEY ([EquivalentAdmissionOpenProgramID]) REFERENCES [dbo].[AdmissionOpenPrograms] ([AdmissionOpenProgramID]),
    CONSTRAINT [UK_AdmissionOpenProgramEquivalents_AdmissionOpenProgramID_EquivalentAdmissionOpenProgramID] UNIQUE NONCLUSTERED ([AdmissionOpenProgramID] ASC, [EquivalentAdmissionOpenProgramID] ASC)
);








