﻿CREATE TABLE [dbo].[Semesters] (
    [SemesterID]   [dbo].[SemesterID] NOT NULL,
    [Year]         SMALLINT           NOT NULL,
    [SemesterType] [dbo].[Enum]       NOT NULL,
    [Visible]      BIT                NOT NULL,
    CONSTRAINT [PK_Semesters] PRIMARY KEY CLUSTERED ([SemesterID] ASC),
    CONSTRAINT [CK_Semesters ([SemesterID]]=([Year]]*(10)+[SemesterType]]))] CHECK ([SemesterID]=([Year]*(10)+[SemesterType]))
);







GO
CREATE NONCLUSTERED INDEX [IX_Semesters_Visible]
    ON [dbo].[Semesters]([Visible] ASC)
    INCLUDE([SemesterID], [Year], [SemesterType]);

