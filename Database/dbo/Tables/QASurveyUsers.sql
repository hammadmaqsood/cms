﻿CREATE TABLE [dbo].[QASurveyUsers] (
    [QASurveyUserID]    INT              IDENTITY (1, 1) NOT NULL,
    [QASurveyConductID] INT              NOT NULL,
    [Code]              UNIQUEIDENTIFIER NOT NULL,
    [FacultyMemberID]   INT              NULL,
    [StudentID]         INT              NULL,
    [AlumniStudentID]   INT              NULL,
    [EmployerID]        INT              NULL,
    [OfferedCourseID]   INT              NULL,
    [SurveyValues]      VARCHAR (MAX)    NULL,
    [SubmissionDate]    DATETIME         NULL,
    [Status]            [dbo].[Enum]     NOT NULL,
    CONSTRAINT [PK_QASurveyUsers] PRIMARY KEY CLUSTERED ([QASurveyUserID] ASC),
    CONSTRAINT [FK_QASurveyUsers_Employers_EmployerID] FOREIGN KEY ([EmployerID]) REFERENCES [dbo].[Employers] ([EmployerID]),
    CONSTRAINT [FK_QASurveyUsers_FacultyMembers_FacultyMemberID] FOREIGN KEY ([FacultyMemberID]) REFERENCES [dbo].[FacultyMembers] ([FacultyMemberID]),
    CONSTRAINT [FK_QASurveyUsers_OfferedCourses_OfferedCourseID] FOREIGN KEY ([OfferedCourseID]) REFERENCES [dbo].[OfferedCourses] ([OfferedCourseID]),
    CONSTRAINT [FK_QASurveyUsers_QASurveyConducts_QASurveyConductID] FOREIGN KEY ([QASurveyConductID]) REFERENCES [dbo].[QASurveyConducts] ([QASurveyConductID]),
    CONSTRAINT [FK_QASurveyUsers_Students_AlumniStudentID] FOREIGN KEY ([AlumniStudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [FK_QASurveyUsers_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [UK_QASurveyUsers_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);

