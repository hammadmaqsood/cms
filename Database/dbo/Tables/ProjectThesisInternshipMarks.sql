﻿CREATE TABLE [dbo].[ProjectThesisInternshipMarks] (
    [ProjectThesisInternshipMarkID] UNIQUEIDENTIFIER  NOT NULL,
    [ProjectThesisInternshipID]     UNIQUEIDENTIFIER  NOT NULL,
    [DisplayIndex]                  TINYINT           NOT NULL,
    [MarksTitle]                    VARCHAR (500)     NOT NULL,
    [TotalMarks]                    [dbo].[ExamMarks] NOT NULL,
    [ObtainedMarks]                 [dbo].[ExamMarks] NOT NULL,
    CONSTRAINT [PK_ProjectThesisInternshipMarks] PRIMARY KEY CLUSTERED ([ProjectThesisInternshipMarkID] ASC),
    CONSTRAINT [FK_ProjectThesisInternshipMarks_ProjectThesisInternships_ProjectThesisInternshipID] FOREIGN KEY ([ProjectThesisInternshipID]) REFERENCES [dbo].[ProjectThesisInternships] ([ProjectThesisInternshipID]),
    CONSTRAINT [UK_ProjectThesisInternshipMarks_ProjectThesisInternshipID_DisplayIndex] UNIQUE NONCLUSTERED ([ProjectThesisInternshipID] ASC, [DisplayIndex] ASC)
);

