﻿CREATE TABLE [dbo].[UserFeedbackAssignees] (
    [UserFeedbackAssigneeID] UNIQUEIDENTIFIER NOT NULL,
    [UserFeedbackID]         UNIQUEIDENTIFIER NOT NULL,
    [UserFeedbackMemberID]   UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_UserFeedbackAssignees] PRIMARY KEY CLUSTERED ([UserFeedbackAssigneeID] ASC),
    CONSTRAINT [FK_UserFeedbackAssignees_UserFeedbackMembers_UserFeedbackMemberID] FOREIGN KEY ([UserFeedbackMemberID]) REFERENCES [dbo].[UserFeedbackMembers] ([UserFeedbackMemberID]),
    CONSTRAINT [FK_UserFeedbackAssignees_UserFeedbacks_UserFeedbackID] FOREIGN KEY ([UserFeedbackID]) REFERENCES [dbo].[UserFeedbacks] ([UserFeedbackID])
);

