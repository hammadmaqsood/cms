﻿CREATE TABLE [dbo].[OfferedCourseClasses] (
    [OfferedCourseClassID] INT                IDENTITY (1, 1) NOT NULL,
    [OfferedCourseID]      INT                NOT NULL,
    [SemesterNo]           [dbo].[SemesterNo] NOT NULL,
    [Section]              [dbo].[Section]    NOT NULL,
    [Shift]                [dbo].[Shift]      NOT NULL,
    [SpecialOffered]       BIT                NOT NULL,
    CONSTRAINT [PK_OfferedCourseClasses] PRIMARY KEY CLUSTERED ([OfferedCourseClassID] ASC),
    CONSTRAINT [FK_OfferedCourseClasses_OfferedCourses_OfferedCourseID] FOREIGN KEY ([OfferedCourseID]) REFERENCES [dbo].[OfferedCourses] ([OfferedCourseID])
);





