﻿CREATE TABLE [dbo].[PgpThesisTitles] (
    [PgpThesisTitleID] INT            IDENTITY (1, 1) NOT NULL,
    [PgpThesiID]       INT            NOT NULL,
    [ThesisTitle]      VARCHAR (1000) NOT NULL,
    [CreatedDate]      DATETIME       NOT NULL,
    [Status]           [dbo].[Enum]   NOT NULL,
    CONSTRAINT [PK_PgpThesisTitles] PRIMARY KEY CLUSTERED ([PgpThesisTitleID] ASC),
    CONSTRAINT [FK_PgpThesisTitles_PgpThesis_PgpThesiID] FOREIGN KEY ([PgpThesiID]) REFERENCES [dbo].[PgpThesis] ([PgpThesiID])
);

