﻿CREATE TABLE [dbo].[CBTLabs] (
    [CBTLabID]    INT           IDENTITY (1, 1) NOT NULL,
    [InstituteID] INT           NOT NULL,
    [TestCentreName] [dbo].[Address] NOT NULL, 
    [LabName]     VARCHAR (200) NOT NULL,
    [Computers]   TINYINT       NOT NULL,
    [Status]      [dbo].[Enum]  NOT NULL,
    CONSTRAINT [PK_CBTLabs] PRIMARY KEY CLUSTERED ([CBTLabID] ASC),
    CONSTRAINT [FK_CBTLabs_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID]),
    CONSTRAINT [UK_CBTLabs_InstituteID_LabName] UNIQUE NONCLUSTERED ([InstituteID] ASC, [LabName] ASC)
);







