﻿CREATE TABLE [dbo].[AdmissionCriteriaPreReqs] (
    [AdmissionCriteriaPreReqID]       INT           IDENTITY (1, 1) NOT NULL,
    [AdmissionCriteriaID]             INT           NOT NULL,
    [PreReqQualification]             VARCHAR (100) NOT NULL,
    [PreReqQualificationResultStatus] VARCHAR (100) NOT NULL,
    [ResultAwaitedDegreeType]         TINYINT       NULL,
    CONSTRAINT [PK_AdmissionCriteriaPreReqs] PRIMARY KEY CLUSTERED ([AdmissionCriteriaPreReqID] ASC),
    CONSTRAINT [FK_AdmissionCriteriaPreReqs_AdmissionCriteria_AdmissionCriteriaID] FOREIGN KEY ([AdmissionCriteriaID]) REFERENCES [dbo].[AdmissionCriteria] ([AdmissionCriteriaID])
);


