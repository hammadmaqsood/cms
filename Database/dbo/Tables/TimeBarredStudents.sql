﻿CREATE TABLE [dbo].[TimeBarredStudents] (
    [TimeBarredStudentID]     INT                 IDENTITY (1, 1) NOT NULL,
    [StudentID]               INT                 NOT NULL,
    [MinSemesterID]           [dbo].[SemesterID]  NOT NULL,
    [MaxSemesterID]           [dbo].[SemesterID]  NOT NULL,
    [CourseCategoriesAllowed] [dbo].[EnumFlags8]  NOT NULL,
    [MaxCreditHours]          [dbo].[CreditHours] NULL,
    [Remarks]                 [dbo].[Remarks]     NOT NULL,
    CONSTRAINT [PK_TimeBarredStudents] PRIMARY KEY CLUSTERED ([TimeBarredStudentID] ASC),
    CONSTRAINT [FK_TimeBarredStudents_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID])
);







