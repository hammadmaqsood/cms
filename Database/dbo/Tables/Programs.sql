﻿CREATE TABLE [dbo].[Programs] (
    [ProgramID]                        INT               IDENTITY (1, 1) NOT NULL,
    [InstituteID]                      INT               NOT NULL,
    [DepartmentID]                     INT               NULL,
    [ProgramName]                      [dbo].[Name]      NOT NULL,
    [ProgramShortName]                 [dbo].[ShortName] NOT NULL,
    [ProgramAlias]                     [dbo].[Alias]     NOT NULL,
    [Duration]                         [dbo].[Enum]      NOT NULL,
    [ProgramCode]                      VARCHAR (10)      NULL,
    [DegreeLevel]                      [dbo].[Enum]      NOT NULL,
    [ProgramType]                      [dbo].[Enum]      NOT NULL,
    [Status]                           [dbo].[Enum]      NOT NULL,
    [DefaultExamMarksGradingSchemeID]  INT               NULL,
    [DefaultExamMarkingSchemeID]       INT               NULL,
    [MigrationID]                      VARCHAR (50)      NULL,
    [DefaultExamMarksPolicyID]         INT               NULL,
    [InitiatedDate]                    DATE              NULL,
    [DefaultExamRemarksPolicyID]       INT               NULL,
    [MaxAllowedCreditHoursPerSemester] TINYINT           NOT NULL,
    [MaxAllowedCoursesPerSemester]     TINYINT           NOT NULL,
    CONSTRAINT [PK_Programs] PRIMARY KEY CLUSTERED ([ProgramID] ASC),
    CONSTRAINT [FK_Programs_Departments_DepartmentID] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Departments] ([DepartmentID]),
    CONSTRAINT [FK_Programs_ExamMarkingSchemes_DefaultExamMarkingSchemeID] FOREIGN KEY ([DefaultExamMarkingSchemeID]) REFERENCES [dbo].[ExamMarkingSchemes] ([ExamMarkingSchemeID]),
    CONSTRAINT [FK_Programs_ExamMarksGradingSchemes_DefaultExamMarksGradingSchemeID] FOREIGN KEY ([DefaultExamMarksGradingSchemeID]) REFERENCES [dbo].[ExamMarksGradingSchemes] ([ExamMarksGradingSchemeID]),
    CONSTRAINT [FK_Programs_ExamMarksPolicies_DefaultExamMarksPolicyID] FOREIGN KEY ([DefaultExamMarksPolicyID]) REFERENCES [dbo].[ExamMarksPolicies] ([ExamMarksPolicyID]),
    CONSTRAINT [FK_Programs_ExamRemarksPolicies_DefaultExamRemarksPolicyID] FOREIGN KEY ([DefaultExamRemarksPolicyID]) REFERENCES [dbo].[ExamRemarksPolicies] ([ExamRemarksPolicyID]),
    CONSTRAINT [FK_Programs_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);














GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Undergraduate, Masters, Postgraduate, Doctrate', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Programs', @level2type = N'COLUMN', @level2name = N'DegreeLevel';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Duration in Years', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Programs', @level2type = N'COLUMN', @level2name = 'Duration';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Active, Inactive', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Programs', @level2type = N'COLUMN', @level2name = N'Status';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Degree, Short Course', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Programs', @level2type = N'COLUMN', @level2name = N'ProgramType';
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Used in Enrollment Generation',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Programs',
    @level2type = N'COLUMN',
    @level2name = N'ProgramCode'
GO
