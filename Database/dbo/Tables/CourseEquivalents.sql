﻿CREATE TABLE [dbo].[CourseEquivalents] (
    [CourseEquivalentID] INT             IDENTITY (1, 1) NOT NULL,
    [CourseID]           INT             NOT NULL,
    [EquivalentCourseID] INT             NOT NULL,
    [Remarks]            [dbo].[Remarks] NULL,
    CONSTRAINT [PK_CourseEquivalents] PRIMARY KEY CLUSTERED ([CourseEquivalentID] ASC),
    CONSTRAINT [FK_CourseEquivalents_Courses_CourseID] FOREIGN KEY ([CourseID]) REFERENCES [dbo].[Courses] ([CourseID]),
    CONSTRAINT [FK_CourseEquivalents_Courses_EquivalentCourseID] FOREIGN KEY ([EquivalentCourseID]) REFERENCES [dbo].[Courses] ([CourseID]),
    CONSTRAINT [UK_CourseEquivalents_CourseID_EquivalentCourseID] UNIQUE NONCLUSTERED ([CourseID] ASC, [EquivalentCourseID] ASC)
);








