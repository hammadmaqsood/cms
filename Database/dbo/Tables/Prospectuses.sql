﻿CREATE TABLE [dbo].[Prospectuses] (
    [ProspectusID]           UNIQUEIDENTIFIER   NOT NULL,
    [SemesterID]             [dbo].[SemesterID] NOT NULL,
    [ProspectusSystemFileID] UNIQUEIDENTIFIER   NULL,
    CONSTRAINT [PK_Prospectuses] PRIMARY KEY CLUSTERED ([ProspectusID] ASC),
    CONSTRAINT [FK_Prospectuses_SystemFiles_ProspectusSystemFileID] FOREIGN KEY ([ProspectusSystemFileID]) REFERENCES [dbo].[SystemFiles] ([SystemFileID])
);

