﻿CREATE TABLE [dbo].[FeeConcessionTypes] (
    [FeeConcessionTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [InstituteID]         INT           NOT NULL,
    [ConcessionName]      VARCHAR (250) NOT NULL,
    [Visible]             BIT           NOT NULL,
    CONSTRAINT [PK_FeeConcessionTypes] PRIMARY KEY CLUSTERED ([FeeConcessionTypeID] ASC),
    CONSTRAINT [FK_FeeConcessionTypes_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);







