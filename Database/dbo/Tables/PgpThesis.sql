﻿CREATE TABLE [dbo].[PgpThesis] (
    [PgpThesiID]     INT                 IDENTITY (1, 1) NOT NULL,
    [StudentID]      INT                 NOT NULL,
    [RegisteredDate] DATETIME            NOT NULL,
    [Status]         [dbo].[EnumFlags32] NOT NULL,
    CONSTRAINT [PK_PgpThesis] PRIMARY KEY CLUSTERED ([PgpThesiID] ASC),
    CONSTRAINT [FK_PgpThesis_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID])
);

