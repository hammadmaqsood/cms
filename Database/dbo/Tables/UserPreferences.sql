﻿CREATE TABLE [dbo].[UserPreferences] (
    [UserPreferenceID] INT              IDENTITY (1, 1) NOT NULL,
    [UserRoleID]       INT              NULL,
    [StudentID]        INT              NULL,
    [FacultyMemberID]  INT              NULL,
    [SettingGuid]      UNIQUEIDENTIFIER NOT NULL,
    [SettingValue]     VARCHAR (1000)   NULL,
    CONSTRAINT [PK_UserPreferences] PRIMARY KEY CLUSTERED ([UserPreferenceID] ASC),
    CONSTRAINT [FK_UserPreferences_FacultyMembers_FacultyMemberID] FOREIGN KEY ([FacultyMemberID]) REFERENCES [dbo].[FacultyMembers] ([FacultyMemberID]),
    CONSTRAINT [FK_UserPreferences_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [FK_UserPreferences_UserRoles_UserRoleID] FOREIGN KEY ([UserRoleID]) REFERENCES [dbo].[UserRoles] ([UserRoleID])
);

