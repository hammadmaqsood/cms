﻿CREATE TABLE [dbo].[FacultyMemberPinCodes] (
    [FacultyMemberPinCodeID] INT              IDENTITY (1, 1) NOT NULL,
    [FacultyMemberID]        INT              NOT NULL,
    [PinCode]                UNIQUEIDENTIFIER NOT NULL,
    [ExpiryDate]             DATETIME         NOT NULL,
    [UserLoginHistoryID]     INT              NULL,
    CONSTRAINT [PK_FacultyMemberPinCodes] PRIMARY KEY CLUSTERED ([FacultyMemberPinCodeID] ASC),
    CONSTRAINT [FK_FacultyMemberPinCodes_FacultyMembers_FacultyMemberID] FOREIGN KEY ([FacultyMemberID]) REFERENCES [dbo].[FacultyMembers] ([FacultyMemberID]),
    CONSTRAINT [FK_FacultyMemberPinCodes_UserLoginHistory_UserLoginHistoryID] FOREIGN KEY ([UserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID]),
    CONSTRAINT [UK_FacultyMemberPinCodes_FacultyMemberID_PinCode] UNIQUE NONCLUSTERED ([FacultyMemberID] ASC, [PinCode] ASC)
);





