﻿CREATE TABLE [dbo].[ExamBuildings] (
    [ExamBuildingID] INT           IDENTITY (1, 1) NOT NULL,
    [InstituteID]    INT           NOT NULL,
    [BuildingName]   VARCHAR (100) NOT NULL,
    [Status]         [dbo].[Enum]  NOT NULL,
    CONSTRAINT [PK_ExamBuildings] PRIMARY KEY CLUSTERED ([ExamBuildingID] ASC),
    CONSTRAINT [FK_ExamBuildings_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID]),
    CONSTRAINT [UK_ExamBuildings_InstituteID_BuildingName] UNIQUE NONCLUSTERED ([InstituteID] ASC, [BuildingName] ASC)
);







