﻿CREATE TABLE [dbo].[ProgramResearchThemes] (
    [ProgramResearchThemeID] UNIQUEIDENTIFIER NOT NULL,
    [ProgramID]              INT              NOT NULL,
    [ResearchThemeID]        UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_ProgramResearchThemes] PRIMARY KEY CLUSTERED ([ProgramResearchThemeID] ASC),
    CONSTRAINT [FK_ProgramResearchThemes_Programs_ProgramID] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[Programs] ([ProgramID]),
    CONSTRAINT [FK_ProgramResearchThemes_ResearchThemes_ResearchThemeID] FOREIGN KEY ([ResearchThemeID]) REFERENCES [dbo].[ResearchThemes] ([ResearchThemeID])
);

