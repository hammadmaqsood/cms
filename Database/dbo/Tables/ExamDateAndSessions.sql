﻿CREATE TABLE [dbo].[ExamDateAndSessions] (
    [ExamDateAndSessionID] INT IDENTITY (1, 1) NOT NULL,
    [ExamConductID]        INT NOT NULL,
    [ExamSessionID]        INT NOT NULL,
    [ExamDateID]           INT NOT NULL,
    CONSTRAINT [PK_ExamDateAndSessions] PRIMARY KEY CLUSTERED ([ExamDateAndSessionID] ASC),
    CONSTRAINT [FK_ExamDateAndSessions_ExamConducts_ExamConductID] FOREIGN KEY ([ExamConductID]) REFERENCES [dbo].[ExamConducts] ([ExamConductID]),
    CONSTRAINT [FK_ExamDateAndSessions_ExamDates_ExamDateID] FOREIGN KEY ([ExamDateID]) REFERENCES [dbo].[ExamDates] ([ExamDateID]),
    CONSTRAINT [FK_ExamDateAndSessions_ExamSessions_ExamSessionID] FOREIGN KEY ([ExamSessionID]) REFERENCES [dbo].[ExamSessions] ([ExamSessionID]),
    CONSTRAINT [UK_ExamDateAndSessions_ExamSessionID_ExamDateID] UNIQUE NONCLUSTERED ([ExamSessionID] ASC, [ExamDateID] ASC)
);









