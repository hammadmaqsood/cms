﻿CREATE TABLE [dbo].[Surveys] (
    [SurveyID]    INT           IDENTITY (1, 1) NOT NULL,
    [InstituteID] INT           NOT NULL,
    [SurveyName]  VARCHAR (100) NOT NULL,
    [Description] VARCHAR (MAX) NULL,
    [SurveyType]  [dbo].[Enum]  NOT NULL,
    CONSTRAINT [PK_Surveys] PRIMARY KEY CLUSTERED ([SurveyID] ASC),
    CONSTRAINT [FK_Surveys_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Student, Alumni, FacultyMember', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Surveys', @level2type = N'COLUMN', @level2name = N'SurveyType';
