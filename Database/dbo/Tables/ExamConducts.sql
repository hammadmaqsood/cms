﻿CREATE TABLE [dbo].[ExamConducts] (
    [ExamConductID]           INT                IDENTITY (1, 1) NOT NULL,
    [InstituteID]             INT                NOT NULL,
    [SemesterID]              [dbo].[SemesterID] NOT NULL,
    [ExamType]                [dbo].[Enum]       NOT NULL,
    [Name]                    VARCHAR (100)      NOT NULL,
    [BlockFeeDefaulters]      BIT                NOT NULL,
    [VisibleToStudents]       BIT                NOT NULL,
    [VisibleToFacultyMembers] BIT                NOT NULL,
    CONSTRAINT [PK_ExamConducts] PRIMARY KEY CLUSTERED ([ExamConductID] ASC),
    CONSTRAINT [FK_ExamConducts_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID]),
    CONSTRAINT [FK_ExamConducts_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID]),
    CONSTRAINT [UK_ExamConducts_InstituteID_SemesterID_ExamType_Name] UNIQUE NONCLUSTERED ([InstituteID] ASC, [SemesterID] ASC, [ExamType] ASC, [Name] ASC)
);













