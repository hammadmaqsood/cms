﻿CREATE TABLE [dbo].[Departments] (
    [DepartmentID]        INT               IDENTITY (1, 1) NOT NULL,
    [InstituteID]         INT               NOT NULL,
    [FacultyID]           UNIQUEIDENTIFIER  NULL,
    [DepartmentName]      [dbo].[Name]      NOT NULL,
    [DepartmentShortName] [dbo].[ShortName] NOT NULL,
    [DepartmentAlias]     [dbo].[Alias]     NOT NULL,
    [FacultyName]         VARCHAR (100)     NULL,
    [Status]              [dbo].[Enum]      NOT NULL,
    CONSTRAINT [PK_Departments] PRIMARY KEY CLUSTERED ([DepartmentID] ASC),
    CONSTRAINT [FK_Departments_Faculties_FacultyID] FOREIGN KEY ([FacultyID]) REFERENCES [dbo].[Faculties] ([FacultyID]),
    CONSTRAINT [FK_Departments_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);






