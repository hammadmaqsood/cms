﻿CREATE TABLE [dbo].[StudentFeeConcessionTypes] (
    [StudentFeeConcessionTypeID] INT IDENTITY (1, 1) NOT NULL,
    [StudentFeeID]               INT NOT NULL,
    [FeeConcessionTypeID]            INT NOT NULL,
    CONSTRAINT [PK_StudentFeeConcessionTypes] PRIMARY KEY CLUSTERED ([StudentFeeConcessionTypeID] ASC),
    CONSTRAINT [FK_StudentFeeConcessionTypes_FeeConcessionTypes_FeeConcessionTypeID] FOREIGN KEY ([FeeConcessionTypeID]) REFERENCES [dbo].[FeeConcessionTypes] ([FeeConcessionTypeID]),
    CONSTRAINT [FK_StudentFeeConcessionTypes_StudentFees_StudentFeeID] FOREIGN KEY ([StudentFeeID]) REFERENCES [dbo].[StudentFees] ([StudentFeeID])
);





