﻿CREATE TABLE [dbo].[BUResults] (
    [BUResultID]       INT                IDENTITY (1, 1) NOT NULL,
    [StudentID]        INT                NOT NULL,
    [RegistrationNo]   INT                NOT NULL,
    [Name]             [dbo].[PersonName] NOT NULL,
    [FatherName]       [dbo].[PersonName] NOT NULL,
    [IntakeSemesterID] [dbo].[SemesterID] NOT NULL,
    [Program]          VARCHAR (100)      NOT NULL,
    [LastUpdatedOn]    DATETIME           NOT NULL,
    CONSTRAINT [PK_BUResults] PRIMARY KEY CLUSTERED ([BUResultID] ASC),
    CONSTRAINT [FK_BUResults_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID])
);




