﻿CREATE TABLE [dbo].[ScholarshipWorkloads] (
    [ScholarshipWorkloadID] INT                 IDENTITY (1, 1) NOT NULL,
    [SemesterID]            [dbo].[SemesterID]  NOT NULL,
    [ProgramID]             INT                 NOT NULL,
    [SemesterNo]            [dbo].[EnumFlags16] NOT NULL,
    [CreditHours]           [dbo].[CreditHours] NOT NULL,
    [Exclude]               BIT                 NOT NULL,
    CONSTRAINT [PK_ScholarshipWorkloads] PRIMARY KEY CLUSTERED ([ScholarshipWorkloadID] ASC),
    CONSTRAINT [FK_ScholarshipWorkloads_Programs_ProgramID] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[Programs] ([ProgramID]),
    CONSTRAINT [FK_ScholarshipWorkloads_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID])
);



