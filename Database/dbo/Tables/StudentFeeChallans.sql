﻿CREATE TABLE [dbo].[StudentFeeChallans] (
    [StudentFeeChallanID]  INT          IDENTITY (1, 1) NOT NULL,
    [StudentFeeID]         INT          NOT NULL,
    [InstallmentNo]        [dbo].[Enum] NOT NULL,
    [Amount]               INT          NOT NULL,
    [DueDate]              DATE         NOT NULL,
    [Status]               [dbo].[Enum] NOT NULL,
    [AccountTransactionID] INT          NULL,
    CONSTRAINT [PK_StudentFeeChallans] PRIMARY KEY CLUSTERED ([StudentFeeChallanID] ASC),
    CONSTRAINT [FK_StudentFeeChallans_AccountTransactions_AccountTransactionID] FOREIGN KEY ([AccountTransactionID]) REFERENCES [dbo].[AccountTransactions] ([AccountTransactionID]),
    CONSTRAINT [FK_StudentFeeChallans_StudentFees_StudentFeeID] FOREIGN KEY ([StudentFeeID]) REFERENCES [dbo].[StudentFees] ([StudentFeeID])
);




















GO


