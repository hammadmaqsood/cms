﻿CREATE TABLE [dbo].[SurveyConducts] (
    [SurveyConductID]   INT                IDENTITY (1, 1) NOT NULL,
    [SurveyID]          INT                NOT NULL,
    [SemesterID]        [dbo].[SemesterID] NOT NULL,
    [SurveyConductType] [dbo].[Enum]       NOT NULL,
    [OpenDate]          DATE               NOT NULL,
    [CloseDate]         DATE               NULL,
    [Status]            [dbo].[Enum]       NOT NULL,
    CONSTRAINT [PK_SurveyConducts] PRIMARY KEY CLUSTERED ([SurveyConductID] ASC),
    CONSTRAINT [FK_SurveyConducts_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID]),
    CONSTRAINT [FK_SurveyConducts_Surveys_SurveyID] FOREIGN KEY ([SurveyID]) REFERENCES [dbo].[Surveys] ([SurveyID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'BeforeMidCourseWise,BeforeMidStudentWise,BeforeFinalCourseWise,BeforeFinalStudentWise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SurveyConducts', @level2type = N'COLUMN', @level2name = N'SurveyConductType';
GO
