﻿CREATE TABLE [dbo].[BUResultSemesterCourses] (
    [BUResultSemesterCourseID] INT                 IDENTITY (1, 1) NOT NULL,
    [BUResultSemesterID]       INT                 NOT NULL,
    [CourseCode]               VARCHAR (50)        NOT NULL,
    [Title]                    VARCHAR (500)       NOT NULL,
    [CreditHours]              [dbo].[CreditHours] NOT NULL,
    [Grade]                    VARCHAR (50)        NULL,
    [GradePoint]               DECIMAL (9, 3)      NULL,
    [Product]                  DECIMAL (9, 3)      NULL,
    CONSTRAINT [PK_BUResultSemesterCourses] PRIMARY KEY CLUSTERED ([BUResultSemesterCourseID] ASC),
    CONSTRAINT [FK_BUResultSemesterCourses_BUResultSemesters_BUResultSemesterID] FOREIGN KEY ([BUResultSemesterID]) REFERENCES [dbo].[BUResultSemesters] ([BUResultSemesterID])
);






