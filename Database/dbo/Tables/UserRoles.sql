﻿CREATE TABLE [dbo].[UserRoles] (
    [UserRoleID]       INT              IDENTITY (1, 1) NOT NULL,
    [UserID]           INT              NOT NULL,
    [UserType]         [dbo].[UserType] NOT NULL,
    [InstituteID]      INT              NULL,
    [UserGroupID]      INT              NULL,
    [ExpiryDate]       DATE             NOT NULL,
    [IsDefault]        BIT              NOT NULL,
    [Status]           [dbo].[Enum]     NOT NULL,
    [AssignedByUserID] INT              NULL,
    [CreatedDate]      DATETIME         NOT NULL,
    [IsDeleted]        BIT              NOT NULL,
    CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED ([UserRoleID] ASC),
    CONSTRAINT [FK_UserRoles_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID]),
    CONSTRAINT [FK_UserRoles_UserGroups_UserGroupID] FOREIGN KEY ([UserGroupID]) REFERENCES [dbo].[UserGroups] ([UserGroupID]),
    CONSTRAINT [FK_UserRoles_Users_AssignedByUserID] FOREIGN KEY ([AssignedByUserID]) REFERENCES [dbo].[Users] ([UserID]),
    CONSTRAINT [FK_UserRoles_Users_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);












GO
