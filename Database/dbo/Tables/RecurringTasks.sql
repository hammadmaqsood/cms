CREATE TABLE [dbo].[RecurringTasks] (
    [RecurringTaskID] INT              NOT NULL IDENTITY,
    [TaskGUID]        UNIQUEIDENTIFIER NOT NULL,
    [StartTime]       DATETIME         NOT NULL,
    [FinishTime]      DATETIME         NOT NULL,
    [Status]          [dbo].[Enum]     NOT NULL,
    CONSTRAINT [PK_RecurringTasks] PRIMARY KEY CLUSTERED ([RecurringTaskID] DESC)
);
