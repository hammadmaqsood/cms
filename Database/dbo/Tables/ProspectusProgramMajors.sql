﻿CREATE TABLE [dbo].[ProspectusProgramMajors] (
    [ProspectusProgramMajorID] UNIQUEIDENTIFIER NOT NULL,
    [ProspectusProgramID]      UNIQUEIDENTIFIER NOT NULL,
    [Majors]                   VARCHAR (50)     NOT NULL,
    CONSTRAINT [PK_ProspectusProgramMajors] PRIMARY KEY CLUSTERED ([ProspectusProgramMajorID] ASC),
    CONSTRAINT [FK_ProspectusProgramMajors_ProspectusPrograms_ProspectusProgramID] FOREIGN KEY ([ProspectusProgramID]) REFERENCES [dbo].[ProspectusPrograms] ([ProspectusProgramID])
);

