﻿CREATE TABLE [dbo].[ResearchThemes] (
    [ResearchThemeID]   UNIQUEIDENTIFIER NOT NULL,
    [ResearchThemeName] VARCHAR (500)    NOT NULL,
    CONSTRAINT [PK_ResearchThemes] PRIMARY KEY CLUSTERED ([ResearchThemeID] ASC)
);

