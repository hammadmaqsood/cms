﻿CREATE TABLE [dbo].[Scholarships] (
    [ScholarshipID]   INT          IDENTITY (1, 1) NOT NULL,
    [InstituteID]     INT          NOT NULL,
    [ScholarshipType] [dbo].[Enum] NOT NULL,
    CONSTRAINT [PK_Scholarships] PRIMARY KEY CLUSTERED ([ScholarshipID] ASC),
    CONSTRAINT [FK_Scholarships_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID]),
    CONSTRAINT [UK_Scholarships_InstituteID_ScholarshipType] UNIQUE NONCLUSTERED ([InstituteID] ASC, [ScholarshipType] ASC)
);