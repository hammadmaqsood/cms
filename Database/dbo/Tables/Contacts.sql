﻿CREATE TABLE [dbo].[Contacts] (
    [ContactID]    UNIQUEIDENTIFIER   NOT NULL,
    [Name]         [dbo].[PersonName] NULL,
    [Email]        [dbo].[Email]      NOT NULL,
    [Designation]  [dbo].[EnumGuid]   NOT NULL,
    [InstituteID]  INT                NULL,
    [FacultyID]    UNIQUEIDENTIFIER   NULL,
    [DepartmentID] INT                NULL,
    CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED ([ContactID] ASC),
    CONSTRAINT [FK_Contacts_Departments_DepartmentID] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Departments] ([DepartmentID]),
    CONSTRAINT [FK_Contacts_Faculties_FacultyID] FOREIGN KEY ([FacultyID]) REFERENCES [dbo].[Faculties] ([FacultyID]),
    CONSTRAINT [FK_Contacts_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);

