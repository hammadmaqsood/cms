﻿CREATE TABLE [dbo].[AdmissionCriteriaPreReqDegreeSubjects] (
    [AdmissionCriteriaPreReqDegreeSubjectID] INT           IDENTITY (1, 1) NOT NULL,
    [AdmissionCriteriaPreReqDegreeID]        INT           NOT NULL,
    [Subjects]                               VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_AdmissionCriteriaPreReqDegreeSubjects] PRIMARY KEY CLUSTERED ([AdmissionCriteriaPreReqDegreeSubjectID] ASC),
    CONSTRAINT [FK_AdmissionCriteriaPreReqDegreeSubjects_AdmissionCriteriaPreReqDegrees_AdmissionCriteriaPreReqDegreeID] FOREIGN KEY ([AdmissionCriteriaPreReqDegreeID]) REFERENCES [dbo].[AdmissionCriteriaPreReqDegrees] ([AdmissionCriteriaPreReqDegreeID])
);




