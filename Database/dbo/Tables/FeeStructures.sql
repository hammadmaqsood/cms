﻿CREATE TABLE [dbo].[FeeStructures] (
    [FeeStructureID]           INT                 IDENTITY (1, 1) NOT NULL,
    [AdmissionOpenProgramID]   INT                 NOT NULL,
    [FirstSemesterCreditHours] [dbo].[CreditHours] NOT NULL,
    CONSTRAINT [PK_FeeStructures] PRIMARY KEY CLUSTERED ([FeeStructureID] ASC),
    CONSTRAINT [FK_FeeStructures_AdmissionOpenPrograms_AdmissionOpenProgramID] FOREIGN KEY ([AdmissionOpenProgramID]) REFERENCES [dbo].[AdmissionOpenPrograms] ([AdmissionOpenProgramID]),
    CONSTRAINT [UK_FeeStructures_AdmissionOpenProgramID] UNIQUE NONCLUSTERED ([AdmissionOpenProgramID] ASC)
);












GO


