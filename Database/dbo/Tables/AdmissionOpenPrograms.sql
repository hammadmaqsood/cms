﻿CREATE TABLE [dbo].[AdmissionOpenPrograms] (
    [AdmissionOpenProgramID]            INT                IDENTITY (1, 1) NOT NULL,
    [SemesterID]                        [dbo].[SemesterID] NOT NULL,
    [ProgramID]                         INT                NOT NULL,
    [AdmissionCriteriaID]               INT                NOT NULL,
    [AdmissionOpenDate]                 DATETIME           NOT NULL,
    [AdmissionClosingDate]              DATETIME           NOT NULL,
    [AdmissionProcessingFeeDueDate]     DATETIME           NOT NULL,
    [Shifts]                            [dbo].[EnumFlags8] NOT NULL,
    [EntryTestType]                     [dbo].[Enum]       NOT NULL,
    [EntryTestStartDate]                DATETIME           NULL,
    [EntryTestEndDate]                  DATETIME           NULL,
    [EntryTestDuration]                 TINYINT            NULL,
    [InterviewsStartDate]               DATETIME           NULL,
    [InterviewsEndDate]                 DATETIME           NULL,
    [LoginAllowedEndDate]               DATE               NOT NULL,
    [ExamSystemType]                    [dbo].[Enum]       NOT NULL,
    [EligibilityCriteriaStatement]      VARCHAR (2000)     NULL,
    [IsSummerRegularSemester]           BIT                NOT NULL,
    [AdmissionProcessingFee]            SMALLINT           NOT NULL,
    [MaxSemesterID]                     [dbo].[SemesterID] NOT NULL,
    [ExamMarksGradingSchemeID]          INT                NOT NULL,
    [ExamMarkingSchemeID]               INT                NOT NULL,
    [MigrationID]                       INT                NULL,
    [FinalSemesterID]                   [dbo].[SemesterID] NULL,
    [DefaultExamMarksPolicyID]          INT                NULL,
    [ExamRemarksPolicyID]               INT                NULL,
    [AdmissionOpenDateForForeigners]    DATETIME           NULL,
    [AdmissionClosingDateForForeigners] DATETIME           NULL,
    [IntakeTargetStudentsCount]         INT                NOT NULL,
    CONSTRAINT [PK_AdmissionOpenPrograms] PRIMARY KEY CLUSTERED ([AdmissionOpenProgramID] ASC),
    CONSTRAINT [FK_AdmissionOpenPrograms_AdmissionCriteria_AdmissionCriteriaID] FOREIGN KEY ([AdmissionCriteriaID]) REFERENCES [dbo].[AdmissionCriteria] ([AdmissionCriteriaID]),
    CONSTRAINT [FK_AdmissionOpenPrograms_ExamMarksPolicies_DefaultExamMarksPolicyID] FOREIGN KEY ([DefaultExamMarksPolicyID]) REFERENCES [dbo].[ExamMarksPolicies] ([ExamMarksPolicyID]),
    CONSTRAINT [FK_AdmissionOpenPrograms_ExamRemarksPolicies_ExamRemarksPolicyID] FOREIGN KEY ([ExamRemarksPolicyID]) REFERENCES [dbo].[ExamRemarksPolicies] ([ExamRemarksPolicyID]),
    CONSTRAINT [FK_AdmissionOpenPrograms_Programs_ProgramID] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[Programs] ([ProgramID]),
    CONSTRAINT [FK_AdmissionOpenPrograms_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID])
);












