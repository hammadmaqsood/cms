﻿CREATE TABLE [dbo].[Candidates] (
    [CandidateID]                     INT                     IDENTITY (1, 1) NOT NULL,
    [SemesterID]                      [dbo].[SemesterID]      NOT NULL,
    [Name]                            [dbo].[PersonName]      NOT NULL,
    [Email]                           [dbo].[Email]           NOT NULL,
    [Password]                        [dbo].[EncryptedHashed] NOT NULL,
    [ConfirmationCode]                UNIQUEIDENTIFIER        NULL,
    [ConfirmationCodeExpiryDate]      DATETIME                NULL,
    [CNIC]                            [dbo].[CNIC]            NULL,
    [PassportNo]                      VARCHAR (50)            NULL,
    [Gender]                          [dbo].[Gender]          NULL,
    [Category]                        [dbo].[Category]        NULL,
    [DOB]                             DATE                    NULL,
    [BloodGroup]                      [dbo].[Enum]            NULL,
    [Phone]                           [dbo].[Phone]           NULL,
    [Mobile]                          [dbo].[Phone]           NULL,
    [Nationality]                     VARCHAR (50)            NULL,
    [Country]                         VARCHAR (50)            NULL,
    [Province]                        VARCHAR (50)            NULL,
    [District]                        VARCHAR (50)            NULL,
    [Tehsil]                          VARCHAR (50)            NULL,
    [Domicile]                        VARCHAR (50)            NULL,
    [AreaType]                        [dbo].[Enum]            NULL,
    [CurrentAddress]                  [dbo].[Address]         NULL,
    [PermanentAddress]                [dbo].[Address]         NULL,
    [ServiceNo]                       VARCHAR (50)            NULL,
    [Remarks]                         [dbo].[Remarks]         NULL,
    [FatherName]                      [dbo].[PersonName]      NULL,
    [FatherCNIC]                      [dbo].[CNIC]            NULL,
    [FatherPassportNo]                VARCHAR (50)            NULL,
    [FatherDesignation]               VARCHAR (100)           NULL,
    [FatherDepartment]                VARCHAR (100)           NULL,
    [FatherOrganization]              VARCHAR (100)           NULL,
    [FatherServiceNo]                 VARCHAR (50)            NULL,
    [FatherOfficePhone]               [dbo].[Phone]           NULL,
    [FatherHomePhone]                 [dbo].[Phone]           NULL,
    [FatherMobile]                    [dbo].[Phone]           NULL,
    [FatherFax]                       [dbo].[Phone]           NULL,
    [SponsoredBy]                     [dbo].[Enum]            NULL,
    [SponsorName]                     VARCHAR (100)           NULL,
    [SponsorCNIC]                     [dbo].[CNIC]            NULL,
    [SponsorPassportNo]               VARCHAR (50)            NULL,
    [SponsorRelationshipWithGuardian] VARCHAR (50)            NULL,
    [SponsorDesignation]              VARCHAR (50)            NULL,
    [SponsorDepartment]               VARCHAR (50)            NULL,
    [SponsorOrganization]             VARCHAR (50)            NULL,
    [SponsorServiceNo]                VARCHAR (50)            NULL,
    [SponsorPhoneHome]                VARCHAR (50)            NULL,
    [SponsorMobile]                   VARCHAR (50)            NULL,
    [SponsorPhoneOffice]              VARCHAR (50)            NULL,
    [SponsorFax]                      VARCHAR (50)            NULL,
    [EmergencyContactName]            VARCHAR (200)           NULL,
    [EmergencyMobile]                 VARCHAR (200)           NULL,
    [EmergencyPhone]                  VARCHAR (200)           NULL,
    [EmergencyRelationship]           VARCHAR (200)           NULL,
    [EmergencyAddress]                [dbo].[Address]         NULL,
    [PhysicalDisability]              VARCHAR (200)           NULL,
    [AnnualFamilyIncome]              VARCHAR (50)            NULL,
    [SourceOfInformation]             [dbo].[Enum]            NULL,
    [Status]                          [dbo].[EnumFlags8]      NOT NULL,
    [EditStatus]                      [dbo].[Enum]            NOT NULL,
    [MigrationID]                     INT                     NULL,
    [Religion]                        VARCHAR (100)           NULL,
    [NextOfKin]                       VARCHAR (200)           NULL,
    [BURegistrationNo]                INT                     NULL,
    [ETSScorer]                       BIT                     NOT NULL,
    [NTNOrCNIC]                       VARCHAR (50)            NULL,
    [PakistanBarCouncilNo]            VARCHAR (50)            NULL,
    [PictureSystemFolderID]           UNIQUEIDENTIFIER        NULL,
    [ForeignStudent]                  BIT                     NOT NULL,
    [NextOfKinRelationship]           VARCHAR (200)           NULL,
    [IsFatherAlive]                   BIT                     NOT NULL,
    CONSTRAINT [PK_Candidates] PRIMARY KEY CLUSTERED ([CandidateID] ASC),
    CONSTRAINT [FK_Candidates_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID]),
    CONSTRAINT [FK_Candidates_SystemFolders_PictureSystemFolderID] FOREIGN KEY ([PictureSystemFolderID]) REFERENCES [dbo].[SystemFolders] ([SystemFolderID]),
    CONSTRAINT [UK_Candidates_SemesterID_Email] UNIQUE NONCLUSTERED ([SemesterID] ASC, [Email] ASC)
);






















