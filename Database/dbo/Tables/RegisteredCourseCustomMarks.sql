﻿CREATE TABLE [dbo].[RegisteredCourseCustomMarks] (
    [RegisteredCourseCustomMarkID] INT               NOT NULL,
    [RegisteredCourseID]           INT               NOT NULL,
    [DisplayIndex]                 TINYINT           NOT NULL,
    [MarksTitle]                   VARCHAR (500)     NOT NULL,
    [TotalMarks]                   [dbo].[ExamMarks] NOT NULL,
    [ObtainedMarks]                [dbo].[ExamMarks] NOT NULL,
    CONSTRAINT [PK_RegisteredCourseCustomMarks] PRIMARY KEY CLUSTERED ([RegisteredCourseCustomMarkID] ASC),
    CONSTRAINT [CK_RegisteredCourseCustomMarks ([ObtainedMarks]]<=[TotalMarks]])] CHECK ([ObtainedMarks]<=[TotalMarks]),
    CONSTRAINT [FK_RegisteredCourseCustomMarks_RegisteredCourses_RegisteredCourseID] FOREIGN KEY ([RegisteredCourseID]) REFERENCES [dbo].[RegisteredCourses] ([RegisteredCourseID]),
    CONSTRAINT [UK_RegisteredCourseCustomMarks_RegisteredCourseID_DisplayIndex] UNIQUE NONCLUSTERED ([RegisteredCourseID] ASC, [DisplayIndex] ASC)
);





