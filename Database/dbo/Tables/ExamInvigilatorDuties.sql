﻿CREATE TABLE [dbo].[ExamInvigilatorDuties] (
    [ExamInvigilatorDutyID] INT          IDENTITY (1, 1) NOT NULL,
    [ExamInvigilatorID]     INT          NOT NULL,
    [ExamDateAndSessionID]  INT          NOT NULL,
    [ExamOfferedRoomID]     INT          NOT NULL,
    [InvigilatorType]       [dbo].[Enum] NOT NULL,
    [AttendanceStatus]      [dbo].[Enum] NOT NULL,
    CONSTRAINT [PK_ExamInvigilatorDuties] PRIMARY KEY CLUSTERED ([ExamInvigilatorDutyID] ASC),
    CONSTRAINT [FK_ExamInvigilatorDuties_ExamDateAndSessions_ExamDateAndSessionID] FOREIGN KEY ([ExamDateAndSessionID]) REFERENCES [dbo].[ExamDateAndSessions] ([ExamDateAndSessionID]),
    CONSTRAINT [FK_ExamInvigilatorDuties_ExamInvigilators_ExamInvigilatorID] FOREIGN KEY ([ExamInvigilatorID]) REFERENCES [dbo].[ExamInvigilators] ([ExamInvigilatorID]),
    CONSTRAINT [FK_ExamInvigilatorDuties_ExamOfferedRooms_ExamOfferedRoomID] FOREIGN KEY ([ExamOfferedRoomID]) REFERENCES [dbo].[ExamOfferedRooms] ([ExamOfferedRoomID])
);








GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Senior Invigilator, Invigilator', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ExamInvigilatorDuties', @level2type = N'COLUMN', @level2name = N'InvigilatorType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Absent, Present', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ExamInvigilatorDuties', @level2type = N'COLUMN', @level2name = N'AttendanceStatus';

