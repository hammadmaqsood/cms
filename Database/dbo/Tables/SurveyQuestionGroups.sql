﻿CREATE TABLE [dbo].[SurveyQuestionGroups] (
    [SurveyQuestionGroupID] INT           IDENTITY (1, 1) NOT NULL,
    [SurveyID]              INT           NOT NULL,
    [QuestionGroupName]     VARCHAR (100) NULL,
    [DisplayIndex]          INT           NOT NULL,
    CONSTRAINT [PK_SurveyQuestionGroups] PRIMARY KEY CLUSTERED ([SurveyQuestionGroupID] ASC),
    CONSTRAINT [FK_SurveyQuestionGroups_Surveys_SurveyID] FOREIGN KEY ([SurveyID]) REFERENCES [dbo].[Surveys] ([SurveyID])
);





