﻿CREATE TABLE [dbo].[FeeDueDates] (
    [FeeDueDateID] INT                IDENTITY (1, 1) NOT NULL,
    [SemesterID]   [dbo].[SemesterID] NOT NULL,
    [ProgramID]    INT                NOT NULL,
    [DueDate]      DATE               NOT NULL,
    CONSTRAINT [PK_FeeDueDates] PRIMARY KEY CLUSTERED ([FeeDueDateID] ASC),
    CONSTRAINT [FK_FeeDueDates_Programs_ProgramID] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[Programs] ([ProgramID]),
    CONSTRAINT [FK_FeeDueDates_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID]),
    CONSTRAINT [UK_FeeDueDates_SemesterID_ProgramID] UNIQUE NONCLUSTERED ([SemesterID] DESC, [ProgramID] ASC)
);





