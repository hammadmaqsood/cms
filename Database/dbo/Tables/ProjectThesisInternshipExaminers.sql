﻿CREATE TABLE [dbo].[ProjectThesisInternshipExaminers] (
    [ProjectThesisInternshipExaminerID] UNIQUEIDENTIFIER   NOT NULL,
    [ProjectThesisInternshipID]         UNIQUEIDENTIFIER   NOT NULL,
    [ExaminerName]                      [dbo].[PersonName] NOT NULL,
    [ExaminerType]                      [dbo].[EnumGuid]   NOT NULL,
    CONSTRAINT [PK_ProjectThesisInternshipExaminers] PRIMARY KEY CLUSTERED ([ProjectThesisInternshipExaminerID] ASC),
    CONSTRAINT [FK_ProjectThesisInternshipExaminers_ProjectThesisInternships_ProjectThesisInternshipID] FOREIGN KEY ([ProjectThesisInternshipID]) REFERENCES [dbo].[ProjectThesisInternships] ([ProjectThesisInternshipID])
);

