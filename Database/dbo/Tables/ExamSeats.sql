﻿CREATE TABLE [dbo].[ExamSeats] (
    [ExamSeatID]         INT     IDENTITY (1, 1) NOT NULL,
    [RegisteredCourseID] INT     NOT NULL,
    [ExamOfferedRoomID]  INT     NOT NULL,
    [Row]                TINYINT NOT NULL,
    [Column]             TINYINT NOT NULL,
    CONSTRAINT [PK_ExamSeats] PRIMARY KEY CLUSTERED ([ExamSeatID] ASC),
    CONSTRAINT [FK_ExamSeats_ExamOfferedRooms_ExamOfferedRoomID] FOREIGN KEY ([ExamOfferedRoomID]) REFERENCES [dbo].[ExamOfferedRooms] ([ExamOfferedRoomID]),
    CONSTRAINT [FK_ExamSeats_RegisteredCourses_RegisteredCourseID] FOREIGN KEY ([RegisteredCourseID]) REFERENCES [dbo].[RegisteredCourses] ([RegisteredCourseID])
);



