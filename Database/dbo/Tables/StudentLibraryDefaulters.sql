﻿CREATE TABLE [dbo].[StudentLibraryDefaulters] (
    [StudentLibraryDefaulterID] INT             IDENTITY (1, 1) NOT NULL,
    [StudentID]                 INT             NOT NULL,
    [Remarks]                   [dbo].[Remarks] NULL,
    [Status]                    [dbo].[Enum]    NOT NULL,
    [LastUpdatedDate] DATETIME NOT NULL, 
    [LastUpdatedByUserLoginHistoryID] INT NOT NULL, 
    CONSTRAINT [PK_StudentLibraryDefaulters] PRIMARY KEY CLUSTERED ([StudentLibraryDefaulterID] ASC),
    CONSTRAINT [FK_StudentLibraryDefaulters_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID]), 
    CONSTRAINT [FK_StudentLibraryDefaulters_UserLoginHistory_LastUpdatedByUserLoginHistoryID] FOREIGN KEY ([LastUpdatedByUserLoginHistoryID]) REFERENCES [UserLoginHistory]([UserLoginHistoryID])
);

