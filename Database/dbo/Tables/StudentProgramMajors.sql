﻿CREATE TABLE [dbo].[StudentProgramMajors] (
    [StudentProgramMajorID] INT IDENTITY (1, 1) NOT NULL,
    [StudentID]             INT NOT NULL,
    [ProgramMajorID]        INT NOT NULL,
    CONSTRAINT [PK_StudentProgramMajors] PRIMARY KEY CLUSTERED ([StudentProgramMajorID] ASC),
    CONSTRAINT [FK_StudentProgramMajors_ProgramMajors_ProgramMajorID] FOREIGN KEY ([ProgramMajorID]) REFERENCES [dbo].[ProgramMajors] ([ProgramMajorID]),
    CONSTRAINT [FK_StudentProgramMajors_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID])
);



