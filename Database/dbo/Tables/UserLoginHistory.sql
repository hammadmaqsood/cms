﻿CREATE TABLE [dbo].[UserLoginHistory] (
    [UserLoginHistoryID]               INT              IDENTITY (1, 1) NOT NULL,
    [LoginSessionGuid]                 UNIQUEIDENTIFIER NOT NULL,
    [UserType]                         [dbo].[UserType] NOT NULL,
    [SubUserType]                      [dbo].[Enum]     NOT NULL,
    [UserID]                           INT              NULL,
    [UserRoleID]                       INT              NULL,
    [CandidateID]                      INT              NULL,
    [StudentID]                        INT              NULL,
    [FacultyMemberID]                  INT              NULL,
    [IntegratedServiceUserID]          INT              NULL,
    [UserName]                         VARCHAR (50)     NOT NULL,
    [IPAddress]                        VARCHAR (50)     NULL,
    [UserAgent]                        VARCHAR (1024)   NULL,
    [LoginStatus]                      [dbo].[Enum]     NOT NULL,
    [LoginDate]                        DATETIME         NOT NULL,
    [LoginFailureReason]               [dbo].[Enum]     NULL,
    [LogOffDate]                       DATETIME         NULL,
    [LogOffReason]                     [dbo].[Enum]     NULL,
    [ImpersonatedByUserLoginHistoryID] INT              NULL,
    CONSTRAINT [PK_UserLoginHistory] PRIMARY KEY CLUSTERED ([UserLoginHistoryID] DESC),
    CONSTRAINT [FK_UserLoginHistory_Candidates_CandidateID] FOREIGN KEY ([CandidateID]) REFERENCES [dbo].[Candidates] ([CandidateID]),
    CONSTRAINT [FK_UserLoginHistory_FacultyMembers_FacultyMemberID] FOREIGN KEY ([FacultyMemberID]) REFERENCES [dbo].[FacultyMembers] ([FacultyMemberID]),
    CONSTRAINT [FK_UserLoginHistory_IntegratedServiceUsers_IntegratedServiceUserID] FOREIGN KEY ([IntegratedServiceUserID]) REFERENCES [dbo].[IntegratedServiceUsers] ([IntegratedServiceUserID]),
    CONSTRAINT [FK_UserLoginHistory_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [FK_UserLoginHistory_UserLoginHistory_ImpersonatedByUserLoginHistoryID] FOREIGN KEY ([ImpersonatedByUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID]),
    CONSTRAINT [FK_UserLoginHistory_UserRoles_UserRoleID] FOREIGN KEY ([UserRoleID]) REFERENCES [dbo].[UserRoles] ([UserRoleID]),
    CONSTRAINT [FK_UserLoginHistory_Users_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID]),
    CONSTRAINT [UK_UserLoginHistory_LoginSessionGuid] UNIQUE NONCLUSTERED ([LoginSessionGuid] ASC)
);
GO
