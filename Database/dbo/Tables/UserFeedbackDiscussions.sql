﻿CREATE TABLE [dbo].[UserFeedbackDiscussions] (
    [UserFeedbackDiscussionID]    UNIQUEIDENTIFIER NOT NULL,
    [UserFeedbackID]              UNIQUEIDENTIFIER NOT NULL,
    [Details]                     VARCHAR (MAX)    NOT NULL,
    [CreatedDate]                 DATETIME         NOT NULL,
    [CreatedByUserLoginHistoryID] INT              NOT NULL,
    [Status]                      [dbo].[EnumGuid] NOT NULL,
    CONSTRAINT [PK_UserFeedbackDiscussions] PRIMARY KEY CLUSTERED ([UserFeedbackDiscussionID] ASC),
    CONSTRAINT [FK_UserFeedbackDiscussions_UserFeedbacks_UserFeedbackID] FOREIGN KEY ([UserFeedbackID]) REFERENCES [dbo].[UserFeedbacks] ([UserFeedbackID]),
    CONSTRAINT [FK_UserFeedbackDiscussions_UserLoginHistory_CreatedByUserLoginHistoryID] FOREIGN KEY ([CreatedByUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID])
);



