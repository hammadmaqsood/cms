﻿CREATE TABLE [dbo].[StudentSemesters] (
    [StudentSemesterID]   INT                IDENTITY (1, 1) NOT NULL,
    [StudentID]           INT                NOT NULL,
    [SemesterID]          [dbo].[SemesterID] NOT NULL,
    [SemesterNo]          [dbo].[SemesterNo] NOT NULL,
    [Section]             [dbo].[Section]    NOT NULL,
    [Shift]               [dbo].[Shift]      NOT NULL,
    [Status]              [dbo].[Enum]       NOT NULL,
    [FreezedStatus]       [dbo].[Enum]       NULL,
    [FreezedDate]         DATE               NULL,
    [Remarks]             [dbo].[Remarks]    NULL,
    [GPA]                 [dbo].[GPA]        NULL,
    [CGPA]                [dbo].[GPA]        NULL,
    [ExamRemarksType]     [dbo].[Enum]       NULL,
    [CGPACalculationDate] DATETIME           NULL,
    [BUExamGPA]           [dbo].[GPA]        NULL,
    [BUExamCGPA]          [dbo].[GPA]        NULL,
    [BUExamResultRemarks] [dbo].[Enum]       NULL,
    CONSTRAINT [PK_StudentSemesters] PRIMARY KEY CLUSTERED ([StudentSemesterID] ASC),
    CONSTRAINT [FK_StudentSemesters_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID]),
    CONSTRAINT [FK_StudentSemesters_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [UK_StudentSemesters_StudentID_SemesterID] UNIQUE NONCLUSTERED ([StudentID] ASC, [SemesterID] ASC)
);












GO
