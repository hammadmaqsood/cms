﻿CREATE TABLE [dbo].[Tehsils] (
    [TehsilID]     INT          IDENTITY (1, 1) NOT NULL,
    [ProvinceName] VARCHAR (50) NOT NULL,
    [DistrictName] VARCHAR (50) NOT NULL,
    [TehsilName]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Tehsils] PRIMARY KEY CLUSTERED ([TehsilID] ASC)
);


GO
