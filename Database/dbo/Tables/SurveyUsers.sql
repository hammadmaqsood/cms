﻿CREATE TABLE [dbo].[SurveyUsers] (
    [SurveyUserID]         INT  IDENTITY (1, 1) NOT NULL,
    [SurveyConductID]      INT  NOT NULL,
    [StudentID]            INT  NULL,
    [RegisteredCourseID]   INT  NULL,
    [FacultyMemberID]      INT  NULL,
    [OfferedCourseID]      INT  NULL,
    [SurveyCompletionDate] DATE NOT NULL,
    CONSTRAINT [PK_SurveyUsers] PRIMARY KEY CLUSTERED ([SurveyUserID] ASC),
    CONSTRAINT [CK_SurveyUsers ([StudentID]] IS NOT NULL OR [RegisteredCourseID]] IS NOT NULL OR [FacultyMemberID]] IS NOT NULL OR [OfferedCourseID] CHECK ([StudentID] IS NOT NULL OR [RegisteredCourseID] IS NOT NULL OR [FacultyMemberID] IS NOT NULL OR [OfferedCourseID] IS NOT NULL),
    CONSTRAINT [FK_SurveyUsers_FacultyMembers_FacultyMemberID] FOREIGN KEY ([FacultyMemberID]) REFERENCES [dbo].[FacultyMembers] ([FacultyMemberID]),
    CONSTRAINT [FK_SurveyUsers_OfferedCourses_OfferedCourseID] FOREIGN KEY ([OfferedCourseID]) REFERENCES [dbo].[OfferedCourses] ([OfferedCourseID]),
    CONSTRAINT [FK_SurveyUsers_RegisteredCourses_RegisteredCourseID] FOREIGN KEY ([RegisteredCourseID]) REFERENCES [dbo].[RegisteredCourses] ([RegisteredCourseID]),
    CONSTRAINT [FK_SurveyUsers_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [FK_SurveyUsers_SurveyConducts_SurveyConductID] FOREIGN KEY ([SurveyConductID]) REFERENCES [dbo].[SurveyConducts] ([SurveyConductID]),
    CONSTRAINT [UK_SurveyUsers_SurveyConductID_StudentID_RegisteredCourseID_FacultyMemberID_OfferedCourseID] UNIQUE NONCLUSTERED ([SurveyConductID] ASC, [StudentID] ASC, [RegisteredCourseID] ASC, [FacultyMemberID] ASC, [OfferedCourseID] ASC)
);