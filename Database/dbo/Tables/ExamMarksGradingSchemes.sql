﻿CREATE TABLE [dbo].[ExamMarksGradingSchemes] (
    [ExamMarksGradingSchemeID] INT           IDENTITY (1, 1) NOT NULL,
    [InstituteID]              INT           NOT NULL,
    [GradingSchemeName]        VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_ExamMarksGradingSchemes] PRIMARY KEY CLUSTERED ([ExamMarksGradingSchemeID] ASC),
    CONSTRAINT [FK_ExamMarksGradingSchemes_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);




