﻿CREATE TABLE [dbo].[DisciplineCasesStudents] (
    [DisciplineCasesStudentID] INT             IDENTITY (1, 1) NOT NULL,
    [DisciplineCaseID]         INT             NOT NULL,
    [StudentID]                INT             NOT NULL,
    [Mobile]                   [dbo].[Phone]   NOT NULL,
    [StudentsStatement]        [dbo].[Remarks] NULL,
    [RemarksType]              [dbo].[Enum]    NULL,
    [Remarks]                  [dbo].[Remarks] NULL,
    CONSTRAINT [PK_DisciplineCasesStudents] PRIMARY KEY CLUSTERED ([DisciplineCasesStudentID] ASC),
    CONSTRAINT [FK_DisciplineCasesStudents_DisciplineCases_DisciplineCaseID] FOREIGN KEY ([DisciplineCaseID]) REFERENCES [dbo].[DisciplineCases] ([DisciplineCaseID]),
    CONSTRAINT [FK_DisciplineCasesStudents_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID])
);



