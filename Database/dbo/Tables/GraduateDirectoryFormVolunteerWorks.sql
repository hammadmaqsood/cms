﻿CREATE TABLE [dbo].[GraduateDirectoryFormVolunteerWorks] (
    [GraduateDirectoryFormVolunteerWorkID] INT            IDENTITY (1, 1) NOT NULL,
    [GraduateDirectoryFormID]              INT            NOT NULL,
    [Organization]                         VARCHAR (500)  NOT NULL,
    [Description]                          VARCHAR (1000) NOT NULL,
    [FromDate]                             DATE           NOT NULL,
    [ToDate]                               DATE           NOT NULL,
    CONSTRAINT [PK_GraduateDirectoryFormVolunteerWorks] PRIMARY KEY CLUSTERED ([GraduateDirectoryFormVolunteerWorkID] ASC),
    CONSTRAINT [CK_GraduateDirectoryFormVolunteerWorks ([FromDate]]<=[ToDate]])] CHECK ([FromDate]<=[ToDate]),
    CONSTRAINT [FK_GraduateDirectoryFormVolunteerWorks_GraduateDirectoryForms_GraduateDirectoryFormID] FOREIGN KEY ([GraduateDirectoryFormID]) REFERENCES [dbo].[GraduateDirectoryForms] ([GraduateDirectoryFormID])
);



