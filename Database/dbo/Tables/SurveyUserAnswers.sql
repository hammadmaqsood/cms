﻿CREATE TABLE [dbo].[SurveyUserAnswers] (
    [SurveyUserID]           INT           NOT NULL,
    [SurveyQuestionID]       INT           NOT NULL,
    [SurveyQuestionOptionID] INT           NULL,
    [OptionText]             VARCHAR (MAX) NULL,
    CONSTRAINT [PK_SurveyUserAnswers] PRIMARY KEY CLUSTERED ([SurveyUserID] ASC, [SurveyQuestionID] ASC),
    CONSTRAINT [FK_SurveyUserAnswers_SurveyQuestionOptions_SurveyQuestionOptionID] FOREIGN KEY ([SurveyQuestionOptionID]) REFERENCES [dbo].[SurveyQuestionOptions] ([SurveyQuestionOptionID]),
    CONSTRAINT [FK_SurveyUserAnswers_SurveyQuestions_SurveyQuestionID] FOREIGN KEY ([SurveyQuestionID]) REFERENCES [dbo].[SurveyQuestions] ([SurveyQuestionID]),
    CONSTRAINT [FK_SurveyUserAnswers_SurveyUsers_SurveyUserID] FOREIGN KEY ([SurveyUserID]) REFERENCES [dbo].[SurveyUsers] ([SurveyUserID])
);





