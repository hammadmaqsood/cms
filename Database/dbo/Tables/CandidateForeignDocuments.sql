﻿CREATE TABLE [dbo].[CandidateForeignDocuments] (
    [CandidateForeignDocumentID] UNIQUEIDENTIFIER NOT NULL,
    [CandidateID]                INT              NOT NULL,
    [DocumentType]               [dbo].[EnumGuid] NOT NULL,
    [Remarks]                    [dbo].[Remarks]  NULL,
    [SystemFileID]               UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_CandidateForeignDocuments] PRIMARY KEY CLUSTERED ([CandidateForeignDocumentID] ASC),
    CONSTRAINT [FK_CandidateForeignDocuments_Candidates_CandidateID] FOREIGN KEY ([CandidateID]) REFERENCES [dbo].[Candidates] ([CandidateID])
);
