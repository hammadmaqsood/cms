﻿CREATE TABLE [dbo].[Courses] (
    [CourseID]                          INT                 IDENTITY (1, 1) NOT NULL,
    [AdmissionOpenProgramID]            INT                 NOT NULL,
    [CourseCode]                        VARCHAR (20)        NOT NULL,
    [Title]                             VARCHAR (250)       NOT NULL,
    [CreditHours]                       [dbo].[CreditHours] NOT NULL,
    [ContactHours]                      [dbo].[CreditHours] NOT NULL,
    [CourseCategory]                    [dbo].[Enum]        NOT NULL,
    [CourseType]                        [dbo].[Enum]        NOT NULL,
    [ProgramMajorID]                    INT                 NULL,
    [SemesterNo]                        [dbo].[SemesterNo]  NOT NULL,
    [Remarks]                           [dbo].[Remarks]     NULL,
    [MigrationID]                       INT                 NULL,
    [CustomMarks]                       BIT                 NOT NULL,
    [SummerGradeCapping]                BIT                 NOT NULL,
    [Status]                            [dbo].[EnumGuid]    NOT NULL,
    [StatusChangedByUserLoginHistoryID] INT                 NULL,
    [StatusDate]                        DATETIME            NULL,
    [SubstitutedCourseID]               INT                 NULL,
    [SubstitutedByUserLoginHistoryID]   INT                 NULL,
    [SubstitutedDateTime]               DATETIME            NULL,
    CONSTRAINT [PK_Courses] PRIMARY KEY CLUSTERED ([CourseID] ASC),
    CONSTRAINT [FK_Courses_AdmissionOpenPrograms_AdmissionOpenProgramID] FOREIGN KEY ([AdmissionOpenProgramID]) REFERENCES [dbo].[AdmissionOpenPrograms] ([AdmissionOpenProgramID]),
    CONSTRAINT [FK_Courses_Courses_SubstitutedCourseID] FOREIGN KEY ([SubstitutedCourseID]) REFERENCES [dbo].[Courses] ([CourseID]),
    CONSTRAINT [FK_Courses_ProgramMajors_ProgramMajorID] FOREIGN KEY ([ProgramMajorID]) REFERENCES [dbo].[ProgramMajors] ([ProgramMajorID]),
    CONSTRAINT [FK_Courses_UserLoginHistory_StatusChangedByUserLoginHistoryID] FOREIGN KEY ([StatusChangedByUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID]),
    CONSTRAINT [FK_Courses_UserLoginHistory_SubstitutedByUserLoginHistoryID] FOREIGN KEY ([SubstitutedByUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Core, Elective', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Courses', @level2type = N'COLUMN', @level2name = N'CourseType';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Course,Lab,Internship,Project', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Courses', @level2type = N'COLUMN', @level2name = N'CourseCategory';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Courses';
GO
