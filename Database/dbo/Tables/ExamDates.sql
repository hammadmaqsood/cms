﻿CREATE TABLE [dbo].[ExamDates] (
    [ExamDateID]    INT  IDENTITY (1, 1) NOT NULL,
    [ExamConductID] INT  NOT NULL,
    [Date]          DATE NOT NULL,
    CONSTRAINT [PK_ExamDates] PRIMARY KEY CLUSTERED ([ExamDateID] ASC),
    CONSTRAINT [FK_ExamDates_ExamConducts_ExamConductID] FOREIGN KEY ([ExamConductID]) REFERENCES [dbo].[ExamConducts] ([ExamConductID])
);





