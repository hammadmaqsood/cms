﻿CREATE TABLE [dbo].[OfferedCourses] (
    [OfferedCourseID]                           INT                IDENTITY (1, 1) NOT NULL,
    [CourseID]                                  INT                NOT NULL,
    [SemesterID]                                [dbo].[SemesterID] NOT NULL,
    [FacultyMemberID]                           INT                NULL,
    [SemesterNo]                                [dbo].[SemesterNo] NOT NULL,
    [Section]                                   [dbo].[Section]    NOT NULL,
    [Shift]                                     [dbo].[Shift]      NOT NULL,
    [Visiting]                                  BIT                NOT NULL,
    [SpecialOffered]                            BIT                NOT NULL,
    [MaxClassStrength]                          TINYINT            NOT NULL,
    [Status]                                    [dbo].[Enum]       NOT NULL,
    [Remarks]                                   [dbo].[Remarks]    NULL,
    [AssignmentsMarksSubmitted]                 BIT                NOT NULL,
    [AssignmentsMarksSubmittedDate]             DATETIME           NULL,
    [QuizzesMarksSubmitted]                     BIT                NOT NULL,
    [QuizzesMarksSubmittedDate]                 DATETIME           NULL,
    [InternalsMarksSubmitted]                   BIT                NOT NULL,
    [InternalsMarksSubmittedDate]               DATETIME           NULL,
    [InternalsPracticalMarksSubmitted]          BIT                NOT NULL,
    [InternalsPracticalMarksSubmittedDate]      DATETIME           NULL,
    [MidMarksSubmitted]                         BIT                NOT NULL,
    [MidMarksSubmittedDate]                     DATETIME           NULL,
    [MidPracticalMarksSubmitted]                BIT                NOT NULL,
    [MidPracticalMarksSubmittedDate]            DATETIME           NULL,
    [FinalMarksSubmitted]                       BIT                NOT NULL,
    [FinalMarksSubmittedDate]                   DATETIME           NULL,
    [FinalPracticalMarksSubmitted]              BIT                NOT NULL,
    [FinalPracticalMarksSubmittedDate]          DATETIME           NULL,
    [MarksEntryCompleted]                       BIT                NOT NULL,
    [MarksEntryLocked]                          BIT                NOT NULL,
    [MarksEntryLockedDate]                      DATETIME           NULL,
    [MigrationID]                               INT                NULL,
    [AssignmentsExamCompilationType]            [dbo].[Enum]       NULL,
    [QuizzesExamCompilationType]                [dbo].[Enum]       NULL,
    [InternalsExamCompilationType]              [dbo].[Enum]       NULL,
    [SubmittedToHODByUserLoginHistoryID]        INT                NULL,
    [SubmittedToHODDate]                        DATETIME           NULL,
    [SubmittedToCampusByUserLoginHistoryID]     INT                NULL,
    [SubmittedToCampusDate]                     DATETIME           NULL,
    [SubmittedToUniversityByUserLoginHistoryID] INT                NULL,
    [SubmittedToUniversityDate]                 DATETIME           NULL,
    CONSTRAINT [PK_OfferedCourses] PRIMARY KEY CLUSTERED ([OfferedCourseID] ASC),
    CONSTRAINT [FK_OfferedCourses_Courses_CourseID] FOREIGN KEY ([CourseID]) REFERENCES [dbo].[Courses] ([CourseID]),
    CONSTRAINT [FK_OfferedCourses_FacultyMembers_FacultyMemberID] FOREIGN KEY ([FacultyMemberID]) REFERENCES [dbo].[FacultyMembers] ([FacultyMemberID]),
    CONSTRAINT [FK_OfferedCourses_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID]),
    CONSTRAINT [FK_OfferedCourses_UserLoginHistory_SubmittedToCampusByUserLoginHistoryID] FOREIGN KEY ([SubmittedToCampusByUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID]),
    CONSTRAINT [FK_OfferedCourses_UserLoginHistory_SubmittedToHODByUserLoginHistoryID] FOREIGN KEY ([SubmittedToHODByUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID]),
    CONSTRAINT [FK_OfferedCourses_UserLoginHistory_SubmittedToUniversityByUserLoginHistoryID] FOREIGN KEY ([SubmittedToUniversityByUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID]),
    CONSTRAINT [UK_OfferedCourses_SemesterID_CourseID_SemesterNo_Section_Shift] UNIQUE NONCLUSTERED ([SemesterID] ASC, [CourseID] ASC, [SemesterNo] ASC, [Section] ASC, [Shift] ASC)
);












GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Registration Blocked, Online Registration Allowed, Manual Registration Allowed',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'OfferedCourses',
    @level2type = N'COLUMN',
    @level2name = N'Status'