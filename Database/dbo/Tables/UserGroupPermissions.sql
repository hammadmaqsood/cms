﻿CREATE TABLE [dbo].[UserGroupPermissions] (
    [UserGroupPermissionID] INT           IDENTITY (1, 1) NOT NULL,
    [UserGroupID]           INT           NOT NULL,
    [Module]                TINYINT       NOT NULL,
    [ModuleName]            VARCHAR (200) NOT NULL,
    [PermissionType]        BIGINT        NOT NULL,
    [PermissionTypeName]    VARCHAR (200) NOT NULL,
    [PermissionValue]       TINYINT       NOT NULL,
    [PermissionValueName]   VARCHAR (200) NOT NULL,
    CONSTRAINT [PK_UserGroupPermissions] PRIMARY KEY CLUSTERED ([UserGroupPermissionID] ASC),
    CONSTRAINT [FK_UserGroupPermissions_UserGroups_UserGroupID] FOREIGN KEY ([UserGroupID]) REFERENCES [dbo].[UserGroups] ([UserGroupID]),
    CONSTRAINT [UK_UserGroupPermissions_UserGroupID_Module_PermissionType] UNIQUE NONCLUSTERED ([UserGroupID] ASC, [Module] ASC, [PermissionType] ASC)
);






GO
