﻿CREATE TABLE [dbo].[StudentTransfers] (
    [StudentTransferID]        INT             IDENTITY (1, 1) NOT NULL,
    [TransferredFromStudentID] INT             NOT NULL,
    [TransferredToStudentID]   INT             NOT NULL,
    [TransferType]             [dbo].[Enum]    NOT NULL,
    [TransferredDate]          DATETIME        NOT NULL,
    [Remarks]                  [dbo].[Remarks] NULL,
    CONSTRAINT [PK_StudentTransfers] PRIMARY KEY CLUSTERED ([StudentTransferID] ASC),
    CONSTRAINT [FK_StudentTransfers_Students_TransferredFromStudentID] FOREIGN KEY ([TransferredFromStudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [FK_StudentTransfers_Students_TransferredToStudentID] FOREIGN KEY ([TransferredToStudentID]) REFERENCES [dbo].[Students] ([StudentID])
);

