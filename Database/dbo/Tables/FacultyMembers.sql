﻿CREATE TABLE [dbo].[FacultyMembers] (
    [FacultyMemberID]            INT                     IDENTITY (1, 1) NOT NULL,
    [InstituteID]                INT                     NOT NULL,
    [Title]                      [dbo].[Enum]            NULL,
    [Name]                       [dbo].[PersonName]      NOT NULL,
    [DisplayName]                [dbo].[PersonName]      NULL,
    [UserName]                   [dbo].[Email]           NULL,
    [Password]                   [dbo].[EncryptedHashed] NULL,
    [Email]                      [dbo].[Email]           NULL,
    [Gender]                     [dbo].[Gender]          NOT NULL,
    [CNIC]                       [dbo].[CNIC]            NULL,
    [PassportNo]                 VARCHAR (50)            NULL,
    [DepartmentID]               INT                     NULL,
    [FacultyType]                [dbo].[Enum]            NOT NULL,
    [Status]                     [dbo].[Enum]            NOT NULL,
    [ConfirmationCode]           UNIQUEIDENTIFIER        NULL,
    [ConfirmationCodeExpiryDate] DATETIME                NULL,
    [MigrationID]                INT                     NULL,
    CONSTRAINT [PK_FacultyMembers] PRIMARY KEY CLUSTERED ([FacultyMemberID] ASC),
    CONSTRAINT [FK_FacultyMembers_Departments_DepartmentID] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Departments] ([DepartmentID]),
    CONSTRAINT [FK_FacultyMembers_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Permanent, Visiting', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FacultyMembers', @level2type = N'COLUMN', @level2name = N'FacultyType';
