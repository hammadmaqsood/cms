﻿CREATE TABLE [dbo].[ThesisInternshipMarks] (
    [ThesisInternshipMarkID] INT               IDENTITY (1, 1) NOT NULL,
    [ThesisInternshipID]     INT               NOT NULL,
    [DisplayIndex]           TINYINT           NOT NULL,
    [MarksTitle]             VARCHAR (500)     NOT NULL,
    [TotalMarks]             [dbo].[ExamMarks] NOT NULL,
    [ObtainedMarks]          [dbo].[ExamMarks] NOT NULL,
    CONSTRAINT [PK_ThesisInternshipMarks] PRIMARY KEY CLUSTERED ([ThesisInternshipMarkID] ASC),
    CONSTRAINT [CK_ThesisInternshipMarks ([ObtainedMarks]]<=[TotalMarks]])] CHECK ([ObtainedMarks]<=[TotalMarks]),
    CONSTRAINT [FK_ThesisInternshipMarks_ThesisInternships_ThesisInternshipID] FOREIGN KEY ([ThesisInternshipID]) REFERENCES [dbo].[ThesisInternships] ([ThesisInternshipID]),
    CONSTRAINT [UK_ThesisInternshipMarks_ThesisInternshipID_DisplayIndex] UNIQUE NONCLUSTERED ([ThesisInternshipID] ASC, [DisplayIndex] ASC)
);






