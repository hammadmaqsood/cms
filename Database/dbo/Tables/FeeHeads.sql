﻿CREATE TABLE [dbo].[FeeHeads] (
    [FeeHeadID]                INT          IDENTITY (1, 1) NOT NULL,
    [InstituteID]              INT          NOT NULL,
    [FeeHeadType]              [dbo].[Enum] NOT NULL,
    [HeadName]                 VARCHAR (50) NOT NULL,
    [HeadShortName]            VARCHAR (50) NOT NULL,
    [HeadAlias]                VARCHAR (50) NOT NULL,
    [RuleType]                 [dbo].[Enum] NOT NULL,
    [Refundable]               BIT          NOT NULL,
    [DisplayIndex]             INT          NOT NULL,
    [Concession]               BIT          NOT NULL,
    [Visible]                  BIT          NOT NULL,
    [WithholdingTaxApplicable] BIT          NOT NULL,
    CONSTRAINT [PK_FeeHeads] PRIMARY KEY CLUSTERED ([FeeHeadID] ASC),
    CONSTRAINT [FK_FeeHeads_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);






