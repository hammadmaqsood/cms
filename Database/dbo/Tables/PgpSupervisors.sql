﻿CREATE TABLE [dbo].[PgpSupervisors] (
    [PgpSupervisorID] INT                IDENTITY (1, 1) NOT NULL,
    [InstituteID]     INT                NOT NULL,
    [Name]            [dbo].[PersonName] NOT NULL,
    [SupervisorType]  [dbo].[EnumFlags8] NOT NULL,
    [Status]          [dbo].[Enum]       NOT NULL,
    [CNIC]            [dbo].[CNIC]       NOT NULL,
    CONSTRAINT [PK_PgpSupervisors] PRIMARY KEY CLUSTERED ([PgpSupervisorID] ASC),
    CONSTRAINT [FK_PgpSupervisors_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);

