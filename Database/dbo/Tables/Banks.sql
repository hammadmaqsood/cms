﻿CREATE TABLE [dbo].[Banks] (
    [BankID]             INT          IDENTITY (1, 1) NOT NULL,
    [BankName]           VARCHAR (50) NOT NULL,
    [BankAlias]          VARCHAR (50) NOT NULL,
    [BankNameForDisplay] VARCHAR (50) NULL,
    CONSTRAINT [PK_Banks] PRIMARY KEY CLUSTERED ([BankID] ASC),
    CONSTRAINT [UK_Banks_BankName] UNIQUE NONCLUSTERED ([BankName] ASC)
);








