﻿CREATE TABLE [dbo].[ScholarshipApplications] (
    [ScholarshipApplicationID]  INT             IDENTITY (1, 1) NOT NULL,
    [StudentID]                 INT             NOT NULL,
    [ScholarshipAnnouncementID] INT             NOT NULL,
    [Status]                    [dbo].[Enum]    NOT NULL,
    [FormData]                  VARCHAR (MAX)   NOT NULL,
    [SubmissionDate]            DATETIME        NULL,
    [DocumentsStatus]           [dbo].[Enum]    NOT NULL,
    CONSTRAINT [PK_ScholarshipApplications] PRIMARY KEY CLUSTERED ([ScholarshipApplicationID] ASC),
    CONSTRAINT [FK_ScholarshipApplications_ScholarshipAnnouncements_ScholarshipAnnouncementID] FOREIGN KEY ([ScholarshipAnnouncementID]) REFERENCES [dbo].[ScholarshipAnnouncements] ([ScholarshipAnnouncementID]),
    CONSTRAINT [FK_ScholarshipApplications_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [UK_ScholarshipApplications_ScholarshipAnnouncementID_StudentID] UNIQUE NONCLUSTERED ([ScholarshipAnnouncementID] ASC, [StudentID] ASC)
);