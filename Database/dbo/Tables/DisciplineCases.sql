﻿CREATE TABLE [dbo].[DisciplineCases] (
    [DisciplineCaseID] INT             IDENTITY (1, 1) NOT NULL,
    [InstituteID]      INT             NOT NULL,
    [CaseFileNo]       VARCHAR (50)    NOT NULL,
    [CaseDate]         DATETIME        NOT NULL,
    [Location]         [dbo].[Address] NOT NULL,
    [EntryDate]        DATETIME        NOT NULL,
    CONSTRAINT [PK_DisciplineCases] PRIMARY KEY CLUSTERED ([DisciplineCaseID] ASC),
    CONSTRAINT [FK_DisciplineCases_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);