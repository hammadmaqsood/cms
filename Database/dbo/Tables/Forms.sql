﻿CREATE TABLE [dbo].[Forms] (
    [FormID]         UNIQUEIDENTIFIER NOT NULL,
    [FormType]       [dbo].[EnumGuid] NOT NULL,
    [StudentID]      INT              NOT NULL,
    [InstituteID]    INT              NOT NULL,
    [SubmissionDate] DATETIME         NULL,
    [FormStatus]     UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_Forms] PRIMARY KEY CLUSTERED ([FormID] ASC),
    CONSTRAINT [FK_Forms_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID]),
    CONSTRAINT [FK_Forms_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID])
);
