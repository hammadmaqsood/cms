﻿CREATE TABLE [dbo].[StudentCreditsTransferredCourses] (
    [StudentCreditsTransferredCourseID] INT           IDENTITY (1, 1) NOT NULL,
    [StudentCreditsTransferID]          INT           NOT NULL,
    [CourseTitle]                       VARCHAR (250) NOT NULL,
    [Grade]                             VARCHAR (50)  NOT NULL,
    [EquivalentCourseID]                INT           NULL,
    [EquivalentGrade]                   [dbo].[Enum]  NULL,
    CONSTRAINT [PK_StudentCreditsTransferredCourses] PRIMARY KEY CLUSTERED ([StudentCreditsTransferredCourseID] ASC),
    CONSTRAINT [CK_StudentCreditsTransferredCourses ([EquivalentCourseID]] IS NULL AND [EquivalentGrade]] IS NULL OR [EquivalentCourseID]] IS NOT N] CHECK ([EquivalentCourseID] IS NULL AND [EquivalentGrade] IS NULL OR [EquivalentCourseID] IS NOT NULL AND [EquivalentGrade] IS NOT NULL),
    CONSTRAINT [FK_StudentCreditsTransferredCourses_Courses_EquivalentCourseID] FOREIGN KEY ([EquivalentCourseID]) REFERENCES [dbo].[Courses] ([CourseID]),
    CONSTRAINT [FK_StudentCreditsTransferredCourses_StudentCreditsTransfers_StudentCreditsTransferID] FOREIGN KEY ([StudentCreditsTransferID]) REFERENCES [dbo].[StudentCreditsTransfers] ([StudentCreditsTransferID])
);
