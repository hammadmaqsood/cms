﻿CREATE TABLE [dbo].[RegisteredCourseExamMarks] (
    [RegisteredCourseExamMarkID] INT               IDENTITY (1, 1) NOT NULL,
    [RegisteredCourseID]         INT               NOT NULL,
    [OfferedCourseExamID]        INT               NOT NULL,
    [ExamMarks]                  [dbo].[ExamMarks] NULL,
    CONSTRAINT [PK_RegisteredCourseExamMarks] PRIMARY KEY CLUSTERED ([RegisteredCourseExamMarkID] ASC),
    CONSTRAINT [FK_RegisteredCourseExamMarks_OfferedCourseExams_OfferedCourseExamID] FOREIGN KEY ([OfferedCourseExamID]) REFERENCES [dbo].[OfferedCourseExams] ([OfferedCourseExamID]),
    CONSTRAINT [FK_RegisteredCourseExamMarks_RegisteredCourses_RegisteredCourseID] FOREIGN KEY ([RegisteredCourseID]) REFERENCES [dbo].[RegisteredCourses] ([RegisteredCourseID])
);




