﻿CREATE TABLE [dbo].[EnumValues] (
    [EnumValueID] INT            NOT NULL,
    [TableName]   NVARCHAR (128) NULL,
    [EnumType]    VARCHAR (100)  NOT NULL,
    [Name]        VARCHAR (100)  NOT NULL,
    [Value]       BIGINT         NOT NULL,
    [FullName]    VARCHAR (1000) NOT NULL,
    CONSTRAINT [PK_EnumValues] PRIMARY KEY CLUSTERED ([EnumValueID] ASC)
);








GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_EnumValues]
    ON [dbo].[EnumValues]([TableName] ASC, [EnumType] ASC, [Value] ASC);





