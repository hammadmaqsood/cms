﻿CREATE TABLE [dbo].[ProgramMajors] (
    [ProgramMajorID] INT          IDENTITY (1, 1) NOT NULL,
    [ProgramID]      INT          NOT NULL,
    [Majors]         VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ProgramMajors] PRIMARY KEY CLUSTERED ([ProgramMajorID] ASC),
    CONSTRAINT [FK_ProgramMajors_Programs_ProgramID] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[Programs] ([ProgramID]),
    CONSTRAINT [UK_ProgramMajors_ProgramID_Majors] UNIQUE NONCLUSTERED ([ProgramID] ASC, [Majors] ASC)
);











