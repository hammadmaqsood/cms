﻿CREATE TABLE [dbo].[GraduateDirectoryFormProjects] (
    [GraduateDirectoryFormProjectID] INT                IDENTITY (1, 1) NOT NULL,
    [GraduateDirectoryFormID]        INT                NOT NULL,
    [SemesterID]                     [dbo].[SemesterID] NOT NULL,
    [Description]                    VARCHAR (1000)     NOT NULL,
    [ProjectType]                    [dbo].[Enum]       NOT NULL,
    CONSTRAINT [PK_GraduateDirectoryFormProjects] PRIMARY KEY CLUSTERED ([GraduateDirectoryFormProjectID] ASC),
    CONSTRAINT [FK_GraduateDirectoryFormProjects_GraduateDirectoryForms_GraduateDirectoryFormID] FOREIGN KEY ([GraduateDirectoryFormID]) REFERENCES [dbo].[GraduateDirectoryForms] ([GraduateDirectoryFormID]),
    CONSTRAINT [FK_GraduateDirectoryFormProjects_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID])
);





