﻿CREATE TABLE [dbo].[FormStudentClearances] (
    [FormStudentClearanceID]                               UNIQUEIDENTIFIER NOT NULL,
    [FormID]                                               UNIQUEIDENTIFIER NOT NULL,
    [StudentID]                                            INT              NOT NULL,
    [Data]                                                 VARCHAR (MAX)    NULL,
    [StudentClearanceStatus]                               [dbo].[EnumGuid] NOT NULL,
    [StudentClearanceStatusChangedDate]                    DATETIME         NOT NULL,
    [StudentClearanceSSCStatus]                            [dbo].[EnumGuid] NOT NULL,
    [StudentClearanceSSCStatusChangedDate]                 DATETIME         NULL,
    [StudentClearanceSSCStatusChangedByUserLoginHistoryID] INT              NULL,
    [UniversityEmailConfirmationCode]                      CHAR (4)         NOT NULL,
    [PersonalEmailConfirmationCode]                        CHAR (4)         NOT NULL,
    CONSTRAINT [PK_FormStudentClearances] PRIMARY KEY CLUSTERED ([FormStudentClearanceID] ASC),
    CONSTRAINT [FK_FormStudentClearances_Forms_FormID] FOREIGN KEY ([FormID]) REFERENCES [dbo].[Forms] ([FormID]),
    CONSTRAINT [FK_FormStudentClearances_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [FK_FormStudentClearances_UserLoginHistory_StudentClearanceSSCStatusChangedByUserLoginHistoryID] FOREIGN KEY ([StudentClearanceSSCStatusChangedByUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID])
);
