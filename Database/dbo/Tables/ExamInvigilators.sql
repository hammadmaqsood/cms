﻿CREATE TABLE [dbo].[ExamInvigilators] (
    [ExamInvigilatorID] INT                IDENTITY (1, 1) NOT NULL,
    [InstituteID]       INT                NOT NULL,
    [DepartmentID]      INT                NULL,
    [ExamDesignationID] INT                NOT NULL,
    [Name]              [dbo].[PersonName] NOT NULL,
    [CNIC]              [dbo].[CNIC]       NULL,
    [Bank]              VARCHAR (50)       NULL,
    [AccountNo]         VARCHAR (50)       NULL,
    [Status]            [dbo].[Enum]       NOT NULL,
    [MigrationID]       INT                NULL,
    CONSTRAINT [PK_ExamInvigilators] PRIMARY KEY CLUSTERED ([ExamInvigilatorID] ASC),
    CONSTRAINT [FK_ExamInvigilators_Departments_DepartmentID] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Departments] ([DepartmentID]),
    CONSTRAINT [FK_ExamInvigilators_ExamDesignations_ExamDesignationID] FOREIGN KEY ([ExamDesignationID]) REFERENCES [dbo].[ExamDesignations] ([ExamDesignationID]),
    CONSTRAINT [FK_ExamInvigilators_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Active, Inactive', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ExamInvigilators', @level2type = N'COLUMN', @level2name = N'Name';

