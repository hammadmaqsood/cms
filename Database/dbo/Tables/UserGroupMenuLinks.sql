﻿CREATE TABLE [dbo].[UserGroupMenuLinks] (
    [UserGroupMenuLinkID]       INT              IDENTITY (1, 1) NOT NULL,
    [UserGroupID]               INT              NOT NULL,
    [ParentUserGroupMenuLinkID] INT              NULL,
    [AspirePageGuid]            UNIQUEIDENTIFIER NULL,
    [LinkText]                  VARCHAR (100)    NULL,
    [LinkUrl]                   [dbo].[Url]      NULL,
    [DisplayIndex]              TINYINT          NOT NULL,
    [Visible]                   BIT              NOT NULL,
    CONSTRAINT [PK_UserGroupMenuLinks] PRIMARY KEY CLUSTERED ([UserGroupMenuLinkID] ASC),
    CONSTRAINT [FK_UserGroupMenuLinks_UserGroupMenuLinks_ParentUserGroupMenuLinkID] FOREIGN KEY ([ParentUserGroupMenuLinkID]) REFERENCES [dbo].[UserGroupMenuLinks] ([UserGroupMenuLinkID]),
    CONSTRAINT [FK_UserGroupMenuLinks_UserGroups_UserGroupID] FOREIGN KEY ([UserGroupID]) REFERENCES [dbo].[UserGroups] ([UserGroupID])
);


GO
