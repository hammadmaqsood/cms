﻿CREATE TABLE [dbo].[UserFeedbackMembers] (
    [UserFeedbackMemberID] UNIQUEIDENTIFIER   NOT NULL,
    [FeedbackBoxType]      [dbo].[EnumGuid]   NOT NULL,
    [InstituteID]          INT                NULL,
    [MemberDesignation]    VARCHAR (500)      NOT NULL,
    [MemberName]           [dbo].[PersonName] NOT NULL,
    [Status]               [dbo].[EnumGuid]   NOT NULL,
    CONSTRAINT [PK_UserFeedbackMembers] PRIMARY KEY CLUSTERED ([UserFeedbackMemberID] ASC),
    CONSTRAINT [FK_UserFeedbackMembers_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID]),
    CONSTRAINT [UK_UserFeedbackMembers_FeedbackBoxType_InstituteID_MemberDesignation] UNIQUE NONCLUSTERED ([FeedbackBoxType], [InstituteID], [MemberDesignation] ASC)
);

