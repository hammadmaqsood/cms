﻿CREATE TABLE [dbo].[StudentCustomInfos] (
    [StudentCustomInfoID]         INT              IDENTITY (1, 1) NOT NULL,
    [StudentID]                   INT              NOT NULL,
    [Type]                        [dbo].[EnumGuid] NULL,
    [ReferenceDate]               DATE             NULL,
    [Subject]                     VARCHAR (MAX)    NOT NULL,
    [Description]                 VARCHAR (MAX)    NOT NULL,
    [CreatedByUserLoginHistoryID] INT              NOT NULL,
    [CreatedDate]                 DATETIME         NOT NULL,
    CONSTRAINT [PK_StudentCustomInfos] PRIMARY KEY CLUSTERED ([StudentCustomInfoID] ASC),
    CONSTRAINT [FK_StudentCustomInfos_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [FK_StudentCustomInfos_UserLoginHistory_CreatedByUserLoginHistoryID] FOREIGN KEY ([CreatedByUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID])
);



