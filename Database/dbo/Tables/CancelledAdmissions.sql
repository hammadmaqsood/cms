﻿CREATE TABLE [dbo].[CancelledAdmissions] (
    [CancelledAdmissionID] INT                IDENTITY (1, 1) NOT NULL,
    [StudentID]            INT                NOT NULL,
    [SemesterID]           [dbo].[SemesterID] NOT NULL,
    [Status]               [dbo].[Enum]       NOT NULL,
    [Remarks]              [dbo].[Remarks]    NULL,
    CONSTRAINT [PK_CancelledAdmissions] PRIMARY KEY CLUSTERED ([CancelledAdmissionID] ASC),
    CONSTRAINT [FK_CancelledAdmissions_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Blocked, Resolved', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CancelledAdmissions', @level2type = N'COLUMN', @level2name = N'Status';

