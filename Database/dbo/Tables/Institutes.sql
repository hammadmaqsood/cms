﻿CREATE TABLE [dbo].[Institutes] (
    [InstituteID]        INT               NOT NULL,
    [InstituteName]      [dbo].[Name]      NOT NULL,
    [InstituteShortName] [dbo].[ShortName] NOT NULL,
    [InstituteAlias]     [dbo].[Alias]     NOT NULL,
    [InstituteCode]      CHAR (2)          NOT NULL,
    [Website]            [dbo].[Url]       NULL,
    [MailAddress]        [dbo].[Address]   NULL,
    [Phone]              [dbo].[Phone]     NULL,
    [Fax]                [dbo].[Phone]     NULL,
    [Status]             [dbo].[Enum]      NOT NULL,
    [MigrationID]        INT               NULL,
    CONSTRAINT [PK_Institutes] PRIMARY KEY CLUSTERED ([InstituteID] ASC)
);




GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Used in Enrollment Generation',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Institutes',
    @level2type = N'COLUMN',
    @level2name = N'InstituteCode'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Full Name',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Institutes',
    @level2type = N'COLUMN',
    @level2name = N'InstituteName'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Short Name, Used in reports.',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Institutes',
    @level2type = N'COLUMN',
    @level2name = N'InstituteShortName'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Used in dropdowns',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Institutes',
    @level2type = N'COLUMN',
    @level2name = N'InstituteAlias'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Active/Inactive',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Institutes',
    @level2type = N'COLUMN',
    @level2name = N'Status'