﻿CREATE TABLE [dbo].[ThesisInternships] (
    [ThesisInternshipID] INT                 IDENTITY (1, 1) NOT NULL,
    [StudentID]          INT                 NOT NULL,
    [SemesterID]         [dbo].[SemesterID]  NOT NULL,
    [RecordType]         [dbo].[Enum]        NOT NULL,
    [CreditHours]        [dbo].[CreditHours] NOT NULL,
    [Title]              VARCHAR (MAX)       NOT NULL,
    [Organization]       VARCHAR (MAX)       NULL,
    [FromDate]           DATETIME            NULL,
    [ToDate]             DATETIME            NULL,
    [DefenceDate]        DATETIME            NULL,
    [SubmissionDate]     DATETIME            NULL,
    [Remarks]            [dbo].[Remarks]     NULL,
    [Total]              [dbo].[ExamMarks]   NOT NULL,
    [Grade]              [dbo].[Enum]        NOT NULL,
    [GradePoint]         DECIMAL (5, 2)      NOT NULL,
    [MarksEntryLocked]   BIT                 NOT NULL,
    CONSTRAINT [PK_ThesisInternships] PRIMARY KEY CLUSTERED ([ThesisInternshipID] ASC),
    CONSTRAINT [FK_ThesisInternships_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID])
);




