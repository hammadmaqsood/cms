﻿CREATE TABLE [dbo].[GraduateDirectoryFormWorkExperiences] (
    [GraduateDirectoryFormWorkExperienceID] INT            IDENTITY (1, 1) NOT NULL,
    [GraduateDirectoryFormID]               INT            NOT NULL,
    [Designation]                           VARCHAR (1000) NOT NULL,
    [Organization]                          VARCHAR (1000) NOT NULL,
    [Duration]                              VARCHAR (1000) NOT NULL,
    [Roles]                                 VARCHAR (1000) NOT NULL,
    [ExperienceType]                        [dbo].[Enum]   NULL,
    CONSTRAINT [PK_GraduateDirectoryFormWorkExperiences] PRIMARY KEY CLUSTERED ([GraduateDirectoryFormWorkExperienceID] ASC),
    CONSTRAINT [FK_GraduateDirectoryFormWorkExperiences_GraduateDirectoryForms_GraduateDirectoryFormID] FOREIGN KEY ([GraduateDirectoryFormID]) REFERENCES [dbo].[GraduateDirectoryForms] ([GraduateDirectoryFormID])
);





