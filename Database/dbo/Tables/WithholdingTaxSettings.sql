﻿CREATE TABLE [dbo].[WithholdingTaxSettings] (
    [WithholdingTaxSettingID]  INT        IDENTITY (1, 1) NOT NULL,
    [InstituteID]              INT        NOT NULL,
    [FinancialYear]            SMALLINT   NOT NULL,
    [ApplicableOnAmount]       INT        NOT NULL,
    [WithholdingTaxPercentage] FLOAT (53) NOT NULL,
    CONSTRAINT [PK_WithholdingTaxSettings] PRIMARY KEY CLUSTERED ([WithholdingTaxSettingID] ASC),
    CONSTRAINT [CK_WithholdingTaxSettings ([ApplicableOnAmount]]>=(0) AND [WithholdingTaxPercentage]]>=(0) AND [FinancialYear]]>=(2018))] CHECK ([ApplicableOnAmount]>=(0) AND [WithholdingTaxPercentage]>=(0) AND [FinancialYear]>=(2018)),
    CONSTRAINT [FK_WithholdingTaxSettings_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID]),
    CONSTRAINT [UK_WithholdingTaxSettings_InstituteID_FinancialYear] UNIQUE NONCLUSTERED ([InstituteID] ASC, [FinancialYear] ASC)
);

