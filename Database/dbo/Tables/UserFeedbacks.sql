﻿CREATE TABLE [dbo].[UserFeedbacks] (
    [UserFeedbackID]          UNIQUEIDENTIFIER NOT NULL,
    [UserLoginHistoryID]      INT              NOT NULL,
    [FeedbackBoxType]         [dbo].[EnumGuid] NOT NULL,
    [FeedbackNo]              VARCHAR (50)     NOT NULL,
    [FeedbackType]            [dbo].[EnumGuid] NOT NULL,
    [FeedbackDetails]         VARCHAR (MAX)    NOT NULL,
    [FeedbackDate]            DATETIME         NOT NULL,
    [Status]                  [dbo].[EnumGuid] NOT NULL,
    [Notes]                   VARCHAR (MAX)    NULL,
    [Archived]                BIT              NOT NULL,
    [LastUpdatedDate]         DATETIME         NOT NULL,
    [Unread]                  BIT              NOT NULL,
    [MovedFromFeedbackBoxType] [dbo].[EnumGuid] NULL,
    CONSTRAINT [PK_UserFeedbacks] PRIMARY KEY CLUSTERED ([UserFeedbackID] ASC),
    CONSTRAINT [FK_UserFeedbacks_UserLoginHistory_UserLoginHistoryID] FOREIGN KEY ([UserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID]),
    CONSTRAINT [UK_UserFeedbacks_FeedbackNo] UNIQUE NONCLUSTERED ([FeedbackNo] ASC)
);
