﻿CREATE TABLE [dbo].[Faculties] (
    [FacultyID]        UNIQUEIDENTIFIER  NOT NULL,
    [InstituteID]      INT               NOT NULL,
    [FacultyName]      [dbo].[Name]      NOT NULL,
    [FacultyShortName] [dbo].[ShortName] NOT NULL,
    [FacultyAlias]     [dbo].[Alias]     NOT NULL,
    CONSTRAINT [PK_Faculties] PRIMARY KEY CLUSTERED ([FacultyID] ASC),
    CONSTRAINT [FK_Faculties_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);

