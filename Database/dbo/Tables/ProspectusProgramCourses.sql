﻿CREATE TABLE [dbo].[ProspectusProgramCourses] (
    [ProspectusProgramCourseID] UNIQUEIDENTIFIER    NOT NULL,
    [ProspectusProgramID]       UNIQUEIDENTIFIER    NOT NULL,
    [CourseCode]                VARCHAR (20)        NOT NULL,
    [CourseTitle]               VARCHAR (250)       NOT NULL,
    [CreditHours]               [dbo].[CreditHours] NOT NULL,
    [ContactHours]              [dbo].[CreditHours] NOT NULL,
    [CourseCategory]            [dbo].[Enum]        NOT NULL,
    [CourseType]                [dbo].[Encrypted]   NOT NULL,
    [SemesterNo]                [dbo].[SemesterNo]  NOT NULL,
    [SummerGradeCapping]        BIT                 NOT NULL,
    [ProspectusProgramMajorID]  UNIQUEIDENTIFIER    NULL,
    CONSTRAINT [PK_ProspectusProgramCourses] PRIMARY KEY CLUSTERED ([ProspectusProgramCourseID] ASC),
    CONSTRAINT [FK_ProspectusProgramCourses_ProspectusProgramMajors_ProspectusProgramMajorID] FOREIGN KEY ([ProspectusProgramMajorID]) REFERENCES [dbo].[ProspectusProgramMajors] ([ProspectusProgramMajorID]),
    CONSTRAINT [FK_ProspectusProgramCourses_ProspectusPrograms_ProspectusProgramID] FOREIGN KEY ([ProspectusProgramID]) REFERENCES [dbo].[ProspectusPrograms] ([ProspectusProgramID])
);

