﻿CREATE TABLE [dbo].[ScholarshipAnnouncementPrograms] (
    [ScholarshipAnnouncementProgramID] INT IDENTITY (1, 1) NOT NULL,
    [ScholarshipAnnouncementID]        INT NOT NULL,
    [ProgramID]                        INT NULL,
    [AdmissionOpenProgramID]           INT NULL,
    CONSTRAINT [PK_ScholarshipAnnouncementPrograms] PRIMARY KEY CLUSTERED ([ScholarshipAnnouncementProgramID] ASC),
    CONSTRAINT [FK_ScholarshipAnnouncementPrograms_AdmissionOpenPrograms_AdmissionOpenProgramID] FOREIGN KEY ([AdmissionOpenProgramID]) REFERENCES [dbo].[AdmissionOpenPrograms] ([AdmissionOpenProgramID]),
    CONSTRAINT [FK_ScholarshipAnnouncementPrograms_Programs_ProgramID] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[Programs] ([ProgramID]),
    CONSTRAINT [FK_ScholarshipAnnouncementPrograms_ScholarshipAnnouncements_ScholarshipAnnouncementID] FOREIGN KEY ([ScholarshipAnnouncementID]) REFERENCES [dbo].[ScholarshipAnnouncements] ([ScholarshipAnnouncementID])
);

