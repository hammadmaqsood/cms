﻿CREATE TABLE [dbo].[QASurveyConducts] (
    [QASurveyConductID] INT                IDENTITY (1, 1) NOT NULL,
    [SurveyTypeGuid]    UNIQUEIDENTIFIER   NOT NULL,
    [SemesterID]        [dbo].[SemesterID] NULL,
    [InstituteID]       INT                NULL,
    [DepartmentID]      INT                NULL,
    [ProgramID]         INT                NULL,
    [StartDate]         DATETIME           NOT NULL,
    [EndDate]           DATETIME           NULL,
    [SurveyStatus]      [dbo].[Enum]       NOT NULL,
    CONSTRAINT [PK_QASurveyConducts] PRIMARY KEY CLUSTERED ([QASurveyConductID] ASC),
    CONSTRAINT [FK_QASurveyConducts_Departments_DepartmentID] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Departments] ([DepartmentID]),
    CONSTRAINT [FK_QASurveyConducts_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID]),
    CONSTRAINT [FK_QASurveyConducts_Programs_ProgramID] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[Programs] ([ProgramID])
);







