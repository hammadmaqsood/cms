﻿CREATE TABLE [dbo].[ExamRemarksPolicyDetails] (
    [ExamRemarksPolicyDetailID] INT          IDENTITY (1, 1) NOT NULL,
    [ExamRemarksPolicyID]       INT          NOT NULL,
    [Sequence]                  TINYINT      NOT NULL,
    [LessThanCGPA]              [dbo].[GPA]  NOT NULL,
    [ExamRemarksType]           [dbo].[Enum] NOT NULL,
    [FirstSemester]             BIT          NOT NULL,
    CONSTRAINT [PK_ExamRemarksPolicyDetails] PRIMARY KEY CLUSTERED ([ExamRemarksPolicyDetailID] ASC),
    CONSTRAINT [FK_ExamRemarksPolicyDetails_ExamRemarksPolicies_ExamRemarksPolicyID] FOREIGN KEY ([ExamRemarksPolicyID]) REFERENCES [dbo].[ExamRemarksPolicies] ([ExamRemarksPolicyID]),
    CONSTRAINT [UK_ExamRemarksPolicyDetails_ExamRemarksPolicyID_Sequence_FirstSemester] UNIQUE NONCLUSTERED ([ExamRemarksPolicyID] ASC, [Sequence] ASC, [FirstSemester] ASC)
);



