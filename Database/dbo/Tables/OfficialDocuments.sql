﻿CREATE TABLE [dbo].[OfficialDocuments] (
    [OfficialDocumentID] INT                 IDENTITY (1, 1) NOT NULL,
    [InstituteID]        INT                 NULL,
    [UserTypes]          [dbo].[EnumFlags16] NOT NULL,
    [SystemFileID]       UNIQUEIDENTIFIER    NOT NULL,
    [DocumentName]       VARCHAR (1000)      NOT NULL,
    [ProvidedBy]         VARCHAR (1000)      NOT NULL,
    [UploadedOn]         DATETIME            NOT NULL,
    [Status]             [dbo].[EnumGuid]    NOT NULL,
    CONSTRAINT [PK_OfficialDocuments] PRIMARY KEY CLUSTERED ([OfficialDocumentID] ASC),
    CONSTRAINT [FK_OfficialDocuments_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);







