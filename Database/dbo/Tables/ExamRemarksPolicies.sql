﻿CREATE TABLE [dbo].[ExamRemarksPolicies] (
    [ExamRemarksPolicyID]          INT                IDENTITY (1, 1) NOT NULL,
    [InstituteID]                  INT                NOT NULL,
    [ExamRemarksPolicyName]        VARCHAR (500)      NOT NULL,
    [ExamRemarksPolicyDescription] [dbo].[Remarks]    NOT NULL,
    [ValidFromSemesterID]          [dbo].[SemesterID] NOT NULL,
    [ValidUpToSemesterID]          [dbo].[SemesterID] NULL,
    CONSTRAINT [PK_ExamRemarksPolicies] PRIMARY KEY CLUSTERED ([ExamRemarksPolicyID] ASC),
    CONSTRAINT [FK_ExamRemarksPolicies_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID]),
    CONSTRAINT [FK_ExamRemarksPolicies_Semesters_ValidFromSemesterID] FOREIGN KEY ([ValidFromSemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID]),
    CONSTRAINT [FK_ExamRemarksPolicies_Semesters_ValidUptoSemesterID] FOREIGN KEY ([ValidUpToSemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID])
);

