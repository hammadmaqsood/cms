﻿CREATE TABLE [dbo].[DisciplineCaseStudentActivities] (
    [DisciplineCaseStudentActivityID] INT IDENTITY (1, 1) NOT NULL,
    [DisciplineCasesStudentID]        INT NOT NULL,
    [DisciplineCaseActivityID]        INT NOT NULL,
    CONSTRAINT [PK_DisciplineCaseStudentActivities] PRIMARY KEY CLUSTERED ([DisciplineCaseStudentActivityID] ASC),
    CONSTRAINT [FK_DisciplineCaseStudentActivities_DisciplineCaseActivities_DisciplineCaseActivityID] FOREIGN KEY ([DisciplineCaseActivityID]) REFERENCES [dbo].[DisciplineCaseActivities] ([DisciplineCaseActivityID]),
    CONSTRAINT [FK_DisciplineCaseStudentActivities_DisciplineCasesStudents_DisciplineCasesStudentID] FOREIGN KEY ([DisciplineCasesStudentID]) REFERENCES [dbo].[DisciplineCasesStudents] ([DisciplineCasesStudentID])
);
