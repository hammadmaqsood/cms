﻿CREATE TABLE [dbo].[SystemFiles] (
    [SystemFileID]              UNIQUEIDENTIFIER    NOT NULL,
    [SystemFolderID]              UNIQUEIDENTIFIER    NOT NULL, 
    [FileName]                    [dbo].[FileName]    NOT NULL,
    [FileExtension]                    [dbo].[EnumString] NOT NULL,
    [FileContentsType]            [dbo].[EnumString] NOT NULL,
    [ChecksumMD5]                 VARCHAR (32)        NOT NULL,
    [FileSize]                    INT                 NOT NULL,
    [CreatedDate]                 DATETIME            NOT NULL,
    [CreatedByUserLoginHistoryID] INT                 NOT NULL,
    [DeletedDate]                 DATETIME            NULL,
    [DeletedByUserLoginHistoryID] INT                 NULL,
    CONSTRAINT [PK_SystemFiles] PRIMARY KEY CLUSTERED ([SystemFileID] ASC),
    CONSTRAINT [FK_SystemFiles_UserLoginHistory_CreatedByUserLoginHistoryID] FOREIGN KEY ([CreatedByUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID]),
    CONSTRAINT [FK_SystemFiles_UserLoginHistory_DeletedByUserLoginHistoryID] FOREIGN KEY ([DeletedByUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID]),
    CONSTRAINT [FK_SystemFiles_SystemFolders_SystemFolderID] FOREIGN KEY ([SystemFolderID]) REFERENCES [dbo].[SystemFolders] ([SystemFolderID])
);









