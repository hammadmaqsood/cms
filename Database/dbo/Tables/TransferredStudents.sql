﻿CREATE TABLE [dbo].[TransferredStudents] (
    [TransferredStudentID]     INT                IDENTITY (1, 1) NOT NULL,
    [TransferredFromStudentID] INT                NOT NULL,
    [TransferredToStudentID]   INT                NOT NULL,
    [TransferSemesterID]       [dbo].[SemesterID] NOT NULL,
    [TransferDate]             DATETIME           NOT NULL,
    [Remarks]                  [dbo].[Remarks]    NULL,
    CONSTRAINT [PK_TransferredStudents] PRIMARY KEY CLUSTERED ([TransferredStudentID] ASC),
    CONSTRAINT [FK_TransferredStudents_Students_TransferredFromStudentID] FOREIGN KEY ([TransferredFromStudentID]) REFERENCES [dbo].[Students] ([StudentID]),
    CONSTRAINT [FK_TransferredStudents_Students_TransferredToStudentID] FOREIGN KEY ([TransferredToStudentID]) REFERENCES [dbo].[Students] ([StudentID])
);







