﻿CREATE TABLE [dbo].[ExamMarksPolicyPrograms] (
    [ExamMarksPolicyProgramID] INT IDENTITY (1, 1) NOT NULL,
    [ExamMarksPolicyID]        INT NOT NULL,
    [ProgramID]                INT NOT NULL,
    CONSTRAINT [PK_ExamMarksPolicyPrograms] PRIMARY KEY CLUSTERED ([ExamMarksPolicyProgramID] ASC),
    CONSTRAINT [FK_ExamMarksPolicyPrograms_ExamMarksPolicies_ExamMarksPolicyID] FOREIGN KEY ([ExamMarksPolicyID]) REFERENCES [dbo].[ExamMarksPolicies] ([ExamMarksPolicyID]),
    CONSTRAINT [FK_ExamMarksPolicyPrograms_Programs_ProgramID] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[Programs] ([ProgramID]),
    CONSTRAINT [UK_ExamMarksPolicyPrograms_ExamMarksPolicyProgramID_ProgramID] UNIQUE NONCLUSTERED ([ExamMarksPolicyProgramID] ASC, [ProgramID] ASC)
);

