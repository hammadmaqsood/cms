﻿CREATE TABLE [dbo].[FeeDefaulterJobSemesters] (
    [FeeDefaulterJobSemesterID] INT                IDENTITY (1, 1) NOT NULL,
    [InstituteID]               INT                NOT NULL,
    [SemesterID]                [dbo].[SemesterID] NOT NULL,
    [Status]                    [dbo].[Enum]       NOT NULL,
    CONSTRAINT [PK_FeeDefaulterJobSemesters] PRIMARY KEY CLUSTERED ([FeeDefaulterJobSemesterID] ASC),
    CONSTRAINT [FK_FeeDefaulterJobSemesters_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID]),
    CONSTRAINT [FK_FeeDefaulterJobSemesters_Semesters_SemesterID] FOREIGN KEY ([SemesterID]) REFERENCES [dbo].[Semesters] ([SemesterID]),
    CONSTRAINT [UK_FeeDefaulterJobSemesters_InstituteID_SemesterID] UNIQUE NONCLUSTERED ([InstituteID] ASC, [SemesterID] ASC)
);



