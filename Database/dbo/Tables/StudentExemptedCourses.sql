﻿CREATE TABLE [dbo].[StudentExemptedCourses] (
    [StudentExemptedCourseID] INT             IDENTITY (1, 1) NOT NULL,
    [StudentID]               INT             NOT NULL,
    [CourseID]                INT             NOT NULL,
    [SubstitutedCourseID]     INT             NULL,
    [ExemptionDate]           DATE            NOT NULL,
    [Remarks]                 [dbo].[Remarks] NOT NULL,
    CONSTRAINT [PK_StudentExemptedCourses] PRIMARY KEY CLUSTERED ([StudentExemptedCourseID] ASC),
    CONSTRAINT [FK_StudentExemptedCourses_Courses_CourseID] FOREIGN KEY ([CourseID]) REFERENCES [dbo].[Courses] ([CourseID]),
    CONSTRAINT [FK_StudentExemptedCourses_Courses_SubstitutedCourseID] FOREIGN KEY ([SubstitutedCourseID]) REFERENCES [dbo].[Courses] ([CourseID]),
    CONSTRAINT [FK_StudentExemptedCourses_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID])
);









