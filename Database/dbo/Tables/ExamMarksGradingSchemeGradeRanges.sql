﻿CREATE TABLE [dbo].[ExamMarksGradingSchemeGradeRanges] (
    [ExamMarksGradingSchemesGradeRangeID] INT               IDENTITY (1, 1) NOT NULL,
    [ExamMarksGradingSchemeID]            INT               NOT NULL,
    [RegularSemester]                     BIT               NOT NULL,
    [Grade]                               [dbo].[Grades]    NOT NULL,
    [MinMarks]                            [dbo].[ExamMarks] NOT NULL,
    [MaxMarks]                            [dbo].[ExamMarks] NOT NULL,
    [GradePoints]                         [dbo].[GPA]       NOT NULL,
    CONSTRAINT [PK_ExamMarksGradingSchemeGradeRanges] PRIMARY KEY CLUSTERED ([ExamMarksGradingSchemesGradeRangeID] ASC),
    CONSTRAINT [FK_ExamMarksGradingSchemeGradeRanges_ExamMarksGradingSchemes_ExamMarksGradingSchemeID] FOREIGN KEY ([ExamMarksGradingSchemeID]) REFERENCES [dbo].[ExamMarksGradingSchemes] ([ExamMarksGradingSchemeID]),
    CONSTRAINT [UK_ExamMarksGradingSchemeGradeRanges_ExamMarksGradingSchemeID_RegularSemester_Grade] UNIQUE NONCLUSTERED ([ExamMarksGradingSchemeID] ASC, [RegularSemester] ASC, [Grade] ASC)
);















