﻿CREATE TABLE [dbo].[StudentCustomFieldValues] (
    [StudentCustomFieldValueID] INT           IDENTITY (1, 1) NOT NULL,
    [StudentID]                 INT           NOT NULL,
    [CustomFieldID]             INT           NOT NULL,
    [Value]                     VARCHAR (MAX) NULL,
    CONSTRAINT [PK_StudentCustomFieldValues] PRIMARY KEY CLUSTERED ([StudentCustomFieldValueID] ASC),
    CONSTRAINT [FK_StudentCustomFieldValues_CustomFields_CustomFieldID] FOREIGN KEY ([CustomFieldID]) REFERENCES [dbo].[CustomFields] ([CustomFieldID]),
    CONSTRAINT [FK_StudentCustomFieldValues_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Custom Fields data of Student.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'StudentCustomFieldValues';

