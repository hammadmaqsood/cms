﻿CREATE TABLE [dbo].[ExamOfferedRooms] (
    [ExamOfferedRoomID] INT          IDENTITY (1, 1) NOT NULL,
    [ExamConductID]     INT          NOT NULL,
    [ExamRoomID]        INT          NOT NULL,
    [Rows]              TINYINT      NOT NULL,
    [Columns]           TINYINT      NOT NULL,
    [RoomUsageType]     [dbo].[Enum] NOT NULL,
    [Sequence]          SMALLINT     NOT NULL,
    CONSTRAINT [PK_ExamOfferedRooms] PRIMARY KEY CLUSTERED ([ExamOfferedRoomID] ASC),
    CONSTRAINT [CK_ExamOfferedRooms ([Rows]]>(0) AND [Columns]]>(0))] CHECK ([Rows]>(0) AND [Columns]>(0)),
    CONSTRAINT [FK_ExamOfferedRooms_ExamConducts_ExamConductID] FOREIGN KEY ([ExamConductID]) REFERENCES [dbo].[ExamConducts] ([ExamConductID]),
    CONSTRAINT [FK_ExamOfferedRooms_ExamRooms_ExamRoomID] FOREIGN KEY ([ExamRoomID]) REFERENCES [dbo].[ExamRooms] ([ExamRoomID])
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Regular, Clashes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ExamOfferedRooms', @level2type = N'COLUMN', @level2name = N'RoomUsageType';

