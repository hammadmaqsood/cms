﻿CREATE TABLE [dbo].[Countries] (
    [CountryID]          INT          IDENTITY (1, 1) NOT NULL,
    [CountryAbbrivation] CHAR (2)     NOT NULL,
    [CountryName]        VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED ([CountryID] ASC)
);


