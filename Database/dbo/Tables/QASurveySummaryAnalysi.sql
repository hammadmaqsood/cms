﻿CREATE TABLE [dbo].[QASurveySummaryAnalysi] (
    [QASurveySummaryAnalysisID]     INT                IDENTITY (1, 1) NOT NULL,
    [QASurveyConductID]             INT                NOT NULL,
    [SummaryTypeGuid]               UNIQUEIDENTIFIER   NOT NULL,
    [SemesterID]                    [dbo].[SemesterID] NOT NULL,
    [DepartmentID]                  INT                NULL,
    [ProgramID]                     INT                NULL,
    [SummaryValues]                 VARCHAR (MAX)      NULL,
    [SubmittedByUserLoginHistoryID] INT                NOT NULL,
    [SubmissionDate]                DATETIME           NULL,
    [Status]                        [dbo].[Enum]       NOT NULL,
    CONSTRAINT [PK_QASurveySummaryAnalysi] PRIMARY KEY CLUSTERED ([QASurveySummaryAnalysisID] ASC),
    CONSTRAINT [FK_QASurveySummaryAnalysi_Departments_DepartmentID] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Departments] ([DepartmentID]),
    CONSTRAINT [FK_QASurveySummaryAnalysi_Programs_ProgramID] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[Programs] ([ProgramID]),
    CONSTRAINT [FK_QASurveySummaryAnalysi_QASurveyConducts_QASurveyConductID] FOREIGN KEY ([QASurveyConductID]) REFERENCES [dbo].[QASurveyConducts] ([QASurveyConductID]),
    CONSTRAINT [FK_QASurveySummaryAnalysi_UserLoginHistory_SubmittedByUserLoginHistoryID] FOREIGN KEY ([SubmittedByUserLoginHistoryID]) REFERENCES [dbo].[UserLoginHistory] ([UserLoginHistoryID])
);
