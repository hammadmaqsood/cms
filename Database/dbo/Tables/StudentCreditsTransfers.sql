﻿CREATE TABLE [dbo].[StudentCreditsTransfers] (
    [StudentCreditsTransferID] INT                IDENTITY (1, 1) NOT NULL,
    [StudentID]                INT                NOT NULL,
    [InstituteName]            VARCHAR (500)      NOT NULL,
    [TransferredDate]          DATE               NOT NULL,
    [TransferredSemesterID]    [dbo].[SemesterID] NOT NULL,
    [CommitteeMembers]         VARCHAR (1000)     NULL,
    [Remarks]                  [dbo].[Remarks]    NULL,
    CONSTRAINT [PK_StudentCreditsTransfers] PRIMARY KEY CLUSTERED ([StudentCreditsTransferID] ASC),
    CONSTRAINT [FK_StudentCreditsTransfers_Students_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Students] ([StudentID])
);
