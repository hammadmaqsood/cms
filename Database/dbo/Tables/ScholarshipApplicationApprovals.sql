﻿CREATE TABLE [dbo].[ScholarshipApplicationApprovals] (
    [ScholarshipApplicationApprovalID] INT                IDENTITY (1, 1) NOT NULL,
    [ScholarshipApplicationID]         INT                NOT NULL,
    [SemesterID]                       [dbo].[SemesterID] NOT NULL,
    [ApprovalDate]                             DATETIME           NOT NULL,
    [Data]                             VARCHAR (MAX)      NULL,
    [ApprovalStatus]                   [dbo].[Enum]       NOT NULL,
    [ChequeStatus] [dbo].[Enum] NOT NULL, 
    CONSTRAINT [PK_ScholarshipApplicationApprovals] PRIMARY KEY CLUSTERED ([ScholarshipApplicationApprovalID] ASC),
    CONSTRAINT [FK_ScholarshipApplicationApprovals_ScholarshipApplications_ScholarshipApplicationID] FOREIGN KEY ([ScholarshipApplicationID]) REFERENCES [dbo].[ScholarshipApplications] ([ScholarshipApplicationID])
);



