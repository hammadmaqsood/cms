﻿CREATE TABLE [dbo].[ExamDesignations] (
    [ExamDesignationID] INT          IDENTITY (1, 1) NOT NULL,
    [InstituteID]       INT          NOT NULL,
    [Designation]       VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ExamDesignations] PRIMARY KEY CLUSTERED ([ExamDesignationID] ASC),
    CONSTRAINT [FK_ExamDesignations_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID]),
    CONSTRAINT [UK_ExamDesignations_InstituteID_Designation] UNIQUE NONCLUSTERED ([InstituteID] ASC, [Designation] ASC)
);









