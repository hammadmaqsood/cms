﻿CREATE TABLE [dbo].[CBTSessions] (
    [CBTSessionID]      INT      IDENTITY (1, 1) NOT NULL,
    [InstituteID]       INT      NOT NULL,
    [StartTime]         DATETIME NOT NULL,
    [DurationInMinutes] TINYINT  NOT NULL,
    CONSTRAINT [PK_CBTSessions] PRIMARY KEY CLUSTERED ([CBTSessionID] ASC),
    CONSTRAINT [FK_CBTSessions_Institutes_InstituteID] FOREIGN KEY ([InstituteID]) REFERENCES [dbo].[Institutes] ([InstituteID])
);

