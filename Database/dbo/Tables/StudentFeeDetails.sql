﻿CREATE TABLE [dbo].[StudentFeeDetails] (
    [StudentFeeDetailID] INT IDENTITY (1, 1) NOT NULL,
    [StudentFeeID]       INT NOT NULL,
    [FeeHeadID]          INT NOT NULL,
    [TotalAmount]        INT NULL,
    [ConcessionAmount]   INT NULL,
    [GrandTotalAmount]   INT NOT NULL,
    CONSTRAINT [PK_StudentFeeDetails] PRIMARY KEY CLUSTERED ([StudentFeeDetailID] ASC),
    CONSTRAINT [CK_StudentFeeDetails (([TotalAmount]] IS NOT NULL OR [ConcessionAmount]] IS NOT NULL) AND (isnull([TotalAmount]],(0))-isnull([Conce] CHECK (([TotalAmount] IS NOT NULL OR [ConcessionAmount] IS NOT NULL) AND (isnull([TotalAmount],(0))-isnull([ConcessionAmount],(0)))=[GrandTotalAmount]),
    CONSTRAINT [FK_StudentFeeDetails_FeeHeads_FeeHeadID] FOREIGN KEY ([FeeHeadID]) REFERENCES [dbo].[FeeHeads] ([FeeHeadID]),
    CONSTRAINT [FK_StudentFeeDetails_StudentFees_StudentFeeID] FOREIGN KEY ([StudentFeeID]) REFERENCES [dbo].[StudentFees] ([StudentFeeID])
);
