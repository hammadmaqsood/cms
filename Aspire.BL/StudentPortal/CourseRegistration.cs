﻿using Aspire.BL.Common;
using Aspire.BL.Entities.CourseRegistration;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.StudentPortal
{
	public static class CourseRegistration
	{
		public static List<CustomOfferedCoursesForStudent> GetRegularOfferedCoursesForStudent(int studentID, out short? offeredSemesterID, out StudentClass studentClass, out bool canStudentRegisterCourseOnline)
		{
			using (var aspireContext = new AspireContext())
			{
				var student = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => new
				{
					s.StudentID,
					s.AdmissionOpenProgramID,
					s.AdmissionOpenProgram.IsSummerRegularSemester,
					s.AdmissionOpenProgram.ProgramID,
					s.AdmissionOpenProgram.Program.InstituteID,
					s.CanStudentRegisterCourseOnline
				}).Single();
				canStudentRegisterCourseOnline = student.CanStudentRegisterCourseOnline == null || student.CanStudentRegisterCourseOnline == true;
				offeredSemesterID = aspireContext.GetRegistrationOpenSemesterID(student.ProgramID, CourseRegistrationSetting.Types.Student);
				if (offeredSemesterID == null)
				{
					offeredSemesterID = aspireContext.GetRegistrationOpenSemesterID(student.ProgramID, CourseRegistrationSetting.Types.Coordinator);
					if (offeredSemesterID == null)
					{
						studentClass = null;
						return null;
					}
				}
				var _offeredSemesterID = offeredSemesterID.Value;
				studentClass = aspireContext.GetStudentClassDetails(studentID, _offeredSemesterID);
				if (studentClass == null)
					return null;
				var offeredCourses = aspireContext.GetOfferedCoursesForStudent(student.InstituteID, student.AdmissionOpenProgramID, _offeredSemesterID, studentClass);
				var registeredCourses = aspireContext.RegisteredCourses
					.Where(rc => rc.StudentID == student.StudentID && rc.DeletedDate == null && rc.OfferedCours.SemesterID == _offeredSemesterID)
					.Select(rc => new
					{
						rc.OfferedCourseID,
						rc.CourseID
					});
				foreach (var customOfferedCoursesForStudent in offeredCourses)
					customOfferedCoursesForStudent.Registered = registeredCourses.Any(rc => rc.OfferedCourseID == customOfferedCoursesForStudent.OfferedCourseID || rc.CourseID == customOfferedCoursesForStudent.CourseID || rc.CourseID == customOfferedCoursesForStudent.EquivalentCourseID);
				return offeredCourses;
			}
		}
	}
}
