﻿using Aspire.Model.Context;
using System.Linq;

namespace Aspire.BL.Common
{
	internal static class Queries
	{
		public static string GetEnrollmentFromStudentID(this AspireContext aspireContext, int studentID)
		{
			return aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => s.Enrollment).SingleOrDefault();
		}
	}
}
