﻿using System;

namespace Aspire.BL.Common
{
	public class SortingNotImplementedException : Exception
	{
		public SortingNotImplementedException(string sortProperty)
			: base(sortProperty)
		{ }
	}
}
