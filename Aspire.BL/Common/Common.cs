﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Aspire.BL.Common
{
	public static class Common
	{
		private static string ToFullName(Enum enumValue)
		{
			var extensionMethod = typeof(Institute).Assembly.GetTypes().Where(t => t.IsClass && t.IsSealed && !t.IsGenericType && !t.IsNested)
				.SelectMany(c => c.GetMethods(BindingFlags.Static | BindingFlags.Public))
				.Where(m => m.IsDefined(typeof(ExtensionAttribute), false))
				.Where(m => m.GetParameters().Length == 1 && m.GetParameters()[0].ParameterType == enumValue.GetType())
				.Where(m => m.ReturnType == typeof(string))
				.Where(m => m.Name == "ToFullName")
				.ToList();
			if (extensionMethod.Count == 1)
				return (string)extensionMethod.Single().Invoke(null, new object[] { enumValue });
			return null;
		}

		public static void InitEnumValues()
		{
#if DEBUG
			if (Configurations.Current.AppSettings.IsDevelopmentPC)
				return;
#endif
			var assembly = typeof(Institute).Assembly;
			var enums = assembly.GetTypes().Where(t => t.IsEnum).ToList();
			var enumValues = enums.SelectMany(e =>
				Enum.GetValues(e).Cast<Enum>().Select(t => new EnumValue
				{
					TableName = e.DeclaringType?.Name,
					EnumType = e.Name,
					Name = t.ToString(),
					Value = Convert.ToInt64(t),
					FullName = ToFullName(t) ?? t.ToString(),
				})).ToList();

			var enumValueID = 1;
			enumValues.OrderBy(e => e.TableName).ThenBy(e => e.EnumType).ThenBy(e => e.Value)
				.ForEach(e => e.EnumValueID = enumValueID++);

			using (var aspireContext = new AspireContext())
			{
				var tableID = 1;
				var tables = ((IObjectContextAdapter)aspireContext).ObjectContext.MetadataWorkspace.GetItemCollection(DataSpace.CSSpace)
					.GetItems<EntityContainerMapping>().Single().EntitySetMappings
					.Select(t => new
					{
						TableName = t.EntityTypeMappings[0].Fragments[0].StoreEntitySet.Name,
						TableSchema = t.EntityTypeMappings[0].Fragments[0].StoreEntitySet.Schema,
						EntityName = t.EntityTypeMappings[0].EntityType.Name,
					})
					.Where(t => t.TableSchema != null).OrderBy(t => t.TableSchema).ThenBy(t => t.TableName)
					.Select(t => new TableName
					{
						TableID = tableID++,
						TableSchema = t.TableSchema,
						TableName1 = t.TableName,
						EntityName = t.EntityName
					}).ToList();

				var dbTables = aspireContext.TableNames.OrderBy(t => t.TableSchema).ThenBy(t => t.TableName1).ToList();
				if (dbTables.ToJsonString() != tables.ToJsonString())
				{
					aspireContext.TableNames.RemoveRange(dbTables);
					aspireContext.SaveChangesAsSystemUser(false);
					aspireContext.TableNames.AddRange(tables);
					aspireContext.SaveChangesAsSystemUser(false);
				}

				var dbEnumValues = aspireContext.EnumValues.OrderBy(e => e.EnumValueID).ToList();
				if (dbEnumValues.ToJsonString() != enumValues.ToJsonString())
				{
					aspireContext.EnumValues.RemoveRange(dbEnumValues);
					aspireContext.SaveChangesAsSystemUser(false);
					aspireContext.EnumValues.AddRange(enumValues);
					aspireContext.SaveChangesAsSystemUser(false);
				}
			}
		}

		public static string GetEnrollmentFromStudentID(int studentID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.GetEnrollmentFromStudentID(studentID);
			}
		}

		public static BasicStudentInformation GetBasicStudentInformation(int instituteID, string enrollment)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.Students.Where(s => s.Enrollment == enrollment && s.AdmissionOpenProgram.Program.InstituteID == instituteID).Select(s => new BasicStudentInformation
				{
					StudentID = s.StudentID,
					Enrollment = s.Enrollment,
					RegistrationNo = s.RegistrationNo,
					Name = s.Name,
					FatherName = s.FatherName,
					ProgramAlias = s.AdmissionOpenProgram.Program.ProgramAlias,
					Duration = s.AdmissionOpenProgram.Program.Duration,
					MaxAllowedSemesterID = s.AdmissionOpenProgram.MaxSemesterID,
					ProgramID = s.AdmissionOpenProgram.Program.ProgramID,
					SemesterID = s.AdmissionOpenProgram.SemesterID,
					AdmissionOpenProgramID = s.AdmissionOpenProgramID,
					Nationality = s.Nationality,
					StatusEnum = (Student.Statuses?)s.Status,
					BlockedReason = s.BlockedReason,
					StatusDate = s.StatusDate,
					CandidateAppliedProgramID = s.CandidateAppliedProgramID,
					ApplicationNo = s.CandidateAppliedProgram.ApplicationNo,
					AppliedSemesterID = s.CandidateAppliedProgram.AdmissionOpenProgram1.SemesterID,
					CandidateID = s.CandidateAppliedProgram.CandidateID,
					BURegistrationNo = s.CandidateAppliedProgram.Candidate.BURegistrationNo,
				}).SingleOrDefault();
			}
		}

		public static List<CustomListItem> GetInstituteBankAccounts(int instituteID, int? bankID, InstituteBankAccount.FeeTypes? feeTypeEnum, bool? active)
		{
			using (var aspireContext = new AspireContext())
			{
				var feeType = (byte?)feeTypeEnum;
				var instituteBankAccounts = aspireContext.InstituteBankAccounts.Where(iba => iba.InstituteID == instituteID);
				if (bankID != null)
					instituteBankAccounts = instituteBankAccounts.Where(iba => iba.BankID == bankID);
				if (feeType != null)
					instituteBankAccounts = instituteBankAccounts.Where(iba => iba.FeeType == feeType);
				if (active != null)
					instituteBankAccounts = instituteBankAccounts.Where(iba => iba.Active);
				return instituteBankAccounts.Select(iba => new
				{
					iba.InstituteBankAccountID,
					iba.AccountTitle,
					iba.AccountNo,
					iba.Bank.BankName,
					iba.Bank.BankAlias,
					iba.Active,
				}).ToList()
				.OrderByDescending(i => i.Active).ThenBy(i => i.BankAlias).ThenBy(i => i.AccountTitle).ThenBy(i => i.AccountNo)
				.Select(iba => new CustomListItem
				{
					ID = iba.InstituteBankAccountID,
					Text = $"{iba.BankAlias} - {iba.AccountTitle} ({iba.AccountNo}) ({(iba.Active ? "Active" : "Inactive")})",
				}).ToList();
			}
		}
	}
}
