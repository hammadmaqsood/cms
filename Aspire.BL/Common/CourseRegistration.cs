﻿using Aspire.BL.Entities.CourseRegistration;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.Common
{
	public static class CourseRegistration
	{
		#region Helper Queries

		internal static short? GetRegistrationOpenSemesterID(this AspireContext aspireContext, int programID, CourseRegistrationSetting.Types type)
		{
			var today = DateTime.Today;
			return aspireContext.CourseRegistrationSettings
				.Where(s => s.ProgramID == programID && s.Status == CourseRegistrationSetting.StatusActive)
				.Where(s => s.Type == (byte)type && s.FromDate <= today && today <= s.ToDate)
				.Select(s => (short?)s.SemesterID).SingleOrDefault();
		}

		internal static StudentClass GetStudentClassDetails(this AspireContext aspireContext, int studentID, short semesterID)
		{
			var studentSemester = aspireContext.StudentSemesters
				.Where(s => s.StudentID == studentID && s.SemesterID <= semesterID)
				.OrderByDescending(s => s.SemesterID)
				.Select(s => new
				{
					IntakeSemesterID = s.Student.AdmissionOpenProgram.SemesterID,
					s.Student.AdmissionOpenProgramID,
					s.Student.AdmissionOpenProgram.IsSummerRegularSemester,
					s.Student.AdmissionOpenProgram.Program.Duration,
					s.Student.AdmissionOpenProgram.ProgramID,
					s.Student.AdmissionOpenProgram.Program.ProgramAlias,
					s.SemesterID,
					s.SemesterNo,
					s.Section,
					s.Shift,
				}).FirstOrDefault();
			if (studentSemester == null)
				return null;

			var nextSemesterNo = (SemesterNos)studentSemester.SemesterNo;
			if (studentSemester.SemesterID != semesterID)
				nextSemesterNo = nextSemesterNo.NextSemesterNo(semesterID, Semester.GetSemesterType(studentSemester.IntakeSemesterID), (ProgramDurations)studentSemester.Duration, studentSemester.IsSummerRegularSemester);
			return new StudentClass
			{
				AdmissionOpenProgramID = studentSemester.AdmissionOpenProgramID,
				ProgramID = studentSemester.ProgramID,
				ProgramAlias = studentSemester.ProgramAlias,
				SemesterID = studentSemester.SemesterID,
				SemesterNoEnum = nextSemesterNo,
				Section = studentSemester.Section,
				Shift = studentSemester.Shift,
			};
		}

		internal static List<CustomOfferedCoursesForStudent> GetOfferedCoursesForStudent(this AspireContext aspireContext, int instituteID, int admissionOpenProgramID, int offeredSemesterID, StudentClass studentClass)
		{
			var eqCourses = from ce in aspireContext.CourseEquivalents
							join c in aspireContext.Courses.Where(c => c.AdmissionOpenProgramID == admissionOpenProgramID) on ce.EquivalentCourseID equals c.CourseID
							select new
							{
								ce.CourseID,
								ce.EquivalentCourseID,
								EquivalentSemesterID = c.AdmissionOpenProgram.SemesterID,
								EquivalentCourseCode = c.CourseCode,
								EquivalentTitle = c.Title,
								EquivalentCreditHours = c.CreditHours,
								EquivalentMajors = c.ProgramMajor.Majors,
							};

			var offeredCourses = (from oc in aspireContext.OfferedCourses.Where(oc => oc.SemesterID == offeredSemesterID && oc.Cours.AdmissionOpenProgram.Program.InstituteID == instituteID)
								  join eqc in eqCourses on oc.CourseID equals eqc.CourseID into grp
								  from g in grp.DefaultIfEmpty()
								  select new CustomOfferedCoursesForStudent
								  {
									  OfferedCourseID = oc.OfferedCourseID,
									  AdmissionOpenProgramID = oc.Cours.AdmissionOpenProgramID,
									  SemesterID = oc.SemesterID,
									  ProgramID = oc.Cours.AdmissionOpenProgram.ProgramID,
									  CourseID = oc.CourseID,
									  CourseCode = oc.Cours.CourseCode,
									  Title = oc.Cours.Title,
									  CreditHours = oc.Cours.CreditHours,
									  Majors = oc.Cours.ProgramMajor.Majors,
									  Program = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
									  SemesterNo = oc.SemesterNo,
									  Section = oc.Section,
									  Shift = oc.Shift,
									  SpecialOffered = oc.SpecialOffered,
									  Status = oc.Status,
									  EquivalentSemesterID = g == null ? (short?)null : g.EquivalentSemesterID,
									  EquivalentCourseID = g == null ? (int?)null : g.EquivalentCourseID,
									  EquivalentCourseCode = g == null ? null : g.EquivalentCourseCode,
									  EquivalentTitle = g == null ? null : g.EquivalentTitle,
									  EquivalentCreditHours = g == null ? (decimal?)null : g.EquivalentCreditHours,
									  EquivalentMajors = g == null ? null : g.EquivalentMajors,
								  }).ToList();
			var offeredClasses = aspireContext.OfferedCourseClasses.Where(occ => occ.OfferedCours.SemesterID == offeredSemesterID && occ.OfferedCours.Cours.AdmissionOpenProgram.Program.InstituteID == instituteID).Select(occ => new
			{
				occ.OfferedCourseID,
				occ.SemesterNo,
				occ.Section,
				occ.Shift,
			}).ToList();

			foreach (var offeredClass in offeredClasses)
				offeredCourses.Find(oc => oc.OfferedCourseID == offeredClass.OfferedCourseID).AddClass(offeredClass.SemesterNo, offeredClass.Section, offeredClass.Shift);

			var ocs = offeredCourses.FindAll(oc => oc.AdmissionOpenProgramID == admissionOpenProgramID && oc.EquivalentCourseID == null);
			foreach (var oc in ocs)
			{
				oc.EquivalentCourseID = oc.CourseID;
				oc.EquivalentCourseCode = oc.CourseCode;
				oc.EquivalentTitle = oc.Title;
				oc.EquivalentCreditHours = oc.CreditHours;
				oc.EquivalentMajors = oc.Majors;
			}

			if (studentClass != null)
				offeredCourses = offeredCourses.FindAll(oc => oc.ContainsClass(studentClass) && oc.EquivalentCourseID != null);
			return offeredCourses;
		}

		#endregion
	}
}