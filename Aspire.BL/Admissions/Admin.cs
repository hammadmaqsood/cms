﻿using Aspire.BL.Common;
using Aspire.BL.Entities.Admissions.Admin;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.Admissions
{
	public static class Admin
	{
		#region AdmissionCriteria

		public static AdmissionCriteria AddAdmissionCriteria(int userAdminRoleID, int instituteID, string criteriaName, int userLoginHistoryID, out bool nameAlreadyExists)
		{
			using (var aspireContext = new AspireContext(true))
			{
				if (!aspireContext.GetInstitutes(userAdminRoleID).Any(i => i.InstituteID == instituteID))
					throw new InvalidOperationException();
				criteriaName = criteriaName.TrimAndCannotBeEmpty();
				nameAlreadyExists = aspireContext.AdmissionCriterias.Any(ac => ac.InstituteID == instituteID && ac.CriteriaName == criteriaName);
				if (nameAlreadyExists)
					return null;
				var admissionCriteria = aspireContext.AdmissionCriterias.Add(new AdmissionCriteria
				{
					CriteriaName = criteriaName,
					InstituteID = instituteID,
				});
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return admissionCriteria;
			}
		}

		public static AdmissionCriteria UpdateAdmissionCriteria(int userAdminRoleID, int admissionCriteriaID, string criteriaName, int userLoginHistoryID, out bool nameAlreadyExists)
		{
			using (var aspireContext = new AspireContext(true))
			{
				criteriaName = criteriaName.TrimAndCannotBeEmpty();
				var admissionCriteria = aspireContext.GetInstitutes(userAdminRoleID).SelectMany(i => i.AdmissionCriterias).SingleOrDefault(ac => ac.AdmissionCriteriaID == admissionCriteriaID);
				if (admissionCriteria == null)
				{
					nameAlreadyExists = false;
					return null;
				}
				admissionCriteria.CriteriaName = criteriaName;
				nameAlreadyExists = aspireContext.AdmissionCriterias.Any(ac => ac.AdmissionCriteriaID != admissionCriteria.AdmissionCriteriaID && ac.InstituteID == admissionCriteria.InstituteID && ac.CriteriaName == admissionCriteria.CriteriaName);
				if (nameAlreadyExists)
					return null;
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return admissionCriteria;
			}
		}

		public static bool DeleteAdmissionCriteria(int userAdminRoleID, int admissionCriteriaID, int userLoginHistoryID, out bool cannotDeleteChildRecordExists)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var admissionCriteria = aspireContext.GetInstitutes(userAdminRoleID).SelectMany(i => i.AdmissionCriterias).SingleOrDefault(ac => ac.AdmissionCriteriaID == admissionCriteriaID);
				if (admissionCriteria == null)
				{
					cannotDeleteChildRecordExists = false;
					return false;
				}
				var query = aspireContext.AdmissionCriterias.First(c => c.AdmissionCriteriaID == admissionCriteria.AdmissionCriteriaID);
				cannotDeleteChildRecordExists = query.AdmissionCriteriaPreReqs.Any();
				if (cannotDeleteChildRecordExists)
					return false;
				cannotDeleteChildRecordExists = query.AdmissionOpenPrograms.Any();
				if (cannotDeleteChildRecordExists)
					return false;
				aspireContext.AdmissionCriterias.Remove(admissionCriteria);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return true;
			}
		}

		public static List<CustomAdmissionCriteria> GetAdmissionCriterias(int userAdminRoleID, int? instituteID, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount)
		{
			using (var aspireContext = new AspireContext(false))
			{
				var institutes = aspireContext.GetInstitutes(userAdminRoleID);
				if (instituteID != null)
					institutes = institutes.Where(i => i.InstituteID == instituteID.Value);
				var query = institutes.SelectMany(i => i.AdmissionCriterias);
				virtualItemCount = query.Count();
				switch (sortExpression)
				{
					case "InstituteAlias":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.Institute.InstituteAlias) : query.OrderByDescending(q => q.Institute.InstituteAlias);
						break;
					case "CriteriaName":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.CriteriaName) : query.OrderByDescending(q => q.CriteriaName);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return query.Skip(pageIndex * pageSize).Take(pageSize).Select(c => new CustomAdmissionCriteria
				{
					AdmissionCriteriaID = c.AdmissionCriteriaID,
					InstituteID = c.InstituteID,
					InstituteAlias = c.Institute.InstituteAlias,
					CriteriaName = c.CriteriaName
				}).ToList();
			}
		}

		public static AdmissionCriteria GetAdmissionCriteria(int admissionCriteriaID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.AdmissionCriterias.SingleOrDefault(ac => ac.AdmissionCriteriaID == admissionCriteriaID);
			}
		}

		public static List<CustomListItem> GetAdmissionCriteriasList(int instituteID)
		{
			using (var aspireContext = new AspireContext())
			{
				var admissionCriteria = aspireContext.AdmissionCriterias.Where(c => c.InstituteID == instituteID).Select(ac => new CustomListItem
				{
					ID = ac.AdmissionCriteriaID,
					Text = ac.CriteriaName
				}).OrderBy(ac => ac.Text).ToList();

				return admissionCriteria;
			}
		}


		#endregion

		#region AdmissionCriteriaPreReq

		public static AdmissionCriteriaPreReq AddAdmissionCriteriaPreReq(int admissionCriteriaID, string preReqQualification, string preReqQualificationResultStatus, byte? resultAwaitedDegreeType, int userLoginHistoryID)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var admissionCriteriaPreReq = new AdmissionCriteriaPreReq
				{
					AdmissionCriteriaID = admissionCriteriaID,
					PreReqQualification = preReqQualification.TrimAndCannotBeEmpty(),
					PreReqQualificationResultStatus = preReqQualificationResultStatus.TrimAndCannotBeEmpty(),
					ResultAwaitedDegreeType = resultAwaitedDegreeType

				};
				aspireContext.AdmissionCriteriaPreReqs.Add(admissionCriteriaPreReq);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return admissionCriteriaPreReq;
			}
		}

		public static AdmissionCriteriaPreReq UpdateAdmissionCriteriaPreReq(int? admissionCriteriaPreReqID, string preReqQualification, string preReqQualificationResultStatus, byte? resultAwaitedDegreeType, int userLoginHistoryID)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var admissionCriteriaPreReq = aspireContext.AdmissionCriteriaPreReqs.SingleOrDefault(acPreReq => acPreReq.AdmissionCriteriaPreReqID == admissionCriteriaPreReqID);
				if (admissionCriteriaPreReq == null)
					return null;

				admissionCriteriaPreReq.PreReqQualification = preReqQualification.TrimAndCannotBeEmpty();
				admissionCriteriaPreReq.PreReqQualificationResultStatus = preReqQualificationResultStatus.TrimAndCannotBeEmpty();
				admissionCriteriaPreReq.ResultAwaitedDegreeType = resultAwaitedDegreeType;
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return admissionCriteriaPreReq;
			}
		}

		public static bool DeleteAdmissionCriteriaPreReq(int admissionCriteriaPreReqID, int userLoginHistoryID, out bool isChildRecordExists)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var admissionCriteriaPreReq = aspireContext.AdmissionCriteriaPreReqs.SingleOrDefault(acPreReq => acPreReq.AdmissionCriteriaPreReqID == admissionCriteriaPreReqID);
				if (admissionCriteriaPreReq == null)
				{
					isChildRecordExists = false;
					return false;
				}
				isChildRecordExists = aspireContext.AdmissionCriteriaPreReqDegrees.Any(acPreReq => acPreReq.AdmissionCriteriaPreReqID == admissionCriteriaPreReqID);
				if (isChildRecordExists)
					return false;
				aspireContext.AdmissionCriteriaPreReqs.Remove(admissionCriteriaPreReq);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return true;
			}
		}

		public static List<AdmissionCriteriaPreReq> GetAdmissionCriteriaPreReqs(int admissionCriteriaID, string sortExpression, SortDirection sortDirection)
		{
			using (var aspireContext = new AspireContext())
			{
				var queryGetAdmissionCriteriaPreReq = aspireContext.AdmissionCriteriaPreReqs.Where(q => q.AdmissionCriteriaID == admissionCriteriaID);
				switch (sortExpression)
				{
					case "PreReqQualification":
						queryGetAdmissionCriteriaPreReq = sortDirection == SortDirection.Ascending ? queryGetAdmissionCriteriaPreReq.OrderBy(q => q.PreReqQualification) : queryGetAdmissionCriteriaPreReq.OrderByDescending(q => q.PreReqQualification);
						break;
					case "PreReqQualificationResultStatus":
						queryGetAdmissionCriteriaPreReq = sortDirection == SortDirection.Ascending ? queryGetAdmissionCriteriaPreReq.OrderBy(q => q.PreReqQualificationResultStatus) : queryGetAdmissionCriteriaPreReq.OrderByDescending(q => q.PreReqQualificationResultStatus);
						break;
					case "ResultAwaitedDegreeType":
						queryGetAdmissionCriteriaPreReq = sortDirection == SortDirection.Ascending ? queryGetAdmissionCriteriaPreReq.OrderBy(q => q.ResultAwaitedDegreeType) : queryGetAdmissionCriteriaPreReq.OrderByDescending(q => q.ResultAwaitedDegreeType);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return queryGetAdmissionCriteriaPreReq.ToList();
			}
		}

		public static AdmissionCriteriaPreReq GetAdmissionCriteriaPreReq(int admissionCriteriaPreReqID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.AdmissionCriteriaPreReqs.SingleOrDefault(acPreReq => acPreReq.AdmissionCriteriaPreReqID == admissionCriteriaPreReqID);
			}
		}

		#endregion

		#region AdmissionCriteriaPreRegDegree

		public static AdmissionCriteriaPreReqDegree AddAdmissionCriteriaPreReqDegree(AdmissionCriteriaPreReqDegree admissionCriteriaPreReqDegree, int userLoginHistoryID)
		{
			using (var aspireContext = new AspireContext(true))
			{
				admissionCriteriaPreReqDegree.Validate();
				admissionCriteriaPreReqDegree = aspireContext.AdmissionCriteriaPreReqDegrees.Add(admissionCriteriaPreReqDegree);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return admissionCriteriaPreReqDegree;
			}
		}

		public static AdmissionCriteriaPreReqDegree UpdateAdmissionCriteriaPreReqDegree(AdmissionCriteriaPreReqDegree admissionCriteriaPreReqDegree, int userLoginHistoryID)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var admissionCriteriaPreReqDegreeforEdit = aspireContext.AdmissionCriteriaPreReqDegrees.SingleOrDefault(acPreReqDeg => acPreReqDeg.AdmissionCriteriaPreReqDegreeID == admissionCriteriaPreReqDegree.AdmissionCriteriaPreReqDegreeID);
				if (admissionCriteriaPreReqDegreeforEdit == null)
					return null;
				admissionCriteriaPreReqDegree.Validate();
				admissionCriteriaPreReqDegreeforEdit.DegreeType = admissionCriteriaPreReqDegree.DegreeType;
				admissionCriteriaPreReqDegreeforEdit.AnnualExamSystem = admissionCriteriaPreReqDegree.AnnualExamSystem;
				admissionCriteriaPreReqDegreeforEdit.SemesterSystem = admissionCriteriaPreReqDegree.SemesterSystem;
				admissionCriteriaPreReqDegreeforEdit.AnnualExamSystemMinimumPercentage = admissionCriteriaPreReqDegree.AnnualExamSystemMinimumPercentage;
				admissionCriteriaPreReqDegreeforEdit.SemesterSystemMinCGPAOutOf4 = admissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf4;
				admissionCriteriaPreReqDegreeforEdit.SemesterSystemMinCGPAOutOf5 = admissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf5;
				admissionCriteriaPreReqDegreeforEdit.DisplaySequence = admissionCriteriaPreReqDegree.DisplaySequence;
				admissionCriteriaPreReqDegreeforEdit.MeritListWeightage = admissionCriteriaPreReqDegree.MeritListWeightage;
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return admissionCriteriaPreReqDegreeforEdit;
				//if (!aspireContext.AdmissionCriteriaPreReqDegrees.Any(d => d.AdmissionCriteriaPreReqDegreeID == admissionCriteriaPreReqDegree.AdmissionCriteriaPreReqDegreeID))
				//	return null;
				//admissionCriteriaPreReqDegree.Validate();
				//aspireContext.Entry(admissionCriteriaPreReqDegree).State = EntityState.Modified;
				//aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				//return admissionCriteriaPreReqDegree;
			}
		}

		public static bool DeleteAdmissionCriteriaPreReqDegree(int admissionCriteriaPreReqDegreeID, int userLoginHistoryID, out bool isChildRecordExists)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var admissionCriteriaPreReqDegree = aspireContext.AdmissionCriteriaPreReqDegrees.SingleOrDefault(acPreReq => acPreReq.AdmissionCriteriaPreReqDegreeID == admissionCriteriaPreReqDegreeID);
				if (admissionCriteriaPreReqDegree == null)
				{
					isChildRecordExists = false;
					return false;
				}
				isChildRecordExists = aspireContext.AdmissionCriteriaPreReqDegreeSubjects.Any(s => s.AdmissionCriteriaPreReqDegreeID == admissionCriteriaPreReqDegreeID);
				if (isChildRecordExists)
					return false;
				aspireContext.AdmissionCriteriaPreReqDegrees.Remove(admissionCriteriaPreReqDegree);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return true;

			}

		}

		public static List<AdmissionCriteriaPreReqDegree> GetAdmissionCriteriaPreReqDegrees(int admissionCriteriaPreReqID, string sortExpression, SortDirection sortDirection)
		{
			using (var aspireContext = new AspireContext())
			{
				var query = aspireContext.AdmissionCriteriaPreReqDegrees.AsQueryable();
				query = query.Where(q => q.AdmissionCriteriaPreReqID == admissionCriteriaPreReqID);
				switch (sortExpression)
				{
					case "DegreeType":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.DegreeType) : query.OrderByDescending(q => q.DegreeType);
						break;
					case "AnnualExamSystem":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.AnnualExamSystem) : query.OrderByDescending(q => q.AnnualExamSystem);
						break;
					case "SemesterSystem":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.SemesterSystem) : query.OrderByDescending(q => q.SemesterSystem);
						break;
					case "AnnualExamSystemMinimumPercentage":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.AnnualExamSystemMinimumPercentage) : query.OrderByDescending(q => q.AnnualExamSystemMinimumPercentage);
						break;
					case "SemesterSystemMinCGPAOutOf4":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.SemesterSystemMinCGPAOutOf4) : query.OrderByDescending(q => q.SemesterSystemMinCGPAOutOf4);
						break;
					case "SemesterSystemMinCGPAOutOf5":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.SemesterSystemMinCGPAOutOf5) : query.OrderByDescending(q => q.SemesterSystemMinCGPAOutOf5);
						break;
					case "DisplaySequence":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.DisplaySequence) : query.OrderByDescending(q => q.DisplaySequence);
						break;
					case "MeritListWeightage":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.MeritListWeightage) : query.OrderByDescending(q => q.MeritListWeightage);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				return query.ToList();
			}
		}

		public static AdmissionCriteriaPreReqDegree GetAdmissionCriteriaPreReqDegree(int? admissionCriteriaPreReqDegID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.AdmissionCriteriaPreReqDegrees.SingleOrDefault(d => d.AdmissionCriteriaPreReqDegreeID == admissionCriteriaPreReqDegID);
			}
		}

		#endregion

		#region AdmissionCriteriaPreReqDegreeSubjects

		public static AdmissionCriteriaPreReqDegreeSubject AddAdmissionCriteriaPreReqDegreeSubject(int admissionCriteriaPreReqDegreeID, string subjects, int userLoginHistoryID)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var admissionCriteriaPreReqDegreeSubject = new AdmissionCriteriaPreReqDegreeSubject
				{
					AdmissionCriteriaPreReqDegreeID = admissionCriteriaPreReqDegreeID,
					Subjects = subjects.TrimAndCannotBeEmpty()
				};
				aspireContext.AdmissionCriteriaPreReqDegreeSubjects.Add(admissionCriteriaPreReqDegreeSubject);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return admissionCriteriaPreReqDegreeSubject;
			}
		}

		public static AdmissionCriteriaPreReqDegreeSubject UpdateAdmissionCriteriaPreReqDegreeSubject(int? admissionCriteriaPreReqDegSubjectID, string subjects, int userLoginHistoryID)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var admissionCriteriaPreReqDegSubject = aspireContext.AdmissionCriteriaPreReqDegreeSubjects.SingleOrDefault(acPreReqDegSub => acPreReqDegSub.AdmissionCriteriaPreReqDegreeSubjectID == admissionCriteriaPreReqDegSubjectID);
				if (admissionCriteriaPreReqDegSubject == null)
					return null;

				admissionCriteriaPreReqDegSubject.Subjects = subjects.TrimAndCannotBeEmpty();
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return admissionCriteriaPreReqDegSubject;
			}
		}

		public static bool DeleteAdmissionCriteriaPreReqDegreeSubject(int admissionCriteriaPreReqDegSubjectID, int userLoginHistoryID)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var admissionCriteriaPreReqDegSub = aspireContext.AdmissionCriteriaPreReqDegreeSubjects.SingleOrDefault(acPreReqDegSub => acPreReqDegSub.AdmissionCriteriaPreReqDegreeSubjectID == admissionCriteriaPreReqDegSubjectID);
				if (admissionCriteriaPreReqDegSub == null)
					return false;

				aspireContext.AdmissionCriteriaPreReqDegreeSubjects.Remove(admissionCriteriaPreReqDegSub);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return true;
			}
		}

		public static List<AdmissionCriteriaPreReqDegreeSubject> GetAdmissionCriteriaPreReqDegreeSubjects(int admissionCriteriaPreReqDegreeID, string sortExpression, SortDirection sortDirection)
		{
			using (var aspireContext = new AspireContext())
			{
				var query = aspireContext.AdmissionCriteriaPreReqDegreeSubjects.AsQueryable();
				query = query.Where(q => q.AdmissionCriteriaPreReqDegreeID == admissionCriteriaPreReqDegreeID);
				switch (sortExpression)
				{
					case "Subjects":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.Subjects) : query.OrderByDescending(q => q.Subjects);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);

				}
				return query.ToList();
			}
		}

		public static AdmissionCriteriaPreReqDegreeSubject GetAdmissionCriteriaPreReqDegreeSubject(int? admissionCriteriaPreReqDegreeSubjectID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.AdmissionCriteriaPreReqDegreeSubjects.SingleOrDefault(acPreReqDegSub => acPreReqDegSub.AdmissionCriteriaPreReqDegreeSubjectID == admissionCriteriaPreReqDegreeSubjectID);
			}
		}

		#endregion

		#region Admission Open Program

		public static AdmissionOpenProgram.ValidationResults AddAdmissionOpenProgram(AdmissionOpenProgram admissionOpenProgram, int userLoginHistoryID)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var alreadyExists = aspireContext.AdmissionOpenPrograms.Any(aop => aop.SemesterID == admissionOpenProgram.SemesterID && aop.ProgramID == admissionOpenProgram.ProgramID);
				if (alreadyExists)
					return AdmissionOpenProgram.ValidationResults.AlreadyExists;

				var validationResult = admissionOpenProgram.Validate();
				if (validationResult != AdmissionOpenProgram.ValidationResults.Success)
					return validationResult;
				admissionOpenProgram.EligibilityCriteriaStatement.TrimAndCannotBeEmpty();
				aspireContext.AdmissionOpenPrograms.Add(admissionOpenProgram);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return validationResult;
			}
		}

		public static AdmissionOpenProgram.ValidationResults UpdateAdmissionOpenProgram(AdmissionOpenProgram admissionOpenProgram, int userLoginHistoryID)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var admissionOpenProgramforEdit = aspireContext.AdmissionOpenPrograms.SingleOrDefault(aop => aop.AdmissionOpenProgramID == admissionOpenProgram.AdmissionOpenProgramID);
				if (admissionOpenProgramforEdit == null)
					return AdmissionOpenProgram.ValidationResults.NoRecordFound;

				var validationResult = admissionOpenProgram.Validate();
				if (validationResult != AdmissionOpenProgram.ValidationResults.Success)
					return validationResult;

				admissionOpenProgramforEdit.Shifts = admissionOpenProgram.Shifts;
				admissionOpenProgramforEdit.EligibilityCriteriaStatement = admissionOpenProgram.EligibilityCriteriaStatement;
				admissionOpenProgramforEdit.AdmissionCriteriaID = admissionOpenProgram.AdmissionCriteriaID;
				admissionOpenProgramforEdit.AdmissionOpenDate = admissionOpenProgram.AdmissionOpenDate;
				admissionOpenProgramforEdit.AdmissionClosingDate = admissionOpenProgram.AdmissionClosingDate;
				admissionOpenProgramforEdit.EntryTestType = admissionOpenProgram.EntryTestType;
				admissionOpenProgramforEdit.EntryTestStartDate = admissionOpenProgram.EntryTestStartDate;
				admissionOpenProgramforEdit.EntryTestEndDate = admissionOpenProgram.EntryTestEndDate;
				admissionOpenProgramforEdit.EntryTestDuration = admissionOpenProgram.EntryTestDuration;
				admissionOpenProgramforEdit.InterviewsStartDate = admissionOpenProgram.InterviewsStartDate;
				admissionOpenProgramforEdit.InterviewsEndDate = admissionOpenProgram.InterviewsEndDate;
				admissionOpenProgramforEdit.LoginAllowedEndDate = admissionOpenProgram.LoginAllowedEndDate;
				admissionOpenProgramforEdit.AdmissionProcessingFee = admissionOpenProgram.AdmissionProcessingFee;
				admissionOpenProgramforEdit.ExamSystemType = admissionOpenProgram.ExamSystemType;
				admissionOpenProgramforEdit.IsSummerRegularSemester = admissionOpenProgram.IsSummerRegularSemester;
				admissionOpenProgramforEdit.MaxSemesterID = admissionOpenProgram.MaxSemesterID;
				admissionOpenProgramforEdit.ExamMarksGradingSchemeID = admissionOpenProgram.ExamMarksGradingSchemeID;
				admissionOpenProgramforEdit.ExamMarkingSchemeID = admissionOpenProgram.ExamMarkingSchemeID;

				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return validationResult;
			}
		}

		public static bool DeleteAdmissionOpenProgram(int userAdminRoleID, int admissionOpenProgramID, int userLoginHistoryID, out bool isChildRecordExists)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var admissionOpenProgram = aspireContext.GetInstitutes(userAdminRoleID).SelectMany(i => i.Programs).SelectMany(p => p.AdmissionOpenPrograms).SingleOrDefault(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID);
				if (admissionOpenProgram == null)
				{
					isChildRecordExists = false;
					return false;
				}
				var query = aspireContext.AdmissionOpenPrograms.First(aop => aop.AdmissionOpenProgramID == admissionOpenProgram.AdmissionOpenProgramID);
				isChildRecordExists = query.AdmissionOpenProgramEquivalents.Any();
				if (isChildRecordExists)
					return false;
				isChildRecordExists = query.CandidateAppliedPrograms.Any();
				if (isChildRecordExists)
					return false;
				isChildRecordExists = query.CandidateAppliedPrograms1.Any();
				if (isChildRecordExists)
					return false;
				isChildRecordExists = query.CandidateAppliedPrograms2.Any();
				if (isChildRecordExists)
					return false;
				isChildRecordExists = query.CandidateAppliedPrograms3.Any();
				if (isChildRecordExists)
					return false;
				isChildRecordExists = query.CandidateAppliedPrograms4.Any();
				if (isChildRecordExists)
					return false;
				isChildRecordExists = query.Students.Any();
				if (isChildRecordExists)
					return false;
				isChildRecordExists = query.Courses.Any();
				if (isChildRecordExists)
					return false;
				aspireContext.AdmissionOpenPrograms.Remove(admissionOpenProgram);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return true;
			}
		}

		public static AdmissionOpenProgram GetAdmissionOpenProgram(int? admissionOpenProgramID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.AdmissionOpenPrograms.Include(aop => aop.Program).SingleOrDefault(aop => aop.AdmissionOpenProgramID == admissionOpenProgramID);
			}
		}

		public static List<CustomAdmissionOpenProgram> GetAdmissionOpenPrograms(int userAdminRoleID, short? semesterID, int? instituteID, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, out int virtualItemCount)
		{
			using (var aspireContext = new AspireContext())
			{
				var admissionOpenPrograms = aspireContext.GetInstitutes(userAdminRoleID).SelectMany(i => i.Programs).SelectMany(p => p.AdmissionOpenPrograms);
				if (semesterID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.SemesterID == semesterID);
				if (instituteID != null)
					admissionOpenPrograms = admissionOpenPrograms.Where(aop => aop.Program.InstituteID == instituteID);

				virtualItemCount = admissionOpenPrograms.Count();
				switch (sortExpression)
				{
					case "InstituteAlias":
						admissionOpenPrograms = sortDirection == SortDirection.Ascending ? admissionOpenPrograms.OrderBy(aop => aop.Program.Institute.InstituteAlias) : admissionOpenPrograms.OrderByDescending(aop => aop.Program.Institute.InstituteAlias);
						break;
					case "ProgramAlias":
						admissionOpenPrograms = sortDirection == SortDirection.Ascending ? admissionOpenPrograms.OrderBy(aop => aop.Program.ProgramAlias) : admissionOpenPrograms.OrderByDescending(aop => aop.Program.ProgramAlias);
						break;
					case "Duration":
						admissionOpenPrograms = sortDirection == SortDirection.Ascending ? admissionOpenPrograms.OrderBy(aop => aop.Program.Duration) : admissionOpenPrograms.OrderByDescending(aop => aop.Program.Duration);
						break;
					case "OpenDates":
						admissionOpenPrograms = sortDirection == SortDirection.Ascending ? admissionOpenPrograms.OrderBy(aop => aop.AdmissionOpenDate) : admissionOpenPrograms.OrderByDescending(aop => aop.AdmissionOpenDate);
						break;
					case nameof(AdmissionOpenProgram.EntryTestType):
					case nameof(AdmissionOpenProgram.EntryTestTypeEnum):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.EntryTestType);
						break;
					case nameof(AdmissionOpenProgram.EntryTestStartDate):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.EntryTestStartDate);
						break;
					case nameof(AdmissionOpenProgram.EntryTestEndDate):
						admissionOpenPrograms = admissionOpenPrograms.OrderBy(sortDirection, aop => aop.EntryTestEndDate);
						break;
					case "Shifts":
						admissionOpenPrograms = sortDirection == SortDirection.Ascending ? admissionOpenPrograms.OrderBy(aop => aop.Shifts) : admissionOpenPrograms.OrderByDescending(aop => aop.Shifts);
						break;
					case "EntryTestDuration":
						admissionOpenPrograms = sortDirection == SortDirection.Ascending ? admissionOpenPrograms.OrderBy(aop => aop.EntryTestDuration) : admissionOpenPrograms.OrderByDescending(aop => aop.EntryTestDuration);
						break;
					case "InterviewDates":
						admissionOpenPrograms = sortDirection == SortDirection.Ascending ? admissionOpenPrograms.OrderBy(aop => aop.InterviewsStartDate) : admissionOpenPrograms.OrderByDescending(aop => aop.InterviewsStartDate);
						break;
					case "LoginAllowedEndDate":
						admissionOpenPrograms = sortDirection == SortDirection.Ascending ? admissionOpenPrograms.OrderBy(aop => aop.LoginAllowedEndDate) : admissionOpenPrograms.OrderByDescending(aop => aop.LoginAllowedEndDate);
						break;
					case "AdmissionProcessingFee":
						admissionOpenPrograms = sortDirection == SortDirection.Ascending ? admissionOpenPrograms.OrderBy(aop => aop.AdmissionProcessingFee) : admissionOpenPrograms.OrderByDescending(aop => aop.AdmissionProcessingFee);
						break;
					case "MaxSemester":
						admissionOpenPrograms = sortDirection == SortDirection.Ascending ? admissionOpenPrograms.OrderBy(aop => aop.MaxSemesterID) : admissionOpenPrograms.OrderByDescending(aop => aop.MaxSemesterID);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);

				}
				admissionOpenPrograms = admissionOpenPrograms.Skip(pageIndex * pageSize).Take(pageSize);
				var aops = admissionOpenPrograms.Select(aop => new CustomAdmissionOpenProgram
				{
					AdmissionOpenProgramID = aop.AdmissionOpenProgramID,
					InstituteAlias = aop.Program.Institute.InstituteAlias,
					ProgramAlias = aop.Program.ProgramAlias,
					Duration = aop.Program.Duration,
					AdmissionOpenDate = aop.AdmissionOpenDate,
					AdmissionCloseDate = aop.AdmissionClosingDate,
					EntryTestType = aop.EntryTestType,
					EntryTestStartDate = aop.EntryTestStartDate,
					EntryTestEndDate = aop.EntryTestEndDate,
					Shifts = aop.Shifts,
					EntryTestDuration = aop.EntryTestDuration,
					InterviewStartDate = aop.InterviewsStartDate,
					InterviewEndDate = aop.InterviewsEndDate,
					LoginAllowedEndDate = aop.LoginAllowedEndDate,
					AdmissionProcessingFee = aop.AdmissionProcessingFee,
					MaxSemesterID = aop.MaxSemesterID,
				}).ToList();

				foreach (var _aop in aops)
				{
					_aop.EquivalentPrograms = string.Empty;
					var query = from p in aspireContext.Programs
								join aop in aspireContext.AdmissionOpenPrograms on p.ProgramID equals aop.ProgramID
								join aope in aspireContext.AdmissionOpenProgramEquivalents on aop.AdmissionOpenProgramID equals aope.EquivalentAdmissionOpenProgramID
								where aope.AdmissionOpenProgramID == _aop.AdmissionOpenProgramID
								select new
								{
									p.ProgramAlias
								};
					if (query.Select(q => q.ProgramAlias).ToList().Count == 0)
						_aop.EquivalentPrograms = "Add";
					else
						_aop.EquivalentPrograms = string.Join(",", query.Select(q => q.ProgramAlias).ToList());
				}
				return aops;
			}
		}

		#endregion

		#region Admission Open Program Equivalents

		public static AdmissionOpenProgramEquivalent AddEquivalentAdmissionOpenProgram(int admissionOpenProgramID, int equivalentAdmissionOpenProgramID, int userLoginHistoryID, out bool alreadyExists, out bool isAddWithWarning)
		{
			using (var aspireContext = new AspireContext())
			{
				alreadyExists = aspireContext.AdmissionOpenProgramEquivalents.Any(aope => aope.AdmissionOpenProgramID == admissionOpenProgramID && aope.EquivalentAdmissionOpenProgramID == equivalentAdmissionOpenProgramID);
				if (alreadyExists)
				{
					isAddWithWarning = false;
					return null;
				}
				var admissionOpenProgram = GetAdmissionOpenProgram(admissionOpenProgramID);
				var equivalentAdmissionOpenProgram = GetAdmissionOpenProgram(equivalentAdmissionOpenProgramID);
				if (admissionOpenProgram.AdmissionOpenDate != equivalentAdmissionOpenProgram.AdmissionOpenDate || admissionOpenProgram.AdmissionClosingDate != equivalentAdmissionOpenProgram.AdmissionClosingDate || admissionOpenProgram.AdmissionProcessingFee != equivalentAdmissionOpenProgram.AdmissionProcessingFee)
					isAddWithWarning = true;
				else
					isAddWithWarning = false;
				var admissionOpenProgramEquivalent = new AdmissionOpenProgramEquivalent
				{
					AdmissionOpenProgramID = admissionOpenProgramID,
					EquivalentAdmissionOpenProgramID = equivalentAdmissionOpenProgramID,
				};
				aspireContext.AdmissionOpenProgramEquivalents.Add(admissionOpenProgramEquivalent);
				aspireContext.SaveChanges(userLoginHistoryID);
				return admissionOpenProgramEquivalent;
			}
		}

		public static List<CustomListItem> GetEquivalentAdmissionOpenPrograms(int admissionOpenProgramID)
		{
			using (var aspireContext = new AspireContext())
			{
				var query = from p in aspireContext.Programs
							join aop in aspireContext.AdmissionOpenPrograms on p.ProgramID equals aop.ProgramID
							join aope in aspireContext.AdmissionOpenProgramEquivalents on aop.AdmissionOpenProgramID equals aope.EquivalentAdmissionOpenProgramID
							where aope.AdmissionOpenProgramID == admissionOpenProgramID
							select new CustomListItem
							{
								ID = aope.AdmissionOpenProgramEquivalentID,
								Text = p.ProgramAlias,
							};
				return query.ToList();
			}
		}

		public static List<CustomListItem> GetAdmissionOpenProgramsForEquivalency(int instituteID, short semesterID, int admissionOpenProgramID)
		{
			using (var aspireContext = new AspireContext())
			{
				var query = aspireContext.AdmissionOpenPrograms.Where(aop => aop.Program.InstituteID == instituteID && aop.SemesterID == semesterID && aop.AdmissionOpenProgramID != admissionOpenProgramID).Select(aop => new CustomListItem
				{
					ID = aop.AdmissionOpenProgramID,
					Text = aop.Program.ProgramAlias,
				});
				return query.ToList();
			}
		}

		public static bool DeleteAdmissionOpenProgramEquivalent(int admissionOpenProgramEquivalentID, int userLoginHistoryID)
		{
			using (var aspireContext = new AspireContext())
			{
				var admissionOpenProgramEquivalent = aspireContext.AdmissionOpenProgramEquivalents.SingleOrDefault(aope => aope.AdmissionOpenProgramEquivalentID == admissionOpenProgramEquivalentID);
				if (admissionOpenProgramEquivalent == null)
					return false;
				aspireContext.AdmissionOpenProgramEquivalents.Remove(admissionOpenProgramEquivalent);
				aspireContext.SaveChanges(userLoginHistoryID);
				return true;
			}
		}

		#endregion
	}
}

