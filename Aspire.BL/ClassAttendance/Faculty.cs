﻿using Aspire.BL.Entities.ClassAttendance;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.ClassAttendance
{
	public static class Faculty
	{
		private static ClassAttendanceSetting GetClassAttendanceSettings(this AspireContext aspireContext, int offeredCourseID)
		{
			var offeredCourse = aspireContext.OfferedCourses.Where(oc => oc.OfferedCourseID == offeredCourseID).Select(oc => new { oc.Cours.AdmissionOpenProgram.ProgramID, oc.SemesterID }).Single();
			return aspireContext.ClassAttendanceSettings.SingleOrDefault(cas => cas.SemesterID == offeredCourse.SemesterID && cas.ProgramID == offeredCourse.ProgramID && cas.Status == ClassAttendanceSetting.StatusActive);
		}

		private static bool IsCurrentlyActive(this ClassAttendanceSetting classAttendanceSetting)
		{
			if (classAttendanceSetting == null)
				return false;
			var today = DateTime.Today;
			return classAttendanceSetting.FromDate.Date <= today && today <= classAttendanceSetting.ToDate.Date;
		}

		private static bool CanMarkAttendance(this AspireContext aspireContext, int offeredCourseID, DateTime classDate, bool bypass)
		{
			classDate = classDate.Date;
			var today = DateTime.Today;
			if (classDate > today)
				return false;
			if (bypass)
				return true;
			var classAttendanceSetting = aspireContext.GetClassAttendanceSettings(offeredCourseID);
			if (!classAttendanceSetting.IsCurrentlyActive())
				return false;
			return today.AddDays(-classAttendanceSetting.CanMarkPastDays) <= classDate;
		}

		private static bool CanEditOrDeleteAttendance(this AspireContext aspireContext, int offeredCourseID, DateTime classDate, bool bypass)
		{
			classDate = classDate.Date;
			var today = DateTime.Today;
			if (classDate > today)
				return false;
			if (bypass)
				return true;
			var classAttendanceSetting = aspireContext.GetClassAttendanceSettings(offeredCourseID);
			if (!classAttendanceSetting.IsCurrentlyActive())
				return false;
			return today.AddDays(-classAttendanceSetting.CanEditPastDays) <= classDate;
		}

		public static int? AddClassAttendance(MarkClassAttendance attendance, bool bypassChecks, int userLoginHistoryID, out bool? canMark)
		{
			using (var aspireContext = new AspireContext(true))
			{
				if (!attendance.Students.Any())
					throw new ArgumentNullException(nameof(attendance));
				if (!aspireContext.OfferedCourses.Any(oc => oc.OfferedCourseID == attendance.OfferedCourseID))
				{
					canMark = null;
					return null;
				}
				canMark = aspireContext.CanMarkAttendance(attendance.OfferedCourseID, attendance.ClassDate, bypassChecks);
				if (!canMark.Value)
					return null;

				var offeredCourseAttendance = new OfferedCourseAttendance
				{
					OfferedCourseID = attendance.OfferedCourseID,
					ClassDate = attendance.ClassDate,
					ClassType = attendance.ClassType,
					HoursValue = attendance.HoursValue,
					RoomID = attendance.RoomID,
					Remarks = attendance.Remarks,
					TopicsCovered = attendance.TopicsCovered,
					MarkedByUserID = aspireContext.UserLoginHistories.Where(h => h.UserLoginHistoryID == userLoginHistoryID).Select(h => h.UserID).Single(),
					MarkedDate = DateTime.Now,
				};
				aspireContext.OfferedCourseAttendances.Add(offeredCourseAttendance);
				foreach (var student in attendance.Students)
				{
					if (!offeredCourseAttendance.HoursValue.GetSmallerOrEqualHourValue().Contains(student.HoursValue))
						throw new InvalidOperationException();
					offeredCourseAttendance.OfferedCourseAttendanceDetails.Add(new OfferedCourseAttendanceDetail
					{
						RegisteredCourseID = student.RegisteredCourseID,
						Hours = student.Hours,
						FeeDefaulter = student.FeeDefaulter
					});
				}
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return offeredCourseAttendance.OfferedCourseAttendanceID;
			}
		}

		public static bool EditClassAttendance(MarkClassAttendance attendance, bool bypassChecks, int userLoginHistoryID, out bool? canEdit)
		{
			if (attendance.OfferedCourseAttendanceID == null)
				throw new ArgumentNullException(nameof(attendance.OfferedCourseAttendanceID));
			using (var aspireContext = new AspireContext(true))
			{
				if (!aspireContext.OfferedCourses.Any(oc => oc.OfferedCourseID == attendance.OfferedCourseID))
				{
					canEdit = null;
					return false;
				}
				canEdit = aspireContext.CanEditOrDeleteAttendance(attendance.OfferedCourseID, attendance.ClassDate, bypassChecks);
				if (!canEdit.Value)
					return false;
				var offeredCourseAttendance = aspireContext.OfferedCourseAttendances
					.Include(oca => oca.OfferedCourseAttendanceDetails)
					.SingleOrDefault(oca => oca.OfferedCourseAttendanceID == attendance.OfferedCourseAttendanceID);
				if (offeredCourseAttendance == null)
				{
					canEdit = null;
					return false;
				}
				if (bypassChecks)
					offeredCourseAttendance.ClassDate = attendance.ClassDate;
				offeredCourseAttendance.ClassType = attendance.ClassType;
				offeredCourseAttendance.Remarks = attendance.Remarks;
				offeredCourseAttendance.TopicsCovered = attendance.TopicsCovered;
				offeredCourseAttendance.RoomID = attendance.RoomID;

				foreach (var student in attendance.Students)
				{
					if (!offeredCourseAttendance.HoursValue.GetSmallerOrEqualHourValue().Contains(student.HoursValue))
						throw new InvalidOperationException();
					var offeredCourseAttendanceDetail = offeredCourseAttendance.OfferedCourseAttendanceDetails
						.SingleOrDefault(ocad => ocad.RegisteredCourseID == student.RegisteredCourseID);
					if (offeredCourseAttendanceDetail == null)
						offeredCourseAttendance.OfferedCourseAttendanceDetails.Add(new OfferedCourseAttendanceDetail
						{
							RegisteredCourseID = student.RegisteredCourseID,
							Hours = student.Hours,
							FeeDefaulter = student.FeeDefaulter
						});
					else
					{
						offeredCourseAttendanceDetail.Hours = student.Hours;
						offeredCourseAttendanceDetail.FeeDefaulter = student.FeeDefaulter;
					}
				}

				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return true;
			}
		}

		public static bool DeleteClassAttendance(int offeredCourseAttendanceID, bool bypassChecks, int userLoginHistoryID, out bool? canDelete)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var offeredCourseAttendance = aspireContext.OfferedCourseAttendances.Include(oca => oca.OfferedCourseAttendanceDetails).SingleOrDefault(oca => oca.OfferedCourseAttendanceID == offeredCourseAttendanceID);
				if (offeredCourseAttendance == null)
				{
					canDelete = null;
					return false;
				}
				if (!aspireContext.OfferedCourses.Any(oc => oc.OfferedCourseID == offeredCourseAttendance.OfferedCourseID))
				{
					canDelete = null;
					return false;
				}
				canDelete = aspireContext.CanEditOrDeleteAttendance(offeredCourseAttendance.OfferedCourseID, offeredCourseAttendance.ClassDate, bypassChecks);
				if (canDelete == false)
					return false;

				aspireContext.OfferedCourseAttendanceDetails.RemoveRange(offeredCourseAttendance.OfferedCourseAttendanceDetails);
				aspireContext.OfferedCourseAttendances.Remove(offeredCourseAttendance);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return true;
			}
		}

		public static CustomOfferedCourseAttendance GetOfferedCourseAttendances(int offeredCourseID, int? offeredCourseAttendanceID, bool byPassChecks, out bool canMark, out bool canEdit, out bool canDelete)
		{
			using (var aspireContext = new AspireContext())
			{
				canMark = false;
				canEdit = false;
				canDelete = false;
				var customOfferedCourseAttendance = aspireContext.OfferedCourses.Where(co => co.OfferedCourseID == offeredCourseID).Select(co => new CustomOfferedCourseAttendance
				{
					OfferedCourseID = co.OfferedCourseID,
					OfferedSemesterID = co.SemesterID,
					CourseCode = co.Cours.CourseCode,
					Title = co.Cours.Title,
					CreditHours = co.Cours.CreditHours,
					ContactHours = co.Cours.CreditHours,
					ProgramID = co.Cours.AdmissionOpenProgram.ProgramID,
					Program = co.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = co.SemesterNo,
					Section = co.Section,
					Shift = co.Shift,
					Teacher = co.FacultyMember.Name,
				}).SingleOrDefault();
				if (customOfferedCourseAttendance == null)
					return null;
				canMark = aspireContext.CanMarkAttendance(offeredCourseID, DateTime.Today, byPassChecks);
				if (offeredCourseAttendanceID != null)
				{
					var attendance = aspireContext.OfferedCourseAttendances.SingleOrDefault(oca => oca.OfferedCourseAttendanceID == offeredCourseAttendanceID);
					if (attendance == null)
						return null;
					customOfferedCourseAttendance.OfferedCourseAttendanceID = attendance.OfferedCourseAttendanceID;
					customOfferedCourseAttendance.Hours = attendance.Hours;
					customOfferedCourseAttendance.ClassDate = attendance.ClassDate;
					customOfferedCourseAttendance.ClassType = attendance.ClassType;
					customOfferedCourseAttendance.Remarks = attendance.Remarks;
					customOfferedCourseAttendance.TopicsCovered = attendance.TopicsCovered;
					customOfferedCourseAttendance.RoomID = attendance.Room.RoomID;
					canDelete = canEdit = aspireContext.CanEditOrDeleteAttendance(customOfferedCourseAttendance.OfferedCourseID, customOfferedCourseAttendance.ClassDate.Value, byPassChecks);
				}
				return customOfferedCourseAttendance;
			}
		}

		private static IQueryable<RegisteredCours> GetValidatedRegisteredCourses(this AspireContext aspireContext, int offeredCourseID)
		{
			var studentValidStatuses = new Student.Statuses[] { }.Cast<byte>().ToList();
			var registeredCourseValidStatuses = new[] { RegisteredCours.Statuses.AttendanceDefaulter, }.Cast<byte>().ToList();
			var registeredCourses = aspireContext.RegisteredCourses.Where(rc => rc.OfferedCourseID == offeredCourseID && rc.DeletedDate == null)
				.Where(rc => rc.FreezedStatus == null);
			if (registeredCourseValidStatuses.Any())
				registeredCourses = registeredCourses.Where(rc => rc.Status == null || registeredCourseValidStatuses.Contains(rc.Status.Value));
			else
				registeredCourses = registeredCourses.Where(rc => rc.Status == null);
			if (studentValidStatuses.Any())
				registeredCourses = registeredCourses.Where(rc => rc.Student.Status == null || studentValidStatuses.Contains(rc.Student.Status.Value));
			else
				registeredCourses = registeredCourses.Where(rc => rc.Student.Status == null);
			return registeredCourses.OrderBy(rc => rc.Student.Enrollment);
		}

		public static List<CustomOfferedCourseAttendanceDetails> GetOfferedCourseAttendanceDetails(int offeredCourseID, int? offeredCourseAttendanceID)
		{
			using (var aspireContext = new AspireContext())
			{
				var enrollments = aspireContext.GetValidatedRegisteredCourses(offeredCourseID);
				var studentsEnrollmentList = enrollments.Select(rc => new CustomOfferedCourseAttendanceDetails
				{
					RegisteredCourseID = rc.RegisteredCourseID,
					StudentID = rc.StudentID,
					Enrollment = rc.Student.Enrollment,
					Name = rc.Student.Name,
					Program = rc.Student.AdmissionOpenProgram.Program.ProgramAlias,
					Hours = 0,
					FeeStatus = rc.StudentFee.Status,
					FeeDefaulter = rc.FeeDefaulter,
				}).ToList();
				var registeredCourseIDsFromStudentsEnrollmentList = studentsEnrollmentList.Select(l => l.RegisteredCourseID).ToList();
				if (offeredCourseAttendanceID != null)
				{
					var attendance = aspireContext.OfferedCourseAttendanceDetails.Where(d => d.OfferedCourseAttendanceID == offeredCourseAttendanceID && d.RegisteredCours.DeletedDate == null && registeredCourseIDsFromStudentsEnrollmentList.Contains(d.RegisteredCourseID)).ToList();
					foreach (var offeredCourseAttendanceDetail in attendance)
					{
						var detail = studentsEnrollmentList.Find(e => e.RegisteredCourseID == offeredCourseAttendanceDetail.RegisteredCourseID);
						detail.Hours = offeredCourseAttendanceDetail.Hours;
						detail.OfferedCourseAttendaceDetailID = offeredCourseAttendanceDetail.OfferedCourseAttendanceDetailID;
					}
				}
				return studentsEnrollmentList;
			}
		}

		public static CustomViewClassAttendance GetOfferedCourseAttendances(int offeredCourseID)
		{
			using (var aspireContext = new AspireContext())
			{
				var customViewClassAttendance = aspireContext.OfferedCourses.Where(oc => oc.OfferedCourseID == offeredCourseID).Select(oc => new CustomViewClassAttendance
				{
					InstituteID = oc.Cours.AdmissionOpenProgram.Program.InstituteID,
					OfferedCourseID = oc.OfferedCourseID,
					SemesterID = oc.SemesterID,
					CourseCode = oc.Cours.CourseCode,
					Title = oc.Cours.Title,
					CreditHours = oc.Cours.CreditHours,
					ContactHours = oc.Cours.ContactHours,
					Program = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = oc.SemesterNo,
					Section = oc.Section,
					Shift = oc.Shift,
					TeacherName = oc.FacultyMember.Name,
				}).SingleOrDefault();
				if (customViewClassAttendance == null)
					return null;

				customViewClassAttendance.Details = aspireContext.OfferedCourseAttendances.Where(oc => oc.OfferedCourseID == offeredCourseID).Select(oca => new CustomViewClassAttendance.Detail
				{
					InstituteID = oca.OfferedCours.Cours.AdmissionOpenProgram.Program.InstituteID,
					OfferedCourseAttendanceID = oca.OfferedCourseAttendanceID,
					OfferedCourseID = oca.OfferedCourseID,
					ClassDate = oca.ClassDate,
					ClassType = oca.ClassType,
					Hours = oca.Hours,
					RoomName = oca.Room.RoomName,
					Remarks = oca.Remarks,
					TopicsCovered = oca.TopicsCovered,
					MarkedDate = oca.MarkedDate,
				}).OrderBy(a => a.ClassDate).ThenBy(a => a.OfferedCourseAttendanceID).ToList();
				return customViewClassAttendance;
			}
		}

		public sealed class RegisteredStudent
		{
			public string Enrollment { get; internal set; }
			public string FacultyMemberName { get; internal set; }
			public bool FeeDefaulter { get; internal set; }
			public byte? FreezeStatus { get; internal set; }
			public string Name { get; internal set; }
			public string ProgramAlias { get; internal set; }
			public int? RegistrationNo { get; internal set; }
			public int Section { get; internal set; }
			public short SemesterNo { get; internal set; }
			public byte Shift { get; internal set; }
			public byte? Status { get; internal set; }
			public string StudentProgramAlias { get; internal set; }
			public string Title { get; internal set; }
		}

		public static List<RegisteredStudent> GetRegisteredStudents(int offeredCourseID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.RegisteredCourses
					.Where(rc => rc.OfferedCourseID == offeredCourseID && rc.DeletedDate == null)
					.Select(rc => new RegisteredStudent
					{
						Title = rc.OfferedCours.Cours.Title,
						ProgramAlias = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
						SemesterNo = rc.OfferedCours.SemesterNo,
						Section = rc.OfferedCours.Section,
						Shift = rc.OfferedCours.Shift,
						Enrollment = rc.Student.Enrollment,
						RegistrationNo = rc.Student.RegistrationNo,
						Name = rc.Student.Name,
						StudentProgramAlias = rc.Student.AdmissionOpenProgram.Program.ProgramAlias,
						Status = rc.Status,
						FreezeStatus = rc.FreezedStatus,
						FeeDefaulter = rc.FeeDefaulter,
						FacultyMemberName = rc.OfferedCours.FacultyMember.Name,
					}).OrderBy(rc => rc.Enrollment).ToList();
			}
		}

		public static CustomAttendanceSummary GetAttendanceSummary(int offeredCourseID)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var customAttendanceSummary = aspireContext.OfferedCourses.Where(oc => oc.OfferedCourseID == offeredCourseID).Select(oc => new CustomAttendanceSummary
				{
					OfferedCourseID = oc.OfferedCourseID,
					SemesterID = oc.SemesterID,
					CourseCode = oc.Cours.CourseCode,
					Title = oc.Cours.Title,
					CreditHours = oc.Cours.CreditHours,
					ContactHours = oc.Cours.ContactHours,
					Program = oc.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = oc.SemesterNo,
					Section = oc.Section,
					Shift = oc.Shift,
					TeacherName = oc.FacultyMember.Name,
					TotalHours = oc.OfferedCourseAttendances.Sum(oca => (decimal?)oca.Hours) ?? 0
				}).SingleOrDefault();
				if (customAttendanceSummary == null)
					return null;

				var allowedPercentage = (byte)75;

				customAttendanceSummary.Summary = aspireContext.GetValidatedRegisteredCourses(offeredCourseID).Select(rc => new CustomAttendanceSummary.Detail
				{
					RegisteredCourseID = rc.RegisteredCourseID,
					OfferedCourseID = rc.OfferedCourseID,
					StudentID = rc.StudentID,
					Enrollment = rc.Student.Enrollment,
					Name = rc.Student.Name,
					Program = rc.Student.AdmissionOpenProgram.Program.ProgramAlias,
					FeeStatus = rc.StudentFee.Status,
					TotalHours = customAttendanceSummary.TotalHours,
					PresentHours = rc.OfferedCourseAttendanceDetails.Sum(ocad => (decimal?)ocad.Hours) ?? 0,
					AllowedPercentage = allowedPercentage,
				}).ToList();

				return customAttendanceSummary;
			}
		}

		public static CustomStudentAttendanceDetails GetStudentAttendanceDetails(int registeredCourseID)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var customStudentAttendanceDetails = aspireContext.RegisteredCourses.Where(rc => rc.RegisteredCourseID == registeredCourseID).Select(rc => new CustomStudentAttendanceDetails
				{
					OfferedCourseID = rc.OfferedCourseID,
					SemesterID = rc.OfferedCours.SemesterID,
					CourseCode = rc.OfferedCours.Cours.CourseCode,
					Title = rc.OfferedCours.Cours.Title,
					CreditHours = rc.OfferedCours.Cours.CreditHours,
					ContactHours = rc.OfferedCours.Cours.ContactHours,
					OfferedCourseProgram = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					OfferedCourseSemesterNo = rc.OfferedCours.SemesterNo,
					OfferedCourseSection = rc.OfferedCours.Section,
					OfferedCourseShift = rc.OfferedCours.Shift,
					TeacherName = rc.OfferedCours.FacultyMember.Name,
					Enrollment = rc.Student.Enrollment,
					Name = rc.Student.Name,
					RegistrationNo = rc.Student.RegistrationNo,
					RegisteredCourseID = rc.RegisteredCourseID,
					StudentProgram = rc.Student.AdmissionOpenProgram.Program.ProgramAlias,
					StudentID = rc.StudentID,
				}).SingleOrDefault();
				if (customStudentAttendanceDetails == null)
					return null;

				var studentSemester = aspireContext.StudentSemesters.Where(ss => ss.StudentID == customStudentAttendanceDetails.StudentID && ss.SemesterID == customStudentAttendanceDetails.SemesterID).Select(ss => new
				{
					ss.SemesterNo,
					ss.Section,
					ss.Shift,
				}).Single();

				customStudentAttendanceDetails.StudentSemesterNo = studentSemester.SemesterNo;
				customStudentAttendanceDetails.StudentSection = studentSemester.Section;
				customStudentAttendanceDetails.StudentShift = studentSemester.Shift;

				customStudentAttendanceDetails.Details = aspireContext.OfferedCourseAttendances.Where(oca => oca.OfferedCourseID == customStudentAttendanceDetails.OfferedCourseID).Select(oca => new CustomStudentAttendanceDetails.Detail
				{
					OfferedCourseID = oca.OfferedCourseID,
					ClassDate = oca.ClassDate,
					ClassType = oca.ClassType,
					OfferedCourseAttendanceID = oca.OfferedCourseAttendanceID,
					Remarks = oca.Remarks,
					TopicsCovered = oca.TopicsCovered,
					RoomName = oca.Room.RoomName,
					TotalHours = oca.Hours,
					PresentHours = oca.OfferedCourseAttendanceDetails.Where(ocad => ocad.RegisteredCourseID == customStudentAttendanceDetails.RegisteredCourseID).Select(ocad => (decimal?)ocad.Hours).FirstOrDefault() ?? 0,
				}).OrderBy(a => a.ClassDate).ThenBy(a => a.OfferedCourseAttendanceID).ToList();
				customStudentAttendanceDetails.ClassesTotalHours = customStudentAttendanceDetails.Details.Sum(d => d.TotalHours);
				customStudentAttendanceDetails.PresentTotalHours = customStudentAttendanceDetails.Details.Sum(d => d.PresentHours);
				customStudentAttendanceDetails.AbsentTotalHours = customStudentAttendanceDetails.Details.Sum(d => d.AbsentHours);
				customStudentAttendanceDetails.PresentTotalPercentage = customStudentAttendanceDetails.ClassesTotalHours == 0 ? 0 : (customStudentAttendanceDetails.PresentTotalHours * 100) / customStudentAttendanceDetails.ClassesTotalHours;
				return customStudentAttendanceDetails;
			}
		}
	}
}