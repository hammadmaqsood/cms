﻿using Aspire.BL.Entities.QualityAssurance.Common;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Aspire.BL.QualityAssurance
{
	public static class Common
	{
		public static SurveyConduct GetSurveyConduct(int surveyConductID, int? studentID, int? registeredCourseID, int? facultyMemberID, int? offeredCourseID, out SurveyStatus status)
		{
			using (var aspireContext = new AspireContext())
			{
				var surveyConduct = aspireContext.SurveyConducts.Include(sc => sc.Survey).SingleOrDefault(sc => sc.SurveyConductID == surveyConductID);
				if (surveyConduct == null)
				{
					status = SurveyStatus.NoRecordFound;
					return null;
				}
				if (DateTime.Today < surveyConduct.OpenDate)
				{
					status = SurveyStatus.NotYetOpened;
					return null;
				}
				if (surveyConduct.CloseDate != null && surveyConduct.CloseDate.Value < DateTime.Today)
				{
					status = SurveyStatus.Closed;
					return null;
				}

				switch (surveyConduct.Survey.SurveyTypeEnum)
				{
					case Survey.SurveyTypes.StudentWise:
						if (studentID == null)
							throw new ArgumentNullException(nameof(studentID));
						if (registeredCourseID != null)
							throw new ArgumentException("Must be null.", nameof(registeredCourseID));
						if (facultyMemberID != null)
							throw new ArgumentException("Must be null.", nameof(facultyMemberID));
						if (offeredCourseID != null)
							throw new ArgumentException("Must be null.", nameof(offeredCourseID));
						if (aspireContext.SurveyUsers.Any(su => su.SurveyConductID == surveyConductID && su.StudentID == studentID.Value))
						{
							status = SurveyStatus.AlreadyCompleted;
							return null;
						}
						break;
					case Survey.SurveyTypes.StudentCourseWise:
						if (studentID == null)
							throw new ArgumentNullException(nameof(studentID));
						if (registeredCourseID == null)
							throw new ArgumentNullException(nameof(registeredCourseID));
						if (facultyMemberID != null)
							throw new ArgumentException("Must be null.", nameof(facultyMemberID));
						if (offeredCourseID != null)
							throw new ArgumentException("Must be null.", nameof(offeredCourseID));
						if (aspireContext.SurveyUsers.Any(su => su.SurveyConductID == surveyConductID && su.StudentID == studentID.Value && su.RegisteredCourseID == registeredCourseID.Value))
						{
							status = SurveyStatus.AlreadyCompleted;
							return null;
						}
						break;
					case Survey.SurveyTypes.FacultyMemberWise:
						if (studentID != null)
							throw new ArgumentException("Must be null.", nameof(studentID));
						if (registeredCourseID != null)
							throw new ArgumentException("Must be null.", nameof(registeredCourseID));
						if (facultyMemberID == null)
							throw new ArgumentNullException(nameof(facultyMemberID));
						if (offeredCourseID != null)
							throw new ArgumentException("Must be null.", nameof(offeredCourseID));
						if (aspireContext.SurveyUsers.Any(su => su.SurveyConductID == surveyConductID && su.FacultyMemberID == facultyMemberID.Value))
						{
							status = SurveyStatus.AlreadyCompleted;
							return null;
						}
						break;
					case Survey.SurveyTypes.FacultyMemberCourseWise:
						if (studentID != null)
							throw new ArgumentException("Must be null.", nameof(studentID));
						if (registeredCourseID != null)
							throw new ArgumentException("Must be null.", nameof(registeredCourseID));
						if (facultyMemberID == null)
							throw new ArgumentNullException(nameof(facultyMemberID));
						if (offeredCourseID == null)
							throw new ArgumentNullException(nameof(offeredCourseID));
						if (aspireContext.SurveyUsers.Any(su => su.SurveyConductID == surveyConductID && su.FacultyMemberID == facultyMemberID.Value && su.OfferedCourseID == offeredCourseID.Value))
						{
							status = SurveyStatus.AlreadyCompleted;
							return null;
						}
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				aspireContext.Entry(surveyConduct).Reference(sc => sc.Survey).Query().Include(s => s.SurveyQuestionGroups.Select(g => g.SurveyQuestions.Select(q => q.SurveyQuestionOptions))).Load();
				status = SurveyStatus.Open;
				return surveyConduct;
			}
		}

		public static SurveySubmissionResults SubmitSurvey(int surveyConductID, int? studentID, int? registeredCourseID, int? facultyMemberID, int? offeredCourseID, List<SurveyUserAnswer> surveyUserAnswers, int userLoginHistoryID)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var surveyConduct = aspireContext.SurveyConducts.Include(sc => sc.Survey).SingleOrDefault(sc => sc.SurveyConductID == surveyConductID);
				if (surveyConduct == null)
					return SurveySubmissionResults.NoRecordFound;
				if (DateTime.Today < surveyConduct.OpenDate)
					return SurveySubmissionResults.NotOpenedYet;
				if (surveyConduct.CloseDate != null && surveyConduct.CloseDate < DateTime.Today)
					return SurveySubmissionResults.SurveyClosed;

				switch (surveyConduct.Survey.SurveyTypeEnum)
				{
					case Survey.SurveyTypes.StudentWise:
						if (studentID == null)
							throw new ArgumentNullException(nameof(studentID));
						if (registeredCourseID != null)
							throw new ArgumentException("Must be null.", nameof(registeredCourseID));
						if (facultyMemberID != null)
							throw new ArgumentException("Must be null.", nameof(facultyMemberID));
						if (offeredCourseID != null)
							throw new ArgumentException("Must be null.", nameof(offeredCourseID));
						break;
					case Survey.SurveyTypes.StudentCourseWise:
						if (studentID == null)
							throw new ArgumentNullException(nameof(studentID));
						if (registeredCourseID == null)
							throw new ArgumentNullException(nameof(registeredCourseID));
						if (facultyMemberID != null)
							throw new ArgumentException("Must be null.", nameof(facultyMemberID));
						if (offeredCourseID != null)
							throw new ArgumentException("Must be null.", nameof(offeredCourseID));
						break;
					case Survey.SurveyTypes.FacultyMemberWise:
						if (studentID != null)
							throw new ArgumentException("Must be null.", nameof(studentID));
						if (registeredCourseID != null)
							throw new ArgumentException("Must be null.", nameof(registeredCourseID));
						if (facultyMemberID == null)
							throw new ArgumentNullException(nameof(facultyMemberID));
						if (offeredCourseID != null)
							throw new ArgumentException("Must be null.", nameof(offeredCourseID));
						break;
					case Survey.SurveyTypes.FacultyMemberCourseWise:
						if (studentID != null)
							throw new ArgumentException("Must be null.", nameof(studentID));
						if (registeredCourseID != null)
							throw new ArgumentException("Must be null.", nameof(registeredCourseID));
						if (facultyMemberID == null)
							throw new ArgumentNullException(nameof(facultyMemberID));
						if (offeredCourseID == null)
							throw new ArgumentNullException(nameof(offeredCourseID));
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				var alreadyExists = aspireContext.SurveyUsers.Any(su => su.SurveyConductID == surveyConductID && su.StudentID == studentID && su.RegisteredCourseID == registeredCourseID && su.FacultyMemberID == facultyMemberID);
				if (alreadyExists)
					return SurveySubmissionResults.AlreadyExists;

				var surveyUser = new SurveyUser
				{
					SurveyConductID = surveyConductID,
					StudentID = studentID,
					RegisteredCourseID = registeredCourseID,
					FacultyMemberID = facultyMemberID,
					OfferedCourseID = offeredCourseID,
					SurveyCompletionDate = DateTime.Now,
				};

				var surveyQuestionIDs = aspireContext.SurveyQuestions.Where(sq => sq.SurveyQuestionGroup.SurveyID == surveyConduct.SurveyID).Select(s => new { s.SurveyQuestionID, s.QuestionType }).ToList();

				foreach (var question in surveyQuestionIDs)
				{
					var surveyUserAnswer = surveyUserAnswers.Find(sua => sua.SurveyQuestionID == question.SurveyQuestionID);
					if (surveyUserAnswer == null)
						throw new InvalidOperationException("Answer to the question is missing.");
					switch ((SurveyQuestion.QuestionTypes)question.QuestionType)
					{
						case SurveyQuestion.QuestionTypes.Options:
							if (!aspireContext.SurveyQuestionOptions.Any(sqo => sqo.SurveyQuestionID == surveyUserAnswer.SurveyQuestionID && sqo.SurveyQuestionOptionID == surveyUserAnswer.SurveyQuestionOptionID))
								throw new InvalidOperationException("Invalid option selected for question.");
							if (surveyUserAnswer.OptionText != null)
								throw new InvalidOperationException("Option Text must be null for options type question.");
							break;
						case SurveyQuestion.QuestionTypes.Text:
							if (surveyUserAnswer.SurveyQuestionOptionID != null)
								throw new InvalidOperationException("Selected answer must be text.");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					surveyUser.SurveyUserAnswers.Add(surveyUserAnswer);
					surveyUserAnswers.Remove(surveyUserAnswer);
				}

				if (surveyUserAnswers.Any())
					throw new InvalidOperationException("must be empty.");

				aspireContext.SurveyUsers.Add(surveyUser);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return SurveySubmissionResults.Success;
			}
		}
	}
}
