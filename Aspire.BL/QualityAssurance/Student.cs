﻿using Aspire.BL.Entities.QualityAssurance.Student;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.BL.QualityAssurance
{
	public static class Student
	{
		#region Student Surveys

		public static List<CustomStudentSurveyConduct> GetStudentSurveys(int studentID)
		{
			using (var aspireContext = new AspireContext())
			{
				var conducts = new List<CustomStudentSurveyConduct>();
				var today = DateTime.Today;
				var instituteID = aspireContext.Students.Where(s => s.StudentID == studentID).Select(s => (int?)s.AdmissionOpenProgram.Program.InstituteID).SingleOrDefault();
				if (instituteID == null)
					return null;
				var openedSurveyConducts = aspireContext.SurveyConducts.Where(sc => sc.Survey.InstituteID == instituteID.Value && sc.Status == SurveyConduct.StatusActive);
				openedSurveyConducts = openedSurveyConducts.Where(sc => sc.OpenDate <= today);
				openedSurveyConducts = openedSurveyConducts.Where(sc => sc.CloseDate == null || today <= sc.CloseDate);

				var openedStudentWiseSurveyConducts = openedSurveyConducts.Where(sc => sc.Survey.SurveyType == Survey.SurveyTypeStudentWise);
				openedStudentWiseSurveyConducts = openedStudentWiseSurveyConducts.Where(sc => !sc.SurveyUsers.Any(su => su.SurveyConductID == sc.SurveyConductID && su.StudentID == studentID));
				conducts.AddRange(openedStudentWiseSurveyConducts.Select(sc => new CustomStudentSurveyConduct
				{
					StudentID = studentID,
					SurveyConductID = sc.SurveyConductID,
					SemesterID = sc.SemesterID,
					SurveyName = sc.Survey.SurveyName,
					SurveyType = sc.Survey.SurveyType,
				}));

				var openedStudentCourseWiseSurveyConducts = openedSurveyConducts.Where(sc => sc.Survey.SurveyType == Survey.SurveyTypeStudentCourseWise).Select(sc => new
				{
					sc.SurveyConductID,
					sc.SemesterID,
					sc.Survey.SurveyName,
					sc.Survey.SurveyType,
				}).ToList();

				foreach (var surveyConduct in openedStudentCourseWiseSurveyConducts)
				{
					var registeredCourses = aspireContext.RegisteredCourses
						.Where(rc => rc.StudentID == studentID && rc.DeletedDate == null && rc.OfferedCours.SemesterID == surveyConduct.SemesterID)
						.Where(rc => !aspireContext.SurveyUsers.Any(su => su.SurveyConductID == surveyConduct.SurveyConductID && su.StudentID == rc.StudentID && su.RegisteredCourseID == rc.RegisteredCourseID)).Select(rc => new
						{
							rc.StudentID,
							rc.RegisteredCourseID,
							rc.OfferedCours.Cours.Title,
							rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
							rc.OfferedCours.SemesterNo,
							rc.OfferedCours.Section,
							rc.OfferedCours.Shift,
							rc.OfferedCours.FacultyMember.Name,
						});
					conducts.AddRange(registeredCourses.ToList().Select(registeredCourse => new CustomStudentSurveyConduct
					{
						SurveyConductID = surveyConduct.SurveyConductID,
						SemesterID = surveyConduct.SemesterID,
						StudentID = registeredCourse.StudentID,
						SurveyName = surveyConduct.SurveyName,
						RegisteredCourseID = registeredCourse.RegisteredCourseID,
						FacultyMemberName = registeredCourse.Name,
						Title = registeredCourse.Title,
						Program = registeredCourse.ProgramAlias,
						SemesterNo = registeredCourse.SemesterNo,
						Section = registeredCourse.Section,
						Shift = registeredCourse.Shift,
						SurveyType = surveyConduct.SurveyType,
					}));
				}

				return conducts;
			}
		}

		#endregion

		#region Survey Question Groups

		public class CustomRegisteredCours
		{
			public string CourseCode { get; set; }
			public string Title { get; set; }
			public short SemesterID { get; set; }
			public string Program { get; set; }
			public short SemesterNo { get; set; }
			public int Section { get; set; }
			public byte Shift { get; set; }
			public string Teacher { get; set; }
		}

		public static CustomRegisteredCours GetRegisteredCourseInfo(int studentID, int registeredCourseID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.RegisteredCourses.Where(rc => rc.StudentID == studentID && rc.RegisteredCourseID == registeredCourseID && rc.DeletedDate == null).Select(rc => new CustomRegisteredCours
				{
					Title = rc.OfferedCours.Cours.Title,
					CourseCode = rc.OfferedCours.Cours.CourseCode,
					SemesterID = rc.OfferedCours.SemesterID,
					Program = rc.OfferedCours.Cours.AdmissionOpenProgram.Program.ProgramAlias,
					SemesterNo = rc.OfferedCours.SemesterNo,
					Section = rc.OfferedCours.Section,
					Shift = rc.OfferedCours.Shift,
					Teacher = rc.OfferedCours.FacultyMember.Name
				}).SingleOrDefault();
			}
		}

		#endregion
	}
}

