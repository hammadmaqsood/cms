﻿using Aspire.BL.Common;
using Aspire.BL.Entities.Common;
using Aspire.BL.Entities.QualityAssurance.Admin;
using Aspire.Model.Context;
using Aspire.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.BL.QualityAssurance
{
	public static class Admin
	{
		#region Control Panel

		public static SurveyConduct.ValidationResults AddSurveyConduct(short semesterID, int instituteID, int surveyID, byte type, DateTime openDate, DateTime? closeDate, byte status, int userLoginHistoryID)
		{
			using (var aspireContext = new AspireContext(true))
			{
				if (aspireContext.SurveyConducts.Any(sc => sc.SemesterID == semesterID && sc.Survey.InstituteID == instituteID && sc.SurveyID == surveyID && sc.SurveyConductType == type))
					return SurveyConduct.ValidationResults.AlreadyExists;
				var result = SurveyConduct.Validate(openDate, closeDate);
				if (result != SurveyConduct.ValidationResults.Success)
					return result;
				var surveyConduct = new SurveyConduct
				{
					SemesterID = semesterID,
					SurveyID = surveyID,
					SurveyConductType = type,
					OpenDate = openDate,
					CloseDate = closeDate,
					Status = status,
				};
				aspireContext.SurveyConducts.Add(surveyConduct);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return SurveyConduct.ValidationResults.Success;
			}
		}

		public static SurveyConduct.ValidationResults UpdateSurveyConduct(int surveyConductID, DateTime openDate, DateTime? closeDate, byte status, int userLoginHistoryID)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var surveyConduct = aspireContext.SurveyConducts.SingleOrDefault(sc => sc.SurveyConductID == surveyConductID);
				if (surveyConduct == null)
					return SurveyConduct.ValidationResults.NoRecordFound;
				openDate = openDate.Date;
				closeDate = closeDate?.Date;
				if (closeDate != null)
					if (openDate.Date > closeDate.Value.Date)
						return SurveyConduct.ValidationResults.InvalidDateRange;
				//if (surveyConduct.OpenDate != openDate)
				//{
				//	if (surveyConduct.OpenDate > DateTime.Today)
				//	{
				//		if (openDate.Date < DateTime.Today)
				//			return SurveyConduct.ValidationResults.InvalidOpenDate;
				//		surveyConduct.OpenDate = openDate;
				//	}
				//}

				//if (surveyConduct.CloseDate != closeDate)
				//{
				//	if (surveyConduct.CloseDate == null)
				//	{
				//		if (closeDate != null && closeDate.Value.Date < DateTime.Today)
				//			return SurveyConduct.ValidationResults.InvalidCloseDate;
				//		if (closeDate != null)
				//			if (surveyConduct.OpenDate > closeDate.Value)
				//				return SurveyConduct.ValidationResults.InvalidDateRange;
				//		surveyConduct.CloseDate = closeDate;
				//	}
				//	else
				//	{
				//		if (closeDate == null)
				//			surveyConduct.CloseDate = null;
				//		else
				//		{
				//			if (closeDate.Value.Date < DateTime.Today)
				//				return SurveyConduct.ValidationResults.InvalidCloseDate;
				//			if (surveyConduct.OpenDate > closeDate.Value)
				//				return SurveyConduct.ValidationResults.InvalidDateRange;
				//			surveyConduct.CloseDate = closeDate;
				//		}
				//	}
				//}
				surveyConduct.OpenDate = openDate;
				surveyConduct.CloseDate = closeDate;
				surveyConduct.Status = status;
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return SurveyConduct.ValidationResults.Success;
			}
		}

		public static bool DeleteSurveyConduct(int surveyConductID, int userLoginHistoryID, out bool isDatePassed)
		{
			using (var aspireContext = new AspireContext(true))
			{
				var surveyConduct = aspireContext.SurveyConducts.SingleOrDefault(sc => sc.SurveyConductID == surveyConductID);
				if (surveyConduct == null)
				{
					isDatePassed = false;
					return false;
				}
				isDatePassed = surveyConduct.OpenDate <= DateTime.Today;
				if (isDatePassed)
					return false;
				aspireContext.SurveyConducts.Remove(surveyConduct);
				aspireContext.SaveChangesAndCommitTransaction(userLoginHistoryID);
				return true;
			}
		}

		public static SurveyConduct GetSurveyConduct(int surveyConductID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.SurveyConducts.Include(sc=>sc.Survey).SingleOrDefault(sc => sc.SurveyConductID == surveyConductID);
			}
		}

		public static List<CustomSurveyConduct> GetSurveyConducts(int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection, short? semesterID, int? instituteID, out int virtualItemCount)
		{
			using (var aspireContext = new AspireContext())
			{
				var query = aspireContext.SurveyConducts.AsQueryable();
				if (semesterID != null)
					query = query.Where(q => q.SemesterID == semesterID);
				if (instituteID != null)
					query = query.Where(q => q.Survey.InstituteID == instituteID);

				virtualItemCount = query.Count();
				switch (sortExpression)
				{
					case "InstituteAlias":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.Survey.Institute.InstituteAlias) : query.OrderByDescending(q => q.Survey.Institute.InstituteAlias);
						break;
					case "Semester":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.SemesterID) : query.OrderByDescending(q => q.SemesterID);
						break;
					case "SurveyName":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.Survey.SurveyName) : query.OrderByDescending(q => q.Survey.SurveyName);
						break;
					case "Type":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.SurveyConductType) : query.OrderByDescending(q => q.SurveyConductType);
						break;
					case "OpenDate":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.OpenDate) : query.OrderByDescending(q => q.OpenDate);
						break;
					case "CloseDate":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.CloseDate) : query.OrderByDescending(q => q.CloseDate);
						break;
					case "Status":
						query = sortDirection == SortDirection.Ascending ? query.OrderBy(q => q.Status) : query.OrderByDescending(q => q.Status);
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				query = query.Skip(pageIndex * pageSize).Take(pageSize);

				return query.Select(q => new CustomSurveyConduct
				{
					SurveyConductID = q.SurveyConductID,
					InstituteAlias = q.Survey.Institute.InstituteAlias,
					SemesterID = q.SemesterID,
					SurveyName = q.Survey.SurveyName,
					Type = q.SurveyConductType,
					OpenDate = q.OpenDate,
					CloseDate = q.CloseDate,
					Status = q.Status,
				}).ToList();
			}
		}

		public static List<CustomListItem> GetSurveys(int instituteID)
		{
			using (var aspireContext = new AspireContext())
			{
				return aspireContext.Surveys.Where( s=>s.InstituteID==instituteID).Select(s => new CustomListItem
				{
					ID = s.SurveyID,
					Text = s.SurveyName,
				}).OrderBy(s => s.Text).ToList();
			}
		}

		public static SurveyConduct UpdateSurveyConductStatus(int surveyConductID, byte status, int userLoginHistoryID)
		{
			using (var aspireContext = new AspireContext())
			{
				var surveyConduct = aspireContext.SurveyConducts.SingleOrDefault(sc => sc.SurveyConductID == surveyConductID);
				if (surveyConduct == null)
					return null;
				surveyConduct.Status = status;
				aspireContext.SaveChanges(userLoginHistoryID);
				return surveyConduct;
			}
		}

		#endregion
	}
}