﻿using Aspire.Lib.Extensions;
using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	public sealed class EnrollmentTextBox : UserControl
	{
		private AspireTextBox _tb;
		private AspireStringValidator _sv;
		private AspireButton _btn;

		public bool ButtonEnabled
		{
			get => this._btn.Enabled;
			set => this._btn.Enabled = value;
		}
		public bool TextBoxEnabled
		{
			get => this._tb.Enabled;
			set => this._tb.Enabled = value;
		}

		public string ValidationGroup
		{
			get
			{
				this.EnsureChildControls();
				return this._tb.ValidationGroup;
			}
			set
			{
				this.EnsureChildControls();
				this._tb.ValidationGroup = value;
				this._btn.ValidationGroup = value;
				this._sv.ValidationGroup = value;
			}
		}

		private const bool DefaultAllowNull = false;
		[DefaultValue(DefaultAllowNull)]
		public bool AllowNull
		{
			get
			{
				this.EnsureChildControls();
				return this._sv.AllowNull;
			}
			set
			{
				this.EnsureChildControls();
				this._sv.AllowNull = value;
			}
		}

		public string Enrollment
		{
			get
			{
				this.EnsureChildControls();
				return this._tb.Text?.ToNullIfWhiteSpace();
			}
			set
			{
				this.EnsureChildControls();
				this._tb.Text = value?.ToNullIfWhiteSpace();
			}
		}

		protected override void CreateChildControls()
		{
			this._tb = new AspireTextBox { ID = "tb", MaxLength = 15, };
			this._sv = new AspireStringValidator
			{
				ControlToValidate = this._tb.ID,
				AllowNull = DefaultAllowNull,
				RequiredErrorMessage = "This field is required.",
				InvalidDataErrorMessage = "Invalid Enrollment Format.",
			};
			this._btn = new AspireButton { ID = "btn", DisableOnClick = false, FontAwesomeIcon = FontAwesomeIcons.solid_arrow_right, ButtonType = AspireButton.ButtonTypes.Primary };
			this._btn.Click += this._btn_Click;
			var divInputGroup = new Panel { CssClass = "input-group", DefaultButton = this._btn.UniqueID };
			this.Controls.Add(divInputGroup);
			{
				divInputGroup.Controls.Add(this._tb);
				var divInputGrpBtn = new HtmlGenericControl("div");
				divInputGrpBtn.Attributes.Add("class", "input-group-btn");
				divInputGroup.Controls.Add(divInputGrpBtn);
				{
					divInputGrpBtn.Controls.Add(this._btn);
					var searchBtn = new HtmlGenericControl("a");
					searchBtn.Attributes.Add("class", "btn btn-primary");
					searchBtn.Attributes.Add("type", "button");
					searchBtn.Attributes.Add("onclick", "searchStudent(this);");
					divInputGrpBtn.Controls.Add(searchBtn);
					{
						searchBtn.Controls.Add(new AspireGlyphicon { Icon = AspireGlyphicons.search });
					}
				}
			}
			this.Controls.Add(this._sv);
			base.CreateChildControls();
		}

		public delegate void SearchHandler(object sender, EventArgs args);
		public event SearchHandler Search;

		private void _btn_Click(object sender, EventArgs e)
		{
			this.Search?.Invoke(this, EventArgs.Empty);
		}

		public override void Focus()
		{
			this.EnsureChildControls();
			this._tb.Focus();
		}

#if DEBUG
		protected override void OnLoad(EventArgs e)
		{
			if (this.Page.Request.IsLocal && !this.Page.IsPostBack && string.IsNullOrEmpty(this.Enrollment))
				this.Enrollment = "01-111191-001";
			base.OnLoad(e);
		}
#endif
	}
}
