﻿using System;

namespace Aspire.Lib.WebControls
{
	public class RadioButtonList : System.Web.UI.WebControls.RadioButtonList
	{
		protected override void OnPreRender(EventArgs e)
		{
			if (this.Enabled == false)
				this.AutoPostBack = false;
			base.OnPreRender(e);
		}
	}
}
