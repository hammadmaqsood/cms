﻿namespace Aspire.Lib.WebControls
{
	internal interface INumberValidator
	{
		bool AllowNull { get; set; }
		string RequiredErrorMessage { get; set; }
		string InvalidNumberErrorMessage { get; set; }
		string InvalidRangeErrorMessage { get; set; }
	}
}