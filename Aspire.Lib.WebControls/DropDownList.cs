﻿using System;

namespace Aspire.Lib.WebControls
{
	public class DropDownList : System.Web.UI.WebControls.DropDownList
	{
		protected override void OnPreRender(EventArgs e)
		{
			if (this.CausesValidation == false && this.AutoPostBack)
				this.Attributes.Add("onchange", "window.Page_BlockSubmit=false;");
			base.OnPreRender(e);
		}
	}
}