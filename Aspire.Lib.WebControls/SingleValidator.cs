﻿using System.ComponentModel;

namespace Aspire.Lib.WebControls
{
	public class SingleValidator : NumberValidator<float>
	{
		protected override float DefaultMaxValue => float.MaxValue;

		protected override float DefaultMinValue => float.MinValue;

		protected override string GetClientValidationFunction()
		{
			return "ValidateDouble";
		}

		[DefaultValue(float.MaxValue)]
		public override float MaxValue
		{
			get { return (float)this.ViewState.GetValue(AspireViewStateKeys.MaxValue, DefaultMaxValue); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MaxValue, value); }
		}

		[DefaultValue(float.MinValue)]
		public override float MinValue
		{
			get { return (float)this.ViewState.GetValue(AspireViewStateKeys.MinValue, DefaultMinValue); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MinValue, value); }
		}
	}
}