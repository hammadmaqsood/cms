using System;

namespace Aspire.Lib.WebControls
{
	public class AspireDropDownList : DropDownList
	{
		protected override void OnPreRender(EventArgs e)
		{
			this.AddCssClass("form-control");
			base.OnPreRender(e);
		}
	}
}