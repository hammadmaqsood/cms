﻿using Aspire.Lib.Helpers;
using System;
using System.ComponentModel;
using System.Web.UI;

namespace Aspire.Lib.WebControls
{
	public class Button : System.Web.UI.WebControls.Button
	{
		public string EncryptedCommandArgument
		{
			get { return this.CommandArgument.Decrypt(); }
			set { this.CommandArgument = value.Encrypt(); }
		}

		[Localizable(true), DefaultValue("")]
		public string ConfirmMessageTitle
		{
			get => (string) this.ViewState.GetValue(AspireViewStateKeys.ConfirmMessageTitle, string.Empty);
			set => this.ViewState.SetValue(AspireViewStateKeys.ConfirmMessageTitle, value);
		}

		[Localizable(true), DefaultValue("")]
		public string ConfirmMessage
		{
			get => (string) this.ViewState.GetValue(AspireViewStateKeys.ConfirmMessage, string.Empty);
			set => this.ViewState.SetValue(AspireViewStateKeys.ConfirmMessage, value);
		}

		[Localizable(true), DefaultValue(true)]
		public bool DisableOnClick
		{
			get => (bool) this.ViewState.GetValue(AspireViewStateKeys.DisableOnClick, true);
			set => this.ViewState.SetValue(AspireViewStateKeys.DisableOnClick, value);
		}

		protected virtual string GetConfirmMethodJavascript()
		{
			return $"confirm({this.ConfirmMessage.Trim().JavaScriptStringEncode(true)})";
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (!string.IsNullOrWhiteSpace(this.ConfirmMessage) && !string.IsNullOrWhiteSpace(this.OnClientClick))
				throw new InvalidOperationException("ConfirmMessage and OnClientClick cannot be set both at a time.");

			var onClientClick = this.OnClientClick ?? string.Empty;
			if (!string.IsNullOrWhiteSpace(this.ConfirmMessage) || !string.IsNullOrWhiteSpace(this.ConfirmMessageTitle))
			{
				if (string.IsNullOrWhiteSpace(this.ConfirmMessageTitle))
					this.ConfirmMessageTitle = "Confirm";
				onClientClick = $@"if(!{this.GetConfirmMethodJavascript()})return false;";
			}

			var pageClientValidateScript = string.Empty;
			if (this.CausesValidation && this.Page.GetValidators(this.ValidationGroup).Count > 0)
				pageClientValidateScript = $@"if(!Page_ClientValidate({this.ValidationGroup.JavaScriptStringEncode(true)}))return false;";

			if (!string.IsNullOrWhiteSpace(pageClientValidateScript))
				base.OnClientClick = pageClientValidateScript;
			if (!string.IsNullOrWhiteSpace(onClientClick))
				base.OnClientClick += onClientClick;
			if (this.DisableOnClick)
				base.OnClientClick += "var btn=this;setTimeout(function (){btn.disabled=true;},200);";
			base.Render(writer);
		}
	}
}