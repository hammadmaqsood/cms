﻿using System.Collections;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	[ParseChildren(true, "AspireTabPages")]
	[PersistChildren(false)]
	public class AspireTabContainer : WebControl, INamingContainer
	{
		private readonly AspireTabPageCollection _aspireTabPages;

		public AspireTabContainer()
		{
			this._aspireTabPages = new AspireTabPageCollection(this);
			this.ActiveTabPageIndex = 0;
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public AspireTabPageCollection AspireTabPages => this._aspireTabPages;

		[DefaultValue(0)]
		public int ActiveTabPageIndex
		{
			get { return (int)this.ViewState.GetValue(AspireViewStateKeys.ActiveTabPageIndex, 0); }
			set { this.ViewState.SetValue(AspireViewStateKeys.ActiveTabPageIndex, value); }
		}

		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		protected override void Render(HtmlTextWriter writer)
		{
			/*<div role="tabpanel">
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">...</div>
    <div role="tabpanel" class="tab-pane" id="profile">...</div>
    <div role="tabpanel" class="tab-pane" id="messages">...</div>
    <div role="tabpanel" class="tab-pane" id="settings">...</div>
  </div>

</div>*/
			writer.AddAttribute("role", "tabpanel");
			writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);
			writer.RenderBeginTag(HtmlTextWriterTag.Div);
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Class, "nav nav-tabs");
				writer.AddAttribute("role", "tablist");
				writer.RenderBeginTag(HtmlTextWriterTag.Ul);
				{
					foreach (AspireTabPage aspireTabPage in this.AspireTabPages)
						aspireTabPage.RenderHeader(writer);
				}
				writer.RenderEndTag();
				writer.AddAttribute(HtmlTextWriterAttribute.Class, "tab-content");
				writer.RenderBeginTag(HtmlTextWriterTag.Div);
				{
					foreach (AspireTabPage aspireTabPage in this.AspireTabPages)
						aspireTabPage.RenderControl(writer);
				}
				writer.RenderEndTag();
			}
			writer.RenderEndTag();
		}

		public class AspireTabPageCollection : CollectionBase
		{
			private readonly Control Parent;

			public AspireTabPageCollection(Control parent)
			{
				this.Parent = parent;
			}

			public AspireTabPage this[int index]
			{
				get { return (AspireTabPage)this.List[index]; }
				set { this.List[index] = value; }
			}

			public void Add(AspireTabPage aspireTabPage)
			{
				this.List.Add(aspireTabPage);
				this.Parent.Controls.Add(aspireTabPage);
			}

			public void Insert(int index, AspireTabPage aspireTabPage)
			{
				this.List.Insert(index, aspireTabPage);
			}

			public void Remove(AspireTabPage aspireTabPage)
			{
				this.List.Remove(aspireTabPage);
			}

			public bool Contains(AspireTabPage aspireTabPage)
			{
				return this.List.Contains(aspireTabPage);
			}

			public int IndexOf(AspireTabPage aspireTabPage)
			{
				return this.List.IndexOf(aspireTabPage);
			}

			public void CopyTo(AspireTabPage[] array, int index)
			{
				this.List.CopyTo(array, index);
			}
		}
	}
}