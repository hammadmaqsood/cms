﻿using Aspire.Lib.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Script.Serialization;

namespace Aspire.Lib.WebControls
{
	public class RecaptchaValidator : CustomValidator
	{
		public class GoogleResponse
		{
			public bool success { get; set; }
			public string challenge_ts { get; set; }
			public string hostname { get; set; }
		}

		private static readonly HttpClient HttpClient = new HttpClient();

		private static bool IsRecaptchaValid(string secret, string response, string remoteIP)
		{
			lock (HttpClient)
			{
				var values = new List<KeyValuePair<string, string>>
				{
					new KeyValuePair<string, string>("secret", secret),
					new KeyValuePair<string, string>("response", response),
					new KeyValuePair<string, string>("remoteip", remoteIP)
				};

				using (var content = new FormUrlEncodedContent(values))
				{
					var dataResult = HttpClient.PostAsync("https://www.google.com/recaptcha/api/siteverify", content).Result;
					var responseJson = dataResult.Content.ReadAsStringAsync().Result;
					return new JavaScriptSerializer().Deserialize<GoogleResponse>(responseJson).success;
				}
			}
		}

		protected override bool OnServerValidate(string value)
		{
#if DEBUG
			if (Configurations.Current.AppSettings.IsDevelopmentPC)
				return true;
#endif
			var secret = Configurations.Current.AppSettings.RecaptchaPrivate;
			if (string.IsNullOrWhiteSpace(secret))
				throw new InvalidOperationException("ReCaptcha.Private AppSettings not found in Web.config.");
			var response = this.Page.Request.Form["g-recaptcha-response"]?.Trim(',');
			if (string.IsNullOrWhiteSpace(response))
				return false;
			var remoteIP = this.Page.Request.UserHostAddress;
			return IsRecaptchaValid(secret, response, remoteIP);
		}

		protected override void OnPreRender(EventArgs e)
		{
#if DEBUG
			if (Configurations.Current.AppSettings.IsDevelopmentPC)
				return;
#endif
			if (!string.IsNullOrWhiteSpace(this.ControlToValidate))
				throw new InvalidOperationException("ControlToValidate must be empty.");
			this.AddCssClass(nameof(RecaptchaValidator));
			this.ValidateEmptyText = true;
			this.ClientValidationFunction = "ValidateRecaptcha";
			base.OnPreRender(e);
		}
	}
}
