﻿using System;

namespace Aspire.Lib.WebControls
{
	public class AspireRadioButtonList : RadioButtonList
	{
		protected override void OnPreRender(EventArgs e)
		{
			this.AddCssClass("AspireRadioButtonList");
			base.OnPreRender(e);
		}
	}
}