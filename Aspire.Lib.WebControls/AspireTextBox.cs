﻿using System;

namespace Aspire.Lib.WebControls
{
	public class AspireTextBox : Aspire.Lib.WebControls.TextBox
	{
		protected override void OnPreRender(EventArgs e)
		{
			this.AddCssClass("form-control");
			base.OnPreRender(e);
		}
	}
}
