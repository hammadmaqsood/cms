﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace Aspire.Lib.WebControls
{
	public abstract class NumberValidator<T> : Aspire.Lib.WebControls.CustomValidator, INumberValidator where T : struct,IComparable<T>, IConvertible
	{
		protected abstract T DefaultMinValue { get; }

		protected abstract T DefaultMaxValue { get; }

		public abstract T MinValue { get; set; }

		public abstract T MaxValue { get; set; }

		protected abstract string GetClientValidationFunction();

		[DefaultValue(true)]
		public bool AllowNull
		{
			get { return (bool)this.ViewState.GetValue(AspireViewStateKeys.AllowNull, true); }
			set { this.ViewState.SetValue(AspireViewStateKeys.AllowNull, value); }
		}

		[DefaultValue(null)]
		public string RequiredErrorMessage
		{
			get { return (string)this.ViewState.GetValue<string>(AspireViewStateKeys.RequiredErrorMessage, null); }
			set { this.ViewState.SetValue(AspireViewStateKeys.RequiredErrorMessage, value); }
		}

		[DefaultValue(null)]
		public string InvalidNumberErrorMessage
		{
			get { return (string)this.ViewState.GetValue<string>(AspireViewStateKeys.InvalidNumberErrorMessage, null); }
			set { this.ViewState.SetValue(AspireViewStateKeys.InvalidNumberErrorMessage, value); }
		}

		[DefaultValue(null)]
		public string InvalidRangeErrorMessage
		{
			get { return (string)this.ViewState.GetValue<string>(AspireViewStateKeys.InvalidRangeErrorMessage, null); }
			set { this.ViewState.SetValue(AspireViewStateKeys.InvalidRangeErrorMessage, value); }
		}

		private string GetInvalidRangeErrorMessage()
		{
			if (string.IsNullOrEmpty(this.InvalidRangeErrorMessage))
				return this.RequiredErrorMessage;
			return this.InvalidRangeErrorMessage.Replace("{min}", this.MinValue.ToString(CultureInfo.CurrentCulture)).Replace("{max}", this.MaxValue.ToString(CultureInfo.CurrentCulture));

		}

		private static bool TryToNumber(string value, out T number)
		{
			IConvertible output;
			bool result;
			switch (default(T).GetTypeCode())
			{
				case TypeCode.Byte:
					{
						byte num;
						result = byte.TryParse(value, out num);
						output = num;
					}
					break;
				case TypeCode.Decimal:
					{
						decimal num;
						result = decimal.TryParse(value, out num);
						output = num;
					}
					break;
				case TypeCode.Double:
					{
						double num;
						result = double.TryParse(value, out num);
						output = num;
					}
					break;
				case TypeCode.Int16:
					{
						short num;
						result = short.TryParse(value, out num);
						output = num;
					}
					break;
				case TypeCode.Int32:
					{
						int num;
						result = int.TryParse(value, out num);
						output = num;
					}
					break;
				case TypeCode.Int64:
					{
						long num;
						result = long.TryParse(value, out num);
						output = num;
					}
					break;
				case TypeCode.SByte:
					{
						sbyte num;
						result = sbyte.TryParse(value, out num);
						output = num;
					}
					break;
				case TypeCode.Single:
					{
						float num;
						result = float.TryParse(value, out num);
						output = num;
					}
					break;
				case TypeCode.UInt16:
					{
						ushort num;
						result = ushort.TryParse(value, out num);
						output = num;
					}
					break;
				case TypeCode.UInt32:
					{
						uint num;
						result = uint.TryParse(value, out num);
						output = num;
					}
					break;
				case TypeCode.UInt64:
					{
						ulong num;
						result = ulong.TryParse(value, out num);
						output = num;
					}
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			number = (T)output;
			return result;
		}

		protected override bool OnServerValidate(string value)
		{
			if (string.IsNullOrWhiteSpace(value))
			{
				if (this.AllowNull)
					return true;
				else
				{
					this.ErrorMessage = this.RequiredErrorMessage;
					this.Text = this.ErrorMessage;
					return false;
				}
			}
			T number;
			if (!TryToNumber(value, out number))
			{
				this.ErrorMessage = this.InvalidNumberErrorMessage;
				this.Text = this.ErrorMessage;
				return false;
			}
			if (number.CompareTo(this.MinValue) < 0 || number.CompareTo(this.MaxValue) > 0)
			{
				this.ErrorMessage = this.GetInvalidRangeErrorMessage();
				this.Text = this.ErrorMessage;
				return false;
			}
			return true;
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.ControlToValidate))
				throw new InvalidOperationException("Provide ControlToValidate");
			this.Attributes.Add("data-RequiredErrorMessage".ToLower(), this.RequiredErrorMessage);
			this.Attributes.Add("data-InvalidNumberErrorMessage".ToLower(), this.InvalidNumberErrorMessage);
			this.Attributes.Add("data-InvalidRangeErrorMessage".ToLower(), this.GetInvalidRangeErrorMessage());
			this.Attributes.Add("data-AllowNull".ToLower(), this.AllowNull.ToString());
			this.Attributes.Add("data-MinValue".ToLower(), this.MinValue.ToString(CultureInfo.CurrentCulture));
			this.Attributes.Add("data-MaxValue".ToLower(), this.MaxValue.ToString(CultureInfo.CurrentCulture));
			this.Attributes.Add("data-type".ToLower(), typeof(T).Name);

			if (this.MinValue.CompareTo(this.MaxValue) > 0)
				throw new InvalidOperationException("MinValue cannot be greater than MaxValue.");
			this.ValidateEmptyText = true;
			this.ClientValidationFunction = this.GetClientValidationFunction();
			base.OnPreRender(e);
		}

		public override string ToString()
		{
			if (this.MinValue.CompareTo(this.MaxValue) > 0)
				return "MinValue should be less than MaxValue";
			var exp = string.Empty;
			if (!this.AllowNull)
			{
				if (string.IsNullOrWhiteSpace(this.RequiredErrorMessage))
					return "Provide RequiredErrorMessage";
				exp += this.RequiredErrorMessage + "/";
			}

			if (string.IsNullOrWhiteSpace(this.InvalidNumberErrorMessage))
				return "Provide InvalidNumberErrorMessage";
			exp += this.InvalidNumberErrorMessage + "/";

			if (this.MinValue.CompareTo(this.DefaultMinValue) != 0 || this.MaxValue.CompareTo(this.DefaultMaxValue) != 0)
			{
				if (string.IsNullOrWhiteSpace(this.InvalidRangeErrorMessage))
					exp += this.InvalidNumberErrorMessage + "/";
				else
					exp += this.InvalidRangeErrorMessage + "/";
			}
			return exp.Trim('/');
		}
	}
}