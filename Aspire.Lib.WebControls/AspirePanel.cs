using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	[PersistChildren(true)]
	[ParseChildren(false)]
	public class AspirePanel : WebControl
	{
		public enum PanelTypes
		{
			Default,
			Primary,
			Success,
			Info,
			Warning,
			Danger,
		}

		[DefaultValue("")]
		[Localizable(true)]
		public string HeaderText
		{
			get { return (string)this.ViewState.GetValue(AspireViewStateKeys.HeaderText, ""); }
			set { this.ViewState.SetValue(AspireViewStateKeys.HeaderText, value); }
		}

		[DefaultValue(PanelTypes.Default)]
		public PanelTypes PanelType
		{
			get { return (PanelTypes)this.ViewState.GetValue(AspireViewStateKeys.PanelTypes, PanelTypes.Default); }
			set { this.ViewState.SetValue(AspireViewStateKeys.PanelTypes, value); }
		}

		protected override void Render(HtmlTextWriter writer)
		{
			writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);
			switch (this.PanelType)
			{
				case PanelTypes.Default:
					writer.AddAttribute(HtmlTextWriterAttribute.Class, "panel panel-default");
					break;
				case PanelTypes.Primary:
					writer.AddAttribute(HtmlTextWriterAttribute.Class, "panel panel-primary");
					break;
				case PanelTypes.Success:
					writer.AddAttribute(HtmlTextWriterAttribute.Class, "panel panel-success");
					break;
				case PanelTypes.Info:
					writer.AddAttribute(HtmlTextWriterAttribute.Class, "panel panel-info");
					break;
				case PanelTypes.Warning:
					writer.AddAttribute(HtmlTextWriterAttribute.Class, "panel panel-warning");
					break;
				case PanelTypes.Danger:
					writer.AddAttribute(HtmlTextWriterAttribute.Class, "panel panel-danger");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			writer.RenderBeginTag(HtmlTextWriterTag.Div);
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Class, "panel-heading");
				writer.RenderBeginTag(HtmlTextWriterTag.Div);
				{
					writer.AddAttribute(HtmlTextWriterAttribute.Class, "panel-title");
					writer.RenderBeginTag(HtmlTextWriterTag.H3);
					writer.WriteEncodedText(this.HeaderText);
					writer.RenderEndTag();
				}
				writer.RenderEndTag();
				writer.AddAttribute(HtmlTextWriterAttribute.Class, "panel-body");
				writer.RenderBeginTag(HtmlTextWriterTag.Div);
				{
					base.Render(writer);
				}
				writer.RenderEndTag();
			}
			writer.RenderEndTag();
		}
	}
}