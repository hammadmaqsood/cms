﻿using System;
using System.ComponentModel;
using System.Web.UI;
using Aspire.Lib.Extensions;

namespace Aspire.Lib.WebControls
{
	[PersistChildren(false)]
	[ParseChildren(true)]
	public sealed class AspireModal : Control, IAttributeAccessor
	{
		public enum ModalSizes
		{
			None,
			Small,
			Large,
		}

		private System.Web.UI.AttributeCollection _attributes;
		private ITemplate _headerTemplate;
		private Control _headerTemplateContainer;
		private ITemplate _bodyTemplate;
		private Control _bodyTemplateContainer;
		private ITemplate _footerTemplate;
		private Control _footerTemplateContainer;

		[DefaultValue(null)]
		public string CssClass
		{
			get { return (string)this.ViewState.GetValue<string>(AspireViewStateKeys.CssClass, null); }
			set { this.ViewState.SetValue(AspireViewStateKeys.CssClass, value); }
		}

		[DefaultValue(ModalSizes.None)]
		public ModalSizes ModalSize
		{
			get { return (ModalSizes)this.ViewState.GetValue(AspireViewStateKeys.ModalSize, ModalSizes.None); }
			set { this.ViewState.SetValue(AspireViewStateKeys.ModalSize, value); }
		}

		[DefaultValue("")]
		public string HeaderText
		{
			get { return (string)this.ViewState.GetValue(AspireViewStateKeys.HeaderText, ""); }
			set { this.ViewState.SetValue(AspireViewStateKeys.HeaderText, value); }
		}

		[DefaultValue(true)]
		[Description("This property is only applicable when HeaderText is set.")]
		public bool CloseButtonVisible
		{
			get { return (bool)this.ViewState.GetValue(AspireViewStateKeys.CloseButtonVisible, true); }
			set { this.ViewState.SetValue(AspireViewStateKeys.CloseButtonVisible, value); }
		}

		[Browsable(false)]
		[TemplateInstance(TemplateInstance.Single)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public ITemplate HeaderTemplate
		{
			get { return this._headerTemplate; }
			set
			{
				if (!this.DesignMode && this._headerTemplate != null)
					throw new InvalidOperationException();
				this._headerTemplate = value;
				if (this._headerTemplate == null)
					return;
				this.CreateHeader();
			}
		}

		[Browsable(false)]
		[TemplateInstance(TemplateInstance.Single)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public ITemplate BodyTemplate
		{
			get { return this._bodyTemplate; }
			set
			{
				if (!this.DesignMode && this._bodyTemplate != null)
					throw new InvalidOperationException();
				this._bodyTemplate = value;
				if (this._bodyTemplate == null)
					return;
				this.CreateBody();
			}
		}

		[Browsable(false)]
		[TemplateInstance(TemplateInstance.Single)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public ITemplate FooterTemplate
		{
			get { return this._footerTemplate; }
			set
			{
				if (!this.DesignMode && this._footerTemplate != null)
					throw new InvalidOperationException();
				this._footerTemplate = value;
				if (this._footerTemplate == null)
					return;
				this.CreateFooter();
			}
		}

		[Browsable(false)]
		private Control HeaderTemplateContainer
		{
			get
			{
				if (this._headerTemplateContainer == null)
				{
					this._headerTemplateContainer = CreateHeaderTemplateContainer();
					this.AddHeaderTemplateContainer();
				}
				return this._headerTemplateContainer;
			}
		}

		[Browsable(false)]
		private Control BodyTemplateContainer
		{
			get
			{
				if (this._bodyTemplateContainer == null)
				{
					this._bodyTemplateContainer = CreateBodyTemplateContainer();
					this.AddBodyTemplateContainer();
				}
				return this._bodyTemplateContainer;
			}
		}

		[Browsable(false)]
		private Control FooterTemplateContainer
		{
			get
			{
				if (this._footerTemplateContainer == null)
				{
					this._footerTemplateContainer = CreateFooterTemplateContainer();
					this.AddFooterTemplateContainer();
				}
				return this._footerTemplateContainer;
			}
		}

		private ModalChildsControlCollection ChildControls => this.Controls as ModalChildsControlCollection;

		private void ClearHeader()
		{
			this.HeaderTemplateContainer.Controls.Clear();
			this._headerTemplateContainer = null;
			this.ChildControls.RemoveHeader();
		}

		private void ClearBody()
		{
			this.BodyTemplateContainer.Controls.Clear();
			this._bodyTemplateContainer = null;
			this.ChildControls.RemoveBody();
		}

		private void ClearFooter()
		{
			this.FooterTemplateContainer.Controls.Clear();
			this._footerTemplateContainer = null;
			this.ChildControls.RemoveFooter();
		}

		private static Control CreateHeaderTemplateContainer()
		{
			return new Control();
		}

		private static Control CreateBodyTemplateContainer()
		{
			return new Control();
		}

		private static Control CreateFooterTemplateContainer()
		{
			return new Control();
		}

		protected override ControlCollection CreateControlCollection()
		{
			return new ModalChildsControlCollection(this);
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			if (this._headerTemplateContainer == null)
			{
				this._headerTemplateContainer = CreateHeaderTemplateContainer();
				this.AddHeaderTemplateContainer();
			}
			if (this._bodyTemplateContainer == null)
			{
				this._bodyTemplateContainer = CreateBodyTemplateContainer();
				this.AddBodyTemplateContainer();
			}
			if (this._footerTemplateContainer == null)
			{
				this._footerTemplateContainer = CreateFooterTemplateContainer();
				this.AddFooterTemplateContainer();
			}
		}

		private void RenderHeader(HtmlTextWriter writer)
		{
			var header = this.ChildControls.Header;
			if (header == null || header.Controls.Count == 0)
			{
				if (string.IsNullOrEmpty(this.HeaderText))
					return;
				writer.AddAttribute(HtmlTextWriterAttribute.Class, "modal-header");
				writer.RenderBeginTag(HtmlTextWriterTag.Div);
				{
					if (this.CloseButtonVisible)
					{
						writer.AddAttribute(HtmlTextWriterAttribute.Type, "button");
						writer.AddAttribute(HtmlTextWriterAttribute.Class, "close");
						writer.AddAttribute("data-dismiss", "modal");
						writer.AddAttribute("aria-label", "close");
						writer.RenderBeginTag(HtmlTextWriterTag.Button);
						{
							writer.AddAttribute("aria-hidden", "true");
							writer.RenderBeginTag(HtmlTextWriterTag.Span);
							writer.Write("&times;");
							writer.RenderEndTag();
						}
						writer.RenderEndTag();
					}
					writer.AddAttribute(HtmlTextWriterAttribute.Class, "modal-title");
					writer.RenderBeginTag(HtmlTextWriterTag.H4);
					writer.WriteEncodedText(this.HeaderText);
					writer.RenderEndTag();
				}
				writer.RenderEndTag();
			}
			else
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Class, "modal-header");
				writer.RenderBeginTag(HtmlTextWriterTag.Div);
				header.RenderControl(writer);
				writer.RenderEndTag();
			}
		}

		private void RenderBody(HtmlTextWriter writer)
		{
			var body = this.ChildControls.Body;
			if (body == null || body.Controls.Count == 0)
				return;
			writer.AddAttribute(HtmlTextWriterAttribute.Class, "modal-body");
			writer.RenderBeginTag(HtmlTextWriterTag.Div);
			body.RenderControl(writer);
			writer.RenderEndTag();
		}

		private void RenderFooter(HtmlTextWriter writer)
		{
			var footer = this.ChildControls.Footer;
			if (footer == null || footer.Controls.Count == 0)
				return;
			writer.AddAttribute(HtmlTextWriterAttribute.Class, "modal-footer");
			writer.RenderBeginTag(HtmlTextWriterTag.Div);
			footer.RenderControl(writer);
			writer.RenderEndTag();
		}

		private void AddHeaderTemplateContainer()
		{
			this.ChildControls.AddHeader(this._headerTemplateContainer);
		}

		private void AddBodyTemplateContainer()
		{
			this.ChildControls.AddBody(this._bodyTemplateContainer);
		}

		private void AddFooterTemplateContainer()
		{
			this.ChildControls.AddFooter(this._footerTemplateContainer);
		}

		private void CreateHeader()
		{
			if (this.DesignMode)
				this.ClearHeader();
			if (this._headerTemplateContainer == null)
			{
				this._headerTemplateContainer = CreateHeaderTemplateContainer();
				this._headerTemplate?.InstantiateIn(this._headerTemplateContainer);
				this.AddHeaderTemplateContainer();
			}
			else
			{
				if (this._headerTemplate == null)
					return;
				this._headerTemplate.InstantiateIn(this._headerTemplateContainer);
			}
		}

		private void CreateBody()
		{
			if (this.DesignMode)
				this.ClearBody();
			if (this._bodyTemplateContainer == null)
			{
				this._bodyTemplateContainer = CreateBodyTemplateContainer();
				this._bodyTemplate?.InstantiateIn(this._bodyTemplateContainer);
				this.AddBodyTemplateContainer();
			}
			else
			{
				if (this._bodyTemplate == null)
					return;
				this._bodyTemplate.InstantiateIn(this._bodyTemplateContainer);
			}
		}

		private void CreateFooter()
		{
			if (this.DesignMode)
				this.ClearFooter();
			if (this._footerTemplateContainer == null)
			{
				this._footerTemplateContainer = CreateFooterTemplateContainer();
				this._footerTemplate?.InstantiateIn(this._footerTemplateContainer);
				this.AddFooterTemplateContainer();
			}
			else
			{
				if (this._footerTemplate == null)
					return;
				this._footerTemplate.InstantiateIn(this._footerTemplateContainer);
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		private System.Web.UI.AttributeCollection Attributes => this._attributes ?? (this._attributes = new System.Web.UI.AttributeCollection(new StateBag(true)));

		string IAttributeAccessor.GetAttribute(string key)
		{
			return this._attributes?[key];
		}

		void IAttributeAccessor.SetAttribute(string key, string value)
		{
			this.Attributes[key] = value;
		}

		protected override void RenderChildren(HtmlTextWriter writer)
		{
			writer.AddAttribute(HtmlTextWriterAttribute.Class, ("modal fade AspireModal " + this.CssClass.TrimAndMakeItNullIfEmpty()).Trim());
			writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);
			this._attributes?.AddAttributes(writer);
			writer.RenderBeginTag(HtmlTextWriterTag.Div);
			{
				switch (this.ModalSize)
				{
					case ModalSizes.None:
						writer.AddAttribute(HtmlTextWriterAttribute.Class, "modal-dialog");
						break;
					case ModalSizes.Small:
						writer.AddAttribute(HtmlTextWriterAttribute.Class, "modal-dialog modal-sm");
						break;
					case ModalSizes.Large:
						writer.AddAttribute(HtmlTextWriterAttribute.Class, "modal-dialog modal-lg");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				writer.RenderBeginTag(HtmlTextWriterTag.Div);
				{
					writer.AddAttribute(HtmlTextWriterAttribute.Class, "modal-content");
					writer.RenderBeginTag(HtmlTextWriterTag.Div);
					{
						this.RenderHeader(writer);
						this.RenderBody(writer);
						this.RenderFooter(writer);
					}
					writer.RenderEndTag();
				}
				writer.RenderEndTag();
			}
			writer.RenderEndTag();
		}

		private bool? _shown;
		public void Show()
		{
			this._shown = true;
		}

		public void Hide()
		{
			this._shown = false;
		}

		public static void CloseAll(object sender)
		{
			ScriptManager.RegisterClientScriptBlock((sender as Control), sender.GetType(), "AspireModalCloseAll", "AspireModal.closeAll();", true);
		}

		public static void Close(AspireModal modalToClose)
		{
			ScriptManager.RegisterClientScriptBlock(modalToClose.Page, modalToClose.Page.GetType(), "AspireModalClose", $"AspireModal.close('#{modalToClose.ClientID}');", true);
		}

		private void ChangeVisibility(bool show)
		{
			var script = this.GetClientScript(show);
			ScriptManager.RegisterStartupScript(this, this.GetType(), this.ClientID, script, true);
		}

		private string GetClientScript(bool show)
		{
			return show
				? $@"var fnShow{this.ClientID}=function(){{
$(function(){{AspireModal.open($(""#{this.ClientID}""),""static"",false,false);}});
setTimeout(function(){{Sys.Application.remove_load(fnShow{this.ClientID});}},1);
}}; Sys.Application.add_load(fnShow{this.ClientID});"
				: $@"var fnHide{this.ClientID}=function(){{
$(function(){{AspireModal.close($(""#{this.ClientID}""),false);}});
setTimeout(function(){{Sys.Application.remove_load(fnHide{this.ClientID});}},1);
}}; Sys.Application.add_load(fnHide{this.ClientID});";
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (this._shown != null)
				this.ChangeVisibility(this._shown.Value);
			base.OnPreRender(e);
		}

		private sealed class ModalChildsControlCollection : ControlCollection
		{
			public Control Header { get; private set; }
			public Control Body { get; private set; }
			public Control Footer { get; private set; }

			public ModalChildsControlCollection(Control owner)
				: base(owner)
			{
			}

			public override void Add(Control child)
			{
				throw new InvalidOperationException();
			}

			public override void AddAt(int index, Control child)
			{
				throw new InvalidOperationException();
			}

			public override void Clear()
			{
				throw new InvalidOperationException();
			}

			public void AddHeader(Control header)
			{
				base.Add(header);
				this.Header = header;
			}

			public void AddBody(Control body)
			{
				base.Add(body);
				this.Body = body;
			}

			public void AddFooter(Control footer)
			{
				base.Add(footer);
				this.Footer = footer;
			}

			public void RemoveHeader()
			{
				if (this.Header != null)
				{
					base.Remove(this.Header);
					this.Header = null;
				}
			}

			public void RemoveBody()
			{
				if (this.Body != null)
				{
					base.Remove(this.Body);
					this.Body = null;
				}
			}

			public void RemoveFooter()
			{
				if (this.Footer != null)
				{
					base.Remove(this.Footer);
					this.Footer = null;
				}
			}

			public override void Remove(Control value)
			{
				throw new InvalidOperationException();
			}

			public override void RemoveAt(int index)
			{
				throw new InvalidOperationException();
			}
		}
	}
}
