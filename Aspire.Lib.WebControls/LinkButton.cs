﻿using Aspire.Lib.Helpers;
using System.ComponentModel;
using System.Web.UI;

namespace Aspire.Lib.WebControls
{
	public class LinkButton : System.Web.UI.WebControls.LinkButton
	{
		public string EncryptedCommandArgument
		{
			get { return this.CommandArgument.Decrypt(); }
			set { this.CommandArgument = value.Encrypt(); }
		}

		[Localizable(true), DefaultValue("Confirm")]
		public string ConfirmMessageTitle
		{
			get => (string) this.ViewState.GetValue(AspireViewStateKeys.ConfirmMessageTitle, "Confirm");
			set => this.ViewState.SetValue(AspireViewStateKeys.ConfirmMessageTitle, value);
		}

		[Localizable(true), DefaultValue("")]
		public string ConfirmMessage
		{
			get => (string) this.ViewState.GetValue(AspireViewStateKeys.ConfirmMessage, string.Empty);
			set => this.ViewState.SetValue(AspireViewStateKeys.ConfirmMessage, value);
		}

		[Localizable(true), DefaultValue(true)]
		public bool DisableOnClick
		{
			get => (bool) this.ViewState.GetValue(AspireViewStateKeys.DisableOnClick, true);
			set => this.ViewState.SetValue(AspireViewStateKeys.DisableOnClick, value);
		}

		protected override void Render(HtmlTextWriter writer)
		{
			this.Attributes.Add("data-" + nameof(this.Enabled), this.Enabled.ToString());
			this.Attributes.Add("data-" + nameof(this.ConfirmMessageTitle), this.ConfirmMessageTitle);
			this.Attributes.Add("data-" + nameof(this.ConfirmMessage), this.ConfirmMessage);
			this.Attributes.Add("data-" + nameof(this.DisableOnClick), this.DisableOnClick.ToString());
			this.Attributes.Add("data-" + nameof(this.OnClientClick), this.OnClientClick);
			this.Attributes.Add("data-" + nameof(this.ValidationGroup), this.ValidationGroup);
			this.Attributes.Add("data-" + nameof(this.CausesValidation), this.CausesValidation.ToString());
			this.OnClientClick = "return ButtonClick(this);";
			base.Render(writer);
		}
	}
}