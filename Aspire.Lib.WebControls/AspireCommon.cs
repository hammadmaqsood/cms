﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	public static class AspireCommon
	{
		public static string ToCssClass(this AspireGlyphicons glyphicon)
		{
			return "glyphicon glyphicon-" + glyphicon.ToString().ToLower().Replace("_", "-").Trim('-');
		}

		public static string ToCssClass(this FontAwesomeIcons fontAwesomeIcon)
		{
			var iconName = fontAwesomeIcon.ToString();
			switch (iconName[0])
			{
				case 's':
					return "fas fa-" + iconName.Substring(6).Replace("_", "-");
				case 'r':
					return "far fa-" + iconName.Substring(8).Replace("_", "-");
				case 'l':
					return "fal fa-" + iconName.Substring(6).Replace("_", "-");
				case 'b':
					return "fab fa-" + iconName.Substring(6).Replace("_", "-");
				default:
					throw new ArgumentOutOfRangeException($"{nameof(FontAwesomeIcons)}: {fontAwesomeIcon}");
			}
		}

		private static string GetKey(this AspireViewStateKeys key)
		{
			return "" + (byte)key;
		}

		internal static object GetValue(this StateBag viewState, AspireViewStateKeys aspireViewStateKey, object defaultValue)
		{
			var key = aspireViewStateKey.GetKey();
			if (viewState[key] == null)
				return defaultValue;
			return viewState[key];
		}

		internal static T GetValue<T>(this StateBag viewState, AspireViewStateKeys aspireViewStateKey, T defaultValue)
		{
			var key = aspireViewStateKey.GetKey();
			if (viewState[key] == null)
				return defaultValue;
			return (T)viewState[key];
		}

		internal static void SetValue(this StateBag viewState, AspireViewStateKeys aspireViewStateKey, object value)
		{
			viewState[aspireViewStateKey.GetKey()] = value;
		}

		private static IEnumerable<string> SplitCssClasses(this string cssClasses)
			=> (IEnumerable<string>)cssClasses?.Split(' ') ?? new List<string>();

		private static string JoinCssClasses(this IEnumerable<string> cssClasses)
			=> string.Join(" ", cssClasses.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct()).Trim();

		private static string RepairCssClasses(this string cssClasses)
		{
			return cssClasses.SplitCssClasses().JoinCssClasses();
		}

		private static string JoinCssClasses(this string[] newCssClasses)
			=> string.Join(" ", newCssClasses);

		private static string AddCssClasses(this string cssClasses, params string[] newCssClasses)
			=> $"{cssClasses} {newCssClasses.JoinCssClasses()}".RepairCssClasses();

		private static string RemoveCssClasses(this string cssClasses, params string[] cssClassesToBeRemoved)
		{
			var toBeRemovedCssClasses = cssClassesToBeRemoved.JoinCssClasses().SplitCssClasses();
			return cssClasses.SplitCssClasses().Where(c => !toBeRemovedCssClasses.Contains(c)).JoinCssClasses();
		}

		internal static void AddCssClasses(this WebControl control, params string[] cssClasses)
		{
			control.CssClass = control.CssClass.AddCssClasses(cssClasses);
		}

		internal static void AddCssClass(this WebControl control, string cssClass)
		{
			control.AddCssClasses(cssClass);
		}

		internal static void RemoveCssClass(this WebControl control, string cssClass)
		{
			control.CssClass = control.CssClass.RemoveCssClasses(cssClass);
		}

		internal static string JavaScriptStringEncode(this string str)
		{
			return HttpUtility.JavaScriptStringEncode(str);
		}

		internal static string JavaScriptStringEncode(this string str, bool addDoubleQuotes)
		{
			return HttpUtility.JavaScriptStringEncode(str, addDoubleQuotes);
		}
	}
}