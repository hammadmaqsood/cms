﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	public sealed class AspireAlert : WebControl, IAlert
	{
		[DefaultValue(true)]
		public bool CloseButtonVisible
		{
			get { return (bool)this.ViewState.GetValue(AspireViewStateKeys.CloseButtonVisible, true); }
			set { this.ViewState.SetValue(AspireViewStateKeys.CloseButtonVisible, value); }
		}

		public enum AlertTypes : byte
		{
			Error = 0,
			Info = 1,
			Success = 2,
			Warning = 3,
		}

		private struct AlertMessage
		{
			public AlertTypes AlertType;
			public string Message;
			public bool HtmlEncoded;

			public string Html => this.HtmlEncoded ? this.Message : HttpUtility.HtmlEncode(this.Message);
		}

		private readonly List<AlertMessage> _messages = new List<AlertMessage>();

		public void AddMessage(AlertTypes alertType, string message, bool isMessageHtmlEncoded = false)
		{
			this._messages.Add(new AlertMessage { AlertType = alertType, Message = message, HtmlEncoded = isMessageHtmlEncoded });
		}

		protected override void OnInit(EventArgs e)
		{
			this.EnableViewState = false;
			base.OnInit(e);
		}

		private void RenderAlerts(HtmlTextWriter writer, AlertTypes alertType)
		{
			var alertMessages = this._messages.Where(m => m.AlertType == alertType).ToList();
			var cssClass = "alert ";
			switch (alertType)
			{
				case AlertTypes.Success:
					cssClass += "alert-success";
					break;
				case AlertTypes.Error:
					cssClass += "alert-danger";
					break;
				case AlertTypes.Info:
					cssClass += "alert-info";
					break;
				case AlertTypes.Warning:
					cssClass += "alert-warning";
					break;
				default: throw new ArgumentOutOfRangeException();
			}
			cssClass += " alert-dismissible";
			writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);
			writer.AddAttribute("role", "alert");
			writer.RenderBeginTag(HtmlTextWriterTag.Div);
			if (this.CloseButtonVisible)
				writer.Write("<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>");
			writer.RenderBeginTag(HtmlTextWriterTag.Strong);
			writer.Write(alertType + "!");
			writer.RenderEndTag();
			writer.Write(" ");
			if (alertMessages.Count() == 1)
				writer.Write(alertMessages.Single().Html);
			else
			{
				writer.RenderBeginTag(HtmlTextWriterTag.Ul);
				foreach (var message in alertMessages)
				{
					writer.RenderBeginTag(HtmlTextWriterTag.Li);
					writer.Write(message.Html);
					writer.RenderEndTag();
				}
				writer.RenderEndTag();
			}
			writer.RenderEndTag();
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (!this._messages.Any())
				return;
			foreach (var alertType in this._messages.Select(m => m.AlertType).Distinct().ToList())
				this.RenderAlerts(writer, alertType);
		}
	}
}
