using Aspire.Lib.Extensions;
using System;
using System.ComponentModel;
using System.Web.UI;

namespace Aspire.Lib.WebControls
{
	public class AspireHyperLinkButton : Aspire.Lib.WebControls.HyperLink
	{
		public enum ButtonTypes
		{
			Default,
			Primary,
			Success,
			Danger,
			OnlyIcon,
		}

		[DefaultValue(ButtonTypes.Default)]
		public ButtonTypes ButtonType
		{
			get { return (ButtonTypes)this.ViewState.GetValue(AspireViewStateKeys.ButtonType, ButtonTypes.Default); }
			set { this.ViewState.SetValue(AspireViewStateKeys.ButtonType, value); }
		}

		[DefaultValue(AspireGlyphicons.None)]
		public AspireGlyphicons Glyphicon
		{
			get { return (AspireGlyphicons)this.ViewState.GetValue(AspireViewStateKeys.Glyphicon, AspireGlyphicons.None); }
			set { this.ViewState.SetValue(AspireViewStateKeys.Glyphicon, value); }
		}

		[DefaultValue(FontAwesomeIcons.None)]
		public FontAwesomeIcons FontAwesomeIcon
		{
			get { return (FontAwesomeIcons)this.ViewState.GetValue(AspireViewStateKeys.FontAwesomeIcon, FontAwesomeIcons.None); }
			set { this.ViewState.SetValue(AspireViewStateKeys.FontAwesomeIcon, value); }
		}

		protected override void CreateChildControls()
		{
			var hasText = !string.IsNullOrEmpty(this.Text);
			if (this.Glyphicon != AspireGlyphicons.None)
			{
				this.Controls.Add(new AspireGlyphicon { Icon = this.Glyphicon });
				if (hasText)
					this.Controls.Add(new LiteralControl(" "));
			}
			if (this.FontAwesomeIcon != FontAwesomeIcons.None)
			{
				this.Controls.Add(new AspireFontAwesomeIcon { Icon = this.FontAwesomeIcon });
				if (hasText)
					this.Controls.Add(new LiteralControl(" "));
			}
			if (hasText)
				this.Controls.Add(new LiteralControl(this.Text.HtmlEncode()));
			base.CreateChildControls();
		}

		protected override void Render(HtmlTextWriter writer)
		{
			switch (this.ButtonType)
			{
				case ButtonTypes.Default:
					this.AddCssClass("btn btn-default");
					break;
				case ButtonTypes.Primary:
					this.AddCssClass("btn btn-primary");
					break;
				case ButtonTypes.Success:
					this.AddCssClass("btn btn-success");
					break;
				case ButtonTypes.Danger:
					this.AddCssClass("btn btn-danger");
					break;
				case ButtonTypes.OnlyIcon:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			base.Render(writer);
		}
	}
}