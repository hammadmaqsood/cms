using System;
using System.ComponentModel;

namespace Aspire.Lib.WebControls
{
	public class AspireCheckBox : Aspire.Lib.WebControls.CheckBox
	{
		[DefaultValue(false)]
		public bool Inline
		{
			get { return (bool)this.ViewState.GetValue(AspireViewStateKeys.Inline, false); }
			set { this.ViewState.SetValue(AspireViewStateKeys.Inline, value); }
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (this.Inline)
				this.AddCssClass("Inline");
			else
				this.RemoveCssClass("Inline");
			this.AddCssClass("AspireCheckBox");
			base.OnPreRender(e);
		}
	}
}