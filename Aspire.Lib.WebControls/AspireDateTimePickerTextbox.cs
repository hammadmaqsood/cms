﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Web.UI;

namespace Aspire.Lib.WebControls
{
	public class AspireDateTimePickerTextbox : AspireTextBox
	{
		public enum DisplayModes : byte
		{
			LongDateLongTime,
			LongDateShortTime,
			LongDate,
			LongTime,
			ShortDate,
			ShortTime,
			ShortDateTime,
		}

		public enum HorizontalPositions
		{
			Auto,
			Left,
			Right
		}

		public enum VerticalPositions
		{
			Auto,
			Top,
			Bottom
		}

		internal string DateFormat
		{
			get
			{
				var cultureInfo = CultureInfo.CurrentCulture;
				switch (this.DisplayMode)
				{
					case DisplayModes.LongDateLongTime:
						return $"{cultureInfo.DateTimeFormat.LongDatePattern} {cultureInfo.DateTimeFormat.LongTimePattern}";
					case DisplayModes.LongDateShortTime:
						return $"{cultureInfo.DateTimeFormat.LongDatePattern} {cultureInfo.DateTimeFormat.ShortTimePattern}";
					case DisplayModes.LongDate:
						return $"{cultureInfo.DateTimeFormat.LongDatePattern}";
					case DisplayModes.LongTime:
						return $"{cultureInfo.DateTimeFormat.LongTimePattern}";
					case DisplayModes.ShortDate:
						return $"{cultureInfo.DateTimeFormat.ShortDatePattern}";
					case DisplayModes.ShortTime:
						return $"{cultureInfo.DateTimeFormat.ShortTimePattern}";
					case DisplayModes.ShortDateTime:
						return $"{cultureInfo.DateTimeFormat.ShortDatePattern} {cultureInfo.DateTimeFormat.ShortTimePattern}";
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		[DefaultValue(DisplayModes.ShortDate)]
		public DisplayModes DisplayMode
		{
			get { return (DisplayModes)this.ViewState.GetValue(AspireViewStateKeys.DisplayMode, DisplayModes.ShortDate); }
			set { this.ViewState.SetValue(AspireViewStateKeys.DisplayMode, value); }
		}

		[DefaultValue(true)]
		public bool UserManualEntryAllowed
		{
			get { return (bool)this.ViewState.GetValue(AspireViewStateKeys.UserManualEntryAllowed, true); }
			set { this.ViewState.SetValue(AspireViewStateKeys.UserManualEntryAllowed, value); }
		}

		[DefaultValue(true)]
		public bool SideBySide
		{
			get { return (bool)this.ViewState.GetValue(AspireViewStateKeys.SideBySide, true); }
			set { this.ViewState.SetValue(AspireViewStateKeys.SideBySide, value); }
		}

		[DefaultValue(false)]
		public bool ShowClose
		{
			get { return (bool)this.ViewState.GetValue(AspireViewStateKeys.ShowClose, false); }
			set { this.ViewState.SetValue(AspireViewStateKeys.ShowClose, value); }
		}

		[DefaultValue(true)]
		public bool ShowClear
		{
			get { return (bool)this.ViewState.GetValue(AspireViewStateKeys.ShowClear, false); }
			set { this.ViewState.SetValue(AspireViewStateKeys.ShowClear, value); }
		}

		[DefaultValue(HorizontalPositions.Auto)]
		public HorizontalPositions HorizontalPosition
		{
			get { return (HorizontalPositions)this.ViewState.GetValue(AspireViewStateKeys.HorizontalPosition, HorizontalPositions.Auto); }
			set { this.ViewState.SetValue(AspireViewStateKeys.HorizontalPosition, value); }
		}

		[DefaultValue(VerticalPositions.Auto)]
		public VerticalPositions VerticalPosition
		{
			get { return (VerticalPositions)this.ViewState.GetValue(AspireViewStateKeys.VerticalPosition, VerticalPositions.Auto); }
			set { this.ViewState.SetValue(AspireViewStateKeys.VerticalPosition, value); }
		}

		[DefaultValue(null)]
		public DateTime? MinDate
		{
			get { return (DateTime?)this.ViewState.GetValue<DateTime?>(AspireViewStateKeys.MinDate, null); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MinDate, value); }
		}

		[DefaultValue(null)]
		public DateTime? MaxDate
		{
			get { return (DateTime?)this.ViewState.GetValue<DateTime?>(AspireViewStateKeys.MaxDate, null); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MaxDate, value); }
		}

		public DateTime? SelectedDate
		{
			get
			{
				if (string.IsNullOrWhiteSpace(this.Text) || this.Text == "dd/mm/yyyy")
					return null;
				return Convert.ToDateTime(this.Text);
			}
			set { this.Text = value?.ToString(this.DateFormat) ?? string.Empty; }
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (this.ReadOnly || !this.Enabled)
			{
				base.Render(writer);
				return;
			}
			switch (this.DisplayMode)
			{
				case DisplayModes.ShortDate:
					this.Attributes.Add("data-inputmask", "'alias': 'dd/mm/yyyy', 'clearIncomplete': true");
					break;
				case DisplayModes.LongDateLongTime:
					break;
				case DisplayModes.LongDateShortTime:
					break;
				case DisplayModes.LongDate:
					break;
				case DisplayModes.LongTime:
					break;
				case DisplayModes.ShortTime:
					this.Attributes.Add("data-inputmask", "'alias': 'h:mm t', 'clearIncomplete': true");
					break;
				case DisplayModes.ShortDateTime:
					this.Attributes.Add("data-inputmask", "'alias': 'dd/mm/yyyy h:mm t', 'clearIncomplete': true");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.TrimText = true;
			this.MaxLength = 100;
			/*
<div id="BodyPH_tbDOB" data-displaymode="Date" class="input-group date DatePickerTextbox">
	<input name="ctl00$BodyPH$tbDOB$tb" type="text" maxlength="11" id="BodyPH_tbDOB_tb" class="form-control" onblur="this.value=this.value.trim();">
	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
</div>*/

			writer.AddAttribute("data-DisplayMode", this.DisplayMode.ToString());
			if (this.ReadOnly)
				this.UserManualEntryAllowed = false;
			writer.AddAttribute("data-UserManualEntryAllowed", this.UserManualEntryAllowed.ToString());
			writer.AddAttribute("data-HorizontalPosition", this.HorizontalPosition.ToString());
			writer.AddAttribute("data-VerticalPosition", this.VerticalPosition.ToString());
			writer.AddAttribute("data-ShowClose", this.ShowClose.ToString());
			writer.AddAttribute("data-ShowClear", this.ShowClear.ToString());
			writer.AddAttribute("data-SideBySide", this.SideBySide.ToString());
			writer.AddAttribute("data-MinDate", this.MinDate?.ToString("d"));
			writer.AddAttribute("data-MaxDate", this.MaxDate?.ToString("d"));
			writer.AddAttribute(HtmlTextWriterAttribute.Class, "input-group date DatePickerTextbox");
			writer.RenderBeginTag(HtmlTextWriterTag.Div);
			{
				base.Render(writer);
				writer.AddAttribute(HtmlTextWriterAttribute.Class, "input-group-addon");
				writer.RenderBeginTag(HtmlTextWriterTag.Span);
				{
					writer.AddAttribute(HtmlTextWriterAttribute.Class, "glyphicon glyphicon-calendar");
					writer.RenderBeginTag(HtmlTextWriterTag.Span);
					writer.RenderEndTag();
				}
				writer.RenderEndTag();
			}
			writer.RenderEndTag();
		}
	}
}