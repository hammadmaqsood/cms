﻿using Aspire.Lib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Lib.WebControls
{
	[Serializable]
	public sealed class AspireSideBarLink
	{
		public Guid? AspirePageGuid { get; }
		public string Text { get; }
		public string NavigateUrl { get; }
		public bool Visible { get; }
		public string CssClass { get; }
		public string Target { get; }
		public byte DisplayIndex { get; private set; }
		public string FullUrl => this.NavigateUrl.ToAbsoluteUrl();
		public AspireSideBarLink ParentAspireSideBarLink { get; private set; }
		private readonly List<AspireSideBarLink> _subLinks = new List<AspireSideBarLink>();
		public IReadOnlyList<AspireSideBarLink> SubLinks => this._subLinks;

		public AspireSideBarLink(AspireSideBarLink parent, Guid? aspirePageGuid, string text, string navigateUrl, bool visible, string target, string cssClass)
		{
			if (string.IsNullOrWhiteSpace(text))
				throw new ArgumentNullException(nameof(text));
			this.ParentAspireSideBarLink = parent;
			this.AspirePageGuid = aspirePageGuid;
			this.Text = text;
			this.NavigateUrl = navigateUrl;
			this.Visible = visible;
			this.CssClass = cssClass;
			this.Target = target;
		}

		public string ToHtml(string parentID, int index)
		{
			if (this.Visible == false)
				return null;
			var subLinks = this.SubLinks.Where(l => l.Visible).ToList();
			if (!subLinks.Any())
				return $@"<a class=""list-group-item {this.CssClass}"" data-parent=""#{parentID.HtmlAttributeEncode()}"" target=""{this.Target ?? "_self"}"" href=""{this.FullUrl.HtmlAttributeEncode()}"">{this.Text.HtmlEncode()}</a>";
			var subMenuDivID = parentID + "_" + index;
			var html = $@"<a class=""list-group-item collapsed {this.CssClass}"" data-parent=""#{parentID.HtmlAttributeEncode()}"" data-toggle=""collapse"" href=""#{subMenuDivID.HtmlAttributeEncode()}"">{this.Text.HtmlEncode()}<i class=""fa fa-caret-down pull-right""></i><i class=""fa fa-caret-right""></i></a>";
			html += $@"<div class=""collapse list-group-subMenu {this.CssClass}"" id=""{subMenuDivID.HtmlAttributeEncode()}"">";
			var i = 0;
			foreach (var aspireSideBarLink in subLinks)
				html += aspireSideBarLink.ToHtml(subMenuDivID, i++);
			return html + "</div>";
		}

		public AspireSideBarLink AddSubLink(AspireSideBarLink sideBarLink)
		{
			this._subLinks.Add(sideBarLink);
			return sideBarLink;
		}

		public AspireSideBarLink AddSubLink(AspireSideBarLink parent, Guid? aspirePageGuid, string text, string navigateUrl, bool visible, string target, string cssClass)
		{
			return this.AddSubLink(new AspireSideBarLink(parent, aspirePageGuid, text, navigateUrl, visible, target, cssClass));
		}

		public AspireSideBarLink FindSidebarLink(Guid aspirePageGuid)
		{
			if (this.AspirePageGuid == aspirePageGuid)
				return this;
			foreach (var aspireSideBarLink in this.SubLinks)
			{
				var link = aspireSideBarLink.FindSidebarLink(aspirePageGuid);
				if (link != null)
					return link;
			}
			return null;
		}

		public void SetDisplayIndexes(byte displayIndex)
		{
			this.DisplayIndex = displayIndex;
			displayIndex = 0;
			foreach (var aspireSideBarLink in this.SubLinks)
				aspireSideBarLink.SetDisplayIndexes(displayIndex++);
		}

		public void AddSubLinks(List<AspireSideBarLink> childSideBarLinks)
		{
			foreach (var aspireSideBarLink in childSideBarLinks)
				this.AddSubLink(aspireSideBarLink);
		}
	}
}
