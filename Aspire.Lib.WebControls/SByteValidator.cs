﻿using System.ComponentModel;

namespace Aspire.Lib.WebControls
{
	public class SByteValidator : NumberValidator<sbyte>
	{
		protected override sbyte DefaultMaxValue => sbyte.MaxValue;

		protected override sbyte DefaultMinValue => sbyte.MinValue;

		protected override string GetClientValidationFunction()
		{
			return "ValidateInteger";
		}

		[DefaultValue(sbyte.MaxValue)]
		public override sbyte MaxValue
		{
			get { return (sbyte)this.ViewState.GetValue(AspireViewStateKeys.MaxValue, DefaultMaxValue); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MaxValue, value); }
		}

		[DefaultValue(sbyte.MinValue)]
		public override sbyte MinValue
		{
			get { return (sbyte)this.ViewState.GetValue(AspireViewStateKeys.MinValue, DefaultMinValue); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MinValue, value); }
		}
	}
}