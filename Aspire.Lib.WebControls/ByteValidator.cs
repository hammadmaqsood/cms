﻿using System.ComponentModel;

namespace Aspire.Lib.WebControls
{
	public class ByteValidator : NumberValidator<byte>
	{
		protected override byte DefaultMaxValue => byte.MaxValue;

		protected override byte DefaultMinValue => byte.MinValue;

		protected override string GetClientValidationFunction()
		{
			return "ValidateInteger";
		}

		[DefaultValue(byte.MaxValue)]
		public override byte MaxValue
		{
			get { return (byte)this.ViewState.GetValue(AspireViewStateKeys.MaxValue, DefaultMaxValue); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MaxValue, value); }
		}

		[DefaultValue(byte.MinValue)]
		public override byte MinValue
		{
			get { return (byte)this.ViewState.GetValue(AspireViewStateKeys.MinValue, DefaultMinValue); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MinValue, value); }
		}
	}
}