namespace Aspire.Lib.WebControls
{
	public class GridView : System.Web.UI.WebControls.GridView
	{
		public void DataBind(object dataSource)
		{
			base.DataSource = dataSource;
			base.DataBind();
		}

		public virtual void DataBind(object dataSource, int pageIndex, int virtualItemCount)
		{
			this.AllowCustomPaging = true;
			this.AllowPaging = true;
			this.PageIndex = pageIndex;
			this.VirtualItemCount = virtualItemCount;
			base.DataSource = dataSource;
			base.DataBind();
		}
	}
}