﻿using Aspire.Lib.Helpers;
using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	public class Captcha : WebControl
	{
		public virtual Guid Guid
		{
			get { return (Guid)this.ViewState.GetValue(AspireViewStateKeys.Guid, Guid.Empty); }
			set { this.ViewState.SetValue(AspireViewStateKeys.Guid, value); }
		}

		public virtual bool IgnoreCase
		{
			get { return (bool)this.ViewState.GetValue(AspireViewStateKeys.IgnoreCase, false); }
			set { this.ViewState.SetValue(AspireViewStateKeys.IgnoreCase, value); }
		}

		private const int DefaultCaptchaLength = 5;
		[DefaultValue(DefaultCaptchaLength)]
		public virtual int CaptchaLength
		{
			get { return (int)this.ViewState.GetValue(AspireViewStateKeys.CaptchaLength, DefaultCaptchaLength); }
			set
			{
				if (value < 4 || value > 128)
					throw new Exception("Captcha Length must be 4 to 128");
				this.ViewState.SetValue(AspireViewStateKeys.CaptchaLength, value);
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if (this.Guid == Guid.Empty)
				this.Guid = Guid.NewGuid();
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (!this.Width.IsEmpty && this.Width.Type != UnitType.Pixel)
				throw new Exception("Width must be in pixels.");
			if (!this.Height.IsEmpty && this.Height.Type != UnitType.Pixel)
				throw new Exception("Height must be in pixels.");

			writer.AddAttribute(HtmlTextWriterAttribute.Class, "AspireCaptcha");
			writer.AddAttribute("Captcha", $"{this.Guid}::{this.IgnoreCase}::{this.CaptchaLength}::{this.Width}::{this.Height}::{this.Font.Size}::{this.Font.Name}".Encrypt(EncryptionModes.ConstantSaltNoneUserSessionNone));
			writer.RenderBeginTag(HtmlTextWriterTag.Div);
			writer.RenderEndTag();
		}
	}
}
