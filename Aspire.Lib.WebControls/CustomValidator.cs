﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	public class CustomValidator : System.Web.UI.WebControls.CustomValidator
	{
		[DefaultValue("")]
		[CssClassProperty]
		public virtual string HighlightCssClass
		{
			get { return (string)this.ViewState.GetValue(AspireViewStateKeys.HighlightCssClass, ""); }
			set { this.ViewState.SetValue(AspireViewStateKeys.HighlightCssClass, value); }
		}

		protected override void OnInit(EventArgs e)
		{
			this.SetFocusOnError = true;
			this.Display = ValidatorDisplay.Dynamic;
			base.OnInit(e);
		}

		protected override void OnPreRender(EventArgs e)
		{
			this.Attributes.Add("data-HighlightCssClass".ToLower(), this.HighlightCssClass);
			base.OnPreRender(e);
		}
	}
}
