﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	public class AspirePasswordStrengthMeter : WebControl
	{
		[DefaultValue("")]
		[IDReferenceProperty(typeof(System.Web.UI.WebControls.TextBox))]
		[Themeable(false)]
		[TypeConverter(typeof(ValidatedControlConverter))]
		public string TextBoxID
		{
			get { return (string)this.ViewState.GetValue(AspireViewStateKeys.TextBoxID, ""); }
			set { this.ViewState.SetValue(AspireViewStateKeys.TextBoxID, value); }
		}

		protected const string DefaultRating = "Very weak,Weak,Adequate,Pretty good,Excellent";
		[DefaultValue(DefaultRating)]
		public string RatingLabels
		{
			get { return (string)this.ViewState.GetValue(AspireViewStateKeys.RatingLabels, DefaultRating); }
			set { this.ViewState.SetValue(AspireViewStateKeys.RatingLabels, value); }
		}

		protected override void OnPreRender(EventArgs e)
		{
			ScriptManager.RegisterStartupScript(this, this.GetType(), "AspirePasswordStrengthMeter", $"InitAspirePasswordStrengthMeter({this.ClientID.JavaScriptStringEncode(true)});", true);
			base.OnPreRender(e);
		}

		protected override void Render(HtmlTextWriter writer)
		{
			var tb = this.FindControl(this.TextBoxID) as System.Web.UI.WebControls.TextBox;
			if (tb == null)
				throw new InvalidOperationException("Invalid or null TextBoxID.");
			writer.AddAttribute(HtmlTextWriterAttribute.Class, "progress AspirePasswordStrengthMeter");
			writer.AddAttribute("data-TextBoxID".ToLower(), tb.ClientID);
			writer.AddAttribute("data-RatingLabels".ToLower(), this.RatingLabels);
			writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);
			writer.RenderBeginTag(HtmlTextWriterTag.Div);
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Class, "progress-bar");
				writer.RenderBeginTag(HtmlTextWriterTag.Div);
				writer.RenderEndTag();
			}
			writer.RenderEndTag();
		}
	}
}
