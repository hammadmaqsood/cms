﻿using System.ComponentModel;

namespace Aspire.Lib.WebControls
{
	public class UInt32Validator : NumberValidator<uint>
	{
		protected override uint DefaultMaxValue => uint.MaxValue;

		protected override uint DefaultMinValue => uint.MinValue;

		protected override string GetClientValidationFunction()
		{
			return "ValidateInteger";
		}

		[DefaultValue(uint.MaxValue)]
		public override uint MaxValue
		{
			get { return (uint)this.ViewState.GetValue(AspireViewStateKeys.MaxValue, DefaultMaxValue); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MaxValue, value); }
		}

		[DefaultValue(uint.MinValue)]
		public override uint MinValue
		{
			get { return (uint)this.ViewState.GetValue(AspireViewStateKeys.MinValue, DefaultMinValue); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MinValue, value); }
		}
	}
}