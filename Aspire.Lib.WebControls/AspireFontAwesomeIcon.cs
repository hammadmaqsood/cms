﻿using System;
using System.ComponentModel;
using System.Web.UI;

namespace Aspire.Lib.WebControls
{
	public sealed class AspireFontAwesomeIcon : Control
	{
		public enum RenderTags { Span, I }

		[DefaultValue(RenderTags.Span)]
		public RenderTags RenderTag
		{
			get => (RenderTags)this.ViewState.GetValue(AspireViewStateKeys.RenderTag, RenderTags.Span);
			set => this.ViewState.SetValue(AspireViewStateKeys.RenderTag, value);
		}

		public FontAwesomeIcons Icon
		{
			get => this.ViewState.GetValue<FontAwesomeIcons?>(AspireViewStateKeys.Icon, null) ?? throw new InvalidOperationException("Icon not set.");
			set => this.ViewState.SetValue(AspireViewStateKeys.Icon, value);
		}

		protected override void Render(HtmlTextWriter writer)
		{
			writer.AddAttribute(HtmlTextWriterAttribute.Class, this.Icon.ToCssClass());
			switch (this.RenderTag)
			{
				case RenderTags.Span:
					writer.RenderBeginTag(HtmlTextWriterTag.Span);
					break;
				case RenderTags.I:
					writer.RenderBeginTag(HtmlTextWriterTag.I);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			writer.RenderEndTag();
		}
	}
}