﻿using System.ComponentModel;

namespace Aspire.Lib.WebControls
{
	public class UInt16Validator : NumberValidator<ushort>
	{
		protected override ushort DefaultMaxValue => ushort.MaxValue;

		protected override ushort DefaultMinValue => ushort.MinValue;

		protected override string GetClientValidationFunction()
		{
			return "ValidateInteger";
		}

		[DefaultValue(ushort.MaxValue)]
		public override ushort MaxValue
		{
			get { return (ushort)this.ViewState.GetValue(AspireViewStateKeys.MaxValue, DefaultMaxValue); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MaxValue, value); }
		}

		[DefaultValue(ushort.MinValue)]
		public override ushort MinValue
		{
			get { return (ushort)this.ViewState.GetValue(AspireViewStateKeys.MinValue, DefaultMinValue); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MinValue, value); }
		}
	}
}