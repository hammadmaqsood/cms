﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	public class AspireGridView : GridView
	{
		[DefaultValue(true)]
		public bool Responsive
		{
			get => this.ViewState.GetValue(AspireViewStateKeys.Responsive, true);
			set => this.ViewState.SetValue(AspireViewStateKeys.Responsive, value);
		}

		[DefaultValue(true)]
		public bool Stripped
		{
			get => this.ViewState.GetValue(AspireViewStateKeys.Stripped, true);
			set => this.ViewState.SetValue(AspireViewStateKeys.Stripped, value);
		}

		[DefaultValue(true)]
		public bool Hover
		{
			get => this.ViewState.GetValue(AspireViewStateKeys.Hover, true);
			set => this.ViewState.SetValue(AspireViewStateKeys.Hover, value);
		}

		[DefaultValue(true)]
		public bool Bordered
		{
			get => this.ViewState.GetValue(AspireViewStateKeys.Bordered, true);
			set => this.ViewState.SetValue(AspireViewStateKeys.Bordered, value);
		}

		[DefaultValue(true)]
		public bool Condensed
		{
			get => this.ViewState.GetValue(AspireViewStateKeys.Condensed, false);
			set => this.ViewState.SetValue(AspireViewStateKeys.Condensed, value);
		}

		[DefaultValue(20)]
		public override int PageSize
		{
			get => base.PageSize;
			set => base.PageSize = value;
		}

		protected override void OnInit(EventArgs e)
		{
			this.CellSpacing = -1;
			this.CellPadding = -1;
			this.PageSize = 20;
			this.GridLines = GridLines.None;
			this.EmptyDataText = "No record found.";
			base.OnInit(e);
		}

		protected override void OnPreRender(EventArgs e)
		{
			this.CellSpacing = -1;
			this.CellPadding = -1;
			this.GridLines = GridLines.None;
			if (this.Rows.Cast<GridViewRow>().Any(r => r.RowType == DataControlRowType.DataRow))
				if (this.BottomPagerRow != null && this.AllowPaging)
					this.BottomPagerRow.Visible = true;
			if (this.Bordered)
				this.AddCssClass("table-bordered");
			if (this.Stripped)
				this.AddCssClass("table-striped");
			if (this.Hover)
				this.AddCssClass("table-hover");
			if (this.Condensed)
				this.AddCssClass("table-condensed");
			this.AddCssClasses("table", "AspireGridView");
			if (this.AllowSorting)
				this.AddCssClass("sortable");
			else
				this.RemoveCssClass("sortable");
			base.OnPreRender(e);
			this.UseAccessibleHeader = true;
			if (this.HeaderRow != null)
				this.HeaderRow.TableSection = TableRowSection.TableHeader;
			if (this.FooterRow != null)
				this.FooterRow.TableSection = TableRowSection.TableFooter;
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (this.Responsive)
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Class, "table-responsive");
				writer.RenderBeginTag(HtmlTextWriterTag.Div);
				base.Render(writer);
				writer.RenderEndTag();
			}
			else
				base.Render(writer);
		}

		private string GetPageLinkNavigateUrl(int pageIndex)
		{
			return $@"javascript:__doPostBack('{this.UniqueID}','Page${pageIndex + 1}')";
		}

		private HyperLink GetPageLink(int pageIndex, bool isCurrentPage)
		{
			var pageNumber = pageIndex + 1;
			var link = new HyperLink { Text = pageNumber.ToString(), Enabled = !isCurrentPage };
			if (link.Enabled)
				link.NavigateUrl = this.GetPageLinkNavigateUrl(pageIndex);
			return link;
		}

		private HyperLink GetPageLink(int pageIndex, string text)
		{
			var link = this.GetPageLink(pageIndex, false);
			link.Text = text;
			return link;
		}

		private void Add(PlaceHolder ph, HyperLink hyperLink, bool isCurrentPage)
		{
			var li = new HtmlGenericControl("li");
			if (isCurrentPage)
				li.Attributes.Add("class", "active");
			li.Controls.Add(hyperLink);
			ph.Controls.Add(li);
		}

		protected override void InitializePager(GridViewRow row, int columnSpan, PagedDataSource pagedDataSource)
		{
			row.TableSection = TableRowSection.TableFooter;
			var tableCell = new TableCell { ColumnSpan = columnSpan, CssClass = "" };
			row.Cells.Add(tableCell);
			var ph = new PlaceHolder();
			tableCell.Controls.Add(ph);

			ph.Controls.Add(new LiteralControl("<ul class=\"pagination pagination-sm\">"));

			/*First Page*/
			var enabled = pagedDataSource.CurrentPageIndex > 0; //First Required
			var hl = new HyperLink { Text = "First", Enabled = enabled, NavigateUrl = this.GetPageLinkNavigateUrl(0) };
			this.Add(ph, hl, false);
			/*Previous Page*/
			enabled = pagedDataSource.CurrentPageIndex > 0; //Previous Required
			hl = new HyperLink { Text = "«", Enabled = enabled, NavigateUrl = this.GetPageLinkNavigateUrl(pagedDataSource.CurrentPageIndex - 1) };
			this.Add(ph, hl, false);
			/*Page Numbers*/
			var pagesFrom = (pagedDataSource.CurrentPageIndex / this.PagerSettings.PageButtonCount) * this.PagerSettings.PageButtonCount;
			var pagesTo = pagesFrom + this.PagerSettings.PageButtonCount - 1;

			if (pagesTo >= pagedDataSource.PageCount)
				pagesTo = pagedDataSource.PageCount - 1;

			if (pagesFrom > 1)
				this.Add(ph, this.GetPageLink(pagesFrom - 1, "..."), false);

			for (var i = pagesFrom; i <= pagesTo; i++)
				this.Add(ph, this.GetPageLink(i, i == pagedDataSource.CurrentPageIndex), i == pagedDataSource.CurrentPageIndex);

			if (pagesTo < pagedDataSource.PageCount - 1)
				this.Add(ph, this.GetPageLink(pagesTo + 1, "..."), false);
			/*Next Page*/
			enabled = pagedDataSource.CurrentPageIndex < pagedDataSource.PageCount - 1; //Next Required
			hl = new HyperLink { Text = "»", Enabled = enabled, NavigateUrl = this.GetPageLinkNavigateUrl(pagedDataSource.CurrentPageIndex + 1) };
			this.Add(ph, hl, false);
			/*Last Page*/
			enabled = pagedDataSource.CurrentPageIndex < pagedDataSource.PageCount - 1; //Last Required
			hl = new HyperLink { Text = "Last", Enabled = enabled, NavigateUrl = this.GetPageLinkNavigateUrl(pagedDataSource.PageCount - 1) };
			this.Add(ph, hl, false);

			ph.Controls.Add(new LiteralControl("</ul>"));

			var totalRows = pagedDataSource.IsCustomPagingEnabled ? pagedDataSource.VirtualCount : pagedDataSource.DataSourceCount;

			var pageSizesDropDownList = new DropDownList { CssClass = "form-control", AutoPostBack = true, CausesValidation = false };
			var pageSizes = new[] { 10, 20, 25, 50, 75, 100, 150, 200, 300, 500, 1000, 2000, 3000, 5000, 10000 };
			pageSizesDropDownList.Items.AddRange(pageSizes.Select(p => new ListItem(p.ToString())).ToArray());
			pageSizesDropDownList.SelectedValue = this.PageSize.ToString();
			pageSizesDropDownList.SelectedIndexChanged += this.pageSizesDropDownList_SelectedIndexChanged;
			var pageSizesDropDownListLabel = new AspireLabel { AssociatedControlID = pageSizesDropDownList.ID, Text = "Page Size" };
			var recordsLabel = new Label { Text = $"<strong>{totalRows}</strong> records in <strong>{pagedDataSource.PageCount}</strong> pages" };

			var pageSizeForm = new HtmlGenericControl("div");
			pageSizeForm.Attributes.Add("class", "form-inline pull-right");
			ph.Controls.Add(pageSizeForm);
			var pageSizeFormGroup = new HtmlGenericControl("div");
			pageSizeFormGroup.Attributes.Add("class", "form-group-sm");
			pageSizeForm.Controls.Add(pageSizeFormGroup);
			pageSizeFormGroup.Controls.Add(new LiteralControl(" "));
			pageSizeFormGroup.Controls.Add(pageSizesDropDownListLabel);
			pageSizeFormGroup.Controls.Add(new LiteralControl(" "));
			pageSizeFormGroup.Controls.Add(pageSizesDropDownList);
			pageSizeFormGroup.Controls.Add(new LiteralControl(" "));
			pageSizeFormGroup.Controls.Add(recordsLabel);
		}

		private void pageSizesDropDownList_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (!(sender is DropDownList ddl))
				throw new ArgumentNullException(nameof(sender));
			this.OnPageSizeChanging(new PageSizeEventArgs(Convert.ToInt32(ddl.SelectedValue)));
		}

		public sealed class PageSizeEventArgs : EventArgs
		{
			public int NewPageSize { get; }

			public PageSizeEventArgs(int newPageSize)
			{
				this.NewPageSize = newPageSize;
			}
		}

		public delegate void PageSizeChangingHandler(object sender, PageSizeEventArgs e);
		public event PageSizeChangingHandler PageSizeChanging;

		protected virtual void OnPageSizeChanging(PageSizeEventArgs args)
		{
			if (this.PageSizeChanging != null)
				this.PageSizeChanging(this, args);
			else
				throw new InvalidOperationException("PageSizeChanging event is not handled for AspireGridView in page. ID: " + this.ID);
		}

		private void ApplyColumnSortStyle(string sortExpression, SortDirection sortDirection)
		{
			foreach (DataControlField column in this.Columns)
				if (string.IsNullOrWhiteSpace(column.SortExpression))
					column.HeaderStyle.CssClass = null;
				else
					column.HeaderStyle.CssClass = column.SortExpression == sortExpression
						? sortDirection.ToString().Substring(0, 3)
						: "S";
		}

		public void DataBind(object dataSource, int pageIndex, int virtualItemCount, string sortExpression, SortDirection sortDirection)
		{
			base.DataBind(dataSource, pageIndex, virtualItemCount);
			this.ApplyColumnSortStyle(sortExpression, sortDirection);
		}

		public void DataBind(object dataSource, string sortExpression, SortDirection sortDirection)
		{
			this.AllowPaging = false;
			this.AllowCustomPaging = false;
			base.DataBind(dataSource);
			this.ApplyColumnSortStyle(sortExpression, sortDirection);
		}
	}
}