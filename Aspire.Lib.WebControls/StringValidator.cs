﻿using Aspire.Lib.Extensions.Exceptions;
using System;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace Aspire.Lib.WebControls
{
	public class StringValidator : Aspire.Lib.WebControls.CustomValidator
	{
		public enum ValidationExpressions
		{
			None,
			Enrollment,
			Custom,
			Email,
			IPV4,
			IPV6,
			Url,
			CNIC,
			Mobile,
			MobilePK,
			MobileInternational,
			PhonePK,
			PhoneInternational,
			GUID,
			NTNCNIC
		}

		[DefaultValue(true)]
		public bool AllowNull
		{
			get => this.ViewState.GetValue(AspireViewStateKeys.AllowNull, true);
			set => this.ViewState.SetValue(AspireViewStateKeys.AllowNull, value);
		}

		[DefaultValue(null)]
		public string RequiredErrorMessage
		{
			get => this.ViewState.GetValue<string>(AspireViewStateKeys.RequiredErrorMessage, null);
			set => this.ViewState.SetValue(AspireViewStateKeys.RequiredErrorMessage, value);
		}

		[DefaultValue(null)]
		public string InvalidDataErrorMessage
		{
			get => this.ViewState.GetValue<string>(AspireViewStateKeys.InvalidDataErrorMessage, null);
			set => this.ViewState.SetValue(AspireViewStateKeys.InvalidDataErrorMessage, value);
		}

		[DefaultValue(ValidationExpressions.None)]
		public ValidationExpressions ValidationExpression
		{
			get => this.ViewState.GetValue(AspireViewStateKeys.ValidationExpression, ValidationExpressions.None);
			set => this.ViewState.SetValue(AspireViewStateKeys.ValidationExpression, value);
		}

		[DefaultValue(null)]
		public string CustomValidationExpression
		{
			get => this.ViewState.GetValue<string>(AspireViewStateKeys.CustomValidationExpression, null);
			set => this.ViewState.SetValue(AspireViewStateKeys.CustomValidationExpression, value);
		}

		private string GetValidationExpression()
		{
			string exp;
			switch (this.ValidationExpression)
			{
				case ValidationExpressions.None:
					return null;
				case ValidationExpressions.Custom:
					if (string.IsNullOrWhiteSpace(this.CustomValidationExpression))
						throw new InvalidOperationException("CustomValidationExpression must be provided.");
					exp = this.CustomValidationExpression;
					break;
				case ValidationExpressions.Email:
					exp = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
					break;
				case ValidationExpressions.Url:
					exp = @"http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?";
					break;
				case ValidationExpressions.IPV4:
					exp = @"\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b";
					break;
				case ValidationExpressions.CNIC:
					exp = @"\d{5}-\d{7}-\d{1}";
					break;
				case ValidationExpressions.Mobile:
				case ValidationExpressions.MobilePK:
					exp = @"03\d{2}-\d{7}";
					break;
				case ValidationExpressions.MobileInternational:
				case ValidationExpressions.PhoneInternational:
					exp = @"00[1-9]\d{8,20}";
					break;
				case ValidationExpressions.PhonePK:
					exp = @"0[1-9]\d{8,20}";
					break;
				case ValidationExpressions.GUID:
					exp = @"[{(]?[0-9A-Fa-f]{8}[-]?([0-9A-Fa-f]{4}[-]?){3}[0-9A-Fa-f]{12}[)}]?";
					break;
				case ValidationExpressions.NTNCNIC:
					exp = @"(\d{7}-\d{1})|(\d{5}-\d{7}-\d{1})";
					break;
				default:
					throw new NotImplementedEnumException(this.ValidationExpression);
			}
			if (!exp.StartsWith("^"))
				exp = "^" + exp;
			if (!exp.EndsWith("$"))
				exp += "$";
			return exp;
		}

		protected override bool OnServerValidate(string value)
		{
			if (string.IsNullOrWhiteSpace(value))
			{
				if (this.AllowNull)
					return true;
				else
				{
					this.ErrorMessage = this.RequiredErrorMessage;
					this.Text = this.ErrorMessage;
					return false;
				}
			}

			var exp = this.GetValidationExpression();
			if (exp != null)
			{
				var regex = new Regex(exp);
				if (!regex.IsMatch(value))
				{
					this.ErrorMessage = this.InvalidDataErrorMessage;
					this.Text = this.ErrorMessage;
					return false;
				}
			}
			return true;
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.ControlToValidate))
				throw new InvalidOperationException("Provide ControlToValidate");
			this.Attributes.Add("data-RequiredErrorMessage".ToLower(), this.RequiredErrorMessage);
			this.Attributes.Add("data-InvalidDataErrorMessage".ToLower(), this.InvalidDataErrorMessage);
			this.Attributes.Add("data-ValidationExpression".ToLower(), this.GetValidationExpression());
			this.Attributes.Add("data-AllowNull".ToLower(), this.AllowNull.ToString());
			this.ValidateEmptyText = true;
			this.ClientValidationFunction = "ValidateString";
			base.OnPreRender(e);
		}

		public override string ToString()
		{
			var exp = string.Empty;
			if (!this.AllowNull)
			{
				if (string.IsNullOrWhiteSpace(this.RequiredErrorMessage))
					return "Provide RequiredErrorMessage";
				exp += this.RequiredErrorMessage + "/";
			}

			switch (this.ValidationExpression)
			{
				case ValidationExpressions.None:
					return exp;
				case ValidationExpressions.Custom:
					if (string.IsNullOrWhiteSpace(this.CustomValidationExpression))
						return "Provide CustomValidationExpression";
					exp += this.CustomValidationExpression + "/";
					break;
				default:
					exp += this.ValidationExpression + "/";
					break;
			}

			if (this.ValidationExpression != ValidationExpressions.None)
			{
				if (string.IsNullOrWhiteSpace(this.InvalidDataErrorMessage))
					return "Provide InvalidDataErrorMessage";
				exp += this.InvalidDataErrorMessage + "/";
			}
			return exp.Trim('/');
		}
	}
}