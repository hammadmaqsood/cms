using Aspire.Lib.Helpers;
using System;
using System.ComponentModel;

namespace Aspire.Lib.WebControls
{
	public class HiddenField : System.Web.UI.WebControls.HiddenField
	{
		public enum Modes
		{
			PlainText,
			Encrypted
		}

		[DefaultValue(Modes.PlainText)]
		public Modes Mode
		{
			get => this.ViewState.GetValue(AspireViewStateKeys.Mode, Modes.PlainText);
			set
			{
				var oldMode = this.Mode;
				switch (oldMode)
				{
					case Modes.PlainText:
						switch (value)
						{
							case Modes.PlainText:
								break;
							case Modes.Encrypted:
								base.Value = base.Value.Encrypt();
								this.ViewState.SetValue(AspireViewStateKeys.Mode, value);
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(value), value, null);
						}
						break;
					case Modes.Encrypted:
						switch (value)
						{
							case Modes.PlainText:
								base.Value = base.Value.Decrypt();
								this.ViewState.SetValue(AspireViewStateKeys.Mode, value);
								break;
							case Modes.Encrypted:
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(value), value, null);
						}
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		public new string Value
		{
			get
			{
				switch (this.Mode)
				{
					case Modes.PlainText:
						return base.Value;
					case Modes.Encrypted:
						return base.Value.Decrypt();
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			set
			{
				switch (this.Mode)
				{
					case Modes.PlainText:
						base.Value = value;
						break;
					case Modes.Encrypted:
						base.Value = value.Encrypt();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
	}
}