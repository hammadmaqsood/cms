﻿using Aspire.Lib.Extensions;
using System.ComponentModel;
using System.Web.UI;

namespace Aspire.Lib.WebControls
{
	public class AspireLinkButton : Aspire.Lib.WebControls.LinkButton
	{
		[DefaultValue(AspireGlyphicons.None)]
		public virtual AspireGlyphicons Glyphicon
		{
			get { return (AspireGlyphicons)this.ViewState.GetValue(AspireViewStateKeys.Glyphicon, AspireGlyphicons.None); }
			set { this.ViewState.SetValue(AspireViewStateKeys.Glyphicon, value); }
		}

		[DefaultValue(FontAwesomeIcons.None)]
		public virtual FontAwesomeIcons FontAwesomeIcon
		{
			get { return (FontAwesomeIcons)this.ViewState.GetValue(AspireViewStateKeys.FontAwesomeIcon, FontAwesomeIcons.None); }
			set { this.ViewState.SetValue(AspireViewStateKeys.FontAwesomeIcon, value); }
		}

		protected override void CreateChildControls()
		{
			var hasText = !string.IsNullOrEmpty(this.Text);
			if (this.Glyphicon != AspireGlyphicons.None)
			{
				this.Controls.Add(new AspireGlyphicon { Icon = this.Glyphicon });
				if (hasText)
					this.Controls.Add(new LiteralControl(" "));
			}
			if (this.FontAwesomeIcon != FontAwesomeIcons.None)
			{
				this.Controls.Add(new AspireFontAwesomeIcon { Icon = this.FontAwesomeIcon });
				if (hasText)
					this.Controls.Add(new LiteralControl(" "));
			}
			if (hasText)
				this.Controls.Add(new LiteralControl(this.Text.HtmlEncode()));
			base.CreateChildControls();
		}
	}
}