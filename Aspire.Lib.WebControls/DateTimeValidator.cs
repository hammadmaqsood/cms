﻿using Aspire.Lib.Extensions.Exceptions;
using System;
using System.ComponentModel;
using System.Globalization;

namespace Aspire.Lib.WebControls
{
	public class DateTimeValidator : Aspire.Lib.WebControls.CustomValidator
	{
		public enum DateTimeFormats
		{
			LongDateLongTime,
			LongDateShortTime,
			LongDate,
			LongTime,
			ShortDate,
			ShortTime,
			ShortDateTime,
			Custom,
		}

		[DefaultValue(true)]
		public bool AllowNull
		{
			get { return (bool)this.ViewState.GetValue(AspireViewStateKeys.AllowNull, true); }
			set { this.ViewState.SetValue(AspireViewStateKeys.AllowNull, value); }
		}

		[DefaultValue(null)]
		public string RequiredErrorMessage
		{
			get { return (string)this.ViewState.GetValue<string>(AspireViewStateKeys.RequiredErrorMessage, null); }
			set { this.ViewState.SetValue(AspireViewStateKeys.RequiredErrorMessage, value); }
		}

		[DefaultValue(null)]
		public string InvalidDataErrorMessage
		{
			get { return (string)this.ViewState.GetValue<string>(AspireViewStateKeys.InvalidDataErrorMessage, null); }
			set { this.ViewState.SetValue(AspireViewStateKeys.InvalidDataErrorMessage, value); }
		}

		[DefaultValue(null)]
		public string InvalidRangeErrorMessage
		{
			get { return (string)this.ViewState.GetValue<string>(AspireViewStateKeys.InvalidRangeErrorMessage, null); }
			set { this.ViewState.SetValue(AspireViewStateKeys.InvalidRangeErrorMessage, value); }
		}

		private string GetInvalidRangeErrorMessage()
		{
			if (string.IsNullOrEmpty(this.InvalidRangeErrorMessage))
				return this.RequiredErrorMessage;
			return this.InvalidRangeErrorMessage.Replace("{min}", this.MinDate?.ToString(this.GetFormat())).Replace("{max}", this.MaxDate?.ToString(this.GetFormat()));
		}

		[DefaultValue(DateTimeFormats.ShortDate)]
		public DateTimeFormats Format
		{
			get { return (DateTimeFormats)this.ViewState.GetValue(AspireViewStateKeys.DateFormat, DateTimeFormats.ShortDate); }
			set { this.ViewState.SetValue(AspireViewStateKeys.DateFormat, value); }
		}

		[DefaultValue(null)]
		public string CustomFormat
		{
			get { return (string)this.ViewState.GetValue<string>(AspireViewStateKeys.CustomFormat, null); }
			set { this.ViewState.SetValue(AspireViewStateKeys.CustomFormat, value); }
		}

		[DefaultValue(null)]
		public DateTime? MinDate
		{
			get { return this.ViewState.GetValue<DateTime?>(AspireViewStateKeys.MinDate, null); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MinDate, value); }
		}

		[DefaultValue(null)]
		public DateTime? MaxDate
		{
			get { return (DateTime?)this.ViewState.GetValue<DateTime?>(AspireViewStateKeys.MaxDate, null); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MaxDate, value); }
		}

		private string GetFormat()
		{
			var cultureInfo = CultureInfo.CurrentCulture;
			switch (this.Format)
			{
				case DateTimeFormats.LongDateLongTime:
					return $"{cultureInfo.DateTimeFormat.LongDatePattern} {cultureInfo.DateTimeFormat.LongTimePattern}";
				case DateTimeFormats.LongDateShortTime:
					return $"{cultureInfo.DateTimeFormat.LongDatePattern} {cultureInfo.DateTimeFormat.ShortTimePattern}";
				case DateTimeFormats.LongDate:
					return $"{cultureInfo.DateTimeFormat.LongDatePattern}";
				case DateTimeFormats.LongTime:
					return $"{cultureInfo.DateTimeFormat.LongTimePattern}";
				case DateTimeFormats.ShortDate:
					return $"{cultureInfo.DateTimeFormat.ShortDatePattern}";
				case DateTimeFormats.ShortTime:
					return $"{cultureInfo.DateTimeFormat.ShortTimePattern}";
				case DateTimeFormats.ShortDateTime:
					return $"{cultureInfo.DateTimeFormat.ShortDatePattern} {cultureInfo.DateTimeFormat.ShortTimePattern}";
				case DateTimeFormats.Custom:
					if (string.IsNullOrWhiteSpace(this.CustomFormat))
						throw new InvalidOperationException("Provide CustomFormat. ID:" + this.ClientID);
					return this.CustomFormat;
				default:
					throw new NotImplementedEnumException(this.Format);
			}
		}

		protected override bool OnServerValidate(string value)
		{
			if (string.IsNullOrWhiteSpace(value))
			{
				if (this.AllowNull)
					return true;
				else
				{
					this.ErrorMessage = this.RequiredErrorMessage;
					this.Text = this.ErrorMessage;
					return false;
				}
			}

			var format = this.GetFormat();

			DateTime dt;
			var result = DateTime.TryParseExact(value, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
			if (!result)
			{
				this.ErrorMessage = this.InvalidDataErrorMessage;
				this.Text = this.ErrorMessage;
				return false;
			}
			if (this.MinDate != null && dt < this.MinDate.Value)
			{
				this.ErrorMessage = this.GetInvalidRangeErrorMessage();
				this.Text = this.ErrorMessage;
				return false;
			}
			if (this.MaxDate != null && this.MaxDate.Value < dt)
			{
				this.ErrorMessage = this.GetInvalidRangeErrorMessage();
				this.Text = this.ErrorMessage;
				return false;
			}
			return true;
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.ControlToValidate))
				throw new InvalidOperationException("Provide ControlToValidate");
			this.Attributes.Add("data-RequiredErrorMessage".ToLower(), this.RequiredErrorMessage);
			this.Attributes.Add("data-InvalidDataErrorMessage".ToLower(), this.InvalidDataErrorMessage);
			this.Attributes.Add("data-InvalidRangeErrorMessage".ToLower(), this.GetInvalidRangeErrorMessage());
			this.Attributes.Add("data-Format".ToLower(), this.GetFormat());
			this.Attributes.Add("data-AllowNull".ToLower(), this.AllowNull.ToString());
			this.Attributes.Add("data-MinDate".ToLower(), this.MinDate?.ToString("f"));
			this.Attributes.Add("data-MaxDate".ToLower(), this.MaxDate?.ToString("f"));
			this.ValidateEmptyText = true;
			this.ClientValidationFunction = "ValidateDateTime";
			base.OnPreRender(e);
		}

		public override string ToString()
		{
			var exp = string.Empty;
			if (!this.AllowNull)
			{
				if (string.IsNullOrWhiteSpace(this.RequiredErrorMessage))
					return "Provide RequiredErrorMessage";
				exp += this.RequiredErrorMessage + "/";
			}
			if (string.IsNullOrWhiteSpace(this.InvalidDataErrorMessage))
				return "Provide InvalidDataErrorMessage";
			exp += this.GetFormat() + "/";
			exp += this.InvalidDataErrorMessage + "/";
			return exp.Trim('/');
		}
	}
}