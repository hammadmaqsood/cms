﻿using System.ComponentModel;

namespace Aspire.Lib.WebControls
{
	public class Int32Validator : NumberValidator<int>
	{
		protected override int DefaultMaxValue => int.MaxValue;

		protected override int DefaultMinValue => int.MinValue;

		protected override string GetClientValidationFunction()
		{
			return "ValidateInteger";
		}

		[DefaultValue(int.MaxValue)]
		public override int MaxValue
		{
			get { return (int)this.ViewState.GetValue(AspireViewStateKeys.MaxValue, DefaultMaxValue); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MaxValue, value); }
		}

		[DefaultValue(int.MinValue)]
		public override int MinValue
		{
			get { return (int)this.ViewState.GetValue(AspireViewStateKeys.MinValue, DefaultMinValue); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MinValue, value); }
		}
	}
}