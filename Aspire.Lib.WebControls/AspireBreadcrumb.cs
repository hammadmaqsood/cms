﻿using System;
using Aspire.Lib.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aspire.Lib.WebControls
{
	public class Breadcrumbs
	{
		public class AspireBreadcrumbLink
		{
			public string Text { get; private set; }
			public string NavigateUrl { get; private set; }
			public bool IsHomeLink { get; private set; }

			public AspireBreadcrumbLink(string text, string navigateUrl, bool isHomeLink)
			{
				this.Text = text;
				this.NavigateUrl = navigateUrl;
				this.IsHomeLink = isHomeLink;
			}

			public string ToHtml()
			{
				if (this.IsHomeLink)
					return string.Format(@"<li><a href=""{0}"" title=""{1}""><i class=""fa fa-desktop fa-1""></i></a> <a href=""{0}"">{1}</a></li>", HttpUtility.HtmlAttributeEncode(this.NavigateUrl.ToAbsoluteUrl()), HttpUtility.HtmlEncode(this.Text));
				var url = this.NavigateUrl.ToAbsoluteUrl().HtmlAttributeEncode();
				if (string.IsNullOrWhiteSpace(url))
					return $@"<li><a>{this.Text.HtmlEncode()}</a></li>";
				return $@"<li><a href=""{url}"">{this.Text.HtmlEncode()}</a></li>";
			}
		}

		private readonly List<AspireBreadcrumbLink> _links = new List<AspireBreadcrumbLink>();

		public AspireBreadcrumbLink AddLink(AspireBreadcrumbLink link)
		{
			if (!this._links.Any())
			{
				if (link.IsHomeLink == false)
					throw new InvalidOperationException("IsHomeLink must be true for first link.");
			}
			else
			{
				if (link.IsHomeLink)
					throw new InvalidOperationException("IsHomeLink must be false for links except first one.");
			}
			this._links.Add(link);
			return link;
		}

		public string ToHtml()
		{
			if (!this._links.Any())
				return null;
			var html = "<ol class=\"breadcrumb\">";
			foreach (var link in this._links)
				html += link.ToHtml();
			return html + "</ol>";
		}
	}
}
