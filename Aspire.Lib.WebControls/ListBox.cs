﻿using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	public class ListBox : System.Web.UI.WebControls.ListBox
	{
		protected override void OnPreRender(EventArgs e)
		{
			if (this.CausesValidation == false && this.AutoPostBack)
				this.Attributes.Add("onchange", "window.Page_BlockSubmit=false;");
			base.OnPreRender(e);
		}

		public string[] SelectedValues => this.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray();

	}
}