using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using System;
using System.ComponentModel;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	public class TextBox : System.Web.UI.WebControls.TextBox
	{
		public enum TextTransforms : byte
		{
			None, UpperCase, LowerCase
		}

		[DefaultValue("")]
		public string PlaceHolder
		{
			get => (string)this.ViewState.GetValue(AspireViewStateKeys.PlaceHolder, "");
			set => this.ViewState.SetValue(AspireViewStateKeys.PlaceHolder, value);
		}

		[DefaultValue(true)]
		public bool TrimText
		{
			get => (bool)this.ViewState.GetValue(AspireViewStateKeys.TrimText, true);
			set => this.ViewState.SetValue(AspireViewStateKeys.TrimText, value);
		}

		public override string Text
		{
			get
			{
				if (this.TrimText)
					base.Text = base.Text.TrimIfNotNull();
				switch (this.TextTransform)
				{
					case TextTransforms.None:
						break;
					case TextTransforms.UpperCase:
						base.Text = base.Text.ToUpper();
						break;
					case TextTransforms.LowerCase:
						base.Text = base.Text.ToLower();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				return base.Text;
			}
			set
			{
				if (this.TrimText)
					base.Text = value.TrimIfNotNull();
				else
					base.Text = value;
				switch (this.TextTransform)
				{
					case TextTransforms.None:
						break;
					case TextTransforms.UpperCase:
						base.Text = base.Text.ToUpper();
						break;
					case TextTransforms.LowerCase:
						base.Text = base.Text.ToLower();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		[DefaultValue(TextTransforms.None)]
		public TextTransforms TextTransform
		{
			get => (TextTransforms)this.ViewState.GetValue(AspireViewStateKeys.TextTransform, TextTransforms.None);
			set => this.ViewState.SetValue(AspireViewStateKeys.TextTransform, value);
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (!string.IsNullOrWhiteSpace(this.PlaceHolder))
				this.Attributes.Add("placeholder", this.PlaceHolder);
			this.Attributes.Add("onblur", "TrimAndTransformIfRequired(this);");
			this.Attributes.Add("data-TrimText", this.TrimText.ToString());
			if (this.MaxLength == 0)
				this.MaxLength = 50;
			if (this.TextMode == TextBoxMode.MultiLine && this.MaxLength > 0 && this.Enabled && this.ReadOnly == false && this.Visible)
			{
				this.Attributes.Add("maxlength", $"{this.MaxLength}");
				this.Attributes.Add("onkeypress", $"TrimTextArea(this,{this.MaxLength})");
				this.Attributes.Add("onblur", $"TrimTextArea(this,{this.MaxLength})");
				this.Attributes.Add("onpaste", $"TrimTextArea(this,{this.MaxLength})");
			}
			switch (this.TextTransform)
			{
				case TextTransforms.None:
					break;
				case TextTransforms.UpperCase:
					this.Style.Add("text-transform", "uppercase");
					break;
				case TextTransforms.LowerCase:
					this.Style.Add("text-transform", "lowercase");
					break;
				default:
					throw new NotImplementedEnumException(this.TextTransform);
			}
			base.OnPreRender(e);
		}
	}
}