﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.SessionState;

namespace Aspire.Lib.WebControls
{
	public class CaptchaHandler : IHttpHandler, IRequiresSessionState
	{
		private static readonly Dictionary<Guid, Captcha> CaptchaList = new Dictionary<Guid, Captcha>();

		private static void DisposeExpiredCaptchas()
		{
			var expiredGuids = CaptchaList.Where(l => l.Value.ExpireDate < DateTime.Now).Select(l => l.Key).ToList();
			foreach (var expiredGuid in expiredGuids)
				CaptchaList.Remove(expiredGuid);
		}

		private static Captcha GetCaptcha(Guid guid, bool ignoreCase, int captchaLength, int? width, int? height, double? fontSize, string fontFamilyName)
		{
			DisposeExpiredCaptchas();
			Captcha captcha;
			if (CaptchaList.ContainsKey(guid))
				captcha = CaptchaList[guid];
			else
			{
				captcha = new Captcha(ignoreCase, captchaLength, width, height, fontSize, fontFamilyName);
				CaptchaList.Add(guid, captcha);
				return captcha;
			}
			captcha.GenerateNewCaptchaText();
			return captcha;
		}

		public void ProcessRequest(HttpContext context)
		{
			lock (CaptchaList)
			{
				Guid captchaKey;
				bool ignoreCase;
				int captchaLength;
				int? width;
				int? height;
				double? fontSize;
				string fontFamilyName;
				try
				{
					var pCaptcha = context.Request.Params["Captcha"];
					var p = pCaptcha.Decrypt().Split(new[] { "::" }, StringSplitOptions.None);
					captchaKey = p[0].ToGuid();
					ignoreCase = p[1].ToBoolean();
					captchaLength = p[2].ToInt32();
					width = p[3].ToNullableInt32();
					height = p[4].ToNullableInt32();
					fontSize = p[5].ToNullableDouble();
					fontFamilyName = p[6];
				}
				catch (Exception) { return; }
				var captcha = GetCaptcha(captchaKey, ignoreCase, captchaLength, width, height, fontSize, fontFamilyName);

				var fileContents = captcha.GetImage();
				context.Response.Clear();
				context.Response.AddHeader("Content-Length", fileContents.Length.ToString());
				context.Response.AddHeader("Content-Disposition", "attachment; filename=Captcha.png");
				context.Response.OutputStream.Write(fileContents, 0, fileContents.Length);
				context.Response.Flush();
				context.Response.End();
			}
		}

		public bool IsReusable => false;

		public static bool ValidateCaptcha(Guid guid, string text)
		{
			lock (CaptchaList)
			{
				if (!CaptchaList.ContainsKey(guid))
					return false;
				return CaptchaList[guid].ValidateText(text);
			}
		}

		private class Captcha
		{
			private bool IgnoreCase { get; set; }
			private int CaptchaLength { get; set; }
			private string CaptchaText { get; set; }
			private double FontSize { get; set; }
			private string FontFamilyName { get; set; }
			private int Width { get; set; }
			private int Height { get; set; }
			public DateTimeOffset ExpireDate { get; private set; }

			public Captcha(bool ignoreCase, int captchaLength, int? width, int? height, double? fontSize, string fontFamilyName)
			{
				this.IgnoreCase = ignoreCase;
				this.CaptchaLength = captchaLength;
				this.Width = width ?? 150;
				this.Height = height ?? 50;
				if (fontSize == null)
					this.FontSize = this.Height + 2;
				else
					this.FontSize = fontSize.Value;
				this.FontFamilyName = string.IsNullOrWhiteSpace(fontFamilyName) ? "Courier New" : fontFamilyName;
				this.GenerateNewCaptchaText();
			}

			public void GenerateNewCaptchaText()
			{
				var captchaText = System.Web.Security.Membership.GeneratePassword(2 * this.CaptchaLength * this.CaptchaLength, 0);
				captchaText = Regex.Replace(captchaText, @"[^a-zA-Z0-9]", m => "").Replace(" ", "");
				if (captchaText.Length < this.CaptchaLength)
					throw new InvalidOperationException("Captcha length is invalid.");
				captchaText = captchaText.Substring(0, this.CaptchaLength);
				if (this.IgnoreCase)
					captchaText = captchaText.ToLower();
				this.CaptchaText = captchaText;
				this.ExpireDate = DateTime.Now.AddMinutes(15);
			}

			internal bool ValidateText(string text)
			{
				var captchaText = this.CaptchaText;
				if (this.IgnoreCase)
				{
					captchaText = captchaText.ToLower();
					text = text.ToLower();
				}
				return captchaText == text;
			}

			internal byte[] GetImage()
			{
				var width = this.Width;
				var height = this.Height;
				var fontSize = this.FontSize;
				var fontFamilyName = this.FontFamilyName;
				var captchaText = this.CaptchaText;

				using (var bitmap = new Bitmap(width, height, PixelFormat.Format32bppArgb))
				using (var g = Graphics.FromImage(bitmap))
				using (var format = new StringFormat { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center, Trimming = StringTrimming.None, FormatFlags = StringFormatFlags.NoWrap })
				using (var path = new GraphicsPath())
				using (var captchaFont = new Font(fontFamilyName, (float)fontSize, FontStyle.Strikeout | FontStyle.Bold))
				using (var ms = new MemoryStream())
				{
					g.SmoothingMode = SmoothingMode.AntiAlias;
					var rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
					using (var hatchBrush = new HatchBrush(HatchStyle.LargeConfetti, Color.LightGray, Color.White))
					{
						g.FillRectangle(hatchBrush, rect);
					}

					path.AddString(captchaText, captchaFont.FontFamily, (int)captchaFont.Style, captchaFont.Size, rect, format);
					const float v = 4F;
					var random = new Random();
					PointF[] points =
				{
					new PointF(random.Next(rect.Width)/v, random.Next(rect.Height)/v),
					new PointF(rect.Width - random.Next(rect.Width)/v, random.Next(rect.Height)/v),
					new PointF(random.Next(rect.Width)/v, rect.Height - random.Next(rect.Height)/v),
					new PointF(rect.Width - random.Next(rect.Width)/v, rect.Height - random.Next(rect.Height)/v)
				};
					var matrix = new Matrix();
					matrix.Translate(0F, 0F);
					path.Warp(points, rect, matrix, WarpMode.Perspective, 0F);
					var rrect = new Rectangle(0, 0, 4, 4);
					using (var brush = new LinearGradientBrush(rrect, Color.DarkGray, Color.Gray, LinearGradientMode.BackwardDiagonal))
					{
						g.FillPath(brush, path);
						var m = Math.Max(rect.Width, rect.Height);
						for (var i = 0; i < (int)(rect.Width * rect.Height / 30F); i++)
						{
							var x = random.Next(rect.Width);
							var y = random.Next(rect.Height);
							var w = random.Next(m / 50);
							var h = random.Next(m / 50);
							g.FillEllipse(brush, x, y, w, h);
						}
					}
					bitmap.Save(ms, ImageFormat.Png);
					return ms.ToArray();
				}
			}
		}
	}
}