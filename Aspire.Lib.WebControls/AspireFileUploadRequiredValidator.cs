﻿using System;
using System.ComponentModel;
using System.Linq;

namespace Aspire.Lib.WebControls
{
	public class AspireFileUploadValidator : AspireCustomValidator
	{
		public string AspireFileUploadID
		{
			get => (string)this.ViewState.GetValue(AspireViewStateKeys.AspireFileUploadID, null);
			set => this.ViewState.SetValue(AspireViewStateKeys.AspireFileUploadID, value);
		}

		[DefaultValue("")]
		public string UploadingInProgressMessage
		{
			get => this.ViewState.GetValue(AspireViewStateKeys.UploadingInProgressMessage, "");
			set => this.ViewState.SetValue(AspireViewStateKeys.UploadingInProgressMessage, value);
		}

		[DefaultValue(true)]
		public bool AllowNull
		{
			get => this.ViewState.GetValue<bool>(AspireViewStateKeys.AllowNull, true);
			set => this.ViewState.SetValue(AspireViewStateKeys.AllowNull, value);
		}

		[DefaultValue("")]
		public string RequiredErrorMessage
		{
			get => this.ViewState.GetValue(AspireViewStateKeys.RequiredErrorMessage, "");
			set => this.ViewState.SetValue(AspireViewStateKeys.RequiredErrorMessage, value);
		}

		protected override bool OnServerValidate(string value)
		{
			return ((AspireFileUpload)this.FindControl(this.AspireFileUploadID))?.UploadedFiles?.Any() == true;
		}

		protected override void OnPreRender(EventArgs e)
		{
			this.AddCssClass("text-danger");
			this.EnableClientScript = true;
			var aspireFileUpload = (AspireFileUpload)this.FindControl(this.AspireFileUploadID);
			if (aspireFileUpload == null)
				throw new InvalidOperationException("AspireFileUploadID is missing. ClientID: " + this.ClientID);
			this.Attributes.Add($"data-{nameof(this.AspireFileUploadID)}", aspireFileUpload.ClientID);
			this.Attributes.Add($"data-{nameof(this.RequiredErrorMessage)}", this.RequiredErrorMessage);
			this.Attributes.Add($"data-{nameof(this.AllowNull)}", this.AllowNull.ToString());
			this.Attributes.Add($"data-{nameof(this.UploadingInProgressMessage)}", this.UploadingInProgressMessage);
			this.ValidateEmptyText = true;
			this.ClientValidationFunction = "AspireFileUploadValidator";
			base.OnPreRender(e);
		}
	}
}