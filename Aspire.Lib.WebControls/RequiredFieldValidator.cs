﻿using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	public class RequiredFieldValidator : CustomValidator
	{
		protected override bool ControlPropertiesValid()
		{
			if (this.FindControl(this.ControlToValidate) is CheckBoxList)
				return true;
			if (this.FindControl(this.ControlToValidate) is RadioButtonList)
				return true;
			if (this.FindControl(this.ControlToValidate) is ListBox)
				return true;
			if (this.FindControl(this.ControlToValidate) is System.Web.UI.WebControls.HiddenField)
				return true;
			if (this.FindControl(this.ControlToValidate) is System.Web.UI.WebControls.CheckBox)
				return true;
			return base.ControlPropertiesValid();
		}

		protected override bool OnServerValidate(string value)
		{
			if (!string.IsNullOrWhiteSpace(value))
				return true;
			var control = this.FindControl(this.ControlToValidate);
			if (control is System.Web.UI.WebControls.TextBox)
				return false;
			if (control is ListControl lc)
				return lc.Items.Cast<ListItem>().Any(i => i.Selected);
			if (control is FileUpload fu)
				return fu.HasFile;
			if (control is System.Web.UI.WebControls.CheckBox cb)
				return cb.Checked;
			throw new NotSupportedException("Type:" + control.GetType());
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.ControlToValidate))
				throw new InvalidOperationException("Provide ControlToValidate");
			this.Attributes.Add("data-RequiredErrorMessage".ToLower(), this.ErrorMessage);
			this.ValidateEmptyText = true;
			this.ClientValidationFunction = "ValidateRequiredField";
			base.OnPreRender(e);
		}
	}
}
