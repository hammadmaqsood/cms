﻿using System;

namespace Aspire.Lib.WebControls
{
	public class AspireLabel : Aspire.Lib.WebControls.Label
	{
		protected override void OnPreRender(EventArgs e)
		{
			this.AddCssClass("control-label");
			base.OnPreRender(e);
		}
	}
}
