﻿using System;

namespace Aspire.Lib.WebControls
{
	public class AspireListBox : ListBox
	{
		protected override void OnPreRender(EventArgs e)
		{
			this.AddCssClass("form-control AspireListBox");
			base.OnPreRender(e);
		}
	}
}