﻿using System;
using System.ComponentModel;
using System.Linq;

namespace Aspire.Lib.WebControls
{
	public sealed class AspirePasswordPolicyValidator : AspireCustomValidator
	{
		public const string LowPasswordStrengthErrorMessage = "Password must be at least 8 characters in length.";
		public const string MediumPasswordStrengthErrorMessage = "Password must be at least 8 characters in length and include at least one letter, one number, and one symbol.";
		public const string HighPasswordStrengthErrorMessage = "Password must be at least 10 characters in length and contain at least one uppercase letter, one lowercase letter, one number, one symbol.";
		public const string HighestPasswordStrengthErrorMessage = "Password must be at least 12 characters in length and contain at least one uppercase letter, one lowercase letter, one number, one symbol.";

		public enum SecurityLevels
		{
			Low,
			Medium,
			High,
			Highest,
		}

		public string GetSecurityLevelDescription()
		{
			switch (this.SecurityLevel)
			{
				case SecurityLevels.Low:
					return LowPasswordStrengthErrorMessage;
				case SecurityLevels.Medium:
					return MediumPasswordStrengthErrorMessage;
				case SecurityLevels.High:
					return HighPasswordStrengthErrorMessage;
				case SecurityLevels.Highest:
					return HighestPasswordStrengthErrorMessage;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		[DefaultValue(SecurityLevels.Medium)]
		public SecurityLevels SecurityLevel
		{
			get { return (SecurityLevels)this.ViewState.GetValue(AspireViewStateKeys.SecurityLevel, SecurityLevels.Medium); }
			set { this.ViewState.SetValue(AspireViewStateKeys.SecurityLevel, value); }
		}

		[DefaultValue(6)]
		public int MinimumPasswordLength
		{
			get { return (int)this.ViewState.GetValue(AspireViewStateKeys.MinimumPasswordLength, -1); }
			private set { this.ViewState.SetValue(AspireViewStateKeys.MinimumPasswordLength, value); }
		}

		[DefaultValue(-1)]
		public int MinimumNoOfLetters
		{
			get { return (int)this.ViewState.GetValue(AspireViewStateKeys.MinimumNoOfLetters, -1); }
			private set { this.ViewState.SetValue(AspireViewStateKeys.MinimumNoOfLetters, value); }
		}

		[DefaultValue(-1)]
		public int MinimumNoOfUpperCaseChars
		{
			get { return (int)this.ViewState.GetValue(AspireViewStateKeys.MinimumNoOfUpperCaseChars, -1); }
			private set { this.ViewState.SetValue(AspireViewStateKeys.MinimumNoOfUpperCaseChars, value); }
		}

		[DefaultValue(-1)]
		public int MinimumNoOfLowerCaseChars
		{
			get { return (int)this.ViewState.GetValue(AspireViewStateKeys.MinimumNoOfLowerCaseChars, -1); }
			private set { this.ViewState.SetValue(AspireViewStateKeys.MinimumNoOfLowerCaseChars, value); }
		}

		[DefaultValue(-1)]
		public int MinimumNoOfNumbers
		{
			get { return (int)this.ViewState.GetValue(AspireViewStateKeys.MinimumNoOfNumbers, -1); }
			private set { this.ViewState.SetValue(AspireViewStateKeys.MinimumNoOfNumbers, value); }
		}

		[DefaultValue(-1)]
		public int MinimumNoOfSymbols
		{
			get { return (int)this.ViewState.GetValue(AspireViewStateKeys.MinimumNoOfSymbols, -1); }
			private set { this.ViewState.SetValue(AspireViewStateKeys.MinimumNoOfSymbols, value); }
		}

		private void ApplySecurityLevelSettings()
		{
			switch (this.SecurityLevel)
			{
				case SecurityLevels.Low:
					this.MinimumPasswordLength = 8;
					this.MinimumNoOfLetters = -1;
					this.MinimumNoOfUpperCaseChars = -1;
					this.MinimumNoOfLowerCaseChars = -1;
					this.MinimumNoOfNumbers = -1;
					this.MinimumNoOfSymbols = -1;
					break;
				case SecurityLevels.Medium:
					this.MinimumPasswordLength = 8;
					this.MinimumNoOfLetters = 1;
					this.MinimumNoOfUpperCaseChars = -1;
					this.MinimumNoOfLowerCaseChars = -1;
					this.MinimumNoOfNumbers = 1;
					this.MinimumNoOfSymbols = 1;
					break;
				case SecurityLevels.High:
					this.MinimumPasswordLength = 10;
					this.MinimumNoOfLetters = -1;
					this.MinimumNoOfUpperCaseChars = 1;
					this.MinimumNoOfLowerCaseChars = 1;
					this.MinimumNoOfNumbers = 1;
					this.MinimumNoOfSymbols = 1;
					break;
				case SecurityLevels.Highest:
					this.MinimumPasswordLength = 12;
					this.MinimumNoOfLetters = -1;
					this.MinimumNoOfUpperCaseChars = 1;
					this.MinimumNoOfLowerCaseChars = 1;
					this.MinimumNoOfNumbers = 1;
					this.MinimumNoOfSymbols = 1;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected override bool OnServerValidate(string value)
		{
			this.ApplySecurityLevelSettings();
			return this.CheckMinimumPasswordLength(value)
				&& this.CheckMinimumNoOfLetters(value)
				&& this.CheckMinimumNoOfUpperCaseChars(value)
				&& this.CheckMinimumNoOfLowerCaseChars(value)
				&& this.CheckMinimumNoOfNumbers(value)
				&& this.CheckMinimumNoOfSymbols(value);
		}

		private bool CheckMinimumPasswordLength(string value)
		{
			if (this.MinimumPasswordLength < 0)
				return true;
			return value.Length >= this.MinimumPasswordLength;
		}

		private const string UpperCaseCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		private const string LowerCaseCharacters = "abcdefghijklmnopqrstuvwxyz";
		private const string Letters = UpperCaseCharacters + LowerCaseCharacters;
		private const string Digits = "0123456789";
		private const string Symbols = @"`~!@#$%^&*()_+-={}|\:"";'<>?,./";

		private bool CheckMinimumNoOfLetters(string value)
		{
			if (this.MinimumNoOfUpperCaseChars < 0)
				return true;
			return value.Count(c => Letters.Contains(c)) >= this.MinimumNoOfLetters;
		}

		private bool CheckMinimumNoOfUpperCaseChars(string value)
		{
			if (this.MinimumNoOfUpperCaseChars < 0)
				return true;
			return value.Count(c => UpperCaseCharacters.Contains(c)) >= this.MinimumNoOfUpperCaseChars;
		}

		private bool CheckMinimumNoOfLowerCaseChars(string value)
		{
			if (this.MinimumNoOfLowerCaseChars < 0)
				return true;
			return value.Count(c => LowerCaseCharacters.Contains(c)) >= this.MinimumNoOfLowerCaseChars;
		}

		private bool CheckMinimumNoOfNumbers(string value)
		{
			if (this.MinimumNoOfNumbers < 0)
				return true;
			return value.Count(c => Digits.Contains(c)) >= this.MinimumNoOfNumbers;
		}

		private bool CheckMinimumNoOfSymbols(string value)
		{
			if (this.MinimumNoOfSymbols < 0)
				return true;
			return value.Count(c => Symbols.Contains(c)) >= this.MinimumNoOfSymbols;
		}

		protected override void OnPreRender(EventArgs e)
		{
			this.ApplySecurityLevelSettings();
			this.ErrorMessage = this.GetSecurityLevelDescription();
			this.ClientValidationFunction = "ValidatePasswordPolicy";
			this.Attributes.Add("data-MinimumPasswordLength", this.MinimumPasswordLength.ToString());
			this.Attributes.Add("data-MinimumNoOfLetters", this.MinimumNoOfLetters.ToString());
			this.Attributes.Add("data-MinimumNoOfUpperCaseChars", this.MinimumNoOfUpperCaseChars.ToString());
			this.Attributes.Add("data-MinimumNoOfLowerCaseChars", this.MinimumNoOfLowerCaseChars.ToString());
			this.Attributes.Add("data-MinimumNoOfNumbers", this.MinimumNoOfNumbers.ToString());
			this.Attributes.Add("data-MinimumNoOfSymbols", this.MinimumNoOfSymbols.ToString());
			base.OnPreRender(e);
		}
	}
}
