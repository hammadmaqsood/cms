using System;
using System.Web.UI;

namespace Aspire.Lib.WebControls
{
	[ValidationProperty("SelectedValues")]
	public class AspireCheckBoxList : CheckBoxList
	{
		protected override void OnPreRender(EventArgs e)
		{
			this.AddCssClass("AspireCheckBoxList");
			base.OnPreRender(e);
		}
	}
}