﻿using Aspire.Lib.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	public sealed class AspireFileUpload : UserControl
	{
		private System.Web.UI.WebControls.HiddenField _hf;

		public sealed class File
		{
			public Guid Instance { get; set; }
			public string FileID { get; set; }
			public string FileName { get; set; }
			public string FileType { get; set; }

			internal void Validate(Guid instanceGuid, string allowedFileTypes)
			{
				if (string.IsNullOrWhiteSpace(this.FileName))
					throw new InvalidOperationException();
				if (!string.IsNullOrWhiteSpace(allowedFileTypes) && !allowedFileTypes.Split(',').Any(t => this.FileName.ToLower().EndsWith(t)))
					throw new InvalidOperationException();
				this.FileID.ToGuid();//just validating.
				this.Instance = instanceGuid;
			}
		}

		public List<File> UploadedFiles
		{
			get
			{
				this.EnsureChildControls();
				var files = this._hf.Value?.FromJsonString<List<File>>();
				files?.ForEach(f => f.Validate(this.InstanceGuid, this.AllowedFileTypes));
				return files;
			}
		}

		public void ClearFiles()
		{
			this.EnsureChildControls();
			this._hf.Value = null;
			this.InstanceGuid = Guid.NewGuid();
		}

		private const bool DefaultEnabled = true;

		[DefaultValue(DefaultEnabled)]
		public bool Enabled
		{
			get => this.ViewState.GetValue(AspireViewStateKeys.Enabled, DefaultEnabled);
			set => this.ViewState.SetValue(AspireViewStateKeys.Enabled, value);
		}

		private const int DefaultMaxFileSize = 5 * 1024 * 1024;

		[DefaultValue(DefaultMaxFileSize)]
		public int? MaxFileSize
		{
			get => this.ViewState.GetValue(AspireViewStateKeys.MaxFileSize, DefaultMaxFileSize);
			set => this.ViewState.SetValue(AspireViewStateKeys.MaxFileSize, value);
		}

		private const byte DefaultMaxNumberOfFiles = 1;

		[DefaultValue(DefaultMaxNumberOfFiles)]
		public byte MaxNumberOfFiles
		{
			get => this.ViewState.GetValue(AspireViewStateKeys.MaxNumberOfFiles, DefaultMaxNumberOfFiles);
			set => this.ViewState.SetValue(AspireViewStateKeys.MaxNumberOfFiles, value);
		}

		[DefaultValue(null)]
		public string AllowedFileTypes
		{
			get => this.ViewState.GetValue(AspireViewStateKeys.AllowedFileTypes, (string)null);
			set => this.ViewState.SetValue(AspireViewStateKeys.AllowedFileTypes, value);
		}

		public Guid InstanceGuid
		{
			get => this.ViewState.GetValue(AspireViewStateKeys.Guid, Guid.Empty);
			private set => this.ViewState.SetValue(AspireViewStateKeys.Guid, value);
		}

		protected override void OnLoad(EventArgs e)
		{
			if (this.InstanceGuid == Guid.Empty)
				this.InstanceGuid = Guid.NewGuid();
			base.OnLoad(e);
		}

		protected override void CreateChildControls()
		{
			this._hf = new System.Web.UI.WebControls.HiddenField { ID = "hf", Value = null };
			this.Controls.Add(this._hf);
			base.CreateChildControls();
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (this.Enabled)
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), this.ClientID, $"initFileUpload(\"{this.ClientID}\",\"{this.InstanceGuid}\");", true);
			base.OnPreRender(e);
		}

		protected override void Render(HtmlTextWriter writer)
		{
			this._hf.RenderControl(writer);
			if (!this.Enabled)
				writer.AddAttribute(HtmlTextWriterAttribute.Disabled, "disabled");
			writer.AddAttribute(HtmlTextWriterAttribute.Class, "btn btn-default");
			writer.AddAttribute(HtmlTextWriterAttribute.Type, "button");
			writer.AddAttribute(HtmlTextWriterAttribute.Id, $"{this.ClientID}_btn");
			writer.RenderBeginTag(HtmlTextWriterTag.Button);
			switch (this.MaxNumberOfFiles)
			{
				case 0:
					throw new InvalidOperationException();
				case 1:
					writer.WriteEncodedText("Select File");
					break;
				default:
					writer.WriteEncodedText("Select Files");
					break;
			}
			writer.RenderEndTag();
			if (!this.Enabled)
				return;
			var fileUpload = new FileUpload
			{
				CssClass = "AspireFileUpload hidden",
				ID = $"{this.ClientID}_fu",
				ClientIDMode = ClientIDMode.Static,
				AllowMultiple = this.MaxNumberOfFiles > 1,
				Attributes = { ["accept"] = this.AllowedFileTypes }
			};
			if (string.IsNullOrWhiteSpace(this.AllowedFileTypes))
				throw new InvalidOperationException($"{nameof(this.AllowedFileTypes)} is not provided.");
			fileUpload.Attributes.Add($"data-hfid", this._hf.ClientID);
			fileUpload.Attributes.Add($"data-{nameof(this.AllowedFileTypes)}".ToLower(), this.AllowedFileTypes.TrimAndCannotEmptyAndMustBeLowerCase());
			fileUpload.Attributes.Add($"data-{nameof(this.MaxNumberOfFiles)}".ToLower(), this.MaxNumberOfFiles.ToString());
			fileUpload.Attributes.Add($"data-{nameof(this.MaxFileSize)}".ToLower(), this.MaxFileSize.ToString());
			fileUpload.RenderControl(writer);
			writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
			writer.AddAttribute(HtmlTextWriterAttribute.Id, $"{this.ClientID}_div");
			writer.RenderBeginTag(HtmlTextWriterTag.Div);
			writer.RenderEndTag();
		}
	}
}
