﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	public class CompareValidator : Aspire.Lib.WebControls.CustomValidator
	{
		protected override void OnInit(EventArgs e)
		{
			this.SetFocusOnError = true;
			this.Display = ValidatorDisplay.Dynamic;
			base.OnInit(e);
		}

		[DefaultValue("")]
		[IDReferenceProperty]
		[Themeable(false)]
		[TypeConverter(typeof(ValidatedControlConverter))]
		public string ControlToCompare
		{
			get => (string)this.ViewState.GetValue(AspireViewStateKeys.ControlToCompare, "");
			set => this.ViewState.SetValue(AspireViewStateKeys.ControlToCompare, value);
		}

		[DefaultValue(ValidationDataType.String)]
		public ValidationDataType Type
		{
			get => (ValidationDataType)this.ViewState.GetValue(AspireViewStateKeys.ValidationDataType, ValidationDataType.String);
			set => this.ViewState.SetValue(AspireViewStateKeys.ValidationDataType, value);
		}

		[DefaultValue(true)]
		public bool CaseSensitiveComparison
		{
			get => (bool)this.ViewState.GetValue(AspireViewStateKeys.CaseSensitiveComparison, true);
			set => this.ViewState.SetValue(AspireViewStateKeys.CaseSensitiveComparison, value);
		}

		[DefaultValue(ValidationCompareOperator.Equal)]
		public ValidationCompareOperator Operator
		{
			get => (ValidationCompareOperator)this.ViewState.GetValue(AspireViewStateKeys.ValidationCompareOperator, ValidationCompareOperator.Equal);
			set => this.ViewState.SetValue(AspireViewStateKeys.ValidationCompareOperator, value);
		}

		[DefaultValue(DateTimeValidator.DateTimeFormats.ShortDate)]
		public DateTimeValidator.DateTimeFormats DateTimeFormat
		{
			get => (DateTimeValidator.DateTimeFormats)this.ViewState.GetValue(AspireViewStateKeys.DateFormat, DateTimeValidator.DateTimeFormats.ShortDate);
			set => this.ViewState.SetValue(AspireViewStateKeys.DateFormat, value);
		}

		[DefaultValue(null)]
		public string CustomDateTimeFormat
		{
			get => (string)this.ViewState.GetValue(AspireViewStateKeys.CustomFormat, null);
			set => this.ViewState.SetValue(AspireViewStateKeys.CustomFormat, value);
		}

		protected virtual string GetDateTimeFormat()
		{
			var cultureInfo = CultureInfo.CurrentCulture;
			switch (this.DateTimeFormat)
			{
				case DateTimeValidator.DateTimeFormats.LongDateLongTime:
					return $"{cultureInfo.DateTimeFormat.LongDatePattern} {cultureInfo.DateTimeFormat.LongTimePattern}";
				case DateTimeValidator.DateTimeFormats.LongDateShortTime:
					return $"{cultureInfo.DateTimeFormat.LongDatePattern} {cultureInfo.DateTimeFormat.ShortTimePattern}";
				case DateTimeValidator.DateTimeFormats.LongDate:
					return $"{cultureInfo.DateTimeFormat.LongDatePattern}";
				case DateTimeValidator.DateTimeFormats.LongTime:
					return $"{cultureInfo.DateTimeFormat.LongTimePattern}";
				case DateTimeValidator.DateTimeFormats.ShortDate:
					return $"{cultureInfo.DateTimeFormat.ShortDatePattern}";
				case DateTimeValidator.DateTimeFormats.ShortTime:
					return $"{cultureInfo.DateTimeFormat.ShortTimePattern}";
				case DateTimeValidator.DateTimeFormats.ShortDateTime:
					return $"{cultureInfo.DateTimeFormat.ShortDatePattern} {cultureInfo.DateTimeFormat.ShortTimePattern}";
				case DateTimeValidator.DateTimeFormats.Custom:
					if (string.IsNullOrWhiteSpace(this.CustomDateTimeFormat))
						throw new InvalidOperationException("Provide CustomDateTimeFormat. ID:" + this.ClientID);
					return this.CustomDateTimeFormat;
				default:
					throw new NotImplementedEnumException(this.DateTimeFormat);
			}
		}

		private string GetControlToCompareValue()
		{
			var control = this.FindControl(this.ControlToCompare);
			if (control is Aspire.Lib.WebControls.TextBox atb)
				return atb.Text;
			if (control is System.Web.UI.WebControls.TextBox tb)
				return tb.Text;
			if (control is System.Web.UI.WebControls.DropDownList ddl)
				return ddl.SelectedValue;
			if (control is HiddenField hf)
				return hf.Value;
			throw new NotSupportedException(control.GetType() + " not supported.");
		}

		protected override bool OnServerValidate(string value)
		{
			if (!this.ValidateEmptyText && string.IsNullOrEmpty(value))
				return true;
			var stringValue = value;
			var integerValue = stringValue.TryToInt();
			var doubleValue = stringValue.TryToDouble();
			var dateTimeValue = stringValue.TryToDateTime();
			var compareStringValue = this.GetControlToCompareValue();
			var compareIntegerValue = compareStringValue.TryToInt();
			var compareDoubleValue = compareStringValue.TryToDouble();
			var compareDateTimeValue = compareStringValue.TryToDateTime(this.GetDateTimeFormat());
			switch (this.Operator)
			{
				case ValidationCompareOperator.Equal:
					switch (this.Type)
					{
						case ValidationDataType.String:
							var stringComparisonType = this.CaseSensitiveComparison ? StringComparison.InvariantCulture : StringComparison.InvariantCultureIgnoreCase;
							return string.Compare(stringValue, compareStringValue, stringComparisonType) == 0;
						case ValidationDataType.Integer:
							return integerValue != null && integerValue == compareIntegerValue;
						case ValidationDataType.Double:
							return doubleValue != null && Equals(doubleValue, compareDoubleValue);
						case ValidationDataType.Date:
							return dateTimeValue != null && dateTimeValue == compareDateTimeValue;
						case ValidationDataType.Currency:
							throw new NotSupportedException();
						default:
							throw new ArgumentOutOfRangeException();
					}
				case ValidationCompareOperator.NotEqual:
					switch (this.Type)
					{
						case ValidationDataType.String:
							var stringComparisonType = this.CaseSensitiveComparison ? StringComparison.InvariantCulture : StringComparison.InvariantCultureIgnoreCase;
							return string.Compare(stringValue, compareStringValue, stringComparisonType) != 0;
						case ValidationDataType.Integer:
							return integerValue != null && integerValue != compareIntegerValue;
						case ValidationDataType.Double:
							return doubleValue != null && !Equals(doubleValue, compareDoubleValue);
						case ValidationDataType.Date:
							return dateTimeValue != null && dateTimeValue != compareDateTimeValue;
						case ValidationDataType.Currency:
							throw new NotSupportedException();
						default:
							throw new ArgumentOutOfRangeException();
					}
				case ValidationCompareOperator.GreaterThan:
					switch (this.Type)
					{
						case ValidationDataType.String:
							var stringComparisonType = this.CaseSensitiveComparison ? StringComparison.InvariantCulture : StringComparison.InvariantCultureIgnoreCase;
							return string.Compare(stringValue, compareStringValue, stringComparisonType) > 0;
						case ValidationDataType.Integer:
							return integerValue != null && integerValue > compareIntegerValue;
						case ValidationDataType.Double:
							return doubleValue != null && doubleValue > compareDoubleValue;
						case ValidationDataType.Date:
							return dateTimeValue != null && dateTimeValue > compareDateTimeValue;
						case ValidationDataType.Currency:
							throw new NotSupportedException();
						default:
							throw new ArgumentOutOfRangeException();
					}
				case ValidationCompareOperator.GreaterThanEqual:
					switch (this.Type)
					{
						case ValidationDataType.String:
							var stringComparisonType = this.CaseSensitiveComparison ? StringComparison.InvariantCulture : StringComparison.InvariantCultureIgnoreCase;
							return string.Compare(stringValue, compareStringValue, stringComparisonType) >= 0;
						case ValidationDataType.Integer:
							return integerValue != null && integerValue >= compareIntegerValue;
						case ValidationDataType.Double:
							return doubleValue != null && doubleValue >= compareDoubleValue;
						case ValidationDataType.Date:
							return dateTimeValue != null && dateTimeValue >= compareDateTimeValue;
						case ValidationDataType.Currency:
							throw new NotSupportedException();
						default:
							throw new ArgumentOutOfRangeException();
					}
				case ValidationCompareOperator.LessThan:
					switch (this.Type)
					{
						case ValidationDataType.String:
							var stringComparisonType = this.CaseSensitiveComparison ? StringComparison.InvariantCulture : StringComparison.InvariantCultureIgnoreCase;
							return string.Compare(stringValue, compareStringValue, stringComparisonType) < 0;
						case ValidationDataType.Integer:
							return integerValue != null && integerValue < compareIntegerValue;
						case ValidationDataType.Double:
							return doubleValue != null && doubleValue < compareDoubleValue;
						case ValidationDataType.Date:
							return dateTimeValue != null && dateTimeValue < compareDateTimeValue;
						case ValidationDataType.Currency:
							throw new NotSupportedException();
						default:
							throw new ArgumentOutOfRangeException();
					}
				case ValidationCompareOperator.LessThanEqual:
					switch (this.Type)
					{
						case ValidationDataType.String:
							var stringComparisonType = this.CaseSensitiveComparison ? StringComparison.InvariantCulture : StringComparison.InvariantCultureIgnoreCase;
							return string.Compare(stringValue, compareStringValue, stringComparisonType) <= 0;
						case ValidationDataType.Integer:
							return integerValue != null && integerValue <= compareIntegerValue;
						case ValidationDataType.Double:
							return doubleValue != null && doubleValue <= compareDoubleValue;
						case ValidationDataType.Date:
							return dateTimeValue != null && dateTimeValue <= compareDateTimeValue;
						case ValidationDataType.Currency:
							throw new NotSupportedException();
						default:
							throw new ArgumentOutOfRangeException();
					}
				case ValidationCompareOperator.DataTypeCheck:
					switch (this.Type)
					{
						case ValidationDataType.String:
							return true;
						case ValidationDataType.Integer:
							return integerValue != null;
						case ValidationDataType.Double:
							return doubleValue != null;
						case ValidationDataType.Date:
							return dateTimeValue != null;
						case ValidationDataType.Currency:
							throw new NotSupportedException();
						default:
							throw new ArgumentOutOfRangeException();
					}
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.ControlToValidate))
				throw new InvalidOperationException("Provide ControlToValidate");
			if (string.IsNullOrWhiteSpace(this.ControlToCompare))
				throw new InvalidOperationException("Provide ControlToCompare");
			var controlToCompare = this.FindControl(this.ControlToCompare);
			if (controlToCompare == null)
				throw new InvalidOperationException("Invalid ControlToCompare reference. ControlToCompare: " + this.ControlToCompare);
			this.Attributes.Add("data-ControlToCompareID".ToLower(), controlToCompare.ClientID);
			this.Attributes.Add("data-Operator".ToLower(), this.Operator.ToString());
			this.Attributes.Add("data-Type".ToLower(), this.Type.ToString());
			this.Attributes.Add("data-DateTimeFormat".ToLower(), this.GetDateTimeFormat());
			this.Attributes.Add("data-CaseSensitiveComparison".ToLower(), this.CaseSensitiveComparison.ToString());
			this.Attributes.Add("data-ComparisonErrorMessage".ToLower(), this.ErrorMessage);
			this.ClientValidationFunction = "ValidateCompare";
			base.OnPreRender(e);
		}
	}
}