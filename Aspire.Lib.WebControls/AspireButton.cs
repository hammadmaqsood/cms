﻿using System;
using System.ComponentModel;
using System.Web.UI;

namespace Aspire.Lib.WebControls
{
	public class AspireButton : Aspire.Lib.WebControls.AspireLinkButton
	{
		public enum ButtonTypes
		{
			Default,
			Primary,
			Success,
			Warning,
			Danger,
			Info
		}

		[DefaultValue(ButtonTypes.Primary)]
		public virtual ButtonTypes ButtonType
		{
			get { return this.ViewState.GetValue(AspireViewStateKeys.ButtonType, ButtonTypes.Primary); }
			set { this.ViewState.SetValue(AspireViewStateKeys.ButtonType, value); }
		}

		public enum ButtonSizes
		{
			Default,
			Large,
			Small,
			ExtraSmall
		}

		[DefaultValue(ButtonTypes.Default)]
		public virtual ButtonSizes ButtonSize
		{
			get { return this.ViewState.GetValue(AspireViewStateKeys.ButtonSize, ButtonSizes.Default); }
			set { this.ViewState.SetValue(AspireViewStateKeys.ButtonSize, value); }
		}

		protected override void Render(HtmlTextWriter writer)
		{
			switch (this.ButtonSize)
			{
				case ButtonSizes.Default:
					switch (this.ButtonType)
					{
						case ButtonTypes.Default:
							this.AddCssClass("btn btn-default");
							break;
						case ButtonTypes.Primary:
							this.AddCssClass("btn btn-primary");
							break;
						case ButtonTypes.Success:
							this.AddCssClass("btn btn-success");
							break;
						case ButtonTypes.Warning:
							this.AddCssClass("btn btn-warning");
							break;
						case ButtonTypes.Danger:
							this.AddCssClass("btn btn-danger");
							break;
						case ButtonTypes.Info:
							this.AddCssClass("btn btn-info");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;

				case ButtonSizes.Large:
					switch (this.ButtonType)
					{
						case ButtonTypes.Default:
							this.AddCssClass("btn btn-default btn-lg");
							break;
						case ButtonTypes.Primary:
							this.AddCssClass("btn btn-primary btn-lg");
							break;
						case ButtonTypes.Success:
							this.AddCssClass("btn btn-success btn-lg");
							break;
						case ButtonTypes.Warning:
							this.AddCssClass("btn btn-warning btn-lg");
							break;
						case ButtonTypes.Danger:
							this.AddCssClass("btn btn-danger btn-lg");
							break;
						case ButtonTypes.Info:
							this.AddCssClass("btn btn-info btn-lg");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;

				case ButtonSizes.Small:
					switch (this.ButtonType)
					{
						case ButtonTypes.Default:
							this.AddCssClass("btn btn-default btn-sm");
							break;
						case ButtonTypes.Primary:
							this.AddCssClass("btn btn-primary btn-sm");
							break;
						case ButtonTypes.Success:
							this.AddCssClass("btn btn-success btn-sm");
							break;
						case ButtonTypes.Warning:
							this.AddCssClass("btn btn-warning btn-sm");
							break;
						case ButtonTypes.Danger:
							this.AddCssClass("btn btn-danger btn-sm");
							break;
						case ButtonTypes.Info:
							this.AddCssClass("btn btn-info btn-sm");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
				case ButtonSizes.ExtraSmall:
					switch (this.ButtonType)
					{
						case ButtonTypes.Default:
							this.AddCssClass("btn btn-default btn-xs");
							break;
						case ButtonTypes.Primary:
							this.AddCssClass("btn btn-primary btn-xs");
							break;
						case ButtonTypes.Success:
							this.AddCssClass("btn btn-success btn-xs");
							break;
						case ButtonTypes.Warning:
							this.AddCssClass("btn btn-warning btn-xs");
							break;
						case ButtonTypes.Danger:
							this.AddCssClass("btn btn-danger btn-xs");
							break;
						case ButtonTypes.Info:
							this.AddCssClass("btn btn-info btn-xs");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			base.Render(writer);
		}
	}
}