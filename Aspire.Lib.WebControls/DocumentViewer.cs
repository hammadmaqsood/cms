﻿using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	public sealed class DocumentViewer : Control
	{
		public string DocumentURL
		{
			get => (string)this.ViewState.GetValue(AspireViewStateKeys.DocumentURL, null);
			set => this.ViewState.SetValue(AspireViewStateKeys.DocumentURL, value);
		}

		[DefaultValue(typeof(Unit), "")]
		public Unit Width
		{
			get => (Unit)this.ViewState.GetValue(AspireViewStateKeys.Width, Unit.Empty);
			set => this.ViewState.SetValue(AspireViewStateKeys.Width, value);
		}

		[DefaultValue(typeof(Unit), "")]
		public Unit Height
		{
			get => (Unit)this.ViewState.GetValue(AspireViewStateKeys.Height, Unit.Empty);
			set => this.ViewState.SetValue(AspireViewStateKeys.Height, value);
		}

		protected override void Render(HtmlTextWriter writer)
		{
			writer.AddAttribute(HtmlTextWriterAttribute.Class, "DocumentViewer");
			writer.RenderBeginTag(HtmlTextWriterTag.Div);
			{

				writer.AddAttribute(HtmlTextWriterAttribute.Width, this.Width.ToString());
				writer.AddAttribute(HtmlTextWriterAttribute.Height, this.Height.ToString());
				writer.AddAttribute(HtmlTextWriterAttribute.Name, this.ClientID);
				writer.AddAttribute(HtmlTextWriterAttribute.Src, this.DocumentURL);
				writer.RenderBeginTag(HtmlTextWriterTag.Iframe);
				writer.RenderEndTag();
			}
			writer.RenderEndTag();
		}
	}
}
