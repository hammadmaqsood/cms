using System;
using System.ComponentModel;
using System.Web.UI;

namespace Aspire.Lib.WebControls
{
	public class AspireStringValidator : StringValidator
	{
		[DefaultValue("has-error")]
		[CssClassProperty]
		public override string HighlightCssClass
		{
			get => this.ViewState.GetValue(AspireViewStateKeys.HighlightCssClass, "has-error");
			set => this.ViewState.SetValue(AspireViewStateKeys.HighlightCssClass, value);
		}

		protected override void OnPreRender(EventArgs e)
		{
			this.AddCssClass("text-danger");
			base.OnPreRender(e);
		}
	}
}