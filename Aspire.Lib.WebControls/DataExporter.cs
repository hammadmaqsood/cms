﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	public sealed class DataExporter : UserControl
	{
		private AspireLinkButton _btnExportToExcel;
		private AspireLinkButton _btnExportToCSV;
		private HtmlGenericControl _liExportToExcel;
		private HtmlGenericControl _liExportToCSV;

		public bool ExportToExcel
		{
			get
			{
				this.EnsureChildControls();
				return this._liExportToExcel.Visible;
			}
			set
			{
				this.EnsureChildControls();
				this._liExportToExcel.Visible = value;
			}
		}

		public bool ExportToCSV
		{
			get
			{
				this.EnsureChildControls();
				return this._liExportToCSV.Visible;
			}
			set
			{
				this.EnsureChildControls();
				this._liExportToCSV.Visible = value;
			}
		}

		protected override void CreateChildControls()
		{
			var panel = new Panel
			{
				CssClass = "row",
				Controls =
				{
					new Panel
					{
						CssClass = "col-md-12",
						Controls =
						{
							new Panel
							{
								CssClass = "btn-group pull-right",
								Controls =
								{
									new HtmlGenericControl("button")
									{
										Attributes =
										{
											["type"] = "button",
											["class"] = "btn btn-default dropdown-toggle",
											["data-toggle"] = "dropdown",
											["aria-haspopup"] = "true",
											["aria-expanded"] = "false"
										},
										InnerHtml = "Export <span class=\"caret\"></span>",
									},
									new HtmlGenericControl("ul")
									{
										Attributes =
										{
											["class"] = "dropdown-menu dropdown-menu-right",
										},
										Controls =
										{
											(this._liExportToExcel = new HtmlGenericControl("li")
											{
												Controls =
												{
													(this._btnExportToExcel = new AspireLinkButton {CausesValidation = false, DisableOnClick = false, Text = "Export To Excel"}),
												}
											}),
											(this._liExportToCSV = new HtmlGenericControl("li")
											{
												Controls =
												{
													(this._btnExportToCSV = new AspireLinkButton {CausesValidation = false, DisableOnClick = false,  Text = "Export To CSV"}),
												}
											}),
										}
									},
								}
							}
						}
					}
				}
			};
			this._btnExportToExcel.Click += (sender, args) => this.Export(this._btnExportToExcel, ExportTypes.Excel);
			this._btnExportToCSV.Click += (sender, args) => this.Export(this._btnExportToCSV, ExportTypes.CSV);
			this.Controls.Add(panel);
			base.CreateChildControls();
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (this.ExportToCSV || this.ExportToExcel)
				base.Render(writer);
		}

		public enum ExportTypes
		{
			Excel,
			CSV
		}

		public sealed class ExportEventArgs : EventArgs
		{
			public ExportTypes ExportType { get; }
			public ExportEventArgs(ExportTypes exportType) => this.ExportType = exportType;
		}

		public delegate void ExportingHandler(object sender, ExportEventArgs args);

		public event ExportingHandler Exporting;

		private void Export(AspireLinkButton btn, ExportTypes exportType)
		{
			if (this.Exporting != null)
				this.Exporting(btn, new ExportEventArgs(exportType));
			else
				throw new InvalidOperationException("Exporting event not implemented. ID: " + this.ClientID);
		}
	}
}
