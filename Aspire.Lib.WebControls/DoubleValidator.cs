﻿using System.ComponentModel;

namespace Aspire.Lib.WebControls
{
	public class DoubleValidator : NumberValidator<double>
	{
		protected override double DefaultMaxValue => double.MaxValue;

		protected override double DefaultMinValue => double.MinValue;

		protected override string GetClientValidationFunction()
		{
			return "ValidateDouble";
		}

		[DefaultValue(double.MaxValue)]
		public override double MaxValue
		{
			get { return (double)this.ViewState.GetValue(AspireViewStateKeys.MaxValue, DefaultMaxValue); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MaxValue, value); }
		}

		[DefaultValue(double.MinValue)]
		public override double MinValue
		{
			get { return (double)this.ViewState.GetValue(AspireViewStateKeys.MinValue, DefaultMinValue); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MinValue, value); }
		}
	}
}