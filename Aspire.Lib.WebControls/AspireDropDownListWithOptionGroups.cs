using System;

namespace Aspire.Lib.WebControls
{
	public class AspireDropDownListWithOptionGroups : DropDownListWithOptionGroups
	{
		protected override void OnPreRender(EventArgs e)
		{
			if (this.CausesValidation == false && this.AutoPostBack)
				this.Attributes.Add("onchange", "window.Page_BlockSubmit=false;");
			this.AddCssClass("form-control");
			base.OnPreRender(e);
		}
	}
}