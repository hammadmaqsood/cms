﻿using System;
using System.ComponentModel;
using System.Web.UI;

namespace Aspire.Lib.WebControls
{
	public sealed class AspireGlyphicon : Control
	{
		public enum RenderTags { Span, I }

		[DefaultValue(RenderTags.Span)]
		public RenderTags RenderTag
		{
			get { return (RenderTags)this.ViewState.GetValue(AspireViewStateKeys.RenderTag, RenderTags.Span); }
			set { this.ViewState.SetValue(AspireViewStateKeys.RenderTag, value); }
		}

		public AspireGlyphicons Icon
		{
			get { return (AspireGlyphicons)this.ViewState.GetValue(AspireViewStateKeys.Icon, AspireGlyphicons.None); }
			set { this.ViewState.SetValue(AspireViewStateKeys.Icon, value); }
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (this.Icon == AspireGlyphicons.None)
				throw new InvalidOperationException("Icon not defined.");
			writer.AddAttribute(HtmlTextWriterAttribute.Class, this.Icon.ToCssClass());
			switch (this.RenderTag)
			{
				case RenderTags.Span:
					writer.RenderBeginTag(HtmlTextWriterTag.Span);
					break;
				case RenderTags.I:
					writer.RenderBeginTag(HtmlTextWriterTag.I);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			writer.RenderEndTag();
		}
	}
}