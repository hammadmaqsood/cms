﻿using Aspire.Lib.Helpers;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	public sealed class ReCaptcha : WebControl
	{
		protected override HtmlTextWriterTag TagKey => HtmlTextWriterTag.Div;

		protected override void OnPreRender(EventArgs e)
		{
			if (!this.Page.ClientScript.IsClientScriptIncludeRegistered("recaptcha"))
				this.Page.ClientScript.RegisterClientScriptInclude("recaptcha", "https://www.google.com/recaptcha/api.js");
			this.Attributes.Add("class", "g-recaptcha");
			var publicKey = Configurations.Current.AppSettings.RecaptchaPublic;
			if (string.IsNullOrWhiteSpace(publicKey))
				throw new InvalidOperationException("ReCaptcha.Public AppSettings not found in Web.config.");
			this.Attributes.Add("data-sitekey", publicKey);
			this.Attributes.Add("data-callback", "ValidateRecaptcha");
			base.OnPreRender(e);
		}
	}
}
