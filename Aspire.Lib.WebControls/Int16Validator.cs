﻿using System.ComponentModel;

namespace Aspire.Lib.WebControls
{
	public class Int16Validator : NumberValidator<short>
	{
		protected override short DefaultMaxValue => short.MaxValue;

		protected override short DefaultMinValue => short.MinValue;

		protected override string GetClientValidationFunction()
		{
			return "ValidateInteger";
		}

		[DefaultValue(short.MaxValue)]
		public override short MaxValue
		{
			get { return (short)this.ViewState.GetValue(AspireViewStateKeys.MaxValue, DefaultMaxValue); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MaxValue, value); }
		}

		[DefaultValue(short.MinValue)]
		public override short MinValue
		{
			get { return (short)this.ViewState.GetValue(AspireViewStateKeys.MinValue, DefaultMinValue); }
			set { this.ViewState.SetValue(AspireViewStateKeys.MinValue, value); }
		}
	}
}