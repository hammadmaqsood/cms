using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	public class DropDownListWithOptionGroups : DropDownList
	{
		private const string OptionGroupTag = "optgroup";
		private const string OptionTag = "option";

		protected override void RenderContents(HtmlTextWriter writer)
		{
			var items = this.Items;
			var count = items.Count;
			if (count <= 0) return;
			var flag = false;
			string previousOptGroup = null;
			for (var i = 0; i < count; i++)
			{
				string optgroupLabel = null;
				var item = items[i];
				if (!item.Enabled)
					continue;
				if (item.Attributes.Count > 0 && item.Attributes[OptionGroupTag] != null)
				{
					optgroupLabel = item.Attributes[OptionGroupTag];
					if (previousOptGroup != optgroupLabel)
					{
						if (previousOptGroup != null)
							writer.WriteEndTag(OptionGroupTag);
						writer.WriteBeginTag(OptionGroupTag);
						if (!string.IsNullOrEmpty(optgroupLabel))
							writer.WriteAttribute("label", optgroupLabel);
						writer.Write('>');
					}
					item.Attributes.Remove(OptionGroupTag);
					previousOptGroup = optgroupLabel;
				}
				else
				{
					if (previousOptGroup != null)
						writer.WriteEndTag(OptionGroupTag);
					previousOptGroup = null;
				}

				writer.WriteBeginTag(OptionTag);
				if (item.Selected)
				{
					if (flag)
						this.VerifyMultiSelect();
					flag = true;
					writer.WriteAttribute("selected", "selected");
				}
				writer.WriteAttribute("value", item.Value, true);
				if (item.Attributes.Count > 0)
					item.Attributes.Render(writer);
				if (optgroupLabel != null)
					item.Attributes.Add(OptionGroupTag, optgroupLabel);
				this.Page?.ClientScript.RegisterForEventValidation(this.UniqueID, item.Value);

				writer.Write('>');
				HttpUtility.HtmlEncode(item.Text, writer);
				writer.WriteEndTag(OptionTag);
				writer.WriteLine();
				if (i == count - 1 && previousOptGroup != null)
					writer.WriteEndTag(OptionGroupTag);
			}
		}

		protected override object SaveViewState()
		{
			var state = new object[this.Items.Count + 1];
			var baseState = base.SaveViewState();
			state[0] = baseState;
			var itemHasAttributes = false;
			for (var i = 0; i < this.Items.Count; i++)
			{
				if (this.Items[i].Attributes.Count <= 0) continue;
				itemHasAttributes = true;
				var attributes = new object[this.Items[i].Attributes.Count * 2];
				var k = 0;
				foreach (string key in this.Items[i].Attributes.Keys)
				{
					attributes[k] = key;
					k++;
					attributes[k] = this.Items[i].Attributes[key];
					k++;
				}
				state[i + 1] = attributes;
			}
			return itemHasAttributes ? state : baseState;
		}

		protected override void LoadViewState(object savedState)
		{
			if (savedState == null)
				return;
			var savedStateType = savedState.GetType();
			if (savedStateType.GetElementType() != null && savedStateType.GetElementType() == typeof(object))
			{
				var state = (object[])savedState;
				base.LoadViewState(state[0]);
				for (var i = 1; i < state.Length; i++)
					if (state[i] != null)
					{
						var attributes = (object[])state[i];
						for (var k = 0; k < attributes.Length; k += 2)
							this.Items[i - 1].Attributes.Add(attributes[k].ToString(), attributes[k + 1].ToString());
					}
			}
			else
				base.LoadViewState(savedState);
		}

		public void AddItem(string text, string value, string groupName)
		{
			var li = new ListItem(text, value);
			li.Attributes.Add(OptionGroupTag, groupName);
			this.Items.Add(li);
		}
	}
}