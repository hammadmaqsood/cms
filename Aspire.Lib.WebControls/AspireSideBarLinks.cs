using System;
using System.Collections.Generic;

namespace Aspire.Lib.WebControls
{
	[Serializable]
	public sealed class AspireSideBarLinks
	{
		private readonly List<AspireSideBarLink> _links;
		public IReadOnlyList<AspireSideBarLink> Links => this._links;
		public AspireSideBarLink HomeSideBarLink { get; }

		public AspireSideBarLinks(AspireSideBarLink homeAspireSideBarLink)
		{
			if (homeAspireSideBarLink == null)
				throw new ArgumentNullException(nameof(homeAspireSideBarLink));
			if (homeAspireSideBarLink.Visible == false)
				throw new ArgumentException(nameof(homeAspireSideBarLink));
			this.HomeSideBarLink = homeAspireSideBarLink;
			this._links = new List<AspireSideBarLink>();
		}

		public string ToHtml()
		{
			var index = 0;
			var html = @"<aside id=""sideBar""><div class=""list-group panel"" id=""sideMenuList"">";
			//html += this.HomeSideBarLink.ToHtml("sideMenuList", index++);
			for (var i = 0; i < this.Links.Count; i++)
				html += this.Links[i].ToHtml("sideMenuList", index++);
			html += "</div></aside>";
			return html;
		}

		public AspireSideBarLink AddLink(AspireSideBarLink sideBarLink)
		{
			this._links.Add(sideBarLink);
			return sideBarLink;
		}

		public AspireSideBarLink AddLink(AspireSideBarLink parent, Guid aspirePageGuid, string text, string navigateUrl, bool visible, string target, string cssClass)
		{
			return this.AddLink(new AspireSideBarLink(parent, aspirePageGuid, text, navigateUrl, visible, target, cssClass));
		}

		public string GetBreadcrumbHtml(Guid aspirePageGuid)
		{
			var sideBarLink = this.FindSidebarLink(aspirePageGuid);
			if (sideBarLink == null)
				return null;
			var breadcrumbLinks = new List<Breadcrumbs.AspireBreadcrumbLink>();
			while (sideBarLink != null)
			{
				var breadcrumbLink = new Breadcrumbs.AspireBreadcrumbLink(sideBarLink.Text, sideBarLink.NavigateUrl, false);
				breadcrumbLinks.Add(breadcrumbLink);
				sideBarLink = sideBarLink.ParentAspireSideBarLink;
			}

			var homeLink = new Breadcrumbs.AspireBreadcrumbLink(this.HomeSideBarLink.Text, this.HomeSideBarLink.NavigateUrl, true);
			var breadcrumbs = new Breadcrumbs();
			breadcrumbs.AddLink(homeLink);
			breadcrumbLinks.Reverse();
			foreach (var link in breadcrumbLinks)
				breadcrumbs.AddLink(link);
			return breadcrumbs.ToHtml();
		}

		public AspireSideBarLink FindSidebarLink(Guid aspirePageGuid)
		{
			foreach (var aspireSideBarLink in this.Links)
			{
				var link = aspireSideBarLink.FindSidebarLink(aspirePageGuid);
				if (link != null)
					return link;
			}
			return null;
		}

		public void AddLinks(List<AspireSideBarLink> sideBarLinks)
		{
			foreach (var aspireSideBarLink in sideBarLinks)
				this.AddLink(aspireSideBarLink);
		}

		public void SetDisplayIndexes()
		{
			byte displayIndex = 0;
			foreach (var aspireSideBarLink in this.Links)
				aspireSideBarLink.SetDisplayIndexes(displayIndex++);
		}
	}
}