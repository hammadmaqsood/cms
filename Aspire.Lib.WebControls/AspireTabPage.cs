﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Lib.WebControls
{
	[PersistChildren(true)]
	[ParseChildren(false)]
	public class AspireTabPage : WebControl
	{
		[DefaultValue("")]
		[Localizable(true)]
		public string HeaderText
		{
			get { return (string)this.ViewState.GetValue(AspireViewStateKeys.HeaderText, ""); }
			set { this.ViewState.SetValue(AspireViewStateKeys.HeaderText, value); }
		}

		[DefaultValue("")]
		[Localizable(true)]
		[CssClassProperty]
		public string Icon
		{
			get { return (string)this.ViewState.GetValue(AspireViewStateKeys.Icon, ""); }
			set { this.ViewState.SetValue(AspireViewStateKeys.Icon, value); }
		}

		[DefaultValue(false)]
		public bool Active
		{
			get { return (bool)this.ViewState.GetValue(AspireViewStateKeys.Active, false); }
			set { this.ViewState.SetValue(AspireViewStateKeys.Active, value); }
		}

		public AspireTabPage()
			: base(HtmlTextWriterTag.Div)
		{
		}

		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			writer.AddAttribute("role", "tab-panel");
			writer.AddAttribute("aria-labelled-by", this.ClientID + "-tab");
			base.AddAttributesToRender(writer);
		}

		protected override void OnPreRender(EventArgs e)
		{
			//<div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">
			this.AddCssClasses("tab-pane", "fade");
			if (this.Active)
				this.AddCssClasses("active", "in");
			base.OnPreRender(e);
		}

		public void RenderHeader(HtmlTextWriter writer)
		{
			/*<li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Home</a></li>*/
			writer.AddAttribute("role", "presentation");
			if (this.Active)
				writer.AddAttribute("class", "active");
			writer.RenderBeginTag(HtmlTextWriterTag.Li);
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Href, "#" + this.ClientID);
				writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID + "-tab");
				writer.AddAttribute("role", "tab");
				writer.AddAttribute("data-toggle", "tab");
				writer.AddAttribute("aria-controls", this.ClientID);
				writer.AddAttribute("aria-expanded", "true");
				writer.RenderBeginTag(HtmlTextWriterTag.A);
				{
					if (!string.IsNullOrWhiteSpace(this.Icon))
					{
						writer.AddAttribute(HtmlTextWriterAttribute.Class, this.Icon);
						writer.RenderBeginTag(HtmlTextWriterTag.I);
						writer.RenderEndTag();
					}
				}
				writer.WriteEncodedText(this.HeaderText);
				writer.RenderEndTag();
			}
			writer.RenderEndTag();
		}
	}
}