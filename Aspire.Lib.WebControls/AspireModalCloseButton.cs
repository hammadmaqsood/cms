﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace Aspire.Lib.WebControls
{
	public sealed class AspireModalCloseButton : HtmlButton
	{
		public enum ButtonTypes
		{
			HeaderCrossButton, FooterCancelButton
		}

		[DefaultValue(ButtonTypes.FooterCancelButton)]
		public ButtonTypes ButtonType
		{
			get => this.ViewState.GetValue(AspireViewStateKeys.ButtonType, ButtonTypes.FooterCancelButton);
			set => this.ViewState.SetValue(AspireViewStateKeys.ButtonType, value);
		}

		protected override void OnPreRender(EventArgs e)
		{
			/* <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
			switch (this.ButtonType)
			{
				case ButtonTypes.HeaderCrossButton:
					this.Attributes.Add("class", "close");
					this.Attributes.Add("aria-label", "Close");
					this.Controls.Clear();
					this.Controls.Add(new LiteralControl("<span aria-hidden=\"true\">&times;</span>"));
					break;
				case ButtonTypes.FooterCancelButton:
					this.Attributes.Add("class", "btn btn-default");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.Attributes.Add("type", "button");
			this.Attributes.Add("data-dismiss", "modal");
			base.OnPreRender(e);
		}
	}
}
