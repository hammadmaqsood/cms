﻿using System;
using System.Security.Cryptography;

namespace Aspire.Lib.PasswordEncryptor
{
	public static class PasswordHash
	{
		private const int SaltByteSize = 24;
		private const int HashByteSize = 24;
		private const int PBKDF2Iterations = 1000;
		private const int IterationIndex = 0;
		private const int SaltIndex = 1;
		private const int PBKDF2Index = 2;

		public static string CreateHash(string password)
		{
			var salt = new byte[SaltByteSize];
			using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
			{
				rngCryptoServiceProvider.GetBytes(salt);
			}
			var hash = PBKDF2(password, salt, PBKDF2Iterations, HashByteSize);
			return PBKDF2Iterations + ":" + Convert.ToBase64String(salt) + ":" + Convert.ToBase64String(hash);
		}

		public static bool ValidatePassword(string password, string correctHash)
		{
			char[] delimiter = { ':' };
			var split = correctHash.Split(delimiter);
			var iterations = int.Parse(split[IterationIndex]);
			var salt = Convert.FromBase64String(split[SaltIndex]);
			var hash = Convert.FromBase64String(split[PBKDF2Index]);
			var testHash = PBKDF2(password, salt, iterations, hash.Length);
			return SlowEquals(hash, testHash);
		}

		private static bool SlowEquals(byte[] a, byte[] b)
		{
			var diff = (uint)a.Length ^ (uint)b.Length;
			for (var i = 0; i < a.Length && i < b.Length; i++)
				diff |= (uint)(a[i] ^ b[i]);
			return diff == 0;
		}

		private static byte[] PBKDF2(string password, byte[] salt, int iterations, int outputBytes)
		{
			using (var pbkdf2 = new Rfc2898DeriveBytes(password, salt))
			{
				pbkdf2.IterationCount = iterations;
				return pbkdf2.GetBytes(outputBytes);
			}
		}
	}
}
