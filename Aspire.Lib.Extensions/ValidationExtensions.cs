﻿using System;
using System.Text.RegularExpressions;

namespace Aspire.Lib.Extensions
{
	public static class ValidationExtensions
	{
		public static string TrimAndMakeItNullIfEmpty(this string str)
		{
			if (string.IsNullOrWhiteSpace(str))
				return null;
			return str.Trim();
		}

		public static string ValidatePassword(this string password)
		{
			return password.CannotBeEmptyOrWhitespace();
		}

		public static string MustBeNullOrEmpty(this string str)
		{
			if (!string.IsNullOrEmpty(str))
				throw new InvalidOperationException("String must be null or empty.");
			return str;
		}

		public static string CannotBeEmptyOrWhitespace(this string str)
		{
			if (string.IsNullOrWhiteSpace(str))
				throw new InvalidOperationException("String cannot be null, empty or whitespace.");
			return str;
		}

		public static Guid CannotBeDefaultOrEmpty(this Guid guid)
		{
			if (guid == Guid.Empty)
				throw new InvalidOperationException("Guid cannot be default or empty.");
			return guid;
		}

		public static string TrimAndCannotBeEmpty(this string str)
		{
			if (string.IsNullOrWhiteSpace(str))
				throw new InvalidOperationException("String cannot be null or whitespace.");
			return str.Trim();
		}

		public static string TrimAndCannotEmptyAndMustBeLowerCase(this string str)
		{
			str = str.TrimAndCannotBeEmpty();
			if (str.ToLower() != str)
				throw new InvalidOperationException("String must be in lowercase.");
			return str;
		}

		public static string TrimAndCannotEmptyAndMustBeUpperCase(this string str)
		{
			str = str.TrimAndCannotBeEmpty();
			if (str.ToUpper() != str)
				throw new InvalidOperationException("String must be in uppercase.");
			return str;
		}

		public static string MustBeEmailAddress(this string str)
		{
			return str.TrimAndCannotEmptyAndMustBeLowerCase().ValidateEmailAddress();
		}

		public static string MustBeNullableEmailAddress(this string str)
		{
			if (string.IsNullOrWhiteSpace(str))
				return null;
			return str.MustBeEmailAddress();
		}

		public static string MustBeCNIC(this string str)
		{
			return str.TrimAndCannotEmptyAndMustBeLowerCase().ValidateCNIC();
		}

		public static string MustBeNullableCNIC(this string str)
		{
			if (string.IsNullOrWhiteSpace(str))
				return null;
			return str.MustBeCNIC();
		}

		public static bool IsMatch(this string str, string pattern, RegexOptions options)
		{
			if (string.IsNullOrWhiteSpace(str) || string.IsNullOrWhiteSpace(pattern))
				return false;
			return Regex.IsMatch(str, pattern, options);
		}

		public static bool IsMatch(this string str, string pattern)
		{
			if (string.IsNullOrWhiteSpace(str) || string.IsNullOrWhiteSpace(pattern))
				return false;
			return Regex.IsMatch(str, pattern);
		}

		public static string MustMatch(this string str, string pattern)
		{
			return str.IsMatch(pattern) ? str : throw new InvalidOperationException("Pattern not matched. Pattern: " + pattern);
		}

		public static string MustMatch(this string str, string pattern, RegexOptions regexOptions)
		{
			return str.IsMatch(pattern, regexOptions) ? str : throw new InvalidOperationException("Pattern not matched. Pattern: " + pattern);
		}

		public static string ValidateEmailAddress(this string str)
		{
			return str.MustMatch(CommonRegularExpressions.Email, RegexOptions.IgnoreCase).ToLower();
		}

		public static string TryValidateEmailAddress(this string str)
		{
			return str.IsMatch(CommonRegularExpressions.Email, RegexOptions.IgnoreCase) ? str.ToLower() : null;
		}

		public static string ValidateCNIC(this string str)
		{
			return str.MustMatch(CommonRegularExpressions.CNIC);
		}

		public static string ValidateMobileNumber(this string str)
		{
			if (string.IsNullOrWhiteSpace(str))
				return null;
			if (Regex.IsMatch(str, CommonRegularExpressions.Mobile, RegexOptions.IgnoreCase))
				return str;
			throw new InvalidOperationException("Mobile Number not validated.");
		}

		public static DateTime CannotBeNull(this DateTime? dateTime)
		{
			if (dateTime == null)
				throw new InvalidOperationException("DateTime cannot be null.");
			return dateTime.Value;
		}

		public static DateTime? CannotBeFutureDate(this DateTime? dateTime)
		{
			if (dateTime != null && dateTime.Value > DateTime.Now)
				throw new InvalidOperationException("Future Date not allowed.");
			return dateTime;
		}
		public static DateTime? CannotBePreviousDate(this DateTime? dateTime)
		{
			if (dateTime != null && dateTime.Value < DateTime.Now)
				throw new InvalidOperationException("Previous Date not allowed.");
			return dateTime;
		}

		public static DateTime? CannotBeFutureDate(this DateTime dateTime)
		{
			return ((DateTime?)dateTime).CannotBeFutureDate();
		}

		public static DateTime? CannotBePreviousDate(this DateTime dateTime)
		{
			return ((DateTime?)dateTime).CannotBePreviousDate();
		}

		public static DateTime CannotBeNullAndFutureDate(this DateTime? dateTime)
		{
			return dateTime.CannotBeFutureDate().CannotBeNull();
		}

		public static DateTime CannotBeNullAndPreviousDate(this DateTime? dateTime)
		{
			return dateTime.CannotBePreviousDate().CannotBeNull();
		}

		public static DateTime CannotBeNullAndFutureDate(this DateTime dateTime)
		{
			return dateTime.CannotBeFutureDate().CannotBeNull();
		}

		public static DateTime CannotBeNullAndPreviousDate(this DateTime dateTime)
		{
			return dateTime.CannotBePreviousDate().CannotBeNull();
		}

		public static T CannotBeNull<T>(this T obj)
		{
			if (obj == null)
				throw new InvalidOperationException("Cannot be null.");
			return obj;
		}

		public static T MustBeNull<T>(this T obj)
		{
			if (obj != null)
				throw new InvalidOperationException("Must be null.");
			return obj;
		}

		public static string TrimUpToMaxLength(this string str, int maxLength)
		{
			if (str == null)
				return null;
			str = str.Trim();
			if (str.Length > maxLength)
				return str.Substring(0, maxLength);
			return str;
		}
	}
}
