﻿namespace Aspire.Lib.Extensions
{
	public static class CommonRegularExpressions
	{
		public static readonly string Email = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
		public static readonly string Url = @"^http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?$";
		public static readonly string IPV4 = @"\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\";
		public static readonly string CNIC = @"^\d{5}-\d{7}-\d{1}$";
		public static readonly string Mobile = @"^03\d{2}-\d{7}$";
	}
}
