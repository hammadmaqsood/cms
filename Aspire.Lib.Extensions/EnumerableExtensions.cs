﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI.WebControls;

namespace Aspire.Lib.Extensions
{
	public static class EnumerableExtensions
	{
		public static IOrderedQueryable<TSource> OrderBy<TSource, TKey>(this IQueryable<TSource> source, SortDirection sortDirection, Expression<Func<TSource, TKey>> keySelector)
		{
			switch (sortDirection)
			{
				case SortDirection.Ascending:
					return source.OrderBy(keySelector);
				case SortDirection.Descending:
					return source.OrderByDescending(keySelector);
				default:
					throw new ArgumentOutOfRangeException(nameof(sortDirection), sortDirection, null);
			}
		}

		public static IOrderedQueryable<TSource> ThenBy<TSource, TKey>(this IOrderedQueryable<TSource> source, SortDirection sortDirection, Expression<Func<TSource, TKey>> keySelector)
		{
			switch (sortDirection)
			{
				case SortDirection.Ascending:
					return source.ThenBy(keySelector);
				case SortDirection.Descending:
					return source.ThenByDescending(keySelector);
				default:
					throw new ArgumentOutOfRangeException(nameof(sortDirection), sortDirection, null);
			}
		}

		public static IOrderedEnumerable<TSource> OrderBy<TSource, TKey>(this IEnumerable<TSource> source, SortDirection sortDirection, Func<TSource, TKey> keySelector)
		{
			switch (sortDirection)
			{
				case SortDirection.Ascending:
					return source.OrderBy(keySelector);
				case SortDirection.Descending:
					return source.OrderByDescending(keySelector);
				default:
					throw new ArgumentOutOfRangeException(nameof(sortDirection), sortDirection, null);
			}
		}

		public static IOrderedEnumerable<TSource> ThenBy<TSource, TKey>(this IOrderedEnumerable<TSource> source, SortDirection sortDirection, Func<TSource, TKey> keySelector)
		{
			switch (sortDirection)
			{
				case SortDirection.Ascending:
					return source.ThenBy(keySelector);
				case SortDirection.Descending:
					return source.ThenByDescending(keySelector);
				default:
					throw new ArgumentOutOfRangeException(nameof(sortDirection), sortDirection, null);
			}
		}

		public static IEnumerable<IEnumerable<T>> SplitIntoGroups<T>(this IEnumerable<T> data, int groupSize)
		{
			var index = 0;
			return data.GroupBy(d => index++ % groupSize).Select(g => g.Select(gg => gg));
		}
	}
}