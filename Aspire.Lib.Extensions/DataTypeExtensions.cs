﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;

namespace Aspire.Lib.Extensions
{
	public static class DataTypeExtensions
	{
		private const long OneKB = 1024;
		private const long OneMB = OneKB * 1024;
		private const long OneGB = OneMB * 1024;
		private const long OneTB = OneGB * 1024;

		public static string ToDiskSize(this long fileSizeInBytes, int decimalPlaces)
		{
			var size = (double)fileSizeInBytes;
			var asTB = Math.Round(size / OneTB, decimalPlaces, MidpointRounding.AwayFromZero);
			var asGB = Math.Round(size / OneGB, decimalPlaces, MidpointRounding.AwayFromZero);
			var asMB = Math.Round(size / OneMB, decimalPlaces, MidpointRounding.AwayFromZero);
			var asKB = Math.Round(size / OneKB, decimalPlaces, MidpointRounding.AwayFromZero);
			return asTB > 1
				? $"{asTB}TB"
				: asGB > 1
					? $"{asGB}GB"
					: asMB > 1
						? $"{asMB}MB"
						: asKB > 1
							? $"{asKB}KB"
							: $"{size}B";
		}

		public static string ToDiskSize(this int value, int decimalPlaces = 0)
		{
			return ((long)value).ToDiskSize(decimalPlaces);
		}

		public static string ComputeHashMD5(this string input)
		{
			using (var md5 = MD5.Create())
			{
				var data = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
				var sBuilder = new StringBuilder();
				foreach (var t in data)
					sBuilder.Append(t.ToString("x2"));
				return sBuilder.ToString();
			}
		}

		#region string

		public static readonly string NA = "N/A";

		public static string ToNullIfEmpty(this string str)
		{
			return string.IsNullOrEmpty(str) ? null : str;
		}

		public static string ToNullIfWhiteSpace(this string str)
		{
			return string.IsNullOrWhiteSpace(str) ? null : str;
		}

		public static string ToNAIfNullOrEmpty(this string str)
		{
			return string.IsNullOrEmpty(str) ? NA : str;
		}

		public static object ToNAIfNullOrEmpty(this object obj)
		{
			if (obj == null)
				return NA;
			if (obj is string s)
				return s.ToNAIfNullOrEmpty();
			return obj;
		}

		public static string TrimIfNotNull(this string str)
		{
			return str?.Trim();
		}

		public static string ToNullIfEmptyOrNA(this string str)
		{
			return string.IsNullOrEmpty(str)
				|| str.Trim().Equals(NA, StringComparison.CurrentCultureIgnoreCase)
				|| str.Trim().Equals("na", StringComparison.CurrentCultureIgnoreCase)
				? null : str;
		}

		public static string ToNullIfWhiteSpaceOrNA(this string str)
		{
			return string.IsNullOrWhiteSpace(str)
				|| str.Trim().Equals(NA, StringComparison.CurrentCultureIgnoreCase)
				|| str.Trim().Equals("na", StringComparison.CurrentCultureIgnoreCase)
				? null : str;
		}

		#endregion

		#region Numbers

		public static long ToLong(this string num)
		{
			return long.Parse(num);
		}

		public static long? ToNullableLong(this string num)
		{
			return num.ToNullIfWhiteSpace()?.ToLong();
		}

		public static long? TryToLong(this string num)
		{
			if (long.TryParse(num, out var val))
				return val;
			return null;
		}

		public static ulong ToULong(this string num)
		{
			return ulong.Parse(num);
		}

		public static ulong? ToNullableULong(this string num)
		{
			return num.ToNullIfWhiteSpace()?.ToULong();
		}

		public static ulong? TryToULong(this string num)
		{
			if (ulong.TryParse(num, out var val))
				return val;
			return null;
		}

		public static int ToInt(this string num)
		{
			return int.Parse(num);
		}

		public static int? ToNullableInt(this string num)
		{
			return num.ToNullIfWhiteSpace()?.ToInt();
		}

		public static int? TryToInt(this string num)
		{
			if (int.TryParse(num, out var val))
				return val;
			return null;
		}

		public static uint ToUInt(this string num)
		{
			return uint.Parse(num);
		}

		public static uint? ToNullableUInt(this string num)
		{
			return num.ToNullIfWhiteSpace()?.ToUInt();
		}

		public static uint? TryToUInt(this string num)
		{
			if (uint.TryParse(num, out var val))
				return val;
			return null;
		}

		public static short ToShort(this string num)
		{
			return short.Parse(num);
		}

		public static short? ToNullableShort(this string num)
		{
			return num.ToNullIfWhiteSpace()?.ToShort();
		}

		public static short? TryToShort(this string num)
		{
			if (short.TryParse(num, out var val))
				return val;
			return null;
		}

		public static ushort ToUShort(this string num)
		{
			return ushort.Parse(num);
		}

		public static ushort? ToNullableUShort(this string num)
		{
			return num.ToNullIfWhiteSpace()?.ToUShort();
		}

		public static ushort? TryToUShort(this string num)
		{
			if (ushort.TryParse(num, out var val))
				return val;
			return null;
		}

		public static byte ToByte(this string num)
		{
			return byte.Parse(num);
		}

		public static byte? ToNullableByte(this string num)
		{
			return num.ToNullIfWhiteSpace()?.ToByte();
		}

		public static byte? TryToByte(this string num)
		{
			if (byte.TryParse(num, out var val))
				return val;
			return null;
		}

		public static sbyte ToSByte(this string num)
		{
			return sbyte.Parse(num);
		}

		public static sbyte? ToNullableSByte(this string num)
		{
			return num.ToNullIfWhiteSpace()?.ToSByte();
		}

		public static sbyte? TryToSByte(this string num)
		{
			if (sbyte.TryParse(num, out var val))
				return val;
			return null;
		}

		public static double ToDouble(this string num)
		{
			return double.Parse(num);
		}

		public static double? ToNullableDouble(this string num)
		{
			return num.ToNullIfWhiteSpace()?.ToDouble();
		}

		public static double? TryToDouble(this string num)
		{
			if (double.TryParse(num, out var val))
				return val;
			return null;
		}

		public static float ToFloat(this string num)
		{
			return float.Parse(num);
		}

		public static float? ToNullableFloat(this string num)
		{
			return num.ToNullIfWhiteSpace()?.ToFloat();
		}

		public static float? TryToFloat(this string num)
		{
			if (float.TryParse(num, out var val))
				return val;
			return null;
		}

		public static decimal ToDecimal(this string num)
		{
			return decimal.Parse(num);
		}

		public static decimal? ToNullableDecimal(this string num)
		{
			return num.ToNullIfWhiteSpace()?.ToDecimal();
		}

		public static decimal? TryToDecimal(this string num)
		{
			if (decimal.TryParse(num, out var val))
				return val;
			return null;
		}

		public static bool ToBoolean(this string value)
		{
			if (value == null)
				throw new InvalidOperationException();
			if (value == "1" || value.ToLower() == "true" || value.ToLower() == "yes")
				return true;
			if (value == "0" || value.ToLower() == "false" || value.ToLower() == "no")
				return false;
			return bool.Parse(value);
		}

		public static bool? ToNullableBoolean(this string num)
		{
			return num.ToNullIfWhiteSpace()?.ToBoolean();
		}

		public static bool? TryToBoolean(this string num)
		{
			try
			{
				return num.ToBoolean();
			}
			catch
			{
				return null;
			}
		}

		#endregion

		#region Guid

		public static Guid ToGuid(this string str)
		{
			return Guid.Parse(str);
		}

		public static Guid? ToNullableGuid(this string str)
		{
			return str.ToNullIfWhiteSpace()?.ToGuid();
		}

		public static Guid? TryToGuid(this string str)
		{
			if (Guid.TryParse(str, out var val))
				return val;
			return null;
		}

		#endregion

		#region DateTime

		public static DateTime ToDateTime(this string s)
		{
			return DateTime.Parse(s);
		}

		public static DateTime ToDateTime(this string s, string format, IFormatProvider formatProvider)
		{
			return DateTime.ParseExact(s, format, formatProvider);
		}

		public static DateTime ToDateTime(this string s, string format)
		{
			return s.ToDateTime(format, CultureInfo.CurrentCulture);
		}

		public static DateTime ToDateTime(this string s, IFormatProvider formatProvider)
		{
			return DateTime.Parse(s, formatProvider);
		}

		public static DateTime? ToNullableDateTime(this string s)
		{
			return s.ToNullIfWhiteSpace()?.ToDateTime();
		}

		public static DateTime? ToNullableDateTime(this string s, string format, IFormatProvider formatProvider)
		{
			return s.ToNullIfWhiteSpace().ToDateTime(format, formatProvider);
		}

		public static DateTime? ToNullableDateTime(this string s, string format)
		{
			return s.ToNullIfWhiteSpace().ToDateTime(format);
		}

		public static DateTime? ToNullableDateTime(this string s, IFormatProvider formatProvider)
		{
			return s.ToNullIfWhiteSpace().ToDateTime(formatProvider);
		}

		public static DateTime? TryToDateTime(this string str)
		{
			try
			{
				return str.ToDateTime();
			}
			catch
			{
				return null;
			}
		}

		public static DateTime? TryToDateTime(this string str, string format, IFormatProvider formatProvider)
		{
			try
			{
				return str.ToDateTime(format, formatProvider);
			}
			catch
			{
				return null;
			}
		}

		public static DateTime? TryToDateTime(this string str, string format)
		{
			try
			{
				return str.ToDateTime(format);
			}
			catch
			{
				return null;
			}
		}

		public static DateTime TruncateSecond(this DateTime dateTime)
		{
			return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second);
		}

		public static DateTime TruncateMinute(this DateTime dateTime)
		{
			return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, 0);
		}

		public static DateTime TruncateHour(this DateTime dateTime)
		{
			return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, 0, 0);
		}

		public static DateTime TruncateDay(this DateTime dateTime)
		{
			return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day);
		}

		public static DateTime TruncateMonth(this DateTime dateTime)
		{
			return new DateTime(dateTime.Year, dateTime.Month, 1);
		}

		public static DateTime TruncateYear(this DateTime dateTime)
		{
			return new DateTime(dateTime.Year, 1, 1);
		}

		#endregion

		#region HttpUtility

		public static string HtmlEncode(this string str)
		{
			return HttpUtility.HtmlEncode(str);
		}

		public static string HtmlEncode(this string str, bool breakNewLines)
		{
			return breakNewLines ? str.HtmlEncode().Replace("\n", "<br />") : str.HtmlEncode();
		}

		public static string HtmlDecode(this string str)
		{
			return HttpUtility.HtmlDecode(str);
		}

		public static string HtmlAttributeEncode(this string str)
		{
			return HttpUtility.HtmlAttributeEncode(str);
		}

		public static string UrlEncode(this string str)
		{
			return HttpUtility.UrlEncode(str);
		}

		public static string UrlDecode(this string str)
		{
			return HttpUtility.UrlDecode(str);
		}

		#endregion

		#region Enums

		public static T ToEnum<T>(this string str) where T : struct, IConvertible
		{
			if (!typeof(T).IsEnum)
				throw new ArgumentException($"T must be enum.");
			return (T)Enum.Parse(typeof(T), str);
		}

		public static T? ToNullableEnum<T>(this string str) where T : struct, IConvertible
		{
			return str.ToNullIfWhiteSpace()?.ToEnum<T>();
		}

		public static IEnumerable<T> GetFlags<T>(this T @enum) where T : struct, IConvertible
		{
			var type = typeof(T);
			if (!type.IsEnum)
				throw new ArgumentException($"Must be enum.", nameof(@enum));
			var _enum = (Enum)Convert.ChangeType(@enum, type);
			return Enum.GetValues(type).Cast<Enum>().Where(value => _enum.HasFlag(value)).Select(value => (T)Convert.ChangeType(value, type));
		}

		public static int ToInt(this Enum @enum)
		{
			return (int)Convert.ChangeType(@enum, TypeCode.Int32);
		}

		public static short ToShort(this Enum @enum)
		{
			return (short)Convert.ChangeType(@enum, TypeCode.Int16);
		}

		public static byte ToByte(this Enum @enum)
		{
			return (byte)Convert.ChangeType(@enum, TypeCode.Byte);
		}

		public static object ToUnderlyingType(this Enum @enum)
		{
			return Convert.ChangeType(@enum, Enum.GetUnderlyingType(@enum.GetType()));
		}

		#endregion

		public static string ResolveClientUrl(this string relativeUrl, Page page)
		{
			if (string.IsNullOrWhiteSpace(relativeUrl))
				return relativeUrl;
			return page.ResolveClientUrl(relativeUrl);
		}

		public static string JavaScriptStringEncode(this string str)
		{
			return HttpUtility.JavaScriptStringEncode(str);
		}

		public static string JavaScriptStringEncode(this string str, bool addDoubleQuotes)
		{
			return HttpUtility.JavaScriptStringEncode(str, addDoubleQuotes);
		}

		public static string UrlPathEncode(this string str)
		{
			return HttpUtility.UrlPathEncode(str);
		}

		public static string ToAbsoluteUrl(this string url)
		{
			if (url == null)
				return null;
			if (url != "~" && !url.StartsWith("~/"))
				throw new InvalidOperationException("Url must start with tilde.");
			var absoluteUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host;
			if (!HttpContext.Current.Request.Url.IsDefaultPort)
				absoluteUrl += ":" + HttpContext.Current.Request.Url.Port;
			absoluteUrl += HttpContext.Current.Request.ApplicationPath;
			if (absoluteUrl.EndsWith("/"))
				return absoluteUrl + url.TrimStart('~', '/');
			return absoluteUrl + url.TrimStart('~');
		}

		public static string MapPath(this string path)
		{
			return HttpContext.Current.Server.MapPath(path);
		}

		public static string ToWords(this int number)
		{
			if (number < 0)
				throw new NotSupportedException("Negative numbers are not supported.");
			if (number == 0)
				return string.Empty;
			var below20 = new[] { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
			var below100 = new[] { "", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
			if (number >= 1 && number < 20)
				return below20[number];
			if (number >= 20 && number <= 99)
				return below100[number / 10] + " " + below20[number % 10];
			if (number >= 100 && number <= 999)
				return ToWords(number / 100) + " Hundred " + ToWords(number % 100);
			if (number >= 1000 && number <= 99999)
				return ToWords(number / 1000) + " Thousand " + ToWords(number % 1000);
			if (number >= 100000 && number <= 9999999)
				return ToWords(number / 100000) + " Lac " + ToWords(number % 100000);
			if (number >= 10000000)
				return ToWords(number / 10000000) + " Crore " + ToWords(number % 10000000);
			throw new NotSupportedException();
		}

		public static string ToWords(this short number)
		{
			return ((int)number).ToWords();
		}

		public static SortDirection Toggle(this SortDirection sortDirection)
		{
			switch (sortDirection)
			{
				case SortDirection.Ascending:
					return SortDirection.Descending;
				case SortDirection.Descending:
					return SortDirection.Ascending;
				default:
					throw new ArgumentOutOfRangeException(nameof(sortDirection), sortDirection, null);
			}
		}

		public static SortDirection Toggle(this SortDirection? sortDirection, SortDirection defaultSortDirection = SortDirection.Ascending)
		{
			if (sortDirection == null)
				return defaultSortDirection;
			return sortDirection.Value.Toggle();
		}

		public static string SplitCamelCasing(this string camelCasingString)
		{
			var stringBuilder = new StringBuilder();
			var lastIsUpper = false;
			foreach (var character in camelCasingString)
			{
				if (char.IsLower(character))
				{
					stringBuilder.Append(character);
					lastIsUpper = false;
				}
				else
				{
					if (!lastIsUpper)
						stringBuilder.Append(' ');
					stringBuilder.Append(character);
					lastIsUpper = true;
				}
			}
			return stringBuilder.ToString().Trim();
		}

		public static string ToYesNo(this bool val)
		{
			return val ? "Yes" : "No";
		}

		public static string ToOneZero(this bool val)
		{
			return val ? "1" : "0";
		}

		public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
		{
			if (source == null)
				throw new ArgumentNullException(nameof(source));
			if (action == null)
				throw new ArgumentNullException(nameof(action));
			foreach (T item in source)
				action(item);
		}

		private const string ToText = " - ";

		public static string ConcatenateDates(this DateTime? fromDate, DateTime? toDate, string dateFormat, string toText = ToText, bool convertToNAIfEmpty = true)
		{
			if (fromDate != null && toDate != null)
			{
				if (fromDate.Value != toDate.Value)
					return $"{fromDate.Value.ToString(dateFormat)}{toText}{toDate.Value.ToString(dateFormat)}";
				return $"{fromDate.Value.ToString(dateFormat)}";
			}
			if (fromDate != null)
				return $"{fromDate.Value.ToString(dateFormat)}";
			if (toDate != null)
				return $"{toDate.Value.ToString(dateFormat)}";
			if (convertToNAIfEmpty)
				return string.Empty.ToNAIfNullOrEmpty();
			return null;
		}

		public static string ConcatenateDates(this DateTime fromDate, DateTime? toDate, string dateFormat, string toText = ToText)
		{
			return ConcatenateDates((DateTime?)fromDate, toDate, dateFormat, toText);
		}

		private static readonly JavaScriptSerializer JavaScriptSerializer = new JavaScriptSerializer();

		public static string ToJsonString<T>(this T type)
		{
			return JavaScriptSerializer.Serialize(type);
		}

		public static T FromJsonString<T>(this string jsonString)
		{
			return JavaScriptSerializer.Deserialize<T>(jsonString);
		}

		public static string ToXmlString<T>(this T type)
		{
			var stringBuilder = new StringBuilder();
			using (var writer = XmlWriter.Create(stringBuilder))
			{
				new XmlSerializer(type.GetType()).Serialize(writer, type);
			}
			return stringBuilder.ToString();
		}

		public static T FromXmlString<T>(this string xml)
		{
			using (var reader = XmlReader.Create(xml))
			{
				return (T)new XmlSerializer(typeof(T)).Deserialize(reader);
			}
		}

		public static string Compress(this string input)
		{
			using (var msi = new MemoryStream(Encoding.UTF8.GetBytes(input)))
			{
				MemoryStream mso = null;
				try
				{
					mso = new MemoryStream();
					using (var gs = new GZipStream(mso, CompressionMode.Compress))
					{
						msi.CopyTo(gs);
					}
					return Encoding.UTF8.GetString(mso.ToArray());
				}
				finally
				{
					mso?.Dispose();
				}
			}
		}

		public static string Decompress(this string input)
		{
			using (var msi = new MemoryStream(Encoding.UTF8.GetBytes(input)))
			{
				MemoryStream mso = null;
				try
				{
					mso = new MemoryStream();
					using (var gs = new GZipStream(msi, CompressionMode.Decompress))
					{
						gs.CopyTo(mso);
					}
					return Encoding.UTF8.GetString(mso.ToArray());
				}
				finally
				{
					mso?.Dispose();
				}
			}
		}

		public static IEnumerable<int> ToInt(this IEnumerable<string> numbers)
		{
			return numbers.Select(n => n.ToInt());
		}

		public static IEnumerable<short> ToShort(this IEnumerable<string> numbers)
		{
			return numbers.Select(n => n.ToShort());
		}

		public static string ToTime12Format(this TimeSpan timeSpan)
		{
			if (timeSpan.Hours == 12)
				return timeSpan.ToString("hh\\:mm") + " PM";
			if (timeSpan.Hours > 12)
				return timeSpan.Add(TimeSpan.FromHours(-12)).ToString("hh\\:mm") + " PM";
			return timeSpan.ToString("hh\\:mm") + " AM";
		}

		public static string ToTime24Format(this TimeSpan timeSpan)
		{
			return timeSpan.ToString("hh\\:mm");
		}

		public static SecureString ToSecureString(this string password)
		{
			if (password == null)
				throw new ArgumentNullException(nameof(password));

			var securePassword = new SecureString();
			foreach (var @char in password)
				securePassword.AppendChar(@char);
			securePassword.MakeReadOnly();
			return securePassword;
		}

		public static string ToUnSecureString(this SecureString secureString)
		{
			if (secureString == null)
				throw new ArgumentNullException(nameof(secureString));

			var unManagedString = IntPtr.Zero;
			try
			{
				unManagedString = Marshal.SecureStringToGlobalAllocUnicode(secureString);
				return Marshal.PtrToStringUni(unManagedString);
			}
			finally
			{
				Marshal.ZeroFreeGlobalAllocUnicode(unManagedString);
			}
		}

		public static IPAddress ToIPAddress(this string ip)
		{
			return IPAddress.Parse(ip);
		}

		public static IPAddress TryToIPAddress(this string ip)
		{
			if (IPAddress.TryParse(ip, out var address))
				return address;
			return null;
		}

		public static IPAddress GetNetworkAddress(this IPAddress address, IPAddress subnetMask)
		{
			var ipAdressBytes = address.GetAddressBytes();
			var subnetMaskBytes = subnetMask.GetAddressBytes();
			if (ipAdressBytes.Length != subnetMaskBytes.Length)
				throw new ArgumentException("Lengths of IP address and subnet mask do not match.");
			var broadcastAddress = new byte[ipAdressBytes.Length];
			for (int i = 0; i < broadcastAddress.Length; i++)
				broadcastAddress[i] = (byte)(ipAdressBytes[i] & (subnetMaskBytes[i]));
			return new IPAddress(broadcastAddress);
		}

		public static bool IsInSameSubnet(this IPAddress address1, IPAddress address2, IPAddress subnetMask)
		{
			return address1.GetNetworkAddress(subnetMask).Equals(address2.GetNetworkAddress(subnetMask));
		}

		public static bool IsEqual(this IPAddress ip1, IPAddress ip2)
		{
			if (IPAddress.IsLoopback(ip1) && IPAddress.IsLoopback(ip2))
				return true;
			return Equals(ip1, ip2);
		}

		public static string Join(this IEnumerable<string> value, string separator)
		{
			return string.Join(separator, value);
		}

		public static byte[] ComputeChecksumMD5(this byte[] input)
		{
			if (input == null || input.Length == 0)
				throw new ArgumentNullException(nameof(input));
			using (var md5 = MD5.Create())
			{
				return md5.ComputeHash(input);
			}
		}

		public static string ToHexFormat(this byte[] bytes)
		{
			return string.Join("", bytes.Select(b => b.ToString("x2")));
		}

		public static string WrapInDoubleQuotes(this string input)
		{
			return $"\"{input}\"";
		}

		public static int ExclusiveOr(this IEnumerable<int> enumFlags)
		{
			var result = 0;
			foreach (var enumFlag in enumFlags)
				result |= enumFlag;
			return result;
		}
	}
}