﻿using System;
using System.Runtime.Serialization;

namespace Aspire.Lib.Extensions.Exceptions
{
	[Serializable]
	public class NotImplementedEnumException : NotImplementedException
	{
		public Enum EnumValue { get; }

		public NotImplementedEnumException(Enum @enum)
		{
			this.EnumValue = @enum;
		}

		protected NotImplementedEnumException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		public override string Message => $"Enum not implemented. Type: {this.EnumValue.GetType().FullName}, ValueName: {this.EnumValue.ToString()}, IntegerValue: {Convert.ChangeType(this.EnumValue, this.EnumValue.GetTypeCode())}";

		public override string ToString()
		{
			return $"{this.Message}\n{this.StackTrace}";
		}
	}
}
