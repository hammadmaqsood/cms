﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Scholarship
{
	[AspirePage("0444E615-68E1-4E01-8789-BC813D420EB9", UserTypes.Staff, "~/Sys/Staff/Scholarship/ScholarshipDashboard.aspx", "Scholarships", true, AspireModules.Scholarship)]
	public partial class ScholarshipDashboard : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_graduation_cap);
		public override string PageTitle => "Scholarships";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Scholarship.Module, UserGroupPermission.Allowed},
		};

		public static string GetPageURL(short? semesterID, bool search)
		{
			return typeof(ScholarshipDashboard).GetAspirePageAttribute().PageUrl.AttachQueryParams("SemesterID", semesterID, "Search", search);
		}

		public static void Redirect(short? semesterID, bool search)
		{
			Redirect(GetPageURL(semesterID, search));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				//this.ddlSemesterID.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
				var semesters = BL.Core.Common.Lists.GetScholarshipAnnouncedSemesters();
				this.ddlSemesterID.FillSemesters(semesters);
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);

				var canGenerateQarzeHasnaApplicationApprovals = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.Scholarship.CanGenerateQarzeHasnaApplicationForAllSemester) == UserGroupPermission.PermissionValues.Allowed;
				if (!canGenerateQarzeHasnaApplicationApprovals)
					this.btnAddAllSemesterApprovals.Enabled = false;

				var canRejectQarzeHasnaApplicationApprovals = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.Scholarship.CanManageQarzeHasanaCampusRecommendations) == UserGroupPermission.PermissionValues.Allowed;
				if (!canRejectQarzeHasnaApplicationApprovals)
					this.btnBulkRejection.Enabled = false;
			}
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData();
		}

		private void GetData()
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var scholarshipApplications = BL.Core.Scholarship.Staff.ScholarshipDashboard.GetScholarshipApplications(semesterID, this.AspireIdentity.LoginSessionGuid);
			if (scholarshipApplications == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect(semesterID, false);
				return;
			}
			this.gvScholarshipDashboard.DataBind(scholarshipApplications);

			this.hlNewApplication.NavigateUrl = ApplicationsStatus.GetPageURL(semesterID, null, false);
			this.hlQarzeHasanaList.NavigateUrl = Reports.ApplicationsList.GetPageURL(semesterID, ScholarshipApplicationApproval.ApprovalStatuses.Approved);
			this.hlQarzeHasnaSemesterWiseList.NavigateUrl = Reports.SemesterWiseApplicationsList.GetPageURL(semesterID, semesterID, ScholarshipApplicationApproval.ApprovalStatuses.Approved);
		}

		public static string RedirectToScholarship(Model.Entities.Scholarship.ScholarshipTypes scholarshipTypeEnum, int scholarshipAnnouncementID, string semesterID)
		{
			switch (scholarshipTypeEnum)
			{
				case Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana:
					return QarzeHasana.Dashboard.GetPageURL(scholarshipAnnouncementID, semesterID);
				case Model.Entities.Scholarship.ScholarshipTypes.StudentStudyLoan:
					return QarzeHasana.Dashboard.GetPageURL(scholarshipAnnouncementID, semesterID);
				default:
					throw new ArgumentOutOfRangeException(nameof(scholarshipTypeEnum), scholarshipTypeEnum, null);
			}
		}

		#region Bulk

		protected void btnAddAllSemesterApprovals_OnClick(object sender, EventArgs e)
		{
			var semesters = BL.Core.Common.Lists.GetScholarshipAnnouncedSemesters();
			this.ddlSemesterIDForApprovals.FillSemesters(semesters, CommonListItems.Select);
			this.ddlSemesterIDForApprovals_OnSelectedIndexChanged(null, null);
			this.modalAddThisSemesterApprovals.Show();
		}

		protected void ddlSemesterIDForApprovals_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDForApprovals.SelectedValue.ToNullableShort();
			if (semesterID == null)
			{
				this.gvBulkApproval.DataBind(null);
				this.btnGenerate.Enabled = false;
				this.updateModalAddThisSemesterApprovalsFooter.Update();
				return;
			}

			var approvals = BL.Core.Scholarship.Staff.QarzeHasana.Approval.GetDataForBulkApprovals(semesterID.Value, null, this.AspireIdentity.LoginSessionGuid);
			if (!approvals.Any())
			{
				this.gvBulkApproval.DataBind(null);
				this.btnGenerate.Enabled = false;
				this.updateModalAddThisSemesterApprovalsFooter.Update();
				return;
			}
			this.gvBulkApproval.DataBind(approvals);
			this.updateModalAddThisSemesterApprovalsFooter.Update();
			this.btnGenerate.Enabled = true;
		}

		protected void btnGenerate_OnClick(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDForApprovals.SelectedValue.ToShort();
			var scholarshipApplicationIDs = new List<int>();
			foreach (DataKey dataKey in this.gvBulkApproval.DataKeys)
				scholarshipApplicationIDs.Add((int)dataKey.Value);
			if (!scholarshipApplicationIDs.Any())
			{
				this.alertModalAddThisSemesterApprovals.AddNoRecordFoundAlert();
				this.updateAddThisSemesterApprovals.Update();
				return;
			}

			var generateApprovals = BL.Core.Scholarship.Staff.QarzeHasana.Approval.GenerateApplications(scholarshipApplicationIDs, semesterID, this.AspireIdentity.LoginSessionGuid);
			switch (generateApprovals.Status)
			{
				case Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.ApplicationGenerationStatusesResults.ApplicationGenerationStatuses.NoRecordAdded:
					this.alertModalAddThisSemesterApprovals.AddErrorAlert("There is an error or no record found.");
					this.updateAddThisSemesterApprovals.Update();
					return;
				case Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.ApplicationGenerationStatusesResults.ApplicationGenerationStatuses.Success:
					this.AddSuccessAlert($"No of {generateApprovals.Count} Record(s) for {semesterID.ToSemesterString()} Approvals have been added.");
					Redirect<ScholarshipDashboard>();
					return;
				default:
					throw new NotImplementedEnumException(generateApprovals.Status);
			}
		}

		protected void btnBulkRejection_OnClick(object sender, EventArgs e)
		{
			var semesters = BL.Core.Common.Lists.GetScholarshipAnnouncedSemesters();
			this.ddlRejectionSemesterID.FillSemesters(semesters, CommonListItems.Select);
			this.ddlRejectionSemesterID_OnSelectedIndexChanged(null, null);
			this.modalRejectSemesterApprovals.Show();
		}

		protected void ddlRejectionSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var rejectionSemesterID = this.ddlRejectionSemesterID.SelectedValue.ToNullableShort();
			if (rejectionSemesterID == null)
			{
				this.gvBulkRejection.DataBind(null);
				this.lblModalCampusRemarks.Visible = false;
				this.tbModalCampusRemarks.Visible = false;
				this.lblRejectionText.Visible = false;
				this.updateModalRejectsFooter.Update();
				this.btnProceed.Enabled = false;
				return;
			}

			var approvals = BL.Core.Scholarship.Staff.QarzeHasana.Approval.GetDataForBulkRejection(rejectionSemesterID.Value, null, this.AspireIdentity.LoginSessionGuid);

			if (!approvals.Any())
			{
				this.gvBulkRejection.DataBind(null);
				this.lblModalCampusRemarks.Visible = false;
				this.tbModalCampusRemarks.Visible = false;
				this.lblRejectionText.Visible = false;
				this.updateModalRejectsFooter.Update();
				this.btnProceed.Enabled = false;
				return;
			}

			this.gvBulkRejection.DataBind(approvals);

			this.lblModalCampusRemarks.Visible = true;
			this.tbModalCampusRemarks.Text = "Ineligible due to GPA/CGPA less than 2.75";
			this.tbModalCampusRemarks.Visible = true;
			this.lblRejectionText.Visible = true;
			this.lblRejectionText.Text = $"Are you sure to reject all cases who's GPA/CGPA less than 2.75 in {rejectionSemesterID.Value.ToSemesterString()}?";
			this.updateModalRejectsFooter.Update();
			this.btnProceed.Enabled = true;
		}

		protected void btnRejectionProceed_OnClick(object sender, EventArgs e)
		{
			var semesterID = this.ddlRejectionSemesterID.SelectedValue.ToShort();
			var scholarshipApplicationIDs = new List<int>();
			foreach (DataKey dataKey in this.gvBulkRejection.DataKeys)
				scholarshipApplicationIDs.Add((int)dataKey.Value);
			if (!scholarshipApplicationIDs.Any())
			{
				this.alertModalRejectSemesterApprovals.AddNoRecordFoundAlert();
				this.updateModalRejectSemesterApprovals.Update();
				this.lblModalCampusRemarks.Visible = false;
				this.tbModalCampusRemarks.Visible = false;
				this.lblRejectionText.Visible = false;
				this.updateModalRejectsFooter.Update();
				this.btnProceed.Enabled = false;
				return;
			}

			string remarks = this.tbModalCampusRemarks.Text;

			var rejectedApprovals = BL.Core.Scholarship.Staff.QarzeHasana.Approval.RejectApplications(scholarshipApplicationIDs, semesterID, remarks, this.AspireIdentity.LoginSessionGuid);
			switch (rejectedApprovals.Status)
			{
				case Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.ApplicationRejectionStatusesResults.ApplicationRejectionStatuses.NoRecordAdded:
					this.alertModalRejectSemesterApprovals.AddErrorAlert("There is an error or no record found.");
					this.updateModalRejectSemesterApprovals.Update();
					return;
				case Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.ApplicationRejectionStatusesResults.ApplicationRejectionStatuses.Success:
					this.AddSuccessAlert($"No of {rejectedApprovals.Count} case(s) who's GPA/CGPA less than {BL.Core.Scholarship.Staff.QarzeHasana.Approval.MinimumAllowedGPACGAP.FormatGPA()} in {semesterID.ToSemesterString()} Approvals have been Rejected.");
					Redirect<ScholarshipDashboard>();
					return;
				default:
					throw new NotImplementedEnumException(rejectedApprovals.Status);
			}
		}

		protected void btnUpdateFee_Click(object sender, EventArgs e)
		{
			var semesters = BL.Core.Common.Lists.GetScholarshipAnnouncedSemesters();
			this.ddlFeeSemesterID.FillSemesters(semesters, CommonListItems.Select);
			this.ddlFeeSemesterID_SelectedIndexChanged(null, null);
			this.trNoRecordFound.Visible = false;
			this.modalUpdateFee.Show();
		}

		protected void ddlFeeSemesterID_SelectedIndexChanged(object sender, EventArgs e)
		{
			var feeSemesterID = this.ddlFeeSemesterID.SelectedValue.ToNullableShort();
			if (feeSemesterID == null)
			{
				this.rpUpdateFee.DataBind(null);
				this.trNoRecordFound.Visible = true;
				return;
			}

			var approvals = BL.Core.Scholarship.Staff.QarzeHasana.Approval.GetDataToUpdateFee(feeSemesterID.Value, null, this.AspireIdentity.LoginSessionGuid);
			if (!approvals.Any())
			{
				this.rpUpdateFee.DataBind(null);
				this.trNoRecordFound.Visible = true;
				return;
			}
			this.rpUpdateFee.DataBind(approvals);
			this.trNoRecordFound.Visible = false;
		}

		protected void btnUpdateFeeProceed_Click(object sender, EventArgs e)
		{
			var semesterID = this.ddlFeeSemesterID.SelectedValue.ToShort();
			var scholarshipApplicationIDs = new List<int>();

			foreach (RepeaterItem item in this.rpUpdateFee.Items)
				if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
					if (((AspireCheckBox)item.FindControl("cbSelect")).Checked)
						scholarshipApplicationIDs.Add(((System.Web.UI.WebControls.HiddenField)item.FindControl("hfScholarshipApplicationID")).Value.ToInt());

			if (!scholarshipApplicationIDs.Any())
			{
				this.alertUpdateFee.AddNoRecordFoundAlert();
				return;
			}

			var feeAddedApprovals = BL.Core.Scholarship.Staff.QarzeHasana.Approval.UpdateFee(scholarshipApplicationIDs, semesterID, this.AspireIdentity.LoginSessionGuid);
			switch (feeAddedApprovals.Status)
			{
				case Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.UpdateFeeStatusesResults.UpdatedFeeStatuses.NoRecordAdded:
					this.alertModalRejectSemesterApprovals.AddErrorAlert("There is an error or no record found.");
					this.updateModalRejectSemesterApprovals.Update();
					return;
				case Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.UpdateFeeStatusesResults.UpdatedFeeStatuses.Success:
					this.AddSuccessAlert($"No of {feeAddedApprovals.Count} student(s) fee have been updated in {semesterID.ToSemesterString()} approvals.");
					Redirect<ScholarshipDashboard>();
					return;
				default:
					throw new NotImplementedEnumException(feeAddedApprovals.Status);
			}
		}

		protected void rpUpdateFee_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var chekbox = (AspireCheckBox)e.Item.FindControl("cbSelect");
				var calculatedFee = ((System.Web.UI.WebControls.HiddenField)e.Item.FindControl("hfCalculatedFee")).Value.ToNullableInt();

				if (calculatedFee == null)
					chekbox.Enabled = false;

				var addedFee = ((System.Web.UI.WebControls.HiddenField)e.Item.FindControl("hfAddedFee")).Value.ToNullableInt();
				if (calculatedFee != null && addedFee == null)
					chekbox.Checked = true;
			}
		}
		#endregion
	}
}
