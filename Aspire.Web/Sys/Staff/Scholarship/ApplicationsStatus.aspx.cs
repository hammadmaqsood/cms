﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Scholarship
{
	[AspirePage("73EC624E-B533-4D68-B699-D5FEF55DBDF7", UserTypes.Staff, "~/Sys/Staff/Scholarship/ApplicationsStatus.aspx", "Applications Status", true, AspireModules.Scholarship)]
	public partial class ApplicationsStatus : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_graduation_cap);
		public override string PageTitle => "Applications Status";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Scholarship.Module, UserGroupPermission.Allowed},
		};

		public static string GetPageURL(short? semesterID, string enrollment, bool search)
		{
			return typeof(ApplicationsStatus).GetAspirePageAttribute().PageUrl.AttachQueryParams(nameof(SemesterID), semesterID, nameof(Enrollment), enrollment, "Search", search);
		}

		public static void Redirect(short? semesterID, string enrollment, bool search)
		{
			Redirect(GetPageURL(semesterID, enrollment, search));
		}

		private string Enrollment => this.Request.GetParameterValue(nameof(this.Enrollment));
		private short? SemesterID => this.Request.GetParameterValue<short>(nameof(this.SemesterID));

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs args)
		{
			this.GetData();
		}

		private void GetData()
		{
			var enrollment = this.tbEnrollment.Enrollment;
			this.panelBody.Visible = false;
			var scholarshipApplications = BL.Core.Scholarship.Staff.QarzeHasana.ViewApplications.GetScholarshipApplications(enrollment, Web.Common.AspireIdentity.Current.LoginSessionGuid);
			if (scholarshipApplications == null)
			{
				this.AddNoRecordFoundAlert();
				return;
			}

			this.gvApplicationsList.DataBind(scholarshipApplications);
			this.panelBody.Visible = true;
		}

		public static string GetPageURLForScholarshipApplicationForm(Model.Entities.Scholarship.ScholarshipTypes scholarshipTypeEnum, int scholarshipAnnouncementID, int studentID)
		{
			switch (scholarshipTypeEnum)
			{
				case Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana:
					return QarzeHasana.ApplicationForm.GetPageUrl(scholarshipAnnouncementID, studentID);
				case Model.Entities.Scholarship.ScholarshipTypes.StudentStudyLoan:
					return QarzeHasana.ApplicationForm.GetPageUrl(scholarshipAnnouncementID, studentID);
				default:
					throw new ArgumentOutOfRangeException(nameof(scholarshipTypeEnum), scholarshipTypeEnum, null);
			}
		}

		protected void gvApplicationsList_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			var commandArgument = e.DecryptedCommandArgument();
			if (commandArgument == "")
			{
				this.AddErrorAlert("Application is not applied.");
				return;
			}

			switch (e.CommandName)
			{
				case "DocumentsStatus":
					var scholarshipApplication = BL.Core.Scholarship.Staff.QarzeHasana.ViewApplications.ToggleDocumentsStatus(commandArgument.ToInt(), Web.Common.AspireIdentity.Current.LoginSessionGuid);
					switch (scholarshipApplication)
					{
						case BL.Core.Scholarship.Staff.QarzeHasana.ViewApplications.UpdateDocumentsStatus.NoRecordFound:
							this.AddNoRecordFoundAlert();
							return;
						case BL.Core.Scholarship.Staff.QarzeHasana.ViewApplications.UpdateDocumentsStatus.InProgress:
							this.AddErrorAlert("Application is in progress.");
							return;
						case BL.Core.Scholarship.Staff.QarzeHasana.ViewApplications.UpdateDocumentsStatus.Success:
							this.GetData();
							return;
						default:
							throw new NotImplementedEnumException(scholarshipApplication);
					}
			}
		}
	}
}