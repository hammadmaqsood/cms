﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="MeritScholarship.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Scholarship.MeritScholarship" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:UpdatePanel runat="server">
		<ContentTemplate>
			<div class="row">
				<div class="form-group col-md-2">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" ValidationGroup="Parameters" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group col-md-4">
					<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" ValidationGroup="Parameters" AutoPostBack="True" OnSelectedIndexChanged="ddlProgramIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group col-md-2">
					<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="ddlSemesterNoFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterNoFilter" ValidationGroup="Parameters" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterNoFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group col-md-2">
					<aspire:AspireLabel runat="server" Text="GPA:" AssociatedControlID="ddlGPAFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlGPAFilter" ValidationGroup="Parameters" AutoPostBack="True" OnSelectedIndexChanged="ddlGPAFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group col-md-2">
					<aspire:AspireLabel runat="server" Text="Regular:" AssociatedControlID="ddlRegularFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlRegularFilter" ValidationGroup="Parameters" AutoPostBack="True" OnSelectedIndexChanged="ddlRegularFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group col-md-2">
					<aspire:AspireLabel runat="server" Text="Position:" AssociatedControlID="ddlPositionFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlPositionFilter" ValidationGroup="Parameters" AutoPostBack="True" OnSelectedIndexChanged="ddlPositionFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group col-md-4">
					<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
					<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" AllowNull="True" ValidationGroup="Parameters" OnSearch="tbEnrollment_OnSearch" />
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-2">
					<aspire:AspireLabel runat="server" Text="Order by:" AssociatedControlID="ddlOrderBy1" />
					<aspire:AspireDropDownList runat="server" ID="ddlOrderBy1" ValidationGroup="Parameters" AutoPostBack="True" OnSelectedIndexChanged="ddlOrderBy1_OnSelectedIndexChanged" />
				</div>
				<div class="form-group col-md-2">
					<aspire:AspireLabel runat="server" Text="Then by:" AssociatedControlID="ddlOrderBy1" />
					<aspire:AspireDropDownList runat="server" ID="ddlOrderBy2" ValidationGroup="Parameters" AutoPostBack="True" OnSelectedIndexChanged="ddlOrderBy2_OnSelectedIndexChanged" />
				</div>
				<div class="form-group col-md-2">
					<aspire:AspireLabel runat="server" Text="Then  by:" AssociatedControlID="ddlOrderBy1" />
					<aspire:AspireDropDownList runat="server" ID="ddlOrderBy3" ValidationGroup="Parameters" AutoPostBack="True" OnSelectedIndexChanged="ddlOrderBy3_OnSelectedIndexChanged" />
				</div>
				<div class="form-group col-md-2">
					<aspire:AspireLabel runat="server" Text="Then  by:" AssociatedControlID="ddlOrderBy1" />
					<aspire:AspireDropDownList runat="server" ID="ddlOrderBy4" ValidationGroup="Parameters" AutoPostBack="True" OnSelectedIndexChanged="ddlOrderBy4_OnSelectedIndexChanged" />
				</div>
				<div class="form-group col-md-2">
					<aspire:AspireLabel runat="server" Text="Then  by:" AssociatedControlID="ddlOrderBy1" />
					<aspire:AspireDropDownList runat="server" ID="ddlOrderBy5" ValidationGroup="Parameters" AutoPostBack="True" OnSelectedIndexChanged="ddlOrderBy5_OnSelectedIndexChanged" />
				</div>
			</div>
			<div style="font-size: 1em">
				<aspire:AspireGridView ID="gvScholarshipStudents" Condensed="True" runat="server" AutoGenerateColumns="False" AllowSorting="False" AllowPaging="True" OnPageIndexChanging="gvScholarshipStudents_OnPageIndexChanging" OnPageSizeChanging="gvScholarshipStudents_OnPageSizeChanging" OnRowCommand="gvScholarshipStudents_OnRowCommand">
					<Columns>
						<asp:BoundField DataField="Enrollment" HeaderText="Enrollment" />
						<asp:BoundField DataField="Name" HeaderText="Name" />
						<asp:BoundField DataField="ProgramAlias" HeaderText="Program" />
						<asp:BoundField DataField="SemesterNoFullName" HeaderText="Sem#" />
						<asp:TemplateField HeaderText="Courses">
							<ItemTemplate>
								<a data-toggle="modal" href="#" data-target="<%# Eval("ScholarshipStudentID",".modal{0}") %>"><%#: Eval("RegisteredCoursesCount") %></a>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="WorkloadCreditHours" HeaderText="Workload Credits" />
						<asp:BoundField DataField="RegisteredCreditHours" HeaderText="Credits" />
						<asp:BoundField DataField="GPACreditHours" HeaderText="GPA Credits" />
						<asp:BoundField DataField="GPA" HeaderText="GPA" DataFormatString="{0:N3}" />
						<asp:BoundField DataField="Percentage" HeaderText="%age" DataFormatString="{0:N3}" />
						<asp:BoundField DataField="RegisteredCoursesNotGraded" HeaderText="Courses Not Graded" />
						<asp:BoundField DataField="RegularYesNo" HeaderText="Regular" />
						<asp:BoundField DataField="OfferedClasses" HeaderText="Classes" />
						<asp:BoundField DataField="Position" HeaderText="Position" />
						<asp:BoundField DataField="ExcludedYesNo" HeaderText="Excluded" />
						<asp:BoundField DataField="Remarks" HeaderText="Remarks" />
						<asp:TemplateField HeaderText="Action">
							<ItemTemplate>
								<aspire:AspireLinkButton runat="server" EncryptedCommandArgument='<%# Eval("ScholarshipStudentID") %>' CommandName="Edit" CausesValidation="False" Text="Edit" />
								<aspire:AspireModal runat="server" ModalSize="Large" CssClass='<%# Eval("ScholarshipStudentID","modal{0}") %>' HeaderText='<%# Eval("Semester","Registered Courses - {0}") %>'>
									<BodyTemplate>
										<aspire:AspireGridView Condensed="True" DataSource='<%# Eval("RegisteredCoursesList") %>' runat="server" ID="gvRegisteredCourses" AutoGenerateColumns="False">
											<Columns>
												<asp:BoundField HeaderText="Enrollment" DataField="Enrollment" />
												<asp:BoundField HeaderText="Title" DataField="Title" />
												<asp:BoundField HeaderText="Major" DataField="Major" />
												<asp:BoundField HeaderText="Credits" DataField="CreditHours" />
												<asp:BoundField HeaderText="Total" DataField="Total" />
												<asp:BoundField HeaderText="Grade" DataField="GradeFullName" />
												<asp:BoundField HeaderText="Grade Points" DataField="GradePoints" />
												<asp:BoundField HeaderText="Status" DataField="StatusFullName" />
											</Columns>
										</aspire:AspireGridView>
									</BodyTemplate>
									<FooterTemplate>
										<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
									</FooterTemplate>
								</aspire:AspireModal>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</aspire:AspireGridView>
			</div>
			<div class="text-center">
				<aspire:AspireButton runat="server" Glyphicon="export" id="btnExportToCSV" CausesValidation="False" Text="Export to CSV" OnClick="btnExportToCSV_OnClick"/>
				<div class="btn-group dropup">
					<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
						Calculate <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li>
							<aspire:LinkButton runat="server" ID="btnRecalculate" ConfirmMessage="Are you sure you want to continue?" Text="Calculate" CausesValidation="False" OnClick="btnRecalculate_OnClick" />
						</li>
						<li>
							<aspire:LinkButton runat="server" ID="btnRecalculateOverwrite" ConfirmMessage="Are you sure you want to continue?" Text="Calculate (overwrite)" CausesValidation="False" OnClick="btnRecalculate_OnClick" />
						</li>
					</ul>
				</div>
			</div>
			<aspire:AspireModal runat="server" ID="modalEdit" HeaderText="Edit">
				<BodyTemplate>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="lblEnrollment" />
						<div class="form-control-static">
							<aspire:Label runat="server" ID="lblEnrollment" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="lblName" />
						<div class="form-control-static">
							<aspire:Label runat="server" ID="lblName" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Excluded:" AssociatedControlID="ddlExcluded" />
						<aspire:AspireDropDownList runat="server" ID="ddlExcluded" ValidationGroup="Save" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" />
						<aspire:AspireTextBox runat="server" TextMode="MultiLine" ID="tbRemarks" ValidationGroup="Save" Rows="3" MaxLength="1000" />
					</div>
				</BodyTemplate>
				<FooterTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="Save" Text="Save" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</FooterTemplate>
			</aspire:AspireModal>
		</ContentTemplate>
		<Triggers>
			<asp:PostBackTrigger ControlID="btnExportToCSV"/>
		</Triggers>
	</asp:UpdatePanel>
</asp:Content>
