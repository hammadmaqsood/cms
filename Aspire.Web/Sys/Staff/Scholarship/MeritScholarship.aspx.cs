﻿using Aspire.BL.Core.Scholarship.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Scholarship
{
	[AspirePage("E917885E-714B-4D7A-B3D0-FF0936625EC1", UserTypes.Staff, "~/Sys/Staff/Scholarship/MeritScholarship.aspx", "Merit Scholarship", true, AspireModules.Scholarship)]
	public partial class MeritScholarship : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Scholarship.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_graduation_cap);
		public override string PageTitle => "Merit Scholarship";

		private static ListItem[] SortItems => new[]
		{
			new ListItem("Program ASC", "ProgramAlias ASC"),
			new ListItem("Program DESC", "ProgramAlias DESC"),
			new ListItem("Semester No. ASC", "SemesterNo ASC"),
			new ListItem("Semester No. DESC", "SemesterNo DESC"),
			new ListItem("GPA ASC", "GPA ASC"),
			new ListItem("GPA DESC", "GPA DESC"),
			new ListItem("Percentage ASC", "Percentage ASC"),
			new ListItem("Percentage DESC", "Percentage DESC"),
			new ListItem("Enrollment ASC", "Enrollment ASC"),
			new ListItem("Enrollment DESC", "Enrollment DESC"),
		};

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters();
				this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, null, true, CommonListItems.All);
				this.ddlSemesterNoFilter.FillSemesterNos(CommonListItems.All);
				this.ddlGPAFilter.DataBind(new[]
				{
					new ListItem("=4.0", "4.00"),
					new ListItem(">=3.75", "3.75"),
					new ListItem(">=3.50", "3.50"),
				}, CommonListItems.All).SelectedIndex = 3;
				this.ddlRegularFilter.FillYesNo(CommonListItems.All);
				this.ddlPositionFilter.DataBind(new[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }, CommonListItems.All);
				this.ddlOrderBy1.DataBind(SortItems).SelectedIndex = 0;
				this.ddlOrderBy2.DataBind(SortItems).SelectedIndex = 2;
				this.ddlOrderBy3.DataBind(SortItems).SelectedIndex = 5;
				this.ddlOrderBy4.DataBind(SortItems).SelectedIndex = 7;
				this.ddlOrderBy5.DataBind(SortItems).SelectedIndex = 8;
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlProgramIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterNoFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterNoFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlGPAFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlGPAFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlRegularFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlRegularFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlPositionFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlPositionFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.tbEnrollment_OnSearch(null, null);
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs args)
		{
			this.ddlOrderBy1_OnSelectedIndexChanged(null, null);
		}

		protected void ddlOrderBy1_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlOrderBy2_OnSelectedIndexChanged(null, null);
		}

		protected void ddlOrderBy2_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlOrderBy3_OnSelectedIndexChanged(null, null);
		}

		protected void ddlOrderBy3_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlOrderBy4_OnSelectedIndexChanged(null, null);
		}

		protected void ddlOrderBy4_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlOrderBy5_OnSelectedIndexChanged(null, null);
		}

		protected void ddlOrderBy5_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void gvScholarshipStudents_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvScholarshipStudents_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvScholarshipStudents.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void GetData(int pageIndex)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var semesterNo = this.ddlSemesterNoFilter.GetSelectedEnumValue<SemesterNos>(null);
			var gpa = this.ddlGPAFilter.SelectedValue.ToNullableDecimal();
			var regular = this.ddlRegularFilter.SelectedValue.ToNullableBoolean();
			var position = this.ddlPositionFilter.SelectedValue.ToNullableInt();
			var enrollment = this.tbEnrollment.Enrollment;
			var orderBy1 = this.ddlOrderBy1.SelectedValue.Replace(" ASC", "").Replace(" DESC", "");
			var sortDirection1 = this.ddlOrderBy1.SelectedValue.EndsWith(" ASC") ? SortDirection.Ascending : SortDirection.Descending;
			var orderBy2 = this.ddlOrderBy2.SelectedValue.Replace(" ASC", "").Replace(" DESC", "");
			var sortDirection2 = this.ddlOrderBy2.SelectedValue.EndsWith(" ASC") ? SortDirection.Ascending : SortDirection.Descending;
			var orderBy3 = this.ddlOrderBy3.SelectedValue.Replace(" ASC", "").Replace(" DESC", "");
			var sortDirection3 = this.ddlOrderBy3.SelectedValue.EndsWith(" ASC") ? SortDirection.Ascending : SortDirection.Descending;
			var orderBy4 = this.ddlOrderBy4.SelectedValue.Replace(" ASC", "").Replace(" DESC", "");
			var sortDirection4 = this.ddlOrderBy4.SelectedValue.EndsWith(" ASC") ? SortDirection.Ascending : SortDirection.Descending;
			var orderBy5 = this.ddlOrderBy5.SelectedValue.Replace(" ASC", "").Replace(" DESC", "");
			var sortDirection5 = this.ddlOrderBy5.SelectedValue.EndsWith(" ASC") ? SortDirection.Ascending : SortDirection.Descending;

			var scholarshipStudents = ScholarshipStudents.GetScholarshipStudents(semesterID, programID, semesterNo, gpa, regular, position, enrollment, orderBy1, sortDirection1, orderBy2, sortDirection2, orderBy3, sortDirection3, orderBy4, sortDirection4, orderBy5, sortDirection5, pageIndex, this.gvScholarshipStudents.PageSize, out int virtualItemsCount, this.StaffIdentity.LoginSessionGuid);
			this.gvScholarshipStudents.DataBind(scholarshipStudents, pageIndex, virtualItemsCount);
		}

		protected void btnRecalculate_OnClick(object sender, EventArgs e)
		{
			var overwrite = sender == this.btnRecalculateOverwrite;
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var result = ScholarshipStudents.CalculateAll(semesterID, overwrite, this.StaffIdentity.LoginSessionGuid);
			switch (result)
			{
				case ScholarshipStudents.CalculateAllResults.Success:
					this.AddSuccessAlert($"Data has been compiled for {semesterID.ToSemesterString()}.");
					break;
				case ScholarshipStudents.CalculateAllResults.AlreadyCalculated:
					this.AddWarningAlert($"Data is already compiled for {semesterID.ToSemesterString()}.");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.GetData(0);
		}

		protected void gvScholarshipStudents_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					var scholarshipStudentID = e.DecryptedCommandArgumentToInt();
					var scholarshipStudent = ScholarshipStudents.GetScholarshipStudent(scholarshipStudentID, this.StaffIdentity.LoginSessionGuid);
					if (scholarshipStudent == null)
					{
						this.AddNoRecordFoundAlert();
						this.GetData(0);
						return;
					}
					this.lblEnrollment.Text = scholarshipStudent.Enrollment;
					this.lblName.Text = scholarshipStudent.Name;
					this.ddlExcluded.FillYesNo().SetBooleanValue(scholarshipStudent.Excluded);
					this.tbRemarks.Text = scholarshipStudent.Remarks;
					this.ViewState[nameof(ScholarshipStudent.ScholarshipStudentID)] = scholarshipStudentID;
					this.modalEdit.Show();
					break;
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var scholarshipStudentID = (int)this.ViewState[nameof(ScholarshipStudent.ScholarshipStudentID)];
			var excluded = this.ddlExcluded.SelectedValue.ToBoolean();
			var remarks = this.tbRemarks.Text;
			var status = Aspire.BL.Core.Scholarship.Staff.ScholarshipStudents.UpdateScholarshipStudents(scholarshipStudentID, excluded, remarks, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case ScholarshipStudents.UpdateScholarshipStudentsStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.GetData(0);
					return;
				case ScholarshipStudents.UpdateScholarshipStudentsStatuses.Success:
					this.AddSuccessAlert("Record has been updated.");
					this.GetData(this.gvScholarshipStudents.PageIndex);
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void btnExportToCSV_OnClick(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var orderBy1 = this.ddlOrderBy1.SelectedValue.Replace(" ASC", "").Replace(" DESC", "");
			var sortDirection1 = this.ddlOrderBy1.SelectedValue.EndsWith(" ASC") ? SortDirection.Ascending : SortDirection.Descending;
			var orderBy2 = this.ddlOrderBy2.SelectedValue.Replace(" ASC", "").Replace(" DESC", "");
			var sortDirection2 = this.ddlOrderBy2.SelectedValue.EndsWith(" ASC") ? SortDirection.Ascending : SortDirection.Descending;
			var orderBy3 = this.ddlOrderBy3.SelectedValue.Replace(" ASC", "").Replace(" DESC", "");
			var sortDirection3 = this.ddlOrderBy3.SelectedValue.EndsWith(" ASC") ? SortDirection.Ascending : SortDirection.Descending;
			var orderBy4 = this.ddlOrderBy4.SelectedValue.Replace(" ASC", "").Replace(" DESC", "");
			var sortDirection4 = this.ddlOrderBy4.SelectedValue.EndsWith(" ASC") ? SortDirection.Ascending : SortDirection.Descending;
			var orderBy5 = this.ddlOrderBy5.SelectedValue.Replace(" ASC", "").Replace(" DESC", "");
			var sortDirection5 = this.ddlOrderBy5.SelectedValue.EndsWith(" ASC") ? SortDirection.Ascending : SortDirection.Descending;

			var scholarshipStudents = ScholarshipStudents.GetScholarshipStudents(semesterID, null, null, null, null, null, null, orderBy1, sortDirection1, orderBy2, sortDirection2, orderBy3, sortDirection3, orderBy4, sortDirection4, orderBy5, sortDirection5, 0, int.MaxValue, out int virtualItemsCount, this.StaffIdentity.LoginSessionGuid);
			var csv = scholarshipStudents.ConvertToCSVFormat(new List<CSVHelper.CSVColumnMapping>
			{
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, PropertyName = nameof(ScholarshipStudents.ScholarshipStudent.Enrollment), ColumnName = "Enrollment"},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 1, PropertyName = nameof(ScholarshipStudents.ScholarshipStudent.Name), ColumnName = "Name"},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 2, PropertyName = nameof(ScholarshipStudents.ScholarshipStudent.ProgramAlias), ColumnName = "Program"},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 3, PropertyName = nameof(ScholarshipStudents.ScholarshipStudent.SemesterNoFullName), ColumnName = "Sem#"},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 4, PropertyName = nameof(ScholarshipStudents.ScholarshipStudent.RegisteredCoursesCount), ColumnName = "Courses"},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 5, PropertyName = nameof(ScholarshipStudents.ScholarshipStudent.WorkloadCreditHours), ColumnName = "Workload Credits"},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 6, PropertyName = nameof(ScholarshipStudents.ScholarshipStudent.RegisteredCreditHours), ColumnName = "Credits"},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 7, PropertyName = nameof(ScholarshipStudents.ScholarshipStudent.GPACreditHours), ColumnName = "GPA Credits"},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 8, PropertyName = nameof(ScholarshipStudents.ScholarshipStudent.GPA), ColumnName = "GPA"},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 9, PropertyName = nameof(ScholarshipStudents.ScholarshipStudent.Percentage), ColumnName = "%age"},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 10, PropertyName = nameof(ScholarshipStudents.ScholarshipStudent.RegisteredCoursesNotGraded), ColumnName = "Courses Not Graded"},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 11, PropertyName = nameof(ScholarshipStudents.ScholarshipStudent.RegularYesNo), ColumnName = "Regular"},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 12, PropertyName = nameof(ScholarshipStudents.ScholarshipStudent.OfferedClasses), ColumnName = "Classes"},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 13, PropertyName = nameof(ScholarshipStudents.ScholarshipStudent.Position), ColumnName = "Position"},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 14, PropertyName = nameof(ScholarshipStudents.ScholarshipStudent.ExcludedYesNo), ColumnName = "Excluded"},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 15, PropertyName = nameof(ScholarshipStudents.ScholarshipStudent.Remarks), ColumnName = "Remarks"},
			});
			var bytes = Encoding.ASCII.GetBytes(csv);
			this.Response.Clear();
			this.Response.ContentType = "text/csv";
			this.Response.AddHeader("Content-Length", bytes.Length.ToString());
			this.Response.AddHeader("Content-disposition", $"attachment; filename=\"Merit Scholarship - {semesterID.ToSemesterString()}.csv" + "\"");
			this.Response.BinaryWrite(bytes);
			this.Response.Flush();
			this.Response.End();
		}
	}
}