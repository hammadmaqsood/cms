﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Scholarship
{
	[AspirePage("BDC803ED-3313-45C5-B3C5-1D252AC72091", UserTypes.Staff, "~/Sys/Staff/Scholarship/SemesterWorkload.aspx", "Semester Workload", true, AspireModules.Scholarship)]
	public partial class SemesterWorkload : StaffPage
	{
		public override PageIcon PageIcon => AspireGlyphicons.pencil.GetIcon();
		public override string PageTitle => "Semester Workload";

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Scholarship.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public static string GetPageUrl(short? semesterID, int? programID)
		{
			return typeof(SemesterWorkload).GetAspirePageAttribute().PageUrl.AttachQueryParams("SemesterID", semesterID, "ProgramID", programID);
		}

		public static void Redirect(short? semesterID, int? programID)
		{
			Redirect(GetPageUrl(semesterID, programID));
		}

		private void Refresh()
		{
			Redirect(this.ddlSemesterIDFilter.SelectedValue.ToNullableShort(), this.ddlProgramIDFilter.SelectedValue.ToNullableInt());
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters().SetSelectedValueIfExists(this.Request.GetParameterValue("SemesterID"));
				this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, null, true, CommonListItems.All).SetSelectedValueIfExists(this.Request.GetParameterValue("ProgramID")); ;
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlProgramIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var workloads = Aspire.BL.Core.Scholarship.Staff.SemesterWorkload.GetScholarshipWorkloads(semesterID, programID, this.StaffIdentity.LoginSessionGuid);

			var crossTab = workloads.GroupBy(w => new
			{
				w.SemesterID,
				w.ProgramID
			}).Select(w => new
			{
				w.Key.ProgramID,
				w.First().ProgramAlias,
				SemesterZero = w.Where(sw => sw.SemesterNoEnum == SemesterNos.Zero).Select(sw => new { sw.OfferedCreditHours, sw.CreditHours }).SingleOrDefault(),
				SemesterOne = w.Where(sw => sw.SemesterNoEnum == SemesterNos.One).Select(sw => new { sw.OfferedCreditHours, sw.CreditHours }).SingleOrDefault(),
				SemesterTwo = w.Where(sw => sw.SemesterNoEnum == SemesterNos.Two).Select(sw => new { sw.OfferedCreditHours, sw.CreditHours }).SingleOrDefault(),
				SemesterThree = w.Where(sw => sw.SemesterNoEnum == SemesterNos.Three).Select(sw => new { sw.OfferedCreditHours, sw.CreditHours }).SingleOrDefault(),
				SemesterFour = w.Where(sw => sw.SemesterNoEnum == SemesterNos.Four).Select(sw => new { sw.OfferedCreditHours, sw.CreditHours }).SingleOrDefault(),
				SemesterFive = w.Where(sw => sw.SemesterNoEnum == SemesterNos.Five).Select(sw => new { sw.OfferedCreditHours, sw.CreditHours }).SingleOrDefault(),
				SemesterSix = w.Where(sw => sw.SemesterNoEnum == SemesterNos.Six).Select(sw => new { sw.OfferedCreditHours, sw.CreditHours }).SingleOrDefault(),
				SemesterSeven = w.Where(sw => sw.SemesterNoEnum == SemesterNos.Seven).Select(sw => new { sw.OfferedCreditHours, sw.CreditHours }).SingleOrDefault(),
				SemesterEight = w.Where(sw => sw.SemesterNoEnum == SemesterNos.Eight).Select(sw => new { sw.OfferedCreditHours, sw.CreditHours }).SingleOrDefault(),
				SemesterNine = w.Where(sw => sw.SemesterNoEnum == SemesterNos.Nine).Select(sw => new { sw.OfferedCreditHours, sw.CreditHours }).SingleOrDefault(),
				SemesterTen = w.Where(sw => sw.SemesterNoEnum == SemesterNos.Ten).Select(sw => new { sw.OfferedCreditHours, sw.CreditHours }).SingleOrDefault(),
			})
			.OrderBy(w => w.ProgramAlias)
			.ToList();
			this.gvSemesterWorkload.Caption = workloads.FirstOrDefault()?.Semester;
			this.gvSemesterWorkload.DataBind(crossTab);
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var workloads = new Dictionary<int, Dictionary<SemesterNos, decimal?>>();

			foreach (GridViewRow row in this.gvSemesterWorkload.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow)
					continue;
				var programID = (int)this.gvSemesterWorkload.DataKeys[row.DataItemIndex].Value;
				var tbCreditHours0 = ((AspireTextBox)row.FindControl("tbCreditHours0")).Text.ToNullableDecimal();
				var tbCreditHours1 = ((AspireTextBox)row.FindControl("tbCreditHours1")).Text.ToNullableDecimal();
				var tbCreditHours2 = ((AspireTextBox)row.FindControl("tbCreditHours2")).Text.ToNullableDecimal();
				var tbCreditHours3 = ((AspireTextBox)row.FindControl("tbCreditHours3")).Text.ToNullableDecimal();
				var tbCreditHours4 = ((AspireTextBox)row.FindControl("tbCreditHours4")).Text.ToNullableDecimal();
				var tbCreditHours5 = ((AspireTextBox)row.FindControl("tbCreditHours5")).Text.ToNullableDecimal();
				var tbCreditHours6 = ((AspireTextBox)row.FindControl("tbCreditHours6")).Text.ToNullableDecimal();
				var tbCreditHours7 = ((AspireTextBox)row.FindControl("tbCreditHours7")).Text.ToNullableDecimal();
				var tbCreditHours8 = ((AspireTextBox)row.FindControl("tbCreditHours8")).Text.ToNullableDecimal();
				var tbCreditHours9 = ((AspireTextBox)row.FindControl("tbCreditHours9")).Text.ToNullableDecimal();
				var tbCreditHours10 = ((AspireTextBox)row.FindControl("tbCreditHours10")).Text.ToNullableDecimal();
				workloads.Add(programID, new Dictionary<SemesterNos, decimal?>
				{
					{SemesterNos.Zero, tbCreditHours0 },
					{SemesterNos.One, tbCreditHours1 },
					{SemesterNos.Two, tbCreditHours2 },
					{SemesterNos.Three, tbCreditHours3 },
					{SemesterNos.Four, tbCreditHours4 },
					{SemesterNos.Five, tbCreditHours5 },
					{SemesterNos.Six, tbCreditHours6 },
					{SemesterNos.Seven, tbCreditHours7 },
					{SemesterNos.Eight, tbCreditHours8 },
					{SemesterNos.Nine, tbCreditHours9 },
					{SemesterNos.Ten, tbCreditHours10},
				});
			}

			var status = Aspire.BL.Core.Scholarship.Staff.SemesterWorkload.UpdateWorkloads(this.ddlSemesterIDFilter.SelectedValue.ToShort(), workloads, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Scholarship.Staff.SemesterWorkload.UpdateWorkloadsStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.Refresh();
					return;
				case BL.Core.Scholarship.Staff.SemesterWorkload.UpdateWorkloadsStatuses.Success:
					this.AddSuccessAlert("Record has been saved.");
					this.Refresh();
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void gvSemesterWorkload_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Courses":
					var args = e.DecryptedCommandArgument().Split(':');
					var programID = args[0].ToInt();
					var semesterNo = (SemesterNos)args[1].ToShort();
					var offeredCourses = Aspire.BL.Core.Scholarship.Staff.SemesterWorkload.GetOfferedCourses(programID, this.ddlSemesterIDFilter.SelectedValue.ToShort(), this.StaffIdentity.LoginSessionGuid);
					this.inputSearchOfferedCourses.Text = $"{semesterNo.ToFullName()} (A)";
					this.modalOfferedCourses.Show();
					this.repeaterOfferedCourses.DataBind(offeredCourses);
					break;
			}
		}
	}
}