﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ScholarshipDashboard.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Scholarship.ScholarshipDashboard" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.BL.Core.Scholarship.Admin" %>
<%@ Import Namespace="Aspire.BL.Core.Scholarship.Staff" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <aspire:AspireLabel runat="server" Text="Intake Scholarship:" AssociatedControlID="ddlSemesterID" />
                <aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
            </div>
        </div>
    </div>
    <p></p>
    <aspire:AspireGridView ItemType="Aspire.BL.Core.Scholarship.Staff.ScholarshipDashboard.ScholarshipAnnouncement" runat="server" ID="gvScholarshipDashboard" Condensed="True" AutoGenerateColumns="False" AllowSorting="False">
        <Columns>
            <asp:TemplateField HeaderText="#">
                <ItemTemplate>
                    <%#: Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Scholarship" DataField="ScholarshipTypeFullName" />
            <asp:BoundField HeaderText="Opening Date" DataField="OpeningDate" DataFormatString="{0:d}" />
            <asp:BoundField HeaderText="Closing Date" DataField="ClosingDate" DataFormatString="{0:d}" />
            <asp:BoundField HeaderText="Applied" DataField="AppliedApplications" />
            <asp:BoundField HeaderText="In Progress" DataField="InProgressApplications" />
            <asp:BoundField HeaderText="Total" DataField="Total" />
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <aspire:AspireHyperLink runat="server" NavigateUrl="<%# Aspire.Web.Sys.Staff.Scholarship.ScholarshipDashboard.RedirectToScholarship(Item.ScholarshipTypeEnum, Item.ScholarshipAnnouncementID, Item.SemesterID.ToString())%>" Text="Details" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </aspire:AspireGridView>

    <aspire:AspirePanel runat="server" HeaderText="Links" ID="LinksPanel" ItemType="Aspire.BL.Core.Scholarship.Staff.ScholarshipDashboard.ScholarshipAnnouncement">
        <aspire:AspireHyperLinkButton runat="server" ID="hlNewApplication" Target="_blank" ButtonType="Default" Text="View Application" />
        <aspire:AspireHyperLinkButton runat="server" ID="hlQarzeHasanaList" Text="Qarz-e-Hasna List" Glyphicon="print" Target="_blank" />
        <aspire:AspireHyperLinkButton runat="server" ID="hlQarzeHasnaSemesterWiseList" Text="Qarz-e-Hasna Semester Wise List" Glyphicon="print" Target="_blank" />
        <aspire:AspireButton runat="server" ID="btnAddAllSemesterApprovals" ButtonType="Success" Glyphicon="plus" Text="Add All Semester Approvals" OnClick="btnAddAllSemesterApprovals_OnClick" />
        <aspire:AspireButton runat="server" ID="btnBulkRejection" ButtonType="Danger" Glyphicon="remove" Text="Bulk Rejection" OnClick="btnBulkRejection_OnClick" />
        <aspire:AspireButton runat="server" ID="btnUpdateFee" ButtonType="Success" Glyphicon="plus" Text="Update Fee" OnClick="btnUpdateFee_Click" />
    </aspire:AspirePanel>

    <%--AspireModalForBulk--%>
    <aspire:AspireModal runat="server" ModalSize="Large" ID="modalAddThisSemesterApprovals" HeaderText="Add All Semester Approvals for Existing Students">
        <BodyTemplate>
            <asp:UpdatePanel runat="server" ID="updateAddThisSemesterApprovals" UpdateMode="Conditional">
                <ContentTemplate>
                    <aspire:AspireAlert runat="server" ID="alertModalAddThisSemesterApprovals" />
                    <div class="form-group">
                        <aspire:AspireLabel runat="server" Text="Select Semester:" AssociatedControlID="ddlSemesterIDForApprovals" />
                        <aspire:AspireDropDownList runat="server" ID="ddlSemesterIDForApprovals" AutoPostBack="true" OnSelectedIndexChanged="ddlSemesterIDForApprovals_OnSelectedIndexChanged" />
                    </div>
                    <aspire:AspireGridView runat="server" ID="gvBulkApproval" DataKeyNames="ScholarshipApplicationID" ItemType="Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.ApplicationForApproval" AutoGenerateColumns="false">
                        <Columns>
                            <asp:TemplateField HeaderText="#">
                                <ItemTemplate><%#: Container.DataItemIndex + 1 %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Enrollment">
                                <ItemTemplate><%#: Item.Enrollment %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate><%#: Item.Name %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Program">
                                <ItemTemplate><%#: Item.ProgramAlias %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Scholarship Intake">
                                <ItemTemplate><%#: Item.AnnouncementSemesterID.ToSemesterString() %></ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </aspire:AspireGridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </BodyTemplate>
        <FooterTemplate>
            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateModalAddThisSemesterApprovalsFooter">
                <ContentTemplate>
                    <aspire:AspireButton runat="server" ID="btnGenerate" Text="Generate" CausesValidation="false" OnClick="btnGenerate_OnClick" />
                    <aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
                </ContentTemplate>
            </asp:UpdatePanel>
        </FooterTemplate>
    </aspire:AspireModal>

    <aspire:AspireModal runat="server" ModalSize="Large" ID="modalRejectSemesterApprovals" HeaderText="All Semester Approvals Bulk Rejection">
        <BodyTemplate>
            <asp:UpdatePanel runat="server" ID="updateModalRejectSemesterApprovals" UpdateMode="Conditional">
                <ContentTemplate>
                    <aspire:AspireAlert runat="server" ID="alertModalRejectSemesterApprovals" />
                    <div class="form-group">
                        <aspire:AspireLabel runat="server" Text="Select Semester to reject applications:" AssociatedControlID="ddlRejectionSemesterID" ValidationGroup="Rejection" />
                        <aspire:AspireDropDownList runat="server" ID="ddlRejectionSemesterID" CausesValidation="false" AutoPostBack="true" OnSelectedIndexChanged="ddlRejectionSemesterID_OnSelectedIndexChanged" ValidationGroup="Rejection" />
                    </div>
                    <aspire:AspireGridView runat="server" ID="gvBulkRejection" DataKeyNames="ScholarshipApplicationID" ItemType="Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.ApplicationForRejection" AutoGenerateColumns="false">
                        <Columns>
                            <asp:TemplateField HeaderText="#">
                                <ItemTemplate><%#: Container.DataItemIndex + 1 %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Enrollment">
                                <ItemTemplate><%#: Item.Enrollment %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate><%#: Item.Name %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Program">
                                <ItemTemplate><%#: Item.ProgramAlias %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Scholarship Intake">
                                <ItemTemplate><%#: Item.AnnouncementSemesterID.ToSemesterString() %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BU Exam GPA">
                                <ItemTemplate><%#: (Item.PreviousStudentSemester?.BUExamGPA?.FormatGPA()).ToNAIfNullOrEmpty() %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BU Exam CGPA">
                                <ItemTemplate><%#: (Item.PreviousStudentSemester?.BUExamCGPA?.FormatGPA()).ToNAIfNullOrEmpty() %></ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </aspire:AspireGridView>
                    <div class="form-group">
                        <aspire:AspireLabel runat="server" ID="lblModalCampusRemarks" Visible="False" AssociatedControlID="lblModalCampusRemarks" Text="Campus Remarks" />
                        <aspire:AspireTextBox runat="server" ID="tbModalCampusRemarks" MaxLength="1000" TextMode="MultiLine" ValidationGroup="Rejection" Visible="False" />
                        <aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbModalCampusRemarks" ErrorMessage="This field is required." ValidationGroup="Rejection" />
                    </div>
                    <aspire:AspireLabel Font-Bold="True" runat="server" ID="lblRejectionText" Visible="False" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </BodyTemplate>
        <FooterTemplate>
            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateModalRejectsFooter">
                <ContentTemplate>
                    <aspire:AspireButton runat="server" ID="btnProceed" Text="Proceed" OnClick="btnRejectionProceed_OnClick" ValidationGroup="Rejection" />
                    <aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
                </ContentTemplate>
            </asp:UpdatePanel>
        </FooterTemplate>
    </aspire:AspireModal>

    <aspire:AspireModal runat="server" ModalSize="Large" ID="modalUpdateFee" HeaderText="Update Fee in All Semester Approvals">
        <BodyTemplate>
            <asp:UpdatePanel runat="server" ID="updatePanelUpdateFee" UpdateMode="Conditional">
                <ContentTemplate>
                    <aspire:AspireAlert runat="server" ID="alertUpdateFee" />
                    <div class="form-group">
                        <aspire:AspireLabel runat="server" Text="Select Semester to Add Fee against applications approvals:" AssociatedControlID="ddlFeeSemesterID" ValidationGroup="UpdateFee" />
                        <aspire:AspireDropDownList runat="server" ID="ddlFeeSemesterID" CausesValidation="false" AutoPostBack="true" OnSelectedIndexChanged="ddlFeeSemesterID_SelectedIndexChanged" ValidationGroup="UpdateFee" />
                    </div>

                    <asp:Repeater ItemType="Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.ApplicationsForUpdaetFee" runat="server" ID="rpUpdateFee" OnItemDataBound="rpUpdateFee_ItemDataBound">
                        <HeaderTemplate>
                            <div class="table-responsive">
                                <table class="table table-bordered table-condensed table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>
                                                <aspire:AspireCheckBox runat="server" CssClass="SelectAll" />
                                            </th>
                                            <th>#</th>
                                            <th>Enrollment</th>
                                            <th>Name</th>
                                            <th>Program</th>
                                            <th>Scholarship
                                                <br />
                                                Intake</th>
                                            <th>Calculated
                                                    <br />
                                                Tution Fee</th>
                                            <th>Tution
                                                    <br />
                                                Fee</th>
                                        </tr>
                                    </thead>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class='<%# Item.CalculatedTutionFee == null ? "danger" : (Item.CalculatedTutionFee != null && Item.ScholarshipApplicationApprovalData.TuitionFee != null && Item.CalculatedTutionFee != Item.ScholarshipApplicationApprovalData.TuitionFee) ? "danger" : "" %>'>
                                <td>
                                    <asp:HiddenField Visible="false" runat="server" ID="hfScholarshipApplicationID" Value="<%# Item.ScholarshipApplicationID %>" />
                                    <aspire:AspireCheckBox runat="server" ValidationGroup="SelectStudent" ID="cbSelect" CssClass="Select" />
                                </td>
                                <td><%#: Container.ItemIndex+ 1 %></td>
                                <td><%#: Item.Enrollment %></td>
                                <td><%#: Item.Name %></td>
                                <td><%#: Item.ProgramAlias %></td>
                                <td><%#: Item.AnnouncementSemesterID.ToSemesterString() %></td>
                                <td>
                                    <asp:HiddenField Visible="false" runat="server" ID="hfCalculatedFee" Value="<%#: Item.CalculatedTutionFee %>" />
                                    <%#: Item.CalculatedTutionFee.ToNAIfNullOrEmpty() %>
                                </td>
                                <td>
                                    <asp:HiddenField Visible="false" runat="server" ID="hfAddedFee" Value="<%#: Item.ScholarshipApplicationApprovalData.TuitionFee %>" />
                                    <%#: Item.ScholarshipApplicationApprovalData.TuitionFee.ToNAIfNullOrEmpty() %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
			                    </div>
                        </FooterTemplate>
                    </asp:Repeater>
                    <table runat="server" id="trNoRecordFound" class="table table-bordered table-condensed table-hover table-striped">
                        <tr>
                            <td colspan="8">No record found.</td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </BodyTemplate>
        <FooterTemplate>
            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <aspire:AspireButton runat="server" ID="btnUpdateFeeProceed" Text="Proceed" OnClick="btnUpdateFeeProceed_Click" ValidationGroup="UpdateFee" />
                    <aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
                </ContentTemplate>
            </asp:UpdatePanel>
        </FooterTemplate>
    </aspire:AspireModal>
    <%--AspireModalForBulk--%>

    <script type="text/javascript">
		function CheckboxSelection() {
			$(function () {
				$(".SelectAll input[type=\"checkbox\"]").change(function () {
					$(".Select input[type=\"checkbox\"]").not(":disabled").prop("checked", $(this).is(":checked"));
					enableDisableFeeProceedBtn();
				});

				$(".Select input[type=\"checkbox\"]").change(function () {
					$(".SelectAll input[type=\"checkbox\"]").prop("checked", $(".Select input[type=\"checkbox\"]:not(:checked)").length === 0);
					enableDisableFeeProceedBtn();
				});
			});

			function enableDisableFeeProceedBtn() {
				var enabled = $(".Select input[type=\"checkbox\"]:checked").length > 0;
				$("#<%=this.btnUpdateFeeProceed.ClientID%>").attr("data-enabled", enabled).toggleClass("disabled", !enabled);
			}
			enableDisableFeeProceedBtn();
		}
		Sys.Application.add_load(CheckboxSelection);
    </script>
</asp:Content>
