﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Approvals.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Scholarship.QarzeHasana.Approvals" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">

    <div class="table-responsive">
        <table class="table table-bordered table-condensed info tableCol4">
            <tr>
                <th>Enrollment</th>
                <td>
                    <aspire:AspireLabel runat="server" ID="lblEnrollment" />
                </td>
                <th>Name</th>
                <td>
                    <aspire:AspireLabel runat="server" ID="lblName" />
                </td>
            </tr>
            <tr>
                <th>Class</th>
                <td>
                    <aspire:AspireLabel runat="server" ID="lblClass" />
                </td>
                <th>Last Result</th>
                <td>
                    <aspire:AspireLabel runat="server" ID="lblLastResult" />
                </td>
            </tr>
            <tr>
                <th>Final Semester</th>
                <td>
                    <aspire:AspireLabel runat="server" ID="lblFinalSemester" />
                </td>
                <th>Scholarship Intake</th>
                <td>
                    <aspire:AspireLabel runat="server" ID="lblScholarshipIntake" />
                </td>
            </tr>
        </table>
    </div>

    <aspire:AspireButton runat="server" ID="btnAddApplicationApproval" ButtonType="Success" Glyphicon="plus" Text="Add" OnClick="btnAddApplicationApproval_OnClick" />
    <p></p>
    <asp:Repeater ItemType="Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.ApplicationApproval" runat="server" ID="repeaterApplicationApprovals" OnItemCommand="repeaterApplicationApprovals_ItemCommand">
        <HeaderTemplate>
            <div class="table-responsive">
                <table class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                        <tr>
                            <th rowspan="2">#</th>
                            <th rowspan="2">Semester</th>
                            <th rowspan="2">Tution Fee</th>
                            <th colspan="5">Recommendations</th>
                            <th rowspan="2">Scholarship Amount</th>
                            <th rowspan="2">Is Cheque<br />
                                Ready?</th>
                            <th rowspan="2">Approval Status</th>
                            <th rowspan="2">Action</th>
                        </tr>
                        <tr>
                            <th>Campus<br />
                                (%)</th>
                            <th>Campus Remarks</th>
                            <th>
                                <abbr title="Central Scholarship Committee" class="initialism">CSC</abbr><br />
                                (%)</th>
                            <th>
                                <abbr title="Central Scholarship Committee" class="initialism">CSC</abbr>
                                Remarks</th>
                            <th>Approval<br />
                                (%)</th>
                        </tr>
                    </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><%#: Container.ItemIndex+ 1 %></td>
                <td><%#: Item.SemesterID.ToSemesterString() %></td>
                <td><%#: Item.ScholarshipApplicationApproval.TuitionFee.ToNAIfNullOrEmpty() %></td>
                <td><%#: Item.ScholarshipApplicationApproval.CampusRecommendation.ToNAIfNullOrEmpty() %></td>
                <td><%#: Item.ScholarshipApplicationApproval.CampusRemarks.ToNAIfNullOrEmpty() %></td>
                <td><%#: Item.ScholarshipApplicationApproval.CSCRecommendation.ToNAIfNullOrEmpty() %></td>
                <td><%#: Item.ScholarshipApplicationApproval.CSCRemarks.ToNAIfNullOrEmpty() %></td>
                <td><%#: Item.ScholarshipApplicationApproval.FinalRecommendation.ToNAIfNullOrEmpty() %></td>
                <td><%#: Item.ScholarshipApplicationApproval.GetApprovedAmount(Item.ApplicationApprovalStatusEnum) %></td>
                <td>
                    <aspire:AspireLinkButton runat="server" CommandName="StudentChequeStatus" Text="<%# Item.ChequeStatusEnum.ToFullName() %>" EncryptedCommandArgument="<%# Item.ScholarshipApplicationApprovalID %>" ConfirmMessage="Are you sure you want to continue?" Enabled="<%# Item.ActionTypeForChequeBtn %>" />
                </td>
                <td><%#: Item.ApplicationApprovalStatusEnum %></td>
                <td>
                    <aspire:AspireLinkButton runat="server" ToolTip="Edit" CausesValidation="False" CommandName="Edit" Glyphicon="edit" EncryptedCommandArgument="<%#: Item.ScholarshipApplicationApprovalID %>" />
                    <aspire:AspireLinkButton runat="server" ToolTip="Delete" ID="modalBtnDelete" CausesValidation="False" CommandName="Delete" Glyphicon="remove" EncryptedCommandArgument="<%#: Item.ScholarshipApplicationApprovalID %>" ConfirmMessage="Are you sure you want to delete it?" Visible="<%# Item.ActionTypeForDeleteBtn %>" />
                </td>
            </tr>
        </ItemTemplate>

        <FooterTemplate>
            </table>
			</div>
        </FooterTemplate>
    </asp:Repeater>

    <aspire:AspireModal runat="server" ID="modalApplicationApproval">
        <BodyTemplate>
            <asp:UpdatePanel runat="server" ID="updateApplicationApproval" UpdateMode="Conditional">
                <ContentTemplate>
                    <aspire:AspireAlert runat="server" ID="alertModalApplicationApproval" />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="lblModalName" />
                                <aspire:AspireLabel runat="server" ID="lblModalName" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="lblModalEnrollment" />
                                <aspire:AspireLabel runat="server" ID="lblModalEnrollment" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
                                <aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="Save" CssClass="col-md-6" />
                            </div>
                            <aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterID" ErrorMessage="This field is required." ValidationGroup="ApplicationApproval" />
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <aspire:AspireLabel runat="server" Text="Tution Fee:" AssociatedControlID="tbTutionFee" />
                                <aspire:AspireHyperLink runat="server" ID="hlCaluclatedFee" CssClass="pull-right" Text="Calculated Fee:" Target="_blank" />
                                <div class="input-group">
                                    <span class="input-group-addon">Rs.</span>
                                    <aspire:AspireTextBox runat="server" ID="tbTutionFee" ValidationGroup="ApplicationApproval" />
                                </div>
                                <aspire:AspireInt32Validator runat="server" ControlToValidate="tbTutionFee" ValidationGroup="ApplicationApproval" AllowNull="True" MinValue="0" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid Number." />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <aspire:AspireLabel runat="server" Text="Campus Recommendation:" AssociatedControlID="tbCampusRecommendation" />
                        <div class="input-group">
                            <aspire:AspireTextBox runat="server" ID="tbCampusRecommendation" ValidationGroup="ApplicationApproval" />
                            <span class="input-group-addon">%</span>
                        </div>
                        <aspire:AspireInt16Validator runat="server" AllowNull="true" ControlToValidate="tbCampusRecommendation" ValidationGroup="ApplicationApproval" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="1" MaxValue="100" />
                    </div>
                    <div class="form-group">
                        <aspire:AspireLabel runat="server" Text="Campus Remarks:" AssociatedControlID="tbCampusRemarks" />
                        <aspire:AspireTextBox runat="server" ID="tbCampusRemarks" MaxLength="500" TextMode="MultiLine" ValidationGroup="ApplicationApproval" />
                        <aspire:AspireStringValidator runat="server" ControlToValidate="tbCampusRemarks" ValidationGroup="ApplicationApproval" AllowNull="True" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." />
                    </div>
                    <div class="form-group">
                        <aspire:AspireLabel runat="server" AssociatedControlID="tbCSCRecommendation"><abbr title="Central Scholarship Committee" class="initialism">CSC</abbr> Recommandations:</aspire:AspireLabel>
                        <div class="input-group">
                            <aspire:AspireTextBox runat="server" ID="tbCSCRecommendation" ValidationGroup="ApplicationApproval" />
                            <span class="input-group-addon">%</span>
                        </div>
                        <aspire:AspireInt16Validator runat="server" AllowNull="true" ControlToValidate="tbCSCRecommendation" ValidationGroup="ApplicationApproval" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="1" MaxValue="100" />
                    </div>
                    <div class="form-group">
                        <aspire:AspireLabel runat="server" AssociatedControlID="tbCSCRemarks"><abbr title="Central Scholarship Committee" class="initialism">CSC</abbr> Remarks:</aspire:AspireLabel>
                        <aspire:AspireTextBox runat="server" ID="tbCSCRemarks" MaxLength="500" TextMode="MultiLine" ValidationGroup="ApplicationApproval" />
                        <aspire:AspireStringValidator runat="server" ControlToValidate="tbCSCRemarks" ValidationGroup="ApplicationApproval" AllowNull="True" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." />
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <aspire:AspireLabel runat="server" Text="Approval:" AssociatedControlID="tbApproval" />
                                <div class="input-group">
                                    <aspire:AspireTextBox runat="server" ID="tbApproval" ValidationGroup="ApplicationApproval" />
                                    <span class="input-group-addon">%</span>
                                </div>
                                <aspire:AspireInt16Validator runat="server" AllowNull="true" ControlToValidate="tbApproval" ValidationGroup="ApplicationApproval" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="1" MaxValue="100" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <aspire:AspireLabel runat="server" Text="Approval Status:" AssociatedControlID="ddlApprovalStatus" />
                                <aspire:AspireDropDownList runat="server" ID="ddlApprovalStatus" ValidationGroup="ApplicationApproval" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </BodyTemplate>
        <FooterTemplate>
            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <aspire:AspireButton runat="server" ID="btnSaveApplicaitonApproval" Text="Save" ButtonType="Primary" ValidationGroup="ApplicationApproval" OnClick="btnSaveApplicationApproval_Click" />
                    <aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
                </ContentTemplate>
            </asp:UpdatePanel>
        </FooterTemplate>
    </aspire:AspireModal>
</asp:Content>
