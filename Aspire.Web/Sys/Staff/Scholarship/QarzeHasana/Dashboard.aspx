﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Scholarship.QarzeHasana.Dashboard" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Web.Sys.Staff.Students" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
    <aspire:AspireHyperLinkButton runat="server" NavigateUrl="../ScholarshipDashboard.aspx" Text="Scholarships" ButtonType="Default" Glyphicon="list" />
    <p></p>
    <asp:Panel runat="server" ID="panelBody">
        <div class="table-responsive">
            <table class="table table-bordered table-condensed info tableCol4">
                <tr>
                    <th>Intake Scholarship</th>
                    <td>
                        <aspire:AspireLabel runat="server" ID="lblSemester" />
                    </td>
                    <th>Opening (Dates)</th>
                    <td>
                        <aspire:AspireLabel runat="server" ID="lblDates" />
                    </td>
                </tr>
                <tr>
                    <th>Applied</th>
                    <td>
                        <aspire:AspireLabel runat="server" ID="lblApplied" />
                    </td>
                    <th>In Progress</th>
                    <td>
                        <aspire:AspireLabel runat="server" ID="lblInProgress" />
                    </td>
                </tr>
            </table>
        </div>

        <aspire:AspirePanel runat="server" HeaderText="Applicants">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <aspire:AspireButton runat="server" ID="btnAddThisSemesterApprovals" ButtonType="Success" Glyphicon="plus" Text="Add This Semester Approvals" OnClick="btnAddThisSemesterApprovals_OnClick" />
                        <aspire:AspireButton runat="server" ID="btnBulkRejection" ButtonType="Danger" Glyphicon="remove" Text="Bulk Rejection" OnClick="btnBulkRejection_OnClick" />
                        <aspire:AspireButton runat="server" ID="btnBulkAddApplications" ButtonType="Success" Glyphicon="plus" Text="Add Applications" OnClick="btnBulkAddApplications_OnClick" />
                        <aspire:AspireButton runat="server" ID="btnBulkAddFee" ButtonType="Success" Glyphicon="plus" Text="Update Fee" OnClick="btnUpdateFee_Click" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <asp:Panel runat="server" CssClass="input-group" DefaultButton="btnSearch">
                            <aspire:AspireTextBox runat="server" ID="tbSearch" PlaceHolder="Search" />
                            <div class="input-group-btn">
                                <aspire:AspireButton ButtonType="Default" Glyphicon="search" runat="server" ID="btnSearch" OnClick="btnSearch_OnClick" />
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <aspire:AspireLabel runat="server" Text="Is Fee Added:" AssociatedControlID="ddlFeeAdded" />
                        <aspire:AspireDropDownList runat="server" ID="ddlFeeAdded" OnSelectedIndexChanged="ddlFeeAdded_SelectedIndexChanged" AutoPostBack="true" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <aspire:AspireLabel runat="server" Text="Is Campus Remarks Added:" AssociatedControlID="ddlCampusRemarksAdded" />
                        <aspire:AspireDropDownList runat="server" ID="ddlCampusRemarksAdded" AutoPostBack="True" OnSelectedIndexChanged="ddlCampusRemarksAdded_SelectedIndexChanged" ValidationGroup="Filter" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <aspire:AspireLabel runat="server" AssociatedControlID="ddlCSCRemarksAdded">Is <abbr title="Central Scholarship Committee" class="initialism">CSC</abbr> Remarks Added:</aspire:AspireLabel>
                        <aspire:AspireDropDownList runat="server" ID="ddlCSCRemarksAdded" AutoPostBack="True" OnSelectedIndexChanged="ddlCSCRemarksAdded_SelectedIndexChanged" ValidationGroup="Filter" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <aspire:AspireLabel runat="server" Text="Last Status:" AssociatedControlID="ddlApprovalStatus" />
                        <aspire:AspireDropDownList runat="server" ID="ddlApprovalStatus" AutoPostBack="True" OnSelectedIndexChanged="ddlApprovalStatus_SelectedIndexChanged" ValidationGroup="Filter" />
                    </div>
                </div>
            </div>
            <%--<div class="row">
				<div class="col-md-12">
					<div class="form-inline">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Fee Added:" AssociatedControlID="ddlFeeAdded" />
							<aspire:AspireDropDownList runat="server" ID="ddlFeeAdded" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlFeeAdded_SelectedIndexChanged" />
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Campus Remarks Added:" AssociatedControlID="ddlCampusRemarksAdded" />
							<aspire:AspireDropDownList runat="server" ID="ddlCampusRemarksAdded" AutoPostBack="True" OnSelectedIndexChanged="ddlCampusRemarksAdded_SelectedIndexChanged" ValidationGroup="Filter" />
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" AssociatedControlID="ddlCSCRemarksAdded"><abbr title="Central Scholarship Committee" class="initialism">CSC</abbr> Remarks Added:</aspire:AspireLabel>
							<aspire:AspireDropDownList runat="server" ID="ddlCSCRemarksAdded" AutoPostBack="True" OnSelectedIndexChanged="ddlCSCRemarksAdded_SelectedIndexChanged" ValidationGroup="Filter" />
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Last Status:" AssociatedControlID="ddlApprovalStatus" />
							<aspire:AspireDropDownList runat="server" ID="ddlApprovalStatus" AutoPostBack="True" OnSelectedIndexChanged="ddlApprovalStatus_SelectedIndexChanged" ValidationGroup="Filter" />
						</div>
					</div>
				</div>
			</div>
			<p></p>--%>
            <aspire:AspireGridView ItemType="Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.Applicant" runat="server" ID="gvApplicants" Responsive="True" Condensed="True" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnSorting="gvApplicants_OnSorting" OnPageIndexChanging="gvApplicants_OnPageIndexChanging" OnPageSizeChanging="gvApplicants_OnPageSizeChanging">
                <Columns>
                    <asp:TemplateField HeaderText="#">
                        <ItemTemplate>
                            <%#: Container.DataItemIndex + 1 %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Enrollment" SortExpression="Enrollment">
                        <ItemTemplate>
                            <aspire:AspireHyperLink runat="server" Text='<%# Item.Enrollment %>' NavigateUrl='<%# StudentProfileView.GetPageUrl(Item.Enrollment, true) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Name" DataField="Name" SortExpression="Name" />
                    <asp:BoundField HeaderText="Program" DataField="ProgramAlias" SortExpression="ProgramAlias" />
                    <asp:TemplateField HeaderText="Final Semester" SortExpression="FinalSemesterID">
                        <ItemTemplate>
                            <%#: Item.FinalSemesterID.ToSemesterString() %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Last Status">
                        <ItemTemplate>
                            <%#: Item.LastStatus %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Old Application">
                        <ItemTemplate>
                            <%#: Item.IsOldApplication %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Actions">
                        <ItemTemplate>
                            <aspire:AspireHyperLink runat="server" NavigateUrl="<%# Aspire.Web.Sys.Staff.Scholarship.QarzeHasana.Approvals.GetPageURL(Item.ScholarshipApplicationID, Item.SemesterID) %>" Text="Status" Target="_blank" ID="gvBtnViewStatus" />
                            <aspire:AspireHyperLink runat="server" NavigateUrl="<%# Aspire.Web.Sys.Staff.Scholarship.ApplicationsStatus.GetPageURLForScholarshipApplicationForm(Item.ScholarshipTypeEnum, Item.ScholarshipAnnouncementID, Item.StudentID) %>" Text="Application" Target="_blank" ID="gvBtnViewForm" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </aspire:AspireGridView>
        </aspire:AspirePanel>

        <%--AspireModalForBulk--%>
        <aspire:AspireModal runat="server" ModalSize="Large" ID="modalAddThisSemesterApprovals" HeaderText="Add This Semester Approvals for Existing Students">
            <BodyTemplate>
                <asp:UpdatePanel runat="server" ID="updateAddThisSemesterApprovals" UpdateMode="Conditional">
                    <ContentTemplate>
                        <aspire:AspireAlert runat="server" ID="alertModalAddThisSemesterApprovals" />
                        <div>
                            <aspire:AspireLabel runat="server" Text="Scholarship Intake:" AssociatedControlID="lblSelectedSemester" />
                            <aspire:AspireLabel runat="server" ID="lblSelectedSemester" />
                        </div>
                        <div class="form-group">
                            <aspire:AspireLabel runat="server" Text="Select Semester:" AssociatedControlID="ddlSemesterID" />
                            <aspire:AspireDropDownList runat="server" ID="ddlSemesterID" AutoPostBack="true" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
                        </div>
                        <aspire:AspireGridView runat="server" ID="gvBulkApproval" DataKeyNames="ScholarshipApplicationID" ItemType="Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.ApplicationForApproval" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField HeaderText="#">
                                    <ItemTemplate><%#: Container.DataItemIndex + 1 %></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Enrollment">
                                    <ItemTemplate><%#: Item.Enrollment %></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate><%#: Item.Name %></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Program">
                                    <ItemTemplate><%#: Item.ProgramAlias %></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Scholarship Intake">
                                    <ItemTemplate><%#: Item.AnnouncementSemesterID.ToSemesterString() %></ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </aspire:AspireGridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </BodyTemplate>
            <FooterTemplate>
                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateModalAddThisSemesterApprovalsFooter">
                    <ContentTemplate>
                        <aspire:AspireButton runat="server" ID="btnGenerate" Text="Generate" CausesValidation="false" OnClick="btnGenerate_OnClick" />
                        <aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </FooterTemplate>
        </aspire:AspireModal>

        <aspire:AspireModal runat="server" ModalSize="Large" ID="modalRejectSemesterApprovals" HeaderText="Bulk Rejection">
            <BodyTemplate>
                <asp:UpdatePanel runat="server" ID="updateModalRejectSemesterApprovals" UpdateMode="Conditional">
                    <ContentTemplate>
                        <aspire:AspireAlert runat="server" ID="alertModalRejectSemesterApprovals" />
                        <div class="form-group">
                            <aspire:AspireLabel runat="server" Text="Select Semester to reject applications:" AssociatedControlID="ddlRejectionSemesterID" ValidationGroup="Rejection" />
                            <aspire:AspireDropDownList runat="server" ID="ddlRejectionSemesterID" CausesValidation="false" AutoPostBack="true" OnSelectedIndexChanged="ddlRejectionSemesterID_OnSelectedIndexChanged" ValidationGroup="Rejection" />
                        </div>
                        <aspire:AspireGridView runat="server" ID="gvBulkRejection" DataKeyNames="ScholarshipApplicationID" ItemType="Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.ApplicationForRejection" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField HeaderText="#">
                                    <ItemTemplate><%#: Container.DataItemIndex + 1 %></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Enrollment">
                                    <ItemTemplate><%#: Item.Enrollment %></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate><%#: Item.Name %></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Program">
                                    <ItemTemplate><%#: Item.ProgramAlias %></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Scholarship Intake">
                                    <ItemTemplate><%#: Item.AnnouncementSemesterID.ToSemesterString() %></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BU Exam GPA">
                                    <ItemTemplate><%#: (Item.PreviousStudentSemester?.BUExamGPA?.FormatGPA()).ToNAIfNullOrEmpty() %></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BU Exam CGPA">
                                    <ItemTemplate><%#: (Item.PreviousStudentSemester?.BUExamCGPA?.FormatGPA()).ToNAIfNullOrEmpty() %></ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </aspire:AspireGridView>
                        <div class="form-group">
                            <aspire:AspireLabel runat="server" ID="lblModalCampusRemarks" Visible="False" AssociatedControlID="lblModalCampusRemarks" Text="Campus Remarks" />
                            <aspire:AspireTextBox runat="server" ID="tbModalCampusRemarks" MaxLength="1000" TextMode="MultiLine" ValidationGroup="Rejection" Visible="False" />
                            <aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbModalCampusRemarks" ErrorMessage="This field is required." ValidationGroup="Rejection" />
                        </div>
                        <aspire:AspireLabel Font-Bold="True" runat="server" ID="lblRejectionText" Visible="False" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </BodyTemplate>
            <FooterTemplate>
                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateModalRejectsFooter">
                    <ContentTemplate>
                        <aspire:AspireButton runat="server" ID="btnProceed" Text="Proceed" OnClick="btnRejectionProceed_OnClick" ValidationGroup="Rejection" />
                        <aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </FooterTemplate>
        </aspire:AspireModal>

        <aspire:AspireModal runat="server" ModalSize="Large" ID="modalAddApplications" HeaderText="Bulk Add Applications">
            <BodyTemplate>
                <asp:UpdatePanel runat="server" ID="updateModalAddApplications" UpdateMode="Conditional">
                    <ContentTemplate>
                        <aspire:AspireAlert runat="server" ID="alertModalAddApplications" />
                        <div class="form-group">
                            <aspire:AspireLabel runat="server" ID="lblEnterEnrollment" AssociatedControlID="tbAddApplicationsText" Text="Enter All Enrollments" />
                            <aspire:AspireTextBox runat="server" ID="tbAddApplicationsText" MaxLength="1000" TextMode="MultiLine" ValidationGroup="AddApplications" />
                            <aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbAddApplicationsText" ErrorMessage="This field is required." ValidationGroup="AddApplications" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </BodyTemplate>
            <FooterTemplate>
                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateModalAddApplicationsFooter">
                    <ContentTemplate>
                        <aspire:AspireButton runat="server" ID="btn" Text="Proceed" OnClick="btnAddApplicationsGenerate_OnClick" ValidationGroup="AddApplications" />
                        <aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </FooterTemplate>
        </aspire:AspireModal>

        <aspire:AspireModal runat="server" ModalSize="Large" ID="modalUpdateFee" HeaderText="Update Fee in this Semester Approvals">
            <BodyTemplate>
                <asp:UpdatePanel runat="server" ID="updatePanelUpdateFee" UpdateMode="Conditional">
                    <ContentTemplate>
                        <aspire:AspireAlert runat="server" ID="alertUpdateFee" />
                        <div class="form-group">
                            <aspire:AspireLabel runat="server" Text="Select Semester to Add Fee against applications approvals:" AssociatedControlID="ddlFeeSemesterID" ValidationGroup="UpdateFee" />
                            <aspire:AspireDropDownList runat="server" ID="ddlFeeSemesterID" CausesValidation="false" AutoPostBack="true" OnSelectedIndexChanged="ddlFeeSemesterID_SelectedIndexChanged" ValidationGroup="UpdateFee" />
                        </div>

                        <asp:Repeater ItemType="Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.ApplicationsForUpdaetFee" runat="server" ID="rpUpdateFee" OnItemDataBound="rpUpdateFee_ItemDataBound">
                            <HeaderTemplate>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <aspire:AspireCheckBox runat="server" CssClass="SelectAll" />
                                                </th>
                                                <th>#</th>
                                                <th>Enrollment</th>
                                                <th>Name</th>
                                                <th>Program</th>
                                                <th>Scholarship
                                                <br />
                                                    Intake</th>
                                                <th>Calculated
                                                    <br />
                                                    Tution Fee</th>
                                                <th>Tution
                                                    <br />
                                                    Fee</th>
                                            </tr>
                                        </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class='<%# Item.CalculatedTutionFee == null ? "danger" : (Item.CalculatedTutionFee != null && Item.ScholarshipApplicationApprovalData.TuitionFee != null && Item.CalculatedTutionFee != Item.ScholarshipApplicationApprovalData.TuitionFee) ? "danger" : "" %>'>
                                    <td>
                                        <asp:HiddenField Visible="false" runat="server" ID="hfScholarshipApplicationID" Value="<%# Item.ScholarshipApplicationID %>" />
                                        <aspire:AspireCheckBox runat="server" ValidationGroup="SelectStudent" ID="cbSelect" CssClass="Select" />
                                    </td>
                                    <td><%#: Container.ItemIndex+ 1 %></td>
                                    <td><%#: Item.Enrollment %></td>
                                    <td><%#: Item.Name %></td>
                                    <td><%#: Item.ProgramAlias %></td>
                                    <td><%#: Item.AnnouncementSemesterID.ToSemesterString() %></td>
                                    <td>
                                        <asp:HiddenField Visible="false" runat="server" ID="hfCalculatedFee" Value="<%#: Item.CalculatedTutionFee %>" />
                                        <%#: Item.CalculatedTutionFee.ToNAIfNullOrEmpty() %>
                                    </td>
                                    <td>
                                        <asp:HiddenField Visible="false" runat="server" ID="hfAddedFee" Value="<%#: Item.ScholarshipApplicationApprovalData.TuitionFee %>" />
                                        <%#: Item.ScholarshipApplicationApprovalData.TuitionFee.ToNAIfNullOrEmpty() %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
			                    </div>
                            </FooterTemplate>
                        </asp:Repeater>
                        <table runat="server" id="trNoRecordFound" class="table table-bordered table-condensed table-hover table-striped">
                            <tr>
                                <td colspan="8">No record found.</td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </BodyTemplate>
            <FooterTemplate>
                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <aspire:AspireButton runat="server" ID="btnUpdateFeeProceed" Text="Proceed" OnClick="btnUpdateFeeProceed_Click" ValidationGroup="UpdateFee" />
                        <aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </FooterTemplate>
        </aspire:AspireModal>

        <%--AspireModalForBulk--%>
    </asp:Panel>

    <script type="text/javascript">
		function CheckboxSelection() {
			$(function () {
				$(".SelectAll input[type=\"checkbox\"]").change(function () {
					$(".Select input[type=\"checkbox\"]").not(":disabled").prop("checked", $(this).is(":checked"));
					enableDisableFeeProceedBtn();
				});

				$(".Select input[type=\"checkbox\"]").change(function () {
					$(".SelectAll input[type=\"checkbox\"]").prop("checked", $(".Select input[type=\"checkbox\"]:not(:checked)").length === 0);
					enableDisableFeeProceedBtn();
				});
			});

			function enableDisableFeeProceedBtn() {
				var enabled = $(".Select input[type=\"checkbox\"]:checked").length > 0;
				$("#<%=this.btnUpdateFeeProceed.ClientID%>").attr("data-enabled", enabled).toggleClass("disabled", !enabled);
			}
			enableDisableFeeProceedBtn();
		}
		Sys.Application.add_load(CheckboxSelection);
    </script>

</asp:Content>
