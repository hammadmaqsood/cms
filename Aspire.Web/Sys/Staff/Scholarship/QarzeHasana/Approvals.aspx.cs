﻿using Aspire.BL.Core.Scholarship.Staff.QarzeHasana;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Scholarship.QarzeHasana
{
	[AspirePage("06542A45-7953-4210-8CF6-D8ECA725BC02", UserTypes.Staff, "~/Sys/Staff/Scholarship/QarzeHasana/Approvals.aspx", "Application Approvals", true, AspireModules.Scholarship)]
	public partial class Approvals : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_graduation_cap);
		public override string PageTitle => "Qarz-e-Hasna Application Approvals";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Scholarship.Module, UserGroupPermission.Allowed},
		};

		public static string GetPageURL(int scholarshipApplicationID, short? semesterID)
		{
			return typeof(Approvals).GetAspirePageAttribute().PageUrl.AttachQueryParams(nameof(ScholarshipApplicationID), scholarshipApplicationID, nameof(SemesterID), semesterID);
		}
		private void RefreshPage(int scholarshipApplicationID)
		{
			Redirect(GetPageURL(scholarshipApplicationID, this.SemesterID));
		}

		private int? ScholarshipApplicationID => this.Request.GetParameterValue<int>(nameof(this.ScholarshipApplicationID));
		//private string Enrollment => this.Request.GetParameterValue(nameof(this.Enrollment));
		private short? SemesterID => this.GetParameterValue<short>(nameof(this.SemesterID));


		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.ScholarshipApplicationID == null)
				{
					Redirect<Dashboard>();
					return;
				}
				this.GetData();
			}

			var DSAManagement = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.Scholarship.CanManageQarzeHasanaDSARecommendations) == UserGroupPermission.PermissionValues.Allowed;
			if (!DSAManagement)
				this.btnAddApplicationApproval.Enabled = false;

			Lib.WebControls.LinkButton linkButton = (Lib.WebControls.LinkButton)this.repeaterApplicationApprovals.Items[0].FindControl("modalBtnDelete");
			linkButton.Visible = false;
		}

		private void GetData()
		{
			var scholarshipApplicantSummary = Approval.GetScholarshipApplicantSummary(this.ScholarshipApplicationID, Web.Common.AspireIdentity.Current.LoginSessionGuid);
			if (scholarshipApplicantSummary == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect<Dashboard>();
				return;
			}
			this.lblEnrollment.Text = scholarshipApplicantSummary.Enrollment;
			this.lblName.Text = scholarshipApplicantSummary.Name;
			this.lblClass.Text = scholarshipApplicantSummary.Class;
			this.lblLastResult.Text = scholarshipApplicantSummary.LastResult;
			this.lblFinalSemester.Text = scholarshipApplicantSummary.FinalSemester.ToSemesterString();
			this.lblScholarshipIntake.Text = scholarshipApplicantSummary.AnnouncementSemesterID.ToSemesterString();
			this.lblModalName.Text = scholarshipApplicantSummary.Name;
			this.lblModalEnrollment.Text = scholarshipApplicantSummary.Enrollment;

			var applicationApprovals = scholarshipApplicantSummary.ApplicationApprovals;
			if (applicationApprovals == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect<Dashboard>();
				return;
			}

			this.repeaterApplicationApprovals.DataBind(applicationApprovals);
		}

		protected void btnAddApplicationApproval_OnClick(object sender, EventArgs e)
		{
			this.DisplayApplicationApprovalModal(null);
		}

		private int? ScholarshipApplicationApprovalID
		{
			get => (int?)this.ViewState[nameof(this.ScholarshipApplicationApprovalID)];
			set => this.ViewState[nameof(this.ScholarshipApplicationApprovalID)] = value;
		}

		private void DisplayApplicationApprovalModal(int? applicationApprovalID)
		{
			var calculatedTutionFee = BL.Core.Scholarship.Staff.QarzeHasana.Approval.GetTutionFee(this.ScholarshipApplicationID.Value, this.AspireIdentity.LoginSessionGuid);
			this.hlCaluclatedFee.NavigateUrl = Aspire.Web.Sys.Staff.FeeManagement.ViewFeeChallans.GetPageUrl(this.lblEnrollment.Text, null, null, true);
			this.hlCaluclatedFee.Text = $"Calculated Fee Rs.{calculatedTutionFee.ToNAIfNullOrEmpty()}";
			this.ddlApprovalStatus.FillScholarshipApprovalStatuses();
			
			var getLastScholarshipApplicationApproval = BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.GetLastScholarshipApplicationApproval(this.ScholarshipApplicationID.Value);
			
			if (applicationApprovalID == null)
			{
				var Semesters = BL.Core.Common.Lists.GetScholarshipAnnouncedSemesters().Where(s => s > this.SemesterID);
				this.ddlSemesterID.FillSemesters(Semesters, CommonListItems.Select);

				var Data = BL.Core.Scholarship.Common.Schemas.ScholarshipApplicationApproval.FromJson(getLastScholarshipApplicationApproval.Data);

				this.tbApproval.Text = Data.FinalRecommendation.ToString();
				this.tbCampusRecommendation.Text = "";
				this.tbCampusRecommendation.ReadOnly = true;
				this.tbCSCRecommendation.Text = "";
				this.tbCSCRecommendation.ReadOnly = true;
				this.ddlSemesterID.Enabled = true;
				this.tbTutionFee.Text = string.Empty;
				this.tbCampusRemarks.Text = "";
				this.tbCSCRemarks.Text = "";
				this.ddlApprovalStatus.SetEnumValue(ScholarshipApplicationApproval.ApprovalStatuses.Pending);
				this.ScholarshipApplicationApprovalID = null;
				this.modalApplicationApproval.HeaderText = "Add Scholarship Application Approval";
			}
			else
			{
				var Semesters = BL.Core.Common.Lists.GetScholarshipAnnouncedSemesters();
				this.ddlSemesterID.FillSemesters(Semesters, CommonListItems.Select);

				var getApplicationApprovalData = this.GetApplicationApprovalData(applicationApprovalID.Value);
				this.ddlSemesterID.SelectedValue = getApplicationApprovalData.SemesterID.ToString();
				this.ddlSemesterID.Enabled = false;
				var data = BL.Core.Scholarship.Common.Schemas.ScholarshipApplicationApproval.FromJson(getApplicationApprovalData.Data);

				var firstScholarshipApplicaiton = BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.TextBoxEnableDisable(this.ScholarshipApplicationID.Value, getApplicationApprovalData.SemesterID, this.AspireIdentity.LoginSessionGuid);
				switch (firstScholarshipApplicaiton)
				{
					case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.TextBoxEnabledStatus.EnabledTextBoxForFirstScholarshipApplication:
						this.tbCampusRecommendation.ReadOnly = false;
						this.tbCSCRecommendation.ReadOnly = false;
						break;
					case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.TextBoxEnabledStatus.DisableTextBox:
						this.tbCampusRecommendation.ReadOnly = true;
						this.tbCSCRecommendation.ReadOnly = true;
						break;
				}

				this.tbTutionFee.Text = data.TuitionFee.ToString();
				this.tbCampusRecommendation.Text = data.CampusRecommendation.ToString();
				this.tbCampusRemarks.Text = data.CampusRemarks;
				this.tbCSCRecommendation.Text = data.CSCRecommendation.ToString();
				this.tbCSCRemarks.Text = data.CSCRemarks;
				this.tbApproval.Text = data.FinalRecommendation.ToString();
				this.ddlApprovalStatus.SetEnumValue(getApplicationApprovalData.ApprovalStatusEnum);
				this.ScholarshipApplicationApprovalID = applicationApprovalID;
				this.modalApplicationApproval.HeaderText = "Edit Scholarship Application Approval";
			}

			this.tbTutionFee.ReadOnly = true;
			this.tbCampusRemarks.ReadOnly = true;
			this.tbCampusRecommendation.ReadOnly = true;
			this.tbCSCRemarks.ReadOnly = true;
			this.tbCSCRecommendation.ReadOnly = true;
			this.tbApproval.ReadOnly = true;
			this.ddlApprovalStatus.Enabled = false;

			var feeManagement = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.Scholarship.CanManageQarzeHasnaStudentsFeeDetails) == UserGroupPermission.PermissionValues.Allowed;
			if (feeManagement)
				this.tbTutionFee.ReadOnly = false;

			var CampusManagement = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.Scholarship.CanManageQarzeHasanaCampusRecommendations) == UserGroupPermission.PermissionValues.Allowed;
			if (CampusManagement)
			{
				this.tbCampusRemarks.ReadOnly = false;
				this.tbCampusRecommendation.ReadOnly = false;
			}

			var CSCManagement = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.Scholarship.CanManageQarzeHasanaDSARecommendations) == UserGroupPermission.PermissionValues.Allowed;
			if (CSCManagement)
			{
				this.tbCSCRemarks.ReadOnly = false;
				this.tbCSCRecommendation.ReadOnly = false;
				this.tbApproval.ReadOnly = false;
				this.ddlApprovalStatus.Enabled = true;
			}
			this.modalApplicationApproval.Show();
		}

		private BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.ApplicationApprovalInfoResult GetApplicationApprovalData(int scholarshipApplicationApprovalID)
		{
			var applicationInfo = BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.GetApplicationApprovalInfo(scholarshipApplicationApprovalID, this.AspireIdentity.LoginSessionGuid);
			if (applicationInfo == null)
			{
				this.AddNoRecordFoundAlert();
				return null;
			}
			return applicationInfo;
		}

		protected void repeaterApplicationApprovals_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "StudentChequeStatus":
					var scholarshipApplication = Approval.ToggleChequeStatus(e.DecryptedCommandArgumentToInt(), this.AspireIdentity.LoginSessionGuid);
					switch (scholarshipApplication)
					{
						case Approval.ChequeStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							return;
						case Approval.ChequeStatuses.Success:
							this.GetData();
							return;
						default:
							throw new NotImplementedEnumException(scholarshipApplication);
					}

				case "Edit":
					this.DisplayApplicationApprovalModal(e.DecryptedCommandArgumentToInt());
					break;

				case "Delete":
					var resultUpdate = BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.DeleteApplicationApproval(e.DecryptedCommandArgumentToInt(), this.AspireIdentity.LoginSessionGuid);

					switch (resultUpdate)
					{
						case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.DeleteApplicationApprovalStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							Redirect<Approvals>();
							return;
						case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.DeleteApplicationApprovalStatuses.FieldsAreNotEmpty:
							this.AddErrorAlert("Some fields are not empty in specified scholarship application.");
							break;
						case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.DeleteApplicationApprovalStatuses.NotAllowedToDeleteFirstSemesterApplication:
							this.AddErrorAlert("The first scholarship application can not deleted.");
							break;
						case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.DeleteApplicationApprovalStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Scholarship");
							this.RefreshPage(this.ScholarshipApplicationID.Value);
							return;
						default:
							throw new NotImplementedEnumException(resultUpdate);
					}
					break;
			}
		}

		protected void btnSaveApplicationApproval_Click(object sender, EventArgs e)
		{
			{
				if (!this.IsValid)
					return;

				int? scholarshipApplicationApprovalID = this.ScholarshipApplicationApprovalID;
				var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
				string tutionFee = this.tbTutionFee.Text;
				string campusRecommendation = this.tbCampusRecommendation.Text;
				string campusRemarks = this.tbCampusRemarks.Text;
				string CSCRecommendation = this.tbCSCRecommendation.Text;
				string CSCRemarks = this.tbCSCRemarks.Text;
				string finalRecommendation = this.tbApproval.Text;
				var approvalStatusEnum = this.ddlApprovalStatus.SelectedValue.ToEnum<ScholarshipApplicationApproval.ApprovalStatuses>();

				if (scholarshipApplicationApprovalID == null)
				{
					var resultAdd = BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.AddApplicationApproval
					(this.ScholarshipApplicationID.Value, tutionFee.ToNullableInt(),
						campusRecommendation.ToNullableInt(), campusRemarks, CSCRecommendation.ToNullableInt(), CSCRemarks,
						finalRecommendation.ToNullableInt(), approvalStatusEnum, semesterID, this.AspireIdentity.LoginSessionGuid);
					switch (resultAdd)
					{
						case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.AddApplicationApprovalStatuses.AlreadyExists:
							this.alertModalApplicationApproval.AddErrorAlert("Approval is already registered in this semester.");
							this.updateApplicationApproval.Update();
							return;
						case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.AddApplicationApprovalStatuses.MaxSemestersExceeded:
							this.alertModalApplicationApproval.AddErrorAlert("No more Scholarships allowed for specified student.");
							this.updateApplicationApproval.Update();
							return;
						case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.AddApplicationApprovalStatuses.Pending:
							this.alertModalApplicationApproval.AddErrorAlert("Not allowed to add new scholarship. Last Scholarship Application is Pending.");
							this.updateApplicationApproval.Update();
							return;
						case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.AddApplicationApprovalStatuses.FirstScholarshipApplicationIsPendingOrRejected:
							this.alertModalApplicationApproval.AddErrorAlert("Not allowed to add new scholarship. First Scholarship Application is Pending/Rejected.");
							this.updateApplicationApproval.Update();
							return;
						case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.AddApplicationApprovalStatuses.PrevousSemesterSelected:
							this.alertModalApplicationApproval.AddErrorAlert("Please select new scholarship.");
							this.updateApplicationApproval.Update();
							return;
						case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.AddApplicationApprovalStatuses.Success:
							this.AddSuccessMessageHasBeenAdded("Scholarship approval");
							this.RefreshPage(this.ScholarshipApplicationID.Value);
							return;
						case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.AddApplicationApprovalStatuses.TutionFeeAndApprovalEmpty:
							this.alertModalApplicationApproval.AddErrorAlert("Tution Fee and Approval should not empty for Approved Application.");
							this.updateApplicationApproval.Update();
							return;
						default:
							throw new NotImplementedEnumException(resultAdd);
					}
				}
				else
				{
					var resultUpdate = BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.UpdateApplicationApproval(scholarshipApplicationApprovalID.Value, this.ScholarshipApplicationID.Value, tutionFee.ToNullableInt(), campusRecommendation.ToNullableInt(), campusRemarks, CSCRecommendation.ToNullableInt(), CSCRemarks, finalRecommendation.ToNullableInt(), approvalStatusEnum, this.AspireIdentity.LoginSessionGuid);

					switch (resultUpdate)
					{
						case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.UpdateApplicationApprovalStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							Redirect<Approvals>();
							return;
						case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.UpdateApplicationApprovalStatuses.TutionFeeAndApprovalEmpty:
							this.alertModalApplicationApproval.AddErrorAlert("Tution Fee and Approval % should not empty for Approved Application.");
							this.updateApplicationApproval.Update();
							return;
						case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.UpdateApplicationApprovalStatuses.NotAllowedChangesInApprovedOrRejectedApplication:
							this.alertModalApplicationApproval.AddErrorAlert("Changes are not allowed in Approved/Rejected Application.");
							this.updateApplicationApproval.Update();
							return;
						case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplicationApproval.UpdateApplicationApprovalStatuses.Success:
							this.AddSuccessMessageHasBeenUpdated("Scholarship approval");
							this.RefreshPage(this.ScholarshipApplicationID.Value);
							return;
						default:
							throw new NotImplementedEnumException(resultUpdate);
					}
				}
			}
		}
	}
}