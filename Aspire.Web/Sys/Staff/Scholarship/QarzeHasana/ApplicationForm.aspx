﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ApplicationForm.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Scholarship.QarzeHasana.ApplicationForm" %>

<%@ Register Src="~/Sys/Common/Scholarships/QarzeHasana/QarzeHasanaForm.ascx" TagPrefix="uc1" TagName="QarzeHasanaForm" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<aspire:AspireButton runat="server" ID="btnDeleteApplication" ButtonType="Danger" Glyphicon="remove" Text="Delete" CausesValidation="False" ConfirmMessage="Are you sure you want to delete this application?" OnClick="btnDeleteApplication_OnClick" />
	<uc1:QarzeHasanaForm runat="server" ID="qarzeHasanaForm" />
</asp:Content>
