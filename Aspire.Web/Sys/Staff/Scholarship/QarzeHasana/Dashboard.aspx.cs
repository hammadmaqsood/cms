﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Aspire.Web.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;

namespace Aspire.Web.Sys.Staff.Scholarship.QarzeHasana
{
	[AspirePage("2F7153A7-D2FF-4A8D-9BA7-558A71BD2770", UserTypes.Staff, "~/Sys/Staff/Scholarship/QarzeHasana/Dashboard.aspx", "Qarz-e-Hasana Dashboard", true, AspireModules.Scholarship)]
	public partial class Dashboard : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_graduation_cap);
		public override string PageTitle => Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana.ToFullName();
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Scholarship.Module, UserGroupPermission.Allowed},
		};

		public static string GetPageURL(int scholarshipAnnouncementID, string semesterID)
		{
			return typeof(Dashboard).GetAspirePageAttribute().PageUrl.AttachQueryParams(nameof(ScholarshipAnnouncementID), scholarshipAnnouncementID, nameof(SemesterID), semesterID);
		}

		public static string GetPageURL(int scholarshipAnnouncementID, string enrollment, string semesterID, bool search)
		{
			return typeof(Dashboard).GetAspirePageAttribute().PageUrl.AttachQueryParams(nameof(Enrollment), enrollment, nameof(ScholarshipAnnouncementID), scholarshipAnnouncementID, nameof(SemesterID), semesterID, "Search", search);
		}

		private static void RefreshPage(int scholarshipAnnouncementID, string enrollment, string semesterID, bool search)
		{
			Redirect(GetPageURL(scholarshipAnnouncementID, enrollment, semesterID, search));
		}

		private int? ScholarshipAnnouncementID => this.GetParameterValue<int>(nameof(this.ScholarshipAnnouncementID));
		//private short? SemesterID => this.GetParameterValue<short>(nameof(this.SemesterID));
		private string Enrollment => this.Request.GetParameterValue(nameof(this.Enrollment));

		private int? ScholarshipApplicantID
		{
			get => (int?)this.ViewState[nameof(this.ScholarshipApplicantID)];
			set => this.ViewState[nameof(this.ScholarshipApplicantID)] = value;
		}

		private short SemesterID
		{
			get => (short)this.ViewState[nameof(this.SemesterID)];
			set => this.ViewState[nameof(this.SemesterID)] = value;
		}

		private BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.FeeAddedStatuses? FeeAddedStatus => this.GetParameterValue<BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.FeeAddedStatuses>(nameof(this.FeeAddedStatus));
		private BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.CampusRemarksAddedStatuses? CampusRemarksAdded => this.GetParameterValue<BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.CampusRemarksAddedStatuses>(nameof(this.CampusRemarksAdded));
		private BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.CSCRemarksAddedStatuses? CSCRemarksAdded => this.GetParameterValue<BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.CSCRemarksAddedStatuses>(nameof(this.CSCRemarksAdded));
		private ScholarshipApplicationApproval.ApprovalStatuses? ApprovalStatus => this.GetParameterValue<ScholarshipApplicationApproval.ApprovalStatuses>(nameof(this.ApprovalStatus));

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.ScholarshipAnnouncementID == null)
				{
					Redirect<ScholarshipDashboard>();
					return;
				}
				this.ViewState.SetSortDirection(this.GetParameterValue<SortDirection>(nameof(SortDirection)) ?? SortDirection.Ascending);
				this.ViewState.SetSortExpression(this.GetParameterValue("SortExpression") ?? nameof(Program.ProgramAlias));
				var pageSize = this.GetParameterValue<int>("PageSize");
				if (pageSize != null)
					this.gvApplicants.PageSize = pageSize.Value;

				var semesterID = this.Request.GetParameterValue("SemesterID");
				this.SemesterID = semesterID.ToShort();

				this.ddlFeeAdded.FillEnums<BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.FeeAddedStatuses>(CommonListItems.All).SetSelectedEnumValueIfNotPostback<BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.FeeAddedStatuses>(nameof(this.FeeAddedStatus));
				this.ddlCampusRemarksAdded.FillEnums<BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.CampusRemarksAddedStatuses>(CommonListItems.All).SetSelectedEnumValueIfNotPostback<BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.CampusRemarksAddedStatuses>(nameof(this.CampusRemarksAdded));
				this.ddlCSCRemarksAdded.FillEnums<BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.CSCRemarksAddedStatuses>(CommonListItems.All).SetSelectedEnumValueIfNotPostback<BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.CSCRemarksAddedStatuses>(nameof(this.CSCRemarksAdded));
				this.ddlApprovalStatus.FillEnums<ScholarshipApplicationApproval.ApprovalStatuses>(CommonListItems.All).SetSelectedEnumValueIfNotPostback<ScholarshipApplicationApproval.ApprovalStatuses>(nameof(this.ApprovalStatus));

				this.GetData(0);

				var canGenerateQarzeHasnaApplicationApprovals = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.Scholarship.CanGenerateQarzeHasnaApplicationsForSpecificSemesters) == UserGroupPermission.PermissionValues.Allowed;
				if (!canGenerateQarzeHasnaApplicationApprovals)
					this.btnAddThisSemesterApprovals.Enabled = false;

				var canRejectQarzeHasnaApplicationApprovals = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.Scholarship.CanManageQarzeHasanaCampusRecommendations) == UserGroupPermission.PermissionValues.Allowed;
				if (!canRejectQarzeHasnaApplicationApprovals)
					this.btnBulkRejection.Enabled = false;

				var canAddApplication = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.Scholarship.CanAddAndDeleteQarzeHasanaApplicationForm) == UserGroupPermission.PermissionValues.Allowed;
				if (!canAddApplication)
					this.btnBulkAddApplications.Visible = false;

				var canAddQarzeHasnaBulkFee = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.Scholarship.CanManageQarzeHasnaStudentsFeeDetails) == UserGroupPermission.PermissionValues.Allowed;
				if (!canAddQarzeHasnaBulkFee)
					this.btnBulkAddFee.Enabled = false;
			}
		}

		protected void btnSearch_OnClick(object sender, EventArgs args)
		{
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			var searchText = this.tbSearch.Text;
			this.panelBody.Visible = false;
			var feeAdded = ddlFeeAdded.GetSelectedEnumValue<BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.FeeAddedStatuses>(null);
			var campusRemarksAdded = ddlCampusRemarksAdded.GetSelectedEnumValue<BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.CampusRemarksAddedStatuses>(null);
			var cscRemarksAdded = ddlCSCRemarksAdded.GetSelectedEnumValue<BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.CSCRemarksAddedStatuses>(null);
			var approvalStatusEnum = this.ddlApprovalStatus.GetSelectedEnumValue<ScholarshipApplicationApproval.ApprovalStatuses>(null);

			var scholarshipAnnouncementSummary = BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.ScholarshipAnnouncementSummary(this.ScholarshipAnnouncementID, feeAdded, campusRemarksAdded, cscRemarksAdded, approvalStatusEnum, searchText, pageIndex, this.gvApplicants.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out var virtualItemCount, this.AspireIdentity.LoginSessionGuid);
			if (scholarshipAnnouncementSummary == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect<ScholarshipDashboard>();
				return;
			}

			this.lblSemester.Text = scholarshipAnnouncementSummary.Semester;
			this.lblDates.Text = scholarshipAnnouncementSummary.OpeningDate.ConcatenateDates(scholarshipAnnouncementSummary.ClosingDate, "d");
			this.lblApplied.Text = scholarshipAnnouncementSummary.AppliedApplications.ToString();
			this.lblInProgress.Text = scholarshipAnnouncementSummary.InProgressApplications.ToString();

			this.panelBody.Visible = true;

			var applicants = scholarshipAnnouncementSummary.Applicants;
			if (applicants == null)
			{
				this.AddNoRecordFoundAlert();
				RefreshPage(this.ScholarshipAnnouncementID ?? throw new InvalidOperationException(), searchText, this.SemesterID.ToString(), false);
			}

			this.gvApplicants.DataBind(applicants, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
			this.gvApplicants.Columns[6].Visible = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.Scholarship.CanAddAndDeleteQarzeHasanaApplicationForm) == UserGroupPermission.PermissionValues.Allowed;
		}

		protected void gvApplicants_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvApplicants_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvApplicants_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvApplicants.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void ddlFeeAdded_SelectedIndexChanged(object sender, EventArgs e)
		{
			GetData(0);
		}

		protected void ddlCampusRemarksAdded_SelectedIndexChanged(object sender, EventArgs e)
		{
			GetData(0);
		}

		protected void ddlCSCRemarksAdded_SelectedIndexChanged(object sender, EventArgs e)
		{
			GetData(0);
		}

		protected void ddlApprovalStatus_SelectedIndexChanged(object sender, EventArgs e)
		{
			GetData(0);
		}

		#region Bulk

		protected void btnAddThisSemesterApprovals_OnClick(object sender, EventArgs e)
		{
			this.lblSelectedSemester.Text = this.SemesterID.ToSemesterString();

			var semesters = BL.Core.Common.Lists.GetScholarshipAnnouncedSemesters().Where(s => s > this.SemesterID);
			this.ddlSemesterID.FillSemesters(semesters, CommonListItems.Select);

			this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			this.modalAddThisSemesterApprovals.Show();
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			if (semesterID == null)
			{
				this.gvBulkApproval.DataBind(null);
				this.btnGenerate.Enabled = false;
				this.updateModalAddThisSemesterApprovalsFooter.Update();
				return;
			}

			var approvals = BL.Core.Scholarship.Staff.QarzeHasana.Approval.GetDataForBulkApprovals(semesterID.Value, this.ScholarshipAnnouncementID, this.AspireIdentity.LoginSessionGuid);
			if (!approvals.Any())
			{
				this.gvBulkApproval.DataBind(null);
				this.btnGenerate.Enabled = false;
				this.updateModalAddThisSemesterApprovalsFooter.Update();
				return;
			}
			this.gvBulkApproval.DataBind(approvals);
			this.updateModalAddThisSemesterApprovalsFooter.Update();
			this.btnGenerate.Enabled = true;
		}

		protected void btnGenerate_OnClick(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var scholarshipApplicationIDs = new List<int>();
			foreach (DataKey dataKey in this.gvBulkApproval.DataKeys)
				scholarshipApplicationIDs.Add((int)dataKey.Value);
			if (!scholarshipApplicationIDs.Any())
			{
				this.alertModalAddThisSemesterApprovals.AddNoRecordFoundAlert();
				this.updateAddThisSemesterApprovals.Update();
				this.updateModalAddThisSemesterApprovalsFooter.Update();
				this.btnGenerate.Enabled = false;
				return;
			}

			var generateApprovals = BL.Core.Scholarship.Staff.QarzeHasana.Approval.GenerateApplications(scholarshipApplicationIDs, semesterID, this.AspireIdentity.LoginSessionGuid);
			switch (generateApprovals.Status)
			{
				case Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.ApplicationGenerationStatusesResults.ApplicationGenerationStatuses.NoRecordAdded:
					this.alertModalAddThisSemesterApprovals.AddErrorAlert("There is an error or no record found.");
					this.updateAddThisSemesterApprovals.Update();
					return;
				case Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.ApplicationGenerationStatusesResults.ApplicationGenerationStatuses.Success:
					this.AddSuccessAlert($"No of {generateApprovals.Count} Record(s) for {semesterID.ToSemesterString()} Approvals have been added.");
					RefreshPage(this.ScholarshipAnnouncementID.Value, null, this.SemesterID.ToString(), true);
					return;
				default:
					throw new NotImplementedEnumException(generateApprovals.Status);
			}
		}

		protected void btnBulkRejection_OnClick(object sender, EventArgs e)
		{
			var semesters = BL.Core.Common.Lists.GetScholarshipAnnouncedSemesters().Where(s => s > this.SemesterID);
			this.ddlRejectionSemesterID.FillSemesters(semesters, CommonListItems.Select);
			this.ddlRejectionSemesterID_OnSelectedIndexChanged(null, null);
			this.modalRejectSemesterApprovals.Show();
		}

		protected void ddlRejectionSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var rejectionSemesterID = this.ddlRejectionSemesterID.SelectedValue.ToNullableShort();
			if (rejectionSemesterID == null)
			{
				this.gvBulkRejection.DataBind(null);
				this.lblModalCampusRemarks.Visible = false;
				this.tbModalCampusRemarks.Visible = false;
				this.lblRejectionText.Visible = false;
				this.updateModalRejectsFooter.Update();
				this.btnProceed.Enabled = false;
				return;
			}

			var approvals = BL.Core.Scholarship.Staff.QarzeHasana.Approval.GetDataForBulkRejection(rejectionSemesterID.Value, this.ScholarshipAnnouncementID, this.AspireIdentity.LoginSessionGuid);
			if (!approvals.Any())
			{
				this.gvBulkRejection.DataBind(null);
				this.lblModalCampusRemarks.Visible = false;
				this.tbModalCampusRemarks.Visible = false;
				this.lblRejectionText.Visible = false;
				this.updateModalRejectsFooter.Update();
				this.btnProceed.Enabled = false;
				return;
			}
			this.gvBulkRejection.DataBind(approvals);

			this.lblModalCampusRemarks.Visible = true;
			this.tbModalCampusRemarks.Text = "Ineligible due to GPA/CGPA less than 2.75";
			this.tbModalCampusRemarks.Visible = true;
			this.lblRejectionText.Visible = true;
			this.lblRejectionText.Text = $"Are you sure to reject all cases who's GPA/CGPA less than 2.75 in {rejectionSemesterID.Value.ToSemesterString()}?";
			this.updateModalRejectsFooter.Update();
			this.btnProceed.Enabled = true;
		}

		protected void btnRejectionProceed_OnClick(object sender, EventArgs e)
		{
			var semesterID = this.ddlRejectionSemesterID.SelectedValue.ToShort();
			var scholarshipApplicationIDs = new List<int>();
			foreach (DataKey dataKey in this.gvBulkRejection.DataKeys)
				scholarshipApplicationIDs.Add((int)dataKey.Value);
			if (!scholarshipApplicationIDs.Any())
			{
				this.alertModalRejectSemesterApprovals.AddNoRecordFoundAlert();
				this.updateModalRejectSemesterApprovals.Update();
				return;
			}

			string remarks = this.tbModalCampusRemarks.Text;

			var rejectedApprovals = BL.Core.Scholarship.Staff.QarzeHasana.Approval.RejectApplications(scholarshipApplicationIDs, semesterID, remarks, this.AspireIdentity.LoginSessionGuid);
			switch (rejectedApprovals.Status)
			{
				case Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.ApplicationRejectionStatusesResults.ApplicationRejectionStatuses.NoRecordAdded:
					this.alertModalRejectSemesterApprovals.AddErrorAlert("There is an error or no record found.");
					this.updateModalRejectSemesterApprovals.Update();
					return;
				case Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.ApplicationRejectionStatusesResults.ApplicationRejectionStatuses.Success:
					this.AddSuccessAlert($"No of {rejectedApprovals.Count} case(s) who's GPA/CGPA less than {BL.Core.Scholarship.Staff.QarzeHasana.Approval.MinimumAllowedGPACGAP.FormatGPA()} in {semesterID.ToSemesterString()} Approvals have been Rejected.");
					RefreshPage(this.ScholarshipAnnouncementID.Value, null, this.SemesterID.ToString(), true);
					return;
				default:
					throw new NotImplementedEnumException(rejectedApprovals.Status);
			}
		}

		protected void btnBulkAddApplications_OnClick(object sender, EventArgs e)
		{
			this.modalAddApplications.Show();
		}

		protected void btnAddApplicationsGenerate_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;

			int announcementID = this.ScholarshipAnnouncementID ?? throw new InvalidOperationException();
			var semesterID = this.SemesterID;
			var textData = this.tbAddApplicationsText.Text;
			var result = BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.AddApplicationsData(textData, announcementID, semesterID, this.AspireIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.AddApplicationStatuses.AlreadyApplied:
					this.alertModalRejectSemesterApprovals.AddErrorAlert("Already Applied");
					this.updateModalRejectSemesterApprovals.Update();
					return;
				case BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.AddApplicationStatuses.MaxSemesterExceeded:
					this.alertModalRejectSemesterApprovals.AddErrorAlert("Maximum Semester");
					this.updateModalRejectSemesterApprovals.Update();
					return;
				case BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.AddApplicationStatuses.Error:
					this.alertModalRejectSemesterApprovals.AddErrorAlert("Error");
					this.updateModalRejectSemesterApprovals.Update();
					return;
				case BL.Core.Scholarship.Staff.QarzeHasana.Dashboard.AddApplicationStatuses.Success:
					this.AddSuccessMessageHasBeenAdded("Applications");
					RefreshPage(this.ScholarshipAnnouncementID ?? throw new InvalidOperationException(), null, this.SemesterID.ToString(), true);
					return;
				default:
					throw new ArgumentOutOfRangeException(nameof(result), result, null);
			}
		}

		protected void btnUpdateFee_Click(object sender, EventArgs e)
		{
			var semesters = BL.Core.Common.Lists.GetScholarshipAnnouncedSemesters();
			this.ddlFeeSemesterID.FillSemesters(semesters, CommonListItems.Select);
			this.ddlFeeSemesterID_SelectedIndexChanged(null, null);
			this.trNoRecordFound.Visible = false;
			this.modalUpdateFee.Show();
		}

		protected void ddlFeeSemesterID_SelectedIndexChanged(object sender, EventArgs e)
		{
			var feeSemesterID = this.ddlFeeSemesterID.SelectedValue.ToNullableShort();
			if (feeSemesterID == null)
			{
				this.rpUpdateFee.DataBind(null);
				this.trNoRecordFound.Visible = true;
				return;
			}

			var approvals = BL.Core.Scholarship.Staff.QarzeHasana.Approval.GetDataToUpdateFee(feeSemesterID.Value, this.ScholarshipAnnouncementID, this.AspireIdentity.LoginSessionGuid);
			if (!approvals.Any())
			{
				this.rpUpdateFee.DataBind(null);
				this.trNoRecordFound.Visible = true;
				return;
			}
			this.rpUpdateFee.DataBind(approvals);
			this.trNoRecordFound.Visible = false;
		}

		protected void btnUpdateFeeProceed_Click(object sender, EventArgs e)
		{
			var semesterID = this.ddlFeeSemesterID.SelectedValue.ToShort();
			var scholarshipApplicationIDs = new List<int>();

			foreach (RepeaterItem item in this.rpUpdateFee.Items)
				if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
					if (((AspireCheckBox)item.FindControl("cbSelect")).Checked)
						scholarshipApplicationIDs.Add(((System.Web.UI.WebControls.HiddenField)item.FindControl("hfScholarshipApplicationID")).Value.ToInt());

			if (!scholarshipApplicationIDs.Any())
			{
				this.alertUpdateFee.AddNoRecordFoundAlert();
				return;
			}

			var feeAddedApprovals = BL.Core.Scholarship.Staff.QarzeHasana.Approval.UpdateFee(scholarshipApplicationIDs, semesterID, this.AspireIdentity.LoginSessionGuid);
			switch (feeAddedApprovals.Status)
			{
				case Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.UpdateFeeStatusesResults.UpdatedFeeStatuses.NoRecordAdded:
					this.alertModalRejectSemesterApprovals.AddErrorAlert("There is an error or no record found.");
					this.updateModalRejectSemesterApprovals.Update();
					return;
				case Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.UpdateFeeStatusesResults.UpdatedFeeStatuses.Success:
					this.AddSuccessAlert($"No of {feeAddedApprovals.Count} student(s) fee have been updated in {semesterID.ToSemesterString()} approvals.");
					RefreshPage(this.ScholarshipAnnouncementID.Value, null, this.SemesterID.ToString(), true);
					return;
				default:
					throw new NotImplementedEnumException(feeAddedApprovals.Status);
			}
		}

		protected void rpUpdateFee_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var chekbox = (AspireCheckBox)e.Item.FindControl("cbSelect");
				var calculatedFee = ((System.Web.UI.WebControls.HiddenField)e.Item.FindControl("hfCalculatedFee")).Value.ToNullableInt();

				if (calculatedFee == null)
					chekbox.Enabled = false;

				var addedFee = ((System.Web.UI.WebControls.HiddenField)e.Item.FindControl("hfAddedFee")).Value.ToNullableInt();
				if (calculatedFee != null && addedFee == null)
					chekbox.Checked = true;
			}
		}

		#endregion
	}
}