﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Scholarship.QarzeHasana
{
	[AspirePage("1A35AFD7-B0ED-4D0B-9EB2-0E4D222D6621", UserTypes.Staff, "~/Sys/Staff/Scholarship/QarzeHasana/ApplicationForm.aspx", "Qarz-e-Hasana Form", true, AspireModules.Scholarship)]
	public partial class ApplicationForm : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.education);
		public override string PageTitle => Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana.ToFullName();
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Scholarship.Module, UserGroupPermission.Allowed},
		};

		public static string GetPageUrl(int scholarshipAnnouncementID, int studentID)
		{
			return GetPageUrl<ApplicationForm>().AttachQueryParams(nameof(ScholarshipAnnouncementID), scholarshipAnnouncementID, nameof(StudentID), studentID);
		}

		public static void Redirect(int scholarshipAnnouncementID, int studentID)
		{
			Redirect(GetPageUrl(scholarshipAnnouncementID, studentID));
		}

		private int? ScholarshipAnnouncementID => this.Request.GetParameterValue<int>(nameof(this.ScholarshipAnnouncementID));
		private int? StudentID => this.Request.GetParameterValue<int>(nameof(this.StudentID));

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.ScholarshipAnnouncementID == null || this.StudentID == null)
				{
					Redirect<Dashboard>();
					return;
				}

				var applicationInfo = BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.GetApplicationInfo(this.ScholarshipAnnouncementID.Value, this.StudentID.Value, this.StaffIdentity.LoginSessionGuid);
				if (applicationInfo == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Dashboard>();
					return;
				}

				this.qarzeHasanaForm.LoadData(applicationInfo, this.ScholarshipAnnouncementID.Value, this.StudentID.Value);
				var canDeleteApplication = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.Scholarship.CanAddAndDeleteQarzeHasanaApplicationForm) == UserGroupPermission.PermissionValues.Allowed;
				if (!canDeleteApplication)
					this.btnDeleteApplication.Visible = false;
			}
		}

		protected void btnDeleteApplication_OnClick(object sender, EventArgs e)
		{
			var result = BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.DeleteApplication(this.ScholarshipAnnouncementID.Value, this.StudentID.Value, this.StaffIdentity.LoginSessionGuid);

			switch (result)
			{
				case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.DeleteStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					return;
				case BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.DeleteStatuses.Success:
					this.AddSuccessMessageHasBeenDeleted("Application");
					Redirect<Dashboard>();
					return;
				default:
					throw new NotImplementedEnumException(result);
			}
		}
	}
}