﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="SemesterWorkload.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Scholarship.SemesterWorkload" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadPH">
</asp:Content>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" CausesValidation="False" AutoPostBack="True" ValidationGroup="Filters" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Program:" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" CausesValidation="False" AutoPostBack="True" ValidationGroup="Filters" OnSelectedIndexChanged="ddlProgramIDFilter_OnSelectedIndexChanged" />
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" DataKeyNames="ProgramID" AutoGenerateColumns="False" Condensed="True" ID="gvSemesterWorkload" OnRowCommand="gvSemesterWorkload_OnRowCommand">
		<Columns>
			<asp:BoundField HeaderText="Program" DataField="ProgramAlias" />
			<asp:TemplateField HeaderText="Sem#0">
				<ItemTemplate>
					<aspire:AspireLinkButton CommandName="Courses" EncryptedCommandArgument='<%# $"{Eval("ProgramID")}:{(short)SemesterNos.Zero}" %>' ToolTip="Offered Credit Hours in Section A" runat="server" Text='<%# (((decimal?)Eval("SemesterZero.OfferedCreditHours")) ?? 0M).ToString("00.0") %>' />
					<aspire:AspireTextBox CssClass="input-sm" runat="server" ID="tbCreditHours0" Text='<%# Eval("SemesterZero.CreditHours") %>' ValidationGroup="Save" MaxLength="4" />
					<aspire:AspireDoubleValidator runat="server" ValidationGroup="Save" ControlToValidate="tbCreditHours0" AllowNull="True" InvalidNumberErrorMessage="Invalid credit hours." InvalidRangeErrorMessage="Value {min} to {max} is allowed." MinValue="0" MaxValue="25" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Sem#1">
				<ItemTemplate>
					<aspire:AspireLinkButton CommandName="Courses" EncryptedCommandArgument='<%# $"{Eval("ProgramID")}:{(short)SemesterNos.One}" %>' ToolTip="Offered Credit Hours in Section A" runat="server" Text='<%# (((decimal?)Eval("SemesterOne.OfferedCreditHours")) ?? 0M).ToString("00.0") %>' />
					<aspire:AspireTextBox CssClass="input-sm" runat="server" ID="tbCreditHours1" Text='<%# Eval("SemesterOne.CreditHours") %>' ValidationGroup="Save" MaxLength="4" />
					<aspire:AspireDoubleValidator runat="server" ValidationGroup="Save" ControlToValidate="tbCreditHours1" AllowNull="True" InvalidNumberErrorMessage="Invalid credit hours." InvalidRangeErrorMessage="Value {min} to {max} is allowed." MinValue="0" MaxValue="25" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Sem#2">
				<ItemTemplate>
					<aspire:AspireLinkButton CommandName="Courses" EncryptedCommandArgument='<%# $"{Eval("ProgramID")}:{(short)SemesterNos.Two}" %>' ToolTip="Offered Credit Hours in Section A" runat="server" Text='<%# (((decimal?)Eval("SemesterTwo.OfferedCreditHours")) ?? 0M).ToString("00.0") %>' />
					<aspire:AspireTextBox CssClass="input-sm" runat="server" ID="tbCreditHours2" Text='<%# Eval("SemesterTwo.CreditHours") %>' ValidationGroup="Save" MaxLength="4" />
					<aspire:AspireDoubleValidator runat="server" ValidationGroup="Save" ControlToValidate="tbCreditHours2" AllowNull="True" InvalidNumberErrorMessage="Invalid credit hours." InvalidRangeErrorMessage="Value {min} to {max} is allowed." MinValue="0" MaxValue="25" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Sem#3">
				<ItemTemplate>
					<aspire:AspireLinkButton CommandName="Courses" EncryptedCommandArgument='<%# $"{Eval("ProgramID")}:{(short)SemesterNos.Three}" %>' ToolTip="Offered Credit Hours in Section A" runat="server" Text='<%# (((decimal?)Eval("SemesterThree.OfferedCreditHours")) ?? 0M).ToString("00.0") %>' />
					<aspire:AspireTextBox CssClass="input-sm" runat="server" ID="tbCreditHours3" Text='<%# Eval("SemesterThree.CreditHours") %>' ValidationGroup="Save" MaxLength="4" />
					<aspire:AspireDoubleValidator runat="server" ValidationGroup="Save" ControlToValidate="tbCreditHours3" AllowNull="True" InvalidNumberErrorMessage="Invalid credit hours." InvalidRangeErrorMessage="Value {min} to {max} is allowed." MinValue="0" MaxValue="25" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Sem#4">
				<ItemTemplate>
					<aspire:AspireLinkButton CommandName="Courses" EncryptedCommandArgument='<%# $"{Eval("ProgramID")}:{(short)SemesterNos.Four}" %>' ToolTip="Offered Credit Hours in Section A" runat="server" Text='<%# (((decimal?)Eval("SemesterFour.OfferedCreditHours")) ?? 0M).ToString("00.0") %>' />
					<aspire:AspireTextBox CssClass="input-sm" runat="server" ID="tbCreditHours4" Text='<%# Eval("SemesterFour.CreditHours") %>' ValidationGroup="Save" MaxLength="4" />
					<aspire:AspireDoubleValidator runat="server" ValidationGroup="Save" ControlToValidate="tbCreditHours4" AllowNull="True" InvalidNumberErrorMessage="Invalid credit hours." InvalidRangeErrorMessage="Value {min} to {max} is allowed." MinValue="0" MaxValue="25" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Sem#5">
				<ItemTemplate>
					<aspire:AspireLinkButton CommandName="Courses" EncryptedCommandArgument='<%# $"{Eval("ProgramID")}:{(short)SemesterNos.Five}" %>' ToolTip="Offered Credit Hours in Section A" runat="server" Text='<%# (((decimal?)Eval("SemesterFive.OfferedCreditHours")) ?? 0M).ToString("00.0") %>' />
					<aspire:AspireTextBox CssClass="input-sm" runat="server" ID="tbCreditHours5" Text='<%# Eval("SemesterFive.CreditHours") %>' ValidationGroup="Save" MaxLength="4" />
					<aspire:AspireDoubleValidator runat="server" ValidationGroup="Save" ControlToValidate="tbCreditHours5" AllowNull="True" InvalidNumberErrorMessage="Invalid credit hours." InvalidRangeErrorMessage="Value {min} to {max} is allowed." MinValue="0" MaxValue="25" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Sem#6">
				<ItemTemplate>
					<aspire:AspireLinkButton CommandName="Courses" EncryptedCommandArgument='<%# $"{Eval("ProgramID")}:{(short)SemesterNos.Six}" %>' ToolTip="Offered Credit Hours in Section A" runat="server" Text='<%# (((decimal?)Eval("SemesterSix.OfferedCreditHours")) ?? 0M).ToString("00.0") %>' />
					<aspire:AspireTextBox CssClass="input-sm" runat="server" ID="tbCreditHours6" Text='<%# Eval("SemesterSix.CreditHours") %>' ValidationGroup="Save" MaxLength="4" />
					<aspire:AspireDoubleValidator runat="server" ValidationGroup="Save" ControlToValidate="tbCreditHours6" AllowNull="True" InvalidNumberErrorMessage="Invalid credit hours." InvalidRangeErrorMessage="Value {min} to {max} is allowed." MinValue="0" MaxValue="25" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Sem#7">
				<ItemTemplate>
					<aspire:AspireLinkButton CommandName="Courses" EncryptedCommandArgument='<%# $"{Eval("ProgramID")}:{(short)SemesterNos.Seven}" %>' ToolTip="Offered Credit Hours in Section A" runat="server" Text='<%# (((decimal?)Eval("SemesterSeven.OfferedCreditHours")) ?? 0M).ToString("00.0") %>' />
					<aspire:AspireTextBox CssClass="input-sm" runat="server" ID="tbCreditHours7" Text='<%# Eval("SemesterSeven.CreditHours") %>' ValidationGroup="Save" MaxLength="4" />
					<aspire:AspireDoubleValidator runat="server" ValidationGroup="Save" ControlToValidate="tbCreditHours7" AllowNull="True" InvalidNumberErrorMessage="Invalid credit hours." InvalidRangeErrorMessage="Value {min} to {max} is allowed." MinValue="0" MaxValue="25" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Sem#8">
				<ItemTemplate>
					<aspire:AspireLinkButton CommandName="Courses" EncryptedCommandArgument='<%# $"{Eval("ProgramID")}:{(short)SemesterNos.Eight}" %>' ToolTip="Offered Credit Hours in Section A" runat="server" Text='<%# (((decimal?)Eval("SemesterEight.OfferedCreditHours")) ?? 0M).ToString("00.0") %>' />
					<aspire:AspireTextBox CssClass="input-sm" runat="server" ID="tbCreditHours8" Text='<%# Eval("SemesterEight.CreditHours") %>' ValidationGroup="Save" MaxLength="4" />
					<aspire:AspireDoubleValidator runat="server" ValidationGroup="Save" ControlToValidate="tbCreditHours8" AllowNull="True" InvalidNumberErrorMessage="Invalid credit hours." InvalidRangeErrorMessage="Value {min} to {max} is allowed." MinValue="0" MaxValue="25" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Sem#9">
				<ItemTemplate>
					<aspire:AspireLinkButton CommandName="Courses" EncryptedCommandArgument='<%# $"{Eval("ProgramID")}:{(short)SemesterNos.Nine}" %>' ToolTip="Offered Credit Hours in Section A" runat="server" Text='<%# (((decimal?)Eval("SemesterNine.OfferedCreditHours")) ?? 0M).ToString("00.0") %>' />
					<aspire:AspireTextBox CssClass="input-sm" runat="server" ID="tbCreditHours9" Text='<%# Eval("SemesterNine.CreditHours") %>' ValidationGroup="Save" MaxLength="4" />
					<aspire:AspireDoubleValidator runat="server" ValidationGroup="Save" ControlToValidate="tbCreditHours9" AllowNull="True" InvalidNumberErrorMessage="Invalid credit hours." InvalidRangeErrorMessage="Value {min} to {max} is allowed." MinValue="0" MaxValue="25" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Sem#10">
				<ItemTemplate>
					<aspire:AspireLinkButton CommandName="Courses" EncryptedCommandArgument='<%# $"{Eval("ProgramID")}:{(short)SemesterNos.Ten}" %>' ToolTip="Offered Credit Hours in Section A" runat="server" Text='<%# (((decimal?)Eval("SemesterTen.OfferedCreditHours")) ?? 0M).ToString("00.0") %>' />
					<aspire:AspireTextBox CssClass="input-sm" runat="server" ID="tbCreditHours10" Text='<%# Eval("SemesterTen.CreditHours") %>' ValidationGroup="Save" MaxLength="4" />
					<aspire:AspireDoubleValidator runat="server" ValidationGroup="Save" ControlToValidate="tbCreditHours10" AllowNull="True" InvalidNumberErrorMessage="Invalid credit hours." InvalidRangeErrorMessage="Value {min} to {max} is allowed." MinValue="0" MaxValue="25" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<div class="text-center">
		<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save" ID="btnSave" ValidationGroup="Save" OnClick="btnSave_OnClick" />
		<aspire:AspireHyperLink runat="server" NavigateUrl="SemesterWorkload.aspx" Text="Cancel" />
	</div>
	<aspire:AspireModal runat="server" ID="modalOfferedCourses" HeaderText="Offered Courses" ModalSize="Large">
		<BodyTemplate>
			<div class="row">
				<div class="col-md-12">
					<div class="form-inline">
						<div class="form-group pull-right">
							<label for="inputSearchOfferedCourses">Search:</label>
							<div class="input-group">
								<aspire:AspireTextBox runat="server" ID="inputSearchOfferedCourses" ClientIDMode="Static" />
								<div class="input-group-btn">
									<button type="button" class="btn btn-default" title="Clear" id="btnClearOfferedCourses"><span class="glyphicon glyphicon-remove"></span></button>
									<button type="button" class="btn btn-default" title="Search" id="btnSearchOfferedCourses"><span class="glyphicon glyphicon-search"></span></button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<p></p>
			<asp:Repeater runat="server" ID="repeaterOfferedCourses">
				<HeaderTemplate>
					<div class="table-responsive" style="max-height: 350px; overflow: auto">
						<table class="table table-bordered table-responsive table-striped table-condensed table-hover" id="<%= this.repeaterOfferedCourses.ClientID %>">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Code</th>
									<th>Title</th>
									<th class="text-center">Majors</th>
									<th class="text-center">Credits</th>
									<th>Class</th>
								</tr>
							</thead>
							<tbody>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td class="text-center"><%#: Container.ItemIndex + 1 %></td>
						<td class="text-center"><%#: Eval("CourseCode") %></td>
						<td class="text-left"><%#: Eval("Title") %></td>
						<td class="text-center"><%#: Eval("Majors") %> </td>
						<td class="text-center" id="creditHours"><%#: Eval("CreditHours") %></td>
						<td class="text-left"><%# Eval("ClassNamesHtmlEncoded") %></td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
					</tbody>
					<tfoot>
						<tr>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th id="creditHours"></th>
							<th></th>
						</tr>
					</tfoot>
					</table>
						</div>
				</FooterTemplate>
			</asp:Repeater>
		</BodyTemplate>
		<FooterTemplate>
			<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
		</FooterTemplate>
	</aspire:AspireModal>
	<script type="text/javascript">
		$(function () {
			var gv = $("#<%=this.gvSemesterWorkload.ClientID%>");
			$("input.input-sm", gv).dblclick(function () {
				var tb = $(this);
				var credit = tb.prev().text();
				tb.val(credit);
			});

			var tableOfferedCourses = $("#<%=this.repeaterOfferedCourses.ClientID %>");
			var inputSearch = $("#inputSearchOfferedCourses");
			applyFilterEvents(tableOfferedCourses, inputSearch, $("#btnClearOfferedCourses"), $("#btnSearchOfferedCourses"), false, function () {
				var creditHours = 0;
				var sNo = 1;
				$("tbody tr", tableOfferedCourses).each(function (i, tr) {
					if ($(tr).css("display") != "none") {
						creditHours += $("td#creditHours", tr).text().toDouble();
						$("td:first", tr).text(sNo.toString());
						sNo++;
					}
				});
				$("tfoot th#creditHours", tableOfferedCourses).text(creditHours);
			});
		});
	</script>
</asp:Content>
