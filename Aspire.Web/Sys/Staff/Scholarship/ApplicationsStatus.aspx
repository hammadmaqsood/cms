﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ApplicationsStatus.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Scholarship.ApplicationsStatus" %>

<%@ Import Namespace="System.Activities.Statements" %>
<%@ Import Namespace="System.Runtime.Remoting.Messaging" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<aspire:AspireHyperLinkButton runat="server" NavigateUrl="ScholarshipDashboard.aspx" Text="Scholarships" ButtonType="Default" Glyphicon="list" />
	<p></p>
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
			<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="Search" OnSearch="tbEnrollment_OnSearch" />
		</div>
	</div>
	<p></p>
	<asp:Panel runat="server" ID="panelBody">
		<aspire:AspireGridView runat="server" ItemType="Aspire.BL.Core.Scholarship.Staff.QarzeHasana.ViewApplications.ScholarshipAnnouncement" ID="gvApplicationsList" Condensed="True" AutoGenerateColumns="False" OnRowCommand="gvApplicationsList_OnRowCommand">
			<Columns>
				<asp:TemplateField HeaderText="#">
					<ItemTemplate>
						<%#: Container.DataItemIndex + 1 %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="Scholarships" DataField="Scholarships" />
				<asp:BoundField HeaderText="Applied Status" DataField="ScholarshipApplicationStatusFullName" />
				<asp:BoundField HeaderText="Scholarship Intake" DataField="ScolarshipIntakeSemester" />
				<asp:BoundField HeaderText="Last Scholarship Status" DataField="LastStatus" />
				<asp:TemplateField HeaderText="Documents">
					<ItemTemplate>
						<aspire:AspireLinkButton runat="server" ConfirmMessage="Are you sure you want to continue?" CommandName="DocumentsStatus" EncryptedCommandArgument="<%# Item.ScholarshipApplicationID %>" Text="<%# Item.DocumentsStatusEnum.ToFullName() %>" Enabled="<%# Item.ActionTypeForDoc %>" />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Actions">
					<ItemTemplate>
						<aspire:AspireHyperLink runat="server" NavigateUrl="<%# Aspire.Web.Sys.Staff.Scholarship.ApplicationsStatus.GetPageURLForScholarshipApplicationForm(Item.ScholarshipTypeEnum, Item.ScholarshipAnnouncementID.Value, Item.StudentID) %>" Text="Application" Target="_blank" />
						<aspire:AspireHyperLink runat="server" Visible="<%# Item.StatusButtonVisible %>" NavigateUrl="<%# Item.StatusButtonVisible ? Aspire.Web.Sys.Staff.Scholarship.QarzeHasana.Approvals.GetPageURL(Item.ScholarshipApplicationID.Value, Item.SemesterID) : null %>" Text="Approval Status" Target="_blank" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
	</asp:Panel>
</asp:Content>
