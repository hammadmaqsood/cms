﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="ApplicationsList.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Scholarship.Reports.ApplicationsList" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">

	<div class="form-inline">
		<asp:UpdatePanel runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="btnFilterReport" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Applications Type:" AssociatedControlID="ddlApplications" />
					<aspire:AspireDropDownList runat="server" ID="ddlApplications" ValidationGroup="btnFilterReport" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlApprovalStatus" />
					<aspire:AspireDropDownList runat="server" ID="ddlApprovalStatus" ValidationGroup="btnFilterReport" />
				</div>
				<div class="form-group">
					<aspire:AspireButton runat="server" ID="btnFilterReport" Text="Filter Report" CssClass="btn-block" ButtonType="Primary" CausesValidation="false" OnClick="btnFilterReport_OnClick" />
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
</asp:Content>
