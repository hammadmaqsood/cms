﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Scholarship.Reports
{
	[AspirePage("A15FF6E3-5069-4E90-898F-C968A095B075", UserTypes.Staff, "~/Sys/Staff/Scholarship/Reports/ApplicationsList.aspx", "Qarz-e-Hasana List", false, AspireModules.Scholarship)]
	public partial class ApplicationsList : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Scholarship.Module, new []{UserGroupPermission.PermissionValues.Allowed }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => $"{Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana.ToFullName()} List";
		public string ReportName => this.PageTitle;
		public bool ExportToExcel => true;
		public bool ExportToWord => false;
		private short? SemesterID => this.GetParameterValue<short>(nameof(this.SemesterID));
		private int? ScholarshipApplicantID => this.GetParameterValue<int>(nameof(this.ScholarshipApplicantID));
		private ScholarshipApplicationApproval.ApprovalStatuses? ApprovalStatus => this.GetParameterValue<ScholarshipApplicationApproval.ApprovalStatuses>(nameof(this.ApprovalStatus));
		private BL.Core.Scholarship.Staff.QarzeHasana.Report.ApplicationList.ApplicationTypes? ApplicationTypes => this.GetParameterValue<BL.Core.Scholarship.Staff.QarzeHasana.Report.ApplicationList.ApplicationTypes>(nameof(this.ApplicationTypes));

		public static string GetPageURL(short semesterID, ScholarshipApplicationApproval.ApprovalStatuses? approvalStatusEnum)
		{
			return typeof(ApplicationsList).GetAspirePageAttribute().PageUrl.AttachQueryParams(nameof(SemesterID), semesterID, nameof(ApprovalStatus), approvalStatusEnum);
		}

		private int? ScholarshipAnnouncementID => this.GetParameterValue<int>(nameof(this.ScholarshipAnnouncementID));


		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var semesters = BL.Core.Common.Lists.GetScholarshipAnnouncedSemesters();
				this.ddlSemesterID.FillSemesters(semesters).SetSelectedValueIfNotPostback(semesters.ToString());
				this.ddlApprovalStatus.FillEnums<ScholarshipApplicationApproval.ApprovalStatuses>(CommonListItems.All).SetSelectedEnumValueIfNotPostback<ScholarshipApplicationApproval.ApprovalStatuses>(nameof(this.ApprovalStatus));
				this.ddlApplications.FillEnums<BL.Core.Scholarship.Staff.QarzeHasana.Report.ApplicationList.ApplicationTypes>(CommonListItems.All).SetSelectedEnumValueIfNotPostback<BL.Core.Scholarship.Staff.QarzeHasana.Report.ApplicationList.ApplicationTypes>(nameof(this.ApplicationTypes));
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			if (semesterID == null)
			{
				reportViewer.Reset();
				return;
			}

			var approvalStatusEnum = this.ddlApprovalStatus.GetSelectedEnumValue<ScholarshipApplicationApproval.ApprovalStatuses>(null);
			var applicationTypes = this.ddlApplications.GetSelectedEnumValue<BL.Core.Scholarship.Staff.QarzeHasana.Report.ApplicationList.ApplicationTypes>(null);

			var reportDataSet = BL.Core.Scholarship.Staff.QarzeHasana.Report.ApplicationList.GetQarzeHasanaLists(null, semesterID.Value, approvalStatusEnum, applicationTypes, BL.Core.Scholarship.Staff.QarzeHasana.Report.ApplicationList.ListTypes.QarzeHasnaList, this.StaffIdentity.LoginSessionGuid);
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/Scholarship/Staff/ApplicationsList.rdlc".MapPath();
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.Applications), reportDataSet.Applications));
			reportViewer.LocalReport.EnableExternalImages = true;
		}

		protected void btnFilterReport_OnClick(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView?.ReportViewer);
		}
	}
}