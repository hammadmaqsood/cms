﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Scholarship.Reports
{
	[AspirePage("1B55B6DF-2789-456B-AE86-F0E3BE547FB0", UserTypes.Staff, "~/Sys/Staff/Scholarship/Reports/ApplicationForm.aspx", "Qarz-e-Hasana Application Form", true, AspireModules.Scholarship)]
	public partial class ApplicationForm : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Scholarship.Module, new []{UserGroupPermission.PermissionValues.Allowed, }}
		};

		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => $"{Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana.ToFullName()} Application Form";
		public string ReportName => this.PageTitle;
		public bool ExportToExcel => false;
		public bool ExportToWord => false;
		private int? ScholarshipApplicationID => this.Request.GetParameterValue<int>(nameof(this.ScholarshipApplicationID));

		public static string GetPageUrl(int scholarshipApplicationID)
		{
			return GetPageUrl<ApplicationForm>().AttachQueryParams(nameof(ScholarshipApplicationID), scholarshipApplicationID, ReportView.ReportFormat, ReportFormats.PDF, ReportView.Inline, true);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (this.ScholarshipApplicationID == null)
				Redirect<Dashboard>();
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			Sys.Common.Methods.RenderQarzeHasanaFormReport<Dashboard>(this, reportViewer, this.ScholarshipApplicationID ?? throw new InvalidOperationException());
		}
	}
}