﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="SemesterWiseApplicationsList.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Scholarship.Reports.SemesterWiseApplicationsList" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">

	<div class="form-inline">
		<asp:UpdatePanel runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<div>
					<aspire:AspireLabel runat="server" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Applied Semester:" AssociatedControlID="ddlAnnouncementSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlAnnouncementSemesterID" ValidationGroup="btnFilterReport" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Approvals Semester:" AssociatedControlID="ddlApprovalsSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlApprovalsSemesterID" ValidationGroup="btnFilterReport" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlApprovalStatus" />
					<aspire:AspireDropDownList runat="server" ID="ddlApprovalStatus" ValidationGroup="btnFilterReport" />
				</div>
				<div class="form-group">
					<aspire:AspireButton runat="server" ID="btnFilterReport" Text="Filter Report" CssClass="btn-block" ButtonType="Primary" OnClick="btnFilterReport_OnClick" />
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
</asp:Content>
