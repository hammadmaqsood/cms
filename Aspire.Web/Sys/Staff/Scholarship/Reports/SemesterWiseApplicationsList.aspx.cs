﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Staff.Scholarship.Reports
{
	[AspirePage("A0750961-43CA-4046-A6AE-E9871176F5B1", UserTypes.Staff, "~/Sys/Staff/Scholarship/Reports/SemesterWiseApplicationsList.aspx", "Qarz-e-Hasana Semester Wise List", false, AspireModules.Scholarship)]

	public partial class SemesterWiseApplicationsList : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Scholarship.Module, new []{UserGroupPermission.PermissionValues.Allowed }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => $"{Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana.ToFullName()} Semester Wise List";
		public string ReportName => this.PageTitle;
		public bool ExportToExcel => true;
		public bool ExportToWord => false;
		private short? AnnouncementSemesterID => this.GetParameterValue<short>(nameof(this.AnnouncementSemesterID));
		private short? ApprovalSemesterID => this.GetParameterValue<short>(nameof(this.ApprovalSemesterID));
		private int? ScholarshipApplicantID => this.GetParameterValue<int>(nameof(this.ScholarshipApplicantID));
		private ScholarshipApplicationApproval.ApprovalStatuses? ApprovalStatus => this.GetParameterValue<ScholarshipApplicationApproval.ApprovalStatuses>(nameof(this.ApprovalStatus));

		public static string GetPageURL(short announcementSemesterID, short approvalSemesterID, ScholarshipApplicationApproval.ApprovalStatuses? approvalStatusEnum)
		{
			return typeof(SemesterWiseApplicationsList).GetAspirePageAttribute().PageUrl.AttachQueryParams(nameof(AnnouncementSemesterID), announcementSemesterID, nameof(ApprovalSemesterID), approvalSemesterID, nameof(ApprovalStatus), approvalStatusEnum);
		}

		private int? ScholarshipAnnouncementID => this.GetParameterValue<int>(nameof(this.ScholarshipAnnouncementID));

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlAnnouncementSemesterID.FillSemesters().SetSelectedValueIfNotPostback(nameof(this.AnnouncementSemesterID));
				this.ddlAnnouncementSemesterID.Enabled = false;
				this.ViewState[nameof(this.AnnouncementSemesterID)] = this.AnnouncementSemesterID;
				var approvalSemesters = BL.Core.Common.Lists.GetScholarshipApprovalSemestersList().Where(s => s >= this.AnnouncementSemesterID);
				this.ddlApprovalsSemesterID.FillSemesters(approvalSemesters);
				this.ddlApprovalStatus.FillEnums<ScholarshipApplicationApproval.ApprovalStatuses>(CommonListItems.All).SetSelectedEnumValueIfNotPostback<ScholarshipApplicationApproval.ApprovalStatuses>(nameof(this.ApprovalStatus));
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			var announcementSemesterID = (short?)this.ViewState[nameof(this.AnnouncementSemesterID)];
			if (announcementSemesterID == null)
			{
				reportViewer.Reset();
				return;
			}

			var approvalSemesterID = this.ddlApprovalsSemesterID.SelectedValue.ToNullableShort();

			var approvalStatusEnum = this.ddlApprovalStatus.GetSelectedEnumValue<ScholarshipApplicationApproval.ApprovalStatuses>(null);
			//var reportDataSet = BL.Core.Scholarship.Staff.QarzeHasana.Report.ApplicationList.GetQarzeHasanaSemesterWiseList(announcementSemesterID.Value, approvalSemesterID.Value, approvalStatusEnum, this.StaffIdentity.LoginSessionGuid);
			var reportDataSet = BL.Core.Scholarship.Staff.QarzeHasana.Report.ApplicationList.GetQarzeHasanaLists(announcementSemesterID.Value, approvalSemesterID.Value, approvalStatusEnum, null, BL.Core.Scholarship.Staff.QarzeHasana.Report.ApplicationList.ListTypes.SemesterWiseList, this.StaffIdentity.LoginSessionGuid);
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/Scholarship/Staff/ApplicationsList.rdlc".MapPath();
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.Applications), reportDataSet.Applications));
			reportViewer.LocalReport.EnableExternalImages = true;
		}

		protected void btnFilterReport_OnClick(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView?.ReportViewer);
		}
	}
}