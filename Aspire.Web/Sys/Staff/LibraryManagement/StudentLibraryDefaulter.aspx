﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="StudentLibraryDefaulter.aspx.cs" Inherits="Aspire.Web.Sys.Staff.LibraryManagement.StudentLibraryDefaulter" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-6">
			<aspire:AspireButton runat="server" ID="btnAdd" Text="Add" Glyphicon="plus" ButtonType="Success" OnClick="btnAdd_OnClick" />
		</div>
		<div class="col-md-6">
			<asp:Panel runat="server" CssClass="form-group" DefaultButton="btnSearch">
				<div class="input-group">
					<aspire:AspireTextBox runat="server" ID="tbSearch" ValidationGroup="Search" PlaceHolder="Search..." />
					<span class="input-group-btn">
						<aspire:AspireButton runat="server" Glyphicon="search" ValidationGroup="Search" ID="btnSearch" OnClick="btnSearch_OnClick" />
					</span>
				</div>
			</asp:Panel>
		</div>
	</div>
	<aspire:AspireGridView runat="server" ID="gvStudentLibraryDefaulter" AllowSorting="True" AutoGenerateColumns="False" OnRowCommand="gvStudentLibraryDefaulter_OnRowCommand" OnPageIndexChanging="gvStudentLibraryDefaulter_OnPageIndexChanging" OnSorting="gvStudentLibraryDefaulter_OnSorting" OnPageSizeChanging="gvStudentLibraryDefaulter_OnPageSizeChanging">
		<Columns>
			<asp:BoundField HeaderText="Enrollment" DataField="Enrollment" SortExpression="Enrollment" />
			<asp:BoundField HeaderText="Name" DataField="StudentName" SortExpression="StudentName" />
			<asp:BoundField HeaderText="Program" DataField="ProgramAlias" SortExpression="ProgramAlias" />
			<asp:BoundField HeaderText="Remarks" DataField="Remarks" SortExpression="Remarks" />
			<asp:BoundField HeaderText="Status" DataField="StatusEnumFullName" SortExpression="StatusEnumFullName" />
			<asp:TemplateField HeaderText="Updated On" SortExpression="LastUpdatedDate">
				<ItemTemplate>
					<%#: this.Eval("LastUpdatedDate") %><br />
					<small>
						<%#:this.Eval("UpdatedByUser") %></small>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" ToolTip="Edit" CommandName="Edit" CausesValidation="False" Glyphicon="edit" EncryptedCommandArgument='<%# this.Eval("StudentLibraryDefaulterID") %>' />
					<aspire:AspireLinkButton runat="server" ToolTip="Delete" CommandName="Delete" CausesValidation="False" Glyphicon="remove" EncryptedCommandArgument='<%# this.Eval("StudentLibraryDefaulterID") %>' ConfirmMessage='<%#this.Eval("Enrollment"," Are you sure you want to delete student library defaulter record {0}?") %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalStudentLibraryDefaulter" HeaderText="Add/Update Library Defaulters">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updateModalStudentLibraryDefaulter" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertModalStudentLibraryDefaulter" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Enrollments:" AssociatedControlID="tbEnrollment" />
						<aspire:AspireTextBox runat="server" ID="tbEnrollment" Rows="5" ValidationGroup="Required" TextMode="MultiLine" MaxLength="150000" />
						<span class="help-block">Enter enrollments one per line.</span>
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbEnrollment" ErrorMessage="This field is required." ValidationGroup="Required" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" />
						<aspire:AspireTextBox runat="server" Rows="3" MaxLength="1000" ID="tbRemarks" ValidationGroup="Required" TextMode="MultiLine" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatus" />
						<aspire:AspireDropDownList runat="server" ID="ddlStatus" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlStatus" ErrorMessage="This field is required." ValidationGroup="Required" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Update Existing:" AssociatedControlID="ddlUpdateExisting" />
						<aspire:AspireDropDownList runat="server" ID="ddlUpdateExisting" />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" ValidationGroup="Required" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
