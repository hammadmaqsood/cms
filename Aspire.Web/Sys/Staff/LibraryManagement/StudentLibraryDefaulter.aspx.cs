﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.LibraryManagement
{
	[AspirePage("D3312488-43C0-4931-BDDF-D5CBA5A99921", UserTypes.Staff, "~/Sys/Staff/LibraryManagement/StudentLibraryDefaulter.aspx", "Student Library Defaulter", true, AspireModules.LibraryManagement)]
	public partial class StudentLibraryDefaulter : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.book);
		public override string PageTitle => "Student Library Defaulter";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.LibraryManagement.Module, UserGroupPermission.Allowed},
		};

		public void Refresh()
		{
			Redirect<StudentLibraryDefaulter>();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("LastUpdatedDate", SortDirection.Descending);
				this.btnSearch_OnClick(null, null);
			}
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void btnAdd_OnClick(object sender, EventArgs e)
		{
			this.DisplayStudentLibraryDefaulterModel(null);
		}

		private void DisplayStudentLibraryDefaulterModel(int? studentLibraryDefaulterID)
		{
			this.ddlStatus.FillEnums<Aspire.Model.Entities.StudentLibraryDefaulter.Statuses>();
			this.ddlUpdateExisting.FillYesNo();
			if (studentLibraryDefaulterID == null)
			{
				this.tbEnrollment.Text = null;
				this.tbRemarks.Text = null;
				this.ddlStatus.SetEnumValue(Model.Entities.StudentLibraryDefaulter.Statuses.Blocked);
				this.ddlUpdateExisting.SetSelectedValueNo();
			}
			else
			{
				var studentLibraryDefaulter = BL.Core.LibraryManagement.Staff.StudentLibraryDefaulter.GetStudentLibraryDefaulter(studentLibraryDefaulterID.Value, this.StaffIdentity.LoginSessionGuid);
				if (studentLibraryDefaulter == null)
				{
					this.AddNoRecordFoundAlert();
					this.Refresh();
					return;
				}
				this.tbEnrollment.Text = studentLibraryDefaulter.Student.Enrollment;
				this.tbRemarks.Text = studentLibraryDefaulter.Remarks;
				this.ddlStatus.SetEnumValue(studentLibraryDefaulter.StatusEnum);
				this.ddlUpdateExisting.SetSelectedValueYes();
			}
			this.modalStudentLibraryDefaulter.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var enrollments = new HashSet<string>(this.tbEnrollment.Text.Split('\n').Select(s => s.TrimAndMakeItNullIfEmpty()).Where(s => s != null));
			if (!enrollments.Any())
				return;
			var remarks = this.tbRemarks.Text;
			var statusEnum = this.ddlStatus.GetSelectedEnumValue<Aspire.Model.Entities.StudentLibraryDefaulter.Statuses>();
			var updateExisting = this.ddlUpdateExisting.SelectedValue.ToBoolean();
			var result = BL.Core.LibraryManagement.Staff.StudentLibraryDefaulter.AddStudentLibraryDefaulter(enrollments, remarks, statusEnum, updateExisting, this.StaffIdentity.LoginSessionGuid);

			var statuses = result.Select(r => new
			{
				r.Key,
				r.Value
			}).GroupBy(r => r.Value).Select(r => new
			{
				r.Key,
				Count = r.Count()
			}).ToList();

			foreach (var status in statuses)
				switch (status.Key)
				{
					case BL.Core.LibraryManagement.Staff.StudentLibraryDefaulter.AddStudentLibraryDefaulterStatuses.NoRecordFound:
						this.AddErrorAlert($"{status.Count} enrollment(s) not found.");
						break;
					case BL.Core.LibraryManagement.Staff.StudentLibraryDefaulter.AddStudentLibraryDefaulterStatuses.Added:
						this.AddSuccessAlert($"{status.Count} records are added to defaulters list.");
						break;
					case BL.Core.LibraryManagement.Staff.StudentLibraryDefaulter.AddStudentLibraryDefaulterStatuses.ExistingUpdated:
						this.AddSuccessAlert($"{status.Count} records are updated.");
						break;
					case BL.Core.LibraryManagement.Staff.StudentLibraryDefaulter.AddStudentLibraryDefaulterStatuses.AlreadyExists:
						this.AddSuccessAlert($"{status.Count} records already exists and not updated.");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			AspireModal.Close(this.modalStudentLibraryDefaulter);
			this.Refresh();
		}

		private void GetData(int pageIndex)
		{
			var search = this.tbSearch.Text.ToNullIfWhiteSpace();
			var defaulters = BL.Core.LibraryManagement.Staff.StudentLibraryDefaulter.GetStudentLibraryDefaulters(search, pageIndex, this.gvStudentLibraryDefaulter.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out int virtualItemCount, this.StaffIdentity.LoginSessionGuid);
			this.gvStudentLibraryDefaulter.DataBind(defaulters, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		protected void gvStudentLibraryDefaulter_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayStudentLibraryDefaulterModel(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var studentLibraryDefaulterID = e.DecryptedCommandArgumentToInt();
					var result = BL.Core.LibraryManagement.Staff.StudentLibraryDefaulter.DeleteStudentLibraryDefaulter(studentLibraryDefaulterID, this.StaffIdentity.LoginSessionGuid);
					switch (result)
					{
						case BL.Core.LibraryManagement.Staff.StudentLibraryDefaulter.DeleteStudentLibraryDefaulterStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Student Library Defaulter");
							this.Refresh();
							break;
						case BL.Core.LibraryManagement.Staff.StudentLibraryDefaulter.DeleteStudentLibraryDefaulterStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.GetData(9);
					break;
			}
		}

		protected void gvStudentLibraryDefaulter_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvStudentLibraryDefaulter_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvStudentLibraryDefaulter_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvStudentLibraryDefaulter.PageSize = e.NewPageSize;
			this.GetData(0);
		}
	}
}