﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Employers.aspx.cs" Inherits="Aspire.Web.Sys.Staff.QA.Employers" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row filters">
		<div class="col-md-2">
			<aspire:AspireButton ID="btnAddEmployer" class="pull-left" ButtonType="Success" runat="server" Text="Add Employer" Glyphicon="plus" OnClick="btnAddEmployer_Click" />
		</div>
		<div class="col-md-10 text-right">
			<div class="form-inline">
				<div class="form-group">
					<asp:Panel runat="server" CssClass="input-group" DefaultButton="btnSearch">
						<aspire:AspireTextBox runat="server" ID="tbSearchText" PlaceHolder="Search" ValidationGroup="Filters" />
						<div class="input-group-btn">
							<aspire:AspireButton ButtonType="Default" Glyphicon="search" runat="server" ID="btnSearch" OnClick="btnSearch_Click" ValidationGroup="Filters" />
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvEmployers" OnPageSizeChanging="gvEmployers_PageSizeChanging" OnSorting="gvEmployers_Sorting" AutoGenerateColumns="False" OnRowCommand="gvEmployers_RowCommand" OnPageIndexChanging="gvEmployers_PageIndexChanging" AllowSorting="True">
		<Columns>
			<asp:BoundField HeaderText="Name" DataField="Name" SortExpression="Name" />
			<asp:BoundField HeaderText="Email" DataField="Email" SortExpression="Email" />
			<asp:BoundField HeaderText="Company Name" DataField="CompanyName" SortExpression="CompanyName" />
			<asp:BoundField HeaderText="Address" DataField="CompanyAddress" />
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CausesValidation="False" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument='<%#this.Eval("EmployerID") %>' />
					<aspire:AspireLinkButton runat="server" CausesValidation="False" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%#this.Eval("EmployerID") %>' ConfirmMessage="Are you sure you want to delete survey conduct schedule?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<aspire:AspireModal runat="server" ID="modalControlPanel">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updateModalControlPanel" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertControlPanel" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbName" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbName" ValidationGroup="Req" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbName" ValidationGroup="Req" ErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Email:" AssociatedControlID="tbEmail" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbEmail" TextTransform="LowerCase" ValidationGroup="Req" MaxLength="100"  PlaceHolder="someone@example.com"/>
								<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbEmail" ValidationGroup="Req" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Email." ValidationExpression="Email"/>
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Company Name:" AssociatedControlID="tbCompanyName" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbCompanyName" ValidationGroup="Req" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbCompanyName" ValidationGroup="Req" ErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Company Address:" AssociatedControlID="tbCompanyAddress" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbCompanyAddress" ValidationGroup="Req" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" ValidationGroup="Req" OnClick="btnSave_Click" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
