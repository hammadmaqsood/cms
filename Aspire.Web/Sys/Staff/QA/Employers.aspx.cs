﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.BL.Core.QualityAssurance.Staff.Surveys;

namespace Aspire.Web.Sys.Staff.QA
{
	[AspirePage("1B7B11D2-62A3-4F7A-A7FA-1FDB93856F9E", UserTypes.Staff, "~/Sys/Staff/QA/Employers.aspx", "Employers", true, AspireModules.UserManagement)]
	public partial class Employers : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.QualityAssurance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.cog);

		public override string PageTitle => "Control Panel: Employer";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Name", SortDirection.Ascending);
				GetData(0);
			}
		}

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void gvEmployers_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{

			this.gvEmployers.PageSize = e.NewPageSize;
			this.GetData(0);

		}

		protected void gvEmployers_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvEmployers_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayControlPanelModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var employerID = e.DecryptedCommandArgumentToInt();
					this.StaffIdentity.Demand(StaffPermissions.QualityAssurance.ManageConductSurveyForm7, UserGroupPermission.PermissionValues.Allowed);
					var deleted = BL.Core.QualityAssurance.Staff.Employers.DeleteEmployer(employerID, StaffIdentity.InstituteID, this.StaffIdentity.LoginSessionGuid);
					switch (deleted)
					{
						case BL.Core.QualityAssurance.Staff.Employers.ValidationResults.None:
							throw new InvalidOperationException();
						case BL.Core.QualityAssurance.Staff.Employers.ValidationResults.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.QualityAssurance.Staff.Employers.ValidationResults.Success:
							this.AddSuccessMessageHasBeenDeleted("Employer Record.");
							break;
						case BL.Core.QualityAssurance.Staff.Employers.ValidationResults.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Employer");
							break;
						case BL.Core.QualityAssurance.Staff.Employers.ValidationResults.AlreadyExists:
							throw new InvalidOperationException();
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.Refresh();
					break;
			}
		}

		protected void gvEmployers_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}
		private void GetData(int pageIndex)
		{
			int virtualItemCount;
			var searchText = this.tbSearchText.Text;
			var pageGuid = this.AspirePageAttribute.AspirePageGuid;
			var employers = BL.Core.QualityAssurance.Staff.Employers.GetEmployers(searchText, pageIndex, this.gvEmployers.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out virtualItemCount, this.StaffIdentity.LoginSessionGuid);
			this.gvEmployers.DataBind(employers, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		protected void btnSave_Click(object sender, EventArgs e)
		{

			if (!this.Page.IsValid)
				return;
			var employerID = (int?)this.ViewState[nameof(BL.Core.QualityAssurance.Staff.Employers.EmployerInfo.EmployerID)];
			var pageGuid = this.AspirePageAttribute.AspirePageGuid;
			var name = tbName.Text.ToString();
			var email = tbEmail.Text.ToString();
			var companyName = tbCompanyName.Text.ToString();
			var companyAddress = tbCompanyAddress.Text.ToString();
			if (employerID == null)
			{
				var employerResult = BL.Core.QualityAssurance.Staff.Employers.AddEmployer(name, email, companyName, companyAddress, StaffIdentity.InstituteID, this.StaffIdentity.LoginSessionGuid);
				switch (employerResult)
				{
					case BL.Core.QualityAssurance.Staff.Employers.ValidationResults.None:
						throw new InvalidOperationException();
					case BL.Core.QualityAssurance.Staff.Employers.ValidationResults.NoRecordFound:
						this.alertControlPanel.AddNoRecordFoundAlert();
						this.updateModalControlPanel.Update();
						break;
					case BL.Core.QualityAssurance.Staff.Employers.ValidationResults.AlreadyExists:
						this.alertControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Record already exists.");
						this.updateModalControlPanel.Update();
						break;

					case BL.Core.QualityAssurance.Staff.Employers.ValidationResults.Success:
						this.alertControlPanel.AddSuccessMessageHasBeenAdded("Survey conduct schedule");
						this.Refresh();
						break;
					case BL.Core.QualityAssurance.Staff.Employers.ValidationResults.ChildRecordExists:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var employerResult = BL.Core.QualityAssurance.Staff.Employers.UpdateEmployer(employerID.Value, name, email, companyName, companyAddress, StaffIdentity.InstituteID, this.StaffIdentity.LoginSessionGuid);
				switch (employerResult)
				{
					case BL.Core.QualityAssurance.Staff.Employers.ValidationResults.None:
						throw new InvalidOperationException();
					case BL.Core.QualityAssurance.Staff.Employers.ValidationResults.NoRecordFound:
						this.AddNoRecordFoundAlert();
						break;
					case BL.Core.QualityAssurance.Staff.Employers.ValidationResults.AlreadyExists:
						this.alertControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Record already exists.");
						this.updateModalControlPanel.Update();
						break;
					case BL.Core.QualityAssurance.Staff.Employers.ValidationResults.Success:
						this.AddSuccessMessageHasBeenUpdated("Survey conduct schedule");
						this.Refresh();
						break;
					case BL.Core.QualityAssurance.Staff.Employers.ValidationResults.ChildRecordExists:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		protected void btnAddEmployer_Click(object sender, EventArgs e)
		{
			this.DisplayControlPanelModal(null);
		}
		private void DisplayControlPanelModal(int? employerID)
		{
			var instituteID = this.StaffIdentity.InstituteID;
			if (employerID == null)
			{
				this.modalControlPanel.HeaderText = "Add";
				this.btnSave.Text = "Add";
				this.ViewState[nameof(BL.Core.QualityAssurance.Staff.Employers.EmployerInfo.EmployerID)] = null;
				this.tbName.Text = null;
				this.tbEmail.Text = null;
				this.tbCompanyName.Text = null;
				this.tbCompanyAddress.Text = null;
				this.modalControlPanel.Show();
			}
			else
			{
				var employer = BL.Core.QualityAssurance.Staff.Employers.GetEmployer(employerID.Value, this.StaffIdentity.LoginSessionGuid);
				if (employer == null)
				{
					this.AddNoRecordFoundAlert();
					this.GetData(0);
					return;
				}
				this.tbName.Text = employer.Name;
				this.tbEmail.Text = employer.Email;
				this.tbCompanyName.Text = employer.CompanyName;
				this.tbCompanyAddress.Text = employer.CompanyAddress;
				this.modalControlPanel.HeaderText = "Edit";
				this.btnSave.Text = "Save";
				this.ViewState[nameof(BL.Core.QualityAssurance.Staff.Employers.EmployerInfo.EmployerID)] = employerID;
				this.modalControlPanel.Show();
			}
		}
		public static string GetPageURL()
		{
			return typeof(Employers).GetAspirePageAttribute().PageUrl;
		}

		public static void Redirect()
		{
			Redirect(GetPageURL());
		}

		private void Refresh()
		{
			Redirect();
		}

	}
}