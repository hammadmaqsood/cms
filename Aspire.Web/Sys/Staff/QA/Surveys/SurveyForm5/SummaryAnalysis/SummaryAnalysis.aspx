﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="SummaryAnalysis.aspx.cs" Inherits="Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm5.SummaryAnalysis.SummaryAnalysis" %>

<%@ Register Src="~/Sys/Common/QA/Surveys/SurveyForm5/SummaryAnalysis.ascx" TagPrefix="uc1" TagName="QASurveySummaryControl" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc1:QASurveySummaryControl runat="server" ID="QASurveySummaryControl" />
</asp:Content>
