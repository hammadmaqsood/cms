﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="SurveyControlPanel.aspx.cs" Inherits="Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm5.SurveyControlPanel" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireButton runat="server" ID="btnAdd" ButtonType="Success" Glyphicon="plus" Text="Add" OnClick="btnAdd_OnClick" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
				</div>
			</div>
		</div>
	</div>
	<p></p>

	<aspire:AspireGridView runat="server" ID="gvControlPanel" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gvControlPanel_OnPageIndexChanging" OnPageSizeChanging="gvControlPanel_OnPageSizeChanging" OnSorting="gvControlPanel_OnSorting" OnRowCommand="gvControlPanel_OnRowCommand">
		<Columns>
			<asp:BoundField HeaderText="Semester" SortExpression="SemesterID" DataField="Semester" />
			<asp:BoundField HeaderText="From" SortExpression="OpenDate" DataField="OpenDate" DataFormatString="{0:MM/dd/yyyy}" />
			<asp:BoundField HeaderText="To" SortExpression="CloseDate" DataField="CloseDate" DataFormatString="{0:MM/dd/yyyy}" />
			<asp:TemplateField HeaderText="Status" SortExpression="Status">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="ToggleStatus" EncryptedCommandArgument='<%#Eval("QASurveyConductID") %>' Text='<%#Eval("StatusEnumFullName")%>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CausesValidation="False" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument='<%#this.Eval("QASurveyConductID") %>' />
					<aspire:AspireLinkButton runat="server" CausesValidation="False" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%#this.Eval("QASurveyConductID") %>' ConfirmMessage="Are you sure you want to delete survey conduct schedule?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalControlPanel">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updateModalControlPanel" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertControlPanel" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterForSave" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlSemesterForSave" ValidationGroup="Req" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterForSave" ValidationGroup="Req" ErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Start Date:" AssociatedControlID="dpOpenDate" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dpOpenDate" ValidationGroup="Req" />
								<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dpOpenDate" ValidationGroup="Req" AllowNull="False" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="End Date:" AssociatedControlID="dpCloseDate" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dpCloseDate" ValidationGroup="Req" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="rblStatus" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireRadioButtonList runat="server" ID="rblStatus" RepeatLayout="Flow" RepeatDirection="Horizontal" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ValidationGroup="Req" ButtonType="Primary" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
	<aspire:AspireModal runat="server" ID="modlLinks">
		<BodyTemplate>
		</BodyTemplate>
	</aspire:AspireModal>
</asp:Content>
