﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Generate.aspx.cs" Inherits="Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm8.Generate" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row filters">
		<div class="col-md-12 text-right">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_SelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatus" />
					<aspire:AspireDropDownList runat="server" ID="ddlStatus" AutoPostBack="True" ValidationGroup="Filters" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" />
				</div>
				<div class="form-group">
					<asp:Panel runat="server" CssClass="input-group" DefaultButton="btnSearch">
						<aspire:AspireTextBox runat="server" ID="tbSearchText" PlaceHolder="Search" ValidationGroup="Filters" />
						<div class="input-group-btn">
							<aspire:AspireButton ButtonType="Default" Glyphicon="search" runat="server" ID="btnSearch" OnClick="btnSearch_Click" ValidationGroup="Filters" />
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>
	<p></p>

	<aspire:AspireGridView runat="server" ID="gvEmployers" OnPageSizeChanging="gvEmployers_PageSizeChanging" AllowPaging="True" OnSorting="gvEmployers_Sorting" AutoGenerateColumns="False" OnPageIndexChanging="gvEmployers_PageIndexChanging" OnRowCommand="gvEmployers_RowCommand" AllowSorting="True">
		<Columns>
			<asp:TemplateField>
				<HeaderTemplate>
					<aspire:CheckBox ID="cbCheckAll" OnCheckedChanged="cbCheckAll_CheckedChanged" AutoPostBack="True" runat="server" />
				</HeaderTemplate>
				<ItemTemplate>
					<asp:HiddenField runat="server" ID="hfEmployerID" Value='<%# this.Eval("EmployerID") %>' Visible="False" />
					<aspire:CheckBox runat="server" ID="cbCheckIndividual" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Name" DataField="Name" SortExpression="Name" />
			<asp:BoundField HeaderText="Email" DataField="Email" SortExpression="Email" />
			<asp:BoundField HeaderText="Company Name" DataField="CompanyName" SortExpression="CompanyName" />
			<asp:BoundField HeaderText="Status" DataField="StatusFullName" SortExpression="Status" />
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireLinkButton ID="lbViewRecord" CommandName="lbViewRecord" runat="server" Text="View" EncryptedCommandArgument='<%#this.Eval("QASurveyUserID") %>'></aspire:AspireLinkButton>

				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<div class="text-center">
		<aspire:AspireButton runat="server" ID="btnAdd" ButtonType="Primary" Visible="False" Text="Send Link(s)" OnClick="btnAdd_Click" />
	</div>
</asp:Content>

