﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Aspire.BL.Core.QualityAssurance.Staff.Surveys.SurveyForm8;

namespace Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm8
{
	[AspirePage("1F92F9A2-6998-474F-A836-02C03D844410", UserTypes.Staff, "~/Sys/Staff/QA/Surveys/SurveyForm8/Generate.aspx", "Generate Survey (Survey Form-8)", true, AspireModules.QualityAssurance)]
	public partial class Generate : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.QualityAssurance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.cog);

		public override string PageTitle => "Generate Link: Employer Survey (Survey Form-8)";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Name", SortDirection.Ascending);
				var instituteID = this.StaffIdentity.InstituteID;
				this.ddlStatus.FillEnums<QASurveyUser.Statuses>(CommonListItems.All);
				this.ddlSemesterID.FillSemesters();
				this.ddlSemesterID_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterID_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetEmployerMembers(0);
		}

		protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetEmployerMembers(0);
		}

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			this.GetEmployerMembers(0);
		}

		protected void gvEmployers_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvEmployers.PageSize = e.NewPageSize;
			this.GetEmployerMembers(0);
		}

		protected void gvEmployers_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetEmployerMembers(0);
		}

		protected void gvEmployers_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetEmployerMembers(e.NewPageIndex);
		}

		protected void gvEmployers_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "lbViewRecord":
					e.Handled = true;
					var qaSurveyUserID = e.DecryptedCommandArgument();
					if (qaSurveyUserID == "")
						this.AddErrorAlert("Survey Link is not sent to Faculty Member");
					else
						SurveyForm8.Form.Redirect(e.DecryptedCommandArgumentToInt());
					break;
			}
		}

		protected void cbCheckAll_CheckedChanged(object sender, EventArgs e)
		{
			var cbCheckAll = (Lib.WebControls.CheckBox)this.gvEmployers.HeaderRow.FindControl("cbCheckAll");

			foreach (GridViewRow row in this.gvEmployers.Rows)
			{
				var cbCheckIndivisual = (Lib.WebControls.CheckBox)row.FindControl("cbCheckIndividual");
				cbCheckIndivisual.Checked = cbCheckAll.Checked;
			}
		}

		protected void btnAdd_Click(object sender, EventArgs e)
		{
			if (!this.Page.IsValid)
				return;
			int addedSurveysCount = 0;
			var semesterID = this.ddlSemesterID.SelectedValue.ToInt();
			var pageGuid = GetAspirePageAttribute<SurveyControlPanel>().AspirePageGuid;
			var qaSurveyConduct = GenerateLinks.GetSurveyConduct(semesterID, pageGuid, this.StaffIdentity.LoginSessionGuid);
			foreach (GridViewRow row in this.gvEmployers.Rows)
			{
				var cbCheckIndivisual = (Lib.WebControls.CheckBox)row.FindControl("cbCheckIndividual");
				var hfEmployerID = (System.Web.UI.WebControls.HiddenField)row.FindControl("hfEmployerID");
				if (cbCheckIndivisual.Checked)
				{
					var employerID = Convert.ToInt32(hfEmployerID.Value);
					var SurveyPageUrl = GetAspirePageAttribute<EmployerSurvey>().PageUrl.ToAbsoluteUrl();
					var queryResult = GenerateLinks.AddSurvey(qaSurveyConduct.QASurveyConductID, employerID, StaffIdentity.InstituteID, SurveyPageUrl, this.StaffIdentity.LoginSessionGuid);
					switch (queryResult)
					{
						case GenerateLinks.AddSurveyEnum.None:
							throw new InvalidOperationException();
						case GenerateLinks.AddSurveyEnum.AlreadyExists:
							this.AddErrorAlert("Link already sent.");
							break;
						case GenerateLinks.AddSurveyEnum.Success:
							addedSurveysCount++;
							break;
						case GenerateLinks.AddSurveyEnum.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
			if (addedSurveysCount > 0)
				this.AddSuccessAlert("Link has been sent.");
			this.GetEmployerMembers(0);
		}

		private void GetEmployerMembers(int pageIndex)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToInt();
			var pageGuid = GetAspirePageAttribute<SurveyControlPanel>().AspirePageGuid;
			var status = this.ddlStatus.SelectedValue.ToNullableEnum<QASurveyUser.Statuses>();
			var searchText = this.tbSearchText.Text;
			var qaSurveyConduct = GenerateLinks.GetSurveyConduct(semesterID, pageGuid, StaffIdentity.LoginSessionGuid);
			if (qaSurveyConduct != null)
			{
				int virtualItemCount;
				var employers = GenerateLinks.GetEmployerMembers(qaSurveyConduct.QASurveyConductID, status, searchText, pageIndex, this.gvEmployers.PageSize, out virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.StaffIdentity.LoginSessionGuid);
				var result = employers.Select(emp => new
				{
					EmployerID = emp.EmployerID,
					Name = emp.Name,
					Email = emp.Email.ToNAIfNullOrEmpty(),
					CompanyName = emp.CompanyName.ToNAIfNullOrEmpty(),
					StatusFullName = emp.StatusFullName.ToNAIfNullOrEmpty(),
				})
					.ToList();
				if (result.Count == 0)
					this.btnAdd.Visible = false;
				else
					this.btnAdd.Visible = true;
				this.gvEmployers.DataBind(employers, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
			}
			else
			{
				this.gvEmployers.DataBind(null);
				this.btnAdd.Visible = false;
				return;
			}
		}
		public static string GetPageUrl()
		{
			return GetAspirePageAttribute<Generate>().PageUrl;
		}

		public static void Redirect()
		{
			Redirect(GetPageUrl());
		}
	}
}