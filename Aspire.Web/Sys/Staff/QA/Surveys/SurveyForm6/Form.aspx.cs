﻿using System;
using System.Collections.Generic;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm6
{
	[AspirePage("A48742D0-59FE-48FB-B83E-A86553ED2CB9", UserTypes.Staff, "~/Sys/Staff/QA/Surveys/SurveyForm6/Form.aspx", "Survey Form-6", true, AspireModules.QualityAssurance)]
	public partial class Form : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.QualityAssurance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.cog);

		public int? SurveyID => this.Request.GetParameterValue("qaSurveyUserID").ToInt();

		public override string PageTitle => " Survey of Department Offering Ph.D. Program (Survey Form-6)";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (this.SurveyID != null)
				this.QAsurveyUserControl.LoadData(this.SurveyID.Value);
		}

		public static string GetPageUrl(int? qaSurveyUserID)
		{
			return GetAspirePageAttribute<Form>().PageUrl.AttachQueryParam("qaSurveyUserID", qaSurveyUserID);
		}

		public static void Redirect(int? qaSurveyUserID)
		{
			Redirect(GetPageUrl(qaSurveyUserID));
		}
	}
}