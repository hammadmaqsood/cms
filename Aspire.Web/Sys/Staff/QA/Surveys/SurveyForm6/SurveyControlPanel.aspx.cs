﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.BL.Core.QualityAssurance.Staff.Surveys.SurveyForm6;

namespace Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm6
{
	[AspirePage("262F6082-282E-42EF-8FE7-5C8DD7138284", UserTypes.Staff, "~/Sys/Staff/QA/Surveys/SurveyForm6/SurveyControlPanel.aspx", "Control Panel (Survey Form-6)", true, AspireModules.QualityAssurance)]

	public partial class SurveyControlPanel : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.QualityAssurance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.cog);

		public override string PageTitle => "Control Panel: Survey of departments offering Ph.D programs (Survey Form-6)";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("SemesterID", SortDirection.Descending);
				this.ddlSemesterID.FillSemesters(CommonListItems.All).SetSelectedValueIfNotPostback("SemesterID");
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void gvControlPanel_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvControlPanel_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvControlPanel.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvControlPanel_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvControlPanel_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "ToggleStatus":
					e.Handled = true;
					this.StaffIdentity.Demand(StaffPermissions.QualityAssurance.ManageConductSurveyForm6, UserGroupPermission.PermissionValues.Allowed);
					var qaSurveyConductID = e.DecryptedCommandArgumentToInt();
					var qAsurveyConduct = QASurveyConducts.ToggleSurveyConductStatus(qaSurveyConductID, this.StaffIdentity.LoginSessionGuid);
					if (qAsurveyConduct == null)
						this.AddNoRecordFoundAlert();
					this.GetData(this.gvControlPanel.PageIndex);
					return;
				case "Edit":
					e.Handled = true;
					this.DisplayControlPanelModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var qasurveyConductID = e.DecryptedCommandArgumentToInt();
					this.StaffIdentity.Demand(StaffPermissions.QualityAssurance.ManageConductSurveyForm6, UserGroupPermission.PermissionValues.Allowed);
					var deleted = QASurveyConducts.DeleteSurveyConduct(qasurveyConductID, StaffIdentity.InstituteID, this.StaffIdentity.LoginSessionGuid);
					switch (deleted)
					{
						case QASurveyConduct.ValidationResults.None:
							throw new InvalidOperationException();
						case QASurveyConduct.ValidationResults.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case QASurveyConduct.ValidationResults.DatePassed:
							this.AddErrorAlert("Records of previous dates can't be deleted.");
							break;
						case QASurveyConduct.ValidationResults.Success:
							this.AddSuccessMessageHasBeenDeleted("Survey conduct schedule");
							break;
						case QASurveyConduct.ValidationResults.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Survey");
							break;
						case QASurveyConduct.ValidationResults.AlreadyExists:
						case QASurveyConduct.ValidationResults.InvalidOpenDate:
						case QASurveyConduct.ValidationResults.InvalidDateRange:
						case QASurveyConduct.ValidationResults.InvalidCloseDate:
							throw new InvalidOperationException();
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.Refresh();
					break;
			}
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void btnAdd_OnClick(object sender, EventArgs e)
		{
			this.DisplayControlPanelModal(null);
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.Page.IsValid)
				return;
			var qasurveyConductID = (int?)this.ViewState[nameof(QASurveyConduct.QASurveyConductID)];
			var pageGuid = this.AspirePageAttribute.AspirePageGuid;
			var deptID = this.ddlDepartmentForSave.SelectedValue.ToInt();
			var programID = this.ddlProgramForSave.SelectedValue.ToInt();
			var openDate = this.dpOpenDate.SelectedDate.Value;
			var closeDate = this.dpCloseDate.SelectedDate;
			var status = this.rblStatus.SelectedValue.ToByte();
			if (qasurveyConductID == null)
			{
				var semesterID = this.ddlSemesterForSave.SelectedValue.ToShort();

				var surveyConductResult = QASurveyConducts.AddQASurveyConduct(semesterID, deptID, programID, pageGuid, openDate, closeDate, status, StaffIdentity.InstituteID, this.StaffIdentity.LoginSessionGuid);
				switch (surveyConductResult)
				{
					case QASurveyConduct.ValidationResults.None:
						throw new InvalidOperationException();
					case QASurveyConduct.ValidationResults.NoRecordFound:
						this.alertControlPanel.AddNoRecordFoundAlert();
						this.updateModalControlPanel.Update();
						break;
					case QASurveyConduct.ValidationResults.AlreadyExists:
						this.alertControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Record already exists.");
						this.updateModalControlPanel.Update();
						break;
					case QASurveyConduct.ValidationResults.InvalidOpenDate:
						this.alertControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Current or future dates allowed in \"Start Date\" field.");
						this.updateModalControlPanel.Update();
						break;
					case QASurveyConduct.ValidationResults.InvalidDateRange:
						this.alertControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Invalid Start date and End date.");
						this.updateModalControlPanel.Update();
						break;
					case QASurveyConduct.ValidationResults.Success:
						this.alertControlPanel.AddSuccessMessageHasBeenAdded("Survey conduct schedule");
						this.Refresh();
						break;
					case QASurveyConduct.ValidationResults.InvalidCloseDate:
						this.alertControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Invalid End date.");
						this.updateModalControlPanel.Update();
						break;
					case QASurveyConduct.ValidationResults.DatePassed:
					case QASurveyConduct.ValidationResults.ChildRecordExists:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var qasurveyConductResult = QASurveyConducts.UpdateSurveyConduct(qasurveyConductID.Value, deptID, programID, openDate, closeDate, status, StaffIdentity.InstituteID, this.StaffIdentity.LoginSessionGuid);
				switch (qasurveyConductResult)
				{
					case QASurveyConduct.ValidationResults.None:
						throw new InvalidOperationException();
					case QASurveyConduct.ValidationResults.NoRecordFound:
						this.AddNoRecordFoundAlert();
						break;
					case QASurveyConduct.ValidationResults.AlreadyExists:
						this.alertControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Record already exists.");
						this.updateModalControlPanel.Update();
						break;
					case QASurveyConduct.ValidationResults.InvalidOpenDate:
						this.alertControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Current or future dates allowed in \"Start Date\" field.");
						this.updateModalControlPanel.Update();
						break;
					case QASurveyConduct.ValidationResults.InvalidDateRange:
						this.alertControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Invalid Start and End dates.");
						this.updateModalControlPanel.Update();
						break;
					case QASurveyConduct.ValidationResults.Success:
						this.AddSuccessMessageHasBeenUpdated("Survey conduct schedule");
						this.Refresh();
						break;
					case QASurveyConduct.ValidationResults.InvalidCloseDate:
					case QASurveyConduct.ValidationResults.DatePassed:
					case QASurveyConduct.ValidationResults.ChildRecordExists:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
			}

		}

		private void DisplayControlPanelModal(int? qasurveyConductID)
		{
			this.ddlSemesterForSave.FillSemesters();
			var instituteID = this.StaffIdentity.InstituteID;
			this.ddlDepartmentForSave.FillDepartments(instituteID);

			this.rblStatus.FillEnums<QASurveyConduct.Statuses>();
			if (qasurveyConductID == null)
			{
				var deptId = ddlDepartmentForSave.SelectedValue.ToInt();
				this.ddlProgramForSave.FillPrograms(instituteID, deptId, DegreeLevels.Doctorate, true);
				this.ddlSemesterForSave.ClearSelection();
				this.ddlSemesterForSave.Enabled = true;
				this.dpOpenDate.SelectedDate = null;
				this.dpCloseDate.SelectedDate = null;
				this.rblStatus.SetEnumValue(QASurveyConduct.Statuses.Inactive);
				this.modalControlPanel.HeaderText = "Add";
				this.btnSave.Text = "Add";
				this.ViewState[nameof(QASurveyConduct.QASurveyConductID)] = null;
				this.modalControlPanel.Show();
			}
			else
			{
				var qasurveyConduct = QASurveyConducts.GetSurveyConduct(qasurveyConductID.Value, this.StaffIdentity.LoginSessionGuid);
				if (qasurveyConduct == null)
				{
					this.AddNoRecordFoundAlert();
					this.GetData(0);
					return;
				}
				this.ddlSemesterForSave.Items.FindByValue(qasurveyConduct.SemesterID.ToString()).Selected = true;
				this.ddlDepartmentForSave.Items.FindByValue(qasurveyConduct.DepartmentID.ToString()).Selected = true;
				var deptId = ddlDepartmentForSave.SelectedValue.ToInt();
				this.ddlProgramForSave.FillPrograms(instituteID, deptId, DegreeLevels.Doctorate, true);
				this.ddlProgramForSave.Items.FindByValue(qasurveyConduct.ProgramID.ToString()).Selected = true;
				this.ddlSemesterForSave.Enabled = false;
				this.dpOpenDate.SelectedDate = qasurveyConduct.StartDate;
				this.dpCloseDate.SelectedDate = qasurveyConduct.EndDate;
				this.rblStatus.SetEnumValue(qasurveyConduct.SurveyStatusEnum);
				this.modalControlPanel.HeaderText = "Edit";
				this.btnSave.Text = "Save";
				this.ViewState[nameof(QASurveyConduct.QASurveyConductID)] = qasurveyConductID;
				this.modalControlPanel.Show();
			}
		}

		private void GetData(int pageIndex)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var pageGuid = this.AspirePageAttribute.AspirePageGuid;
			int virtualItemCount;
			var qasurveyConduct = QASurveyConducts.GetSurveyConducts(semesterID, pageGuid, pageIndex, this.gvControlPanel.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out virtualItemCount, this.StaffIdentity.LoginSessionGuid);
			this.gvControlPanel.DataBind(qasurveyConduct, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		public static string GetPageURL(short? semesterID)
		{
			return typeof(SurveyControlPanel).GetAspirePageAttribute().PageUrl.AttachQueryParam("SemesterID", semesterID);
		}

		public static void Redirect(short? semesterID)
		{
			Redirect(GetPageURL(semesterID));
		}

		private void Refresh()
		{
			Redirect(this.ddlSemesterID.SelectedValue.ToNullableShort());
		}

		protected void ddlDepartmentForSave_SelectedIndexChanged(object sender, EventArgs e)
		{
			var deptID = ddlDepartmentForSave.SelectedValue.ToInt();
			this.ddlProgramForSave.FillPrograms(StaffIdentity.InstituteID, deptID, DegreeLevels.Doctorate, true);
		}
	}
}
