﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Aspire.BL.Core.QualityAssurance.Staff.Surveys.SurveyForm6;

namespace Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm6
{
	[AspirePage("6148F140-8388-4E20-B35C-3EA5657B6504", UserTypes.Staff, "~/Sys/Staff/QA/Surveys/SurveyForm6/Generate.aspx", "Generate Survey (Survey Form-6)", true, AspireModules.QualityAssurance)]

	public partial class Generate : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.QualityAssurance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.cog);

		public override string PageTitle => "Generate Link: Department Survey (Survey Form-6)";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Name", SortDirection.Ascending);
				var instituteID = this.StaffIdentity.InstituteID;
				this.ddlDepartmentID.FillDepartments(instituteID);
				this.ddlStatus.FillEnums<QASurveyUser.Statuses>(CommonListItems.All);
				var deptId = ddlDepartmentID.SelectedItem.Value.ToInt();
				this.ddlProgramID.FillPrograms(instituteID, deptId, DegreeLevels.Doctorate, true);
				this.ddlSemesterID.FillSemesters();
				this.ddlSemesterID_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterID_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetFacultyMembers(0);
		}

		protected void ddlDepartmentID_SelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.StaffIdentity.InstituteID;
			var deptId = ddlDepartmentID.SelectedItem.Value.ToInt();
			this.ddlProgramID.FillPrograms(instituteID, deptId, DegreeLevels.Doctorate, true);
			this.GetFacultyMembers(0);
		}

		protected void ddlProgramID_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetFacultyMembers(0);
		}

		protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetFacultyMembers(0);
		}

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			this.GetFacultyMembers(0);
		}

		protected void gvFacultyMembers_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvFacultyMembers.PageSize = e.NewPageSize;
			this.GetFacultyMembers(0);
		}

		protected void gvFacultyMembers_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetFacultyMembers(0);
		}

		private void GetFacultyMembers(int pageIndex)
		{
			var departmentID = this.ddlDepartmentID.SelectedValue.ToInt();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var semesterID = this.ddlSemesterID.SelectedValue.ToInt();
			var pageGuid = GetAspirePageAttribute<SurveyControlPanel>().AspirePageGuid;
			var status = this.ddlStatus.SelectedValue.ToNullableEnum<QASurveyUser.Statuses>();
			var searchText = this.tbSearchText.Text;
			if (programID != null)
			{
				var qaSurveyConduct = GenerateLinks.GetSurveyConduct(semesterID, departmentID, programID.Value, pageGuid, StaffIdentity.LoginSessionGuid);
				if (qaSurveyConduct != null)
				{
					int virtualItemCount;
					var faculty = GenerateLinks.GetFacultyMembers(qaSurveyConduct.QASurveyConductID, departmentID, programID, status, searchText, pageIndex, this.gvFacultyMembers.PageSize, out virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.StaffIdentity.LoginSessionGuid);
					var result = faculty.Select(f => new
					{
						FacultyMemberID = f.FacultyMemberID,
						Name = f.Name,
						Email = f.Email.ToNAIfNullOrEmpty(),
						DepartmentAlias = f.DepartmentAlias.ToNAIfNullOrEmpty(),
						StatusFullName = f.StatusFullName.ToNAIfNullOrEmpty(),
					})
						.ToList();
					if (result.Count == 0)
						this.btnAdd.Visible = false;
					else
						this.btnAdd.Visible = true;
					this.gvFacultyMembers.DataBind(faculty, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
				}
			}
			else
			{
				this.gvFacultyMembers.DataBind(null);
				this.btnAdd.Visible = false;
				return;
			}
		}

		protected void gvFacultyMembers_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetFacultyMembers(e.NewPageIndex);
		}

		protected void gvFacultyMembers_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "lbViewRecord":
					e.Handled = true;
					var qaSurveyUserID = e.DecryptedCommandArgument();
					if (qaSurveyUserID == "")
						this.AddErrorAlert("Survey Link is not sent to Faculty Member");
					else
						SurveyForm6.Form.Redirect(e.DecryptedCommandArgumentToInt());
					break;
			}
		}

		protected void cbCheckAll_CheckedChanged(object sender, EventArgs e)
		{
			var cbCheckAll = (Lib.WebControls.CheckBox)this.gvFacultyMembers.HeaderRow.FindControl("cbCheckAll");

			foreach (GridViewRow row in this.gvFacultyMembers.Rows)
			{
				var cbCheckIndivisual = (Lib.WebControls.CheckBox)row.FindControl("cbCheckIndividual");
				cbCheckIndivisual.Checked = cbCheckAll.Checked;
			}
		}

		protected void btnAdd_Click(object sender, EventArgs e)
		{
			if (!this.Page.IsValid)
				return;
			int addedSurveysCount = 0;
			var semesterID = this.ddlSemesterID.SelectedValue.ToInt();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToInt();
			var programID = this.ddlProgramID.SelectedValue.ToInt();
			var pageGuid = GetAspirePageAttribute<SurveyControlPanel>().AspirePageGuid;
			var qaSurveyConduct = GenerateLinks.GetSurveyConduct(semesterID, departmentID, programID, pageGuid, this.StaffIdentity.LoginSessionGuid);
			foreach (GridViewRow row in this.gvFacultyMembers.Rows)
			{
				var cbCheckIndivisual = (Lib.WebControls.CheckBox)row.FindControl("cbCheckIndividual");
				var hfFacultyID = (System.Web.UI.WebControls.HiddenField)row.FindControl("hfFacultyMemberID");
				if (cbCheckIndivisual.Checked)
				{
					var facultyID = Convert.ToInt32(hfFacultyID.Value);
					var queryResult = GenerateLinks.AddSurvey(qaSurveyConduct.QASurveyConductID, facultyID, StaffIdentity.InstituteID, departmentID, programID, this.StaffIdentity.LoginSessionGuid);
					switch (queryResult)
					{
						case GenerateLinks.AddSurveyEnum.None:
							throw new InvalidOperationException();
						case GenerateLinks.AddSurveyEnum.AlreadyExists:
							this.AddErrorAlert("Link already sent.");
							break;
						case GenerateLinks.AddSurveyEnum.Success:
							addedSurveysCount++;
							break;
						case GenerateLinks.AddSurveyEnum.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
			if (addedSurveysCount > 0)
				this.AddSuccessAlert("Link has been sent.");
			this.GetFacultyMembers(0);
		}

		public static string GetPageUrl()
		{
			return GetAspirePageAttribute<Generate>().PageUrl;
		}

		public static void Redirect()
		{
			Redirect(GetPageUrl());
		}
	}
}