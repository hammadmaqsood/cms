﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.BL.Core.QualityAssurance.Staff.Surveys.SurveyForm6;

namespace Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm6.SummaryAnalysis
{
	[AspirePage("F371CC59-3068-43B1-8514-A2D1FEC23DB3", UserTypes.Staff, "~/Sys/Staff/QA/Surveys/SurveyForm6/SummaryAnalysis/SummaryControlPanel.aspx", "Summary Control Panel (Survey Form-6)", true, AspireModules.QualityAssurance)]
	public partial class SummaryControlPanel : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.QualityAssurance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.cog);

		public override string PageTitle => "Control Panel: Department Survey Summary (Survey Form-6)";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("SemesterID", SortDirection.Descending);
				this.ddlSemesterID.FillSemesters(CommonListItems.All).SetSelectedValueIfNotPostback("SemesterID");
				this.ddlSemesterID_SelectedIndexChanged(null, null);
			}
		}

		protected void btnAdd_Click(object sender, EventArgs e)
		{
			this.DisplayControlPanelModal(null);
		}

		protected void ddlSemesterID_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void gvControlPanel_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvControlPanel_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvControlPanel.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvControlPanel_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvControlPanel_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "lbViewRecord":
					e.Handled = true;
					var qaSurveySummaryAnalysisID = e.DecryptedCommandArgumentToInt();
					if (qaSurveySummaryAnalysisID == 0)
						this.AddErrorAlert("Summary does not exists");
					else
						SummaryAnalysis.Redirect(e.DecryptedCommandArgumentToInt());
					break;
				case "Edit":
					e.Handled = true;
					this.DisplayControlPanelModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					qaSurveySummaryAnalysisID = e.DecryptedCommandArgumentToInt();
					this.StaffIdentity.Demand(StaffPermissions.QualityAssurance.ManageSummarySurveyForm6, UserGroupPermission.PermissionValues.Allowed);
					var deleted = QASummaryConducts.DeleteSummaryConduct(qaSurveySummaryAnalysisID, StaffIdentity.InstituteID, this.StaffIdentity.LoginSessionGuid);
					switch (deleted)
					{
						case QASurveySummaryAnalysi.ValidationResults.None:
							throw new InvalidOperationException();
						case QASurveySummaryAnalysi.ValidationResults.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case QASurveySummaryAnalysi.ValidationResults.Success:
							this.AddSuccessMessageHasBeenDeleted("Summary conduct schedule");
							break;
						case QASurveySummaryAnalysi.ValidationResults.SummaryGenerated:
							this.AddErrorAlert("Summary can not be deleted after submission.");
							break;
						case QASurveySummaryAnalysi.ValidationResults.AlreadyExists:
						case QASurveySummaryAnalysi.ValidationResults.StaffCannotEditThisRecord:
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.Refresh();
					break;
			}
		}

		protected void btnSave_Click(object sender, EventArgs e)
		{
			if (!this.Page.IsValid)
				return;
			var qaSummaryID = (int?)this.ViewState[nameof(QASurveySummaryAnalysi.QASurveySummaryAnalysisID)];
			var summaryTypeGuid = this.AspirePageAttribute.AspirePageGuid;
			var surveyTypeGuid = GetAspirePageAttribute<SurveyControlPanel>().AspirePageGuid;
			var semesterID = this.ddlSemesterForSave.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentForSave.SelectedValue.ToInt();
			if (qaSummaryID == null)
			{
				var summaryConductResult = QASummaryConducts.AddQASummaryConduct(semesterID, StaffIdentity.InstituteID, departmentID, surveyTypeGuid, summaryTypeGuid, this.StaffIdentity.LoginSessionGuid);
				switch (summaryConductResult)
				{
					case QASurveySummaryAnalysi.ValidationResults.None:
						throw new InvalidOperationException();
					case QASurveySummaryAnalysi.ValidationResults.NoRecordFound:
						this.alertControlPanel.AddNoRecordFoundAlert();
						this.updateModalControlPanel.Update();
						break;
					case QASurveySummaryAnalysi.ValidationResults.AlreadyExists:
						this.alertControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Record already exists.");
						this.updateModalControlPanel.Update();
						break;
					case QASurveySummaryAnalysi.ValidationResults.Success:
						this.alertControlPanel.AddSuccessMessageHasBeenAdded("Summary conduct schedule");
						this.Refresh();
						break;
					case QASurveySummaryAnalysi.ValidationResults.SummaryGenerated:
					case QASurveySummaryAnalysi.ValidationResults.StaffCannotEditThisRecord:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				this.StaffIdentity.Demand(StaffPermissions.QualityAssurance.ManageSummarySurveyForm6, UserGroupPermission.PermissionValues.Allowed);
				var qasurveyConductResult = QASummaryConducts.UpdateSummaryConduct(qaSummaryID.Value, semesterID, departmentID, surveyTypeGuid, summaryTypeGuid, this.StaffIdentity.LoginSessionGuid);
				switch (qasurveyConductResult)
				{
					case QASurveySummaryAnalysi.ValidationResults.None:
						throw new InvalidOperationException();
					case QASurveySummaryAnalysi.ValidationResults.NoRecordFound:
						this.AddNoRecordFoundAlert();
						break;
					case QASurveySummaryAnalysi.ValidationResults.AlreadyExists:
						this.alertControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Record already exists.");
						this.updateModalControlPanel.Update();
						break;
					case QASurveySummaryAnalysi.ValidationResults.SummaryGenerated:
						this.alertControlPanel.AddErrorAlert("Can not edit becuase summary has been generated for this conduct.");
						break;
					case QASurveySummaryAnalysi.ValidationResults.Success:
						this.AddSuccessMessageHasBeenUpdated("Summary conduct schedule");
						this.Refresh();
						break;
					case QASurveySummaryAnalysi.ValidationResults.StaffCannotEditThisRecord:
						throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
		private void DisplayControlPanelModal(int? qaSurveySummaryAnalysisID)
		{
			this.ddlSemesterForSave.FillSemesters();
			var instituteID = this.StaffIdentity.InstituteID;
			this.ddlDepartmentForSave.FillDepartments(instituteID);
			if (qaSurveySummaryAnalysisID == null)
			{
				this.ddlSemesterForSave.ClearSelection();
				this.ddlSemesterForSave.Enabled = true;
				this.modalControlPanel.HeaderText = "Add";
				this.btnSave.Text = "Add";
				this.ViewState[nameof(QASurveySummaryAnalysi.QASurveySummaryAnalysisID)] = null;
				this.modalControlPanel.Show();
			}
			else
			{
				var qaSummaryConduct = QASummaryConducts.GetSummaryConduct(qaSurveySummaryAnalysisID.Value, this.StaffIdentity.LoginSessionGuid);
				if (qaSummaryConduct == null)
				{
					this.AddErrorAlert("Record can not be updated because summary has been generated for the specified record.");
					this.GetData(0);
					return;
				}
				this.ddlSemesterForSave.Items.FindByValue(qaSummaryConduct.SemesterID.ToString()).Selected = true;
				this.ddlDepartmentForSave.Items.FindByValue(qaSummaryConduct.DepartmentID.ToString()).Selected = true;
				this.ddlSemesterForSave.Enabled = true;
				this.modalControlPanel.HeaderText = "Edit";
				this.btnSave.Text = "Save";
				this.ViewState[nameof(QASurveySummaryAnalysi.QASurveySummaryAnalysisID)] = qaSurveySummaryAnalysisID;
				this.modalControlPanel.Show();
			}
		}

		private void GetData(int pageIndex)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var pageGuid = this.AspirePageAttribute.AspirePageGuid;
			int virtualItemCount;
			var qasummaryConduct = QASummaryConducts.GetSummaryConducts(semesterID, pageGuid, pageIndex, this.gvControlPanel.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out virtualItemCount, this.StaffIdentity.LoginSessionGuid);
			this.gvControlPanel.DataBind(qasummaryConduct, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		public static string GetPageURL(short? semesterID)
		{
			return typeof(SummaryControlPanel).GetAspirePageAttribute().PageUrl.AttachQueryParam("SemesterID", semesterID);
		}

		public static void Redirect(short? semesterID)
		{
			Redirect(GetPageURL(semesterID));
		}

		private void Refresh()
		{
			Redirect(this.ddlSemesterID.SelectedValue.ToNullableShort());
		}
	}
}