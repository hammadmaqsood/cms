﻿using System;
using System.Collections.Generic;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm6.SummaryAnalysis
{
	[AspirePage("39D86ACE-A1FD-4392-8095-4C80837EBF5F", UserTypes.Staff, "~/Sys/Staff/QA/Surveys/SurveyForm6/SummaryAnalysis/SummaryAnalysis.aspx", "Summary Form-6", true, AspireModules.QualityAssurance)]
	public partial class SummaryAnalysis : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.QualityAssurance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.cog);

		public int? SummaryID => this.Request.GetParameterValue("qaSurveySummaryAnalysisID").ToInt();

		public override string PageTitle => "Summary of departments offering Ph.D programs (Survey Form-6)";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.Page.IsPostBack)
			{
				if (this.SummaryID != null)
					this.QASurveySummaryControl.LoadData(this.SummaryID.Value);
			}
		}
		public static string GetPageUrl(int? qaSurveySummaryAnalysisID)
		{
			return GetAspirePageAttribute<SummaryAnalysis>().PageUrl.AttachQueryParam("qaSurveySummaryAnalysisID", qaSurveySummaryAnalysisID);
		}

		public static void Redirect(int? qaSurveySummaryAnalysisID)
		{
			Redirect(GetPageUrl(qaSurveySummaryAnalysisID));
		}
	}
}