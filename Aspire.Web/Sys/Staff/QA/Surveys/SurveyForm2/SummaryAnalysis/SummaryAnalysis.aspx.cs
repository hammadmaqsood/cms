﻿using System;
using System.Collections.Generic;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm2.SummaryAnalysis
{
	[AspirePage("5CC2E97D-B40A-413D-98E9-C78928C8C9F7", UserTypes.Staff, "~/Sys/Staff/QA/Surveys/SurveyForm2/SummaryAnalysis/SummaryAnalysis.aspx", "Summary Form-2", true, AspireModules.QualityAssurance)]
	public partial class SummaryAnalysis : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.QualityAssurance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.cog);

		public int? SummaryID => this.Request.GetParameterValue("qaSurveySummaryAnalysisID").ToInt();

		public override string PageTitle => "Faculty Course Review Report Summary (Survey Form-2)";
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.Page.IsPostBack)
			{
				if (this.SummaryID != null)
					this.QASurveySummaryControl.LoadData(this.SummaryID.Value);
			}
		}
		public static string GetPageUrl(int? qaSurveySummaryAnalysisID)
		{
			return GetAspirePageAttribute<SummaryAnalysis>().PageUrl.AttachQueryParam("qaSurveySummaryAnalysisID", qaSurveySummaryAnalysisID);
		}

		public static void Redirect(int? qaSurveySummaryAnalysisID)
		{
			Redirect(GetPageUrl(qaSurveySummaryAnalysisID));
		}
	}
}