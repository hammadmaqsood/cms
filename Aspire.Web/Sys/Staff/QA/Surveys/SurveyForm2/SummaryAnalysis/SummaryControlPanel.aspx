﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="SummaryControlPanel.aspx.cs" Inherits="Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm2.SummaryAnalysis.SummaryControlPanel" %>


<asp:Content ID="Content2" ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireButton runat="server" ID="btnAdd" ButtonType="Success" Glyphicon="plus" Text="Add" OnClick="btnAdd_Click" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_SelectedIndexChanged" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvControlPanel" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gvControlPanel_PageIndexChanging" OnPageSizeChanging="gvControlPanel_PageSizeChanging" OnSorting="gvControlPanel_Sorting" OnRowCommand="gvControlPanel_RowCommand">
		<Columns>
			<asp:BoundField HeaderText="Semester" SortExpression="SemesterID" DataField="Semester" />
			<asp:BoundField HeaderText="Department" SortExpression="DepartmentAlias" DataField="DepartmentAlias" />
			<asp:BoundField HeaderText="Program" SortExpression="ProgramAlias" DataField="ProgramAlias" />
			<asp:BoundField HeaderText="Summary Status" SortExpression="StatusFullName" DataField="StatusFullName" />
			<asp:TemplateField HeaderText="Summary">
				<ItemTemplate>
					<aspire:AspireLinkButton ID="lbViewRecord" CommandName="lbViewRecord" runat="server" Text="View" EncryptedCommandArgument='<%#this.Eval("QASurveySummaryAnalysisID") %>'></aspire:AspireLinkButton>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CausesValidation="False" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument='<%#this.Eval("QASurveySummaryAnalysisID") %>' />
					<aspire:AspireLinkButton runat="server" CausesValidation="False" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%#this.Eval("QASurveySummaryAnalysisID") %>' ConfirmMessage="Are you sure you want to delete survey conduct schedule?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<aspire:AspireModal runat="server" ID="modalControlPanel">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updateModalControlPanel" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertControlPanel" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterForSave" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlSemesterForSave" ValidationGroup="Req" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterForSave" ValidationGroup="Req" ErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentForSave" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlDepartmentForSave" AutoPostBack="true" ValidationGroup="Filters" OnSelectedIndexChanged="ddlDepartmentForSave_SelectedIndexChanged" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlDepartmentForSave" ValidationGroup="Req" ErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramForSave" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlProgramForSave" AutoPostBack="true" ValidationGroup="Filters" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlProgramForSave" ValidationGroup="Req" ErrorMessage="This field is required." />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ValidationGroup="Req" ButtonType="Primary" OnClick="btnSave_Click" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
	<aspire:AspireModal runat="server" ID="modlLinks">
		<BodyTemplate>
		</BodyTemplate>
	</aspire:AspireModal>
</asp:Content>
