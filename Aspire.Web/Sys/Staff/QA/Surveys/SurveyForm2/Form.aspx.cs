﻿using System;
using System.Collections.Generic;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm2
{
	[AspirePage("59CBF743-8835-4B93-B047-E628CC99C97A", UserTypes.Staff, "~/Sys/Staff/QA/Surveys/SurveyForm2/Form.aspx", "Survey Form-2", true, AspireModules.QualityAssurance)]
	public partial class Form : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.QualityAssurance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.cog);

		public int? SurveyID => this.Request.GetParameterValue("qaSurveyUserID").ToInt();

		public override string PageTitle => "Faculty Course Review Report (Survey Form-2)";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.Page.IsPostBack)
			{
				if (this.SurveyID != null)
					this.QAsurveyUserControl.LoadData(this.SurveyID.Value);
			}
		}

		public static string GetPageUrl(int? qaSurveyUserID)
		{
			return GetAspirePageAttribute<Form>().PageUrl.AttachQueryParam("qaSurveyUserID", qaSurveyUserID);
		}

		public static void Redirect(int? qaSurveyUserID)
		{
			Redirect(GetPageUrl(qaSurveyUserID));
		}
	}
}