﻿using System;
using System.Collections.Generic;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm7
{
	[AspirePage("1947F685-BED7-40F6-8EC6-3A9B05116034", UserTypes.Staff, "~/Sys/Staff/QA/Surveys/SurveyForm7/Form.aspx", "Survey Form-7", true, AspireModules.QualityAssurance)]
	public partial class Form : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.QualityAssurance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.cog);

		public int? SurveyID => this.Request.GetParameterValue("qaSurveyUserID").ToInt();

		public override string PageTitle => "Alumni Survey (Survey Form-7)";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (this.SurveyID != null)
				this.QAsurveyUserControl.LoadData(this.SurveyID.Value);
		}
		public static string GetPageUrl(int? qaSurveyUserID)
		{
			return GetAspirePageAttribute<Form>().PageUrl.AttachQueryParam("qaSurveyUserID", qaSurveyUserID);
		}

		public static void Redirect(int? qaSurveyUserID)
		{
			Redirect(GetPageUrl(qaSurveyUserID));
		}
	}
}