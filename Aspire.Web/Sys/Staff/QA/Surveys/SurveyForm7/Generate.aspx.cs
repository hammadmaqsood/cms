﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Aspire.BL.Core.QualityAssurance.Staff.Surveys.SurveyForm7;

namespace Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm7
{
	[AspirePage("681439C9-AB46-42BC-A7D6-812C9355F838", UserTypes.Staff, "~/Sys/Staff/QA/Surveys/SurveyForm7/Generate.aspx", "Generate Survey (Survey Form-7)", true, AspireModules.QualityAssurance)]
	public partial class Generate : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.QualityAssurance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.cog);

		public override string PageTitle => "Generate Link: Alumni Survey (Survey Form-7)";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Name", SortDirection.Ascending);
				var instituteID = this.StaffIdentity.InstituteID;
				this.ddlDepartmentID.FillDepartments(instituteID);
				this.ddlStatus.FillEnums<QASurveyUser.Statuses>(CommonListItems.All);
				var deptId = ddlDepartmentID.SelectedItem.Value.ToInt();
				this.ddlProgramID.FillPrograms(instituteID, deptId, null, true);
				this.ddlSemesterID.FillSemesters();
				this.ddlSemesterID_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterID_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetAlumniMembers(0);
		}

		protected void ddlDepartmentID_SelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.StaffIdentity.InstituteID;
			var deptId = ddlDepartmentID.SelectedItem.Value.ToInt();
			this.ddlProgramID.FillPrograms(instituteID, deptId, null, true);
			this.GetAlumniMembers(0);
		}

		protected void ddlProgramID_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetAlumniMembers(0);
		}

		protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetAlumniMembers(0);
		}

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			this.GetAlumniMembers(0);
		}

		protected void gvAlumniStudents_PageSizeChanging(object sender, Lib.WebControls.AspireGridView.PageSizeEventArgs e)
		{
			this.gvAlumniStudents.PageSize = e.NewPageSize;
			this.GetAlumniMembers(0);
		}

		protected void gvAlumniStudents_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetAlumniMembers(0);
		}

		protected void gvAlumniStudents_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetAlumniMembers(e.NewPageIndex);
		}

		protected void gvAlumniStudents_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "lbViewRecord":
					e.Handled = true;
					var qaSurveyUserID = e.DecryptedCommandArgument();
					if (qaSurveyUserID == "")
						this.AddErrorAlert("Survey Link is not sent to Faculty Member");
					else
						SurveyForm7.Form.Redirect(e.DecryptedCommandArgumentToInt());
					break;
			}
		}

		protected void cbCheckAll_CheckedChanged(object sender, EventArgs e)
		{
			var cbCheckAll = (Lib.WebControls.CheckBox)this.gvAlumniStudents.HeaderRow.FindControl("cbCheckAll");

			foreach (GridViewRow row in this.gvAlumniStudents.Rows)
			{
				var cbCheckIndivisual = (Lib.WebControls.CheckBox)row.FindControl("cbCheckIndividual");
				cbCheckIndivisual.Checked = cbCheckAll.Checked;
			}
		}

		protected void btnAdd_Click(object sender, EventArgs e)
		{
			if (!this.Page.IsValid)
				return;
			int addedSurveysCount = 0;
			var semesterID = this.ddlSemesterID.SelectedValue.ToInt();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToInt();
			var programID = this.ddlProgramID.SelectedValue.ToInt();
			var pageGuid = GetAspirePageAttribute<SurveyControlPanel>().AspirePageGuid;
			var qaSurveyConduct = GenerateLinks.GetSurveyConduct(semesterID, departmentID, programID, pageGuid, this.StaffIdentity.LoginSessionGuid);
			foreach (GridViewRow row in this.gvAlumniStudents.Rows)
			{
				var cbCheckIndivisual = (Lib.WebControls.CheckBox)row.FindControl("cbCheckIndividual");
				var hfAlumniStudentID = (System.Web.UI.WebControls.HiddenField)row.FindControl("hfAlumniStudentID");
				if (cbCheckIndivisual.Checked)
				{
					var AlumniID = Convert.ToInt32(hfAlumniStudentID.Value);
					var SurveyPageUrl = GetAspirePageAttribute<Student.QualityAssurance.AlumniSurvey>().PageUrl.ToAbsoluteUrl();
					var queryResult = GenerateLinks.AddSurvey(qaSurveyConduct.QASurveyConductID, AlumniID, StaffIdentity.InstituteID, departmentID, programID, SurveyPageUrl, this.StaffIdentity.LoginSessionGuid);
					switch (queryResult)
					{
						case GenerateLinks.AddSurveyEnum.None:
							throw new InvalidOperationException();
						case GenerateLinks.AddSurveyEnum.AlreadyExists:
							this.AddErrorAlert("Link already sent.");
							break;
						case GenerateLinks.AddSurveyEnum.Success:
							addedSurveysCount++;
							break;
						case GenerateLinks.AddSurveyEnum.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
			if (addedSurveysCount > 0)
				this.AddSuccessAlert("Link has been sent.");
			this.GetAlumniMembers(0);
		}

		private void GetAlumniMembers(int pageIndex)
		{
			var departmentID = this.ddlDepartmentID.SelectedValue.ToInt();
			var programID = this.ddlProgramID.SelectedValue.ToInt();
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var pageGuid = GetAspirePageAttribute<SurveyControlPanel>().AspirePageGuid;
			var status = this.ddlStatus.SelectedValue.ToNullableEnum<QASurveyUser.Statuses>();
			var searchText = this.tbSearchText.Text;
			var qaSurveyConduct = GenerateLinks.GetSurveyConduct(semesterID, departmentID, programID, pageGuid, StaffIdentity.LoginSessionGuid);
			if (qaSurveyConduct != null)
			{
				int virtualItemCount;
				var faculty = GenerateLinks.GetAlumniMembers(qaSurveyConduct.QASurveyConductID, departmentID, programID, semesterID, status, searchText, pageIndex, this.gvAlumniStudents.PageSize, out virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.StaffIdentity.LoginSessionGuid);
				var result = faculty.Select(f => new
				{
					AlumniStudentID = f.AlumniStudentID,
					Name = f.Name,
					Enrollment = f.Enrollment,
					Email = f.Email.ToNAIfNullOrEmpty(),
					ProgramAlias = f.ProgramAlias.ToNAIfNullOrEmpty(),
					StatusFullName = f.StatusFullName.ToNAIfNullOrEmpty(),
				})
					.ToList();
				if (result.Count == 0)
					this.btnAdd.Visible = false;
				else
					this.btnAdd.Visible = true;
				this.gvAlumniStudents.DataBind(faculty, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
			}
			else
			{
				this.gvAlumniStudents.DataBind(null);
				this.btnAdd.Visible = false;
				return;
			}
		}
		public static string GetPageUrl()
		{
			return GetAspirePageAttribute<Generate>().PageUrl;
		}

		public static void Redirect()
		{
			Redirect(GetPageUrl());
		}
	}
}