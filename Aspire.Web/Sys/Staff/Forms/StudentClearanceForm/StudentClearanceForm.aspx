﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="StudentClearanceForm.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Forms.StudentClearanceForm.StudentClearanceForm" %>

<%@ Register Src="~/Sys/Common/Forms/StudentClearanceForm/StudentClearanceForm.ascx" TagPrefix="uc1" TagName="ClearanceForm" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
    <div class="form-inline">
        <div class="form-group">
            <aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
            <aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="Search" OnSearch="tbEnrollment_Search" />
        </div>
    </div>
    <asp:Panel runat="server" ID="panelStudent">
        <p></p>
        <uc1:clearanceForm runat="server" id="clearanceForm" />
    </asp:Panel>

   <%-- <p></p>
    <div class="text-center" runat="server" id="divSSC">
        <p runat="server" id="pSSCMsg">Please Verify the student clearance form. (If selected Verfied it will not be changed again)</p>
        <aspire:DropDownList runat="server" ID="ddlSSCStatus" AutoPostBack="true" OnSelectedIndexChanged="ddlSSCStatus_SelectedIndexChanged" />
    </div>
    <p></p>--%>

</asp:Content>

