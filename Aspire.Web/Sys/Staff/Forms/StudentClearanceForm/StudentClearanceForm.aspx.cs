﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Forms.StudentClearanceForm
{
	[AspirePage("659F4B7D-CD4F-4E53-8500-B13A58C0BAF4", UserTypes.Staff, "~/Sys/Staff/Forms/StudentClearanceForm/StudentClearanceForm.aspx", "Student Clearance Form", true, AspireModules.Forms)]

	public partial class StudentClearanceForm : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.menu_hamburger);
		public override string PageTitle => Model.Entities.Form.FormTypes.StudentClearanceForm.GetFullName();

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Forms.Module, UserGroupPermission.Allowed},
		};

		public static string GetPageUrl(string enrollment, bool search)
		{
			return GetAspirePageAttribute<StudentClearanceForm>().PageUrl.AttachQueryParams("Enrollment", enrollment, "Search", search);
		}

		public static string GetPageURL(string enrollment, bool search)
		{
			return typeof(StudentClearanceForm).GetAspirePageAttribute().PageUrl.AttachQueryParams("Enrollment", enrollment, "Search", search);
		}

		public static new void Redirect(string enrollment, bool search)
		{
			Redirect(GetPageUrl(enrollment, search));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.panelStudent.Visible = false;
				var enrollment = this.Request.GetParameterValue("Enrollment");
				this.tbEnrollment.Enrollment = enrollment;
				if (this.Request.GetParameterValue<bool>("Search") == true)
					this.tbEnrollment_Search(null, null);
			}
		}

		protected void tbEnrollment_Search(object sender, EventArgs args)
		{
			var enrollment = this.tbEnrollment.Enrollment;
			var data = BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceForm.GetStudentFormData(null, enrollment, this.AspireIdentity.LoginSessionGuid);
			if (data == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect(enrollment, false);
				this.panelStudent.Visible = false;
				return;
			}

			this.panelStudent.Visible = true;
			this.clearanceForm.LoadData(data);
		}
	}
}