﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="SearchClearanceForm.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Forms.StudentClearanceForm.SearchClearanceForm" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Web.Sys.Staff.Students" %>



<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
    <aspire:AspirePanel runat="server" HeaderText="Applicants">
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <aspire:AspireLabel runat="server" Text="SSC Status:" AssociatedControlID="ddlSSCStatus" />
                    <aspire:AspireDropDownList runat="server" ID="ddlSSCStatus" OnSelectedIndexChanged="ddlSSCStatus_SelectedIndexChanged" AutoPostBack="true" />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <aspire:AspireLabel runat="server" Text="Form Status" AssociatedControlID="ddlFormStatus" />
                    <aspire:AspireDropDownList runat="server" ID="ddlFormStatus" AutoPostBack="True" OnSelectedIndexChanged="ddlFormStatus_SelectedIndexChanged" ValidationGroup="Filter" />
                </div>
            </div>            
            <div class="col-md-2">
                <div class="form-group">
                    <aspire:AspireLabel runat="server" Text="Department Status:" AssociatedControlID="ddlDepartmentStatus" />
                    <aspire:AspireDropDownList runat="server" ID="ddlDepartmentStatus" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentStatus_SelectedIndexChanged" ValidationGroup="Filter" />
                </div>
            </div>
			<div class="col-md-3">
                 <div class="form-group">
                    <aspire:AspireLabel runat="server" Text="Departments" AssociatedControlID="ddlDepartments" />
                    <aspire:AspireDropDownList runat="server" ID="ddlDepartments" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartments_SelectedIndexChanged" ValidationGroup="Filter" />
                </div>
            </div>
			 <div class="col-md-3">
				 <div>
					<aspire:AspireLabel runat="server" Text="&nbsp;" AssociatedControlID="tbSearch" />
				</div>
                <div class="form-group">
                    <asp:Panel runat="server" CssClass="input-group" DefaultButton="btnSearch">
                        <aspire:AspireTextBox runat="server" ID="tbSearch" PlaceHolder="Search" />
                        <div class="input-group-btn">
                            <aspire:AspireButton ButtonType="Default" Glyphicon="search" runat="server" ID="btnSearch" OnClick="btnSearch_Click" />
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </div>
        <aspire:AspireGridView runat="server" ID="gvSearch" AutoGenerateColumns="false" Responsive="True" Condensed="True" ItemType="Aspire.BL.Core.Forms.StudentClearanceForm.Staff.SearchStudentClearanceForm.SearchClearanceForm" AllowPaging="True" OnPageIndexChanging="gvSearch_PageIndexChanging" OnPageSizeChanging="gvSearch_PageSizeChanging">
            <Columns>
                <asp:TemplateField HeaderText="#">
                    <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Enrollment">
                    <ItemTemplate>
                        <aspire:AspireHyperLink runat="server" Text='<%# Item.Enrollment %>' NavigateUrl='<%# StudentProfileView.GetPageUrl(Item.Enrollment, true) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Name" DataField="Name" />
                <asp:TemplateField HeaderText="Submission Date">
                    <ItemTemplate>
                        <%#: (Item.FormSubmissionDate?.ToString("d")).ToNAIfNullOrEmpty() %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Form Status">
                    <ItemTemplate>
                        <%#: Item.FormStudentClearanceStatusEnum.GetFullName() %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SSC Status">
                    <ItemTemplate>
                        <%#: Item.FormStudentClearanceSSCStatusEnum.GetFullName() %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Actions">
                    <ItemTemplate>
                        <aspire:AspireHyperLink runat="server" NavigateUrl="<%# Aspire.Web.Sys.Staff.Forms.StudentClearanceForm.StudentClearanceForm.GetPageUrl(Item.Enrollment, true) %>" Text="Status" Target="_blank" ID="gvBtnViewStatus" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </aspire:AspireGridView>
    </aspire:AspirePanel>
</asp:Content>
