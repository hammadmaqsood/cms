﻿using System;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System.Collections.Generic;
using Aspire.Web.Common;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Forms.StudentClearanceForm
{
	[AspirePage("575097DA-786C-4439-BC73-218320D4F809", UserTypes.Staff, "~/Sys/Staff/Forms/StudentClearanceForm/SearchClearanceForm.aspx", "Search Clearance Form", true, AspireModules.Forms)]

	public partial class SearchClearanceForm : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.menu_hamburger);
		public override string PageTitle => "Search Clearance Form";

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Forms.Module, UserGroupPermission.Allowed},
		};

		private FormStudentClearance.StudentClearanceStatuses? FormStudentClearanceStatus => this.GetParameterValue<FormStudentClearance.StudentClearanceStatuses>(nameof(this.FormStudentClearanceStatus));
		private FormStudentClearance.StudentClearanceSSCStatuses? FormStudentClearanceSSCStatus => this.GetParameterValue<FormStudentClearance.StudentClearanceSSCStatuses>(nameof(this.FormStudentClearanceSSCStatus));
		private FormStudentClearanceDepartmentStatus.Departments? Departments => this.GetParameterValue<FormStudentClearanceDepartmentStatus.Departments>(nameof(this.Departments));
		private FormStudentClearanceDepartmentStatus.DepartmentStatuses? DepartmentStatus => this.GetParameterValue<FormStudentClearanceDepartmentStatus.DepartmentStatuses>(nameof(this.DepartmentStatus));


		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				this.ddlSSCStatus.FillGuidEnums<FormStudentClearance.StudentClearanceSSCStatuses>(CommonListItems.All).SetSelectedEnumValueIfNotPostback<FormStudentClearance.StudentClearanceSSCStatuses>(nameof(this.ddlSSCStatus));
				this.ddlFormStatus.FillGuidEnums<FormStudentClearance.StudentClearanceStatuses>(CommonListItems.All).SetSelectedEnumValueIfNotPostback<FormStudentClearance.StudentClearanceStatuses>(nameof(this.ddlFormStatus));
				this.ddlDepartments.FillGuidEnums<FormStudentClearanceDepartmentStatus.Departments>(CommonListItems.All).SetSelectedEnumValueIfNotPostback<FormStudentClearanceDepartmentStatus.Departments>(nameof(this.Departments));
				this.ddlDepartmentStatus.FillGuidEnums<FormStudentClearanceDepartmentStatus.DepartmentStatuses>(CommonListItems.All).SetSelectedEnumValueIfNotPostback<FormStudentClearanceDepartmentStatus.DepartmentStatuses>(nameof(this.DepartmentStatus));
				this.GetData(0);
			}
		}

		private void GetData(int pageIndex)
		{
			var searchText = this.tbSearch.Text;
			var sscStatusEnum = this.ddlSSCStatus.GetSelectedNullableGuidEnum<FormStudentClearance.StudentClearanceSSCStatuses>();
			var formStatusEnum = this.ddlFormStatus.GetSelectedNullableGuidEnum<FormStudentClearance.StudentClearanceStatuses>();
			var departmentsEnum = this.ddlDepartments.GetSelectedNullableGuidEnum<FormStudentClearanceDepartmentStatus.Departments>();
			var departmentStatusEnum = this.ddlDepartmentStatus.GetSelectedNullableGuidEnum<FormStudentClearanceDepartmentStatus.DepartmentStatuses>();

			var result = Aspire.BL.Core.Forms.StudentClearanceForm.Staff.SearchStudentClearanceForm.StudentClearanceData(searchText, sscStatusEnum, formStatusEnum, departmentsEnum, departmentStatusEnum, pageIndex, this.gvSearch.PageSize, out var virtualItemCount, Web.Common.AspireIdentity.Current.LoginSessionGuid);

			if (result != null)
				this.gvSearch.DataBind(null);
			this.gvSearch.DataBind(result, pageIndex, virtualItemCount);
		}

		protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvSearch_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvSearch.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void ddlSSCStatus_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void ddlFormStatus_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void ddlDepartments_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void ddlDepartmentStatus_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}
	}
}