﻿using Aspire.Lib.Helpers;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Staff
{
	[AspirePage("{4D4B4FF6-915F-418B-88AD-C3B6702D165E}", UserTypes.Staff, "~/Sys/Staff/Logoff.aspx", null, false, AspireModules.None)]
	public partial class Logoff : LogoffBasePage
	{
		public static string GetPageUrl(UserLoginHistory.LogOffReasons logoffReason)
		{
			return GetPageUrl(GetPageUrl<Logoff>(), logoffReason);
		}

		public static void Redirect(UserLoginHistory.LogOffReasons logoffReason)
		{
			Redirect(GetPageUrl(logoffReason));
		}

		protected override string LoginPageUrl => GetPageUrl<Logins.Staff.Login>();
	}
}