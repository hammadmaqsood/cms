﻿<%@ Page MasterPageFile="~/Base.Master" Language="C#" AutoEventWireup="true" CodeBehind="DateAndSessions.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.DateAndSessions" %>

<asp:Content runat="server" ContentPlaceHolderID="BodyPH">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireButton runat="server" CausesValidation="False" ID="btnAddExamDateAndSession" Text="Add Date and Session" Glyphicon="plus" OnClick="btnAddExamDateAndSession_OnClick" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
					<aspire:AspireDropDownList runat="server" CausesValidation="False" ID="ddlSemesterIDFilter" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" AutoPostBack="True" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Exam:" AssociatedControlID="ddlExamTypeFilter" />
					<aspire:AspireDropDownList runat="server" CausesValidation="False" ID="ddlExamTypeFilter" OnSelectedIndexChanged="ddlExamTypeFilter_OnSelectedIndexChanged" AutoPostBack="True" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Exam Conduct:" AssociatedControlID="ddlExamConductIDFilter" />
					<aspire:AspireDropDownList runat="server" CausesValidation="False" ID="ddlExamConductIDFilter" OnSelectedIndexChanged="ddlExamConductIDFilter_OnSelectedIndexChanged" AutoPostBack="True" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvExamDateAndSessions" AutoGenerateColumns="False" OnRowCommand="gvExamDateAndSessions_OnRowCommand">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%#: Container.DataItemIndex+1%></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Semester" DataField="Semester" />
			<asp:BoundField HeaderText="Exam Type" DataField="ExamTypeFullName" />
			<asp:BoundField HeaderText="Exam Conduct" DataField="ExamConductName" />
			<asp:BoundField HeaderText="Date" DataField="Date" DataFormatString="{0:D}" />
			<asp:BoundField HeaderText="Session Name" DataField="SessionName" />
			<asp:BoundField HeaderText="Timing" DataField="Timing" />
			<asp:BoundField HeaderText="Shift" DataField="ShiftFullName" />
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%# Eval("ExamDateAndSessionID") %>' ConfirmMessage="Are you sure you want to delete?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalAddExamDateAndSession" HeaderText="Add Exam Session">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelDateAndSession" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertAddDateAndSession" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" CausesValidation="False" ValidationGroup="Session" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterID" ErrorMessage="This field is required." ValidationGroup="Session" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Exam Type:" AssociatedControlID="ddlExamType" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlExamType" CausesValidation="False" ValidationGroup="Session" AutoPostBack="True" OnSelectedIndexChanged="ddlExamType_OnSelectedIndexChanged" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamType" ErrorMessage="This field is required." ValidationGroup="Session" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Exam Conduct:" AssociatedControlID="ddlExamConductID" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlExamConductID" ValidationGroup="Session" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="ddlExamConductID_OnSelectedIndexChanged" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamConductID" ErrorMessage="This field is required." ValidationGroup="Session" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Exam Date:" AssociatedControlID="ddlExamDateID" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlExamDateID" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamDateID" ErrorMessage="This field is required." ValidationGroup="Session" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Session Name:" AssociatedControlID="ddlExamSessionID" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlExamSessionID" ValidationGroup="Session" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamSessionID" ErrorMessage="This field is required." ValidationGroup="Session" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ButtonType="Primary" Text="Add" ID="btnSave" ValidationGroup="Session" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server" CausesValidation="False">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
