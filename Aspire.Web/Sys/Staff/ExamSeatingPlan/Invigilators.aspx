﻿<%@ Page MasterPageFile="~/Base.Master" Language="C#" AutoEventWireup="true" CodeBehind="Invigilators.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.Invigilators" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>

<asp:Content runat="server" ContentPlaceHolderID="BodyPH">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireButton runat="server" Text="Add Invigilator" CausesValidation="False" Glyphicon="plus" ID="btnAddInvigilator" OnClick="btnAddInvigilator_OnClick" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Designation:" AssociatedControlID="ddlExamDesignationIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlExamDesignationIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlExamDesignationIDFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
				</div>
				<asp:Panel runat="server" CssClass="input-group" DefaultButton="btnSearch">
					<aspire:AspireTextBox runat="server" ID="tbSearchText" ValidationGroup="Filter" />
					<div class="input-group-btn">
						<aspire:AspireButton ButtonType="Default" Glyphicon="search" runat="server" ID="btnSearch" ValidationGroup="Filter" OnClick="btnSearch_Click" />
					</div>
				</asp:Panel>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvExamInvigilators" AutoGenerateColumns="False" OnRowCommand="gvExamInvigilators_OnRowCommand" AllowSorting="True" OnSorting="gvExamInvigilators_OnSorting" AllowPaging="True" OnPageIndexChanging="gvExamInvigilators_OnPageIndexChanging" OnPageSizeChanging="gvExamInvigilators_OnPageSizeChanging">
		<Columns>
			<asp:BoundField HeaderText="Invigilator" DataField="Name" SortExpression="Name" />
			<asp:TemplateField HeaderText="Department" SortExpression="DepartmentName">
				<ItemTemplate>
					<%#: Eval("DepartmentName").ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Designation" DataField="Designation" SortExpression="Designation" />
			<asp:BoundField HeaderText="CNIC" DataField="CNIC" SortExpression="CNIC" />
			<asp:BoundField HeaderText="Bank" DataField="Bank" SortExpression="Bank" />
			<asp:BoundField HeaderText="Account No." DataField="AccountNo" SortExpression="AccountNo" />
			<asp:TemplateField HeaderText="Status" SortExpression="Status">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" Text='<%# Eval("StatusFullName") %>' CommandName="Status" EncryptedCommandArgument='<%# Eval("ExamInvigilatorID") %>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" Glyphicon="edit" CausesValidation="False" CommandName="Edit" EncryptedCommandArgument='<%# Eval("ExamInvigilatorID") %>' />
					<aspire:AspireLinkButton runat="server" Glyphicon="remove" CausesValidation="False" CommandName="Delete" EncryptedCommandArgument='<%# Eval("ExamInvigilatorID") %>' ConfirmMessage="Are you sure you want to delete this invigilator?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalAddInvigilator" HeaderText="Add Invigilator">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelInvigilator" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertAddInvigilator" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
							<div class="col-md-8">
								<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" ValidationGroup="Add" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Designation:" AssociatedControlID="ddlExamDesignationID" />
							<div class="col-md-8">
								<aspire:AspireDropDownList runat="server" ID="ddlExamDesignationID" ValidationGroup="Add" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamDesignationID" ErrorMessage="This field is required." ValidationGroup="Add" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Name:" AssociatedControlID="tbInvigilatorName" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" ID="tbInvigilatorName" ValidationGroup="Add" />
								<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbInvigilatorName" RequiredErrorMessage="This field is required." ValidationGroup="Add" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="CNIC:" AssociatedControlID="tbCnic" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" ID="tbCnic" data-inputmask="'alias': 'cnic'" ValidationGroup="Add" />
								<aspire:AspireStringValidator runat="server" ControlToValidate="tbCnic" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid CNIC." ValidationGroup="Add" ValidationExpression="CNIC" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Bank:" AssociatedControlID="tbBank" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" ID="tbBank" ValidationGroup="Add" />
								<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbBank" RequiredErrorMessage="This field is required." ValidationGroup="Add" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Account Number:" AssociatedControlID="tbAccountNumber" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" ID="tbAccountNumber" ValidationGroup="Add" />
								<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbAccountNumber" RequiredErrorMessage="This field is required." ValidationGroup="Add" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Status:" AssociatedControlID="rblStatus" />
							<div class="col-md-8">
								<aspire:AspireRadioButtonList runat="server" ID="rblStatus" RepeatLayout="Flow" RepeatDirection="Horizontal" ValidationGroup="Add" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblStatus" ErrorMessage="This field is required." ValidationGroup="Add" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ButtonType="Primary" Text="Add" ID="btnSave" OnClick="btnSave_OnClick" ValidationGroup="Add" />
					<aspire:AspireModalCloseButton runat="server" CausesValidation="False">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>

