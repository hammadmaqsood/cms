﻿using Aspire.BL.Core.ExamSeatingPlan.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("9CD9350B-0E4D-44CA-837A-1B148082F567", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Sessions.aspx", "Sessions", true, AspireModules.ExamSeatingPlan)]
	public partial class Sessions : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};
		public override PageIcon PageIcon => FontAwesomeIcons.solid_clock.GetIcon();
		public override string PageTitle => "Exam Sessions";

		public static string GetPageUrl(short? semesterID, ExamConduct.ExamTypes? examType, int? examConductID)
		{
			return GetAspirePageAttribute<ExamSeatingPlan.Sessions>().PageUrl.AttachQueryParams("SemesterID", semesterID, "ExamType", examType, "ExamConductID", examConductID);
		}

		private void RefreshPage()
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var examType = this.ddlExamTypeFilter.GetSelectedEnumValue<ExamConduct.ExamTypes>(null);
			var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToNullableInt();
			this.Response.Redirect(GetPageUrl(semesterID, examType, examConductID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
				this.ddlExamTypeFilter.FillExamConductExamTypes().SetSelectedValueIfNotPostback("ExamType");
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamTypeFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamTypeFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var examTypeEnum = this.ddlExamTypeFilter.GetSelectedEnumValue<ExamConduct.ExamTypes>();
			if (semesterID == null)
				this.ddlExamConductIDFilter.Items.Clear();
			else
			{
				var examConducts = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamConducts.GetExamConductsList(semesterID.Value, examTypeEnum, this.StaffIdentity.LoginSessionGuid);
				this.ddlExamConductIDFilter.DataBind(examConducts).SetSelectedValueIfNotPostback("ExamConductID");
				this.ddlExamConductIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlExamConductIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetExamSessions();
		}

		private void GetExamSessions()
		{
			var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToNullableInt();
			if (examConductID == null)
			{
				this.gvExamSessions.DataBindNull();
				return;
			}
			var sessions = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamSessions.GetExamSessions(examConductID.Value, this.StaffIdentity.LoginSessionGuid);
			this.gvExamSessions.DataBind(sessions);
		}

		protected void gvExamSessions_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Up":
				case "Down":
					var examSessionID = e.DecryptedCommandArgumentToInt();
					ExamSessions.MoveExamSessionStatuses moveStatus;
					if (e.CommandName == "Up")
						moveStatus = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamSessions.MoveExamSessionUp(examSessionID, this.StaffIdentity.LoginSessionGuid);
					else
						moveStatus = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamSessions.MoveExamSessionDown(examSessionID, this.StaffIdentity.LoginSessionGuid);
					switch (moveStatus)
					{
						case ExamSessions.MoveExamSessionStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamSessions.MoveExamSessionStatuses.AlreadyAtTop:
							this.AddInfoAlert("Exam Session is already at top.");
							break;
						case ExamSessions.MoveExamSessionStatuses.AlreadyAtBottom:
							this.AddInfoAlert("Exam Session is already at bottom.");
							break;
						case ExamSessions.MoveExamSessionStatuses.Success:
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.RefreshPage();
					break;
				case "Edit":
					e.Handled = true;
					this.ShowExamSessionModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					examSessionID = e.DecryptedCommandArgumentToInt();
					var status = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamSessions.DeleteExamSession(examSessionID, this.StaffIdentity.LoginSessionGuid);
					switch (status)
					{
						case ExamSessions.DeleteExamSessionStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.RefreshPage();
							return;
						case ExamSessions.DeleteExamSessionStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Exam Session");
							this.RefreshPage();
							return;
						case ExamSessions.DeleteExamSessionStatuses.ChildRecordExistsExamDateAndSessions:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Exam Session");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		protected void btnAddExamSession_OnClick(object sender, EventArgs e)
		{
			this.ShowExamSessionModal(null);
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var examTypeEnum = this.ddlExamType.GetSelectedEnumValue<ExamConduct.ExamTypes>(null);
			if (semesterID == null || examTypeEnum == null)
				this.ddlExamConductID.Items.Clear();
			else
			{
				var examConducts = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamConducts.GetExamConductsList(semesterID.Value, examTypeEnum.Value, this.StaffIdentity.LoginSessionGuid);
				this.ddlExamConductID.DataBind(examConducts);
			}
		}

		private void ShowExamSessionModal(int? examSessionID)
		{
			if (this.ddlSemesterID.Items.Count == 0)
			{
				this.ddlSemesterID.FillSemesters();
				this.ddlExamType.FillExamConductExamTypes();
				this.ddlShift.FillShifts();
			}
			if (examSessionID != null)
			{
				var examSession = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamSessions.GetExamSession(examSessionID.Value, this.StaffIdentity.LoginSessionGuid);
				if (examSession == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Sessions>();
					return;
				}
				this.ddlSemesterID.SetSelectedValue(examSession.ExamConduct.SemesterID.ToString()).Enabled = false;
				this.ddlExamType.SetEnumValue(examSession.ExamConduct.ExamTypeEnum).Enabled = false;
				this.ddlExamType_OnSelectedIndexChanged(null, null);
				this.ddlExamConductID.SetSelectedValue(examSession.ExamConductID.ToString()).Enabled = false;
				this.tbSessionName.Text = examSession.SessionName;
				this.dtpStartTime.Text = examSession.StartTime.ToTime12Format();
				this.tbTotalTime.Text = examSession.TotalTimeInMinutes.ToString();
				this.tbPayRate.Text = examSession.InvigilationPayRate.ToString();
				this.ddlShift.SetEnumValue(examSession.ShiftEnum);
				this.modalAddExamSession.HeaderText = "Edit Exam Session";
				this.btnSave.Text = @"Save";
				this.ViewState["ExamSessionID"] = examSessionID;
			}
			else
			{
				this.ddlSemesterID.Enabled = true;
				this.ddlExamType.Enabled = true;
				this.ddlExamConductID.Enabled = true;
				this.ddlSemesterID.ClearSelection();
				this.ddlExamType.ClearSelection();
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);

				this.tbSessionName.Text = null;
				this.dtpStartTime.Text = null;
				this.tbTotalTime.Text = null;
				this.tbPayRate.Text = null;
				this.ddlShift.ClearSelection();
				this.modalAddExamSession.HeaderText = "Add Exam Session";
				this.btnSave.Text = @"Add";
				this.ViewState["ExamSessionID"] = null;
			}
			this.modalAddExamSession.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var examConductID = this.ddlExamConductID.SelectedValue.ToInt();
			var sessionName = this.tbSessionName.Text;
			var startTime = this.dtpStartTime.Text.ToDateTime().TimeOfDay;
			var totalTime = this.tbTotalTime.Text.ToByte();
			var payRate = this.tbPayRate.Text.ToShort();
			var shift = this.ddlShift.GetSelectedEnumValue<Shifts>();
			var examSessionID = (int?)this.ViewState["ExamSessionID"];
			if (examSessionID == null)
			{
				var status = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamSessions.AddExamSession(examConductID, sessionName, startTime, totalTime, payRate, shift, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case ExamSessions.AddExamSessionStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Exam session");
						this.RefreshPage();
						break;
					case ExamSessions.AddExamSessionStatuses.NameAlreadyExists:
						this.alertAddSession.AddErrorAlert("Session Name already exists in selected Exam Conduct.");
						this.updatePanelSession.Update();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var status = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamSessions.UpdateExamSession(examSessionID.Value, sessionName, startTime, totalTime, payRate, shift, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case ExamSessions.UpdateExamSessionStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						return;
					case ExamSessions.UpdateExamSessionStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Exam Session");
						this.RefreshPage();
						return;
					case ExamSessions.UpdateExamSessionStatuses.NameAlreadyExists:
						this.alertAddSession.AddErrorAlert("Session Name already exists in selected Exam Conduct.");
						this.updatePanelSession.Update();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
	}
}