﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{


	public partial class IndividualSeating
	{

		/// <summary>
		/// ddlSemesterID control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlSemesterID;

		/// <summary>
		/// ddlExamType control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlExamType;

		/// <summary>
		/// ddlExamConductID control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlExamConductID;

		/// <summary>
		/// tbEnrollment control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.EnrollmentTextBox tbEnrollment;

		/// <summary>
		/// gvNotSeatedRegisteredCourses control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireGridView gvNotSeatedRegisteredCourses;

		/// <summary>
		/// modalAssignSeat control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireModal modalAssignSeat;

		/// <summary>
		/// lblEnrollment control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.Label lblEnrollment;

		/// <summary>
		/// cbApplySameOnClassFellow control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireCheckBox cbApplySameOnClassFellow;

		/// <summary>
		/// gvFreeSeats control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireGridView gvFreeSeats;
	}
}
