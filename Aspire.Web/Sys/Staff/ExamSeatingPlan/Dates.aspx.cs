﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("ECBA0F35-E06B-4CD9-9D02-1C8CF95535A6", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Dates.aspx", "Dates", true, AspireModules.ExamSeatingPlan)]
	public partial class Dates : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.ManageExamSessions, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};
		public override PageIcon PageIcon => AspireGlyphicons.calendar.GetIcon();
		public override string PageTitle => "Exam Dates";

		public static string GetPageUrl(short? semesterID, ExamConduct.ExamTypes? examType, int? examConductID)
		{
			return GetAspirePageAttribute<ExamSeatingPlan.Dates>().PageUrl.AttachQueryParams("SemesterID", semesterID, "ExamType", examType, "ExamConductID", examConductID);
		}

		private void RefreshPage()
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var examType = this.ddlExamTypeFilter.GetSelectedEnumValue<ExamConduct.ExamTypes>(null);
			var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToNullableInt();
			this.Response.Redirect(GetPageUrl(semesterID, examType, examConductID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
				this.ddlExamTypeFilter.FillExamConductExamTypes().SetSelectedValueIfNotPostback("ExamType");
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamTypeFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamTypeFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var examTypeEnum = this.ddlExamTypeFilter.GetSelectedEnumValue<ExamConduct.ExamTypes>();
			if (semesterID == null)
				this.ddlExamConductIDFilter.Items.Clear();
			else
			{
				var examConducts = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamConducts.GetExamConductsList(semesterID.Value, examTypeEnum, this.StaffIdentity.LoginSessionGuid);
				this.ddlExamConductIDFilter.DataBind(examConducts).SetSelectedValueIfNotPostback("ExamConductID");
				this.ddlExamConductIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlExamConductIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetExamDates();
		}

		private void GetExamDates()
		{
			var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToNullableInt();
			if (examConductID == null)
			{
				this.gvExamDates.DataBindNull();
				return;
			}
			var examDates = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamDates.GetExamDates(examConductID.Value, this.StaffIdentity.LoginSessionGuid);
			this.gvExamDates.DataBind(examDates);
		}

		protected void gvExamDates_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					var examDateID = e.DecryptedCommandArgumentToInt();
					var status = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamDates.DeleteExamDate(examDateID, this.StaffIdentity.LoginSessionGuid);
					switch (status)
					{
						case BL.Core.ExamSeatingPlan.Staff.ExamDates.DeleteExamDateStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.RefreshPage();
							return;
						case BL.Core.ExamSeatingPlan.Staff.ExamDates.DeleteExamDateStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Exam Date");
							this.RefreshPage();
							return;
						case BL.Core.ExamSeatingPlan.Staff.ExamDates.DeleteExamDateStatuses.ChildRecordExistsExamDateAndSessions:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Exam Date");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		protected void btnAddExamDate_OnClick(object sender, EventArgs e)
		{
			this.ShowExamSessionModal();
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var examTypeEnum = this.ddlExamType.GetSelectedEnumValue<ExamConduct.ExamTypes>(null);
			if (semesterID == null || examTypeEnum == null)
				this.ddlExamConductID.Items.Clear();
			else
			{
				var examConducts = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamConducts.GetExamConductsList(semesterID.Value, examTypeEnum.Value, this.StaffIdentity.LoginSessionGuid);
				this.ddlExamConductID.DataBind(examConducts);
			}
		}

		private void ShowExamSessionModal()
		{
			if (this.ddlSemesterID.Items.Count == 0)
			{
				this.ddlSemesterID.FillSemesters();
				this.ddlExamType.FillExamConductExamTypes();
			}
			this.ddlSemesterID.Enabled = true;
			this.ddlExamType.Enabled = true;
			this.ddlExamConductID.Enabled = true;
			this.ddlSemesterID.ClearSelection();
			this.ddlExamType.ClearSelection();
			this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			this.dtpDate.SelectedDate = null;

			this.modalAddExamDate.HeaderText = "Add Exam Date";
			this.btnSave.Text = @"Add";

			this.modalAddExamDate.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var examConductID = this.ddlExamConductID.SelectedValue.ToInt();
			var date = this.dtpDate.SelectedDate.Value.Date;
			var status = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamDates.AddExamDate(examConductID, date, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.ExamSeatingPlan.Staff.ExamDates.AddExamDateStatuses.Success:
					this.AddSuccessMessageHasBeenAdded("Exam Date");
					this.RefreshPage();
					break;
				case BL.Core.ExamSeatingPlan.Staff.ExamDates.AddExamDateStatuses.NameAlreadyExists:
					this.alertAddSession.AddErrorAlert("Exam Date already exists in selected Exam Conduct.");
					this.updatePanelSession.Update();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}