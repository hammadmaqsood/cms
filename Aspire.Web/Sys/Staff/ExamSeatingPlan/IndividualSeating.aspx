﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="IndividualSeating.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.IndividualSeating" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Filter" ID="ddlSemesterID" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="ExamType:" AssociatedControlID="ddlExamType" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Filter" ID="ddlExamType" AutoPostBack="True" OnSelectedIndexChanged="ddlExamType_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Exam Conduct:" AssociatedControlID="ddlExamConductID" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Filter" ID="ddlExamConductID" AutoPostBack="True" OnSelectedIndexChanged="ddlExamConductID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
					<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" AllowNull="True" ValidationGroup="Filter" OnSearch="tbEnrollment_OnSearch" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView AllowSorting="True" AutoGenerateColumns="False" runat="server" ID="gvNotSeatedRegisteredCourses" OnPageIndexChanging="gvNotSeatedRegisteredCourses_PageIndexChanging" OnPageSizeChanging="gvNotSeatedRegisteredCourses_PageSizeChanging" OnSorting="gvNotSeatedRegisteredCourses_Sorting" OnRowCommand="gvNotSeatedRegisteredCourses_RowCommand">
		<Columns>
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CausesValidation="False" EncryptedCommandArgument='<%# Eval("RegisteredCourseID") + ":" + Eval("Enrollment") %>' CommandName="Assign" Text="Assign" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="Enrollment" SortExpression="Enrollment" HeaderText="Enrollment" />
			<asp:BoundField DataField="Name" SortExpression="Name" HeaderText="Name" />
			<asp:BoundField DataField="Title" SortExpression="Title" HeaderText="Title" />
			<asp:BoundField DataField="Class" SortExpression="Class" HeaderText="Class" />
			<asp:BoundField DataField="Date" SortExpression="Date" HeaderText="Date" DataFormatString="{0:d}" />
			<asp:BoundField DataField="SessionName" SortExpression="SessionName" HeaderText="Session" />
		</Columns>
	</aspire:AspireGridView>
	<aspire:AspireModal runat="server" ID="modalAssignSeat" HeaderText="Assign Seat">
		<BodyTemplate>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="lblEnrollment" />
				<aspire:Label runat="server" ID="lblEnrollment" />
			</div>
			<div class="form-group">
				<aspire:AspireCheckBox runat="server" ID="cbApplySameOnClassFellow" Text="Apply the same on the class fellows" Inline="true" />
			</div>
			<aspire:AspireGridView ItemType="Aspire.BL.Core.ExamSeatingPlan.Staff.IndividualSeating.FreeSeat" AllowSorting="True" runat="server" ID="gvFreeSeats" AutoGenerateColumns="False" OnSorting="gvFreeSeats_OnSorting" OnRowCommand="gvFreeSeats_OnRowCommand">
				<Columns>
					<asp:BoundField DataField="BuildingName" SortExpression="BuildingName" HeaderText="Building" />
					<asp:BoundField DataField="RoomName" SortExpression="RoomName" HeaderText="Room" />
					<asp:BoundField DataField="RoomUsageType" SortExpression="RoomUsageType" HeaderText="Type" />
					<asp:BoundField DataField="ClassFellows" SortExpression="ClassFellows" HeaderText="Class Fellows" />
					<asp:BoundField DataField="FreeSeats" SortExpression="FreeSeats" HeaderText="Free Seats" />
					<asp:TemplateField HeaderText="Action">
						<ItemTemplate>
							<aspire:AspireLinkButton runat="server" Text="Assign" CausesValidation="False" CommandName="Assign" EncryptedCommandArgument='<%# Item.ExamOfferedRoomID %>' />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</aspire:AspireGridView>
		</BodyTemplate>
		<FooterTemplate>
			<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
