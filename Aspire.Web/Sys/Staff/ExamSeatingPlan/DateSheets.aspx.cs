﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("03297F47-3782-4C72-A360-B16E8DAEE18D", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/DateSheets.aspx", "Date Sheet", true, AspireModules.ExamSeatingPlan)]
	public partial class DateSheets : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.calendar);
		public override string PageTitle => "Date Sheet";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
			}
		}
	}
}