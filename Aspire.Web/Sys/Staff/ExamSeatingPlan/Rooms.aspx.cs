﻿using Aspire.BL.Core.ExamSeatingPlan.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("2A1A06C2-F6E4-48B3-8B69-7B1F5F4942BD", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Rooms.aspx", "Rooms", true, AspireModules.ExamSeatingPlan)]
	public partial class Rooms : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};
		public override PageIcon PageIcon => FontAwesomeIcons.solid_table.GetIcon();
		public override string PageTitle => "Exam Rooms";

		public static string GetPageUrl(int? examBuildingID, ExamRoom.Statuses? status)
		{
			return typeof(ExamSeatingPlan.Rooms).GetAspirePageAttribute().PageUrl.AttachQueryParams("ExamBuildingID", examBuildingID, "Status", status);
		}

		public static void Redirect(int? examBuildingID, ExamRoom.Statuses? status)
		{
			Redirect(GetPageUrl(examBuildingID, status));
		}

		private void RefreshPage()
		{
			var examBuildingID = this.ddlExamBuildingIDFilter.SelectedValue.ToNullableInt();
			var status = this.ddlExamBuildingIDFilter.GetSelectedEnumValue<ExamRoom.Statuses>(null);
			Redirect(examBuildingID, status);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var examBuildings = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamBuildings.GetExamBuildingsList(this.StaffIdentity.LoginSessionGuid);
				this.ddlExamBuildingIDFilter.DataBind(examBuildings, CommonListItems.All).SetSelectedValueIfNotPostback("ExamBuildingID");
				this.ddlExamBuildingID.DataBind(examBuildings);

				this.ddlStatusFilter.FillExamRoomStatuses(CommonListItems.All).SetSelectedValueIfNotPostback("Status");
				this.rblStatus.FillExamRoomStatuses();
				this.ddlExamBuildingIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlExamBuildingIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlStatusFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlStatusFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetExamRooms();
		}

		private void GetExamRooms()
		{
			var examBuildingID = this.ddlExamBuildingIDFilter.SelectedValue.ToNullableInt();
			var statusEnum = this.ddlStatusFilter.GetSelectedEnumValue<ExamRoom.Statuses>(null);
			var examRooms = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamRooms.GetExamRooms(examBuildingID, statusEnum, this.StaffIdentity.LoginSessionGuid);
			var rooms = examRooms.GroupBy(er => er.ExamBuildingID).Select(g => new
			{
				ExamBuildingID = g.Key,
				ExamRooms = g.Where(er => er.ExamBuildingID == g.Key).ToList(),
			}).ToList();
			this.repeaterExamRooms.DataBind(rooms);
			var totalSeats = examRooms.Sum(r => r.TotalSeats);
			var activeTotalSeats = examRooms.Where(r => r.Status == ExamRoom.StatusActive).Sum(r => r.TotalSeats);
			var inactiveTotalSeats = examRooms.Where(r => r.Status == ExamRoom.StatusInactive).Sum(r => r.TotalSeats);
			this.lblActiveRoomsTotalSeats.Text = $"Total Seats: <strong>{totalSeats}</strong>, Active: <strong>{activeTotalSeats}</strong>, Inactive: <strong>{inactiveTotalSeats}</strong>";
		}

		protected void btnAddExamRoom_OnClick(object sender, EventArgs e)
		{
			this.ShowExamRoomsModal(null);
		}

		private void ShowExamRoomsModal(int? examRoomID)
		{
			if (examRoomID != null)
			{
				var examRoom = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamRooms.GetExamRoom(examRoomID.Value, this.StaffIdentity.LoginSessionGuid);
				if (examRoom == null)
				{
					this.ViewState["ExamRoomID"] = null;
					this.AddNoRecordFoundAlert();
					return;
				}
				this.ddlExamBuildingID.SetSelectedValue(examRoom.ExamBuildingID.ToString()).Enabled = false;
				this.tbRoomName.Text = examRoom.RoomName;
				this.tbRows.Text = examRoom.Rows.ToString();
				this.tbColumns.Text = examRoom.Columns.ToString();
				this.rblStatus.SetEnumValue(examRoom.StatusEnum);
				this.btnSave.Text = @"Save";
				this.modalExamRooms.HeaderText = @"Edit Exam Room";
				this.ViewState["ExamRoomID"] = examRoom.ExamRoomID;
			}
			else
			{
				this.ddlExamBuildingID.ClearSelection();
				this.ddlExamBuildingID.Enabled = true;
				this.tbRoomName.Text = null;
				this.tbRows.Text = null;
				this.tbColumns.Text = null;
				this.rblStatus.SelectedIndex = 0;
				this.btnSave.Text = @"Add";
				this.modalExamRooms.HeaderText = @"Add Exam Room";
				this.ViewState["ExamRoomID"] = null;
			}
			this.modalExamRooms.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var examBuildingID = this.ddlExamBuildingID.SelectedValue.ToInt();
			var roomName = this.tbRoomName.Text;
			var rows = this.tbRows.Text.ToByte();
			var columns = this.tbColumns.Text.ToByte();
			var statusEnum = this.rblStatus.GetSelectedEnumValue<ExamRoom.Statuses>();
			var examRoomID = (int?)this.ViewState["ExamRoomID"];
			if (examRoomID != null)
			{
				var status = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamRooms.UpdateExamRoom(examRoomID.Value, roomName, rows, columns, statusEnum, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case ExamRooms.UpdateExamRoomStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						break;
					case ExamRooms.UpdateExamRoomStatuses.NameAlreadyExists:
						this.alertRoom.AddErrorAlert("Name already exists.");
						this.updatePanelRooms.Update();
						return;
					case ExamRooms.UpdateExamRoomStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Exam Room");
						this.RefreshPage();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var status = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamRooms.AddExamRoom(examBuildingID, roomName, rows, columns, statusEnum, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case ExamRooms.AddExamRoomStatuses.NameAlreadyExists:
						this.alertRoom.AddErrorAlert("Name already exists.");
						break;
					case ExamRooms.AddExamRoomStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Exam Room");
						this.RefreshPage();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		protected void repeaterExamRooms_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Up":
				case "Down":
					ExamRooms.MoveExamRoomStatuses result;
					if (e.CommandName == "Up")
						result = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamRooms.MoveExamRoomUp(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					else
						result = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamRooms.MoveExamRoomDown(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					switch (result)
					{
						case ExamRooms.MoveExamRoomStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamRooms.MoveExamRoomStatuses.AlreadyAtTop:
							this.AddInfoAlert("Room is already at top.");
							break;
						case ExamRooms.MoveExamRoomStatuses.AlreadyAtBottom:
							this.AddInfoAlert("Room is already at bottom.");
							break;
						case ExamRooms.MoveExamRoomStatuses.Success:
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.RefreshPage();
					break;
				case "Edit":
					this.ShowExamRoomsModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					var status = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamRooms.DeleteExamRoom(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					switch (status)
					{
						case ExamRooms.DeleteExamRoomStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamRooms.DeleteExamRoomStatuses.CannotDeleteChildRecordExistsExamOfferedRooms:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Exam Room");
							break;
						case ExamRooms.DeleteExamRoomStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Exam Room");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.RefreshPage();
					break;
			}
		}

		protected void btnUpdateSequence_OnClick(object sender, EventArgs e)
		{
			var values = this.hfSequenceState.Value;
			var examRoomIDs = values.FromJsonString<string[]>().Select(s => s.Decrypt().ToInt()).ToList();
			var status = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamRooms.UpdateRoomsSequence(examRoomIDs, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case ExamRooms.UpdateRoomsSequenceStatuses.RoomsHaveBeenUpdatedInOtherSession:
					this.AddErrorAlert("Rooms have been updated meanwhile in other session. Try again.");
					break;
				case ExamRooms.UpdateRoomsSequenceStatuses.Success:
					this.AddSuccessAlert("Rooms' Sequence has been updated.");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.RefreshPage();
		}
	}
}
