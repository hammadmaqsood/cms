﻿using Aspire.BL.Common;
using Aspire.BL.Core.ExamSeatingPlan.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("54E24123-C4C7-44EC-B255-33F6115A361C", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/InvigilatorsAvailability.aspx", "Invigilators' Availability", true, AspireModules.ExamSeatingPlan)]
	public partial class InvigilatorsAvailability : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Invigilators' Availability";

		public static string GetPageUrl(short? semesterID, int? departmentID, ExamConduct.ExamTypes? examType, int? examConductID, string searchText)
		{
			return GetAspirePageAttribute<ExamSeatingPlan.InvigilatorsAvailability>().PageUrl.AttachQueryParams("SemesterID", semesterID, "DepartmentID", departmentID, "ExamType", (byte?)examType, "ExamConductID", examConductID, "SearchText", searchText);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Name", SortDirection.Ascending);
				this.ddlSemesterIDFilter.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All).SetSelectedValueIfNotPostback("DepartmentID");
				this.ddlExamTypeFilter.FillExamConductExamTypes().SetSelectedValueIfNotPostback("ExamType");
				this.tbSearch.Text = this.GetParameterValue("SearchText");
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		private void RefreshPage()
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var examType = this.ddlExamTypeFilter.GetSelectedEnumValue<ExamConduct.ExamTypes>(null);
			var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToNullableInt();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var searchText = this.tbSearch.Text;
			this.Response.Redirect(GetPageUrl(semesterID, departmentID, examType, examConductID, searchText));
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamTypeFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamTypeFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var examTypeEnum = this.ddlExamTypeFilter.GetSelectedEnumValue<ExamConduct.ExamTypes>();
			if (semesterID == null)
				this.ddlExamConductIDFilter.Items.Clear();
			else
			{
				var examConducts = BL.Core.ExamSeatingPlan.Staff.ExamConducts.GetExamConductsList(semesterID.Value, examTypeEnum, this.StaffIdentity.LoginSessionGuid);
				this.ddlExamConductIDFilter.DataBind(examConducts).SetSelectedValueIfNotPostback("ExamConductID");
			}
			this.ddlExamConductIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamConductIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		private ExamInvigilators.GetExamInvigilatorExamSessionsResult _invigilatorAndSessions;
		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData();
		}

		protected void gvInvigilatorsAvailability_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData();
		}

		protected void gvInvigilatorsAvailability_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow)
				return;
			var cblExamSessions = (AspireCheckBoxList)e.Row.FindControl("cblExamSessions");
			cblExamSessions.DataBind(this._invigilatorAndSessions.ExamSessions);
			var examInvigilatorExamSessions = (ExamInvigilators.ExamInvigilatorExamSessions)e.Row.DataItem;
			cblExamSessions.SetSelectedValues(examInvigilatorExamSessions.ExamSessionIDsList.Select(es => es.ToString()));
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var dictionary = new Dictionary<int, List<int>>();
			foreach (GridViewRow row in this.gvInvigilatorsAvailability.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow)
					continue;
				var cblExamSessions = (AspireCheckBoxList)row.FindControl("cblExamSessions");
				dictionary.Add((int)this.gvInvigilatorsAvailability.DataKeys[row.RowIndex].Value, cblExamSessions.GetSelectedValues().Select(i => i.ToInt()).ToList());
			}

			var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToInt();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var result = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamInvigilators.UpdateExamInvigilatorExamSessions(examConductID, departmentID, dictionary, this.StaffIdentity.LoginSessionGuid);
			switch (result)
			{
				case ExamInvigilators.UpdateExamInvigilatorExamSessionsStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					break;
				case ExamInvigilators.UpdateExamInvigilatorExamSessionsStatuses.Success:
					this.AddSuccessAlert("Record has been updated.");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.RefreshPage();
		}

		private void GetData()
		{
			var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToNullableInt();
			if (examConductID == null)
			{
				this.btnSaveUp.Enabled = this.btnSaveDown.Enabled =
					this.btnAssignAllDutiesUp.Enabled = this.btnRevokeAllDutiesUp.Enabled =
						this.btnAssignAllDutiesDown.Enabled = this.btnRevokeAllDutiesDown.Enabled = false;
				this.gvInvigilatorsAvailability.DataBindNull();
				return;
			}
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			this._invigilatorAndSessions = BL.Core.ExamSeatingPlan.Staff.ExamInvigilators.GetExamInvigilatorExamSessions(examConductID.Value, departmentID, this.StaffIdentity.LoginSessionGuid);

			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var result = this._invigilatorAndSessions.ExamInvigilatorAndSessions.AsEnumerable();
			switch (sortExpression)
			{
				case "Name":
					result = sortDirection == SortDirection.Ascending ? result.OrderBy(i => i.Name) : result.OrderByDescending(i => i.Name);
					break;
				case "DepartmentName":
					result = sortDirection == SortDirection.Ascending ? result.OrderBy(i => i.DepartmentName) : result.OrderByDescending(i => i.DepartmentName);
					break;
				case "Designation":
					result = sortDirection == SortDirection.Ascending ? result.OrderBy(i => i.Designation) : result.OrderByDescending(i => i.Designation);
					break;
				default:
					throw new SortingNotImplementedException(sortExpression);
			}
			this.gvInvigilatorsAvailability.DataBind(result.ToList(), sortExpression, sortDirection);
			this.btnSaveUp.Enabled = this.btnSaveDown.Enabled =
			this.btnAssignAllDutiesUp.Enabled = this.btnRevokeAllDutiesUp.Enabled =
				this.btnAssignAllDutiesDown.Enabled = this.btnRevokeAllDutiesDown.Enabled =
					this._invigilatorAndSessions.ExamInvigilatorAndSessions.Any();
		}

		protected void gvInvigilatorsAvailability_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "RevokeAll":
					var examInvigilatorID = e.DecryptedCommandArgumentToInt();
					var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToInt();
					var status = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamInvigilatorDuties.ClearAllAssignedDutiesOfInvigilator(examConductID, examInvigilatorID, this.StaffIdentity.LoginSessionGuid);
					switch (status)
					{
						case ExamInvigilatorDuties.ClearAllAssignedDutiesOfInvigilatorStatuses.Success:
							this.AddSuccessAlert("All exam invigilation duties have been unassigned from the selected invigilator.");
							break;
						case ExamInvigilatorDuties.ClearAllAssignedDutiesOfInvigilatorStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamInvigilatorDuties.ClearAllAssignedDutiesOfInvigilatorStatuses.NotAllDeletedAttendanceRecordExists:
							this.AddSuccessAlert("All exam invigilation duties have been unassigned from the selected invigilator in which attendance is not marked or invigilator is absent.");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.GetData();
					break;
				case "AssignAll":
					examInvigilatorID = e.DecryptedCommandArgumentToInt();
					examConductID = this.ddlExamConductIDFilter.SelectedValue.ToInt();
					var duties = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamInvigilatorDuties.AutoAssignAllDutiesToInvigilator(examConductID, examInvigilatorID, this.StaffIdentity.LoginSessionGuid);
					if (duties == 0)
						this.AddInfoAlert("No duty has been assigned.");
					else if (duties == 1)
						this.AddSuccessAlert("One duty has been assigned.");
					else if (duties > 1)
						this.AddSuccessAlert($"{duties} duties have been assigned.");
					this.GetData();
					break;
			}
		}

		protected void btnAssignAllDuties_OnClick(object sender, EventArgs e)
		{
			var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToInt();
			var duties = ExamInvigilatorDuties.AutoAssignAllDutiesToInvigilator(examConductID, this.StaffIdentity.LoginSessionGuid);
			switch (duties)
			{
				case 0:
					this.AddInfoAlert("No duty has been assigned.");
					break;
				case 1:
					this.AddSuccessAlert("One duty has been assigned.");
					break;
				default:
					this.AddSuccessAlert($"{duties} duties have been assigned.");
					break;
			}
			this.GetData();
		}

		protected void btnRevokeAllDuties_OnClick(object sender, EventArgs e)
		{
			var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToInt();
			var statuses = new List<ExamInvigilatorDuties.ClearAllAssignedDutiesOfInvigilatorStatuses>();
			foreach (DataKey dataKey in this.gvInvigilatorsAvailability.DataKeys)
				statuses.Add(Aspire.BL.Core.ExamSeatingPlan.Staff.ExamInvigilatorDuties.ClearAllAssignedDutiesOfInvigilator(examConductID, (int)dataKey.Value, this.StaffIdentity.LoginSessionGuid));
			if (statuses.All(s => s == ExamInvigilatorDuties.ClearAllAssignedDutiesOfInvigilatorStatuses.Success))
				this.AddSuccessAlert("All exam invigilation duties have been revoked from all the invigilators.");
			else if (statuses.Any(s => s == ExamInvigilatorDuties.ClearAllAssignedDutiesOfInvigilatorStatuses.NotAllDeletedAttendanceRecordExists))
				this.AddSuccessAlert("All exam invigilation duties have been revoked from the all the invigilator in which attendance is not marked or invigilator is absent.");
			this.GetData();
		}
	}
}