﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="OfferedRooms.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.OfferedRooms" %>

<asp:Content ContentPlaceHolderID="HeadPH" runat="server">
	<link href="../../../Scripts/Plugins/jquery-ui/jquery-ui.css" rel="stylesheet" />
	<style type="text/css">
		table.table.rooms td {
			vertical-align: middle !important;
		}
	</style>
</asp:Content>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:ScriptManagerProxy runat="server">
		<Scripts>
			<asp:ScriptReference Name="jquery-ui" />
		</Scripts>
	</asp:ScriptManagerProxy>
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
				<aspire:AspireDropDownList runat="server" CausesValidation="False" ID="ddlSemesterIDFilter" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Exam:" AssociatedControlID="ddlExamTypeFilter" />
				<aspire:AspireDropDownList runat="server" CausesValidation="False" ID="ddlExamTypeFilter" OnSelectedIndexChanged="ddlExamTypeFilter_OnSelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Exam Conduct:" AssociatedControlID="ddlExamConductIDFilter" />
				<aspire:AspireDropDownList runat="server" CausesValidation="False" ID="ddlExamConductIDFilter" OnSelectedIndexChanged="ddlExamConductIDFilter_OnSelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatusFilter" />
				<aspire:AspireDropDownList runat="server" CausesValidation="False" ID="ddlStatusFilter" OnSelectedIndexChanged="ddlStatusFilter_OnSelectedIndexChanged" AutoPostBack="True">
					<Items>
						<asp:ListItem Text="All" Value="" />
						<asp:ListItem Text="Offered Rooms Only" Value="1" />
						<asp:ListItem Text="Not Offered Rooms Only" Value="0" />
					</Items>
				</aspire:AspireDropDownList>
			</div>
		</div>
	</div>
	<asp:Repeater runat="server" ID="repeaterExamOfferedRooms" ItemType="Aspire.Web.Sys.Staff.ExamSeatingPlan.OfferedRooms.Rooms">
		<HeaderTemplate>
			<div class="table-responsive">
				<table class="table table-bordered table-hover table-striped rooms" id="<%# this.repeaterExamOfferedRooms.ClientID %>">
					<thead>
						<tr>
							<th></th>
							<th>#</th>
							<th>Semester</th>
							<th>Exam Type</th>
							<th>Exam Conduct</th>
							<th>Building Name</th>
							<th>Room Name</th>
							<th>Rows</th>
							<th>Columns</th>
							<th>Total Seats</th>
							<th>Type</th>
							<th>Action</th>
						</tr>
					</thead>
		</HeaderTemplate>
		<ItemTemplate>
			<tbody class="<%# Item.Offered ? "Offered" : "" %>">
				<asp:Repeater runat="server" ItemType="Aspire.BL.Core.ExamSeatingPlan.Staff.ExamOfferedRooms.ExamOfferedRoom" OnItemCommand="repeaterExamOfferedRooms_OnItemCommand" DataSource="<%# Item.RoomsList %>">
					<ItemTemplate>
						<tr>
							<td><span runat="server" visible='<%# Item.ExamOfferedRoomID != null %>' class="handle fas fa-ellipsis-v fa-2x" title="Drag to change sequence" style="cursor: move"></span></td>
							<td><%#: Container.ItemIndex + 1 %></td>
							<td><%#: Item.Semester %></td>
							<td><%#: Item.ExamTypeFullName %></td>
							<td><%#: Item.ExamConductName %></td>
							<td><%#: Item.BuildingName %></td>
							<td><%#: Item.RoomName %></td>
							<td>
								<%#: Item.ExamOfferedRoomID == null ? Item.Rows.ToString() : "" %>
								<div class="input-group input-group-sm" runat="server" visible='<%# Item.ExamOfferedRoomID != null %>'>
									<div class="input-group-btn">
										<aspire:AspireButton runat="server" CausesValidation="false" CommandName="Rows--" ConfirmMessage="Are you sure you want to remove one row?" EncryptedCommandArgument='<%# Item.ExamOfferedRoomID %>' Glyphicon="minus" />
									</div>
									<div class="form-control text-center">
										<%#: Item.Rows %>
									</div>
									<div class="input-group-btn">
										<aspire:AspireButton runat="server" CausesValidation="false" CommandName="Rows++" ConfirmMessage="Are you sure you want to add one row?" EncryptedCommandArgument='<%# Item.ExamOfferedRoomID %>' Glyphicon="plus" />
									</div>
								</div>
							</td>
							<td>
								<%#: Item.ExamOfferedRoomID == null ? Item.Columns.ToString() : "" %>
								<div class="input-group input-group-sm" runat="server" visible='<%# Item.ExamOfferedRoomID != null %>'>
									<div class="input-group-btn">
										<aspire:AspireButton runat="server" CausesValidation="false" CommandName="Columns--" ConfirmMessage="Are you sure you want to remove one column?" EncryptedCommandArgument='<%# Item.ExamOfferedRoomID %>' Glyphicon="minus" />
									</div>
									<div class="form-control text-center">
										<%#: Item.Columns %>
									</div>
									<div class="input-group-btn">
										<aspire:AspireButton runat="server" CausesValidation="false" CommandName="Columns++" ConfirmMessage="Are you sure you want to add one column?" EncryptedCommandArgument='<%# Item.ExamOfferedRoomID %>' Glyphicon="plus" />
									</div>
								</div>
							</td>
							<td><%#: Item.TotalSeats %></td>
							<td>
								<aspire:AspireLinkButton runat="server" Visible='<%# Item.ExamOfferedRoomID != null %>' Text='<%#: Item.RoomUsageTypeFullName %>' ConfirmMessage="Are you sure you want to change?" CausesValidation="False" CommandName="ToggleRoomUsageType" EncryptedCommandArgument='<%# Item.ExamOfferedRoomID %>' />
							</td>
							<td>
								<aspire:HiddenField runat="server" Visible='<%# Item.ExamOfferedRoomID != null %>' Mode="Encrypted" Value='<%# Item.ExamOfferedRoomID %>' />
								<aspire:AspireLinkButton runat="server" ToolTip="Add" Visible='<%# Item.ExamOfferedRoomID == null %>' CausesValidation="False" EncryptedCommandArgument='<%# Item.ExamRoomID %>' CommandName="Offer" Glyphicon="plus" />
								<aspire:AspireLinkButton runat="server" ToolTip="Delete" Visible='<%# Item.ExamOfferedRoomID != null %>' CausesValidation="False" EncryptedCommandArgument='<%# Item.ExamOfferedRoomID%>' CommandName="Delete" Glyphicon="remove" ConfirmMessage="Are you sure you want to delete?" />
								<aspire:AspireLinkButton runat="server" ToolTip="Clear All Seats" Visible='<%# Item.ExamOfferedRoomID != null %>' CausesValidation="False" EncryptedCommandArgument='<%# Item.ExamOfferedRoomID%>' CommandName="ClearAllSeats" CssClass="text-danger" FontAwesomeIcon="solid_user_times" ConfirmMessage='<%# $"Are you sure you want to clear all seats of {Item.RoomName}?" %>' />
							</td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
			</tbody>
		</ItemTemplate>
		<FooterTemplate>
			<tbody runat="server" visible="<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>">
				<tr>
					<td colspan="12">No record found.</td>
				</tr>
			</tbody>
			<tfoot runat="server" visible="<%# ((Repeater)Container.NamingContainer).Items.Count > 0 %>">
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th>
						<%#: this.TotalOfferedSeats %> / <%#: this.TotalSeats %>
					</th>
				</tr>
			</tfoot>
			</table>
			</div>
		</FooterTemplate>
	</asp:Repeater>
	<div class="text-center">
		<aspire:HiddenField runat="server" ID="hfSequenceState" Mode="PlainText" />
		<aspire:AspireButton runat="server" ButtonType="Primary" ID="btnUpdateSequence" Text="Update Sequence" CausesValidation="False" OnClick="btnUpdateSequence_OnClick" />
	</div>
	<script type="text/javascript">
		$(function () {
			var btnUpdateSequence = $("#<%=this.btnUpdateSequence.ClientID%>").hide();
			var table = $("#<%=this.repeaterExamOfferedRooms.ClientID%>");
			$("tbody.Offered", table).sortable({
				handle: ".handle",
				stop: function (event, ui) {
					$(btnUpdateSequence).show();
					var state = [];
					$("input:hidden", table).each(function (i, hf) {
						state.push($(hf).val());
					});
					$("#<%=this.hfSequenceState.ClientID%>").val(JSON.stringify(state));
				}
			});
		})
	</script>
</asp:Content>
