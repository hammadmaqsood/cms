﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Conducts.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.Conducts" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="btnAdd" Text="&nbsp;" />
				<div>
					<aspire:AspireButton runat="server" ID="btnAdd" ButtonType="Success" OnClick="btnAdd_OnClick" Text="Add Exam Conduct" Glyphicon="plus" />
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Exam Type:" AssociatedControlID="ddlExamTypeFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlExamTypeFilter" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlExamTypeFilter_OnSelectedIndexChanged" />
			</div>
		</div>
	</div>
	<aspire:AspireGridView runat="server" ID="gvExamConduct" ItemType="Aspire.Model.Entities.ExamConduct" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gvExamConduct_OnPageIndexChanging" OnPageSizeChanging="gvExamConduct_OnPageSizeChanging" OnSorting="gvExamConduct_OnSorting" OnRowCommand="gvExamConduct_OnRowCommand">
		<Columns>
			<asp:TemplateField HeaderText="Semester" SortExpression="SemesterID">
				<ItemTemplate>
					<%#: Item.SemesterID.ToSemesterString() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Exam Type" SortExpression="ExamType">
				<ItemTemplate>
					<%#: Item.ExamTypeEnum.ToFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Name" SortExpression="Name">
				<ItemTemplate>
					<%#: Item.Name %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Block Fee Defaulters" SortExpression="BlockFeeDefaulters">
				<ItemTemplate>
					<%#: Item.BlockFeeDefaulters.ToYesNo() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Visible To Students" SortExpression="VisibleToStudents">
				<ItemTemplate>
					<%#: Item.VisibleToStudents.ToYesNo() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Visible To Faculty Members" SortExpression="VisibleToFacultyMembers">
				<ItemTemplate>
					<%#: Item.VisibleToFacultyMembers.ToYesNo() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" ToolTip="Edit" CausesValidation="False" CommandName="Edit" EncryptedCommandArgument='<%# Item.ExamConductID %>' Glyphicon="edit" />
					<aspire:AspireLinkButton runat="server" ToolTip="Delete" CausesValidation="False" CommandName="Delete" EncryptedCommandArgument='<%# Item.ExamConductID %>' Glyphicon="remove" ConfirmMessage="Are you sure you want to delete this record?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalExamConduct">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updateModalExamConduct" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertModalExamConduct" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
						<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterID" ErrorMessage="This field is required." ValidationGroup="ExamConduct" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Exam Type:" AssociatedControlID="ddlExamType" />
						<aspire:AspireDropDownList runat="server" ID="ddlExamType" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamType" ErrorMessage="This field is required." ValidationGroup="ExamConduct" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbName" />
						<aspire:AspireTextBox runat="server" ID="tbName" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbName" ErrorMessage="This field is required." ValidationGroup="ExamConduct" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Block Fee Defaulters:" AssociatedControlID="rblBlockFeeDefaulters" />
						<div>
							<aspire:AspireRadioButtonList runat="server" ID="rblBlockFeeDefaulters" RepeatDirection="Horizontal" RepeatLayout="Flow" />
						</div>
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblBlockFeeDefaulters" ErrorMessage="This field is required." ValidationGroup="ExamConduct" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Visible To Students:" AssociatedControlID="rblVisibleToStudents" />
						<div>
							<aspire:AspireRadioButtonList runat="server" ID="rblVisibleToStudents" RepeatDirection="Horizontal" RepeatLayout="Flow" />
						</div>
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblVisibleToStudents" ErrorMessage="This field is required." ValidationGroup="ExamConduct" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Visible To Faculty Members:" AssociatedControlID="rblVisibleToFacultyMembers" />
						<div>
							<aspire:AspireRadioButtonList runat="server" ID="rblVisibleToFacultyMembers" RepeatDirection="Horizontal" RepeatLayout="Flow" />
						</div>
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblVisibleToFacultyMembers" ErrorMessage="This field is required." ValidationGroup="ExamConduct" />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" OnClick="btnSave_OnClick" ButtonType="Primary" ValidationGroup="ExamConduct" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
