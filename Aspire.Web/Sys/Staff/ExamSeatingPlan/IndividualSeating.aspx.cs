﻿using Aspire.BL.Common;
using Aspire.BL.Core.ExamSeatingPlan.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("A5620A2D-919F-4BA2-BB7D-907086A3F4CA", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/IndividualSeating.aspx", "Individual Seating", true, AspireModules.ExamSeatingPlan)]
	public partial class IndividualSeating : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.ExamSeatingPlan.Module, new [] {UserGroupPermission.PermissionValues.Allowed, }},
		};

		public override PageIcon PageIcon => FontAwesomeIcons.solid_user_plus.GetIcon();
		public override string PageTitle => "Individual Seating";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Enrollment", SortDirection.Ascending);
				this.ViewState.SetSortProperties1("ClassFellows", SortDirection.Descending);
				this.ddlSemesterID.FillSemesters();
				this.ddlExamType.FillExamConductExamTypes();
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var examTypeEnum = this.ddlExamType.GetSelectedEnumValue<ExamConduct.ExamTypes>();
			BL.Core.ExamSeatingPlan.Staff.ExamConducts.GetExamConductsList(semesterID, examTypeEnum, this.StaffIdentity.LoginSessionGuid)
				.DataBind(this.ddlExamConductID);
			this.ddlExamConductID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamConductID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.tbEnrollment_OnSearch(null, null);
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs args)
		{
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			this.ViewState["RegisteredCourseID"] = null;
			var examConductID = this.ddlExamConductID.SelectedValue.ToNullableInt();
			if (examConductID == null)
			{
				this.gvNotSeatedRegisteredCourses.DataBindNull();
				return;
			}
			var registeredCourses = Aspire.BL.Core.ExamSeatingPlan.Staff.IndividualSeating.GetNotSeatedRegisteredCourses(examConductID.Value, null, this.tbEnrollment.Enrollment, pageIndex, this.gvNotSeatedRegisteredCourses.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.StaffIdentity.LoginSessionGuid);
			this.gvNotSeatedRegisteredCourses.DataBind(registeredCourses.NotSeatedRegisteredCourses, pageIndex, registeredCourses.VirtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		protected void gvNotSeatedRegisteredCourses_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvNotSeatedRegisteredCourses_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvNotSeatedRegisteredCourses.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvNotSeatedRegisteredCourses_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvNotSeatedRegisteredCourses_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Assign":
					var args = e.DecryptedCommandArgument().Split(':');
					this.ViewState["RegisteredCourseID"] = args[0].ToInt();
					this.lblEnrollment.Text = args[1];
					this.cbApplySameOnClassFellow.Checked = false;
					this.GetFreeSeats();
					this.modalAssignSeat.Show();
					break;
			}
		}

		private void GetFreeSeats()
		{
			var registeredCourseID = this.ViewState["RegisteredCourseID"] as int?;
			if (registeredCourseID == null)
				throw new InvalidOperationException();
			var examConductID = this.ddlExamConductID.SelectedValue.ToInt();
			var freeSeats = Aspire.BL.Core.ExamSeatingPlan.Staff.IndividualSeating.GetFreeSeats(examConductID, registeredCourseID.Value, this.StaffIdentity.LoginSessionGuid);
			var sortExpression = this.ViewState.GetSortExpression1();
			var sortDirection = this.ViewState.GetSortDirection1();
			switch (sortExpression)
			{
				case "BuildingName":
					freeSeats = sortDirection == SortDirection.Ascending ? freeSeats.OrderBy(s => s.BuildingName).ToList() : freeSeats.OrderByDescending(s => s.BuildingName).ToList();
					break;
				case "RoomName":
					freeSeats = sortDirection == SortDirection.Ascending ? freeSeats.OrderBy(s => s.RoomName).ToList() : freeSeats.OrderByDescending(s => s.RoomName).ToList();
					break;
				case "ClassFellows":
					freeSeats = sortDirection == SortDirection.Ascending ? freeSeats.OrderBy(s => s.ClassFellows).ToList() : freeSeats.OrderByDescending(s => s.ClassFellows).ToList();
					break;
				case "RoomUsageType":
					freeSeats = sortDirection == SortDirection.Ascending ? freeSeats.OrderBy(s => s.RoomUsageType).ToList() : freeSeats.OrderByDescending(s => s.RoomUsageType).ToList();
					break;
				case "FreeSeats":
					freeSeats = sortDirection == SortDirection.Ascending ? freeSeats.OrderBy(s => s.FreeSeats).ToList() : freeSeats.OrderByDescending(s => s.FreeSeats).ToList();
					break;
				default:
					throw new SortingNotImplementedException(sortExpression);

			}
			this.gvFreeSeats.DataBind(freeSeats, sortExpression, sortDirection);
		}

		protected void gvFreeSeats_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs1(this.ViewState);
			this.GetFreeSeats();
		}

		protected void gvFreeSeats_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Assign":
					var registeredCourseID = (int)this.ViewState["RegisteredCourseID"];
					var examOfferedRoomID = e.DecryptedCommandArgumentToInt();
					var applySameOnClassFellow = this.cbApplySameOnClassFellow.Checked;
					var result = ExecuteSeatingPlan.IndividualSeat(registeredCourseID, examOfferedRoomID, this.StaffIdentity.LoginSessionGuid);
					if (result == null)
						this.AddNoRecordFoundAlert();
					else
						switch (result.Status)
						{
							case ExecuteSeatingPlan.IndividualSeatResult.Statuses.NoRecordFound:
								this.AddNoRecordFoundAlert();
								break;
							case ExecuteSeatingPlan.IndividualSeatResult.Statuses.SeatAlreadyAllocated:
								this.AddWarningAlert($"Seat has already been allocated. Enrollment: {result.Enrollment}, Offered Course Title: {result.OfferedTitle}.");
								break;
							case ExecuteSeatingPlan.IndividualSeatResult.Statuses.SeatCannotBeAllocatedInTheSelectedRoom:
								this.AddErrorAlert($"Seat cannot be allocated in the selected room. Enrollment: {result.Enrollment}, Offered Course Title: {result.OfferedTitle}.");
								break;
							case ExecuteSeatingPlan.IndividualSeatResult.Statuses.FreeSeatsNotFound:
								this.AddErrorAlert($"Free Seats are not available in selected room. Enrollment: {result.Enrollment}, Offered Course Title: {result.OfferedTitle}.");
								break;
							case ExecuteSeatingPlan.IndividualSeatResult.Statuses.Success:
								this.AddSuccessAlert($"Seat has been allocated. Enrollment: {result.Enrollment}, Offered Course Title: {result.OfferedTitle}.");
								if (applySameOnClassFellow)
								{
									var examConductID = this.ddlExamConductID.SelectedValue.ToInt();
									var registeredCoursesClassFellows = Aspire.BL.Core.ExamSeatingPlan.Staff.IndividualSeating.GetNotSeatedRegisteredCourses(examConductID, result.OfferedCourseID ?? throw new InvalidOperationException(), null, 0, 100, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.StaffIdentity.LoginSessionGuid);
									var breakLoop = true;
									foreach (var registeredCourse in registeredCoursesClassFellows.NotSeatedRegisteredCourses)
									{
										result = ExecuteSeatingPlan.IndividualSeat(registeredCourse.RegisteredCourseID, examOfferedRoomID, this.StaffIdentity.LoginSessionGuid);
										if (result == null)
											this.AddNoRecordFoundAlert();
										else
											switch (result.Status)
											{
												case ExecuteSeatingPlan.IndividualSeatResult.Statuses.NoRecordFound:
													this.AddNoRecordFoundAlert();
													break;
												case ExecuteSeatingPlan.IndividualSeatResult.Statuses.SeatAlreadyAllocated:
													this.AddWarningAlert($"Seat has already been allocated. Enrollment: {result.Enrollment}, Offered Course Title: {result.OfferedTitle}.");
													break;
												case ExecuteSeatingPlan.IndividualSeatResult.Statuses.SeatCannotBeAllocatedInTheSelectedRoom:
													this.AddErrorAlert($"Seat cannot be allocated in the selected room. Enrollment: {result.Enrollment}, Offered Course Title: {result.OfferedTitle}.");
													break;
												case ExecuteSeatingPlan.IndividualSeatResult.Statuses.FreeSeatsNotFound:
													this.AddErrorAlert($"Free Seats are not available in selected room. Enrollment: {result.Enrollment}, Offered Course Title: {result.OfferedTitle}.");
													break;
												case ExecuteSeatingPlan.IndividualSeatResult.Statuses.Success:
													this.AddSuccessAlert($"Seat has been allocated. Enrollment: {result.Enrollment}, Offered Course Title: {result.OfferedTitle}.");
													breakLoop = false;
													break;
												default:
													throw new NotImplementedEnumException(result.Status);
											}
										if (breakLoop)
											break;
									}
								}
								break;
							default:
								throw new NotImplementedEnumException(result.Status);
						}
					this.modalAssignSeat.Hide();
					this.GetData(0);
					break;
			}
		}
	}
}