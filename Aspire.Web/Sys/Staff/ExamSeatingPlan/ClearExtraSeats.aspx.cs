﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("5360BB69-E392-4645-AE7A-2B428E19DE5B", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/ClearExtraSeats.aspx", "Clear Extra Seats", true, AspireModules.ExamSeatingPlan)]
	public partial class ClearExtraSeats : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.ExamSeatingPlan.Module, new [] {UserGroupPermission.PermissionValues.Allowed, }},
		};
		public override PageIcon PageIcon => AspireGlyphicons.erase.GetIcon();
		public override string PageTitle => "Clear Extra Seats";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Enrollment", SortDirection.Ascending);
				this.ddlSemesterID.FillSemesters();
				this.ddlExamType.FillExamConductExamTypes();
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var examTypeEnum = this.ddlExamType.GetSelectedEnumValue<ExamConduct.ExamTypes>();
			BL.Core.ExamSeatingPlan.Staff.ExamConducts.GetExamConductsList(semesterID, examTypeEnum, this.StaffIdentity.LoginSessionGuid)
				.DataBind(this.ddlExamConductID);
			this.ddlExamConductID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamConductID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			var examConductID = this.ddlExamConductID.SelectedValue.ToNullableInt();
			if (examConductID == null)
			{
				this.gv.DataBindNull();
				this.btnClear.Visible = false;
				return;
			}
			var result = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamSeats.GetExtraSeats(examConductID.Value, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), pageIndex, this.gv.PageSize, this.StaffIdentity.LoginSessionGuid);
			this.gv.DataBind(result.ExtraExamSeats, pageIndex, result.VirtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
			this.btnClear.Visible = result.ExtraExamSeats.Any();
		}

		protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gv_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gv.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gv_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void btnClear_OnClick(object sender, EventArgs e)
		{
			var examConductID = this.ddlExamConductID.SelectedValue.ToNullableInt();
			if (examConductID != null)
			{
				var result = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamSeats.ClearExtraSeats(examConductID.Value, this.StaffIdentity.LoginSessionGuid);
				switch (result)
				{
					case Aspire.BL.Core.ExamSeatingPlan.Staff.ExamSeats.ClearExtraSeatsResult.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.GetData(0);
						break;
					case Aspire.BL.Core.ExamSeatingPlan.Staff.ExamSeats.ClearExtraSeatsResult.Success:
						this.AddSuccessAlert("Extra seats have been cleared.");
						this.GetData(0);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
	}
}