﻿<%@ Page MasterPageFile="~/Base.Master" Language="C#" AutoEventWireup="true" CodeBehind="Dates.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.Dates" %>

<asp:Content runat="server" ContentPlaceHolderID="BodyPH">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireButton runat="server" CausesValidation="False" ID="btnAddExamDate" Text="Add Date" Glyphicon="plus" OnClick="btnAddExamDate_OnClick" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
					<aspire:AspireDropDownList runat="server" CausesValidation="False" ID="ddlSemesterIDFilter" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" AutoPostBack="True" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Exam:" AssociatedControlID="ddlExamTypeFilter" />
					<aspire:AspireDropDownList runat="server" CausesValidation="False" ID="ddlExamTypeFilter" OnSelectedIndexChanged="ddlExamTypeFilter_OnSelectedIndexChanged" AutoPostBack="True" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Exam Conduct:" AssociatedControlID="ddlExamConductIDFilter" />
					<aspire:AspireDropDownList runat="server" CausesValidation="False" ID="ddlExamConductIDFilter" OnSelectedIndexChanged="ddlExamConductIDFilter_OnSelectedIndexChanged" AutoPostBack="True" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvExamDates" AutoGenerateColumns="False" OnRowCommand="gvExamDates_OnRowCommand">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%#: Container.DataItemIndex+1%></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Semester" DataField="Semester" />
			<asp:BoundField HeaderText="Exam Type" DataField="ExamTypeFullName" />
			<asp:BoundField HeaderText="Exam Conduct" DataField="ExamConductName" />
			<asp:BoundField HeaderText="Exam Date" DataField="Date" DataFormatString="{0:D}" />
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%# Eval("ExamDateID") %>' ConfirmMessage="Are you sure you want to delete this exam date?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalAddExamDate" HeaderText="Add Exam Date">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelSession" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertAddSession" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" CausesValidation="False" ValidationGroup="Session" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterID" ErrorMessage="This field is required." ValidationGroup="Session" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Exam Type:" AssociatedControlID="ddlExamType" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlExamType" CausesValidation="False" ValidationGroup="Session" AutoPostBack="True" OnSelectedIndexChanged="ddlExamType_OnSelectedIndexChanged" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamType" ErrorMessage="This field is required." ValidationGroup="Session" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Exam Conduct:" AssociatedControlID="ddlExamConductID" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlExamConductID" ValidationGroup="Session" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamConductID" ErrorMessage="This field is required." ValidationGroup="Session" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Exam Date:" AssociatedControlID="dtpDate" />
							<div class="col-md-9">
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpDate" ValidationGroup="Session" DisplayMode="ShortDate" />
								<aspire:AspireDateTimeValidator runat="server" Format="ShortDate" AllowNull="False" ControlToValidate="dtpDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." ValidationGroup="Session" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ButtonType="Primary" Text="Add" ID="btnSave" ValidationGroup="Session" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server" CausesValidation="False">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
