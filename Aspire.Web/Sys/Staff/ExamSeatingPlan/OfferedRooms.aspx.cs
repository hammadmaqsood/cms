﻿using Aspire.BL.Core.ExamSeatingPlan.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("CBBD6230-4DFC-41FF-8D6E-2779D32AA25E", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/OfferedRooms.aspx", "Offered Rooms", true, AspireModules.ExamSeatingPlan)]
	public partial class OfferedRooms : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};
		public override PageIcon PageIcon => FontAwesomeIcons.solid_table.GetIcon();
		public override string PageTitle => "Offered Rooms";

		public static string GetPageUrl(short? semesterID, ExamConduct.ExamTypes? examType, int? examConductID, int? status)
		{
			return GetAspirePageAttribute<ExamSeatingPlan.OfferedRooms>().PageUrl.AttachQueryParams("SemesterID", semesterID, "ExamType", (byte?)examType, "ExamConductID", examConductID, "Status", status);
		}

		private void RefreshPage()
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var examType = this.ddlExamTypeFilter.GetSelectedEnumValue<ExamConduct.ExamTypes>(null);
			var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToNullableInt();
			var status = this.ddlStatusFilter.SelectedValue.ToNullableInt();
			this.Response.Redirect(GetPageUrl(semesterID, examType, examConductID, status));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
				this.ddlExamTypeFilter.FillExamConductExamTypes().SetSelectedValueIfNotPostback("ExamType");
				this.ddlStatusFilter.SetSelectedValueIfNotPostback("Status");
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamTypeFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamTypeFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var examTypeEnum = this.ddlExamTypeFilter.GetSelectedEnumValue<ExamConduct.ExamTypes>(null);
			if (semesterID == null || examTypeEnum == null)
				this.ddlExamConductIDFilter.Items.Clear();
			else
			{
				var examConducts = BL.Core.ExamSeatingPlan.Staff.ExamConducts.GetExamConductsList(semesterID.Value, examTypeEnum.Value, this.StaffIdentity.LoginSessionGuid);
				this.ddlExamConductIDFilter.DataBind(examConducts).SetSelectedValueIfNotPostback("ExamConductID");
			}
			this.ddlExamConductIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamConductIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlStatusFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlStatusFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetOfferedRooms();
		}

		protected int TotalSeats { get; private set; }
		protected int TotalOfferedSeats { get; private set; }

		protected sealed class Rooms
		{
			public bool Offered { get; set; }
			public List<Aspire.BL.Core.ExamSeatingPlan.Staff.ExamOfferedRooms.ExamOfferedRoom> RoomsList { get; set; }
		}

		private void GetOfferedRooms()
		{
			var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToNullableInt();
			if (examConductID == null)
			{
				this.repeaterExamOfferedRooms.DataBind(new Rooms[] { });
				return;
			}
			var includeOffered = this.ddlStatusFilter.SelectedValue == "" || this.ddlStatusFilter.SelectedValue == "1";
			var includeNotOffered = this.ddlStatusFilter.SelectedValue == "" || this.ddlStatusFilter.SelectedValue == "0";
			var examOfferedRooms = ExamOfferedRooms.GetExamOfferedRooms(examConductID.Value, includeOffered, includeNotOffered, this.StaffIdentity.LoginSessionGuid);
			var rooms = examOfferedRooms?.GroupBy(r => new { Offered = r.ExamOfferedRoomID != null })
				.Select(g => new Rooms
				{
					Offered = g.Key.Offered,
					RoomsList = g.ToList()
				}).ToList();
			this.TotalOfferedSeats = examOfferedRooms?.Where(r => r.ExamOfferedRoomID != null).Sum(r => (int?)r.TotalSeats) ?? 0;
			this.TotalSeats = examOfferedRooms?.Sum(r => (int?)r.TotalSeats) ?? 0;
			this.repeaterExamOfferedRooms.DataBind(rooms);
		}

		protected void repeaterExamOfferedRooms_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			var examOfferedRoomID = e.DecryptedCommandArgumentToInt();
			switch (e.CommandName)
			{
				case "Offer":
					var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToNullableInt();
					if (examConductID == null)
						this.AddNoRecordFoundAlert();
					else
					{
						var offerStatus = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamOfferedRooms.AddExamOfferedRoom(examConductID.Value, e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
						switch (offerStatus)
						{
							case ExamOfferedRooms.AddExamOfferedRoomStatuses.AlreadyExists:
								this.AddErrorAlert("Room has already been offered.");
								break;
							case ExamOfferedRooms.AddExamOfferedRoomStatuses.Success:
								this.AddSuccessAlert("Room has been offered.");
								break;
							case ExamOfferedRooms.AddExamOfferedRoomStatuses.NoRecordFoundExamRoom:
								this.AddErrorAlert("Room doesn't exists.");
								break;
							case ExamOfferedRooms.AddExamOfferedRoomStatuses.NoRecordFoundExamConduct:
								this.AddErrorAlert("Exam Conduct record doesn't exists.");
								break;
							default:
								throw new NotImplementedEnumException(offerStatus);
						}
					}
					this.RefreshPage();
					break;
				case "Delete":
					var deleteStatus = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamOfferedRooms.DeleteExamOfferedRoom(examOfferedRoomID, this.StaffIdentity.LoginSessionGuid);
					switch (deleteStatus)
					{
						case ExamOfferedRooms.DeleteExamOfferedRoomStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamOfferedRooms.DeleteExamOfferedRoomStatuses.CannotDeleteChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Offered Room");
							break;
						case ExamOfferedRooms.DeleteExamOfferedRoomStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Offered Room");
							break;
						default:
							throw new NotImplementedEnumException(deleteStatus);
					}
					break;
				case "ClearAllSeats":
					var clearAllSeatsStatus = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamOfferedRooms.ClearAllSeatsOfExamOfferedRoom(examOfferedRoomID, this.StaffIdentity.LoginSessionGuid);
					switch (clearAllSeatsStatus)
					{
						case ExamOfferedRooms.ClearAllSeatsOfExamOfferedRoomStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamOfferedRooms.ClearAllSeatsOfExamOfferedRoomStatuses.Success:
							this.AddSuccessAlert("Room Seats have been cleared.");
							break;
						default:
							throw new NotImplementedEnumException(clearAllSeatsStatus);
					}
					break;
				case "ToggleRoomUsageType":
					var toggleStatus = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamOfferedRooms.ToggleRoomUsageType(examOfferedRoomID, this.StaffIdentity.LoginSessionGuid);
					switch (toggleStatus)
					{
						case ExamOfferedRooms.ToggleStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamOfferedRooms.ToggleStatuses.CannotUpdateChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Offered Room");
							break;
						case ExamOfferedRooms.ToggleStatuses.Success:
							this.AddSuccessAlert("Offered Room's Type has been changed.");
							break;
						default:
							throw new NotImplementedEnumException(toggleStatus);
					}
					break;
				case "Rows--":
					var decrementStatus = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamOfferedRooms.DecrementRows(examOfferedRoomID, this.StaffIdentity.LoginSessionGuid);
					switch (decrementStatus)
					{
						case ExamOfferedRooms.DecrementStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamOfferedRooms.DecrementStatuses.CannotUpdateChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Offered Room");
							break;
						case ExamOfferedRooms.DecrementStatuses.CannotUpdateRowOrColumnCannotBeLessThanOne:
							this.AddErrorAlert("Rows must be at least one.");
							break;
						case ExamOfferedRooms.DecrementStatuses.Success:
							this.AddSuccessMessageHasBeenUpdated("Offered room's rows", true);
							break;
						default:
							throw new NotImplementedEnumException(decrementStatus);
					}
					break;
				case "Rows++":
					var incrementStatus = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamOfferedRooms.IncrementRows(examOfferedRoomID, this.StaffIdentity.LoginSessionGuid);
					switch (incrementStatus)
					{
						case ExamOfferedRooms.IncrementStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamOfferedRooms.IncrementStatuses.CannotUpdateChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Offered Room");
							break;
						case ExamOfferedRooms.IncrementStatuses.CannotUpdateRowOrColumnCannotBeGreaterThenMax:
							this.AddErrorAlert("Rows cannot be greater than 255.");
							break;
						case ExamOfferedRooms.IncrementStatuses.Success:
							this.AddSuccessMessageHasBeenUpdated("Offered room's rows", true);
							break;
						default:
							throw new NotImplementedEnumException(incrementStatus);
					}
					break;
				case "Columns--":
					decrementStatus = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamOfferedRooms.DecrementColumns(examOfferedRoomID, this.StaffIdentity.LoginSessionGuid);
					switch (decrementStatus)
					{
						case ExamOfferedRooms.DecrementStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamOfferedRooms.DecrementStatuses.CannotUpdateChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Offered Room");
							break;
						case ExamOfferedRooms.DecrementStatuses.CannotUpdateRowOrColumnCannotBeLessThanOne:
							this.AddErrorAlert("Columns must be at least one.");
							break;
						case ExamOfferedRooms.DecrementStatuses.Success:
							this.AddSuccessMessageHasBeenUpdated("Offered room's columns", true);
							break;
						default:
							throw new NotImplementedEnumException(decrementStatus);
					}
					break;
				case "Columns++":
					incrementStatus = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamOfferedRooms.IncrementColumns(examOfferedRoomID, this.StaffIdentity.LoginSessionGuid);
					switch (incrementStatus)
					{
						case ExamOfferedRooms.IncrementStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamOfferedRooms.IncrementStatuses.CannotUpdateChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Offered Room");
							break;
						case ExamOfferedRooms.IncrementStatuses.CannotUpdateRowOrColumnCannotBeGreaterThenMax:
							this.AddErrorAlert("Columns cannot be greater than 255.");
							break;
						case ExamOfferedRooms.IncrementStatuses.Success:
							this.AddSuccessMessageHasBeenUpdated("Offered room's columns", true);
							break;
						default:
							throw new NotImplementedEnumException(incrementStatus);
					}
					break;
			}
			this.RefreshPage();
		}

		protected void btnUpdateSequence_OnClick(object sender, EventArgs e)
		{
			if (this.ddlStatusFilter.SelectedValue != "" && this.ddlStatusFilter.SelectedValue != "1")
				throw new InvalidOperationException();
			var values = this.hfSequenceState.Value;
			var examOfferedRoomIDs = values.FromJsonString<string[]>().Select(s => s.Decrypt().ToInt()).ToList();
			var status = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamOfferedRooms.UpdateRoomsSequence(examOfferedRoomIDs, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case ExamOfferedRooms.UpdateRoomsSequenceStatuses.RoomsHaveBeenUpdatedInOtherSession:
					this.AddErrorAlert("Rooms have been updated meanwhile in other session. Try again.");
					break;
				case ExamOfferedRooms.UpdateRoomsSequenceStatuses.Success:
					this.AddSuccessAlert("Rooms' Sequence has been updated.");
					break;
				default:
					throw new NotImplementedEnumException(status);
			}
			this.RefreshPage();
		}
	}
}