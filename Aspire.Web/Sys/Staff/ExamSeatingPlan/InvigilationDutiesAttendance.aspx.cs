﻿using Aspire.BL.Core.ExamSeatingPlan.Staff;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("C229BB07-E79A-472A-B3FC-3910D8EA0123", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/InvigilationDutiesAttendance.aspx", "Invigilation Duties Attendance", true, AspireModules.ExamSeatingPlan)]
	public partial class InvigilationDutiesAttendance : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Invigilation Duties Attendance";

		protected static readonly List<CustomListItem> AttendanceStatuses = Enum.GetValues(typeof(ExamInvigilatorDuty.AttendanceStatuses)).Cast<ExamInvigilatorDuty.AttendanceStatuses>().Select(i => new CustomListItem
		{
			Text = i.ToFullName(),
			ID = (byte)i,
		}).ToList();

		public static string GetPageUrl(short? semesterID, ExamConduct.ExamTypes? examType, int? examConductID, int? examDateAndSessionID)
		{
			return GetAspirePageAttribute<ExamSeatingPlan.InvigilationDutiesAttendance>().PageUrl.AttachQueryParams("SemesterID", semesterID, "ExamType", (byte?)examType, "ExamConductID", examConductID, "ExamDateAndSessionID", examDateAndSessionID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
				this.ddlExamTypeFilter.FillExamConductExamTypes().SetSelectedValueIfNotPostback("ExamType");
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamTypeFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamTypeFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var examTypeEnum = this.ddlExamTypeFilter.GetSelectedEnumValue<ExamConduct.ExamTypes>();
			if (semesterID == null)
				this.ddlExamConductIDFilter.Items.Clear();
			else
			{
				var examConducts = BL.Core.ExamSeatingPlan.Staff.ExamConducts.GetExamConductsList(semesterID.Value, examTypeEnum, this.StaffIdentity.LoginSessionGuid);
				this.ddlExamConductIDFilter.DataBind(examConducts).SetSelectedValueIfNotPostback("ExamConductID");
			}
			this.ddlExamConductIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamConductIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToNullableInt();
			if (examConductID == null)
				this.ddlExamDateAndSessionIDFilter.Items.Clear();
			else
			{
				var examDateAndSessions = BL.Core.ExamSeatingPlan.Staff.ExamDateAndSessions.GetExamDateAndSessionsList(examConductID.Value, this.StaffIdentity.LoginSessionGuid);
				this.ddlExamDateAndSessionIDFilter.DataBind(examDateAndSessions).SetSelectedValueIfNotPostback("ExamDateAndSessionID");
			}
			this.ddlExamDateAndSessionIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamDateAndSessionIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData();
		}

		private void GetData()
		{
			var examDateAndSessionID = this.ddlExamDateAndSessionIDFilter.SelectedValue.ToNullableInt();
			if (examDateAndSessionID == null)
			{
				this.gvExamInvigilatorDuties.DataBindNull();
				this.btnSave.Visible = false;
			}
			else
			{
				var invigilatorsDuties = BL.Core.ExamSeatingPlan.Staff.ExamInvigilatorDuties.GetExamInvigilatorDutiesForAttendance(examDateAndSessionID.Value, this.StaffIdentity.LoginSessionGuid);
				this.gvExamInvigilatorDuties.DataBind(invigilatorsDuties);
				this.btnSave.Visible = invigilatorsDuties.Any();
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var attedance = new Dictionary<int, ExamInvigilatorDuty.AttendanceStatuses>();
			foreach (GridViewRow row in this.gvExamInvigilatorDuties.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow)
					continue;
				var examInvigilatorDutyID = (int)this.gvExamInvigilatorDuties.DataKeys[row.RowIndex].Value;
				var rblAttendance = (AspireRadioButtonList)row.FindControl("rblAttendance");
				var attendanceStatus = rblAttendance.GetSelectedEnumValue<ExamInvigilatorDuty.AttendanceStatuses>();
				attedance.Add(examInvigilatorDutyID, attendanceStatus);
			}
			if (attedance.Any())
			{
				var examDateAndSessionID = this.ddlExamDateAndSessionIDFilter.SelectedValue.ToInt();
				var status = BL.Core.ExamSeatingPlan.Staff.ExamInvigilatorDuties.MarkExamInvigilatorDutiesAttendance(examDateAndSessionID, attedance, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case ExamInvigilatorDuties.MarkExamInvigilatorDutiesAttendanceStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.GetData();
						return;
					case ExamInvigilatorDuties.MarkExamInvigilatorDutiesAttendanceStatuses.Success:
						this.AddSuccessAlert("Attendance has been marked.");
						this.GetData();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
	}
}