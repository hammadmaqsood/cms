﻿using Aspire.BL.Core.ExamSeatingPlan.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("6E092205-7766-4243-BA16-0862F843D0DA", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/DeleteSeats.aspx", "Delete Seats", true, AspireModules.ExamSeatingPlan)]
	public partial class DeleteSeats : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module, UserGroupPermission.Allowed}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.remove);
		public override string PageTitle => "Delete Seats";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterID.FillSemesters();
				this.ddlExamType.FillEnums<ExamConduct.ExamTypes>();
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);

				var studentID = this.Request.GetParameterValue<int>("StudentID");
				if (studentID != null)
					this.tbEnrollment.Enrollment = Aspire.BL.Common.Common.GetEnrollmentFromStudentID(studentID.Value);
				else
					this.tbEnrollment.Enrollment = this.Request.GetParameterValue("Enrollment");
				if (!string.IsNullOrWhiteSpace(this.tbEnrollment.Enrollment))
					this.tbEnrollment_OnSearch(null, null);
			}

		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamType_OnSelectedIndexChanged(null, null);
		}

		public short? SemesterID => this.ddlSemesterID.SelectedValue.ToNullableShort();
		public ExamConduct.ExamTypes ExamTypeEnum => this.ddlExamType.GetSelectedEnumValue<ExamConduct.ExamTypes>();
		public int? ExamConductID => this.ddlExamConductID.SelectedValue.ToNullableInt();

		protected void ddlExamType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.SemesterID == null)
				this.ddlExamConductID.Items.Clear();
			else
			{
				var examConducts = BL.Core.ExamSeatingPlan.Staff.ExamConducts.GetExamConductsList(this.SemesterID.Value, this.ExamTypeEnum, this.StaffIdentity.LoginSessionGuid);
				this.ddlExamConductID.DataBind(examConducts);
				this.tbEnrollment_OnSearch(null, null);
			}
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs args)
		{
			var enrollment = this.tbEnrollment.Enrollment;
			if (string.IsNullOrWhiteSpace(enrollment) || this.SemesterID == null)
			{
				this.panelDeleteSeats.Visible = false;
				return;
			}

			var studentExamSeatInfo = BL.Core.ExamSeatingPlan.Staff.DeleteExamSeats.GetStudentExamSeatInfo(enrollment, this.SemesterID.Value, this.ExamConductID, this.StaffIdentity.LoginSessionGuid);

			if (studentExamSeatInfo == null)
			{
				this.panelDeleteSeats.Visible = false;
				this.AddNoRecordFoundAlert();
				return;
			}
			this.ViewState["StudentID"] = studentExamSeatInfo.StudentInfo.StudentID;
			this.hlEnrollment.Text = studentExamSeatInfo.StudentInfo.Enrollment;
			this.lblRegistrationNo.Text = studentExamSeatInfo.StudentInfo.RegistrationNo.ToString().ToNAIfNullOrEmpty();
			this.lblName.Text = studentExamSeatInfo.StudentInfo.Name;
			this.lblFatherName.Text = studentExamSeatInfo.StudentInfo.FatherName;
			this.lblClass.Text = studentExamSeatInfo.StudentInfo.Class;
			this.lblStudentStatus.Text = studentExamSeatInfo.StudentInfo.StudentStatusFullName;
			this.lblStudentStatusDate.Text = studentExamSeatInfo.StudentInfo.StudentStatusDate.ToString().ToNAIfNullOrEmpty();
			this.lblSemesterFreezedStatus.Text = studentExamSeatInfo.StudentInfo.StudentStatusDate.ToString().ToNAIfNullOrEmpty();
			this.lblSemesterFreezedStatusDate.Text = studentExamSeatInfo.StudentInfo.SemesterFreezedDate.ToString().ToNAIfNullOrEmpty();
			this.lblSemester.Text = studentExamSeatInfo.StudentInfo.SemesterID.ToSemesterString();

			if (this.ExamConductID == null)
				this.gvExamSeats.Caption = $"Exam Seats: (Semester:  {this.SemesterID.ToSemesterString()})";
			else
				this.gvExamSeats.Caption = $"Exam Seats: (Semester: {this.SemesterID.ToSemesterString()} Exam Type:{this.ExamTypeEnum.ToFullName()} Exam Conduct:{this.ddlExamConductID.SelectedItem.Text})";
			this.gvExamSeats.DataBind(studentExamSeatInfo.Seats);

			this.panelDeleteSeats.Visible = true;
		}

		protected void gvExamSeats_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					var examSeatID = e.DecryptedCommandArgumentToInt();
					var result = BL.Core.ExamSeatingPlan.Staff.DeleteExamSeats.DeleteExamSeat(examSeatID, this.StaffIdentity.LoginSessionGuid);
					switch (result)
					{
						case ExecuteSeatingPlan.DeleteExamSeatStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExecuteSeatingPlan.DeleteExamSeatStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Exam seat");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.tbEnrollment_OnSearch(null, null);
					break;
			}
		}
	}
}