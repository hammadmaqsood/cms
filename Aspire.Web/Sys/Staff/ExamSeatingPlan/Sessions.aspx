﻿<%@ Page MasterPageFile="~/Base.Master" Language="C#" AutoEventWireup="true" CodeBehind="Sessions.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.Sessions" %>

<asp:Content runat="server" ContentPlaceHolderID="BodyPH">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireButton runat="server" CausesValidation="False" ID="btnAddExamSession" Text="Add Session" Glyphicon="plus" OnClick="btnAddExamSession_OnClick" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
					<aspire:AspireDropDownList runat="server" CausesValidation="False" ID="ddlSemesterIDFilter" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" AutoPostBack="True" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Exam:" AssociatedControlID="ddlExamTypeFilter" />
					<aspire:AspireDropDownList runat="server" CausesValidation="False" ID="ddlExamTypeFilter" OnSelectedIndexChanged="ddlExamTypeFilter_OnSelectedIndexChanged" AutoPostBack="True" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Exam Conduct:" AssociatedControlID="ddlExamConductIDFilter" />
					<aspire:AspireDropDownList runat="server" CausesValidation="False" ID="ddlExamConductIDFilter" OnSelectedIndexChanged="ddlExamConductIDFilter_OnSelectedIndexChanged" AutoPostBack="True" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvExamSessions" AutoGenerateColumns="False" OnRowCommand="gvExamSessions_OnRowCommand">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%#: Container.DataItemIndex+1%></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Semester" DataField="Semester" />
			<asp:BoundField HeaderText="Exam Type" DataField="ExamTypeFullName" />
			<asp:BoundField HeaderText="Exam Conduct" DataField="ExamConductName" />
			<asp:BoundField HeaderText="Session Name" DataField="SessionName" />
			<asp:BoundField HeaderText="Start Time" DataField="StartTimeString" />
			<asp:BoundField HeaderText="Total Time" DataField="TotalTimeInMinutes" DataFormatString="{0} Minutes" />
			<asp:BoundField HeaderText="Timing" DataField="Timing" />
			<asp:BoundField HeaderText="Invigilation Pay Rate" DataField="InvigilationPayRate" DataFormatString="Rs. {0}" />
			<asp:BoundField HeaderText="Shift" DataField="ShiftFullName" />
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CausesValidation="false" CommandName="Up" Glyphicon="chevron_up" EncryptedCommandArgument='<%# Eval("ExamSessionID")%>' />
					<aspire:AspireLinkButton runat="server" CausesValidation="false" CommandName="Down" Glyphicon="chevron_down" EncryptedCommandArgument='<%# Eval("ExamSessionID")%>' />
					<aspire:AspireLinkButton runat="server" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument='<%# Eval("ExamSessionID") %>' />
					<aspire:AspireLinkButton runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%# Eval("ExamSessionID") %>' ConfirmMessage="Are you sure you want to delete this exam session?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalAddExamSession" HeaderText="Add Exam Session">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelSession" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertAddSession" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" CausesValidation="False" ValidationGroup="Session" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterID" ErrorMessage="This field is required." ValidationGroup="Session" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Exam Type:" AssociatedControlID="ddlExamType" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlExamType" CausesValidation="False" ValidationGroup="Session" AutoPostBack="True" OnSelectedIndexChanged="ddlExamType_OnSelectedIndexChanged" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamType" ErrorMessage="This field is required." ValidationGroup="Session" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Exam Conduct:" AssociatedControlID="ddlExamConductID" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlExamConductID" ValidationGroup="Session" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamConductID" ErrorMessage="This field is required." ValidationGroup="Session" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Session Name:" AssociatedControlID="tbSessionName" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbSessionName" MaxLength="50" ValidationGroup="Session" />
								<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbSessionName" RequiredErrorMessage="This field is required." ValidationGroup="Session" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Start Time:" AssociatedControlID="dtpStartTime" />
							<div class="col-md-9">
								<aspire:AspireDateTimePickerTextbox runat="server" MaxLength="8" ID="dtpStartTime" ValidationGroup="Session" DisplayMode="ShortTime" />
								<aspire:AspireDateTimeValidator runat="server" Format="ShortTime" AllowNull="False" ControlToValidate="dtpStartTime" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Time." ValidationGroup="Session" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Total Time:" AssociatedControlID="tbTotalTime" />
							<div class="col-md-9">
								<div class="input-group">
									<aspire:AspireTextBox runat="server" ID="tbTotalTime" MaxLength="3" ValidationGroup="Session" />
									<span class="input-group-addon">Minutes</span>
								</div>
								<aspire:AspireByteValidator runat="server" AllowNull="False" ControlToValidate="tbTotalTime" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Number must be in between {min} and {max}." RequiredErrorMessage="This field is required." ValidationGroup="Session" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Pay Rate:" AssociatedControlID="tbPayRate" />
							<div class="col-md-9">
								<div class="input-group">
									<span class="input-group-addon">Rs.</span>
									<aspire:AspireTextBox runat="server" ID="tbPayRate" MaxLength="4" ValidationGroup="Session" />
								</div>
								<aspire:AspireInt16Validator AllowNull="False" runat="server" MinValue="0" ControlToValidate="tbPayRate" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Number must be in between {min} and {max}." ValidationGroup="Session" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Shift:" AssociatedControlID="ddlShift" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlShift" ValidationGroup="Session" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlShift" ErrorMessage="This field is required." ValidationGroup="Session" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ButtonType="Primary" Text="Add" ID="btnSave" ValidationGroup="Session" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server" CausesValidation="False">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
