﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports
{
	[AspirePage("954D8957-41E6-4256-9F56-C44FF90433BE", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Reports/InvigilatorSlip.aspx", "Invigilator's Slip", true, AspireModules.ExamSeatingPlan)]
	public partial class InvigilatorSlip : StaffPage, IReportViewer
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Invigilator's Slip";
		public string ReportName => "Invigilator's Slip";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		private new ExamConduct Master => (ExamConduct)base.Master;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
				this.ddlDisplayRoomNames.FillYesNo().SetSelectedValue(false);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this.Master.ExamConductID != null)
			{
				var invigilators = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamInvigilators.GetExamInvigilatorsList(null, this.StaffIdentity.LoginSessionGuid);
				this.ddlInvigilatorIDFilter.DataBind(invigilators, CommonListItems.All);
			}
			else
				this.ddlInvigilatorIDFilter.DataBind(CommonListItems.All);
			this.ddlInvigilatorIDFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlInvigilatorIDFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDisplayRoomNames_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDisplayRoomNames_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var reportViewer = ((ReportView)this.Master.Master).ReportViewer;
			if (this.Master.ExamConductID == null)
			{
				reportViewer.LocalReport.DataSources.Clear();
				return;
			}
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/ExamSeatingPlan/Staff/InvigilatorsSlip.rdlc".MapPath();
			var invigilatorID = this.ddlInvigilatorIDFilter.SelectedValue.ToNullableInt();
			var displayRoomNames = this.ddlDisplayRoomNames.SelectedValue.ToBoolean();
			var dataSet = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.InvigilatorSlip.GetInvigilatorsSlips(this.Master.ExamConductID.Value, invigilatorID, displayRoomNames, this.StaffIdentity.LoginSessionGuid);

			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(dataSet.PageHeader), new[] { dataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(dataSet.SlipDetails), dataSet.SlipDetails));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}