﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports
{
	[AspirePage("E5769819-C91D-43B7-AF10-4E5AD1292CF7", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Reports/DateSheet.aspx", "Date Sheet", true, AspireModules.ExamSeatingPlan)]
	public partial class DateSheet : StaffPage, IReportViewer
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Date Sheet";
		public string ReportName => "Date Sheet";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;
		private new ExamConduct Master => (ExamConduct)base.Master;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All);
				this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			if (this.Master.ExamConductID == null)
			{
				reportViewer.LocalReport.DataSources.Clear();
				return;
			}
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/ExamSeatingPlan/Staff/DateSheet.rdlc".MapPath();
			var reportDataSet = BL.Core.ExamSeatingPlan.Staff.Reports.DateSheet.GetDateSheet(this.Master.ExamConductID.Value, departmentID, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.DateSheet), reportDataSet.DateSheet));
			reportViewer.LocalReport.EnableExternalImages = true;
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master.Master;
			this.RenderReport(reportView.ReportViewer);
		}
	}
}