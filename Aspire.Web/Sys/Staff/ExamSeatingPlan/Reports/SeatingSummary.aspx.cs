﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports
{
	[AspirePage("31CBD811-DA22-401C-8127-9C7AA86DA2A5", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Reports/SeatingSummary", "Seating Summary", true, AspireModules.ExamSeatingPlan)]
	public partial class SeatingSummary : StaffPage, IReportViewer
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public string ReportName => "Seating Summary";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Seating Summary";

		private new DateAndSession Master => (DateAndSession)base.Master;
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		public void RenderReport(ReportViewer reportViewer)
		{
			this.ddlReportType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlReportType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			switch (this.ddlReportType.SelectedValue)
			{
				case "Overall Summary (Matrix)":
				case "Department-wise Summary (Matrix)":
					this.ddlDisplayTeacherNames.ClearSelection();
					this.divDisplayTeacherNames.Visible = true;
					break;
				case "Overall Summary":
				case "Flash":
					this.ddlDisplayTeacherNames.SelectedValue = "1";
					this.divDisplayTeacherNames.Visible = false;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.ddlDisplayTeacherNames_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDisplayTeacherNames_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var displayTeacherNames = this.ddlDisplayTeacherNames.SelectedValue.ToBoolean();
			var reportViewer = ((ReportView)this.Master.Master).ReportViewer;
			if (this.Master.ExamDateAndSessionID == null)
			{
				reportViewer.LocalReport.DataSources.Clear();
				return;
			}
			reportViewer.LocalReport.DataSources.Clear();
			string reportFileName;
			BL.Core.ExamSeatingPlan.Staff.Reports.SeatingSummary.ReportDataset dataSet;
			bool groupByDepartment;
			switch (this.ddlReportType.SelectedValue)
			{
				case "Overall Summary (Matrix)":
					groupByDepartment = false;
					reportFileName = "~/Reports/ExamSeatingPlan/Staff/SeatingSummaryMatrix.rdlc".MapPath();
					dataSet = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.SeatingSummary.GetSeatingSummary(this.Master.ExamDateAndSessionID.Value, displayTeacherNames, this.StaffIdentity.LoginSessionGuid);
					break;
				case "Overall Summary":
					groupByDepartment = false;
					reportFileName = "~/Reports/ExamSeatingPlan/Staff/SeatingSummary.rdlc".MapPath();
					dataSet = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.SeatingSummary.GetSeatingSummary(this.Master.ExamDateAndSessionID.Value, displayTeacherNames, this.StaffIdentity.LoginSessionGuid);
					break;
				case "Department-wise Summary (Matrix)":
					groupByDepartment = true;
					reportFileName = "~/Reports/ExamSeatingPlan/Staff/SeatingSummaryMatrix.rdlc".MapPath();
					dataSet = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.SeatingSummary.GetSeatingSummary(this.Master.ExamDateAndSessionID.Value, displayTeacherNames, this.StaffIdentity.LoginSessionGuid);
					break;
				case "Flash":
					groupByDepartment = true;
					reportFileName = "~/Reports/ExamSeatingPlan/Staff/SeatingFlash.rdlc".MapPath();
					displayTeacherNames = false;
					dataSet = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.SeatingSummary.GetSeatingSummary(this.Master.ExamDateAndSessionID.Value, displayTeacherNames, this.StaffIdentity.LoginSessionGuid);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(dataSet.PageHeader), new[] { dataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(dataSet.SummaryDetails), dataSet.SummaryDetails));
			reportViewer.LocalReport.SetParameters(new ReportParameter("GroupByDepartment", groupByDepartment.ToString()));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}