﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports
{
	[AspirePage("9056B808-D021-4147-ACB3-3ED09C2A1D71", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Reports/ExaminerEnvelope.aspx", "Examiner Envelope", true, AspireModules.ExamSeatingPlan)]
	public partial class ExaminerEnvelope : StaffPage, IReportViewer
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Examiner Envelope";
		public string ReportName => "Examiner Envelope";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;
		private new DateAndSession Master => (DateAndSession)base.Master;

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this.Master.ExamDateAndSessionID == null)
			{
				reportViewer.LocalReport.DataSources.Clear();
				return;
			}
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/ExamSeatingPlan/Staff/ExaminerEnvelope.rdlc".MapPath();
			var reportDataSet = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.ExaminerEnvelope.GetExaminerEnvelope(this.Master.ExamDateAndSessionID.Value, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.ExaminerEnvelopeDetails), reportDataSet.ExaminerEnvelopeDetails));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}