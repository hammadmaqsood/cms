﻿<%@ Page Language="C#" MasterPageFile="~/Sys/Staff/ExamSeatingPlan/Reports/DateAndSession.master" AutoEventWireup="true" CodeBehind="SeatingSummary.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports.SeatingSummary" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlReportType" />
				<aspire:AspireDropDownList runat="server" ID="ddlReportType" ValidationGroup="Parameters" AutoPostBack="True" OnSelectedIndexChanged="ddlReportType_OnSelectedIndexChanged">
					<Items>
						<asp:ListItem Text="Overall Summary (Matrix)" />
						<asp:ListItem Text="Department-wise Summary (Matrix)" />
						<asp:ListItem Text="Overall Summary" />
						<asp:ListItem Text="Flash" />
					</Items>
				</aspire:AspireDropDownList>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group" id="divDisplayTeacherNames" runat="server">
				<aspire:AspireLabel runat="server" Text="Teacher Names:" AssociatedControlID="ddlDisplayTeacherNames" />
				<aspire:AspireDropDownList runat="server" ID="ddlDisplayTeacherNames" ValidationGroup="Parameters" AutoPostBack="True" OnSelectedIndexChanged="ddlDisplayTeacherNames_OnSelectedIndexChanged">
					<Items>
						<asp:ListItem Text="Visible" Value="1" />
						<asp:ListItem Text="Hidden" Value="0" />
					</Items>
				</aspire:AspireDropDownList>
			</div>
		</div>
	</div>
</asp:Content>
