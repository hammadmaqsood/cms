﻿<%@ Page Language="C#" MasterPageFile="~/Sys/Staff/ExamSeatingPlan/Reports/ExamConduct.master" AutoEventWireup="true" CodeBehind="InvigilatorDutiesDateWise.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports.InvigilatorDutiesDateWise" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Exam Dates:" AssociatedControlID="ddlExamDateIDFilter" />
		<aspire:AspireDropDownList runat="server" ID="ddlExamDateIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlExamDateIDFilter_SelectedIndexChanged" />
	</div>
</asp:Content>
