﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports
{
	[AspirePage("36397C20-1FC1-4CBF-9200-4366A1EF6343", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Reports/InvigilatorDutiesDateWise.aspx", "Invigilator Duties Date Wise", true, AspireModules.ExamSeatingPlan)]
	public partial class InvigilatorDutiesDateWise : StaffPage, IReportViewer
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Invigilator Duties Date Wise";
		public string ReportName => "Invigilator Duties Date Wise";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		private new ExamConduct Master => (ExamConduct)base.Master;

		protected void Page_Load(object sender, EventArgs e)
		{
		}

		public void FillExamDates(int examConductID)
		{
			var examDates = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.InvigilatorDutiesDatewise.GetExamDates(examConductID);
			this.ddlExamDateIDFilter.DataBind(examDates);
			this.ddlExamDateIDFilter_SelectedIndexChanged(null, null);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this.Master.ExamConductID == null)
				this.ddlExamDateIDFilter.Items.Clear();
			else
			{
				var examDates = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamDates.GetExamDatesList(this.Master.ExamConductID.Value, this.StaffIdentity.LoginSessionGuid);
				this.ddlExamDateIDFilter.DataBind(examDates);
			}
			this.ddlExamDateIDFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlExamDateIDFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master.Master;
			var reportViewer = reportView.ReportViewer;
			var examDateID = this.ddlExamDateIDFilter.SelectedValue.ToNullableInt();
			if (examDateID == null)
			{
				reportViewer.LocalReport.DataSources.Clear();
				reportViewer.Reset();
				return;
			}
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/ExamSeatingPlan/Staff/InvigilatorDutiesDateWise.rdlc".MapPath();
			var dataSet = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.InvigilatorDutiesDatewise.GetInvigilatorDutiesDatewise(examDateID.Value, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(dataSet.PageHeader), new[] { dataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(dataSet.InvigilatorDuties), dataSet.InvigilatorDuties));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}