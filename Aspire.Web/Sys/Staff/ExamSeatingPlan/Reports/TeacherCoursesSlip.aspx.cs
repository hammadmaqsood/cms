﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports
{
	[AspirePage("1D9E9525-83D7-4F00-8851-BB217BC3033E", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Reports/TeacherCoursesSlip", "Teacher Courses Slip", true, AspireModules.ExamSeatingPlan)]
	public partial class TeacherCoursesSlip : StaffPage, IReportViewer
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Teacher's Courses Slip";
		public string ReportName => "Teacher's Courses Slip";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		private new ExamConduct Master => (ExamConduct)base.Master;

		public void RenderReport(ReportViewer reportViewer)
		{
			reportViewer.LocalReport.DataSources.Clear();
			if (this.Master.ExamConductID == null)
				return;
			var reportFileName = "~/Reports/ExamSeatingPlan/Staff/TeacherCoursesSlip.rdlc".MapPath();
			var reportDataSet = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.TeacherCoursesSlip.GetTeacherCoursesSlips(this.Master.ExamConductID.Value, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.CoursesSlipDetails), reportDataSet.CoursesSlipDetails));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}