﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports
{
	[AspirePage("A0D3E9E4-7EDB-4459-BBDC-F6845D984E40", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Reports/PaperDistributionEnvelopeTitles", "Paper Distribution Envelope Titles", true, AspireModules.ExamSeatingPlan)]
	public partial class PaperDistributionEnvelopeTitles : StaffPage, IReportViewer
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_print);
		public override string PageTitle => "Paper Distribution Envelope Titles";
		public string ReportName => "Paper Distribution Envelope Titles";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;
		private new DateAndSession Master => (DateAndSession)base.Master;
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		public void RenderReport(ReportViewer reportViewer)
		{
			reportViewer.LocalReport.DataSources.Clear();
			if (this.Master.ExamDateAndSessionID == null)
			{
				return;
			}
			var reportFileName = "~/Reports/ExamSeatingPlan/Staff/PaperDistributionEnvelopeTitles.rdlc".MapPath();
			var dataset = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.PaperDistributionEnvelopeTitles.GetEnvelopeTitles(this.Master.ExamDateAndSessionID.Value, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("EnvelopeTitlesDataset", dataset.EnvelopeTitlesDataset));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("PageHeader", new[] { dataset.EnvelopeTitlePageHeader }));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}