﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports
{
	[AspirePage("3CC8C7DD-089A-4597-9E0B-06E31E53B0AD", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Reports/InvigilationDuties.aspx", "Invigilation Duties", true, AspireModules.ExamSeatingPlan)]
	public partial class InvigilationDuties : StaffPage, IReportViewer
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Invigilation Duties";
		public string ReportName => "Invigilation Duties";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;
		private new DateAndSession Master => (DateAndSession)base.Master;

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this.Master.ExamDateAndSessionID == null)
			{
				reportViewer.LocalReport.DataSources.Clear();
				return;
			}
			reportViewer.LocalReport.DataSources.Clear();

			string reportFileName;
			BL.Core.ExamSeatingPlan.Staff.Reports.InvigilationDuties.ReportDataset dataSet;
			switch (this.ddlReportType.SelectedValue)
			{
				case "Attendance Sheet":
					reportFileName = "~/Reports/ExamSeatingPlan/Staff/InvigilationDuties.rdlc".MapPath();
					dataSet = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.InvigilationDuties.GetInvigilatorDuties(this.Master.ExamDateAndSessionID.Value, false, this.StaffIdentity.LoginSessionGuid);
					break;
				case "Question Paper Receiving":
					reportFileName = "~/Reports/ExamSeatingPlan/Staff/InvigilationDutiesPaperReceiving.rdlc".MapPath();
					dataSet = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.InvigilationDuties.GetInvigilatorDuties(this.Master.ExamDateAndSessionID.Value, false, this.StaffIdentity.LoginSessionGuid);
					break;
				case "Sorted List":
					reportFileName = "~/Reports/ExamSeatingPlan/Staff/InvigilationDutiesSorted.rdlc".MapPath();
					dataSet = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.InvigilationDuties.GetInvigilatorDuties(this.Master.ExamDateAndSessionID.Value, true, this.StaffIdentity.LoginSessionGuid);
					break;
				default: throw new ArgumentException();
			}

			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(dataSet.PageHeader), new[] { dataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(dataSet.InvigilationDuties), dataSet.InvigilationDuties));
			reportViewer.LocalReport.EnableExternalImages = true;
		}

		protected void ddlReportType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.RenderReport(((ReportView)this.Master.Master).ReportViewer);
		}
	}
}