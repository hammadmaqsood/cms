﻿using Aspire.BL.Core.ExamSeatingPlan.Staff.Reports;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports
{
	[AspirePage("520E7858-8717-434B-A858-716BE2E108EC", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Reports/StudentAttendanceSheet.aspx", "Student Attendance Sheet", true, AspireModules.ExamSeatingPlan)]
	public partial class StudentAttendanceSheet : StaffPage, IReportViewer
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Student Attendance Sheet";
		public string ReportName => "Student Attendance Sheet";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		private new DateAndSession Master => (DateAndSession)base.Master;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlDepartmentID.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All);
				this.ddlSemesterNo.FillSemesterNos(CommonListItems.All);
				this.ddlSection.FillSections(CommonListItems.All);
				this.ddlShift.FillShifts(CommonListItems.All);
				var facultyMembers = BL.Core.Common.Lists.GetFacultyMembersList(this.StaffIdentity.InstituteID, null);
				this.ddlFacultyMemberID.DataBind(facultyMembers, CommonListItems.All);
				this.ddlDepartmentID_SelectedIndexChanged(null, null);
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this.Master.ExamDateAndSessionID == null)
			{
				reportViewer.LocalReport.DataSources.Clear();
				return;
			}
			reportViewer.LocalReport.DataSources.Clear();
			string reportFileName;
			StudentAttendanceSheets.ReportDataSet reportDataSet;
			var examDateAndSessionID = this.Master.ExamDateAndSessionID.Value;
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
			var sectionEnum = this.ddlSection.GetSelectedEnumValue<Sections>(null);
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>(null);
			var facultyMemberID = this.ddlFacultyMemberID.SelectedValue.ToNullableInt();

			switch (this.ddlPaperSize.SelectedValue)
			{
				case "A4":
					reportFileName = "~/Reports/ExamSeatingPlan/Staff/StudentAttendanceSheetA4.rdlc".MapPath();
					reportDataSet = StudentAttendanceSheets.GetData(examDateAndSessionID, departmentID, programID, semesterNoEnum, sectionEnum, shiftEnum, facultyMemberID, ExamOfferedRoom.RoomUsageTypes.SinglePaper, false, this.StaffIdentity.LoginSessionGuid);
					break;
				case "Legal":
					reportFileName = "~/Reports/ExamSeatingPlan/Staff/StudentAttendanceSheetLegal.rdlc".MapPath();
					reportDataSet = StudentAttendanceSheets.GetData(examDateAndSessionID, departmentID, programID, semesterNoEnum, sectionEnum, shiftEnum, facultyMemberID, ExamOfferedRoom.RoomUsageTypes.SinglePaper, true, this.StaffIdentity.LoginSessionGuid);
					break;
				case "Clashes":
					reportFileName = "~/Reports/ExamSeatingPlan/Staff/StudentAttendanceSheetClashes.rdlc".MapPath();
					reportDataSet = StudentAttendanceSheets.GetData(examDateAndSessionID, departmentID, programID, semesterNoEnum, sectionEnum, shiftEnum, facultyMemberID, ExamOfferedRoom.RoomUsageTypes.MultiplePapers, false, this.StaffIdentity.LoginSessionGuid);
					break;
				default:
					throw new ArgumentOutOfRangeException("ddlPaperSize.SelectedValue", this.ddlPaperSize.SelectedValue, null);
			}
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.StudentDetails), reportDataSet.StudentDetails));
			reportViewer.LocalReport.EnableExternalImages = true;
		}

		protected void ddlDepartmentID_SelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			this.ddlProgramID.FillPrograms(this.StaffIdentity.InstituteID, departmentID, true, CommonListItems.All);
			this.ddlProgramID_SelectedIndexChanged(null, null);
		}

		protected void ddlProgramID_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterNo_SelectedIndexChanged(null, null);
		}

		protected void ddlSemesterNo_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSection_SelectedIndexChanged(null, null);
		}

		protected void ddlSection_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlShift_SelectedIndexChanged(null, null);
		}

		protected void ddlShift_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlFacultyMemberID_SelectedIndexChanged(null, null);
		}

		protected void ddlFacultyMemberID_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlPaperSize_OnSelectedIndexChanged(null, null);
		}

		protected void ddlPaperSize_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master.Master;
			this.RenderReport(reportView.ReportViewer);
		}
	}
}