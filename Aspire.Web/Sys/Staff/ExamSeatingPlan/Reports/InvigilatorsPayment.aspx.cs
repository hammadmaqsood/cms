﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports
{
	[AspirePage("109B96A3-AAE2-4D71-8C5E-72EC8D637976", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Reports/InvigilatorsPayment.aspx", "Invigilators Payment", true, AspireModules.ExamSeatingPlan)]
	public partial class InvigilatorsPayment : StaffPage, IReportViewer
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Invigilators Payment";
		public string ReportName => "Invigilators Payment";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		private new ExamConduct Master => (ExamConduct)base.Master;

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this.Master.ExamConductID == null)
			{
				reportViewer.LocalReport.DataSources.Clear();
				return;
			}
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/ExamSeatingPlan/Staff/InvigilatorsPayment.rdlc".MapPath();
			var dataSet = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.InvigilatorsPayment.GetInvigilatorsPayment(this.Master.ExamConductID.Value, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(dataSet.PageHeader), new[] { dataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(dataSet.PaymentDetails), dataSet.PaymentDetails));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}