﻿<%@ Page Language="C#" MasterPageFile="~/Sys/Staff/ExamSeatingPlan/Reports/DateAndSession.master" AutoEventWireup="true" CodeBehind="QuestionPapersEnvelopeTitles.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports.QuestionPapersEnvelopeTitles" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlReportType" />
		<aspire:AspireDropDownList runat="server" ID="ddlReportType" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlReportType_OnSelectedIndexChanged">
			<Items>
				<asp:ListItem Text="Room-wise" />
				<asp:ListItem Text="Room and Paper wise" />
			</Items>
		</aspire:AspireDropDownList>
	</div>
</asp:Content>
