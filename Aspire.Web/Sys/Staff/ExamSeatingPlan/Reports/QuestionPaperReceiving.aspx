﻿<%@ Page Language="C#" MasterPageFile="~/Sys/Staff/ExamSeatingPlan/Reports/ExamConduct.master" AutoEventWireup="true" CodeBehind="QuestionPaperReceiving.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports.QuestionPaperReceiving" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="row">
		<div class="col-md-8">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Question Paper Received:" AssociatedControlID="ddlQuestionPaperReceived" />
				<aspire:AspireDropDownList runat="server" ID="ddlQuestionPaperReceived" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlQuestionPaperReceived_OnSelectedIndexChanged" />
			</div>
		</div>
	</div>
</asp:Content>
