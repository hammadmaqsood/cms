﻿<%@ Page Language="C#" MasterPageFile="~/Sys/Staff/ExamSeatingPlan/Reports/ExamConduct.master" AutoEventWireup="true" CodeBehind="InvigilatorSlip.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports.InvigilatorSlip" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="row">
		<div class="col-md-8">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Invigilators:" AssociatedControlID="ddlInvigilatorIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlInvigilatorIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlInvigilatorIDFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="ddlDisplayRoomNames" Text="Display Room Names:" />
				<aspire:AspireDropDownList runat="server" ID="ddlDisplayRoomNames" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlDisplayRoomNames_OnSelectedIndexChanged" />
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function () {
			$("#<%=this.ddlInvigilatorIDFilter.ClientID%>").applySelect2();
		});
	</script>
</asp:Content>
