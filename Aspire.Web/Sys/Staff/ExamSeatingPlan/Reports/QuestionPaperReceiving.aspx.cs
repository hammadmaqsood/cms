﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports
{
	[AspirePage("032BE51D-F67F-477A-9680-BF1BC74441BD", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Reports/QuestionPaperReceiving", "Question Paper Receiving", true, AspireModules.ExamSeatingPlan)]
	public partial class QuestionPaperReceiving : StaffPage, IReportViewer
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_print);
		public override string PageTitle => "Question Paper Receiving";
		public string ReportName => "Question Paper Receiving";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;
		private new ExamConduct Master => (ExamConduct)base.Master;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All);
				this.ddlQuestionPaperReceived.FillYesNo(CommonListItems.All);
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlQuestionPaperReceived_OnSelectedIndexChanged(null, null);
		}

		protected void ddlQuestionPaperReceived_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var reportViewer = ((ReportView)this.Master.Master).ReportViewer;
			reportViewer.LocalReport.DataSources.Clear();
			if (this.Master.ExamConductID == null)
				return;
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var questionPaperReceived = this.ddlQuestionPaperReceived.SelectedValue.ToNullableBoolean();
			var reportFileName = "~/Reports/ExamSeatingPlan/Staff/QuestionPaperReceiving.rdlc".MapPath();
			var dataSet = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.QuestionPaperReceiving.GetQuestionPaperReceiving(this.Master.ExamConductID.Value, departmentID, questionPaperReceived, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(dataSet.PageHeader), new[] { dataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(dataSet.ReceivingDetails), dataSet.ReceivingDetails));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}