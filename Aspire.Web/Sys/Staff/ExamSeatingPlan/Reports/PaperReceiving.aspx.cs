﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports
{
	[AspirePage("0DC3720B-B76B-46C3-B8A4-C3697F81A9C5", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Reports/PaperReceiving", "Paper Receiving", true, AspireModules.ExamSeatingPlan)]
	public partial class PaperReceiving : StaffPage, IReportViewer
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Paper Receiving";
		public string ReportName => "Paper Receiving";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;
		private new DateAndSession Master => (DateAndSession)base.Master;

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this.Master.ExamDateAndSessionID == null)
			{
				reportViewer.LocalReport.DataSources.Clear();
				return;
			}
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/ExamSeatingPlan/Staff/PaperReceiving.rdlc".MapPath();
			var dataSet = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.PaperReceiving.GetPaperReceiving(this.Master.ExamDateAndSessionID.Value, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(dataSet.PageHeader), new[] { dataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(dataSet.Details), dataSet.Details));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}