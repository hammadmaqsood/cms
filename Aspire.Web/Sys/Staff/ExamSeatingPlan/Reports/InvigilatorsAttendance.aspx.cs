﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports
{
	[AspirePage("3215F100-ABAC-4125-9CAB-2CF636E733A8", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Reports/InvigilatorsAttendance.aspx", "Invigilators Attendance", true, AspireModules.ExamSeatingPlan)]
	public partial class InvigilatorsAttendance : StaffPage, IReportViewer
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		protected void Page_Load(object sender, EventArgs e)
		{

		}
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Invigilators Attendance";
		public string ReportName => "Invigilators Attendance";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;
		private new ExamConduct Master => (ExamConduct)base.Master;

		public void RenderReport(ReportViewer reportViewer)
		{
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/ExamSeatingPlan/Staff/InvigilatorsAttendance.rdlc".MapPath();
			if (this.Master.ExamConductID == null)
			{
				reportViewer.LocalReport.DataSources.Clear();
				return;
			}
			var dataset = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.InvigilatorsAttendance.GetInvigilatorsAttendance(this.Master.ExamConductID.Value, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(dataset.PageHeader), new[] { dataset.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(dataset.AttendanceDetails), dataset.AttendanceDetails));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}