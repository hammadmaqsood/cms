﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports
{
	[AspirePage("25AC8603-5351-4381-ACD2-477696E64A3B", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Reports/QuestionPapersEnvelopeTitles", "Question Papers Envelope Titles", true, AspireModules.ExamSeatingPlan)]
	public partial class QuestionPapersEnvelopeTitles : StaffPage, IReportViewer
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Question Papers Envelope Titles";
		public string ReportName => "Question Papers Envelope Titles";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;
		private new DateAndSession Master => (DateAndSession)base.Master;

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		public void RenderReport(ReportViewer reportViewer)
		{
			this.ddlReportType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlReportType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var reportViewer = ((ReportView)this.Master.Master).ReportViewer;
			if (this.Master.ExamDateAndSessionID == null)
			{
				reportViewer.LocalReport.DataSources.Clear();
				return;
			}
			reportViewer.LocalReport.DataSources.Clear();
			string reportFileName;
			switch (this.ddlReportType.SelectedValue)
			{
				case "Room-wise":
					reportFileName = "~/Reports/ExamSeatingPlan/Staff/QuestionPaperEnvelopeTitlesRoomWise.rdlc".MapPath();
					break;
				case "Room and Paper wise":
					reportFileName = "~/Reports/ExamSeatingPlan/Staff/QuestionPaperEnvelopeTitlesRoomAndPaperWise.rdlc".MapPath();
					break;
				default:
					throw new ArgumentException();
			}
			var dataSet = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.QuestionPapersEnvelopeTitles.GetData(this.Master.ExamDateAndSessionID.Value, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(dataSet.EnvelopeTitles), dataSet.EnvelopeTitles));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}