﻿using Aspire.Lib.Extensions;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports
{
	public partial class ExamConduct : System.Web.UI.MasterPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterID.FillSemesters();
				this.ddlExamType.FillEnums<Model.Entities.ExamConduct.ExamTypes>();
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.SemesterID == null)
				this.ddlExamConductID.Items.Clear();
			else
			{
				var examConducts = BL.Core.ExamSeatingPlan.Staff.ExamConducts.GetExamConductsList(this.SemesterID.Value, this.ExamTypeEnum, StaffIdentity.Current.LoginSessionGuid);
				this.ddlExamConductID.DataBind(examConducts);
			}
			this.ddlExamConductID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamConductID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var iReportViewer = (IReportViewer)this.Page;
			var reportView = (ReportView)this.Master;
			iReportViewer.RenderReport(reportView.ReportViewer);
		}

		public short? SemesterID => this.ddlSemesterID.SelectedValue.ToNullableShort();
		public Model.Entities.ExamConduct.ExamTypes ExamTypeEnum => this.ddlExamType.GetSelectedEnumValue<Model.Entities.ExamConduct.ExamTypes>();
		public int? ExamConductID => this.ddlExamConductID.SelectedValue.ToNullableInt();
	}
}