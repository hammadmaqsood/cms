﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports
{
	[AspirePage("F8D5CE2C-2D93-496D-993C-BCE0EA2220F4", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Reports/StudentSlips", "Student Slip", true, AspireModules.ExamSeatingPlan)]
	public partial class StudentSlips : StaffPage, IReportViewer
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Student Slip";
		public string ReportName => "Student Slip";
		public bool ExportToExcel => false;
		public bool ExportToWord => false;

		private new ExamConduct Master => (ExamConduct)base.Master;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
			}
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs args)
		{
			var iReportViewer = (IReportViewer)this.Page;
			var reportView = (ReportView)this.Master.Master;
			iReportViewer.RenderReport(reportView.ReportViewer);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this.Master.ExamConductID == null || string.IsNullOrWhiteSpace(this.tbEnrollment.Enrollment))
			{
				reportViewer.LocalReport.DataSources.Clear();
				return;
			}
			reportViewer.LocalReport.DataSources.Clear();
			var enrollment = this.tbEnrollment.Enrollment;
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/ExamSeatingPlan/Common/StudentSlips.rdlc".MapPath();
			var registeredCourses = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.StudentSlip.GetStudentSlips(this.Master.ExamConductID.Value, enrollment, this.StaffIdentity.LoginSessionGuid);
			if (registeredCourses == null)
			{
				this.AddNoRecordFoundAlert();
				return;
			}
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("RegisteredCourses", registeredCourses));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}