﻿<%@ Page Language="C#" MasterPageFile="~/Sys/Staff/ExamSeatingPlan/Reports/ExamConduct.master" AutoEventWireup="true" CodeBehind="DateSheet.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports.DateSheet" %>

<asp:Content runat="server" ContentPlaceHolderID="Parameters">
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
		<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" />
	</div>
</asp:Content>