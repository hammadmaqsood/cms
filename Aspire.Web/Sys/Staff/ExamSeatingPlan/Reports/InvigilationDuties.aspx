﻿<%@ Page Language="C#" MasterPageFile="~/Sys/Staff/ExamSeatingPlan/Reports/DateAndSession.master" AutoEventWireup="true" CodeBehind="InvigilationDuties.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports.InvigilationDuties" %>

<asp:Content runat="server" ContentPlaceHolderID="Parameters">
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlReportType" />
		<aspire:AspireDropDownList runat="server" ID="ddlReportType" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlReportType_OnSelectedIndexChanged">
			<Items>
				<asp:ListItem Text="Attendance Sheet" />
				<asp:ListItem Text="Question Paper Receiving" />
				<asp:ListItem Text="Sorted List" />
			</Items>
		</aspire:AspireDropDownList>
	</div>
</asp:Content>
