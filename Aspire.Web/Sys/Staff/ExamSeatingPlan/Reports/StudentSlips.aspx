﻿<%@ Page Language="C#" MasterPageFile="~/Sys/Staff/ExamSeatingPlan/Reports/ExamConduct.master" AutoEventWireup="true" CodeBehind="StudentSlips.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports.StudentSlips" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
				<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="RequiredEnrollment" OnSearch="tbEnrollment_OnSearch" />
			</div>
		</div>
	</div>
</asp:Content>
