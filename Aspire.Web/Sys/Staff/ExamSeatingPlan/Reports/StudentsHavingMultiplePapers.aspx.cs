﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports
{
	[AspirePage("0EC0A541-581C-42E6-8BFE-A0B7EADE99F3", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Reports/StudentsHavingMultiplePapers.aspx", "Students Having Multiple Papers", true, AspireModules.ExamSeatingPlan)]
	public partial class StudentsHavingMultiplePapers : StaffPage, IReportViewer
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Students Having Multiple Papers";
		public string ReportName => "Students Having Multiple Papers";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		private new DateAndSession Master => (DateAndSession)base.Master;

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this.Master.ExamDateAndSessionID == null)
			{
				reportViewer.LocalReport.DataSources.Clear();
				return;
			}
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/ExamSeatingPlan/Staff/StudentsHavingMultiplePapers.rdlc".MapPath();
			var reportDataSet = Aspire.BL.Core.ExamSeatingPlan.Staff.Reports.StudentsHavingMultiplePapers.GetData(this.Master.ExamDateAndSessionID.Value, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.StudentDetails), reportDataSet.StudentDetails));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}