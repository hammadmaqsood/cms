﻿<%@ Page Language="C#" MasterPageFile="~/Sys/Staff/ExamSeatingPlan/Reports/DateAndSession.master" AutoEventWireup="true" CodeBehind="StudentAttendanceSheet.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports.StudentAttendanceSheet" %>

<asp:Content runat="server" ContentPlaceHolderID="Parameters">
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
				<aspire:AspireDropDownList ValidationGroup="Parameters" runat="server" ID="ddlDepartmentID" OnSelectedIndexChanged="ddlDepartmentID_SelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
				<aspire:AspireDropDownList ValidationGroup="Parameters" runat="server" ID="ddlProgramID" OnSelectedIndexChanged="ddlProgramID_SelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="ddlSemesterNo" />
				<aspire:AspireDropDownList ValidationGroup="Parameters" runat="server" ID="ddlSemesterNo" OnSelectedIndexChanged="ddlSemesterNo_SelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSection" />
				<aspire:AspireDropDownList ValidationGroup="Parameters" runat="server" ID="ddlSection" OnSelectedIndexChanged="ddlSection_SelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShift" />
				<aspire:AspireDropDownList ValidationGroup="Parameters" runat="server" ID="ddlShift" OnSelectedIndexChanged="ddlShift_SelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Teacher:" AssociatedControlID="ddlFacultyMemberID" />
				<aspire:AspireDropDownList ValidationGroup="Parameters" runat="server" ID="ddlFacultyMemberID" OnSelectedIndexChanged="ddlFacultyMemberID_SelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Paper Size:" AssociatedControlID="ddlPaperSize" />
				<aspire:AspireDropDownList ValidationGroup="Parameters" runat="server" ID="ddlPaperSize" AutoPostBack="True" OnSelectedIndexChanged="ddlPaperSize_OnSelectedIndexChanged">
					<Items>
						<asp:ListItem Text="A4" />
						<asp:ListItem Text="Legal" />
						<asp:ListItem Text="Clashes" />
					</Items>
				</aspire:AspireDropDownList>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function () {
			$("#<%=this.ddlFacultyMemberID.ClientID%>").applySelect2();
		});
	</script>
</asp:Content>
