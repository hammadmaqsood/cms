﻿<%@  Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="InvigilatorsAvailability.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.InvigilatorsAvailability" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
					<aspire:AspireDropDownList ValidationGroup="Filter" runat="server" ID="ddlSemesterIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="ExamType:" AssociatedControlID="ddlExamTypeFilter" />
					<aspire:AspireDropDownList ValidationGroup="Filter" runat="server" ID="ddlExamTypeFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlExamTypeFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Exam Conduct:" AssociatedControlID="ddlExamConductIDFilter" />
					<aspire:AspireDropDownList ValidationGroup="Filter" runat="server" ID="ddlExamConductIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlExamConductIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
					<aspire:AspireDropDownList ValidationGroup="Filter" runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<div id="inputGroupSearch" class="input-group">
						<aspire:AspireTextBox runat="server" ID="tbSearch" PlaceHolder="Search..." />
						<span class="input-group-btn">
							<button class="btn btn-default" type="button"><span class="glyphicon glyphicon-remove"></span></button>
							<button class="btn btn-primary" type="button"><span class="glyphicon glyphicon-search"></span></button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<div class="text-center">
		<aspire:AspireButton runat="server" ButtonType="Primary" ID="btnSaveUp" Text="Save" OnClick="btnSave_OnClick" />
		<aspire:AspireButton runat="server" ButtonType="Primary" ID="btnAssignAllDutiesUp" Text="Assign All Duties" ConfirmMessage="Are you sure you want to auto assign all duties?" OnClick="btnAssignAllDuties_OnClick" />
		<aspire:AspireButton runat="server" ButtonType="Primary" ID="btnRevokeAllDutiesUp" Text="Revoke All Duties" ConfirmMessage="Are you sure you want to revoke all assigned duties?" OnClick="btnRevokeAllDuties_OnClick" />
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvInvigilatorsAvailability" DataKeyNames="ExamInvigilatorID" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="False" OnRowDataBound="gvInvigilatorsAvailability_OnRowDataBound" OnSorting="gvInvigilatorsAvailability_OnSorting" OnRowCommand="gvInvigilatorsAvailability_OnRowCommand">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%#: Container.DataItemIndex+1 %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Name" DataField="Name" SortExpression="Name" />
			<asp:BoundField HeaderText="Department" DataField="DepartmentName" SortExpression="DepartmentName" />
			<asp:BoundField HeaderText="Designation" DataField="Designation" SortExpression="Designation" />
			<asp:TemplateField HeaderText="Availability">
				<ItemTemplate>
					<aspire:AspireCheckBoxList runat="server" ID="cblExamSessions" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<div class="btn-group">
						<a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></a>
						<ul class="dropdown-menu dropdown-menu-right">
							<li>
								<aspire:AspireLinkButton Text="Assign All Duties" CommandName="AssignAll" EncryptedCommandArgument='<%# Eval("ExamInvigilatorID") %>' runat="server" CausesValidation="False" ConfirmMessage="Are you sure you want to assign all duties?" />
							</li>
							<li>
								<aspire:AspireLinkButton Text="Revoke All Duties" CommandName="RevokeAll" EncryptedCommandArgument='<%# Eval("ExamInvigilatorID") %>' runat="server" CausesValidation="False" ConfirmMessage="Are you sure you want to revoke all duties?" />
							</li>
						</ul>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<div class="text-center">
		<aspire:AspireButton runat="server" ButtonType="Primary" ID="btnSaveDown" Text="Save" OnClick="btnSave_OnClick" />
		<aspire:AspireButton runat="server" ButtonType="Primary" ID="btnAssignAllDutiesDown" Text="Assign All Duties" ConfirmMessage="Are you sure you want to auto assign all duties?" OnClick="btnAssignAllDuties_OnClick" />
		<aspire:AspireButton runat="server" ButtonType="Primary" ID="btnRevokeAllDutiesDown" Text="Revoke All Duties" ConfirmMessage="Are you sure you want to revoke all assigned duties?" OnClick="btnRevokeAllDuties_OnClick" />
	</div>
	<script type="text/javascript">
		$(function () {
			var inputGroupSearch = $("#inputGroupSearch");
			var tb = $("input", inputGroupSearch);
			var btnSearch = $("button.btn-primary", inputGroupSearch);
			var btnClear = $("button.btn-default", inputGroupSearch);
			btnSearch.click(function () {
				filterRows($("#<%=this.gvInvigilatorsAvailability.ClientID%>"), tb.val(), false);
			}).click();
			btnClear.click(function () {
				tb.val("");
				btnSearch.click();
			});
			tb.on("keypress", function () {
				filterRows($("#<%=this.gvInvigilatorsAvailability.ClientID%>"), tb.val(), false);
			});
		});
	</script>
</asp:Content>
