﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="DateSheets.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.DateSheets" %>

<%@ Register Src="~/Sys/Staff/Common/DateSheets.ascx" TagPrefix="uc1" TagName="DateSheets" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc1:DateSheets runat="server" ID="ucDateSheets" />
</asp:Content>
