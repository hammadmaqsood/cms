﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="InvigilatorsDuties.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.InvigilatorsDuties" %>

<%@ Import Namespace="Aspire.Lib.Helpers" %>
<asp:Content runat="server" ContentPlaceHolderID="BodyPH">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Exam Type:" AssociatedControlID="ddlExamTypeFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlExamTypeFilter" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlExamTypeFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Exam Conduct:" AssociatedControlID="ddlExamConductIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlExamConductIDFilter" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlExamConductIDFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Date and Session:" AssociatedControlID="ddlExamDateAndSessionIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlExamDateAndSessionIDFilter" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlExamDateAndSessionIDFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
				</div>
				<div class="form-group">
					<div class="input-group" id="inputGroupSearch">
						<input class="form-control" placeholder="Search..." type="text" />
						<span class="input-group-btn">
							<button class="btn btn-primary" type="button"><span class="glyphicon glyphicon-search"></span></button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<asp:UpdatePanel runat="server" ID="updatePanelDuties">
		<ContentTemplate>
			<aspire:AspireGridView runat="server" ID="gvInvigilatorsDuties" AllowSorting="True" AutoGenerateColumns="False" OnRowCommand="gvInvigilatorsDuties_OnRowCommand" OnSorting="gvInvigilatorsDuties_OnSorting">
				<Columns>
					<asp:TemplateField HeaderText="#">
						<ItemTemplate>
							<%#: Container.DataItemIndex+1 %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Fill Sequence" SortExpression="Sequence">
						<ItemTemplate>
							<%#: (short)Eval("Sequence")+1 %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField HeaderText="Building" DataField="BuildingName" SortExpression="BuildingName" />
					<asp:BoundField HeaderText="Room" DataField="RoomName" SortExpression="RoomName" />
					<asp:BoundField HeaderText="Type" DataField="RoomUsageTypeFullName" SortExpression="RoomUsageTypeFullName" />
					<asp:TemplateField HeaderText="Seats" SortExpression="Students">
						<ItemTemplate><%#: Eval("Students") %>/<%#: Eval("TotalSeats") %></ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Invigilator 1" SortExpression="ExamInvigilatorOneName">
						<ItemTemplate>
							<%#: Eval("ExamInvigilatorOneName") %>
							<aspire:AspireLinkButton CausesValidation="False" CommandName="Delete" ConfirmMessage="Are you sure you want to remove?" EncryptedCommandArgument='<%# Eval("ExamInvigilatorIDOne") %>' runat="server" Glyphicon="remove" Visible='<%# Eval("ExamInvigilatorOneDeleteAllowed") %>' />
							<aspire:AspireHyperLinkButton ButtonType="OnlyIcon" CssClass="text-success" href="#" data-toggle="modal" data-target='<%# "#"+ this.modalAssignInvigilator.ClientID %>' runat="server" Glyphicon="plus" data-RoomName='<%# Eval("RoomName") %>' data-BuildingName='<%# Eval("BuildingName") %>' data-InvigilatorType="Invigilator 1" data-ExamOfferedRoomID='<%# Eval("ExamOfferedRoomID").ToString().Encrypt() %>' Visible='<%# Eval("ExamInvigilatorIDOne") == null %>' />
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Invigilator 2" SortExpression="ExamInvigilatorTwoName">
						<ItemTemplate>
							<%#: Eval("ExamInvigilatorTwoName") %>
							<aspire:AspireLinkButton CausesValidation="False" CommandName="Delete" ConfirmMessage="Are you sure you want to remove?" EncryptedCommandArgument='<%# Eval("ExamInvigilatorIDTwo") %>' runat="server" Glyphicon="remove" Visible='<%# Eval("ExamInvigilatorTwoDeleteAllowed") %>' />
							<aspire:AspireHyperLinkButton ButtonType="OnlyIcon" CssClass="text-success" href="#" data-toggle="modal" data-target='<%# "#"+ this.modalAssignInvigilator.ClientID %>' runat="server" Glyphicon="plus" data-RoomName='<%# Eval("RoomName") %>' data-BuildingName='<%# Eval("BuildingName") %>' data-InvigilatorType="Invigilator 2" data-ExamOfferedRoomID='<%# Eval("ExamOfferedRoomID").ToString().Encrypt() %>' Visible='<%# Eval("ExamInvigilatorIDTwo") == null %>' />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</aspire:AspireGridView>
			<div class="text-right">
				<aspire:AspireButton runat="server" ID="btnClearAll" ButtonType="Danger" OnClick="btnClearAll_OnClick" Text="Clear All" CausesValidation="False" ConfirmMessage="Are you sure you want to auto clear all assigned duties?" />
				<aspire:AspireButton runat="server" ID="btnAutoAssign" OnClick="btnAutoAssign_OnClick" Text="Auto Assign All" CausesValidation="False" ConfirmMessage="Are you sure you want to auto assign all duties?" />
			</div>
			<aspire:AspireModal runat="server" HeaderText="Assign Invigilator" ID="modalAssignInvigilator">
				<BodyTemplate>
					<aspire:HiddenField runat="server" Mode="Encrypted" ID="hfExamOfferedRoomID" />
					<aspire:HiddenField runat="server" Mode="PlainText" ID="hfInvigilatorType" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Building:" AssociatedControlID="lblBuildingName" />
							<div class="col-md-10 form-control-static">
								<aspire:Label runat="server" ID="lblBuildingName" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Room:" AssociatedControlID="lblRoomName" />
							<div class="col-md-10 form-control-static">
								<aspire:Label runat="server" ID="lblRoomName" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Type:" AssociatedControlID="lblInvigilatorType" />
							<div class="col-md-10 form-control-static">
								<aspire:Label runat="server" ID="lblInvigilatorType" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Invigilators (Recent):" AssociatedControlID="ddlExamInvigilatorIDRecent" />
							<div class="col-md-10">
								<aspire:AspireDropDownList runat="server" ID="ddlExamInvigilatorIDRecent" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Invigilators:" AssociatedControlID="ddlExamInvigilatorID" />
							<div class="col-md-10">
								<aspire:AspireDropDownListWithOptionGroups runat="server" ID="ddlExamInvigilatorID" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamInvigilatorID" ErrorMessage="This field is required." ValidationGroup="Assign" />
							</div>
						</div>
					</div>
				</BodyTemplate>
				<FooterTemplate>
					<aspire:AspireButton ButtonType="Primary" runat="server" Text="Assign" ID="btnAssign" ValidationGroup="Assign" OnClick="btnAssign_OnClick" />
					<aspire:AspireModalCloseButton runat="server" Text="Add" CausesValidation="False">Cancel</aspire:AspireModalCloseButton>
				</FooterTemplate>
			</aspire:AspireModal>
		</ContentTemplate>
	</asp:UpdatePanel>
	<script type="text/javascript">
		$(function () {
			var inputGroupSearch = $("#inputGroupSearch");
			var tb = $("input", inputGroupSearch);
			$("button", inputGroupSearch).click(function () {
				filterRows($("#<%=this.gvInvigilatorsDuties.ClientID%>"), tb.val(), false);
			});
			tb.on("keypress", function () {
				filterRows($("#<%=this.gvInvigilatorsDuties.ClientID%>"), tb.val(), false);
			});
			var init = function () {
				$("#<%=this.ddlExamInvigilatorID.ClientID%>").add($("#<%=this.ddlExamInvigilatorIDRecent.ClientID%>")).select2({
					theme: "bootstrap",
					width: "style",
					dropdownParent: $('#<%=this.modalAssignInvigilator.ClientID%>')
				});
				$('#<%=this.modalAssignInvigilator.ClientID%>')
					.on('show.bs.modal',
						function (event) {
							var button = $(event.relatedTarget);
							$("#<%=this.hfExamOfferedRoomID.ClientID%>").val(button.data("ExamOfferedRoomID".toLowerCase()));
							$("#<%=this.hfInvigilatorType.ClientID%>").val(button.data("InvigilatorType".toLowerCase()));
							$("#<%=this.lblBuildingName.ClientID%>").text(button.data("BuildingName".toLowerCase()));
							$("#<%=this.lblRoomName.ClientID%>").text(button.data("RoomName".toLowerCase()));
							$("#<%=this.lblInvigilatorType.ClientID%>").text(button.data("InvigilatorType".toLowerCase()));
							var modal = $(this);
							$("#<%=this.btnAssign.ClientID%>").click(function () { $(modal).modal("hide"); });
						});
			};
			init();
			Sys.Application.add_load(init);
		});
	</script>
</asp:Content>
