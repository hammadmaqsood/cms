﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="DeleteSeats.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.DeleteSeats" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" AutoPostBack="True" ValidationGroup="Filter" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Exam Type:" AssociatedControlID="ddlExamType" />
					<aspire:AspireDropDownList runat="server" ID="ddlExamType" AutoPostBack="True" ValidationGroup="Filter" OnSelectedIndexChanged="ddlExamType_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Exam Conduct:" AssociatedControlID="ddlExamConductID" />
					<aspire:AspireDropDownList runat="server" ID="ddlExamConductID" ValidationGroup="Filter" />
					<%--<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamConductID" ErrorMessage="This field is required." ValidationGroup="Search" />--%>
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
					<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="Search" OnSearch="tbEnrollment_OnSearch" />
				</div>
			</div>
		</div>
	</div>
	<asp:Panel runat="server" ID="panelDeleteSeats" Visible="False">
		<div class="table-responsive">
			<table class="table table-bordered table-condensed tableCol4">
				<caption>Student Information</caption>
				<tbody>
					<tr>
						<th>Enrollment</th>
						<td>
							<aspire:AspireHyperLink runat="server" ID="hlEnrollment" />
						</td>
						<th>Registration No.</th>
						<td>
							<aspire:Label runat="server" ID="lblRegistrationNo" /></td>
					</tr>
					<tr>
						<th>Name</th>
						<td>
							<aspire:Label runat="server" ID="lblName" /></td>
						<th>Father Name</th>
						<td>
							<aspire:Label runat="server" ID="lblFatherName" /></td>
					</tr>
					<tr>
						<th>Class</th>
						<td>
							<aspire:Label runat="server" ID="lblClass" /></td>
						<th>Student Status</th>
						<td>
							<aspire:Label runat="server" ID="lblStudentStatus" />
						</td>

					</tr>
					<tr>
						<th>Student Status Date</th>
						<td>
							<aspire:Label runat="server" ID="lblStudentStatusDate" />
						</td>
						<th>Semester Freezed Status</th>
						<td>
							<aspire:Label runat="server" ID="lblSemesterFreezedStatus" /></td>

					</tr>
					<tr>
						<th>Semester Freezed Status Date</th>
						<td>
							<aspire:Label runat="server" ID="lblSemesterFreezedStatusDate" /></td>
						<th>Semester</th>
						<td>
							<aspire:Label runat="server" ID="lblSemester" /></td>
					</tr>
				</tbody>
			</table>
		</div>

		<aspire:AspireGridView runat="server" ID="gvExamSeats" AutoGenerateColumns="False" OnRowCommand="gvExamSeats_OnRowCommand">
			<Columns>
				<asp:BoundField DataField="Title" HeaderText="Course Title" />
				<asp:BoundField DataField="Class" HeaderText="Class" />
				<asp:BoundField DataField="CompleteStatus" HeaderText="Status" />
				<asp:BoundField DataField="FeeStatusFullName" HeaderText="Fee Status" />
				<asp:BoundField DataField="Session" HeaderText="Session and Date" />
				<asp:BoundField DataField="RoomName" HeaderText="Room" />
				<asp:BoundField DataField="Row" HeaderText="Row" />
				<asp:BoundField DataField="Column" HeaderText="Column" />
				<asp:TemplateField>
					<ItemTemplate>
						<aspire:AspireLinkButton runat="server" CommandName="Delete" CausesValidation="False" EncryptedCommandArgument='<%#this.Eval("ExamSeatID") %>' Glyphicon="remove" ToolTip="Delete" Visible='<%#(int)this.Eval("ExamSeatID")!=0%>' ConfirmMessage="Are you sure you want to delete?" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
	</asp:Panel>
</asp:Content>
