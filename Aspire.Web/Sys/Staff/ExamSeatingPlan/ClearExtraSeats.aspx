﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ClearExtraSeats.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.ClearExtraSeats" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Filter" ID="ddlSemesterID" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="ExamType:" AssociatedControlID="ddlExamType" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Filter" ID="ddlExamType" AutoPostBack="True" OnSelectedIndexChanged="ddlExamType_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Exam Conduct:" AssociatedControlID="ddlExamConductID" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Filter" ID="ddlExamConductID" AutoPostBack="True" OnSelectedIndexChanged="ddlExamConductID_OnSelectedIndexChanged" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gv" AutoGenerateColumns="False" OnPageIndexChanging="gv_PageIndexChanging" OnPageSizeChanging="gv_PageSizeChanging" OnSorting="gv_Sorting">
		<Columns>
			<asp:BoundField DataField="Enrollment" HeaderText="Enrollment" SortExpression="Enrollment" />
			<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
			<asp:BoundField DataField="ProgramAlias" HeaderText="Program" SortExpression="ProgramAlias" />
			<asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
			<asp:BoundField DataField="ExamDate" HeaderText="Date" DataFormatString="{0:D}" SortExpression="ExamDate" />
			<asp:BoundField DataField="SessionName" HeaderText="Session" SortExpression="SessionName" />
		</Columns>
	</aspire:AspireGridView>

	<p class="text-center">
		<aspire:AspireButton ButtonType="Danger" ID="btnClear" CausesValidation="False" runat="server" Text="Clear All" ConfirmMessage="Are you sure you want to clear all extra seats?" OnClick="btnClear_OnClick" />
	</p>
</asp:Content>
