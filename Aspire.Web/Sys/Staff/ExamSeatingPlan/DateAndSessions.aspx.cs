﻿using Aspire.BL.Core.ExamSeatingPlan.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("6A59F0DA-2975-4D3F-9062-05BF481D64DB", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/DateAndSessions.aspx", "Date And Sessions", true, AspireModules.ExamSeatingPlan)]
	public partial class DateAndSessions : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.ManageExamDateAndSessions , new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};
		public override PageIcon PageIcon => FontAwesomeIcons.solid_calendar_alt.GetIcon();
		public override string PageTitle => "Exam Dates And Sessions";

		public static string GetPageUrl(short? semesterID, ExamConduct.ExamTypes? examType, int? examConductID)
		{
			return GetAspirePageAttribute<DateAndSessions>().PageUrl.AttachQueryParams("SemesterID", semesterID, "ExamType", examType, "ExamConductID", examConductID);
		}

		private void RefreshPage()
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var examType = this.ddlExamTypeFilter.GetSelectedEnumValue<ExamConduct.ExamTypes>(null);
			var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToNullableInt();
			this.Response.Redirect(GetPageUrl(semesterID, examType, examConductID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
				this.ddlExamTypeFilter.FillExamConductExamTypes().SetSelectedValueIfNotPostback("ExamType");
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamTypeFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamTypeFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var examTypeEnum = this.ddlExamTypeFilter.GetSelectedEnumValue<ExamConduct.ExamTypes>();
			if (semesterID == null)
				this.ddlExamConductIDFilter.Items.Clear();
			else
			{
				var examConducts = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamConducts.GetExamConductsList(semesterID.Value, examTypeEnum, this.StaffIdentity.LoginSessionGuid);
				this.ddlExamConductIDFilter.DataBind(examConducts).SetSelectedValueIfNotPostback("ExamConductID");
				this.ddlExamConductIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlExamConductIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetExamDateAndSessions();
		}

		private void GetExamDateAndSessions()
		{
			var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToNullableInt();
			if (examConductID == null)
			{
				this.gvExamDateAndSessions.DataBindNull();
				return;
			}
			var examDateAndSessions = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamDateAndSessions.GetExamDateAndSessions(examConductID.Value, this.StaffIdentity.LoginSessionGuid);
			this.gvExamDateAndSessions.DataBind(examDateAndSessions);
		}

		protected void gvExamDateAndSessions_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					var examDateAndSessionID = e.DecryptedCommandArgumentToInt();
					var status = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamDateAndSessions.DeleteExamDateAndSession(examDateAndSessionID, this.StaffIdentity.LoginSessionGuid);
					switch (status)
					{
						case ExamDateAndSessions.DeleteExamDateAndSessionStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.RefreshPage();
							return;
						case ExamDateAndSessions.DeleteExamDateAndSessionStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Exam Date And Session");
							this.RefreshPage();
							return;
						case ExamDateAndSessions.DeleteExamDateAndSessionStatuses.ChildRecordExistsExamDateSheets:
						case ExamDateAndSessions.DeleteExamDateAndSessionStatuses.ChildRecordExistsExamInvigilatorDuties:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Exam Date And Session");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		protected void btnAddExamDateAndSession_OnClick(object sender, EventArgs e)
		{
			this.ShowExamDateAndSessionModal();
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var examTypeEnum = this.ddlExamType.GetSelectedEnumValue<ExamConduct.ExamTypes>(null);
			if (semesterID == null || examTypeEnum == null)
			{
				this.ddlExamConductID.Items.Clear();
				this.ddlExamDateID.Items.Clear();
				this.ddlExamSessionID.Items.Clear();
			}
			else
			{
				var examConducts = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamConducts.GetExamConductsList(semesterID.Value, examTypeEnum.Value, this.StaffIdentity.LoginSessionGuid);
				this.ddlExamConductID.DataBind(examConducts);
				this.ddlExamConductID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlExamConductID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var examConductID = this.ddlExamConductID.SelectedValue.ToNullableInt();
			if (examConductID == null)
			{
				this.ddlExamDateID.Items.Clear();
				this.ddlExamSessionID.Items.Clear();
			}
			else
			{
				var examDates = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamDates.GetExamDatesList(examConductID.Value, this.StaffIdentity.LoginSessionGuid);
				var examSessions = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamSessions.GetExamSessionsList(examConductID.Value, this.StaffIdentity.LoginSessionGuid);
				this.ddlExamDateID.DataBind(examDates);
				this.ddlExamSessionID.DataBind(examSessions);
			}
		}

		private void ShowExamDateAndSessionModal()
		{
			if (this.ddlSemesterID.Items.Count == 0)
			{
				this.ddlSemesterID.FillSemesters();
				this.ddlExamType.FillExamConductExamTypes();
			}

			this.ddlSemesterID.ClearSelection();
			this.ddlExamType.ClearSelection();
			this.ddlSemesterID_OnSelectedIndexChanged(null, null);

			this.modalAddExamDateAndSession.HeaderText = "Add Exam Date And Session";
			this.btnSave.Text = @"Add";
			this.modalAddExamDateAndSession.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var examConductID = this.ddlExamConductID.SelectedValue.ToInt();
			var examSessionID = this.ddlExamSessionID.SelectedValue.ToInt();
			var examDateID = this.ddlExamDateID.SelectedValue.ToInt();

			var status = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamDateAndSessions.AddExamDateAndSession(examConductID, examDateID, examSessionID, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case ExamDateAndSessions.AddExamDateAndSessionStatuses.Success:
					this.AddSuccessMessageHasBeenAdded("Exam Date And Session");
					this.RefreshPage();
					break;
				case ExamDateAndSessions.AddExamDateAndSessionStatuses.NameAlreadyExists:
					this.alertAddDateAndSession.AddErrorAlert("Exam Date And Session already exists in selected Exam Conduct.");
					this.updatePanelDateAndSession.Update();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}