﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="InvigilationDutiesAttendance.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.InvigilationDutiesAttendance" %>

<%@ Import Namespace="Aspire.BL.Core.ExamSeatingPlan.Staff" %>
<%@ Import Namespace="Aspire.Model.Entities" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Exam Type:" AssociatedControlID="ddlExamTypeFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlExamTypeFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlExamTypeFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Exam Conduct:" AssociatedControlID="ddlExamConductIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlExamConductIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlExamConductIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Exam Date and Session:" AssociatedControlID="ddlExamDateAndSessionIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlExamDateAndSessionIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlExamDateAndSessionIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<div id="inputGroupSearch" class="input-group">
						<input class="form-control" placeholder="Search..." type="text" />
						<span class="input-group-btn">
							<button class="btn btn-primary" type="button"><span class="glyphicon glyphicon-search"></span></button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvExamInvigilatorDuties" AutoGenerateColumns="False" DataKeyNames="ExamInvigilatorDutyID">
		<Columns>
			<asp:BoundField HeaderText="Invigilator Name" DataField="InvigilatorName" />
			<asp:BoundField HeaderText="Room" DataField="RoomName" />
			<asp:BoundField HeaderText="Date and Session" DataField="ExamDateAndSession" />
			<asp:TemplateField HeaderText="Attendance">
				<ItemTemplate>
					<aspire:AspireRadioButtonList runat="server" ID="rblAttendance" DataSource='<%# AttendanceStatuses %>' DataTextField="Text" DataValueField="ID" SelectedValue='<%# Eval("AttendanceStatus") %>' RepeatDirection="Horizontal" RepeatLayout="Flow" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblAttendance" ErrorMessage="This field is required." ValidationGroup="Attendance" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<div class="text-center">
		<button type="button" id="btnAllAbsent" class="btn btn-default">All Absent</button>
		<button type="button" id="btnAllPresent" class="btn btn-default">All Present</button>
		<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" OnClick="btnSave_OnClick" ValidationGroup="Attendance" />
	</div>
	<script type="text/javascript">
		$(function () {
			var inputGroupSearch = $("#inputGroupSearch");
			var tb = $("input", inputGroupSearch);
			$("button", inputGroupSearch).click(function () {
				filterRows($("#<%=this.gvExamInvigilatorDuties.ClientID%>"), tb.val(), false);
			});
			tb.on("keypress", function () {
				filterRows($("#<%=this.gvExamInvigilatorDuties.ClientID%>"), tb.val(), false);
			});

			$("#btnAllAbsent").click(function () {
				$(":radio[value=<%=ExamInvigilatorDuty.AttendanceStatusAbsent%>]")
					.each(function (i, cb) {
						$(cb).prop("checked", true);
					});
			});
			$("#btnAllPresent").click(function () {
				$(":radio[value=<%=ExamInvigilatorDuty.AttendanceStatusPresent%>]")
					.each(function (i, cb) {
						$(cb).prop("checked", true);
					});
			});
		});
	</script>
</asp:Content>
