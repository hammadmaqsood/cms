﻿using Aspire.BL.Core.ExamSeatingPlan.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("15F2C394-585A-4720-BC4D-3D1D4019C3C6", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Conducts.aspx", "Conducts", true, AspireModules.ExamSeatingPlan)]
	public partial class Conducts : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.ManageExamConducts, new [] { UserGroupPermission.PermissionValues.Allowed, } }
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Exam Conducts";

		public static string GetPageUrl(short? semesterID, ExamConduct.ExamTypes? examType, int? pageIndex, int? pageSize, SortDirection sortDirection, string sortExpression)
		{
			return GetAspirePageAttribute<ExamSeatingPlan.Conducts>().PageUrl.AttachQueryParams("SemesterID", semesterID, "ExamType", (byte?)examType, "PageIndex", pageIndex, "PageSize", pageSize, "SortDirection", sortDirection, "SortExpression", sortExpression);
		}

		private void RefreshPage()
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var examType = this.ddlExamTypeFilter.GetSelectedEnumValue<ExamConduct.ExamTypes>(null);
			var pageIndex = this.gvExamConduct.PageIndex;
			var pageSize = this.gvExamConduct.PageSize;
			var sortDirection = this.ViewState.GetSortDirection();
			var sortExpression = this.ViewState.GetSortExpression();
			this.Response.Redirect(GetPageUrl(semesterID, examType, pageIndex, pageSize, sortDirection, sortExpression));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(this.GetParameterValue("SortExpression") ?? "SemesterID", this.GetParameterValue<SortDirection>("SortDirection") ?? SortDirection.Descending);
				this.ddlSemesterIDFilter.FillSemesters(CommonListItems.All).SetSelectedValueIfNotPostback("SemesterID");
				this.ddlExamTypeFilter.FillExamConductExamTypes(CommonListItems.All).SetSelectedValueIfNotPostback("ExamType");
				this.gvExamConduct.PageSize = this.GetParameterValue<int>("PageSize") ?? this.gvExamConduct.PageSize;
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		#region Filters

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamTypeFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamTypeFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
				this.GetData(this.GetParameterValue<int>("PageIndex") ?? 0);
			else
				this.GetData(0);
		}

		protected void btnAdd_OnClick(object sender, EventArgs e)
		{
			this.DisplayExamConductModal(null);
		}

		#endregion

		#region Exam Conduct

		protected void gvExamConduct_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvExamConduct_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvExamConduct_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvExamConduct.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			this.StaffIdentity.Demand(StaffPermissions.ExamSeatingPlan.ManageExamConducts, UserGroupPermission.PermissionValues.Allowed);
			var examConductID = (int?)this.ViewState["ExamConductID"];
			var examTypeEnum = this.ddlExamType.GetSelectedEnumValue<ExamConduct.ExamTypes>();
			var name = this.tbName.Text;
			var blockFeeDefaulters = this.rblBlockFeeDefaulters.SelectedValue.ToBoolean();
			var visibleToStudents = this.rblVisibleToStudents.SelectedValue.ToBoolean();
			var visibleToFacultyMembers = this.rblVisibleToFacultyMembers.SelectedValue.ToBoolean();
			if (examConductID == null)
			{
				var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
				var result = BL.Core.ExamSeatingPlan.Staff.ExamConducts.AddExamConduct(semesterID, examTypeEnum, name, blockFeeDefaulters, this.StaffIdentity.LoginSessionGuid);
				switch (result)
				{
					case ExamConducts.AddExamConductStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Exam Conduct");
						this.RefreshPage();
						return;
					case ExamConducts.AddExamConductStatuses.NameAlreadyExist:
						this.alertModalExamConduct.AddErrorAlert("Exam Conduct Name already exists.");
						this.updateModalExamConduct.Update();
						break;
					default:
						throw new NotImplementedEnumException(result);
				}
			}
			else
			{
				var result = BL.Core.ExamSeatingPlan.Staff.ExamConducts.UpdateExamConduct(examConductID.Value, name, blockFeeDefaulters, visibleToStudents, visibleToFacultyMembers, this.StaffIdentity.LoginSessionGuid);
				switch (result)
				{
					case ExamConducts.UpdateExamConductStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<Conducts>();
						return;
					case ExamConducts.UpdateExamConductStatuses.NameAlreadyExists:
						this.alertModalExamConduct.AddErrorAlert("Exam Conduct Name already exists.");
						this.updateModalExamConduct.Update();
						return;
					case ExamConducts.UpdateExamConductStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Exam Conduct");
						this.RefreshPage();
						return;
					case ExamConducts.UpdateExamConductStatuses.CanNotVisibleToStudentsDueToNoExamDatesFound:
						this.alertModalExamConduct.AddErrorAlert("Cannot make Visible to Students because no sessions record found.");
						this.updateModalExamConduct.Update();
						return;
					case ExamConducts.UpdateExamConductStatuses.CanNotVisibleToStudentsDueToExamsFinished:
						this.alertModalExamConduct.AddErrorAlert("Cannot make Visible to Students because exams are already finished.");
						this.updateModalExamConduct.Update();
						return;
					case ExamConducts.UpdateExamConductStatuses.CanNotVisibleToStudentsDueToTeacherEvaluationSurveyBeforeMidNotOpen:
						this.alertModalExamConduct.AddErrorAlert("Cannot make Visible to Students because Teacher Evaluation Survey (Before Mid) not opened.");
						this.updateModalExamConduct.Update();
						return;
					case ExamConducts.UpdateExamConductStatuses.CanNotVisibleToStudentsDueToTeacherEvaluationSurveyBeforeFinalNotOpen:
						this.alertModalExamConduct.AddErrorAlert("Cannot make Visible to Students because Teacher Evaluation Survey (Before Final) not opened.");
						this.updateModalExamConduct.Update();
						return;
					case ExamConducts.UpdateExamConductStatuses.CanNotVisibleToStudentsDueToCourseEvaluationSurveyBeforeFinalNotOpen:
						this.alertModalExamConduct.AddErrorAlert("Cannot make Visible to Students because Course Evaluation Survey (Before Final) not opened.");
						this.updateModalExamConduct.Update();
						return;
					case ExamConducts.UpdateExamConductStatuses.CanNotVisibleToStudentsDueToGraduateSurveyBeforeFinalNotOpen:
						this.alertModalExamConduct.AddErrorAlert("Cannot make Visible to Students because Graduating Student Survey (Before Final) not opened.");
						this.updateModalExamConduct.Update();
						return;
					default:
						throw new NotImplementedEnumException(result);
				}
			}
		}

		protected void gvExamConduct_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayExamConductModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					this.StaffIdentity.Demand(StaffPermissions.ExamSeatingPlan.ManageExamConducts, UserGroupPermission.PermissionValues.Allowed);
					var examConductID = e.DecryptedCommandArgumentToInt();
					var result = BL.Core.ExamSeatingPlan.Staff.ExamConducts.DeleteExamConduct(examConductID, this.StaffIdentity.LoginSessionGuid);
					switch (result)
					{
						case ExamConducts.DeleteExamConductStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							Redirect<Conducts>();
							break;
						case ExamConducts.DeleteExamConductStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Exam Conduct");
							this.RefreshPage();
							break;
						case ExamConducts.DeleteExamConductStatuses.ChildRecordExistsExamDateAndSessions:
						case ExamConducts.DeleteExamConductStatuses.ChildRecordExistsExamDates:
						case ExamConducts.DeleteExamConductStatuses.ChildRecordExistsExamOfferedRooms:
						case ExamConducts.DeleteExamConductStatuses.ChildRecordExistsExamSessions:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Exam Conduct");
							break;
						default:
							throw new NotImplementedEnumException(result);
					}
					break;
			}
		}

		#endregion

		#region Page Specific Fucntions

		private void GetData(int pageIndex)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var examTypeEnum = this.ddlExamTypeFilter.GetSelectedEnumValue<ExamConduct.ExamTypes>(null);
			var examConducts = BL.Core.ExamSeatingPlan.Staff.ExamConducts.GetExamConducts(semesterID, examTypeEnum, pageIndex, this.gvExamConduct.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out int virtualItemCount, this.StaffIdentity.LoginSessionGuid);
			this.gvExamConduct.DataBind(examConducts, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		private void DisplayExamConductModal(int? examConductID)
		{
			this.StaffIdentity.Demand(StaffPermissions.ExamSeatingPlan.ManageExamConducts, UserGroupPermission.PermissionValues.Allowed);

			this.ddlSemesterID.FillSemesters();
			this.ddlExamType.FillExamConductExamTypes();
			this.rblBlockFeeDefaulters.FillYesNo();
			this.rblVisibleToStudents.FillYesNo();
			this.rblVisibleToFacultyMembers.FillYesNo();

			if (examConductID == null)
			{
				this.ddlSemesterID.ClearSelection();
				this.ddlExamType.ClearSelection();
				this.ddlSemesterID.Enabled = true;
				this.ddlExamType.Enabled = true;
				this.tbName.Text = null;
				this.rblBlockFeeDefaulters.SetSelectedValueNo();
				this.rblVisibleToStudents.SetSelectedValueNo();
				this.rblVisibleToStudents.Enabled = false;
				this.rblVisibleToFacultyMembers.SetSelectedValueNo();
				this.rblVisibleToFacultyMembers.Enabled = false;

				this.modalExamConduct.HeaderText = "Add Exam Conduct";
				this.btnSave.Text = @"Add";
				this.ViewState["ExamConductID"] = null;
			}
			else
			{
				var examConduct = BL.Core.ExamSeatingPlan.Staff.ExamConducts.GetExamConduct(examConductID.Value, this.StaffIdentity.LoginSessionGuid);
				if (examConduct == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Conducts>();
					return;
				}
				this.ddlSemesterID.SelectedValue = examConduct.SemesterID.ToString();
				this.ddlSemesterID.Enabled = false;
				this.ddlExamType.SelectedValue = examConduct.ExamType.ToString();
				this.ddlExamType.Enabled = false;
				this.tbName.Text = examConduct.Name;
				this.rblBlockFeeDefaulters.SetSelectedValue(examConduct.BlockFeeDefaulters);
				this.rblVisibleToStudents.SetSelectedValue(examConduct.VisibleToStudents);
				this.rblVisibleToStudents.Enabled = true;
				this.rblVisibleToFacultyMembers.SetSelectedValue(examConduct.VisibleToFacultyMembers);
				this.rblVisibleToFacultyMembers.Enabled = true;

				this.modalExamConduct.HeaderText = "Edit Exam Conduct";
				this.btnSave.Text = @"Save";
				this.ViewState["ExamConductID"] = examConductID;
			}
			this.modalExamConduct.Show();
		}

		#endregion
	}
}