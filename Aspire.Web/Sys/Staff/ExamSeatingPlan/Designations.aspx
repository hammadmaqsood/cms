﻿<%@ Page MasterPageFile="~/Base.Master" Language="C#" AutoEventWireup="true" CodeBehind="Designations.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.Designations" %>

<asp:Content runat="server" ContentPlaceHolderID="BodyPH">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireButton runat="server" Text="Add Designation" Glyphicon="plus" ID="btnAddDesignation" OnClick="addDesignation_OnClick" />
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvExamDesignations" AllowSorting="True" AutoGenerateColumns="False" OnRowCommand="gvExamDesignations_OnRowCommand" OnSorting="gvExamDesignations_OnSorting">
		<Columns>
			<asp:BoundField HeaderText="Designation" DataField="Designation" SortExpression="Designation" />
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument='<%# Eval("ExamDesignationID") %>' />
					<aspire:AspireLinkButton runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%# Eval("ExamDesignationID") %>' ConfirmMessage="Are you sure you want to delete this exam designation?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalAddDesignation" HeaderText="Add Designation">
		<BodyTemplate>
			<asp:UpdatePanel runat="server">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertAddDesignation" />
					<asp:Panel runat="server" DefaultButton="bntInsertDesignation">
						<div class="form-horizontal">
							<div class="form-group">
								<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Designation:" AssociatedControlID="tbDesignation" />
								<div class="col-md-8">
									<aspire:AspireTextBox runat="server" ID="tbDesignation" ValidationGroup="Desixion" />
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbDesignation" ErrorMessage="This field is required." ValidationGroup="Desixion" />
								</div>
							</div>
						</div>
					</asp:Panel>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ValidationGroup="Desixion" ButtonType="Primary" Text="Add" ID="bntInsertDesignation" OnClick="btnInsertExamDesignation_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>

</asp:Content>

