﻿using Aspire.BL.Core.ExamSeatingPlan.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("BAEC9123-379A-4F3F-BFEB-AC2F2F34A948", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Execute.aspx", "Execute Exam Seating Plan", true, AspireModules.ExamSeatingPlan)]
	public partial class Execute : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module,new[] {UserGroupPermission.PermissionValues.Allowed, } },
		};
		public override PageIcon PageIcon => AspireGlyphicons.cog.GetIcon();
		public override string PageTitle => "Execute Exam Seating Plan";
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterID.FillSemesters();
				this.ddlExamType.FillExamConductExamTypes();
				this.ddlRoomUsageType.FillRoomUsageTypes();
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var examTypeEnum = this.ddlExamType.GetSelectedEnumValue<ExamConduct.ExamTypes>();
			BL.Core.ExamSeatingPlan.Staff.ExamConducts.GetExamConductsList(semesterID, examTypeEnum, this.StaffIdentity.LoginSessionGuid)
				.DataBind(this.ddlExamConductID);
			this.ddlExamConductID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamConductID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlRoomUsageType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlRoomUsageType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData();
		}

		protected void gv_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData();
		}

		private void GetData()
		{
			var examConductID = this.ddlExamConductID.SelectedValue.ToNullableInt();
			if (examConductID == null)
			{
				this.gv.DataBindNull();
				this.divExecute.Visible = false;
				this.btnClear.Visible = false;
				this.tbIssues.Text = null;
			}
			else
			{
				var roomUsageType = this.ddlRoomUsageType.GetSelectedEnumValue<ExamOfferedRoom.RoomUsageTypes>();
				var summary = ExecuteSeatingPlan.GetSummary(examConductID.Value, roomUsageType, this.StaffIdentity.LoginSessionGuid);
				{
					var dictionary = ExecuteSeatingPlan.ValidatePlan(examConductID.Value, out List<string> validationIssues, this.StaffIdentity.LoginSessionGuid);
					this.tbIssues.Text = string.Join("\n", validationIssues);
					foreach (var result in dictionary)
					{
						var row = summary.Find(s => s.ExamDateAndSessionID == result.Key);
						if (row != null)
						{
							row.Statuses = new List<string>();
							foreach (var status in result.Value)
							{
								switch (status.Key)
								{
									case ExecuteSeatingPlan.ValidatePlanStatuses.SameSeatAssignedToMultipleStudents:
										row.Statuses.Add($"Same Seat Assigned To Multiple Students: {status.Value}");
										break;
									case ExecuteSeatingPlan.ValidatePlanStatuses.MultipleSeatsAssignedToSameStudent:
										row.Statuses.Add($"Multiple Seats Assigned To Same Student: {status.Value}");
										break;
									case ExecuteSeatingPlan.ValidatePlanStatuses.SingleTypeRoomAssignedToStudentHavingClashes:
										row.Statuses.Add($"Single Type Room Assigned To Student Having Clashes: {status.Value}");
										break;
									case ExecuteSeatingPlan.ValidatePlanStatuses.MultipleTypeRoomAssignedToStudentHavingNoClash:
										row.Statuses.Add($"Multiple Type Room Assigned To Student Having No Clash: {status.Value}");
										break;
									default:
										throw new ArgumentOutOfRangeException();
								}
							}
						}
					}
				}

				this.gv.DataBind(summary);
				this.divExecute.Visible = summary.Any();
				this.btnClear.Visible = this.divExecute.Visible;
				if (this.divExecute.Visible)
				{
					this.liDeptWise.Visible = roomUsageType == ExamOfferedRoom.RoomUsageTypes.SinglePaper;
					this.liProgWise.Visible = roomUsageType == ExamOfferedRoom.RoomUsageTypes.SinglePaper;
				}
			}
		}

		protected void btnClear_OnClick(object sender, EventArgs e)
		{
			var examConductID = this.ddlExamConductID.SelectedValue.ToInt();
			var roomUsageTypeEnum = this.ddlRoomUsageType.GetSelectedEnumValue<ExamOfferedRoom.RoomUsageTypes>();
			var singlePaperResult = Aspire.BL.Core.ExamSeatingPlan.Staff.ExecuteSeatingPlan.ClearSeatingPlan(examConductID, roomUsageTypeEnum, this.StaffIdentity.LoginSessionGuid);
			switch (singlePaperResult)
			{
				case ExecuteSeatingPlan.ClearSeatingPlanResult.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.GetData();
					break;
				case ExecuteSeatingPlan.ClearSeatingPlanResult.Success:
					this.AddSuccessAlert("Exam Seating Plan has been cleared.");
					this.GetData();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private void ExecuteExamSeatingPlan(ExecuteSeatingPlan.ExecutionOrders executionOrder)
		{
			var examConductID = this.ddlExamConductID.SelectedValue.ToInt();
			var roomUsageTypeEnum = this.ddlRoomUsageType.GetSelectedEnumValue<ExamOfferedRoom.RoomUsageTypes>();
			var dictionary = Aspire.BL.Core.ExamSeatingPlan.Staff.ExecuteSeatingPlan.ExecutePlan(examConductID, roomUsageTypeEnum, executionOrder, this.StaffIdentity.LoginSessionGuid);

			var results = dictionary.Select(r => new { ExamDateAndSessionID = r.Key, r.Value }).GroupBy(r => r.Value).Select(g => new { g.Key, Count = g.Count() }).ToList();

			foreach (var result in results)
			{
				switch (result.Key)
				{
					case ExecuteSeatingPlan.ExecuteExamDateAndSessionResult.NoRecordFound:
						this.AddErrorAlert($"{result.Count} sessions record not found.");
						break;
					case ExecuteSeatingPlan.ExecuteExamDateAndSessionResult.AlreadyGenerated:
						this.AddWarningAlert($"{result.Count} sessions already executed.");
						break;
					case ExecuteSeatingPlan.ExecuteExamDateAndSessionResult.SeatsNotAvailable:
						this.AddErrorAlert($"Seats not available for {result.Count} sessions.");
						break;
					case ExecuteSeatingPlan.ExecuteExamDateAndSessionResult.Success:
						this.AddSuccessAlert($"{result.Count} sessions executed successfully.");
						break;
					case ExecuteSeatingPlan.ExecuteExamDateAndSessionResult.NoRegisteredCourseFound:
						this.AddWarningAlert($"{result.Count} No registered course found.");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			this.GetData();
		}

		protected void lbtnDepartmentWise_OnClick(object sender, EventArgs e)
		{
			this.ExecuteExamSeatingPlan(ExecuteSeatingPlan.ExecutionOrders.DepartmentWise);
		}

		protected void lbtnProgramWise_OnClick(object sender, EventArgs e)
		{
			this.ExecuteExamSeatingPlan(ExecuteSeatingPlan.ExecutionOrders.ProgramWise);
		}

		protected void lbtnRandom_OnClick(object sender, EventArgs e)
		{
			this.ExecuteExamSeatingPlan(ExecuteSeatingPlan.ExecutionOrders.Random);
		}

		protected void gv_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow || this.ddlRoomUsageType.GetSelectedEnumValue<ExamOfferedRoom.RoomUsageTypes>() != ExamOfferedRoom.RoomUsageTypes.SinglePaper)
				return;
			var summary = (ExecuteSeatingPlan.Summary)e.Row.DataItem;
			if (summary.Seats == 0)
				return;
			if (summary.RegisteredCourses == summary.Seats)
				e.Row.CssClass = "bg-success";
			if (summary.RegisteredCourses > summary.Seats)
				e.Row.CssClass = "bg-warning";
		}
	}
}