﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Execute.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.Execute" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Filter" ID="ddlSemesterID" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="ExamType:" AssociatedControlID="ddlExamType" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Filter" ID="ddlExamType" AutoPostBack="True" OnSelectedIndexChanged="ddlExamType_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Exam Conduct:" AssociatedControlID="ddlExamConductID" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Filter" ID="ddlExamConductID" AutoPostBack="True" OnSelectedIndexChanged="ddlExamConductID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlRoomUsageType" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Filter" ID="ddlRoomUsageType" AutoPostBack="True" OnSelectedIndexChanged="ddlRoomUsageType_OnSelectedIndexChanged" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gv" Stripped="False" AutoGenerateColumns="False" OnSorting="gv_OnSorting" OnRowDataBound="gv_OnRowDataBound">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%#: Container.DataItemIndex+1 %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Semester" DataField="Semester" />
			<asp:BoundField HeaderText="Date" DataField="Date" DataFormatString="{0:D}" />
			<asp:BoundField HeaderText="Session" DataField="SessionName" />
			<asp:BoundField HeaderText="Rooms" DataField="Rooms" />
			<asp:BoundField HeaderText="Total Seats" DataField="TotalSeats" />
			<asp:BoundField HeaderText="Registered Courses" DataField="RegisteredCourses" />
			<asp:BoundField HeaderText="Seats" DataField="Seats" />
			<asp:BoundField HeaderText="Free Seats" DataField="FreeSeats" />
			<asp:BoundField HeaderText="Status" DataField="StatusFullName" HtmlEncode="False" />
		</Columns>
	</aspire:AspireGridView>
	<p></p>
	<div class="text-center">
		<div class="btn-group dropup" id="divExecute" runat="server">
			<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
				Execute <span class="caret"></span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<li id="liDeptWise" runat="server">
					<aspire:AspireLinkButton CausesValidation="False" ConfirmMessage="Are you sure you want to execute?" runat="server" Text="Department-wise" ID="lbtnDepartmentWise" OnClick="lbtnDepartmentWise_OnClick" /></li>
				<li id="liProgWise" runat="server">
					<aspire:AspireLinkButton CausesValidation="False" ConfirmMessage="Are you sure you want to execute?" runat="server" Text="Program-wise" ID="lbtnProgramWise" OnClick="lbtnProgramWise_OnClick" /></li>
				<li>
					<aspire:AspireLinkButton CausesValidation="False" ConfirmMessage="Are you sure you want to execute?" runat="server" Text="Random" ID="lbtnRandom" OnClick="lbtnRandom_OnClick" /></li>
			</ul>
		</div>
		<aspire:AspireButton runat="server" ID="btnClear" ButtonType="Danger" Text="Clear" CausesValidation="False" ConfirmMessage="Are you sure you want to clear?" OnClick="btnClear_OnClick" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Validation Issues:" AssociatedControlID="tbIssues" />
		<aspire:AspireTextBox ID="tbIssues" runat="server" TextMode="MultiLine" MaxLength="1048576" Rows="10" ReadOnly="True" />
	</div>
</asp:Content>
