﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("0AC1F3F2-0970-47CB-A67A-FA42AD3A77DA", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/StudentExamSeats.aspx", "Student Exam Seating Plan", true, AspireModules.ExamSeatingPlan)]
	public partial class StudentExamSeats : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};
		public override PageIcon PageIcon => FontAwesomeIcons.solid_list.GetIcon();
		public override string PageTitle => "Exam Seating Plan";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var examSeatsFilterCustomListItems = BL.Core.ExamSeatingPlan.Common.ExamSeats.GetSemestersExamTypes(this.StaffIdentity.LoginSessionGuid);
				if (examSeatsFilterCustomListItems == null || !examSeatsFilterCustomListItems.Semesters.Any() || !examSeatsFilterCustomListItems.ExamTypes.Any())
				{
					this.AddErrorAlert("No exam seating plan record found.");
					Redirect<Dashboard>();
					return;
				}

				this.ddlSemesterIDFilter.DataBind(examSeatsFilterCustomListItems.Semesters);
				this.ddlExamTypeFilter.DataBind(examSeatsFilterCustomListItems.ExamTypes);
				this.tbEnrollment.Focus();
				this.studentExamSeats.Visible = false;
			}
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs args)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var examTypeEnum = this.ddlExamTypeFilter.GetSelectedEnumValue<ExamConduct.ExamTypes>();
			var enrollment = this.tbEnrollment.Enrollment;
			this.studentExamSeats.DisplayData(semesterID, examTypeEnum, enrollment);
		}
	}
}