﻿<%@ Page MasterPageFile="~/Base.Master" Language="C#" AutoEventWireup="true" CodeBehind="Rooms.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.Rooms" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadPH">
	<link href="../../../Scripts/Plugins/jquery-ui/jquery-ui.css" rel="stylesheet" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="BodyPH">
	<asp:ScriptManagerProxy runat="server">
		<Scripts>
			<asp:ScriptReference Name="jquery-ui" />
		</Scripts>
	</asp:ScriptManagerProxy>
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireButton runat="server" CausesValidation="False" ID="btnAddExamRoom" Text="Add Room" Glyphicon="plus" OnClick="btnAddExamRoom_OnClick" />
			<aspire:AspireHyperLinkButton runat="server" Text="Buildings" Glyphicon="tower" NavigateUrl="Buildings.aspx" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Building:" AssociatedControlID="ddlExamBuildingIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlExamBuildingIDFilter" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlExamBuildingIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatusFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlStatusFilter" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlStatusFilter_OnSelectedIndexChanged" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:Label runat="server" ID="lblActiveRoomsTotalSeats" />
	<asp:Repeater runat="server" ID="repeaterExamRooms">
		<HeaderTemplate>
			<div class="table-responsive">
				<table class="table table-bordered table-hover table-striped" id="<%# this.repeaterExamRooms.ClientID %>">
					<thead>
						<tr>
							<th></th>
							<th>#</th>
							<th>Building Name</th>
							<th>Room Name</th>
							<th>Rows</th>
							<th>Columns</th>
							<th>Total Seats</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
		</HeaderTemplate>
		<ItemTemplate>
			<tbody>
				<asp:Repeater runat="server" ID="innerRepeater" DataSource='<%# Eval("ExamRooms") %>' OnItemCommand="repeaterExamRooms_OnItemCommand">
					<ItemTemplate>
						<tr>
							<td><i class="handle fa fa-bars" title="Drag to change sequence" style="cursor: move"></i></td>
							<td><%#: Container.ItemIndex + 1 %></td>
							<td><%#: Eval("BuildingName") %></td>
							<td><%#: Eval("RoomName") %></td>
							<td><%#: Eval("Rows") %></td>
							<td><%#: Eval("Columns") %></td>
							<td><%#: Eval("TotalSeats") %></td>
							<td><%#: Eval("StatusFullName") %></td>
							<td>
								<aspire:HiddenField runat="server" Mode="Encrypted" Value='<%# Eval("ExamRoomID") %>' />
								<aspire:AspireLinkButton runat="server" CausesValidation="False" EncryptedCommandArgument='<%# Eval("ExamRoomID")%>' CommandName="Up" Glyphicon="chevron_up" />
								<aspire:AspireLinkButton runat="server" CausesValidation="False" EncryptedCommandArgument='<%# Eval("ExamRoomID")%>' CommandName="Down" Glyphicon="chevron_down" />
								<aspire:AspireLinkButton runat="server" CausesValidation="False" EncryptedCommandArgument='<%# Eval("ExamRoomID")%>' CommandName="Edit" Glyphicon="edit" />
								<aspire:AspireLinkButton runat="server" CausesValidation="False" EncryptedCommandArgument='<%# Eval("ExamRoomID")%>' CommandName="Delete" Glyphicon="remove" ConfirmMessage="Are you sure you want to delete?" />
							</td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
			</tbody>
		</ItemTemplate>
		<FooterTemplate>
			</table>
		</FooterTemplate>
	</asp:Repeater>
	<div class="text-center">
		<aspire:HiddenField runat="server" ID="hfSequenceState" Mode="PlainText" />
		<aspire:AspireButton runat="server" ButtonType="Primary" ID="btnUpdateSequence" Text="Update Sequence" CausesValidation="False" OnClick="btnUpdateSequence_OnClick" />
	</div>
	<aspire:AspireModal runat="server" ID="modalExamRooms" HeaderText="Add Exam Room">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelRooms">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertRoom" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Building Name:" AssociatedControlID="ddlExamBuildingID" />
							<div class="col-md-8">
								<aspire:AspireDropDownList runat="server" ID="ddlExamBuildingID" ValidationGroup="Rooms" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamBuildingID" ErrorMessage="This field is required." ValidationGroup="Rooms" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Room Name:" AssociatedControlID="tbRoomName" />
							<div class="col-md-8">
								<aspire:AspireTextBox MaxLength="100" runat="server" ID="tbRoomName" ValidationGroup="Rooms" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbRoomName" ErrorMessage="This field is required." ValidationGroup="Rooms" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Rows:" AssociatedControlID="tbRows" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" ID="tbRows" ValidationGroup="Rooms" />
								<aspire:AspireByteValidator runat="server" AllowNull="False" ControlToValidate="tbRows" InvalidNumberErrorMessage="Invalid Number." MinValue="1" InvalidRangeErrorMessage="Number must be in between {min} and {max}." RequiredErrorMessage="This field is required." ValidationGroup="Rooms" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Columns:" AssociatedControlID="tbColumns" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" ID="tbColumns" ValidationGroup="Rooms" />
								<aspire:AspireByteValidator runat="server" AllowNull="False" ControlToValidate="tbColumns" InvalidNumberErrorMessage="Invalid Number." MinValue="1" InvalidRangeErrorMessage="Number must be in between {min} and {max}." RequiredErrorMessage="This field is required." ValidationGroup="Rooms" />
							</div>
						</div>
						<div class="form-group Inline">
							<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Status:" AssociatedControlID="rblStatus" />
							<div class="col-md-9">
								<aspire:AspireRadioButtonList runat="server" ID="rblStatus" ValidationGroup="Rooms" RepeatDirection="Horizontal" RepeatLayout="Flow" />
								<br />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblStatus" ErrorMessage="This field is required." ValidationGroup="Rooms" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ButtonType="Primary" Text="Add" ID="btnSave" ValidationGroup="Rooms" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server" CausesValidation="False">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
	<script type="text/javascript">
		$(function () {
			var btnUpdateSequence = $("#<%=this.btnUpdateSequence.ClientID%>").hide();
			var table = $("#<%=this.repeaterExamRooms.ClientID%>");
			$("tbody", table).sortable({
				handle: ".handle",
				stop: function (event, ui) {
					$(btnUpdateSequence).show();
					var state = [];
					$("input:hidden", table).each(function (i, hf) {
						state.push($(hf).val());
					});
					$("#<%=this.hfSequenceState.ClientID%>").val(JSON.stringify(state));
				}
			});
		})
	</script>
</asp:Content>
