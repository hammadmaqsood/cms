﻿using Aspire.BL.Common;
using Aspire.BL.Core.ExamSeatingPlan.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("7389C860-F771-4F67-91CD-640AEB81A068", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/InvigilatorsDuties.aspx", "Invigilators Duties", true, AspireModules.ExamSeatingPlan)]
	public partial class InvigilatorsDuties : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.user);
		public override string PageTitle => "Invigilators Duties";
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(nameof(ExamInvigilatorDuties.GetExamInvigilatorDutiesResult.ExamInvigilatorDuty.Sequence), SortDirection.Ascending);
				this.ddlSemesterIDFilter.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
				this.ddlExamTypeFilter.FillExamConductExamTypes().SetSelectedValueIfNotPostback("ExamType");
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}
		public static string GetPageUrl(short? semesterID, byte? examType, int? examConductID)
		{
			return GetAspirePageAttribute<ExamSeatingPlan.InvigilatorsDuties>().PageUrl.AttachQueryParams("SemesterID", semesterID, "ExamType", examType, "ExamConductID", examConductID);
		}

		private void RefreshPage()
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var examType = this.ddlExamTypeFilter.SelectedValue.ToNullableByte();
			var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToNullableInt();
			this.Response.Redirect(GetPageUrl(semesterID, examType, examConductID));
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamTypeFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamTypeFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var examTypeEnum = this.ddlExamTypeFilter.GetSelectedEnumValue<ExamConduct.ExamTypes>();
			if (semesterID == null)
				this.ddlExamConductIDFilter.Items.Clear();
			else
			{
				var examConducts = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamConducts.GetExamConductsList(semesterID.Value, examTypeEnum, this.StaffIdentity.LoginSessionGuid);
				this.ddlExamConductIDFilter.DataBind(examConducts);
			}
			this.ddlExamConductIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamConductIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var examConductID = this.ddlExamConductIDFilter.SelectedValue.ToNullableInt();
			if (examConductID != null)
			{
				var examDateAndSessions = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamDateAndSessions.GetExamDateAndSessionsList(examConductID.Value, this.StaffIdentity.LoginSessionGuid);
				this.ddlExamDateAndSessionIDFilter.DataBind(examDateAndSessions);
			}
			else
				this.ddlExamDateAndSessionIDFilter.Items.Clear();
			this.ddlExamDateAndSessionIDFilter_OnSelectedIndexChanged(null, null);
		}

		private List<int> RecentlyUnAssignedInvigilatorIDs
		{
			get { return this.ViewState.GetValue("RecentlyUnAssigned", new List<int>()); }
			set { this.ViewState.SetValue("RecentlyUnAssigned", value); }
		}

		protected void ddlExamDateAndSessionIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData();
		}

		private void GetData()
		{
			this.ddlExamInvigilatorIDRecent.Items.Clear();
			this.ddlExamInvigilatorID.Items.Clear();
			this.modalAssignInvigilator.Visible = false;
			this.btnAutoAssign.Visible = false;
			var examDateAndSessionID = this.ddlExamDateAndSessionIDFilter.SelectedValue.ToNullableInt();
			if (examDateAndSessionID == null)
				this.gvInvigilatorsDuties.DataBindNull();
			else
			{
				this.btnAutoAssign.Visible = true;
				this.btnAutoAssign.Enabled = false;
				var examInvigilatorDuties = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamInvigilatorDuties.GetExamInvigilatorDuties(examDateAndSessionID.Value, this.StaffIdentity.LoginSessionGuid);
				var sortExpression = this.ViewState.GetSortExpression();
				var sortDirection = this.ViewState.GetSortDirection();
				var duties = examInvigilatorDuties.ExamInvigilatorDuties;
				switch (sortExpression)
				{
					case nameof(ExamInvigilatorDuties.GetExamInvigilatorDutiesResult.ExamInvigilatorDuty.Sequence):
						duties = duties.OrderBy(sortDirection, d => d.Sequence).ToList();
						break;
					case nameof(ExamInvigilatorDuties.GetExamInvigilatorDutiesResult.ExamInvigilatorDuty.BuildingName):
						duties = duties.OrderBy(sortDirection, d => d.BuildingName).ToList();
						break;
					case nameof(ExamInvigilatorDuties.GetExamInvigilatorDutiesResult.ExamInvigilatorDuty.RoomName):
						duties = duties.OrderBy(sortDirection, d => d.RoomName).ToList();
						break;
					case nameof(ExamInvigilatorDuties.GetExamInvigilatorDutiesResult.ExamInvigilatorDuty.RoomUsageTypeFullName):
						duties = duties.OrderBy(sortDirection, d => d.RoomUsageTypeFullName).ToList();
						break;
					case nameof(ExamInvigilatorDuties.GetExamInvigilatorDutiesResult.ExamInvigilatorDuty.Students):
						duties = duties.OrderBy(sortDirection, d => d.Students).ThenBy(sortDirection, d => d.TotalSeats).ToList();
						break;
					case nameof(ExamInvigilatorDuties.GetExamInvigilatorDutiesResult.ExamInvigilatorDuty.ExamInvigilatorOneName):
						duties = duties.OrderBy(sortDirection, d => d.ExamInvigilatorOneName).ToList();
						break;
					case nameof(ExamInvigilatorDuties.GetExamInvigilatorDutiesResult.ExamInvigilatorDuty.ExamInvigilatorTwoName):
						duties = duties.OrderBy(sortDirection, d => d.ExamInvigilatorTwoName).ToList();
						break;
					default:
						throw new SortingNotImplementedException(sortExpression);
				}
				this.gvInvigilatorsDuties.DataBind(duties, sortExpression, sortDirection);
				if (examInvigilatorDuties.ExamInvigilatorDuties.Any())
				{
					var invigilators = examInvigilatorDuties.ExamInvigilators.OrderBy(i => i.Name).Select(i => new
					{
						i.ExamInvigilatorID,
						i.Available,
						Name = $"{i.Name}{(i.Available ? null : " (Not Available)")}",
					});

					var recent = this.RecentlyUnAssignedInvigilatorIDs
										   .Select(id => invigilators.SingleOrDefault(i => i.ExamInvigilatorID == id))
										   .Where(i => i != null)
										   .Select(i => new ListItem(i.Name, i.ExamInvigilatorID.ToString()));

					this.ddlExamInvigilatorIDRecent.DataBind(recent, CommonListItems.Select);

					invigilators.OrderByDescending(i => i.Available).ThenBy(i => i.Name).ForEach(i => this.ddlExamInvigilatorID.AddItem(i.Name, i.ExamInvigilatorID.ToString(), "Not Assigned"));
					this.modalAssignInvigilator.Visible = true;
				}
				this.btnAutoAssign.Enabled = !examInvigilatorDuties.ExamInvigilatorDuties.Any(d => d.ExamInvigilators.Any());
			}
		}

		protected void btnAssign_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			this.StaffIdentity.Demand(StaffPermissions.ExamSeatingPlan.ManageExamInvigilatorDuties, UserGroupPermission.PermissionValues.Allowed);
			var examOfferedRoomID = this.hfExamOfferedRoomID.Value.ToInt();
			var examDateAndSessionID = this.ddlExamDateAndSessionIDFilter.SelectedValue.ToInt();
			var invigilatorType = this.hfInvigilatorType.Value == "Invigilator 1" ? ExamInvigilatorDuty.InvigilatorTypes.InvigilatorOne : ExamInvigilatorDuty.InvigilatorTypes.InvigilatorTwo;
			var examInvigilatorID = this.ddlExamInvigilatorIDRecent.SelectedValue.ToNullableInt() ?? this.ddlExamInvigilatorID.SelectedValue.ToNullableInt();
			var status = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamInvigilatorDuties.AddExamInvigilatorDuty(examOfferedRoomID, examDateAndSessionID, examInvigilatorID.Value, invigilatorType, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case ExamInvigilatorDuties.AddExamInvigilatorDutyStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.GetData();
					return;
				case ExamInvigilatorDuties.AddExamInvigilatorDutyStatuses.Success:
					this.AddSuccessAlert("Duty has been assigned.");
					this.GetData();
					return;
				case ExamInvigilatorDuties.AddExamInvigilatorDutyStatuses.DutyAlreadyAssignedToInvigilator:
					this.AddErrorAlert("Duty has already been assigned to Invigilator.");
					this.GetData();
					return;
				case ExamInvigilatorDuties.AddExamInvigilatorDutyStatuses.DutyAlreadyAssignedInRoom:
					this.AddErrorAlert("Duty has already been assigned in the room.");
					this.GetData();
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void gvInvigilatorsDuties_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					this.StaffIdentity.Demand(StaffPermissions.ExamSeatingPlan.ManageExamInvigilatorDuties, UserGroupPermission.PermissionValues.Allowed);
					var examInvigilatorID = e.DecryptedCommandArgumentToInt();
					var examDateAndSessionID = this.ddlExamDateAndSessionIDFilter.SelectedValue.ToInt();
					var status = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamInvigilatorDuties.DeleteExamInvigilatorDuty(examInvigilatorID, examDateAndSessionID, this.StaffIdentity.LoginSessionGuid);
					switch (status)
					{
						case ExamInvigilatorDuties.DeleteExamInvigilatorDutyStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.GetData();
							return;
						case ExamInvigilatorDuties.DeleteExamInvigilatorDutyStatuses.Success:
							this.AddSuccessAlert("Invigilator has been unassigned.");
							var recent = this.RecentlyUnAssignedInvigilatorIDs;
							recent.Insert(0, examInvigilatorID);
							this.RecentlyUnAssignedInvigilatorIDs = recent;
							this.GetData();
							return;
						case ExamInvigilatorDuties.DeleteExamInvigilatorDutyStatuses.CannotDeleteAttendanceIsPresent:
							this.AddErrorAlert("Invigilator cannot be unassigned because its attendance has been marked as present.");
							this.GetData();
							return;
						default:
							throw new ArgumentOutOfRangeException();
					}
			}
		}

		protected void btnAutoAssign_OnClick(object sender, EventArgs e)
		{
			this.StaffIdentity.Demand(StaffPermissions.ExamSeatingPlan.ManageExamInvigilatorDuties, UserGroupPermission.PermissionValues.Allowed);
			var examDateAndSessionID = this.ddlExamDateAndSessionIDFilter.SelectedValue.ToInt();
			var status = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamInvigilatorDuties.AutoAssignAllDuties(examDateAndSessionID, out var assignedDutiesCount, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case ExamInvigilatorDuties.AutoAssignAllDutiesStatus.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.GetData();
					return;
				case ExamInvigilatorDuties.AutoAssignAllDutiesStatus.InvigilatorsNotAvailable:
					this.AddWarningAlert("No more invigilators are available.");
					switch (assignedDutiesCount)
					{
						case 0:
							this.AddSuccessAlert($"No duty has been assigned.");
							break;
						case 1:
							this.AddSuccessAlert($"1 duty has been assigned.");
							break;
						default:
							this.AddSuccessAlert($"{assignedDutiesCount} duties have been assigned.");
							break;
					}
					this.GetData();
					return;
				case ExamInvigilatorDuties.AutoAssignAllDutiesStatus.Success:
					switch (assignedDutiesCount)
					{
						case 0:
							this.AddSuccessAlert($"No duty has been assigned.");
							break;
						case 1:
							this.AddSuccessAlert($"1 duty has been assigned.");
							break;
						default:
							this.AddSuccessAlert($"{assignedDutiesCount} duties have been assigned.");
							break;
					}
					this.GetData();
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void btnClearAll_OnClick(object sender, EventArgs e)
		{
			this.StaffIdentity.Demand(StaffPermissions.ExamSeatingPlan.ManageExamInvigilatorDuties, UserGroupPermission.PermissionValues.Allowed);
			var examDateAndSessionID = this.ddlExamDateAndSessionIDFilter.SelectedValue.ToInt();
			var result = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamInvigilatorDuties.ClearAllAssignedDuties(examDateAndSessionID, this.StaffIdentity.LoginSessionGuid);
			foreach (var r in result)
			{
				switch (r.Key)
				{
					case ExamInvigilatorDuties.ClearAllAssignedDutiesStatuses.AttendanceMarked:
						this.AddWarningAlert($"{r.Value} duties cannot be cleared because attendance has been marked.");
						break;
					case ExamInvigilatorDuties.ClearAllAssignedDutiesStatuses.Success:
						this.AddSuccessAlert($"{r.Value} duties have been cleared.");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			this.GetData();
		}

		protected void gvInvigilatorsDuties_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData();
		}
	}
}