﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="StudentExamSeats.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.StudentExamSeats" %>

<%@ Register Src="~/Sys/Common/ExamSeatingPlan/StudentExamSeats.ascx" TagPrefix="uc1" TagName="StudentExamSeats" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Exam Type:" AssociatedControlID="ddlExamTypeFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlExamTypeFilter" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
			<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" OnSearch="tbEnrollment_OnSearch" />
		</div>
	</div>
	<uc1:StudentExamSeats runat="server" ID="studentExamSeats" />
</asp:Content>
