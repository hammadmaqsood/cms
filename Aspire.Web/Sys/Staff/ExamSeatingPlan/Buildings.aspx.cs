﻿using Aspire.BL.Core.ExamSeatingPlan.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("5EC9A3D0-9F44-405D-B41C-797498D8FAA8", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Buildings.aspx", "Buildings", true, AspireModules.ExamSeatingPlan)]
	public partial class Buildings : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.tower);
		public override string PageTitle => "Exam Buildings";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.GetData();
			}
		}

		protected void btnAdd_OnClick(object sender, EventArgs e)
		{
			this.DisplayExamBuildingModal(null);
		}

		protected void gvExamBuildings_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Status":
					var args = e.DecryptedCommandArgument().Split(':');
					var examBuildingID = args[0].ToInt();
					var statusEnum = args[1].ToEnum<ExamBuilding.Statuses>();
					ExamBuildings.UpdateExamBuildingStatusResults result;
					switch (statusEnum)
					{
						case ExamBuilding.Statuses.Active:
							result = BL.Core.ExamSeatingPlan.Staff.ExamBuildings.UpdateExamBuildingStatus(examBuildingID, ExamBuilding.Statuses.Inactive, this.StaffIdentity.LoginSessionGuid);
							break;
						case ExamBuilding.Statuses.Inactive:
							result = BL.Core.ExamSeatingPlan.Staff.ExamBuildings.UpdateExamBuildingStatus(examBuildingID, ExamBuilding.Statuses.Active, this.StaffIdentity.LoginSessionGuid);
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					switch (result)
					{
						case ExamBuildings.UpdateExamBuildingStatusResults.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamBuildings.UpdateExamBuildingStatusResults.Success:
							this.AddSuccessAlert("Exam Building's Status has been updated.");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.GetData();
					break;
				case "Edit":
					e.Handled = true;
					this.DisplayExamBuildingModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var status = BL.Core.ExamSeatingPlan.Staff.ExamBuildings.DeleteExamBuilding(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					switch (status)
					{
						case ExamBuildings.DeleteExamBuildingStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamBuildings.DeleteExamBuildingStatuses.CannotDeleteChildRecordExistsExamRooms:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Exam Building");
							break;
						case ExamBuildings.DeleteExamBuildingStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Exam Building");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					Redirect<Buildings>();
					break;
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;

			var examBuildingID = (int?)this.ViewState["ExamBuildingID"];
			var buildingName = this.tbBuildingName.Text;
			var statusEnum = this.rblStatus.GetSelectedEnumValue<ExamBuilding.Statuses>();
			if (examBuildingID == null)
			{
				var status = BL.Core.ExamSeatingPlan.Staff.ExamBuildings.AddExamBuilding(buildingName, statusEnum, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case ExamBuildings.AddExamBuildingStatuses.NameAlreadyExists:
						this.alertBuilding.AddErrorAlert("Exam Building Name already exists.");
						this.updatePanelBuilding.Update();
						return;
					case ExamBuildings.AddExamBuildingStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Exam Building");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				Redirect<Buildings>();
			}
			else
			{
				var status = BL.Core.ExamSeatingPlan.Staff.ExamBuildings.UpdateExamBuilding(examBuildingID.Value, buildingName, statusEnum, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case ExamBuildings.UpdateExamBuildingStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<Buildings>();
						return;
					case ExamBuildings.UpdateExamBuildingStatuses.NameAlreadyExists:
						this.alertBuilding.AddErrorAlert("Exam Building Name already exists.");
						this.updatePanelBuilding.Update();
						return;
					case ExamBuildings.UpdateExamBuildingStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Exam Building");
						Redirect<Buildings>();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		private void DisplayExamBuildingModal(int? examBuildingID)
		{
			if (this.rblStatus.Items.Count == 0)
				this.rblStatus.FillEnums<ExamBuilding.Statuses>();
			if (examBuildingID == null)
			{
				this.tbBuildingName.Text = null;
				this.rblStatus.SetEnumValue(ExamBuilding.Statuses.Active);

				this.modalExamBuildings.HeaderText = "Add Exam Building";
				this.btnSave.Text = "Add";
				this.ViewState["ExamBuildingID"] = null;
				this.modalExamBuildings.Show();
			}
			else
			{
				var examBuilding = BL.Core.ExamSeatingPlan.Staff.ExamBuildings.GetExamBuilding(examBuildingID.Value, this.StaffIdentity.LoginSessionGuid);
				if (examBuilding == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Buildings>();
					return;
				}
				this.tbBuildingName.Text = examBuilding.BuildingName;
				this.rblStatus.SetEnumValue(examBuilding.StatusEnum);

				this.modalExamBuildings.HeaderText = "Edit Exam Building";
				this.btnSave.Text = "Save";
				this.ViewState["ExamBuildingID"] = examBuildingID;
				this.modalExamBuildings.Show();
			}
		}

		private void GetData()
		{
			var examBuildings = BL.Core.ExamSeatingPlan.Staff.ExamBuildings.GetExamBuildings(this.StaffIdentity.LoginSessionGuid);
			this.gvExamBuildings.DataBind(examBuildings);
		}
	}
}