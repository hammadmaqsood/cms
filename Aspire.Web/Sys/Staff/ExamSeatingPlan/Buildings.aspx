﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Buildings.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ExamSeatingPlan.Buildings" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireButton CausesValidation="False" runat="server" ID="btnAdd" Text="Add Exam Building" OnClick="btnAdd_OnClick" Glyphicon="plus" />
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvExamBuildings" AutoGenerateColumns="False" OnRowCommand="gvExamBuildings_OnRowCommand">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%#: Container.DataItemIndex+1 %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Building Name" DataField="BuildingName" />
			<asp:TemplateField HeaderText="Status">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Status" EncryptedCommandArgument='<%# Eval("ExamBuildingID")+":"+ Eval("Status")%>' Text='<%# this.Eval("StatusFullName") %>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireLinkButton CausesValidation="False" runat="server" CommandName="Edit" EncryptedCommandArgument='<%# Eval("ExamBuildingID")%>' Glyphicon="edit" />
					<aspire:AspireLinkButton CausesValidation="False" runat="server" CommandName="Delete" EncryptedCommandArgument='<%# Eval("ExamBuildingID")%>' Glyphicon="remove" ConfirmMessage='<%# this.Eval("BuildingName","Are you sure you want to delete exam building \"{0}\"?") %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalExamBuildings">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updatePanelBuilding">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertBuilding" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Building Name:" AssociatedControlID="tbBuildingName" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox MaxLength="100" runat="server" ID="tbBuildingName" ValidationGroup="ExamBuilding" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbBuildingName" ErrorMessage="This field is required." ValidationGroup="ExamBuilding" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="rblStatus" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireRadioButtonList runat="server" ID="rblStatus" RepeatDirection="Horizontal" RepeatLayout="Flow" />
								<br />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblStatus" ErrorMessage="This field is required." ValidationGroup="ExamBuilding" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" OnClick="btnSave_OnClick" ValidationGroup="ExamBuilding"></aspire:AspireButton>
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
