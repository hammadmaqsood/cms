﻿using Aspire.BL.Core.ExamSeatingPlan.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("77C62F36-39B9-4241-9650-61C43F243886", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Invigilators.aspx", "Invigilators", true, AspireModules.ExamSeatingPlan)]
	public partial class Invigilators : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ExamSeatingPlan.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.user);
		public override string PageTitle => "Invigilators";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Name", SortDirection.Ascending);
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All).SetSelectedValueIfNotPostback("DepartmentID");
				var designations = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamDesignations.GetExamDesignationsList(this.StaffIdentity.LoginSessionGuid);
				if (designations.Any())
					this.ddlExamDesignationIDFilter.DataBind(designations, CommonListItems.All).SetSelectedValueIfNotPostback("ExamDesignationID");
				else
				{
					this.AddErrorAlert("No designations found for the selected institute.");
					Redirect<Designations>();
					return;
				}
				this.tbSearchText.Text = this.Request.GetParameterValue("SearchText");
				this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamDesignationIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamDesignationIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearch_Click(null, null);
		}
		protected void btnSearch_Click(object sender, EventArgs e)
		{
			this.GetExamInvigilators(0);
		}

		private void GetExamInvigilators(int pageIndex)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var designationID = this.ddlExamDesignationIDFilter.SelectedValue.ToNullableInt();
			var searchText = this.tbSearchText.Text;
			var result = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamInvigilators.GetExamInvigilators(departmentID, designationID, searchText, pageIndex, this.gvExamInvigilators.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.StaffIdentity.LoginSessionGuid);
			this.gvExamInvigilators.DataBind(result.ExamInvigilators, pageIndex, result.VirtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		protected void btnAddInvigilator_OnClick(object sender, EventArgs e)
		{
			this.ShowExamInvigilatorModal(null);
		}

		protected void gvExamInvigilators_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Status":
					var result = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamInvigilators.ToggleInvigilatorStatus(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					switch (result)
					{
						case ExamInvigilators.ToggleInvigilatorStatusResult.Success:
							this.AddSuccessMessageHasBeenUpdated("Invigilator's status");
							break;
						case ExamInvigilators.ToggleInvigilatorStatusResult.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.GetExamInvigilators(0);
					break;
				case "Edit":
					e.Handled = true;
					this.ShowExamInvigilatorModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var deletedStatus = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamInvigilators.DeleteExamInvigilator(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					switch (deletedStatus)
					{
						case ExamInvigilators.DeleteExamInvigilatorStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Exam Invigilator");
							break;
						case ExamInvigilators.DeleteExamInvigilatorStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamInvigilators.DeleteExamInvigilatorStatuses.CannotDeleteChildRecordExistsExamInvigilatorDuties:
						case ExamInvigilators.DeleteExamInvigilatorStatuses.CannotDeleteChildRecordExistsExamInvigilatorExamSessions:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Exam Invigilator");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.RefreshPage();
					break;
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var designationID = this.ddlExamDesignationID.SelectedValue.ToInt();
			var name = this.tbInvigilatorName.Text;
			var cnic = this.tbCnic.Text;
			var bank = this.tbBank.Text;
			var accountNumber = this.tbAccountNumber.Text;
			var status = this.rblStatus.GetSelectedEnumValue<ExamInvigilator.Statuses>();
			var examInvigilatorID = (int?)this.ViewState[nameof(ExamInvigilator.ExamInvigilatorID)];
			if (examInvigilatorID == null)
			{
				var added = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamInvigilators.AddExamInvigilator(departmentID, designationID, name, cnic, bank, accountNumber, status, this.StaffIdentity.LoginSessionGuid);
				switch (added)
				{
					case ExamInvigilators.AddExamInvigilatorStatuses.CNICAlreadyExists:
						this.alertAddInvigilator.AddErrorAlert("CNIC already exists.");
						this.updatePanelInvigilator.Update();
						return;
					case ExamInvigilators.AddExamInvigilatorStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Invigilator");
						this.RefreshPage();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var updated = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamInvigilators.UpdateExamInvigilator(examInvigilatorID.Value, departmentID, designationID, name, cnic, bank, accountNumber, status, this.StaffIdentity.LoginSessionGuid);
				switch (updated)
				{
					case ExamInvigilators.UpdateExamInvigilatorStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Exam Invigilator");
						this.RefreshPage();
						break;
					case ExamInvigilators.UpdateExamInvigilatorStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						break;
					case ExamInvigilators.UpdateExamInvigilatorStatuses.CNICAlreadyExists:
						this.alertAddInvigilator.AddErrorAlert("CNIC already exists.");
						this.updatePanelInvigilator.Update();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		private void RefreshPage()
		{
			Redirect<Invigilators>("DepartmentID", this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt(), "ExamDesignationID", this.ddlExamDesignationIDFilter.SelectedValue.ToNullableInt(), "SearchText", this.tbSearchText.Text);
		}

		private void ShowExamInvigilatorModal(int? examInvigilatorID)
		{
			this.ddlDepartmentID.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.Select);
			var designations = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamDesignations.GetExamDesignationsList(this.StaffIdentity.LoginSessionGuid);
			if (designations.Any())
				this.ddlExamDesignationID.DataBind(designations);
			else
			{
				Redirect<Invigilators>();
				return;
			}
			this.rblStatus.FillEnums<ExamInvigilator.Statuses>();
			if (examInvigilatorID != null)
			{
				var invigilator = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamInvigilators.GetExamInvigilator(examInvigilatorID.Value, this.StaffIdentity.LoginSessionGuid);
				this.ddlDepartmentID.SelectedValue = invigilator.DepartmentID.ToString();
				this.ddlExamDesignationID.SelectedValue = invigilator.ExamDesignationID.ToString();
				this.tbInvigilatorName.Text = invigilator.Name;
				this.tbCnic.Text = invigilator.CNIC;
				this.tbBank.Text = invigilator.Bank;
				this.tbAccountNumber.Text = invigilator.AccountNo;
				this.rblStatus.SetEnumValue(invigilator.StatusEnum);
				this.modalAddInvigilator.HeaderText = "Edit Invigilator";
				this.btnSave.Text = @"Save";
			}
			else
			{
				this.ddlDepartmentID.ClearSelection();
				this.ddlExamDesignationID.ClearSelection();
				this.tbInvigilatorName.Text = null;
				this.tbCnic.Text = null;
				this.tbBank.Text = null;
				this.tbAccountNumber.Text = null;
				this.rblStatus.SelectedIndex = 0;
				this.modalAddInvigilator.HeaderText = "Add Invigilator";
				this.btnSave.Text = @"Add";
			}
			this.ViewState[nameof(ExamInvigilator.ExamInvigilatorID)] = examInvigilatorID;
			this.modalAddInvigilator.Show();
		}

		protected void gvExamInvigilators_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetExamInvigilators(0);
		}

		protected void gvExamInvigilators_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetExamInvigilators(e.NewPageIndex);
		}

		protected void gvExamInvigilators_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvExamInvigilators.PageSize = e.NewPageSize;
			this.GetExamInvigilators(0);
		}
	}
}