﻿using Aspire.BL.Core.ExamSeatingPlan.Staff;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.ExamSeatingPlan
{
	[AspirePage("6AAA83A1-D3D0-4552-A51F-30137948DA1B", UserTypes.Staff, "~/Sys/Staff/ExamSeatingPlan/Designations.aspx", "Designations", true, AspireModules.ExamSeatingPlan)]
	public partial class Designations : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.ExamSeatingPlan.Module, new []{UserGroupPermission.PermissionValues.Allowed, } }
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Designations";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(nameof(ExamDesignation.Designation), SortDirection.Ascending);
				this.GetDesignations();
			}
		}

		private void GetDesignations()
		{
			var designations = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamDesignations.GetExamDesignations(this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.StaffIdentity.LoginSessionGuid);
			this.gvExamDesignations.DataBind(designations, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		protected void btnInsertExamDesignation_OnClick(object sender, EventArgs e)
		{
			if (!IsValid)
				return;

			this.StaffIdentity.Demand(StaffPermissions.ExamSeatingPlan.ManageExamDesignations, UserGroupPermission.PermissionValues.Allowed);
			var examDesignationID = (int?)this.ViewState["ExamDesignationID"];
			var designation = this.tbDesignation.Text;
			if (examDesignationID == null)
			{
				var result = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamDesignations.AddExamDesignation(designation, this.StaffIdentity.LoginSessionGuid);
				switch (result.Status)
				{
					case ExamDesignations.AddExamDesignationResult.Statuses.NameAlreadyExists:
						this.alertAddDesignation.AddWarningAlert("Designation already exists.");
						break;
					case ExamDesignations.AddExamDesignationResult.Statuses.Success:
						this.AddSuccessMessageHasBeenAdded("Designation");
						Redirect<Designations>();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var status = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamDesignations.UpdateExamDesignation(examDesignationID.Value, designation, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case ExamDesignations.UpdateExamDesignationStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Designation");
						Redirect<Designations>();
						break;
					case ExamDesignations.UpdateExamDesignationStatuses.RecordNotFound:
						this.AddNoRecordFoundAlert();
						Redirect<Designations>();
						break;
					case ExamDesignations.UpdateExamDesignationStatuses.ChildRecordsExistExamInvigilators:
						this.alertAddDesignation.AddErrorMessageCannotBeUpdatedBecauseChildRecordExists("Designation");
						break;
					case ExamDesignations.UpdateExamDesignationStatuses.NameAlreadyExists:
						this.AddErrorAlert("Designation already exists.");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		protected void addDesignation_OnClick(object sender, EventArgs e)
		{
			this.ShowExamDesignationModal(null);
		}

		#region Gridview Events

		protected void gvExamDesignations_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetDesignations();
		}

		protected void gvExamDesignations_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.ShowExamDesignationModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					this.StaffIdentity.Demand(StaffPermissions.ExamSeatingPlan.ManageExamDesignations, UserGroupPermission.PermissionValues.Allowed);
					var deleted = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamDesignations.DeleteExamDesignation(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					switch (deleted)
					{
						case ExamDesignations.DeleteExamDesignationStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Designation");
							break;
						case ExamDesignations.DeleteExamDesignationStatuses.RecordNotFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamDesignations.DeleteExamDesignationStatuses.ChildRecordsExistExamInvigilators:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Designation");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					Redirect<Designations>();
					break;
			}
		}

		#endregion

		private void ShowExamDesignationModal(int? examDesignationID)
		{
			this.StaffIdentity.Demand(StaffPermissions.ExamSeatingPlan.ManageExamDesignations, UserGroupPermission.PermissionValues.Allowed);
			if (examDesignationID != null)
			{
				var examDesignation = Aspire.BL.Core.ExamSeatingPlan.Staff.ExamDesignations.GetExamDesignation(examDesignationID.Value, this.StaffIdentity.LoginSessionGuid);
				if (examDesignation == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Designations>();
					return;
				}
				this.tbDesignation.Text = examDesignation.Designation;
				this.modalAddDesignation.HeaderText = "Edit Designation";
				this.bntInsertDesignation.Text = @"Save";
				this.ViewState["ExamDesignationID"] = examDesignation.ExamDesignationID;
			}
			else
			{
				this.tbDesignation.Text = null;
				this.modalAddDesignation.HeaderText = "Add Designation";
				this.bntInsertDesignation.Text = @"Add";
				this.ViewState["ExamDesignationID"] = null;
			}
			this.modalAddDesignation.Show();
		}
	}
}