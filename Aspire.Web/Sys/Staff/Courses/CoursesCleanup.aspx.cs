﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Courses
{
	[AspirePage("F18F84E1-34D1-4011-984C-A7A0976E5EFD", UserTypes.Staff, "~/Sys/Staff/Courses/CoursesCleanup.aspx", "Courses Cleanup", true, AspireModules.Courses)]
	public partial class CoursesCleanup : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Courses.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.book);
		public override string PageTitle => "Courses Cleanup";

		private short? IntakeSemesterID => this.GetParameterValue<short>(nameof(this.IntakeSemesterID));
		private int? DepartmentID => this.GetParameterValue<int>(nameof(this.DepartmentID));
		private int? ProgramID => this.GetParameterValue<int>(nameof(this.ProgramID));
		private SemesterNos? SemesterNo => this.GetParameterValue<SemesterNos>(nameof(this.SemesterNo));
		private Cours.Statuses? Status => this.GetParameterValue<Cours.Statuses>(nameof(this.Status));
		private int? PageIndex => this.GetParameterValue<int>(nameof(this.PageIndex));
		private int? PageSize => this.GetParameterValue<int>(nameof(this.PageSize));
		private string SortExpression => this.GetParameterValue(nameof(this.SortExpression));
		private SortDirection? SortDirection => this.GetParameterValue<SortDirection>(nameof(this.SortDirection));
		private string SearchText => this.GetParameterValue(nameof(this.SearchText));

		public static string GetPageUrl(short? intakeSemesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Cours.Statuses? statusEnum, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection)
		{
			return GetPageUrl<CoursesCleanup>().AttachQueryParams(
				(nameof(IntakeSemesterID), intakeSemesterID),
				(nameof(DepartmentID), departmentID),
				(nameof(ProgramID), programID),
				(nameof(SemesterNo), semesterNoEnum),
				(nameof(Status), statusEnum),
				(nameof(SearchText), searchText),
				(nameof(PageIndex), pageIndex),
				(nameof(PageSize), pageSize),
				(nameof(SortExpression), sortExpression),
				(nameof(SortDirection), sortDirection));
		}

		public static void Redirect(short? intakeSemesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Cours.Statuses? statusEnum, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection)
		{
			Redirect(GetPageUrl(intakeSemesterID, departmentID, programID, semesterNoEnum, statusEnum, searchText, pageIndex, pageSize, sortExpression, sortDirection));
		}

		private void RefreshPage()
		{
			var intakeSemesterID = this.ddlIntakeSemesterIDFilter.SelectedValue.ToNullableShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var semesterNoEnum = this.ddlSemesterNoFilter.GetSelectedEnumValue<SemesterNos>(null);
			var statusEnum = this.ddlStatusFilter.GetSelectedNullableGuidEnum<Cours.Statuses>();
			var searchText = this.tbSearchTextFilters.Text;
			Redirect(intakeSemesterID, departmentID, programID, semesterNoEnum, statusEnum, searchText, this.gvCourses.PageIndex, this.gvCourses.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(this.SortExpression ?? nameof(Cours.CourseCode), this.SortDirection ?? System.Web.UI.WebControls.SortDirection.Ascending);
				this.ddlIntakeSemesterIDFilter.FillSemesters();
				if (this.IntakeSemesterID != null)
					this.ddlIntakeSemesterIDFilter.SelectedValue = this.IntakeSemesterID.Value.ToString();
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All);
				if (this.DepartmentID != null)
					this.ddlDepartmentIDFilter.SelectedValue = this.DepartmentID.Value.ToString();
				if (this.SearchText != null)
					this.tbSearchTextFilters.Text = this.SearchText;
				this.ddlSemesterNoFilter.FillSemesterNos(CommonListItems.All);
				if (this.SemesterNo != null)
					this.ddlSemesterNoFilter.SetEnumValue(this.SemesterNo.Value);
				this.ddlStatusFilter.FillGuidEnums<Cours.Statuses>(CommonListItems.All);
				if (this.Status != null)
					this.ddlStatusFilter.SetSelectedGuidEnum(this.Status.Value);
				this.ddlDepartmentIDFilter_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlIntakeSemesterIDFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, departmentID, true);
			if (!this.IsPostBack && this.ProgramID != null)
				this.ddlProgramIDFilter.SelectedValue = this.ProgramID.Value.ToString();
			this.ddlProgramIDFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearchFilters_Click(null, null);
		}

		protected void ddlSemesterNoFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void ddlStatusFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void btnSearchFilters_Click(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			if (!this.IsPostBack)
			{
				if (this.PageIndex != null)
					pageIndex = this.PageIndex.Value;
				if (this.PageSize != null)
					this.gvCourses.PageSize = this.PageSize.Value;
			}
			var intakeSemesterID = this.ddlIntakeSemesterIDFilter.SelectedValue.ToNullableShort();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var semesterNoEnum = this.ddlSemesterNoFilter.GetSelectedEnumValue<SemesterNos>(null);
			var statusEnum = this.ddlStatusFilter.GetSelectedNullableGuidEnum<Cours.Statuses>();
			var searchText = this.tbSearchTextFilters.Text.ToNullIfWhiteSpace();
			if (intakeSemesterID == null || programID == null)
			{
				this.gvCourses.DataBind(null);
				return;
			}
			var sortDirection = this.ViewState.GetSortDirection();
			var sortExpression = this.ViewState.GetSortExpression();
			var pageSize = this.gvCourses.PageSize;
			var result = BL.Core.Courses.Staff.CoursesCleanup.GetCourses(intakeSemesterID.Value, programID.Value, semesterNoEnum, statusEnum, searchText, pageIndex, pageSize, sortExpression, sortDirection, this.StaffIdentity.LoginSessionGuid);
			this.gvCourses.DataBind(result.Courses, pageIndex, result.VirtualItemCount, sortExpression, sortDirection);
			this.btnVerifySelectedCourses.Visible = result.Courses.Any();
			this.btnRejectSelectedCourses.Visible = result.Courses.Any();
			this.btnDeleteSelectedCourses.Visible = result.Courses.Any();
			this.courseDependenciesModal.Visible = false;
		}

		protected void gvCourses_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Verify":
					this.UpdateCourse(e.DecryptedCommandArgumentToInt(), Cours.Statuses.Verified, null, false);
					return;
				case "Reject":
					this.UpdateCourse(e.DecryptedCommandArgumentToInt(), Cours.Statuses.Rejected, null, false);
					return;
				case "Edit":
					{
						this.ShowEditCourseModal(e.DecryptedCommandArgumentToInt());
						e.Handled = true;
						return;
					}
				case "Delete":
					{
						e.Handled = true;
						var deletedResult = BL.Core.Courses.Staff.CoursesCleanup.DeleteCourse(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
						switch (deletedResult)
						{
							case BL.Core.Courses.Staff.CoursesCleanup.DeleteCourseStatuses.NoRecordFound:
								this.AddNoRecordFoundAlert();
								this.RefreshPage();
								return;
							case BL.Core.Courses.Staff.CoursesCleanup.DeleteCourseStatuses.CourseStatusMustBeRejected:
								this.AddErrorAlert("Status must be rejected before deleting a course.");
								this.RefreshPage();
								return;
							case BL.Core.Courses.Staff.CoursesCleanup.DeleteCourseStatuses.Success:
								this.AddSuccessMessageHasBeenDeleted("Course");
								this.RefreshPage();
								return;
							case BL.Core.Courses.Staff.CoursesCleanup.DeleteCourseStatuses.CanNotDeleteWhenDependenciesExists:
								this.AddSuccessMessageHasBeenDeleted("Course can not be deleted when dependencies exists.");
								this.RefreshPage();
								return;
							default:
								throw new NotImplementedEnumException(deletedResult);
						}
					}
				case "Process":
					{
						var processResult = BL.Core.Courses.Staff.CoursesCleanup.ProcessCleanUpProcedure(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
						switch (processResult)
						{
							case BL.Core.Courses.Staff.CoursesCleanup.ProcessCleanUpProcedureStatuses.NoRecordFound:
								this.AddNoRecordFoundAlert();
								this.RefreshPage();
								return;
							case BL.Core.Courses.Staff.CoursesCleanup.ProcessCleanUpProcedureStatuses.CourseStatusMustBeRejected:
								this.AddErrorAlert("Course status must be rejected.");
								this.RefreshPage();
								return;
							case BL.Core.Courses.Staff.CoursesCleanup.ProcessCleanUpProcedureStatuses.SubstituteCourseNotSet:
								this.AddErrorAlert("Substitute Course is not set for the course.");
								this.RefreshPage();
								return;
							case BL.Core.Courses.Staff.CoursesCleanup.ProcessCleanUpProcedureStatuses.SubstituteCourseNotFound:
								this.AddErrorAlert("Substitute Course not found.");
								this.RefreshPage();
								return;
							case BL.Core.Courses.Staff.CoursesCleanup.ProcessCleanUpProcedureStatuses.SubstituteCourseIsNotInSameRoadmap:
								this.AddErrorAlert("Roadmap of Substituted Course is different than the original course.");
								this.RefreshPage();
								return;
							case BL.Core.Courses.Staff.CoursesCleanup.ProcessCleanUpProcedureStatuses.Success:
								this.AddSuccessAlert("Course has been replaced by substitute course.");
								this.RefreshPage();
								return;
							default:
								throw new NotImplementedEnumException(processResult);
						}
					}
			}
		}

		protected void repeaterDependency_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "ViewDependencies":
					{
						var commandArguments = e.DecryptedCommandArgument().Split(':');
						var courseID = commandArguments[0].ToInt();
						var dependencyType = commandArguments[1].ToEnum<BL.Core.Courses.Staff.Common.DependencyTypes>();
						this.courseDependenciesModal.ShowModal(courseID, dependencyType);
						return;
					}
			}
		}

		private void ShowEditCourseModal(int courseID)
		{
			var result = BL.Core.Courses.Staff.CoursesCleanup.GetCourse(courseID, this.StaffIdentity.LoginSessionGuid);
			if (result?.course == null)
			{
				this.AddNoRecordFoundAlert();
				this.RefreshPage();
				return;
			}

			var courses = result.Value.coursesForSubstition
				.Where(c => c.CreditHours == result.Value.course.CreditHours)
				.Select(c => new CustomListItem
				{
					ID = c.CourseID,
					Text = $"{c.CourseCode} - {c.Title} - {c.CreditHours}"
				}).ToList();
			this.hfCourseIDEditCourse.Value = result.Value.course.CourseID.ToString();
			this.lblCourseCodeEditCourse.Text = result.Value.course.CourseCode;
			this.lblTitleEditCourse.Text = result.Value.course.Title;
			this.lblCreditHoursEditCourse.Text = result.Value.course.CreditHours.ToCreditHoursFullName();
			this.lblCourseCodeEditCourse.Text = result.Value.course.CourseCode;
			this.rblStatusEditCourse.FillGuidEnums<Cours.Statuses>().SetSelectedGuidEnum(result.Value.course.StatusEnum);
			this.ddlSubstituteCourseIDEditCourse.DataBind(courses, CommonListItems.None).SelectedValue = result.Value.course.SubstitutedCourseID.ToString();
			this.rblStatusEditCourse_SelectedIndexChanged(null, null);
			this.modalEditCourse.Visible = true;
			this.modalEditCourse.Show();
		}

		protected void rblStatusEditCourse_SelectedIndexChanged(object sender, EventArgs e)
		{
			var status = this.rblStatusEditCourse.GetSelectedGuidEnum<Cours.Statuses>();
			switch (status)
			{
				case Cours.Statuses.Pending:
				case Cours.Statuses.Verified:
					this.ddlSubstituteCourseIDEditCourse.ClearSelection();
					this.ddlSubstituteCourseIDEditCourse.Enabled = false;
					break;
				case Cours.Statuses.Rejected:
					this.ddlSubstituteCourseIDEditCourse.Enabled = true;
					break;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		protected void btnSaveEditCourse_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var courseID = this.hfCourseIDEditCourse.Value.ToInt();
			var status = this.rblStatusEditCourse.GetSelectedGuidEnum<Cours.Statuses>();
			var substitutedCourseID = this.ddlSubstituteCourseIDEditCourse.SelectedValue.ToNullableInt();
			this.UpdateCourse(courseID, status, substitutedCourseID, true);
		}

		private void UpdateCourse(int courseID, Cours.Statuses statusEnum, int? substitutedCourseID, bool editModal)
		{
			var updateStatus = BL.Core.Courses.Staff.CoursesCleanup.UpdateCourse(courseID, statusEnum, substitutedCourseID, this.StaffIdentity.LoginSessionGuid);
			switch (updateStatus)
			{
				case BL.Core.Courses.Staff.CoursesCleanup.UpdateCourseStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case BL.Core.Courses.Staff.CoursesCleanup.UpdateCourseStatuses.Success:
					this.AddSuccessAlert("Course has been updated.");
					this.RefreshPage();
					return;
				case BL.Core.Courses.Staff.CoursesCleanup.UpdateCourseStatuses.DuplicateCourseCode:
					{
						var message = "Course Code duplicated in verified courses.";
						if (editModal)
						{
							this.alertEditCourse.AddErrorAlert(message);
							this.updatePanelEditCourse.Update();
						}
						else
						{
							this.AddErrorAlert(message);
							this.RefreshPage();
						}
						return;
					}
				case BL.Core.Courses.Staff.CoursesCleanup.UpdateCourseStatuses.DuplicateTitle:
					{
						var message = "Title duplicated in verified courses.";
						if (editModal)
						{
							this.alertEditCourse.AddErrorAlert(message);
							this.updatePanelEditCourse.Update();
						}
						else
						{
							this.AddErrorAlert(message);
							this.RefreshPage();
						}
						return;
					}
				default:
					throw new NotImplementedEnumException(updateStatus);
			}
		}

		protected void gvCourses_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvCourses_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvCourses_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvCourses.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvCourses_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow)
				return;
			var item = (BL.Core.Courses.Staff.Common.Course)e.Row.DataItem;
			switch (item.Cours.StatusEnum)
			{
				case Cours.Statuses.Pending:
					return;
				case Cours.Statuses.Rejected:
					e.Row.CssClass = "danger";
					return;
				case Cours.Statuses.Verified:
					e.Row.CssClass = "success";
					return;
				default:
					throw new NotImplementedEnumException(item.Cours.StatusEnum);
			}
		}

		protected void btnVerifySelectedCourses_Click(object sender, EventArgs e)
		{
			var courseIDs = new List<int>();
			foreach (GridViewRow row in this.gvCourses.Rows)
				if (row.RowType == DataControlRowType.DataRow)
					if (((AspireCheckBox)row.FindControl("cbSelect")).Checked)
						courseIDs.Add(((System.Web.UI.WebControls.HiddenField)row.FindControl("hfCourseID")).Value.ToInt());
			if (!courseIDs.Any())
			{
				this.AddErrorAlert("No course is selected.");
				return;
			}

			var updateStatuses = BL.Core.Courses.Staff.CoursesCleanup.VerifyCourses(courseIDs, this.StaffIdentity.LoginSessionGuid);
			var statuses = updateStatuses.GroupBy(s => s).Select(g => new
			{
				g.Key,
				Total = g.Count()
			});
			foreach (var status in statuses)
				switch (status.Key)
				{
					case BL.Core.Courses.Staff.CoursesCleanup.UpdateCourseStatuses.NoRecordFound:
						if (status.Total == 1)
							this.AddErrorAlert($"{status.Total} course not found.");
						else
							this.AddErrorAlert($"{status.Total} course not found.");
						break;
					case BL.Core.Courses.Staff.CoursesCleanup.UpdateCourseStatuses.Success:
						if (status.Total == 1)
							this.AddSuccessAlert($"{status.Total} course has been verified.");
						else
							this.AddSuccessAlert($"{status.Total} courses have been verified.");
						break;
					case BL.Core.Courses.Staff.CoursesCleanup.UpdateCourseStatuses.DuplicateCourseCode:
						if (status.Total == 1)
							this.AddErrorAlert($"Course Code is duplicated in one course.");
						else
							this.AddErrorAlert($"Course Code is duplicated in {status.Total} courses.");
						break;
					case BL.Core.Courses.Staff.CoursesCleanup.UpdateCourseStatuses.DuplicateTitle:
						if (status.Total == 1)
							this.AddErrorAlert($"Course Title is duplicated in one course.");
						else
							this.AddErrorAlert($"Course Title is duplicated in {status.Total} courses.");
						break;
					default:
						throw new NotImplementedEnumException(status.Key);
				}
			this.RefreshPage();
		}

		protected void btnRejectSelectedCourses_OnClick(object sender, EventArgs e)
		{
			var courseIDs = new List<int>();
			foreach (GridViewRow row in this.gvCourses.Rows)
				if (row.RowType == DataControlRowType.DataRow)
					if (((AspireCheckBox)row.FindControl("cbSelect")).Checked)
						courseIDs.Add(((System.Web.UI.WebControls.HiddenField)row.FindControl("hfCourseID")).Value.ToInt());
			if (!courseIDs.Any())
			{
				this.AddErrorAlert("No course is selected.");
				return;
			}

			var updateStatuses = BL.Core.Courses.Staff.CoursesCleanup.RejectCourses(courseIDs, this.StaffIdentity.LoginSessionGuid);
			var statuses = updateStatuses.GroupBy(s => s).Select(g => new
			{
				g.Key,
				Total = g.Count()
			});
			foreach (var status in statuses)
				switch (status.Key)
				{
					case BL.Core.Courses.Staff.CoursesCleanup.UpdateCourseStatuses.NoRecordFound:
						if (status.Total == 1)
							this.AddErrorAlert($"{status.Total} course not found.");
						else
							this.AddErrorAlert($"{status.Total} course not found.");
						break;
					case BL.Core.Courses.Staff.CoursesCleanup.UpdateCourseStatuses.Success:
						if (status.Total == 1)
							this.AddSuccessAlert($"{status.Total} course has been rejected.");
						else
							this.AddSuccessAlert($"{status.Total} courses have been rejected.");
						break;
					case BL.Core.Courses.Staff.CoursesCleanup.UpdateCourseStatuses.DuplicateCourseCode:
						if (status.Total == 1)
							this.AddErrorAlert($"Course Code is duplicated in one course.");
						else
							this.AddErrorAlert($"Course Code is duplicated in {status.Total} courses.");
						break;
					case BL.Core.Courses.Staff.CoursesCleanup.UpdateCourseStatuses.DuplicateTitle:
						if (status.Total == 1)
							this.AddErrorAlert($"Course Title is duplicated in one course.");
						else
							this.AddErrorAlert($"Course Title is duplicated in {status.Total} courses.");
						break;
					default:
						throw new NotImplementedEnumException(status.Key);
				}
			this.RefreshPage();
		}

		protected void btnDeleteSelectedCourses_OnClick(object sender, EventArgs e)
		{
			var courseIDs = new List<int>();
			foreach (GridViewRow row in this.gvCourses.Rows)
				if (row.RowType == DataControlRowType.DataRow)
					if (((AspireCheckBox)row.FindControl("cbSelect")).Checked)
						courseIDs.Add(((System.Web.UI.WebControls.HiddenField)row.FindControl("hfCourseID")).Value.ToInt());
			if (!courseIDs.Any())
			{
				this.AddErrorAlert("No course is selected.");
				return;
			}

			var deleteStatuses = BL.Core.Courses.Staff.CoursesCleanup.DeleteCourses(courseIDs, this.StaffIdentity.LoginSessionGuid);
			var statuses = deleteStatuses.GroupBy(s => s).Select(g => new
			{
				g.Key,
				Total = g.Count()
			});
			foreach (var status in statuses)
				switch (status.Key)
				{
					case BL.Core.Courses.Staff.CoursesCleanup.DeleteCourseStatuses.NoRecordFound:
						if (status.Total == 1)
							this.AddErrorAlert($"{status.Total} course not found.");
						else
							this.AddErrorAlert($"{status.Total} course not found.");
						break;
					case BL.Core.Courses.Staff.CoursesCleanup.DeleteCourseStatuses.Success:
						if (status.Total == 1)
							this.AddSuccessAlert($"{status.Total} course has been deleted.");
						else
							this.AddSuccessAlert($"{status.Total} courses have been deleted.");
						break;
					case BL.Core.Courses.Staff.CoursesCleanup.DeleteCourseStatuses.CanNotDeleteWhenDependenciesExists:
						if (status.Total == 1)
							this.AddErrorAlert($"One course cannot be deleted due to dependencies.");
						else
							this.AddErrorAlert($"{status.Total} courses cannot be deleted due to dependencies.");
						break;
					case BL.Core.Courses.Staff.CoursesCleanup.DeleteCourseStatuses.CourseStatusMustBeRejected:
						if (status.Total == 1)
							this.AddErrorAlert($"One course must be rejected before deletion.");
						else
							this.AddErrorAlert($"{status.Total} courses must be rejected before deletion.");
						break;
					default:
						throw new NotImplementedEnumException(status.Key);
				}
			this.RefreshPage();
		}
	}
}