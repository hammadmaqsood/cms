﻿using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Staff.Courses
{
	public partial class CourseDependenciesModal : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
		}

		public void ShowModal(int courseID, BL.Core.Courses.Staff.Common.DependencyTypes dependencyType)
		{
			var result = BL.Core.Courses.Staff.Common.GetDependencies(courseID, dependencyType, StaffIdentity.Current.LoginSessionGuid);
			this.repeaterCourseInfo.DataSource = new[] { result };
			this.repeaterCourseInfo.DataBind();
			this.modalDependencies.Visible = true;
			this.modalDependencies.Show();
			this.Visible = true;
		}
	}
}