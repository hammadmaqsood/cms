﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CoursesEquivalents.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Courses.CoursesEquivalents" %>

<%@ Import Namespace="Aspire.Web.Sys.Staff.Courses" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="ShowCourses" CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_SelectedIndexChanged" />
			<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="ShowCourses" ControlToValidate="ddlSemesterID" ErrorMessage="This field is required." />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlAdmissionOpenProgramID" />
			<aspire:AspireDropDownList runat="server" ID="ddlAdmissionOpenProgramID" ValidationGroup="ShowCourses" CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="ddlAdmissionOpenProgramID_SelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester No:" AssociatedControlID="ddlSemesterNo" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" ValidationGroup="ShowCourses" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterNo_SelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Category:" AssociatedControlID="ddlCourseCategory" />
			<aspire:AspireDropDownList runat="server" ID="ddlCourseCategory" ValidationGroup="ShowCourses" AutoPostBack="True" OnSelectedIndexChanged="ddlCourseCategory_SelectedIndexChanged" />
		</div>
		<div class="form-group input-group">
			<aspire:AspireTextBox runat="server" ID="tbSearchText" ValidationGroup="ShowCourses" PlaceHolder="Search" />
			<div class="input-group-btn">
				<aspire:AspireButton ButtonType="Default" Glyphicon="search" runat="server" ID="btnSearch" ValidationGroup="Search" OnClick="btnSearch_Click" />
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView ID="gvCoursesEquivalents" runat="server" AllowSorting="True" AutoGenerateColumns="False" OnPageSizeChanging="gvCoursesEquivalents_PageSizeChanging" OnSorting="gvCoursesEquivalents_Sorting" OnPageIndexChanging="gvCoursesEquivalents_PageIndexChanging" AllowPaging="True" OnRowCommand="gvCoursesEquivalents_RowCommand">
		<Columns>
			<asp:BoundField DataField="Semester" HeaderText="Semester" SortExpression="SemesterID" />
			<asp:BoundField DataField="Class" HeaderText="Class" SortExpression="ProgramAlias" />
			<asp:BoundField DataField="CourseCode" HeaderText="Code" SortExpression="CourseCode" />
			<asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
			<asp:BoundField DataField="CreditHoursFullName" HeaderText="Credit Hrs." SortExpression="CreditHours" />
			<asp:BoundField DataField="EqSemester" HeaderText="Semester" SortExpression="EqSemesterID" />
			<asp:BoundField DataField="EqClass" HeaderText="Class" SortExpression="EqProgramAlias" />
			<asp:BoundField DataField="EqCourseCode" HeaderText="Code" SortExpression="EqCourseCode" />
			<asp:BoundField DataField="EqTitle" HeaderText="Title" SortExpression="EqTitle" />
			<asp:BoundField DataField="EqCreditHoursFullName" HeaderText="Credit Hrs." SortExpression="EqCreditHours" />
			<asp:BoundField DataField="EqRemarks" HeaderText="Remarks" SortExpression="EqRemarks" />
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" NavigateUrl='<%# CourseEquivalents.GetPageUrl((int) Eval("CourseID")) %>'><Aspire:AspireGlyphicon Icon="edit" runat="server"/></aspire:AspireHyperLink>
					<aspire:AspireLinkButton runat="server" Glyphicon="remove" CausesValidation="False" CommandName="DeleteEquivalentCourse" EncryptedCommandArgument='<%# Eval("CourseEquivalentID") %>' ConfirmMessage='<%# Eval("EqTitle","Are you sure you want to delete equivalent course \"{0}\"?") %>' Visible='<%# Eval("EqCourseID")!= null %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<div class="text-center">
		<aspire:AspireHyperLink runat="server" ID="hplCoursesEquivalentsReport" Text="Courses Equivalents Report" Target="_blank" />
	</div>
</asp:Content>
