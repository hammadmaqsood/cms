﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Staff.Courses
{
	[AspirePage("552E0BFA-B114-4495-A802-DA867FC2D5F9", UserTypes.Staff, "~/Sys/Staff/Courses/Course.aspx", "Course", true, AspireModules.Courses)]
	public partial class Course : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Courses.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => this.CourseID == null ? "Add Course" : "Edit Course";
		private int? CourseID => this.GetParameterValue<int>(nameof(this.CourseID));
		private short? SemesterID => this.GetParameterValue<short>(nameof(this.SemesterID));
		private int? ProgramID => this.GetParameterValue<int>(nameof(this.ProgramID));
		private SemesterNos? SemesterNoEnum => this.GetParameterValue<SemesterNos>(nameof(this.SemesterNoEnum));
		private CourseCategories? CourseCategoryEnum => this.GetParameterValue<CourseCategories>(nameof(this.CourseCategoryEnum));
		private CourseTypes? CourseTypeEnum => this.GetParameterValue<CourseTypes>(nameof(this.CourseTypeEnum));

		public static string GetPageUrl(short? semesterID, int? programID, SemesterNos? semesterNoEnum, CourseCategories? courseCategoryEnum, CourseTypes? courseTypeEnum)
		{
			return GetPageUrl<Course>().AttachQueryParams(nameof(SemesterID), semesterID, nameof(ProgramID), programID, nameof(SemesterNoEnum), semesterNoEnum, nameof(CourseCategoryEnum), courseCategoryEnum, nameof(CourseTypeEnum), courseTypeEnum);
		}

		public static string GetPageUrl(int courseID)
		{
			return GetPageUrl<Course>().AttachQueryParam(nameof(CourseID), courseID, EncryptionModes.ConstantSaltUserSession);
		}

		public static void Redirect(short? semesterID, int? programID, SemesterNos? semesterNoEnum, CourseCategories? courseCategoryEnum, CourseTypes? courseTypeEnum)
		{
			Redirect(GetPageUrl(semesterID, programID, semesterNoEnum, courseCategoryEnum, courseTypeEnum));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterID.FillSemesters();
				this.ddlSemesterID_SelectedIndexChanged(null, null);
				this.ddlSemesterNo.FillSemesterNos();
				this.ddlCreditHours.FillCreditHours().SelectedValue = CreditHours.Three.ToCreditHoursFullName();
				this.ddlContactHours.FillCreditHours().SelectedValue = CreditHours.Three.ToCreditHoursFullName();
				this.ddlCategory.FillCourseCategories();
				this.ddlCourseType.FillCourseTypes();
				this.ddlSummerGradeCapping.FillYesNo();
				this.ddlStatus.FillGuidEnums<Cours.Statuses>();

				if (this.CourseID != null)
				{
					var record = BL.Core.Courses.Staff.Courses.GetCourseWithDependencies(this.CourseID.Value, this.StaffIdentity.LoginSessionGuid);
					if (record == null)
					{
						this.AddNoRecordFoundAlert();
						Redirect<Courses>();
						return;
					}
					var course = record.Value.Course.Cours;

					var verified = record.Value.Course.Cours.StatusEnum == Cours.Statuses.Verified;
					var dependenciesExists = record.Value.Course.Dependencies.Any();

					this.ddlSemesterID.SelectedValue = record.Value.SemesterID.ToString();
					this.ddlSemesterID.Enabled = false;
					this.ddlSemesterID_SelectedIndexChanged(null, null);
					this.ddlAdmissionOpenProgramID.SelectedValue = course.AdmissionOpenProgramID.ToString();
					this.ddlAdmissionOpenProgramID.Enabled = false;
					this.ddlAdmissionOpenProgramID_OnSelectedIndexChanged(null, null);
					this.ddlProgramMajorID.SelectedValue = course.ProgramMajorID.ToString();
					this.ddlSemesterNo.SetEnumValue(course.SemesterNoEnum);
					this.ddlSemesterNo.Enabled = !verified;
					this.tbCourseCode.Text = course.CourseCode;
					this.tbCourseCode.ReadOnly = verified || dependenciesExists;
					this.tbCourseTitle.Text = course.Title;
					this.tbCourseTitle.ReadOnly = verified || dependenciesExists;
					this.ddlCreditHours.SelectedValue = course.CreditHours.ToCreditHoursFullName();
					this.ddlCreditHours.Enabled = !(verified || dependenciesExists);
					this.ddlContactHours.SelectedValue = course.ContactHours.ToCreditHoursFullName();
					this.ddlContactHours.Enabled = !(verified || dependenciesExists);
					this.ddlCategory.SetEnumValue(course.CourseCategoryEnum);
					this.ddlCategory.Enabled = !verified;
					this.ddlCourseType.SetEnumValue(course.CourseTypeEnum);
					this.ddlCourseType.Enabled = !verified;
					this.ddlProgramMajorID.SelectedValue = course.ProgramMajorID.ToString();
					this.ddlProgramMajorID.Enabled = !verified;
					this.ddlSummerGradeCapping.SetBooleanValue(course.SummerGradeCapping);
					this.ddlSummerGradeCapping.Enabled = !verified && this.StaffIdentity.TryDemand(StaffPermissions.Courses.ManageCourseSummerGradeCapping, UserGroupPermission.PermissionValues.Allowed) != null;
					this.ddlStatus.SetSelectedGuidEnum(course.StatusEnum);
					this.ddlStatus.Enabled = false;
					this.tbRemarks.Text = course.Remarks;
					this.tbRemarks.ReadOnly = verified;

					this.btnSave.Visible = this.btnSave.Enabled = this.ddlSemesterNo.Enabled || !this.tbCourseCode.ReadOnly || !this.tbCourseTitle.ReadOnly || this.ddlCreditHours.Enabled
						|| this.ddlContactHours.Enabled || this.ddlCategory.Enabled || this.ddlCourseType.Enabled || this.ddlProgramMajorID.Enabled
						|| this.ddlSummerGradeCapping.Enabled || !this.tbRemarks.ReadOnly;
					this.btnSaveAndAddAnother.Visible = false;
					this.gvDependencies.Visible = true;
					this.gvDependencies.DataBind(record.Value.Course.Dependencies);

					var admissionOpenProgram = BL.Core.Courses.Staff.Courses.GetAdmissionOpenProgram(course.AdmissionOpenProgramID, this.StaffIdentity.LoginSessionGuid);
					if (admissionOpenProgram != null)
						this.hlCancel.NavigateUrl = Courses.GetPageUrl(admissionOpenProgram.Value.semesterID, admissionOpenProgram.Value.programID, course.SemesterNoEnum, course.CourseCategoryEnum, course.CourseTypeEnum, null, null, null, null, null);
					else
						throw new InvalidOperationException();
				}
				else
				{
					this.StaffIdentity.Demand(StaffPermissions.Courses.ManageCourses, UserGroupPermission.PermissionValues.Allowed);
					this.ddlSemesterID.SetSelectedValueIfNotPostback(nameof(this.SemesterID));
					this.ddlSemesterID_SelectedIndexChanged(null, null);
					if (this.SemesterID != null && this.ProgramID != null)
					{
						var admissionOpenProgramID = BL.Core.Courses.Staff.Courses.GetAdmissionOpenProgramID(this.SemesterID.Value, this.ProgramID.Value, this.StaffIdentity.LoginSessionGuid);
						this.ddlAdmissionOpenProgramID.SetSelectedValueIfExists(admissionOpenProgramID.ToString());
					}
					this.ddlAdmissionOpenProgramID_OnSelectedIndexChanged(null, null);
					this.ddlSemesterNo.SetEnumValue(SemesterNos.One);
					this.ddlSemesterNo.SetSelectedEnumValueIfNotPostback<SemesterNos>(nameof(this.SemesterNoEnum));
					this.ddlCategory.SetEnumValue(CourseCategories.Course);
					this.ddlCategory.SetSelectedEnumValueIfNotPostback<CourseCategories>(nameof(this.CourseCategoryEnum));
					this.ddlCategory.SetEnumValue(CourseTypes.Core);
					this.ddlCourseType.SetSelectedEnumValueIfNotPostback<CourseTypes>(nameof(this.CourseTypeEnum));
					this.ddlSummerGradeCapping.SetBooleanValue(true);
					this.ddlSummerGradeCapping.Enabled = this.StaffIdentity.TryDemand(StaffPermissions.Courses.ManageCourseSummerGradeCapping, UserGroupPermission.PermissionValues.Allowed) != null;
					this.ddlStatus.SetSelectedGuidEnum(Cours.Statuses.Pending);
					this.ddlStatus.Enabled = false;
					this.btnSave.Enabled = true;
					this.btnSaveAndAddAnother.Enabled = true;
					this.courseDependenciesModal.Visible = false;
					this.gvDependencies.Visible = false;
					this.hlCancel.NavigateUrl = Courses.GetPageUrl(this.SemesterID, this.ProgramID, this.SemesterNoEnum, this.CourseCategoryEnum, this.CourseTypeEnum, null, null, null, null, null);
				}
			}
		}

		protected void ddlSemesterID_SelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var admissionOpenPrograms = BL.Core.Common.Lists.GetAdmissionOpenPrograms(semesterID, this.StaffIdentity.InstituteID);
			this.ddlAdmissionOpenProgramID.DataBind(admissionOpenPrograms, CommonListItems.Select);
			this.ddlAdmissionOpenProgramID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlAdmissionOpenProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var admissionOpenProgramID = this.ddlAdmissionOpenProgramID.SelectedValue.ToNullableInt();
			if (admissionOpenProgramID == null)
			{
				this.ddlProgramMajorID.Items.Clear();
				this.ddlProgramMajorID.Enabled = false;
			}
			else
			{
				var programMajors = BL.Core.Common.Lists.GetAdmissionOpenProgramMajors(admissionOpenProgramID.Value);
				this.ddlProgramMajorID.DataBind(programMajors, CommonListItems.None);
				this.ddlProgramMajorID.Enabled = true;
			}
		}

		private void Save(bool saveAndAddNext)
		{
			var courseID = this.CourseID;
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var admissionOpenProgramID = this.ddlAdmissionOpenProgramID.SelectedValue.ToInt();
			var courseCode = this.tbCourseCode.Text;
			var title = this.tbCourseTitle.Text;
			var creditHours = this.ddlCreditHours.SelectedValue.ToDecimal();
			var contactHours = this.ddlContactHours.SelectedValue.ToDecimal();
			var courseCategoryEnum = this.ddlCategory.GetSelectedEnumValue<CourseCategories>();
			var courseTypeEnum = this.ddlCourseType.GetSelectedEnumValue<CourseTypes>();
			var programMajorID = this.ddlProgramMajorID.SelectedValue.ToNullableInt();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>();
			var remarks = this.tbRemarks.Text;
			var summerGradeCapping = this.ddlSummerGradeCapping.SelectedValue.ToBoolean();

			if (courseID == null)
			{
				var course = BL.Core.Courses.Staff.Courses.AddCourse(admissionOpenProgramID, courseCode, title, creditHours, contactHours, courseCategoryEnum, courseTypeEnum, programMajorID, semesterNoEnum, summerGradeCapping, remarks, this.StaffIdentity.LoginSessionGuid);
				if (course == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Courses>();
				}
				else
				{
					this.AddSuccessMessageHasBeenAdded("Course");
					if (saveAndAddNext)
						Redirect(semesterID, course.AdmissionOpenProgramID, course.SemesterNoEnum, course.CourseCategoryEnum, course.CourseTypeEnum);
					else
					{
						var admissionOpenProgram = BL.Core.Courses.Staff.Courses.GetAdmissionOpenProgram(course.AdmissionOpenProgramID, this.StaffIdentity.LoginSessionGuid);
						if (admissionOpenProgram != null)
							Courses.Redirect(admissionOpenProgram.Value.semesterID, admissionOpenProgram.Value.programID, course.SemesterNoEnum, course.CourseCategoryEnum, course.CourseTypeEnum, null, null, null, null, null);
						else
							throw new InvalidOperationException();
					}
				}
			}
			else
			{
				var course = BL.Core.Courses.Staff.Courses.UpdateCourse(courseID.Value, courseCode, title, creditHours, contactHours, courseCategoryEnum, courseTypeEnum, programMajorID, semesterNoEnum, summerGradeCapping, remarks, this.StaffIdentity.LoginSessionGuid);
				if (course == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Courses>();
				}
				else
				{
					this.AddSuccessMessageHasBeenUpdated("Course");
					var admissionOpenProgram = BL.Core.Courses.Staff.Courses.GetAdmissionOpenProgram(course.AdmissionOpenProgramID, this.StaffIdentity.LoginSessionGuid);
					if (admissionOpenProgram != null)
						Courses.Redirect(admissionOpenProgram.Value.semesterID, admissionOpenProgram.Value.programID, course.SemesterNoEnum, course.CourseCategoryEnum, course.CourseTypeEnum, null, null, null, null, null);
					else
						throw new InvalidOperationException();
				}
			}
		}

		protected void btnSave_Click(object sender, EventArgs e)
		{
			this.Save(false);
		}

		protected void btnSaveAndAddAnother_OnClick(object sender, EventArgs e)
		{
			this.Save(true);
		}

		protected void gvDependencies_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
		{
			var dependencyType = e.DecryptedCommandArgument().ToEnum<BL.Core.Courses.Staff.Common.DependencyTypes>();
			this.courseDependenciesModal.ShowModal(this.CourseID ?? throw new InvalidOperationException(), dependencyType);
		}
	}
}