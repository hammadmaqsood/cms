﻿using Aspire.BL.Core.Courses.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Courses
{
	[AspirePage("B96A11F6-C1FA-411F-8772-474B8082FBFD", UserTypes.Staff, "~/Sys/Staff/Courses/Courses.aspx", "Courses", true, AspireModules.Courses)]
	public partial class Courses : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Courses.Module, UserGroupPermission.Allowed},
		};

		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.book);
		public override string PageTitle => "Courses";

		private short? SemesterID => this.GetParameterValue<short>(nameof(this.SemesterID));
		private int? ProgramID => this.GetParameterValue<int>(nameof(this.ProgramID));
		private SemesterNos? SemesterNoEnum => this.GetParameterValue<SemesterNos>(nameof(this.SemesterNoEnum));
		private CourseCategories? CourseCategoryEnum => this.GetParameterValue<CourseCategories>(nameof(this.CourseCategoryEnum));
		private CourseTypes? CourseTypeEnum => this.GetParameterValue<CourseTypes>(nameof(this.CourseTypeEnum));
		private string SearchText => this.GetParameterValue(nameof(this.SearchText));
		private string SortExpression => this.GetParameterValue(nameof(this.SortExpression));
		private SortDirection? SortDirection => this.GetParameterValue<SortDirection>(nameof(this.SortDirection));
		private int? PageIndex => this.GetParameterValue<int>(nameof(this.PageIndex));
		private int? PageSize => this.GetParameterValue<int>(nameof(this.PageSize));

		public static string GetPageUrl(short? semesterID, int? programID, SemesterNos? semesterNoEnum, CourseCategories? courseCategoryEnum, CourseTypes? courseTypeEnum, string searchText, string sortExpression, SortDirection? sortDirection, int? pageIndex, int? pageSize)
		{
			return GetPageUrl<Courses>().AttachQueryParams(
				(nameof(SemesterID), semesterID),
				(nameof(ProgramID), programID),
				(nameof(SemesterNoEnum), semesterNoEnum),
				(nameof(CourseCategoryEnum), courseCategoryEnum),
				(nameof(CourseTypeEnum), courseTypeEnum),
				(nameof(SearchText), searchText),
				(nameof(sortExpression), sortExpression),
				(nameof(SortDirection), sortDirection),
				(nameof(PageIndex), pageIndex),
				(nameof(PageSize), pageSize));
		}

		public static void Redirect(short? semesterID, int? programID, SemesterNos? semesterNoEnum, CourseCategories? courseCategoryEnum, CourseTypes? courseTypeEnum, string searchText, string sortExpression, SortDirection? sortDirection, int? pageIndex, int? pageSize)
		{
			Redirect(GetPageUrl(semesterID, programID, semesterNoEnum, courseCategoryEnum, courseTypeEnum, searchText, sortExpression, sortDirection, pageIndex, pageSize));
		}

		private void RefreshPage()
		{
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
			var courseCategoryEnum = this.ddlCourseCategory.GetSelectedEnumValue<CourseCategories>(null);
			var courseTypeEnum = this.ddlCourseType.GetSelectedEnumValue<CourseTypes>(null);
			var searchText = this.tbSearchText.Text;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var pageIndex = this.gvCourses.PageIndex;
			var pageSize = this.gvCourses.PageSize;
			Redirect(semesterID, programID, semesterNoEnum, courseCategoryEnum, courseTypeEnum, searchText, sortExpression, sortDirection, pageIndex, pageSize);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.Page.IsPostBack)
			{
				var sortExpression = this.SortExpression ?? nameof(Course.Title);
				var sortDirection = this.SortDirection ?? System.Web.UI.WebControls.SortDirection.Ascending;
				this.ViewState.SetSortProperties(sortExpression, sortDirection);
				this.ddlSemesterID.FillSemesters(CommonListItems.All).SetSelectedValueIfNotPostback(nameof(this.SemesterID));
				this.ddlProgramID.FillPrograms(this.StaffIdentity.InstituteID, null, true, CommonListItems.All).SetSelectedValueIfNotPostback(nameof(this.ProgramID));
				this.ddlSemesterNo.FillSemesterNos(CommonListItems.All).SetSelectedEnumValueIfNotPostback<SemesterNos>(nameof(this.SemesterNoEnum));
				this.ddlCourseCategory.FillCourseCategories(CommonListItems.All).SetSelectedEnumValueIfNotPostback<CourseCategories>(nameof(this.CourseCategoryEnum));
				this.ddlCourseType.FillCourseTypes(CommonListItems.All).SetSelectedEnumValueIfNotPostback<CourseTypes>(nameof(this.CourseTypeEnum));
				this.tbSearchText.Text = this.GetParameterValue(nameof(this.SearchText));
				this.gvCourses.PageSize = this.PageSize ?? this.gvCourses.PageSize;
				this.GetData(this.PageIndex ?? 0);
			}
		}

		protected void ddlSemesterID_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void ddlProgramID_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void ddlSemesterNo_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void ddlCourseCategory_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void ddlCourseType_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void gvCourses_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvCourses.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvCourses_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvCourses_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		private void GetData(int pageIndex)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
			var courseType = this.ddlCourseType.GetSelectedEnumValue<CourseTypes>(null);
			var courseCategory = this.ddlCourseCategory.GetSelectedEnumValue<CourseCategories>(null);
			var searchText = this.tbSearchText.Text;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			this.hlAddCourse.NavigateUrl = Course.GetPageUrl(semesterID, programID, semesterNoEnum, courseCategory, courseType);

			var result = BL.Core.Courses.Staff.Courses.GetCourses(semesterID, programID, semesterNoEnum, courseCategory, courseType, searchText, pageIndex, this.gvCourses.PageSize, sortExpression, sortDirection, this.StaffIdentity.LoginSessionGuid);
			this.gvCourses.DataBind(result.Courses, pageIndex, result.VirtualItemCount, sortExpression, sortDirection);
			this.btnExport.Enabled = semesterID != null && programID != null;
			if (semesterID == null || programID == null)
				this.hplCoursesReport.NavigateUrl = GetPageUrl<Reports.Courses>();
			else
				this.hplCoursesReport.NavigateUrl = Reports.Courses.GetPageUrl(semesterID.Value, programID.Value);
		}

		protected void gvCourses_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "View":
					var course = BL.Core.Courses.Staff.Courses.GetCourse(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					if (course == null)
					{
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						return;
					}

					this.ViewState[nameof(Cours.CourseID)] = course.CourseID;
					this.lblSemester.Text = course.AdmissionOpenProgram.SemesterID.ToSemesterString();
					this.lblProgram.Text = course.AdmissionOpenProgram.Program.ProgramAlias;
					this.lblCourseCode.Text = course.CourseCode;
					this.lblCourseTitle.Text = course.Title;
					this.GetPreRequisites();
					this.modalPreReqs.Show();
					break;
			}
		}

		private void GetPreRequisites()
		{
			var result = CoursePreRequisites.GetPreRequisiteCourses((int)this.ViewState[nameof(Cours.CourseID)], this.StaffIdentity.LoginSessionGuid);
			if (result == null)
			{
				this.AddNoRecordFoundAlert();
				this.RefreshPage();
				return;
			}

			var preReqCourses = result.CoursePreRequsites.Select(p => new
			{
				p.CoursePreReqID,
				PreReq1Title = result.Courses.Find(c => c.ID == p.PreReqCourseID1)?.Text,
				PreReq2Title = result.Courses.Find(c => c.ID == p.PreReqCourseID2)?.Text,
				PreReq3Title = result.Courses.Find(c => c.ID == p.PreReqCourseID3)?.Text,
			}).ToList();

			this.gvPreReqs.DataBind(preReqCourses);
			this.ddlPreRequisite1.DataBind(result.Courses, CommonListItems.None);
			this.ddlPreRequisite2.DataBind(result.Courses, CommonListItems.None);
			this.ddlPreRequisite3.DataBind(result.Courses, CommonListItems.None);
		}

		protected void btnAddPreReq_OnClick(object sender, EventArgs e)
		{
			var courseIDs = new[]
				{
					this.ddlPreRequisite1.SelectedValue.ToNullableInt(),
					this.ddlPreRequisite2.SelectedValue.ToNullableInt(),
					this.ddlPreRequisite3.SelectedValue.ToNullableInt(),
				}.Distinct()
				.Where(c => c.HasValue)
				.Select(c => c.Value).ToList();
			if (!courseIDs.Any())
				return;
			var preReqAdded = CoursePreRequisites.AddPreRequisiteCourses((int)this.ViewState[nameof(Cours.CourseID)], courseIDs, this.StaffIdentity.LoginSessionGuid);
			switch (preReqAdded)
			{
				case CoursePreRequisites.AddPreRequisiteCoursesStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case CoursePreRequisites.AddPreRequisiteCoursesStatuses.RecordAlreadyExists:
					this.aspireAlertPreReq.AddErrorAlert("Pre-Requisites already exists.");
					this.updatePanelPreReqs.Update();
					return;
				case CoursePreRequisites.AddPreRequisiteCoursesStatuses.Success:
					this.aspireAlertPreReq.AddSuccessAlert("Pre-Requisites have been added.");
					this.GetPreRequisites();
					this.updatePanelPreReqs.Update();
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void gvPreReqs_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					var preRequisiteCourseID = e.DecryptedCommandArgumentToInt();
					var deleted = CoursePreRequisites.DeletePreRequisiteCourse(preRequisiteCourseID, this.StaffIdentity.LoginSessionGuid);
					switch (deleted)
					{
						case CoursePreRequisites.DeletePreRequisiteCourseStatuses.NoRecordFound:
							this.aspireAlertPreReq.AddNoRecordFoundAlert();
							this.RefreshPage();
							break;
						case CoursePreRequisites.DeletePreRequisiteCourseStatuses.Success:
							this.aspireAlertPreReq.AddSuccessMessageHasBeenDeleted("Pre-requisite course");
							this.GetPreRequisites();
							this.updatePanelPreReqs.Update();
							break;
						default:
							throw new NotImplementedEnumException(deleted);
					}
					break;
			}
		}

		protected void gvCourses_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow)
				return;
			var course = (BL.Core.Courses.Staff.Courses.Course)e.Row.DataItem;
			switch (course.StatusEnum)
			{
				case Cours.Statuses.Pending:
					break;
				case Cours.Statuses.Rejected:
					e.Row.CssClass = "danger";
					break;
				case Cours.Statuses.Verified:
					e.Row.CssClass = "success";
					break;
				default:
					throw new NotImplementedEnumException(course.StatusEnum);
			}
		}

		protected void btnExport_OnClick(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			if (semesterID == null || programID == null)
			{
				this.RefreshPage();
				return;
			}
			var (csv, fileName) = BL.Core.Courses.Staff.Courses.GetCoursesInCSVFormat(semesterID.Value, programID.Value, this.StaffIdentity.LoginSessionGuid);
			if (csv == null || fileName == null)
			{
				this.AddNoRecordFoundAlert();
				this.RefreshPage();
				return;
			}
			this.Response.Clear();
			this.Response.Buffer = true;
			this.Response.AddHeader("content-disposition", $"attachment;filename=\"{fileName}\"");
			this.Response.Charset = "";
			this.Response.ContentType = "text/csv";
			this.Response.Output.Write(csv);
			this.Response.Flush();
			this.Response.End();
		}

		protected void ddlSemesterIDImport_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDImport.SelectedValue.ToNullableShort();
			if (semesterID == null)
				this.ddlAdmissionOpenProgramIDImport.DataBind(CommonListItems.Select);
			else
			{
				var admissionOpenPrograms = BL.Core.Common.Lists.GetAdmissionOpenPrograms(semesterID.Value, this.StaffIdentity.InstituteID);
				this.ddlAdmissionOpenProgramIDImport.DataBind(admissionOpenPrograms, CommonListItems.Select);
			}
		}

		protected void btnShowImportModal_OnClick(object sender, EventArgs e)
		{
			this.ddlSemesterIDImport.FillSemesters(CommonListItems.Select);
			this.ddlAdmissionOpenProgramIDImport.DataBind(CommonListItems.Select);
			this.fileUpload.ClearFiles();
			this.modalImportCourses.Show();
			this.modalImportCourses.Visible = true;
		}

		protected void btnImportCourses_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var admissionOpenProgramID = this.ddlAdmissionOpenProgramIDImport.SelectedValue.ToInt();
			var instanceGuid = this.fileUpload.InstanceGuid;
			var temporarySystemFileID = this.fileUpload.UploadedFiles.Single().FileID.ToGuid();
			var result = BL.Core.Courses.Staff.Courses.ImportCSV(admissionOpenProgramID, instanceGuid, temporarySystemFileID, this.StaffIdentity.LoginSessionGuid);

			switch (result.Status)
			{
				case BL.Core.Courses.Staff.Courses.ImportCSVResult.Statuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case BL.Core.Courses.Staff.Courses.ImportCSVResult.Statuses.FileIsNotValid:
					this.alertImportCourses.AddErrorAlert("Uploaded file is not valid.");
					this.updatePanelImportCourses.Update();
					return;
				case BL.Core.Courses.Staff.Courses.ImportCSVResult.Statuses.Success:
					this.AddSuccessAlert($"{result.CoursesCount} courses have been imported.");
					this.RefreshPage();
					break;
				case BL.Core.Courses.Staff.Courses.ImportCSVResult.Statuses.Failed:
					this.alertImportCourses.AddErrorAlert(result.ErrorMessage);
					this.updatePanelImportCourses.Update();
					return;
				default:
					throw new NotImplementedEnumException(result.Status);
			}
		}
	}
}
