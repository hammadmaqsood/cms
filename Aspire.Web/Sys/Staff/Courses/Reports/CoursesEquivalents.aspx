﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="CoursesEquivalents.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Courses.Reports.CoursesEquivalents" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Offered Semester:" AssociatedControlID="ddlOfferedSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlOfferedSemesterIDFilter" ValidationGroup="Filters" CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="ddlOfferedSemesterIDFilter_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" ValidationGroup="Filters" CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="ddlProgramIDFilter_OnSelectedIndexChanged" />
		</div>
	</div>
</asp:Content>
