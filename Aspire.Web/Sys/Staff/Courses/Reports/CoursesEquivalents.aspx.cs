﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Courses.Reports
{
	[AspirePage("FEB2AB94-C539-457D-BCF4-15F523A4A5F9", UserTypes.Staff, "~/Sys/Staff/Courses/Reports/CoursesEquivalents.aspx", "Courses Equivalents", true, AspireModules.Courses)]
	public partial class CoursesEquivalents : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Courses.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Courses Equivalents";
		public string ReportName => "Courses Equivalents";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		public static string GetPageUrl(short semesterID, int? programID)
		{
			return GetPageUrl<CoursesEquivalents>().AttachQueryParams("SemesterID", semesterID, "ProgramID", programID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlOfferedSemesterIDFilter.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
				if (this.ddlOfferedSemesterIDFilter.Items.Count == 0)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<CoursesEquivalents>();
					return;
				}
				this.ddlOfferedSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlOfferedSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlOfferedSemesterIDFilter.SelectedValue.ToShort();
			var programs = Aspire.BL.Core.Common.Lists.GetPrograms(this.StaffIdentity.InstituteID, null, true);
			this.ddlProgramIDFilter.DataBind(programs).SetSelectedValueIfNotPostback("ProgramID");
			if (this.ddlProgramIDFilter.Items.Count == 0)
			{
				this.AddErrorAlert("No Programs record found.");
				Redirect<Dashboard>();
				return;
			}
			this.ddlProgramIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView.ReportViewer);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			var semesterID = this.ddlOfferedSemesterIDFilter.SelectedValue.ToShort();
			var admissionOpenProgramID = this.ddlProgramIDFilter.SelectedValue.ToInt();
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/Courses/Staff/CoursesEquivalents.rdlc".MapPath();
			var reportDataSet = BL.Core.Courses.Staff.Reports.CoursesEquivalents.GetCoursesEquivalents(semesterID, admissionOpenProgramID, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.EquivalentCourses), reportDataSet.EquivalentCourses));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}