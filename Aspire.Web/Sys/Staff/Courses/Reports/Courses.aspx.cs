﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Courses.Reports
{
	[AspirePage("548785D4-3C9C-42F1-8CA2-649062DC1223", UserTypes.Staff, "~/Sys/Staff/Courses/Reports/Courses.aspx", "Courses", true, AspireModules.Courses)]
	public partial class Courses : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Courses.Module,new [] { UserGroupPermission.PermissionValues.Allowed }}
		};

		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Courses";
		public string ReportName => "Courses";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		public static string GetPageUrl(short semesterID, int programID)
		{
			return GetPageUrl<Courses>().AttachQueryParams("SemesterID", semesterID, "ProgramID", programID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
				if (this.ddlSemesterIDFilter.Items.Count == 0)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<Courses>();
					return;
				}
				this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, null, true, CommonListItems.Select);
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlProgramIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView.ReportViewer);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			if (semesterID == null || programID == null)
			{
				reportViewer.Reset();
				return;
			}
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/Courses/Staff/Courses.rdlc".MapPath();
			var reportDataSet = BL.Core.Courses.Staff.Reports.Courses.GetCourses(semesterID.Value, programID.Value, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.Courses), reportDataSet.Courses));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}