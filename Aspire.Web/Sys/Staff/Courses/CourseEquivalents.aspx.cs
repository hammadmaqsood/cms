﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Courses
{
	[AspirePage("7EBB4A77-A032-496E-9C56-76DE5F25C935", UserTypes.Staff, "~/Sys/Staff/Courses/CourseEquivalents.aspx", "Course Equivalents", true, AspireModules.Courses)]
	public partial class CourseEquivalents : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Courses.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Course Equivalents";

		public static string GetPageUrl(int? courseID)
		{
			if (courseID != null)
				return GetPageUrl<CourseEquivalents>().AttachQueryParam(nameof(Cours.CourseID), courseID);
			return GetPageUrl<CourseEquivalents>();
		}

		public static void Redirect(int courseID)
		{
			Redirect(GetPageUrl(courseID));
		}

		private int? CourseID => this.Request.GetParameterValue<int>(nameof(Cours.CourseID));

		private void RefreshPage()
		{
			Redirect(this.CourseID.Value);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(nameof(Cours.Title), SortDirection.Ascending);
				this.ViewState.SetSortProperties1(nameof(Cours.Title), SortDirection.Ascending);
				if (CourseID == null)
				{
					Redirect<CoursesEquivalents>();
					return;
				}
				var course = BL.Core.Courses.Staff.Courses.GetCourse(this.CourseID.Value, this.StaffIdentity.LoginSessionGuid);
				if (course == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<CoursesEquivalents>();
					return;
				}
				this.lblSemester.Text = course.AdmissionOpenProgram.SemesterID.ToSemesterString();
				this.lblProgram.Text = course.AdmissionOpenProgram.Program.ProgramAlias;
				this.lblCourseCode.Text = course.CourseCode;
				this.lblCourseTitle.Text = course.Title;
				this.lblCreditHrs.Text = course.CreditHours.ToString();
				this.lblSemesterNo.Text = course.SemesterNoEnum.ToFullName();
				this.GetEquivalentCourses(0);
				this.btnAutoFind_OnClick(null, null);
			}
		}

		#region Equivalent Courses

		private void GetEquivalentCourses(int pageIndex)
		{
			var result = BL.Core.Courses.Staff.CourseEquivalents.GetCourseEquivalents(this.CourseID.Value, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), pageIndex, this.gvEquivalentCourses.PageSize, this.StaffIdentity.LoginSessionGuid);
			this.gvEquivalentCourses.DataBind(result.CourseEquivalents, pageIndex, result.VirtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		protected void gvEquivalentCourses_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvEquivalentCourses.PageSize = e.NewPageSize;
			this.GetEquivalentCourses(0);
		}

		protected void gvEquivalentCourses_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetEquivalentCourses(e.NewPageIndex);
		}

		protected void gvEquivalentCourses_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetEquivalentCourses(0);
		}

		protected void gvEquivalentCourses_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					var eqCourse = Aspire.BL.Core.Courses.Staff.CourseEquivalents.GetCourseEquivalent(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					if (eqCourse == null)
					{
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						return;
					}
					this.tbRemarksEdit.Text = eqCourse.EquivalencyRemarks;
					this.lblSemesterEdit.Text = eqCourse.Semester;
					this.lblProgramEdit.Text = eqCourse.ProgramAlias;
					this.lblCourseCodeEdit.Text = eqCourse.CourseCode;
					this.lblCourseTitleEdit.Text = eqCourse.Title;
					this.lblCreditHrsEdit.Text = eqCourse.CreditHours.ToString();
					this.ViewState[nameof(eqCourse.CourseEquivalentID)] = eqCourse.CourseEquivalentID;
					this.modalEditRemarks.Show();
					break;
				case "Delete":
					e.Handled = true;
					var result = Aspire.BL.Core.Courses.Staff.CourseEquivalents.DeleteEquivalentCourse(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					switch (result)
					{
						case BL.Core.Courses.Staff.CourseEquivalents.DeleteEquivalentCourseStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.RefreshPage();
							return;
						case BL.Core.Courses.Staff.CourseEquivalents.DeleteEquivalentCourseStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Specific Equivalency of the course has been deleted.");
							this.RefreshPage();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		#endregion

		#region Courses

		protected void gvCourses_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvCourses.PageSize = e.NewPageSize;
			this.GetAllCourses(0);
		}

		protected void gvCourses_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs1(this.ViewState);
			this.GetAllCourses(0);
		}

		protected void gvCourses_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetAllCourses(e.NewPageIndex);
		}

		private void GetAllCourses(int pageIndex)
		{
			var autoFind = (bool)this.ViewState["AutoFind"];
			var result = BL.Core.Courses.Staff.CourseEquivalents.GetCoursesForEquivalency(this.CourseID.Value, autoFind, tbSearchText.Text, pageIndex, this.gvCourses.PageSize, this.ViewState.GetSortExpression1(), this.ViewState.GetSortDirection1(), this.StaffIdentity.LoginSessionGuid);
			this.gvCourses.DataBind(result.Courses, pageIndex, result.VirtualItemCount, this.ViewState.GetSortExpression1(), this.ViewState.GetSortDirection1());
		}

		protected void btnAutoFind_OnClick(object sender, EventArgs e)
		{
			this.ViewState["AutoFind"] = true;
			this.tbSearchText.Text = null;
			this.GetAllCourses(0);
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			this.ViewState["AutoFind"] = false;
			this.GetAllCourses(0);
		}

		protected void btnSaveEquivalentCourses_Click(object sender, EventArgs e)
		{
			if (this.CourseID == null)
				throw new InvalidOperationException();
			var equivalentCourseIDs = new List<int>();
			foreach (GridViewRow row in gvCourses.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow)
					continue;
				var cb = (AspireCheckBox)row.FindControl("cbCourseID");
				if (cb.Checked)
					equivalentCourseIDs.Add((int)this.gvCourses.DataKeys[row.DataItemIndex].Value);
			}
			var result = BL.Core.Courses.Staff.CourseEquivalents.AddEquivalentCourses(this.CourseID.Value, equivalentCourseIDs, this.StaffIdentity.LoginSessionGuid);
			var results = result.GroupBy(r => r).Select(r => new { r.Key, Count = r.Count() });
			foreach (var res in results)
			{
				switch (res.Key)
				{
					case BL.Core.Courses.Staff.CourseEquivalents.AddEquivalentCourseStatuses.Success:
						if (res.Count == 1)
							this.AddSuccessAlert("Equivalent course has been added.");
						else
							this.AddSuccessAlert($"{res.Count} Equivalent courses have been added.");
						break;
					case BL.Core.Courses.Staff.CourseEquivalents.AddEquivalentCourseStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<CoursesEquivalents>();
						return;
					case BL.Core.Courses.Staff.CourseEquivalents.AddEquivalentCourseStatuses.EquivalentCourseDoesnotExists:
						if (res.Count == 1)
							this.AddErrorAlert("Equivalent course not found.");
						else
							this.AddErrorAlert($"{res.Count} Equivalent courses not found.");
						break;
					case BL.Core.Courses.Staff.CourseEquivalents.AddEquivalentCourseStatuses.CannotAddBecauseSameRoadmap:
						if (res.Count == 1)
							this.AddWarningAlert("Equivalent course cannot be added because it belongs to the same roadmap.");
						else
							this.AddWarningAlert($"{res.Count} Equivalent courses cannot be added because they belongs to the same roadmap.");
						break;
					case BL.Core.Courses.Staff.CourseEquivalents.AddEquivalentCourseStatuses.CannotAddBecauseCourseIsAlreadyMappedToEquivalentCourseRoadmap:
						if (res.Count == 1)
							this.AddWarningAlert("Equivalent course cannot be added because course from the target roadmap is already added as equivalent course.");
						else
							this.AddWarningAlert($"{res.Count} Equivalent courses cannot be added because courses from the target roadmap are already added as equivalent courses.");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			this.RefreshPage();
		}

		#endregion

		#region Modal Edit Remarks

		protected void btnSaveRemarks_OnClick(object sender, EventArgs e)
		{
			var courseEquivalentID = (int?)this.ViewState[nameof(CourseEquivalent.CourseEquivalentID)];
			if (courseEquivalentID == null)
				throw new InvalidOperationException();

			var remarks = this.tbRemarksEdit.Text;
			var status = Aspire.BL.Core.Courses.Staff.CourseEquivalents.UpdateCourseEquivalentRemarks(courseEquivalentID.Value, remarks, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Courses.Staff.CourseEquivalents.UpdateCourseEquivalentRemarksStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					break;
				case BL.Core.Courses.Staff.CourseEquivalents.UpdateCourseEquivalentRemarksStatuses.Success:
					this.AddSuccessAlert("Remarks for the equivalent course has been updated.");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.RefreshPage();
		}

		#endregion
	}
}