﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Course.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Courses.Course" %>

<%@ Register Src="~/Sys/Staff/Courses/CourseDependenciesModal.ascx" TagPrefix="uc1" TagName="CourseDependenciesModal" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:Panel runat="server">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="AddCourse" CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_SelectedIndexChanged" />
					<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="AddCourse" ControlToValidate="ddlSemesterID" ErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlAdmissionOpenProgramID" />
					<aspire:AspireDropDownList runat="server" ID="ddlAdmissionOpenProgramID" CausesValidation="false" ValidationGroup="AddCourse" OnSelectedIndexChanged="ddlAdmissionOpenProgramID_OnSelectedIndexChanged" AutoPostBack="True" />
					<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="AddCourse" ControlToValidate="ddlAdmissionOpenProgramID" ErrorMessage="This field is required." />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester No:" AssociatedControlID="ddlSemesterNo" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" ValidationGroup="AddCourse" />
					<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="AddCourse" ControlToValidate="ddlSemesterNo" ErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Course Code:" AssociatedControlID="tbCourseCode" />
					<aspire:AspireTextBox ValidationGroup="AddCourse" runat="server" MaxLength="50" ID="tbCourseCode" TextTransform="UpperCase" />
					<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="AddCourse" ControlToValidate="tbCourseCode" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Course Code." />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Course Title:" AssociatedControlID="tbCourseTitle" />
					<aspire:AspireTextBox ValidationGroup="AddCourse" runat="server" MaxLength="250" ID="tbCourseTitle" />
					<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="AddCourse" ControlToValidate="tbCourseTitle" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Course Code." />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Credit Hours:" AssociatedControlID="ddlCreditHours" />
					<aspire:AspireDropDownList runat="server" ID="ddlCreditHours" ValidationGroup="AddCourse" />
					<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="AddCourse" ControlToValidate="ddlCreditHours" ErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Contact Hours:" AssociatedControlID="ddlContactHours" />
					<aspire:AspireDropDownList runat="server" ID="ddlContactHours" ValidationGroup="AddCourse" />
					<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="AddCourse" ControlToValidate="ddlContactHours" ErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Course Category:" AssociatedControlID="ddlCategory" />
					<aspire:AspireDropDownList runat="server" ID="ddlCategory" />
					<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="AddCourse" ControlToValidate="ddlCategory" ErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Course Type:" AssociatedControlID="ddlCourseType" />
					<aspire:AspireDropDownList runat="server" ID="ddlCourseType" />
					<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="AddCourse" ControlToValidate="ddlCourseType" ErrorMessage="This field is required." />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Majors:" AssociatedControlID="ddlProgramMajorID" />
					<aspire:AspireDropDownList runat="server" ID="ddlProgramMajorID" ValidationGroup="AddCourse" />
					<span class="help-block">This is mandatory for major courses.</span>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Grade Capping (In Summers):" AssociatedControlID="ddlSummerGradeCapping" />
					<aspire:AspireDropDownList runat="server" ID="ddlSummerGradeCapping" />
					<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="AddCourse" ControlToValidate="ddlSummerGradeCapping" ErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatus" />
					<aspire:AspireDropDownList runat="server" ID="ddlStatus" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" />
			<aspire:AspireTextBox TextMode="MultiLine" Rows="3" ValidationGroup="AddCourse" runat="server" MaxLength="1000" ID="tbRemarks" />
		</div>
		<aspire:AspireGridView runat="server" ID="gvDependencies" ItemType="Aspire.BL.Core.Courses.Staff.Common.Course.Dependency" AutoGenerateColumns="false" Caption="Dependencies" OnRowCommand="gvDependencies_RowCommand">
			<Columns>
				<asp:BoundField DataField="Name" HeaderText="Name" />
				<asp:BoundField DataField="Count" HeaderText="Count" />
				<asp:TemplateField HeaderText="Action">
					<ItemTemplate>
						<aspire:AspireLinkButton runat="server" CausesValidation="false" Text="View" CommandName="ViewDependencies" EncryptedCommandArgument='<%# Item.DependencyType %>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
		<div class="form-group text-center">
			<aspire:AspireButton runat="server" ID="btnSave" ButtonType="Primary" ValidationGroup="AddCourse" Text="Save" OnClick="btnSave_Click" />
			<aspire:AspireButton runat="server" ID="btnSaveAndAddAnother" ButtonType="Primary" ValidationGroup="AddCourse" Text="Save & Add Another" OnClick="btnSaveAndAddAnother_OnClick" />
			<aspire:AspireHyperLink runat="server" Text="Cancel" ID="hlCancel" NavigateUrl="~/Sys/Staff/Courses/Courses.aspx" />
		</div>
	</asp:Panel>
	<uc1:CourseDependenciesModal runat="server" ID="courseDependenciesModal" />
</asp:Content>
