﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CoursesCleanup.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Courses.CoursesCleanup" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Register Src="~/Sys/Staff/Courses/CourseDependenciesModal.ascx" TagPrefix="uc1" TagName="CourseDependenciesModal" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Intake Semester:" AssociatedControlID="ddlIntakeSemesterIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlIntakeSemesterIDFilter" ValidationGroup="Filters" CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="ddlIntakeSemesterIDFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" ValidationGroup="Filters" CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentIDFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" ValidationGroup="Filters" CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="ddlProgramIDFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="ddlSemesterNoFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterNoFilter" ValidationGroup="Filters" CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterNoFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatusFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlStatusFilter" ValidationGroup="Filters" CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="ddlStatusFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-4">
			<aspire:AspireLabel runat="server" Text="Search:" AssociatedControlID="tbSearchTextFilters" />
			<asp:Panel runat="server" CssClass="input-group" DefaultButton="btnSearchFilters">
				<aspire:AspireTextBox runat="server" ID="tbSearchTextFilters" ValidationGroup="Filters" />
				<div class="input-group-btn">
					<aspire:AspireButton ButtonType="Default" Glyphicon="search" runat="server" ID="btnSearchFilters" ValidationGroup="Search" OnClick="btnSearchFilters_Click" />
				</div>
			</asp:Panel>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvCourses" AutoGenerateColumns="false" AllowSorting="true" ItemType="Aspire.BL.Core.Courses.Staff.Common.Course" OnRowCommand="gvCourses_RowCommand" OnSorting="gvCourses_Sorting" OnPageIndexChanging="gvCourses_PageIndexChanging" OnPageSizeChanging="gvCourses_PageSizeChanging" OnRowDataBound="gvCourses_RowDataBound">
		<Columns>
			<asp:TemplateField HeaderText="Select">
				<HeaderTemplate>
					<aspire:AspireCheckBox runat="server" CssClass="SelectAll" />
				</HeaderTemplate>
				<ItemTemplate>
					<asp:HiddenField Visible="false" runat="server" ID="hfCourseID" Value="<%# Item.Cours.CourseID %>" />
					<aspire:AspireCheckBox runat="server" ValidationGroup="SelectCourse" ID="cbSelect" CssClass="Select" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Actions" ItemStyle-Wrap="false">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CausesValidation="false" EncryptedCommandArgument="<%# Item.Cours.CourseID %>" CommandName="Edit" ToolTip="Edit" Glyphicon="edit" />
					<aspire:AspireLinkButton runat="server" CausesValidation="false" EncryptedCommandArgument="<%# Item.Cours.CourseID %>" CommandName="Verify" ToolTip="Verify" Visible="<%# Item.CanVerify %>" CssClass="text-success" FontAwesomeIcon="solid_check" ConfirmMessage='<%# $"Are you sure you want to verify {Item.Cours.CourseCode} - {Item.Cours.Title}?" %>' />
					<aspire:AspireLinkButton runat="server" CausesValidation="false" EncryptedCommandArgument="<%# Item.Cours.CourseID %>" CommandName="Reject" ToolTip="Reject" Visible="<%# Item.CanReject %>" CssClass="text-danger" FontAwesomeIcon="solid_ban" ConfirmMessage='<%# $"Are you sure you want to reject {Item.Cours.CourseCode} - {Item.Cours.Title}?" %>' />
					<aspire:AspireLinkButton runat="server" CausesValidation="false" EncryptedCommandArgument="<%# Item.Cours.CourseID %>" CommandName="Process" ToolTip="Process Substitution" Visible="<%# Item.CanProcess %>" CssClass="text-danger" FontAwesomeIcon="solid_cogs" ConfirmMessage='<%# $"Are you sure you want to process substitution of {Item.Cours.CourseCode} - {Item.Cours.Title}?" %>' />
					<aspire:AspireLinkButton runat="server" CausesValidation="false" EncryptedCommandArgument="<%# Item.Cours.CourseID %>" CommandName="Delete" ToolTip="Delete" Visible="<%# Item.CanDelete %>" Glyphicon="remove" ConfirmMessage='<%# $"Are you sure you want to delete {Item.Cours.CourseCode} - {Item.Cours.Title}?" %>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Code" SortExpression="CourseCode">
				<ItemTemplate>
					<aspire:Label Font-Strikeout="<%# Item.SubstitutedCourse != null %>" runat="server" Text="<%# Item.Cours.CourseCode %>" />
					<div runat="server" visible="<%# Item.SubstitutedCourse != null %>">
						<%#: Item.SubstitutedCourse?.Cours.CourseCode %>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Title" SortExpression="Title">
				<ItemTemplate>
					<aspire:AspireHyperLink Text="<%# Item.Cours.Title %>" Font-Strikeout="<%# Item.SubstitutedCourse != null %>" runat="server" Target="_blank" NavigateUrl="<%# Aspire.Web.Sys.Staff.Courses.Course.GetPageUrl(Item.Cours.CourseID) %>">
						
					</aspire:AspireHyperLink>
					<div runat="server" visible="<%# Item.SubstitutedCourse != null %>">
						<%#: Item.SubstitutedCourse?.Cours.Title %>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Credit Hours" SortExpression="CreditHours">
				<ItemTemplate>
					<aspire:Label Font-Strikeout="<%# Item.SubstitutedCourse != null %>" runat="server" Text="<%# Item.Cours.CreditHours.ToCreditHoursFullName() %>" />
					<div runat="server" visible="<%# Item.SubstitutedCourse != null %>">
						<%#: Item.SubstitutedCourse?.Cours.CreditHours.ToCreditHoursFullName() %>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Type" SortExpression="CourseTypeEnum">
				<ItemTemplate>
					<aspire:Label Font-Strikeout="<%# Item.SubstitutedCourse != null %>" runat="server" Text="<%# Item.Cours.CourseTypeEnum.ToFullName() %>" />
					<div runat="server" visible="<%# Item.SubstitutedCourse != null %>">
						<%#: Item.SubstitutedCourse?.Cours.CourseTypeEnum.ToFullName() %>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Sem. #" SortExpression="SemesterNoEnum">
				<ItemTemplate>
					<aspire:Label Font-Strikeout="<%# Item.SubstitutedCourse != null %>" runat="server" Text="<%# Item.Cours.SemesterNoEnum.ToFullName() %>" />
					<div runat="server" visible="<%# Item.SubstitutedCourse != null %>">
						<%#: Item.SubstitutedCourse?.Cours.SemesterNoEnum.ToFullName() %>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Dependencies" SortExpression="DependenciesCount">
				<ItemTemplate>
					<asp:Repeater runat="server" ID="repeaterDependency" DataSource="<%# Item.Dependencies %>" ItemType="Aspire.BL.Core.Courses.Staff.Common.Course.Dependency" OnItemCommand="repeaterDependency_ItemCommand">
						<ItemTemplate>
							<aspire:AspireLinkButton runat="server" Text='<%# $"{Item.Name}: {Item.Count}" %>' CausesValidation="False" CommandName="ViewDependencies" EncryptedCommandArgument='<%# $"{Item.Course.Cours.CourseID}:{Item.DependencyType}" %>' />
						</ItemTemplate>
						<SeparatorTemplate>
							<br />
						</SeparatorTemplate>
					</asp:Repeater>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Issues" SortExpression="ErrorsCount">
				<ItemTemplate>
					<%# $"{Item.Errors.Join("\n")}".HtmlEncode(true) %>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<div class="text-center">
		<aspire:AspireButton runat="server" ButtonType="Success" ID="btnVerifySelectedCourses" ValidationGroup="SelectCourse" Text="Verify Selected Courses" ConfirmMessage="Are you sure you want to continue?" OnClick="btnVerifySelectedCourses_Click" />
		<aspire:AspireButton runat="server" ButtonType="Primary" ID="btnRejectSelectedCourses" ValidationGroup="SelectCourse" Text="Reject Selected Courses" ConfirmMessage="Are you sure you want to continue?" OnClick="btnRejectSelectedCourses_OnClick" />
		<aspire:AspireButton runat="server" ButtonType="Danger" ID="btnDeleteSelectedCourses" ValidationGroup="SelectCourse" Text="Delete Selected Courses" ConfirmMessage="Are you sure you want to continue?" OnClick="btnDeleteSelectedCourses_OnClick" />
	</div>

	<aspire:AspireModal runat="server" ID="modalEditCourse" ModalSize="None" HeaderText="Edit Course" Visible="false">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelEditCourse" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertEditCourse" />
					<div class="row">
						<div class="col-md-2">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Code:" AssociatedControlID="lblCourseCodeEditCourse" />
								<div class="form-control-static">
									<aspire:Label ID="lblCourseCodeEditCourse" runat="server" />
									<aspire:HiddenField ID="hfCourseIDEditCourse" Visible="false" Mode="Encrypted" runat="server" />
								</div>
							</div>
						</div>
						<div class="col-md-7">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Title:" AssociatedControlID="lblTitleEditCourse" />
								<div class="form-control-static">
									<aspire:Label ID="lblTitleEditCourse" runat="server" />
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Credit Hours:" AssociatedControlID="lblCreditHoursEditCourse" />
								<div class="form-control-static">
									<aspire:Label ID="lblCreditHoursEditCourse" runat="server" />
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="rblStatusEditCourse" />
						<div class="form-control-static">
							<aspire:AspireRadioButtonList RepeatDirection="Horizontal" RepeatLayout="Flow" runat="server" ID="rblStatusEditCourse" ValidationGroup="EditCourse" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="rblStatusEditCourse_SelectedIndexChanged" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Substitute Course:" AssociatedControlID="ddlSubstituteCourseIDEditCourse" />
						<aspire:AspireDropDownList runat="server" ID="ddlSubstituteCourseIDEditCourse" ValidationGroup="Substitute" />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelEditCourseButtons" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSaveEditCourse" Text="Save" ValidationGroup="EditCourse" OnClick="btnSaveEditCourse_Click" />
					<aspire:AspireModalCloseButton runat="server">Close</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>

	<uc1:CourseDependenciesModal runat="server" ID="courseDependenciesModal" />
	<script type="text/javascript">
		$(function () {
			function enableDisableButtons() {
				var enabled = $(".Select input[type=\"checkbox\"]:checked").length > 0;
				$("#<%=this.btnVerifySelectedCourses.ClientID%>").attr("data-enabled", enabled).toggleClass("disabled", !enabled);
				$("#<%=this.btnRejectSelectedCourses.ClientID%>").attr("data-enabled", enabled).toggleClass("disabled", !enabled);
				$("#<%=this.btnDeleteSelectedCourses.ClientID%>").attr("data-enabled", enabled).toggleClass("disabled", !enabled);
			}
			$(".SelectAll input[type=\"checkbox\"]").change(function () {
				$(".Select input[type=\"checkbox\"]").prop("checked", $(this).is(":checked"));
				enableDisableButtons();
			});
			$(".Select input[type=\"checkbox\"]").change(function () {
				$(".SelectAll input[type=\"checkbox\"]").prop("checked", $(".Select input[type=\"checkbox\"]:not(:checked)").length === 0);
				enableDisableButtons();
			});
			enableDisableButtons();
			var init = function () {
				$("#<%=this.ddlSubstituteCourseIDEditCourse.ClientID%>").applySelect2();
			};
			init();
			Sys.Application.add_load(init);
		});
	</script>
</asp:Content>
