﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CourseDependenciesModal.ascx.cs" Inherits="Aspire.Web.Sys.Staff.Courses.CourseDependenciesModal" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.BL.Core.Courses.Staff" %>
<aspire:AspireModal runat="server" ID="modalDependencies" ModalSize="Large" HeaderText="Dependencies" Visible="false">
	<BodyTemplate>
		<asp:Repeater runat="server" ID="repeaterCourseInfo" ItemType="Aspire.BL.Core.Courses.Staff.Common.GetDependenciesResult">
			<ItemTemplate>
				<table class="table table-bordered table-condensed tableCol4">
					<tbody>
						<tr>
							<th>Code</th>
							<td><%#: Item.CourseCode %></td>
							<th>Title</th>
							<td><%#: Item.Title %></td>
						</tr>
						<tr>
							<th>Credit Hours</th>
							<td><%#: Item.CreditHours.ToCreditHoursFullName() %></td>
							<th>Type</th>
							<td><%#: Item.CourseTypeEnum.ToFullName() %></td>
						</tr>
						<tr>
							<th>Intake</th>
							<td><%#: Item.SemesterID.ToSemesterString() %></td>
							<th>Program</th>
							<td><%#: Item.ProgramAlias %></td>
						</tr>
					</tbody>
				</table>
				<div style="max-height: 250px" class="table-responsive">
					<asp:Repeater runat="server" Visible="<%# Item.DependencyType == Common.DependencyTypes.RegisteredCourses %>" DataSource="<%# Item.RegisteredCourses %>" ItemType="Aspire.BL.Core.Courses.Staff.Common.RegisteredCourse">
						<HeaderTemplate>
							<table class="table table-bordered table-condensed table-striped table-hover">
								<caption>Registered Courses</caption>
								<thead>
									<tr>
										<th>#</th>
										<th>Enrollment</th>
										<th>Offered Semester</th>
										<th>Deleted Date</th>
									</tr>
								</thead>
								<tbody>
						</HeaderTemplate>
						<ItemTemplate>
							<tr>
								<td><%#: Container.ItemIndex + 1 %></td>
								<td><%#: Item.Enrollment %></td>
								<td><%#: Item.OfferedSemesterID.ToSemesterString() %></td>
								<td><%#: (Item.DeletedDate?.ToString("d")).ToNAIfNullOrEmpty() %></td>
							</tr>
						</ItemTemplate>
						<FooterTemplate>
							<tr runat="server" visible="<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>">
								<td colspan="10">No record found.</td>
							</tr>
							</tbody>
							</table>
						</FooterTemplate>
					</asp:Repeater>
					<asp:Repeater runat="server" Visible="<%# Item.DependencyType == Common.DependencyTypes.OfferedCourses %>" DataSource="<%# Item.OfferedCourses %>" ItemType="Aspire.BL.Core.Courses.Staff.Common.OfferedCourse">
						<HeaderTemplate>
							<table class="table table-bordered table-condensed table-striped table-hover">
								<caption>Offered Courses</caption>
								<thead>
									<tr>
										<th>#</th>
										<th>Semester</th>
										<th>Class</th>
										<th>Faculty Member</th>
									</tr>
								</thead>
								<tbody>
						</HeaderTemplate>
						<ItemTemplate>
							<tr>
								<td><%#: Container.ItemIndex + 1 %></td>
								<td><%#: Item.SemesterID.ToSemesterString() %></td>
								<td><%#: Item.ClassName %></td>
								<td><%#: Item.FacultyMemberName.ToNAIfNullOrEmpty() %></td>
							</tr>
						</ItemTemplate>
						<FooterTemplate>
							<tr runat="server" visible="<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>">
								<td colspan="10">No record found.</td>
							</tr>
							</tbody>
							</table>
						</FooterTemplate>
					</asp:Repeater>
					<asp:Repeater runat="server" Visible="<%# Item.DependencyType == Common.DependencyTypes.StudentExemptedCourses %>" DataSource="<%# Item.StudentExemptedCourses %>" ItemType="Aspire.BL.Core.Courses.Staff.Common.StudentExemptedCourse">
						<HeaderTemplate>
							<table class="table table-bordered table-condensed table-striped table-hover">
								<caption>Exempted Courses</caption>
								<thead>
									<tr>
										<th>#</th>
										<th>Enrollment</th>
										<th>Name</th>
									</tr>
								</thead>
								<tbody>
						</HeaderTemplate>
						<ItemTemplate>
							<tr>
								<td><%#: Container.ItemIndex + 1 %></td>
								<td><%#: Item.Enrollment %></td>
								<td><%#: Item.Name %></td>
							</tr>
						</ItemTemplate>
						<FooterTemplate>
							<tr runat="server" visible="<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>">
								<td colspan="10">No record found.</td>
							</tr>
							</tbody>
							</table>
						</FooterTemplate>
					</asp:Repeater>
					<asp:Repeater runat="server" Visible="<%# Item.DependencyType == Common.DependencyTypes.StudentCreditsTransferredCourses %>" DataSource="<%# Item.StudentCreditsTransferredCourses %>" ItemType="Aspire.BL.Core.Courses.Staff.Common.StudentCreditsTransferredCourse">
						<HeaderTemplate>
							<table class="table table-bordered table-condensed table-striped table-hover">
								<caption>Credits Transferred Courses</caption>
								<thead>
									<tr>
										<th>#</th>
										<th>Enrollment</th>
										<th>Name</th>
									</tr>
								</thead>
								<tbody>
						</HeaderTemplate>
						<ItemTemplate>
							<tr>
								<td><%#: Container.ItemIndex + 1 %></td>
								<td><%#: Item.Enrollment %></td>
								<td><%#: Item.Name %></td>
							</tr>
						</ItemTemplate>
						<FooterTemplate>
							<tr runat="server" visible="<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>">
								<td colspan="10">No record found.</td>
							</tr>
							</tbody>
							</table>
						</FooterTemplate>
					</asp:Repeater>
					<asp:Repeater runat="server" Visible="<%# Item.DependencyType == Common.DependencyTypes.TransferredStudentCourseMappings %>" DataSource="<%# Item.TransferredStudentCourseMappings %>" ItemType="Aspire.BL.Core.Courses.Staff.Common.TransferredStudentCourseMapping">
						<HeaderTemplate>
							<table class="table table-bordered table-condensed table-striped table-hover">
								<caption>Inter-Campus Transfers</caption>
								<thead>
									<tr>
										<th rowspan="2">#</th>
										<th colspan="2">From</th>
										<th colspan="2">To</th>
									</tr>
									<tr>
										<th>Enrollment</th>
										<th>Name</th>
										<th>Enrollment</th>
										<th>Name</th>
									</tr>
								</thead>
								<tbody>
						</HeaderTemplate>
						<ItemTemplate>
							<tr>
								<td><%#: Container.ItemIndex + 1 %></td>
								<td><%#: Item.FromEnrollment %></td>
								<td><%#: Item.FromName %></td>
								<td><%#: Item.ToEnrollment %></td>
								<td><%#: Item.ToName %></td>
							</tr>
						</ItemTemplate>
						<FooterTemplate>
							<tr runat="server" visible="<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>">
								<td colspan="10">No record found.</td>
							</tr>
							</tbody>
							</table>
						</FooterTemplate>
					</asp:Repeater>
				</div>
			</ItemTemplate>
		</asp:Repeater>
	</BodyTemplate>
	<FooterTemplate>
		<aspire:AspireModalCloseButton runat="server">Close</aspire:AspireModalCloseButton>
	</FooterTemplate>
</aspire:AspireModal>
