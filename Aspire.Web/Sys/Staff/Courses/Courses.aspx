﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Courses.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Courses.Courses" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<aspire:AspireHyperLinkButton runat="server" ID="hlAddCourse" ButtonType="Success" Glyphicon="plus" Target="_blank" NavigateUrl="Course.aspx" Text="Add Course" />
				<aspire:AspireButton runat="server" ID="btnShowImportModal" CausesValidation="False" Text="Import Courses" ButtonType="Primary" Glyphicon="import" OnClick="btnShowImportModal_OnClick" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Intake Semester:" AssociatedControlID="ddlSemesterID" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="Filters" CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
				<aspire:AspireDropDownList runat="server" ID="ddlProgramID" ValidationGroup="Filters" CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="ddlProgramID_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="ddlSemesterNo" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterNo_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Category:" AssociatedControlID="ddlCourseCategory" />
				<aspire:AspireDropDownList runat="server" ID="ddlCourseCategory" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlCourseCategory_SelectedIndexChanged" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlCourseType" />
				<aspire:AspireDropDownList runat="server" ID="ddlCourseType" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlCourseType_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-6">
			<aspire:AspireLabel runat="server" Text="Search:" AssociatedControlID="tbSearchText" />
			<asp:Panel runat="server" CssClass="input-group" DefaultButton="btnSearch">
				<aspire:AspireTextBox runat="server" ID="tbSearchText" ValidationGroup="Filters" />
				<div class="input-group-btn">
					<aspire:AspireButton ButtonType="Default" Glyphicon="search" runat="server" ID="btnSearch" ValidationGroup="Search" OnClick="btnSearch_Click" />
				</div>
			</asp:Panel>
		</div>
		<div class="col-md-4">
			<div>
				<aspire:AspireLabel runat="server" Text="&nbsp;" AssociatedControlID="btnExport" />
			</div>
			<aspire:AspireButton runat="server" CssClass="pull-right" ID="btnExport" CausesValidation="False" Text="Export" ButtonType="Primary" Glyphicon="export" OnClick="btnExport_OnClick" />
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView ItemType="Aspire.BL.Core.Courses.Staff.Courses.Course" OnRowCommand="gvCourses_OnRowCommand" ID="gvCourses" runat="server" AllowSorting="True" AllowPaging="True" AutoGenerateColumns="False" OnPageSizeChanging="gvCourses_PageSizeChanging" OnSorting="gvCourses_Sorting" OnPageIndexChanging="gvCourses_PageIndexChanging" OnRowDataBound="gvCourses_RowDataBound">
		<Columns>
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireHyperLinkButton Glyphicon="edit" ButtonType="OnlyIcon" runat="server" NavigateUrl="<%# Aspire.Web.Sys.Staff.Courses.Course.GetPageUrl(Item.CourseID) %>" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Intake Semester" SortExpression="SemesterID">
				<ItemTemplate>
					<%#: Item.Semester %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Program" SortExpression="ProgramAlias">
				<ItemTemplate>
					<%#: AspireFormats.GetClassName(Item.ProgramAlias, Item.SemesterNoEnum) %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Code" SortExpression="CourseCode">
				<ItemTemplate>
					<%#: Item.CourseCode %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Title" SortExpression="Title">
				<ItemTemplate>
					<%#: Item.Title %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Credit Hours" SortExpression="CreditHours">
				<ItemTemplate>
					<%#: Item.CreditHoursFullName %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Contact Hours" SortExpression="ContactHours">
				<ItemTemplate>
					<%#: Item.ContactHoursFullName %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Type" SortExpression="CourseTypeEnum">
				<ItemTemplate>
					<%#: Item.CourseTypeFullName %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Category" SortExpression="CourseCategoryEnum">
				<ItemTemplate>
					<%#: Item.CourseCategoryFullName %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Majors" SortExpression="Majors">
				<ItemTemplate>
					<%#: Item.Majors.ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Pre-Reqs">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CausesValidation="False" Text="View" EncryptedCommandArgument="<%# Item.CourseID %>" CommandName="View" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Grade Capping (In Summers)" SortExpression="SummerGradeCapping">
				<ItemTemplate>
					<%#: Item.SummerGradeCappingYesNo %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Remarks" SortExpression="Remarks">
				<ItemTemplate>
					<%#: Item.Remarks.ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<div class="text-center">
		<aspire:AspireHyperLink runat="server" ID="hplCoursesReport" Text="Courses Report" Target="_blank" />
	</div>
	<aspire:AspireModal runat="server" ID="modalImportCourses" HeaderText="Import Courses" ModalSize="None">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelImportCourses" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertImportCourses" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Intake Semester:" AssociatedControlID="ddlSemesterIDImport" />
						<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDImport" ValidationGroup="Import" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterIDImport_OnSelectedIndexChanged" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterIDImport" ErrorMessage="This field is required." ValidationGroup="Import" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlAdmissionOpenProgramIDImport" />
						<aspire:AspireDropDownList runat="server" ID="ddlAdmissionOpenProgramIDImport" ValidationGroup="Import" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlAdmissionOpenProgramIDImport" ErrorMessage="This field is required." ValidationGroup="Import" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Upload CSV File:" AssociatedControlID="fileUpload" />
						<div>
							<aspire:AspireFileUpload runat="server" ID="fileUpload" AllowedFileTypes=".csv" MaxFileSize="52428800" MaxNumberOfFiles="1" />
							<aspire:AspireFileUploadValidator AllowNull="False" runat="server" AspireFileUploadID="fileUpload" ValidationGroup="Import" RequiredErrorMessage="File is not yet uploaded." UploadingInProgressMessage="File upload is in progress." />
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelImportCoursesButtons" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" Text="Import" ID="btnImportCourses" ValidationGroup="Import" OnClick="btnImportCourses_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Close</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
	<aspire:AspireModal runat="server" ID="modalPreReqs" HeaderText="Pre-Requisites" ModalSize="Large">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updatePanelPreReqs">
				<ContentTemplate>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="lblSemester" />
								<div class="form-control-static">
									<aspire:Label runat="server" ID="lblSemester" />
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="lblProgram" />
								<div class="form-control-static">
									<aspire:Label runat="server" ID="lblProgram" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Course Code:" AssociatedControlID="lblCourseCode" />
								<div class="form-control-static">
									<aspire:Label runat="server" ID="lblCourseCode" />
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Course Title:" AssociatedControlID="lblCourseTitle" />
								<div class="form-control-static">
									<aspire:Label runat="server" ID="lblCourseTitle" />
								</div>
							</div>
						</div>
					</div>
					<aspire:AspireAlert runat="server" ID="aspireAlertPreReq" />
					<aspire:AspireGridView runat="server" ID="gvPreReqs" AutoGenerateColumns="False" OnRowCommand="gvPreReqs_OnRowCommand">
						<Columns>
							<asp:TemplateField HeaderText="Pre Requisite 1">
								<ItemTemplate>
									<strong><%# Container.DataItemIndex==0?"(":"OR (" %></strong>
									<%#: Eval("PreReq1Title") %>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="Pre Requisite 2">
								<ItemTemplate>
									<strong><%# Eval("PreReq2Title")==null?"":"AND " %></strong>
									<%#: (string)Eval("PreReq2Title").ToNAIfNullOrEmpty() %>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="Pre Requisite 3">
								<ItemTemplate>
									<strong><%# Eval("PreReq3Title")==null?"":"AND " %></strong>
									<%#: Eval("PreReq3Title").ToNAIfNullOrEmpty()  %>
									<strong>)</strong>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<ItemTemplate>
									<aspire:AspireLinkButton runat="server" Text="Delete" CausesValidation="False" CommandName="Delete" EncryptedCommandArgument='<%# Eval("CoursePreReqID") %>' ConfirmMessage="Are you sure you want to delete this record?" />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</aspire:AspireGridView>
					<div class="panel panel-default">
						<div class="panel-heading">Add Pre-requisite Course(s)</div>
						<div class="panel-body">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Pre Requisite 1:" AssociatedControlID="ddlPreRequisite1" />
								<aspire:AspireDropDownList runat="server" ID="ddlPreRequisite1" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlPreRequisite1" ValidationGroup="PreReqAdd" ErrorMessage="This field is required." />
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Pre Requisite 2:" AssociatedControlID="ddlPreRequisite2" />
								<aspire:AspireDropDownList runat="server" ID="ddlPreRequisite2" />
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Pre Requisite 3:" AssociatedControlID="ddlPreRequisite3" />
								<aspire:AspireDropDownList runat="server" ID="ddlPreRequisite3" />
							</div>
							<div class="form-group text-center">
								<aspire:AspireButton CssClass="btn btn-primary" ValidationGroup="PreReqAdd" runat="server" ID="btnAddPreReq" Text="Add" OnClick="btnAddPreReq_OnClick" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<aspire:AspireModalCloseButton runat="server">Close</aspire:AspireModalCloseButton>
		</FooterTemplate>
	</aspire:AspireModal>
	<script type="text/javascript">
		$(function () {
			var init = function () {
				$("#<%=this.ddlPreRequisite1.ClientID%>")
					.add($("#<%=this.ddlPreRequisite2.ClientID%>"))
					.add($("#<%=this.ddlPreRequisite3.ClientID%>"))
					.applySelect2();
			};
			init();
			Sys.Application.add_load(init);
		});
	</script>
</asp:Content>
