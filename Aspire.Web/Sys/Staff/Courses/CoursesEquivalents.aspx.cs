﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Courses
{
	[AspirePage("F75B8ADD-CA9D-42C5-AFAC-6CAE0540EA0C", UserTypes.Staff, "~/Sys/Staff/Courses/CoursesEquivalents.aspx", "Courses Equivalents", true, AspireModules.Courses)]
	public partial class CoursesEquivalents : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Courses.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Courses Equivalents";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				this.ViewState.SetSortProperties(nameof(Cours.Title), SortDirection.Ascending);
				this.ddlSemesterID.FillSemesters();
				if (this.ddlSemesterID.Items.Count == 0)
				{
					this.AddErrorAlert("No semesters record found.");
					Redirect<CourseEquivalents>();
					return;
				}
				this.ddlCourseCategory.FillCourseCategories(CommonListItems.All);
				this.ddlSemesterNo.FillSemesterNos(CommonListItems.All);
				this.ddlSemesterID_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterID_SelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var admissionOpenPrograms = Aspire.BL.Core.Common.Lists.GetAdmissionOpenPrograms(semesterID, this.StaffIdentity.InstituteID);
			this.ddlAdmissionOpenProgramID.DataBind(admissionOpenPrograms, CommonListItems.All);
			this.ddlAdmissionOpenProgramID_SelectedIndexChanged(null, null);
		}

		protected void ddlAdmissionOpenProgramID_SelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			this.hplCoursesEquivalentsReport.NavigateUrl = Reports.CoursesEquivalents.GetPageUrl(semesterID, null);
			this.ddlSemesterNo_SelectedIndexChanged(null, null);
		}

		protected void ddlSemesterNo_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlCourseCategory_SelectedIndexChanged(null, null);
		}

		protected void ddlCourseCategory_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearch_Click(null, null);
		}

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			this.GetCoursesEquivalents(0);
		}

		protected void gvCoursesEquivalents_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetCoursesEquivalents(0);
		}

		protected void gvCoursesEquivalents_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetCoursesEquivalents(e.NewPageIndex);
		}

		protected void gvCoursesEquivalents_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvCoursesEquivalents.PageSize = e.NewPageSize;
			this.GetCoursesEquivalents(0);
		}

		private void GetCoursesEquivalents(int pageIndex)
		{
			var admissionOpenProgramID = this.ddlAdmissionOpenProgramID.SelectedValue.ToNullableInt();
			var semesterID = this.ddlSemesterID.SelectedItem.Value.ToShort();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
			var searchText = this.tbSearchText.Text;
			var courseCategoryEnum = this.ddlCourseCategory.GetSelectedEnumValue<CourseCategories>(null);
			var result = BL.Core.Courses.Staff.CourseEquivalents.GetEquivalentCourses(semesterID, admissionOpenProgramID, semesterNoEnum, courseCategoryEnum, searchText, pageIndex, this.gvCoursesEquivalents.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.StaffIdentity.LoginSessionGuid);
			this.gvCoursesEquivalents.DataBind(result.EquivalentCourses, pageIndex, result.VirtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		protected void gvCoursesEquivalents_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "DeleteEquivalentCourse":
					e.Handled = true;
					var courseIDForDelete = e.DecryptedCommandArgumentToInt();
					var status = Aspire.BL.Core.Courses.Staff.CourseEquivalents.DeleteEquivalentCourse(courseIDForDelete, this.StaffIdentity.LoginSessionGuid);
					switch (status)
					{
						case BL.Core.Courses.Staff.CourseEquivalents.DeleteEquivalentCourseStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.Courses.Staff.CourseEquivalents.DeleteEquivalentCourseStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Equivalent Course");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.GetCoursesEquivalents(0);
					break;
			}
		}
	}
}