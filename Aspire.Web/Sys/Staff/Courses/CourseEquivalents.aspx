﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CourseEquivalents.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Courses.CourseEquivalents" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<table class="table table-bordered table-condensed table-responsive tableCol4">
		<caption>Course Information</caption>
		<tbody>
			<tr>
				<th>Semester</th>
				<td>
					<aspire:AspireLabel ID="lblSemester" runat="server" />
				</td>
				<th>Credit Hours</th>
				<td>
					<aspire:AspireLabel ID="lblCreditHrs" runat="server" />
				</td>
			</tr>
			<tr>
				<th>Code</th>
				<td>
					<aspire:AspireLabel ID="lblCourseCode" runat="server" /></td>
				<th>Title</th>
				<td>
					<aspire:AspireLabel ID="lblCourseTitle" runat="server" />
				</td>
			</tr>
			<tr>
				<th>Program</th>
				<td>
					<aspire:AspireLabel ID="lblProgram" runat="server" />
				</td>
				<th>Semester No.</th>
				<td>
					<aspire:AspireLabel ID="lblSemesterNo" runat="server" /></td>
			</tr>
		</tbody>
	</table>
	<aspire:AspireGridView ID="gvEquivalentCourses" Caption="Equivalent Courses" runat="server" AllowSorting="True" AutoGenerateColumns="False" OnRowCommand="gvEquivalentCourses_RowCommand" OnSorting="gvEquivalentCourses_Sorting" AllowPaging="True" OnPageIndexChanging="gvEquivalentCourses_PageIndexChanging" OnPageSizeChanging="gvEquivalentCourses_PageSizeChanging">
		<Columns>
			<asp:BoundField DataField="Semester" HeaderText="Roadmap" SortExpression="SemesterID" />
			<asp:BoundField DataField="Class" HeaderText="Class" SortExpression="Class" />
			<asp:BoundField DataField="CourseCode" HeaderText="Code" SortExpression="CourseCode" />
			<asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
			<asp:BoundField DataField="CreditHoursFullName" HeaderText="Credit Hrs." SortExpression="CreditHours" />
			<asp:BoundField DataField="CourseRemarks" HeaderText="Course Remarks" SortExpression="CourseRemarks" />
			<asp:BoundField DataField="EquivalencyRemarks" HeaderText="Remarks" SortExpression="EquivalencyRemarks" />
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" Glyphicon="edit" CausesValidation="False" CommandName="Edit" EncryptedCommandArgument='<%# Eval("CourseEquivalentID") %>' />
					<aspire:AspireLinkButton runat="server" Glyphicon="remove" CausesValidation="False" CommandName="Delete" EncryptedCommandArgument='<%# Eval("CourseEquivalentID") %>' ConfirmMessage="Are you sure you want to delete?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<p></p>
	<div class="row">
		<asp:Panel runat="server" DefaultButton="btnSearch" CssClass="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireButton runat="server" Glyphicon="search" Text="Auto Find" CausesValidation="False" ID="btnAutoFind" OnClick="btnAutoFind_OnClick" />
				</div>
				<div class="form-group pull-right">
					<div class="input-group">
						<aspire:AspireTextBox runat="server" ID="tbSearchText" ValidationGroup="Search" PlaceHolder="Search" />
						<div class="input-group-btn">
							<aspire:AspireButton ButtonType="Default" Glyphicon="search" runat="server" ID="btnSearch" ValidationGroup="Search" OnClick="btnSearch_OnClick" />
						</div>
					</div>
				</div>
			</div>
		</asp:Panel>
	</div>
	<p></p>
	<div class="help-block">Courses from the same department can be added as equivalent courses. Course can not be equivalent to more than one course in target roadmap.</div>
	<aspire:AspireGridView ID="gvCourses" runat="server" DataKeyNames="CourseID" AllowSorting="True" AutoGenerateColumns="False" OnPageSizeChanging="gvCourses_PageSizeChanging" OnSorting="gvCourses_Sorting" OnPageIndexChanging="gvCourses_PageIndexChanging" AllowPaging="True">
		<Columns>
			<asp:TemplateField HeaderText="Select">
				<HeaderTemplate>
					<aspire:AspireCheckBox runat="server" ID="cbSelectAll" />
				</HeaderTemplate>
				<ItemTemplate>
					<aspire:AspireCheckBox runat="server" ID="cbCourseID" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="Semester" HeaderText="Semester" SortExpression="SemesterID" />
			<asp:BoundField DataField="Class" HeaderText="Class" SortExpression="Class" />
			<asp:BoundField DataField="CourseCode" HeaderText="Code" SortExpression="CourseCode" />
			<asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
			<asp:BoundField DataField="CreditHoursFullName" HeaderText="Credit Hrs." SortExpression="CreditHours" />
			<asp:BoundField DataField="Remarks" HeaderText="Course Remarks" SortExpression="Remarks" />
		</Columns>
	</aspire:AspireGridView>
	<script type="text/javascript">
		$(function () {
			var table = $("#<%=this.gvCourses.ClientID%>");
			$("input[id$='cbSelectAll']", table).change(function () { $("input[name*='cbCourseID']", table).prop("checked", $(this).is(":checked")); });
		});
	</script>
	<div class="text-center">
		<aspire:AspireButton ButtonType="Primary" Text="Save" runat="server" ID="btnSaveEquivalentCourses" OnClick="btnSaveEquivalentCourses_Click" />
		<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="CoursesEquivalents.aspx" />
	</div>
	<aspire:AspireModal runat="server" HeaderText="Edit Equivalent Course" ID="modalEditRemarks">
		<BodyTemplate>
			<asp:Panel runat="server" DefaultButton="btnSaveRemarks">
				<div class="form-horizontal">
					<div class="form-group">
						<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Semester:" AssociatedControlID="lblSemesterEdit" />
						<div class="form-control-static col-md-8">
							<aspire:AspireLabel ID="lblSemesterEdit" runat="server" Text="Semester:" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Program:" AssociatedControlID="lblProgramEdit" />
						<div class="form-control-static col-md-8">
							<aspire:AspireLabel ID="lblProgramEdit" runat="server" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Course Code:" AssociatedControlID="lblCourseCodeEdit" />
						<div class="form-control-static col-md-8">
							<aspire:AspireLabel ID="lblCourseCodeEdit" runat="server" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Course Title:" AssociatedControlID="lblCourseTitleEdit" />
						<div class="form-control-static col-md-8">
							<aspire:AspireLabel ID="lblCourseTitleEdit" runat="server" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Credit Hrs:" AssociatedControlID="lblCreditHrsEdit" />
						<div class="form-control-static col-md-8">
							<aspire:AspireLabel ID="lblCreditHrsEdit" runat="server" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Remarks:" CssClass="col-md-4" AssociatedControlID="tbRemarksEdit" />
						<div class="col-md-8">
							<aspire:AspireTextBox TextMode="MultiLine" Rows="2" runat="server" ID="tbRemarksEdit" MaxLength="500" ValidationGroup="Remarks" />
						</div>
					</div>
				</div>
			</asp:Panel>
		</BodyTemplate>
		<FooterTemplate>
			<aspire:AspireButton runat="server" Text="Save" ButtonType="Primary" ID="btnSaveRemarks" ValidationGroup="Remarks" OnClick="btnSaveRemarks_OnClick" />
			<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>

