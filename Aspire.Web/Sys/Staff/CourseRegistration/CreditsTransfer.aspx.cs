﻿using Aspire.BL.Core.CourseRegistration.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.CourseRegistration
{
	[AspirePage("20181F0F-79B3-408F-9622-4EBC0033BD08", UserTypes.Staff, "~/Sys/Staff/CourseRegistration/CreditsTransfer.aspx", "Credits Transfer", true, AspireModules.CourseRegistration)]
	public partial class CreditsTransfer : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.CourseRegistration.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.import);
		public override string PageTitle => "Credits Transfer";

		public static string GetPageUrl(string enrollment, bool search)
		{
			return GetAspirePageAttribute<CreditsTransfer>().PageUrl.AttachQueryParams("Enrollment", enrollment, "Search", search);
		}

		public new static void Redirect(string enrollment, bool search)
		{
			Redirect(GetPageUrl(enrollment, search));
		}

		private void RefreshPage()
		{
			Redirect(this.ucStudentInfo.Enrollment, true);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.tbEnrollment.Enrollment = this.Request.GetParameterValue("Enrollment");
				this.panelCreditsTransfer.Visible = false;
				if (this.Request.GetParameterValue<bool>("Search") == true)
					this.tbEnrollment_OnSearch(null, null);
			}
		}

		private int? StudentCreditsTransferID
		{
			get => (int?)this.ViewState[nameof(this.StudentCreditsTransferID)];
			set => this.ViewState[nameof(this.StudentCreditsTransferID)] = value;
		}

		private int? StudentCreditsTransferredCourseID
		{
			get => (int?)this.ViewState[nameof(this.StudentCreditsTransferredCourseID)];
			set => this.ViewState[nameof(this.StudentCreditsTransferredCourseID)] = value;
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs e)
		{
			if (this.IsPostbackAndNotValid)
				return;
			var enrollment = this.tbEnrollment.Enrollment;
			var studentInfo = this.ucStudentInfo.LoadBasicStudentInformation(enrollment);
			if (studentInfo == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect(enrollment, false);
				return;
			}
			this.panelCreditsTransfer.Visible = true;
			var studentCreditsTransfer = StudentCreditsTransfers.GetStudentCreditsTransfer(studentInfo.StudentID, this.StaffIdentity.LoginSessionGuid);
			this.ddlTransferredSemesterID.FillSemesters();
			if (studentCreditsTransfer == null)
			{
				this.tbInstituteName.Text = null;
				this.dpTransferredDate.SelectedDate = null;
				this.ddlTransferredSemesterID.ClearSelection();
				this.tbCommitteeMembers.Text = null;
				this.tbRemarks.Text = null;
				this.btnDeleteCreditsTransfer.Visible = false;
				this.panelCourses.Visible = false;
				this.StudentCreditsTransferID = null;
				this.StudentCreditsTransferredCourseID = null;
			}
			else
			{
				this.tbInstituteName.Text = studentCreditsTransfer.InstituteName;
				this.dpTransferredDate.SelectedDate = studentCreditsTransfer.TransferredDate;
				this.ddlTransferredSemesterID.SelectedValue = studentCreditsTransfer.TransferredSemesterID.ToString();
				this.tbCommitteeMembers.Text = studentCreditsTransfer.CommitteeMembers;
				this.tbRemarks.Text = studentCreditsTransfer.Remarks;
				this.btnDeleteCreditsTransfer.Visible = true;
				this.btnDeleteCreditsTransfer.Enabled = !studentCreditsTransfer.Courses.Any();
				this.panelCourses.Visible = true;
				this.gvCreditsTransferred.DataBind(studentCreditsTransfer.Courses);
				this.StudentCreditsTransferID = studentCreditsTransfer.StudentCreditsTransferID;
				this.StudentCreditsTransferredCourseID = null;
			}
		}

		#region Student Credits Transfer 

		protected void btnSaveCreditsTransfer_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;

			var studentID = this.ucStudentInfo.StudentID ?? throw new InvalidOperationException();
			var instituteName = this.tbInstituteName.Text;
			var transferredDate = this.dpTransferredDate.SelectedDate ?? throw new InvalidOperationException();
			var transferredSemesterID = this.ddlTransferredSemesterID.SelectedValue.ToShort();
			var committeeMembers = this.tbCommitteeMembers.Text;
			var remarks = this.tbRemarks.Text;
			if (this.StudentCreditsTransferID == null)
			{
				var status = StudentCreditsTransfers.AddStudentCreditsTransfer(studentID, instituteName, transferredDate, transferredSemesterID, committeeMembers, remarks, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case StudentCreditsTransfers.AddStudentCreditsTransferStatus.RecordAlreadyExists:
						this.AddErrorAlert("Student credits transfer record already exists.");
						break;
					case StudentCreditsTransfers.AddStudentCreditsTransferStatus.Success:
						this.AddSuccessMessageHasBeenAdded("Student credits transfer record");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var status = StudentCreditsTransfers.UpdateStudentCreditsTransfer(this.StudentCreditsTransferID.Value, instituteName, transferredDate, transferredSemesterID, committeeMembers, remarks, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case StudentCreditsTransfers.UpdateStudentCreditsTransferStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						break;
					case StudentCreditsTransfers.UpdateStudentCreditsTransferStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Student credits transfer record");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			this.RefreshPage();
		}

		protected void btnDeleteCreditsTransfer_OnClick(object sender, EventArgs e)
		{
			if (this.StudentCreditsTransferID == null)
				throw new InvalidOperationException();
			var status = StudentCreditsTransfers.DeleteStudentCreditsTransfer(this.StudentCreditsTransferID.Value, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case StudentCreditsTransfers.DeleteStudentCreditsTransferStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					break;
				case StudentCreditsTransfers.DeleteStudentCreditsTransferStatuses.ChildRecordExists:
					this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Student Credits Transfer record");
					break;
				case StudentCreditsTransfers.DeleteStudentCreditsTransferStatuses.Success:
					this.AddSuccessMessageHasBeenDeleted("Student Credits Transfer record");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.RefreshPage();
		}

		#endregion

		#region Student Credits Transfered Courses

		private void DisplayCreditsTransferredModal(int? studentCreditsTransferredCourseID)
		{
			if (this.StudentCreditsTransferID == null)
				return;
			var studentID = this.ucStudentInfo.StudentID ?? throw new InvalidOperationException();
			var roadmapCourses = BL.Core.CourseRegistration.Staff.CourseRegistration.GetStudentRoadmapCourses(studentID)
				.Select(c => new ListItem
				{
					Text = $"[{c.CourseCode}] {c.Title} [{c.CreditHours}]",
					Value = c.CourseID.ToString(),
				});
			this.ddlBUCourse.DataBind(roadmapCourses, CommonListItems.Select);
			this.ddlBUGrade.FillExamGrades(CommonListItems.Select);

			if (studentCreditsTransferredCourseID == null)
			{
				this.tbCourseTitle.Text = null;
				this.tbGrade.Text = null;
				this.ddlStatus.ClearSelection();
				this.ddlStatus_OnSelectedIndexChanged(null, null);
				this.ddlBUCourse.ClearSelection();
				this.ddlBUGrade.ClearSelection();

				this.modalCreditsTransferred.HeaderText = "Add Course";
				this.btnSaveCourse.Text = "Add";
				this.StudentCreditsTransferredCourseID = null;
			}
			else
			{
				var studentCreditsTransferredCourse = StudentCreditsTransfers.GetStudentCreditsTransferCourse(studentCreditsTransferredCourseID.Value, this.StaffIdentity.LoginSessionGuid);
				if (studentCreditsTransferredCourse == null)
				{
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				}
				this.tbCourseTitle.Text = studentCreditsTransferredCourse.CourseTitle;
				this.tbGrade.Text = studentCreditsTransferredCourse.Grade;
				this.ddlStatus.SelectedValue = studentCreditsTransferredCourse.EquivalentCourseID != null && studentCreditsTransferredCourse.EquivalentGradeEnum != null
					? "Accepted"
					: "Rejected";
				this.ddlStatus_OnSelectedIndexChanged(null, null);
				this.ddlBUCourse.SelectedValue = studentCreditsTransferredCourse.EquivalentCourseID.ToString();
				this.ddlBUGrade.SetEnumValue(studentCreditsTransferredCourse.EquivalentGradeEnum);

				this.modalCreditsTransferred.HeaderText = "Edit Course Transfer";
				this.btnSaveCourse.Text = "Save";
				this.StudentCreditsTransferredCourseID = studentCreditsTransferredCourseID.Value;
			}
			this.modalCreditsTransferred.Show();
		}

		protected void btnAddCourse_OnClick(object sender, EventArgs e)
		{
			this.DisplayCreditsTransferredModal(null);
		}

		protected void gvCreditsTransferred_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayCreditsTransferredModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var studentCreditTransferredCourseID = e.DecryptedCommandArgumentToInt();
					var status = StudentCreditsTransfers.DeleteStudentCreditsTransferredCourse(studentCreditTransferredCourseID, this.StaffIdentity.LoginSessionGuid);
					switch (status)
					{
						case StudentCreditsTransfers.DeleteStudentCreditsTransferredCourseStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case StudentCreditsTransfers.DeleteStudentCreditsTransferredCourseStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Transferred Course");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.RefreshPage();
					break;
			}
		}

		protected void ddlStatus_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var isAccepted = this.ddlStatus.SelectedValue == "Accepted";
			if (!isAccepted)
			{
				this.ddlBUCourse.ClearSelection();
				this.ddlBUGrade.ClearSelection();
			}

			this.divddlBUCourse.Visible = isAccepted;
			this.divddlBUGrade.Visible = isAccepted;
		}

		protected void btnSaveCourse_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var studentCreditsTransferID = (int?)this.ViewState[nameof(StudentCreditsTransfer.StudentCreditsTransferID)];
			if (studentCreditsTransferID == null)
				return;

			var courseTitle = this.tbCourseTitle.Text;
			var grade = this.tbGrade.Text;
			var accepted = this.ddlStatus.SelectedValue == "Accepted";
			var equivalentCourseID = accepted ? this.ddlBUCourse.SelectedValue.ToInt() : (int?)null;
			var equivalentGradeEnum = accepted ? this.ddlBUGrade.GetSelectedEnumValue<ExamGrades>() : (ExamGrades?)null;

			if (this.StudentCreditsTransferredCourseID == null)
			{
				var status = StudentCreditsTransfers.AddStudentCreditsTransferredCourse(this.StudentCreditsTransferID?? throw new InvalidOperationException(), courseTitle, grade, equivalentCourseID, equivalentGradeEnum, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case StudentCreditsTransfers.AddStudentCreditsTransferredCourseStatuses.NoRecordFound:
						this.RefreshPage();
						break;
					case StudentCreditsTransfers.AddStudentCreditsTransferredCourseStatuses.EquivalentCourseAlreadyExists:
						this.modalAlert.AddMessage(AspireAlert.AlertTypes.Error, "Equivalent course already exists.");
						this.updatePanelCreditsTransferred.Update();
						break;
					case StudentCreditsTransfers.AddStudentCreditsTransferredCourseStatuses.CourseTitleAlreadyExists:
						this.modalAlert.AddMessage(AspireAlert.AlertTypes.Error, "Course Title already exists.");
						this.updatePanelCreditsTransferred.Update();
						break;
					case StudentCreditsTransfers.AddStudentCreditsTransferredCourseStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Course");
						this.RefreshPage();
						break;
					case StudentCreditsTransfers.AddStudentCreditsTransferredCourseStatuses.ExamMarksPolicyNotFound:
						this.modalAlert.AddMessage(AspireAlert.AlertTypes.Error, "Exam Marks Policy is set for the Transferred Semester.");
						this.updatePanelCreditsTransferred.Update();
						break;
					case StudentCreditsTransfers.AddStudentCreditsTransferredCourseStatuses.EquivalentGradeNotAllowedAsPerExamPolicy:
						this.modalAlert.AddMessage(AspireAlert.AlertTypes.Error, "As per Exam Marks Policy, selected Equivalent Grade is not allowed.");
						this.updatePanelCreditsTransferred.Update();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var status = StudentCreditsTransfers.UpdateStudentCreditsTransferredCourse(this.StudentCreditsTransferredCourseID.Value, courseTitle, grade, equivalentCourseID, equivalentGradeEnum, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case StudentCreditsTransfers.UpdateStudentCreditsTransferredCourseStatuses.NoRecordFound:
						this.RefreshPage();
						break;
					case StudentCreditsTransfers.UpdateStudentCreditsTransferredCourseStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Course");
						this.RefreshPage();
						break;
					case StudentCreditsTransfers.UpdateStudentCreditsTransferredCourseStatuses.EquivalentCourseAlreadyExists:
						this.modalAlert.AddMessage(AspireAlert.AlertTypes.Error, "Equivalent course already exists.");
						this.updatePanelCreditsTransferred.Update();
						break;
					case StudentCreditsTransfers.UpdateStudentCreditsTransferredCourseStatuses.CourseTitleAlreadyExists:
						this.modalAlert.AddMessage(AspireAlert.AlertTypes.Error, "Course Title already exists.");
						this.updatePanelCreditsTransferred.Update();
						break;
					case StudentCreditsTransfers.UpdateStudentCreditsTransferredCourseStatuses.ExamMarksPolicyNotFound:
						this.modalAlert.AddMessage(AspireAlert.AlertTypes.Error, "Exam Marks Policy is set for the Transferred Semester.");
						this.updatePanelCreditsTransferred.Update();
						break;
					case StudentCreditsTransfers.UpdateStudentCreditsTransferredCourseStatuses.EquivalentGradeNotAllowedAsPerExamPolicy:
						this.modalAlert.AddMessage(AspireAlert.AlertTypes.Error, "As per Exam Marks Policy, selected Equivalent Grade is not allowed.");
						this.updatePanelCreditsTransferred.Update();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		#endregion
	}
}