﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.CourseRegistration
{
	[AspirePage("23BF9744-06A1-484C-B480-CA21788B9A81", UserTypes.Staff, "~/Sys/Staff/CourseRegistration/ExemptedCourses.aspx", "Exempted Courses", true, AspireModules.CourseRegistration)]
	public partial class ExemptedCourses : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.CourseRegistration.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.import);
		public override string PageTitle => "Exempted Courses";

		public static string GetPageUrl(int? studentID)
		{
			return GetAspirePageAttribute<ExemptedCourses>().PageUrl.AttachQueryParam("StudentID", studentID);
		}

		public static string GetPageUrl(string enrollment)
		{
			return GetAspirePageAttribute<ExemptedCourses>().PageUrl.AttachQueryParam("Enrollment", enrollment);
		}

		public static void Redirect(int? studentID)
		{
			Redirect<ExemptedCourses>("StudentID", studentID);
		}

		public static void Redirect(string enrollment)
		{
			Redirect<ExemptedCourses>("Enrollment", enrollment);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Reset();
				string enrollment = null;
				var studentID = this.Request.GetParameterValue<int>("StudentID");
				if (studentID != null)
					enrollment = BL.Common.Common.GetEnrollmentFromStudentID(studentID.Value);
				if (enrollment == null)
					enrollment = this.Request.GetParameterValue("Enrollment");
				if (!string.IsNullOrWhiteSpace(enrollment))
				{
					this.tbSearchEnrollment.Enrollment = enrollment;
					this.tbEnrollment_OnSearch(null, null);
				}
			}
		}

		private void Reset()
		{
			this.panelExemptedCourses.Visible = false;
			this.ucStudentBasicInfo.Reset();
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs e)
		{
			var enrollment = this.tbSearchEnrollment.Enrollment;
			var studentInfo = this.ucStudentBasicInfo.LoadBasicStudentInformation(enrollment);
			if (studentInfo == null)
			{
				this.AddNoRecordFoundAlert();
				this.Reset();
				return;
			}
			this.panelExemptedCourses.Visible = true;
			var exemptedCourses = BL.Core.CourseRegistration.Staff.ExemptedCourses.GetStudentExemptedCourses(studentInfo.StudentID, this.StaffIdentity.LoginSessionGuid);
			this.gvExemptedCourses.DataBind(exemptedCourses);
		}

		protected void btnAddExemptedCourse_OnClick(object sender, EventArgs e)
		{
			this.DisplayModalAddCourse(null);
		}

		protected void gvExemptedCourses_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					var studentExemptedCourseID = e.DecryptedCommandArgumentToInt();
					this.DisplayModalAddCourse(studentExemptedCourseID);
					break;
				case "Delete":
					e.Handled = true;
					var exemptedCourseID = e.DecryptedCommandArgumentToInt();
					var status = BL.Core.CourseRegistration.Staff.ExemptedCourses.DeleteStudentExemptedCourse(exemptedCourseID, this.StaffIdentity.LoginSessionGuid);
					switch (status)
					{
						case BL.Core.CourseRegistration.Staff.ExemptedCourses.DeleteStudentExemptedCourseStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.tbEnrollment_OnSearch(null, null);
							break;
						case BL.Core.CourseRegistration.Staff.ExemptedCourses.DeleteStudentExemptedCourseStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Exempted course");
							var studentID = this.ucStudentBasicInfo.StudentID ?? throw new InvalidOperationException();
							var courses = BL.Core.CourseRegistration.Staff.ExemptedCourses.GetStudentExemptedCourses(studentID, this.StaffIdentity.LoginSessionGuid);
							this.gvExemptedCourses.DataBind(courses);
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		protected void bntAddEditCourse_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;

			var studentID = this.ucStudentBasicInfo.StudentID ?? throw new InvalidOperationException();
			var courseID = this.ddlCourseID.SelectedValue.ToInt();
			var substitutedCourseID = this.ddlSubstitutedCourseID.SelectedValue.ToNullableInt();
			var exemptionDate = this.dtpExemptionDate.SelectedDate ?? throw new InvalidOperationException();
			var remarks = this.tbRemarks.Text;
			var studentExemptedCourseID = (int?)this.ViewState["StudentExemptedCourseID"];
			if (studentExemptedCourseID != null)
			{
				var updated = BL.Core.CourseRegistration.Staff.ExemptedCourses.UpdateStudentExemptedCourse(studentExemptedCourseID.Value, exemptionDate, substitutedCourseID, remarks, this.StaffIdentity.LoginSessionGuid);
				switch (updated)
				{
					case BL.Core.CourseRegistration.Staff.ExemptedCourses.UpdateStudentExemptedCourseStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						break;
					case BL.Core.CourseRegistration.Staff.ExemptedCourses.UpdateStudentExemptedCourseStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Exempted course");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var status = BL.Core.CourseRegistration.Staff.ExemptedCourses.AddStudentExemptedCourse(studentID, courseID, exemptionDate, substitutedCourseID, remarks, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.CourseRegistration.Staff.ExemptedCourses.AddStudentExemptedCourseStatuses.CourseAlreadyExists:
						this.AddWarningAlert("Exempted course already exists");
						break;
					case BL.Core.CourseRegistration.Staff.ExemptedCourses.AddStudentExemptedCourseStatuses.NoRecordFound:
						break;
					case BL.Core.CourseRegistration.Staff.ExemptedCourses.AddStudentExemptedCourseStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Exempted course");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			var exemptedCourses = BL.Core.CourseRegistration.Staff.ExemptedCourses.GetStudentExemptedCourses(studentID, this.StaffIdentity.LoginSessionGuid);
			this.gvExemptedCourses.DataBind(exemptedCourses);
		}

		private void DisplayModalAddCourse(int? studentExemptedCourseID)
		{
			var studentID = this.ucStudentBasicInfo.StudentID ?? throw new InvalidOperationException();
			var courses = BL.Core.CourseRegistration.Staff.CourseRegistration.GetStudentRoadmapCourses(studentID).Select(c => new ListItem
			{
				Text = $"[{c.CourseCode}] {c.Title} [{c.CreditHoursFullName}]",
				Value = c.CourseID.ToString()
			}).ToList();
			this.ddlCourseID.DataBind(courses, CommonListItems.Select);
			this.ddlSubstitutedCourseID.DataBind(courses, CommonListItems.None);

			if (studentExemptedCourseID == null)
			{
				this.ddlCourseID.Enabled = true;
				this.ddlCourseID.ClearSelection();
				this.ddlSubstitutedCourseID.ClearSelection();
				this.dtpExemptionDate.SelectedDate = null;
				this.tbRemarks.Text = null;
				this.bntAddEditCourse.Text = "Add";
				this.modalAddEditExemptedCourse.HeaderText = "Add Exempted Course";
				this.ViewState["StudentExemptedCourseID"] = null;
			}
			else
			{
				var studentExemptedCourse = BL.Core.CourseRegistration.Staff.ExemptedCourses.GetStudentExemptedCourse(studentExemptedCourseID.Value, this.StaffIdentity.LoginSessionGuid);
				if (studentExemptedCourse == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect(this.ucStudentBasicInfo.Enrollment);
					return;
				}
				this.ddlCourseID.SelectedValue = studentExemptedCourse.CourseID.ToString();
				this.ddlCourseID.Enabled = false;
				this.ddlSubstitutedCourseID.SelectedValue = studentExemptedCourse.SubstitutedCourseID.ToString();
				this.dtpExemptionDate.SelectedDate = studentExemptedCourse.ExemptionDate;
				this.tbRemarks.Text = studentExemptedCourse.Remarks;
				this.bntAddEditCourse.Text = "Save";
				this.modalAddEditExemptedCourse.HeaderText = "Edit Exempted Course";
				this.ViewState["StudentExemptedCourseID"] = studentExemptedCourse.StudentExemptedCourseID;
			}
			this.modalAddEditExemptedCourse.Show();
		}
	}
}