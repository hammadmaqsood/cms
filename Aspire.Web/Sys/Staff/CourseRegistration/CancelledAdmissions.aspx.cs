﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.CourseRegistration
{
	[AspirePage("60E4F148-CBD0-4B9F-A6F0-97F4674E819A", UserTypes.Staff, "~/Sys/Staff/CourseRegistration/CancelledAdmissions.aspx", "Canceled Admission", true, AspireModules.CourseRegistration)]
	public partial class CancelledAdmissions : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.CourseRegistration.Module, UserGroupPermission.Allowed}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Canceled Admissions";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var studentID = this.Request.GetParameterValue<int>("StudentID");
				if (studentID != null)
					this.tbEnrollment.Enrollment = Aspire.BL.Common.Common.GetEnrollmentFromStudentID(studentID.Value);
				else
					this.tbEnrollment.Enrollment = this.Request.GetParameterValue("Enrollment");
				if (!string.IsNullOrWhiteSpace(this.tbEnrollment.Enrollment))
					this.tbEnrollment_OnSearch(null, null);
			}
		}

		private void Reset()
		{
			this.ucStudentInfo.Reset();
			this.panelCancelledAdmission.Visible = false;
		}

		private void Redirect()
		{
			var enrollment = this.tbEnrollment.Enrollment;
			Redirect<CancelledAdmissions>("Enrollment", enrollment);
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs args)
		{
			var enrollment = this.tbEnrollment.Enrollment;
			if (string.IsNullOrWhiteSpace(enrollment))
			{
				this.Reset();
				this.AddNoRecordFoundAlert();
				return;
			}
			var studentinfo = this.ucStudentInfo.LoadBasicStudentInformation(enrollment);
			if (studentinfo == null)
			{
				this.Reset();
				this.AddNoRecordFoundAlert();
				return;
			}
			this.ViewState[nameof(StudentSemester.StudentID)] = studentinfo.StudentID;

			var studentSemesterSummary = BL.Core.CourseRegistration.Staff.CancelledAdmissions.GetStudentSemesters(studentinfo.StudentID, this.StaffIdentity.LoginSessionGuid);
			this.gvStudentSemesters.DataBind(studentSemesterSummary);

			var cancelledAdmission = BL.Core.CourseRegistration.Staff.CancelledAdmissions.GetCancelledAdmissions(studentinfo.StudentID, this.StaffIdentity.LoginSessionGuid);
			this.gvCancelledAdmission.DataBind(cancelledAdmission);
			this.panelCancelledAdmission.Visible = true;
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var cancelledAdmissionID = (int?)this.ViewState[nameof(CancelledAdmission.CancelledAdmissionID)];
			var studentID = (int)this.ViewState[nameof(StudentSemester.StudentID)];
			var statusEnum = this.rblStatus.GetSelectedEnumValue<CancelledAdmission.Statuses>();
			var remarks = this.tbRemarks.Text;
			if (cancelledAdmissionID == null)
			{

				var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
				var result = BL.Core.CourseRegistration.Staff.CancelledAdmissions.AddCancelledAdmission(studentID, semesterID, statusEnum, remarks, this.StaffIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.CourseRegistration.Staff.CancelledAdmissions.AddCancelledAdmissionStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.Redirect();
						break;
					case BL.Core.CourseRegistration.Staff.CancelledAdmissions.AddCancelledAdmissionStatuses.AlreadyExists:
						this.alertModalCancelledAdmission.AddMessage(AspireAlert.AlertTypes.Error, "Cancelled Admission on selected semester already exists.");
						this.updateModalCancelledAdmission.Update();
						break;
					case BL.Core.CourseRegistration.Staff.CancelledAdmissions.AddCancelledAdmissionStatuses.InvalidSemester:
						this.alertModalCancelledAdmission.AddMessage(AspireAlert.AlertTypes.Error, "Selected semester is invalid for admission cancelled.");
						this.updateModalCancelledAdmission.Update();
						break;
					case BL.Core.CourseRegistration.Staff.CancelledAdmissions.AddCancelledAdmissionStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Cancelled admission");
						this.Redirect();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var result = BL.Core.CourseRegistration.Staff.CancelledAdmissions.UpdateCancelledAdmission(cancelledAdmissionID.Value, studentID, statusEnum, remarks, this.StaffIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.CourseRegistration.Staff.CancelledAdmissions.UpdateCancelledAdmissionStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.Redirect();
						break;
					case BL.Core.CourseRegistration.Staff.CancelledAdmissions.UpdateCancelledAdmissionStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Cancelled admission");
						this.Redirect();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		protected void btnAddCancelledAdmission_OnClick(object sender, EventArgs e)
		{
			this.DisplayModalCancelledAdmission(null);
		}

		private void DisplayModalCancelledAdmission(int? cancelledAdmissionID)
		{
			var studentID = (int?)this.ViewState[nameof(StudentSemester.StudentID)];
			if (studentID == null)
				return;
			this.ddlSemesterID.FillSemesters();
			this.rblStatus.FillEnums<CancelledAdmission.Statuses>();

			if (cancelledAdmissionID == null)
			{
				this.ddlSemesterID.ClearSelection();
				this.ddlSemesterID.Enabled = true;
				this.rblStatus.SetEnumValue(CancelledAdmission.StatusBlocked);
				this.tbRemarks.Text = null;

				this.modalCancelledAdmission.HeaderText = "Add Cancelled Admission";
				this.btnSave.Text = @"Add";
				this.ViewState[nameof(CancelledAdmission.CancelledAdmissionID)] = null;
				this.modalCancelledAdmission.Show();
			}
			else
			{
				var cancelledAdmission = BL.Core.CourseRegistration.Staff.CancelledAdmissions.GetCancelledAdmission(cancelledAdmissionID.Value, this.StaffIdentity.LoginSessionGuid);
				this.ddlSemesterID.SelectedValue = cancelledAdmission.SemesterID.ToString();
				this.ddlSemesterID.Enabled = false;
				this.rblStatus.SetEnumValue(cancelledAdmission.StatusEnum);
				this.tbRemarks.Text = cancelledAdmission.Remarks;

				this.modalCancelledAdmission.HeaderText = "Edit Cancelled Admission";
				this.btnSave.Text = @"Save";
				this.ViewState[nameof(CancelledAdmission.CancelledAdmissionID)] = cancelledAdmissionID;
				this.modalCancelledAdmission.Show();
			}
		}

		protected void gvCancelledAdmission_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayModalCancelledAdmission(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var studentID = (int)this.ViewState[nameof(StudentSemester.StudentID)];
					var result = BL.Core.CourseRegistration.Staff.CancelledAdmissions.DeleteCanCelledAdmission(e.DecryptedCommandArgumentToInt(), studentID, this.StaffIdentity.LoginSessionGuid);
					switch (result)
					{
						case BL.Core.CourseRegistration.Staff.CancelledAdmissions.DeleteCancelledAdmissionStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.Redirect();
							break;
						case BL.Core.CourseRegistration.Staff.CancelledAdmissions.DeleteCancelledAdmissionStatuses.Success:
							this.AddSuccessMessageHasBeenUpdated("Cancelled admission");
							this.Redirect();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}
	}
}