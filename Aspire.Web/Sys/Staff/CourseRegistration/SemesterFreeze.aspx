﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="SemesterFreeze.aspx.cs" Inherits="Aspire.Web.Sys.Staff.CourseRegistration.SemesterFreeze" %>

<%@ Register TagPrefix="uc" TagName="StudentInfo" Src="~/Sys/Staff/Common/StudentInfoUserControl.ascx" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
					<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="Search" OnSearch="tbEnrollment_OnSearch" />
				</div>
				<div class="form-group">
					<aspire:AspireHyperLink runat="server" Text="Clear" NavigateUrl="SemesterFreeze.aspx" />
				</div>
			</div>
		</div>
	</div>
	<asp:Panel runat="server" ID="panelDetails">
		<p></p>
		<uc:StudentInfo runat="server" ID="ucStudentInfo" />
		<div class="row">
			<div class="col-md-12">
				<div class="form-inline">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
						<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
					</div>
				</div>
			</div>
		</div>
		<p></p>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">
					<aspire:AspireLabel runat="server" ID="lblSemester" /></h3>
			</div>
			<div class="panel-body">
				<table class="table table-bordered table-condensed tableCol6">
					<tbody>
						<tr>
							<th>Class</th>
							<td>
								<aspire:AspireLabel runat="server" ID="lblClass" /></td>
							<th>Freezed Status</th>
							<td>
								<aspire:AspireLabel runat="server" ID="lblFreezedStatus" /></td>
							<th>Freezed Date</th>
							<td>
								<aspire:AspireLabel runat="server" ID="lblFreezedDate" /></td>
						</tr>
						<tr>
							<th>Remarks</th>
							<td colspan="5">
								<aspire:AspireLabel runat="server" ID="lblRemarks" />
							</td>
						</tr>
					</tbody>
				</table>
				<aspire:AspireGridView Caption="Registered Courses" runat="server" ID="gvRegisteredCourses" AutoGenerateColumns="False">
					<Columns>
						<asp:BoundField HeaderText="Offered Title" DataField="OfferedTitle" />
						<asp:BoundField HeaderText="Offered Class" DataField="OfferedClass" />
						<asp:BoundField HeaderText="Status" DataField="StatusFullName" />
						<asp:BoundField HeaderText="Freezed Status" DataField="FreezedStatusFullName" />
						<asp:BoundField HeaderText="Freezed Date" DataField="FreezedDate" DataFormatString="{0:d}" />
					</Columns>
				</aspire:AspireGridView>
				<div class="form-horizontal">
					<asp:UpdatePanel runat="server">
						<ContentTemplate>
							<div class="form-group">
								<aspire:AspireLabel runat="server" AssociatedControlID="ddlStatus" CssClass="col-md-2" Text="Freezed Status:" />
								<div class="col-md-4">
									<aspire:AspireDropDownList ID="ddlStatus" ValidationGroup="Save" runat="server" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlStatus_OnSelectedIndexChanged" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" AssociatedControlID="dtpFreezedDate" CssClass="col-md-2" Text="Freezed Date:" />
								<div class="col-md-4">
									<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpFreezedDate" />
								</div>
								<div class="col-md-4 form-control-static">
									<aspire:AspireDateTimeValidator ValidationGroup="Save" ID="dtpvFreezedDate" runat="server" ControlToValidate="dtpFreezedDate" InvalidDataErrorMessage="Invalid Date." RequiredErrorMessage="This field is required." />
								</div>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
					<div class="form-group">
						<aspire:AspireLabel runat="server" CssClass="col-md-2" AssociatedControlID="tbRemarks" Text="Remarks:" />
						<div class="col-md-10">
							<aspire:AspireTextBox ID="tbRemarks" runat="server" ValidationGroup="Save" MaxLength="500" TextMode="MultiLine" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-2 col-md-10">
							<aspire:AspireButton ID="btnSave" runat="server" ValidationGroup="Save" ConfirmMessage="Are you sure you want to save?" ButtonType="Primary" OnClick="btnSave_OnClick" Text="Save" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</asp:Panel>
</asp:Content>
