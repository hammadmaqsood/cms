﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="RegisteredCourses.aspx.cs" Inherits="Aspire.Web.Sys.Staff.CourseRegistration.RegisteredCourses" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Offered Semester:" AssociatedControlID="ddlSemesterID" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-3 form-group">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" OnSelectedIndexChanged="ddlDepartmentID_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-3 form-group">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramID" OnSelectedIndexChanged="ddlProgramID_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-1 form-group">
			<aspire:AspireLabel runat="server" Text="Sem. #:" AssociatedControlID="ddlSemesterNo" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" OnSelectedIndexChanged="ddlSemesterNo_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-1 form-group">
			<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSection" />
			<aspire:AspireDropDownList runat="server" ID="ddlSection" OnSelectedIndexChanged="ddlSection_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShift" />
			<aspire:AspireDropDownList runat="server" ID="ddlShift" OnSelectedIndexChanged="ddlShift_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
	</div>
	<div class="row">
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Report Type:" AssociatedControlID="ddlReportType" />
			<aspire:AspireDropDownList runat="server" ID="ddlReportType" OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True">
				<asp:ListItem Text="Summary" Value="Summary" />
			</aspire:AspireDropDownList>
		</div>
		<div class="col-md-2 form-group" runat="server" id="divCreditHours">
			<aspire:AspireLabel runat="server" Text="Credit Hours:" AssociatedControlID="ddlCreditHours" />
			<aspire:AspireDropDownList runat="server" ID="ddlCreditHours" OnSelectedIndexChanged="ddlCreditHours_SelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group" runat="server" id="divRegisteredCourses">
			<aspire:AspireLabel runat="server" Text="Registered Courses:" AssociatedControlID="ddlRegisteredCourses" />
			<aspire:AspireDropDownList runat="server" ID="ddlRegisteredCourses" OnSelectedIndexChanged="ddlRegisteredCourses_SelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
	</div>
	<aspire:AspireGridView runat="server" ID="gvSummary" ItemType="Aspire.BL.Core.CourseRegistration.Staff.CourseRegistration.StudentWiseRegisteredCoursesSummary" AutoGenerateColumns="False" OnPageSizeChanging="gvSummary_OnPageSizeChanging" OnPageIndexChanging="gvSummary_OnPageIndexChanging" OnSorting="gvSummary_OnSorting" AllowSorting="True">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%#: Container.DataItemIndex + 1 %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Offered Semester" SortExpression="SemesterID">
				<ItemTemplate><%#: Item.SemesterID.ToSemesterString() %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Program" SortExpression="ProgramAlias">
				<ItemTemplate><%#: Item.ProgramAlias %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Sem#" SortExpression="SemesterNoEnum">
				<ItemTemplate><%#: Item.SemesterNoEnum.ToFullName() %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Section" SortExpression="SectionEnum">
				<ItemTemplate><%#: Item.SectionEnum.ToFullName() %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Shift" SortExpression="ShiftEnum">
				<ItemTemplate><%#: Item.ShiftEnum %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Enrollment" SortExpression="Enrollment">
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" Text="<%# Item.Enrollment %>" Target="_blank" NavigateUrl="<%# Aspire.Web.Sys.Staff.CourseRegistration.RegisterCourse.GetPageUrl(Item.SemesterID, Item.Enrollment) %>" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Credit Hours" SortExpression="CreditHours">
				<ItemTemplate><%#: Item.CreditHours %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Registered Courses" SortExpression="Courses">
				<ItemTemplate><%#: Item.Courses %></ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<aspire:AspireGridView runat="server" ID="gvRegisteredCourses" AutoGenerateColumns="False" OnPageSizeChanging="gvRegisteredCourses_PageSizeChanging" OnPageIndexChanging="gvRegisteredCourses_PageIndexChanging" OnSorting="gvRegisteredCourses_Sorting" AllowSorting="True">
		<Columns>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
