﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.CourseRegistration
{
	[AspirePage("ab80f3e0-9a7c-497f-becd-7faa9ee34fa0", UserTypes.Staff, "~/Sys/Staff/CourseRegistration/RegisteredCourses.aspx", "Student-wise Registered Courses", true, AspireModules.CourseRegistration)]
	public partial class RegisteredCourses : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Exams.Module, new []{UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => AspireGlyphicons.book.GetIcon();
		public override string PageTitle => "Student-wise Registered Courses";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Enrollment", SortDirection.Ascending);
				this.ViewState.SetSortProperties1("Enrollment", SortDirection.Ascending);
				this.ddlSemesterID.FillSemesters();
				this.ddlDepartmentID.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All);
				this.ddlSemesterNo.FillSemesterNos(CommonListItems.All);
				this.ddlSection.FillSections(CommonListItems.All);
				this.ddlShift.FillShifts(CommonListItems.All);
				var creditHours = Enumerable.Range(1, 24).Select(i => new ListItem($"{i} or Greater", i.ToString())).ToList();
				this.ddlCreditHours.DataBind(creditHours, CommonListItems.All);
				var registeredCourses = Enumerable.Range(1, 12).Select(i => new ListItem($"{i} or Greater", i.ToString())).ToList();
				this.ddlRegisteredCourses.DataBind(registeredCourses, CommonListItems.All);
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			this.ddlProgramID.FillPrograms(this.StaffIdentity.InstituteID, departmentID, true, CommonListItems.All);
			this.ddlProgramID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterNo_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterNo_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSection_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSection_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlShift_OnSelectedIndexChanged(null, null);
		}

		protected void ddlShift_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlReportType_SelectedIndexChanged(null, null);
		}

		protected void ddlReportType_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch (this.ddlReportType.SelectedValue)
			{
				case "Summary":
					this.divCreditHours.Visible = true;
					this.divRegisteredCourses.Visible = true;
					this.gvSummary.Visible = true;
					this.gvRegisteredCourses.Visible = false;
					this.ddlCreditHours_SelectedIndexChanged(null, null);
					break;
				case "Courses":
					this.divCreditHours.Visible = false;
					this.divRegisteredCourses.Visible = false;
					this.gvSummary.Visible = false;
					this.gvRegisteredCourses.Visible = true;
					this.GetRegisteredCourses(0);
					break;
				default:
					throw new NotImplementedException();
			}
		}

		protected void ddlCreditHours_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlRegisteredCourses_SelectedIndexChanged(null, null);
		}

		protected void ddlRegisteredCourses_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetSummary(0);
		}

		protected void gvSummary_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetSummary(0);
		}

		protected void gvSummary_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvSummary.PageSize = e.NewPageSize;
			this.GetSummary(0);
		}

		protected void gvSummary_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetSummary(e.NewPageIndex);
		}

		protected void gvRegisteredCourses_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvRegisteredCourses.PageSize = e.NewPageSize;
			this.GetRegisteredCourses(0);
		}

		protected void gvRegisteredCourses_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetRegisteredCourses(e.NewPageIndex);
		}

		protected void gvRegisteredCourses_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs1(this.ViewState);
			this.GetRegisteredCourses(0);
		}

		private void GetSummary(int pageIndex)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
			var sectionEnum = this.ddlSection.GetSelectedEnumValue<Sections>(null);
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>(null);
			var creditHours = this.ddlCreditHours.SelectedValue.ToNullableInt();
			var registeredCourses = this.ddlRegisteredCourses.SelectedValue.ToNullableInt();
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var summary = BL.Core.CourseRegistration.Staff.CourseRegistration.GetStudentWiseRegisteredCoursesSummary(semesterID, departmentID, programID, semesterNoEnum, sectionEnum, shiftEnum, creditHours, registeredCourses, pageIndex, this.gvSummary.PageSize, out var virtualItemCount, sortExpression, sortDirection, this.StaffIdentity.LoginSessionGuid);
			this.gvSummary.DataBind(summary, pageIndex, virtualItemCount, sortExpression, sortDirection);
		}

		private void GetRegisteredCourses(int pageIndex)
		{
			//var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			//var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			//var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			//var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
			//var sectionEnum = this.ddlSection.GetSelectedEnumValue<Sections>(null);
			//var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>(null);
			//var sortExpression = this.ViewState.GetSortExpression1();
			//var sortDirection = this.ViewState.GetSortDirection1();
			//var summary = BL.Core.CourseRegistration.Staff.CourseRegistration.GetStudentWiseRegisteredCourses(semesterID, departmentID, programID, semesterNoEnum, sectionEnum, shiftEnum, pageIndex, this.gvSummary.PageSize, out var virtualItemCount, sortExpression, sortDirection, this.StaffIdentity.LoginSessionGuid);
			//this.gvRegisteredCourses.DataBind(summary, pageIndex, virtualItemCount, sortExpression, sortDirection);
		}
	}
}