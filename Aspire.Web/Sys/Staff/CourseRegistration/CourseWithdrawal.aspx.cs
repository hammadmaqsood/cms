﻿using Aspire.BL.Core.CourseRegistration.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.CourseRegistration
{
	[AspirePage("052A75B2-C753-4513-8254-61E8C8A210B4", UserTypes.Staff, "~/Sys/Staff/CourseRegistration/CourseWithdrawal.aspx", "Withdraw Course", true, AspireModules.CourseRegistration)]
	public partial class CourseWithdrawal : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.CourseRegistration.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Withdraw Course";

		public static void Redirect(string enrollment, short? semesterID)
		{
			Redirect<CourseWithdrawal>("Enrollment", enrollment, "SemesterID", semesterID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Reset();
				var enrollment = this.Request.GetParameterValue("Enrollment");
				if (!string.IsNullOrWhiteSpace(enrollment))
				{
					this.tbEnrollment.Enrollment = enrollment;
					this.tbEnrollment_OnSearch(null, null);
				}
			}
		}

		private void Reset()
		{
			this.ucStudentInfo.Reset();
			this.panelDetails.Visible = false;
			this.panelCourseDetails.Visible = false;
		}

		private void RefreshPage()
		{
			Redirect(this.ucStudentInfo.Enrollment, this.ddlSemesterID.SelectedValue.ToNullableShort());
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs args)
		{
			var enrollment = this.tbEnrollment.Enrollment;
			if (string.IsNullOrWhiteSpace(enrollment))
			{
				this.AddNoRecordFoundAlert();
				this.Reset();
				return;
			}
			var studentInfo = this.ucStudentInfo.LoadBasicStudentInformation(enrollment);
			if (studentInfo == null)
			{
				this.Reset();
				this.AddNoRecordFoundAlert();
				return;
			}
			this.panelDetails.Visible = true;
			this.panelCourseDetails.Visible = true;
			this.ddlSemesterID.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
			this.ddlSemesterID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var studentID = this.ucStudentInfo.StudentID ?? throw new InvalidOperationException();
			var programAlias = this.ucStudentInfo.ProgramAlias;
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			this.lblSemester.Text = semesterID.ToSemesterString();

			var studentSemester = BL.Core.CourseRegistration.Staff.StudentSemesters.GetStudentSemester(studentID, semesterID, this.StaffIdentity.LoginSessionGuid);
			if (studentSemester != null)
			{
				this.lblClass.Text = AspireFormats.GetClassName(programAlias, studentSemester.SemesterNoEnum, studentSemester.SectionEnum, studentSemester.ShiftEnum);
				var registeredCourses = BL.Core.CourseRegistration.Staff.CourseRegistration.GetStudentRegisteredCourses(studentID, semesterID, this.StaffIdentity.LoginSessionGuid);
				this.gvRegisteredCourses.DataBind(registeredCourses);
				this.gvRegisteredCourses.Columns[this.gvRegisteredCourses.Columns.Count - 1].Visible = studentSemester.FreezedStatus == null;
				this.ViewState["StudentSemesterID"] = studentSemester.StudentSemesterID;
			}
			else
			{
				this.lblClass.Text = string.Empty.ToNAIfNullOrEmpty();
				this.gvRegisteredCourses.DataBindNull();
				this.ddlStatuses_OnSelectedIndexChanged(null, null);
				this.ViewState["StudentSemesterID"] = null;
			}
		}

		protected void ddlStatuses_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var status = this.ddlStatuses.SelectedValue.ToNullableByte();
			this.dtpvStatus.AllowNull = status == null;
			this.dtpStatusDate.ReadOnly = status == null;
			if (status == null)
				this.dtpStatusDate.SelectedDate = null;
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var registeredCourseID = (int)this.ViewState["RegisteredCourseID"];
			var statusEnum = this.ddlStatuses.GetSelectedEnumValue<RegisteredCours.Statuses>(null);
			var statusDate = statusEnum == null ? (DateTime?)null : this.dtpStatusDate.SelectedDate ?? throw new InvalidOperationException();
			var remarks = this.tbRemarks.Text;
			var result = BL.Core.CourseRegistration.Staff.RegisteredCourseStatus.UpdateRegisteredCourseStatus(registeredCourseID, statusEnum, statusDate, remarks, this.StaffIdentity.LoginSessionGuid);
			switch (result)
			{
				case RegisteredCourseStatus.UpdateRegisteredCourseStatusResult.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case RegisteredCourseStatus.UpdateRegisteredCourseStatusResult.Success:
					this.AddSuccessMessageHasBeenUpdated("Registered Course Status");
					this.RefreshPage();
					return;
				case RegisteredCourseStatus.UpdateRegisteredCourseStatusResult.MarksEntryLocked:
					this.AddErrorAlert("Registered Course Status can not be updated because marks entry for this record has been locked.");
					this.RefreshPage();
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void gvRegisteredCourses_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					var registeredCourse = Aspire.BL.Core.CourseRegistration.Staff.CourseRegistration.GetStudentRegisteredCourse(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					if (registeredCourse == null)
					{
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						return;
					}

					if (registeredCourse.MarksEntryLocked)
					{
						this.AddErrorAlert("Registered Course Status can not be edited because marks entry for this record has been locked.");
						this.RefreshPage();
						return;
					}

					this.lblCourseCode.Text = registeredCourse.OfferedCourseCode;
					this.lblCourseTitle.Text = registeredCourse.OfferedTitle;
					this.lblCreditHours.Text = registeredCourse.OfferedCreditHours.ToString();
					this.lblClassEdit.Text = registeredCourse.OfferedClass;
					this.ddlStatuses.FillEnums<RegisteredCours.Statuses>(CommonListItems.None);
					this.ddlStatuses.SetEnumValue(registeredCourse.StatusEnum);
					this.ddlStatuses_OnSelectedIndexChanged(null, null);
					this.dtpStatusDate.SelectedDate = registeredCourse.StatusDate;
					this.tbRemarks.Text = registeredCourse.Remarks;
					this.lblFreezedStatus.Text = registeredCourse.FreezedStatusFullName.ToNAIfNullOrEmpty();
					this.lblFreezedDate.Text = (registeredCourse.FreezedDate?.ToString("d")).ToNAIfNullOrEmpty();
					this.ViewState["RegisteredCourseID"] = registeredCourse.RegisteredCourseID;
					this.modalChangeStatus.Show();
					break;
			}
		}

		protected void btnShowBulkAD_OnClick(object sender, EventArgs e)
		{
			this.ddlSemesterIDAD.FillSemesters(CommonListItems.Select);
			this.tbRegisteredCourseIDs.Text = null;
			this.ddlSemesterIDAD.ClearSelection();
			this.tbRegisteredCourseIDs.Enabled = true;
			this.ddlSemesterIDAD.Enabled = true;
			this.btnUploadAD.Visible = true;
			this.modalBulkAD.Show();
		}

		protected void btnUploadAD_OnClick(object sender, EventArgs e)
		{
			this.SetMaximumScriptTimeout();
			var semesterID = this.ddlSemesterIDAD.SelectedValue.ToShort();
			var registeredCourseIDs = this.tbRegisteredCourseIDs.Text;
			TasksHelper.AddTaskAndRun(token =>
			{
				Aspire.BL.Core.CourseRegistration.Staff.RegisteredCourseStatus.UpdateRegisteredCourseStatusToAttendanceDefaulterAndEmailResult(semesterID, registeredCourseIDs, this.StaffIdentity.LoginSessionGuid);
			});
			this.AddSuccessAlert("Process has been started. You will receive the result in your mailbox.");
			Redirect<CourseWithdrawal>();
		}
	}
}