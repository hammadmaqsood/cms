﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="FirstSemesterCourseRegistration.aspx.cs" Inherits="Aspire.Web.Sys.Staff.CourseRegistration.FirstSemesterCourseRegistration" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" AutoPostBack="True" CausesValidation="False" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlProgramIDFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="ddlSemesterNoFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterNoFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlSemesterNoFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-1">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSectionFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSectionFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlSectionFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-1">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShiftFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlShiftFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlShiftFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Majors:" AssociatedControlID="ddlProgramMajorIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlProgramMajorIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlProgramMajorIDFilter_SelectedIndexChanged" />
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ItemType="Aspire.BL.Core.CourseRegistration.Staff.CourseRegistration.Student" ID="gvStudents" DataKeyNames="StudentID,Enrollment" AutoGenerateColumns="False" OnRowCreated="gvStudents_OnRowCreated" OnRowDataBound="gvStudents_OnRowDataBound" OnRowCommand="gvStudents_OnRowCommand">
		<Columns>
			<asp:TemplateField HeaderText="Select">
				<ItemTemplate>
					<aspire:AspireCheckBox runat="server" ID="cbSelect" Visible="<%# Item.RegisteredCourses.Count == 0 %>" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate>
					<%#: Container.DataItemIndex + 1 %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Enrollment">
				<ItemTemplate>
					<aspire:AspireHyperLink Target="_blank" ToolTip="Register Courses" runat="server" Text="<%# Item.Enrollment %>" NavigateUrl="<%# Aspire.Web.Sys.Staff.CourseRegistration.RegisterCourse.GetPageUrl(Item.IntakeSemesterID, Item.Enrollment) %>" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Name" DataField="Name" />
			<asp:BoundField HeaderText="Status" DataField="StatusFullName" />
			<asp:BoundField HeaderText="Program" DataField="ProgramAlias" />
			<asp:BoundField HeaderText="Registered Courses" DataField="CountHtml" HtmlEncode="False" />
			<asp:TemplateField HeaderText="Majors">
				<ItemTemplate>
					<aspire:AspireDropDownList runat="server" Enabled="<%# Item.RegisteredCourses.Count <= 0 %>" ID="ddlProgramMajorID" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Sem. #">
				<ItemTemplate>
					<aspire:AspireDropDownList runat="server" Enabled="<%# Item.RegisteredCourses.Count <= 0 %>" ID="ddlSemesterNo" SelectedValue="<%# Item.SemesterNo %>" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterNo" ValidationGroup='<%# $"Single{Container.DataItemIndex}" %>' ErrorMessage="Required" />
					<aspire:AspireCustomValidator Enabled="<%# Item.RegisteredCourses.Count <= 0 %>" runat="server" ID="cvddlSemesterNo" OnServerValidate="cv_OnServerValidate" ControlToValidate="ddlSemesterNo" ValidationGroup="All" ValidateEmptyText="True" ClientValidationFunction="ValidateRow" ErrorMessage="Required" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Section">
				<ItemTemplate>
					<aspire:AspireDropDownList runat="server" Enabled="<%# Item.RegisteredCourses.Count <= 0 %>" ID="ddlSection" SelectedValue="<%# Item.Section %>" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSection" ValidationGroup='<%# $"Single{Container.DataItemIndex}" %>' ErrorMessage="Required" />
					<aspire:AspireCustomValidator Enabled="<%# Item.RegisteredCourses.Count <= 0 %>" runat="server" ID="cvddlSection" OnServerValidate="cv_OnServerValidate" ControlToValidate="ddlSection" ValidationGroup="All" ValidateEmptyText="True" ClientValidationFunction="ValidateRow" ErrorMessage="Required" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Shift">
				<ItemTemplate>
					<aspire:AspireDropDownList runat="server" Enabled="<%# Item.RegisteredCourses.Count <= 0 %>" ID="ddlShift" SelectedValue="<%# Item.Shift %>" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlShift" ValidationGroup='<%# $"Single{Container.DataItemIndex}" %>' ErrorMessage="Required" />
					<aspire:AspireCustomValidator Enabled="<%# Item.RegisteredCourses.Count <= 0 %>" runat="server" ID="cvddlShift" OnServerValidate="cv_OnServerValidate" ControlToValidate="ddlShift" ValidationGroup="All" ValidateEmptyText="True" ClientValidationFunction="ValidateRow" ErrorMessage="Required" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Shift">
				<ItemTemplate>
					<aspire:AspireButton runat="server" Enabled="<%# Item.RegisteredCourses.Count <= 0 %>" Text="Register" ValidationGroup='<%# $"Single{Container.DataItemIndex}" %>' CommandName="Register" CommandArgument="<%# Container.DataItemIndex %>" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<div class="text-center">
		<aspire:AspireButton runat="server" ID="btnRegister" ValidationGroup="All" ButtonType="Danger" Text="Register" OnClick="btnRegister_OnClick" ConfirmMessage="Are you sure you want to register the selected students" />
		<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="FirstSemesterCourseRegistration.aspx" />
	</div>
	<script type="text/javascript">
		function ValidateRow(source, args) {
			args.IsValid = $("input:checkbox", $(source).closest("tr")).is(":checked") === 0 || args.Value.length > 0;
		}
	</script>
</asp:Content>
