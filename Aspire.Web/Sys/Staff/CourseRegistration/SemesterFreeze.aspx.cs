﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.CourseRegistration
{
	[AspirePage("544D71D4-0880-4232-A9CD-61DF47B9218D", UserTypes.Staff, "~/Sys/Staff/CourseRegistration/SemesterFreeze.aspx", "Semester Freeze", true, AspireModules.CourseRegistration)]
	public partial class SemesterFreeze : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.CourseRegistration.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Semester Freeze";

		public static string GetPageUrl(string enrollment, short? semesterID)
		{
			return typeof(SemesterFreeze).GetAspirePageAttribute().PageUrl.AttachQueryParams("Enrollment", enrollment, "SemesterID", semesterID);
		}

		public static void Redirect(string enrollment, short? semesterID)
		{
			BasePage.Redirect(GetPageUrl(enrollment, semesterID));
		}

		private void RefreshPage()
		{
			Redirect(this.ucStudentInfo.Enrollment, this.ddlSemesterID.SelectedValue.ToNullableShort());
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Reset();
				var enrollment = this.Request.GetParameterValue("Enrollment");
				if (!string.IsNullOrWhiteSpace(enrollment))
				{
					this.tbEnrollment.Enrollment = enrollment;
					this.tbEnrollment_OnSearch(null, null);
				}
			}
		}

		private void Reset()
		{
			this.ucStudentInfo.Reset();
			this.panelDetails.Visible = false;
			this.gvRegisteredCourses.DataBindNull();
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs args)
		{
			var enrollment = this.tbEnrollment.Enrollment;
			if (string.IsNullOrWhiteSpace(this.tbEnrollment.Enrollment))
			{
				this.AddNoRecordFoundAlert();
				this.Reset();
				return;
			}
			var studentInfo = ucStudentInfo.LoadBasicStudentInformation(enrollment);
			if (studentInfo == null)
			{
				this.AddNoRecordFoundAlert();
				this.Reset();
				return;
			}
			this.panelDetails.Visible = true;
			this.ddlSemesterID.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
			this.ddlSemesterID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var studentID = this.ucStudentInfo.StudentID.Value;
			var programAlias = this.ucStudentInfo.ProgramAlias;
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			this.lblSemester.Text = semesterID.ToSemesterString();

			var studentSemester = BL.Core.CourseRegistration.Staff.StudentSemesters.GetStudentSemester(studentID, semesterID, this.StaffIdentity.LoginSessionGuid);
			if (studentSemester != null)
			{
				this.lblClass.Text = AspireFormats.GetClassName(programAlias, studentSemester.SemesterNoEnum, studentSemester.SectionEnum, studentSemester.ShiftEnum);
				this.lblFreezedDate.Text = studentSemester.FreezedDate?.ToShortDateString().ToNAIfNullOrEmpty();
				this.lblFreezedStatus.Text = studentSemester.FreezedStatusFullName.ToNAIfNullOrEmpty();
				this.lblRemarks.Text = studentSemester.Remarks.ToNAIfNullOrEmpty();
				var registeredCourses = BL.Core.CourseRegistration.Staff.CourseRegistration.GetStudentRegisteredCourses(studentID, semesterID, this.StaffIdentity.LoginSessionGuid);
				this.gvRegisteredCourses.DataBind(registeredCourses);
				this.tbRemarks.Text = studentSemester.Remarks;
				this.tbRemarks.ReadOnly = false;
				//if (!registeredCourses.Any())
				//	this.ddlStatus.FillEnums<FreezedStatuses>().AddStaticListItem(new ListItem("Not Freezed", CommonListItems.EmptyValue));
				//else 
				this.ddlStatus.FillEnums<FreezedStatuses>().AddStaticListItem(new ListItem("Not Freezed", CommonListItems.EmptyValue));
				this.ddlStatus.SetEnumValue(studentSemester.FreezedStatusEnum).Enabled = true;
				this.ddlStatus_OnSelectedIndexChanged(null, null);
				this.dtpFreezedDate.SelectedDate = studentSemester.FreezedDate;
				this.btnSave.Enabled = true;
				this.ViewState[nameof(StudentSemester.StudentSemesterID)] = studentSemester.StudentSemesterID;
			}
			else
			{
				this.lblClass.Text = string.Empty.ToNAIfNullOrEmpty();
				this.lblFreezedDate.Text = string.Empty.ToNAIfNullOrEmpty();
				this.lblFreezedStatus.Text = string.Empty.ToNAIfNullOrEmpty();
				this.lblRemarks.Text = string.Empty.ToNAIfNullOrEmpty();
				this.gvRegisteredCourses.DataBindNull();
				this.tbRemarks.Text = null;
				this.tbRemarks.Text = null;
				this.tbRemarks.ReadOnly = true;
				//this.ddlStatus.FillEnums(FreezedStatuses.FreezedWithFee).AddStaticListItem(new ListItem("Not Freezed", CommonListItems.EmptyValue));
				this.ddlStatus.ClearSelection();
				this.ddlStatus.Enabled = false;
				this.ddlStatus_OnSelectedIndexChanged(null, null);
				this.btnSave.Enabled = false;
				this.ViewState[nameof(StudentSemester.StudentSemesterID)] = null;
				this.AddInfoAlert("First add the student semester which you want to freeze.");
			}
		}

		protected void ddlStatus_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var freezedStatusEnum = this.ddlStatus.GetSelectedEnumValue<FreezedStatuses>(null);
			switch (freezedStatusEnum)
			{
				case FreezedStatuses.FreezedWithFee:
				case FreezedStatuses.FreezedWithoutFee:
					this.dtpFreezedDate.ReadOnly = false;
					this.dtpvFreezedDate.AllowNull = false;
					break;
				case null:
					this.dtpFreezedDate.ReadOnly = true;
					this.dtpFreezedDate.SelectedDate = null;
					this.dtpvFreezedDate.AllowNull = true;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var studentSemesterID = (int)this.ViewState["StudentSemesterID"];
			var freezedStatus = this.ddlStatus.GetSelectedEnumValue<FreezedStatuses>(null);
			var freezedDate = this.dtpFreezedDate.SelectedDate;
			var remarks = this.tbRemarks.Text;
			var status = BL.Core.CourseRegistration.Staff.SemesterFreeze.UpdateStudentSemesterFreezedStatus(studentSemesterID, freezedStatus, freezedDate, remarks, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.CourseRegistration.Staff.SemesterFreeze.UpdateStudentSemesterFreezedStatusResults.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case BL.Core.CourseRegistration.Staff.SemesterFreeze.UpdateStudentSemesterFreezedStatusResults.Success:
					this.AddSuccessMessageHasBeenUpdated("Freezed Status");
					this.RefreshPage();
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}