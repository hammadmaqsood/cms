﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.CourseRegistration
{
	[AspirePage("1639738C-05C6-4B82-AF81-7D2AB300805E", UserTypes.Staff, "~/Sys/Staff/CourseRegistration/TimeBarredStudents.aspx", "Time Barred Students", true, AspireModules.CourseRegistration)]
	public partial class TimeBarredStudents : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.CourseRegistration.Module, UserGroupPermission.Allowed}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.time);
		public override string PageTitle => "Time Barred Student";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var studentID = this.Request.GetParameterValue<int>("StudentID");
				if (studentID != null)
					this.tbEnrollment.Enrollment = Aspire.BL.Common.Common.GetEnrollmentFromStudentID(studentID.Value);
				else
					this.tbEnrollment.Enrollment = this.Request.GetParameterValue("Enrollment");
				if (!string.IsNullOrWhiteSpace(this.tbEnrollment.Enrollment))
					this.tbEnrollment_OnSearch(null, null);
			}
		}

		private void Reset()
		{
			this.ucStudentInfo.Reset();
			this.panelTimeBars.Visible = false;
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs args)
		{
			var enrollment = this.tbEnrollment.Enrollment;
			if (string.IsNullOrWhiteSpace(enrollment))
			{
				this.Reset();
				this.AddNoRecordFoundAlert();
				return;
			}
			var studentinfo = this.ucStudentInfo.LoadBasicStudentInformation(enrollment);
			if (studentinfo == null)
			{
				this.Reset();
				this.AddNoRecordFoundAlert();
				return;
			}
			this.ViewState[nameof(StudentSemester.StudentID)] = studentinfo.StudentID;
			this.GetData();
			this.panelTimeBars.Visible = true;
		}

		private void GetData()
		{
			var studentID = (int)this.ViewState[nameof(StudentSemester.StudentID)];
			var timeBarredStudent = BL.Core.CourseRegistration.Staff.TimeBarredStudents.GetTimeBarredStudent(studentID, this.StaffIdentity.LoginSessionGuid);
			this.gvTimeBarredStudent.DataBind(timeBarredStudent);
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var timeBarredStudentID = (int?)this.ViewState[nameof(TimeBarredStudent.TimeBarredStudentID)];
			var studentID = this.ucStudentInfo.StudentID.Value;
			var minSemesterID = this.ddlMinSemester.SelectedValue.ToShort();
			var maxSemesterID = this.ddlMaxSemester.SelectedValue.ToShort();
			var courseCategories = this.CombineCourseCategoriesValues();
			var courseCategoryAllowedEnum = courseCategories;
			var maxCreditHours = this.tbMaxCreditHours.Text.ToNullableDecimal();
			var remarks = this.tbRemarks.Text;
			if (timeBarredStudentID == null)
			{
				var result = BL.Core.CourseRegistration.Staff.TimeBarredStudents.AddTimeBarredStudent(studentID, minSemesterID, maxSemesterID, courseCategoryAllowedEnum, maxCreditHours, remarks, this.StaffIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.CourseRegistration.Staff.TimeBarredStudents.AddTimeBarredStudentStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						break;
					case BL.Core.CourseRegistration.Staff.TimeBarredStudents.AddTimeBarredStudentStatuses.InvalidTimeBar:
						this.alertModalTimeBar.AddMessage(AspireAlert.AlertTypes.Error, "Invalid time bar.");
						this.updateModalTimeBarredStudentPanel.Update();
						break;
					case BL.Core.CourseRegistration.Staff.TimeBarredStudents.AddTimeBarredStudentStatuses.Success:
						this.AddSuccessAlert("Timebar record has been added.");
						Redirect<TimeBarredStudents>("Enrollment", this.ucStudentInfo.Enrollment);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var result = BL.Core.CourseRegistration.Staff.TimeBarredStudents.UpdateTimeBarredStudent(timeBarredStudentID.Value, minSemesterID, maxSemesterID, courseCategoryAllowedEnum, maxCreditHours, remarks, this.StaffIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.CourseRegistration.Staff.TimeBarredStudents.UpdateTimeBarredStudentStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						break;
					case BL.Core.CourseRegistration.Staff.TimeBarredStudents.UpdateTimeBarredStudentStatuses.InvalidTimeBar:
						this.alertModalTimeBar.AddMessage(AspireAlert.AlertTypes.Error, "Invalid time bar.");
						this.updateModalTimeBarredStudentPanel.Update();
						break;
					case BL.Core.CourseRegistration.Staff.TimeBarredStudents.UpdateTimeBarredStudentStatuses.Success:
						this.AddSuccessAlert("Timebar record has been updated.");
						Redirect<TimeBarredStudents>("Enrollment", this.ucStudentInfo.Enrollment);
						break;
					case BL.Core.CourseRegistration.Staff.TimeBarredStudents.UpdateTimeBarredStudentStatuses.CannotUpdateChildRecordExists:
						this.alertModalTimeBar.AddMessage(AspireAlert.AlertTypes.Error, "Can't update because child record exists.");
						this.updateModalTimeBarredStudentPanel.Update();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		protected void ddlMinSemester_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlMinSemester.SelectedValue.ToShort();
			this.ddlMaxSemester.Items.Clear();
			for (var i = 0; i < 10; i++)
			{
				this.ddlMaxSemester.Items.Add(new ListItem(semesterID.ToSemesterString(), semesterID.ToString()));
				semesterID = Semester.NextSemester(semesterID);
			}
		}

		protected void btnAddTimeBar_OnClick(object sender, EventArgs e)
		{
			this.DisplayTimeBarStudentModal(null);
			this.ddlMinSemester.FillSemesters();
			this.ddlMinSemester_OnSelectedIndexChanged(null, null);
			this.cbCourseCategory.FillEnums<CourseCategories>();

			this.panelTimeBars.Visible = true;
		}

		private void DisplayTimeBarStudentModal(int? timeBarredStudentID)
		{
			this.ddlMinSemester.FillSemesters();
			this.cbCourseCategory.FillEnums<CourseCategories>();
			if (timeBarredStudentID == null)
			{
				this.ddlMinSemester.ClearSelection();
				this.ddlMinSemester_OnSelectedIndexChanged(null, null);
				this.tbMaxCreditHours.Text = null;
				this.tbRemarks.Text = null;

				this.modalTimeBarredStudent.HeaderText = "Add Time Bar";
				this.ViewState[nameof(TimeBarredStudent.TimeBarredStudentID)] = null;
				this.modalTimeBarredStudent.Show();
			}
			else
			{
				var timeBarredStudent = BL.Core.CourseRegistration.Staff.TimeBarredStudents.GetTimeBarredofStudent(timeBarredStudentID.Value);
				var courseCategories = timeBarredStudent.CourseCategoriesAllowedEnum.GetFlags();
				var cbCourseCategoryItems = this.cbCourseCategory.Items.Cast<ListItem>();
				foreach (var cbCourseCategoryItem in cbCourseCategoryItems)
					cbCourseCategoryItem.Selected = false;
				foreach (var courseCategory in courseCategories)
					this.cbCourseCategory.Items.Cast<ListItem>().Single(i => i.Value.ToEnum<CourseCategories>() == courseCategory).Selected = true;
				this.ddlMinSemester.SelectedValue = timeBarredStudent.MinSemesterID.ToString();
				this.ddlMinSemester_OnSelectedIndexChanged(null, null);
				this.ddlMaxSemester.SelectedValue = timeBarredStudent.MaxSemesterID.ToString();
				this.tbMaxCreditHours.Text = timeBarredStudent.MaxCreditHours.ToString();
				this.tbRemarks.Text = timeBarredStudent.Remarks;

				this.modalTimeBarredStudent.HeaderText = "Save Time Bar";
				this.ViewState[nameof(TimeBarredStudent.TimeBarredStudentID)] = timeBarredStudentID;
				this.modalTimeBarredStudent.Show();
			}
		}

		private CourseCategories CombineCourseCategoriesValues()
		{
			var courseCategories = this.cbCourseCategory.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value.ToEnum<CourseCategories>()).Combine();
			if (courseCategories == null)
				throw new InvalidOperationException();
			return courseCategories.Value;
		}

		protected void gvTimeBarredStudent_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayTimeBarStudentModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var timeBarredStudentID = e.DecryptedCommandArgumentToInt();
					var studentID = (int)this.ViewState[nameof(StudentSemester.StudentID)];
					var result = BL.Core.CourseRegistration.Staff.TimeBarredStudents.DeleteTimeBarredStudent(timeBarredStudentID, this.StaffIdentity.LoginSessionGuid);
					switch (result)
					{
						case BL.Core.CourseRegistration.Staff.TimeBarredStudents.DeleteTimeBarredStudentStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Time bar");
							break;
						case BL.Core.CourseRegistration.Staff.TimeBarredStudents.DeleteTimeBarredStudentStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.CourseRegistration.Staff.TimeBarredStudents.DeleteTimeBarredStudentStatuses.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Time bar");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					Redirect<TimeBarredStudents>("Enrollment", this.tbEnrollment.Enrollment);
					break;
			}
		}
	}
}