﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CourseWithdrawal.aspx.cs" Inherits="Aspire.Web.Sys.Staff.CourseRegistration.CourseWithdrawal" %>

<%@ Register TagPrefix="uc" TagName="StudentInfo" Src="~/Sys/Staff/Common/StudentInfoUserControl.ascx" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
					<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="Search" OnSearch="tbEnrollment_OnSearch" />
				</div>
				<div class="form-group">
					<aspire:AspireHyperLink runat="server" Text="Clear" NavigateUrl="CourseWithdrawal.aspx" />
				</div>
				<div class="form-group pull-right">
					<aspire:AspireButton runat="server" ButtonType="Default" Text="Bulk Operation" ID="btnShowBulkAD" CausesValidation="False" OnClick="btnShowBulkAD_OnClick" />
				</div>
			</div>
		</div>
	</div>
	<asp:Panel runat="server" ID="panelDetails">
		<uc:StudentInfo runat="server" ID="ucStudentInfo" />
		<div class="row">
			<div class="col-md-12">
				<div class="form-inline">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
						<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
					</div>
				</div>
			</div>
		</div>
	</asp:Panel>
	<p></p>
	<asp:Panel runat="server" ID="panelCourseDetails">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">
					<aspire:AspireLabel runat="server" ID="lblSemester" /></h3>
			</div>
			<div class="panel-body">
				<aspire:AspireLabel runat="server" Text="Class:" AssociatedControlID="lblClass" />
				<aspire:AspireLabel runat="server" ID="lblClass" CssClass="form-control-static" />
				<aspire:AspireGridView Caption="Registered Courses" runat="server" ID="gvRegisteredCourses" AutoGenerateColumns="False" OnRowCommand="gvRegisteredCourses_OnRowCommand">
					<Columns>
						<asp:BoundField HeaderText="Code" DataField="RoadmapCourseCode" />
						<asp:BoundField HeaderText="Title" DataField="RoadmapTitle" />
						<asp:BoundField HeaderText="Credits Hrs." DataField="RoadmapCreditHours" />
						<asp:BoundField HeaderText="Class" DataField="OfferedClass" />
						<asp:BoundField HeaderText="Status" DataField="StatusFullName" />
						<asp:BoundField HeaderText="Status Date" DataField="StatusDate" DataFormatString="{0:d}" />
						<asp:BoundField HeaderText="Freeze Status" DataField="FreezedStatusFullName" />
						<asp:BoundField HeaderText="Freeze Status Date" DataField="FreezedDate" DataFormatString="{0:d}" />
						<asp:BoundField HeaderText="Remarks" DataField="Remarks" />
						<asp:TemplateField HeaderText="Action">
							<ItemTemplate>
								<aspire:AspireLinkButton runat="server" Glyphicon="edit" CommandName="Edit" CausesValidation="False" EncryptedCommandArgument='<%# Eval("RegisteredCourseID") %>' />
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</aspire:AspireGridView>
			</div>
		</div>
	</asp:Panel>

	<aspire:AspireModal runat="server" ID="modalChangeStatus" HeaderText="Change Course Withdrawal Status">
		<BodyTemplate>
			<asp:UpdatePanel runat="server">
				<ContentTemplate>
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-3" AssociatedControlID="lblCourseCode" Text="Course Code:" />
							<div class="col-md-9 form-control-static">
								<aspire:AspireLabel runat="server" ID="lblCourseCode" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-3" AssociatedControlID="lblCourseTitle" Text="Title:" />
							<div class="col-md-9 form-control-static">
								<aspire:AspireLabel runat="server" ID="lblCourseTitle" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-3" AssociatedControlID="lblCreditHours" Text="Credit Hours:" />
							<div class="col-md-9 form-control-static">
								<aspire:AspireLabel runat="server" ID="lblCreditHours" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-3" AssociatedControlID="lblClassEdit" Text="Class:" />
							<div class="col-md-9 form-control-static">
								<aspire:AspireLabel runat="server" ID="lblClassEdit" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-3" AssociatedControlID="lblFreezedStatus" Text="Freeze Status:" />
							<div class="col-md-9 form-control-static">
								<aspire:AspireLabel runat="server" ID="lblFreezedStatus" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-3" AssociatedControlID="lblFreezedDate" Text="Freeze Date:" />
							<div class="col-md-9 form-control-static">
								<aspire:AspireLabel runat="server" ID="lblFreezedDate" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-3" AssociatedControlID="ddlStatuses" Text="Statuses:" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlStatuses" AutoPostBack="True" OnSelectedIndexChanged="ddlStatuses_OnSelectedIndexChanged" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-3" AssociatedControlID="dtpStatusDate" Text="Date:" />
							<div class="col-md-9">
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpStatusDate" AutoPostBack="True" />
								<aspire:AspireDateTimeValidator ControlToValidate="dtpStatusDate" runat="server" ID="dtpvStatus" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-3" AssociatedControlID="tbRemarks" Text="Remarks:" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbRemarks" MaxLength="1000" TextMode="MultiLine" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton ButtonType="Primary" runat="server" Text="Save" ID="btnSave" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>

	<aspire:AspireModal runat="server" ID="modalBulkAD" HeaderText="Change Status To Attendance Defaulter (Bulk Operation)">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelAD" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertModalAD" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" AssociatedControlID="ddlSemesterIDAD" Text="Semester:" />
						<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDAD" />
						<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ControlToValidate="ddlSemesterIDAD" ValidationGroup="AD" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" AssociatedControlID="tbRegisteredCourseIDs" Text="Registered Course IDs (one per line):" />
						<aspire:AspireTextBox runat="server" ValidationGroup="AD" TextMode="MultiLine" ID="tbRegisteredCourseIDs" MaxLength="1048576" Rows="10" />
						<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ControlToValidate="tbRegisteredCourseIDs" ValidationGroup="AD" />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updatePanelModalADButtons">
				<ContentTemplate>
					<aspire:AspireButton ButtonType="Danger" ValidationGroup="AD" runat="server" Text="Upload" ID="btnUploadAD" OnClick="btnUploadAD_OnClick" ConfirmMessage="This operation cannot be rollback. Are you sure you want to continue?" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
