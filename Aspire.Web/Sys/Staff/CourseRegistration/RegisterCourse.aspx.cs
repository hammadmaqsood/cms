﻿using Aspire.BL.Core.CourseRegistration.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.CourseRegistration
{
	[AspirePage("6D5C41C3-B64B-4520-88F0-E0B5BAC4E2B5", UserTypes.Staff, "~/Sys/Staff/CourseRegistration/RegisterCourse.aspx", "Register Course", true, AspireModules.CourseRegistration)]
	public partial class RegisterCourse : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.CourseRegistration.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.book);
		public override string PageTitle => "Register Course";

		public static string GetPageUrl(short? semesterID, string enrollment)
		{
			return GetAspirePageAttribute<RegisterCourse>().PageUrl.AttachQueryParams("SemesterID", semesterID, "Enrollment", enrollment);
		}

		public static void Redirect(short? semesterID, string enrollment)
		{
			Redirect(GetPageUrl(semesterID, enrollment));
		}

		private void Refresh()
		{
			if (this.SelectedCourse != null)
				Redirect(this.SelectedCourse.OfferedSemesterID, this.SelectedCourse.Enrollment);
			else
				Redirect(this.ddlSemesterID.SelectedValue.ToNullableShort(), this.tbEnrollment.Enrollment);
		}

		private bool? _canDeleteCourseRegistrationWithAttendance;
		protected bool CanDeleteCourseRegistrationWithAttendance
		{
			get
			{
				if (this._canDeleteCourseRegistrationWithAttendance == null)
					this._canDeleteCourseRegistrationWithAttendance = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.CourseRegistration.CanDeleteCourseRegistrationWithAttendance) == UserGroupPermission.PermissionValues.Allowed;
				return this._canDeleteCourseRegistrationWithAttendance.Value;
			}
		}

		private bool? _canDeleteCourseRegistration;
		protected bool CanDeleteCourseRegistration
		{
			get
			{
				if (this._canDeleteCourseRegistration == null)
					this._canDeleteCourseRegistration = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.CourseRegistration.CanDeleteCourseRegistration) == UserGroupPermission.PermissionValues.Allowed;
				return this._canDeleteCourseRegistration.Value;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterID.FillSemesters();
				this.ddlSemesterID.SetSelectedValueIfNotPostback("SemesterID");
				this.panelBody.Visible = false;
				this.tbEnrollment.Focus();

				var enrollment = this.Request.GetParameterValue("Enrollment");
				if (!string.IsNullOrWhiteSpace(enrollment))
				{
					this.tbEnrollment.Enrollment = enrollment;
					this.tbEnrollment_OnSearch(null, null);
				}
			}
		}

		[Serializable]
		private class CourseToBeRegistered
		{
			public int StudentID { get; set; }
			public string Enrollment { get; set; }
			public int ProgramID { get; set; }
			public int AdmissionOpenProgramID { get; set; }
			public short OfferedSemesterID { get; set; }
			public int? OfferedCourseID { get; set; }
			public int? EquivalentCourseID { get; set; }
			public RegisteredCours.BypassedCheckTypes? ByPassCheckTypes { get; set; }

			public void Clear()
			{
				this.OfferedCourseID = null;
				this.EquivalentCourseID = null;
				this.ByPassCheckTypes = null;
			}
		}

		private CourseToBeRegistered SelectedCourse
		{
			get => (CourseToBeRegistered)this.ViewState[nameof(this.SelectedCourse)];
			set => this.ViewState[nameof(this.SelectedCourse)] = value;
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs args)
		{
			var enrollment = this.tbEnrollment.Enrollment;
			var offeredSemesterID = this.ddlSemesterID.SelectedValue.ToShort();
			this.panelBody.Visible = false;
			this.SelectedCourse = null;
			var data = BL.Core.CourseRegistration.Staff.CourseRegistration.GetCourseRegistrationInfo(enrollment, offeredSemesterID, this.AspireIdentity.LoginSessionGuid);
			if (data == null)
			{
				this.AddNoRecordFoundAlert();
				return;
			}

			this.hlEnrollment.Text = data.Enrollment;
			this.hlEnrollment.NavigateUrl = Students.StudentProfileView.GetPageUrl(data.Enrollment);
			this.lblRegistrationNo.Text = data.RegistrationNo.ToString();
			this.lblName.Text = data.Name;
			this.lblFatherName.Text = data.FatherName;
			this.lblProgramAlias.Text = data.Program;
			this.lblIntakeSemester.Text = data.IntakeSemesterID.ToSemesterString();
			this.lblStatus.Text = (data.StatusEnum?.ToFullName()).ToNAIfNullOrEmpty();
			this.lblMaxAllowedCoursesPerSemester.Text = data.MaxAllowedCoursesPerSemester.ToString();
			this.lblMaxAllowedCreditHoursPerSemester.Text = data.MaxAllowedCreditHoursPerSemester.ToString();

			if (data.CanStudentRegisterCourseOnline)
			{
				this.lbtnCanStudentRegisterCourseOnline.CssClass = "";
				this.lbtnCanStudentRegisterCourseOnline.Text = "Allowed";
				this.lbtnCanStudentRegisterCourseOnline.ConfirmMessage = "Are you sure you want to block this student from online course registration?";
			}
			else
			{
				this.lbtnCanStudentRegisterCourseOnline.CssClass = "text-danger";
				this.lbtnCanStudentRegisterCourseOnline.Text = "Not Allowed";
				this.lbtnCanStudentRegisterCourseOnline.ConfirmMessage = "Are you sure you want to allow this student for online course registration?";
			}
			this.lblOfferedSemester.Text = data.OfferedSemesterID.ToSemesterString();
			this.lblProgram.Text = data.Program;
			if (this.ddlSemesterNo.Items.Count == 0)
			{
				this.ddlSemesterNo.FillSemesterNos();
				this.ddlSection.FillSections();
				this.ddlShift.FillShifts();
			}

			this.SelectedCourse = new CourseToBeRegistered
			{
				StudentID = data.StudentID,
				Enrollment = data.Enrollment,
				OfferedSemesterID = data.OfferedSemesterID,
				ProgramID = data.ProgramID,
				AdmissionOpenProgramID = data.AdmissionOpenProgramID,
				OfferedCourseID = null,
				EquivalentCourseID = null,
				ByPassCheckTypes = null,
			};

			this.gvSemesterDetails.DataBind(data.StudentSemesters);
			var lastSemester = data.StudentSemesters.LastOrDefault();
			if (lastSemester != null)
			{
				this.ddlSemesterNo.SetEnumValue(lastSemester.SemesterNoEnum);
				this.ddlSection.SetEnumValue(lastSemester.SectionEnum);
				this.ddlShift.SetEnumValue(lastSemester.ShiftEnum);
			}

			var permissionValue = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.CourseRegistration.RegisterCourse);
			this.repeaterRegisteredCourses.DataBind(data.RegisteredCourses);
			this.repeaterRequestedCourses.DataBind(data.RequestedCourses);

			if (data.RegisteredCourseResults.All(rc => rc.Enrollment == data.Enrollment))
			{
				this.gvPreviousResult.DataBind(data.RegisteredCourseResults);
				this.gvPreviousResult.Visible = true;
				this.gvPreviousResultTransferred.Visible = false;
			}
			else
			{
				this.gvPreviousResultTransferred.DataBind(data.RegisteredCourseResults);
				this.gvPreviousResult.Visible = false;
				this.gvPreviousResultTransferred.Visible = true;
			}
			this.panelBody.Visible = true;

			var canChangeSectionShift = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.CourseRegistration.CanChangeStudentSectionShift) == UserGroupPermission.PermissionValues.Allowed;
			if (permissionValue != UserGroupPermission.PermissionValues.Allowed)
			{
				this.divRegisterButton.Visible = false;
				this.modalCourses.Visible = false;
				this.modalOfferedCourses.Visible = false;
				if (canChangeSectionShift)
				{
					this.ddlSemesterNo.Enabled = true;
					this.ddlSection.Enabled = true;
					this.ddlShift.Enabled = true;
					this.btnUpdateStudentSemester.Enabled = true;
					this.btnUpdateStudentSemester.Visible = true;
				}
				else
				{
					this.ddlSemesterNo.Enabled = false;
					this.ddlSection.Enabled = false;
					this.ddlShift.Enabled = false;
					this.btnUpdateStudentSemester.Enabled = false;
					this.btnUpdateStudentSemester.Visible = false;
				}
			}
			else
			{
				this.divRegisterButton.Visible = true;
				var canByPassPreRequisitesCheck = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.CourseRegistration.CanByPassPreRequisitesCheck) == UserGroupPermission.PermissionValues.Allowed;
				var canByPassImprovementCheck = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.CourseRegistration.CanByPassImprovementCheck) == UserGroupPermission.PermissionValues.Allowed;
				var canByPassRegistrationOpenCheck = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.CourseRegistration.CanByPassRegistrationOpenCheck) == UserGroupPermission.PermissionValues.Allowed;
				var canByPassMaxAllowedCoursesPerSemester = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.CourseRegistration.CanByPassMaxAllowedCoursesPerSemester) == UserGroupPermission.PermissionValues.Allowed;
				var canByPassMaxAllowedCreditHoursPerSemester = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.CourseRegistration.CanByPassMaxAllowedCreditHoursPerSemester) == UserGroupPermission.PermissionValues.Allowed;
				var bypassCheckItems = Enum.GetValues(typeof(RegisteredCours.BypassedCheckTypes)).Cast<RegisteredCours.BypassedCheckTypes>().Select(e =>
				{
					switch (e)
					{
						case RegisteredCours.BypassedCheckTypes.None:
							break;
						case RegisteredCours.BypassedCheckTypes.PreRequisites:
							if (canByPassPreRequisitesCheck)
								return new ListItem("Pre-requisites", $"{(byte)e}");
							break;
						case RegisteredCours.BypassedCheckTypes.ImprovementCheck:
							if (canByPassImprovementCheck)
								return new ListItem("Improvement", $"{(byte)e}");
							break;
						case RegisteredCours.BypassedCheckTypes.RegistrationOpenCheck:
							if (canByPassRegistrationOpenCheck)
								return new ListItem("Registration Open", $"{(byte)e}");
							break;
						case RegisteredCours.BypassedCheckTypes.MaxAllowedCoursesPerSemester:
							if (canByPassMaxAllowedCoursesPerSemester)
								return new ListItem("Max. Allowed Courses", $"{(byte)e}");
							break;
						case RegisteredCours.BypassedCheckTypes.MaxAllowedCreditHoursPerSemester:
							if (canByPassMaxAllowedCreditHoursPerSemester)
								return new ListItem("Max. Allowed Credit Hours", $"{(byte)e}");
							break;
						default:
							throw new NotImplementedEnumException(e);
					}
					return null;
				}).Where(i => i != null);
				this.chkBypassChecks.DataBind(bypassCheckItems);
				this.lblBypassChecks.Visible = this.chkBypassChecks.Items.Count > 0;
				if (canChangeSectionShift)
				{
					this.ddlSemesterNo.Enabled = true;
					this.ddlSection.Enabled = true;
					this.ddlShift.Enabled = true;
					this.btnUpdateStudentSemester.Enabled = true;
					this.btnUpdateStudentSemester.Visible = true;
				}
				else
				{
					this.ddlSemesterNo.Enabled = true;
					this.ddlSection.Enabled = false;
					this.ddlShift.Enabled = false;
					this.btnUpdateStudentSemester.Enabled = true;
					this.btnUpdateStudentSemester.Visible = true;
				}
			}
		}

		protected void lbtnCanStudentRegisterCourseOnline_OnClick(object sender, EventArgs e)
		{
			var status = BL.Core.CourseRegistration.Staff.CourseRegistration.ToggleCanStudentRegisterCourseOnlineStatus(this.SelectedCourse.StudentID, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.CourseRegistration.Staff.CourseRegistration.ToggleCanStudentRegisterCourseOnlineStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					break;
				case BL.Core.CourseRegistration.Staff.CourseRegistration.ToggleCanStudentRegisterCourseOnlineStatuses.SuccessAllowed:
					this.AddSuccessAlert($"Student \"{this.SelectedCourse.Enrollment}\" is allowed for online course registration.");
					break;
				case BL.Core.CourseRegistration.Staff.CourseRegistration.ToggleCanStudentRegisterCourseOnlineStatuses.SuccessNotAllowed:
					this.AddSuccessAlert($"Student \"{this.SelectedCourse.Enrollment}\" is blocked from online course registration.");
					break;
				default:
					throw new NotImplementedEnumException(status);
			}
			this.Refresh();
		}

		protected void btnRegisterCourse_OnClick(object sender, EventArgs e)
		{
			var bypassChecks = RegisteredCours.BypassedCheckTypes.None;
			var checks = this.chkBypassChecks.GetSelectedEnumValues<RegisteredCours.BypassedCheckTypes>();
			foreach (var byPassCheckType in checks)
				bypassChecks = bypassChecks | byPassCheckType;
			this.DisplayOfferedCourses(bypassChecks);
		}

		protected void repeaterOfferedCourses_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Register":
					var args = e.CommandArgument.ToString().Decrypt().Split(':');
					var course = this.SelectedCourse;
					course.OfferedCourseID = args[0].ToInt();
					course.EquivalentCourseID = args[1].ToNullableInt();
					this.SelectedCourse = course;
					this.modalOfferedCourses.Hide();
					if (course.EquivalentCourseID != null)
						this.RegisterSelectedCourse();
					else
						this.DisplayCourses();
					break;
			}
		}

		private void DisplayOfferedCourses(RegisteredCours.BypassedCheckTypes byPassCheckTypes)
		{
			var course = this.SelectedCourse;
			course.Clear();
			course.ByPassCheckTypes = byPassCheckTypes;
			this.SelectedCourse = course;
			this.modalOfferedCourses.HeaderText = "Offered Courses: " + course.OfferedSemesterID.ToSemesterString();
			var offeredCourses = BL.Core.CourseRegistration.Staff.CourseRegistration.GetOfferedCourses(course.AdmissionOpenProgramID, course.OfferedSemesterID, this.StaffIdentity.LoginSessionGuid);
			this.repeaterOfferedCourses.DataBind(offeredCourses);
			this.modalOfferedCourses.Show();
		}

		private void DisplayCourses()
		{
			var courses = BL.Core.CourseRegistration.Staff.CourseRegistration.GetStudentRoadmapCourses(this.SelectedCourse.StudentID);
			this.repeaterCourses.DataBind(courses);
			this.modalCourses.HeaderText = "Select Equivalent Roadmap Course";
			this.modalCourses.Show();
		}

		protected void repeaterCourses_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Register":
					var courseID = e.DecryptedCommandArgumentToInt();
					var course = this.SelectedCourse;
					course.EquivalentCourseID = courseID;
					this.SelectedCourse = course;
					this.RegisterSelectedCourse();
					this.modalCourses.Hide();
					break;
			}
		}

		private void RegisterSelectedCourse()
		{
			this.StaffIdentity.Demand(StaffPermissions.CourseRegistration.RegisterCourse, UserGroupPermission.PermissionValues.Allowed);
			var selectedCourse = this.SelectedCourse;
			if (selectedCourse == null)
				throw new InvalidOperationException();
			var semesterNo = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>();
			var section = this.ddlSection.GetSelectedEnumValue<Sections>();
			var shift = this.ddlShift.GetSelectedEnumValue<Shifts>();
			var offeredCourseID = selectedCourse.OfferedCourseID ?? throw new InvalidOperationException();
			var equivalentCourseID = selectedCourse.EquivalentCourseID ?? throw new InvalidOperationException();
			var byPassCheckTypes = selectedCourse.ByPassCheckTypes ?? throw new InvalidOperationException();
			var coursesRegister = BL.Core.CourseRegistration.Staff.CourseRegistration.RegisterCourse(selectedCourse.StudentID, selectedCourse.OfferedSemesterID, semesterNo, section, shift, offeredCourseID, equivalentCourseID, byPassCheckTypes, this.StaffIdentity.LoginSessionGuid);
			if (coursesRegister == null)
			{
				this.AddErrorAlert("You are not authorized to register course for the selected student.");
				this.Refresh();
				return;
			}
			coursesRegister.GetErrorMessages()?.ForEach(message => this.AddErrorAlert(message));
			coursesRegister.GetWarningMessages()?.ForEach(message => this.AddWarningAlert(message));
			coursesRegister.GetSuccessMessages()?.ForEach(message => this.AddSuccessAlert(message));
			this.Refresh();
		}

		protected void repeaterRegisteredCourses_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
				case "DeleteWithAttendance":
					var registeredCourseID = e.DecryptedCommandArgumentToInt();
					var deleteWithAttendance = e.CommandName == "DeleteWithAttendance";
					var bypassChecks = this.chkBypassChecks.GetSelectedEnumValues<RegisteredCours.BypassedCheckTypes>();
					var bypassRegistrationOpenCheck = bypassChecks.Contains(RegisteredCours.BypassedCheckTypes.RegistrationOpenCheck);
					var deleteStatus = BL.Core.CourseRegistration.Staff.CourseRegistration.DeleteRegisteredCourse(registeredCourseID, deleteWithAttendance, bypassRegistrationOpenCheck, this.StaffIdentity.LoginSessionGuid);
					switch (deleteStatus)
					{
						case BL.Core.CourseRegistration.Staff.CourseRegistration.DeleteRegisteredCourseStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.CourseRegistration.Staff.CourseRegistration.DeleteRegisteredCourseStatuses.NotAuthorized:
							this.AddErrorAlert("You are not authorized to delete registered course for selected student.");
							break;
						case BL.Core.CourseRegistration.Staff.CourseRegistration.DeleteRegisteredCourseStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Registered Courses");
							break;
						case BL.Core.CourseRegistration.Staff.CourseRegistration.DeleteRegisteredCourseStatuses.CannotDeleteExamMarksExists:
						case BL.Core.CourseRegistration.Staff.CourseRegistration.DeleteRegisteredCourseStatuses.CannotDeleteStatusExists:
							this.AddErrorAlert("Cannot delete registered course because exam record exists.");
							break;
						case BL.Core.CourseRegistration.Staff.CourseRegistration.DeleteRegisteredCourseStatuses.CannotDeleteAttendanceRecordExists:
							this.AddErrorAlert("Cannot delete registered course because attendance record exists.");
							break;
						case BL.Core.CourseRegistration.Staff.CourseRegistration.DeleteRegisteredCourseStatuses.CannotDeleteSemesterFreeze:
							this.AddErrorAlert("Cannot delete registered course student's semester has been freeze.");
							break;
						case BL.Core.CourseRegistration.Staff.CourseRegistration.DeleteRegisteredCourseStatuses.CannotDeleteFeeGenerated:
							this.AddErrorAlert("Cannot delete registered course because Fee record exists.");
							break;
						case BL.Core.CourseRegistration.Staff.CourseRegistration.DeleteRegisteredCourseStatuses.RegistrationClosed:
							this.AddErrorAlert("Course Registration has been closed.");
							break;
						case BL.Core.CourseRegistration.Staff.CourseRegistration.DeleteRegisteredCourseStatuses.CannotDeleteSurveyRecordExists:
							this.AddErrorAlert("Cannot delete registered course because QA Survey record exists.");
							break;
						default:
							throw new NotImplementedEnumException(deleteStatus);
					}
					this.Refresh();
					break;
			}
		}

		protected void repeaterRequestedCourses_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Accept":
					var course = this.SelectedCourse;
					course.OfferedCourseID = e.DecryptedCommandArgumentToInt();
					course.EquivalentCourseID = null;
					this.SelectedCourse = course;
					this.DisplayCourses();
					break;
				case "Reject":
					var status = BL.Core.CourseRegistration.Staff.CourseRegistration.DeleteRequestedCourse(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					switch (status)
					{
						case BL.Core.CourseRegistration.Staff.CourseRegistration.DeleteRequestedCourseStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.CourseRegistration.Staff.CourseRegistration.DeleteRequestedCourseStatuses.NotAuthorized:
							this.AddErrorAlert("You are not authorized to delete the requested course for the selected student.");
							break;
						case BL.Core.CourseRegistration.Staff.CourseRegistration.DeleteRequestedCourseStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Requested for Course registration");
							break;
						default:
							throw new NotImplementedEnumException(status);
					}
					this.Refresh();
					break;
			}
		}

		protected void btnUpdateStudentSemester_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			this.StaffIdentity.Demand(StaffPermissions.CourseRegistration.RegisterCourse, UserGroupPermission.PermissionValues.Allowed);
			if (this.SelectedCourse == null)
				return;
			var selectedCourse = this.SelectedCourse;
			var semesterNo = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>();
			var section = this.ddlSection.GetSelectedEnumValue<Sections>();
			var shift = this.ddlShift.GetSelectedEnumValue<Shifts>();
			var coursesRegister = BL.Core.CourseRegistration.Staff.CourseRegistration.ChangeStudentClass(selectedCourse.StudentID, selectedCourse.OfferedSemesterID, semesterNo, section, shift, this.StaffIdentity.LoginSessionGuid);
			if (coursesRegister == null)
			{
				this.AddErrorAlert("You are not authorized to update class information for the selected student.");
				this.Refresh();
				return;
			}
			coursesRegister.GetErrorMessages().ForEach(message => this.AddErrorAlert(message));
			coursesRegister.GetWarningMessages().ForEach(message => this.AddWarningAlert(message));
			coursesRegister.GetSuccessMessages().ForEach(message => this.AddSuccessAlert(message));
			this.Refresh();
		}

		protected static string GetBypassLabelsHtml(RegisteredCours.BypassedCheckTypes bypassedCheckTypeFlags)
		{
			IEnumerable<string> GetHtml()
			{
				var bypassedCheckTypeFlagsArray = Enum.GetValues(typeof(RegisteredCours.BypassedCheckTypes))
					.Cast<RegisteredCours.BypassedCheckTypes>()
					.Where(f => f != RegisteredCours.BypassedCheckTypes.None)
					.Where(f => bypassedCheckTypeFlags.HasFlag(f))
					.ToArray();
				string LabelHtml(string text) => $"<span class=\"label label-danger\">{text.HtmlEncode()}</span>";
				foreach (var bypassedCheckTypeFlag in bypassedCheckTypeFlagsArray)
					switch (bypassedCheckTypeFlag)
					{
						case RegisteredCours.BypassedCheckTypes.None:
							throw new InvalidOperationException();
						case RegisteredCours.BypassedCheckTypes.PreRequisites:
							yield return LabelHtml("Pre-requisites Bypassed");
							break;
						case RegisteredCours.BypassedCheckTypes.ImprovementCheck:
							yield return LabelHtml("Improvement Check Bypassed");
							break;
						case RegisteredCours.BypassedCheckTypes.RegistrationOpenCheck:
							yield return LabelHtml("Registration Open Check Bypassed");
							break;
						case RegisteredCours.BypassedCheckTypes.MaxAllowedCoursesPerSemester:
							yield return LabelHtml("Maximum Allowed Courses Check Bypassed");
							break;
						case RegisteredCours.BypassedCheckTypes.MaxAllowedCreditHoursPerSemester:
							yield return LabelHtml("Maximum Allowed Credit Hours Check Bypassed");
							break;
						default:
							throw new NotImplementedEnumException(bypassedCheckTypeFlag);
					}
			}
			return GetHtml().Join("");
		}
	}
}