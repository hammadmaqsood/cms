<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CancelledAdmissions.aspx.cs" Inherits="Aspire.Web.Sys.Staff.CourseRegistration.CancelledAdmissions" %>

<%@ Import Namespace="Aspire.Model.Entities" %>

<%@ Register TagPrefix="uc" TagName="studentinfo" Src="~/Sys/Staff/Common/StudentInfoUserControl.ascx" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
					<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="RequiredEnrollment" OnSearch="tbEnrollment_OnSearch" />
				</div>
				<div class="form-group">
					<aspire:AspireHyperLink runat="server" Text="Clear" NavigateUrl="CancelledAdmissions.aspx" />
				</div>
			</div>
		</div>
	</div>
	<asp:Panel runat="server" ID="panelCancelledAdmission" Visible="False">
		<uc:studentinfo runat="server" ID="ucStudentInfo" />
		<aspire:AspireGridView runat="server" ID="gvStudentSemesters" AutoGenerateColumns="False" Caption="Student Semester Summary">
			<Columns>
				<asp:TemplateField HeaderText="Sr.#">
					<ItemTemplate>
						<%#: Container.DataItemIndex+1 %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField DataField="Semester" HeaderText="Semester" />
				<asp:BoundField DataField="Class" HeaderText="Class" />
				<asp:BoundField DataField="StatusFullName" HeaderText="Status" />
				<asp:BoundField DataField="FreezedStatusFullName" HeaderText="Freezed Status" />
				<asp:BoundField DataField="Remarks" HeaderText="Remarks" />
			</Columns>
		</aspire:AspireGridView>

		<aspire:AspireButton runat="server" ID="btnAddCancelledAdmission" Text="Add Cancelled Admission" Glyphicon="plus" ButtonType="Success" CausesValidation="False" OnClick="btnAddCancelledAdmission_OnClick" />
		<p></p>

		<aspire:AspireGridView runat="server" ID="gvCancelledAdmission" AutoGenerateColumns="False" OnRowCommand="gvCancelledAdmission_OnRowCommand">
			<Columns>
				<asp:TemplateField HeaderText="Sr.#">
					<ItemTemplate>
						<%#: Container.DataItemIndex+1 %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField DataField="Semester" HeaderText="Semester" />
				<asp:BoundField DataField="StatusEnum" HeaderText="Status" />
				<asp:BoundField DataField="Remarks" HeaderText="Remarks" />
				<asp:TemplateField>
					<ItemTemplate>
						<aspire:AspireLinkButton runat="server" CommandName="Edit" EncryptedCommandArgument="<%#this.Eval(nameof(CancelledAdmission.CancelledAdmissionID))%>" CausesValidation="False" Glyphicon="edit" />
						<aspire:AspireLinkButton runat="server" CommandName="Delete" EncryptedCommandArgument="<%#this.Eval(nameof(CancelledAdmission.CancelledAdmissionID))%>" CausesValidation="False" Glyphicon="remove" ConfirmMessage="Are you sure you want to delete?" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>

		<aspire:AspireModal runat="server" ID="modalCancelledAdmission">
			<BodyTemplate>
				<asp:UpdatePanel runat="server" ID="updateModalCancelledAdmission" UpdateMode="Conditional">
					<ContentTemplate>
						<aspire:AspireAlert runat="server" ID="alertModalCancelledAdmission" />
						<div class="form-horizontal">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" CssClass="col-md-2" />
								<div class="col-md-10">
									<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="Required" />
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterID" ErrorMessage="This field is required." ValidationGroup="Required" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="rblStatus" CssClass="col-md-2" />
								<div class="col-md-4">
									<aspire:AspireRadioButtonList runat="server" ID="rblStatus" RepeatDirection="Horizontal" RepeatLayout="Flow" ValidationGroup="Required" />
								</div>
								<div class="form-control-static col-md-4">
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblStatus" ErrorMessage="This field is required." ValidationGroup="Required" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" CssClass="col-md-2" />
								<div class="col-md-10">
									<aspire:AspireTextBox runat="server" ID="tbRemarks" TextMode="MultiLine" ValidationGroup="Required" MaxLength="1000" />
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbRemarks" ErrorMessage="This field is required." ValidationGroup="Required" />
								</div>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</BodyTemplate>
			<FooterTemplate>
				<asp:UpdatePanel runat="server">
					<ContentTemplate>
						<div class="form-group">
							<div class="col-md-offset-2 col-md-10">
								<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" ValidationGroup="Required" OnClick="btnSave_OnClick" />
								<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</FooterTemplate>
		</aspire:AspireModal>
	</asp:Panel>
</asp:Content>
