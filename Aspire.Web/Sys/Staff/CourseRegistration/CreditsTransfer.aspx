﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CreditsTransfer.aspx.cs" Inherits="Aspire.Web.Sys.Staff.CourseRegistration.CreditsTransfer" %>

<%@ Register TagPrefix="uc" TagName="StudentInfo" Src="~/Sys/Staff/Common/StudentInfoUserControl.ascx" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
					<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="RequiredEnrollment" OnSearch="tbEnrollment_OnSearch" />
				</div>
				<div class="form-group">
					<aspire:AspireHyperLink runat="server" Text="Clear" NavigateUrl="CreditsTransfer.aspx" />
				</div>
			</div>
		</div>
	</div>
	<asp:Panel runat="server" ID="panelCreditsTransfer">
		<uc:StudentInfo runat="server" ID="ucStudentInfo" />
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute Name:" AssociatedControlID="tbInstituteName" />
					<aspire:AspireTextBox runat="server" ID="tbInstituteName" ValidationGroup="CreditsTransfer" MaxLength="500" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbInstituteName" ErrorMessage="This field is required." ValidationGroup="CreditsTransfer" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Transferred Date:" AssociatedControlID="dpTransferredDate" />
					<aspire:AspireDateTimePickerTextbox runat="server" ID="dpTransferredDate" ValidationGroup="CreditsTransfer" />
					<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dpTransferredDate" RequiredErrorMessage="This field is required." ValidationGroup="CreditsTransfer" AllowNull="False" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Transferred Semester:" AssociatedControlID="ddlTransferredSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlTransferredSemesterID" ValidationGroup="CreditsTransfer" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlTransferredSemesterID" ErrorMessage="This field is required." ValidationGroup="CreditsTransfer" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Committee Members:" AssociatedControlID="tbCommitteeMembers" />
					<aspire:AspireTextBox runat="server" ID="tbCommitteeMembers" ValidationGroup="CreditsTransfer" MaxLength="1000" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbCommitteeMembers" ErrorMessage="This field is required." ValidationGroup="CreditsTransfer" />
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" />
					<aspire:AspireTextBox runat="server" ID="tbRemarks" TextMode="MultiLine" ValidationGroup="CreditsTransfer" MaxLength="1000" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbRemarks" ErrorMessage="This field is required." ValidationGroup="CreditsTransfer" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12 text-center">
				<aspire:AspireButton runat="server" ID="btnSaveCreditsTransfer" Text="Save" ButtonType="Primary" ValidationGroup="CreditsTransfer" OnClick="btnSaveCreditsTransfer_OnClick" />
				<aspire:AspireButton runat="server" ID="btnDeleteCreditsTransfer" ButtonType="Danger" Text="Delete" CausesValidation="False" ConfirmMessage="Are you sure you want to delete this record?" OnClick="btnDeleteCreditsTransfer_OnClick" />
			</div>
		</div>
		<asp:Panel runat="server" ID="panelCourses">
			<aspire:AspireButton ButtonType="Success" Glyphicon="plus" runat="server" ID="btnAddCourse" Text="Add Course" CausesValidation="False" OnClick="btnAddCourse_OnClick" />
			<p></p>
			<aspire:AspireGridView ItemType="Aspire.BL.Core.CourseRegistration.Staff.StudentCreditsTransfers.StudentCreditsTransfer.Course" runat="server" ID="gvCreditsTransferred" AutoGenerateColumns="False" OnRowCommand="gvCreditsTransferred_OnRowCommand">
				<Columns>
					<asp:BoundField HeaderText="Course Title" DataField="CourseTitle" />
					<asp:BoundField HeaderText="Grade" DataField="Grade" />
					<asp:BoundField HeaderText="BU Equivalent Course" DataField="CourseInfo" />
					<asp:BoundField HeaderText="BU Grade" DataField="EquivalentGradeFullName" />
					<asp:TemplateField HeaderText="Actions">
						<ItemTemplate>
							<aspire:AspireLinkButton runat="server" CausesValidation="False" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument='<%# Item.StudentCreditsTransferredCourseID %>' />
							<aspire:AspireLinkButton runat="server" CausesValidation="False" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%# Item.StudentCreditsTransferredCourseID %>' ConfirmMessage="Are you sure you want to delete?" />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</aspire:AspireGridView>
			<aspire:AspireModal runat="server" ID="modalCreditsTransferred" ModalSize="None">
				<BodyTemplate>
					<asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updatePanelCreditsTransferred">
						<ContentTemplate>
							<aspire:AspireAlert runat="server" ID="modalAlert" />
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Course Title:" AssociatedControlID="tbCourseTitle" />
								<aspire:AspireTextBox runat="server" ID="tbCourseTitle" ValidationGroup="SaveCourse" MaxLength="250" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbCourseTitle" ErrorMessage="This field is required." ValidationGroup="SaveCourse" />
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Grade:" AssociatedControlID="tbGrade" />
								<aspire:AspireTextBox runat="server" ID="tbGrade" ValidationGroup="SaveCourse" MaxLength="50" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbGrade" ErrorMessage="This field is required." ValidationGroup="SaveCourse" />
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatus" />
								<aspire:AspireDropDownList runat="server" ID="ddlStatus" AutoPostBack="True" OnSelectedIndexChanged="ddlStatus_OnSelectedIndexChanged" ValidationGroup="SaveCourse">
									<asp:ListItem Value="Accepted">Accepted</asp:ListItem>
									<asp:ListItem Value="Rejected">Rejected</asp:ListItem>
								</aspire:AspireDropDownList>
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlStatus" ErrorMessage="This field is required." ValidationGroup="SaveCourse" />
							</div>
							<div id="divddlBUCourse" class="form-group" runat="server">
								<aspire:AspireLabel runat="server" Text="BU Course:" AssociatedControlID="ddlBUCourse" />
								<aspire:AspireDropDownList runat="server" ID="ddlBUCourse" ValidationGroup="SaveCourse" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlBUCourse" ErrorMessage="This field is required." ValidationGroup="SaveCourse" />
								<span class="help-block">Format: [Code] Title [Credit Hours]</span>
							</div>
							<div id="divddlBUGrade" class="form-group" runat="server">
								<aspire:AspireLabel runat="server" Text="BU Grade:" AssociatedControlID="ddlBUGrade" />
								<aspire:AspireDropDownList runat="server" ID="ddlBUGrade" ValidationGroup="SaveCourse" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlBUGrade" ErrorMessage="This field is required." ValidationGroup="SaveCourse" />
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
				</BodyTemplate>
				<FooterTemplate>
					<asp:UpdatePanel runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<aspire:AspireButton runat="server" ID="btnSaveCourse" OnClick="btnSaveCourse_OnClick" Text="Save" ButtonType="Primary" ValidationGroup="SaveCourse" />
							<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
						</ContentTemplate>
					</asp:UpdatePanel>
				</FooterTemplate>
			</aspire:AspireModal>
		</asp:Panel>
	</asp:Panel>
	<script type="text/javascript">
		$(function () {
			$("#<%= this.ddlBUCourse.ClientID %>").applySelect2();
		})
	</script>
</asp:Content>
