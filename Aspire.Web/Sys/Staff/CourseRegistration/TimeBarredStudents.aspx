﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="TimeBarredStudents.aspx.cs" Inherits="Aspire.Web.Sys.Staff.CourseRegistration.TimeBarredStudents" %>

<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Register TagPrefix="uc" TagName="studentinfo" Src="~/Sys/Staff/Common/StudentInfoUserControl.ascx" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
					<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="Search" OnSearch="tbEnrollment_OnSearch" />
				</div>
				<div class="form-group">
					<aspire:AspireHyperLink runat="server" Text="Clear" NavigateUrl="TimeBarredStudents.aspx" />
				</div>
			</div>
		</div>
	</div>
	<asp:Panel runat="server" ID="panelTimeBars" Visible="False">
		<uc:studentinfo runat="server" ID="ucStudentInfo" />
		<aspire:AspireGridView runat="server" ID="gvTimeBarredStudent" AutoGenerateColumns="False" Caption="Student Time Bars" OnRowCommand="gvTimeBarredStudent_OnRowCommand">
			<Columns>
				<asp:BoundField DataField="MinSemesterString" HeaderText="Minimum Semester" />
				<asp:BoundField DataField="MaxSemesterString" HeaderText="Maximum Semester" />
				<asp:BoundField DataField="CourseCategoriesAllowedFullNames" HeaderText="Course Categories Allowed" />
				<asp:BoundField DataField="MaxCreditHours" HeaderText="Max Credit Hours" />
				<asp:BoundField DataField="Remarks" HeaderText="Remarks" />
				<asp:TemplateField HeaderText="Action">
					<ItemTemplate>
						<aspire:AspireLinkButton runat="server" CommandName="Edit" EncryptedCommandArgument='<%#this.Eval(nameof(TimeBarredStudent.TimeBarredStudentID)) %>' Glyphicon="edit" CausesValidation="False" />
						<aspire:AspireLinkButton runat="server" CommandName="Delete" EncryptedCommandArgument='<%#this.Eval(nameof(TimeBarredStudent.TimeBarredStudentID)) %>' Glyphicon="remove" ConfirmMessage="Are you sure you want to delete?" CausesValidation="False" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
		<aspire:AspireButton runat="server" ID="btnAddTimeBar" Text="Add Time Bar" Glyphicon="plus" ButtonType="Success" CausesValidation="False" OnClick="btnAddTimeBar_OnClick" />
		<aspire:AspireModal runat="server" ID="modalTimeBarredStudent">
			<BodyTemplate>
				<asp:UpdatePanel runat="server" ID="updateModalTimeBarredStudentPanel" UpdateMode="Conditional">
					<ContentTemplate>
						<aspire:AspireAlert runat="server" ID="alertModalTimeBar" />
						<div class="form-horizontal">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Minimum Semester:" AssociatedControlID="ddlMinSemester" CssClass="col-md-4" />
								<div class="col-md-8">
									<aspire:AspireDropDownList runat="server" ID="ddlMinSemester" ValidationGroup="Required" AutoPostBack="True" OnSelectedIndexChanged="ddlMinSemester_OnSelectedIndexChanged" />
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlMinSemester" ErrorMessage="This field is required." ValidationGroup="Required" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Maximum Semester:" AssociatedControlID="ddlMaxSemester" CssClass="col-md-4" />
								<div class="col-md-8">
									<aspire:AspireDropDownList runat="server" ID="ddlMaxSemester" ValidationGroup="Required" />
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlMaxSemester" ErrorMessage="This field is required." ValidationGroup="Required" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Course Category:" AssociatedControlID="cbCourseCategory" CssClass="col-md-4" />
								<div class="col-md-8">
									<aspire:AspireCheckBoxList runat="server" ID="cbCourseCategory" RepeatLayout="Flow" RepeatDirection="Horizontal" ValidationGroup="Required" />
									<br />
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="cbCourseCategory" ErrorMessage="This field is required." ValidationGroup="Required" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Maximum Credit Hours:" AssociatedControlID="tbMaxCreditHours" CssClass="col-md-4" />
								<div class="col-md-8">
									<aspire:AspireTextBox runat="server" ID="tbMaxCreditHours" />
									<aspire:AspireByteValidator runat="server" ControlToValidate="tbMaxCreditHours" InvalidNumberErrorMessage="Invalid number." InvalidRangeErrorMessage="Invalid range." MinValue="0" MaxValue="255" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" CssClass="col-md-4" />
								<div class="col-md-8">
									<aspire:AspireTextBox runat="server" ID="tbRemarks" TextMode="MultiLine" ValidationGroup="Required" MaxLength="1000" />
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbRemarks" ErrorMessage="This field is required." ValidationGroup="Required" />
								</div>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</BodyTemplate>
			<FooterTemplate>
				<asp:UpdatePanel runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<div class="form-group">
							<div class="col-md-offset-2 col-md-10">
								<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" ValidationGroup="Required" OnClick="btnSave_OnClick" />
								<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</FooterTemplate>
		</aspire:AspireModal>
	</asp:Panel>
</asp:Content>
