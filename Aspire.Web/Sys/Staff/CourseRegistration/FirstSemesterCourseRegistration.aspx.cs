﻿using Aspire.BL.Core.CourseRegistration.Common;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.CourseRegistration
{
	[AspirePage("32FB4BFE-C796-4C66-9579-04C936199F0E", UserTypes.Staff, "~/Sys/Staff/CourseRegistration/FirstSemesterCourseRegistration.aspx", "First Semester Course Registration", true, AspireModules.CourseRegistration)]
	public partial class FirstSemesterCourseRegistration : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.import);
		public override string PageTitle => "First Semester Course Registration";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.CourseRegistration.Module, UserGroupPermission.Allowed},
		};

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters();
				if (this.ddlSemesterIDFilter.Items.Count == 0)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
				this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, null, true);
				this.ddlSemesterNoFilter.FillSemesterNos().SetEnumValue(SemesterNos.One).Enabled = false;
				this.ddlSectionFilter.FillSections(CommonListItems.All);
				this.ddlShiftFilter.FillShifts(CommonListItems.All);
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlProgramIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			if (programID != null)
			{
				var programMajors = BL.Core.Common.Lists.GetProgramMajors(programID.Value);
				this.ddlProgramMajorIDFilter.DataBind(programMajors, CommonListItems.None);
			}
			else
				this.ddlProgramMajorIDFilter.DataBind(CommonListItems.None);
			this.ddlSemesterNoFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterNoFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSectionFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSectionFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlShiftFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlShiftFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlProgramMajorIDFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlProgramMajorIDFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData();
		}

		private void GetData()
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			if (programID == null)
			{
				this.gvStudents.DataBindNull();
				return;
			}
			var semesterNo = this.ddlSemesterNoFilter.GetSelectedEnumValue<SemesterNos>(null);
			var section = this.ddlSectionFilter.GetSelectedEnumValue<Sections>(null);
			var shift = this.ddlShiftFilter.GetSelectedEnumValue<Shifts>(null);
			var programMajorID = this.ddlProgramMajorIDFilter.SelectedValue.ToNullableInt();
			var students = BL.Core.CourseRegistration.Staff.CourseRegistration.GetStudents(semesterID, programID.Value, programMajorID, semesterNo, section, shift, this.StaffIdentity.LoginSessionGuid);
			this.gvStudents.DataBind(students);
		}

		private List<CustomListItem> _programMajors;
		private int? _programID;

		protected void gvStudents_OnRowCreated(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow)
				return;
			var ddlProgramMajorID = e.Row.FindControl("ddlProgramMajorID") as AspireDropDownList;
			var ddlSemesterNo = e.Row.FindControl("ddlSemesterNo") as AspireDropDownList;
			var ddlSection = e.Row.FindControl("ddlSection") as AspireDropDownList;
			var ddlShift = e.Row.FindControl("ddlShift") as AspireDropDownList;
			var programID = this.ddlProgramIDFilter.SelectedValue.ToInt();
			if (this._programMajors == null || this._programID != programID)
			{
				this._programID = programID;
				this._programMajors = BL.Core.Common.Lists.GetProgramMajors(programID);
			}
			ddlProgramMajorID.DataBind(this._programMajors, CommonListItems.None);
			ddlSection.FillSections(CommonListItems.Select);
			ddlSemesterNo.FillSemesterNos(CommonListItems.Select);
			ddlShift.FillShifts(CommonListItems.Select);
		}

		protected void gvStudents_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow)
				return;
			var student = (Aspire.BL.Core.CourseRegistration.Staff.CourseRegistration.Student)e.Row.DataItem;
			if (!student.RegisteredCourses.Any())
			{
				e.Row.CssClass = "danger";
				((AspireDropDownList)e.Row.FindControl("ddlSemesterNo")).SetEnumValue(SemesterNos.One).Enabled = false;
			}
		}

		protected void btnRegister_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var studentsRegistrations = new List<BL.Core.CourseRegistration.Staff.CourseRegistration.StudentRegistration>();
			foreach (GridViewRow row in this.gvStudents.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow || !((AspireCheckBox)row.FindControl("cbSelect")).Checked)
					continue;

				var programMajorID = ((AspireDropDownList)row.FindControl("ddlProgramMajorID")).SelectedValue.ToNullableInt();
				var semesterNo = ((AspireDropDownList)row.FindControl("ddlSemesterNo")).GetSelectedEnumValue<SemesterNos>();
				var section = ((AspireDropDownList)row.FindControl("ddlSection")).GetSelectedEnumValue<Sections>();
				var shift = ((AspireDropDownList)row.FindControl("ddlShift")).GetSelectedEnumValue<Shifts>();
				var studentID = (int)this.gvStudents.DataKeys[row.RowIndex]["StudentID"];
				var enrollment = (string)this.gvStudents.DataKeys[row.RowIndex]["Enrollment"];
				studentsRegistrations.Add(new BL.Core.CourseRegistration.Staff.CourseRegistration.StudentRegistration(studentID, enrollment, semesterNo, section, shift, programMajorID));
			}
			this.RegisterCourses(studentsRegistrations);
		}

		private void RegisterCourses(List<BL.Core.CourseRegistration.Staff.CourseRegistration.StudentRegistration> studentsRegistrations)
		{
			if (!studentsRegistrations.Any())
			{
				this.AddInfoAlert("Select at least one student.");
				return;
			}
			studentsRegistrations = BL.Core.CourseRegistration.Staff.CourseRegistration.RegisterFirstSemesterCourses(studentsRegistrations, this.StaffIdentity.LoginSessionGuid);
			foreach (var studentRegistration in studentsRegistrations)
			{
				if (studentRegistration.CoursesRegister == null)
					this.AddErrorAlert($"{studentRegistration.Enrollment}: No relevant offered course found.");
				else
				{
					studentRegistration.CoursesRegister.GetErrorMessages().ForEach(m => this.AddErrorAlert($"{studentRegistration.Enrollment}: {m}"));
					studentRegistration.CoursesRegister.GetWarningMessages().ForEach(m => this.AddWarningAlert($"{studentRegistration.Enrollment}: {m}"));
					studentRegistration.CoursesRegister.GetSuccessMessages().ForEach(m => this.AddSuccessAlert($"{studentRegistration.Enrollment}: {m}"));
				}
			}
			this.GetData();
		}

		protected void gvStudents_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Register":
					var studentsRegistrations = new List<BL.Core.CourseRegistration.Staff.CourseRegistration.StudentRegistration>();
					var row = this.gvStudents.Rows[e.CommandArgument.ToString().ToInt()];
					var programMajorID = ((AspireDropDownList)row.FindControl("ddlProgramMajorID")).SelectedValue.ToNullableInt();
					var semesterNo = ((AspireDropDownList)row.FindControl("ddlSemesterNo")).GetSelectedEnumValue<SemesterNos>();
					var section = ((AspireDropDownList)row.FindControl("ddlSection")).GetSelectedEnumValue<Sections>();
					var shift = ((AspireDropDownList)row.FindControl("ddlShift")).GetSelectedEnumValue<Shifts>();
					var studentID = (int)this.gvStudents.DataKeys[row.RowIndex]["StudentID"];
					var enrollment = (string)this.gvStudents.DataKeys[row.RowIndex]["Enrollment"];
					studentsRegistrations.Add(new BL.Core.CourseRegistration.Staff.CourseRegistration.StudentRegistration(studentID, enrollment, semesterNo, section, shift, programMajorID));
					this.RegisterCourses(studentsRegistrations);
					break;
			}
		}

		protected void cv_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			var cbSelect = (AspireCheckBox)((AspireCustomValidator)source).FindControl("cbSelect");
			args.IsValid = !cbSelect.Checked || !string.IsNullOrEmpty(args.Value);
		}
	}
}