﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ExemptedCourses.aspx.cs" Inherits="Aspire.Web.Sys.Staff.CourseRegistration.ExemptedCourses" %>

<%@ Register TagPrefix="uc" TagName="StudentInfoControl" Src="~/Sys/Staff/Common/StudentInfoUserControl.ascx" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbSearchEnrollment" />
			<aspire:EnrollmentTextBox runat="server" ID="tbSearchEnrollment" ValidationGroup="ExemptedCourses" OnSearch="tbEnrollment_OnSearch" />
		</div>
	</div>
	<asp:Panel runat="server" ID="panelExemptedCourses">
		<uc:StudentInfoControl runat="server" ID="ucStudentBasicInfo" />
		<div class="row">
			<div class="col-md-12">
				<aspire:AspireButton runat="server" ButtonType="Success" Glyphicon="plus" Text="Add Exempted Course" ID="btnAddExemptedCourse" CausesValidation="False" OnClick="btnAddExemptedCourse_OnClick" />
			</div>
		</div>
		<p></p>
		<aspire:AspireGridView runat="server" ItemType="Aspire.BL.Core.CourseRegistration.Staff.ExemptedCourses.StudentExemptedCourse" AutoGenerateColumns="False" ID="gvExemptedCourses" OnRowCommand="gvExemptedCourses_OnRowCommand">
			<Columns>
				<asp:BoundField DataField="CourseCode" HeaderText="Code" />
				<asp:BoundField DataField="Title" HeaderText="Title" />
				<asp:BoundField DataField="CreditHoursFullName" HeaderText="Credit Hours" />
				<asp:BoundField DataField="ExemptionDate" HeaderText="Date" DataFormatString="{0:d}" />
				<asp:BoundField DataField="SubstitutedCourseCode" HeaderText="Substituted Code" />
				<asp:BoundField DataField="SubstitutedTitle" HeaderText="Substituted Title" />
				<asp:BoundField DataField="SubstitutedCreditHoursFullName" HeaderText="Substituted Credit Hours" />
				<asp:BoundField DataField="Remarks" HeaderText="Remarks" />
				<asp:TemplateField>
					<ItemTemplate>
						<aspire:AspireLinkButton runat="server" Glyphicon="edit" CausesValidation="False" CommandName="Edit" EncryptedCommandArgument="<%# Item.StudentExemptedCourseID %>" />
						<aspire:AspireLinkButton runat="server" Glyphicon="remove" CausesValidation="False" CommandName="Delete" EncryptedCommandArgument="<%# Item.StudentExemptedCourseID %>" ConfirmMessage="Are you sure you want to delete?" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
		<aspire:AspireModal runat="server" ID="modalAddEditExemptedCourse" HeaderText="Add/Edit Course">
			<BodyTemplate>
				<asp:Panel runat="server" CssClass="form-horizontal" DefaultButton="bntAddEditCourse">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Exempted Course:" AssociatedControlID="ddlCourseID" />
						<aspire:AspireDropDownList runat="server" ID="ddlCourseID" ValidationGroup="AddEditCourse" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlCourseID" ErrorMessage="This field is required." ValidationGroup="AddEditCourse" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Exemption Date:" AssociatedControlID="dtpExemptionDate" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpExemptionDate" ValidationGroup="AddEditCourse" />
						<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dtpExemptionDate" InvalidDataErrorMessage="Invalid Date" RequiredErrorMessage="This field is required" AllowNull="False" ValidationGroup="AddEditCourse" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Substituted Course:" AssociatedControlID="ddlSubstitutedCourseID" />
						<aspire:AspireDropDownList runat="server" ID="ddlSubstitutedCourseID" ValidationGroup="AddEditCourse" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" />
						<aspire:AspireTextBox CssClass="form-control" runat="server" ID="tbRemarks" TextMode="MultiLine" Rows="3" ValidationGroup="AddEditCourse" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbRemarks" ErrorMessage="This field is required." ValidationGroup="AddEditCourse" />
					</div>
				</asp:Panel>
			</BodyTemplate>
			<FooterTemplate>
				<aspire:AspireButton ButtonType="Primary" runat="server" CausesValidation="True" Text="Add/Save" ID="bntAddEditCourse" OnClick="bntAddEditCourse_OnClick" ValidationGroup="AddEditCourse" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</FooterTemplate>
		</aspire:AspireModal>
	</asp:Panel>
</asp:Content>
