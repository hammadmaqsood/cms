﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="StudentSemesters.aspx.cs" Inherits="Aspire.Web.Sys.Staff.CourseRegistration.StudentSemesters" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<%@ Register TagPrefix="uc" TagName="StudentInfoControl" Src="~/Sys/Staff/Common/StudentInfoUserControl.ascx" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
			<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="Search" OnSearch="tbEnrollment_OnSearch" />
		</div>
		<div class="form-group">
			<aspire:AspireHyperLink runat="server" Text="Clear" NavigateUrl="StudentSemesters.aspx" />
		</div>
	</div>
	<asp:Panel runat="server" ID="panelStudentSemester">
		<uc:StudentInfoControl runat="server" ID="ucStudentBasicInfo" />
		<aspire:AspireButton runat="server" Text="Add" ID="btnAddStudentSemester" ButtonType="Success" Glyphicon="plus" OnClick="btnAddStudentSemester_OnClick" />
		<aspire:AspireGridView ItemType="Aspire.Model.Entities.StudentSemester" runat="server" Caption="Student Semesters" AutoGenerateColumns="False" ID="gvStudentSemesters" OnRowCommand="gvStudentSemesters_OnRowCommand">
			<Columns>
				<asp:BoundField DataField="SemesterFullName" HeaderText="Semester" />
				<asp:BoundField DataField="SemesterNoFullName" HeaderText="Semester No." />
				<asp:BoundField DataField="SectionFullName" HeaderText="Section" />
				<asp:BoundField DataField="ShiftFullName" HeaderText="Shift" />
				<asp:BoundField DataField="StatusFullName" HeaderText="Status" />
				<asp:BoundField DataField="FreezedStatusFullName" HeaderText="Freezed Status" />
				<asp:BoundField HeaderText="Freezed Date" DataField="FreezedDate" DataFormatString="{0:d}" />
				<asp:TemplateField HeaderText="BU Exam GPA">
					 <ItemTemplate><%#: (Item.BUExamGPA?.FormatGPA()).ToNAIfNullOrEmpty() %></ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="BU Exam CGPA">
					 <ItemTemplate><%#: (Item.BUExamCGPA?.FormatGPA()).ToNAIfNullOrEmpty() %></ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="Remarks" DataField="Remarks" />
				<asp:TemplateField>
					<ItemTemplate>
						<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument='<%# Item.StudentSemesterID %>' />
						<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%# Item.StudentSemesterID %>' ConfirmMessage="Are you sure you want to delete this record?" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
		<aspire:AspireModal runat="server" ID="modalAddEditStudentSemesters">
			<BodyTemplate>
				<asp:UpdatePanel runat="server" ID="updatePanelSemester" UpdateMode="Conditional">
					<ContentTemplate>
						<aspire:AspireAlert runat="server" ID="modalAlert" />
						<div class="form-horizontal">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Semester:" CssClass="col-md-3" AssociatedControlID="ddlSemesterID" />
								<div class="col-md-9">
									<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="Add" />
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterID" ErrorMessage="This field is required." ValidationGroup="Add" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Semester No.:" CssClass="col-md-3" AssociatedControlID="ddlSemesterNo" />
								<div class="col-md-9">
									<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" ValidationGroup="Add" />
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterNo" ErrorMessage="This field is required." ValidationGroup="Add" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Section:" CssClass="col-md-3" AssociatedControlID="ddlSection" />
								<div class="col-md-9">
									<aspire:AspireDropDownList runat="server" ID="ddlSection" ValidationGroup="Add" />
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSection" ErrorMessage="This field is required." ValidationGroup="Add" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Shift:" CssClass="col-md-3" AssociatedControlID="ddlShift" />
								<div class="col-md-9">
									<aspire:AspireDropDownList runat="server" ID="ddlShift" ValidationGroup="Add" />
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlShift" ErrorMessage="This field is required." ValidationGroup="Add" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="BU Exam GPA:" CssClass="col-md-3" AssociatedControlID="tbBUExamGPA" />
								<div class="col-md-9">
									<aspire:AspireTextBox runat="server" MaxLength="4" ID="tbBUExamGPA" ValidationGroup="Add" />
									<aspire:AspireDoubleValidator AllowNull="true" MinValue="0" MaxValue="4" runat="server" ControlToValidate="tbBUExamGPA" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid GPA." InvalidRangeErrorMessage="Invalid GPA." ValidationGroup="Add" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="BU Exam CGPA:" CssClass="col-md-3" AssociatedControlID="tbBUExamCGPA" />
								<div class="col-md-9">
									<aspire:AspireTextBox runat="server" MaxLength="4" ID="tbBUExamCGPA" ValidationGroup="Add" />
									<aspire:AspireDoubleValidator AllowNull="true" MinValue="0" MaxValue="4" runat="server" ControlToValidate="tbBUExamCGPA" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid CGPA." InvalidRangeErrorMessage="Invalid CGPA." ValidationGroup="Add" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Remarks:" CssClass="col-md-3" AssociatedControlID="tbRemarks" />
								<div class="col-md-9">
									<aspire:AspireTextBox runat="server" ID="tbRemarks" TextMode="MultiLine" Rows="4" ValidationGroup="Add" />
								</div>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</BodyTemplate>
			<FooterTemplate>
				<asp:UpdatePanel runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<aspire:AspireButton ButtonType="Primary" runat="server" ID="bntAddEditStudentSemester" OnClick="bntAddEditStudentSemester_OnClick" ValidationGroup="Add" />
						<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
					</ContentTemplate>
				</asp:UpdatePanel>
			</FooterTemplate>
		</aspire:AspireModal>
	</asp:Panel>
</asp:Content>
