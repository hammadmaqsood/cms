﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="RegisterCourse.aspx.cs" Inherits="Aspire.Web.Sys.Staff.CourseRegistration.RegisterCourse" %>

<%@ Import Namespace="Aspire.Web.Sys.Staff.Students.ClassAttendance" %>
<%@ Import Namespace="Aspire.BL.Core.CourseRegistration.Staff" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<asp:Content ContentPlaceHolderID="HeadPH" runat="server">
	<style type="text/css">
		.middleCenter {
			vertical-align: middle !important;
			text-align: center !important;
		}

		.middleLeft {
			vertical-align: middle !important;
			text-align: left !important;
		}
	</style>
</asp:Content>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="Search" />
			<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterID" ErrorMessage="This field is required." ValidationGroup="Search" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
			<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="Search" OnSearch="tbEnrollment_OnSearch" />
		</div>
		<div class="form-group">
			<aspire:AspireHyperLink runat="server" Text="Clear" NavigateUrl="RegisterCourse.aspx" />
		</div>
	</div>
	<asp:Panel runat="server" ID="panelBody">
		<div class="table-responsive">
			<table class="table table-bordered table-condensed tableCol4">
				<caption>Student Information</caption>
				<tr>
					<th>Enrollment</th>
					<td>
						<aspire:AspireHyperLink runat="server" ID="hlEnrollment" /></td>
					<th>Registration No.</th>
					<td>
						<aspire:Label runat="server" ID="lblRegistrationNo" /></td>
				</tr>
				<tr>
					<th>Name</th>
					<td>
						<aspire:Label runat="server" ID="lblName" /></td>
					<th>Father Name</th>
					<td>
						<aspire:Label runat="server" ID="lblFatherName" /></td>
				</tr>
				<tr>
					<th>Program</th>
					<td>
						<aspire:Label runat="server" ID="lblProgramAlias" /></td>
					<th>Intake Semester</th>
					<td>
						<aspire:Label runat="server" ID="lblIntakeSemester" /></td>
				</tr>
				<tr>
					<th>Status</th>
					<td>
						<aspire:Label runat="server" ID="lblStatus" /></td>
					<th>Online Course Registration</th>
					<td>
						<aspire:AspireLinkButton ID="lbtnCanStudentRegisterCourseOnline" runat="server" OnClick="lbtnCanStudentRegisterCourseOnline_OnClick" CausesValidation="False" />
					</td>
				</tr>
				<tr>
					<th>Maximum Allowed</th>
					<td>
						<aspire:Label runat="server" ID="lblMaxAllowedCoursesPerSemester" />
						Courses Per Semester
					</td>
					<th>Maximum Allowed</th>
					<td>
						<aspire:Label runat="server" ID="lblMaxAllowedCreditHoursPerSemester" />
						Credit Hours Per Semester
					</td>
				</tr>
			</table>
		</div>
		<aspire:AspireGridView Caption="Classes" runat="server" ID="gvSemesterDetails" Condensed="True" AutoGenerateColumns="False" ItemType="Aspire.Model.Entities.StudentSemester">
			<Columns>
				<asp:TemplateField HeaderText="#">
					<ItemTemplate>
						<%#: Container.DataItemIndex+1 %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="Semester" DataField="SemesterFullName" />
				<asp:BoundField HeaderText="Semester No." DataField="SemesterNoFullName" />
				<asp:BoundField HeaderText="Section" DataField="SectionFullName" />
				<asp:BoundField HeaderText="Shift" DataField="ShiftFullName" />
				<asp:BoundField DataField="StatusFullName" HeaderText="Status" />
				<asp:TemplateField HeaderText="Freeze Status">
					<ItemTemplate>
						<%#: Item.FreezedStatusFullName.ToNAIfNullOrEmpty() %>
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>

		<div class="panel panel-default" runat="server">
			<div class="panel-heading">
				<h3 class="panel-title">
					<aspire:Label runat="server" ID="lblOfferedSemester" /></h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-9">
						<div class="form-inline">
							<div class="form-group form-group-sm">
								<aspire:AspireLabel runat="server" Text="Class:" AssociatedControlID="lblProgram" />
								<aspire:Label runat="server" ID="lblProgram" />
								-
							<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" ToolTip="Semester No." ValidationGroup="StudentSemester" />
								(
							<aspire:AspireDropDownList runat="server" ID="ddlSection" ToolTip="Section" ValidationGroup="StudentSemester" />
								)
							<aspire:AspireDropDownList runat="server" ID="ddlShift" ToolTip="Shift" ValidationGroup="StudentSemester" />
							</div>
							<div class="form-group form-group-sm">
								<aspire:AspireButton runat="server" ToolTip="Update Class" ID="btnUpdateStudentSemester" ButtonType="Primary" Text="Update" ValidationGroup="StudentSemester" OnClick="btnUpdateStudentSemester_OnClick" ConfirmMessage="Are you sure you want to update the class?" />
							</div>
						</div>
					</div>
					<div class="col-md-3 text-right">
						<div class="form-group form-group-sm">
							<button type="button" class="btn btn-default btn-sm" accesskey="P" data-toggle="modal" data-target="#<%=this.modalPreviousResult.ClientID %>">Previous Result</button>
						</div>
					</div>
					<div class="col-md-12" id="divRegisterButton" runat="server">
						<div class="form-inline pull-right">
							<div class="form-group">
								<aspire:AspireLabel runat="server" ID="lblBypassChecks" Text="Bypass Checks: " AssociatedControlID="chkBypassChecks" />
								<aspire:AspireCheckBoxList runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" ID="chkBypassChecks" AutoPostBack="False" CausesValidation="False" />
								<aspire:AspireButton ButtonSize="Small" CausesValidation="False" runat="server" ID="btnRegisterCourse" ButtonType="Default" AccessKey="R" OnClick="btnRegisterCourse_OnClick" Text="Register Course" />
							</div>
						</div>
					</div>
				</div>
				<asp:Repeater ID="repeaterRegisteredCourses" runat="server" OnItemCommand="repeaterRegisteredCourses_OnItemCommand" ItemType="Aspire.BL.Core.CourseRegistration.Staff.CourseRegistration.RegisteredCourse">
					<HeaderTemplate>
						<div class="table-responsive">
							<table class="table table-bordered table-condensed table-hover table-striped">
								<caption>Registered Courses</caption>
								<thead runat="server" visible="<%# ((List<CourseRegistration.RegisteredCourse>)this.repeaterRegisteredCourses.DataSource).Count>0%>">
									<tr>
										<th rowspan="2">#</th>
										<th colspan="2" class="text-center">Student Roadmap</th>
										<th colspan="3" class="text-center">Offered Course</th>
										<th rowspan="2">Registered By</th>
										<th rowspan="2">Actions</th>
									</tr>
									<tr>
										<th>Course</th>
										<th class="text-center">Credit<br />
											Hours</th>
										<th>Course</th>
										<th class="text-center">Credit<br />
											Hours</th>
										<th>Class</th>
									</tr>
								</thead>
								<tbody>
					</HeaderTemplate>
					<ItemTemplate>
						<tr>
							<td class="middleCenter"><%#: Container.ItemIndex + 1 %></td>
							<td class="middleLeft">
								<div>
									<span title="Course Code" class="label label-default"><%#: Item.RoadmapCourseCode %></span>
									<span runat="server" title="Majors" visible="<%# Item.RoadmapMajors != null %>" class="label label-info"><%#: Item.RoadmapMajors %></span>
									<aspire:AspireHyperLink runat="server" Target="_blank" NavigateUrl="<%# StudentSummary.GetPageUrl(Item.RegisteredCourseID) %>" ToolTip="Click to view student's attendance record.">
									<span class="label label-primary">Attendance</span>
									</aspire:AspireHyperLink>
									<span runat="server" visible="<%# Item.FeeDefaulter %>" class="label label-danger">Fee Defaulter</span>
								</div>
								<%#: Item.RoadmapTitle %>
								<div><%# GetBypassLabelsHtml(Item.BypassedChecksEnum) %></div>
							</td>
							<td class="middleCenter"><%#: Item.RoadmapCreditHoursFullName %></td>
							<td class="middleLeft">
								<span title="Course Code" class="label label-default"><%#: Item.OfferedCourseCode %></span>
								<span runat="server" visible="<%# Item.SpecialOffered %>" class="label label-info">SPECIAL</span>
								<br />
								<%#: Item.OfferedTitle %>
							</td>
							<td class="middleCenter"><%#: Item.OfferedCreditHoursFullName %></td>
							<td class="middleLeft">
								<div>
									<span runat="server" title="Faculty Member" visible="<%# Item.FacultyMemberName != null %>" class="label label-success"><%#: Item.FacultyMemberName %></span>
								</div>
								<%#: Item.OfferedClass %>
							</td>
							<td class="middleLeft">
								<%#: Item.RegisteredBy %>
								<br />
								<%#: Item.RegistrationDate.ToString("g") %>
							</td>
							<td class="middleCenter">
								<div class="btn-group" id="divDelete" runat="server" visible="<%# this.CanDeleteCourseRegistration || this.CanDeleteCourseRegistrationWithAttendance %>">
									<a href="#" class="dropdown-toggle text-nowrap" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions<span class="caret"></span></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li runat="server" visible="<%# this.CanDeleteCourseRegistration %>">
											<aspire:AspireLinkButton runat="server" CausesValidation="False" Text="Delete" CommandName="Delete" EncryptedCommandArgument="<%# Item.RegisteredCourseID %>" ConfirmMessage='<%# $"Are you sure you want to delete the registered course \"{Item.RoadmapTitle}\"?" %>' />
										</li>
										<li runat="server" visible="<%# this.CanDeleteCourseRegistrationWithAttendance %>">
											<aspire:AspireLinkButton runat="server" CausesValidation="False" Visible="<%# this.CanDeleteCourseRegistrationWithAttendance %>" Text="Delete With Attendance" CommandName="DeleteWithAttendance" EncryptedCommandArgument="<%# Item.RegisteredCourseID %>" ConfirmMessage='<%# $"Are you sure you want to delete the registered course \"{Item.RoadmapTitle}\" with its attendance record?" %>' />
										</li>
									</ul>
								</div>
							</td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
						<tr runat="server" visible="<%# this.repeaterRegisteredCourses.Items.Count==0 %>">
							<td colspan="10">No record found.</td>
						</tr>
						<tfoot>
							<tr runat="server" visible="<%# this.repeaterRegisteredCourses.Items.Count > 0 %>">
								<th colspan="2" class="text-right">Total Credit Hours</th>
								<th class="text-center"><%#: ((List<CourseRegistration.RegisteredCourse>)this.repeaterRegisteredCourses.DataSource)?.Sum(c=> c.RoadmapCreditHours).ToCreditHoursFullName() %></th>
							</tr>
						</tfoot>
						</tbody>
						</table>
						</div>
					</FooterTemplate>
				</asp:Repeater>
				<asp:Repeater ID="repeaterRequestedCourses" runat="server" OnItemCommand="repeaterRequestedCourses_OnItemCommand" ItemType="Aspire.BL.Core.CourseRegistration.Staff.CourseRegistration.RequestedCourse">
					<HeaderTemplate>
						<div class="table-responsive">
							<table class="table table-bordered table-condensed table-hover table-striped">
								<caption>Requested Courses</caption>
								<thead runat="server" visible="<%# ((List<CourseRegistration.RequestedCourse>)this.repeaterRequestedCourses.DataSource).Count>0%>">
									<tr>
										<th>Code</th>
										<th>Title</th>
										<th class="text-center">Credit Hours</th>
										<th>Class</th>
										<th>Special Course</th>
										<th>Requested</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
					</HeaderTemplate>
					<ItemTemplate>
						<tr>
							<td><%#: Item.OfferedCourseCode %></td>
							<td><%#: Item.OfferedTitle %></td>
							<td class="text-center"><%#: Item.OfferedCreditHoursFullName %></td>
							<td><%#: Item.OfferedClass %></td>
							<td><%#: Item.SpecialOfferedYesNo %></td>
							<td><%#: Item.RequestDate.ToString("d") %></td>
							<td>
								<aspire:AspireLinkButton ToolTip="Accept" CausesValidation="False" runat="server" Glyphicon="ok" CommandName="Accept" EncryptedCommandArgument="<%# Item.OfferedCourseID %>" />
								<aspire:AspireLinkButton ToolTip="Reject" CausesValidation="False" ConfirmMessage="Are you sure you want to delete?" runat="server" Glyphicon="remove" CommandName="Reject" EncryptedCommandArgument="<%# Item.RequestedCourseID %>" />
							</td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
						<tr runat="server" visible="<%# this.repeaterRequestedCourses.Items.Count==0 %>">
							<td colspan="10">No record found.</td>
						</tr>
						</tbody></table></div>
					</FooterTemplate>
				</asp:Repeater>
			</div>
		</div>
		<aspire:AspireModal runat="server" ID="modalOfferedCourses" HeaderText="Offered Courses" ModalSize="Large">
			<BodyTemplate>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group ">
							<div class="input-group">
								<input type="text" placeholder="Search..." class="form-control" id="inputSearchOfferedCourses" value='<%=$"{this.lblProgram.Text}-{this.ddlSemesterNo.SelectedItem.Text} ({this.ddlSection.SelectedItem.Text}) {this.ddlShift.SelectedItem.Text}" %>' />
								<div class="input-group-btn">
									<button type="button" class="btn btn-default" title="Clear" id="btnClearOfferedCourses"><i class="glyphicon glyphicon-remove"></i></button>
									<button type="button" class="btn btn-default" title="Search" id="btnSearchOfferedCourses"><i class="glyphicon glyphicon-search"></i></button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<p></p>
				<asp:Repeater runat="server" ID="repeaterOfferedCourses" OnItemCommand="repeaterOfferedCourses_OnItemCommand" ItemType="Aspire.BL.Core.CourseRegistration.Common.ExtensionMethods.OfferedCourse">
					<HeaderTemplate>
						<div class="table-responsive" style="max-height: 350px; overflow: auto">
							<table class="table table-bordered table-responsive table-striped table-condensed table-hover" id="<%= this.repeaterOfferedCourses.ClientID %>">
								<thead>
									<tr>
										<th colspan="4" class="text-center">Offered Course</th>
										<th colspan="4" class="text-center">Equivalent Roadmap Course</th>
									</tr>
									<tr>
										<th class="text-center">Title</th>
										<th class="text-center">Credit
													<br />
											Hours</th>
										<th class="text-center">Majors</th>
										<th>Class</th>
										<th class="text-center">Title</th>
										<th class="text-center">Credit Hours</th>
										<th class="text-center">Majors</th>
										<th class="text-center">Status</th>
									</tr>
								</thead>
								<tbody>
					</HeaderTemplate>
					<ItemTemplate>
						<tr>
							<td><%#: Item.CourseCode %>
								<br />
								<%#: Item.Title %>
							</td>
							<td><%#: Item.CreditHours %></td>
							<td><%#: Item.Majors %> </td>
							<td><%# Item.ClassNamesHtmlEncoded %></td>
							<td><%#: Item.EquivalentCourseCode %>
								<br />
								<%#: Item.EquivalentTitle %></td>
							<td><%#: Item.EquivalentCreditHours %></td>
							<td><%#: Item.EquivalentMajors %> </td>
							<td><%#: Item.EquivalentStatus?.GetFullName() %> </td>
							<td>
								<aspire:AspireLinkButton runat="server" CausesValidation="False" EncryptedCommandArgument='<%# $"{Item.OfferedCourseID}:{Item.EquivalentCourseID}" %>' CommandName="Register" Text="Register" />
							</td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
						</tbody>
						</table>
						</div>
					</FooterTemplate>
				</asp:Repeater>
			</BodyTemplate>
			<FooterTemplate>
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</FooterTemplate>
		</aspire:AspireModal>
		<aspire:AspireModal runat="server" ID="modalCourses" ModalSize="Large">
			<BodyTemplate>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group ">
							<div class="input-group">
								<input type="text" placeholder="Search..." class="form-control" id="inputSearchCourses" value="" />
								<div class="input-group-btn">
									<button type="button" class="btn btn-default" title="Clear" id="btnClearCourses"><i class="glyphicon glyphicon-remove"></i></button>
									<button type="button" class="btn btn-default" title="Search" id="btnSearchCourses"><i class="glyphicon glyphicon-search"></i></button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<p></p>
				<asp:Repeater runat="server" ID="repeaterCourses" OnItemCommand="repeaterCourses_OnItemCommand" ItemType="Aspire.BL.Core.CourseRegistration.Staff.CourseRegistration.Course">
					<HeaderTemplate>
						<div class="table-responsive" style="max-height: 350px; overflow: auto">
							<table class="table table-bordered table-responsive table-striped table-condensed table-hover" id="<%= this.repeaterCourses.ClientID %>">
								<thead>
									<tr>
										<th class="text-center">Code</th>
										<th>Title</th>
										<th class="text-center">Credit Hours</th>
										<th class="text-center">Majors</th>
										<th>Course Category</th>
										<th>Semester No.</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
					</HeaderTemplate>
					<ItemTemplate>
						<tr>
							<td><%#: Item.CourseCode %></td>
							<td><%#: Item.Title %></td>
							<td><%#: Item.CreditHours %></td>
							<td><%#: Item.Majors %> </td>
							<td><%#: Item.CourseCategoryFullName %> </td>
							<td><%#: Item.SemesterNoFullName %></td>
							<td><%#: Item.Status.GetFullName() %></td>
							<td>
								<aspire:AspireLinkButton runat="server" CausesValidation="False" EncryptedCommandArgument="<%# Item.CourseID %>" CommandName="Register" Text="Register" />
							</td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
						</tbody>
						</table>
						</div>
					</FooterTemplate>
				</asp:Repeater>
			</BodyTemplate>
			<FooterTemplate>
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</FooterTemplate>
		</aspire:AspireModal>

		<aspire:AspireModal runat="server" ID="modalPreviousResult" ModalSize="Large" HeaderText="Previous Result">
			<BodyTemplate>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group ">
							<div class="input-group">
								<input type="text" placeholder="Search..." class="form-control" id="inputSearchPreviousResultCourses" value="" />
								<div class="input-group-btn">
									<button type="button" class="btn btn-default" title="Clear" id="btnClearPreviousResultCourses"><i class="glyphicon glyphicon-remove"></i></button>
									<button type="button" class="btn btn-default" title="Search" id="btnSearchPreviousResultCourses"><i class="glyphicon glyphicon-search"></i></button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<p></p>
				<div style="max-height: 350px; overflow: auto">
					<aspire:AspireGridView runat="server" ID="gvPreviousResult" AutoGenerateColumns="False" ItemType="Aspire.BL.Core.CourseRegistration.Staff.CourseRegistration.RegisteredCourseResult">
						<Columns>
							<asp:BoundField HeaderText="Semester" DataField="OfferedSemester" />
							<asp:BoundField HeaderText="Code" DataField="RoadmapCourseCode" />
							<asp:BoundField HeaderText="Title" DataField="RoadmapTitle" />
							<asp:BoundField HeaderText="Credit Hours" DataField="RoadmapCreditHoursFullName" />
							<asp:BoundField HeaderText="Grade" DataField="GradeFullName" />
							<asp:BoundField HeaderText="Status" DataField="CompleteStatus" />
						</Columns>
					</aspire:AspireGridView>
					<aspire:AspireGridView runat="server" ID="gvPreviousResultTransferred" AutoGenerateColumns="False" ItemType="Aspire.BL.Core.CourseRegistration.Staff.CourseRegistration.RegisteredCourseResult">
						<Columns>
							<asp:BoundField HeaderText="Institute" DataField="InstituteAlias" />
							<asp:BoundField HeaderText="Program" DataField="ProgramAlias" />
							<asp:BoundField HeaderText="Enrollment" DataField="Enrollment" />
							<asp:BoundField HeaderText="Semester" DataField="OfferedSemester" />
							<asp:BoundField HeaderText="Code" DataField="RoadmapCourseCode" />
							<asp:BoundField HeaderText="Title" DataField="RoadmapTitle" />
							<asp:BoundField HeaderText="Credit Hours" DataField="RoadmapCreditHoursFullName" />
							<asp:BoundField HeaderText="Grade" DataField="GradeFullName" />
							<asp:BoundField HeaderText="Status" DataField="CompleteStatus" />
						</Columns>
					</aspire:AspireGridView>
				</div>
			</BodyTemplate>
			<FooterTemplate>
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</FooterTemplate>
		</aspire:AspireModal>
		<script type="text/javascript">
			$(document).ready(function () {
				var tableOfferedCourses = $("#<%=this.repeaterOfferedCourses.ClientID %>");
				applyFilterEvents(tableOfferedCourses, $("#inputSearchOfferedCourses"), $("#btnClearOfferedCourses"), $("#btnSearchOfferedCourses"), false);
				applyFilterEvents($("#<%=this.repeaterCourses.ClientID %>"), $("#inputSearchCourses"), $("#btnClearCourses"), $("#btnSearchCourses"), false);
				applyFilterEvents($("#<%=this.gvPreviousResult.ClientID %>"), $("#inputSearchPreviousResultCourses"), $("#btnClearPreviousResultCourses"), $("#btnSearchPreviousResultCourses"), false);
				$('#<%=this.modalPreviousResult.ClientID%>').on('shown.bs.modal', function () {
					$("#inputSearchPreviousResultCourses").val("").change().focus();
				});
			});
		</script>
	</asp:Panel>
</asp:Content>

