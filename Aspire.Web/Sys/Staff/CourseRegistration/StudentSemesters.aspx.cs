﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.CourseRegistration
{
	[AspirePage("85271585-96CF-497C-8A18-421EAD9AA042", UserTypes.Staff, "~/Sys/Staff/CourseRegistration/StudentSemesters.aspx", "Student Semesters", true, AspireModules.CourseRegistration)]
	public partial class StudentSemesters : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.CourseRegistration.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.import);
		public override string PageTitle => "Student Semesters";

		public static string GetPageUrl(string enrollment)
		{
			return typeof(StudentSemesters).GetAspirePageAttribute().PageUrl.AttachQueryParam("Enrollment", enrollment);
		}

		public static void Redirect(string enrollment)
		{
			BasePage.Redirect(GetPageUrl(enrollment));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Reset();
				var enrollment = this.Request.GetParameterValue("Enrollment");
				if (!string.IsNullOrWhiteSpace(enrollment))
				{
					this.tbEnrollment.Enrollment = enrollment;
					this.tbEnrollment_OnSearch(null, null);
				}
			}
		}

		private void Reset()
		{
			this.ViewState["StudentSemesterID"] = null;
			this.panelStudentSemester.Visible = false;
			this.ucStudentBasicInfo.Reset();
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs e)
		{
			var enrollment = this.tbEnrollment.Enrollment;
			this.Reset();
			var studentInfo = this.ucStudentBasicInfo.LoadBasicStudentInformation(enrollment);
			if (studentInfo == null)
			{
				this.AddNoRecordFoundAlert();
				return;
			}
			this.panelStudentSemester.Visible = true;
			this.GetSemestersData();
		}

		private void GetSemestersData()
		{
			var semesters = Aspire.BL.Core.CourseRegistration.Staff.StudentSemesters.GetStudentSemesters(this.ucStudentBasicInfo.StudentID.Value, this.StaffIdentity.LoginSessionGuid);
			this.gvStudentSemesters.DataBind(semesters);
		}

		protected void gvStudentSemesters_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayStudentSemesterModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var deleted = Aspire.BL.Core.CourseRegistration.Staff.StudentSemesters.DeleteStudentSemester(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					switch (deleted)
					{
						case BL.Core.CourseRegistration.Staff.StudentSemesters.DeleteStudentSemesterStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.RefreshPage();
							break;
						case BL.Core.CourseRegistration.Staff.StudentSemesters.DeleteStudentSemesterStatuses.SemesterIsFreezed:
							this.AddErrorAlert("Cannot be deleted. Reason: Semester is freezed.");
							break;
						case BL.Core.CourseRegistration.Staff.StudentSemesters.DeleteStudentSemesterStatuses.RegisteredCoursesFound:
							this.AddErrorAlert("Cannot be deleted. Reason: Courses has been registered in this semester.");
							break;
						case BL.Core.CourseRegistration.Staff.StudentSemesters.DeleteStudentSemesterStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Student Semester");
							this.RefreshPage();
							break;
						default:
							throw new NotImplementedEnumException(deleted);
					}
					break;
			}
		}

		protected void btnAddStudentSemester_OnClick(object sender, EventArgs e)
		{
			this.DisplayStudentSemesterModal(null);
		}

		protected void bntAddEditStudentSemester_OnClick(object sender, EventArgs e)
		{
			var studentSemesterID = (int?)this.ViewState["StudentSemesterID"];
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>();
			var sectionEnum = this.ddlSection.GetSelectedEnumValue<Sections>();
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>();
			var buExamGPA = this.tbBUExamGPA.Text.ToNullableDecimal();
			var buExamCGPA = this.tbBUExamCGPA.Text.ToNullableDecimal();
			var remarks = this.tbRemarks.Text;

			if (studentSemesterID != null)
			{
				var status = Aspire.BL.Core.CourseRegistration.Staff.StudentSemesters.UpdateStudentSemester(studentSemesterID.Value, semesterNoEnum, sectionEnum, shiftEnum, buExamGPA, buExamCGPA, remarks, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.CourseRegistration.Staff.StudentSemesters.UpdateStudentSemesterStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						return;
					case BL.Core.CourseRegistration.Staff.StudentSemesters.UpdateStudentSemesterStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Student Semester");
						this.RefreshPage();
						return;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
			else
			{
				var status = Aspire.BL.Core.CourseRegistration.Staff.StudentSemesters.AddStudentSemester(this.ucStudentBasicInfo.StudentID.Value, semesterID, semesterNoEnum, sectionEnum, shiftEnum, buExamGPA, buExamCGPA, remarks, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.CourseRegistration.Staff.StudentSemesters.AddStudentSemesterStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						break;
					case BL.Core.CourseRegistration.Staff.StudentSemesters.AddStudentSemesterStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Student Semester");
						this.RefreshPage();
						break;
					case BL.Core.CourseRegistration.Staff.StudentSemesters.AddStudentSemesterStatuses.RecordAlreadyExists:
						this.modalAlert.AddErrorAlert("Semester record already exists.");
						this.updatePanelSemester.Update();
						return;
					case BL.Core.CourseRegistration.Staff.StudentSemesters.AddStudentSemesterStatuses.CannotAddSemesterBeforeIntake:
						this.modalAlert.AddErrorAlert("Semester before intake cannot be added.");
						this.updatePanelSemester.Update();
						return;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
		}

		private void RefreshPage()
		{
			Redirect(this.ucStudentBasicInfo.Enrollment);
		}

		private void DisplayStudentSemesterModal(int? studentSemesterID)
		{
			this.ddlSemesterID.FillSemesters();
			this.ddlSemesterNo.FillSemesterNos();
			this.ddlSection.FillSections();
			this.ddlShift.FillShifts();
			var canManageGPACGPA = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.CourseRegistration.ManageStudentGPACGPA) == UserGroupPermission.PermissionValues.Allowed;
			this.tbBUExamGPA.ReadOnly = !canManageGPACGPA;
			this.tbBUExamCGPA.ReadOnly = !canManageGPACGPA;
			if (studentSemesterID != null)
			{
				var studentSemester = Aspire.BL.Core.CourseRegistration.Staff.StudentSemesters.GetStudentSemester(studentSemesterID.Value, this.StaffIdentity.LoginSessionGuid);
				if (studentSemester == null)
				{
					this.AddNoRecordFoundAlert();
					return;
				}
				this.ddlSemesterID.SelectedValue = studentSemester.SemesterID.ToString();
				this.ddlSemesterID.Enabled = false;
				this.ddlSemesterNo.SetEnumValue(studentSemester.SemesterNoEnum);
				this.ddlSection.SetEnumValue(studentSemester.SectionEnum);
				this.ddlShift.SetEnumValue(studentSemester.ShiftEnum);
				this.tbBUExamGPA.Text = studentSemester.BUExamGPA.FormatGPA();
				this.tbBUExamCGPA.Text = studentSemester.BUExamCGPA.FormatGPA();
				this.tbRemarks.Text = studentSemester.Remarks;
				this.ViewState["StudentSemesterID"] = studentSemesterID;
				this.modalAddEditStudentSemesters.HeaderText = "Update Semester";
				this.bntAddEditStudentSemester.Text = "Save";
			}
			else
			{
				this.ddlSemesterID.ClearSelection();
				this.ddlSection.ClearSelection();
				this.ddlSemesterNo.ClearSelection();
				this.ddlShift.ClearSelection();
				this.tbBUExamGPA.Text = null;
				this.tbBUExamCGPA.Text = null;
				this.tbRemarks.Text = null;
				this.ViewState["StudentSemesterID"] = null;
				this.modalAddEditStudentSemesters.HeaderText = "Add Semester";
				this.ddlSemesterNo.SetEnumValue(SemesterNos.One);
				this.bntAddEditStudentSemester.Text = "Add";
			}
			this.modalAddEditStudentSemesters.Show();
		}
	}
}