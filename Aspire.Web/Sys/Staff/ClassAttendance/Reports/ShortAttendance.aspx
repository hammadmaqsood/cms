﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="ShortAttendance.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ClassAttendance.Reports.ShortAttendance" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlProgramIDFilter_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Minimum Percentage:" AssociatedControlID="tbMinimumPercentage" />
			<asp:Panel CssClass="input-group" DefaultButton="btnSearch" runat="server">
				<aspire:AspireTextBox runat="server" ID="tbMinimumPercentage" ValidationGroup="Parameters" Text="75" />
				<div class="input-group-btn">
					<aspire:AspireButton runat="server" ButtonType="Primary" Glyphicon="search" ValidationGroup="Parameters" ID="btnSearch" OnClick="btnSearch_OnClick" />
				</div>
			</asp:Panel>
			<aspire:AspireDoubleValidator runat="server" MinValue="0" MaxValue="100" ControlToValidate="tbMinimumPercentage" ValidationGroup="Parameters" InvalidNumberErrorMessage="Invalid number." InvalidRangeErrorMessage="Invalid percentrage." />
		</div>
	</div>
</asp:Content>
