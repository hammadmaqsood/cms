﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using Aspire.BL.Entities.Common;

namespace Aspire.Web.Sys.Staff.ClassAttendance.Reports
{
	[AspirePage("20688533-7D36-4B74-89AB-C94DFAFF2B94", UserTypes.Staff, "~/Sys/Staff/ClassAttendance/Reports/LectureWiseAttendance.aspx", "Lecture Wise Attendance", true, AspireModules.ClassAttendance)]
	public partial class LectureWiseAttendance : StaffPage,IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Lecture Wise Attendance";
		public string ReportName => this.PageTitle;
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ClassAttendance.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};

		public static string GetPageUrl(int offeredCourseID, bool? inline, ReportFormats? reportFormat)
		{
			return GetPageUrl<LectureWiseAttendance>().AttachQueryParams("OfferedCourseID", offeredCourseID, ReportView.ReportFormat, reportFormat, ReportView.Inline, inline);
		}

		public static void Redirect(int offeredCourseID)
		{
			Redirect(GetPageUrl(offeredCourseID,true,ReportFormats.PDF));
		}

		private int? OfferedCourseID => this.Request.GetParameterValue<int>("OfferedCourseID");

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var reportView = (ReportView) this.Master;
				this.RenderReport(reportView.ReportViewer);
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this.OfferedCourseID == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect<ViewClassAttendance>();
				return;
			}
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/ClassAttendance/Staff/LectureWiseAttendance.rdlc".MapPath();
			var reportDataSet = BL.Core.ClassAttendance.Staff.Reports.FacultyWiseAttendance.GetOfferedCourseAttendances(this.OfferedCourseID.Value, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.CourseInformation), new[] { reportDataSet.CourseInformation}));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.LectureWiseAttendances), reportDataSet.LectureWiseAttendances));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}