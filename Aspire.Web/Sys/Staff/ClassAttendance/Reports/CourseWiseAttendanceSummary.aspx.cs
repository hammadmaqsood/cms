﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.ClassAttendance.Reports
{
	[AspirePage("30F7CFC7-119B-4EA5-B316-50B6B02B4BCB", UserTypes.Staff, "~/Sys/Staff/ClassAttendance/Reports/CourseWiseAttendanceSummary.aspx", "Course Wise Attendance Summary", true, AspireModules.ClassAttendance)]
	public partial class CourseWiseAttendanceSummary : StaffPage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Course Wise Attendance Summary";
		public string ReportName => this.PageTitle;
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ClassAttendance.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters();
				if (this.ddlSemesterIDFilter.Items.Count == 0)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All);
				this.ddlFacultyType.FillEnums<FacultyMember.FacultyTypes>(CommonListItems.All);
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, departmentID, departmentID == null, CommonListItems.All);
			this.ddlProgramIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlFacultyType_OnSelectedIndexChanged(null,null);
		}
		public void RenderReport(ReportViewer reportViewer)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var facultyTypeEnum = this.ddlFacultyType.GetSelectedEnumValue<FacultyMember.FacultyTypes>(null);
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/ClassAttendance/Staff/CourseWiseAttendanceSummary.rdlc".MapPath();
			var reportDataSet = BL.Core.ClassAttendance.Staff.Reports.CourseWiseAttendanceSummary.GetCourseWiseAttendanceSummary(semesterID, departmentID, programID, facultyTypeEnum, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.CourseWiseAttendanceSummary), reportDataSet.CourseWiseAttendanceSummary));
			reportViewer.LocalReport.EnableExternalImages = true;
		}

		protected void ddlFacultyType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView.ReportViewer);
		}
	}
}