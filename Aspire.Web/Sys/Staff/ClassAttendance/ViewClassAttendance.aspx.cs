﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Aspire.BL.Entities.Common;
using Aspire.Web.Sys.Staff.ClassAttendance.Reports;

namespace Aspire.Web.Sys.Staff.ClassAttendance
{
	[AspirePage("8E69056E-3FA3-452D-A0DC-340A092DC724", UserTypes.Faculty, "~/Sys/Staff/ClassAttendance/ViewClassAttendance.aspx", "View Class Attendance", true, AspireModules.ClassAttendance)]
	public partial class ViewClassAttendance : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.book);
		public override string PageTitle => "Lecture Wise Attendance";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ClassAttendance.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};

		private int? OfferedCourseID => this.Request.GetParameterValue<int>("OfferedCourseID");

		public static string GetPageUrl(int offeredCourseID)
		{
			return GetPageUrl<ViewClassAttendance>().AttachQueryParam("OfferedCourseID", offeredCourseID);
		}

		public static void Redirect(int offeredCourseID)
		{
			Redirect(GetPageUrl(offeredCourseID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.OfferedCourseID == null)
				{
					Redirect<Courses>();
					return;
				}
				this.hlLectureWiseAttendanceReport.NavigateUrl = LectureWiseAttendance.GetPageUrl(this.OfferedCourseID.Value,true,ReportFormats.PDF);
				var offeredCourseAttendance = BL.ClassAttendance.Faculty.GetOfferedCourseAttendances(this.OfferedCourseID.Value);
				if (offeredCourseAttendance == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Courses>();
					return;
				}

				this.lblCourseTitle.Text = $"[{offeredCourseAttendance.CourseCode}] {offeredCourseAttendance.Title}";
				this.lblTeacherName.Text = offeredCourseAttendance.TeacherName.ToNAIfNullOrEmpty();
				this.lblCreditHrs.Text = offeredCourseAttendance.CreditHours.ToCreditHoursFullName();
				this.lblContactHrs.Text = offeredCourseAttendance.ContactHours.ToCreditHoursFullName();
				this.lblClass.Text = offeredCourseAttendance.ClassName;
				this.lblSemesterID.Text = offeredCourseAttendance.SemesterID.ToSemesterString();
				if (offeredCourseAttendance.Details.Any())
					this.gvAttendanceRecord.ShowFooter = true;
				this.gvAttendanceRecord.DataBind(offeredCourseAttendance.Details);
				if (this.gvAttendanceRecord.ShowFooter && this.gvAttendanceRecord.FooterRow != null)
				{
					var hours = offeredCourseAttendance.Details.Sum(d => d.Hours);
					this.gvAttendanceRecord.FooterRow.Cells[3].Text = hours.ToString("N");
				}
			}
		}
	}
}