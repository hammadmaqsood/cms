﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Courses.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ClassAttendance.Courses" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-3 form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" CausesValidation="False" AutoPostBack="True" />
		</div>
		<div class="form-group col-md-3">
			<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="ddlSemesterNo"/>
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" OnSelectedIndexChanged="ddlSemesterNo_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
		<div class="form-group col-md-3">
			<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSection" />
			<aspire:AspireDropDownList runat="server" ID="ddlSection" OnSelectedIndexChanged="ddlSection_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
		<div class="form-group col-md-3">
			<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShift" />
			<aspire:AspireDropDownList runat="server" ID="ddlShift" OnSelectedIndexChanged="ddlShift_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-4">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramID" OnSelectedIndexChanged="ddlProgramID_OnSelectedIndexChanged" CausesValidation="False" AutoPostBack="True" />
		</div>
		<div class="form-group col-md-8">
			<aspire:AspireLabel runat="server" Text="Faculty Name:" AssociatedControlID="ddlFacultyMemberID" />
			<aspire:AspireDropDownList runat="server" ID="ddlFacultyMemberID" OnSelectedIndexChanged="ddlFacultyMemberID_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ItemType="Aspire.BL.Core.Exams.Staff.OfferedCourses.OfferedCourse" ID="gvCourses" AutoGenerateColumns="False" OnPageSizeChanging="gvCourses_OnPageSizeChanging" OnPageIndexChanging="gvCourses_OnPageIndexChanging" OnSorting="gvCourses_OnSorting" AllowSorting="True">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate>
					<%#: Container.DataItemIndex + 1 %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Semester" SortExpression="Semester">
				<ItemTemplate>
					<%#: Item.SemesterID.ToSemesterString() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Title" DataField="Title" SortExpression="Title" />
			<asp:BoundField HeaderText="Class" DataField="Class" />
			<asp:BoundField HeaderText="Teacher" DataField="Teacher" SortExpression="Teacher" />
			<asp:TemplateField HeaderText="Category" SortExpression="CategoryEnum">
				<ItemTemplate>
					<%#: Item.CategoryEnum.ToFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" Text="Lecture wise attendance" NavigateUrl='<%#Aspire.Web.Sys.Staff.ClassAttendance.ViewClassAttendance.GetPageUrl(Item.OfferedCourseID) %>' Target="_blank" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<script type="text/javascript">
		$(document).ready(function () {
			$("#<%=this.ddlFacultyMemberID.ClientID%>").applySelect2();
		});
	</script>
</asp:Content>
