﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ViewClassAttendance.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ClassAttendance.ViewClassAttendance" %>

<%@ Import Namespace="Aspire.Web.Sys.Staff.ClassAttendance" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<style>
		table#courseInfo th {
			white-space: nowrap;
		}

		table#courseInfo td {
			width: 50%;
		}
	</style>
	<aspire:AspireHyperLink runat="server" ID="hlLectureWiseAttendanceReport" Text="Print version"  CssClass="btn btn-primary" Target="_blank" />
	<table class="table table-bordered table-condensed" id="courseInfo">
		<caption>Course Information</caption>
		<tbody>
			<tr>
				<th>Title</th>
				<td>
					<aspire:Label runat="server" ID="lblCourseTitle" />
				</td>
				<th>Teacher Name</th>
				<td>
					<aspire:Label runat="server" ID="lblTeacherName" />
				</td>
			</tr>
			<tr>
				<th>Credit Hours</th>
				<td>
					<aspire:Label runat="server" ID="lblCreditHrs" />
				</td>
				<th>Contact Hours</th>
				<td>
					<aspire:Label runat="server" ID="lblContactHrs" />
				</td>
			</tr>
			<tr>
				<th>Class</th>
				<td>
					<aspire:Label runat="server" ID="lblClass" />
				</td>
				<th>Semester</th>
				<td>
					<aspire:Label runat="server" ID="lblSemesterID" />
				</td>
			</tr>
		</tbody>
	</table>
	<style scoped="scoped">
		table[id$=gvAttendanceRecord] td:first-child,
		table[id$=gvAttendanceRecord] th:first-child,
		table[id$=gvAttendanceRecord] td:nth-child(4),
		table[id$=gvAttendanceRecord] th:nth-child(4) {
			text-align: center;
		}
	</style>
	<aspire:AspireGridView Caption="Attendance Record" runat="server" CssClass="table-condensed" ID="gvAttendanceRecord" AutoGenerateColumns="False" ShowFooter="True">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate>
					<aspire:Label runat="server" Text="<%#: Container.DataItemIndex + 1 %>" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Class Date" DataField="ClassDate" DataFormatString="{0:D}" />
			<asp:BoundField HeaderText="Class Type" DataField="ClassTypeFullName" />
			<asp:BoundField HeaderText="Hours" DataField="HoursValueFullName" />
			<asp:BoundField HeaderText="Room" DataField="RoomName" />
			<asp:BoundField HeaderText="Remarks" DataField="Remarks" />
			<asp:BoundField HeaderText="Marked on Date/Time" DataField="MarkedDate" DataFormatString="{0:F}" />
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireHyperLink  Text="Details" runat="server" NavigateUrl='<%# ClassWiseAttendance.GetPageUrl((int) this.Eval("OfferedCourseID"), (int) this.Eval("OfferedCourseAttendanceID")) %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
		<FooterStyle Font-Bold="True" />
	</aspire:AspireGridView>
</asp:Content>
