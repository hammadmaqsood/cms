﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Aspire.Lib.Extensions;

namespace Aspire.Web.Sys.Staff.ClassAttendance
{
	[AspirePage("2D54C3DB-F301-4C78-A5BE-E7DC891935D6", UserTypes.Staff, "~/Sys/Staff/ClassAttendance/StudentWiseAttendance.aspx", "Student Wise", true, AspireModules.ClassAttendance)]
	public partial class StudentWiseAttendance : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.ClassAttendance.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.import);
		public override string PageTitle => "Student Wise";

		public static string GetPageUrl(int? studentID)
		{
			return GetPageUrl<StudentWiseAttendance>().AttachQueryParam("StudentID", studentID);
		}

		public static string GetPageUrl(string enrollment)
		{
			return GetPageUrl<StudentWiseAttendance>().AttachQueryParam("Enrollment", enrollment);
		}

		public static void Redirect(int? studentID)
		{
			Redirect<StudentWiseAttendance>("StudentID", studentID);
		}

		public static void Redirect(string enrollment)
		{
			Redirect<StudentWiseAttendance>("Enrollment", enrollment);
		}

		private void Reset()
		{
			this.ucStudentInfo.Reset();
			this.panelStudentAttedance.Visible = false;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Reset();
				this.ddlSemesterID.FillSemesters();
				if (this.ddlSemesterID.Items.Count == 0)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<StudentWiseAttendance>();
					return;
				}
				string enrollment = null;
				var studentID = this.Request.GetParameterValue<int>("StudentID");
				if (studentID != null)
					enrollment = Aspire.BL.Common.Common.GetEnrollmentFromStudentID(studentID.Value);
				if (enrollment == null)
					enrollment = this.Request.GetParameterValue("Enrollment");
				if (!string.IsNullOrWhiteSpace(enrollment))
				{
					this.tbEnrollment.Enrollment = enrollment;
					this.tbEnrollment_OnSearch(null, null);
				}
			}
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs e)
		{
			var enrollment = this.tbEnrollment.Enrollment;
			if (string.IsNullOrWhiteSpace(enrollment))
			{
				this.Reset();
				this.AddNoRecordFoundAlert();
				return;
			}
			var studentinfo = this.ucStudentInfo.LoadBasicStudentInformation(enrollment);
			if (studentinfo == null)
			{
				this.Reset();
				this.AddNoRecordFoundAlert();
				return;
			}
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var registeredCourses = BL.Core.ClassAttendance.Staff.StudentWiseAttendance.GetRegisteredCourses(semesterID, studentinfo.StudentID, this.StaffIdentity.LoginSessionGuid);
			var dataSource = registeredCourses.Select(rc => rc.SemesterID).Distinct().ToList().Select(s => new
			{
				SemesterID = s,
				Semester = s.ToSemesterString(),
				RegisteredCourses = registeredCourses.FindAll(rc => rc.SemesterID == s),
			}).OrderByDescending(s => s.SemesterID).ToList();
			this.repeaterRegisteredCourses.DataBind(dataSource);
			this.panelStudentAttedance.Visible = true;
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var enrollment = this.tbEnrollment.Enrollment;
			if (string.IsNullOrWhiteSpace(enrollment))
			{
				this.Reset();
				this.AddNoRecordFoundAlert();
				return;
			}
			this.tbEnrollment_OnSearch(null, null);
		}
	}
}