﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="StudentWiseAttendance.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ClassAttendance.StudentWiseAttendance" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.BL.Core.CourseRegistration.Student" %>
<%@ Register TagPrefix="uc" TagName="studentinfo" Src="~/Sys/Staff/Common/StudentInfoUserControl.ascx" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
					<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="RequiredEnrollment" OnSearch="tbEnrollment_OnSearch" />
				</div>
				<div class="form-group">
					<aspire:AspireHyperLink runat="server" Text="Clear" NavigateUrl="StudentWiseAttendance.aspx" />
				</div>
			</div>
		</div>
	</div>
	<asp:Panel runat="server" ID="panelStudentAttedance">
		<uc:studentinfo runat="server" ID="ucStudentInfo" />
	</asp:Panel>
	<div class="table-responsive">
		<asp:Repeater runat="server" ID="repeaterRegisteredCourses">
			<ItemTemplate>
				<table class="table table-bordered table-responsive table-condensed table-hover">
					<caption>
						<%#:this.Eval("Semester") %></caption>
					<thead>
						<tr>
							<th class="text-center">#</th>
							<th class="text-center">Code</th>
							<th>Registered Course Title</th>
							<th class="text-center">Credit<br />
								Hours</th>
							<th class="text-center">Majors</th>
							<th>Offered Course Title</th>
							<th>Class</th>
							<th>Teacher Name</th>
							<th>Fee Status</th>
							<th>Present Hours</th>
							<th>Absent Hours</th>
							<th>Total Hours</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<asp:Repeater runat="server" DataSource='<%#this.Eval("RegisteredCourses") %>'>
							<ItemTemplate>
								<tr>
									<td class="text-center"><%#: Container.ItemIndex+1 %></td>
									<td class="text-center"><%#: this.Eval("RegisteredCourseCode") %></td>
									<td><%#:this.Eval("RegisteredTitle") %></td>
									<td class="text-center"><%#:this.Eval("RegisteredCreditHours") %></td>
									<td class="text-center"><%#:	this.Eval("RegisteredMajors").ToNAIfNullOrEmpty() %> </td>
									<td><%#:this.Eval("Title") %></td>
									<td><%#:this.Eval("ClassName") %></td>
									<td><%#:this.Eval("TeacherName").ToNAIfNullOrEmpty() %> </td>
									<td class="text-center"><%#:this.Eval("FeeStatusFullName").ToNAIfNullOrEmpty() %> </td>
									<td class="text-center"><%#:$"{this.Eval("PresentHours")} :{this.Eval("PercentageShort")}%" %></td>
									<td class="text-center"><%#:this.Eval("AbsentHours")%> </td>
									<td class="text-center"><%#:this.Eval("TotalHours") %> </td>
									<td class="text-center">
										<aspire:AspireHyperLink runat="server" NavigateUrl='<%# Aspire.Web.Sys.Staff.Students.ClassAttendance.StudentSummary.GetPageUrl((int)this.Eval("RegisteredCourseID"),(bool)false)  %>' Text="View Attendance Detail" Target="_blank" /></td>
								</tr>
							</ItemTemplate>
						</asp:Repeater>
					</tbody>
				</table>
			</ItemTemplate>
		</asp:Repeater>
	</div>
</asp:Content>
