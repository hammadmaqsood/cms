﻿using Aspire.BL.Entities.ClassAttendance;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.ClassAttendance
{
	[AspirePage("626B9D9F-572C-4EC5-90A9-7A0B9AA48938", UserTypes.Staff, "~/Sys/Staff/ClassAttendance/ClassWiseAttendance.aspx", "Class Wise Attendance", true, AspireModules.ClassAttendance)]
	public partial class ClassWiseAttendance : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.list);
		public override string PageTitle => "Class Attendance";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.ClassAttendance.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};

		private bool ByPassChecks => true;
		private int? OfferedCourseID => this.Request.GetParameterValue<int>("OfferedCourseID");
		private int? OfferedCourseAttendanceID => this.Request.GetParameterValue<int>("OfferedCourseAttendanceID");

		public static string GetPageUrl(int offeredCourseID, int? offeredCourseAttendanceID)
		{
			return GetPageUrl<ClassWiseAttendance>().AttachQueryParams("OfferedCourseID", offeredCourseID, "OfferedCourseAttendanceID", offeredCourseAttendanceID);
		}


		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.OfferedCourseID == null || this.OfferedCourseAttendanceID == null)
				{
					Redirect<ViewClassAttendance>();
					return;
				}
				this.hlLectureWiseAttendance.NavigateUrl = ViewClassAttendance.GetPageUrl(this.OfferedCourseID.Value);
				this.hlCancel.NavigateUrl = ViewClassAttendance.GetPageUrl(this.OfferedCourseID.Value);
				//todo Making Single Source For Faculty, Staff , or Admin, With their permission.
				var offeredCourse = BL.ClassAttendance.Faculty.GetOfferedCourseAttendances(this.OfferedCourseID.Value, this.OfferedCourseAttendanceID, this.ByPassChecks, out var canMark, out var canEdit, out var canDelete); // Making same source as in faculty.
				if (offeredCourse == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<ViewClassAttendance>();
					return;
				}
				this.lblClass.Text = offeredCourse.ClassName;
				this.lblContactHrs.Text = offeredCourse.ContactHours.ToCreditHoursFullName();
				this.lblCourseTitle.Text = offeredCourse.Title;
				this.lblCreditHrs.Text = offeredCourse.CreditHours.ToCreditHoursFullName();
				this.lblTeacherName.Text = offeredCourse.Teacher;
				this.lblSemesterID.Text = offeredCourse.OfferedSemesterID.ToSemesterString();

				this.rblNoOfHours.FillHoursValues(true);
				this.rblClassTypes.FillSemesters().FillEnums<OfferedCourseAttendance.ClassTypes>();
				this.ddlRoom.GetRoomsForAttendance(this.StaffIdentity.InstituteID);
				if (offeredCourse.OfferedCourseAttendanceID != null)
				{
					this.rblClassTypes.SetEnumValue(offeredCourse.ClassTypeEnum.Value);
					this.rblNoOfHours.SetEnumValue(offeredCourse.HoursValue.Value);
					this.dtpClassDate.SelectedDate = offeredCourse.ClassDate.Value;
					this.ddlRoom.SelectedValue = offeredCourse.RoomID.ToString();
					this.tbRemarks.Text = offeredCourse.Remarks;
					this.dtpClassDate.ReadOnly = true;
					this.ddlRoom.Enabled = false;
					this.rblClassTypes.Enabled = false;
					this.rblNoOfHours.Enabled = false;
					this.rblNoOfHours.AutoPostBack = false;
					this.rblNoOfHours.SelectedIndexChanged -= this.rblNoOfHours_OnSelectedIndexChanged;
					this.tbRemarks.ReadOnly = true;
				}
				this.rblNoOfHours_OnSelectedIndexChanged(null, null);
			}
		}

		protected void gvOfferedCourseEnrollments_OnRowCreated(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow)
				return;
			var rblHours = (AspireRadioButtonList)e.Row.FindControl("rblHours");
			var totalHours = this.rblNoOfHours.GetSelectedEnumValue<OfferedCourseAttendance.HoursValues>();
			var presentIfOneOrOneAndHalf = totalHours == OfferedCourseAttendance.HoursValues.One || totalHours == OfferedCourseAttendance.HoursValues.OneAndHalf;
			rblHours.FillHoursValues(totalHours.GetSmallerOrEqualHourValue(), presentIfOneOrOneAndHalf);
			rblHours.Enabled = false;
			if (!this.IsPostBack && this.OfferedCourseAttendanceID != null)
			{
				var hours = ((Aspire.BL.Entities.ClassAttendance.CustomOfferedCourseAttendanceDetails)e.Row.DataItem).HoursValue;
				switch (hours)
				{
					case null:
					case OfferedCourseAttendance.HoursValues.Absent:
						e.Row.CssClass = "bg-danger";
						break;
					case OfferedCourseAttendance.HoursValues.One:
					case OfferedCourseAttendance.HoursValues.OneAndHalf:
					case OfferedCourseAttendance.HoursValues.Two:
					case OfferedCourseAttendance.HoursValues.Three:
						e.Row.CssClass = totalHours == hours.Value ? "bg-success" : "bg-warning";
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		protected void rblNoOfHours_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var offeredCourseID = this.OfferedCourseID.Value;
			List<CustomOfferedCourseAttendanceDetails> attendanceDetails;
			//todo Making Single Source For Faculty, Staff , or Admin, With their permission.
			attendanceDetails = BL.ClassAttendance.Faculty.GetOfferedCourseAttendanceDetails(offeredCourseID, this.OfferedCourseAttendanceID.Value); // Making same source as in faculty.
			this.tableSummary.Visible = true;
			var totalHours = this.rblNoOfHours.GetSelectedEnumValue<OfferedCourseAttendance.HoursValues>();
			var presentCell = this.tableSummary.Rows[0].Cells[0];
			var partialCell = this.tableSummary.Rows[0].Cells[1];
			var absentCell = this.tableSummary.Rows[0].Cells[2];
			switch (totalHours)
			{
				case OfferedCourseAttendance.HoursValues.Absent:
					throw new InvalidOperationException();
				case OfferedCourseAttendance.HoursValues.One:
				case OfferedCourseAttendance.HoursValues.OneAndHalf:
					presentCell.Text = $"Present: {attendanceDetails.Count(a => a.HoursValue == totalHours)}";
					partialCell.Visible = false;
					absentCell.Text = $"Absent: {attendanceDetails.Count(a => a.HoursValue != totalHours)}";
					break;
				case OfferedCourseAttendance.HoursValues.Two:
				case OfferedCourseAttendance.HoursValues.Three:
					presentCell.Text = $"Full Present: {attendanceDetails.Count(a => a.HoursValue == totalHours)}";
					partialCell.Text = $"Partial Present: {attendanceDetails.Count(a => a.HoursValue != null && a.HoursValue != OfferedCourseAttendance.HoursValues.Absent && a.HoursValue != totalHours)}";
					absentCell.Text = $"Full Absent: {attendanceDetails.Count(a => (a.HoursValue ?? OfferedCourseAttendance.HoursValues.Absent) == OfferedCourseAttendance.HoursValues.Absent)}";
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.gvOfferedCourseEnrollments.DataBind(attendanceDetails);
		}
	}
}