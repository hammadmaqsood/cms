﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff
{
	[AspirePage("B46EB4BE-E957-49E2-8099-75463C2FC40C", UserTypes.Staff, "~/Sys/Staff/ChangePassword.aspx", "Change Password", true, AspireModules.None)]
	public partial class ChangePassword : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.lock_);
		public override string PageTitle => "Change Password";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired { get; }

		protected void Page_Load(object sender, EventArgs e)
		{
		}
	}
}