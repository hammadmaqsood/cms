﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CreateCandidateAccount.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.CreateCandidateAccount" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:Panel CssClass="form-horizontal" runat="server" DefaultButton="btnRegister">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Applying for Semester:" AssociatedControlID="rblSemesterID" />
			<aspire:AspireTextBox runat="server" ID="tbSemester" ValidationGroup="Register" ReadOnly="True" />
			<div>
				<aspire:AspireRadioButtonList runat="server" ID="rblSemesterID" RepeatLayout="Flow" RepeatDirection="Horizontal" />
			</div>
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Full Name (as per SSC):" AssociatedControlID="tbName" />
			<aspire:AspireTextBox runat="server" AutoCompleteType="Disabled" TextTransform="UpperCase" ID="tbName" PlaceHolder="Full Name (as per SSC)" MaxLength="100" ValidationGroup="Register" />
			<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbName" RequiredErrorMessage="This field is required." ValidationGroup="Register" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Email:" AssociatedControlID="tbEmail" />
			<aspire:AspireTextBox runat="server" AutoCompleteType="Disabled" TextTransform="LowerCase" ID="tbEmail" PlaceHolder="Email" MaxLength="100" ValidationGroup="Register" />
			<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbEmail" RequiredErrorMessage="This field is required." ValidationGroup="Register" InvalidDataErrorMessage="Invalid Email address." ValidationExpression="Email" />
		</div>
		<div class="form-group">
			<aspire:AspireButton Text="Register" ButtonType="Primary" ValidationGroup="Register" ID="btnRegister" CssClass="btn-block" runat="server" OnClick="btnRegister_OnClick" />
		</div>
	</asp:Panel>
</asp:Content>
