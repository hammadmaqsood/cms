﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Admissions
{
	[AspirePage("A378FD7F-A6B4-41E0-BEA8-33FF8BED4824", UserTypes.Staff, "~/Sys/Staff/Admissions/StudentTransfer.aspx", "Student Transfer", true, AspireModules.Admissions)]
	public partial class StudentTransfer : StaffPage
	{
		public override PageIcon PageIcon => AspireGlyphicons.transfer.GetIcon();
		public override string PageTitle => "Student Transfer";

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};

		public static string GetPageUrl(int instituteID, string enrollment, bool search)
		{
			return typeof(StudentTransfer).GetAspirePageAttribute().PageUrl.AttachQueryParams(nameof(InstituteID), instituteID, nameof(Enrollment), enrollment, "Search", search);
		}

		public static void Redirect(int instituteID, string enrollment, bool search)
		{
			Redirect(GetPageUrl(instituteID, enrollment, search));
		}
		private int? InstituteID
		{
			get => (int?)this.ViewState[nameof(this.InstituteID)];
			set => this.ViewState[nameof(this.InstituteID)] = value;
		}
		private int? StudentID
		{
			get => (int?)this.ViewState[nameof(this.StudentID)];
			set => this.ViewState[nameof(this.StudentID)] = value;
		}

		private string Enrollment
		{
			get => (string)this.ViewState[nameof(this.Enrollment)];
			set => this.ViewState[nameof(this.Enrollment)] = value;
		}

		private void RefreshPage()
		{
			Redirect(this.InstituteID ?? throw new InvalidOperationException(), this.Enrollment, true);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.panelStudentInfo.Visible = false;
				this.ddlInstituteIDSearch.FillAllActiveInstitutes(CommonListItems.Select).SetSelectedValueIfNotPostback(nameof(this.InstituteID));
				this.tbEnrollmentSearch.Enrollment = this.GetParameterValue(nameof(this.Enrollment));
				if (this.ddlInstituteIDSearch.SelectedValue.ToNullableInt() != null && this.tbEnrollmentSearch.Enrollment != null && this.GetParameterValue<bool>("Search") == true)
					this.tbEnrollmentSearch_OnSearch(null, null);
			}
		}

		protected void tbEnrollmentSearch_OnSearch(object sender, EventArgs args)
		{
			if (this.IsPostbackAndNotValid)
				return;
			var student = BL.Core.Admissions.Staff.StudentTransfer.GetStudent(this.ddlInstituteIDSearch.SelectedValue.ToInt(), this.tbEnrollmentSearch.Enrollment, this.StaffIdentity.LoginSessionGuid);
			if (student == null)
			{
				this.panelStudentInfo.Visible = false;
				this.AddNoRecordFoundAlert();
				return;
			}

			this.Enrollment = student.Enrollment;
			this.InstituteID = student.InstituteID;
			this.StudentID = student.StudentID;

			this.panelStudentInfo.Visible = true;
			this.lblEnrollment.Text = student.Enrollment;
			this.lblRegistrationNo.Text = student.RegistrationNo.ToString().ToNAIfNullOrEmpty();
			this.lblName.Text = student.Name;
			this.lblInstituteAlias.Text = student.InstituteAlias;
			this.lblProgram.Text = student.ProgramShortName;
			this.lblIntakeSemesterID.Text = student.IntakeSemesterID.ToSemesterString();
			this.lblStatus.Text = student.StatusFullName.ToNAIfNullOrEmpty();
			this.lblStatusDate.Text = (student.StatusDate?.ToShortDateString()).ToNAIfNullOrEmpty();
			this.repeaterTransferredStudents.DataBind(student.TransferredStudents);
			this.ddlIntakeSemesterID.FillSemesters(CommonListItems.Select);
			this.ddlIntakeSemesterID_OnSelectedIndexChanged(null, null);
		}

		protected void btnAddStudentTransfer_OnClick(object sender, EventArgs e)
		{
			this.tbInstituteAlias.Text = BL.Core.Common.Common.GetInstitute(this.StaffIdentity.InstituteID, this.StaffIdentity.LoginSessionGuid)?.InstituteAlias ?? throw new InvalidOperationException();
			this.ddlIntakeSemesterID.FillSemesters();
			this.ddlIntakeSemesterID_OnSelectedIndexChanged(null, null);
			this.modalTransfer.Show();
		}

		protected void ddlIntakeSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var intakeSemesterID = this.ddlIntakeSemesterID.SelectedValue.ToNullableShort();
			if (intakeSemesterID == null)
				this.ddlAdmissionOpenProgramID.Items.Clear();
			else
			{
				var admissionOpenPrograms = BL.Core.Common.Lists.GetAdmissionOpenPrograms(intakeSemesterID.Value, this.StaffIdentity.InstituteID);
				this.ddlAdmissionOpenProgramID.DataBind(admissionOpenPrograms, CommonListItems.Select);
			}
			this.ddlAdmissionOpenProgramID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlAdmissionOpenProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var admissionOpenProgramID = this.ddlAdmissionOpenProgramID.SelectedValue.ToNullableInt();
			if (admissionOpenProgramID == null)
				this.ddlShift.DataBind(CommonListItems.Select);
			else
				this.ddlShift.FillShifts(CommonListItems.Select);
			this.ddlShift_OnSelectedIndexChanged(null, null);
		}

		protected void ddlShift_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>(null);
			if (shiftEnum == null)
				this.ddlTransferSemesterID.DataBind(CommonListItems.Select);
			else
			{
				var intakeSemesterID = this.ddlIntakeSemesterID.SelectedValue.ToNullableShort() ?? throw new InvalidOperationException("Intake semester cannot be null.");
				this.ddlTransferSemesterID.FillSemesters(intakeSemesterID, CommonListItems.Select);
			}
			this.ddlTransferSemesterID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlTransferSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var transferSemesterID = this.ddlTransferSemesterID.SelectedValue.ToNullableShort();
			if (transferSemesterID == null)
			{
				this.tbNewEnrollment.Text = null;
				return;
			}

			var studentID = this.StudentID ?? throw new InvalidOperationException();
			var admissionOpenProgramID = this.ddlAdmissionOpenProgramID.SelectedValue.ToInt();
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>();
			var (enrollment, alreadyGenerated) = BL.Core.Admissions.Staff.StudentTransfer.GetNewEnrollment(studentID, admissionOpenProgramID, shiftEnum, this.StaffIdentity.LoginSessionGuid);

			this.tbNewEnrollment.Text = alreadyGenerated
				? $"Existing {enrollment} enrollment will be assigned."
				: $"New {enrollment} enrollment will be assigned.";
		}

		protected void btnTransfer_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var studentID = this.StudentID ?? throw new InvalidOperationException();
			var admissionOpenProgramID = this.ddlAdmissionOpenProgramID.SelectedValue.ToInt();
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>();
			var transferSemesterID = this.ddlTransferSemesterID.SelectedValue.ToShort();
			var remarks = this.tbRemarks.Text;
			var result = BL.Core.Admissions.Staff.StudentTransfer.TransferStudent(studentID, admissionOpenProgramID, shiftEnum, transferSemesterID, remarks, this.StaffIdentity.LoginSessionGuid);
			if (result == null)
			{
				this.AddNoRecordFoundAlert();
				this.RefreshPage();
				return;
			}
			switch (result.Status)
			{
				case BL.Core.Admissions.Staff.StudentTransfer.TransferStudentResult.Statuses.ErrorAlreadyEnrolled:
					this.alertModalTransfer.AddErrorAlert("Student is already enrolled in selected program.");
					this.updatePanelTransfer.Update();
					return;
				case BL.Core.Admissions.Staff.StudentTransfer.TransferStudentResult.Statuses.ErrorAlreadyTransferred:
					this.alertModalTransfer.AddErrorAlert($"Student is already transferred from the selected enrollment.");
					this.updatePanelTransfer.Update();
					return;
				case BL.Core.Admissions.Staff.StudentTransfer.TransferStudentResult.Statuses.ErrorStudentStatusMustBeTransferredToOtherCampus:
					this.alertModalTransfer.AddErrorAlert($"Student status must be \"{Model.Entities.Student.Statuses.TransferredToOtherCampus.ToFullName()}\".");
					this.updatePanelTransfer.Update();
					return;
				case BL.Core.Admissions.Staff.StudentTransfer.TransferStudentResult.Statuses.ErrorStudentStatusMustBeProgramChanged:
					this.alertModalTransfer.AddErrorAlert($"Student status must be \"{Model.Entities.Student.Statuses.ProgramChanged.ToFullName()}\".");
					this.updatePanelTransfer.Update();
					return;
				case BL.Core.Admissions.Staff.StudentTransfer.TransferStudentResult.Statuses.ErrorCannotTransferToPreviousIntakeSemesters:
					this.alertModalTransfer.AddErrorAlert($"Student cannot be transferred to previous intake semesters.");
					this.updatePanelTransfer.Update();
					return;
				case BL.Core.Admissions.Staff.StudentTransfer.TransferStudentResult.Statuses.ErrorStudentCannotTransferToPreviousSemesters:
					this.alertModalTransfer.AddErrorAlert($"Student cannot transfer to previous semesters.");
					this.updatePanelTransfer.Update();
					return;
				case BL.Core.Admissions.Staff.StudentTransfer.TransferStudentResult.Statuses.Success:
					this.AddSuccessAlert($"Student has been transferred.");
					this.RefreshPage();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}