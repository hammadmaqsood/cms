﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Admissions
{
	[AspirePage("EC92851E-EB6F-4A72-B497-0A8D12C6CF4B", UserTypes.Staff, "~/Sys/Staff/Admissions/SearchCandidate.aspx", "Search Candidate", true, AspireModules.Admissions)]
	public partial class SearchCandidate : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_search);
		public override string PageTitle => "Search Candidate";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterID.FillSemesters(CommonListItems.All);
				this.ddlForeignStudent.FillYesNo(CommonListItems.All);
				this.tbChallanNo.Attributes.Add("data-inputmask", $"'mask': '{this.StaffIdentity.InstituteCode}1999999999'");
				this.ViewState.SetSortProperties("Name", SortDirection.Ascending);
			}
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			this.GetSearchCandidate(0);
		}

		protected void gvSearchCandidate_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetSearchCandidate(e.NewPageIndex);
		}

		protected void gvSearchCandidate_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvSearchCandidate.PageSize = e.NewPageSize;
			this.GetSearchCandidate(0);
		}

		protected void gvSearchCandidate_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetSearchCandidate(0);
		}

		private void GetSearchCandidate(int pageIndex)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var challanNo = AspireFormats.ChallanNo.TryParse(this.tbChallanNo.Text);
			if (challanNo == null)
				this.tbChallanNo.Text = null;
			var applicationNo = this.tbApplicationNo.Text.ToNullableInt();
			var name = this.tbName.Text;
			var fatherName = this.tbFatherName.Text;
			var email = this.tbEmail.Text;
			var foreignStudent = this.ddlForeignStudent.SelectedValue.ToNullableBoolean();
			var pageSize = this.gvSearchCandidate.PageSize;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var result = BL.Core.Admissions.Staff.CandidateInfo.GetCandidateInformation(semesterID, challanNo, applicationNo, name, fatherName, email, foreignStudent, pageIndex, pageSize, sortExpression, sortDirection, this.StaffIdentity.LoginSessionGuid);
			this.gvSearchCandidate.DataBind(result.Candidates, pageIndex, result.VirtualItemCount, sortExpression, sortDirection);
		}
	}
}