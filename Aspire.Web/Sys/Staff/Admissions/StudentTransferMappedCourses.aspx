﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="StudentTransferMappedCourses.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.StudentTransferMappedCourses" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.BL.Entities.Common" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<table class="table table-bordered table-condensed tableCol4">
		<tr>
			<th>Enrollment</th>
			<td>
				<aspire:Label runat="server" ID="lblEnrollment" />
			</td>
			<th>Registration No.</th>
			<td>
				<aspire:Label runat="server" ID="lblRegistrationNo" />
			</td>
		</tr>
		<tr>
			<th>Name</th>
			<td>
				<aspire:Label runat="server" ID="lblName" />
			</td>
			<th>Transfer Semester</th>
			<td>
				<aspire:Label runat="server" ID="lblTransferSemester" />
			</td>
		</tr>
		<tr>
			<th>From Campus</th>
			<td>
				<aspire:Label runat="server" ID="lblFromInstituteAlias" />
			</td>
			<th>To Campus</th>
			<td>
				<aspire:Label runat="server" ID="lblToInstituteAlias" />
			</td>
		</tr>
		<tr>
			<th>From Program</th>
			<td>
				<aspire:Label runat="server" ID="lblFromProgramShortName" />
			</td>
			<th>To Program</th>
			<td>
				<aspire:Label runat="server" ID="lblToProgramShortName" />
			</td>
		</tr>
		<tr>
			<th>From Intake Semester</th>
			<td>
				<aspire:Label runat="server" ID="lblFromIntakeSemesterID" />
			</td>
			<th>To Intake Semester</th>
			<td>
				<aspire:Label runat="server" ID="lblToIntakeSemesterID" />
			</td>
		</tr>
	</table>
	<asp:Panel runat="server" ID="panelCourseMappings">
		<aspire:AspireButton runat="server" Glyphicon="edit" Text="Edit" CausesValidation="False" ID="btnEdit" OnClick="btnEdit_OnClick" />
		<p></p>
		<asp:Repeater runat="server" ID="repeaterTransferredStudents" ItemType="Aspire.Web.Sys.Staff.Admissions.StudentTransferMappedCourses.CourseMapping">
			<HeaderTemplate>
				<table class="table table-hover table-bordered table-responsive table-striped AspireGridView">
					<thead>
						<tr>
							<th rowspan="2">#</th>
							<th colspan="5">Registered Course</th>
							<th colspan="4">Roadmap Equivalent Course</th>
						</tr>
						<tr>
							<th>Institute</th>
							<th>Program</th>
							<th>Code</th>
							<th>Title</th>
							<th>Credit Hours</th>
							<th>Code</th>
							<th>Title</th>
							<th>Credit Hours</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><%#: Container.ItemIndex+1 %></td>
					<td><%#: Item.RegisteredCourse.InstituteAlias %></td>
					<td><%#: Item.RegisteredCourse.ProgramAlias %></td>
					<td><%#: Item.RegisteredCourse.CourseCode %></td>
					<td><%#: Item.RegisteredCourse.Title %></td>
					<td><%#: Item.RegisteredCourse.CreditHours.ToCreditHoursFullName() %></td>
					<td><%#: Item.RoadmapCourse?.CourseCode %></td>
					<td><%#: Item.RoadmapCourse?.Title %></td>
					<td><%#: Item.RoadmapCourse?.CreditHours.ToCreditHoursFullName() %></td>
					<td><%#: Item.TransferredStudentCourseMapping?.StatusEnum.ToFullName() %></td>
				</tr>
			</ItemTemplate>
			<FooterTemplate>
				<td colspan="10" runat="server" visible="<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>">No record found.</td>
				</tbody>
				</table>
			</FooterTemplate>
		</asp:Repeater>
		<div class="text-center">
			<aspire:AspireHyperLink runat="server" Text="Cancel" ID="hlCancel" />
		</div>
	</asp:Panel>
	<asp:Panel runat="server" ID="panelCourseMappingsEdit">
		<asp:Repeater runat="server" ID="repeaterTransferredStudentsEdit" ItemType="Aspire.Web.Sys.Staff.Admissions.StudentTransferMappedCourses.CourseMapping">
			<HeaderTemplate>
				<table class="table table-hover table-bordered table-responsive table-striped AspireGridView">
					<thead>
						<tr>
							<th rowspan="2">#</th>
							<th colspan="4">Registered Course</th>
							<th rowspan="2">Roadmap Equivalent Course<br />
								Format: <em>[Code] Title [Credit Hours]</em></th>
							<th rowspan="2">Status</th>
						</tr>
						<tr>
							<th>Institute</th>
							<th>Program</th>
							<th>Semester</th>
							<th>[Code] Title [Credit Hours]</th>
						</tr>
					</thead>
					<tbody>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><%#: Container.ItemIndex+1 %></td>
					<td><%#: Item.RegisteredCourse.InstituteAlias %></td>
					<td><%#: Item.RegisteredCourse.ProgramAlias %></td>
					<td><%#: Item.RegisteredCourse.OfferedSemesterID.ToSemesterString() %></td>
					<td><%#: $"[{Item.RegisteredCourse.CourseCode}] {Item.RegisteredCourse.Title} [{Item.RegisteredCourse.CreditHours.ToCreditHoursFullName()}]" %></td>
					<td>
						<aspire:HiddenField Mode="Encrypted" runat="server" Visible="False" ID="hfRegisteredCourseID" Value="<%# Item.RegisteredCourse.RegisteredCourseID%>" />
						<aspire:AspireDropDownList ValidationGroup="Save" runat="server" CssClass="Courses" AppendDataBoundItems="True" ID="ddlRoadmapCourseID" SelectedValue="<%# Item.RoadmapCourse?.CourseID %>" DataSource="<%# this.RoadmapCourses %>" DataValueField="<%# nameof(CustomListItem.ID) %>" DataTextField="<%# nameof(CustomListItem.Text) %>">
							<Items>
								<asp:ListItem Text="Select" Value="" />
							</Items>
						</aspire:AspireDropDownList>
					</td>
					<td>
						<aspire:AspireDropDownList ValidationGroup="Save" runat="server" ID="ddlStatus" SelectedValue="<%# Item.TransferredStudentCourseMapping?.Status ?? (byte)Aspire.Model.Entities.TransferredStudentCourseMapping.Statuses.Included  %>" DataSource="<%# this.Statuses %>" DataValueField="<%# nameof(CustomListItem.ID) %>" DataTextField="<%# nameof(CustomListItem.Text) %>" />
					</td>
				</tr>
			</ItemTemplate>
			<FooterTemplate>
				<tr colspan="10" runat="server" visible="<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>"><td>No record found.</td></tr>
				</tbody>
			</table>
			</FooterTemplate>
		</asp:Repeater>
		<div class="text-center">
			<aspire:AspireButton runat="server" ID="btnSaveMappings" ValidationGroup="Save" Text="Save" OnClick="btnSaveMappings_OnClick" />
			<aspire:AspireHyperLink runat="server" Text="Cancel" ID="hlCancelEdit" />
		</div>
		<script type="text/javascript">
			$(function () { $("select.Courses").applySelect2(); });
		</script>
	</asp:Panel>
</asp:Content>
