﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Admissions
{
	[AspirePage("F41CA751-2096-4F53-AF16-563F43E30743", UserTypes.Staff, "~/Sys/Staff/Admissions/BankBranches.aspx", "Bank Branches", true, AspireModules.Admissions)]
	public partial class BankBranches : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.building);
		public override string PageTitle => "Bank Branches";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("BranchName", SortDirection.Ascending);
				var banks = BL.Core.Admissions.Staff.AdmissionProcessingFee.GetBanksForAdmissionProcessingFeeList(this.StaffIdentity.LoginSessionGuid);
				this.ddlBankID.DataBind(banks, CommonListItems.All);
				this.ddlBankID_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlBankID_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetBankBranches(0);
		}

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			this.GetBankBranches(0);
		}

		protected void gvBankBranches_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetBankBranches(e.NewPageIndex);
		}

		protected void gvBankBranches_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvBankBranches.PageSize = e.NewPageSize;
			this.GetBankBranches(0);
		}

		protected void gvBankBranches_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetBankBranches(0);
		}

		private void GetBankBranches(int pageIndex)
		{
			var bankID = this.ddlBankID.SelectedItem.Value.ToNullableInt32();
			var searchText = this.tbSearch.Text.Trim();
			var result = Aspire.BL.Core.Admissions.Staff.AdmissionProcessingFee.GetBankBranches(bankID, searchText, pageIndex, this.gvBankBranches.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.StaffIdentity.LoginSessionGuid);
			this.gvBankBranches.DataBind(result.BankBranches, pageIndex, result.VirtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}
	}
}