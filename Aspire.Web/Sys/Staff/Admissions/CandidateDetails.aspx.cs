﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Admissions
{
	[AspirePage("B32FB730-5A6D-4268-A98D-24A013270E2A", UserTypes.Staff, "~/Sys/Staff/Admissions/CandidateDetails.aspx", "Candidate Profile", true, AspireModules.Admissions)]
	public partial class CandidateDetails : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_user);
		public override string PageTitle => "Candidate Profile";

		public static string GetPageUrl(int? candidateID)
		{
			return GetPageUrl<CandidateDetails>().AttachQueryParam("CandidateID", candidateID);
		}

		public static void Redirect(int? candidateID)
		{
			Redirect(GetPageUrl(candidateID));
		}

		private int? CandidateID => this.Request.GetParameterValue<int>("CandidateID");

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.CandidateID == null)
				{
					Redirect<SearchCandidate>();
					return;
				}

				var customCandidate = this.profile.LoadCandidateInfo(this.CandidateID.Value, this.StaffIdentity.LoginSessionGuid);
				if (customCandidate == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<SearchCandidate>();
				}
			}
		}
	}
}