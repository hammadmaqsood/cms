﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CandidateDetails.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.CandidateDetails" %>

<%@ Register Src="~/Sys/Common/Admissions/CandidateDetailedProfile.ascx" TagPrefix="uc1" TagName="CandidateDetailedProfile" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc1:CandidateDetailedProfile runat="server" id="profile" />
</asp:Content>
