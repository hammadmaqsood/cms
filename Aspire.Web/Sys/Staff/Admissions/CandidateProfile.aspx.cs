﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Admissions
{
	[AspirePage("1CDCC7EF-B71B-4E79-8078-8B5E57069B87", UserTypes.Staff, "~/Sys/Staff/Admissions/CandidateProfile.aspx", "Candidate Profile", true, AspireModules.Admissions)]
	public partial class CandidateProfile : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => AspireGlyphicons.pencil.GetIcon();
		public override string PageTitle => "Candidate's Profile";

		public static string GetPageUrl(int? candidateID)
		{
			return typeof(CandidateProfile).GetAspirePageAttribute().PageUrl.AttachQueryParam("CandidateID", candidateID);
		}

		public static void Redirect(int? candidateID)
		{
			Redirect(GetPageUrl(candidateID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
		}
	}
}