﻿using Aspire.BL.Core.Admissions.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Admissions
{
	[AspirePage("{C3B6FF34-DF0E-4E16-B867-37D1C9E7E9EF}", UserTypes.Staff, "~/Sys/Staff/Admissions/CandidateInterview.aspx", "Candidate Interview", true, AspireModules.Admissions)]
	public partial class CandidateInterview : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.Module,new [] {UserGroupPermission.PermissionValues.Allowed, } }
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.brand_slideshare);
		public override string PageTitle => "Candidate Interview";

		public static void Redirect(int? applicationNo, short? semesterID)
		{
			Redirect<CandidateInterview>("ApplicationNo", applicationNo, "SemesterID", semesterID);
		}

		private void Refresh()
		{
			Redirect((int?)this.ViewState["CandidateApplicationNo"], (short?)this.ViewState["SemesterID"]);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.panelInterview.Visible = false;
				this.ddlSemesterID.FillSemesters();
				this.ddlSemesterID.SetSelectedValueIfNotPostback("SemesterID");
				var applicationNo = this.Request.GetParameterValue<int>("ApplicationNo");
				if (applicationNo != null)
				{
					this.tbApplicationNo.Text = applicationNo.ToString();
					this.btnSearch_OnClick(null, null);
				}
			}
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			if (this.IsPostBack && !this.IsValid)
				return;
			this.ViewState["CandidateApplicationNo"] = null;
			this.ViewState["SemesterID"] = null;
			this.ViewState["CandidateAppliedProgramID"] = null;
			this.ViewState["Choice1AdmissionOpenProgramID"] = null;
			this.ViewState["Choice2AdmissionOpenProgramID"] = null;
			this.ViewState["Choice3AdmissionOpenProgramID"] = null;
			this.ViewState["Choice4AdmissionOpenProgramID"] = null;

			var applicationNo = this.tbApplicationNo.Text.ToInt();
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			this.panelInterview.Visible = false;
			var candidate = BL.Core.Admissions.Staff.CandidateInterviews.GetCandidateForInterview(applicationNo, semesterID, this.StaffIdentity.LoginSessionGuid);
			if (candidate == null)
			{
				this.AddNoRecordFoundAlert();
				return;
			}
			if (candidate.EnrollmentGenerated)
				this.AddWarningAlert("Enrollment has been generated.");
			else if (candidate.FeeChallanGenerated)
				this.AddWarningAlert("Fee Challan has been generated.");
			else if (candidate.DepositDate == null && !candidate.ForeignStudent)
				this.AddWarningAlert("Admission Processing Fee is not submitted.");

			this.panelInterview.Visible = true;
			this.lblApplicationNo.Text = candidate.ApplicationNo.ToString();
			this.hlCandidateName.Text = candidate.Name;
			this.hlCandidateName.NavigateUrl = CandidateDetails.GetPageUrl(candidate.CandidateID);
			this.lblCandidateFatherName.Text = candidate.FatherName;
			this.lblCandidateCNIC.Text = candidate.CNIC;
			this.lblCategory.Text = candidate.CategoryEnum.ToFullName();
			this.ViewState[nameof(Model.Entities.Candidate.CategoryEnum)] = candidate.CategoryEnum;
			this.ViewState["FeeChallanGenerated"] = candidate.FeeChallanGenerated;
			this.lblSemester.Text = candidate.SemesterID.ToSemesterString();
			this.lblShift.Text = ((Shifts)candidate.Shift).ToFullName();
			this.lblAdmissionOpenProgram1.Text = candidate.AdmissionOpenPrograms.Find(aop => aop.AdmissionOpenProgramID == candidate.Choice1AdmissionOpenProgramID).ProgramAlias;
			this.ddlProgramChoice1Shift.FillShifts().SetEnumValue((Shifts)candidate.Shift);

			if (candidate.Choice2AdmissionOpenProgramID != null)
			{
				this.lblAdmissionOpenProgram2.Text = candidate.AdmissionOpenPrograms.Find(aop => aop.AdmissionOpenProgramID == candidate.Choice2AdmissionOpenProgramID).ProgramAlias;
				this.ddlProgramChoice2Shift.FillShifts();
				this.ddlProgramChoice2Shift.Enabled = false;
				this.divChoice2.Visible = true;
			}
			else
				this.divChoice2.Visible = false;

			if (candidate.Choice3AdmissionOpenProgramID != null)
			{
				this.lblAdmissionOpenProgram3.Text = candidate.AdmissionOpenPrograms.Find(aop => aop.AdmissionOpenProgramID == candidate.Choice3AdmissionOpenProgramID).ProgramAlias;
				this.ddlProgramChoice3Shift.FillShifts();
				this.ddlProgramChoice3Shift.Enabled = false;
				this.divChoice3.Visible = true;
			}
			else
				this.divChoice3.Visible = false;

			if (candidate.Choice4AdmissionOpenProgramID != null)
			{
				this.lblAdmissionOpenProgram4.Text = candidate.AdmissionOpenPrograms.Find(aop => aop.AdmissionOpenProgramID == candidate.Choice4AdmissionOpenProgramID).ProgramAlias;
				this.ddlProgramChoice4Shift.FillShifts();
				this.ddlProgramChoice4Shift.Enabled = false;
				this.divChoice4.Visible = true;
			}
			else
				this.divChoice4.Visible = false;

			var programs = candidate.AdmissionOpenPrograms
				.Where(aop => aop.AdmissionOpenProgramID != candidate.Choice1AdmissionOpenProgramID)
				.Where(aop => aop.AdmissionOpenProgramID != candidate.Choice2AdmissionOpenProgramID)
				.Where(aop => aop.AdmissionOpenProgramID != candidate.Choice3AdmissionOpenProgramID)
				.Where(aop => aop.AdmissionOpenProgramID != candidate.Choice4AdmissionOpenProgramID)
				.Select(aop => new ListItem(aop.ProgramAlias, aop.AdmissionOpenProgramID.ToString()))
				.OrderBy(i => i.Text)
				.ToList();
			if (programs.Any())
			{
				this.ddlAnotherProgram.DataBind(programs);
				this.ddlAnotherProgramShift.FillShifts();
				this.ddlAnotherProgram.Enabled = false;
				this.ddlAnotherProgramShift.Enabled = false;
				this.divChoiceAnother.Visible = true;
			}
			else
				this.divChoiceAnother.Visible = false;

			this.rbProgramChoiceNone.Enabled = true;
			this.rbProgramChoice1.Enabled = true;
			this.rbProgramChoice2.Enabled = true;
			this.rbProgramChoice3.Enabled = true;
			this.rbProgramChoice4.Enabled = true;
			this.rbAnotherProgram.Enabled = true;

			this.rbProgramChoiceNone.Checked = false;
			this.rbProgramChoice1.Checked = false;
			this.rbProgramChoice2.Checked = false;
			this.rbProgramChoice3.Checked = false;
			this.rbProgramChoice4.Checked = false;
			this.rbAnotherProgram.Checked = false;

			if (candidate.Choice1AdmissionOpenProgramID == candidate.AdmittedAdmissionOpenProgramID)
			{
				this.rbProgramChoice1.Checked = true;
				this.ddlProgramChoice1Shift.SetEnumValue(candidate.AdmittedShiftEnum);
			}
			else if (candidate.Choice2AdmissionOpenProgramID != null && candidate.Choice2AdmissionOpenProgramID == candidate.AdmittedAdmissionOpenProgramID)
			{
				this.rbProgramChoice2.Checked = true;
				this.ddlProgramChoice2Shift.SetEnumValue(candidate.AdmittedShiftEnum);
			}
			else if (candidate.Choice3AdmissionOpenProgramID != null && candidate.Choice3AdmissionOpenProgramID == candidate.AdmittedAdmissionOpenProgramID)
			{
				this.rbProgramChoice3.Checked = true;
				this.ddlProgramChoice3Shift.SetEnumValue(candidate.AdmittedShiftEnum);
			}
			else if (candidate.Choice4AdmissionOpenProgramID != null && candidate.Choice4AdmissionOpenProgramID == candidate.AdmittedAdmissionOpenProgramID)
			{
				this.rbProgramChoice4.Checked = true;
				this.ddlProgramChoice4Shift.SetEnumValue(candidate.AdmittedShiftEnum);
			}
			else if (candidate.AdmittedAdmissionOpenProgramID != null)
			{
				this.rbAnotherProgram.Checked = true;
				this.ddlAnotherProgram.SelectedValue = candidate.AdmittedAdmissionOpenProgramID.ToString();
				this.ddlAnotherProgramShift.SetEnumValue(candidate.AdmittedShiftEnum);
			}
			else
			{
				if (candidate.InterviewDate == null)
				{
					this.rbProgramChoice1.Checked = true;
					this.ddlProgramChoice1Shift.SetEnumValue(candidate.ShiftEnum);
				}
				else
				{
					this.rbProgramChoiceNone.Checked = true;
					this.rbProgramChoiceNone_OnCheckedChanged(null, null);
				}
			}

			this.UpdateControlsAccordingToSelectedRadio();
			this.tbInterviewDate.SelectedDate = candidate.InterviewDate;

			if (candidate.InterviewDate != null)
			{
				if (candidate.AdmittedAdmissionOpenProgramID != null)
					this.tbLastDateOfFeeSubmission.SelectedDate = candidate.AdmissionFeeLastDate.Value;
			}
			else
				this.tbLastDateOfFeeSubmission.SelectedDate = null;
			this.tbRemarks.Text = candidate.InterviewRemarks;

			this.btnDelete.Visible = candidate.CanBeDeleted;
			this.btnSave.Visible = candidate.CanBeEdited;
			this.btnSaveAndGenerateFeeChallan.Visible = candidate.CanBeEdited && candidate.FeeChallanGenerated == false;

			if (candidate.CanBeEdited)
			{
				this.rbProgramChoiceNone.Enabled = !candidate.FeeChallanGenerated;
				this.rbProgramChoice1.Enabled = !candidate.EnrollmentGenerated;
				this.rbProgramChoice2.Enabled = !candidate.EnrollmentGenerated;
				this.rbProgramChoice3.Enabled = !candidate.EnrollmentGenerated;
				this.rbProgramChoice4.Enabled = !candidate.EnrollmentGenerated;
				this.rbAnotherProgram.Enabled = !candidate.EnrollmentGenerated;
				this.UpdateControlsAccordingToSelectedRadio();
				this.tbInterviewDate.ReadOnly = candidate.EnrollmentGenerated;
				this.tbLastDateOfFeeSubmission.ReadOnly = this.rbProgramChoiceNone.Checked || candidate.EnrollmentGenerated;
				this.tbRemarks.ReadOnly = candidate.EnrollmentGenerated;
				this.UpdateConfirmMessageOfSaveButton();
			}
			else
			{
				const bool @readonly = true;
				this.rbProgramChoiceNone.Enabled = !@readonly;
				this.rbProgramChoice1.Enabled = !@readonly;
				this.rbProgramChoice2.Enabled = !@readonly;
				this.rbProgramChoice3.Enabled = !@readonly;
				this.rbProgramChoice4.Enabled = !@readonly;
				this.rbAnotherProgram.Enabled = !@readonly;
				this.UpdateControlsAccordingToSelectedRadio();
				this.tbInterviewDate.ReadOnly = @readonly;
				this.tbLastDateOfFeeSubmission.ReadOnly = @readonly;
				this.tbRemarks.ReadOnly = @readonly;
			}

			this.ViewState["CandidateApplicationNo"] = candidate.ApplicationNo;
			this.ViewState["SemesterID"] = candidate.SemesterID;
			this.ViewState["CandidateAppliedProgramID"] = candidate.CandidateAppliedProgramID;
			this.ViewState["Choice1AdmissionOpenProgramID"] = candidate.Choice1AdmissionOpenProgramID;
			this.ViewState["Choice2AdmissionOpenProgramID"] = candidate.Choice2AdmissionOpenProgramID;
			this.ViewState["Choice3AdmissionOpenProgramID"] = candidate.Choice3AdmissionOpenProgramID;
			this.ViewState["Choice4AdmissionOpenProgramID"] = candidate.Choice4AdmissionOpenProgramID;
		}

		private void UpdateControlsAccordingToSelectedRadio()
		{
			this.ddlProgramChoice1Shift.Enabled = this.rbProgramChoice1.Enabled && this.rbProgramChoice1.Checked;
			this.ddlProgramChoice2Shift.Enabled = this.rbProgramChoice2.Enabled && this.rbProgramChoice2.Checked;
			this.ddlProgramChoice3Shift.Enabled = this.rbProgramChoice3.Enabled && this.rbProgramChoice3.Checked;
			this.ddlProgramChoice4Shift.Enabled = this.rbProgramChoice4.Enabled && this.rbProgramChoice4.Checked;
			this.ddlAnotherProgram.Enabled = this.rbAnotherProgram.Enabled && this.rbAnotherProgram.Checked;
			this.ddlAnotherProgramShift.Enabled = this.rbAnotherProgram.Enabled && this.rbAnotherProgram.Checked;
			this.tbLastDateOfFeeSubmission.ReadOnly = this.rbProgramChoiceNone.Checked;
			this.dtvtbLastDateOfFeeSubmission.AllowNull = this.rbProgramChoiceNone.Checked;
			this.dtvtbLastDateOfFeeSubmission.Enabled = this.rbProgramChoiceNone.Checked == false;
		}

		private void UpdateConfirmMessageOfSaveButton()
		{
			//var feeChallanGenerated = (bool)this.ViewState["FeeChallanGenerated"];
			//var categoryEnum = (Categories)this.ViewState[nameof(Model.Entities.Candidate.CategoryEnum)];

			//if (this.rbProgramChoiceNone.Checked)
			//{
			//	if (feeChallanGenerated)
			//		throw new InvalidOperationException();
			//	this.btnSave.ConfirmMessage = null;
			//}
			//else
			//{
			//	if (feeChallanGenerated)
			//		this.btnSave.ConfirmMessage = @"Fee Challan is already generated. Fee Adjustment needs to done if required. Do you want to proceed?";
			//	else
			//	{
			//		if (categoryEnum.IsNaval())
			//			this.btnSave.ConfirmMessage = @"Fee Challan will be generated by Accounts Office after verification of Naval Entitlement. Do you want to proceed?";
			//		else
			//			this.btnSave.ConfirmMessage = @"Fee Challan will be automatically generated. This process cannot be reverted until fee challan is deleted by Account office. Do you want to proceed?";
			//	}
			//}
		}

		protected void rbProgramChoiceNone_OnCheckedChanged(object sender, EventArgs e)
		{
			if (this.rbProgramChoiceNone.Enabled)
			{
				if (this.rbProgramChoiceNone.Checked)
				{
					this.tbLastDateOfFeeSubmission.SelectedDate = null;
					this.tbLastDateOfFeeSubmission.ReadOnly = true;
					this.dtvtbLastDateOfFeeSubmission.AllowNull = true;
					this.dtvtbLastDateOfFeeSubmission.Enabled = false;
					this.rfvRemarks.Enabled = true;
				}
				else
				{
					this.tbLastDateOfFeeSubmission.ReadOnly = false;
					this.dtvtbLastDateOfFeeSubmission.AllowNull = false;
					this.dtvtbLastDateOfFeeSubmission.Enabled = true;
					this.rfvRemarks.Enabled = false;
				}

				this.UpdateConfirmMessageOfSaveButton();
			}
		}

		protected void rbProgramChoice1_OnCheckedChanged(object sender, EventArgs e)
		{
			this.UpdateControlsAccordingToSelectedRadio();
		}

		protected void rbProgramChoice2_OnCheckedChanged(object sender, EventArgs e)
		{
			this.UpdateControlsAccordingToSelectedRadio();
		}

		protected void rbProgramChoice3_OnCheckedChanged(object sender, EventArgs e)
		{
			this.UpdateControlsAccordingToSelectedRadio();
		}

		protected void rbProgramChoice4_OnCheckedChanged(object sender, EventArgs e)
		{
			this.UpdateControlsAccordingToSelectedRadio();
		}

		protected void rbAnotherProgram_OnCheckedChanged(object sender, EventArgs e)
		{
			this.UpdateControlsAccordingToSelectedRadio();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (this.IsPostbackAndNotValid)
				return;
			this.StaffIdentity.Demand(StaffPermissions.Admissions.ManageInterviews, UserGroupPermission.PermissionValues.Allowed);
			var candidateAppliedProgramID = (int)this.ViewState["CandidateAppliedProgramID"];
			int? admittedAdmissionOpenProgramID;
			Shifts? admittedShiftEnum;
			var generateFeeChallan = sender == this.btnSaveAndGenerateFeeChallan;

			if (this.rbProgramChoiceNone.Checked)
			{
				admittedAdmissionOpenProgramID = null;
				admittedShiftEnum = null;
			}
			else if (this.rbProgramChoice1.Checked)
			{
				admittedAdmissionOpenProgramID = (int)this.ViewState["Choice1AdmissionOpenProgramID"];
				admittedShiftEnum = this.ddlProgramChoice1Shift.GetSelectedEnumValue<Shifts>();
			}
			else if (this.divChoice1.Visible && this.rbProgramChoice2.Checked)
			{
				admittedAdmissionOpenProgramID = (int)this.ViewState["Choice2AdmissionOpenProgramID"];
				admittedShiftEnum = this.ddlProgramChoice2Shift.GetSelectedEnumValue<Shifts>();
			}
			else if (this.divChoice2.Visible && this.rbProgramChoice3.Checked)
			{
				admittedAdmissionOpenProgramID = (int)this.ViewState["Choice3AdmissionOpenProgramID"];
				admittedShiftEnum = this.ddlProgramChoice3Shift.GetSelectedEnumValue<Shifts>();
			}
			else if (this.divChoice3.Visible && this.rbProgramChoice4.Checked)
			{
				admittedAdmissionOpenProgramID = (int)this.ViewState["Choice4AdmissionOpenProgramID"];
				admittedShiftEnum = this.ddlProgramChoice4Shift.GetSelectedEnumValue<Shifts>();
			}
			else if (this.divChoiceAnother.Visible && this.rbAnotherProgram.Checked)
			{
				admittedAdmissionOpenProgramID = this.ddlAnotherProgram.SelectedValue.ToInt();
				admittedShiftEnum = this.ddlAnotherProgramShift.GetSelectedEnumValue<Shifts>();
			}
			else
				throw new InvalidOperationException();

			var interviewDate = this.tbInterviewDate.SelectedDate.Value;
			var admissionFeeLastDate = this.tbLastDateOfFeeSubmission.ReadOnly
				? (DateTime?)null
				: this.tbLastDateOfFeeSubmission.SelectedDate.Value;
			var interviewRemarks = this.tbRemarks.Text;
			var result = BL.Core.Admissions.Staff.CandidateInterviews.UpdateCandidateInterview(candidateAppliedProgramID, admittedShiftEnum, admittedAdmissionOpenProgramID, interviewDate, admissionFeeLastDate, interviewRemarks, generateFeeChallan, this.StaffIdentity.LoginSessionGuid);
			switch (result)
			{
				case CandidateInterviews.UpdateCandidateInterviewResults.Success:
					this.AddSuccessAlert("Record has been updated.");
					break;
				case CandidateInterviews.UpdateCandidateInterviewResults.NoRecordFound:
					this.AddNoRecordFoundAlert();
					break;
				case CandidateInterviews.UpdateCandidateInterviewResults.FeeStructureNotfound:
					this.AddErrorAlert("Fee Structure not found.");
					break;
				case CandidateInterviews.UpdateCandidateInterviewResults.AdmissionProcessingFeeNotPaid:
					this.AddErrorAlert("Admission processing fee is not paid.");
					break;
				case CandidateInterviews.UpdateCandidateInterviewResults.EnrollmentHasBeenGenerated:
					this.AddErrorAlert("Cannot update because Enrollment has been generated.");
					break;
				case CandidateInterviews.UpdateCandidateInterviewResults.EntryTestRecordNotFound:
					this.AddErrorAlert("Entry Test record not found.");
					break;
				case CandidateInterviews.UpdateCandidateInterviewResults.AdmittedProgramMustBeDefinedBecauseFeeChallanHasBeenGenerated:
					this.AddErrorAlert("Cannot update because Fee Challan has been generated. If you want to reject admission then fee challan must be deleted.");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.Refresh();
		}

		protected void btnDelete_OnClick(object sender, EventArgs e)
		{
			this.StaffIdentity.Demand(StaffPermissions.Admissions.ManageInterviews, UserGroupPermission.PermissionValues.Allowed);
			var candidateAppliedProgramID = (int)this.ViewState["CandidateAppliedProgramID"];
			var result = BL.Core.Admissions.Staff.CandidateInterviews.DeleteCandidateInterviewInfo(candidateAppliedProgramID, this.StaffIdentity.LoginSessionGuid);
			switch (result)
			{
				case CandidateInterviews.DeleteCandidateInterviewInfoResults.Success:
					this.AddSuccessAlert("Interview Record has been deleted.");
					break;
				case CandidateInterviews.DeleteCandidateInterviewInfoResults.InterviewRecordNotExists:
					this.AddErrorAlert("Interview Record does not exists.");
					break;
				case CandidateInterviews.DeleteCandidateInterviewInfoResults.NoRecordFound:
					this.AddNoRecordFoundAlert();
					break;
				case CandidateInterviews.DeleteCandidateInterviewInfoResults.AdmissionProcessingFeeNotPaid:
					this.AddErrorAlert("Admission processing fee is not paid.");
					break;
				case CandidateInterviews.DeleteCandidateInterviewInfoResults.CannotDeleteFeeGenerated:
					this.AddErrorAlert("Cannot delete interview record because fee challan has been generated.");
					break;
				case CandidateInterviews.DeleteCandidateInterviewInfoResults.CannotDeleteEnrollmentGenerated:
					this.AddErrorAlert("Cannot delete interview record because enrollment has been generated.");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.Refresh();
		}
	}
}