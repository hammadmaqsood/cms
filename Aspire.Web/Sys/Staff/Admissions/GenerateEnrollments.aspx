﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="GenerateEnrollments.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.GenerateEnrollments" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Intake Semester:" AssociatedControlID="ddlIntakeSemester" />
			<aspire:AspireDropDownList runat="server" ID="ddlIntakeSemester" CausesValidation="True" ValidationGroup="Enrollment" AutoPostBack="True" OnSelectedIndexChanged="ddlIntakeSemester_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Deferred:" AssociatedControlID="ddlDeferred" />
			<aspire:AspireDropDownList runat="server" ID="ddlDeferred" CausesValidation="True" ValidationGroup="Enrollment" AutoPostBack="True" OnSelectedIndexChanged="ddlDeferred_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlAdmissionOpenProgramID" />
			<aspire:AspireDropDownList runat="server" ID="ddlAdmissionOpenProgramID" CausesValidation="True" ValidationGroup="Enrollment" AutoPostBack="True" OnSelectedIndexChanged="ddlAdmissionOpenProgramID_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShift" />
			<aspire:AspireDropDownList runat="server" ID="ddlShift" CausesValidation="True" ValidationGroup="Enrollment" AutoPostBack="True" OnSelectedIndexChanged="ddlShift_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Order by:" AssociatedControlID="ddlIntakeSemester" />
			<aspire:AspireDropDownList runat="server" ID="ddlOrderby" CausesValidation="True" ValidationGroup="Enrollment" AutoPostBack="True" OnSelectedIndexChanged="ddlOrderby_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Enrollment No. Format:" AssociatedControlID="tbEnrollmentNoFormat" />
			<aspire:AspireTextBox runat="server" ID="tbEnrollmentNoFormat" ReadOnly="True" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Last Enrollment No.:" AssociatedControlID="tbLastEnrollmentNo" />
			<aspire:AspireTextBox runat="server" ID="tbLastEnrollmentNo" ReadOnly="True" />
			<asp:HiddenField runat="server" ID="hfLastRollNo" />
		</div>
	</div>
	<p></p>
	<div class="table-responsive">
		<aspire:AspireGridView runat="server" ID="gvEnrollments" AutoGenerateColumns="False" AllowSorting="False">
			<Columns>
				<asp:TemplateField>
					<HeaderTemplate>
						<asp:CheckBox runat="server" ID="cbSelectAll" ClientIDMode="Static" Checked="True" />
					</HeaderTemplate>
					<ItemTemplate>
						<asp:CheckBox runat="server" ID="cb" Checked="True" />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField DataField="Enrollment" HeaderText="Enrollment" SortExpression="Enrollment" />
				<asp:BoundField DataField="ApplicationNo" HeaderText="Application No." SortExpression="ApplicationNo" />
				<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
				<asp:BoundField DataField="FatherName" HeaderText="Father Name" />
				<asp:BoundField DataField="GenderFullName" HeaderText="Gender" />
				<asp:BoundField HeaderText="Enrollment Preview" />
				<asp:BoundField DataField="Remarks" HeaderText="Remarks" />
			</Columns>
		</aspire:AspireGridView>
	</div>
	<p class="text-center">
		<aspire:AspireButton runat="server" ButtonType="Primary" Text="Generate" ConfirmMessage="The visible enrollment(s) may differ when generated. Are you sure you want to generate enrollment(s)?" ID="btnGenerate" OnClick="btnGenerate_OnClick" />
		<aspire:AspireHyperLinkButton runat="server" ButtonType="Default" Text="View Report" ID="btnViewReport" Target="_blank" />
	</p>
	<script type="text/javascript">
		var gv = $("#<%=this.gvEnrollments.ClientID%>");
		function regenerateEnrollments() {
			var tbody = $("tbody", gv);
			var rows = $("tr", tbody);
			var enrollmentFormat = $("#<%=this.tbEnrollmentNoFormat.ClientID%>").val();
			var maxRollNo = $("#<%=this.hfLastRollNo.ClientID%>").val();
			rows.each(function (inex, row) {
				var cb = $("input", row);
				var cell = $("td:nth-child(6)", row);
				if (!cb.is(":checked"))
					cell.text("");
				else {
					var enrollment = "000" + (++maxRollNo);
					cell.text(enrollmentFormat + enrollment.slice(-3));
				}
			});
		}

		$(function () {
			var cbSelectAll = $("th #cbSelectAll", gv);
			cbSelectAll.click(function () {
				$("td input", gv).prop("checked", $(this).is(":checked"));
				regenerateEnrollments();
			});

			function setcbSelectAll() {
				cbSelectAll.prop("checked", true);
				$("td input", gv).each(function () {
					if (!$(this).is(":checked")) {
						cbSelectAll.prop("checked", false);
						return false;
					}
				});
			}
			$("td input", gv).click(function () {
				setcbSelectAll();
				regenerateEnrollments();
			});
			regenerateEnrollments();
		});
	</script>
</asp:Content>
