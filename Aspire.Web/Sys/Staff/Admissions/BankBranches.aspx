﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="BankBranches.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.BankBranches" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<asp:Panel runat="server" class="form-inline" DefaultButton="btnSearch">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Bank:" AssociatedControlID="ddlBankID" />
					<aspire:AspireDropDownList runat="server" ID="ddlBankID" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="ddlBankID_SelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:Label runat="server" class="control-label" Text="Search Branch" AssociatedControlID="tbSearch" />
					<div class="input-group">
						<aspire:AspireTextBox runat="server" ID="tbSearch" ValidationGroup="Search" TextTransform="UpperCase" />
						<div class="input-group-btn">
							<aspire:AspireButton ButtonType="Default" Glyphicon="search" runat="server" ID="btnSearch" ValidationGroup="Search" OnClick="btnSearch_Click" />
						</div>
					</div>
				</div>
			</asp:Panel>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvBankBranches" AutoGenerateColumns="False" AllowSorting="True" OnPageIndexChanging="gvBankBranches_PageIndexChanging" OnPageSizeChanging="gvBankBranches_PageSizeChanging" OnSorting="gvBankBranches_Sorting">
		<Columns>
			<asp:BoundField DataField="BankName" HeaderText="Bank Name" SortExpression="BankName" />
			<asp:BoundField DataField="BranchCode" HeaderText="Branch Code" SortExpression="BranchCode" />
			<asp:BoundField DataField="BranchName" HeaderText="Branch Name" SortExpression="BranchName" />
			<asp:BoundField DataField="BranchAddress" HeaderText="Branch Address" SortExpression="BranchAddress" />
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
