﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Staff.Admissions
{
	[AspirePage("47528871-CF26-416D-BA3D-435DE5424F9A", UserTypes.Staff, "~/Sys/Staff/Admissions/CandidateStatus.aspx", "Candidate Status", true, AspireModules.Admissions)]
	public partial class CandidateStatus : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.Module,new [] {UserGroupPermission.PermissionValues.Allowed, } }
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.brand_slideshare);
		public override string PageTitle => "Candidate Status";

		public static void Redirect(int? applicationNo, short? semesterID, bool search = true)
		{
			Redirect<CandidateStatus>("ApplicationNo", applicationNo, "SemesterID", semesterID, "Search", search);
		}

		private void Refresh(bool search = true)
		{
			Redirect((int?)this.ViewState["ApplicationNo"], (short?)this.ViewState["SemesterID"], search);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.panelCandidate.Visible = false;
				this.ddlSemesterID.FillSemesters();
				this.ddlSemesterID.SetSelectedValueIfNotPostback("SemesterID");
				this.ddlDeferredToShift.FillShifts(CommonListItems.Select);
				var applicationNo = this.Request.GetParameterValue<int>("ApplicationNo");
				if (applicationNo != null)
				{
					this.tbApplicationNo.Text = applicationNo.ToString();
					if (this.Request.GetParameterValue<bool>("Search") == true)
						this.btnSearch_OnClick(null, null);
				}
			}
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			if (this.IsPostbackAndNotValid)
				return;
			var applicationNo = this.tbApplicationNo.Text.ToInt();
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			this.panelCandidate.Visible = false;
			var candidateAppliedProgram = BL.Core.Admissions.Staff.CandidateStatus.GetCandidateAppliedProgram(applicationNo, semesterID, this.StaffIdentity.LoginSessionGuid);
			if (candidateAppliedProgram == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect(applicationNo, semesterID, false);
				return;
			}
			this.ViewState["CandidateAppliedProgramID"] = candidateAppliedProgram.CandidateAppliedProgramID;
			this.ViewState["ApplicationNo"] = candidateAppliedProgram.ApplicationNo;
			this.ViewState["SemesterID"] = candidateAppliedProgram.SemesterID;
			this.panelCandidate.Visible = true;
			this.lblApplicationNo.Text = candidateAppliedProgram.ApplicationNo.ToString();
			this.hlCandidateName.Text = candidateAppliedProgram.Name;
			this.hlCandidateName.NavigateUrl = CandidateDetails.GetPageUrl(candidateAppliedProgram.CandidateID);
			this.lblCandidateFatherName.Text = candidateAppliedProgram.FatherName;
			this.lblSemester.Text = candidateAppliedProgram.SemesterID.ToSemesterString();
			this.lblProgramApplied.Text = candidateAppliedProgram.Choice1ProgramAlias;
			this.lblShiftApplied.Text = candidateAppliedProgram.ShiftEnum.ToFullName();
			this.lblProgramAdmitted.Text = candidateAppliedProgram.AdmittedProgramAlias.ToNAIfNullOrEmpty();
			this.lblShiftAdmitted.Text = (candidateAppliedProgram.AdmittedShiftEnum?.ToFullName()).ToNAIfNullOrEmpty();
			var statuses = Enum.GetValues(typeof(CandidateAppliedProgram.Statuses))
				.Cast<CandidateAppliedProgram.Statuses>()
				.Select(s => new System.Web.UI.WebControls.ListItem
				{
					Text = s.ToFullName() ?? "",
					Value = ((byte)s).ToString(),
				});
			this.ddlStatus.DataBind(statuses.ToArray());
			this.ddlStatus.SetEnumValue(candidateAppliedProgram.StatusEnum ?? CandidateAppliedProgram.Statuses.None);
			this.lblStatusDate.Text = (candidateAppliedProgram.StatusDate?.ToShortDateString()).ToNAIfNullOrEmpty();
			this.lblEnrollmentGenerated.Text = candidateAppliedProgram.EnrollmentGenerated.ToYesNo();
			this.lblAdmissionFeePaid.Text = candidateAppliedProgram.FeePaid.ToYesNo();
			this.dtpDeferredDate.SelectedDate = candidateAppliedProgram.DeferredDate;
			if (candidateAppliedProgram.DeferredDate == null)
			{
				this.ddlDeferredToSemester.Items.Clear();
				this.ddlDeferredToSemester.Enabled = false;
				this.ddlDeferredToProgram.Items.Clear();
				this.ddlDeferredToProgram.Enabled = false;
				this.ddlDeferredToShift.Items.Clear();
				this.ddlDeferredToShift.Enabled = false;
			}
			else
			{
				var semesters = BL.Core.Common.Lists.GetSemestersList(null).Where(s => s > candidateAppliedProgram.SemesterID);
				this.ddlDeferredToSemester.FillSemesters(semesters, CommonListItems.Select);
				if (candidateAppliedProgram.DeferredToSemesterID != null)
				{
					this.ddlDeferredToSemester.SelectedValue = candidateAppliedProgram.DeferredToSemesterID.ToString();
					this.ddlDeferredToSemester_OnSelectedIndexChanged(null, null);
					this.ddlDeferredToProgram.SelectedValue = candidateAppliedProgram.DeferredToAdmittedAdmissionOpenProgramID.ToString();
					this.ddlDeferredToShift.SelectedValue = candidateAppliedProgram.DeferredToShift.ToString();
				}
				else
				{
					this.ddlDeferredToSemester.ClearSelection();
					this.ddlDeferredToSemester_OnSelectedIndexChanged(null, null);
				}
			}
			if (candidateAppliedProgram.EnrollmentGenerated)
				this.AddWarningAlert($"Enrollment \"{candidateAppliedProgram.Enrollment}\" has been generated.");

			this.btnSave.Enabled = candidateAppliedProgram.EnrollmentGenerated == false;
		}

		protected void ddlDeferredToSemester_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var deferredToSemesterID = this.ddlDeferredToSemester.SelectedValue.ToNullableShort();
			if (deferredToSemesterID != null)
			{
				var aops = Aspire.BL.Core.Common.Lists.GetAdmissionOpenPrograms(deferredToSemesterID.Value, this.StaffIdentity.InstituteID);
				this.ddlDeferredToProgram.DataBind(aops, CommonListItems.Select);
				this.ddlDeferredToProgram.Enabled = true;
				this.ddlDeferredToShift.FillShifts(CommonListItems.Select);
				this.ddlDeferredToShift.Enabled = true;
			}
			else
			{
				this.ddlDeferredToProgram.DataBind(CommonListItems.Select);
				this.ddlDeferredToProgram.Enabled = false;
				this.ddlDeferredToShift.ClearSelection();
				this.ddlDeferredToShift.Enabled = false;
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var candidateAppliedProgramID = (int)this.ViewState["CandidateAppliedProgramID"];
			var statusEnum = this.ddlStatus.GetSelectedEnumValue<CandidateAppliedProgram.Statuses>();
			var deferrredDate = this.dtpDeferredDate.SelectedDate;
			var deferredToAdmissionOpenProgramID = this.ddlDeferredToProgram.SelectedValue.ToNullableInt();
			var deferredToShiftEnum = this.ddlDeferredToShift.GetSelectedEnumValue<Shifts>(null);
			if (deferredToShiftEnum == null && deferredToAdmissionOpenProgramID != null)
			{
				this.AddErrorAlert("Provide Shift.");
				this.ddlDeferredToShift.Focus();
				return;
			}

			var status = Aspire.BL.Core.Admissions.Staff.CandidateStatus.ChangeCandidateAppliedProgramStatus(candidateAppliedProgramID, statusEnum, deferrredDate, deferredToAdmissionOpenProgramID, deferredToShiftEnum, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Admissions.Staff.CandidateStatus.ChangeCandidateAppliedProgramStatusStatuses.NoRecordFound:
					this.Refresh();
					break;
				case BL.Core.Admissions.Staff.CandidateStatus.ChangeCandidateAppliedProgramStatusStatuses.InterviewNotCleared:
					this.AddErrorAlert("Interview record doesn't exists or interview is not cleared.");
					break;
				case BL.Core.Admissions.Staff.CandidateStatus.ChangeCandidateAppliedProgramStatusStatuses.Success:
					this.AddSuccessAlert("Record has been saved.");
					this.Refresh();
					break;
				case BL.Core.Admissions.Staff.CandidateStatus.ChangeCandidateAppliedProgramStatusStatuses.FeeMustBePaidWhenDeferring:
					this.AddErrorAlert("Fee must be paid when deferring admission.");
					break;
				case BL.Core.Admissions.Staff.CandidateStatus.ChangeCandidateAppliedProgramStatusStatuses.FeeMustBePaidWhenMakeItRefunded:
					this.AddErrorAlert("Fee must be paid when status is refunded.");
					break;
				case BL.Core.Admissions.Staff.CandidateStatus.ChangeCandidateAppliedProgramStatusStatuses.RefundedCaseCannotBeDeferred:
					this.AddErrorAlert("Refunded admission cannot be deferred.");
					break;
				case BL.Core.Admissions.Staff.CandidateStatus.ChangeCandidateAppliedProgramStatusStatuses.EnrollmentHasBeenGenerated:
					this.AddErrorAlert("Record can not be updated because Enrollment has been generated.");
					this.Refresh();
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}
	}
}