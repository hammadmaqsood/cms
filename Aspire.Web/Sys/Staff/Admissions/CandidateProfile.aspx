﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CandidateProfile.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.CandidateProfile" %>

<%@ Register Src="~/Sys/Common/Admissions/CandidateProfile.ascx" TagPrefix="uc1" TagName="CandidateProfile" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc1:CandidateProfile runat="server" ID="candidateProfile" />
</asp:Content>
