﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CandidateStatus.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.CandidateStatus" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<asp:Panel runat="server" class="form-inline" DefaultButton="btnSearch">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" CausesValidation="False" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Application No.:" AssociatedControlID="tbApplicationNo" />
					<div class="input-group">
						<aspire:AspireTextBox runat="server" ID="tbApplicationNo" ValidationGroup="search" />
						<div class="input-group-btn">
							<aspire:AspireButton runat="server" Glyphicon="search" ButtonType="Primary" ID="btnSearch" OnClick="btnSearch_OnClick" ValidationGroup="search" />
						</div>
					</div>
					<aspire:AspireInt32Validator runat="server" AllowNull="False" ValidationGroup="search" ControlToValidate="tbApplicationNo" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid Number" />
				</div>
				<div class="form-group">
					<aspire:AspireHyperLink runat="server" NavigateUrl="CandidateStatus.aspx" Text="Cancel" />
				</div>
			</asp:Panel>
		</div>
	</div>
	<asp:Panel ID="panelCandidate" runat="server">
		<p></p>
		<div class="form-horizontal">
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Application No.:" AssociatedControlID="lblApplicationNo" />
				<div class="col-md-6 form-control-static">
					<aspire:Label runat="server" ID="lblApplicationNo" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Semester:" AssociatedControlID="lblSemester" />
				<div class="col-md-6 form-control-static">
					<aspire:Label runat="server" ID="lblSemester" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Name:" AssociatedControlID="hlCandidateName" />
				<div class="col-md-6 form-control-static">
					<aspire:AspireHyperLink runat="server" ID="hlCandidateName" Target="_blank" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Father Name:" AssociatedControlID="lblCandidateFatherName" />
				<div class="col-md-6 form-control-static">
					<aspire:Label runat="server" ID="lblCandidateFatherName" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Program (Applied):" AssociatedControlID="lblProgramApplied" />
				<div class="col-md-6 form-control-static">
					<aspire:Label runat="server" ID="lblProgramApplied" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Shift (Applied):" AssociatedControlID="lblShiftApplied" />
				<div class="col-md-6 form-control-static">
					<aspire:Label runat="server" ID="lblShiftApplied" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Program (Admitted):" AssociatedControlID="lblProgramApplied" />
				<div class="col-md-6 form-control-static">
					<aspire:Label runat="server" ID="lblProgramAdmitted" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Shift (Admitted):" AssociatedControlID="lblShiftAdmitted" />
				<div class="col-md-6 form-control-static">
					<aspire:Label runat="server" ID="lblShiftAdmitted" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Enrollment Generated:" AssociatedControlID="lblEnrollmentGenerated" />
				<div class="col-md-6 form-control-static">
					<aspire:Label runat="server" ID="lblEnrollmentGenerated" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Admission Fee Paid:" AssociatedControlID="lblAdmissionFeePaid" />
				<div class="col-md-6 form-control-static">
					<aspire:Label runat="server" ID="lblAdmissionFeePaid" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Status:" AssociatedControlID="ddlStatus" />
				<div class="col-md-6">
					<aspire:AspireDropDownList runat="server" ID="ddlStatus" ValidationGroup="Status" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Status Date:" AssociatedControlID="lblStatusDate" />
				<div class="col-md-6 form-control-static">
					<aspire:Label runat="server" ID="lblStatusDate" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Deferred Date:" AssociatedControlID="dtpDeferredDate" />
				<div class="col-md-6">
					<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpDeferredDate" DisplayMode="ShortDate" ValidationGroup="Status" />
					<aspire:AspireDateTimeValidator ID="dtvtbDeferredDate" runat="server" Format="ShortDate" ControlToValidate="dtpDeferredDate" InvalidDataErrorMessage="Invalid Date." AllowNull="True" RequiredErrorMessage="This field is required." ValidationGroup="Status" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Deferred To Semester:" AssociatedControlID="ddlDeferredToSemester" />
				<div class="col-md-6">
					<aspire:AspireDropDownList runat="server" ID="ddlDeferredToSemester" ValidationGroup="Status" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="ddlDeferredToSemester_OnSelectedIndexChanged" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Deferred To Program:" AssociatedControlID="ddlDeferredToProgram" />
				<div class="col-md-6">
					<aspire:AspireDropDownList runat="server" ID="ddlDeferredToProgram" ValidationGroup="Status" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Deferred To Shift:" AssociatedControlID="ddlDeferredToShift" />
				<div class="col-md-6">
					<aspire:AspireDropDownList runat="server" ID="ddlDeferredToShift" ValidationGroup="Status" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-md-offset-2">
					<aspire:AspireButton runat="server" ID="btnSave" ButtonType="Primary" Text="Save" ValidationGroup="Status" OnClick="btnSave_OnClick" />
				</div>
			</div>
		</div>
	</asp:Panel>
</asp:Content>
