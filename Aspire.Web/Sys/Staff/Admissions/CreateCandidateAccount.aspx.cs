﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Logins.Candidate;
using Login = System.Web.UI.WebControls.Login;

namespace Aspire.Web.Sys.Staff.Admissions
{
	[AspirePage("8DE2A0E8-47A4-401C-8032-4979AE844AA2", UserTypes.Staff, "~/Sys/Staff/Admissions/CreateCandidateAccount.aspx", "Create Candidate Account", true, AspireModules.Admissions)]
	public partial class CreateCandidateAccount : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.CreateCandidateAccount, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};
		public override PageIcon PageIcon => AspireGlyphicons.edit.GetIcon();
		public override string PageTitle => "Create Candidate Account";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var semesterIDs = Aspire.BL.Core.Admissions.Candidate.Common.GetAdmissionOpenSemesters();
				this.rblSemesterID.DataBindSemesters(semesterIDs);
				if (this.rblSemesterID.Items.Count == 0)
				{
					this.AddErrorAlert("Admissions are closed.");
					Redirect<Dashboard>();
					return;
				}
				this.rblSemesterID.SelectedIndex = 0;
				this.rblSemesterID.Visible = this.rblSemesterID.Items.Count > 1;
				this.tbSemester.Visible = !this.rblSemesterID.Visible;
				if (this.tbSemester.Visible)
					this.tbSemester.Text = this.rblSemesterID.SelectedItem.Text;
			}
		}

		protected void btnRegister_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;

			var result = Aspire.BL.Core.Admissions.Candidate.Registration.RegisterCandidate(this.tbEmail.Text, this.tbName.Text, this.rblSemesterID.SelectedValue.ToShort());
			switch (result.Status)
			{
				case BL.Core.Admissions.Candidate.Registration.RegisterCandidateResult.Statuses.AdmissionsNotOpen:
					this.AddErrorAlert("Admissions are closed.");
					Redirect<CreateCandidateAccount>();
					return;
				case BL.Core.Admissions.Candidate.Registration.RegisterCandidateResult.Statuses.EmailAlreadyRegistered:
					this.AddErrorAlert($"The email address {this.tbEmail.Text} is already registered.");
					break;
				case BL.Core.Admissions.Candidate.Registration.RegisterCandidateResult.Statuses.AlreadyExists:
					this.SendRegistrationVerificationEmail(result.CandidateTemp, true);
					return;
				case BL.Core.Admissions.Candidate.Registration.RegisterCandidateResult.Statuses.Success:
					this.SendRegistrationVerificationEmail(result.CandidateTemp, false);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private void SendRegistrationVerificationEmail(CandidateTemp candidateTemp, bool alreadySent)
		{
			if (candidateTemp == null)
				throw new ArgumentNullException(nameof(candidateTemp));
			var template = System.IO.File.ReadAllText(Server.MapPath("~/EmailTemplates/Admissions/CandidateRegistrationVerificationEmail.html"));
			var confirmationLink = ConfirmRegistration.GetPageAbsoluteUrl(candidateTemp.VerificationCode.ToString());
			template = template.Replace("{LinkHtmlAttributeEncoded}", confirmationLink.HtmlAttributeEncode());
			template = template.Replace("{LinkHtmlEncoded}", confirmationLink.HtmlEncode());
			template = template.Replace("{NameHtmlEncoded}", candidateTemp.Name.HtmlEncode());
			template = template.Replace("{SemesterHtmlEncoded}", candidateTemp.SemesterID.ToSemesterString().HtmlEncode());
			template = template.Replace("{ConfirmationLinkExpiryDate}", candidateTemp.CodeExpiryDate.ToString("F").HtmlEncode());

			var subject = "Registration Verification";
			EmailHelper.SendInBackground(candidateTemp.Email, subject, template, true);
			if (alreadySent)
				this.AddSuccessAlert("We have sent you confirmation email again. Please check you email inbox. You will soon receive the confirmation email.");
			else
				this.AddSuccessAlert("Please check you email inbox. You will soon receive the confirmation email.");
			Redirect<Login>();
		}
	}
}