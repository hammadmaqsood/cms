﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="StudentTransfer.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.StudentTransfer" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteIDSearch" />
			<aspire:AspireDropDownList runat="server" ID="ddlInstituteIDSearch" ValidationGroup="Search" />
			<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlInstituteIDSearch" ValidationGroup="Search" ErrorMessage="This field is required." />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollmentSearch" />
			<aspire:EnrollmentTextBox runat="server" ValidationGroup="Search" AllowNull="False" ID="tbEnrollmentSearch" OnSearch="tbEnrollmentSearch_OnSearch" />
		</div>
	</div>
	<asp:Panel runat="server" ID="panelStudentInfo">
		<table class="table table-bordered table-condensed tableCol4">
			<tr>
				<th>Enrollment</th>
				<td>
					<aspire:Label runat="server" ID="lblEnrollment" />
				</td>
				<th>Registration No.</th>
				<td>
					<aspire:Label runat="server" ID="lblRegistrationNo" />
				</td>
			</tr>
			<tr>
				<th>Name</th>
				<td>
					<aspire:Label runat="server" ID="lblName" />
				</td>
				<th>Institute</th>
				<td>
					<aspire:Label runat="server" ID="lblInstituteAlias" />
				</td>
			</tr>
			<tr>
				<th>Program</th>
				<td>
					<aspire:Label runat="server" ID="lblProgram" />
				</td>
				<th>Intake Semester</th>
				<td>
					<aspire:Label runat="server" ID="lblIntakeSemesterID" />
				</td>
			</tr>
			<tr>
				<th>Status</th>
				<td>
					<aspire:Label runat="server" ID="lblStatus" />
				</td>
				<th>Status Date</th>
				<td>
					<aspire:Label runat="server" ID="lblStatusDate" />
				</td>
			</tr>
		</table>
		<div>
			<aspire:AspireButton runat="server" ID="btnAddStudentTransfer" ButtonType="Success" Text="Add Transfer Record" Glyphicon="plus" CausesValidation="False" OnClick="btnAddStudentTransfer_OnClick" />
			<p></p>
		</div>
		<asp:Repeater runat="server" ID="repeaterTransferredStudents" ItemType="Aspire.BL.Core.Admissions.Staff.StudentTransfer.Student.TransferredStudent">
			<HeaderTemplate>
				<table class="table table-hover table-bordered table-responsive table-striped AspireGridView">
					<thead>
						<tr>
							<th rowspan="2">Transfer Date</th>
							<th rowspan="2">Transfer Semester</th>
							<th colspan="3">From</th>
							<th colspan="3">To</th>
							<th rowspan="2">Remarks</th>
							<th rowspan="2">Actions</th>
						</tr>
						<tr>
							<th>Enrollment</th>
							<th>Program</th>
							<th>Institute</th>
							<th>Enrollment</th>
							<th>Program</th>
							<th>Institute</th>
						</tr>
					</thead>
					<tbody>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><%#: Item.TransferDate.ToString("d") %></td>
					<td><%#: Item.TransferSemesterID.ToSemesterString() %></td>
					<td><%#: Item.FromEnrollment %></td>
					<td><%#: Item.FromProgramShortName %></td>
					<td><%#: Item.FromInstituteAlias %></td>
					<td><%#: Item.ToEnrollment %></td>
					<td><%#: Item.ToProgramShortName %></td>
					<td><%#: Item.ToInstituteAlias %></td>
					<td><%#: Item.Remarks %></td>
					<td>
						<aspire:AspireHyperLinkButton ButtonType="OnlyIcon" runat="server" Glyphicon="edit" NavigateUrl="<%# Aspire.Web.Sys.Staff.Admissions.StudentTransferMappedCourses.GetPageUrl(Item.TransferredStudentID) %>" />
					</td>
				</tr>
			</ItemTemplate>
			<FooterTemplate>
				</tbody>
				</table>
			</FooterTemplate>
		</asp:Repeater>

		<aspire:AspireModal runat="server" ID="modalTransfer" ModalSize="Large" HeaderText="Transfer">
			<BodyTemplate>
				<asp:UpdatePanel runat="server" ID="updatePanelTransfer" UpdateMode="Conditional">
					<ContentTemplate>
						<aspire:AspireAlert ID="alertModalTransfer" runat="server" />
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="Campus:" AssociatedControlID="tbInstituteAlias" />
									<aspire:AspireTextBox runat="server" ID="tbInstituteAlias" ValidationGroup="Transfer" ReadOnly="True" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="Intake Semester:" AssociatedControlID="ddlIntakeSemesterID" />
									<aspire:AspireDropDownList runat="server" ID="ddlIntakeSemesterID" AutoPostBack="True" CausesValidation="False" ValidationGroup="Transfer" OnSelectedIndexChanged="ddlIntakeSemesterID_OnSelectedIndexChanged" />
									<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Transfer" ControlToValidate="ddlIntakeSemesterID" ErrorMessage="This field is required." />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlAdmissionOpenProgramID" />
									<aspire:AspireDropDownList runat="server" ID="ddlAdmissionOpenProgramID" AutoPostBack="True" CausesValidation="False" ValidationGroup="Transfer" OnSelectedIndexChanged="ddlAdmissionOpenProgramID_OnSelectedIndexChanged" />
									<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Transfer" ControlToValidate="ddlAdmissionOpenProgramID" ErrorMessage="This field is required." />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShift" />
									<aspire:AspireDropDownList runat="server" ID="ddlShift" AutoPostBack="True" CausesValidation="False" ValidationGroup="Transfer" OnSelectedIndexChanged="ddlShift_OnSelectedIndexChanged" />
									<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Transfer" ControlToValidate="ddlShift" ErrorMessage="This field is required." />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="Transfer Semester:" AssociatedControlID="ddlTransferSemesterID" />
									<aspire:AspireDropDownList runat="server" ID="ddlTransferSemesterID" AutoPostBack="True" CausesValidation="False" ValidationGroup="Transfer" OnSelectedIndexChanged="ddlTransferSemesterID_OnSelectedIndexChanged" />
									<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Transfer" ControlToValidate="ddlTransferSemesterID" ErrorMessage="This field is required." />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="New Enrollment:" AssociatedControlID="tbNewEnrollment" />
									<aspire:AspireTextBox runat="server" ID="tbNewEnrollment" ReadOnly="True" MaxLength="1000" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" />
									<aspire:AspireTextBox TextMode="MultiLine" MaxLength="1000" runat="server" ID="tbRemarks" ValidationGroup="Transfer" Rows="3" />
								</div>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</BodyTemplate>
			<FooterTemplate>
				<asp:UpdatePanel runat="server" UpdateMode="Always">
					<ContentTemplate>
						<aspire:AspireButton runat="server" ID="btnTransfer" ButtonType="Primary" ValidationGroup="Transfer" OnClick="btnTransfer_OnClick" Text="Transfer" />
						<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
					</ContentTemplate>
				</asp:UpdatePanel>
			</FooterTemplate>
		</aspire:AspireModal>
	</asp:Panel>
</asp:Content>
<%--		<aspire:AspireModal runat="server" ID="modalCourseMappings" HeaderText="Edit Course Mappings" ModalSize="Large">
			<BodyTemplate>
				<asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updatePanelCreditsTransferred">
					<ContentTemplate>
						<aspire:AspireAlert runat="server" ID="modalAlert" />
						<table class="table table-bordered table-hover table-striped" id="<%=this.repeaterCourseMappings.ClientID %>">
							<thead>
								<tr>
									<th>From</th>
									<th>To</th>
								</tr>
								<tr>
									<th>
										<aspire:Label runat="server" ID="lblFromProgram" />
										@
												<aspire:Label runat="server" ID="lblFromCampus" />
									</th>
									<th>
										<aspire:Label runat="server" ID="lblToProgram" />
										@
												<aspire:Label runat="server" ID="lblToCampus" />
									</th>
								</tr>
								<tr>
									<th class="col-md-6">Code - Title</th>
									<th class="col-md-6">Code - Title</th>
								</tr>
							</thead>
							<tbody>
								<asp:Repeater runat="server" ID="repeaterCourseMappings">
									<ItemTemplate>
										<tr>
											<td>
												<aspire:HiddenField runat="server" Visible="False" Value='<%# Eval("RegisteredCourseID") %>' ID="hfRegisteredCourseID" />
												<%# Eval("CourseCode") %> - <%# Eval("Title") %>
											</td>
											<td>
												<aspire:AspireDropDownList runat="server" ID="ddlCourseID" />
											</td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</tbody>
						</table>
					</ContentTemplate>
				</asp:UpdatePanel>
			</BodyTemplate>
			<FooterTemplate>
				<asp:UpdatePanel runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<aspire:AspireButton runat="server" ID="btnSaveMappings" OnClick="btnSaveMappings_OnClick" Text="Save" ButtonType="Primary" ValidationGroup="Mappings" />
						<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
					</ContentTemplate>
				</asp:UpdatePanel>
			</FooterTemplate>
		</aspire:AspireModal>
	</asp:Panel>
	<script type="text/javascript">
		$(function () {
			var init = function () {
				var table = $("#<%=this.repeaterCourseMappings.ClientID%>");
				$("select[id*='ddlCourseID']", table).select2({
					theme: "bootstrap",
					width: "style",
					dropdownParent: $("#<%=this.modalCourseMappings.ClientID%>")
				});
			}
			init();
			Sys.Application.add_load(init);
		});
	</script>--%>
