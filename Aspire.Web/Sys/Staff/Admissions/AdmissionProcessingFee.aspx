﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="AdmissionProcessingFee.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.AdmissionProcessingFee" %>

<%@ Register Src="~/Sys/Staff/Common/FeePaymentUserControl.ascx" TagPrefix="uc1" TagName="FeePaymentUserControl" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc1:FeePaymentUserControl runat="server" ID="feePaymentUserControl" />
</asp:Content>
