﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Staff.Admissions.Reports
{
	[AspirePage("C0E901D9-91FA-4326-BD68-46037B776058", UserTypes.Staff, "~/Sys/Staff/Admissions/Reports/ETSScorerNominalReport.aspx", "ETS Scorer Nominal", true, AspireModules.Admissions)]
	public partial class ETSScorerNominalReport : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "ETS Scorer Nominal";
		public string ReportName => "ETS Scorer Nominal";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlETSScoreSemesterID.FillSemesters();
				this.ddlETSScoreSemesterID_SelectedIndexChanged(null, null);
			}
		}
		protected void ddlETSScoreSemesterID_SelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlETSScoreSemesterID.SelectedValue.ToShort();
			var admissionOpenPrograms = Aspire.BL.Core.Common.Lists.GetAdmissionOpenPrograms(semesterID, this.StaffIdentity.InstituteID);
			this.ddlETSScoreAdmissionOpenProgramID.DataBind(admissionOpenPrograms, CommonListItems.All);
			ddlETSScoreAdmissionOpenProgramID_SelectedIndexChanged(null, null);
		}

		protected void ddlETSScoreAdmissionOpenProgramID_SelectedIndexChanged(object sender, EventArgs e)
		{
			var iReportViewer = (IReportViewer)this.Page;
			var reportView = (ReportView)this.Master;
			iReportViewer.RenderReport(reportView.ReportViewer);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/Admissions/Staff/ETSNominal.rdlc".MapPath();
			var semesterID = this.ddlETSScoreSemesterID.SelectedValue.ToShort();
			var admissionOpenProgramID = this.ddlETSScoreAdmissionOpenProgramID.SelectedValue.ToNullableInt();
			var dataSource = BL.Core.Admissions.Staff.Reports.ETSNominals.GetETSNominal(semesterID, admissionOpenProgramID, this.StaffIdentity.LoginSessionGuid);

			var parameters = new ReportParameter[3];
			parameters[0] = new ReportParameter("ReportTitle", $"ETS Nominal Report [{semesterID.ToSemesterString()}]");
			parameters[1] = new ReportParameter("Institute", $"Bahria University {dataSource.Select(c => c.Institute).FirstOrDefault()}");
			parameters[2] = new ReportParameter("ReportLogoPath", "~/Images/ReportLogo.gif".ToAbsoluteUrl());

			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSource", dataSource));
			reportViewer.LocalReport.EnableExternalImages = true;
			reportViewer.LocalReport.SetParameters(parameters);
		}
	}
}