﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Staff.Admissions.Reports
{
	[AspirePage("A5C4CADE-25C9-44D1-AD6D-D628363C7E3D", UserTypes.Staff, "~/Sys/Staff/Admissions/Reports/MeritListReport.aspx", "Merit List", true, AspireModules.Admissions)]
	public partial class MeritListReport : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Merit List Report";
		public string ReportName => "Merit List Report";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterID.FillSemesters();
				var departments = Aspire.BL.Core.Common.Lists.GetDepartmentsList(this.StaffIdentity.InstituteID, null);
				this.ddlDepartmentID.DataBind(departments, CommonListItems.All);
				this.ddlDisplayOnlyPassed.FillYesNo().SetSelectedValueYes();
				this.ddlDisplayMobile.FillYesNo().SetSelectedValueNo();
				this.ddlDepartmentID_OnSelectedIndexChanged(null, null);
			}
			this.SetMaximumScriptTimeout();
		}

		protected void ddlDepartmentID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var programs = Aspire.BL.Core.Common.Lists.GetPrograms(this.StaffIdentity.InstituteID, departmentID, true);
			this.ddlProgramID.DataBind(programs, CommonListItems.All);
		}

		private bool _execute;
		protected void btnDisplayReport_Click(object sender, EventArgs e)
		{
			this._execute = true;
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView.ReportViewer);
			this._execute = false;
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this._execute == false)
				return;
			reportViewer.LocalReport.DataSources.Clear();
			var displayMobile = this.ddlDisplayMobile.SelectedValue.ToBoolean();
			var reportFileName = displayMobile
				? "~/Reports/Admissions/Staff/MeritListWithMobileEmail.rdlc".MapPath()
				: "~/Reports/Admissions/Staff/MeritList.rdlc".MapPath();
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var applicationNos = this.tbApplicationNos.Text.Split(',').Select(n => n.TryToInt()).Where(t => t.HasValue).Select(t => t.Value).ToList();
			var entryTestDateFrom = this.dtpEntryTestDateFrom.SelectedDate;
			var entryTestDateTo = this.dtpEntryTestDateTo.SelectedDate;
			double? entryTestMarksPercentageFromInclusive = null;
			double? entryTestMarksPercentageToExclusive = null;
			double? etsMarksPercentageFromInclusive = null;
			double? etsMarksPercentageToExclusive = null;
			var displayOnlyPassed = this.ddlDisplayOnlyPassed.SelectedValue.ToBoolean();

			switch (this.ddlEntryTestMarksPercentageRange.SelectedValue)
			{
				case "All":
					break;
				case "None":
					entryTestMarksPercentageToExclusive = 0;
					break;
				case "<33%":
					entryTestMarksPercentageToExclusive = 33d;
					break;
				case ">=33%":
					entryTestMarksPercentageFromInclusive = 33d;
					break;
				case "<50%":
					entryTestMarksPercentageToExclusive = 50d;
					break;
				case ">=50%":
					entryTestMarksPercentageFromInclusive = 50d;
					break;
				case "<60%":
					entryTestMarksPercentageToExclusive = 60d;
					break;
				case ">=60%":
					entryTestMarksPercentageFromInclusive = 60d;
					break;
				case "<70%":
					entryTestMarksPercentageToExclusive = 70d;
					break;
				case ">=70%":
					entryTestMarksPercentageFromInclusive = 70d;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			switch (this.ddlETSMarksPercentageRange.SelectedValue)
			{
				case "All":
					break;
				case "None":
					etsMarksPercentageToExclusive = 0d;
					break;
				case "<33%":
					etsMarksPercentageToExclusive = 33d;
					break;
				case ">=33%":
					etsMarksPercentageFromInclusive = 33d;
					break;
				case "<50%":
					etsMarksPercentageToExclusive = 50d;
					break;
				case ">=50%":
					etsMarksPercentageFromInclusive = 50d;
					break;
				case "<60%":
					etsMarksPercentageToExclusive = 60d;
					break;
				case ">=60%":
					etsMarksPercentageFromInclusive = 60d;
					break;
				case "<70%":
					etsMarksPercentageToExclusive = 70d;
					break;
				case ">=70%":
					etsMarksPercentageFromInclusive = 70d;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			var reportDataSet = BL.Core.Admissions.Staff.Reports.MeritList.Generate(semesterID, departmentID, programID, applicationNos, entryTestDateFrom, entryTestDateTo, entryTestMarksPercentageFromInclusive, entryTestMarksPercentageToExclusive, etsMarksPercentageFromInclusive, etsMarksPercentageToExclusive, displayOnlyPassed, this.StaffIdentity.LoginSessionGuid);

			var diplomas = reportDataSet.DiplomaCandidates.ToList();
			var bachelors = reportDataSet.BachelorsCandidates.ToList();
			var masters = reportDataSet.MastersCandidates.ToList();
			var doctorate = reportDataSet.DoctorateCandidates.ToList();
			var summary = reportDataSet.Summary.ToList();
			switch (this.ddlType.SelectedValue)
			{
				case "All":
					break;
				case "Diploma":
					bachelors.Clear();
					masters.Clear();
					doctorate.Clear();
					summary = summary.FindAll(s => s.DegreeLevelEnum == DegreeLevels.Diploma);
					break;
				case "Undergraduate":
					diplomas.Clear();
					masters.Clear();
					doctorate.Clear();
					summary = summary.FindAll(s => s.DegreeLevelEnum == DegreeLevels.Undergraduate);
					break;
				case "Postgraduate":
					diplomas.Clear();
					bachelors.Clear();
					doctorate.Clear();
					summary = summary.FindAll(s => s.DegreeLevelEnum == DegreeLevels.Postgraduate);
					break;
				case "Doctorate":
					diplomas.Clear();
					bachelors.Clear();
					masters.Clear();
					summary = summary.FindAll(s => s.DegreeLevelEnum == DegreeLevels.Doctorate);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Clear();
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.DoctorateCandidates), doctorate));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.MastersCandidates), masters));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.BachelorsCandidates), bachelors));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.DiplomaCandidates), diplomas));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.Summary), summary));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}