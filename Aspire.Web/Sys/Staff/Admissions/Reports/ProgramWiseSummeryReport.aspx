﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="ProgramWiseSummeryReport.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.Reports.ProgramWiseSummeryReport" %>

<asp:Content runat="server" ContentPlaceHolderID="Parameters">
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
				<aspire:AspireDropDownList runat="server" ValidationGroup="Parameters" ID="ddlSemesterID" CausesValidation="False" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" ValidationGroup="Parameters" Text="Program:" AssociatedControlID="ddlAdmissionOpenProgramID" />
				<aspire:AspireDropDownList runat="server" ValidationGroup="Parameters" ID="ddlAdmissionOpenProgramID" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" ValidationGroup="Parameters" Text="Shift:" AssociatedControlID="ddlShifts" />
				<aspire:AspireDropDownList runat="server" ValidationGroup="Parameters" ID="ddlShifts" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Application No.:" AssociatedControlID="tbApplicationNo" />
				<aspire:AspireTextBox runat="server" ValidationGroup="Parameters" ID="tbApplicationNo" MaxLength="10" />
				<aspire:AspireInt32Validator AllowNull="True" ValidationGroup="Parameters" runat="server" ControlToValidate="tbApplicationNo" InvalidNumberErrorMessage="Invalid number" />
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Apply Date (From):" AssociatedControlID="dtpAppliedDateFrom" />
				<aspire:AspireDateTimePickerTextbox ID="dtpAppliedDateFrom" ValidationGroup="Parameters" runat="server" />
				<aspire:AspireDateTimeValidator AllowNull="True" runat="server" ControlToValidate="dtpAppliedDateFrom" ValidationGroup="Parameters" InvalidDataErrorMessage="Invalid Date" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Apply Date (To):" AssociatedControlID="dtpAppliedDateTo" />
				<aspire:AspireDateTimePickerTextbox ID="dtpAppliedDateTo" ValidationGroup="Parameters" runat="server" />
				<aspire:AspireDateTimeValidator AllowNull="True" runat="server" ControlToValidate="dtpAppliedDateTo" ValidationGroup="Parameters" InvalidDataErrorMessage="Invalid Date" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Deposit Date (From):" AssociatedControlID="dtpDepositDateFrom" />
				<aspire:AspireDateTimePickerTextbox ID="dtpDepositDateFrom" ValidationGroup="Parameters" runat="server" />
				<aspire:AspireDateTimeValidator AllowNull="True" runat="server" ControlToValidate="dtpDepositDateFrom" ValidationGroup="Parameters" InvalidDataErrorMessage="Invalid Date" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Deposit Date (To):" AssociatedControlID="dtpDepositDateTo" />
				<aspire:AspireDateTimePickerTextbox ID="dtpDepositDateTo" ValidationGroup="Parameters" runat="server" />
				<aspire:AspireDateTimeValidator AllowNull="True" runat="server" ControlToValidate="dtpDepositDateTo" ValidationGroup="Parameters" InvalidDataErrorMessage="Invalid Date" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Foreigner:" AssociatedControlID="ddlForeignCandidate" />
				<aspire:AspireDropDownList runat="server" ID="ddlForeignCandidate" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<div>
					<aspire:AspireLabel runat="server" Text="&nbsp;" AssociatedControlID="btnDisplayReport" />
				</div>
				<aspire:AspireButton runat="server" ButtonType="Primary" ID="btnDisplayReport" ValidationGroup="Parameters" Text="Display Report" OnClick="btnDisplayReport_Click" />
			</div>
		</div>
	</div>
</asp:Content>
