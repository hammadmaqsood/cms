﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Admissions.Reports
{
	[AspirePage("EFA153DB-ECB6-4A34-B9E1-E4AA9FED11EC", UserTypes.Candidate, "~/Sys/Staff/Admissions/Reports/AdmitSlipsReport.aspx", "Admit Slips", true, AspireModules.Admissions)]
	public partial class AdmitSlipsReport : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Admissions.Module, new []{UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Admit Slip(s)";
		public string ReportName { get; }
		public bool ExportToExcel => false;
		public bool ExportToWord => false;

		public static string GetPageUrl(int candidateID, int? candidateAppliedProgramID)
		{
			return GetPageUrl<AdmitSlipsReport>().AttachQueryParams(ReportView.ReportFormat, ReportFormats.PDF, ReportView.Inline, true, "CandidateID", candidateID, "CandidateAppliedProgramID", candidateAppliedProgramID);
		}

		private int? CandidateAppliedProgramID => this.Request.GetParameterValue<int>("CandidateAppliedProgramID");
		private int? CandidateID => this.Request.GetParameterValue<int>("CandidateID");

		protected void Page_Load(object sender, EventArgs e)
		{
			if (this.CandidateID == null)
				Redirect<Dashboard>();
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			Sys.Common.Methods.RenderAdmitSlip<Dashboard>(this, reportViewer, this.CandidateID.Value, this.CandidateAppliedProgramID);
		}
	}
}