﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Staff.Admissions.Reports
{
	[AspirePage("C0E15DC9-5D8F-43EE-8A81-5F72C1402E63", UserTypes.Staff, "~/Sys/Staff/Admissions/Reports/CandidateNominalReport.aspx", "Candidate Nominal", true, AspireModules.Admissions)]
	public partial class CandidateNominalReport : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Candidate Nominal";
		public string ReportName => "Candidate Nominal";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlCandidateNominalSemesterID.FillSemesters();
				this.ddlShifts.FillShifts(CommonListItems.All);
				this.ddlForeignCandidate.FillYesNo(CommonListItems.All).SetSelectedValueNo();
				this.ddlCandidateNominalSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlCandidateNominalSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlCandidateNominalSemesterID.SelectedValue.ToShort();
			var admissionOpenPrograms = Aspire.BL.Core.Common.Lists.GetAdmissionOpenPrograms(semesterID, this.StaffIdentity.InstituteID);
			this.ddlCandidateNominalAdmissionOpenProgramID.DataBind(admissionOpenPrograms, CommonListItems.All);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (!this.IsPostBack)
				return;
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/Admissions/Staff/CandidateNominal.rdlc".MapPath();
			var semesterID = this.ddlCandidateNominalSemesterID.SelectedValue.ToShort();
			var admissionOpenProgramID = this.ddlCandidateNominalAdmissionOpenProgramID.SelectedValue.ToNullableInt();
			var shiftEnum = this.ddlShifts.GetSelectedEnumValue<Shifts>(null);
			var applicationNo = this.tbCandidateNominalApplicationNo.Text.ToNullableInt();
			var admissionProcessingFeeSubmitted = this.ddlCandidateNominalAdmissionProcessingFeeSubmitted.SelectedValue.ToNullableBoolean();
			var applyDateFrom = this.dtpCandidateNominalAppliedDateFrom.SelectedDate;
			var applyDateTo = this.dtpCandidateNominalAppliedDateTo.SelectedDate;
			var depositDateFrom = this.dtpCandidateNominalDepositDateFrom.SelectedDate;
			var depositDateTo = this.dtpCandidateNominalDepositDateTo.SelectedDate;
			var foreignCandidate = this.ddlForeignCandidate.SelectedValue.ToNullableBoolean();
			var reportDataSet = BL.Core.Admissions.Staff.Reports.CandidateNominals.GetCandidateNominal(semesterID, admissionOpenProgramID, shiftEnum, applicationNo, admissionProcessingFeeSubmitted, applyDateFrom, applyDateTo, depositDateFrom, depositDateTo,foreignCandidate, this.StaffIdentity.LoginSessionGuid);

			var filters = string.IsNullOrWhiteSpace(reportDataSet.Filters) ? null : "Filters: " + reportDataSet.Filters;

			var parameters = new ReportParameter[5];
			parameters[0] = new ReportParameter("ReportTitle", $"Candidates Nominal [{semesterID.ToSemesterString()}]");
			parameters[1] = new ReportParameter("Institute", $"Bahria University {reportDataSet.Candidates.Select(c => c.Institute).FirstOrDefault()}");
			parameters[2] = new ReportParameter("ReportLogoPath", "~/Images/ReportLogo.gif".ToAbsoluteUrl());
			parameters[3] = new ReportParameter("Filters", filters);
			parameters[4] = new ReportParameter("FiltersVisible", string.IsNullOrWhiteSpace(filters).ToString());

			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.Candidates), reportDataSet.Candidates));
			reportViewer.LocalReport.SetParameters(parameters);
			reportViewer.LocalReport.EnableExternalImages = true;
		}

		protected void btnDisplayReport_Click(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView.ReportViewer);
		}
	}
}