﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Admissions.Reports
{
	[AspirePage("4B4E109A-132B-47F4-A6FE-200B9638EAC7", UserTypes.Candidate, "~/Sys/Staff/Admissions/Reports/AdmissionProcessingFeeChallanReport.aspx", "Admission Processing Fee Challan", true, AspireModules.Admissions)]
	public partial class AdmissionProcessingFeeChallanReport : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Admissions.Module, new []{UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Admission Processing Fee Challan";
		public string ReportName => "Admission Processing Fee Challan";
		public bool ExportToExcel => false;
		public bool ExportToWord => false;

		public static string GetPageUrl(int candidateAppliedProgramID)
		{
			return GetPageUrl<AdmissionProcessingFeeChallanReport>().AttachQueryParams(ReportView.ReportFormat, ReportFormats.PDF, ReportView.Inline, true, nameof(CandidateAppliedProgramID), candidateAppliedProgramID);
		}

		private int? CandidateAppliedProgramID => this.Request.GetParameterValue<int>(nameof(this.CandidateAppliedProgramID));

		protected void Page_Load(object sender, EventArgs e)
		{
			if (this.CandidateAppliedProgramID == null)
				Redirect<Dashboard>();
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			Sys.Common.Methods.RenderAdmissionProcessingFeeChallansReport<Dashboard>(this, reportViewer, this.CandidateAppliedProgramID ?? throw new InvalidOperationException());
		}
	}
}