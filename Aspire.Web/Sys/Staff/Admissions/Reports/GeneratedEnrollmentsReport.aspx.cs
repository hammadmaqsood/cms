﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Admissions.Reports
{
	[AspirePage("49D2E75B-B18D-44A5-8B30-420EBFD8BC78", UserTypes.Staff, "~/Sys/Staff/Admissions/Reports/GeneratedEnrollmentsReport.aspx", "Generated Enrollments", true, AspireModules.Admissions)]
	public partial class GeneratedEnrollmentsReport : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Generated Enrollments";
		public string ReportName => "Generated Enrollments";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		public static string GetPageUrl(int admissionOpenProgramID)
		{
			return GetPageUrl<GeneratedEnrollmentsReport>().AttachQueryParam("AdmissionOpenProgramID ", admissionOpenProgramID);
		}

		private int? AdmissionOpenProgramID => this.Request.GetParameterValue<int>("AdmissionOpenProgramID ");

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.AdmissionOpenProgramID == null)
				{
					Redirect<GenerateEnrollments>();
					return;
				}
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/Admissions/Staff/GeneratedEnrollments.rdlc".MapPath();
			var reportsDataSet = Aspire.BL.Core.Admissions.Staff.Reports.GeneratedEnrollments.GetGeneratedEnrollments(this.AdmissionOpenProgramID.Value, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("ReportHeader", new[] { reportsDataSet.ReportHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("Students", reportsDataSet.Students));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}