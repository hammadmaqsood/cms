﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Admissions.Reports
{
	[AspirePage("1B7A7C57-58AE-45B5-81DE-AC9CD813A9C5", UserTypes.Staff, "~/Sys/Staff/Admissions/Reports/CandidateDetailsReport.aspx", "Candidates Detail", true, AspireModules.Admissions)]
	public partial class CandidateDetailsReport : StaffPage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Candidates Detail";
		public string ReportName => this.PageTitle;
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};

		private bool _renderDisabled = true;
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters();
				if (this.ddlSemesterIDFilter.Items.Count == 0)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID);
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var admissionOpenPrograms = Aspire.BL.Core.Admissions.Staff.Reports.CandidateDetailsReport.GetAdmissionOpenPrograms(this.StaffIdentity.InstituteID, semesterID, departmentID);
			this.ddlProgramIDFilter.DataBind(admissionOpenPrograms);
			this.ddlProgramIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearch_OnClick(null, null);
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this._renderDisabled = false;
			this.RenderReport(reportView.ReportViewer);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this._renderDisabled)
				return;
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/Admissions/Staff/CandidateDetailsReport.rdlc".MapPath();
			var reportDataSet = BL.Core.Admissions.Staff.Reports.CandidateDetailsReport.GetCandidatesDetails(semesterID, departmentID, programID, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.SubreportProcessing += (sender, e) =>
			{
				switch (e.ReportPath)
				{
					case "CandidateAppliedPrograms":
						var candidateID = e.Parameters["CandidateID"].Values[0].ToInt();
						var candidateAppliedPrograms = BL.Core.Admissions.Staff.Reports.CandidateDetailsReport.GetCandidateAppliedPrograms(semesterID,candidateID,this.StaffIdentity.LoginSessionGuid);
						e.DataSources.Add(new ReportDataSource("CandidateAppliedPrograms", candidateAppliedPrograms));
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(e.ReportPath));
				}
			};

			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.CandidateDetails), reportDataSet.CandidateDetails));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}