﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="SummaryOfAppliedCandidates.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.Reports.SummaryOfAppliedCandidates" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" AutoPostBack="True" CausesValidation="False" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" AutoPostBack="True" CausesValidation="False" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Admission Processing Fee Paid:" AssociatedControlID="ddlAdmissionProcessingFee" />
			<aspire:AspireDropDownList runat="server" ID="ddlAdmissionProcessingFee" OnSelectedIndexChanged="ddlAdmissionProcessingFee_OnSelectedIndexChanged" AutoPostBack="True" CausesValidation="False" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Entry Test Cleared:" AssociatedControlID="ddlEntryTestCleared" />
			<aspire:AspireDropDownList runat="server" ID="ddlEntryTestCleared" OnSelectedIndexChanged="ddlEntryTestCleared_OnSelectedIndexChanged" AutoPostBack="True" CausesValidation="False" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Interview Cleared:" AssociatedControlID="ddlInterviewCleared" />
			<aspire:AspireDropDownList runat="server" ID="ddlInterviewCleared" OnSelectedIndexChanged="ddlInterviewCleared_OnSelectedIndexChanged" AutoPostBack="True" CausesValidation="False" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Enrollment Generated:" AssociatedControlID="ddlEnrollmentGenerated" />
			<aspire:AspireDropDownList runat="server" ID="ddlEnrollmentGenerated" OnSelectedIndexChanged="ddlEnrollmentGenerated_OnSelectedIndexChanged" AutoPostBack="True" CausesValidation="False" />
		</div>
		
	</div>
</asp:Content>
