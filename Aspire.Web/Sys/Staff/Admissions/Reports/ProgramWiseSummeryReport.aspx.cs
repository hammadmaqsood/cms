﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Admissions.Reports
{
	[AspirePage("BE80B7AA-DA07-4560-9A67-1A6DDFA0E859", UserTypes.Staff, "~/Sys/Staff/Admissions/Reports/ProgramWiseSummeryReport.aspx", "Program Wise Summery", true, AspireModules.Admissions)]

	public partial class ProgramWiseSummeryReport : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Program Wise Summery";
		public string ReportName => "Program Wise Summery";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterID.FillSemesters();
				this.ddlShifts.FillShifts(CommonListItems.All);
				this.ddlForeignCandidate.FillYesNo(CommonListItems.All);
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var admissionOpenPrograms = Aspire.BL.Core.Common.Lists.GetAdmissionOpenPrograms(semesterID, this.StaffIdentity.InstituteID);
			this.ddlAdmissionOpenProgramID.DataBind(admissionOpenPrograms, CommonListItems.All);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (!this.IsPostBack)
				return;

			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var admissionOpenProgramID = this.ddlAdmissionOpenProgramID.SelectedValue.ToNullableInt();
			var shiftEnum = this.ddlShifts.GetSelectedEnumValue<Shifts>(null);
			var applicationNo = this.tbApplicationNo.Text.ToNullableInt();
			var applyDateFrom = this.dtpAppliedDateFrom.SelectedDate;
			var applyDateTo = this.dtpAppliedDateTo.SelectedDate;
			var depositDateFrom = this.dtpDepositDateFrom.SelectedDate;
			var depositDateTo = this.dtpDepositDateTo.SelectedDate;
			var foreignCandidate = this.ddlForeignCandidate.SelectedValue.ToNullableBoolean();

			var reportDataSet = BL.Core.Admissions.Staff.Reports.ProgramWiseSummary.GetProgramWiseSummery(semesterID, admissionOpenProgramID, shiftEnum, applicationNo, applyDateFrom, applyDateTo, depositDateFrom, depositDateTo, foreignCandidate, this.StaffIdentity.LoginSessionGuid);
			
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/Admissions/Staff/ProgramWiseSummery.rdlc".MapPath();
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.ProgramSummary), reportDataSet.ProgramSummary));
		}

		protected void btnDisplayReport_Click(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView.ReportViewer);
		}
	}
}