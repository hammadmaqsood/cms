﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="ETSScorerNominalReport.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.Reports.ETSScorerNominalReport" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlETSScoreSemesterID" />
			<aspire:AspireDropDownList runat="server" ValidationGroup="Parameters" ID="ddlETSScoreSemesterID" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="ddlETSScoreSemesterID_SelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlETSScoreAdmissionOpenProgramID" />
			<aspire:AspireDropDownList runat="server" ValidationGroup="Parameters" ID="ddlETSScoreAdmissionOpenProgramID" AutoPostBack="true" OnSelectedIndexChanged="ddlETSScoreAdmissionOpenProgramID_SelectedIndexChanged" />
		</div>
	</div>
</asp:Content>
