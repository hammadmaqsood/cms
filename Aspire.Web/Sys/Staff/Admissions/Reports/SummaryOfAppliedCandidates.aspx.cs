﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using Aspire.Lib.Extensions;

namespace Aspire.Web.Sys.Staff.Admissions.Reports
{
	[AspirePage("4A4CBE36-2B65-45BA-AA9A-5B3038809DD4", UserTypes.Staff, "~/Sys/Staff/Admissions/Reports/SummaryOfAppliedCandidates.aspx", "Program-Wise Application Summary", true, AspireModules.StudentInformation)]
	public partial class SummaryOfAppliedCandidates : StaffPage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Program-wise Application Summary";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};

		public string ReportName => "Program-Wise Application Summary";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters();
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All);
				this.ddlAdmissionProcessingFee.FillYesNo(CommonListItems.All);
				this.ddlEntryTestCleared.FillYesNo(CommonListItems.All);
				this.ddlInterviewCleared.FillYesNo(CommonListItems.All);
				this.ddlEnrollmentGenerated.FillYesNo(CommonListItems.All);
				if (this.ddlSemesterIDFilter.Items.Count == 0)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}


		public void RenderReport(ReportViewer reportViewer)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var departmentString = this.ddlDepartmentIDFilter.SelectedItem.Text;
			var admissionProcessingFee = this.ddlAdmissionProcessingFee.SelectedValue.ToNullableBoolean();
			var entryTestCleared = this.ddlEntryTestCleared.SelectedValue.ToNullableBoolean();
			var interviewCleared = this.ddlInterviewCleared.SelectedValue.ToNullableBoolean();
			var enrollmentGenerated = this.ddlEnrollmentGenerated.SelectedValue.ToNullableBoolean();
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/Admissions/Staff/SummaryOfAppliedCandidates.rdlc".MapPath();
			var reportDataSet = BL.Core.Admissions.Staff.Reports.SummaryOfAppliedCandidates.GetAppliedCandidatesSummary(semesterID, departmentID, departmentString, admissionProcessingFee, entryTestCleared, interviewCleared, enrollmentGenerated, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.AppliedCandidateSummary), reportDataSet.AppliedCandidateSummary));
			reportViewer.LocalReport.EnableExternalImages = true;
		}


		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlAdmissionProcessingFee_OnSelectedIndexChanged(null, null);
		}

		protected void ddlAdmissionProcessingFee_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlEnrollmentGenerated_OnSelectedIndexChanged(null, null);
		}

		protected void ddlEntryTestCleared_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlInterviewCleared_OnSelectedIndexChanged(null, null);
		}

		protected void ddlInterviewCleared_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlEnrollmentGenerated_OnSelectedIndexChanged(null, null);
		}

		protected void ddlEnrollmentGenerated_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView.ReportViewer);
		}
	}
}