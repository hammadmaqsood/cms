﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="CandidateNominalReport.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.Reports.CandidateNominalReport" %>

<asp:Content runat="server" ContentPlaceHolderID="Parameters">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlCandidateNominalSemesterID" />
			<aspire:AspireDropDownList runat="server" ValidationGroup="Parameters" ID="ddlCandidateNominalSemesterID" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="ddlCandidateNominalSemesterID_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" ValidationGroup="Parameters" Text="Program:" AssociatedControlID="ddlCandidateNominalAdmissionOpenProgramID" />
			<aspire:AspireDropDownList runat="server" ValidationGroup="Parameters" ID="ddlCandidateNominalAdmissionOpenProgramID" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" ValidationGroup="Parameters" Text="Shift:" AssociatedControlID="ddlShifts" />
			<aspire:AspireDropDownList runat="server" ValidationGroup="Parameters" ID="ddlShifts" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Application No.:" AssociatedControlID="tbCandidateNominalApplicationNo" />
			<aspire:AspireTextBox runat="server" ValidationGroup="Parameters" ID="tbCandidateNominalApplicationNo" MaxLength="10" />
			<aspire:AspireInt32Validator AllowNull="True" ValidationGroup="Parameters" runat="server" ControlToValidate="tbCandidateNominalApplicationNo" InvalidNumberErrorMessage="Invalid number" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Apply Date (From):" AssociatedControlID="dtpCandidateNominalAppliedDateFrom" />
			<aspire:AspireDateTimePickerTextbox ID="dtpCandidateNominalAppliedDateFrom" ValidationGroup="Parameters" runat="server" />
			<aspire:AspireDateTimeValidator AllowNull="True" runat="server" ControlToValidate="dtpCandidateNominalAppliedDateFrom" ValidationGroup="Parameters" InvalidDataErrorMessage="Invalid Date" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Apply Date (To):" AssociatedControlID="dtpCandidateNominalAppliedDateTo" />
			<aspire:AspireDateTimePickerTextbox ID="dtpCandidateNominalAppliedDateTo" ValidationGroup="Parameters" runat="server" />
			<aspire:AspireDateTimeValidator AllowNull="True" runat="server" ControlToValidate="dtpCandidateNominalAppliedDateTo" ValidationGroup="Parameters" InvalidDataErrorMessage="Invalid Date" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Admission Processing Fee Status:" AssociatedControlID="ddlCandidateNominalAdmissionProcessingFeeSubmitted" />
			<aspire:AspireDropDownList runat="server" ID="ddlCandidateNominalAdmissionProcessingFeeSubmitted">
				<Items>
					<asp:ListItem Text="All" Value="" />
					<asp:ListItem Text="Submitted" Value="1" />
					<asp:ListItem Text="Not Submitted" Value="0" />
				</Items>
			</aspire:AspireDropDownList>
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Deposit Date (From):" AssociatedControlID="dtpCandidateNominalDepositDateFrom" />
			<aspire:AspireDateTimePickerTextbox ID="dtpCandidateNominalDepositDateFrom" ValidationGroup="Parameters" runat="server" />
			<aspire:AspireDateTimeValidator AllowNull="True" runat="server" ControlToValidate="dtpCandidateNominalDepositDateFrom" ValidationGroup="Parameters" InvalidDataErrorMessage="Invalid Date" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Deposit Date (To):" AssociatedControlID="dtpCandidateNominalDepositDateTo" />
			<aspire:AspireDateTimePickerTextbox ID="dtpCandidateNominalDepositDateTo" ValidationGroup="Parameters" runat="server" />
			<aspire:AspireDateTimeValidator AllowNull="True" runat="server" ControlToValidate="dtpCandidateNominalDepositDateFrom" ValidationGroup="Parameters" InvalidDataErrorMessage="Invalid Date" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Foreigner:" AssociatedControlID="ddlForeignCandidate" />
			<aspire:AspireDropDownList runat="server" ID="ddlForeignCandidate" />
		</div>
		<div class="form-group">
			<aspire:AspireButton runat="server" ButtonType="Primary" ID="btnDisplayReport" ValidationGroup="Parameters" Text="Display Report" OnClick="btnDisplayReport_Click" />
		</div>
	</div>
</asp:Content>

