﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="MeritListReport.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.Reports.MeritListReport" %>

<asp:Content runat="server" ContentPlaceHolderID="Parameters">
	<asp:UpdatePanel runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
						<aspire:AspireDropDownList runat="server" ValidationGroup="Parameters" ID="ddlSemesterID" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlType" />
						<aspire:AspireDropDownList runat="server" ValidationGroup="Parameters" ID="ddlType">
							<Items>
								<asp:ListItem Text="All" />
								<asp:ListItem Text="Diploma" />
								<asp:ListItem Text="Undergraduate" />
								<asp:ListItem Text="Postgraduate" />
								<asp:ListItem Text="Doctorate" />
							</Items>
						</aspire:AspireDropDownList>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
						<aspire:AspireDropDownList runat="server" ValidationGroup="Parameters" ID="ddlDepartmentID" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentID_OnSelectedIndexChanged" />
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
						<aspire:AspireDropDownList runat="server" ValidationGroup="Parameters" ID="ddlProgramID" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Entry Test Score Range:" AssociatedControlID="ddlEntryTestMarksPercentageRange" />
						<aspire:AspireDropDownList runat="server" ValidationGroup="Parameters" ID="ddlEntryTestMarksPercentageRange">
							<Items>
								<asp:ListItem Text="All" />
								<asp:ListItem Text="None" />
								<asp:ListItem Text="<33%" />
								<asp:ListItem Text=">=33%" />
								<asp:ListItem Text="<50%" />
								<asp:ListItem Text=">=50%" />
								<asp:ListItem Text="<60%" />
								<asp:ListItem Text=">=60%" />
								<asp:ListItem Text="<70%" />
								<asp:ListItem Text=">=70%" />
							</Items>
						</aspire:AspireDropDownList>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="ETS Score Range:" AssociatedControlID="ddlETSMarksPercentageRange" />
						<aspire:AspireDropDownList runat="server" ValidationGroup="Parameters" ID="ddlETSMarksPercentageRange">
							<Items>
								<asp:ListItem Text="All" />
								<asp:ListItem Text="None" />
								<asp:ListItem Text="<33%" />
								<asp:ListItem Text=">=33%" />
								<asp:ListItem Text="<50%" />
								<asp:ListItem Text=">=50%" />
								<asp:ListItem Text="<60%" />
								<asp:ListItem Text=">=60%" />
								<asp:ListItem Text="<70%" />
								<asp:ListItem Text=">=70%" />
							</Items>
						</aspire:AspireDropDownList>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Entry Test Date (From):" AssociatedControlID="dtpEntryTestDateFrom" />
						<aspire:AspireDateTimePickerTextbox ID="dtpEntryTestDateFrom" ValidationGroup="Parameters" runat="server" />
						<aspire:AspireDateTimeValidator AllowNull="True" runat="server" ControlToValidate="dtpEntryTestDateFrom" ValidationGroup="Parameters" InvalidDataErrorMessage="Invalid Date" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Entry Test Date (To):" AssociatedControlID="dtpEntryTestDateTo" />
						<aspire:AspireDateTimePickerTextbox ID="dtpEntryTestDateTo" ValidationGroup="Parameters" runat="server" />
						<aspire:AspireDateTimeValidator AllowNull="True" runat="server" ControlToValidate="dtpEntryTestDateTo" ValidationGroup="Parameters" InvalidDataErrorMessage="Invalid Date" />
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Application No(s).:" AssociatedControlID="tbApplicationNos" />
						<aspire:AspireTextBox runat="server" ValidationGroup="Parameters" ID="tbApplicationNos" MaxLength="1000" />
						<aspire:AspireStringValidator runat="server" ControlToValidate="tbApplicationNos" AllowNull="True" ValidationGroup="Parameters" CustomValidationExpression="^(\d{1,6})(,(\d{1,6}))*$" InvalidDataErrorMessage="Invalid Application No(s)." ValidationExpression="Custom" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Display Only Passed:" AssociatedControlID="ddlDisplayOnlyPassed" />
						<aspire:AspireDropDownList runat="server" ValidationGroup="Parameters" ID="ddlDisplayOnlyPassed" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Display Mobile/Email:" AssociatedControlID="ddlDisplayMobile" />
						<aspire:AspireDropDownList runat="server" ValidationGroup="Parameters" ID="ddlDisplayMobile" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<div>
							<aspire:AspireLabel runat="server" AssociatedControlID="btnDisplayReport" Text="&nbsp;" />
						</div>
						<aspire:AspireButton runat="server" ButtonType="Primary" ID="btnDisplayReport" ValidationGroup="Parameters" Text="Display Report" OnClick="btnDisplayReport_Click" />
					</div>
				</div>
			</div>
		</ContentTemplate>
		<Triggers>
			<asp:PostBackTrigger ControlID="btnDisplayReport" />
		</Triggers>
	</asp:UpdatePanel>
</asp:Content>
