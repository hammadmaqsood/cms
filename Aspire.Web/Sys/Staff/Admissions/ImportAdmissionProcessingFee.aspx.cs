﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Admissions
{
	[AspirePage("EC92851E-EB6F-4A72-B497-0A8D12CC6F4B", UserTypes.Staff, "~/Sys/Staff/Admissions/ImportAdmissionProcessingFee.aspx", "Import Admission Processing Fee", true, AspireModules.Admissions)]
	public partial class ImportAdmissionProcessingFee : StaffPage
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.Module, new[] {Model.Entities.UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.money_bill_alt);
		public override string PageTitle => "Import Admission Processing Fee";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSkipRows.DataBind(Enumerable.Range(0, 11).Select(i => i.ToString()));
				var instituteBankAccounts = BL.Common.Common.GetInstituteBankAccounts(this.StaffIdentity.InstituteID, null, InstituteBankAccount.FeeTypes.AdmissionProcessingFee, null);
				this.ddlInstituteBankAccountID.DataBind(instituteBankAccounts, CommonListItems.Select);
				this.panelGrids.Visible = false;
			}
		}

		private void DeleteFile()
		{
			var fileInfo = new FileInfo((string)this.ViewState["FileName"]);
			if (fileInfo.Exists)
				fileInfo.Delete();
			this.ViewState["FileName"] = null;
		}

		protected void btnUploadFile_Click(object sender, EventArgs e)
		{
			this.panelGrids.Visible = false;
			if (!IsValid)
				return;
			var directory = "~/App_Data/Temp/".MapPath();
			var fileName = Path.Combine(directory, Guid.NewGuid().ToString("N"));
			this.fileUpload.PostedFile.SaveAs(fileName);
			var connectionString = ExcelHelper.GetOledbConnectionString(fileName, ExcelHelper.ExcelFormat.Xlsx, false);
			this.ViewState["FileName"] = fileName;
			string errorMessage;
			if (!ExcelHelper.TestConnection(connectionString, out errorMessage))
			{
				this.DeleteFile();
				this.AddErrorAlert($"Invalid Excel file uploaded. Error Message: {errorMessage}");
				Redirect<ImportAdmissionProcessingFee>();
				return;
			}
			var sheets = ExcelHelper.GetAllSheetNames(connectionString);
			this.ddlSheet.DataBind(sheets);
			this.ViewState["ConnectionString"] = connectionString;
			this.ddlSheet_OnSelectedIndexChanged(null, null);
			this.panelGrids.Visible = true;
			this.panelSaveButton.Visible = false;
			this.panelFileUpload.Visible = false;
		}

		private DataTable _excelDataTable;

		private void LoadDataTableIfNull()
		{
			if (_excelDataTable == null)
				this._excelDataTable = ExcelHelper.GetData((string)this.ViewState["ConnectionString"], this.ddlSheet.SelectedValue);
		}

		protected void ddlSheet_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.LoadDataTableIfNull();
			this.ddlSkipRows_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSkipRows_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			List<string> columnNames;
			using (var dataTable = this.GetExcelDataTable(this.ddlSkipRows.SelectedValue.ToInt32()))
			{
				columnNames = dataTable.Columns.Cast<DataColumn>().Select(c => c.ColumnName).ToList();
			}
			if (columnNames.Count < 7)
			{
				this.DeleteFile();
				this.AddErrorAlert("Excel file must have at least 7 columns.");
				Redirect<ImportAdmissionProcessingFee>();
			}
			this.ViewState["ColumnNames"] = columnNames;
			this.ddlChallanNo.DataBind(columnNames);
			this.ddlName.DataBind(columnNames);
			this.ddlProgram.DataBind(columnNames);
			this.ddlInstitute.DataBind(columnNames);
			this.ddlAmount.DataBind(columnNames);
			this.ddlBranchCode.DataBind(columnNames);
			this.ddlDepositDate.DataBind(columnNames);
			this.ddlChallanType.DataBind(columnNames);

			//Branch	Date	Challan	FName	Program	Institution	Enrollment	Challan Type	Amount
			this.SetValueIfExists(this.ddlChallanNo, "Challan");
			this.SetValueIfExists(this.ddlName, "FName");
			this.SetValueIfExists(this.ddlProgram, "Program");
			this.SetValueIfExists(this.ddlInstitute, "Institution");
			this.SetValueIfExists(this.ddlAmount, "Amount");
			this.SetValueIfExists(this.ddlBranchCode, "Branch");
			this.SetValueIfExists(this.ddlChallanType, "Challan Type");
			this.SetValueIfExists(this.ddlDepositDate, "Date");

			this.DisplayExcelFile();
		}

		private void SetValueIfExists(AspireDropDownList ddl, string value)
		{
			var item = ddl.Items.FindByValue(value);
			if (item != null)
				ddl.SelectedValue = value;
		}

		protected void ddlChallanNo_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.DisplayExcelFile();
		}

		protected void ddlName_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.DisplayExcelFile();
		}

		protected void ddlProgram_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.DisplayExcelFile();
		}

		protected void ddlInstitute_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.DisplayExcelFile();
		}

		protected void ddlAmount_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.DisplayExcelFile();
		}

		protected void ddlBranchCode_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.DisplayExcelFile();
		}

		protected void ddlDepositDate_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.DisplayExcelFile();
		}

		protected void ddlChallanType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.DisplayExcelFile();
		}

		protected const string ColumnNameChallanNo = "ChallanNo";
		protected const string ColumnNameName = "Name";
		protected const string ColumnNameProgram = "Program";
		protected const string ColumnNameInstitute = "Institute";
		protected const string ColumnNameAmount = "Amount";
		protected const string ColumnNameBranchCode = "BranchCode";
		protected const string ColumnNameChallanType = "ChallanType";
		protected const string ColumnNameDepositDate = "DepositDate";

		protected const string DBColumnNameFullChallanNo = "DBFullChallanNo";
		protected const string DBColumnNameName = "DBName";
		protected const string DBColumnNameProgram = "DBProgram";
		protected const string DBColumnNameInstitute = "DBInstitute";
		protected const string DBColumnNameAmount = "DBAmount";
		protected const string DBColumnNameBranchCode = "DBBranchCode";
		protected const string DBColumnNameChallanType = "DBChallanType";
		protected const string DBColumnNameDepositDate = "DBDepositDate";

		private DataTable GetExcelDataTable(int skipRows)
		{
			this.LoadDataTableIfNull();
			var rows = this._excelDataTable.AsEnumerable().Skip(skipRows);
			DataTable dataTable;
			if (rows.Any())
				dataTable = rows.CopyToDataTable();
			else
				dataTable = new DataTable();
			var firstRow = dataTable.AsEnumerable().FirstOrDefault();
			if (firstRow != null)
			{
				foreach (DataColumn column in dataTable.Columns)
				{
					var cellValue = firstRow[column.ColumnName]?.ToString();
					if (string.IsNullOrWhiteSpace(cellValue))
						cellValue = "Column#" + (column.Ordinal + 1);
					try
					{
						column.ColumnName = cellValue;
					}
					catch (DuplicateNameException)
					{
						dataTable.Reset();
						return dataTable;
					}
				}
				dataTable.Rows.Remove(firstRow);
			}
			return dataTable;
		}

		private DataTable GetDataTableForDisplayAndComparison()
		{
			using (var dataTable = this.GetExcelDataTable(this.ddlSkipRows.SelectedValue.ToInt32()))
			{
				var table = new DataTable();
				table.Columns.Add(ColumnNameChallanNo, typeof(string));
				table.Columns.Add(ColumnNameName, typeof(string));
				table.Columns.Add(ColumnNameProgram, typeof(string));
				table.Columns.Add(ColumnNameInstitute, typeof(string));
				table.Columns.Add(ColumnNameDepositDate, typeof(string));
				table.Columns.Add(ColumnNameBranchCode, typeof(string));
				table.Columns.Add(ColumnNameChallanType, typeof(string));
				table.Columns.Add(ColumnNameAmount, typeof(string));
				var indexChallanNo = dataTable.Columns[this.ddlChallanNo.SelectedValue].Ordinal;
				var indexNameFatherName = dataTable.Columns[this.ddlName.SelectedValue].Ordinal;
				var indexProgram = dataTable.Columns[this.ddlProgram.SelectedValue].Ordinal;
				var indexInstitute = dataTable.Columns[this.ddlInstitute.SelectedValue].Ordinal;
				var indexAmount = dataTable.Columns[this.ddlAmount.SelectedValue].Ordinal;
				var indexBranchCode = dataTable.Columns[this.ddlBranchCode.SelectedValue].Ordinal;
				var indexDepositDate = dataTable.Columns[this.ddlDepositDate.SelectedValue].Ordinal;
				var indexChallanType = dataTable.Columns[this.ddlChallanType.SelectedValue].Ordinal;
				foreach (var row in dataTable.AsEnumerable())
				{
					var itemArray = new[] { row[indexChallanNo], row[indexNameFatherName], row[indexProgram], row[indexInstitute], row[indexDepositDate], row[indexBranchCode], row[indexChallanType], row[indexAmount] };
					if (itemArray.Any(i => i != null && i != DBNull.Value && !string.IsNullOrWhiteSpace(i.ToString())))
						table.Rows.Add(itemArray);
				}
				return table;
			}
		}

		private void DisplayExcelFile()
		{
			this.gvExcelFile.Visible = true;
			using (var dataTable = this.GetDataTableForDisplayAndComparison())
			{
				this.gvExcelFile.DataBind(dataTable);
				this.lvMatchRecord.DataBindNull();
				this.lvMatchRecord.Visible = false;
				this.panelSaveButton.Visible = false;
				this.btnDisplayComparison.Enabled = false;
				for (var i = 0; i < dataTable.Rows.Count; i++)
				{
					var row = dataTable.Rows[i];
					var error = false;
					if (row.IsNull(ColumnNameChallanNo) || row[ColumnNameChallanNo].ToString().TryToDecimal() == null)
					{
						this.AddErrorAlert("Challan No. column has an error or missing value.");
						error = true;
					}
					if (row.IsNull(ColumnNameDepositDate) || row[ColumnNameDepositDate].ToString().TryToDateTime() == null)
					{
						this.AddErrorAlert("Deposit Date. column has an error or missing value.");
						error = true;
					}
					if (row.IsNull(ColumnNameBranchCode) || row[ColumnNameBranchCode].ToString().TryToInt16() == null)
					{
						this.AddErrorAlert("Branch Code column has an error or missing value.");
						error = true;
					}
					if (row.IsNull(ColumnNameAmount) || row[ColumnNameAmount].ToString().TryToInt16() == null)
					{
						this.AddErrorAlert("Amount column has an error or missing value.");
						error = true;
					}
					if (error)
					{
						this.gvExcelFile.Rows[dataTable.Rows.IndexOf(row)].CssClass = "bg-danger";
						return;
					}
				}
				this.btnDisplayComparison.Enabled = true;
			}
		}

		protected void btnDisplayComparison_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			using (var excelData = this.GetDataTableForDisplayAndComparison())
			{
				var challanNos = excelData.AsEnumerable().Select(r => r[ColumnNameChallanNo].ToString().TryToDecimal())
					.Where(i => i != null).Select(i => i.Value).ToList();
				var candidateAppliedPrograms = Aspire.BL.Core.Admissions.Staff.AdmissionProcessingFee.GetCandidateAppliedProgramsForImportFees(challanNos, this.StaffIdentity.LoginSessionGuid);

				excelData.Columns.Add(DBColumnNameFullChallanNo, typeof(string));
				excelData.Columns.Add(DBColumnNameName, typeof(string));
				excelData.Columns.Add(DBColumnNameProgram, typeof(string));
				excelData.Columns.Add(DBColumnNameInstitute, typeof(string));
				excelData.Columns.Add(DBColumnNameDepositDate, typeof(string));
				excelData.Columns.Add(DBColumnNameBranchCode, typeof(string));
				excelData.Columns.Add(DBColumnNameChallanType, typeof(string));
				excelData.Columns.Add(DBColumnNameAmount, typeof(double));

				foreach (var cap in candidateAppliedPrograms)
				{
					DataRow row;
					var filter = excelData.AsEnumerable().Where(r => r[ColumnNameChallanNo].ToString().TryToDecimal() == cap.FullChallanNo.ToDecimal());
					switch (filter.Count())
					{
						case 1:
							row = filter.Single();
							break;
						default:
							this.DeleteFile();
							this.AddErrorAlert("Duplicate Challan No found. Challan No: " + cap.FullChallanNo);
							Redirect<ImportAdmissionProcessingFee>();
							return;
					}
					row[DBColumnNameFullChallanNo] = cap.FullChallanNo;
					row[DBColumnNameName] = cap.Name;
					row[DBColumnNameProgram] = cap.Program;
					row[DBColumnNameInstitute] = cap.Institute;
					row[DBColumnNameDepositDate] = cap.DepositDate;
					row[DBColumnNameBranchCode] = cap.BranchCode?.ToString().PadLeft(4, '0');
					row[DBColumnNameAmount] = cap.Amount;
				}

				this.gvExcelFile.Visible = false;
				this.gvExcelFile.DataBind(null);
				this.lvMatchRecord.Visible = true;
				this.lvMatchRecord.DataBind(excelData);
				this.panelSaveButton.Visible = true;
			}
		}

		private string GetString(object obj)
		{
			if (string.IsNullOrWhiteSpace(obj?.ToString()))
				return null;
			return obj.ToString().Trim().Replace(" ", "");
		}

		protected string GetClassName(object excel, object db)
		{
			//const string danger = "bg-danger";
			const string success = "bg-success";
			var excelString = GetString(excel);
			var dbString = GetString(db);
			if (excelString != null && excelString == dbString)
				return success;
			return null;
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!IsValid)
				return;
			var list = new List<Aspire.BL.Core.Admissions.Staff.AdmissionProcessingFee.AdmissionProcessingFeeChallan>();
			var instituteBankAccountID = this.ddlInstituteBankAccountID.SelectedValue.ToInt();
			foreach (var item in this.lvMatchRecord.Items)
			{
				var cb = (System.Web.UI.WebControls.CheckBox)item.FindControl("cb");
				if (cb.Visible == false || cb.Checked == false)
					continue;
				var key = this.lvMatchRecord.DataKeys[item.DataItemIndex];
				var admissionProcessingFeeChallan = new Aspire.BL.Core.Admissions.Staff.AdmissionProcessingFee.AdmissionProcessingFeeChallan
				{
					DepositDate = ((string)key[ColumnNameDepositDate]).ToDateTime(),
					InstituteBankAccountID = instituteBankAccountID,
					BranchCode = ((string)key[ColumnNameBranchCode]).ToShort(),
					FullChallanNo = ((string)key[ColumnNameChallanNo]).ToDecimal(),
					DepositPlaceEnum = DepositPlaces.Bank,
					DepositBankBranchID = null,
					FeeUpdatedByUserTypeEnum = FeeUpdatedByUserTypes.Staff,
					Paid = true,
				};
				list.Add(admissionProcessingFeeChallan);
			}

			if (!list.Any())
			{
				this.AddWarningAlert("No record found.");
				return;
			}

			var result = Aspire.BL.Core.Admissions.Staff.AdmissionProcessingFee.UpdateAdmissionProcessingFeeStatus(list, this.StaffIdentity.LoginSessionGuid);
			var counts = result.GroupBy(g => g.Value).Select(g => new
			{
				Status = g.Key,
				Challans = g.Select(c => c.Key).ToList(),
			}).ToList();

			foreach (var count in counts)
			{
				switch (count.Status)
				{
					case BL.Core.Admissions.Staff.AdmissionProcessingFee.UpdateAdmissionProcessingFeeStatusResults.Success:
						this.AddSuccessAlert($"{count.Challans.Count} records updated.");
						break;
					case BL.Core.Admissions.Staff.AdmissionProcessingFee.UpdateAdmissionProcessingFeeStatusResults.NoRecordFound:
						this.AddErrorAlert($"{count.Challans.Count} records not found.");
						break;
					case BL.Core.Admissions.Staff.AdmissionProcessingFee.UpdateAdmissionProcessingFeeStatusResults.CannotUpdateAdmissionProcessingFee:
						this.AddWarningAlert($"{count.Challans.Count} record cannot be updated because it has been updated through online services.");
						break;
					case BL.Core.Admissions.Staff.AdmissionProcessingFee.UpdateAdmissionProcessingFeeStatusResults.CannotBeUnpaidEnrollmentHasBeenGenerated:
					case BL.Core.Admissions.Staff.AdmissionProcessingFee.UpdateAdmissionProcessingFeeStatusResults.CannotBeUnpaidFeeChallanHasBeenGenerated:
						throw new InvalidOperationException();
					case BL.Core.Admissions.Staff.AdmissionProcessingFee.UpdateAdmissionProcessingFeeStatusResults.InvalidBranchCode:
						this.AddErrorAlert($"{count.Challans.Count} Invalid branch codes: " + string.Join(",", count.Challans.Select(c => c.BranchCode)));
						break;
					case BL.Core.Admissions.Staff.AdmissionProcessingFee.UpdateAdmissionProcessingFeeStatusResults.AlreadyPaid:
						this.AddErrorAlert($"{count.Challans.Count} Already paid. Challan Nos: " + string.Join(",", count.Challans.Select(c => c.FullChallanNo)));
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			Redirect<ImportAdmissionProcessingFee>();
		}

		protected void btnCancel_OnClick(object sender, EventArgs e)
		{
			Redirect<ImportAdmissionProcessingFee>();
		}
	}
}