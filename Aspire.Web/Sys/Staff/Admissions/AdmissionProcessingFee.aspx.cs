﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Admissions
{
	[AspirePage("6DD12A16-E891-404A-9C62-34557A17F2CC", UserTypes.Staff, "~/Sys/Staff/Admissions/AdmissionProcessingFee.aspx", "Admission Processing Fee", true, AspireModules.Admissions)]
	public partial class AdmissionProcessingFee : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.ManageAdmissionProcessingFee, new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override string PageTitle => "Admission Processing Fee Payment";
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.usd);

		public static string GetPageUrl(decimal? fullChallanNo, bool search)
		{
			return typeof(AdmissionProcessingFee).GetAspirePageAttribute().PageUrl.AttachQueryParams("FullChallanNo", fullChallanNo?.ToString(AspireFormats.ChallanNo.FullChallanNoDecimalFormat), "Search", search);
		}

		public static void Redirect(decimal? fullChallanNo, bool search)
		{
			Redirect(GetPageUrl(fullChallanNo, search));
		}
	}
}