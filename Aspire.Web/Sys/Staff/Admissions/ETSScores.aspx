﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ETSScores.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.ETSScores" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterSearch" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterSearch" CausesValidation="True" ValidationGroup="Search" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Application No.:" AssociatedControlID="tbApplicationNo" />
					<div class="input-group">
						<aspire:AspireTextBox ID="tbApplicationNoSearch" runat="server" ValidationGroup="Search" />
						<span class="input-group-btn">
							<aspire:AspireButton runat="server" ButtonType="Default" Glyphicon="search" ID="btnSearchCandidate" CausesValidation="True" OnClick="btnSearchCandidate_OnClick" ValidationGroup="Search" />
						</span>
					</div>
					<aspire:AspireInt32Validator runat="server" AllowNull="False" ControlToValidate="tbApplicationNoSearch" RequiredErrorMessage="This field is required." ValidationGroup="Search" InvalidNumberErrorMessage="Invalid Number." />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<div class="row" id="divETSInfo" runat="server">
		<div class="form-group col-md-6">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="tbSemester" />
			<aspire:AspireTextBox runat="server" ID="tbSemester" ReadOnly="True" />
		</div>
		<div class="form-group col-md-6">
			<aspire:AspireLabel runat="server" Text="Application No.:" AssociatedControlID="tbApplicationNo" />
			<aspire:AspireTextBox runat="server" ID="tbApplicationNo" ReadOnly="True" />
		</div>
		<div class="form-group col-md-6">
			<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbName" />
			<aspire:AspireTextBox runat="server" ID="tbName" ReadOnly="True" />
		</div>
		<div class="form-group col-md-6">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="tbProgram" />
			<aspire:AspireTextBox runat="server" ID="tbProgram" ReadOnly="True" />
		</div>
		<div class="form-group col-md-6">
			<aspire:AspireLabel runat="server" Text="ETS Type:" AssociatedControlID="ddlETSType" />
			<aspire:AspireDropDownList runat="server" ID="ddlETSType" />
		</div>
		<div class="form-group col-md-6">
			<aspire:AspireLabel runat="server" Text="Obtained Marks:" AssociatedControlID="tbETSObtained" ValidationGroup="tbETSObtained" />
			<aspire:AspireTextBox runat="server" ID="tbETSObtained" />
			<aspire:AspireDoubleValidator runat="server" AllowNull="False" ValidationGroup="ETSScore" ControlToValidate="tbETSObtained" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Number must be from {min} to {max}." InvalidNumberErrorMessage="Invalid number." />
		</div>
		<div class="form-group col-md-6">
			<aspire:AspireLabel runat="server" Text="Total Marks:" AssociatedControlID="tbETSTotal" />
			<aspire:AspireTextBox runat="server" ID="tbETSTotal" />
			<aspire:AspireUInt16Validator runat="server" AllowNull="False" ValidationGroup="ETSScore" ControlToValidate="tbETSTotal" ErrorMessage="Invalid Number." RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid number." InvalidRangeErrorMessage="Number must be from {min} to {max}." />
			<aspire:AspireCompareValidator ValidateEmptyText="True" runat="server" ErrorMessage="Obtained Marks must be less than or equal to Total Marks." ControlToValidate="tbETSTotal" ControlToCompare="tbETSObtained" Type="Double" ValidationGroup="ETSScore" Operator="GreaterThanEqual" />
		</div>
		<div class="form-group col-md-12 text-center">
			<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save" ValidationGroup="ETSScore" ID="btnSave" OnClick="btnSave_Click" />
			<aspire:AspireButton runat="server" ButtonType="Danger" Text="Delete" CausesValidation="False" ConfirmMessage="Are you sure you want to delete ETS Score?" ID="btnDelete" OnClick="btnDelete_OnClick" />
		</div>
	</div>
</asp:Content>
