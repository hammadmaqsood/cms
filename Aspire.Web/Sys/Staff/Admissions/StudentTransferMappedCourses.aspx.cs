﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Admissions
{
	[AspirePage("CF4E8775-D86A-407D-9A95-E95A97DD6930", UserTypes.Staff, "~/Sys/Staff/Admissions/StudentTransferMappedCourses.aspx", "Student Transfer - Mapped Courses", true, AspireModules.Admissions)]
	public partial class StudentTransferMappedCourses : StaffPage
	{
		public override PageIcon PageIcon => AspireGlyphicons.transfer.GetIcon();
		public override string PageTitle => "Student Transfer - Course Mapping";

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};

		public static string GetPageUrl(int transferredStudentID)
		{
			return typeof(StudentTransferMappedCourses).GetAspirePageAttribute().PageUrl.AttachQueryParam(nameof(TransferredStudentID), transferredStudentID);
		}

		public static void Redirect(int transferredStudentID)
		{
			Redirect(GetPageUrl(transferredStudentID));
		}

		private int? TransferredStudentID => this.GetParameterValue<int>(nameof(this.TransferredStudentID));

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.TransferredStudentID == null)
				{
					Redirect<StudentTransfer>();
					return;
				}

				var transferredStudentDetails = BL.Core.Admissions.Staff.StudentTransfer.GetTransferredStudentDetails(this.TransferredStudentID.Value, this.StaffIdentity.LoginSessionGuid);
				if (transferredStudentDetails == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<StudentTransfer>();
					return;
				}

				this.lblEnrollment.Text = transferredStudentDetails.Enrollment;
				this.lblRegistrationNo.Text = transferredStudentDetails.RegistrationNo.ToString();
				this.lblName.Text = transferredStudentDetails.Name;
				this.lblTransferSemester.Text = transferredStudentDetails.TransferSemesterID.ToSemesterString();
				this.lblFromInstituteAlias.Text = transferredStudentDetails.FromInstituteAlias;
				this.lblFromProgramShortName.Text = transferredStudentDetails.FromProgramShortName;
				this.lblFromIntakeSemesterID.Text = transferredStudentDetails.FromIntakeSemesterID.ToSemesterString();
				this.lblToInstituteAlias.Text = transferredStudentDetails.ToInstituteAlias;
				this.lblToProgramShortName.Text = transferredStudentDetails.ToProgramShortName;
				this.lblToIntakeSemesterID.Text = transferredStudentDetails.ToIntakeSemesterID.ToSemesterString();

				this.panelCourseMappings.Visible = true;
				this.panelCourseMappingsEdit.Visible = false;
				this.repeaterTransferredStudents.DataBind(GetCourseMappings(transferredStudentDetails));
				this.hlCancel.NavigateUrl = StudentTransfer.GetPageUrl(this.StaffIdentity.InstituteID, transferredStudentDetails.Enrollment, true);
			}
		}

		private static List<CourseMapping> GetCourseMappings(BL.Core.Admissions.Staff.StudentTransfer.TransferredStudentDetails transferredStudentDetails)
		{
			var mappings = from tscm in transferredStudentDetails.TransferredStudentCourseMappings
						   join rc in transferredStudentDetails.RoadmapCourses on tscm.ToCourseID equals rc.CourseID
						   select new
						   {
							   tscm,
							   rc
						   };

			var mappedCourses = from rc in transferredStudentDetails.RegisteredCourses
								join m in mappings on rc.RegisteredCourseID equals m.tscm.FromRegisteredCourseID into grp
								from g in grp.DefaultIfEmpty()
								select new CourseMapping
								{
									RegisteredCourse = rc,
									TransferredStudentCourseMapping = g?.tscm,
									RoadmapCourse = g?.rc
								};
			return mappedCourses.ToList();
		}

		protected sealed class CourseMapping
		{
			public BL.Core.Admissions.Staff.StudentTransfer.TransferredStudentDetails.RegisteredCourse RegisteredCourse { get; set; }
			public TransferredStudentCourseMapping TransferredStudentCourseMapping { get; internal set; }
			public BL.Core.Admissions.Staff.StudentTransfer.TransferredStudentDetails.RoadmapCourse RoadmapCourse { get; internal set; }
		}

		protected List<CustomListItem> RoadmapCourses { get; set; }
		protected List<CustomListItem> Statuses { get; set; }

		protected void btnEdit_OnClick(object sender, EventArgs e)
		{
			var transferredStudentDetails = BL.Core.Admissions.Staff.StudentTransfer.GetTransferredStudentDetails(this.TransferredStudentID ?? throw new InvalidOperationException(), this.StaffIdentity.LoginSessionGuid);
			if (transferredStudentDetails == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect<StudentTransfer>();
				return;
			}

			this.RoadmapCourses = transferredStudentDetails.RoadmapCourses.Select(rc => new CustomListItem
			{
				Text = $"[{rc.CourseCode}] {rc.Title} [{rc.CreditHours.ToCreditHoursFullName()}]",
				ID = rc.CourseID
			}).OrderBy(i => i.Text)
				.ToList();

			this.panelCourseMappings.Visible = false;
			this.panelCourseMappingsEdit.Visible = true;

			var courseMappings = GetCourseMappings(transferredStudentDetails);
			foreach (var courseMapping in courseMappings.Where(c => c.RoadmapCourse == null))
			{
				courseMapping.RoadmapCourse = transferredStudentDetails.RoadmapCourses
					.Where(rc => rc.CourseCode?.ToLower() == courseMapping.RegisteredCourse.CourseCode?.ToLower())
					.Where(rc => rc.Title?.ToLower() == courseMapping.RegisteredCourse.Title?.ToLower())
					.FirstOrDefault(rc => rc.CreditHours == courseMapping.RegisteredCourse.CreditHours);
			}

			this.Statuses = Enum.GetValues(typeof(TransferredStudentCourseMapping.Statuses)).Cast<TransferredStudentCourseMapping.Statuses>().Select(m => new CustomListItem
			{
				ID = (byte)m,
				Text = m.ToFullName()
			}).ToList();
			this.repeaterTransferredStudentsEdit.DataBind(courseMappings);
			this.btnSaveMappings.Enabled = courseMappings.Any();
			this.hlCancelEdit.NavigateUrl = GetPageUrl(transferredStudentDetails.TransferredStudentID);
		}

		protected void btnSaveMappings_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;

			var courseMappings = new Dictionary<int, (int CourseID, TransferredStudentCourseMapping.Statuses StatusEnum)?>();
			foreach (RepeaterItem item in this.repeaterTransferredStudentsEdit.Items)
			{
				var registeredCourseID = ((Lib.WebControls.HiddenField)item.FindControl("hfRegisteredCourseID")).Value.ToInt();
				var roadmapCourseID = ((AspireDropDownList)item.FindControl("ddlRoadmapCourseID")).SelectedValue.ToNullableInt();
				var status = ((AspireDropDownList)item.FindControl("ddlStatus")).GetSelectedEnumValue<TransferredStudentCourseMapping.Statuses>();
				if (roadmapCourseID != null)
					courseMappings.Add(registeredCourseID, (roadmapCourseID.Value, status));
				else
					courseMappings.Add(registeredCourseID, null);
			}

			var updated = BL.Core.Admissions.Staff.StudentTransfer.UpdateCourseMappings(this.TransferredStudentID ?? throw new InvalidOperationException(), courseMappings, this.StaffIdentity.LoginSessionGuid);
			if (updated)
				this.AddSuccessAlert("Course Mappings have been saved.");
			else
				this.AddNoRecordFoundAlert();
			Redirect(this.TransferredStudentID ?? throw new InvalidOperationException());
		}
	}
}