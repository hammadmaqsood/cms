﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="SearchCandidate.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.SearchCandidate" %>

<%@ Import Namespace="Aspire.Web.Sys.Staff.Admissions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:Panel runat="server" DefaultButton="btnSearch">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="SearchCandidate" />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Challan No.:" AssociatedControlID="tbChallanNo" />
					<aspire:AspireTextBox runat="server" ID="tbChallanNo" ValidationGroup="SearchCandidate" />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Application No.:" AssociatedControlID="tbApplicationNo" />
					<aspire:AspireTextBox runat="server" ID="tbApplicationNo" ValidationGroup="SearchCandidate" />
					<aspire:AspireInt32Validator runat="server" ControlToValidate="tbApplicationNo" AllowNull="True" ValidationGroup="SearchCandidate" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Application No." />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbName" />
					<aspire:AspireTextBox runat="server" ID="tbName" ValidationGroup="SearchCandidate" TextTransform="UpperCase" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Father Name:" AssociatedControlID="tbFatherName" />
					<aspire:AspireTextBox runat="server" ID="tbFatherName" ValidationGroup="SearchCandidate" TextTransform="UpperCase" />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Email:" AssociatedControlID="tbEmail" />
					<aspire:AspireTextBox runat="server" ID="tbEmail" />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Foreigner:" AssociatedControlID="ddlForeignStudent" />
					<aspire:AspireDropDownList ID="ddlForeignStudent" ValidationGroup="SearchCandidate" runat="server" />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<aspire:AspireLabel runat="server" Text="&nbsp;" AssociatedControlID="btnSearch" />
					</div>
					<aspire:AspireButton runat="server" Text="Search" CssClass="btn-block" ButtonType="Primary" ID="btnSearch" ValidationGroup="SearchCandidate" OnClick="btnSearch_OnClick" />
				</div>
			</div>
		</div>
	</asp:Panel>
	<aspire:AspireGridView runat="server" ItemType="Aspire.BL.Core.Admissions.Staff.CandidateInfo.GetCandidateInformationResult.Candidate" ID="gvSearchCandidate" AutoGenerateColumns="False" AllowSorting="True" OnPageIndexChanging="gvSearchCandidate_OnPageIndexChanging" OnPageSizeChanging="gvSearchCandidate_OnPageSizeChanging" OnSorting="gvSearchCandidate_OnSorting" PageSize="100">
		<Columns>
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<div class="btn-group">
						<a href="#" class="dropdown-toggle text-nowrap" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions<span class="caret"></span></a>
						<ul class="dropdown-menu dropdown-menu-left">
							<li runat="server">
								<aspire:HyperLink Target="_blank" runat="server" Text="View Profile" NavigateUrl="<%# CandidateDetails.GetPageUrl(Item.CandidateID) %>" />
							</li>
							<li runat="server" visible="<%# !Item.ForeignStudent %>">
								<aspire:HyperLink Target="_blank" runat="server" Text="Admission Processing Fee Challan(s)" NavigateUrl="<%# Aspire.Web.Sys.Staff.Admissions.Reports.AdmissionProcessingFeeChallanReport.GetPageUrl(Item.CandidateAppliedProgramID) %>" />
							</li>
							<li runat="server" visible="<%# !Item.ForeignStudent && Item.ApplicationNo != null %>">
								<aspire:HyperLink Target="_blank" runat="server" Text="Admit Slip(s)" NavigateUrl="<%# Aspire.Web.Sys.Staff.Admissions.Reports.AdmitSlipsReport.GetPageUrl(Item.CandidateID, Item.CandidateAppliedProgramID) %>" />
							</li>
							<li runat="server">
								<aspire:HyperLink Target="_blank" runat="server" Text="Edit Profile Information" NavigateUrl="<%# CandidateProfile.GetPageUrl(Item.CandidateID) %>" />
							</li>
						</ul>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="Semester" HeaderText="Semester" SortExpression="SemesterID" />
			<asp:BoundField DataField="FullChallanNo" HeaderText="Challan No." SortExpression="ChallanNo" />
			<asp:BoundField DataField="ApplicationNo" HeaderText="Application No." SortExpression="ApplicationNo" />
			<asp:BoundField DataField="Enrollment" HeaderText="Enrollment" SortExpression="Enrollment" />
			<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
			<asp:BoundField DataField="FatherName" HeaderText="Father Name" SortExpression="FatherName" />
			<asp:BoundField DataField="ProgramAlias" HeaderText="Program" SortExpression="ProgramAlias" />
			<asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
			<asp:BoundField DataField="Nationality" HeaderText="Nationality" SortExpression="Nationality" />
			<asp:BoundField DataField="InstituteAlias" HeaderText="Institute" SortExpression="InstituteAlias" />
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
