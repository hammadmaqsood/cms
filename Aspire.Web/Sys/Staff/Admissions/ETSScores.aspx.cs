﻿using Aspire.BL.Core.Admissions.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Admissions
{
	[AspirePage("E839A2DE-21D8-4839-9068-3662E02C8D94", UserTypes.Staff, "~/Sys/Staff/Admissions/ETSScores.aspx", "ETS Score", true, AspireModules.Admissions)]
	public partial class ETSScores : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Admissions.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_edit);
		public override string PageTitle => "ETS Score";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterSearch.FillSemesters();
				this.ddlETSType.FillETSTypes();
				this.divETSInfo.Visible = false;
			}
		}

		protected void btnSearchCandidate_OnClick(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterSearch.SelectedItem.Value.ToShort();
			var applicationNo = this.tbApplicationNoSearch.Text.ToInt();
			var etsInformation = BL.Core.Admissions.Staff.ETSScorers.GetCandidateETSScore(semesterID, applicationNo, this.StaffIdentity.LoginSessionGuid);
			if (etsInformation == null)
			{
				this.AddNoRecordFoundAlert();
				this.tbApplicationNo.Text = null;
				this.tbSemester.Text = null;
				this.tbName.Text = null;
				this.tbProgram.Text = null;
				this.ddlETSType.ClearSelection();
				this.tbETSObtained.Text = null;
				this.tbETSTotal.Text = null;
				this.divETSInfo.Visible = false;
				this.btnSave.Visible = false;
				this.btnDelete.Visible = false;
				this.ViewState["CandidateAppliedProgramID"] = null;
				return;
			}
			this.divETSInfo.Visible = true;
			this.ViewState["CandidateAppliedProgramID"] = etsInformation.CandidateAppliedProgramID;
			this.tbApplicationNo.Text = etsInformation.ApplicationNo.ToString();
			this.tbSemester.Text = etsInformation.SemesterID.ToSemesterString();
			this.tbName.Text = etsInformation.Name;
			this.tbProgram.Text = etsInformation.ProgramAlias;
			this.btnSave.Visible = true;
			this.btnDelete.Visible = true;
			if (etsInformation.EnrollmentGenerated != null)
			{
				this.AddWarningAlert("Enrollment has been generated.");
				this.btnSave.Visible = false;
				this.btnDelete.Visible = false;
			}
			if (etsInformation.ETSType != null)
			{
				this.ddlETSType.SelectedValue = etsInformation.ETSType.ToString();
				this.tbETSObtained.Text = etsInformation.ETSObtained.ToString();
				this.tbETSTotal.Text = etsInformation.ETSTotal.ToString();
			}
			else
			{
				this.ddlETSType.ClearSelection();
				this.tbETSObtained.Text = null;
				this.tbETSTotal.Text = null;
				this.btnDelete.Visible = false;
			}
		}

		protected void btnSave_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			this.StaffIdentity.Demand(StaffPermissions.Admissions.ManageETSScore, UserGroupPermission.PermissionValues.Allowed,UserGroupPermission.PermissionValues.Special);
			var candidateAppliedProgramID = (int)this.ViewState["CandidateAppliedProgramID"];
			var etsTypeEnum = this.ddlETSType.GetSelectedEnumValue<ETSTypes>();
			var etsObtained = this.tbETSObtained.Text.ToDouble();
			var etsTotal = this.tbETSTotal.Text.ToDouble();
			var status = BL.Core.Admissions.Staff.ETSScorers.UpdateETSScore(candidateAppliedProgramID, etsTypeEnum, etsObtained, etsTotal, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case ETSScorers.UpdateETSScoreStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<ETSScores>();
					break;
				case ETSScorers.UpdateETSScoreStatuses.EnrollmentGenerated:
					this.AddErrorAlert("ETS Score cannot be updated because Enrollment has been generated.");
					break;
				case ETSScorers.UpdateETSScoreStatuses.EntryTestMarksEntered:
					this.AddErrorAlert("ETS Score cannot be updated because entry test marks already exists.");
					break;
				case ETSScorers.UpdateETSScoreStatuses.Success:
					this.AddSuccessMessageHasBeenUpdated("ETS Score");
					Redirect<ETSScores>();
					break;
				case ETSScorers.UpdateETSScoreStatuses.EntryTestScoreExists:
					this.AddErrorAlert("Cannot update because entry test score exists for the selected program, also it can cause changes in Merit List.");
					break;
				case ETSScorers.UpdateETSScoreStatuses.EntryTestEndDatePassed:
					this.AddErrorAlert("Cannot update because entry tests have been conducted, also it can cause changes in Merit List.");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void btnDelete_OnClick(object sender, EventArgs e)
		{
			var candidateAppliedProgramID = (int)this.ViewState["CandidateAppliedProgramID"];
			var status = BL.Core.Admissions.Staff.ETSScorers.UpdateETSScore(candidateAppliedProgramID, null, null, null, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case ETSScorers.UpdateETSScoreStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<ETSScores>();
					break;
				case ETSScorers.UpdateETSScoreStatuses.EnrollmentGenerated:
					this.AddErrorAlert("ETS Score cannot be deleted because Enrollment has been generated.");
					break;
				case ETSScorers.UpdateETSScoreStatuses.EntryTestMarksEntered:
					this.AddErrorAlert("ETS Score cannot be deleted because entry test marks already exists.");
					break;
				case ETSScorers.UpdateETSScoreStatuses.Success:
					this.AddSuccessMessageHasBeenDeleted("ETS Score");
					Redirect<ETSScores>();
					break;
				case ETSScorers.UpdateETSScoreStatuses.EntryTestScoreExists:
					this.AddErrorAlert("Cannot update because entry test score exists for the selected program, also it can cause changes in Merit List.");
					break;
				case ETSScorers.UpdateETSScoreStatuses.EntryTestEndDatePassed:
					this.AddErrorAlert("Cannot update because entry tests have been conducted, also it can cause changes in Merit List.");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}