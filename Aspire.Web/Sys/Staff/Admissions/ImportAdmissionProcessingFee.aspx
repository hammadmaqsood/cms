﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ImportAdmissionProcessingFee.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.ImportAdmissionProcessingFee" %>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyPH" runat="server">
	<asp:Panel runat="server" ID="panelFileUpload" class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Excel File:" AssociatedControlID="fileUpload" />
			<asp:FileUpload runat="server" ID="fileUpload" CssClass="form-control" ValidationGroup="file" />
			<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="file" ControlToValidate="fileUpload" ErrorMessage="This field is required." />
		</div>
		<div class="form-group">
			<aspire:AspireButton runat="server" Text="Load" ButtonType="Primary" ID="btnUploadFile" ValidationGroup="file" OnClick="btnUploadFile_Click" />
		</div>
	</asp:Panel>
	<asp:Panel runat="server" ID="panelGrids">
		<p></p>
		<asp:Panel runat="server" ID="panelSettings">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Sheet:" AssociatedControlID="ddlSheet" />
					<aspire:AspireDropDownList runat="server" ID="ddlSheet" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlSheet_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Skip Top Rows:" AssociatedControlID="ddlSkipRows" />
					<aspire:AspireDropDownList runat="server" ID="ddlSkipRows" CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="ddlSkipRows_OnSelectedIndexChanged" />
				</div>
			</div>
			<p></p>
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Challan No.:" AssociatedControlID="ddlChallanNo" />
					<aspire:AspireDropDownList runat="server" ID="ddlChallanNo" AutoPostBack="True" OnSelectedIndexChanged="ddlChallanNo_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="ddlName" />
					<aspire:AspireDropDownList runat="server" ID="ddlName" AutoPostBack="True" OnSelectedIndexChanged="ddlName_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgram" />
					<aspire:AspireDropDownList runat="server" ID="ddlProgram" AutoPostBack="True" OnSelectedIndexChanged="ddlProgram_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstitute" />
					<aspire:AspireDropDownList runat="server" ID="ddlInstitute" AutoPostBack="True" OnSelectedIndexChanged="ddlInstitute_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Deposit Date:" AssociatedControlID="ddlDepositDate" />
					<aspire:AspireDropDownList runat="server" ID="ddlDepositDate" AutoPostBack="True" OnSelectedIndexChanged="ddlDepositDate_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Branch Code:" AssociatedControlID="ddlBranchCode" />
					<aspire:AspireDropDownList runat="server" ID="ddlBranchCode" AutoPostBack="True" OnSelectedIndexChanged="ddlBranchCode_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Challan Type:" AssociatedControlID="ddlChallanType" />
					<aspire:AspireDropDownList runat="server" ID="ddlChallanType" AutoPostBack="True" OnSelectedIndexChanged="ddlChallanType_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Amount:" AssociatedControlID="ddlAmount" />
					<aspire:AspireDropDownList runat="server" ID="ddlAmount" AutoPostBack="True" OnSelectedIndexChanged="ddlAmount_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Bank:" AssociatedControlID="ddlInstituteBankAccountID" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Compare" ID="ddlInstituteBankAccountID" />
					<aspire:AspireRequiredFieldValidator ControlToValidate="ddlInstituteBankAccountID" runat="server" ErrorMessage="This field is required." ValidationGroup="Compare" />
				</div>
				<div class="form-group">
					<aspire:AspireButton runat="server" ValidationGroup="Compare" Text="Display Comparison" ButtonType="Primary" ID="btnDisplayComparison" OnClick="btnDisplayComparison_OnClick" />
				</div>
				<div class="form-group">
					<aspire:AspireButton runat="server" Text="Cancel" ButtonType="Primary" ID="btnCancel" OnClick="btnCancel_OnClick" />
				</div>
			</div>
		</asp:Panel>
		<p></p>
		<aspire:AspireGridView runat="server" ID="gvExcelFile" AutoGenerateColumns="false" AllowSorting="False">
			<Columns>
				<asp:TemplateField HeaderText="Challan No.">
					<ItemTemplate>
						<asp:Label runat="server" Text='<%# Eval(ColumnNameChallanNo) %>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Name/Father Name">
					<ItemTemplate>
						<asp:Label runat="server" Text='<%# Eval(ColumnNameName) %>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Program">
					<ItemTemplate>
						<asp:Label runat="server" Text='<%# Eval(ColumnNameProgram) %>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Institute">
					<ItemTemplate>
						<asp:Label runat="server" Text='<%# Eval(ColumnNameInstitute) %>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Deposit Date">
					<ItemTemplate>
						<asp:Label runat="server" Text='<%# Eval(ColumnNameDepositDate,"{0:D}") %>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Branch Code">
					<ItemTemplate>
						<asp:Label runat="server" Text='<%# Eval(ColumnNameBranchCode) %>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Challan Type">
					<ItemTemplate>
						<asp:Label runat="server" Text='<%# Eval(ColumnNameChallanType) %>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Amount">
					<ItemTemplate>
						<asp:Label runat="server" Text='<%# Eval(ColumnNameAmount) %>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
		<asp:ListView runat="server" ID="lvMatchRecord" DataKeyNames='<%# new[] {ColumnNameChallanNo, ColumnNameBranchCode, ColumnNameDepositDate} %>'>
			<LayoutTemplate>
				<div class="table-responsive">
					<table id="itemPlaceholderContainer" class="table table-hover table-bordered">
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th>Challan No.</th>
								<th>Name/Father Name</th>
								<th>Program</th>
								<th>Institute</th>
								<th>Deposit Date</th>
								<th>Branch Code</th>
								<th>Challan Type</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr id="itemPlaceholder" runat="server" />
						</tbody>
					</table>
				</div>
			</LayoutTemplate>
			<ItemTemplate>
				<tr>
					<td rowspan="2">
						<asp:CheckBox runat="server" ID="cb" Visible='<%# Eval(DBColumnNameFullChallanNo)!=DBNull.Value && Eval(DBColumnNameDepositDate)==DBNull.Value %>' />
						<asp:HiddenField runat="server" ID="hf" Value='<%# Eval(ColumnNameChallanNo) %>' />
					</td>
					<td>Excel</td>
					<td rowspan="2" style="vertical-align: middle">
						<asp:Label runat="server" Text='<%# Eval(ColumnNameChallanNo) %>' />
					</td>
					<td class='<%# GetClassName(Eval(ColumnNameName), Eval(DBColumnNameName)) %>'>
						<asp:Label runat="server" Text='<%# Eval(ColumnNameName) %>' />
					</td>
					<td class='<%# GetClassName(Eval(ColumnNameProgram), Eval(DBColumnNameProgram)) %>'>
						<asp:Label runat="server" Text='<%# Eval(ColumnNameProgram) %>' />
					</td>
					<td class='<%# GetClassName(Eval(ColumnNameInstitute), Eval(DBColumnNameInstitute)) %>'>
						<asp:Label runat="server" Text='<%# Eval(ColumnNameInstitute) %>' />
					</td>
					<td class='<%# GetClassName(Eval(ColumnNameDepositDate), Eval(DBColumnNameDepositDate)) %>'>
						<asp:Label runat="server" Text='<%# Eval(ColumnNameDepositDate) %>' />
					</td>
					<td class='<%# GetClassName(Eval(ColumnNameBranchCode), Eval(DBColumnNameBranchCode)) %>'>
						<asp:Label runat="server" Text='<%# Eval(ColumnNameBranchCode) %>' />
					</td>
					<td class='<%# GetClassName(Eval(ColumnNameChallanType), Eval(DBColumnNameChallanType)) %>'>
						<asp:Label runat="server" Text='<%# Eval(ColumnNameChallanType) %>' />
					</td>
					<td class='<%# GetClassName(Eval(ColumnNameAmount), Eval(DBColumnNameAmount)) %>'>
						<asp:Label runat="server" Text='<%# Eval(ColumnNameAmount) %>' />
					</td>
				</tr>
				<tr>
					<td>Database</td>
					<td class='<%# GetClassName(Eval(ColumnNameName), Eval(DBColumnNameName)) %>'>
						<asp:Label runat="server" Text='<%# Eval(DBColumnNameName) %>' />
					</td>
					<td class='<%# GetClassName(Eval(ColumnNameProgram), Eval(DBColumnNameProgram)) %>'>
						<asp:Label runat="server" Text='<%# Eval(DBColumnNameProgram) %>' />
					</td>
					<td class='<%# GetClassName(Eval(ColumnNameInstitute), Eval(DBColumnNameInstitute)) %>'>
						<asp:Label runat="server" Text='<%# Eval(DBColumnNameInstitute) %>' />
					</td>
					<td class='<%# GetClassName(Eval(ColumnNameDepositDate), Eval(DBColumnNameDepositDate)) %>'>
						<asp:Label runat="server" Text='<%# Eval(DBColumnNameDepositDate) %>' />
					</td>
					<td class='<%# GetClassName(Eval(ColumnNameBranchCode), Eval(DBColumnNameBranchCode)) %>'>
						<asp:Label runat="server" Text='<%# Eval(DBColumnNameBranchCode) %>' />
					</td>
					<td class='<%# GetClassName(Eval(ColumnNameChallanType), Eval(DBColumnNameChallanType)) %>'>
						<asp:Label runat="server" Text='<%# Eval(DBColumnNameChallanType) %>' />
					</td>
					<td class='<%# GetClassName(Eval(ColumnNameAmount), Eval(DBColumnNameAmount)) %>'>
						<asp:Label runat="server" Text='<%# Eval(DBColumnNameAmount) %>' />
					</td>
				</tr>
			</ItemTemplate>
		</asp:ListView>
		<asp:Panel class="text-center" runat="server" ID="panelSaveButton">
			<p></p>
			<aspire:AspireButton runat="server" Text="Save" ButtonType="Primary" ID="btnSave" OnClick="btnSave_OnClick" />
			<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="ImportAdmissionProcessingFee.aspx" />
		</asp:Panel>
	</asp:Panel>
</asp:Content>
