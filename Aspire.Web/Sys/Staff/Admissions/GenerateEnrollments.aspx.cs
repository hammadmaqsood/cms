﻿using Aspire.BL.Core.Admissions.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Sys.Staff.Admissions.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Admissions
{
	[AspirePage("A121DE0B-60D6-483A-9D82-9CC19043DF50", UserTypes.Staff, "~/Sys/Staff/Admissions/GenerateEnrollments.aspx", "Generate Enrollments", true, AspireModules.Admissions)]
	public partial class GenerateEnrollments : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Admissions.Module, new []{UserGroupPermission.PermissionValues.Allowed }}
		};
		public override bool BreadcrumbVisible => false;
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_user_plus);
		public override string PageTitle => "Generate Enrollments";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.FillOrderby();
				this.ddlIntakeSemester.FillSemesters();
				this.ddlDeferred.Items.Clear();
				this.ddlDeferred.Items.Add(new ListItem("No", "0"));
				this.ddlDeferred.Items.Add(new ListItem("Deferred Before Enrollment Generation", "1"));
				this.ddlDeferred.Items.Add(new ListItem("Deferred After Enrollment Generation", "2"));
				this.ddlShift.FillShifts();
				this.ddlIntakeSemester_OnSelectedIndexChanged(null, null);
			}
		}

		private void FillOrderby()
		{
			this.ddlOrderby.Items.Add(new ListItem("Application No", "ApplicationNo"));
			this.ddlOrderby.Items.Add(new ListItem("Name", "Name") { Selected = true });
		}

		protected void ddlIntakeSemester_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var admissionOpenPrograms = BL.Core.Common.Lists.GetAdmissionOpenPrograms(this.ddlIntakeSemester.SelectedValue.ToShort(), this.StaffIdentity.InstituteID);
			this.ddlAdmissionOpenProgramID.DataBind(admissionOpenPrograms);
			this.ddlDeferred_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDeferred_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlAdmissionOpenProgramID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlAdmissionOpenProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlShift_OnSelectedIndexChanged(null, null);
		}

		protected void ddlShift_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlOrderby_OnSelectedIndexChanged(null, null);
		}

		protected void ddlOrderby_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ViewState.SetSortExpression(this.ddlOrderby.SelectedValue);
			this.GetCandidatesForEnrollmentGeneration();
		}

		private void GetCandidatesForEnrollmentGeneration()
		{
			var deferredValue = this.ddlDeferred.SelectedValue.ToInt();
			var deferred = deferredValue != 0;
			var deferredAfterEnrollmentGeneration = deferredValue == 2;
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>();
			var admissionOpenProgramID = this.ddlAdmissionOpenProgramID.SelectedValue.ToNullableInt();
			if (admissionOpenProgramID == null)
			{
				this.tbEnrollmentNoFormat.Text = null;
				this.tbLastEnrollmentNo.Text = null;
				this.hfLastRollNo.Value = null;
				this.gvEnrollments.DataBind(null);
				this.btnGenerate.Visible = false;
				this.btnViewReport.Visible = false;
				this.btnViewReport.NavigateUrl = null;
			}
			else
			{
				this.btnViewReport.NavigateUrl = GeneratedEnrollmentsReport.GetPageUrl(admissionOpenProgramID.Value);
				this.btnViewReport.Visible = true;
				var candidate = EnrollmentGeneration.GetCandidatesForEnrollmentGeneration(deferred, deferredAfterEnrollmentGeneration, admissionOpenProgramID.Value, shiftEnum, this.ViewState.GetSortExpression(), out var enrollmentFormat, out var maxEnrollmentGenerated, this.StaffIdentity.LoginSessionGuid);
				if (deferredAfterEnrollmentGeneration)
					this.gvEnrollments.DataKeyNames = new[] { "StudentID" };
				else
					this.gvEnrollments.DataKeyNames = new[] { "CandidateAppliedProgramID" };
				this.gvEnrollments.Columns[1].Visible = deferredAfterEnrollmentGeneration;//Enrollment Column
				this.gvEnrollments.Columns[2].Visible = !deferredAfterEnrollmentGeneration;//ApplicationNo Column
				this.gvEnrollments.DataBind(candidate, this.ViewState.GetSortExpression(), SortDirection.Ascending);

				if (string.IsNullOrWhiteSpace(enrollmentFormat))
					foreach (GridViewRow gridViewRow in this.gvEnrollments.Rows)
					{
						switch (gridViewRow.RowType)
						{
							case DataControlRowType.Header:
								((System.Web.UI.WebControls.CheckBox)gridViewRow.FindControl("cbSelectAll")).Enabled = false;
								((System.Web.UI.WebControls.CheckBox)gridViewRow.FindControl("cbSelectAll")).Checked = false;
								break;
							case DataControlRowType.DataRow:
								((System.Web.UI.WebControls.CheckBox)gridViewRow.FindControl("cb")).Enabled = false;
								((System.Web.UI.WebControls.CheckBox)gridViewRow.FindControl("cb")).Checked = false;
								break;
							case DataControlRowType.Footer:
							case DataControlRowType.Separator:
							case DataControlRowType.Pager:
							case DataControlRowType.EmptyDataRow:
								break;
							default:
								throw new NotImplementedEnumException(gridViewRow.RowType);
						}
					}

				this.tbEnrollmentNoFormat.Text = enrollmentFormat;
				this.tbLastEnrollmentNo.Text = maxEnrollmentGenerated;
				if (string.IsNullOrWhiteSpace(maxEnrollmentGenerated))
					this.hfLastRollNo.Value = 0.ToString();
				else
					this.hfLastRollNo.Value = maxEnrollmentGenerated.Replace(enrollmentFormat, "");
				this.btnGenerate.Visible = candidate.Any();
			}
		}

		protected void btnGenerate_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			this.StaffIdentity.Demand(StaffPermissions.Admissions.GenerateEnrollments, UserGroupPermission.PermissionValues.Allowed);
			var candidateAppliedProgramIDs = new List<int>();
			foreach (GridViewRow row in this.gvEnrollments.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow)
					continue;
				var cb = (System.Web.UI.WebControls.CheckBox)row.FindControl("cb");
				if (cb.Checked && cb.Visible)
					candidateAppliedProgramIDs.Add((int)this.gvEnrollments.DataKeys[row.DataItemIndex].Value);
			}
			if (!candidateAppliedProgramIDs.Any())
			{
				this.AddWarningAlert("No record selected.");
				return;
			}
			var deferredValue = this.ddlDeferred.SelectedValue.ToInt();
			var deferred = deferredValue != 0;
			var deferredAfterEnrollmentGeneration = deferredValue == 2;
			var result = EnrollmentGeneration.GenerateEnrollments(deferred, deferredAfterEnrollmentGeneration, candidateAppliedProgramIDs, this.StaffIdentity.LoginSessionGuid);
			var statuses = result.Select(r => r.Value).Distinct();
			foreach (var status in statuses)
			{
				var count = result.Count(r => r.Value == status);
				if (count > 0)
					switch (status)
					{
						case EnrollmentGeneration.EnrollmentGenerationStatus.Success:
							this.AddSuccessAlert($"{count} enrollments generated.");
							break;
						case EnrollmentGeneration.EnrollmentGenerationStatus.SuccessWithPictureMigrationFailed:
							this.AddWarningAlert($"{count} enrollments generated but Student Picture migration failed.");
							break;
						case EnrollmentGeneration.EnrollmentGenerationStatus.InterviewStatusNotFound:
							this.AddErrorAlert($"{count} enrollments generation failed. Reason: Interview record not found.");
							break;
						case EnrollmentGeneration.EnrollmentGenerationStatus.EnrollmentAlreadyGenerated:
							this.AddErrorAlert($"{count} enrollments generation failed. Reason: Enrollment No. already generated.");
							break;
						case EnrollmentGeneration.EnrollmentGenerationStatus.RollNoMustBeThreeDigitLong:
							this.AddErrorAlert($"{count} enrollments generation failed. Reason: Enrollment No. exceeded from 999.");
							break;
						case EnrollmentGeneration.EnrollmentGenerationStatus.EnrollmentAlreadyExists:
							this.AddErrorAlert($"{count} enrollments generation failed. Reason: Assigned Enrollment No. is already been generated.");
							break;
						case EnrollmentGeneration.EnrollmentGenerationStatus.AdmissionFeeNotPaid:
							this.AddErrorAlert($"{count} enrollments generation failed. Reason: Admission Fee is not paid.");
							break;
						case EnrollmentGeneration.EnrollmentGenerationStatus.Deferred:
							this.AddErrorAlert($"{count} enrollments generation failed. Reason: Deferred.");
							break;
						case EnrollmentGeneration.EnrollmentGenerationStatus.NotDeferred:
							this.AddErrorAlert($"{count} enrollments generation failed. Reason: Not Deferred.");
							break;
						case EnrollmentGeneration.EnrollmentGenerationStatus.NoRecordFound:
							this.AddErrorAlert($"{count} enrollments generation failed. Reason: No record found.");
							break;
						case EnrollmentGeneration.EnrollmentGenerationStatus.NewEnrollmentIsNotValid:
							this.AddErrorAlert($"{count} enrollments generation failed. Reason: Enrollment format is not valid.");
							break;
						default:
							throw new NotImplementedEnumException(status);
					}
			}
			this.GetCandidatesForEnrollmentGeneration();
		}
	}
}