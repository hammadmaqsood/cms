﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CandidateInterview.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Admissions.CandidateInterview" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<style type="text/css">
		.program-addon {
			width: 100% !important;
			text-align: left;
		}

		.program-addon-control {
			width: 100% !important;
			padding: 0 !important;
		}

			.program-addon-control .form-control {
				border: 0 !important;
				border-radius: 0 !important;
			}

		.shift-addon {
			width: auto !important;
			padding: 0 !important;
		}

			.shift-addon > .form-control {
				border: 0 !important;
				width: auto !important;
			}
	</style>
	<div class="row">
		<div class="col-md-12">
			<asp:Panel runat="server" class="form-inline" DefaultButton="btnSearch">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" CausesValidation="False" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Application No.:" AssociatedControlID="tbApplicationNo" />
					<div class="input-group">
						<aspire:AspireTextBox runat="server" ID="tbApplicationNo" ValidationGroup="search" />
						<div class="input-group-btn">
							<aspire:AspireButton runat="server" Glyphicon="search" ButtonType="Primary" ID="btnSearch" OnClick="btnSearch_OnClick" ValidationGroup="search" />
						</div>
					</div>
					<aspire:AspireInt32Validator runat="server" AllowNull="False" ValidationGroup="search" ControlToValidate="tbApplicationNo" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid Number" />
				</div>
				<div class="form-group">
					<aspire:AspireHyperLink runat="server" NavigateUrl="CandidateInterview.aspx" Text="Cancel" />
				</div>
			</asp:Panel>
		</div>
	</div>
	<asp:Panel ID="panelInterview" runat="server" DefaultButton="btnSave">
		<p></p>
		<div class="form-horizontal">
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Application No.:" AssociatedControlID="lblApplicationNo" />
				<div class="col-md-6 form-control-static">
					<aspire:Label runat="server" ID="lblApplicationNo" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Semester:" AssociatedControlID="lblSemester" />
				<div class="col-md-6 form-control-static">
					<aspire:Label runat="server" ID="lblSemester" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Name:" AssociatedControlID="hlCandidateName" />
				<div class="col-md-6 form-control-static">
					<aspire:AspireHyperLink runat="server" ID="hlCandidateName" Target="_blank" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Father Name:" AssociatedControlID="lblCandidateFatherName" />
				<div class="col-md-6 form-control-static">
					<aspire:Label runat="server" ID="lblCandidateFatherName" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="CNIC:" AssociatedControlID="lblCandidateCNIC" />
				<div class="col-md-6 form-control-static">
					<aspire:Label runat="server" ID="lblCandidateCNIC" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Category:" AssociatedControlID="lblCategory" />
				<div class="col-md-6 form-control-static">
					<aspire:Label runat="server" ID="lblCategory" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Shift:" AssociatedControlID="lblShift" />
				<div class="col-md-1 form-control-static">
					<aspire:Label runat="server" ID="lblShift" />
				</div>
			</div>
			<div id="divNone" runat="server" class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Program:" AssociatedControlID="rbProgramChoiceNone" />
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon">
							<aspire:AspireRadioButton GroupName="Program" runat="server" ID="rbProgramChoiceNone" Text="" AutoPostBack="True" OnCheckedChanged="rbProgramChoiceNone_OnCheckedChanged" /><%----%>
						</span>
						<div class="input-group-addon program-addon">
							<aspire:Label runat="server" ID="Label1" Text="None." />
						</div>
					</div>
				</div>
			</div>
			<div id="divChoice1" runat="server" class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Program (Choice 1):" AssociatedControlID="rbProgramChoice1" />
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon">
							<aspire:AspireRadioButton GroupName="Program" runat="server" ID="rbProgramChoice1" AutoPostBack="True" OnCheckedChanged="rbProgramChoice1_OnCheckedChanged" />
						</span>
						<div class="input-group-addon program-addon">
							<aspire:Label runat="server" ID="lblAdmissionOpenProgram1" />
						</div>
						<div class="input-group-addon shift-addon">
							<aspire:AspireDropDownList runat="server" ID="ddlProgramChoice1Shift" />
						</div>
					</div>
				</div>
			</div>
			<div id="divChoice2" runat="server" class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Program (Choice 2):" AssociatedControlID="rbProgramChoice2" />
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon">
							<aspire:AspireRadioButton GroupName="Program" runat="server" ID="rbProgramChoice2" AutoPostBack="True" OnCheckedChanged="rbProgramChoice2_OnCheckedChanged" />
						</span>
						<div class="input-group-addon program-addon">
							<aspire:Label runat="server" ID="lblAdmissionOpenProgram2" />
						</div>
						<div class="input-group-addon shift-addon">
							<aspire:AspireDropDownList runat="server" ID="ddlProgramChoice2Shift" />
						</div>
					</div>
				</div>
			</div>
			<div id="divChoice3" runat="server" class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Program (Choice 3):" AssociatedControlID="rbProgramChoice3" />
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon">
							<aspire:AspireRadioButton GroupName="Program" runat="server" ID="rbProgramChoice3" AutoPostBack="True" OnCheckedChanged="rbProgramChoice3_OnCheckedChanged" />
						</span>
						<div class="input-group-addon program-addon">
							<aspire:Label runat="server" ID="lblAdmissionOpenProgram3" />
						</div>
						<div class="input-group-addon shift-addon">
							<aspire:AspireDropDownList runat="server" ID="ddlProgramChoice3Shift" />
						</div>
					</div>
				</div>
			</div>
			<div id="divChoice4" runat="server" class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Program (Choice 4):" AssociatedControlID="rbProgramChoice4" />
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon">
							<aspire:AspireRadioButton GroupName="Program" runat="server" ID="rbProgramChoice4" AutoPostBack="True" OnCheckedChanged="rbProgramChoice4_OnCheckedChanged" />
						</span>
						<div class="input-group-addon program-addon">
							<aspire:Label runat="server" ID="lblAdmissionOpenProgram4" />
						</div>
						<div class="input-group-addon shift-addon">
							<aspire:AspireDropDownList runat="server" ID="ddlProgramChoice4Shift" />
						</div>
					</div>
				</div>
			</div>
			<div id="divChoiceAnother" runat="server" class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Another Program:" AssociatedControlID="rbAnotherProgram" />
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon">
							<aspire:AspireRadioButton ID="rbAnotherProgram" GroupName="Program" runat="server" AutoPostBack="True" OnCheckedChanged="rbAnotherProgram_OnCheckedChanged" />
						</span>
						<div class="input-group-addon program-addon-control">
							<aspire:AspireDropDownList runat="server" ID="ddlAnotherProgram" />
						</div>
						<div class="input-group-addon shift-addon">
							<aspire:AspireDropDownList runat="server" ID="ddlAnotherProgramShift" />
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Interview Date:" AssociatedControlID="tbInterviewDate" />
				<div class="col-md-6">
					<aspire:AspireDateTimePickerTextbox runat="server" ID="tbInterviewDate" DisplayMode="ShortDate" ValidationGroup="interview" />
					<aspire:AspireDateTimeValidator ID="dttbDOB" runat="server" ControlToValidate="tbInterviewDate" InvalidDataErrorMessage="Invalid Date." AllowNull="False" RequiredErrorMessage="This field is required." ValidationGroup="interview" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Last Date of Fee Submissions:" AssociatedControlID="tbLastDateOfFeeSubmission" />
				<div class="col-md-6">
					<aspire:AspireDateTimePickerTextbox runat="server" ID="tbLastDateOfFeeSubmission" DisplayMode="ShortDate" ValidationGroup="interview" />
					<aspire:AspireDateTimeValidator ID="dtvtbLastDateOfFeeSubmission" runat="server" ControlToValidate="tbLastDateOfFeeSubmission" InvalidDataErrorMessage="Invalid Date." AllowNull="False" RequiredErrorMessage="This field is required." ValidationGroup="interview" />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Remarks (if any):" AssociatedControlID="tbRemarks" />
				<div class="col-md-6">
					<aspire:AspireTextBox ValidationGroup="interview" runat="server" MaxLength="500" ID="tbRemarks" TextMode="MultiLine" />
					<aspire:AspireRequiredFieldValidator ValidationGroup="interview" runat="server" ID="rfvRemarks" ControlToValidate="tbRemarks" ErrorMessage="This field is required." Visible="False" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<ul class="help-block" style="padding-left: 15px">
						<li>Fee adjustment needs to be done by Accounts Office if program is changed after generation of fee challan.</li>
						<li>For Naval candidates Fee Challan will be generated by Accounts Office after verification of Naval Entitlement.</li>
					</ul>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-md-offset-2">
					<aspire:AspireButton runat="server" ID="btnSave" ButtonType="Primary" Text="Save" ValidationGroup="interview" OnClick="btnSave_OnClick" />
					<aspire:AspireButton runat="server" ID="btnSaveAndGenerateFeeChallan" ButtonType="Primary" Text="Save & Generate Fee Challan" ValidationGroup="interview" OnClick="btnSave_OnClick" />
					<aspire:AspireButton runat="server" CausesValidation="False" ID="btnDelete" ConfirmMessage="Are you sure you want to delete the Interview's record?" ButtonType="Danger" Text="Delete" OnClick="btnDelete_OnClick" />
				</div>
			</div>
		</div>
	</asp:Panel>
</asp:Content>
