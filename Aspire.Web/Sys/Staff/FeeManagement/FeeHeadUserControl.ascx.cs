﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities;
using System;
using System.ComponentModel;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.FeeManagement
{
	public partial class FeeHeadUserControl : System.Web.UI.UserControl
	{
		public class Head
		{
			public int FeeHeadID { get; set; }
			public FeeHead.FeeHeadTypes FeeHeadTypeEnum { get; set; }
			public FeeHead.RuleTypes RuleTypeEnum { get; set; }
			public string HeadName { get; set; }
			public bool Refundable { get; set; }
			public bool Concession { get; set; }
			public bool WithholdingTaxApplicable { get; set; }
			public int? Amount { get; set; }
		}

		[Bindable(true)]
		public object DataSource
		{
			set
			{
				var feeHead = (Head)value;
				if (feeHead == null)
					throw new ArgumentNullException();
				this.FeeHeadTypeEnum = feeHead.FeeHeadTypeEnum;
				var refundable = feeHead.Refundable ? " <em>(Refundable)</em>" : null;
				var withholdingTaxApplicable = feeHead.WithholdingTaxApplicable ? " <em>(<abbr title=\"Withholding Tax\">WHT</abbr> Applicable)</em>" : null;
				switch (feeHead.RuleTypeEnum)
				{
					case FeeHead.RuleTypes.Credit:
						this.lblFeeHeadName.Text = $"+ {feeHead.HeadName}{refundable}{withholdingTaxApplicable}:";
						break;
					case FeeHead.RuleTypes.Debit:
						this.lblFeeHeadName.Text = $"- {feeHead.HeadName}{refundable}{withholdingTaxApplicable}:";
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				if (feeHead.FeeHeadTypeEnum == FeeHead.FeeHeadTypes.TutionFee)
					this.tbTotalAmount.Text = null;
				else
					this.tbTotalAmount.Text = feeHead.Amount?.ToString();
				this.tbTotalAmount.Attributes.Add("RuleType", feeHead.RuleTypeEnum.ToString());
				this.tbTotalAmount.Attributes.Add("FeeHeadType", feeHead.FeeHeadTypeEnum.ToString());
				this.ivtbTotalAmount.Enabled = true;

				this.tbConcessionAmount.Text = null;
				this.tbConcessionAmount.Attributes.Add("RuleType", feeHead.RuleTypeEnum.ToString());
				this.tbConcessionAmount.Enabled = feeHead.Concession;
				this.ivtbConcessionAmount.Enabled = feeHead.Concession;

				this.ivtbGrandTotalAmount.Enabled = true;
				this.cv.Enabled = true;

				this.tbTotalAmount.Attributes.Add("data-inputmask", "'alias':'integer'");
				this.tbConcessionAmount.Attributes.Add("data-inputmask", "'alias':'integer'");

				this.ViewState["FeeHeadID"] = feeHead.FeeHeadID;

				this.tbGrandTotalAmount.Text = ((this.tbTotalAmount.Text.ToNullableInt() ?? 0) - (this.tbConcessionAmount.Text.ToNullableInt() ?? 0)).ToString();
			}
		}

		public bool Header
		{
			set
			{
				if (value == false)
					return;
				this.lblFeeHeadName.Text = "Fee Head";

				this.tbTotalAmount.Text = "Amount";
				this.tbTotalAmount.Font.Bold = true;
				this.tbTotalAmount.ReadOnly = true;
				this.ivtbTotalAmount.Enabled = false;

				this.tbConcessionAmount.Text = "Concession";
				this.tbConcessionAmount.Font.Bold = true;
				this.tbConcessionAmount.ReadOnly = true;
				this.ivtbConcessionAmount.Enabled = false;

				this.tbGrandTotalAmount.Text = "Total";
				this.tbGrandTotalAmount.ReadOnly = true;
				this.tbGrandTotalAmount.Font.Bold = true;
				this.ivtbGrandTotalAmount.Enabled = false;
				this.cv.Enabled = false;
			}
		}

		public bool Footer
		{
			set
			{
				if (value == false)
					return;
				this.lblFeeHeadName.Text = "= Total:";

				this.tbTotalAmount.Text = null;
				this.tbTotalAmount.Font.Bold = true;
				this.tbTotalAmount.ReadOnly = false;
				this.ivtbTotalAmount.AllowNull = false;
				this.ivtbTotalAmount.Enabled = true;

				this.tbConcessionAmount.Text = null;
				this.tbConcessionAmount.Font.Bold = true;
				this.tbConcessionAmount.ReadOnly = false;
				this.ivtbConcessionAmount.AllowNull = false;
				this.ivtbConcessionAmount.Enabled = true;

				this.tbGrandTotalAmount.Text = null;
				this.tbGrandTotalAmount.Font.Bold = true;
				this.tbGrandTotalAmount.ReadOnly = false;
				this.ivtbGrandTotalAmount.AllowNull = false;
				this.ivtbGrandTotalAmount.Enabled = true;
				this.cv.Enabled = true;
			}
		}

		public bool Concession
		{
			set
			{
				if (this.ViewState[nameof(this.FeeHeadID)] != null && this.tbConcessionAmount.Enabled)
					this.tbConcessionAmount.ReadOnly = !value;
			}
		}

		public int? FeeHeadID => (int?)this.ViewState[nameof(this.FeeHeadID)];

		public FeeHead.FeeHeadTypes FeeHeadTypeEnum
		{
			get => (FeeHead.FeeHeadTypes)this.ViewState[nameof(this.FeeHeadTypeEnum)];
			set => this.ViewState[nameof(this.FeeHeadTypeEnum)] = value;
		}

		public int? TotalAmount
		{
			get
			{
				var totalAmount = this.tbTotalAmount.Text.ToNullableInt();
				return (totalAmount ?? 0) == 0 ? null : totalAmount;
			}
		}

		public int? ConcessionAmount
		{
			get
			{
				if (this.tbConcessionAmount.Enabled)
				{
					var concessionAmount = this.tbConcessionAmount.Text.ToNullableInt();
					return (concessionAmount ?? 0) == 0 ? null : concessionAmount;
				}
				return null;
			}
		}

		public int GrandTotalAmount => (this.TotalAmount ?? 0) - (this.ConcessionAmount ?? 0);

		protected void cv_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			args.IsValid = this.GrandTotalAmount == (this.tbGrandTotalAmount.Text.ToNullableInt() ?? 0);
		}
	}
}