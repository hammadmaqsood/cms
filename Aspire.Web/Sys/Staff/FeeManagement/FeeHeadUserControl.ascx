﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeeHeadUserControl.ascx.cs" Inherits="Aspire.Web.Sys.Staff.FeeManagement.FeeHeadUserControl" %>

<div class="form-group form-group-sm FeeChallan">
	<aspire:AspireLabel CssClass="col-xs-4 col-sm-3 col-md-3 col-lg-3" runat="server" ID="lblFeeHeadName" AssociatedControlID="tbTotalAmount" />
	<div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
		<div class="input-group">
			<aspire:AspireTextBox runat="server" CssClass="text-right Amount" ID="tbTotalAmount" MaxLength="7" ValidationGroup="FeeChallan" />
			<span class="input-group-addon"><strong>-</strong></span>
			<aspire:AspireTextBox runat="server" CssClass="text-right Concession" ID="tbConcessionAmount" MaxLength="7" ValidationGroup="FeeChallan" />
			<span class="input-group-addon"><strong>=</strong></span>
			<aspire:AspireTextBox runat="server" CssClass="text-right Total" ID="tbGrandTotalAmount" ValidationGroup="FeeChallan" />
		</div>
	</div>
	<div class="col-xs-2 col-sm-3 col-md-4 col-lg-5 form-control-static">
		<aspire:AspireInt32Validator AllowNull="True" runat="server" ID="ivtbTotalAmount" ControlToValidate="tbTotalAmount" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid number." InvalidRangeErrorMessage="Invalid Amount." ValidationGroup="FeeChallan" />
		<aspire:AspireInt32Validator AllowNull="True" runat="server" ID="ivtbConcessionAmount" ControlToValidate="tbConcessionAmount" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid number." InvalidRangeErrorMessage="Invalid Concession Amount." ValidationGroup="FeeChallan" />
		<aspire:AspireInt32Validator AllowNull="True" runat="server" ID="ivtbGrandTotalAmount" ControlToValidate="tbGrandTotalAmount" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid number." InvalidRangeErrorMessage="Invalid Total Amount." ValidationGroup="FeeChallan" />
		<aspire:AspireCustomValidator runat="server" ID="cv" OnServerValidate="cv_OnServerValidate" ErrorMessage="Invalid Total Amount." ValidateEmptyText="False" ValidationGroup="FeeChallan" />
	</div>
</div>
