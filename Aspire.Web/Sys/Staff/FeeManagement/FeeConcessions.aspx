﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="FeeConcessions.aspx.cs" Inherits="Aspire.Web.Sys.Staff.FeeManagement.FeeConcessions" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<aspire:AspireButton runat="server" Glyphicon="plus" Text="Add" CausesValidation="False" ID="btnAddFeeConcession" OnClick="btnAddFeeConcession_OnClick" />
	<p></p>
	<aspire:AspireGridView runat="server" AllowSorting="True" AutoGenerateColumns="False" ID="gvFeeConcessions" OnRowCommand="gvFeeConcessions_OnRowCommand" OnSorting="gvFeeConcessions_OnSorting" OnPageIndexChanging="gvFeeConcessions_PageIndexChanging" OnPageSizeChanging="gvFeeConcessions_PageSizeChanging">
		<Columns>
			<asp:BoundField DataField="ConcessionName" HeaderText="Concession Name" SortExpression="ConcessionName" />
			<asp:TemplateField HeaderText="Visible" SortExpression="Visible">
				<ItemTemplate>
					<%#: ((bool)Eval("visible")).ToYesNo() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CausesValidation="false" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument='<%# Eval("FeeConcessionTypeID") %>' />
					<aspire:AspireLinkButton runat="server" CausesValidation="false" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%# Eval("FeeConcessionTypeID") %>' ConfirmMessage='<%# Eval("ConcessionName","Are you sure you want to delete concession named \"{0}\"?") %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<aspire:AspireModal runat="server" HeaderText="Add/Edit Fee Concession" ID="modalAddEditFeeConcession">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelModal" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="modalAlert" />
					<asp:Panel runat="server" DefaultButton="btnAddEditModalFeeConcession" class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Name:" CssClass="col-md-2" AssociatedControlID="tbFeeConcessionName" />
							<div class="col-md-10">
								<aspire:AspireTextBox runat="server" ID="tbFeeConcessionName" CausesValidation="false" ValidationGroup="FeeConcession" MaxLength="250" />
								<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="FeeConcession" ControlToValidate="tbFeeConcessionName" ErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Visible:" CssClass="col-md-2" AssociatedControlID="ddlFeeConcessionVisible" />
							<div class="col-md-10">
								<aspire:AspireDropDownList runat="server" ID="ddlFeeConcessionVisible" ValidationGroup="FeeConcession" />
								<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="FeeConcession" ControlToValidate="ddlFeeConcessionVisible" ErrorMessage="This field is required." />
							</div>
						</div>
					</asp:Panel>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ButtonType="Primary" Text="Add/Save" ID="btnAddEditModalFeeConcession" ValidationGroup="FeeConcession" OnClick="btnAddEditModalFeeConcession_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Close</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
