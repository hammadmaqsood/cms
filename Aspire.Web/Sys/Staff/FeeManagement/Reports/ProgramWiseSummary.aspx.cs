﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.FeeManagement.Reports
{
	[AspirePage("64745C5B-65D0-49BE-B994-348E1C3E2440", UserTypes.Staff, "~/Sys/Staff/FeeManagement/Reports/ProgramWiseSummary.aspx", "Program-wise Summary", true, AspireModules.FeeManagement)]
	public partial class ProgramWiseSummary : StaffPage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Program-wise Summary";
		public string ReportName => this.PageTitle;
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.FeeManagement.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters();
				if (this.ddlSemesterIDFilter.Items.Count == 0)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All);
				this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, departmentID, true, CommonListItems.All);
		}

		protected void btnDisplay_OnClick(object sender, EventArgs e)
		{
			this._renderReport = true;
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView.ReportViewer);
			this._renderReport = true;
		}

		private bool _renderReport = false;

		public void RenderReport(ReportViewer reportViewer)
		{
			if (!this._renderReport)
				return;
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var newAdmission = this.ddlNewAdmission.SelectedValue.ToNullableBoolean();
			var depositDateFrom = this.dtpDepositDateFrom.SelectedDate?.Date;
			var depositDateTo = this.dtpDepositDateTo.SelectedDate?.Date;
			var reportDataSet = Aspire.BL.Core.FeeManagement.Staff.Reports.ProgramWiseSummary.GetProgramWiseFeeSummary(semesterID, departmentID, programID, newAdmission, depositDateFrom, depositDateTo, this.StaffIdentity.LoginSessionGuid);
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/FeeManagement/Staff/ProgramWiseSummary.rdlc".MapPath();
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.SummaryRows), reportDataSet.SummaryRows));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}