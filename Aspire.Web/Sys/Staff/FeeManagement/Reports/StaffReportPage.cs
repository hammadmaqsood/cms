﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;using Aspire.Lib.Helpers;using Aspire.Lib.WebControls;using Aspire.Web.Common;using Microsoft.Reporting.WebForms;
namespace Aspire.Web.Sys.Staff.FeeManagement.Reports
{
	public abstract class StaffReportPage : StaffPage, IReportViewer
	{		protected static string GetPageUrl(string pageUrl, BL.Core.FeeManagement.Staff.Reports.FeeReportParameters parameters, string filtersText, ReportFormats reportFormatEnum, bool inline)		{			return pageUrl.AttachQueryParams("P", parameters.ToJsonString(), nameof(FiltersText), filtersText, ReportView.ReportFormat, reportFormatEnum, ReportView.Inline, inline);		}		private BL.Core.FeeManagement.Staff.Reports.FeeReportParameters _queryParameters;

		protected BL.Core.FeeManagement.Staff.Reports.FeeReportParameters QueryParameters		{			get			{				if (this._queryParameters == null)					this._queryParameters = this.GetParameterValue("P")?.FromJsonString<BL.Core.FeeManagement.Staff.Reports.FeeReportParameters>().ConvertDatesToLocalTime();				return this._queryParameters;			}		}		public string FiltersText => this.GetParameterValue(nameof(this.FiltersText));
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public abstract void RenderReport(ReportViewer reportViewer);
		public bool ExportToExcel => true;
		public bool ExportToWord => false;

		public abstract string ReportName { get; }
	}
}
