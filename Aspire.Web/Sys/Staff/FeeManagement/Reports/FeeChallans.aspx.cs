﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Staff.FeeManagement.Reports
{
	[AspirePage("FB74813A-5BDC-429E-A12B-570EA8C9768C", UserTypes.Staff, "~/Sys/Staff/FeeManagement/Reports/FeeChallans.aspx", "Fee Challan(s)", true, AspireModules.FeeManagement)]
	public partial class FeeChallans : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.FeeManagement.Module,new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Fee Challan(s)";

		public string ReportName => "Fee Challan(s)";
		public bool ExportToExcel => false;
		public bool ExportToWord => false;

		private List<int> StudentFeeIDs
		{
			get
			{
				try
				{
					return this.GetParameterValue(nameof(this.StudentFeeIDs)).Split(',').Select(s => s.ToInt()).ToList();
				}
				catch
				{
					return null;
				}
			}
		}

		public static string GetPageUrl(List<int> studentFeeIDs, List<int> studentFeeChallanIDs)
		{
			return GetPageUrl<FeeChallans>().AttachQueryParams(nameof(StudentFeeIDs), string.Join(",", studentFeeIDs), ReportView.ReportFormat, ReportFormats.PDF, ReportView.Inline, true);
		}

		public static string GetPageUrl(int studentFeeID)
		{
			return GetPageUrl(new List<int> { studentFeeID }, null);
		}

		public static void Redirect(List<int> studentFeeIDs, List<int> studentFeeChallanIDs)
		{
			Redirect(GetPageUrl(studentFeeIDs, studentFeeChallanIDs));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.StudentFeeIDs?.Any() != true)
					Redirect<Dashboard>();
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			Sys.Common.Methods.RenderFeeChallansReport<Dashboard>(reportViewer, this.StudentFeeIDs);
		}
	}
}