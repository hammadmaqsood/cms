﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Staff.FeeManagement.Reports
{
	[AspirePage("BC502727-B239-4F63-8BFB-01EBC1AFB348", UserTypes.Staff, "~/Sys/Staff/FeeManagement/Reports/DetailedScrollReport.aspx", "Detailed Scroll Report", true, AspireModules.FeeManagement)]
	public partial class DetailedScrollReport : StaffReportPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.FeeManagement.Module,new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};

		public override string ReportName => this.PageTitle;

		public override string PageTitle => "Detailed Scroll";

		public static string GetPageUrl(BL.Core.FeeManagement.Staff.Reports.FeeReportParameters parameters, string filtersText, ReportFormats reportFormatEnum, bool inline)
		{
			return GetPageUrl(GetPageUrl<DetailedScrollReport>(), parameters, filtersText, reportFormatEnum, inline);
		}

		public static void Redirect(BL.Core.FeeManagement.Staff.Reports.FeeReportParameters parameters, string filtersText, ReportFormats reportFormatEnum, bool inline)
		{
			Redirect(GetPageUrl(parameters, filtersText, reportFormatEnum, inline));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.QueryParameters == null)
				{
					Redirect<SelectReport>();
					return;
				}
			}
		}

		public override void RenderReport(ReportViewer reportViewer)
		{
			var reportFileName = "~/Reports/FeeManagement/Staff/DetailedScroll.rdlc".MapPath();
			var reportDataSet = Aspire.BL.Core.FeeManagement.Staff.Reports.DetailedScroll.GetDetailedScroll(this.QueryParameters, this.FiltersText, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("FeeChallansFullRecordTrue", reportDataSet.FeeChallans
				.OrderBy(c => c.FeeHeadDisplayIndex)
				.ThenBy(c => c.EnrollmentOrApplicationNo)
				.ThenBy(c => c.StudentFeeID).ToList()));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("ReportHeader", new[] { reportDataSet.ReportHeader }));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}