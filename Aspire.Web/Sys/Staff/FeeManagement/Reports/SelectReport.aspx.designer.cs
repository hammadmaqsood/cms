﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Web.Sys.Staff.FeeManagement.Reports
{


	public partial class SelectReport
	{

		/// <summary>
		/// ddlReportTypes control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlReportTypes;

		/// <summary>
		/// divFeeType control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divFeeType;

		/// <summary>
		/// ddlFeeType control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlFeeType;

		/// <summary>
		/// divNewAdmission control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divNewAdmission;

		/// <summary>
		/// rblNewAdmission control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireRadioButtonList rblNewAdmission;

		/// <summary>
		/// divSemesterID control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divSemesterID;

		/// <summary>
		/// ddlSemesterID control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlSemesterID;

		/// <summary>
		/// divDepartmentProgram control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divDepartmentProgram;

		/// <summary>
		/// ddlDepartmentID control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlDepartmentID;

		/// <summary>
		/// divProgramID control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divProgramID;

		/// <summary>
		/// ddlProgramID control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlProgramID;

		/// <summary>
		/// divSemesterNo control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divSemesterNo;

		/// <summary>
		/// ddlSemesterNo control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlSemesterNo;

		/// <summary>
		/// divSection control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divSection;

		/// <summary>
		/// ddlSection control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlSection;

		/// <summary>
		/// divShift control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divShift;

		/// <summary>
		/// ddlShift control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlShift;

		/// <summary>
		/// divCreatedDate control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divCreatedDate;

		/// <summary>
		/// dtpCreatedDateFrom control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDateTimePickerTextbox dtpCreatedDateFrom;

		/// <summary>
		/// dtpCreatedDateTo control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDateTimePickerTextbox dtpCreatedDateTo;

		/// <summary>
		/// divDueDate control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divDueDate;

		/// <summary>
		/// dtpDueDateFrom control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDateTimePickerTextbox dtpDueDateFrom;

		/// <summary>
		/// dtpDueDateTo control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDateTimePickerTextbox dtpDueDateTo;

		/// <summary>
		/// divDepositDate control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divDepositDate;

		/// <summary>
		/// dtpDepositDateFrom control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDateTimePickerTextbox dtpDepositDateFrom;

		/// <summary>
		/// dtvDepositDateFrom control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDateTimeValidator dtvDepositDateFrom;

		/// <summary>
		/// dtpDepositDateTo control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDateTimePickerTextbox dtpDepositDateTo;

		/// <summary>
		/// dtvDepositDateTo control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDateTimeValidator dtvDepositDateTo;

		/// <summary>
		/// divCategory control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divCategory;

		/// <summary>
		/// cblCategories control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireCheckBoxList cblCategories;

		/// <summary>
		/// divCreditHours control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divCreditHours;

		/// <summary>
		/// tbCreditHours control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireTextBox tbCreditHours;

		/// <summary>
		/// ddlCreditHoursComparisonOperator control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlCreditHoursComparisonOperator;

		/// <summary>
		/// divExemptWHT control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divExemptWHT;

		/// <summary>
		/// ddlExemptWHT control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlExemptWHT;

		/// <summary>
		/// divFeeConcessionTypes control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divFeeConcessionTypes;

		/// <summary>
		/// cblFeeConcessionTypeIDs control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireCheckBoxList cblFeeConcessionTypeIDs;

		/// <summary>
		/// divFeeConcessionTypesOperator control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divFeeConcessionTypesOperator;

		/// <summary>
		/// ddlFeeConcessionTypeIDsLogicalOperator control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlFeeConcessionTypeIDsLogicalOperator;

		/// <summary>
		/// divInstituteBankAccountID control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divInstituteBankAccountID;

		/// <summary>
		/// ddlInstituteBankAccountID control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlInstituteBankAccountID;

		/// <summary>
		/// divService control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divService;

		/// <summary>
		/// ddlService control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlService;

		/// <summary>
		/// divStudentFeeChallanStatus control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divStudentFeeChallanStatus;

		/// <summary>
		/// rblStudentFeeChallanStatus control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireRadioButtonList rblStudentFeeChallanStatus;

		/// <summary>
		/// divStudentFeeStatus control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divStudentFeeStatus;

		/// <summary>
		/// rblStudentFeeStatus control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireRadioButtonList rblStudentFeeStatus;

		/// <summary>
		/// divRemarks control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divRemarks;

		/// <summary>
		/// tbRemarks control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireTextBox tbRemarks;

		/// <summary>
		/// divReportFormat control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divReportFormat;

		/// <summary>
		/// rblReportFormat control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireRadioButtonList rblReportFormat;

		/// <summary>
		/// divFeeHeads control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divFeeHeads;

		/// <summary>
		/// cblFeeHeadIDs control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireCheckBoxList cblFeeHeadIDs;

		/// <summary>
		/// btnViewReport control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireButton btnViewReport;

		/// <summary>
		/// hfReportLink control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.HiddenField hfReportLink;
	}
}
