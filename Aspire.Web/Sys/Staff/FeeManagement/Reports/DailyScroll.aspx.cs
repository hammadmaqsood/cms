﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.FeeManagement.Reports
{
	[AspirePage("C749171F-B0FE-4228-A9AD-C9FB86DA3749", UserTypes.Staff, "~/Sys/Staff/FeeManagement/Reports/DailyScroll.aspx", "Daily Scroll", true, AspireModules.FeeManagement)]
	public partial class DailyScroll : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.FeeManagement.Module,new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Daily Scroll";
		public string ReportName => "Daily Scroll";
		public bool ExportToExcel => true;
		public bool ExportToWord => false;

		public void RenderReport(ReportViewer reportViewer)
		{
			if (!this.IsPostBack)
				return;
			var reportFileName = "~/Reports/FeeManagement/Staff/DetailedFeeChallans.rdlc".MapPath();
			//var feeChallans = Aspire.BL.Core.FeeManagement.Staff.Reports.DetailedScroll.GetDetailedFeeChallansReport(this.QueryParameters);

			////if (feeChallans == null || !feeChallans.Any())
			////{
			////	this.AddNoRecordFoundAlert();
			////	Redirect<SelectReport>();
			////	return;
			////}
			//reportViewer.ProcessingMode = ProcessingMode.Local;
			//reportViewer.LocalReport.ReportPath = reportFileName;
			//reportViewer.LocalReport.DisplayName = this.ReportName;
			//reportViewer.LocalReport.DataSources.Add(new ReportDataSource("StudentFeeChallans", feeChallans));
			//reportViewer.LocalReport.EnableExternalImages = true;
		}

		protected void btnDisplay_OnClick(object sender, EventArgs e)
		{
			this.RenderReport(((ReportView)this.Master).ReportViewer);
		}
	}
}