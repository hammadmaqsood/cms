﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.FeeManagement.Reports
{
	[AspirePage("9771DFDD-B5EB-43B6-8D74-22C8851DFD40", UserTypes.Staff, "~/Sys/Staff/FeeManagement/Reports/DailyStatementBankScrollReport.aspx", "Daily Statement Bank Scroll Report", true, AspireModules.FeeManagement)]
	public partial class DailyStatementBankScrollReport : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.FeeManagement.Module,new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};

		public static string GetPageUrl(short? semesterID, DateTime? depositDateFrom, DateTime? depositDateTo, int? instituteBankAccountID, ReportFormats reportFormatEnum, bool inline)
		{
			return GetPageUrl<DailyStatementBankScrollReport>().AttachQueryParams("SemesterID", semesterID, "DepositDateFrom", depositDateFrom, "DepositDateTo", depositDateTo, "InstituteBankAccountID", instituteBankAccountID, ReportView.ReportFormat, reportFormatEnum, ReportView.Inline, inline);
		}

		public static void Redirect(short? semesterID, DateTime? depositDateFrom, DateTime? depositDateTo, int? instituteBankAccountID, ReportFormats reportFormatEnum, bool inline)
		{
			Redirect(GetPageUrl(semesterID, depositDateFrom, depositDateTo, instituteBankAccountID, reportFormatEnum, inline));
		}

		public string ReportName => "Daily Statement Bank Scroll";
		public bool ExportToExcel => true;
		public bool ExportToWord => false;
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => this.ReportName;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			var reportFileName = "~/Reports/FeeManagement/Staff/DailyStatementBankScroll.rdlc".MapPath();
			var semesterID = this.Request.GetParameterValue<short>("SemesterID");
			var depositDateFrom = this.Request.GetParameterValue<DateTime>("DepositDateFrom");
			var depositDateTo = this.Request.GetParameterValue<DateTime>("DepositDateTo");
			var instituteBankAccountID = this.Request.GetParameterValue<int>("InstituteBankAccountID");
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			var reportDataSet = Aspire.BL.Core.FeeManagement.Staff.Reports.DailyStatementBankScroll.GetDailyStatementBankScroll(semesterID, depositDateFrom, depositDateTo, instituteBankAccountID, this.StaffIdentity.LoginSessionGuid);
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("Details", reportDataSet.Details));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("ReportHeader", new[] { reportDataSet.ReportHeader }));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}