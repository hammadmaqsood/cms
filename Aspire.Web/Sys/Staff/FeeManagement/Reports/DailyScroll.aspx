﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="DailyScroll.aspx.cs" Inherits="Aspire.Web.Sys.Staff.FeeManagement.Reports.DailyScroll" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="Parameters">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="AspireLabel1" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="Filter" />
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="Filter" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="AspireLabel2" />
					<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpFrom" ValidationGroup="Filter" />
					<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dtpFrom" AllowNull="True" InvalidDataErrorMessage="Invalid date." RequiredErrorMessage="This field is required." ValidationGroup="Filter" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="AspireLabel3" />
					<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpTo" ValidationGroup="Filter" />
					<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dtpTo" AllowNull="True" InvalidDataErrorMessage="Invalid date." RequiredErrorMessage="This field is required." ValidationGroup="Filter" />
				</div>
				<div class="form-group">
					<aspire:AspireButton runat="server" ID="btnDisplay" ValidationGroup="Filter" Text="Display" CausesValidation="True" OnClick="btnDisplay_OnClick" />
				</div>
			</div>
		</div>
	</div>
</asp:Content>
