﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="SelectReport.aspx.cs" Inherits="Aspire.Web.Sys.Staff.FeeManagement.Reports.SelectReport" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<style type="text/css">
		.form-horizontal .form-group .form-inline .form-group {
			margin-left: 0;
			margin-right: 0;
		}

			.form-horizontal .form-group .form-inline .form-group label {
				vertical-align: baseline;
			}
	</style>
	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Report Types:" AssociatedControlID="ddlReportTypes" CssClass="col-md-2" />
					<div class="col-md-5">
						<aspire:AspireDropDownList runat="server" ID="ddlReportTypes" AutoPostBack="True" OnSelectedIndexChanged="ddlReportTypes_OnSelectedIndexChanged" CausesValidation="False" />
					</div>
				</div>
				<div class="form-group" id="divFeeType" runat="server">
					<aspire:AspireLabel runat="server" Text="Fee Type:" AssociatedControlID="ddlFeeType" CssClass="col-md-2" />
					<div class="col-md-3">
						<aspire:AspireDropDownList runat="server" ID="ddlFeeType" />
					</div>
				</div>
				<div class="form-group" runat="server" id="divNewAdmission">
					<aspire:AspireLabel runat="server" Text="New Admission:" AssociatedControlID="rblNewAdmission" CssClass="col-md-2" />
					<div class="col-md-10">
						<aspire:AspireRadioButtonList runat="server" ID="rblNewAdmission" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="rblNewAdmission_OnSelectedIndexChanged" />
					</div>
				</div>
				<div class="form-group" runat="server" id="divSemesterID">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" CssClass="col-md-2" />
					<div class="col-md-3">
						<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" />
					</div>
				</div>
				<div class="form-group" runat="server" id="divDepartmentProgram">
					<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" CssClass="col-md-2" />
					<div class="col-md-10">
						<div class="form-inline">
							<div class="form-group">
								<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentID_OnSelectedIndexChanged" />
							</div>
							<div class="form-group" runat="server" id="divProgramID">
								<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
								<aspire:AspireDropDownList runat="server" ID="ddlProgramID" />
							</div>
							<div class="form-group" runat="server" id="divSemesterNo">
								<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="ddlSemesterNo" />
								<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" />
							</div>
							<div class="form-group" runat="server" id="divSection">
								<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSection" />
								<aspire:AspireDropDownList runat="server" ID="ddlSection" />
							</div>
							<div class="form-group" runat="server" id="divShift">
								<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShift" />
								<aspire:AspireDropDownList runat="server" ID="ddlShift" />
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" runat="server" id="divCreatedDate">
					<aspire:AspireLabel runat="server" Text="Created Date:" AssociatedControlID="dtpCreatedDateFrom" CssClass="col-md-2" />
					<div class="col-md-10">
						<div class="form-inline">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="From:" AssociatedControlID="dtpCreatedDateFrom" />
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpCreatedDateFrom" />
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="To:" AssociatedControlID="dtpCreatedDateTo" />
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpCreatedDateTo" />
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" runat="server" id="divDueDate">
					<aspire:AspireLabel runat="server" Text="Due Date:" AssociatedControlID="dtpDueDateFrom" CssClass="col-md-2" />
					<div class="col-md-10">
						<div class="form-inline">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="From:" AssociatedControlID="dtpDueDateFrom" />
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpDueDateFrom" />
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="To:" AssociatedControlID="dtpDueDateTo" />
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpDueDateTo" />
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" runat="server" id="divDepositDate">
					<aspire:AspireLabel runat="server" Text="Deposit Date:" AssociatedControlID="dtpDepositDateFrom" CssClass="col-md-2" />
					<div class="col-md-10">
						<div class="form-inline">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="From:" AssociatedControlID="dtpDepositDateFrom" />
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpDepositDateFrom" />
								<aspire:AspireDateTimeValidator runat="server" AllowNull="True" ID="dtvDepositDateFrom" ControlToValidate="dtpDepositDateFrom" ValidationGroup="Filter" InvalidDataErrorMessage="Invalid Date." InvalidRangeErrorMessage="Invalid Date Range." RequiredErrorMessage="This field is required." />
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="To:" AssociatedControlID="dtpDepositDateTo" />
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpDepositDateTo" />
								<aspire:AspireDateTimeValidator runat="server" AllowNull="True" ID="dtvDepositDateTo" ControlToValidate="dtpDepositDateTo" ValidationGroup="Filter" InvalidDataErrorMessage="Invalid Date." InvalidRangeErrorMessage="Invalid Date Range." RequiredErrorMessage="This field is required." />
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" runat="server" id="divCategory">
					<aspire:AspireLabel runat="server" Text="Category:" AssociatedControlID="cblCategories" CssClass="col-md-2" />
					<div class="col-md-10">
						<aspire:AspireCheckBoxList runat="server" ID="cblCategories" RepeatLayout="Flow" RepeatDirection="Horizontal" />
					</div>
				</div>
				<div class="form-group" runat="server" id="divCreditHours">
					<aspire:AspireLabel runat="server" Text="Credit Hours:" AssociatedControlID="tbCreditHours" CssClass="col-md-2" />
					<div class="col-md-10">
						<div class="form-inline">
							<div class="form-group">
								<aspire:AspireTextBox runat="server" ID="tbCreditHours" />
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Comparison Operator:" AssociatedControlID="ddlCreditHoursComparisonOperator" />
								<aspire:AspireDropDownList runat="server" ID="ddlCreditHoursComparisonOperator" />
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" runat="server" id="divExemptWHT">
					<aspire:AspireLabel runat="server" Text="WHT Exempted:" AssociatedControlID="ddlExemptWHT" CssClass="col-md-2" />
					<div class="col-md-3">
						<aspire:AspireDropDownList runat="server" ID="ddlExemptWHT" />
					</div>
				</div>
				<div class="form-group" id="divFeeConcessionTypes" runat="server">
					<aspire:AspireLabel runat="server" Text="Fee Concession Type:" AssociatedControlID="cblFeeConcessionTypeIDs" CssClass="col-md-2" />
					<div class="col-md-10">
						<aspire:AspireCheckBoxList runat="server" ID="cblFeeConcessionTypeIDs" RepeatLayout="Flow" RepeatDirection="Vertical" />
					</div>
				</div>
				<div class="form-group" id="divFeeConcessionTypesOperator" runat="server">
					<aspire:AspireLabel runat="server" Text="Fee Concession Type Logical Operator:" AssociatedControlID="ddlFeeConcessionTypeIDsLogicalOperator" CssClass="col-md-2" />
					<div class="col-md-3">
						<aspire:AspireDropDownList runat="server" ID="ddlFeeConcessionTypeIDsLogicalOperator" />
					</div>
				</div>
				<div class="form-group" runat="server" id="divInstituteBankAccountID">
					<aspire:AspireLabel runat="server" Text="Bank Account:" AssociatedControlID="ddlInstituteBankAccountID" CssClass="col-md-2" />
					<div class="col-md-10">
						<aspire:AspireDropDownList runat="server" ID="ddlInstituteBankAccountID" />
					</div>
				</div>
				<div class="form-group" runat="server" id="divService">
					<aspire:AspireLabel runat="server" Text="Service:" AssociatedControlID="ddlService" CssClass="col-md-2" />
					<div class="col-md-10">
						<aspire:AspireDropDownList runat="server" ID="ddlService" />
					</div>
				</div>
				<div class="form-group" runat="server" id="divStudentFeeChallanStatus">
					<aspire:AspireLabel runat="server" Text="Fee Challan Status:" AssociatedControlID="rblStudentFeeChallanStatus" CssClass="col-md-2" />
					<div class="col-md-10">
						<aspire:AspireRadioButtonList runat="server" ID="rblStudentFeeChallanStatus" RepeatLayout="Flow" RepeatDirection="Horizontal" />
					</div>
				</div>
				<div class="form-group" runat="server" id="divStudentFeeStatus">
					<aspire:AspireLabel runat="server" Text="Student Fee Status:" AssociatedControlID="rblStudentFeeStatus" CssClass="col-md-2" />
					<div class="col-md-10">
						<aspire:AspireRadioButtonList runat="server" ID="rblStudentFeeStatus" RepeatLayout="Flow" RepeatDirection="Horizontal" />
					</div>
				</div>
				<div class="form-group" runat="server" id="divRemarks">
					<aspire:AspireLabel runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" CssClass="col-md-2" />
					<div class="col-md-10">
						<aspire:AspireTextBox runat="server" ID="tbRemarks" MaxLength="1000" />
					</div>
				</div>
				<div class="form-group" runat="server" id="divReportFormat">
					<aspire:AspireLabel runat="server" Text="Report Format:" AssociatedControlID="rblReportFormat" CssClass="col-md-2" />
					<div class="col-md-10">
						<aspire:AspireRadioButtonList runat="server" ID="rblReportFormat" RepeatDirection="Horizontal" RepeatLayout="Flow" />
					</div>
				</div>
				<div class="form-group" id="divFeeHeads" runat="server">
					<aspire:AspireLabel runat="server" Text="Fee Heads:" AssociatedControlID="cblFeeHeadIDs" CssClass="col-md-2" />
					<div class="col-md-10">
						<aspire:AspireCheckBoxList runat="server" ID="cblFeeHeadIDs" RepeatLayout="Flow" RepeatDirection="Vertical" />
					</div>
				</div>
				<div class="form-group" runat="server">
					<div class="col-md-offset-2 col-md-10">
						<aspire:AspireButton runat="server" ValidationGroup="Filter" ID="btnViewReport" Text="View Report" ButtonType="Primary" OnClick="btnViewReport_OnClick" />
						<aspire:AspireHyperLink runat="server" NavigateUrl="~/Sys/Staff/FeeManagement/Reports/SelectReport.aspx" Text="Reset to Default"></aspire:AspireHyperLink>
						<aspire:HiddenField Mode="PlainText" runat="server" ID="hfReportLink" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function () {
			var hfReportLink = $("#<%= this.hfReportLink.ClientID %>");
			var link = hfReportLink.val();
			if (link.length > 0)
				window.open(link);
		});
	</script>
</asp:Content>
