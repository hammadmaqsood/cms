﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.FeeManagement.Reports
{
	[AspirePage("8B1DD588-CE6E-4E76-BA00-80E41B35C069", UserTypes.Staff, "~/Sys/Staff/FeeManagement/Reports/ScholarshipReport.aspx", "Scholarship Report", true, AspireModules.FeeManagement)]
	public partial class ScholarshipReport : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.FeeManagement.Module,new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};

		public static string GetPageUrl(short? semesterID, ReportFormats reportFormatEnum, bool inline)
		{
			return GetPageUrl<ScholarshipReport>().AttachQueryParams("SemesterID", semesterID, ReportView.ReportFormat, reportFormatEnum, ReportView.Inline, inline);
		}

		public static void Redirect(short? semesterID, ReportFormats reportFormatEnum, bool inline)
		{
			Redirect(GetPageUrl(semesterID, reportFormatEnum, inline));
		}

		public string ReportName => "Scholarship Report";
		public bool ExportToExcel => true;
		public bool ExportToWord => false;
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => this.ReportName;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var semesterID = this.Request.GetParameterValue<short>("SemesterID");
				if (semesterID == null)
				{
					Redirect<SelectReport>();
					return;
				}
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			var reportFileName = "~/Reports/FeeManagement/Staff/Scholarship.rdlc".MapPath();
			var semesterID = this.Request.GetParameterValue<short>("SemesterID")?? throw new InvalidOperationException();
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			var reportDataSet = Aspire.BL.Core.FeeManagement.Staff.Reports.Scholarship.GetScholarships(semesterID, this.StaffIdentity.LoginSessionGuid);
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("Details", reportDataSet.Details));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("ReportHeader", new[] { reportDataSet.ReportHeader }));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}