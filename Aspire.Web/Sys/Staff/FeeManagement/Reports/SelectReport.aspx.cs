﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.FeeManagement.Reports
{
	[AspirePage("62B19CBF-5E8B-4B6A-94A0-AA84D080A931", UserTypes.Staff, "~/Sys/Staff/FeeManagement/Reports/SelectReport.aspx", "Select Report", true, AspireModules.FeeManagement)]
	public partial class SelectReport : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.FeeManagement.Module,new [] { UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override AspireThemes AspireTheme => AspireDefaultThemes.Staff;
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.filter);
		public override string PageTitle => "Select Report";

		public enum ReportTypes
		{
			DetailedFeeChallanReport,
			DailyStatementBankScroll,
			Scholarship,
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			this.hfReportLink.Value = null;
			if (!this.IsPostBack)
			{
				this.ResetParameters();
			}
		}

		protected void rblNewAdmission_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var newAdmissions = this.rblNewAdmission.SelectedValue.ToNullableBoolean();
			if (newAdmissions != null)
			{
				var instituteBankAccounts = BL.Common.Common.GetInstituteBankAccounts(this.StaffIdentity.InstituteID, null, newAdmissions.Value ? InstituteBankAccount.FeeTypes.NewAdmissionFee : InstituteBankAccount.FeeTypes.StudentFee, null);
				this.ddlInstituteBankAccountID.DataBind(instituteBankAccounts, CommonListItems.All);
			}
			else
			{
				this.ddlInstituteBankAccountID.Items.Clear();
				this.ddlInstituteBankAccountID.Items.Add(CommonListItems.All);
			}
		}

		protected void ddlDepartmentID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			if (departmentID == null)
				this.ddlProgramID.FillPrograms(this.StaffIdentity.InstituteID, null, false, CommonListItems.All);
			else
				this.ddlProgramID.FillPrograms(this.StaffIdentity.InstituteID, departmentID, false, CommonListItems.All);
		}

		private void ResetParameters()
		{
			this.ddlReportTypes.FillEnums<ReportTypes>();
			this.ddlReportTypes_OnSelectedIndexChanged(null, null);
		}

		private BL.Core.FeeManagement.Staff.Reports.FeeReportParameters GetParameters()
		{
			return new BL.Core.FeeManagement.Staff.Reports.FeeReportParameters
			{
				FeeType = this.ddlFeeType.SelectedValue.ToByte(),
				InstituteID = this.StaffIdentity.InstituteID,
				NewAdmission = this.divNewAdmission.Visible ? this.rblNewAdmission.SelectedValue.ToNullableBoolean() : null,
				SemesterID = this.divSemesterID.Visible ? this.ddlSemesterID.SelectedValue.ToNullableShort() : null,
				DepartmentID = this.divDepartmentProgram.Visible ? this.ddlDepartmentID.SelectedValue.ToNullableInt() : null,
				ProgramID = this.divDepartmentProgram.Visible && this.divProgramID.Visible ? this.ddlProgramID.SelectedValue.ToNullableInt() : null,
				SemesterNo = this.divDepartmentProgram.Visible && this.divSemesterNo.Visible ? this.ddlSemesterNo.SelectedValue.ToNullableShort() : null,
				Section = this.divDepartmentProgram.Visible && this.divSection.Visible ? this.ddlSection.SelectedValue.ToNullableInt() : null,
				Shift = this.divDepartmentProgram.Visible && this.divShift.Visible ? this.ddlShift.SelectedValue.ToNullableByte() : null,
				CreatedFromDate = this.divCreatedDate.Visible ? this.dtpCreatedDateFrom.SelectedDate : null,
				CreatedToDate = this.divCreatedDate.Visible ? this.dtpCreatedDateTo.SelectedDate : null,
				DueFromDate = this.divDueDate.Visible ? this.dtpDueDateFrom.SelectedDate : null,
				DueToDate = this.divDueDate.Visible ? this.dtpDueDateTo.SelectedDate : null,
				TransactionDateFrom = this.divDepositDate.Visible ? this.dtpDepositDateFrom.SelectedDate : null,
				TransactionDateTo = this.divDepositDate.Visible ? this.dtpDepositDateTo.SelectedDate : null,
				Categories = this.divCategory.Visible ? this.cblCategories.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => (byte)i.Value.ToEnum<Categories>()).ToList() : new List<byte>(),
				CreditHours = this.divCreditHours.Visible ? this.tbCreditHours.Text.ToNullableDecimal() : null,
				CreditHoursOperator = this.ddlCreditHoursComparisonOperator.GetSelectedEnumValue<ComparisonOperators>(),
				ExemptWHT = this.divExemptWHT.Visible ? this.ddlExemptWHT.SelectedValue.ToNullableBoolean() : null,
				FeeConcessionTypeIDs = this.divFeeConcessionTypes.Visible ? this.cblFeeConcessionTypeIDs.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value.ToInt()).ToList() : new List<int>(),
				FeeConcessionTypeIDsLogicalOperator = this.ddlFeeConcessionTypeIDsLogicalOperator.GetSelectedEnumValue<LogicalOperators>(),
				InstituteBankAccountID = this.divInstituteBankAccountID.Visible ? this.ddlInstituteBankAccountID.SelectedValue.ToNullableInt() : null,
				Service = this.ddlService.SelectedValue.ToNullableByte(),
				FeeChallanStatus = this.divStudentFeeChallanStatus.Visible ? this.rblStudentFeeChallanStatus.SelectedValue.ToNullableByte() : null,
				StudentFeeStatus = this.divStudentFeeStatus.Visible ? this.rblStudentFeeStatus.SelectedValue.ToNullableByte() : null,
				Remarks = this.divRemarks.Visible ? this.tbRemarks.Text.ToNullIfWhiteSpace() : null,
				FeeHeadIDs = this.divFeeHeads.Visible ? this.cblFeeHeadIDs.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value.ToInt()).ToList() : new List<int>(),
			};
		}

		protected void ddlReportTypes_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var reportType = this.ddlReportTypes.GetSelectedEnumValue<ReportTypes>();
			switch (reportType)
			{
				case ReportTypes.DetailedFeeChallanReport:
					this.divFeeType.Visible = true;
					this.ddlFeeType.FillFeeTypes().SetEnumValue(StudentFee.FeeTypes.Challan);

					this.divNewAdmission.Visible = true;
					this.divInstituteBankAccountID.Visible = true;
					this.rblNewAdmission.FillYesNo(CommonListItems.All).SetSelectedValue(CommonListItems.NoValue);
					this.rblNewAdmission_OnSelectedIndexChanged(null, null);

					this.divSemesterID.Visible = true;
					this.ddlSemesterID.FillSemesters(CommonListItems.All);
					if (this.divSemesterID.Visible && this.ddlSemesterID.Items.Count > 1)
						this.ddlSemesterID.SelectedIndex = 1;

					this.divDepartmentProgram.Visible = true;
					this.ddlDepartmentID.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All);
					this.ddlDepartmentID_OnSelectedIndexChanged(null, null);
					this.ddlSemesterNo.FillSemesterNos(CommonListItems.All);
					this.ddlSection.FillSections(CommonListItems.All);
					this.ddlShift.FillShifts(CommonListItems.All);

					this.divCreatedDate.Visible = true;
					this.dtpCreatedDateFrom.SelectedDate = null;
					this.dtpCreatedDateTo.SelectedDate = null;

					this.divDueDate.Visible = true;
					this.dtpDueDateFrom.SelectedDate = null;
					this.dtpDueDateTo.SelectedDate = null;

					this.divDepositDate.Visible = true;
					this.dtpDepositDateFrom.SelectedDate = null;
					this.dtpDepositDateTo.SelectedDate = null;
					this.dtvDepositDateFrom.AllowNull = true;
					this.dtvDepositDateTo.AllowNull = true;

					this.divCategory.Visible = true;
					this.cblCategories.FillCategories();

					this.divCreditHours.Visible = true;
					this.tbCreditHours.Text = null;
					this.ddlCreditHoursComparisonOperator.FillEnums<ComparisonOperators>();

					this.ddlExemptWHT.FillYesNo(CommonListItems.All);
					this.divExemptWHT.Visible = true;

					this.divFeeConcessionTypes.Visible = true;
					var feeConcessionTypes = BL.Core.FeeManagement.Staff.FeeConcessionTypes.GetFeeConcessionTypesList(null, this.StaffIdentity.LoginSessionGuid);
					this.cblFeeConcessionTypeIDs.DataBind(feeConcessionTypes);
					this.ddlFeeConcessionTypeIDsLogicalOperator.FillEnums<LogicalOperators>();
					this.divFeeConcessionTypes.Visible = this.cblFeeConcessionTypeIDs.Items.Count > 0;
					this.divFeeConcessionTypesOperator.Visible = this.divFeeConcessionTypes.Visible;

					this.divService.Visible = true;
					this.ddlService.FillEnums<AccountTransaction.Services>(CommonListItems.All);

					this.divStudentFeeChallanStatus.Visible = true;
					this.rblStudentFeeChallanStatus.FillStudentFeeChallanStatus(CommonListItems.All).SelectedIndex = 0;

					this.divStudentFeeStatus.Visible = true;
					this.rblStudentFeeStatus.FillEnums<StudentFee.Statuses>(CommonListItems.All).SelectedIndex = 0;

					this.divRemarks.Visible = true;

					this.divFeeHeads.Visible = true;
					var feeHeads = BL.Core.FeeManagement.Staff.FeeHeads.GetFeeHeadsList(this.StaffIdentity.LoginSessionGuid);
					this.cblFeeHeadIDs.DataBind(feeHeads);
					this.cblFeeHeadIDs.SelectAll();

					this.rblReportFormat.FillEnums(ReportFormats.Image).SetEnumValue(ReportFormats.PDF);
					break;
				case ReportTypes.DailyStatementBankScroll:
					this.divFeeType.Visible = true;
					this.ddlFeeType.FillFeeTypes(null, StudentFee.FeeTypes.Attendance, StudentFee.FeeTypes.Refund).SetEnumValue(StudentFee.FeeTypes.Challan);

					this.divNewAdmission.Visible = false;
					this.divInstituteBankAccountID.Visible = false;

					this.divSemesterID.Visible = true;
					this.ddlSemesterID.FillSemesters();

					this.divDepartmentProgram.Visible = false;

					this.divCreatedDate.Visible = false;

					this.divDueDate.Visible = false;

					this.divDepositDate.Visible = true;
					this.dtpDepositDateFrom.SelectedDate = null;
					this.dtpDepositDateTo.SelectedDate = null;
					this.dtvDepositDateFrom.AllowNull = false;
					this.dtvDepositDateTo.AllowNull = false;

					this.divCategory.Visible = false;

					this.divCreditHours.Visible = false;

					this.divExemptWHT.Visible = false;

					this.divFeeConcessionTypes.Visible = false;
					this.divFeeConcessionTypesOperator.Visible = false;

					this.divService.Visible = false;

					this.divStudentFeeChallanStatus.Visible = true;
					this.rblStudentFeeChallanStatus.FillStudentFeeChallanStatus(null, StudentFeeChallan.Statuses.NotPaid).SelectedIndex = 0;

					this.divStudentFeeStatus.Visible = true;
					this.rblStudentFeeStatus.FillStudentFeeStatus(null, StudentFee.Statuses.NotPaid).SelectedIndex = 0;

					this.divRemarks.Visible = false;

					this.divFeeHeads.Visible = false;

					this.rblReportFormat.FillEnums(ReportFormats.Image).SetEnumValue(ReportFormats.PDF);
					break;
				case ReportTypes.Scholarship:
					this.divFeeType.Visible = true;
					this.ddlFeeType.FillFeeTypes(null, StudentFee.FeeTypes.Attendance, StudentFee.FeeTypes.Refund).SetEnumValue(StudentFee.FeeTypes.Challan);

					this.divNewAdmission.Visible = false;
					this.divInstituteBankAccountID.Visible = false;

					this.divSemesterID.Visible = true;
					this.ddlSemesterID.FillSemesters();

					this.divDepartmentProgram.Visible = false;

					this.divCreatedDate.Visible = false;

					this.divDueDate.Visible = false;

					this.divDepositDate.Visible = false;
					this.dtpDepositDateFrom.SelectedDate = null;
					this.dtpDepositDateTo.SelectedDate = null;
					this.dtvDepositDateFrom.AllowNull = false;
					this.dtvDepositDateTo.AllowNull = false;

					this.divCategory.Visible = false;

					this.divCreditHours.Visible = false;

					this.divExemptWHT.Visible = false;

					this.divFeeConcessionTypes.Visible = false;
					this.divFeeConcessionTypesOperator.Visible = false;

					this.divService.Visible = false;

					this.divStudentFeeChallanStatus.Visible = true;
					this.rblStudentFeeChallanStatus.FillStudentFeeChallanStatus(null, StudentFeeChallan.Statuses.NotPaid).SelectedIndex = 0;

					this.divStudentFeeStatus.Visible = true;
					this.rblStudentFeeStatus.FillStudentFeeStatus(null, StudentFee.Statuses.NotPaid).SelectedIndex = 0;

					this.divRemarks.Visible = false;

					this.divFeeHeads.Visible = false;

					this.rblReportFormat.FillEnums(ReportFormats.Image).SetEnumValue(ReportFormats.PDF);
					break;
				default:
					throw new NotImplementedEnumException(reportType);
			}
		}

		protected void btnViewReport_OnClick(object sender, EventArgs e)
		{
			var report = this.ddlReportTypes.GetSelectedEnumValue<ReportTypes>();
			var reportFormat = this.rblReportFormat.GetSelectedEnumValue<ReportFormats>();
			bool inline;
			switch (reportFormat)
			{
				case ReportFormats.PDF:
					inline = true;
					break;
				case ReportFormats.Excel:
				case ReportFormats.Word:
				case ReportFormats.Image:
					inline = false;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			var parameters = this.GetParameters();
			var department = parameters.DepartmentID != null ? this.ddlDepartmentID.SelectedItem.Text : null;
			var program = parameters.ProgramID != null ? this.ddlProgramID.SelectedItem.Text : null;
			List<string> feeConcessionTypes = null;
			if (parameters.FeeConcessionTypeIDs != null && parameters.FeeConcessionTypeIDs.Any())
				feeConcessionTypes = this.cblFeeConcessionTypeIDs.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Text).ToList();
			string instituteBankAccount = null;
			if (parameters.InstituteBankAccountID != null)
				instituteBankAccount = this.ddlInstituteBankAccountID.SelectedItem.Text;

			List<string> feeHeads = null;
			if (parameters.FeeHeadIDs != null && parameters.FeeHeadIDs.Any())
				feeHeads = this.cblFeeHeadIDs.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Text).ToList();

			var filtersText = parameters.GetFiltersText(department, program, instituteBankAccount, feeConcessionTypes, feeHeads);
			switch (report)
			{
				case ReportTypes.DetailedFeeChallanReport:
					var url = DetailedScrollReport.GetPageUrl(parameters, filtersText, reportFormat, inline);
					this.hfReportLink.Value = url.ResolveClientUrl(this);
					break;
				case ReportTypes.DailyStatementBankScroll:
					url = DailyStatementBankScrollReport.GetPageUrl(parameters.SemesterID, parameters.TransactionDateFrom, parameters.TransactionDateTo, parameters.InstituteBankAccountID, reportFormat, inline);
					this.hfReportLink.Value = url.ResolveClientUrl(this);
					break;
				case ReportTypes.Scholarship:
					url = ScholarshipReport.GetPageUrl(parameters.SemesterID, reportFormat, inline);
					this.hfReportLink.Value = url.ResolveClientUrl(this);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}
