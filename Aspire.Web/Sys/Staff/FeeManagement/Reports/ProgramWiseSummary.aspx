﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="ProgramWiseSummary.aspx.cs" Inherits="Aspire.Web.Sys.Staff.FeeManagement.Reports.ProgramWiseSummary" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" ValidationGroup="Parameters" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" ValidationGroup="Parameters" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlNewAdmission" />
			<aspire:AspireDropDownList runat="server" ID="ddlNewAdmission" ValidationGroup="Parameters">
				<Items>
					<asp:ListItem Text="Existing Students" Value="0" />
					<asp:ListItem Text="New Admissions" Value="1" />
				</Items>
			</aspire:AspireDropDownList>
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Deposit Date (From):" AssociatedControlID="dtpDepositDateFrom" />
			<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpDepositDateFrom" />
			<aspire:AspireDateTimeValidator runat="server" AllowNull="True" ID="dtvDepositDateFrom" ControlToValidate="dtpDepositDateFrom" ValidationGroup="Parameters" InvalidDataErrorMessage="Invalid Date." InvalidRangeErrorMessage="Invalid Date Range." RequiredErrorMessage="This field is required." />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Deposit Date (To):" AssociatedControlID="dtpDepositDateTo" />
			<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpDepositDateTo" />
			<aspire:AspireDateTimeValidator runat="server" AllowNull="True" ID="dtvDepositDateTo" ControlToValidate="dtpDepositDateTo" ValidationGroup="Parameters" InvalidDataErrorMessage="Invalid Date." InvalidRangeErrorMessage="Invalid Date Range." RequiredErrorMessage="This field is required." />
		</div>
		<aspire:AspireButton runat="server" Text="Display" ValidationGroup="Parameters" ID="btnDisplay" OnClick="btnDisplay_OnClick" />
	</div>
</asp:Content>

