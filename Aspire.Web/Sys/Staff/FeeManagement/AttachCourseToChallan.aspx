﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="AttachCourseToChallan.aspx.cs" Inherits="Aspire.Web.Sys.Staff.FeeManagement.AttachCourseToChallan" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<%@ Register Src="~/Sys/Staff/Common/StudentInfoUserControl.ascx" TagPrefix="uc1" TagName="StudentInfoUserControl" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel AssociatedControlID="tbEnrollment" runat="server" Text="Enrollment:" />
			<aspire:EnrollmentTextBox runat="server" ValidationGroup="SearchEnrollment" ID="tbEnrollment" OnSearch="tbEnrollment_OnSearch" />
		</div>
		<div class="form-group">
			<aspire:AspireHyperLink runat="server" Text="Clear" NavigateUrl="~/Sys/Staff/FeeManagement/AttachCourseToChallan.aspx" />
		</div>
	</div>
	<asp:Panel runat="server" ID="panelChallan">
		<uc1:StudentInfoUserControl runat="server" ID="ucStudentInfo" />
		<asp:Repeater runat="server" ItemType="System.Int16" ID="repeaterOfferedSemesters">
			<HeaderTemplate>
				<div class="table-responsive">
					<table class="table table-hover table-striped table-condensed table-bordered">
						<thead>
							<tr>
								<th>Challan No.</th>
								<th>Title</th>
								<th>Credit Hours</th>
								<th>Class</th>
								<th>Status</th>
								<th>Fee Defaulter</th>
							</tr>
						</thead>
			</HeaderTemplate>
			<ItemTemplate>
				<thead>
					<tr>
						<th colspan="6"><%# Item.ToSemesterString() %></th>
					</tr>
				</thead>
				<tbody>
					<asp:Repeater runat="server" ItemType="Aspire.BL.Core.FeeManagement.Staff.StudentFees.StudentFeeInfo.RegisteredCourse" DataSource="<%# this.StudentInfo.RegisteredCourses.FindAll(rc=> rc.OfferedSemesterID == Item) %>" ID="repeaterRegisteredCourses" OnItemCommand="repeaterRegisteredCourses_OnItemCommand">
						<ItemTemplate>
							<tr>
								<td>
									<aspire:AspireLinkButton EncryptedCommandArgument="<%# Item.RegisteredCourseID %>" ToolTip="Attach to Fee Challan" runat="server" Visible='<%# Item.StudentFeeID == null %>' CausesValidation="False" CommandName="Attach" Text='<%# Item.FullChallanNo.ToNAIfNullOrEmpty() %>' />
									<aspire:AspireLinkButton EncryptedCommandArgument='<%# $"{Item.RegisteredCourseID}:{Item.StudentFeeID}" %>' ConfirmMessage='<%# $"Are you sure you want to detach \"{Item.Title}\" course from challan \"{Item.FullChallanNo}\"?" %>' ToolTip="Detach from Fee Challan" runat="server" Visible='<%# Item.StudentFeeID != null %>' CausesValidation="False" CommandName="Detach" Text='<%# Item.FullChallanNo.ToNullIfWhiteSpace()?? "Error" %>' />
								</td>
								<td><%#: Item.Title %></td>
								<td><%#: Item.CreditHours.ToCreditHoursFullName() %></td>
								<td><%#: Item.Class %></td>
								<td><%# Item.StatusHtml.ToNAIfNullOrEmpty() %></td>
								<td><%#: Item.FeeDefaulterYesNo %></td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
					<tr>
						<th colspan="2"></th>
						<th><%#: this.StudentInfo.RegisteredCourses.Where(rc=> rc.OfferedSemesterID == Item).Sum(rc=> rc.CreditHours).ToCreditHoursFullName() %></th>
						<th colspan="3"></th>
					</tr>
				</tbody>
			</ItemTemplate>
			<FooterTemplate>
				</table>
				</div>
			</FooterTemplate>
		</asp:Repeater>
		<aspire:AspireModal runat="server" ID="modalAttach" HeaderText="Attach Registered Course To Fee Challan">
			<BodyTemplate>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Title:" AssociatedControlID="lblTitle" />
					<div>
						<aspire:Label runat="server" ID="lblTitle" />
					</div>
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="lblOfferedSemester" />
					<div>
						<aspire:Label runat="server" ID="lblOfferedSemester" />
					</div>
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Fee Challan:" AssociatedControlID="ddlStudentFeeID" />
					<aspire:AspireDropDownList runat="server" ID="ddlStudentFeeID" ValidationGroup="Attach" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlStudentFeeID" ErrorMessage="This field is required." ValidationGroup="Attach" />
				</div>
			</BodyTemplate>
			<FooterTemplate>
				<aspire:AspireButton runat="server" ID="btnAttachFeeChallan" Text="Save" ValidationGroup="Attach" OnClick="btnAttachFeeChallan_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</FooterTemplate>
		</aspire:AspireModal>
	</asp:Panel>
</asp:Content>
