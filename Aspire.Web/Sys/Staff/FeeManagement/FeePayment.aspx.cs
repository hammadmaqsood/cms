﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.FeeManagement
{
	[AspirePage("DFA664D3-A0E2-4550-ABFB-7AF90D0C53D4", UserTypes.Staff, "~/Sys/Staff/FeeManagement/FeePayment.aspx", "Fee Payment", true, AspireModules.FeeManagement)]
	public partial class FeePayment : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.FeeManagement.ManageFeePayment,new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.usd);
		public override string PageTitle => "Fee Payment";

		public static string GetPageUrl(bool newAdmission, int studentFeeChallanID, bool search)
		{
			var fullChallanNo = AspireFormats.ChallanNo.GetFullChallanNoDecimal(Web.Common.StaffIdentity.Current.InstituteCode, newAdmission ? InstituteBankAccount.FeeTypes.NewAdmissionFee : InstituteBankAccount.FeeTypes.StudentFee, studentFeeChallanID);
			return GetPageUrl(fullChallanNo, search);
		}

		public static string GetPageUrl(decimal? fullChallanNo, bool search)
		{
			return typeof(FeePayment).GetAspirePageAttribute().PageUrl.AttachQueryParams("FullChallanNo", fullChallanNo?.ToString(AspireFormats.ChallanNo.FullChallanNoDecimalFormat), "Search", search);
		}

		public static void Redirect(decimal? fullChallanNo, bool search)
		{
			Redirect(GetPageUrl(fullChallanNo, search));
		}
	}
}