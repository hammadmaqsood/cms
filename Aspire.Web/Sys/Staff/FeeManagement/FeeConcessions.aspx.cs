﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;
using Aspire.BL.Core.FeeManagement.Staff;

namespace Aspire.Web.Sys.Staff.FeeManagement
{
	[AspirePage("A4A82832-421F-49BF-A30A-834E40E0C11F", UserTypes.Staff, "~/Sys/Staff/FeeManagement/FeeConcessions.aspx", "Fee Concessions", true, AspireModules.FeeManagement)]
	public partial class FeeConcessions : StaffPage
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.FeeManagement.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.edit);
		public override string PageTitle => "Fee Concessions";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("ConcessionName", SortDirection.Ascending);
				this.GetData(0);
			}
		}

		public void Refresh()
		{
			Redirect<FeeConcessions>();
		}

		protected void btnAddFeeConcession_OnClick(object sender, EventArgs e)
		{
			this.DisplayModalFeeConcession(null);
		}

		protected void gvFeeConcessions_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					var feeConcessionTypeID = e.DecryptedCommandArgumentToInt();
					this.DisplayModalFeeConcession(feeConcessionTypeID);
					break;
				case "Delete":
					e.Handled = true;
					feeConcessionTypeID = e.DecryptedCommandArgumentToInt();
					var status = FeeConcessionTypes.DeleteFeeConcessionType(feeConcessionTypeID, this.StaffIdentity.LoginSessionGuid);
					switch (status)
					{
						case FeeConcessionTypes.DeleteFeeConcessionTypeStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.Refresh();
							break;
						case FeeConcessionTypes.DeleteFeeConcessionTypeStatuses.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Fee Concession Type");
							this.Refresh();
							break;
						case FeeConcessionTypes.DeleteFeeConcessionTypeStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Fee Concession Type");
							this.Refresh();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		protected void gvFeeConcessions_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvFeeConcessions_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvFeeConcessions_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvFeeConcessions.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		private void DisplayModalFeeConcession(int? feeConcessionTypeID)
		{
			this.ddlFeeConcessionVisible.FillYesNo().SetSelectedValue(CommonListItems.YesValue);
			if (feeConcessionTypeID != null)
			{
				this.btnAddEditModalFeeConcession.Text = @"Save";
				var feeConcessionType = FeeConcessionTypes.GetFeeConcessionType(feeConcessionTypeID.Value, this.StaffIdentity.LoginSessionGuid);
				if (feeConcessionType == null)
				{
					this.AddNoRecordFoundAlert();
					this.Refresh();
					return;
				}
				this.tbFeeConcessionName.Text = feeConcessionType.ConcessionName;
				this.ddlFeeConcessionVisible.SetSelectedValue(feeConcessionType.Visible);
				this.ViewState["FeeConcessionTypeID"] = feeConcessionType.FeeConcessionTypeID;
			}
			else
			{
				this.tbFeeConcessionName.Text = null;
				this.ddlFeeConcessionVisible.ClearSelection();
				this.btnAddEditModalFeeConcession.Text = @"Add";
				this.ViewState["FeeConcessionTypeID"] = null;
			}

			this.tbFeeConcessionName.Focus();
			this.modalAddEditFeeConcession.Show();
		}

		protected void btnAddEditModalFeeConcession_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var feeConcessionTypeID = (int?)this.ViewState["FeeConcessionTypeID"];
			var concessionName = this.tbFeeConcessionName.Text;
			var visible = this.ddlFeeConcessionVisible.SelectedValue.ToBoolean();
			if (feeConcessionTypeID != null)
			{
				var status = FeeConcessionTypes.UpdateFeeConcessionType(feeConcessionTypeID.Value, concessionName, visible, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case FeeConcessionTypes.UpdateFeeConcessionTypeStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.Refresh();
						return;
					case FeeConcessionTypes.UpdateFeeConcessionTypeStatuses.NameAlreadyExists:
						this.modalAlert.AddErrorAlert("Name already exists.");
						this.updatePanelModal.Update();
						return;
					case FeeConcessionTypes.UpdateFeeConcessionTypeStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Fee Concession Type");
						this.Refresh();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var status = FeeConcessionTypes.AddFeeConcessionType(concessionName, visible, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case FeeConcessionTypes.AddFeeConcessionTypeStatuses.NameAlreadyExists:
						this.modalAlert.AddErrorAlert("Name already exists.");
						this.updatePanelModal.Update();
						return;
					case FeeConcessionTypes.AddFeeConcessionTypeStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Fee Concession Type");
						this.Refresh();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		private void GetData(int pageIndex)
		{
			var result = FeeConcessionTypes.GetFeeConcessions(pageIndex, this.gvFeeConcessions.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.StaffIdentity.LoginSessionGuid);
			this.gvFeeConcessions.DataBind(result.FeeConcessionTypes, pageIndex, result.VirtualItemsCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}
	}
}