﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="FeeStructures.aspx.cs" Inherits="Aspire.Web.Sys.Staff.FeeManagement.FeeStructures" %>

<%@ Register TagPrefix="uc" TagName="FeeStructurePreviewUserControl" Src="~/Sys/Staff/FeeManagement/FeeStructurePreviewUserControl.ascx" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ID="ctFeeStructure" ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemester" />
					<aspire:AspireDropDownList runat="server" CausesValidation="False" ID="ddlSemester" AutoPostBack="True" OnSelectedIndexChanged="ddlSemester_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgram" />
					<aspire:AspireDropDownList runat="server" CausesValidation="False" ID="ddlProgram" AutoPostBack="True" OnSelectedIndexChanged="ddlProgram_OnSelectedIndexChanged" />
				</div>
			</div>
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="First Semester Credit Hours:" AssociatedControlID="tbCreditHours" />
					<div class="input-group">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button"><i class="glyphicon glyphicon-minus"></i></button>
						</span>
						<aspire:AspireTextBox runat="server" ID="tbCreditHours" ValidationGroup="ReqFeeStructure" />
						<span class="input-group-btn">
							<button class="btn btn-default" type="button"><i class="glyphicon glyphicon-plus"></i></button>
						</span>
					</div>
					<aspire:AspireByteValidator runat="server" ControlToValidate="tbCreditHours" MinValue="0" MaxValue="25" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid number." InvalidRangeErrorMessage="Invalid Credit Hours." ValidationGroup="ReqFeeStructure" AllowNull="False" />
					<aspire:AspireButton runat="server" ID="btnSaveFeeStructure" ButtonType="Primary" Text="Save" ValidationGroup="ReqFeeStructure" OnClick="btnSaveFeeStructure_OnClick" />
				</div>
			</div>
		</div>
	</div>
	<asp:Panel runat="server" ID="panelFeeStructureDetails" Visible="False">
		<p></p>
		<aspire:AspireButton runat="server" ButtonType="Success" Glyphicon="plus" CausesValidation="False" ID="btnAdd" Text="Add Fee Structure Details" OnClick="btnAdd_OnClick" />
		<aspire:AspireButton runat="server" CausesValidation="False" ID="btnPreview" Text="Preview" OnClick="btnPreview_OnClick" />
		<p></p>
		<aspire:AspireGridView runat="server" Responsive="True" ID="gvFeeStructureDetail" AutoGenerateColumns="False" OnRowCommand="gvFeeStructureDetail_OnRowCommand">
			<Columns>
				<asp:BoundField HeaderText="Fee Head" DataField="FeeHead.HeadName" />
				<asp:TemplateField HeaderText="Refundable">
					<ItemTemplate>
						<%#: ((bool)Eval("FeeHead.Refundable")).ToYesNo() %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="Semester No." DataField="SemesterNoEnumFullName" ItemStyle-Wrap="False" />
				<asp:BoundField HeaderText="Categories" DataField="CategoryEnumFullName" ItemStyle-Wrap="False" />
				<asp:BoundField HeaderText="Amount" DataField="Amount" />
				<asp:TemplateField>
					<ItemTemplate>
						<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Edit" Glyphicon="edit" EncryptedCommandArgument='<%#Eval("FeeStructureDetailID")%>' />
						<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Delete" Glyphicon="remove" EncryptedCommandArgument='<%#Eval("FeeStructureDetailID")%>' ConfirmMessage="Are you sure you want to delete a record of fee structure details" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
		<aspire:AspireModal runat="server" ID="modalFeeStructureDetail">
			<BodyTemplate>
				<asp:UpdatePanel runat="server" ID="updatePanelDetails" UpdateMode="Conditional">
					<ContentTemplate>
						<aspire:AspireAlert runat="server" ID="alertFeeStructureDetail" />
						<div class="form-horizontal">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="lbSemesterForSave" CssClass="col-md-3" />
								<div class="form-control-static col-md-9">
									<aspire:AspireLabel runat="server" ID="lbSemesterForSave" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="lbSemesterForSave" CssClass="col-md-3" />
								<div class="form-control-static col-md-9">
									<aspire:AspireLabel runat="server" ID="lbProgramForSave" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Fee Head:" AssociatedControlID="ddlFeeHead" CssClass="col-md-3" />
								<div class="col-md-9">
									<aspire:AspireDropDownList runat="server" ID="ddlFeeHead" />
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlFeeHead" ErrorMessage="This field is required." ValidationGroup="ReqFeeStructureModal" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Semester Nos.:" AssociatedControlID="rblSemesterNo" CssClass="col-md-3" />
								<div class="form-control-static col-md-9">
									<aspire:AspireRadioButtonList runat="server" ID="rblSemesterNo" RepeatLayout="Flow" RepeatDirection="Horizontal" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Category:" AssociatedControlID="rblCategory" CssClass="col-md-3" />
								<div class="form-control-static col-md-9">
									<aspire:AspireRadioButtonList runat="server" ID="rblCategory" RepeatLayout="Flow" RepeatDirection="Horizontal" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Amount:" AssociatedControlID="tbAmount" CssClass="col-md-3" />
								<div class="col-md-9">
									<div class="input-group ">
										<div class="input-group-addon">Rs.</div>
										<aspire:AspireTextBox runat="server" ID="tbAmount" ValidationGroup="ReqFeeStructureModal" />
									</div>
									<aspire:AspireInt32Validator runat="server" ControlToValidate="tbAmount" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid number." ValidationGroup="ReqFeeStructureModal" AllowNull="False" InvalidRangeErrorMessage="Invalid Amount." />
								</div>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</BodyTemplate>
			<FooterTemplate>
				<asp:UpdatePanel runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" OnClick="btnSave_OnClick" ValidationGroup="ReqFeeStructureModal" />
						<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
					</ContentTemplate>
				</asp:UpdatePanel>
			</FooterTemplate>
		</aspire:AspireModal>

		<uc:FeeStructurePreviewUserControl runat="server" ID="ucFeeStructurePreviewUserControl" />
	</asp:Panel>
	<script type="text/javascript">
		$(function () {
			var tb = $("input#<%=this.tbCreditHours.ClientID%>");
			function applySpinner(tb) {
				$("button", $(tb).prev()).off("click").click(function () {
					var value = $(tb).val().tryToInt();
					if (value != null && value >= 1 && value <= 255)
						$(tb).val(--value);
					if (value == null)
						$(tb).val(1);
					$(tb).change();
				});
				$("button", $(tb).next()).off("click").click(function () {
					var value = $(tb).val().tryToInt();
					if (value != null && value >= 0 && value <= 254)
						$(tb).val(++value);
					if (value == null)
						$(tb).val(1);
					$(tb).change();
				});
			}
			applySpinner(tb);
		});
	</script>
</asp:Content>
