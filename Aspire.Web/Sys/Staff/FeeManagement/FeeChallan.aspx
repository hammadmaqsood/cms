﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="FeeChallan.aspx.cs" Inherits="Aspire.Web.Sys.Staff.FeeManagement.FeeChallan" %>

<%@ Register TagPrefix="uc" TagName="FeeHeadUserControl" Src="FeeHeadUserControl.ascx" %>
<%@ Register Src="~/Sys/Staff/FeeManagement/InstallmentsUserControl.ascx" TagPrefix="uc" TagName="InstallmentsUserControl" %>
<%@ Register Src="~/Sys/Staff/FeeManagement/FeeStructurePreviewUserControl.ascx" TagPrefix="uc" TagName="FeeStructurePreviewUserControl" %>
<asp:Content runat="server" ContentPlaceHolderID="HeadPH">
	<style type="text/css">
		.DatePickerTextbox > input.form-control {
			border-top-left-radius: 0;
			border-bottom-left-radius: 0;
		}
	</style>
</asp:Content>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
			<aspire:AspireDropDownList ID="ddlSemesterID" ValidationGroup="Search" runat="server" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlFeeType" />
			<aspire:AspireDropDownList ID="ddlFeeType" ValidationGroup="Search" runat="server" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel AssociatedControlID="tbEnrollment" runat="server" Text="Enrollment:" />
			<aspire:EnrollmentTextBox runat="server" ValidationGroup="SearchEnrollment" ID="tbEnrollment" OnSearch="tbEnrollment_OnSearch" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel AssociatedControlID="tbEnrollment" runat="server" Text="Application No.:" />
			<asp:Panel runat="server" DefaultButton="btnSearchAppNo" CssClass="input-group">
				<aspire:AspireTextBox runat="server" ID="tbApplicationNo" ValidationGroup="SearchAppNo" />
				<span class="input-group-btn">
					<aspire:AspireButton runat="server" ButtonType="Primary" Glyphicon="arrow_right" ID="btnSearchAppNo" ValidationGroup="SearchAppNo" OnClick="btnSearchAppNo_OnClick" />
				</span>
			</asp:Panel>
			<aspire:AspireInt32Validator runat="server" ValidationGroup="SearchAppNo" AllowNull="False" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Application No." RequiredErrorMessage="This field is required." ControlToValidate="tbApplicationNo" />
		</div>
		<div class="form-group">
			<aspire:AspireHyperLink runat="server" Text="Clear" NavigateUrl="~/Sys/Staff/FeeManagement/FeeChallan.aspx" />
		</div>
	</div>
	<asp:Panel runat="server" ID="panelChallan">
		<div class="table-responsive">
			<style type="text/css" scoped="scoped">
				.studentInfo th {
					white-space: nowrap;
				}

				.studentInfo td {
					width: 50%;
				}
			</style>
			<table class="table table-bordered table-condensed studentInfo" id="tableStudentInfo" runat="server">
				<tbody>
					<tr>
						<th>Enrollment</th>
						<td>
							<aspire:AspireHyperLink runat="server" ID="hlEnrollment" />
						</td>
						<th>Registration No.</th>
						<td>
							<aspire:Label runat="server" ID="lblRegistration" /></td>
					</tr>
					<tr>
						<th>Name</th>
						<td>
							<aspire:Label runat="server" ID="lblName" /></td>
						<th>Category</th>
						<td>
							<aspire:Label runat="server" ID="lblCategory" /></td>
					</tr>
					<tr>
						<th>Program</th>
						<td>
							<aspire:Label runat="server" ID="lblProgram" /></td>
						<th>Intake Semester</th>
						<td>
							<aspire:Label runat="server" ID="lblIntakeSemester" /></td>
					</tr>
					<tr>
						<th>Nationality</th>
						<td>
							<aspire:Label runat="server" ID="lblNationality" /></td>
						<th>Status</th>
						<td>
							<aspire:Label runat="server" ID="lblStatus" /></td>
					</tr>
				</tbody>
			</table>
			<table class="table table-bordered table-condensed studentInfo" id="tableCandidateInfo" runat="server">
				<tbody>
					<tr>
						<th>Application No.</th>
						<td>
							<aspire:AspireHyperLink runat="server" ID="hlApplicationNoC" />
						</td>
						<th>Intake Semester</th>
						<td>
							<aspire:Label runat="server" ID="lblIntakeSemesterC" /></td>
					</tr>
					<tr>
						<th>Name</th>
						<td>
							<aspire:Label runat="server" ID="lblNameC" /></td>
						<th>Category</th>
						<td>
							<aspire:Label runat="server" ID="lblCategoryC" /></td>
					</tr>
					<tr>
						<th>Program</th>
						<td>
							<aspire:Label runat="server" ID="lblProgramC" /></td>
						<th>Nationality</th>
						<td>
							<aspire:Label runat="server" ID="lblNationalityC" /></td>
					</tr>
				</tbody>
			</table>
			<script type="text/javascript">
				$(function () {
					$("#<%=this.tableStudentInfo.ClientID%>").prepend($("<caption>Student Information</caption>"));
					$("#<%=this.tableCandidateInfo.ClientID%>").prepend($("<caption>Candidate Information</caption>"));
				});
			</script>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">
					<asp:Literal Mode="Encode" ID="ltrlPanelTitle" runat="server" /></h3>
			</div>
			<div class="panel-body">
				<aspire:AspireGridView Caption="Registered Courses" runat="server" CssClass="table-condensed" ID="gvRegisteredCourses" AutoGenerateColumns="False">
					<Columns>
						<asp:TemplateField HeaderText="Select">
							<HeaderTemplate>
								<input type="checkbox" id="cbSelectAll" checked="checked" title="Select All" />
							</HeaderTemplate>
							<ItemTemplate>
								<aspire:AspireCheckBox Checked='<%# Eval("StudentFeeID")==null %>' Visible='<%# Eval("StudentFeeID")==null %>' runat="server" ID="cb" ClientIDMode="Static" />
								<input type="hidden" id="hfCreditHours" value='<%# Eval("RoadmapCreditHours") %>' />
								<aspire:HiddenField runat="server" Mode="Encrypted" ID="hf" Value='<%# Eval("RegisteredCourseID") %>' />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField HeaderText="Code" DataField="RoadmapCourseCode" />
						<asp:BoundField HeaderText="Title" DataField="RoadmapTitle" />
						<asp:BoundField HeaderText="Credit Hours" DataField="RoadmapCreditHours" />
						<asp:BoundField HeaderText="Class" DataField="OfferedClass" />
					</Columns>
				</aspire:AspireGridView>
				<div class="form-horizontal">
					<div class="form-group-sm text-right">
						<aspire:AspireHyperLinkButton Text="View Fee Challans" runat="server" CssClass="btn-sm" ID="hlViewFeeChallans" Target="_blank" />
						<aspire:AspireButton runat="server" CssClass="btn-sm" Text="Fee Structure" ID="btnFeeStructure" CausesValidation="False" OnClick="btnFeeStructure_OnClick" />
					</div>
					<div class="form-group form-group-sm">
						<aspire:AspireLabel CssClass="col-sm-3" runat="server" Text="Semester:" AssociatedControlID="lblSemesterID" />
						<div class="form-control-static col-sm-9">
							<aspire:AspireLabel CssClass="form-control-static" runat="server" ID="lblSemesterID" />
						</div>
					</div>
					<div class="form-group form-group-sm">
						<aspire:AspireLabel CssClass="col-sm-3" runat="server" Text="Exempt Withholding Tax for this Challan?" AssociatedControlID="ddlExemptWHT" />
						<div class="col-sm-6 col-md-5 col-lg-4">
							<aspire:AspireDropDownList runat="server" ID="ddlExemptWHT" ValidationGroup="FeeChallan" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="ddlExemptWHT_SelectedIndexChanged" />
						</div>
					</div>
					<div class="form-group form-group-sm">
						<aspire:AspireLabel CssClass="col-sm-3" runat="server" Text="Concession / Scholarship:" AssociatedControlID="cblConcessions" />
						<div class="col-sm-9">
							<aspire:AspireCheckBoxList runat="server" ID="cblConcessions" ValidationGroup="FeeChallan" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="cblConcessions_OnSelectedIndexChanged" RepeatLayout="Flow" RepeatDirection="Horizontal" />
						</div>
					</div>
					<div class="form-group form-group-sm">
						<aspire:AspireLabel CssClass="col-sm-3" runat="server" Text="Credit Hours:" AssociatedControlID="tbCreditHours" />
						<div class="col-sm-3">
							<div class="input-group input-group-sm">
								<div class="input-group-btn">
									<button type="button" class="btn btn-default"><i class="glyphicon glyphicon-minus"></i></button>
								</div>
								<aspire:AspireTextBox runat="server" Text="" ID="tbCreditHours" ValidationGroup="FeeChallan" />
								<div class="input-group-btn">
									<button type="button" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i></button>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbCreditHours" MinValue="0" MaxValue="150" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Credit Hours." ValidationGroup="FeeChallan" />
						</div>
					</div>
					<div class="form-group form-group-sm">
						<aspire:AspireLabel CssClass="col-sm-3" runat="server" Text="Tuition Fee (Per Semester):" AssociatedControlID="tbTuitionFee" />
						<div class="col-sm-3">
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" Text="" ID="tbTuitionFee" ValidationGroup="FeeChallan" ReadOnly="True" />
							</div>
						</div>
					</div>
					<asp:Repeater runat="server" ID="repeaterFeeChallan">
						<HeaderTemplate>
							<uc:FeeHeadUserControl runat="server" ID="ucFeeHeadHeader" Header="true" />
						</HeaderTemplate>
						<ItemTemplate>
							<uc:FeeHeadUserControl runat="server" ID="ucFeeHead" DataSource='<%# Container.DataItem %>' />
						</ItemTemplate>
						<FooterTemplate>
							<uc:FeeHeadUserControl runat="server" ID="ucFeeHeadFooter" Footer="true" />
						</FooterTemplate>
					</asp:Repeater>
					<asp:Repeater runat="server" ID="repeaterInstallments">
						<ItemTemplate>
							<uc:InstallmentsUserControl runat="server" ID="ucInstallments" DataSource='<%# Container.DataItem %>' />
						</ItemTemplate>
					</asp:Repeater>
					<div class="form-group form-group-sm">
						<div class="col-sm-6 col-md-5 col-lg-4 col-sm-offset-3">
							<aspire:AspireCustomValidator runat="server" ClientValidationFunction="validateInstallmentsTotal" ErrorMessage="Sum of installment amount is not equal to Total Amount." ID="cvInstallmentAmounts" ValidationGroup="FeeChallan" OnServerValidate="cvInstallmentAmounts_OnServerValidate" />
						</div>
					</div>
					<div class="form-group form-group-sm">
						<aspire:AspireLabel CssClass="col-sm-3" runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" />
						<div class="col-sm-6 col-md-5 col-lg-4">
							<aspire:AspireTextBox runat="server" ID="tbRemarks" TextMode="MultiLine" Rows="3" ValidationGroup="FeeChallan" MaxLength="1000" />
						</div>
					</div>
					<div class="form-group form-group-sm">
						<div class="col-sm-6 col-md-5 col-lg-4 col-sm-offset-3">
							<aspire:AspireButton runat="server" ID="btnSave" CssClass="btn-sm" ButtonType="Primary" ValidationGroup="FeeChallan" Text="Save" OnClick="btnSave_Click" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="FeeChallan.js"></script>
		<script type="text/javascript">
			$(function () {
				var registeredCoursesTable = $("#<%=this.gvRegisteredCourses.ClientID%>");
				var tbCreditHours = $("input#<%=this.tbCreditHours.ClientID%>");
				var tbTutionFeePerSemester = $("input#<%=this.tbTuitionFee.ClientID%>");
				applyEvents(registeredCoursesTable, tbCreditHours, tbTutionFeePerSemester);
			});
		</script>
	</asp:Panel>
	<uc:FeeStructurePreviewUserControl runat="server" ID="ucFeeStructurePreviewUserControl" />
</asp:Content>
