﻿function getDataRows() {
	var dataRows = [];
	var footerRow;
	$("div.form-group.FeeChallan").each(function (i, row) {
		if (i > 0)
			dataRows.push(row);
		footerRow = row;
	});
	dataRows.pop(footerRow);
	return { dataRows: dataRows, footerRow: footerRow };
}

function applyEvents(registeredCoursesTable, tbCreditHours, tbTutionFeePerSemester) {
	$("input#cbSelectAll", registeredCoursesTable).click(function () {
		$("input#cb:checkbox", registeredCoursesTable).prop("checked", !$(this).prop("checked")).click();
	});
	$("input#cb:checkbox", registeredCoursesTable).click(function () {
		var creditHours = 0.0;
		$("input#cb:checkbox:checked", registeredCoursesTable).each(function (index, cb) {
			creditHours += $("input#hfCreditHours", $(this).closest("td")).val().toDouble();
		});
		console.log(creditHours);
		$(tbCreditHours).val(creditHours).change();
	});

	var installmentsManuallyUpdated = false;
	$("div.form-group.Installment").each(function (index, row) {
		var amount = $("input.Amount", row).val();
		if (amount != null && amount.length > 0) {
			installmentsManuallyUpdated = true;
			return false;
		}
		return true;
	});
	//#region Credit Hours
	var maxCreditHours = 150.0;
	$("button", tbCreditHours.prev()).click(function () {
		var value = tbCreditHours.val().toDouble();
		if (value != null && value >= 1.0 && value <= maxCreditHours)
			tbCreditHours.val(Math.round10(value - 1.0, -2));
		else if (value != null && value > 0.0 && value < 1.0)
			tbCreditHours.val(0);
		tbCreditHours.change();
	});
	$("button", tbCreditHours.next()).click(function () {
		var value = tbCreditHours.val().toDouble();
		if (value != null && value >= 0.0 && value <= maxCreditHours - 1)
			tbCreditHours.val(Math.round10(value + 1.0, -2));
		else if (value != null && value >= 0.0 && value < maxCreditHours)
			tbCreditHours.val(maxCreditHours);
		tbCreditHours.change();
	});
	//#endregion

	var rows = getDataRows();
	var dataRows = rows.dataRows;
	var footerRow = rows.footerRow;

	var tbAmountTotal = $("input.Amount", footerRow).prop("readonly", "readonly");
	var tbConcessionTotal = $("input.Concession", footerRow).prop("readonly", "readonly");
	var tbTotalGrand = $("input.Total", footerRow).prop("readonly", "readonly");

	function getAmount(tb) {
		if ($(tb).val().trim().length === 0)
			return 0;
		return $(tb).val().tryToInt();
	}

	function updateInstallments(totalAmount) {
		var installments = 1;
		if (installmentsManuallyUpdated && installments > 1)
			return;
		var sum = 0;
		$("div.form-group.Installment").each(function (index, row) {
			var tbAmount = $("input.Amount", row);
			if (installments === 1)
				tbAmount.prop("readonly", "readonly");
			if (totalAmount == null)
				tbAmount.val("");
			else {
				if (index !== (installments - 1)) {
					var amount = Math.floor(totalAmount / installments);
					sum += amount;
					tbAmount.val(amount);
				}
				else
					tbAmount.val(totalAmount - sum);
			}
		});
	}

	function updateGrandTotal() {
		var amount = $(tbAmountTotal).val();
		var concession = $(tbConcessionTotal).val();
		if (amount.length === 0 || concession.length === 0)
			$(tbTotalGrand).val("");
		else
			$(tbTotalGrand).val(amount.toInt() - concession.tryToInt());
		updateInstallments($(tbTotalGrand).val().toNullableInt());
	}

	function updateColumnTotal(cssSelector, tbTotal) {
		var totalAmount = 0;
		$(dataRows).each(function (i, row) {
			var tb = $(cssSelector, row);
			var amount = getAmount(tb);
			if (totalAmount != null) {
				if (amount == null)
					totalAmount = null;
				else {
					var ruleType = tb.attr("RuleType".toLowerCase());
					switch (ruleType) {
						case "Credit":
							totalAmount = totalAmount + amount;
							break;
						case "Debit":
							totalAmount = totalAmount - amount;
							break;
						default:
							throw ruleType + " not supported.";
					}
				}
			}
		});
		tbTotal.val(totalAmount == null ? "" : totalAmount);
		updateGrandTotal();
	}

	function updateTotal(tbAm, tbCo, tbTot, cssSelector, tbTotal) {
		var amount = getAmount(tbAm);
		var concession = getAmount(tbCo);
		if (amount == null || concession == null)
			$(tbTot).val("");
		else
			$(tbTot).val(amount - concession);
		updateColumnTotal(cssSelector, tbTotal);
	}

	var events = "keyup blur click change paste";
	$("div.form-group.Installment input.Amount").on(events, function () { installmentsManuallyUpdated = true; });

	$(dataRows).each(function (i, row) {
		var tbAmount = $("input.Amount", row);
		var tbConcession = $("input.Concession", row);
		var tbTotal = $("input.Total", row).prop("readonly", "readonly");
		var events = "keyup blur click change paste";
		tbAmount.on(events, function () { updateTotal(tbAmount, tbConcession, tbTotal, "input.Amount", tbAmountTotal); }).click();
		tbConcession.on(events, function () { updateTotal(tbAmount, tbConcession, tbTotal, "input.Concession", tbConcessionTotal); }).click();
	});

	tbCreditHours.change(function () {
		var creditHours = tbCreditHours.val().tryToDouble();
		if (creditHours == null || creditHours < 0 || creditHours > 999)
			return;
		var tutionFeePerSemester = $(tbTutionFeePerSemester).val().tryToInt();
		if (tutionFeePerSemester == null)
			return;
		var tutionFee = Math.ceil(creditHours * tutionFeePerSemester);
		$(dataRows).each(function (i, row) {
			var tb = $("input.Amount", row);
			if (tb.attr("FeeHeadType".toLowerCase()) === "TutionFee")
				tb.val(tutionFee).change();
		});
	}).change();
}

function validateInstallmentsTotal(source, args) {
	var total = 0;
	$("div.form-group.Installment").each(function (index, row) {
		var amount = $("input.Amount", row).val().tryToInt();
		if (amount == null) {
			total = null;
			return false;
		}
		else {
			total += amount;
			return true;
		}
	});

	var rows = getDataRows();
	var footerRow = rows.footerRow;

	var tbTotalGrand = $("input.Total", footerRow).val().tryToInt();
	args.IsValid = (total === tbTotalGrand);
}
