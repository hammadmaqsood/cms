﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using System;
using System.ComponentModel;

namespace Aspire.Web.Sys.Staff.FeeManagement
{
	public partial class InstallmentsUserControl : System.Web.UI.UserControl
	{
		public int Amount => this.tbAmount.Text.ToInt();
		public InstallmentNos InstallmentNo => (InstallmentNos)this.ViewState["InstallmentNo"];
		public DateTime DueDate => this.dtpDueDate.SelectedDate.Value;

		protected void Page_Load(object sender, EventArgs e)
		{
		}

		[Bindable(true)]
		public object DataSource
		{
			set
			{
				var installmentNo = (InstallmentNos)value;
				this.ViewState["InstallmentNo"] = installmentNo;
				this.lblInstallmentNo.Text = $"Installment No. {installmentNo.ToFullName()}:";
				if (installmentNo == InstallmentNos.One)
					this.dtpDueDate.SelectedDate = DateTime.Today.AddDays(7);
			}
		}
	}
}