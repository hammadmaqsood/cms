﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.FeeManagement
{
	[AspirePage("830C65D2-3AD1-424A-8F88-95182E8F5A6B", UserTypes.Staff, "~/Sys/Staff/FeeManagement/FeeStructures.aspx", "Fee Structures", true, AspireModules.FeeManagement)]
	public partial class FeeStructures : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.FeeManagement.ManageFeeStructure, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Fee Structure";

		private void RefreshPage()
		{
			Redirect<FeeStructures>("SemesterID", this.ddlSemester.SelectedValue.ToNullableShort(), "AdmissionOpenProgramID", this.ddlProgram.SelectedValue.ToNullableInt());
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemester.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
				if (this.ddlSemester.Items.Count == 0)
				{
					this.AddErrorAlert("No semester record found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlSemester_OnSelectedIndexChanged(null, null);
			}
		}

		#region Fee Structure

		protected void ddlSemester_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemester.SelectedValue.ToShort();
			BL.Core.Common.Lists.GetAdmissionOpenPrograms(semesterID, this.StaffIdentity.InstituteID).DataBind(this.ddlProgram).SetSelectedValueIfNotPostback("AdmissionOpenProgramID");
			this.ddlProgram_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgram_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData();
		}

		protected void btnSaveFeeStructure_OnClick(object sender, EventArgs e)
		{
			var creditHours = this.tbCreditHours.Text.ToByte();
			var feeStructureID = (int?)this.ViewState["FeeStructureID"];
			if (feeStructureID == null)
			{
				var admissionOpenProgramID = this.ddlProgram.SelectedValue.ToInt();
				var status = BL.Core.FeeManagement.Staff.FeeStructures.AddFeeStructure(admissionOpenProgramID, creditHours, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.FeeManagement.Staff.FeeStructures.AddFeeStructureStatuses.AlreadyExists:
						this.AddErrorAlert("Fee structure already exists.");
						this.GetData();
						return;
					case BL.Core.FeeManagement.Staff.FeeStructures.AddFeeStructureStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Fee structure");
						this.GetData();
						return;
					case BL.Core.FeeManagement.Staff.FeeStructures.AddFeeStructureStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.GetData();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var status = BL.Core.FeeManagement.Staff.FeeStructures.EditFeeStructure(feeStructureID.Value, creditHours, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.FeeManagement.Staff.FeeStructures.EditFeeStructureStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<FeeStructures>();
						return;
					case BL.Core.FeeManagement.Staff.FeeStructures.EditFeeStructureStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Fee structure");
						this.GetData();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		#endregion

		#region Fee Structure Details

		protected void btnAdd_OnClick(object sender, EventArgs e)
		{
			this.DisplayFeeStructureDetailModal(null);
		}

		protected void gvFeeStructureDetail_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayFeeStructureDetailModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var feeStructureDetailID = e.DecryptedCommandArgumentToInt();
					var status = BL.Core.FeeManagement.Staff.FeeStructures.DeleteFeeStructureDetail(feeStructureDetailID, this.StaffIdentity.LoginSessionGuid);
					switch (status)
					{
						case BL.Core.FeeManagement.Staff.FeeStructures.DeleteFeeStructureDetailStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.FeeManagement.Staff.FeeStructures.DeleteFeeStructureDetailStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Fee structure detail");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.GetData();
					break;
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var feeHeadID = this.ddlFeeHead.SelectedValue.ToInt();
			var semesterNosEnum = this.rblSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
			var categoriesEnum = this.rblCategory.GetSelectedEnumValue<Categories>(null);
			var amount = this.tbAmount.Text.ToInt();
			var feeStructureDetailID = (int?)this.ViewState["FeeStructureDetailID"];
			if (feeStructureDetailID == null)
			{
				var feeStructureID = (int?)this.ViewState["FeeStructureID"];
				var status = BL.Core.FeeManagement.Staff.FeeStructures.AddFeeStructureDetail(feeStructureID.Value, feeHeadID, semesterNosEnum, categoriesEnum, amount, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.FeeManagement.Staff.FeeStructures.AddFeeStructureDetailStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						break;
					case BL.Core.FeeManagement.Staff.FeeStructures.AddFeeStructureDetailStatuses.AlreadyExists:
						this.alertFeeStructureDetail.AddMessage(AspireAlert.AlertTypes.Error, "Fee structure detail already exists with provided selection.");
						this.updatePanelDetails.Update();
						return;
					case BL.Core.FeeManagement.Staff.FeeStructures.AddFeeStructureDetailStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Fee structure details ");
						this.RefreshPage();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var status = BL.Core.FeeManagement.Staff.FeeStructures.UpdateFeeStructureDetail(feeStructureDetailID.Value, amount, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.FeeManagement.Staff.FeeStructures.UpdateFeeStructureDetailStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						break;
					case BL.Core.FeeManagement.Staff.FeeStructures.UpdateFeeStructureDetailStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Fee structure details ");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				this.RefreshPage();
			}
		}

		#endregion

		#region Page Specific Functions

		private void DisplayFeeStructureDetailModal(int? feeStructureDetailID)
		{
			var feeStructureID = (int?)this.ViewState["FeeStructureID"];
			if (feeStructureID == null) return;

			var admissionOpenProgramID = (int)this.ViewState["AdmissionOpenProgramID"];
			var feeStructure = BL.Core.FeeManagement.Staff.FeeStructures.GetFeeStructure(admissionOpenProgramID, this.StaffIdentity.LoginSessionGuid);
			this.lbSemesterForSave.Text = feeStructure.AdmissionOpenProgram.SemesterID.ToSemesterString();
			this.lbProgramForSave.Text = feeStructure.AdmissionOpenProgram.Program.ProgramAlias;

			var feeHeads = BL.Core.FeeManagement.Staff.FeeHeads.GetFeeHeadsList(this.StaffIdentity.LoginSessionGuid);
			this.ddlFeeHead.DataBind(feeHeads);
			this.rblSemesterNo.FillSemesterNos(CommonListItems.All).SetSelectedValue(CommonListItems.All.Value).Enabled = true;
			this.rblCategory.FillCategories(CommonListItems.All).SetSelectedValue(CommonListItems.All.Value).Enabled = true;
			if (feeStructureDetailID == null)
			{
				this.ddlFeeHead.ClearSelection();
				this.ddlFeeHead.Enabled = true;
				this.tbAmount.Text = null;

				this.modalFeeStructureDetail.HeaderText = "Add Fee Structure";
				this.btnSave.Text = @"Add";
				this.ViewState["FeeStructureDetailID"] = null;
				this.modalFeeStructureDetail.Show();
			}
			else
			{
				var feeStructureDetail = BL.Core.FeeManagement.Staff.FeeStructures.GetFeeStructureDetail(feeStructureDetailID.Value, this.StaffIdentity.LoginSessionGuid);
				if (feeStructureDetail == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<FeeStructures>();
					return;
				}

				this.ddlFeeHead.SelectedValue = feeStructureDetail.FeeHead.FeeHeadID.ToString();
				this.ddlFeeHead.Enabled = false;
				this.rblSemesterNo.SetEnumValue(feeStructureDetail.SemesterNo).Enabled = false;
				this.rblCategory.SetEnumValue(feeStructureDetail.CategoryEnum).Enabled = false;
				this.tbAmount.Text = feeStructureDetail.Amount.ToString();

				this.modalFeeStructureDetail.HeaderText = "Edit Fee Structure";
				this.btnSave.Text = @"Save";
				this.ViewState["FeeStructureDetailID"] = feeStructureDetailID;
				this.modalFeeStructureDetail.Show();
			}
		}

		private void GetData()
		{
			var admissionOpenProgramID = this.ddlProgram.SelectedValue.ToNullableInt();
			if (admissionOpenProgramID == null)
			{
				this.AddErrorAlert("No Program record found.");
				this.tbCreditHours.Text = null;
				this.tbCreditHours.Enabled = false;
				this.btnSaveFeeStructure.Enabled = false;
				this.btnSave.Enabled = false;
				this.panelFeeStructureDetails.Visible = false;
				return;
			}
			var feeStructure = BL.Core.FeeManagement.Staff.FeeStructures.GetFeeStructure(admissionOpenProgramID.Value, this.StaffIdentity.LoginSessionGuid);
			if (feeStructure == null)
			{
				this.tbCreditHours.Text = null;
				this.tbCreditHours.Enabled = true;
				this.btnSaveFeeStructure.Text = @"Add";
				this.btnSaveFeeStructure.Enabled = true;

				this.ViewState["FeeStructureID"] = null;
				this.btnSave.Enabled = false;
				this.panelFeeStructureDetails.Visible = false;
				return;
			}
			this.ddlProgram.SelectedValue = feeStructure.AdmissionOpenProgramID.ToString();
			this.tbCreditHours.Text = feeStructure.FirstSemesterCreditHours.ToString();
			this.tbCreditHours.Enabled = true;

			this.btnSaveFeeStructure.Text = @"Save";
			this.ViewState["FeeStructureID"] = feeStructure.FeeStructureID;
			this.ViewState["AdmissionOpenProgramID"] = feeStructure.AdmissionOpenProgramID;
			this.btnSaveFeeStructure.Enabled = true;
			this.btnSave.Enabled = true;
			this.panelFeeStructureDetails.Visible = true;
			feeStructure.FeeStructureDetails = feeStructure.FeeStructureDetails.OrderBy(fs => fs.FeeHead.DisplayIndex).ToList();
			this.gvFeeStructureDetail.DataBind(feeStructure.FeeStructureDetails);
		}

		#endregion

		protected void btnPreview_OnClick(object sender, EventArgs e)
		{
			this.ucFeeStructurePreviewUserControl.ShowDialog((int)this.ViewState["AdmissionOpenProgramID"]);
		}
	}
}
