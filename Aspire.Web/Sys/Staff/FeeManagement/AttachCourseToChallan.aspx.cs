﻿using Aspire.BL.Core.FeeManagement.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.FeeManagement
{
	[AspirePage("8D9B3001-A67C-42F8-8291-2B932290E70F", UserTypes.Staff, "~/Sys/Staff/FeeManagement/AttachCourseToChallan.aspx", "Attach Course To Challan", true, AspireModules.FeeManagement)]
	public partial class AttachCourseToChallan : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.FeeManagement.Module, UserGroupPermission.Allowed}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.flag);
		public override string PageTitle => "Attach Course(s) To Challan";

		public static string GetPageUrl(string enrollment, bool search)
		{
			return typeof(AttachCourseToChallan).GetAspirePageAttribute().PageUrl.AttachQueryParams("Enrollment", enrollment, "Search", search);
		}

		public new static void Redirect(string enrollment, bool search)
		{
			Redirect(GetPageUrl(enrollment, search));
		}

		private void RefreshPage()
		{
			Redirect(this.ucStudentInfo.Enrollment, true);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.panelChallan.Visible = false;
				this.tbEnrollment.Enrollment = this.Request.GetParameterValue("Enrollment");
				var search = this.Request.GetParameterValue<bool>("Search") == true;
				if (search && !string.IsNullOrWhiteSpace(this.tbEnrollment.Enrollment))
					this.tbEnrollment_OnSearch(null, null);
				this.tbEnrollment.Focus();
			}

		}
		protected void tbEnrollment_OnSearch(object sender, EventArgs e)
		{
			if (this.IsPostbackAndNotValid)
				return;
			var enrollment = this.tbEnrollment.Enrollment;
			this.GetData(enrollment);
		}

		public StudentFees.StudentFeeInfo StudentInfo { get; private set; }

		private void GetData(string enrollment)
		{
			this.StudentInfo = BL.Core.FeeManagement.Staff.StudentFees.GetStudentFeeInfo(enrollment, null, this.StaffIdentity.LoginSessionGuid);
			if (this.StudentInfo == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect(enrollment, false);
				return;
			}

			this.panelChallan.Visible = true;
			this.ucStudentInfo.SetStudentInformation(this.StudentInfo.Enrollment, this.StudentInfo.StudentID, this.StudentInfo.RegistrationNo, this.StudentInfo.Name, this.StudentInfo.FatherName, this.StudentInfo.ProgramAlias, this.StudentInfo.IntakeSemesterID, this.StudentInfo.StatusEnum, this.StudentInfo.StatusDate, this.StudentInfo.BlockedReason);
			this.repeaterOfferedSemesters.DataBind(this.StudentInfo.OfferedSemesterIDs);
		}

		protected void repeaterRegisteredCourses_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Attach":
					this.DisplayModal(e.DecryptedCommandArgumentToInt());
					return;
				case "Detach":
					var args = e.DecryptedCommandArgument().Split(':');
					var result = AttachFeeChallanToCourse.DetachRegisteredCourseToStudentFee(args[0].ToInt(), args[1].ToInt(), this.StaffIdentity.LoginSessionGuid);
					switch (result)
					{
						case AttachFeeChallanToCourse.DetachRegisteredCourseToStudentFeeStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case AttachFeeChallanToCourse.DetachRegisteredCourseToStudentFeeStatuses.AlreadyDetached:
							this.AddErrorAlert("Fee challan not found against the Registered course.");
							break;
						case AttachFeeChallanToCourse.DetachRegisteredCourseToStudentFeeStatuses.Success:
							this.AddSuccessAlert("Course has been detached from challan.");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.RefreshPage();
					return;
			}
		}

		private void DisplayModal(int registeredCourseID)
		{
			var data = AttachFeeChallanToCourse.GetRegisteredCourseForFeeAttachment(registeredCourseID, this.StaffIdentity.LoginSessionGuid);
			if (data == null)
			{
				this.AddNoRecordFoundAlert();
				this.RefreshPage();
				return;
			}
			this.lblTitle.Text = data.Title;
			this.lblOfferedSemester.Text = data.OfferedSemesterID.ToSemesterString();
			this.ddlStudentFeeID.DataBind(data.StudentFeeChallans);
			this.ViewState["RegisteredCourseID"] = data.RegisteredCourseID;
			this.modalAttach.Show();
		}

		protected void btnAttachFeeChallan_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var registeredCourseID = (int)this.ViewState["RegisteredCourseID"];
			var studentFeeID = this.ddlStudentFeeID.SelectedValue.ToInt();
			var result = AttachFeeChallanToCourse.AttachRegisteredCourseToStudentFee(registeredCourseID, studentFeeID, this.StaffIdentity.LoginSessionGuid);
			switch (result)
			{
				case AttachFeeChallanToCourse.AttachRegisteredCourseToStudentFeeStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					break;
				case AttachFeeChallanToCourse.AttachRegisteredCourseToStudentFeeStatuses.AlreadyAttached:
					this.AddErrorAlert("Registered course is already attached to fee challan.");
					break;
				case AttachFeeChallanToCourse.AttachRegisteredCourseToStudentFeeStatuses.Success:
					this.AddSuccessAlert("Course has been attached to fee challan.");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.RefreshPage();
		}
	}
}