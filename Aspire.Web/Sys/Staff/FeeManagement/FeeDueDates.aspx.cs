﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.FeeManagement
{
	[AspirePage("D73FEAA9-279A-453A-8DB9-8011F99BF1A0", UserTypes.Staff, "~/Sys/Staff/FeeManagement/FeeDueDates.aspx", "Fee Due Dates", true, AspireModules.FeeManagement)]
	public partial class FeeDueDates : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.FeeManagement.Module, new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.calendar);
		public override string PageTitle => "Fee Due Dates";

		public static void Redirect(short? semesterID, int? departmentID, int? programID, SortDirection sortDirection, string sortExpression, int? pageSize)
		{
			Redirect<FeeDueDates>("SemesterID", semesterID, "DepartmentID", departmentID, "ProgramID", programID, "SortDirection", sortDirection, "SortExpression", sortExpression, "PageSize", pageSize);
		}

		private void RefreshPage()
		{
			Redirect(this.ddlSemesterID.SelectedValue.ToNullableShort(), this.ddlDepartmentID.SelectedValue.ToNullableInt(), this.ddlProgramID.SelectedValue.ToNullableInt(), this.ViewState.GetSortDirection(), this.ViewState.GetSortExpression(), this.gvFeeDueDates.PageSize);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(this.Request.GetParameterValue("SortExpression") ?? "Semester", this.Request.GetParameterValue<SortDirection>("SortDirection") ?? SortDirection.Descending);
				this.ddlSemesterID.FillSemesters(CommonListItems.All).SetSelectedValueIfNotPostback("SemesterID");
				this.ddlDepartmentID.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All).SetSelectedValueIfNotPostback("DepartmentID");
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		#region Filters for Control Panel Record

		protected void btnAdd_OnClick(object sender, EventArgs e)
		{
			this.DisplayControlPanelModal(null);
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.StaffIdentity.InstituteID;
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			this.ddlProgramID.FillPrograms(instituteID, departmentID, true, CommonListItems.All).SetSelectedValueIfNotPostback("ProgramID");
			this.ddlProgramID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		#endregion

		#region Control Panel

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var dueDate = this.dtpDueDate.SelectedDate;
			if (dueDate == null)
				throw new InvalidOperationException();
			var semesterID = this.ddlSemesterForSave.SelectedValue.ToShort();
			var programIDs = this.cblPrograms.GetSelectedValues().Where(s => !string.IsNullOrWhiteSpace(s)).Select(s => s.ToInt()).ToList();
			var result = BL.Core.FeeManagement.Staff.FeeDueDates.AddOrUpdateFeeDueDates(semesterID, programIDs, dueDate.Value, this.StaffIdentity.LoginSessionGuid);
			switch (result.Status)
			{
				case BL.Core.FeeManagement.Staff.FeeDueDates.AddOrUpdateFeeDueDatesResult.Statuses.None:
					throw new InvalidOperationException();
				case BL.Core.FeeManagement.Staff.FeeDueDates.AddOrUpdateFeeDueDatesResult.Statuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<FeeDueDates>();
					return;
				case BL.Core.FeeManagement.Staff.FeeDueDates.AddOrUpdateFeeDueDatesResult.Statuses.Success:
					if (result.SuccessAdd > 0)
						this.AddSuccessAlert(result.SuccessAdd + " out of " + result.TotalRecords + " record(s) Added.", true);
					if (result.SuccessUpdate > 0)
						this.AddSuccessAlert(result.SuccessUpdate + " out of " + result.TotalRecords + " record(s) Updated.", true);
					this.RefreshPage();
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void gvFeeDueDates_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvFeeDueDates_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvFeeDueDates_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvFeeDueDates.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvFeeDueDates_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayControlPanelModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var resultDelete = BL.Core.FeeManagement.Staff.FeeDueDates.DeleteFeeDueDate(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					switch (resultDelete)
					{
						case BL.Core.FeeManagement.Staff.FeeDueDates.DeleteFeeDueDateStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.FeeManagement.Staff.FeeDueDates.DeleteFeeDueDateStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Fee Due Date");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.RefreshPage();
					break;
			}
		}

		protected void ddlDepartmentIDForSave_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.StaffIdentity.InstituteID;
			var departmentID = this.ddlDepartmentIDForSave.SelectedValue.ToNullableInt();
			if (departmentID == null)
				this.cblPrograms.FillProgramsWithDepartment(instituteID, null, true, CommonListItems.All);
			else
				this.cblPrograms.FillPrograms(instituteID, departmentID.Value, true, CommonListItems.All);
		}

		#endregion

		#region Page Specific Functions

		private void GetData(int pageIndex)
		{
			if (!this.IsPostBack)
			{
				var pageSize = this.Request.GetParameterValue<int>("PageSize");
				if (pageSize != null)
					this.gvFeeDueDates.PageSize = pageSize.Value;
			}
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var examMarkSettings = BL.Core.FeeManagement.Staff.FeeDueDates.GetFeeDueDates(semesterID, departmentID, programID, pageIndex, this.gvFeeDueDates.PageSize, sortExpression, sortDirection, out var virtualItemCount, this.StaffIdentity.LoginSessionGuid);
			this.gvFeeDueDates.DataBind(examMarkSettings, pageIndex, virtualItemCount, sortExpression, sortDirection);
		}

		private void DisplayControlPanelModal(int? feeDueDateID)
		{
			this.ddlSemesterForSave.FillSemesters();
			this.modalFeeDueDates.HeaderText = "Add/Edit";
			var instituteID = this.StaffIdentity.InstituteID;
			this.ddlDepartmentIDForSave.FillDepartments(instituteID, CommonListItems.All); this.ddlDepartmentIDForSave.ClearSelection();

			if (feeDueDateID == null)
			{
				this.ddlSemesterForSave.ClearSelection();
				this.dtpDueDate.SelectedDate = null;
				this.ddlDepartmentIDForSave_OnSelectedIndexChanged(null, null);
			}
			else
			{
				var feeDueDate = BL.Core.FeeManagement.Staff.FeeDueDates.GetFeeDueDate(feeDueDateID.Value, this.StaffIdentity.LoginSessionGuid);
				if (feeDueDate == null)
				{
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				}
				this.ddlSemesterForSave.SelectedValue = feeDueDate.SemesterID.ToString();
				this.dtpDueDate.SelectedDate = feeDueDate.DueDate;
				this.ddlDepartmentIDForSave.SelectedValue = feeDueDate.Program.DepartmentID.ToString();
				this.ddlDepartmentIDForSave_OnSelectedIndexChanged(null, null);
				this.cblPrograms.SelectedValue = feeDueDate.ProgramID.ToString();
			}
			this.modalFeeDueDates.Show();
		}

		#endregion
	}
}