﻿using Aspire.BL.Core.FeeManagement.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.FeeManagement
{
	[AspirePage("B801BA56-3F1D-416D-AB4A-ACC041DAB362", UserTypes.Staff, "~/Sys/Staff/FeeManagement/FeeChallan.aspx", "Fee Challan", true, AspireModules.FeeManagement)]
	public partial class FeeChallan : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.FeeManagement.Module, UserGroupPermission.Allowed}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_dollar_sign);
		public override string PageTitle => "Fee Challan";

		public static string GetPageUrl(short? semesterID, StudentFee.FeeTypes? feeType, string enrollment, int? applicationNo, bool? exemptWHT, bool search)
		{
			return typeof(FeeChallan).GetAspirePageAttribute().PageUrl.AttachQueryParams("SemesterID", semesterID, "FeeType", (byte)feeType, "Enrollment", enrollment, "ApplicationNo", applicationNo, "ExemptWHT", exemptWHT, "Search", search);
		}

		public static void Redirect(short? semesterID, StudentFee.FeeTypes? feeType, string enrollment, int? applicationNo, bool? exemptWHT, bool search)
		{
			Redirect(GetPageUrl(semesterID, feeType, enrollment, applicationNo, exemptWHT, search));
		}

		private void Refresh(bool search)
		{
			var semesterID = this.ViewState["SemesterID"] as short?;
			var enrollment = this.ViewState["Enrollment"] as string;
			var feeTypeEnum = this.ViewState["FeeType"] as StudentFee.FeeTypes?;
			var applicationNo = this.ViewState["ApplicationNo"] as int?;
			var exemptWHT = this.ViewState["ExemptWHT"] as bool?;
			Redirect(semesterID, feeTypeEnum, enrollment, applicationNo, exemptWHT, search);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterID.FillSemesters();
				if (this.ddlSemesterID.Items.Count == 0)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlFeeType.FillFeeTypes(null, StudentFee.FeeTypes.Attendance).SetSelectedValueIfNotPostback("FeeType");
				var concessionList = BL.Core.FeeManagement.Staff.FeeConcessionTypes.GetFeeConcessionTypesList(true, this.StaffIdentity.LoginSessionGuid);
				this.cblConcessions.DataBind(concessionList);

				this.panelChallan.Visible = false;
				this.ddlSemesterID.SetSelectedValueIfNotPostback("SemesterID");
				this.tbEnrollment.Enrollment = this.Request.GetParameterValue("Enrollment");
				this.tbApplicationNo.Text = this.Request.GetParameterValue("ApplicationNo");
				var search = this.Request.GetParameterValue<bool>("Search") == true;
				if (search && !string.IsNullOrWhiteSpace(this.tbEnrollment.Enrollment))
				{
					this.tbEnrollment_OnSearch(null, null);
					this.tbEnrollment.Focus();
				}
				else if (search && !string.IsNullOrWhiteSpace(this.tbApplicationNo.Text))
				{
					this.btnSearchAppNo_OnClick(null, null);
					this.tbApplicationNo.Focus();
				}
				else
					this.tbEnrollment.Focus();
			}
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs e)
		{
			if (this.IsPostbackAndNotValid)
				return;
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var feeTypeEnum = this.ddlFeeType.GetSelectedEnumValue<StudentFee.FeeTypes>();
			var enrollment = this.tbEnrollment.Enrollment;
			this.tbApplicationNo.Text = null;
			var exemptWHT = false;
			if (!this.IsPostBack)
				exemptWHT = this.Request.GetParameterValue<bool>("ExemptWHT") ?? false;
			this.GetData(semesterID, feeTypeEnum, enrollment, null, exemptWHT);
		}

		protected void btnSearchAppNo_OnClick(object sender, EventArgs e)
		{
			if (this.IsPostbackAndNotValid)
				return;
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var feeTypeEnum = this.ddlFeeType.GetSelectedEnumValue<StudentFee.FeeTypes>();
			var applicationNo = this.tbApplicationNo.Text.ToInt();
			this.tbEnrollment.Enrollment = null;
			var exemptWHT = false;
			if (!this.IsPostBack)
				exemptWHT = this.Request.GetParameterValue<bool>("ExemptWHT") ?? false;
			this.GetData(semesterID, feeTypeEnum, null, applicationNo, false);
		}

		private void GetData(short semesterID, StudentFee.FeeTypes feeTypeEnum, string enrollment, int? applicationNo, bool exemptWHT)
		{
			var feeInformation = BL.Core.FeeManagement.Staff.StudentFees.GetStudentFeeForGeneratingFeeChallan(semesterID, enrollment, applicationNo, this.StaffIdentity.LoginSessionGuid);
			if (feeInformation == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect(semesterID, feeTypeEnum, enrollment, applicationNo, null, false);
				return;
			}
			List<FeeStructureDetail> feeStructureDetails;
			if (feeInformation.StudentInfo != null)
			{
				if (feeInformation.FeeStructure == null)
				{
					this.AddErrorAlert($"Fee structure record not found for Program {feeInformation.StudentInfo.Program}({feeInformation.StudentInfo.IntakeSemesterID.ToSemesterString()}).");
					Redirect(semesterID, feeTypeEnum, enrollment, applicationNo, null, false);
					return;
				}
				if (feeInformation.StudentInfo.FeeChallanAlreadyGenerated)
					this.AddWarningAlert($"Fee Challan has already been generated for {semesterID.ToSemesterString()}");
				this.hlViewFeeChallans.NavigateUrl = ViewFeeChallans.GetPageUrl(feeInformation.StudentInfo.Enrollment, null, null, true);
				this.hlEnrollment.Text = feeInformation.StudentInfo.Enrollment;
				this.lblRegistration.Text = feeInformation.StudentInfo.RegistrationNo.ToNAIfNullOrEmpty().ToString();
				this.lblName.Text = feeInformation.StudentInfo.Name;
				this.lblCategory.Text = feeInformation.StudentInfo.CategoryEnum.ToFullName();
				this.lblProgram.Text = feeInformation.StudentInfo.Program;
				this.lblIntakeSemester.Text = feeInformation.StudentInfo.IntakeSemesterID.ToSemesterString();
				this.lblNationality.Text = feeInformation.StudentInfo.Nationality;
				this.lblStatus.Text = (feeInformation.StudentInfo.StatusEnum?.ToFullName()).ToNAIfNullOrEmpty();
				this.lblSemesterID.Text = semesterID.ToSemesterString();
				this.tbCreditHours.Text = feeInformation.CreditHours.ToString();

				feeStructureDetails = feeInformation.FeeStructure.FeeStructureDetails
					.Where(fsd => fsd.Category == null || fsd.Category == feeInformation.StudentInfo.Category)
					.Where(fsd => fsd.SemesterNo == null || fsd.SemesterNo == feeInformation.StudentInfo.SemesterNo).ToList();
				this.tbTuitionFee.Text = feeStructureDetails.SingleOrDefault(fsd => fsd.FeeHead.FeeHeadTypeEnum == FeeHead.FeeHeadTypes.TutionFee)?.Amount.ToString();
			}
			else
			{
				if (feeInformation.CandidateInfo.StudentID != null)
				{
					this.AddErrorAlert($"Fee Challan can not be generated for the Application No.: {feeInformation.CandidateInfo.ApplicationNo} of {feeInformation.CandidateInfo.IntakeSemester} because enrollment \"{feeInformation.CandidateInfo.Enrollment}\" has been generated.");
					Redirect(semesterID, feeTypeEnum, feeInformation.CandidateInfo.Enrollment, applicationNo, null, false);
					return;
				}
				if (feeInformation.FeeStructure == null)
				{
					this.AddErrorAlert($"Fee structure record not found for Program {feeInformation.CandidateInfo.Program}({feeInformation.CandidateInfo.IntakeSemesterID.ToSemesterString()}).");
					Redirect(semesterID, feeTypeEnum, enrollment, applicationNo, null, false);
					return;
				}

				if (feeInformation.CandidateInfo.FeeChallanAlreadyGenerated)
					this.AddWarningAlert($"Fee Challan has already been generated for {semesterID.ToSemesterString()}.");
				this.hlViewFeeChallans.NavigateUrl = ViewFeeChallans.GetPageUrl(null, feeInformation.CandidateInfo.IntakeSemesterID, feeInformation.CandidateInfo.ApplicationNo, true);
				this.hlApplicationNoC.Text = feeInformation.CandidateInfo.ApplicationNo.ToString();
				this.lblNameC.Text = feeInformation.CandidateInfo.Name;
				this.lblCategoryC.Text = feeInformation.CandidateInfo.CategoryEnum.ToFullName();
				this.lblProgramC.Text = feeInformation.CandidateInfo.Program;
				this.lblIntakeSemesterC.Text = feeInformation.CandidateInfo.IntakeSemesterID.ToSemesterString();
				this.lblNationalityC.Text = feeInformation.CandidateInfo.Nationality;
				this.lblSemesterID.Text = semesterID.ToSemesterString();
				this.tbCreditHours.Text = feeInformation.FeeStructure.FirstSemesterCreditHours.ToString();

				feeStructureDetails = feeInformation.FeeStructure.FeeStructureDetails
					.Where(fsd => fsd.Category == null || fsd.Category == feeInformation.CandidateInfo.Category)
					.Where(fsd => fsd.SemesterNo == null || fsd.SemesterNoEnum == SemesterNos.One).ToList();
				this.tbTuitionFee.Text = feeStructureDetails.SingleOrDefault(fsd => fsd.FeeHead.FeeHeadTypeEnum == FeeHead.FeeHeadTypes.TutionFee)?.Amount.ToString();
			}

			var feeHeads = feeInformation.FeeHeads.Select(feeHead => new FeeHeadUserControl.Head
			{
				FeeHeadID = feeHead.FeeHeadID,
				HeadName = feeHead.HeadName,
				FeeHeadTypeEnum = feeHead.FeeHeadTypeEnum,
				Concession = feeHead.Concession,
				RuleTypeEnum = feeHead.RuleTypeEnum,
				Refundable = feeHead.Refundable,
				WithholdingTaxApplicable = exemptWHT ? false : feeHead.WithholdingTaxApplicable,
				Amount = feeStructureDetails.FirstOrDefault(fsd => fsd.FeeHeadID == feeHead.FeeHeadID)?.Amount,
			}).ToList();

			var arrearsFeeHead = feeHeads.Find(h => h.FeeHeadTypeEnum == FeeHead.FeeHeadTypes.Arrears);
			if (arrearsFeeHead != null)
			{
				arrearsFeeHead.Amount = (arrearsFeeHead.Amount ?? 0) + (feeInformation.Arrears ?? 0);
				if (arrearsFeeHead.Amount == 0)
					arrearsFeeHead.Amount = null;
			}
			else if ((feeInformation.Arrears ?? 0) != 0)
				this.AddWarningAlert("Arrears Fee Head not found. Arrears: " + feeInformation.Arrears);
			this.ddlExemptWHT.FillYesNo().SetBooleanValue(exemptWHT);
			this.repeaterFeeChallan.DataBind(feeHeads);
			this.cblConcessions.DataBind(feeInformation.ConcessionList).Enabled = feeTypeEnum == StudentFee.FeeTypes.Challan;
			this.cblConcessions_OnSelectedIndexChanged(null, null);
			this.repeaterInstallments.DataBind(new[] { InstallmentNos.One });

			this.ViewState["SemesterID"] = semesterID;
			this.ViewState["StudentID"] = feeInformation.StudentInfo?.StudentID ?? feeInformation.CandidateInfo?.StudentID;
			this.ViewState["Enrollment"] = feeInformation.StudentInfo?.Enrollment ?? feeInformation.CandidateInfo?.Enrollment;
			this.ViewState["CandidateAppliedProgramID"] = feeInformation.CandidateInfo?.CandidateAppliedProgramID;
			this.ViewState["ApplicationNo"] = feeInformation.CandidateInfo?.ApplicationNo;
			this.ViewState["FeeType"] = feeTypeEnum;
			this.ViewState["AdmissionOpenProgramID"] = feeInformation.StudentInfo?.AdmissionOpenProgramID ?? feeInformation.CandidateInfo.AdmissionOpenProgramID;
			this.ViewState["Category"] = feeInformation.StudentInfo?.CategoryEnum ?? feeInformation.CandidateInfo.CategoryEnum;

			this.panelChallan.Visible = true;
			switch (feeTypeEnum)
			{
				case StudentFee.FeeTypes.Attendance:
					throw new InvalidOperationException();
				case StudentFee.FeeTypes.Challan:
					this.ltrlPanelTitle.Text = "Fee Challan";
					break;
				case StudentFee.FeeTypes.Refund:
					this.ltrlPanelTitle.Text = "Refund";
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(feeTypeEnum), feeTypeEnum, null);
			}
			this.tableStudentInfo.Visible = feeInformation.StudentInfo != null;
			this.tableCandidateInfo.Visible = feeInformation.CandidateInfo != null;

			if (feeInformation.StudentInfo != null)
			{
				this.tableCandidateInfo.Visible = false;
				this.tableStudentInfo.Visible = true;
				this.gvRegisteredCourses.Visible = true;
				this.gvRegisteredCourses.Caption = @"Registered Courses - " + semesterID.ToSemesterString();
				this.gvRegisteredCourses.DataBind(feeInformation.RegisteredCourses);
			}
			else
			{
				this.tableCandidateInfo.Visible = true;
				this.tableStudentInfo.Visible = false;
				this.gvRegisteredCourses.Visible = false;
				this.gvRegisteredCourses.DataBindNull();
			}
		}

		protected void btnFeeStructure_OnClick(object sender, EventArgs e)
		{
			this.ucFeeStructurePreviewUserControl.ShowDialog((int)this.ViewState["AdmissionOpenProgramID"], (Categories)this.ViewState["Category"]);
		}

		protected void cblConcessions_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var concession = this.cblConcessions.Items.Cast<ListItem>().Any(i => i.Selected);
			foreach (RepeaterItem item in this.repeaterFeeChallan.Items)
			{
				if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
					continue;
				var ucFeeHead = (FeeHeadUserControl)item.FindControl("ucFeeHead");
				ucFeeHead.Concession = concession;
			}
		}

		protected void cvInstallmentAmounts_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			var installmentsTotal = 0;
			foreach (RepeaterItem item in this.repeaterInstallments.Items)
			{
				if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
					continue;
				var ucInstallments = (InstallmentsUserControl)item.FindControl("ucInstallments");
				installmentsTotal += ucInstallments.Amount;
			}
			args.IsValid = (this.GrandTotalAmount == installmentsTotal);
		}

		private RepeaterItem FooterItem => this.repeaterFeeChallan.Controls.Cast<RepeaterItem>().Last(i => i.ItemType == ListItemType.Footer);
		private int TotalAmount => ((FeeHeadUserControl)this.FooterItem.FindControl("ucFeeHeadFooter")).TotalAmount ?? 0;
		private int TotalConcessionAmount => ((FeeHeadUserControl)this.FooterItem.FindControl("ucFeeHeadFooter")).ConcessionAmount ?? 0;
		private int GrandTotalAmount => ((FeeHeadUserControl)this.FooterItem.FindControl("ucFeeHeadFooter")).GrandTotalAmount;

		protected void btnSave_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var studentID = (int?)this.ViewState["StudentID"];
			var semesterID = (short)this.ViewState["SemesterID"];
			var feeTypeEnum = (StudentFee.FeeTypes)this.ViewState["FeeType"];
			var candidateAppliedProgramID = (int?)this.ViewState["CandidateAppliedProgramID"];

			switch (feeTypeEnum)
			{
				case StudentFee.FeeTypes.Attendance:
					throw new InvalidOperationException();
				case StudentFee.FeeTypes.Challan:
					break;
				case StudentFee.FeeTypes.Refund:
					break;
				default:
					throw new NotImplementedEnumException(feeTypeEnum);
			}

			var studentFee = new StudentFee
			{
				StudentFeeID = 0,
				SemesterID = semesterID,
				StudentID = studentID,
				CandidateAppliedProgramID = candidateAppliedProgramID,
				FeeTypeEnum = feeTypeEnum,
				CreatedDate = DateTime.Now,
				CreditHours = this.tbCreditHours.Text.ToDecimal(),
				NewAdmission = false,
				NoOfInstallmentsEnum = InstallmentNos.One,
				Remarks = this.tbRemarks.Text,
				TotalAmount = this.TotalAmount,
				ConcessionAmount = this.TotalConcessionAmount,
				GrandTotalAmount = this.GrandTotalAmount,
				StatusEnum = this.GrandTotalAmount == 0 ? StudentFee.Statuses.Paid : StudentFee.Statuses.NotPaid,
				ExemptWHT = this.ddlExemptWHT.SelectedValue.ToBoolean()
			};

			var feeConcessionTypeIDs = this.cblConcessions.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value.ToInt());
			foreach (var feeConcessionTypeID in feeConcessionTypeIDs)
				studentFee.StudentFeeConcessionTypes.Add(new StudentFeeConcessionType
				{
					FeeConcessionTypeID = feeConcessionTypeID,
				});

			var withholdingTaxFeeHeadFound = false;
			foreach (RepeaterItem item in this.repeaterFeeChallan.Items)
			{
				if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
					continue;
				var ucFeeHead = (FeeHeadUserControl)item.FindControl("ucFeeHead");
				var totalAmount = ucFeeHead.TotalAmount;
				var concessionAmount = ucFeeHead.ConcessionAmount;
				var grandTotalAmount = ucFeeHead.GrandTotalAmount;
				if (totalAmount != null || concessionAmount != null)
				{
					if (ucFeeHead.FeeHeadTypeEnum == FeeHead.FeeHeadTypes.WithholdingTax)
						withholdingTaxFeeHeadFound = true;
					studentFee.StudentFeeDetails.Add(new StudentFeeDetail
					{
						TotalAmount = totalAmount,
						ConcessionAmount = concessionAmount,
						GrandTotalAmount = grandTotalAmount,
						FeeHeadID = ucFeeHead.FeeHeadID ?? throw new InvalidOperationException()
					});
				}
			}

			if (withholdingTaxFeeHeadFound && studentFee.StudentFeeDetails.Count != 1)
			{
				this.AddErrorAlert("Generate a separate Fee Challan for Withholding Tax.");
				return;
			}

			foreach (RepeaterItem item in this.repeaterInstallments.Items)
			{
				if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
					continue;
				var ucInstallments = ((InstallmentsUserControl)item.FindControl("ucInstallments"));
				studentFee.StudentFeeChallans.Add(new StudentFeeChallan
				{
					Amount = ucInstallments.Amount,
					DueDate = ucInstallments.DueDate.Date,
					InstallmentNoEnum = ucInstallments.InstallmentNo,
					StatusEnum = ucInstallments.Amount == 0 ? StudentFeeChallan.Statuses.Paid : StudentFeeChallan.Statuses.NotPaid,
				});
			}

			var registeredCourseIDs = new List<int>();
			if (this.gvRegisteredCourses.Visible)
				foreach (GridViewRow row in this.gvRegisteredCourses.Rows)
				{
					if (row.RowType != DataControlRowType.DataRow)
						continue;
					var cb = (AspireCheckBox)row.FindControl("cb");
					if (cb.Visible && cb.Checked)
						registeredCourseIDs.Add(((Lib.WebControls.HiddenField)row.FindControl("hf")).Value.ToInt());
				}

			var result = StudentFees.AddStudentFee(studentFee, registeredCourseIDs, this.StaffIdentity.LoginSessionGuid);
			switch (result.Status)
			{
				case StudentFees.AddStudentFeeResult.Statuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.Refresh(true);
					return;
				case StudentFees.AddStudentFeeResult.Statuses.Success:
					if (studentFee.GrandTotalAmount == 0)
						ViewFeeChallans.Redirect(this.hlEnrollment.Text, null, null, true);
					else
						Reports.FeeChallans.Redirect(result.StudentFeeIDs, null);
					return;
				case StudentFees.AddStudentFeeResult.Statuses.EnrollmentGenerated:
					this.AddErrorAlert("Fee Challan can not be generated for Application No. whose enrollment have been generated.");
					this.Refresh(false);
					return;
				default:
					throw new NotImplementedEnumException(result.Status);
			}
		}

		protected void ddlExemptWHT_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ViewState["ExemptWHT"] = this.ddlExemptWHT.SelectedValue.ToBoolean();
			this.Refresh(true);
		}
	}
}