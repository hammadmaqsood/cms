﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="FeePayment.aspx.cs" Inherits="Aspire.Web.Sys.Staff.FeeManagement.FeePayment" %>

<%@ Register Src="~/Sys/Staff/Common/FeePaymentUserControl.ascx" TagPrefix="uc1" TagName="FeePaymentUserControl" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc1:FeePaymentUserControl runat="server" id="feePaymentUserControl" />
</asp:Content>
