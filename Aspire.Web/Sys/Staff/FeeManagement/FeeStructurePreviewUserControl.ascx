﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeeStructurePreviewUserControl.ascx.cs" Inherits="Aspire.Web.Sys.Staff.FeeManagement.FeeStructurePreviewUserControl" %>
<aspire:AspireModal runat="server" ID="modalPreview" ModalSize="Large" HeaderText="Fee Structure">
	<BodyTemplate>
		<asp:UpdatePanel runat="server">
			<ContentTemplate>
				<div class="form-horizontal">
					<div class="row">
						<div class="form-group col-md-6">
							<aspire:AspireLabel CssClass="col-md-2" runat="server" AssociatedControlID="lbProgram" Text="Program:" />
							<div class="col-md-10 form-control-static">
								<aspire:Label runat="server" ID="lbProgram" />
							</div>
						</div>
						<div class="form-group col-md-6">
							<aspire:AspireLabel CssClass="col-md-2" runat="server" AssociatedControlID="lbSemester" Text="Semester:" />
							<div class="col-md-10 form-control-static">
								<aspire:Label runat="server" ID="lbSemester" />
							</div>
						</div>
						<div class="form-group  col-md-6">
							<aspire:AspireLabel runat="server" CssClass="col-md-2" AssociatedControlID="ddlCategoryForPreview" Text="Category:" />
							<div class="col-md-10">
								<aspire:AspireDropDownList runat="server" ID="ddlCategoryForPreview" AutoPostBack="True" CausesValidation="true" ValidationGroup="Preview" OnSelectedIndexChanged="ddlCategoryForPreview_OnSelectedIndexChanged" />
							</div>
						</div>
					</div>
				</div>
				<p></p>
				<asp:PlaceHolder runat="server" ID="phPreview" />
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<aspire:AspireModalCloseButton runat="server">Close</aspire:AspireModalCloseButton>
	</FooterTemplate>
</aspire:AspireModal>
<script type="text/javascript">
	$(function () {
		function applySpinner(tb) {
			$("button", $(tb).prev()).off("click").click(function () {
				var value = $(tb).val().tryToDouble();
				if (value != null && value >= 1 && value <= 999)
					$(tb).val(Math.round10(value - 1, -2));
				if (value == null)
					$(tb).val(1);
				$(tb).change();
			});
			$("button", $(tb).next()).off("click").click(function () {
				var value = $(tb).val().tryToDouble();
				if (value != null && value >= 0 && value <= 998)
					$(tb).val(Math.round10(value + 1, -2));
				if (value == null)
					$(tb).val(1);
				$(tb).change();
			});
		}

		function applyPreview() {
			var table = $("table.Preview");
			$("input.Credits", $(table)).each(function (i, _tb) {
				$(_tb).prop("readonly", "readonly");
				$(_tb).change(function () {
					var credits = $(_tb).val().toDouble();
					var columnIndex = $(_tb).parents("td").index();
					var tutionFeePerSemester;
					$("tr.TutionFee.PerSemester", $(table)).each(function (i, tr) {
						tutionFeePerSemester = $(tr).children().eq(columnIndex).text().tryToInt();
					});
					if (tutionFeePerSemester == null)
						tutionFeePerSemester = 0;

					$("tr.TutionFee", $(table)).each(function (i, tr) {
						if (!$(tr).hasClass("PerSemester"))
							$(tr).children().eq(columnIndex).text(Math.ceil(credits * tutionFeePerSemester));
					});

					var sum = 0;
					$("tr", $(table)).each(function (i, tr) {
						if (!$(tr).hasClass("PerSemester") && !$(tr).hasClass("Footer") && !$(tr).hasClass("Header")) {
							var amount = $(tr).children().eq(columnIndex).text().tryToInt();
							if (amount != null)
								sum += amount;
						}
					});

					$("tr.Footer", $(table)).children().eq(columnIndex).text(sum);
				}).change();
				applySpinner(_tb);
			});
		}

		applyPreview();
		Sys.Application.add_load(applyPreview);
	});
</script>
