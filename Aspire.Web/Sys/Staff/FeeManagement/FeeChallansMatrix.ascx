﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeeChallansMatrix.ascx.cs" Inherits="Aspire.Web.Sys.Staff.FeeManagement.FeeChallansMatrix" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<asp:Repeater runat="server" ID="repeaterMatrix" ItemType="Aspire.Model.Entities.StudentFeeChallan">
	<HeaderTemplate>
		<div class="table-responsive">
			<table class="table table-bordered table-condensed table-hover">
				<caption>Detailed Report</caption>
				<thead>
					<tr>
						<th rowspan="2">Challan No.</th>
						<asp:Repeater runat="server" DataSource="<%# this.FeeHeads %>" ItemType="Aspire.Model.Entities.FeeHead">
							<ItemTemplate>
								<th colspan="3">
									<%#:$"{(Item.RuleTypeEnum== Aspire.Model.Entities.FeeHead.RuleTypes.Credit?"+":"-")}{Item.HeadName}" %>
								</th>
							</ItemTemplate>
						</asp:Repeater>
						<th colspan="3">=Total</th>
					</tr>
					<tr>
						<asp:Repeater runat="server" DataSource="<%# this.FeeHeads %>" ItemType="Aspire.Model.Entities.FeeHead">
							<ItemTemplate>
								<th>Total</th>
								<th>-Concession</th>
								<th>=Grand Total</th>
							</ItemTemplate>
						</asp:Repeater>
						<th>Total</th>
						<th>-Concession</th>
						<th>=Grand Total</th>
					</tr>
				</thead>
				<tbody>
	</HeaderTemplate>
	<ItemTemplate>
		<tr>
			<td title="<%# GetTooltip(Item, "Challan No.") %>">
				<aspire:HyperLink Target="_blank" runat="server" NavigateUrl="<%# Aspire.Web.Sys.Staff.FeeManagement.Reports.FeeChallans.GetPageUrl(Item.StudentFeeID) %>" Text="<%# Item.GetFullChallanNo(this.InstituteCode) %>" />
			</td>
			<asp:Repeater runat="server" DataSource="<%# this.GetStudentFeeDetails(Item) %>" ItemType="Aspire.Model.Entities.StudentFeeDetail">
				<ItemTemplate>
					<td class="text-right" title="<%# GetTooltip(Item, "Total") %>">
						<%# Item?.TotalAmount %>
					</td>
					<td class="text-right" title="<%# GetTooltip(Item, "Concession") %>">
						<%# Item?.ConcessionAmount %>
					</td>
					<td class="text-right" title="<%# GetTooltip(Item, "Grand Total") %>">
						<strong><%# Item?.GrandTotalAmount %></strong>
					</td>
				</ItemTemplate>
			</asp:Repeater>
			<td class="text-right" title="<%# GetTooltip(Item, "Total") %>">
				<%#: Item.StudentFee.TotalAmount %>
			</td>
			<td class="text-right" title="<%# GetTooltip(Item, "Concession") %>">
				<%#: Item.StudentFee.ConcessionAmount %>
			</td>
			<td class="text-right" title="<%# GetTooltip(Item, "Grand Total") %>">
				<strong><%#: Item.StudentFee.GrandTotalAmount %></strong>
			</td>
		</tr>
	</ItemTemplate>
	<FooterTemplate>
		</tbody>
		<tfoot>
			<tr>
				<th colspan="1">Total</th>
				<asp:Repeater runat="server" DataSource="<%# this.GetStudentFeeHeadTotals() %>" ItemType="Aspire.Model.Entities.StudentFeeDetail">
					<ItemTemplate>
						<th class="text-right" title="<%# Item.FeeHead.HeadName %> - Total"><%# Item.TotalAmount %></th>
						<th class="text-right" title="<%# Item.FeeHead.HeadName %> - Concession"><%# Item.ConcessionAmount %></th>
						<th class="text-right" title="<%# Item.FeeHead.HeadName %> - Grand Total"><%# Item.GrandTotalAmount %></th>
					</ItemTemplate>
				</asp:Repeater>
				<th><%# this.StudentFeeChallans.Sum(sfc=> sfc.StudentFee.TotalAmount) %></th>
				<th><%# this.StudentFeeChallans.Sum(sfc=> sfc.StudentFee.ConcessionAmount) %></th>
				<th><%# this.StudentFeeChallans.Sum(sfc=> sfc.StudentFee.GrandTotalAmount) %></th>
			</tr>
		</tfoot>
		</table>
		</div>
	</FooterTemplate>
</asp:Repeater>
