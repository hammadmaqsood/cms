﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.FeeManagement
{
	public partial class FeeStructurePreviewUserControl : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
				this.ddlCategoryForPreview.FillCategories();
		}

		protected void ddlCategoryForPreview_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.DisplayPreviewRecord();
		}

		private void DisplayPreviewRecord()
		{
			var admissionOpenProgramID = (int)this.ViewState["AdmissionOpenProgramID"];
			var feeStructure = BL.Core.FeeManagement.Staff.FeeStructures.GetFeeStructure(admissionOpenProgramID, StaffIdentity.Current.LoginSessionGuid);
			if (feeStructure == null)
				return;
			this.lbProgram.Text = feeStructure.AdmissionOpenProgram.Program.ProgramAlias;
			this.lbSemester.Text = feeStructure.AdmissionOpenProgram.SemesterID.ToSemesterString();
			var table = new Table { CssClass = "table table-bordered table-striped table-hover Preview" };
			var dictionary = new Dictionary<SemesterNos, int>();
			var headerRow = new TableRow { TableSection = TableRowSection.TableHeader, CssClass = "Header" };
			table.Rows.Add(headerRow);
			headerRow.Cells.Add(new TableHeaderCell { Text = "Fee Head", Wrap = false });
			foreach (SemesterNos semesterNoEnum in Enum.GetValues(typeof(SemesterNos)))
				dictionary.Add(semesterNoEnum, headerRow.Cells.Add(new TableHeaderCell { Text = $"Semester No. {semesterNoEnum.ToFullName()}", CssClass = "col-md-2" }));

			var getRow = new Func<bool, TableRow>(delegate (bool headerCells)
			 {
				 var tableRow = new TableRow { TableSection = TableRowSection.TableBody };
				 Enumerable.Range(0, headerRow.Cells.Count).ForEach(i => tableRow.Cells.Add(headerCells || i == 0 ? new TableHeaderCell { Wrap = false } : new TableCell()));
				 table.Rows.Add(tableRow);
				 return tableRow;
			 });

			var addRow = new Func<bool, TableRow>(delegate (bool headerCells)
			{
				var tableRow = getRow(headerCells);
				table.Rows.Add(tableRow);
				return tableRow;
			});

			var creditsRow = addRow(false);
			creditsRow.CssClass = "Credits";
			creditsRow.Cells[0].Text = "Credit Hours";
			foreach (var d in dictionary)
			{
				var cell = creditsRow.Cells[d.Value];
				var divInputGroup = new HtmlGenericControl("div");
				divInputGroup.Attributes.Add("class", "input-group input-group-sm");
				cell.Controls.Add(divInputGroup);
				{
					var divInputGroupBtn = new HtmlGenericControl("div");
					divInputGroupBtn.Attributes.Add("class", "input-group-btn");
					divInputGroup.Controls.Add(divInputGroupBtn);
					{
						var btnMinus = new HtmlGenericControl("button");
						btnMinus.Attributes.Add("type", "button");
						btnMinus.Attributes.Add("class", "btn btn-default Minus");
						divInputGroupBtn.Controls.Add(btnMinus);
						btnMinus.Controls.Add(new AspireGlyphicon { Icon = AspireGlyphicons.minus });
					}

					var input = new HtmlInputText("text") { Value = feeStructure.FirstSemesterCreditHours.ToString(), };
					input.Attributes.Add("class", "form-control Credits");
					input.Style.Add("min-width", "50px");
					divInputGroup.Controls.Add(input);

					divInputGroupBtn = new HtmlGenericControl("div");
					divInputGroupBtn.Attributes.Add("class", "input-group-btn");
					divInputGroup.Controls.Add(divInputGroupBtn);
					{
						var btnMinus = new HtmlGenericControl("button");
						btnMinus.Attributes.Add("type", "button");
						btnMinus.Attributes.Add("class", "btn btn-default Plus");
						divInputGroupBtn.Controls.Add(btnMinus);
						btnMinus.Controls.Add(new AspireGlyphicon { Icon = AspireGlyphicons.plus });
					}
				}
			}

			var category = this.ddlCategoryForPreview.GetSelectedEnumValue<Categories>();

			var feeHeadIDs = feeStructure.FeeStructureDetails.Where(fsd => fsd.CategoryEnum == null || fsd.CategoryEnum == category).OrderBy(fsd => fsd.FeeHead.DisplayIndex).Select(fsd => fsd.FeeHeadID).Distinct().ToList();

			TableRow tutionFeeRow = null;
			List<FeeStructureDetail> tutionFeeStructureDetails = null;
			foreach (var feeHeadID in feeHeadIDs)
			{
				var feeStructureDetails = feeStructure.FeeStructureDetails.Where(fsd => fsd.FeeHeadID == feeHeadID && (fsd.CategoryEnum == null || fsd.CategoryEnum == category)).ToList();
				var feeHead = feeStructureDetails.First().FeeHead;

				var row = addRow(false);
				if (feeHead.FeeHeadTypeEnum == FeeHead.FeeHeadTypes.TutionFee)
				{
					tutionFeeRow = row;
					row = addRow(false);
					tutionFeeStructureDetails = feeStructureDetails;
				}
				foreach (var feeStructureDetail in feeStructureDetails)
				{
					row.Cells[0].Text = feeStructureDetail.FeeHead.HeadName;
					row.CssClass = feeStructureDetail.FeeHead.FeeHeadTypeEnum.ToString();
					if (feeStructureDetail.SemesterNo == null)
						foreach (var d in dictionary)
							row.Cells[d.Value].Text = feeStructureDetail.Amount.ToString();
					else
						row.Cells[dictionary[feeStructureDetail.SemesterNoEnum.Value]].Text = feeStructureDetail.Amount.ToString();
				}
			}

			if (tutionFeeRow != null)
			{
				var row = tutionFeeRow;
				var feeHead = tutionFeeStructureDetails.First().FeeHead;
				row.Cells[0].Text = feeHead.HeadName + " (Per Credit Hour)";
				row.CssClass = feeHead.FeeHeadTypeEnum + " PerSemester";
				foreach (var feeStructureDetail in tutionFeeStructureDetails)
				{
					if (feeStructureDetail.SemesterNo == null)
						foreach (var d in dictionary)
							row.Cells[d.Value].Text = feeStructureDetail.Amount.ToString();
					else
						row.Cells[dictionary[feeStructureDetail.SemesterNoEnum.Value]].Text = feeStructureDetail.Amount.ToString();
				}
			}

			var footerRow = addRow(true);
			footerRow.TableSection = TableRowSection.TableFooter;
			footerRow.Cells[0].Text = "Total";
			footerRow.CssClass = "Footer";

			this.phPreview.Controls.Clear();
			var div = new HtmlGenericControl("div");
			div.Attributes.Add("class", "table-responsive");
			div.Controls.Add(table);
			this.phPreview.Controls.Add(div);
		}

		public void ShowDialog(int admissionOpenProgramID, Categories? category = null)
		{
			this.ddlCategoryForPreview.GetSelectedEnumValue(category);
			this.ViewState["AdmissionOpenProgramID"] = admissionOpenProgramID;
			this.DisplayPreviewRecord();
			this.modalPreview.Show();
		}
	}
}