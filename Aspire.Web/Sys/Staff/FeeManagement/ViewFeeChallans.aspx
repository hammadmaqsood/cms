﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ViewFeeChallans.aspx.cs" Inherits="Aspire.Web.Sys.Staff.FeeManagement.ViewFeeChallans" %>

<%@ Import Namespace="Aspire.Web.Sys.Staff.FeeManagement.Reports" %>
<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Import Namespace="Aspire.Web.Sys.Staff.FeeManagement" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Register Src="~/Sys/Staff/FeeManagement/FeeChallansMatrix.ascx" TagPrefix="uc1" TagName="FeeChallansMatrix" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<style type="text/css">
		.form-inline .form-group .input-group .input-group-addon {
			padding: 0;
			border-width: 0;
		}

			.form-inline .form-group .input-group .input-group-addon .form-control {
				border-radius: inherit;
				width: auto;
			}
	</style>
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
					<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="searchEnrollment" OnSearch="tbEnrollment_OnSearch" />
				</div>
				<asp:Panel runat="server" CssClass="form-group" DefaultButton="btnSearchApplicationNo">
					<aspire:AspireLabel runat="server" Text="Application No.:" AssociatedControlID="tbApplicationNo" />
					<div class="input-group">
						<span class="input-group-addon">
							<aspire:AspireDropDownList runat="server" ID="ddlSemester" ValidationGroup="searchAppNo" />
						</span>
						<aspire:AspireTextBox runat="server" ID="tbApplicationNo" ValidationGroup="searchAppNo" />
						<div class="input-group-btn">
							<aspire:AspireButton runat="server" Glyphicon="arrow_right" ValidationGroup="searchAppNo" ID="btnSearchApplicationNo" ButtonType="Primary" OnClick="btnSearchApplicationNo_OnClick" />
						</div>
					</div>
					<aspire:AspireInt32Validator runat="server" ControlToValidate="tbApplicationNo" ValidationGroup="searchAppNo" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Application No." RequiredErrorMessage="This field is required." AllowNull="False" />
				</asp:Panel>
				<div class="form-group">
					<aspire:AspireHyperLink runat="server" Text="Clear" NavigateUrl="ViewFeeChallans.aspx" />
				</div>
			</div>
		</div>
	</div>
	<asp:Panel runat="server" ID="panelStudentInfoEnrollment">
		<table class="table table-bordered table-condensed studentInfo tableCol4">
			<caption>Student Information</caption>
			<tbody>
				<tr>
					<th>Enrollment</th>
					<td>
						<aspire:AspireHyperLink runat="server" ID="hlEnrollment" />
					</td>
					<th>Registration No.</th>
					<td>
						<aspire:Label runat="server" ID="lblRegistrationNo" /></td>
				</tr>
				<tr>
					<th>Name</th>
					<td>
						<aspire:Label runat="server" ID="lblName" /></td>
					<th>Father Name</th>
					<td>
						<aspire:Label runat="server" ID="lblFatherName" /></td>
				</tr>
				<tr>
					<th>Program</th>
					<td>
						<aspire:Label runat="server" ID="lblProgram" /></td>
					<th>Intake Semester</th>
					<td>
						<aspire:Label runat="server" ID="lblIntakeSemester" /></td>
				</tr>
				<tr>
					<th>Nationality</th>
					<td>
						<aspire:Label runat="server" ID="lblNationality" /></td>
					<th>Status</th>
					<td>
						<aspire:Label runat="server" ID="lblStatus" /></td>
				</tr>
			</tbody>
		</table>
	</asp:Panel>
	<asp:Panel runat="server" ID="panelCandidateInfoApplicationNo">
		<table class="table table-bordered table-condensed studentInfo tableCol4">
			<caption>Candidate Information</caption>
			<tbody>
				<tr>
					<th>Application No.</th>
					<td>
						<aspire:Label runat="server" ID="lblApplicationNo" />
					</td>
					<th>Name</th>
					<td>
						<aspire:Label runat="server" ID="lblNameA" /></td>
				</tr>
				<tr>
					<th>Father Name</th>
					<td>
						<aspire:Label runat="server" ID="lblFatherNameA" /></td>
					<th>Category</th>
					<td>
						<aspire:Label runat="server" ID="lblCategory" /></td>
				</tr>
				<tr>
					<th>Program</th>
					<td>
						<aspire:Label runat="server" ID="lblProgramA" /></td>
					<th>Intake Semester</th>
					<td>
						<aspire:Label runat="server" ID="lblIntakeSemesterA" /></td>
				</tr>
				<tr>
					<th>Nationality</th>
					<td colspan="3">
						<aspire:Label runat="server" ID="lblNationalityA" /></td>
				</tr>
			</tbody>
		</table>
	</asp:Panel>
	<asp:Panel runat="server" ID="panelFeeDetails">
		<div class="form-inline">
			<div class="form-group">
				<aspire:AspireHyperLinkButton ButtonType="Success" Glyphicon="plus" Text="Create New Fee Challan" runat="server" ID="hlNewFeeChallan" Target="_blank" />
			</div>
			<div class="form-group">
				<aspire:AspireHyperLinkButton ButtonType="Primary" Text="Refund Fee" runat="server" ID="hlRefundFee" Target="_blank" />
			</div>
			<div class="form-group">
				<aspire:AspireButton Glyphicon="cog" OnClick="btnExecuteFeeDefaulterProcess_OnClick" ID="btnExecuteFeeDefaulterProcess" CausesValidation="False" ConfirmMessage="Are you sure you want to execute the fee defaulter process?" ButtonType="Primary" Text="Execute Fee Defaulter Process" runat="server" />
			</div>
			<div class="form-group pull-right">
				<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterViewChallans" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterViewChallans" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterViewChallans_OnSelectedIndexChanged" />
			</div>
		</div>
		<asp:Repeater runat="server" ID="repeaterStudentFees" OnItemCommand="repeaterStudentFees_OnItemCommand" ItemType="Aspire.Model.Entities.StudentFeeChallan">
			<HeaderTemplate>
				<table class="table table-bordered table-condensed table-hover table-striped">
					<caption>Student Fee</caption>
					<thead>
						<tr>
							<th>Semester</th>
							<th>Type</th>
							<th>Challan No.</th>
							<th>Total</th>
							<th>Concession</th>
							<th>Grand Total</th>
							<th>Created Date</th>
							<th>Due Date</th>
							<th>Status</th>
							<th>Deposit Date</th>
							<th>Remarks</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td>
						<%#: Item.StudentFee.Semester %>
					</td>
					<td>
						<%#: Item.StudentFee.FeeTypeEnum.ToFullName() %>
					</td>
					<td>
						<%#: AspireFormats.ChallanNo.GetFullChallanNoString(this.StaffIdentity.InstituteCode, Item.StudentFee.NewAdmission ? InstituteBankAccount.FeeTypes.NewAdmissionFee:InstituteBankAccount.FeeTypes.StudentFee, Item.StudentFeeChallanID) %>
					</td>
					<td>
						<%#: $"Rs. {Item.StudentFee.TotalAmount}" %>
					</td>
					<td>
						<%#: $"Rs. {Item.StudentFee.ConcessionAmount}" %>
					</td>
					<td>
						<%#: $"Rs. {Item.StudentFee.GrandTotalAmount}" %>
					</td>
					<td>
						<%#: $"{Item.StudentFee.CreatedDate:dd-MMM-yyyy}".ToNAIfNullOrEmpty() %>
					</td>
					<td>
						<aspire:AspireLinkButton Enabled="<%# !Item.IsZero %>" EncryptedCommandArgument='<%# Item.StudentFeeChallanID %>' ToolTip="Edit Due Date" runat="server" Text='<%# $"{Item.DueDate:dd-MMM-yyyy}" %>' CausesValidation="False" CommandName="EditDueDate" />
					</td>
					<td>
						<%#: Item.StatusFullName %>
					</td>
					<td>
						<%#: $"{Item.AccountTransaction?.TransactionDate:dd-MMM-yyyy}".ToNAIfNullOrEmpty() %>
					</td>
					<td>
						<aspire:AspireLinkButton EncryptedCommandArgument='<%# Item.StudentFeeChallanID %>' ToolTip="Edit Remarks" runat="server" Text='<%# Item.StudentFee.Remarks.ToNAIfNullOrEmpty() %>' CausesValidation="False" CommandName="EditRemarks" />
						<span runat="server" class="label label-danger" title="Withholding Tax is exempted on this challan." visible="<%# Item.StudentFee.ExemptWHT %>">WHT Exempted</span>
					</td>
					<td>
						<aspire:AspireHyperLinkButton runat="server" Visible="<%# !Item.IsZero && !Item.StudentFee.IsZero %>" ToolTip="View/Print Fee Challan" ButtonType="OnlyIcon" Glyphicon="print" NavigateUrl='<%# FeeChallans.GetPageUrl(Item.StudentFee.StudentFeeID) %>' Target="_blank" />
						<aspire:AspireHyperLinkButton runat="server" Visible="<%# !Item.IsZero && !Item.StudentFee.IsZero %>" ToolTip="Edit Fee Challan" ButtonType="OnlyIcon" Glyphicon="edit" NavigateUrl='<%# FeePayment.GetPageUrl(Item.StudentFee.NewAdmission, Item.StudentFeeChallanID, true) %>' Target="_blank" />
						<aspire:AspireLinkButton CausesValidation="False" runat="server" ToolTip="Delete Fee Challan" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%# Item.StudentFee.StudentFeeID %>' ConfirmMessage='<%# $"Are you sure you want to delete student fee with its {Item.StudentFee.NoOfInstallments} fee challan(s)?" %>' Visible='<%# Item.IsZero || (!Item.IsZero && Item.StatusEnum != StudentFeeChallan.Statuses.Paid)%>' />
					</td>
				</tr>
			</ItemTemplate>
			<FooterTemplate>
				</tbody></table>
			</FooterTemplate>
		</asp:Repeater>
		<asp:Repeater runat="server" ID="repeaterWithHoldingTaxes" ItemType="Aspire.BL.Core.FeeManagement.Common.StudentFees.WithHoldingTax">
			<HeaderTemplate>
				<table class="table table-bordered table-condensed table-hover table-striped">
					<caption>Withholding Tax</caption>
					<thead>
						<tr>
							<th>Financial Year</th>
							<th>Taxable Amount</th>
							<th>WHT Criteria</th>
							<th>Total Tax</th>
							<th>Tax Generated</th>
							<th>Tax Remaining</th>
						</tr>
					</thead>
					<tbody>
			</HeaderTemplate>
			<ItemTemplate>
				<tr class='<%# Item.Remaining == 0 ? "success" : (Item.Remaining < 0 ? "warning" : "danger") %>'>
					<td>
						<%#: $"{Item.FinancialYear} ({Item.MinDate:d} - {Item.MaxDateExclusive.AddDays(-1):d})" %>
					</td>
					<td>
						<%#: $"Rs. {Item.TaxableAmount}" %>
					</td>
					<td>
						<%#: Item.WithholdingTaxSetting == null ? "".ToNAIfNullOrEmpty() : $"{Item.WithholdingTaxSetting.WithholdingTaxPercentage}% if Taxable Amount > {Item.WithholdingTaxSetting.ApplicableOnAmount -1}" %>
					</td>
					<td>
						<%#: Item.Tax %>
					</td>
					<td>
						<%#: Item.Given %>
					</td>
					<td>
						<%#: Item.Remaining %>
					</td>
				</tr>
			</ItemTemplate>
			<FooterTemplate>
				</tbody>
				</table>
				</div>
			</FooterTemplate>
		</asp:Repeater>
		<uc1:FeeChallansMatrix runat="server" ID="feeChallansMatrix" />
		<aspire:AspireModal runat="server" ID="modalEditFeeChallan" HeaderText="Edit Fee Challan">
			<BodyTemplate>
				<asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updatePanelModalEditFeeChallan">
					<ContentTemplate>
						<aspire:AspireAlert runat="server" ID="alertEditFeeChallan" />
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Challan No.:" AssociatedControlID="lblFullChallanNo" />
							<div>
								<aspire:Label runat="server" ID="lblFullChallanNo" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Amount:" AssociatedControlID="lblAmount" />
							<div>
								<aspire:Label runat="server" ID="lblAmount" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Exempt Withholding Tax for this Challan?" AssociatedControlID="ddlExemptWHT" />
							<aspire:AspireDropDownList runat="server" ID="ddlExemptWHT" ValidationGroup="UpdateFeeChallan" />
							<aspire:AspireRequiredFieldValidator ErrorMessage="This field is required." runat="server" ControlToValidate="ddlExemptWHT" />
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Due Date:" AssociatedControlID="dtpDueDate" />
							<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpDueDate" ValidationGroup="UpdateFeeChallan" />
							<aspire:AspireDateTimeValidator RequiredErrorMessage="This field is required." AllowNull="False" runat="server" ControlToValidate="dtpDueDate" InvalidDataErrorMessage="Invalid Data." InvalidRangeErrorMessage="Invalid Date Range." />
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" />
							<aspire:AspireTextBox Rows="3" TextMode="MultiLine" MaxLength="1000" runat="server" ID="tbRemarks" ValidationGroup="UpdateFeeChallan" />
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</BodyTemplate>
			<FooterTemplate>
				<asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updatePanelModalEditFeeChallanButtons">
					<ContentTemplate>
						<aspire:AspireButton Text="Update" runat="server" ID="btnUpdateFeeChallan" ValidationGroup="UpdateFeeChallan" ButtonType="Primary" OnClick="btnUpdateFeeChallan_OnClick" />
						<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
					</ContentTemplate>
				</asp:UpdatePanel>
			</FooterTemplate>
		</aspire:AspireModal>
	</asp:Panel>
</asp:Content>
