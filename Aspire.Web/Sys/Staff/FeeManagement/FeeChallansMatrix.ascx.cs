﻿using Aspire.Model.Entities;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Staff.FeeManagement
{
	public partial class FeeChallansMatrix : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
		}

		protected string InstituteCode { get; private set; }
		protected List<StudentFeeChallan> StudentFeeChallans { get; private set; }

		protected List<FeeHead> FeeHeads { get; private set; }

		internal void DataBind(List<StudentFeeChallan> studentFeeChallans)
		{
			switch (AspireIdentity.Current.UserType)
			{
				case Model.Entities.Common.UserTypes.Student:
					this.InstituteCode = StudentIdentity.Current.InstituteCode;
					break;
				case Model.Entities.Common.UserTypes.Staff:
					this.InstituteCode = StaffIdentity.Current.InstituteCode;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(AspireIdentity.Current.UserType), AspireIdentity.Current.UserType, null);
			}
			this.StudentFeeChallans = studentFeeChallans.Where(sfc => sfc.StudentFee.FeeTypeEnum == StudentFee.FeeTypes.Challan).OrderBy(sfc => sfc.StudentFee.SemesterID).ThenBy(sfc => sfc.StudentFeeChallanID).ToList();
			this.FeeHeads = this.StudentFeeChallans.SelectMany(sfc => sfc.StudentFee.StudentFeeDetails).Select(sfd => sfd.FeeHead).Distinct().OrderBy(fh => fh.DisplayIndex).ToList();
			this.repeaterMatrix.DataBind(this.StudentFeeChallans);
		}

		protected List<StudentFeeDetail> GetStudentFeeDetails(StudentFeeChallan studentFeeChallan)
		{
			return this.FeeHeads.Select(fh => studentFeeChallan.StudentFee.StudentFeeDetails.SingleOrDefault(sfd => sfd.FeeHeadID == fh.FeeHeadID)).ToList();
		}

		protected List<StudentFeeDetail> GetStudentFeeHeadTotals()
		{
			return this.FeeHeads.Select(fh =>
			{
				var studentFeeChallans = this.StudentFeeChallans.SelectMany(sfc => sfc.StudentFee.StudentFeeDetails).Where(sfd => sfd.FeeHeadID == fh.FeeHeadID).ToList();
				if (!studentFeeChallans.Any())
					return null;
				return new StudentFeeDetail
				{
					FeeHead = fh,
					TotalAmount = studentFeeChallans.Sum(sfc => sfc.TotalAmount),
					ConcessionAmount = studentFeeChallans.Sum(sfc => sfc.ConcessionAmount),
					GrandTotalAmount = studentFeeChallans.Sum(sfc => sfc.GrandTotalAmount),
				};
			}).ToList();
		}

		protected static string GetTooltip(StudentFeeChallan studentFeeChallan, string headName)
		{
			return null;
		}

		protected static string GetTooltip(StudentFeeDetail studentFeeDetail, string headName)
		{
			return null;
		}
	}
}