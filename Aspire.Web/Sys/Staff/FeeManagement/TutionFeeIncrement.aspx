﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="TutionFeeIncrement.aspx.cs" Inherits="Aspire.Web.Sys.Staff.FeeManagement.TutionFeeIncrement" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<asp:Content ContentPlaceHolderID="HeadPH" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Up to Semester:" AssociatedControlID="ddlUpToSemesterIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlUpToSemesterIDFilter" AutoPostBack="true" OnSelectedIndexChanged="ddlUpToSemesterIDFilter_SelectedIndexChanged" />
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentIDFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" AutoPostBack="true" OnSelectedIndexChanged="ddlProgramIDFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Category:" AssociatedControlID="ddlCategoryFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlCategoryFilter" AutoPostBack="true" OnSelectedIndexChanged="ddlCategoryFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="ddlSemesterNoFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterNoFilter" AutoPostBack="true" OnSelectedIndexChanged="ddlSemesterNoFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Fee Head:" AssociatedControlID="ddlFeeHeadIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlFeeHeadIDFilter" AutoPostBack="true" OnSelectedIndexChanged="ddlFeeHeadIDFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Increment:" AssociatedControlID="ddlIncrementFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlIncrementFilter" AutoPostBack="true" OnSelectedIndexChanged="ddlIncrementFilter_SelectedIndexChanged">
					<asp:ListItem Text="5%" Value="5" />
				</aspire:AspireDropDownList>
			</div>
		</div>
	</div>
	<aspire:AspireGridView runat="server" AllowSorting="true" ID="gvTutionFees" ItemType="Aspire.BL.Core.FeeManagement.Staff.FeeStructures.FeeStructureDetail" AutoGenerateColumns="false" OnSorting="gvTutionFees_Sorting">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate>
					<%#: Container.DataItemIndex + 1 %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Intake Semester" SortExpression="SemesterID">
				<ItemTemplate>
					<%#: Item.SemesterID.ToSemesterString() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Department" SortExpression="DepartmentAlias">
				<ItemTemplate>
					<%#: Item.DepartmentAlias.ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Program" SortExpression="ProgramAlias">
				<ItemTemplate>
					<%#: Item.ProgramAlias %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Category" SortExpression="CategoryEnum">
				<ItemTemplate>
					<%#: (Item.CategoryEnum?.ToFullName()) ?? "All" %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Semester No." SortExpression="SemesterNoEnum">
				<ItemTemplate>
					<%#: (Item.SemesterNoEnum?.ToFullName()) ?? "All" %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Fee Head" SortExpression="HeadAlias">
				<ItemTemplate>
					<%#: Item.HeadAlias %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Amount" SortExpression="Amount">
				<ItemTemplate>
					<%#: $"Rs. {Item.Amount}" %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Increment">
				<ItemTemplate>
					<%#: $"{Item.IncrementPercentage}% = Rs. {Item.IncrementAmount}" %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Amount After Increment">
				<ItemTemplate>
					<%#: $"Rs. {Item.AmountAfterIncrement}" %>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<aspire:AspireButton runat="server" Text="Update Fee Structure" ID="btnUpdateFee" CausesValidation="false" ConfirmMessage="Note: This process is not reversible. Are you sure you want to continue?" OnClick="btnUpdateFee_Click" />
</asp:Content>
