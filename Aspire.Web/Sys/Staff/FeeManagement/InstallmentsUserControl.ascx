﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InstallmentsUserControl.ascx.cs" Inherits="Aspire.Web.Sys.Staff.FeeManagement.InstallmentsUserControl" %>

<div class="form-group form-group-sm Installment">
	<aspire:AspireLabel CssClass="col-xs-4 col-sm-3 col-md-3 col-lg-3" runat="server" Text="Installment:" ID="lblInstallmentNo" AssociatedControlID="tbAmount" />
	<div class="col-xs-6 col-sm-6 col-md-5 col-lg-4 ">
		<div class="input-group input-group-sm">
			<span class="input-group-addon">Rs.</span>
			<aspire:AspireTextBox runat="server" ID="tbAmount" ValidationGroup="FeeChallan" CssClass="Amount" />
			<div class="input-group-addon">Due Date:</div>
			<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpDueDate" ValidationGroup="FeeChallan" />
		</div>
	</div>
	<div class="col-xs-2 col-sm-3 col-md-4 col-lg-5 form-control-static">
		<aspire:AspireInt32Validator AllowNull="False" ControlToValidate="tbAmount" runat="server" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Amount." ValidationGroup="FeeChallan" />
		<aspire:AspireDateTimeValidator AllowNull="False" runat="server" RequiredErrorMessage="This field is required." ControlToValidate="dtpDueDate" ValidationGroup="FeeChallan" InvalidDataErrorMessage="Invalid Date." />
	</div>
</div>
