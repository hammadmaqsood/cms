﻿using Aspire.BL.Core.FeeManagement.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.FeeManagement
{
	[AspirePage("DD660E1C-7EA4-412F-8B53-DF54220A7EDB", UserTypes.Staff, "~/Sys/Staff/FeeManagement/ViewFeeChallans.aspx", "View Fee Challans", true, AspireModules.FeeManagement)]
	public partial class ViewFeeChallans : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.FeeManagement.Module,new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.usd);
		public override string PageTitle => "View Fee Challans";

		public static string GetPageUrl(string enrollment, short? intakeSemesterID, int? applicationNo, bool search = true)
		{
			return typeof(ViewFeeChallans).GetAspirePageAttribute().PageUrl.AttachQueryParams("IntakeSemesterID", intakeSemesterID, "Enrollment", enrollment, "ApplicationNo", applicationNo, "Search", search);
		}

		public static void Redirect(string enrollment, short? intakeSemesterID, int? applicationNo, bool search = true)
		{
			Redirect(GetPageUrl(enrollment, intakeSemesterID, applicationNo, search));
		}

		private void Refresh(bool search = true)
		{
			Redirect(this.tbEnrollment.Enrollment, this.ddlSemester.SelectedValue.ToNullableShort(), this.tbApplicationNo.Text.TryToInt(), search);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState[nameof(Model.Entities.Student.StudentID)] = null;
				this.ViewState[nameof(CandidateAppliedProgram.CandidateAppliedProgramID)] = null;
				this.panelStudentInfoEnrollment.Visible = false;
				this.panelCandidateInfoApplicationNo.Visible = false;
				this.panelFeeDetails.Visible = false;

				this.ddlSemester.FillSemesters();
				this.ddlSemesterViewChallans.FillSemesters(CommonListItems.All);

				this.tbEnrollment.Enrollment = this.Request.GetParameterValue("Enrollment");
				this.ddlSemester.SetSelectedValueIfNotPostback("IntakeSemesterID");
				this.tbApplicationNo.SetTextIfNotPostback("ApplicationNo");
				var search = this.Request.GetParameterValue<bool>("Search") == true;
				if (search && !string.IsNullOrWhiteSpace(this.tbEnrollment.Enrollment))
				{
					this.tbEnrollment_OnSearch(null, null);
					this.tbEnrollment.Focus();
				}
				else if (search && !string.IsNullOrWhiteSpace(this.tbApplicationNo.Text))
				{
					this.btnSearchApplicationNo_OnClick(null, null);
					this.tbApplicationNo.Focus();
				}
				else
					this.tbEnrollment.Focus();
			}
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs args)
		{
			if (this.IsPostbackAndNotValid)
				return;
			this.GetData(this.tbEnrollment.Enrollment, null, null);
		}

		protected void btnSearchApplicationNo_OnClick(object sender, EventArgs e)
		{
			if (this.IsPostbackAndNotValid)
				return;
			this.GetData(null, this.ddlSemester.SelectedValue.ToShort(), this.tbApplicationNo.Text.ToInt());
		}

		private void GetData(string enrollment, short? semesterID, int? applicationNo)
		{
			this.panelCandidateInfoApplicationNo.Visible = false;
			this.panelStudentInfoEnrollment.Visible = false;
			if (enrollment != null)
				this.DisplayStudentInformation(enrollment);
			else if (applicationNo != null && semesterID != null)
				this.DisplayCandidateInformation(semesterID.Value, applicationNo.Value);
			else
				throw new InvalidOperationException();
		}

		private void DisplayStudentInformation(string enrollment)
		{
			var studentInfo = BL.Common.Common.GetBasicStudentInformation(this.StaffIdentity.InstituteID, enrollment);
			if (studentInfo == null)
			{
				this.AddNoRecordFoundAlert();
				this.Refresh(false);
				return;
			}


			this.hlEnrollment.Text = studentInfo.Enrollment;
			this.lblRegistrationNo.Text = studentInfo.RegistrationNo.ToString().ToNAIfNullOrEmpty();
			this.lblName.Text = studentInfo.Name;
			this.lblFatherName.Text = studentInfo.FatherName;
			this.lblProgram.Text = studentInfo.ProgramAlias;
			this.lblIntakeSemester.Text = studentInfo.SemesterID.ToSemesterString();
			this.lblNationality.Text = studentInfo.Nationality;
			this.lblStatus.Text = (studentInfo.StatusEnum?.ToFullName()).ToNAIfNullOrEmpty();
			this.ddlSemesterViewChallans.ClearSelection();

			this.panelStudentInfoEnrollment.Visible = true;
			this.panelFeeDetails.Visible = true;
			this.ViewState[nameof(Model.Entities.Student.Enrollment)] = studentInfo.Enrollment;
			this.ViewState[nameof(Model.Entities.CandidateAppliedProgram.ApplicationNo)] = null;
			this.ViewState["IntakeSemesterID"] = null;
			this.ViewState[nameof(Model.Entities.Student.StudentID)] = studentInfo.StudentID;
			this.ViewState[nameof(CandidateAppliedProgram.CandidateAppliedProgramID)] = null;
			this.ddlSemesterViewChallans_OnSelectedIndexChanged(null, null);
		}

		private void DisplayCandidateInformation(short semesterID, int applicationNo)
		{
			var candidateInfo = BL.Core.FeeManagement.Staff.StudentFees.GetCandidateInfo(applicationNo, semesterID, this.StaffIdentity.LoginSessionGuid);
			if (candidateInfo == null)
			{
				this.AddNoRecordFoundAlert();
				this.Refresh(false);
				return;
			}


			this.lblApplicationNo.Text = candidateInfo.ApplicationNo.ToString();
			this.lblNameA.Text = candidateInfo.Name;
			this.lblFatherNameA.Text = candidateInfo.FatherName;
			this.lblCategory.Text = candidateInfo.CategoryEnumFullName;
			this.lblProgramA.Text = candidateInfo.Program;
			this.lblIntakeSemesterA.Text = candidateInfo.SemesterID.ToSemesterString();
			this.lblNationalityA.Text = candidateInfo.Nationality;

			this.panelCandidateInfoApplicationNo.Visible = true;
			this.panelFeeDetails.Visible = true;
			this.ViewState[nameof(Model.Entities.Student.Enrollment)] = null;
			this.ViewState[nameof(Model.Entities.CandidateAppliedProgram.ApplicationNo)] = candidateInfo.ApplicationNo;
			this.ViewState["IntakeSemesterID"] = candidateInfo.SemesterID;
			this.ViewState[nameof(Model.Entities.Student.StudentID)] = null;
			this.ViewState[nameof(CandidateAppliedProgram.CandidateAppliedProgramID)] = candidateInfo.CandidateAppliedProgramID;
			this.ddlSemesterViewChallans.FillSemesters(candidateInfo.SemesterID);
			this.ddlSemesterViewChallans_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterViewChallans_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterViewChallans.SelectedValue.ToNullableShort();
			var enrollment = this.ViewState["Enrollment"] as string;
			var applicationNo = this.ViewState["ApplicationNo"] as int?;
			var intakeSemesterID = this.ViewState["IntakeSemesterID"] as short?;

			this.hlNewFeeChallan.NavigateUrl = applicationNo != null ?
				FeeChallan.GetPageUrl(intakeSemesterID, StudentFee.FeeTypes.Challan, null, applicationNo, null, true)
				: FeeChallan.GetPageUrl(semesterID, StudentFee.FeeTypes.Challan, enrollment, null, null, true);
			this.hlRefundFee.NavigateUrl = applicationNo != null ?
				FeeChallan.GetPageUrl(intakeSemesterID, StudentFee.FeeTypes.Refund, null, applicationNo, null, true)
				: FeeChallan.GetPageUrl(semesterID, StudentFee.FeeTypes.Refund, enrollment, null, null, true);

			var studentID = (int?)this.ViewState[nameof(Model.Entities.Student.StudentID)];
			var candidateAppliedProgramID = (int?)this.ViewState[nameof(CandidateAppliedProgram.CandidateAppliedProgramID)];
			var (studentFeeChallans, withHoldingTaxes) = BL.Core.FeeManagement.Staff.StudentFees.GetStudentFeeChallans(studentID, candidateAppliedProgramID, semesterID, this.StaffIdentity.LoginSessionGuid);
			this.repeaterStudentFees.DataBind(studentFeeChallans);
			this.feeChallansMatrix.DataBind(studentFeeChallans);
			this.feeChallansMatrix.Visible = studentFeeChallans?.Any() == true;
			this.repeaterWithHoldingTaxes.DataBind(withHoldingTaxes);
		}

		protected void repeaterStudentFees_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					var studentFeeID = e.DecryptedCommandArgumentToInt();
					var status = BL.Core.FeeManagement.Staff.StudentFees.DeleteStudentFee(studentFeeID, this.StaffIdentity.LoginSessionGuid);
					switch (status)
					{
						case StudentFees.DeleteStudentFeeStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case StudentFees.DeleteStudentFeeStatuses.CannotDeleteFeePaid:
							this.AddErrorAlert("Fee cannot be deleted because it has been marked as paid. or Updated by Bank.");
							break;
						case StudentFees.DeleteStudentFeeStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Fee");
							break;
						default:
							throw new NotImplementedEnumException(status);
					}
					this.Refresh();
					break;
				case "EditRemarks":
				case "EditDueDate":
					var studentFeeChallanID = e.DecryptedCommandArgumentToInt();
					this.ViewState["StudentFeeChallanID"] = null;
					var studentFeeChallan = BL.Core.FeeManagement.Staff.StudentFees.GetStudentFeeChallan(studentFeeChallanID, this.StaffIdentity.LoginSessionGuid);
					if (studentFeeChallan == null)
					{
						this.AddNoRecordFoundAlert();
						this.Refresh(true);
						return;
					}

					this.ViewState["StudentFeeChallanID"] = studentFeeChallan.StudentFeeChallanID;
					this.lblFullChallanNo.Text = AspireFormats.ChallanNo.GetFullChallanNoString(this.StaffIdentity.InstituteCode, studentFeeChallan.StudentFee.NewAdmission ? InstituteBankAccount.FeeTypes.NewAdmissionFee : InstituteBankAccount.FeeTypes.StudentFee, studentFeeChallan.StudentFeeChallanID);
					this.lblAmount.Text = $"Rs. {studentFeeChallan.Amount}";
					this.ddlExemptWHT.FillYesNo().SetBooleanValue(studentFeeChallan.StudentFee.ExemptWHT);
					this.ddlExemptWHT.Enabled = !studentFeeChallan.StudentFee.StudentFeeDetails.Any(sfd => sfd.FeeHead.FeeHeadTypeEnum == FeeHead.FeeHeadTypes.WithholdingTax);
					this.dtpDueDate.SelectedDate = studentFeeChallan.DueDate;
					this.dtpDueDate.Enabled = studentFeeChallan.StatusEnum != StudentFeeChallan.Statuses.Paid;
					this.tbRemarks.Text = studentFeeChallan.StudentFee.Remarks;
					this.modalEditFeeChallan.Show();
					break;

			}
		}

		protected void btnUpdateFeeChallan_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var studentFeeChallanID = (int)this.ViewState["StudentFeeChallanID"];
			var exemptWHT = this.ddlExemptWHT.SelectedValue.ToBoolean();
			var dueDate = this.dtpDueDate.SelectedDate.Value;
			var remarks = this.tbRemarks.Text;
			var status = BL.Core.FeeManagement.Staff.StudentFees.UpdateDueDateAndRemarks(studentFeeChallanID, exemptWHT, dueDate, remarks, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case StudentFees.UpdateDueDateAndRemarksStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.Refresh(true);
					return;
				case StudentFees.UpdateDueDateAndRemarksStatuses.CannotUpdateDueDateBecauseFeePaid:
					this.alertEditFeeChallan.AddErrorAlert("Due Date of a paid challan can not be updated.");
					this.updatePanelModalEditFeeChallan.Update();
					return;
				case StudentFees.UpdateDueDateAndRemarksStatuses.Success:
					this.AddSuccessMessageHasBeenUpdated("Fee Challan");
					this.Refresh(true);
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		protected void btnExecuteFeeDefaulterProcess_OnClick(object sender, EventArgs e)
		{
			var studentID = (int)this.ViewState[nameof(Model.Entities.Student.StudentID)];
			var reasons = StudentFees.ProcessFeeDefaulter(studentID, this.StaffIdentity.LoginSessionGuid);
			this.AddSuccessAlert($"Fee Defaulter Process has been executed for the selected student.");
			reasons.Where(r => r.IsWarning).ForEach(r => this.AddWarningAlert(r.ToString(false)));
			reasons.Where(r => !r.IsWarning).ForEach(r => this.AddErrorAlert(r.ToString(false)));
		}
	}
}