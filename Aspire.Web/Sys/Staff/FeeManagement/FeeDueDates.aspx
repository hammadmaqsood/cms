﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="FeeDueDates.aspx.cs" Inherits="Aspire.Web.Sys.Staff.FeeManagement.FeeDueDates" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireButton runat="server" ID="btnAdd" Text="Add/Edit" CausesValidation="False" Glyphicon="pencil" ButtonType="Success" OnClick="btnAdd_OnClick" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
					<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
					<aspire:AspireDropDownList runat="server" ID="ddlProgramID" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlProgramID_OnSelectedIndexChanged" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvFeeDueDates" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnSorting="gvFeeDueDates_OnSorting" OnPageIndexChanging="gvFeeDueDates_OnPageIndexChanging" OnPageSizeChanging="gvFeeDueDates_OnPageSizeChanging" OnRowCommand="gvFeeDueDates_OnRowCommand">
		<Columns>
			<asp:BoundField HeaderText="Semester" DataField="Semester" SortExpression="Semester" />
			<asp:BoundField HeaderText="Department" DataField="DepartmentAlias" SortExpression="DepartmentAlias" />
			<asp:BoundField HeaderText="Program" DataField="ProgramAlias" SortExpression="ProgramAlias" />
			<asp:BoundField HeaderText="Due Date" DataField="DueDate" SortExpression="DueDate" DataFormatString="{0:D}" />
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument='<%#Eval("FeeDueDateID") %>' />
					<aspire:AspireLinkButton runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%#Eval("FeeDueDateID") %>' ConfirmMessage="Are you sure you want to delete this record?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<aspire:AspireModal runat="server" ID="modalFeeDueDates">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updateModalFeeDueDates" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertModalFeeDueDates" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterForSave" />
						<aspire:AspireDropDownList runat="server" ID="ddlSemesterForSave" ValidationGroup="RequiredControlPanel" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterForSave" ErrorMessage="This field is required." ValidationGroup="RequiredControlPanel" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Due Date:" AssociatedControlID="dtpDueDate" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpDueDate" />
						<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dtpDueDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." AllowNull="False" ValidationGroup="RequiredControlPanel" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDForSave" />
						<aspire:AspireDropDownList CausesValidation="False" runat="server" ID="ddlDepartmentIDForSave" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentIDForSave_OnSelectedIndexChanged" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Programs:" AssociatedControlID="cblPrograms" />
						<div>
							<aspire:AspireCheckBoxList runat="server" ID="cblPrograms" RepeatLayout="Flow" RepeatDirection="Vertical" />
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" ValidationGroup="RequiredControlPanel" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
	<script type="text/javascript">
		cblApplySelectAll("#<%=this.cblPrograms.ClientID%>");
	</script>
</asp:Content>
