﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.FeeManagement
{
	[AspirePage("E2AB3F76-B8BF-4C71-BD85-4DF2929C1774", UserTypes.Staff, "~/Sys/Staff/FeeManagement/FeePaymentFirstSemester.aspx", "Fee Payment (First Semester)", true, AspireModules.FeeManagement)]
	public partial class FeePaymentFirstSemester : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.FeeManagement.Module,new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.usd);
		public override string PageTitle => "Fee Payment (First Semester)";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Enrollment", SortDirection.Ascending);
				this.ddlSemesterID.FillSemesters();
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData();
		}

		protected void gvStudents_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData();
		}

		private void GetData()
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var students = Aspire.BL.Core.FeeManagement.Staff.AttachFeeChallanToCourse.GetStudentNotPaidRegisteredCourses(semesterID, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.StaffIdentity.LoginSessionGuid);
			var count = students.Count;
			this.gvStudents.DataBind(students, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
			this.gvStudents.Caption = $"Total {count} record found.";
			this.btnUpdateFeeStatus.Enabled = students.Any(s => s.CanSelect);
		}

		protected void btnUpdateFeeStatus_OnClick(object sender, EventArgs e)
		{
			var studentIDs = new List<int>();
			foreach (GridViewRow row in this.gvStudents.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow)
					continue;
				var studentID = (int)this.gvStudents.DataKeys[row.RowIndex][0];
				var cb = (AspireCheckBox)row.FindControl("cbSelect");
				if (cb.Visible && cb.Checked)
					studentIDs.Add(studentID);
			}
			if (!studentIDs.Any())
			{
				this.AddWarningAlert("No record selected.");
				return;
			}
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var status = Aspire.BL.Core.FeeManagement.Staff.AttachFeeChallanToCourse.AttachStudentFeeIDToFirstSemesterRegisteredCourses(semesterID, studentIDs, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.FeeManagement.Staff.AttachFeeChallanToCourse.AttachStudentFeeIDToFirstSemesterRegisteredCoursesStatuses.Success:
					this.AddSuccessAlert("Registered courses have been updated.");
					break;
				case BL.Core.FeeManagement.Staff.AttachFeeChallanToCourse.AttachStudentFeeIDToFirstSemesterRegisteredCoursesStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.GetData();
		}
	}
}