﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="FeePaymentFirstSemester.aspx.cs" Inherits="Aspire.Web.Sys.Staff.FeeManagement.FeePaymentFirstSemester" %>

<asp:Content runat="server" ContentPlaceHolderID="BodyPH">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Intake Semester:" AssociatedControlID="ddlSemesterID" />
			<aspire:AspireDropDownList ID="ddlSemesterID" ValidationGroup="Search" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvStudents" DataKeyNames="StudentID" AutoGenerateColumns="False" AllowSorting="True" OnSorting="gvStudents_OnSorting">
		<Columns>
			<asp:TemplateField>
				<HeaderTemplate>
					<aspire:AspireCheckBox runat="server" ID="cbSelectAll" ClientIDMode="Static" />
				</HeaderTemplate>
				<ItemTemplate>
					<aspire:AspireCheckBox Visible='<%# Eval("CanSelect") %>' runat="server" ID="cbSelect" ClientIDMode="Static" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%#: Container.DataItemIndex+1 %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="IntakeSemester" SortExpression="IntakeSemester" HeaderText="Intake Semester" />
			<asp:BoundField DataField="Enrollment" SortExpression="Enrollment" HeaderText="Enrollment" />
			<asp:BoundField DataField="Name" SortExpression="Name" HeaderText="Name" />
			<asp:BoundField DataField="Class" SortExpression="Class" HeaderText="Class" />
			<asp:BoundField DataField="RegisteredCourses" SortExpression="RegisteredCourses" HeaderText="Registered Courses" />
			<asp:BoundField DataField="FeeChallanExistsYesNo" SortExpression="FeeChallanExists" HeaderText="Fee Challan Exists" />
		</Columns>
	</aspire:AspireGridView>
	<div class="text-center">
		<aspire:AspireButton runat="server" ID="btnUpdateFeeStatus" ButtonType="Primary" Text="Update Fee Status" ConfirmMessage="Are you sure you want to continue?" CausesValidation="False" OnClick="btnUpdateFeeStatus_OnClick" />
	</div>
	<script type="text/javascript">
		$(function () {
			var gvStudents = $("#<%=this.gvStudents.ClientID%>");
			$("input#cbSelectAll", gvStudents).click(function () { $("input#cbSelect:checkbox", gvStudents).prop("checked", !$(this).prop("checked")).click(); });
		});
	</script>
</asp:Content>

