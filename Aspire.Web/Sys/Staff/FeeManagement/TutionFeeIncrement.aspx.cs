﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.FeeManagement
{
	[AspirePage("8fe3fc5d-2fd9-4544-9f7f-f14c21c835fb", UserTypes.Staff, "~/Sys/Staff/FeeManagement/TutionFeeIncrement.aspx", "Tution Fee Increment", true, AspireModules.FeeManagement)]
	public partial class TutionFeeIncrement : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.FeeManagement.ManageFeeStructure, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.edit);
		public override string PageTitle => "Tution Fee Increment";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(nameof(BL.Core.FeeManagement.Staff.FeeStructures.FeeStructureDetail.SemesterID), SortDirection.Descending);
				this.ddlUpToSemesterIDFilter.FillSemesters();
				this.ddlUpToSemesterIDFilter.SelectedIndex = 1;
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All);
				this.ddlCategoryFilter.FillCategories(CommonListItems.All);
				this.ddlSemesterNoFilter.FillSemesterNos(CommonListItems.All);
				var feeHeadsItems = BL.Core.FeeManagement.Staff.FeeStructures.GetFeeHeadsForIncrement(this.StaffIdentity.LoginSessionGuid)
					.Select(fh => new ListItem(fh.HeadAlias, fh.FeeHeadID.ToString()));
				this.ddlFeeHeadIDFilter.DataBind(feeHeadsItems);
				this.ddlUpToSemesterIDFilter_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlUpToSemesterIDFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, departmentID, true, CommonListItems.All);
			this.ddlProgramIDFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlCategoryFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlCategoryFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterNoFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlSemesterNoFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlFeeHeadIDFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlFeeHeadIDFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlIncrementFilter_SelectedIndexChanged(null, null);
		}

		protected void gvTutionFees_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData();
		}

		protected void ddlIncrementFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData();
		}

		private void GetData()
		{
			var upToSemesterID = this.ddlUpToSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var categoryEnum = this.ddlCategoryFilter.GetSelectedEnumValue<Categories>(null);
			var semesterNoEnum = this.ddlSemesterNoFilter.GetSelectedEnumValue<SemesterNos>(null);
			var feeHeadID = this.ddlFeeHeadIDFilter.SelectedValue.ToInt();
			var incrementPercentage = this.ddlIncrementFilter.SelectedValue.ToInt();
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var tutionFees = BL.Core.FeeManagement.Staff.FeeStructures.GetFeeStructureDetails(upToSemesterID, departmentID, programID, categoryEnum, semesterNoEnum, feeHeadID, incrementPercentage, sortExpression, sortDirection, this.StaffIdentity.LoginSessionGuid);
			this.gvTutionFees.DataBind(tutionFees, sortExpression, sortDirection);
			this.btnUpdateFee.Enabled = tutionFees.Any();
		}

		protected void btnUpdateFee_Click(object sender, EventArgs e)
		{
			var upToSemesterID = this.ddlUpToSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var categoryEnum = this.ddlCategoryFilter.GetSelectedEnumValue<Categories>(null);
			var semesterNoEnum = this.ddlSemesterNoFilter.GetSelectedEnumValue<SemesterNos>(null);
			var feeHeadID = this.ddlFeeHeadIDFilter.SelectedValue.ToInt();
			var incrementPercentage = this.ddlIncrementFilter.SelectedValue.ToInt();
			var (status, count) = BL.Core.FeeManagement.Staff.FeeStructures.UpdateFeeStructureDetails(upToSemesterID, departmentID, programID, categoryEnum, semesterNoEnum, feeHeadID, incrementPercentage, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.FeeManagement.Staff.FeeStructures.UpdateFeeStructureDetailsStatuses.Success:
					this.AddSuccessAlert($"{count} record(s) have been updated.");
					this.GetData();
					return;
				case BL.Core.FeeManagement.Staff.FeeStructures.UpdateFeeStructureDetailsStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.GetData();
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}
	}
}