﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.OfficialDocuments
{
	[AspirePage("0ED37CED-AEC5-4086-A38A-009CB582B456", UserTypes.Staff, "~/Sys/Staff/OfficialDocuments/Documents.aspx", "Official Documents", true, AspireModules.OfficialDocuments)]
	public partial class Documents : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.OfficialDocuments.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.book);
		public override string PageTitle => "Official Documents";

		protected void Page_Load(object sender, EventArgs e)
		{

		}
	}
}