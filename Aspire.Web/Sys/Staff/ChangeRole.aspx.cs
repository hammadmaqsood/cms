﻿using Aspire.Lib.Helpers;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Logins.Staff;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Staff
{
	[AspirePage("1D4F48FC-870E-410E-B19C-403371403DCE", UserTypes.Staff, "~/Sys/Staff/ChangeRole.aspx", null, false, AspireModules.None)]
	public partial class ChangeRole : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => null;
		public override PageIcon PageIcon => null;
		public override string PageTitle => null;

		public static string GetPageUrl(int userRoleID)
		{
			return GetAspirePageAttribute<ChangeRole>().PageUrl.AttachQueryParam("UserRoleID", userRoleID);
		}

		public static void Redirect(int userRoleID)
		{
			Redirect(GetPageUrl(userRoleID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			var newUserRoleID = this.Request.GetParameterValue<int>("UserRoleID");
			if (newUserRoleID == null || this.StaffIdentity.Roles.All(r => r.Key != newUserRoleID))
			{
				Redirect<Login>();
				return;
			}
			var loginResult = Aspire.BL.Core.LoginManagement.Staff.ChangeRole(this.StaffIdentity.LoginSessionGuid, newUserRoleID.Value, this.UserAgent, this.ClientIPAddress);
			if (loginResult.LoginFailureReason != null)
				Logoff.Redirect(UserLoginHistory.LogOffReasons.LogOff);
			else
			{
				var loginHistory = loginResult.LoginHistory;
				var staffIdentity = new StaffIdentity(loginHistory.Username, loginHistory.Name, loginHistory.Email, loginHistory.UserLoginHistoryID, loginHistory.LoginSessionGuid, loginHistory.UserID, loginHistory.UserRoleID, loginHistory.InstituteID, loginHistory.InstituteCode, loginHistory.InstituteName, loginHistory.InstituteShortName, loginHistory.InstituteAlias, loginHistory.Roles.ToDictionary(r => r.UserRoleID, r => r.InstituteAlias), this.ClientIPAddress, this.UserAgent);
				SessionHelper.CreateUserSession(staffIdentity, GetPageUrl<Sys.Staff.PrepareSession>());
			}
		}
	}
}