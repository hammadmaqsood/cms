﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StudentInfoUserControl.ascx.cs" Inherits="Aspire.Web.Sys.Staff.Common.StudentInfoUserControl" %>
<div class="table-responsive">
	<table class="table table-bordered table-condensed tableCol4">
		<caption>Student Information</caption>
		<tr>
			<th>Enrollment</th>
			<td>
				<aspire:AspireHyperLink runat="server" ID="hlEnrollment" /></td>
			<th>Registration No.</th>
			<td>
				<aspire:Label runat="server" ID="lblRegistrationNo" /></td>
		</tr>
		<tr>
			<th>Name</th>
			<td>
				<aspire:Label runat="server" ID="lblName" /></td>
			<th>Father Name</th>
			<td>
				<aspire:Label runat="server" ID="lblFatherName" /></td>
		</tr>
		<tr>
			<th>Program</th>
			<td>
				<aspire:Label runat="server" ID="lblProgramAlias" /></td>
			<th>Intake Semester</th>
			<td>
				<aspire:Label runat="server" ID="lblIntakeSemester" /></td>
		</tr>
		<tr>
			<th>Status</th>
			<td>
				<aspire:Label runat="server" ID="lblStatus" /></td>
			<th>Status Date</th>
			<td>
				<aspire:Label runat="server" ID="lblStatusDate" /></td>
		</tr>
	</table>
</div>
