﻿using Aspire.BL.Common;
using Aspire.BL.Core.ExamSeatingPlan.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Common
{
	public partial class DateSheets : System.Web.UI.UserControl
	{
		private new StaffPage Page => (StaffPage)base.Page;
		private IStaffIdentity StaffIdentity => this.Page.StaffIdentity;
		private bool ForCoordinators => this.Page is CourseOffering.DateSheets;

		private void RefreshPage()
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var examType = this.ddlExamType.GetSelectedEnumValue<ExamConduct.ExamTypes>(null);
			var examConductID = this.ddlExamConductID.SelectedValue.ToNullableInt();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
			var sectionEnum = this.ddlSection.GetSelectedEnumValue<Sections>(null);
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>(null);
			var facultyMemberID = this.ddlFacultyMemberID.SelectedValue.ToNullableInt();
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			string pageUrl;
			if (this.Page is ExamSeatingPlan.DateSheets)
				pageUrl = BasePage.GetPageUrl<ExamSeatingPlan.DateSheets>();
			else if (this.Page is CourseOffering.DateSheets)
				pageUrl = BasePage.GetPageUrl<CourseOffering.DateSheets>();
			else
				throw new InvalidOperationException();
			pageUrl = pageUrl.AttachQueryParams("SemesterID", semesterID, "ExamType", (byte?)examType, "ExamConductID", examConductID, "DepartmentID", departmentID, "ProgramID", programID, "SemesterNo", (short?)semesterNoEnum, "Section", (int?)sectionEnum, "Shift", (byte?)shiftEnum, "FacultyMemberID", facultyMemberID, "SortExpression", sortExpression, "SortDirection", sortDirection);
			this.Response.Redirect(pageUrl);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var sortExpression = this.Request.GetParameterValue("SortExpression") ?? "Title";
				var sortDirection = this.Request.GetParameterValue<SortDirection>("SortDirection") ?? SortDirection.Ascending;
				this.ViewState.SetSortProperties(sortExpression, sortDirection);
				this.ViewState.SetSortProperties1("Enrollment", SortDirection.Ascending);
				this.ddlSemesterID.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
				this.ddlExamType.FillExamConductExamTypes().SetSelectedValueIfNotPostback("ExamType");
				this.ddlDepartmentID.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All);
				if (this.ddlDepartmentID.Items.Count > 0)
					this.ddlDepartmentID.SelectedIndex = 1;
				this.ddlDepartmentID.SetSelectedValueIfNotPostback("DepartmentID");

				this.ddlSemesterNo.FillSemesterNos(CommonListItems.All).SetSelectedValueIfNotPostback("SemesterNo");
				this.ddlSection.FillSections(CommonListItems.All).SetSelectedValueIfNotPostback("Section");
				this.ddlShift.FillShifts(CommonListItems.All).SetSelectedValueIfNotPostback("Shift");
				this.ddlDisplayClashesCount.FillYesNo().SetBooleanValue(false);
				var facultyMembers = BL.Core.Common.Lists.GetFacultyMembersList(this.StaffIdentity.InstituteID, null);
				this.ddlFacultyMemberID.DataBind(facultyMembers, CommonListItems.All).SetSelectedValueIfNotPostback("FacultyMemberID");
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		#region Filters

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			if (semesterID == null)
				this.ddlExamConductID.Items.Clear();
			else
			{
				var examTypeEnum = this.ddlExamType.GetSelectedEnumValue<ExamConduct.ExamTypes>();
				var examConducts = ExamConducts.GetExamConductsList(semesterID.Value, examTypeEnum, this.StaffIdentity.LoginSessionGuid);
				this.ddlExamConductID.DataBind(examConducts).SetSelectedValueIfNotPostback("ExamConductID");
			}
			this.ddlExamConductID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamConductID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			if (departmentID == null)
				this.ddlProgramID.FillPrograms(this.StaffIdentity.InstituteID, null, true, CommonListItems.All).SetSelectedValueIfNotPostback("ProgramID");
			else
				this.ddlProgramID.FillPrograms(this.StaffIdentity.InstituteID, departmentID.Value, false, CommonListItems.All).SetSelectedValueIfNotPostback("ProgramID");
			this.ddlProgramID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterNo_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterNo_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSection_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSection_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlShift_OnSelectedIndexChanged(null, null);
		}

		protected void ddlShift_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlFacultyMemberID_SelectedIndexChanged(null, null);
		}

		protected void ddlFacultyMemberID_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDisplayClashesCount_SelectedIndexChanged(null, null);
		}

		protected void ddlDisplayClashesCount_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.lbtnViewOverallClashes.Enabled = this.ddlExamConductID.SelectedValue.ToNullableInt() != null;
			this.GetData();
		}

		#endregion

		protected void gvDateSheet_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayModalDateSheet(e.DecryptedCommandArgumentToInt());
					break;
				case "ViewClashes":
					this.DisplayClashes(e.DecryptedCommandArgumentToInt());
					break;
			}
		}

		protected void gvDateSheet_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData();
		}

		private void DisplayModalDateSheet(int offeredCourseID)
		{
			if (this.ForCoordinators)
				this.StaffIdentity.Demand(StaffPermissions.CourseOffering.ManageExamDateSheet, UserGroupPermission.PermissionValues.Allowed);
			else
				this.StaffIdentity.Demand(StaffPermissions.ExamSeatingPlan.ManageExamDateSheet, UserGroupPermission.PermissionValues.Allowed);

			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var examConductID = this.ddlExamConductID.SelectedValue.ToInt();
			var examDateAndSessions = ExamDateAndSessions.GetExamDateAndSessionsList(examConductID, this.StaffIdentity.LoginSessionGuid);
			this.ddlExamDateAndSession.DataBind(examDateAndSessions, CommonListItems.None);

			var offeredCourseDetail = ExamDateSheets.GetOfferedCourseDetail(examConductID, offeredCourseID, this.StaffIdentity.LoginSessionGuid);
			if (offeredCourseDetail == null)
			{
				this.Page.AddNoRecordFoundAlert();
				this.RefreshPage();
				return;
			}
			this.lblSemester.Text = semesterID.ToSemesterString();
			this.lblExamType.Text = offeredCourseDetail.ExamTypeEnum.ToFullName();
			this.lblExamConductName.Text = offeredCourseDetail.ExamConductName;
			this.lblCourseTitle.Text = offeredCourseDetail.Title;
			this.lblTeacher.Text = offeredCourseDetail.Teacher;
			this.lblClass.Text = offeredCourseDetail.Class;
			if (offeredCourseDetail.ExamDateAndSessionID == null)
			{
				this.ddlExamDateAndSession.ClearSelection();
				this.rfvddlExamDateAndSession.Visible = true;
				this.rfvddlExamDateAndSession.Enabled = true;
				this.dtpQuestionPaperReceivedDate.SelectedDate = offeredCourseDetail.QuestionPaperReceivedDate;

				this.divAnswerSheetsReceivedByExaminerDate.Visible = false;
				this.divAnswerSheetsReceivedByExaminerCount.Visible = false;
				this.divAnswerSheetsReceivedByExamCellDate.Visible = false;
				this.divAnswerSheetsReceivedByExamCellCount.Visible = false;
				this.dtpAnswerSheetsReceivedByExaminerDate.SelectedDate = null;
				this.tbAnswerSheetsReceivedByExaminerCount.Text = null;
				this.dtpAnswerSheetsReceivedByExamCellDate.SelectedDate = null;
				this.tbAnswerSheetsReceivedByExamCellCount.Text = null;

				this.modalDateSheet.HeaderText = "Add Date Sheet";
				this.btnSave.Text = "Add";
			}
			else
			{
				this.ddlExamDateAndSession.SelectedValue = offeredCourseDetail.ExamDateAndSessionID.ToString();
				this.rfvddlExamDateAndSession.Visible = false;
				this.rfvddlExamDateAndSession.Enabled = false;
				this.dtpQuestionPaperReceivedDate.SelectedDate = offeredCourseDetail.QuestionPaperReceivedDate;

				this.divAnswerSheetsReceivedByExaminerDate.Visible = true;
				this.divAnswerSheetsReceivedByExaminerCount.Visible = true;
				this.divAnswerSheetsReceivedByExamCellDate.Visible = true;
				this.divAnswerSheetsReceivedByExamCellCount.Visible = true;
				this.dtpAnswerSheetsReceivedByExaminerDate.SelectedDate = offeredCourseDetail.AnswerSheetsReceivedByExaminerDate;
				this.tbAnswerSheetsReceivedByExaminerCount.Text = offeredCourseDetail.AnswerSheetsReceivedByExaminerCount.ToString();
				this.dtpAnswerSheetsReceivedByExamCellDate.SelectedDate = offeredCourseDetail.AnswerSheetsReceivedByExamCellDate;
				this.tbAnswerSheetsReceivedByExamCellCount.Text = offeredCourseDetail.AnswerSheetsReceivedByExamCellCount.ToString();

				this.modalDateSheet.HeaderText = "Edit Date Sheet";
				this.btnSave.Text = "Save";
			}

			this.divQuestionPaperReceived.Visible = !this.ForCoordinators;
			this.divAnswerSheetsReceivedByExaminerDate.Visible = !this.ForCoordinators;
			this.divAnswerSheetsReceivedByExaminerCount.Visible = !this.ForCoordinators;
			this.divAnswerSheetsReceivedByExamCellDate.Visible = !this.ForCoordinators;
			this.divAnswerSheetsReceivedByExamCellCount.Visible = !this.ForCoordinators;

			this.btnDeleteSeats.Visible = !this.ForCoordinators;
			this.ViewState[nameof(offeredCourseDetail.OfferedCourseID)] = offeredCourseID;
			this.modalDateSheet.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.Page.IsValid)
				return;
			var offeredCourseID = (int)this.ViewState[nameof(OfferedCours.OfferedCourseID)];
			var examConductID = this.ddlExamConductID.SelectedValue.ToInt();
			var examDateAndSessionID = this.ddlExamDateAndSession.SelectedValue.ToNullableInt();
			var questionPaperReceivedDate = this.dtpQuestionPaperReceivedDate.SelectedDate;

			var answerSheetsReceivedByExaminerDate = this.dtpAnswerSheetsReceivedByExaminerDate.SelectedDate;
			var answerSheetsReceivedByExaminerCount = this.tbAnswerSheetsReceivedByExaminerCount.Text.ToNullableInt();
			var answerSheetsReceivedByExamCellDate = this.dtpAnswerSheetsReceivedByExamCellDate.SelectedDate;
			var answerSheetsReceivedByExamCellCount = this.tbAnswerSheetsReceivedByExamCellCount.Text.ToNullableInt();
			var status = ExamDateSheets.UpdateExamDateSheet(offeredCourseID, this.ForCoordinators, examConductID, examDateAndSessionID, questionPaperReceivedDate, answerSheetsReceivedByExaminerDate, answerSheetsReceivedByExaminerCount, answerSheetsReceivedByExamCellDate, answerSheetsReceivedByExamCellCount, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case ExamDateSheets.UpdateExamDateSheetStatuses.NoRecordFound:
					this.Page.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case ExamDateSheets.UpdateExamDateSheetStatuses.NoChangeFound:
				case ExamDateSheets.UpdateExamDateSheetStatuses.Success:
					this.Page.AddSuccessMessageHasBeenUpdated("Date Sheet");
					this.RefreshPage();
					return;
				case ExamDateSheets.UpdateExamDateSheetStatuses.CannotUpdateDueToChildRecordExistsExamSeats:
					this.alertModalDateSheet.AddErrorMessageCannotBeUpdatedBecauseChildRecordExists("Date Sheet");
					this.updateModalDateSheet.Update();
					return;
				case ExamDateSheets.UpdateExamDateSheetStatuses.DateSheetExistsInOtherExamConduct:
					this.alertModalDateSheet.AddErrorAlert("Date Sheet exists in some other Exam Conduct record.");
					this.updateModalDateSheet.Update();
					return;
				case ExamDateSheets.UpdateExamDateSheetStatuses.CannotUpdateDueToQuestionPaperReceived:
					this.alertModalDateSheet.AddErrorAlert("Date Sheet cannot be updated because Question Papers have been received.");
					this.updateModalDateSheet.Update();
					return;
				case ExamDateSheets.UpdateExamDateSheetStatuses.CannotUpdateDueToAnswerSheetsReceivedByExaminer:
					this.alertModalDateSheet.AddErrorAlert("Date Sheet cannot be updated because Answer Sheet have been received by Examiner.");
					this.updateModalDateSheet.Update();
					return;
				case ExamDateSheets.UpdateExamDateSheetStatuses.CannotUpdateDueToAnswerSheetsReceivedByExamCell:
					this.alertModalDateSheet.AddErrorAlert("Date Sheet cannot be updated because Answer Sheet have been received by Exam Cell.");
					this.updateModalDateSheet.Update();
					return;
				case ExamDateSheets.UpdateExamDateSheetStatuses.InvalidData:
					this.alertModalDateSheet.AddErrorAlert("Question Papers and Answer Sheets Receiving dates and count must be in sequence.");
					this.updateModalDateSheet.Update();
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		private void GetData()
		{
			var examConductID = this.ddlExamConductID.SelectedValue.ToNullableInt();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			if (examConductID == null)
			{
				this.gvDateSheet.DataBindNull();
				return;
			}
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
			var sectionEnum = this.ddlSection.GetSelectedEnumValue<Sections>(null);
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>(null);
			var facultyMemberID = this.ddlFacultyMemberID.SelectedValue.ToNullableInt();
			var calculateClashes = this.ddlDisplayClashesCount.SelectedValue.ToBoolean();

			var examDateSheets = ExamDateSheets.GetExamDateSheets(examConductID.Value, calculateClashes, departmentID, programID, semesterNoEnum, sectionEnum, shiftEnum, facultyMemberID, this.StaffIdentity.LoginSessionGuid);
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			switch (this.ViewState.GetSortExpression())
			{
				case nameof(ExamDateSheets.ExamDateSheet.Title):
					examDateSheets = examDateSheets.OrderBy(sortDirection, eds => eds.Title).ToList();
					break;
				case nameof(ExamDateSheets.ExamDateSheet.Teacher):
					examDateSheets = examDateSheets.OrderBy(sortDirection, eds => eds.Teacher).ToList();
					break;
				case nameof(ExamDateSheets.ExamDateSheet.Class):
					examDateSheets = examDateSheets.OrderBy(sortDirection, eds => eds.Class).ToList();
					break;
				case nameof(ExamDateSheets.ExamDateSheet.RegisteredStudents):
					examDateSheets = examDateSheets.OrderBy(sortDirection, eds => eds.RegisteredStudents).ToList();
					break;
				case nameof(ExamDateSheets.ExamDateSheet.StudentsHavingClash):
					examDateSheets = examDateSheets.OrderBy(sortDirection, eds => eds.StudentsHavingClash).ToList();
					break;
				case nameof(ExamDateSheets.ExamDateSheet.DateSheet.ExamDateAndSession):
					examDateSheets = examDateSheets.OrderBy(sortDirection, eds => eds.ExDateSheet?.Date).ThenBy(eds => eds.ExDateSheet?.SessionName).ToList();
					break;
				case nameof(ExamDateSheets.ExamDateSheet.DateSheet.QuestionPaperReceivedDate):
					examDateSheets = examDateSheets.OrderBy(sortDirection, eds => eds.ExDateSheet?.QuestionPaperReceivedDate).ToList();
					break;
				case nameof(ExamDateSheets.ExamDateSheet.Rooms):
					examDateSheets = examDateSheets.OrderBy(sortDirection, eds => eds.Rooms).ToList();
					break;
				case nameof(ExamDateSheets.ExamDateSheet.Seats):
					examDateSheets = examDateSheets.OrderBy(sortDirection, eds => eds.Seats).ToList();
					break;
				case nameof(ExamDateSheets.ExamDateSheet.DateSheet.AnswerSheetsReceivedByExaminerDate):
					examDateSheets = examDateSheets.OrderBy(sortDirection, eds => eds.ExDateSheet?.AnswerSheetsReceivedByExaminerDate).ToList();
					break;
				case nameof(ExamDateSheets.ExamDateSheet.DateSheet.AnswerSheetsReceivedByExaminerCount):
					examDateSheets = examDateSheets.OrderBy(sortDirection, eds => eds.ExDateSheet?.AnswerSheetsReceivedByExaminerCount).ToList();
					break;
				case nameof(ExamDateSheets.ExamDateSheet.DateSheet.AnswerSheetsReceivedByExamCellDate):
					examDateSheets = examDateSheets.OrderBy(sortDirection, eds => eds.ExDateSheet?.AnswerSheetsReceivedByExamCellDate).ToList();
					break;
				case nameof(ExamDateSheets.ExamDateSheet.DateSheet.AnswerSheetsReceivedByExamCellCount):
					examDateSheets = examDateSheets.OrderBy(sortDirection, eds => eds.ExDateSheet?.AnswerSheetsReceivedByExamCellCount).ToList();
					break;
				default:
					throw new SortingNotImplementedException(sortExpression);
			}

			this.gvDateSheet.Columns[5].Visible = calculateClashes;
			this.gvDateSheet.DataBind(examDateSheets, sortExpression, sortDirection);
		}

		protected void lbtnViewOverallClashes_OnClick(object sender, EventArgs e)
		{
			this.DisplayClashes(null);
		}

		protected void ddlStudentClashes_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetClashes(0);
		}

		protected void gvClashes_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvClashes.PageSize = e.NewPageSize;
			this.GetClashes(0);
		}

		protected void gvClashes_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs1(this.ViewState);
			this.GetClashes(0);
		}

		protected void gvClashes_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetClashes(e.NewPageIndex);
		}

		private void DisplayClashes(int? offeredCourseID)
		{
			this.ViewState["OfferedCourseID"] = offeredCourseID;
			this.ddlStudentClashes.ClearSelection();
			this.GetClashes(0);
			this.modalClashes.Show();
		}

		private void GetClashes(int pageIndex)
		{
			var examConductID = this.ddlExamConductID.SelectedValue.ToNullableInt();
			if (examConductID == null)
				this.gvClashes.DataBindNull();
			else
			{
				var offeredCourseID = this.ViewState["OfferedCourseID"] as int?;
				var clashes = this.ddlStudentClashes.SelectedValue.ToInt();
				var studentClashes = ExamDateSheets.GetClashes(examConductID.Value, offeredCourseID, clashes, pageIndex, this.gvClashes.PageSize, this.ViewState.GetSortExpression1(), this.ViewState.GetSortDirection1(), this.StaffIdentity.LoginSessionGuid);
				this.gvClashes.DataBind(studentClashes.RegisteredCourses, pageIndex, studentClashes.VirtualItemCount, this.ViewState.GetSortExpression1(), this.ViewState.GetSortDirection1());
			}
		}

		protected void btnDeleteSeats_OnClick(object sender, EventArgs e)
		{
			var offeredCourseID = (int)this.ViewState[nameof(OfferedCours.OfferedCourseID)];
			var examConductID = this.ddlExamConductID.SelectedValue.ToInt();
			var status = ExecuteSeatingPlan.DeleteExamSeatsForOfferedCourseID(offeredCourseID, examConductID, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case ExecuteSeatingPlan.DeleteExamSeatsForOfferedCourseIDStatuses.NoRecordFound:
					this.Page.AddNoRecordFoundAlert();
					this.RefreshPage();
					break;
				case ExecuteSeatingPlan.DeleteExamSeatsForOfferedCourseIDStatuses.Success:
					this.Page.AddSuccessAlert("Seats have been deleted.");
					this.RefreshPage();
					break;
				default:
					throw new NotImplementedEnumException(status);
			}
		}
	}
}
