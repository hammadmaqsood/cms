﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Sys.Executive;
using System;

namespace Aspire.Web.Sys.Staff.Common
{
	public partial class StudentInfoUserControl : System.Web.UI.UserControl
	{
		private new AspireBasePage Page => base.Page as AspireBasePage;
		//public new StaffPage Page => (StaffPage)base.Page;
		private StaffPage PageStaff;
		private ExecutivePage PageExecutive;
		private int InstituteID;
		protected void Page_Load(object sender, EventArgs e)
		{
		}

		public BasicStudentInformation LoadBasicStudentInformation(string enrollment)
		{
			switch (this.Page.AspireIdentity.UserType)
			{
				case UserTypes.Executive:
					this.PageExecutive = this.Page as ExecutivePage;
					if (this.PageExecutive != null)
						this.InstituteID = this.PageExecutive.ExecutiveIdentity.InstituteID;
					break;
				case UserTypes.Staff:
					this.PageStaff = this.Page as StaffPage;
					if (this.PageStaff != null)
						this.InstituteID = this.PageStaff.StaffIdentity.InstituteID;
					break;
				case UserTypes.Student:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			var info = Aspire.BL.Common.Common.GetBasicStudentInformation(this.InstituteID, enrollment);
			if (info != null)
				this.SetStudentInformation(info.Enrollment, info.StudentID, info.RegistrationNo, info.Name, info.FatherName, info.ProgramAlias, info.SemesterID, info.StatusEnum, info.StatusDate, info.BlockedReason);
			else
				this.Reset();
			return info;
		}

		public void SetStudentInformation(string enrollment, int studentID, int? registrationNo, string name, string fatherName, string programAlias, short intakeSemesterID, Model.Entities.Student.Statuses? status, DateTime? statusDate, string blockedReason)
		{
			this.StudentID = studentID;
			this.hlEnrollment.Text = enrollment;
			this.hlEnrollment.NavigateUrl = Staff.Students.StudentProfileView.GetPageUrl(enrollment, true);
			this.lblRegistrationNo.Text = registrationNo.ToString().ToNAIfNullOrEmpty();
			this.lblName.Text = name;
			this.lblFatherName.Text = fatherName;
			this.lblProgramAlias.Text = programAlias;
			this.lblIntakeSemester.Text = intakeSemesterID.ToSemesterString();
			this.lblStatusDate.Text = (statusDate?.ToString("D")).ToNAIfNullOrEmpty();
			if (status == Model.Entities.Student.Statuses.Blocked && !string.IsNullOrWhiteSpace(blockedReason))
				this.lblStatus.Text = $"{status.Value.ToFullName()} - {blockedReason}";
			else
				this.lblStatus.Text = (status?.ToFullName()).ToNAIfNullOrEmpty();
		}

		public void Reset()
		{
			this.StudentID = null;
			this.hlEnrollment.Text = null;
			this.lblRegistrationNo.Text = null;
			this.lblName.Text = null;
			this.lblFatherName.Text = null;
			this.lblProgramAlias.Text = null;
			this.lblIntakeSemester.Text = null;
			this.lblStatus.Text = null;
			this.lblStatusDate.Text = null;
		}

		public int? StudentID
		{
			get => (int?)this.ViewState[nameof(this.StudentID)];
			private set => this.ViewState[nameof(this.StudentID)] = value;
		}

		public string Enrollment => this.hlEnrollment.Text;
		public string ProgramAlias => this.lblProgramAlias.Text;
	}
}