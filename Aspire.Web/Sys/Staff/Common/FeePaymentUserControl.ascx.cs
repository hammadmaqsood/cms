﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Sys.Staff.FeeManagement;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Staff.Common
{
	public partial class FeePaymentUserControl : System.Web.UI.UserControl
	{
		private new StaffPage Page => (StaffPage)base.Page;

		private void Redirect(decimal? fullChallanNo, bool search)
		{
			if (this.Page is FeePayment)
				FeePayment.Redirect(fullChallanNo, search);
			else
				Admissions.AdmissionProcessingFee.Redirect(fullChallanNo, search);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.panelChallan.Visible = false;
				this.panelPayment.Visible = false;
				string inputMask;
				if (this.Page is Admissions.AdmissionProcessingFee)
					inputMask = $"'mask': '{this.Page.StaffIdentity.InstituteCode}1999999999'";
				else if (this.Page is FeePayment)
					inputMask = $"'mask': '({this.Page.StaffIdentity.InstituteCode}2999999999)|({this.Page.StaffIdentity.InstituteCode}3999999999)'";
				else
					throw new InvalidOperationException();
				this.tbFullChallanNo.Attributes.Add("data-inputmask", inputMask);
				this.tbSwapFullChallanNo.Attributes.Add("data-inputmask", inputMask);

				this.tbFullChallanNo.SetTextIfNotPostback("FullChallanNo");
				if (this.Request.GetParameterValue<bool>("Search") == true)
					this.btnSearch_OnClick(null, null);
				else
					this.tbFullChallanNo.Focus();
			}
			if (this.Page is Admissions.AdmissionProcessingFee)
				this.hlCancel.NavigateUrl = typeof(Admissions.AdmissionProcessingFee).GetAspirePageAttribute().PageUrl;
			else if (this.Page is FeePayment)
				this.hlCancel.NavigateUrl = typeof(FeePayment).GetAspirePageAttribute().PageUrl;
			else
				throw new InvalidOperationException();
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			var challanNo = AspireFormats.ChallanNo.TryParse(this.tbFullChallanNo.Text);
			bool validChallanNo;
			switch (challanNo?.FeeTypeEnum)
			{
				case null:
					validChallanNo = false;
					break;
				case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
					validChallanNo = this.Page is Admissions.AdmissionProcessingFee;
					break;
				case InstituteBankAccount.FeeTypes.NewAdmissionFee:
				case InstituteBankAccount.FeeTypes.StudentFee:
					validChallanNo = this.Page is FeePayment;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			if (!validChallanNo)
			{
				this.Page.AddErrorAlert("Invalid Challan No.");
				this.Redirect(null, false);
				return;
			}

			if (this.panelChallan.Visible)
			{
				this.Redirect(challanNo.Value.FullChallanNo, true);
				return;
			}
			var rawFeeChallan = BL.Core.FeeManagement.Staff.FeePayment.GetRawFeeChallanDetails(challanNo.Value, out var accountTransaction, AspireIdentity.Current.LoginSessionGuid);
			if (rawFeeChallan == null)
			{
				this.Page.AddNoRecordFoundAlert();
				this.Redirect(challanNo.Value.FullChallanNo, false);
				return;
			}
			this.panelChallan.Visible = true;
			this.FullChallanNo = rawFeeChallan.FullChallanNo;
			this.lblFullChallanNo.Text = rawFeeChallan.FullChallanNo.ToString(AspireFormats.ChallanNo.FullChallanNoDecimalFormat);
			this.lblChallanType.Text = challanNo.Value.FeeTypeEnum.ToFullName();
			this.lblSemester.Text = rawFeeChallan.SemesterID.ToSemesterString();
			if (rawFeeChallan.Enrollment != null)
			{
				this.lbllblEnrollmentOrApplicationNo.Text = "Enrollment:";
				this.lblEnrollmentOrApplicationNo.Text = rawFeeChallan.Enrollment;
			}
			else
			{
				this.lbllblEnrollmentOrApplicationNo.Text = "Application No.:";
				this.lblEnrollmentOrApplicationNo.Text = (rawFeeChallan.ApplicationNo?.ToString()).ToNAIfNullOrEmpty();
			}
			this.lblName.Text = rawFeeChallan.Name.ToNAIfNullOrEmpty();
			this.lblAmount.Text = rawFeeChallan.Amount.ToString();
			this.lblDueDate.Text = rawFeeChallan.DueDate.ToShortDateString();
			this.lblPaid.Text = rawFeeChallan.Paid.ToYesNo();
			this.lblServiceFullName.Text = (rawFeeChallan.ServiceEnum?.ToFullName()).ToNAIfNullOrEmpty();
			this.lblDepositDate.Text = (rawFeeChallan.DepositDate?.ToString("d")).ToNAIfNullOrEmpty();
			this.lblTransactionReferenceID.Text = rawFeeChallan.TransactionReferenceID.ToNAIfNullOrEmpty();
			this.lblDepositedInAccountNo.Text = rawFeeChallan.DepositedInAccountNo.ToNAIfNullOrEmpty();
			this.lblDepositedInBankName.Text = rawFeeChallan.DepositedInBankName.ToNAIfNullOrEmpty();
			if (rawFeeChallan.SystemDate != null)
				this.lblLastUpdatedOn.Text = $"{rawFeeChallan.SystemDate:F} by " + (string.IsNullOrEmpty(rawFeeChallan.LastUpdatedByUserName)
								? rawFeeChallan.LastUpdatedByUserType.ToString()
								: $"{rawFeeChallan.LastUpdatedByUserName} ({rawFeeChallan.LastUpdatedByUserType})");
			else
				this.lblLastUpdatedOn.Text = string.Empty.ToNAIfNullOrEmpty();
			this.panelSwap.Visible = challanNo.Value.FeeTypeEnum == InstituteBankAccount.FeeTypes.AdmissionProcessingFee;
			if (accountTransaction == null)
				this.panelPayment.Visible = true;
			else
			{
				if (accountTransaction.Verified == false)
					this.panelPayment.Visible = true;
				else
				{
					switch (challanNo.Value.FeeTypeEnum)
					{
						case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
							this.panelPayment.Visible = StaffIdentity.Current.TryDemand(StaffPermissions.Admissions.ManageAdmissionProcessingFeeForLockedRecords, UserGroupPermission.PermissionValues.Allowed) != null;
							break;
						case InstituteBankAccount.FeeTypes.NewAdmissionFee:
						case InstituteBankAccount.FeeTypes.StudentFee:
							this.panelPayment.Visible = StaffIdentity.Current.TryDemand(StaffPermissions.FeeManagement.ManageFeePaymentOfLockedRecords, UserGroupPermission.PermissionValues.Allowed) != null;
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
			if (this.panelPayment.Visible)
			{
				this.ddlService.FillAccountTransactionServices(CommonListItems.Select);
				if (accountTransaction == null)
				{
					this.btnDelete.Visible = false;
					this.ddlService_OnSelectedIndexChanged(null, null);
				}
				else
				{
					this.ddlService.SetEnumValue(accountTransaction.ServiceEnum);
					this.ddlService_OnSelectedIndexChanged(null, null);
					this.ddlTransactionChannel.SetEnumValue(accountTransaction.TransactionChannelEnum);
					this.ddlTransactionChannel_OnSelectedIndexChanged(null, null);
					this.ddlPaymentType.SetEnumValue(accountTransaction.PaymentTypeEnum);
					this.ddlInstituteBankAccountID.SelectedValue = accountTransaction.InstituteBankAccountID.ToString();
					this.tbTransactionReferenceID.Text = accountTransaction.TransactionReferenceID;
					this.dtpTransactionDate.SelectedDate = accountTransaction.TransactionDate;
					this.tbAccountNo.Text = accountTransaction.AccountNo;
					this.tbAccountTitle.Text = accountTransaction.AccountTitle;
					this.tbBranchCode.Text = accountTransaction.BranchCode;
					this.tbBranchName.Text = accountTransaction.BranchName;
					this.tbRemarks.Text = accountTransaction.Remarks;
					this.AccountTransactionID = accountTransaction.AccountTransactionID;
					this.btnDelete.Visible = true;
				}
			}
		}

		private decimal? FullChallanNo
		{
			get => (decimal?)this.ViewState[nameof(this.FullChallanNo)];
			set => this.ViewState[nameof(this.FullChallanNo)] = value;
		}

		private int? AccountTransactionID
		{
			get => (int?)this.ViewState[nameof(this.AccountTransactionID)];
			set => this.ViewState[nameof(this.AccountTransactionID)] = value;
		}

		protected void ddlService_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.FullChallanNo == null)
				throw new InvalidOperationException();
			var serviceEnum = this.ddlService.GetSelectedEnumValue<AccountTransaction.Services>(null);
			if (serviceEnum == null)
			{
				this.ddlInstituteBankAccountID.DataBind(CommonListItems.Select);
				this.ddlInstituteBankAccountID.Enabled = false;
				this.ddlTransactionChannel.DataBind(CommonListItems.Select);
				this.ddlTransactionChannel.Enabled = false;
				this.ddlPaymentType.DataBind(CommonListItems.Select);
				this.ddlPaymentType.Enabled = false;
			}
			else
			{
				var feeTypeEnum = AspireFormats.ChallanNo.Parse(this.FullChallanNo.Value).FeeTypeEnum;
				var instituteBankAccounts = BL.Core.FeeManagement.Staff.FeePayment.GetInstituteBankAccounts(serviceEnum.Value, feeTypeEnum, this.Page.StaffIdentity.LoginSessionGuid);
				this.ddlInstituteBankAccountID.DataBind(instituteBankAccounts, CommonListItems.Select);
				switch (serviceEnum.Value)
				{
					case AccountTransaction.Services.Manual:
						this.ddlInstituteBankAccountID.Enabled = true;
						break;
					case AccountTransaction.Services.BankAlfalah:
						this.ddlInstituteBankAccountID.Enabled = false;
						this.ddlInstituteBankAccountID.SelectedValue = instituteBankAccounts.SingleOrDefault()?.ID.ToString();
						break;
					case AccountTransaction.Services.AlliedBank:
						this.ddlInstituteBankAccountID.Enabled = false;
						this.ddlInstituteBankAccountID.SelectedValue = instituteBankAccounts.SingleOrDefault()?.ID.ToString();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				var transactionChannels = AccountTransaction.ValidValues.Where(vv => vv.Service == serviceEnum.Value).Select(vv => vv.TransactionChannel).Distinct().ToList();
				this.ddlTransactionChannel.FillTransactionChannels(transactionChannels, CommonListItems.Select);
				this.ddlTransactionChannel.Enabled = true;
			}
			this.ddlTransactionChannel_OnSelectedIndexChanged(null, null);
		}

		protected void ddlTransactionChannel_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var serviceEnum = this.ddlService.GetSelectedEnumValue<AccountTransaction.Services>(null);
			var transactionChannelEnum = this.ddlTransactionChannel.GetSelectedEnumValue<AccountTransaction.TransactionChannels>(null);
			if (serviceEnum == null || transactionChannelEnum == null)
			{
				this.ddlPaymentType.DataBind(CommonListItems.Select);
				this.ddlPaymentType.Enabled = false;
			}
			else
			{
				var paymentTypes = AccountTransaction.ValidValues.Where(vv => vv.Service == serviceEnum.Value && vv.TransactionChannel == transactionChannelEnum.Value).Select(vv => vv.PaymentType).Distinct().ToList();
				this.ddlPaymentType.FillPaymentTypes(paymentTypes, CommonListItems.Select);
				this.ddlPaymentType.Enabled = true;
			}
		}

		protected void btnDelete_OnClick(object sender, EventArgs e)
		{
			if (this.FullChallanNo == null)
				return;
			var challanNo = AspireFormats.ChallanNo.Parse(this.FullChallanNo.Value);
			var status = BL.Core.FeeManagement.Staff.FeePayment.DeleteAccountTransaction(challanNo, this.Page.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.FeeManagement.Staff.FeePayment.DeleteAccountTransactionStatuses.NoRecordFound:
					this.Redirect(this.FullChallanNo, true);
					return;
				case BL.Core.FeeManagement.Staff.FeePayment.DeleteAccountTransactionStatuses.ChallanIsNotPaid:
					this.Page.AddErrorAlert("Challan is not paid.");
					this.Redirect(this.FullChallanNo, true);
					return;
				case BL.Core.FeeManagement.Staff.FeePayment.DeleteAccountTransactionStatuses.CannotDeleteVerifiedAccountTransaction:
					this.Page.AddSuccessAlert("Verified Payment Transaction cannot be deleted.");
					break;
				case BL.Core.FeeManagement.Staff.FeePayment.DeleteAccountTransactionStatuses.Success:
					this.Page.AddSuccessMessageHasBeenDeleted("Payment");
					this.Redirect(this.FullChallanNo, true);
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.Page.IsValid || this.FullChallanNo == null)
				return;
			var serviceEnum = this.ddlService.GetSelectedEnumValue<AccountTransaction.Services>();
			var transactionChannelEnum = this.ddlTransactionChannel.GetSelectedEnumValue<AccountTransaction.TransactionChannels>();
			var paymentTypeEnum = this.ddlPaymentType.GetSelectedEnumValue<AccountTransaction.PaymentTypes>();
			var instituteBankAccountID = this.ddlInstituteBankAccountID.SelectedValue.ToInt();
			var transactionReferenceID = this.tbTransactionReferenceID.Text;
			var transactionDate = this.dtpTransactionDate.SelectedDate ?? throw new InvalidOperationException();
			var accountNo = this.tbAccountNo.Text;
			var accountTitle = this.tbAccountTitle.Text;
			var branchCode = this.tbBranchCode.Text;
			var branchName = this.tbBranchName.Text;
			var remarks = this.tbRemarks.Text;
			var challanNo = AspireFormats.ChallanNo.Parse(this.FullChallanNo.Value);
			var status = BL.Core.FeeManagement.Staff.FeePayment.PayFee(challanNo, this.AccountTransactionID, this.lblAmount.Text.ToInt(), serviceEnum, transactionChannelEnum, paymentTypeEnum, instituteBankAccountID, transactionReferenceID, transactionDate, accountNo, accountTitle, branchCode, branchName, remarks, this.Page.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.FeeManagement.Staff.FeePayment.PayFeeStatuses.NoRecordFound:
					this.Redirect(this.FullChallanNo.Value, true);
					return;
				case BL.Core.FeeManagement.Staff.FeePayment.PayFeeStatuses.AlreadyPaid:
					this.Page.AddErrorAlert("Challan is already paid.");
					break;
				case BL.Core.FeeManagement.Staff.FeePayment.PayFeeStatuses.Added:
					this.Page.AddSuccessAlert("Payment Transaction has been added.");
					this.Redirect(this.FullChallanNo.Value, true);
					return;
				case BL.Core.FeeManagement.Staff.FeePayment.PayFeeStatuses.Updated:
					this.Page.AddSuccessAlert("Payment Transaction has been updated.");
					this.Redirect(this.FullChallanNo.Value, true);
					return;
				case BL.Core.FeeManagement.Staff.FeePayment.PayFeeStatuses.CannotUpdateVerifiedAccountTransaction:
					this.Page.AddSuccessAlert("Verified Payment Transaction cannot be updated.");
					this.Redirect(this.FullChallanNo.Value, true);
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		protected void btnSwapChallanNos_OnClick(object sender, EventArgs e)
		{
			if (!this.Page.IsValid || this.FullChallanNo == null)
				return;
			var challanNo1 = AspireFormats.ChallanNo.Parse(this.FullChallanNo.Value);
			var challanNo2 = AspireFormats.ChallanNo.Parse(this.tbSwapFullChallanNo.Text);
			var status = BL.Core.FeeManagement.Staff.FeePayment.SwapChallanNos(challanNo1, challanNo2, this.Page.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.FeeManagement.Staff.FeePayment.SwapChallanNosStatuses.NoRecordFound:
					this.Page.AddNoRecordFoundAlert();
					break;
				case BL.Core.FeeManagement.Staff.FeePayment.SwapChallanNosStatuses.NotImplementedFeature:
					this.Page.AddErrorAlert("These types of challans cannot be swapped.");
					break;
				case BL.Core.FeeManagement.Staff.FeePayment.SwapChallanNosStatuses.CandidateIDNotSame:
					this.Page.AddErrorAlert("Both challan numbers must belong to same candidate and semester.");
					break;
				case BL.Core.FeeManagement.Staff.FeePayment.SwapChallanNosStatuses.BothChallansAreSame:
					this.Page.AddErrorAlert("Both challan numbers are same.");
					break;
				case BL.Core.FeeManagement.Staff.FeePayment.SwapChallanNosStatuses.BothChallansArePaid:
					this.Page.AddErrorAlert("Both challan are paid. Why you are swapping it?");
					break;
				case BL.Core.FeeManagement.Staff.FeePayment.SwapChallanNosStatuses.BothChallansAreNotPaid:
					this.Page.AddErrorAlert("Both challan are not paid. Why you are swapping it?");
					break;
				case BL.Core.FeeManagement.Staff.FeePayment.SwapChallanNosStatuses.AmountsAreNotSame:
					this.Page.AddErrorAlert("Challans cannot be swapped because Amount of both challan is not same.");
					break;
				case BL.Core.FeeManagement.Staff.FeePayment.SwapChallanNosStatuses.InstitutesMustBeSameDueToBankAccounts:
					this.Page.AddErrorAlert("Institute of both Challans must be same. Otherwise bank scroll will not be consistent.");
					break;
				case BL.Core.FeeManagement.Staff.FeePayment.SwapChallanNosStatuses.Success:
					this.Page.AddSuccessAlert("Both Challan numbers are swapped.");
					break;
				default:
					throw new NotImplementedEnumException(status);
			}
		}
	}
}