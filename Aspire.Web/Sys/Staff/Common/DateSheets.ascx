﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateSheets.ascx.cs" Inherits="Aspire.Web.Sys.Staff.Common.DateSheets" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<div class="row">
	<div class="col-md-2">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="ExamType:" AssociatedControlID="ddlExamType" />
			<aspire:AspireDropDownList runat="server" ID="ddlExamType" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlExamType_OnSelectedIndexChanged" />
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Exam Conduct:" AssociatedControlID="ddlExamConductID" />
			<aspire:AspireDropDownList runat="server" ID="ddlExamConductID" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlExamConductID_OnSelectedIndexChanged" />
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentID_OnSelectedIndexChanged" />
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramID" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlProgramID_OnSelectedIndexChanged" />
		</div>
	</div>
	<div class="col-md-1">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Sem#:" AssociatedControlID="ddlSemesterNo" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterNo_OnSelectedIndexChanged" />
		</div>
	</div>
	<div class="col-md-1">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSection" />
			<aspire:AspireDropDownList runat="server" ID="ddlSection" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlSection_OnSelectedIndexChanged" />
		</div>
	</div>
	<div class="col-md-1">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShift" />
			<aspire:AspireDropDownList runat="server" ID="ddlShift" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlShift_OnSelectedIndexChanged" />
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Teacher:" AssociatedControlID="ddlFacultyMemberID" />
			<aspire:AspireDropDownList runat="server" ID="ddlFacultyMemberID" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlFacultyMemberID_SelectedIndexChanged" />
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Display Clashes Count:" AssociatedControlID="ddlDisplayClashesCount" />
			<aspire:AspireDropDownList runat="server" ID="ddlDisplayClashesCount" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlDisplayClashesCount_SelectedIndexChanged" />
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group text-right">
			<aspire:AspireLinkButton runat="server" ID="lbtnViewOverallClashes" Text="View Overall Clashes" CausesValidation="False" OnClick="lbtnViewOverallClashes_OnClick" />
		</div>
	</div>
</div>
<aspire:AspireGridView runat="server" ItemType="Aspire.BL.Core.ExamSeatingPlan.Staff.ExamDateSheets.ExamDateSheet" ID="gvDateSheet" AllowSorting="True" AutoGenerateColumns="False" OnRowCommand="gvDateSheet_OnRowCommand" OnSorting="gvDateSheet_OnSorting">
	<Columns>
		<asp:TemplateField HeaderText="#">
			<ItemTemplate><%#: Container.DataItemIndex + 1 %></ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="Offered Course Title" SortExpression="Title">
			<ItemTemplate>
				<%#: Item.Title %>
				<span class="label label-primary"><%#: Item.CourseCategoryEnum.ToFullName() %></span>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="Teacher" SortExpression="Teacher">
			<ItemTemplate><%#: Item.Teacher.ToNAIfNullOrEmpty() %></ItemTemplate>
		</asp:TemplateField>
		<asp:BoundField HeaderText="Class" DataField="Class" SortExpression="Class" />
		<asp:BoundField HeaderText="Students" DataField="RegisteredStudents" SortExpression="RegisteredStudents" />
		<asp:TemplateField HeaderText="Clashes" SortExpression="StudentsHavingClash">
			<ItemTemplate>
				<%#: Item.StudentsHavingClash %>
				<%#: Item.StudentsHavingClash > 0 ? "(":"" %>
				<aspire:AspireLinkButton EncryptedCommandArgument="<%# Item.OfferedCourseID %>" Visible="<%# Item.StudentsHavingClash > 0 %>" runat="server" CausesValidation="False" CommandName="ViewClashes" Text="View" />
				<%#: Item.StudentsHavingClash > 0 ? ")":"" %>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="Exam Date & Session" SortExpression="ExamDateAndSession">
			<ItemTemplate>
				<%#: (Item.ExDateSheet?.ExamDateAndSession).ToNAIfNullOrEmpty() %>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="Paper Received" SortExpression="QuestionPaperReceivedDate">
			<ItemTemplate>
				<%#: $"{Item.ExDateSheet?.QuestionPaperReceivedDate:d}".ToNAIfNullOrEmpty() %>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="Rooms" SortExpression="Rooms">
			<ItemTemplate>
				<%#: $"{Item.Rooms}".ToNAIfNullOrEmpty() %>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="Seats" SortExpression="Seats">
			<ItemTemplate>
				<%#: $"{Item.Seats}".ToNAIfNullOrEmpty() %>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="Answer Sheets Received By Examiner" SortExpression="AnswerSheetsReceivedByExaminerDate">
			<ItemTemplate><%#: Item.ExDateSheet?.AnswerSheetsReceivedByExaminerDate != null && Item.ExDateSheet?.AnswerSheetsReceivedByExaminerCount != null ? $"{Item.ExDateSheet.AnswerSheetsReceivedByExaminerDate.Value:d} : {Item.ExDateSheet.AnswerSheetsReceivedByExaminerCount}" : "".ToNAIfNullOrEmpty() %></ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="Answer Sheets Received By Exam Cell" SortExpression="AnswerSheetsReceivedByExamCellDate">
			<ItemTemplate><%#: Item.ExDateSheet?.AnswerSheetsReceivedByExamCellDate != null && Item.ExDateSheet?.AnswerSheetsReceivedByExamCellCount != null ? $"{Item.ExDateSheet.AnswerSheetsReceivedByExamCellDate.Value:d} : {Item.ExDateSheet.AnswerSheetsReceivedByExamCellCount}" : "".ToNAIfNullOrEmpty() %></ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="Action">
			<ItemTemplate>
				<aspire:AspireLinkButton ToolTip="Edit" runat="server" CausesValidation="False" CommandName="Edit" Glyphicon="edit" EncryptedCommandArgument="<%# Item.OfferedCourseID %>" />
			</ItemTemplate>
		</asp:TemplateField>
	</Columns>
</aspire:AspireGridView>

<aspire:AspireModal runat="server" ID="modalDateSheet" ModalSize="Large">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updateModalDateSheet" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alertModalDateSheet" />
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="lblSemester" />
							<div>
								<aspire:Label runat="server" ID="lblSemester" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Exam Type:" AssociatedControlID="lblExamType" />
							<div>
								<aspire:Label runat="server" ID="lblExamType" />
							</div>
						</div>

					</div>
					<div class="col-md-4">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Exam Conduct:" AssociatedControlID="lblExamConductName" />
							<div>
								<aspire:Label runat="server" ID="lblExamConductName" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Course Title:" AssociatedControlID="lblCourseTitle" />
							<div>
								<aspire:Label runat="server" ID="lblCourseTitle" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Class:" AssociatedControlID="lblClass" />
							<div>
								<aspire:Label runat="server" ID="lblClass" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Teacher:" AssociatedControlID="lblTeacher" />
							<div>
								<aspire:Label runat="server" ID="lblTeacher" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Exam Date and Session:" AssociatedControlID="ddlExamDateAndSession" />
							<aspire:AspireDropDownList runat="server" ID="ddlExamDateAndSession" ValidationGroup="Req" />
							<aspire:AspireRequiredFieldValidator runat="server" ID="rfvddlExamDateAndSession" ControlToValidate="ddlExamDateAndSession" ErrorMessage="This field is required." ValidationGroup="Req" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group" id="divQuestionPaperReceived" runat="server">
							<aspire:AspireLabel runat="server" Text="Question Paper Received By Exam Cell Date:" AssociatedControlID="dtpQuestionPaperReceivedDate" />
							<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpQuestionPaperReceivedDate" ValidationGroup="Req" />
							<aspire:AspireDateTimeValidator runat="server" AllowNull="True" ControlToValidate="dtpQuestionPaperReceivedDate" ValidationGroup="Req" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group" id="divAnswerSheetsReceivedByExaminerDate" runat="server">
							<aspire:AspireLabel runat="server" Text="Answer Sheet Received By Examiner Date:" AssociatedControlID="dtpAnswerSheetsReceivedByExaminerDate" />
							<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpAnswerSheetsReceivedByExaminerDate" ValidationGroup="Req" />
							<aspire:AspireDateTimeValidator ID="dtvdtpAnswerSheetsReceivedByExaminerDate" runat="server" AllowNull="True" ControlToValidate="dtpAnswerSheetsReceivedByExaminerDate" ValidationGroup="Req" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group" id="divAnswerSheetsReceivedByExaminerCount" runat="server">
							<aspire:AspireLabel runat="server" Text="Answer Sheets Received By Examiner Count:" AssociatedControlID="dtpAnswerSheetsReceivedByExaminerDate" />
							<aspire:AspireTextBox runat="server" ID="tbAnswerSheetsReceivedByExaminerCount" MaxLength="8" ValidationGroup="Req" />
							<aspire:AspireUInt16Validator ID="ivtbAnswerSheetsReceivedByExaminerCount" runat="server" AllowNull="True" ControlToValidate="tbAnswerSheetsReceivedByExaminerCount" ValidationGroup="Req" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Number." />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group" id="divAnswerSheetsReceivedByExamCellDate" runat="server">
							<aspire:AspireLabel runat="server" Text="Answer Sheet Received By Exam Cell Date:" AssociatedControlID="dtpAnswerSheetsReceivedByExamCellDate" />
							<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpAnswerSheetsReceivedByExamCellDate" ValidationGroup="Req" />
							<aspire:AspireDateTimeValidator ID="dtvdtpAnswerSheetsReceivedByExamCellDate" runat="server" AllowNull="True" ControlToValidate="dtpAnswerSheetsReceivedByExamCellDate" ValidationGroup="Req" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group" id="divAnswerSheetsReceivedByExamCellCount" runat="server">
							<aspire:AspireLabel runat="server" Text="Answer Sheets Received By Exam Cell Count:" AssociatedControlID="dtpAnswerSheetsReceivedByExamCellDate" />
							<aspire:AspireTextBox runat="server" ID="tbAnswerSheetsReceivedByExamCellCount" MaxLength="8" ValidationGroup="Req" />
							<aspire:AspireUInt16Validator ID="ivtbAnswerSheetsReceivedByExamCellCount" runat="server" AllowNull="True" ControlToValidate="tbAnswerSheetsReceivedByExamCellCount" ValidationGroup="Req" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Number." />
						</div>
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireButton runat="server" CssClass="pull-left" ID="btnDeleteSeats" Text="Delete Seats" ButtonType="Danger" ConfirmMessage="Are you sure you want to delete allocated seats for selected course?" OnClick="btnDeleteSeats_OnClick" CausesValidation="False" />
				<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" OnClick="btnSave_OnClick" ValidationGroup="Req" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>

<aspire:AspireModal runat="server" ID="modalClashes" HeaderText="Clashes" ModalSize="Large">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Student Having Papers:" AssociatedControlID="ddlStudentClashes" />
					<aspire:AspireDropDownList runat="server" ID="ddlStudentClashes" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="ddlStudentClashes_OnSelectedIndexChanged">
						<Items>
							<asp:ListItem Text=">=2" Value="2" />
							<asp:ListItem Text=">=3" Value="3" />
							<asp:ListItem Text=">=4" Value="4" />
						</Items>
					</aspire:AspireDropDownList>
				</div>
				<aspire:AspireGridView AllowSorting="true" runat="server" ID="gvClashes" AutoGenerateColumns="False" OnPageSizeChanging="gvClashes_OnPageSizeChanging" OnSorting="gvClashes_OnSorting" OnPageIndexChanging="gvClashes_OnPageIndexChanging">
					<Columns>
						<asp:BoundField HeaderText="Enrollment" DataField="Enrollment" SortExpression="Enrollment" />
						<asp:BoundField HeaderText="Name" DataField="Name" SortExpression="Name" />
						<asp:BoundField HeaderText="Class" DataField="Class" SortExpression="Class" />
						<asp:BoundField HeaderText="Title" DataField="Title" SortExpression="Title" />
						<asp:BoundField HeaderText="Teacher" DataField="Teacher" SortExpression="Teacher" />
						<asp:BoundField HeaderText="Date" DataField="Date" SortExpression="Date" DataFormatString="{0:d}" />
						<asp:BoundField HeaderText="Session" DataField="SessionName" SortExpression="SessionName" />
					</Columns>
				</aspire:AspireGridView>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
	</FooterTemplate>
</aspire:AspireModal>

<script type="text/javascript">
	$(function () {
		$("#<%=this.ddlFacultyMemberID.ClientID%>").applySelect2();
		$("#<%=this.ddlExamDateAndSession.ClientID%>").applySelect2();
	});
</script>
