﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeePaymentUserControl.ascx.cs" Inherits="Aspire.Web.Sys.Staff.Common.FeePaymentUserControl" %>
<div class="row">
	<div class="col-md-12">
		<div class="form-inline">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Full Challan No.:" AssociatedControlID="tbFullChallanNo" />
				<asp:Panel runat="server" DefaultButton="btnSearch" CssClass="input-group">
					<aspire:AspireTextBox runat="server" data-inputmask="'mask': '999999999999'" ID="tbFullChallanNo" ValidationGroup="Search" />
					<div class="input-group-btn">
						<aspire:AspireButton runat="server" ID="btnSearch" FontAwesomeIcon="solid_arrow_right" ButtonType="Primary" ValidationGroup="Search" OnClick="btnSearch_OnClick" />
					</div>
				</asp:Panel>
				<aspire:AspireStringValidator ValidationExpression="Custom" CustomValidationExpression="^\d{11,12}$" runat="server" ControlToValidate="tbFullChallanNo" RequiredErrorMessage="This field is required." AllowNull="False" ValidationGroup="Search" InvalidDataErrorMessage="Invalid Challan No." />
			</div>
		</div>
	</div>
</div>
<p></p>
<asp:Panel runat="server" CssClass="row" ID="panelChallan">
	<div class="form-group col-md-6">
		<aspire:AspireLabel runat="server" Text="Full Challan No.:" AssociatedControlID="lblFullChallanNo" />
		<div>
			<aspire:Label runat="server" ID="lblFullChallanNo" />
		</div>
	</div>
	<div class="form-group col-md-6">
		<aspire:AspireLabel runat="server" Text="Challan Type:" AssociatedControlID="lblChallanType" />
		<div>
			<aspire:Label runat="server" ID="lblChallanType" />
		</div>
	</div>
	<div class="form-group col-md-6">
		<aspire:AspireLabel runat="server" ID="lbllblEnrollmentOrApplicationNo" Text="Enrollment/Application No.:" AssociatedControlID="lblEnrollmentOrApplicationNo" />
		<div>
			<aspire:Label runat="server" ID="lblEnrollmentOrApplicationNo" />
		</div>
	</div>
	<div class="form-group col-md-6">
		<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="lblSemester" />
		<div>
			<aspire:Label runat="server" ID="lblSemester" />
		</div>
	</div>
	<div class="form-group col-md-6">
		<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="lblName" />
		<div>
			<aspire:Label runat="server" ID="lblName" />
		</div>
	</div>
	<div class="form-group col-md-6">
		<aspire:AspireLabel runat="server" Text="Amount:" AssociatedControlID="lblAmount" />
		<div>
			Rs.
			<aspire:Label runat="server" ID="lblAmount" />
		</div>
	</div>
	<div class="form-group col-md-6">
		<aspire:AspireLabel runat="server" Text="Due Date:" AssociatedControlID="lblDueDate" />
		<div>
			<aspire:Label runat="server" ID="lblDueDate" />
		</div>
	</div>
	<div class="form-group col-md-6">
		<aspire:AspireLabel runat="server" Text="Paid:" AssociatedControlID="lblPaid" />
		<div>
			<aspire:Label runat="server" ID="lblPaid" />
		</div>
	</div>
	<div class="form-group col-md-6">
		<aspire:AspireLabel runat="server" Text="Deposit Date:" AssociatedControlID="lblDepositDate" />
		<div>
			<aspire:Label runat="server" ID="lblDepositDate" />
		</div>
	</div>
	<div class="form-group col-md-6">
		<aspire:AspireLabel runat="server" Text="Transaction Reference:" AssociatedControlID="lblTransactionReferenceID" />
		<div>
			<aspire:Label runat="server" ID="lblTransactionReferenceID" />
		</div>
	</div>
	<div class="form-group col-md-6">
		<aspire:AspireLabel runat="server" Text="Service:" AssociatedControlID="lblServiceFullName" />
		<div>
			<aspire:Label runat="server" ID="lblServiceFullName" />
		</div>
	</div>
	<div class="form-group col-md-6">
		<aspire:AspireLabel runat="server" Text="Deposited In Bank:" AssociatedControlID="lblDepositedInBankName" />
		<div>
			<aspire:Label runat="server" ID="lblDepositedInBankName" />
		</div>
	</div>
	<div class="form-group col-md-6">
		<aspire:AspireLabel runat="server" Text="Deposited In Account No.:" AssociatedControlID="lblDepositedInAccountNo" />
		<div>
			<aspire:Label runat="server" ID="lblDepositedInAccountNo" />
		</div>
	</div>
	<div class="form-group col-md-6">
		<aspire:AspireLabel runat="server" Text="Last Updated On:" AssociatedControlID="lblLastUpdatedOn" />
		<div>
			<aspire:Label runat="server" ID="lblLastUpdatedOn" />
		</div>
	</div>
	<asp:Panel runat="server" class="form-group col-md-6" ID="panelSwap" DefaultButton="btnSwapChallanNos">
		<aspire:AspireLabel runat="server" Text="Swap With Full Challan No.:" AssociatedControlID="tbSwapFullChallanNo" />
		<div class="input-group">
			<aspire:AspireTextBox runat="server" data-inputmask="'mask': '999999999999'" ID="tbSwapFullChallanNo" ValidationGroup="Swap" />
			<div class="input-group-btn">
				<aspire:AspireButton FontAwesomeIcon="solid_exchange_alt" runat="server" ID="btnSwapChallanNos" ButtonType="Primary" ValidationGroup="Swap" Text="Swap" OnClick="btnSwapChallanNos_OnClick" />
			</div>
		</div>
		<aspire:AspireStringValidator ValidationExpression="Custom" CustomValidationExpression="^\d{11,12}$" runat="server" ControlToValidate="tbSwapFullChallanNo" RequiredErrorMessage="This field is required." AllowNull="False" ValidationGroup="Swap" InvalidDataErrorMessage="Invalid Challan No." />
	</asp:Panel>
</asp:Panel>
<asp:Panel runat="server" ID="panelPayment">
	<div class="row">
		<div class="form-group col-md-6">
			<aspire:AspireLabel runat="server" Text="Service:" AssociatedControlID="ddlService" />
			<aspire:AspireDropDownList runat="server" ID="ddlService" ValidationGroup="Payment" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="ddlService_OnSelectedIndexChanged" />
			<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Payment" ControlToValidate="ddlService" ErrorMessage="This field is required." />
		</div>
		<div class="form-group col-md-6">
			<aspire:AspireLabel runat="server" Text="Transaction Channel:" AssociatedControlID="ddlTransactionChannel" />
			<aspire:AspireDropDownList runat="server" ID="ddlTransactionChannel" ValidationGroup="Payment" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="ddlTransactionChannel_OnSelectedIndexChanged" />
			<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Payment" ControlToValidate="ddlTransactionChannel" ErrorMessage="This field is required." />
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-6">
			<aspire:AspireLabel runat="server" Text="Payment Type:" AssociatedControlID="ddlPaymentType" />
			<aspire:AspireDropDownList runat="server" ID="ddlPaymentType" ValidationGroup="Payment" />
			<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Payment" ControlToValidate="ddlPaymentType" ErrorMessage="This field is required." />
		</div>
		<div class="form-group col-md-6">
			<aspire:AspireLabel runat="server" Text="Bank Account:" AssociatedControlID="ddlInstituteBankAccountID" />
			<aspire:AspireDropDownList runat="server" ID="ddlInstituteBankAccountID" ValidationGroup="Payment" />
			<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Payment" ControlToValidate="ddlInstituteBankAccountID" ErrorMessage="This field is required." />
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-6">
			<aspire:AspireLabel runat="server" Text="Transaction Reference:" AssociatedControlID="tbTransactionReferenceID" />
			<aspire:AspireTextBox runat="server" ID="tbTransactionReferenceID" MaxLength="100" ValidationGroup="Payment" />
			<aspire:AspireStringValidator runat="server" AllowNull="True" ValidationGroup="Payment" ControlToValidate="tbTransactionReferenceID" InvalidDataErrorMessage="Invalid Transaction Reference." RequiredErrorMessage="This field is required." />
		</div>
		<div class="form-group col-md-6">
			<aspire:AspireLabel runat="server" Text="Transaction Date:" AssociatedControlID="dtpTransactionDate" />
			<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpTransactionDate" ValidationGroup="Payment" DisplayMode="ShortDateTime" />
			<aspire:AspireDateTimeValidator runat="server" Format="ShortDateTime" ValidationGroup="Payment" ControlToValidate="dtpTransactionDate" AllowNull="False" InvalidDataErrorMessage="Invalid Date." RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Invalid Date range." />
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-6">
			<aspire:AspireLabel runat="server" Text="Account No.:" AssociatedControlID="tbAccountNo" />
			<aspire:AspireTextBox runat="server" ID="tbAccountNo" MaxLength="500" ValidationGroup="Payment" />
			<aspire:AspireStringValidator runat="server" AllowNull="True" ValidationGroup="Payment" ControlToValidate="tbAccountNo" InvalidDataErrorMessage="Invalid Account No." RequiredErrorMessage="This field is required." />
		</div>
		<div class="form-group col-md-6">
			<aspire:AspireLabel runat="server" Text="Account Title:" AssociatedControlID="tbAccountTitle" />
			<aspire:AspireTextBox runat="server" ID="tbAccountTitle" MaxLength="500" ValidationGroup="Payment" />
			<aspire:AspireStringValidator runat="server" AllowNull="True" ValidationGroup="Payment" ControlToValidate="tbAccountTitle" InvalidDataErrorMessage="Invalid Account Title." RequiredErrorMessage="This field is required." />
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-6">
			<aspire:AspireLabel runat="server" Text="Branch Code:" AssociatedControlID="tbBranchCode" />
			<aspire:AspireTextBox runat="server" ID="tbBranchCode" MaxLength="50" ValidationGroup="Payment" />
			<aspire:AspireStringValidator runat="server" ValidationExpression="Custom" CustomValidationExpression="^\d+$" AllowNull="True" ValidationGroup="Payment" ControlToValidate="tbBranchCode" InvalidDataErrorMessage="Invalid Branch Code." RequiredErrorMessage="This field is required." />
		</div>
		<div class="form-group col-md-6">
			<aspire:AspireLabel runat="server" Text="Branch Name:" AssociatedControlID="tbBranchName" />
			<aspire:AspireTextBox runat="server" ID="tbBranchName" MaxLength="500" ValidationGroup="Payment" />
			<aspire:AspireStringValidator runat="server" AllowNull="True" ValidationGroup="Payment" ControlToValidate="tbBranchName" InvalidDataErrorMessage="Invalid Branch Name." RequiredErrorMessage="This field is required." />
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-12">
			<aspire:AspireLabel runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" />
			<aspire:AspireTextBox Rows="3" TextMode="MultiLine" runat="server" ID="tbRemarks" MaxLength="1000" ValidationGroup="Payment" />
			<aspire:AspireStringValidator runat="server" AllowNull="True" ValidationGroup="Payment" ControlToValidate="tbRemarks" InvalidDataErrorMessage="Invalid Remarks." RequiredErrorMessage="This field is required." />
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-4">
			<aspire:AspireButton runat="server" ID="btnDelete" Text="Delete" ButtonType="Danger" CausesValidation="False" ConfirmMessage="Are you sure you want to delete this transaction?" OnClick="btnDelete_OnClick" />
		</div>
		<div class="form-group col-md-4 text-center">
			<aspire:AspireButton runat="server" Text="Save" ValidationGroup="Payment" ID="btnSave" OnClick="btnSave_OnClick" />
			<aspire:HyperLink runat="server" ID="hlCancel" NavigateUrl="FeePayment.aspx" Text="Cancel" />
		</div>
	</div>
</asp:Panel>
