﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="AlumniProfessionalInformation.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Alumni.Report.AlumniProfessionalInformation" %>

<asp:Content runat="server" ContentPlaceHolderID="Parameters">
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Intake Semester:" AssociatedControlID="ddlSemesterID" ValidationGroup="Filter" />
				<aspire:AspireDropDownList runat="server" ValidationGroup="Filter" ID="ddlSemesterID" CausesValidation="False" AutoPostBack="false" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" ValidationGroup="Filter" Text="Program:" AssociatedControlID="ddlAdmissionOpenProgramID" />
				<aspire:AspireDropDownList runat="server" ValidationGroup="Filter" ID="ddlAdmissionOpenProgramID" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" ValidationGroup="Filter" Text="Job Status:" AssociatedControlID="ddlJobStatus" />
				<aspire:AspireDropDownList runat="server" ValidationGroup="Filter" ID="ddlJobStatus" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<div>
					<aspire:AspireLabel runat="server" Text="&nbsp;" AssociatedControlID="btnDisplayReport" />
				</div>
				<aspire:AspireButton runat="server" ButtonType="Primary" ID="btnDisplayReport" ValidationGroup="Filter" Text="Display Report" OnClick="btnDisplayReport_Click" />
			</div>
		</div>		
	</div>	
</asp:Content>

