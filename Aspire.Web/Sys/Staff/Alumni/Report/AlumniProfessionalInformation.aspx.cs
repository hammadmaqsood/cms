﻿using System;
using System.Collections.Generic;
using Aspire.Web.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Microsoft.Reporting.WebForms;

namespace Aspire.Web.Sys.Staff.Alumni.Report
{
	[AspirePage("BC178501-D96F-4ADB-8641-C90C4639CB02", UserTypes.Staff, "~/Sys/Staff/Alumni/Report/AlumniProfessionalInformation.aspx", "Alumni Professional Information", true, AspireModules.Alumni)]

	public partial class AlumniProfessionalInformation : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Alumni.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Alumni's Professional Information";
		public string ReportName => "Alumni's Professional Information";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterID.FillSemesters();
				this.ddlJobStatus.FillGuidEnums<AlumniJobPosition.JobStatuses>(CommonListItems.All);
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var admissionOpenPrograms = Aspire.BL.Core.Common.Lists.GetAdmissionOpenPrograms(semesterID.Value, this.StaffIdentity.InstituteID);
			this.ddlAdmissionOpenProgramID.DataBind(admissionOpenPrograms, CommonListItems.All);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (!this.IsPostBack)
				return;

			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var admissionOpenProgramID = this.ddlAdmissionOpenProgramID.SelectedValue.ToNullableInt();
			var jobStatusEnum = this.ddlJobStatus.GetSelectedNullableGuidEnum<AlumniJobPosition.JobStatuses>();

			var reportDataSet = BL.Core.Alumni.Staff.AlumniProfessionalInformation.GetProfessionalInformation(semesterID, admissionOpenProgramID, jobStatusEnum, this.StaffIdentity.LoginSessionGuid);

			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/Alumni/Staff/AlumniProfessionalInformation.rdlc".MapPath();
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.ProfessionalInformation), reportDataSet.ProfessionalInformation));
		}

		protected void btnDisplayReport_Click(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView.ReportViewer);
		}
	}
}
