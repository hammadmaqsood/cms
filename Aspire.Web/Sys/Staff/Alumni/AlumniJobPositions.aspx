﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="AlumniJobPositions.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Alumni.AlumniJobPositions" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Import Namespace="Aspire.Lib.Helpers" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">

	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
			<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="Search" OnSearch="tbEnrollment_Search" />
		</div>
	</div>
	<p></p>
	<asp:Panel runat="server" ID="panelStudentInformation" Visible="false">

		<asp:Repeater runat="server" ID="repeaterBasicInfo" ItemType="Aspire.BL.Core.Alumni.Staff.AlumniJobPositions.AlumniBasicInformation">
			<ItemTemplate>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Alumni Basic Information
						</h3>
					</div>

					<table class="table table-bordered table-condensed tableCol4">
						<tr>
							<th>Enrollment</th>
							<td><%#: Item.Enrollment %></td>
							<th>Name</th>
							<td><%#: Item.Name %></td>
							<td rowspan="7" style="min-width: 160px">
								<asp:Image runat="server" ID="imgPicture" ToolTip="Photo" CssClass="img img-thumbnail" AlternateText="<%# Item.Enrollment %>" ImageUrl="<%# Aspire.Web.Sys.Common.StudentPicture.GetImageUrl(Item.StudentID, TimeSpan.Zero) %>" />
							</td>
						</tr>
						<tr>
							<th>Department</th>
							<td><%#: Item.Department %></td>
							<th>Program</th>
							<td><%#: Item.ProgramAlias %> - <%#: Item.DurationEnum.ToFullName() %> </td>
						</tr>
						<tr>
							<th>Intake Semester</th>
							<td><%#: Item.IntakeSemester %></td>
							<th>Intake Year</th>
							<td><%#: Item.IntakeYear %></td>
						</tr>
						<tr>
							<th>Status</th>
							<td><%#: (Item.StatusEnum?.ToFullName()).ToNAIfNullOrEmpty() %></td>
							<th>Status Last Updated On</th>
							<td><%#: (Item.StatusDate?.ToString("F")).ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr runat="server" visible="<%# Item.StatusEnum == Student.Statuses.Blocked %>">
							<th>Reason for Blocking</th>
							<td colspan="3"><%#: Item.BlockedReason.ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr runat="server" visible="<%# Item.StatusEnum == Student.Statuses.Graduated %>">
							<th>Graduation Semester</th>
							<td><%#: Item.GraduationSemester %></td>
							<th>Graduation Year</th>
							<td><%#: Item.GraduationYear %></td>
						</tr>
					</table>
				</div>
			</ItemTemplate>
		</asp:Repeater>
	</asp:Panel>

	<aspire:AspireButton runat="server" Visible="False" ID="btnAddAlumniInformation" ButtonType="Success" Glyphicon="plus" Text="Add New Information" CausesValidation="false" OnClick="btnAddAlumniInformation_Click" />

	<p></p>
	<asp:Panel runat="server" ID="panelAlumniProfessionalInfo" Visible="false">

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Alumni Professional Information</h3>
			</div>

			<aspire:AspireGridView ItemType="Aspire.BL.Core.Alumni.Staff.AlumniJobPositions.AlumniProfessionalInformation" runat="server" ID="gvProfessionalInformation" Responsive="True" Condensed="True" AutoGenerateColumns="False" OnRowCommand="gvProfessionalInformation_RowCommand">
				<Columns>
					<asp:TemplateField HeaderText="#">
						<ItemTemplate>
							<%#: Container.DataItemIndex + 1 %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Job Status">
						<ItemTemplate>
							<%# Item.JobStatusEnum.GetFullName() %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Last Updated On">
						<ItemTemplate>
							<%# (Item.LastUpdatedOn.ToString("d")).ToNAIfNullOrEmpty() %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Organization">
						<ItemTemplate>
							<%# Item.Organization.ToNAIfNullOrEmpty() %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Designation">
						<ItemTemplate>
							<%# Item.Designation.ToNAIfNullOrEmpty() %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="University">
						<ItemTemplate>
							<%# Item.University.ToNAIfNullOrEmpty() %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Degree Title">
						<ItemTemplate>
							<%# Item.DegreeTitle.ToNAIfNullOrEmpty() %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Country">
						<ItemTemplate>
							<%# Item.Country.ToNAIfNullOrEmpty() %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="City">
						<ItemTemplate>
							<%# Item.City.ToNAIfNullOrEmpty() %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Mobile">
						<ItemTemplate>
							<%# Item.Mobile.ToNAIfNullOrEmpty() %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Phone">
						<ItemTemplate>
							<%# Item.Phone.ToNAIfNullOrEmpty() %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Personal Email">
						<ItemTemplate>
							<%# Item.PersonalEmail.ToNAIfNullOrEmpty() %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Mailing Address">
						<ItemTemplate>
							<%# Item.MailingAddress.ToNAIfNullOrEmpty() %>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField HeaderText="Actions">
						<ItemTemplate>
							<aspire:AspireLinkButton ToolTip="Edit" Glyphicon="edit" runat="server" CommandName="Edit" CausesValidation="false" EncryptedCommandArgument='<%#: Item.AlumniJobPositionID %>' />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</aspire:AspireGridView>
		</div>
	</asp:Panel>

	<aspire:AspireModal runat="server" ID="modalAlumniProfessionalInfo" ModalSize="Large">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updateModalAlumniProfessionalInfo" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertModalAlumniProfessionalInfo" />

					<div class="row">
						<div class="col-md-6">
							<aspire:AspireLabel runat="server" Text="Job Status:" AssociatedControlID="ddlModalJobStatus" />
							<aspire:AspireDropDownList runat="server" ID="ddlModalJobStatus" ValidationGroup="AddAlumniProfessional" CssClass="col-md-6" OnSelectedIndexChanged="ddlModalJobStatus_SelectedIndexChanged" AutoPostBack="true" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlModalJobStatus" ErrorMessage="This field is required." ValidationGroup="AddAlumniProfessional" />
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Last Updated On:" AssociatedControlID="dtpModalLastUpdatedOn" />
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpModalLastUpdatedOn" ValidationGroup="AddAlumniProfessional" />
								<aspire:AspireDateTimeValidator AllowNull="False" runat="server" ControlToValidate="dtpModalLastUpdatedOn" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid date." ValidationGroup="AddAlumniProfessional" />
							</div>
						</div>
					</div>

					<div class="row" id="divEmployed" runat="server">
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Organization:" ID="lblModalOrganization" AssociatedControlID="tbModalOrganization" />
								<aspire:AspireTextBox runat="server" ID="tbModalOrganization" ValidationGroup="AddAlumniProfessional" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbModalOrganization" ErrorMessage="This field is required." ValidationGroup="AddAlumniProfessional" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Designation:" ID="lblModalDesignation" AssociatedControlID="tbModalDesignation" />
								<aspire:AspireTextBox runat="server" ID="tbModalDesignation" ValidationGroup="AddAlumniProfessional" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbModalDesignation" ErrorMessage="This field is required." ValidationGroup="AddAlumniProfessional" />
							</div>
						</div>
					</div>

					<div class="row" id="divStudies" runat="server">
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="University:" ID="lblModalUniversity" AssociatedControlID="tbModalUniversity" />
								<aspire:AspireTextBox runat="server" ID="tbModalUniversity" ValidationGroup="AddAlumniProfessional" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbModalUniversity" ErrorMessage="This field is required." ValidationGroup="AddAlumniProfessional" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Degree Title:" ID="lblModalDegreeTitle" AssociatedControlID="tbModalDegreeTitle" />
								<aspire:AspireTextBox runat="server" ID="tbModalDegreeTitle" ValidationGroup="AddAlumniProfessional" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbModalDegreeTitle" ErrorMessage="This field is required." ValidationGroup="AddAlumniProfessional" />
							</div>
						</div>
					</div>

					<div class="row" id="divLocation" runat="server">
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Country:" ID="lblModalCountry" AssociatedControlID="tbModalCountry" />
								<aspire:AspireTextBox runat="server" ID="tbModalCountry" ValidationGroup="AddAlumniProfessional" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbModalCountry" ErrorMessage="This field is required." ValidationGroup="AddAlumniProfessional" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="City:" ID="lblModalCity" AssociatedControlID="tbModalCity" />
								<aspire:AspireTextBox runat="server" ID="tbModalCity" ValidationGroup="AddAlumniProfessional" />
							</div>
						</div>
					</div>

					<div class="row" id="divContact" runat="server">
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Mobile No:" ID="lblModalMobile" AssociatedControlID="tbModalMobile" />
								<aspire:AspireTextBox runat="server" ID="tbModalMobile" data-inputmask="'alias': 'mobile'" ValidationGroup="AddAlumniProfessional" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbModalMobile" ErrorMessage="This field is required." ValidationGroup="AddAlumniProfessional" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Phone No:" ID="lblModalPhone" AssociatedControlID="tbModalPhone" />
								<aspire:AspireTextBox runat="server" ID="tbModalPhone" ValidationGroup="AddAlumniProfessional" />
							</div>
						</div>
					</div>

					<div class="row" id="divMailing" runat="server">
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Personal Email:" ID="lblModalPersonalEmail" AssociatedControlID="tbModalPersonalEmail" />
								<aspire:AspireTextBox runat="server" TextTransform="LowerCase" MaxLength="100" TextMode="Email" ID="tbModalPersonalEmail" PlaceHolder="someone@example.com" ValidationGroup="AddAlumniProfessional" />
								<aspire:AspireStringValidator runat="server" ValidationGroup="AddAlumniProfessional" ControlToValidate="tbModalPersonalEmail" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Email Address." ValidationExpression="Email" />

							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Mailing Address:" ID="lblModalMailingAddress" AssociatedControlID="tbModalMailingAddress" />
								<aspire:AspireTextBox runat="server" ID="tbModalMailingAddress" TextMode="MultiLine" MaxLength="500" ValidationGroup="AddAlumniProfessional" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSaveAlumniProfessionalInfo" Text="Save" ButtonType="Primary" ValidationGroup="AddAlumniProfessional" OnClick="btnSaveAlumniProfessionalInfo_Click" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>

