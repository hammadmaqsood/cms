﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Staff.Alumni
{
	[AspirePage("5570B7D7-BF14-4087-8BF9-DD3C7C44F5C7", UserTypes.Staff, "~/Sys/Staff/Alumni/AlumniJobPositions.aspx", "Alumni Job Positions", true, AspireModules.Alumni)]

	public partial class AlumniJobPositions : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Alumni.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_user);

		public override string PageTitle => "Add Alumni's Information";

		public string Enrollment
		{
			get => (string)this.ViewState[nameof(this.Enrollment)];
			private set => this.ViewState[nameof(this.Enrollment)] = value;
		}

		public int StudentID
		{
			get => (int)this.ViewState[nameof(this.StudentID)];
			private set => this.ViewState[nameof(this.StudentID)] = value;
		}

		public Guid? JobPositionID
		{
			get => (Guid?)this.ViewState[nameof(this.JobPositionID)];
			private set => this.ViewState[nameof(this.JobPositionID)] = value;
		}

		public static string GetPageUrl(string enrollment, bool search)
		{
			return GetAspirePageAttribute<AlumniJobPositions>().PageUrl.AttachQueryParams("Enrollment", enrollment, "Search", search);
		}

		public new static void Redirect(string enrollment, bool search)
		{
			Redirect(GetPageUrl(enrollment, search));
		}

		public void Refresh()
		{
			Redirect(this.Enrollment, true);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.panelStudentInformation.Visible = false;
				this.panelAlumniProfessionalInfo.Visible = false;

				var enrollment = this.Request.GetParameterValue("Enrollment");
				this.tbEnrollment.Enrollment = enrollment;
				if (this.Request.GetParameterValue<bool>("Search") == true)
					this.tbEnrollment_Search(null, null);
			}
		}

		protected void tbEnrollment_Search(object sender, EventArgs args)
		{
			var enrollment = this.tbEnrollment.Enrollment;
			var alumniBasicInformation = BL.Core.Alumni.Staff.AlumniJobPositions.GetAlumniBasicInformation(enrollment, this.AspireIdentity.LoginSessionGuid);
			if (alumniBasicInformation == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect(enrollment, false);
				return;
			}
			this.StudentID = alumniBasicInformation.StudentID;
			this.Enrollment = alumniBasicInformation.Enrollment;

			if (alumniBasicInformation.StatusEnum != Model.Entities.Student.Statuses.Graduated)
			{
				this.AddErrorAlert("Please enter graduated alumni enrollment.");
				this.panelStudentInformation.Visible = false;
				this.panelAlumniProfessionalInfo.Visible = false;
				this.btnAddAlumniInformation.Visible = false;
				return;
			}

			this.panelStudentInformation.Visible = true;
			this.panelAlumniProfessionalInfo.Visible = false;
			this.repeaterBasicInfo.DataBind(new[] { alumniBasicInformation });
			this.btnAddAlumniInformation.Visible = true;
			var getProfessionalInfo = alumniBasicInformation.ProfessionalInformation;
			if (getProfessionalInfo.Any())
			{
				this.panelAlumniProfessionalInfo.Visible = true;
				this.gvProfessionalInformation.DataBind(getProfessionalInfo);
			}
		}

		private void DisplayApplicationApprovalModal(Guid? alumniJobPositionID)
		{
			this.ddlModalJobStatus.FillEnums<AlumniJobPosition.JobStatuses>(CommonListItems.Select);

			if (alumniJobPositionID == null)
			{
				var alumni = BL.Core.Alumni.Staff.AlumniJobPositions.GetAlumniPreviousDetails(this.StudentID, this.AspireIdentity.LoginSessionGuid);
				this.dtpModalLastUpdatedOn.SelectedDate = null;
				this.tbModalOrganization.Text = "";
				this.tbModalDesignation.Text = "";
				this.tbModalCountry.Text = "";
				this.tbModalCity.Text = "";
				this.tbModalPhone.Text = "";
				this.tbModalMobile.Text = alumni.Item1;
				this.tbModalPersonalEmail.Text = "";
				this.tbModalMailingAddress.Text = alumni.Item2;

				this.JobPositionID = null;
				this.ddlModalJobStatus.Enabled = true;
				this.modalAlumniProfessionalInfo.HeaderText = "Add Alumni's Professional Information";
			}
			else
			{
				var alumniData = Aspire.BL.Core.Alumni.Staff.AlumniJobPositions.GetAlumniProfessionalInformation(alumniJobPositionID.Value, this.AspireIdentity.LoginSessionGuid);

				this.ddlModalJobStatus.Enabled = false;
				this.ddlModalJobStatus.SetEnumValue(alumniData.JobStatusEnum);
				this.dtpModalLastUpdatedOn.SelectedDate = alumniData.LastUpdatedOn.Date;
				this.tbModalOrganization.Text = alumniData.Organization;
				this.tbModalDesignation.Text = alumniData.Designation;
				this.tbModalUniversity.Text = alumniData.University;
				this.tbModalDegreeTitle.Text = alumniData.DegreeTitle;
				this.tbModalCountry.Text = alumniData.Country;
				this.tbModalCity.Text = alumniData.City;
				this.tbModalPhone.Text = alumniData.Phone;
				this.tbModalMobile.Text = alumniData.Mobile;
				this.tbModalPersonalEmail.Text = alumniData.PersonalEmail;
				this.tbModalMailingAddress.Text = alumniData.MailingAddress;

				this.JobPositionID = alumniData.AlumniJobPositionID;
				this.modalAlumniProfessionalInfo.HeaderText = "Edit Alumni's Professional Information";
			}
			this.modalAlumniProfessionalInfo.Show();
		}

		protected void btnAddAlumniInformation_Click(object sender, EventArgs e)
		{
			this.InvisibleFields();
			this.DisplayApplicationApprovalModal(null);
		}

		protected void gvProfessionalInformation_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayApplicationApprovalModal(e.DecryptedCommandArgumentToGuid());
					this.ddlModalJobStatus_SelectedIndexChanged(null, null);
					break;
				default:
					throw new NotImplementedException(e.CommandName);
			}
		}

		protected void btnSaveAlumniProfessionalInfo_Click(object sender, EventArgs e)
		{
			var jobStatusEnum = this.ddlModalJobStatus.SelectedValue.ToEnum<AlumniJobPosition.JobStatuses>();
			if (this.JobPositionID == null)
			{

				var resultAdd = BL.Core.Alumni.Staff.AlumniJobPositions.AddProfessionalInformation(this.StudentID, jobStatusEnum,
					this.dtpModalLastUpdatedOn.SelectedDate, this.tbModalOrganization.Text, this.tbModalDesignation.Text, this.tbModalUniversity.Text,
					this.tbModalDegreeTitle.Text, this.tbModalCountry.Text, this.tbModalCity.Text, this.tbModalPhone.Text, this.tbModalMobile.Text,
					this.tbModalPersonalEmail.Text, this.tbModalMailingAddress.Text, this.AspireIdentity.LoginSessionGuid);

				switch (resultAdd)
				{
					case BL.Core.Alumni.Staff.AlumniJobPositions.AddUpdateProfessionalInformationStatuses.NoRecordFound:
						this.alertModalAlumniProfessionalInfo.AddNoRecordFoundAlert();
						this.updateModalAlumniProfessionalInfo.Update();
						return;
					case BL.Core.Alumni.Staff.AlumniJobPositions.AddUpdateProfessionalInformationStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Alumni professional record");
						this.Refresh();
						return;
					default:
						throw new NotImplementedEnumException(resultAdd);
				}
			}
			else
			{
				var resultUpdate = BL.Core.Alumni.Staff.AlumniJobPositions.UpdateProfessionalInformation(this.JobPositionID.Value, jobStatusEnum,
					this.dtpModalLastUpdatedOn.SelectedDate, this.tbModalOrganization.Text, this.tbModalDesignation.Text, this.tbModalUniversity.Text,
					this.tbModalDegreeTitle.Text, this.tbModalCountry.Text, this.tbModalCity.Text, this.tbModalPhone.Text, this.tbModalMobile.Text,
					this.tbModalPersonalEmail.Text, this.tbModalMailingAddress.Text, this.AspireIdentity.LoginSessionGuid);

				switch (resultUpdate)
				{
					case BL.Core.Alumni.Staff.AlumniJobPositions.AddUpdateProfessionalInformationStatuses.NoRecordFound:
						this.alertModalAlumniProfessionalInfo.AddNoRecordFoundAlert();
						this.updateModalAlumniProfessionalInfo.Update();
						return;
					case BL.Core.Alumni.Staff.AlumniJobPositions.AddUpdateProfessionalInformationStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Alumni professional record");
						this.Refresh();
						return;
					default:
						throw new NotImplementedEnumException(resultUpdate);
				}
			}
		}

		protected void ddlModalJobStatus_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.InvisibleFields();
			var jobStatusEnum = this.ddlModalJobStatus.SelectedValue.ToNullableEnum<AlumniJobPosition.JobStatuses>();
			if (jobStatusEnum == null)
				return;

			switch (jobStatusEnum)
			{
				case AlumniJobPosition.JobStatuses.Unemployed:
				case AlumniJobPosition.JobStatuses.HomeMaker:
					this.VisibleFields();
					return;
				case AlumniJobPosition.JobStatuses.Employed:
				case AlumniJobPosition.JobStatuses.SelfEmpoyed:
					this.VisibleFields();
					this.divEmployed.Visible = true;
					return;
				case AlumniJobPosition.JobStatuses.HigherStudies:
					this.VisibleFields();
					this.divStudies.Visible = true;
					return;
				case AlumniJobPosition.JobStatuses.NonContactable:
					return;
				case AlumniJobPosition.JobStatuses.Other:
					return;
				default:
					throw new NotImplementedEnumException(jobStatusEnum);
			}
		}

		private void InvisibleFields()
		{
			this.divEmployed.Visible = false;
			this.divStudies.Visible = false;
			this.divLocation.Visible = false;
			this.divContact.Visible = false;
			this.divMailing.Visible = false;
		}

		private void VisibleFields()
		{
			this.divLocation.Visible = true;
			this.divContact.Visible = true;
			this.divMailing.Visible = true;
		}
	}
}