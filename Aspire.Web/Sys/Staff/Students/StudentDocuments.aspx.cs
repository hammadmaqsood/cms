﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Students
{
	[AspirePage("40AA0E8E-8637-4459-A8E0-6A048B2F59A8", UserTypes.Staff, "~/Sys/Staff/Students/StudentDocuments.aspx", "Student Documents", true, AspireModules.StudentInformation)]
	public partial class StudentDocuments : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.StudentInformation.ViewStudentDocuments, new []{UserGroupPermission.PermissionValues.Allowed}}
		};
		public override PageIcon PageIcon => AspireGlyphicons.file.GetIcon();
		public override string PageTitle => "Student Documents";

		public static string GetPageUrl(string enrollment, bool search)
		{
			return typeof(StudentDocuments).GetAspirePageAttribute().PageUrl.AttachQueryParams(("Enrollment", enrollment), ("Search", search));
		}

		public static new void Redirect(string enrollment, bool search)
		{
			Redirect(GetPageUrl(enrollment, search));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.studentDocuments.Visible = false;
				this.tbEnrollment.Enrollment = this.GetParameterValue("Enrollment");
				if (this.GetParameterValue<bool>("Search") == true)
					this.tbEnrollment_Search(null, null);
			}
		}

		protected void tbEnrollment_Search(object sender, EventArgs args)
		{
			if (this.IsPostbackAndNotValid)
				return;
			var enrollment = this.tbEnrollment.Enrollment;
			this.studentDocuments.Show(enrollment);
		}
	}
}