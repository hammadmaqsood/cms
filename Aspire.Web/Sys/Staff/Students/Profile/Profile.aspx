﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Base.Master" CodeBehind="Profile.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.Profile.Profile" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Import Namespace="Aspire.Lib.Helpers" %>
<%@ Register Src="~/Sys/Staff/Students/Profile/BasicInformation.ascx" TagPrefix="uc1" TagName="BasicInformation" %>
<%@ Register Src="~/Sys/Staff/Students/Profile/PersonalInformation.ascx" TagPrefix="uc1" TagName="PersonalInformation" %>
<%@ Register Src="~/Sys/Staff/Students/Profile/EmergencyInformation.ascx" TagPrefix="uc1" TagName="EmergencyInformation" %>
<%@ Register Src="~/Sys/Staff/Students/Profile/FatherInformation.ascx" TagPrefix="uc1" TagName="FatherInformation" %>
<%@ Register Src="~/Sys/Staff/Students/Profile/SponsorInformation.ascx" TagPrefix="uc1" TagName="SponsorInformation" %>
<%@ Register Src="~/Sys/Staff/Students/Profile/AdditionalInformation.ascx" TagPrefix="uc1" TagName="AdditionalInformation" %>
<%@ Register Src="~/Sys/Staff/Students/Profile/AcademicInformation.ascx" TagPrefix="uc1" TagName="AcademicInformation" %>
<%@ Register Src="~/Sys/Staff/Students/Profile/DegreeInformation.ascx" TagPrefix="uc1" TagName="DegreeInformation" %>
<%@ Register Src="~/Sys/Common/PhotoUploadUserControl.ascx" TagPrefix="uc1" TagName="PhotoUploadUserControl" %>

<asp:Content runat="server" ContentPlaceHolderID="BodyPH">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
			<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="Search" OnSearch="tbEnrollment_Search" />
		</div>
	</div>
	<asp:Panel runat="server" ID="panelStudent">
		<p></p>
		<asp:Repeater runat="server" ID="repeaterStudent" ItemType="Aspire.Model.Entities.Student">
			<ItemTemplate>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Basic Information
						<aspire:AspireLinkButton runat="server" Glyphicon="edit" ToolTip="Edit" CssClass="pull-right text-primary" ID="lbtnEditBasic" CausesValidation="False" OnClick="lbtnEditBasic_OnClick" />
						</h3>
					</div>
					<table class="table table-bordered table-condensed tableCol4">
						<tr>
							<th>Enrollment</th>
							<td><%#: Item.Enrollment %></td>
							<th>Registration No.</th>
							<td><%#: Item.RegistrationNo.ToNAIfNullOrEmpty() %></td>
							<td rowspan="7" style="min-width: 160px">
								<asp:Image runat="server" ID="imgPicture" ToolTip="Photo" CssClass="img img-thumbnail" AlternateText="<%# Item.Enrollment %>" ImageUrl="<%# Aspire.Web.Sys.Common.StudentPicture.GetImageUrl(Item.StudentID, TimeSpan.Zero) %>" />
								<div style="margin-top: 10px" class="text-center">
									<aspire:AspireButton runat="server" ID="btnUpload" CssClass="btn-sm" ButtonType="Default" Text="Browse" CausesValidation="False" OnClick="btnUpload_OnClick" />
								</div>
							</td>
						</tr>
						<tr>
							<th>Application No.</th>
							<td><%#: Item.CandidateAppliedProgram?.ApplicationNo.ToNAIfNullOrEmpty() %></td>
							<th>Applied Semester</th>
							<td><%#: Item.CandidateAppliedProgram?.Candidate.SemesterID.ToSemesterString() %></td>
						</tr>
						<tr>
							<th>Name</th>
							<td><%#: Item.Name %></td>
							<th>Admission Type</th>
							<td><%#: Item.AdmissionTypeEnum.ToFullName() %></td>
						</tr>
						<tr>
							<th>Program</th>
							<td><%#: Item.AdmissionOpenProgram.Program.ProgramAlias %> - <%#: Item.AdmissionOpenProgram.Program.DurationFullName %></td>
							<th>Intake Semester</th>
							<td><%#: Item.AdmissionOpenProgram.SemesterID.ToSemesterString() %></td>
						</tr>
						<tr>
							<th>Final Semester</th>
							<td><%#: Item.AdmissionOpenProgram.FinalSemesterID.ToSemesterString() %></td>
							<th>Max. Allowed</th>
							<td><%#: Item.AdmissionOpenProgram.MaxSemesterID.ToSemesterString() %></td>
						</tr>
						<tr>
							<th>Status</th>
							<td><%#: (Item.StatusEnum?.ToFullName()).ToNAIfNullOrEmpty() %></td>
							<th>Status Last Updated On</th>
							<td><%#: (Item.StatusDate?.ToString("F")).ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr runat="server" visible="<%# Item.StatusEnum == Student.Statuses.Blocked %>">
							<th>Reason for Blocking</th>
							<td colspan="3"><%#: Item.BlockedReason.ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr runat="server" id="trDeferredTo" visible="False">
							<th>Deferred To:</th>
							<td>
								<aspire:Label runat="server" ID="lblDeferredTo" /></td>
							<th>Deferred To Enrollment:</th>
							<td>
								<aspire:AspireHyperLink runat="server" ID="hlDeferredToEnrollment" Target="_blank" />
							</td>
						</tr>
						<tr runat="server" id="trDeferredFrom" visible="False">
							<th>Deferred From:</th>
							<td>
								<aspire:Label runat="server" ID="lblDeferredFrom" /></td>
							<th>Deferred From Enrollment:</th>
							<td>
								<aspire:AspireHyperLink runat="server" ID="hlDeferredFromEnrollment" Target="_blank" />
							</td>
						</tr>
					</table>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Personal Information
						<aspire:AspireLinkButton runat="server" Glyphicon="edit" ToolTip="Edit" CssClass="pull-right text-primary" ID="lbtnEditPersonal" CausesValidation="False" OnClick="lbtnEditPersonal_OnClick" />
						</h3>
					</div>
					<table class="table table-bordered table-condensed tableCol4">
						<tr>
							<th>Personal Email</th>
							<td><%#: Item.PersonalEmail.ToNAIfNullOrEmpty() %></td>
							<th>University Email</th>
							<td><%#: Item.UniversityEmail.ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Phone No. (Home)</th>
							<td><%#: (Item.Phone).ToNAIfNullOrEmpty() %></td>
							<th>Mobile No.</th>
							<td><%#: (Item.Mobile).ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>CNIC No.</th>
							<td><%#: (Item.CNIC).ToNAIfNullOrEmpty() %></td>
							<th>Passport No.</th>
							<td><%#: (Item.PassportNo).ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Date of Birth</th>
							<td><%#: (Item.DOB.ToShortDateString()).ToNAIfNullOrEmpty() %></td>
							<th>Gender</th>
							<td><%#: (Item.GenderEnum.ToFullName()).ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Category</th>
							<td><%#: (Item.CategoryEnum.ToFullName()).ToNAIfNullOrEmpty() %></td>
							<th>Blood Group</th>
							<td><%#: (Item.BloodGroupEnum?.ToFullName()).ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Nationality</th>
							<td><%#: (Item.Nationality).ToNAIfNullOrEmpty() %></td>
							<th>Country</th>
							<td><%#: (Item.Country).ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>State/Province</th>
							<td><%#: (Item.Province).ToNAIfNullOrEmpty() %></td>
							<th>District/City</th>
							<td><%#: (Item.District).ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Tehsil</th>
							<td><%#: (Item.Tehsil).ToNAIfNullOrEmpty() %></td>
							<th>Domicile</th>
							<td><%#: (Item.Domicile).ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Area Type</th>
							<td><%#: (Item.AreaTypeEnum?.ToFullName()).ToNAIfNullOrEmpty() %></td>
							<th>Service No. (If Naval)</th>
							<td><%#: (Item.ServiceNo).ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Religion</th>
							<td><%#: (Item.Religion).ToNAIfNullOrEmpty() %></td>
							<th>Next of Kin</th>
							<td><%#: (Item.NextOfKin).ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Current Address</th>
							<td colspan="3"><%#: (Item.CurrentAddress).ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Permanent Address</th>
							<td colspan="3"><%#: (Item.PermanentAddress).ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Physical Disability</th>
							<td colspan="3"><%#: (Item.PhysicalDisability).ToNAIfNullOrEmpty() %></td>
						</tr>
					</table>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Emergency Information
						<aspire:AspireLinkButton runat="server" Glyphicon="edit" ToolTip="Edit" CssClass="pull-right text-primary" ID="lbtnEmergency" CausesValidation="False" OnClick="lbtnEmergency_OnClick" />
						</h3>
					</div>
					<table class="table table-bordered table-condensed tableCol4">
						<tr>
							<th>Contact Name</th>
							<td><%#: Item.EmergencyContactName.ToNAIfNullOrEmpty() %></td>
							<th>Mobile No.</th>
							<td><%#: Item.EmergencyMobile.ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Phone No.</th>
							<td><%#: Item.EmergencyPhone.ToNAIfNullOrEmpty() %></td>
							<th>Relationship</th>
							<td><%#: Item.EmergencyRelationship.ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Address</th>
							<td colspan="3"><%#: Item.EmergencyAddress.ToNAIfNullOrEmpty() %></td>
						</tr>
					</table>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Father's Information
						<aspire:AspireLinkButton runat="server" Glyphicon="edit" ToolTip="Edit" CssClass="pull-right text-primary" ID="lbtnEditFather" CausesValidation="False" OnClick="lbtnEditFather_OnClick" />
						</h3>
					</div>
					<table class="table table-bordered table-condensed tableCol4">
						<tr>
							<th>Name</th>
							<td><%#: Item.FatherName.ToNAIfNullOrEmpty() %></td>
							<th>CNIC No.</th>
							<td><%#: Item.FatherCNIC.ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Passport No.</th>
							<td><%#: Item.FatherPassportNo.ToNAIfNullOrEmpty() %></td>
							<th>Designation/Profession</th>
							<td><%#: Item.FatherDesignation.ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Department/Unit</th>
							<td><%#: Item.FatherDepartment.ToNAIfNullOrEmpty() %></td>
							<th>Organization</th>
							<td><%#: Item.FatherOrganization.ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Service No. (If Naval)</th>
							<td><%#: Item.FatherServiceNo.ToNAIfNullOrEmpty() %></td>
							<th>Mobile No.</th>
							<td><%#: Item.FatherMobile.ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Phone No. (Office)</th>
							<td><%#: Item.FatherOfficePhone.ToNAIfNullOrEmpty() %></td>
							<th>Phone No. (Home)</th>
							<td><%#: Item.FatherHomePhone.ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Fax</th>
							<td><%#: Item.FatherFax.ToNAIfNullOrEmpty() %></td>
						</tr>
					</table>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Sponsor's Information
							<aspire:AspireLinkButton runat="server" Glyphicon="edit" ToolTip="Edit" CssClass="pull-right text-primary" ID="lbtnEditSponsor" CausesValidation="False" OnClick="lbtnEditSponsor_OnClick" />
						</h3>
					</div>
					<table class="table table-bordered table-condensed tableCol4">
						<tr>
							<th>Sponsored By</th>
							<td><%#: (Item.SponsoredByEnum?.ToFullName()).ToNAIfNullOrEmpty() %></td>
							<th>Name</th>
							<td><%#: Item.SponsorName.ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>CNIC No.</th>
							<td><%#: Item.SponsorCNIC.ToNAIfNullOrEmpty() %></td>
							<th>Passport No.</th>
							<td><%#: Item.SponsorPassportNo.ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Relationship</th>
							<td><%#: Item.SponsorRelationshipWithGuardian.ToNAIfNullOrEmpty() %></td>
							<th>Designation/Profession</th>
							<td><%#: Item.SponsorDesignation.ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Department/Unit</th>
							<td><%#: Item.SponsorDepartment.ToNAIfNullOrEmpty() %></td>
							<th>Organization</th>
							<td><%#: Item.SponsorOrganization.ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Service No. (If Naval)</th>
							<td><%#: Item.SponsorServiceNo.ToNAIfNullOrEmpty() %></td>
							<th>Mobile No.</th>
							<td><%#: Item.SponsorMobile.ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Phone No. (Office)</th>
							<td><%#: Item.SponsorPhoneOffice.ToNAIfNullOrEmpty() %></td>
							<th>Phone No. (Home)</th>
							<td><%#: Item.SponsorPhoneHome.ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Fax</th>
							<td><%#: Item.SponsorFax.ToNAIfNullOrEmpty() %></td>
						</tr>
					</table>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Current Academic Information
							<aspire:AspireLinkButton runat="server" Glyphicon="edit" ToolTip="Edit" CssClass="pull-right text-primary" ID="lbtnEditDegree" CausesValidation="False" OnClick="lbtnEditDegree_Click" />
						</h3>
					</div>
					<table class="table table-bordered table-condensed tableCol4">
						<tr>
							<th>Final Transcript No.</th>
							<td><%#: Item.FinalTranscriptNo.ToNAIfNullOrEmpty() %></td>
							<th>Final Transcript Issue Date</th>
							<td><%#: (Item.FinalTranscriptIssueDate?.ToString("d")).ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Final CGPA</th>
							<td><%#: (Item.FinalCGPA.FormatGPA()).ToNAIfNullOrEmpty() %></td>
							<th>Degree No.</th>
							<td><%#: Item.DegreeNo.ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Degree Gazette No.</th>
							<td><%#: Item.DegreeGazetteNo.ToNAIfNullOrEmpty() %></td>
							<th>Degree Issue Date</th>
							<td><%#: (Item.DegreeIssueDate?.ToString("D")).ToNAIfNullOrEmpty() %></td>
						</tr>
						<tr>
							<th>Degree Issued to Student Date</th>
							<td><%#: (Item.DegreeIssuedToStudent?.ToString("D")).ToNAIfNullOrEmpty() %></td>
						</tr>
					</table>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Previous Academic Information
							<aspire:AspireLinkButton runat="server" Glyphicon="plus" ToolTip="Add" CssClass="pull-right text-success" ID="lbtnEditAcademic" CausesValidation="False" OnClick="lbtnEditAcademic_OnClick" />
						</h3>
					</div>
					<table class="table table-bordered table-condensed table-hover table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Degree</th>
								<th>Institute</th>
								<th>Board/University</th>
								<th>Subjects</th>
								<th>Marks</th>
								<th>Percentage</th>
								<th>Passing Year</th>
								<th>Remarks</th>
								<th>HEC Verification Stamp Date</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<tr runat="server" visible="<%# !Item.StudentAcademicRecords.Any() %>">
								<td colspan="12">No record found.</td>
							</tr>
							<asp:Repeater ID="repeaterAcademicRecord" runat="server" ItemType="Aspire.Model.Entities.StudentAcademicRecord" DataSource="<%# Item.StudentAcademicRecords.OrderBy(sar=> sar.DegreeType) %>" OnItemCommand="repeaterAcademicRecord_ItemCommand">
								<ItemTemplate>
									<tr>
										<td><%#: Container.ItemIndex + 1 %></td>
										<td><%#: (Item.DegreeTypeEnum).ToFullName() %></td>
										<td><%#: (Item.Institute).ToNAIfNullOrEmpty() %></td>
										<td><%#: (Item.BoardUniversity).ToNAIfNullOrEmpty() %></td>
										<td><%#: (Item.Subjects).ToNAIfNullOrEmpty() %></td>
										<td><%#: $"{Item.ObtainedMarks:#.##}/{Item.TotalMarks:#.##}" %></td>
										<td><%#: $"{Item.Percentage:#.##}%" %></td>
										<td><%#: (Item.PassingYear).ToNAIfNullOrEmpty() %></td>
										<td><%#: (Item.Remarks).ToNAIfNullOrEmpty() %></td>
										<td>
											<aspire:AspireLinkButton runat="server" Text='<%#: (Item.HECDegreeVerificationStampDate?.ToString("d")) ?? "Add" %>' ToolTip="HEC Degree Verification" ButtonType="Success" ID="btnHECDegreeVerification" CausesValidation="False" OnClick="btnHECDegreeVerification_OnClick" />
										</td>
										<td><%#: (Item.StatusEnum?.ToFullName()).ToNAIfNullOrEmpty() %></td>
										<td>
											<aspire:AspireLinkButton runat="server" ToolTip="Edit" Glyphicon="edit" CausesValidation="false" CommandName="Edit" EncryptedCommandArgument="<%# Item.StudentAcademicRecordID %>" />
											<aspire:AspireLinkButton runat="server" ToolTip="Delete" Glyphicon="remove" CausesValidation="false" CommandName="Delete" ConfirmMessage="Are you sure you want to delete?" EncryptedCommandArgument="<%# Item.StudentAcademicRecordID %>" />
										</td>
									</tr>
								</ItemTemplate>
							</asp:Repeater>
						</tbody>
					</table>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Additional Information
						<aspire:AspireLinkButton runat="server" Glyphicon="plus" ToolTip="Add" CssClass="pull-right text-success" ID="lbtnEditAdditional" CausesValidation="False" OnClick="lbtnEditAdditional_OnClick" />
						</h3>
					</div>
					<table class="table table-bordered table-condensed table-hover table-striped">
						<thead>
							<tr>
								<th>Type</th>
								<th>Date</th>
								<th>Subject</th>
								<th>Description</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<tr runat="server" visible="<%# !Item.StudentCustomInfos.Any() %>">
								<td colspan="6">No record found.</td>
							</tr>
							<asp:Repeater ID="repeaterAdditionalInfo" runat="server" ItemType="Aspire.Model.Entities.StudentCustomInfo" DataSource="<%# Item.StudentCustomInfos.OrderBy(sci=> sci.CreatedDate) %>" OnItemCommand="repeaterAdditionalInfo_ItemCommand">
								<ItemTemplate>
									<tr>
										<td><%#: (Item.Type?.GetFullName()).ToNAIfNullOrEmpty() %></td>
										<td><%#: (Item.ReferenceDate?.ToShortDateString()).ToNAIfNullOrEmpty() %></td>
										<td><%#: Item.Subject.ToNAIfNullOrEmpty() %></td>
										<td><%# Item.Description.HtmlEncode(true) %></td>
										<td>
											<aspire:AspireLinkButton runat="server" ToolTip="Edit" Glyphicon="edit" CausesValidation="false" CommandName="Edit" EncryptedCommandArgument="<%# Item.StudentCustomInfoID %>" />
											<aspire:AspireLinkButton runat="server" ToolTip="Delete" Glyphicon="remove" CausesValidation="false" CommandName="Delete" ConfirmMessage="Are you sure you want to delete?" EncryptedCommandArgument="<%# Item.StudentCustomInfoID %>" />
										</td>
									</tr>
								</ItemTemplate>
							</asp:Repeater>
						</tbody>
					</table>
				</div>
			</ItemTemplate>
		</asp:Repeater>
		<uc1:BasicInformation runat="server" ID="basicInfo" />
		<uc1:PhotoUploadUserControl runat="server" ID="photoUploadUserControl" OnSave="photoUploadUserControl_OnSave" />
		<uc1:PersonalInformation runat="server" ID="personalInfo" />
		<uc1:EmergencyInformation runat="server" ID="emergencyInfo" />
		<uc1:FatherInformation runat="server" ID="fatherInfo" />
		<uc1:SponsorInformation runat="server" ID="sponsorInfo" />
		<uc1:AdditionalInformation runat="server" ID="additionalInfo" />
		<uc1:AcademicInformation runat="server" ID="academicInfo" />
		<uc1:DegreeInformation runat="server" ID="DegreeInfo" />
	</asp:Panel>
</asp:Content>


