﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SponsorInformation.ascx.cs" Inherits="Aspire.Web.Sys.Staff.Students.Profile.SponsorInformation" %>

<aspire:AspireModal runat="server" ID="modal" ModalSize="Large" HeaderText="Sponsor Information">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanelMain">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alertModal" />
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
							<aspire:AspireTextBox runat="server" ID="tbEnrollment" ReadOnly="True" ValidationGroup="Sponsor" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbName" />
							<aspire:AspireTextBox runat="server" ID="tbName" TextMode="SingleLine" ValidationGroup="Sponsor" ReadOnly="true" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Sponsored By:" AssociatedControlID="ddlSponsoredBy" />
							<aspire:AspireDropDownList runat="server" ID="ddlSponsoredBy" AutoPostBack="True" CausesValidation="false" OnSelectedIndexChanged="ddlSponsoredBy_SelectedIndexChanged" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSponsoredBy" ValidationGroup="Sponsor" ErrorMessage="This field is required." />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbSponsorName" />
							<aspire:AspireTextBox TextTransform="UpperCase" ValidationGroup="Sponsor" runat="server" MaxLength="100" ID="tbSponsorName" />
							<aspire:AspireStringValidator ID="svtbSponsorName" runat="server" ValidationGroup="Sponsor" ControlToValidate="tbSponsorName" AllowNull="True" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="CNIC No.:" AssociatedControlID="tbSponsorCNIC" />
							<aspire:AspireTextBox ValidationGroup="Sponsor" runat="server" MaxLength="15" ID="tbSponsorCNIC" data-inputmask="'alias': 'cnic'" />
							<aspire:AspireStringValidator ID="svtbSponsorCNIC" runat="server" AllowNull="True" ValidationGroup="Sponsor" ControlToValidate="tbSponsorCNIC" InvalidDataErrorMessage="Invalid CNIC." RequiredErrorMessage="This field is required." ValidationExpression="CNIC" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Passport No.:" AssociatedControlID="tbSponsorPassportNo" />
							<aspire:AspireTextBox ValidationGroup="Sponsor" runat="server" MaxLength="50" ID="tbSponsorPassportNo" />
							<aspire:AspireStringValidator ID="svtbSponsorPassportNo" runat="server" ValidationGroup="Sponsor" ControlToValidate="tbSponsorPassportNo" AllowNull="True" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Relationship:" AssociatedControlID="tbSponsorRelationshipWithGuardian" />
							<aspire:AspireTextBox ValidationGroup="Sponsor" runat="server" MaxLength="50" ID="tbSponsorRelationshipWithGuardian" />
							<aspire:AspireStringValidator ID="svtbSponsorRelationshipWithGuardian" runat="server" ValidationGroup="Sponsor" ControlToValidate="tbSponsorRelationshipWithGuardian" AllowNull="True" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Designation:" AssociatedControlID="tbSponsorDesignation" />
							<aspire:AspireTextBox ValidationGroup="Sponsor" runat="server" MaxLength="50" ID="tbSponsorDesignation" />
							<aspire:AspireStringValidator ID="svtbSponsorDesignation" runat="server" ValidationGroup="Sponsor" ControlToValidate="tbSponsorDesignation" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="tbSponsorDepartment" />
							<aspire:AspireTextBox ValidationGroup="Sponsor" runat="server" MaxLength="50" ID="tbSponsorDepartment" />
							<aspire:AspireStringValidator ID="svtbSponsorDepartment" runat="server" ValidationGroup="Sponsor" ControlToValidate="tbSponsorDepartment" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Organization:" AssociatedControlID="tbSponsorOrganization" />
							<aspire:AspireTextBox ValidationGroup="Sponsor" runat="server" MaxLength="50" ID="tbSponsorOrganization" />
							<aspire:AspireStringValidator ID="svtbSponsorOrganization" runat="server" ValidationGroup="Sponsor" ControlToValidate="tbSponsorOrganization" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Service No. (If Naval):" AssociatedControlID="tbSponsorServiceNo" />
							<aspire:AspireTextBox ValidationGroup="Sponsor" runat="server" MaxLength="50" ID="tbSponsorServiceNo" />
							<aspire:AspireStringValidator ID="svtbSponsorServiceNo" runat="server" ValidationGroup="Sponsor" ControlToValidate="tbSponsorServiceNo" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Mobile No:" AssociatedControlID="tbSponsorMobile" />
							<aspire:AspireTextBox ValidationGroup="Sponsor" runat="server" data-inputmask="'alias': 'mobile'" ID="tbSponsorMobile" MaxLength="50" />
							<aspire:AspireStringValidator ID="svtbSponsorMobile" runat="server" ValidationGroup="Sponsor" ControlToValidate="tbSponsorMobile" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Phone No. (Office):" AssociatedControlID="tbSponsorPhoneOffice" />
							<aspire:AspireTextBox ValidationGroup="Sponsor" runat="server" MaxLength="50" ID="tbSponsorPhoneOffice" />
							<aspire:AspireStringValidator ID="svtbSponsorPhoneOffice" runat="server" ValidationGroup="Sponsor" ControlToValidate="tbSponsorPhoneOffice" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Phone No. (Home):" AssociatedControlID="tbSponsorPhoneHome" />
							<aspire:AspireTextBox ValidationGroup="Sponsor" runat="server" MaxLength="50" ID="tbSponsorPhoneHome" />
							<aspire:AspireStringValidator ID="svtbSponsorPhoneHome" runat="server" ValidationGroup="Sponsor" ControlToValidate="tbSponsorPhoneHome" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Fax:" AssociatedControlID="tbSponsorFax" />
							<aspire:AspireTextBox ValidationGroup="Sponsor" runat="server" MaxLength="50" ID="tbSponsorFax" />
							<aspire:AspireStringValidator ID="svtbSponsorFax" runat="server" ValidationGroup="Sponsor" ControlToValidate="tbSponsorFax" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="Sponsor" Text="Save" OnClick="btnSave_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>
