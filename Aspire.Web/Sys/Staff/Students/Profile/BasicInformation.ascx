﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BasicInformation.ascx.cs" Inherits="Aspire.Web.Sys.Staff.Students.Profile.BasicInformation" %>

<aspire:AspireModal runat="server" ID="modal" ModalSize="Large" HeaderText="Basic Information">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanelMain">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alertModal" />
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
							<aspire:AspireTextBox runat="server" ID="tbEnrollment" ReadOnly="True" ValidationGroup="Basic" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Registration No:" AssociatedControlID="tbRegistrationNo" />
							<aspire:AspireTextBox runat="server" ID="tbRegistrationNo" TextMode="SingleLine" ValidationGroup="Basic" />
							<aspire:AspireInt32Validator runat="server" MinValue="1" ControlToValidate="tbRegistrationNo" ValidationGroup="Basic" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Registration No." AllowNull="True" />
							<aspire:Label ID="lblBURegistrationNo" runat="server" CssClass="help-block" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Name (as per SSC):" AssociatedControlID="tbName" />
							<aspire:AspireTextBox ValidationGroup="Basic" TextTransform="UpperCase" runat="server" MaxLength="100" ID="tbName" />
							<aspire:AspireStringValidator ID="svtbName" runat="server" AllowNull="False" ControlToValidate="tbName" RequiredErrorMessage="This field is required." ValidationGroup="Basic" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Student Status:" AssociatedControlID="ddlStudentStatus" />
							<aspire:AspireDropDownList runat="server" ID="ddlStudentStatus" AutoPostBack="True" CausesValidation="False" ValidationGroup="Basic" OnSelectedIndexChanged="ddlStudentStatus_OnSelectedIndexChanged" />
						</div>
					</div>
				</div>
				<div class="row" id="divBlockedReason" runat="server">
					<div class="col-md-12">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Reason for Blocking:" AssociatedControlID="tbBlockedReason" />
							<aspire:AspireTextBox TextMode="MultiLine" MaxLength="1000" ValidationGroup="Basic" runat="server" ID="tbBlockedReason" />
							<aspire:AspireStringValidator AllowNull="False" ID="svtbBlockedReason" runat="server" ControlToValidate="tbBlockedReason" ErrorMessage="This field is required." ValidationGroup="Basic" />
						</div>
					</div>
				</div>
				<div class="row" id="divDeferredTo" runat="server">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Deferred To Intake Semester:" AssociatedControlID="ddlDeferredToIntakeSemesterID" />
							<aspire:AspireDropDownList ValidationGroup="Basic" runat="server" ID="ddlDeferredToIntakeSemesterID" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlDeferredToIntakeSemesterID_OnSelectedIndexChanged" />
							<aspire:AspireRequiredFieldValidator ID="rfvddlDeferredToIntakeSemesterID" runat="server" ControlToValidate="ddlDeferredToIntakeSemesterID" ErrorMessage="This field is required." ValidationGroup="Basic" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Deferred To Program:" AssociatedControlID="ddlDeferredToAdmissionOpenProgramID" />
							<aspire:AspireDropDownList runat="server" ID="ddlDeferredToAdmissionOpenProgramID" ValidationGroup="Basic" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="ddlDeferredToAdmissionOpenProgramID_OnSelectedIndexChanged" />
							<aspire:AspireRequiredFieldValidator ID="rfvddlDeferredToAdmissionOpenProgramID" runat="server" ControlToValidate="ddlDeferredToAdmissionOpenProgramID" ErrorMessage="This field is required." ValidationGroup="Basic" />
						</div>
					</div>
				</div>
				<div class="row" id="divDeferredToShift" runat="server">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Deferred To Shift:" AssociatedControlID="ddlDeferredToShift" />
							<aspire:AspireDropDownList ValidationGroup="Basic" runat="server" ID="ddlDeferredToShift" />
							<aspire:AspireRequiredFieldValidator ID="rfvddlDeferredToShift" runat="server" ControlToValidate="ddlDeferredToShift" ErrorMessage="This field is required." ValidationGroup="Basic" />
						</div>
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="Basic" Text="Save" OnClick="btnSave_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>
