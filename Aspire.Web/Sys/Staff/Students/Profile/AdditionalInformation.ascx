﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdditionalInformation.ascx.cs" Inherits="Aspire.Web.Sys.Staff.Students.Profile.AdditionalInformation" %>

<aspire:AspireModal runat="server" ID="modal" ModalSize="Large" HeaderText="Additiaonal Information">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanelMain">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alertModal" />
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
							<aspire:AspireTextBox runat="server" ID="tbEnrollment" ReadOnly="True" ValidationGroup="Additional" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbName" />
							<aspire:AspireTextBox runat="server" ID="tbName" TextMode="SingleLine" ValidationGroup="Additional" ReadOnly="true" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlType" />
					<aspire:AspireDropDownList runat="server" ID="ddlType" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbSubject" ErrorMessage="This field is required." ValidationGroup="Additional" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Reference Date:" AssociatedControlID="dtpReferenceDate" />
					<aspire:AspireDateTimePickerTextbox ID="dtpReferenceDate" runat="server" DisplayMode="ShortDate" ValidationGroup="Additional" />
					<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dtpReferenceDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." InvalidRangeErrorMessage="Invalid Date." AllowNull="false" ValidationGroup="Additional" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Subject:" AssociatedControlID="tbSubject" />
					<aspire:AspireTextBox ValidationGroup="Additional" runat="server" MaxLength="1000" ID="tbSubject" />
					<aspire:AspireStringValidator runat="server" AllowNull="false" ControlToValidate="tbSubject" RequiredErrorMessage="This field is required." ValidationGroup="Additional" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Description:" AssociatedControlID="tbDescription" />
					<aspire:AspireTextBox ValidationGroup="Additional" runat="server" Rows="3" TextMode="MultiLine" MaxLength="1000" ID="tbDescription" />
					<aspire:AspireStringValidator runat="server" AllowNull="false" ControlToValidate="tbDescription" RequiredErrorMessage="This field is required." ValidationGroup="Additional" />
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="Additional" Text="Save" OnClick="btnSave_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>
