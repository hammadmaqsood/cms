﻿using System;

namespace Aspire.Web.Sys.Staff.Students.Profile
{
	public partial class EmergencyInformation : System.Web.UI.UserControl
	{
		private new Profile Page => (Profile)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Visible = false;
			}
		}

		public void ShowModal()
		{
			var student = BL.Core.StudentInformation.Staff.StudentProfile.GetStudent(this.Page.Enrollment, this.Page.StaffIdentity.LoginSessionGuid);
			if (student == null)
			{
				this.Page.AddNoRecordFoundAlert();
				Profile.Redirect(this.Page.Enrollment, false);
				return;
			}

			this.tbEnrollment.Text = student.Enrollment;
			this.tbName.Text = student.Name;
			this.tbEmergencyContactName.Text = student.EmergencyContactName;
			this.tbEmergencyMobile.Text = student.EmergencyMobile;
			this.tbEmergencyPhone.Text = student.EmergencyPhone;
			this.tbEmergencyRelationship.Text = student.EmergencyRelationship;
			this.tbEmergencyAddress.Text = student.EmergencyAddress;
			this.Visible = true;
			this.modal.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var studentID = this.Page.StudentID;
			var emergencyContactName = this.tbEmergencyContactName.Text;
			var emergencyMobile = this.tbEmergencyMobile.Text;
			var emergencyPhone = this.tbEmergencyPhone.Text;
			var emergencyRelationship = this.tbEmergencyRelationship.Text;
			var emergencyAddress = this.tbEmergencyAddress.Text;

			var result = BL.Core.StudentInformation.Staff.StudentProfile.UpdateEmergencyInformation(studentID, emergencyContactName, emergencyMobile, emergencyPhone, emergencyRelationship, emergencyAddress, this.Page.StaffIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.NoRecordFound:
					this.Page.Refresh();
					break;
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.Success:
					this.Page.AddSuccessMessageHasBeenUpdated("Emergency Information");
					this.Page.Refresh();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}