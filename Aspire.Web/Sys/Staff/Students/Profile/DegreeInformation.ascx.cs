﻿using Aspire.Lib.Extensions;
using System;

namespace Aspire.Web.Sys.Staff.Students.Profile
{
	public partial class DegreeInformation : System.Web.UI.UserControl
	{
		private new Profile Page => (Profile)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Visible = false;
			}
		}

		public void ShowModal()
		{
			var student = BL.Core.StudentInformation.Staff.StudentProfile.GetStudent(this.Page.Enrollment, this.Page.StaffIdentity.LoginSessionGuid);
			if (student == null)
			{
				this.Page.AddNoRecordFoundAlert();
				Profile.Redirect(this.Page.Enrollment, false);
				return;
			}

			this.tbEnrollment.Text = student.Enrollment;
			this.tbName.Text = student.Name;
			this.tbFinalTranscriptNo.Text = student.FinalTranscriptNo.ToString();
			this.dtpFinalTranscriptIssueDate.SelectedDate = student.FinalTranscriptIssueDate;
			this.tbFinalCGPA.Text = student.FinalCGPA.ToString();
			this.tbDegreeNo.Text = student.DegreeNo;
			this.tbDegreeGazetteNo.Text = student.DegreeGazetteNo.ToString();
			this.dtpDegreeIssueDate.SelectedDate = student.DegreeIssueDate;
			this.dtpDegreeIssuedToStudent.SelectedDate = student.DegreeIssuedToStudent;
			this.Visible = true;
			this.modal.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var studentID = this.Page.StudentID;
			var finalTranscriptNo = this.tbFinalTranscriptNo.Text.ToNullableInt();
			var finalTranscriptIssueDate = this.dtpFinalTranscriptIssueDate.SelectedDate;
			var finalCGPA = this.tbFinalCGPA.Text.ToNullableDecimal();
			var degreeNo = this.tbDegreeNo.Text.TrimAndMakeItNullIfEmpty();
			var degreeGazetteNo = this.tbDegreeGazetteNo.Text.ToNullableInt();
			var degreeIssueDate = this.dtpDegreeIssueDate.SelectedDate;
			var degreeIssuedToStudent = this.dtpDegreeIssuedToStudent.SelectedDate;

			var result = BL.Core.StudentInformation.Staff.StudentProfile.UpdateDegreeInformation(studentID, finalTranscriptNo, finalTranscriptIssueDate, finalCGPA, degreeNo, degreeGazetteNo, degreeIssueDate, degreeIssuedToStudent, this.Page.StaffIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.NoRecordFound:
					this.Page.Refresh();
					break;
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.Success:
					this.Page.AddSuccessMessageHasBeenUpdated("Current Academic Information");
					this.Page.Refresh();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}