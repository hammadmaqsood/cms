﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmergencyInformation.ascx.cs" Inherits="Aspire.Web.Sys.Staff.Students.Profile.EmergencyInformation" %>

<aspire:AspireModal runat="server" ID="modal" ModalSize="Large" HeaderText="Emergency Information">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanelMain">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alertModal" />
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
							<aspire:AspireTextBox runat="server" ID="tbEnrollment" ReadOnly="True" ValidationGroup="Emergency" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbName" />
							<aspire:AspireTextBox runat="server" ID="tbName" TextMode="SingleLine" ValidationGroup="Emergency" ReadOnly="true" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Contact Name:" AssociatedControlID="tbEmergencyContactName" />
							<aspire:AspireTextBox ValidationGroup="Emergency" TextTransform="UpperCase" runat="server" MaxLength="200" ID="tbEmergencyContactName" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Mobile:" AssociatedControlID="tbEmergencyMobile" />
							<aspire:AspireTextBox ValidationGroup="Emergency" data-inputmask="'alias': 'mobile'" runat="server" ID="tbEmergencyMobile" MaxLength="200" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Phone:" AssociatedControlID="tbEmergencyPhone" />
							<aspire:AspireTextBox ValidationGroup="Emergency" runat="server" MaxLength="200" ID="tbEmergencyPhone" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Relationship:" AssociatedControlID="tbEmergencyRelationship" />
							<aspire:AspireTextBox ValidationGroup="Emergency" runat="server" MaxLength="200" ID="tbEmergencyRelationship" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Address:" AssociatedControlID="tbEmergencyAddress" />
							<aspire:AspireTextBox runat="server" ID="tbEmergencyAddress" ValidationGroup="Emergency" MaxLength="500" TextMode="MultiLine" />
						</div>
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="Emergency" Text="Save" OnClick="btnSave_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>
