﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Sys.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Students.Profile
{
	[AspirePage("349C8483-7F34-459F-B5F9-A7C88FFE9301", UserTypes.Staff, "~/Sys/Staff/Students/Profile/Profile.aspx", "Student Profile", true, AspireModules.StudentInformation)]
	public partial class Profile : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.StudentInformation.Module,new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};

		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_user);

		public override string PageTitle => "Profile Information";

		public static string GetPageUrl(string enrollment, bool search)
		{
			return GetAspirePageAttribute<Profile>().PageUrl.AttachQueryParams("Enrollment", enrollment, "Search", search);
		}

		public new static void Redirect(string enrollment, bool search)
		{
			Redirect(GetPageUrl(enrollment, search));
		}

		public string Enrollment
		{
			get => (string)this.ViewState[nameof(this.Enrollment)];
			private set => this.ViewState[nameof(this.Enrollment)] = value;
		}

		public int StudentID
		{
			get => (int)this.ViewState[nameof(this.StudentID)];
			private set => this.ViewState[nameof(this.StudentID)] = value;
		}

		public void Refresh()
		{
			Redirect(this.Enrollment, true);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.panelStudent.Visible = false;
				var enrollment = this.Request.GetParameterValue("Enrollment");
				this.tbEnrollment.Enrollment = enrollment;
				if (this.Request.GetParameterValue<bool>("Search") == true)
					this.tbEnrollment_Search(null, null);
			}
		}

		protected void tbEnrollment_Search(object sender, EventArgs args)
		{
			var enrollment = this.tbEnrollment.Enrollment;
			var student = BL.Core.StudentInformation.Staff.StudentProfile.GetStudent(enrollment, this.AspireIdentity.LoginSessionGuid);
			if (student == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect(enrollment, false);
				return;
			}
			this.panelStudent.Visible = true;
			this.StudentID = student.StudentID;
			this.Enrollment = student.Enrollment;
			this.repeaterStudent.DataBind(new[] { student });
			var trDeferredTo = this.repeaterStudent.Items[0].FindControl("trDeferredTo");
			var lblDeferredTo = ((Label)this.repeaterStudent.Items[0].FindControl("lblDeferredTo"));
			var hlDeferredToEnrollment = ((AspireHyperLink)this.repeaterStudent.Items[0].FindControl("hlDeferredToEnrollment"));
			var trDeferredFrom = this.repeaterStudent.Items[0].FindControl("trDeferredFrom");
			var lblDeferredFrom = ((Label)this.repeaterStudent.Items[0].FindControl("lblDeferredFrom"));
			var hlDeferredFromEnrollment = ((AspireHyperLink)this.repeaterStudent.Items[0].FindControl("hlDeferredFromEnrollment"));

			if (student.DeferredToAdmissionOpenProgramID != null || student.DeferredToStudentID != null)
			{
				var deferredTo = BL.Core.StudentInformation.Staff.StudentProfile.GetStudentDeferredTo(student.StudentID, this.StaffIdentity.LoginSessionGuid);
				trDeferredTo.Visible = true;
				lblDeferredTo.Text = $"{deferredTo.DeferredToProgramAlias} of {deferredTo.DeferredToIntakeSemesterID.ToSemesterString()}";
				hlDeferredToEnrollment.Text = deferredTo.DeferredToEnrollment.ToNAIfNullOrEmpty();
				if (deferredTo.DeferredToEnrollment != null)
					hlDeferredToEnrollment.NavigateUrl = GetPageUrl(deferredTo.DeferredToEnrollment, true);
			}

			if (student.DeferredFromStudentID != null)
			{
				var deferredFrom = BL.Core.StudentInformation.Staff.StudentProfile.GetStudentDeferredFrom(student.StudentID, this.StaffIdentity.LoginSessionGuid);
				trDeferredFrom.Visible = true;
				lblDeferredFrom.Text = $"{deferredFrom.DeferredFromProgramAlias} of {deferredFrom.DeferredFromIntakeSemesterID.ToSemesterString()}";
				hlDeferredFromEnrollment.Text = deferredFrom.DeferredFromEnrollment.ToNAIfNullOrEmpty();
				if (deferredFrom.DeferredFromEnrollment != null)
					hlDeferredFromEnrollment.NavigateUrl = GetPageUrl(deferredFrom.DeferredFromEnrollment, true);
			}
		}

		protected void btnUpload_OnClick(object sender, EventArgs e)
		{
			this.photoUploadUserControl.ShowModal();
		}

		protected void photoUploadUserControl_OnSave(object sender, PhotoUploadUserControl.SaveEventArgs e)
		{
			var status = BL.Core.StudentInformation.Staff.StudentProfile.UpdatePhoto(this.StudentID, e.Bytes, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdatePhotoStatuses.NoRecordFound:
					this.Refresh();
					return;
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdatePhotoStatuses.InvalidImage:
					e.Status = PhotoUploadUserControl.SaveEventArgs.Statuses.InvalidImage;
					return;
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdatePhotoStatuses.Success:
					this.AddSuccessAlert("Photo has been updated.");
					this.Refresh();
					return;
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdatePhotoStatuses.ResolutionMustBe400X400:
					this.AddSuccessAlert("Image Resolution of cropped photo must be 400 x 400 pixels.");
					this.Refresh();
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		protected void lbtnEditBasic_OnClick(object sender, EventArgs e)
		{
			this.basicInfo.ShowModal();
		}

		protected void lbtnEditPersonal_OnClick(object sender, EventArgs e)
		{
			this.personalInfo.ShowModal();
		}

		protected void lbtnEmergency_OnClick(object sender, EventArgs e)
		{
			this.emergencyInfo.ShowModal();
		}

		protected void lbtnEditFather_OnClick(object sender, EventArgs e)
		{
			this.fatherInfo.ShowModal();
		}

		protected void lbtnEditSponsor_OnClick(object sender, EventArgs e)
		{
			this.sponsorInfo.ShowModal();
		}

		protected void lbtnEditAcademic_OnClick(object sender, EventArgs e)
		{
			this.academicInfo.ShowModal(null);
		}

		protected void lbtnEditAdditional_OnClick(object sender, EventArgs e)
		{
			this.additionalInfo.ShowModal(null);
		}

		protected void repeaterAdditionalInfo_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					this.additionalInfo.ShowModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					var studentCustomInfoID = e.DecryptedCommandArgumentToInt();
					var result = BL.Core.StudentInformation.Staff.StudentProfile.DeleteStudentCustomInfo(this.StudentID, studentCustomInfoID, this.StaffIdentity.LoginSessionGuid);
					switch (result)
					{
						case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.NoRecordFound:
							this.Refresh();
							break;
						case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.Success:
							this.AddSuccessMessageHasBeenDeleted("Additional Information");
							this.Refresh();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		protected void repeaterAcademicRecord_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					this.academicInfo.ShowModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					var studentAcademicRecordID = e.DecryptedCommandArgumentToInt();
					var result = BL.Core.StudentInformation.Staff.StudentProfile.DeleteStudentAcademicRecord(this.StudentID, studentAcademicRecordID, this.StaffIdentity.LoginSessionGuid);
					switch (result)
					{
						case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.NoRecordFound:
							this.Refresh();
							break;
						case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.Success:
							this.AddSuccessMessageHasBeenDeleted("Previous Academic Record");
							this.Refresh();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		protected void lbtnEditDegree_Click(object sender, EventArgs e)
		{
			this.DegreeInfo.ShowModal();
		}

		protected void btnHECDegreeVerification_OnClick(object sender, EventArgs e)
		{
			this.academicInfo.ShowModalHECDegreeVerification();
		}
	}
}