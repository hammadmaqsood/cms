﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Students.Profile
{
	public partial class AcademicInformation : System.Web.UI.UserControl
	{
		private new Profile Page => (Profile)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Visible = false;
			}
		}

		private int? StudentAcademicRecordID
		{
			get => (int?)this.ViewState[nameof(this.StudentAcademicRecordID)];
			set => this.ViewState[nameof(this.StudentAcademicRecordID)] = value;
		}

		public void ShowModal(int? studentAcademicRecordID)
		{
			this.StudentAcademicRecordID = null;
			var student = BL.Core.StudentInformation.Staff.StudentProfile.GetStudent(this.Page.Enrollment, this.Page.StaffIdentity.LoginSessionGuid);
			if (student == null)
			{
				this.Page.AddNoRecordFoundAlert();
				Profile.Redirect(this.Page.Enrollment, false);
				return;
			}

			this.tbEnrollment.Text = student.Enrollment;
			this.tbName.Text = student.Name;
			this.ddlDegreeType.FillDegreeTypes(CommonListItems.Select);
			this.ddlStatus.FillStudentAcademicRecordStatuses(CommonListItems.Select);
			this.ddlType.DataBind(new[] { new ListItem("Semester System", "1"), new ListItem("Annual System", "0") });
			this.ivtbPassingYear.MinValue = 1950;
			this.ivtbPassingYear.MaxValue = (ushort)DateTime.Now.Year;

			if (studentAcademicRecordID == null)
			{
				this.ddlDegreeType.ClearSelection();
				this.ddlType.ClearSelection();
				this.ddlType_SelectedIndexChanged(null, null);
				this.tbInstitute.Text = null;
				this.tbBoardUniversity.Text = null;
				this.tbSubjects.Text = null;
				this.tbTotalMarks.Text = null;
				this.tbObtainedMarks.Text = null;
				this.tbPassingYear.Text = null;
				this.tbRemarks.Text = null;
				this.ddlStatus.ClearSelection();
				this.modal.HeaderText = "Add Academic Information";
			}
			else
			{
				var studentAcademicRecord = student.StudentAcademicRecords.SingleOrDefault(ci => ci.StudentAcademicRecordID == studentAcademicRecordID);
				if (studentAcademicRecord == null)
				{
					this.Page.AddNoRecordFoundAlert();
					this.Page.Refresh();
					return;
				}

				this.ddlDegreeType.SetEnumValue(studentAcademicRecord.DegreeTypeEnum);
				this.ddlType.SetBooleanValue(studentAcademicRecord.IsCGPA);
				this.ddlType_SelectedIndexChanged(null, null);
				this.tbInstitute.Text = studentAcademicRecord.Institute;
				this.tbBoardUniversity.Text = studentAcademicRecord.BoardUniversity;
				this.tbSubjects.Text = studentAcademicRecord.Subjects;
				this.tbTotalMarks.Text = studentAcademicRecord.TotalMarks.ToString();
				this.tbObtainedMarks.Text = studentAcademicRecord.ObtainedMarks.ToString();
				this.tbPassingYear.Text = studentAcademicRecord.PassingYear.ToString();
				this.tbRemarks.Text = studentAcademicRecord.Remarks;
				this.ddlStatus.SetEnumValue(studentAcademicRecord.StatusEnum);
				this.modal.HeaderText = "Edit Academic Information";
				this.StudentAcademicRecordID = studentAcademicRecord.StudentAcademicRecordID;
			}
			this.Visible = true;
			this.modal.Show();
		}

		protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
		{
			var isCGPA = this.ddlType.SelectedValue.ToBoolean();
			if (isCGPA)
			{
				this.dvtbTotalMarks.MinValue = this.dvtbObtainedMarks.MinValue = 0;
				this.dvtbTotalMarks.MaxValue = this.dvtbObtainedMarks.MaxValue = 5;
			}
			else
			{
				this.dvtbTotalMarks.MinValue = this.dvtbObtainedMarks.MinValue = 0;
				this.dvtbTotalMarks.MaxValue = this.dvtbObtainedMarks.MaxValue = 15000;
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var studentID = this.Page.StudentID;
			var degreeTypeEnum = this.ddlDegreeType.GetSelectedEnumValue<DegreeTypes>();
			var isCGPA = this.ddlType.SelectedValue.ToBoolean();
			var institute = this.tbInstitute.Text.TrimAndCannotBeEmpty();
			var boardUniversity = this.tbBoardUniversity.Text.TrimAndCannotBeEmpty();
			var subjects = this.tbSubjects.Text.TrimAndCannotBeEmpty();
			var totalMarks = this.tbTotalMarks.Text.ToDouble();
			var obtainedMarks = this.tbObtainedMarks.Text.ToDouble();
			var passingYear = this.tbPassingYear.Text.ToShort();
			var remarks = this.tbRemarks.Text.TrimAndMakeItNullIfEmpty();
			var statusEnum = this.ddlStatus.GetSelectedEnumValue<Model.Entities.StudentAcademicRecord.Statuses>();

			if (obtainedMarks > totalMarks)
			{
				this.alertModal.AddErrorAlert("Obtained Marks must be less than or equal to Total Marks.");
				this.updatePanelMain.Update();
				return;
			}

			var result = BL.Core.StudentInformation.Staff.StudentProfile.AddOrUpdateStudentAcademicRecord(studentID, this.StudentAcademicRecordID, degreeTypeEnum, isCGPA, institute, boardUniversity, subjects, totalMarks, obtainedMarks, passingYear, remarks, statusEnum, this.Page.StaffIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.NoRecordFound:
					this.Page.Refresh();
					break;
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.Success:
					if (this.StudentAcademicRecordID == null)
						this.Page.AddSuccessMessageHasBeenAdded("Academic Information");
					else
						this.Page.AddSuccessMessageHasBeenUpdated("Academic Information");
					this.Page.Refresh();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void btnSaveHECDegreeVerification_OnClick(object sender, EventArgs e)
		{
			Dictionary<int, DateTime?> hecDegreeVerificationStampDates = new Dictionary<int, DateTime?>();
			foreach (RepeaterItem item in this.repeaterHECDegreeVerificationAcademicRecords.Items)
			{
				if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
				{
					var dtpHECDegreeVerificationStampDate = (AspireDateTimePickerTextbox)item.FindControl("dtpHECDegreeVerificationStampDate");
					var hfStudentAcademicRecordID = (Lib.WebControls.HiddenField)item.FindControl("hfStudentAcademicRecordID");
					hecDegreeVerificationStampDates.Add(hfStudentAcademicRecordID.Value.ToInt(), dtpHECDegreeVerificationStampDate.SelectedDate);
				}
			}

			if (!hecDegreeVerificationStampDates.Any())
				return;

			var result = BL.Core.StudentInformation.Staff.StudentProfile.UpdateHECDegreeVerificationStampDate(this.Page.Enrollment, hecDegreeVerificationStampDates, AspireIdentity.Current.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateHECDegreeVerificationStampDateStatuses.NoRecordFound:
					this.Page.AddNoRecordFoundAlert();
					this.Page.Refresh();
					break;
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateHECDegreeVerificationStampDateStatuses.Success:
					this.Page.AddSuccessMessageHasBeenUpdated("HEC Degree Verification Stamp Date");
					this.Page.Refresh();
					break;
				default:
					throw new NotImplementedEnumException(result);
			}
		}

		public void ShowModalHECDegreeVerification()
		{
			var student = BL.Core.StudentInformation.Staff.StudentProfile.GetStudent(this.Page.Enrollment, this.Page.StaffIdentity.LoginSessionGuid);
			if (student == null)
			{
				this.Page.AddNoRecordFoundAlert();
				Profile.Redirect(this.Page.Enrollment, false);
				return;
			}

			this.repeaterHECDegreeVerificationAcademicRecords.DataBind(student.StudentAcademicRecords);
			this.Visible = true;
			this.modalHECDegreeVerification.Show();
		}
	}
}