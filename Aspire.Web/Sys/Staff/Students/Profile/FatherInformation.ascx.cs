﻿using Aspire.Lib.Extensions;
using System;

namespace Aspire.Web.Sys.Staff.Students.Profile
{
	public partial class FatherInformation : System.Web.UI.UserControl
	{
		private new Profile Page => (Profile)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Visible = false;
			}
		}

		public void ShowModal()
		{
			var student = BL.Core.StudentInformation.Staff.StudentProfile.GetStudent(this.Page.Enrollment, this.Page.StaffIdentity.LoginSessionGuid);
			if (student == null)
			{
				this.Page.AddNoRecordFoundAlert();
				Profile.Redirect(this.Page.Enrollment, false);
				return;
			}

			this.tbEnrollment.Text = student.Enrollment;
			this.tbName.Text = student.Name;
			this.tbFatherName.Text = student.FatherName;
			this.tbFatherCNIC.Text = student.FatherCNIC;
			this.tbFatherPassport.Text = student.FatherPassportNo;
			this.tbFatherDesignation.Text = student.FatherDesignation;
			this.tbFatherDepartment.Text = student.FatherDepartment;
			this.tbFatherOrganization.Text = student.FatherOrganization;
			this.tbFatherServiceNo.Text = student.FatherServiceNo;
			this.tbFatherMobile.Text = student.FatherMobile;
			this.tbFatherOfficePhone.Text = student.FatherOfficePhone;
			this.tbFatherHomePhone.Text = student.FatherHomePhone;
			this.tbFatherFax.Text = student.FatherFax;
			this.Visible = true;
			this.modal.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var studentID = this.Page.StudentID;

			var fatherName = this.tbFatherName.Text.TrimAndCannotBeEmpty();
			var fatherCNIC = this.tbFatherCNIC.Text.ToNullIfWhiteSpace();
			var fatherPassportNo = this.tbFatherPassport.Text.ToNullIfWhiteSpace();
			var fatherDesignation = this.tbFatherDesignation.Text.ToNullIfWhiteSpace();
			var fatherDepartment = this.tbFatherDepartment.Text.ToNullIfWhiteSpace();
			var fatherOrganization = this.tbFatherOrganization.Text.ToNullIfWhiteSpace();
			var fatherServiceNo = this.tbFatherServiceNo.Text.ToNullIfWhiteSpace();
			var fatherMobile = this.tbFatherMobile.Text.ToNullIfWhiteSpace();
			var fatherOfficePhone = this.tbFatherOfficePhone.Text.ToNullIfWhiteSpace();
			var fatherHomePhone = this.tbFatherHomePhone.Text.ToNullIfWhiteSpace();
			var fatherFax = this.tbFatherFax.Text.ToNullIfWhiteSpace();

			var result = BL.Core.StudentInformation.Staff.StudentProfile.UpdateFatherInformation(studentID, fatherName, fatherCNIC, fatherPassportNo, fatherDesignation, fatherDepartment, fatherOrganization, fatherServiceNo, fatherMobile, fatherOfficePhone, fatherHomePhone, fatherFax, this.Page.StaffIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.NoRecordFound:
					this.Page.Refresh();
					break;
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.Success:
					this.Page.AddSuccessMessageHasBeenUpdated("Father Information");
					this.Page.Refresh();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}