﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AcademicInformation.ascx.cs" Inherits="Aspire.Web.Sys.Staff.Students.Profile.AcademicInformation" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<aspire:AspireModal runat="server" ID="modal" ModalSize="Large" HeaderText="Previous Academic Information">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanelMain">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alertModal" />
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
							<aspire:AspireTextBox runat="server" ID="tbEnrollment" ReadOnly="True" ValidationGroup="Additional" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbName" />
							<aspire:AspireTextBox runat="server" ID="tbName" TextMode="SingleLine" ValidationGroup="Additional" ReadOnly="true" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Degree Type:" AssociatedControlID="ddlDegreeType" />
							<aspire:AspireDropDownList runat="server" ID="ddlDegreeType" ValidationGroup="Academic" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlDegreeType" ErrorMessage="This field is required." ValidationGroup="Academic" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Examination System:" AssociatedControlID="ddlType" />
							<aspire:AspireDropDownList runat="server" ID="ddlType" CausesValidation="false" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlType" ErrorMessage="This field is required." ValidationGroup="Academic" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="tbInstitute" />
							<aspire:AspireTextBox runat="server" ID="tbInstitute" MaxLength="500" ValidationGroup="Academic" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbInstitute" ErrorMessage="This field is required." ValidationGroup="Academic" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Board/University:" AssociatedControlID="tbBoardUniversity" />
							<aspire:AspireTextBox runat="server" ID="tbBoardUniversity" MaxLength="500" ValidationGroup="Academic" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbBoardUniversity" ErrorMessage="This field is required." ValidationGroup="Academic" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Subjects:" AssociatedControlID="tbSubjects" />
							<aspire:AspireTextBox runat="server" ID="tbSubjects" MaxLength="500" ValidationGroup="Academic" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbSubjects" ErrorMessage="This field is required." ValidationGroup="Academic" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Total Marks:" AssociatedControlID="tbTotalMarks" />
							<aspire:AspireTextBox runat="server" ID="tbTotalMarks" ValidationGroup="Academic" />
							<aspire:AspireDoubleValidator ID="dvtbTotalMarks" runat="server" ControlToValidate="tbTotalMarks" RequiredErrorMessage="This field is required." AllowNull="False" InvalidNumberErrorMessage="Invalid number." InvalidRangeErrorMessage="Invalid Range." ValidationGroup="Academic" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Obtained Marks:" AssociatedControlID="tbObtainedMarks" />
							<aspire:AspireTextBox runat="server" ID="tbObtainedMarks" ValidationGroup="Academic" />
							<aspire:AspireDoubleValidator ID="dvtbObtainedMarks" runat="server" ControlToValidate="tbObtainedMarks" RequiredErrorMessage="This field is required." AllowNull="False" InvalidNumberErrorMessage="Invalid number." ValidationGroup="Academic" />
							<aspire:AspireCompareValidator runat="server" ControlToCompare="tbTotalMarks" ControlToValidate="tbObtainedMarks" ErrorMessage="Obtained marks must be less than or equal to total marks." ValidationGroup="Academic" Operator="LessThanEqual" Type="Double" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Passing Year:" AssociatedControlID="tbPassingYear" />
							<aspire:AspireTextBox runat="server" ID="tbPassingYear" ValidationGroup="Academic" />
							<aspire:AspireUInt16Validator ID="ivtbPassingYear" runat="server" ControlToValidate="tbPassingYear" AllowNull="false" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid number." InvalidRangeErrorMessage="Invalid passing year." ValidationGroup="Academic" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatus" />
							<aspire:AspireDropDownList runat="server" ID="ddlStatus" ValidationGroup="Academic" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlStatus" ErrorMessage="This field is required." ValidationGroup="Academic" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" />
							<aspire:AspireTextBox runat="server" ID="tbRemarks" MaxLength="1000" ValidationGroup="Academic" TextMode="MultiLine" Rows="3" />
						</div>
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="Academic" Text="Save" OnClick="btnSave_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>
<aspire:AspireModal runat="server" ID="modalHECDegreeVerification" ModalSize="Large" HeaderText="HEC Degree Verification">
	<BodyTemplate>
		<aspire:AspireAlert runat="server" ID="alertHECDegreeVerification" />
		<asp:Repeater ID="repeaterHECDegreeVerificationAcademicRecords" runat="server" ItemType="Aspire.Model.Entities.StudentAcademicRecord">
			<HeaderTemplate>
				<table class="table table-bordered table-condensed table-hover table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Degree</th>
							<th>Institute</th>
							<th>Board/University</th>
							<th>Marks</th>
							<th>Percentage</th>
							<th>Passing Year</th>
							<th>HEC Degree Verification Stamp Date</th>
						</tr>
					</thead>
					<tbody>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><%#: Container.ItemIndex + 1 %></td>
					<td><%#: Item.DegreeTypeEnum.ToFullName() %></td>
					<td><%#: Item.Institute.ToNAIfNullOrEmpty() %></td>
					<td><%#: Item.BoardUniversity.ToNAIfNullOrEmpty() %></td>
					<td><%#: $"{Item.ObtainedMarks:#.##}/{Item.TotalMarks:#.##}" %></td>
					<td><%#: $"{Item.Percentage:#.##}%" %></td>
					<td><%#: Item.PassingYear.ToNAIfNullOrEmpty() %></td>
					<td>
						<aspire:AspireDateTimePickerTextbox DisplayMode="ShortDate" runat="server" ID="dtpHECDegreeVerificationStampDate" Text='<%#: Item.HECDegreeVerificationStampDate %>' ValidationGroup="HECDegreeVerification" />
						<aspire:AspireDateTimeValidator Format="ShortDate" runat="server" ControlToValidate="dtpHECDegreeVerificationStampDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." InvalidRangeErrorMessage="Invalid range." ValidationGroup="HECDegreeVerification" />
						<aspire:HiddenField runat="server" Visible="false" ID="hfStudentAcademicRecordID" Value='<%#: Item.StudentAcademicRecordID %>' />
					</td>
				</tr>
			</ItemTemplate>
			<FooterTemplate>
				<tr runat="server" visible="<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>">
					<td colspan="10">No record found.</td>
				</tr>
				</tbody></table>
			</FooterTemplate>
		</asp:Repeater>
	</BodyTemplate>
	<FooterTemplate>
		<aspire:AspireButton runat="server" ID="btnSaveHECDegreeVerification" Text="Save" OnClick="btnSaveHECDegreeVerification_OnClick" />
		<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
	</FooterTemplate>
</aspire:AspireModal>
