﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Staff.Students.Profile
{
	public partial class BasicInformation : System.Web.UI.UserControl
	{
		private new Profile Page => (Profile)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Visible = false;
			}
		}

		public void ShowModal()
		{
			var student = BL.Core.StudentInformation.Staff.StudentProfile.GetStudent(this.Page.Enrollment, this.Page.StaffIdentity.LoginSessionGuid);
			if (student == null)
			{
				this.Page.AddNoRecordFoundAlert();
				Profile.Redirect(this.Page.Enrollment, false);
				return;
			}

			this.tbEnrollment.Text = student.Enrollment;
			this.tbRegistrationNo.Text = student.RegistrationNo.ToString();
			this.tbName.Text = student.Name;
			this.ddlStudentStatus.FillStudentStatuses(CommonListItems.Empty).SetEnumValue(student.StatusEnum);
			this.ddlStudentStatus.Enabled = student.DeferredToStudentID == null;
			this.ddlStudentStatus_OnSelectedIndexChanged(null, null);
			if (student.DeferredToStudentID != null || student.DeferredToAdmissionOpenProgramID != null)
			{
				var deferredTo = BL.Core.StudentInformation.Staff.StudentProfile.GetStudentDeferredTo(student.StudentID, this.Page.StaffIdentity.LoginSessionGuid);
				this.ddlDeferredToIntakeSemesterID.SelectedValue = deferredTo.DeferredToIntakeSemesterID.ToString();
				this.ddlDeferredToIntakeSemesterID_OnSelectedIndexChanged(null, null);
				this.ddlDeferredToAdmissionOpenProgramID.SelectedValue = deferredTo.DeferredToAdmissionOpenProgramID.ToString();
				this.ddlDeferredToAdmissionOpenProgramID_OnSelectedIndexChanged(null, null);
				this.ddlDeferredToShift.SetEnumValue(deferredTo.DeferredToShiftEnum);
			}

			this.tbBlockedReason.Text = student.BlockedReason;
			this.Visible = true;
			this.modal.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.Page.IsValid)
				return;
			var registrationNo = this.tbRegistrationNo.Text.ToNullableInt();
			var name = this.tbName.Text.TrimAndCannotEmptyAndMustBeUpperCase();
			var statusEnum = this.ddlStudentStatus.GetSelectedEnumValue<Model.Entities.Student.Statuses>(null);
			int? deferredToAdmissionOpenProgramID = null;
			var blockedReason = this.tbBlockedReason.Text.TrimAndMakeItNullIfEmpty();
			Shifts? deferredToShiftEnum = null;
			if (statusEnum == Model.Entities.Student.Statuses.Deferred)
			{
				deferredToAdmissionOpenProgramID = this.ddlDeferredToAdmissionOpenProgramID.SelectedValue.ToNullableInt();
				deferredToShiftEnum = this.ddlDeferredToShift.GetSelectedEnumValue<Shifts>(null);
			}

			var result = BL.Core.StudentInformation.Staff.StudentProfile.UpdateBasicInformation(this.Page.StudentID, registrationNo, name, statusEnum, blockedReason, deferredToAdmissionOpenProgramID, deferredToShiftEnum, this.Page.StaffIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.NoRecordFound:
					this.Page.Refresh();
					break;
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.Success:
					this.Page.AddSuccessMessageHasBeenUpdated("Basic information");
					this.Page.Refresh();
					break;
				default:
					throw new NotImplementedEnumException(result);
			}
		}

		protected void ddlStudentStatus_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var status = this.ddlStudentStatus.GetSelectedEnumValue<Model.Entities.Student.Statuses>(null);
			switch (status)
			{
				case null:
				case Model.Entities.Student.Statuses.Dropped:
				case Model.Entities.Student.Statuses.Expelled:
				case Model.Entities.Student.Statuses.Graduated:
				case Model.Entities.Student.Statuses.Left:
				case Model.Entities.Student.Statuses.ProgramChanged:
				case Model.Entities.Student.Statuses.Rusticated:
				case Model.Entities.Student.Statuses.TransferredToOtherCampus:
				case Model.Entities.Student.Statuses.AdmissionCancelled:
				case Model.Entities.Student.Statuses.Blocked:
					this.divDeferredTo.Visible = false;
					this.divDeferredToShift.Visible = false;
					this.ddlDeferredToAdmissionOpenProgramID.Items.Clear();
					this.ddlDeferredToAdmissionOpenProgramID.Items.Clear();
					this.ddlDeferredToShift.Items.Clear();
					this.rfvddlDeferredToIntakeSemesterID.Enabled = false;
					this.rfvddlDeferredToAdmissionOpenProgramID.Enabled = false;
					this.rfvddlDeferredToShift.Enabled = false;
					this.divBlockedReason.Visible = status == Model.Entities.Student.Statuses.Blocked;
					this.svtbBlockedReason.Enabled = status == Model.Entities.Student.Statuses.Blocked;
					if (status != Model.Entities.Student.Statuses.Blocked)
						this.tbBlockedReason.Text = null;
					break;
				case Model.Entities.Student.Statuses.Deferred:
					this.divDeferredTo.Visible = true;
					this.divDeferredToShift.Visible = true;
					this.ddlDeferredToIntakeSemesterID.Enabled = this.ddlDeferredToAdmissionOpenProgramID.Enabled = this.ddlDeferredToShift.Enabled = this.ddlStudentStatus.Enabled;
					this.ddlDeferredToIntakeSemesterID.FillSemesters(CommonListItems.Select);
					this.ddlDeferredToIntakeSemesterID_OnSelectedIndexChanged(null, null);
					this.divBlockedReason.Visible = false;
					this.svtbBlockedReason.Enabled = false;
					this.tbBlockedReason.Text = null;
					break;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		protected void ddlDeferredToIntakeSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var intakeSemesterID = this.ddlDeferredToIntakeSemesterID.SelectedValue.ToNullableShort();
			if (intakeSemesterID == null)
			{
				this.rfvddlDeferredToIntakeSemesterID.Enabled = false;
				this.rfvddlDeferredToAdmissionOpenProgramID.Enabled = false;
				this.rfvddlDeferredToShift.Enabled = false;
				this.ddlDeferredToAdmissionOpenProgramID.DataBind(CommonListItems.Select);
			}
			else
			{
				this.rfvddlDeferredToIntakeSemesterID.Enabled = true;
				this.rfvddlDeferredToAdmissionOpenProgramID.Enabled = true;
				this.rfvddlDeferredToShift.Enabled = true;
				var admissionOpenPrograms = BL.Core.Common.Lists.GetAdmissionOpenPrograms(intakeSemesterID.Value, this.Page.StaffIdentity.InstituteID);
				this.ddlDeferredToAdmissionOpenProgramID.DataBind(admissionOpenPrograms, CommonListItems.Select);
			}
			this.ddlDeferredToAdmissionOpenProgramID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDeferredToAdmissionOpenProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var admissionOpenProgramID = this.ddlDeferredToAdmissionOpenProgramID.SelectedValue.ToNullableInt();
			if (admissionOpenProgramID == null)
				this.ddlDeferredToShift.DataBind(CommonListItems.Select);
			else
				this.ddlDeferredToShift.FillShifts(CommonListItems.Select);
		}
	}
}