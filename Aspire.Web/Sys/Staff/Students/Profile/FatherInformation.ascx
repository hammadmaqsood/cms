﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FatherInformation.ascx.cs" Inherits="Aspire.Web.Sys.Staff.Students.Profile.FatherInformation" %>

<aspire:AspireModal runat="server" ID="modal" ModalSize="Large" HeaderText="Father Information">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanelMain">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alertModal" />
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
							<aspire:AspireTextBox runat="server" ID="tbEnrollment" ReadOnly="True" ValidationGroup="Father" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbName" />
							<aspire:AspireTextBox runat="server" ID="tbName" TextMode="SingleLine" ValidationGroup="Father" ReadOnly="true" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Name (As per SSC):" AssociatedControlID="tbFatherName" />
							<aspire:AspireTextBox TextTransform="UpperCase" ValidationGroup="Father" runat="server" MaxLength="100" ID="tbFatherName" />
							<aspire:AspireStringValidator ID="svtbFatherName" runat="server" AllowNull="False" ValidationGroup="Father" ControlToValidate="tbFatherName" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="CNIC No.:" AssociatedControlID="tbFatherCNIC" />
							<aspire:AspireTextBox ValidationGroup="Father" runat="server" MaxLength="15" data-inputmask="'alias': 'cnic'" ID="tbFatherCNIC" />
							<aspire:AspireStringValidator ID="svtbFatherCNIC" runat="server" AllowNull="True" ValidationGroup="Father" ControlToValidate="tbFatherCNIC" InvalidDataErrorMessage="Invalid CNIC." RequiredErrorMessage="This field is required." ValidationExpression="CNIC" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Passport No.:" AssociatedControlID="tbFatherPassport" />
							<aspire:AspireTextBox ValidationGroup="Father" runat="server" MaxLength="100" ID="tbFatherPassport" />
							<aspire:AspireStringValidator ID="svtbFatherPassport" runat="server" AllowNull="True" ValidationGroup="Father" ControlToValidate="tbFatherPassport" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Designation/Profession:" AssociatedControlID="tbFatherDesignation" />
							<aspire:AspireTextBox ValidationGroup="Father" runat="server" MaxLength="100" ID="tbFatherDesignation" />
							<aspire:AspireStringValidator ID="svtbFatherDesignation" runat="server" AllowNull="True" ValidationGroup="Father" ControlToValidate="tbFatherDesignation" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Department/Unit:" AssociatedControlID="tbFatherDepartment" />
							<aspire:AspireTextBox ValidationGroup="Father" runat="server" MaxLength="100" ID="tbFatherDepartment" />
							<aspire:AspireStringValidator ID="svtbFatherDepartment" runat="server" AllowNull="True" ValidationGroup="Father" ControlToValidate="tbFatherDepartment" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Organization:" AssociatedControlID="tbFatherOrganization" />
							<aspire:AspireTextBox ValidationGroup="Father" runat="server" MaxLength="100" ID="tbFatherOrganization" />
							<aspire:AspireStringValidator ID="svtbFatherOrganization" runat="server" AllowNull="True" ValidationGroup="Father" ControlToValidate="tbFatherOrganization" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Service No. (If Naval):" AssociatedControlID="tbFatherServiceNo" />
							<aspire:AspireTextBox ValidationGroup="Father" runat="server" MaxLength="50" ID="tbFatherServiceNo" />
							<aspire:AspireStringValidator ID="svtbFatherServiceNo" runat="server" AllowNull="True" ValidationGroup="Father" ControlToValidate="tbFatherServiceNo" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Mobile No:" AssociatedControlID="tbFatherMobile" />
							<aspire:AspireTextBox ValidationGroup="Father" runat="server" data-inputmask="'alias': 'mobile'" ID="tbFatherMobile" MaxLength="50" />
							<aspire:AspireStringValidator ID="svtbFatherMobile" runat="server" AllowNull="True" ValidationGroup="Father" ControlToValidate="tbFatherMobile" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Phone No. (Office):" AssociatedControlID="tbFatherOfficePhone" />
							<aspire:AspireTextBox ValidationGroup="Father" runat="server" MaxLength="50" ID="tbFatherOfficePhone" />
							<aspire:AspireStringValidator ID="svtbFatherOfficePhone" runat="server" AllowNull="True" ValidationGroup="Father" ControlToValidate="tbFatherOfficePhone" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Phone No. (Home):" AssociatedControlID="tbFatherHomePhone" />
							<aspire:AspireTextBox ValidationGroup="Father" runat="server" MaxLength="50" ID="tbFatherHomePhone" />
							<aspire:AspireStringValidator ID="svtbFatherHomePhone" runat="server" AllowNull="True" ValidationGroup="Father" ControlToValidate="tbFatherHomePhone" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Fax:" AssociatedControlID="tbFatherFax" />
							<aspire:AspireTextBox ValidationGroup="Father" runat="server" MaxLength="50" ID="tbFatherFax" />
							<aspire:AspireStringValidator ID="svtbFatherFax" runat="server" AllowNull="True" ValidationGroup="Father" ControlToValidate="tbFatherFax" RequiredErrorMessage="This field is required. " />
						</div>
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="Father" Text="Save" OnClick="btnSave_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>
