﻿using Aspire.Lib.Extensions;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Staff.Students.Profile
{
	public partial class SponsorInformation : System.Web.UI.UserControl
	{
		private new Profile Page => (Profile)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Visible = false;
			}
		}

		public void ShowModal()
		{
			var student = BL.Core.StudentInformation.Staff.StudentProfile.GetStudent(this.Page.Enrollment, this.Page.StaffIdentity.LoginSessionGuid);
			if (student == null)
			{
				this.Page.AddNoRecordFoundAlert();
				Profile.Redirect(this.Page.Enrollment, false);
				return;
			}

			this.tbEnrollment.Text = student.Enrollment;
			this.tbName.Text = student.Name;
			this.ddlSponsoredBy.FillSponsorships().SetEnumValue(student.SponsoredByEnum);
			this.ddlSponsoredBy_SelectedIndexChanged(null, null);
			this.tbSponsorName.Text = student.SponsorName;
			this.tbSponsorCNIC.Text = student.SponsorCNIC;
			this.tbSponsorPassportNo.Text = student.SponsorPassportNo;
			this.tbSponsorRelationshipWithGuardian.Text = student.SponsorRelationshipWithGuardian;
			this.tbSponsorDesignation.Text = student.SponsorDesignation;
			this.tbSponsorDepartment.Text = student.SponsorDepartment;
			this.tbSponsorOrganization.Text = student.SponsorOrganization;
			this.tbSponsorServiceNo.Text = student.SponsorServiceNo;
			this.tbSponsorMobile.Text = student.SponsorMobile;
			this.tbSponsorPhoneOffice.Text = student.SponsorPhoneOffice;
			this.tbSponsorPhoneHome.Text = student.SponsorPhoneHome;
			this.tbSponsorFax.Text = student.SponsorFax;
			this.Visible = true;
			this.modal.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var studentID = this.Page.StudentID;

			var sponsoredByEnum = this.ddlSponsoredBy.GetSelectedEnumValue<Aspire.Model.Entities.Common.Sponsorships>();
			var sponsorName = this.tbSponsorName.Text.ToNullIfWhiteSpace();
			var sponsorCNIC = this.tbSponsorCNIC.Text.ToNullIfWhiteSpace();
			var sponsorPassportNo = this.tbSponsorPassportNo.Text.ToNullIfWhiteSpace();
			var sponsorRelationshipWithGuardian = this.tbSponsorRelationshipWithGuardian.Text.ToNullIfWhiteSpace();
			var sponsorDesignation = this.tbSponsorDesignation.Text.ToNullIfWhiteSpace();
			var sponsorDepartment = this.tbSponsorDepartment.Text.ToNullIfWhiteSpace();
			var sponsorOrganization = this.tbSponsorOrganization.Text.ToNullIfWhiteSpace();
			var sponsorServiceNo = this.tbSponsorServiceNo.Text.ToNullIfWhiteSpace();
			var sponsorMobile = this.tbSponsorMobile.Text.ToNullIfWhiteSpace();
			var sponsorPhoneOffice = this.tbSponsorPhoneOffice.Text.ToNullIfWhiteSpace();
			var sponsorPhoneHome = this.tbSponsorPhoneHome.Text.ToNullIfWhiteSpace();
			var sponsorFax = this.tbSponsorFax.Text.ToNullIfWhiteSpace();

			var result = BL.Core.StudentInformation.Staff.StudentProfile.UpdateSponsorInformation(studentID, sponsoredByEnum, sponsorName, sponsorCNIC, sponsorPassportNo, sponsorRelationshipWithGuardian, sponsorDesignation, sponsorDepartment, sponsorOrganization, sponsorServiceNo, sponsorMobile, sponsorPhoneOffice, sponsorPhoneHome, sponsorFax, this.Page.StaffIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.NoRecordFound:
					this.Page.Refresh();
					break;
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.Success:
					this.Page.AddSuccessMessageHasBeenUpdated("Sponsor Information");
					this.Page.Refresh();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void ddlSponsoredBy_SelectedIndexChanged(object sender, EventArgs e)
		{
			//comment the allow null settings b/c from admission department this query comes to remove required field in our student profile edit.
			var sponsoredByEnum = this.ddlSponsoredBy.GetSelectedEnumValue<Model.Entities.Common.Sponsorships>();

			this.svtbSponsorName.AllowNull = true;
			this.svtbSponsorCNIC.AllowNull = true;
			this.svtbSponsorPassportNo.AllowNull = true;
			this.svtbSponsorRelationshipWithGuardian.AllowNull = true;
			this.svtbSponsorDesignation.AllowNull = true;
			this.svtbSponsorDepartment.AllowNull = true;
			this.svtbSponsorOrganization.AllowNull = true;
			this.svtbSponsorServiceNo.AllowNull = true;
			this.svtbSponsorMobile.AllowNull = true;
			this.svtbSponsorPhoneOffice.AllowNull = true;
			this.svtbSponsorPhoneHome.AllowNull = true;
			this.svtbSponsorFax.AllowNull = true;
			switch (sponsoredByEnum)
			{
				case Model.Entities.Common.Sponsorships.Father:
					this.tbSponsorName.Enabled = false;
					this.tbSponsorCNIC.Enabled = false;
					this.tbSponsorPassportNo.Enabled = false;
					this.tbSponsorRelationshipWithGuardian.Enabled = false;
					this.tbSponsorDesignation.Enabled = false;
					this.tbSponsorDepartment.Enabled = false;
					this.tbSponsorOrganization.Enabled = false;
					this.tbSponsorServiceNo.Enabled = false;
					this.tbSponsorMobile.Enabled = false;
					this.tbSponsorPhoneOffice.Enabled = false;
					this.tbSponsorPhoneHome.Enabled = false;
					this.tbSponsorFax.Enabled = false;
					break;
				case Model.Entities.Common.Sponsorships.Self:
					this.tbSponsorName.Enabled = false;
					this.tbSponsorCNIC.Enabled = false;
					this.tbSponsorPassportNo.Enabled = false;
					this.tbSponsorRelationshipWithGuardian.Enabled = false;
					this.tbSponsorDesignation.Enabled = true;
					this.tbSponsorDepartment.Enabled = true;
					this.tbSponsorOrganization.Enabled = true;
					this.tbSponsorServiceNo.Enabled = false;
					this.tbSponsorMobile.Enabled = false;
					this.tbSponsorPhoneOffice.Enabled = false;
					this.tbSponsorPhoneHome.Enabled = false;
					this.tbSponsorFax.Enabled = false;
					break;
				case Model.Entities.Common.Sponsorships.Guardian:
					this.tbSponsorName.Enabled = true;
					this.tbSponsorCNIC.Enabled = true;
					this.tbSponsorPassportNo.Enabled = true;
					this.tbSponsorRelationshipWithGuardian.Enabled = true;
					this.tbSponsorDesignation.Enabled = true;
					this.tbSponsorDepartment.Enabled = true;
					this.tbSponsorOrganization.Enabled = true;
					this.tbSponsorServiceNo.Enabled = true;
					this.tbSponsorMobile.Enabled = true;
					this.tbSponsorPhoneOffice.Enabled = true;
					this.tbSponsorPhoneHome.Enabled = true;
					this.tbSponsorFax.Enabled = true;
					break;
				case Model.Entities.Common.Sponsorships.Organization:
					this.tbSponsorName.Enabled = true;
					this.tbSponsorCNIC.Enabled = false;
					this.tbSponsorPassportNo.Enabled = false;
					this.tbSponsorRelationshipWithGuardian.Enabled = true;
					this.tbSponsorDesignation.Enabled = false;
					this.tbSponsorDepartment.Enabled = false;
					this.tbSponsorOrganization.Enabled = false;
					this.tbSponsorServiceNo.Enabled = false;
					this.tbSponsorMobile.Enabled = false;
					this.tbSponsorPhoneOffice.Enabled = true;
					this.tbSponsorPhoneHome.Enabled = false;
					this.tbSponsorFax.Enabled = true;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(sponsoredByEnum), sponsoredByEnum, null);
			}

			this.svtbSponsorName.Enabled = this.tbSponsorName.Enabled;
			this.svtbSponsorCNIC.Enabled = this.tbSponsorCNIC.Enabled;
			this.svtbSponsorPassportNo.Enabled = this.tbSponsorPassportNo.Enabled;
			this.svtbSponsorRelationshipWithGuardian.Enabled = this.tbSponsorRelationshipWithGuardian.Enabled;
			this.svtbSponsorDesignation.Enabled = this.tbSponsorDesignation.Enabled;
			this.svtbSponsorDepartment.Enabled = this.tbSponsorDepartment.Enabled;
			this.svtbSponsorOrganization.Enabled = this.tbSponsorOrganization.Enabled;
			this.svtbSponsorServiceNo.Enabled = this.tbSponsorServiceNo.Enabled;
			this.svtbSponsorMobile.Enabled = this.tbSponsorMobile.Enabled;
			this.svtbSponsorPhoneOffice.Enabled = this.tbSponsorPhoneOffice.Enabled;
			this.svtbSponsorPhoneHome.Enabled = this.tbSponsorPhoneHome.Enabled;
			this.svtbSponsorFax.Enabled = this.tbSponsorFax.Enabled;

			if (!this.tbSponsorName.Enabled)
				this.tbSponsorName.Text = null;
			if (!this.tbSponsorCNIC.Enabled)
				this.tbSponsorCNIC.Text = null;
			if (!this.tbSponsorPassportNo.Enabled)
				this.tbSponsorPassportNo.Text = null;
			if (!this.tbSponsorRelationshipWithGuardian.Enabled)
				this.tbSponsorRelationshipWithGuardian.Text = null;
			if (!this.tbSponsorDesignation.Enabled)
				this.tbSponsorDesignation.Text = null;
			if (!this.tbSponsorDepartment.Enabled)
				this.tbSponsorDepartment.Text = null;
			if (!this.tbSponsorOrganization.Enabled)
				this.tbSponsorOrganization.Text = null;
			if (!this.tbSponsorServiceNo.Enabled)
				this.tbSponsorServiceNo.Text = null;
			if (!this.tbSponsorMobile.Enabled)
				this.tbSponsorMobile.Text = null;
			if (!this.tbSponsorPhoneOffice.Enabled)
				this.tbSponsorPhoneOffice.Text = null;
			if (!this.tbSponsorPhoneHome.Enabled)
				this.tbSponsorPhoneHome.Text = null;
			if (!this.tbSponsorFax.Enabled)
				this.tbSponsorFax.Text = null;
		}
	}
}