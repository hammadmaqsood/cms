﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Web.Sys.Staff.Students.Profile
{


	public partial class AdditionalInformation
	{

		/// <summary>
		/// modal control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireModal modal;

		/// <summary>
		/// updatePanelMain control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.UpdatePanel updatePanelMain;

		/// <summary>
		/// alertModal control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireAlert alertModal;

		/// <summary>
		/// tbEnrollment control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireTextBox tbEnrollment;

		/// <summary>
		/// tbName control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireTextBox tbName;

		/// <summary>
		/// ddlType control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlType;

		/// <summary>
		/// dtpReferenceDate control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDateTimePickerTextbox dtpReferenceDate;

		/// <summary>
		/// tbSubject control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireTextBox tbSubject;

		/// <summary>
		/// tbDescription control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireTextBox tbDescription;

		/// <summary>
		/// btnSave control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireButton btnSave;
	}
}
