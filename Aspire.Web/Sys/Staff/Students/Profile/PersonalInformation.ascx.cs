﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Staff.Students.Profile
{
	public partial class PersonalInformation : System.Web.UI.UserControl
	{
		private new Profile Page => (Profile)base.Page;

		public const string Pakistani = "PAKISTANI";
		public const string Other = "OTHER";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Visible = false;
			}
		}

		public void ShowModal()
		{
			var student = BL.Core.StudentInformation.Staff.StudentProfile.GetStudent(this.Page.Enrollment, this.Page.StaffIdentity.LoginSessionGuid);
			if (student == null)
			{
				this.Page.AddNoRecordFoundAlert();
				Profile.Redirect(this.Page.Enrollment, false);
				return;
			}

			this.tbEnrollment.Text = student.Enrollment;
			this.tbName.Text = student.Name;
			this.tbPersonalEmail.Text = student.PersonalEmail;
			this.tbUniversityEmail.Text = student.UniversityEmail;
			this.tbPhoneHome.Text = student.Phone;
			this.tbMobile.Text = student.Mobile;
			this.tbCNIC.Text = student.CNIC;
			this.tbPassportNo.Text = student.PassportNo;
			this.tbDOB.SelectedDate = student.DOB;
			this.ddlGender.FillGenders(CommonListItems.Select).SetEnumValue(student.GenderEnum);
			this.ddlCategory.FillCategories(CommonListItems.Select).SetEnumValue(student.CategoryEnum);
			this.ddlBloodGroup.FillBloodGroups(CommonListItems.Select).SetEnumValue(student.BloodGroupEnum);
			this.ddlNationality.DataBind(new[] { Pakistani, Other });
			this.ddlCountry.FillCountries();
			this.lblCountry.Text = student.Country;
			this.lblProvince.Text = student.Province;
			this.lblDistrict.Text = student.District;
			this.lblTehsil.Text = student.Tehsil;
			this.lblDomicile.Text = student.Domicile;
			var isPakistani = student.Nationality == Pakistani;
			if (isPakistani)
			{
				this.ddlNationality.SelectedValue = Pakistani;
				this.ddlNationality_OnSelectedIndexChanged(null, null);
				this.ddlProvince.SetSelectedValueIfExists(student.Province);
				this.ddlProvince_OnSelectedIndexChanged(null, null);
				this.ddlDistrict.SetSelectedValueIfExists(student.District);
				this.ddlDistrict_OnSelectedIndexChanged(null, null);
				this.ddlTehsil.SetSelectedValueIfExists(student.Tehsil);
				this.ddlDomicile.SetSelectedValueIfExists(student.Domicile);

				this.tbProvince.Text = student.Province;
				this.tbDistrict.Text = student.District;
				this.tbTehsil.Text = student.Tehsil;
				this.tbDomicile.Text = student.Domicile;
			}
			else
			{
				this.ddlNationality.SelectedValue = Other;
				this.ddlNationality_OnSelectedIndexChanged(null, null);
				this.tbNationality.Text = student.Nationality;
				this.ddlCountry.SetSelectedValueIfExists(student.Country);

				this.tbProvince.Text = student.Province;
				this.tbDistrict.Text = student.District;
				this.tbTehsil.Text = student.Tehsil;
				this.tbDomicile.Text = student.Domicile;
			}

			this.ddlAreaType.FillAreaTypes(CommonListItems.Select).SetEnumValue(student.AreaTypeEnum);
			this.tbServiceNo.Text = student.ServiceNo;
			this.tbReligion.Text = student.Religion;
			this.tbNextOfKin.Text = student.NextOfKin;
			this.tbCurrentAddress.Text = student.CurrentAddress;
			this.tbPermanentAddress.Text = student.PermanentAddress;
			this.tbPhysicalDisability.Text = student.PhysicalDisability;
			this.Visible = true;
			this.modal.Show();
		}

		protected void ddlNationality_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var isPakistani = this.ddlNationality.SelectedValue == Pakistani;

			this.tbNationality.Visible = isPakistani == false;
			this.tbNationality.Text = isPakistani ? Pakistani : null;

			this.ddlCountry.SelectedValue = isPakistani ? "Pakistan" : null;
			this.ddlCountry.Enabled = isPakistani == false;

			if (isPakistani)
				this.ddlProvince.FillProvinces(CommonListItems.Select);
			else
				this.ddlProvince.DataBind(CommonListItems.Select);
			this.ddlProvince.Visible = isPakistani;
			this.tbProvince.Visible = isPakistani == false;

			this.ddlDistrict.DataBind(CommonListItems.Select);
			this.ddlDistrict.Visible = isPakistani;
			this.tbDistrict.Visible = isPakistani == false;

			this.ddlTehsil.DataBind(CommonListItems.Select);
			this.ddlTehsil.Visible = isPakistani;
			this.tbTehsil.Visible = isPakistani == false;

			if (isPakistani)
				this.ddlDomicile.FillAllDistricts().AddStaticListItem(CommonListItems.Select);
			else
				this.ddlDomicile.DataBind(CommonListItems.Select);
			this.ddlDomicile.Visible = isPakistani;
			this.tbDomicile.Visible = isPakistani == false;

			this.svtbNationality.Enabled = this.svtbNationality.Visible = this.tbNationality.Visible;
			this.svtbProvince.Enabled = this.svtbProvince.Visible = this.tbProvince.Visible;
			this.svtbDistrict.Enabled = this.svtbDistrict.Visible = this.tbDistrict.Visible;
			this.svtbTehsil.Enabled = this.svtbTehsil.Visible = this.tbTehsil.Visible;
			this.svtbDomicile.Enabled = this.svtbDomicile.Visible = this.tbDomicile.Visible;
			this.rfvddlCountry.Enabled = this.rfvddlCountry.Visible = this.ddlCountry.Enabled;
			this.rfvddlProvince.Enabled = this.rfvddlProvince.Visible = this.ddlProvince.Visible;
			this.rfvddlDistrict.Enabled = this.rfvddlDistrict.Visible = this.ddlDistrict.Visible;
			this.rfvddlTehsil.Enabled = this.rfvddlTehsil.Visible = this.ddlTehsil.Visible;
			this.rfvddlDomicile.Enabled = this.rfvddlDomicile.Visible = this.ddlDomicile.Visible;
		}

		protected void ddlProvince_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var province = this.ddlProvince.SelectedValue;
			if (string.IsNullOrWhiteSpace(province))
				this.ddlDistrict.DataBind(CommonListItems.Select);
			else
				this.ddlDistrict.FillDistricts(province, CommonListItems.Select);
			this.ddlDistrict_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDistrict_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var province = this.ddlProvince.SelectedValue;
			var district = this.ddlDistrict.SelectedValue;
			if (string.IsNullOrWhiteSpace(district))
				this.ddlTehsil.DataBind(CommonListItems.Select);
			else
				this.ddlTehsil.FillTehsils(province, district, CommonListItems.Select);
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var studentID = this.Page.StudentID;
			var personalEmail = this.tbPersonalEmail.Text;
			var universityEmail = this.tbUniversityEmail.Text;
			var phoneHome = this.tbPhoneHome.Text;
			var mobile = this.tbMobile.Text;
			var cnic = this.tbCNIC.Text;
			var passportNo = this.tbPassportNo.Text;
			var dob = this.tbDOB.SelectedDate ?? throw new InvalidOperationException();
			var genderEnum = this.ddlGender.GetSelectedEnumValue<Genders>();
			var categoryEnum = this.ddlCategory.GetSelectedEnumValue<Categories>();
			var bloodGroupEnum = this.ddlBloodGroup.GetSelectedEnumValue<BloodGroups>(null);
			var nationality = this.tbNationality.Text;
			var country = this.ddlCountry.SelectedValue.ToNullIfWhiteSpace() ?? this.lblCountry.Text;
			var province = this.ddlProvince.SelectedValue.ToNullIfWhiteSpace() ?? this.tbProvince.Text.ToNullIfWhiteSpace() ?? this.lblProvince.Text;
			var district = this.ddlDistrict.SelectedValue.ToNullIfWhiteSpace() ?? this.tbDistrict.Text.ToNullIfWhiteSpace() ?? this.lblDistrict.Text;
			var tehsil = this.ddlTehsil.SelectedValue.ToNullIfWhiteSpace() ?? this.tbTehsil.Text.ToNullIfWhiteSpace() ?? this.lblTehsil.Text;
			var domicile = this.ddlDomicile.SelectedValue.ToNullIfWhiteSpace() ?? this.tbDomicile.Text.ToNullIfWhiteSpace() ?? this.lblDomicile.Text;
			var areaTypeEnum = this.ddlAreaType.GetSelectedEnumValue<AreaTypes>(null);
			var serviceNo = this.tbServiceNo.Text;
			var religion = this.tbReligion.Text;
			var nextOfKin = this.tbNextOfKin.Text;
			var currentAddress = this.tbCurrentAddress.Text;
			var permanentAddress = this.tbPermanentAddress.Text;
			var physicalDisability = this.tbPhysicalDisability.Text;

			var result = BL.Core.StudentInformation.Staff.StudentProfile.UpdatePersonalInformation(studentID, personalEmail, universityEmail, phoneHome, mobile, cnic, passportNo, dob, genderEnum, categoryEnum, bloodGroupEnum, nationality, country, province, district, tehsil, domicile, areaTypeEnum, serviceNo, religion, nextOfKin, currentAddress, permanentAddress, physicalDisability, this.Page.StaffIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.NoRecordFound:
					this.Page.Refresh();
					break;
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.Success:
					this.Page.AddSuccessMessageHasBeenUpdated("Personal Information");
					this.Page.Refresh();
					break;
				default:
					throw new NotImplementedEnumException(result);
			}
		}
	}
}