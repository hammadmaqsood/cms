﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DegreeInformation.ascx.cs" Inherits="Aspire.Web.Sys.Staff.Students.Profile.DegreeInformation" %>

<aspire:AspireModal runat="server" ID="modal" ModalSize="Large" HeaderText="Current Academic Information">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanelMain">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alertModal" />
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
							<aspire:AspireTextBox runat="server" ID="tbEnrollment" ReadOnly="True" ValidationGroup="Degree" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbName" />
							<aspire:AspireTextBox runat="server" ID="tbName" TextMode="SingleLine" ValidationGroup="Degree" ReadOnly="true" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Final Transcript No.:" AssociatedControlID="tbFinalTranscriptNo" />
							<aspire:AspireTextBox runat="server" ID="tbFinalTranscriptNo" ValidationGroup="Degree" />
							<aspire:AspireUInt32Validator runat="server" ValidationGroup="Degree" AllowNull="True" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Transcript No." RequiredErrorMessage="This field is required." ControlToValidate="tbFinalTranscriptNo" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Final Transcript Issue Date:" AssociatedControlID="dtpFinalTranscriptIssueDate" />
							<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpFinalTranscriptIssueDate" ValidationGroup="Degree" />
							<aspire:AspireDateTimeValidator runat="server" AllowNull="True" ControlToValidate="dtpFinalTranscriptIssueDate" ValidationGroup="Degree" InvalidDataErrorMessage="Invalid Date." RequiredErrorMessage="This field is required." />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Final CGPA:" AssociatedControlID="tbFinalCGPA" />
							<aspire:AspireTextBox runat="server" ID="tbFinalCGPA" data-inputmask="'alias': 'cgpa4'" ValidationGroup="Degree" />
							<aspire:AspireDoubleValidator runat="server" ValidationGroup="Degree" AllowNull="True" MinValue="0" MaxValue="4" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid CGPA." RequiredErrorMessage="This field is required." ControlToValidate="tbFinalCGPA" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Degree No.:" AssociatedControlID="tbDegreeNo" />
							<aspire:AspireTextBox runat="server" ID="tbDegreeNo" ValidationGroup="Degree" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Gazette No.:" AssociatedControlID="tbDegreeGazetteNo" />
							<aspire:AspireTextBox runat="server" ID="tbDegreeGazetteNo" ValidationGroup="Degree" />
							<aspire:AspireUInt16Validator AllowNull="True" runat="server" ValidationGroup="Degree" ControlToValidate="tbDegreeGazetteNo" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid number." InvalidRangeErrorMessage="Invalid number." />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Degree Issue Date:" AssociatedControlID="dtpDegreeIssueDate" />
							<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpDegreeIssueDate" ValidationGroup="Degree" />
							<aspire:AspireDateTimeValidator runat="server" AllowNull="True" ControlToValidate="dtpDegreeIssueDate" ValidationGroup="Degree" InvalidDataErrorMessage="Invalid Date." RequiredErrorMessage="This field is required." />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Degree Issued to Student Date:" AssociatedControlID="dtpDegreeIssuedToStudent" />
							<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpDegreeIssuedToStudent" ValidationGroup="Degree" />
							<aspire:AspireDateTimeValidator runat="server" AllowNull="True" ControlToValidate="dtpDegreeIssuedToStudent" ValidationGroup="Degree" InvalidDataErrorMessage="Invalid Date." RequiredErrorMessage="This field is required." />
						</div>
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="Basic" Text="Save" OnClick="btnSave_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>
