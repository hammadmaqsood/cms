﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PersonalInformation.ascx.cs" Inherits="Aspire.Web.Sys.Staff.Students.Profile.PersonalInformation" %>

<aspire:AspireModal runat="server" ID="modal" ModalSize="Large" HeaderText="Personal Information">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanelMain">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alertModal" />
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
							<aspire:AspireTextBox runat="server" ID="tbEnrollment" ReadOnly="True" ValidationGroup="Personal" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbName" />
							<aspire:AspireTextBox runat="server" ID="tbName" TextMode="SingleLine" ValidationGroup="Personal" ReadOnly="true" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Personal Email:" AssociatedControlID="tbPersonalEmail" />
							<aspire:AspireTextBox TextTransform="LowerCase" ValidationGroup="Personal" runat="server" MaxLength="100" ID="tbPersonalEmail" TextMode="Email" />
							<aspire:AspireStringValidator runat="server" ValidationExpression="Email" ControlToValidate="tbPersonalEmail" InvalidDataErrorMessage="Invalid Email." ValidationGroup="Personal" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="University Email:" AssociatedControlID="tbUniversityEmail" />
							<aspire:AspireTextBox ValidationGroup="Personal" runat="server" MaxLength="100" ID="tbUniversityEmail" />
							<aspire:AspireStringValidator runat="server" ValidationExpression="Email" ControlToValidate="tbUniversityEmail" InvalidDataErrorMessage="Invalid Email." ValidationGroup="Personal" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Phone No. (Home):" AssociatedControlID="tbPhoneHome" />
							<aspire:AspireTextBox ValidationGroup="Personal" runat="server" MaxLength="50" ID="tbPhoneHome" />
							<aspire:AspireStringValidator ID="svtbPhoneHome" runat="server" AllowNull="True" ValidationGroup="Personal" ControlToValidate="tbPhoneHome" RequiredErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Mobile No:" AssociatedControlID="tbMobile" />
							<aspire:AspireTextBox ValidationGroup="Personal" data-inputmask="'alias': 'mobile'" runat="server" ID="tbMobile" MaxLength="50" />
							<aspire:AspireStringValidator ID="svtbMobile" runat="server" AllowNull="True" ValidationGroup="Personal" ControlToValidate="tbMobile" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Mobile No." ValidationExpression="Mobile" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="CNIC:" AssociatedControlID="tbCNIC" />
							<aspire:AspireTextBox ValidationGroup="Personal" runat="server" MaxLength="15" data-inputmask="'alias': 'cnic'" ID="tbCNIC" />
							<aspire:AspireStringValidator ID="svtbCNIC" runat="server" AllowNull="True" ValidationGroup="Personal" ControlToValidate="tbCNIC" InvalidDataErrorMessage="Invalid CNIC." RequiredErrorMessage="This field is required." ValidationExpression="CNIC" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Passport No.:" AssociatedControlID="tbPassportNo" />
							<aspire:AspireTextBox ValidationGroup="Personal" runat="server" MaxLength="50" ID="tbPassportNo" />
							<aspire:AspireStringValidator ID="svtbPassportNo" runat="server" AllowNull="True" ValidationGroup="Personal" ControlToValidate="tbPassportNo" RequiredErrorMessage="This field is required." />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Date of Birth:" AssociatedControlID="tbDOB" />
							<aspire:AspireDateTimePickerTextbox runat="server" ID="tbDOB" DisplayMode="ShortDate" ValidationGroup="Personal" />
							<aspire:AspireDateTimeValidator ID="dttbDOB" runat="server" ControlToValidate="tbDOB" InvalidDataErrorMessage="Invalid Date." AllowNull="False" RequiredErrorMessage="This field is required." ValidationGroup="Personal" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Gender:" AssociatedControlID="ddlGender" />
							<aspire:AspireDropDownList runat="server" ID="ddlGender" RepeatDirection="Horizontal" RepeatLayout="Flow" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlGender" RequiredErrorMessage="This field is required." ValidationGroup="Personal" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Category:" AssociatedControlID="ddlCategory" />
							<aspire:AspireDropDownList runat="server" ID="ddlCategory" RepeatDirection="Horizontal" RepeatLayout="Flow" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlCategory" RequiredErrorMessage="This field is required." ValidationGroup="Personal" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Blood Group:" AssociatedControlID="ddlBloodGroup" />
							<aspire:AspireDropDownList runat="server" ID="ddlBloodGroup" />
							<aspire:AspireRequiredFieldValidator ID="rfvddlBloodGroup" runat="server" ValidationGroup="Personal" ControlToValidate="ddlBloodGroup" ErrorMessage="This field is required." />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Nationality:" AssociatedControlID="ddlNationality" />
							<aspire:AspireDropDownList runat="server" ID="ddlNationality" RepeatLayout="Flow" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="ddlNationality_OnSelectedIndexChanged" />
							<aspire:AspireTextBox runat="server" ID="tbNationality" MaxLength="50" TextTransform="UpperCase" TrimText="False" ValidationGroup="Personal" PlaceHolder="Provide Nationality" />
							<aspire:AspireRequiredFieldValidator ID="svtbNationality" runat="server" ValidationGroup="Personal" ControlToValidate="tbNationality" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Country:" AssociatedControlID="ddlCountry" />
							<aspire:AspireDropDownList runat="server" ID="ddlCountry" />
							<aspire:Label runat="server" ID="lblCountry" />
							<aspire:AspireRequiredFieldValidator ID="rfvddlCountry" runat="server" ValidationGroup="Personal" ControlToValidate="ddlCountry" ErrorMessage="This field is required." />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="State/Province:" AssociatedControlID="ddlProvince" />
							<aspire:AspireDropDownList runat="server" ValidationGroup="Personal" ID="ddlProvince" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="ddlProvince_OnSelectedIndexChanged" />
							<aspire:AspireTextBox ValidationGroup="Personal" runat="server" ID="tbProvince" />
							<aspire:AspireLabel runat="server" ID="lblProvince" />
							<aspire:AspireRequiredFieldValidator ID="rfvddlProvince" runat="server" ValidationGroup="Personal" ControlToValidate="ddlProvince" ErrorMessage="This field is required." />
							<aspire:AspireStringValidator ID="svtbProvince" runat="server" AllowNull="false" ValidationGroup="Personal" ControlToValidate="tbProvince" RequiredErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="District/City:" AssociatedControlID="ddlDistrict" />
							<aspire:AspireDropDownList runat="server" ValidationGroup="Personal" ID="ddlDistrict" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="ddlDistrict_OnSelectedIndexChanged" />
							<aspire:AspireTextBox ValidationGroup="Personal" runat="server" ID="tbDistrict" />
							<aspire:AspireLabel runat="server" ID="lblDistrict" />
							<aspire:AspireRequiredFieldValidator ID="rfvddlDistrict" runat="server" ValidationGroup="Personal" ControlToValidate="ddlDistrict" ErrorMessage="This field is required." />
							<aspire:AspireStringValidator ID="svtbDistrict" runat="server" AllowNull="false" ValidationGroup="Personal" ControlToValidate="tbDistrict" RequiredErrorMessage="This field is required." />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Tehsil:" AssociatedControlID="ddlTehsil" />
							<aspire:AspireDropDownList runat="server" ValidationGroup="Personal" ID="ddlTehsil" />
							<aspire:AspireTextBox ValidationGroup="Personal" runat="server" ID="tbTehsil" />
							<aspire:AspireLabel runat="server" ID="lblTehsil" />
							<aspire:AspireRequiredFieldValidator ID="rfvddlTehsil" runat="server" ValidationGroup="Personal" ControlToValidate="ddlTehsil" ErrorMessage="This field is required." />
							<aspire:AspireStringValidator ID="svtbTehsil" runat="server" AllowNull="True" ValidationGroup="Personal" ControlToValidate="tbTehsil" RequiredErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Domicile:" AssociatedControlID="ddlDomicile" />
							<aspire:AspireDropDownListWithOptionGroups runat="server" ValidationGroup="Personal" ID="ddlDomicile" />
							<aspire:AspireTextBox ValidationGroup="Personal" runat="server" ID="tbDomicile" />
							<aspire:AspireLabel runat="server" ID="lblDomicile" />
							<aspire:AspireRequiredFieldValidator ID="rfvddlDomicile" runat="server" ValidationGroup="Personal" ControlToValidate="ddlDomicile" ErrorMessage="This field is required." />
							<aspire:AspireStringValidator ID="svtbDomicile" runat="server" AllowNull="True" ValidationGroup="Personal" ControlToValidate="tbDomicile" RequiredErrorMessage="This field is required." />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Area Type:" AssociatedControlID="ddlAreaType" />
							<aspire:AspireDropDownList runat="server" ValidationGroup="Personal" ID="ddlAreaType" RepeatLayout="Flow" RepeatDirection="Horizontal" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Service No. (If Naval):" AssociatedControlID="tbServiceNo" />
							<aspire:AspireTextBox ValidationGroup="Personal" runat="server" MaxLength="50" ID="tbServiceNo" />
							<aspire:AspireStringValidator ID="svtbServiceNo" runat="server" AllowNull="True" ValidationGroup="Personal" ControlToValidate="tbServiceNo" RequiredErrorMessage="This field is required." />
						</div>
					</div>

				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Religion:" AssociatedControlID="tbReligion" />
							<aspire:AspireTextBox runat="server" ID="tbReligion" MaxLength="100" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Next Of Kin:" AssociatedControlID="tbNextOfKin" />
							<aspire:AspireTextBox runat="server" ID="tbNextOfKin" MaxLength="100" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Current Address:" AssociatedControlID="tbCurrentAddress" />
							<aspire:AspireTextBox ValidationGroup="Personal" runat="server" MaxLength="500" Rows="3" ID="tbCurrentAddress" TextMode="MultiLine" />
							<aspire:AspireStringValidator ID="svtbCurrentAddress" runat="server" AllowNull="True" ValidationGroup="Personal" ControlToValidate="tbCurrentAddress" RequiredErrorMessage="This field is required." />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Permanent Address:" AssociatedControlID="tbPermanentAddress" />
							<aspire:AspireTextBox ValidationGroup="Personal" runat="server" MaxLength="500" Rows="3" ID="tbPermanentAddress" TextMode="MultiLine" />
							<aspire:AspireStringValidator ID="svtbPermanentAddress" runat="server" AllowNull="True" ValidationGroup="Personal" ControlToValidate="tbPermanentAddress" RequiredErrorMessage="This field is required." />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Physical Disability:" AssociatedControlID="tbPhysicalDisability" />
							<aspire:AspireTextBox ValidationGroup="Personal" runat="server" MaxLength="500" Rows="3" ID="tbPhysicalDisability" />
							<aspire:AspireStringValidator ID="svtbPhysicalDisability" runat="server" AllowNull="True" ValidationGroup="Personal" ControlToValidate="tbPhysicalDisability" RequiredErrorMessage="This field is required." />
						</div>
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="Personal" Text="Save" OnClick="btnSave_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>
