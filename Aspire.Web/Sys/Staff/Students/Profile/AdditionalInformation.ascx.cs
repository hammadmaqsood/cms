﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Staff.Students.Profile
{
	public partial class AdditionalInformation : System.Web.UI.UserControl
	{
		private new Profile Page => (Profile)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Visible = false;
			}
		}

		private int? StudentCustomInfoID
		{
			get => (int?)this.ViewState[nameof(this.StudentCustomInfoID)];
			set => this.ViewState[nameof(this.StudentCustomInfoID)] = value;
		}

		public void ShowModal(int? studentCustomInfoID)
		{
			this.StudentCustomInfoID = null;
			var student = BL.Core.StudentInformation.Staff.StudentProfile.GetStudent(this.Page.Enrollment, this.Page.StaffIdentity.LoginSessionGuid);
			if (student == null)
			{
				this.Page.AddNoRecordFoundAlert();
				Profile.Redirect(this.Page.Enrollment, false);
				return;
			}

			this.tbEnrollment.Text = student.Enrollment;
			this.tbName.Text = student.Name;
			this.ddlType.FillGuidEnums<Model.Entities.StudentCustomInfo.Types>(CommonListItems.Select);
			if (studentCustomInfoID == null)
			{
				this.modal.HeaderText = "Add Additional Information";
				this.ddlType.ClearSelection();
				this.dtpReferenceDate.SelectedDate = null;
				this.tbSubject.Text = null;
				this.tbDescription.Text = null;
			}
			else
			{
				var studentCustomInfo = student.StudentCustomInfos.SingleOrDefault(ci => ci.StudentCustomInfoID == studentCustomInfoID);
				if (studentCustomInfo == null)
				{
					this.Page.AddNoRecordFoundAlert();
					this.Page.Refresh();
					return;
				}
				this.modal.HeaderText = "Edit Additional Information";
				this.ddlType.SetSelectedGuidEnum(studentCustomInfo.TypeEnum);
				this.dtpReferenceDate.SelectedDate = studentCustomInfo.ReferenceDate;
				this.StudentCustomInfoID = studentCustomInfo.StudentCustomInfoID;
				this.tbSubject.Text = studentCustomInfo.Subject;
				this.tbDescription.Text = studentCustomInfo.Description;
			}
			this.Visible = true;
			this.modal.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var studentID = this.Page.StudentID;
			var typeEnum = this.ddlType.GetSelectedGuidEnum<Model.Entities.StudentCustomInfo.Types>();
			var referenceDate = this.dtpReferenceDate.SelectedDate?.Date ?? throw new InvalidOperationException();
			var subject = this.tbSubject.Text.TrimAndCannotBeEmpty();
			var description = this.tbDescription.Text.TrimAndCannotBeEmpty();

			var result = BL.Core.StudentInformation.Staff.StudentProfile.AddOrUpdateAdditionalInformation(studentID, this.StudentCustomInfoID, typeEnum, referenceDate, subject, description, this.Page.StaffIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.NoRecordFound:
					this.Page.Refresh();
					break;
				case BL.Core.StudentInformation.Staff.StudentProfile.UpdateResult.Success:
					if (this.StudentCustomInfoID == null)
						this.Page.AddSuccessMessageHasBeenAdded("Additional Information");
					else
						this.Page.AddSuccessMessageHasBeenUpdated("Additional Information");
					this.Page.Refresh();
					break;
				default:
					throw new NotImplementedEnumException(result);
			}
		}
	}
}