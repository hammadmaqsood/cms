﻿using Aspire.Lib.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Staff.Students
{
	[AspirePage("E8A72D0B-8C97-44FB-8095-C334ED763B37", UserTypes.Staff, "~/Sys/Staff/Students/CommunityServices.aspx", "Community Services", true, AspireModules.StudentInformation)]
	public partial class CommunityServices : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.export);
		public override string PageTitle => "Community Services";

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.StudentInformation.Module, UserGroupPermission.Allowed},
		};
		public static string GetPageUrl(string enrollment, bool search = true)
		{
			return GetAspirePageAttribute<CommunityServices>().PageUrl.AttachQueryParams("Enrollment", enrollment, "Search", search);
		}

		private void Refresh()
		{
			Redirect(GetPageUrl((string)this.ViewState["Enrollment"]));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.panelCommunitySupportProgram.Visible = false;
				var enrollment = this.Request.GetParameterValue("Enrollment");
				this.tbEnrollment.Enrollment = enrollment;
				if (this.Request.GetParameterValue<bool>("Search") == true)
					this.tbEnrollment_Search(null, null);
			}
		}

		protected void tbEnrollment_Search(object sender, EventArgs args)
		{
			var enrollment = this.tbEnrollment.Enrollment;
			var studentInfo = this.ucStudentBasicInfo.LoadBasicStudentInformation(enrollment);
			if (studentInfo == null)
			{
				this.AddNoRecordFoundAlert();
				return;
			}
			this.panelCommunitySupportProgram.Visible = true;
			this.ViewState["StudentID"] = studentInfo.StudentID;
			this.ViewState["Enrollment"] = studentInfo.Enrollment;
			this.GetData(0);
		}



		protected void btnAdd_OnClick(object sender, EventArgs e)
		{
			this.DisplayCommunityServiceModal(null);
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{

			if (!this.IsValid)
				return;
			var studentCommunityServiceID = (int?)this.ViewState[nameof(StudentCommunityService.StudentCommunityServiceID)];
			var studentID = (int)this.ViewState["StudentID"];
			var semesterID = this.ddlSemester.SelectedValue.ToShort();
			var organization = this.tbOrganization.Text;
			var jobTitle = this.tbJobTitle.Text;
			var completedDate = this.dtpCompletedDate.SelectedDate;
			var creditHours = this.tbCreditHours.Text.ToByte();
			if (studentCommunityServiceID == null)
			{
				var result = Aspire.BL.Core.StudentInformation.Staff.CommunityServices.AddCommunityService(studentID, semesterID, organization, jobTitle, completedDate.Value, creditHours, this.StaffIdentity.LoginSessionGuid);
				switch (result)
				{
					case Aspire.BL.Core.StudentInformation.Staff.CommunityServices.AddCommunityServiceStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.Refresh();
						break;
					case Aspire.BL.Core.StudentInformation.Staff.CommunityServices.AddCommunityServiceStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Student Community Service");
						this.Refresh();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var result = Aspire.BL.Core.StudentInformation.Staff.CommunityServices.UpdateCommunityService(studentCommunityServiceID, semesterID, organization, jobTitle, completedDate.Value, creditHours, this.StaffIdentity.LoginSessionGuid);
				switch (result)
				{
					case Aspire.BL.Core.StudentInformation.Staff.CommunityServices.UpdateCommunityServiceStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.Refresh();
						break;
					case Aspire.BL.Core.StudentInformation.Staff.CommunityServices.UpdateCommunityServiceStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Student Community Service");
						this.Refresh();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

			}
		}

		private void DisplayCommunityServiceModal(int? studentCommunityServiceID)
		{
			this.ddlSemester.FillSemesters();
			if (studentCommunityServiceID == null)
			{
				this.ddlSemester.ClearSelection();
				this.tbOrganization.Text = null;
				this.tbJobTitle.Text = null;
				this.dtpCompletedDate.SelectedDate = null;
				this.tbCreditHours.Text = null;

				this.modalCommunityServices.HeaderText = "Add Community Service";
				this.btnSave.Text = @"Add";
				this.ViewState[nameof(StudentCommunityService.StudentCommunityServiceID)] = null;
				this.modalCommunityServices.Show();
			}
			else
			{
				var studentCommunityService = Aspire.BL.Core.StudentInformation.Staff.CommunityServices.GetCommunityService(studentCommunityServiceID.Value, this.StaffIdentity.LoginSessionGuid);
				this.ddlSemester.SelectedValue = studentCommunityService.SemesterID.ToString();
				this.tbOrganization.Text = studentCommunityService.Organization;
				this.tbJobTitle.Text = studentCommunityService.JobTitle;
				this.dtpCompletedDate.SelectedDate = studentCommunityService.CompletedDate;
				this.tbCreditHours.Text = studentCommunityService.Hours.ToString();

				this.modalCommunityServices.HeaderText = "Edit Community Service";
				this.btnSave.Text = @"Save";
				this.ViewState[nameof(StudentCommunityService.StudentCommunityServiceID)] = studentCommunityServiceID;
				this.modalCommunityServices.Show();
			}
		}

		private void GetData(int pageIndex)
		{
			var studentID = (int)this.ViewState["StudentID"];
			var communityService = BL.Core.StudentInformation.Staff.CommunityServices.GetStudentCommunityServices(studentID, this.StaffIdentity.LoginSessionGuid);
			this.gvCommunityServices.DataBind(communityService);
			if (this.gvCommunityServices.FooterRow != null)
				this.gvCommunityServices.FooterRow.Cells[4].Text = communityService.Sum(cs => cs.Hours).ToString();
		}

		protected void gvCommunityServices_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayCommunityServiceModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var studentID = (int)this.ViewState["StudentID"];
					var deleted = Aspire.BL.Core.StudentInformation.Staff.CommunityServices.DeleteStudentCommunityService(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					switch (deleted)
					{
						case BL.Core.StudentInformation.Staff.CommunityServices.DeleteCommunityServiceStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.Refresh();
							break;
						case BL.Core.StudentInformation.Staff.CommunityServices.DeleteCommunityServiceStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Student Semester");
							this.Refresh();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}
	}
}