﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Students
{
	[AspirePage("51111797-7024-47D5-94EC-20B01145AAE7", UserTypes.Staff, "~/Sys/Staff/Students/DisciplineCaseActivities.aspx", "Discipline Case Activities", true, AspireModules.CourseRegistration)]
	public partial class DisciplineCaseActivities : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.StudentInformation.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Discipline Case Activities";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.GetData(0);
			}
		}

		protected void btnAddDisciplineCaseActivity_OnClick(object sender, EventArgs e)
		{
			this.DisplayDisciplineCaseActivityModal(null);
		}

		private void DisplayDisciplineCaseActivityModal(int? disciplineCaseActivityID)
		{
			if (disciplineCaseActivityID != null)
			{
				var disciplineCaseActivity = Aspire.BL.Core.StudentInformation.Staff.DisciplineCaseActivities.GetDisciplineCaseActivity(disciplineCaseActivityID.Value, this.StaffIdentity.LoginSessionGuid);
				if (disciplineCaseActivity == null)
				{
					this.AddNoRecordFoundAlert();
					return;
				}

				this.tbActivityName.Text = disciplineCaseActivity.ActivityName;
				this.tbFineAmount.Text = disciplineCaseActivity.FineAmount.ToString();
				this.ViewState["DisciplineCaseActivityID"] = disciplineCaseActivityID;
				this.modalAddEditDisciplineCaseActivity.HeaderText = "Update Discipline Case Activity";
				this.btnAddEditDisciplineCaseActivities.Text = "Save";
			}
			else
			{
				this.ViewState["DisciplineCaseActivityID"] = null;
				this.modalAddEditDisciplineCaseActivity.HeaderText = "Add Discipline Case Activity";
				this.btnAddEditDisciplineCaseActivities.Text = "Add";
			}
			this.modalAddEditDisciplineCaseActivity.Show();
		}

		protected void bntAddEditDisciplineCaseActivities_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var disciplineCaseActivityID = (int?)this.ViewState["DisciplineCaseActivityID"];
			var activityName = this.tbActivityName.Text;
			var fineAmount = this.tbFineAmount.Text.ToNullableInt();
			if (disciplineCaseActivityID == null)
			{
				var result = Aspire.BL.Core.StudentInformation.Staff.DisciplineCaseActivities.AddDisciplineCaseActivity(activityName, fineAmount, this.StaffIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.StudentInformation.Staff.DisciplineCaseActivities.AddDisciplineCaseActivityStatuses.AlreadyExists:
						this.modalAlert.AddMessage(AspireAlert.AlertTypes.Error, "Discipline Case Activity of same name already exists.");
						this.UpdateModel(this.modalAddEditDisciplineCaseActivity);
						break;
					case BL.Core.StudentInformation.Staff.DisciplineCaseActivities.AddDisciplineCaseActivityStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Disciplince Case Activity");
						Redirect<DisciplineCaseActivities>();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var result = Aspire.BL.Core.StudentInformation.Staff.DisciplineCaseActivities.UpdateDisciplineCaseActivity(disciplineCaseActivityID.Value, activityName, fineAmount, this.StaffIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.StudentInformation.Staff.DisciplineCaseActivities.UpdateDisciplineCaseActivityStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<DisciplineCaseActivities>();
						break;
					case BL.Core.StudentInformation.Staff.DisciplineCaseActivities.UpdateDisciplineCaseActivityStatuses.AlreadyExists:
						this.modalAlert.AddMessage(AspireAlert.AlertTypes.Error, "Discipline Case Activity of same name already exists.");
						this.updatePanelDisciplineCaseActivity.Update();
						break;
					case BL.Core.StudentInformation.Staff.DisciplineCaseActivities.UpdateDisciplineCaseActivityStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Disciplince Case Activity");
						Redirect<DisciplineCaseActivities>();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		protected void gvDisciplineCaseActivities_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayDisciplineCaseActivityModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var result = Aspire.BL.Core.StudentInformation.Staff.DisciplineCaseActivities.DeleteDisciplineCaseActivity(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					switch (result)
					{
						case BL.Core.StudentInformation.Staff.DisciplineCaseActivities.DeleteDisciplineCaseActivityStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							Redirect<DisciplineCaseActivities>();
							break;
						case BL.Core.StudentInformation.Staff.DisciplineCaseActivities.DeleteDisciplineCaseActivityStatuses.ChildRecordExists:
							this.AddErrorAlert("Cannot be deleted. Reason: Child record exists.");
							Redirect<DisciplineCaseActivities>();
							break;

						case BL.Core.StudentInformation.Staff.DisciplineCaseActivities.DeleteDisciplineCaseActivityStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Disciplince Case Activity");
							Redirect<DisciplineCaseActivities>();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		private void GetData(int pageIndex)
		{
			var disciplineCaseActivities = Aspire.BL.Core.StudentInformation.Staff.DisciplineCaseActivities.GetDisciplineCaseActivities(this.StaffIdentity.LoginSessionGuid);
			this.gvDisciplineCaseActivities.DataBind(disciplineCaseActivities);
		}
	}
}