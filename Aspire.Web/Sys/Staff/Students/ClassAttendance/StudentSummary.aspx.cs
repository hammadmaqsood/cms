﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Sys.Staff.CourseRegistration;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Students.ClassAttendance
{
	[AspirePage("4F5AE89C-228E-4DF4-8EFC-5A0CFF526D87", UserTypes.Faculty, "~/Sys/Staff/Students/ClassAttendance/StudentSummary.aspx", "Student Attendance", true, AspireModules.ClassAttendance)]
	public partial class StudentSummary : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.CourseRegistration.Module, UserGroupPermission.Allowed},
			{StaffPermissions.ClassAttendance.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.list);
		public override string PageTitle => "Student's Attendance";

		private int? RegisteredCourseID => this.Request.GetParameterValue<int>("RegisteredCourseID");
		private bool? RegisterCourseLinkVisibility => this.Request.GetParameterValue<bool>("RegisterCourseLinkVisibility");

		public static string GetPageUrl(int registeredCourseID, bool registerCourseLinkVisibility = true)
		{
			return GetPageUrl<StudentSummary>().AttachQueryParams("RegisteredCourseID", registeredCourseID, "RegisterCourseLinkVisibility", registerCourseLinkVisibility);
		}

		public static void Redirect(int registeredCourseID)
		{
			Redirect(GetPageUrl(registeredCourseID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.RegisteredCourseID == null)
				{
					Redirect<Dashboard>();
					return;
				}
				this.hlRegisterCourse.Visible = this.RegisterCourseLinkVisibility ?? true;
				var summary = BL.Core.CourseRegistration.Staff.AttendanceSummary.GetStudentAttendanceDetails(this.RegisteredCourseID.Value, this.StaffIdentity.LoginSessionGuid);
				if (summary == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<RegisterCourse>();
					return;
				}

				this.lblEnrollment.Text = summary.Enrollment;
				this.lblRegistration.Text = summary.RegistrationNo.ToString();
				this.lblName.Text = summary.Name;
				this.lblStudentProgram.Text = summary.ProgramAlias;

				this.lblClass.Text = summary.RegisteredCourse.OfferedClass;
				this.lblContactHrs.Text = summary.RegisteredCourse.OfferedContactHoursFullName;
				this.lblCourseTitle.Text = summary.RegisteredCourse.OfferedTitle;
				this.lblCreditHrs.Text = summary.RegisteredCourse.OfferedCreditHoursFullName;
				this.lblTeacherName.Text = summary.RegisteredCourse.FacultyMemberName;
				this.lblSemesterID.Text = summary.RegisteredCourse.OfferedSemesterID.ToSemesterString();
				this.gvAttendanceDetails.DataBind(summary.AttendaceDetails);
				if (this.gvAttendanceDetails.FooterRow != null)
				{
					this.gvAttendanceDetails.FooterRow.Cells[5].Text = summary.ClassesTotalHours.ToString("0.##");
					this.gvAttendanceDetails.FooterRow.Cells[6].Text = $"{summary.PresentTotalHours.ToString("0.##")} : {summary.PresentTotalPercentage.ToString("0.##")}%";
					this.gvAttendanceDetails.FooterRow.Cells[7].Text = summary.AbsentTotalHours.ToString("0.##");
				}
			}
		}
	}
}