﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="StudentSummary.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.ClassAttendance.StudentSummary" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<style scoped="scoped">
		table.tab th {
			white-space: nowrap;
		}

		table.tab td {
			width: 50%;
		}
	</style>
	<aspire:AspireHyperLink runat="server" Text="Register Course" ID="hlRegisterCourse" NavigateUrl="~/Sys/Staff/CourseRegistration/RegisterCourse.aspx" CssClass="btn btn-primary"/>
	<table class="table table-bordered table-condensed tab">
		<caption>Student Information</caption>
		<tbody>
			<tr>
				<th>Enrollment:</th>
				<td>
					<aspire:AspireLabel runat="server" ID="lblEnrollment" /></td>
				<th>Registration No.</th>
				<td>
					<aspire:Label runat="server" ID="lblRegistration" />
				</td>
			</tr>
			<tr>
				<th>Name</th>
				<td>
					<aspire:Label runat="server" ID="lblName" /></td>
				<th>Program</th>
				<td>
					<aspire:Label runat="server" ID="lblStudentProgram" /></td>
			</tr>
		</tbody>
	</table>
	<table class="table table-bordered table-condensed tab">
		<caption>Course Information</caption>
		<tbody>
			<tr>
				<th>Title</th>
				<td>
					<aspire:Label runat="server" ID="lblCourseTitle" />
				</td>
				<th>Teacher Name</th>
				<td>
					<aspire:Label runat="server" ID="lblTeacherName" />
				</td>
			</tr>
			<tr>
				<th>Credit Hours</th>
				<td>
					<aspire:Label runat="server" ID="lblCreditHrs" />
				</td>
				<th>Contact Hours</th>
				<td>
					<aspire:Label runat="server" ID="lblContactHrs" />
				</td>
			</tr>
			<tr>
				<th>Class</th>
				<td>
					<aspire:Label runat="server" ID="lblClass" />
				</td>
				<th>Semester</th>
				<td>
					<aspire:Label runat="server" ID="lblSemesterID" />
				</td>
			</tr>
		</tbody>
	</table>
	<style scoped="scoped">
		table[id$=gvAttendanceDetails] td:first-child,
		table[id$=gvAttendanceDetails] th:first-child,
		table[id$=gvAttendanceDetails] th:nth-child(6),
		table[id$=gvAttendanceDetails] td:nth-child(6),
		table[id$=gvAttendanceDetails] th:nth-child(7),
		table[id$=gvAttendanceDetails] td:nth-child(7),
		table[id$=gvAttendanceDetails] th:nth-child(8),
		table[id$=gvAttendanceDetails] td:nth-child(8),
		table[id$=gvAttendanceDetails] th:nth-child(9),
		table[id$=gvAttendanceDetails] td:nth-child(9) {
			text-align: center;
		}
	</style>
	<aspire:AspireGridView Caption="Attendance Summary" runat="server" ID="gvAttendanceDetails" AutoGenerateColumns="False" ShowFooter="True">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate>
					<aspire:Label runat="server" Text="<%#: Container.DataItemIndex + 1 %>" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Class Date" DataField="ClassDate" DataFormatString="{0:D}" />
			<asp:BoundField HeaderText="Class Type" DataField="ClassTypeFullName" />
			<asp:BoundField HeaderText="Room" DataField="RoomName" />
			<asp:BoundField HeaderText="Remarks" DataField="Remarks" />
			<asp:TemplateField HeaderText="Total Hours">
				<ItemTemplate><%#: Eval("TotalHours","{0:0.##}") %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Present Hours">
				<ItemTemplate><%#: Eval("PresentHours","{0:0.##}") %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Absent Hours">
				<ItemTemplate><%#: Eval("AbsentHours","{0:0.##}") %></ItemTemplate>
			</asp:TemplateField>
			<%--<asp:BoundField HeaderText="Percentage" DataField="Percentage" DataFormatString="{0:###.##}%" />--%>
		</Columns>
		<FooterStyle Font-Bold="True"></FooterStyle>
	</aspire:AspireGridView>
</asp:Content>
