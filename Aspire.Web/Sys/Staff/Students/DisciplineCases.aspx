﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="DisciplineCases.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.DisciplineCases" %>

<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Import Namespace="Aspire.Web.Sys.Staff.Students" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<style type="text/css">
		.removeMargin {
			margin: 0px !important;
		}
	</style>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Add Discipline Case</h3>
				</div>
				<div class="panel-body">
					<asp:UpdatePanel runat="server">
						<ContentTemplate>
							<div class="form-inline">
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="Case Date:" AssociatedControlID="dtpCaseDate" />
									<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpCaseDate" ValidationGroup="DisciplineCase" DisplayMode="ShortDateTime" />
									<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dtpCaseDate" AllowNull="False" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." ValidationGroup="DisciplineCase" Format="ShortDateTime" />
								</div>
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="Location:" AssociatedControlID="tbLocation" />
									<aspire:AspireTextBox runat="server" ID="tbLocation" ValidationGroup="DisciplineCase" />
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbLocation" ErrorMessage="This field is required." ValidationGroup="DisciplineCase" />
								</div>
								<div class="form-group">
									<aspire:AspireButton runat="server" ButtonType="Primary" Text="Add" ID="btnSave" ValidationGroup="DisciplineCase" Glyphicon="plus" OnClick="btnSave_OnClick" />
								</div>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Discipline Cases</h3>
				</div>
				<aspire:AspireGridView runat="server" ID="gvDisciplineCases" ItemType="Aspire.Model.Entities.DisciplineCas" DataKeyNames="DisciplineCaseID" AutoGenerateColumns="False" OnRowCommand="gvDisciplineCases_OnRowCommand" AllowPaging="True" AllowSorting="True" OnPageSizeChanging="gvDisciplineCases_OnPageSizeChanging" OnPageIndexChanging="gvDisciplineCases_OnPageIndexChanging" OnSorting="gvDisciplineCases_OnSorting" CssClass="table-condensed removeMargin">
					<Columns>
						<asp:TemplateField HeaderText="#.">
							<ItemTemplate>
								<%#Container.DataItemIndex+1 %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField HeaderText="CaseFileNo" DataField="CaseFileNo" SortExpression="CaseFileNo" />
						<asp:BoundField HeaderText="Date" DataField="CaseDate" SortExpression="CaseDate" DataFormatString="{0:dd-MMMM-yyyy hh:mm:ss tt}" />
						<asp:BoundField HeaderText="Location" DataField="Location" SortExpression="Location" />
						<asp:TemplateField HeaderText="Students">
							<ItemTemplate>
								<aspire:AspireLinkButton CausesValidation="false" runat="server" Text="<%#Item.DisciplineCasesStudents.Count %>" CommandName="Students" EncryptedCommandArgument="<%# this.Eval(nameof(DisciplineCas.DisciplineCaseID)) %>" />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Actions">
							<ItemTemplate>
								<aspire:AspireHyperLinkButton runat="server" CausesValidation="False" ButtonType="OnlyIcon" Glyphicon="plus" Text="Add Students" NavigateUrl="<%# DisciplineCasesStudents.GetPageUrl((int?)this.Eval(nameof(DisciplineCas.DisciplineCaseID)),null,false)%>" />
								<aspire:AspireLinkButton runat="server" CommandName="Delete" CausesValidation="False" Glyphicon="remove" EncryptedCommandArgument="<%# this.Eval(nameof(DisciplineCas.DisciplineCaseID)) %>" ConfirmMessage="Are you sure you want to delete?" />
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
					<SelectedRowStyle Font-Bold="True" />
				</aspire:AspireGridView>
			</div>
			<aspire:AspireModal runat="server" ID="modelDisciplineCasesStudent" ModalSize="Large">
				<HeaderTemplate>
					Student(s) Discipline Cases
				</HeaderTemplate>
				<BodyTemplate>
					<asp:Repeater runat="server" ID="rptDisciplineCase" ItemType="Aspire.Model.Entities.DisciplineCas">
						<ItemTemplate>
							<table class="table table-bordered table-condensed">
								<tr>
									<th>Case File No.</th>
									<th>Date</th>
									<th>Location</th>
								</tr>
								<tr>
									<td><%# Item.CaseFileNo %></td>
									<td><%# Item.CaseDate.ToString("dd-MMMM-yyyy hh:ss:mm tt") %></td>
									<td><%# Item.Location %></td>
								</tr>
							</table>
						</ItemTemplate>
					</asp:Repeater>
					<p></p>
					<aspire:AspireGridView runat="server" ID="gvDisciplineCasesStudent" AutoGenerateColumns="False" OnRowCommand="gvDisciplineCasesStudent_OnRowCommand">
						<Columns>
							<asp:TemplateField HeaderText="#">
								<ItemTemplate>
									<%#Container.DataItemIndex+1 %>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField HeaderText="Enrollment" DataField="Enrollment" />
							<asp:BoundField HeaderText="Name" DataField="Name" />
							<asp:BoundField HeaderText="Program" DataField="ProgramAlias" />
							<asp:BoundField HeaderText="Mobile" DataField="Mobile" />
							<asp:TemplateField HeaderText="Offence Activities">
								<ItemTemplate>
									<%# string.Join(", ",((List<string>)this.Eval("DisciplineCaseStudentActivities")).Select(s=>s)) %>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField HeaderText="Student Statement" DataField="StudentStatement" />
							<asp:BoundField HeaderText="Remarks Type(s)" DataField="RemarksTypeEnumFullName" />
							<asp:BoundField HeaderText="Remarks" DataField="Remarks" />
							<asp:TemplateField>
								<ItemTemplate>
									<aspire:AspireHyperLinkButton runat="server" CausesValidation="False" ButtonType="OnlyIcon" Glyphicon="edit" NavigateUrl="<%# DisciplineCasesStudents.GetPageUrl((int?)this.Eval(nameof(DisciplineCas.DisciplineCaseID)),(int?)this.Eval(nameof(DisciplineCasesStudent.DisciplineCasesStudentID)),false)%>" />
									<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Delete" Glyphicon="remove" EncryptedCommandArgument="<%# this.Eval(nameof(DisciplineCasesStudent.DisciplineCasesStudentID))%>" ConfirmMessage="Are you sure you want to delete?" />
									<aspire:AspireHyperLinkButton runat="server" CausesValidation="False" ButtonType="OnlyIcon" Glyphicon="plus" Text="Remarks" NavigateUrl="<%# DisciplineCasesStudents.GetPageUrl((int?)this.Eval(nameof(DisciplineCas.DisciplineCaseID)),(int?)this.Eval(nameof(DisciplineCasesStudent.DisciplineCasesStudentID)),true)%>" />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</aspire:AspireGridView>
				</BodyTemplate>
				<FooterTemplate>
					<aspire:AspireModalCloseButton runat="server" ButtonType="FooterCancelButton">Cancel</aspire:AspireModalCloseButton>
				</FooterTemplate>
			</aspire:AspireModal>
		</div>
	</div>
</asp:Content>
