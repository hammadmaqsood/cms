﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="UpdateRegistrationNos.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.UpdateRegistrationNos" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<aspire:AspireButton runat="server" ID="btnUpload" ButtonType="Default" Text="Upload File" CausesValidation="false" OnClick="btnUpload_Click" />
	<asp:Panel runat="server" ID="mainPanel">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Oledb Provider:" AssociatedControlID="ddlOledbProvider" />
					<aspire:AspireDropDownList runat="server" ID="ddlOledbProvider" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlOledbProvider_SelectedIndexChanged" />
				</div>
			</div>
			<div class="col-md-4" runat="server" id="divSheetName">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Sheet Name:" AssociatedControlID="ddlSheetName" />
					<aspire:AspireDropDownList runat="server" ID="ddlSheetName" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlSheetName_SelectedIndexChanged" />
				</div>
			</div>
			<div class="col-md-4" runat="server" id="divHDR">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Column Header:" AssociatedControlID="ddlHDR" />
					<aspire:AspireDropDownList runat="server" ID="ddlHDR" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlHDR_SelectedIndexChanged" />
				</div>
			</div>
			<div class="col-md-4" runat="server" id="divIMEX">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="IMEX:" AssociatedControlID="ddlIMEX" />
					<aspire:AspireDropDownList runat="server" ID="ddlIMEX" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlIMEX_SelectedIndexChanged" />
				</div>
			</div>
			<div class="col-md-4" runat="server" id="divEnrollmentColumnName">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Enrollment Column:" AssociatedControlID="ddlEnrollmentColumnName" />
					<aspire:AspireDropDownList runat="server" ID="ddlEnrollmentColumnName" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlEnrollmentColumnName" ErrorMessage="This field is required." ValidationGroup="Execute" />
				</div>
			</div>
			<div class="col-md-4" runat="server" id="divRegistrationNoColumnName">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Registration No. Column:" AssociatedControlID="ddlRegistrationNoColumnName" />
					<aspire:AspireDropDownList runat="server" ID="ddlRegistrationNoColumnName" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlRegistrationNoColumnName" ErrorMessage="This field is required." ValidationGroup="Execute" />
				</div>
			</div>
			<div class="col-md-4" runat="server" id="divNameColumnName">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Name Column:" AssociatedControlID="ddlNameColumnName" />
					<aspire:AspireDropDownList runat="server" ID="ddlNameColumnName" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlNameColumnName" ErrorMessage="This field is required." ValidationGroup="Execute" />
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" AssociatedControlID="btnExecute" Text="&nbsp;" />
					<div>
						<aspire:AspireButton runat="server" ID="btnExecute" OnClientClick="return AspireModal.showProgressModal();" ValidationGroup="Execute" Text="Update Registration No." OnClick="btnExecute_Click" />
						<aspire:AspireHyperLinkButton runat="server" NavigateUrl="UpdateRegistrationNos.aspx" Text="Cancel" />
					</div>
				</div>
			</div>
		</div>
		<aspire:AspireGridView runat="server" ID="gvExcelData" AutoGenerateColumns="true" AllowSorting="false" AllowPaging="false">
			<Columns>
				<asp:TemplateField HeaderText="#">
					<ItemTemplate>
						<%#: Container.DataItemIndex + 1 %>
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
	</asp:Panel>
	<aspire:AspireModal runat="server" ID="modalUpload" HeaderText="Upload File">
		<BodyTemplate>
			<div class="form-group" runat="server" id="divFileUpload">
				<aspire:AspireLabel runat="server" Text="Excel File:" AssociatedControlID="fileUpload" />
				<aspire:AspireFileUpload runat="server" ID="fileUpload" AllowedFileTypes=".xls,.xlsx" MaxFileSize="20971520" MaxNumberOfFiles="1" />
				<aspire:AspireFileUploadValidator runat="server" AllowNull="false" AspireFileUploadID="fileUpload" ValidationGroup="File" RequiredErrorMessage="File is not yet uploaded." UploadingInProgressMessage="File upload is in progress." />
			</div>
		</BodyTemplate>
		<FooterTemplate>
			<aspire:AspireButton ButtonType="Primary" runat="server" ID="btnDisplay" Text="Display" ValidationGroup="File" OnClick="btnDisplay_Click" />
			<aspire:AspireButton runat="server" ButtonType="Default" ID="btnCancel" Text="Cancel" CausesValidation="false" OnClick="btnCancel_Click" />
		</FooterTemplate>
	</aspire:AspireModal>
	<aspire:AspireModal runat="server" ID="modalResult" HeaderText="Result Status" Visible="true" ModalSize="Large">
		<BodyTemplate>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Details:" AssociatedControlID="tbUpdateDetails" />
				<aspire:AspireTextBox runat="server" Wrap="false" TextMode="MultiLine" ReadOnly="true" ID="tbUpdateDetails" MaxLength="0" Rows="10" />
			</div>
		</BodyTemplate>
		<FooterTemplate>
			<aspire:AspireModalCloseButton runat="server">Close</aspire:AspireModalCloseButton>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
