﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="DisciplineCasesStudents.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.DisciplineCasesStudents" %>

<%@ Import Namespace="Aspire.Web.Sys.Staff.Students" %>
<%@ Import Namespace="Aspire.Model.Entities" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<style type="text/css">
		.removeMargin {
			margin: 0px !important;
		}
	</style>
	<div class="col-md-12">
		<aspire:AspireButton runat="server" ButtonType="Success" Text="Add Student" ID="btnAddStudent" Glyphicon="plus" CausesValidation="False" OnClick="btnAddStudent_OnClick" />
		<aspire:AspireHyperLinkButton runat="server" ID="hplBtnDisciplineCases" ButtonType="Primary" Text="Discipline Cases" NavigateUrl="<%# DisciplineCases.GetPageUrl(this.DisciplineCaseID) %>" />
		<p></p>
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title">Discipline Case</h3>
			</div>
			<div class="panel-body">
				<div class="form-horizontal">
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Case File No:" AssociatedControlID="lblCaseFileNo" />
							<aspire:AspireLabel runat="server" ID="lblCaseFileNo" />
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Case Date:" AssociatedControlID="lblCaseDate" />
							<aspire:AspireLabel runat="server" ID="lblCaseDate" />
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Location:" AssociatedControlID="lblLocation" />
							<aspire:AspireLabel runat="server" ID="lblLocation" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><span runat="server" id="spHeadingDisciplineCasesStudent">Add</span> Student in Discipline Case</h3>
			</div>
			<div class="panel-body">
				<asp:UpdatePanel runat="server">
					<ContentTemplate>
						<div class="form-horizontal">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" CssClass="col-md-2" />
								<div class="col-md-3">
									<aspire:AspireTextBox runat="server" ID="tbEnrollment" ValidationGroup="DisciplineCaseStudent" MaxLength="15" OnTextChanged="tbEnrollment_OnTextChanged" AutoPostBack="True" />
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbEnrollment" ErrorMessage="This field is required." ValidationGroup="DisciplineCaseStudent" />
								</div>
							</div>
							<div runat="server" id="dvInfoStudent">
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="lblName" CssClass="col-md-2" />
									<div class="form-control-static col-md-3">
										<aspire:AspireLabel runat="server" ID="lblName" />
									</div>
								</div>
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="lblProgram" CssClass="col-md-2" />
									<div class="form-control-static col-md-3">
										<aspire:AspireLabel runat="server" ID="lblProgram" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Mobile:" AssociatedControlID="tbMobile" CssClass="col-md-2" />
								<div class="col-md-3">
									<aspire:AspireTextBox runat="server" ID="tbMobile" ValidationGroup="DisciplineCaseStudent" data-inputmask="'alias': 'mobile'" />
									<aspire:AspireStringValidator RequiredErrorMessage="This field is required." runat="server" ValidationGroup="DisciplineCaseStudent" ControlToValidate="tbMobile" ValidationExpression="Mobile" AllowNull="False" />
								</div>
							</div>
							<div class="form-group" runat="server" id="dvOffenceActivities">
								<aspire:AspireLabel runat="server" Text="Offence Activities:" AssociatedControlID="cblOffenceActivities" CssClass="col-md-2" />
								<div class="col-md-3">
									<aspire:AspireCheckBoxList runat="server" ID="cblOffenceActivities" RepeatDirection="Horizontal" RepeatLayout="Flow" />
									<div class="form-control-static">
										<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="cblOffenceActivities" ValidationGroup="DisciplineCaseStudent" ErrorMessage="This field is required." />
									</div>
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Student Statement:" AssociatedControlID="tbStudentStatement" CssClass="col-md-2" />
								<div class="col-md-3">
									<aspire:AspireTextBox runat="server" ID="tbStudentStatement" TextMode="MultiLine" />
								</div>
							</div>
							<div runat="server" id="dvRemarks">
								<table class="table table-bordered table-condensed">
									<tr style="background-color: antiquewhite">
										<td>
											<h5 class="text-center">Remarks</h5>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group">
												<aspire:AspireLabel runat="server" Text="Remarks Type:" AssociatedControlID="cblRemarksType" CssClass="col-md-2" />
												<div class="col-md-3">
													<aspire:AspireCheckBoxList runat="server" ID="cblRemarksType" RepeatLayout="Flow" RepeatDirection="Horizontal" />
												</div>
												<div class="form-control-static col-md-2">
													<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="cblRemarksType" ErrorMessage="This field is required." ValidationGroup="DisciplineCaseStudent" />
												</div>
											</div>
											<div class="form-group">
												<aspire:AspireLabel runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" CssClass="col-md-2" />
												<div class="col-md-3">
													<aspire:AspireTextBox runat="server" ID="tbRemarks" TextMode="MultiLine" />
													<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbRemarks" ErrorMessage="This field is required." ValidationGroup="DisciplineCaseStudent" />
												</div>
											</div>
											<div class="form-group">
												<aspire:AspireLabel runat="server" Text="Same For Other Students:" AssociatedControlID="rblSameForOthers" CssClass="col-md-2" />
												<div class="col-md-3">
													<aspire:AspireRadioButtonList runat="server" ID="rblSameForOthers" RepeatLayout="Flow" RepeatDirection="Horizontal" />
												</div>
											</div>
										</td>
									</tr>
								</table>
							</div>
							<div class="form-group">
								<div class="col-md-offset-2 col-md-3">
									<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" ValidationGroup="DisciplineCaseStudent" OnClick="btnSave_OnClick" />
									<aspire:AspireHyperLink runat="server" Text="Cancel" ID="hplCancel" NavigateUrl="<%# DisciplineCasesStudents.GetPageUrl(this.DisciplineCaseID,null) %>" />
								</div>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Student(s) Discipline Cases</h3>
			</div>
			<aspire:AspireGridView runat="server" ID="gvDisciplineCasesStudent" AutoGenerateColumns="False" OnRowCommand="gvDisciplineCasesStudent_OnRowCommand" CssClass="table-condensed  removeMargin">
				<Columns>
					<asp:TemplateField HeaderText="#.">
						<ItemTemplate>
							<%#Container.DataItemIndex+1 %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField HeaderText="Enrollment" DataField="Enrollment" />
					<asp:BoundField HeaderText="Name" DataField="Name" />
					<asp:BoundField HeaderText="Program" DataField="ProgramAlias" />
					<asp:TemplateField HeaderText="Offence Activities">
						<ItemTemplate>
							<%# string.Join(", ",((List<string>)this.Eval("DisciplineCaseStudentActivities")).Select(s=>s)) %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField HeaderText="Mobile" DataField="Mobile" />
					<asp:BoundField HeaderText="Student Statement" DataField="StudentStatement" />
					<asp:BoundField HeaderText="Remarks Type(s)" DataField="RemarksTypeEnumFullName" />
					<asp:BoundField HeaderText="Remarks" DataField="Remarks" />
					<asp:TemplateField>
						<ItemTemplate>
							<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Edit" Glyphicon="edit" EncryptedCommandArgument="<%# this.Eval(nameof(DisciplineCasesStudent.DisciplineCasesStudentID))%>" />
							<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Delete" Glyphicon="remove" EncryptedCommandArgument="<%# this.Eval(nameof(DisciplineCasesStudent.DisciplineCasesStudentID))%>" ConfirmMessage="Are you sure you want to delete?" />
							<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Remarks" Glyphicon="plus" Text="Remarks" EncryptedCommandArgument="<%# this.Eval(nameof(DisciplineCasesStudent.DisciplineCasesStudentID))%>" />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</aspire:AspireGridView>
		</div>
	</div>
</asp:Content>
