﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Students
{
	[AspirePage("94B8F601-BB22-447A-BC20-D00C924163A4", UserTypes.Staff, "~/Sys/Staff/Students/SearchStudents.aspx", "Search Students", true, AspireModules.StudentInformation)]
	public partial class SearchStudents : StaffPage
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.StudentInformation.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_search);
		public override string PageTitle => "Search Students";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterFilter.FillSemesters(CommonListItems.All);
				this.ddlProgramFilter.FillPrograms(this.StaffIdentity.InstituteID, null, true, CommonListItems.All);
				this.ViewState.SetSortProperties(nameof(Model.Entities.Student.Enrollment), SortDirection.Ascending);
			}
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			this.GetSearchStudents(0);
		}

		protected void gvSearchStudent_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetSearchStudents(e.NewPageIndex);
		}

		protected void gvSearchStudent_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvSearchStudent.PageSize = e.NewPageSize;
			this.GetSearchStudents(0);
		}

		protected void gvSearchStudent_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetSearchStudents(0);
		}

		private void GetSearchStudents(int pageIndex)
		{
			var intakeSemesterID = this.ddlSemesterFilter.SelectedItem.Value.ToNullableShort();
			var enrollment = this.tbEnrollmentFilter.Text;
			var name = this.tbNameFilter.Text;
			var programID = this.ddlProgramFilter.SelectedValue.ToNullableInt();
			var cnic = this.tbCnicFilter.Text;
			var registrationNo = this.tbRegistrationNoFilter.Text.ToNullableInt();

			var result = BL.Core.StudentInformation.Staff.StudentSearch.SearchStudents(enrollment, registrationNo, name, intakeSemesterID, programID, cnic, pageIndex, this.gvSearchStudent.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.StaffIdentity.LoginSessionGuid);
			this.gvSearchStudent.DataBind(result.Students, pageIndex, result.VirtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}
	}
}