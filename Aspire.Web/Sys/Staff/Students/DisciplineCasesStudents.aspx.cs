﻿using Aspire.BL.Core.StudentInformation.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Students
{
	[AspirePage("FBFC0332-ECDA-4935-8AE7-F5F173C362BC", UserTypes.Staff, "~/Sys/Staff/Students/DisciplineCasesStudents.aspx", "Discipline Case Students", true, AspireModules.StudentInformation)]
	public partial class DisciplineCasesStudents : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_bookmark);
		public override string PageTitle => "Student Discipline Case";

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.StudentInformation.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public static string GetPageUrl(int? disciplineCaseID, int? disciplineCasesStudentID, bool forRemarks = false)
		{
			return GetAspirePageAttribute<DisciplineCasesStudents>().PageUrl.AttachQueryParams(nameof(DisciplineCas.DisciplineCaseID), disciplineCaseID, nameof(DisciplineCasesStudent.DisciplineCasesStudentID), disciplineCasesStudentID, nameof(forRemarks), forRemarks);
		}

		public int? DisciplineCaseID => this.Request.GetParameterValue<int>("DisciplineCaseID");
		public int? DisciplineCasesStudentID => this.Request.GetParameterValue<int>("DisciplineCasesStudentID");
		public bool? ForRemarks => this.Request.GetParameterValue<bool>("forRemarks");

		public void Refresh(int? disciplineCaseID, int? disciplineCasesStudentID = null, bool forRemarks = false)
		{
			Redirect(GetPageUrl(disciplineCaseID, disciplineCasesStudentID, forRemarks));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.DisciplineCaseID == null)
					Redirect<DisciplineCases>();
				var disciplineCase = BL.Core.StudentInformation.Staff.DisciplineCase.GetDisciplineCase(this.DisciplineCaseID.Value, this.StaffIdentity.LoginSessionGuid);
				this.lblCaseFileNo.Text = disciplineCase.CaseFileNo;
				this.lblCaseDate.Text = disciplineCase.CaseDate.ToString("dd-MMM-yyyy hh:mm:ss tt");
				this.lblLocation.Text = disciplineCase.Location;
				this.DisplayDisciplineCasesStudent(this.DisciplineCasesStudentID, this.ForRemarks.Value);
				var disciplineCaseStudents = BL.Core.StudentInformation.Staff.DisciplineCase.GetDisciplineCasesStudents(this.DisciplineCaseID.Value, this.StaffIdentity.LoginSessionGuid);
				this.gvDisciplineCasesStudent.DataBind(disciplineCaseStudents);
				this.hplBtnDisciplineCases.DataBind();
				this.hplCancel.DataBind();
				//this.panelStudentDisciplineCase.Visible = false;
				//var enrollment = this.Request.GetParameterValue("Enrollment");
				//this.tbEnrollment.Enrollment = enrollment;
				//if (this.Request.GetParameterValue<bool>("Search") == true)
				//	this.tbEnrollment_Search(null, null);
			}
		}

		private void DisplayDisciplineCasesStudent(int? disciplineCasesStudentID, bool forRemarks = false)
		{
			var disciplineCaseActivities = BL.Core.StudentInformation.Staff.DisciplineCase.GetDisciplineCaseActivities(this.StaffIdentity.LoginSessionGuid);
			this.dvOffenceActivities.Visible = disciplineCaseActivities.Any();
			this.btnSave.Enabled = disciplineCaseActivities.Any();
			if (!disciplineCaseActivities.Any())
				this.AddInfoAlert("Offence activities are missing. First add the offence activities.");
			this.cblOffenceActivities.DataBind(disciplineCaseActivities);
			this.cblRemarksType.FillEnums<DisciplineCasesStudent.RemarksTypes>();
			this.rblSameForOthers.FillYesNo().SetSelectedValueNo();
			this.dvRemarks.Visible = false;
			this.dvInfoStudent.Visible = false;
			if (disciplineCasesStudentID == null && forRemarks)
				throw new InvalidOperationException();
			this.tbMobile.Enabled = this.cblOffenceActivities.Enabled = this.tbStudentStatement.Enabled = !forRemarks;
			if (disciplineCasesStudentID == null && !forRemarks)
			{
				this.tbEnrollment.Text = null;
				this.tbEnrollment.Enabled = true;
				this.lblProgram.Text = null;
				this.tbMobile.Text = null;
				this.cblOffenceActivities.ClearSelection();
				this.tbStudentStatement.Text = null;
				this.cblRemarksType.ClearSelection();
				this.tbRemarks.Text = null;
				this.btnSave.Text = @"Add";
				this.spHeadingDisciplineCasesStudent.InnerText = "Add";
			}
			else
			{
				var disciplineCasesStudent = BL.Core.StudentInformation.Staff.DisciplineCase.GetDisciplineCasesStudent(disciplineCasesStudentID.Value, forRemarks, this.StaffIdentity.LoginSessionGuid);
				this.tbEnrollment.Text = disciplineCasesStudent.Student.Enrollment;
				this.tbEnrollment.Enabled = false;
				this.lblName.Text = disciplineCasesStudent.Student.Name;
				this.lblProgram.Text = disciplineCasesStudent.Student.AdmissionOpenProgram.Program.ProgramAlias;
				this.dvInfoStudent.Visible = true;
				this.tbMobile.Text = disciplineCasesStudent.Mobile;

				disciplineCasesStudent.DisciplineCaseStudentActivities.ForEach(dcsa => this.cblOffenceActivities.Items.FindByValue(dcsa.DisciplineCaseActivityID.ToString()).Selected = true);
				this.cblOffenceActivities.Enabled = string.IsNullOrEmpty(disciplineCasesStudent.Remarks);
				this.tbStudentStatement.Text = disciplineCasesStudent.StudentsStatement;
				var remarksTypes = disciplineCasesStudent.RemarksTypeEnum?.GetFlags();
				if (disciplineCasesStudent.RemarksTypeEnum != null)
					this.SetRemarksTypeEnumsValues(this.cblRemarksType, remarksTypes);
				this.tbRemarks.Text = disciplineCasesStudent.Remarks;
				this.dvRemarks.Visible = forRemarks;
				this.btnSave.Text = !forRemarks ? @"Save" : @"Save Remarks";
				this.spHeadingDisciplineCasesStudent.InnerText = !forRemarks ? @"Edit" : @"Remarks of";
			}
			this.ViewState[nameof(DisciplineCasesStudent.DisciplineCasesStudentID)] = disciplineCasesStudentID;
			this.ViewState["forRemarks"] = forRemarks;
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var disciplineCasesStudentID = (int?)this.ViewState[nameof(DisciplineCasesStudent.DisciplineCasesStudentID)];
			var forRemarks = (bool?)this.ViewState["forRemarks"] ?? false;
			var enrollment = this.tbEnrollment.Text;
			var mobile = this.tbMobile.Text;
			var offenceActivities = this.cblOffenceActivities.Items.Cast<ListItem>().Where(oa => oa.Selected).Select(oa => oa.Value.ToInt()).ToList();
			var studentStatement = this.tbStudentStatement.Text;
			var remarksTypesEnum = this.cblRemarksType.GetSelectedEnumValues<DisciplineCasesStudent.RemarksTypes>().Combine();
			var remarks = this.tbRemarks.Text;
			var sameForOthers = this.rblSameForOthers.SelectedValue.ToBoolean();
			if (!offenceActivities.Any())
				throw new InvalidOperationException("Offence activities are required in form.");
			if (disciplineCasesStudentID == null && forRemarks)
				throw new InvalidOperationException();
			if (disciplineCasesStudentID == null && !forRemarks)
			{
				var result = BL.Core.StudentInformation.Staff.DisciplineCase.AddDisciplineCasesStudent(this.DisciplineCaseID.Value, enrollment, mobile, offenceActivities, studentStatement, this.StaffIdentity.LoginSessionGuid);
				switch (result)
				{
					case DisciplineCase.AddDisciplineCasesStudentStatues.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.Refresh(this.DisciplineCaseID);
						break;
					case DisciplineCase.AddDisciplineCasesStudentStatues.AlreadyExists:
						this.AddErrorAlert("Student has already exists in selected discipline case.");
						break;
					case DisciplineCase.AddDisciplineCasesStudentStatues.Success:
						this.AddSuccessMessageHasBeenAdded("Student");
						this.Refresh(this.DisciplineCaseID);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var result = BL.Core.StudentInformation.Staff.DisciplineCase.UpdateDisciplineCasesStudent(disciplineCasesStudentID.Value, mobile, studentStatement, offenceActivities, remarksTypesEnum, remarks, sameForOthers, forRemarks, this.StaffIdentity.LoginSessionGuid);
				switch (result)
				{
					case DisciplineCase.UpdateDisciplineCasesStudentStatues.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<DisciplineCases>();
						break;
					case DisciplineCase.UpdateDisciplineCasesStudentStatues.Success:
						if (forRemarks)
							this.AddSuccessAlert("Student remarks has been updated.");
						else
							this.AddSuccessMessageHasBeenUpdated("Student");
						this.Refresh(this.DisciplineCaseID, disciplineCasesStudentID, forRemarks);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		protected void gvDisciplineCasesStudent_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayDisciplineCasesStudent(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var result = BL.Core.StudentInformation.Staff.DisciplineCase.DeleteDisciplineCasesStudent(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					switch (result)
					{
						case DisciplineCase.DeleteDisciplineCasesStudentStatues.NoRecordFound:
							this.AddNoRecordFoundAlert();
							Redirect<DisciplineCasesStudents>(nameof(DisciplineCas.DisciplineCaseID), this.DisciplineCaseID);
							break;
						case DisciplineCase.DeleteDisciplineCasesStudentStatues.RemarksIsExists:
							this.AddErrorAlert("Remarks of student exists record can't be delete.");
							break;
						case DisciplineCase.DeleteDisciplineCasesStudentStatues.Success:
							this.AddSuccessMessageHasBeenDeleted("Student");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.Refresh(this.DisciplineCaseID);
					break;
				case "Remarks":
					var disciplineCasesStudentID = e.DecryptedCommandArgumentToInt();
					this.DisplayDisciplineCasesStudent(disciplineCasesStudentID, true);
					break;
			}
		}

		protected void btnAddStudent_OnClick(object sender, EventArgs e)
		{
			this.DisplayDisciplineCasesStudent(null);
		}

		private void SetRemarksTypeEnumsValues(Aspire.Lib.WebControls.CheckBoxList cblList, IEnumerable<DisciplineCasesStudent.RemarksTypes> enumTypes)
		{
			var cblItems = cblList.Items.Cast<ListItem>();
			foreach (var cblItem in cblItems)
				cblItem.Selected = false;
			if (enumTypes.Any())
				foreach (var enumType in enumTypes)
				{
					cblList.Items.Cast<ListItem>().Single(i => i.Value.ToEnum<DisciplineCasesStudent.RemarksTypes>() == enumType).Selected = true;
				}
		}

		protected void tbEnrollment_OnTextChanged(object sender, EventArgs e)
		{
			var enrollment = this.tbEnrollment.Text;
			var student = BL.Core.StudentInformation.Staff.DisciplineCase.GetStudent(enrollment, this.StaffIdentity.LoginSessionGuid);
			if (string.IsNullOrEmpty(enrollment))
			{
				this.lblProgram.Text = null;
				this.tbMobile.Text = null;
				this.dvInfoStudent.Visible = false;
				return;
			}
			if (student == null)
			{
				this.AddNoRecordFoundAlert();
				this.Refresh(this.DisciplineCaseID);
				return;
			}
			this.lblName.Text = student.Name;
			this.lblProgram.Text = student.AdmissionOpenProgram.Program.ProgramAlias;
			this.tbMobile.Text = student.Mobile;
			this.dvInfoStudent.Visible = true;
		}
	}
}