﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Students
{
	[AspirePage("d1c0a267-c6d8-4a07-9071-65be36620343", Model.Entities.Common.UserTypes.Staff, "~/Sys/Staff/Students/UpdateRegistrationNos.aspx", "Update Registration Nos.", true, Model.Entities.Common.AspireModules.StudentInformation)]
	public partial class UpdateRegistrationNos : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.StudentInformation.Module,new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};

		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_id_badge);

		public override string PageTitle => "Update Registration Nos.";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.mainPanel.Visible = false;
			}

		}

		protected void btnUpload_Click(object sender, EventArgs e)
		{
			this.modalUpload.Visible = true;
			this.modalUpload.Show();
			this.btnUpload.Visible = false;
		}

		protected void btnDisplay_Click(object sender, EventArgs e)
		{
			this.mainPanel.Visible = true;
			var providers = Enum.GetValues(typeof(Lib.Helpers.ExcelFileHelper.OledbProviders)).Cast<Lib.Helpers.ExcelFileHelper.OledbProviders>()
				.Select(p => new ListItem(p.GetProviderName(), p.ToString()));
			this.ddlOledbProvider.DataBind(providers, CommonListItems.Select);
			this.ddlOledbProvider_SelectedIndexChanged(null, null);
		}

		protected void btnCancel_Click(object sender, EventArgs e)
		{
			Redirect<UpdateRegistrationNos>();
		}

		protected void ddlOledbProvider_SelectedIndexChanged(object sender, EventArgs e)
		{
			var provider = this.ddlOledbProvider.SelectedValue.ToNullableEnum<Lib.Helpers.ExcelFileHelper.OledbProviders>();
			if (provider == null)
			{
				this.divSheetName.Visible = false;
				this.ddlSheetName.ClearSelection();
				this.divHDR.Visible = false;
				this.divIMEX.Visible = false;
				this.divEnrollmentColumnName.Visible = false;
				this.divRegistrationNoColumnName.Visible = false;
				this.divNameColumnName.Visible = false;
				this.btnExecute.Visible = false;
			}
			else
			{
				this.divSheetName.Visible = true;
				var sheetNames = BL.Core.FileServer.TemporaryFiles.GetExcelSheetNames(provider.Value, this.fileUpload.UploadedFiles[0].Instance, this.fileUpload.UploadedFiles[0].FileID.ToGuid(), this.AspireIdentity.LoginSessionGuid);
				this.divSheetName.Visible = true;
				this.ddlSheetName.DataBind(sheetNames);
				this.ddlSheetName_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlSheetName_SelectedIndexChanged(object sender, EventArgs e)
		{
			var sheetName = this.ddlSheetName.SelectedValue.ToNullIfEmpty();
			var provider = this.ddlOledbProvider.SelectedValue.ToNullableEnum<Lib.Helpers.ExcelFileHelper.OledbProviders>();
			if (provider == null || sheetName == null)
			{
				this.divHDR.Visible = false;
				this.divIMEX.Visible = false;
				this.divEnrollmentColumnName.Visible = false;
				this.ddlEnrollmentColumnName.Items.Clear();
				this.divRegistrationNoColumnName.Visible = false;
				this.ddlRegistrationNoColumnName.Items.Clear();
				this.divNameColumnName.Visible = false;
				this.ddlNameColumnName.Items.Clear();
			}
			else
			{
				this.divHDR.Visible = true;
				this.divIMEX.Visible = true;
				this.ddlHDR.FillYesNo().SetBooleanValue(true);
				var numbers = Enumerable.Range(0, 11).Select(i => i.ToString()).ToList();
				this.ddlIMEX.DataBind(numbers).SelectedValue = "1";
				this.ddlHDR_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlHDR_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlIMEX_SelectedIndexChanged(null, null);
		}

		private DataTable GetData()
		{
			var sheetName = this.ddlSheetName.SelectedValue;
			var provider = this.ddlOledbProvider.SelectedValue.ToEnum<Lib.Helpers.ExcelFileHelper.OledbProviders>();
			var hdr = this.ddlHDR.SelectedValue.ToBoolean();
			var imex = this.ddlIMEX.SelectedValue.ToUInt();
			return BL.Core.FileServer.TemporaryFiles.GetExcelSheetData(provider, sheetName, hdr, imex, this.fileUpload.UploadedFiles[0].Instance, this.fileUpload.UploadedFiles[0].FileID.ToGuid(), this.AspireIdentity.LoginSessionGuid);
		}

		protected void ddlIMEX_SelectedIndexChanged(object sender, EventArgs e)
		{
			using (var dataTable = this.GetData())
			{
				this.ddlEnrollmentColumnName.Items.Clear();
				this.ddlRegistrationNoColumnName.Items.Clear();
				this.ddlNameColumnName.Items.Clear();
				foreach (DataColumn column in dataTable.Columns)
				{
					this.ddlEnrollmentColumnName.Items.Add(column.ColumnName);
					this.ddlRegistrationNoColumnName.Items.Add(column.ColumnName);
					this.ddlNameColumnName.Items.Add(column.ColumnName);
				}
				this.ddlEnrollmentColumnName.AddStaticListItem(CommonListItems.Select);
				this.ddlRegistrationNoColumnName.AddStaticListItem(CommonListItems.Select);
				this.ddlNameColumnName.AddStaticListItem(CommonListItems.Select);
				this.gvExcelData.DataBind(dataTable);
			}

			this.ddlEnrollmentColumnName.SetSelectedValueIfExists("ENROLLMENT");
			this.ddlEnrollmentColumnName.SetSelectedValueIfExists("Enrollment");
			this.ddlRegistrationNoColumnName.SetSelectedValueIfExists("REGISTRATION");
			this.ddlRegistrationNoColumnName.SetSelectedValueIfExists("Registration");
			this.ddlRegistrationNoColumnName.SetSelectedValueIfExists("REG NO");
			this.ddlRegistrationNoColumnName.SetSelectedValueIfExists("REG");
			this.ddlRegistrationNoColumnName.SetSelectedValueIfExists("Registration No.");
			this.ddlNameColumnName.SetSelectedValueIfExists("NAME");
			this.ddlNameColumnName.SetSelectedValueIfExists("Name");
			this.divEnrollmentColumnName.Visible = this.ddlEnrollmentColumnName.Items.Count > 0;
			this.divRegistrationNoColumnName.Visible = this.ddlRegistrationNoColumnName.Items.Count > 0;
			this.divNameColumnName.Visible = this.ddlNameColumnName.Items.Count > 0;
			this.btnExecute.Visible = this.divEnrollmentColumnName.Visible && this.divRegistrationNoColumnName.Visible && this.divNameColumnName.Visible;
		}

		protected void btnExecute_Click(object sender, EventArgs e)
		{
			using (var dataTable = this.GetData())
			{
				var registrationColumnName = this.ddlRegistrationNoColumnName.SelectedValue;
				var enrollmentColumnName = this.ddlEnrollmentColumnName.SelectedValue;
				var nameColumnName = this.ddlNameColumnName.SelectedValue;
				var data = dataTable.AsEnumerable().Select(r => new
				{
					RegistrationNo = r.IsNull(registrationColumnName) ? null : r[registrationColumnName]?.ToString().ToNullableInt(),
					Enrollment = r.IsNull(enrollmentColumnName) ? null : r[enrollmentColumnName]?.ToString(),
					Name = r.IsNull(nameColumnName) ? null : r[nameColumnName]?.ToString().TrimAndMakeItNullIfEmpty()
				}).ToList();

				if (data.Any(d => d.RegistrationNo == null || d.Enrollment == null || d.Name == null))
				{
					this.AddErrorAlert("Data is not valid.");
					return;
				}
				var registrationInfos = data.Select(d => new BL.Core.StudentInformation.Staff.StudentProfile.RegistrationInfo
				{
					Enrollment = d.Enrollment,
					RegistrationNo = d.RegistrationNo ?? throw new InvalidOperationException(),
					Name = d.Name,
					Success = false,
					Message = null
				}).ToList();
				BL.Core.StudentInformation.Staff.StudentProfile.UpdateRegistrationNos(registrationInfos, this.StaffIdentity.LoginSessionGuid);

				var result = new StringBuilder();
				registrationInfos.ForEach(i =>
				{
					if (i.Success)
					{
						if (i.Message != null)
							result.AppendLine($"Warning: {i.Message}");
					}
					else
					{
						if (i.Message != null)
							result.AppendLine($"Error: {i.Message}");
						else
							result.AppendLine($"Error: {i.Enrollment} failed.");
					}
				});
				this.modalResult.Visible = true;
				this.modalResult.Show();
				this.tbUpdateDetails.Text = result.ToString();
			}
		}
	}
}