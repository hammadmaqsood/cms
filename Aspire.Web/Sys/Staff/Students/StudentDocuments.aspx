﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="StudentDocuments.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.StudentDocuments" %>

<%@ Register Src="~/Sys/Common/StudentInformation/StudentDocuments.ascx" TagPrefix="uc1" TagName="StudentDocuments" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
			<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="Search" OnSearch="tbEnrollment_Search" />
		</div>
	</div>
	<uc1:StudentDocuments runat="server" ID="studentDocuments" />
</asp:Content>
