﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="AcademicRecords.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.AcademicRecords" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Intake Semester:" AssociatedControlID="ddlIntakeSemesterID" />
			<aspire:AspireDropDownList runat="server" ID="ddlIntakeSemesterID" AutoPostBack="True" ValidationGroup="Filters" OnSelectedIndexChanged="ddlIntakeSemesterID_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramID" AutoPostBack="True" ValidationGroup="Filters" OnSelectedIndexChanged="ddlProgramID_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Degrees:" AssociatedControlID="cblDegreeTypes" />
			<aspire:AspireCheckBoxList runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" ID="cblDegreeTypes" AutoPostBack="True" ValidationGroup="Filters" OnSelectedIndexChanged="cblDegreeTypes_OnSelectedIndexChanged" />
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvStudents" AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvStudents_OnSorting" OnRowDataBound="gvStudents_OnRowDataBound">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate>
					<%#: Container.DataItemIndex+1 %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="IntakeSemester" HeaderText="Intake" />
			<asp:BoundField DataField="ProgramAlias" HeaderText="Program" />
			<asp:HyperLinkField DataTextField="Enrollment" Target="_blank" HeaderText="Enrollment" SortExpression="Enrollment" DataNavigateUrlFields="StudentProfileURL" DataNavigateUrlFormatString="{0}" />
			<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
			<asp:BoundField DataField="StudentStatus" HeaderText="Student Status" SortExpression="StudentStatus" />
			<asp:BoundField DataField="DegreeTypeFullName" HeaderText="Degree" SortExpression="DegreeTypeFullName" />
			<asp:BoundField DataField="Marks" HeaderText="Marks" ItemStyle-HorizontalAlign="Right" SortExpression="Marks" />
			<asp:BoundField DataField="IsCGPA" HeaderText="Is CGPA" SortExpression="IsCGPA" />
			<asp:BoundField DataField="StatusFullName" HeaderText="Status" SortExpression="StatusFullName" />
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
