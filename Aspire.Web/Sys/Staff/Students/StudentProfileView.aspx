﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="StudentProfileView.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.StudentProfileView" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
    <div class="form-inline">
        <div class="form-group">
            <aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
            <aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="Search" OnSearch="tbEnrollment_OnSearch" />
        </div>
    </div>
    <asp:Panel runat="server" ID="panelInfo">
        <p></p>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Personal Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group col-md-12 text-center">
                        <asp:Image runat="server" ID="imgPicture" Width="100px" CssClass="img-thumbnail" AlternateText="Photo" />
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Enrollment:" AssociatedControlID="lblEnrollment" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblEnrollment" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Registration No.:" AssociatedControlID="lblRegistrationNo" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblRegistrationNo" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Program:" AssociatedControlID="lblProgram" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblProgram" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Intake Semester:" AssociatedControlID="lblIntakeSemester" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblIntakeSemester" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Student Status:" AssociatedControlID="lblStudentStatus" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblStudentStatus" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Name:" AssociatedControlID="lblName" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblName" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Father Name:" AssociatedControlID="lblFatherName" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblFatherName" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Gender:" AssociatedControlID="lblGender" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblGender" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Category:" AssociatedControlID="lblCategory" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblCategory" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="University Email:" AssociatedControlID="lblUniversityEmail" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblUniversityEmail" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Personal Email:" AssociatedControlID="lblPersonalEmail" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblPersonalEmail" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Nationality:" AssociatedControlID="lblNationality" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblNationality" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Country:" AssociatedControlID="lblCountry" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblCountry" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="State/Province:" AssociatedControlID="lblProvince" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblProvince" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="District/City:" AssociatedControlID="lblDistrict" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblDistrict" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div runat="server" id="divTehsil" class="form-group col-md-6 ">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Tehsil:" AssociatedControlID="lblTehsil" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblTehsil" />
                            </div>
                        </div>
                        <div class="form-group col-md-6" runat="server" id="divDomicile">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Domicile:" AssociatedControlID="lblDomicile" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblDomicile" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6" runat="server">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Area Type:" AssociatedControlID="lblAreaType" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblAreaType" />
                            </div>
                        </div>
                        <div class="form-group col-md-6" runat="server">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Religion:" AssociatedControlID="lblReligion" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblReligion" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6" runat="server">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Next Of Kin:" AssociatedControlID="lblNextOfKin" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblNextOfKin" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Emergency Contact Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Contact Name:" AssociatedControlID="lblEmergencyContactName" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblEmergencyContactName" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Mobile:" AssociatedControlID="lblEmergencyMobile" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblEmergencyMobile" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Phone:" AssociatedControlID="lblEmergencyPhone" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblEmergencyPhone" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Relationship:" AssociatedControlID="lblEmergencyRelationship" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblEmergencyRelationship" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Address:" AssociatedControlID="lblEmergencyAddress" />
                            <div class="form-control-static col-md-8">
                                <aspire:AspireLabel runat="server" ID="lblEmergencyAddress" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
