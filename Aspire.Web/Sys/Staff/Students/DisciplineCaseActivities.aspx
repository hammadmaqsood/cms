﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="DisciplineCaseActivities.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.DisciplineCaseActivities" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<aspire:AspireButton runat="server" Text="Add Discipline Case Activity" ID="btnAddDisciplineCaseActivity" ButtonType="Success" Glyphicon="plus" OnClick="btnAddDisciplineCaseActivity_OnClick" CausesValidation="False" />
	<aspire:AspireGridView runat="server" Caption="Discipline Case Activities" AutoGenerateColumns="False" ID="gvDisciplineCaseActivities" OnRowCommand="gvDisciplineCaseActivities_OnRowCommand">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate>
					<%#: Container.DataItemIndex+1 %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="ActivityName" HeaderText="Activity Name" />
			<asp:BoundField DataField="FineAmount" HeaderText="Fine Amount" />
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument='<%# this.Eval("DisciplineCaseActivityID") %>' />
					<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%# this.Eval("DisciplineCaseActivityID") %>' ConfirmMessage="Are you sure want to delete?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<aspire:AspireModal runat="server" ID="modalAddEditDisciplineCaseActivity">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelDisciplineCaseActivity" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="modalAlert" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Activity Name:" CssClass="col-md-3" AssociatedControlID="tbActivityName" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbActivityName" ValidationGroup="Add" MaxLength="100" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbActivityName" ErrorMessage="This field is required." ValidationGroup="Add" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Fine Amount:" CssClass="col-md-3" AssociatedControlID="tbFineAmount" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbFineAmount" ValidationGroup="Add" />
								<aspire:AspireInt32Validator runat="server" ControlToValidate="tbFineAmount" AllowNull="True" InvalidRangeErrorMessage="Invalid Range." InvalidNumberErrorMessage="Invalid Number." ValidationGroup="Add" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton ButtonType="Primary" runat="server" ID="btnAddEditDisciplineCaseActivities" OnClick="bntAddEditDisciplineCaseActivities_OnClick" ValidationGroup="Add" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
