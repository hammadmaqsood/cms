﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="StudentInfoAndAcademicRecords.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.Reports.StudentInfoAndAcademicRecords" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-8">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Report Type:" AssociatedControlID="ddlReportType" />
				<aspire:AspireDropDownList runat="server" ID="ddlReportType" ValidationGroup="Parameters" AutoPostBack="True" OnSelectedIndexChanged="ddlReportType_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" ValidationGroup="Parameters" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" ValidationGroup="Parameters" />
			</div>
		</div>
		<div class="col-md-4" runat="server" id="divSemesterNo">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester No:" AssociatedControlID="ddlSemesterNo" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" ValidationGroup="Parameters" />
			</div>
		</div>
		<div class="col-md-4" runat="server" id="divSection">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSection" />
				<aspire:AspireDropDownList runat="server" ID="ddlSection" ValidationGroup="Parameters" />
			</div>
		</div>
		<div class="col-md-4" runat="server" id="divShift">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShift" />
				<aspire:AspireDropDownList runat="server" ID="ddlShift" ValidationGroup="Parameters" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="&nbsp;" AssociatedControlID="btnExport" />
				<div>
					<aspire:AspireButton ID="btnExport" ButtonType="Primary" runat="server" Text="Export To CSV" ValidationGroup="Parameters" Glyphicon="export" OnClick="btnExport_OnClick" />
				</div>
			</div>
		</div>
	</div>
</asp:Content>
