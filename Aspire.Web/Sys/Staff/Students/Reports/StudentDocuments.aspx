﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="StudentDocuments.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.Reports.StudentDocuments" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" AutoPostBack="true" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlSemesterIDFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlDepartmentIDFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" AutoPostBack="true" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlProgramIDFilter_SelectedIndexChanged" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Missing Documents:" AssociatedControlID="ddlDocumentType" />
				<aspire:AspireDropDownList runat="server" ID="ddlDocumentType" AutoPostBack="true" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlDocumentType_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Missing Page:" AssociatedControlID="ddlPage" />
				<aspire:AspireDropDownList runat="server" ID="ddlPage" AutoPostBack="true" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlPage_SelectedIndexChanged" />
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ItemType="Aspire.BL.Core.StudentInformation.Staff.StudentDocuments.Student" ID="gvStudentDocuments" AutoGenerateColumns="false" AllowSorting="true" OnPageIndexChanging="gvStudentDocuments_PageIndexChanging" OnPageSizeChanging="gvStudentDocuments_PageSizeChanging" OnSorting="gvStudentDocuments_Sorting">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%#: Container.DataItemIndex + 1 %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Enrollment" SortExpression="Enrollment">
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" Text="<%# Item.Enrollment %>" NavigateUrl="<%# Aspire.Web.Sys.Staff.Students.StudentDocuments.GetPageUrl(Item.Enrollment, true) %>" Target="_blank" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
			<asp:BoundField DataField="ProgramAlias" HeaderText="Program" SortExpression="ProgramAlias" />
			<asp:TemplateField HeaderText="Intake Semester" SortExpression="IntakeSemesterID">
				<ItemTemplate><%#: Item.IntakeSemesterID.ToSemesterString() %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Documents">
				<ItemTemplate><%# Item.DocumentsHtml %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Document Status" SortExpression="StudentDocumentsStatus">
				<ItemTemplate>
					<%#: (Item.StudentDocumentsStatus?.GetEnum<Aspire.Model.Entities.Student.StudentDocumentsStatuses>() ?? Aspire.Model.Entities.Student.StudentDocumentsStatuses.VerificationPending).GetFullName() %>
					<div>
						<small>
							<%#: Item.StudentDocumentsStatusDate != null
									  ? $"{Item.StudentDocumentsStatusDate:f} by {Item.StudentDocumentsLastUpdatedBy}"
									  : string.Empty.ToNAIfNullOrEmpty() %>
						</small>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
