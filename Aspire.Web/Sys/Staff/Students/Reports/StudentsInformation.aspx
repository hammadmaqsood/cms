﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="StudentsInformation.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.Reports.StudentsInformation" %>
<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" CausesValidation="False" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" ValidationGroup="Parameters"  />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Student Status:" AssociatedControlID="ddlStudentStatusFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlStudentStatusFilter" CausesValidation="False" />
		</div>
		<div class="form-group">
			<aspire:AspireButton runat="server" ID="btnDisplayReport" Text="Display Report" OnClick="btnDisplayReport_OnClick" CausesValidation="False" />
		</div>
	</div>
</asp:Content>
