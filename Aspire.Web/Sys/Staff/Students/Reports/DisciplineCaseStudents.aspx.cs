﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using Aspire.Lib.Extensions;
using Microsoft.Reporting.WebForms;

namespace Aspire.Web.Sys.Staff.Students.Reports
{
	[AspirePage("3DE5FC24-24E4-47F0-B5EB-2147ABD10125", UserTypes.Staff, "~/Sys/Staff/Students/Reports/DisciplineCaseStudents.aspx", "Discipline Case Students", true, AspireModules.StudentInformation)]
	public partial class DisciplineCaseStudents : StaffPage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Discipline Case Students";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.StudentInformation.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};

		public string ReportName => this.PageTitle;
		public bool ExportToExcel => true;
		public bool ExportToWord => true;
		private bool _renderDisabled = true;
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var offenceActivities = BL.Core.StudentInformation.Staff.DisciplineCase.GetDisciplineCaseActivities(this.StaffIdentity.LoginSessionGuid);
				this.ddlOffenceActivities.DataBind(offenceActivities, CommonListItems.All);
				this._renderDisabled = true;
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this._renderDisabled)
				return;
			var tbCaseFileNo = this.tbCaseFileNo.Text.Trim();
			var caseDateFrom = this.dtpOffenceDateFrom.Text.ToNullableDateTime();
			var caseDateTo = this.dtpOffenceDateTo.Text.ToNullableDateTime();
			var enrollment = this.tbEnrollment.Text;
			var offenceActivityID = this.ddlOffenceActivities.SelectedValue.ToNullableInt();
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/StudentInformation/Staff/DisciplineCaseStudents.rdlc".MapPath();
			var reportDataSet = BL.Core.StudentInformation.Staff.Reports.DisciplineCaseStudents.GetDisciplineCaseStudents(tbCaseFileNo, caseDateFrom, caseDateTo, enrollment, offenceActivityID, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.DisciplineCaseStudents), reportDataSet.DisciplineCaseStudents));
			reportViewer.LocalReport.EnableExternalImages = true;
		}


		protected void btnDisplayReport_OnClick(object sender, EventArgs e)
		{
			this._renderDisabled = false;
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView.ReportViewer);
		}
	}
}