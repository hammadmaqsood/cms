﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="RegistrationForm.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.Reports.RegistrationForm" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlProgramIDFilter_OnSelectedIndexChanged" />
		</div>
	</div>
</asp:Content>
