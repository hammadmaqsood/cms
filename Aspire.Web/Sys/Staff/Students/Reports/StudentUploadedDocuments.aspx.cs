﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Staff.Students.Reports
{
	[AspirePage("0632C09E-ED61-40FA-93AE-16337FF688E2", UserTypes.Staff, "~/Sys/Staff/Students/Reports/StudentUploadedDocuments.aspx", "Student Uploaded Documents", true, AspireModules.StudentInformation)]

	public partial class StudentUploadedDocuments : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.StudentInformation.Module, new []{UserGroupPermission.PermissionValues.Allowed}},
			{StaffPermissions.StudentInformation.ViewStudentDocuments, new []{UserGroupPermission.PermissionValues.Allowed}}
		};

		public override PageIcon PageIcon => FontAwesomeIcons.regular_file_alt.GetIcon();
		public override string PageTitle => "Students' Uploaded Documents";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterFilter.FillSemesters();
				this.ddlDocumentStatusFilter.FillGuidEnums<Model.Entities.Student.StudentDocumentsStatuses>(CommonListItems.All);
				this.ddlNationalityFilter.FillYesNo(CommonListItems.All);
				this.cblGroupBy.FillGuidEnums<Aspire.Model.Entities.StudentDocument.DocumentTypes>().SetSelectedGuidEnum(Model.Entities.StudentDocument.DocumentTypes.CNIC);
				this.ddlSemesterFilter_SelectedIndexChanged(null, null);
			}
			this.SetMaximumScriptTimeout();
		}

		protected void ddlSemesterFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterFilter.SelectedValue.ToShort();
			this.ddlProgramFilter.FillPrograms(this.StaffIdentity.InstituteID, null, true);
			this.ddlProgramFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlProgramFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDocumentStatusFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlDocumentStatusFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlNationalityFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlNationalityFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.cblGroupBy_SelectedIndexChanged(null, null);
		}

		protected void cblGroupBy_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.StudentsUploadedDocuments();
		}

		private void StudentsUploadedDocuments()
		{
			var semesterID = this.ddlSemesterFilter.SelectedValue.ToShort();
			var programID = this.ddlProgramFilter.SelectedValue.ToInt();
			var documentStatusEnum = this.ddlDocumentStatusFilter.GetSelectedNullableGuidEnum<Model.Entities.Student.StudentDocumentsStatuses>();
			var pakistani = this.ddlNationalityFilter.SelectedValue.ToNullableBoolean();

			var groupByFields = new List<Model.Entities.StudentDocument.DocumentTypes>();
			foreach (var field in this.cblGroupBy.GetSelectedValues())
			{
				groupByFields.Add(field.ToGuid().GetEnum<Model.Entities.StudentDocument.DocumentTypes>());
			}

			if (!groupByFields.Any())
			{
				this.AddErrorAlert("Select at-least one Column.");
				this.gvUploadedDocuments.DataBindNull();
				return;
			}

			var dataTable = BL.Core.StudentInformation.Staff.Reports.StudentUploadedDocuments.GetUploadedDocuments(semesterID, programID, documentStatusEnum, pakistani, groupByFields, this.StaffIdentity.LoginSessionGuid);
			this.gvUploadedDocuments.DataBind(dataTable.DefaultView);
		}
	}
}