﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="DisciplineCaseStudents.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.Reports.DisciplineCaseStudents" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Case File No:" AssociatedControlID="tbCaseFileNo" />
			<aspire:AspireTextBox runat="server" ID="tbCaseFileNo" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
			<aspire:AspireTextBox runat="server" ID="tbEnrollment" CausesValidation="False" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Offence Activities:" AssociatedControlID="ddlOffenceActivities" />
			<aspire:AspireDropDownList runat="server" ID="ddlOffenceActivities" CausesValidation="False" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Offence Date From:" AssociatedControlID="dtpOffenceDateFrom" />
			<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpOffenceDateFrom" CausesValidation="False" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Offence Date To:" AssociatedControlID="dtpOffenceDateTo" />
			<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpOffenceDateTo" CausesValidation="False" />
		</div>
		
		<div class="form-group">
			<aspire:AspireButton runat="server" ID="btnDisplayReport" Text="Display Report" ButtonType="Primary" ToolTip="Display Report" OnClick="btnDisplayReport_OnClick" />
		</div>
	</div>
</asp:Content>
