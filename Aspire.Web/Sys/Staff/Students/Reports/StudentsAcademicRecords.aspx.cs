﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Students.Reports
{
	[AspirePage("874A0A68-3FE2-488C-80F6-55317096C455", UserTypes.Staff, "~/Sys/Staff/Students/Reports/StudentsAcademicRecords.aspx", "Students Academic Records", true, AspireModules.StudentInformation)]
	public partial class StudentsAcademicRecords : StaffPage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Students Academic Records";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.StudentInformation.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};

		public string ReportName => "Students Academic Records";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;
		private bool _renderDisabled = true;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters();
				if (this.ddlSemesterIDFilter.Items.Count == 0)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID);
				this.cblDegreeType.FillEnums<DegreeTypes>();
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);

		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, departmentID, departmentID == null, CommonListItems.All);
		}
		
		protected void btnDisplayReport_OnClick(object sender, EventArgs e)
		{
			this._renderDisabled = false;
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView.ReportViewer);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this._renderDisabled)
				return;
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var percentage = this.tbPercentage.Text.ToNullableDouble();
			var degreeTypesList = this.cblDegreeType.GetSelectedValues().Select(s => s.ToByte()).ToList();
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/StudentInformation/Staff/StudentsAcademicRecords.rdlc".MapPath();
			var reportDataSet = BL.Core.StudentInformation.Staff.Reports.StudentsAcademicRecords.GetStudentsAcademicRecords(semesterID, departmentID, programID, percentage, degreeTypesList, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.StudentsAcademicRecords), reportDataSet.StudentsAcademicRecords));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}