﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="GraduateDirectory.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.Reports.GraduateDirectory" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Intake Semester:" AssociatedControlID="ddlIntakeSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlIntakeSemesterIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlIntakeSemesterIDFilter_SelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlProgramIDFilter_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
		</div>
	</div>
</asp:Content>
