﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="StudentUploadedDocuments.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.Reports.StudentUploadedDocuments" %>

<asp:Content runat="server" ContentPlaceHolderID="BodyPH">
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Intake Semester:" AssociatedControlID="ddlSemesterFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterFilter" ValidationGroup="Filter" AutoPostBack="true" OnSelectedIndexChanged="ddlSemesterFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlProgramFilter" ValidationGroup="Filter" AutoPostBack="true" OnSelectedIndexChanged="ddlProgramFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Document Status:" AssociatedControlID="ddlDocumentStatusFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlDocumentStatusFilter" ValidationGroup="Filter" AutoPostBack="true" OnSelectedIndexChanged="ddlDocumentStatusFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Pakistani:" AssociatedControlID="ddlNationalityFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlNationalityFilter" ValidationGroup="Filter" AutoPostBack="true" OnSelectedIndexChanged="ddlNationalityFilter_SelectedIndexChanged" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<aspire:AspireLabel runat="server" Text="Show Columns:" AssociatedControlID="cblGroupBy" />
				<aspire:AspireCheckBoxList runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" ID="cblGroupBy" AutoPostBack="True" OnSelectedIndexChanged="cblGroupBy_SelectedIndexChanged" ValidationGroup="Grouping" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="cblGroupBy" ErrorMessage="Select atleast one 'Group By' clause." ValidationGroup="Grouping" />
			</div>
		</div>
	</div>
	<aspire:AspireGridView runat="server" AutoGenerateColumns="True" ID="gvUploadedDocuments" AllowSorting="False" AllowPaging="False" />
</asp:Content>
