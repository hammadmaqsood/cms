﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Students.Reports
{
	[AspirePage("1650E69E-93B7-4733-9210-8D93ABC0154F", UserTypes.Staff, "~/Sys/Staff/Students/Reports/GraduateDirectory.aspx", "Graduate Directory", true, AspireModules.StudentInformation)]
	public partial class GraduateDirectory : StaffPage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_graduation_cap);
		public override string PageTitle => "Graduate Directory";
		public string ReportName => this.PageTitle;
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.StudentInformation.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};

		private bool _renderDisabled = true;
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlIntakeSemesterIDFilter.FillSemesters(CommonListItems.All);
				if (this.ddlIntakeSemesterIDFilter.Items.Count == 1)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All);
				this.ddlIntakeSemesterIDFilter_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlIntakeSemesterIDFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			var intakeSemesterID = this.ddlIntakeSemesterIDFilter.SelectedValue.ToNullableShort();
			if (intakeSemesterID == null)
				this.ddlSemesterIDFilter.FillSemesters();
			else
				this.ddlSemesterIDFilter.FillSemesters(intakeSemesterID.Value);
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, departmentID, departmentID == null, CommonListItems.All);
			this.ddlProgramIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearch_OnClick(null, null);
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this._renderDisabled = false;
			this.RenderReport(reportView.ReportViewer);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this._renderDisabled)
				return;
			var intakeSemesterID = this.ddlIntakeSemesterIDFilter.SelectedValue.ToNullableShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/StudentInformation/Staff/GraduateDirectory/GraduateDirectory.rdlc".MapPath();
			var reportDataSet = BL.Core.StudentInformation.Staff.Reports.GraduateDirectory.GetGraduateDirectoryForms(intakeSemesterID, departmentID, programID, semesterID, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.SubreportProcessing += (sender, e) =>
			{
				var studentID = e.Parameters["StudentID"].Values[0].ToInt();
				var graduateDirectoryForm = reportDataSet.GraduateDirectoryForms.Find(s => s.StudentID == studentID);
				switch (e.ReportPath)
				{
					case "AcademicRecords":
						e.DataSources.Add(new ReportDataSource(nameof(graduateDirectoryForm.StudentAcademicRecords), graduateDirectoryForm.StudentAcademicRecords));
						break;
					case "WorkExperiences":
						e.DataSources.Add(new ReportDataSource(nameof(graduateDirectoryForm.GraduateDirectoryFormWorkExperiences), graduateDirectoryForm.GraduateDirectoryFormWorkExperiences));
						break;
					case "Projects":
						e.DataSources.Add(new ReportDataSource(nameof(graduateDirectoryForm.GraduateDirectoryFormProjects), graduateDirectoryForm.GraduateDirectoryFormProjects));
						break;
					case "VolunteerWorks":
						e.DataSources.Add(new ReportDataSource(nameof(graduateDirectoryForm.GraduateDirectoryFormVolunteerWorks), graduateDirectoryForm.GraduateDirectoryFormVolunteerWorks));
						break;
					case "Workshops":
						e.DataSources.Add(new ReportDataSource(nameof(graduateDirectoryForm.GraduateDirectoryFormWorkshops), graduateDirectoryForm.GraduateDirectoryFormWorkshops));
						break;
					case "ExtraCurricularActivities":
						e.DataSources.Add(new ReportDataSource(nameof(graduateDirectoryForm.GraduateDirectoryExtraCurricularActivities), graduateDirectoryForm.GraduateDirectoryExtraCurricularActivities));
						break;
					case "Skills":
						e.DataSources.Add(new ReportDataSource(nameof(graduateDirectoryForm.GraduateDirectoryFormSkills), graduateDirectoryForm.GraduateDirectoryFormSkills));
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(e.ReportPath), e.ReportPath, null);
				}
			};

			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.GraduateDirectoryForms), reportDataSet.GraduateDirectoryForms));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}