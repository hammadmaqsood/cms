﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Students.Reports
{
	[AspirePage("D70FF1CE-CB09-4D5D-AD5A-396E216AB71E", UserTypes.Staff, "~/Sys/Staff/Students/Reports/StudentsInformation.aspx", "Students Information", true, AspireModules.StudentInformation)]
	public partial class StudentsInformation : StaffPage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Students Information";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.StudentInformation.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};

		public string ReportName => "Students Information";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;
		private bool _renderDisabled = true;
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters();
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All);
				this.ddlStudentStatusFilter.FillEnums<Model.Entities.Student.Statuses>(CommonListItems.All);
				this.ddlStudentStatusFilter.Items.Insert(1, new ListItem
				{
					Text = CommonListItems.NoneLabel,
					Value = byte.MaxValue.ToString(),

				});
				this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, departmentID, false, CommonListItems.All);
		}

		protected void btnDisplayReport_OnClick(object sender, EventArgs e)
		{
			this._renderDisabled = false;
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView.ReportViewer);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this._renderDisabled)
				return;
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var ddlstudentStatusText = this.ddlStudentStatusFilter.SelectedItem.Text;
			var studentStatusEnum = this.ddlStudentStatusFilter.GetSelectedEnumValue<Model.Entities.Student.Statuses>(null);
			var studentStatus = this.ddlStudentStatusFilter.SelectedValue.ToNullableByte();
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/StudentInformation/Staff/StudentsInformation.rdlc".MapPath();
			var reportDataSet = BL.Core.StudentInformation.Staff.Reports.StudentInformation.GetStudentsInformation(semesterID, departmentID, programID, ddlstudentStatusText, studentStatus, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.StudentsInformation), reportDataSet.StudentsInformation));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}