﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Students.Reports
{
	[AspirePage("721E11EE-9269-4E30-90CF-72D7C250F7CA", UserTypes.Staff, "~/Sys/Staff/Students/Reports/RegistrationForm.aspx", "Registration Form", true, AspireModules.StudentInformation)]
	public partial class RegistrationForm : StaffPage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Registration Form";
		public string ReportName => this.PageTitle;
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.StudentInformation.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};

		private bool _renderDisabled = true;
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters();
				if (this.ddlSemesterIDFilter.Items.Count == 0)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID);
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, departmentID, departmentID == null, CommonListItems.All);
			this.ddlProgramIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearch_OnClick(null, null);
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this._renderDisabled = false;
			this.RenderReport(reportView.ReportViewer);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this._renderDisabled)
				return;
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/StudentInformation/Staff/RegistrationForm.rdlc".MapPath();
			var reportDataSet = BL.Core.StudentInformation.Staff.Reports.RegistrationForm.GetStudentsForRegistrationForm(semesterID, departmentID, programID, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.SubreportProcessing += (sender, e) =>
			{
				switch (e.ReportPath)
				{
					case "StudentAcademicRecords":
						var studentID = e.Parameters["StudentID"].Values[0].ToInt();
						var studentAcademicRecords = reportDataSet.StudentsForRegistrationForm.Find(s => s.StudentID == studentID).StudentAcademicRecords;
						e.DataSources.Add(new ReportDataSource("StudentAcademicRecords", studentAcademicRecords));
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(e.ReportPath));
				}
			};

			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.StudentsForRegistrationForm), reportDataSet.StudentsForRegistrationForm));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}