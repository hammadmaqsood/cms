﻿using Aspire.BL.Core.StudentInformation.Staff.Reports;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Staff.Students.Reports
{
	[AspirePage("A6AA4D55-9F3A-475A-919D-759527EF2362", UserTypes.Staff, "~/Sys/Staff/Students/Reports/StudentInfoAndAcademicRecords.aspx", "Students Information and Academic Records", true, AspireModules.StudentInformation)]
	public partial class StudentInfoAndAcademicRecords : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Students Information";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.StudentInformation.Module,new [] { UserGroupPermission.PermissionValues.Allowed, }},
			{StaffPermissions.Exams.Module,new [] { UserGroupPermission.PermissionValues.Allowed, }}
		};
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlReportType.FillEnums<StudentInformation.ReportTypes>();
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All);
				this.ddlSemesterNo.FillSemesterNos(CommonListItems.All);
				this.ddlSection.FillSections(CommonListItems.All);
				this.ddlShift.FillShifts(CommonListItems.All);
				this.ddlReportType_OnSelectedIndexChanged(null, null);
				this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlReportType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var reportTypeEnum = this.ddlReportType.GetSelectedEnumValue<StudentInformation.ReportTypes>();
			switch (reportTypeEnum)
			{
				case StudentInformation.ReportTypes.StudentsInformation:
				case StudentInformation.ReportTypes.StudentsAcademicRecords:
				case StudentInformation.ReportTypes.StudentsInformationWithAcademicRecordsNational:
				case StudentInformation.ReportTypes.StudentsInformationWithAcademicRecordsInternational:
					this.ddlSemesterIDFilter.FillSemesters(CommonListItems.All);
					this.ddlSemesterNo.ClearSelection();
					this.ddlSection.ClearSelection();
					this.ddlShift.ClearSelection();
					this.divSemesterNo.Visible = false;
					this.divSection.Visible = false;
					this.divShift.Visible = false;
					break;
				case StudentInformation.ReportTypes.StudentsInformationCourseRegistrationWise:
				case StudentInformation.ReportTypes.StudentsInformationWithRegisteredCoursesDetails:
					this.ddlSemesterIDFilter.FillSemesters();
					this.divSemesterNo.Visible = true;
					this.ddlSemesterNo.ClearSelection();
					this.divSection.Visible = true;
					this.ddlSection.ClearSelection();
					this.divShift.Visible = true;
					this.ddlShift.ClearSelection();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, departmentID, false, CommonListItems.All);
		}

		protected void btnExport_OnClick(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var semesterNo = this.ddlSemesterNo.SelectedValue.ToNullableShort();
			var section = this.ddlSection.SelectedValue.ToNullableInt();
			var shift = this.ddlShift.SelectedValue.ToNullableByte();
			var reportType = this.ddlReportType.GetSelectedEnumValue<StudentInformation.ReportTypes>();
			List<StudentInformation.Student> data;
			string csv;
			switch (reportType)
			{
				case StudentInformation.ReportTypes.StudentsInformation:
					data = StudentInformation.GetStudentsInformationAndAcademicRecords(semesterID, departmentID, programID, reportType, this.StaffIdentity.LoginSessionGuid);
					csv = data.ConvertToCSVFormat(new List<CSVHelper.CSVColumnMapping>
					{
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "INSTITUTE", PropertyName=nameof(StudentInformation.Student.InstituteAlias)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "INTAKE_SEMESTER", PropertyName=nameof(StudentInformation.Student.IntakeSemester)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "REGISTRATION", PropertyName=nameof(StudentInformation.Student.RegistrationNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "APP_NO", PropertyName=nameof(StudentInformation.Student.ApplicationNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "ENROLLMENT", PropertyName=nameof(StudentInformation.Student.Enrollment)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "NAME", PropertyName=nameof(StudentInformation.Student.Name)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DEPARTMENT", PropertyName=nameof(StudentInformation.Student.DepartmentName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PROGRAM", PropertyName=nameof(StudentInformation.Student.ProgramAlias)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CLASS", PropertyName=nameof(StudentInformation.Student.Class)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DEG_DURATION", PropertyName=nameof(StudentInformation.Student.DurationFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "EMAIL", PropertyName=nameof(StudentInformation.Student.PersonalEmail)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "OFFICIAL_EMAIL", PropertyName=nameof(StudentInformation.Student.UniversityEmail)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "GENDER", PropertyName=nameof(StudentInformation.Student.GenderFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CNIC", PropertyName=nameof(StudentInformation.Student.CNIC)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DOB", PropertyName=nameof(StudentInformation.Student.DOBDateOnly)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "BLOOD_GROUP", PropertyName=nameof(StudentInformation.Student.BloodGroupFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CATEGORY", PropertyName=nameof(StudentInformation.Student.CategoryFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PHONE", PropertyName=nameof(StudentInformation.Student.Phone)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "MOBILE", PropertyName=nameof(StudentInformation.Student.Mobile)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CURRENT_ADD", PropertyName=nameof(StudentInformation.Student.CurrentAddress)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PERMANENT_ADD", PropertyName=nameof(StudentInformation.Student.PermanentAddress)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "NATIONALITY", PropertyName=nameof(StudentInformation.Student.Nationality)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "COUNTRY", PropertyName=nameof(StudentInformation.Student.Country)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PROVINCE", PropertyName=nameof(StudentInformation.Student.Province)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DISTRICT", PropertyName=nameof(StudentInformation.Student.District)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "TEHSIL", PropertyName=nameof(StudentInformation.Student.Tehsil)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DOMICILE", PropertyName=nameof(StudentInformation.Student.Domicile)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "AREA_TYPE", PropertyName=nameof(StudentInformation.Student.AreaTypeFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PHYSICAL_DISABILITY", PropertyName=nameof(StudentInformation.Student.PhysicalDisability)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CREDIT_TRANSFER", PropertyName=nameof(StudentInformation.Student.CreditTranferYesNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_NAME", PropertyName=nameof(StudentInformation.Student.FatherName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_CNIC", PropertyName=nameof(StudentInformation.Student.FatherCNIC)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_DESIGNATION", PropertyName=nameof(StudentInformation.Student.FatherDesignation)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_DEPARTMENT", PropertyName=nameof(StudentInformation.Student.FatherDepartment)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_ORGANIZATION", PropertyName=nameof(StudentInformation.Student.FatherOrganization)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_PASSPORTNO", PropertyName=nameof(StudentInformation.Student.FatherPassportNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_MOBILE", PropertyName=nameof(StudentInformation.Student.FatherMobile)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_PHONE", PropertyName=nameof(StudentInformation.Student.FatherHomePhone)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSORED_BY", PropertyName=nameof(StudentInformation.Student.SponsoredByFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_NAME", PropertyName=nameof(StudentInformation.Student.SponsorName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_CNIC", PropertyName=nameof(StudentInformation.Student.SponsorCNIC)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_DESIGNATION", PropertyName=nameof(StudentInformation.Student.SponsorDesignation)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_DEPARTMENT", PropertyName=nameof(StudentInformation.Student.SponsorDepartment)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_ORGANIZATION", PropertyName=nameof(StudentInformation.Student.SponsorOrganization)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_RELATIONSHIP", PropertyName=nameof(StudentInformation.Student.SponsorRelationshipWithGuardian)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_PASSPORTNO", PropertyName=nameof(StudentInformation.Student.SponsorPassportNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_MOBILE", PropertyName=nameof(StudentInformation.Student.SponsorMobile)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_PHONE", PropertyName=nameof(StudentInformation.Student.SponsorPhoneHome)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "ENTRY_TEST_DATE", PropertyName=nameof(StudentInformation.Student.EntryTestDateOnly)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "ENTRY_MARKS", PropertyName=nameof(StudentInformation.Student.EntryTestMarksComplete)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "STATUS", PropertyName=nameof(StudentInformation.Student.StudentStatusFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "STATUS_DATE", PropertyName=nameof(StudentInformation.Student.StatusDateOnly)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DEGREE_NO", PropertyName=nameof(StudentInformation.Student.DegreeNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DEGREE_GAZETTE_NO", PropertyName=nameof(StudentInformation.Student.DegreeGazetteNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DEGREE_ISSUE_DATE", PropertyName=nameof(StudentInformation.Student.DegreeIssueDate)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DEGREE_ISSUE_TO_STUDENT", PropertyName=nameof(StudentInformation.Student.DegreeIssuedToStudent)},
				});
					break;
				case StudentInformation.ReportTypes.StudentsInformationCourseRegistrationWise:
					data = StudentInformation.GetStudentsInformationCourseRegistrationWise(semesterID, departmentID, programID, semesterNo, section, shift, reportType, this.StaffIdentity.LoginSessionGuid);
					csv = data.ConvertToCSVFormat(new List<CSVHelper.CSVColumnMapping>
					{
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "INSTITUTE", PropertyName=nameof(StudentInformation.Student.InstituteAlias)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "INTAKE_SEMESTER", PropertyName=nameof(StudentInformation.Student.IntakeSemester)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "REGISTRATION", PropertyName=nameof(StudentInformation.Student.RegistrationNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "APP_NO", PropertyName=nameof(StudentInformation.Student.ApplicationNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "ENROLLMENT", PropertyName=nameof(StudentInformation.Student.Enrollment)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "NAME", PropertyName=nameof(StudentInformation.Student.Name)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DEPARTMENT", PropertyName=nameof(StudentInformation.Student.DepartmentName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PROGRAM", PropertyName=nameof(StudentInformation.Student.ProgramShortName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CLASS", PropertyName=nameof(StudentInformation.Student.Class)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DEG_DURATION", PropertyName=nameof(StudentInformation.Student.DurationFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "EMAIL", PropertyName=nameof(StudentInformation.Student.PersonalEmail)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "OFFICIAL_EMAIL", PropertyName=nameof(StudentInformation.Student.UniversityEmail)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "GENDER", PropertyName=nameof(StudentInformation.Student.GenderFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CNIC", PropertyName=nameof(StudentInformation.Student.CNIC)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DOB", PropertyName=nameof(StudentInformation.Student.DOBDateOnly)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "BLOOD_GROUP", PropertyName=nameof(StudentInformation.Student.BloodGroupFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CATEGORY", PropertyName=nameof(StudentInformation.Student.CategoryFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PHONE", PropertyName=nameof(StudentInformation.Student.Phone)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "MOBILE", PropertyName=nameof(StudentInformation.Student.Mobile)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CURRENT_ADD", PropertyName=nameof(StudentInformation.Student.CurrentAddress)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PERMANENT_ADD", PropertyName=nameof(StudentInformation.Student.PermanentAddress)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "NATIONALITY", PropertyName=nameof(StudentInformation.Student.Nationality)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "COUNTRY", PropertyName=nameof(StudentInformation.Student.Country)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PROVINCE", PropertyName=nameof(StudentInformation.Student.Province)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DISTRICT", PropertyName=nameof(StudentInformation.Student.District)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "TEHSIL", PropertyName=nameof(StudentInformation.Student.Tehsil)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DOMICILE", PropertyName=nameof(StudentInformation.Student.Domicile)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "AREA_TYPE", PropertyName=nameof(StudentInformation.Student.AreaTypeFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PHYSICAL_DISABILITY", PropertyName=nameof(StudentInformation.Student.PhysicalDisability)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CREDIT_TRANSFER", PropertyName=nameof(StudentInformation.Student.CreditTranferYesNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_NAME", PropertyName=nameof(StudentInformation.Student.FatherName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_CNIC", PropertyName=nameof(StudentInformation.Student.FatherCNIC)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_DESIGNATION", PropertyName=nameof(StudentInformation.Student.FatherDesignation)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_DEPARTMENT", PropertyName=nameof(StudentInformation.Student.FatherDepartment)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_ORGANIZATION", PropertyName=nameof(StudentInformation.Student.FatherOrganization)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_PASSPORTNO", PropertyName=nameof(StudentInformation.Student.FatherPassportNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_MOBILE", PropertyName=nameof(StudentInformation.Student.FatherMobile)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_PHONE", PropertyName=nameof(StudentInformation.Student.FatherHomePhone)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSORED_BY", PropertyName=nameof(StudentInformation.Student.SponsoredByFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_NAME", PropertyName=nameof(StudentInformation.Student.SponsorName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_CNIC", PropertyName=nameof(StudentInformation.Student.SponsorCNIC)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_DESIGNATION", PropertyName=nameof(StudentInformation.Student.SponsorDesignation)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_DEPARTMENT", PropertyName=nameof(StudentInformation.Student.SponsorDepartment)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_ORGANIZATION", PropertyName=nameof(StudentInformation.Student.SponsorOrganization)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_RELATIONSHIP", PropertyName=nameof(StudentInformation.Student.SponsorRelationshipWithGuardian)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_PASSPORTNO", PropertyName=nameof(StudentInformation.Student.SponsorPassportNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_MOBILE", PropertyName=nameof(StudentInformation.Student.SponsorMobile)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_PHONE", PropertyName=nameof(StudentInformation.Student.SponsorPhoneHome)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "STATUS", PropertyName=nameof(StudentInformation.Student.StudentStatusFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SEMESTER FREEZE STATUS", PropertyName=nameof(StudentInformation.Student.SemesterFreezeStatusEnumFullName)},
					});
					break;
				case StudentInformation.ReportTypes.StudentsAcademicRecords:
					data = StudentInformation.GetStudentsInformationAndAcademicRecords(semesterID, departmentID, programID, reportType, this.StaffIdentity.LoginSessionGuid);
					csv = data.ConvertToCSVFormat(new List<CSVHelper.CSVColumnMapping>
					{
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "ENROLLMENT", PropertyName=nameof(StudentInformation.Student.Enrollment)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DEGREE_TYPE", PropertyName=nameof(StudentInformation.Student.DegreeTypeFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "OBTAINED_MARKS", PropertyName=nameof(StudentInformation.Student.ObtainedMarks)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "TOTAL_MARKS", PropertyName=nameof(StudentInformation.Student.TotalMarks)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PERCENTAGE", PropertyName=nameof(StudentInformation.Student.ShortPercentage)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SUBJECTS", PropertyName=nameof(StudentInformation.Student.Subjects)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "INSTITUTE", PropertyName=nameof(StudentInformation.Student.Institute)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "BOARD_OR_UNIVERSITY", PropertyName=nameof(StudentInformation.Student.BoardUniversity )},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PASSING_YEAR", PropertyName=nameof(StudentInformation.Student.PassingYear)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "REMARKS", PropertyName=nameof(StudentInformation.Student.RemarksNA)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DEGREE_DOCUMENT_STATUS", PropertyName=nameof(StudentInformation.Student.StudentAcademicStatusFullName)},
					});
					break;
				case StudentInformation.ReportTypes.StudentsInformationWithAcademicRecordsNational:
				case StudentInformation.ReportTypes.StudentsInformationWithAcademicRecordsInternational:
					data = StudentInformation.GetStudentsInformationAndAcademicRecords(semesterID, departmentID, programID, reportType, this.StaffIdentity.LoginSessionGuid);
					csv = data.ConvertToCSVFormat(new List<CSVHelper.CSVColumnMapping>
					{
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "INSTITUTE", PropertyName = nameof(StudentInformation.Student.InstituteAlias)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "INTAKE_SEMESTER", PropertyName = nameof(StudentInformation.Student.IntakeSemester)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "REGISTRATION", PropertyName = nameof(StudentInformation.Student.RegistrationNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "APP_NO", PropertyName = nameof(StudentInformation.Student.ApplicationNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "ENROLLMENT", PropertyName = nameof(StudentInformation.Student.Enrollment)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "NAME", PropertyName = nameof(StudentInformation.Student.Name)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DEPARTMENT", PropertyName = nameof(StudentInformation.Student.DepartmentName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PROGRAM", PropertyName = nameof(StudentInformation.Student.ProgramAlias)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CLASS", PropertyName = nameof(StudentInformation.Student.Class)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DEG_DURATION", PropertyName = nameof(StudentInformation.Student.DurationFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "EMAIL", PropertyName = nameof(StudentInformation.Student.PersonalEmail)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "OFFICIAL_EMAIL", PropertyName = nameof(StudentInformation.Student.UniversityEmail)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "GENDER", PropertyName = nameof(StudentInformation.Student.GenderFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CNIC", PropertyName = nameof(StudentInformation.Student.CNIC)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DOB", PropertyName = nameof(StudentInformation.Student.DOBDateOnly)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "BLOOD_GROUP", PropertyName = nameof(StudentInformation.Student.BloodGroupFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CATEGORY", PropertyName = nameof(StudentInformation.Student.CategoryFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PHONE", PropertyName = nameof(StudentInformation.Student.Phone)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "MOBILE", PropertyName = nameof(StudentInformation.Student.Mobile)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CURRENT_ADD", PropertyName = nameof(StudentInformation.Student.CurrentAddress)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PERMANENT_ADD", PropertyName = nameof(StudentInformation.Student.PermanentAddress)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "NATIONALITY", PropertyName = nameof(StudentInformation.Student.Nationality)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "COUNTRY", PropertyName = nameof(StudentInformation.Student.Country)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PROVINCE", PropertyName = nameof(StudentInformation.Student.Province)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DISTRICT", PropertyName = nameof(StudentInformation.Student.District)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "TEHSIL", PropertyName = nameof(StudentInformation.Student.Tehsil)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DOMICILE", PropertyName = nameof(StudentInformation.Student.Domicile)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "AREA_TYPE", PropertyName = nameof(StudentInformation.Student.AreaTypeFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PHYSICAL_DISABILITY", PropertyName = nameof(StudentInformation.Student.PhysicalDisability)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CREDIT_TRANSFER", PropertyName = nameof(StudentInformation.Student.CreditTranferYesNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_NAME", PropertyName = nameof(StudentInformation.Student.FatherName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_CNIC", PropertyName = nameof(StudentInformation.Student.FatherCNIC)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_DESIGNATION", PropertyName = nameof(StudentInformation.Student.FatherDesignation)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_DEPARTMENT", PropertyName = nameof(StudentInformation.Student.FatherDepartment)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_ORGANIZATION", PropertyName = nameof(StudentInformation.Student.FatherOrganization)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_PASSPORTNO", PropertyName = nameof(StudentInformation.Student.FatherPassportNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_MOBILE", PropertyName = nameof(StudentInformation.Student.FatherMobile)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_PHONE", PropertyName = nameof(StudentInformation.Student.FatherHomePhone)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSORED_BY", PropertyName = nameof(StudentInformation.Student.SponsoredByFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_NAME", PropertyName = nameof(StudentInformation.Student.SponsorName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_CNIC", PropertyName = nameof(StudentInformation.Student.SponsorCNIC)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_DESIGNATION", PropertyName = nameof(StudentInformation.Student.SponsorDesignation)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_DEPARTMENT", PropertyName = nameof(StudentInformation.Student.SponsorDepartment)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_ORGANIZATION", PropertyName = nameof(StudentInformation.Student.SponsorOrganization)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_RELATIONSHIP", PropertyName = nameof(StudentInformation.Student.SponsorRelationshipWithGuardian)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_PASSPORTNO", PropertyName = nameof(StudentInformation.Student.SponsorPassportNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_MOBILE", PropertyName = nameof(StudentInformation.Student.SponsorMobile)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_PHONE", PropertyName = nameof(StudentInformation.Student.SponsorPhoneHome)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "ENTRY_TEST_DATE", PropertyName = nameof(StudentInformation.Student.EntryTestDateOnly)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "ENTRY_MARKS", PropertyName = nameof(StudentInformation.Student.EntryTestMarksComplete)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "STATUS", PropertyName = nameof(StudentInformation.Student.StudentStatusFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "STATUS_DATE", PropertyName = nameof(StudentInformation.Student.StatusDateOnly)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DEGREE_TYPE", PropertyName = nameof(StudentInformation.Student.DegreeTypeFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "OBTAINED_MARKS", PropertyName = nameof(StudentInformation.Student.ObtainedMarks)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "TOTAL_MARKS", PropertyName = nameof(StudentInformation.Student.TotalMarks)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PERCENTAGE", PropertyName = nameof(StudentInformation.Student.ShortPercentage)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SUBJECTS", PropertyName = nameof(StudentInformation.Student.Subjects)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "INSTITUTE", PropertyName = nameof(StudentInformation.Student.Institute)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "BOARD_OR_UNIVERSITY", PropertyName = nameof(StudentInformation.Student.BoardUniversity)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PASSING_YEAR", PropertyName = nameof(StudentInformation.Student.PassingYear)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "REMARKS", PropertyName = nameof(StudentInformation.Student.RemarksNA)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DEGREE_DOCUMENT_STATUS", PropertyName = nameof(StudentInformation.Student.StudentAcademicStatusFullName)},
					});
					break;
				case StudentInformation.ReportTypes.StudentsInformationWithRegisteredCoursesDetails:
					data = StudentInformation.GetStudentsInformationCourseRegistrationWise(semesterID, departmentID, programID, semesterNo, section, shift, reportType, this.StaffIdentity.LoginSessionGuid);
					csv = data.ConvertToCSVFormat(new List<CSVHelper.CSVColumnMapping>
					{
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "INSTITUTE", PropertyName=nameof(StudentInformation.Student.InstituteAlias)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "INTAKE_SEMESTER", PropertyName=nameof(StudentInformation.Student.IntakeSemester)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "REGISTRATION", PropertyName=nameof(StudentInformation.Student.RegistrationNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "APP_NO", PropertyName=nameof(StudentInformation.Student.ApplicationNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "ENROLLMENT", PropertyName=nameof(StudentInformation.Student.Enrollment)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "NAME", PropertyName=nameof(StudentInformation.Student.Name)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DEPARTMENT", PropertyName=nameof(StudentInformation.Student.DepartmentName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PROGRAM", PropertyName=nameof(StudentInformation.Student.ProgramShortName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "STUDENT CLASS", PropertyName=nameof(StudentInformation.Student.Class)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DEG_DURATION", PropertyName=nameof(StudentInformation.Student.DurationFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "EMAIL", PropertyName=nameof(StudentInformation.Student.PersonalEmail)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "OFFICIAL_EMAIL", PropertyName=nameof(StudentInformation.Student.UniversityEmail)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "GENDER", PropertyName=nameof(StudentInformation.Student.GenderFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CNIC", PropertyName=nameof(StudentInformation.Student.CNIC)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DOB", PropertyName=nameof(StudentInformation.Student.DOBDateOnly)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "BLOOD_GROUP", PropertyName=nameof(StudentInformation.Student.BloodGroupFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CATEGORY", PropertyName=nameof(StudentInformation.Student.CategoryFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PHONE", PropertyName=nameof(StudentInformation.Student.Phone)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "MOBILE", PropertyName=nameof(StudentInformation.Student.Mobile)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CURRENT_ADD", PropertyName=nameof(StudentInformation.Student.CurrentAddress)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PERMANENT_ADD", PropertyName=nameof(StudentInformation.Student.PermanentAddress)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "NATIONALITY", PropertyName=nameof(StudentInformation.Student.Nationality)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "COUNTRY", PropertyName=nameof(StudentInformation.Student.Country)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PROVINCE", PropertyName=nameof(StudentInformation.Student.Province)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DISTRICT", PropertyName=nameof(StudentInformation.Student.District)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "TEHSIL", PropertyName=nameof(StudentInformation.Student.Tehsil)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "DOMICILE", PropertyName=nameof(StudentInformation.Student.Domicile)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "AREA_TYPE", PropertyName=nameof(StudentInformation.Student.AreaTypeFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PHYSICAL_DISABILITY", PropertyName=nameof(StudentInformation.Student.PhysicalDisability)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CREDIT_TRANSFER", PropertyName=nameof(StudentInformation.Student.CreditTranferYesNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_NAME", PropertyName=nameof(StudentInformation.Student.FatherName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_CNIC", PropertyName=nameof(StudentInformation.Student.FatherCNIC)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_DESIGNATION", PropertyName=nameof(StudentInformation.Student.FatherDesignation)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_DEPARTMENT", PropertyName=nameof(StudentInformation.Student.FatherDepartment)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_ORGANIZATION", PropertyName=nameof(StudentInformation.Student.FatherOrganization)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_PASSPORTNO", PropertyName=nameof(StudentInformation.Student.FatherPassportNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_MOBILE", PropertyName=nameof(StudentInformation.Student.FatherMobile)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FATHER_PHONE", PropertyName=nameof(StudentInformation.Student.FatherHomePhone)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSORED_BY", PropertyName=nameof(StudentInformation.Student.SponsoredByFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_NAME", PropertyName=nameof(StudentInformation.Student.SponsorName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_CNIC", PropertyName=nameof(StudentInformation.Student.SponsorCNIC)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_DESIGNATION", PropertyName=nameof(StudentInformation.Student.SponsorDesignation)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_DEPARTMENT", PropertyName=nameof(StudentInformation.Student.SponsorDepartment)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_ORGANIZATION", PropertyName=nameof(StudentInformation.Student.SponsorOrganization)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_RELATIONSHIP", PropertyName=nameof(StudentInformation.Student.SponsorRelationshipWithGuardian)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_PASSPORTNO", PropertyName=nameof(StudentInformation.Student.SponsorPassportNo)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_MOBILE", PropertyName=nameof(StudentInformation.Student.SponsorMobile)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SPONSOR_PHONE", PropertyName=nameof(StudentInformation.Student.SponsorPhoneHome)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "STATUS", PropertyName=nameof(StudentInformation.Student.StudentStatusFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Offered Course Code", PropertyName=nameof(StudentInformation.Student.OfferedCourseCode)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Offered Title", PropertyName=nameof(StudentInformation.Student.OfferedTitle)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Offered Credit Hours", PropertyName=nameof(StudentInformation.Student.OfferedCreditHours)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Offered Class", PropertyName=nameof(StudentInformation.Student.OfferedClass)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "RoadMap Course Code", PropertyName=nameof(StudentInformation.Student.RoadMapCourseCode)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "RoadMap Title", PropertyName=nameof(StudentInformation.Student.RoadMapTitle)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "RoadMap Credit Hours", PropertyName=nameof(StudentInformation.Student.RoadMapCreditHours)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Registered Course Status", PropertyName=nameof(StudentInformation.Student.RegisteredCourseStatusEnumFullName)},
						new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Registered Course Freeze Status", PropertyName=nameof(StudentInformation.Student.RegisteredCourseFreezedStatusEnumFullName)},
					});
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			this.Response.Clear();
			this.Response.Buffer = true;
			var instituteAlias = data.FirstOrDefault()?.InstituteAlias;
			var fileName = $"{instituteAlias ?? this.StaffIdentity.InstituteID.ToString()} - {semesterID}-{reportType} - {DateTime.Now:dd-MM-yyyy hh:mm:ss}.csv";
			this.Response.AddHeader("content-disposition", $"attachment;filename=\"{fileName}\"");
			this.Response.Charset = "";
			this.Response.ContentType = "text/csv";
			this.Response.Output.Write(csv);
			this.Response.Flush();
			this.Response.End();
		}
	}
}