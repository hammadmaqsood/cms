﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Students.Reports
{
	[AspirePage("FE0174B0-9FF8-47F3-87EF-6C0AE7208BAB", UserTypes.Staff, "~/Sys/Staff/Students/Reports/StudentDocuments.aspx", "Student Documents", true, AspireModules.StudentInformation)]
	public partial class StudentDocuments : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.StudentInformation.ViewStudentDocuments, new []{UserGroupPermission.PermissionValues.Allowed}}
		};

		public override PageIcon PageIcon => AspireGlyphicons.file.GetIcon();
		public override string PageTitle => "Student Documents";

		public static string GetPageUrl(short? semesterID, int? departmentID, int? programID, int? pageIndex, int? pageSize, string sortExpression, SortDirection sortDirection)
		{
			return typeof(StudentDocuments).GetAspirePageAttribute().PageUrl.AttachQueryParams(
				("SemesterID", semesterID),
				("DepartmentID", departmentID),
				("ProgramID", programID),
				("PageIndex", pageIndex),
				("PageSize", pageSize),
				("SortExpression", sortExpression),
				("SortDirection", sortDirection));
		}

		public static void Redirect(short? semesterID, int? departmentID, int? programID, int? pageIndex, int? pageSize, string sortExpression, SortDirection sortDirection)
		{
			Redirect(GetPageUrl(semesterID, departmentID, programID, pageIndex, pageSize, sortExpression, sortDirection));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var sortExpression = this.GetParameterValue("SortExpression");
				var sortDirection = this.GetParameterValue<SortDirection>("SortDirection") ?? SortDirection.Ascending;
				this.ViewState.SetSortProperties(sortExpression ?? "Enrollment", sortDirection);
				this.ddlSemesterIDFilter.FillSemesters(20191, CommonListItems.All).SetSelectedValueIfNotPostback("SemesterID");
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All).SetSelectedValueIfNotPostback("DepartmentID");
				this.ddlDocumentType.FillGuidEnums<Model.Entities.StudentDocument.DocumentTypes>(CommonListItems.None);
				this.ddlSemesterIDFilter_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterIDFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			var deparmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, deparmentID, true, CommonListItems.All).SetSelectedValueIfNotPostback("ProgramID");
			this.ddlProgramIDFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDocumentType_SelectedIndexChanged(null, null);
		}

		protected void ddlDocumentType_SelectedIndexChanged(object sender, EventArgs e)
		{
			var documentTypeEnum = this.ddlDocumentType.GetSelectedNullableGuidEnum<Model.Entities.StudentDocument.DocumentTypes>();
			if (documentTypeEnum == null)
				this.ddlPage.DataBind(CommonListItems.None);
			else
			{
				var documentAttribute = Model.Entities.StudentDocument.GetDocumentAttribute(documentTypeEnum.Value);
				var documentPages = documentAttribute.Pages.Select(p => new ListItem
				{
					Text = p.Page.GetFullName(),
					Value = p.Page.GetGuid().ToString()
				}).ToList();
				this.ddlPage.DataBind(documentPages, CommonListItems.None);
			}
			this.ddlPage_SelectedIndexChanged(null, null);
		}

		protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			var intakeSemesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var missingDocumentTypeEnum = this.ddlDocumentType.GetSelectedNullableGuidEnum<StudentDocument.DocumentTypes>();
			var missingPageEnum = this.ddlPage.GetSelectedNullableGuidEnum<StudentDocument.Pages>();
			var pageSize = this.gvStudentDocuments.PageSize;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var (studentDocuments, virtualItemCount) = BL.Core.StudentInformation.Staff.StudentDocuments.GetStudentDocuments(intakeSemesterID, departmentID, programID, missingDocumentTypeEnum, missingPageEnum, pageIndex, pageSize, sortExpression, sortDirection, this.StaffIdentity.LoginSessionGuid);
			this.gvStudentDocuments.DataBind(studentDocuments, pageIndex, virtualItemCount, sortExpression, sortDirection);
		}

		protected void gvStudentDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvStudentDocuments_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvStudentDocuments.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvStudentDocuments_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}
	}
}