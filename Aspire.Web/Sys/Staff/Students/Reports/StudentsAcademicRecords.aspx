﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="StudentsAcademicRecords.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.Reports.StudentsAcademicRecords" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" AutoPostBack="True" CausesValidation="False" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter"  ValidationGroup="Parameters" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Percentage:" AssociatedControlID="tbPercentage" />
			<aspire:AspireTextBox runat="server" ID="tbPercentage" ValidationGroup="Parameters" />
		</div>
	</div>
	<p></p>
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Degree Type:" AssociatedControlID="cblDegreeType" />
			<aspire:AspireCheckBoxList runat="server" ID="cblDegreeType"  ValidationGroup="Parameters" RepeatLayout="Flow" RepeatDirection="Horizontal" />
		</div>
	</div>
	<p></p>
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireButton runat="server" ID="btnDisplayReport" Text="Display Report" ButtonType="Primary" ValidationGroup="Parameters" ToolTip="Display Report" OnClick="btnDisplayReport_OnClick" />
		</div>
	</div>

</asp:Content>
