﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Students
{
	[AspirePage("B738EF12-1058-4CA4-8581-C97CC0013908", UserTypes.Staff, "~/Sys/Staff/Students/StudentProfileView.aspx", "Student Profile View", true, AspireModules.None)]
	public partial class StudentProfileView : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_user);
		public override string PageTitle => "Student Profile";

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.StudentInformation.Module,new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};

		public static string GetPageUrl(string enrollment, bool search = true)
		{
			return GetAspirePageAttribute<StudentProfileView>().PageUrl.AttachQueryParams("Enrollment", enrollment, "Search", search);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.panelInfo.Visible = false;
				var enrollment = this.Request.GetParameterValue("Enrollment");
				this.tbEnrollment.Enrollment = enrollment;
				if (this.Request.GetParameterValue<bool>("Search") == true)
					this.tbEnrollment_OnSearch(null, null);
			}
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs args)
		{
			var enrollment = this.tbEnrollment.Enrollment;
			var student = Aspire.BL.Core.StudentInformation.Staff.StudentProfile.GetStudent(enrollment, this.AspireIdentity.LoginSessionGuid);
			if (student == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect(GetPageUrl(enrollment, false));
				return;
			}
			this.ViewState[nameof(Model.Entities.Student.StudentID)] = student.StudentID;
			this.ViewState[nameof(Model.Entities.Student.Enrollment)] = student.Enrollment;
			this.LoadStudentProfileDetail(student);
			this.panelInfo.Visible = true;
		}

		private void LoadStudentProfileDetail(Model.Entities.Student student)
		{
			//todo student picture
			//this.imgPicture.ImageUrl = Aspire.Web.Sys.Common.CandidatePicture.GetImageUrl(student.StudentID,student EncryptionModes.ConstantSaltNoneUserSession);
			this.lblEnrollment.Text = student.Enrollment;
			this.lblRegistrationNo.Text = student.RegistrationNo.ToString().ToNAIfNullOrEmpty();
			this.lblProgram.Text = student.AdmissionOpenProgram.Program.ProgramAlias;
			this.lblIntakeSemester.Text = student.AdmissionOpenProgram.SemesterID.ToSemesterString();
			this.lblName.Text = student.Name.ToNAIfNullOrEmpty();
			this.lblFatherName.Text = student.FatherName.ToNAIfNullOrEmpty();
			this.lblUniversityEmail.Text = student.UniversityEmail.ToNAIfNullOrEmpty();
			this.lblPersonalEmail.Text = student.PersonalEmail.ToNAIfNullOrEmpty();
			this.lblGender.Text = student.GenderEnum.ToFullName().ToNAIfNullOrEmpty();
			this.lblCategory.Text = student.CategoryEnum.ToFullName().ToNAIfNullOrEmpty();
			this.lblNationality.Text = student.Nationality.ToNAIfNullOrEmpty();
			this.lblCountry.Text = student.Country.ToNAIfNullOrEmpty();
			this.lblProvince.Text = student.Province.ToNAIfNullOrEmpty();
			this.lblDistrict.Text = student.District.ToNAIfNullOrEmpty();
			this.lblTehsil.Text = student.Tehsil.ToNAIfNullOrEmpty();
			this.lblDomicile.Text = student.Domicile.ToNAIfNullOrEmpty();
			this.lblAreaType.Text = student.AreaTypeEnum?.ToFullName().ToNAIfNullOrEmpty();
			this.lblEmergencyContactName.Text = student.EmergencyContactName.ToNAIfNullOrEmpty();
			this.lblEmergencyMobile.Text = student.EmergencyMobile.ToNAIfNullOrEmpty();
			this.lblEmergencyPhone.Text = student.EmergencyPhone.ToNAIfNullOrEmpty();
			this.lblEmergencyRelationship.Text = student.EmergencyRelationship.ToNAIfNullOrEmpty();
			this.lblEmergencyAddress.Text = student.EmergencyAddress.ToNAIfNullOrEmpty();
			this.lblAreaType.Text = (student.AreaTypeEnum?.ToFullName()).ToNAIfNullOrEmpty();
			this.lblReligion.Text =student.Religion.ToNAIfNullOrEmpty();
			this.lblNextOfKin.Text =student.NextOfKin.ToNAIfNullOrEmpty();
			this.lblStudentStatus.Text = (student.StatusEnum?.ToFullName()).ToNAIfNullOrEmpty();
		}
	}
}