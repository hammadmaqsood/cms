﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CommunityServices.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.CommunityServices" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Model.Entities" %>

<%@ Register TagPrefix="uc" TagName="StudentInfoControl" Src="~/Sys/Staff/Common/StudentInfoUserControl.ascx" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
			<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="Search" OnSearch="tbEnrollment_Search" />
		</div>
	</div>
	<p></p>
	<asp:Panel runat="server" ID="panelCommunitySupportProgram">
		<uc:StudentInfoControl runat="server" ID="ucStudentBasicInfo" />
		<aspire:AspireButton runat="server" ButtonType="Success" Glyphicon="plus" Text="Add" ID="btnAdd" OnClick="btnAdd_OnClick" />
		<p></p>
		<aspire:AspireGridView runat="server" ID="gvCommunityServices" AutoGenerateColumns="False" OnRowCommand="gvCommunityServices_OnRowCommand" ShowFooter="True">
			<Columns>
				<asp:TemplateField HeaderText="Semester">
					<ItemTemplate>
						<%#: ((short)this.Eval("SemesterID")).ToSemesterString() %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="Organization" DataField="Organization" />
				<asp:BoundField HeaderText="Job Title" DataField="JobTitle" />
				<asp:BoundField HeaderText="Completed Date" DataField="CompletedDate" DataFormatString="{0:dd-MMM-yyyy}" />
				<asp:BoundField HeaderText="Hours" DataField="Hours" />
				<asp:TemplateField>
					<ItemTemplate>
						<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument="<%# Eval(nameof(StudentCommunityService.StudentCommunityServiceID)) %>" />
						<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument="<%# Eval(nameof(StudentCommunityService.StudentCommunityServiceID)) %>" ConfirmMessage="Are you sure want to delete community service record?" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
				<FooterStyle Font-Bold="True"></FooterStyle>
		</aspire:AspireGridView>
		<aspire:AspireModal runat="server" ID="modalCommunityServices">
			<BodyTemplate>
				<asp:UpdatePanel runat="server">
					<ContentTemplate>
						<aspire:AspireAlert runat="server" ID="alertCommunityServices" />
						<div class="form-horizontal">
							<div class="form-group">
								<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Semester:" AssociatedControlID="ddlSemester" />
								<div class="col-md-9">
									<aspire:AspireDropDownList runat="server" ID="ddlSemester" />
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemester" ErrorMessage="This field is required." ValidationGroup="CommunityServices" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Organization:" AssociatedControlID="tbOrganization" />
								<div class="col-md-9">
									<aspire:AspireTextBox runat="server" ID="tbOrganization" MaxLength="500" />
								</div>
							</div>

							<div class="form-group">
								<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Job Title:" AssociatedControlID="tbJobTitle" />
								<div class="col-md-9">
									<aspire:AspireTextBox runat="server" ID="tbJobTitle" MaxLength="500" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Completed Date:" AssociatedControlID="dtpCompletedDate" />
								<div class="col-md-9">
									<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpCompletedDate" />
									<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dtpCompletedDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." AllowNull="False" ValidationGroup="CommunityServices" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel CssClass="col-md-3" runat="server" Text="Credit Hours:" AssociatedControlID="tbCreditHours" />
								<div class="col-md-9">
									<aspire:AspireTextBox runat="server" ID="tbCreditHours" />
									<aspire:AspireByteValidator runat="server" ControlToValidate="tbCreditHours" ErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." AllowNull="False" ValidationGroup="CommunityServices" />
								</div>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</BodyTemplate>
			<FooterTemplate>
				<asp:UpdatePanel runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<aspire:AspireButton runat="server" Text="Save" ButtonType="Primary" ID="btnSave" OnClick="btnSave_OnClick" ValidationGroup="CommunityServices" />
						<aspire:AspireModalCloseButton runat="server" CausesValidation="false">Cancel</aspire:AspireModalCloseButton>
					</ContentTemplate>
				</asp:UpdatePanel>
			</FooterTemplate>
		</aspire:AspireModal>
	</asp:Panel>
</asp:Content>
