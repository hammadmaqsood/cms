﻿using Aspire.BL.Core.StudentInformation.Staff;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Students
{
	[AspirePage("43F920E8-9D98-49BD-BA56-D4E98A6AAAF5", UserTypes.Staff, "~/Sys/Staff/Students/DisciplineCases.aspx", "Discipline Cases", true, AspireModules.StudentInformation)]
	public partial class DisciplineCases : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.check);
		public override string PageTitle => "Discipline Cases";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.StudentInformation.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public static string GetPageUrl(int? disciplineCaseID)
		{
			return GetAspirePageAttribute<DisciplineCases>().PageUrl.AttachQueryParam(nameof(DisciplineCas.DisciplineCaseID), disciplineCaseID);
		}

		public static void Redirect(int? disciplineCaseID)
		{
			Redirect(GetPageUrl(disciplineCaseID));
		}

		public int? DisciplineCaseID => this.Request.GetParameterValue<int>("DisciplineCaseID");

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("CaseFileNo", SortDirection.Ascending);
				this.GetData(0);
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var caseDate = this.dtpCaseDate.SelectedDate.Value;
			var location = this.tbLocation.Text;
			var result = BL.Core.StudentInformation.Staff.DisciplineCase.AddDisciplineCase(caseDate, location, this.StaffIdentity.LoginSessionGuid);
			switch (result)
			{
				case DisciplineCase.AddDisciplineCaseStatuses.AlreadyExists:
					this.AddErrorAlert("Case number already exists.");
					break;
				case DisciplineCase.AddDisciplineCaseStatuses.Success:
					this.AddSuccessMessageHasBeenAdded("Discipline case");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			Redirect<DisciplineCases>();
		}

		protected void gvDisciplineCases_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvDisciplineCases.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvDisciplineCases_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvDisciplineCases_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvDisciplineCasesStudent_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					var result = BL.Core.StudentInformation.Staff.DisciplineCase.DeleteDisciplineCasesStudent(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					switch (result)
					{
						case DisciplineCase.DeleteDisciplineCasesStudentStatues.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case DisciplineCase.DeleteDisciplineCasesStudentStatues.RemarksIsExists:
							this.AddErrorAlert("Remarks of student exists record can't be delete.");
							break;
						case DisciplineCase.DeleteDisciplineCasesStudentStatues.Success:
							this.AddSuccessMessageHasBeenDeleted("Student");
							this.GetData(0);
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		private void GetData(int pageIndex)
		{
			int virtualItemCount = 0;
			var disciplineCases = BL.Core.StudentInformation.Staff.DisciplineCase.GetDisciplineCases(pageIndex, this.gvDisciplineCases.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out virtualItemCount, this.StaffIdentity.LoginSessionGuid);
			this.gvDisciplineCases.DataBind(disciplineCases, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
			this.SelectDisciplineCaseRow(this.DisciplineCaseID);
		}

		private void SelectDisciplineCaseRow(int? disciplineCaseID)
		{
			foreach (GridViewRow rowDisciplineCase in this.gvDisciplineCases.Rows)
			{
				var disciplineCaseIDFromRow = this.gvDisciplineCases.DataKeys[rowDisciplineCase.RowIndex].Value.ToString().ToInt();
				if (disciplineCaseIDFromRow == disciplineCaseID && disciplineCaseID != null)
				{
					this.gvDisciplineCases.SelectedIndex = rowDisciplineCase.RowIndex;
					return;
				}
			}
		}

		protected void gvDisciplineCases_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					var result = BL.Core.StudentInformation.Staff.DisciplineCase.DeleteDisciplineCase(e.DecryptedCommandArgumentToInt(), this.StaffIdentity.LoginSessionGuid);
					switch (result)
					{
						case DisciplineCase.DeleteDisciplineCasesStatues.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case DisciplineCase.DeleteDisciplineCasesStatues.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Discipline case");
							break;
						case DisciplineCase.DeleteDisciplineCasesStatues.Success:
							this.AddSuccessMessageHasBeenDeleted("Discipline case");
							Redirect<DisciplineCases>();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
				case "Students":
					var disciplineCaseID = e.DecryptedCommandArgumentToInt();
					var disciplineCaseStudents = BL.Core.StudentInformation.Staff.DisciplineCase.GetDisciplineCasesStudents(disciplineCaseID, this.StaffIdentity.LoginSessionGuid);
					if (!disciplineCaseStudents.Any()) return;
					var disciplineCase = disciplineCaseStudents.FirstOrDefault()?.DisciplineCase;
					this.rptDisciplineCase.DataBind(new[] { disciplineCase });
					this.gvDisciplineCasesStudent.DataBind(disciplineCaseStudents);
					this.SelectDisciplineCaseRow(disciplineCase?.DisciplineCaseID);
					this.modelDisciplineCasesStudent.Show();

					break;
			}
		}
	}
}