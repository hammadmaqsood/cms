﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="SearchStudents.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Students.SearchStudents" %>

<%@ Import Namespace="Aspire.Web.Sys.Staff.CourseRegistration" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:Panel runat="server" CssClass="" DefaultButton="btnSearch">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollmentFilter" />
					<aspire:AspireTextBox runat="server" ID="tbEnrollmentFilter" ValidationGroup="SearchStudent" />
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Registration No.:" AssociatedControlID="tbRegistrationNoFilter" />
					<aspire:AspireTextBox runat="server" ID="tbRegistrationNoFilter" ValidationGroup="SearchStudent" />
					<aspire:AspireUInt16Validator AllowNull="True" runat="server" ControlToValidate="tbRegistrationNoFilter" ValidationGroup="SearchStudent" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Registration No." RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbNameFilter" />
					<aspire:AspireTextBox runat="server" ID="tbNameFilter" ValidationGroup="SearchStudent" TextTransform="UpperCase" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Intake Semester:" AssociatedControlID="ddlSemesterFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterFilter" ValidationGroup="SearchStudent" />
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlProgramFilter" ValidationGroup="SearchStudent" />
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="CNIC:" AssociatedControlID="tbCnicFilter" />
					<aspire:AspireTextBox runat="server" ID="tbCnicFilter" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="form-group">
					<aspire:AspireButton runat="server" Text="Search" CssClass="btn-block" ButtonType="Primary" ID="btnSearch" ValidationGroup="SearchStudent" OnClick="btnSearch_OnClick" />
				</div>
			</div>
		</div>
	</asp:Panel>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvSearchStudent" ItemType="Aspire.BL.Core.StudentInformation.Staff.StudentSearch.Student" AutoGenerateColumns="False" AllowSorting="True" OnPageIndexChanging="gvSearchStudent_OnPageIndexChanging" OnPageSizeChanging="gvSearchStudent_OnPageSizeChanging" OnSorting="gvSearchStudent_OnSorting" PageSize="100">
		<Columns>
			<asp:TemplateField HeaderText="Enrollment" SortExpression="Enrollment">
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" Text="<%# Item.Enrollment %>" NavigateUrl="<%# Aspire.Web.Sys.Staff.Students.Profile.Profile.GetPageUrl(Item.Enrollment,true) %>" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="Enrollment" HeaderText="Enrollment" SortExpression="Enrollment" />
			<asp:BoundField DataField="RegistrationNo" HeaderText="Reg. No." SortExpression="RegistrationNo" />
			<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
			<asp:BoundField DataField="FatherName" HeaderText="Father Name" SortExpression="FatherName" />
			<asp:BoundField DataField="IntakeSemester" HeaderText="Intake Semester" SortExpression="IntakeSemesterID" />
			<asp:BoundField DataField="ProgramAlias" HeaderText="Program" SortExpression="ProgramAlias" />
			<asp:BoundField DataField="CNIC" HeaderText="CNIC" SortExpression="CNIC" />
			<asp:TemplateField>
				<ItemTemplate>
					<div class="btn-group">
						<a href="#" class="dropdown-toggle text-nowrap" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions<span class="caret"></span></a>
						<ul class="dropdown-menu dropdown-menu-right">
							<li>
								<aspire:AspireHyperLink runat="server" Text="Registered Courses" NavigateUrl="<%# RegisterCourse.GetPageUrl(null,Item.Enrollment) %>" Target="_blank" />
							</li>
							<li>
								<aspire:AspireHyperLink runat="server" Text="Profile" NavigateUrl="<%# Aspire.Web.Sys.Staff.Students.Profile.Profile.GetPageUrl(Item.Enrollment,true) %>" Target="_blank" />
							</li>
						</ul>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
