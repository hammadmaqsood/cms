﻿using Aspire.BL.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Students
{
	[AspirePage("B37BDDC1-C4AE-450E-AE79-468E430E2DDD", UserTypes.Staff, "~/Sys/Staff/Students/AcademicRecords.aspx", "Academic Records", true, AspireModules.StudentInformation)]
	public partial class AcademicRecords : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_graduation_cap);
		public override string PageTitle => "Academic Records";

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.StudentInformation.Module, UserGroupPermission.Allowed},
		};

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Enrollment", SortDirection.Ascending);
				this.ddlIntakeSemesterID.FillSemesters();
				this.ddlProgramID.FillPrograms(this.StaffIdentity.InstituteID, null, true, CommonListItems.Select);
				this.cblDegreeTypes.FillDegreeTypes();
				this.ddlIntakeSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlIntakeSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlProgramID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.cblDegreeTypes_OnSelectedIndexChanged(null, null);
		}

		protected void cblDegreeTypes_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData();
		}

		private void GetData()
		{
			var intakeSemesterID = this.ddlIntakeSemesterID.SelectedValue.ToNullableShort();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var degreeTypes = this.cblDegreeTypes.GetSelectedEnumValues<DegreeTypes>().ToList();
			if (intakeSemesterID == null || programID == null || degreeTypes?.Any() != true)
			{
				this.gvStudents.DataBind(null);
				return;
			}

			var studentAcademicRecords = Aspire.BL.Core.StudentInformation.Staff.StudentProfile.GetStudentAcademicRecords(intakeSemesterID.Value, programID.Value, degreeTypes, this.StaffIdentity.LoginSessionGuid);
			var result = studentAcademicRecords.SelectMany(sar =>
			{
				if (!sar.AcademicRecords.Any())
					return new[]
					{
						new
						{
							sar.StudentID,
							sar.Enrollment,
							StudentProfileURL=Sys.Staff.Students.Profile.Profile.GetPageUrl(sar.Enrollment,true),
							sar.Name,
							sar.ProgramAlias,
							sar.IntakeSemesterID,
							IntakeSemester = sar.IntakeSemesterID.ToSemesterString(),
							StudentStatus = sar.StatusFullName,
							DegreeTypeFullName = (string) null,
							Marks=(string)null,
							IsCGPA = (string) null,
							StatusFullName = (string) null,
							Percentage=(double?)null,
							RowCssClass="danger",
						}
					};
				return sar.AcademicRecords.Select(ar => new
				{
					sar.StudentID,
					sar.Enrollment,
					StudentProfileURL = Sys.Staff.Students.Profile.Profile.GetPageUrl(sar.Enrollment, true),
					sar.Name,
					sar.ProgramAlias,
					sar.IntakeSemesterID,
					IntakeSemester = sar.IntakeSemesterID.ToSemesterString(),
					StudentStatus = sar.StatusFullName,
					ar.DegreeTypeFullName,
					Marks = $"{ar.ObtainedMarks}/{ar.TotalMarks}{(ar.IsCGPA != true ? $"={ar.Percentage:N2}%" : "")}",
					IsCGPA = (ar.IsCGPA ?? false).ToYesNo(),
					ar.StatusFullName,
					ar.Percentage,
					RowCssClass = (string)null,
				});
			});
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			switch (sortExpression)
			{
				case "Enrollment":
				case "StudentID":
					result = result.OrderBy(sortDirection, r => r.Enrollment);
					break;
				case "Name":
					result = result.OrderBy(sortDirection, r => r.Name).ThenBy(r => r.Enrollment);
					break;
				case "ProgramAlias":
					result = result.OrderBy(sortDirection, r => r.ProgramAlias).ThenBy(r => r.Enrollment);
					break;
				case "IntakeSemesterID":
				case "IntakeSemester":
					result = result.OrderBy(sortDirection, r => r.IntakeSemesterID).ThenBy(r => r.Enrollment);
					break;
				case "IsCGPA":
					result = result.OrderBy(sortDirection, r => r.IsCGPA).ThenBy(r => r.Enrollment);
					break;
				case "DegreeTypeFullName":
					result = result.OrderBy(sortDirection, r => r.DegreeTypeFullName).ThenBy(r => r.Enrollment);
					break;
				case "Marks":
				case "Percentage":
					result = result.OrderBy(sortDirection, r => r.Percentage).ThenBy(r => r.Enrollment);
					break;
				case "StudentStatus":
					result = result.OrderBy(sortDirection, r => r.StudentStatus).ThenBy(r => r.Enrollment);
					break;
				case "StatusFullName":
					result = result.OrderBy(sortDirection, r => r.StatusFullName).ThenBy(r => r.Enrollment);
					break;
				default:
					throw new SortingNotImplementedException(sortExpression);
			}
			this.gvStudents.DataBind(result.ToList(), sortExpression, sortDirection);
		}

		protected void gvStudents_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData();
		}

		protected void gvStudents_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			var rowCssClass = (string)DataBinder.Eval(e.Row.DataItem, "RowCssClass");
			if (rowCssClass != null)
				e.Row.CssClass = rowCssClass;
		}
	}
}