﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Staff
{
	[AspirePage("EFAFBADB-4EC6-4F7D-BEE4-ADEE22CA8A57", UserTypes.Staff, "~/Sys/Staff/PrepareSession.aspx", null, false, AspireModules.None)]
	public partial class PrepareSession : BasePage
	{
		protected override void Authenticate()
		{
			if (StaffIdentity.Current == null)
				throw new AspireAccessDeniedException(UserTypes.Staff, UserTypes.Staff);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SessionHelper.SideBarLinkHtml == null)
			{
				PrepareMenu();
			}
			Redirect(GetPageUrl<Dashboard>());
		}

		private static void PrepareMenu()
		{
			var homePageAspirePageAttribute = AspirePageAttributesDictionary.GetAspirePageAttribute<Dashboard>();
			var homeAspireSideBarLink = new AspireSideBarLink(null, homePageAspirePageAttribute.AspirePageGuid, homePageAspirePageAttribute.MenuItemText, homePageAspirePageAttribute.PageUrl, true, null, null);
			var userGroupMenuLinks = Aspire.BL.Core.LoginManagement.Staff.GetUserGroupMenuLinks(StaffIdentity.Current.LoginSessionGuid);

			var sidebarLinks = userGroupMenuLinks.ToAspireSideBarLinks(homeAspireSideBarLink);
			SessionHelper.SetSideBarLinks(sidebarLinks);
		}
	}
}