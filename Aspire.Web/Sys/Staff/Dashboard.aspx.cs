﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff
{
	[AspirePage("{F1ECF22D-0F14-419F-B91F-0351F0ECC56A}", UserTypes.Staff, "~/Sys/Staff/Dashboard.aspx", "Home", false, AspireModules.None)]
	public partial class Dashboard : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => null;

		protected void Page_Load(object sender, EventArgs e)
		{
		}

		public override bool BreadcrumbVisible => false;
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_desktop);
		public override string PageTitle => "Dashboard";
	}
}