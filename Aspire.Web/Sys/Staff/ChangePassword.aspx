﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="Aspire.Web.Sys.Staff.ChangePassword" %>

<%@ Register Src="~/Sys/Common/ChangePassword.ascx" TagPrefix="uc1" TagName="ChangePassword" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc1:ChangePassword runat="server" id="userControlChangePassword" />
</asp:Content>
