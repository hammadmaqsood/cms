using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff
{
	public abstract class StaffPage : AspireBasePage, IBreadcrumb, ISidebar, IProfileInfo
	{
		protected abstract IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired { get; }
		protected override void Authenticate()
		{
			if (this.StaffIdentity == null)
				throw new AspireAccessDeniedException(UserTypes.Staff, UserTypes.Staff);
			if (this.PermissionsRequired != null)
				foreach (var permission in this.PermissionsRequired)
				{
					this.StaffIdentity.Demand(permission.Key, permission.Value);
					return;
				}
		}

		private IStaffIdentity _staffIdentity;
		public IStaffIdentity StaffIdentity => this._staffIdentity ?? (this._staffIdentity = this.AspireIdentity as IStaffIdentity);

		public override AspireThemes AspireTheme => AspireDefaultThemes.Staff;

		public virtual bool BreadcrumbVisible => true;

		public override bool PageTitleVisible => true;

		public virtual bool SidebarVisible => true;

		public virtual string SidebarHtml => SessionHelper.SideBarLinkHtml;

		public virtual string BreadcrumbHtml => SessionHelper.SideBarLinks?.GetBreadcrumbHtml(this.AspirePageAttribute.AspirePageGuid);

		public virtual bool ProfileInfoVisible => false;

		public virtual bool ProfileLinkVisible => false;

		public bool IsCustomProfileImage => false;

		public string CustomProfileImageUrl => null;

		public virtual string ProfileNavigateUrl => null;

		public virtual bool LogoffLinkVisible => true;

		public virtual string LogoffNavigateUrl => Logoff.GetPageUrl(UserLoginHistory.LogOffReasons.LogOff);

		public virtual string Username => this.StaffIdentity.Name;

		public virtual string Name => this.StaffIdentity.Name;

		public virtual string Email => this.StaffIdentity.Email;
	}
}