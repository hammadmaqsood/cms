﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Complaints
{
	[AspirePage("B312AA81-8DF2-482F-A7DB-0D22C76CE928", UserTypes.Staff, "~/Sys/Staff/Complaints/StudentComplaints.aspx", "Student Complaints", true, AspireModules.Complaints)]
	public partial class StudentComplaints : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.warning_sign);
		public override string PageTitle => "Student Complaints";

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Complaints.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(nameof(Complaint.ComplaintNo), SortDirection.Ascending);
				this.ddlFilterStatus.FillGuidEnums<Complaint.Statuses>(CommonListItems.All);
				this.GetData(0);
			}
		}

		protected void gvComplaints_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvComplaints.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvComplaints_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvComplaints_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			var complaintNo = this.tbFilterComplaintNo.Text;
			var enrollment = this.tbFilterEnrollment.Text;
			var complaintDateFrom = this.tbFilterComplaintDateFrom.SelectedDate;
			var complaintDateTo = this.tbFilterComplaintDateTo.SelectedDate;
			var status = this.ddlFilterStatus.SelectedValue.ToNullableGuid();
			var complaints = BL.Core.Complaints.Common.ComplaintComment.GetComplaints(pageIndex, this.gvComplaints.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out int virtualItemCount, this.StaffIdentity.LoginSessionGuid, enrollment, complaintNo, complaintDateFrom, complaintDateTo, status);
			//	var disciplineCases = BL.Core.StudentInformation.Staff.DisciplineCase.GetDisciplineCases(pageIndex, this.gvDisciplineCases.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out virtualItemCount, this.StaffIdentity.LoginSessionGuid);
			this.gvComplaints.DataBind(complaints, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		protected void btnDisplayComplaints_OnClick(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void ddlFilterStatus_OnSelectedIndexChanged_(object sender, EventArgs e)
		{
			this.btnDisplayComplaints_OnClick(null, null);
		}

		protected void gvComplaints_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Status":
					var complaintID = e.DecryptedCommandArgumentToGuid();
					var result = BL.Core.Complaints.Common.ComplaintComment.ToggleComplaintStatus(complaintID, this.StaffIdentity.LoginSessionGuid);
					switch (result)
					{
						case BL.Core.Complaints.Common.ComplaintComment.ToggleComplaintStatusResults.NoRecordFound:
							this.AddNoRecordFoundAlert();
							Redirect<StudentComplaints>();
							break;
						case BL.Core.Complaints.Common.ComplaintComment.ToggleComplaintStatusResults.Success:
							this.AddSuccessMessageHasBeenUpdated("Complaint Status");
							Redirect<StudentComplaints>();
							break;
						default:
							throw new NotImplementedEnumException(result);
					}
					break;
				case "Comment":
					var complaintIDForComment = e.DecryptedCommandArgumentToGuid();
					this.ucCommentComplaint.ShowModal(complaintIDForComment);
					break;
			}

		}
	}
}