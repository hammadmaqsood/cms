﻿<%@ Page Title="Student Complaints" Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="StudentComplaints.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Complaints.StudentComplaints" %>

<%@ Register Src="~/Sys/Common/Complaints/ComplaintComment.ascx" TagPrefix="uc1" TagName="CommentComplaint" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbFilterEnrollment" />
					<aspire:AspireTextBox runat="server" ID="tbFilterEnrollment" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Complaint No:" AssociatedControlID="tbFilterComplaintNo" />
					<aspire:AspireTextBox runat="server" ID="tbFilterComplaintNo" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Complaint Date From:" AssociatedControlID="tbFilterComplaintDateFrom" />
					<aspire:AspireDateTimePickerTextbox runat="server" ID="tbFilterComplaintDateFrom" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Complaint Date To:" AssociatedControlID="tbFilterComplaintDateTo" />
					<aspire:AspireDateTimePickerTextbox runat="server" ID="tbFilterComplaintDateTo" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlFilterStatus" />
					<aspire:AspireDropDownList runat="server" ID="ddlFilterStatus" OnSelectedIndexChanged="ddlFilterStatus_OnSelectedIndexChanged_" AutoPostBack="True" />
				</div>
				<div class="form-group">
					<aspire:AspireButton runat="server" ID="btnDisplayComplaints" Text="Display Complaints" OnClick="btnDisplayComplaints_OnClick" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Complaints <span class="glyphicon glyphicon-alert"></span></h3>
		</div>
		<aspire:AspireGridView runat="server" ID="gvComplaints" ItemType="Aspire.BL.Core.Complaints.Common.ComplaintComment.Complaint" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnPageSizeChanging="gvComplaints_OnPageSizeChanging" OnPageIndexChanging="gvComplaints_OnPageIndexChanging" OnSorting="gvComplaints_OnSorting" CssClass="table-condensed removeMargin" OnRowCommand="gvComplaints_OnRowCommand">
			<Columns>
				<asp:TemplateField HeaderText="#.">
					<ItemTemplate>
						<%#   Container.DataItemIndex+1 %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="Enrollment" DataField="Enrollment" SortExpression="Enrollment" />
				<asp:BoundField HeaderText="Name" DataField="Name" SortExpression="Name" />
				<asp:BoundField HeaderText="ProgramAlias" DataField="ProgramAlias" SortExpression="ProgramAlias" />
				<asp:BoundField HeaderText="Complaint No" DataField="ComplaintNo" SortExpression="ComplaintNo" />
				<asp:BoundField HeaderText="Complaint Type" DataField="ComplaintTypeName" SortExpression="ComplaintTypeName" />
				<asp:BoundField HeaderText="Description" DataField="Description" SortExpression="Description" />
				<asp:BoundField HeaderText="Registered Date" DataField="RegisteredDate" SortExpression="RegisteredDate" DataFormatString="{0:dd-MMMM-yyyy hh:mm:ss tt}" />
				<asp:TemplateField HeaderText="Status" SortExpression="Status">
					<ItemTemplate>
						<aspire:AspireLinkButton runat="server" CommandName="Status" CausesValidation="False" Text='<%#: Item.StatusEnumToFullName%>' EncryptedCommandArgument="<%#: Item.ComplaintID %>" />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="Status Change Date" DataField="StatusChangeDate" SortExpression="StatusChangeDate" DataFormatString="{0:dd-MMMM-yyyy hh:mm:ss tt}" />
				<asp:TemplateField HeaderText="Action">
					<ItemTemplate>
						<aspire:AspireLinkButton runat="server" CommandName="Comment" CausesValidation="False" Glyphicon="plus_sign" Text="Comment" EncryptedCommandArgument="<%#: Item.ComplaintID %>" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
	</div>
	<uc1:CommentComplaint runat="server" ID="ucCommentComplaint" />
</asp:Content>
