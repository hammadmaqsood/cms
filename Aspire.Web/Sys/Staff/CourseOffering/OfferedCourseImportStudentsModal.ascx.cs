﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.CourseOffering
{
	public partial class OfferedCourseImportStudentsModal : System.Web.UI.UserControl
	{
		private new OfferCourses Page => (OfferCourses)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Title", SortDirection.Ascending);
				this.Visible = false;
			}
		}

		private int? OfferedCourseID
		{
			get => (int?)this.ViewState[nameof(this.OfferedCourseID)];
			set => this.ViewState[nameof(this.OfferedCourseID)] = value;
		}
		private short? SemesterID
		{
			get => (short?)this.ViewState[nameof(this.SemesterID)];
			set => this.ViewState[nameof(this.SemesterID)] = value;
		}

		public void ShowModal(int offeredCourseID)
		{
			this.Page.StaffIdentity.Demand(StaffPermissions.CourseOffering.CanMergeClasses, Model.Entities.UserGroupPermission.PermissionValues.Allowed);
			this.OfferedCourseID = null;
			var offeredCourse = BL.Core.CourseOffering.Staff.OfferedCourses.GetOfferedCourse(offeredCourseID, this.Page.StaffIdentity.LoginSessionGuid);
			if (offeredCourse == null)
			{
				this.Page.AddNoRecordFoundAlert();
				this.Page.Refresh();
				return;
			}
			if (offeredCourse.AttendanceRecordExists)
			{
				this.Page.AddErrorAlert("Course Registration record can not be imported from other classes. Reason: Attendance record exists.");
				return;
			}
			this.offeredCourseInfo.DataBind(offeredCourse);
			this.OfferedCourseID = offeredCourse.OfferedCourseID;
			this.SemesterID = offeredCourse.SemesterID;
			this.ddlProgramID.FillPrograms(this.Page.StaffIdentity.InstituteID, null, true, CommonListItems.All);
			this.ddlProgramID_SelectedIndexChanged(null, null);
			this.Visible = true;
			this.modal.Show();
		}

		protected void ddlProgramID_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.OfferedCourseID == null || this.SemesterID == null)
				throw new InvalidOperationException();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			if (programID == null)
			{
				this.gvOfferedCourses.DataBindNull();
				return;
			}
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var offeredCourses = BL.Core.CourseOffering.Staff.OfferedCourses.GetOfferedCourses(this.SemesterID.Value, programID.Value, null, null, null, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.Page.StaffIdentity.LoginSessionGuid)
				.Where(oc => oc.OfferedCourseID != this.OfferedCourseID.Value);
			this.gvOfferedCourses.DataBind(offeredCourses, sortExpression, sortDirection);
		}

		protected void btnImport_Click(object sender, EventArgs e)
		{
			if (this.OfferedCourseID == null)
				throw new InvalidOperationException();

			var offeredCourseIDs = new List<int>();

			foreach (GridViewRow row in this.gvOfferedCourses.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow)
					continue;
				var cb = (CheckBox)row.FindControl("cb");
				if (!cb.Checked || !cb.Visible)
					continue;
				var hf = (Lib.WebControls.HiddenField)row.FindControl("hfOfferedCourseID");
				offeredCourseIDs.Add(hf.Value.ToInt());
			}

			if (!offeredCourseIDs.Any())
			{
				this.alert.AddWarningAlert("No course(s) selected.");
				this.updatePanel.Update();
				return;
			}
			var (status, importResults) = BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCourses(this.OfferedCourseID.Value, offeredCourseIDs, this.Page.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesStatuses.NoRecordFound:
					this.Page.AddNoRecordFoundAlert();
					this.Page.Refresh();
					return;
				case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesStatuses.OfferedCourseAttendancesExists:
					this.alert.AddErrorAlert($"Attendance record exists for \"{this.offeredCourseInfo.Title}\" @ {this.offeredCourseInfo.Class}.");
					this.updatePanel.Update();
					return;
				case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesStatuses.OfferedCourseExamsExists:
					this.alert.AddErrorAlert($"Examination record exists for \"{this.offeredCourseInfo.Title}\" @ {this.offeredCourseInfo.Class}.");
					this.updatePanel.Update();
					return;
				case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesStatuses.AssignmentsMarksSubmitted:
					this.alert.AddErrorAlert($"Assignments marks exist for \"{this.offeredCourseInfo.Title}\" @ {this.offeredCourseInfo.Class}.");
					this.updatePanel.Update();
					return;
				case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesStatuses.QuizzesMarksSubmitted:
					this.alert.AddErrorAlert($"Quizzes marks exist for \"{this.offeredCourseInfo.Title}\" @ {this.offeredCourseInfo.Class}.");
					this.updatePanel.Update();
					return;
				case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesStatuses.InternalsMarksSubmitted:
					this.alert.AddErrorAlert($"Internal marks exist for \"{this.offeredCourseInfo.Title}\" @ {this.offeredCourseInfo.Class}.");
					this.updatePanel.Update();
					return;
				case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesStatuses.MidMarksSubmitted:
					this.alert.AddErrorAlert($"Mid marks exist for \"{this.offeredCourseInfo.Title}\" @ {this.offeredCourseInfo.Class}.");
					this.updatePanel.Update();
					return;
				case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesStatuses.FinalMarksSubmitted:
					this.alert.AddErrorAlert($"Final marks exist for \"{this.offeredCourseInfo.Title}\" @ {this.offeredCourseInfo.Class}.");
					this.updatePanel.Update();
					return;
				case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesStatuses.MarksEntryLocked:
					this.alert.AddErrorAlert($"Marks entry is locked for \"{this.offeredCourseInfo.Title}\" @ {this.offeredCourseInfo.Class}.");
					this.updatePanel.Update();
					return;
				case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesStatuses.Failed:
					foreach (var importResult in importResults)
						switch (importResult.Status)
						{
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.NoRecordFound:
								this.Page.AddNoRecordFoundAlert();
								this.Page.Refresh();
								return;
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.FacultyMemberMismatched:
								this.alert.AddErrorAlert($"Faculty Member is not same. \"{importResult.Title}\" @ {importResult.Class}");
								break;
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.AttendanceRecordExists:
								this.alert.AddErrorAlert($"Attendance record exists for \"{importResult.Title}\" @ {importResult.Class}.");
								break;
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.OfferedCourseExamsExists:
								this.alert.AddErrorAlert($"Examination record exists for \"{importResult.Title}\" @ {importResult.Class}.");
								break;
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.AssignmentsMarksSubmitted:
								this.alert.AddErrorAlert($"Assignments marks exist for \"{importResult.Title}\" @ {importResult.Class}.");
								break;
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.QuizzesMarksSubmitted:
								this.alert.AddErrorAlert($"Quizzes marks exist for \"{importResult.Title}\" @ {importResult.Class}.");
								break;
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.InternalsMarksSubmitted:
								this.alert.AddErrorAlert($"Internal marks exist for \"{importResult.Title}\" @ {importResult.Class}.");
								break;
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.MidMarksSubmitted:
								this.alert.AddErrorAlert($"Mid marks exist for \"{importResult.Title}\" @ {importResult.Class}.");
								break;
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.FinalMarksSubmitted:
								this.alert.AddErrorAlert($"Final marks exist for \"{importResult.Title}\" @ {importResult.Class}.");
								break;
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.MarksEntryLocked:
								this.alert.AddErrorAlert($"Marks entry is locked for \"{importResult.Title}\" @ {importResult.Class}.");
								break;
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.Success:
								throw new InvalidOperationException();
							default:
								throw new ArgumentOutOfRangeException(nameof(importResult.Status), importResult.Status, null);
						}

					this.ddlProgramID_SelectedIndexChanged(null, null);
					this.updatePanel.Update();
					return;
				case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesStatuses.Success:
					foreach (var importResult in importResults)
						switch (importResult.Status)
						{
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.NoRecordFound:
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.FacultyMemberMismatched:
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.AttendanceRecordExists:
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.OfferedCourseExamsExists:
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.AssignmentsMarksSubmitted:
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.QuizzesMarksSubmitted:
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.InternalsMarksSubmitted:
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.MidMarksSubmitted:
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.FinalMarksSubmitted:
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.MarksEntryLocked:
								throw new InvalidOperationException();
							case BL.Core.CourseOffering.Staff.OfferedCourses.ImportRegisteredCoursesResult.Statuses.Success:
								this.alert.AddSuccessAlert($"{importResult.RegisteredCoursesCount} Registered Student(s) of \"{importResult.Title}\" @ {importResult.Class} are imported.");
								break;
							default:
								throw new ArgumentOutOfRangeException(nameof(importResult.Status), importResult.Status, null);
						}

					this.ddlProgramID_SelectedIndexChanged(null, null);
					this.updatePanel.Update();
					return;
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}
	}
}