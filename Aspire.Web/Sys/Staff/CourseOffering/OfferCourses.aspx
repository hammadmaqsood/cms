﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="OfferCourses.aspx.cs" Inherits="Aspire.Web.Sys.Staff.CourseOffering.OfferCourses" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Register Src="~/Sys/Staff/CourseOffering/OfferedCourseAssignFacultyMemberModal.ascx" TagPrefix="uc1" TagName="OfferedCourseAssignFacultyMemberModal" %>
<%@ Register Src="~/Sys/Staff/CourseOffering/OfferedCourseViewStudentsModal.ascx" TagPrefix="uc1" TagName="OfferedCourseViewStudentsModal" %>
<%@ Register Src="~/Sys/Staff/CourseOffering/OfferedCourseAssignClassesModal.ascx" TagPrefix="uc1" TagName="OfferedCourseAssignClassesModal" %>
<%@ Register Src="~/Sys/Staff/CourseOffering/OfferedCourseEditModal.ascx" TagPrefix="uc1" TagName="OfferedCourseEditModal" %>
<%@ Register Src="~/Sys/Staff/CourseOffering/OfferedCourseSelectCoursesModal.ascx" TagPrefix="uc1" TagName="OfferedCourseSelectCoursesModal" %>
<%@ Register Src="~/Sys/Staff/CourseOffering/OfferedCourseImportStudentsModal.ascx" TagPrefix="uc1" TagName="OfferedCourseImportStudentsModal" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Offering Semester:" AssociatedControlID="ddlSemesterID" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" AutoPostBack="True" CausesValidation="False" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
				<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" OnSelectedIndexChanged="ddlDepartmentID_OnSelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
				<aspire:AspireDropDownList runat="server" ID="ddlProgramID" OnSelectedIndexChanged="ddlProgramID_OnSelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-1">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Sem.#:" AssociatedControlID="ddlSemesterNo" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" CausesValidation="false" OnSelectedIndexChanged="ddlSemesterNo_OnSelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="&nbsp;" AssociatedControlID="btnSelectCourses" CssClass="visible-md visible-lg" />
				<aspire:AspireButton ID="btnSelectCourses" CssClass="btn-block" runat="server" ButtonType="Primary" Text="Select Course(s)" CausesValidation="False" OnClick="btnSelectCourses_OnClick" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Sections:" AssociatedControlID="cblSections" />
				<aspire:AspireCheckBoxList TextAlign="Right" runat="server" ID="cblSections" RepeatLayout="Flow" RepeatDirection="Horizontal" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="cblSections_OnSelectedIndexChanged" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Shifts:" AssociatedControlID="cblShifts" />
				<aspire:AspireCheckBoxList TextAlign="Right" runat="server" ID="cblShifts" RepeatLayout="Flow" RepeatDirection="Horizontal" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="cblShifts_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<div class="input-group" id="inputGroupSearchMain">
					<input type="text" class="form-control" placeholder="Search..." maxlength="50" id="tbSearchMain" />
					<div class="input-group-btn">
						<button type="button" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span></button>
						<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<aspire:AspireGridView runat="server" AllowSorting="True" ItemType="Aspire.BL.Core.CourseOffering.Staff.OfferedCourses.OfferedCourseForGrid" ID="gvOfferedCourses" OnRowCommand="gvOfferedCourses_OnRowCommand" AutoGenerateColumns="False" OnSorting="gvOfferedCourses_Sorting">
		<Columns>
			<asp:TemplateField ItemStyle-Wrap="False" HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" ToolTip="Import Students from other classes" Glyphicon="import" CausesValidation="False" CommandName="ImportStudents" EncryptedCommandArgument="<%# Item.OfferedCourseID %>" />
					<aspire:AspireLinkButton runat="server" ToolTip="Edit" Glyphicon="edit" CausesValidation="False" CommandName="Edit" EncryptedCommandArgument="<%# Item.OfferedCourseID %>" />
					<aspire:AspireLinkButton runat="server" Visible="<%# !Item.MarksEntryLocked %>" ToolTip="Delete" Glyphicon="remove" CausesValidation="False" CommandName="Delete" EncryptedCommandArgument="<%# Item.OfferedCourseID %>" ConfirmMessage='<%# $"Are you sure you want to delete offered course \"{Item.Title}\"?" %>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Title" SortExpression="Title" ItemStyle-Wrap="False">
				<ItemTemplate>
					<div>
						<span class="label label-primary" title="Roadmap"><%# Item.CourseSemesterID.ToSemesterString().ToUpper() %></span>
						<span class="label label-default" title="Course Code"><%# Item.CourseCode %></span>
						<span runat="server" visible="<%# Item.SpecialOffered %>" class="label label-danger">SPECIAL</span>
						<span runat="server" visible="<%# Item.Visiting %>" class="label label-info">VISITING</span>
					</div>
					<aspire:AspireHyperLink runat="server" Text="<%# Item.Title %>" NavigateUrl="<%# Aspire.Web.Sys.Staff.ClassAttendance.ViewClassAttendance.GetPageUrl(Item.OfferedCourseID) %>" Target="_blank" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField SortExpression="CreditHours" HeaderText="Credit<br/>Hours">
				<ItemTemplate><%#: Item.CreditHours.ToCreditHoursFullName() %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Majors" SortExpression="Major">
				<ItemTemplate><%#: Item.Major.ToNAIfNullOrEmpty() %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Class" ItemStyle-Wrap="False" SortExpression="PrimaryClassName">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CommandName="AssignClasses" EncryptedCommandArgument="<%# Item.OfferedCourseID %>">
						<%#: Item.PrimaryClassName %>
						<div>
							<small><em><%# Item.SecondaryClassNames.Join("\n").HtmlEncode(true) %></em></small>
						</div>
					</aspire:AspireLinkButton>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Teacher" SortExpression="FacultyMemberName">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" Text="<%# Item.FacultyMemberName.ToNAIfNullOrEmpty() %>" CommandName="AssignTeacher" EncryptedCommandArgument="<%# Item.OfferedCourseID %>" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Strength" SortExpression="MaxClassStrength">
				<ItemTemplate>
					<aspire:AspireLinkButton ToolTip="View Registered Students" runat="server" EncryptedCommandArgument="<%# Item.OfferedCourseID %>" Text="<%# Item.RegisteredStrength %>" CommandName="ViewStudents" CausesValidation="False" />
					/ <span title="Maximum Allowed"><%#: Item.MaxClassStrength %></span>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Status" SortExpression="Status">
				<ItemTemplate>
					<%#: Item.Status.GetFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Remarks" SortExpression="Remarks">
				<ItemTemplate>
					<%#: Item.Remarks.ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<div class="text-center">
		<aspire:AspireHyperLink runat="server" ID="hlOfferedCoursesReport" Text="Offered Courses Report" Target="_blank" Visible="True" />
	</div>
	<uc1:OfferedCourseAssignFacultyMemberModal runat="server" ID="offeredCourseAssignFacultyMemberModal" />
	<uc1:OfferedCourseViewStudentsModal runat="server" ID="offeredCourseViewStudentsModal" />
	<uc1:OfferedCourseAssignClassesModal runat="server" ID="offeredCourseAssignClassesModal" />
	<uc1:OfferedCourseEditModal runat="server" ID="offeredCourseEditModal" />
	<uc1:OfferedCourseSelectCoursesModal runat="server" ID="offeredCourseSelectCoursesModal" />
	<uc1:OfferedCourseImportStudentsModal runat="server" ID="offeredCourseImportStudentsModal" />

	<script type="text/javascript">
		$(function () {
			var inputGroupSearch = $("#inputGroupSearchMain");
			var tb = $("input", inputGroupSearch);
			var btnSearch = $("button.btn-primary", inputGroupSearch);
			var btnClear = $("button.btn-default", inputGroupSearch);
			btnSearch.click(function () {
				filterRows($("#<%=this.gvOfferedCourses.ClientID%>"), tb.val(), false);
			}).click();
			btnClear.click(function () {
				tb.val("");
				btnSearch.click();
			});
			tb.on("keypress", function () { filterRows($("#<%=this.gvOfferedCourses.ClientID%>"), tb.val(), false); });
			tb.on("keydown", function (event) {
				if (event.keyCode === 13) {
					event.preventDefault();
					return false;
				}
				return true;
			});
		});
	</script>
</asp:Content>
