﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfferedCourseSelectCoursesModal.ascx.cs" Inherits="Aspire.Web.Sys.Staff.CourseOffering.OfferedCourseSelectCoursesModal" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>

<aspire:AspireModal runat="server" ModalSize="Large" ID="modal" HeaderText="Select Courses for Offering">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alert" />
				<div class="row">
					<div class="col-md-2">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Offering Semester:" AssociatedControlID="lblOfferingSemester" />
							<div>
								<aspire:Label runat="server" ID="lblOfferingSemester" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="lblProgramAlias" />
							<div>
								<aspire:Label runat="server" ID="lblProgramAlias" />
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="lblSemesterNo" />
							<div>
								<aspire:Label runat="server" ID="lblSemesterNo" />
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Sections:" AssociatedControlID="lblSections" />
							<div>
								<aspire:Label runat="server" ID="lblSections" />
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Shifts:" AssociatedControlID="lblShifts" />
							<div>
								<aspire:Label runat="server" ID="lblShifts" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Roadmap:" AssociatedControlID="ddlAdmissionOpenProgramID" />
							<aspire:AspireDropDownList runat="server" ID="ddlAdmissionOpenProgramID" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlAdmissionOpenProgramID_OnSelectedIndexChanged" />
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="ddlSemesterNo" />
							<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" CausesValidation="False" />
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label for="tbSearch">Search:</label>
							<div class="input-group">
								<input class="form-control" type="text" id="tbSearch" placeholder="Search" />
								<div class="input-group-btn">
									<button type="button" id="btnSearch" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div style="overflow: auto; height: 300px; width: 100%;">
					<aspire:AspireGridView ItemType="Aspire.BL.Core.CourseOffering.Staff.OfferedCourses.Course" OnSorting="gvCourses_OnSorting" ID="gvCourses" runat="server" AllowSorting="True" AutoGenerateColumns="False">
						<Columns>
							<asp:TemplateField HeaderText="Select">
								<ItemTemplate>
									<asp:CheckBox runat="server" ID="cb" ClientIDMode="Static" ValidationGroup="SelectCourses" />
									<aspire:HiddenField runat="server" ID="hfCourseID" Value="<%# Item.CourseID %>" Mode="Encrypted" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="CourseCode" HeaderText="Code" SortExpression="CourseCode" />
							<asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
							<asp:BoundField DataField="CreditHours" HeaderText="Credit Hrs." SortExpression="CreditHours" />
							<asp:TemplateField HeaderText="Semester No." SortExpression="SemesterNo">
								<ItemTemplate>
									<aspire:AspireLabel runat="server" CssClass="SemNo" Text="<%# Item.SemesterNoEnum.ToFullName() %>" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="Majors" HeaderText="Majors" SortExpression="Majors" />
							<asp:TemplateField HeaderText="Status" SortExpression="Status">
								<ItemTemplate>
									<%#: Item.Status.GetFullName() %>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</aspire:AspireGridView>
				</div>
				<span class="help-block">Courses will be offered to the selected semester no., sections and shifts.</span>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Always">
			<ContentTemplate>
				<aspire:Label ID="lblSelectedCoursesCountForOffer" runat="server" />
				<aspire:AspireButton ButtonType="Primary" runat="server" ValidationGroup="SelectCourses" Text="Offer" ID="btnOffer" OnClick="btnOffer_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>
<script type="text/javascript">
	function init() {
		var gv = $("#<%=this.gvCourses.ClientID%>");
		if (gv.length === 0)
			return;
		var ddlSemNo = $("#<%=this.ddlSemesterNo.ClientID%>");
		var tbSearchText = $("#tbSearch");
		var btnSearch = $("#btnSearch");

		function filterCourses() {
			var semNo = ddlSemNo.find('option:selected').text().trim();
			if (semNo.toLowerCase() === "All".toLowerCase())
				semNo = null;
			var searchText = tbSearchText.val().trim().toLowerCase();
			$("tr", gv).each(function (index, row) {
				if (index === 0)
					return;
				var visible = true;
				if (semNo != null && semNo.length > 0) {
					if ($("span.SemNo", $(row)).text().trim() !== semNo)
						visible = false;
				}
				if (visible) {
					if (searchText != null && searchText.length > 0)
						if ($(row).text().toLowerCase().indexOf(searchText) < 0)
							visible = false;
				}
				if (visible)
					$(row).show();
				else {
					if ($("th", $(row)).length === 0)
						$(row).hide();
				}
			});
		}
		ddlSemNo.change(filterCourses);
		tbSearchText.on("change blur keyup keydown", filterCourses);
		btnSearch.on("click", function (e) {
			e.preventDefault();
			filterCourses();
		});
		filterCourses();
		var lblSelectedCoursesCountForOffer = $("#<%= this.lblSelectedCoursesCountForOffer.ClientID %>");
		function onCheckBoxChange() {
			var selectedCoursesCountForOffer = $("input[type=checkbox]:checked", gv).length;
			$(lblSelectedCoursesCountForOffer).text(selectedCoursesCountForOffer + " course(s) selected");
		}
		$("input[type=checkbox]", gv).on("change", onCheckBoxChange);
		onCheckBoxChange();
	}
	Sys.Application.add_load(init);
</script>
