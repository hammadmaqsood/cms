﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfferedCourseImportStudentsModal.ascx.cs" Inherits="Aspire.Web.Sys.Staff.CourseOffering.OfferedCourseImportStudentsModal" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Register Src="~/Sys/Staff/CourseOffering/OfferedCourseInfo.ascx" TagPrefix="uc1" TagName="OfferedCourseInfo" %>

<aspire:AspireModal runat="server" ModalSize="Large" ID="modal" HeaderText="Import Students From Other Class">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alert" />
				<uc1:OfferedCourseInfo runat="server" ID="offeredCourseInfo" />
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
							<aspire:AspireDropDownList runat="server" ID="ddlProgramID" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlProgramID_SelectedIndexChanged" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="tbSearch">Search:</label>
							<div class="input-group">
								<input class="form-control" type="text" id="tbSearch" placeholder="Search" />
								<div class="input-group-btn">
									<button type="button" id="btnSearch" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div style="overflow: auto; height: 300px; width: 100%;">
					<aspire:AspireGridView ItemType="Aspire.BL.Core.CourseOffering.Staff.OfferedCourses.OfferedCourseForGrid" ID="gvOfferedCourses" runat="server" AutoGenerateColumns="False">
						<Columns>
							<asp:TemplateField HeaderText="Select">
								<ItemTemplate>
									<asp:CheckBox runat="server" Visible="<%# Item.RegisteredStrength > 0 %>" ID="cb" ClientIDMode="Static" ValidationGroup="Import" />
									<aspire:HiddenField runat="server" ID="hfOfferedCourseID" Value="<%# Item.OfferedCourseID %>" Mode="Encrypted" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="RegisteredStrength" HeaderText="Strength" />
							<asp:BoundField DataField="Title" HeaderText="Title" />
							<asp:BoundField DataField="CreditHours" HeaderText="Credit Hrs." />
							<asp:BoundField DataField="PrimaryClassName" HeaderText="Class" />
							<asp:TemplateField HeaderText="Faculty Member">
								<ItemTemplate>
									<%# Item.FacultyMemberName.ToNAIfNullOrEmpty() %>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</aspire:AspireGridView>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Always">
			<ContentTemplate>
				<aspire:Label ID="lblSelectedCoursesCountForImport" runat="server" />
				<aspire:AspireButton ButtonType="Primary" runat="server" ValidationGroup="Import" Text="Import" ID="btnImport" OnClick="btnImport_Click" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>
<script type="text/javascript">
	function initImport() {
		var gv = $("#<%=this.gvOfferedCourses.ClientID%>");
		if (gv.length === 0)
			return;
		var tbSearchText = $("#tbSearch");
		var btnSearch = $("#btnSearch");
		function filterCourses() {
			var searchText = tbSearchText.val().trim().toLowerCase();
			$("tr", gv).each(function (index, row) {
				if (index === 0)
					return;
				var visible = true;
				if (searchText != null && searchText.length > 0)
					if ($(row).text().toLowerCase().indexOf(searchText) < 0)
						visible = false;
				if (visible)
					$(row).show();
				else {
					if ($("th", $(row)).length === 0)
						$(row).hide();
				}
			});
		}
		tbSearchText.on("change blur keyup keydown", filterCourses);
		btnSearch.on("click", function (e) {
			e.preventDefault();
			filterCourses();
		});
		filterCourses();
		var lblSelectedCoursesCountForImport = $("#<%= this.lblSelectedCoursesCountForImport.ClientID %>");
		function onCheckBoxChange() {
			var selectedCoursesCountForImport = $("input[type=checkbox]:checked", gv).length;
			$(lblSelectedCoursesCountForImport).text(selectedCoursesCountForImport + " course(s) selected");
		}
		$("input[type=checkbox]", gv).on("change", onCheckBoxChange);
		onCheckBoxChange();
	}
	initImport();
	Sys.Application.add_load(initImport);
</script>
