﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.CourseOffering
{
	[AspirePage("42db9481-261f-4f0e-a026-2c3caadceb8c", UserTypes.Staff, "~/Sys/Staff/CourseOffering/OfferedCourses.aspx", "Offered Courses", true, AspireModules.CourseOffering)]
	public partial class OfferedCourses : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.CourseOffering.Module, new []{UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => AspireGlyphicons.book.GetIcon();
		public override string PageTitle => "Offered Courses";

		public static string GetPageUrl(short? semesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int? facultyMemberID, OnlineTeachingReadiness.ReadinessTypes? readinessTypeEnum, bool? submittedByClusterHead, bool? submittedByHOD, bool? submittedByDirector, bool? submittedByDG, int? pageIndex, int? pageSize, string sortExpression, SortDirection? sortDirection)
		{
			return typeof(OfferedCourses).GetAspirePageAttribute().PageUrl.AttachQueryParams(
				("SemesterID", semesterID),
				("DepartmentID", departmentID),
				("ProgramID", programID),
				("SemesterNo", semesterNoEnum),
				("Section", sectionEnum),
				("Shift", shiftEnum),
				("FacultyMemberID", facultyMemberID),
				("ReadinessTypeEnum", readinessTypeEnum),
				("SubmittedByClusterHead", submittedByClusterHead),
				("SubmittedByHOD", submittedByHOD),
				("SubmittedByDirector", submittedByDirector),
				("SubmittedByDG", submittedByDG),
				("PageIndex", pageIndex),
				("PageSize", pageSize),
				("SortExpression", sortExpression),
				("SortDirection", sortDirection));
		}

		public static void Redirect(short? semesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int? facultyMemberID, OnlineTeachingReadiness.ReadinessTypes? readinessTypeEnum, bool? submittedByClusterHead, bool? submittedByHOD, bool? submittedByDirector, bool? submittedByDG, int? pageIndex, int? pageSize, string sortExpression, SortDirection? sortDirection)
		{
			Redirect(GetPageUrl(semesterID, departmentID, programID, semesterNoEnum, sectionEnum, shiftEnum, facultyMemberID, readinessTypeEnum, submittedByClusterHead, submittedByHOD, submittedByDirector, submittedByDG, pageIndex, pageSize, sortExpression, sortDirection));
		}

		private void RefreshPage()
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
			var sectionEnum = this.ddlSection.GetSelectedEnumValue<Sections>(null);
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>(null);
			var facultyMemberID = this.ddlFacultyMemberID.SelectedValue.ToNullableInt();
			var readinessTypeEnum = this.ddlReadinessType.GetSelectedNullableGuidEnum<OnlineTeachingReadiness.ReadinessTypes>();
			var submittedByClusterHead = this.ddlSubmittedByClusterHead.SelectedValue.ToNullableBoolean();
			var submittedByHOD = this.ddlSubmittedByHOD.SelectedValue.ToNullableBoolean();
			var submittedByDirector = this.ddlSubmittedByDirector.SelectedValue.ToNullableBoolean();
			var submittedByDG = this.ddlSubmittedByDG.SelectedValue.ToNullableBoolean();
			var pageIndex = this.gvCourses.PageIndex;
			var pageSize = this.gvCourses.PageSize;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			Redirect(semesterID, departmentID, programID, semesterNoEnum, sectionEnum, shiftEnum, facultyMemberID, readinessTypeEnum, submittedByClusterHead, submittedByHOD, submittedByDirector, submittedByDG, pageIndex, pageSize, sortExpression, sortDirection);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var sortExpression = this.GetParameterValue("SortExpression") ?? "Title";
				var sortDirection = this.GetParameterValue<SortDirection>("SortDirection") ?? SortDirection.Ascending;
				this.ViewState.SetSortProperties(sortExpression, sortDirection);
				this.ddlSemesterID.FillSemesters().SelectedValue = "20201";
				this.ddlSemesterID.SetSelectedValueIfNotPostback("SemesterID");
				this.ddlDepartmentID.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All).SetSelectedValueIfNotPostback("DepartmentID");
				this.ddlSemesterNo.FillSemesterNos(CommonListItems.All).SetSelectedEnumValueIfNotPostback<SemesterNos>("SemesterNo");
				this.ddlSection.FillSections(CommonListItems.All).SetSelectedEnumValueIfNotPostback<Sections>("Section");
				this.ddlShift.FillShifts(CommonListItems.All).SetSelectedEnumValueIfNotPostback<Shifts>("Shift");
				this.ddlReadinessType.FillGuidEnums<OnlineTeachingReadiness.ReadinessTypes>(CommonListItems.All).SetSelectedEnumGuidValueIfNotPostback<OnlineTeachingReadiness.ReadinessTypes>("ReadinessTypeEnum");
				this.ddlSubmittedByClusterHead.FillYesNo(CommonListItems.All).SetSelectedBooleanValueIfNotPostback("SubmittedByClusterHead");
				this.ddlSubmittedByHOD.FillYesNo(CommonListItems.All).SetSelectedBooleanValueIfNotPostback("SubmittedByHOD");
				this.ddlSubmittedByDirector.FillYesNo(CommonListItems.All).SetSelectedBooleanValueIfNotPostback("SubmittedByDirector");
				this.ddlSubmittedByDG.FillYesNo(CommonListItems.All).SetSelectedBooleanValueIfNotPostback("SubmittedByDG");
				this.gvCourses.PageSize = 100;
				if (Configurations.Current.AppSettings.IsDevelopmentPC || Configurations.Current.AppSettings.DEBUG)
					this.gvCourses.PageSize = 10;
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		#region Filters

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var facultyMembers = BL.Core.Common.Lists.GetFacultyMembersList(this.StaffIdentity.InstituteID, semesterID);
			this.ddlFacultyMemberID.DataBind(facultyMembers, CommonListItems.All).SetSelectedValueIfNotPostback("FacultyMemberID");
			this.ddlDepartmentID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			this.ddlProgramID.FillPrograms(this.StaffIdentity.InstituteID, departmentID, true, CommonListItems.All).SetSelectedValueIfNotPostback("ProgramID");
			this.ddlProgramID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterNo_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterNo_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSection_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSection_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlShift_OnSelectedIndexChanged(null, null);
		}

		protected void ddlShift_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlFacultyMemberID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlFacultyMemberID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlReadinessType_SelectedIndexChanged(null, null);
		}

		protected void ddlReadinessType_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSubmittedByClusterHead_SelectedIndexChanged(null, null);
		}

		protected void ddlSubmittedByClusterHead_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSubmittedByHOD_SelectedIndexChanged(null, null);
		}

		protected void ddlSubmittedByHOD_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSubmittedByDirector_SelectedIndexChanged(null, null);
		}

		protected void ddlSubmittedByDirector_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSubmittedByDG_SelectedIndexChanged(null, null);
		}

		protected void ddlSubmittedByDG_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.gvCourses.PageSize = this.GetParameterValue<int>("PageSize") ?? this.gvCourses.PageSize;
				this.GetOfferedCourses(this.GetParameterValue<int>("PageIndex") ?? 0);
			}
			else
				this.GetOfferedCourses(0);
		}

		#endregion

		#region Gridview Events

		protected void gvCourses_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetOfferedCourses(0);
		}

		protected void gvCourses_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvCourses.PageSize = e.NewPageSize;
			this.GetOfferedCourses(0);
		}

		protected void gvCourses_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetOfferedCourses(e.NewPageIndex);
		}

		#endregion

		private void GetOfferedCourses(int pageIndex)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
			var sectionEnum = this.ddlSection.GetSelectedEnumValue<Sections>(null);
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>(null);
			var facultyMemberID = this.ddlFacultyMemberID.SelectedValue.ToNullableInt();
			var readinessTypeEnum = this.ddlReadinessType.GetSelectedNullableGuidEnum<OnlineTeachingReadiness.ReadinessTypes>();
			var submittedByClusterHead = this.ddlSubmittedByClusterHead.SelectedValue.ToNullableBoolean();
			var submittedByHOD = this.ddlSubmittedByHOD.SelectedValue.ToNullableBoolean();
			var submittedByDirector = this.ddlSubmittedByDirector.SelectedValue.ToNullableBoolean();
			var submittedByDG = this.ddlSubmittedByDG.SelectedValue.ToNullableBoolean();
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var (offeredCourses, virtualItemCount) = BL.Core.CourseOffering.Staff.OnlineTeachingReadiness.GetOfferedCourses(semesterID, departmentID, programID, semesterNoEnum, sectionEnum, shiftEnum, facultyMemberID, readinessTypeEnum, submittedByClusterHead, submittedByHOD, submittedByDirector, submittedByDG, pageIndex, this.gvCourses.PageSize, sortExpression, sortDirection, this.StaffIdentity.LoginSessionGuid);
			this.gvCourses.DataBind(offeredCourses, pageIndex, virtualItemCount, sortExpression, sortDirection);

			this.btnExport.Enabled = submittedByClusterHead == true || submittedByHOD == true || submittedByDirector == true || submittedByDG == true;

			this.btnSubmitAsHOD.Enabled = departmentID != null && submittedByClusterHead == true && submittedByHOD == false && offeredCourses.Any(oc => oc.SubmittedByClusterHead && !oc.SubmittedByHOD && !oc.SubmittedByDirector && !oc.SubmittedByDG);
			this.btnUndoSubmitAsHOD.Visible = departmentID != null && submittedByHOD == true && submittedByDirector == false && submittedByDG == false && offeredCourses.Any(oc => oc.SubmittedByHOD && !oc.SubmittedByDirector && !oc.SubmittedByDG);

			this.btnSubmitAsDirector.Enabled = submittedByHOD == true && submittedByDirector == false && offeredCourses.Any(oc => oc.SubmittedByClusterHead && oc.SubmittedByHOD && !oc.SubmittedByDirector && !oc.SubmittedByDG);
			this.btnUndoSubmitAsDirector.Visible = submittedByDirector == true && submittedByDG == false && offeredCourses.Any(oc => oc.SubmittedByDirector && !oc.SubmittedByDG);

			this.btnSubmitAsDG.Enabled = submittedByDirector == true && submittedByDG == false && offeredCourses.Any(oc => oc.SubmittedByClusterHead && oc.SubmittedByHOD && oc.SubmittedByDirector && !oc.SubmittedByDG);
			this.btnUndoSubmitAsDG.Visible = submittedByDG == true && offeredCourses.Any(oc => oc.SubmittedByDG);

			if (this.btnUndoSubmitAsHOD.Visible)
				this.btnUndoSubmitAsHOD.Visible = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.CourseOffering.CanUndoSubmitOnlineTeachingReadinessFormAsHOD) == UserGroupPermission.PermissionValues.Allowed;
			if (this.btnUndoSubmitAsDirector.Visible)
				this.btnUndoSubmitAsDirector.Visible = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.CourseOffering.CanUndoSubmitOnlineTeachingReadinessFormAsDirector) == UserGroupPermission.PermissionValues.Allowed;
			if (this.btnUndoSubmitAsDG.Visible)
				this.btnUndoSubmitAsDG.Visible = this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.CourseOffering.CanUndoSubmitOnlineTeachingReadinessFormAsDG) == UserGroupPermission.PermissionValues.Allowed;
		}

		private List<int> OfferedCourseIDs => this.gvCourses.Rows.Cast<GridViewRow>().Where(r => r.RowType == DataControlRowType.DataRow)
			.Select(r =>
			{
				var cb = (AspireCheckBox)r.FindControl("cbSelect");
				if (cb.Visible && cb.Checked)
					return ((System.Web.UI.WebControls.HiddenField)r.FindControl("hfOfferedCourseID")).Value.ToInt();
				return (int?)null;
			})
			.Where(id => id != null)
			.Select(id => id.Value)
			.ToList();

		protected void btnSubmitAsHOD_Click(object sender, EventArgs e)
		{
			var offeredCourseIDs = this.OfferedCourseIDs;
			if (!offeredCourseIDs.Any())
			{
				this.AddErrorAlert("No course is selected.");
				return;
			}
			var (updated, total) = BL.Core.CourseOffering.Staff.OnlineTeachingReadiness.SubmitOnlineTeachingReadiness(offeredCourseIDs, true, false, false, this.StaffIdentity.LoginSessionGuid);
			if (updated == 1 || updated == 0)
				this.AddSuccessAlert($"{updated} record is verified by HOD.");
			else
				this.AddSuccessAlert($"{updated} records are verified by HOD.");
			this.RefreshPage();
		}

		protected void btnUndoSubmitAsHOD_Click(object sender, EventArgs e)
		{
			var offeredCourseIDs = this.OfferedCourseIDs;
			if (!offeredCourseIDs.Any())
			{
				this.AddErrorAlert("No course is selected.");
				return;
			}
			var (updated, total) = BL.Core.CourseOffering.Staff.OnlineTeachingReadiness.UndoSubmitOnlineTeachingReadiness(offeredCourseIDs, true, false, false, this.StaffIdentity.LoginSessionGuid);
			if (updated == 1 || updated == 0)
				this.AddSuccessAlert($"{updated} record is unverified by HOD.");
			else
				this.AddSuccessAlert($"{updated} records are unverified by HOD.");
			this.RefreshPage();
		}

		protected void btnSubmitAsDirector_Click(object sender, EventArgs e)
		{
			var offeredCourseIDs = this.OfferedCourseIDs;
			if (!offeredCourseIDs.Any())
			{
				this.AddErrorAlert("No course is selected.");
				return;
			}
			var (updated, total) = BL.Core.CourseOffering.Staff.OnlineTeachingReadiness.SubmitOnlineTeachingReadiness(offeredCourseIDs, false, true, false, this.StaffIdentity.LoginSessionGuid);
			if (updated == 1 || updated == 0)
				this.AddSuccessAlert($"{updated} record is certified by Director/Principal.");
			else
				this.AddSuccessAlert($"{updated} records are certified by Director/Principal.");
			this.RefreshPage();
		}

		protected void btnUndoSubmitAsDirector_Click(object sender, EventArgs e)
		{
			var offeredCourseIDs = this.OfferedCourseIDs;
			if (!offeredCourseIDs.Any())
			{
				this.AddErrorAlert("No course is selected.");
				return;
			}
			var (updated, total) = BL.Core.CourseOffering.Staff.OnlineTeachingReadiness.UndoSubmitOnlineTeachingReadiness(offeredCourseIDs, false, true, false, this.StaffIdentity.LoginSessionGuid);
			if (updated == 1 || updated == 0)
				this.AddSuccessAlert($"{updated} record is uncertified by Director/Principal.");
			else
				this.AddSuccessAlert($"{updated} records are uncertified by Director/Principal.");
			this.RefreshPage();
		}

		protected void btnSubmitAsDG_Click(object sender, EventArgs e)
		{
			var offeredCourseIDs = this.OfferedCourseIDs;
			if (!offeredCourseIDs.Any())
			{
				this.AddErrorAlert("No course is selected.");
				return;
			}
			var (updated, total) = BL.Core.CourseOffering.Staff.OnlineTeachingReadiness.SubmitOnlineTeachingReadiness(offeredCourseIDs, false, false, true, this.StaffIdentity.LoginSessionGuid);
			if (updated == 1 || updated == 0)
				this.AddSuccessAlert($"{updated} record is certified by DG.");
			else
				this.AddSuccessAlert($"{updated} records are certified by DG.");
			this.RefreshPage();
		}

		protected void btnUndoSubmitAsDG_Click(object sender, EventArgs e)
		{
			var offeredCourseIDs = this.OfferedCourseIDs;
			if (!offeredCourseIDs.Any())
			{
				this.AddErrorAlert("No course is selected.");
				return;
			}
			var (updated, total) = BL.Core.CourseOffering.Staff.OnlineTeachingReadiness.UndoSubmitOnlineTeachingReadiness(offeredCourseIDs, false, false, true, this.StaffIdentity.LoginSessionGuid);
			if (updated == 1 || updated == 0)
				this.AddSuccessAlert($"{updated} record is uncertified by DG.");
			else
				this.AddSuccessAlert($"{updated} records are uncertified by DG.");
			this.RefreshPage();
		}

		protected void btnExport_Click(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
			var sectionEnum = this.ddlSection.GetSelectedEnumValue<Sections>(null);
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>(null);
			var facultyMemberID = this.ddlFacultyMemberID.SelectedValue.ToNullableInt();
			var readinessTypeEnum = this.ddlReadinessType.GetSelectedNullableGuidEnum<OnlineTeachingReadiness.ReadinessTypes>();
			var submittedByClusterHead = this.ddlSubmittedByClusterHead.SelectedValue.ToNullableBoolean();
			var submittedByHOD = this.ddlSubmittedByHOD.SelectedValue.ToNullableBoolean();
			var submittedByDirector = this.ddlSubmittedByDirector.SelectedValue.ToNullableBoolean();
			var submittedByDG = this.ddlSubmittedByDG.SelectedValue.ToNullableBoolean();
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var offeredCourses = BL.Core.CourseOffering.Staff.OnlineTeachingReadiness.GetOnlineTeachingReadinessDetails(semesterID, departmentID, programID, semesterNoEnum, sectionEnum, shiftEnum, facultyMemberID, readinessTypeEnum, submittedByClusterHead, submittedByHOD, submittedByDirector, submittedByDG, sortExpression, sortDirection, this.StaffIdentity.LoginSessionGuid)
				.Select(oc => new
				{
					oc.InstituteAlias,
					oc.DepartmentAlias,
					oc.ProgramAlias,
					oc.Class,
					oc.CourseCode,
					oc.Title,
					CreditHours = oc.CreditHours.ToCreditHoursFullName(),
					ContactHours = oc.ContactHours.ToCreditHoursFullName(),
					oc.FacultyMemberName,
					oc.FacultyMemberDepartmentAlias,
					FacultyTypeEnum = oc.FacultyTypeEnum?.ToFullName(),
					ReadinessType = oc.OnlineTeachingReadiness.ReadinessTypeEnum.GetFullName(),
					oc.OnlineTeachingReadiness.NotReadyAlreadyDeliveredFaceToFaceContactHours,
					oc.OnlineTeachingReadiness.NotReadyRemainingContactHours,
					oc.OnlineTeachingReadiness.NotReadyTotalContactHours,
					oc.OnlineTeachingReadiness.NotReadyTheoreticalContactHours,
					oc.OnlineTeachingReadiness.NotReadyLabContactHours,
					oc.OnlineTeachingReadiness.NotReadyFieldStudyContactHours,
					oc.OnlineTeachingReadiness.NotReadyOtherContactHours,
					oc.OnlineTeachingReadiness.NotReadyReasons,
					oc.OnlineTeachingReadiness.FullyReadyAlreadyDeliveredFaceToFaceContactHours,
					oc.OnlineTeachingReadiness.FullyReadyToBeDeliveredOnlineContactHours,
					oc.OnlineTeachingReadiness.FullyReadyTotalContactHours,
					oc.OnlineTeachingReadiness.FullyReadyTheoreticalContactHours,
					oc.OnlineTeachingReadiness.FullyReadyLabContactHours,
					oc.OnlineTeachingReadiness.FullyReadyFieldStudyContactHours,
					oc.OnlineTeachingReadiness.FullyReadyOtherContactHours,
					oc.OnlineTeachingReadiness.FullyReadyQuestions,
					oc.OnlineTeachingReadiness.PartiallyReadyAlreadyDeliveredFaceToFaceContactHours,
					oc.OnlineTeachingReadiness.PartiallyReadyToBeDeliveredOnlineContactHours,
					oc.OnlineTeachingReadiness.PartiallyReadyTotalContactHours,
					oc.OnlineTeachingReadiness.PartiallyReadyCoveredOnlineTheoreticalContactHours,
					oc.OnlineTeachingReadiness.PartiallyReadyCoveredOnlineLabContactHours,
					oc.OnlineTeachingReadiness.PartiallyReadyCoveredOnlineFieldStudyContactHours,
					oc.OnlineTeachingReadiness.PartiallyReadyCoveredOnlineOtherContactHours,
					oc.OnlineTeachingReadiness.PartiallyReadyCanNotCoveredOnlineTheoreticalContactHours,
					oc.OnlineTeachingReadiness.PartiallyReadyCanNotCoveredOnlineLabContactHours,
					oc.OnlineTeachingReadiness.PartiallyReadyCanNotCoveredOnlineFieldStudyContactHours,
					oc.OnlineTeachingReadiness.PartiallyReadyCanNotCoveredOnlineOtherContactHours,
					oc.OnlineTeachingReadiness.PartiallyReadyReasons,
					oc.OnlineTeachingReadiness.PartiallyReadyRemainingContentsCoveredInSemesterID,
					oc.OnlineTeachingReadiness.PartiallyReadyQuestions,
				}); ;

			var offeredCourse = offeredCourses.FirstOrDefault();
			if (offeredCourse == null)
			{
				this.AddNoRecordFoundAlert();
				return;
			}

			var csv = offeredCourses.ConvertToCSVFormat(new[]{
				new CSVHelper.CSVColumnMapping{ ColumnIndex=0, ColumnName="Institute", PropertyName= nameof(offeredCourse.InstituteAlias) },
				new CSVHelper.CSVColumnMapping{ ColumnIndex=0, ColumnName="Department", PropertyName= nameof(offeredCourse.DepartmentAlias) },
				new CSVHelper.CSVColumnMapping{ ColumnIndex=0, ColumnName="Program", PropertyName= nameof(offeredCourse.ProgramAlias) },
				new CSVHelper.CSVColumnMapping{ ColumnIndex=0, ColumnName="Class", PropertyName= nameof(offeredCourse.Class) },
				new CSVHelper.CSVColumnMapping{ ColumnIndex=0, ColumnName="Course Code", PropertyName= nameof(offeredCourse.CourseCode) },
				new CSVHelper.CSVColumnMapping{ ColumnIndex=0, ColumnName="Course Title", PropertyName= nameof(offeredCourse.Title) },
				new CSVHelper.CSVColumnMapping{ ColumnIndex=0, ColumnName="Credit Hours", PropertyName= nameof(offeredCourse.CreditHours) },
				new CSVHelper.CSVColumnMapping{ ColumnIndex=0, ColumnName="Contact Hours", PropertyName= nameof(offeredCourse.ContactHours) },
				new CSVHelper.CSVColumnMapping{ ColumnIndex=0, ColumnName="Faculty Member", PropertyName= nameof(offeredCourse.FacultyMemberName) },
				new CSVHelper.CSVColumnMapping{ ColumnIndex=0, ColumnName="Faculty Member's Department", PropertyName= nameof(offeredCourse.FacultyMemberDepartmentAlias) },
				new CSVHelper.CSVColumnMapping{ ColumnIndex=0, ColumnName="Faculty Member Type", PropertyName= nameof(offeredCourse.FacultyTypeEnum) },
				new CSVHelper.CSVColumnMapping{ ColumnIndex=0, ColumnName="Online Teaching Readiness", PropertyName= nameof(offeredCourse.ReadinessType) },
				new CSVHelper.CSVColumnMapping{ ColumnIndex=0, ColumnName="Partially Ready: Reasons/rationale", PropertyName= nameof(offeredCourse.PartiallyReadyReasons) },
				new CSVHelper.CSVColumnMapping{ ColumnIndex=0, ColumnName="Not Ready: Reasons/rationale", PropertyName= nameof(offeredCourse.NotReadyReasons) },
			});

			this.Response.Clear();
			this.Response.Buffer = true;
			var fileName = $"Online Teaching Readiness Data - {semesterID.ToSemesterString()} - {DateTime.Now:dd-MM-yyyy hh:mm:ss}.csv";
			this.Response.AddHeader("content-disposition", $"attachment;filename=\"{fileName}\"");
			this.Response.Charset = "";
			this.Response.ContentType = "text/csv";
			this.Response.Output.Write(csv);
			this.Response.Flush();
			this.Response.End();
		}
	}
}