﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfferedCourseAssignClassesModal.ascx.cs" Inherits="Aspire.Web.Sys.Staff.CourseOffering.OfferedCourseAssignClassesModal" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Register Src="~/Sys/Staff/CourseOffering/OfferedCourseInfo.ascx" TagPrefix="uc1" TagName="OfferedCourseInfo" %>
<aspire:AspireModal runat="server" ModalSize="Large" ID="modal" HeaderText="Assign/Delete Classes" CloseButtonVisible="False">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alert" />
				<uc1:OfferedCourseInfo runat="server" ID="offeredCourseInfo" />
				<aspire:AspireGridView ItemType="Aspire.Model.Entities.OfferedCourseClass" runat="server" ID="gvClasses" AutoGenerateColumns="False" OnRowCommand="gvClasses_OnRowCommand">
					<Columns>
						<asp:BoundField HeaderText="Class" DataField="ClassName" />
						<asp:TemplateField HeaderText="Special Course">
							<ItemTemplate>
								<%#: Item.SpecialOffered.ToYesNo() %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Action">
							<ItemTemplate>
								<aspire:AspireLinkButton runat="server" Glyphicon="remove" CausesValidation="False" CommandName="Delete" EncryptedCommandArgument="<%# Item.OfferedCourseClassID %>" ConfirmMessage='<%# $"Are you sure you want to delete \"{Item.ClassName}\" class?" %>' />
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</aspire:AspireGridView>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="ddlSemesterNo" />
									<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" ValidationGroup="AddClass" />
									<aspire:AspireRequiredFieldValidator ControlToValidate="ddlSemesterNo" ValidationGroup="AddClass" runat="server" ErrorMessage="This field is required." />
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSection" />
									<aspire:AspireDropDownList runat="server" ID="ddlSection" ValidationGroup="AddClass" />
									<aspire:AspireRequiredFieldValidator ControlToValidate="ddlSection" ValidationGroup="AddClass" runat="server" ErrorMessage="This field is required." />
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="Shift:" ID="Section" AssociatedControlID="ddlShift" />
									<aspire:AspireDropDownList runat="server" ID="ddlShift" ValidationGroup="AddClass" />
									<aspire:AspireRequiredFieldValidator ControlToValidate="ddlShift" ValidationGroup="AddClass" runat="server" ErrorMessage="This field is required." />
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="Special Course:" AssociatedControlID="ddlSpecialCourse" />
									<aspire:AspireDropDownList runat="server" ID="ddlSpecialCourse" ValidationGroup="AddClass" />
									<aspire:AspireRequiredFieldValidator ControlToValidate="ddlSpecialCourse" ValidationGroup="AddClass" runat="server" ErrorMessage="This field is required." />
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="&nbsp;" AssociatedControlID="btnAddClass" />
									<div>
										<aspire:AspireButton ButtonType="Primary" runat="server" ValidationGroup="AddClass" Text="Add" ID="btnAddClass" OnClick="btnAddClass_OnClick" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<aspire:AspireButton ButtonType="Default" runat="server" CausesValidation="false" Text="Close" ID="btnClose" OnClick="btnClose_OnClick" />
	</FooterTemplate>
</aspire:AspireModal>
