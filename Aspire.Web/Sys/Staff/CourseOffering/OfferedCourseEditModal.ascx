﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfferedCourseEditModal.ascx.cs" Inherits="Aspire.Web.Sys.Staff.CourseOffering.OfferedCourseEditModal" %>
<%@ Register Src="~/Sys/Staff/CourseOffering/OfferedCourseInfo.ascx" TagPrefix="uc1" TagName="OfferedCourseInfo" %>

<aspire:AspireModal runat="server" ModalSize="Large" ID="modal" HeaderText="Edit Offered Course">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alert" />
				<uc1:OfferedCourseInfo runat="server" ID="offeredCourseInfo" />
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="ddlSemesterNo" />
							<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" ValidationGroup="EditOfferedCourse" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterNo" ValidationGroup="EditOfferedCourse" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSection" />
							<aspire:AspireDropDownList runat="server" ID="ddlSection" ValidationGroup="EditOfferedCourse" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSection" ValidationGroup="EditOfferedCourse" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShift" />
							<aspire:AspireDropDownList runat="server" ID="ddlShift" ValidationGroup="EditOfferedCourse" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlShift" ValidationGroup="EditOfferedCourse" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Special Offered:" AssociatedControlID="ddlSpecialOffered" />
							<aspire:AspireDropDownList runat="server" ID="ddlSpecialOffered" ValidationGroup="EditOfferedCourse" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSpecialOffered" ValidationGroup="EditOfferedCourse" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Visiting Course:" AssociatedControlID="ddlVisitingCourse" />
							<aspire:AspireDropDownList runat="server" ID="ddlVisitingCourse" ValidationGroup="EditOfferedCourse" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlVisitingCourse" ValidationGroup="EditOfferedCourse" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatus" />
							<aspire:AspireDropDownList runat="server" ID="ddlStatus" ValidationGroup="EditOfferedCourse" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlStatus" ValidationGroup="EditOfferedCourse" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Max. Class Strength:" AssociatedControlID="tbMaxClassStrength" />
							<aspire:AspireTextBox runat="server" ValidationGroup="EditOfferedCourse" ID="tbMaxClassStrength" />
							<aspire:AspireByteValidator AllowNull="False" ValidationGroup="EditOfferedCourse" ControlToValidate="tbMaxClassStrength" runat="server" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid number." InvalidRangeErrorMessage="Invalid class strength." />
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" />
							<aspire:AspireTextBox TextMode="MultiLine" ValidationGroup="Edit" Rows="2" runat="server" ID="tbRemarks" />
						</div>
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireButton ButtonType="Primary" runat="server" ValidationGroup="EditOfferedCourse" Text="Save" ID="btnSave" OnClick="btnSave_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>
