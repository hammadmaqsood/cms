﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="OfferedCourses.aspx.cs" Inherits="Aspire.Web.Sys.Staff.CourseOffering.OfferedCourses" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Offered Semester:" AssociatedControlID="ddlSemesterID" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" CausesValidation="False" AutoPostBack="True" />
		</div>
		<div class="col-md-3 form-group">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" OnSelectedIndexChanged="ddlDepartmentID_OnSelectedIndexChanged" CausesValidation="False" AutoPostBack="True" />
		</div>
		<div class="col-md-3 form-group">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramID" OnSelectedIndexChanged="ddlProgramID_OnSelectedIndexChanged" CausesValidation="False" AutoPostBack="True" />
		</div>
		<div class="col-md-1 form-group">
			<aspire:AspireLabel runat="server" Text="Sem. #:" AssociatedControlID="ddlSemesterNo" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" OnSelectedIndexChanged="ddlSemesterNo_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
		<div class="col-md-1 form-group">
			<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSection" />
			<aspire:AspireDropDownList runat="server" ID="ddlSection" OnSelectedIndexChanged="ddlSection_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShift" />
			<aspire:AspireDropDownList runat="server" ID="ddlShift" OnSelectedIndexChanged="ddlShift_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
	</div>
	<div class="row">
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Teacher:" AssociatedControlID="ddlFacultyMemberID" />
			<aspire:AspireDropDownList runat="server" ID="ddlFacultyMemberID" OnSelectedIndexChanged="ddlFacultyMemberID_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Online Readiness:" AssociatedControlID="ddlReadinessType" />
			<aspire:AspireDropDownList runat="server" ID="ddlReadinessType" OnSelectedIndexChanged="ddlReadinessType_SelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Submitted by Cluster Head:" AssociatedControlID="ddlSubmittedByClusterHead" />
			<aspire:AspireDropDownList runat="server" ID="ddlSubmittedByClusterHead" OnSelectedIndexChanged="ddlSubmittedByClusterHead_SelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Verified by HOD:" AssociatedControlID="ddlSubmittedByHOD" />
			<aspire:AspireDropDownList runat="server" ID="ddlSubmittedByHOD" OnSelectedIndexChanged="ddlSubmittedByHOD_SelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Certified by Director:" AssociatedControlID="ddlSubmittedByDirector" />
			<aspire:AspireDropDownList runat="server" ID="ddlSubmittedByDirector" OnSelectedIndexChanged="ddlSubmittedByDirector_SelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Certified by DG:" AssociatedControlID="ddlSubmittedByDG" />
			<aspire:AspireDropDownList runat="server" ID="ddlSubmittedByDG" OnSelectedIndexChanged="ddlSubmittedByDG_SelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
	</div>
	<aspire:AspireGridView runat="server" ID="gvCourses" ItemType="Aspire.BL.Core.CourseOffering.Staff.OnlineTeachingReadiness.OfferedCourse" AutoGenerateColumns="False" OnPageSizeChanging="gvCourses_OnPageSizeChanging" OnPageIndexChanging="gvCourses_OnPageIndexChanging" OnSorting="gvCourses_OnSorting" AllowSorting="True">
		<Columns>
			<asp:TemplateField>
				<HeaderTemplate>
					<aspire:AspireCheckBox runat="server" ID="cbSelectAll" CssClass="SelectAll" ValidationGroup="SelectAll" />
				</HeaderTemplate>
				<ItemTemplate>
					<asp:HiddenField Visible="false" Value="<%# Item.OfferedCourseID %>" runat="server" ID="hfOfferedCourseID" />
					<aspire:AspireCheckBox runat="server" Visible="<%# Item.SubmittedByClusterHead %>" ID="cbSelect" CssClass="Select" ValidationGroup="Select" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate>
					<%#: Container.DataItemIndex + 1 %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Course" SortExpression="Title">
				<ItemTemplate>
					<span class="label label-default" title="Offered Semester"><%# Item.SemesterID.ToSemesterString() %></span>
					<div>
						<%#: Item.Title %>
					</div>
					<span runat="server" visible="<%# Item.SubmittedByClusterHead %>" class="label label-default">Submitted By Cluster Head/Subject Specialist</span>
					<span runat="server" visible="<%# Item.SubmittedByHOD %>" class="label label-info">Submitted By HOD</span>
					<span runat="server" visible="<%# Item.SubmittedByDirector %>" class="label label-danger">Submitted By Director/Principal</span>
					<span runat="server" visible="<%# Item.SubmittedByDG %>" class="label label-success">Submitted By DG</span>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Class" SortExpression="Class">
				<ItemTemplate>
					<span class="label label-primary" title="Class"><%#: Item.Teacher.ToNAIfNullOrEmpty() %></span>
					<div>
						<%#: Item.Class %>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Online Readiness" SortExpression="ReadinessType">
				<ItemTemplate>
					<%#: (Item.ReadinessType?.GetFullName()).ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireHyperLink Target="_blank" runat="server" ToolTip="View Exam Marks Sheet" Text="View" NavigateUrl='<%# Aspire.Web.Sys.Staff.CourseOffering.OnlineTeachingReadinessForm.GetPageUrl(Item.OfferedCourseID) %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<div class="form-group text-center">
		<style type="text/css">
			.popover {
				max-width: 500px !important;
			}
		</style>
		<div class="btn-group">
			<button class="btn btn-default" id="btnInfoExport" type="button" data-toggle="popover" data-placement="top" data-trigger="focus" data-html="true"><span class="glyphicon glyphicon-info-sign"></span></button>
			<aspire:AspireButton runat="server" ID="btnExport" DisableOnClick="false" ConfirmMessage="Are you sure you want to continue?" ButtonType="Default" CausesValidation="false" Text="Export" OnClick="btnExport_Click" />
		</div>
		<div class="btn-group">
			<button class="btn btn-default" id="btnInfoHOD" type="button" data-toggle="popover" data-placement="top" data-trigger="focus" data-html="true"><span class="glyphicon glyphicon-info-sign"></span></button>
			<aspire:AspireButton runat="server" ID="btnSubmitAsHOD" ConfirmMessage="Are you sure you want to continue?" ButtonType="Default" CausesValidation="false" Text="Submit as HOD" OnClick="btnSubmitAsHOD_Click" />
			<aspire:AspireButton runat="server" ID="btnUndoSubmitAsHOD" ConfirmMessage="Are you sure you want to continue?" ButtonType="Danger" CausesValidation="false" Text="Undo Submit as HOD" OnClick="btnUndoSubmitAsHOD_Click" />
		</div>
		<div class="btn-group">
			<button class="btn btn-default" id="btnInfoDirector" type="button" data-toggle="popover" data-placement="top" data-trigger="focus" data-html="true"><span class="glyphicon glyphicon-info-sign"></span></button>
			<aspire:AspireButton runat="server" ID="btnSubmitAsDirector" ConfirmMessage="Are you sure you want to continue?" ButtonType="Danger" CausesValidation="false" Text="Submit as Director/Principal" OnClick="btnSubmitAsDirector_Click" />
			<aspire:AspireButton runat="server" ID="btnUndoSubmitAsDirector" ConfirmMessage="Are you sure you want to continue?" ButtonType="Danger" CausesValidation="false" Text="Undo Submit as Director/Principal" OnClick="btnUndoSubmitAsDirector_Click" />
		</div>
		<div class="btn-group">
			<button class="btn btn-default" id="btnInfoDG" type="button" data-toggle="popover" data-placement="top" data-trigger="focus" data-html="true"><span class="glyphicon glyphicon-info-sign"></span></button>
			<aspire:AspireButton runat="server" ID="btnSubmitAsDG" ConfirmMessage="Are you sure you want to continue?" ButtonType="Success" CausesValidation="false" Text="Submit as DG" OnClick="btnSubmitAsDG_Click" />
			<aspire:AspireButton runat="server" ID="btnUndoSubmitAsDG" ConfirmMessage="Are you sure you want to continue?" ButtonType="Danger" CausesValidation="false" Text="Undo Submit as DG" OnClick="btnUndoSubmitAsDG_Click" />
		</div>
	</div>
	<script type="text/javascript">
		$(function () {
			$("#<%=this.ddlFacultyMemberID.ClientID%>").applySelect2();
			applySelectAllToCheckBoxes($(".SelectAll input"), $(".Select input"));
			function getHtml(html, bulletsHtml) {
				var body = $("<div></div>").css({ "width": "100%" });
				$("<span></span>").html(html).appendTo(body);
				var ul = $("<ul></ul>").appendTo(body).css({ "padding": "0 15px" });
				for (var i = 0; i < bulletsHtml.length; i++)
					$("<li></li>").html(bulletsHtml[i]).appendTo(ul);
				return $("<div></div>").append(body).html();
			}
			$("#btnInfoExport").popover({
				sanitize: false,
				container: 'body',
				content: getHtml("<strong>Export</strong> option will be enabled by filtering above record as. (Individual course selecttion is not required.)", [
					"<strong>Submitted by Cluster Head</strong> => <strong>Yes</strong>",
					"Or <strong>Verified by HOD</strong> => <strong>Yes</strong>",
					"Or <strong>Certified by Director</strong> => <strong>Yes</strong>",
					"Or <strong>Certified by DG</strong> => <strong>Yes</strong>"
				])
			});
			$("#btnInfoHOD").popover({
				sanitize: false,
				container: 'body',
				content: getHtml("<strong>Submit as HOD</strong> option will be enabled by filtering above record as. (Individual course selecttion is required.)", [
					"Select your respective <strong>Department</strong>.",
					"<strong>Submitted by Cluster Head</strong> => <strong>Yes</strong>",
					"<strong>Verified by HOD</strong> => <strong>No</strong>"
				])
			});
			$("#btnInfoDirector").popover({
				sanitize: false,
				container: 'body',
				content: getHtml("<strong>Submit as Director/Principal</strong> option will be enabled by filtering above record as. (Individual course selecttion is required.)", [
					"<strong>Verified by HOD</strong> => <strong>Yes</strong>",
					"<strong>Certified by Director</strong> => <strong>No</strong>"
				])
			});
			$("#btnInfoDG").popover({
				sanitize: false,
				container: 'body',
				content: getHtml("<strong>Submit as DG</strong> option will be enabled by filtering above record as. (Individual course selecttion is required.)", [
					"<strong>Certified by Director</strong> => <strong>Yes</strong>",
					"<strong>Certified by DG</strong> => <strong>No</strong>"
				])
			});
		});
	</script>
</asp:Content>
