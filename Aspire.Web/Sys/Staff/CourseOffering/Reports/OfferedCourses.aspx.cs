﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.CourseOffering.Reports
{
	[AspirePage("3B0F3E16-6AAD-4783-A74E-3D860716E11C", UserTypes.Staff, "~/Sys/Staff/CourseOffering/Reports/OfferedCourses.aspx", "Offered Courses", true, AspireModules.CourseOffering)]
	public partial class OfferedCourses : StaffPage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Offered Courses";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.CourseOffering.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};

		public string ReportName => "Offered Courses";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		public static string GetPageUrl(short? semesterID, int? departmentID)
		{
			return GetPageUrl<OfferedCourses>().AttachQueryParams("SemesterID", semesterID, "DepartmentID", departmentID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
				if (this.ddlSemesterIDFilter.Items.Count == 0)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<OfferedCourses>();
					return;
				}
				this.ddlVisiting.FillYesNo(CommonListItems.All);
				this.ddlSpecialOffered.FillYesNo(CommonListItems.All);
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All).SetSelectedValueIfNotPostback("DepartmentID");
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var visiting = this.ddlVisiting.SelectedValue.ToNullableBoolean();
			var specialOffered = this.ddlSpecialOffered.SelectedValue.ToNullableBoolean();
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/CourseOffering/Staff/OfferedCourses.rdlc".MapPath();
			var reportDataSet = BL.Core.CourseOffering.Staff.Reports.OfferedCourses.GetOfferedCourses(semesterID, departmentID, visiting, specialOffered, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.OfferedCourses), reportDataSet.OfferedCourses));
			reportViewer.LocalReport.EnableExternalImages = true;
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlVisiting_OnSelectedIndexChanged(null, null);
		}

		protected void ddlVisiting_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSpecialOffered_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSpecialOffered_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView.ReportViewer);
		}
	}
}