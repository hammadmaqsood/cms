﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="OfferedCourses.aspx.cs" Inherits="Aspire.Web.Sys.Staff.CourseOffering.Reports.OfferedCourses" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" AutoPostBack="True" CausesValidation="False" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" AutoPostBack="True" CausesValidation="False" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Visiting:" AssociatedControlID="ddlVisiting" />
			<aspire:AspireDropDownList runat="server" ID="ddlVisiting" OnSelectedIndexChanged="ddlVisiting_OnSelectedIndexChanged" AutoPostBack="True" CausesValidation="False" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Special Offered:" AssociatedControlID="ddlSpecialOffered" />
			<aspire:AspireDropDownList runat="server" ID="ddlSpecialOffered" OnSelectedIndexChanged="ddlSpecialOffered_OnSelectedIndexChanged" AutoPostBack="True" CausesValidation="False" />
		</div>
	</div>
</asp:Content>
