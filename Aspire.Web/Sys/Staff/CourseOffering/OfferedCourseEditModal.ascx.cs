﻿using Aspire.BL.Core.CourseOffering.Staff;
using Aspire.Lib.Extensions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Staff.CourseOffering
{
	public partial class OfferedCourseEditModal : System.Web.UI.UserControl
	{
		private new OfferCourses Page => (OfferCourses)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
				this.Visible = false;
		}

		public void ShowModal(int offeredCourseID)
		{
			this.Page.StaffIdentity.Demand(StaffPermissions.CourseOffering.ManageOfferedCourses, Model.Entities.UserGroupPermission.PermissionValues.Allowed);
			var offeredCourse = BL.Core.CourseOffering.Staff.OfferedCourses.GetOfferedCourse(offeredCourseID, this.Page.StaffIdentity.LoginSessionGuid);
			if (offeredCourse == null)
			{
				this.Page.AddNoRecordFoundAlert();
				this.Page.Refresh();
				return;
			}
			this.offeredCourseInfo.DataBind(offeredCourse);
			this.ddlSemesterNo.FillSemesterNos(CommonListItems.Select).SetEnumValue(offeredCourse.SemesterNoEnum);
			this.ddlSection.FillSections(CommonListItems.Select).SetEnumValue(offeredCourse.SectionEnum);
			this.ddlShift.FillShifts(CommonListItems.Select).SetEnumValue(offeredCourse.ShiftEnum);
			this.ddlSpecialOffered.FillYesNo(CommonListItems.Select).SetBooleanValue(offeredCourse.SpecialOffered);
			this.ddlVisitingCourse.FillYesNo(CommonListItems.Select).SetBooleanValue(offeredCourse.VisitingCourse);
			this.tbMaxClassStrength.Text = offeredCourse.MaxClassStrength.ToString();
			this.ddlStatus.FillOfferedCourseStatuses(CommonListItems.Select).SetEnumValue(offeredCourse.StatusEnum);
			this.tbRemarks.Text = offeredCourse.Remarks;
			this.Visible = true;
			this.modal.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.Page.IsValid || this.offeredCourseInfo.OfferedCourseID == null)
				throw new InvalidOperationException();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>();
			var sectionEnum = this.ddlSection.GetSelectedEnumValue<Sections>();
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>();
			var specialOffered = this.ddlSpecialOffered.SelectedValue.ToBoolean();
			var visiting = this.ddlVisitingCourse.SelectedValue.ToBoolean();
			var statusEnum = this.ddlStatus.GetSelectedEnumValue<OfferedCours.Statuses>();
			var maxClassStrength = this.tbMaxClassStrength.Text.ToByte();
			var remarks = this.tbRemarks.Text;
			var updateStatus = BL.Core.CourseOffering.Staff.OfferedCourses.UpdateOfferedCourse(this.offeredCourseInfo.OfferedCourseID.Value, semesterNoEnum, sectionEnum, shiftEnum, specialOffered, visiting, statusEnum, maxClassStrength, remarks, this.Page.StaffIdentity.LoginSessionGuid);
			switch (updateStatus)
			{
				case BL.Core.CourseOffering.Staff.OfferedCourses.UpdateOfferedCourseStatuses.NoRecordFound:
					this.Page.AddNoRecordFoundAlert();
					break;
				case BL.Core.CourseOffering.Staff.OfferedCourses.UpdateOfferedCourseStatuses.Success:
					this.Page.AddSuccessMessageHasBeenUpdated("Offered course");
					break;
				case BL.Core.CourseOffering.Staff.OfferedCourses.UpdateOfferedCourseStatuses.MarksEntryLocked:
					this.Page.AddErrorAlert("Record can not be updated because record is locked.");
					break;
				case BL.Core.CourseOffering.Staff.OfferedCourses.UpdateOfferedCourseStatuses.MaxClassStrengthCannotBeLessThanRegisteredStudents:
					this.Page.AddErrorAlert("Max Class Strength cannot be less than already registered students.");
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(updateStatus), updateStatus, null);
			}
			this.Page.Refresh();
		}
	}
}