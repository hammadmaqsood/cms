﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.CourseOffering
{
	public partial class OfferedCourseAssignClassesModal : System.Web.UI.UserControl
	{
		private new OfferCourses Page => (OfferCourses)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
				this.Visible = false;
		}

		public void ShowModal(int offeredCourseID)
		{
			this.Page.StaffIdentity.Demand(StaffPermissions.CourseOffering.ManageOfferedCourses, Model.Entities.UserGroupPermission.PermissionValues.Allowed);
			var offeredCourse = BL.Core.CourseOffering.Staff.OfferedCourses.GetOfferedCourse(offeredCourseID, this.Page.StaffIdentity.LoginSessionGuid);
			if (offeredCourse == null)
			{
				this.Page.AddNoRecordFoundAlert();
				this.Page.Refresh();
				return;
			}
			this.offeredCourseInfo.DataBind(offeredCourse);
			this.GetClasses();
			this.ddlSemesterNo.FillSemesterNos(CommonListItems.Select);
			this.ddlSection.FillSections(CommonListItems.Select);
			this.ddlShift.FillShifts(CommonListItems.Select);
			this.ddlSpecialCourse.FillYesNo(CommonListItems.Select);
			this.Visible = true;
			this.modal.Show();
		}

		protected void gvClasses_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					var offeredCourseClassID = e.DecryptedCommandArgumentToInt();
					var status = BL.Core.CourseOffering.Staff.OfferedCourses.DeleteOfferedCourseClass(offeredCourseClassID, this.Page.StaffIdentity.LoginSessionGuid);
					switch (status)
					{
						case BL.Core.CourseOffering.Staff.OfferedCourses.DeleteOfferedCourseClassStatuses.NoRecordFound:
							this.Page.AddNoRecordFoundAlert();
							this.Page.Refresh();
							return;
						case BL.Core.CourseOffering.Staff.OfferedCourses.DeleteOfferedCourseClassStatuses.MarksEntryLocked:
							this.alert.AddErrorAlert("Class cannot be deleted because record is locked.");
							this.GetClasses();
							this.updatePanel.Update();
							return;
						case BL.Core.CourseOffering.Staff.OfferedCourses.DeleteOfferedCourseClassStatuses.Success:
							this.alert.AddSuccessMessageHasBeenDeleted("Class");
							this.GetClasses();
							this.updatePanel.Update();
							return;
						default:
							throw new ArgumentOutOfRangeException();
					}
			}
		}

		private void GetClasses()
		{
			if (this.offeredCourseInfo.OfferedCourseID == null)
				throw new InvalidOperationException();
			var classes = BL.Core.CourseOffering.Staff.OfferedCourses.GetOfferedCourseClasses(this.offeredCourseInfo.OfferedCourseID.Value, this.Page.StaffIdentity.LoginSessionGuid);
			this.gvClasses.DataBind(classes);
			this.updatePanel.Update();
		}

		protected void btnAddClass_OnClick(object sender, EventArgs e)
		{
			if (!this.Page.IsValid || this.offeredCourseInfo.OfferedCourseID == null)
				throw new InvalidOperationException();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>();
			var sectionEnum = this.ddlSection.GetSelectedEnumValue<Sections>();
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>();
			var special = this.ddlSpecialCourse.SelectedValue.ToBoolean();
			var result = BL.Core.CourseOffering.Staff.OfferedCourses.AddOfferedCourseClass(this.offeredCourseInfo.OfferedCourseID.Value, semesterNoEnum, sectionEnum, shiftEnum, special, this.Page.StaffIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.CourseOffering.Staff.OfferedCourses.AddOfferedCourseClassStatuses.NoRecordFound:
					this.Page.AddNoRecordFoundAlert();
					this.Page.Refresh();
					return;
				case BL.Core.CourseOffering.Staff.OfferedCourses.AddOfferedCourseClassStatuses.AlreadyPrimaryClass:
					this.alert.AddErrorAlert("Selected Class is the primary class of the selected Offered Course.");
					this.GetClasses();
					return;
				case BL.Core.CourseOffering.Staff.OfferedCourses.AddOfferedCourseClassStatuses.ClassAlreadyExists:
					this.alert.AddWarningAlert("Selected Class is already added to the Offered Course.");
					this.GetClasses();
					return;
				case BL.Core.CourseOffering.Staff.OfferedCourses.AddOfferedCourseClassStatuses.CourseAlreadyOfferedToClass:
					this.alert.AddWarningAlert("Course has already been offered to the selected class.");
					this.GetClasses();
					return;
				case BL.Core.CourseOffering.Staff.OfferedCourses.AddOfferedCourseClassStatuses.Success:
					this.alert.AddSuccessMessageHasBeenAdded("Class");
					this.GetClasses();
					return;
				default:
					throw new ArgumentOutOfRangeException(nameof(result), result, null);
			}
		}

		protected void btnClose_OnClick(object sender, EventArgs e)
		{
			this.Page.Refresh();
		}
	}
}