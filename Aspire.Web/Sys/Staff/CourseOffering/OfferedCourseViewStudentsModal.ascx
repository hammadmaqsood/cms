﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfferedCourseViewStudentsModal.ascx.cs" Inherits="Aspire.Web.Sys.Staff.CourseOffering.OfferedCourseViewStudentsModal" %>
<%@ Register Src="~/Sys/Staff/CourseOffering/OfferedCourseInfo.ascx" TagPrefix="uc1" TagName="OfferedCourseInfo" %>

<aspire:AspireModal runat="server" ModalSize="Large" ID="modal" HeaderText="View Student Registrations/Requests">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alert" />
				<uc1:OfferedCourseInfo runat="server" ID="offeredCourseInfo" />
				<aspire:AspireGridView runat="server" ItemType="Aspire.BL.Core.CourseOffering.Staff.OfferedCourses.Student" Caption="Registered Students" ID="gvRegisteredStudents" AutoGenerateColumns="False">
					<Columns>
						<asp:TemplateField HeaderText="#">
							<ItemTemplate>
								<%#: Container.DataItemIndex + 1 %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField HeaderText="Enrollment" DataField="Enrollment" />
						<asp:BoundField HeaderText="Name" DataField="Name" />
						<asp:BoundField HeaderText="Program" DataField="ProgramAlias" />
						<asp:BoundField HeaderText="University Email" DataField="UniversityEmail" />
						<asp:BoundField HeaderText="Personal Email" DataField="PersonalEmail" />
					</Columns>
				</aspire:AspireGridView>
				<aspire:AspireGridView runat="server" ItemType="Aspire.BL.Core.CourseOffering.Staff.OfferedCourses.Student" Caption="Requests" ID="gvRequestedStudents" AutoGenerateColumns="False">
					<Columns>
						<asp:TemplateField HeaderText="#">
							<ItemTemplate>
								<%#: Container.DataItemIndex + 1 %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField HeaderText="Enrollment" DataField="Enrollment" />
						<asp:BoundField HeaderText="Name" DataField="Name" />
						<asp:BoundField HeaderText="Program" DataField="ProgramAlias" />
						<asp:BoundField HeaderText="University Email" DataField="UniversityEmail" />
						<asp:BoundField HeaderText="Personal Email" DataField="PersonalEmail" />
					</Columns>
				</aspire:AspireGridView>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<aspire:AspireModalCloseButton runat="server">Close</aspire:AspireModalCloseButton>
	</FooterTemplate>
</aspire:AspireModal>
