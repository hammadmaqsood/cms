﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="OnlineTeachingReadinessForm.aspx.cs" Inherits="Aspire.Web.Sys.Staff.CourseOffering.OnlineTeachingReadinessForm" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Course Code:" AssociatedControlID="tbCourseCode" />
				<aspire:AspireTextBox runat="server" MaxLength="1000" ID="tbCourseCode" ReadOnly="true" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Course Title:" AssociatedControlID="tbCourseTitle" />
				<aspire:AspireTextBox runat="server" MaxLength="1000" ID="tbCourseTitle" ReadOnly="true" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Class:" AssociatedControlID="tbClassName" />
				<aspire:AspireTextBox runat="server" MaxLength="1000" ID="tbClassName" ReadOnly="true" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Credit / Contact Hours:" AssociatedControlID="tbCreditContactHours" />
				<aspire:AspireTextBox runat="server" ID="tbCreditContactHours" ReadOnly="true" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Teacher Name:" AssociatedControlID="tbFacultyMemberName" />
				<aspire:AspireTextBox runat="server" MaxLength="1000" ID="tbFacultyMemberName" ReadOnly="true" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Teacher's Department:" AssociatedControlID="tbFacultyMemberDepartmentAlias" />
				<aspire:AspireTextBox runat="server" MaxLength="1000" ID="tbFacultyMemberDepartmentAlias" ReadOnly="true" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Faculty Member Type:" AssociatedControlID="tbFacultyMemberType" />
				<aspire:AspireTextBox runat="server" MaxLength="1000" ID="tbFacultyMemberType" ReadOnly="true" />
			</div>
		</div>
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Course Readiness on Online Teaching:" AssociatedControlID="btnNotReady" />
		<em class="text-muted">Please select from following options.</em>
		<div class="btn-group btn-group-justified">
			<aspire:AspireButton runat="server" ID="btnFullyReady" Text="Fully Ready" CausesValidation="false" OnClick="btnReadiness_Click" />
			<aspire:AspireButton runat="server" ID="btnPartiallyReady" Text="Partially Ready" CausesValidation="false" OnClick="btnReadiness_Click" />
			<aspire:AspireButton runat="server" ID="btnNotReady" Text="Not Ready" CausesValidation="false" OnClick="btnReadiness_Click" />
		</div>
	</div>
	<asp:MultiView runat="server" ID="multiView">
		<asp:View runat="server" ID="viewFullyReady">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="a. Contact hours already delivered through face-to-face teaching:" AssociatedControlID="ddlFullyReadyAlreadyDeliveredFaceToFaceContactHours" />
				<aspire:AspireDropDownList runat="server" ID="ddlFullyReadyAlreadyDeliveredFaceToFaceContactHours" CssClass="Hours" />
				<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlFullyReadyAlreadyDeliveredFaceToFaceContactHours" ErrorMessage="This field is required." />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="b. Contact hours are to be delivered through online teaching (w.e.f. decision taken for online teaching):" AssociatedControlID="ddlFullyReadyToBeDeliveredOnlineContactHours" />
				<aspire:AspireDropDownList runat="server" ID="ddlFullyReadyToBeDeliveredOnlineContactHours" CssClass="Hours" />
				<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlFullyReadyToBeDeliveredOnlineContactHours" ErrorMessage="This field is required." />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="c. Total Contact Hours:" AssociatedControlID="tbFullyReadyTotalContactHours" />
				<aspire:AspireTextBox runat="server" ID="tbFullyReadyTotalContactHours" />
				<aspire:AspireDoubleValidator ID="dvFullyReadyTotalContactHours" MinValue="1" MaxValue="48" runat="server" ValidationGroup="Save" AllowNull="false" ControlToValidate="tbFullyReadyTotalContactHours" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid range is {min} to {max}." InvalidDataErrorMessage="Value is not valid." />
				<span class="help-block" title="Formula for Total Contact Hours">c = a + b, c = d + e + f + g</span>
			</div>
			<aspire:AspireAlert runat="server" ID="alertFullyReadyContactHours" />
			<aspire:AspireLabel runat="server" Text="No. of contact hours (portion-wise):" AssociatedControlID="ddlFullyReadyTheoreticalContactHours" />
			<div class="well">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="d. Theoretical:" AssociatedControlID="ddlFullyReadyTheoreticalContactHours" />
							<aspire:AspireDropDownList runat="server" ID="ddlFullyReadyTheoreticalContactHours" CssClass="Hours" />
							<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlFullyReadyTheoreticalContactHours" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="e. Lab Based:" AssociatedControlID="ddlFullyReadyLabContactHours" />
							<aspire:AspireDropDownList runat="server" ID="ddlFullyReadyLabContactHours" CssClass="Hours" />
							<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlFullyReadyLabContactHours" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="f. Field Visit/Clinical:" AssociatedControlID="ddlFullyReadyFieldStudyContactHours" />
							<aspire:AspireDropDownList runat="server" ID="ddlFullyReadyFieldStudyContactHours" CssClass="Hours" />
							<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlFullyReadyFieldStudyContactHours" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="g. Any other:" AssociatedControlID="ddlFullyReadyOtherContactHours" />
							<aspire:AspireDropDownList runat="server" ID="ddlFullyReadyOtherContactHours" CssClass="Hours" />
							<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlFullyReadyOtherContactHours" ErrorMessage="This field is required." />
						</div>
					</div>
				</div>
			</div>
			<asp:Repeater runat="server" ID="repeaterFullyReadyQuestions" ItemType="Aspire.Model.Entities.OnlineTeachingReadiness.Question">
				<ItemTemplate>
					<asp:HiddenField runat="server" ID="hfQuestionNo" Visible="false" Value="<%# Item.QuestionNo %>" />
					<asp:HiddenField runat="server" ID="hfQuestionText" Visible="false" Value="<%# Item.QuestionText %>" />
					<div class="form-group Question">
						<aspire:AspireLabel runat="server" Text='<%# $"{Item.QuestionNo}. {Item.QuestionText}" %>' AssociatedControlID="rbl" />
						<div>
							<div>
								<aspire:AspireRadioButtonList runat="server" Enabled="<%# this.EditingAllowed %>" CssClass="YesNo" ID="rbl" SelectedIndex="<%# Item.Ready == true ? 0 : (Item.Ready==false?1:-1) %>" RepeatLayout="Flow" RepeatDirection="Horizontal">
									<asp:ListItem Text="Yes" Value="true" />
									<asp:ListItem Text="No" Value="false" />
								</aspire:AspireRadioButtonList>
							</div>
							<aspire:AspireRequiredFieldValidator runat="server" Enabled="<%# this.EditingAllowed %>" ControlToValidate="rbl" ValidationGroup="Save" ErrorMessage="This field is required." />
						</div>
						<div id="divReason" style="display: none">
							<aspire:AspireTextBox runat="server" ID="tb" ReadOnly="<%# !this.EditingAllowed %>" CssClass="Reason" MaxLength="500" Text="<%# Item.NotReadyReason %>" PlaceHolder="Reasons..." />
							<aspire:AspireCustomValidator runat="server" Enabled="<%# this.EditingAllowed %>" ControlToValidate="tb" ValidateEmptyText="true" ValidationGroup="Save" ClientValidationFunction="ValidateQuestion" ErrorMessage="This field is required." />
						</div>
					</div>
				</ItemTemplate>
				<SeparatorTemplate>
					<hr />
				</SeparatorTemplate>
			</asp:Repeater>
		</asp:View>
		<asp:View runat="server" ID="viewPartiallyReady">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="a. Contact hours already delivered through face-to-face teaching:" AssociatedControlID="ddlPartiallyReadyAlreadyDeliveredFaceToFaceContactHours" />
				<aspire:AspireDropDownList runat="server" ID="ddlPartiallyReadyAlreadyDeliveredFaceToFaceContactHours" CssClass="Hours" />
				<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlPartiallyReadyAlreadyDeliveredFaceToFaceContactHours" ErrorMessage="This field is required." />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="b. Contact hours are to be delivered through online teaching (w.e.f. decision taken for online teaching):" AssociatedControlID="ddlPartiallyReadyToBeDeliveredOnlineContactHours" />
				<aspire:AspireDropDownList runat="server" ID="ddlPartiallyReadyToBeDeliveredOnlineContactHours" CssClass="Hours" />
				<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlPartiallyReadyToBeDeliveredOnlineContactHours" ErrorMessage="This field is required." />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="c. Total Contact Hours:" AssociatedControlID="tbPartiallyReadyTotalContactHours" />
				<aspire:AspireTextBox runat="server" ID="tbPartiallyReadyTotalContactHours" />
				<aspire:AspireDoubleValidator ID="dvPartiallyReadyTotalContactHours" MinValue="1" MaxValue="48" runat="server" ValidationGroup="Save" AllowNull="false" ControlToValidate="tbPartiallyReadyTotalContactHours" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid range is {min} to {max}." InvalidDataErrorMessage="Value is not valid." />
				<span class="help-block" title="Formula for Total Contact Hours">c = a + b, c = a + d + e + f + g + h + i + j + k</span>
			</div>
			<aspire:AspireAlert runat="server" ID="alertPartiallyReadyContactHours" />
			<aspire:AspireLabel runat="server" Text="No. of contact hours partially covered through online:" AssociatedControlID="ddlPartiallyReadyCoveredOnlineTheoreticalContactHours" />
			<div class="well">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="d. Theoretical:" AssociatedControlID="ddlPartiallyReadyCoveredOnlineTheoreticalContactHours" />
							<aspire:AspireDropDownList runat="server" ID="ddlPartiallyReadyCoveredOnlineTheoreticalContactHours" CssClass="Hours" />
							<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlPartiallyReadyCoveredOnlineTheoreticalContactHours" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="e. Lab Based:" AssociatedControlID="ddlPartiallyReadyCoveredOnlineLabContactHours" />
							<aspire:AspireDropDownList runat="server" ID="ddlPartiallyReadyCoveredOnlineLabContactHours" CssClass="Hours" />
							<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlPartiallyReadyCoveredOnlineLabContactHours" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="f. Field Visit/Clinical:" AssociatedControlID="ddlPartiallyReadyCoveredOnlineFieldStudyContactHours" />
							<aspire:AspireDropDownList runat="server" ID="ddlPartiallyReadyCoveredOnlineFieldStudyContactHours" CssClass="Hours" />
							<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlPartiallyReadyCoveredOnlineFieldStudyContactHours" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="g. Any other:" AssociatedControlID="ddlPartiallyReadyCoveredOnlineOtherContactHours" />
							<aspire:AspireDropDownList runat="server" ID="ddlPartiallyReadyCoveredOnlineOtherContactHours" CssClass="Hours" />
							<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlPartiallyReadyCoveredOnlineOtherContactHours" ErrorMessage="This field is required." />
						</div>
					</div>
				</div>
			</div>
			<aspire:AspireLabel runat="server" Text="No. of contact hours which cannot be covered through online:" AssociatedControlID="ddlNotReadyTheoreticalContactHours" />
			<div class="well">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="h: Theoretical:" AssociatedControlID="ddlPartiallyReadyCanNotCoveredOnlineTheoreticalContactHours" />
							<aspire:AspireDropDownList runat="server" ID="ddlPartiallyReadyCanNotCoveredOnlineTheoreticalContactHours" CssClass="Hours" />
							<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlPartiallyReadyCanNotCoveredOnlineTheoreticalContactHours" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="i: Lab Based:" AssociatedControlID="ddlPartiallyReadyCanNotCoveredOnlineLabContactHours" />
							<aspire:AspireDropDownList runat="server" ID="ddlPartiallyReadyCanNotCoveredOnlineLabContactHours" CssClass="Hours" />
							<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlPartiallyReadyCanNotCoveredOnlineLabContactHours" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="j: Field Visit/Clinical:" AssociatedControlID="ddlPartiallyReadyCanNotCoveredOnlineFieldStudyContactHours" />
							<aspire:AspireDropDownList runat="server" ID="ddlPartiallyReadyCanNotCoveredOnlineFieldStudyContactHours" CssClass="Hours" />
							<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlPartiallyReadyCanNotCoveredOnlineFieldStudyContactHours" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="k: Any other:" AssociatedControlID="ddlPartiallyReadyCanNotCoveredOnlineOtherContactHours" />
							<aspire:AspireDropDownList runat="server" ID="ddlPartiallyReadyCanNotCoveredOnlineOtherContactHours" CssClass="Hours" />
							<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlPartiallyReadyCanNotCoveredOnlineOtherContactHours" ErrorMessage="This field is required." />
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Reasons/rationale for not covering the above-mentioned portion through online:" AssociatedControlID="tbPartiallyReadyReasons" />
				<aspire:AspireTextBox runat="server" TextMode="MultiLine" Rows="4" MaxLength="0" ID="tbPartiallyReadyReasons" />
				<aspire:AspireRequiredFieldValidator runat="server" ID="rfvPartiallyReadyReasons" ValidationGroup="Save" ControlToValidate="tbPartiallyReadyReasons" ErrorMessage="This field is required." />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Plan for covering the remaining contents in:" AssociatedControlID="rblPartiallyReadyRemainingContentsCoveredInSemesterID" />
				<div>
					<aspire:AspireRadioButtonList runat="server" ID="rblPartiallyReadyRemainingContentsCoveredInSemesterID" RepeatDirection="Horizontal" RepeatLayout="Flow">
						<asp:ListItem Text="Summer 2020" Value="20202" />
						<asp:ListItem Text="Extended Spring 2020" Value="20201" />
					</aspire:AspireRadioButtonList>
				</div>
				<aspire:AspireRequiredFieldValidator runat="server" ID="rfvPartiallyReadyRemainingContentsCoveredInSemesterID" ValidationGroup="Save" ControlToValidate="rblPartiallyReadyRemainingContentsCoveredInSemesterID" ErrorMessage="This field is required." />
			</div>
			<asp:Repeater runat="server" ID="repeaterPartiallyReadyQuestions" ItemType="Aspire.Model.Entities.OnlineTeachingReadiness.Question">
				<ItemTemplate>
					<asp:HiddenField runat="server" ID="hfQuestionNo" Visible="false" Value="<%# Item.QuestionNo %>" />
					<asp:HiddenField runat="server" ID="hfQuestionText" Visible="false" Value="<%# Item.QuestionText %>" />
					<div class="form-group Question">
						<aspire:AspireLabel runat="server" Text='<%# $"{Item.QuestionNo}. {Item.QuestionText}" %>' AssociatedControlID="rbl" />
						<div>
							<div>
								<aspire:AspireRadioButtonList runat="server" Enabled="<%# this.EditingAllowed %>" CssClass="YesNo" ID="rbl" SelectedIndex="<%# Item.Ready == true ? 0 : (Item.Ready==false?1:-1) %>" RepeatLayout="Flow" RepeatDirection="Horizontal">
									<asp:ListItem Text="Yes" Value="true" />
									<asp:ListItem Text="No" Value="false" />
								</aspire:AspireRadioButtonList>
							</div>
							<aspire:AspireRequiredFieldValidator runat="server" Enabled="<%# this.EditingAllowed %>" ControlToValidate="rbl" ValidationGroup="Save" ErrorMessage="This field is required." />
						</div>
						<div id="divReason" style="display: none">
							<aspire:AspireTextBox runat="server" ID="tb" ReadOnly="<%# !this.EditingAllowed %>" CssClass="Reason" MaxLength="500" Text="<%# Item.NotReadyReason %>" PlaceHolder="Reasons..." />
							<aspire:AspireCustomValidator runat="server" Enabled="<%# this.EditingAllowed %>" ControlToValidate="tb" ValidateEmptyText="true" ValidationGroup="Save" ClientValidationFunction="ValidateQuestion" ErrorMessage="This field is required." />
						</div>
					</div>
				</ItemTemplate>
				<SeparatorTemplate>
					<hr />
				</SeparatorTemplate>
			</asp:Repeater>
		</asp:View>
		<asp:View runat="server" ID="viewNotReady">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="a. Contact hours already delivered through face-to-face teaching:" AssociatedControlID="ddlNotReadyAlreadyDeliveredFaceToFaceContactHours" />
				<aspire:AspireDropDownList runat="server" ID="ddlNotReadyAlreadyDeliveredFaceToFaceContactHours" CssClass="Hours" />
				<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlNotReadyAlreadyDeliveredFaceToFaceContactHours" ErrorMessage="This field is required." />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="b. Remaining contact hours to be delivered in Summer/Extended Spring 2020:" AssociatedControlID="ddlNotReadyRemainingContactHours" />
				<aspire:AspireDropDownList runat="server" ID="ddlNotReadyRemainingContactHours" CssClass="Hours" />
				<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlNotReadyRemainingContactHours" ErrorMessage="This field is required." />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="c. Total Contact Hours:" AssociatedControlID="tbNotReadyTotalContactHours" />
				<aspire:AspireTextBox runat="server" ID="tbNotReadyTotalContactHours" MaxLength="8" />
				<aspire:AspireDoubleValidator ID="dvNotReadyTotalContactHours" MinValue="1" MaxValue="48" runat="server" ValidationGroup="Save" AllowNull="false" ControlToValidate="tbNotReadyTotalContactHours" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid range is {min} to {max}." InvalidDataErrorMessage="Value is not valid." />
				<span class="help-block" title="Formula for Total Contact Hours">c = a + b, c = d + e + f + g</span>
			</div>
			<aspire:AspireAlert runat="server" ID="alertNotReadyContactHours" />
			<aspire:AspireLabel runat="server" Text="No. of contact hours (portion-wise):" AssociatedControlID="ddlNotReadyTheoreticalContactHours" />
			<div class="well">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="d. Theoretical:" AssociatedControlID="ddlNotReadyTheoreticalContactHours" />
							<aspire:AspireDropDownList runat="server" ID="ddlNotReadyTheoreticalContactHours" CssClass="Hours" />
							<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlNotReadyTheoreticalContactHours" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="e. Lab Based:" AssociatedControlID="ddlNotReadyLabContactHours" />
							<aspire:AspireDropDownList runat="server" ID="ddlNotReadyLabContactHours" CssClass="Hours" />
							<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlNotReadyLabContactHours" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="f. Field Visit/Clinical:" AssociatedControlID="ddlNotReadyFieldStudyContactHours" />
							<aspire:AspireDropDownList runat="server" ID="ddlNotReadyFieldStudyContactHours" CssClass="Hours" />
							<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlNotReadyFieldStudyContactHours" ErrorMessage="This field is required." />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="g. Any other:" AssociatedControlID="ddlNotReadyOtherContactHours" />
							<aspire:AspireDropDownList runat="server" ID="ddlNotReadyOtherContactHours" CssClass="Hours" />
							<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="ddlNotReadyOtherContactHours" ErrorMessage="This field is required." />
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Solid Reasons/Rationale may please be mentioned by respective HOD/Cluster Head:" AssociatedControlID="tbNotReadyReasons" />
				<aspire:AspireTextBox runat="server" TextMode="MultiLine" Rows="4" MaxLength="0" ID="tbNotReadyReasons" />
				<aspire:AspireRequiredFieldValidator runat="server" ID="rfvtbNotReadyReasons" ValidationGroup="Save" ControlToValidate="tbNotReadyReasons" ErrorMessage="This field is required." />
			</div>
		</asp:View>
	</asp:MultiView>
	<div class="row" id="divSubmittedBy" runat="server">
		<div class="col-md-3">
			<div class="form-group text-center">
				<aspire:AspireLabel runat="server" Text="Cluster Head/Subject Expert" AssociatedControlID="btnSubmitAsClusterHead" />
				<div>
					<aspire:AspireButton CssClass="btn-block" runat="server" Text="Save/Submit as Cluster Head/Subject Expert" ID="btnSubmitAsClusterHead" ValidationGroup="Save" ConfirmMessage="Are you sure you want to continue?" OnClick="btnSubmitAsClusterHead_Click" />
					<aspire:Label ID="lblSubmitAsClusterHead" runat="server" />
					<div>
						<aspire:Label ID="lblSubmitAsClusterHeadDate" runat="server" />
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group text-center">
				<aspire:AspireLabel runat="server" Text="Head of Department" AssociatedControlID="btnSubmitAsHOD" />
				<div>
					<aspire:AspireButton CssClass="btn-block" runat="server" Text="Save/Verify as Head of Department" ID="btnSubmitAsHOD" ValidationGroup="Save" ConfirmMessage="Are you sure you want to continue?" OnClick="btnSubmitAsHOD_Click" />
					<aspire:Label ID="lblSubmitAsHOD" runat="server" />
					<div>
						<aspire:Label ID="lblSubmitAsHODDate" runat="server" />
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group text-center">
				<aspire:AspireLabel runat="server" Text="Director/Principal" AssociatedControlID="btnSubmitAsDirector" />
				<div>
					<aspire:AspireButton CssClass="btn-block" runat="server" Text="Save/Certify as Director/Principal" ID="btnSubmitAsDirector" ValidationGroup="Save" ConfirmMessage="Are you sure you want to continue?" OnClick="btnSubmitAsDirector_Click" />
					<aspire:Label ID="lblSubmitAsDirector" runat="server" />
					<div>
						<aspire:Label ID="lblSubmitAsDirectorDate" runat="server" />
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group text-center">
				<aspire:AspireLabel runat="server" Text="DG" AssociatedControlID="btnSubmitAsDG" />
				<div>
					<aspire:AspireButton CssClass="btn-block" runat="server" Text="Save/Certify as DG" ID="btnSubmitAsDG" ValidationGroup="Save" ConfirmMessage="Are you sure you want to continue?" OnClick="btnSubmitAsDG_Click" />
					<aspire:Label ID="lblSubmitAsDG" runat="server" />
					<div>
						<aspire:Label ID="lblSubmitAsDGDate" runat="server" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group text-center">
		<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ValidationGroup="Save" OnClick="btnSave_Click" />
		<aspire:AspireHyperLink runat="server" ID="btnCancel" NavigateUrl="OfferedCourses.aspx" Text="Cancel" />
	</div>
	<script type="text/javascript">
		$(function () {
			function getTotal(dropDownLists) {
				var total = 0;
				for (var i = 0; i < dropDownLists.length; i++) {
					var hours = $(dropDownLists[i]).val().toNullableDouble();
					if (hours != null)
						total += hours;
				}
				return total;
			}
			function applyOnChange(dropDownLists, totalTextBox) {
				dropDownLists.on("blur change paste", function () {
					totalTextBox.val(getTotal(dropDownLists));
				});
			}

			var dropDownLists = $("#<%=this.ddlNotReadyAlreadyDeliveredFaceToFaceContactHours.ClientID%>").add($("#<%=this.ddlNotReadyRemainingContactHours.ClientID%>"));
			var totalTextBox = $("#<%=this.tbNotReadyTotalContactHours.ClientID%>").prop("readonly", true);
			applyOnChange(dropDownLists, totalTextBox);

			dropDownLists = $("#<%=this.ddlPartiallyReadyAlreadyDeliveredFaceToFaceContactHours.ClientID%>").add($("#<%=this.ddlPartiallyReadyToBeDeliveredOnlineContactHours.ClientID%>"));
			totalTextBox = $("#<%=this.tbPartiallyReadyTotalContactHours.ClientID%>").prop("readonly", true);
			applyOnChange(dropDownLists, totalTextBox);

			dropDownLists = $("#<%=this.ddlFullyReadyAlreadyDeliveredFaceToFaceContactHours.ClientID%>").add($("#<%=this.ddlFullyReadyToBeDeliveredOnlineContactHours.ClientID%>"));
			totalTextBox = $("#<%=this.tbFullyReadyTotalContactHours.ClientID%>").prop("readonly", true);
			applyOnChange(dropDownLists, totalTextBox);

			var questionDivs = $("div.form-group.Question");
			var enableFocus = false;
			questionDivs.each(function (i, questionDiv) {
				questionDiv = $(questionDiv);
				var divReason = $("#divReason", questionDiv);
				var tbReason = $("input.Reason", divReason);
				var rbl = $(".YesNo.AspireRadioButtonList", questionDiv);
				$("input:radio", rbl).click(function () {
					if ($(this).val() == "true" && $(this).is(":checked"))
						divReason.hide();
					else {
						divReason.show();
						if (enableFocus)
							tbReason.focus();
					}
				});
				var rblYes = $("input:radio", rbl)[0];
				var rblNo = $("input:radio", rbl)[1];

				if ($(rblYes).is(":checked"))
					$(rblYes).click();
				else if ($(rblNo).is(":checked"))
					$(rblNo).click();
				else
					divReason.hide();
			});
			enableFocus = true;
		});
		function ValidateQuestion(source, args) {
			var questionDiv = $(source).closest("div.form-group.Question");
			var divReason = $("#divReason", questionDiv);
			var rbl = $(".YesNo.AspireRadioButtonList", questionDiv);
			var tbReason = $("input.Reason", divReason);
			var rbYes = $("input:radio", rbl)[0];
			//var rbNo = $("input:radio", rbl)[1];
			args.IsValid = $(rbYes).is(":checked") || tbReason.val().trim().length > 0;
			source.errormessage = args.IsValid ? "This field is required." : null;
			if (args.IsValid)
				divReason.removeClass("has-error");
			else
				divReason.addClass("has-error");
		}
	</script>
</asp:Content>
