﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfferedCourseAssignFacultyMemberModal.ascx.cs" Inherits="Aspire.Web.Sys.Staff.CourseOffering.OfferedCourseAssignFacultyMemberModal" %>
<%@ Register Src="~/Sys/Staff/CourseOffering/OfferedCourseInfo.ascx" TagPrefix="uc1" TagName="OfferedCourseInfo" %>

<aspire:AspireModal runat="server" ModalSize="Large" ID="modal" HeaderText="Assign/Change Faculty Member">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alert" />
				<uc1:OfferedCourseInfo runat="server" ID="offeredCourseInfo" />
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Faculty Member:" AssociatedControlID="ddlFacultyMemberID" />
					<aspire:AspireDropDownList runat="server" ID="ddlFacultyMemberID" ValidationGroup="FacultyMember" />
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireButton ButtonType="Primary" runat="server" ValidationGroup="FacultyMember" CausesValidation="false" Text="Save" ID="btnSave" OnClick="btnSave_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>
<script type="text/javascript">
	$(function () {
		function init() {
			$("#<%=this.ddlFacultyMemberID.ClientID%>").applySelect2();
		}
		init();
		Sys.Application.add_load(init);
	})
</script>
