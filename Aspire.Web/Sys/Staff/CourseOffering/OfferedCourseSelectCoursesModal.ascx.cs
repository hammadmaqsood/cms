﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.CourseOffering
{
	public partial class OfferedCourseSelectCoursesModal : System.Web.UI.UserControl
	{
		private new OfferCourses Page => (OfferCourses)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Title", SortDirection.Ascending);
				this.Visible = false;
			}
		}

		public void ShowModal(short offeredSemesterID, int programID, SemesterNos semesterNoEnum, Sections sectionFlags, Shifts shiftFlags)
		{
			this.Page.StaffIdentity.Demand(StaffPermissions.CourseOffering.ManageOfferedCourses, Model.Entities.UserGroupPermission.PermissionValues.Allowed);
		
			var (admissionOpenPrograms, programAlias, selectedAdmissionOpenProgramID) = BL.Core.CourseOffering.Staff.OfferedCourses.GetAdmissionOpenPrograms(programID, offeredSemesterID, semesterNoEnum, this.Page.StaffIdentity.LoginSessionGuid);

			this.ddlAdmissionOpenProgramID.DataBind(admissionOpenPrograms).SetSelectedValueIfExists(selectedAdmissionOpenProgramID.ToString());
			this.ddlSemesterNo.FillSemesterNos(CommonListItems.All).SetEnumValue(semesterNoEnum);
			this.ddlAdmissionOpenProgramID_OnSelectedIndexChanged(null, null);

			this.lblOfferingSemester.Text = offeredSemesterID.ToSemesterString();
			this.lblProgramAlias.Text = programAlias;
			this.lblSemesterNo.Text = semesterNoEnum.ToFullName();
			this.lblSections.Text = sectionFlags.ToFullNames();
			this.lblShifts.Text = shiftFlags.ToFullNames();

			this.Visible = true;
			this.ViewState["OfferedSemesterID"] = offeredSemesterID;
			this.ViewState["SemesterNoEnum"] = semesterNoEnum;
			this.ViewState["SectionFlags"] = sectionFlags;
			this.ViewState["ShiftFlags"] = shiftFlags;
			this.modal.Show();
		}
		protected void ddlAdmissionOpenProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetCoursesForOffering();
		}

		protected void gvCourses_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetCoursesForOffering();
		}

		private void GetCoursesForOffering()
		{
			var admissionOpenProgramID = this.ddlAdmissionOpenProgramID.SelectedValue.ToNullableInt();
			if (admissionOpenProgramID != null)
			{
				var courses = BL.Core.CourseOffering.Staff.OfferedCourses.GetRoadmapCoursesForOffering(admissionOpenProgramID.Value, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.Page.StaffIdentity.LoginSessionGuid);
				this.gvCourses.DataBind(courses, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
			}
			else
				this.gvCourses.DataBindNull();
		}

		protected void btnOffer_OnClick(object sender, EventArgs e)
		{
			var offeringSemesterID = (short)this.ViewState["OfferedSemesterID"];
			var semesterNoEnum = (SemesterNos)this.ViewState["SemesterNoEnum"];
			var sectionFlags = (Sections)this.ViewState["SectionFlags"];
			var shiftFlags = (Shifts)this.ViewState["ShiftFlags"];
			var courseIDs = new List<int>();

			foreach (GridViewRow row in this.gvCourses.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow)
					continue;
				var cb = (CheckBox)row.FindControl("cb");
				if (!cb.Checked || !cb.Visible)
					continue;
				var hf = (Lib.WebControls.HiddenField)row.FindControl("hfCourseID");
				courseIDs.Add(hf.Value.ToInt());
			}

			if (!courseIDs.Any())
			{
				this.alert.AddWarningAlert("No course selected.");
				this.updatePanel.Update();
				return;
			}

			var result = BL.Core.CourseOffering.Staff.OfferedCourses.OfferCourses(courseIDs, offeringSemesterID, semesterNoEnum, sectionFlags, shiftFlags, false, false, 50, OfferedCours.Statuses.Restricted, null, this.Page.StaffIdentity.LoginSessionGuid);
			if (result == null)
			{
				this.Page.AddNoRecordFoundAlert();
				this.Page.Refresh();
				return;
			}
			foreach (var offerCourseStatus in result)
			{
				if (offerCourseStatus.Added > 0)
					this.Page.AddSuccessAlert($"'{offerCourseStatus.Title}' has been offered to {offerCourseStatus.Added} class{(offerCourseStatus.Added == 1 ? "" : "es")}.");
				if (offerCourseStatus.AlreadyExists > 0)
					this.Page.AddSuccessAlert($"'{offerCourseStatus.Title}' has already been offered to {offerCourseStatus.AlreadyExists} class{(offerCourseStatus.AlreadyExists == 1 ? "" : "es")}.");
			}
			this.Page.Refresh();
		}
	}
}