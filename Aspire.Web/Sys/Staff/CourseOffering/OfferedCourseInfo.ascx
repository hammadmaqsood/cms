﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfferedCourseInfo.ascx.cs" Inherits="Aspire.Web.Sys.Staff.CourseOffering.OfferedCourseInfo" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<asp:Repeater runat="server" ID="repeaterOfferedCourse" ItemType="Aspire.BL.Core.CourseOffering.Staff.OfferedCourses.OfferedCourse">
	<ItemTemplate>
		<table class="table table-condensed table-bordered tableCol4">
			<tbody>
				<tr>
					<th>Offered Semester</th>
					<td><%#: Item.SemesterID.ToSemesterString() %></td>
					<th>Class</th>
					<td>
						<asp:Label Text="<%#: AspireFormats.GetClassName(Item.Program, Item.SemesterNoEnum, Item.SectionEnum, Item.ShiftEnum) %>" ID="lblClass" runat="server" /></td>
				</tr>
				<tr>
					<th>Title</th>
					<td>
						<asp:Label Text="<%#: Item.Title %>" ID="lblTitle" runat="server" /></td>
					<th>Credit Hours</th>
					<td><%#: Item.CreditHours.ToCreditHoursFullName()%></td>
				</tr>
				<tr>
					<th>Faculty Member</th>
					<td><%#: Item.FacultyMemberName.ToNAIfNullOrEmpty() %></td>
					<th>Majors</th>
					<td><%#: Item.Major.ToNAIfNullOrEmpty() %></td>
				</tr>
				<tr>
					<th>Maximum Strength</th>
					<td><%#: Item.MaxClassStrength %></td>
					<th>Registered Strength</th>
					<td><%#: Item.RegisteredStrength %></td>
				</tr>
			</tbody>
		</table>
	</ItemTemplate>
</asp:Repeater>
