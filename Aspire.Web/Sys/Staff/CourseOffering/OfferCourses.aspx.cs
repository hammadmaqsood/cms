﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.CourseOffering
{
	[AspirePage("CAA1BFB8-9001-4F3D-87AC-2D1D543EBF8B", UserTypes.Staff, "~/Sys/Staff/CourseOffering/OfferCourses.aspx", "Offer Course(s)", true, AspireModules.CourseOffering)]
	public partial class OfferCourses : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.CourseOffering.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Offer Course(s)";

		public static string GetPageUrl(short? semesterID, int? departmentID, int? programID, SemesterNos? semesterNo, Sections? sectionsFlag, Shifts? shiftsFlag, string sortExpression, SortDirection sortDirection)
		{
			return typeof(OfferCourses).GetAspirePageAttribute().PageUrl.AttachQueryParams(
				"SemesterID", semesterID,
				"DepartmentID", departmentID,
				"ProgramID", programID,
				"SemesterNo", semesterNo,
				"Sections", sectionsFlag,
				"Shifts", shiftsFlag,
				"SortExpression", sortExpression,
				"SortDirection", sortDirection);
		}

		public static void Redirect(short? semesterID, int? departmentID, int? programID, SemesterNos? semesterNo, Sections? sectionsFlag, Shifts? shiftsFlag, string sortExpression, SortDirection sortDirection)
		{
			Redirect(GetPageUrl(semesterID, departmentID, programID, semesterNo, sectionsFlag, shiftsFlag, sortExpression, sortDirection));
		}

		public void Refresh()
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var semesterNo = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
			var sectionFlags = this.cblSections.GetSelectedEnumValues<Sections>().Combine();
			var shiftFlags = this.cblShifts.GetSelectedEnumValues<Shifts>().Combine();
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			Redirect(semesterID, departmentID, programID, semesterNo, sectionFlags, shiftFlags, sortExpression, sortDirection);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.Page.IsPostBack)
			{
				this.ViewState.SetSortProperties("Title", SortDirection.Ascending);
				this.ddlSemesterID.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
				if (this.ddlSemesterID.Items.Count == 0)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<Dashboard>();
					return;
				}

				this.ddlDepartmentID.FillDepartments(this.StaffIdentity.InstituteID).SetSelectedValueIfNotPostback("DepartmentID");
				this.ddlSemesterNo.FillSemesterNos().SetEnumValue(SemesterNos.One);
				var semesterNo = this.GetParameterValue<SemesterNos>("SemesterNo");
				if (semesterNo != null)
					this.ddlSemesterNo.SetEnumValue(semesterNo.Value);
				this.cblSections.FillSections().SetEnumValue(Sections.A);
				this.cblShifts.FillShifts().SetEnumValue(Shifts.Morning);
				var sectionFlags = this.GetParameterValue<Sections>("Sections");
				if (sectionFlags != null)
					this.cblSections.Items.Cast<ListItem>().ForEach(item => item.Selected = sectionFlags.Value.HasFlag(item.Value.ToEnum<Sections>()));
				var shiftFlags = this.GetParameterValue<Shifts>("Shifts");
				if (shiftFlags != null)
					this.cblShifts.Items.Cast<ListItem>().ForEach(item => item.Selected = shiftFlags.Value.HasFlag(item.Value.ToEnum<Shifts>()));
				this.ddlDepartmentID_OnSelectedIndexChanged(null, null);
			}
		}

		#region Filter Events		

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.StaffIdentity.InstituteID;
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			this.hlOfferedCoursesReport.NavigateUrl = Reports.OfferedCourses.GetPageUrl(semesterID, departmentID);
			this.ddlProgramID.FillPrograms(instituteID, departmentID, false).SetSelectedValueIfNotPostback("ProgramID");
			this.ddlProgramID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterNo_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterNo_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.cblSections_OnSelectedIndexChanged(null, null);
		}

		protected void cblSections_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.cblShifts_OnSelectedIndexChanged(null, null);
		}

		protected void cblShifts_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetOfferedCourses();
		}

		#endregion

		#region gvOfferedCourses Events

		protected void gvOfferedCourses_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.offeredCourseEditModal.ShowModal(e.DecryptedCommandArgumentToInt());
					return;
				case "Delete":
					e.Handled = true;
					var offeredCourseID = e.DecryptedCommandArgumentToInt();
					var deleteStatus = BL.Core.CourseOffering.Staff.OfferedCourses.DeleteOfferedCourse(offeredCourseID, this.StaffIdentity.LoginSessionGuid);
					switch (deleteStatus)
					{
						case BL.Core.CourseOffering.Staff.OfferedCourses.DeleteOfferedCourseStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.CourseOffering.Staff.OfferedCourses.DeleteOfferedCourseStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Offered Course");
							break;
						case BL.Core.CourseOffering.Staff.OfferedCourses.DeleteOfferedCourseStatuses.CannotDeleteChildRecordExistsRegisteredCourses:
							this.AddErrorAlert("Offered Course cannot be deleted because child record exists i.e. Course Registration.");
							break;
						case BL.Core.CourseOffering.Staff.OfferedCourses.DeleteOfferedCourseStatuses.CannotDeleteChildRecordExistsExamDateSheets:
							this.AddErrorAlert("Offered Course cannot be deleted because child record exists i.e. Exam Date Sheet.");
							break;
						case BL.Core.CourseOffering.Staff.OfferedCourses.DeleteOfferedCourseStatuses.CannotDeleteChildRecordExistsAttendance:
							this.AddErrorAlert("Offered Course cannot be deleted because child record exists i.e. Class Attendance.");
							break;
						case BL.Core.CourseOffering.Staff.OfferedCourses.DeleteOfferedCourseStatuses.CannotDeleteChildRecordExistsOfferedCourseExams:
							this.AddErrorAlert("Offered Course cannot be deleted because child record exists i.e. Exam Marks.");
							break;
						case BL.Core.CourseOffering.Staff.OfferedCourses.DeleteOfferedCourseStatuses.CannotDeleteChildRecordExistsRequestedCourses:
							this.AddErrorAlert("Offered Course cannot be deleted because child record exists i.e. Course Registration Requests.");
							break;
						case BL.Core.CourseOffering.Staff.OfferedCourses.DeleteOfferedCourseStatuses.CannotDeleteChildRecordExistsExamsMarksSubmitted:
							this.AddErrorAlert("Offered Course cannot be deleted because exam marks have been submitted.");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.Refresh();
					return;
				case "AssignTeacher":
					this.offeredCourseAssignFacultyMemberModal.ShowModal(e.DecryptedCommandArgumentToInt());
					return;
				case "AssignClasses":
					this.offeredCourseAssignClassesModal.ShowModal(e.DecryptedCommandArgumentToInt());
					return;
				case "ViewStudents":
					this.offeredCourseViewStudentsModal.ShowModal(e.DecryptedCommandArgumentToInt());
					return;
				case "ImportStudents":
					this.offeredCourseImportStudentsModal.ShowModal(e.DecryptedCommandArgumentToInt());
					return;
			}
		}

		protected void gvOfferedCourses_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetOfferedCourses();
		}

		private void GetOfferedCourses()
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>();
			var sectionEnums = this.cblSections.GetSelectedEnumValues<Sections>().Combine();
			var shiftEnums = this.cblShifts.GetSelectedEnumValues<Shifts>().Combine();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();

			if (sectionEnums != null && shiftEnums != null && programID != null)
			{
				var courses = BL.Core.CourseOffering.Staff.OfferedCourses.GetOfferedCourses(semesterID, programID.Value, semesterNoEnum, sectionEnums.Value, shiftEnums.Value, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.StaffIdentity.LoginSessionGuid);
				this.gvOfferedCourses.DataBind(courses, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
				this.btnSelectCourses.Enabled = true;
			}
			else
			{
				this.gvOfferedCourses.DataBindNull();
				this.btnSelectCourses.Enabled = false;
			}
		}

		#endregion

		protected void btnSelectCourses_OnClick(object sender, EventArgs e)
		{
			var offeredSemesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var programID = this.ddlProgramID.SelectedValue.ToInt();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>();
			var sectionFlags = this.cblSections.GetSelectedEnumValues<Sections>().Combine() ?? throw new InvalidOperationException();
			var shiftFlags = this.cblShifts.GetSelectedEnumValues<Shifts>().Combine() ?? throw new InvalidOperationException();
			this.offeredCourseSelectCoursesModal.ShowModal(offeredSemesterID, programID, semesterNoEnum, sectionFlags, shiftFlags);
		}
	}
}