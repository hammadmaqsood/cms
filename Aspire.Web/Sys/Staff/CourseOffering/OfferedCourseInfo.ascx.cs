﻿using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Staff.CourseOffering
{
	public partial class OfferedCourseInfo : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		public int? OfferedCourseID
		{
			get => (int?)this.ViewState[nameof(this.OfferedCourseID)];
			private set => this.ViewState[nameof(this.OfferedCourseID)] = value;
		}

		public string Class => ((System.Web.UI.WebControls.Label)this.repeaterOfferedCourse.Items[0].FindControl("lblClass")).Text;
		public string Title => ((System.Web.UI.WebControls.Label)this.repeaterOfferedCourse.Items[0].FindControl("lblTitle")).Text;

		public void DataBind(BL.Core.CourseOffering.Staff.OfferedCourses.OfferedCourse offeredCourse)
		{
			if (offeredCourse == null)
				throw new ArgumentNullException(nameof(offeredCourse));
			this.OfferedCourseID = offeredCourse?.OfferedCourseID;
			this.repeaterOfferedCourse.DataBind(new[] { offeredCourse });
		}
	}
}