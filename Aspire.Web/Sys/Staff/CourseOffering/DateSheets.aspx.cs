﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.CourseOffering
{
	[AspirePage("DD0C50FD-EB8F-4078-BC40-AA4887738A40", UserTypes.Staff, "~/Sys/Staff/CourseOffering/DateSheets.aspx", "Date Sheet", true, AspireModules.CourseOffering)]
	public partial class DateSheets : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.CourseOffering.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.calendar);
		public override string PageTitle => "Date Sheet";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
			}
		}
	}
}