﻿using Aspire.BL.Core.CourseOffering.Staff;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.CourseOffering
{
	public partial class OfferedCourseViewStudentsModal : System.Web.UI.UserControl
	{
		private new OfferCourses Page => (OfferCourses)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
				this.Visible = false;
		}

		public void ShowModal(int offeredCourseID)
		{
			var offeredCourse = BL.Core.CourseOffering.Staff.OfferedCourses.GetOfferedCourse(offeredCourseID, this.Page.StaffIdentity.LoginSessionGuid);
			if (offeredCourse == null)
			{
				this.Page.AddNoRecordFoundAlert();
				this.Page.Refresh();
				return;
			}
			this.offeredCourseInfo.DataBind(offeredCourse);

			var (registeredStudents, requestedStudents) = BL.Core.CourseOffering.Staff.OfferedCourses.GetStudents(offeredCourseID, this.Page.StaffIdentity.LoginSessionGuid);
			this.gvRegisteredStudents.DataBind(registeredStudents, "Enrollment", SortDirection.Ascending);
			this.gvRequestedStudents.DataBind(requestedStudents, "Enrollment", SortDirection.Ascending);
			this.Visible = true;
			this.modal.Show();
		}
	}
}