﻿using Aspire.BL.Core.CourseOffering.Staff;
using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Staff.CourseOffering
{
	public partial class OfferedCourseAssignFacultyMemberModal : System.Web.UI.UserControl
	{
		private new OfferCourses Page => (OfferCourses)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
				this.Visible = false;
		}

		public void ShowModal(int offeredCourseID)
		{
			this.Page.StaffIdentity.Demand(StaffPermissions.CourseOffering.AssignTeacher, Model.Entities.UserGroupPermission.PermissionValues.Allowed);
			var offeredCourse = BL.Core.CourseOffering.Staff.OfferedCourses.GetOfferedCourse(offeredCourseID, this.Page.StaffIdentity.LoginSessionGuid);
			if (offeredCourse == null)
			{
				this.Page.AddNoRecordFoundAlert();
				this.Page.Refresh();
				return;
			}
			this.offeredCourseInfo.DataBind(offeredCourse);
			var facultyMembers = BL.Core.Common.Lists.GetFacultyMembersList(this.Page.StaffIdentity.InstituteID, null);
			this.ddlFacultyMemberID.DataBind(facultyMembers, CommonListItems.Select);
			this.ddlFacultyMemberID.SelectedValue = offeredCourse.FacultyMemberID.ToString();
			this.Visible = true;
			this.modal.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (this.offeredCourseInfo.OfferedCourseID == null)
				throw new InvalidOperationException();
			var facultyMemberID = this.ddlFacultyMemberID.SelectedValue.ToNullableInt();
			var status = BL.Core.CourseOffering.Staff.OfferedCourses.AssignFacultyMember(this.offeredCourseInfo.OfferedCourseID.Value, facultyMemberID, this.Page.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.CourseOffering.Staff.OfferedCourses.AssignFacultyMemberStatuses.NoRecordFound:
					this.Page.AddNoRecordFoundAlert();
					this.Page.Refresh();
					return;
				case BL.Core.CourseOffering.Staff.OfferedCourses.AssignFacultyMemberStatuses.MarksEntryLocked:
					this.alert.AddErrorAlert("Faculty Member can not be assigned/changed when record is locked.");
					this.updatePanel.Update();
					return;
				case BL.Core.CourseOffering.Staff.OfferedCourses.AssignFacultyMemberStatuses.Success:
					if (facultyMemberID != null)
						this.Page.AddSuccessAlert("Faculty Member has been assigned to the Offered Course.");
					else
						this.Page.AddSuccessAlert("Faculty Member has been removed from the Offered Course.");
					this.Page.Refresh();
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}