﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Exams
{
	[AspirePage("AE72FF00-0F2C-4D46-B83E-97E41A10C089", UserTypes.Staff, "~/Sys/Staff/Exams/ExamMarksPolicy.aspx", "Exam Marks Policy", true, AspireModules.Exams)]
	public partial class ExamMarksPolicy : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Exams.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};
		public override PageIcon PageIcon => AspireGlyphicons.book.GetIcon();
		public override string PageTitle => "Exam Marks Policy";

		public static string GetPageUrl(int? departmentID, int? programID, short? intakeSemesterID, short? semesterID)
		{
			return GetPageUrl<ExamMarksPolicy>().AttachQueryParams("DepartmentID", departmentID, "ProgramID", programID, "IntakeSemesterID", intakeSemesterID, "SemesterID", semesterID);
		}

		public static void Redirect(int? departmentID, int? programID, short? intakeSemesterID, short? semesterID)
		{
			Redirect(GetPageUrl(departmentID, programID, intakeSemesterID, semesterID));
		}

		private void RefreshPage()
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var intakeSemesterID = this.ddlIntakeSemesterIDFilter.SelectedValue.ToNullableShort();
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			Redirect(departmentID, programID, intakeSemesterID, semesterID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All).SetSelectedValueIfNotPostback("DepartmentID");
				this.ddlIntakeSemesterIDFilter.FillSemesters(CommonListItems.All).SetSelectedValueIfNotPostback("IntakeSemesterID");
				this.ddlSemesterIDFilter.FillSemesters(CommonListItems.All);
				if (this.ddlSemesterIDFilter.Items.Count > 1)
					this.ddlSemesterIDFilter.SelectedIndex = 1;
				this.ddlSemesterIDFilter.SetSelectedValueIfNotPostback("SemesterID");
				this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, departmentID, true, CommonListItems.All).SetSelectedValueIfNotPostback("ProgramID");
			this.ddlProgramIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlIntakeSemesterIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlIntakeSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData();
		}

		public Aspire.BL.Core.Exams.Staff.ExamMarksPolicy.GetExamMarksPoliciesForCrossTabGridResult Result;

		private void GetData()
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var intakeSemesterID = this.ddlIntakeSemesterIDFilter.SelectedValue.ToNullableShort();
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			this.Result = BL.Core.Exams.Staff.ExamMarksPolicy.GetExamMarksPoliciesForCrossTabGrid(departmentID, programID, intakeSemesterID, semesterID, this.AspireIdentity.LoginSessionGuid);
			this.repeaterProgram.DataBind(this.Result.CrossTabRows);
			if (semesterID != null && this.Result.CrossTabRows.Any())
			{
				var confirmMessage = $"Are you sure you want to copy Exam Marks Policy of below selected Program Batches from {Semester.PreviousSemester(semesterID.Value).ToSemesterString()} semester to {semesterID.ToSemesterString()} semester?";
				this.btnCopyExamMarksPolicy.ConfirmMessage = confirmMessage;
				this.btnCopyExamMarksPolicy.Enabled = true;
			}
			else
			{
				this.btnCopyExamMarksPolicy.Enabled = false;
			}
		}

		protected void btnAssignUnAssignExamMarksPolicy_OnClick(object sender, EventArgs e)
		{
			this.DisplayAssignPolicyModal(null, null, null, null, null);
		}

		private void DisplayAssignPolicyModal(List<int> programIDs, short? intakeSemesterIDFrom, short? intakeSemesterIDTo, short? semesterIDFrom, short? semesterIDTo)
		{
			this.lbProgramIDs.FillPrograms(this.StaffIdentity.InstituteID, null, true);
			this.lbProgramIDs.ClearSelection();
			if (programIDs != null)
				foreach (var programID in programIDs)
				{
					var item = this.lbProgramIDs.Items.FindByValue(programID.ToString());
					if (item != null)
						item.Selected = true;
				}
			this.ddlIntakeSemesterFrom.FillSemesters(CommonListItems.Select).SetSelectedValueIfExists(intakeSemesterIDFrom.ToString());
			this.ddlIntakeSemesterTo.FillSemesters(CommonListItems.Select).SetSelectedValueIfExists(intakeSemesterIDTo.ToString());
			this.ddlSemesterFrom.FillSemesters(CommonListItems.Select).SetSelectedValueIfExists(semesterIDFrom.ToString());
			this.ddlSemesterTo.FillSemesters(CommonListItems.Select).SetSelectedValueIfExists(semesterIDTo.ToString());
			var examMarksPolicies = BL.Core.Exams.Staff.ExamMarksPolicy.GetExamMarksPolicies(this.StaffIdentity.LoginSessionGuid);
			this.ddlExamMarksPolicyID.Items.Clear();
			this.ddlExamMarksPolicyID.Items.Add(new ListItem
			{
				Text = CommonListItems.NoneLabel,
				Value = CommonListItems.NoneValue,
				Attributes = { ["Description"] = "Selecting this option will un-assign the policy.", ["Summary"] = null, }
			});
			foreach (var examMarksPolicy in examMarksPolicies)
				this.ddlExamMarksPolicyID.Items.Add(new ListItem
				{
					Text = examMarksPolicy.ExamMarksPolicyName,
					Value = examMarksPolicy.ExamMarksPolicyID.ToString(),
					Attributes =
					{
						["Validity"] = examMarksPolicy.Validity,
						["Summary"] = examMarksPolicy.Summary,
						["Description"] = examMarksPolicy.ExamMarksPolicyDescription
					}
				});
			this.modalExamMarkPolicy.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var programIDs = this.lbProgramIDs.SelectedValues.Select(s => s.ToInt()).ToList();
			if (!programIDs.Any())
			{
				this.alertAssignPolicy.AddErrorAlert("Select at least one program.");
				this.updatePanelAssignPolicy.Update();
				return;
			}
			var intakeSemesterIDFrom = this.ddlIntakeSemesterFrom.SelectedValue.ToShort();
			var intakeSemesterIDTo = this.ddlIntakeSemesterTo.SelectedValue.ToShort();
			var semesterIDFrom = this.ddlSemesterFrom.SelectedValue.ToShort();
			var semesterIDTo = this.ddlSemesterTo.SelectedValue.ToShort();
			var examMarksPolicyID = this.ddlExamMarksPolicyID.SelectedValue.ToNullableInt();
			if (intakeSemesterIDFrom > intakeSemesterIDTo)
			{
				this.alertAssignPolicy.AddErrorAlert("Intake Semester (From) must be less than or equal to Intake Semester (To).");
				this.updatePanelAssignPolicy.Update();
				return;
			}
			if (semesterIDFrom > semesterIDTo)
			{
				this.alertAssignPolicy.AddErrorAlert("Semester (From) must be less than or equal to Semester (To).");
				this.updatePanelAssignPolicy.Update();
				return;
			}
			var result = BL.Core.Exams.Staff.ExamMarksPolicy.AssignUnAssignExamMarksPolicy(programIDs, intakeSemesterIDFrom, intakeSemesterIDTo, semesterIDFrom, semesterIDTo, examMarksPolicyID, this.StaffIdentity.LoginSessionGuid);
			this.AddSuccessAlert($"{result.UpdatedRecords} records have been updated out of {result.ValidRecords} valid record.");
			this.RefreshPage();
		}

		protected void repeaterSemesters_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Assign":
					var args = e.DecryptedCommandArgument().Split(':');
					var programID = args[0].ToInt();
					var intakeSemesterID = args[1].ToShort();
					var semesterID = args[2].ToShort();
					var examMarksPolicyID = args[3].ToNullableInt();
					this.DisplayAssignPolicyModal(new List<int> { programID }, intakeSemesterID, intakeSemesterID, semesterID, semesterID);
					this.lbProgramIDs.SelectedValue = programID.ToString();
					this.ddlIntakeSemesterFrom.SelectedValue = this.ddlIntakeSemesterTo.SelectedValue = intakeSemesterID.ToString();
					this.ddlSemesterFrom.SelectedValue = this.ddlSemesterTo.SelectedValue = semesterID.ToString();
					this.ddlExamMarksPolicyID.SelectedValue = examMarksPolicyID.ToString();
					break;
			}
		}

		protected void btnCopyExamMarksPolicy_OnClick(object sender, EventArgs e)
		{
			if (!this.btnCopyExamMarksPolicy.Enabled)
				return;
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var intakeSemesterID = this.ddlIntakeSemesterIDFilter.SelectedValue.ToNullableShort();
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var result = BL.Core.Exams.Staff.ExamMarksPolicy.CopyExamMarksPolicyFromPreviousSemester(departmentID, programID, intakeSemesterID, semesterID, this.StaffIdentity.LoginSessionGuid);
			if (result > 1)
				this.AddSuccessAlert($"{result} records updated.");
			else if (result == 1)
				this.AddSuccessAlert($"{result} record updated.");
			else
				this.AddWarningAlert("No record updated.");
			this.RefreshPage();
		}
	}
}