﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Exams
{
	[AspirePage("60C452ED-EB48-4A10-99E9-5B2EE3C9A9DB", UserTypes.Staff, "~/Sys/Staff/Exams/Courses.aspx", "Courses", true, AspireModules.Exams)]
	public partial class Courses : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Exams.Module, new []{UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => AspireGlyphicons.book.GetIcon();
		public override string PageTitle => "Courses";

		public static string GetPageUrl(short? semesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int? facultyMemberID, bool? marksEntryCompleted, bool? submittedToHOD, bool? submittedToCampus, bool? submittedToUniversity, int? pageIndex, int? pageSize, string sortExpression, SortDirection? sortDirection)
		{
			return typeof(Courses).GetAspirePageAttribute().PageUrl.AttachQueryParams(
				("SemesterID", semesterID),
				("DepartmentID", departmentID),
				("ProgramID", programID),
				("SemesterNo", semesterNoEnum),
				("Section", sectionEnum),
				("Shift", shiftEnum),
				("FacultyMemberID", facultyMemberID),
				("MarksEntryCompleted", marksEntryCompleted),
				("SubmittedToHOD", submittedToHOD),
				("SubmittedToCampus", submittedToCampus),
				("SubmittedToUniversity", submittedToUniversity),
				("PageIndex", pageIndex),
				("PageSize", pageSize),
				("SortExpression", sortExpression),
				("SortDirection", sortDirection));
		}

		public static void Redirect(short? semesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int? facultyMemberID, bool? marksEntryCompleted, bool? submittedToHOD, bool? submittedToCampus, bool? submittedToUniversity, int? pageIndex, int? pageSize, string sortExpression, SortDirection? sortDirection)
		{
			Redirect(GetPageUrl(semesterID, departmentID, programID, semesterNoEnum, sectionEnum, shiftEnum, facultyMemberID, marksEntryCompleted, submittedToHOD, submittedToCampus, submittedToUniversity, pageIndex, pageSize, sortExpression, sortDirection));
		}

		private void RefreshPage()
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
			var sectionEnum = this.ddlSection.GetSelectedEnumValue<Sections>(null);
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>(null);
			var facultyMemberID = this.ddlFacultyMemberID.SelectedValue.ToNullableInt();
			var marksEntryCompleted = this.ddlMarksEntryCompleted.SelectedValue.ToNullableBoolean();
			var submittedToHOD = this.ddlSubmittedToHOD.SelectedValue.ToNullableBoolean();
			var submittedToCampus = this.ddlSubmittedToCampus.SelectedValue.ToNullableBoolean();
			var submittedToUniversity = this.ddlSubmittedToUniversity.SelectedValue.ToNullableBoolean();
			var pageIndex = this.gvCourses.PageIndex;
			var pageSize = this.gvCourses.PageSize;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			Redirect(semesterID, departmentID, programID, semesterNoEnum, sectionEnum, shiftEnum, facultyMemberID, marksEntryCompleted, submittedToHOD, submittedToCampus, submittedToUniversity, pageIndex, pageSize, sortExpression, sortDirection);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var sortExpression = this.GetParameterValue("SortExpression") ?? "Title";
				var sortDirection = this.GetParameterValue<SortDirection>("SortDirection") ?? SortDirection.Ascending;
				this.ViewState.SetSortProperties(sortExpression, sortDirection);
				this.ddlSemesterID.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
				this.ddlDepartmentID.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All).SetSelectedValueIfNotPostback("DepartmentID");
				this.ddlSemesterNo.FillSemesterNos(CommonListItems.All).SetSelectedEnumValueIfNotPostback<SemesterNos>("SemesterNo");
				this.ddlSection.FillSections(CommonListItems.All).SetSelectedEnumValueIfNotPostback<Sections>("Section");
				this.ddlShift.FillShifts(CommonListItems.All).SetSelectedEnumValueIfNotPostback<Shifts>("Shift");
				this.ddlMarksEntryCompleted.FillYesNo(CommonListItems.All).SetSelectedBooleanValueIfNotPostback("MarksEntryCompleted");
				this.ddlSubmittedToHOD.FillYesNo(CommonListItems.All).SetSelectedBooleanValueIfNotPostback("SubmittedToHOD");
				this.ddlSubmittedToCampus.FillYesNo(CommonListItems.All).SetSelectedBooleanValueIfNotPostback("SubmittedToCampus");
				this.ddlSubmittedToUniversity.FillYesNo(CommonListItems.All).SetSelectedBooleanValueIfNotPostback("SubmittedToUniversity");
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		#region Filters

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var facultyMembers = BL.Core.Common.Lists.GetFacultyMembersList(this.StaffIdentity.InstituteID, semesterID);
			this.ddlFacultyMemberID.DataBind(facultyMembers, CommonListItems.All).SetSelectedValueIfNotPostback("FacultyMemberID");
			this.ddlDepartmentID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			this.ddlProgramID.FillPrograms(this.StaffIdentity.InstituteID, departmentID, true, CommonListItems.All).SetSelectedValueIfNotPostback("ProgramID");
			this.ddlProgramID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterNo_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterNo_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSection_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSection_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlShift_OnSelectedIndexChanged(null, null);
		}

		protected void ddlShift_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlFacultyMemberID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlFacultyMemberID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlMarksEntryCompleted_OnSelectedIndexChanged(null, null);
		}

		protected void ddlMarksEntryCompleted_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSubmittedToHOD_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSubmittedToHOD_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSubmittedToCampus_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSubmittedToCampus_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSubmittedToUniversity_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSubmittedToUniversity_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.gvCourses.PageSize = this.GetParameterValue<int>("PageSize") ?? this.gvCourses.PageSize;
				this.GetOfferedCourses(this.GetParameterValue<int>("PageIndex") ?? 0);
			}
			else
				this.GetOfferedCourses(0);
		}

		#endregion

		#region Gridview Events
		protected void gvCourses_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetOfferedCourses(0);
		}

		protected void gvCourses_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvCourses.PageSize = e.NewPageSize;
			this.GetOfferedCourses(0);
		}

		protected void gvCourses_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetOfferedCourses(e.NewPageIndex);
		}

		#endregion

		private void GetOfferedCourses(int pageIndex)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
			var sectionEnum = this.ddlSection.GetSelectedEnumValue<Sections>(null);
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>(null);
			var facultyMemberID = this.ddlFacultyMemberID.SelectedValue.ToNullableInt();
			var marksEntryCompleted = this.ddlMarksEntryCompleted.SelectedValue.ToNullableBoolean();
			var submittedToHOD = this.ddlSubmittedToHOD.SelectedValue.ToNullableBoolean();
			var submittedToCampus = this.ddlSubmittedToCampus.SelectedValue.ToNullableBoolean();
			var submittedToUniversity = this.ddlSubmittedToUniversity.SelectedValue.ToNullableBoolean();
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var courses = BL.Core.Exams.Staff.OfferedCourses.GetOfferedCourses(semesterID, departmentID, programID, semesterNoEnum, sectionEnum, shiftEnum, facultyMemberID, marksEntryCompleted, submittedToHOD, submittedToCampus, submittedToUniversity, pageIndex, this.gvCourses.PageSize, out var virtualItemCount, sortExpression, sortDirection, this.StaffIdentity.LoginSessionGuid);
			this.gvCourses.DataBind(courses, pageIndex, virtualItemCount, sortExpression, sortDirection);
		}
	}
}