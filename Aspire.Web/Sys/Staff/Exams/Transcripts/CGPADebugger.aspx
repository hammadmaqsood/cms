﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CGPADebugger.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Exams.Transcripts.CGPADebugger" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.BL.Core.Exams.Common.Transcripts" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Compile Up To Semester:" AssociatedControlID="ddlCompileUpToSemesterID" />
			<aspire:AspireDropDownList runat="server" ID="ddlCompileUpToSemesterID" ValidationGroup="Search" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollmentSearch" />
			<aspire:EnrollmentTextBox runat="server" ValidationGroup="Search" AllowNull="False" ID="tbEnrollmentSearch" OnSearch="tbEnrollmentSearch_OnSearch" />
		</div>
	</div>
	<p></p>
	<asp:Repeater runat="server" ID="repeaterTranscript" ItemType="Aspire.BL.Core.Exams.Common.Transcripts.Transcript">
		<ItemTemplate>
			<div>
				<table class="table table-bordered table-condensed tableCol4">
					<tbody>
						<tr>
							<th>Enrollment</th>
							<td><%#: Item.Enrollment %></td>
							<th>Registration No.</th>
							<td><%#: Item.RegistrationNo%></td>
						</tr>
						<tr>
							<th>Name</th>
							<td><%#: Item.Name %></td>
							<th>Father Name</th>
							<td><%#: Item.FatherName %></td>
						</tr>
						<tr>
							<th>Program</th>
							<td><%#: Item.ProgramShortName %></td>
							<th>Intake Semester</th>
							<td><%#: Item.IntakeSemesterID.ToSemesterString() %></td>
						</tr>
				</table>
				<asp:Repeater runat="server" ItemType="Aspire.BL.Core.Exams.Common.Transcripts.Transcript.Semester" DataSource="<%# Item.Semesters.OrderBy(s=> s.SemesterID) %>">
					<HeaderTemplate>
						<small>
							<table class="table table-bordered table-condensed table-hover">
					</HeaderTemplate>
					<ItemTemplate>
						<thead id="th<%# Item.SemesterID %>">
							<tr>
								<th colspan="12"><%#: Item.SemesterID.ToSemesterString() %></th>
							</tr>
							<tr>
								<th rowspan="2">#</th>
								<th rowspan="2">Roadmap Course</th>
								<th colspan="7">Registered Course</th>
								<th rowspan="2">Type</th>
								<th rowspan="2">GPA</th>
								<th rowspan="2">CGPA</th>
							</tr>
							<tr>
								<th>Semester</th>
								<th>Program</th>
								<th>Course</th>
								<th>Status</th>
								<th>Grade</th>
								<th>Grade Points</th>
								<th>Product</th>
							</tr>
						</thead>
						<tbody id="tbody<%# Item.SemesterID %>">
							<asp:Repeater runat="server" ItemType="Aspire.BL.Core.Exams.Common.Transcripts.Transcript.Semester.Course" DataSource="<%# Item.Courses.OrderBy(c=> c.OfferedSemesterID).ThenBy(c=> c.CourseIndex) %>">
								<ItemTemplate>
									<tr>
										<td><%#: Item.CourseIndex + 1 %></td>
										<td title="Course ID: <%#: Item.StudentRoadmapCourseID %>">Code: <%#: Item.StudentRoadmapCourseCode %>
											<br />
											Title:	<%#: Item.StudentRoadmapTitle %>
											<br />
											Credits: <%#: Item.StudentRoadmapCreditHours.ToCreditHoursFullName() %>
											Type: <%#: Item.StudentRoadmapCourseTypeEnum.ToFullName() %>
											<%# Item.CGPAStatus == Transcript.Semester.CGPAStatuses.ExcludedDueToRepeat ? "<span class=\"label label-default\">Repeat</span>" : "" %>
										</td>
										<td><%#: Item.OfferedSemesterID.ToSemesterString() %></td>
										<td><%#: Item.RegisteredCourseInstituteAlias %>
											<br />
											<%#: Item.RegisteredCourseProgramAlias %></td>
										<td title="RegisteredCourseID: <%#: Item.RegisteredCourseID %>">Code: <%#: Item.RegisteredCourseRoadmapCourseCode %>
											<br />
											Title: <%#: Item.RegisteredCourseRoadmapCourseTitle %>
											<br />
											Credits: <%#: Item.RegisteredCourseRoadmapCourseCreditHours.ToCreditHoursFullName() %>
										</td>
										<td><%# AspireFormats.GetRegisteredCourseStatusHtml(Item.RegisteredCourseStatusEnum, Item.RegisteredCourseFreezeStatusEnum) %></td>
										<td><%#: Item.RegisteredCourseGradeEnum.ToFullName() %></td>
										<td><%#: Item.RegisteredCourseGradePoints.FormatGradePoints() %></td>
										<td><%#: Item.RegisteredCourseProduct.FormatProduct() %></td>
										<td><%#: Item.CourseType %></td>
										<td><%# Item.GPAStatus == Transcript.Semester.GPAStatuses.Included ? "<span class=\"text-success glyphicon glyphicon-ok\"></span>" : $"<span class=\"text-danger glyphicon glyphicon-remove\" title=\"{Item.GPAStatus}\"></span>"  %></td>
										<td><%# Item.CGPAStatus == Transcript.Semester.CGPAStatuses.Included ? "<span class=\"text-success glyphicon glyphicon-ok\"></span>" : $"<span class=\"text-danger glyphicon glyphicon-remove\" title=\"{Item.CGPAStatus}\"></span>"  %></td>
									</tr>
								</ItemTemplate>
							</asp:Repeater>
						</tbody>
						<thead>
							<tr>
								<td colspan="12">
									<span class="text-left">
										<strong>Semester Status:</strong> <%#: Item.SemesterStatus %>
									</span>
									<span class="pull-right">
										<strong>GPA:</strong> <%#: Item.GPA.FormatGPA() %> <strong>CGPA:</strong> <%#: Item.CGPA.FormatGPA() %> <%# Item.ExamRemarksTypeFullName == null ? "" : $"<strong>Remarks:</strong> {Item.ExamRemarksTypeFullName}" %>
									</span>
								</td>
							</tr>
						</thead>
					</ItemTemplate>
					<FooterTemplate>
						</table>
					</FooterTemplate>
				</asp:Repeater>
			</div>
		</ItemTemplate>
	</asp:Repeater>

	<div class="text-center">
		<aspire:AspireButton runat="server" ID="btnUpdateResult" Text="Update GPA, CGPA and Remarks" ValidationGroup="Save" ConfirmMessage="Are you sure you want to continue?" OnClick="btnUpdateResult_OnClick" />
	</div>
</asp:Content>
