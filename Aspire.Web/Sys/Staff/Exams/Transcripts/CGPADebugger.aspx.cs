﻿using Aspire.BL.Core.Exams.Common.Transcripts;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Exams.Transcripts
{
	[AspirePage("B505193E-6D0D-4387-BB4E-D940B7FB0ED6", UserTypes.Staff, "~/Sys/Staff/Exams/Transcripts/CGPADebugger.aspx", "CGPA Debugger", true, AspireModules.Exams)]
	public partial class CGPADebugger : StaffPage
	{
		public override PageIcon PageIcon => AspireGlyphicons.cog.GetIcon();
		public override string PageTitle => "GPA/CGPA Debugger";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Exams.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};

		public static string GetPageUrl(short? compileUpToSemesterID, string enrollment, bool search)
		{
			return GetPageUrl<CGPADebugger>().AttachQueryParams("CompileUpToSemesterID", compileUpToSemesterID, nameof(Enrollment), enrollment, "Search", search);
		}

		public static void Redirect(short? compileUpToSemesterID, string enrollment, bool search)
		{
			Redirect(GetPageUrl(compileUpToSemesterID, enrollment, search));
		}

		private string Enrollment
		{
			get => (string)this.ViewState[nameof(this.Enrollment)];
			set => this.ViewState[nameof(this.Enrollment)] = value;
		}

		private int? StudentID
		{
			get => (int?)this.ViewState[nameof(this.StudentID)];
			set => this.ViewState[nameof(this.StudentID)] = value;
		}

		private short? CompileUpToSemesterID
		{
			get => (short?)this.ViewState[nameof(this.CompileUpToSemesterID)];
			set => this.ViewState[nameof(this.CompileUpToSemesterID)] = value;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlCompileUpToSemesterID.FillSemesters(CommonListItems.All).SetSelectedValueIfNotPostback("CompileUpToSemesterID");
				this.tbEnrollmentSearch.Enrollment = this.GetParameterValue(nameof(this.Enrollment));
				if (this.GetParameterValue<bool>("Search") == true)
					this.tbEnrollmentSearch_OnSearch(null, null);
				this.btnUpdateResult.Visible = false;
			}
		}

		protected void tbEnrollmentSearch_OnSearch(object sender, EventArgs args)
		{
			if (this.IsPostbackAndNotValid)
				return;
			var enrollment = this.tbEnrollmentSearch.Enrollment;
			var compileUpToSemesterID = this.ddlCompileUpToSemesterID.SelectedValue.ToNullableShort();
			var transcript = BL.Core.Exams.Staff.Transcripts.Transcript.GetTranscript(compileUpToSemesterID, null, enrollment, this.StaffIdentity.LoginSessionGuid);
			if (transcript == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect(compileUpToSemesterID, enrollment, false);
				return;
			}

			if (transcript.IntakeSemesterID < 20163)
			{
				this.AddSuccessAlert("Result cannot be compiled for Intake earlier than Fall-2016.");
				Redirect<CGPADebugger>();
				return;
			}

			transcript.Errors.ForEach(m => this.AddErrorAlert(m));
			transcript.Warnings.ForEach(m => this.AddWarningAlert(m));
			this.repeaterTranscript.DataSource = new[] { transcript };
			this.repeaterTranscript.DataBind();
			this.Enrollment = enrollment;
			this.StudentID = transcript.StudentID;
			this.btnUpdateResult.Visible = true;
		}

		protected void btnUpdateResult_OnClick(object sender, EventArgs e)
		{
			if (this.StudentID == null)
				return;
			var status = BL.Core.Exams.Staff.Transcripts.Transcript.CompileResult(this.CompileUpToSemesterID, this.StudentID.Value, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case GenerateTranscripts.CompileResultStatuses.NoRecordFound:
					Redirect(this.CompileUpToSemesterID, this.Enrollment, true);
					return;
				case GenerateTranscripts.CompileResultStatuses.Compiled:
					this.AddSuccessAlert("GPA, CGPA and Remarks have been compiled.");
					Redirect(this.CompileUpToSemesterID, this.Enrollment, true);
					return;
				case GenerateTranscripts.CompileResultStatuses.PreFall2016NotSupported:
					this.AddErrorAlert("Result cannot be compiled for Intake earlier than Fall-2016.");
					Redirect<CGPADebugger>();
					break;
				case GenerateTranscripts.CompileResultStatuses.NoChangesFound:
					this.AddWarningAlert("No changes found.");
					Redirect(this.CompileUpToSemesterID, this.Enrollment, true);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}