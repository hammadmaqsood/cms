﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using Aspire.Lib.Extensions;

namespace Aspire.Web.Sys.Staff.Exams
{
	[AspirePage("5275285A-51AD-4540-BC40-74A4CBEE762C", UserTypes.Staff, "~/Sys/Staff/Exams/CustomExamMarks.aspx", "Custom Exam Marks", true, AspireModules.Exams)]
	public partial class CustomExamMarks : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Exams.Module,new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => AspireGlyphicons.pencil.GetIcon();
		public override string PageTitle => "Custom Exam Marks";

		public static string GetPageUrl(int? studentID, string enrollment, bool search = true)
		{
			return GetAspirePageAttribute<ThesisInternships>().PageUrl.AttachQueryParams("StudentID", studentID, "Enrollment", enrollment, "Search", search);
		}

		public static void Redirect(int? studentID, string enrollment, bool search = true)
		{
			BasePage.Redirect(GetPageUrl(studentID, enrollment, search));
		}

		private void Refresh()
		{

		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Reset();
				string enrollment = null;
				var studentID = this.Request.GetParameterValue<int>("StudentID");
				if (studentID != null)
					enrollment = BL.Common.Common.GetEnrollmentFromStudentID(studentID.Value);
				if (enrollment == null)
					enrollment = this.Request.GetParameterValue("Enrollment");
				var search = this.Request.GetParameterValue<bool>("Search") == true;
#if DEBUG
				if (this.Request.IsLocal)
					enrollment = "01-244072-002";
#endif
				this.tbEnrollment.Enrollment = enrollment;
				if (!string.IsNullOrWhiteSpace(enrollment) && search)
					this.tbEnrollment_OnSearch(null, null);
			}
		}

		private int? StudentID => this.ucStudentInfo.StudentID;

		private void Reset()
		{
			this.ucStudentInfo.Reset();
			this.mainPanel.Visible = false;
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs e)
		{
			if (this.IsPostbackAndNotValid)
				return;
			var enrollment = this.tbEnrollment.Enrollment;
			var studentInfo = this.ucStudentInfo.LoadBasicStudentInformation(enrollment);
			if (studentInfo == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect(enrollment, false);
				return;
			}
			this.mainPanel.Visible = true;
			var registeredCourses = BL.Core.Exams.Staff.CustomExamMarks.GetRegisteredCourses(studentInfo.StudentID, null, this.StaffIdentity.LoginSessionGuid);
			this.gvRegisteredCourses.DataBind(registeredCourses);
			//this.panelThesisInternShip.Visible = true;
		}

		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
		public static List<BL.Core.Exams.Staff.CustomExamMarks.RegisteredCourseCustomMarks> GetRegisteredCourseMarks(string encryptedRegisteredCourseID)
		{
			var staffIdentity = StaffIdentity.Current;
			if (staffIdentity == null)
				return null;
			var registeredCourseID = encryptedRegisteredCourseID.Decrypt().ToInt();
			//return BL.Core.Exams.Staff.CustomExamMarks.GetRegisteredCourseCustomMarks(registeredCourseID, staffIdentity.LoginSessionGuid);
			return new List<BL.Core.Exams.Staff.CustomExamMarks.RegisteredCourseCustomMarks>
			{
				new BL.Core.Exams.Staff.CustomExamMarks.RegisteredCourseCustomMarks
				{
					DisplayIndex= 0, MarksTitle= "1", ObtainedMarks= 1, TotalMarks = 100
				},
				new BL.Core.Exams.Staff.CustomExamMarks.RegisteredCourseCustomMarks
				{
					DisplayIndex= 1, MarksTitle= "1", ObtainedMarks= 1, TotalMarks = 100
				},
			};
		}

		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
		public static object SaveRegisteredCourseMarks(string encryptedRegisteredCourseID, List<BL.Core.Exams.Staff.CustomExamMarks.RegisteredCourseCustomMarks> registeredCourseCustomMarks)
		{
			var staffIdentity = StaffIdentity.Current;
			if (staffIdentity == null)
				return new
				{
					Success = false,
					Message = "Invalid Session."
				};
			var registeredCourseID = encryptedRegisteredCourseID.Decrypt().ToInt();
			var result = BL.Core.Exams.Staff.CustomExamMarks.SaveRegisteredCourseCustomMarks(registeredCourseID, registeredCourseCustomMarks, staffIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.Exams.Staff.CustomExamMarks.SaveRegisteredCourseCustomMarksResults.NoRecordFound:
					return new
					{
						Success = false,
						Message = "No record found."
					};
				case BL.Core.Exams.Staff.CustomExamMarks.SaveRegisteredCourseCustomMarksResults.Success:
					return new
					{
						Success = true,
						Message = (string)null
					};
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}