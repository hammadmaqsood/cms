﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ProjectThesisInternshipMarksChangeDetails.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Exams.ProjectThesisInternshipMarksChangeDetails" %>

<asp:Content ContentPlaceHolderID="HeadPH" runat="server">
	<link href="../../../Scripts/Plugins/codediff.js/googlecode.css" rel="stylesheet" />
	<link href="../../../Scripts/Plugins/codediff.js/codediff.css" rel="stylesheet" />
</asp:Content>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:HiddenField runat="server" ID="hfOld" />
	<asp:HiddenField runat="server" ID="hfNew" />
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="lblAction" Text="Action:" />
				<div>
					<aspire:Label runat="server" ID="lblAction" />
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="lblChangeDate" Text="Date:" />
				<div>
					<aspire:Label runat="server" ID="lblChangeDate" />
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="lblChangedBy" Text="Changed By:" />
				<div>
					<aspire:Label runat="server" ID="lblChangedBy"></aspire:Label>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="lblChangedBy" Text="Display Only Differences:" />
				<div>
					<aspire:AspireRadioButtonList runat="server" ID="rblDisplayOnlyDifferences" RepeatDirection="Horizontal" RepeatLayout="Flow">
						<Items>
							<asp:ListItem Text="Yes" Value="10" Selected="True" />
							<asp:ListItem Text="No" Value="2147483647" />
						</Items>
					</aspire:AspireRadioButtonList>
				</div>
			</div>
		</div>
	</div>
	<div id="diffOutput"></div>
	<script src="<%= this.ResolveClientUrl("~/Scripts/Plugins/codediff.js/highlight.min.js") %>"></script>
	<script src="<%= this.ResolveClientUrl("~/Scripts/Plugins/codediff.js/difflib.js") %>"></script>
	<script src="<%= this.ResolveClientUrl("~/Scripts/Plugins/codediff.js/codediff.js") %>"></script>
	<script type="text/javascript">
		$(function () {
			var oldJson = $("#<%=this.hfOld.ClientID%>").val();
		var newJson = $("#<%=this.hfNew.ClientID%>").val();
		function displayDifference() {
			var minJumpSize = $("input[type=\"radio\"]:checked", $("#<%= this.rblDisplayOnlyDifferences.ClientID %>")).val().toInt();
			$(codediff.buildView(oldJson, newJson, {
				language: 'json',
				beforeName: 'Before',
				afterName: 'After',
				wordWrap: true,
				minJumpSize: minJumpSize
			})).appendTo($("#diffOutput").empty());
		}
		$("input[type=\"radio\"]", $("#<%= this.rblDisplayOnlyDifferences.ClientID %>")).change(displayDifference);
			displayDifference();
		});
	</script>
</asp:Content>
