﻿using Aspire.BL.Core.Exams.Common.New;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Exams
{
	[AspirePage("52437122-53A8-4ADF-BD8B-A3341F3E920B", UserTypes.Staff, "~/Sys/Staff/Exams/MarksSubmission.aspx", "Marks Submission", true, AspireModules.Exams)]
	public partial class MarksSubmission : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Exams.Module, new []{UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => AspireGlyphicons.book.GetIcon();
		public override string PageTitle => "Marks Submission";

		public static string GetPageUrl(short? semesterID, int? departmentID, bool? marksEntryCompleted, bool? submittedToHOD, bool? submittedToCampus, bool? submittedToUniversity, string sortExpression, SortDirection sortDirection)
		{
			return typeof(MarksSubmission).GetAspirePageAttribute().PageUrl.AttachQueryParams(
						("SemesterID", semesterID),
						("DepartmentID", departmentID),
						("MarksEntryCompleted", marksEntryCompleted),
						("SubmittedToHOD", submittedToHOD),
						("SubmittedToCampus", submittedToCampus),
						("SubmittedToUniversity", submittedToUniversity),
						("SortExpression", sortExpression),
						("SortDirection", sortDirection));
		}

		protected bool? MarksEntryCompleted => this.ddlMarksEntryCompleted.SelectedValue.ToNullableBoolean();
		protected bool? SubmittedToHOD => this.ddlSubmittedToHOD.SelectedValue.ToNullableBoolean();
		protected bool? SubmittedToCampus => this.ddlSubmittedToCampus.SelectedValue.ToNullableBoolean();
		protected bool? SubmittedToUniversity => this.ddlSubmittedToUniversity.SelectedValue.ToNullableBoolean();

		public static void Redirect(short? semesterID, int? departmentID, bool? marksEntryCompleted, bool? submittedToHOD, bool? submittedToCampus, bool? submittedToUniversity, string sortExpression, SortDirection sortDirection)
		{
			Redirect(GetPageUrl(semesterID, departmentID, marksEntryCompleted, submittedToHOD, submittedToCampus, submittedToUniversity, sortExpression, sortDirection));
		}

		protected string GetRefreshPageUrl()
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var marksEntryCompleted = this.ddlMarksEntryCompleted.SelectedValue.ToNullableBoolean();
			var submittedToHOD = this.ddlSubmittedToHOD.SelectedValue.ToNullableBoolean();
			var submittedToCampus = this.ddlSubmittedToCampus.SelectedValue.ToNullableBoolean();
			var submittedToUniversity = this.ddlSubmittedToUniversity.SelectedValue.ToNullableBoolean();
			return GetPageUrl(semesterID, departmentID, marksEntryCompleted, submittedToHOD, submittedToCampus, submittedToUniversity, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		private void RefreshPage()
		{
			Redirect(this.GetRefreshPageUrl());
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var sortExpression = this.GetParameterValue("SortExpression");
				var sortDirection = this.GetParameterValue<SortDirection>("SortDirection");
				this.ViewState.SetSortProperties(sortExpression ?? "DepartmentAlias", sortDirection ?? SortDirection.Ascending);
				this.ddlSemesterID.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
				this.ddlDepartmentID.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All).SetSelectedValueIfNotPostback("DepartmentID");
				this.ddlMarksEntryCompleted.FillYesNo(CommonListItems.All).SetSelectedBooleanValueIfNotPostback("MarksEntryCompleted");
				this.ddlSubmittedToHOD.FillYesNo(CommonListItems.All).SetSelectedBooleanValueIfNotPostback("SubmittedToHOD");
				this.ddlSubmittedToCampus.FillYesNo(CommonListItems.All).SetSelectedBooleanValueIfNotPostback("SubmittedToCampus");
				this.ddlSubmittedToUniversity.FillYesNo(CommonListItems.All).SetSelectedBooleanValueIfNotPostback("SubmittedToUniversity");

				this.GetData();
			}
			this.alertMarksSubmissionMessage.AddMessage(AspireAlert.AlertTypes.Warning, "This activity has to be performed after Final Exams.");
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlMarksEntryCompleted_OnSelectedIndexChanged(null, null);
		}

		protected void ddlMarksEntryCompleted_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSubmittedToHOD_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSubmittedToHOD_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSubmittedToCampus_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSubmittedToCampus_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSubmittedToUniversity_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSubmittedToUniversity_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData();
		}

		protected void gvPrograms_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData();
		}

		private List<MarksHelper.MarksSubmission> _programs;

		private void GetData()
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var marksEntryCompleted = this.ddlMarksEntryCompleted.SelectedValue.ToNullableBoolean();
			var submittedToHOD = this.ddlSubmittedToHOD.SelectedValue.ToNullableBoolean();
			var submittedToCampus = this.ddlSubmittedToCampus.SelectedValue.ToNullableBoolean();
			var submittedToUniversity = this.ddlSubmittedToUniversity.SelectedValue.ToNullableBoolean();
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			this._programs = MarksHelper.GetMarksSubmissionStatuses(semesterID, departmentID, marksEntryCompleted, submittedToHOD, submittedToCampus, submittedToUniversity, sortExpression, sortDirection, this.StaffIdentity.LoginSessionGuid);
			this.gvPrograms.ShowFooter = true;
			this.gvPrograms.DataBind(this._programs, sortExpression, sortDirection);
			if (this.gvPrograms.FooterRow != null)
			{
				((System.Web.UI.WebControls.Label)this.gvPrograms.FooterRow.FindControl("lblTotal")).Text = this._programs.Sum(p => p.Total).ToString();
				((System.Web.UI.WebControls.Label)this.gvPrograms.FooterRow.FindControl("lblMarksEntryCompleted")).Text = this._programs.Sum(p => p.MarksEntryCompleted).ToString();
				((System.Web.UI.WebControls.Label)this.gvPrograms.FooterRow.FindControl("lblSubmittedToHOD")).Text = this._programs.Sum(p => p.SubmittedToHOD).ToString();
				((System.Web.UI.WebControls.Label)this.gvPrograms.FooterRow.FindControl("lblSubmittedToCampus")).Text = this._programs.Sum(p => p.SubmittedToCampus).ToString();
				((System.Web.UI.WebControls.Label)this.gvPrograms.FooterRow.FindControl("lblSubmittedToUniversity")).Text = this._programs.Sum(p => p.SubmittedToUniversity).ToString();
			}

			this.btnSubmitToCampus.Visible = this.btnSafeSubmitToCampus.Visible = this._programs.Any();
			this.btnSubmitToUniversity.Visible = this._programs.Any() && marksEntryCompleted == null && submittedToHOD == null && submittedToCampus == null && submittedToUniversity != true;
			this.btnRevertSubmitToHOD.Visible = this._programs.Any() && this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.Exams.CanRevertSubmitToHOD) == UserGroupPermission.PermissionValues.Allowed;
			this.btnRevertSubmitToCampus.Visible = this._programs.Any() && this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.Exams.CanRevertSubmitToCampus) == UserGroupPermission.PermissionValues.Allowed;
			this.btnRevertSubmitToUniversity.Visible = this._programs.Any() && this.StaffIdentity.GetStaffPermissionValue(StaffPermissions.Exams.CanRevertSubmitToUniversity) == UserGroupPermission.PermissionValues.Allowed;
		}

		private List<MarksHelper.OfferedCourse> GetSelectedOfferedCourses(bool? submittedToHOD, bool? submittedToCampus, bool? submittedToUniversity)
		{
			var programIDs = new List<int>();
			foreach (GridViewRow row in this.gvPrograms.Rows)
				if (row.RowType == DataControlRowType.DataRow && ((AspireCheckBox)row.FindControl("cb")).Checked)
					programIDs.Add((int?)this.gvPrograms.DataKeys[row.DataItemIndex]?["ProgramID"] ?? throw new InvalidOperationException());
			if (!programIDs.Any())
				return new List<MarksHelper.OfferedCourse>();

			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var marksEntryCompleted = this.ddlMarksEntryCompleted.SelectedValue.ToNullableBoolean();
			return MarksHelper.GetOfferedCourses(semesterID, departmentID, marksEntryCompleted, submittedToHOD, submittedToCampus, submittedToUniversity, programIDs, this.StaffIdentity.LoginSessionGuid);
		}

		protected void btnSubmitToCampus_OnClick(object sender, EventArgs e)
		{
			var safeSubmit = sender == this.btnSafeSubmitToCampus;
			var submittedToHOD = this.ddlSubmittedToHOD.SelectedValue.ToNullableBoolean();
			var selectedOfferedCourses = safeSubmit
				? this.GetSelectedOfferedCourses(true, false, false)
				: this.GetSelectedOfferedCourses(submittedToHOD, false, false);
			if (!selectedOfferedCourses.Any())
			{
				this.AddErrorAlert("No record found.");
				this.RefreshPage();
				return;
			}

			this.gvOfferedCourses.DataBind(selectedOfferedCourses);
			this.hfAction.Value = "SubmitToCampus";
			this.modalOfferedCourses.Visible = true;
			this.modalOfferedCourses.Show();
			this.modalOfferedCourses.HeaderText = "Submit To Campus";
		}

		protected void btnSubmitToUniversity_OnClick(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var selectedOfferedCourses = MarksHelper.GetOfferedCourses(semesterID, null, null, null, null, false, null, this.StaffIdentity.LoginSessionGuid);
			if (!selectedOfferedCourses.Any())
			{
				this.AddErrorAlert("No record found.");
				this.RefreshPage();
				return;
			}
			this.gvOfferedCourses.DataBind(selectedOfferedCourses);
			this.hfAction.Value = "SubmitToUniversity";
			this.modalOfferedCourses.Visible = true;
			this.modalOfferedCourses.Show();
			this.modalOfferedCourses.HeaderText = "Submit to BUHO Exams";
		}

		protected void btnRevertSubmitToHOD_Click(object sender, EventArgs e)
		{
			var selectedOfferedCourses = this.GetSelectedOfferedCourses(true, false, false);
			if (!selectedOfferedCourses.Any())
			{
				this.AddErrorAlert("No record found.");
				this.RefreshPage();
				return;
			}
			this.gvOfferedCourses.DataBind(selectedOfferedCourses);
			this.hfAction.Value = "RevertSubmitToHOD";
			this.modalOfferedCourses.Visible = true;
			this.modalOfferedCourses.Show();
			this.modalOfferedCourses.HeaderText = "Revert to Faculty Members";
		}

		protected void btnRevertSubmitToCampus_Click(object sender, EventArgs e)
		{
			var selectedOfferedCourses = this.GetSelectedOfferedCourses(null, true, false);
			if (!selectedOfferedCourses.Any())
			{
				this.AddErrorAlert("No record found.");
				this.RefreshPage();
				return;
			}
			this.gvOfferedCourses.DataBind(selectedOfferedCourses);
			this.hfAction.Value = "RevertSubmitToCampus";
			this.modalOfferedCourses.Visible = true;
			this.modalOfferedCourses.Show();
			this.modalOfferedCourses.HeaderText = "Revert to HoDs/Faculty Members";
		}

		protected void btnRevertSubmitToUniversity_Click(object sender, EventArgs e)
		{
			var selectedOfferedCourses = this.GetSelectedOfferedCourses(null, null, true);
			if (!selectedOfferedCourses.Any())
			{
				this.AddErrorAlert("No record found.");
				this.RefreshPage();
				return;
			}
			this.gvOfferedCourses.DataBind(selectedOfferedCourses);
			this.hfAction.Value = "RevertSubmitToUniversity";
			this.modalOfferedCourses.Visible = true;
			this.modalOfferedCourses.Show();
			this.modalOfferedCourses.HeaderText = "Revert to Campus/HoDs/Faculty Members";
		}

		protected void btnCloseOfferedCourseModal_OnClick(object sender, EventArgs e)
		{
			this.RefreshPage();
		}

		public sealed class Input
		{
			public List<int> OfferedCourseIDs { get; set; }
		}

		[WebMethod(EnableSession = true, CacheDuration = 0, BufferResponse = false)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
		public static object SubmitToCampus(Input input)
		{
			var loginSessionGuid = Web.Common.StaffIdentity.Current?.LoginSessionGuid;
			if (loginSessionGuid == null)
				return "No session found.";
			var result = new List<object>();

			foreach (var offeredCourseID in input.OfferedCourseIDs.Distinct())
			{
				var submitResult = MarksHelper.SubmitToCampus(offeredCourseID, loginSessionGuid.Value);
				if (submitResult == null)
					result.Add(new
					{
						OfferedCourseID = offeredCourseID,
						Message = "No record found.",
						Success = false
					});
				else
					result.Add(new
					{
						OfferedCourseID = offeredCourseID,
						Message = submitResult.Value.submitToCampusFailureReasons.FirstOrDefault(),
						Success = submitResult.Value.success
					});
			}

			return result;
		}

		[WebMethod(EnableSession = true, CacheDuration = 0, BufferResponse = false)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
		public static object SubmitToUniversity(Input input)
		{
			var loginSessionGuid = Web.Common.StaffIdentity.Current?.LoginSessionGuid;
			if (loginSessionGuid == null)
				return "No session found.";
			var result = new List<object>();

			foreach (var offeredCourseID in input.OfferedCourseIDs.Distinct())
			{
				var submitResult = MarksHelper.SubmitToUniversity(offeredCourseID, loginSessionGuid.Value);
				if (submitResult == null)
					result.Add(new
					{
						OfferedCourseID = offeredCourseID,
						Message = "No record found.",
						Success = false
					});
				else
					result.Add(new
					{
						OfferedCourseID = offeredCourseID,
						Message = submitResult.Value.submitToUniversityFailureReasons.FirstOrDefault(),
						Success = submitResult.Value.success
					});
			}

			return result;
		}

		[WebMethod(EnableSession = true, CacheDuration = 0, BufferResponse = false)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
		public static object RevertSubmitToHOD(Input input)
		{
			var loginSessionGuid = Web.Common.StaffIdentity.Current?.LoginSessionGuid;
			if (loginSessionGuid == null)
				return "No session found.";
			var result = new List<object>();

			foreach (var offeredCourseID in input.OfferedCourseIDs.Distinct())
			{
				var submitResult = MarksHelper.RevertSubmitToHOD(offeredCourseID, loginSessionGuid.Value);
				if (submitResult == null)
					result.Add(new
					{
						OfferedCourseID = offeredCourseID,
						Message = "No record found.",
						Success = false
					});
				else
					result.Add(new
					{
						OfferedCourseID = offeredCourseID,
						Message = submitResult.Value.revertSubmitToHODFailureReasons.FirstOrDefault(),
						Success = submitResult.Value.success
					});
			}

			return result;
		}

		[WebMethod(EnableSession = true, CacheDuration = 0, BufferResponse = false)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
		public static object RevertSubmitToCampus(Input input)
		{
			var loginSessionGuid = Web.Common.StaffIdentity.Current?.LoginSessionGuid;
			if (loginSessionGuid == null)
				return "No session found.";
			var result = new List<object>();

			foreach (var offeredCourseID in input.OfferedCourseIDs.Distinct())
			{
				var submitResult = MarksHelper.RevertSubmitToCampus(offeredCourseID, loginSessionGuid.Value);
				if (submitResult == null)
					result.Add(new
					{
						OfferedCourseID = offeredCourseID,
						Message = "No record found.",
						Success = false
					});
				else
					result.Add(new
					{
						OfferedCourseID = offeredCourseID,
						Message = submitResult.Value.revertSubmitToCampusFailureReasons.FirstOrDefault(),
						Success = submitResult.Value.success
					});
			}

			return result;
		}

		[WebMethod(EnableSession = true, CacheDuration = 0, BufferResponse = false)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
		public static object RevertSubmitToUniversity(Input input)
		{
			var loginSessionGuid = Web.Common.StaffIdentity.Current?.LoginSessionGuid;
			if (loginSessionGuid == null)
				return "No session found.";
			var result = new List<object>();

			foreach (var offeredCourseID in input.OfferedCourseIDs.Distinct())
			{
				var submitResult = MarksHelper.RevertSubmitToUniversity(offeredCourseID, loginSessionGuid.Value);
				if (submitResult == null)
					result.Add(new
					{
						OfferedCourseID = offeredCourseID,
						Message = "No record found.",
						Success = false
					});
				else
					result.Add(new
					{
						OfferedCourseID = offeredCourseID,
						Message = submitResult.Value.revertSubmitToUniversityFailureReasons.FirstOrDefault(),
						Success = submitResult.Value.success
					});
			}

			return result;
		}
	}
}