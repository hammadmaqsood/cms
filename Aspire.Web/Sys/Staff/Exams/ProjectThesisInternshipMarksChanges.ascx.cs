﻿using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Staff.Exams
{
	public partial class ProjectThesisInternshipMarksChanges : System.Web.UI.UserControl
	{
		private new AspireBasePage Page => (AspireBasePage)base.Page;

		private int StudentID
		{
			get => (int)this.ViewState[nameof(this.StudentID)];
			set => this.ViewState[nameof(this.StudentID)] = value;
		}

		private Guid? ProjectThesisInternshipID
		{
			get => (Guid?)this.ViewState[nameof(this.ProjectThesisInternshipID)];
			set => this.ViewState[nameof(this.ProjectThesisInternshipID)] = value;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Visible = false;
			}
		}

		public void DisplayChanges(int studentID, Guid? projectThesisInternshipID)
		{
			this.StudentID = studentID;
			this.ProjectThesisInternshipID = projectThesisInternshipID;
			this.modalMarksChanges.Show();
			this.Visible = true;
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			var changes = BL.Core.Exams.Staff.ProjectThesisInternships.GetProjectThesisInternshipChanges(this.StudentID, pageIndex, this.gvMarksChanges.PageSize, StaffIdentity.Current.LoginSessionGuid);
			if (changes == null)
			{
				this.Page.AddNoRecordFoundAlert();
				this.modalMarksChanges.Hide();
				this.modalMarksChanges.Visible = false;
				return;
			}
			this.gvMarksChanges.DataBind(changes.Value.ProjectThesisInternshipChanges, pageIndex, changes.Value.VirtualItemCount);
		}

		protected void gvMarksChanges_PageSizeChanging(object sender, Lib.WebControls.AspireGridView.PageSizeEventArgs e)
		{
			this.gvMarksChanges.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvMarksChanges_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}
	}
}