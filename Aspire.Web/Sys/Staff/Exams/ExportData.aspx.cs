﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Staff.Exams
{
	[AspirePage("84892E16-9A10-4BEE-B8D3-519622625850", UserTypes.Staff, "~/Sys/Staff/Exams/ExportData.aspx", "Export Data", true, AspireModules.Exams)]
	public partial class ExportData : StaffPage
	{
		public override PageIcon PageIcon => AspireGlyphicons.export.GetIcon();
		public override string PageTitle => "Export Data";

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Exams.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.AddWarningAlert("Only locked record will be exported.");
				this.ddlSemesterID.FillSemesters(CommonListItems.All).SelectedIndex = 1;
				this.ddlFromIntakeSemesterID.FillSemesters();
				this.ddlFromIntakeSemesterID.SelectedValue = "20163";
				this.ddlFromIntakeSemesterID.Enabled = false;
				this.ddlUpToOfferedSemesterID.FillSemesters(20163);
			}
		}

		protected void btnExportMarks_OnClick(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var data = BL.Core.Exams.Staff.ExportData.GetData(semesterID, this.StaffIdentity.LoginSessionGuid);
			var csv = data.ConvertToCSVFormat(new List<CSVHelper.CSVColumnMapping>
			{
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "REGID", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.RegisteredCourseID)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "REGISTRATION", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.RegistrationNo)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "NAME", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.Name)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "ENROLLMENT", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.Enrollment)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "COURSE_OFF_ID", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.OfferedCourseID)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "COURSECODE", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.CourseCode)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "TITLE", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.Title)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PROGRAM", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.ProgramAlias)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "COURSE_ID", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.CourseID)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SECTION", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.SectionFullName)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SEM_OFFERED", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.SemesterNoFullName)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SHIFT", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.ShiftFullName)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CREDIT_HRS", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.CreditHours)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "YEAR", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.Year)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SEMESTER", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.SemesterType)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "STATUS", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.FullStatus)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "ATTENDANCE", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.Attendance)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "PRODUCT", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.Product)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "ASSIGNMENTS", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.Assignments)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "QUIZZES", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.Quizzes)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "INTERNALS", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.Internals)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "MID", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.Mid)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "FINAL", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.Final)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "TOTAL", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.Total)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "GRADE", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.GradeFullName)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "INSTITUTE", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.RegisteredCourse.InstituteAlias)},
			});

			this.Response.Clear();
			this.Response.Buffer = true;
			var instituteAlias = data.FirstOrDefault()?.InstituteAlias;
			var fileName = $"{instituteAlias ?? this.StaffIdentity.InstituteID.ToString()} - {semesterID} - {DateTime.Now:yyyyMMddhhmmss}.csv";
			this.Response.AddHeader("content-disposition", $"attachment;filename=\"{fileName}\"");
			this.Response.Charset = "";
			this.Response.ContentType = "text/csv";
			this.Response.Output.Write(csv);
			this.Response.Flush();
			this.Response.End();
		}

		protected void btnExportThesisProjects_Click(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var data = BL.Core.Exams.Staff.ExportData.GetThesisProjectData(semesterID, this.StaffIdentity.LoginSessionGuid);
			var csv = data.ConvertToCSVFormat(new List<CSVHelper.CSVColumnMapping>
			{
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Institute", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.ThesisInternshipRecord.InstituteAlias)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Record Type", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.ThesisInternshipRecord.RecordTypeFullName)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Semester", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.ThesisInternshipRecord.Semester)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Enrollment", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.ThesisInternshipRecord.Enrollment)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Registration No.", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.ThesisInternshipRecord.RegistrationNo)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Program", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.ThesisInternshipRecord.ProgramAlias)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Credit Hours", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.ThesisInternshipRecord.CreditHoursFullName)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Title", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.ThesisInternshipRecord.Title)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Organization", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.ThesisInternshipRecord.Organization)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Submission Date", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.ThesisInternshipRecord.SubmissionDate)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Defense Date", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.ThesisInternshipRecord.DefenceDate)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "From Date", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.ThesisInternshipRecord.FromDate)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "To Date", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.ThesisInternshipRecord.ToDate)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Obtained Marks", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.ThesisInternshipRecord.ObtainedMarks)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Total Marks", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.ThesisInternshipRecord.TotalMarks)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Grade", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.ThesisInternshipRecord.GradeFullName)},
			});

			this.Response.Clear();
			this.Response.Buffer = true;
			var instituteAlias = data.FirstOrDefault()?.InstituteAlias;
			var fileName = $"{instituteAlias ?? this.StaffIdentity.InstituteID.ToString()} - {semesterID} - {DateTime.Now:yyyyMMddhhmmss}.csv";
			this.Response.AddHeader("content-disposition", $"attachment;filename=\"{fileName}\"");
			this.Response.Charset = "";
			this.Response.ContentType = "text/csv";
			this.Response.Output.Write(csv);
			this.Response.Flush();
			this.Response.End();
		}

		protected void btnExportCGPA_Click(object sender, EventArgs e)
		{
			var fromIntakeSemesterID = this.ddlFromIntakeSemesterID.SelectedValue.ToShort();
			var upToOfferedSemesterID = this.ddlUpToOfferedSemesterID.SelectedValue.ToShort();
			var data = BL.Core.Exams.Staff.ExportData.GetCGPARecords(fromIntakeSemesterID, upToOfferedSemesterID, this.StaffIdentity.LoginSessionGuid);
			var csv = data.ConvertToCSVFormat(new List<CSVHelper.CSVColumnMapping>
			{
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Institute", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.StudentSemesterResult.InstituteAlias)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Program", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.StudentSemesterResult.ProgramAlias)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Program Duration", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.StudentSemesterResult.ProgramDuration)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Intake Semester ID", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.StudentSemesterResult.IntakeSemesterID)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Intake Semester", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.StudentSemesterResult.IntakeSemester)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Enrollment", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.StudentSemesterResult.Enrollment)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Registration No.", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.StudentSemesterResult.RegistrationNo)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Name", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.StudentSemesterResult.Name)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Father Name", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.StudentSemesterResult.FatherName)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Semester ID", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.StudentSemesterResult.SemesterID)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Semester", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.StudentSemesterResult.Semester)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "GPA", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.StudentSemesterResult.GPA)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "CGPA", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.StudentSemesterResult.CGPA)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "Remarks", PropertyName=nameof(BL.Core.Exams.Staff.ExportData.StudentSemesterResult.Remarks)},
			});

			this.Response.Clear();
			this.Response.Buffer = true;
			var instituteAlias = data.FirstOrDefault()?.InstituteAlias;
			var fileName = $"{instituteAlias ?? this.StaffIdentity.InstituteID.ToString()} - Intake{fromIntakeSemesterID}-{upToOfferedSemesterID} - {DateTime.Now:yyyyMMddhhmmss}.csv";
			this.Response.AddHeader("content-disposition", $"attachment;filename=\"{fileName}\"");
			this.Response.Charset = "";
			this.Response.ContentType = "text/csv";
			this.Response.Output.Write(csv);
			this.Response.Flush();
			this.Response.End();
		}
	}
}