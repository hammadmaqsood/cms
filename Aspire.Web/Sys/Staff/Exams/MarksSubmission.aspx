﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="MarksSubmission.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Exams.MarksSubmission" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Offered Semester:" AssociatedControlID="ddlSemesterID" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-4 form-group">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" OnSelectedIndexChanged="ddlDepartmentID_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Marks Entry Completed:" AssociatedControlID="ddlMarksEntryCompleted" />
			<aspire:AspireDropDownList runat="server" ID="ddlMarksEntryCompleted" OnSelectedIndexChanged="ddlMarksEntryCompleted_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Submitted to HOD:" AssociatedControlID="ddlSubmittedToHOD" />
			<aspire:AspireDropDownList runat="server" ID="ddlSubmittedToHOD" OnSelectedIndexChanged="ddlSubmittedToHOD_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Submitted to Campus:" AssociatedControlID="ddlSubmittedToCampus" />
			<aspire:AspireDropDownList runat="server" ID="ddlSubmittedToCampus" OnSelectedIndexChanged="ddlSubmittedToCampus_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Submitted to BUHO Exams:" AssociatedControlID="ddlSubmittedToUniversity" />
			<aspire:AspireDropDownList runat="server" ID="ddlSubmittedToUniversity" OnSelectedIndexChanged="ddlSubmittedToUniversity_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
	</div>
	<aspire:AspireGridView runat="server" ID="gvPrograms" ShowFooter="True" DataKeyNames="ProgramID" AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvPrograms_OnSorting" ItemType="Aspire.BL.Core.Exams.Common.New.MarksHelper.MarksSubmission">
		<Columns>
			<asp:TemplateField>
				<HeaderTemplate>
					<aspire:AspireCheckBox runat="server" CssClass="SelectAll" ID="cbSelectAll" />
				</HeaderTemplate>
				<ItemTemplate>
					<aspire:AspireCheckBox runat="server" CssClass="Select" ID="cb" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate>
					<%#: Container.DataItemIndex + 1 %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Offered Semester" SortExpression="OfferedSemesterID">
				<ItemTemplate>
					<%#: Item.OfferedSemesterID.ToSemesterString() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Department" SortExpression="DepartmentAlias">
				<ItemTemplate>
					<%#: Item.DepartmentAlias %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Program" SortExpression="ProgramAlias">
				<ItemTemplate>
					<%#: Item.ProgramAlias %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Total Offered Courses" SortExpression="Total">
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" Target="_blank" Text="<%# Item.Total %>" NavigateUrl="<%# Aspire.Web.Sys.Staff.Exams.Courses.GetPageUrl(Item.OfferedSemesterID, Item.DepartmentID, Item.ProgramID,null,null,null,null,this.MarksEntryCompleted,this.SubmittedToHOD,this.SubmittedToCampus,this.SubmittedToUniversity,null,null,null,null) %>" />
				</ItemTemplate>
				<FooterTemplate>
					<asp:Label runat="server" ID="lblTotal" />
				</FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Marks Entry Completed" SortExpression="MarksEntryCompleted">
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" Target="_blank" Text="<%# Item.MarksEntryCompleted %>" NavigateUrl="<%# Aspire.Web.Sys.Staff.Exams.Courses.GetPageUrl(Item.OfferedSemesterID, Item.DepartmentID, Item.ProgramID,null,null,null,null,true,this.SubmittedToHOD,this.SubmittedToCampus,this.SubmittedToUniversity,null,null,null,null) %>" />
				</ItemTemplate>
				<FooterTemplate>
					<asp:Label runat="server" ID="lblMarksEntryCompleted" />
				</FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Submitted To HoD" SortExpression="SubmittedToHOD">
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" Target="_blank" Text="<%# Item.SubmittedToHOD %>" NavigateUrl="<%# Aspire.Web.Sys.Staff.Exams.Courses.GetPageUrl(Item.OfferedSemesterID, Item.DepartmentID, Item.ProgramID,null,null,null,null,this.MarksEntryCompleted,true,this.SubmittedToCampus,this.SubmittedToUniversity,null,null,null,null) %>" />
				</ItemTemplate>
				<FooterTemplate>
					<asp:Label runat="server" ID="lblSubmittedToHOD" />
				</FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Submitted To Campus" SortExpression="SubmittedToCampus">
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" Target="_blank" Text="<%# Item.SubmittedToCampus %>" NavigateUrl="<%# Aspire.Web.Sys.Staff.Exams.Courses.GetPageUrl(Item.OfferedSemesterID, Item.DepartmentID, Item.ProgramID,null,null,null,null,this.MarksEntryCompleted,this.SubmittedToHOD,true,this.SubmittedToUniversity,null,null,null,null) %>" />
				</ItemTemplate>
				<FooterTemplate>
					<asp:Label runat="server" ID="lblSubmittedToCampus" />
				</FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Submitted to BUHO Exams" SortExpression="SubmittedToUniversity">
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" Target="_blank" Text="<%# Item.SubmittedToUniversity %>" NavigateUrl="<%# Aspire.Web.Sys.Staff.Exams.Courses.GetPageUrl(Item.OfferedSemesterID, Item.DepartmentID, Item.ProgramID,null,null,null,null,this.MarksEntryCompleted,this.SubmittedToHOD,this.SubmittedToCampus,true,null,null,null,null) %>" />
				</ItemTemplate>
				<FooterTemplate>
					<asp:Label runat="server" ID="lblSubmittedToUniversity" />
				</FooterTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<div>
		<aspire:AspireButton runat="server" Text="Submit To Campus" ID="btnSubmitToCampus" CausesValidation="False" OnClick="btnSubmitToCampus_OnClick" />
		<aspire:AspireButton runat="server" Text="Submit To Campus (Safe)" ToolTip="Only submit those records to Campus which are completed and submitted to HoD." ID="btnSafeSubmitToCampus" CausesValidation="False" OnClick="btnSubmitToCampus_OnClick" />
		<aspire:AspireButton runat="server" Text="Submit Whole Result to BUHO Exams" ID="btnSubmitToUniversity" CausesValidation="False" OnClick="btnSubmitToUniversity_OnClick" />
		<aspire:AspireButton runat="server" Text="Revert To Faculty Members" ID="btnRevertSubmitToHOD" CausesValidation="False" OnClick="btnRevertSubmitToHOD_Click" />
		<aspire:AspireButton runat="server" Text="Revert To HoDs/Faculty Members" ID="btnRevertSubmitToCampus" CausesValidation="False" OnClick="btnRevertSubmitToCampus_Click" />
		<aspire:AspireButton runat="server" Text="Revert To Campus/HoDs/Faculty Members" ID="btnRevertSubmitToUniversity" CausesValidation="False" OnClick="btnRevertSubmitToUniversity_Click" />
	</div>
	<aspire:AspireModal runat="server" ID="modalOfferedCourses" Visible="False" ModalSize="Large">
		<BodyTemplate>
			<asp:HiddenField runat="server" ID="hfAction" Visible="False" />
			<aspire:AspireGridView runat="server" ID="gvOfferedCourses" AutoGenerateColumns="False" ItemType="Aspire.BL.Core.Exams.Common.New.MarksHelper.OfferedCourse">
				<Columns>
					<asp:TemplateField HeaderText="#">
						<ItemTemplate>
							<%#: Container.DataItemIndex + 1 %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Title">
						<ItemTemplate>
							<div>
								<label class="label label-default"><%#: Item.FacultyMemberName.ToNAIfNullOrEmpty() %></label>
							</div>
							<%#: Item.Title %>
							<div>
								<label runat="server" visible="<%# Item.MarksEntryCompleted %>" class="label label-success">Marks Entry Completed</label>
								<label runat="server" visible="<%# Item.SubmittedToHOD %>" class="label label-success">Submitted to HOD</label>
								<label runat="server" visible="<%# Item.SubmittedToCampus %>" class="label label-success">Submitted to Campus</label>
								<label runat="server" visible="<%# Item.SubmittedToUniversity %>" class="label label-success">Submitted to BUHO Exams</label>
							</div>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Class">
						<ItemTemplate>
							<%#: Item.ClassName %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Status">
						<ItemTemplate>
							<div class="offeredCourse">
								<aspire:HiddenField runat="server" ID="hfOfferedCourseID" Mode="PlainText" Value="<%# Item.OfferedCourseID %>" />
								<span class="Status"></span>
							</div>
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</aspire:AspireGridView>
			<aspire:AspireAlert runat="server" ID="alertMarksSubmissionMessage" CloseButtonVisible="false" />
		</BodyTemplate>
		<FooterTemplate>
			<button id="btnStartSubmission" class="btn btn-primary" type="button"><span>Start Submission</span></button>
			<aspire:AspireButton runat="server" ButtonType="Default" CausesValidation="False" Text="Cancel" ID="btnCloseOfferedCourseModal" OnClick="btnCloseOfferedCourseModal_OnClick" />
		</FooterTemplate>
	</aspire:AspireModal>
	<script type="text/javascript">
		$(function () {
			function enableDisableButtons() {
				var enabled = $(".Select input[type=\"checkbox\"]:checked").length > 0;
				$("#<%=this.btnSubmitToCampus.ClientID%>").attr("data-enabled", enabled).toggleClass("disabled", !enabled);
				$("#<%=this.btnSafeSubmitToCampus.ClientID%>").attr("data-enabled", enabled).toggleClass("disabled", !enabled);
				$("#<%=this.btnRevertSubmitToHOD.ClientID%>").attr("data-enabled", enabled).toggleClass("disabled", !enabled);
				$("#<%=this.btnRevertSubmitToCampus.ClientID%>").attr("data-enabled", enabled).toggleClass("disabled", !enabled);
				$("#<%=this.btnRevertSubmitToUniversity.ClientID%>").attr("data-enabled", enabled).toggleClass("disabled", !enabled);
			}
			$(".SelectAll input[type=\"checkbox\"]").change(function () {
				$(".Select input[type=\"checkbox\"]").prop("checked", $(this).is(":checked"));
				enableDisableButtons();
			});
			$(".Select input[type=\"checkbox\"]").change(function () {
				$(".SelectAll input[type=\"checkbox\"]").prop("checked", $(".Select input[type=\"checkbox\"]:not(:checked)").length === 0);
				enableDisableButtons();
			});
			enableDisableButtons();
			var cancelProcessing = false;
			function submit(offeredCourses) {
				if (cancelProcessing) {
					console.log("Cancelled by user.");
					return false;
				}
				var offeredCourseIDs = [];
				for (var i = 0; i < offeredCourses.length; i++)
					offeredCourseIDs.push(offeredCourses[i].OfferedCourseID);
				if (offeredCourseIDs.length === 0)
					throw "Invalid Operation";
				$.ajax({
					type: "POST",
					async: false,
					url: window.aspireRootPath + "Sys/Staff/Exams/MarksSubmission.aspx/<%=this.hfAction.Value%>",
					data: JSON.stringify({ input: { OfferedCourseIDs: offeredCourseIDs } }),
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (response) {
						var results = response.d;
						for (var i = 0; i < results.length; i++) {
							var result = results[i];
							$.each(offeredCourses, function (index, offeredCourse) {
								if (result.OfferedCourseID === offeredCourse.OfferedCourseID) {
									if (result.Success)
										$("span.Status", $(offeredCourse.div)).text("Succeeded")
											.addClass("text-success");
									else
										$("span.Status", $(offeredCourse.div)).text(result.Message)
											.addClass("text-danger");
								}
							});
						}
					},
					error: function (jqXHR, status, errorThrown) {
						console.error(jqXHR);
						console.error(status);
						console.error(errorThrown);
						throw "Failed";
					}
				});

				for (var i = 0; i < offeredCourses.length; i++)
					$(offeredCourses[i].div).addClass("Processed");
			}

			function processSubmission() {
				var modal = $("#<%=this.modalOfferedCourses.ClientID%>");
				if (modal.length === 0)
					return;
				var btnStartSubmission = $("#btnStartSubmission", modal);
				if (btnStartSubmission.length === 0)
					return;
				btnStartSubmission.click(function () {
					$(btnStartSubmission).prop('disabled', true).prepend("<i class=\"fas fa-spinner fa-spin\"></i> ");
					while (true) {
						var offeredCourses = [];
						var pending = $(".offeredCourse:not(\".Processed\")", modal);
						for (var i = 0; i < pending.length; i++) {
							if (offeredCourses.length >= 10)
								break;
							var div = pending[i];
							var offeredCourseID = $("input[type=\"hidden\"]", $(div)).val().toInt();
							offeredCourses.push({ OfferedCourseID: offeredCourseID, div: div });
						}
						if (offeredCourses.length === 0)
							break;
						submit(offeredCourses);
						$("span", btnStartSubmission).text("Processed " + ($(".offeredCourse.Processed", modal).length) + " of " + $(".offeredCourse", modal).length);
					}
					$("i", btnStartSubmission).remove();
					if (cancelProcessing)
						$("span", btnStartSubmission).text("Process Cancelled");
					else
						$("span", btnStartSubmission).text("Process Completed");
					$("#<%=this.btnCloseOfferedCourseModal.ClientID%>").text("Close");
				});
			}
			processSubmission();
		});
	</script>
</asp:Content>
