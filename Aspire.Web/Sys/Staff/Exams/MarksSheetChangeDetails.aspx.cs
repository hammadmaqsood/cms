﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Exams
{
	[AspirePage("91A6B9EF-0C2C-42B7-B7C2-94B64BA5DD04", UserTypes.Staff, "~/Sys/Staff/Exams/MarksSheetChangeDetails.aspx", "Marks Sheet Change Details", true, AspireModules.Exams)]
	public partial class MarksSheetChangeDetails : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Exams.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_history);
		public override string PageTitle => "Marks Sheet Change Details";

		public static string GetPageUrl(int offeredCourseID, Guid? leftMarksSheetChangeID, Guid rightMarksSheetChangeID)
		{
			return GetPageUrl<MarksSheetChangeDetails>().AttachQueryParams("LeftMarksSheetChangeID", leftMarksSheetChangeID, "RightMarksSheetChangeID", rightMarksSheetChangeID, "OfferedCourseID", offeredCourseID);
		}

		public static void Redirect(int offeredCourseID, Guid? leftMarksSheetChangeID, Guid rightMarksSheetChangeID)
		{
			Redirect(GetPageUrl(offeredCourseID, leftMarksSheetChangeID, rightMarksSheetChangeID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
		}
	}
}