﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Exams
{
	[AspirePage("2FF15380-A555-45E1-9592-50AE9E59BEA2", UserTypes.Staff, "~/Sys/Staff/Exams/ProjectThesisInternships.aspx", "Project/Thesis/Internship", true, AspireModules.Exams)]
	public partial class ProjectThesisInternships : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Project/Thesis/Internships";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Exams.Module, UserGroupPermission.Allowed},
		};

		public static string GetPageUrl(int? studentID, bool search)
		{
			return GetAspirePageAttribute<ProjectThesisInternships>().PageUrl.AttachQueryParams("StudentID", studentID, "Search", search);
		}

		public static string GetPageUrl(string enrollment, bool search)
		{
			return GetAspirePageAttribute<ProjectThesisInternships>().PageUrl.AttachQueryParams("Enrollment", enrollment, "Search", search);
		}

		public static void Redirect(int? studentID, bool search)
		{
			BasePage.Redirect(GetPageUrl(studentID, search));
		}

		public static new void Redirect(string enrollment, bool search)
		{
			BasePage.Redirect(GetPageUrl(enrollment, search));
		}

		private void Refresh()
		{
			var studentID = (int?)this.ViewState[nameof(ProjectThesisInternship.StudentID)];
			Redirect(studentID, studentID != null);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Reset();
				string enrollment = null;
#if DEBUG
				enrollment = "01-133142-001";
#endif
				var studentID = this.Request.GetParameterValue<int>("StudentID");
				if (studentID != null)
					enrollment = BL.Common.Common.GetEnrollmentFromStudentID(studentID.Value);
				if (enrollment == null)
					enrollment = this.Request.GetParameterValue("Enrollment");
				var search = this.Request.GetParameterValue<bool>("Search") == true;
				this.tbEnrollment.Enrollment = enrollment;
				if (!string.IsNullOrWhiteSpace(enrollment) && search)
					this.tbEnrollment_OnSearch(null, null);
			}
		}

		private void Reset()
		{
			this.ViewState[nameof(ProjectThesisInternship.StudentID)] = null;
			this.ViewState[nameof(ProjectThesisInternship.ProjectThesisInternshipID)] = null;
			this.ucStudentInfo.Reset();
			this.panelThesisInternShip.Visible = false;
			this.gvProjectThesisInternships.DataBindNull();
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs e)
		{
			if (this.IsPostbackAndNotValid)
				return;
			var enrollment = this.tbEnrollment.Enrollment;
			var studentInfo = this.ucStudentInfo.LoadBasicStudentInformation(enrollment);
			if (studentInfo == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect(enrollment, false);
				return;
			}
			this.hlAdd.NavigateUrl = ProjectThesisInternshipMarks.GetPageUrl(studentInfo.StudentID, null);
			this.ViewState[nameof(Model.Entities.Student.StudentID)] = studentInfo.StudentID;
			var thesisInternships = BL.Core.Exams.Staff.ProjectThesisInternships.GetProjectThesisInternships(studentInfo.StudentID, this.StaffIdentity.LoginSessionGuid);
			this.gvProjectThesisInternships.DataBind(thesisInternships.OrderBy(t => t.SemesterID).ThenBy(t => t.RecordTypeEnum));
			this.panelThesisInternShip.Visible = true;
		}

		protected void gvProjectThesisInternships_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					var result = BL.Core.Exams.Staff.ProjectThesisInternships.DeleteProjectThesisInternship(e.DecryptedCommandArgumentToGuid(), this.StaffIdentity.LoginSessionGuid);
					switch (result)
					{
						case BL.Core.Exams.Staff.ProjectThesisInternships.DeleteProjectThesisInternshipStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.Exams.Staff.ProjectThesisInternships.DeleteProjectThesisInternshipStatuses.MarksEntryIsLocked:
							this.AddErrorAlert("Marks Entry is locked.");
							break;
						case BL.Core.Exams.Staff.ProjectThesisInternships.DeleteProjectThesisInternshipStatuses.SubmittedToUniversity:
							this.AddErrorAlert("Record cannot be deleted because marks have been submitted to University.");
							break;
						case BL.Core.Exams.Staff.ProjectThesisInternships.DeleteProjectThesisInternshipStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Record");
							break;
						case BL.Core.Exams.Staff.ProjectThesisInternships.DeleteProjectThesisInternshipStatuses.MarksCanNotBeEditWhileSubmittedToHOD:
							this.AddErrorAlert("Record cannot be deleted while marks are submitted to HoD.");
							break;
						default:
							throw new NotImplementedEnumException(result);
					}
					this.Refresh();
					return;
			}
		}

		protected void btnChangeHistory_OnClick(object sender, EventArgs e)
		{
			this.projectThesisInternshipMarksChanges.DisplayChanges(this.ucStudentInfo.StudentID?? throw new InvalidOperationException(), null);
		}
	}
}