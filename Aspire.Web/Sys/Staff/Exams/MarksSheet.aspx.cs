﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Staff.Exams
{
	[AspirePage("2F13E13B-3C79-421E-95FE-8FA9FBE8349C", UserTypes.Staff, "~/Sys/Staff/Exams/MarksSheet.aspx", "Marks Sheet", true, AspireModules.Exams)]
	public partial class MarksSheet : StaffPage
	{
		protected override System.Collections.Generic.IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Exams.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.list);
		public override string PageTitle => "Marks Sheet";

		public static string GetPageUrl(int offeredCourseID)
		{
			return GetPageUrl<MarksSheet>().AttachQueryParam("OfferedCourseID", offeredCourseID);
		}

		public static void Redirect(int offeredCourseID)
		{
			Redirect(GetPageUrl(offeredCourseID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
		}
	}
}