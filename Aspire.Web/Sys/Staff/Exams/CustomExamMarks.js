﻿function editMarks(hl) {
	var encryptedRegisteredCourseID = $(hl).attr("data-EncryptedID");
	var bodyContents = $("<div/>");

	var panel = createMarksEntryForm().appendTo(bodyContents);
	var modal = AspireModal.createFullModal("Marks", "modal-lg", true, bodyContents, "Save", "btn btn-primary", "Cancel", "btn btn-default");
	AspireModal.getFooterOkButton(modal).click(function () {
		if (!validateMarks(panel))
			return;
		var registeredCourseCustomMarks = getMarks(panel);
		CustomExamMarksWebMethods.SaveRegisteredCourseMarks(encryptedRegisteredCourseID, registeredCourseCustomMarks,
			function () {

			});
	});
	CustomExamMarksWebMethods.GetRegisteredCourseMarks(encryptedRegisteredCourseID,
		function (registeredCourseCustomMarks) {
			setMarks(panel, registeredCourseCustomMarks);
			AspireModal.open(modal, "static", true, true);
		});
}

function applyMarksMask(input) {
	$(input).inputmask({ "alias": "marks" });
}

function createMarksEntryForm() {
	var panel = $("<div/>").addClass("panel panel-default")
		.append($("<div/>").addClass("panel-heading").append($("<h3/>").addClass("panel-title").text("Marks")));
	var panelBody = $("<div/>").addClass("panel-body").appendTo(panel);
	for (var i = 0; i < 10; i++) {
		var row = $("<div/>").addClass("row").appendTo(panelBody);
		var formGroup = $("<div/>").addClass("form-group col-md-8").appendTo(row);
		var inputGroup = $("<div/>").addClass("input-group").appendTo(formGroup);
		$("<span/>").addClass("input-group-addon").appendTo(inputGroup).text(i + 1);
		var tbTitle = $("<input/>").attr({ "data-TrimText": true, placeholder: "Title", title: "Title", id: "tbTitle", type: "text" }).addClass("form-control col-md-8").appendTo(inputGroup);
		var spanErrorTitle = $("<span/>").addClass("text-danger").attr({ id: "spanErrorTitle" }).appendTo(formGroup).text("").hide();

		formGroup = $("<div/>").addClass("form-group col-md-2").appendTo(row);
		var tbObtainedMarks = $("<input/>").attr({ placeholder: "Obtained Marks", title: "Obtained Marks", id: "tbObtainedMarks", type: "text" }).addClass("form-control col-md-2").appendTo(formGroup);
		var spanErrorObtainedMarks = $("<span/>").addClass("text-danger").attr({ id: "spanErrorObtainedMarks" }).appendTo(formGroup).text("").hide();

		formGroup = $("<div/>").addClass("form-group col-md-2").appendTo(row);
		var tbTotalMarks = $("<input/>").attr({ placeholder: "Total Marks", title: "Total Marks", id: "tbTotalMarks", type: "text" }).addClass("form-control col-md-2").appendTo(formGroup);
		var spanErrorTotalMarks = $("<span/>").addClass("text-danger").attr({ id: "spanErrorTotalMarks" }).appendTo(formGroup).text("").hide();
		applyMarksMask(tbObtainedMarks);
		applyMarksMask(tbTotalMarks);
	}
	$("input", panel).on("blur", function () {
		validateRow($(this).closest("div.row"));
	});
	return panel;
}

function getTbTitle(row) { return $("input#tbTitle", $(row)) }
function getTbObtainedMarks(row) { return $("input#tbObtainedMarks", $(row)) }
function getTbTotalMarks(row) { return $("input#tbTotalMarks", $(row)) }
function getSpanErrorTitle(row) { return $("span#spanErrorTitle", $(row)); }
function getSpanErrorObtainedMarks(row) { return $("span#spanErrorObtainedMarks", $(row)); }
function getSpanErrorTotalMarks(row) { return $("span#spanErrorTotalMarks", $(row)); }

function isRowEmpty(row) {
	var tbTitle = getTbTitle(row);
	var tbObtainedMarks = getTbObtainedMarks(row);
	var tbTotalMarks = getTbTotalMarks(row);
	var spanErrorTitle = getSpanErrorTitle(row).hide();
	var spanErrorObtainedMarks = getSpanErrorObtainedMarks(row).hide();
	var spanErrorTotalMarks = getSpanErrorTotalMarks(row).hide();
	TrimAndTransformIfRequired(tbTitle);
	var title = tbTitle.val().trim();
	var obtainedMarks = tbObtainedMarks.val().trim();
	var totalMarks = tbTotalMarks.val().trim();
	return title.length === 0 && obtainedMarks.length === 0 && totalMarks.length === 0;
}

function makeRowRequired(row) {
	var spanErrorTitle = getSpanErrorTitle(row);
	var spanErrorObtainedMarks = getSpanErrorObtainedMarks(row);
	var spanErrorTotalMarks = getSpanErrorTotalMarks(row);
	spanErrorTitle.text("This field is required.").show();
	spanErrorObtainedMarks.text("This field is required.").show();
	spanErrorTotalMarks.text("This field is required.").show();
}

function validateRow(row) {
	if (isRowEmpty(row))
		return true;
	var tbTitle = getTbTitle(row);
	var tbObtainedMarks = getTbObtainedMarks(row);
	var tbTotalMarks = getTbTotalMarks(row);
	var spanErrorTitle = getSpanErrorTitle(row).hide();
	var spanErrorObtainedMarks = getSpanErrorObtainedMarks(row).hide();
	var spanErrorTotalMarks = getSpanErrorTotalMarks(row).hide();
	var title = tbTitle.val().trim();
	var obtainedMarks = tbObtainedMarks.val().trim();
	var totalMarks = tbTotalMarks.val().trim();
	if (!title.length)
		spanErrorTitle.text("This field is required.").show();
	if (obtainedMarks.length === 0)
		spanErrorObtainedMarks.text("This field is required.").show();
	if (totalMarks.length === 0)
		spanErrorTotalMarks.text("This field is required.").show();
	if (!$(spanErrorObtainedMarks).is(":visible") && obtainedMarks.tryToDouble() == null)
		spanErrorObtainedMarks.text("Invalid marks.").show();
	if (!$(spanErrorTotalMarks).is(":visible") && totalMarks.tryToDouble() == null)
		spanErrorTotalMarks.text("Invalid marks.").show();
	if (!$(spanErrorObtainedMarks).is(":visible") && !$(spanErrorTotalMarks).is(":visible") && obtainedMarks.tryToDouble() > totalMarks.tryToDouble())
		spanErrorObtainedMarks.text("Obtained Marks must be less than Total Marks.").show();
	return $(spanErrorTitle).is(":visible") || $(spanErrorObtainedMarks).is(":visible") || $(spanErrorTotalMarks).is(":visible");
}

function getRowMarks(row, displayIndex) {
	if (isRowEmpty(row))
		return null;
	var tbTitle = getTbTitle(row);
	var tbObtainedMarks = getTbObtainedMarks(row);
	var tbTotalMarks = getTbTotalMarks(row);
	return {
		DisplayIndex: displayIndex,
		MarksTitle: tbTitle.val().trim(),
		ObtainedMarks: tbObtainedMarks.val().trim().toDouble(),
		TotalMarks: tbTotalMarks.val().trim().toDouble(),
	};
}

function setRowMarks(row, registeredCourseCustomMarks) {
	if (registeredCourseCustomMarks != null) {
		getTbTitle(row).val(registeredCourseCustomMarks.MarksTitle);
		getTbObtainedMarks(row).val(registeredCourseCustomMarks.ObtainedMarks);
		getTbTotalMarks(row).val(registeredCourseCustomMarks.TotalMarks);
	}
	else {
		getTbTitle(row).val(null);
		getTbObtainedMarks(row).val(null);
		getTbTotalMarks(row).val(null);
	}
}

function validateMarks(panel) {
	var rows = $("div.row", panel);
	var nonEmptyFound = false;
	for (var i = rows.length - 1; i >= 0; i--) {
		var row = rows.get(i);
		var isEmpty = isRowEmpty(row);
		if (!isEmpty) {
			validateRow(row);
			nonEmptyFound = true;
		}
		else if (nonEmptyFound)
			makeRowRequired(row);
	}
	var errors = $("span.text-danger:visible", panel);
	if (errors.length > 0) {
		$(errors.get(0)).prev().focus();
		return false;
	}
	return true;
}

function getMarks(panel) {
	var marks = [];
	var rows = $("div.row", panel);
	var displayIndex = 0;
	for (var i = 0; i < rows.length ; i++) {
		var mark = getRowMarks(rows.get(i), displayIndex++);
		if (mark == null)
			break;
		marks.push(mark);
	}
	return marks;
}

function setMarks(panel, registeredCourseCustomMarks) {
	debugger;
	var rows = $("div.row", panel);
	for (var i = 0; i < rows.length ; i++) {
		var marks = registeredCourseCustomMarks != null && i < registeredCourseCustomMarks.length
			? registeredCourseCustomMarks[i]
			: null;
		setRowMarks(rows.get(i), marks);
	}
}

var CustomExamMarksWebMethods = {
	GetRegisteredCourseMarks: function (encryptedRegisteredCourseID, callback) {
		$.ajax({
			type: "POST",
			url: window.aspireRootPath + "Sys/Staff/Exams/CustomExamMarks.aspx/GetRegisteredCourseMarks",
			data: JSON.stringify({ encryptedRegisteredCourseID: encryptedRegisteredCourseID }),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			cache: false,
			success: function (response) {
				callback(response.d);
			},
			failure: function (msg) { console.error(msg); }
		});
	},
	SaveRegisteredCourseMarks: function (encryptedRegisteredCourseID, registeredCourseCustomMarks, callback) {
		$.ajax({
			type: "POST",
			url: window.aspireRootPath + "Sys/Staff/Exams/CustomExamMarks.aspx/SaveRegisteredCourseMarks",
			data: JSON.stringify({ encryptedRegisteredCourseID: encryptedRegisteredCourseID, registeredCourseCustomMarks: registeredCourseCustomMarks }),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			cache: false,
			success: function (response) {
				callback(response.d);
			},
			failure: function (msg) { console.error(msg); }
		});
	}
};