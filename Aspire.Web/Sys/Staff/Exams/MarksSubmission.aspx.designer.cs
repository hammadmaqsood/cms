﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Web.Sys.Staff.Exams
{


	public partial class MarksSubmission
	{

		/// <summary>
		/// ddlSemesterID control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlSemesterID;

		/// <summary>
		/// ddlDepartmentID control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlDepartmentID;

		/// <summary>
		/// ddlMarksEntryCompleted control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlMarksEntryCompleted;

		/// <summary>
		/// ddlSubmittedToHOD control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlSubmittedToHOD;

		/// <summary>
		/// ddlSubmittedToCampus control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlSubmittedToCampus;

		/// <summary>
		/// ddlSubmittedToUniversity control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlSubmittedToUniversity;

		/// <summary>
		/// gvPrograms control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireGridView gvPrograms;

		/// <summary>
		/// btnSubmitToCampus control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireButton btnSubmitToCampus;

		/// <summary>
		/// btnSafeSubmitToCampus control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireButton btnSafeSubmitToCampus;

		/// <summary>
		/// btnSubmitToUniversity control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireButton btnSubmitToUniversity;

		/// <summary>
		/// btnRevertSubmitToHOD control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireButton btnRevertSubmitToHOD;

		/// <summary>
		/// btnRevertSubmitToCampus control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireButton btnRevertSubmitToCampus;

		/// <summary>
		/// btnRevertSubmitToUniversity control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireButton btnRevertSubmitToUniversity;

		/// <summary>
		/// modalOfferedCourses control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireModal modalOfferedCourses;

		/// <summary>
		/// hfAction control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.WebControls.HiddenField hfAction;

		/// <summary>
		/// gvOfferedCourses control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireGridView gvOfferedCourses;

		/// <summary>
		/// alertMarksSubmissionMessage control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireAlert alertMarksSubmissionMessage;

		/// <summary>
		/// btnCloseOfferedCourseModal control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireButton btnCloseOfferedCourseModal;
	}
}
