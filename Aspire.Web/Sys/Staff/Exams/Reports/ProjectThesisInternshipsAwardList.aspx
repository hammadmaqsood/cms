﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="ProjectThesisInternshipsAwardList.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Exams.Reports.ProjectThesisInternshipsAwardList" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<asp:UpdatePanel runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
						<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" ValidationGroup="Parameters" />
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
						<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" CausesValidation="False" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" />
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
						<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" ValidationGroup="Parameters" />
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlRecordTypeFilter" />
						<aspire:AspireDropDownList runat="server" ID="ddlRecordTypeFilter" ValidationGroup="Parameters" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Completion (From):" AssociatedControlID="dtpCompletionDateFromFilter" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpCompletionDateFromFilter" ValidationGroup="Parameters" />
						<aspire:AspireDateTimeValidator runat="server" ValidationGroup="Parameters" AllowNull="True" ControlToValidate="dtpCompletionDateFromFilter" InvalidDataErrorMessage="Invalid date." InvalidRangeErrorMessage="Invalid date range." RequiredErrorMessage="This field is required." />
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Completion (To):" AssociatedControlID="dtpCompletionDateToFilter" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpCompletionDateToFilter" ValidationGroup="Parameters" />
						<aspire:AspireDateTimeValidator runat="server" ValidationGroup="Parameters" AllowNull="True" ControlToValidate="dtpCompletionDateToFilter" InvalidDataErrorMessage="Invalid date." InvalidRangeErrorMessage="Invalid date range." RequiredErrorMessage="This field is required." />
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Marks Entry Locked:" AssociatedControlID="ddlMarksEntryLocked" />
						<aspire:AspireDropDownList runat="server" ID="ddlMarksEntryLocked" ValidationGroup="Parameters" />
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Submitted to HoD:" AssociatedControlID="ddlSubmittedToHOD" />
						<aspire:AspireDropDownList runat="server" ID="ddlSubmittedToHOD" ValidationGroup="Parameters" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Submitted to Campus:" AssociatedControlID="ddlSubmittedToCampus" />
						<aspire:AspireDropDownList runat="server" ID="ddlSubmittedToCampus" ValidationGroup="Parameters" />
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Submitted to BUHO Exams:" AssociatedControlID="ddlSubmittedToUniversity" />
						<aspire:AspireDropDownList runat="server" ID="ddlSubmittedToUniversity" ValidationGroup="Parameters" />
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<div>
							<aspire:AspireLabel runat="server" AssociatedControlID="btnDisplay" Text="&nbsp;" />
						</div>
						<aspire:AspireButton runat="server" ID="btnDisplay" Text="Display" ValidationGroup="Parameters" OnClick="btnDisplay_OnClick" />
					</div>
				</div>
			</div>
		</ContentTemplate>
		<Triggers>
			<asp:PostBackTrigger ControlID="btnDisplay" />
		</Triggers>
	</asp:UpdatePanel>
</asp:Content>
