﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Exams.Reports
{
	[AspirePage("0AD16CEC-3B8D-4753-9A50-E0AF981660B3", UserTypes.Staff, "~/Sys/Staff/Exams/Reports/SemesterResult.aspx", "Semester Result", true, AspireModules.Exams)]
	public partial class SemesterResult : StaffPage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Semester Result";
		public string ReportName => this.PageTitle;
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Exams.Module,new [] { UserGroupPermission.PermissionValues.Allowed, }}
		};

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters();
				if (this.ddlSemesterIDFilter.Items.Count == 0)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All);
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, departmentID, true, CommonListItems.All);
			this.ddlProgramIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView.ReportViewer);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var reportDataSet = BL.Core.Exams.Staff.Reports.SemesterResult.GetSemesterResult(semesterID, departmentID, programID, this.StaffIdentity.LoginSessionGuid);
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/Exams/Staff/SemesterResult.rdlc".MapPath();
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.RegisteredCourses), reportDataSet.RegisteredCourses));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}