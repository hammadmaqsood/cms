﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="SemesterResult.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Exams.Reports.SemesterResult" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlProgramIDFilter_OnSelectedIndexChanged" />
			</div>
		</div>
	</div>
</asp:Content>
