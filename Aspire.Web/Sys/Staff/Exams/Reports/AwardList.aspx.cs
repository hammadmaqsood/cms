﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Exams.Reports
{
	[AspirePage("AFF892B0-6677-4D1E-96F9-652A6B006FF5", UserTypes.Staff, "~/Sys/Staff/Exams/Reports/AwardList.aspx", "Award List", true, AspireModules.Exams)]
	public partial class AwardList : StaffPage, IReportViewer
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Exams.Module,new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Award List";

		public string ReportName => "Award List";
		public bool ExportToExcel => true;
		public bool ExportToWord => false;

		private int? OfferedCourseID => this.Request.GetParameterValue<int>(nameof(this.OfferedCourseID));
		private bool? Revised => this.Request.GetParameterValue<bool>(nameof(this.Revised));

		public static string GetPageUrl(int offeredCourseID, bool revised, bool? inline, ReportFormats? reportFormat)
		{
			return GetPageUrl<AwardList>().AttachQueryParams(nameof(OfferedCourseID), offeredCourseID, nameof(Revised), revised, ReportView.ReportFormat, reportFormat, ReportView.Inline, inline);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.OfferedCourseID == null)
					Redirect<Courses>();
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this.OfferedCourseID != null)
				Sys.Common.Exam.CommonMethods.RenderAwardListReport<Courses>(reportViewer, this.OfferedCourseID.Value);
		}
	}
}