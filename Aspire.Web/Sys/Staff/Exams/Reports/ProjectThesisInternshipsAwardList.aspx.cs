﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Staff.Exams.Reports
{
	[AspirePage("327CCC42-3DB9-49D3-8D95-6279B5A7DBF4", UserTypes.Staff, "~/Sys/Staff/Exams/Reports/ProjectThesisInternshipsAwardList.aspx", "Project/Thesis/Internship Award List", true, AspireModules.Exams)]
	public partial class ProjectThesisInternshipsAwardList : StaffPage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Project/Thesis/Internship Award List";
		public string ReportName => this.PageTitle;
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Exams.Module,new [] { UserGroupPermission.PermissionValues.Allowed, }}
		};

		private bool _renderReport;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this._renderReport = false;
				this.ddlSemesterIDFilter.FillSemesters();
				if (this.ddlSemesterIDFilter.Items.Count == 0)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<Dashboard>();
					return;
				}

				this.ddlRecordTypeFilter.FillGuidEnums<ProjectThesisInternship.RecordTypes>();
				this.ddlMarksEntryLocked.FillYesNo(CommonListItems.All);
				this.ddlSubmittedToHOD.FillYesNo(CommonListItems.All);
				this.ddlSubmittedToCampus.FillYesNo(CommonListItems.All);
				this.ddlSubmittedToUniversity.FillYesNo(CommonListItems.All);
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All);
				this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, departmentID, true, CommonListItems.All);
		}

		protected void btnDisplay_OnClick(object sender, EventArgs e)
		{
			this._renderReport = true;
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView.ReportViewer);
			this._renderReport = false;
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (!this._renderReport)
				return;
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var recordTypeEnum = this.ddlRecordTypeFilter.GetSelectedGuidEnum<ProjectThesisInternship.RecordTypes>();
			var completionDateFrom = this.dtpCompletionDateFromFilter.SelectedDate?.Date;
			var completionDateTo = this.dtpCompletionDateToFilter.SelectedDate?.Date.AddDays(1);
			var marksEntryLocked = this.ddlMarksEntryLocked.SelectedValue.ToNullableBoolean();
			var submittedToHOD = this.ddlSubmittedToHOD.SelectedValue.ToNullableBoolean();
			var submittedToCampus = this.ddlSubmittedToCampus.SelectedValue.ToNullableBoolean();
			var submittedToUniversity = this.ddlSubmittedToUniversity.SelectedValue.ToNullableBoolean();
			var projectThesisInternships = BL.Core.Exams.Staff.Reports.ProjectThesisInternshipAwardList.GetData(semesterID, departmentID, programID, recordTypeEnum, completionDateFrom, completionDateTo, marksEntryLocked, submittedToHOD, submittedToCampus, submittedToUniversity, this.StaffIdentity.LoginSessionGuid);
			if (!projectThesisInternships.Any())
			{
				this.AddNoRecordFoundAlert();
				reportViewer.Reset();
				return;
			}
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/Exams/Staff/ProjectThesisInternshipsAwardList.rdlc".MapPath();
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("ProjectThesisInternships", projectThesisInternships));
			reportViewer.LocalReport.SetParameters(new ReportParameter("IsInternship", (recordTypeEnum == ProjectThesisInternship.RecordTypes.Internship).ToString()));
			reportViewer.LocalReport.SetParameters(new ReportParameter("IsProject", (recordTypeEnum == ProjectThesisInternship.RecordTypes.Project).ToString()));
			reportViewer.LocalReport.SetParameters(new ReportParameter("IsThesis", (recordTypeEnum == ProjectThesisInternship.RecordTypes.Thesis).ToString()));
			switch (recordTypeEnum)
			{
				case ProjectThesisInternship.RecordTypes.Internship:
					reportViewer.LocalReport.SetParameters(new ReportParameter("CoordinatorType", "Internship Coordinator"));
					reportViewer.LocalReport.SetParameters(new ReportParameter("ReportTitle", "Internship Result Award List"));
					break;
				case ProjectThesisInternship.RecordTypes.Project:
					reportViewer.LocalReport.SetParameters(new ReportParameter("CoordinatorType", "Research Coordinator"));
					reportViewer.LocalReport.SetParameters(new ReportParameter("ReportTitle", "Project Result Award List"));
					break;
				case ProjectThesisInternship.RecordTypes.Thesis:
					reportViewer.LocalReport.SetParameters(new ReportParameter("CoordinatorType", "Research Coordinator"));
					reportViewer.LocalReport.SetParameters(new ReportParameter("ReportTitle", "Thesis Result Award List"));
					break;
				default:
					throw new NotImplementedEnumException(recordTypeEnum);
			}
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}