﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Exams.Reports
{
	[AspirePage("80A13E36-405C-4C5E-A8C8-1963008DB889", UserTypes.Staff, "~/Sys/Staff/Exams/Reports/ExamResultStudentWise.aspx", "Provision Result Card", true, AspireModules.Exams)]
	public partial class ExamResultStudentWise : StaffPage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Provision Result Card";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Exams.Module, UserGroupPermission.Allowed},
		};

		public int? StudentID => this.Request.GetParameterValue<int>("StudentID");
		public static string GetPageUrl(int? studentID, bool? inline, ReportFormats? reportFormat)
		{
			return GetAspirePageAttribute<ExamResultStudentWise>().PageUrl.AttachQueryParams("StudentID", studentID, ReportView.Inline, inline, ReportView.ReportFormat, reportFormat);
		}

		public static string GetPageUrl(string enrollment, bool? inline, ReportFormats? reportFormat)
		{
			return GetAspirePageAttribute<ExamResultStudentWise>().PageUrl.AttachQueryParams("Enrollment", enrollment, ReportView.Inline, inline, ReportView.ReportFormat, reportFormat);
		}

		public static void Redirect(int? studentID)
		{
			Redirect<ExamResultStudentWise>("StudentID", studentID);
		}

		public static void Redirect(string enrollment)
		{
			Redirect<ExamResultStudentWise>("Enrollment", enrollment);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.StudentID == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Aspire.Web.Sys.Staff.Exams.ExamResultStudentWise>();
				}
			}
		}

		public string ReportName => this.PageTitle;
		public bool ExportToExcel => false;
		public bool ExportToWord => false;

		public void RenderReport(ReportViewer reportViewer)
		{
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/Exams/Staff/ExamResultStudentWise.rdlc".MapPath();
			var reportDataSet = BL.Core.Exams.Common.ExamResults.GetExamResult(this.StudentID.Value, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.StudentInformation), new[] { reportDataSet.StudentInformation}));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.ExamResults), reportDataSet.ExamResults));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}