﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Exams.Reports
{
	[AspirePage("48650E81-E33B-4A5B-8A27-92F47419607C", UserTypes.Staff, "~/Sys/Staff/Exams/Reports/SemesterFreeze.aspx", "Semester Freeze", true, AspireModules.Exams)]
	public partial class SemesterFreeze : StaffPage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Semester Freeze";
		public string ReportName => this.PageTitle;
		public bool ExportToExcel => true;
		public bool ExportToWord => true;
		private bool _renderDisabled = true;

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Exams.Module, new[] {Model.Entities.UserGroupPermission.PermissionValues.Allowed,}}
		};

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters();
				if (this.ddlSemesterIDFilter.Items.Count == 0)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All);
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}
		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, departmentID, false, CommonListItems.All);
		}

		protected void DisplayReport_OnClick(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this._renderDisabled = false;
			this.RenderReport(reportView.ReportViewer);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this._renderDisabled)
				return;
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/Exams/Staff/SemesterFreeze.rdlc".MapPath();
			var reportDataSet = BL.Core.Exams.Staff.Reports.SemesterFreeze.GetSemesterFreezeList(semesterID, departmentID, programID, this.StaffIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.Students), reportDataSet.Students));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}