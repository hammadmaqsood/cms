﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Exams
{
	[AspirePage("4c48edd3-6d57-49c9-8704-6bd7f1d9781d", UserTypes.Staff, "~/Sys/Staff/Exams/MarksSheetChanges.aspx", "Marks Sheet Changes", true, AspireModules.Exams)]
	public partial class MarksSheetChanges : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_history);
		public override string PageTitle => "Marks Sheet Changes";

		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Exams.Module, new [] { UserGroupPermission.PermissionValues.Allowed }}
		};

		public static string GetPageUrl(short? semesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int? facultyMemberID, string searchText, int? pageIndex, int? pageSize, string sortExpression, SortDirection? sortDirection)
		{
			return typeof(Courses).GetAspirePageAttribute().PageUrl.AttachQueryParams(
				("SemesterID", semesterID),
				("DepartmentID", departmentID),
				("ProgramID", programID),
				("SemesterNo", semesterNoEnum),
				("Section", sectionEnum),
				("Shift", shiftEnum),
				("FacultyMemberID", facultyMemberID),
				("SearchText", searchText),
				("PageIndex", pageIndex),
				("PageSize", pageSize),
				("SortExpression", sortExpression),
				("SortDirection", sortDirection));
		}

		public static void Redirect(short? semesterID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int? facultyMemberID, string searchText, int? pageIndex, int? pageSize, string sortExpression, SortDirection? sortDirection)
		{
			Redirect(GetPageUrl(semesterID, departmentID, programID, semesterNoEnum, sectionEnum, shiftEnum, facultyMemberID, searchText, pageIndex, pageSize, sortExpression, sortDirection));
		}

		private void RefreshPage()
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
			var sectionEnum = this.ddlSection.GetSelectedEnumValue<Sections>(null);
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>(null);
			var facultyMemberID = this.ddlFacultyMemberID.SelectedValue.ToNullableInt();
			var searchText = this.tbSearchText.Text.TrimAndMakeItNullIfEmpty();
			var pageIndex = this.gvMarksSheetChanges.PageIndex;
			var pageSize = this.gvMarksSheetChanges.PageSize;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			Redirect(semesterID, departmentID, programID, semesterNoEnum, sectionEnum, shiftEnum, facultyMemberID, searchText, pageIndex, pageSize, sortExpression, sortDirection);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var sortExpression = this.GetParameterValue("SortExpression") ?? "ActionDate";
				var sortDirection = this.GetParameterValue<SortDirection>("SortDirection") ?? SortDirection.Descending;
				this.ViewState.SetSortProperties(sortExpression, sortDirection);
				this.ddlSemesterID.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
				this.ddlDepartmentID.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All).SetSelectedValueIfNotPostback("DepartmentID");
				this.ddlSemesterNo.FillSemesterNos(CommonListItems.All).SetSelectedEnumValueIfNotPostback<SemesterNos>("SemesterNo");
				this.ddlSection.FillSections(CommonListItems.All).SetSelectedEnumValueIfNotPostback<Sections>("Section");
				this.ddlShift.FillShifts(CommonListItems.All).SetSelectedEnumValueIfNotPostback<Shifts>("Shift");
				this.tbSearchText.Text = this.GetParameterValue("SearchText");
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		#region Filters

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var facultyMembers = BL.Core.Common.Lists.GetFacultyMembersList(this.StaffIdentity.InstituteID, semesterID);
			this.ddlFacultyMemberID.DataBind(facultyMembers, CommonListItems.All).SetSelectedValueIfNotPostback("FacultyMemberID");
			this.ddlDepartmentID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			this.ddlProgramID.FillPrograms(this.StaffIdentity.InstituteID, departmentID, true, CommonListItems.All).SetSelectedValueIfNotPostback("ProgramID");
			this.ddlProgramID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterNo_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterNo_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSection_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSection_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlShift_OnSelectedIndexChanged(null, null);
		}

		protected void ddlShift_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlFacultyMemberID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlFacultyMemberID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearch_Click(null, null);
		}

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.gvMarksSheetChanges.PageSize = this.GetParameterValue<int>("PageSize") ?? this.gvMarksSheetChanges.PageSize;
				this.GetMarksSheetChanges(this.GetParameterValue<int>("PageIndex") ?? 0);
			}
			else
				this.GetMarksSheetChanges(0);
		}

		#endregion

		#region Gridview Events
		protected void gvMarksSheetChanges_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetMarksSheetChanges(0);
		}

		protected void gvMarksSheetChanges_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvMarksSheetChanges.PageSize = e.NewPageSize;
			this.GetMarksSheetChanges(0);
		}

		protected void gvMarksSheetChanges_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetMarksSheetChanges(e.NewPageIndex);
		}

		#endregion

		private void GetMarksSheetChanges(int pageIndex)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var semesterNoEnum = this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
			var sectionEnum = this.ddlSection.GetSelectedEnumValue<Sections>(null);
			var shiftEnum = this.ddlShift.GetSelectedEnumValue<Shifts>(null);
			var facultyMemberID = this.ddlFacultyMemberID.SelectedValue.ToNullableInt();
			var searchText = this.tbSearchText.Text.TrimAndMakeItNullIfEmpty();
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var courses = BL.Core.Exams.Staff.MarksSheetChanges.GetMarksSheetChanges(semesterID, departmentID, programID, semesterNoEnum, sectionEnum, shiftEnum, facultyMemberID, searchText, pageIndex, this.gvMarksSheetChanges.PageSize, out var virtualItemCount, sortExpression, sortDirection, this.StaffIdentity.LoginSessionGuid);
			this.gvMarksSheetChanges.DataBind(courses, pageIndex, virtualItemCount, sortExpression, sortDirection);
		}
	}
}