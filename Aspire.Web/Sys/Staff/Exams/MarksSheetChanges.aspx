﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="MarksSheetChanges.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Exams.MarksSheetChanges" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Offered Semester:" AssociatedControlID="ddlSemesterID" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-3 form-group">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" OnSelectedIndexChanged="ddlDepartmentID_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-3 form-group">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramID" OnSelectedIndexChanged="ddlProgramID_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-1 form-group">
			<aspire:AspireLabel runat="server" Text="Sem. #:" AssociatedControlID="ddlSemesterNo" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" OnSelectedIndexChanged="ddlSemesterNo_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-1 form-group">
			<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSection" />
			<aspire:AspireDropDownList runat="server" ID="ddlSection" OnSelectedIndexChanged="ddlSection_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShift" />
			<aspire:AspireDropDownList runat="server" ID="ddlShift" OnSelectedIndexChanged="ddlShift_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 form-group">
			<aspire:AspireLabel runat="server" Text="Teacher:" AssociatedControlID="ddlFacultyMemberID" />
			<aspire:AspireDropDownList runat="server" ID="ddlFacultyMemberID" OnSelectedIndexChanged="ddlFacultyMemberID_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-8 form-group">
			<aspire:AspireLabel runat="server" Text="Search:" AssociatedControlID="tbSearchText" />
			<asp:Panel runat="server" CssClass="input-group" DefaultButton="btnSearch">
				<aspire:AspireTextBox runat="server" ID="tbSearchText" ValidationGroup="Filters" />
				<div class="input-group-btn">
					<aspire:AspireButton ButtonType="Default" Glyphicon="search" runat="server" ID="btnSearch" ValidationGroup="Filters" OnClick="btnSearch_Click" />
				</div>
			</asp:Panel>
		</div>
	</div>
	<aspire:AspireGridView runat="server" ID="gvMarksSheetChanges" ItemType="Aspire.BL.Core.Exams.Staff.MarksSheetChanges.MarksSheetChange" AutoGenerateColumns="False" OnPageSizeChanging="gvMarksSheetChanges_OnPageSizeChanging" OnPageIndexChanging="gvMarksSheetChanges_OnPageIndexChanging" OnSorting="gvMarksSheetChanges_OnSorting" AllowSorting="True">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate>
					<%#: Container.DataItemIndex + 1 %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Semester" SortExpression="SemesterID">
				<ItemTemplate><%#: Item.SemesterID.ToSemesterString() %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Course" SortExpression="Title">
				<ItemTemplate><%#: Item.Title %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Faculty Member" SortExpression="FacultyMemberName">
				<ItemTemplate><%#: Item.FacultyMemberName %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Class" SortExpression="Class">
				<ItemTemplate><%#: Item.Class %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Action Type" SortExpression="ActionType">
				<ItemTemplate><%#: Item.ActionType.GetFullName() %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Action Date" SortExpression="ActionDate">
				<ItemTemplate><%#: $"{Item.ActionDate:g}" %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Changed By" SortExpression="ChangedByUserName">
				<ItemTemplate>
					<label class="label label-default"><%#: Item.ChangedByUserType.ToFullName() %></label>
					<div><%#: Item.ChangedByUserName %></div>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireHyperLink Target="_blank" runat="server" Text="Details" NavigateUrl='<%# Aspire.Web.Sys.Staff.Exams. MarksSheetChangeDetails.GetPageUrl(Item.OfferedCourseID, null, Item.MarksSheetChangeID) %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<script type="text/javascript">
		$(function () {
			$("#<%=this.ddlFacultyMemberID.ClientID%>").applySelect2();
		});
	</script>
</asp:Content>
