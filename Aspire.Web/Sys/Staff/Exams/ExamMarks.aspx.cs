﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Exams
{
	[AspirePage("64EF1A8D-D71C-452F-83A5-C5FC3E6E007F", UserTypes.Staff, "~/Sys/Staff/Exams/ExamMarks.aspx", "ExamMarks", true, AspireModules.Exams)]
	public partial class ExamMarks : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Exams.Module,new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => AspireGlyphicons.pencil.GetIcon();
		public override string PageTitle => this.ExamMarksType?.ToFullName();

		private ExamMarksTypes? ExamMarksType => this.Request.GetParameterValue<ExamMarksTypes>(nameof(this.ExamMarksType));

		public static string GetPageUrl(int offeredCourseID, ExamMarksTypes examMarksType)
		{
			return GetPageUrl<ExamMarks>().AttachQueryParams("OfferedCourseID", offeredCourseID, nameof(ExamMarksType), examMarksType);
		}

		public static void Redirect(int offeredCourseID, ExamMarksTypes examMarksType)
		{
			Redirect(GetPageUrl(offeredCourseID, examMarksType));
		}
	}
}