﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Aspire.Web.Sys.Staff.Exams
{
	[AspirePage("10D7EA09-B2C2-44EC-ACF4-700A8A7F90C6", UserTypes.Staff, "~/Sys/Staff/Exams/ProjectThesisInternshipMarksChangeDetails.aspx", "Project/Thesis/Internship Marks Change Details", true, AspireModules.Exams)]
	public partial class ProjectThesisInternshipMarksChangeDetails : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]> PermissionsRequired => new System.Collections.Generic.Dictionary<StaffPermissionType, Model.Entities.UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Exams.Module,new [] { Model.Entities.UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_history);
		public override string PageTitle => "Project/Thesis/Internship Marks Change Details";

		public static string GetPageUrl(int studentID, Guid projectThesisInternshipChangeID)
		{
			return GetPageUrl<ProjectThesisInternshipMarksChangeDetails>().AttachQueryParams("StudentID", studentID, "ProjectThesisInternshipChangeID", projectThesisInternshipChangeID);
		}

		public static void Redirect(int studentID, Guid projectThesisInternshipChangeID)
		{
			Redirect(GetPageUrl(studentID, projectThesisInternshipChangeID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var studentID = this.Request.GetParameterValue<int>("StudentID");
				var projectThesisInternshipChangeID = this.Request.GetParameterValue<Guid>("ProjectThesisInternshipChangeID");
				if (studentID != null && projectThesisInternshipChangeID != null)
					this.Display(studentID.Value, projectThesisInternshipChangeID.Value);
				else
					ProjectThesisInternships.Redirect(studentID, true);
			}
		}

		private void Display(int studentID, Guid projectThesisInternshipChangeID)
		{
			var projectThesisInternshipChange = BL.Core.Exams.Staff.ProjectThesisInternships.GetProjectThesisInternshipChange(projectThesisInternshipChangeID, studentID, this.StaffIdentity.LoginSessionGuid);
			if (projectThesisInternshipChange == null)
			{
				this.AddNoRecordFoundAlert();
				ProjectThesisInternships.Redirect(studentID, true);
				return;
			}
			this.hfOld.Value = projectThesisInternshipChange.OldJSON;
			this.hfNew.Value = projectThesisInternshipChange.NewJSON;
			this.lblAction.Text = projectThesisInternshipChange.ActionTypeFullName;
			this.lblChangeDate.Text = projectThesisInternshipChange.ActionDate.ToString(CultureInfo.CurrentCulture);
			this.lblChangedBy.Text = projectThesisInternshipChange.ChangedBy;
		}
	}
}