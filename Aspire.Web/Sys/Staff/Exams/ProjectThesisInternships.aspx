﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ProjectThesisInternships.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Exams.ProjectThesisInternships" %>

<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Import Namespace="Aspire.Web.Sys.Staff.Exams" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Register TagPrefix="uc" TagName="StudentInfo" Src="~/Sys/Staff/Common/StudentInfoUserControl.ascx" %>
<%@ Register Src="ProjectThesisInternshipMarksChanges.ascx" TagPrefix="uc" TagName="ProjectThesisInternshipMarksChanges" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
					<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="RequiredEnrollment" OnSearch="tbEnrollment_OnSearch" />
				</div>
				<div class="form-group">
					<aspire:AspireHyperLink runat="server" Text="Clear" NavigateUrl="ProjectThesisInternships.aspx" />
				</div>
			</div>
		</div>
	</div>
	<asp:Panel runat="server" ID="panelThesisInternShip">
		<uc:StudentInfo runat="server" ID="ucStudentInfo" />
		<aspire:AspireHyperLinkButton runat="server" ID="hlAdd" Text="Add Project/Thesis/Internship" Glyphicon="plus" CssClass="btn btn-success" NavigateUrl="ProjectThesisInternshipMarks.aspx" />
		<aspire:AspireButton runat="server" ID="btnChangeHistory" Text="Change History" FontAwesomeIcon="solid_history" CausesValidation="False" OnClick="btnChangeHistory_OnClick" />
		<p></p>
		<aspire:AspireGridView runat="server" ID="gvProjectThesisInternships" ItemType="Aspire.Model.Entities.ProjectThesisInternship" AutoGenerateColumns="False" OnRowCommand="gvProjectThesisInternships_OnRowCommand">
			<Columns>
				<asp:BoundField HeaderText="Type" DataField="RecordTypeFullName" />
				<asp:BoundField HeaderText="Semester" DataField="Semester" />
				<asp:TemplateField HeaderText="Registered Course">
					<ItemTemplate>
						<%#: Item.RegisteredCours?.Cours.Title ?? string.Empty.ToNAIfNullOrEmpty() %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="Total Marks" DataField="Total" />
				<asp:BoundField HeaderText="Grade" DataField="GradeFullName" />
				<asp:TemplateField HeaderText="Marks Entry Locked">
					<ItemTemplate><%#: Item.MarksEntryLocked.ToYesNo() %></ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Submitted to HOD">
					<ItemTemplate><%#: Item.SubmittedToHODDate?.ToString("d") ?? false.ToYesNo() %></ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Submitted to Campus">
					<ItemTemplate><%#: Item.SubmittedToCampusDate?.ToString("d") ?? false.ToYesNo() %></ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Submitted to BUHO">
					<ItemTemplate><%#: Item.SubmittedToUniversityDate?.ToString("d") ?? false.ToYesNo() %></ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField>
					<ItemTemplate>
						<aspire:AspireHyperLinkButton ToolTip="Edit" runat="server" ButtonType="OnlyIcon" Glyphicon="edit" Target="_blank" NavigateUrl="<%# ProjectThesisInternshipMarks.GetPageUrl(Item.StudentID, Item.ProjectThesisInternshipID) %>" />
						<aspire:AspireLinkButton ToolTip="Delete" runat="server" CausesValidation="False" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument="<%# this.Eval(nameof(ProjectThesisInternship.ProjectThesisInternshipID))%>" ConfirmMessage="Are you sure you want to delete?" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
	</asp:Panel>
	<uc:ProjectThesisInternshipMarksChanges runat="server" id="projectThesisInternshipMarksChanges" />
</asp:Content>
