﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="MarksSheet.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Exams.MarksSheet" %>

<%@ Register Src="~/Sys/Common/Exam/MarksSheet.ascx" TagPrefix="uc1" TagName="MarksSheet" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc1:MarksSheet runat="server" ID="ucMarksSheet" />
</asp:Content>
