﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ExamMarksPolicy.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Exams.ExamMarksPolicy" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-group">
		<aspire:AspireButton runat="server" ButtonType="Success" Glyphicon="check" Text="Assign/Un-assign Exam Marks Policy" CausesValidation="False" ID="btnAssignUnAssignExamMarksPolicy" OnClick="btnAssignUnAssignExamMarksPolicy_OnClick" />
	</div>
	<div class="row">
		<div class="col-md-3">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-3">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" OnSelectedIndexChanged="ddlProgramIDFilter_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-2">
			<aspire:AspireLabel runat="server" Text="Intake Semester:" AssociatedControlID="ddlIntakeSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlIntakeSemesterIDFilter" OnSelectedIndexChanged="ddlIntakeSemesterIDFilter_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-2">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-2">
			<div>
				<aspire:AspireLabel runat="server" Text="&nbsp;" AssociatedControlID="btnCopyExamMarksPolicy" />
			</div>
			<aspire:AspireButton runat="server" ButtonType="Primary" Glyphicon="copy" Text="Copy" CausesValidation="False" ID="btnCopyExamMarksPolicy" OnClick="btnCopyExamMarksPolicy_OnClick" />
		</div>
	</div>
	<p></p>
	<asp:Repeater runat="server" ID="repeaterProgram" ItemType="Aspire.BL.Core.Exams.Staff.ExamMarksPolicy.GetExamMarksPoliciesForCrossTabGridResult.CrossTabRow">
		<HeaderTemplate>
			<div class="table-responsive">
				<table class="table table-bordered table-hover table-condensed">
					<thead>
						<tr>
							<th rowspan="2">#</th>
							<th rowspan="2">Program</th>
							<th rowspan="2">Intake Semester</th>
							<asp:Repeater runat="server" ItemType="System.Int16" DataSource="<%# this.Result.SemesterIDs %>">
								<ItemTemplate>
									<th><%# Container.ItemIndex + 1 %></th>
								</ItemTemplate>
							</asp:Repeater>
						</tr>
						<tr>
							<asp:Repeater runat="server" ItemType="System.Int16" DataSource="<%# this.Result.SemesterIDs %>">
								<ItemTemplate>
									<th><%# Item.ToSemesterString() %></th>
								</ItemTemplate>
							</asp:Repeater>
						</tr>
					</thead>
					<tbody>
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
				<td><%# Container.ItemIndex+1 %></td>
				<td><%# Item.ProgramAlias %></td>
				<td><%# Item.IntakeSemesterID.ToSemesterString() %></td>
				<asp:Repeater runat="server" ID="repeaterSemesters" DataSource="<%# Item.Policies %>" ItemType="Aspire.BL.Core.Exams.Staff.ExamMarksPolicy.GetExamMarksPoliciesForCrossTabGridResult.CrossTabRow.Policy" OnItemCommand="repeaterSemesters_OnItemCommand">
					<ItemTemplate>
						<td class="<%# Item.CellCssClass %>" title="<%# Item.Tooltip %>">
							<aspire:AspireLinkButton Visible="<%# !string.IsNullOrEmpty(Item.CellCssClass) %>" runat="server" CommandName="Assign" EncryptedCommandArgument='<%# $"{Item.ProgramID}:{Item.IntakeSemesterID}:{Item.SemesterID}:{Item.ExamMarksPolicy?.ExamMarksPolicyID}" %>' Text='<%# Item.ExamMarksPolicy?.ExamMarksPolicyName ?? "N/A" %>' />
						</td>
					</ItemTemplate>
				</asp:Repeater>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</tbody>
			</table>
			</div>
		</FooterTemplate>
	</asp:Repeater>
	<aspire:AspireModal runat="server" ID="modalExamMarkPolicy" HeaderText="Assign/Un-assign Exam Marks Policy">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelAssignPolicy" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertAssignPolicy" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="lbProgramIDs" />
						<aspire:AspireListBox runat="server" ID="lbProgramIDs" SelectionMode="Multiple" CausesValidation="False" ValidationGroup="Assign" />
					</div>
					<div class="row">
						<div class="col-md-6 form-group">
							<aspire:AspireLabel runat="server" Text="Intake Semester (From):" AssociatedControlID="ddlIntakeSemesterFrom" />
							<aspire:AspireDropDownList runat="server" ID="ddlIntakeSemesterFrom" ValidationGroup="Assign" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlIntakeSemesterFrom" ErrorMessage="This field is required." ValidationGroup="Assign" />
						</div>
						<div class="col-md-6 form-group">
							<aspire:AspireLabel runat="server" Text="Intake Semester (To):" AssociatedControlID="ddlIntakeSemesterTo" />
							<aspire:AspireDropDownList runat="server" ID="ddlIntakeSemesterTo" ValidationGroup="Assign" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlIntakeSemesterTo" ErrorMessage="This field is required." ValidationGroup="Assign" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 form-group">
							<aspire:AspireLabel runat="server" Text="Semester (From):" AssociatedControlID="ddlSemesterFrom" />
							<aspire:AspireDropDownList runat="server" ID="ddlSemesterFrom" ValidationGroup="Assign" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterFrom" ErrorMessage="This field is required." ValidationGroup="Assign" />
						</div>
						<div class="col-md-6 form-group">
							<aspire:AspireLabel runat="server" Text="Semester (To):" AssociatedControlID="ddlSemesterTo" />
							<aspire:AspireDropDownList runat="server" ID="ddlSemesterTo" ValidationGroup="Assign" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterTo" ErrorMessage="This field is required." ValidationGroup="Assign" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Exam Marks Policy:" AssociatedControlID="ddlExamMarksPolicyID" />
						<aspire:AspireDropDownList runat="server" ID="ddlExamMarksPolicyID" ValidationGroup="Assign" />
						<span class="help-block">Select None to un-assign exam marks policy.</span>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Always">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="Assign" Text="Save" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
	<script type="text/javascript">
		$(function () {
			var init = function () {
				$("#<%=this.lbProgramIDs.ClientID%>").select2({
					theme: "bootstrap",
					width: "style",
					closeOnSelect: false,
					dropdownParent: $('#<%=this.modalExamMarkPolicy.ClientID%>')
				});
				$("#<%=this.ddlExamMarksPolicyID.ClientID%>").select2({
					theme: "bootstrap",
					width: "style",
					dropdownParent: $('#<%=this.modalExamMarkPolicy.ClientID%>'),
					templateResult: function (result) {
						var item = $("<div/>");
						$("<strong/>").text(result.text).appendTo($("<div/>").appendTo(item));
						$("<strong/>").text($(result.element).attr("Validity")).appendTo($("<em/>").appendTo($("<div/>").appendTo(item)));
						$("<strong/>").text($(result.element).attr("Summary")).appendTo($("<em/>").appendTo($("<div/>").appendTo(item)));
						$("<em/>").text($(result.element).attr("Description")).appendTo($("<div/>").appendTo(item));
						return item;
					}
				});
			};
			init();
			Sys.Application.add_load(init);
		});
	</script>
</asp:Content>
