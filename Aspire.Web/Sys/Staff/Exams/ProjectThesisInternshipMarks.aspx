﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ProjectThesisInternshipMarks.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Exams.ProjectThesisInternshipMarks" %>

<%@ Register Src="~/Sys/Staff/Exams/ProjectThesisInternshipMarksChanges.ascx" TagPrefix="uc1" TagName="ProjectThesisInternshipMarksChanges" %>


<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="table-responsive">
		<table class="table table-bordered table-condensed tableCol4">
			<caption>Student Information</caption>
			<tr>
				<th>Enrollment</th>
				<td>
					<aspire:Label runat="server" ID="lblEnrollment" /></td>
				<th>Registration No.</th>
				<td>
					<aspire:Label runat="server" ID="lblRegistrationNo" /></td>
			</tr>
			<tr>
				<th>Name</th>
				<td>
					<aspire:Label runat="server" ID="lblName" /></td>
				<th>Father Name</th>
				<td>
					<aspire:Label runat="server" ID="lblFatherName" /></td>
			</tr>
			<tr>
				<th>Program</th>
				<td>
					<aspire:Label runat="server" ID="lblProgramAlias" /></td>
				<th>Intake Semester</th>
				<td>
					<aspire:Label runat="server" ID="lblIntakeSemester" /></td>
			</tr>
			<tr>
				<th>Status</th>
				<td>
					<aspire:Label runat="server" ID="lblStatus" /></td>
				<th>Status Date</th>
				<td>
					<aspire:Label runat="server" ID="lblStatusDate" /></td>
			</tr>
			<tr>
				<th>Marks Entry Locked</th>
				<td>
					<aspire:Label runat="server" ID="lblMarksEntryLocked" /></td>
				<th>Submitted to HoD</th>
				<td>
					<aspire:Label runat="server" ID="lblSubmittedToHoD" /></td>
			</tr>
			<tr>
				<th>Submitted to Campus</th>
				<td>
					<aspire:Label runat="server" ID="lblSubmittedToCampus" /></td>
				<th>Submitted to BUHO Exams</th>
				<td>
					<aspire:Label runat="server" ID="lblSubmittedToBUHOExams" /></td>
			</tr>
		</table>
	</div>
	<div>
		<aspire:AspireButton ID="btnAttachRegisteredCourse" runat="server" ButtonType="Success" FontAwesomeIcon="solid_link" Text="Attach Registered Course" CausesValidation="False" OnClick="btnAttachRegisteredCourse_OnClick" />
		<aspire:AspireButton runat="server" ID="btnLockMarksEntry" ConfirmMessage="Are you sure you want to lock Marks Entry?" FontAwesomeIcon="solid_lock" Text="Lock Marks Entry" ButtonType="Danger" CausesValidation="False" OnClick="btnLockMarksEntry_OnClick" />
		<aspire:AspireButton runat="server" ID="btnUnlockMarksEntry" ConfirmMessage="Are you sure you want to unlock Marks Entry?" FontAwesomeIcon="solid_lock_open" Text="Unlock Marks Entry" ButtonType="Danger" CausesValidation="False" OnClick="btnUnlockMarksEntry_OnClick" />
		<div class="btn-group" runat="server" id="divSubmit">
			<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Submit <span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
				<li>
					<aspire:LinkButton runat="server" ID="lbtnSubmitToHOD" Text="Submit To HoD" ConfirmMessage="I certify that I have checked the result (marks and grades) and found them correct." CausesValidation="False" OnClick="lbtnSubmitToHOD_OnClick" />
				</li>
				<li>
					<aspire:LinkButton runat="server" ID="lbtnSubmitToCampus" Text="Submit To Campus" ConfirmMessage="Are you sure you want to submit marks to Campus?" CausesValidation="False" OnClick="lbtnSubmitToCampus_OnClick" />
				</li>
				<li>
					<aspire:LinkButton runat="server" ID="lbtnSubmitToBUHO" Text="Submit to BUHO Exams" ConfirmMessage="Are you sure you want to submit marks to BUHO Exams?" CausesValidation="False" OnClick="lbtnSubmitToBUHO_OnClick" />
				</li>
			</ul>
		</div>
		<aspire:AspireButton runat="server" ID="btnChangeHistory" Text="Change History" FontAwesomeIcon="solid_history" CausesValidation="False" OnClick="btnChangeHistory_OnClick" />
	</div>
	<div class="table-responsive">
		<table class="table table-bordered table-condensed tableCol6">
			<caption>Registered Course Information</caption>
			<tr>
				<th>Course Code</th>
				<td>
					<aspire:Label runat="server" ID="lblCourseCode" /></td>
				<th>Title</th>
				<td>
					<aspire:Label runat="server" ID="lblCourseTitle" /></td>
				<th>Credit Hours</th>
				<td>
					<aspire:Label runat="server" ID="lblCourseCreditHours" /></td>
			</tr>
		</table>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Record Type:" AssociatedControlID="ddlRecordType" />
				<aspire:AspireDropDownList runat="server" ID="ddlRecordType" AutoPostBack="True" OnSelectedIndexChanged="ddlRecordType_OnSelectedIndexChanged" ValidationGroup="ThesisInternship" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlRecordType" ErrorMessage="This field is required." ValidationGroup="ThesisInternship" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel ID="lblSemester" runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="ThesisInternship" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterID" ErrorMessage="This field is required." ValidationGroup="ThesisInternship" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel ID="lblCreditHours" runat="server" Text="Credit Hours:" AssociatedControlID="tbCreditHours" />
				<aspire:AspireTextBox runat="server" ID="tbCreditHours" MaxLength="4" ValidationGroup="ThesisInternship" />
				<aspire:AspireDoubleValidator ID="dtvCreditHours" AllowNull="False" MinValue="0" MaxValue="36" runat="server" ControlToValidate="tbCreditHours" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Credit Hours. Valid Range: 0-36." RequiredErrorMessage="This field is required." ValidationGroup="ThesisInternship" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group" runat="server" id="divTitle">
				<aspire:AspireLabel ID="lblTitle" runat="server" Text="Title:" AssociatedControlID="tbTitle" />
				<aspire:AspireTextBox runat="server" ID="tbTitle" ValidationGroup="ThesisInternship" MaxLength="1000" />
				<aspire:AspireRequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="tbTitle" ErrorMessage="This field is required." ValidationGroup="ThesisInternship" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div runat="server" id="divOrganization" class="form-group">
				<aspire:AspireLabel ID="lblOrganization" runat="server" Text="Organization/Company:" AssociatedControlID="tbOrganization" />
				<aspire:AspireTextBox runat="server" ID="tbOrganization" ValidationGroup="ThesisInternship" />
				<aspire:AspireRequiredFieldValidator ID="rfvOrganization" runat="server" ControlToValidate="tbOrganization" ErrorMessage="This field is required." ValidationGroup="ThesisInternship" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div runat="server" id="divFromDate" class="form-group">
				<aspire:AspireLabel ID="lblFromDate" runat="server" Text="From Date:" AssociatedControlID="dtpFromDate" />
				<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpFromDate" ValidationGroup="ThesisInternship" />
				<aspire:AspireDateTimeValidator ID="dtvFromDate" runat="server" ControlToValidate="dtpFromDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid date." InvalidRangeErrorMessage="Invalid range." AllowNull="False" ValidationGroup="ThesisInternship" />
			</div>
		</div>
		<div class="col-md-6">
			<div runat="server" id="divToDate" class="form-group">
				<aspire:AspireLabel ID="lblToDate" runat="server" Text="To Date:" AssociatedControlID="dtpToDate" />
				<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpToDate" ValidationGroup="ThesisInternship" />
				<aspire:AspireDateTimeValidator ID="dtvToDate" runat="server" ControlToValidate="dtpToDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid date." InvalidRangeErrorMessage="Invalid range." AllowNull="False" ValidationGroup="ThesisInternship" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div runat="server" id="divReportSubmissionDate" class="form-group">
				<aspire:AspireLabel ID="lblReportSubmissionDate" runat="server" Text="Report Submission Date:" AssociatedControlID="dtpReportSubmissionDate" />
				<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpReportSubmissionDate" ValidationGroup="ThesisInternship" />
				<aspire:AspireDateTimeValidator ID="dtvReportSubmissionDate" runat="server" ControlToValidate="dtpReportSubmissionDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid date." InvalidRangeErrorMessage="Invalid range." AllowNull="False" ValidationGroup="ThesisInternship" />
			</div>
		</div>
		<div class="col-md-6">
			<div runat="server" id="divDefenceDate" class="form-group">
				<aspire:AspireLabel ID="lblDefenceVivaDate" runat="server" Text="Defence/Viva Date:" AssociatedControlID="dtpDefenceVivaDate" />
				<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpDefenceVivaDate" ValidationGroup="ThesisInternship" />
				<aspire:AspireDateTimeValidator ID="dtvDefenceVivaDate" runat="server" ControlToValidate="dtpDefenceVivaDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid date." InvalidRangeErrorMessage="Invalid range." AllowNull="False" ValidationGroup="ThesisInternship" />
			</div>
		</div>
		<div class="col-md-6">
			<div runat="server" id="divCompletionDate" class="form-group">
				<aspire:AspireLabel ID="lblCompletionDate" runat="server" Text="Completion Date:" AssociatedControlID="dtpCompletionDate" />
				<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpCompletionDate" ValidationGroup="ThesisInternship" />
				<aspire:AspireDateTimeValidator ID="dtvCompletionDate" runat="server" ControlToValidate="dtpCompletionDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid date." InvalidRangeErrorMessage="Invalid range." AllowNull="False" ValidationGroup="ThesisInternship" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<aspire:AspireLabel ID="lblRemarks" runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" />
				<aspire:AspireTextBox MaxLength="1000" runat="server" ID="tbRemarks" TextMode="MultiLine" />
			</div>
		</div>
	</div>
	<asp:Repeater runat="server" ID="repeaterProjectThesisInternshipMarks" ItemType="Aspire.BL.Core.Exams.Staff.ProjectThesisInternships.GetProjectThesisInternshipResult.Marks">
		<HeaderTemplate>
			<table class="table table-bordered table-condensed table-hover table-striped" id="<%= this.repeaterProjectThesisInternshipMarks.ClientID %>">
				<caption>Detailed Marks</caption>
				<thead>
					<tr>
						<th class="text-center">#</th>
						<th class="text-center">Title</th>
						<th class="text-center">Obtained Marks</th>
						<th class="text-center">Total Marks</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
				<td class="text-center"><%#: Container.ItemIndex + 1 %></td>
				<td>
					<aspire:AspireTextBox runat="server" ID="tbMarksTitle" Text="<%# Item.MarksTitle %>" />
					<aspire:Label runat="server" ID="tbMarksTitleError" CssClass="text-danger" />
				</td>
				<td>
					<aspire:AspireTextBox runat="server" ID="tbObtainedMarks" Text="<%# Item.ObtainedMarks %>" ValidationGroup="ThesisInternship" />
					<aspire:Label runat="server" ID="tbObtainedMarksError" CssClass="text-danger" />
				</td>
				<td>
					<aspire:AspireTextBox runat="server" ID="tbTotalMarks" Text="<%# Item.TotalMarks %>" ValidationGroup="ThesisInternship" />
					<aspire:Label runat="server" ID="tbTotalMarksError" CssClass="text-danger" />
				</td>
				<td>
					<a id="btnDeleteMarks" title="Delete" style="cursor: pointer"><i class="glyphicon glyphicon-remove"></i></a>
				</td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</tbody>
			<tfoot>
				<tr>
					<td></td>
					<td></td>
					<td id="obtained"></td>
					<td id="total"></td>
					<td></td>
				</tr>
			</tfoot>
			</table>
		</FooterTemplate>
	</asp:Repeater>
	<asp:Repeater runat="server" ID="repeaterProjectThesisInternshipExaminers" ItemType="Aspire.BL.Core.Exams.Staff.ProjectThesisInternships.GetProjectThesisInternshipResult.Examiner">
		<HeaderTemplate>
			<table class="table table-bordered table-condensed table-hover table-striped" id="<%= this.repeaterProjectThesisInternshipExaminers.ClientID %>">
				<caption>Examiners</caption>
				<thead>
					<tr>
						<th class="text-center">#</th>
						<th class="text-center">Examiner Type</th>
						<th class="text-center">Examiner Name</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
				<td class="text-center"><%#: Container.ItemIndex + 1 %></td>
				<td>
					<aspire:AspireDropDownList DataSource="<%# ExaminerTypes %>" SelectedValue="<%# Item.ExaminerType %>" DataTextField="Text" DataValueField="Value" runat="server" ID="ddlExaminerType" Text="<%# Item.ExaminerType %>" ValidationGroup="ThesisInternship" />
					<aspire:Label runat="server" ID="ddlExaminerTypeError" CssClass="text-danger" />
				</td>
				<td>
					<aspire:AspireTextBox runat="server" ID="tbExaminerName" Text="<%# Item.ExaminerName %>" />
					<aspire:Label runat="server" ID="tbExaminerNameError" CssClass="text-danger" />
				</td>
				<td>
					<a id="btnDeleteExaminer" title="Delete" style="cursor: pointer"><i class="glyphicon glyphicon-remove"></i></a>
				</td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</tbody>
			</table>
		</FooterTemplate>
	</asp:Repeater>
	<aspire:AspireCustomValidator runat="server" ID="cvMarks" ClientValidationFunction="ValidateMarks" OnServerValidate="cvMarks_OnServerValidate" ValidationGroup="ThesisInternship" />
	<aspire:AspireCustomValidator runat="server" ID="cvExaminers" ClientValidationFunction="ValidateExaminers" OnServerValidate="cvExaminers_OnServerValidate" ValidationGroup="ThesisInternship" />
	<div class="form-group text-center">
		<aspire:AspireButton runat="server" ID="btnSaveThesisInternship" Text="Save" ValidationGroup="ThesisInternship" OnClick="btnSaveThesisInternship_OnClick" />
		<aspire:AspireHyperLink runat="server" Text="Cancel" ID="hlCancel" />
	</div>
	<aspire:AspireModal runat="server" ID="modalAttachRegisteredCourse" HeaderText="Attach Registered Course" Visible="False">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelAttachRegisteredCourse" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertAttachRegisteredCourse" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Registered Course:" AssociatedControlID="ddlRegisteredCourseID" />
						<aspire:AspireDropDownList runat="server" ID="ddlRegisteredCourseID" ValidationGroup="RegisteredCourse" />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelAttachRegisteredCourseFooter" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSaveRegisteredCourse" Text="Save" ValidationGroup="RegisteredCourse" OnClick="btnSaveRegisteredCourse_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
	<uc1:ProjectThesisInternshipMarksChanges runat="server" ID="ProjectThesisInternshipMarksChanges" />
	<script type="text/javascript">
		$(function () {
			$("a#btnDeleteMarks", $("#<%=this.repeaterProjectThesisInternshipMarks.ClientID%>")).click(function () {
				var currentRow = $(this).closest("tr");
				while (currentRow.length > 0) {
					var tbMarksTitle = $("input[id*='_tbMarksTitle_']", currentRow).val("");
					var tbObtainedMarks = $("input[id*='_tbObtainedMarks_']", currentRow).val("");
					var tbTotalMarks = $("input[id*='_tbTotalMarks_']", currentRow).val("");
					currentRow = currentRow.next("tr");
					if (currentRow.length > 0) {
						tbMarksTitle.val($("input[id*='_tbMarksTitle_']", currentRow).val());
						tbObtainedMarks.val($("input[id*='_tbObtainedMarks_']", currentRow).val());
						tbTotalMarks.val($("input[id*='_tbTotalMarks_']", currentRow).val());
					}
				}

				ValidatorValidate($("#<%=this.cvMarks.ClientID%>")[0]);
			});
			ValidatorValidate($("#<%=this.cvMarks.ClientID%>")[0]);

			$("a#btnDeleteExaminer", $("#<%=this.repeaterProjectThesisInternshipExaminers.ClientID%>")).click(function () {
				var currentRow = $(this).closest("tr");
				while (currentRow.length > 0) {
					var ddlExaminerType = $("select[id*='_ddlExaminerType_']", currentRow).val("");
					var tbExaminerName = $("input[id*='_tbExaminerName_']", currentRow).val("");
					currentRow = currentRow.next("tr");
					if (currentRow.length > 0) {
						ddlExaminerType.val($("select[id*='_ddlExaminerType_']", currentRow).val());
						tbExaminerName.val($("input[id*='_tbExaminerName_']", currentRow).val());
					}
				}

				ValidatorValidate($("#<%=this.cvExaminers.ClientID%>")[0]);
			});
			ValidatorValidate($("#<%=this.cvExaminers.ClientID%>")[0]);

			$("a.aspNetDisabled.disabled").css({ "cursor": "not-allowed" });
			$("a.aspNetDisabled.disabled[reasons]").click(function () {
				var reasons = $(this).attr("reasons");
				//reasons = "<ul style=\"padding-left:20px\">" + reasons + "</ul>";
				AspireModal.alert("Reasons", reasons, null, true);
			});
		});

		function ValidateMarks(source, args) {
			var table = $("#<%=this.repeaterProjectThesisInternshipMarks.ClientID%>");
			var rows = $("tbody>tr", table).removeClass("danger");
			args.IsValid = true;
			var previousBlankRows;
			rows.each(function (i, r) {
				var row = $(r);
				var tbMarksTitle = $("input[id*='_tbMarksTitle_']", row);
				var tbObtainedMarks = $("input[id*='_tbObtainedMarks_']", row);
				var tbTotalMarks = $("input[id*='_tbTotalMarks_']", row);
				var tbMarksTitleError = $("span[id*='_tbMarksTitleError_']", row).text("").hide();
				var tbObtainedMarksError = $("span[id*='_tbObtainedMarksError_']", row).text("").hide();
				var tbTotalMarksError = $("span[id*='_tbTotalMarksError_']", row).text("").hide();

				var title = tbMarksTitle.val().trim();
				var obtMarks = tbObtainedMarks.val().trim();
				var totMarks = tbTotalMarks.val().trim();
				if (title === "" && obtMarks === "" && totMarks === "") {
					if (i === 0) {
						row.addClass("danger");
						args.IsValid = false;
						return false;
					}
					if (previousBlankRows == null)
						previousBlankRows = row;
					else
						previousBlankRows = $(previousBlankRows).add(row);
					return true;
				}
				else {
					if (previousBlankRows != null && previousBlankRows.length) {
						previousBlankRows.addClass("danger");
						args.IsValid = false;
						return false;
					}
					if (title === "") {
						args.IsValid = false;
						tbMarksTitleError.text("This field is required.").show().prev().focus();
					}
					if (obtMarks == null || obtMarks === "") {
						args.IsValid = false;
						tbObtainedMarksError.text("This field is required.").show().prev().focus();
					} else if (obtMarks.tryToDouble() == null) {
						args.IsValid = false;
						tbObtainedMarksError.text("Invalid Number.").show().prev().focus();
					}
					if (totMarks == null || totMarks === "") {
						args.IsValid = false;
						tbTotalMarksError.text("This field is required.").show().prev().focus();
					} else if (totMarks.tryToDouble() == null) {
						args.IsValid = false;
						tbTotalMarksError.text("Invalid Number.").show().prev().focus();
					}
					if (obtMarks != null &&
						totMarks != null &&
						totMarks.tryToDouble() != null &&
						obtMarks.tryToDouble() != null &&
						totMarks.tryToDouble() < obtMarks.tryToDouble()) {
						args.IsValid = false;
						tbTotalMarksError.text("Obtained Marks must be less than or equal to Total Marks.").show().prev().focus();
					}
				}
				return args.IsValid;
			});

			var obtained = $("tfoot td#obtained", table);
			var total = $("tfoot td#total", table);
			if (args.IsValid) {
				var obt = 0;
				var tot = 0;
				$("input[id*='_tbObtainedMarks_']", rows).each(function (i, tb) {
					var marks = $(tb).val().toNullableDouble();
					if (marks != null)
						obt += marks;
				});
				$("input[id*='_tbTotalMarks_']", rows).each(function (i, tb) {
					var marks = $(tb).val().toNullableDouble();
					if (marks != null)
						tot += marks;
				});

				obtained.text(obt.toString());
				total.text(tot.toString());
			}
		}
		function ValidateExaminers(source, args) {
			var table = $("#<%=this.repeaterProjectThesisInternshipExaminers.ClientID%>");
			var rows = $("tbody>tr", table).removeClass("danger");
			args.IsValid = true;
			var previousBlankRows;
			rows.each(function (i, r) {
				var row = $(r);
				var ddlExaminerType = $("select[id*='_ddlExaminerType_']", row);
				var tbExaminerName = $("input[id*='_tbExaminerName_']", row);
				var ddlExaminerTypeError = $("span[id*='_ddlExaminerTypeError_']", row).text("").hide();
				var tbExaminerNameError = $("span[id*='_tbExaminerNameError_']", row).text("").hide();

				var examinerType = ddlExaminerType.val().trim();
				var examinerName = tbExaminerName.val().trim();
				if (examinerType === "" && examinerName === "") {
					if (i === 0) {
						row.addClass("danger");
						args.IsValid = false;
						return false;
					}
					if (previousBlankRows == null)
						previousBlankRows = row;
					else
						previousBlankRows = $(previousBlankRows).add(row);
					return true;
				}
				else {
					if (previousBlankRows != null && previousBlankRows.length) {
						previousBlankRows.addClass("danger");
						args.IsValid = false;
						return false;
					}
					if (examinerType === null || examinerType === "") {
						args.IsValid = false;
						ddlExaminerTypeError.text("This field is required.").show().prev().focus();
					}
					if (examinerName == null || examinerName === "") {
						args.IsValid = false;
						tbExaminerNameError.text("This field is required.").show().prev().focus();
					}
				}
				return args.IsValid;
			});
		}
	</script>
</asp:Content>
