﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CustomExamMarks.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Exams.CustomExamMarks" %>

<%@ Import Namespace="Aspire.Lib.Helpers" %>

<%@ Register TagPrefix="uc" TagName="studentinfo" Src="~/Sys/Staff/Common/StudentInfoUserControl.ascx" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<script src="CustomExamMarks.js"></script>
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
					<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="RequiredEnrollment" OnSearch="tbEnrollment_OnSearch" />
				</div>
				<div class="form-group">
					<aspire:AspireHyperLink runat="server" Text="Clear" NavigateUrl="ThesisInternships.aspx" Target="_blank" />
				</div>
			</div>
		</div>
	</div>
	<asp:Panel runat="server" ID="mainPanel">
		<uc:studentinfo runat="server" ID="ucStudentInfo" />
		<aspire:AspireGridView runat="server" ID="gvRegisteredCourses" AutoGenerateColumns="False" Caption="Registered Courses">
			<Columns>
				<asp:BoundField HeaderText="Semester" DataField="OfferedSemester" />
				<asp:BoundField HeaderText="Code" DataField="RoadmapCourseCode" />
				<asp:BoundField HeaderText="Roadmap Title" DataField="RoadmapTitle" />
				<asp:BoundField HeaderText="Credit Hours" DataField="RoadmapCreditHoursFullName" />
				<asp:BoundField HeaderText="Offered Title" DataField="OfferedTitle" />
				<asp:BoundField HeaderText="Class" DataField="OfferedClass" />
				<asp:BoundField HeaderText="Faculty Member" DataField="FacultyMemberName" />
				<asp:BoundField HeaderText="Status" DataField="FullStatus" />
				<asp:TemplateField HeaderText="Actions">
					<ItemTemplate>
						<aspire:AspireHyperLinkButton runat="server" ButtonType="OnlyIcon" Glyphicon="edit" ID="btnEdit" onclick="editMarks(this);" data-EncryptedID='<%# Eval("RegisteredCourseID").ToString().Encrypt() %>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
	</asp:Panel>
</asp:Content>
