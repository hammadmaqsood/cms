﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ExamResultStudentWise.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Exams.ExamResultStudentWise" %>

<%@ Register TagPrefix="ucer" TagName="ExamResultStudentWise" Src="~/Sys/Common/Exam/ExamResultStudentWise.ascx" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<ucer:ExamResultStudentWise runat="server" ID="ucExamResultStudentWise" />
</asp:Content>
