﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ExamRemarksPolicy.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Exams.ExamRemarksPolicy" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-group">
		<aspire:AspireButton runat="server" ButtonType="Success" Glyphicon="check" Text="Assign/Un-assign Exam Remarks Policy" CausesValidation="False" ID="btnAssignUnAssignExamMarksPolicy" OnClick="btnAssignUnAssignExamRemarksPolicy_OnClick" />
	</div>
	<div class="row">
		<div class="col-md-3">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-3">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" OnSelectedIndexChanged="ddlProgramIDFilter_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
		<div class="col-md-3">
			<aspire:AspireLabel runat="server" Text="Intake Semester:" AssociatedControlID="ddlIntakeSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlIntakeSemesterIDFilter" OnSelectedIndexChanged="ddlIntakeSemesterIDFilter_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
	</div>
	<p></p>
	<asp:Repeater runat="server" ID="repeaterProgram" ItemType="Aspire.BL.Core.Exams.Staff.ExamRemarksPolicy.GetExamRemarksPoliciesForCrossTabGridResult.CrossTabRow">
		<HeaderTemplate>
			<div class="table-responsive">
				<table class="table table-bordered table-hover table-condensed">
					<thead>
						<tr>
							<th>#</th>
							<th>Program</th>
							<asp:Repeater runat="server" ItemType="System.Int16" DataSource="<%# this.Result.SemesterIDs.OrderByDescending(s=> s) %>">
								<ItemTemplate>
									<th><%# Item.ToSemesterString() %></th>
								</ItemTemplate>
							</asp:Repeater>
						</tr>
					</thead>
					<tbody>
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
				<td><%# Container.ItemIndex + 1 %></td>
				<td><%# Item.ProgramAlias %></td>
				<asp:Repeater runat="server" ID="repeaterSemesters" DataSource="<%# Item.Policies.OrderByDescending(p=> p.IntakeSemesterID) %>" ItemType="Aspire.BL.Core.Exams.Staff.ExamRemarksPolicy.GetExamRemarksPoliciesForCrossTabGridResult.CrossTabRow.Policy" OnItemCommand="repeaterSemesters_OnItemCommand">
					<ItemTemplate>
						<td runat="server" visible="<%# Item.AdmissionOpenProgramID == null %>"></td>
						<td title='<%# $"Students: {Item.Students}" %>' runat="server" visible="<%# Item.AdmissionOpenProgramID != null %>" class='<%# Item.Valid ? "success" : "danger" %>'>
							<aspire:AspireLinkButton runat="server" CommandName="Assign" EncryptedCommandArgument='<%# $"{Item.Program.ProgramID}:{Item.IntakeSemesterID}:{Item.ExamRemarksPolicy?.ExamRemarksPolicyID}" %>' Text='<%# Item.ExamRemarksPolicy?.ExamRemarksPolicyName ?? "N/A" %>' />
						</td>
					</ItemTemplate>
				</asp:Repeater>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</tbody>
			</table>
			</div>
		</FooterTemplate>
	</asp:Repeater>
	<aspire:AspireModal runat="server" ID="modalExamRemarkPolicy" HeaderText="Assign/Un-assign Exam Remarks Policy">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelAssignPolicy" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertAssignPolicy" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="lbProgramIDs" />
						<aspire:AspireListBox runat="server" ID="lbProgramIDs" SelectionMode="Multiple" CausesValidation="False" ValidationGroup="Assign" />
					</div>
					<div class="row">
						<div class="col-md-6 form-group">
							<aspire:AspireLabel runat="server" Text="Intake Semester (From):" AssociatedControlID="ddlIntakeSemesterFrom" />
							<aspire:AspireDropDownList runat="server" ID="ddlIntakeSemesterFrom" ValidationGroup="Assign" OnSelectedIndexChanged="ddlIntakeSemesterFrom_OnSelectedIndexChanged" AutoPostBack="True" CausesValidation="False" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlIntakeSemesterFrom" ErrorMessage="This field is required." ValidationGroup="Assign" />
						</div>
						<div class="col-md-6 form-group">
							<aspire:AspireLabel runat="server" Text="Intake Semester (To):" AssociatedControlID="ddlIntakeSemesterTo" />
							<aspire:AspireDropDownList runat="server" ID="ddlIntakeSemesterTo" ValidationGroup="Assign" OnSelectedIndexChanged="ddlIntakeSemesterTo_OnSelectedIndexChanged" AutoPostBack="True" CausesValidation="False" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlIntakeSemesterTo" ErrorMessage="This field is required." ValidationGroup="Assign" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Exam Remarks Policy:" AssociatedControlID="ddlExamRemarksPolicyID" />
						<aspire:AspireDropDownList runat="server" ID="ddlExamRemarksPolicyID" ValidationGroup="Assign" />
						<span class="help-block">Select None to un-assign exam marks policy.</span>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Always">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="Assign" Text="Save" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
	<script type="text/javascript">
		$(function () {
			var init = function () {
				$("#<%=this.lbProgramIDs.ClientID%>").select2({
					theme: "bootstrap",
					width: "style",
					closeOnSelect: false,
					dropdownParent: $('#<%=this.modalExamRemarkPolicy.ClientID%>')
				});
				$("#<%=this.ddlExamRemarksPolicyID.ClientID%>").select2({
					theme: "bootstrap",
					width: "style",
					dropdownParent: $('#<%=this.modalExamRemarkPolicy.ClientID%>'),
					templateResult: function (result) {
						var item = $("<div/>");//.css({ border: "1px solid #ccc" });
						$("<strong/>").text(result.text).appendTo($("<div/>").appendTo(item));
						$("<strong/>").html($(result.element).attr("Summary")).appendTo($("<em/>").appendTo($("<div/>").appendTo(item)));
						$("<em/>").text($(result.element).attr("Description")).appendTo($("<div/>").appendTo(item));
						return item;
					}
				});
			};
			init();
			Sys.Application.add_load(init);
		});
	</script>
</asp:Content>
