﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Exams
{
	[AspirePage("6D893196-E08B-4C13-ABD6-E72200664817", UserTypes.Staff, "~/Sys/Staff/Exams/ProjectThesisInternshipMarks.aspx", "Project/Thesis/Internship Marks", true, AspireModules.Exams)]
	public partial class ProjectThesisInternshipMarks : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => this.ProjectThesisInternshipID == null ? "Add Project/Thesis/Internship Marks" : "Edit Project/Thesis/Internship Marks";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Exams.Module, UserGroupPermission.Allowed},
		};

		private int? StudentID => this.Request.GetParameterValue<int>(nameof(this.StudentID));
		private Guid? ProjectThesisInternshipID => this.Request.GetParameterValue<Guid>(nameof(this.ProjectThesisInternshipID));

		public static string GetPageUrl(int studentID, Guid? projectThesisInternshipID)
		{
			return GetAspirePageAttribute<ProjectThesisInternshipMarks>().PageUrl.AttachQueryParams(nameof(StudentID), studentID, nameof(ProjectThesisInternshipID), projectThesisInternshipID);
		}

		public static void Redirect(int studentID, Guid? projectThesisInternshipID)
		{
			Redirect(GetPageUrl(studentID, projectThesisInternshipID));
		}

		private void RefreshPage()
		{
			Redirect(GetPageUrl(this.StudentID ?? throw new InvalidOperationException(), this.ProjectThesisInternshipID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.StudentID == null)
				{
					Redirect<ProjectThesisInternships>();
					return;
				}

				this.ddlRecordType.FillGuidEnums<ProjectThesisInternship.RecordTypes>().SetSelectedGuidEnum(ProjectThesisInternship.RecordTypes.Project);
				this.ddlSemesterID.FillSemesters();
				this.hlCancel.NavigateUrl = ProjectThesisInternships.GetPageUrl(this.StudentID, true);

				if (this.ProjectThesisInternshipID != null)
				{
					var projectThesisInternship = BL.Core.Exams.Staff.ProjectThesisInternships.GetProjectThesisInternship(this.StudentID.Value, this.ProjectThesisInternshipID.Value, this.StaffIdentity.LoginSessionGuid);
					if (projectThesisInternship == null)
					{
						ProjectThesisInternships.Redirect(this.StudentID, false);
						return;
					}
					this.lblEnrollment.Text = projectThesisInternship.Enrollment;
					this.lblRegistrationNo.Text = (projectThesisInternship.RegistrationNo?.ToString()).ToNAIfNullOrEmpty();
					this.lblName.Text = projectThesisInternship.Name;
					this.lblFatherName.Text = projectThesisInternship.FatherName;
					this.lblProgramAlias.Text = projectThesisInternship.ProgramAlias;
					this.lblIntakeSemester.Text = projectThesisInternship.IntakeSemesterID.ToSemesterString();
					this.lblStatus.Text = (projectThesisInternship.StatusEnum?.ToFullName()).ToNAIfNullOrEmpty();
					this.lblStatusDate.Text = (projectThesisInternship.StatusDate.ToString()).ToNAIfNullOrEmpty();

					this.lblMarksEntryLocked.Text = projectThesisInternship.MarksEntryLocked.ToYesNo();
					this.lblSubmittedToHoD.Text = projectThesisInternship.SubmittedToHODDate?.ToString() ?? false.ToYesNo();
					this.lblSubmittedToCampus.Text = projectThesisInternship.SubmittedToCampusDate?.ToString() ?? false.ToYesNo();
					this.lblSubmittedToBUHOExams.Text = projectThesisInternship.SubmittedToUniversityDate?.ToString() ?? false.ToYesNo();

					this.lblCourseCode.Text = projectThesisInternship.CourseCode.ToNAIfNullOrEmpty();
					this.lblCourseTitle.Text = projectThesisInternship.CourseTitle.ToNAIfNullOrEmpty();
					this.lblCourseCreditHours.Text = (projectThesisInternship.CourseCreditHours?.ToCreditHoursFullName()).ToNAIfNullOrEmpty();

					this.ddlRecordType.SetSelectedGuidEnum(projectThesisInternship.RecordType.GetEnum<ProjectThesisInternship.RecordTypes>());
					this.ddlRecordType.Enabled = false;
					this.ddlRecordType_OnSelectedIndexChanged(null, null);
					this.ddlSemesterID.Enabled = projectThesisInternship.RegisteredCourseID == null;
					this.ddlSemesterID.SelectedValue = projectThesisInternship.SemesterID.ToString();
					this.tbCreditHours.Text = projectThesisInternship.CreditHours.ToCreditHoursFullName();
					this.tbCreditHours.ReadOnly = projectThesisInternship.RegisteredCourseID != null;
					this.tbTitle.Text = projectThesisInternship.Title;
					this.tbOrganization.Text = projectThesisInternship.Organization;
					this.dtpFromDate.SelectedDate = projectThesisInternship.FromDate;
					this.dtpToDate.SelectedDate = projectThesisInternship.ToDate;
					this.dtpReportSubmissionDate.SelectedDate = projectThesisInternship.ReportSubmissionDate;
					this.dtpDefenceVivaDate.SelectedDate = projectThesisInternship.DefenceVivaDate;
					this.dtpCompletionDate.SelectedDate = projectThesisInternship.CompletionDate;
					this.tbRemarks.Text = projectThesisInternship.Remarks;

					this.repeaterProjectThesisInternshipMarks.DataBind(projectThesisInternship.ProjectThesisInternshipMarks);
					this.repeaterProjectThesisInternshipExaminers.DataBind(projectThesisInternship.ProjectThesisInternshipExaminers.OrderByDescending(ptie => ptie.ExaminerType).ThenBy(ptie => ptie.ExaminerName));

					this.btnSaveThesisInternship.Visible = !projectThesisInternship.MarksEntryLocked
						&& !(projectThesisInternship.SubmittedToHODDate != null && projectThesisInternship.SubmittedToCampusDate == null && projectThesisInternship.SubmittedToUniversityDate == null);

					this.btnAttachRegisteredCourse.Enabled = !projectThesisInternship.MarksEntryLocked;
					this.btnLockMarksEntry.Visible = !projectThesisInternship.MarksEntryLocked;
					this.btnUnlockMarksEntry.Visible = projectThesisInternship.MarksEntryLocked;
					if (projectThesisInternship.SubmittedToUniversityDate != null)
					{
						this.lbtnSubmitToBUHO.Enabled = false;
						this.lbtnSubmitToBUHO.Attributes.Add("reasons", "Marks are already submitted to BUHO Exams.");
					}
					else
					{
						if (projectThesisInternship.SubmittedToCampusDate != null)
						{
							if (projectThesisInternship.MarksEntryLocked)
								this.lbtnSubmitToBUHO.Enabled = true;
							else
							{
								this.lbtnSubmitToBUHO.Enabled = false;
								this.lbtnSubmitToBUHO.Attributes.Add("reasons", "Marks Entry is not locked.");
							}
						}
						else
						{
							this.lbtnSubmitToBUHO.Enabled = false;
							this.lbtnSubmitToBUHO.Attributes.Add("reasons", "Marks are not submitted to Campus.");
						}
					}

					if (projectThesisInternship.SubmittedToCampusDate != null)
					{
						this.lbtnSubmitToCampus.Enabled = false;
						this.lbtnSubmitToCampus.Attributes.Add("reasons", "Marks are already submitted to Campus.");
					}
					else
					{
						if (projectThesisInternship.SubmittedToUniversityDate != null)
						{
							this.lbtnSubmitToCampus.Enabled = false;
							this.lbtnSubmitToCampus.Attributes.Add("reasons", "Marks are already submitted to BUHO Exams.");
						}
						else
						{
							if (projectThesisInternship.MarksEntryLocked)
								this.lbtnSubmitToCampus.Enabled = true;
							else
							{
								this.lbtnSubmitToCampus.Enabled = false;
								this.lbtnSubmitToCampus.Attributes.Add("reasons", "Marks Entry is not locked.");
							}
						}
					}

					if (projectThesisInternship.SubmittedToHODDate != null)
					{
						this.lbtnSubmitToHOD.Enabled = false;
						this.lbtnSubmitToHOD.Attributes.Add("reasons", "Marks are already submitted to HoD.");
					}
					else
					{
						if (projectThesisInternship.SubmittedToUniversityDate != null)
						{
							this.lbtnSubmitToHOD.Enabled = false;
							this.lbtnSubmitToHOD.Attributes.Add("reasons", "Marks are already submitted to BUHO Exams.");
						}
						else
						{
							if (projectThesisInternship.SubmittedToCampusDate != null)
							{
								this.lbtnSubmitToHOD.Enabled = false;
								this.lbtnSubmitToHOD.Attributes.Add("reasons", "Marks are already submitted to Campus.");
							}
							else
							{
								if (projectThesisInternship.MarksEntryLocked)
									this.lbtnSubmitToHOD.Enabled = true;
								else
								{
									this.lbtnSubmitToHOD.Enabled = false;
									this.lbtnSubmitToHOD.Attributes.Add("reasons", "Marks Entry is not locked.");
								}
							}
						}
					}
				}
				else
				{
					var student = BL.Core.Exams.Staff.ProjectThesisInternships.GetStudentInfo(this.StudentID.Value, this.StaffIdentity.LoginSessionGuid);
					if (student == null)
					{
						ProjectThesisInternships.Redirect(this.StudentID.Value, false);
						return;
					}

					this.lblEnrollment.Text = student.Enrollment;
					this.lblRegistrationNo.Text = (student.RegistrationNo?.ToString()).ToNAIfNullOrEmpty();
					this.lblName.Text = student.Name;
					this.lblFatherName.Text = student.FatherName;
					this.lblProgramAlias.Text = student.ProgramAlias;
					this.lblIntakeSemester.Text = student.IntakeSemesterID.ToSemesterString();
					this.lblStatus.Text = (student.StatusEnum?.ToFullName()).ToNAIfNullOrEmpty();
					this.lblStatusDate.Text = (student.StatusDate.ToString()).ToNAIfNullOrEmpty();

					this.lblMarksEntryLocked.Text = false.ToYesNo();
					this.lblSubmittedToHoD.Text = false.ToYesNo();
					this.lblSubmittedToCampus.Text = false.ToYesNo();
					this.lblSubmittedToBUHOExams.Text = false.ToYesNo();

					this.lblCourseCode.Text = string.Empty.ToNAIfNullOrEmpty();
					this.lblCourseTitle.Text = string.Empty.ToNAIfNullOrEmpty();
					this.lblCourseCreditHours.Text = string.Empty.ToNAIfNullOrEmpty();

					this.ddlRecordType.SelectedIndex = 0;
					this.ddlRecordType.Enabled = true;
					this.ddlRecordType_OnSelectedIndexChanged(null, null);
					this.ddlSemesterID.ClearSelection();
					this.tbCreditHours.Text = null;
					this.tbTitle.Text = null;
					this.tbOrganization.Text = null;
					this.dtpFromDate.SelectedDate = null;
					this.dtpToDate.SelectedDate = null;
					this.dtpReportSubmissionDate.SelectedDate = null;
					this.dtpDefenceVivaDate.SelectedDate = null;
					this.dtpCompletionDate.SelectedDate = null;
					this.tbRemarks.Text = null;
					this.repeaterProjectThesisInternshipMarks.DataBind(student.ProjectThesisInternshipMarks);
					this.repeaterProjectThesisInternshipExaminers.DataBind(student.ProjectThesisInternshipExaminers);
					this.btnSaveThesisInternship.Enabled = true;

					this.btnLockMarksEntry.Visible = false;
					this.btnUnlockMarksEntry.Visible = false;
					this.divSubmit.Visible = false;
					this.lbtnSubmitToHOD.Visible = false;
					this.lbtnSubmitToCampus.Visible = false;
					this.lbtnSubmitToBUHO.Visible = false;
					this.btnChangeHistory.Visible = false;

					this.btnAttachRegisteredCourse.Enabled = false;
				}
			}
		}

		protected static IEnumerable<ListItem> ExaminerTypes
		{
			get
			{
				var items = WebControlsExtensions.GetEnumGuidValuesListItems<ProjectThesisInternshipExaminer.ExaminerTypes>().ToList();
				items.Insert(0, CommonListItems.Select);
				return items;
			}
		}

		protected void ddlRecordType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var recordTypeEnum = this.ddlRecordType.GetSelectedGuidEnum<ProjectThesisInternship.RecordTypes>();
			switch (recordTypeEnum)
			{
				case ProjectThesisInternship.RecordTypes.Thesis:
					this.divTitle.Visible = true;
					this.lblTitle.Text = "Title of Thesis:";
					this.rfvTitle.Enabled = true;

					this.divOrganization.Visible = this.rfvOrganization.Enabled = false;
					this.divFromDate.Visible = this.dtvFromDate.Enabled = false;
					this.divToDate.Visible = this.dtvToDate.Enabled = false;

					this.divReportSubmissionDate.Visible = true;
					this.lblReportSubmissionDate.Text = "Date of Submission:";
					this.dtvReportSubmissionDate.Enabled = true;
					this.dtvReportSubmissionDate.AllowNull = false;

					this.divDefenceDate.Visible = true;
					this.lblDefenceVivaDate.Text = "Date of Viva:";
					this.dtvDefenceVivaDate.Enabled = true;
					this.dtvDefenceVivaDate.AllowNull = false;

					this.divCompletionDate.Visible = true;
					this.lblCompletionDate.Text = "Thesis Completion Date:";
					this.dtvCompletionDate.Enabled = true;
					this.dtvCompletionDate.AllowNull = false;
					break;
				case ProjectThesisInternship.RecordTypes.Project:
					this.divTitle.Visible = true;
					this.lblTitle.Text = "Project Title:";
					this.rfvTitle.Enabled = true;

					this.divOrganization.Visible = true;
					this.rfvOrganization.Enabled = true;
					this.lblOrganization.Text = "Name of Company:";

					this.divFromDate.Visible = this.dtvFromDate.Enabled = false;
					this.divToDate.Visible = this.dtvToDate.Enabled = false;

					this.divReportSubmissionDate.Visible = true;
					this.lblReportSubmissionDate.Text = "Date of Submission:";
					this.dtvReportSubmissionDate.Enabled = true;
					this.dtvReportSubmissionDate.AllowNull = false;

					this.divDefenceDate.Visible = true;
					this.lblDefenceVivaDate.Text = "Viva Date:";
					this.dtvDefenceVivaDate.Enabled = true;
					this.dtvDefenceVivaDate.AllowNull = false;

					this.divCompletionDate.Visible = true;
					this.lblCompletionDate.Text = "Project Completion Date:";
					this.dtvCompletionDate.Enabled = true;
					this.dtvCompletionDate.AllowNull = false;
					break;
				case ProjectThesisInternship.RecordTypes.Internship:
					this.divTitle.Visible = false;
					this.lblTitle.Text = "Project Title:";
					this.rfvTitle.Enabled = true;

					this.divOrganization.Visible = true;
					this.rfvOrganization.Enabled = true;
					this.lblOrganization.Text = "Company Name:";

					this.divFromDate.Visible = true;
					this.dtvFromDate.Enabled = true;
					this.dtvFromDate.AllowNull = false;

					this.divToDate.Visible = true;
					this.dtvToDate.Enabled = true;
					this.dtvToDate.AllowNull = false;

					this.divReportSubmissionDate.Visible = true;
					this.lblReportSubmissionDate.Text = "Date of Report Submission:";
					this.dtvReportSubmissionDate.Enabled = true;
					this.dtvReportSubmissionDate.AllowNull = false;

					this.divDefenceDate.Visible = true;
					this.lblDefenceVivaDate.Text = "Internship Completion Date (Date of Viva):";
					this.dtvDefenceVivaDate.Enabled = true;
					this.dtvDefenceVivaDate.AllowNull = false;

					this.divCompletionDate.Visible = this.dtvCompletionDate.Enabled = false;
					break;
				default:
					throw new NotImplementedEnumException(recordTypeEnum);
			}
		}

		protected void btnSaveThesisInternship_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
			{
				this.AddErrorAlert("Data is not valid.");
				return;
			}

			var studentID = this.StudentID ?? throw new InvalidOperationException();
			var recordTypeEnum = this.ddlRecordType.GetSelectedGuidEnum<ProjectThesisInternship.RecordTypes>();
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var creditHours = this.tbCreditHours.Text.ToDecimal();
			var title = this.tbTitle.Text;
			var organization = this.tbOrganization.Text;
			var fromDate = this.dtpFromDate.SelectedDate;
			var toDate = this.dtpToDate.SelectedDate;
			var reportSubmissionDate = this.dtpReportSubmissionDate.SelectedDate;
			var defenceVivaDate = this.dtpDefenceVivaDate.SelectedDate;
			var completionDate = this.dtpCompletionDate.SelectedDate;
			var remarks = this.tbRemarks.Text;
			byte displayIndex = 0;
			var projectThesisInternshipMarks = this.repeaterProjectThesisInternshipMarks.Items.Cast<RepeaterItem>()
				.Where(i => i.ItemType == ListItemType.Item || i.ItemType == ListItemType.AlternatingItem)
				.Select(i => new
				{
					MarksTitle = ((AspireTextBox)i.FindControl("tbMarksTitle")).Text,
					ObtainedMarks = ((AspireTextBox)i.FindControl("tbObtainedMarks")).Text,
					TotalMarks = ((AspireTextBox)i.FindControl("tbTotalMarks")).Text,
				})
				.TakeWhile(m => !string.IsNullOrWhiteSpace(m.MarksTitle) && !string.IsNullOrWhiteSpace(m.ObtainedMarks) && !string.IsNullOrWhiteSpace(m.TotalMarks))
				.Select(t => new ProjectThesisInternshipMark
				{
					MarksTitle = t.MarksTitle,
					DisplayIndex = displayIndex++,
					ObtainedMarks = t.ObtainedMarks.ToDecimal(),
					TotalMarks = t.TotalMarks.ToDecimal()
				})
				.ToList();
			var projectThesisInternshipExaminers = this.repeaterProjectThesisInternshipExaminers.Items.Cast<RepeaterItem>()
				.Where(i => i.ItemType == ListItemType.Item || i.ItemType == ListItemType.AlternatingItem)
				.Select(i => new
				{
					ExaminerTypeEnum = ((AspireDropDownList)i.FindControl("ddlExaminerType")).GetSelectedNullableGuidEnum<ProjectThesisInternshipExaminer.ExaminerTypes>(),
					ExaminerName = ((AspireTextBox)i.FindControl("tbExaminerName")).Text
				})
				.TakeWhile(x => x.ExaminerTypeEnum != null && !string.IsNullOrWhiteSpace(x.ExaminerName))
				.Select(x => new ProjectThesisInternshipExaminer
				{
					ExaminerName = x.ExaminerName,
					ExaminerTypesEnum = x.ExaminerTypeEnum ?? throw new InvalidOperationException()
				})
				.ToList();
			if (this.ProjectThesisInternshipID == null)
			{
				var (status, projectThesisInternshipID) = BL.Core.Exams.Staff.ProjectThesisInternships.AddProjectThesisInternship(studentID, recordTypeEnum, semesterID, creditHours, title, organization, fromDate, toDate, reportSubmissionDate, defenceVivaDate, completionDate, remarks, projectThesisInternshipMarks, projectThesisInternshipExaminers, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.Exams.Staff.ProjectThesisInternships.AddProjectThesisInternshipStatuses.NoRecordFound:
						Redirect(this.StudentID.Value, null);
						return;
					case BL.Core.Exams.Staff.ProjectThesisInternships.AddProjectThesisInternshipStatuses.Success:
						this.AddSuccessMessageHasBeenAdded(recordTypeEnum.GetFullName());
						Redirect(studentID, projectThesisInternshipID);
						return;
					case BL.Core.Exams.Staff.ProjectThesisInternships.AddProjectThesisInternshipStatuses.ExamMarksPolicyNotFound:
						this.AddErrorAlert($"Exam Marks Policy not found for semester {semesterID.ToSemesterString()}.");
						return;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
			else
			{
				var status = BL.Core.Exams.Staff.ProjectThesisInternships.UpdateProjectThesisInternship(this.ProjectThesisInternshipID.Value, semesterID, creditHours, title, organization, fromDate, toDate, reportSubmissionDate, defenceVivaDate, completionDate, remarks, projectThesisInternshipMarks, projectThesisInternshipExaminers, this.StaffIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.Exams.Staff.ProjectThesisInternships.UpdateProjectThesisInternshipStatuses.NoRecordFound:
						this.RefreshPage();
						return;
					case BL.Core.Exams.Staff.ProjectThesisInternships.UpdateProjectThesisInternshipStatuses.ExamMarksPolicyNotFound:
						this.AddErrorAlert($"Exam Marks Policy not found for semester {semesterID.ToSemesterString()}.");
						return;
					case BL.Core.Exams.Staff.ProjectThesisInternships.UpdateProjectThesisInternshipStatuses.MarksCanNotBeEditWhileSubmittedToHOD:
						this.AddErrorAlert($"Marks cannot be edited while submitted to HoD.");
						this.RefreshPage();
						return;
					case BL.Core.Exams.Staff.ProjectThesisInternships.UpdateProjectThesisInternshipStatuses.MarksEntryIsLocked:
						this.AddErrorAlert($"Marks Entry is locked.");
						this.RefreshPage();
						return;
					case BL.Core.Exams.Staff.ProjectThesisInternships.UpdateProjectThesisInternshipStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated(recordTypeEnum.GetFullName());
						Redirect(studentID, this.ProjectThesisInternshipID);
						return;
					case BL.Core.Exams.Staff.ProjectThesisInternships.UpdateProjectThesisInternshipStatuses.SemesterCannotBeChangedWhenAttachedToRegisteredCourse:
						this.AddErrorAlert("Semester cannot be changed because registered course has been attached.");
						this.RefreshPage();
						return;
					case BL.Core.Exams.Staff.ProjectThesisInternships.UpdateProjectThesisInternshipStatuses.CreditHoursCannotBeChangedWhenAttachedToRegisteredCourse:
						this.AddErrorAlert("Credit Hours cannot be changed because registered course has been attached.");
						this.RefreshPage();
						return;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
		}

		protected void cvMarks_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			var marks = this.repeaterProjectThesisInternshipMarks.Items.Cast<RepeaterItem>()
				.Where(i => i.ItemType == ListItemType.Item || i.ItemType == ListItemType.AlternatingItem)
				.Select(i => new
				{
					Title = ((AspireTextBox)i.FindControl("tbMarksTitle")).Text,
					ObtainedMarks = ((AspireTextBox)i.FindControl("tbObtainedMarks")).Text,
					TotalMarks = ((AspireTextBox)i.FindControl("tbTotalMarks")).Text,
				})
				.Reverse()
				.SkipWhile(i => string.IsNullOrWhiteSpace(i.Title) && string.IsNullOrWhiteSpace(i.ObtainedMarks) && string.IsNullOrWhiteSpace(i.TotalMarks))
				.ToList();
			args.IsValid = marks.Any() && marks.All(i => !string.IsNullOrWhiteSpace(i.Title) && i.ObtainedMarks.TryToDecimal() != null && i.TotalMarks.TryToDecimal() != null);
		}

		protected void cvExaminers_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			var marks = this.repeaterProjectThesisInternshipExaminers.Items.Cast<RepeaterItem>()
				.Where(i => i.ItemType == ListItemType.Item || i.ItemType == ListItemType.AlternatingItem)
				.Select(i => new
				{
					ExaminerTypeEnum = ((AspireDropDownList)i.FindControl("ddlExaminerType")).GetSelectedNullableGuidEnum<ProjectThesisInternshipExaminer.ExaminerTypes>(),
					ExaminerName = ((AspireTextBox)i.FindControl("tbExaminerName")).Text
				})
				.Reverse()
				.SkipWhile(i => i.ExaminerTypeEnum == null && string.IsNullOrWhiteSpace(i.ExaminerName))
				.ToList();
			args.IsValid = marks.Any() && marks.All(i => i.ExaminerTypeEnum != null && !string.IsNullOrWhiteSpace(i.ExaminerName));
		}

		protected void btnLockMarksEntry_OnClick(object sender, EventArgs e)
		{
			var status = BL.Core.Exams.Staff.ProjectThesisInternships.LockMarksEntry(this.ProjectThesisInternshipID ?? throw new InvalidOperationException(), this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Exams.Staff.ProjectThesisInternships.LockMarksEntryStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.LockMarksEntryStatuses.MarksEntryIsAlreadyLocked:
					this.AddErrorAlert("Marks Entry is already locked.");
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.LockMarksEntryStatuses.Success:
					this.AddSuccessAlert("Marks Entry has been locked.");
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.LockMarksEntryStatuses.RegisteredCourseIsNotAttached:
					this.AddErrorAlert("Registered Course is not attached.");
					this.RefreshPage();
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		protected void btnUnlockMarksEntry_OnClick(object sender, EventArgs e)
		{
			var status = BL.Core.Exams.Staff.ProjectThesisInternships.UnlockMarksEntry(this.ProjectThesisInternshipID ?? throw new InvalidOperationException(), this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Exams.Staff.ProjectThesisInternships.UnlockMarksEntryStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.UnlockMarksEntryStatuses.CannotUnlockBecauseMarksSubmittedToHOD:
					this.AddErrorAlert("Marks Entry cannot be unlock because Marks have been submitted to HoD.");
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.UnlockMarksEntryStatuses.Success:
					this.AddSuccessAlert("Marks Entry has been unlocked.");
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.UnlockMarksEntryStatuses.MarksEntryIsAlreadyUnlocked:
					this.AddErrorAlert("Marks Entry is already unlocked.");
					this.RefreshPage();
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		protected void lbtnSubmitToHOD_OnClick(object sender, EventArgs e)
		{
			var status = BL.Core.Exams.Staff.ProjectThesisInternships.SubmitToHOD(this.ProjectThesisInternshipID ?? throw new InvalidOperationException(), this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Exams.Staff.ProjectThesisInternships.SubmitToHODStatuses.Success:
					this.AddSuccessAlert("Marks have been submitted to HoD.");
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.SubmitToHODStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.SubmitToHODStatuses.MarksAreAlreadySubmittedToUniversity:
					this.AddSuccessAlert("Marks have been submitted to BUHO Exams.");
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.SubmitToHODStatuses.MarksAreAlreadySubmittedToCampus:
					this.AddSuccessAlert("Marks have been submitted to Campus.");
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.SubmitToHODStatuses.MarksAreAlreadySubmittedToHOD:
					this.AddSuccessAlert("Marks have been submitted to HoD.");
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.SubmitToHODStatuses.MarksEntryIsNotLocked:
					this.AddErrorAlert("Marks Entry is not locked.");
					this.RefreshPage();
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		protected void lbtnSubmitToCampus_OnClick(object sender, EventArgs e)
		{
			var status = BL.Core.Exams.Staff.ProjectThesisInternships.SubmitToCampus(this.ProjectThesisInternshipID ?? throw new InvalidOperationException(), this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Exams.Staff.ProjectThesisInternships.SubmitToCampusStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.SubmitToCampusStatuses.Success:
					this.AddSuccessAlert("Marks have been submitted to Campus.");
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.SubmitToCampusStatuses.MarksAreAlreadySubmittedToUniversity:
					this.AddErrorAlert("Marks are already submitted to BUHO Exams.");
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.SubmitToCampusStatuses.MarksAreAlreadySubmittedToCampus:
					this.AddErrorAlert("Marks are already submitted to Campus.");
					this.RefreshPage();
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		protected void lbtnSubmitToBUHO_OnClick(object sender, EventArgs e)
		{
			var status = BL.Core.Exams.Staff.ProjectThesisInternships.SubmitToUniversity(this.ProjectThesisInternshipID ?? throw new InvalidOperationException(), this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Exams.Staff.ProjectThesisInternships.SubmitToUniversityStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.SubmitToUniversityStatuses.Success:
					this.AddSuccessAlert("Marks have been submitted to BUHO Exams.");
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.SubmitToUniversityStatuses.MarksAreAlreadySubmittedToUniversity:
					this.AddErrorAlert("Marks are already submitted to BUHO Exams.");
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.SubmitToUniversityStatuses.MarksAreNotSubmittedToCampus:
					this.AddErrorAlert("Marks are not submitted to Campus.");
					this.RefreshPage();
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		protected void btnAttachRegisteredCourse_OnClick(object sender, EventArgs e)
		{
			this.modalAttachRegisteredCourse.Show();
			var registeredCourses = BL.Core.Exams.Staff.ProjectThesisInternships.GetRegisteredCourses(this.ProjectThesisInternshipID ?? throw new InvalidOperationException(), this.StaffIdentity.LoginSessionGuid);
			this.ddlRegisteredCourseID.DataBind(registeredCourses, CommonListItems.Select);
			this.modalAttachRegisteredCourse.Visible = true;
		}

		protected void btnSaveRegisteredCourse_OnClick(object sender, EventArgs e)
		{
			var registeredCourseID = this.ddlRegisteredCourseID.SelectedValue.ToNullableInt();
			var status = BL.Core.Exams.Staff.ProjectThesisInternships.AttachRegisteredCourses(this.ProjectThesisInternshipID ?? throw new InvalidOperationException(), registeredCourseID, this.StaffIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Exams.Staff.ProjectThesisInternships.AttachRegisteredCoursesStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.AttachRegisteredCoursesStatuses.MarksCanNotBeEditWhileSubmittedToHOD:
					this.AddErrorAlert($"Marks cannot be edited while submitted to HoD.");
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.AttachRegisteredCoursesStatuses.MarksEntryIsLocked:
					this.AddErrorAlert("Marks Entry is locked.");
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.AttachRegisteredCoursesStatuses.RegisteredCourseCategoryMustBeInternship:
					this.alertAttachRegisteredCourse.AddErrorAlert($"Course Category of Registered Course must be {CourseCategories.Internship.ToFullName()}.");
					this.updatePanelAttachRegisteredCourse.Update();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.AttachRegisteredCoursesStatuses.RegisteredCourseCategoryMustBeProject:
					this.alertAttachRegisteredCourse.AddErrorAlert($"Course Category of Registered Course must be {CourseCategories.Project.ToFullName()}.");
					this.updatePanelAttachRegisteredCourse.Update();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.AttachRegisteredCoursesStatuses.RegisteredCourseCategoryMustBeThesis:
					this.alertAttachRegisteredCourse.AddErrorAlert($"Course Category of Registered Course must be {CourseCategories.Thesis.ToFullName()}.");
					this.updatePanelAttachRegisteredCourse.Update();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.AttachRegisteredCoursesStatuses.RegisteredCourseCreditHoursDoNotMatch:
					this.alertAttachRegisteredCourse.AddErrorAlert("Credit Hours of Registered Course do not matched with mentioned credit hours.");
					this.updatePanelAttachRegisteredCourse.Update();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.AttachRegisteredCoursesStatuses.NoChangeFound:
					this.AddWarningAlert("No changed found.");
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.AttachRegisteredCoursesStatuses.RegisteredCourseAttached:
					this.AddSuccessAlert("Registered Course has been attached.");
					this.RefreshPage();
					return;
				case BL.Core.Exams.Staff.ProjectThesisInternships.AttachRegisteredCoursesStatuses.RegisteredCourseDetached:
					this.AddSuccessAlert("Registered Course has been detached.");
					this.RefreshPage();
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		protected void btnChangeHistory_OnClick(object sender, EventArgs e)
		{
			this.ProjectThesisInternshipMarksChanges.DisplayChanges(this.StudentID ?? throw new InvalidOperationException(), this.ProjectThesisInternshipID);
		}
	}
}