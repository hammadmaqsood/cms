﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Staff.Exams
{
	[AspirePage("22084E20-5C89-49C0-A804-F265495918F4", UserTypes.Staff, "~/Sys/Staff/Exams/ExamRemarksPolicy.aspx", "Exam Remarks Policy", true, AspireModules.Exams)]
	public partial class ExamRemarksPolicy : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Exams.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};
		public override PageIcon PageIcon => AspireGlyphicons.book.GetIcon();
		public override string PageTitle => "Exam Remarks Policy";

		public static string GetPageUrl(int? departmentID, int? programID, short? intakeSemesterID)
		{
			return GetPageUrl<ExamRemarksPolicy>().AttachQueryParams("DepartmentID", departmentID, "ProgramID", programID, "IntakeSemesterID", intakeSemesterID);
		}

		public static void Redirect(int? departmentID, int? programID, short? intakeSemesterID)
		{
			Redirect(GetPageUrl(departmentID, programID, intakeSemesterID));
		}

		private void RefreshPage()
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var intakeSemesterID = this.ddlIntakeSemesterIDFilter.SelectedValue.ToNullableShort();
			Redirect(departmentID, programID, intakeSemesterID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlDepartmentIDFilter.FillDepartments(this.StaffIdentity.InstituteID, CommonListItems.All).SetSelectedValueIfNotPostback("DepartmentID");
				this.ddlIntakeSemesterIDFilter.FillSemesters(CommonListItems.All).SetSelectedValueIfNotPostback("IntakeSemesterID");
				this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			this.ddlProgramIDFilter.FillPrograms(this.StaffIdentity.InstituteID, departmentID, true, CommonListItems.All).SetSelectedValueIfNotPostback("ProgramID");
			this.ddlProgramIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlIntakeSemesterIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlIntakeSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData();
		}

		public BL.Core.Exams.Staff.ExamRemarksPolicy.GetExamRemarksPoliciesForCrossTabGridResult Result;

		private void GetData()
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var intakeSemesterID = this.ddlIntakeSemesterIDFilter.SelectedValue.ToNullableShort();
			this.Result = BL.Core.Exams.Staff.ExamRemarksPolicy.GetExamRemarksPoliciesForCrossTabGrid(departmentID, programID, intakeSemesterID, this.AspireIdentity.LoginSessionGuid);
			this.repeaterProgram.DataBind(this.Result.CrossTabRows);
		}

		protected void btnAssignUnAssignExamRemarksPolicy_OnClick(object sender, EventArgs e)
		{
			this.DisplayAssignPolicyModal(null, null, null);
		}

		private void DisplayAssignPolicyModal(IReadOnlyCollection<int> programIDs, short? intakeSemesterIDFrom, short? intakeSemesterIDTo)
		{
			this.lbProgramIDs.FillPrograms(this.StaffIdentity.InstituteID, null, true);
			this.lbProgramIDs.ClearSelection();
			if (programIDs != null)
				foreach (var programID in programIDs)
				{
					var item = this.lbProgramIDs.Items.FindByValue(programID.ToString());
					if (item != null)
						item.Selected = true;
				}
			this.ddlIntakeSemesterFrom.FillSemesters(CommonListItems.Select).SetSelectedValueIfExists(intakeSemesterIDFrom.ToString());
			this.ddlIntakeSemesterTo.FillSemesters(CommonListItems.Select).SetSelectedValueIfExists(intakeSemesterIDTo.ToString());
			this.ddlIntakeSemesterTo_OnSelectedIndexChanged(null, null);
			this.modalExamRemarkPolicy.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var programIDs = this.lbProgramIDs.SelectedValues.Select(s => s.ToInt()).ToList();
			if (!programIDs.Any())
			{
				this.alertAssignPolicy.AddErrorAlert("Select at least one program.");
				this.updatePanelAssignPolicy.Update();
				return;
			}
			var intakeSemesterIDFrom = this.ddlIntakeSemesterFrom.SelectedValue.ToShort();
			var intakeSemesterIDTo = this.ddlIntakeSemesterTo.SelectedValue.ToShort();
			var examRemarksPolicyID = this.ddlExamRemarksPolicyID.SelectedValue.ToNullableInt();
			if (intakeSemesterIDFrom > intakeSemesterIDTo)
			{
				this.alertAssignPolicy.AddErrorAlert("Intake Semester (From) must be less than or equal to Intake Semester (To).");
				this.updatePanelAssignPolicy.Update();
				return;
			}
			var result = BL.Core.Exams.Staff.ExamRemarksPolicy.AssignUnAssignExamRemarksPolicy(programIDs, intakeSemesterIDFrom, intakeSemesterIDTo, examRemarksPolicyID, this.StaffIdentity.LoginSessionGuid);
			this.AddSuccessAlert($"{result.UpdatedRecords} records have been updated out of {result.ValidRecords} valid record.");
			this.RefreshPage();
		}

		protected void repeaterSemesters_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Assign":
					var args = e.DecryptedCommandArgument().Split(':');
					var programID = args[0].ToInt();
					var intakeSemesterID = args[1].ToNullableShort();
					var examRemarksPolicyID = args[2].ToNullableInt();
					this.DisplayAssignPolicyModal(new List<int> { programID }, intakeSemesterID, intakeSemesterID);
					this.lbProgramIDs.SelectedValue = programID.ToString();
					this.ddlIntakeSemesterFrom.SelectedValue = this.ddlIntakeSemesterTo.SelectedValue = intakeSemesterID.ToString();
					this.ddlExamRemarksPolicyID.SelectedValue = examRemarksPolicyID.ToString();
					break;
			}
		}

		protected void ddlIntakeSemesterFrom_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.FillExamRemarksPolicies();
		}

		protected void ddlIntakeSemesterTo_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.FillExamRemarksPolicies();
		}

		private void FillExamRemarksPolicies()
		{
			var fromSemesterID = this.ddlIntakeSemesterFrom.SelectedValue.ToNullableShort();
			var toSemesterID = this.ddlIntakeSemesterTo.SelectedValue.ToNullableShort();
			this.ddlExamRemarksPolicyID.Items.Clear();
			this.ddlExamRemarksPolicyID.Items.Add(new ListItem
			{
				Text = CommonListItems.NoneLabel,
				Value = CommonListItems.NoneValue,
				Attributes = { ["Description"] = "Selecting this option will un-assign the policy.", ["Summary"] = null, }
			});

			if (fromSemesterID > toSemesterID)
			{
				this.btnSave.Enabled = false;
				return;
			}

			this.btnSave.Enabled = true;

			var examRemarksPolicies = BL.Core.Exams.Staff.ExamRemarksPolicy.GetExamRemarksPolicies(this.StaffIdentity.LoginSessionGuid);
			foreach (var examRemarksPolicy in examRemarksPolicies)
			{
				var enabled = (fromSemesterID == null || examRemarksPolicy.ValidFromSemesterID <= fromSemesterID.Value)
							&& (toSemesterID == null
								|| (examRemarksPolicy.ValidUpToSemesterID == null || toSemesterID.Value <= examRemarksPolicy.ValidUpToSemesterID.Value));
				var item = new ListItem
				{
					Text = examRemarksPolicy.ExamRemarksPolicyName,
					Value = examRemarksPolicy.ExamRemarksPolicyID.ToString(),
					Attributes = { ["Description"] = examRemarksPolicy.ExamRemarksPolicyDescription, ["Summary"] = examRemarksPolicy.Summary.HtmlEncode(true) }
				};
				if (enabled == false)
					item.Attributes.Add("disabled", "disabled");
				this.ddlExamRemarksPolicyID.Items.Add(item);
			}
		}
	}
}