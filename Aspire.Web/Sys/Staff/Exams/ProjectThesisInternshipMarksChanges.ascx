﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProjectThesisInternshipMarksChanges.ascx.cs" Inherits="Aspire.Web.Sys.Staff.Exams.ProjectThesisInternshipMarksChanges" %>
<%@ Import Namespace="Aspire.Web.Sys.Staff.Exams" %>

<aspire:AspireModal runat="server" ID="modalMarksChanges" HeaderText="Change History" ModalSize="Large">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanelMain" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireGridView runat="server" AutoGenerateColumns="false" ID="gvMarksChanges" ItemType="Aspire.BL.Core.Exams.Staff.ProjectThesisInternships.ProjectThesisInternshipChange" OnPageSizeChanging="gvMarksChanges_PageSizeChanging" OnPageIndexChanging="gvMarksChanges_PageIndexChanging">
					<Columns>
						<asp:TemplateField HeaderText="Action">
							<ItemTemplate>
								<%#: Item.ActionTypeFullName %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Date">
							<ItemTemplate>
								<%#: Item.ActionDate %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Changed By">
							<ItemTemplate>
								<%#: Item.ChangedBy %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Details">
							<ItemTemplate>
								<aspire:AspireHyperLink runat="server" Text="Details" Target="_blank" NavigateUrl="<%# ProjectThesisInternshipMarksChangeDetails.GetPageUrl(Item.StudentID, Item.ProjectThesisInternshipChangeID) %>" />
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</aspire:AspireGridView>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<aspire:AspireModalCloseButton runat="server">Close</aspire:AspireModalCloseButton>
	</FooterTemplate>
</aspire:AspireModal>
