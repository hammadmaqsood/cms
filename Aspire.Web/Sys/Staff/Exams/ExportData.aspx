﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ExportData.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Exams.ExportData" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div>
		<div class="form-inline">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Offered Semester:" AssociatedControlID="ddlSemesterID" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="Export" />
			</div>
			<div class="form-group">
				<aspire:AspireButton ID="btnExportMarks" ButtonType="Primary" runat="server" Text="Export Marks To CSV" ValidationGroup="Export" Glyphicon="export" OnClick="btnExportMarks_OnClick" />
				<aspire:AspireButton ID="btnExportThesisProjects" ButtonType="Primary" runat="server" Text="Export Thesis/Projects To CSV" ValidationGroup="Export" Glyphicon="export" OnClick="btnExportThesisProjects_Click" />
			</div>
		</div>
	</div>
	<p></p>
	<div>
		<div class="form-inline">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="From Intake Semester:" AssociatedControlID="ddlFromIntakeSemesterID" />
				<aspire:AspireDropDownList runat="server" ID="ddlFromIntakeSemesterID" ValidationGroup="CGPA" />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Up To Offered Semester:" AssociatedControlID="ddlUpToOfferedSemesterID" />
				<aspire:AspireDropDownList runat="server" ID="ddlUpToOfferedSemesterID" ValidationGroup="CGPA" />
			</div>
			<div class="form-group">
				<aspire:AspireButton ID="btnExportCGPA" ButtonType="Primary" runat="server" Text="Export GPA/CGPA To CSV" ValidationGroup="CGPA" Glyphicon="export" OnClick="btnExportCGPA_Click" />
			</div>
		</div>
	</div>
</asp:Content>
