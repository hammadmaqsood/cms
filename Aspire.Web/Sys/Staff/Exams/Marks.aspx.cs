﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Exams
{
	[AspirePage("12AC693D-A76B-433F-B730-7F0E78510314", UserTypes.Staff, "~/Sys/Staff/Exams/Marks.aspx", "Marks", true, AspireModules.Exams)]
	public partial class Marks : StaffPage
	{
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{StaffPermissions.Exams.Module,new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => this.ExamType?.ToFullName();

		private OfferedCourseExam.ExamTypes? ExamType => this.Request.GetParameterValue<OfferedCourseExam.ExamTypes>("ExamType");

		public static string GetPageUrl(int offeredCourseID, OfferedCourseExam.ExamTypes examType, int? offeredCourseExamID)
		{
			return GetPageUrl<Marks>().AttachQueryParams("OfferedCourseExamID", offeredCourseExamID, "OfferedCourseID", offeredCourseID, nameof(ExamType), examType);
		}

		public static void Redirect(int offeredCourseID, OfferedCourseExam.ExamTypes examType, int? offeredCourseExamID)
		{
			Redirect(GetPageUrl(offeredCourseID, examType, offeredCourseExamID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
		}
	}
}