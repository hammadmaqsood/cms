﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="MarksSheetChangeDetails.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Exams.MarksSheetChangeDetails" %>

<%@ Register Src="~/Sys/Common/Exam/MarksSheetChangeDetails.ascx" TagPrefix="uc1" TagName="MarksSheetChangeDetails" %>
<asp:Content ContentPlaceHolderID="HeadPH" runat="server">
	<link href="../../../Scripts/Plugins/codediff.js/googlecode.css" rel="stylesheet" />
	<link href="../../../Scripts/Plugins/codediff.js/codediff.css" rel="stylesheet" />
	<style type="text/css">
		.select2-container--bootstrap .select2-selection--single {
			height: inherit !important;
		}
	</style>
</asp:Content>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc1:MarksSheetChangeDetails runat="server" ID="marksSheetChangeDetail" />
</asp:Content>
