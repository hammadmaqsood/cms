﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Courses.aspx.cs" Inherits="Aspire.Web.Sys.Staff.Exams.Courses" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Offered Semester:" AssociatedControlID="ddlSemesterID" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" CausesValidation="False" AutoPostBack="True" />
		</div>
		<div class="col-md-3 form-group">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" OnSelectedIndexChanged="ddlDepartmentID_OnSelectedIndexChanged" CausesValidation="False" AutoPostBack="True" />
		</div>
		<div class="col-md-3 form-group">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramID" OnSelectedIndexChanged="ddlProgramID_OnSelectedIndexChanged" CausesValidation="False" AutoPostBack="True" />
		</div>
		<div class="col-md-1 form-group">
			<aspire:AspireLabel runat="server" Text="Sem. #:" AssociatedControlID="ddlSemesterNo" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" OnSelectedIndexChanged="ddlSemesterNo_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
		<div class="col-md-1 form-group">
			<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSection" />
			<aspire:AspireDropDownList runat="server" ID="ddlSection" OnSelectedIndexChanged="ddlSection_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShift" />
			<aspire:AspireDropDownList runat="server" ID="ddlShift" OnSelectedIndexChanged="ddlShift_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 form-group">
			<aspire:AspireLabel runat="server" Text="Teacher:" AssociatedControlID="ddlFacultyMemberID" />
			<aspire:AspireDropDownList runat="server" ID="ddlFacultyMemberID" OnSelectedIndexChanged="ddlFacultyMemberID_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Marks Entry Completed:" AssociatedControlID="ddlMarksEntryCompleted" />
			<aspire:AspireDropDownList runat="server" ID="ddlMarksEntryCompleted" OnSelectedIndexChanged="ddlMarksEntryCompleted_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Submitted to HOD:" AssociatedControlID="ddlSubmittedToHOD" />
			<aspire:AspireDropDownList runat="server" ID="ddlSubmittedToHOD" OnSelectedIndexChanged="ddlSubmittedToHOD_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Submitted to Campus:" AssociatedControlID="ddlSubmittedToCampus" />
			<aspire:AspireDropDownList runat="server" ID="ddlSubmittedToCampus" OnSelectedIndexChanged="ddlSubmittedToCampus_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
		<div class="col-md-2 form-group">
			<aspire:AspireLabel runat="server" Text="Submitted to BUHO Exams:" AssociatedControlID="ddlSubmittedToUniversity" />
			<aspire:AspireDropDownList runat="server" ID="ddlSubmittedToUniversity" OnSelectedIndexChanged="ddlSubmittedToUniversity_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
		</div>
	</div>
	<aspire:AspireGridView runat="server" ID="gvCourses" ItemType="Aspire.BL.Core.Exams.Staff.OfferedCourses.OfferedCourse" AutoGenerateColumns="False" OnPageSizeChanging="gvCourses_OnPageSizeChanging" OnPageIndexChanging="gvCourses_OnPageIndexChanging" OnSorting="gvCourses_OnSorting" AllowSorting="True">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate>
					<%#: Container.DataItemIndex + 1 %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Course" SortExpression="Title">
				<ItemTemplate>
					<span class="label label-default" title="Offered Semester"><%# Item.SemesterID.ToSemesterString() %></span>
					<span class="label label-info" title="Course Category"><%# Item.CategoryEnum.ToFullName() %></span>
					<div>
						<%#: Item.Title %>
					</div>
					<span runat="server" visible="<%# Item.MarksEntryCompleted %>" class="label label-success" title="Marks Entry Completed">Marks Entry Completed</span>
					<span runat="server" visible="<%# Item.SubmittedToHOD != null %>" class="label label-success" title="<%#: Item.SubmittedToHOD?.ToShortDateString() %>">Submitted To HOD</span>
					<span runat="server" visible="<%# Item.SubmittedToCampus != null %>" class="label label-success" title="<%#: Item.SubmittedToCampus?.ToShortDateString() %>">Submitted To Campus</span>
					<span runat="server" visible="<%# Item.SubmittedToUniversity != null %>" class="label label-success" title="<%#: Item.SubmittedToUniversity?.ToShortDateString() %>">Submitted to BUHO Exams</span>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Class" SortExpression="Class">
				<ItemTemplate>
					<span class="label label-primary" title="Class"><%#: Item.Teacher.ToNAIfNullOrEmpty() %></span>
					<div>
						<%#: Item.Class %>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireHyperLink Visible='<%# Eval("MarksSheetVisible") %>' Target="_blank" runat="server" ToolTip="View Exam Marks Sheet" Text="View" NavigateUrl='<%#Aspire.Web.Sys.Staff.Exams. MarksSheet.GetPageUrl((int)Eval("OfferedCourseID")) %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<script type="text/javascript">
		$(function () {
			$("#<%=this.ddlFacultyMemberID.ClientID%>").select2({ theme: "bootstrap" });
		});
	</script>
</asp:Content>
