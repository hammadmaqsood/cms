﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Web.Sys.Staff.Exams {
    
    
    public partial class ProjectThesisInternships {
        
        /// <summary>
        /// tbEnrollment control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.EnrollmentTextBox tbEnrollment;
        
        /// <summary>
        /// panelThesisInternShip control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel panelThesisInternShip;
        
        /// <summary>
        /// ucStudentInfo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Web.Sys.Staff.Common.StudentInfoUserControl ucStudentInfo;
        
        /// <summary>
        /// hlAdd control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireHyperLinkButton hlAdd;
        
        /// <summary>
        /// btnChangeHistory control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireButton btnChangeHistory;
        
        /// <summary>
        /// gvProjectThesisInternships control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireGridView gvProjectThesisInternships;
        
        /// <summary>
        /// projectThesisInternshipMarksChanges control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Web.Sys.Staff.Exams.ProjectThesisInternshipMarksChanges projectThesisInternshipMarksChanges;
    }
}
