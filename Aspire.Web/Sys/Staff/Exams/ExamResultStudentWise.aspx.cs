﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Staff.Exams
{
	[AspirePage("F756AE0A-DE68-4339-A9D7-9979FFCCA893", UserTypes.Staff, "~/Sys/Staff/Exams/ExamResultStudentWise.aspx", "Exam Result Student Wise", true, AspireModules.Exams)]
	public partial class ExamResultStudentWise : StaffPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.education);
		public override string PageTitle => "Exam Result Student Wise";
		protected override IReadOnlyDictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<StaffPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ StaffPermissions.Exams.Module, UserGroupPermission.Allowed},
		};

		public static string GetPageUrl(int? studentID)
		{
			return GetAspirePageAttribute<ExamResultStudentWise>().PageUrl.AttachQueryParam("StudentID", studentID);
		}

		public static string GetPageUrl(string enrollment)
		{
			return GetAspirePageAttribute<ExamResultStudentWise>().PageUrl.AttachQueryParam("Enrollment", enrollment);
		}

		public static void Redirect(int? studentID)
		{
			Redirect<ExamResultStudentWise>("StudentID", studentID);
		}

		public static void Redirect(string enrollment)
		{
			Redirect<ExamResultStudentWise>("Enrollment", enrollment);
		}

		protected void Page_Load(object sender, EventArgs e)
		{

		}
	}
}