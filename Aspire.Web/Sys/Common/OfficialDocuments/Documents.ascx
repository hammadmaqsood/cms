﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Documents.ascx.cs" Inherits="Aspire.Web.Sys.Common.OfficialDocuments.Documents" %>

<div class=" table-responsive">
	<table class="AspireGridView table table-bordered table-condensed table-hover table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Document Name</th>
				<th>Provided By</th>
				<th>Uploaded On</th>
			</tr>
		</thead>
		<tbody>
			<asp:Repeater runat="server" ItemType="Aspire.Model.Entities.OfficialDocument" ID="repeaterDocuments" OnItemCommand="repeaterDocuments_OnItemCommand">
				<ItemTemplate>
					<tr>
						<td><%#: Container.ItemIndex + 1 %></td>
						<td>
							<aspire:AspireLinkButton CommandName="Download" EncryptedCommandArgument="<%# Item.OfficialDocumentID %>" runat="server" ToolTip="Click to Download File" Text="<%# Item.DocumentName %>" CausesValidation="False" />
						</td>
						<td><%#: Item.ProvidedBy %></td>
						<td><%#: Item.UploadedOn.ToString("D") %></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
			<tr id="trNoRecordFound" runat="server">
				<td colspan="4">No record found.</td>
			</tr>
		</tbody>
	</table>
</div>
