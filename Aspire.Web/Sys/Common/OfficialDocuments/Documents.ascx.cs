﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Common.OfficialDocuments
{
	public partial class Documents : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				List<OfficialDocument> officialDocuments;
				switch (AspireIdentity.Current.UserType)
				{
					case UserTypes.Executive:
						officialDocuments = BL.Core.OfficialDocuments.Executive.Documents.GetOfficialDocuments(AspireIdentity.Current.LoginSessionGuid);
						break;
					case UserTypes.Staff:
						officialDocuments = BL.Core.OfficialDocuments.Staff.Documents.GetOfficialDocuments(AspireIdentity.Current.LoginSessionGuid);
						break;
					case UserTypes.Student:
						officialDocuments = BL.Core.OfficialDocuments.Student.Documents.GetOfficialDocuments(AspireIdentity.Current.LoginSessionGuid);
						break;
					case UserTypes.Faculty:
						officialDocuments = BL.Core.OfficialDocuments.Faculty.Documents.GetOfficialDocuments(AspireIdentity.Current.LoginSessionGuid);
						break;
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.System:
					case UserTypes.Anonymous:
					case UserTypes.Candidate:
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Admins:
					case UserTypes.Any:
						throw new InvalidOperationException();
					default:
						throw new NotImplementedEnumException(AspireIdentity.Current.UserType);
				}

				if (officialDocuments?.Any() == true)
				{
					this.repeaterDocuments.DataBind(officialDocuments);
					this.repeaterDocuments.Visible = true;
				}
				else
					this.repeaterDocuments.Visible = false;

				this.trNoRecordFound.Visible = !this.repeaterDocuments.Visible;
			}
		}

		protected void repeaterDocuments_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Download":
					var officialDocumentID = e.DecryptedCommandArgumentToInt();
					(OfficialDocument OfficaDocument, SystemFile SystemFile, byte[] FileBytes)? result;
					switch (AspireIdentity.Current.UserType)
					{
						case UserTypes.Executive:
							result = BL.Core.OfficialDocuments.Executive.Documents.ReadOfficialDocuments(officialDocumentID, AspireIdentity.Current.LoginSessionGuid);
							break;
						case UserTypes.Staff:
							result = BL.Core.OfficialDocuments.Staff.Documents.ReadOfficialDocuments(officialDocumentID, AspireIdentity.Current.LoginSessionGuid);
							break;
						case UserTypes.Student:
							result = BL.Core.OfficialDocuments.Student.Documents.ReadOfficialDocuments(officialDocumentID, AspireIdentity.Current.LoginSessionGuid);
							break;
						case UserTypes.Faculty:
							result = BL.Core.OfficialDocuments.Faculty.Documents.ReadOfficialDocuments(officialDocumentID, AspireIdentity.Current.LoginSessionGuid);
							break;
						case UserTypes.MasterAdmin:
						case UserTypes.Admin:
						case UserTypes.System:
						case UserTypes.Anonymous:
						case UserTypes.Candidate:
						case UserTypes.Alumni:
						case UserTypes.IntegratedService:
						case UserTypes.ManualSQL:
						case UserTypes.Admins:
						case UserTypes.Any:
							throw new InvalidOperationException();
						default:
							throw new NotImplementedEnumException(AspireIdentity.Current.UserType);
					}
					if (result != null)
					{
						this.Response.Clear();
						var docName = result.Value.OfficaDocument.DocumentName;
						var desiredFileExtension = result.Value.SystemFile.FileExtensionEnum.ToFileExtension();
						if (System.IO.Path.GetExtension(docName) != desiredFileExtension)
							docName += desiredFileExtension;
						this.Response.AddHeader("Content-Disposition", $"attachment; filename=\"{docName}\"");
						this.Response.AddHeader("Content-Length", result.Value.FileBytes.Length.ToString());
						this.Response.ContentType = docName.ToMimeType();
						this.Response.BinaryWrite(result.Value.FileBytes);
					}
					break;
			}
		}
	}
}