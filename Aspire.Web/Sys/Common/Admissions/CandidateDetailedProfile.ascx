﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CandidateDetailedProfile.ascx.cs" Inherits="Aspire.Web.Sys.Common.Admissions.CandidateDetailedProfile" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<asp:FormView runat="server" ID="fvCandidate" AllowPaging="False" DefaultMode="ReadOnly" RenderOuterTable="False" ItemType="Aspire.BL.Core.Admissions.Common.ProfileInformation.CandidateDetailedProfile">
	<ItemTemplate>
		<div class="panel panel-primary" id="divPersonalInformation">
			<div class="panel-heading">
				<h3 class="panel-title">Personal Information</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group text-center">
							<asp:Image runat="server" CssClass="img img-thumbnail" ID="imgPhoto" Width="100px" Height="100px" AlternateText="Photo" ImageUrl="<%# Aspire.Web.Sys.Common.CandidatePicture.GetImageUrl(Item.Candidate.CandidateID, TimeSpan.Zero) %>" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="lblName" />
							<div>
								<aspire:Label runat="server" ID="lblName" Text="<%# Item.Candidate.Name.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Email:" AssociatedControlID="lblEmail" />
							<div>
								<aspire:Label runat="server" ID="lblEmail" Text="<%# Item.Candidate.Email.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group" runat="server" visible="<%# !Item.Candidate.ForeignStudent %>">
							<aspire:AspireLabel runat="server" Text="CNIC:" AssociatedControlID="lblCNIC" />
							<div>
								<aspire:Label runat="server" ID="lblCNIC" Text="<%# Item.Candidate.CNIC.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Bahria University Registration #:" AssociatedControlID="lblBahriaUniversityRegistrationNo" />
							<div>
								<aspire:Label runat="server" ID="lblBahriaUniversityRegistrationNo" Text="<%# Item.Candidate.BURegistrationNo.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%# Item.Candidate.ForeignStudent %>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Passport No.:" AssociatedControlID="lblPassport" />
							<div>
								<aspire:Label runat="server" ID="lblPassport" Text="<%# Item.Candidate.PassportNo.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Gender:" AssociatedControlID="lblGender" />
							<div>
								<aspire:Label runat="server" ID="lblGender" Text="<%# (Item.Candidate.GenderEnum?.ToFullName()).ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Category:" AssociatedControlID="lblCategory" />
							<div>
								<aspire:Label runat="server" ID="lblCategory" Text="<%# (Item.Candidate.CategoryEnum?.ToFullName()).ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="DOB:" AssociatedControlID="lblDOB" />
							<div>
								<aspire:Label runat="server" ID="lblDOB" Text='<%# (Item.Candidate.DOB?.ToString("d")).ToNAIfNullOrEmpty() %>' />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Blood Group:" AssociatedControlID="lblBloodGroup" />
							<div>
								<aspire:Label runat="server" ID="lblBloodGroup" Text="<%# (Item.Candidate.BloodGroupEnum?.ToFullName()).ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Phone No. (Home):" AssociatedControlID="lblPhone" />
							<div>
								<aspire:Label runat="server" ID="lblPhone" Text="<%# Item.Candidate.Phone.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Mobile No. " AssociatedControlID="lblMobile" />
							<div>
								<aspire:Label runat="server" ID="lblMobile" Text="<%# Item.Candidate.Mobile.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Nationality:" AssociatedControlID="lblNationality" />
							<div>
								<aspire:Label runat="server" ID="lblNationality" Text="<%# Item.Candidate.Nationality.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Country:" AssociatedControlID="lblCountry" />
							<div>
								<aspire:Label runat="server" ID="lblCountry" Text="<%# Item.Candidate.Country.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Province/State:" AssociatedControlID="lblProvince" />
							<div>
								<aspire:Label runat="server" ID="lblProvince" Text="<%# Item.Candidate.Province.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="District/City:" AssociatedControlID="lblDistrict" />
							<div>
								<aspire:Label runat="server" ID="lblDistrict" Text="<%# Item.Candidate.District.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%# !Item.Candidate.ForeignStudent %>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Tehsil:" AssociatedControlID="lblTehsil" />
							<div>
								<aspire:Label runat="server" ID="lblTehsil" Text="<%# Item.Candidate.Tehsil.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%# !Item.Candidate.ForeignStudent %>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Domicile:" AssociatedControlID="lblDomicile" />
							<div>
								<aspire:Label runat="server" ID="lblDomicile" Text="<%# Item.Candidate.Domicile.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%# !Item.Candidate.ForeignStudent %>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Area Type:" AssociatedControlID="lblAreaType" />
							<div>
								<aspire:Label runat="server" ID="lblAreaType" Text="<%# (Item.Candidate.AreaTypeEnum?.ToFullName()).ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Current Address:" AssociatedControlID="lblPresentAddress" />
							<div>
								<aspire:Label runat="server" ID="lblPresentAddress" Text="<%# Item.Candidate.CurrentAddress.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Permanent Address:" AssociatedControlID="lblPermanentAddress" />
							<div>
								<aspire:Label runat="server" ID="lblPermanentAddress" Text="<%# Item.Candidate.PermanentAddress.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%# !Item.Candidate.ForeignStudent %>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Service No.:" AssociatedControlID="lblServiceNo" />
							<div>
								<aspire:Label runat="server" ID="lblServiceNo" Text="<%# Item.Candidate.ServiceNo.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Emergency Contact Name:" AssociatedControlID="lblEmergencyContactName" />
							<div>
								<aspire:Label runat="server" ID="lblEmergencyContactName" Text="<%# Item.Candidate.EmergencyContactName.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Emergency Mobile:" AssociatedControlID="lblEmgMobile" />
							<div>
								<aspire:Label runat="server" ID="lblEmgMobile" Text="<%# Item.Candidate.EmergencyMobile.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Emergency Phone:" AssociatedControlID="lblEmgPhone" />
							<div>
								<aspire:Label runat="server" ID="lblEmgPhone" Text="<%# Item.Candidate.EmergencyPhone.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Religion:" AssociatedControlID="lblReligion" />
							<div>
								<aspire:Label runat="server" ID="lblReligion" Text="<%# Item.Candidate.Religion.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Next Of Kin:" AssociatedControlID="lblNextOfKin" />
							<div>
								<aspire:Label runat="server" ID="lblNextOfKin" Text="<%# Item.Candidate.NextOfKin.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Source of Information:" AssociatedControlID="lblSourceOfInformation" />
							<div>
								<aspire:Label runat="server" ID="lblSourceOfInformation" Text="<%# (Item.Candidate.SourceOfInformationEnum?.ToFullName()).ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Physical Disability:" AssociatedControlID="lblPhysicalDisability" />
							<div>
								<aspire:Label runat="server" ID="lblPhysicalDisability" Text="<%# Item.Candidate.PhysicalDisability.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%# !Item.Candidate.ForeignStudent %>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="NTN/CNIC for Taxation:" AssociatedControlID="lblNTNOrCNIC" />
							<div>
								<aspire:Label runat="server" ID="lblNTNOrCNIC" Text="<%# Item.Candidate.NTNOrCNIC.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default" id="divFatherInformation">
			<div class="panel-heading">
				<h3 class="panel-title">Father's Information</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="lblFatherName" />
							<div>
								<aspire:Label runat="server" ID="lblFatherName" Text="<%# Item.Candidate.FatherName.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Alive:" AssociatedControlID="lblFatherAlive" />
							<div>
								<aspire:Label runat="server" ID="lblFatherAlive" Text="<%# Item.Candidate.IsFatherAlive.ToYesNo() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%# !Item.Candidate.ForeignStudent %>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="CNIC:" AssociatedControlID="lblFatherCNIC" />
							<div>
								<aspire:Label runat="server" ID="lblFatherCNIC" Text="<%# Item.Candidate.FatherCNIC.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%# Item.Candidate.ForeignStudent %>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Passport No.:" AssociatedControlID="lblFatherPassport" />
							<div>
								<aspire:Label runat="server" ID="lblFatherPassport" Text="<%# Item.Candidate.FatherPassportNo.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Designation/Profession:" AssociatedControlID="lblDesignation" />
							<div>
								<aspire:Label runat="server" ID="lblDesignation" Text="<%# Item.Candidate.FatherDesignation.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Department/Unit:" AssociatedControlID="lblDepartment" />
							<div>
								<aspire:Label runat="server" ID="lblDepartment" Text="<%# Item.Candidate.FatherDepartment.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%# !Item.Candidate.ForeignStudent %>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Service No. (If Naval):" AssociatedControlID="lblFatherServiceNo" />
							<div>
								<aspire:Label runat="server" ID="lblFatherServiceNo" Text="<%# Item.Candidate.FatherServiceNo.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Organization:" AssociatedControlID="lblFatherOrganization" />
							<div>
								<aspire:Label runat="server" ID="lblFatherOrganization" Text="<%# Item.Candidate.FatherOrganization.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Mobile No:" AssociatedControlID="lblFatherMobile" />
							<div>
								<aspire:Label runat="server" ID="lblFatherMobile" Text="<%# Item.Candidate.FatherMobile.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Phone No. (Office):" AssociatedControlID="lblFatherPhoneOffice" />
							<div>
								<aspire:Label runat="server" ID="lblFatherPhoneOffice" Text="<%# Item.Candidate.FatherOfficePhone.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Phone No. (Home):" AssociatedControlID="lblFatherPhoneHome" />
							<div>
								<aspire:Label runat="server" ID="lblFatherPhoneHome" Text="<%# Item.Candidate.FatherHomePhone.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Fax (Office):" AssociatedControlID="lblFatherFaxOffice" />
							<div>
								<aspire:Label runat="server" ID="lblFatherFaxOffice" Text="<%# Item.Candidate.FatherFax.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default" id="divSponsorInformation">
			<div class="panel-heading">
				<h3 class="panel-title">Sponsor's Information</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Sponsor By:" AssociatedControlID="lblSponsorBy" />
							<div>
								<aspire:Label runat="server" ID="lblSponsorBy" Text="<%# Item.Candidate.SponsoredByEnum.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%# Item.Candidate.SponsoredByEnum!=Sponsorships.Father && Item.Candidate.SponsoredByEnum!=Sponsorships.Self  %>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="lblSponsorName" />
							<div>
								<aspire:Label runat="server" ID="lblSponsorName" Text="<%# Item.Candidate.SponsorName.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%# !Item.Candidate.ForeignStudent && Item.Candidate.SponsoredByEnum==Sponsorships.Guardian%>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="CNIC:" AssociatedControlID="lblSponsorCNIC" />
							<div>
								<aspire:Label runat="server" ID="lblSponsorCNIC" Text="<%# Item.Candidate.SponsorCNIC.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%# Item.Candidate.ForeignStudent && Item.Candidate.SponsoredByEnum==Sponsorships.Guardian%>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Passport No.:" AssociatedControlID="lblSponsorPassport" />
							<div>
								<aspire:Label runat="server" ID="lblSponsorPassport" Text="<%# Item.Candidate.SponsorPassportNo.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%#  Item.Candidate.SponsoredByEnum!=Sponsorships.Father &&  Item.Candidate.SponsoredByEnum!=Sponsorships.Self %>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Relationship with Sponsor:" AssociatedControlID="lblSponsorRelationShip" />
							<div>
								<aspire:Label runat="server" ID="lblSponsorRelationShip" Text="<%# Item.Candidate.SponsorRelationshipWithGuardian.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%#  Item.Candidate.SponsoredByEnum!=Sponsorships.Father && Item.Candidate.SponsoredByEnum!=Sponsorships.Organization%>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Designation:" AssociatedControlID="lblSponsorDesignation" />
							<div>
								<aspire:Label runat="server" ID="lblSponsorDesignation" Text="<%# Item.Candidate.SponsorDesignation.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%#  Item.Candidate.SponsoredByEnum!=Sponsorships.Father && Item.Candidate.SponsoredByEnum!=Sponsorships.Organization%>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="lblSponsorDepartment" />
							<div>
								<aspire:Label runat="server" ID="lblSponsorDepartment" Text="<%# Item.Candidate.SponsorDepartment.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%#  !Item.Candidate.ForeignStudent && Item.Candidate.SponsoredByEnum==Sponsorships.Guardian%>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Service No. (If Naval):" AssociatedControlID="lblSponsorServiceNo" />
							<div>
								<aspire:Label runat="server" ID="lblSponsorServiceNo" Text="<%# Item.Candidate.SponsorServiceNo.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%#  Item.Candidate.SponsoredByEnum!=Sponsorships.Father && Item.Candidate.SponsoredByEnum!=Sponsorships.Organization%>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Organization:" AssociatedControlID="lblSponsorOrganization" />
							<div>
								<aspire:Label runat="server" ID="lblSponsorOrganization" Text="<%# Item.Candidate.SponsorOrganization.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%#  Item.Candidate.SponsoredByEnum==Sponsorships.Guardian%>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Mobile No:" AssociatedControlID="lblSponsorMobile" />
							<div>
								<aspire:Label runat="server" ID="lblSponsorMobile" Text="<%# Item.Candidate.SponsorMobile.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%#  Item.Candidate.SponsoredByEnum==Sponsorships.Guardian%>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Phone No. (Home):" AssociatedControlID="lblSponsorPhoneHome" />
							<div>
								<aspire:Label runat="server" ID="lblSponsorPhoneHome" Text="<%# Item.Candidate.SponsorPhoneHome.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%#  Item.Candidate.SponsoredByEnum!=Sponsorships.Father%>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Phone No. (Office):" AssociatedControlID="lblSponsorPhoneOffice" />
							<div>
								<aspire:Label runat="server" ID="lblSponsorPhoneOffice" Text="<%# Item.Candidate.SponsorPhoneOffice.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%#  Item.Candidate.SponsoredByEnum!=Sponsorships.Father%>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Fax (Office):" AssociatedControlID="lblSponsorFaxOffice" />
							<div>
								<aspire:Label runat="server" ID="lblSponsorFaxOffice" Text="<%# Item.Candidate.SponsorFax.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
					<div class="col-md-6" runat="server" visible="<%#  !Item.Candidate.ForeignStudent && Item.Candidate.SponsoredByEnum!=Sponsorships.Organization%>">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Annual Income:" AssociatedControlID="lblAnnualIncome" />
							<div>
								<aspire:Label runat="server" ID="lblAnnualIncome" Text="<%# Item.Candidate.AnnualFamilyIncome.ToNAIfNullOrEmpty() %>" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default" runat="server" visible="<%# !Item.Candidate.ForeignStudent %>" id="divAppliedProgramsNationalStudent">
			<div class="panel-heading">
				<h3 class="panel-title">Applied Programs</h3>
			</div>
			<div class="panel-body">
				<asp:Repeater runat="server" ID="rptCandidateAppliedPrograms" DataSource="<%# Item.AppliedPrograms %>" ItemType="Aspire.BL.Core.Admissions.Common.ProfileInformation.CandidateDetailedProfile.AppliedProgram">
					<ItemTemplate>
						<div class="panel panel-default" id="divAppliedProgram">
							<div class="panel-heading">
								<%#: $"{Item.AdmissionOpenProgram1.Program.ProgramShortName}-{Item.AdmissionOpenProgram1.Program.Institute.InstituteShortName}" %>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<aspire:AspireLabel runat="server" Text="Application No.:" AssociatedControlID="lblApplicationNo" />
											<div>
												<aspire:Label runat="server" ID="lblApplicationNo" Text="<%# Item.CandidateAppliedProgram.ApplicationNo.ToNAIfNullOrEmpty() %>" />
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="lblAppliedProgramSemester" />
											<div>
												<aspire:Label runat="server" ID="lblAppliedProgramSemester" Text="<%# Item.AdmissionOpenProgram1.SemesterID.ToSemesterString() %>" />
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<aspire:AspireLabel runat="server" Text="Challan No.:" AssociatedControlID="lblFullChallanNo" />
											<div>
												<aspire:Label runat="server" ID="lblFullChallanNo" Text="<%# Item.CandidateAppliedProgram.FullChallanNo %>" />
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<aspire:AspireLabel runat="server" Text="Admission Processing Fee:" AssociatedControlID="lblAppliedProgramAdmissionProcessingFee" />
											<div>
												<aspire:Label runat="server" ID="lblAppliedProgramAdmissionProcessingFee" Text='<%# $"Rs. {Item.CandidateAppliedProgram.Amount}" %>' />
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<aspire:AspireLabel runat="server" Text="Fee Submission Date:" AssociatedControlID="lblAppliedProgramAdmissionFeeSubmissionDate" />
											<div>
												<aspire:Label runat="server" ID="lblAppliedProgramAdmissionFeeSubmissionDate" Text='<%# (Item.CandidateAppliedProgram.AccountTransaction?.TransactionDate.ToString("D")).ToNAIfNullOrEmpty() %>' />
											</div>
										</div>
									</div>
									<div class="col-md-6" runat="server" visible="<%# Item.CandidateAppliedProgram.AccountTransactionID != null %>">
										<div class="form-group">
											<aspire:AspireLabel runat="server" Text="Bank:" AssociatedControlID="lblBank" />
											<div>
												<aspire:Label runat="server" ID="lblBank" Text="<%# Item.CandidateAppliedProgram.AccountTransaction?.InstituteBankAccount.Bank.BankName %>" />
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<aspire:AspireLabel runat="server" Text="Entry Test Date:" AssociatedControlID="lblAppliedProgramEntryTestDate" />
											<div>
												<aspire:Label runat="server" ID="lblAppliedProgramEntryTestDate" Text="<%# Item.EntryTestDateString %>" />
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<aspire:AspireLabel runat="server" Text="Pre-Requisite Qualification:" AssociatedControlID="lblPreRequisiteQualification" />
											<div>
												<aspire:Label runat="server" ID="lblPreRequisiteQualification" Text="<%# Item.CandidateAppliedProgram.PreReqDegree %>" />
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<aspire:AspireLabel runat="server" Text="Result Awaited:" AssociatedControlID="lblResultAwaited" />
											<div>
												<aspire:Label runat="server" ID="lblResultAwaited" Text='<%# Item.CandidateAppliedProgram.ResultAwaitedDegreeTypeEnum?.ToFullName() ?? "No" %>' />
											</div>
										</div>
									</div>
									<div class="col-md-6" runat="server" visible="<%# Item.CandidateAppliedProgram.ResearchThemeArea!=null %>">
										<div class="form-group">
											<aspire:AspireLabel runat="server" Text="Research Theme Area:" AssociatedControlID="lblResearchThemeArea" />
											<div>
												<aspire:Label runat="server" ID="lblResearchThemeArea" Text='<%# Item.CandidateAppliedProgram.ResearchThemeArea?.AreaName.ToNAIfNullOrEmpty() %>' />
											</div>
										</div>
									</div>
								</div>
							</div>
							<table class="table table-hover table-stripped table-bordered">
								<thead>
									<tr>
										<th>Degree</th>
										<th>Marks/CGPA</th>
										<th>Percentage</th>
										<th>Subject</th>
										<th>Institute</th>
										<th>Board/University</th>
										<th>Passing Year</th>
									</tr>
								</thead>
								<tbody>
									<asp:Repeater runat="server" ID="rptCandidateAppliedProgramAcademicRecords" DataSource="<%# Item.AcademicRecords %>" ItemType="Aspire.Model.Entities.CandidateAppliedProgramAcademicRecord">
										<ItemTemplate>
											<tr>
												<td>
													<%#: Item.DegreeTypeEnum.ToFullName() %>
												</td>
												<td>
													<%#: $"{Item.ObtainedMarks}/{Item.TotalMarks}" %>
												</td>
												<td>
													<%#: Item.Percentage.ToString("F2") %>
												</td>
												<td>
													<%#: Item.Subjects.ToNAIfNullOrEmpty() %>
												</td>
												<td>
													<%#: Item.Institute.ToNAIfNullOrEmpty() %>
												</td>
												<td>
													<%#: Item.BoardUniversity.ToNAIfNullOrEmpty() %>
												</td>
												<td>
													<%#: Item.PassingYear %>
												</td>
											</tr>
										</ItemTemplate>
									</asp:Repeater>
								</tbody>
							</table>
						</div>
					</ItemTemplate>
					<FooterTemplate>
						<div runat="server" visible="<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>">No record found.</div>
					</FooterTemplate>
				</asp:Repeater>
			</div>
		</div>
		<div class="panel panel-default" runat="server" visible="<%# Item.Candidate.ForeignStudent %>" id="divAcademicRecord">
			<div class="panel-heading">
				<h3 class="panel-title">Academic Records</h3>
			</div>
			<div class="panel-body">
				<aspire:AspireGridView runat="server" ID="gvCandidateForgeinAcademicRecords" Responsive="true" Caption="Academic Records" AutoGenerateColumns="false" DataSource="<%# Item.CandidateForeignAcademicRecords %>" ItemType="Aspire.Model.Entities.CandidateForeignAcademicRecord" OnRowCommand="gvCandidateForgeinAcademicRecords_OnRowCommand">
					<Columns>
						<asp:TemplateField HeaderText="#">
							<ItemTemplate>
								<%# Container.DataItemIndex + 1 %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Degree">
							<ItemTemplate>
								<%#: Item.DegreeName %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Institute">
							<ItemTemplate>
								<%#: Item.Institute %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Subjects">
							<ItemTemplate>
								<%#: Item.Subjects.ToNAIfNullOrEmpty() %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Obtained Marks">
							<ItemTemplate>
								<%#: Item.ObtainedMarks %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Total Marks">
							<ItemTemplate>
								<%#: Item.TotalMarks %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Passing Year">
							<ItemTemplate>
								<%#: Item.PassingYear %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Documents">
							<ItemTemplate>
								<aspire:AspireLinkButton runat="server" ToolTip="Download Document" CausesValidation="false" Text="Download" CommandName="Download" EncryptedCommandArgument="<%# Item.CandidateForeignAcademicRecordID %>" />
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</aspire:AspireGridView>

				<aspire:AspireGridView runat="server" ID="gvCandidateForgeinDocuments" Responsive="true" Caption="Documents" AutoGenerateColumns="false" DataSource="<%# Item.CandidateForeignDocuments %>" ItemType="Aspire.Model.Entities.CandidateForeignDocument" OnRowCommand="gvCandidateForgeinDocuments_OnRowCommand">
					<Columns>
						<asp:TemplateField HeaderText="#">
							<ItemTemplate>
								<%#: Container.DataItemIndex + 1 %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Document Type">
							<ItemTemplate>
								<%#: Item.DocumentType.GetFullName() %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Action">
							<ItemTemplate>
								<aspire:AspireLinkButton runat="server" ToolTip="Download Document" CausesValidation="false" Text="Download" CommandName="Download" EncryptedCommandArgument="<%# Item.CandidateForeignDocumentID %>" />
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</aspire:AspireGridView>
			</div>
		</div>
		<div class="panel panel-default" runat="server" visible="<%# Item.Candidate.ForeignStudent %>" id="divAppliedProgramsForeignStudent">
			<div class="panel-heading">
				<span class="panel-title">Applied Programs</span>
			</div>
			<asp:Repeater runat="server" DataSource="<%# Item.CandidateAppliedProgramsForeigner %>" ItemType="Aspire.BL.Core.Admissions.Common.ProfileInformation.CandidateDetailedProfile.CandidateAppliedProgramForeigner" ID="repeaterCandidateAppliedProgramsForeigner">
				<HeaderTemplate>
					<table class="table table-bordered table-condensed table-hover AspireGridView">
						<thead>
							<tr>
								<th>#</th>
								<th>Program</th>
								<th>Session</th>
								<th>Credit Transfer</th>
							</tr>
						</thead>
						<tbody>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td>
							<%#: Container.ItemIndex + 1 %>
						</td>
						<td>
							<strong>Institute:</strong> <%#: Item.InstituteName %>
							<br />
							<strong>Program:</strong> <%#: Item.ProgramName %> - <%#: Item.DurationEnum.ToFullName() %>
							<br />
							<strong>Eligibility Criteria:</strong> <%#: Item.EligibilityCriteriaStatement%>
							<br />
							<span runat="server" visible='<%#Item.ResearchThemeAreaName.ToNullIfEmpty()!=null%>'><strong>Research Theme Area:</strong> <%#: Item.ResearchThemeAreaName%></span>
						</td>
						<td>
							<%#: Item.ShiftEnum.ToFullNames() %>
						</td>
						<td>
							<%#: Item.CreditTransfer.ToYesNo() %>
						</td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
					<tr runat="server" visible="<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>">
						<td colspan="4">No record found.</td>
					</tr>
					</tbody>
			</table>
				</FooterTemplate>
			</asp:Repeater>
		</div>
	</ItemTemplate>
</asp:FormView>
<script type="text/javascript">
	$(function () {
		function layout(panels) {
			$("div.panel-body", panels).each(function (panelIndex, panelBody) {
				var firstRow = $("div.row", panelBody);
				var currentRow = null;
				$(">div", firstRow).each(function (i, div) {
					if ($(div).hasClass("col-md-6")) {
						if (currentRow === null)
							currentRow = $("<div></div>").addClass("row").appendTo(panelBody).append($(div));
						else {
							currentRow.append($(div));
							currentRow = null;
						}
					}
					else if ($(div).hasClass("col-md-12")) {
						$("<div></div>").addClass("row").appendTo(panelBody).append($(div));
						currentRow = null;
					}
				});
				if ($("*", firstRow).length === 0)
					firstRow.remove();
			});
		}

		layout($("div#divPersonalInformation"));
		layout($("div#divFatherInformation"));
		layout($("div#divSponsorInformation"));
		layout($("div#divAppliedProgram"));
	});
</script>
