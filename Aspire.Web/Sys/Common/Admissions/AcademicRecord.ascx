﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AcademicRecord.ascx.cs" Inherits="Aspire.Web.Sys.Common.Admissions.AcademicRecord" %>

<div class="form-horizontal">
	<div class="form-group" runat="server">
		<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Degree:" AssociatedControlID="ddlDegreeType" />
		<div class="col-md-6">
			<aspire:AspireDropDownList runat="server" ValidationGroup="ApplyProgram" ID="ddlDegreeType" AutoPostBack="True" OnSelectedIndexChanged="ddlDegreeType_SelectedIndexChanged" />
		</div>
	</div>
	<div class="form-group" id="divExaminationSystem" runat="server">
		<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Examination System:" AssociatedControlID="rblExaminationSystem" />
		<div class="col-md-6">
			<aspire:AspireRadioButtonList runat="server" ID="rblExaminationSystem" ValidationGroup="ApplyProgram" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="True" OnSelectedIndexChanged="rblExaminationSystem_SelectedIndexChanged" />
		</div>
	</div>
	<div class="form-group" id="divTotalCGPA" runat="server">
		<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Total CGPA:" AssociatedControlID="ddlTotalCGPA" />
		<div class="col-md-3">
			<aspire:AspireDropDownList runat="server" ID="ddlTotalCGPA" AutoPostBack="True" OnSelectedIndexChanged="ddlTotalCGPA_SelectedIndexChanged" />
		</div>
	</div>
	<div class="form-group" id="divObtainedCGPA" runat="server">
		<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Obtained CGPA:" AssociatedControlID="tbObtainedCGPA" />
		<div class="col-md-6">
			<aspire:AspireTextBox runat="server" ValidationGroup="ApplyProgram" ID="tbObtainedCGPA" MaxLength="8" />
		</div>
		<div class="col-md-4 form-control-static">
			<aspire:AspireDoubleValidator MinValue="0" MaxValue="5" ID="dvtbObtainedCGPA" runat="server" AllowNull="False" ValidationGroup="ApplyProgram" ControlToValidate="tbObtainedCGPA" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Number must be from {min} to {max}." />
		</div>
	</div>
	<div class="form-group" id="divObtainedMarks" runat="server">
		<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Obtained Marks:" AssociatedControlID="tbObtainedMarks" />
		<div class="col-md-6">
			<aspire:AspireTextBox runat="server" ValidationGroup="ApplyProgram" ID="tbObtainedMarks" MaxLength="8" />
		</div>
		<div class="col-md-4 form-control-static">
			<aspire:AspireUInt16Validator ID="ivtbObtainedMarks" runat="server" AllowNull="False" ValidationGroup="ApplyProgram" ControlToValidate="tbObtainedMarks" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Number must be from {min} to {max}." />
		</div>
	</div>
	<div class="form-group" id="divTotalMarks" runat="server">
		<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Total Marks:" AssociatedControlID="tbTotalMarks" />
		<div class="col-md-6">
			<aspire:AspireTextBox runat="server" ValidationGroup="ApplyProgram" ID="tbTotalMarks" MaxLength="8" />
		</div>
		<div class="col-md-4 form-control-static">
			<aspire:AspireUInt16Validator ID="ivtbTotalMarks" runat="server" AllowNull="False" ValidationGroup="ApplyProgram" ControlToValidate="tbTotalMarks" RequiredErrorMessage="This field is required." InvalidNumberInvalidRangeErrorMessage="Number must be from {min} to {max}." />
			<aspire:AspireCompareValidator ValidateEmptyText="False" runat="server" ID="cvtbTotalMarks" ErrorMessage="Obtained Marks must be less than or equal to Total Marks." ControlToValidate="tbTotalMarks" ControlToCompare="tbObtainedMarks" Type="Integer" ValidationGroup="ApplyProgram" Operator="GreaterThanEqual" />
		</div>
	</div>
	<div class="form-group" id="divPercentage" runat="server">
		<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Percentage:" AssociatedControlID="tbPercentage" />
		<div class="col-md-6">
			<aspire:AspireTextBox runat="server" ValidationGroup="ApplyProgram" ID="tbPercentage" ReadOnly="True" MaxLength="20" />
		</div>
		<div class="col-md-4 form-control-static">
			<aspire:AspireCustomValidator ValidateEmptyText="True" runat="server" ID="cvtbPercentage" ClientValidationFunction="validatePercentage" ValidationGroup="ApplyProgram" OnServerValidate="cvtbPercentage_OnServerValidate" />
		</div>
	</div>
	<div class="form-group" runat="server" id="divddlSubjects">
		<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Subjects:" AssociatedControlID="ddlSubjects" />
		<div class="col-md-6">
			<aspire:AspireDropDownList runat="server" ValidationGroup="ApplyProgram" ID="ddlSubjects" />
		</div>
	</div>
	<div class="form-group" id="divtbSubjects" runat="server">
		<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Subjects:" AssociatedControlID="tbSubjects" />
		<div class="col-md-6">
			<aspire:AspireTextBox runat="server" ValidationGroup="ApplyProgram" ID="tbSubjects" MaxLength="500" />
		</div>
		<div class="col-md-4 form-control-static">
			<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbSubjects" RequiredErrorMessage="This field is required." ValidationGroup="ApplyProgram" />
		</div>
	</div>
	<div class="form-group" id="divtbInstitute" runat="server">
		<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Institute:" AssociatedControlID="tbInstitute" />
		<div class="col-md-6">
			<aspire:AspireTextBox runat="server" ValidationGroup="ApplyProgram" ID="tbInstitute" MaxLength="500" />
		</div>
		<div class="col-md-4 form-control-static">
			<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbInstitute" RequiredErrorMessage="This field is required." ValidationGroup="ApplyProgram" />
		</div>
	</div>
	<div class="form-group" id="divtbBoardUniversity" runat="server">
		<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Board/University:" AssociatedControlID="tbBoardUniversity" />
		<div class="col-md-6">
			<aspire:AspireTextBox runat="server" ValidationGroup="ApplyProgram" ID="tbBoardUniversity" MaxLength="500" />
		</div>
		<div class="col-md-4 form-control-static">
			<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbBoardUniversity" RequiredErrorMessage="This field is required." ValidationGroup="ApplyProgram" />
		</div>
	</div>
	<div class="form-group" id="divtbPassingYear" runat="server">
		<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Passing Year:" AssociatedControlID="tbPassingYear" />
		<div class="col-md-6">
			<aspire:AspireTextBox runat="server" ValidationGroup="ApplyProgram" ID="tbPassingYear" MaxLength="4" data-inputmask="'alias': 'short'" />
		</div>
		<div class="col-md-4 form-control-static">
			<aspire:AspireInt16Validator ID="ivtbPassingYear" runat="server" AllowNull="False" ValidationGroup="ApplyProgram" ControlToValidate="tbPassingYear" RequiredErrorMessage="This field is required." InvalidNumberInvalidRangeErrorMessage="Year must be from {min} to {max}." />
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function () { autoCalculatePercentage($("#<%=this.tbObtainedMarks.ClientID%>"), $("#<%=this.tbTotalMarks.ClientID%>"), $("#<%=this.tbPercentage.ClientID%>")); });
</script>
