﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CandidateProfile.ascx.cs" Inherits="Aspire.Web.Sys.Common.Admissions.CandidateProfile" %>
<%@ Register Src="~/Sys/Common/PhotoUploadUserControl.ascx" TagPrefix="uc1" TagName="PhotoUploadUserControl" %>

<aspire:HiddenField runat="server" Mode="Encrypted" ID="hfCandidateID" />
<div class="panel panel-default" id="divPersonalInformation">
	<div class="panel-heading">
		<h3 class="panel-title">Personal Information</h3>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div style="width: 100px; margin: auto">
						<asp:Image runat="server" ID="imgPicture" Width="100%" CssClass="img-thumbnail" AlternateText="Upload Photo" />
					</div>
					<div class="text-center" style="margin-top: 10px">
						<aspire:AspireButton runat="server" Text="Browse" ID="btnBrowse" CausesValidation="False" OnClick="btnBrowse_OnClick" />
						<span runat="server" id="spanRequiredStar" style="color: red; font-size: medium;" title="This field is required.">*</span>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Name (as per SSC): <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbName" />
					<aspire:AspireTextBox ValidationGroup="Profile" TextTransform="UpperCase" runat="server" MaxLength="100" ID="tbName" />
					<aspire:AspireStringValidator ID="svtbName" runat="server" AllowNull="False" ControlToValidate="tbName" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Name." ValidationGroup="Profile" ValidationExpression="Custom" CustomValidationExpression="^[A-Z]+([\s][A-Z]+)*$" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Email: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbEmail" />
					<aspire:AspireTextBox TextTransform="LowerCase" ValidationGroup="Profile" runat="server" MaxLength="200" ID="tbEmail" TextMode="Email" ReadOnly="false" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Bahria University Registration No.:" AssociatedControlID="tbBURegistrationNo" />
					<aspire:AspireTextBox ID="tbBURegistrationNo" ValidationGroup="Profile" runat="server" MaxLength="10" />
					<aspire:AspireInt32Validator MinValue="1" ID="ivtbBURegistrationNo" runat="server" AllowNull="True" ValidationGroup="Profile" ControlToValidate="tbBURegistrationNo" InvalidNumberErrorMessage="Invalid Number." RequiredErrorMessage="This field is required." />
					<aspire:Label runat="server" CssClass="help-block" Text="This field is mandatory for Bahria University Alumni/Existing Students." />
				</div>
			</div>
			<div class="col-md-6" id="divCNIC" runat="server">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="CNIC: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbCNIC" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="15" data-inputmask="'alias': 'cnic'" ID="tbCNIC" />
					<span class="help-block">Format: 99999-9999999-9</span>
					<aspire:AspireStringValidator ID="svtbCNIC" runat="server" AllowNull="False" ValidationGroup="Profile" ControlToValidate="tbCNIC" InvalidDataErrorMessage="Invalid CNIC." RequiredErrorMessage="This field is required." ValidationExpression="CNIC" />
				</div>
			</div>
			<div class="col-md-6" id="divPassport" runat="server">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Passport No.: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbPassportNo" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbPassportNo" />
					<aspire:AspireStringValidator ID="svtbPassportNo" runat="server" AllowNull="False" ValidationGroup="Profile" ControlToValidate="tbPassportNo" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Gender: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="rblGender" />
					<div class="form-control-static">
						<aspire:AspireRadioButtonList runat="server" ID="rblGender" RepeatDirection="Horizontal" RepeatLayout="Flow" />
					</div>
					<aspire:AspireRequiredFieldValidator ID="rfvrblGender" runat="server" ControlToValidate="rblGender" ErrorMessage="This field is required." ValidationGroup="Profile" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Category: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="rblCategory" />
					<div class="form-control-static">
						<aspire:AspireRadioButtonList runat="server" ID="rblCategory" RepeatDirection="Horizontal" RepeatLayout="Flow" />
					</div>
					<aspire:AspireRequiredFieldValidator ID="rfvrblCategory" runat="server" ControlToValidate="rblCategory" ErrorMessage="This field is required." ValidationGroup="Profile" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Date of Birth: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbDOB" />
					<aspire:AspireDateTimePickerTextbox runat="server" ID="tbDOB" DisplayMode="ShortDate" ValidationGroup="Profile" />
					<aspire:AspireDateTimeValidator ID="dttbDOB" runat="server" ControlToValidate="tbDOB" InvalidDataErrorMessage="Invalid Date." AllowNull="False" RequiredErrorMessage="This field is required." ValidationGroup="Profile" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Blood Group:" AssociatedControlID="ddlBloodGroup" />
					<aspire:AspireDropDownList runat="server" ID="ddlBloodGroup" />
					<aspire:AspireRequiredFieldValidator ID="rfvddlBloodGroup" runat="server" ValidationGroup="Profile" ControlToValidate="ddlBloodGroup" ErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Mobile No: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbMobile" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" ID="tbMobile" MaxLength="50" />
					<aspire:AspireStringValidator ID="svtbMobile" runat="server" AllowNull="false" ValidationGroup="Profile" ControlToValidate="tbMobile" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Mobile No." ValidationExpression="Mobile" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Phone No. (Home): <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbPhoneHome" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbPhoneHome" />
					<aspire:AspireStringValidator ID="svtbPhoneHome" runat="server" AllowNull="false" ValidationGroup="Profile" ControlToValidate="tbPhoneHome" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Nationality: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbNationality" />
					<aspire:AspireTextBox ValidationGroup="Profile" TextTransform="UpperCase" TrimText="False" runat="server" MaxLength="50" ID="tbNationality" />
					<aspire:AspireStringValidator ID="svtbNationality" runat="server" AllowNull="false" ValidationGroup="Profile" ControlToValidate="tbNationality" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Country: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="ddlCountry" />
					<aspire:AspireDropDownList runat="server" ID="ddlCountry" />
					<aspire:AspireRequiredFieldValidator ID="rfvddlCountry" runat="server" ValidationGroup="Profile" ControlToValidate="ddlCountry" ErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="State/Province: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="ddlProvince" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Profile" ID="ddlProvince" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="ddlProvince_SelectedIndexChanged" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" ID="tbProvince" />
					<aspire:AspireStringValidator ID="svtbProvince" runat="server" AllowNull="false" ValidationGroup="Profile" ControlToValidate="tbProvince" RequiredErrorMessage="This field is required." />
					<aspire:AspireRequiredFieldValidator ID="rfvddlProvince" runat="server" ValidationGroup="Profile" ControlToValidate="ddlProvince" ErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="District/City: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="ddlDistrict" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Profile" ID="ddlDistrict" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="ddlDistrict_SelectedIndexChanged" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" ID="tbDistrict" />
					<aspire:AspireStringValidator ID="svtbDistrict" runat="server" AllowNull="false" ValidationGroup="Profile" ControlToValidate="tbDistrict" RequiredErrorMessage="This field is required." />
					<aspire:AspireRequiredFieldValidator ID="rfvddlDistrict" runat="server" ValidationGroup="Profile" ControlToValidate="ddlDistrict" ErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6" id="divTehsil" runat="server">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Tehsil: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="ddlTehsil" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Profile" ID="ddlTehsil" />
					<aspire:AspireRequiredFieldValidator ID="rfvddlTehsil" runat="server" ValidationGroup="Profile" ControlToValidate="ddlTehsil" ErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6" id="divDomicile" runat="server">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Domicile: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="ddlDomicile" />
					<aspire:AspireDropDownListWithOptionGroups runat="server" ValidationGroup="Profile" ID="ddlDomicile" />
					<aspire:AspireRequiredFieldValidator ID="rfvddlDomicile" runat="server" ValidationGroup="Profile" ControlToValidate="ddlDomicile" ErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Current Address: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbCurrentAddress" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="500" Rows="3" ID="tbCurrentAddress" TextMode="MultiLine" />
					<aspire:AspireStringValidator ID="svtbCurrentAddress" runat="server" AllowNull="false" ValidationGroup="Profile" ControlToValidate="tbCurrentAddress" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Permanent Address: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbPermanentAddress" />
					<div>
						<aspire:AspireCheckBox ID="cbSameAsCurrentAddress" runat="server" Text="Same as Current Address" />
						<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="500" Rows="3" ID="tbPermanentAddress" TextMode="MultiLine" />
					</div>
					<aspire:AspireStringValidator ID="svtbPermanentAddress" runat="server" AllowNull="false" ValidationGroup="Profile" ControlToValidate="tbPermanentAddress" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Source of Information: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="ddlSourceOfInformation" />
					<aspire:AspireDropDownList ValidationGroup="Profile" runat="server" ID="ddlSourceOfInformation" />
					<aspire:AspireRequiredFieldValidator runat="server" ID="rfvddlSourceOfInformation" ControlToValidate="ddlSourceOfInformation" ErrorMessage="This field is required." ValidationGroup="Profile" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Next of Kin: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbNextOfKin" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="200" ID="tbNextOfKin" />
					<aspire:AspireStringValidator ID="svtbNextOfKin" runat="server" AllowNull="False" ValidationGroup="Profile" ControlToValidate="tbNextOfKin" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Name." ValidationExpression="Custom" CustomValidationExpression="^[a-zA-Z]+([\s][a-zA-Z]+)*$" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Relationship with Next of Kin: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbNextOfKinRelationship" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="200" ID="tbNextOfKinRelationship" />
					<aspire:AspireStringValidator ID="svtbNextOfKinRelationship" runat="server" AllowNull="False" ValidationGroup="Profile" ControlToValidate="tbNextOfKinRelationship" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid value." ValidationExpression="Custom" CustomValidationExpression="^[a-zA-Z]+([\s][a-zA-Z]+)*$" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Emergency Contact Name: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbEmergencyContactName" />
					<aspire:AspireTextBox ValidationGroup="Profile" TextTransform="UpperCase" runat="server" MaxLength="50" ID="tbEmergencyContactName" />
					<aspire:AspireStringValidator ID="svtbEmergencyContactName" runat="server" AllowNull="false" ValidationGroup="Profile" ControlToValidate="tbEmergencyContactName" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Name." ValidationExpression="Custom" CustomValidationExpression="^[A-Z]+([\s][A-Z]+)*$" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Emergency Mobile: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbEmergencyMobile" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" ID="tbEmergencyMobile" MaxLength="50" />
					<aspire:AspireStringValidator ID="svtbEmergencyMobile" runat="server" AllowNull="false" ValidationGroup="Profile" ControlToValidate="tbEmergencyMobile" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Emergency Phone: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbEmergencyPhone" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbEmergencyPhone" />
					<aspire:AspireStringValidator ID="svtbEmergencyPhone" runat="server" AllowNull="False" ValidationGroup="Profile" ControlToValidate="tbEmergencyPhone" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Do you have ETS (SAT, LAT, GRE, NTS-GAT(General/Subject)) Scores?" AssociatedControlID="rblETSScorer" />
					<div class="form-control-static">
						<aspire:AspireRadioButtonList ValidationGroup="Profile" runat="server" ID="rblETSScorer" RepeatLayout="Flow" RepeatDirection="Horizontal" />
					</div>
					<aspire:AspireRequiredFieldValidator runat="server" ID="rfvrblETSScorer" ControlToValidate="rblETSScorer" ErrorMessage="This field is required." ValidationGroup="Profile" />	
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Religion:" AssociatedControlID="ddlReligion" />
					<div class="input-group">
						<div class="input-group-btn">
							<aspire:DropDownList CssClass="btn" runat="server" ID="ddlReligion">
								<asp:ListItem Text="Islam" Value="Islam" />
								<asp:ListItem Text="Christian" Value="Christian" />
								<asp:ListItem Text="Hindu" Value="Hindu" />
								<asp:ListItem Text="Other" Value="" />
							</aspire:DropDownList>
						</div>
						<aspire:AspireTextBox ValidationGroup="Profile" runat="server" ID="tbReligion" MaxLength="200" PlaceHolder="Religion" />
					</div>
					<aspire:AspireCustomValidator ID="cvtbReligion" runat="server" ValidationGroup="Profile" ControlToValidate="tbReligion" ClientValidationFunction="validateReligion" ValidateEmptyText="True" OnServerValidate="cvReligion_ServerValidate" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Physical Disability:" AssociatedControlID="ddlPhysicalDisability" />
					<div class="input-group">
						<div class="input-group-btn">
							<aspire:DropDownList CssClass="btn" runat="server" ID="ddlPhysicalDisability" />
						</div>
						<aspire:AspireTextBox ValidationGroup="Profile" runat="server" ID="tbPhysicalDisability" MaxLength="200" PlaceHolder="Physical Disability Description" />
					</div>
					<aspire:AspireCustomValidator ID="cvtbPhysicalDisability" runat="server" ValidationGroup="Profile" ControlToValidate="tbPhysicalDisability" ClientValidationFunction="validatePhysicalDisability" ValidateEmptyText="True" OnServerValidate="cvtbPhysicalDisability_OnServerValidate" />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divAreaType">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Area Type: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="rblAreaType" />
					<div class="form-control-static">
						<aspire:AspireRadioButtonList runat="server" ValidationGroup="Profile" ID="rblAreaType" RepeatLayout="Flow" RepeatDirection="Horizontal" />
					</div>
					<aspire:AspireRequiredFieldValidator ID="rfvrblAreaType" runat="server" ControlToValidate="rblAreaType" ErrorMessage="This field is required." ValidationGroup="Profile" />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divServiceNo">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Service No. (If Naval):" AssociatedControlID="tbServiceNo" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbServiceNo" />
					<aspire:AspireStringValidator ID="svtbServiceNo" runat="server" AllowNull="True" ValidationGroup="Profile" ControlToValidate="tbServiceNo" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Is your Father/Mother advocate by profession:" AssociatedControlID="tbPakistanBarCouncilNo" />
					<div class="input-group">
						<div class="input-group-btn">
							<aspire:DropDownList CssClass="btn" runat="server" ID="ddlPakistanBarCouncilNo" />
						</div>
						<aspire:AspireTextBox ValidationGroup="Profile" runat="server" ID="tbPakistanBarCouncilNo" MaxLength="9" PlaceHolder="Provide Pakistan Bar Councile Number." />
					</div>
					<aspire:AspireCustomValidator ID="cvtbPakistanBarCouncilNo" runat="server" ValidationGroup="Profile" ControlToValidate="tbPakistanBarCouncilNo" ClientValidationFunction="validatePakistanBarCouncilNo" ValidateEmptyText="True" OnServerValidate="cvtbPakistanBarCouncilNo_OnServerValidate" />
				</div>
			</div>
			<div class="col-md-6" id="divNTNOrCNIC" runat="server">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="NTN/CNIC of self/parents/guardian/spouse etc. to whom name tax be deposited by BU in FBR: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbNTNOrCNIC" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbNTNOrCNIC" data-inputmask="'alias': 'ntncnic'" />
					<span class="help-block">NTN Format: 9999999-9, CNIC Format: 99999-9999999-9</span>
					<aspire:AspireStringValidator runat="server" ID="svtbNTNOrCNIC" AllowNull="False" ValidationGroup="Profile" ControlToValidate="tbNTNOrCNIC" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid string." ValidationExpression="NTNCNIC" />
				</div>
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default" id="divFatherInformation">
	<div class="panel-heading">
		<h3 class="panel-title">Father's Information</h3>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Father's  Name (As per SSC): <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbFatherName" />
					<aspire:AspireTextBox TextTransform="UpperCase" ValidationGroup="Profile" runat="server" MaxLength="100" ID="tbFatherName" />
					<aspire:AspireStringValidator ID="svtbFatherName" runat="server" AllowNull="False" ValidationGroup="Profile" ControlToValidate="tbFatherName" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Name." ValidationExpression="Custom" CustomValidationExpression="^[A-Z]+([\s][A-Z]+)*$" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Is your Father Alive: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="rblFatherAlive" />
					<div class="form-control-static">
						<aspire:AspireRadioButtonList runat="server" ID="rblFatherAlive" RepeatDirection="Horizontal" RepeatLayout="Flow" ValidationGroup="Profile"  AutoPostBack="True" OnSelectedIndexChanged="rblFatherAlive_OnSelectedIndexChanged"/>
					</div>
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblFatherAlive" ErrorMessage="This field is required." ValidationGroup="Profile" />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divFatherCNIC">
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblFatherCNIC" Text="Father's CNIC No.: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbFatherCNIC" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="15" data-inputmask="'alias': 'cnic'" ID="tbFatherCNIC" />
					<aspire:AspireStringValidator ID="svtbFatherCNIC" runat="server" AllowNull="False" ValidationGroup="Profile" ControlToValidate="tbFatherCNIC" InvalidDataErrorMessage="Invalid CNIC." RequiredErrorMessage="This field is required." ValidationExpression="CNIC" />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divFatherPassport">
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblFatherPassport" Text="Father's Passport No.: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbFatherPassport" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="100" ID="tbFatherPassport" />
					<aspire:AspireStringValidator ID="svtbFatherPassport" runat="server" AllowNull="False" ValidationGroup="Profile" ControlToValidate="tbFatherPassport" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblFatherDesignation" Text="Father's Designation/Profession: <span style='color:red;' title='This field is required.'>*</span>" AssociatedControlID="tbFatherDesignation" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="100" ID="tbFatherDesignation" />
					<aspire:AspireStringValidator ID="svtbFatherDesignation" runat="server" AllowNull="False" ValidationGroup="Profile" ControlToValidate="tbFatherDesignation" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid string." ValidationExpression="Custom" CustomValidationExpression="^[a-zA-Z]+([\s][a-zA-Z]+)*$" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Father's Department/Unit:" AssociatedControlID="tbFatherDepartment" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="100" ID="tbFatherDepartment" />
					<aspire:AspireStringValidator ID="svtbFatherDepartment" runat="server" AllowNull="true" ValidationGroup="Profile" ControlToValidate="tbFatherDepartment" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid string." ValidationExpression="Custom" CustomValidationExpression="^[a-zA-Z]+([\s][a-zA-Z]+)*$" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Father's Organization:" AssociatedControlID="tbFatherOrganization" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="100" ID="tbFatherOrganization" />
					<aspire:AspireStringValidator ID="svtbFatherOrganization" runat="server" AllowNull="true" ValidationGroup="Profile" ControlToValidate="tbFatherOrganization" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid string." ValidationExpression="Custom" CustomValidationExpression="^[a-zA-Z]+([\s][a-zA-Z]+)*$" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Father's Mobile No:" AssociatedControlID="tbFatherMobile" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" ID="tbFatherMobile" MaxLength="50" />
					<aspire:AspireStringValidator ID="svtbFatherMobile" runat="server" AllowNull="true" ValidationGroup="Profile" ControlToValidate="tbFatherMobile" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Father's Phone No. (Office):" AssociatedControlID="tbFatherOfficePhone" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbFatherOfficePhone" />
					<aspire:AspireStringValidator ID="svtbFatherOfficePhone" runat="server" AllowNull="true" ValidationGroup="Profile" ControlToValidate="tbFatherOfficePhone" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Father's Phone No. (Home):" AssociatedControlID="tbFatherHomePhone" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbFatherHomePhone" />
					<aspire:AspireStringValidator ID="svtbFatherHomePhone" runat="server" AllowNull="true" ValidationGroup="Profile" ControlToValidate="tbFatherHomePhone" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Father's Fax:" AssociatedControlID="tbFatherFax" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbFatherFax" />
					<aspire:AspireStringValidator ID="svtbFatherFax" runat="server" AllowNull="True" ValidationGroup="Profile" ControlToValidate="tbFatherFax" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divFatherServiceNo">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Father's / Mother's Service No. (If Naval):" AssociatedControlID="tbFatherServiceNo" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbFatherServiceNo" />
					<aspire:AspireStringValidator ID="svtbFatherServiceNo" runat="server" AllowNull="true" ValidationGroup="Profile" ControlToValidate="tbFatherServiceNo" RequiredErrorMessage="This field is required." />
				</div>
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default" id="divSponsorInformation">
	<div class="panel-heading">
		<h3 class="panel-title">Sponsor Information</h3>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Sponsored By:" AssociatedControlID="ddlSponsoredBy" />
					<aspire:AspireDropDownList runat="server" CausesValidation="false" ID="ddlSponsoredBy" AutoPostBack="True" OnSelectedIndexChanged="ddlSponsoredBy_OnSelectedIndexChanged" />
					<aspire:AspireRequiredFieldValidator runat="server" ID="rfvddlSponsoredBy" ControlToValidate="ddlSponsoredBy" ValidationGroup="Profile" ErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divSponsorName">
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblSponsorName" Text="Sponsor's Name:" AssociatedControlID="tbSponsorName" />
					<aspire:AspireTextBox TextTransform="UpperCase" ValidationGroup="Profile" runat="server" MaxLength="100" ID="tbSponsorName" />
					<aspire:AspireStringValidator ID="svtbSponsorName" runat="server" ValidationGroup="Profile" ControlToValidate="tbSponsorName" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Name." ValidationExpression="Custom" CustomValidationExpression="^[A-Z]+([\s][A-Z]+)*$" />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divSponsorCNIC">
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblSponsorSponsorCNIC" Text="Sponsor's CNIC No.:" AssociatedControlID="tbSponsorCNIC" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="15" ID="tbSponsorCNIC" data-inputmask="'alias': 'cnic'" />
					<aspire:AspireStringValidator ID="svtbSponsorCNIC" runat="server" AllowNull="True" ValidationGroup="Profile" ControlToValidate="tbSponsorCNIC" InvalidDataErrorMessage="Invalid CNIC." RequiredErrorMessage="This field is required." ValidationExpression="CNIC" />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divSponsorPassportNo">
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblSponsorPassportNo" Text="Sponsor's Passport No.:" AssociatedControlID="tbSponsorPassportNo" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbSponsorPassportNo" />
					<aspire:AspireStringValidator ID="svtbSponsorPassportNo" runat="server" ValidationGroup="Profile" ControlToValidate="tbSponsorPassportNo" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divSponsorRelationshipWithGuardian">
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblSponsorRelationshipWithGuardian" Text="Relationship with Sponsor:" AssociatedControlID="tbSponsorRelationshipWithGuardian" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbSponsorRelationshipWithGuardian" />
					<aspire:AspireStringValidator ID="svtbSponsorRelationshipWithGuardian" runat="server" ValidationGroup="Profile" ControlToValidate="tbSponsorRelationshipWithGuardian" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid string." ValidationExpression="Custom" CustomValidationExpression="^[a-zA-Z]+([\s][a-zA-Z]+)*$" />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divSponsorDesignation">
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblSponsorDesignation" Text="Sponsor's Designation:" AssociatedControlID="tbSponsorDesignation" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbSponsorDesignation" />
					<aspire:AspireStringValidator ID="svtbSponsorDesignation" runat="server" ValidationGroup="Profile" ControlToValidate="tbSponsorDesignation" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid string." ValidationExpression="Custom" CustomValidationExpression="^[a-zA-Z]+([\s][a-zA-Z]+)*$" />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divSponsorDepartment">
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblSponsorrDepartment" Text="Sponsor's Department:" AssociatedControlID="tbSponsorDepartment" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbSponsorDepartment" />
					<aspire:AspireStringValidator ID="svtbSponsorDepartment" runat="server" ValidationGroup="Profile" ControlToValidate="tbSponsorDepartment" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid string." ValidationExpression="Custom" CustomValidationExpression="^[a-zA-Z]+([\s][a-zA-Z]+)*$" />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divSponsorOrganization">
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblSponsorOrganization" Text="Sponsor's Organization:" AssociatedControlID="tbSponsorOrganization" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbSponsorOrganization" />
					<aspire:AspireStringValidator ID="svtbSponsorOrganization" runat="server" ValidationGroup="Profile" ControlToValidate="tbSponsorOrganization" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid string." ValidationExpression="Custom" CustomValidationExpression="^[a-zA-Z]+([\s][a-zA-Z]+)*$" />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divSponsorMobile">
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblSponsorMobile" Text="Sponsor's Mobile No:" AssociatedControlID="tbSponsorMobile" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" ID="tbSponsorMobile" MaxLength="50" />
					<aspire:AspireStringValidator ID="svtbSponsorMobile" runat="server" ValidationGroup="Profile" ControlToValidate="tbSponsorMobile" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divSponsorPhoneOffice">
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblSponsorPhoneOffice" Text="Sponsor's Phone No. (Office):" AssociatedControlID="tbSponsorPhoneOffice" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbSponsorPhoneOffice" />
					<aspire:AspireStringValidator ID="svtbSponsorPhoneOffice" runat="server" ValidationGroup="Profile" ControlToValidate="tbSponsorPhoneOffice" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divSponsorPhoneHome">
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblSponsorPhoneHome" Text="Sponsor's Phone No. (Home):" AssociatedControlID="tbSponsorPhoneHome" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbSponsorPhoneHome" />
					<aspire:AspireStringValidator ID="svtbSponsorPhoneHome" runat="server" ValidationGroup="Profile" ControlToValidate="tbSponsorPhoneHome" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divSponsorFax">
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblSponsorFax" Text="Sponsor's Fax:" AssociatedControlID="tbSponsorFax" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbSponsorFax" />
					<aspire:AspireStringValidator ID="svtbSponsorFax" runat="server" ValidationGroup="Profile" ControlToValidate="tbSponsorFax" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divSponsorServiceNo">
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblSponsorServiceNo" Text="Sponsor's Service No. (If Naval):" AssociatedControlID="tbSponsorServiceNo" />
					<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbSponsorServiceNo" />
					<aspire:AspireStringValidator ID="svtbSponsorServiceNo" runat="server" ValidationGroup="Profile" ControlToValidate="tbSponsorServiceNo" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divAnnualIncome">
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblAnnualIncome" Text="Annual Income:" AssociatedControlID="tbAnnualIncome" />
					<div class="input-group">
						<span class="input-group-addon">Rs.</span>
						<aspire:AspireTextBox ValidationGroup="Profile" runat="server" MaxLength="50" ID="tbAnnualIncome" />
					</div>
					<aspire:AspireInt32Validator runat="server" MinValue="0" ID="ivtbAnnualIncome" ValidationGroup="Profile" ControlToValidate="tbAnnualIncome" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid value." InvalidRangeErrorMessage="Invalid range." AllowNull="False" />
				</div>
			</div>
		</div>
	</div>
</div>

<div class="form-group text-center">
	<aspire:AspireButton runat="server" ID="btnSave" ButtonType="Primary" ValidationGroup="Profile" Text="Save" OnClick="btnSave_OnClick" />
	<aspire:AspireHyperLink runat="server" Text="Cancel" ID="hlCancel" />
</div>
<uc1:PhotoUploadUserControl runat="server" ID="photoUploadUserControl" OnSave="photoUploadUserControl_OnSave" />

<script type="text/javascript">
	function validatePhysicalDisability(source, args) {
		var tbPhysicalDisability = $("#<%=this.tbPhysicalDisability.ClientID%>");
			var ddlPhysicalDisability = $("#<%=this.ddlPhysicalDisability.ClientID%>");
		var physicalDisability = tbPhysicalDisability.val().trim();
		if (physicalDisability.toLowerCase() === "N/A".toLowerCase())
			physicalDisability = "";

		if (ddlPhysicalDisability.val() === "1") {
			args.IsValid = physicalDisability.length > 0;
			if (!args.IsValid)
				source.errormessage = "This field is required.";
			else
				source.errormessage = "";
		}
		else {
			args.IsValid = physicalDisability.length === 0;
			if (!args.IsValid)
				source.errormessage = "Invalid Value.";
			else
				source.errormessage = "";
		}
		ApplyHighlightCss(source, source.controltovalidate, args);
	}

	function validatePakistanBarCouncilNo(source, args) {
		var tbPakistanBarCouncilNo = $("#<%=this.tbPakistanBarCouncilNo.ClientID%>");
			var ddlPakistanBarCouncilNo = $("#<%=this.ddlPakistanBarCouncilNo.ClientID%>");
		var pakistanBarCouncilNo = tbPakistanBarCouncilNo.val().trim();
		if (pakistanBarCouncilNo.toLowerCase() === "N/A".toLowerCase())
			pakistanBarCouncilNo = "";

		if (ddlPakistanBarCouncilNo.val() === "1") {
			if (pakistanBarCouncilNo.tryToInt() != null && pakistanBarCouncilNo.toInt() > 0) {
				args.IsValid = true;
				source.errormessage = "";
			}
			else if (pakistanBarCouncilNo === null || pakistanBarCouncilNo === "") {
				args.IsValid = false;
				source.errormessage = "This field is required.";
			}
			else {
				args.IsValid = false;
				source.errormessage = "Invalid number.";
			}
		}
		else {
			args.IsValid = pakistanBarCouncilNo.length === 0;
			if (!args.IsValid)
				source.errormessage = "Invalid Value.";
			else
				source.errormessage = "";
		}
		ApplyHighlightCss(source, source.controltovalidate, args);
	}

	function validateReligion(source, args) {
		var ddlReligion = $("#<%=this.ddlReligion.ClientID%>");
			var tbReligion = $("#<%=this.tbReligion.ClientID%>");
		var religion = tbReligion.val().trim();

		if (ddlReligion.val() === "") {
			args.IsValid = religion.length > 0;
			source.errormessage = "This field is required.";
		}
		else {
			args.IsValid = true;
			source.errormessage = "";
		}
		ApplyHighlightCss(source, source.controltovalidate, args);
	}

	$(function () {
		function initCandidateProfilePage() {
			var tbPhysicalDisability = $("#<%=this.tbPhysicalDisability.ClientID%>");
				var tbPhysicalDisabilityReadonly = tbPhysicalDisability.prop("readonly");
				var ddlPhysicalDisability = $("#<%=this.ddlPhysicalDisability.ClientID%>");
				var tbPakistanBarCouncilNo = $("#<%=this.tbPakistanBarCouncilNo.ClientID%>");
				var tbPakistanBarCouncilNoReadonly = tbPakistanBarCouncilNo.prop("readonly");
				var ddlPakistanBarCouncilNo = $("#<%=this.ddlPakistanBarCouncilNo.ClientID%>");
				var ddlReligion = $("#<%=this.ddlReligion.ClientID%>");
				var tbReligion = $("#<%=this.tbReligion.ClientID%>");
				var tbReligionReadonly = tbReligion.prop("readonly");
				ddlPhysicalDisability.change(function () {
					if (ddlPhysicalDisability.val() === "1") {
						$(tbPhysicalDisability).prop("readonly", tbPhysicalDisabilityReadonly || false);
						if ($(tbPhysicalDisability).val().toLowerCase() === "n/a" || $(tbPhysicalDisability).val().toLowerCase() === "na")
							$(tbPhysicalDisability).val("").change();
					}
					else {
						$(tbPhysicalDisability).prop("readonly", true).val("N/A").change();
					}
				}).change();

				ddlPakistanBarCouncilNo.change(function () {
					if (ddlPakistanBarCouncilNo.val() === "1") {
						$(tbPakistanBarCouncilNo).prop("readonly", tbPakistanBarCouncilNoReadonly || false);
						if ($(tbPakistanBarCouncilNo).val().toLowerCase() === "n/a" || $(tbPakistanBarCouncilNo).val().toLowerCase() === "na")
							$(tbPakistanBarCouncilNo).val("").change();
					}
					else {
						$(tbPakistanBarCouncilNo).prop("readonly", true).val("N/A").change();
					}
				}).change();

				ddlReligion.change(function () {
					if (ddlReligion.val() === "") {
						$(tbReligion).prop("readonly", tbReligionReadonly || false);
						if ($(tbReligion).val().toLowerCase() === "n/a" || $(tbReligion).val().toLowerCase() === "na")
							$(tbReligion).val("").change();
					}
					else {
						$(tbReligion).prop("readonly", true).val("N/A").change();
					}
				}).change();

				var tbCurrentAddress = $("#<%=this.tbCurrentAddress.ClientID%>");
				var tbPermanentAddress = $("#<%=this.tbPermanentAddress.ClientID%>");
				var cbSameAsCurrentAddress = $("#<%=this.cbSameAsCurrentAddress.ClientID%>").change(function () {
				tbCurrentAddress.trigger("input");
			});
			tbCurrentAddress.on("input propertychange paste", function () {
				if ($(cbSameAsCurrentAddress).is(":checked"))
					tbPermanentAddress.prop("readonly", true).val(tbCurrentAddress.val());
				else
					tbPermanentAddress.prop("readonly", false);
			});
			tbCurrentAddress.trigger("input");
		}

		function layout(panel) {
			var panelBody = $(".panel-body", panel);
			var firstRow = $("div.row", panelBody);
			var currentRow = null;
			$(">div", firstRow).each(function (i, div) {
				if ($(div).hasClass("col-md-6")) {
					if (currentRow === null)
						currentRow = $("<div></div>").addClass("row").appendTo(panelBody).append($(div));
					else {
						currentRow.append($(div));
						currentRow = null;
					}
				}
				else if ($(div).hasClass("col-md-12")) {
					$("<div></div>").addClass("row").appendTo(panelBody).append($(div));
					currentRow = null;
				}
			});
			if ($("*", firstRow).length === 0)
				firstRow.remove();
		}

		layout($("div#divPersonalInformation"));
		layout($("div#divFatherInformation"));
		layout($("div#divSponsorInformation"));
		initCandidateProfilePage();
		Sys.Application.add_load(initCandidateProfilePage);
	});
</script>
