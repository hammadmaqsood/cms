﻿using Aspire.BL.Core.Admissions.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities;
using Aspire.Web.Common;
using System;
using System.Web;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Common.Admissions
{
	public partial class CandidateDetailedProfile : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
		}

		public ProfileInformation.CandidateDetailedProfile LoadCandidateInfo(int candidateID, Guid loginSessionGuid)
		{
			var profile = ProfileInformation.GetCandidateDetailedProfile(candidateID, loginSessionGuid);
			if (profile != null)
			{
				this.fvCandidate.DataSource = new[] { profile };
				this.fvCandidate.DataBind();
			}
			return profile;
		}

		protected string GetInterviewDates(AdmissionOpenProgram admissionOpenProgram)
		{
			return admissionOpenProgram?.InterviewsStartDate.ConcatenateDates(admissionOpenProgram?.InterviewsEndDate, "D", " to ");
		}

		protected void gvCandidateForgeinAcademicRecords_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Download":
					var candidateForeignAcademicRecordID = e.DecryptedCommandArgument().ToGuid();
					var result = BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.DownloadCandidateForeignAcademicRecord(candidateForeignAcademicRecordID, AspireIdentity.Current.LoginSessionGuid);
					if (result == null)
					{
						switch (AspireIdentity.Current.UserType)
						{

							case Model.Entities.Common.UserTypes.Staff:
								HttpContext.Current.Response.Redirect(BasePage.GetPageUrl<Staff.Dashboard>(), true);
								return;
							case Model.Entities.Common.UserTypes.Candidate:
								HttpContext.Current.Response.Redirect(BasePage.GetPageUrl<Candidate.Dashboard>(), true);
								return;
							default:
								throw new NotImplementedEnumException(AspireIdentity.Current.UserType);
						}
					}
					this.Response.Clear();
					this.Response.AddHeader("Content-Disposition", $"attachment; filename=\"{result.Value.systemFile.FileName}\"");
					this.Response.AddHeader("Content-Length", result.Value.systemFile.FileSize.ToString());
					this.Response.ContentType = result.Value.systemFile.MimeType;
					this.Response.BinaryWrite(result.Value.fileBytes);
					return;
			}
		}

		private void DownloadAdditionalDocument(Guid candidateForeignDocumentID)
		{
			var result = BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.DownloadCandidateForeignDocument(candidateForeignDocumentID, AspireIdentity.Current.LoginSessionGuid);
			if (result == null)
			{
				switch (AspireIdentity.Current.UserType)
				{

					case Model.Entities.Common.UserTypes.Staff:
						HttpContext.Current.Response.Redirect(BasePage.GetPageUrl<Staff.Dashboard>(), true);
						return;
					case Model.Entities.Common.UserTypes.Candidate:
						HttpContext.Current.Response.Redirect(BasePage.GetPageUrl<Candidate.Dashboard>(), true);
						return;
					default:
						throw new NotImplementedEnumException(AspireIdentity.Current.UserType);
				}
			}
			this.Response.Clear();
			this.Response.AddHeader("Content-Disposition", $"attachment; filename=\"{result.Value.systemFile.FileName}\"");
			this.Response.AddHeader("Content-Length", result.Value.systemFile.FileSize.ToString());
			this.Response.ContentType = result.Value.systemFile.MimeType;
			this.Response.BinaryWrite(result.Value.fileBytes);
			return;
		}

		protected void gvCandidateForgeinDocuments_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Download":
					var candidateForeignDocumentID = e.DecryptedCommandArgument().ToGuid();
					this.DownloadAdditionalDocument(candidateForeignDocumentID);
					break;
			}
		}
	}
}