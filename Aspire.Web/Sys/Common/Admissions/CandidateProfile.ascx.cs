﻿using Aspire.BL.Core.Admissions.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Sys.Staff.Admissions;
using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Common.Admissions
{
	public partial class CandidateProfile : System.Web.UI.UserControl
	{
		public new AspireBasePage Page => base.Page as AspireBasePage;
		private const string Pakistani = "PAKISTANI";
		private const string Pakistan = "Pakistan";
		private int? CandidateID
		{
			get
			{
				switch (this.Page.AspireIdentity.UserType)
				{
					case UserTypes.Staff:
						return this.Request.GetParameterValue<int>(nameof(this.CandidateID));
					case UserTypes.Candidate:
						return CandidateIdentity.Current.CandidateID;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		private bool ForeignStudent
		{
			get => this.ViewState[nameof(this.ForeignStudent)] as bool? == true;
			set => this.ViewState[nameof(this.ForeignStudent)] = value;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.CandidateID == null)
				{
					BasePage.Redirect<Sys.Staff.Admissions.SearchCandidate>();
					return;
				}

				var candidate = Aspire.BL.Core.Admissions.Common.ProfileInformation.GetCandidate(this.CandidateID.Value, this.Page.AspireIdentity.LoginSessionGuid);
				if (candidate == null)
				{
					BasePage.Redirect<Sys.Staff.Admissions.SearchCandidate>();
					return;
				}
				this.ForeignStudent = candidate.ForeignStudent;
				switch (AspireIdentity.Current.UserType)
				{
					case UserTypes.Staff:
						this.hlCancel.NavigateUrl = typeof(Sys.Staff.Admissions.SearchCandidate).GetAspirePageAttribute().PageUrl;
						break;
					case UserTypes.Candidate:
						this.hlCancel.NavigateUrl = typeof(Sys.Candidate.Dashboard).GetAspirePageAttribute().PageUrl;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				this.hfCandidateID.Value = candidate.CandidateID.ToString();
				this.imgPicture.ImageUrl = CandidatePicture.GetImageUrl(candidate.CandidateID, TimeSpan.Zero);
				this.btnBrowse.Visible = this.spanRequiredStar.Visible = candidate.CanEdit(nameof(candidate.PictureSystemFolderID));
				this.rblFatherAlive.FillYesNo().SetSelectedValue(candidate.IsFatherAlive);
				this.rblFatherAlive_OnSelectedIndexChanged(null, null);

				void SetTextBox(string fieldName, AspireTextBox tb, string value, params Aspire.Lib.WebControls.CustomValidator[] validators)
				{
					tb.Text = value;
					var canEdit = candidate.CanEdit(fieldName);
					tb.ReadOnly = !canEdit;
					validators?.ForEach(v => v.Enabled = canEdit);
				}
				void SetDateTimePicker(string fieldName, AspireDateTimePickerTextbox tb, DateTime? value, params Aspire.Lib.WebControls.CustomValidator[] validators)
				{
					tb.SelectedDate = value;
					var canEdit = candidate.CanEdit(fieldName);
					tb.ReadOnly = !canEdit;
					validators?.ForEach(v => v.Enabled = canEdit);
				}
				void SetListControlString(string fieldName, System.Web.UI.WebControls.ListControl rbl, string value, params Aspire.Lib.WebControls.CustomValidator[] validators)
				{
					rbl.SelectedValue = value;
					var canEdit = candidate.CanEdit(fieldName);
					rbl.Enabled = canEdit;
					validators?.ForEach(v => v.Enabled = canEdit);
				}
				void SetListControlEnum<T>(string fieldName, System.Web.UI.WebControls.ListControl rbl, T? value, params Aspire.Lib.WebControls.CustomValidator[] validators) where T : struct, IConvertible
				{
					rbl.SetEnumValue(value);
					var canEdit = candidate.CanEdit(fieldName);
					rbl.Enabled = canEdit;
					validators?.ForEach(v => v.Enabled = canEdit);
				}
				void SetListControlBoolean(string fieldName, System.Web.UI.WebControls.ListControl rbl, bool value, params Aspire.Lib.WebControls.CustomValidator[] validators)
				{
					rbl.SetBooleanValue(value);
					var canEdit = candidate.CanEdit(fieldName);
					rbl.Enabled = canEdit;
					validators?.ForEach(v => v.Enabled = canEdit);
				}

				#region Personal Information

				SetTextBox(nameof(candidate.Name), this.tbName, candidate.Name, this.svtbName);
				SetTextBox(nameof(candidate.Email), this.tbEmail, candidate.Email);

				SetTextBox(nameof(candidate.BURegistrationNo), this.tbBURegistrationNo, candidate.BURegistrationNo.ToString(), this.ivtbBURegistrationNo);
				SetTextBox(nameof(candidate.CNIC), this.tbCNIC, candidate.CNIC, this.svtbCNIC);
				this.divCNIC.Visible = !candidate.ForeignStudent;
				SetTextBox(nameof(candidate.PassportNo), this.tbPassportNo, candidate.PassportNo, this.svtbPassportNo);
				this.divPassport.Visible = candidate.ForeignStudent;
				this.svtbPassportNo.AllowNull = !candidate.ForeignStudent;

				this.rblGender.FillGenders();
				SetListControlEnum(nameof(candidate.Gender), this.rblGender, candidate.GenderEnum, this.rfvrblGender);
				if (candidate.ForeignStudent)
					candidate.CategoryEnum = candidate.CategoryEnum ?? Categories.Others;
				this.rblCategory.FillCategories();
				SetListControlEnum(nameof(candidate.Category), this.rblCategory, candidate.CategoryEnum, this.rfvrblCategory);
				if (candidate.ForeignStudent)
					this.rblCategory.Enabled = false;

				SetDateTimePicker(nameof(candidate.DOB), this.tbDOB, candidate.DOB, this.dttbDOB);
				this.ddlBloodGroup.FillBloodGroups();
				SetListControlEnum(nameof(candidate.BloodGroup), this.ddlBloodGroup, candidate.BloodGroupEnum, this.rfvddlBloodGroup);

				SetTextBox(nameof(candidate.Mobile), this.tbMobile, candidate.Mobile, this.svtbMobile);
				if (candidate.ForeignStudent)
				{
					this.tbMobile.Attributes.Add("data-inputmask", "'alias': 'mobileInternational'");
					this.svtbMobile.ValidationExpression = StringValidator.ValidationExpressions.MobileInternational;
				}
				else
				{
					this.tbMobile.Attributes.Add("data-inputmask", "'alias': 'mobilePK'");
					this.svtbMobile.ValidationExpression = StringValidator.ValidationExpressions.MobilePK;
				}
				SetTextBox(nameof(candidate.Phone), this.tbPhoneHome, candidate.Phone, this.svtbPhoneHome);
				if (candidate.ForeignStudent)
				{
					this.tbPhoneHome.Attributes.Add("data-inputmask", "'alias': 'phoneInternational'");
					this.svtbPhoneHome.ValidationExpression = StringValidator.ValidationExpressions.PhoneInternational;
				}
				else
				{
					this.tbPhoneHome.Attributes.Add("data-inputmask", "'alias': 'phonePK'");
					this.svtbPhoneHome.ValidationExpression = StringValidator.ValidationExpressions.PhonePK;
				}
				SetTextBox(nameof(candidate.Nationality), this.tbNationality, candidate.Nationality, this.svtbNationality);
				if (!candidate.ForeignStudent)
				{
					this.tbNationality.Text = Pakistani;
					this.tbNationality.ReadOnly = true;
				}
				else
				{
					this.svtbNationality.ValidationExpression = StringValidator.ValidationExpressions.Custom;
					this.svtbNationality.InvalidDataErrorMessage = "Invalid Nationality.";
					this.svtbNationality.CustomValidationExpression = "^[A-Z]+([\\s][A-Z]+)*$";
				}

				this.ddlCountry.FillCountries(CommonListItems.Select);
				if (!candidate.ForeignStudent)
				{
					SetListControlString(nameof(candidate.Country), this.ddlCountry, candidate.Country, this.rfvddlCountry);
					this.ddlCountry.SelectedValue = candidate.Country.ToNullIfWhiteSpace() ?? Pakistan;
					this.ddlCountry.Enabled = false;
				}
				else
				{
					this.ddlCountry.Items.Remove(Pakistan);
					SetListControlString(nameof(candidate.Country), this.ddlCountry, candidate.Country, this.rfvddlCountry);
				}

				if (candidate.ForeignStudent)
				{
					SetTextBox(nameof(candidate.Province), this.tbProvince, candidate.Province, this.svtbProvince);
					this.svtbProvince.InvalidDataErrorMessage = "Invalid string.";
					this.svtbProvince.ValidationExpression = StringValidator.ValidationExpressions.Custom;
					this.svtbProvince.CustomValidationExpression = "^[a-zA-Z]+([\\s][a-zA-Z]+)*$";

					this.ddlProvince.Visible = false;
					this.ddlProvince.Items.Clear();
					this.rfvddlProvince.Enabled = false;

					SetTextBox(nameof(candidate.District), this.tbDistrict, candidate.District, this.svtbDistrict);
					this.svtbDistrict.InvalidDataErrorMessage = "Invalid string.";
					this.svtbDistrict.ValidationExpression = StringValidator.ValidationExpressions.Custom;
					this.svtbDistrict.CustomValidationExpression = "^[a-zA-Z]+([\\s][a-zA-Z]+)*$";

					this.ddlDistrict.Visible = false;
					this.ddlDistrict.Items.Clear();
					this.rfvddlDistrict.Enabled = false;

					this.divTehsil.Visible = false;
					this.divDomicile.Visible = false;
					this.ddlTehsil.Items.Clear();
					this.rfvddlTehsil.Enabled = false;
					this.ddlDomicile.Items.Clear();
					this.rfvddlDomicile.Enabled = false;
				}
				else
				{
					this.ddlProvince.FillProvinces(CommonListItems.Select);
					SetListControlString(nameof(candidate.Province), this.ddlProvince, candidate.Province, this.rfvddlProvince);
					this.tbProvince.Visible = false;
					this.tbProvince.Text = null;
					this.svtbProvince.Enabled = false;

					this.ddlProvince_SelectedIndexChanged(null, null);

					SetListControlString(nameof(candidate.District), this.ddlDistrict, candidate.District, this.rfvddlDistrict);
					this.tbDistrict.Visible = false;
					this.tbDistrict.Text = null;
					this.svtbDistrict.Enabled = false;

					this.divTehsil.Visible = true;
					this.divDomicile.Visible = true;
					this.ddlDistrict_SelectedIndexChanged(null, null);

					SetListControlString(nameof(candidate.Tehsil), this.ddlTehsil, candidate.Tehsil, this.rfvddlTehsil);
					this.ddlDomicile.FillAllDistricts(CommonListItems.Select);
					SetListControlString(nameof(candidate.Domicile), this.ddlDomicile, candidate.Domicile, this.rfvddlDomicile);
				}

				SetTextBox(nameof(candidate.CurrentAddress), this.tbCurrentAddress, candidate.CurrentAddress, this.svtbCurrentAddress);
				SetTextBox(nameof(candidate.PermanentAddress), this.tbPermanentAddress, candidate.PermanentAddress, this.svtbPermanentAddress);
				this.cbSameAsCurrentAddress.Checked = candidate.CurrentAddress == candidate.PermanentAddress;
				this.cbSameAsCurrentAddress.Visible = candidate.CanEdit(nameof(candidate.PermanentAddress));

				this.ddlSourceOfInformation.FillSourceOfInformation(CommonListItems.Select);
				SetListControlEnum(nameof(candidate.SourceOfInformation), this.ddlSourceOfInformation, candidate.SourceOfInformationEnum, this.rfvddlSourceOfInformation);
				SetTextBox(nameof(candidate.NextOfKin), this.tbNextOfKin, candidate.NextOfKin, this.svtbNextOfKin);
				SetTextBox(nameof(candidate.NextOfKinRelationship), this.tbNextOfKinRelationship, candidate.NextOfKinRelationship, this.svtbNextOfKinRelationship);

				SetTextBox(nameof(candidate.EmergencyContactName), this.tbEmergencyContactName, candidate.EmergencyContactName, this.svtbEmergencyContactName);
				SetTextBox(nameof(candidate.EmergencyMobile), this.tbEmergencyMobile, candidate.EmergencyMobile, this.svtbEmergencyMobile);
				if (candidate.ForeignStudent)
				{
					this.tbEmergencyMobile.Attributes.Add("data-inputmask", "'alias': 'mobileInternational'");
					this.svtbEmergencyMobile.ValidationExpression = StringValidator.ValidationExpressions.MobileInternational;
				}
				else
				{
					this.tbEmergencyMobile.Attributes.Add("data-inputmask", "'alias': 'mobilePK'");
					this.svtbEmergencyMobile.ValidationExpression = StringValidator.ValidationExpressions.MobilePK;
				}

				SetTextBox(nameof(candidate.EmergencyPhone), this.tbEmergencyPhone, candidate.EmergencyPhone, this.svtbEmergencyPhone);
				if (candidate.ForeignStudent)
				{
					this.tbEmergencyPhone.Attributes.Add("data-inputmask", "'alias': 'phoneInternational'");
					this.svtbEmergencyPhone.ValidationExpression = StringValidator.ValidationExpressions.PhoneInternational;
				}
				else
				{
					this.tbEmergencyPhone.Attributes.Add("data-inputmask", "'alias': 'phonePK'");
					this.svtbEmergencyPhone.ValidationExpression = StringValidator.ValidationExpressions.PhonePK;
				}

				this.rblETSScorer.FillYesNo();
				SetListControlBoolean(nameof(candidate.ETSScorer), this.rblETSScorer, candidate.ETSScorer, this.rfvrblETSScorer);

				if (this.ddlReligion.Items.FindByValue(candidate.Religion ?? "") != null)
					this.ddlReligion.SelectedValue = candidate.Religion ?? "";
				else
				{
					this.ddlReligion.SelectedValue = "";
					this.tbReligion.Text = candidate.Religion;
				}
				this.ddlReligion.Enabled = candidate.CanEdit(nameof(candidate.Religion));
				this.tbReligion.ReadOnly = !candidate.CanEdit(nameof(candidate.Religion));
				this.cvtbReligion.Enabled = candidate.CanEdit(nameof(candidate.Religion));

				this.ddlPhysicalDisability.FillYesNo();
				var physicalDisability = candidate.PhysicalDisability.TrimAndMakeItNullIfEmpty();
				if (physicalDisability?.ToLower() == "N/A".ToLower())
					physicalDisability = null;
				SetListControlBoolean(nameof(candidate.PhysicalDisability), this.ddlPhysicalDisability, physicalDisability != null);
				SetTextBox(nameof(candidate.PhysicalDisability), this.tbPhysicalDisability, physicalDisability, this.cvtbPhysicalDisability);

				if (candidate.ForeignStudent)
				{
					this.divAreaType.Visible = false;
					this.divServiceNo.Visible = false;
					this.rblAreaType.Items.Clear();
					this.rfvrblAreaType.Enabled = false;
					this.tbServiceNo.Text = null;
					this.svtbServiceNo.Enabled = false;
					this.svtbServiceNo.Enabled = false;
					this.divNTNOrCNIC.Visible = false;
					this.tbNTNOrCNIC.Text = null;
					this.svtbNTNOrCNIC.Enabled = false;
				}
				else
				{
					this.rblAreaType.FillAreaTypes();
					SetListControlEnum(nameof(candidate.AreaType), this.rblAreaType, candidate.AreaTypeEnum, this.rfvrblAreaType);
					SetTextBox(nameof(candidate.ServiceNo), this.tbServiceNo, candidate.ServiceNo, this.svtbServiceNo);
					SetTextBox(nameof(candidate.NTNOrCNIC), this.tbNTNOrCNIC, candidate.NTNOrCNIC, this.svtbNTNOrCNIC);
				}

				this.ddlPakistanBarCouncilNo.FillYesNo();
				var pakistanBarCouncilNo = candidate.PakistanBarCouncilNo.TrimAndMakeItNullIfEmpty();
				if (pakistanBarCouncilNo?.ToLower() == "N/A".ToLower())
					pakistanBarCouncilNo = null;
				SetListControlBoolean(nameof(candidate.PakistanBarCouncilNo), this.ddlPakistanBarCouncilNo, pakistanBarCouncilNo.TryToInt() != null);
				SetTextBox(nameof(candidate.PakistanBarCouncilNo), this.tbPakistanBarCouncilNo, pakistanBarCouncilNo, this.cvtbPakistanBarCouncilNo);

				#endregion

				#region Father Information

				SetTextBox(nameof(candidate.FatherName), this.tbFatherName, candidate.FatherName, this.svtbFatherName);
				if (candidate.ForeignStudent)
				{
					this.divFatherPassport.Visible = true;
					SetTextBox(nameof(candidate.FatherPassportNo), this.tbFatherPassport, candidate.FatherPassportNo, this.svtbFatherPassport);
					this.divFatherCNIC.Visible = false;
					this.svtbFatherCNIC.Enabled = false;
					this.tbFatherCNIC.Text = null;
				}
				else
				{
					this.divFatherPassport.Visible = false;
					this.divFatherCNIC.Visible = true;
					this.tbFatherPassport.Text = null;
					this.svtbFatherPassport.Enabled = false;
					SetTextBox(nameof(candidate.FatherCNIC), this.tbFatherCNIC, candidate.FatherCNIC, this.svtbFatherCNIC);
				}

				SetTextBox(nameof(candidate.FatherDesignation), this.tbFatherDesignation, candidate.FatherDesignation, this.svtbFatherDesignation);
				SetTextBox(nameof(candidate.FatherDepartment), this.tbFatherDepartment, candidate.FatherDepartment, this.svtbFatherDepartment);

				SetTextBox(nameof(candidate.FatherOrganization), this.tbFatherOrganization, candidate.FatherOrganization, this.svtbFatherOrganization);
				SetTextBox(nameof(candidate.FatherMobile), this.tbFatherMobile, candidate.FatherMobile, this.svtbFatherMobile);
				if (candidate.ForeignStudent)
				{
					this.tbFatherMobile.Attributes.Add("data-inputmask", "'alias': 'mobileInternational'");
					this.svtbFatherMobile.ValidationExpression = StringValidator.ValidationExpressions.MobileInternational;
				}
				else
				{
					this.tbFatherMobile.Attributes.Add("data-inputmask", "'alias': 'mobilePK'");
					this.svtbFatherMobile.ValidationExpression = StringValidator.ValidationExpressions.MobilePK;
				}

				SetTextBox(nameof(candidate.FatherHomePhone), this.tbFatherHomePhone, candidate.FatherHomePhone, this.svtbFatherHomePhone);
				SetTextBox(nameof(candidate.FatherOfficePhone), this.tbFatherOfficePhone, candidate.FatherOfficePhone, this.svtbFatherOfficePhone);

				SetTextBox(nameof(candidate.FatherFax), this.tbFatherFax, candidate.FatherFax, this.svtbFatherFax);
				if (candidate.ForeignStudent)
				{
					this.tbFatherHomePhone.Attributes.Add("data-inputmask", "'alias': 'phoneInternational'");
					this.svtbFatherHomePhone.ValidationExpression = StringValidator.ValidationExpressions.PhoneInternational;

					this.tbFatherOfficePhone.Attributes.Add("data-inputmask", "'alias': 'phoneInternational'");
					this.svtbFatherOfficePhone.ValidationExpression = StringValidator.ValidationExpressions.PhoneInternational;

					this.tbFatherFax.Attributes.Add("data-inputmask", "'alias': 'phoneInternational'");
					this.svtbFatherFax.ValidationExpression = StringValidator.ValidationExpressions.PhoneInternational;
				}
				else
				{
					this.tbFatherHomePhone.Attributes.Add("data-inputmask", "'alias': 'phonePK'");
					this.svtbFatherHomePhone.ValidationExpression = StringValidator.ValidationExpressions.PhonePK;

					this.tbFatherOfficePhone.Attributes.Add("data-inputmask", "'alias': 'phonePK'");
					this.svtbFatherOfficePhone.ValidationExpression = StringValidator.ValidationExpressions.PhonePK;

					this.tbFatherFax.Attributes.Add("data-inputmask", "'alias': 'phonePK'");
					this.svtbFatherFax.ValidationExpression = StringValidator.ValidationExpressions.PhonePK;
				}
				if (candidate.ForeignStudent)
				{
					this.divFatherServiceNo.Visible = false;
					this.tbFatherServiceNo.Text = null;
					this.svtbFatherServiceNo.Enabled = false;
				}
				else
					SetTextBox(nameof(candidate.FatherServiceNo), this.tbFatherServiceNo, candidate.FatherServiceNo, this.svtbFatherServiceNo);

				#endregion

				#region Sponsor Information

				this.ddlSponsoredBy.FillSponsorships();
				if (candidate.SponsoredByEnum == null)
					candidate.SponsoredByEnum = Sponsorships.Father;
				SetListControlEnum(nameof(candidate.SponsoredBy), this.ddlSponsoredBy, candidate.SponsoredByEnum, this.rfvddlSponsoredBy);
				this.ViewState[nameof(candidate.ForeignStudent)] = candidate.ForeignStudent;

				if (this.ForeignStudent)
				{
					this.tbSponsorMobile.Attributes.Add("data-inputmask", "'alias': 'mobileInternational'");
					this.svtbSponsorMobile.ValidationExpression = StringValidator.ValidationExpressions.MobileInternational;

					this.tbSponsorPhoneHome.Attributes.Add("data-inputmask", "'alias': 'phoneInternational'");
					this.svtbSponsorPhoneHome.ValidationExpression = StringValidator.ValidationExpressions.PhoneInternational;

					this.tbSponsorPhoneOffice.Attributes.Add("data-inputmask", "'alias': 'phoneInternational'");
					this.svtbSponsorPhoneOffice.ValidationExpression = StringValidator.ValidationExpressions.PhoneInternational;

					this.tbSponsorFax.Attributes.Add("data-inputmask", "'alias': 'phoneInternational'");
					this.svtbSponsorFax.ValidationExpression = StringValidator.ValidationExpressions.PhoneInternational;
				}
				else
				{
					this.tbSponsorMobile.Attributes.Add("data-inputmask", "'alias': 'mobilePK'");
					this.svtbSponsorMobile.ValidationExpression = StringValidator.ValidationExpressions.MobilePK;

					this.tbSponsorPhoneHome.Attributes.Add("data-inputmask", "'alias': 'phonePK'");
					this.svtbSponsorPhoneHome.ValidationExpression = StringValidator.ValidationExpressions.PhonePK;

					this.tbSponsorPhoneOffice.Attributes.Add("data-inputmask", "'alias': 'phonePK'");
					this.svtbSponsorPhoneOffice.ValidationExpression = StringValidator.ValidationExpressions.PhonePK;

					this.tbSponsorFax.Attributes.Add("data-inputmask", "'alias': 'phonePK'");
					this.svtbSponsorFax.ValidationExpression = StringValidator.ValidationExpressions.PhonePK;
				}

				this.ddlSponsoredBy_OnSelectedIndexChanged(null, null);

				switch (candidate.SponsoredByEnum.Value)
				{
					case Sponsorships.Father:
						SetTextBox(nameof(candidate.AnnualFamilyIncome), this.tbAnnualIncome, candidate.AnnualFamilyIncome, this.ivtbAnnualIncome);
						break;
					case Sponsorships.Self:
						SetTextBox(nameof(candidate.SponsorDesignation), this.tbSponsorDesignation, candidate.SponsorDesignation, this.svtbSponsorDesignation);
						SetTextBox(nameof(candidate.SponsorDepartment), this.tbSponsorDepartment, candidate.SponsorDepartment, this.svtbSponsorDepartment);
						SetTextBox(nameof(candidate.SponsorOrganization), this.tbSponsorOrganization, candidate.SponsorOrganization, this.svtbSponsorOrganization);
						SetTextBox(nameof(candidate.SponsorPhoneOffice), this.tbSponsorPhoneOffice, candidate.SponsorPhoneOffice, this.svtbSponsorPhoneOffice);
						SetTextBox(nameof(candidate.SponsorFax), this.tbSponsorFax, candidate.SponsorFax, this.svtbSponsorFax);
						SetTextBox(nameof(candidate.AnnualFamilyIncome), this.tbAnnualIncome, candidate.AnnualFamilyIncome, this.ivtbAnnualIncome);
						break;
					case Sponsorships.Guardian:
						SetTextBox(nameof(candidate.SponsorName), this.tbSponsorName, candidate.SponsorName, this.svtbSponsorName);
						if (candidate.ForeignStudent)
							SetTextBox(nameof(candidate.SponsorPassportNo), this.tbSponsorPassportNo, candidate.SponsorPassportNo, this.svtbSponsorPassportNo);
						else
							SetTextBox(nameof(candidate.SponsorCNIC), this.tbSponsorCNIC, candidate.SponsorCNIC, this.svtbSponsorCNIC);
						SetTextBox(nameof(candidate.SponsorRelationshipWithGuardian), this.tbSponsorRelationshipWithGuardian, candidate.SponsorRelationshipWithGuardian, this.svtbSponsorRelationshipWithGuardian);
						SetTextBox(nameof(candidate.SponsorDesignation), this.tbSponsorDesignation, candidate.SponsorDesignation, this.svtbSponsorDesignation);
						SetTextBox(nameof(candidate.SponsorDepartment), this.tbSponsorDepartment, candidate.SponsorDepartment, this.svtbSponsorDepartment);
						SetTextBox(nameof(candidate.SponsorOrganization), this.tbSponsorOrganization, candidate.SponsorOrganization, this.svtbSponsorOrganization);
						SetTextBox(nameof(candidate.SponsorMobile), this.tbSponsorMobile, candidate.SponsorMobile, this.svtbMobile);
						SetTextBox(nameof(candidate.SponsorPhoneOffice), this.tbSponsorPhoneOffice, candidate.SponsorPhoneOffice, this.svtbSponsorPhoneOffice);
						SetTextBox(nameof(candidate.SponsorPhoneHome), this.tbSponsorPhoneHome, candidate.SponsorPhoneHome, this.svtbSponsorPhoneHome);
						SetTextBox(nameof(candidate.SponsorFax), this.tbSponsorFax, candidate.SponsorFax, this.svtbSponsorFax);
						if (!candidate.ForeignStudent)
							SetTextBox(nameof(candidate.SponsorServiceNo), this.tbSponsorServiceNo, candidate.SponsorServiceNo, this.svtbSponsorServiceNo);
						SetTextBox(nameof(candidate.AnnualFamilyIncome), this.tbAnnualIncome, candidate.AnnualFamilyIncome, this.ivtbAnnualIncome);
						break;
					case Sponsorships.Organization:
						SetTextBox(nameof(candidate.SponsorName), this.tbSponsorName, candidate.SponsorName, this.svtbSponsorName);
						SetTextBox(nameof(candidate.SponsorRelationshipWithGuardian), this.tbSponsorRelationshipWithGuardian, candidate.SponsorRelationshipWithGuardian, this.svtbSponsorRelationshipWithGuardian);
						SetTextBox(nameof(candidate.SponsorPhoneOffice), this.tbSponsorPhoneOffice, candidate.SponsorPhoneOffice, this.svtbSponsorPhoneOffice);
						SetTextBox(nameof(candidate.SponsorFax), this.tbSponsorFax, candidate.SponsorFax, this.svtbSponsorFax);
						break;
					default:
						throw new NotImplementedEnumException(candidate.SponsoredByEnum);
				}

				#endregion
			}
		}

		protected void ddlSponsoredBy_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var sponsoredBy = this.ddlSponsoredBy.GetSelectedEnumValue<Sponsorships>();
			var foreignStudent = (bool)this.ViewState[nameof(Model.Entities.Candidate.ForeignStudent)];

			void SetPropertiesString(bool visible, HtmlGenericControl div, AspireTextBox textBox, AspireStringValidator aspireStringValidator, bool allowNull, AspireLabel label)
			{
				div.Visible = visible;
				if (visible)
				{
					textBox.Enabled = true;
					aspireStringValidator.Enabled = true;
					aspireStringValidator.AllowNull = allowNull;
					if (!allowNull && !label.Text.Contains(@"<span style=""color:red;"" title=""This field is required."">*</span>"))
						label.Text = $@"{label.Text} <span style=""color:red;"" title=""This field is required."">*</span>";
					else if (allowNull && label.Text.Contains(@"<span style=""color:red;"" title=""This field is required."">*</span>"))
						label.Text = label.Text.Replace(@"<span style=""color:red;"" title=""This field is required."">*</span>", string.Empty);

				}
				else
				{
					textBox.Enabled = false;
					aspireStringValidator.Enabled = false;
					textBox.Text = null;
				}
			}
			void SetPropertiesInt(bool visible, HtmlGenericControl div, AspireTextBox textBox, AspireInt32Validator aspireInt32Validator, bool allowNull, AspireLabel label)
			{
				div.Visible = visible;
				if (visible)
				{
					textBox.Enabled = true;
					aspireInt32Validator.Enabled = true;
					aspireInt32Validator.AllowNull = allowNull;
					if (!allowNull && !label.Text.Contains(@"<span style=""color:red;"" title=""This field is required."">*</span>"))
						label.Text = $@"{label.Text} <span style=""color:red;"" title=""This field is required."">*</span>";
					else if (allowNull && label.Text.Contains(@"<span style=""color:red;"" title=""This field is required."">*</span>"))
						label.Text = label.Text.Replace(@"<span style=""color:red;"" title=""This field is required."">*</span>", string.Empty);
				}
				else
				{
					textBox.Enabled = false;
					aspireInt32Validator.Enabled = false;
					textBox.Text = null;
				}
			}

			switch (sponsoredBy)
			{
				case Sponsorships.Father:
					SetPropertiesString(false, this.divSponsorName, this.tbSponsorName, this.svtbSponsorName, true, this.lblSponsorName);
					SetPropertiesString(false, this.divSponsorCNIC, this.tbSponsorCNIC, this.svtbSponsorCNIC, true, this.lblSponsorSponsorCNIC);
					SetPropertiesString(false, this.divSponsorPassportNo, this.tbSponsorPassportNo, this.svtbSponsorPassportNo, true, this.lblSponsorPassportNo);
					SetPropertiesString(false, this.divSponsorRelationshipWithGuardian, this.tbSponsorRelationshipWithGuardian, this.svtbSponsorRelationshipWithGuardian, true, this.lblSponsorRelationshipWithGuardian);
					SetPropertiesString(false, this.divSponsorDesignation, this.tbSponsorDesignation, this.svtbSponsorDesignation, true, this.lblSponsorDesignation);
					SetPropertiesString(false, this.divSponsorDepartment, this.tbSponsorDepartment, this.svtbSponsorDepartment, true, this.lblSponsorrDepartment);
					SetPropertiesString(false, this.divSponsorOrganization, this.tbSponsorOrganization, this.svtbSponsorOrganization, true, this.lblSponsorOrganization);
					SetPropertiesString(false, this.divSponsorMobile, this.tbSponsorMobile, this.svtbSponsorMobile, true, this.lblSponsorMobile);
					SetPropertiesString(false, this.divSponsorPhoneOffice, this.tbSponsorPhoneOffice, this.svtbSponsorPhoneOffice, true, this.lblSponsorPhoneOffice);
					SetPropertiesString(false, this.divSponsorPhoneHome, this.tbSponsorPhoneHome, this.svtbSponsorPhoneHome, true, this.lblSponsorPhoneHome);
					SetPropertiesString(false, this.divSponsorFax, this.tbSponsorFax, this.svtbSponsorFax, true, this.lblSponsorFax);
					SetPropertiesString(false, this.divSponsorServiceNo, this.tbSponsorServiceNo, this.svtbSponsorServiceNo, true, this.lblSponsorServiceNo);
					SetPropertiesInt(!foreignStudent, this.divAnnualIncome, this.tbAnnualIncome, this.ivtbAnnualIncome, false, this.lblAnnualIncome);
					break;
				case Sponsorships.Self:
					SetPropertiesString(false, this.divSponsorName, this.tbSponsorName, this.svtbSponsorName, true, this.lblSponsorName);
					SetPropertiesString(false, this.divSponsorCNIC, this.tbSponsorCNIC, this.svtbSponsorCNIC, true, this.lblSponsorSponsorCNIC);
					SetPropertiesString(false, this.divSponsorPassportNo, this.tbSponsorPassportNo, this.svtbSponsorPassportNo, true, this.lblSponsorPassportNo);
					SetPropertiesString(false, this.divSponsorRelationshipWithGuardian, this.tbSponsorRelationshipWithGuardian, this.svtbSponsorRelationshipWithGuardian, true, this.lblSponsorRelationshipWithGuardian);
					SetPropertiesString(true, this.divSponsorDesignation, this.tbSponsorDesignation, this.svtbSponsorDesignation, false, this.lblSponsorDesignation);
					SetPropertiesString(true, this.divSponsorDepartment, this.tbSponsorDepartment, this.svtbSponsorDepartment, false, this.lblSponsorrDepartment);
					SetPropertiesString(true, this.divSponsorOrganization, this.tbSponsorOrganization, this.svtbSponsorOrganization, false, this.lblSponsorOrganization);
					SetPropertiesString(false, this.divSponsorMobile, this.tbSponsorMobile, this.svtbSponsorMobile, true, this.lblSponsorMobile);
					SetPropertiesString(true, this.divSponsorPhoneOffice, this.tbSponsorPhoneOffice, this.svtbSponsorPhoneOffice, false, this.lblSponsorPhoneOffice);
					SetPropertiesString(false, this.divSponsorPhoneHome, this.tbSponsorPhoneHome, this.svtbSponsorPhoneHome, true, this.lblSponsorPhoneHome);
					SetPropertiesString(true, this.divSponsorFax, this.tbSponsorFax, this.svtbSponsorFax, true, this.lblSponsorFax);
					SetPropertiesString(false, this.divSponsorServiceNo, this.tbSponsorServiceNo, this.svtbSponsorServiceNo, true, this.lblSponsorServiceNo);
					SetPropertiesInt(!foreignStudent, this.divAnnualIncome, this.tbAnnualIncome, this.ivtbAnnualIncome, false, this.lblAnnualIncome);
					break;
				case Sponsorships.Guardian:
					SetPropertiesString(true, this.divSponsorName, this.tbSponsorName, this.svtbSponsorName, false, this.lblSponsorName);
					SetPropertiesString(!foreignStudent, this.divSponsorCNIC, this.tbSponsorCNIC, this.svtbSponsorCNIC, false, this.lblSponsorSponsorCNIC);
					SetPropertiesString(foreignStudent, this.divSponsorPassportNo, this.tbSponsorPassportNo, this.svtbSponsorPassportNo, false, this.lblSponsorPassportNo);
					SetPropertiesString(true, this.divSponsorRelationshipWithGuardian, this.tbSponsorRelationshipWithGuardian, this.svtbSponsorRelationshipWithGuardian, false, this.lblSponsorRelationshipWithGuardian);
					SetPropertiesString(true, this.divSponsorDesignation, this.tbSponsorDesignation, this.svtbSponsorDesignation, false, this.lblSponsorDesignation);
					SetPropertiesString(true, this.divSponsorDepartment, this.tbSponsorDepartment, this.svtbSponsorDepartment, false, this.lblSponsorrDepartment);
					SetPropertiesString(true, this.divSponsorOrganization, this.tbSponsorOrganization, this.svtbSponsorOrganization, false, this.lblSponsorOrganization);
					SetPropertiesString(true, this.divSponsorMobile, this.tbSponsorMobile, this.svtbSponsorMobile, false, this.lblSponsorMobile);
					SetPropertiesString(true, this.divSponsorPhoneOffice, this.tbSponsorPhoneOffice, this.svtbSponsorPhoneOffice, true, this.lblSponsorPhoneOffice);
					SetPropertiesString(true, this.divSponsorPhoneHome, this.tbSponsorPhoneHome, this.svtbSponsorPhoneHome, false, this.lblSponsorPhoneHome);
					SetPropertiesString(true, this.divSponsorFax, this.tbSponsorFax, this.svtbSponsorFax, true, this.lblSponsorFax);
					SetPropertiesString(!foreignStudent, this.divSponsorServiceNo, this.tbSponsorServiceNo, this.svtbSponsorServiceNo, true, this.lblSponsorServiceNo);
					SetPropertiesInt(!foreignStudent, this.divAnnualIncome, this.tbAnnualIncome, this.ivtbAnnualIncome, false, this.lblAnnualIncome);
					break;
				case Sponsorships.Organization:
					SetPropertiesString(true, this.divSponsorName, this.tbSponsorName, this.svtbSponsorName, false, this.lblSponsorName);
					SetPropertiesString(false, this.divSponsorCNIC, this.tbSponsorCNIC, this.svtbSponsorCNIC, false, this.lblSponsorSponsorCNIC);
					SetPropertiesString(false, this.divSponsorPassportNo, this.tbSponsorPassportNo, this.svtbSponsorPassportNo, false, this.lblSponsorPassportNo);
					SetPropertiesString(true, this.divSponsorRelationshipWithGuardian, this.tbSponsorRelationshipWithGuardian, this.svtbSponsorRelationshipWithGuardian, false, this.lblSponsorRelationshipWithGuardian);
					SetPropertiesString(false, this.divSponsorDesignation, this.tbSponsorDesignation, this.svtbSponsorDesignation, false, this.lblSponsorDesignation);
					SetPropertiesString(false, this.divSponsorDepartment, this.tbSponsorDepartment, this.svtbSponsorDepartment, false, this.lblSponsorrDepartment);
					SetPropertiesString(false, this.divSponsorOrganization, this.tbSponsorOrganization, this.svtbSponsorOrganization, false, this.lblSponsorOrganization);
					SetPropertiesString(false, this.divSponsorMobile, this.tbSponsorMobile, this.svtbSponsorMobile, false, this.lblSponsorMobile);
					SetPropertiesString(true, this.divSponsorPhoneOffice, this.tbSponsorPhoneOffice, this.svtbSponsorPhoneOffice, false, this.lblSponsorPhoneOffice);
					SetPropertiesString(false, this.divSponsorPhoneHome, this.tbSponsorPhoneHome, this.svtbSponsorPhoneHome, false, this.lblSponsorPhoneHome);
					SetPropertiesString(true, this.divSponsorFax, this.tbSponsorFax, this.svtbSponsorFax, true, this.lblSponsorFax);
					SetPropertiesString(false, this.divSponsorServiceNo, this.tbSponsorServiceNo, this.svtbSponsorServiceNo, true, this.lblSponsorServiceNo);
					SetPropertiesInt(false, this.divAnnualIncome, this.tbAnnualIncome, this.ivtbAnnualIncome, false, this.lblAnnualIncome);
					break;
				default:
					throw new NotImplementedEnumException(sponsoredBy);
			}
		}

		protected void ddlProvince_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDistrict.FillDistricts(this.ddlProvince.SelectedValue, CommonListItems.Select);
			this.ddlDistrict_SelectedIndexChanged(null, null);
		}

		protected void ddlDistrict_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlTehsil.FillTehsils(this.ddlProvince.SelectedValue, this.ddlDistrict.SelectedValue, CommonListItems.Select);
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.Page.IsValid)
				return;
			if (AspireIdentity.Current.UserType.IsStaff())
				StaffIdentity.Current.Demand(StaffPermissions.Admissions.ChangeProfileInformation, UserGroupPermission.PermissionValues.Allowed);
			if (this.CandidateID == null)
				throw new InvalidOperationException();

			if (this.tbDOB.SelectedDate >= DateTime.Today)
			{
				this.Page.AddErrorAlert("Date of Birth cannot be future or current date.");
				this.tbDOB.Focus();
				return;
			}

			if (this.ForeignStudent)
			{
				if (this.tbNationality.Text.ToLower().Contains(Pakistan.ToLower()))
				{
					this.Page.AddErrorAlert("For Foreign Students, Nationality can not be Pakistani.");
					this.tbNationality.Focus();
					return;
				}
				if (this.ddlCountry.SelectedValue.ToLower().Contains(Pakistan.ToLower()))
				{
					this.Page.AddErrorAlert("For Foreign Students, Country can not be Pakistan.");
					this.tbNationality.Focus();
					return;
				}
			}
			else
			{
				if (this.tbNationality.Text.ToLower() != Pakistani.ToLower())
				{
					this.Page.AddErrorAlert($"Nationality must be {Pakistani}.");
					this.tbNationality.Focus();
					return;
				}
				if (!this.ddlCountry.SelectedValue.ToLower().Contains(Pakistan.ToLower()))
				{
					this.Page.AddErrorAlert($"Country must be {Pakistan}");
					this.tbNationality.Focus();
					return;
				}
			}

			var categoryEnum = this.rblCategory.GetSelectedEnumValue<Categories>();
			if (this.ForeignStudent == false)
				switch (categoryEnum)
				{
					case Categories.NavalRetired:
					case Categories.NavalServing:
					case Categories.Naval:
						var serviceNo = this.tbServiceNo.Text.ToNullIfEmpty() ?? this.tbFatherServiceNo.Text.ToNullIfEmpty() ?? this.tbSponsorServiceNo.Text.ToNullIfEmpty();
						if (serviceNo == null)
						{
							this.Page.AddErrorAlert($"Your or your father's or guardian's Service No. of Pakistan NAVY is required because you have selected the \"{categoryEnum.ToFullName()}\" as a category.");
							this.tbServiceNo.Focus();
							return;
						}
						break;
					case Categories.Others:
						break;
					default:
						throw new NotImplementedEnumException(categoryEnum);
				}

			var candidate = Aspire.BL.Core.Admissions.Common.ProfileInformation.GetCandidate(this.CandidateID.Value, this.Page.AspireIdentity.LoginSessionGuid);
			if (candidate == null)
			{
				this.Page.AddNoRecordFoundAlert();
				this.RefreshPage();
				return;
			}
			candidate.Name = this.tbName.Text;
			candidate.BURegistrationNo = this.tbBURegistrationNo.Text.ToNullableInt();
			candidate.CNIC = this.tbCNIC.Text;
			candidate.PassportNo = this.tbPassportNo.Text;
			candidate.GenderEnum = this.rblGender.GetSelectedEnumValue<Genders>();
			candidate.CategoryEnum = this.rblCategory.GetSelectedEnumValue<Categories>();
			candidate.DOB = this.tbDOB.SelectedDate;
			candidate.BloodGroupEnum = this.ddlBloodGroup.GetSelectedEnumValue<BloodGroups>();
			candidate.Mobile = this.tbMobile.Text;
			candidate.Phone = this.tbPhoneHome.Text;
			candidate.Nationality = this.tbNationality.Text;
			candidate.Country = this.ddlCountry.SelectedValue;
			candidate.Province = this.ddlProvince.SelectedValue.ToNullIfEmpty() ?? this.tbProvince.Text;
			candidate.District = this.ddlDistrict.SelectedValue.ToNullIfEmpty() ?? this.tbDistrict.Text;
			candidate.Tehsil = this.ddlTehsil.SelectedValue;
			candidate.Domicile = this.ddlDomicile.SelectedValue;
			candidate.CurrentAddress = this.tbCurrentAddress.Text;
			candidate.PermanentAddress = this.tbPermanentAddress.Text;
			candidate.SourceOfInformationEnum = this.ddlSourceOfInformation.GetSelectedEnumValue<Model.Entities.Candidate.SourceOfInformations>();
			candidate.NextOfKin = this.tbNextOfKin.Text;
			candidate.NextOfKinRelationship = this.tbNextOfKinRelationship.Text;
			candidate.EmergencyContactName = this.tbEmergencyContactName.Text;
			candidate.EmergencyMobile = this.tbEmergencyMobile.Text;
			candidate.EmergencyPhone = this.tbEmergencyPhone.Text;
			candidate.ETSScorer = this.rblETSScorer.SelectedValue.ToBoolean();
			candidate.Religion = this.ddlReligion.SelectedValue.ToNullIfWhiteSpace() ?? this.tbReligion.Text;
			candidate.PhysicalDisability = this.tbPhysicalDisability.Text.ToNullIfWhiteSpaceOrNA();
			candidate.AreaTypeEnum = this.rblAreaType.GetSelectedEnumValue<AreaTypes>(null);
			candidate.ServiceNo = this.tbServiceNo.Text;
			candidate.PakistanBarCouncilNo = this.tbPakistanBarCouncilNo.Text.ToNullIfWhiteSpaceOrNA();
			candidate.NTNOrCNIC = this.tbNTNOrCNIC.Text;

			candidate.IsFatherAlive = this.rblFatherAlive.SelectedValue.ToBoolean();
			candidate.FatherName = this.tbFatherName.Text;
			candidate.FatherCNIC = this.tbFatherCNIC.Text;
			candidate.FatherPassportNo = this.tbFatherPassport.Text;
			candidate.FatherDesignation = this.tbFatherDesignation.Text;
			candidate.FatherDepartment = this.tbFatherDepartment.Text;
			candidate.FatherOrganization = this.tbFatherOrganization.Text;
			candidate.FatherServiceNo = this.tbFatherServiceNo.Text;
			candidate.FatherMobile = this.tbFatherMobile.Text;
			candidate.FatherOfficePhone = this.tbFatherOfficePhone.Text;
			candidate.FatherHomePhone = this.tbFatherHomePhone.Text;
			candidate.FatherFax = this.tbFatherFax.Text;
			candidate.FatherServiceNo = this.tbFatherServiceNo.Text;

			candidate.SponsoredByEnum = this.ddlSponsoredBy.GetSelectedEnumValue<Sponsorships>();
			candidate.SponsorName = this.tbSponsorName.Text;
			candidate.SponsorCNIC = this.tbSponsorCNIC.Text;
			candidate.SponsorPassportNo = this.tbSponsorPassportNo.Text;
			candidate.SponsorRelationshipWithGuardian = this.tbSponsorRelationshipWithGuardian.Text;
			candidate.SponsorDesignation = this.tbSponsorDesignation.Text;
			candidate.SponsorDepartment = this.tbSponsorDepartment.Text;
			candidate.SponsorOrganization = this.tbSponsorOrganization.Text;
			candidate.SponsorServiceNo = this.tbSponsorServiceNo.Text;
			candidate.SponsorMobile = this.tbSponsorMobile.Text;
			candidate.SponsorPhoneOffice = this.tbSponsorPhoneOffice.Text;
			candidate.SponsorPhoneHome = this.tbSponsorPhoneHome.Text;
			candidate.SponsorFax = this.tbSponsorFax.Text;
			candidate.SponsorServiceNo = this.tbSponsorServiceNo.Text;
			candidate.AnnualFamilyIncome = this.tbAnnualIncome.Text;

			var updated = ProfileInformation.UpdateCandidate(candidate, this.Page.AspireIdentity.LoginSessionGuid);
			if (updated)
				this.Page.AddSuccessAlert("Profile Information has been updated.");
			else
				this.Page.AddNoRecordFoundAlert();
			this.RefreshPage();
		}

		protected void btnBrowse_OnClick(object sender, EventArgs e)
		{
			this.photoUploadUserControl.ShowModal();
		}

		protected void photoUploadUserControl_OnSave(object sender, PhotoUploadUserControl.SaveEventArgs e)
		{
			var result = ProfileInformation.UpdatePhoto(this.CandidateID ?? throw new InvalidOperationException(), e.Bytes, AspireIdentity.Current.LoginSessionGuid);
			switch (result)
			{
				case ProfileInformation.UpdatePhotoStatuses.NoRecordFound:
					this.Page.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case ProfileInformation.UpdatePhotoStatuses.RecordCannotBeChanged:
					this.Page.AddErrorAlert("Record can not be changed.");
					this.RefreshPage();
					return;
				case ProfileInformation.UpdatePhotoStatuses.ResolutionMustBe400X400:
					this.Page.AddErrorAlert("Image resolution must be 400 x 400 pixels.");
					this.RefreshPage();
					return;
				case ProfileInformation.UpdatePhotoStatuses.InvalidImage:
					e.Status = PhotoUploadUserControl.SaveEventArgs.Statuses.InvalidImage;
					return;
				case ProfileInformation.UpdatePhotoStatuses.Success:
					e.Status = PhotoUploadUserControl.SaveEventArgs.Statuses.Success;
					this.RefreshPage();
					return;
				default:
					throw new NotImplementedEnumException(result);
			}
		}

		private void RefreshPage()
		{
			switch (this.Page.AspireIdentity.UserType)
			{
				case UserTypes.Staff:
					if (this.Page is Aspire.Web.Sys.Staff.Admissions.CandidateProfile)
						Aspire.Web.Sys.Staff.Admissions.CandidateProfile.Redirect(this.CandidateID);
					else
						BasePage.Redirect<SearchCandidate>();
					return;
				case UserTypes.Candidate:
					if (this.Page is Aspire.Web.Sys.Candidate.Profile)
						BasePage.Redirect<Candidate.Profile>();
					else
						BasePage.Redirect<Candidate.Dashboard>();
					return;
				default:
					throw new NotImplementedEnumException(this.Page.AspireIdentity.UserType);
			}
		}

		protected void cvtbPakistanBarCouncilNo_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			if (this.ddlPakistanBarCouncilNo.SelectedValue.ToBoolean())
			{
				if (this.tbPakistanBarCouncilNo.Text.TryToInt() != null)
					args.IsValid = true;
				else if (this.tbSponsorPassportNo.Text.ToNullIfWhiteSpace() == null)
				{
					args.IsValid = false;
					this.cvtbPakistanBarCouncilNo.ErrorMessage = "This field is required.";
				}
				else
				{
					args.IsValid = false;
					this.cvtbPakistanBarCouncilNo.ErrorMessage = "Invalid number.";
				}
			}
			else
			{
				if (this.tbPakistanBarCouncilNo.Text.ToNullIfWhiteSpaceOrNA() == null)
					args.IsValid = true;
				else
				{
					args.IsValid = false;
					this.cvtbPakistanBarCouncilNo.ErrorMessage = "Invalid value.";
				}
			}
		}

		protected void cvtbPhysicalDisability_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			if (this.ddlPhysicalDisability.SelectedValue.ToBoolean())
			{
				if (this.tbPhysicalDisability.Text.ToNullIfWhiteSpaceOrNA() != null)
					args.IsValid = true;
				else
				{
					args.IsValid = false;
					this.cvtbPhysicalDisability.ErrorMessage = "This field is required.";
				}
			}
			else
			{
				if (this.tbPhysicalDisability.Text.ToNullIfWhiteSpaceOrNA() == null)
					args.IsValid = true;
				else
				{
					args.IsValid = false;
					this.cvtbPakistanBarCouncilNo.ErrorMessage = "Invalid value.";
				}
			}
		}

		protected void cvReligion_ServerValidate(object source, ServerValidateEventArgs args)
		{
			var religion = (this.ddlReligion.SelectedValue.ToNullIfWhiteSpace() ?? this.tbReligion.Text).ToNullIfWhiteSpaceOrNA();
			args.IsValid = religion != null;
			this.cvtbReligion.ErrorMessage = "This field is required.";
		}

		protected void rblFatherAlive_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var fatherAlive = this.rblFatherAlive.SelectedValue.ToBoolean();
			this.svtbFatherDesignation.AllowNull = this.svtbFatherCNIC.AllowNull = this.svtbFatherPassport.AllowNull = !fatherAlive;
			if (fatherAlive)
			{
				if (!this.lblFatherCNIC.Text.Contains(@"<span style='color:red;' title='This field is required.'>*</span>"))
					this.lblFatherCNIC.Text = $@"{this.lblFatherCNIC.Text} <span style='color:red;' title='This field is required.'>*</span>";
				if (!this.lblFatherDesignation.Text.Contains(@"<span style='color:red;' title='This field is required.'>*</span>"))
					this.lblFatherDesignation.Text = $@"{this.lblFatherDesignation.Text} <span style='color:red;' title='This field is required.'>*</span>";
				if (!this.lblFatherPassport.Text.Contains(@"<span style='color:red;' title='This field is required.'>*</span>"))
					this.lblFatherPassport.Text = $@"{this.lblFatherPassport.Text} <span style='color:red;' title='This field is required.'>*</span>";
			}
			else
			{
				if (this.lblFatherCNIC.Text.Contains(@"<span style='color:red;' title='This field is required.'>*</span>"))
					this.lblFatherCNIC.Text = this.lblFatherCNIC.Text.Replace(@"<span style='color:red;' title='This field is required.'>*</span>", string.Empty);
				if (this.lblFatherDesignation.Text.Contains(@"<span style='color:red;' title='This field is required.'>*</span>"))
					this.lblFatherDesignation.Text = this.lblFatherDesignation.Text.Replace(@" <span style='color:red;' title='This field is required.'>*</span>", string.Empty);
				if (this.lblFatherPassport.Text.Contains(@"<span style='color:red;' title='This field is required.'>*</span>"))
					this.lblFatherPassport.Text = this.lblFatherPassport.Text.Replace(@" <span style='color:red;' title='This field is required.'>*</span>", string.Empty);
			}
		}
	}
}