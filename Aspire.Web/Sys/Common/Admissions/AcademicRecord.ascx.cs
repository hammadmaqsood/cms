﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Common.Admissions
{
	public partial class AcademicRecord : UserControl
	{
		private AdmissionCriteriaPreReqDegree _admissionCriteriaPreReqDegree;

		public AdmissionCriteriaPreReqDegree AdmissionCriteriaPreReqDegree
		{
			get
			{
				if (this._admissionCriteriaPreReqDegree == null)
				{
					var admissionCriteriaPreReqDegreeID = this.ddlDegreeType.SelectedValue.ToInt();
					this._admissionCriteriaPreReqDegree = BL.Core.Admissions.Common.ApplyProgram.GetAdmissionCriteriaPreReqDegree(admissionCriteriaPreReqDegreeID, CandidateIdentity.Current.LoginSessionGuid);
					if (this._admissionCriteriaPreReqDegree == null)
					{
						((IAlert)this.Page).AddNoRecordFoundAlert();
						switch (AspireIdentity.Current.UserType)
						{
							case UserTypes.Staff:
								BasePage.Redirect<Sys.Staff.Admissions.SearchCandidate>();
								break;
							case UserTypes.Candidate:
								BasePage.Redirect<Sys.Candidate.ApplyProgram>();
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}
					}
				}
				return this._admissionCriteriaPreReqDegree;
			}
		}

		protected override void OnDataBinding(EventArgs e)
		{
			var dataItem = ((RepeaterItem)this.NamingContainer).DataItem;
			this.ddlDegreeType.DataBind((List<CustomListItem>)DataBinder.Eval(dataItem, "PreReqDegreeTypes"));
			this.ddlDegreeType.Enabled = this.ddlDegreeType.Items.Count > 1;
			this.ddlDegreeType_SelectedIndexChanged(null, null);
			base.OnDataBinding(e);
		}

		protected void ddlDegreeType_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.rblExaminationSystem.Items.Clear();
			if (this.AdmissionCriteriaPreReqDegree.AnnualExamSystem)
				this.rblExaminationSystem.Items.Add(new ListItem("Annual System", "A"));
			if (this.AdmissionCriteriaPreReqDegree.SemesterSystem)
				this.rblExaminationSystem.Items.Add(new ListItem("Semester System", "S"));
			if (this.rblExaminationSystem.Items.Count == 0)
				throw new InvalidOperationException("Invalid Criteria.");
			this.rblExaminationSystem.SelectedIndex = 0;
			this.divExaminationSystem.Visible = this.rblExaminationSystem.Items.Count > 1;

			if (this.AdmissionCriteriaPreReqDegree.AdmissionCriteriaPreReqDegreeSubjects.Any())
			{
				this.ddlSubjects.DataBind(this.AdmissionCriteriaPreReqDegree.AdmissionCriteriaPreReqDegreeSubjects.Select(s => s.Subjects));
				this.ddlSubjects.Enabled = this.ddlSubjects.Items.Count > 1;
				this.divddlSubjects.Visible = true;
				this.divtbSubjects.Visible = false;
			}
			else
			{
				this.divddlSubjects.Visible = false;
				this.ddlSubjects.Items.Clear();
				this.divtbSubjects.Visible = true;
			}
			this.ivtbPassingYear.MinValue = (short)(DateTime.Now.Year - 100);
			this.ivtbPassingYear.MaxValue = (short)DateTime.Now.Year;

			this.rblExaminationSystem_SelectedIndexChanged(null, null);
		}

		protected void rblExaminationSystem_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.AdmissionCriteriaPreReqDegree == null)
				throw new InvalidOperationException();
			var isAnnualSystem = this.rblExaminationSystem.SelectedValue == "A";
			var isSemesterSystem = this.rblExaminationSystem.SelectedValue == "S";

			this.divObtainedCGPA.Visible = this.divTotalCGPA.Visible = isSemesterSystem;
			this.dvtbObtainedCGPA.Enabled = isSemesterSystem;

			this.divObtainedMarks.Visible = this.divTotalMarks.Visible = this.divPercentage.Visible = isAnnualSystem;
			this.ivtbObtainedMarks.Enabled = this.ivtbTotalMarks.Enabled = this.cvtbTotalMarks.Enabled = this.cvtbPercentage.Enabled = isAnnualSystem;

			this.ViewState["MinPercentage"] = null;
			if (isAnnualSystem)
			{
				var minPercentage = this.AdmissionCriteriaPreReqDegree.AnnualExamSystemMinimumPercentage ?? 0;
				this.ViewState["MinPercentage"] = minPercentage;
				this.cvtbPercentage.Attributes.Add("tbObtainedMarks", this.tbObtainedMarks.ClientID);
				this.cvtbPercentage.Attributes.Add("tbTotalMarks", this.tbTotalMarks.ClientID);
				this.cvtbPercentage.Attributes.Add("tbPercentage", this.tbPercentage.ClientID);
				this.cvtbPercentage.Attributes.Add("min", minPercentage.ToString());
				this.cvtbPercentage.ErrorMessage = $"Minimum {minPercentage}% and maximum 100% is required.";
			}
			if (isSemesterSystem)
			{
				this.ddlTotalCGPA.Items.Clear();
				if (this.AdmissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf4 != null)
					this.ddlTotalCGPA.Items.Add(new ListItem("4.0"));
				if (this.AdmissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf5 != null)
					this.ddlTotalCGPA.Items.Add(new ListItem("5.0"));

				if (this.ddlTotalCGPA.Items.Count == 0)
					throw new InvalidOperationException("Invalid Criteria");
				this.ddlTotalCGPA_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlTotalCGPA_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.ddlTotalCGPA.SelectedValue == "4.0")
			{
				this.tbObtainedCGPA.Attributes.Add("data-inputmask", "'alias': 'cgpa4'");
				if (this.AdmissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf4 == null)
					throw new InvalidOperationException("SemesterSystemMinCGPAOutOf4 cannot be null for CGPA 4.");
				this.dvtbObtainedCGPA.MinValue = this.AdmissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf4.Value;
				this.dvtbObtainedCGPA.MaxValue = 4f;
			}
			else if (this.ddlTotalCGPA.SelectedValue == "5.0")
			{
				this.tbObtainedCGPA.Attributes.Add("data-inputmask", "'alias': 'cgpa5'");
				if (this.AdmissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf5 == null)
					throw new InvalidOperationException("SemesterSystemMinCGPAOutOf5 cannot be null for CGPA 5.");
				this.dvtbObtainedCGPA.MinValue = this.AdmissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf5.Value;
				this.dvtbObtainedCGPA.MaxValue = 5f;
			}
			else
				throw new InvalidOperationException("Invalid Criteria");
		}

		public CandidateAppliedProgramAcademicRecord ToAcademicRecord()
		{
			var admissionCriteriaPreReqDegreeID = this.ddlDegreeType.SelectedValue.ToInt();
			var degreeType = Enum.GetValues(typeof(DegreeTypes)).Cast<DegreeTypes>().Single(d => d.ToFullName() == this.ddlDegreeType.SelectedItem.Text);
			string subjects;
			if (this.divddlSubjects.Visible)
				subjects = this.ddlSubjects.SelectedValue;
			else if (this.divtbSubjects.Visible)
				subjects = this.tbSubjects.Text;
			else throw new InvalidOperationException();
			var institute = this.tbInstitute.Text;
			var boardUniversity = this.tbBoardUniversity.Text;
			var passingYear = this.tbPassingYear.Text.ToShort();
			if (this.rblExaminationSystem.SelectedValue == "S")
			{
				var obtainedMarks = this.tbObtainedCGPA.Text.ToDouble();
				var totalMarks = this.ddlTotalCGPA.SelectedValue.ToDouble();
				if (obtainedMarks < this.dvtbObtainedCGPA.MinValue || this.dvtbObtainedCGPA.MaxValue < obtainedMarks)
					throw new InvalidOperationException("Min CGPA tempered.");
				var percentage = obtainedMarks * 100f / totalMarks;
				return new CandidateAppliedProgramAcademicRecord
				{
					AdmissionCriteriaPreReqDegreeID = admissionCriteriaPreReqDegreeID,
					DegreeTypeEnum = degreeType,
					BoardUniversity = boardUniversity,
					IsCGPA = true,
					ObtainedMarks = obtainedMarks,
					TotalMarks = totalMarks,
					Percentage = percentage,
					PassingYear = passingYear,
					Subjects = subjects,
				};
			}
			else
			{
				var obtainedMarks = this.tbObtainedMarks.Text.ToDouble();
				var totalMarks = this.tbTotalMarks.Text.ToDouble();
				var percentage = obtainedMarks * 100f / totalMarks;
				var minPercentage = (double)(int)this.ViewState["MinPercentage"];
				if (percentage < minPercentage || 100 < percentage)
					throw new InvalidOperationException("Percentage tempered.");
				return new CandidateAppliedProgramAcademicRecord
				{
					AdmissionCriteriaPreReqDegreeID = admissionCriteriaPreReqDegreeID,
					DegreeTypeEnum = degreeType,
					Institute = institute,
					BoardUniversity = boardUniversity,
					IsCGPA = false,
					ObtainedMarks = obtainedMarks,
					TotalMarks = totalMarks,
					Percentage = percentage,
					PassingYear = passingYear,
					Subjects = subjects,
				};
			}
		}

		protected void cvtbPercentage_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			var minPercentage = (double)(int)this.ViewState["MinPercentage"];
			var obtainedMarks = this.tbObtainedMarks.Text.ToDouble();
			var totalMarks = this.tbTotalMarks.Text.ToDouble();
			var percentage = obtainedMarks * 100f / totalMarks;
			args.IsValid = minPercentage <= percentage && percentage <= 100d;
		}
	}
}