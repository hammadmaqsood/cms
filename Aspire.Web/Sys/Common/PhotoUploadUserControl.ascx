﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PhotoUploadUserControl.ascx.cs" Inherits="Aspire.Web.Sys.Common.PhotoUploadUserControl" %>

<aspire:AspireModal runat="server" ID="modal" HeaderText="Upload Photo">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alert" />
				<div class="form-inline">
					<div class="form-group">
						<asp:FileUpload CssClass="hidden" AllowMultiple="False" runat="server" ID="fileUpload" />
						<label for="btnBrowsePhoto">Select File:</label>
						<div class="input-group">
							<aspire:AspireTextBox ID="tbFileName" ReadOnly="True" runat="server" ondblclick="browsePhoto();" />
							<div class="input-group-btn">
								<button type="button" id="btnBrowsePhoto" class="btn btn-default" onclick="browsePhoto();">Browse</button>
							</div>
						</div>
					</div>
				</div>
				<p></p>
				<div class="row">
					<div class="col-md-12">
						<div id="cropContainer">
						</div>
						<div style="display: inline-block; vertical-align: top; top: 0">
							<div>
								<label for="imgPreview">Preview:</label>
							</div>
							<img id="imgPreview" alt="" width="200" />
							<asp:HiddenField runat="server" ID="hfBase64Image" />
						</div>
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updatePanelButtons">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="PhotoUpload" Text="Save" OnClick="btnSave_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>
<script type="text/javascript">
	var fileUploadInput;
	var photoModal;
	var imgPhoto;
	var cropContainer;
	var imgPreview;
	function browsePhoto() {
		fileUploadInput.click();
	}
	function clearPhoto() {
		cropContainer.empty().css({ "display": "inline-block", "margin-right": "20px" });
		imgPhoto = $("<img/>").on("error", function () {
			fileUploadInput.val("");
			clearPhoto();
			AspireModal.alert("Error", "File is not a valid.");
		}).appendTo(cropContainer);
		imgPreview[0].src = null;
		cropContainer.closest(".row").hide();
		$("#<%=this.btnSave.ClientID%>").attr("data-enabled", false).addClass("disabled");
	}
	function loadPhoto() {
		clearPhoto();
		var file = $(fileUploadInput)[0].files[0];
		var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
		if ($.inArray(file.name.split('.').pop().toLowerCase(), fileExtension) === -1) {
			fileUploadInput.val("");
			clearPhoto();
			AspireModal.alert("Error", "Only '.jpeg','.jpg', '.png', '.gif', '.bmp' file formats are allowed.");
			return;
		}
		var reader = new FileReader();
		cropContainer.closest(".row").show();
		reader.onload = function (e) {
			$(imgPhoto).attr('src', e.target.result);
			$(imgPhoto).croppie({
				enableExif: true,
				viewport: {
					width: 275,
					height: 275,
					type: 'square'
				},
				boundary: {
					width: 300,
					height: 300
				},
				update: function () {
					$(".croppie-container .cr-slider-wrap", photoModal).css({ "width": "100%" });
					$(imgPhoto).croppie('result',
						{
							type: 'rawcanvas',
							circle: false,
							size: { width: 400, height: 400 },
							format: 'png'
						}).then(function (canvas) {
							var dataURL = canvas.toDataURL();
							$("#<%=this.hfBase64Image.ClientID%>").val(dataURL);
							imgPreview[0].src = dataURL;
							$("#<%=this.btnSave.ClientID%>").attr("data-enabled", true).removeClass("disabled");
						});
				}
			});
		}
		reader.readAsDataURL(file);
	}
	$(function () {
		function init() {
			photoModal = $("#<%=this.modal.ClientID%>");
			fileUploadInput = $("#<%=this.fileUpload.ClientID%>").on("change", loadPhoto);
			imgPhoto = $('img#photo', photoModal);
			cropContainer = $('div#cropContainer', photoModal);
			imgPreview = $('img#imgPreview', photoModal);
			clearPhoto();
		}
		init();
		Sys.Application.add_load(init);
	});
</script>
