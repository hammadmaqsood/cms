﻿using Aspire.BL.Entities.QualityAssurance.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Common
{
	public partial class SurveyUserControl : System.Web.UI.UserControl
	{
		public new AspireBasePage Page => (AspireBasePage)base.Page;
		private int? SurveyConductID => this.Request.GetParameterValue<int>("SurveyConductID");
		private int? StudentID => StudentIdentity.Current?.StudentID;
		private int? RegisteredCourseID => this.Request.GetParameterValue<int>("RegisteredCourseID");
		private int? FacultyMemberID => FacultyIdentity.Current?.FacultyMemberID;
		private int? OfferedCourseID => this.Request.GetParameterValue<int>("OfferedCourseID");

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.SurveyConductID == null)
					throw new InvalidOperationException();

				SurveyStatus status;
				var surveyConduct = BL.QualityAssurance.Common.GetSurveyConduct(this.SurveyConductID.Value, this.StudentID, this.RegisteredCourseID, this.FacultyMemberID, this.OfferedCourseID, out status);
				if (surveyConduct == null)
				{
					switch (status)
					{
						case SurveyStatus.Closed:
							this.OnSurveySubmitted(SurveyEventArgs.Closed);
							break;
						case SurveyStatus.NoRecordFound:
							this.OnSurveySubmitted(SurveyEventArgs.NoRecordFound);
							break;
						case SurveyStatus.NotYetOpened:
							this.OnSurveySubmitted(SurveyEventArgs.NotYetOpened);
							break;
						case SurveyStatus.Open:
							throw new InvalidOperationException();
						case SurveyStatus.AlreadyCompleted:
							this.OnSurveySubmitted(SurveyEventArgs.AlreadyExists);
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
				else
				{
					switch (status)
					{
						case SurveyStatus.Closed:
						case SurveyStatus.NoRecordFound:
						case SurveyStatus.NotYetOpened:
						case SurveyStatus.AlreadyCompleted:
							throw new InvalidOperationException();
						case SurveyStatus.Open:
							this.lbName.Text = "Survey: " + surveyConduct.Survey.SurveyName;
							this.lbDescription.Text = surveyConduct.Survey.Description;
							this.repeaterQuestionGroups.DataBind(surveyConduct.Survey.SurveyQuestionGroups.OrderBy(g => g.DisplayIndex));
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (!this.Page.IsValid)
				return;
			this.OnSurveySubmitted(this.SaveSurvey());
		}

		#region Events

		public sealed class SurveyEventArgs : EventArgs
		{
			public enum Results
			{
				AlreadyExists,
				Cancelled,
				Closed,
				NoRecordFound,
				NotYetOpened,
				Success,
			}

			public Results Result { get; private set; }

			private SurveyEventArgs()
			{
			}

			public static SurveyEventArgs Success => new SurveyEventArgs { Result = Results.Success };

			public static SurveyEventArgs Cancel => new SurveyEventArgs { Result = Results.Cancelled };

			public static SurveyEventArgs AlreadyExists => new SurveyEventArgs { Result = Results.AlreadyExists };

			public static SurveyEventArgs NotYetOpened => new SurveyEventArgs { Result = Results.NotYetOpened };

			public static SurveyEventArgs NoRecordFound => new SurveyEventArgs { Result = Results.NoRecordFound };

			public static SurveyEventArgs Closed => new SurveyEventArgs { Result = Results.Closed };
		}

		public delegate void SurveySubmittedHandler(object sender, SurveyEventArgs e);

		public event SurveySubmittedHandler SurveySubmitted;

		protected virtual void OnSurveySubmitted(SurveyEventArgs args)
		{
			if (this.SurveySubmitted != null)
				this.SurveySubmitted(this, args);
			else
				throw new NotImplementedException("SurveySubmitted event is not handled. ID: " + this.ID);
		}

		#endregion

		protected void lbtnCancel_OnClick(object sender, EventArgs e)
		{
			this.OnSurveySubmitted(SurveyEventArgs.Cancel);
		}

		private SurveyEventArgs SaveSurvey()
		{
			if (this.SurveyConductID == null)
				throw new InvalidOperationException();

			var surveyUserAnswers = new List<SurveyUserAnswer>();
			foreach (RepeaterItem itemGroup in this.repeaterQuestionGroups.Items)
			{
				if (itemGroup.ItemType != ListItemType.Item && itemGroup.ItemType != ListItemType.AlternatingItem)
					continue;
				var repeater = (Repeater)itemGroup.FindControl("repeaterQuestions");
				foreach (RepeaterItem item in repeater.Items)
				{
					if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
						continue;
					var divOptions = item.FindControl("divOptions");
					var divtb = item.FindControl("divtb");
					var rbl = (AspireRadioButtonList)item.FindControl("rbl");
					var hfQID = (Aspire.Lib.WebControls.HiddenField)item.FindControl("hfQID");
					var tb = (AspireTextBox)item.FindControl("tb");
					surveyUserAnswers.Add(new SurveyUserAnswer
					{
						SurveyQuestionID = hfQID.Value.ToInt(),
						SurveyQuestionOptionID = divOptions.Visible ? rbl.SelectedValue.ToInt() : (int?)null,
						OptionText = divtb.Visible ? tb.Text.Trim() : null,
					});
				}
			}

			var surveySubmissionResult = Aspire.BL.QualityAssurance.Common.SubmitSurvey(this.SurveyConductID.Value, this.StudentID, this.RegisteredCourseID, this.FacultyMemberID, this.OfferedCourseID, surveyUserAnswers, AspireIdentity.Current.UserLoginHistoryID);
			switch (surveySubmissionResult)
			{
				case SurveySubmissionResults.Success:
					return SurveyEventArgs.Success;
				case SurveySubmissionResults.AlreadyExists:
					return SurveyEventArgs.AlreadyExists;
				case SurveySubmissionResults.NotOpenedYet:
					return SurveyEventArgs.NotYetOpened;
				case SurveySubmissionResults.SurveyClosed:
					return SurveyEventArgs.Closed;
				case SurveySubmissionResults.NoRecordFound:
					return SurveyEventArgs.NoRecordFound;
				default:
					throw new ArgumentOutOfRangeException(nameof(surveySubmissionResult), surveySubmissionResult, null);
			}
		}

		public void DisplayMessageAndRedirect<T>(SurveyEventArgs args) where T : AspireBasePage
		{
			switch (args.Result)
			{
				case SurveyEventArgs.Results.AlreadyExists:
					this.Page.AddErrorAlert("Survey has already been submitted.");
					break;
				case SurveyEventArgs.Results.Cancelled:
					break;
				case SurveyEventArgs.Results.Closed:
					this.Page.AddErrorAlert("Survey has been closed.");
					break;
				case SurveyEventArgs.Results.NoRecordFound:
					this.Page.AddNoRecordFoundAlert();
					break;
				case SurveyEventArgs.Results.NotYetOpened:
					this.Page.AddErrorAlert("Survey is not available now.");
					break;
				case SurveyEventArgs.Results.Success:
					this.Page.AddSuccessAlert("Survey has been submitted.");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			BasePage.Redirect<T>();
		}
	}
}