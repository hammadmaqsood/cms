﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SurveyUserControl.ascx.cs" Inherits="Aspire.Web.Sys.Common.SurveyUserControl" %>
<%@ Import Namespace="Aspire.Model.Entities" %>

<div class="row">
	<div class="col-md-12">
		<aspire:Label CssClass="h3" runat="server" ID="lbName" />
		<aspire:Label CssClass="help-block" runat="server" ID="lbDescription" />
	</div>
</div>
<asp:Repeater ID="repeaterQuestionGroups" runat="server">
	<ItemTemplate>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><%#:Eval("QuestionGroupName")%></h3>
			</div>
			<div class="list-group">
				<asp:Repeater runat="server" ID="repeaterQuestions" DataSource='<%# ((IEnumerable<SurveyQuestion>)Eval("SurveyQuestions")).OrderBy(q=> q.DisplayIndex) %>'>
					<ItemTemplate>
						<div class="list-group-item">
							<aspire:HiddenField runat="server" ID="hfQID" Value='<%# Eval("SurveyQuestionID") %>' Mode="Encrypted" />
							<div class="form-horizontal">
								<div id="divOptions" class="form-group" runat="server" visible='<%# (SurveyQuestion.QuestionTypes)Eval("QuestionTypeEnum")==SurveyQuestion.QuestionTypes.Options %>'>
									<div class="col-md-6">
										<span class="control-label"><%#: Eval("QuestionNo") %>. <%#: Eval("QuestionText") %></span>
									</div>
									<div class="col-md-6">
										<div class="form-inline">
											<aspire:AspireRadioButtonList runat="server" ID="rbl" ValidationGroup="Survey" DataSource='<%# Eval("SurveyQuestionOptions") %>' DataTextField="OptionText" DataValueField="SurveyQuestionOptionID" AutoPostBack="False" CausesValidation="False" RepeatDirection="Horizontal" RepeatLayout="Flow" />
										</div>
										<aspire:AspireRequiredFieldValidator ErrorMessage="This field is required." ControlToValidate="rbl" ValidationGroup="Survey" runat="server" />
									</div>
								</div>
								<div id="divtb" class="form-group" runat="server" visible='<%# (SurveyQuestion.QuestionTypes)Eval("QuestionTypeEnum")==SurveyQuestion.QuestionTypes.Text %>'>
									<div class="col-md-12">
										<span class="control-label"><%#: Eval("QuestionNo") %>. <%#: Eval("QuestionText") %></span>
										<aspire:AspireTextBox runat="server" ID="tb" ValidationGroup="Survey" MaxLength="4096" TextMode="MultiLine" />
									</div>
								</div>
							</div>
						</div>
					</ItemTemplate>
				</asp:Repeater>
			</div>
		</div>
	</ItemTemplate>
</asp:Repeater>
<div class="text-center">
	<aspire:AspireButton runat="server" ButtonType="Primary" Text="Submit" ValidationGroup="Survey" ID="btnSubmit" OnClick="btnSubmit_Click" />
	<aspire:AspireLinkButton runat="server" Text="Cancel" CausesValidation="False" ID="lbtnCancel" OnClick="lbtnCancel_OnClick" />
</div>
