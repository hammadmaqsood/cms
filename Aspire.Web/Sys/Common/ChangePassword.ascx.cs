﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Common
{
	public partial class ChangePassword : System.Web.UI.UserControl
	{
		private new AspireBasePage Page => (AspireBasePage)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
		}

		protected void btnSubmit_OnClick(object sender, EventArgs e)
		{
			if (!this.Page.IsValid)
				return;
			var currentPassword = this.tbCurrentPassword.Text;
			var newPassword = this.tbNewPassword.Text;
			var result = BL.Core.LoginManagement.Common.ChangePassword(currentPassword, newPassword, AspireIdentity.Current.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.LoginManagement.Common.ChangePasswordResult.CurrentPasswordDoNotMatch:
					this.Page.AddErrorAlert("Current Password is incorrect.");
					return;
				case BL.Core.LoginManagement.Common.ChangePasswordResult.NewPasswordIsSameAsCurrentPassword:
					this.Page.AddErrorAlert("New passwords must be different from current password.");
					return;
				case BL.Core.LoginManagement.Common.ChangePasswordResult.NoRecordFound:
					this.Page.AddNoRecordFoundAlert();
					break;
				case BL.Core.LoginManagement.Common.ChangePasswordResult.StatusIsNotActive:
					break;
				case BL.Core.LoginManagement.Common.ChangePasswordResult.Success:
					this.Page.AddSuccessAlert("Password has been changed.");
					break;
				default:
					throw new NotImplementedEnumException(result);
			}

			switch (AspireIdentity.Current.UserType)
			{
				case Model.Entities.Common.UserTypes.MasterAdmin:
				case Model.Entities.Common.UserTypes.Admin:
				case Model.Entities.Common.UserTypes.Admins:
					BasePage.Redirect<Sys.Admin.Dashboard>();
					return;
				case Model.Entities.Common.UserTypes.Executive:
					BasePage.Redirect<Sys.Executive.Dashboard>();
					return;
				case Model.Entities.Common.UserTypes.Staff:
					BasePage.Redirect<Sys.Staff.Dashboard>();
					return;
				case Model.Entities.Common.UserTypes.Candidate:
					BasePage.Redirect<Sys.Candidate.Dashboard>();
					return;
				case Model.Entities.Common.UserTypes.Student:
					BasePage.Redirect<Sys.Student.Dashboard>();
					return;
				case Model.Entities.Common.UserTypes.Faculty:
					BasePage.Redirect<Sys.Faculty.Dashboard>();
					return;
				default:
					throw new NotImplementedEnumException(AspireIdentity.Current.UserType);
			}
		}
	}
}