﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommentComplaint.ascx.cs" Inherits="Aspire.Web.Sys.Common.Complaints.ComplaintComment" %>
<aspire:AspireModal runat="server" ID="modalComplaintComment" HeaderText="Comments">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<table class="table table-bordered table-condensed table-hover table-striped">
					<thead>
						<tr>
							<th>Name</th>
							<th>Description</th>
						</tr>
					</thead>
					<tbody>
						<asp:Repeater runat="server" ID="repeaterComplaintComment" ItemType="Aspire.BL.Core.Complaints.Common.ComplaintComment.ComplaintCommentResult">
							<ItemTemplate>
								<tr>
									<td><%#: Item.CommentedBy %></td>
									<td><%#: Item.Text %>
										<aspire:Label runat="server" CssClass="help-block pull-right" Text="<%#:Item.CommentedDate %>" Font-Italic="True" />
									</td>
								</tr>
							</ItemTemplate>
							<FooterTemplate>
								<tr runat="server" visible="<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>">
									<td colspan="10">No record found.</td>
								</tr>
								</tbody>
							</table>
							</FooterTemplate>
						</asp:Repeater>
						<tr>
							<td colspan="2">
								<aspire:AspireTextBox runat="server" ID="tbAddComment" PlaceHolder="Write a comment..." TextMode="MultiLine" />
							</td>
						</tr>
					</tbody>
				</table>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnSave" Text="Comment" ButtonType="Primary" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>
