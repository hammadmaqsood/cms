﻿using System;
using Aspire.Web.Common;
using Aspire.Web.Sys.Staff.Complaints;
using Aspire.Web.Sys.Student.Complaints;

namespace Aspire.Web.Sys.Common.Complaints
{
	public partial class ComplaintComment : System.Web.UI.UserControl
	{
		private StudentComplaints StudentComplaintPage => (StudentComplaints)base.Page;
		private RegisterComplaints RegisterComplaintPage => (RegisterComplaints)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
			}
		}

		public void ShowModal(Guid complaintID)
		{
			//this.Visible = true;
			var complaintComments = BL.Core.Complaints.Common.ComplaintComment.GetComplaintComments(complaintID, AspireIdentity.Current.LoginSessionGuid);
			this.repeaterComplaintComment.DataBind(complaintComments);
			this.modalComplaintComment.Show();
		}
	}
}