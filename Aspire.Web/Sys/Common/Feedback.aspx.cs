﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Common
{
	[AspirePage("23B1FC38-F8FD-4099-B4CF-E8E0AEBF18C5", UserTypes.Executive, "~/Sys/Common/Feedback.aspx", "Feedback", false, AspireModules.None)]
	public partial class Feedback : AspireBasePage
	{
		private static IEnumerable<UserTypes> AllowedUserTypes => new[] { UserTypes.Executive, UserTypes.Staff, UserTypes.Faculty, };
		public override AspireThemes AspireTheme => AspireDefaultThemes.GetDefaultThemeForCurrentUser();
		public override PageIcon PageIcon => AspireGlyphicons.comment.GetIcon();
		public override string PageTitle => "Feedback";

		protected override void Authenticate()
		{
			var userType = this.AspireIdentity?.UserType;
			if (userType == null || !AllowedUserTypes.Contains(userType.Value))
				throw new AspireAccessDeniedException(UserTypes.Staff, UserTypes.Staff);
		}

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void lbtnCancel_OnClick(object sender, EventArgs e)
		{
			Default.RedirectToHomeScreen();
		}

		protected void btnSubmit_OnClick(object sender, EventArgs e)
		{
			var toList = new[] { (string.Empty, "cmssupport@bahria.edu.pk") };
			var ccList = new[] { (this.AspireIdentity.Name, this.AspireIdentity.Email) };
			var replyToList = new[] { (this.AspireIdentity.Name, this.AspireIdentity.Email) };
			var subject = this.tbSubject.Text;
			var emailBody = this.tbDescription.Text;
			(string, byte[])[] attachments = null;
			if (this.fileUpload.HasFile)
				attachments = new[] { (this.fileUpload.FileName, this.fileUpload.FileBytes) };
			BL.Core.Common.EmailsManagement.EmailHelper.AddEmailAndSaveChanges(Model.Entities.Email.EmailTypes.UserFeedback, subject, emailBody, false, Model.Entities.Email.Priorities.Highest, DateTime.Now, null, this.AspireIdentity.LoginSessionGuid, toList: toList, ccList: ccList, bccList: null, replyToList: replyToList, attachmentList: attachments);
			this.AddSuccessAlert("Your feedback has been submitted.");
			Redirect(Default.GetDashboardPage().GetAspirePageAttribute().PageUrl);
		}
	}
}