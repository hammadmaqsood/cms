﻿using System;

namespace Aspire.Web.Sys.Common
{
	public partial class PhotoUploadUserControl : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (this.IsPostBack)
			{
				this.Visible = false;
			}
		}

		public void ShowModal()
		{
			this.Visible = true;
			this.modal.Show();
		}

		public sealed class SaveEventArgs : EventArgs
		{
			public enum Statuses
			{
				NotHandled,
				Success,
				InvalidImage
			}

			public byte[] Bytes { get; }
			public Statuses Status { get; set; }

			public SaveEventArgs(byte[] bytes)
			{
				this.Bytes = bytes;
				this.Status = Statuses.NotHandled;
			}
		}

		public delegate void OnSave(object sender, SaveEventArgs e);

		public event OnSave Save;

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.Page.IsValid)
				return;
			var base64Image = this.hfBase64Image.Value;
			var prefix = "data:image/png;base64,";
			if (base64Image?.StartsWith(prefix, StringComparison.InvariantCultureIgnoreCase) != true)
				throw new Exception($"Invalid Image Uploaded. Contents: {base64Image?.Substring(0, 50)}...");

			var bytes = Convert.FromBase64String(base64Image.Substring(prefix.Length));
			var saveEventArgs = new SaveEventArgs(bytes);
			this.Save?.Invoke(this, saveEventArgs);

			switch (saveEventArgs.Status)
			{
				case SaveEventArgs.Statuses.NotHandled:
					throw new InvalidOperationException("Event not handled.");
				case SaveEventArgs.Statuses.Success:
					break;
				case SaveEventArgs.Statuses.InvalidImage:
					this.alert.AddErrorAlert("Invalid image uploaded.");
					this.updatePanel.Update();
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(saveEventArgs.Status), saveEventArgs.Status, null);
			}
		}
	}
}