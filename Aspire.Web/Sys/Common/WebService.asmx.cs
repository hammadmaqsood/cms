﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Common
{
	[WebService(Namespace = "http://cms.bahria.edu.pk/Sys/Common/WebService.asmx")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	[ScriptService]
	public sealed class WebService : System.Web.Services.WebService
	{
		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public List<Aspire.BL.Core.Common.Lists.Student> SearchStudents(string enrollment, int? registrationNo, string name, string fatherName, string cnic, short? intakeSemesterID, int? programID, string sortExpression, string sortDirection)
		{
			var loginSessionGuid = AspireIdentity.Current?.LoginSessionGuid;
			if (loginSessionGuid == null)
				return null;
			return Aspire.BL.Core.Common.Lists.SearchStudents(enrollment.TrimAndMakeItNullIfEmpty(), registrationNo, name.TrimAndMakeItNullIfEmpty(), fatherName.TrimAndMakeItNullIfEmpty(), cnic.TrimAndMakeItNullIfEmpty(), intakeSemesterID, programID, sortExpression.TrimAndMakeItNullIfEmpty(), sortDirection == "Ascending" ? SortDirection.Ascending : SortDirection.Descending, loginSessionGuid.Value);
		}

		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public object GetSemestersAndPrograms()
		{
			var semesters = Aspire.BL.Core.Common.Lists.GetSemestersList(null)
				.Select(s => new
				{
					Semester = s.ToSemesterString(),
					SemesterID = s,
				}).ToList();

			int? instituteID;
			switch (AspireIdentity.Current?.UserType)
			{
				case null:
					return null;
				case UserTypes.Anonymous:
				case UserTypes.System:
				case UserTypes.Admins:
				case UserTypes.Any:
				case UserTypes.ManualSQL:
				case UserTypes.IntegratedService:
				case UserTypes.Alumni:
				case UserTypes.Student:
				case UserTypes.Candidate:
					throw new InvalidOperationException();
				case UserTypes.MasterAdmin:
				case UserTypes.Admin:
					instituteID = null;
					break;
				case UserTypes.Executive:
					instituteID = ExecutiveIdentity.Current.InstituteID;
					break;
				case UserTypes.Staff:
					instituteID = StaffIdentity.Current.InstituteID;
					break;
				case UserTypes.Faculty:
					instituteID = FacultyIdentity.Current.InstituteID;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			return new
			{
				Semesters = semesters,
				Programs = instituteID != null ? BL.Core.Common.Lists.GetPrograms(instituteID.Value, null, true) : null,
			};
		}
	}
}