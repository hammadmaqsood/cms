﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.IO;
using System.Linq;
using System.Web;

namespace Aspire.Web.Sys.Common
{
	public class StudentPicture : AspireHandler
	{
		public static string GetImageUrl(int? studentID, TimeSpan? cacheTimeSpan)
		{
			return "~/Sys/Common/StudentPicture.ashx".AttachQueryParams(nameof(StudentID), studentID, nameof(CacheTimeSpan), cacheTimeSpan, EncryptionModes.ConstantSaltUserSession);
		}

		private int? StudentID => this.Context.Request.GetParameterValue<int>(nameof(this.StudentID));
		private TimeSpan CacheTimeSpan => this.Context.Request.GetParameterValue<TimeSpan>(nameof(this.CacheTimeSpan)) ?? TimeSpan.Zero;

		protected override void ProcessRequest()
		{
			byte[] bytes = null;
			var userTypeEnum = AspireIdentity.Current?.UserType;
			if (userTypeEnum != null)
			{
				var loginSessionGuid = AspireIdentity.Current.LoginSessionGuid;
				switch (userTypeEnum.Value)
				{
					case UserTypes.Alumni:
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.Candidate:
					case UserTypes.Faculty:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Any:
						break;
					case UserTypes.Admin:
					case UserTypes.Admins:
					case UserTypes.MasterAdmin:
						bytes = BL.Core.StudentInformation.Admin.Students.ReadStudentPicture(this.StudentID.Value, loginSessionGuid);
						break;
					case UserTypes.Staff:
						bytes = BL.Core.StudentInformation.Staff.StudentProfile.ReadStudentPicture(this.StudentID.Value, loginSessionGuid);
						break;
					case UserTypes.Executive:
						bytes = BL.Core.ExecutiveInformation.Executive.Students.StudentProfile.ReadStudentPicture(this.StudentID.Value, loginSessionGuid);
						break;
					case UserTypes.Student:
						bytes = BL.Core.StudentInformation.Student.Profile.ReadStudentPicture(loginSessionGuid);
						break;
					default:
						throw new NotImplementedEnumException(AspireIdentity.Current?.UserType);
				}
			}
			SendPictureToResponse(this.Context, bytes, this.CacheTimeSpan);
		}

		internal static void SendPictureToResponse(HttpContext context, byte[] bytes, TimeSpan cacheTimeSpan)
		{
			var response = context.Response;
			if (bytes?.Any() != true)
				bytes = File.ReadAllBytes(context.Server.MapPath("~/Images/MaleAvatar.png"));

			response.Cache.SetExpires(DateTime.Now.Add(cacheTimeSpan));
			response.Cache.SetCacheability(HttpCacheability.Public);
			response.Cache.SetValidUntilExpires(false);

			response.Buffer = true;
			response.Charset = "";
			response.ContentType = "image/png";
			response.AddHeader("content-disposition", "inline;filename=ProfilePicture.png");
			response.BinaryWrite(bytes);
			response.Flush();
			response.End();
		}

		protected override void Authenticate()
		{
		}

		public override bool IsReusable => true;
	}
}