﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StaffV1.ascx.cs" Inherits="Aspire.Web.Sys.Common.QA.Surveys.SurveyForm8.StaffV1" %>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">1.	Student Knowledge</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Math, Science, Humanities and professional discipline, (if applicable) </h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ1p1" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ1p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Problem formulation and solving skills</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ1p2" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ1p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Collecting and analyzing appropriate data </h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ1p3" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ1p3" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">4.	Ability to link theory to practice</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ1p4" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ1p4" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">5.	Ability to design a system component or process</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ1p5" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ1p5" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">6.	Computer knowledge</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ1p6" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ1p6" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">2.	Communications Skills</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Oral communication</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ2p1" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ2p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Report writing</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ2p2" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ2p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Presentation skills</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ2p3" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ2p3" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">3.	Interpersonal Skills</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Ability to work in teams</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ3p1" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ3p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Leadership</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ3p2" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ3p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Independent thinking</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ3p3" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ3p3" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">4.	Motivation</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ3p4" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ3p4" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">5.	Reliability</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ3p5" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ3p5" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">6.	Appreciation of ethical Values</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ3p6" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ3p6" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">4.	Work Skills</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Time management skills</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ4p1" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ4p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Judgment</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ4p2" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ4p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Discipline</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ4p3" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ4p3" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">5.	General Comments</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">Please make any additional comments or suggestions, which you think would help us to strengthen our programs for the preparation of graduates who will enter your field. What professional quality & skills, your organization would like to see in BU Graduates?
				</h4>
				<p class="list-group-item-text">
					<aspire:AspireTextBox runat="server" ID="tbQ5" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ5" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">6.	Information About Organization</h3>
	</div>
	<div class="panel-body">
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="1.	Organization Name" AssociatedControlID="tbQ6P1" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ6p1" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ6p1" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="2.	Type of Business" AssociatedControlID="tbQ6P2" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ6p2" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ6p2" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-12">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="3.	Number of Graduates (specify the program) in your Organization" AssociatedControlID="tbQ6p3" />
			</div>
			<div class="col-md-12">
				<aspire:AspireTextBox runat="server" ID="tbQ6p3" ValidationGroup="SaveAndSubmit" TextMode="MultiLine" MaxLength="4096" />
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ6p3" />
			</div>
		</div>
	</div>
</div>

<div class="form-group text-center">
	<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save" ID="btnSave" OnClick="btnSave_Click" />
	<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save & Submit" ValidationGroup="SaveAndSubmit" ConfirmMessage="Are you sure you want to submit?" ID="btnSubmit" OnClick="btnSubmit_Click" />
	<aspire:AspireLinkButton runat="server" Text="Cancel" CausesValidation="False" ID="lbtnCancel" OnClick="lbtnCancel_Click" />
</div>
