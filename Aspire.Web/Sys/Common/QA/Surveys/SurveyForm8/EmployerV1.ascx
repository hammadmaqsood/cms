﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployerV1.ascx.cs" Inherits="Aspire.Web.Sys.Common.QA.Surveys.SurveyForm8.EmployerV1" %>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">1.	Student Knowledge</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Math, Science, Humanities and professional discipline, (if applicable) </h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ1p1" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ1p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Problem formulation and solving skills</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ1p2" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ1p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Collecting and analyzing appropriate data </h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ1p3" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ1p3" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">4.	Ability to link theory to practice</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ1p4" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ1p4" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">5.	Ability to design a system component or process</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ1p5" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ1p5" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">6.	Computer knowledge</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ1p6" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ1p6" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">2.	Communications Skills</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Oral communication</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ2p1" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ2p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Report writing</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ2p2" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ2p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Presentation skills</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ2p3" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ2p3" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">3.	Interpersonal Skills</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Ability to work in teams</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ3p1" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ3p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Leadership</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ3p2" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ3p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Independent thinking</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ3p3" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ3p3" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">4.	Motivation</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ3p4" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ3p4" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">5.	Reliability</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ3p5" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ3p5" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">6.	Appreciation of ethical Values</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ3p6" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ3p6" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">4.	Work Skills</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Time management skills</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ4p1" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ4p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Judgment</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ4p2" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ4p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Discipline</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ4p3" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ4p3" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">5.	General Comments</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">Please make any additional comments or suggestions, which you think would help us to strengthen our programs for the preparation of graduates who will enter your field. What professional quality & skills, your organization would like to see in BU Graduates?
				</h4>
				<p class="list-group-item-text">
					<aspire:aspiretextbox runat="server" id="tbQ5" validationgroup="SaveAndSubmit" maxlength="4096" textmode="MultiLine" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="tbQ5" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">6.	Information About Organization</h3>
	</div>
	<div class="panel-body">
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:aspirelabel runat="server" font-size="Large" font-bold="false" text="1.	Organization Name" associatedcontrolid="tbQ6P1" />
			</div>
			<div class="col-md-3">
				<aspire:aspiretextbox runat="server" id="tbQ6p1" validationgroup="SaveAndSubmit" />
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="tbQ6p1" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:aspirelabel runat="server" font-size="Large" font-bold="false" text="2.	Type of Business" associatedcontrolid="tbQ6P2" />
			</div>
			<div class="col-md-3">
				<aspire:aspiretextbox runat="server" id="tbQ6p2" validationgroup="SaveAndSubmit" />
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="tbQ6p2" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-12">
				<aspire:aspirelabel runat="server" font-size="Large" font-bold="false" text="3.	Number of Graduates (specify the program) in your Organization" associatedcontrolid="tbQ6p3" />
			</div>
			<div class="col-md-12">
				<aspire:aspiretextbox runat="server" id="tbQ6p3" validationgroup="SaveAndSubmit" textmode="MultiLine" maxlength="4096" />
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="tbQ6p3" />
			</div>
		</div>
	</div>
</div>

<div class="form-group text-center">
	<aspire:aspirebutton runat="server" buttontype="Primary" text="Save" id="btnSave" enabled="false" onclick="btnSave_Click" />
	<aspire:aspirebutton runat="server" buttontype="Primary" text="Save & Submit" enabled="false" validationgroup="SaveAndSubmit" confirmmessage="Are you sure you want to submit?" id="btnSubmit" onclick="btnSubmit_Click" />
</div>
