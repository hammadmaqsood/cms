﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AlumniV1.ascx.cs" Inherits="Aspire.Web.Sys.Common.QA.Surveys.SurveyForm7.AlumniV1" %>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">1.	Knowledge</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Math, Science, Humanities and professional discipline, (if applicable) </h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ1p1" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ1p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Problem formulation and solving skills</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ1p2" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ1p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Collecting and analyzing appropriate data </h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ1p3" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ1p3" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">4.	Ability to link theory to practice</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ1p4" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ1p4" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">5.	Ability to design a system component or process</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ1p5" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ1p5" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">6.	IT knowledge</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ1p6" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ1p6" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">2.	Communications Skills</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Oral communication</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ2p1" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ2p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Report writing</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ2p2" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ2p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Presentation skills</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ2p3" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ2p3" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">3.	Interpersonal Skills</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Ability to work in teams</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ3p1" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ3p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Ability to work in arduous /Challenging situation</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ3p2" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ3p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Independent thinking</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ3p3" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ3p3" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">4.	Appreciation of ethical Values</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ3p4" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ3p4" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">4.	Management /leadership Skills</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Resource and Time management skills</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ4p1" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ4p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Judgment</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ4p2" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ4p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Discipline</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ4p3" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ4p3" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">5.	General Comments</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">Please make any additional comments or suggestions, which you think would help strengthen our programs. (New courses that you would recommend and courses that you did not gain much from)</h4>
				<p class="list-group-item-text">
					<aspire:AspireTextBox runat="server" ID="tbQ5" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ5" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">6.	Career Opportunities</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">Career Opportunities / Counseling provided by the department / university</h4>
				<p class="list-group-item-text">
					<aspire:AspireTextBox runat="server" ID="tbQ6" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ6" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">7.	Department Status</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Infrastructure</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ7p1" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ7p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Faculty</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ7p2" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ7p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Repute at National level</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ7p3" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ7p3" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">4.	Repute at international level</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" ID="rblQ7p4" RepeatDirection="Horizontal" RepeatLayout="Flow" />
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ7p4" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">8.	Alumni Information</h3>
	</div>
	<div class="panel-body">
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="1. Name" AssociatedControlID="tbQ8P1" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ8p1" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireRequiredFieldValidator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ8P1" AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="2. Name of organization" AssociatedControlID="tbQ8p2" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ8p2" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireRequiredFieldValidator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ8p2" AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="3. Position in organization" AssociatedControlID="tbQ8p3" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ8p3" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireRequiredFieldValidator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ8p3" AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="4.	Year of graduation" AssociatedControlID="dpQ8p4" />
			</div>
			<div class="col-md-3">
				<aspire:AspireDateTimePickerTextbox runat="server" ID="dpQ8p4" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireRequiredFieldValidator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="dpQ8p4" AllowNull="false" />
				<aspire:AspireAlert ID="dpAlert" runat="server" />
			</div>
		</div>
	</div>
</div>
<div class="form-group text-center">
	<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save" ID="btnSave" Enabled="false" OnClick="btnSave_Click" />
	<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save & Submit" Enabled="false" ValidationGroup="SaveAndSubmit" ConfirmMessage="Are you sure you want to submit?" ID="btnSubmit" OnClick="btnSubmit_Click" />
</div>