﻿using Aspire.BL.Core.QualityAssurance.Common;
using Aspire.BL.Core.QualityAssurance.Common.Schemas;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm7;
using System;
using System.Web.UI;

namespace Aspire.Web.Sys.Common.QA.Surveys.SurveyForm7
{
	public partial class StaffV1 : UserControl
	{
		private IAlert BasePage => this.Page as IAlert;
		public void LoadData(int qaSurveyUserID)
		{
			if (qaSurveyUserID == null || qaSurveyUserID == 0)
				RefreshPage();
			this.QASurveyUserID = qaSurveyUserID;
			this.BindRadioButtons();
			this.GetSurveyUserResult = QASurveyUsers.GetSurvey7StaffUser(qaSurveyUserID, AspireIdentity.Current.LoginSessionGuid);
		}
		private void BindRadioButtons()
		{
			var datasource = QASurveyUsers.GetSurveyOptionList();
			this.rblQ1p1.DataBind(datasource);
			this.rblQ1p2.DataBind(datasource);
			this.rblQ1p3.DataBind(datasource);
			this.rblQ1p4.DataBind(datasource);
			this.rblQ1p5.DataBind(datasource);
			this.rblQ1p6.DataBind(datasource);
			this.rblQ2p1.DataBind(datasource);
			this.rblQ2p2.DataBind(datasource);
			this.rblQ2p3.DataBind(datasource);
			this.rblQ3p1.DataBind(datasource);
			this.rblQ3p2.DataBind(datasource);
			this.rblQ3p3.DataBind(datasource);
			this.rblQ3p4.DataBind(datasource);
			this.rblQ4p1.DataBind(datasource);
			this.rblQ4p2.DataBind(datasource);
			this.rblQ4p3.DataBind(datasource);
			this.rblQ7p1.DataBind(datasource);
			this.rblQ7p2.DataBind(datasource);
			this.rblQ7p3.DataBind(datasource);
			this.rblQ7p4.DataBind(datasource);
		}
		private int QASurveyUserID
		{
			get => (int)this.ViewState[nameof(this.QASurveyUserID)];
			set => this.ViewState[nameof(this.QASurveyUserID)] = value;
		}
		public QASurveyUsers.GetSurveyUserResult GetSurveyUserResult
		{
			set
			{
				this.QASurveyUserID = value.QASurveyUser.QASurveyUserID;
				bool isReadonly = value.AlumniCanEdit == false;
				switch (AspireIdentity.Current.UserType)
				{
					case UserTypes.Staff:
						isReadonly = true;
						break;
					case UserTypes.Anonymous:
						isReadonly = value.AlumniCanEdit == false;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				var qaSurveyUser = value.QASurveyUser;
				this.BindRadioButtons();
				if (qaSurveyUser.SurveyValues != null && (qaSurveyUser.StatusEnum == QASurveyUser.Statuses.InProgress || qaSurveyUser.StatusEnum == QASurveyUser.Statuses.Completed))
				{
					var surveyValues = qaSurveyUser.SurveyValues.FromJsonString<SurveyForm7Version1>();
					this.rblQ1p1.SelectedValue = surveyValues.Q1P1?.ToString();
					this.rblQ1p2.SelectedValue = surveyValues.Q1P2?.ToString();
					this.rblQ1p3.SelectedValue = surveyValues.Q1P3?.ToString();
					this.rblQ1p4.SelectedValue = surveyValues.Q1P4?.ToString();
					this.rblQ1p5.SelectedValue = surveyValues.Q1P5?.ToString();
					this.rblQ1p6.SelectedValue = surveyValues.Q1P6?.ToString();
					this.rblQ2p1.SelectedValue = surveyValues.Q2P1?.ToString();
					this.rblQ2p2.SelectedValue = surveyValues.Q2P2?.ToString();
					this.rblQ2p3.SelectedValue = surveyValues.Q2P3?.ToString();
					this.rblQ3p1.SelectedValue = surveyValues.Q3P1?.ToString();
					this.rblQ3p2.SelectedValue = surveyValues.Q3P2?.ToString();
					this.rblQ3p3.SelectedValue = surveyValues.Q3P3?.ToString();
					this.rblQ3p4.SelectedValue = surveyValues.Q3P4?.ToString();
					this.rblQ4p1.SelectedValue = surveyValues.Q4P1?.ToString();
					this.rblQ4p2.SelectedValue = surveyValues.Q4P2?.ToString();
					this.rblQ4p3.SelectedValue = surveyValues.Q4P3?.ToString();
					this.tbQ5.Text = surveyValues.Q5;
					this.tbQ6.Text = surveyValues.Q6;
					this.rblQ7p1.SelectedValue = surveyValues.Q7P1?.ToString();
					this.rblQ7p2.SelectedValue = surveyValues.Q7P2?.ToString();
					this.rblQ7p3.SelectedValue = surveyValues.Q7P3?.ToString();
					this.rblQ7p4.SelectedValue = surveyValues.Q7P4?.ToString();
					this.tbQ8p1.Text = surveyValues.Q8P1;
					this.tbQ8p2.Text = surveyValues.Q8P2;
					this.tbQ8p3.Text = surveyValues.Q8P3;
					this.dpQ8p4.SelectedDate = surveyValues.Q8P4?.ToLocalTime();
				}
				if (isReadonly)
				{
					this.btnSave.Enabled = false;
					this.btnSubmit.Enabled = false;
				}
				else
				{
					this.btnSave.Enabled = true;
					this.btnSubmit.Enabled = true;
				}
			}
		}

		private SurveyForm7Version1 GetFormValues()
		{
			return new SurveyForm7Version1
			{
				Q1P1 = this.rblQ1p1.SelectedValue.ToNullableByte(),
				Q1P2 = this.rblQ1p2.SelectedValue.ToNullableByte(),
				Q1P3 = this.rblQ1p3.SelectedValue.ToNullableByte(),
				Q1P4 = this.rblQ1p4.SelectedValue.ToNullableByte(),
				Q1P5 = this.rblQ1p5.SelectedValue.ToNullableByte(),
				Q1P6 = this.rblQ1p6.SelectedValue.ToNullableByte(),
				Q2P1 = this.rblQ2p1.SelectedValue.ToNullableByte(),
				Q2P2 = this.rblQ2p2.SelectedValue.ToNullableByte(),
				Q2P3 = this.rblQ2p3.SelectedValue.ToNullableByte(),
				Q3P1 = this.rblQ3p1.SelectedValue.ToNullableByte(),
				Q3P2 = this.rblQ3p2.SelectedValue.ToNullableByte(),
				Q3P3 = this.rblQ3p3.SelectedValue.ToNullableByte(),
				Q3P4 = this.rblQ3p4.SelectedValue.ToNullableByte(),
				Q4P1 = this.rblQ4p1.SelectedValue.ToNullableByte(),
				Q4P2 = this.rblQ4p2.SelectedValue.ToNullableByte(),
				Q4P3 = this.rblQ4p3.SelectedValue.ToNullableByte(),
				Q5 = this.tbQ5.Text,
				Q6 = this.tbQ6.Text,
				Q7P1 = this.rblQ7p1.SelectedValue.ToNullableByte(),
				Q7P2 = this.rblQ7p2.SelectedValue.ToNullableByte(),
				Q7P3 = this.rblQ7p3.SelectedValue.ToNullableByte(),
				Q7P4 = this.rblQ7p4.SelectedValue.ToNullableByte(),
				Q8P1 = this.tbQ8p1.Text,
				Q8P2 = this.tbQ8p2.Text,
				Q8P3 = this.tbQ8p3.Text,
				Q8P4 = this.dpQ8p4.SelectedDate
			};
		}
		protected void btnSave_Click(object sender, EventArgs e)
		{
			this.Save(QASurveyUser.Statuses.InProgress);
		}

		protected void lbtnCancel_Click(object sender, EventArgs e)
		{
			RefreshPage();
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			var surveyValues = this.GetFormValues();
			var errorFound = false;
			foreach (var validateValue in surveyValues.ValidateValues())
			{
				this.BasePage.AddErrorAlert(validateValue);
				errorFound = true;
			}
			if (errorFound)
				return;
			this.Save(QASurveyUser.Statuses.Completed);
		}
		private void Save(QASurveyUser.Statuses statusEnum)
		{
			var surveyValues = this.GetFormValues().ToJsonString();
			var result = QASurveyUsers.UpdateSurvey7Values(this.QASurveyUserID, surveyValues, statusEnum);
			switch (result)
			{
				case QASurveyUsers.UpdateSurveyValuesEnum.NoRecordFound:
					PageAlertsController.AddNoRecordFoundAlert(null);
					break;
				case QASurveyUsers.UpdateSurveyValuesEnum.Success:
					PageAlertsController.AddSuccessAlert(null, "Survey has been saved.");
					break;
				case QASurveyUsers.UpdateSurveyValuesEnum.AlumniCannotEditThisRecord:
					PageAlertsController.AddErrorAlert(null, "You are not authorized to edit this record.");
					break;
				case QASurveyUsers.UpdateSurveyValuesEnum.FacultyMemberCannotEditThisRecord:
				case QASurveyUsers.UpdateSurveyValuesEnum.StudentCannotEditThisRecord:
				case QASurveyUsers.UpdateSurveyValuesEnum.StaffCannotEditThisRecord:
				case QASurveyUsers.UpdateSurveyValuesEnum.EmployerCannotEditThisRecord:
					break;
				default:
					throw new InvalidOperationException();
			}
			RefreshPage();
		}
		private void RefreshPage()
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					Generate.Redirect();
					break;
				case UserTypes.Anonymous:
				case UserTypes.System:
				case UserTypes.MasterAdmin:
				case UserTypes.Admin:
				case UserTypes.Executive:
				case UserTypes.Candidate:
				case UserTypes.Student:
				case UserTypes.Faculty:
				case UserTypes.Alumni:
				case UserTypes.IntegratedService:
				case UserTypes.ManualSQL:
				case UserTypes.Admins:
				case UserTypes.Any:
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}