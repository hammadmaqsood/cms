﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StaffV1.ascx.cs" Inherits="Aspire.Web.Sys.Common.QA.Surveys.SurveyForm7.StaffV1" %>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">1.	Knowledge</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Math, Science, Humanities and professional discipline, (if applicable) </h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ1p1" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ1p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Problem formulation and solving skills</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ1p2" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ1p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Collecting and analyzing appropriate data </h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ1p3" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ1p3" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">4.	Ability to link theory to practice</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ1p4" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ1p4" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">5.	Ability to design a system component or process</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ1p5" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ1p5" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">6.	IT knowledge</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ1p6" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ1p6" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">2.	Communications Skills</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Oral communication</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ2p1" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ2p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Report writing</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ2p2" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ2p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Presentation skills</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ2p3" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ2p3" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">3.	Interpersonal Skills</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Ability to work in teams</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ3p1" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ3p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Ability to work in arduous /Challenging situation</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ3p2" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ3p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Independent thinking</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ3p3" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ3p3" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">4.	Appreciation of ethical Values</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ3p4" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ3p4" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">4.	Management /leadership Skills</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Resource and Time management skills</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ4p1" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ4p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Judgment</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ4p2" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ4p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Discipline</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ4p3" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ4p3" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">5.	General Comments</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">Please make any additional comments or suggestions, which you think would help strengthen our programs. (New courses that you would recommend and courses that you did not gain much from)</h4>
				<p class="list-group-item-text">
					<aspire:aspiretextbox runat="server" id="tbQ5" validationgroup="SaveAndSubmit" maxlength="4096" textmode="MultiLine" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="tbQ5" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">6.	Career Opportunities</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">Career Opportunities / Counseling provided by the department / university</h4>
				<p class="list-group-item-text">
					<aspire:aspiretextbox runat="server" id="tbQ6" validationgroup="SaveAndSubmit" maxlength="4096" textmode="MultiLine" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="tbQ6" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">7.	Department Status</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading">1.	Infrastructure</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ7p1" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ7p1" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">2.	Faculty</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ7p2" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ7p2" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">3.	Repute at National level</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ7p3" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ7p3" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">4.	Repute at international level</h4>
				<p class="list-group-item-text">
					<aspire:aspireradiobuttonlist runat="server" id="rblQ7p4" repeatdirection="Horizontal" repeatlayout="Flow" />
				</p>
				<aspire:aspirerequiredfieldvalidator runat="server" errormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="rblQ7p4" />
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">8.	Alumni Information</h3>
	</div>
	<div class="panel-body">
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:aspirelabel runat="server" font-size="Large" font-bold="false" text="1. Name" associatedcontrolid="tbQ8P1" />
			</div>
			<div class="col-md-3">
				<aspire:aspiretextbox runat="server" id="tbQ8p1" validationgroup="SaveAndSubmit" />
				<aspire:aspirerequiredfieldvalidator runat="server" requirederrormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="tbQ8P1" allownull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:aspirelabel runat="server" font-size="Large" font-bold="false" text="2. Name of organization" associatedcontrolid="tbQ8p2" />
			</div>
			<div class="col-md-3">
				<aspire:aspiretextbox runat="server" id="tbQ8p2" validationgroup="SaveAndSubmit" />
				<aspire:aspirerequiredfieldvalidator runat="server" requirederrormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="tbQ8p2" allownull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:aspirelabel runat="server" font-size="Large" font-bold="false" text="3. Position in organization" associatedcontrolid="tbQ8p3" />
			</div>
			<div class="col-md-3">
				<aspire:aspiretextbox runat="server" id="tbQ8p3" validationgroup="SaveAndSubmit" />
				<aspire:aspirerequiredfieldvalidator runat="server" requirederrormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="tbQ8p3" allownull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:aspirelabel runat="server" font-size="Large" font-bold="false" text="4.	Year of graduation" associatedcontrolid="dpQ8p4" />
			</div>
			<div class="col-md-3">
				<aspire:aspiredatetimepickertextbox runat="server" id="dpQ8p4" validationgroup="SaveAndSubmit" />
				<aspire:aspirerequiredfieldvalidator runat="server" requirederrormessage="This field is required." validationgroup="SaveAndSubmit" controltovalidate="dpQ8p4" allownull="false" />
			</div>
		</div>
	</div>
</div>
<div class="form-group text-center">
	<aspire:aspirebutton runat="server" buttontype="Primary" text="Save" id="btnSave" onclick="btnSave_Click" />
	<aspire:aspirebutton runat="server" buttontype="Primary" text="Save & Submit" validationgroup="SaveAndSubmit" confirmmessage="Are you sure you want to submit?" id="btnSubmit" onclick="btnSubmit_Click" />
	<aspire:aspirelinkbutton runat="server" text="Cancel" causesvalidation="False" id="lbtnCancel" onclick="lbtnCancel_Click" />
</div>
