﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="V1.ascx.cs" Inherits="Aspire.Web.Sys.Common.QA.Surveys.SurveyForm5.V1" %>
<div class="list-group">
	<div class="list-group-item">
		<h4 class="list-group-item-heading">1. Level of research, teaching & community services being conducted.</h4>
		<p class="list-group-item-text">
			<aspire:AspireRadioButtonList runat="server" ID="rblQuestion1" RepeatDirection="Horizontal" RepeatLayout="Flow" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQuestion1" />
	</div>
	<div class="list-group-item">
		<h4 class="list-group-item-heading">2. The intellectual stimulation of your work.</h4>
		<p class="list-group-item-text">
			<aspire:AspireRadioButtonList runat="server" ID="rblQuestion2" RepeatDirection="Horizontal" RepeatLayout="Flow" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQuestion2" />
	</div>
	<div class="list-group-item">
		<h4 class="list-group-item-heading">3. Type of teaching / research you currently do.</h4>
		<p class="list-group-item-text">
			<aspire:AspireRadioButtonList runat="server" ID="rblQuestion3" RepeatDirection="Horizontal" RepeatLayout="Flow" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQuestion3" />
	</div>
	<div class="list-group-item">
		<h4 class="list-group-item-heading">4. Your interaction with students.</h4>
		<p class="list-group-item-text">
			<aspire:AspireRadioButtonList runat="server" ID="rblQuestion4" RepeatDirection="Horizontal" RepeatLayout="Flow" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQuestion4" />
	</div>
	<div class="list-group-item">
		<h4 class="list-group-item-heading">5. Cooperation you receive from colleagues.</h4>
		<p class="list-group-item-text">
			<aspire:AspireRadioButtonList runat="server" ID="rblQuestion5" RepeatDirection="Horizontal" RepeatLayout="Flow" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQuestion5" />
	</div>
	<div class="list-group-item">
		<h4 class="list-group-item-heading">6. The mentoring available to you.</h4>
		<p class="list-group-item-text">
			<aspire:AspireRadioButtonList runat="server" ID="rblQuestion6" RepeatDirection="Horizontal" RepeatLayout="Flow" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQuestion6" />
	</div>
	<div class="list-group-item">
		<h4 class="list-group-item-heading">7. Administrative support from the department.</h4>
		<p class="list-group-item-text">
			<aspire:AspireRadioButtonList runat="server" ID="rblQuestion7" RepeatDirection="Horizontal" RepeatLayout="Flow" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQuestion7" />
	</div>
	<div class="list-group-item">
		<h4 class="list-group-item-heading">8. Providing clarity about the faculty promotion process.</h4>
		<p class="list-group-item-text">
			<aspire:AspireRadioButtonList runat="server" ID="rblQuestion8" RepeatDirection="Horizontal" RepeatLayout="Flow" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQuestion8" />
	</div>

	<div class="list-group-item">
		<h4 class="list-group-item-heading">9. Your prospects for advancement and progress through ranks.</h4>
		<p class="list-group-item-text">
			<aspire:AspireRadioButtonList runat="server" ID="rblQuestion9" RepeatDirection="Horizontal" RepeatLayout="Flow" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQuestion9" />
	</div>
	<div class="list-group-item">
		<h4 class="list-group-item-heading">10. Salary and compensation package.</h4>
		<p class="list-group-item-text">
			<aspire:AspireRadioButtonList runat="server" ID="rblQuestion10" RepeatDirection="Horizontal" RepeatLayout="Flow" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQuestion10" />
	</div>
	<div class="list-group-item">
		<h4 class="list-group-item-heading">11. Job security and stability at the department.</h4>
		<p class="list-group-item-text">
			<aspire:AspireRadioButtonList runat="server" ID="rblQuestion11" RepeatDirection="Horizontal" RepeatLayout="Flow" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQuestion11" />
	</div>
	<div class="list-group-item">
		<h4 class="list-group-item-heading">12. Amount of time you have for yourself and family.</h4>
		<p class="list-group-item-text">
			<aspire:AspireRadioButtonList runat="server" ID="rblQuestion12" RepeatDirection="Horizontal" RepeatLayout="Flow" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQuestion12" />
	</div>
	<div class="list-group-item">
		<h4 class="list-group-item-heading">13. The overall environment at the department.</h4>
		<p class="list-group-item-text">
			<aspire:AspireRadioButtonList runat="server" ID="rblQuestion13" RepeatDirection="Horizontal" RepeatLayout="Flow" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQuestion13" />
	</div>
	<div class="list-group-item">
		<h4 class="list-group-item-heading">14. Whether the department is utilizing your experience and knowledge.</h4>
		<p class="list-group-item-text">
			<aspire:AspireRadioButtonList runat="server" ID="rblQuestion14" RepeatDirection="Horizontal" RepeatLayout="Flow" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQuestion14" />
	</div>
	<div class="list-group-item">
		<h4 class="list-group-item-heading">15. What are the best programs / factors currently available in your department that enhance your motivation and job satisfaction:</h4>
		<p class="list-group-item-text">
			<aspire:AspireTextBox runat="server" ID="tbQuestion15" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQuestion15" />
	</div>
	<div class="list-group-item">
		<h4 class="list-group-item-heading">16. Suggest programs / factors that could improve your motivation and job satisfaction?</h4>
		<p class="list-group-item-text">
			<aspire:AspireTextBox runat="server" ID="tbQuestion16" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQuestion16" />
	</div>
	<div class="list-group-item">
		<h4 class="list-group-item-heading">17. Academic Rank.</h4>
		<p class="list-group-item-text">
			<aspire:AspireRadioButtonList runat="server" ID="rblAcademicRank" RepeatDirection="Horizontal" RepeatLayout="Flow" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblAcademicRank" />
	</div>
	<div class="list-group-item">
		<h4 class="list-group-item-heading">18. Years of Service.</h4>
		<p class="list-group-item-text">
			<aspire:AspireRadioButtonList runat="server" ID="rblYearsOfService" RepeatDirection="Horizontal" RepeatLayout="Flow" />
		</p>
		<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblYearsOfService" />
	</div>
</div>
<div class="form-group text-center">
	<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save" ID="btnSave" OnClick="btnSave_OnClick" />
	<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save & Submit" ValidationGroup="SaveAndSubmit" ConfirmMessage="Are you sure you want to submit?" ID="btnSubmit" OnClick="btnSubmit_OnClick" />
	<aspire:AspireLinkButton runat="server" Text="Cancel" CausesValidation="False" ID="lbtnCancel" OnClick="lbtnCancel_OnClick" />
</div>
