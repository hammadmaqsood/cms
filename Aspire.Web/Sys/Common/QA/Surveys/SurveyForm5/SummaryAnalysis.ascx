﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SummaryAnalysis.ascx.cs" Inherits="Aspire.Web.Sys.Common.QA.Surveys.SurveyForm5.SummaryAnalysis" %>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Summary & Analysis</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="panel-body">
				<div class="form-group row">
					<h4 class="list-group-item-heading">A.	Summary</h4>
					<p class="list-group-item-text">
						<aspire:AspireTextBox runat="server" ID="tbQ1" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
					</p>
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ1" />
				</div>
				<div class="form-group row">
					<h4 class="list-group-item-heading">B.	Analysis</h4>
					<p class="list-group-item-text">
						<aspire:AspireTextBox runat="server" ID="tbQ2" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
					</p>
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ2" />
				</div>
				<div class="form-group row">
					<h4 class="list-group-item-heading">C.	Outcomes</h4>
					<p class="list-group-item-text">
						<aspire:AspireTextBox runat="server" ID="tbQ3" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
					</p>
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3" />
				</div>
				<div class="form-group row">
					<h4 class="list-group-item-heading">i.	Programmes / Factors currently available for motivation / satisfaction level of FMs.</h4>
					<p class="list-group-item-text">
						<aspire:AspireTextBox runat="server" ID="tbQ3P1" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
					</p>
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3P1" />
				</div>
				<div class="form-group row">
					<h4 class="list-group-item-heading">ii.	Common de-motivation factors at department / university.</h4>
					<p class="list-group-item-text">
						<aspire:AspireTextBox runat="server" ID="tbQ3P2" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
					</p>
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3P2" />
				</div>
				<div class="form-group row">
					<h4 class="list-group-item-heading">iii.	Workable suggestion /proposal for adequate enhancement of motivation / satisfaction of the faculty.</h4>
					<p class="list-group-item-text">
						<aspire:AspireTextBox runat="server" ID="tbQ3P3" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
					</p>
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3P3" />
				</div>
				<div class="form-group row">
					<h4 class="list-group-item-heading">iv.	Corrective actions discussed and resolved at HOD level.</h4>
					<p class="list-group-item-text">
						<aspire:AspireTextBox runat="server" ID="tbQ3P4" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
					</p>
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3P4" />
				</div>
				<div class="form-group row">
					<h4 class="list-group-item-heading">v.	Corrective Actions need approval of the statutory bodies.</h4>
					<p class="list-group-item-text">
						<aspire:AspireTextBox runat="server" ID="tbQ3P5" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
					</p>
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3P5" />
				</div>
				<div class="form-group row">
					<h4 class="list-group-item-heading">vi.	Weaknesses.</h4>
					<p class="list-group-item-text">
						<aspire:AspireTextBox runat="server" ID="tbQ3P6" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
					</p>
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3P6" />
				</div>
			</div>
		</div>
	</div>
</div>
<div class="form-group text-center">
	<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save" ID="btnSave" OnClick="btnSave_Click" />
	<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save & Submit" ValidationGroup="SaveAndSubmit" ConfirmMessage="Are you sure you want to submit?" ID="btnSubmit" OnClick="btnSubmit_Click" />
	<aspire:AspireLinkButton runat="server" Text="Cancel" CausesValidation="False" ID="lbtnCancel" OnClick="lbtnCancel_Click" />
</div>
