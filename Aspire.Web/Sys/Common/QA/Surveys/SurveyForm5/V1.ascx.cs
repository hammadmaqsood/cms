﻿using Aspire.BL.Core.QualityAssurance.Common;
using Aspire.BL.Core.QualityAssurance.Common.Schemas;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm5;
using System;
using System.Web.UI;
using Aspire.Web.Sys.Faculty.QA.Surveys;

namespace Aspire.Web.Sys.Common.QA.Surveys.SurveyForm5
{
	public partial class V1 : UserControl
	{
		private IAlert BasePage => this.Page as IAlert;

		public void LoadData(int qaSurveyUserID)
		{
			if (qaSurveyUserID == null || qaSurveyUserID == 0)
				this.RefreshPage();
			this.QASurveyUserID = qaSurveyUserID;
			this.BindRadioButtons();
			this.GetSurveyUserResult = QASurveyUsers.GetSurvey5User(qaSurveyUserID, AspireIdentity.Current.LoginSessionGuid);
		}

		private void BindRadioButtons()
		{
			var datasource = QASurveyUsers.GetSurveyOptionList();
			this.rblQuestion1.DataBind(datasource);
			this.rblQuestion2.DataBind(datasource);
			this.rblQuestion3.DataBind(datasource);
			this.rblQuestion4.DataBind(datasource);
			this.rblQuestion5.DataBind(datasource);
			this.rblQuestion6.DataBind(datasource);
			this.rblQuestion7.DataBind(datasource);
			this.rblQuestion8.DataBind(datasource);
			this.rblQuestion9.DataBind(datasource);
			this.rblQuestion10.DataBind(datasource);
			this.rblQuestion11.DataBind(datasource);
			this.rblQuestion12.DataBind(datasource);
			this.rblQuestion13.DataBind(datasource);
			this.rblQuestion14.DataBind(datasource);
			this.rblAcademicRank.DataBind(QASurveyUsers.GetAcademicRankList());
			this.rblYearsOfService.DataBind(QASurveyUsers.GetYearOfServiceList());
		}

		private int QASurveyUserID
		{
			get => (int)this.ViewState[nameof(this.QASurveyUserID)];
			set => this.ViewState[nameof(this.QASurveyUserID)] = value;
		}

		public QASurveyUsers.GetSurveyUserResult GetSurveyUserResult
		{
			set
			{
				this.QASurveyUserID = value.QASurveyUser.QASurveyUserID;
				bool isReadonly;
				switch (AspireIdentity.Current.UserType)
				{
					case UserTypes.Staff:
						isReadonly = true;
						break;
					case UserTypes.Faculty:
						isReadonly = value.FacultyMemberCanEdit == false;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				var qaSurveyUser = value.QASurveyUser;
				this.BindRadioButtons();
				if (qaSurveyUser.SurveyValues != null && (qaSurveyUser.StatusEnum == QASurveyUser.Statuses.InProgress || qaSurveyUser.StatusEnum == QASurveyUser.Statuses.Completed))
				{
					var surveyValues = qaSurveyUser.SurveyValues.FromJsonString<SurveyForm5Version1>();
					this.rblQuestion1.SelectedValue = surveyValues.Q1?.ToString();
					this.rblQuestion2.SelectedValue = surveyValues.Q2?.ToString();
					this.rblQuestion3.SelectedValue = surveyValues.Q3?.ToString();
					this.rblQuestion4.SelectedValue = surveyValues.Q4?.ToString();
					this.rblQuestion5.SelectedValue = surveyValues.Q5?.ToString();
					this.rblQuestion6.SelectedValue = surveyValues.Q6?.ToString();
					this.rblQuestion7.SelectedValue = surveyValues.Q7?.ToString();
					this.rblQuestion8.SelectedValue = surveyValues.Q8?.ToString();
					this.rblQuestion9.SelectedValue = surveyValues.Q9?.ToString();
					this.rblQuestion10.SelectedValue = surveyValues.Q10?.ToString();
					this.rblQuestion11.SelectedValue = surveyValues.Q11?.ToString();
					this.rblQuestion12.SelectedValue = surveyValues.Q12?.ToString();
					this.rblQuestion13.SelectedValue = surveyValues.Q13?.ToString();
					this.rblQuestion14.SelectedValue = surveyValues.Q14?.ToString();
					this.tbQuestion15.Text = surveyValues.Q15;
					this.tbQuestion16.Text = surveyValues.Q16;
					this.rblAcademicRank.SelectedValue = surveyValues.Q17?.ToString();
					this.rblYearsOfService.SelectedValue = surveyValues.Q18?.ToString();
				}
				if (isReadonly)
				{
					this.btnSave.Enabled = false;
					this.btnSubmit.Enabled = false;
				}
				else
				{
					this.btnSave.Enabled = true;
					this.btnSubmit.Enabled = true;
				}
			}
		}

		private SurveyForm5Version1 GetFormValues()
		{
			return new SurveyForm5Version1
			{
				Q1 = this.rblQuestion1.SelectedValue.ToNullableByte(),
				Q2 = this.rblQuestion2.SelectedValue.ToNullableByte(),
				Q3 = this.rblQuestion3.SelectedValue.ToNullableByte(),
				Q4 = this.rblQuestion4.SelectedValue.ToNullableByte(),
				Q5 = this.rblQuestion5.SelectedValue.ToNullableByte(),
				Q6 = this.rblQuestion6.SelectedValue.ToNullableByte(),
				Q7 = this.rblQuestion7.SelectedValue.ToNullableByte(),
				Q8 = this.rblQuestion8.SelectedValue.ToNullableByte(),
				Q9 = this.rblQuestion9.SelectedValue.ToNullableByte(),
				Q10 = this.rblQuestion10.SelectedValue.ToNullableByte(),
				Q11 = this.rblQuestion11.SelectedValue.ToNullableByte(),
				Q12 = this.rblQuestion12.SelectedValue.ToNullableByte(),
				Q13 = this.rblQuestion13.SelectedValue.ToNullableByte(),
				Q14 = this.rblQuestion14.SelectedValue.ToNullableByte(),
				Q15 = this.tbQuestion15.Text,
				Q16 = this.tbQuestion16.Text,
				Q17 = this.rblAcademicRank.SelectedValue.ToNullableByte(),
				Q18 = this.rblYearsOfService.SelectedValue.ToNullableByte()
			};
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			this.Save(QASurveyUser.Statuses.InProgress);
		}

		protected void btnSubmit_OnClick(object sender, EventArgs e)
		{
			var surveyValues = this.GetFormValues();
			var errorFound = false;
			foreach (var validateValue in surveyValues.ValidateValues())
			{
				this.BasePage.AddErrorAlert(validateValue);
				errorFound = true;
			}
			if (errorFound)
				return;
			this.Save(QASurveyUser.Statuses.Completed);
		}

		private void Save(QASurveyUser.Statuses statusEnum)
		{
			var surveyValues = this.GetFormValues().ToJsonString();
			var result = QASurveyUsers.UpdateSurvey5Values(this.QASurveyUserID, surveyValues, statusEnum, AspireIdentity.Current.LoginSessionGuid);
			switch (result)
			{
				case QASurveyUsers.UpdateSurveyValuesEnum.NoRecordFound:
					PageAlertsController.AddNoRecordFoundAlert(null);
					break;
				case QASurveyUsers.UpdateSurveyValuesEnum.Success:
					PageAlertsController.AddSuccessAlert(null, "Survey has been saved.");
					break;
				case QASurveyUsers.UpdateSurveyValuesEnum.FacultyMemberCannotEditThisRecord:
					PageAlertsController.AddErrorAlert(null, "You are not authorized to edit this record.");
					break;
				default:
					throw new InvalidOperationException();
			}
			this.RefreshPage();
		}

		private void RefreshPage()
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					Generate.Redirect();
					break;
				case UserTypes.Faculty:
					SurveysList.Redirect();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void lbtnCancel_OnClick(object sender, EventArgs e)
		{
			RefreshPage();
		}
	}
}