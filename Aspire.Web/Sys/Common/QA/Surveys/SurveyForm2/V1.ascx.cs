﻿using Aspire.BL.Core.QualityAssurance.Common;
using Aspire.BL.Core.QualityAssurance.Common.Schemas;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm2;
using System;
using System.Web.UI;
using Aspire.Web.Sys.Faculty.QA.Surveys;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Common.QA.Surveys.SurveyForm2
{
	public partial class V1 : UserControl
	{
		private IAlert BasePage => this.Page as IAlert;

		public void LoadData(int qaSurveyUserID)
		{
			if (qaSurveyUserID == null || qaSurveyUserID == 0)
				this.RefreshPage();
			this.QASurveyUserID = qaSurveyUserID;
			this.GetSurveyUserResult = QASurveyUsers.GetSurvey2User(qaSurveyUserID, AspireIdentity.Current.LoginSessionGuid);
		}

		private int QASurveyUserID
		{
			get => (int)this.ViewState[nameof(this.QASurveyUserID)];
			set => this.ViewState[nameof(this.QASurveyUserID)] = value;
		}

		public QASurveyUsers.GetSurveyUserResult GetSurveyUserResult
		{
			set
			{
				this.QASurveyUserID = value.QASurveyUser.QASurveyUserID;
				bool isReadonly;
				switch (AspireIdentity.Current.UserType)
				{
					case UserTypes.Staff:
						isReadonly = true;
						break;
					case UserTypes.Faculty:
						isReadonly = value.FacultyMemberCanEdit == false;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				var qaSurveyUser = value.QASurveyUser;
				if (value.CourseCategory == (byte)CourseCategories.Course)
				{
					this.lblQ1p13a1.Visible = true;
					this.tbQ1p13a1.Visible = true;
					this.lblQ1p13a2.Visible = true;
					this.tbQ1p13a2.Visible = true;
					this.lblQ1p13b1.Visible = true;
					this.tbQ1p13b1.Visible = true;
					this.lblQ1p13b2.Visible = true;
					this.tbQ1p13b2.Visible = true;
					this.lblQ1p13c1.Visible = true;
					this.tbQ1p13c1.Visible = true;
					this.lblQ1p13c2.Visible = true;
					this.tbQ1p13c2.Visible = true;
					this.tbQ1p13a1.Text = value.NoOfAssignments.ToString();
					this.tbQ1p13a2.Text = value.CourseAssignments.ToString();
					this.tbQ1p13b1.Text = value.NoOfQuizzes.ToString();
					this.tbQ1p13b2.Text = value.CourseQuizzes.ToString();
					this.tbQ1p13c2.Text = value.CourseMid.ToString();
					this.tbQ1p13d2.Text = value.CourseFinal.ToString();
				}
				else
				if (value.CourseCategory == (byte)CourseCategories.Lab)
				{
					this.tbQ1p13d2.Text = value.LabFinal.ToString();
				}
				if (qaSurveyUser.SurveyValues == null)
				{
					this.lblQ1p1.Text = value.DepartmentAlias;
					this.lblQ1p2.Text = value.FacultyName.ToNAIfNullOrEmpty();
					this.lblQ1p3.Text = value.CourseCode;
					this.lblQ1p4.Text = value.CourseTitle;
					this.lblQ1p5.Text = value.ShiftEnum.ToString();
					this.lblQ1p6.Text = value.SemesterIDEnum.ToString();
					this.lblQ1p7.Text = value.CreditHours.ToString();
					this.lblQ1p8.Text = value.DegreeLevelEnum.ToString();
					this.gvCoursePrerequisites.DataBind(value.Prerequisites);
					this.gvGrades.DataBind(new[] { value.Grades });
					this.lblQ1p10.Text = value.FacultyMemberName;
					this.lblQ1p11.Text = value.LectureContactHours > 0 ? value.LectureContactHours.ToString() : "0";
				}
				if (qaSurveyUser.SurveyValues != null && (qaSurveyUser.StatusEnum == QASurveyUser.Statuses.InProgress || qaSurveyUser.StatusEnum == QASurveyUser.Statuses.Completed))
				{
					var surveyValues = qaSurveyUser.SurveyValues.FromJsonString<SurveyForm2Version1>();
					this.lblQ1p1.Text = surveyValues.Q1p1;
					this.lblQ1p2.Text = surveyValues.Q1p2;
					this.lblQ1p3.Text = surveyValues.Q1p3;
					this.lblQ1p4.Text = surveyValues.Q1p4;
					this.lblQ1p5.Text = surveyValues.Q1p5;
					this.lblQ1p6.Text = surveyValues.Q1p6;
					this.lblQ1p7.Text = surveyValues.Q1p7;
					this.lblQ1p8.Text = surveyValues.Q1p8;
					this.gvCoursePrerequisites.DataBind(surveyValues.Prerequisites);
					this.lblQ1p10.Text = surveyValues.Q1p10;
					this.lblQ1p11.Text = surveyValues.Q1p11;
					this.tbQ1p12.Text = surveyValues.Q1p12;
					this.tbQ1p13a1.Text = surveyValues.Q1p13a1;
					this.tbQ1p13a2.Text = surveyValues.Q1p13a2;
					this.tbQ1p13b1.Text = surveyValues.Q1p13b1;
					this.tbQ1p13b2.Text = surveyValues.Q1p13b2;
					this.tbQ1p13c1.Text = surveyValues.Q1p13c1;
					this.tbQ1p13c2.Text = surveyValues.Q1p13c2;
					this.tbQ1p13d1.Text = surveyValues.Q1p13d1;
					this.tbQ1p13d2.Text = surveyValues.Q1p13d2;
					this.gvGrades.DataBind(new[] { surveyValues.finalGrades });
					this.tbQuestion3.Text = surveyValues.Q3;
					this.tbQuestion4.Text = surveyValues.Q4;
					this.tbQuestion5.Text = surveyValues.Q5;
					this.tbQuestion6.Text = surveyValues.Q6;
					this.tbQuestion7.Text = surveyValues.Q7;
					this.tbQuestion8.Text = surveyValues.Q8;
					this.tbQuestion9.Text = surveyValues.Q9;
				}
				if (isReadonly)
				{
					this.btnSave.Enabled = false;
					this.btnSubmit.Enabled = false;
				}
				else
				{
					this.btnSave.Enabled = true;
					this.btnSubmit.Enabled = true;
				}
			}
		}

		public List<Prerequisites> Subjects()
		{
			List<Prerequisites> prerequisites = new List<Prerequisites>();
			Prerequisites temp = new Prerequisites();
			for (int i = 0; i < gvCoursePrerequisites.Rows.Count; i++)
			{
				temp.prerequisites = gvCoursePrerequisites.Rows[i].Cells[0].Text;
				prerequisites.Add(temp);
			}
			return prerequisites;
		}
		public FinalGrades Grades()
		{
			FinalGrades grades = new FinalGrades();
			for (int i = 0; i < gvGrades.Rows.Count; i++)
			{
				grades.GradeACount = gvGrades.Rows[i].Cells[0].Text.ToInt();
				grades.GradeAMinusCount = gvGrades.Rows[i].Cells[1].Text.ToInt();
				grades.GradeBPlusCount = gvGrades.Rows[i].Cells[2].Text.ToInt();
				grades.GradeBCount = gvGrades.Rows[i].Cells[3].Text.ToInt();
				grades.GradeBMinusCount = gvGrades.Rows[i].Cells[4].Text.ToInt();
				grades.GradeCPlusCount = gvGrades.Rows[i].Cells[5].Text.ToInt();
				grades.GradeCCount = gvGrades.Rows[i].Cells[6].Text.ToInt();
				grades.GradeCMinusCount = gvGrades.Rows[i].Cells[7].Text.ToInt();
				grades.GradeDPlusCount = gvGrades.Rows[i].Cells[8].Text.ToInt();
				grades.GradeDCount = gvGrades.Rows[i].Cells[9].Text.ToInt();
				grades.GradeFCount = gvGrades.Rows[i].Cells[10].Text.ToInt();
				grades.NoGradeCount = gvGrades.Rows[i].Cells[11].Text.ToInt();
				grades.GradeWCount = gvGrades.Rows[i].Cells[12].Text.ToInt();
			}
			return grades;
		}
		private SurveyForm2Version1 GetFormValues()
		{
			return new SurveyForm2Version1
			{
				Q1p1 = this.lblQ1p1.Text,
				Q1p2 = this.lblQ1p2.Text,
				Q1p3 = this.lblQ1p3.Text,
				Q1p4 = this.lblQ1p4.Text,
				Q1p5 = this.lblQ1p5.Text,
				Q1p6 = this.lblQ1p6.Text,
				Q1p7 = this.lblQ1p7.Text,
				Q1p8 = this.lblQ1p8.Text,
				Prerequisites = Subjects(),
				Q1p10 = this.lblQ1p10.Text,
				Q1p11 = this.lblQ1p11.Text,
				Q1p12 = this.tbQ1p12.Text,
				Q1p13a1 = this.tbQ1p13a1.Text,
				Q1p13a2 = this.tbQ1p13a2.Text,
				Q1p13b1 = this.tbQ1p13b1.Text,
				Q1p13b2 = this.tbQ1p13b2.Text,
				Q1p13c1 = this.tbQ1p13c1.Text,
				Q1p13c2 = this.tbQ1p13c2.Text,
				Q1p13d1 = this.tbQ1p13d1.Text,
				Q1p13d2 = this.tbQ1p13d2.Text,
				finalGrades = Grades(),
				Q3 = this.tbQuestion3.Text,
				Q4 = this.tbQuestion4.Text,
				Q5 = this.tbQuestion5.Text,
				Q6 = this.tbQuestion6.Text,
				Q7 = this.tbQuestion7.Text,
				Q8 = this.tbQuestion8.Text,
				Q9 = this.tbQuestion9.Text,
			};
		}

		protected void btnSave_Click(object sender, EventArgs e)
		{
			this.Save(QASurveyUser.Statuses.InProgress);
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			var surveyValues = this.GetFormValues();
			var errorFound = false;
			foreach (var validateValue in surveyValues.ValidateValues())
			{
				this.BasePage.AddErrorAlert(validateValue);
				errorFound = true;
			}
			if (errorFound)
				return;
			this.Save(QASurveyUser.Statuses.Completed);
		}

		private void Save(QASurveyUser.Statuses statusEnum)
		{
			var surveyValues = this.GetFormValues().ToJsonString();
			var result = QASurveyUsers.UpdateSurvey2Values(this.QASurveyUserID, surveyValues, statusEnum, AspireIdentity.Current.LoginSessionGuid);
			switch (result)
			{
				case QASurveyUsers.UpdateSurveyValuesEnum.NoRecordFound:
					PageAlertsController.AddNoRecordFoundAlert(null);
					break;
				case QASurveyUsers.UpdateSurveyValuesEnum.Success:
					PageAlertsController.AddSuccessAlert(null, "Survey has been saved.");
					break;
				case QASurveyUsers.UpdateSurveyValuesEnum.FacultyMemberCannotEditThisRecord:
					PageAlertsController.AddErrorAlert(null, "You are not authorized to edit this record.");
					break;
				case QASurveyUsers.UpdateSurveyValuesEnum.StudentCannotEditThisRecord:
				case QASurveyUsers.UpdateSurveyValuesEnum.StaffCannotEditThisRecord:
					break;
				default:
					throw new InvalidOperationException();
			}
			this.RefreshPage();
		}

		protected void lbtnCancel_Click(object sender, EventArgs e)
		{
			RefreshPage();
		}
		private void RefreshPage()
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					Generate.Redirect();
					break;
				case UserTypes.Faculty:
					SurveysList.Redirect();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}