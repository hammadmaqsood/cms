﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="V1.ascx.cs" Inherits="Aspire.Web.Sys.Common.QA.Surveys.SurveyForm2.V1" %>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">1. Basic information about the course</h3>
	</div>
	<div class="panel-body">
		<div class="form-group row">
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="lblQ1p1" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p1" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Text="Faculty:" AssociatedControlID="lblQ1p2" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p2" RepeatLayout="Flow" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Text="Course Code:" AssociatedControlID="lblQ1p3" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p3" RepeatLayout="Flow" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Text="Title:" AssociatedControlID="lblQ1p4" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p4" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Text="Session:" AssociatedControlID="lblQ1p5" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p5" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="lblQ1p6" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p6" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Text="Credit hours:" AssociatedControlID="lblQ1p7" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p7" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Text="Level:" AssociatedControlID="lblQ1p8" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p8" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Text="Prerequisites:" AssociatedControlID="gvCoursePrerequisites" />
			</div>
			<div class="col-md-9">
				<aspire:AspireGridView AutoGenerateColumns="false" runat="server" ID="gvCoursePrerequisites" ShowHeader="False">
					<Columns>
						<asp:BoundField DataField="Prerequisites" />
					</Columns>
				</aspire:AspireGridView>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Text="Name of Course Instructor:" AssociatedControlID="lblQ1p10" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p10" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Text="No. of Students Contact Hours (Lectures)" AssociatedControlID="lblQ1p11" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p11" />
			</div>
		</div>
		<div class="form-group row">
			<aspire:AspireLabel CssClass="col-md-6" runat="server" Text="No. of Students Contact Hours (Seminars):" AssociatedControlID="tbQ1p12" />
			<div class="col-md-6">
				<aspire:AspireTextBox runat="server" ID="tbQ1p12" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ1p12" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<aspire:AspireLabel CssClass="col-md-12" runat="server" Text="Assessment Methods:give precise details (no & length of assignments, exams, weightings etc):" AssociatedControlID="tbQ1p13a1" />
		</div>
		<div class="form-group row">
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p13a1" Text="Number of Assignments:" Visible="false" AssociatedControlID="tbQ1p13a1" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ1p13a1" Visible="false" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ1p13a1" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." AllowNull="false" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p13a2" Text="Assignments Weightage:" Visible="false" AssociatedControlID="tbQ1p13a2" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ1p13a2" Visible="false" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ1p13a2" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p13b1" Text="Number of Quizzes:" Visible="false" AssociatedControlID="tbQ1p13b1" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ1p13b1" Visible="false" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ1p13b1" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." AllowNull="false" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p13b2" Text="Quizzes Weightage:" Visible="false" AssociatedControlID="tbQ1p13b2" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ1p13b2" Visible="false" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ1p13b2" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p13c1" Text="Number of Mid-term Exams:" Visible="false" AssociatedControlID="tbQ1p13c1" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ1p13c1" Text="1" Visible="false" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ1p13c1" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." AllowNull="false" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p13c2" Text="Mid-term Exams Weightage:" Visible="false" AssociatedControlID="tbQ1p13c2" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ1p13c2" Visible="false" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ1p13c2" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p13d1" Text="Number of Final-term Exams:" AssociatedControlID="tbQ1p13d1" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ1p13d1" Text="1" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ1p13d1" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." AllowNull="false" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1p13d2" Text="Final-term Exams Weightage:" AssociatedControlID="tbQ1p13d2" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ1p13d2" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ1p13d2" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." AllowNull="false" />
			</div>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">2. Distribution of Grade/Marks and other Outcomes: (adopt the grading system as required)</h3>
	</div>
	<div class="panel-body">
		<div class="form-group row">
			<div class="col-md-12">
				<aspire:AspireGridView runat="server" ID="gvGrades" AutoGenerateColumns="False">
					<Columns>
						<asp:BoundField HeaderText="A" DataField="GradeACount" />
						<asp:BoundField HeaderText="A-" DataField="GradeAMinusCount" />
						<asp:BoundField HeaderText="B+" DataField="GradeBPlusCount" />
						<asp:BoundField HeaderText="B" DataField="GradeBCount" />
						<asp:BoundField HeaderText="B-" DataField="GradeBMinusCount" />
						<asp:BoundField HeaderText="C+" DataField="GradeCPlusCount" />
						<asp:BoundField HeaderText="C" DataField="GradeCCount" />
						<asp:BoundField HeaderText="C-" DataField="GradeCMinusCount" />
						<asp:BoundField HeaderText="D+" DataField="GradeDPlusCount" />
						<asp:BoundField HeaderText="D" DataField="GradeDCount" />
						<asp:BoundField HeaderText="F" DataField="GradeFCount" />
						<asp:BoundField HeaderText="No Grade" DataField="NoGradeCount" />
						<asp:BoundField HeaderText="Withdrawl" DataField="GradeWCount" />
						<asp:BoundField HeaderText="Total" DataField="TotalGradeCount" />
					</Columns>
				</aspire:AspireGridView>
			</div>
		</div>
		<div class="form-group row">
			<aspire:AspireLabel CssClass="col-md-12" runat="server" Text="3. Salient course outline: " AssociatedControlID="tbQuestion3" />
		</div>
		<div class="form-group row">
			<div class="col-md-12">
				<aspire:AspireTextBox runat="server" ID="tbQuestion3" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQuestion3" />
			</div>
		</div>

		<div class="form-group row">
			<aspire:AspireLabel CssClass="col-md-12" runat="server" Text="4. Course Objectives: " AssociatedControlID="tbQuestion4" />
		</div>
		<div class="form-group row">
			<div class="col-md-12">
				<aspire:AspireTextBox runat="server" ID="tbQuestion4" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQuestion4" />
			</div>
		</div>
		<div class="form-group row">
			<aspire:AspireLabel CssClass="col-md-12" runat="server" Text="5. Expected course outcomes: " AssociatedControlID="tbQuestion5" />
		</div>
		<div class="form-group row">
			<div class="col-md-12">
				<aspire:AspireTextBox runat="server" ID="tbQuestion5" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQuestion5" />
			</div>
		</div>
		<div class="form-group row">
			<aspire:AspireLabel CssClass="col-md-12" runat="server" Text="6. Curriculum: comment on the continuing appropriateness of the Course curriculum in relation to the intended learning outcomes (course objectives) and its compliance with the HEC Approved / Revised National Curriculum Guidelines: " AssociatedControlID="tbQuestion6" />
		</div>
		<div class="form-group row">
			<div class="col-md-12">
				<aspire:AspireTextBox runat="server" ID="tbQuestion6" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQuestion6" />
			</div>
		</div>
		<div class="form-group row">
			<aspire:AspireLabel CssClass="col-md-12" runat="server" Text="7. Assessment: comment on the continuing effectiveness of method(s) of assessment in relation to the intended learning outcomes (Course objectives): " AssociatedControlID="tbQuestion7" />
		</div>
		<div class="form-group row">
			<div class="col-md-12">
				<aspire:AspireTextBox runat="server" ID="tbQuestion7" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQuestion7" />
			</div>
		</div>
		<div class="form-group row">
			<aspire:AspireLabel CssClass="col-md-12" runat="server" Text="8. Enhancement: comment on the implementation of changes proposed in earlier Faculty Course Review Reports " AssociatedControlID="tbQuestion8" />
		</div>
		<div class="form-group row">
			<div class="col-md-12">
				<aspire:AspireTextBox runat="server" ID="tbQuestion8" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQuestion8" />
			</div>
		</div>
		<div class="form-group row">
			<aspire:AspireLabel CssClass="col-md-12" runat="server" Text="9. Proposed changes in the contents of syllabi or any other suggestion for improvement in quality of teaching / learning: " AssociatedControlID="tbQuestion9" />
		</div>
		<div class="form-group row">
			<div class="col-md-12">
				<aspire:AspireTextBox runat="server" ID="tbQuestion9" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQuestion9" />
			</div>
		</div>
	</div>
</div>
<div class="form-group text-center">
	<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save" ID="btnSave" OnClick="btnSave_Click" />
	<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save & Submit" ValidationGroup="SaveAndSubmit" ConfirmMessage="Are you sure you want to submit?" ID="btnSubmit" OnClick="btnSubmit_Click" />
	<aspire:AspireLinkButton runat="server" Text="Cancel" CausesValidation="False" ID="lbtnCancel" OnClick="lbtnCancel_Click" />
</div>
