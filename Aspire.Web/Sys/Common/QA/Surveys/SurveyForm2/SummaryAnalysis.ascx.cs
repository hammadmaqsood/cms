﻿using Aspire.BL.Core.QualityAssurance.Common.Schemas;
using Aspire.BL.Core.QualityAssurance.Common.Schemas.SummaryAnalysis;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI;

namespace Aspire.Web.Sys.Common.QA.Surveys.SurveyForm2
{
	public partial class SummaryAnalysis : UserControl
	{
		private IAlert BasePage => this.Page as IAlert;
		public void LoadData(int qaSurveySummaryAnalysisID)
		{
			if (qaSurveySummaryAnalysisID == null || qaSurveySummaryAnalysisID == 0)
				this.RefreshPage();
			this.QASurveySummaryAnalysisID = qaSurveySummaryAnalysisID;
			this.GetSurveyUserResult = QASummaryUsers.GetSummary2User(qaSurveySummaryAnalysisID, AspireIdentity.Current.LoginSessionGuid);
		}

		private int QASurveySummaryAnalysisID
		{
			get => (int)this.ViewState[nameof(this.QASurveySummaryAnalysisID)];
			set => this.ViewState[nameof(this.QASurveySummaryAnalysisID)] = value;
		}
		public QASummaryUsers.GetSummaryUserResult GetSurveyUserResult
		{
			set
			{
				this.QASurveySummaryAnalysisID = value.QASurveySummaryAnalysi.QASurveySummaryAnalysisID;
				bool isReadonly;
				switch (AspireIdentity.Current.UserType)
				{
					case UserTypes.Staff:
						isReadonly = value.StaffCanEdit == false;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				var qaSurveySummaryAnalysi = value.QASurveySummaryAnalysi;
				if (qaSurveySummaryAnalysi.SummaryValues != null && (qaSurveySummaryAnalysi.Status == (byte)QASurveySummaryAnalysi.Statuses.InProgress || qaSurveySummaryAnalysi.Status == (byte)QASurveySummaryAnalysi.Statuses.Completed))
				{
					var summaryValues = qaSurveySummaryAnalysi.SummaryValues.FromJsonString<SummaryAnalysisForm2Version1>();
					this.tbQ1.Text = summaryValues.Q1;
					this.tbQ2.Text = summaryValues.Q2;
					this.tbQ3.Text = summaryValues.Q3;
					this.tbQ3P1.Text = summaryValues.Q3p1;
					this.tbQ3P2.Text = summaryValues.Q3p2;
					this.tbQ3P3.Text = summaryValues.Q3p3;
					this.tbQ3P4.Text = summaryValues.Q3p4;
				}
				if (isReadonly)
				{
					this.btnSave.Enabled = false;
					this.btnSubmit.Enabled = false;
				}
				else
				{
					this.btnSave.Enabled = true;
					this.btnSubmit.Enabled = true;
				}
			}
		}
		private void RefreshPage()
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					Staff.QA.Surveys.SurveyForm2.SummaryAnalysis.SummaryControlPanel.Redirect(null);
					break;
				case UserTypes.Faculty:
				case UserTypes.Anonymous:
				case UserTypes.System:
				case UserTypes.MasterAdmin:
				case UserTypes.Admin:
				case UserTypes.Executive:
				case UserTypes.Candidate:
				case UserTypes.Student:
				case UserTypes.Alumni:
				case UserTypes.IntegratedService:
				case UserTypes.ManualSQL:
				case UserTypes.Admins:
				case UserTypes.Any:
					throw new ArgumentOutOfRangeException();
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void btnSave_Click(object sender, EventArgs e)
		{
			this.Save(QASurveySummaryAnalysi.Statuses.InProgress);
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			var surveyValues = this.GetFormValues();
			var errorFound = false;
			foreach (var validateValue in surveyValues.ValidateValues())
			{
				this.BasePage.AddErrorAlert(validateValue);
				errorFound = true;
			}
			if (errorFound)
				return;
			this.Save(QASurveySummaryAnalysi.Statuses.Completed);
		}

		protected void lbtnCancel_Click(object sender, EventArgs e)
		{
			this.RefreshPage();
		}

		private SummaryAnalysisForm2Version1 GetFormValues()
		{
			return new SummaryAnalysisForm2Version1
			{
				Q1 = this.tbQ1.Text.ToString(),
				Q2 = this.tbQ2.Text.ToString(),
				Q3 = this.tbQ3.Text.ToString(),
				Q3p1 = this.tbQ3P1.Text.ToString(),
				Q3p2 = this.tbQ3P2.Text.ToString(),
				Q3p3 = this.tbQ3P3.Text.ToString(),
				Q3p4 = this.tbQ3P4.Text.ToString(),
			};
		}

		private void Save(QASurveySummaryAnalysi.Statuses statusEnum)
		{
			var surveyValues = this.GetFormValues().ToJsonString();
			var result = QASummaryUsers.UpdateSummary2Values(this.QASurveySummaryAnalysisID, surveyValues, statusEnum, AspireIdentity.Current.LoginSessionGuid);
			switch (result)
			{
				case QASurveySummaryAnalysi.ValidationResults.NoRecordFound:
					PageAlertsController.AddNoRecordFoundAlert(null);
					break;
				case QASurveySummaryAnalysi.ValidationResults.Success:
					PageAlertsController.AddSuccessAlert(null, "Summary has been saved.");
					break;
				case QASurveySummaryAnalysi.ValidationResults.StaffCannotEditThisRecord:
					PageAlertsController.AddErrorAlert(null, "You are not authorized to edit this record.");
					break;
				case QASurveySummaryAnalysi.ValidationResults.SummaryGenerated:
					PageAlertsController.AddErrorAlert(null, "Summary can not be updated because it is submitted.");
					break;
				case QASurveySummaryAnalysi.ValidationResults.None:
				case QASurveySummaryAnalysi.ValidationResults.AlreadyExists:
					throw new InvalidOperationException();
				default:
					throw new InvalidOperationException();
			}
			this.RefreshPage();
		}

	}
}