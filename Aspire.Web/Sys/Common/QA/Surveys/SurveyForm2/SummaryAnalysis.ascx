﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SummaryAnalysis.ascx.cs" Inherits="Aspire.Web.Sys.Common.QA.Surveys.SurveyForm2.SummaryAnalysis" %>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Summary & Analysis</h3>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class="panel-body">
				<div class="form-group row">
					<h4 class="list-group-item-heading">A.	Summary</h4>
					<p class="list-group-item-text">
						<aspire:AspireTextBox runat="server" ID="tbQ1" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
					</p>
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ1" />
				</div>
				<div class="form-group row">
					<h4 class="list-group-item-heading">B.	Analysis</h4>
					<p class="list-group-item-text">
						<aspire:AspireTextBox runat="server" ID="tbQ2" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
					</p>
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ2" />
				</div>
				<div class="form-group row">
					<h4 class="list-group-item-heading">C.	Outcomes</h4>
					<p class="list-group-item-text">
						<aspire:AspireTextBox runat="server" ID="tbQ3" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
					</p>
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3" />
				</div>
				<div class="form-group row">
					<h4 class="list-group-item-heading">i.	Strength of the course under review/ survey.</h4>
					<p class="list-group-item-text">
						<aspire:AspireTextBox runat="server" ID="tbQ3P1" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
					</p>
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3P1" />
				</div>
				<div class="form-group row">
					<h4 class="list-group-item-heading">ii.	Areas need improvement.</h4>
					<p class="list-group-item-text">
						<aspire:AspireTextBox runat="server" ID="tbQ3P2" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
					</p>
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3P2" />
				</div>
				<div class="form-group row">
					<h4 class="list-group-item-heading">iii.	Corrective actions proposed & implemented at HOD level</h4>
					<p class="list-group-item-text">
						<aspire:AspireTextBox runat="server" ID="tbQ3P3" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
					</p>
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3P3" />
				</div>
				<div class="form-group row">
					<h4 class="list-group-item-heading">iv.	Suggestion/ corrective actions discussed in DBS/ FBS and forwarded to Academic Council/ HERC for approval.</h4>
					<p class="list-group-item-text">
						<aspire:AspireTextBox runat="server" ID="tbQ3P4" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
					</p>
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3P4" />
				</div>
			</div>
		</div>
	</div>
</div>
<div class="form-group text-center">
	<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save" ID="btnSave" OnClick="btnSave_Click" />
	<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save & Submit" ValidationGroup="SaveAndSubmit" ConfirmMessage="Are you sure you want to submit?" ID="btnSubmit" OnClick="btnSubmit_Click" />
	<aspire:AspireLinkButton runat="server" Text="Cancel" CausesValidation="False" ID="lbtnCancel" OnClick="lbtnCancel_Click" />
</div>
