﻿using Aspire.BL.Core.QualityAssurance.Common;
using Aspire.BL.Core.QualityAssurance.Common.Schemas;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Sys.Staff.QA.Surveys.SurveyForm6;
using System;
using System.Web.UI;
using Aspire.Web.Sys.Faculty.QA.Surveys;

namespace Aspire.Web.Sys.Common.QA.Surveys.SurveyForm6
{
	public partial class V1 : UserControl
	{
		private IAlert BasePage => this.Page as IAlert;

		public void LoadData(int qaSurveyUserID)
		{
			if (!this.IsPostBack)
			{
				if (qaSurveyUserID == 0)
					this.RefreshPage();
				this.QASurveyUserID = qaSurveyUserID;
				this.BindRadioButtons();
				this.GetSurveyUserResult = QASurveyUsers.GetSurvey6User(qaSurveyUserID, AspireIdentity.Current.LoginSessionGuid);
			}
		}

		public QASurveyUsers.GetSurveyUserResult GetSurveyUserResult
		{
			set
			{
				this.QASurveyUserID = value.QASurveyUser.QASurveyUserID;
				bool isReadonly;
				switch (AspireIdentity.Current.UserType)
				{
					case UserTypes.Staff:
						isReadonly = true;
						break;
					case UserTypes.Faculty:
						isReadonly = value.FacultyMemberCanEdit == false;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				var qaSurveyUser = value.QASurveyUser;
				this.BindRadioButtons();
				if (qaSurveyUser.SurveyValues == null)
				{
					this.lblQ1P1.Text = value.DepartmentAlias;
					this.lblQ1P2.Text = (value.FacultyName == null) ? "N/A" : value.FacultyName.ToUpper();
					this.lblQ1P3.Text = value.ProgramAlias.ToNAIfNullOrEmpty();
					this.lblQ1P4.Text = (value.ProgramStartDate.ToString() == "") ? "N/A" : String.Format("{0:dd/MM/yyyy}", value.ProgramStartDate);
					this.lblQ5P3.Text = (value.ProgramDurationFullName == null) ? "N/A" : value.ProgramDurationFullName.ToUpper();
					this.lblQ4P3.Text = (value.StudentsToApplicantstRatio == ":") ? "N/A" : value.StudentsToApplicantstRatio;
				}
				if (qaSurveyUser.SurveyValues != null && (qaSurveyUser.StatusEnum == QASurveyUser.Statuses.InProgress || qaSurveyUser.StatusEnum == QASurveyUser.Statuses.Completed))
				{
					var surveyValues = qaSurveyUser.SurveyValues.FromJsonString<SurveyForm6Version1>();
					this.lblQ1P1.Text = surveyValues.Q1P1.ToString();
					this.lblQ1P2.Text = surveyValues.Q1P2.ToString();
					this.lblQ1P3.Text = surveyValues.Q1P3.ToString();
					this.lblQ1P4.Text = surveyValues.Q1P4.ToString();
					this.tbQ1P5.Text = surveyValues.Q1P5?.ToString();
					this.tbQ1P6.Text = surveyValues.Q1P6?.ToString();
					this.tbQ1P7.Text = surveyValues.Q1P7?.ToString();
					this.tbQ2P1.Text = surveyValues.Q2P1?.ToString();
					this.tbQ2P2.Text = surveyValues.Q2P2?.ToString();
					this.tbQ2P3.Text = surveyValues.Q2P3?.ToString();
					this.tbQ3P1.Text = surveyValues.Q3P1?.ToString();
					this.tbQ3P2.Text = surveyValues.Q3P2?.ToString();
					this.tbQ3P3.Text = surveyValues.Q3P3?.ToString();
					this.tbQ3P4.Text = surveyValues.Q3P4?.ToString();
					this.tbQ3P5.Text = surveyValues.Q3P5?.ToString();
					this.tbQ3P6.Text = surveyValues.Q3P6?.ToString();
					this.tbQ4P1.Text = surveyValues.Q4P1?.ToString();
					this.tbQ4P2.Text = surveyValues.Q4P2?.ToString();
					this.lblQ4P3.Text = surveyValues.Q4P3?.ToString();
					this.tbQ5P1.Text = surveyValues.Q5P1?.ToString();
					this.rblQ5P2.SelectedValue = surveyValues.Q5P2?.ToString();
					this.lblQ5P3.Text = surveyValues.Q5P3?.ToString();
					this.lblQ5P4.Text = surveyValues.Q5P4?.ToString();
					this.lblQ5P5.Text = surveyValues.Q5P5?.ToString();
					this.lblQ5P6.Text = surveyValues.Q5P6?.ToString();
					this.rblQ5P7a.SelectedValue = surveyValues.Q5P7a?.ToString();
					this.rblQ5P7b.SelectedValue = surveyValues.Q5P7b?.ToString();
					this.rblQ5P7c.SelectedValue = surveyValues.Q5P7c?.ToString();
					this.rblQ5P7d.SelectedValue = surveyValues.Q5P7d?.ToString();
					this.tbQ5P7d.Text = surveyValues.Q5P7e.ToString();
					this.tbQ5P8.Text = surveyValues.Q5P8?.ToString();
					this.lblQ5P9.Text = surveyValues.Q5P9?.ToString();
					this.lblQ5P10.Text = surveyValues.Q5P10?.ToString();
					this.tbQ6P1.Text = surveyValues.Q6P1?.ToString();
				}
				if (rblQ5P7d.SelectedValue == "1")
					divQ5P7d.Visible = true;
				else
					divQ5P7d.Visible = false;

				if (isReadonly)
				{
					this.btnSave.Enabled = false;
					this.btnSubmit.Enabled = false;
				}
				else
				{
					this.btnSave.Enabled = true;
					this.btnSubmit.Enabled = true;
				}
			}
		}

		private SurveyForm6Version1 GetFormValues()
		{
			return new SurveyForm6Version1
			{
				Q1P1 = this.lblQ1P1.Text,
				Q1P2 = this.lblQ1P2.Text,
				Q1P3 = this.lblQ1P3.Text,
				Q1P4 = this.lblQ1P4.Text,
				Q1P5 = this.tbQ1P5.Text,
				Q1P6 = this.tbQ1P6.Text,
				Q1P7 = this.tbQ1P7.Text,
				Q2P1 = this.tbQ2P1.Text,
				Q2P2 = this.tbQ2P2.Text,
				Q2P3 = this.tbQ2P3.Text,
				Q3P1 = this.tbQ3P1.Text,
				Q3P2 = this.tbQ3P2.Text,
				Q3P3 = this.tbQ3P3.Text,
				Q3P4 = this.tbQ3P4.Text,
				Q3P5 = this.tbQ3P5.Text,
				Q3P6 = this.tbQ3P6.Text,
				Q4P1 = this.tbQ4P1.Text,
				Q4P2 = this.tbQ4P2.Text,
				Q4P3 = this.lblQ4P3.Text,
				Q5P1 = this.tbQ5P1.Text,
				Q5P2 = this.rblQ5P2.SelectedValue.ToNullableByte(),
				Q5P3 = this.lblQ5P3.Text,
				Q5P4 = this.lblQ5P4.Text,
				Q5P5 = this.lblQ5P5.Text,
				Q5P6 = this.lblQ5P6.Text,
				Q5P7a = this.rblQ5P7a.SelectedValue.ToNullableByte(),
				Q5P7b = this.rblQ5P7b.SelectedValue.ToNullableByte(),
				Q5P7c = this.rblQ5P7c.SelectedValue.ToNullableByte(),
				Q5P7d = this.rblQ5P7d.SelectedValue.ToNullableByte(),
				Q5P7e = this.tbQ5P7d.Text,
				Q5P8 = this.tbQ5P8.Text,
				Q5P9 = this.lblQ5P9.Text,
				Q5P10 = this.lblQ5P10.Text,
				Q6P1 = this.tbQ6P1.Text
			};
		}

		private void BindRadioButtons()
		{
			rblQ5P2.FillYesNo();
			rblQ5P7a.DataBind(QASurveyUsers.GetPhdExams());
			rblQ5P7b.FillYesNo();
			rblQ5P7c.FillYesNo();
			rblQ5P7d.FillYesNo();
		}

		private int QASurveyUserID
		{
			get => (int)this.ViewState[nameof(this.QASurveyUserID)];
			set => this.ViewState[nameof(this.QASurveyUserID)] = value;
		}

		private void RefreshPage()
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					Generate.Redirect();
					break;
				case UserTypes.Faculty:
					SurveysList.Redirect();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			var surveyValues = this.GetFormValues();
			var errorFound = false;
			foreach (var validateValue in surveyValues.ValidateValues())
			{
				this.BasePage.AddErrorAlert(validateValue);
				errorFound = true;
			}
			if (errorFound)
				return;
			this.Save(QASurveyUser.Statuses.Completed);
		}

		protected void lbtnCancel_Click(object sender, EventArgs e)
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					Generate.Redirect();
					break;
				case UserTypes.Faculty:
					SurveysList.Redirect();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void btnSave_Click(object sender, EventArgs e)
		{
			this.Save(QASurveyUser.Statuses.InProgress);
		}

		private void Save(QASurveyUser.Statuses statusEnum)
		{
			var surveyValues = this.GetFormValues().ToJsonString();
			var result = QASurveyUsers.UpdateSurvey6Values(this.QASurveyUserID, surveyValues, statusEnum, AspireIdentity.Current.LoginSessionGuid);
			switch (result)
			{
				case QASurveyUsers.UpdateSurveyValuesEnum.NoRecordFound:
					PageAlertsController.AddNoRecordFoundAlert(null);
					break;
				case QASurveyUsers.UpdateSurveyValuesEnum.Success:
					PageAlertsController.AddSuccessAlert(null, "Survey has been saved.");
					break;
				case QASurveyUsers.UpdateSurveyValuesEnum.FacultyMemberCannotEditThisRecord:
					PageAlertsController.AddErrorAlert(null, "You are not authorized to edit this record.");
					break;
				default:
					throw new InvalidOperationException();
			}
			this.RefreshPage();
		}

		protected void rblQ5P7d_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (rblQ5P7d.SelectedItem.Value == "1")
			{
				divQ5P7d.Visible = true;
			}
			else
			{
				divQ5P7d.Visible = false;
			}
		}
	}
}