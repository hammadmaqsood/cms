<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="V1.ascx.cs" Inherits="Aspire.Web.Sys.Common.QA.Surveys.SurveyForm6.V1" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Model.Entities" %>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">1.	General Information</h3>
	</div>
	<div class="panel-body">
		<div class="form-group row">
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="1.	Name of Department" AssociatedControlID="lblQ1p1" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1P1"></aspire:AspireLabel>
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="2.	Name of Faculty" AssociatedControlID="lblQ1P2" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1P2"></aspire:AspireLabel>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="3.	Name of program" AssociatedControlID="lblQ1P3" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1P3"></aspire:AspireLabel>
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="4.	Date of Initiation of Ph.D program" AssociatedControlID="lblQ1P4" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" ID="lblQ1P4"></aspire:AspireLabel>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="5.	Total number of academic journals subscribed in area relevant to Ph.D. program." AssociatedControlID="tbQ1P5" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ1P5" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ1P5" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." MinValue="0" MaxValue="50" AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="6.	Number of Computers available per Ph.D. student." AssociatedControlID="tbQ1P6" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ1P6" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ1P6" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." MinValue="0" MaxValue="50" AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="7.	Total Internet Bandwidth available to all the students in the Department." AssociatedControlID="tbQ1P7" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ1P7" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ1P7" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." MinValue="0" MaxValue="50" AllowNull="false" />
			</div>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">2.	Faculty Resources</h3>
	</div>
	<div class="panel-body">
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="1.	Number of faculty members holding Ph.D. degree in the department." AssociatedControlID="tbQ2P1" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ2P1" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ2P1" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." MinValue="0" MaxValue="100" AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="2.	Number of HEC approved Ph.D. Supervisors in the department." AssociatedControlID="tbQ2P2" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ2P2" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ2P2" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." MinValue="0" MaxValue="100" AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="3.	Number of HEC Registered PhD Supervisor in the department." AssociatedControlID="tbQ2P3" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ2P3" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ2P3" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." MinValue="0" MaxValue="100" AllowNull="false" />
			</div>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">3.	Research Output</h3>
	</div>
	<div class="panel-body">
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="1.	Total number of articles published last year in International Academic Journals that are authored by faculty members and students in the department." AssociatedControlID="tbQ3P1" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ3P1" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3P1" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." MinValue="0" MaxValue="100" AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="2.	Total number of articles published last year in Asian Academic Journals that are authored by faculty members and students in the department." AssociatedControlID="tbQ3P2" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ3P2" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3P2" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." MinValue="0" MaxValue="100" AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="3.	Total number of ongoing research projects in the department funded by different organizations." AssociatedControlID="tbQ3P3" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ3P3" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3P3" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." MinValue="0" MaxValue="100" AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="4.	Number of post-graduate students in the department holding scholarships/fellowships." AssociatedControlID="tbQ3P4" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ3P4" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3P4" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." MinValue="0" MaxValue="100" AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="5.	Total Research Funds available to the Department from all sources (in millions)." AssociatedControlID="tbQ3P5" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ3P5" ValidationGroup="SaveAndSubmit" />
				<aspire:DoubleValidator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3P5" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." MinValue="0" AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-12">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="6.	Number of active international linkages involving exchange of researchers/students/faculty etc. (Add Hyperlinks)." AssociatedControlID="tbQ3P6" />
			</div>
			<div class="col-md-12">
				<aspire:AspireTextBox runat="server" ID="tbQ3P6" MaxLength="4096" TextMode="MultiLine" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireRequiredFieldValidator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ3P6" AllowNull="false" />
			</div>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">4.	Student Information</h3>
	</div>
	<div class="panel-body">
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="1.	Number of Ph.D. degrees conferred to date to students from the Department during the past three academic years." AssociatedControlID="tbQ4P1" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ4P1" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ4P1" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." MinValue="0" MaxValue="100" AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="2.	Number of Ph.D. students currently enrolled in the department." AssociatedControlID="tbQ4P2" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ4P2" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ4P2" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." MinValue="0" MaxValue="100" AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="3.	Ratio of number of students accepted to total number of applicants for Ph.D. Program." AssociatedControlID="lblQ4P3" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Font-Size="Medium" ID="lblQ4P3"></aspire:AspireLabel>
			</div>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">5.	Program Information</h3>
	</div>
	<div class="panel-body">
		<div class="form-group row">
			<div class="col-md-12">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="1.	Entrance requirements into Ph.D. Program (M.Sc. / M.Phil) Indicate subjects or M.Sc. / M.Phil." AssociatedControlID="tbQ5P1" />
			</div>
			<div class="col-md-12">
				<aspire:AspireTextBox runat="server" ID="tbQ5P1" MaxLength="4096" TextMode="MultiLine" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ5P1" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="2.	Is your Ph.D. program based on research only? (Y/N)." AssociatedControlID="rblQ5P2" />
			</div>
			<div class="col-md-3">
				<aspire:AspireRadioButtonList runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" ID="rblQ5P2"></aspire:AspireRadioButtonList>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ5P2" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="3.	Maximum number of years in which a Ph.D. degree has to be completed after initial date of enrollment in Ph.D. program." AssociatedControlID="lblQ5P3" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Font-Size="Medium" ID="lblQ5P3"></aspire:AspireLabel>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="4.	Total number of post M.Sc. (16 year equivalent) courses required for Ph.D." AssociatedControlID="lblQ5P4" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Font-Size="Medium" ID="lblQ5P4" Text="N/A"></aspire:AspireLabel>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="5.	Total number of M.Phil level courses taught on average in a Term / Semester." AssociatedControlID="lblQ5P5" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Font-Size="Medium" ID="lblQ5P5" Text="3 Courses"></aspire:AspireLabel>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="6.	Total number of Ph.D. level courses taught on average in a Term / Semester." AssociatedControlID="lblQ5P6" />
			</div>
			<div class="col-md-3">
				<aspire:AspireLabel runat="server" Font-Size="Medium" ID="lblQ5P6" Text="3 Courses"></aspire:AspireLabel>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="7.	Do your students have to take/write:" />
			</div>
		</div>
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group- item-heading">a.	Take.</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" ID="rblQ5P7a"></aspire:AspireRadioButtonList>
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ5P7a" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">b.	Comprehensive examination (Y/N).</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" ID="rblQ5P7b"></aspire:AspireRadioButtonList>
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ5P7b" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">c.	Research paper in HEC approved Journals (W,X,Y,Z).</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" ID="rblQ5P7c"></aspire:AspireRadioButtonList>
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ5P7c" />
			</div>
			<div class="list-group-item">
				<h4 class="list-group-item-heading">d.	Any other examination (Y/N).</h4>
				<p class="list-group-item-text">
					<aspire:AspireRadioButtonList runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" ID="rblQ5P7d" AutoPostBack="true" OnSelectedIndexChanged="rblQ5P7d_SelectedIndexChanged"></aspire:AspireRadioButtonList>
				</p>
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="rblQ5P7d" />
				<div id="divQ5P7d" runat="server">
					<aspire:AspireLabel runat="server" ID="lblQ5P7d" Font-Size="Medium" Text="Exam name"></aspire:AspireLabel>
					<aspire:AspireTextBox runat="server" ID="tbQ5P7d" Width="25%" />
					<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ5P7d" />
				</div>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-9">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="8.	Total number of International examiners to which the Ph.D. dissertation is sent." AssociatedControlID="tbQ5P8" />
			</div>
			<div class="col-md-3">
				<aspire:AspireTextBox runat="server" ID="tbQ5P8" ValidationGroup="SaveAndSubmit" />
				<aspire:AspireInt32Validator runat="server" RequiredErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ5P8" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Range." MinValue="0" MaxValue="100" AllowNull="false" />
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-12">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="9.	How is the selection of an examiner from technologically advanced countries carried out?" AssociatedControlID="lblQ5P9" />
			</div>
			<div class="col-md-12">
				<aspire:AspireLabel runat="server" ID="lblQ5P9" Text="As per HEC guidelines. Ph.D. Rules (2016), 32. Thesis Defence Examinars, Page # 27." ></aspire:AspireLabel>
				</div>
		</div>
		<div class="form-group row">
			<div class="col-md-12">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="10.	Is there a minimum residency requirement (on campus) for award of Ph.D. degree?" AssociatedControlID="lblQ5P10" />
			</div>
			<div class="col-md-12">
				<aspire:AspireLabel runat="server" ID="lblQ5P10" Text="Minimum residency requirement for Ph.D. is three years. Ph.D. Rules (2016), 15. Duration of Ph.D. Programs, page # 17."></aspire:AspireLabel>
			</div>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">6.	Additional Information</h3>
	</div>
	<div class="panel-body">
		<div class="form-group row">
			<div class="col-md-12">
				<aspire:AspireLabel runat="server" Font-Size="Large" Font-Bold="false" Text="1.	Any other information that you would like to provide." AssociatedControlID="tbQ6P1" />
			</div>
			<div class="col-md-12">
				<aspire:AspireTextBox runat="server" ID="tbQ6P1" ValidationGroup="SaveAndSubmit" MaxLength="4096" TextMode="MultiLine" />
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ValidationGroup="SaveAndSubmit" ControlToValidate="tbQ6P1" />
			</div>
		</div>
	</div>
</div>
<div class="form-group text-center">
	<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save" ID="btnSave" OnClick="btnSave_Click" />
	<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save & Submit" ValidationGroup="SaveAndSubmit" ConfirmMessage="Are you sure you want to submit?" ID="btnSubmit" OnClick="btnSubmit_Click" />
	<aspire:AspireLinkButton runat="server" Text="Cancel" CausesValidation="False" ID="lbtnCancel" OnClick="lbtnCancel_Click" />
</div>
