﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Feedback.aspx.cs" Inherits="Aspire.Web.Sys.Common.Feedback" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Subject:" AssociatedControlID="tbSubject" />
				<aspire:AspireTextBox runat="server" ID="tbSubject" MaxLength="255" ValidationGroup="Feedback" />
				<aspire:AspireStringValidator runat="server" ControlToValidate="tbSubject" AllowNull="False" RequiredErrorMessage="This field is required." ValidationGroup="Feedback" />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Attachment:" AssociatedControlID="tbSubject" />
				<asp:FileUpload runat="server" AllowMultiple="False" ID="fileUpload" />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Description:" AssociatedControlID="tbDescription" />
				<aspire:AspireTextBox TextMode="MultiLine" Rows="10" MaxLength="25000" runat="server" ID="tbDescription" ValidationGroup="Feedback" />
				<aspire:AspireStringValidator runat="server" ControlToValidate="tbDescription" AllowNull="False" RequiredErrorMessage="This field is required." ValidationGroup="Feedback" />
			</div>
			<div class="form-group text-center">
				<aspire:AspireButton runat="server" Text="Submit" ValidationGroup="Feedback" ID="btnSubmit" OnClick="btnSubmit_OnClick" />
				<aspire:AspireLinkButton runat="server" ID="lbtnCancel" CausesValidation="False" Text="Cancel" OnClick="lbtnCancel_OnClick" />
			</div>
		</div>
	</div>
</asp:Content>
