﻿using Aspire.Lib.Extensions;
using Aspire.Web.Common;
using System;
using System.IO;
using System.Web;
using System.Web.SessionState;

namespace Aspire.Web.Sys.Common.StudentInformation
{
	public class StudentDocument : IHttpHandler, IRequiresSessionState
	{
		public static string GetImageUrl(Guid studentDocumentID)
		{
			return $"~/Sys/Common/StudentInformation/StudentDocument.ashx?ID={studentDocumentID:N}";
		}

		private static void Send404Response(HttpContext context)
		{
			context.Response.Clear();
			context.Response.StatusCode = 404;
			context.Response.End();
		}

		public void ProcessRequest(HttpContext context)
		{
			if (context.Request.UrlReferrer != null && context.Request.UrlReferrer?.ToString().StartsWith("~".ToAbsoluteUrl()) != true)
			{
				Send404Response(context);
				return;
			}
			var currentLoginSessionGuid = AspireIdentity.Current?.LoginSessionGuid;
			if (currentLoginSessionGuid == null)
			{
				Send404Response(context);
				return;
			}

			switch (context.Request.HttpMethod)
			{
				case "GET":
					{
						var studentDocumentID = context.Request.Params["ID"].TryToGuid();
						if (studentDocumentID == null)
						{
							Send404Response(context);
							return;
						}

						var file = BL.Core.StudentInformation.Common.StudentDocuments.ReadStudentDocument(studentDocumentID.Value, currentLoginSessionGuid.Value);
						if (file == null)
						{
							Send404Response(context);
							return;
						}

						var fileName = Path.ChangeExtension(file.Value.systemFile.FileName, file.Value.systemFile.FileExtension.ToLower());
						context.Response.Clear();
						context.Response.AddHeader("Content-Disposition", $"attachment; filename=\"{fileName}\"");
						context.Response.AddHeader("Content-Length", file.Value.systemFile.FileSize.ToString());
						context.Response.ContentType = file.Value.systemFile.MimeType;
						context.Response.BinaryWrite(file.Value.fileBytes);
					}
					return;
				default:
					Send404Response(context);
					return;
			}
		}

		public bool IsReusable => false;
	}
}