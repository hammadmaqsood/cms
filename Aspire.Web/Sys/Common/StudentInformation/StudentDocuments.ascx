﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StudentDocuments.ascx.cs" Inherits="Aspire.Web.Sys.Common.StudentInformation.StudentDocuments" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Register Src="~/Sys/Staff/Common/StudentInfoUserControl.ascx" TagPrefix="uc1" TagName="StudentInfoUserControl" %>

<p></p>
<asp:Panel runat="server" ID="panelStaff">
	<uc1:StudentInfoUserControl runat="server" ID="staffStudentInfoUserControl" />
</asp:Panel>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Previous Academic Information</h3>
	</div>
	<asp:Repeater ID="repeaterAcademicRecord" runat="server" ItemType="Aspire.Model.Entities.StudentAcademicRecord">
		<HeaderTemplate>
			<table class="table table-bordered table-condensed table-hover table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Degree</th>
						<th>Institute</th>
						<th>Board/University</th>
						<th>Subjects</th>
						<th>Marks</th>
						<th>Percentage</th>
						<th>Passing Year</th>
						<th>Remarks</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<% if (this.repeaterAcademicRecord.Items.Count == 0)
						{ %>
					<tr>
						<td colspan="10">No record found.</td>
					</tr>
					<% } %>
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
				<td><%#: Container.ItemIndex + 1 %></td>
				<td><%#: (Item.DegreeTypeEnum).ToFullName() %></td>
				<td><%#: (Item.Institute).ToNAIfNullOrEmpty() %></td>
				<td><%#: (Item.BoardUniversity).ToNAIfNullOrEmpty() %></td>
				<td><%#: (Item.Subjects).ToNAIfNullOrEmpty() %></td>
				<td><%#: $"{Item.ObtainedMarks:#.##}/{Item.TotalMarks:#.##}" %></td>
				<td><%#: $"{Item.Percentage:#.##}%" %></td>
				<td><%#: (Item.PassingYear).ToNAIfNullOrEmpty() %></td>
				<td><%#: (Item.Remarks).ToNAIfNullOrEmpty() %></td>
				<td><%#: (Item.StatusEnum?.ToFullName()).ToNAIfNullOrEmpty() %></td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</tbody>
			</table>
		</FooterTemplate>
	</asp:Repeater>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Student Documents</h3>
	</div>
	<table class="table table-bordered table-condensed tableCol4">
		<tbody>
			<tr>
				<th>Documents Status</th>
				<td>
					<aspire:Label ID="lblDocumentStatus" runat="server" />
				</td>
				<th>Last Updated</th>
				<td>
					<aspire:Label ID="lblDocumentStatusDate" runat="server" />
				</td>
			</tr>
			<tr>
				<th>Remarks</th>
				<td colspan="3">
					<asp:Literal ID="ltrlDocumentStatus" runat="server" Mode="PassThrough" />
				</td>
			</tr>
		</tbody>
	</table>
	<div class="panel-body">
		<aspire:AspireButton runat="server" CausesValidation="false" ButtonType="Success" Glyphicon="plus" Text="Upload Document" ID="btnUploadDocument" OnClick="btnUploadDocument_Click" />
		<aspire:AspireButton runat="server" CausesValidation="false" ButtonType="Info" Glyphicon="info_sign" Text="Instructions" ID="btnInstructions" OnClick="btnInstructions_OnClick" />
	</div>
	<asp:Repeater ID="repeaterStudentDocuments" runat="server" ItemType="Aspire.Model.Entities.StudentDocument" OnItemCommand="repeaterStudentDocuments_OnItemCommand">
		<HeaderTemplate>
			<table class="table table-bordered table-condensed table-hover table-striped">
				<thead>
					<tr>
						<th>Document</th>
						<th>Page</th>
						<th>Dimensions</th>
						<th>Resolution</th>
						<th>Remarks</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<% if (this.repeaterStudentDocuments.Items.Count == 0)
						{ %>
					<tr>
						<td colspan="8">No record found.</td>
					</tr>
					<% } %>
		</HeaderTemplate>
		<ItemTemplate>
			<tr class='<%# Item.StatusEnum == StudentDocument.Statuses.Verified ? "success" : (Item.StatusEnum == StudentDocument.Statuses.Rejected ? "danger" : "") %>'>
				<td><%#: Item.DocumentType.GetFullName() %></td>
				<td><%#: Item.Page.GetFullName() %></td>
				<td><%#: Item.Dimensions %></td>
				<td><%#: Item.Resolution %></td>
				<td>
					<%#: Item.Remarks.ToNAIfNullOrEmpty() %>
				</td>
				<td><%#: Item.StatusEnum.GetFullName() %></td>
				<td>
					<aspire:AspireLinkButton runat="server" Text="View" CausesValidation="False" CommandName="View" EncryptedCommandArgument="<%# Item.StudentDocumentID %>" />
				</td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</tbody>
			</table>
		</FooterTemplate>
	</asp:Repeater>
	<div id="divStaffStudentStatus" runat="server" class="panel-body text-center">
		<aspire:AspireButton runat="server" ButtonType="Success" ID="btnVerify" Text="Verify" CausesValidation="false" ConfirmMessage="Are you sure you want to verify these documents?" OnClick="btnVerify_Click" />
		<aspire:AspireButton runat="server" ButtonType="Danger" ID="btnReject" Text="Reject" CausesValidation="false" OnClick="btnRejected_Click" />
	</div>
</div>
<aspire:AspireModal runat="server" ID="modalDocument" HeaderText="Upload Document">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanelModalDocument" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireAlert ID="alertUploadDocument" runat="server" />
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Document Type:" AssociatedControlID="ddlDocumentType" />
					<aspire:AspireDropDownList runat="server" ID="ddlDocumentType" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlDocumentType_SelectedIndexChanged" />
					<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Upload" ErrorMessage="This field is required." ControlToValidate="ddlDocumentType" />
				</div>
				<asp:Repeater runat="server" ID="repeaterPages" ItemType="Aspire.Web.Sys.Common.StudentInformation.StudentDocuments.DocumentPage">
					<ItemTemplate>
						<div class="form-group">
							<aspire:HiddenField runat="server" ID="hfPage" Visible="false" Value="<%# Item.PageEnum.GetGuid() %>" />
							<aspire:AspireLabel runat="server" Text='<%# Item.PageEnum.GetFullName() + ":" %>' AssociatedControlID="fileUpload" />
							<div>
								<aspire:AspireFileUpload runat="server" ID="fileUpload" AllowedFileTypes=".jpg,.jpeg" MaxFileSize="20971520" MaxNumberOfFiles="1" />
								<aspire:AspireFileUploadValidator AllowNull="<%# Item.AllowNull %>" ValidationGroup="Upload" runat="server" AspireFileUploadID="fileUpload" RequiredErrorMessage="File is missing." />
							</div>
							<span class="help-block"><%#: Item.PageEnum.GetDescription() %></span>
						</div>
					</ItemTemplate>
				</asp:Repeater>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanelSaveButton" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ValidationGroup="Upload" OnClick="btnSave_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>

<aspire:AspireModal runat="server" ID="modalView" HeaderText="View Document" ModalSize="Large" Visible="False">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanelModalView" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireAlert ID="alertViewDocument" runat="server" />
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Document Type:" AssociatedControlID="tbDocumentType" />
							<aspire:AspireTextBox ReadOnly="True" runat="server" ID="tbDocumentType" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Page:" AssociatedControlID="tbPage" />
							<aspire:AspireTextBox ReadOnly="True" runat="server" ID="tbPage" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<asp:Image runat="server" ID="imgDocument" Style="max-width: 100%" ToolTip="Click to View" />
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Dimensions:" AssociatedControlID="tbDimensions" />
							<aspire:AspireTextBox ReadOnly="True" runat="server" ID="tbDimensions" />
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group" runat="server" id="divResolution">
							<aspire:AspireLabel runat="server" Text="Resolution:" AssociatedControlID="tbResolution" />
							<aspire:AspireTextBox ReadOnly="True" runat="server" ID="tbResolution" />
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatus" />
							<div>
								<aspire:AspireDropDownList runat="server" ID="ddlStatus" ValidationGroup="Update" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlStatus_OnSelectedIndexChanged" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlStatus" ValidationGroup="Update" ErrorMessage="This field is required." />
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" />
							<aspire:AspireTextBox runat="server" ID="tbRemarks" MaxLength="1000" TextMode="MultiLine" ValidationGroup="Update" />
							<aspire:AspireRequiredFieldValidator ID="rfvtbRemarks" runat="server" ControlToValidate="tbRemarks" ValidationGroup="Update" ErrorMessage="This field is required." />
						</div>
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanelModalViewButtons" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnDelete" ButtonType="Danger" CssClass="pull-left" Text="Delete" CausesValidation="false" ConfirmMessage="Are you sure you want to delete all sheets/pages of the selected document?" OnClick="btnDelete_Click" />
				<aspire:AspireButton runat="server" ID="btnSkip" ButtonType="Default" Text="Skip" CausesValidation="false" OnClick="btnSaveStudentDocument_OnClick" />
				<aspire:AspireButton runat="server" ID="btnSaveStudentDocument" Text="Save" ValidationGroup="Update" OnClick="btnSaveStudentDocument_OnClick" />
				<aspire:AspireButton runat="server" ID="btnSaveAndNextStudentDocument" Text="Save & Next" ValidationGroup="Update" OnClick="btnSaveStudentDocument_OnClick" />
				<aspire:AspireButton runat="server" ID="btnCancel" ButtonType="Default" Text="Cancel" CausesValidation="false" OnClick="btnCancel_Click" />
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>

<aspire:AspireModal runat="server" ID="modalReject" HeaderText="Reject Documents">
	<BodyTemplate>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Remarks:" AssociatedControlID="tbRejectRemarks" />
			<aspire:AspireTextBox TextMode="MultiLine" runat="server" ID="tbRejectRemarks" ValidationGroup="Reject" />
			<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbRejectRemarks" ErrorMessage="This field is required." ValidationGroup="Reject" />
		</div>
	</BodyTemplate>
	<FooterTemplate>
		<aspire:AspireButton runat="server" ID="btnRejectSave" Text="Reject Documents" ValidationGroup="Reject" OnClick="btnRejectSave_Click" />
		<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
	</FooterTemplate>
</aspire:AspireModal>

<aspire:AspireModal runat="server" ID="modalInstructions" HeaderText="Instructions for Uploading Documents" ModalSize="Large" Visible="False">
	<BodyTemplate>
		<h4>Important</h4>
		<ol>
			<li>Scan the required documents (mentioned below) in <strong>Jpg/Jpeg</strong> format with at least <strong>150 Dpi</strong> resolution.</li>
			<li>Use scanner to scan documents. Images taken through Camera/Mobile will be rejected due to low resolution.</li>
			<li>Be sure, each file size does not exceed <strong>20MB</strong>.</li>
			<li>Admission Office will verify all the uploaded documents. Remarks will be provided in case of rejection.</li>
			<li>Document will not be altered once it is verified. In case of correction, contact admissions office of your respective campus.</li>
			<li>If you have any problem/query regarding uploading documents. Please contact admissions office of your respective campus.</li>
		</ol>
		<h4>For Undergraduate Programs</h4>
		<ol>
			<li>SSC Marks Sheet</li>
			<li>SSC Certificate</li>
			<li>General Certificate of Education from (Cambridge) (O-Level)</li>
			<li>O-Level Equivalence Certificate from
				<abbr title="Inter Board Committee of Chairmen">IBCC</abbr></li>
			<li>HSSC Marks Sheet</li>
			<li>HSSC Certificate</li>
			<li>General Certificate of Education from (Cambridge) (A-level)</li>
			<li>A-Level Equivalence Certificate from
				<abbr title="Inter Board Committee of Chairmen">IBCC</abbr></li>
			<li>CNIC / Form 'B'</li>
			<li>Passport (International Student)</li>
		</ol>
		<h4>For Postgraduate Programs</h4>
		<ol>
			<li>SSC Marks Sheet</li>
			<li>SSC Certificate</li>
			<li>General Certificate of Education from (Cambridge) (O-Level)</li>
			<li>O-Level  Equivalence Certificate from
				<abbr title="Inter Board Committee of Chairmen">IBCC</abbr></li>
			<li>HSSC Marks Sheet</li>
			<li>HSSC Certificate</li>
			<li>General Certificate of Education from (Cambridge) (A-Level)</li>
			<li>A-Level Equivalence Certificate from
				<abbr title="Inter Board Committee of Chairmen">IBCC</abbr></li>
			<li>Bachelor Marks Sheet (2 Years BA, BSc, B.Com)</li>
			<li>Bachelor's Degree (2 Years)</li>
			<li>Bachelor's Transcript (04 years)</li>
			<li>Bachelor's Degree (4 Years)</li>
			<li>Master's Marks Sheet (2 Years MA,MSc,M.Com)</li>
			<li>Master's Degree (2 Years)</li>
			<li>Foreign Transcript</li>
			<li>Foreign Degree</li>
			<li>
				<abbr title="Higher Education Commission">HEC</abbr>
				Equivalence Certificate for Foreign Degree</li>
			<li>CNIC</li>
			<li>Passport (International Student)</li>
		</ol>
		<h4>For PhD Programs</h4>
		<ol>
			<li>SSC Marks Sheet</li>
			<li>SSC Certificate</li>
			<li>General Certificate of Education from (Cambridge) (O-Level)</li>
			<li>O-Level  Equivalence Certificate from
				<abbr title="Inter Board Committee of Chairmen">IBCC</abbr></li>
			<li>HSSC Marks Sheet</li>
			<li>HSSC Certificate</li>
			<li>General Certificate of Education from (Cambridge) (A-Level)</li>
			<li>A-Level Equivalence Certificate from
				<abbr title="Inter Board Committee of Chairmen">IBCC</abbr></li>
			<li>Bachelor Marks Sheet (2 Years BA, BSc, B.Com)</li>
			<li>Bachelor Degree (2 Years)</li>
			<li>Bachelor (04 years) Transcript</li>
			<li>Bachelor Degree (4 Years)</li>
			<li>Master Marks Sheet (2 years MA, MSc, M.Com) including MBA(3.5 year)</li>
			<li>Master Degree (2 Years / 3.5 Years)</li>
			<li>Master (MS/M-Phil) Transcript</li>
			<li>Master (MS/M-Phil) Degree</li>
			<li>Foreign Transcript</li>
			<li>Foreign Degree</li>
			<li>
				<abbr title="Higher Education Commission">HEC</abbr>
				Equivalence Certificate for Foreign Degree</li>
			<li>CNIC</li>
			<li>Passport (International Student)</li>
		</ol>
		<h4>For Undergraduate Students (Bahria University Medical & Dental College)</h4>
		<ol>
			<li>SSC Marks Sheet</li>
			<li>SSC Certificate</li>
			<li>General Certificate of Education from (Cambridge) (O-Level)</li>
			<li>O-Level  Equivalence Certificate from
				<abbr title="Inter Board Committee of Chairmen">IBCC</abbr></li>
			<li>HSSC Marks Sheet</li>
			<li>HSSC Certificate</li>
			<li>General Certificate of Education from (Cambridge) (A-Level)</li>
			<li>A-Level Equivalence Certificate from
				<abbr title="Inter Board Committee of Chairmen">IBCC</abbr></li>
			<li>SAT II Score</li>
			<li>Undertaking</li>
			<li>Acceptance Form</li>
			<li>Confidential Form</li>
			<li>Medical Report</li>
			<li>CNIC</li>
			<li>Passport (International Student)</li>
		</ol>
		<h4>For Postgraduate Students (Bahria University Medical & Dental College)</h4>
		<ol>
			<li>MBBS/BDS Degree</li>
			<li>PMDC Registration Certificate</li>
			<li>House Job Certificates</li>
			<li>Interim Transcript (Five Years)</li>
			<li>Consolidated Transcript</li>
			<li>GAT Score</li>
			<li>Undertaking</li>
			<li>Acceptance Form</li>
			<li>Confidential Form</li>
			<li>Medical Report</li>
			<li>CNIC</li>
			<li>Passport (International Student)</li>
		</ol>
	</BodyTemplate>
	<FooterTemplate>
		<aspire:AspireModalCloseButton runat="server">Close</aspire:AspireModalCloseButton>
	</FooterTemplate>
</aspire:AspireModal>
<script type="text/javascript">
	$(function () {
		var init = function () {
			var imgDocument = $("[id$=imgDocument]");
			if (imgDocument.length) {
				var viewer = new Viewer(imgDocument[0], {
					inline: false,
					viewed() { viewer.zoomTo(1); },
					toolbar: false,
					backdrop: true,
					button: true,
					navbar: false
				});
			}
		};
		Sys.Application.add_load(init);
		init();
	});
</script>
