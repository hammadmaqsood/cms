﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Common.StudentInformation
{
	public partial class StudentDocuments : System.Web.UI.UserControl, IAlert
	{
		private new BasePage Page => (BasePage)base.Page;
		private void RefreshPage(bool search, bool addNoRecordFoundAlert)
		{
			if (this.Page is Staff.Students.StudentDocuments staffPage)
			{
				if (addNoRecordFoundAlert)
					staffPage.AddNoRecordFoundAlert();
				Staff.Students.StudentDocuments.Redirect(this.Enrollment, search);
			}
			else if (this.Page is Student.StudentInformation.AcademicDocuments studentPage)
			{
				if (addNoRecordFoundAlert)
					studentPage.AddNoRecordFoundAlert();
				Student.StudentInformation.AcademicDocuments.Redirect();
			}
			else
				throw new NotImplementedException("Page: " + this.Request.Url);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.modalDocument.Visible = false;
				this.ddlDocumentType.FillGuidEnums<Model.Entities.StudentDocument.DocumentTypes>(CommonListItems.Select);
			}
		}

		private string Enrollment
		{
			get => (string)this.ViewState[nameof(this.Enrollment)];
			set => this.ViewState[nameof(this.Enrollment)] = value;
		}

		private int? StudentID
		{
			get => (int?)this.ViewState[nameof(this.StudentID)];
			set => this.ViewState[nameof(this.StudentID)] = value;
		}

		public void Show(string enrollment)
		{
			this.Visible = false;
			var result = BL.Core.StudentInformation.Common.StudentDocuments.GetStudentInfoAndDocuments(null, enrollment, AspireIdentity.Current.LoginSessionGuid);
			if (result == null)
			{
				this.RefreshPage(false, true);
				return;
			}
			short intakeSemesterID = 20191;
			if (result.IntakeSemesterID < intakeSemesterID)
			{
				this.AddErrorAlert($"Documents feature is only available for {intakeSemesterID.ToSemesterString()} onward students.");
				if (AspireIdentity.Current.UserType.IsStudent())
					BasePage.Redirect<Student.Dashboard>();
				else
					this.RefreshPage(false, false);
				return;
			}

			this.Enrollment = result.Enrollment;
			this.StudentID = result.StudentID;

			if (StaffIdentity.Current != null)
			{
				this.panelStaff.Visible = true;
				this.staffStudentInfoUserControl.SetStudentInformation(result.Enrollment, result.StudentID, result.RegistrationNo, result.Name, result.FatherName, result.ProgramAlias, result.IntakeSemesterID, result.StatusEnum, result.StatusDate, result.BlockedReason);
				this.lblDocumentStatus.Text = (result.StudentDocumentsStatus?.GetFullName()).ToNAIfNullOrEmpty();
				if (result.StatusDate != null || result.StudentDocumentsLastUpdatedBy != null)
					this.lblDocumentStatusDate.Text = $"{result.StudentDocumentsStatusDate:f} by {result.StudentDocumentsLastUpdatedBy}";
				else
					this.lblDocumentStatusDate.Text = string.Empty.ToNAIfNullOrEmpty();
				this.ltrlDocumentStatus.Text = result.StudentDocumentsRemarks.ToNAIfNullOrEmpty().HtmlEncode(true);
				this.divStaffStudentStatus.Visible = true;

				var studentDocumentsStatusEnum = result.StudentDocumentsStatus?.GetEnum<Model.Entities.Student.StudentDocumentsStatuses>();
				switch (studentDocumentsStatusEnum)
				{
					case null:
					case Model.Entities.Student.StudentDocumentsStatuses.VerificationPending:
						this.btnVerify.Text = "Verify";
						this.btnReject.Text = "Reject";
						this.btnVerify.Enabled = result.StudentDocuments.Any() && result.StudentDocuments.All(sd => sd.StatusEnum == Model.Entities.StudentDocument.Statuses.Verified);
						this.btnReject.Enabled = result.StudentDocuments.Any();
						break;
					case Model.Entities.Student.StudentDocumentsStatuses.Rejected:
						this.btnVerify.Text = "Verify";
						this.btnReject.Text = "Rejected";
						this.btnVerify.Enabled = result.StudentDocuments.Any() && result.StudentDocuments.All(sd => sd.StatusEnum == Model.Entities.StudentDocument.Statuses.Verified);
						this.btnReject.Enabled = false;
						break;
					case Model.Entities.Student.StudentDocumentsStatuses.Verified:
						this.btnVerify.Text = "Verified";
						this.btnReject.Text = "Reject";
						this.btnVerify.Enabled = false;
						this.btnReject.Enabled = result.StudentDocuments.Any();
						break;
					default:
						throw new NotImplementedEnumException(studentDocumentsStatusEnum);
				}
			}
			else
			{
				this.panelStaff.Visible = false;
				this.divStaffStudentStatus.Visible = false;
			}

			this.repeaterAcademicRecord.DataBind(result.StudentAcademicRecords.OrderBy(sar => sar.DegreeType));
			var studentDocuments = result.StudentDocuments.OrderBy(sd => (int)sd.DocumentTypeEnum).ThenBy(sd => (int)sd.PageEnum).ToList();
			this.repeaterStudentDocuments.DataBind(studentDocuments);
			this.ViewState["StudentDocumentIDs"] = studentDocuments.Select(sd => (Guid?)sd.StudentDocumentID).ToList();
			this.ViewState["UploadedDocumentTypes"] = studentDocuments.Select(sd => sd.DocumentType).ToList();
			this.Visible = true;

			if (StudentIdentity.Current != null && SessionHelper.StudentDocumentsRead != true)
			{
				SessionHelper.SetStudentDocumentsRead();
				this.btnInstructions_OnClick(null, null);
			}
		}

		protected void btnUploadDocument_Click(object sender, EventArgs e)
		{
			this.ddlDocumentType.ClearSelection();
			this.ddlDocumentType_SelectedIndexChanged(null, null);
			this.modalDocument.Visible = true;
			this.modalDocument.Show();
		}

		protected sealed class DocumentPage
		{
			public Model.Entities.StudentDocument.DocumentTypes DocumentTypeEnum { get; internal set; }
			public Model.Entities.StudentDocument.Pages PageEnum { get; internal set; }
			public bool AllowNull { get; internal set; }
		}

		protected void ddlDocumentType_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.repeaterPages.Visible = false;
			this.btnUploadDocument.Enabled = false;
			var documentType = this.ddlDocumentType.SelectedValue.ToNullableGuid();
			if (documentType == null)
			{
				this.repeaterPages.DataBind(null);
			}
			else
			{
				var documentTypeEnum = documentType.Value.GetEnum<Model.Entities.StudentDocument.DocumentTypes>();
				var documentAttribute = Model.Entities.StudentDocument.GetDocumentAttribute(documentTypeEnum);
				var uploadedDocumentTypes = this.ViewState["UploadedDocumentTypes"] as List<Guid>;

				if (documentAttribute.AllowMultiple == false && uploadedDocumentTypes?.Contains(documentType.Value) == true)
				{
					this.alertUploadDocument.AddErrorAlert($"{documentType.Value.GetFullName()} is already uploaded.");
				}
				else
				{
					var documentPages = documentAttribute.Pages.Select(p => new DocumentPage
					{
						DocumentTypeEnum = documentTypeEnum,
						PageEnum = p.Page,
						AllowNull = p.Required != true
					}).ToList();
					this.repeaterPages.DataBind(documentPages);
					this.repeaterPages.Visible = true;
					this.btnUploadDocument.Enabled = true;
				}
			}

			this.updatePanelModalDocument.Update();
			this.updatePanelSaveButton.Update();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (this.StudentID == null)
				throw new InvalidOperationException();
			var documentTypeEnum = this.ddlDocumentType.GetSelectedGuidEnum<Model.Entities.StudentDocument.DocumentTypes>();
			var documents = new List<(Model.Entities.StudentDocument.Pages PageEnum, Guid InstanceGuid, Guid TemporarySystemFileID)>();
			foreach (RepeaterItem item in this.repeaterPages.Items)
			{
				var pageEnum = ((System.Web.UI.WebControls.HiddenField)item.FindControl("hfPage")).Value.ToGuid().GetEnum<Model.Entities.StudentDocument.Pages>();
				var fileUpload = (AspireFileUpload)item.FindControl("fileUpload");
				var file = fileUpload.UploadedFiles?.SingleOrDefault();
				if (file != null)
					documents.Add((pageEnum, file.Instance, file.FileID.ToGuid()));
			}
			var result = BL.Core.StudentInformation.Common.StudentDocuments.AddStudentDocument(this.StudentID.Value, documentTypeEnum, documents, this.Page.AspireIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.StudentInformation.Common.StudentDocuments.AddStudentDocumentStatuses.NoRecordFound:
					this.RefreshPage(false, true);
					return;
				case BL.Core.StudentInformation.Common.StudentDocuments.AddStudentDocumentStatuses.DuplicateDocumentNotAllowed:
					this.AddErrorAlert($"{documentTypeEnum.GetFullName()} is already uploaded.");
					this.RefreshPage(true, false);
					return;
				case BL.Core.StudentInformation.Common.StudentDocuments.AddStudentDocumentStatuses.DocumentIsNotJpeg:
					this.alertUploadDocument.AddSuccessAlert("Only jpg/jpeg format documents are allowed.");
					this.updatePanelModalDocument.Update();
					return;
				case BL.Core.StudentInformation.Common.StudentDocuments.AddStudentDocumentStatuses.FileNotFound:
				case BL.Core.StudentInformation.Common.StudentDocuments.AddStudentDocumentStatuses.TemporaryFileNotFound:
					this.AddErrorAlert($"File not found.");
					this.RefreshPage(true, false);
					return;
				case BL.Core.StudentInformation.Common.StudentDocuments.AddStudentDocumentStatuses.DocumentIsNotValid:
					this.AddErrorAlert($"Document Uploaded is not valid. Only .jpg and .jpeg images are allowed.");
					this.RefreshPage(true, false);
					return;
				case BL.Core.StudentInformation.Common.StudentDocuments.AddStudentDocumentStatuses.Success:
					this.AddSuccessAlert("Document has been uploaded.");
					this.RefreshPage(true, false);
					return;
				case BL.Core.StudentInformation.Common.StudentDocuments.AddStudentDocumentStatuses.StudentCanNotAddDocumentAfterVerification:
					this.AddErrorAlert("Documents can not be uploaded after verification.");
					this.RefreshPage(true, false);
					return;
				default:
					throw new NotImplementedEnumException(result);
			}
		}

		protected void repeaterStudentDocuments_OnItemCommand(object sender, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "View":
					this.ShowDocument(e.DecryptedCommandArgument().ToGuid());
					return;
			}
		}

		private void ShowDocument(Guid studentDocumentID)
		{
			var document = BL.Core.StudentInformation.Common.StudentDocuments.GetStudentDocument(studentDocumentID, this.Page.AspireIdentity.LoginSessionGuid);
			if (document == null)
			{
				this.RefreshPage(true, true);
				return;
			}

			this.ViewState[nameof(document.StudentDocumentID)] = document.StudentDocumentID;
			this.ViewState[nameof(document.DocumentTypeEnum)] = document.DocumentTypeEnum;
			this.tbDocumentType.Text = document.DocumentTypeEnum.GetFullName();
			this.tbPage.Text = document.PageEnum.ToFullName();
			this.imgDocument.ImageUrl = StudentDocument.GetImageUrl(document.StudentDocumentID);
			this.imgDocument.AlternateText = $"{document.DocumentType.GetFullName()} - {document.Page.GetFullName()}";
			this.tbDimensions.Text = document.Dimensions;
			this.tbResolution.Text = document.Resolution;
			if (document.ImageDpiX < 150 || document.ImageDpiY < 150)
				this.divResolution.Attributes.Add("class", "form-group has-error");
			else
				this.divResolution.Attributes.Add("class", "form-group");
			if (this.ddlStatus.Items.Count == 0)
				this.ddlStatus.FillGuidEnums<Model.Entities.StudentDocument.Statuses>(CommonListItems.Select);
			this.ddlStatus.SetSelectedGuidEnum(document.StatusEnum);
			this.ddlStatus_OnSelectedIndexChanged(null, null);
			this.tbRemarks.Text = document.Remarks;
			this.modalView.Visible = true;
			this.modalView.Show();
			var studentDocumentIDs = (List<Guid?>)this.ViewState["StudentDocumentIDs"];
			Guid? nextStudentDocumentID = null;
			if (studentDocumentIDs != null)
				nextStudentDocumentID = studentDocumentIDs.SkipWhile(id => id != document.StudentDocumentID).Skip(1).FirstOrDefault();
			this.btnSaveAndNextStudentDocument.Visible = nextStudentDocumentID != null;
			this.btnSkip.Visible = nextStudentDocumentID != null;
			this.btnSaveAndNextStudentDocument.EncryptedCommandArgument = nextStudentDocumentID?.ToString();
			this.updatePanelModalView.Update();
			this.updatePanelModalViewButtons.Update();
		}

		protected void ddlStatus_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var status = this.ddlStatus.GetSelectedNullableGuidEnum<Model.Entities.StudentDocument.Statuses>();
			switch (status)
			{
				case null:
				case Model.Entities.StudentDocument.Statuses.VerificationPending:
				case Model.Entities.StudentDocument.Statuses.Verified:
					this.rfvtbRemarks.Enabled = false;
					this.tbRemarks.ReadOnly = true;
					this.tbRemarks.Text = null;
					break;
				case Model.Entities.StudentDocument.Statuses.Rejected:
					this.rfvtbRemarks.Enabled = true;
					this.tbRemarks.ReadOnly = false;
					break;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		protected void btnSaveStudentDocument_OnClick(object sender, EventArgs e)
		{
			if (sender == this.btnSkip)
			{
				this.ShowDocument(this.btnSaveAndNextStudentDocument.EncryptedCommandArgument.ToGuid());
				return;
			}
			var next = sender == this.btnSaveAndNextStudentDocument;
			var studentDocumentID = (Guid)this.ViewState[nameof(Model.Entities.StudentDocument.StudentDocumentID)];

			var studentDocumentStatusEnum = this.ddlStatus.GetSelectedGuidEnum<Model.Entities.StudentDocument.Statuses>();
			var remarks = this.tbRemarks.Text;
			if (this.tbRemarks.ReadOnly)
				remarks = null;
			var result = BL.Core.StudentInformation.Staff.StudentDocuments.UpdateStudentDocument(studentDocumentID, studentDocumentStatusEnum, remarks, this.Page.AspireIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.StudentInformation.Staff.StudentDocuments.UpdateStudentDocumentStatuses.NoRecordFound:
					this.RefreshPage(true, true);
					return;
				case BL.Core.StudentInformation.Staff.StudentDocuments.UpdateStudentDocumentStatuses.Success:
					if (next)
					{
						this.alertViewDocument.AddSuccessAlert("Document status has been updated.");
						this.ShowDocument(this.btnSaveAndNextStudentDocument.EncryptedCommandArgument.ToGuid());
					}
					else
					{
						this.AddSuccessAlert("Document status has been updated.");
						this.RefreshPage(true, false);
					}
					return;
				case BL.Core.StudentInformation.Staff.StudentDocuments.UpdateStudentDocumentStatuses.LowResolutionIsNotAllowed:
					this.alertViewDocument.AddErrorAlert("Resolution must be at least 150px.");
					this.updatePanelModalView.Update();
					return;
				default:
					throw new NotImplementedEnumException(result);
			}
		}

		protected void btnDelete_Click(object sender, EventArgs e)
		{
			var documentTypeEnum = (Model.Entities.StudentDocument.DocumentTypes)this.ViewState[nameof(Model.Entities.StudentDocument.DocumentTypeEnum)];
			var result = BL.Core.StudentInformation.Common.StudentDocuments.DeleteStudentDocument(this.StudentID ?? throw new InvalidOperationException(), documentTypeEnum, this.Page.AspireIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.StudentInformation.Common.StudentDocuments.DeleteStudentDocumentStatuses.NoRecordFound:
					this.RefreshPage(true, true);
					return;
				case BL.Core.StudentInformation.Common.StudentDocuments.DeleteStudentDocumentStatuses.Success:
					this.AddSuccessMessageHasBeenDeleted("Document");
					this.RefreshPage(true, false);
					return;
				case BL.Core.StudentInformation.Common.StudentDocuments.DeleteStudentDocumentStatuses.StudentCanNotDeleteDocumentAfterVerification:
					this.AddErrorAlert("Documents can not be deleted after verification.");
					this.RefreshPage(true, false);
					return;
				default:
					throw new NotImplementedEnumException(result);
			}
		}

		protected void btnCancel_Click(object sender, EventArgs e)
		{
			this.RefreshPage(true, false);
		}

		protected void btnRejected_Click(object sender, EventArgs e)
		{
			this.modalReject.Visible = true;
			this.tbRejectRemarks.Text = null;
			this.modalReject.Show();
		}

		protected void btnVerify_Click(object sender, EventArgs e)
		{
			var result = BL.Core.StudentInformation.Staff.StudentDocuments.VerifyStudentDocuments(this.StudentID ?? throw new InvalidOperationException(), this.Page.AspireIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.StudentInformation.Staff.StudentDocuments.VerifyStudentDocumentsStatuses.NoRecordFound:
					this.RefreshPage(false, true);
					return;
				case BL.Core.StudentInformation.Staff.StudentDocuments.VerifyStudentDocumentsStatuses.AlreadyVerified:
					this.AddErrorAlert("Documents are already verified.");
					this.RefreshPage(true, false);
					return;
				case BL.Core.StudentInformation.Staff.StudentDocuments.VerifyStudentDocumentsStatuses.AllDocumentsAreNotVerified:
					this.AddErrorAlert("All Documents are not yet verified.");
					this.RefreshPage(true, false);
					return;
				case BL.Core.StudentInformation.Staff.StudentDocuments.VerifyStudentDocumentsStatuses.NoDocumentsFound:
					this.AddErrorAlert("No Document found for verification.");
					this.RefreshPage(true, false);
					return;
				case BL.Core.StudentInformation.Staff.StudentDocuments.VerifyStudentDocumentsStatuses.Success:
					this.AddSuccessAlert("Documents have been verified.");
					this.RefreshPage(true, false);
					return;
				default:
					throw new NotImplementedEnumException(result);
			}
		}

		protected void btnRejectSave_Click(object sender, EventArgs e)
		{
			var result = BL.Core.StudentInformation.Staff.StudentDocuments.RejectStudentDocuments(this.StudentID ?? throw new InvalidOperationException(), this.tbRejectRemarks.Text, this.Page.AspireIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.StudentInformation.Staff.StudentDocuments.RejectStudentDocumentsStatuses.NoRecordFound:
					this.RefreshPage(false, true);
					return;
				case BL.Core.StudentInformation.Staff.StudentDocuments.RejectStudentDocumentsStatuses.Success:
					this.AddSuccessAlert("Documents have been rejected.");
					this.RefreshPage(true, false);
					return;
				default:
					throw new NotImplementedEnumException(result);
			}
		}

		protected void btnInstructions_OnClick(object sender, EventArgs e)
		{
			this.modalInstructions.Visible = true;
			this.modalInstructions.Show();
		}
	}
}