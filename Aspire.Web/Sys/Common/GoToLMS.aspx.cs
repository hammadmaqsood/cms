﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Common
{
	[AspirePage("{D61A52B4-76CD-442C-BCEE-EFE52EEE3F78}", UserTypes.Faculty | UserTypes.Student | UserTypes.Executive, "~/Sys/Common/GoToLMS.aspx", "Go To LMS", true, AspireModules.LMS)]
	public partial class GoToLMS : AspireBasePage
	{
		private static IEnumerable<UserTypes> AllowedUserTypes => new[] { UserTypes.Student, UserTypes.Faculty, UserTypes.Executive };
		public override AspireThemes AspireTheme => AspireDefaultThemes.GetDefaultThemeForCurrentUser();
		public override PageIcon PageIcon => AspireGlyphicons.arrow_right.GetIcon();
		public override string PageTitle => "Go To LMS";

		protected override void Authenticate()
		{
			var userType = this.AspireIdentity?.UserType;
			if (userType == null || !AllowedUserTypes.Contains(userType.Value))
				throw new AspireAccessDeniedException(UserTypes.Faculty, UserTypes.Faculty);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				Guid key;
				switch (this.AspireIdentity.UserType)
				{
					case UserTypes.Faculty:
						key = BL.Core.IntegrationServices.LMS.Authentication.AddFacultyMember(FacultyIdentity.Current.FacultyMemberID, FacultyIdentity.Current.Name, FacultyIdentity.Current.Email);
						break;
					case UserTypes.Student:
						var qaStudentOnlineTeachingSurvey = BL.Core.QualityAssurance.Student.HECOnlineEducationSurvey.GetQAStudentOnlineTeachingSurvey(Sys.Student.QualityAssurance.HECOnlineEducationSurvey.SemesterID, StudentIdentity.Current.LoginSessionGuid);
						if (qaStudentOnlineTeachingSurvey?.SubmittedDate == null)
						{
							Redirect<Sys.Student.QualityAssurance.HECOnlineEducationSurvey>();
							return;
						}
						key = BL.Core.IntegrationServices.LMS.Authentication.AddStudent(StudentIdentity.Current.StudentID, StudentIdentity.Current.Name, StudentIdentity.Current.Email);
						break;
					case UserTypes.Executive:
						key = BL.Core.IntegrationServices.LMS.Authentication.AddExecutive(ExecutiveIdentity.Current.UserID, ExecutiveIdentity.Current.Name, ExecutiveIdentity.Current.Email);
						break;
					default:
						throw new NotImplementedEnumException(this.AspireIdentity.UserType);
				}

				if (Global.LiveSite)
					this.Response.Redirect($"https://lms.bahria.edu.pk/auth.php?C={key:N}");
				else
					this.Response.Redirect($"https://dev-lms.bahria.edu.pk/auth.php?C={key:N}");
			}
		}
	}
}