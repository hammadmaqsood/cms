﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QarzeHasanaForm.ascx.cs" Inherits="Aspire.Web.Sys.Common.Scholarships.QarzeHasana.QarzeHasanaForm" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<div runat="server" id="divPrint">
	<aspire:AspireHyperLinkButton ButtonType="Primary" Glyphicon="print" runat="server" ID="hlPrintReport" Text="Print Application Form" Target="_blank" />
	<p></p>
</div>
<asp:Panel CssClass="panel panel-default" runat="server" ID="PanelStudentInfo">
	<div class="panel-heading">
		<h3 class="panel-title">Student Information</h3>
	</div>
	<table class="table table-bordered table-condensed info table-responsive tableCol4">
		<tr>
			<th>Enrollment</th>
			<td>
				<aspire:AspireLabel runat="server" ID="lblEnrollment" />
			</td>
			<th>Registration No.</th>
			<td>
				<aspire:AspireLabel runat="server" ID="lblRegistration" />
			</td>
		</tr>
		<tr>
			<th>Program</th>
			<td>
				<aspire:AspireLabel runat="server" ID="lblProgram" />
			</td>
			<th>Semester</th>
			<td>
				<aspire:AspireLabel runat="server" ID="lblSemester" />
			</td>
		</tr>
		<tr>
			<th>Name</th>
			<td>
				<aspire:AspireLabel runat="server" ID="lblStudentName" />
			</td>
			<th>Father Name</th>
			<td>
				<aspire:AspireLabel runat="server" ID="lblFatherName" />
			</td>
		</tr>
		<tr>
			<th>Date of Birth</th>
			<td>
				<aspire:AspireLabel runat="server" ID="lblDOB" />
			</td>
			<th>Gender</th>
			<td>
				<aspire:AspireLabel runat="server" ID="lblGender" />
			</td>
		</tr>
		<tr>
			<th>Section</th>
			<td>
				<aspire:AspireLabel runat="server" ID="lblSection" />
			</td>
			<th>Email</th>
			<td>
				<aspire:AspireLabel runat="server" ID="lblEmail" />
			</td>
		</tr>
		<tr>
			<th>Domicile</th>
			<td>
				<aspire:AspireLabel runat="server" ID="lblDomicile" />
			</td>
			<th>Last Result</th>
			<td>
				<aspire:AspireLabel runat="server" ID="lblGPA" />
			</td>
		</tr>
		<tr>
			<th>Status</th>
			<td>
				<aspire:AspireLabel runat="server" ID="lblScholarshipApplicationStatus" />
			</td>
			<th>
				<aspire:AspireLabel runat="server" ID="lblDocumentsStatus" Text="Documents" />
			</th>
			<td>
				<aspire:AspireLabel runat="server" ID="lblDocuments" />
			</td>
		</tr>
	</table>

	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Phone No.:" AssociatedControlID="lblPhone" />
					<div>
						<aspire:Label runat="server" ID="lblPhone" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Mobile No.:" AssociatedControlID="lblMobile" />
					<div>
						<aspire:Label runat="server" ID="lblMobile" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Residential Address:" AssociatedControlID="lblResidentialAddress" />
					<div>
						<aspire:Label runat="server" ID="lblResidentialAddress" />
					</div>
				</div>
			</div>
		</div>
		<div class="text-center">
			<aspire:AspireButton runat="server" ID="btnEditStudentInfo" ButtonType="Primary" Text="Edit" data-toggle="modal" data-target="#modalStudentInfo" />
		</div>
	</div>
</asp:Panel>

<asp:Panel CssClass="panel panel-default" runat="server" ID="PanelFamilyInformation">
	<div class="panel-heading">
		<h3 class="panel-title">Family Information</h3>
	</div>
	<div class="panel-body">
		<aspire:AspireButton runat="server" ID="btnAddFamilyMember" ButtonType="Success" Glyphicon="plus" Text="Add" OnClick="btnAddFamilyMember_OnClick" />
		<aspire:AspireGridView runat="server" ID="gvFamilyMember" AutoGenerateColumns="False" AllowPaging="False" Caption="Family members living in the same house" OnRowCommand="gvFamilyMember_OnRowCommand">
			<Columns>
				<asp:TemplateField HeaderText="#">
					<ItemTemplate>
						<%#Container.DataItemIndex + 1 %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="Relationship" DataField="Relation" />
				<asp:BoundField HeaderText="Occupation" DataField="Occupation" NullDisplayText="N/A" />
				<asp:BoundField HeaderText="Age (Years)" DataField="Age" />
				<asp:TemplateField HeaderText="Actions">
					<ItemTemplate>
						<aspire:AspireLinkButton runat="server" CausesValidation="false" CommandName="Edit" Glyphicon="edit" EncryptedCommandArgument='<%# this.Eval("FamilyMemberID") %>' />
						<aspire:AspireLinkButton runat="server" CausesValidation="false" CommandName="Delete" Glyphicon="remove" ConfirmMessage="Are you sure you want to delete this record?" EncryptedCommandArgument='<%# this.Eval("FamilyMemberID") %>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="No. of family members studying in Bahria University:" AssociatedControlID="lblSiblingsStudyingAtBU" />
					<div>
						<aspire:Label runat="server" ID="lblSiblingsStudyingAtBU" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Siblings (Brothers and Sisters) studying in other educational institutions:" AssociatedControlID="lblSiblingsStudyingAtOthers" />
					<div>
						<aspire:Label runat="server" ID="lblSiblingsStudyingAtOthers" />
					</div>
				</div>
			</div>
		</div>
		<div class="text-center">
			<aspire:AspireButton runat="server" ID="btnEditFamilyInformation" ButtonType="Primary" Text="Edit" data-toggle="modal" data-target="#modalFamilyInformation" />
		</div>
	</div>
</asp:Panel>

<asp:Panel CssClass="panel panel-default" runat="server" ID="PanelFamilyAssets">
	<div class="panel-heading">
		<h3 class="panel-title">Statement of Family Assets</h3>
	</div>
	<div class="panel-body">
		<aspire:AspireButton runat="server" ID="btnAddFamilyAsset" ButtonType="Success" Glyphicon="plus" Text="Add" OnClick="btnAddFamilyAsset_OnClick" />
		<aspire:AspireGridView runat="server" ID="gvFamilyAsset" AutoGenerateColumns="False" AllowPaging="False" Caption="Houses/Agricultural Lands/Plots" EmptyDataText="N/A" OnRowCommand="gvFamilyAsset_OnRowCommand">
			<Columns>
				<asp:TemplateField HeaderText="#">
					<ItemTemplate>
						<%#Container.DataItemIndex + 1 %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Asset Type">
					<ItemTemplate>
						<%# (((AssetTypes)this.Eval("AssetType").ToString().ToByte()).ToString()).SplitCamelCasing() %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="Owned By" DataField="OwnedBy" />
				<asp:BoundField HeaderText="Locality" DataField="Locality" />
				<asp:BoundField HeaderText="Area" DataField="Area" />
				<asp:BoundField HeaderText="Value" DataField="PriceValue" NullDisplayText="N/A" />
				<asp:TemplateField HeaderText="Actions">
					<ItemTemplate>
						<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Edit" Glyphicon="edit" EncryptedCommandArgument='<%# this.Eval("FamilyAssetID") %>' />
						<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Delete" Glyphicon="remove" ConfirmMessage="Are you sure you want to delete this record?" EncryptedCommandArgument='<%# this.Eval("FamilyAssetID") %>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>

		<p></p>
		<aspire:AspireButton runat="server" ID="btnAddVehicle" ButtonType="Success" Glyphicon="plus" Text="Add" OnClick="btnAddVehicle_OnClick" />
		<aspire:AspireGridView runat="server" ID="gvVehicle" AutoGenerateColumns="False" AllowPaging="False" Caption="Vehicles" EmptyDataText="N/A" OnRowCommand="gvVehicle_OnRowCommand">
			<Columns>
				<asp:TemplateField HeaderText="#">
					<ItemTemplate>
						<%#Container.DataItemIndex + 1 %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="Owned By" DataField="OwnedBy" />
				<asp:BoundField HeaderText="Make" DataField="Make" />
				<asp:BoundField HeaderText="Model" DataField="Model" />
				<asp:BoundField HeaderText="CC" DataField="CC" />
				<asp:BoundField HeaderText="Value" DataField="PriceValue" NullDisplayText="N/A" />
				<asp:TemplateField HeaderText="Actions">
					<ItemTemplate>
						<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Edit" Glyphicon="edit" EncryptedCommandArgument='<%# this.Eval("VehicleID") %>' />
						<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Delete" Glyphicon="remove" ConfirmMessage="Are you sure you want to delete this record?" EncryptedCommandArgument='<%# this.Eval("VehicleID") %>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>

		<p></p>
		<aspire:AspireButton runat="server" ID="btnAddSaving" ButtonType="Success" Glyphicon="plus" Text="Add" OnClick="btnAddSaving_OnClick" />
		<aspire:AspireGridView runat="server" ID="gvSaving" AutoGenerateColumns="False" AllowPaging="False" Caption="Savings/Investments" EmptyDataText="N/A" OnRowCommand="gvSaving_OnRowCommand">
			<Columns>
				<asp:TemplateField HeaderText="#">
					<ItemTemplate>
						<%#Container.DataItemIndex + 1 %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="Owned By" DataField="OwnedBy" />
				<asp:BoundField HeaderText="Type" DataField="Type" />
				<asp:BoundField HeaderText="Value" DataField="PriceValue" />
				<asp:TemplateField HeaderText="Actions">
					<ItemTemplate>
						<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Edit" Glyphicon="edit" EncryptedCommandArgument='<%# this.Eval("SavingID") %>' />
						<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Delete" Glyphicon="remove" ConfirmMessage="Are you sure you want to delete this record?" EncryptedCommandArgument='<%# this.Eval("SavingID") %>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
	</div>
</asp:Panel>

<asp:Panel CssClass="panel panel-default" runat="server" ID="PanelFamilyIncome">
	<div class="panel-heading">
		<h3 class="panel-title">Statement of Family Income</h3>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Father Income (Per Annum):" AssociatedControlID="lblFatherIncome" />
					<div>
						<aspire:Label runat="server" ID="lblFatherIncome" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Mother Income (Per Annum):" AssociatedControlID="lblMotherIncome" />
					<div>
						<aspire:Label runat="server" ID="lblMotherIncome" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Spouse Income (Per Annum):" AssociatedControlID="lblSpouseIncome" />
					<div>
						<aspire:Label runat="server" ID="lblSpouseIncome" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Self Income (Per Annum):" AssociatedControlID="lblSelfIncome" />
					<div>
						<aspire:Label runat="server" ID="lblSelfIncome" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Sibling Income (Per Annum):" AssociatedControlID="lblSiblingIncome" />
					<div>
						<aspire:Label runat="server" ID="lblSiblingIncome" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Persons (including you) depend on your parents:" AssociatedControlID="lblPersonsDependingOnParentsIncludingYou" />
					<div>
						<aspire:Label runat="server" ID="lblPersonsDependingOnParentsIncludingYou" />
					</div>
				</div>
			</div>
		</div>
		<div class="text-center">
			<aspire:AspireButton runat="server" ID="btnEditFamilyIncome" ButtonType="Primary" Text="Edit" data-toggle="modal" data-target="#modalFamilyIncome" />
		</div>
		<p></p>
		<aspire:AspireButton runat="server" ID="btnAddOtherIncome" ButtonType="Success" Glyphicon="plus" Text="Add" OnClick="btnAddOtherIncome_OnClick" />
		<aspire:AspireGridView runat="server" ID="gvOtherIncome" AutoGenerateColumns="False" AllowPaging="False" Caption="Other Incomes (e.g. business) per annum" OnRowCommand="gvOtherIncome_OnRowCommand">
			<Columns>
				<asp:TemplateField HeaderText="#">
					<ItemTemplate>
						<%#Container.DataItemIndex + 1 %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="Income Type" DataField="IncomeType" />
				<asp:BoundField HeaderText="Value" DataField="PriceValue" />
				<asp:BoundField HeaderText="Explanation" DataField="Description" NullDisplayText="N/A" />
				<asp:TemplateField HeaderText="Actions">
					<ItemTemplate>
						<aspire:AspireLinkButton runat="server" CommandName="Edit" Glyphicon="edit" EncryptedCommandArgument='<%# this.Eval("OtherIncomeID") %>' />
						<aspire:AspireLinkButton runat="server" CommandName="Delete" Glyphicon="remove" ConfirmMessage="Are you sure you want to delete this record?" EncryptedCommandArgument='<%# this.Eval("OtherIncomeID") %>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
	</div>
</asp:Panel>

<asp:Panel CssClass="panel panel-default" runat="server" ID="PanelFamilyExpenditures">
	<div class="panel-heading">
		<h3 class="panel-title">Statement of Family Expenditures</h3>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Children Fee (Per Annum):" AssociatedControlID="lblChildrenFee" />
					<div>
						<aspire:Label runat="server" ID="lblChildrenFee" />
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Electricity Bill (Per Annum):" AssociatedControlID="lblElectricityBill" />
					<div>
						<aspire:Label runat="server" ID="lblElectricityBill" />
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Loan (Re-paid) (Per Annum):" AssociatedControlID="lblLoan" />
					<div>
						<aspire:Label runat="server" ID="lblLoan" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="House Rent (Per Annum):" AssociatedControlID="lblHouseRent" />
					<div>
						<aspire:Label runat="server" ID="lblHouseRent" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Telephone Bill (Per Annum):" AssociatedControlID="lblTelephoneBill" />
					<div>
						<aspire:Label runat="server" ID="lblTelephoneBill" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Medical Expenses (Per Annum):" AssociatedControlID="lblMedicalExpenses" />
					<div>
						<aspire:Label runat="server" ID="lblMedicalExpenses" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Taxes Paid (Per Annum):" AssociatedControlID="lblTaxesPaid" />
					<div>
						<aspire:Label runat="server" ID="lblTaxesPaid" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Others (Per Annum):" AssociatedControlID="lblOtherExpenses" />
					<div>
						<aspire:Label runat="server" ID="lblOtherExpenses" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Educational expenses (Per Annum) family can pay:" AssociatedControlID="lblEducationalExpensesFamilyCanPay" />
					<div>
						<aspire:Label runat="server" ID="lblEducationalExpensesFamilyCanPay" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Your required educational expenses (Per Annum):" AssociatedControlID="lblReqEducationalExpenses" />
					<div>
						<aspire:Label runat="server" ID="lblReqEducationalExpenses" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Additional Details:" AssociatedControlID="lblAdditionalDetails" />
					<div>
						<aspire:Label runat="server" ID="lblAdditionalDetails" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="text-center">
		<aspire:AspireButton runat="server" ID="btnEditFamilyExpenditures" ButtonType="Primary" Text="Edit" data-toggle="modal" data-target="#modalFamilyExpenditures" />
	</div>
	<p></p>
</asp:Panel>

<div class="text-center">
	<aspire:AspireButton runat="server" ID="btnSubmit" ButtonType="Primary" Text="Submit" ConfirmMessage="Are you sure you want to submit application?" OnClick="btnSubmit_OnClick" />
	<aspire:AspireLinkButton runat="server" ID="btnCancelForm" Text="Cancel" OnClick="btnCancelForm_OnClick" />
</div>

<%-- Aspire Modals --%>
<aspire:AspireModal runat="server" ID="modalFamilyMember">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updateModalFamilyMember" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alertModalFamilyMember" />
				<div class="form-horizontal">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Relationship:" AssociatedControlID="tbRelationship" CssClass="col-md-3" />
						<div class="col-md-9">
							<aspire:AspireTextBox runat="server" ID="tbRelationship" ValidationGroup="SaveFamilyMember" />
							<aspire:AspireStringValidator runat="server" ControlToValidate="tbRelationship" ValidationGroup="SaveFamilyMember" AllowNull="False" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Occupation:" AssociatedControlID="tbOccupation" CssClass="col-md-3" />
						<div class="col-md-9">
							<aspire:AspireTextBox runat="server" ID="tbOccupation" ValidationGroup="SaveFamilyMember" />
							<aspire:AspireStringValidator runat="server" ControlToValidate="tbOccupation" ValidationGroup="SaveFamilyMember" AllowNull="True" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Age:" AssociatedControlID="tbAge" CssClass="col-md-3" />
						<div class="col-md-9">
							<div class="input-group">
								<aspire:AspireTextBox runat="server" ID="tbAge" ValidationGroup="SaveFamilyMember" />
								<span class="input-group-addon">Years</span>
							</div>
							<aspire:AspireInt16Validator runat="server" AllowNull="false" ControlToValidate="tbAge" ValidationGroup="SaveFamilyMember" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="1" MaxValue="120" />
						</div>
					</div>

				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnSaveFamilyMember" Text="Save" ButtonType="Primary" ValidationGroup="SaveFamilyMember" OnClick="btnSaveFamilyMember_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
			<Triggers>
				<asp:PostBackTrigger ControlID="btnSaveFamilyMember" />
			</Triggers>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>

<aspire:AspireModal runat="server" ID="modalFamilyAsset">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updateModalFamilyAsset" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alertModalFamilyAsset" />
				<div class="form-horizontal">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Asset Type:" AssociatedControlID="ddlAssetType" CssClass="col-md-3" />
						<div class="col-md-9">
							<aspire:AspireDropDownList runat="server" ID="ddlAssetType" ValidationGroup="SaveFamilyAsset" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Owned By:" AssociatedControlID="tbOwner" CssClass="col-md-3" />
						<div class="col-md-9">
							<aspire:AspireTextBox runat="server" ID="tbOwner" ValidationGroup="SaveFamilyAsset" />
							<aspire:AspireStringValidator runat="server" ControlToValidate="tbOwner" ValidationGroup="SaveFamilyAsset" AllowNull="False" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Locality:" AssociatedControlID="tbLocality" CssClass="col-md-3" />
						<div class="col-md-9">
							<aspire:AspireTextBox runat="server" ID="tbLocality" ValidationGroup="SaveFamilyAsset" />
							<aspire:AspireStringValidator runat="server" ControlToValidate="tbLocality" ValidationGroup="SaveFamilyAsset" AllowNull="False" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Area:" AssociatedControlID="tbArea" CssClass="col-md-3" />
						<div class="col-md-9">
							<aspire:AspireTextBox runat="server" ID="tbArea" ValidationGroup="SaveFamilyAsset" />
							<aspire:AspireStringValidator runat="server" ControlToValidate="tbArea" ValidationGroup="SaveFamilyAsset" AllowNull="False" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Value:" AssociatedControlID="tbValue" CssClass="col-md-3" />
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbValue" ValidationGroup="SaveFamilyAsset" />
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbValue" AllowNull="True" ValidationGroup="SaveFamilyAsset" InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>

				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnSaveFamilyAsset" Text="Save" ButtonType="Primary" ValidationGroup="SaveFamilyAsset" OnClick="btnSaveFamilyAsset_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
			<Triggers>
				<asp:PostBackTrigger ControlID="btnSaveFamilyAsset" />
			</Triggers>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>

<aspire:AspireModal runat="server" ID="modalVehicle">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updateModalVehicle" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alertModalVehicle" />
				<div class="form-horizontal">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Owned By:" AssociatedControlID="tbOwnerVehicle" CssClass="col-md-3" />
						<div class="col-md-9">
							<aspire:AspireTextBox runat="server" ID="tbOwnerVehicle" ValidationGroup="SaveVehicle" />
							<aspire:AspireStringValidator runat="server" ControlToValidate="tbOwnerVehicle" ValidationGroup="SaveVehicle" AllowNull="False" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Make:" AssociatedControlID="tbMakeVehicle" CssClass="col-md-3" />
						<div class="col-md-9">
							<aspire:AspireTextBox runat="server" ID="tbMakeVehicle" ValidationGroup="SaveVehicle" />
							<aspire:AspireStringValidator runat="server" ControlToValidate="tbMakeVehicle" ValidationGroup="SaveVehicle" AllowNull="False" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Model:" AssociatedControlID="tbModel" CssClass="col-md-3" />
						<div class="col-md-9">
							<aspire:AspireTextBox runat="server" ID="tbModel" ValidationGroup="SaveVehicle" />
							<aspire:AspireStringValidator runat="server" ControlToValidate="tbModel" ValidationGroup="SaveVehicle" AllowNull="False" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="C.C:" AssociatedControlID="tbCC" CssClass="col-md-3" />
						<div class="col-md-9">
							<aspire:AspireTextBox runat="server" ID="tbCC" ValidationGroup="SaveVehicle" />
							<aspire:AspireStringValidator runat="server" ControlToValidate="tbCC" ValidationGroup="SaveVehicle" AllowNull="False" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Value:" AssociatedControlID="tbValueVehicle" CssClass="col-md-3" />
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon2">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbValueVehicle" ValidationGroup="SaveVehicle" />
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbValueVehicle" AllowNull="True" ValidationGroup="SaveVehicle" InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>

				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnSaveVehicle" Text="Save" ButtonType="Primary" ValidationGroup="SaveVehicle" OnClick="btnSaveVehicle_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
			<Triggers>
				<asp:PostBackTrigger ControlID="btnSaveVehicle" />
			</Triggers>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>

<aspire:AspireModal runat="server" ID="modalSaving">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updateModalSaving" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alertModalSaving" />
				<div class="form-horizontal">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Owned By:" AssociatedControlID="tbOwnerSaving" CssClass="col-md-3" />
						<div class="col-md-9">
							<aspire:AspireTextBox runat="server" ID="tbOwnerSaving" ValidationGroup="SaveSaving" />
							<aspire:AspireStringValidator runat="server" ControlToValidate="tbOwnerSaving" ValidationGroup="SaveSaving" AllowNull="False" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="tbType" CssClass="col-md-3" />
						<div class="col-md-9">
							<aspire:AspireTextBox runat="server" ID="tbType" ValidationGroup="SaveSaving" />
							<aspire:AspireStringValidator runat="server" ControlToValidate="tbType" ValidationGroup="SaveSaving" AllowNull="False" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Value:" AssociatedControlID="tbValueSaving" CssClass="col-md-3" />
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbValueSaving" ValidationGroup="SaveSaving" />
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbValueSaving" AllowNull="False" ValidationGroup="SaveSaving" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>

				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnSaveSaving" Text="Save" ButtonType="Primary" ValidationGroup="SaveSaving" OnClick="btnSaveSaving_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
			<Triggers>
				<asp:PostBackTrigger ControlID="btnSaveSaving" />
			</Triggers>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>

<aspire:AspireModal runat="server" ID="modalOtherIncome">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updateModalOtherIncome" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alertModalOtherIncome" />
				<div class="form-horizontal">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Income Type:" AssociatedControlID="tbIncomeType" CssClass="col-md-3" />
						<div class="col-md-9">
							<aspire:AspireTextBox runat="server" ID="tbIncomeType" ValidationGroup="SaveOtherIncome" />
							<aspire:AspireStringValidator runat="server" ControlToValidate="tbIncomeType" ValidationGroup="SaveOtherIncome" AllowNull="False" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Value:" AssociatedControlID="tbValueOtherIncome" CssClass="col-md-3" />
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbValueOtherIncome" ValidationGroup="SaveOtherIncome" />
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbValueOtherIncome" AllowNull="False" ValidationGroup="SaveOtherIncome" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Income Explanation:" AssociatedControlID="tbIncomeExplanation" CssClass="col-md-3" />
						<div class="col-md-9">
							<aspire:AspireTextBox runat="server" ID="tbIncomeExplanation" ValidationGroup="SaveOtherIncome" TextMode="MultiLine" MaxLength="500" />
							<aspire:AspireStringValidator runat="server" ControlToValidate="tbIncomeExplanation" ValidationGroup="SaveOtherIncome" AllowNull="True" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." />
						</div>
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Always">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnSaveOtherIncome" Text="Save" ButtonType="Primary" ValidationGroup="SaveOtherIncome" OnClick="btnSaveOtherIncome_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
			<Triggers>
				<asp:PostBackTrigger ControlID="btnSaveOtherIncome" />
			</Triggers>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>

<%-- Models to edit text fields --%>
<div class="modal fade" id="modalStudentInfo" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Student Information</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Phone No.:" AssociatedControlID="tbPhone" />
					<aspire:AspireTextBox runat="server" ID="tbPhone" ValidationGroup="SaveStudentInfo" MaxLength="50" />
					<aspire:AspireStringValidator runat="server" ControlToValidate="tbPhone" ValidationGroup="SaveStudentInfo" AllowNull="True" RequiredErrorMessage="This field is required." />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Mobile No.:" AssociatedControlID="tbMobile" />
					<aspire:AspireTextBox runat="server" ID="tbMobile" ValidationGroup="SaveStudentInfo" data-inputmask="'alias': 'mobile'" />
					<aspire:AspireStringValidator runat="server" ControlToValidate="tbMobile" ValidationExpression="Mobile" InvalidDataErrorMessage="Invalid Mobile No." ValidationGroup="SaveStudentInfo" AllowNull="False" RequiredErrorMessage="This field is required." />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Residential Address:" AssociatedControlID="tbResidentialAddress" />
					<aspire:AspireTextBox runat="server" ID="tbResidentialAddress" ValidationGroup="SaveStudentInfo" MaxLength="100" Rows="3" TextMode="MultiLine" />
					<aspire:AspireStringValidator runat="server" ControlToValidate="tbResidentialAddress" ValidationGroup="SaveStudentInfo" AllowNull="True" RequiredErrorMessage="This field is required." />
				</div>
			</div>
			<div class="modal-footer">
				<aspire:AspireButton runat="server" ID="btnSaveStudentInfo" ButtonType="Primary" ValidationGroup="SaveStudentInfo" Text="Save" OnClick="btnSaveStudentInfo_OnClick" />
				<aspire:AspireButton runat="server" ID="btnCancelStudentInfo" Text="Cancel" ButtonType="Default" OnClick="btnCancelEdit_OnClick" />
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalFamilyInformation" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Family Information</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="No. of family members studying in Bahria University:" AssociatedControlID="tbSiblingsStudyingAtBU" />
					<div class="input-group">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button"><i class="glyphicon glyphicon-minus"></i></button>
						</span>
						<aspire:AspireTextBox runat="server" ID="tbSiblingsStudyingAtBU" ValidationGroup="SaveFamilyInformation" MaxLength="1" />
						<span class="input-group-btn">
							<button class="btn btn-default" type="button"><i class="glyphicon glyphicon-plus"></i></button>
						</span>
					</div>
					<aspire:AspireByteValidator runat="server" ControlToValidate="tbSiblingsStudyingAtBU" ValidationGroup="SaveFamilyInformation" AllowNull="False" MinValue="0" MaxValue="9" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid number." InvalidRangeErrorMessage="Invalid number of family members." />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Siblings (Brothers and Sisters) studying in other educational institutions:" AssociatedControlID="tbSiblingsStudyingAtOthers" />
					<div class="input-group">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button"><span class="glyphicon glyphicon-minus"></span></button>
						</span>
						<aspire:AspireTextBox runat="server" ID="tbSiblingsStudyingAtOthers" ValidationGroup="SaveFamilyInformation" MaxLength="1" />
						<span class="input-group-btn">
							<button class="btn btn-default" type="button"><span class="glyphicon glyphicon-plus"></span></button>
						</span>
					</div>
					<aspire:AspireByteValidator runat="server" ControlToValidate="tbSiblingsStudyingAtOthers" ValidationGroup="SaveFamilyInformation" AllowNull="False" MinValue="0" MaxValue="9" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid number." InvalidRangeErrorMessage="Invalid number of siblings." />
				</div>
			</div>
			<div class="modal-footer">
				<aspire:AspireButton runat="server" ID="btnSaveFamilyInformation" ValidationGroup="SaveFamilyInformation" ButtonType="Primary" Text="Save" OnClick="btnSaveFamilyInformation_OnClick" />
				<aspire:AspireButton runat="server" ID="btnCancelFamilyInformation" ButtonType="Default" Text="Cancel" OnClick="btnCancelEdit_OnClick" />
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalFamilyIncome" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Family Income</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Father Income <em>(if any)</em>:" AssociatedControlID="tbFatherIncome" />
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbFatherIncome" MaxLength="50" ValidationGroup="SaveFamilyIncome" />
								<span class="input-group-addon">Per Annum</span>
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbFatherIncome" AllowNull="True" ValidationGroup="SaveFamilyIncome" InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Mother Income <em>(if any)</em>:" AssociatedControlID="tbMotherIncome" />
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbMotherIncome" MaxLength="50" ValidationGroup="SaveFamilyIncome" />
								<span class="input-group-addon">Per Annum</span>
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbMotherIncome" AllowNull="True" ValidationGroup="SaveFamilyIncome" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Spouse Income <em>(if any)</em>:" AssociatedControlID="tbSpouseIncome" />
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbSpouseIncome" MaxLength="50" ValidationGroup="SaveFamilyIncome" />
								<span class="input-group-addon">Per Annum</span>
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbSpouseIncome" AllowNull="True" ValidationGroup="SaveFamilyIncome" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Self Income <em>(if any)</em>:" AssociatedControlID="tbSelfIncome" />
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbSelfIncome" MaxLength="50" ValidationGroup="SaveFamilyIncome" />
								<span class="input-group-addon">Per Annum</span>
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbSelfIncome" AllowNull="True" ValidationGroup="SaveFamilyIncome" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Sibling Income <em>(if any)</em>:" AssociatedControlID="tbSiblingIncome" />
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbSiblingIncome" MaxLength="50" ValidationGroup="SaveFamilyIncome" />
								<span class="input-group-addon">Per Annum</span>
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbSiblingIncome" AllowNull="True" ValidationGroup="SaveFamilyIncome" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Persons (including you) depend on your parents:" AssociatedControlID="tbPersonsDependingOnParentsIncludingYou" />
							<div class="input-group">
								<span class="input-group-btn">
									<button class="btn btn-default" type="button"><i class="glyphicon glyphicon-minus"></i></button>
								</span>
								<aspire:AspireTextBox runat="server" ID="tbPersonsDependingOnParentsIncludingYou" MaxLength="50" ValidationGroup="SaveFamilyIncome" />
								<span class="input-group-btn">
									<button class="btn btn-default" type="button"><i class="glyphicon glyphicon-plus"></i></button>
								</span>
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbPersonsDependingOnParentsIncludingYou" ValidationGroup="SaveFamilyIncome" AllowNull="False" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Valid Range is {min} to {max}" MinValue="0" MaxValue="50" />
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<aspire:AspireButton runat="server" ID="btnSaveFamilyIncome" ValidationGroup="SaveFamilyIncome" ButtonType="Primary" Text="Save" OnClick="btnSaveFamilyIncome_OnClick" />
				<aspire:AspireButton runat="server" ID="btnCancelFamilyIncome" ButtonType="Default" Text="Cancel" OnClick="btnCancelEdit_OnClick" />
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalFamilyExpenditures" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Family Expenditures</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Children Fee:" AssociatedControlID="tbChildrenFee" />
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbChildrenFee" MaxLength="10" ValidationGroup="SaveFamilyExpenditures" />
								<span class="input-group-addon">Per Annum</span>
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbChildrenFee" AllowNull="True" ValidationGroup="SaveFamilyExpenditures" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Electricity Bill:" AssociatedControlID="tbElectricityBill" />
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbElectricityBill" MaxLength="10" ValidationGroup="SaveFamilyExpenditures" />
								<span class="input-group-addon">Per Annum</span>
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbElectricityBill" AllowNull="True" ValidationGroup="SaveFamilyExpenditures" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Loan (Re-paid):" AssociatedControlID="tbLoan" />
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbLoan" MaxLength="10" ValidationGroup="SaveFamilyExpenditures" />
								<span class="input-group-addon">Per Annum</span>
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbLoan" AllowNull="True" ValidationGroup="SaveFamilyExpenditures" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="House Rent:" AssociatedControlID="tbHouseRent" />
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbHouseRent" MaxLength="10" ValidationGroup="SaveFamilyExpenditures" />
								<span class="input-group-addon">Per Annum</span>
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbHouseRent" AllowNull="True" ValidationGroup="SaveFamilyExpenditures" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Telephone Bill:" AssociatedControlID="tbTelephoneBill" />
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbTelephoneBill" MaxLength="10" ValidationGroup="SaveFamilyExpenditures" />
								<span class="input-group-addon">Per Annum</span>
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbTelephoneBill" AllowNull="True" ValidationGroup="SaveFamilyExpenditures" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Medical Expenses:" AssociatedControlID="tbMedicalExpenses" />
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbMedicalExpenses" MaxLength="10" ValidationGroup="SaveFamilyExpenditures" />
								<span class="input-group-addon">Per Annum</span>
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbMedicalExpenses" AllowNull="True" ValidationGroup="SaveFamilyExpenditures" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Taxes Paid:" AssociatedControlID="tbTaxesPaid" />
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbTaxesPaid" MaxLength="10" ValidationGroup="SaveFamilyExpenditures" />
								<span class="input-group-addon">Per Annum</span>
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbTaxesPaid" AllowNull="True" ValidationGroup="SaveFamilyExpenditures" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Others:" AssociatedControlID="tbOtherExpenses" />
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbOtherExpenses" MaxLength="10" ValidationGroup="SaveFamilyExpenditures" />
								<span class="input-group-addon">Per Annum</span>
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbOtherExpenses" AllowNull="True" ValidationGroup="SaveFamilyExpenditures" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Educational expenses (your family can pay):" AssociatedControlID="tbEducationalExpensesFamilyCanPAy" />
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbEducationalExpensesFamilyCanPay" MaxLength="10" ValidationGroup="SaveFamilyExpenditures" />
								<span class="input-group-addon">Per Annum</span>
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbEducationalExpensesFamilyCanPay" AllowNull="False" ValidationGroup="SaveFamilyExpenditures" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Your required educational expenses:" AssociatedControlID="tbReqEducationalExpenses" />
							<div class="input-group">
								<span class="input-group-addon">Rs.</span>
								<aspire:AspireTextBox runat="server" ID="tbReqEducationalExpenses" MaxLength="10" ValidationGroup="SaveFamilyExpenditures" />
								<span class="input-group-addon">Per Annum</span>
							</div>
							<aspire:AspireDoubleValidator runat="server" ControlToValidate="tbReqEducationalExpenses" AllowNull="False" ValidationGroup="SaveFamilyExpenditures" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." MinValue="0" MaxValue="999999999.99" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Additional Details:" AssociatedControlID="tbAdditionalDetails" />
							<aspire:AspireTextBox runat="server" ID="tbAdditionalDetails" MaxLength="500" ValidationGroup="SaveFamilyExpenditures" TextMode="MultiLine" />
							<aspire:AspireStringValidator runat="server" ControlToValidate="tbAdditionalDetails" AllowNull="True" ValidationGroup="SaveFamilyExpenditures" ErrorMessage="This field is required." />
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<aspire:AspireButton runat="server" ID="btnSaveFamilyExpenditures" ButtonType="Primary" ValidationGroup="SaveFamilyExpenditures" Text="Save" OnClick="btnSaveFamilyExpenditures_OnClick" />
				<aspire:AspireButton runat="server" ID="btnCancelFamilyExpenditures" ButtonType="Default" Text="Cancel" OnClick="btnCancelEdit_OnClick" />
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function () {
		var tb = $("input#<%=this.tbSiblingsStudyingAtBU.ClientID%>");
		function applySpinner(tb) {
			$("button", $(tb).prev()).off("click").click(function () {
				var value = $(tb).val().tryToInt();
				if (value !== null && value >= 1 && value <= 8)
					$(tb).val(--value);
				if (value === null)
					$(tb).val(1);
				$(tb).change();
			});
			$("button", $(tb).next()).off("click").click(function () {
				var value = $(tb).val().tryToInt();
				if (value !== null && value >= 0 && value <= 8)
					$(tb).val(++value);
				if (value === null)
					$(tb).val(1);
				$(tb).change();
			});
		}
		applySpinner(tb);
	});

	$(function () {
		var tb = $("input#<%=this.tbSiblingsStudyingAtOthers.ClientID%>");
		function applySpinner(tb) {
			$("button", $(tb).prev()).off("click").click(function () {
				var value = $(tb).val().tryToInt();
				if (value != null && value >= 1 && value <= 8)
					$(tb).val(--value);
				if (value == null)
					$(tb).val(1);
				$(tb).change();
			});
			$("button", $(tb).next()).off("click").click(function () {
				var value = $(tb).val().tryToInt();
				if (value != null && value >= 0 && value <= 8)
					$(tb).val(++value);
				if (value == null)
					$(tb).val(1);
				$(tb).change();
			});
		}
		applySpinner(tb);
	});

	$(function () {
		var tb = $("input#<%=this.tbPersonsDependingOnParentsIncludingYou.ClientID%>");
		function applySpinner(tb) {
			$("button", $(tb).prev()).off("click").click(function () {
				var value = $(tb).val().tryToInt();
				if (value != null && value >= 1 && value <= 8)
					$(tb).val(--value);
				if (value == null)
					$(tb).val(1);
				$(tb).change();
			});
			$("button", $(tb).next()).off("click").click(function () {
				var value = $(tb).val().tryToInt();
				if (value != null && value >= 0 && value <= 8)
					$(tb).val(++value);
				if (value == null)
					$(tb).val(1);
				$(tb).change();
			});
		}
		applySpinner(tb);
	});

	$(document).ready(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>
