﻿using Aspire.BL.Core.Scholarship.Common.QarzeHasana;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Globalization;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Common.Scholarships.QarzeHasana
{
	public partial class QarzeHasanaForm : System.Web.UI.UserControl
	{
		private IAlert Alert => this.Page as IAlert;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
			}
		}

		private int ScholarshipAnnouncementID
		{
			get => (int)this.ViewState[nameof(this.ScholarshipAnnouncementID)];
			set => this.ViewState[nameof(this.ScholarshipAnnouncementID)] = value;
		}
		private int StudentID
		{
			get => (int)this.ViewState[nameof(this.StudentID)];
			set => this.ViewState[nameof(this.StudentID)] = value;
		}
		private int? ScholarshipApplicationID
		{
			get => (int?)this.ViewState[nameof(this.ScholarshipApplicationID)];
			set => this.ViewState[nameof(this.ScholarshipApplicationID)] = value;
		}
		private string Enrollment
		{
			get => (string)this.ViewState[nameof(this.Enrollment)];
			set => this.ViewState[nameof(this.Enrollment)] = value;
		}
		private short? SemesterID
		{
			get => (short)this.ViewState[nameof(this.SemesterID)];
			set => this.ViewState[nameof(this.SemesterID)] = value;
		}
		public Model.Entities.ScholarshipApplication.Statuses? ScholarshipApplicationStatusEnum { get; set; }
		public string ScholarshipApplicationStatusFullName => this.ScholarshipApplicationStatusEnum.ToFullName();
		public Model.Entities.ScholarshipApplication.DocumentsStatuses? DocumentsStatusEnum { get; internal set; }
		public string DocumentsStatusFullName => this.DocumentsStatusEnum.ToFullName();
		private bool ShowOrHideButton { get; set; } = true;
		private string SetAsNA { get; } = string.Empty.ToNAIfNullOrEmpty();
		private string ValueInRs { get; } = "Rs. ";

		internal void LoadData(ScholarshipApplication.GetApplicationInfoResult applicationInfo, int scholarshipAnnouncementID, int studentID)
		{
			if (applicationInfo == null)
				throw new ArgumentNullException(nameof(applicationInfo));

			this.divPrint.Visible = applicationInfo.ScholarshipApplication?.StatusEnum == Model.Entities.ScholarshipApplication.Statuses.Submitted;
			if (this.divPrint.Visible)
				switch (AspireIdentity.Current.UserType)
				{
					case UserTypes.Staff:
						this.hlPrintReport.NavigateUrl = Staff.Scholarship.Reports.ApplicationForm.GetPageUrl(applicationInfo.ScholarshipApplication?.ScholarshipApplicationID ?? throw new InvalidOperationException());
						break;
					case UserTypes.Student:
						this.hlPrintReport.NavigateUrl = Student.Scholarships.Report.QarzeHasana.GetPageUrl(applicationInfo.ScholarshipApplication?.ScholarshipApplicationID ?? throw new InvalidOperationException());
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

			this.ScholarshipAnnouncementID = scholarshipAnnouncementID;
			this.ScholarshipApplicationID = applicationInfo.ScholarshipApplication?.ScholarshipApplicationID;
			this.StudentID = studentID;
			this.SemesterID = applicationInfo.AnnouncementSemesterID;
			this.lblScholarshipApplicationStatus.Text = this.ScholarshipApplicationStatusFullName;
			this.Enrollment = applicationInfo.Enrollment;
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					this.lblDocuments.Text = this.DocumentsStatusFullName;
					break;
				case UserTypes.Student:
					this.lblDocumentsStatus.Visible = false;
					this.lblDocuments.Visible = false;
					break;
			}

			this.LoadStudentInfo(applicationInfo);
			if (applicationInfo.ScholarshipApplication != null)
			{
				this.ScholarshipApplicationStatusEnum = (Model.Entities.ScholarshipApplication.Statuses?)applicationInfo.ScholarshipApplication.Status;
				this.lblScholarshipApplicationStatus.Text = this.ScholarshipApplicationStatusFullName;
				switch (AspireIdentity.Current.UserType)
				{
					case UserTypes.Staff:
						this.DocumentsStatusEnum = (Model.Entities.ScholarshipApplication.DocumentsStatuses?)applicationInfo.ScholarshipApplication.DocumentsStatus;
						this.lblDocuments.Text = this.DocumentsStatusFullName;
						break;
				}

				var form = BL.Core.Scholarship.Common.Schemas.QarzeHasana.FromJson(applicationInfo.ScholarshipApplication.FormData);
				if (form == null)
					throw new InvalidOperationException();

				this.tbPhone.Text = form.Phone;
				this.tbMobile.Text = form.Mobile;
				this.tbResidentialAddress.Text = form.ResidentialAddress;
				this.lblPhone.Text = form.PhoneWithNullCheck;
				this.lblMobile.Text = form.Mobile.ToNAIfNullOrEmpty();
				this.lblResidentialAddress.Text = form.ResidentialAddress.ToNAIfNullOrEmpty();

				this.LoadFamilyInformation(form);
				this.LoadFamilyIncome(form);
				this.LoadFamilyExpenditures(form);
				this.gvFamilyMember.DataBind(form.FamilyMembers);
				this.gvFamilyAsset.DataBind(form.FamilyAssets);
				this.gvSaving.DataBind(form.Savings);
				this.gvVehicle.DataBind(form.Vehicles);
				this.gvOtherIncome.DataBind(form.OtherIncomes);
				this.SetButtonsVisibility();
			}
			else
			{
				this.btnSubmit.Visible = false;
				this.hlPrintReport.Visible = false;
				this.LoadFamilyInformation();
				this.LoadFamilyIncome();
				this.LoadFamilyExpenditures();
				this.gvFamilyMember.DataBindNull();
				this.gvFamilyAsset.DataBindNull();
				this.gvSaving.DataBindNull();
				this.gvVehicle.DataBindNull();
				this.gvOtherIncome.DataBindNull();
			}
		}

		private void SetButtonsVisibility()
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					this.ButtonState(this.ShowOrHideButton);
					break;
				case UserTypes.Student:
					if (this.ScholarshipApplicationStatusEnum == Model.Entities.ScholarshipApplication.Statuses.Submitted)
						this.ShowOrHideButton = false;
					this.ButtonState(this.ShowOrHideButton);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			this.hlPrintReport.Visible = this.ScholarshipApplicationStatusEnum == Model.Entities.ScholarshipApplication.Statuses.Submitted;
		}

		private void ButtonState(bool buttonState)
		{
			this.btnEditStudentInfo.Visible = buttonState;
			this.btnAddFamilyMember.Visible = buttonState;
			this.btnEditFamilyInformation.Visible = buttonState;
			this.btnAddFamilyAsset.Visible = buttonState;
			this.btnAddVehicle.Visible = buttonState;
			this.btnAddSaving.Visible = buttonState;
			this.btnEditFamilyIncome.Visible = buttonState;
			this.btnAddOtherIncome.Visible = buttonState;
			this.btnEditFamilyExpenditures.Visible = buttonState;
			this.btnSubmit.Visible = buttonState;
			this.gvFamilyMember.Columns[4].Visible = buttonState;
			this.gvFamilyAsset.Columns[6].Visible = buttonState;
			this.gvVehicle.Columns[6].Visible = buttonState;
			this.gvSaving.Columns[4].Visible = buttonState;
			this.gvOtherIncome.Columns[4].Visible = buttonState;
		}

		private void LoadStudentInfo(ScholarshipApplication.GetApplicationInfoResult applicationInfo)
		{
			this.lblStudentName.Text = applicationInfo.Name;
			this.lblFatherName.Text = applicationInfo.FatherName;
			this.lblDOB.Text = applicationInfo.DOB.ToString("d");
			this.lblGender.Text = applicationInfo.GenderFullName;
			this.lblEnrollment.Text = applicationInfo.Enrollment;
			this.lblRegistration.Text = applicationInfo.RegistrationNo.ToString().ToNAIfNullOrEmpty();
			this.lblProgram.Text = applicationInfo.ProgramAlias;
			this.lblSemester.Text = (applicationInfo.SemesterNo?.ToFullName()).ToNAIfNullOrEmpty();
			this.lblSection.Text = (applicationInfo.Section?.ToFullName()).ToNAIfNullOrEmpty();
			this.lblEmail.Text = applicationInfo.Email;
			this.tbPhone.Text = applicationInfo.Phone;
			this.tbMobile.Text = applicationInfo.Mobile;
			this.tbResidentialAddress.Text = applicationInfo.CurrentAddress;
			this.lblPhone.Text = applicationInfo.Phone.ToNAIfNullOrEmpty();
			this.lblMobile.Text = applicationInfo.Mobile.ToNAIfNullOrEmpty();
			this.lblResidentialAddress.Text = applicationInfo.CurrentAddress.ToNAIfNullOrEmpty();
			this.lblDomicile.Text = applicationInfo.Domicile;
			this.lblGPA.Text = applicationInfo.LastResult;
		}

		private void LoadFamilyInformation()
		{
			this.lblSiblingsStudyingAtBU.Text = this.SetAsNA;
			this.lblSiblingsStudyingAtOthers.Text = this.SetAsNA;
		}

		private void LoadFamilyInformation(BL.Core.Scholarship.Common.Schemas.QarzeHasana form)
		{
			this.tbSiblingsStudyingAtBU.Text = form.NoOfFamilyMembersStudyingAtBU.ToString();
			this.tbSiblingsStudyingAtOthers.Text = form.SiblingsStudyingAtOthers.ToString();

			this.lblSiblingsStudyingAtBU.Text = form.NoOfFamilyMembersStudyingAtBU.ToString().ToNAIfNullOrEmpty();
			this.lblSiblingsStudyingAtOthers.Text = form.SiblingsStudyingAtOthers.ToString().ToNAIfNullOrEmpty();
		}

		private void LoadFamilyIncome()
		{
			this.lblFatherIncome.Text = this.SetAsNA;
			this.lblMotherIncome.Text = this.SetAsNA;
			this.lblSpouseIncome.Text = this.SetAsNA;
			this.lblSelfIncome.Text = this.SetAsNA;
			this.lblSiblingIncome.Text = this.SetAsNA;
			this.lblPersonsDependingOnParentsIncludingYou.Text = this.SetAsNA;
		}

		private void LoadFamilyIncome(BL.Core.Scholarship.Common.Schemas.QarzeHasana form)
		{
			this.tbFatherIncome.Text = form.FatherIncome.ToString();
			this.tbMotherIncome.Text = form.MotherIncome.ToString();
			this.tbSpouseIncome.Text = form.SpouseIncome.ToString();
			this.tbSelfIncome.Text = form.SelfIncome.ToString();
			this.tbSiblingIncome.Text = form.SiblingIncome.ToString();
			this.tbPersonsDependingOnParentsIncludingYou.Text = form.PersonsDependingOnParentsIncludingYou.ToString();

			this.lblFatherIncome.Text = form.FatherIncome == null ? this.SetAsNA : this.ValueInRs + form.FatherIncome;
			this.lblMotherIncome.Text = form.MotherIncome == null ? this.SetAsNA : this.ValueInRs + form.MotherIncome;
			this.lblSpouseIncome.Text = form.SpouseIncome == null ? this.SetAsNA : this.ValueInRs + form.SpouseIncome;
			this.lblSelfIncome.Text = form.SelfIncome == null ? this.SetAsNA : this.ValueInRs + form.SelfIncome;
			this.lblSiblingIncome.Text = form.SiblingIncome == null ? this.SetAsNA : this.ValueInRs + form.SiblingIncome;
			this.lblPersonsDependingOnParentsIncludingYou.Text = form.PersonsDependingOnParentsIncludingYou.ToString().ToNAIfNullOrEmpty();
		}

		private void LoadFamilyExpenditures()
		{
			this.lblChildrenFee.Text = this.SetAsNA;
			this.lblElectricityBill.Text = this.SetAsNA;
			this.lblLoan.Text = this.SetAsNA;
			this.lblHouseRent.Text = this.SetAsNA;
			this.lblTelephoneBill.Text = this.SetAsNA;
			this.lblMedicalExpenses.Text = this.SetAsNA;
			this.lblTaxesPaid.Text = this.SetAsNA;
			this.lblOtherExpenses.Text = this.SetAsNA;
			this.lblEducationalExpensesFamilyCanPay.Text = this.SetAsNA;
			this.lblReqEducationalExpenses.Text = this.SetAsNA;
			this.lblAdditionalDetails.Text = this.SetAsNA;
		}

		private void LoadFamilyExpenditures(BL.Core.Scholarship.Common.Schemas.QarzeHasana formData)
		{
			this.tbChildrenFee.Text = formData.ChildrenFee.ToString();
			this.tbElectricityBill.Text = formData.ElectricityBill.ToString();
			this.tbLoan.Text = formData.Loan.ToString();
			this.tbHouseRent.Text = formData.HouseRent.ToString();
			this.tbTelephoneBill.Text = formData.TelephoneBill.ToString();
			this.tbMedicalExpenses.Text = formData.MedicalExpenses.ToString();
			this.tbTaxesPaid.Text = formData.TaxesPaid.ToString();
			this.tbOtherExpenses.Text = formData.OtherExpenses.ToString();
			this.tbEducationalExpensesFamilyCanPay.Text = formData.EducationalExpensesFamilyCanPay.ToString();
			this.tbReqEducationalExpenses.Text = formData.RequiredEducationalExpenses.ToString();
			this.tbAdditionalDetails.Text = formData.AdditionalDetails;

			this.lblChildrenFee.Text = this.ValueInRs + formData.ChildrenFee.ToString().ToNAIfNullOrEmpty();
			this.lblElectricityBill.Text = this.ValueInRs + formData.ElectricityBill.ToString().ToNAIfNullOrEmpty();
			this.lblLoan.Text = formData.Loan == null ? this.SetAsNA : this.ValueInRs + formData.Loan;
			this.lblHouseRent.Text = formData.HouseRent == null ? this.SetAsNA : this.ValueInRs + formData.HouseRent;
			this.lblTelephoneBill.Text = this.ValueInRs + formData.TelephoneBill.ToString().ToNAIfNullOrEmpty();
			this.lblMedicalExpenses.Text = this.ValueInRs + formData.MedicalExpenses.ToString().ToNAIfNullOrEmpty();
			this.lblTaxesPaid.Text = formData.TaxesPaid == null ? this.SetAsNA : this.ValueInRs + formData.TaxesPaid;
			this.lblOtherExpenses.Text = formData.OtherExpenses == null ? this.SetAsNA : this.ValueInRs + formData.OtherExpenses;
			this.lblEducationalExpensesFamilyCanPay.Text = this.ValueInRs + formData.EducationalExpensesFamilyCanPay.ToString().ToNAIfNullOrEmpty();
			this.lblReqEducationalExpenses.Text = this.ValueInRs + formData.RequiredEducationalExpenses.ToString().ToNAIfNullOrEmpty();
			this.lblAdditionalDetails.Text = formData.AdditionalDetails.ToNAIfNullOrEmpty();
		}

		private void RedirectToHome()
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					Staff.Scholarship.ApplicationsStatus.Redirect(this.SemesterID, this.Enrollment, true);
					return;
				case UserTypes.Student:
					BasePage.Redirect<Student.Scholarships.AvailableScholarships>();
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(AspireIdentity.Current.UserType), AspireIdentity.Current.UserType, null);
			}
		}

		private void RefreshPage()
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					Staff.Scholarship.QarzeHasana.ApplicationForm.Redirect(this.ScholarshipAnnouncementID, this.StudentID);
					return;
				case UserTypes.Student:
					Student.Scholarships.QarzeHasanaForm.Redirect(this.ScholarshipAnnouncementID);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(AspireIdentity.Current.UserType), AspireIdentity.Current.UserType, null);
			}
		}

		private void AddScholarshipApplication()
		{
			if (this.ScholarshipApplicationID != null)
				return;
			var result = ScholarshipApplication.AddScholarshipApplication(this.StudentID, this.ScholarshipAnnouncementID, this.tbPhone.Text, this.SemesterID, AspireIdentity.Current.LoginSessionGuid);
			switch (result.Status)
			{
				case ScholarshipApplication.AddScholarshipApplicationResult.Statuses.NoRecordFound:
					this.Alert.AddNoRecordFoundAlert();
					this.RedirectToHome();
					return;
				case ScholarshipApplication.AddScholarshipApplicationResult.Statuses.ScholarshipsNotOpened:
					this.Alert.AddErrorAlert("Scholarships are closed or not yet opened.");
					this.RedirectToHome();
					return;
				case ScholarshipApplication.AddScholarshipApplicationResult.Statuses.AlreadyApplied:
					this.Alert.AddErrorAlert("You have already applied for scholarship.");
					this.RedirectToHome();
					return;
				case ScholarshipApplication.AddScholarshipApplicationResult.Statuses.MaxSemesterExceeded:
					this.Alert.AddErrorAlert("You are not eligible for scholarship. Maximum semesters allowed for scholarship are completed.");
					this.RedirectToHome();
					return;
				case ScholarshipApplication.AddScholarshipApplicationResult.Statuses.Success:
					this.ScholarshipApplicationID = result.ScholarshipApplication.ScholarshipApplicationID;
					this.ScholarshipAnnouncementID = result.ScholarshipApplication.ScholarshipAnnouncementID;
					this.StudentID = result.ScholarshipApplication.StudentID;
					return;
				default:
					throw new ArgumentOutOfRangeException(nameof(result.Status), result.Status, null);
			}
		}

		private void DisplayMessage(ScholarshipApplication.UpdateScholarshipApplicationStatuses status)
		{
			switch (status)
			{
				case ScholarshipApplication.UpdateScholarshipApplicationStatuses.NoRecordFound:
					this.Alert.AddNoRecordFoundAlert();
					this.RedirectToHome();
					return;
				case ScholarshipApplication.UpdateScholarshipApplicationStatuses.AlreadySubmitted:
					this.Alert.AddErrorAlert("You have already submitted this application.");
					this.RedirectToHome();
					return;
				case ScholarshipApplication.UpdateScholarshipApplicationStatuses.NotAuthorizedForChange:
					this.Alert.AddErrorAlert("You are not authorized to Add/Edit application.");
					this.RedirectToHome();
					return;
				case ScholarshipApplication.UpdateScholarshipApplicationStatuses.ScholarshipsNotOpened:
					this.Alert.AddErrorAlert("Scholarship end date is over.");
					this.RedirectToHome();
					return;
				case ScholarshipApplication.UpdateScholarshipApplicationStatuses.Success:
					this.Alert.AddSuccessAlert("Student Information has been saved.");
					this.RefreshPage();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		#region Events to edit Model Popup

		protected void btnSaveStudentInfo_OnClick(object sender, EventArgs e)
		{
			this.AddScholarshipApplication();
			var result = ScholarshipApplication.UpdateStudentInfo(this.ScholarshipApplicationID ?? throw new InvalidOperationException(), this.StudentID, this.tbPhone.Text, this.tbMobile.Text, this.tbResidentialAddress.Text, AspireIdentity.Current.LoginSessionGuid);
			this.DisplayMessage(result);
		}

		protected void btnSaveFamilyInformation_OnClick(object sender, EventArgs e)
		{
			this.AddScholarshipApplication();
			var result = ScholarshipApplication.UpdateFamilyInformation(this.ScholarshipApplicationID ?? throw new InvalidOperationException(), this.tbSiblingsStudyingAtBU.Text.ToByte(), this.tbSiblingsStudyingAtOthers.Text.ToByte(), AspireIdentity.Current.LoginSessionGuid);
			this.DisplayMessage(result);
		}

		protected void btnSaveFamilyIncome_OnClick(object sender, EventArgs e)
		{
			this.AddScholarshipApplication();
			var result = ScholarshipApplication.UpdateFamilyIncome(this.ScholarshipApplicationID ?? throw new InvalidOperationException(), this.tbFatherIncome.Text.ToNullableDouble(), this.tbMotherIncome.Text.ToNullableDouble(), this.tbSpouseIncome.Text.ToNullableDouble(), this.tbSelfIncome.Text.ToNullableDouble(), this.tbSiblingIncome.Text.ToNullableDouble(), this.tbPersonsDependingOnParentsIncludingYou.Text.ToByte(), AspireIdentity.Current.LoginSessionGuid);
			this.DisplayMessage(result);
		}

		protected void btnSaveFamilyExpenditures_OnClick(object sender, EventArgs e)
		{
			this.AddScholarshipApplication();
			var result = ScholarshipApplication.UpdateFamilyExpenditures(this.ScholarshipApplicationID ?? throw new InvalidOperationException(), this.tbChildrenFee.Text.ToNullableDouble(), this.tbElectricityBill.Text.ToNullableDouble(), this.tbLoan.Text.ToNullableDouble(), this.tbHouseRent.Text.ToNullableDouble(), this.tbTelephoneBill.Text.ToNullableDouble(), this.tbMedicalExpenses.Text.ToNullableDouble(), this.tbTaxesPaid.Text.ToNullableDouble(), this.tbOtherExpenses.Text.ToNullableDouble(), this.tbEducationalExpensesFamilyCanPay.Text.ToDouble(), this.tbReqEducationalExpenses.Text.ToDouble(), this.tbAdditionalDetails.Text, AspireIdentity.Current.LoginSessionGuid);
			this.DisplayMessage(result);
		}

		#endregion

		#region Family Members
		protected void btnAddFamilyMember_OnClick(object sender, EventArgs e)
		{
			this.DisplayFamilyMemberModal(null);
		}

		private int? FamilyMemberID
		{
			get => (int?)this.ViewState[nameof(this.FamilyMemberID)];
			set => this.ViewState[nameof(this.FamilyMemberID)] = value;
		}

		private BL.Core.Scholarship.Common.Schemas.QarzeHasana GetForm()
		{
			var applicationInfo = ScholarshipApplication.GetApplicationInfo(this.ScholarshipAnnouncementID, this.StudentID, AspireIdentity.Current.LoginSessionGuid);
			if (applicationInfo == null)
			{
				this.Alert.AddNoRecordFoundAlert();
				this.RedirectToHome();
				return null;
			}
			var form = BL.Core.Scholarship.Common.Schemas.QarzeHasana.FromJson(applicationInfo.ScholarshipApplication.FormData);
			if (form == null)
				throw new InvalidOperationException();
			return form;
		}

		private void DisplayFamilyMemberModal(int? familyMemberID)
		{
			if (familyMemberID == null)
			{
				this.tbRelationship.Text = string.Empty;
				this.tbOccupation.Text = string.Empty;
				this.tbAge.Text = string.Empty;

				this.modalFamilyMember.HeaderText = "Add Family Member";
				this.FamilyMemberID = null;
			}
			else
			{
				var form = this.GetForm();
				var familyMember = form.GetFamilyMember(familyMemberID.Value);
				if (familyMember == null)
				{
					this.Alert.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				}
				this.tbRelationship.Text = familyMember.Relation;
				this.tbOccupation.Text = familyMember.Occupation;
				this.tbAge.Text = familyMember.Age.ToString(CultureInfo.CurrentCulture);
				this.modalFamilyMember.HeaderText = "Edit Family Member";
				this.FamilyMemberID = familyMember.FamilyMemberID;
			}
			this.modalFamilyMember.Show();
		}

		protected void btnSaveFamilyMember_OnClick(object sender, EventArgs e)
		{
			this.AddScholarshipApplication();
			var result = ScholarshipApplication.AddOrUpdateFamilyMember(this.ScholarshipApplicationID ?? throw new InvalidOperationException(), this.FamilyMemberID, this.tbAge.Text.ToDouble(), this.tbOccupation.Text, this.tbRelationship.Text, AspireIdentity.Current.LoginSessionGuid);
			this.DisplayMessage(result);
		}

		protected void gvFamilyMember_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayFamilyMemberModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var result = ScholarshipApplication.DeleteFamilyMember(this.ScholarshipApplicationID ?? throw new InvalidOperationException(), e.DecryptedCommandArgumentToInt(), AspireIdentity.Current.LoginSessionGuid);
					this.DisplayMessage(result);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		#endregion Family Members

		#region Family Assets

		protected void btnAddFamilyAsset_OnClick(object sender, EventArgs e)
		{
			this.DisplayFamilyAssetModal(null);
		}

		private int? FamilyAssetID
		{
			get => (int?)this.ViewState[nameof(this.FamilyAssetID)];
			set => this.ViewState[nameof(this.FamilyAssetID)] = value;
		}

		public enum AssetTypes : byte
		{
			House = 1,
			AgriculturalLand = 2,
			Plot = 3
		}

		private void DisplayFamilyAssetModal(int? familyAssetID)
		{
			this.ddlAssetType.FillEnums<AssetTypes>();

			if (familyAssetID == null)
			{
				this.ddlAssetType.ClearSelection();
				this.tbOwner.Text = string.Empty;
				this.tbLocality.Text = string.Empty;
				this.tbArea.Text = string.Empty;
				this.tbValue.Text = string.Empty;
				this.modalFamilyAsset.HeaderText = "Add Family Asset";
				this.FamilyAssetID = null;
			}
			else
			{
				var form = this.GetForm();
				var familyAsset = form.GetFamilyAsset(familyAssetID.Value);
				if (familyAsset == null)
				{
					this.Alert.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				}

				this.ddlAssetType.SelectedValue = familyAsset.AssetType;
				this.tbOwner.Text = familyAsset.OwnedBy;
				this.tbLocality.Text = familyAsset.Locality;
				this.tbArea.Text = familyAsset.Area;
				this.tbValue.Text = familyAsset.PriceValue.ToString();
				this.modalFamilyAsset.HeaderText = "Edit Family Asset";
				this.FamilyAssetID = familyAsset.FamilyAssetID;
			}
			this.modalFamilyAsset.Show();
		}

		protected void gvFamilyAsset_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayFamilyAssetModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var result = ScholarshipApplication.DeleteFamilyAsset(this.ScholarshipApplicationID ?? throw new InvalidOperationException(), e.DecryptedCommandArgumentToInt(), AspireIdentity.Current.LoginSessionGuid);
					this.DisplayMessage(result);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void btnSaveFamilyAsset_OnClick(object sender, EventArgs e)
		{
			this.AddScholarshipApplication();
			var result = ScholarshipApplication.AddOrUpdateFamilyAsset(this.ScholarshipApplicationID ?? throw new InvalidOperationException(), this.FamilyAssetID, this.ddlAssetType.SelectedValue, this.tbOwner.Text, this.tbLocality.Text, this.tbArea.Text, this.tbValue.Text.TryToDouble(), AspireIdentity.Current.LoginSessionGuid);
			this.DisplayMessage(result);
		}

		#endregion Family Assets

		#region Vehicles
		protected void btnAddVehicle_OnClick(object sender, EventArgs e)
		{
			this.DisplayVehicleModal(null);
		}

		private int? VehicleID
		{
			get => (int?)this.ViewState[nameof(this.VehicleID)];
			set => this.ViewState[nameof(this.VehicleID)] = value;
		}

		private void DisplayVehicleModal(int? vehicleID)
		{
			if (vehicleID == null)
			{
				this.tbOwnerVehicle.Text = string.Empty;
				this.tbMakeVehicle.Text = string.Empty;
				this.tbModel.Text = string.Empty;
				this.tbCC.Text = string.Empty;
				this.tbValueVehicle.Text = string.Empty;
				this.modalVehicle.HeaderText = "Add Vehicle";
				this.VehicleID = null;
			}
			else
			{
				var form = this.GetForm();
				var vehicle = form.GetVehicle(vehicleID.Value);
				if (vehicle == null)
				{
					this.Alert.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				}
				this.tbOwnerVehicle.Text = vehicle.OwnedBy;
				this.tbMakeVehicle.Text = vehicle.Make;
				this.tbModel.Text = vehicle.Model;
				this.tbCC.Text = vehicle.CC;
				this.tbValueVehicle.Text = vehicle.PriceValue.ToString();
				this.modalVehicle.HeaderText = "Edit Vehicle";
				this.VehicleID = vehicle.VehicleID;
			}
			this.modalVehicle.Show();
		}

		protected void gvVehicle_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayVehicleModal(e.DecryptedCommandArgumentToInt());
					break;

				case "Delete":
					e.Handled = true;
					var result = ScholarshipApplication.DeleteVehicle(this.ScholarshipApplicationID ?? throw new InvalidOperationException(), e.DecryptedCommandArgumentToInt(), AspireIdentity.Current.LoginSessionGuid);
					this.DisplayMessage(result);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void btnSaveVehicle_OnClick(object sender, EventArgs e)
		{
			this.AddScholarshipApplication();
			var result = ScholarshipApplication.AddOrUpdateVehicle(this.ScholarshipApplicationID ?? throw new InvalidOperationException(), this.VehicleID, this.tbMakeVehicle.Text, this.tbModel.Text, this.tbCC.Text, this.tbOwnerVehicle.Text, this.tbValueVehicle.Text.TryToDouble(), AspireIdentity.Current.LoginSessionGuid);
			this.DisplayMessage(result);
		}

		#endregion Vehicles

		#region Savings
		protected void btnAddSaving_OnClick(object sender, EventArgs e)
		{
			this.DisplaySavingModal(null);
		}

		private int? SavingID
		{
			get => (int?)this.ViewState[nameof(this.SavingID)];
			set => this.ViewState[nameof(this.SavingID)] = value;
		}

		private void DisplaySavingModal(int? savingID)
		{
			if (savingID == null)
			{
				this.tbOwnerSaving.Text = string.Empty;
				this.tbType.Text = string.Empty;
				this.tbValueSaving.Text = string.Empty;
				this.modalSaving.HeaderText = "Add Savings/Investments";
				this.SavingID = null;
			}
			else
			{
				var form = this.GetForm();
				var saving = form.GetSaving(savingID.Value);
				if (saving == null)
				{
					this.Alert.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				}
				this.tbOwnerSaving.Text = saving.OwnedBy;
				this.tbType.Text = saving.Type;
				this.tbValueSaving.Text = saving.PriceValue.ToString();
				this.modalSaving.HeaderText = "Edit Savings/Investments";
				this.SavingID = saving.SavingID;
			}
			this.modalSaving.Show();
		}

		protected void gvSaving_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplaySavingModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var result = ScholarshipApplication.DeleteSaving(this.ScholarshipApplicationID ?? throw new InvalidOperationException(), e.DecryptedCommandArgumentToInt(), AspireIdentity.Current.LoginSessionGuid);
					this.DisplayMessage(result);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void btnSaveSaving_OnClick(object sender, EventArgs e)
		{
			this.AddScholarshipApplication();
			var result = ScholarshipApplication.AddOrUpdateSaving(this.ScholarshipApplicationID ?? throw new InvalidOperationException(), this.SavingID, this.tbOwnerSaving.Text, this.tbType.Text, this.tbValueSaving.Text.ToDouble(), AspireIdentity.Current.LoginSessionGuid);
			this.DisplayMessage(result);
		}

		#endregion Savings

		#region Other Incomes
		protected void btnAddOtherIncome_OnClick(object sender, EventArgs e)
		{
			this.DisplayOtherIncomeModal(null);
		}

		private int? OtherIncomeID
		{
			get => (int?)this.ViewState[nameof(this.OtherIncomeID)];
			set => this.ViewState[nameof(this.OtherIncomeID)] = value;
		}

		private void DisplayOtherIncomeModal(int? otherIncomeID)
		{
			if (otherIncomeID == null)
			{
				this.tbIncomeType.Text = string.Empty;
				this.tbValueOtherIncome.Text = string.Empty;
				this.tbIncomeExplanation.Text = string.Empty;
				this.modalOtherIncome.HeaderText = "Add Other Incomes";
				this.OtherIncomeID = null;
			}
			else
			{
				var form = this.GetForm();
				var otherIncome = form.GetOtherIncome(otherIncomeID.Value);
				if (otherIncome == null)
				{
					this.Alert.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				}
				this.tbIncomeType.Text = otherIncome.IncomeType;
				this.tbValueOtherIncome.Text = otherIncome.PriceValue.ToString();
				this.tbIncomeExplanation.Text = otherIncome.Description;
				this.modalOtherIncome.HeaderText = "Edit Other Incomes";
				this.OtherIncomeID = otherIncome.OtherIncomeID;
			}
			this.modalOtherIncome.Show();
		}

		protected void gvOtherIncome_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayOtherIncomeModal(e.DecryptedCommandArgumentToInt());
					break;

				case "Delete":
					e.Handled = true;
					var result = ScholarshipApplication.DeleteOtherIncome(this.ScholarshipApplicationID ?? throw new InvalidOperationException(), e.DecryptedCommandArgumentToInt(), AspireIdentity.Current.LoginSessionGuid);
					this.DisplayMessage(result);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void btnSaveOtherIncome_OnClick(object sender, EventArgs e)
		{
			this.AddScholarshipApplication();
			var result = ScholarshipApplication.AddOrUpdateOtherIncome(this.ScholarshipApplicationID ?? throw new InvalidOperationException(), this.OtherIncomeID, this.tbIncomeType.Text, this.tbValueOtherIncome.Text.ToDouble(), this.tbIncomeExplanation.Text, AspireIdentity.Current.LoginSessionGuid);
			this.DisplayMessage(result);
		}

		#endregion Other Incomes

		protected void btnSubmit_OnClick(object sender, EventArgs e)
		{
			var result = ScholarshipApplication.SubmitApplication(this.ScholarshipApplicationID ?? throw new InvalidOperationException(), AspireIdentity.Current.LoginSessionGuid);
			if (result.Errors != null)
			{
				foreach (var error in result.Errors)
					switch (error)
					{
						case ScholarshipApplication.SubmitApplicationResult.Error.NotAuthorizedForChange:
							this.Alert.AddErrorMessageYouAreNotAuthorizedToPerformThisAction();
							return;
						case ScholarshipApplication.SubmitApplicationResult.Error.NoRecordFound:
							this.Alert.AddNoRecordFoundAlert();
							break;
						case ScholarshipApplication.SubmitApplicationResult.Error.AlreadySubmitted:
							this.Alert.AddErrorAlert("Application is already submitted.");
							break;
						case ScholarshipApplication.SubmitApplicationResult.Error.StudentInformationIsMissing:
							this.Alert.AddErrorAlert("Student Information is missing.");
							break;
						case ScholarshipApplication.SubmitApplicationResult.Error.FamilyMembersAreMissing:
							this.Alert.AddErrorAlert("Family member is missing");
							break;
						case ScholarshipApplication.SubmitApplicationResult.Error.FamilyInformationIsMissing:
							this.Alert.AddErrorAlert("Family Information is missing.");
							break;
						case ScholarshipApplication.SubmitApplicationResult.Error.FamilyIncomeIsMissing:
							this.Alert.AddErrorAlert("Family Income is missing.");
							break;
						case ScholarshipApplication.SubmitApplicationResult.Error.FamilyExpendituresAreMissing:
							this.Alert.AddErrorAlert("Family Expenditure is missing.");
							break;
						case ScholarshipApplication.SubmitApplicationResult.Error.ScholarshipsNotOpened:
							this.Alert.AddErrorAlert("Scholarship end date is over.");
							this.RedirectToHome();
							break;
						case ScholarshipApplication.SubmitApplicationResult.Error.Success:
							throw new InvalidOperationException();
						default:
							throw new ArgumentOutOfRangeException();
					}
			}
			else
			{
				this.Alert.AddSuccessAlert("Scholarship application has been submitted.");
				this.RefreshPage();
			}
		}

		protected void btnCancelEdit_OnClick(object sender, EventArgs e)
		{
			this.RefreshPage();
		}

		protected void btnCancelForm_OnClick(object sender, EventArgs e)
		{
			this.RedirectToHome();
		}
	}
}