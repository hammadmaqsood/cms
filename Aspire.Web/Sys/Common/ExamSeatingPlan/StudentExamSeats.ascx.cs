﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Common.ExamSeatingPlan
{
	public partial class StudentExamSeats : System.Web.UI.UserControl
	{
		private new AspireBasePage Page => (AspireBasePage)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
		}

		public void DisplayData(short semesterID, ExamConduct.ExamTypes examTypeEnum, string enrollment)
		{
			var seats = BL.Core.ExamSeatingPlan.Common.ExamSeats.GetStudentSlips(enrollment, semesterID, examTypeEnum, false, AspireIdentity.Current.LoginSessionGuid);
			if (seats == null || !seats.Any())
			{
				this.Visible = false;
				this.Page.AddNoRecordFoundAlert();
				return;
			}

			if (seats.Any(s => s.GraduateFormRequiredAndNotSubmitted))
			{
				this.Page.AddErrorAlert("To view your exam seating plan, please fill the \"Graduate Directory Form\" first.");
				switch (this.Page.AspireIdentity.UserType)
				{
					case UserTypes.Staff:
						break;
					case UserTypes.Student:
						Sys.Student.StudentInformation.GraduateDirectory.Redirect(null, semesterID);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				return;
			}

			var studentInfo = seats.First();
			this.lbEnrollment.Text = studentInfo.Enrollment;
			this.lblRegistrationNo.Text = studentInfo.RegistrationNo.ToString().ToNAIfNullOrEmpty();
			this.lblName.Text = studentInfo.Name;
			this.lblClass.Text = studentInfo.StudentClass;
			this.lblOfferedSemester.Text = studentInfo.OfferedSemesterID.ToSemesterString();
			this.lblStatus.Text = studentInfo.StudentStatusDisplay.ToNAIfNullOrEmpty();
			this.Visible = true;
			seats = seats.OrderBy(s => s.Date).ThenBy(s => s.StartTime).ToList();
			this.gvExamSeats.DataBind(seats);
			var examConductID = seats.FirstOrDefault(s => s.ExamConductID != null)?.ExamConductID;
			if (examConductID != null)
			{
				switch (this.Page.AspireIdentity.UserType)
				{
					case UserTypes.Staff:
						this.hlDownloadStudent.Visible = false;
						break;
					case UserTypes.Student:
						this.hlDownloadStudent.Visible = true;
						this.hlDownloadStudent.Attributes.Add("data-toggle", "modal");
						this.hlDownloadStudent.Attributes.Add("data-target", $"#{this.modalInstructions.ClientID}");
						this.hlDownload.NavigateUrl = Student.ExamSeatingPlan.StudentSlip.GetPageUrl(semesterID, examTypeEnum);
						this.hlDownload.Target = "_blank";
						this.cbInstructionsRead.Checked = false;
						this.hlDownload.Visible = false;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
				this.hlDownload.Visible = false;
		}

		protected void gvExamSeats_OnRowCreated(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow)
				return;
			var registeredCourseExamSeat = (BL.Core.ExamSeatingPlan.Common.ExamSeats.RegisteredCourseExamSeat)e.Row.DataItem;
			if (registeredCourseExamSeat == null || registeredCourseExamSeat.Date == null || registeredCourseExamSeat.StartTime == null || registeredCourseExamSeat.TotalTimeInMinutes == null)
				return;
			var paperStartTime = registeredCourseExamSeat.Date.Value.Date.Add(registeredCourseExamSeat.StartTime.Value);
			var paperFinishTime = paperStartTime.AddMinutes(registeredCourseExamSeat.TotalTimeInMinutes.Value);

			if (paperStartTime.Date <= DateTime.Now && DateTime.Now <= paperFinishTime)
			{
				e.Row.CssClass = "success";
				return;
			}
			if (paperFinishTime < DateTime.Now)
				e.Row.Font.Strikeout = true;
		}

		protected void cbInstructionsRead_OnCheckedChanged(object sender, EventArgs e)
		{
			this.hlDownload.Visible = this.cbInstructionsRead.Checked;
		}
	}
}