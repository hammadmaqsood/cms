﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StudentExamSeats.ascx.cs" Inherits="Aspire.Web.Sys.Common.ExamSeatingPlan.StudentExamSeats" %>
<table class="table table-bordered table-condensed studentInfo tableCol4">
	<caption>Student Information</caption>
	<tbody>
		<tr>
			<th>Enrollment</th>
			<td>
				<aspire:Label runat="server" ID="lbEnrollment" />
			</td>
			<th>Registration No.</th>
			<td>
				<aspire:Label runat="server" ID="lblRegistrationNo" />
			</td>
		</tr>
		<tr>
			<th>Name</th>
			<td>
				<aspire:Label runat="server" ID="lblName" />
			</td>
			<th>Semester</th>
			<td>
				<aspire:Label runat="server" ID="lblOfferedSemester" />
			</td>
		</tr>
		<tr>
			<th>Class</th>
			<td>
				<aspire:Label runat="server" ID="lblClass" />
			</td>
			<th>Status</th>
			<td>
				<aspire:Label runat="server" ID="lblStatus" />
			</td>
		</tr>
	</tbody>
</table>
<p></p>
<div class="alert alert-info">
	<strong>NOTE!</strong> Please check class, session, room. In case of any queries contact exams section immediately.
</div>
<p></p>
<aspire:AspireGridView runat="server" ItemType="Aspire.BL.Core.ExamSeatingPlan.Common.ExamSeats.RegisteredCourseExamSeat" ID="gvExamSeats" AutoGenerateColumns="False" Caption="Exam Seating Plan" OnRowCreated="gvExamSeats_OnRowCreated">
	<Columns>
		<asp:TemplateField HeaderText="#">
			<ItemTemplate><%#: Container.DataItemIndex+1 %></ItemTemplate>
		</asp:TemplateField>
		<asp:BoundField HeaderText="Title" DataField="Title" />
		<asp:BoundField HeaderText="Class" DataField="OfferedClass" />
		<asp:BoundField HeaderText="Date" DataField="Date" DataFormatString="{0:d}" />
		<asp:BoundField HeaderText="Session" DataField="SessionName" />
		<asp:BoundField HeaderText="Start Time" DataField="Timing" DataFormatString="{0:t}" />
		<asp:BoundField HeaderText="Room" DataField="RoomNameDisplay" />
		<asp:BoundField HeaderText="Row" DataField="RowDisplay" />
		<asp:BoundField HeaderText="Column" DataField="ColumnDisplay" />
		<asp:BoundField HeaderText="Status" HtmlEncode="False" DataField="RegisteredCourseStatusDisplay" />
		<asp:BoundField HeaderText="Fee Defaulter" DataField="FeeDisplay" />
		<asp:BoundField HeaderText="Surveys" DataField="SurveysDisplay" />
	</Columns>
</aspire:AspireGridView>
<div class="text-center">
	<aspire:AspireHyperLinkButton ButtonType="Primary" data-toggle="modal" runat="server" FontAwesomeIcon="solid_file_pdf" Text="Download PDF" ID="hlDownloadStudent" />
</div>
<aspire:AspireModal runat="server" ID="modalInstructions" HeaderText="Instructions to Students in the Examination Hall" ModalSize="Large">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanelInstructions" UpdateMode="Conditional">
			<ContentTemplate>
				<ol>
					<li>Read instructions printed on front page of answer sheets carefully.</li>
					<li>You must wear university I.D. card around the neck and must have your admit slips.</li>
					<li>Students will not be allowed to sit in the Exam after 5 minutes of start time. Students, after start of paper will not be permitted to go to washrooms/toilets or any other place outside the examination venue.</li>
					<li>Any unauthorized materials, such as cell phones, books, paper, documents, pictures and electronic devices with communication and/or storage capabilities such as tablet, mobile phones, smart watch etc. are not to be brought into the examination hall. Such materials or devices found in your possession after the start of the examination will be confiscated, and you will be liable to disciplinary action.</li>
					<li>All your personal belongings must be placed at the designated area. The University will not be responsible for loss or damage of any belongings.</li>
					<li>If a student is found talking to another student or person inside the examination hall, his paper shall be cancelled. All rough work is to be done on right side of the answer book, opposite the same question.</li>
					<li>If a student receives or attempts to receive help from any source, inside or outside the exam hall or has given help or attempted to give help, paper shall be cancelled.</li>
					<li>If a student writes even a question or anything relevant or irrelevant, either on tissue paper, admit slip or any other piece of paper including question paper, or attempts to pass on question paper or part thereof may lead to paper cancellation.</li>
					<li>Exchanging of seats or writing the roll number of another student on your answer book or question paper is not allowed.</li>
					<li>Creating any disturbance during the examination or refusal to obey the supervisory staff, may lead to rustication for one semester.</li>
					<li>No students shall be permitted to leave the exam hall earlier than half of the time of total exam duration.</li>
					<li>Any query related to question paper is to be clarified by concerned faculty member within first thirty minutes of the paper only.</li>
				</ol>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanelInstructionsButton" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireCheckBox CssClass="pull-left" runat="server" ID="cbInstructionsRead" ValidationGroup="Instructions" Text="I hereby acknowledge that I have read and understood the above instructions." AutoPostBack="True" OnCheckedChanged="cbInstructionsRead_OnCheckedChanged" />
				<aspire:AspireHyperLinkButton ButtonType="Primary" ID="hlDownload" Text="Download" Glyphicon="download" runat="server" />
				<aspire:AspireModalCloseButton runat="server">Close</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>
