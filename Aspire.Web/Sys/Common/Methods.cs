﻿using Aspire.BL.Core.FeeManagement.Common.Reports;
using Aspire.BL.Core.Scholarship.Common.Schemas;
using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Common
{
	internal static class Methods
	{
		public static void RenderFeeChallansReport<TRedirectPage>(ReportViewer reportViewer, List<int> studentFeeIDs) where TRedirectPage : BasePage
		{
			var reportFileName = "~/Reports/FeeManagement/Common/FeeChallans.rdlc".MapPath();
			List<FeeChallans.FeeChallan> feeChallans;
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					feeChallans = BL.Core.FeeManagement.Staff.Reports.FeeChallans.GetFeeChallans(studentFeeIDs, StaffIdentity.Current.LoginSessionGuid);
					break;
				case UserTypes.Student:
					feeChallans = BL.Core.FeeManagement.Student.Reports.FeeChallans.GetFeeChallans(studentFeeIDs, StudentIdentity.Current.LoginSessionGuid);
					break;
				case UserTypes.Candidate:
					feeChallans = BL.Core.FeeManagement.Candidate.Reports.FeeChallans.GetFeeChallans(studentFeeIDs, CandidateIdentity.Current.LoginSessionGuid);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			var aspireBasePage = (AspireBasePage)reportViewer.Page;
			var iReportViewer = (IReportViewer)aspireBasePage;
			if (feeChallans == null || !feeChallans.Any())
			{
				aspireBasePage.AddNoRecordFoundAlert();
				BasePage.Redirect<TRedirectPage>();
				return;
			}
			reportViewer.LocalReport.SubreportProcessing += (sender, e) =>
			{
				switch (e.ReportPath)
				{
					case "../../BULogo":
						break;
					case "FeeChallansSubReportBankInstituteCopy":
					case "FeeChallansSubReportCandidateCopy":
						var challanID = e.Parameters["StudentFeeChallanID"].Values[0].ToInt();
						var _feeChallans = feeChallans.FindAll(fc => fc.StudentFeeChallanID == challanID);
						e.DataSources.Add(new ReportDataSource("FeeChallan", _feeChallans));
						break;
					case "FeeChallansSubReportCandidateCopyBankDetails":
					case "FeeChallansSubReportBankInstituteCopyBankDetails":
						challanID = e.Parameters["StudentFeeChallanID"].Values[0].ToInt();
						var bankAccounts = feeChallans.Find(fc => fc.StudentFeeChallanID == challanID).BankAccounts;
						e.DataSources.Add(new ReportDataSource("BankAccounts", bankAccounts));
						break;
					case "FeeChallansSubReportCandidateCopyFeeDetails":
						challanID = e.Parameters["StudentFeeChallanID"].Values[0].ToInt();
						var feeDetails = feeChallans.Find(fc => fc.StudentFeeChallanID == challanID).FeeDetails;
						e.DataSources.Add(new ReportDataSource("FeeDetails", feeDetails));
						break;
					case "FeeChallansSubReportCandidateCopyRegisteredCourses":
						challanID = e.Parameters["StudentFeeChallanID"].Values[0].ToInt();
						var registeredCourses = feeChallans.Find(fc => fc.StudentFeeChallanID == challanID).RegisteredCourses;
						e.DataSources.Add(new ReportDataSource("RegisteredCourses", registeredCourses));
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(e.ReportPath));
				}
			};
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = iReportViewer.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("FeeChallans", feeChallans));
			reportViewer.LocalReport.EnableExternalImages = true;
		}

		public static void RenderAdmissionProcessingFeeChallansReport<TRedirectPage>(AspireBasePage aspireBasePage, ReportViewer reportViewer, int candidateAppliedProgramID) where TRedirectPage : BasePage
		{
			var reportFileName = "~/Reports/Admissions/Common/AdmissionProcessingFeeChallan.rdlc".MapPath();
			var admissionProcessingFeeChallans = BL.Core.Admissions.Common.Reports.GetAdmissionProcessingFeeChallans(candidateAppliedProgramID, AspireIdentity.Current.LoginSessionGuid);
			if (admissionProcessingFeeChallans == null || !admissionProcessingFeeChallans.Any())
			{
				aspireBasePage.AddNoRecordFoundAlert();
				BasePage.Redirect<TRedirectPage>();
				return;
			}
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.SubreportProcessing += (sender, e) =>
			{
				switch (e.ReportPath)
				{
					case "AdmissionProcessingFeeChallanSubReportBankAccounts":
						candidateAppliedProgramID = e.Parameters["CandidateAppliedProgramID"].Values[0].ToInt();
						e.DataSources.Add(new ReportDataSource("BankAccounts", admissionProcessingFeeChallans.Find(apfc => apfc.CandidateAppliedProgramID == candidateAppliedProgramID).BankAccounts));
						break;
				}
			};
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = $"Admission Processing Fee Challan(s) [{admissionProcessingFeeChallans.First().Name}]";
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("AdmissionProcessingFeeChallans", admissionProcessingFeeChallans));
			reportViewer.LocalReport.EnableExternalImages = true;
		}

		public static void RenderAdmitSlip<TRedirectPage>(AspireBasePage aspireBasePage, ReportViewer reportViewer, int candidateID, int? candidateAppliedProgramID) where TRedirectPage : BasePage
		{
			var reportFileName = "~/Reports/Admissions/Common/AdmitSlip.rdlc".MapPath();
			var admitSlips = BL.Core.Admissions.Common.Reports.GetAdmitSlip(candidateID, candidateAppliedProgramID, AspireIdentity.Current.LoginSessionGuid);
			if (admitSlips == null || !admitSlips.Any())
			{
				aspireBasePage.AddNoRecordFoundAlert();
				BasePage.Redirect<TRedirectPage>();
				return;
			}

			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					if (admitSlips.Any(a => !a.IsValid))
						aspireBasePage.AddErrorAlert("Candidate has not confirm the CBT session.");
					break;
				case UserTypes.Candidate:
					admitSlips = admitSlips.Where(a => a.IsValid).ToList();
					if (!admitSlips.Any())
					{
						if (candidateAppliedProgramID != null)
							Candidate.CBTSession.Redirect(candidateAppliedProgramID.Value);
						else
							BasePage.Redirect<Candidate.Dashboard>();
						return;
					}
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = $"Admit Slip(s) [{admitSlips.First().Name}]";
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("AdmitSlips", admitSlips.ToList()));
			reportViewer.LocalReport.EnableExternalImages = true;
		}

		public static void RenderQarzeHasanaFormReport<TRedirectPage>(AspireBasePage aspireBasePage, ReportViewer reportViewer, int scholarshipApplicationID) where TRedirectPage : BasePage
		{
			var reportDataSet = BL.Core.Scholarship.Common.QarzeHasana.Report.QarzeHasanaForm.GetScholarshipApplicationQarzeHasana(scholarshipApplicationID, AspireIdentity.Current.LoginSessionGuid);
			if (reportDataSet == null)
			{
				BasePage.Redirect<TRedirectPage>();
				return;
			}

			var reportFileName = "~/Reports/Scholarship/Common/QarzeHasana/ApplicationForm.rdlc".MapPath();
			reportViewer.LocalReport.SubreportProcessing += (sender, e) =>
			{
				switch (e.ReportPath)
				{
					case "../../../A4PortraitHeader":
					case "BULogo":
						break;
					case "ApplicationFormFamilyMembers":
						e.DataSources.Add(new ReportDataSource(nameof(reportDataSet.QarzeHasanaForm.FamilyMembers), reportDataSet.QarzeHasanaForm.FamilyMembers ?? new List<QarzeHasana.FamilyMember>()));
						break;
					case "ApplicationFormFamilyAssets":
						e.DataSources.Add(new ReportDataSource(nameof(reportDataSet.QarzeHasanaForm.FamilyAssets), reportDataSet.QarzeHasanaForm.FamilyAssets ?? new List<QarzeHasana.FamilyAsset>()));
						break;
					case "ApplicationFormVehicles":
						e.DataSources.Add(new ReportDataSource(nameof(reportDataSet.QarzeHasanaForm.Vehicles), reportDataSet.QarzeHasanaForm.Vehicles ?? new List<QarzeHasana.Vehicle>()));
						break;
					case "ApplicationFormSavings":
						e.DataSources.Add(new ReportDataSource(nameof(reportDataSet.QarzeHasanaForm.Savings), reportDataSet.QarzeHasanaForm.Savings ?? new List<QarzeHasana.Saving>()));
						break;
					case "ApplicationFormOtherIncomes":
						e.DataSources.Add(new ReportDataSource(nameof(reportDataSet.QarzeHasanaForm.OtherIncomes), reportDataSet.QarzeHasanaForm.OtherIncomes ?? new List<QarzeHasana.OtherIncome>()));
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(e.ReportPath), e.ReportPath, null);
				}
			};
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.QarzeHasanaForm), new[] { reportDataSet.QarzeHasanaForm }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.StudentInfo), new[] { reportDataSet.StudentInfo }));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}
