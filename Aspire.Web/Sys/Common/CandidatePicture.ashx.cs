﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Common
{
	public class CandidatePicture : AspireHandler
	{
		public static string GetImageUrl(int candidateID, TimeSpan cacheTimeSpan)
		{
			return "~/Sys/Common/CandidatePicture.ashx".AttachQueryParams(nameof(CandidateID), candidateID, nameof(CacheTimeSpan), cacheTimeSpan, EncryptionModes.ConstantSaltUserSession);
		}

		private int? CandidateID
		{
			get
			{
				switch (AspireIdentity.Current?.UserType)
				{
					case UserTypes.Admin:
					case UserTypes.Admins:
					case UserTypes.Staff:
					case UserTypes.MasterAdmin:
					case UserTypes.Executive:
						return this.Context.Request.GetParameterValue<int>(nameof(this.CandidateID));
					case UserTypes.Alumni:
					case UserTypes.Anonymous:
					case UserTypes.System:
					case UserTypes.Faculty:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Any:
					case UserTypes.Student:
					case null:
						return null;
					case UserTypes.Candidate:
						return CandidateIdentity.Current?.CandidateID ?? throw new InvalidOperationException();
					default:
						throw new NotImplementedEnumException(AspireIdentity.Current?.UserType);
				}
			}
		}

		private TimeSpan CacheTimeSpan => this.Context.Request.GetParameterValue<TimeSpan>(nameof(this.CacheTimeSpan)) ?? TimeSpan.Zero;

		protected override void ProcessRequest()
		{
			byte[] bytes = null;
			if (this.CandidateID != null)
				bytes = BL.Core.Admissions.Common.ProfileInformation.GetCandidateProfilePicture(this.CandidateID.Value, AspireIdentity.Current.LoginSessionGuid);
			StudentPicture.SendPictureToResponse(this.Context, bytes, this.CacheTimeSpan);
		}

		protected override void Authenticate()
		{
		}

		public override bool IsReusable => true;
	}
}