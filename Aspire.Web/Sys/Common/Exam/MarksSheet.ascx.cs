﻿using Aspire.BL.Core.Exams.Common.New;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Common.Exam
{
	public partial class MarksSheet : System.Web.UI.UserControl, IAlert
	{
		private new AspireBasePage Page => (AspireBasePage)base.Page;
		private int? OfferedCourseID => this.Page.GetParameterValue<int>(nameof(this.OfferedCourseID));
		private IMarksSheet _sheet;
		protected IMarksSheet Sheet => this._sheet ?? (this._sheet = CommonMethods.GetValidatedMarksSheet(this.Page));

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ucCourseInfo.SetCourseInfo(this.Sheet);
				this.repeaterMarksSheet.DataSource = new[] { this.Sheet };
				this.repeaterMarksSheet.DataBind();
				if (this.Request.Params["Debug"] == "Aspire" || Configurations.Current.AppSettings.DEBUG)
				{
					const string separator = "\n\t\t";
					var list = new List<(string, string)>
					{
						($"{nameof(this.Sheet.CanNotAddAssignmentReasons)}", $"{this.Sheet.CanNotAddAssignmentReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotEditAssignmentsReasons)}", $"{this.Sheet.CanNotEditAssignmentsReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotCompileAssignmentsReasons)}", $"{this.Sheet.CanNotCompileAssignmentsReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotUnlockAssignmentsReasons)}", $"{this.Sheet.CanNotUnlockAssignmentsReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotAddQuizReasons)}", $"{this.Sheet.CanNotAddQuizReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotEditQuizzesReasons)}", $"{this.Sheet.CanNotEditQuizzesReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotCompileQuizzesReasons)}", $"{this.Sheet.CanNotCompileQuizzesReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotUnlockQuizzesReasons)}", $"{this.Sheet.CanNotUnlockQuizzesReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotCompileInternalsReasons)}", $"{this.Sheet.CanNotCompileInternalsReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotUnlockInternalsReasons)}", $"{this.Sheet.CanNotUnlockInternalsReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotEditMidReasons)}", $"{this.Sheet.CanNotEditMidReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotUnlockMidReasons)}", $"{this.Sheet.CanNotUnlockMidReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotEditFinalReasons)}", $"{this.Sheet.CanNotEditFinalReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotUnlockFinalReasons)}", $"{this.Sheet.CanNotUnlockFinalReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotCompileGradesReasons)}", $"{this.Sheet.CanNotCompileGradesReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotLockMarksEntryReasons)}", $"{this.Sheet.CanNotLockMarksEntryReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotUnlockMarksEntryReasons)}", $"{this.Sheet.CanNotUnlockMarksEntryReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotSubmitToHODReasons)}", $"{this.Sheet.CanNotSubmitToHODReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotSubmitToCampusReasons)}", $"{this.Sheet.CanNotSubmitToCampusReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotSubmitToUniversityReasons)}", $"{this.Sheet.CanNotSubmitToUniversityReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotRevertSubmitToHODReasons)}", $"{this.Sheet.CanNotRevertSubmitToHODReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotRevertSubmitToCampusReasons)}", $"{this.Sheet.CanNotRevertSubmitToCampusReasons.Join(separator)}"),
						($"{nameof(this.Sheet.CanNotRevertSubmitToUniversityReasons)}", $"{this.Sheet.CanNotRevertSubmitToUniversityReasons.Join(separator)}"),
					};
					list.ForEach(i => this.tableLogs.Rows.Add(new TableRow
					{
						CssClass = i.Item2?.Any() == true ? "danger" : "success",
						Cells = { new TableCell { Text = i.Item1.HtmlEncode() }, new TableCell { Text = i.Item2.HtmlEncode(true) } }
					}));
					this.tableLogs.Visible = true;
				}
			}
		}

		protected void NavigationButton_OnCommand(object sender, NavigationButton.CommandEventArgs e)
		{
			if (this.OfferedCourseID == null)
				throw new InvalidOperationException();
			switch (e.CommandName)
			{
				case NavigationButton.CommandEventArgs.CommandNames.UnlockAssignment:
				case NavigationButton.CommandEventArgs.CommandNames.UnlockQuiz:
					{
						Model.Entities.OfferedCourseExam.ExamTypes examTypeEnum;
						switch (e.CommandName)
						{
							case NavigationButton.CommandEventArgs.CommandNames.UnlockAssignment:
								examTypeEnum = Model.Entities.OfferedCourseExam.ExamTypes.Assignment;
								break;
							case NavigationButton.CommandEventArgs.CommandNames.UnlockQuiz:
								examTypeEnum = Model.Entities.OfferedCourseExam.ExamTypes.Quiz;
								break;
							case NavigationButton.CommandEventArgs.CommandNames.UnlockAssignments:
							case NavigationButton.CommandEventArgs.CommandNames.UnlockCompileAssignments:
							case NavigationButton.CommandEventArgs.CommandNames.UnlockQuizzes:
							case NavigationButton.CommandEventArgs.CommandNames.UnlockCompileQuizzes:
							case NavigationButton.CommandEventArgs.CommandNames.UnlockCompileInternals:
							case NavigationButton.CommandEventArgs.CommandNames.UnlockMid:
							case NavigationButton.CommandEventArgs.CommandNames.UnlockFinal:
								throw new InvalidOperationException();
							default:
								throw new NotImplementedEnumException(e.CommandName);
						}
						var result = MarksHelper.UnlockOfferedCourseExamMarks(this.OfferedCourseID.Value, e.OfferedCourseExamID ?? throw new InvalidOperationException(), examTypeEnum, this.Page.AspireIdentity.LoginSessionGuid);
						if (result == null)
						{
							this.AddNoRecordFoundAlert();
							CommonMethods.RedirectToHome(this.OfferedCourseID.Value);
							return;
						}

						if (result.Value.success && !result.Value.unlockFailureReasons.Any())
						{
							this.AddSuccessAlert($"{examTypeEnum.ToFullName()} Marks have been unlocked.");
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}

						if (result.Value.unlockFailureReasons.Any())
						{
							this.AddErrorAlerts(false, result.Value.unlockFailureReasons);
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}

						throw new InvalidOperationException();
					}
				case NavigationButton.CommandEventArgs.CommandNames.UnlockAssignments:
				case NavigationButton.CommandEventArgs.CommandNames.UnlockCompileAssignments:
				case NavigationButton.CommandEventArgs.CommandNames.UnlockQuizzes:
				case NavigationButton.CommandEventArgs.CommandNames.UnlockCompileQuizzes:
				case NavigationButton.CommandEventArgs.CommandNames.UnlockCompileInternals:
				case NavigationButton.CommandEventArgs.CommandNames.UnlockMid:
				case NavigationButton.CommandEventArgs.CommandNames.UnlockFinal:
					{
						ExamMarksTypes examMarksTypeEnum;
						switch (e.CommandName)
						{
							case NavigationButton.CommandEventArgs.CommandNames.UnlockAssignments:
							case NavigationButton.CommandEventArgs.CommandNames.UnlockCompileAssignments:
								examMarksTypeEnum = ExamMarksTypes.Assignments;
								break;
							case NavigationButton.CommandEventArgs.CommandNames.UnlockQuizzes:
							case NavigationButton.CommandEventArgs.CommandNames.UnlockCompileQuizzes:
								examMarksTypeEnum = ExamMarksTypes.Quizzes;
								break;
							case NavigationButton.CommandEventArgs.CommandNames.UnlockCompileInternals:
								examMarksTypeEnum = ExamMarksTypes.Internals;
								break;
							case NavigationButton.CommandEventArgs.CommandNames.UnlockMid:
								examMarksTypeEnum = ExamMarksTypes.Mid;
								break;
							case NavigationButton.CommandEventArgs.CommandNames.UnlockFinal:
								examMarksTypeEnum = ExamMarksTypes.Final;
								break;
							case NavigationButton.CommandEventArgs.CommandNames.UnlockAssignment:
							case NavigationButton.CommandEventArgs.CommandNames.UnlockQuiz:
								throw new InvalidOperationException();
							default:
								throw new NotImplementedEnumException(e.CommandName);
						}
						var result = MarksHelper.UnlockExamMarks(this.OfferedCourseID.Value, examMarksTypeEnum, this.Page.AspireIdentity.LoginSessionGuid);
						if (result == null)
						{
							this.AddNoRecordFoundAlert();
							CommonMethods.RedirectToHome(this.OfferedCourseID.Value);
							return;
						}

						if (result.Value.success && !result.Value.unlockFailureReasons.Any())
						{
							this.AddSuccessAlert($"{examMarksTypeEnum.ToFullName()} Marks have been unlocked.");
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}

						if (result.Value.unlockFailureReasons.Any())
						{
							this.AddErrorAlerts(false, result.Value.unlockFailureReasons);
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						throw new InvalidOperationException();
					}
				default:
					throw new NotImplementedEnumException(e.CommandName);
			}
		}

		protected enum PageURLs
		{
			AddAssignment,
			EditAssignment,
			EditAssignments,
			CompileAssignments,
			AddQuiz,
			EditQuiz,
			EditQuizzes,
			CompileQuizzes,
			CompileInternals,
			EditMid,
			EditFinal
		}

		protected static string GetPageUrl(PageURLs pageUrl, int offeredCourseID, int? offeredCourseExamID)
		{
			switch (pageUrl)
			{
				case PageURLs.AddAssignment:
					return CommonMethods.GetMarksUrl(offeredCourseID, Model.Entities.OfferedCourseExam.ExamTypes.Assignment, null);
				case PageURLs.EditAssignment:
					return CommonMethods.GetMarksUrl(offeredCourseID, Model.Entities.OfferedCourseExam.ExamTypes.Assignment, offeredCourseExamID);
				case PageURLs.EditAssignments:
					return CommonMethods.GetExamMarksUrl(offeredCourseID, ExamMarksTypes.Assignments);
				case PageURLs.CompileAssignments:
					return CommonMethods.GetCompileExamUrl(offeredCourseID, CompileExamMarksTypes.Assignments);
				case PageURLs.AddQuiz:
					return CommonMethods.GetMarksUrl(offeredCourseID, Model.Entities.OfferedCourseExam.ExamTypes.Quiz, null);
				case PageURLs.EditQuiz:
					return CommonMethods.GetMarksUrl(offeredCourseID, Model.Entities.OfferedCourseExam.ExamTypes.Quiz, offeredCourseExamID);
				case PageURLs.EditQuizzes:
					return CommonMethods.GetExamMarksUrl(offeredCourseID, ExamMarksTypes.Quizzes);
				case PageURLs.CompileQuizzes:
					return CommonMethods.GetCompileExamUrl(offeredCourseID, CompileExamMarksTypes.Quizzes);
				case PageURLs.CompileInternals:
					return CommonMethods.GetCompileExamUrl(offeredCourseID, CompileExamMarksTypes.Internals);
				case PageURLs.EditMid:
					return CommonMethods.GetExamMarksUrl(offeredCourseID, ExamMarksTypes.Mid);
				case PageURLs.EditFinal:
					return CommonMethods.GetExamMarksUrl(offeredCourseID, ExamMarksTypes.Final);
				default:
					throw new NotImplementedEnumException(pageUrl);
			}
		}

		protected void repeaterMarksSheet_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "CompileGrades":
					{
						if (this.OfferedCourseID == null)
							throw new InvalidOperationException();
						var result = MarksHelper.CompileTotalAndGrades(this.OfferedCourseID.Value, this.Page.AspireIdentity.LoginSessionGuid);
						if (result == null)
						{
							this.AddNoRecordFoundAlert();
							CommonMethods.RedirectToHome(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.success && !result.Value.compileFailureReasons.Any())
						{
							this.AddSuccessAlert("Total and Grades have been compiled.");
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.compileFailureReasons.Any())
						{
							this.AddErrorAlerts(false, result.Value.compileFailureReasons);
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						throw new InvalidOperationException();
					}
				case "LockMarksEntry":
					{
						if (this.OfferedCourseID == null)
							throw new InvalidOperationException();
						var result = MarksHelper.LockMarksEntry(this.OfferedCourseID.Value, this.Page.AspireIdentity.LoginSessionGuid);
						if (result == null)
						{
							this.AddNoRecordFoundAlert();
							CommonMethods.RedirectToHome(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.success && !result.Value.lockFailureReasons.Any())
						{
							this.AddSuccessAlert("Marks Entry has been locked.");
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.lockFailureReasons.Any())
						{
							this.AddErrorAlerts(false, result.Value.lockFailureReasons);
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						throw new InvalidOperationException();

					}
				case "UnlockMarksEntry":
					{
						if (this.OfferedCourseID == null)
							throw new InvalidOperationException();
						var result = MarksHelper.UnlockMarksEntry(this.OfferedCourseID.Value, this.Page.AspireIdentity.LoginSessionGuid);
						if (result == null)
						{
							this.AddNoRecordFoundAlert();
							CommonMethods.RedirectToHome(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.success && !result.Value.unlockFailureReasons.Any())
						{
							this.AddSuccessAlert("Marks Entry has been unlocked.");
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.unlockFailureReasons.Any())
						{
							this.AddErrorAlerts(false, result.Value.unlockFailureReasons);
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						throw new InvalidOperationException();
					}
				case "SubmitToHOD":
					{
						if (this.OfferedCourseID == null)
							throw new InvalidOperationException();
						var result = MarksHelper.SubmitToHOD(this.OfferedCourseID.Value, this.Page.AspireIdentity.LoginSessionGuid);
						if (result == null)
						{
							this.AddNoRecordFoundAlert();
							CommonMethods.RedirectToHome(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.success && !result.Value.submitToHODFailureReasons.Any())
						{
							this.AddSuccessAlert("Marks have been submitted to HOD.");
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.submitToHODFailureReasons.Any())
						{
							this.AddErrorAlerts(false, result.Value.submitToHODFailureReasons);
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						throw new InvalidOperationException();
					}
				case "SubmitToCampus":
					{
						if (this.OfferedCourseID == null)
							throw new InvalidOperationException();
						var result = MarksHelper.SubmitToCampus(this.OfferedCourseID.Value, this.Page.AspireIdentity.LoginSessionGuid);
						if (result == null)
						{
							this.AddNoRecordFoundAlert();
							CommonMethods.RedirectToHome(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.success && !result.Value.submitToCampusFailureReasons.Any())
						{
							this.AddSuccessAlert("Marks have been submitted to Campus.");
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.submitToCampusFailureReasons.Any())
						{
							this.AddErrorAlerts(false, result.Value.submitToCampusFailureReasons);
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						throw new InvalidOperationException();
					}
				case "SubmitToUniversity":
					{
						if (this.OfferedCourseID == null)
							throw new InvalidOperationException();
						var result = MarksHelper.SubmitToUniversity(this.OfferedCourseID.Value, this.Page.AspireIdentity.LoginSessionGuid);
						if (result == null)
						{
							this.AddNoRecordFoundAlert();
							CommonMethods.RedirectToHome(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.success && !result.Value.submitToUniversityFailureReasons.Any())
						{
							this.AddSuccessAlert("Marks have been submitted to BUHO Exams.");
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.submitToUniversityFailureReasons.Any())
						{
							this.AddErrorAlerts(false, result.Value.submitToUniversityFailureReasons);
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						throw new InvalidOperationException();
					}
				case "RevertSubmitToHOD":
					{
						if (this.OfferedCourseID == null)
							throw new InvalidOperationException();
						var result = MarksHelper.RevertSubmitToHOD(this.OfferedCourseID.Value, this.Page.AspireIdentity.LoginSessionGuid);
						if (result == null)
						{
							this.AddNoRecordFoundAlert();
							CommonMethods.RedirectToHome(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.success && !result.Value.revertSubmitToHODFailureReasons.Any())
						{
							this.AddSuccessAlert("Marks submission have been reverted back to Faculty Member.");
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.revertSubmitToHODFailureReasons.Any())
						{
							this.AddErrorAlerts(false, result.Value.revertSubmitToHODFailureReasons);
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						throw new InvalidOperationException();
					}
				case "RevertSubmitToCampus":
					{
						if (this.OfferedCourseID == null)
							throw new InvalidOperationException();
						var result = MarksHelper.RevertSubmitToCampus(this.OfferedCourseID.Value, this.Page.AspireIdentity.LoginSessionGuid);
						if (result == null)
						{
							this.AddNoRecordFoundAlert();
							CommonMethods.RedirectToHome(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.success && !result.Value.revertSubmitToCampusFailureReasons.Any())
						{
							this.AddSuccessAlert("Marks submission have been reverted back to HOD.");
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.revertSubmitToCampusFailureReasons.Any())
						{
							this.AddErrorAlerts(false, result.Value.revertSubmitToCampusFailureReasons);
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						throw new InvalidOperationException();
					}
				case "RevertSubmitToUniversity":
					{
						if (this.OfferedCourseID == null)
							throw new InvalidOperationException();
						var result = MarksHelper.RevertSubmitToUniversity(this.OfferedCourseID.Value, this.Page.AspireIdentity.LoginSessionGuid);
						if (result == null)
						{
							this.AddNoRecordFoundAlert();
							CommonMethods.RedirectToHome(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.success && !result.Value.revertSubmitToUniversityFailureReasons.Any())
						{
							this.AddSuccessAlert("Marks submission have been reverted back to Campus.");
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.revertSubmitToUniversityFailureReasons.Any())
						{
							this.AddErrorAlerts(false, result.Value.revertSubmitToUniversityFailureReasons);
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						throw new InvalidOperationException();
					}
				case "History":
					{
						if (this.OfferedCourseID == null)
							throw new InvalidOperationException();
						this.marksSheetChanges.DisplayChanges(this.OfferedCourseID.Value);
						return;
					}
			}
		}

		protected void repeaterRegisteredCourses_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Unlock":
					{
						if (this.OfferedCourseID == null)
							throw new InvalidOperationException();
						var registeredCourseID = e.DecryptedCommandArgumentToInt();
						var result = MarksHelper.UnlockMarksEntryForRegisteredCourse(this.OfferedCourseID.Value, registeredCourseID, this.Page.AspireIdentity.LoginSessionGuid);
						if (result == null)
						{
							this.AddNoRecordFoundAlert();
							CommonMethods.RedirectToHome(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.success && !result.Value.unlockFailureReasons.Any())
						{
							this.AddSuccessAlert("Marks Entry for Registered Course has been unlocked.");
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						if (result.Value.unlockFailureReasons.Any())
						{
							this.AddErrorAlerts(false, result.Value.unlockFailureReasons);
							CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
							return;
						}
						throw new InvalidOperationException();
					}
			}
		}
	}
}