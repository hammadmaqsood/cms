﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExamResultStudentWise.ascx.cs" Inherits="Aspire.Web.Sys.Common.Exam.ExamResultStudentWise" %>
<%@ Register TagPrefix="uc" TagName="studentinfo" Src="~/Sys/Staff/Common/StudentInfoUserControl.ascx" %>
<div class="row">
	<div class="col-md-12">
		<aspire:AspireAlert runat="server" ID="alertResult"/>
	</div>
	<div class="col-md-12">
		<div class="form-inline">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollment" />
				<aspire:EnrollmentTextBox runat="server" ID="tbEnrollment" ValidationGroup="RequiredEnrollment" OnSearch="tbEnrollment_OnSearch" />
			</div>
			<div class="form-group">
				<aspire:AspireHyperLink runat="server" Text="Clear" NavigateUrl="~/Sys/Staff/Exams/ExamResultStudentWise.aspx" />
			</div>
		</div>
	</div>
</div>
<p></p>
<asp:Panel runat="server" ID="panelExamResultStudentWise">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireHyperLink runat="server" ID="hblPrintResult" Text="Print Result" CssClass="btn btn-primary" Target="_blank" />
				</div>
			</div>
		</div>
	</div>

	<uc:studentinfo runat="server" ID="ucStudentInfo" />
	<asp:Repeater runat="server" ID="repeaterExamResult">
		<ItemTemplate>
			<table class="table table-bordered table-responsive table-condensed table-hover">
				<caption>
					<%#:this.Eval("Semester") %></caption>
				<thead>
					<tr>
						<th class="text-center">#</th>
						<th class="text-center">Code</th>
						<th>Title</th>
						<th class="text-center">Credit Hours</th>
						<th>Teacher</th>
						<th class="text-center">Class</th>
						<th class="text-center">Assignments</th>
						<th class="text-center">Quizzes</th>
						<th class="text-center">Internals</th>
						<th class="text-center">Mid</th>
						<th class="text-center">Final</th>
						<th class="text-center">Total</th>
						<th class="text-center">Grade</th>
						<th class="text-center">Grade Points</th>
						<th class="text-center">Product</th>
						<th class="text-center">Status</th>
						<th class="text-center">Fee</th>
						<th class="text-center">Present Hrs</th>
						<th class="text-center">Absent Hrs</th>
						<th class="text-center">Total Hrs</th>
					</tr>
				</thead>
				<tbody>
					<asp:Repeater runat="server" DataSource='<%#this.Eval("RegisteredCourses") %>'>
						<ItemTemplate>
							<tr>
								<td class="text-center"><%#: Container.ItemIndex+1 %></td>
								<td class="text-center"><%#: this.Eval("CourseCode") %></td>
								<td><%#:this.Eval("Title") %></td>
								<td class="text-center"><%#: this.Eval("StringCreditHours") %></td>
								<td><%#: this.Eval("Teacher") %></td>
								<td><%#: this.Eval("Class") %></td>
								<td class="text-center"><%#: this.Eval("AssignmentsString") %></td>
								<td class="text-center"><%#: this.Eval("QuizzesString") %></td>
								<td class="text-center"><%#: this.Eval("InternalString") %></td>
								<td class="text-center"><%#: this.Eval("MidString") %></td>
								<td class="text-center"><%#: this.Eval("FinalString") %></td>
								<td class="text-center"><%#: this.Eval("Total") %></td>
								<td class="text-center"><%#: this.Eval("GradeEnumFullName") %></td>
								<td class="text-center"><%#: this.Eval("GradePoints") %></td>
								<td class="text-center"><%#: this.Eval("Product") %></td>
								<td class="text-center"><%#: this.Eval("StatusComplete") %></td>
								<td class="text-center"><%#: this.Eval("StudentFeeStatusFullName") %></td>
								<td class="text-center"><%#: $"{this.Eval("PresentHours")} :{this.Eval("PercentageShort")}%" %></td>
								<td class="text-center"><%#: this.Eval("AbsentHours") %></td>
								<td class="text-center"><%#: this.Eval("TotalHours") %></td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
					<tr>
						<td colspan="16" class="text-right"><strong>GPA:</strong> <%#:this.Eval("StringBUExamGPA") %>
						</td>
						<td colspan="2"><strong>CGPA:</strong> <%#:this.Eval("StringBUExamCGPA") %>
						</td>
						<td colspan="3"><strong>Remarks:</strong> <%#:this.Eval("StringBUExamResultRemarks") %>
						</td>
					</tr>
				</tbody>
			</table>
		</ItemTemplate>
	</asp:Repeater>
</asp:Panel>
