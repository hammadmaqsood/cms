﻿using Aspire.BL.Core.Exams.Common.New;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Common.Exam
{
	public partial class CompileExam : System.Web.UI.UserControl, IAlert
	{
		private new AspireBasePage Page => (AspireBasePage)base.Page;
		private int? OfferedCourseID => this.Page.GetParameterValue<int>(nameof(this.OfferedCourseID));
		protected CompileExamMarksTypes? CompileExamMarksType => this.Page.GetParameterValue<CompileExamMarksTypes>(nameof(this.CompileExamMarksType));
		private IMarksSheet _sheet;
		protected IMarksSheet Sheet => this._sheet ?? (this._sheet = CommonMethods.GetValidatedMarksSheet(this.Page));

		private void RefreshPage()
		{
			this.Response.Redirect(CommonMethods.GetCompileExamUrl(this.OfferedCourseID ?? throw new InvalidOperationException(), this.CompileExamMarksType ?? throw new InvalidOperationException()));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.Sheet == null)
					throw new InvalidOperationException();
				if (this.CompileExamMarksType == null)
				{
					CommonMethods.RedirectToMarksSheet(this.Sheet.OfferedCourse.OfferedCourseID);
					return;
				}

				OfferedCours.ExamCompilationTypes examCompilationType;
				switch (this.CompileExamMarksType.Value)
				{
					case CompileExamMarksTypes.Assignments:
						if (!this.Sheet.CanCompileAssignments)
						{
							this.AddErrorAlerts(false, this.Sheet.CanNotCompileAssignmentsReasons);
							CommonMethods.RedirectToMarksSheet(this.Sheet);
							return;
						}
						examCompilationType = this.Sheet.OfferedCourse.AssignmentsExamCompilationTypeEnum ?? OfferedCours.ExamCompilationTypes.OverallAverage;
						break;
					case CompileExamMarksTypes.Quizzes:
						if (!this.Sheet.CanCompileQuizzes)
						{
							this.AddErrorAlerts(false, this.Sheet.CanNotCompileQuizzesReasons);
							CommonMethods.RedirectToMarksSheet(this.Sheet);
							return;
						}
						examCompilationType = this.Sheet.OfferedCourse.QuizzesExamCompilationTypeEnum ?? OfferedCours.ExamCompilationTypes.OverallAverage;
						break;
					case CompileExamMarksTypes.Internals:
						if (!this.Sheet.CanCompileInternals)
						{
							this.AddErrorAlerts(false, this.Sheet.CanNotCompileInternalsReasons);
							CommonMethods.RedirectToMarksSheet(this.Sheet);
							return;
						}
						examCompilationType = this.Sheet.OfferedCourse.InternalsExamCompilationTypeEnum ?? OfferedCours.ExamCompilationTypes.OverallAverage;
						break;
					default:
						throw new NotImplementedEnumException(this.CompileExamMarksType);
				}

				this.ucCourseInfo.SetCourseInfo(this.Sheet);
				this.hlMarksSheet.NavigateUrl = CommonMethods.GetMarksSheetUrl(this.Sheet);
				this.ddlExamCompilationType.FillExamCompilationTypes();
				this.ddlExamCompilationType.SetEnumValue(examCompilationType);
				this.ddlExamCompilationType_OnSelectedIndexChanged(null, null);

				if (this.Sheet.IsStaff)
				{
					this.btnSave.ConfirmMessage = "Faculty member will not be able to update this record. Are you sure you want to save?";
					this.btnSubmit.Visible = false;
				}
			}
		}

		protected OfferedCours.ExamCompilationTypes ExamCompilationType;

		protected void ddlExamCompilationType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ExamCompilationType = this.ddlExamCompilationType.GetSelectedEnumValue<OfferedCours.ExamCompilationTypes>();
			this.repeaterRegisteredCourses.DataBind(this.Sheet.OfferedCourse.RegisteredCourses.OrderBy(rc => rc.Enrollment));
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!CommonMethods.IsPinCodeVerified() || this.CompileExamMarksType == null || this.OfferedCourseID == null)
				return;
			var submit = sender == this.btnSubmit || this.Page.AspireIdentity.UserType.IsStaff();
			var examCompilationType = this.ddlExamCompilationType.GetSelectedEnumValue<OfferedCours.ExamCompilationTypes>();
			var result = MarksHelper.CompileExamMarks(this.OfferedCourseID.Value, this.CompileExamMarksType.Value, examCompilationType, submit, this.Page.AspireIdentity.LoginSessionGuid);
			if (result == null)
			{
				this.Page.AddNoRecordFoundAlert();
				CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
				return;
			}
			if (result.Value.success)
			{
				if (submit)
				{
					this.AddSuccessAlert($"{this.CompileExamMarksType.Value.ToFullName()} marks have been compiled, saved and submitted.");
					CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
				}
				else
				{
					this.AddSuccessAlert($"{this.CompileExamMarksType.Value.ToFullName()} marks have been compiled and saved.");
					this.RefreshPage();
				}
				return;
			}
			if (result.Value.compileFailureReasons.Any())
			{
				this.AddErrorAlerts(false, result.Value.compileFailureReasons);
				return;
			}
			throw new InvalidOperationException();
		}
	}
}
