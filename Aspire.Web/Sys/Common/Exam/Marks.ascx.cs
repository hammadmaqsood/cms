﻿using Aspire.BL.Core.Exams.Common.New;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using OfferedCourseExam = Aspire.Model.Entities.OfferedCourseExam;

namespace Aspire.Web.Sys.Common.Exam
{
	public partial class Marks : System.Web.UI.UserControl, IAlert
	{
		private new AspireBasePage Page => (AspireBasePage)base.Page;
		private int? OfferedCourseExamID => this.Page.GetParameterValue<int>(nameof(this.OfferedCourseExamID));
		private int? OfferedCourseID => this.Page.GetParameterValue<int>(nameof(this.OfferedCourseID));
		private OfferedCourseExam.ExamTypes? ExamType => this.Page.GetParameterValue<OfferedCourseExam.ExamTypes>(nameof(this.ExamType));
		private IMarksSheet _sheet;
		private IMarksSheet Sheet => this._sheet ?? (this._sheet = CommonMethods.GetValidatedMarksSheet(this.Page));

		private void RefreshPage(int? offeredCourseExamID)
		{
			this.Response.Redirect(CommonMethods.GetMarksUrl(this.OfferedCourseID ?? throw new InvalidOperationException(), this.ExamType ?? throw new InvalidOperationException(), offeredCourseExamID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.Sheet == null)
					throw new InvalidOperationException();
				if (this.ExamType == null)
				{
					CommonMethods.RedirectToMarksSheet(this.Sheet.OfferedCourse.OfferedCourseID);
					return;
				}
				if (this.OfferedCourseExamID != null)
				{
					var offeredCourseExam = this.Sheet.OfferedCourse.OfferedCourseExams.SingleOrDefault(oce => oce.OfferedCourseExamID == this.OfferedCourseExamID.Value);
					if (offeredCourseExam == null)
					{
						this.AddErrorAlert($"{this.ExamType.Value.ToFullName()} record not found.");
						CommonMethods.RedirectToMarksSheet(this.Sheet);
						return;
					}
					IReadOnlyList<IStudentMarks> marks;
					switch (this.ExamType.Value)
					{
						case OfferedCourseExam.ExamTypes.Assignment:
							if (!offeredCourseExam.CanEditAssignment)
							{
								this.AddErrorAlerts(false, offeredCourseExam.CanNotEditAssignmentReasons);
								CommonMethods.RedirectToMarksSheet(this.Sheet);
								return;
							}
							marks = this.Sheet.GetAssignmentMarks(this.OfferedCourseExamID.Value);
							break;
						case OfferedCourseExam.ExamTypes.Quiz:
							if (!offeredCourseExam.CanEditQuiz)
							{
								this.AddErrorAlerts(false, offeredCourseExam.CanNotEditQuizReasons);
								CommonMethods.RedirectToMarksSheet(this.Sheet);
								return;
							}
							marks = this.Sheet.GetQuizMarks(this.OfferedCourseExamID.Value);
							break;
						default:
							throw new NotImplementedEnumException(this.ExamType);
					}

					this.lblExamNo.Text = this.ExamType.Value.ToFullName() + " No.:";
					var examNos = Enumerable.Range(1, 16).Select(i => new ListItem
					{
						Text = i.ToString(),
						Value = i.ToString(),
						Enabled = !this.Sheet.OfferedCourse.OfferedCourseExams.Any(oce => oce.OfferedCourseExamID != offeredCourseExam.OfferedCourseExamID && oce.ExamTypeEnum == this.ExamType && oce.ExamNo == i)
					});
					this.ddlExamNo.DataBind(examNos);
					this.ddlExamNo.SelectedValue = offeredCourseExam.ExamNo.ToString();
					this.dtpExamDate.SelectedDate = offeredCourseExam.ExamDate;
					this.tbTotalMarks.Text = offeredCourseExam.TotalMarks.ToExamMarksString();
					this.tbTotalMarks.ReadOnly = true;
					this.tbRemarks.Text = offeredCourseExam.Remarks;
					this.ucStudents.Visible = true;
					this.ucStudents.DataBind(marks);

					this.btnDeleteOfferedCourseExam.Visible = true;
					this.btnSubmitOfferedCourseExam.Visible = AspireIdentity.Current.UserType.IsFaculty();
					this.btnAddOfferedCourseExam.Visible = false;
					this.btnSaveOfferedCourseExam.Visible = true;
					if (AspireIdentity.Current.UserType.IsStaff())
					{
						this.btnSaveOfferedCourseExam.ConfirmMessage = "Faculty Member will not be able to update this record. Do you want to save this record?";
						this.btnSaveOfferedCourseExam.Text = "Save & Submit";
					}
					else
					{
						this.btnSaveOfferedCourseExam.ConfirmMessage = null;
						this.btnSaveOfferedCourseExam.Text = "Save";
					}
				}
				else
				{
					switch (this.ExamType.Value)
					{
						case OfferedCourseExam.ExamTypes.Assignment:
							if (!this.Sheet.CanAddAssignment)
							{
								this.AddErrorAlerts(false, this.Sheet.CanNotAddAssignmentReasons);
								CommonMethods.RedirectToMarksSheet(this.Sheet);
								return;
							}
							break;
						case OfferedCourseExam.ExamTypes.Quiz:
							if (!this.Sheet.CanAddQuiz)
							{
								this.AddErrorAlerts(false, this.Sheet.CanNotAddQuizReasons);
								CommonMethods.RedirectToMarksSheet(this.Sheet);
								return;
							}
							break;
						default:
							throw new NotImplementedEnumException(this.ExamType);
					}

					var examNos = Enumerable.Range(1, 16).Select(i => new ListItem
					{
						Text = i.ToString(),
						Value = i.ToString(),
						Enabled = !this.Sheet.OfferedCourse.OfferedCourseExams.Any(oce => oce.ExamTypeEnum == this.ExamType && oce.ExamNo == i)
					});
					this.lblExamNo.Text = this.ExamType.Value.ToFullName() + " No.:";
					this.ddlExamNo.DataBind(examNos);
					this.dtpExamDate.SelectedDate = DateTime.Today;
					this.tbTotalMarks.Text = null;
					this.tbRemarks.Text = null;
					this.ucStudents.Visible = false;
					this.btnDeleteOfferedCourseExam.Visible = false;
					this.btnSubmitOfferedCourseExam.Visible = false;
					this.btnAddOfferedCourseExam.Visible = true;
					this.btnSaveOfferedCourseExam.Visible = false;
				}
				this.ucCourseInfo.SetCourseInfo(this.Sheet);
				this.hlMarksSheet.NavigateUrl = CommonMethods.GetMarksSheetUrl(this.Sheet);
				this.hlCancel.NavigateUrl = CommonMethods.GetMarksSheetUrl(this.Sheet);
			}
		}

		protected void btnAddOfferedCourseExam_OnClick(object sender, EventArgs e)
		{
			if (!CommonMethods.IsPinCodeVerified())
				return;
			if (!this.Page.IsValid || this.OfferedCourseExamID != null)
				return;
			var examType = this.ExamType ?? throw new InvalidOperationException();
			var examDate = this.dtpExamDate.SelectedDate ?? throw new InvalidOperationException();
			var examNo = this.ddlExamNo.SelectedValue.ToByte();
			var totalMarks = this.tbTotalMarks.Text.ToDecimal();
			var remarks = this.tbRemarks.Text;
			if (this.OfferedCourseExamID != null || this.OfferedCourseID == null)
				throw new InvalidOperationException();
			var result = MarksHelper.AddOfferedCourseExam(this.OfferedCourseID.Value, examType, examDate, examNo, totalMarks, remarks, this.Page.AspireIdentity.LoginSessionGuid);
			if (result == null)
			{
				this.Page.AddNoRecordFoundAlert();
				this.RefreshPage(null);
				return;
			}
			if (result.Value.offeredCourseExamID != null)
			{
				this.AddSuccessMessageHasBeenAdded($"{examType.ToFullName()}");
				this.RefreshPage(result.Value.offeredCourseExamID.Value);
				return;
			}
			if (result.Value.addFailureReasons.Any())
			{
				this.AddErrorAlerts(false, result.Value.addFailureReasons);
				return;
			}
			throw new InvalidOperationException();
		}

		protected void btnSaveOfferedCourseExam_OnClick(object sender, EventArgs e)
		{
			if (!CommonMethods.IsPinCodeVerified())
				return;
			if (!this.Page.IsValid || this.OfferedCourseExamID == null)
				return;
			if (this.OfferedCourseID == null | this.OfferedCourseExamID == null || this.ExamType == null)
				throw new InvalidOperationException();
			var examDate = this.dtpExamDate.SelectedDate ?? throw new InvalidOperationException();
			var examNo = this.ddlExamNo.SelectedValue.ToByte();
			var remarks = this.tbRemarks.Text;
			var marks = this.ucStudents.GetMarks();
			var submit = sender == this.btnSubmitOfferedCourseExam || this.Page.AspireIdentity.UserType.IsStaff();

			var result = MarksHelper.UpdateOfferedCourseExam(this.OfferedCourseID.Value, this.ExamType.Value, this.OfferedCourseExamID.Value, examDate, examNo, remarks, marks, submit, this.Page.AspireIdentity.LoginSessionGuid);
			if (result == null)
			{
				this.Page.AddNoRecordFoundAlert();
				this.RefreshPage(null);
				return;
			}
			if (result.Value.success)
			{
				if (submit)
				{
					this.AddSuccessAlert($"{this.ExamType?.ToFullName()} No. {examNo} marks have been saved and submitted.");
					CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
				}
				else
				{
					this.AddSuccessAlert($"{this.ExamType?.ToFullName()} No. {examNo} marks have been saved.");
					this.RefreshPage(this.OfferedCourseExamID.Value);
				}
				return;
			}
			if (result.Value.updateFailureReasons.Any())
			{
				this.AddErrorAlerts(false, result.Value.updateFailureReasons);
				return;
			}
			throw new InvalidOperationException();
		}

		protected void btnDeleteOfferedCourseExam_OnClick(object sender, EventArgs e)
		{
			if (!CommonMethods.IsPinCodeVerified())
				return;
			if (this.OfferedCourseExamID == null || this.OfferedCourseID == null || this.ExamType == null)
				throw new InvalidOperationException();
			var result = MarksHelper.DeleteOfferedCourseExam(this.OfferedCourseID.Value, this.ExamType.Value, this.OfferedCourseExamID.Value, this.Page.AspireIdentity.LoginSessionGuid);
			if (result == null)
			{
				this.Page.AddNoRecordFoundAlert();
				this.RefreshPage(null);
				return;
			}
			if (result.Value.success)
			{
				this.AddSuccessMessageHasBeenDeleted($"{this.ExamType.Value.ToFullName()}");
				CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
				return;
			}
			if (result.Value.deleteFailureReasons.Any())
			{
				this.AddErrorAlerts(false, result.Value.deleteFailureReasons);
				return;
			}
			throw new InvalidOperationException();
		}
	}
}
