﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Marks.ascx.cs" Inherits="Aspire.Web.Sys.Common.Exam.Marks" %>
<%@ Register Src="~/Sys/Common/Exam/CourseInfo.ascx" TagPrefix="uc1" TagName="CourseInfo" %>
<%@ Register Src="~/Sys/Common/Exam/Students.ascx" TagPrefix="uc1" TagName="Students" %>
<%@ Register Src="~/Sys/Common/Exam/GeneratePinCode.ascx" TagPrefix="uc1" TagName="GeneratePinCode" %>

<uc1:CourseInfo runat="server" ID="ucCourseInfo" />
<div class="form-group">
	<aspire:AspireHyperLinkButton runat="server" ID="hlMarksSheet" Glyphicon="list" Text="Marks Sheet" />
	<uc1:GeneratePinCode runat="server" ID="GeneratePinCode" />
</div>
<div class="row">
	<div class="form-group col-md-4">
		<aspire:AspireLabel runat="server" ID="lblExamNo" AssociatedControlID="ddlExamNo" />
		<aspire:AspireDropDownList runat="server" ID="ddlExamNo" ValidationGroup="Exam" />
		<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamNo" ErrorMessage="This field is required." ValidationGroup="Exam" />
	</div>
	<div class="form-group col-md-4">
		<aspire:AspireLabel runat="server" Text="Date:" AssociatedControlID="dtpExamDate" />
		<aspire:AspireDateTimePickerTextbox HorizontalPosition="Left" VerticalPosition="Bottom" UserManualEntryAllowed="False" runat="server" ID="dtpExamDate" ValidationGroup="Exam" />
		<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dtpExamDate" AllowNull="False" RequiredErrorMessage="This field is required." ErrorMessage="Invalid Date." ValidationGroup="Exam" />
	</div>
	<div class="form-group col-md-4">
		<aspire:AspireLabel runat="server" Text="Total Marks:" AssociatedControlID="tbTotalMarks" />
		<aspire:AspireTextBox data-inputmask="'alias': 'marks'" runat="server" ID="tbTotalMarks" ValidationGroup="Exam" MaxLength="8" />
		<aspire:AspireDoubleValidator runat="server" AllowNull="False" ControlToValidate="tbTotalMarks" InvalidRangeErrorMessage="Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." ValidationGroup="Exam" MinValue="0.001" MaxValue="9999.999" />
	</div>
</div>
<div class="form-group">
	<aspire:AspireLabel runat="server" Text="Remarks:" AssociatedControlID="tbRemarks" />
	<aspire:AspireTextBox runat="server" ID="tbRemarks" TextMode="MultiLine" MaxLength="1048576" Rows="3" ValidationGroup="Exam" />
</div>
<uc1:Students runat="server" ID="ucStudents" />
<div class="text-center">
	<div class="pull-left">
		<aspire:AspireButton runat="server" Text="Delete" ButtonType="Danger" CausesValidation="False" ID="btnDeleteOfferedCourseExam" OnClick="btnDeleteOfferedCourseExam_OnClick" ConfirmMessage="Are you sure you want to delete this record?" />
		<aspire:AspireButton runat="server" Text="Save & Submit" ButtonType="Danger" ValidationGroup="insert" ID="btnSubmitOfferedCourseExam" OnClick="btnSaveOfferedCourseExam_OnClick" ConfirmMessage="You will not be able to make any changes after submission. Are you sure you want to submit this record?" />
	</div>
	<aspire:AspireButton ButtonType="Primary" runat="server" Text="Add" ID="btnAddOfferedCourseExam" ValidationGroup="Exam" OnClick="btnAddOfferedCourseExam_OnClick" />
	<aspire:AspireButton runat="server" Text="Save" ID="btnSaveOfferedCourseExam" ButtonType="Primary" OnClick="btnSaveOfferedCourseExam_OnClick" ValidationGroup="insert" />
	<aspire:AspireHyperLink runat="server" Text="Cancel" ID="hlCancel" />
</div>
