﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExamNavigateButton.ascx.cs" Inherits="Aspire.Web.Sys.Common.Exam.ExamNavigateButton" %>

<div class="btn-group btn-group-sm btn-group" style="display: flex;">
	<aspire:AspireHyperLinkButton runat="server" ID="btn" ButtonType="Default" />
	<aspire:Label runat="server" ID="lbl" />
	<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="btnDropDown" runat="server">
		<span class="caret"></span>
		<span class="sr-only">Toggle Dropdown</span>
	</button>
	<ul class="dropdown-menu" id="ulDropDown" runat="server">
		<li>
			<aspire:LinkButton ID="btnUnlock" runat="server" ConfirmMessage="Are you sure you want to unlock marks for Faculty Member?" Text="Unlock Marks" CausesValidation="False" OnClick="btnUnlock_OnClick" />
		</li>
	</ul>
</div>
