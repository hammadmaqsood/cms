﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Common.Exam
{
	public partial class CourseInfo : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		[Serializable]
		private class KeyValue
		{
			public string Key { get; set; }
			public string Value { get; set; }
		}

		public void SetCourseInfo(BL.Core.Exams.Common.New.IMarksSheet marksSheet)
		{
			var information = new List<KeyValue>
			{
				new KeyValue {Key = "Title", Value = marksSheet.OfferedCourse.Title},
				new KeyValue {Key = "Teacher Name", Value = marksSheet.OfferedCourse.FacultyMemberName.ToNAIfNullOrEmpty()},
				new KeyValue {Key = "Credit Hours", Value = marksSheet.OfferedCourse.CreditHours.ToCreditHoursFullName()},
				new KeyValue {Key = "Contact Hours", Value = marksSheet.OfferedCourse.ContactHours.ToCreditHoursFullName()},
				new KeyValue {Key = "Category", Value = marksSheet.OfferedCourse.CourseCategoryEnum.ToFullName()},
				new KeyValue {Key = "Class", Value = marksSheet.OfferedCourse.Class},
				new KeyValue {Key = "Semester", Value = marksSheet.OfferedCourse.OfferedSemesterID.ToSemesterString()},
			};

			switch (marksSheet.MarksEntryTypeEnum)
			{
				case ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
				case ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
					information.Add(new KeyValue { Key = "Assignments Submitted", Value = marksSheet.OfferedCourse.AssignmentsMarksSubmittedString });
					information.Add(new KeyValue { Key = "Quizzes Submitted", Value = marksSheet.OfferedCourse.QuizzesMarksSubmittedString });
					information.Add(new KeyValue { Key = "Mid Submitted", Value = marksSheet.OfferedCourse.MidMarksSubmittedString });
					information.Add(new KeyValue { Key = "Final Submitted", Value = marksSheet.OfferedCourse.FinalMarksSubmittedString });
					break;
				case ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					information.Add(new KeyValue { Key = "Internals Submitted", Value = marksSheet.OfferedCourse.InternalsMarksSubmittedString });
					information.Add(new KeyValue { Key = "Mid Submitted", Value = marksSheet.OfferedCourse.MidMarksSubmittedString });
					information.Add(new KeyValue { Key = "Final Submitted", Value = marksSheet.OfferedCourse.FinalMarksSubmittedString });
					break;
				case ExamMarksPolicy.MarksEntryTypes.Final:
					information.Add(new KeyValue { Key = "Final Submitted", Value = marksSheet.OfferedCourse.FinalMarksSubmittedString });
					break;
				default:
					throw new NotImplementedEnumException(marksSheet.MarksEntryTypeEnum);
			}

			information.Add(new KeyValue { Key = "Marks Entry Completed", Value = marksSheet.OfferedCourse.MarksEntryCompleted.ToYesNo() });
			information.Add(new KeyValue { Key = "Marks Entry Locked", Value = marksSheet.OfferedCourse.MarksEntryLockedString });
			information.Add(new KeyValue { Key = "Submitted to HoD", Value = marksSheet.OfferedCourse.SubmittedToHODString });
			information.Add(new KeyValue { Key = "Submitted to Campus", Value = marksSheet.OfferedCourse.SubmittedToCampusString });
			information.Add(new KeyValue { Key = "Submitted to BUHO Exams", Value = marksSheet.OfferedCourse.SubmittedToUniversityString });
			this.ViewState["Information"] = information;
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			this.tableCourseInfo.Rows.Clear();
			var information = (List<KeyValue>)this.ViewState["Information"];
			if (information != null)
			{
				const int cols = 3;
				this.tableCourseInfo.CssClass += " tableCol6";
				TableRow row = null;
				var count = 0;
				foreach (var info in information)
				{
					if (count % cols == 0)
					{
						row = new TableRow();
						this.tableCourseInfo.Rows.Add(row);
					}
					if (row == null)
						throw new InvalidOperationException();
					row.Cells.Add(new TableHeaderCell { Text = info.Key });
					row.Cells.Add(new TableCell { Text = info.Value });
					count++;
				}
			}
		}
	}
}