﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Students.ascx.cs" Inherits="Aspire.Web.Sys.Common.Exam.Students" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Register TagPrefix="uc1" TagName="MarksTextBox" Src="MarksTextBox.ascx" %>
<asp:Repeater runat="server" ID="repeater" ItemType="Aspire.BL.Core.Exams.Common.New.IStudentMarks">
	<HeaderTemplate>
		<style type="text/css">
			table.marks td {
				vertical-align: middle !important;
			}
		</style>
		<table class="marks table table-condensed table-bordered table-striped table-hover" id="<%#this.repeater.ClientID %>">
			<thead>
				<tr>
					<th>#</th>
					<th>Enrollment</th>
					<th>Name</th>
					<th>Student Status</th>
					<th>Program</th>
					<th>Marks</th>
					<th>Status</th>
					<th>Record</th>
				</tr>
			</thead>
			<tbody>
	</HeaderTemplate>
	<ItemTemplate>
		<tr class="trs">
			<td><%#: Container.ItemIndex + 1 %></td>
			<td>
				<%#: Item.RegisteredCourse.Enrollment %>
			</td>
			<td>
				<%#: Item.RegisteredCourse.Name %>
			</td>
			<td>
				<%#: Item.RegisteredCourse.StudentStatusEnum?.ToFullName() %>
			</td>
			<td>
				<%#: Item.RegisteredCourse.ProgramAlias %>
			</td>
			<td class="col-md-3">
				<uc1:MarksTextBox ID="ucMarks" EditingAllowed="<%# Item.RegisteredCourse.MarksEntryAllowed %>" MarksInDB="<%# Item.MarksObtained %>" MarksNew="<%# Item.MarksObtained %>" MinMarks="<%# Item.Min %>" MaxMarks="<%# Item.Max %>" AllowNull="true" ValidationGroup="<%# this.ValidationGroup %>" runat="server" />
				<aspire:HiddenField runat="server" ID="hf" Mode="Encrypted" Value="<%# Item.RegisteredCourse.RegisteredCourseID %>" />
			</td>
			<td>
				<%# Item.RegisteredCourse.StatusHtml %>
			</td>
			<td class="text-center"><%# Item.RegisteredCourse.MarksEntryLocked ? "Locked" : "Unlocked" %></td>
		</tr>
	</ItemTemplate>
	<FooterTemplate>
		</tbody> 
		</table>
	</FooterTemplate>
</asp:Repeater>
<script src='<%="~/Sys/Common/Exam/MarksUserControl.js".ResolveClientUrl(this.Page) %>'></script>
<script type="text/javascript">
	$(function () {
		applyPresentAbsentEvents($("#<%=this.repeater.ClientID%>"));
		$("input.tbMarks").css("min-width", "60px");
		$(window).scrollTop(0);
	});
</script>
