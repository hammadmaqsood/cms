﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExamMarks.ascx.cs" Inherits="Aspire.Web.Sys.Common.Exam.ExamMarks" %>
<%@ Register Src="~/Sys/Common/Exam/CourseInfo.ascx" TagPrefix="uc1" TagName="CourseInfo" %>
<%@ Register Src="~/Sys/Common/Exam/Students.ascx" TagPrefix="uc1" TagName="Students" %>
<%@ Register Src="~/Sys/Common/Exam/GeneratePinCode.ascx" TagPrefix="uc1" TagName="GeneratePinCode" %>

<uc1:CourseInfo runat="server" ID="ucCourseInfo" />
<div class="form-group">
	<aspire:AspireHyperLinkButton runat="server" ID="hlMarksSheet" Glyphicon="list" Text="Marks Sheet" />
	<uc1:GeneratePinCode runat="server" ID="GeneratePinCode" />
</div>
<uc1:Students runat="server" ID="ucStudents" ValidationGroup="Insert" />
<div class="text-center">
	<aspire:AspireButton runat="server" CssClass="pull-left" Text="Save & Submit" ID="btnSaveAndSubmit" ConfirmMessage="You will not be able to update the above record after submit. Are you sure you want to save and submit the marks?" ButtonType="Danger" OnClick="btnSaveAndSubmit_OnClick" ValidationGroup="Insert" />
	<aspire:AspireButton runat="server" Text="Save" ID="btnSave" ButtonType="Primary" OnClick="btnSave_OnClick" ValidationGroup="Insert" />
	<aspire:AspireHyperLink runat="server" Text="Cancel" ID="hlCancel" />
</div>
