﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MarksTextBox.ascx.cs" Inherits="Aspire.Web.Sys.Common.Exam.MarksTextBox" %>
<div class="input-group input-group-sm">
	<span class="input-group-btn" id="spanButtons" runat="server">
		<button type="button" data-toggle="button" tabindex="1" id="btnAbsent" class="btn btn-default">Absent</button>
		<button type="button" data-toggle="button" tabindex="2" id="btnPresent" class="btn btn-default">Present</button>
	</span>
	<aspire:AspireTextBox runat="server" CssClass="tbMarks" MaxLength="8" TabIndex="0" ID="tb" />
	<aspire:Label runat="server" ID="lblTotal" ToolTip="Total Marks" CssClass="input-group-addon" />
</div>
<aspire:AspireDoubleValidator runat="server" ID="dvtb" ControlToValidate="tb" InvalidRangeErrorMessage="Marks obtained can't exceed total marks." RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid Number." />