﻿using Aspire.BL.Core.Exams.Common.New;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Common.Exam
{
	public partial class MarksSheetChangeDetails : System.Web.UI.UserControl
	{
		private new AspireBasePage Page => (AspireBasePage)base.Page;

		private int? OfferedCourseID => this.Page.Request.GetParameterValue<int>(nameof(this.OfferedCourseID));

		private Guid? LeftMarksSheetChangeID => this.Page.Request.GetParameterValue<Guid>(nameof(this.LeftMarksSheetChangeID));

		private Guid? RightMarksSheetChangeID => this.Page.Request.GetParameterValue<Guid>(nameof(this.RightMarksSheetChangeID));

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.OfferedCourseID == null || this.RightMarksSheetChangeID == null)
				{
					CommonMethods.RedirectToHome(this.OfferedCourseID);
					return;
				}

				var changes = MarksHelper.GetMarksSheetChanges(this.OfferedCourseID.Value, 0, int.MaxValue, AspireIdentity.Current.LoginSessionGuid);
				if (changes?.MarksSheetChanges.Any(c => c.MarksSheetChangesID == this.RightMarksSheetChangeID) == true)
				{
					if (this.LeftMarksSheetChangeID == null || changes.Value.MarksSheetChanges.Any(c => c.MarksSheetChangesID == this.LeftMarksSheetChangeID.Value) == true)
					{
						var changeListItems = changes.Value.MarksSheetChanges
							.OrderByDescending(c => c.ActionDate)
							.Select(c => new ListItem($"{c.ActionTypeFullName} @ {c.ActionDate:g} ({c.ChangedBy})", c.MarksSheetChangesID.ToString())).ToArray();
						this.ddlLeftMarksSheetChangesID.DataBind(changeListItems, CommonListItems.Select);
						this.ddlRightMarksSheetChangesID.DataBind(changeListItems);
						this.ddlLeftMarksSheetChangesID.SelectedValue = (this.LeftMarksSheetChangeID ?? this.RightMarksSheetChangeID).ToString();
						this.ddlRightMarksSheetChangesID.SelectedValue = this.RightMarksSheetChangeID.ToString();
						this.ViewState["Changes"] = changes.Value.MarksSheetChanges;
						this.DisplayChanges();
						return;
					}
				}
				this.Page.AddNoRecordFoundAlert();
				CommonMethods.RedirectToHome(this.OfferedCourseID);
			}
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
			var changes = (List<MarksHelper.MarksSheetChange>)this.ViewState["Changes"];
			this.ddlLeftMarksSheetChangesID.Items.Cast<ListItem>().ForEach(i =>
			{
				var change = changes.SingleOrDefault(c => c.MarksSheetChangesID == i.Value.ToNullableGuid());
				i.Attributes["data-ActionType"] = change?.ActionTypeFullName;
				i.Attributes["data-ActionDate"] = change?.ActionDate.ToString("F");
				i.Attributes["data-ChangedBy"] = change?.ChangedBy;
			});
			this.ddlRightMarksSheetChangesID.Items.Cast<ListItem>().ForEach(i =>
			{
				var change = changes.SingleOrDefault(c => c.MarksSheetChangesID == i.Value.ToNullableGuid());
				i.Attributes["data-ActionType"] = change?.ActionTypeFullName;
				i.Attributes["data-ActionDate"] = change?.ActionDate.ToString("F");
				i.Attributes["data-ChangedBy"] = change?.ChangedBy;
			});
		}
		private void DisplayChanges()
		{
			var offeredCourseID = this.OfferedCourseID.Value;
			var leftMarksSheetChangeID = this.ddlLeftMarksSheetChangesID.SelectedValue.ToNullableGuid();
			var rightMarksSheetChangeID = this.ddlRightMarksSheetChangesID.SelectedValue.ToNullableGuid();
			if (rightMarksSheetChangeID == null)
			{
				this.hfOld.Value = null;
				this.hfNew.Value = null;
			}
			else
			{
				var changes = MarksHelper.GetMarksSheetChange(leftMarksSheetChangeID, rightMarksSheetChangeID.Value, offeredCourseID, AspireIdentity.Current.LoginSessionGuid);
				var left = changes.SingleOrDefault(c => c.MarksSheetChangesID == leftMarksSheetChangeID);
				var right = changes.Single(c => c.MarksSheetChangesID == rightMarksSheetChangeID);
				if (this.LeftMarksSheetChangeID == null || this.LeftMarksSheetChangeID == this.RightMarksSheetChangeID)
					this.hfOld.Value = right.OldMarksSheetJSON;
				else
					this.hfOld.Value = left.NewMarksSheetJSON;
				this.hfNew.Value = right.NewMarksSheetJSON;
			}
		}

		protected void ddlLeftMarksSheetChangesID_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.DisplayChanges();
		}

		protected void ddlRightMarksSheetChangesID_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.DisplayChanges();
		}
	}
}