﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MarksSheetChanges.ascx.cs" Inherits="Aspire.Web.Sys.Common.Exam.MarksSheetChanges" %>

<aspire:AspireModal runat="server" ID="modalMarksSheetChanges" HeaderText="Change History" ModalSize="Large">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanelMain" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireGridView runat="server" AutoGenerateColumns="false" ID="gvMarksSheetChanges" ItemType="Aspire.BL.Core.Exams.Common.New.MarksHelper.MarksSheetChange" OnPageSizeChanging="gvMarksSheetChanges_PageSizeChanging" OnPageIndexChanging="gvMarksSheetChanges_PageIndexChanging">
					<Columns>
						<asp:TemplateField HeaderText="Action">
							<ItemTemplate>
								<%#: Item.ActionTypeFullName %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Date">
							<ItemTemplate>
								<%#: Item.ActionDate %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Changed By">
							<ItemTemplate>
								<%#: Item.ChangedBy %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Details">
							<ItemTemplate>
								<aspire:AspireHyperLink runat="server" Text="Details" Target="_blank" NavigateUrl="<%# this.GetMarksSheetChangeDetailsPageUrl(Item.MarksSheetChangesID) %>" />
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</aspire:AspireGridView>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<aspire:AspireModalCloseButton runat="server">Close</aspire:AspireModalCloseButton>
	</FooterTemplate>
</aspire:AspireModal>
