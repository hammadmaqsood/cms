﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CompileExam.ascx.cs" Inherits="Aspire.Web.Sys.Common.Exam.CompileExam" %>
<%@ Register TagPrefix="uc1" TagName="CourseInfo" Src="CourseInfo.ascx" %>
<%@ Register Src="~/Sys/Common/Exam/GeneratePinCode.ascx" TagPrefix="uc1" TagName="GeneratePinCode" %>
<%@ Register Src="~/Sys/Common/Exam/NavigationButton.ascx" TagPrefix="uc1" TagName="NavigationButton" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Web.Common" %>
<%@ Import Namespace="Aspire.Model.Entities" %>
<uc1:CourseInfo runat="server" ID="ucCourseInfo" />
<div class="form-group">
	<aspire:AspireHyperLinkButton ID="hlMarksSheet" runat="server" Text="Marks Sheet" Glyphicon="list" />
	<uc1:GeneratePinCode runat="server" ID="GeneratePinCode" />
</div>
<div class="row">
	<div class="col-md-4" runat="server" id="divButtons">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Compilation Type:" AssociatedControlID="ddlExamCompilationType" />
			<aspire:AspireDropDownList runat="server" ID="ddlExamCompilationType" OnSelectedIndexChanged="ddlExamCompilationType_OnSelectedIndexChanged" AutoPostBack="True" ValidationGroup="SaveMarks" />
			<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamCompilationType" ErrorMessage="This field is required." ValidationGroup="SubmitMarks" />
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<div>
				<aspire:AspireLabel runat="server" Text="&nbsp;" AssociatedControlID="btnSave" />
			</div>
			<aspire:AspireButton ButtonType="Primary" ValidationGroup="SaveMarks" runat="server" ID="btnSave" Text="Save" OnClick="btnSave_OnClick" />
			<aspire:AspireButton ButtonType="Danger" ValidationGroup="SubmitMarks" ConfirmMessage="You will not be able to update this record after submission. Are you sure you want to submit marks?" runat="server" ID="btnSubmit" Text="Submit" OnClick="btnSave_OnClick" />
		</div>
	</div>
</div>
<asp:Repeater runat="server" ID="repeaterRegisteredCourses" ItemType="Aspire.BL.Core.Exams.Common.New.IRegisteredCourse">
	<HeaderTemplate>
		<div class="table-responsive">
			<table class="table table-bordered table-hover table-condensed MarksSheet">
				<thead>
					<tr>
						<th class="text-center" rowspan="2">#</th>
						<th rowspan="2">Enrollment</th>
						<th rowspan="2">Name</th>
						<th rowspan="2">Program</th>
						<th rowspan="2">Student Status</th>
						<th class="Assignments" runat="server" visible="<%# this.CompileExamMarksType == CompileExamMarksTypes.Assignments || this.CompileExamMarksType == CompileExamMarksTypes.Internals %>" colspan="<%# this.Sheet.OfferedCourse.OfferedCourseExamAssignments.Any() ? this.Sheet.OfferedCourse.OfferedCourseExamAssignments.Count : 1 %>">Assignments</th>
						<th class="Quizzes" runat="server" visible="<%# this.CompileExamMarksType == CompileExamMarksTypes.Quizzes || this.CompileExamMarksType == CompileExamMarksTypes.Internals %>" colspan="<%# this.Sheet.OfferedCourse.OfferedCourseExamQuizzes.Any() ? this.Sheet.OfferedCourse.OfferedCourseExamQuizzes.Count : 1 %>">Quizzes</th>
						<th rowspan="2" class="Total" runat="server">
							<%#: this.ddlExamCompilationType.GetSelectedEnumValue<OfferedCours.ExamCompilationTypes>().ToFullName() %>
						</th>
						<th rowspan="2" class="text-center">Status</th>
						<th rowspan="2" class="text-center">Record</th>
					</tr>
					<tr>
						<asp:Repeater runat="server" Visible="<%# this.CompileExamMarksType == CompileExamMarksTypes.Assignments || this.CompileExamMarksType == CompileExamMarksTypes.Internals %>" DataSource="<%# this.Sheet.OfferedCourse.OfferedCourseExamAssignments %>" ItemType="Aspire.BL.Core.Exams.Common.New.IOfferedCourseExam">
							<ItemTemplate>
								<th class="Assignment">
									<uc1:NavigationButton runat="server" IsLocked="<%# Item.Locked %>" CanEdit="<%# Item.CanEditAssignment %>" CanUnlock="false" CommandName="Assignment" ExamNo="<%# Item.ExamNo %>" OfferedCourseExamID="<%# Item.OfferedCourseExamID %>" OfferedCourseID="<%# Item.OfferedCourse.OfferedCourseID %>" />
								</th>
							</ItemTemplate>
						</asp:Repeater>
						<th runat="server" class="Assignment" visible="<%# (this.CompileExamMarksType == CompileExamMarksTypes.Assignments || this.CompileExamMarksType == CompileExamMarksTypes.Internals) && this.Sheet.OfferedCourse.OfferedCourseExamAssignments?.Any() != true %>"></th>
						<asp:Repeater runat="server" Visible="<%# this.CompileExamMarksType == CompileExamMarksTypes.Quizzes || this.CompileExamMarksType == CompileExamMarksTypes.Internals %>" DataSource="<%# this.Sheet.OfferedCourse.OfferedCourseExamQuizzes %>" ItemType="Aspire.BL.Core.Exams.Common.New.IOfferedCourseExam">
							<ItemTemplate>
								<th class="Quiz">
									<uc1:NavigationButton runat="server" IsLocked="<%# Item.Locked %>" CanEdit="<%# Item.CanEditQuiz %>" CanUnlock="false" CommandName="Quiz" ExamNo="<%# Item.ExamNo %>" OfferedCourseExamID="<%# Item.OfferedCourseExamID %>" OfferedCourseID="<%# Item.OfferedCourse.OfferedCourseID %>" />
								</th>
							</ItemTemplate>
						</asp:Repeater>
						<th runat="server" class="Quiz" visible="<%# (this.CompileExamMarksType == CompileExamMarksTypes.Quizzes || this.CompileExamMarksType == CompileExamMarksTypes.Internals) && this.Sheet.OfferedCourse.OfferedCourseExamQuizzes?.Any() != true %>"></th>
					</tr>
				</thead>
				<tbody>
	</HeaderTemplate>
	<ItemTemplate>
		<tr>
			<td class="text-center"><%#: Container.ItemIndex + 1 %></td>
			<td class="text-left"><%#: Item.Enrollment %></td>
			<td class="text-left"><%#: Item.Name %></td>
			<td class="text-left"><%#: Item.ProgramAlias %></td>
			<td class="text-left"><%#: Item.StudentStatusEnum?.ToFullName() %></td>
			<asp:Repeater Visible="<%# this.CompileExamMarksType == CompileExamMarksTypes.Assignments || this.CompileExamMarksType == CompileExamMarksTypes.Internals %>" runat="server" DataSource="<%# Item.OfferedCourseExamAssignments %>" ItemType="Aspire.BL.Core.Exams.Common.New.IExamMarks">
				<ItemTemplate>
					<td class="Assignment"><%#: Item.OfferedCourseExam.OfferedCourse.MarksSheet.Display.GetMarksString(Item.RegisteredCourseExamMark?.ExamMarks, Item.OfferedCourseExam.TotalMarks) %></td>
				</ItemTemplate>
			</asp:Repeater>
			<td runat="server" class="Assignment" visible="<%# (this.CompileExamMarksType == CompileExamMarksTypes.Assignments || this.CompileExamMarksType == CompileExamMarksTypes.Internals) && Item.OfferedCourse.OfferedCourseExamAssignments?.Any() != true %>"></td>
			<td class="Total" runat="server" visible="<%# this.CompileExamMarksType == CompileExamMarksTypes.Assignments %>">
				<%# this.CompileExamMarksType == CompileExamMarksTypes.Assignments ? Item.OfferedCourse.MarksSheet.Display.GetMarksString(Item.GetCompiledAssignmentsMarks(this.ExamCompilationType), Item.AssignmentsTotal) : null %>
			</td>
			<asp:Repeater Visible="<%# this.CompileExamMarksType == CompileExamMarksTypes.Quizzes || this.CompileExamMarksType == CompileExamMarksTypes.Internals %>" runat="server" DataSource="<%# Item.OfferedCourseExamQuizzes %>" ItemType="Aspire.BL.Core.Exams.Common.New.IExamMarks">
				<ItemTemplate>
					<td class="Quiz"><%#: Item.OfferedCourseExam.OfferedCourse.MarksSheet.Display.GetMarksString(Item.RegisteredCourseExamMark?.ExamMarks, Item.OfferedCourseExam.TotalMarks) %></td>
				</ItemTemplate>
			</asp:Repeater>
			<td runat="server" class="Quiz" visible="<%# (this.CompileExamMarksType == CompileExamMarksTypes.Quizzes || this.CompileExamMarksType == CompileExamMarksTypes.Internals) && Item.OfferedCourse.OfferedCourseExamQuizzes?.Any() != true %>"></td>
			<td class="Total" runat="server" visible="<%# this.CompileExamMarksType == CompileExamMarksTypes.Quizzes %>">
				<%# this.CompileExamMarksType == CompileExamMarksTypes.Quizzes ? Item.OfferedCourse.MarksSheet.Display.GetMarksString(Item.GetCompiledQuizzesMarks(this.ExamCompilationType), Item.QuizzesTotal) : null %>
			</td>
			<td class="Total" runat="server" visible="<%# this.CompileExamMarksType == CompileExamMarksTypes.Internals %>">
				<%# this.CompileExamMarksType == CompileExamMarksTypes.Internals ? Item.OfferedCourse.MarksSheet.Display.GetMarksString(Item.GetCompiledInternalsMarks(this.ExamCompilationType), Item.InternalsTotal) : null %>
			</td>
			<td class="text-center"><%# Item.StatusHtml %></td>
			<td class="text-center"><%# Item.MarksEntryLocked ? "Locked" : "Unlocked" %></td>
		</tr>
	</ItemTemplate>
	<FooterTemplate>
		</tbody>
		</table>
	</div>
	</FooterTemplate>
</asp:Repeater>
