﻿function applyPresentAbsentEvents(repeater) {
	var anyMarksFound = false;
	$(".input-group .tbMarks", repeater).each(function (i, t) {
		if ($(t).val().length > 0)
			anyMarksFound = true;
	});
	$(".input-group", repeater).each(function (index, inputGroup) {
		var btnPresent = $("#btnPresent", inputGroup);
		var btnAbsent = $("#btnAbsent", inputGroup);
		var tbMarks = $(".tbMarks", inputGroup);

		$(btnPresent).click(function () {
			$(this).removeClass("btn-default").addClass("btn-success");
			$(btnAbsent).button("toggle");
			$(btnAbsent).removeClass("btn-danger").addClass("btn-default");
			$(tbMarks).prop("readonly", false);
		});
		$(btnAbsent).click(function () {
			$(this).removeClass("btn-default").addClass("btn-danger");
			$(btnPresent).button("toggle");
			$(btnPresent).removeClass("btn-success").addClass("btn-default");
			$(tbMarks).val("").prop("readonly", true);
			$(".text-danger").each(function (i, validator) { ValidatorValidate(validator); });
		});

		if (anyMarksFound) {
			var empty = ((tbMarks.val().length === 0)) ? true : false;
			if (empty) {
				$(btnAbsent).click();
				$(btnPresent).button("toggle");
			}
			else {
				$(btnPresent).click();
				$(btnAbsent).button("toggle");
			}
		}
		else {
			$(btnPresent).click();
			$(btnAbsent).button("toggle");
		}
	});

	$("input.tbMarks", repeater).keypress(function (e) {
		if (e.which === 13)
			$("input.tbMarks", $(this).closest("tr").next()).focus();
	});
}