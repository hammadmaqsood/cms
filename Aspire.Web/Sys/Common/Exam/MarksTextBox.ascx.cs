﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using System;
using System.ComponentModel;

namespace Aspire.Web.Sys.Common.Exam
{
	public partial class MarksTextBox : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		[Bindable(true)]
		public bool EditingAllowed
		{
			get => !this.tb.ReadOnly;
			set
			{
				this.tb.ReadOnly = !value;
				this.dvtb.Enabled = value;
				this.spanButtons.Visible = value;
			}
		}

		[Bindable(true)]
		public decimal? MarksInDB
		{
			get => (decimal?)this.ViewState[nameof(this.MarksInDB)];
			set => this.ViewState[nameof(this.MarksInDB)] = value;
		}

		[Bindable(true)]
		public decimal? MarksNew
		{
			get => this.tb.Text.ToNullableDecimal();
			set => this.tb.Text = value?.ToExamMarksString();
		}

		[Bindable(true)]
		public decimal MinMarks
		{
			get => (decimal)this.dvtb.MinValue;
			set => this.dvtb.MinValue = (double)value;
		}

		[Bindable(true)]
		public decimal MaxMarks
		{
			get => (decimal)this.dvtb.MaxValue;
			set
			{
				this.dvtb.MaxValue = (double)value;
				this.lblTotal.Text = $"/{value.ToExamMarksString()}";
			}
		}

		[Bindable(true)]
		public bool AllowNull
		{
			get => this.dvtb.AllowNull;
			set => this.dvtb.AllowNull = value;
		}

		[Bindable(true)]
		public string ValidationGroup
		{
			get => this.dvtb.ValidationGroup;
			set
			{
				this.dvtb.ValidationGroup = value;
				this.tb.ValidationGroup = value;
			}
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
			if (!this.tb.ReadOnly)
			{
				var inputMask = $"'alias':'marks','min':'{this.MinMarks.ToExamMarksString()}', 'max':'{this.MaxMarks.ToExamMarksString()}'";
				this.tb.Attributes.Add("data-inputmask", inputMask);
			}
		}
	}
}