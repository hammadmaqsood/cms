﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneratePinCode.ascx.cs" Inherits="Aspire.Web.Sys.Common.Exam.GeneratePinCode" %>
<button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#<%=this.modalPinCode.ClientID %>">Enter Pin Code</button>
<aspire:aspiremodal runat="server" id="modalPinCode" headertext="Pin Code">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updatePanelPinCode" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alertPinCode" />
				<div class="form-group">
					<aspire:AspireLabel runat="server" AssociatedControlID="tbPinCode" Text="Enter Pin Code:" />
					<aspire:AspireTextBox runat="server" ValidationGroup="ConfirmPinCode" ID="tbPinCode" MaxLength="8" />
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireButton CssClass="pull-left" runat="server" ButtonType="Danger" FontAwesomeIcon="solid_user_secret" ConfirmMessage="Are you sure you want to generate pin code?" Text="Generate New Pin Code" ValidationGroup="FacultyPinCode" OnClick="btnGeneratePinCode_OnClick" ID="btnGeneratePinCode" />
				<aspire:AspireButton runat="server" Text="Confirm" ID="btnConfirmPinCode" ValidationGroup="ConfirmPinCode" OnClick="btnConfirmPinCode_OnClick" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:aspiremodal>
