﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MarksSheetChangeDetails.ascx.cs" Inherits="Aspire.Web.Sys.Common.Exam.MarksSheetChangeDetails" %>

<asp:HiddenField runat="server" ID="hfOld" />
<asp:HiddenField runat="server" ID="hfNew" />
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<aspire:AspireLabel runat="server" AssociatedControlID="ddlLeftMarksSheetChangesID" Text="Left:" />
			<aspire:AspireDropDownList runat="server" ID="ddlLeftMarksSheetChangesID" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlLeftMarksSheetChangesID_SelectedIndexChanged" />
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<aspire:AspireLabel runat="server" AssociatedControlID="ddlRightMarksSheetChangesID" Text="Right:" />
			<aspire:AspireDropDownList runat="server" ID="ddlRightMarksSheetChangesID" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlRightMarksSheetChangesID_SelectedIndexChanged" />
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<aspire:AspireLabel runat="server" AssociatedControlID="rblDisplayOnlyDifferences" Text="Display Only Differences:" />
			<div>
				<aspire:AspireRadioButtonList runat="server" ID="rblDisplayOnlyDifferences" RepeatDirection="Horizontal" RepeatLayout="Flow">
					<Items>
						<asp:ListItem Text="Yes" Value="10" Selected="True" />
						<asp:ListItem Text="No" Value="2147483647" />
					</Items>
				</aspire:AspireRadioButtonList>
			</div>
		</div>
	</div>
</div>
<div id="diffOutput"></div>
<script src="<%= this.ResolveClientUrl("~/Scripts/Plugins/codediff.js/highlight.min.js") %>"></script>
<script src="<%= this.ResolveClientUrl("~/Scripts/Plugins/codediff.js/difflib.js") %>"></script>
<script src="<%= this.ResolveClientUrl("~/Scripts/Plugins/codediff.js/codediff.js") %>"></script>
<script type="text/javascript">
	function formatState(state) {
		var element = $(state.element);
		if ($(element).attr("value") === "")
			return $("<div></div>").text("Select Change");

		var actionType = $("<div></div>").text(element.attr("data-ActionType"));
		var actionDate = $("<div></div>").text(element.attr("data-ActionDate"));
		var changedBy = $("<div></div>").text(element.attr("data-ChangedBy"));
		return $("<div></div>").append(actionType.css("font-weight", "bold"), actionDate, changedBy);
	};
	$(function () {
		$("#<%=this.ddlLeftMarksSheetChangesID.ClientID%>").add("#<%=this.ddlRightMarksSheetChangesID.ClientID%>").each(function (i, ddl) {
			$(ddl).select2({
				theme: "bootstrap",
				width: "style",
				dropdownParent: $(ddl).parent(),
				templateResult: formatState,
				templateSelection: formatState
			});
		});

		var oldJson = $("#<%=this.hfOld.ClientID%>").val();
		var newJson = $("#<%=this.hfNew.ClientID%>").val();
		function displayDifference() {
			var minJumpSize = $("input[type=\"radio\"]:checked", $("#<%= this.rblDisplayOnlyDifferences.ClientID %>")).val().toInt();
			$(codediff.buildView(oldJson, newJson, {
				language: 'json',
				beforeName: $("#<%=this.ddlLeftMarksSheetChangesID.ClientID%>").val() == $("#<%=this.ddlRightMarksSheetChangesID.ClientID%>").val() ? "Before" : "Left",
				afterName: $("#<%=this.ddlLeftMarksSheetChangesID.ClientID%>").val() == $("#<%=this.ddlRightMarksSheetChangesID.ClientID%>").val() ? "After" : "Right",
				wordWrap: true,
				minJumpSize: minJumpSize
			})).appendTo($("#diffOutput").empty());
		}
		$("input[type=\"radio\"]", $("#<%= this.rblDisplayOnlyDifferences.ClientID %>")).change(displayDifference);
		displayDifference();
	});
</script>
