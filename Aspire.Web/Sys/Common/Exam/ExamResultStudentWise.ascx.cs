﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;
using Aspire.Lib.WebControls;

namespace Aspire.Web.Sys.Common.Exam
{
	public partial class ExamResultStudentWise : System.Web.UI.UserControl
	{
		public new AspireBasePage Page => base.Page as AspireBasePage;
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Reset();
				string enrollment = null;
				var studentID = this.Page.Request.GetParameterValue<int>("StudentID");
				if (studentID != null)
					enrollment = Aspire.BL.Common.Common.GetEnrollmentFromStudentID(studentID.Value);
				if (enrollment == null)
					enrollment = this.Page.Request.GetParameterValue("Enrollment");
				if (!string.IsNullOrWhiteSpace(enrollment))
				{
					this.tbEnrollment.Enrollment = enrollment;
					this.tbEnrollment_OnSearch(null, null);
				}

			}
		}

		private void Reset()
		{
			this.ucStudentInfo.Reset();
			this.panelExamResultStudentWise.Visible = false;
		}

		protected void tbEnrollment_OnSearch(object sender, EventArgs e)
		{
			var enrollment = this.tbEnrollment.Enrollment;
			if (string.IsNullOrWhiteSpace(enrollment))
			{
				this.Reset();
				this.Page.AddNoRecordFoundAlert();
				return;
			}
			var studentinfo = this.ucStudentInfo.LoadBasicStudentInformation(enrollment);
			if (studentinfo == null)
			{
				this.Reset();
				this.Page.AddNoRecordFoundAlert();
				return;
			}
			var reportDataSet = BL.Core.Exams.Common.ExamResults.GetExamResult(studentinfo.StudentID, this.Page.AspireIdentity.LoginSessionGuid);
			var dataSource = reportDataSet.ExamResults.GroupBy(rc => new
			{
				rc.SemesterID,
			}).Select(s => new
			{
				SemesterID = s.Key.SemesterID,
				Semester = s.Key.SemesterID.ToSemesterString(),
				StringGPA = reportDataSet.ExamResults.First(rc => rc.SemesterID == s.Key.SemesterID).GPA?.ToString("0.##") ?? string.Empty.ToNAIfNullOrEmpty(),
				StringCGPA = reportDataSet.ExamResults.First(rc => rc.SemesterID == s.Key.SemesterID).CGPA?.ToString("0.##") ?? string.Empty.ToNAIfNullOrEmpty(),
				StringBUExamGPA = reportDataSet.ExamResults.First(rc => rc.SemesterID == s.Key.SemesterID).BUExamGPA?.ToString("0.##") ?? reportDataSet.ExamResults.First(rc => rc.SemesterID == s.Key.SemesterID).GPA?.ToString("0.##") ?? string.Empty.ToNAIfNullOrEmpty(),
				StringBUExamCGPA = reportDataSet.ExamResults.First(rc => rc.SemesterID == s.Key.SemesterID).BUExamCGPA?.ToString("0.##") ?? reportDataSet.ExamResults.First(rc => rc.SemesterID == s.Key.SemesterID).CGPA?.ToString("0.##") ?? string.Empty.ToNAIfNullOrEmpty(),
				StringBUExamResultRemarks = reportDataSet.ExamResults.First(rc => rc.SemesterID == s.Key.SemesterID).StringBUExamResultRemarks,
				RegisteredCourses = reportDataSet.ExamResults.FindAll(rc => rc.SemesterID == s.Key.SemesterID),
			}).OrderByDescending(s => s.SemesterID).ToList();
			this.repeaterExamResult.DataBind(dataSource);
			switch (this.Page.AspireIdentity.UserType)
			{

				case UserTypes.Executive:
					this.hblPrintResult.Visible = false;
					break;
				case UserTypes.Staff:
					this.hblPrintResult.NavigateUrl = Aspire.Web.Sys.Staff.Exams.Reports.ExamResultStudentWise.GetPageUrl(studentinfo.StudentID, true, ReportFormats.PDF);
					this.hblPrintResult.Visible = true;
					break;
				case UserTypes.Student:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.alertResult.AddMessage(AspireAlert.AlertTypes.Info, "Complete result will be shown after marks entry has been locked.");
			this.panelExamResultStudentWise.Visible = true;
		}
	}
}