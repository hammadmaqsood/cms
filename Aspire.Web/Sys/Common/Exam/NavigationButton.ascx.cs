﻿using Aspire.Lib.Extensions.Exceptions;
using System;
using System.ComponentModel;

namespace Aspire.Web.Sys.Common.Exam
{
	public partial class NavigationButton : System.Web.UI.UserControl
	{
		public enum CommandNames
		{
			Assignment,
			Assignments,
			CompileAssignments,
			Quiz,
			Quizzes,
			CompileQuizzes,
			CompileInternals,
			Mid,
			Final
		}

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		[Bindable(true)]
		public CommandNames CommandName
		{
			get => (CommandNames)this.ViewState[nameof(this.CommandName)];
			set => this.ViewState[nameof(this.CommandName)] = value;
		}

		[Bindable(true)]
		public byte? ExamNo
		{
			get => (byte?)this.ViewState[nameof(this.ExamNo)];
			set => this.ViewState[nameof(this.ExamNo)] = value;
		}

		[Bindable(true)]
		public int OfferedCourseID
		{
			get => (int)this.ViewState[nameof(this.OfferedCourseID)];
			set => this.ViewState[nameof(this.OfferedCourseID)] = value;
		}

		[Bindable(true)]
		public int? OfferedCourseExamID
		{
			get => (int?)this.ViewState[nameof(this.OfferedCourseExamID)];
			set => this.ViewState[nameof(this.OfferedCourseExamID)] = value;
		}

		[Bindable(true)]
		public bool CanEdit
		{
			get => this.ViewState[nameof(this.CanEdit)] as bool? == true;
			set => this.ViewState[nameof(this.CanEdit)] = value;
		}

		[Bindable(true)]
		public bool IsLocked
		{
			get => this.ViewState[nameof(this.IsLocked)] as bool? == true;
			set => this.ViewState[nameof(this.IsLocked)] = value;
		}

		[Bindable(true)]
		public bool CanUnlock
		{
			get => this.ViewState[nameof(this.CanUnlock)] as bool? == true;
			set => this.ViewState[nameof(this.CanUnlock)] = value;
		}

		protected override void OnPreRender(EventArgs e)
		{
			this.btn.Visible = this.CanEdit;
			this.lbl.Visible = !this.CanEdit;
			this.btnDropDown.Visible = this.ulDropDown.Visible = this.IsLocked && this.CanUnlock;
			this.btn.FontAwesomeIcon = this.IsLocked ? Lib.WebControls.FontAwesomeIcons.solid_lock : Lib.WebControls.FontAwesomeIcons.solid_lock_open;
			switch (this.CommandName)
			{
				case CommandNames.Assignment:
					this.lbl.Text = this.btn.Text = $"#{this.ExamNo ?? throw new InvalidOperationException()}";
					this.btn.NavigateUrl = CommonMethods.GetMarksUrl(this.OfferedCourseID, Model.Entities.OfferedCourseExam.ExamTypes.Assignment, this.OfferedCourseExamID);
					break;
				case CommandNames.Assignments:
					this.lbl.Text = "Assignments";
					this.btn.NavigateUrl = CommonMethods.GetExamMarksUrl(this.OfferedCourseID, Model.Entities.Common.ExamMarksTypes.Assignments);
					break;
				case CommandNames.CompileAssignments:
					this.lbl.Text = this.btn.Text = "Total";
					this.btn.NavigateUrl = CommonMethods.GetCompileExamUrl(this.OfferedCourseID, Model.Entities.Common.CompileExamMarksTypes.Assignments);
					break;
				case CommandNames.Quiz:
					this.lbl.Text = this.btn.Text = $"#{this.ExamNo ?? throw new InvalidOperationException()}";
					this.btn.NavigateUrl = CommonMethods.GetMarksUrl(this.OfferedCourseID, Model.Entities.OfferedCourseExam.ExamTypes.Quiz, this.OfferedCourseExamID);
					break;
				case CommandNames.Quizzes:
					this.lbl.Text = this.btn.Text = "Quizzes";
					this.btn.NavigateUrl = CommonMethods.GetExamMarksUrl(this.OfferedCourseID, Model.Entities.Common.ExamMarksTypes.Quizzes);
					break;
				case CommandNames.CompileQuizzes:
					this.lbl.Text = this.btn.Text = "Total";
					this.btn.NavigateUrl = CommonMethods.GetCompileExamUrl(this.OfferedCourseID, Model.Entities.Common.CompileExamMarksTypes.Quizzes);
					break;
				case CommandNames.CompileInternals:
					this.lbl.Text = this.btn.Text = "Internals";
					this.btn.NavigateUrl = CommonMethods.GetCompileExamUrl(this.OfferedCourseID, Model.Entities.Common.CompileExamMarksTypes.Internals);
					break;
				case CommandNames.Mid:
					this.lbl.Text = this.btn.Text = "Mid";
					this.btn.NavigateUrl = CommonMethods.GetExamMarksUrl(this.OfferedCourseID, Model.Entities.Common.ExamMarksTypes.Mid);
					break;
				case CommandNames.Final:
					this.lbl.Text = this.btn.Text = "Final";
					this.btn.NavigateUrl = CommonMethods.GetExamMarksUrl(this.OfferedCourseID, Model.Entities.Common.ExamMarksTypes.Final);
					break;
				default:
					throw new NotImplementedEnumException(this.CommandName);
			}
			base.OnPreRender(e);
		}

		public delegate void OnCommand(object sender, CommandEventArgs e);

		public event OnCommand Command;

		public sealed class CommandEventArgs : EventArgs
		{
			public enum CommandNames
			{
				UnlockAssignment,
				UnlockAssignments,
				UnlockCompileAssignments,
				UnlockQuiz,
				UnlockQuizzes,
				UnlockCompileQuizzes,
				UnlockCompileInternals,
				UnlockMid,
				UnlockFinal
			}

			public int? OfferedCourseExamID { get; }
			public CommandNames CommandName { get; }

			public CommandEventArgs(CommandNames commandName, int? offeredCourseExamID)
			{
				this.OfferedCourseExamID = offeredCourseExamID;
				this.CommandName = commandName;
			}
		}

		protected void btnUnlock_Click(object sender, EventArgs e)
		{
			CommandEventArgs.CommandNames commandName;
			switch (this.CommandName)
			{
				case CommandNames.Assignment:
					commandName = CommandEventArgs.CommandNames.UnlockAssignment;
					break;
				case CommandNames.Assignments:
					commandName = CommandEventArgs.CommandNames.UnlockAssignments;
					break;
				case CommandNames.CompileAssignments:
					commandName = CommandEventArgs.CommandNames.UnlockCompileAssignments;
					break;
				case CommandNames.Quiz:
					commandName = CommandEventArgs.CommandNames.UnlockQuiz;
					break;
				case CommandNames.Quizzes:
					commandName = CommandEventArgs.CommandNames.UnlockQuizzes;
					break;
				case CommandNames.CompileQuizzes:
					commandName = CommandEventArgs.CommandNames.UnlockCompileQuizzes;
					break;
				case CommandNames.CompileInternals:
					commandName = CommandEventArgs.CommandNames.UnlockCompileInternals;
					break;
				case CommandNames.Mid:
					commandName = CommandEventArgs.CommandNames.UnlockMid;
					break;
				case CommandNames.Final:
					commandName = CommandEventArgs.CommandNames.UnlockFinal;
					break;
				default:
					throw new NotImplementedEnumException(this.CommandName);
			}
			this.Command?.Invoke(this, new CommandEventArgs(commandName, this.OfferedCourseExamID));
		}
	}
}