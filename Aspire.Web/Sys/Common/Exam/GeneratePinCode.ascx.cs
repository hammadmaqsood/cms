﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Common.Exam
{
	public partial class GeneratePinCode : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			var facultyIdentity = FacultyIdentity.Current;
			if (facultyIdentity == null || SessionHelper.FacultyMemberPinCode != null)
				this.Visible = false;
			else
			{
#if DEBUG
				if (this.Request.IsLocal)
				{
					SessionHelper.SetFacultyMemberPinCodeVerified("ABCDEFGH");
					this.Visible = false;
				}
#endif
			}
		}

		protected void btnGeneratePinCode_OnClick(object sender, EventArgs e)
		{
			if (SessionHelper.FacultyMemberPinCode != null)
				return;
			var emailAddress = BL.Core.Exams.Faculty.PinCode.GeneratePinCode(FacultyIdentity.Current.LoginSessionGuid);
			if (emailAddress != null)
				this.alertPinCode.AddMessage(AspireAlert.AlertTypes.Success, $"Pin Code has been generated. Please check your email inbox ({emailAddress}). Note: Contact administrator to update your email address if required.");
			else
				this.alertPinCode.AddMessage(AspireAlert.AlertTypes.Error, "Please contact administrator to update your email address.");
			this.tbPinCode.Focus();
			this.updatePanelPinCode.Update();
		}

		protected void btnConfirmPinCode_OnClick(object sender, EventArgs e)
		{
			if (SessionHelper.FacultyMemberPinCode != null)
				return;
			var pinCode = this.tbPinCode.Text.TrimAndMakeItNullIfEmpty();
			if (pinCode != null && pinCode.Length == 8)
			{
				var pinCodeVerified = BL.Core.Exams.Faculty.PinCode.VerifyPinCode(FacultyIdentity.Current.LoginSessionGuid, pinCode);
				if (pinCodeVerified)
				{
					SessionHelper.SetFacultyMemberPinCodeVerified(pinCode);
					PageAlertsController.AddSuccessAlert(null, "Pin Code has been verified.");
					this.Response.Redirect(this.Request.Url.ToString());
					return;
				}
			}
			this.alertPinCode.AddMessage(AspireAlert.AlertTypes.Error, "Pin Code is invalid or expired.");
			this.updatePanelPinCode.Update();
		}
	}
}