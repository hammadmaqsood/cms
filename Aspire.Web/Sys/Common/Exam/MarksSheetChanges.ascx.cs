﻿using Aspire.BL.Core.Exams.Common.New;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Common.Exam
{
	public partial class MarksSheetChanges : System.Web.UI.UserControl
	{
		private new AspireBasePage Page => (AspireBasePage)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.Visible = false;
			}
		}

		private int OfferedCourseID
		{
			get => (int)this.ViewState[nameof(this.OfferedCourseID)];
			set => this.ViewState[nameof(this.OfferedCourseID)] = value;
		}

		public void DisplayChanges(int offeredCourseID)
		{
			this.OfferedCourseID = offeredCourseID;
			this.modalMarksSheetChanges.Show();
			this.Visible = true;
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			var changes = MarksHelper.GetMarksSheetChanges(this.OfferedCourseID, pageIndex, this.gvMarksSheetChanges.PageSize, AspireIdentity.Current.LoginSessionGuid);
			if (changes == null)
			{
				this.Page.AddNoRecordFoundAlert();
				this.modalMarksSheetChanges.Hide();
				this.modalMarksSheetChanges.Visible = false;
				return;
			}
			this.gvMarksSheetChanges.DataBind(changes.Value.MarksSheetChanges, pageIndex, changes.Value.VirtualItemCount);
		}

		protected void gvMarksSheetChanges_PageSizeChanging(object sender, Lib.WebControls.AspireGridView.PageSizeEventArgs e)
		{
			this.gvMarksSheetChanges.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvMarksSheetChanges_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected string GetMarksSheetChangeDetailsPageUrl(Guid marksSheetChangeID)
		{
			switch (AspireIdentity.Current.UserType)
			{
				case Model.Entities.Common.UserTypes.Staff:
					return Staff.Exams.MarksSheetChangeDetails.GetPageUrl(this.OfferedCourseID, null, marksSheetChangeID);
				case Model.Entities.Common.UserTypes.Faculty:
					return Faculty.Exams.MarksSheetChangeDetails.GetPageUrl(this.OfferedCourseID, null, marksSheetChangeID);
				default:
					throw new NotImplementedEnumException(AspireIdentity.Current?.UserType);
			}
		}
	}
}