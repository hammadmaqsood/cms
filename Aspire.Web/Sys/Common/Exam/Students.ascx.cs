﻿using Aspire.BL.Core.Exams.Common.New;
using Aspire.Lib.Extensions;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Common.Exam
{
	public partial class Students : System.Web.UI.UserControl
	{
		private new AspireBasePage Page => (AspireBasePage)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		[DefaultValue("")]
		public string ValidationGroup
		{
			get => this.ViewState[nameof(this.ValidationGroup)] as string;
			set => this.ViewState[nameof(this.ValidationGroup)] = value;
		}

		public void DataBind(IEnumerable<IStudentMarks> students)
		{
			this.repeater.DataSource = students.OrderBy(s => s.RegisteredCourse.Enrollment);
			this.repeater.DataBind();
		}

		public IReadOnlyDictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)> GetMarks()
		{
			var marks = new Dictionary<int, (decimal? marksObtainedInDB, decimal? marksObtainedNew)>();
			foreach (var item in this.repeater.Items.Cast<RepeaterItem>().Where(i => i.ItemType == ListItemType.Item || i.ItemType == ListItemType.AlternatingItem))
			{
				var registeredCourseID = ((Lib.WebControls.HiddenField)item.FindControl("hf")).Value.ToInt();
				var ucMarks = (MarksTextBox)item.FindControl("ucMarks");
				var marksObtainedInDB = ucMarks.MarksInDB;
				var marksObtainedNew = ucMarks.MarksNew;
				marks.Add(registeredCourseID, (marksObtainedInDB, marksObtainedNew));
			}
			return marks;
		}
	}
}