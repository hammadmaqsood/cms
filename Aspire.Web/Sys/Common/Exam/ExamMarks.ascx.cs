﻿using Aspire.BL.Core.Exams.Common.New;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Common.Exam
{
	public partial class ExamMarks : System.Web.UI.UserControl, IAlert
	{
		private new AspireBasePage Page => (AspireBasePage)base.Page;
		private int? OfferedCourseID => this.Page.GetParameterValue<int>(nameof(this.OfferedCourseID));
		private ExamMarksTypes? ExamMarksType => this.Page.GetParameterValue<ExamMarksTypes>(nameof(this.ExamMarksType));
		private IMarksSheet _sheet;
		private IMarksSheet Sheet => this._sheet ?? (this._sheet = CommonMethods.GetValidatedMarksSheet(this.Page));

		private void RefreshPage()
		{
			this.Response.Redirect(CommonMethods.GetExamMarksUrl(this.OfferedCourseID, this.ExamMarksType ?? throw new InvalidOperationException()));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.Sheet == null)
					throw new InvalidOperationException();
				if (this.ExamMarksType == null)
				{
					CommonMethods.RedirectToMarksSheet(this.Sheet.OfferedCourse.OfferedCourseID);
					return;
				}

				IReadOnlyList<IStudentMarks> marks;
				switch (this.ExamMarksType.Value)
				{
					case ExamMarksTypes.Assignments:
						if (!this.Sheet.CanEditAssignments)
						{
							this.AddErrorAlerts(false, this.Sheet.CanNotEditAssignmentsReasons);
							CommonMethods.RedirectToMarksSheet(this.Sheet);
							return;
						}
						marks = this.Sheet.GetAssignmentsMarks();
						break;
					case ExamMarksTypes.Quizzes:
						if (!this.Sheet.CanEditQuizzes)
						{
							this.AddErrorAlerts(false, this.Sheet.CanNotEditQuizzesReasons);
							CommonMethods.RedirectToMarksSheet(this.Sheet);
							return;
						}
						marks = this.Sheet.GetQuizzesMarks();
						break;
					case ExamMarksTypes.Mid:
						if (!this.Sheet.CanEditMid)
						{
							this.AddErrorAlerts(false, this.Sheet.CanNotEditMidReasons);
							CommonMethods.RedirectToMarksSheet(this.Sheet);
							return;
						}
						marks = this.Sheet.GetMidMarks();
						break;
					case ExamMarksTypes.Final:
						if (!this.Sheet.CanEditFinal)
						{
							this.AddErrorAlerts(false, this.Sheet.CanNotEditFinalReasons);
							CommonMethods.RedirectToMarksSheet(this.Sheet);
							return;
						}
						marks = this.Sheet.GetFinalMarks();
						break;
					case ExamMarksTypes.Internals:
						throw new InvalidOperationException();
					default:
						throw new NotImplementedEnumException(this.ExamMarksType);
				}

				this.ucCourseInfo.SetCourseInfo(this.Sheet);
				this.hlMarksSheet.NavigateUrl = CommonMethods.GetMarksSheetUrl(this.Sheet);
				this.hlCancel.NavigateUrl = this.hlMarksSheet.NavigateUrl;
				this.ucStudents.DataBind(marks.OrderBy(m => m.RegisteredCourse.Enrollment));

				if (this.Page.AspireIdentity.UserType.IsFaculty())
				{
					this.btnSave.Text = "Save";
					this.btnSaveAndSubmit.Visible = true;
				}
				else
				{
					this.btnSave.Text = "Save & Submit";
					this.btnSaveAndSubmit.Visible = false;
				}
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			this.SaveMarks(this.Page.AspireIdentity.UserType.IsStaff());
		}

		protected void btnSaveAndSubmit_OnClick(object sender, EventArgs e)
		{
			this.SaveMarks(true);
		}

		private void SaveMarks(bool submit)
		{
			if (!CommonMethods.IsPinCodeVerified())
				return;
			if (!this.Page.IsValid || this.OfferedCourseID == null || this.ExamMarksType == null)
				return;
			var marks = this.ucStudents.GetMarks();

			var result = MarksHelper.UpdateExamMarks(this.OfferedCourseID.Value, this.ExamMarksType.Value, marks, submit, this.Page.AspireIdentity.LoginSessionGuid);
			if (result == null)
			{
				this.Page.AddNoRecordFoundAlert();
				CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
				return;
			}
			if (result.Value.success)
			{
				if (submit)
				{
					this.AddSuccessAlert($"{this.ExamMarksType.Value.ToFullName()} marks have been saved and submitted.");
					CommonMethods.RedirectToMarksSheet(this.OfferedCourseID.Value);
				}
				else
				{
					this.AddSuccessAlert($"{this.ExamMarksType.Value.ToFullName()} marks have been saved.");
					this.RefreshPage();
				}
				return;
			}
			if (result.Value.updateFailureReasons.Any())
			{
				this.AddErrorAlerts(false, result.Value.updateFailureReasons);
				return;
			}
			throw new InvalidOperationException();
		}
	}
}