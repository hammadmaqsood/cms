﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.ComponentModel;

namespace Aspire.Web.Sys.Common.Exam
{
	public partial class ExamNavigateButton : System.Web.UI.UserControl, IAlert
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		[Bindable(true)]
		public OfferedCourseExam.ExamTypes? ExamType
		{
			get => (OfferedCourseExam.ExamTypes?)this.ViewState[nameof(this.ExamType)];
			set => this.ViewState[nameof(this.ExamType)] = value;
		}

		[Bindable(true)]
		public ExamMarksTypes? ExamMarksType
		{
			get => (ExamMarksTypes?)this.ViewState[nameof(this.ExamMarksType)];
			set => this.ViewState[nameof(this.ExamMarksType)] = value;
		}

		[Bindable(true)]
		public int OfferedCourseID
		{
			get => (int)this.ViewState[nameof(this.OfferedCourseID)];
			set => this.ViewState[nameof(this.OfferedCourseID)] = value;
		}

		[Bindable(true)]
		public int? OfferedCourseExamID
		{
			get => (int?)this.ViewState[nameof(this.OfferedCourseExamID)];
			set => this.ViewState[nameof(this.OfferedCourseExamID)] = value;
		}

		[Bindable(true)]
		public byte? ExamNo
		{
			get => (byte?)this.ViewState[nameof(this.ExamNo)];
			set => this.ViewState[nameof(this.ExamNo)] = value;
		}

		private bool IsStaff => AspireIdentity.Current.UserType.IsStaff();

		[Bindable(true)]
		public bool MarksEntryLocked
		{
			get => (bool)this.ViewState[nameof(this.MarksEntryLocked)];
			set => this.ViewState[nameof(this.MarksEntryLocked)] = value;
		}

		[Bindable(true)]
		public bool Submitted
		{
			get => (bool)this.ViewState[nameof(this.Submitted)];
			set => this.ViewState[nameof(this.Submitted)] = value;
		}

		[Bindable(true)]
		public bool Compilation
		{
			get => (bool)this.ViewState[nameof(this.Compilation)];
			set => this.ViewState[nameof(this.Compilation)] = value;
		}

		private void DoSettings()
		{
			if (this.MarksEntryLocked)
			{
				this.btn.Visible = false;
				this.lbl.Visible = true;
				this.btnDropDown.Visible = this.ulDropDown.Visible = false;
			}
			else
			{
				if (this.IsStaff)
				{
					if (this.Submitted)
					{
						this.btn.Visible = true;
						this.lbl.Visible = false;
						this.btnDropDown.Visible = this.ulDropDown.Visible = true;
						this.btn.FontAwesomeIcon = FontAwesomeIcons.solid_lock;
					}
					else
					{
						this.btn.Visible = true;
						this.lbl.Visible = false;
						this.btnDropDown.Visible = this.ulDropDown.Visible = false;
						this.btn.FontAwesomeIcon = FontAwesomeIcons.solid_unlock;
					}
				}
				else
				{
					this.btnDropDown.Visible = this.ulDropDown.Visible = false;
					if (this.Submitted)
					{
						this.btn.Visible = false;
						this.lbl.Visible = true;
					}
					else
					{
						this.btn.Visible = true;
						this.lbl.Visible = false;
					}
				}
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			switch (this.ExamType)
			{
				case OfferedCourseExam.ExamTypes.Assignment:
				case OfferedCourseExam.ExamTypes.Quiz:
					this.btn.Text = this.lbl.Text = $"{this.ExamNo ?? throw new InvalidOperationException()}";
					//this.btn.NavigateUrl = CommonMethods.GetMarksUrl(this.OfferedCourseID, this.OfferedCourseExamID ?? throw new InvalidOperationException(), this.ExamType);
					this.DoSettings();
					return;
				case null:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			switch (this.ExamMarksType ?? throw new InvalidOperationException())
			{
				case ExamMarksTypes.Assignments:
					this.btn.Text = this.lbl.Text = this.Compilation ? "Total" : this.ExamMarksType.Value.ToFullName();
					this.btn.NavigateUrl = this.Compilation
						? CommonMethods.GetCompileExamUrl(this.OfferedCourseID, CompileExamMarksTypes.Assignments)
						: CommonMethods.GetExamMarksUrl(this.OfferedCourseID, this.ExamMarksType.Value);
					break;
				case ExamMarksTypes.Quizzes:
					this.btn.Text = this.lbl.Text = this.Compilation ? "Total" : this.ExamMarksType.Value.ToFullName();
					this.btn.NavigateUrl = this.Compilation
						? CommonMethods.GetCompileExamUrl(this.OfferedCourseID, CompileExamMarksTypes.Quizzes)
						: CommonMethods.GetExamMarksUrl(this.OfferedCourseID, this.ExamMarksType.Value);
					break;
				case ExamMarksTypes.Mid:
					this.btn.Text = this.lbl.Text = this.ExamMarksType.Value.ToFullName();
					this.btn.NavigateUrl = this.Compilation
						? throw new InvalidOperationException()
						: CommonMethods.GetExamMarksUrl(this.OfferedCourseID, this.ExamMarksType.Value);
					break;
				case ExamMarksTypes.Final:
					this.btn.Text = this.lbl.Text = this.ExamMarksType.Value.ToFullName();
					this.btn.NavigateUrl = this.Compilation
						? throw new InvalidOperationException()
						: CommonMethods.GetExamMarksUrl(this.OfferedCourseID, this.ExamMarksType.Value);
					break;
				case ExamMarksTypes.Internals:
					this.btn.Text = this.lbl.Text = this.ExamMarksType.Value.ToFullName();
					this.btn.NavigateUrl = this.Compilation
						? CommonMethods.GetCompileExamUrl(this.OfferedCourseID, CompileExamMarksTypes.Internals)
						: throw new InvalidOperationException();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.DoSettings();
			base.OnPreRender(e);
		}

		private void ProcessMessage(BL.Core.Exams.Common.MarksSheet.Message message, int? offeredCourseID, string successMessage)
		{
			if (message == null)
			{
				this.AddNoRecordFoundAlert();
				CommonMethods.RedirectToHome(offeredCourseID);
			}
			else if (message.Success)
			{
				this.AddSuccessAlert(successMessage);
				CommonMethods.RedirectToHome(offeredCourseID);
			}
			else
			{
				this.AddErrorAlert(message.ErrorMessage);
				switch (message.Redirection)
				{
					case BL.Core.Exams.Common.MarksSheet.Message.Redirections.None:
						break;
					case BL.Core.Exams.Common.MarksSheet.Message.Redirections.RefreshPage:
					case BL.Core.Exams.Common.MarksSheet.Message.Redirections.MarksSheet:
						CommonMethods.RedirectToHome(offeredCourseID);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		protected void btnUnlock_OnClick(object sender, EventArgs e)
		{
			BL.Core.Exams.Common.MarksSheet.Message message;
			if (this.OfferedCourseExamID != null)
			{
				message = BL.Core.Exams.Common.OfferedCourse.UnlockMarksEntry(this.OfferedCourseID, this.OfferedCourseExamID.Value, AspireIdentity.Current.LoginSessionGuid);
				switch (this.ExamType ?? throw new InvalidOperationException())
				{
					case OfferedCourseExam.ExamTypes.Assignment:
						this.ProcessMessage(message, this.OfferedCourseID, $"Assignment marks have been unlocked.");
						return;
					case OfferedCourseExam.ExamTypes.Quiz:
						this.ProcessMessage(message, this.OfferedCourseID, $"Quiz marks have been unlocked.");
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}

			var examMarksType = this.ExamMarksType ?? throw new InvalidOperationException();
			message = BL.Core.Exams.Common.OfferedCourse.UnlockMarksEntry(this.OfferedCourseID, examMarksType, AspireIdentity.Current.LoginSessionGuid);
			switch (examMarksType)
			{
				case ExamMarksTypes.Assignments:
					this.ProcessMessage(message, this.OfferedCourseID, $"Assignments marks have been unlocked.");
					break;
				case ExamMarksTypes.Quizzes:
					this.ProcessMessage(message, this.OfferedCourseID, $"Quizzes marks have been unlocked.");
					break;
				case ExamMarksTypes.Mid:
					this.ProcessMessage(message, this.OfferedCourseID, $"Mid marks have been unlocked.");
					break;
				case ExamMarksTypes.Final:
					this.ProcessMessage(message, this.OfferedCourseID, $"Final marks have been unlocked.");
					break;
				case ExamMarksTypes.Internals:
					this.ProcessMessage(message, this.OfferedCourseID, $"Internals marks have been unlocked.");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}