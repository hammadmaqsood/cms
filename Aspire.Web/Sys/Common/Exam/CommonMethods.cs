﻿using Aspire.BL.Core.Exams.Common.New;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Common.Exam
{
	public static class CommonMethods
	{
		private static void RedirectToCourses()
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					BasePage.Redirect<Staff.Exams.Courses>();
					return;
				case UserTypes.Faculty:
					BasePage.Redirect<Faculty.Courses>();
					return;
				default:
					throw new NotImplementedEnumException(AspireIdentity.Current.UserType);
			}
		}

		public static IMarksSheet GetValidatedMarksSheet(AspireBasePage page)
		{
			var offeredCourseID = page.Request.GetParameterValue<int>("OfferedCourseID");
			if (offeredCourseID == null)
			{
				RedirectToCourses();
				return null;
			}
			var result = MarksHelper.GetMarksSheet(offeredCourseID.Value, AspireIdentity.Current.LoginSessionGuid);
			if (result == null)
			{
				RedirectToCourses();
				return null;
			}
			if (result.Value.errorMessage != null)
			{
				page.AddErrorAlert(result.Value.errorMessage);
				RedirectToCourses();
				return null;
			}
			if (result.Value.marksSheet == null)
				throw new InvalidOperationException();

			var (errors, warnings) = result.Value.marksSheet.Validate();
			if (errors.Any())
			{
				if (AspireIdentity.Current.UserType != UserTypes.Staff)
					page.AddErrorAlert("Please contact Exam Section for the following errors/warnings.");
				errors.ForEach(error => page.AddErrorAlert(error));
				warnings?.ForEach(warning => page.AddWarningAlert(warning));
				RedirectToCourses();
				return null;
			}
			warnings?.ForEach(warning => page.AddWarningAlert(warning));
			return result.Value.marksSheet;
		}

		public static string GetMarksSheetUrl(int offeredCourseID)
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					return Staff.Exams.MarksSheet.GetPageUrl(offeredCourseID);
				case UserTypes.Faculty:
					return Faculty.Exams.MarksSheet.GetPageUrl(offeredCourseID);
				default:
					throw new NotImplementedEnumException(AspireIdentity.Current.UserType);
			}
		}

		public static void RedirectToMarksSheet(int offeredCourseID)
		{
			BasePage.Redirect(GetMarksSheetUrl(offeredCourseID));
		}

		public static string GetMarksSheetUrl(IMarksSheet marksSheet)
		{
			return GetMarksSheetUrl(marksSheet.OfferedCourse.OfferedCourseID);
		}

		public static void RedirectToMarksSheet(IMarksSheet marksSheet)
		{
			RedirectToMarksSheet(marksSheet.OfferedCourse.OfferedCourseID);
		}

		public static void RedirectToHome(int? offeredCourseID)
		{
			if (offeredCourseID == null)
				RedirectToCourses();
			else
				RedirectToMarksSheet(offeredCourseID.Value);
		}

		public static string GetMarksUrl(int offeredCourseID, Model.Entities.OfferedCourseExam.ExamTypes examTypeEnum, int? offeredCourseExamID)
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					return Staff.Exams.Marks.GetPageUrl(offeredCourseID, examTypeEnum, offeredCourseExamID);
				case UserTypes.Faculty:
					return Faculty.Exams.Marks.GetPageUrl(offeredCourseID, examTypeEnum, offeredCourseExamID);
				default:
					throw new NotImplementedEnumException(AspireIdentity.Current.UserType);
			}
		}

		public static string GetExamMarksUrl(int offeredCourseID, ExamMarksTypes examMarksType)
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					return Staff.Exams.ExamMarks.GetPageUrl(offeredCourseID, examMarksType);
				case UserTypes.Faculty:
					return Faculty.Exams.ExamMarks.GetPageUrl(offeredCourseID, examMarksType);
				default:
					throw new NotImplementedEnumException(AspireIdentity.Current.UserType);
			}
		}

		public static string GetCompileExamUrl(int offeredCourseID, CompileExamMarksTypes compileExamMarksType)
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					return Staff.Exams.CompileExam.GetPageUrl(offeredCourseID, compileExamMarksType);
				case UserTypes.Faculty:
					return Faculty.Exams.CompileExam.GetPageUrl(offeredCourseID, compileExamMarksType);
				default:
					throw new NotImplementedEnumException(AspireIdentity.Current.UserType);
			}
		}

		public static string GetExamMarksUrl(int? offeredCourseID, ExamMarksTypes? examMarksType)
		{
			if (offeredCourseID == null)
				throw new ArgumentNullException(nameof(offeredCourseID));
			if (examMarksType == null)
				throw new ArgumentNullException(nameof(examMarksType));
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					return Staff.Exams.ExamMarks.GetPageUrl(offeredCourseID.Value, examMarksType.Value);
				case UserTypes.Faculty:
					return Faculty.Exams.ExamMarks.GetPageUrl(offeredCourseID.Value, examMarksType.Value);
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static string GetAwardListUrl(int offeredCourseID)
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					return Staff.Exams.Reports.AwardList.GetPageUrl(offeredCourseID, false, null, null);
				case UserTypes.Faculty:
					return Faculty.Exams.Reports.AwardList.GetPageUrl(offeredCourseID, null, null);
				default:
					throw new NotImplementedEnumException(AspireIdentity.Current.UserType);
			}
		}

		public static void RenderAwardListReport<T>(ReportViewer reportViewer, int offeredCourseID) where T : BasePage
		{
			var aspireBasePage = (AspireBasePage)reportViewer.Page;
			var reportFileName = "~/Reports/Exams/AwardList/AwardLists.rdlc".MapPath();
			var result = MarksHelper.GetAwardList(offeredCourseID, AspireIdentity.Current.LoginSessionGuid);
			if (result == null)
			{
				aspireBasePage.AddNoRecordFoundAlert();
				BasePage.Redirect<T>();
				return;
			}
			if (result.Value.errors != null)
			{
				result.Value.errors.ForEach(e => aspireBasePage.AddErrorAlert(e));
				BasePage.Redirect<T>();
				return;
			}

			var iReportViewer = (IReportViewer)aspireBasePage;
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = iReportViewer.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("AwardListRows", result.Value.awardListRows));
			reportViewer.LocalReport.EnableExternalImages = true;
		}

		public static bool IsPinCodeVerified()
		{
			if (FacultyIdentity.Current == null || SessionHelper.FacultyMemberPinCode != null)
				return true;
			PageAlertsController.AddErrorAlert(null, "Pin code verification is required for exam marks entry.");
			return false;
		}
	}
}