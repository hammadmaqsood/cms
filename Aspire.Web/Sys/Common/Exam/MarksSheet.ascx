﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MarksSheet.ascx.cs" Inherits="Aspire.Web.Sys.Common.Exam.MarksSheet" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Web.Sys.Common.Exam" %>
<%@ Register Src="CourseInfo.ascx" TagPrefix="uc1" TagName="CourseInfo" %>
<%@ Register Src="~/Sys/Common/Exam/GeneratePinCode.ascx" TagPrefix="uc1" TagName="GeneratePinCode" %>
<%@ Register Src="~/Sys/Common/Exam/NavigationButton.ascx" TagPrefix="uc1" TagName="NavigationButton" %>
<%@ Register Src="~/Sys/Common/Exam/MarksSheetChanges.ascx" TagPrefix="uc1" TagName="MarksSheetChanges" %>

<uc1:CourseInfo runat="server" ID="ucCourseInfo" />
<asp:Repeater runat="server" ID="repeaterMarksSheet" ItemType="Aspire.BL.Core.Exams.Common.New.IMarksSheet" OnItemCommand="repeaterMarksSheet_ItemCommand">
	<ItemTemplate>
		<div class="form-group">
			<div class="btn-group btn-group-sm" role="group">
				<aspire:AspireHyperLinkButton runat="server" ButtonType="Success" Glyphicon="plus" reasons="<%# Item.Options.CanNotAddAssignmentReasons %>" Enabled="<%# Item.Options.CanAddAssignment %>" Visible="<%# Item.Options.AddAssignment %>" NavigateUrl="<%# GetPageUrl(PageURLs.AddAssignment, Item.OfferedCourse.OfferedCourseID, null) %>" Text="Add Assignment" />
				<aspire:AspireHyperLinkButton runat="server" ButtonType="Success" Glyphicon="plus" reasons="<%# Item.Options.CanNotAddQuizReasons %>" Enabled="<%# Item.Options.CanAddQuiz %>" Visible="<%# Item.Options.AddQuiz %>" NavigateUrl="<%# GetPageUrl(PageURLs.AddQuiz, Item.OfferedCourse.OfferedCourseID, null) %>" Text="Add Quiz" />
			</div>
			<div class="btn-group btn-group-sm" role="group">
				<aspire:AspireHyperLinkButton runat="server" ButtonType="Default" Glyphicon="edit" reasons="<%# Item.Options.CanNotEditAssignmentsReasons %>" Enabled="<%# Item.Options.CanEditAssignments %>" Visible="<%# Item.Options.Assignments %>" NavigateUrl="<%# GetPageUrl(PageURLs.EditAssignments, Item.OfferedCourse.OfferedCourseID, null) %>" Text="Assignments" />
				<aspire:AspireHyperLinkButton runat="server" ButtonType="Default" Glyphicon="edit" reasons="<%# Item.Options.CanNotEditQuizzesReasons %>" Enabled="<%# Item.Options.CanEditQuizzes %>" Visible="<%# Item.Options.Quizzes %>" NavigateUrl="<%# GetPageUrl(PageURLs.EditQuizzes, Item.OfferedCourse.OfferedCourseID, null) %>" Text="Quizzes" />
				<aspire:AspireHyperLinkButton runat="server" ButtonType="Default" Glyphicon="edit" reasons="<%# Item.Options.CanNotEditMidReasons %>" Enabled="<%# Item.Options.CanEditMid %>" Visible="<%# Item.Options.Mid %>" NavigateUrl="<%# GetPageUrl(PageURLs.EditMid, Item.OfferedCourse.OfferedCourseID, null) %>" Text="Mid" />
				<aspire:AspireHyperLinkButton runat="server" ButtonType="Default" FontAwesomeIcon="solid_cogs" reasons="<%# Item.Options.CanNotCompileAssignmentsReasons %>" Enabled="<%# Item.Options.CanCompileAssignments %>" Visible="<%# Item.Options.CompileAssignments %>" NavigateUrl="<%# GetPageUrl(PageURLs.CompileAssignments, Item.OfferedCourse.OfferedCourseID, null) %>" Text="Compile Assignments" />
				<aspire:AspireHyperLinkButton runat="server" ButtonType="Default" FontAwesomeIcon="solid_cogs" reasons="<%# Item.Options.CanNotCompileQuizzesReasons %>" Enabled="<%# Item.Options.CanCompileQuizzes %>" Visible="<%# Item.Options.CompileQuizzes %>" NavigateUrl="<%# GetPageUrl(PageURLs.CompileQuizzes, Item.OfferedCourse.OfferedCourseID, null) %>" Text="Compile Quizzes" />
				<aspire:AspireHyperLinkButton runat="server" ButtonType="Default" FontAwesomeIcon="solid_cogs" reasons="<%# Item.Options.CanNotCompileInternalsReasons %>" Enabled="<%# Item.Options.CanCompileInternals %>" Visible="<%# Item.Options.CompileInternals %>" NavigateUrl="<%# GetPageUrl(PageURLs.CompileInternals, Item.OfferedCourse.OfferedCourseID, null) %>" Text="Compile Internals" />
				<aspire:AspireHyperLinkButton runat="server" ButtonType="Default" Glyphicon="edit" reasons="<%# Item.Options.CanNotEditFinalReasons %>" Enabled="<%# Item.Options.CanEditFinal %>" Visible="<%# Item.Options.Final %>" NavigateUrl="<%# GetPageUrl(PageURLs.EditFinal, Item.OfferedCourse.OfferedCourseID, null) %>" Text="Final" />
				<aspire:AspireButton runat="server" ButtonType="Default" FontAwesomeIcon="solid_cogs" reasons="<%# Item.Options.CanNotCompileTotalAndGradesReasons %>" Enabled="<%# Item.Options.CanCompileTotalAndGrades %>" CommandName="CompileGrades" Text="Compile Grades" CausesValidation="False" />
			</div>
			<aspire:AspireButton runat="server" ButtonSize="Small" ButtonType="Danger" FontAwesomeIcon="solid_lock" reasons="<%# Item.Options.CanNotLockMarksEntryReasons %>" Enabled="<%# Item.Options.CanLockMarksEntry %>" Visible="<%# Item.Options.LockMarksEntry %>" CommandName="LockMarksEntry" Text="Lock Marks Entry" ConfirmMessage="After locking marks entry you will not be able to update it. Are you sure you want to lock marks entry?" CausesValidation="False" />
			<aspire:AspireButton runat="server" ButtonSize="Small" ButtonType="Danger" FontAwesomeIcon="solid_lock_open" reasons="<%# Item.Options.CanNotUnlockMarksEntryReasons %>" Enabled="<%# Item.Options.CanUnlockMarksEntry %>" Visible="<%# Item.Options.UnlockMarksEntry %>" CommandName="UnlockMarksEntry" Text="Unlock Marks Entry" ConfirmMessage="Are you sure you want to unlock Marks Entry?" CausesValidation="False" />
			<div class="btn-group btn-group-sm" runat="server" visible="<%# Item.Options.SubmitToHOD || Item.Options.SubmitToCampus || Item.Options.SubmitToUniversity %>">
				<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Submit <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<li runat="server" visible="<%# Item.Options.SubmitToHOD %>">
						<aspire:LinkButton reasons="<%# Item.Options.CanNotSubmitToHODReasons %>" Enabled="<%# Item.Options.CanSubmitToHOD %>" CommandName="SubmitToHOD" runat="server" Text="Submit To HoD" ConfirmMessage="I certify that I have checked the result (marks and grades) and found them correct." CausesValidation="False" />
					</li>
					<li runat="server" visible="<%# Item.Options.SubmitToCampus %>">
						<aspire:LinkButton reasons="<%# Item.Options.CanNotSubmitToCampusReasons %>" Enabled="<%# Item.Options.CanSubmitToCampus %>" CommandName="SubmitToCampus" runat="server" Text="Submit To Campus" ConfirmMessage="Are you sure you want to submit marks to Campus?" CausesValidation="False" />
					</li>
					<li runat="server" visible="<%# Item.Options.SubmitToUniversity %>">
						<aspire:LinkButton reasons="<%# Item.Options.CanNotSubmitToUniversityReasons %>" Enabled="<%# Item.Options.CanSubmitToUniversity %>" CommandName="SubmitToUniversity" runat="server" Text="Submit to BUHO Exams" ConfirmMessage="Are you sure you want to submit marks to BUHO Exams?" CausesValidation="False" />
					</li>
					<li runat="server" visible="<%# Item.CanRevertSubmitToHOD %>">
						<aspire:LinkButton reasons="<%# Item.Options.CanNotRevertSubmitToHODReasons %>" Enabled="<%# Item.Options.CanRevertSubmitToHOD %>" CommandName="RevertSubmitToHOD" runat="server" Text="Revert To Faculty" ConfirmMessage="Are you sure you want to revert marks submission back to Faculty member from HOD?" CausesValidation="False" />
					</li>
					<li runat="server" visible="<%# Item.CanRevertSubmitToCampus %>">
						<aspire:LinkButton reasons="<%# Item.Options.CanNotRevertSubmitToCampusReasons %>" Enabled="<%# Item.Options.CanRevertSubmitToCampus %>" CommandName="RevertSubmitToCampus" runat="server" Text="Revert To HOD" ConfirmMessage="Are you sure you want to revert marks submission back to HOD/Faculty member from Campus?" CausesValidation="False" />
					</li>
					<li runat="server" visible="<%# Item.CanRevertSubmitToUniversity %>">
						<aspire:LinkButton reasons="<%# Item.Options.CanNotRevertSubmitToUniversityReasons %>" Enabled="<%# Item.Options.CanRevertSubmitToUniversity %>" CommandName="RevertSubmitToUniversity" runat="server" Text="Revert To Campus" ConfirmMessage="Are you sure you want to revert marks submission back to Campus/HOD/Faculty Member from Campus?" CausesValidation="False" />
					</li>
				</ul>
			</div>
			<aspire:AspireButton runat="server" ButtonSize="Small" ButtonType="Info" FontAwesomeIcon="solid_history" CommandName="History" Text="Change History" CausesValidation="False" />
			<aspire:AspireHyperLinkButton runat="server" CssClass="btn-sm" ButtonType="Default" Target="_blank" FontAwesomeIcon="solid_file_pdf" NavigateUrl="<%# CommonMethods.GetAwardListUrl(Item.OfferedCourse.OfferedCourseID) %>" Text="Award List" />
			<uc1:GeneratePinCode runat="server" />
		</div>
		<div class="table-responsive">
			<table class="table table-bordered table-hover table-condensed MarksSheet">
				<thead>
					<tr>
						<th class="text-center" rowspan="<%# Item.Display.MultiRowHeader ? 2: 1 %>">#</th>
						<th rowspan="<%# Item.Display.MultiRowHeader ? 2: 1 %>">Enrollment</th>
						<th rowspan="<%# Item.Display.MultiRowHeader ? 2: 1 %>">Name</th>
						<th rowspan="<%# Item.Display.MultiRowHeader ? 2: 1 %>">Program</th>
						<th rowspan="<%# Item.Display.MultiRowHeader ? 2: 1 %>">Student Status</th>
						<th class="Assignments" runat="server" visible="<%# Item.Display.AssignmentVisible %>" colspan="<%# Item.Display.AssignmentsHeaderColSpan %>">Assignments</th>
						<th class="Assignments" runat="server" visible="<%# Item.Display.AssignmentsVisible %>">
							<uc1:NavigationButton runat="server" CanEdit="<%# Item.CanEditAssignments %>" CommandName="Assignments" OfferedCourseID="<%# Item.OfferedCourse.OfferedCourseID %>" IsLocked="<%# Item.IsAssignmentsLocked %>" CanUnlock="<%# Item.CanUnlockAssignments %>" OnCommand="NavigationButton_OnCommand" />
						</th>
						<th class="Quizzes" runat="server" visible="<%# Item.Display.QuizVisible %>" colspan="<%# Item.Display.QuizzesHeaderColSpan %>">Quizzes</th>
						<th class="Quizzes" runat="server" visible="<%# Item.Display.QuizzesVisible %>">
							<uc1:NavigationButton runat="server" CanEdit="<%# Item.CanEditQuizzes %>" CommandName="Quizzes" OfferedCourseID="<%# Item.OfferedCourse.OfferedCourseID %>" IsLocked="<%# Item.IsQuizzesLocked %>" CanUnlock="<%# Item.CanUnlockQuizzes %>" OnCommand="NavigationButton_OnCommand" />
						</th>
						<th class="Internals" rowspan="<%# Item.Display.MultiRowHeader ? 2: 1 %>" runat="server" visible="<%# Item.Display.InternalsVisible %>">
							<uc1:NavigationButton runat="server" CanEdit="<%# Item.CanCompileInternals %>" CommandName="CompileInternals" OfferedCourseID="<%# Item.OfferedCourse.OfferedCourseID %>" IsLocked="<%# Item.IsInternalsLocked %>" CanUnlock="<%# Item.CanUnlockInternals %>" OnCommand="NavigationButton_OnCommand" />
						</th>
						<th rowspan="<%# Item.Display.MultiRowHeader ? 2: 1 %>" class="Mid" runat="server" visible="<%# Item.Display.MidVisible %>">
							<uc1:NavigationButton runat="server" CanEdit="<%# Item.CanEditMid %>" CommandName="Mid" OfferedCourseID="<%# Item.OfferedCourse.OfferedCourseID %>" IsLocked="<%# Item.IsMidLocked %>" CanUnlock="<%# Item.CanUnlockMid %>" OnCommand="NavigationButton_OnCommand" />
						</th>
						<th rowspan="<%# Item.Display.MultiRowHeader ? 2: 1 %>" class="Final" runat="server">
							<uc1:NavigationButton runat="server" CanEdit="<%# Item.CanEditFinal %>" CommandName="Final" OfferedCourseID="<%# Item.OfferedCourse.OfferedCourseID %>" IsLocked="<%# Item.IsFinalLocked %>" CanUnlock="<%# Item.CanUnlockFinal %>" OnCommand="NavigationButton_OnCommand" />
						</th>
						<th rowspan="<%# Item.Display.MultiRowHeader ? 2: 1 %>" class="Total">Total</th>
						<th rowspan="<%# Item.Display.MultiRowHeader ? 2: 1 %>" class="Grade">Grade</th>
						<th rowspan="<%# Item.Display.MultiRowHeader ? 2: 1 %>" class="text-center" runat="server" visible="<%# Aspire.Model.Entities.Semester.IsSummer(Item.OfferedCourse.OfferedSemesterID) %>">Grade
							<br />
							Capping</th>
						<th rowspan="<%# Item.Display.MultiRowHeader ? 2: 1 %>" class="text-center">Status</th>
						<th rowspan="<%# Item.Display.MultiRowHeader ? 2: 1 %>">Record</th>
					</tr>
					<asp:Literal runat="server" Mode="PassThrough" Text="<tr>" Visible="<%# Item.Display.MultiRowHeader %>" />
					<asp:Repeater runat="server" Visible="<%# Item.Display.AssignmentVisible %>" DataSource="<%# Item.OfferedCourse.OfferedCourseExamAssignments %>" ItemType="Aspire.BL.Core.Exams.Common.New.IOfferedCourseExam">
						<ItemTemplate>
							<th class="Assignment">
								<uc1:NavigationButton runat="server" CanEdit="<%# Item.CanEditAssignment %>" CommandName="Assignment" IsLocked="<%# Item.Locked %>" ExamNo="<%# Item.ExamNo %>" OfferedCourseID="<%# Item.OfferedCourse.OfferedCourseID %>" OfferedCourseExamID="<%# Item.OfferedCourseExamID %>" CanUnlock="<%# Item.OfferedCourse.MarksSheet.CanUnlockAssignment(Item.OfferedCourseExamID) %>" OnCommand="NavigationButton_OnCommand" />
							</th>
						</ItemTemplate>
					</asp:Repeater>
					<th class="Assignment" runat="server" visible="<%# Item.Display.EmptyAssignmentVisible %>">&nbsp;</th>
					<th class="Assignments" runat="server" visible="<%# Item.Display.CompileAssignmentsVisible %>">
						<uc1:NavigationButton runat="server" CanEdit="<%# Item.CanCompileAssignments %>" CanUnlock="<%# Item.CanUnlockAssignments %>" IsLocked="<%# Item.IsAssignmentsLocked %>" OfferedCourseID="<%# Item.OfferedCourse.OfferedCourseID %>" CommandName="CompileAssignments" OnCommand="NavigationButton_OnCommand" />
					</th>
					<asp:Repeater runat="server" Visible="<%# Item.Display.QuizVisible %>" DataSource="<%# Item.OfferedCourse.OfferedCourseExamQuizzes %>" ItemType="Aspire.BL.Core.Exams.Common.New.IOfferedCourseExam">
						<ItemTemplate>
							<th class="Quiz">
								<uc1:NavigationButton runat="server" CanEdit="<%# Item.CanEditQuiz %>" CommandName="Quiz" IsLocked="<%# Item.Locked %>" ExamNo="<%# Item.ExamNo %>" OfferedCourseID="<%# Item.OfferedCourse.OfferedCourseID %>" OfferedCourseExamID="<%# Item.OfferedCourseExamID %>" CanUnlock="<%# Item.OfferedCourse.MarksSheet.CanUnlockQuiz(Item.OfferedCourseExamID) %>" OnCommand="NavigationButton_OnCommand" />
							</th>
						</ItemTemplate>
					</asp:Repeater>
					<th class="Quiz" runat="server" visible="<%# Item.Display.EmptyQuizVisible %>">&nbsp;</th>
					<th class="Quizzes" runat="server" visible="<%# Item.Display.CompileQuizzesVisible %>">
						<uc1:NavigationButton runat="server" CanEdit="<%# Item.CanCompileQuizzes %>" CanUnlock="<%# Item.CanUnlockQuizzes %>" IsLocked="<%# Item.IsQuizzesLocked %>" OfferedCourseID="<%# Item.OfferedCourse.OfferedCourseID %>" CommandName="CompileQuizzes" OnCommand="NavigationButton_OnCommand" />
					</th>
					<asp:Literal runat="server" Mode="PassThrough" Text="</tr>" Visible="<%# Item.Display.MultiRowHeader %>" />
				</thead>
				<tbody>
					<asp:Repeater ID="repeaterRegisteredCourses" runat="server" DataSource="<%# Item.OfferedCourse.RegisteredCourses.OrderBy(rc=> rc.Enrollment) %>" ItemType="Aspire.BL.Core.Exams.Common.New.IRegisteredCourse" OnItemCommand="repeaterRegisteredCourses_OnItemCommand">
						<ItemTemplate>
							<tr>
								<td class="text-center"><%#: Container.ItemIndex + 1 %></td>
								<td class="text-left"><%#: Item.Enrollment %></td>
								<td class="text-left"><%#: Item.Name %></td>
								<td class="text-left"><%#: Item.ProgramAlias %></td>
								<td class="text-left"><%#: Item.StudentStatusEnum?.ToFullName() %></td>
								<asp:Repeater Visible="<%# Item.OfferedCourse.MarksSheet.Display.AssignmentVisible %>" runat="server" DataSource="<%# Item.OfferedCourseExamAssignments %>" ItemType="Aspire.BL.Core.Exams.Common.New.IExamMarks">
									<ItemTemplate>
										<td class="Assignment"><%#: Item.OfferedCourseExam.OfferedCourse.MarksSheet.Display.GetMarksString(Item.RegisteredCourseExamMark?.ExamMarks, Item.OfferedCourseExam.TotalMarks) %></td>
									</ItemTemplate>
								</asp:Repeater>
								<td class="Assignment" runat="server" visible="<%# Item.OfferedCourse.MarksSheet.Display.EmptyAssignmentVisible %>"></td>
								<td class="Assignments" runat="server" visible="<%# Item.OfferedCourse.MarksSheet.Display.CompileAssignmentsVisible %>"><%#: Item.AssignmentsString %></td>
								<td class="Assignments" runat="server" visible="<%# Item.OfferedCourse.MarksSheet.Display.AssignmentsVisible %>"><%#: Item.AssignmentsString %></td>
								<asp:Repeater runat="server" Visible="<%# Item.OfferedCourse.MarksSheet.Display.QuizVisible %>" DataSource="<%# Item.OfferedCourseExamQuizzes %>" ItemType="Aspire.BL.Core.Exams.Common.New.IExamMarks">
									<ItemTemplate>
										<td class="Quiz"><%#: Item.OfferedCourseExam.OfferedCourse.MarksSheet.Display.GetMarksString(Item.RegisteredCourseExamMark?.ExamMarks, Item.OfferedCourseExam.TotalMarks) %></td>
									</ItemTemplate>
								</asp:Repeater>
								<td class="Quiz" runat="server" visible="<%# Item.OfferedCourse.MarksSheet.Display.EmptyQuizVisible %>"></td>
								<td class="Quizzes" runat="server" visible="<%# Item.OfferedCourse.MarksSheet.Display.CompileQuizzesVisible %>"><%#: Item.QuizzesString %></td>
								<td class="Quizzes" runat="server" visible="<%# Item.OfferedCourse.MarksSheet.Display.QuizzesVisible %>"><%#: Item.QuizzesString %></td>
								<td class="Internals" runat="server" visible="<%# Item.OfferedCourse.MarksSheet.Display.InternalsVisible %>"><%#: Item.InternalsString %></td>
								<td class="Mid" runat="server" visible="<%# Item.OfferedCourse.MarksSheet.Display.MidVisible %>"><%#: Item.MidString %></td>
								<td class="Final"><%#: Item.FinalString %></td>
								<td class="Total"><%#: Item.TotalString %></td>
								<td class="Grade"><%#: Item.GradeString %></td>
								<td class="text-center" runat="server" visible="<%# Aspire.Model.Entities.Semester.IsSummer(this.Sheet.OfferedCourse.OfferedSemesterID) %>">
									<%# Item.SummerGradeCapping 
											? (Item.ExamMarksPolicyToBeApplied.SummerCappingExamGradeEnum?.ToFullName() ?? "<strong title=\"As per policy\">N/A</strong>")
											: "<strong title=\"For Course\">N/A</strong>" %>
								</td>
								<td class="text-center"><%# Item.StatusHtml %></td>
								<td runat="server" class="text-center">
									<asp:Literal Visible="<%# Item.OfferedCourse.MarksSheet.IsFacultyMember %>" runat="server" Text='<%# Item.MarksEntryLocked ? "Locked" : "Unlocked" %>' />
									<aspire:AspireLinkButton Visible="<%# Item.OfferedCourse.MarksSheet.IsStaff %>" EncryptedCommandArgument="<%# Item.RegisteredCourseID %>" CommandName="Unlock" Enabled="<%# Item.CanUnlockMarksEntry %>" reasons="<%# Item.CanNotUnlockMarksEntryReasons.FirstOrDefault() %>" runat="server" Text='<%# Item.MarksEntryLocked ? "Locked" : "Unlocked" %>' CausesValidation="False" ConfirmMessage="Are you sure you want to unlock this record?" />
								</td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
				</tbody>
			</table>
		</div>
	</ItemTemplate>
</asp:Repeater>
<uc1:MarksSheetChanges runat="server" ID="marksSheetChanges" />
<asp:Table ID="tableLogs" runat="server" CssClass="table table-bordered table-hover table-condensed" Visible="False">
	<Rows>
		<asp:TableRow runat="server">
			<Cells>
				<asp:TableHeaderCell runat="server">Permission Name</asp:TableHeaderCell>
				<asp:TableHeaderCell runat="server">Reasons</asp:TableHeaderCell>
			</Cells>
		</asp:TableRow>
	</Rows>
</asp:Table>
<script type="text/javascript">
	$(function () {
		$("a.aspNetDisabled.disabled").css({ "cursor": "not-allowed" });
		$("a.aspNetDisabled.disabled[reasons]").click(function () {
			var reasons = $(this).attr("reasons");
			//reasons = "<ul style=\"padding-left:20px\">" + reasons + "</ul>";
			AspireModal.alert("Reasons", reasons, null, true);
		});
	});
</script>
