﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StudentClearanceForm.ascx.cs" Inherits="Aspire.Web.Sys.Common.Forms.StudentClearanceForm.StudentClearanceForm" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>

<div runat="server" id="divPrint">
	<aspire:AspireHyperLinkButton ButtonType="Primary" Glyphicon="print" runat="server" ID="hlPrintReport" Text="Print Form" Target="_blank" />
</div>

<table class="table table-bordered table-condensed info table-responsive tableCol4">
	<tr>
		<th>Enrollment</th>
		<td>
			<aspire:Label runat="server" ID="lblEnrollment" />
		</td>
		<th>Name</th>
		<td>
			<aspire:Label runat="server" ID="lblStudentName" />
		</td>
	</tr>
	<tr>
		<th>Program</th>
		<td>
			<aspire:Label runat="server" ID="lblPrograms" />
		</td>
		<th>Intake Semester</th>
		<td>
			<aspire:Label runat="server" ID="lblSemesters" />
		</td>
	</tr>
</table>

<aspire:AspirePanel runat="server" PanelType="Default" HeaderText="Required Information">
	<p>
		<strong>I request that my Tuition fee/Caution Money/Degree fee may please be refunded through Cross Cheque to be issued in favour of:</strong>
	</p>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Full Name:" AssociatedControlID="tbFullName" />
				<aspire:AspireTextBox runat="server" ID="tbFullName" ValidationGroup="RequiredInformation" MaxLength="100" />
				<aspire:AspireStringValidator runat="server" ControlToValidate="tbFullName" ValidationGroup="RequiredInformation" AllowNull="False" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="CNIC:" AssociatedControlID="tbCNIC" />
				<aspire:AspireTextBox runat="server" ID="tbCNIC" ValidationGroup="AddRecord" MaxLength="15" data-inputmask="'alias': 'cnic'" />
				<aspire:AspireStringValidator runat="server" ValidationExpression="CNIC" ControlToValidate="tbCNIC" ValidationGroup="RequiredInformation" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid CNIC." AllowNull="False" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Relationship:" AssociatedControlID="tbRelationship" />
				<aspire:AspireTextBox runat="server" ID="tbRelationship" ValidationGroup="RequiredInformation" MaxLength="100" />
				<aspire:AspireStringValidator runat="server" ControlToValidate="tbRelationship" ValidationGroup="RequiredInformation" AllowNull="False" ErrorMessage="This field is required." RequiredErrorMessage="This field is required." />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Student's Mobile:" AssociatedControlID="tbMobile" />
				<aspire:AspireTextBox runat="server" ID="tbMobile" data-inputmask="'alias': 'mobile'" MaxLength="15" />
				<aspire:AspireStringValidator RequiredErrorMessage="This field is required." runat="server" ValidationGroup="RequiredInformation" ControlToValidate="tbMobile" ValidationExpression="Mobile" AllowNull="False" ErrorMessage="This field is required." />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Reason of Clearance:" AssociatedControlID="tbReason" />
				<aspire:AspireTextBox runat="server" ID="tbReason" MaxLength="1000" ValidationGroup="RequiredInformation" TextMode="MultiLine" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbMobile" ValidationGroup="RequiredInformation" ErrorMessage="This field is required." />
			</div>
		</div>
	</div>
	<div class="pull-right">
		<aspire:AspireButton runat="server" ID="btnSaveRequiredInformation" ButtonType="Success" ValidationGroup="RequiredInformation" OnClick="btnSaveRequiredInformation_Click" />
	</div>
</aspire:AspirePanel>

<div runat="server" id="divSSC">
	<div class="form-inline">
		<aspire:AspireLabel runat="server" Text="Please Verify the student clearance form. (If selected Verfied it will not be changed again):" AssociatedControlID="ddlSSCStatus" />
		<aspire:AspireDropDownList runat="server" ID="ddlSSCStatus" CausesValidation="false" AutoPostBack="true" OnSelectedIndexChanged="ddlSSCStatus_SelectedIndexChanged" />
	</div>
</div>
<p></p>

<asp:Panel CssClass="panel panel-default" runat="server" ID="panelDepartmentStatuses">
	<div class="panel-heading">
		<h3 class="panel-title">Departments</h3>
	</div>
	<div class="panel-body">
		<aspire:AspireGridView runat="server" ID="gvDepartmentStatuses" Condensed="True" AutoGenerateColumns="False" AllowPaging="False" ItemType="Aspire.BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceForm.ClearanceFormDepartmentStatuses" OnRowCommand="gvStatuses_RowCommand" OnRowDataBound="gvStatuses_RowDataBound">
			<Columns>
				<asp:TemplateField HeaderText="#">
					<ItemTemplate>
						<%#Container.DataItemIndex + 1 %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Departments">
					<ItemTemplate>
						<%#: Item.DepartmentEnum.GetFullName() %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Signed By">
					<ItemTemplate>
						<%#: Item.Name.ToNAIfNullOrEmpty() %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Designation">
					<ItemTemplate>
						<%#: Item.Designation.ToNAIfNullOrEmpty() %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Status">
					<ItemTemplate>
						<%#: Item.DepartmentStatusEnum.GetFullName() %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Remarks/Reason">
					<ItemTemplate>
						<%#: Item.Remarks.ToNAIfNullOrEmpty() %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Actions">
					<ItemTemplate>
						<aspire:AspireLinkButton runat="server" CausesValidation="false" CommandName="Edit" Glyphicon="edit" EncryptedCommandArgument='<%#: $"{Item.FormStudentClearanceDepartmentStatusID},{Item.Departments}" %>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
	</div>
</asp:Panel>

<div class="text-center">
	<aspire:AspireButton runat="server" ID="btnSubmit" ButtonType="Primary" CausesValidation="false" Text="Submit" ConfirmMessage="Are you sure you want to submit application for Clearance?" OnClick="btnSubmit_Click" />
	<aspire:AspireLinkButton runat="server" ID="btnCancelForm" Text="Cancel" CausesValidation="false" OnClick="btnCancelForm_OnClick" />
</div>

<aspire:AspireModal runat="server" ID="modalEditStatus">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="updateEditStatus" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alertEditStatus" />
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatus" />
					<aspire:AspireDropDownList runat="server" ID="ddlStatus" ValidationGroup="EditStatus" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlStatus" ErrorMessage="This field is required." ValidationGroup="EditStatus" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Remarks/Reason:" AssociatedControlID="tbRemarks" />
					<aspire:AspireTextBox runat="server" ID="tbRemarks" ValidationGroup="EditStatus" TextMode="MultiLine" Rows="3" MaxLength="1000" />
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnSaveStatus" Text="Save" ButtonType="Primary" ValidationGroup="EditStatus" OnClick="btnSaveStatus_Click" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>

<aspire:AspireModal runat="server" ID="modalFormSubmissionChecks">
	<BodyTemplate>
		<asp:UpdatePanel runat="server" ID="UpdatePanelModalFormSubmissionChecks" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireAlert runat="server" ID="alertModalFormSubmissionChecks" />
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Confirm Passowrd:" AssociatedControlID="tbConfirmPassword" />
					<aspire:AspireTextBox runat="server" ID="tbConfirmPassword" ValidationGroup="FormSubmissionChecks" MaxLength="50" TextMode="Password" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbConfirmPassword" ValidationGroup="FormSubmissionChecks" ErrorMessage="This field is required." />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblConfirmUniversityEmailCode" AssociatedControlID="tbConfirmUniversityEmailCode" />
					<aspire:AspireTextBox runat="server" ID="tbConfirmUniversityEmailCode" ValidationGroup="FormSubmissionChecks" MaxLength="4" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbConfirmUniversityEmailCode" ValidationGroup="FormSubmissionChecks" ErrorMessage="This field is required." />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" ID="lblConfirmPersonalEmailCode" AssociatedControlID="tbConfirmPersonalEmailCode" />
					<aspire:AspireTextBox runat="server" ID="tbConfirmPersonalEmailCode" ValidationGroup="FormSubmissionChecks" MaxLength="4" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbConfirmPersonalEmailCode" ValidationGroup="FormSubmissionChecks" ErrorMessage="This field is required." />
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</BodyTemplate>
	<FooterTemplate>
		<asp:UpdatePanel runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<aspire:AspireButton runat="server" ID="btnResendConfirmationCodes" CssClass="pull-left" Text="Resend Confirmation Codes" ButtonType="Primary" CausesValidation="false" OnClick="btnResendConfirmationCodes_Click" />
				<aspire:AspireButton runat="server" ID="btnFormSubmitAfterChecks" Text="Submit" ButtonType="Primary" ValidationGroup="FormSubmissionChecks" OnClick="btnFormSubmitAfterChecks_Click" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</ContentTemplate>
		</asp:UpdatePanel>
	</FooterTemplate>
</aspire:AspireModal>
