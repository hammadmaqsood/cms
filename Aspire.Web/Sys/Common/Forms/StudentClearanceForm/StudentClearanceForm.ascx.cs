﻿using System;
using Aspire.Lib.Extensions;
using System.Web.UI.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Lib.WebControls;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using System.Linq;

namespace Aspire.Web.Sys.Common.Forms.StudentClearanceForm
{
	public partial class StudentClearanceForm : System.Web.UI.UserControl
	{
		private IAlert Alert => this.Page as IAlert;

		#region IDs

		private Guid? FormID
		{
			get => (Guid?)this.ViewState[nameof(this.FormID)];
			set => this.ViewState[nameof(this.FormID)] = value;
		}

		private Guid? FormStudentClearanceID
		{
			get => (Guid?)this.ViewState[nameof(this.FormStudentClearanceID)];
			set => this.ViewState[nameof(this.FormStudentClearanceID)] = value;
		}

		private int StudentID
		{
			get => (int)this.ViewState[nameof(this.StudentID)];
			set => this.ViewState[nameof(this.StudentID)] = value;
		}

		private int InstituteID
		{
			get => (int)this.ViewState[nameof(this.InstituteID)];
			set => this.ViewState[nameof(this.InstituteID)] = value;
		}

		public string Enrollment
		{
			get => (string)this.ViewState[nameof(this.Enrollment)];
			private set => this.ViewState[nameof(this.Enrollment)] = value;
		}

		public Guid? FormStudentClearanceDepartmentStatusID
		{
			get => (Guid?)this.ViewState[nameof(this.FormStudentClearanceDepartmentStatusID)];
			private set => this.ViewState[nameof(this.FormStudentClearanceDepartmentStatusID)] = value;
		}

		public Model.Entities.FormStudentClearanceDepartmentStatus.Departments DepartmentEnum
		{
			get => (Model.Entities.FormStudentClearanceDepartmentStatus.Departments)this.ViewState[nameof(this.DepartmentEnum)];
			private set => this.ViewState[nameof(this.DepartmentEnum)] = value;
		}

		#endregion

		private void RefreshPage()
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					Web.Sys.Staff.Forms.StudentClearanceForm.StudentClearanceForm.Redirect(this.Enrollment, true);
					break;
				case UserTypes.Student:
					BasePage.Redirect<Web.Sys.Student.Forms.StudentClearanceForm.StudentClearanceForm>();
					break;
				default:
					throw new NotImplementedEnumException(AspireIdentity.Current.UserType);
			}
		}


		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (AspireIdentity.Current.UserType.IsStaff())
				{
					var permission = StaffIdentity.Current.TryDemand(StaffPermissions.Forms.StudentClearanceFormCanInitiateOrEdit, Aspire.Model.Entities.UserGroupPermission.PermissionValues.Allowed);
					if (permission == null)
					{
						this.tbMobile.ReadOnly = true;
						this.tbReason.ReadOnly = true;
						this.tbFullName.ReadOnly = true;
						this.tbCNIC.ReadOnly = true;
						this.tbRelationship.ReadOnly = true;
						this.btnSaveRequiredInformation.Enabled = false;
					}
				}
			}
		}
		internal void LoadData(Aspire.BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceForm.StudentInformation data)
		{
			if (data == null)
				throw new ArgumentNullException(nameof(data));
			this.ControlsVisibility();

			//this.divPrint.Visible = getData?.GetStudentClearanceFormData?.StatusEnum == Model.Entities.StudentClearanceForm.Statuses.Initiated;
			this.StudentID = data.StudentID;
			this.InstituteID = data.InstituteID;
			this.FormID = data.ClearanceFormData?.FormID;
			this.Enrollment = data.Enrollment;

			this.lblStudentName.Text = data.Name;
			this.lblEnrollment.Text = data.Enrollment;
			this.lblPrograms.Text = data.ProgramAlias;
			this.lblSemesters.Text = data.IntakeSemester;

			if (data.ClearanceFormData?.StudentClearanceForm == null)
			{
				this.btnSaveRequiredInformation.Visible = true;
				this.btnSaveRequiredInformation.Text = "Save Information";
				this.tbMobile.Text = "";
				this.tbReason.Text = "";
				this.tbFullName.Text = "";
				this.tbCNIC.Text = "";
				this.tbRelationship.Text = "";
				this.FormStudentClearanceID = null;
				this.gvDepartmentStatuses.DataBind(null);
				return;
			}

			this.tbFullName.Text = data.ClearanceFormData.StudentClearanceForm.FullName;
			this.tbCNIC.Text = data.ClearanceFormData.StudentClearanceForm.CNIC;
			this.tbRelationship.Text = data.ClearanceFormData.StudentClearanceForm.Relationship;
			this.tbMobile.Text = data.ClearanceFormData.StudentClearanceForm.StudentMobile;
			this.tbReason.Text = data.ClearanceFormData.StudentClearanceForm.Reason;
			this.FormStudentClearanceID = data.ClearanceFormData.FormStudentClearanceID;
			if (data.ClearanceFormData.StudentClearanceStatusEnum == Model.Entities.FormStudentClearance.StudentClearanceStatuses.NotInitiated)
			{
				this.btnSaveRequiredInformation.Text = "Update Information";
				this.btnSaveRequiredInformation.Visible = true;
				this.btnSubmit.Visible = true;
				this.btnCancelForm.Visible = true;
			}

			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					this.divSSC.Visible = data.ClearanceFormData.StudentClearanceStatusEnum != Model.Entities.FormStudentClearance.StudentClearanceStatuses.NotInitiated;
					if (data.ClearanceFormData?.StudentClearanceSSCStatusEnum != null)
					{
						this.ddlSSCStatus.Visible = true;
						this.ddlSSCStatus.FillGuidEnums<Model.Entities.FormStudentClearance.StudentClearanceSSCStatuses>();
					}
					this.ddlSSCStatus.Enabled = data.ClearanceFormData.StudentClearanceSSCStatusEnum != Model.Entities.FormStudentClearance.StudentClearanceSSCStatuses.Verified;
					this.ddlSSCStatus.SetSelectedGuidEnum(data.ClearanceFormData.StudentClearanceSSCStatusEnum);
					this.gvDepartmentStatuses.Columns[6].Visible = data.ClearanceFormData.StudentClearanceStatusEnum != Model.Entities.FormStudentClearance.StudentClearanceStatuses.Completed;
					this.btnSaveRequiredInformation.Visible = true;
					this.btnSaveRequiredInformation.Text = "Update Information";
					break;
				case UserTypes.Student:
					if (data.ClearanceFormData.StudentClearanceStatusEnum != Model.Entities.FormStudentClearance.StudentClearanceStatuses.NotInitiated)
					{
						this.tbMobile.ReadOnly = true;
						this.tbReason.ReadOnly = true;
						this.tbFullName.ReadOnly = true;
						this.tbCNIC.ReadOnly = true;
						this.tbRelationship.ReadOnly = true;
					}
					var sscStatusPending = data.ClearanceFormData.FormSubmissionDate != null && data.ClearanceFormData.StudentClearanceSSCStatusEnum == Model.Entities.FormStudentClearance.StudentClearanceSSCStatuses.Pending;
					if (sscStatusPending)
						this.Alert.AddInfoAlert("Clearance Form Verfication is Pending from SSC.");
					var sscStatusNotVerified = data.ClearanceFormData.StudentClearanceSSCStatusEnum == Model.Entities.FormStudentClearance.StudentClearanceSSCStatuses.NotVerified;
					if (sscStatusNotVerified)
						this.Alert.AddInfoAlert("Clearance Form is Not Verified by SSC.");
					var sscStatusVerified = data.ClearanceFormData.StudentClearanceSSCStatusEnum == Model.Entities.FormStudentClearance.StudentClearanceSSCStatuses.Verified && data.ClearanceFormData.StudentClearanceStatusEnum != Model.Entities.FormStudentClearance.StudentClearanceStatuses.Completed;
					if (sscStatusVerified)
						this.Alert.AddInfoAlert("Clearance Form is Verified by SSC, Departments statuses are showed.");
					var completed = data.ClearanceFormData.StudentClearanceStatusEnum == Model.Entities.FormStudentClearance.StudentClearanceStatuses.Completed;
					if (completed)
						this.Alert.AddSuccessAlert("Clearance Form is approved by all departments.");
					this.gvDepartmentStatuses.Columns[6].Visible = false;
					break;
				default:
					throw new NotImplementedEnumException(AspireIdentity.Current.UserType);
			}

			if (data.ClearanceFormData.ClearanceFormDepartmentStatuses?.Any() != true)
			{
				this.gvDepartmentStatuses.DataBind(null);
				return;
			}
			this.gvDepartmentStatuses.DataBind(data.ClearanceFormData.ClearanceFormDepartmentStatuses);
			this.panelDepartmentStatuses.Visible = true;
		}

		protected void btnSaveRequiredInformation_Click(object sender, EventArgs e)
		{
			if (this.FormStudentClearanceID == null)
			{
				var result = BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.AddClearanceForm(this.StudentID, this.InstituteID,
					this.tbFullName.Text, this.tbCNIC.Text, this.tbRelationship.Text, this.tbMobile.Text, this.tbReason.Text, AspireIdentity.Current.LoginSessionGuid);

				switch (result.Status)
				{
					case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.AddClearanceFormResult.AddClearanceFormResultStatuses.NoRecordFound:
						this.Alert.AddErrorAlert("No record found.");
						return;
					case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.AddClearanceFormResult.AddClearanceFormResultStatuses.AlreadyExist:
						this.Alert.AddErrorAlert("Student Clearance Form already submitted.");
						return;
					case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.AddClearanceFormResult.AddClearanceFormResultStatuses.Success:
						this.Alert.AddSuccessMessageHasBeenAdded("Information");
						this.RefreshPage();
						return;
					default:
						throw new NotImplementedEnumException(result.Status);
				}
			}
			else
			{
				var result = BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.AddOrEditRequiredInformation(this.FormStudentClearanceID.Value,
					this.tbFullName.Text, this.tbCNIC.Text, this.tbRelationship.Text, this.tbMobile.Text, this.tbReason.Text, AspireIdentity.Current.LoginSessionGuid);

				switch (result)
				{
					case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.AddOrEditRequiredInformationStatuses.NoRecordFound:
						this.Alert.AddErrorAlert("No Record Found.");
						return;
					case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.AddOrEditRequiredInformationStatuses.Success:
						this.Alert.AddSuccessMessageHasBeenUpdated("Information");
						this.RefreshPage();
						return;
					default:
						throw new NotImplementedEnumException(result);
				}
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			this.SendConfirmationEmails(false);
		}

		protected void btnResendConfirmationCodes_Click(object sender, EventArgs e)
		{
			this.SendConfirmationEmails(true);
		}

		private void SendConfirmationEmails(bool resendEmail)
		{
			this.tbConfirmPassword.Text = "";
			this.tbConfirmUniversityEmailCode.Text = "";
			this.tbConfirmPersonalEmailCode.Text = "";
			var status = BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.SendEmailConfirmationCodeToStudent(this.FormStudentClearanceID.Value, resendEmail, AspireIdentity.Current.LoginSessionGuid);
			this.lblConfirmUniversityEmailCode.Text = $"Confirmation Code: ({status.UniversityEmail})";
			this.lblConfirmPersonalEmailCode.Text = $"Confirmation Code: ({status.PersoanlEmail})";
			switch (status.EmailStatus)
			{
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.Emails.EmailStatuses.UniversityEmailMissing:
					this.Alert.AddErrorAlert("University Email missing.");
					return;
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.Emails.EmailStatuses.PersonalEmailMissing:
					this.Alert.AddErrorAlert("Personal Email missing.");
					return;
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.Emails.EmailStatuses.Success:
					if (resendEmail)
					{
						this.alertModalFormSubmissionChecks.AddSuccessAlert("Confirmation codes are sent on your emails.");
						this.UpdatePanelModalFormSubmissionChecks.Update();
					}
					this.modalFormSubmissionChecks.HeaderText = "Emails have been sent. Please verify";
					break;
				default:
					throw new NotImplementedEnumException(status.EmailStatus);
			}
			this.modalFormSubmissionChecks.Show();
		}

		protected void btnFormSubmitAfterChecks_Click(object sender, EventArgs e)
		{
			var result = BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.SubmitForm(this.FormStudentClearanceID.Value, this.tbConfirmPassword.Text, this.tbConfirmUniversityEmailCode.Text, this.tbConfirmPersonalEmailCode.Text, AspireIdentity.Current.LoginSessionGuid);

			switch (result)
			{
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.SubmitFormStatuses.NoRecordFound:
					this.alertModalFormSubmissionChecks.AddNoRecordFoundAlert();
					this.UpdatePanelModalFormSubmissionChecks.Update();
					return;
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.SubmitFormStatuses.alreadySubmitted:
					this.alertModalFormSubmissionChecks.AddErrorAlert("Clearance form already submitted.");
					this.UpdatePanelModalFormSubmissionChecks.Update();
					return;
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.SubmitFormStatuses.PasswordNotCorrect:
					this.alertModalFormSubmissionChecks.AddErrorAlert("The entered password is not correct.");
					this.UpdatePanelModalFormSubmissionChecks.Update();
					return;
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.SubmitFormStatuses.UniversityEmailCodeNotCorrect:
					this.alertModalFormSubmissionChecks.AddErrorAlert("University email code is not correct.");
					this.UpdatePanelModalFormSubmissionChecks.Update();
					return;
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.SubmitFormStatuses.PersonalEmailCodeNotCorrect:
					this.alertModalFormSubmissionChecks.AddErrorAlert("Persoanl email code is not correct.");
					this.UpdatePanelModalFormSubmissionChecks.Update();
					return;
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.SubmitFormStatuses.Success:
					this.Alert.AddSuccessAlert("Clearance form has been submitted.");
					this.RefreshPage();
					return;
				default:
					throw new NotImplementedEnumException(result);
			}
		}

		protected void btnCancelForm_OnClick(object sender, EventArgs e)
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Staff:
					BasePage.Redirect<Web.Sys.Staff.Dashboard>();
					break;
				case UserTypes.Student:
					BasePage.Redirect<Web.Sys.Student.Dashboard>();
					break;
				default:
					throw new NotImplementedEnumException(AspireIdentity.Current.UserType);
			}
		}

		protected void gvStatuses_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					var commandArgs = e.CommandArgument.ToString().Decrypt().Split(new char[] { ',' });
					this.FormStudentClearanceDepartmentStatusID = commandArgs[0].ToGuid();
					var department = commandArgs[1].ToGuid();
					this.DepartmentEnum = department.GetEnum<Model.Entities.FormStudentClearanceDepartmentStatus.Departments>();
					this.DisplayStatusModal(this.FormStudentClearanceDepartmentStatusID);
					break;
				default:
					throw new NotImplementedException(e.CommandName);
			}
		}

		public void DisplayStatusModal(Guid? formStudentClearanceDepartmentStatusID)
		{
			this.ddlStatus.FillEnums<Model.Entities.FormStudentClearanceDepartmentStatus.DepartmentStatuses>();

			if (formStudentClearanceDepartmentStatusID == null)
				throw new ArgumentNullException(nameof(formStudentClearanceDepartmentStatusID));

			var status = Aspire.BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceForm.DepartmentStatus(formStudentClearanceDepartmentStatusID.Value, AspireIdentity.Current.LoginSessionGuid);

			this.ddlStatus.SetEnumValue(status.DepartmentStatusEnum);
			this.tbRemarks.Text = status.Remarks;
			this.modalEditStatus.HeaderText = "Edit " + $"{DepartmentEnum.GetFullName()}" + " Status";
			this.modalEditStatus.Show();
		}

		protected void btnSaveStatus_Click(object sender, EventArgs e)
		{
			var result = BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.UpdateStatus(this.FormStudentClearanceDepartmentStatusID.Value, this.DepartmentEnum, this.ddlStatus.SelectedValue.ToEnum<Model.Entities.FormStudentClearanceDepartmentStatus.DepartmentStatuses>(), this.tbRemarks.Text, AspireIdentity.Current.LoginSessionGuid);

			switch (result)
			{
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.UpdateStatuses.NoRecordFound:
					this.alertEditStatus.AddNoRecordFoundAlert();
					this.updateEditStatus.Update();
					return;
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.UpdateStatuses.RemarksIfNotApproved:
					this.alertEditStatus.AddErrorAlert("Please give the reason of selected Not Approved in remarks.");
					this.updateEditStatus.Update();
					return;
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.UpdateStatuses.NotAuthorized:
					this.alertEditStatus.AddErrorAlert("You are not authorized to perform this operation.");
					this.updateEditStatus.Update();
					return;
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.UpdateStatuses.NotAllStatusesAreApproved:
					this.alertEditStatus.AddErrorAlert("You are not allowed to approve because other department(s) didn't approved.");
					this.updateEditStatus.Update();
					return;
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.UpdateStatuses.Success:
					this.Alert.AddSuccessMessageHasBeenUpdated("Status");
					this.RefreshPage();
					return;
				default:
					throw new NotImplementedEnumException(result);
			}
		}

		protected void gvStatuses_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow)
				return;

			var row = (BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceForm.ClearanceFormDepartmentStatuses)e.Row.DataItem;

			if (row != null && row.DepartmentStatusEnum == Model.Entities.FormStudentClearanceDepartmentStatus.DepartmentStatuses.NotApproved)
				e.Row.CssClass = "danger";

			if (row != null && row.DepartmentStatusEnum == Model.Entities.FormStudentClearanceDepartmentStatus.DepartmentStatuses.Approved)
				e.Row.CssClass = "success";
		}

		protected void ddlSSCStatus_SelectedIndexChanged(object sender, EventArgs e)
		{
			var ddlSSCStatus = this.ddlSSCStatus.GetSelectedGuidEnum<Model.Entities.FormStudentClearance.StudentClearanceSSCStatuses>();
			var result = BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.SubmitSSCStatus(this.FormStudentClearanceID.Value, ddlSSCStatus, AspireIdentity.Current.LoginSessionGuid);

			switch (result)
			{
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.SubmitSSCStatuses.NoRecordFound:
					this.Alert.AddNoRecordFoundAlert();
					return;
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.SubmitSSCStatuses.Pending:
					this.Alert.AddSuccessAlert("SSC Status has been set to Pending.");
					this.RefreshPage();
					return;
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.SubmitSSCStatuses.NotAllowedToChangeWhenVerified:
					this.Alert.AddErrorAlert("Changes are not allowed in verified student.");
					return;
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.SubmitSSCStatuses.NotVerified:
					this.Alert.AddSuccessAlert("SSC Status has been set to not verified.");
					this.RefreshPage();
					return;
				case BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceFormMethods.SubmitSSCStatuses.Success:
					this.Alert.AddSuccessMessageHasBeenUpdated("SSC Status");
					this.RefreshPage();
					return;
				default:
					throw new NotImplementedEnumException(result);
			}
		}

		private void ControlsVisibility()
		{
			this.divPrint.Visible = false;
			this.btnSubmit.Visible = false;
			this.btnCancelForm.Visible = false;
			this.divSSC.Visible = false;
			this.ddlSSCStatus.Visible = false;
			this.panelDepartmentStatuses.Visible = false;
			this.btnSaveRequiredInformation.Visible = false;
		}
	}
}