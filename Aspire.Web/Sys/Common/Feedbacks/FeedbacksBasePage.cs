﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Common.Feedbacks
{
	public abstract class FeedbacksBasePage : AspireBasePage, IBreadcrumb, ISidebar, IProfileInfo
	{
		public static IEnumerable<UserTypes> AllowedUserTypes => new[] { UserTypes.Executive, UserTypes.Staff, UserTypes.Faculty, UserTypes.Student };

		public override AspireThemes AspireTheme => AspireDefaultThemes.GetDefaultThemeForCurrentUser();

		public override PageIcon PageIcon => FontAwesomeIcons.regular_comments.GetIcon();

		public virtual bool BreadcrumbVisible => true;

		public override bool PageTitleVisible => true;

		public virtual bool SidebarVisible => true;

		public virtual string SidebarHtml => SessionHelper.SideBarLinkHtml;

		public virtual string BreadcrumbHtml => SessionHelper.SideBarLinks?.GetBreadcrumbHtml(this.AspirePageAttribute.AspirePageGuid);

		public virtual bool ProfileInfoVisible => false;

		public virtual bool ProfileLinkVisible => false;

		public bool IsCustomProfileImage => false;

		public string CustomProfileImageUrl => null;

		public virtual string ProfileNavigateUrl => null;

		public virtual bool LogoffLinkVisible => true;

		public virtual string LogoffNavigateUrl
		{
			get
			{
				switch (this.AspireIdentity?.UserType)
				{
					case UserTypes.Admins:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
						return Admin.Logoff.GetPageUrl(UserLoginHistory.LogOffReasons.LogOff);
					case UserTypes.Executive:
						return Executive.Logoff.GetPageUrl(UserLoginHistory.LogOffReasons.LogOff);
					case UserTypes.Staff:
						return Staff.Logoff.GetPageUrl(UserLoginHistory.LogOffReasons.LogOff);
					case UserTypes.Candidate:
						return Candidate.Logoff.GetPageUrl(UserLoginHistory.LogOffReasons.LogOff);
					case UserTypes.Student:
						return Student.Logoff.GetPageUrl(UserLoginHistory.LogOffReasons.LogOff);
					case UserTypes.Faculty:
						return Faculty.Logoff.GetPageUrl(UserLoginHistory.LogOffReasons.LogOff);
					case UserTypes.Alumni:
					case UserTypes.IntegratedService:
					case UserTypes.ManualSQL:
					case UserTypes.Any:
					case null:
						throw new InvalidOperationException();
					default:
						throw new NotImplementedEnumException(this.AspireIdentity?.UserType);
				}
			}
		}

		public virtual string Username => this.AspireIdentity.Name;

		public virtual string Name => this.AspireIdentity.Name;

		public virtual string Email => this.AspireIdentity.Email;

		protected override void Authenticate()
		{
			var userType = this.AspireIdentity?.UserType;
			if (userType == null || !AllowedUserTypes.Contains(userType.Value))
				throw new AspireAccessDeniedException(UserTypes.Staff, UserTypes.Staff);
		}
	}
}