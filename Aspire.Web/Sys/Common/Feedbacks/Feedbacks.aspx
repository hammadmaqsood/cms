﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Feedbacks.aspx.cs" Inherits="Aspire.Web.Sys.Common.Feedbacks.Feedbacks" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-group">
		<aspire:AspireHyperLinkButton runat="server" ID="hlAddFeedback" ButtonType="Success" Glyphicon="plus" NavigateUrl="Feedback.aspx" Text="Add" />
	</div>
	<aspire:AspireGridView runat="server" AllowSorting="true" ItemType="Aspire.BL.Core.Feedbacks.Common.UserFeedback.Feedback" ID="gvUserFeedbacks" AutoGenerateColumns="false" OnPageIndexChanging="gvUserFeedbacks_PageIndexChanging" OnPageSizeChanging="gvUserFeedbacks_PageSizeChanging" OnSorting="gvUserFeedbacks_Sorting">
		<Columns>
			<asp:TemplateField HeaderText="Reference No." SortExpression="FeedbackNo">
				<ItemTemplate>
					<%#: Item.FeedbackNo %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="To" SortExpression="FeedbackBoxType">
				<ItemTemplate>
					<%#: Item.MovedFromFeedbackBoxType == null ? Item.FeedbackBoxType.GetFullName() : $"{Item.MovedFromFeedbackBoxType.Value.GetFullName()} -> {Item.FeedbackBoxType.GetFullName()}" %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Type" SortExpression="FeedbackType">
				<ItemTemplate>
					<%#: Item.FeedbackType.GetFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Date" SortExpression="FeedbackDate">
				<ItemTemplate>
					<%#: $"{Item.FeedbackDate:g}" %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Status" SortExpression="Status">
				<ItemTemplate>
					<%#: Item.Status.GetFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireHyperLinkButton ButtonType="OnlyIcon" Text="View" runat="server" NavigateUrl="<%# Aspire.Web.Sys.Common.Feedbacks.Feedback.GetPageUrl(Item.UserFeedbackID) %>" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
