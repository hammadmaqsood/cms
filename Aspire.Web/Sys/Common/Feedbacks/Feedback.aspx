﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Feedback.aspx.cs" Inherits="Aspire.Web.Sys.Common.Feedbacks.Feedback" %>

<asp:Content ContentPlaceHolderID="HeadPH" runat="server">
	<link href="../../../Scripts/Plugins/summernote/summernote.css" rel="stylesheet" />
	<style type="text/css">
		.note-editor.note-frame.panel {
			margin-bottom: 0;
		}
	</style>
</asp:Content>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-group" id="divFeedbackNo" runat="server">
		<aspire:AspireLabel runat="server" AssociatedControlID="tbFeedbackNo" Text="Reference No.:" />
		<aspire:AspireTextBox runat="server" ID="tbFeedbackNo" MaxLength="50" ReadOnly="true" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" AssociatedControlID="ddlFeedbackBoxType" Text="Register Complaint/Suggestion To:" />
		<aspire:AspireDropDownList runat="server" ID="ddlFeedbackBoxType" ValidationGroup="Feedback" />
		<aspire:AspireTextBox runat="server" ID="tbFeedbackBoxType" ValidationGroup="Feedback" MaxLength="1000" ReadOnly="true" />
		<aspire:AspireRequiredFieldValidator runat="server" ID="rfvddlFeedbackBoxType" ControlToValidate="ddlFeedbackBoxType" ValidationGroup="Feedback" ErrorMessage="This field is required." />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" AssociatedControlID="ddlFeedbackType" Text="Type:" />
		<aspire:AspireDropDownList runat="server" ID="ddlFeedbackType" ValidationGroup="Feedback" />
		<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlFeedbackType" ValidationGroup="Feedback" ErrorMessage="This field is required." />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" AssociatedControlID="tbFeedbackDetails" Text="Details: " />
		<em runat="server" id="emNote">(Please don't disclose your identity in below text area.)</em>
		<aspire:AspireTextBox CssClass="hidden" runat="server" ValidateRequestMode="Disabled" ID="tbFeedbackDetails" Rows="10" ValidationGroup="Feedback" TextMode="MultiLine" MaxLength="10485760" />
		<aspire:AspireCustomValidator runat="server" ValidateEmptyText="true" ID="cvtbFeedbackDetails" HighlightCssClass="has-error" ControlToValidate="tbFeedbackDetails" ValidationGroup="Feedback" ErrorMessage="This field is required." ClientValidationFunction="ValidateFeedback" OnServerValidate="cvtbFeedbackDetails_ServerValidate" />
		<div class="well" runat="server" id="divltrlFeedbackDetails">
			<asp:Literal runat="server" ID="ltrlFeedbackDetails" />
		</div>
	</div>
	<div class="form-group" runat="server" id="divFeedbackDate" visible="false">
		<aspire:AspireLabel runat="server" AssociatedControlID="tbFeedbackDate" Text="Submission Date:" />
		<aspire:AspireTextBox runat="server" ID="tbFeedbackDate" ReadOnly="true" MaxLength="200" />
	</div>
	<div class="form-group" runat="server" id="divStatus" visible="false">
		<aspire:AspireLabel runat="server" AssociatedControlID="tbStatus" Text="Status:" />
		<aspire:AspireTextBox runat="server" ID="tbStatus" ReadOnly="true" MaxLength="200" />
	</div>
	<div class="form-group text-center" runat="server" id="divSubmitButtons">
		<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="Feedback" Text="Submit" ConfirmMessage="Are you sure you want to submit?" OnClick="btnSave_Click" />
		<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="Feedbacks.aspx" />
	</div>
	<asp:Repeater ID="repeaterDiscussion" ItemType="Aspire.BL.Core.Feedbacks.Common.UserFeedback.Feedback.FeedbackDiscussion" runat="server">
		<HeaderTemplate><strong>Discussion:</strong></HeaderTemplate>
		<ItemTemplate>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><%#: Item.CreatedByName %> <em class="pull-right"><%#: Item.CreatedDate.ToString("F") %></em></h3>
				</div>
				<div class="panel-body">
					<%# Item.Details %>
				</div>
			</div>
		</ItemTemplate>
	</asp:Repeater>
	<div class="form-group" runat="server" id="divDiscussionAdd">
		<aspire:AspireLabel Text="Comment:" runat="server" AssociatedControlID="tbDiscussionDetails" />
		<em runat="server" id="emCommentsNote">(Please don't disclose your identity in below text area.)</em>
		<aspire:AspireTextBox CssClass="hidden" runat="server" ValidateRequestMode="Disabled" ID="tbDiscussionDetails" Rows="5" ValidationGroup="Discussion" TextMode="MultiLine" MaxLength="10485760" />
		<aspire:AspireCustomValidator runat="server" ValidateEmptyText="true" ID="cvtbDiscussionDetails" HighlightCssClass="has-error" ControlToValidate="tbDiscussionDetails" ValidationGroup="Discussion" ErrorMessage="This field is required." ClientValidationFunction="ValidateDiscussion" OnServerValidate="cvtbDiscussionDetails_ServerValidate" />
	</div>
	<div class="form-group text-center" runat="server" id="divDiscussionAddButtons">
		<aspire:AspireButton runat="server" ButtonType="Primary" ID="btnAdd" ValidationGroup="Discussion" Text="Add" OnClick="btnAdd_Click" />
		<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="Feedbacks.aspx" />
	</div>
	<aspire:AspireModal runat="server" ID="modalInstruction" CloseButtonVisible="false" HeaderText="Important Note">
		<BodyTemplate>
			<ol style="padding-left: 15px">
				<li>This feedback mechanism has been made for betterment of university.</li>
				<li>No personal complaints/comments will be entertained.</li>
				<li>Please give your input in the form of valid suggestions/complaints in order to improve and strengthen the overall academic and administrative environment of the university. It is to be noted that for campus level suggestions/complaints, please select <strong>DG Campus</strong> as the recipient and for suggestion/complaint pertains to the university level select <strong>Rector</strong> as the recipient.</li>
				<li>It is to be noted that your identity will only be visible to the recipeint (Rector or <abbr title="Director General Campus">DG Campus</abbr>).</li>
			</ol>
		</BodyTemplate>
		<FooterTemplate>
			<button type="button" class="btn btn-primary" id="btnIAgree">I agree</button>
			<asp:Button CssClass="btn btn-default" runat="server" Text="Cancel" PostBackUrl="Feedbacks.aspx" />
		</FooterTemplate>
	</aspire:AspireModal>
	<script src="../../../Scripts/Plugins/summernote/summernote.min.js"></script>
	<script type="text/javascript">
		var tbFeedbackDetails = $("#<%=this.tbFeedbackDetails.ClientID%>");
		var tbDiscussionDetails = $("#<%=this.tbDiscussionDetails.ClientID%>");
		function ValidateFeedback(source, args) {
			var html = tbFeedbackDetails.summernote("code");
			var hiddenDiv = $("<div></div>").appendTo($("body")).hide().append(html);
			var text = hiddenDiv.text();
			hiddenDiv.remove();
			args.IsValid = text != undefined && text != null && text.trim().length > 0;
			if (args.IsValid)
				tbFeedbackDetails.val(html);
			else
				tbFeedbackDetails.val("");
			ApplyHighlightCss(source, "<%=this.tbFeedbackDetails.ClientID%>", args);
		}
		function ValidateDiscussion(source, args) {
			var html = tbDiscussionDetails.summernote("code");
			var hiddenDiv = $("<div></div>").appendTo($("body")).hide().append(html);
			var text = hiddenDiv.text();
			hiddenDiv.remove();
			args.IsValid = text != undefined && text != null && text.trim().length > 0;
			if (args.IsValid)
				tbDiscussionDetails.val(html);
			else
				tbDiscussionDetails.val("");
			ApplyHighlightCss(source, "<%=this.tbDiscussionDetails.ClientID%>", args);
		}
		$(document).ready(function () {
			var modalImportantNotes = $("#<%=this.modalInstruction.ClientID%>");
			if (modalImportantNotes.length > 0) {
				AspireModal.open(modalImportantNotes, "static", false, true);
				$("#btnIAgree").click(function () {
					AspireModal.close($("#<%=this.modalInstruction.ClientID%>"));
				});
			}
			tbFeedbackDetails.summernote({
				height: 300,
				minHeight: 300,
				maxHeight: null,
				focus: false,
				codeviewFilter: true,
				codeviewIframeFilter: true,
				toolbar: [
					['style', ['style']],
					['font', ['bold', 'italic', 'underline', 'clear']],
					['fontname', ['fontname']],
					['color', ['color']],
					['para', ['ul', 'ol', 'paragraph']],
					['table', ['table']],
					['insert', ['link', 'picture', 'video']]
				]
			});
			if (tbFeedbackDetails.prop("readonly"))
				tbFeedbackDetails.next().find(".note-editable").attr("contenteditable", false);

			tbDiscussionDetails.summernote({
				height: 300,
				minHeight: 300,
				maxHeight: null,
				focus: false,
				codeviewFilter: true,
				codeviewIframeFilter: true,
				toolbar: [
					['style', ['style']],
					['font', ['bold', 'italic', 'underline', 'clear']],
					['fontname', ['fontname']],
					['color', ['color']],
					['para', ['ul', 'ol', 'paragraph']],
					['table', ['table']],
					['insert', ['link', 'picture', 'video']]
				]
			});
			if (tbDiscussionDetails.prop("readonly"))
				tbDiscussionDetails.next().find(".note-editable").attr("contenteditable", false);
		});
	</script>
</asp:Content>
