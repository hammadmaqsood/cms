﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Common.Feedbacks
{
	[AspirePage("604A556C-44D6-4F46-9B66-9C5E7110066D", UserTypes.Student | UserTypes.Executive | UserTypes.Faculty | UserTypes.Staff | UserTypes.Admins, "~/Sys/Common/Feedbacks/Feedbacks.aspx", "Virtual Suggestion Box", true, AspireModules.Feedbacks)]
	public partial class Feedbacks : FeedbacksBasePage
	{
		public override string PageTitle => "Virtual Suggestion Box";

		public static string GetPageUrl(int? pageIndex, int? pageSize, SortDirection sortDirection, string sortExpression)
		{
			return typeof(Feedbacks).GetAspirePageAttribute().PageUrl.AttachQueryParams(
				("PageIndex", pageIndex),
				("PageSize", pageSize),
				("SortDirection", sortDirection),
				("SortExpression", sortExpression));
		}

		public static void Redirect(int? pageIndex, int? pageSize, SortDirection sortDirection, string sortExpression)
		{
			Redirect(GetPageUrl(pageIndex, pageSize, sortDirection, sortExpression));
		}

		private void RefreshPage()
		{
			var pageIndex = this.gvUserFeedbacks.PageIndex;
			var pageSize = this.gvUserFeedbacks.PageSize;
			var sortDirection = this.ViewState.GetSortDirection();
			var sortExpression = this.ViewState.GetSortExpression();
			Redirect(pageIndex, pageSize, sortDirection, sortExpression);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(this.GetParameterValue("SortExpression") ?? nameof(UserFeedback.FeedbackDate), this.GetParameterValue<SortDirection>("SortDirection") ?? SortDirection.Descending);
				this.gvUserFeedbacks.PageSize = this.GetParameterValue<int>("PageSize") ?? this.gvUserFeedbacks.PageSize;
				this.ddlFeedbackTypeFilter_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlFeedbackTypeFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void gvUserFeedbacks_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvUserFeedbacks_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvUserFeedbacks.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvUserFeedbacks_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			if (!this.IsPostBack)
				pageIndex = this.GetParameterValue<int>("PageIndex") ?? pageIndex;

			var pageSize = this.gvUserFeedbacks.PageSize;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();

			var (feedbacks, virtualItemCount) = BL.Core.Feedbacks.Common.UserFeedback.GetUserFeedbacks(null, null, pageIndex, pageSize, sortExpression, sortDirection, this.AspireIdentity.LoginSessionGuid);
			this.gvUserFeedbacks.DataBind(feedbacks, pageIndex, virtualItemCount, sortExpression, sortDirection);
		}
	}
}