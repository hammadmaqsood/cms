﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Common.Feedbacks
{
	[AspirePage("3D3380F8-F26F-4630-9340-24C6AC05AB59", UserTypes.Student | UserTypes.Executive | UserTypes.Faculty | UserTypes.Staff | UserTypes.Admins, "~/Sys/Common/Feedbacks/Feedback.aspx", "Virtual Suggestion Box", true, AspireModules.Feedbacks)]
	public partial class Feedback : FeedbacksBasePage
	{
		public override string PageTitle => "Virtual Suggestion Box";

		public static string GetPageUrl(Guid userFeedbackID)
		{
			return typeof(Feedback).GetAspirePageAttribute().PageUrl.AttachQueryParam(nameof(UserFeedbackID), userFeedbackID);
		}

		public static void Redirect(Guid userFeedbackID)
		{
			Redirect(GetPageUrl(userFeedbackID));
		}

		public Guid? UserFeedbackID => this.GetParameterValue<Guid>(nameof(this.UserFeedbackID));

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlFeedbackBoxType.FillGuidEnums<Model.Entities.UserFeedback.FeedbackBoxTypes>(CommonListItems.Select);
				this.ddlFeedbackType.FillGuidEnums<Model.Entities.UserFeedback.FeedbackTypes>(CommonListItems.Select);
				if (this.UserFeedbackID != null)
				{
					var feedback = BL.Core.Feedbacks.Common.UserFeedback.GetUserFeedback(this.UserFeedbackID.Value, this.AspireIdentity.LoginSessionGuid);
					if (feedback == null)
					{
						this.AddNoRecordFoundAlert();
						Redirect<Feedbacks>();
						return;
					}
					this.divFeedbackNo.Visible = true;
					this.tbFeedbackNo.Text = feedback.FeedbackNo;
					this.ddlFeedbackBoxType.SetSelectedGuidEnum(feedback.FeedbackBoxTypeEnum).Enabled = false;
					this.tbFeedbackBoxType.Text = feedback.MovedFromFeedbackBoxType == null
						? feedback.FeedbackBoxType.GetFullName()
						: $"{feedback.FeedbackBoxType.GetFullName()} => {feedback.MovedFromFeedbackBoxType.Value.GetFullName()}";
					this.tbFeedbackBoxType.Visible = true;
					this.ddlFeedbackBoxType.Visible = false;
					this.rfvddlFeedbackBoxType.Visible = false;
					this.ddlFeedbackType.SetSelectedGuidEnum(feedback.FeedbackTypeEnum).Enabled = false;
					this.tbFeedbackDetails.Text = feedback.FeedbackDetails;
					this.tbFeedbackDetails.ReadOnly = true;
					this.tbFeedbackDetails.Visible = false;
					this.cvtbFeedbackDetails.Visible = false;
					this.ltrlFeedbackDetails.Text = feedback.FeedbackDetails;
					this.ltrlFeedbackDetails.Mode = System.Web.UI.WebControls.LiteralMode.PassThrough;
					this.ltrlFeedbackDetails.Visible = true;
					this.divltrlFeedbackDetails.Visible = true;
					this.tbFeedbackDate.Text = $"{feedback.FeedbackDate:F}";
					this.tbFeedbackDate.ReadOnly = true;
					this.tbStatus.Text = feedback.Status.GetFullName();

					this.divDiscussionAdd.Visible = false;
					this.divDiscussionAddButtons.Visible = false;
					if (feedback.FeedbackDiscussions?.Any() == true)
					{
						this.repeaterDiscussion.DataBind(feedback.FeedbackDiscussions.OrderBy(d => d.CreatedDate));
						this.repeaterDiscussion.Visible = true;

						switch (feedback.StatusEnum)
						{
							case Model.Entities.UserFeedback.Statuses.InProcess:
							case Model.Entities.UserFeedback.Statuses.Pending:
								this.divDiscussionAdd.Visible = true;
								this.divDiscussionAddButtons.Visible = true;
								break;
							case Model.Entities.UserFeedback.Statuses.SpamJunk:
							case Model.Entities.UserFeedback.Statuses.Closed:
								break;
							default:
								throw new NotImplementedEnumException(feedback.StatusEnum);
						}
					}
					else
					{
						this.repeaterDiscussion.DataBind(null);
						this.repeaterDiscussion.Visible = false;
						this.divSubmitButtons.Visible = true;
					}

					this.divStatus.Visible = true;
					this.btnSave.Visible = false;
					this.divSubmitButtons.Visible = !this.divDiscussionAdd.Visible;
					this.ViewState[nameof(this.UserFeedbackID)] = feedback.UserFeedbackID;
					this.modalInstruction.Visible = false;
				}
				else
				{
					this.divFeedbackNo.Visible = false;
					this.tbFeedbackNo.Text = null;
					this.ddlFeedbackBoxType.ClearSelection();
					this.tbFeedbackBoxType.Visible = false;
					this.ddlFeedbackBoxType.Visible = true;
					this.rfvddlFeedbackBoxType.Visible = true;
					this.ddlFeedbackType.ClearSelection();
					this.ddlFeedbackType.Enabled = true;
					this.tbFeedbackDetails.ReadOnly = false;
					this.tbFeedbackDetails.Text = null;
					this.tbFeedbackDetails.Visible = true;
					this.cvtbFeedbackDetails.Visible = true;
					this.ltrlFeedbackDetails.Visible = false;
					this.divltrlFeedbackDetails.Visible = false;
					this.repeaterDiscussion.Visible = false;
					this.divDiscussionAdd.Visible = false;
					this.divStatus.Visible = false;
					this.btnSave.Visible = true;
					this.divDiscussionAdd.Visible = false;
					this.divDiscussionAddButtons.Visible = false;
					this.ViewState[nameof(this.UserFeedbackID)] = null;
				}
			}
			else
				this.modalInstruction.Visible = false;

			this.emNote.Visible = !this.tbFeedbackDetails.ReadOnly && this.tbFeedbackDetails.Visible;
			this.emCommentsNote.Visible = !this.tbDiscussionDetails.ReadOnly && this.tbDiscussionDetails.Visible;
		}

		protected void btnSave_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var feedbackBoxTypeEnum = this.ddlFeedbackBoxType.GetSelectedGuidEnum<Model.Entities.UserFeedback.FeedbackBoxTypes>();
			var feedbackTypeEnum = this.ddlFeedbackType.GetSelectedGuidEnum<Model.Entities.UserFeedback.FeedbackTypes>();
			var feedbackDetails = this.Request.Unvalidated.Form[this.tbFeedbackDetails.UniqueID];
			var status = BL.Core.Feedbacks.Common.UserFeedback.AddUserFeedback(feedbackBoxTypeEnum, feedbackTypeEnum, feedbackDetails, this.AspireIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Feedbacks.Common.UserFeedback.AddUserFeedbackStatuses.NotAuthorized:
					this.AddErrorMessageYouAreNotAuthorizedToPerformThisAction();
					Redirect<Default>();
					return;
				case BL.Core.Feedbacks.Common.UserFeedback.AddUserFeedbackStatuses.Success:
					this.AddSuccessAlert($"Your {feedbackTypeEnum.GetFullName()} has been submitted.");
					Redirect<Feedbacks>();
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		protected void cvtbFeedbackDetails_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
		{
			args.IsValid = !string.IsNullOrWhiteSpace(this.tbFeedbackDetails.Text);
		}

		protected void cvtbDiscussionDetails_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
		{
			args.IsValid = this.divDiscussionAdd.Visible && !string.IsNullOrWhiteSpace(this.tbDiscussionDetails.Text);
		}

		protected void btnAdd_Click(object sender, EventArgs e)
		{
			var userFeedbackID = (Guid)this.ViewState[nameof(this.UserFeedbackID)];
			var discussionDetails = this.Request.Unvalidated.Form[this.tbDiscussionDetails.UniqueID];
			var status = BL.Core.Feedbacks.Common.UserFeedback.AddDiscussion(userFeedbackID, discussionDetails, this.AspireIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Feedbacks.Common.UserFeedback.AddDiscussionStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<Feedbacks>();
					return;
				case BL.Core.Feedbacks.Common.UserFeedback.AddDiscussionStatuses.StatusIsNotInProcess:
					this.AddErrorAlert("Status is not valid for discussion.");
					Redirect(userFeedbackID);
					return;
				case BL.Core.Feedbacks.Common.UserFeedback.AddDiscussionStatuses.Success:
					this.AddSuccessAlert("Your feedback has been saved.");
					Redirect(userFeedbackID);
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}
	}
}