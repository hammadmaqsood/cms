﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StudentInfoUserControl.ascx.cs" Inherits="Aspire.Web.Sys.Student.StudentInfoUserControl" %>

<asp:Panel runat="server" ID="panelStudentInfoEnrollment">
	<table class="table table-bordered table-condensed studentInfo tableCol4">
		<caption>Student Information</caption>
		<tbody>
			<tr>
				<th>Enrollment</th>
				<td>
					<aspire:Label runat="server" ID="lbEnrollment" />
				</td>
				<th>Registration No.</th>
				<td>
					<aspire:Label runat="server" ID="lblRegistrationNo" />
				</td>
			</tr>
			<tr>
				<th>Name</th>
				<td>
					<aspire:Label runat="server" ID="lblName" />
				</td>
				<th>Intake Semester</th>
				<td>
					<aspire:Label runat="server" ID="lblIntakeSemester" />
				</td>
			</tr>
			<tr>
				<th>Program</th>
				<td>
					<aspire:Label runat="server" ID="lblProgram" />
				</td>
				<th>Status</th>
				<td>
					<aspire:Label runat="server" ID="lblStatus" />
				</td>
			</tr>
		</tbody>
	</table>
</asp:Panel>
