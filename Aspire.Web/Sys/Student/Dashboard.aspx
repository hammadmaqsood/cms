﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Aspire.Web.Sys.Student.Dashboard" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<ul>
		<li>If you have forgot Microsoft O365 Email Account's Password, please click <strong>
			<asp:HyperLink NavigateUrl="~/Sys/Student/EmailAccount.aspx" runat="server">here</asp:HyperLink></strong> to reset it.
		</li>
		<li>Registrar Notification No. 007/2020: Male students of Bahria University are allowed to wear Shalwar Kameez with Coat/Waist Coat and Shoes on all days of the week.</li>
		<li>Ehsaas Scholarship <a target="_blank" rel="noopener noreferrer" href="https://bahria.edu.pk/ehsaas-undergraduate-scholarship-program">https://bahria.edu.pk/ehsaas-undergraduate-scholarship-program</a></li>
		<li>Sindh Endowment Fund Scholarship <a rel="noopener noreferrer" target="_blank" href="https://bahria.edu.pk/scholarships/sindh-endowment-fund-scholarship">https://bahria.edu.pk/scholarships/sindh-endowment-fund-scholarship</a></li>
		<li>A new feature titled <a href="../Common/Feedbacks/Feedbacks.aspx">Virtual Suggestion Box</a> has been added in CMS. Please give your valuable suggestions or complaints for betterment of University. It is to be noted that for campus level suggestions/complaints, please select <strong>DG Campus</strong> as the recipient and for suggestion/complaint pertains to the university level select <strong>Rector</strong> as the recipient.</li>
	</ul>
</asp:Content>
