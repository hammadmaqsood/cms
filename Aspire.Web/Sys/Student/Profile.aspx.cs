﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Student
{
	[AspirePage("FB26AFD8-73A5-467B-AC35-FA97E7D0C7CE", UserTypes.Student, "~/Sys/Student/Profile.aspx", "Profile Information", true, AspireModules.StudentInformation)]
	public partial class Profile : StudentAndParentPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_user);
		public override string PageTitle => "Profile Information";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var student = BL.Core.StudentInformation.Student.Profile.GetStudentInfo(this.StudentIdentity.LoginSessionGuid);
				if (student == null)
					throw new InvalidOperationException();

				this.fvStudent.DataSource = new[] { student };
				this.fvStudent.DataBind();
			}
		}

		protected void fvStudent_OnItemCommand(object sender, FormViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "EditPersonalEmail":
					var student = BL.Core.StudentInformation.Student.Profile.GetStudentInfo(this.StudentIdentity.LoginSessionGuid);
					this.tbPersonalEmail.Text = student.PersonalEmailNotVerified ?? student.PersonalEmail;
					this.modalPersonalEmail.Show();
					break;
				case "ResendConfirmationForPersonalEmail":
					student = BL.Core.StudentInformation.Student.Profile.GetStudentInfo(this.StudentIdentity.LoginSessionGuid);
					if (student.PersonalEmailNotVerified == null)
					{
						Redirect<Profile>();
						return;
					}
					var pageURL = GetPageUrl<ConfirmPersonalEmail>().ToAbsoluteUrl();
					BL.Core.StudentInformation.Student.Profile.SendEmailConfirmationLinkToStudent(pageURL, this.StudentIdentity.LoginSessionGuid);
					this.AddSuccessAlert($"An email has been sent again to {student.PersonalEmailNotVerified} to make sure it is a valid address.");
					Redirect<Profile>();
					break;
			}
		}

		protected void btnUpdatePersonalEmail_OnClick(object sender, EventArgs e)
		{
			if (this.IsPostbackAndNotValid)
				return;
			var personalEmail = this.tbPersonalEmail.Text.MustBeEmailAddress();
			var pageURL = GetPageUrl<ConfirmPersonalEmail>().ToAbsoluteUrl();
			var updated = BL.Core.StudentInformation.Student.Profile.UpdatePersonalEmail(this.tbPersonalEmail.Text, pageURL, this.StudentIdentity.LoginSessionGuid);
			if (updated)
				this.AddSuccessAlert($"An email has been sent to {personalEmail} to make sure it is a valid address.");
			Redirect<Profile>();
		}
	}
}