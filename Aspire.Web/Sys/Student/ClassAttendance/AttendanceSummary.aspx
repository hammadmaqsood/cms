﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="AttendanceSummary.aspx.cs" Inherits="Aspire.Web.Sys.Student.ClassAttendance.AttendanceSummary" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<style scoped="scoped">
		table.tab th {
			white-space: nowrap;
		}

		table.tab td {
			width: 50%;
		}
	</style>

	<aspire:AspireHyperLink runat="server" NavigateUrl="~/Sys/Student/CourseRegistration/RegisteredCourses.aspx" Text="Registered Courses" CssClass="btn btn-success" />
	<table class="table table-bordered table-condensed tab">
		<caption>Course Information</caption>
		<tbody>
			<tr>
				<th>Title</th>
				<td>
					<aspire:Label runat="server" ID="lblCourseTitle" />
				</td>
				<th>Teacher Name</th>
				<td>
					<aspire:Label runat="server" ID="lblTeacherName" />
				</td>
			</tr>
			<tr>
				<th>Credit Hours</th>
				<td>
					<aspire:Label runat="server" ID="lblCreditHrs" />
				</td>
				<th>Contact Hours</th>
				<td>
					<aspire:Label runat="server" ID="lblContactHrs" />
				</td>
			</tr>
			<tr>
				<th>Class</th>
				<td>
					<aspire:Label runat="server" ID="lblClass" />
				</td>
				<th>Semester</th>
				<td>
					<aspire:Label runat="server" ID="lblSemesterID" />
				</td>
			</tr>
		</tbody>
	</table>
	<style scoped="scoped">
		table[id$=gvAttendanceDetails] td:first-child,
		table[id$=gvAttendanceDetails] th:first-child,
		table[id$=gvAttendanceDetails] th:nth-child(6),
		table[id$=gvAttendanceDetails] td:nth-child(6),
		table[id$=gvAttendanceDetails] th:nth-child(7),
		table[id$=gvAttendanceDetails] td:nth-child(7),
		table[id$=gvAttendanceDetails] th:nth-child(8),
		table[id$=gvAttendanceDetails] td:nth-child(8),
		table[id$=gvAttendanceDetails] th:nth-child(9),
		table[id$=gvAttendanceDetails] td:nth-child(9) {
			text-align: center;
		}
	</style>
	<aspire:AspireGridView Caption="Attendance Summary" ItemType="Aspire.BL.Core.ClassAttendance.Student.ClassAttendance.AttendanceSummary.AttendanceDetail" runat="server" Stripped="False" ID="gvAttendanceDetails" AutoGenerateColumns="False" ShowFooter="True">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%#: Container.DataItemIndex + 1 %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Class Date" DataField="ClassDate" DataFormatString="{0:D}" />
			<asp:BoundField HeaderText="Class Type" DataField="ClassTypeFullName" />
			<asp:BoundField HeaderText="Room" DataField="RoomName" />
			<asp:TemplateField HeaderText="Remarks / Topics Covered">
				<ItemTemplate>
					<strong>Remarks:</strong>
					<p><%# (Item.Remarks?.HtmlEncode(true)).ToNAIfNullOrEmpty() %></p>
					<strong>Topics Covered:</strong>
					<p><%# (Item.TopicsCovered?.HtmlEncode(true)).ToNAIfNullOrEmpty() %></p>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Total Hours">
				<ItemTemplate><%#: Item.TotalHours.ToString("0.##") %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Present Hours">
				<ItemTemplate><%#: Item.PresentHours.ToString("0.##") %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Absent Hours">
				<ItemTemplate><%#: Item.AbsentHours.ToString("0.##") %></ItemTemplate>
			</asp:TemplateField>
		</Columns>
		<FooterStyle Font-Bold="True"></FooterStyle>
	</aspire:AspireGridView>
</asp:Content>
