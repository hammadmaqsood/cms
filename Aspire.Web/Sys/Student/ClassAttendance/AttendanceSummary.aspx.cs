﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Sys.Student.CourseRegistration;
using System;

namespace Aspire.Web.Sys.Student.ClassAttendance
{
	[AspirePage("8EED61D7-255C-4806-B355-E498D0A2D86D", UserTypes.Student, "~/Sys/Student/ClassAttendance/AttendanceSummary.aspx", "Attendance Summary", true, AspireModules.ClassAttendance)]
	public partial class AttendanceSummary : StudentAndParentPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_list_ol);
		public override string PageTitle => "Attendance Summary";

		public static string GetPageUrl(int registeredCourseID)
		{
			return GetPageUrl<AttendanceSummary>().AttachQueryParam("RegisteredCourseID", registeredCourseID);
		}

		public static void Redirect(int registeredCourseID)
		{
			Redirect(GetPageUrl(registeredCourseID));
		}

		public int? RegisteredCourseID => this.Request.GetParameterValue<int>("RegisteredCourseID");

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.RegisteredCourseID == null)
				{
					Redirect<Dashboard>();
					return;
				}
				else if (this.RegisteredCourseID.HasValue)
				{
					var offeredCourse = Aspire.BL.Core.ClassAttendance.Student.ClassAttendance.GetAttendanceSummary(this.RegisteredCourseID.Value);
					if (offeredCourse == null)
					{
						this.AddNoRecordFoundAlert();
						Redirect<RegisteredCourses>();
						return;
					}
					this.lblCourseTitle.Text = offeredCourse.Title;
					this.lblTeacherName.Text = offeredCourse.TeacherName;
					this.lblCreditHrs.Text = offeredCourse.CreditHours.ToCreditHoursFullName();
					this.lblContactHrs.Text = offeredCourse.ContactHours.ToCreditHoursFullName();
					this.lblClass.Text = offeredCourse.OfferedCourseClassName;
					this.lblSemesterID.Text = offeredCourse.SemesterID.ToSemesterString();
					this.gvAttendanceDetails.DataBind(offeredCourse.AttendanceDetails);
					if (this.gvAttendanceDetails.FooterRow != null)
					{
						this.gvAttendanceDetails.FooterRow.Cells[5].Text = offeredCourse.ClassesTotalHours.ToString("0.##");
						this.gvAttendanceDetails.FooterRow.Cells[6].Text = $"{offeredCourse.PresentTotalHours.ToString("0.##")} : {offeredCourse.PresentTotalPercentage.ToString("0.##")}%";
						this.gvAttendanceDetails.FooterRow.Cells[7].Text = offeredCourse.AbsentTotalHours.ToString("0.##");
					}
				}
			}
		}
	}
}