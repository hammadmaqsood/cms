﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Student.Pgp
{
	[AspirePage("6F008D06-E1EE-4521-B34B-B005513E78BC", UserTypes.Student, "~/Sys/Student/Pgp/Dashboard.aspx", "Thesis Dashboard", true, AspireModules.Pgp)]
	public partial class Dashboard : StudentPage
	{
		public override bool BreadcrumbVisible => false;
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_desktop);
		public override string PageTitle => "Thesis Dashboard";
		protected void Page_Load(object sender, EventArgs e)
		{

		}
	}
}