using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Student
{
	public abstract class StudentAndParentPage : AspireBasePage, IBreadcrumb, ISidebar, IProfileInfo
	{
		protected override void Authenticate()
		{
			if (this.StudentIdentity == null)
				throw new AspireAccessDeniedException(UserTypes.Student, UserTypes.Student);
		}

		private IStudentIdentity _studentIdentity;
		public IStudentIdentity StudentIdentity => this._studentIdentity ?? (this._studentIdentity = this.AspireIdentity as IStudentIdentity);

		public override AspireThemes AspireTheme => AspireDefaultThemes.Student;

		public virtual bool BreadcrumbVisible => true;

		public override bool PageTitleVisible => true;

		public virtual bool SidebarVisible => true;

		public virtual string SidebarHtml => SessionHelper.SideBarLinkHtml;

		public virtual string BreadcrumbHtml => SessionHelper.SideBarLinks?.GetBreadcrumbHtml(this.AspirePageAttribute.AspirePageGuid);

		public virtual bool ProfileInfoVisible => true;

		public virtual bool ProfileLinkVisible => true;

		public bool IsCustomProfileImage => false;

		public string CustomProfileImageUrl => null;

		public virtual string Username => this.StudentIdentity.Username;

		public string Name => this.StudentIdentity.Name;

		public string Email => this.StudentIdentity.Email;

		public virtual string ProfileNavigateUrl => GetPageUrl<Profile>();

		public virtual bool LogoffLinkVisible => true;

		public virtual string LogoffNavigateUrl => Logoff.GetPageUrl(UserLoginHistory.LogOffReasons.LogOff);
	}
}