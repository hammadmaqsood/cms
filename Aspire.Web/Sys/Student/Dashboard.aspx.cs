﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Student
{
	[AspirePage("{7AF23332-1275-44B3-9B3B-E6E0270E37AA}", UserTypes.Student, "~/Sys/Student/Dashboard.aspx", "Home", false, AspireModules.None)]
	public partial class Dashboard : StudentAndParentPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		public override bool BreadcrumbVisible => false;
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_desktop);
		public override string PageTitle => "Dashboard";
	}
}