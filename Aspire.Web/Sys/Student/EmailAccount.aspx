﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="EmailAccount.aspx.cs" Inherits="Aspire.Web.Sys.Student.EmailAccount" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:FormView runat="server" ID="fvStudent" AllowPaging="False" DefaultMode="ReadOnly" RenderOuterTable="False" ItemType="Aspire.BL.Core.StudentInformation.Student.Profile.Student">
		<ItemTemplate>
			<table class="table table-bordered table-condensed info table-responsive tableCol4">
				<tr>
					<th>Enrollment</th>
					<td>
						<%#: Item.Enrollment %>
					</td>
					<th>Registration No.</th>
					<td>
						<%#: Item.RegistrationNo.ToNAIfNullOrEmpty() %>
					</td>
				</tr>
				<tr>
					<th>Name</th>
					<td>
						<%#: Item.Name %>
					</td>
					<th>Program</th>
					<td>
						<%#: Item.ProgramAlias %>
					</td>
				</tr>
				<tr>
					<th>Personal Email</th>
					<td>
						<%#: Item.PersonalEmail %>
					</td>
					<th>University Email</th>
					<td>
						<%# Item.UniversityEmail.TrimAndMakeItNullIfEmpty().HtmlEncode() ?? "<em class=\"text-info\">Contact IT Department of your campus.</em>" %>
					</td>
				</tr>
			</table>
		</ItemTemplate>
	</asp:FormView>
	<aspire:AspirePanel ID="panel" runat="server" HeaderText="Reset Microsoft Password" PanelType="Danger">
		<p>
			<strong>Follow the below steps to reset password.</strong>
		</p>
		<ol>
			<li>Click on below <strong>Reset Password</strong> button and confirm to continue.</li>
			<li>The new randomly generated password will be displayed on the top of this page with success message.</li>
			<li>Save the newly generated password.</li>
			<li>If you want to set your own password after reset, click <strong><a target="_blank" title="https://prp.pern.edu.pk/ChangePass" href="https://prp.pern.edu.pk/ChangePass">here</a></strong> to change it.</li>
		</ol>
		<p>
			<strong>Note: You will be able to use the new password in 30-45 minutes after the reset/change.</strong>
		</p>
		<div class="pull-left">
		</div>
		<div class="text-center">
			<aspire:AspireButton runat="server" ButtonType="Danger" ConfirmMessage="Are you sure you want to continue?" ID="btnResetPassword" OnClick="btnResetPassword_Click" ValidationGroup="ResetPassword" Text="Reset Password" />
			&nbsp;
			<aspire:AspireHyperLinkButton runat="server" ButtonType="Primary" Text="Microsoft Portal" FontAwesomeIcon="brand_microsoft" Target="_blank" NavigateUrl="https://portal.office.com/" />
		</div>
	</aspire:AspirePanel>
</asp:Content>
