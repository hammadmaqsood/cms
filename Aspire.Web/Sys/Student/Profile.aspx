﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="Aspire.Web.Sys.Student.Profile" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:FormView runat="server" ID="fvStudent" AllowPaging="False" DefaultMode="ReadOnly" RenderOuterTable="False" OnItemCommand="fvStudent_OnItemCommand">
		<ItemTemplate>
			<div class="alert alert-info">
				<strong>Note!</strong> In case of any correction in below information, please contact Deputy Director Academics office.
			</div>
			<table class="table table-bordered table-condensed info table-responsive tableCol4">
				<tr>
					<th>Enrollment</th>
					<td>
						<%#: this.Eval(nameof(Aspire.BL.Core.StudentInformation.Student.Profile.Student.Enrollment)) %>
					</td>
					<th>Registration No.</th>
					<td>
						<%#: this.Eval(nameof(Aspire.BL.Core.StudentInformation.Student.Profile.Student.RegistrationNo)).ToNAIfNullOrEmpty() %>
					</td>
				</tr>
				<tr>
					<th>Name</th>
					<td>
						<%#: this.Eval(nameof(Aspire.BL.Core.StudentInformation.Student.Profile.Student.Name)) %>
					</td>
					<th>Father Name</th>
					<td>
						<%#: this.Eval(nameof(Aspire.BL.Core.StudentInformation.Student.Profile.Student.FatherName)) %>
					</td>
				</tr>
				<tr>
					<th>Program</th>
					<td>
						<%#: this.Eval(nameof(Aspire.BL.Core.StudentInformation.Student.Profile.Student.ProgramAlias)) %>
					</td>
					<th>Degree Duration</th>
					<td>
						<%#: this.Eval(nameof(Aspire.BL.Core.StudentInformation.Student.Profile.Student.DegreeDurationFullName)) %>
					</td>
				</tr>
				<tr>
					<th>Intake Semester</th>
					<td>
						<%#: this.Eval(nameof(Aspire.BL.Core.StudentInformation.Student.Profile.Student.IntakeSemester)) %>
					</td>
					<th>Max Semester</th>
					<td>
						<%#: this.Eval(nameof(Aspire.BL.Core.StudentInformation.Student.Profile.Student.MaxSemester)) %>
					</td>
				</tr>
				<tr>
					<th>Mobile No.</th>
					<td>
						<%#: this.Eval(nameof(Aspire.BL.Core.StudentInformation.Student.Profile.Student.Mobile)).ToNAIfNullOrEmpty() %>
					</td>
					<th>Phone No.</th>
					<td>
						<%#: this.Eval(nameof(Aspire.BL.Core.StudentInformation.Student.Profile.Student.Phone)).ToNAIfNullOrEmpty() %>
					</td>
				</tr>
				<tr>
					<th>Personal Email</th>
					<td>
						<%#: this.Eval(nameof(Aspire.BL.Core.StudentInformation.Student.Profile.Student.PersonalEmail)).ToNAIfNullOrEmpty() %>
						<small>
							<aspire:AspireLinkButton runat="server" CausesValidation="False" Text="Update" CommandName="EditPersonalEmail" />
						</small>
						<asp:Panel runat="server" CssClass="text-muted" Visible='<%# this.Eval(nameof(Aspire.BL.Core.StudentInformation.Student.Profile.Student.PersonalEmailNotVerified))!=null %>'>
							<%#: this.Eval(nameof(Aspire.BL.Core.StudentInformation.Student.Profile.Student.PersonalEmailNotVerified)) %>
							<small>(Not verified |
							<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="ResendConfirmationForPersonalEmail" Text="Resend confirmation email" />)							
							</small>
						</asp:Panel>
					</td>
					<th>University Email</th>
					<td>
						<%# this.Eval(nameof(Aspire.BL.Core.StudentInformation.Student.Profile.Student.UniversityEmail))?.ToString().TrimAndMakeItNullIfEmpty()??"<em class=\"text-info\">Contact System Administrator</em>" %>
					</td>
				</tr>
				<tr>
					<th>Current Address</th>
					<td colspan="3">
						<%#: this.Eval(nameof(Aspire.BL.Core.StudentInformation.Student.Profile.Student.CurrentAddress)).ToNAIfNullOrEmpty() %>
					</td>
				</tr>
				<tr>
					<th>Permanent Address</th>
					<td colspan="3">
						<%#: this.Eval(nameof(Aspire.BL.Core.StudentInformation.Student.Profile.Student.PermanentAddress)).ToNAIfNullOrEmpty() %>
					</td>
				</tr>
			</table>
		</ItemTemplate>
	</asp:FormView>
	<aspire:AspireModal runat="server" ID="modalPersonalEmail" HeaderText="Edit">
		<BodyTemplate>
			<div class="form1-horizontal">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Personal Email:" AssociatedControlID="tbPersonalEmail" />
					<aspire:AspireTextBox TextTransform="LowerCase" runat="server" ID="tbPersonalEmail" ValidationGroup="PersonalEmail" />
					<aspire:AspireStringValidator runat="server" ControlToValidate="tbPersonalEmail" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Email Address." ValidationGroup="PersonalEmail" AllowNull="False" ValidationExpression="Email" />
				</div>
			</div>
		</BodyTemplate>
		<FooterTemplate>
			<aspire:AspireButton runat="server" ButtonType="Primary" ValidationGroup="PersonalEmail" ID="btnUpdatePersonalEmail" Text="Update" OnClick="btnUpdatePersonalEmail_OnClick" />
			<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
