﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Student.FeeManagement.Reports
{
	[AspirePage("77515DC0-81E5-46CE-B3A5-37F887083DBC", UserTypes.Student, "~/Sys/Student/FeeManagement/Reports/ViewFeeChallan.aspx", "Fee Challan(s)", true, AspireModules.FeeManagement)]
	public partial class ViewFeeChallan : StudentAndParentPage, IReportViewer
	{
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Fee Challan(s)";

		public string ReportName => "Fee Challan(s)";
		public bool ExportToExcel => false;
		public bool ExportToWord => false;

		private List<int> StudentFeeIDs
		{
			get
			{
				try
				{
					return this.GetParameterValue(nameof(this.StudentFeeIDs)).Split(',').Select(s => s.ToInt()).ToList();
				}
				catch
				{
					return null;
				}
			}
		}

		public static string GetPageUrl(List<int> studentFeeIDs)
		{
			return GetPageUrl<ViewFeeChallan>().AttachQueryParams(nameof(StudentFeeIDs), string.Join(",", studentFeeIDs), ReportView.ReportFormat, ReportFormats.PDF, ReportView.Inline, true);
		}

		public static string GetPageUrl(int studentFeeID)
		{
			return GetPageUrl(new List<int> { studentFeeID });
		}

		public static void Redirect(List<int> studentFeeIDs)
		{
			Redirect(GetPageUrl(studentFeeIDs));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.StudentFeeIDs?.Any() != true)
					Redirect<Dashboard>();
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			Common.Methods.RenderFeeChallansReport<Dashboard>(reportViewer, this.StudentFeeIDs);
		}
	}
}