﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="FeeChallans.aspx.cs" Inherits="Aspire.Web.Sys.Student.FeeManagement.FeeChallans" %>

<%@ Import Namespace="Aspire.Web.Sys.Student.FeeManagement.Reports" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Register Src="~/Sys/Student/StudentInfoUserControl.ascx" TagPrefix="uc" TagName="StudentInfoUserControl" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc:StudentInfoUserControl runat="server" ID="ucStudentInfo" />
	<asp:Repeater runat="server" ID="repeaterStudentFees" ItemType="Aspire.Model.Entities.StudentFeeChallan">
		<HeaderTemplate>
			<table class="table table-bordered table-condensed table-hover table-striped">
				<caption>Student Fee</caption>
				<thead>
					<tr>
						<th>Semester</th>
						<th>Type</th>
						<th>Challan No.</th>
						<th>Amount</th>
						<th>Due Date</th>
						<th>Status</th>
						<th>Deposit Date</th>
						<th>Remarks</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
				<td>
					<%#: Item.StudentFee.Semester %>
				</td>
				<td>
					<%#: Item.StudentFee.FeeTypeEnum.ToFullName() %>
				</td>
				<td>
					<%#: AspireFormats.ChallanNo.GetFullChallanNoString(this.StudentIdentity.InstituteCode, Item.StudentFee.NewAdmission ? InstituteBankAccount.FeeTypes.NewAdmissionFee : InstituteBankAccount.FeeTypes.StudentFee, Item.StudentFeeChallanID) %>
				</td>
				<td>
					<%#: $"Rs. {Item.Amount}" %>
				</td>
				<td>
					<%#: Item.DueDate.ToShortDateString() %>
				</td>
				<td>
					<%#: Item.StatusEnum.ToFullName() %>
				</td>
				<td>
					<%#: (Item.AccountTransaction?.TransactionDate).ToNAIfNullOrEmpty() %>
				</td>
				<td>
					<%#: Item.StudentFee.Remarks.ToNAIfNullOrEmpty() %>
				</td>
				<td>
					<aspire:AspireHyperLink runat="server" Text="View" NavigateUrl='<%# ViewFeeChallan.GetPageUrl(Item.StudentFeeID) %>' Target="_blank" />
				</td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</tbody></table></div>
		</FooterTemplate>
	</asp:Repeater>
</asp:Content>
