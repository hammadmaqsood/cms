﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Student.FeeManagement
{
	[AspirePage("0715A7D7-D5EF-48B0-8F1B-155C9C420D34", UserTypes.Student, "~/Sys/Student/FeeManagement/FeeChallans", "Fee Challans", true, AspireModules.FeeManagement)]
	public partial class FeeChallans : StudentAndParentPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.list);
		public override string PageTitle => "Fee Challans";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var studentInfo = this.ucStudentInfo.DisplayStudentInformation(this.StudentIdentity.Enrollment, this.StudentIdentity.InstituteID);
				if (studentInfo == null)
				{
					this.AddNoRecordFoundAlert();
					return;
				}

				var (studentFeeChallans, reasons) = BL.Core.FeeManagement.Student.StudentFees.GetStudentFeeChallans(this.StudentIdentity.LoginSessionGuid);
				this.repeaterStudentFees.DataBind(studentFeeChallans);
				if (reasons?.Any(r => !r.IsWarning) == true)
				{
					this.AddInfoAlert("Your name has been removed from the attendance list(s) due to the following reasons.");
					reasons.Where(r => !r.IsWarning).ForEach(r => this.AddErrorAlert(r.ToString(true)));
				}
				else if (reasons?.Any(r => r.IsWarning) == true)
				{
					this.AddInfoAlert("Your name will be removed from the attendance list(s) due to the following reasons.");
					reasons.Where(r => r.IsWarning).ForEach(r => this.AddWarningAlert(r.ToString(true)));
				}
			}
		}
	}
}