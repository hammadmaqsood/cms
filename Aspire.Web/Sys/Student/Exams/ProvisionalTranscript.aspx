﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ProvisionalTranscript.aspx.cs" Inherits="Aspire.Web.Sys.Student.Exams.ProvisionalTranscript" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:Repeater runat="server" ID="repeaterTranscript">
		<ItemTemplate>
			<div class="row">
				<div class="col-md-6">
					Name: <strong><%#: Eval("BUName") %></strong>
				</div>
				<div class="col-md-6"></div>
			</div>
			<asp:Repeater runat="server" ID="repeaterSemesters" DataSource='<%# Eval("Semesters") %>'>
				<ItemTemplate>
					<div class="row">
						<div class="col-md-12">
							<strong><%#: Eval("BUName") %></strong>
						</div>
					</div>
					<asp:Repeater runat="server" ID="repeaterCourses" DataSource='<%# Eval("Semesters") %>'>
						<table class="table table-bordered table-responsive table-condensed table-hover">
							<caption>
								<%#:this.Eval("Semester") %></caption>
							<thead class="text-center">
								<tr>
									<th class="text-center">S#</th>
									<th class="text-center">Code</th>
									<th class="text-center">Title</th>
									<th class="text-center">Credit Hours</th>
									<th class="text-center">Grade</th>
									<th class="text-center">Grade Points</th>
									<th class="text-center">Product</th>
								</tr>
							</thead>
							<tbody>
								<asp:Repeater runat="server" DataSource='<%#this.Eval("RegisteredCourses") %>'>
									<ItemTemplate>
										<tr>
											<td class="text-center"><%#: Container.ItemIndex+1 %></td>
											<td class="text-center"><%#: this.Eval("CourseCode") %></td>
											<td><%#:this.Eval("Title") %></td>
											<td class="text-center"><%#: this.Eval("StringCreditHours") %></td>
											<td class="text-center"><%#: this.Eval("GradeEnumFullName") %></td>
											<td class="text-center"><%#: this.Eval("GradePoint") %></td>
											<td class="text-center"><%#: this.Eval("Product") %></td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
								<tr>
									<td colspan="6" class="text-right"><strong>GPA:</strong> <%#:this.Eval("StringGPA") %>
									</td>
									<td colspan="7"><strong>CGPA:</strong> <%#:this.Eval("StringCGPA") %>
									</td>
								</tr>
							</tbody>
						</table>
					</asp:Repeater>
				</ItemTemplate>
			</asp:Repeater>
		</ItemTemplate>
	</asp:Repeater>
</asp:Content>
