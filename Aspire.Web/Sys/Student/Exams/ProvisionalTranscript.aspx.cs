﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Student.Exams
{
	[AspirePage("580676C0-BCC9-4FC9-AD32-D055709DBCDC", UserTypes.Student, "~/Sys/Student/Exams/ProvisionalTranscript.aspx", "Provisional Transcript", true, AspireModules.Exams)]
	public partial class ProvisionalTranscript : StudentPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.education);
		public override string PageTitle => "Provisional Transcript";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var provisionalTranscript = BL.Core.Exams.Student.ExamResults.GetProvisionalTranscript(this.StudentIdentity.StudentID, this.StudentIdentity.LoginSessionGuid);
				if (provisionalTranscript == null)
				{
					this.AddNoRecordFoundAlert();
					this.repeaterExamResult.Visible = false;
					return;
				}
				this.repeaterExamResult.DataBind(new[] { provisionalTranscript });
			}
		}
	}
}