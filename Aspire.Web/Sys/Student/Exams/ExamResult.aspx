﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ExamResult.aspx.cs" Inherits="Aspire.Web.Sys.Student.Exams.ExamResult" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:Repeater runat="server" ID="repeaterExamResult" ItemType="Aspire.BL.Core.Exams.Student.ExamResults.StudentSemester">
		<ItemTemplate>
			<table class="table table-bordered table-responsive table-condensed table-hover">
				<caption><%#: Item.SemesterID.ToSemesterString() %></caption>
				<thead class="text-center">
					<tr>
						<th class="text-center">#</th>
						<th class="text-center">Code</th>
						<th class="text-center">Title</th>
						<th class="text-center">Majors</th>
						<th class="text-center">Credit Hours</th>
						<th class="text-center">Grade</th>
						<th class="text-center">Grade Points</th>
						<th class="text-center">Product</th>
					</tr>
				</thead>
				<tbody>
					<asp:Repeater runat="server" ItemType="Aspire.BL.Core.Exams.Student.ExamResults.StudentSemester.RegisteredCourse" DataSource='<%# Item.RegisteredCourses %>'>
						<ItemTemplate>
							<tr>
								<td class="text-center"><%#: Container.ItemIndex + 1 %></td>
								<td class="text-center"><%#: Item.CourseCode %></td>
								<td><%#: Item.Title %></td>
								<td class="text-center"><%#: Item.Majors.ToNAIfNullOrEmpty() %></td>
								<td class="text-center"><%#: Item.CreditHours.ToCreditHoursFullName() %></td>
								<td class="text-center"><%#: Item.GradeEnum.ToFullName().ToNAIfNullOrEmpty() %></td>
								<td class="text-center"><%#: Item.GradePoints.FormatGradePoints().ToNAIfNullOrEmpty() %></td>
								<td class="text-center"><%#: Item.Product.FormatProduct().ToNAIfNullOrEmpty() %></td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
					<tr>
						<td colspan="8" class="text-right">
							<strong>GPA:</strong> <%#: Item.BUExamGPA.FormatGPA().ToNAIfNullOrEmpty() %>, 
							<strong>CGPA:</strong> <%#: Item.BUExamCGPA.FormatGPA().ToNAIfNullOrEmpty() %><span runat="server" visible="<%# Item.BUExamResultRemarksEnum != null && Item.BUExamResultRemarksEnum.Value != Aspire.Model.Entities.StudentSemester.ResultRemarksTypes.None %>">,
								<strong>Remarks:</strong> <%#: Item.BUExamResultRemarksEnum?.ToFullName() %>
							</span>
						</td>
					</tr>
				</tbody>
			</table>
		</ItemTemplate>
	</asp:Repeater>
</asp:Content>
