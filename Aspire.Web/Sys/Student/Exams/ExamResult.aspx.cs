﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Student.Exams
{
	[AspirePage("0F44C6B7-4FAE-4927-B994-3B895BB77EB7", UserTypes.Student, "~/Sys/Student/Exams/ExamResult.aspx", "Provisional Result", true, AspireModules.Exams)]
	public partial class ExamResult : StudentAndParentPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.education);
		public override string PageTitle => "Provisional Result";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var studentSemesters = BL.Core.Exams.Student.ExamResults.GetExamResult(this.StudentIdentity.LoginSessionGuid);
				this.repeaterExamResult.DataBind(studentSemesters.OrderBy(ss => ss.SemesterID));
			}
		}
	}
}