﻿using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Sys.Student.QualityAssurance;
using System;

namespace Aspire.Web.Sys.Student
{
	[AspirePage("3F24EAAE-C56F-46A3-B7DA-BB46CE49EC9A", UserTypes.Student, "~/Sys/Student/PrepareSession.aspx", null, false, AspireModules.None)]
	public partial class PrepareSession : BasePage
	{
		protected override void Authenticate()
		{
			if (StudentIdentity.Current == null)
				throw new AspireAccessDeniedException(UserTypes.Student, UserTypes.Student);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SessionHelper.SideBarLinkHtml == null)
			{
				PrepareMenu();
			}

			Redirect(GetPageUrl<HECOnlineEducationSurvey>());
		}

		private static void PrepareMenu()
		{
			switch (StudentIdentity.Current.SubUserType)
			{
				case SubUserTypes.None:
					SessionHelper.SetSideBarLinks(DefaultMenus.Student.Links);
					break;
				case SubUserTypes.Parents:
					SessionHelper.SetSideBarLinks(DefaultMenus.StudentParent.Links);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}