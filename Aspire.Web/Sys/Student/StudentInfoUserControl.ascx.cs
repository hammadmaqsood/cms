﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Web.Sys.Student
{
	public partial class StudentInfoUserControl : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
		}

		public BasicStudentInformation DisplayStudentInformation(string enrollment, int instituteID)
		{
			var studentInfo = BL.Common.Common.GetBasicStudentInformation(instituteID, enrollment);
			if (studentInfo == null)
				return null;
			this.lbEnrollment.Text = studentInfo.Enrollment;
			this.lblRegistrationNo.Text = studentInfo.RegistrationNo.ToString().ToNAIfNullOrEmpty();
			this.lblName.Text = studentInfo.Name;
			this.lblProgram.Text = studentInfo.ProgramAlias;
			this.lblIntakeSemester.Text = studentInfo.SemesterID.ToSemesterString();
			this.lblStatus.Text = (studentInfo.StatusEnum?.ToFullName()).ToNAIfNullOrEmpty();
			this.panelStudentInfoEnrollment.Visible = true;
			this.ViewState[nameof(Model.Entities.Student.StudentID)] = studentInfo.StudentID;
			return studentInfo;
		}
	}
}