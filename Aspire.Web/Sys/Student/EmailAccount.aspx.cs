﻿using Aspire.Lib.WebControls;
using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Security.Authentication;

namespace Aspire.Web.Sys.Student
{
	[AspirePage("8d9c28e2-66fd-438d-8c55-c75e5cbe7d35", UserTypes.Student, "~/Sys/Student/EmailAccount.aspx", "Microsoft Email Account", true, AspireModules.None)]
	public partial class EmailAccount : StudentPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.envelope);

		public override string PageTitle => "Microsoft Email Account";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var student = BL.Core.StudentInformation.Student.Profile.GetStudentInfo(this.StudentIdentity.LoginSessionGuid);
				if (student == null)
					throw new InvalidOperationException();

				var universityEmail = student.UniversityEmail?.TrimAndMakeItNullIfEmpty()?.TryValidateEmailAddress();

				if (universityEmail == null)
				{
					this.AddErrorAlert("University Email address is invalid or missing.");
					Redirect<Profile>();
					return;
				}

				if (!universityEmail.EndsWith("@student.bahria.edu.pk"))
				{
					this.AddErrorAlert("University Email address must belong to <strong>student.bahria.edu.pk</strong> domain.", true);
					Redirect<Profile>();
					return;
				}

				if (universityEmail != $"{student.Enrollment}@student.bahria.edu.pk")
				{
					this.AddErrorAlert($"University Email address must be <strong>{student.Enrollment}@student.bahria.edu.pk</strong>. Please contact IT department of your campus.", true);
					Redirect<Profile>();
					return;
				}

				this.fvStudent.DataSource = new[] { student };
				this.fvStudent.DataBind();
				this.panel.HeaderText = $"Reset Password - Microsoft Account ({universityEmail})";
				this.UniversityEmail = universityEmail;
			}
		}

		protected string UniversityEmail { get; set; }

		protected void btnResetPassword_Click(object sender, EventArgs e)
		{
			var newPassword = GenerateRandomPassword();
			using (var handler = new HttpClientHandler())
			{
				handler.SslProtocols = SslProtocols.Tls12;
				handler.ServerCertificateCustomValidationCallback += (httpRequestMessage, cert, cetChain, policyErrors) => true;
				using (var httpClient = new HttpClient(handler))
				{
					var student = new
					{
						Email = this.StudentIdentity.Email,
						NewPassword = newPassword,
						ClientID = Lib.Helpers.Configurations.Current.AppSettings.PernClientID
					}.ToJsonString();
					var resetPasswordUrl = new Uri(Lib.Helpers.Configurations.Current.AppSettings.PernService.TrimEnd('/', '\\') + "/ResetPassword");
					using (var httpContent = new StringContent(student, Encoding.UTF8, "application/json"))
					using (var httpResponseMessage = httpClient.PostAsync(resetPasswordUrl, httpContent).Result)
					{
						var responseContents = httpResponseMessage.Content.ReadAsStringAsync().Result;
						if (httpResponseMessage.StatusCode != System.Net.HttpStatusCode.OK)
							throw new InvalidOperationException(responseContents);
						httpResponseMessage.EnsureSuccessStatusCode();
						this.AddSuccessAlert(responseContents);
						this.AddSuccessAlert($"New password: <strong>{newPassword}</strong>", true);
						Redirect<EmailAccount>();
					}
				}
			}
		}

		public static string GenerateRandomPassword()
		{
			var options = new
			{
				RequiredLength = 12,
				RequiredUniqueChars = 4,
				RequireDigit = true,
				RequireLowercase = true,
				RequireUppercase = true,
				RequireNonAlphanumeric = true
			};

			var randomChars = new[] { "ABCDEFGHJKLMNOPQRSTUVWXYZ", "abcdefghijkmnopqrstuvwxyz", "0123456789", "!@$?_-" };
			var rand = new Random(Environment.TickCount);
			var chars = new List<char>();

			if (options.RequireUppercase)
				chars.Insert(rand.Next(0, chars.Count), randomChars[0][rand.Next(0, randomChars[0].Length)]);

			if (options.RequireLowercase)
				chars.Insert(rand.Next(0, chars.Count), randomChars[1][rand.Next(0, randomChars[1].Length)]);

			if (options.RequireDigit)
				chars.Insert(rand.Next(0, chars.Count), randomChars[2][rand.Next(0, randomChars[2].Length)]);

			if (options.RequireNonAlphanumeric)
				chars.Insert(rand.Next(0, chars.Count), randomChars[3][rand.Next(0, randomChars[3].Length)]);

			for (int i = chars.Count; i < options.RequiredLength || chars.Distinct().Count() < options.RequiredUniqueChars; i++)
			{
				var rcs = randomChars[rand.Next(0, randomChars.Length)];
				chars.Insert(rand.Next(0, chars.Count), rcs[rand.Next(0, rcs.Length)]);
			}

			return new string(chars.ToArray());
		}
	}
}