﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Student.Forms.StudentClearanceForm
{
	[AspirePage("DED7FD9C-4915-465E-9B25-CB616275C5AA", UserTypes.Student, "~/Sys/Student/Forms/StudentClearanceForm/StudentClearanceForm.aspx", "Student Clearance Form", true, AspireModules.Forms)]
	public partial class StudentClearanceForm : StudentPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.menu_hamburger);
		public override string PageTitle => Model.Entities.Form.FormTypes.StudentClearanceForm.GetFullName();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var data = BL.Core.Forms.StudentClearanceForm.Common.StudentClearanceForm.GetStudentFormData(this.StudentIdentity.StudentID, null, this.StudentIdentity.LoginSessionGuid);
				if (data == null)
				{
					this.AddNoRecordFoundAlert();
					return;
				}

				this.clearanceForm.LoadData(data);
			}
		}
	}
}