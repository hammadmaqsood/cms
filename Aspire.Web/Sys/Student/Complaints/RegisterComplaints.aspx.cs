﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Student.Complaints
{
	[AspirePage("6C02F3EF-E25A-4EE5-9047-356A723001C1", UserTypes.Student, "~/Sys/Student/Complaints/RegisterComplaints.aspx", "Register Complaints", true, AspireModules.Complaints)]
	public partial class RegisterComplaints : StudentPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_pencil_alt);
		public override string PageTitle => "Register Complaints";
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(nameof(Complaint.ComplaintNo), SortDirection.Ascending);
				this.GetData(0);
			}
		}

		private void DisplayRegisterComplaintModel(Guid? complaintID)
		{
			var complaintTypes = BL.Core.Complaints.Student.RegisterComplaints.GetComplaintTypes(this.StudentIdentity.LoginSessionGuid);
			this.ddlComplaintType.DataBind(complaintTypes, CommonListItems.Select);
			if (complaintID == null)
			{
				this.modalRegisterComplaint.HeaderText = "Register Complaint";
				this.ddlComplaintType.ClearSelection();
				this.tbDescription.Text = null;
				this.modalRegisterComplaint.Show();
			}
		}

		protected void btnAddComplaint_OnClick(object sender, EventArgs e)
		{
			this.DisplayRegisterComplaintModel(null);
		}

		protected void btnSubmit_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var complaintTypeID = this.ddlComplaintType.SelectedValue.ToGuid();
			var description = this.tbDescription.Text;
			var result = BL.Core.Complaints.Student.RegisterComplaints.AddComplaint(complaintTypeID, description, this.StudentIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.Complaints.Student.RegisterComplaints.AddComplaintStatuses.AlreadyExists:
					this.AddErrorAlert("Complaint already exists with same complaint type.");
					break;
				case BL.Core.Complaints.Student.RegisterComplaints.AddComplaintStatuses.Success:
					this.AddSuccessAlert("Complaint has been submitted to SSC department.");
					break;
				case BL.Core.Complaints.Student.RegisterComplaints.AddComplaintStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					break;
				default:
					throw new NotImplementedEnumException(result);
			}
			Redirect<RegisterComplaints>();
		}

		protected void gvComplaints_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvComplaints.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvComplaints_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvComplaints_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			var complaints = BL.Core.Complaints.Common.ComplaintComment.GetComplaints(pageIndex, this.gvComplaints.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out int virtualItemCount, this.StudentIdentity.LoginSessionGuid);
			//	var disciplineCases = BL.Core.StudentInformation.Staff.DisciplineCase.GetDisciplineCases(pageIndex, this.gvDisciplineCases.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out virtualItemCount, this.StaffIdentity.LoginSessionGuid);
			this.gvComplaints.DataBind(complaints, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		protected void gvComplaints_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Comment":
					var complaintID = e.DecryptedCommandArgumentToGuid();
					this.ucComplaintComment.ShowModal(complaintID);
					break;
			}
		}
	}
}