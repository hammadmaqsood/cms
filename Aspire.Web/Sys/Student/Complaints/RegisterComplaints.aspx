﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="RegisterComplaints.aspx.cs" Inherits="Aspire.Web.Sys.Student.Complaints.RegisterComplaints" %>

<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Register Src="~/Sys/Common/Complaints/ComplaintComment.ascx" tagPrefix="uc1" tagName="ComplaintComment"  %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<style type="text/css">
		.removeMargin {
			margin: 0px !important;
		}
	</style>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<aspire:AspireButton runat="server" ID="btnAddComplaint" ButtonType="Success" Glyphicon="plus" Text="Add Complaint" OnClick="btnAddComplaint_OnClick" />
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Complaints <span class="glyphicon glyphicon-alert"></span></h3>
		</div>
		<aspire:AspireGridView runat="server" ID="gvComplaints" DataKeyNames="ComplaintID" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnPageSizeChanging="gvComplaints_OnPageSizeChanging" OnPageIndexChanging="gvComplaints_OnPageIndexChanging" OnSorting="gvComplaints_OnSorting" OnRowCommand="gvComplaints_OnRowCommand" CssClass="table-condensed removeMargin">
			<Columns>
				<asp:TemplateField HeaderText="#.">
					<ItemTemplate>
						<%#Container.DataItemIndex+1 %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="Complaint No" DataField="ComplaintNo" SortExpression="ComplaintNo" />
				<asp:BoundField HeaderText="Complaint Type" DataField="ComplaintTypeName" SortExpression="ComplaintTypeName" />
				<asp:BoundField HeaderText="Description" DataField="Description" SortExpression="Description" />
				<asp:BoundField HeaderText="Registered Date" DataField="RegisteredDate" SortExpression="RegisteredDate" DataFormatString="{0:dd-MMMM-yyyy hh:mm:ss tt}" />
				<asp:BoundField HeaderText="Status" DataField="StatusEnumToFullName" SortExpression="Status" />
				<asp:TemplateField HeaderText="Actions">
					<ItemTemplate>
						<aspire:AspireLinkButton runat="server" CommandName="Comment" Text="Comment" CausesValidation="False" Glyphicon="plus_sign" EncryptedCommandArgument="<%# this.Eval(nameof(Complaint.ComplaintID)) %>" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
	</div>
	<aspire:AspireModal runat="server" ID="modalRegisterComplaint" ModalSize="Large">
		<BodyTemplate>
			<%--	<aspire:AspireAlert runat="server" ID="alertModalRegisterComplaint" />--%>
			<div class="form-horizontal">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Complaint Type:" AssociatedControlID="ddlComplaintType" CssClass="col-md-2" />
					<div class="col-md-10">
						<aspire:AspireDropDownList runat="server" ID="ddlComplaintType" ValidationGroup="Required" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlComplaintType" ErrorMessage="This field is required." ValidationGroup="Required" />
					</div>
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Decription:" AssociatedControlID="tbDescription" CssClass="col-md-2" />
					<div class="col-md-10">
						<aspire:AspireTextBox runat="server" ID="tbDescription" ValidationGroup="Required" TextMode="MultiLine"/>
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbDescription" ErrorMessage="This field is required." ValidationGroup="Required" />
						<aspire:Label runat="server" CssClass="help-block" Text="Complaints with incomplete information will not be entertained." />
					</div>

				</div>
			</div>
		</BodyTemplate>
		<FooterTemplate>
			<aspire:AspireButton runat="server" ID="btnSubmit" Text="Submit" ButtonType="Primary" ValidationGroup="Required" OnClick="btnSubmit_OnClick" ConfirmMessage="Are you sure you want to submit the complaint?" />
			<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
		</FooterTemplate>
	</aspire:AspireModal>
	<uc1:ComplaintComment runat="server" ID="ucComplaintComment" />
</asp:Content>
