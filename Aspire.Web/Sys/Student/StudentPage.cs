using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Student
{
	public abstract class StudentPage : StudentAndParentPage
	{
		protected override void Authenticate()
		{
			if (this.StudentIdentity == null || this.StudentIdentity.SubUserType != SubUserTypes.None)
				throw new AspireAccessDeniedException(UserTypes.Student, UserTypes.Student);
		}
	}
}