﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Student
{
	[AspirePage("77D8DAD7-9C19-43C8-9DCC-9DB3B5BB180C", UserTypes.Student, "~/Sys/Student/ConfirmPersonalEmail.aspx", null, false, AspireModules.StudentInformation)]
	public partial class ConfirmPersonalEmail : AspireBasePage
	{
		public override AspireThemes AspireTheme => AspireDefaultThemes.Student;
		public override PageIcon PageIcon => AspireGlyphicons.envelope.GetIcon();
		public override string PageTitle => "Email Confirmation";

		protected override void Authenticate()
		{
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			var studentID = this.Request.GetParameterValue<int>("StudentID");
			var personalEmail = this.Request.GetParameterValue("PersonalEmail");
			if (studentID == null || string.IsNullOrWhiteSpace(personalEmail))
			{
				Redirect<Logins.Student.Login>();
				return;
			}

			var confirmed = Aspire.BL.Core.StudentInformation.Student.Profile.ConfirmPersonalEmail(studentID.Value, personalEmail);
			if (confirmed)
				this.AddSuccessAlert($"Email address {personalEmail} has been set as Personal Email address.");
			else
				this.AddErrorAlert("Confirmation link has been expired.");
		}
	}
}