﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Student.OfficialDocuments
{
	[AspirePage("CDDE0E8E-E4B3-45C4-82A6-2D21FC2FA88D", UserTypes.Student, "~/Sys/Student/OfficialDocuments/Documents.aspx", "Official Documents", true, AspireModules.OfficialDocuments)]
	public partial class Documents : StudentPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.book);
		public override string PageTitle => "Official Documents";

		protected void Page_Load(object sender, EventArgs e)
		{

		}
	}
}