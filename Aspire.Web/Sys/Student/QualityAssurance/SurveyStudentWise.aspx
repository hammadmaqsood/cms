﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="SurveyStudentWise.aspx.cs" Inherits="Aspire.Web.Sys.Student.QualityAssurance.SurveyStudentWise" %>

<%@ Register Src="~/Sys/Common/SurveyUserControl.ascx" TagPrefix="uc1" TagName="SurveyUserControl" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc1:SurveyUserControl runat="server" ID="surveyUserControl" OnSurveySubmitted="SurveyUserControl_OnSurveySubmitted" />
</asp:Content>
