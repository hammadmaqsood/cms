﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Sys.Common;
using System;

namespace Aspire.Web.Sys.Student.QualityAssurance
{
	[AspirePage("665D6B2F-54A6-462B-A1BF-B3CB15C720B7", UserTypes.Student, "~/Sys/Student/QualityAssurance/SurveyStudentCourseWise.aspx", "Survey Student Course Wise", true, AspireModules.QualityAssurance)]
	public partial class SurveyStudentCourseWise : StudentPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.question_sign);
		public override string PageTitle => "Survey";

		public static string GetPageUrl(int surveyConductID, int studentID, int registeredCourseID)
		{
			return GetPageUrl<SurveyStudentCourseWise>().AttachQueryParams("SurveyConductID", surveyConductID, "StudentID", studentID, "RegisteredCourseID", registeredCourseID);
		}

		private int? StudentID => this.Request.GetParameterValue<int>("StudentID");
		private int? SurveyConductID => this.Request.GetParameterValue<int>("SurveyConductID");
		private int? RegisteredCourseID => this.Request.GetParameterValue<int>("RegisteredCourseID");

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.SurveyConductID == null || this.RegisteredCourseID == null || this.StudentID != this.StudentIdentity.StudentID)
				{
					Redirect<QualityAssuranceSurveys>();
					return;
				}

				var courseInfo = BL.QualityAssurance.Student.GetRegisteredCourseInfo(this.StudentID.Value, this.RegisteredCourseID.Value);
				if (courseInfo == null) return;
				this.lblTitle.Text = courseInfo.Title;
				this.lblSemester.Text = courseInfo.SemesterID.ToSemesterString();
				this.lblClass.Text = AspireFormats.GetClassName(courseInfo.Program,courseInfo.SemesterNo,courseInfo.Section,courseInfo.Shift);
				this.lblTeacher.Text = courseInfo.Teacher;
			}
		}

		protected void surveyUserControl_OnSurveySubmitted(object sender, SurveyUserControl.SurveyEventArgs e)
		{
			this.surveyUserControl.DisplayMessageAndRedirect<QualityAssuranceSurveys>(e);
		}
	}
}
