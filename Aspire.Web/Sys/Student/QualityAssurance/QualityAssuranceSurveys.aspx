﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="QualityAssuranceSurveys.aspx.cs" Inherits="Aspire.Web.Sys.Student.QualityAssurance.QualityAssuranceSurveys" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<aspire:AspireGridView runat="server" ID="gvSurveyConducts" Caption="Surveys (Course Wise)" AutoGenerateColumns="False">
		<Columns>
			<asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
				<ItemTemplate>
					<%#: Container.DataItemIndex+1 %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Semester" DataField="Semester" />
			<asp:BoundField HeaderText="Name" DataField="SurveyName" />
			<asp:BoundField HeaderText="Title" DataField="Title" />
			<asp:BoundField HeaderText="Class" DataField="ClassName" />
			<asp:BoundField HeaderText="Teacher" DataField="FacultyMemberName" />
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" Text="Evaluate" NavigateUrl='<%# this.GetPageUrl(Container.DataItem) %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
