﻿using System;
using System.Collections.Generic;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Student.QualityAssurance
{
	[AspirePage("3716B14D-3B31-4D67-BFA1-CCACD305AB6E", UserTypes.Anonymous, "~/Sys/Student/QualityAssurance/AlumniSurvey.aspx", "Survey Form-7", true, AspireModules.None)]
	public partial class AlumniSurvey : AnonymousPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.lock_);
		public override string PageTitle => "Alumni Survey (Survey Form-7)";
		public override AspireThemes AspireTheme => AspireDefaultThemes.Student;
		private Guid? SurveyCode => this.Request.Params["C"].TryToGuid();
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var code = this.SurveyCode;
				if (code == null)
				{
					this.AddErrorAlert("You are not authorized to access this form.");
					this.QAsurveyUserControl.Visible = false;
					return;
				}
				var surveyUserID = BL.Core.QualityAssurance.Student.Alumni.GetSurveyUser(code.Value);
				this.QAsurveyUserControl.LoadData(surveyUserID.QASurveyUserID);
				this.QAsurveyUserControl.Visible = true;
			}
		}
		public static string GetPageUrl(int? qaSurveyUserID)
		{
			return GetAspirePageAttribute<AlumniSurvey>().PageUrl.AttachQueryParam("qaSurveyUserID", qaSurveyUserID);
		}

		public static void Redirect(int? qaSurveyUserID)
		{
			Redirect(GetPageUrl(qaSurveyUserID));
		}
	}
}