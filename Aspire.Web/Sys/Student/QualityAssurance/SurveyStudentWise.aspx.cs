﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Sys.Common;
using System;

namespace Aspire.Web.Sys.Student.QualityAssurance
{
	[AspirePage("79CD5874-C0A5-46E9-8A27-C519A0B7B34D", UserTypes.Student, "~/Sys/Student/QualityAssurance/SurveyStudentWise.aspx", "Survey Student Wise", true, AspireModules.QualityAssurance)]
	public partial class SurveyStudentWise : StudentPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.question_sign);
		public override string PageTitle => "Survey";

		public static string GetPageUrl(int surveyConductID, int studentID)
		{
			return GetPageUrl<SurveyStudentWise>().AttachQueryParams("SurveyConductID", surveyConductID, "StudentID", studentID);
		}

		private int? SurveyConductID => this.Request.GetParameterValue<int>("SurveyConductID");
		private int? StudentID => this.Request.GetParameterValue<int>("StudentID");

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.SurveyConductID == null || this.StudentID != this.StudentIdentity.StudentID)
				{
					Redirect<QualityAssuranceSurveys>();
					return;
				}
			}
		}

		protected void SurveyUserControl_OnSurveySubmitted(object sender, SurveyUserControl.SurveyEventArgs e)
		{
			this.surveyUserControl.DisplayMessageAndRedirect<QualityAssuranceSurveys>(e);
		}
	}
}