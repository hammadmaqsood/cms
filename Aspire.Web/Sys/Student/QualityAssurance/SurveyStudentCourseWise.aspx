﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="SurveyStudentCourseWise.aspx.cs" Inherits="Aspire.Web.Sys.Student.QualityAssurance.SurveyStudentCourseWise" %>

<%@ Register Src="~/Sys/Common/SurveyUserControl.ascx" TagPrefix="uc1" TagName="SurveyUserControl" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="table-responsive">
		<style scoped="scoped">
			table#course th {
				white-space: nowrap;
			}

			table#course td {
				width: 50%;
			}
		</style>
		<table class="table table-bordered table-condensed" id="course">
			<caption>Course Information</caption>
			<tbody>
				<tr>
					<th>Title</th>
					<td>
						<aspire:Label runat="server" ID="lblTitle" /></td>
					<th>Semester</th>
					<td>
						<aspire:Label runat="server" ID="lblSemester" /></td>
				</tr>
				<tr>
					<th>Teacher</th>
					<td>
						<aspire:Label runat="server" ID="lblTeacher" /></td>
					<th>Class</th>
					<td>
						<aspire:Label runat="server" ID="lblClass" /></td>
				</tr>
			</tbody>
		</table>
	</div>
	<uc1:SurveyUserControl runat="server" ID="surveyUserControl" OnSurveySubmitted="surveyUserControl_OnSurveySubmitted" />
</asp:Content>
