﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="HECOnlineEducationSurvey.aspx.cs" Inherits="Aspire.Web.Sys.Student.QualityAssurance.HECOnlineEducationSurvey" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<h3>Availability of Internet, Connectivity Issues, and Quality of Online Education</h3>
	<span class="help-block">Instructions:	(i) Please complete all fields. (ii) Your inputs are very important and will help in improving the quality of online education. (iii) Your responses are confidential.</span>

	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="1. Whatsapp Number?" AssociatedControlID="tbWhatsappNo" />
		<aspire:AspireTextBox runat="server" ID="tbWhatsappNo" MaxLength="20" data-inputmask="'alias': 'mobile'" />
		<aspire:AspireStringValidator ValidationGroup="Save" runat="server" ControlToValidate="tbWhatsappNo" AllowNull="true" ValidationExpression="Mobile" InvalidDataErrorMessage="Invalid Whatsapp Number." RequiredErrorMessage="This field is required." />
	</div>
	<hr />
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="2. What is the best way of contacting you?" AssociatedControlID="rblQ12" />
		<div>
			<aspire:AspireRadioButtonList runat="server" ID="rblQ12" AutoPostBack="false" RepeatDirection="Vertical" RepeatLayout="Flow" />
		</div>
		<aspire:AspireRequiredFieldValidator ValidationGroup="Save" runat="server" ControlToValidate="rblQ12" ErrorMessage="This field is required." />
	</div>
	<hr />
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="3. How do you USUALLY access the Internet?" AssociatedControlID="rblQ13" />
		<div>
			<aspire:AspireRadioButtonList runat="server" ID="rblQ13" AutoPostBack="false" RepeatDirection="Vertical" RepeatLayout="Flow" />
		</div>
		<aspire:AspireRequiredFieldValidator ValidationGroup="Save" runat="server" ControlToValidate="rblQ13" ErrorMessage="This field is required." />
	</div>
	<hr />
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="4. If you use a Mobile package, please check relevant box" AssociatedControlID="rblQ14" />
		<div>
			<aspire:AspireRadioButtonList runat="server" ID="rblQ14" AutoPostBack="false" RepeatDirection="Vertical" RepeatLayout="Flow" />
		</div>
		<aspire:AspireRequiredFieldValidator ValidationGroup="Save" runat="server" ControlToValidate="rblQ14" ErrorMessage="This field is required." />
	</div>
	<hr />
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="5. If you have Mobile Access, what is the name of the Service Provider (e.g., PTCL, Jazz, Zong, Telenor)" AssociatedControlID="tbQ15" />
		<aspire:AspireTextBox runat="server" ValidateRequestMode="Disabled" ID="tbQ15" MaxLength="50" />
		<aspire:AspireRequiredFieldValidator ValidationGroup="Save" runat="server" ControlToValidate="tbQ15" ErrorMessage="This field is required." />
	</div>
	<hr />
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="6. Do you have a TV at home? If yes, please specify" AssociatedControlID="rblQ16" />
		<div>
			<aspire:AspireRadioButtonList runat="server" ID="rblQ16" AutoPostBack="false" RepeatDirection="Vertical" RepeatLayout="Flow" />
		</div>
		<aspire:AspireRequiredFieldValidator ValidationGroup="Save" runat="server" ControlToValidate="rblQ16" ErrorMessage="This field is required." />
	</div>
	<hr />
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="7. How much load-shedding is there in your area/place of residence?" AssociatedControlID="rblQ17" />
		<div>
			<aspire:AspireRadioButtonList runat="server" ID="rblQ17" AutoPostBack="false" RepeatDirection="Vertical" RepeatLayout="Flow" />
		</div>
		<aspire:AspireRequiredFieldValidator ValidationGroup="Save" runat="server" ControlToValidate="rblQ17" ErrorMessage="This field is required." />
	</div>
	<hr />
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="8. Has your university arranged online courses during the COVID-19 pandemic" AssociatedControlID="rblQ18" />
		<div>
			<aspire:AspireRadioButtonList runat="server" ID="rblQ18" AutoPostBack="false" RepeatDirection="Vertical" RepeatLayout="Flow" />
		</div>
		<aspire:AspireRequiredFieldValidator ValidationGroup="Save" runat="server" ControlToValidate="rblQ18" ErrorMessage="This field is required." />
	</div>
	<hr />
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="9. Which software does your university use for delivery of online lectures?" AssociatedControlID="cblQ19" />
		<div>
			<aspire:AspireCheckBoxList runat="server" ID="cblQ19" AutoPostBack="false" RepeatDirection="Vertical" RepeatLayout="Flow" />
		</div>
		<aspire:AspireRequiredFieldValidator ValidationGroup="Save" runat="server" ControlToValidate="cblQ19" ErrorMessage="This field is required." />
	</div>
	<hr />
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="10. Are you satisfied with the quality of online teaching?" AssociatedControlID="rblQ20" />
		<div>
			<aspire:AspireRadioButtonList runat="server" ID="rblQ20" AutoPostBack="false" RepeatDirection="Vertical" RepeatLayout="Flow" />
		</div>
		<aspire:AspireRequiredFieldValidator ValidationGroup="Save" runat="server" ControlToValidate="rblQ20" ErrorMessage="This field is required." />
	</div>
	<hr />
	<asp:UpdatePanel runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="11. What can be done to improve the quality of online teaching?" AssociatedControlID="cblQ21" />
				<div>
					<aspire:AspireCheckBoxList runat="server" ID="cblQ21" AutoPostBack="True" RepeatDirection="Vertical" RepeatLayout="Flow" OnSelectedIndexChanged="cblQ21_SelectedIndexChanged" />
				</div>
				<aspire:AspireRequiredFieldValidator ValidationGroup="Save" runat="server" ControlToValidate="cblQ21" ErrorMessage="This field is required." />
				<aspire:AspireTextBox runat="server" ValidateRequestMode="Disabled" ID="tbQ21Other" MaxLength="500" Rows="3" TextMode="MultiLine" />
				<aspire:AspireRequiredFieldValidator ID="rfvtbQ21Other" ValidationGroup="Save" runat="server" ControlToValidate="tbQ21Other" ErrorMessage="This field is required." />
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>
	<div class="form-group text-center">
		<aspire:AspireButton runat="server" ID="btnSaveAndSubmit" ValidationGroup="Save" Text="Save & Submit" OnClick="btnSaveAndSubmit_Click" ConfirmMessage="Are you sure you want to submit?" />
	</div>
</asp:Content>
