﻿using Aspire.BL.Entities.QualityAssurance.Student;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Student.QualityAssurance
{
	[AspirePage("E2E9A6E0-4B37-4F91-9BD7-24CB7BC4104E", UserTypes.Student, "~/Sys/Student/QualityAssurance/QualityAssuranceSurveys.aspx", "Quality Assurance Surveys", true, AspireModules.QualityAssurance)]
	public partial class QualityAssuranceSurveys : StudentPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Quality Assurance Surveys";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var surveyConducts = BL.QualityAssurance.Student.GetStudentSurveys(this.StudentIdentity.StudentID);
				this.gvSurveyConducts.DataBind(surveyConducts);
			}
		}

		protected string GetPageUrl(object dataItem)
		{
			var surveyConduct = (CustomStudentSurveyConduct)dataItem;
			switch (surveyConduct.SurveyTypeEnum)
			{
				case Survey.SurveyTypes.StudentWise:
					return SurveyStudentWise.GetPageUrl(surveyConduct.SurveyConductID, surveyConduct.StudentID);
				case Survey.SurveyTypes.StudentCourseWise:
					return SurveyStudentCourseWise.GetPageUrl(surveyConduct.SurveyConductID, surveyConduct.StudentID, surveyConduct.RegisteredCourseID.Value);
				case Survey.SurveyTypes.FacultyMemberWise:
				case Survey.SurveyTypes.FacultyMemberCourseWise:
					throw new InvalidOperationException();
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}