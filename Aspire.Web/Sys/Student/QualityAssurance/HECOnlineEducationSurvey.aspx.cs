﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Student.QualityAssurance
{
	[AspirePage("531d9f6e-6be5-47ef-b594-fbd316aeb42b", UserTypes.Student, "~/Sys/Student/QualityAssurance/HECOnlineEducationSurvey.aspx", "HEC Online Education Survey ", true, AspireModules.QualityAssurance)]
	public partial class HECOnlineEducationSurvey : StudentPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Student Survey";
		public override bool SidebarVisible => false;
		public override bool BreadcrumbVisible => false;
		internal static short SemesterID => 20201;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var qaStudentOnlineTeachingSurvey = BL.Core.QualityAssurance.Student.HECOnlineEducationSurvey.GetQAStudentOnlineTeachingSurvey(SemesterID, this.StudentIdentity.LoginSessionGuid);
				if (qaStudentOnlineTeachingSurvey?.SubmittedDate != null)
				{
					Redirect<Dashboard>();
					return;
				}
				this.tbWhatsappNo.Text = null;
				this.rblQ12.FillGuidEnums<QAStudentOnlineTeachingSurvey.Q12Options>();
				this.rblQ13.FillGuidEnums<QAStudentOnlineTeachingSurvey.Q13Options>();
				this.rblQ14.FillGuidEnums<QAStudentOnlineTeachingSurvey.Q14Options>();
				this.tbQ15.Text = null;
				this.rblQ16.FillGuidEnums<QAStudentOnlineTeachingSurvey.Q16Options>();
				this.rblQ17.FillGuidEnums<QAStudentOnlineTeachingSurvey.Q17Options>();
				this.rblQ18.FillGuidEnums<QAStudentOnlineTeachingSurvey.Q18Options>();
				this.cblQ19.FillGuidEnums<QAStudentOnlineTeachingSurvey.Q19Options>();
				this.rblQ20.FillGuidEnums<QAStudentOnlineTeachingSurvey.Q20Options>();
				this.cblQ21.FillGuidEnums<QAStudentOnlineTeachingSurvey.Q21Options>();
				this.tbQ21Other.Text = null;

				if (qaStudentOnlineTeachingSurvey != null)
				{
					this.tbWhatsappNo.Text = qaStudentOnlineTeachingSurvey.Q11WhatsappNo;
					this.rblQ12.SetSelectedGuidEnum((QAStudentOnlineTeachingSurvey.Q12Options)qaStudentOnlineTeachingSurvey.Q12);
					this.rblQ13.SetSelectedGuidEnum((QAStudentOnlineTeachingSurvey.Q13Options)qaStudentOnlineTeachingSurvey.Q13);
					this.rblQ14.SetSelectedGuidEnum((QAStudentOnlineTeachingSurvey.Q14Options)qaStudentOnlineTeachingSurvey.Q14);
					this.tbQ15.Text = qaStudentOnlineTeachingSurvey.Q15;
					this.rblQ16.SetSelectedGuidEnum((QAStudentOnlineTeachingSurvey.Q16Options)qaStudentOnlineTeachingSurvey.Q16);
					this.rblQ17.SetSelectedGuidEnum((QAStudentOnlineTeachingSurvey.Q17Options)qaStudentOnlineTeachingSurvey.Q17);
					this.rblQ18.SetSelectedGuidEnum((QAStudentOnlineTeachingSurvey.Q18Options)qaStudentOnlineTeachingSurvey.Q18);
					this.cblQ19.SetSelectedGuidEnums((QAStudentOnlineTeachingSurvey.Q19Options)qaStudentOnlineTeachingSurvey.Q19);
					this.rblQ20.SetSelectedGuidEnum((QAStudentOnlineTeachingSurvey.Q20Options)qaStudentOnlineTeachingSurvey.Q20);
					this.cblQ21.SetSelectedGuidEnums((QAStudentOnlineTeachingSurvey.Q21Options)qaStudentOnlineTeachingSurvey.Q21);
					this.tbQ21Other.Text = qaStudentOnlineTeachingSurvey.Q21Other;
				}
				this.cblQ21_SelectedIndexChanged(null, null);
			}
		}

		protected void cblQ21_SelectedIndexChanged(object sender, EventArgs e)
		{
			var q21 = this.cblQ21.GetSelectedGuidEnums<QAStudentOnlineTeachingSurvey.Q21Options>();
			var otherSelected = q21.Contains(QAStudentOnlineTeachingSurvey.Q21Options.Other);
			this.tbQ21Other.ReadOnly = !otherSelected;
			this.rfvtbQ21Other.Enabled = otherSelected;
			if (this.tbQ21Other.ReadOnly)
			{
				this.tbQ21Other.Text = null;
				this.tbQ21Other.Visible = false;
				this.rfvtbQ21Other.Visible = false;
			}
			else
			{
				this.tbQ21Other.Visible = true;
				this.rfvtbQ21Other.Visible = true;
			}
		}

		protected void btnSaveAndSubmit_Click(object sender, EventArgs e)
		{
			var qaStudentOnlineTeachingSurvey = new Model.Entities.QAStudentOnlineTeachingSurvey
			{
				QAStudentOnlineTeachingSurveyID = default,
				StudentID = this.StudentIdentity.StudentID,
				SemesterID = SemesterID,
				SubmittedDate = DateTime.Now,
				Q11WhatsappNo = this.tbWhatsappNo.Text,
				Q12 = (int)this.rblQ12.GetSelectedGuidEnum<QAStudentOnlineTeachingSurvey.Q12Options>(),
				Q13 = (int)this.rblQ13.GetSelectedGuidEnum<QAStudentOnlineTeachingSurvey.Q13Options>(),
				Q14 = (int)this.rblQ14.GetSelectedGuidEnum<QAStudentOnlineTeachingSurvey.Q14Options>(),
				Q15 = this.tbQ15.Text,
				Q16 = (int)this.rblQ16.GetSelectedGuidEnum<QAStudentOnlineTeachingSurvey.Q16Options>(),
				Q17 = (int)this.rblQ17.GetSelectedGuidEnum<QAStudentOnlineTeachingSurvey.Q17Options>(),
				Q18 = (int)this.rblQ18.GetSelectedGuidEnum<QAStudentOnlineTeachingSurvey.Q18Options>(),
				Q19 = this.cblQ19.GetSelectedGuidEnums<QAStudentOnlineTeachingSurvey.Q19Options>().Cast<int>().ExclusiveOr(),
				Q20 = (int)this.rblQ20.GetSelectedGuidEnum<QAStudentOnlineTeachingSurvey.Q20Options>(),
				Q21 = this.cblQ21.GetSelectedGuidEnums<QAStudentOnlineTeachingSurvey.Q21Options>().Cast<int>().ExclusiveOr(),
				Q21Other = this.tbQ21Other.Text
			};
			var status = BL.Core.QualityAssurance.Student.HECOnlineEducationSurvey.UpdateQAStudentOnlineTeachingSurvey(qaStudentOnlineTeachingSurvey, this.StudentIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.QualityAssurance.Student.HECOnlineEducationSurvey.UpdateQAStudentOnlineTeachingSurveyStatuses.AlreadySubmitted:
					this.AddErrorAlert("Survey is already submitted.");
					Redirect<HECOnlineEducationSurvey>();
					return;
				case BL.Core.QualityAssurance.Student.HECOnlineEducationSurvey.UpdateQAStudentOnlineTeachingSurveyStatuses.Saved:
					this.AddSuccessAlert("Survey has been saved.");
					Redirect<HECOnlineEducationSurvey>();
					return;
				case BL.Core.QualityAssurance.Student.HECOnlineEducationSurvey.UpdateQAStudentOnlineTeachingSurveyStatuses.Submitted:
					this.AddSuccessAlert("Survey has been submitted.");
					Redirect<HECOnlineEducationSurvey>();
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}
	}
}