﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Student.Scholarships
{
	[AspirePage("2E7457C9-5C96-4564-9D2C-B05CD466FB91", UserTypes.Student, "~/Sys/Student/Scholarships/QarzeHasanaForm.aspx", "Qarz-e-Hasana Form", true, AspireModules.Scholarship)]
	public partial class QarzeHasanaForm : StudentPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.education);
		public override string PageTitle => Scholarship.ScholarshipTypes.QarzeHasana.ToFullName();

		public static string GetPageUrl(int scholarshipAnnouncementID)
		{
			return GetPageUrl<QarzeHasanaForm>().AttachQueryParam(nameof(ScholarshipAnnouncementID), scholarshipAnnouncementID);
		}

		public static void Redirect(int scholarshipAnnouncementID)
		{
			Redirect(GetPageUrl(scholarshipAnnouncementID));
		}

		private int? ScholarshipAnnouncementID => this.Request.GetParameterValue<int>(nameof(this.ScholarshipAnnouncementID));

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.ScholarshipAnnouncementID == null)
				{
					Redirect<AvailableScholarships>();
					return;
				}

				var applicationInfo = BL.Core.Scholarship.Common.QarzeHasana.ScholarshipApplication.GetApplicationInfo(this.ScholarshipAnnouncementID.Value, this.StudentIdentity.StudentID, this.StudentIdentity.LoginSessionGuid);
				if (applicationInfo == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<AvailableScholarships>();
					return;
				}

				this.qarzeHasanaForm.LoadData(applicationInfo, this.ScholarshipAnnouncementID.Value, this.StudentIdentity.StudentID);
			}
		}
	}
}
