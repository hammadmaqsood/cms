﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Student.Scholarships
{
	[AspirePage("FFA82FC4-F2F8-46E2-8523-8E50F8434800", UserTypes.Student, "~/Sys/Student/Scholarships/AvailableScholarships.aspx", "Available Scholarships", true, AspireModules.Scholarship)]
	public partial class AvailableScholarships : StudentPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.education);
		public override string PageTitle => "Available Scholarships";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.GetData(0);
			}
		}

		private void GetData(int pageIndex)
		{
			var scholarshipAnnouncements = BL.Core.Scholarship.Student.ScholarshipAnnouncements.GetAnnouncedScholarships(this.StudentIdentity.InstituteID, this.StudentIdentity.LoginSessionGuid);
			this.repeaterScholarshipAnnouncements.DataBind(scholarshipAnnouncements);
			this.trNoRecordFound.Visible = !scholarshipAnnouncements.Any();
		}

		public static string GetPageURLForScholarshipForm(Model.Entities.Scholarship.ScholarshipTypes scholarshipTypeEnum, int scholarshipAnnouncementID)
		{
			switch (scholarshipTypeEnum)
			{
				case Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana:
					return QarzeHasanaForm.GetPageUrl(scholarshipAnnouncementID);
				case Model.Entities.Scholarship.ScholarshipTypes.StudentStudyLoan:
					return QarzeHasanaForm.GetPageUrl(scholarshipAnnouncementID);
				default:
					throw new NotImplementedEnumException(scholarshipTypeEnum);
			}
		}

		public static string GetPageURLForStatus(Model.Entities.Scholarship.ScholarshipTypes scholarshipTypeEnum, int? scholarshipApplicationID)
		{
			switch (scholarshipTypeEnum)
			{
				case Model.Entities.Scholarship.ScholarshipTypes.QarzeHasana:
					return ApprovalStatus.GetPageURL(scholarshipApplicationID);
				case Model.Entities.Scholarship.ScholarshipTypes.StudentStudyLoan:
					return ApprovalStatus.GetPageURL(scholarshipApplicationID);
				default:
					throw new NotImplementedEnumException(scholarshipTypeEnum);
			}
		}
	}
}