﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="AvailableScholarships.aspx.cs" Inherits="Aspire.Web.Sys.Student.Scholarships.AvailableScholarships" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Model.Entities" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">

	<div class="table-responsive">
		<table class="table table-bordered table-responsive table-condensed table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Semester</th>
					<th>Scholarship</th>
					<th>Status</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<asp:Repeater runat="server" ItemType="Aspire.BL.Core.Scholarship.Student.ScholarshipAnnouncements.ScholarshipAnnouncement" ID="repeaterScholarshipAnnouncements">
					<ItemTemplate>
						<tr>
							<td><%# Container.ItemIndex + 1 %></td>
							<td><%#: Item.SemesterID.ToSemesterString() %></td>
							<td><%#: Item.ScholarshipTypeEnum.ToFullName() %></td>
							<td><%#: Item.ScholarshipApplicationStatus.ToFullName() %></td>
							<td><%#: (Item.StartDate.ToShortDateString()) %></td>
							<td><%#: (Item.EndDate?.ToShortDateString()) %></td>
							<td>
								<aspire:aspirehyperlink runat="server" navigateurl="<%# Aspire.Web.Sys.Student.Scholarships.AvailableScholarships.GetPageURLForScholarshipForm(Item.ScholarshipTypeEnum, Item.ScholarshipAnnouncementID)  %>" text="Form"  visible="<%# Item.ActionTypeForFormBtn %>" />
								<aspire:aspirehyperlink runat="server" navigateurl="<%# Aspire.Web.Sys.Student.Scholarships.AvailableScholarships.GetPageURLForStatus(Item.ScholarshipTypeEnum, Item.ScholarshipApplicationID) %>" text="Approval Status" target="_blank" visible="<%# Item.ActionTypeForApprovalStatusBtn %>" />
							</td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
				<tr runat="server" id="trNoRecordFound">
					<td colspan="7">No record found.</td>
				</tr>
			</tbody>
		</table>
	</div>
</asp:Content>
