﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ApprovalStatus.aspx.cs" Inherits="Aspire.Web.Sys.Student.Scholarships.ApprovalStatus" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.BL.Core.Scholarship.Common.QarzeHasana" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="table-responsive">
		<table class="table table-bordered table-condensed info tableCol4">
			<tr>
				<th>Enrollment</th>
				<td>
					<aspire:AspireLabel runat="server" ID="lblEnrollment" />
				</td>
				<th>Name</th>
				<td>
					<aspire:AspireLabel runat="server" ID="lblName" />
				</td>
			</tr>
			<tr>
				<th>Class</th>
				<td>
					<aspire:AspireLabel runat="server" ID="lblClass" />
				</td>
				<th>Last Result</th>
				<td>
					<aspire:AspireLabel runat="server" ID="lblLastResult" />
				</td>
			</tr>
			<tr>
				<th>Final Semester</th>
				<td>
					<aspire:AspireLabel runat="server" ID="lblFinalSemester" />
				</td>
				<th>Scholarship Intake</th>
				<td>
					<aspire:AspireLabel runat="server" ID="lblScholarshipIntake" />
				</td>
			</tr>
		</table>
	</div>

	<p></p>
	<asp:Repeater ItemType="Aspire.BL.Core.Scholarship.Staff.QarzeHasana.Approval.ApplicationApproval" runat="server" ID="repeaterApplicationApprovals">
		<HeaderTemplate>
			<table class="table table-bordered table-responsive table-condensed table-hover table-striped">
				<thead>
					<tr>
						<th rowspan="2">#</th>
						<th rowspan="2">Semester</th>
						<th rowspan="2">Tution Fee</th>
						<th>Approval</th>
						<th>Scholarship Amount</th>
						<th rowspan="2">Status</th>
					</tr>
				</thead>
		</HeaderTemplate>
		<ItemTemplate>
			<tr class="table-hover">
				<td><%#: Container.ItemIndex+ 1 %></td>
				<td><%#: Item.SemesterID.ToSemesterString() %></td>
				<td><%#: Item.ScholarshipApplicationApproval.TuitionFee != null ? $"Rs. {Item.ScholarshipApplicationApproval.TuitionFee}": string.Empty.ToNAIfNullOrEmpty() %></td>
				<td><%#: Item.ChequeStatusEnum == Aspire.Model.Entities.ScholarshipApplicationApproval.ChequeStatuses.Yes &&
						Item.ScholarshipApplicationApproval.FinalRecommendation != null 
                         && Item.ApplicationApprovalStatusEnum == Aspire.Model.Entities.ScholarshipApplicationApproval.ApprovalStatuses.Approved
                         ? $"{Item.ScholarshipApplicationApproval.FinalRecommendation}% of Tution Fee" 
                         : string.Empty.ToNAIfNullOrEmpty() %></td>
				<td><%#: Item.ChequeStatusEnum == Aspire.Model.Entities.ScholarshipApplicationApproval.ChequeStatuses.Yes
					    ? $"{Item.ScholarshipApplicationApproval.GetApprovedAmount(Item.ApplicationApprovalStatusEnum)}" : "Rs. 0" %></td>
				<td><%#: Item.ApprovalStatuses %></td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</table>
		</FooterTemplate>
	</asp:Repeater>
</asp:Content>
