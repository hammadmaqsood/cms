﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="QarzeHasanaForm.aspx.cs" Inherits="Aspire.Web.Sys.Student.Scholarships.QarzeHasanaForm" %>

<%@ Register Src="~/Sys/Common/Scholarships/QarzeHasana/QarzeHasanaForm.ascx" TagPrefix="uc1" TagName="QarzeHasanaForm" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<aspire:aspirehyperlink runat="server" navigateurl="AvailableScholarships.aspx" text="Available Scholarships" cssclass="btn btn-success" />
	<p></p>
	<uc1:qarzehasanaform runat="server" id="qarzeHasanaForm" />
</asp:Content>
