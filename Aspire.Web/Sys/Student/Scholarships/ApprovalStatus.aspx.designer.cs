﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Web.Sys.Student.Scholarships {
    
    
    public partial class ApprovalStatus {
        
        /// <summary>
        /// lblEnrollment control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireLabel lblEnrollment;
        
        /// <summary>
        /// lblName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireLabel lblName;
        
        /// <summary>
        /// lblClass control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireLabel lblClass;
        
        /// <summary>
        /// lblLastResult control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireLabel lblLastResult;
        
        /// <summary>
        /// lblFinalSemester control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireLabel lblFinalSemester;
        
        /// <summary>
        /// lblScholarshipIntake control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireLabel lblScholarshipIntake;
        
        /// <summary>
        /// repeaterApplicationApprovals control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Repeater repeaterApplicationApprovals;
    }
}
