﻿using Aspire.BL.Core.Scholarship.Staff.QarzeHasana;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Student.Scholarships
{
	[AspirePage("00A73F38-B6F0-4B9A-A4CB-A279D2D3897D", UserTypes.Student, "~/Sys/Student/Scholarships/ApprovalStatus.aspx", "Approval Status for Students", true, AspireModules.Scholarship)]
	public partial class ApprovalStatus : StudentPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_graduation_cap);
		public override string PageTitle => "Qarz-e-Hasna Application Status";

		public static string GetPageURL(int? scholarshipApplicationID)
		{
			return typeof(ApprovalStatus).GetAspirePageAttribute().PageUrl.AttachQueryParam(nameof(ScholarshipApplicationID), scholarshipApplicationID);
		}

		private int? ScholarshipApplicationID => this.Request.GetParameterValue<int>(nameof(this.ScholarshipApplicationID));
		private int? SemesterID => this.Request.GetParameterValue<int>(nameof(this.SemesterID));

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.ScholarshipApplicationID == null)
				{
					Redirect<AvailableScholarships>();
					return;
				}
				this.GetData();
			}
		}

		private void GetData()
		{
			var scholarshipApplicantSummary = Approval.GetScholarshipApplicationSummaryAndStatusForStudent(this.ScholarshipApplicationID, this.AspireIdentity.LoginSessionGuid);
			if (scholarshipApplicantSummary == null)
			{
				this.AddNoRecordFoundAlert();
				return;
			}
			this.lblEnrollment.Text = scholarshipApplicantSummary.Enrollment;
			this.lblName.Text = scholarshipApplicantSummary.Name;
			this.lblClass.Text = scholarshipApplicantSummary.Class;
			this.lblLastResult.Text = scholarshipApplicantSummary.LastResult;
			this.lblFinalSemester.Text = scholarshipApplicantSummary.FinalSemester.ToSemesterString();
			this.lblScholarshipIntake.Text = scholarshipApplicantSummary.AnnouncementSemesterID.ToSemesterString();

			var applicationApprovals = scholarshipApplicantSummary.ApplicationApprovals;
			if (applicationApprovals == null)
			{
				this.AddNoRecordFoundAlert();
				return;
			}

			this.repeaterApplicationApprovals.DataBind(applicationApprovals);
		}
	}
}