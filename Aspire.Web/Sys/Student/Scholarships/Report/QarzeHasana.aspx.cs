﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Student.Scholarships.Report
{
	[AspirePage("0A3D601C-AF44-4D6C-85CF-0FC7AF6E4191", UserTypes.Student, "~/Sys/Student/Scholarships/Report/QarzeHasana.aspx", "Qarz-e-Hasana Application Form", true, AspireModules.Scholarship)]
	public partial class QarzeHasana : StudentPage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => $"{Scholarship.ScholarshipTypes.QarzeHasana.ToFullName()} Application Form";
		public string ReportName => this.PageTitle;
		public bool ExportToExcel => false;
		public bool ExportToWord => false;
		private int? ScholarshipApplicationID => this.GetParameterValue<int>(nameof(this.ScholarshipApplicationID));

		public static string GetPageUrl(int scholarshipApplicationID)
		{
			return GetPageUrl<QarzeHasana>().AttachQueryParams(nameof(ScholarshipApplicationID), scholarshipApplicationID, ReportView.ReportFormat, ReportFormats.PDF, ReportView.Inline, true);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (this.ScholarshipApplicationID == null)
				Redirect<Dashboard>();
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			Common.Methods.RenderQarzeHasanaFormReport<Dashboard>(this, reportViewer, this.ScholarshipApplicationID ?? throw new InvalidOperationException());
		}
	}
}