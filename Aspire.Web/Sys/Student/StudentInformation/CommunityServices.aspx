﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CommunityServices.aspx.cs" Inherits="Aspire.Web.Sys.Student.StudentInformation.CommunityServices" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">

	<aspire:AspireGridView runat="server" ID="gvCommunityServices" AutoGenerateColumns="False" ShowFooter="True">
		<Columns>
			<asp:TemplateField HeaderText="Semester">
				<ItemTemplate>
					<%#: ((short)this.Eval("SemesterID")).ToSemesterString() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Organization">
				<ItemTemplate>
					<%#: this.Eval("Organization").ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Job Title">
				<ItemTemplate>
					<%#: this.Eval("JobTitle").ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Completed Date" DataField="CompletedDate" DataFormatString="{0:D}" />
			<asp:BoundField HeaderText="Hours" DataField="Hours" />
		</Columns>
		<FooterStyle Font-Bold="True"></FooterStyle>
	</aspire:AspireGridView>
</asp:Content>
