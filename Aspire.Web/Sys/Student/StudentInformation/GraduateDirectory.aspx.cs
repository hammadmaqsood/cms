﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Student.StudentInformation
{
	[AspirePage("7EED1E84-DCFC-4FAA-AF7B-4D0E035E4F9A", UserTypes.Student, "~/Sys/Student/StudentInformation/GraduateDirectory.aspx", "Graduate Directory", true, AspireModules.StudentInformation)]
	public partial class GraduateDirectory : StudentPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.education);
		public override string PageTitle => "Graduate Directory";

		public static string GetPageUrl(int? graduateDirectoryFormID, short? semesterID)
		{
			return GetAspirePageAttribute<GraduateDirectory>().PageUrl.AttachQueryParams(nameof(GraduateDirectoryFormID), graduateDirectoryFormID, nameof(SemesterID), semesterID);
		}

		public static void Redirect(int? graduateDirectoryFormID, short? semesterID)
		{
			Redirect(GetPageUrl(graduateDirectoryFormID, semesterID));
		}

		private int? GraduateDirectoryFormID => this.GetParameterValue<int>(nameof(this.GraduateDirectoryFormID));

		private short? SemesterID => this.GetParameterValue<short>(nameof(this.SemesterID));

		private void Refresh()
		{
			Redirect(GetPageUrl(this.GraduateDirectoryFormID, null));
		}

		private void FormAlreadySubmitted()
		{
			this.AddErrorAlert("Form data can not be changed after submission.");
			this.Refresh();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.GraduateDirectoryFormID == null)
				{
					if (this.SemesterID == null)
					{
						this.AddErrorAlert("Graduate Directory Form is not available now or has been submitted. Note: This is only for Last Semester Students.");
						Redirect<Dashboard>();
						return;
					}
					var graduateDirectoryForm = BL.Core.StudentInformation.Student.GraduateDirectory.AddAndGetGraduateDirectoryForm(this.SemesterID.Value, this.StudentIdentity.LoginSessionGuid);
					Redirect(graduateDirectoryForm.GraduateDirectoryFormID, null);
					return;
				}
				this.GetData();
			}
		}

		#region GraduateDirectoryFormWorkExperience

		protected void btnAddGraduateDirectoryFormWorkExperience_OnClick(object sender, EventArgs e)
		{
			this.DisplayGraduateDirectoryFormWorkExperienceModal(null);
		}

		protected void gvGraduateDirectoryFormWorkExperience_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayGraduateDirectoryFormWorkExperienceModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var deleted = BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormWorkExperience(e.DecryptedCommandArgumentToInt(), this.StudentIdentity.LoginSessionGuid);
					switch (deleted)
					{
						case BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormWorkExperienceStatuses.NoRecordFound:
							this.Refresh();
							return;
						case BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormWorkExperienceStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Work Experience");
							this.Refresh();
							break;
						case BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormWorkExperienceStatuses.SubmittedFormCannotBeChanged:
							this.FormAlreadySubmitted();
							return;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		private void DisplayGraduateDirectoryFormWorkExperienceModal(int? graduateDirectoryFormWorkExperienceID)
		{
			this.ddlExperienceType.FillGraduateDirectoryFormWorkExperienceTypes(CommonListItems.Select);
			if (graduateDirectoryFormWorkExperienceID == null)
			{
				this.ddlExperienceType.ClearSelection();
				this.tbDesignation.Text = null;
				this.tbOrganization.Text = null;
				this.tbDuration.Text = null;
				this.tbRoles.Text = null;

				this.modalGraduateDirectoryFormWorkExperience.HeaderText = "Add Work Experience";
				this.btnSaveGraduateDirectoryFormWorkExperience.Text = @"Add";
				this.ViewState[nameof(GraduateDirectoryFormWorkExperience.GraduateDirectoryFormWorkExperienceID)] = null;
				this.modalGraduateDirectoryFormWorkExperience.Show();
			}
			else
			{
				var graduateDirectoryFormWorkExperience = BL.Core.StudentInformation.Student.GraduateDirectory.GetGraduateDirectoryFormWorkExperience(graduateDirectoryFormWorkExperienceID.Value, this.StudentIdentity.LoginSessionGuid);
				if (graduateDirectoryFormWorkExperience == null)
				{
					this.Refresh();
					return;
				}

				this.ddlExperienceType.SetEnumValue(graduateDirectoryFormWorkExperience.ExperienceTypeEnum);
				this.tbDesignation.Text = graduateDirectoryFormWorkExperience.Designation;
				this.tbOrganization.Text = graduateDirectoryFormWorkExperience.Organization;
				this.tbDuration.Text = graduateDirectoryFormWorkExperience.Duration;
				this.tbRoles.Text = graduateDirectoryFormWorkExperience.Roles;

				this.modalGraduateDirectoryFormWorkExperience.HeaderText = "Edit Work Experience";
				this.btnSaveGraduateDirectoryFormWorkExperience.Text = @"Save";
				this.ViewState[nameof(GraduateDirectoryFormWorkExperience.GraduateDirectoryFormWorkExperienceID)] = graduateDirectoryFormWorkExperience.GraduateDirectoryFormWorkExperienceID;
				this.modalGraduateDirectoryFormWorkExperience.Show();
			}
		}

		protected void btnSaveGraduateDirectoryFormWorkExperience_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid || this.GraduateDirectoryFormID == null)
				return;
			var graduateDirectoryFormWorkExperienceID = (int?)this.ViewState[nameof(GraduateDirectoryFormWorkExperience.GraduateDirectoryFormWorkExperienceID)];
			var experienceTypeEnum = this.ddlExperienceType.GetSelectedEnumValue<GraduateDirectoryFormWorkExperience.ExperienceTypes>();
			var designation = this.tbDesignation.Text;
			var organization = this.tbOrganization.Text;
			var duration = this.tbDuration.Text;
			var roles = this.tbRoles.Text;
			if (graduateDirectoryFormWorkExperienceID == null)
			{
				var result = BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryFormWorkExperience(this.GraduateDirectoryFormID.Value, experienceTypeEnum, designation, organization, duration, roles, this.StudentIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryFormWorkExperienceStatuses.SubmittedFormCannotBeChanged:
						this.Refresh();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryFormWorkExperienceStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Work Experience");
						this.Refresh();
						break;

					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var result = BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryFormWorkExperience(graduateDirectoryFormWorkExperienceID.Value, experienceTypeEnum, designation, organization, duration, roles, this.StudentIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryFormWorkExperienceStatuses.NoRecordFound:
						this.Refresh();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryFormWorkExperienceStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Work Experience");
						this.Refresh();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		#endregion

		#region Graduate Directory Form Project

		protected void btnAddGraduateDirectoryFormProject_OnClick(object sender, EventArgs e)
		{
			this.DisplayGraduateDirectoryFormProjectModal(null);
		}

		protected void gvGraduateDirectoryFormProject_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayGraduateDirectoryFormProjectModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var deleted = BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormProject(e.DecryptedCommandArgumentToInt(), this.StudentIdentity.LoginSessionGuid);
					switch (deleted)
					{
						case BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormProjectStatuses.NoRecordFound:
							this.Refresh();
							return;
						case BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormProjectStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Project");
							this.Refresh();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		private void DisplayGraduateDirectoryFormProjectModal(int? graduateDirectoryFormProjectID)
		{
			this.ddlSemesterID.FillSemesters();
			this.ddlProjectType.FillEnums<GraduateDirectoryFormProject.ProjectTypes>();
			if (graduateDirectoryFormProjectID == null)
			{
				this.ddlSemesterID.ClearSelection();
				this.ddlProjectType.ClearSelection();
				this.tbDescription.Text = null;

				this.modalGraduateDirectoryFormProject.HeaderText = "Add Project";
				this.btnSaveGraduateDirectoryFormProject.Text = @"Add";
				this.ViewState[nameof(GraduateDirectoryFormProject.GraduateDirectoryFormProjectID)] = null;
				this.modalGraduateDirectoryFormProject.Show();
			}
			else
			{
				var graduateDirectoryFormProject = BL.Core.StudentInformation.Student.GraduateDirectory.GetGraduateDirectoryFormProject(graduateDirectoryFormProjectID.Value, this.StudentIdentity.LoginSessionGuid);
				this.ddlSemesterID.SelectedValue = graduateDirectoryFormProject.SemesterID.ToString();
				this.ddlProjectType.SetEnumValue(graduateDirectoryFormProject.ProjectTypeEnum);
				this.tbDescription.Text = graduateDirectoryFormProject.Description;

				this.modalGraduateDirectoryFormProject.HeaderText = "Edit Project";
				this.btnSaveGraduateDirectoryFormProject.Text = @"Save";
				this.ViewState[nameof(GraduateDirectoryFormProject.GraduateDirectoryFormProjectID)] = graduateDirectoryFormProjectID;
				this.modalGraduateDirectoryFormProject.Show();
			}
		}

		protected void btnSaveGraduateDirectoryFormProject_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid || this.GraduateDirectoryFormID == null)
				return;
			var graduateDirectoryFormProjectID = (int?)this.ViewState[nameof(GraduateDirectoryFormProject.GraduateDirectoryFormProjectID)];
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var projectTypeEnum = this.ddlProjectType.GetSelectedEnumValue<GraduateDirectoryFormProject.ProjectTypes>();
			var description = this.tbDescription.Text;
			if (graduateDirectoryFormProjectID == null)
			{
				var result = BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryFormProject(this.GraduateDirectoryFormID.Value, semesterID, projectTypeEnum, description, this.StudentIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryFormProjectStatuses.SubmittedFormCannotBeChanged:
						this.Refresh();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryFormProjectStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Project");
						this.Refresh();
						break;

					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var result = BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryFormProject(graduateDirectoryFormProjectID.Value, semesterID, projectTypeEnum, description, this.StudentIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryFormProjectStatuses.NoRecordFound:
						this.Refresh();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryFormProjectStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Project");
						this.Refresh();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		#endregion

		#region Graduate Directory Volunteer Work

		protected void btnAddGraduateDirectoryFormVolunteerWork_OnClick(object sender, EventArgs e)
		{
			this.DisplayGraduateDirectoryFormVolunteerWorksModal(null);
		}

		protected void gvGraduateDirectoryFormVolunteerWorks_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayGraduateDirectoryFormVolunteerWorksModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var deleted = BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormVolunteerWork(e.DecryptedCommandArgumentToInt(), this.StudentIdentity.LoginSessionGuid);
					switch (deleted)
					{
						case BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormVolunteerWorkStatuses.NoRecordFound:
							this.Refresh();
							return;
						case BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormVolunteerWorkStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Volunteer Work");
							this.Refresh();
							break;
						case BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormVolunteerWorkStatuses.SubmittedFormCannotBeChanged:
							this.FormAlreadySubmitted();
							return;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		private void DisplayGraduateDirectoryFormVolunteerWorksModal(int? graduateDirectoryFormVolunteerWorkID)
		{
			if (graduateDirectoryFormVolunteerWorkID == null)
			{
				this.tbOrganizationVW.Text = null;
				this.tbDescriptionVW.Text = null;
				this.dtpFromDateVW.Text = null;
				this.dtpToDateVW.Text = null;

				this.modalGraduateDirectoryVolunteerWork.HeaderText = "Add Volunteer Work";
				this.btnSaveGraduateDirectoryVolunteerWork.Text = @"Add";
				this.ViewState[nameof(GraduateDirectoryFormVolunteerWork.GraduateDirectoryFormVolunteerWorkID)] = null;
				this.modalGraduateDirectoryVolunteerWork.Show();
			}
			else
			{
				var graduateDirectoryFormVolunteerWork = BL.Core.StudentInformation.Student.GraduateDirectory.GetGraduateDirectoryFormVolunteerWork(graduateDirectoryFormVolunteerWorkID.Value, this.StudentIdentity.LoginSessionGuid);
				this.tbOrganizationVW.Text = graduateDirectoryFormVolunteerWork.Organization;
				this.tbDescriptionVW.Text = graduateDirectoryFormVolunteerWork.Description;
				this.dtpFromDateVW.SelectedDate = graduateDirectoryFormVolunteerWork.FromDate;
				this.dtpToDateVW.SelectedDate = graduateDirectoryFormVolunteerWork.ToDate;
				this.modalGraduateDirectoryVolunteerWork.HeaderText = "Edit Volunteer Work";
				this.btnSaveGraduateDirectoryVolunteerWork.Text = @"Save";
				this.ViewState[nameof(GraduateDirectoryFormVolunteerWork.GraduateDirectoryFormVolunteerWorkID)] = graduateDirectoryFormVolunteerWork.GraduateDirectoryFormVolunteerWorkID;
				this.modalGraduateDirectoryVolunteerWork.Show();
			}
		}

		protected void btnSaveGraduateDirectoryVolunteerWork_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid || this.GraduateDirectoryFormID == null)
				return;
			var graduateDirectoryFormVolunteerWorkID = (int?)this.ViewState[nameof(GraduateDirectoryFormVolunteerWork.GraduateDirectoryFormVolunteerWorkID)];
			var organization = this.tbOrganizationVW.Text;
			var description = this.tbDescriptionVW.Text;
			var fromDate = this.dtpFromDateVW.SelectedDate ?? throw new InvalidOperationException();
			var toDate = this.dtpToDateVW.SelectedDate ?? throw new InvalidOperationException();
			if (graduateDirectoryFormVolunteerWorkID == null)
			{
				var result = BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryFormVolunteerWork(this.GraduateDirectoryFormID.Value, organization, description, fromDate, toDate, this.StudentIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryFormVolunteerWorkStatuses.NoRecordFound:
						this.Refresh();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryFormVolunteerWorkStatuses.SubmittedFormCannotBeChanged:
						this.FormAlreadySubmitted();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryFormVolunteerWorkStatuses.FromDateMustBeLessThanOrEqualToToDate:
						this.alertGraduateDirectoryVolunteerWork.AddErrorAlert("Form Date must be less than or equal to To Date.");
						this.updatePanelVolunteerWork.Update();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryFormVolunteerWorkStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Volunteer Work");
						this.Refresh();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var result = BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryFormVolunteerWork(graduateDirectoryFormVolunteerWorkID.Value, organization, description, fromDate, toDate, this.StudentIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryFormVolunteerWorkStatuses.NoRecordFound:
						this.Refresh();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryFormVolunteerWorkStatuses.SubmittedFormCannotBeChanged:
						this.FormAlreadySubmitted();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryFormVolunteerWorkStatuses.FromDateMustBeLessThanOrEqualToToDate:
						this.alertGraduateDirectoryVolunteerWork.AddErrorAlert("Form Date must be less than or equal to To Date.");
						this.updatePanelVolunteerWork.Update();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryFormVolunteerWorkStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Volunteer Work");
						this.Refresh();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		#endregion

		#region Graduate Directory Workshops/Seminars

		protected void btnAddGraduateDirectoryFormWorkshops_OnClick(object sender, EventArgs e)
		{
			this.DisplayGraduateDirectoryFormWorkshopModal(null);
		}

		protected void gvGraduateDirectoryFormWorkshops_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayGraduateDirectoryFormWorkshopModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var deleted = BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormWorkshop(e.DecryptedCommandArgumentToInt(), this.StudentIdentity.LoginSessionGuid);
					switch (deleted)
					{
						case BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormWorkshopStatuses.NoRecordFound:
							this.Refresh();
							return;
						case BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormWorkshopStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Workshop/Seminar");
							this.Refresh();
							break;
						case BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormWorkshopStatuses.SubmittedFormCannotBeChanged:
							this.FormAlreadySubmitted();
							return;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		private void DisplayGraduateDirectoryFormWorkshopModal(int? graduateDirectoryFormWorkshopID)
		{
			this.ddlType.FillGraduateDirectoryFormWorkshopTypes(CommonListItems.Select);
			if (graduateDirectoryFormWorkshopID == null)
			{
				this.tbDescriptionWorkshop.Text = null;
				this.ddlType.ClearSelection();
				this.dtpFromDateWorkshop.Text = null;
				this.dtpToDateWorkshop.Text = null;

				this.modalGraduateDirectoryWorkshop.HeaderText = "Add Workshop/Seminar";
				this.btnSaveGraduateDirectoryWorkshop.Text = @"Add";
				this.ViewState[nameof(GraduateDirectoryFormWorkshop.GraduateDirectoryFormWorkshopID)] = null;
				this.modalGraduateDirectoryWorkshop.Show();
			}
			else
			{
				var graduateDirectoryFormWorkshop = BL.Core.StudentInformation.Student.GraduateDirectory.GetGraduateDirectoryFormWorkshop(graduateDirectoryFormWorkshopID.Value, this.StudentIdentity.LoginSessionGuid);
				this.ddlType.SetEnumValue(graduateDirectoryFormWorkshop.TypeEnum);
				this.tbDescriptionWorkshop.Text = graduateDirectoryFormWorkshop.Description;
				this.dtpFromDateWorkshop.SelectedDate = graduateDirectoryFormWorkshop.FromDate;
				this.dtpToDateWorkshop.SelectedDate = graduateDirectoryFormWorkshop.ToDate;
				this.modalGraduateDirectoryWorkshop.HeaderText = "Edit Workshop/Seminar";
				this.btnSaveGraduateDirectoryWorkshop.Text = @"Save";
				this.ViewState[nameof(GraduateDirectoryFormWorkshop.GraduateDirectoryFormWorkshopID)] = graduateDirectoryFormWorkshop.GraduateDirectoryFormWorkshopID;
				this.modalGraduateDirectoryWorkshop.Show();
			}
		}

		protected void btnSaveGraduateDirectoryWorkshop_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid || this.GraduateDirectoryFormID == null)
				return;
			var graduateDirectoryFormWorkshopID = (int?)this.ViewState[nameof(GraduateDirectoryFormWorkshop.GraduateDirectoryFormWorkshopID)];
			var typeEnum = this.ddlType.GetSelectedEnumValue<GraduateDirectoryFormWorkshop.Types>();
			var description = this.tbDescriptionWorkshop.Text;
			var fromDate = this.dtpFromDateWorkshop.SelectedDate ?? throw new InvalidOperationException();
			var toDate = this.dtpToDateWorkshop.SelectedDate ?? throw new InvalidOperationException();
			if (graduateDirectoryFormWorkshopID == null)
			{
				var result = BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryFormWorkshop(this.GraduateDirectoryFormID.Value, typeEnum, description, fromDate, toDate, this.StudentIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryFormWorkshopStatuses.NoRecordFound:
						this.Refresh();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryFormWorkshopStatuses.SubmittedFormCannotBeChanged:
						this.FormAlreadySubmitted();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryFormWorkshopStatuses.FromDateMustBeLessThanOrEqualToToDate:
						this.alertGraduateDirectoryWorkshop.AddErrorAlert("Form Date must be less than or equal to To Date.");
						this.updatePanelVolunteerWork.Update();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryFormWorkshopStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Workshop/Seminar");
						this.Refresh();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var result = BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryFormWorkshop(graduateDirectoryFormWorkshopID.Value, typeEnum, description, fromDate, toDate, this.StudentIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryFormWorkshopStatuses.NoRecordFound:
						this.Refresh();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryFormWorkshopStatuses.SubmittedFormCannotBeChanged:
						this.FormAlreadySubmitted();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryFormWorkshopStatuses.FromDateMustBeLessThanOrEqualToToDate:
						this.alertGraduateDirectoryWorkshop.AddErrorAlert("Form Date must be less than or equal to To Date.");
						this.updatePanelVolunteerWork.Update();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryFormWorkshopStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Workshop/Seminar");
						this.Refresh();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		#endregion

		#region  Graduate Directory Extra Curricular Activity

		protected void btnAddGraduateDirectoryExtraCurricularActivity_OnClick(object sender, EventArgs e)
		{
			this.DisplayGraduateDirectoryExtraCurricularActivityModal(null);
		}

		protected void gvGraduateDirectoryExtraCurricularActivities_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayGraduateDirectoryExtraCurricularActivityModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var deleted = BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryExtraCurricularActivity(e.DecryptedCommandArgumentToInt(), this.StudentIdentity.LoginSessionGuid);
					switch (deleted)
					{
						case BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryExtraCurricularActivityStatuses.NoRecordFound:
							this.Refresh();
							return;
						case BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryExtraCurricularActivityStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Extra Curricular Activity");
							this.Refresh();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		private void DisplayGraduateDirectoryExtraCurricularActivityModal(int? graduateDirectoryExtraCurricularActivityID)
		{
			if (graduateDirectoryExtraCurricularActivityID == null)
			{
				this.tbDescriptionECA.Text = null;

				this.modalGraduateDirectoryExtraCurricularActivity.HeaderText = "Add Extra Curricular Activity";
				this.btnSaveGraduateDirectoryExtraCurricularActivity.Text = @"Add";
				this.ViewState[nameof(GraduateDirectoryExtraCurricularActivity.GraduateDirectoryExtraCurricularActivityID)] = null;
				this.modalGraduateDirectoryExtraCurricularActivity.Show();
			}
			else
			{
				var graduateDirectoryExtraCurricularActivity = BL.Core.StudentInformation.Student.GraduateDirectory.GetGraduateDirectoryExtraCurricularActivity(graduateDirectoryExtraCurricularActivityID.Value, this.StudentIdentity.LoginSessionGuid);
				this.tbDescriptionECA.Text = graduateDirectoryExtraCurricularActivity.Description;

				this.modalGraduateDirectoryExtraCurricularActivity.HeaderText = "Edit Extra Curricular Activity";
				this.btnSaveGraduateDirectoryExtraCurricularActivity.Text = @"Save";
				this.ViewState[nameof(GraduateDirectoryExtraCurricularActivity.GraduateDirectoryExtraCurricularActivityID)] = graduateDirectoryExtraCurricularActivityID;
				this.modalGraduateDirectoryExtraCurricularActivity.Show();
			}
		}

		protected void btnSaveGraduateDirectoryExtraCurricularActivity_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid || this.GraduateDirectoryFormID == null)
				return;
			var graduateDirectoryExtraCurricularActivityID = (int?)this.ViewState[nameof(GraduateDirectoryExtraCurricularActivity.GraduateDirectoryExtraCurricularActivityID)];
			var description = this.tbDescriptionECA.Text;
			if (graduateDirectoryExtraCurricularActivityID == null)
			{
				var result = BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryExtraCurricularActivity(this.GraduateDirectoryFormID.Value, description, this.StudentIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryExtraCurricularActivityStatuses.SubmittedFormCannotBeChanged:
						this.Refresh();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectoryExtraCurricularActivityStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Extra Curricular Activity");
						this.Refresh();
						break;

					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var result = BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryExtraCurricularActivity(graduateDirectoryExtraCurricularActivityID.Value, description, this.StudentIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryExtraCurricularActivityStatuses.NoRecordFound:
						this.Refresh();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectoryExtraCurricularActivityStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Extra Curricular Activity");
						this.Refresh();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		#endregion

		#region Graduate Directory Form Skill

		protected void btnAddGraduateDirectoryFormSkill_OnClick(object sender, EventArgs e)
		{
			this.DisplayGraduateDirectoryFormSkillModal(null);
		}

		protected void gvGraduateDirectoryFormSkill_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayGraduateDirectoryFormSkillModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var deleted = BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormSkill(e.DecryptedCommandArgumentToInt(), this.StudentIdentity.LoginSessionGuid);
					switch (deleted)
					{
						case BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormSkillStatuses.NoRecordFound:
							this.Refresh();
							return;
						case BL.Core.StudentInformation.Student.GraduateDirectory.DeleteGraduateDirectoryFormSkillStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Skill");
							this.Refresh();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		private void DisplayGraduateDirectoryFormSkillModal(int? graduateDirectoryFormSkillID)
		{

			if (graduateDirectoryFormSkillID == null)
			{
				this.tbDescriptionSkill.Text = null;

				this.modalGraduateDirectoryFormSkill.HeaderText = "Add Skill";
				this.btnSaveGraduateDirectoryFormSkill.Text = @"Add";
				this.ViewState[nameof(GraduateDirectoryFormSkill.GraduateDirectorySkillID)] = null;
				this.modalGraduateDirectoryFormSkill.Show();
			}
			else
			{
				var graduateDirectoryFormSkill = BL.Core.StudentInformation.Student.GraduateDirectory.GetGraduateDirectoryFormSkill(graduateDirectoryFormSkillID.Value, this.StudentIdentity.LoginSessionGuid);
				this.tbDescriptionSkill.Text = graduateDirectoryFormSkill.Description;

				this.modalGraduateDirectoryFormSkill.HeaderText = "Edit Skill";
				this.btnSaveGraduateDirectoryFormSkill.Text = @"Save";
				this.ViewState[nameof(GraduateDirectoryFormSkill.GraduateDirectorySkillID)] = graduateDirectoryFormSkillID;
				this.modalGraduateDirectoryFormSkill.Show();
			}
		}

		protected void btnSaveGraduateDirectoryFormSkill_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid || this.GraduateDirectoryFormID == null)
				return;
			var graduateDirectorySkillID = (int?)this.ViewState[nameof(GraduateDirectoryFormSkill.GraduateDirectorySkillID)];
			var description = this.tbDescriptionSkill.Text;
			if (graduateDirectorySkillID == null)
			{
				var result = BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectorySkill(this.GraduateDirectoryFormID.Value, description, this.StudentIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectorySkillStatuses.SubmittedFormCannotBeChanged:
						this.Refresh();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectorySkillStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Skill");
						this.Refresh();
						break;
					case BL.Core.StudentInformation.Student.GraduateDirectory.AddGraduateDirectorySkillStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.Refresh();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var result = BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectorySkill(graduateDirectorySkillID.Value, description, this.StudentIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectorySkillStatuses.NoRecordFound:
						this.Refresh();
						return;
					case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateGraduateDirectorySkillStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Skill");
						this.Refresh();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		#endregion

		private void GetData()
		{
			if (this.GraduateDirectoryFormID == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect<Dashboard>();
				return;
			}
			var (graduateDirectoryForm, programMajors, programAlias) = BL.Core.StudentInformation.Student.GraduateDirectory.GetGraduateDirectoryForm(this.GraduateDirectoryFormID.Value, this.StudentIdentity.LoginSessionGuid);
			if (graduateDirectoryForm.Submitted != null)
			{
				this.AddErrorAlert("Graduate Directory Form has already been submitted.");
				Redirect<Dashboard>();
				return;
			}

			this.gvGraduateDirectoryFormWorkExperience.DataBind(graduateDirectoryForm.GraduateDirectoryFormWorkExperiences);
			this.gvGraduateDirectoryFormProject.DataBind(graduateDirectoryForm.GraduateDirectoryFormProjects);
			this.gvGraduateDirectoryFormVolunteerWorks.DataBind(graduateDirectoryForm.GraduateDirectoryFormVolunteerWorks);
			this.gvGraduateDirectoryFormWorkshops.DataBind(graduateDirectoryForm.GraduateDirectoryFormWorkshops);
			this.gvGraduateDirectoryExtraCurricularActivities.DataBind(graduateDirectoryForm.GraduateDirectoryExtraCurricularActivities);
			this.gvGraduateDirectoryFormSkill.DataBind(graduateDirectoryForm.GraduateDirectoryFormSkills);

			this.ddlProgramMajorID.DataBind(programMajors, CommonListItems.None);
			this.ddlProgramMajorID.SelectedValue = graduateDirectoryForm.ProgramMajorID.ToString();
			this.ddlProgramMajorID.Enabled = this.btnSaveMajors.Enabled = programMajors.Any();
			this.tbProgramAlias.Text = programAlias;
			this.tbProgramAlias.ReadOnly = true;
			if (this.btnSaveMajors.Enabled == false)
			{
				this.divbtnSave.Visible = false;
				this.divtbProgramAlias.Attributes["class"] = "col-md-6";
				this.divddlProgramMajorID.Attributes["class"] = "col-md-6";
			}
		}

		protected void btnSubmit_OnClick(object sender, EventArgs e)
		{
			if (this.GraduateDirectoryFormID == null)
				return;
			var status = BL.Core.StudentInformation.Student.GraduateDirectory.SubmitGraduateDirectoryForm(this.GraduateDirectoryFormID.Value, this.StudentIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.StudentInformation.Student.GraduateDirectory.SubmitGraduateDirectoryFormStatuses.NoRecordFound:
					this.Refresh();
					return;
				case BL.Core.StudentInformation.Student.GraduateDirectory.SubmitGraduateDirectoryFormStatuses.AlreadySubmitted:
					this.AddErrorAlert("Form already submitted.");
					this.Refresh();
					break;
				case BL.Core.StudentInformation.Student.GraduateDirectory.SubmitGraduateDirectoryFormStatuses.ProjectsRecordNotEntered:
					this.AddErrorAlert("Projects/Thesis information not provided.");
					break;
				case BL.Core.StudentInformation.Student.GraduateDirectory.SubmitGraduateDirectoryFormStatuses.WorkExperienceNotEntered:
					this.AddErrorAlert("Work Experience information not provided.");
					break;
				case BL.Core.StudentInformation.Student.GraduateDirectory.SubmitGraduateDirectoryFormStatuses.SkillNotEntered:
					this.AddErrorAlert("Skills information not provided.");
					break;
				case BL.Core.StudentInformation.Student.GraduateDirectory.SubmitGraduateDirectoryFormStatuses.ActivitiesNotEntered:
					this.AddErrorAlert("Extra Curricular Activities information not provided.");
					break;
				case BL.Core.StudentInformation.Student.GraduateDirectory.SubmitGraduateDirectoryFormStatuses.Submitted:
					this.AddSuccessAlert("Graduate Directory Form has been submitted.");
					Redirect<Dashboard>();
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void btnSaveMajors_OnClick(object sender, EventArgs e)
		{
			if (this.GraduateDirectoryFormID == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect<Dashboard>();
				return;
			}
			var programMajorID = this.ddlProgramMajorID.SelectedValue.ToNullableInt();
			var statuses = BL.Core.StudentInformation.Student.GraduateDirectory.UpdateProgramMajors(this.GraduateDirectoryFormID.Value, programMajorID, this.StudentIdentity.LoginSessionGuid);
			switch (statuses)
			{
				case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateProgramMajorsStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.Refresh();
					return;
				case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateProgramMajorsStatuses.SubmittedFormCannotBeChanged:
					this.Refresh();
					return;
				case BL.Core.StudentInformation.Student.GraduateDirectory.UpdateProgramMajorsStatuses.Success:
					this.AddSuccessAlert("Majors has been updated.");
					this.Refresh();
					return;
				default:
					throw new ArgumentOutOfRangeException(nameof(statuses), statuses, null);
			}
		}
	}
}