﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="AcademicDocuments.aspx.cs" Inherits="Aspire.Web.Sys.Student.StudentInformation.AcademicDocuments" %>

<%@ Register Src="~/Sys/Common/StudentInformation/StudentDocuments.ascx" TagPrefix="uc1" TagName="StudentDocuments" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc1:StudentDocuments runat="server" ID="studentDocuments" />
</asp:Content>