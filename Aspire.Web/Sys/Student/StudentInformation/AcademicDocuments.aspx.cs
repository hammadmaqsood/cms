﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Student.StudentInformation
{
	[AspirePage("219EC97C-CD5C-4C44-A8AA-3E918B5E5BBE", UserTypes.Student, "~/Sys/Student/StudentInformation/AcademicDocuments.aspx", "Academic Documents", true, AspireModules.StudentInformation)]
	public partial class AcademicDocuments : StudentAndParentPage
	{
		public override PageIcon PageIcon => AspireGlyphicons.file.GetIcon();
		public override string PageTitle => "Academic Documents";

		public static string GetPageUrl()
		{
			return typeof(AcademicDocuments).GetAspirePageAttribute().PageUrl;
		}

		public static void Redirect()
		{
			Redirect(GetPageUrl());
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.studentDocuments.Show(this.StudentIdentity.Enrollment);
			}
		}
	}
}