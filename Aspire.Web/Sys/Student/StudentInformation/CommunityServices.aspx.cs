﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Student.StudentInformation
{
	[AspirePage("93D4DAD6-0F74-4C91-9170-16C5CB363693", UserTypes.Student, "~/Sys/Student/StudentInformation/CommunityServices.aspx", "Community Services", true, AspireModules.StudentInformation)]
	public partial class CommunityServices : StudentAndParentPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.export);
		public override string PageTitle => "Community Services";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var studentID = this.StudentIdentity.StudentID;
				var communityService = BL.Core.StudentInformation.Student.CommunityServices.GetStudentCommunityServices(studentID, this.StudentIdentity.LoginSessionGuid);
				this.gvCommunityServices.DataBind(communityService);
				if (this.gvCommunityServices.FooterRow != null)
					this.gvCommunityServices.FooterRow.Cells[4].Text = communityService.Sum(cs=>cs.Hours).ToString();
			}
		}
	}
}