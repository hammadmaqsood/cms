﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="GraduateDirectory.aspx.cs" Inherits="Aspire.Web.Sys.Student.StudentInformation.GraduateDirectory" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-5" runat="server" id="divtbProgramAlias">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="tbProgramAlias" />
				<aspire:AspireTextBox runat="server" ID="tbProgramAlias" ReadOnly="True" ValidationGroup="Majors" />
			</div>
		</div>
		<div class="col-md-5" runat="server" id="divddlProgramMajorID">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Major:" AssociatedControlID="ddlProgramMajorID" />
				<aspire:AspireDropDownList runat="server" ID="ddlProgramMajorID" ValidationGroup="Majors" />
				<span class="help-block">If the required program's major is not available in the list, please contact DSA office.</span>
			</div>
		</div>
		<div class="col-md-2" runat="server" id="divbtnSave">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="&nbsp;" AssociatedControlID="btnSaveMajors" />
				<div>
					<aspire:AspireButton runat="server" CssClass="btn-block" ID="btnSaveMajors" Text="Save" ValidationGroup="Majors" OnClick="btnSaveMajors_OnClick" />
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Work Experience</h3>
		</div>
		<div class="panel-body">
			<aspire:AspireButton runat="server" ButtonType="Success" CausesValidation="False" Glyphicon="plus" Text="Add" ID="btnAddGraduateDirectoryFormWorkExperience" OnClick="btnAddGraduateDirectoryFormWorkExperience_OnClick" />
			<p></p>
			<aspire:AspireGridView runat="server" ItemType="Aspire.Model.Entities.GraduateDirectoryFormWorkExperience" ID="gvGraduateDirectoryFormWorkExperience" AutoGenerateColumns="False" OnRowCommand="gvGraduateDirectoryFormWorkExperience_OnRowCommand">
				<Columns>
					<asp:TemplateField HeaderText="#">
						<ItemTemplate>
							<%#: Container.DataItemIndex + 1 %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Type">
						<ItemTemplate>
							<%#: Item.ExperienceTypeFullName.ToNAIfNullOrEmpty() %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField HeaderText="Designation" DataField="Designation" />
					<asp:BoundField HeaderText="Organization" DataField="Organization" />
					<asp:BoundField HeaderText="Duration" DataField="Duration" />
					<asp:BoundField HeaderText="Roles" DataField="Roles" />
					<asp:TemplateField HeaderText="Actions">
						<ItemTemplate>
							<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument="<%# Item.GraduateDirectoryFormWorkExperienceID %>" />
							<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument="<%# Item.GraduateDirectoryFormWorkExperienceID %>" ConfirmMessage="Are you sure want to delete this record?" />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</aspire:AspireGridView>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Projects</h3>
		</div>
		<div class="panel-body">
			<aspire:AspireButton runat="server" ButtonType="Success" CausesValidation="False" Glyphicon="plus" Text="Add" ID="btnAddGraduateDirectoryFormProject" OnClick="btnAddGraduateDirectoryFormProject_OnClick" />
			<p></p>
			<aspire:AspireGridView runat="server" ItemType="Aspire.Model.Entities.GraduateDirectoryFormProject" ID="gvGraduateDirectoryFormProject" AutoGenerateColumns="False" OnRowCommand="gvGraduateDirectoryFormProject_OnRowCommand">
				<Columns>
					<asp:TemplateField HeaderText="Semester">
						<ItemTemplate>
							<%#: Item.SemesterID.ToSemesterString() %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Project Type">
						<ItemTemplate>
							<%#: Item.ProjectTypeFullName %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField HeaderText="Description" DataField="Description" />
					<asp:TemplateField HeaderText="Actions">
						<ItemTemplate>
							<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument="<%# Item.GraduateDirectoryFormProjectID %>" />
							<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument="<%# Item.GraduateDirectoryFormProjectID %>" ConfirmMessage="Are you sure want to delete this record?" />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</aspire:AspireGridView>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Volunteer Work</h3>
		</div>
		<div class="panel-body">
			<aspire:AspireButton runat="server" ButtonType="Success" CausesValidation="False" Glyphicon="plus" Text="Add" ID="btnAddGraduateDirectoryFormVolunteerWork" OnClick="btnAddGraduateDirectoryFormVolunteerWork_OnClick" />
			<p></p>
			<aspire:AspireGridView runat="server" ItemType="Aspire.Model.Entities.GraduateDirectoryFormVolunteerWork" ID="gvGraduateDirectoryFormVolunteerWorks" AutoGenerateColumns="False" OnRowCommand="gvGraduateDirectoryFormVolunteerWorks_OnRowCommand">
				<Columns>
					<asp:TemplateField HeaderText="#">
						<ItemTemplate>
							<%#: Container.DataItemIndex + 1 %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField HeaderText="Organization" DataField="Organization" />
					<asp:BoundField HeaderText="Description" DataField="Description" />
					<asp:TemplateField HeaderText="Dates">
						<ItemTemplate>
							<%#: $"{Item.FromDate:d} - {Item.ToDate:d}" %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Actions">
						<ItemTemplate>
							<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument="<%# Item.GraduateDirectoryFormVolunteerWorkID %>" />
							<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument="<%# Item.GraduateDirectoryFormVolunteerWorkID %>" ConfirmMessage="Are you sure want to delete this record?" />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</aspire:AspireGridView>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Workshops/Seminars Attended</h3>
		</div>
		<div class="panel-body">
			<aspire:AspireButton runat="server" ButtonType="Success" CausesValidation="False" Glyphicon="plus" Text="Add" ID="btnAddGraduateDirectoryFormWorkshops" OnClick="btnAddGraduateDirectoryFormWorkshops_OnClick" />
			<p></p>
			<aspire:AspireGridView runat="server" ItemType="Aspire.Model.Entities.GraduateDirectoryFormWorkshop" ID="gvGraduateDirectoryFormWorkshops" AutoGenerateColumns="False" OnRowCommand="gvGraduateDirectoryFormWorkshops_OnRowCommand">
				<Columns>
					<asp:TemplateField HeaderText="#">
						<ItemTemplate>
							<%#: Container.DataItemIndex + 1 %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField HeaderText="Type" DataField="TypeFullName" />
					<asp:BoundField HeaderText="Description" DataField="Description" />
					<asp:TemplateField HeaderText="Dates">
						<ItemTemplate>
							<%#: $"{Item.FromDate:d} - {Item.ToDate:d}" %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Actions">
						<ItemTemplate>
							<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument="<%# Item.GraduateDirectoryFormWorkshopID %>" />
							<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument="<%# Item.GraduateDirectoryFormWorkshopID %>" ConfirmMessage="Are you sure want to delete this record?" />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</aspire:AspireGridView>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Extra Curricular Activities</h3>
		</div>
		<div class="panel-body">
			<aspire:AspireButton runat="server" ButtonType="Success" CausesValidation="False" Glyphicon="plus" Text="Add" ID="btnAddGraduateDirectoryExtraCurricularActivity" OnClick="btnAddGraduateDirectoryExtraCurricularActivity_OnClick" />
			<p></p>
			<aspire:AspireGridView runat="server" ItemType="Aspire.Model.Entities.GraduateDirectoryExtraCurricularActivity" ID="gvGraduateDirectoryExtraCurricularActivities" AutoGenerateColumns="False" OnRowCommand="gvGraduateDirectoryExtraCurricularActivities_OnRowCommand">
				<Columns>
					<asp:TemplateField HeaderText="#">
						<ItemTemplate>
							<%#: Container.DataItemIndex + 1 %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField HeaderText="Description" DataField="Description" />
					<asp:TemplateField HeaderText="Actions">
						<ItemTemplate>
							<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument="<%# Item.GraduateDirectoryExtraCurricularActivityID %>" />
							<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument="<%# Item.GraduateDirectoryExtraCurricularActivityID %>" ConfirmMessage="Are you sure want to delete this record?" />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</aspire:AspireGridView>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Skills</h3>
		</div>
		<div class="panel-body">
			<aspire:AspireButton runat="server" ButtonType="Success" CausesValidation="False" Glyphicon="plus" Text="Add" ID="btnAddGraduateDirectoryFormSkill" OnClick="btnAddGraduateDirectoryFormSkill_OnClick" />
			<p></p>
			<aspire:AspireGridView runat="server" ItemType="Aspire.Model.Entities.GraduateDirectoryFormSkill" ID="gvGraduateDirectoryFormSkill" AutoGenerateColumns="False" OnRowCommand="gvGraduateDirectoryFormSkill_OnRowCommand">
				<Columns>
					<asp:TemplateField HeaderText="#">
						<ItemTemplate>
							<%#: Container.DataItemIndex + 1 %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField HeaderText="Description" DataField="Description" />
					<asp:TemplateField HeaderText="Actions">
						<ItemTemplate>
							<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument="<%# Item.GraduateDirectorySkillID %>" />
							<aspire:AspireLinkButton CausesValidation="False" runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument="<%# Item.GraduateDirectorySkillID %>" ConfirmMessage="Are you sure want to delete this record?" />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</aspire:AspireGridView>
		</div>
	</div>

	<div class="text-center">
		<aspire:AspireButton runat="server" Text="Submit" ConfirmMessage="After submit you will not be able to modify this record. Are you sure you want to submit?" ButtonType="Primary" ID="btnSubmit" OnClick="btnSubmit_OnClick" CausesValidation="False" />
	</div>

	<aspire:AspireModal runat="server" ID="modalGraduateDirectoryFormWorkExperience" HeaderText="Work Experience">
		<BodyTemplate>
			<asp:UpdatePanel runat="server">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertGraduateDirectoryFormWorkExperience" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlExperienceType" />
						<aspire:AspireDropDownList runat="server" ID="ddlExperienceType" ValidationGroup="GraduateDirectoryFormWorkExperience" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExperienceType" ErrorMessage="This field is required." ValidationGroup="GraduateDirectoryFormWorkExperience" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Designation:" AssociatedControlID="tbDesignation" />
						<aspire:AspireTextBox runat="server" ID="tbDesignation" MaxLength="1000" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbDesignation" ErrorMessage="This field is required." ValidationGroup="GraduateDirectoryFormWorkExperience" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Organization:" AssociatedControlID="tbOrganization" />
						<aspire:AspireTextBox runat="server" ID="tbOrganization" MaxLength="1000" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbOrganization" ErrorMessage="This field is required." ValidationGroup="GraduateDirectoryFormWorkExperience" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Duration:" AssociatedControlID="tbDuration" />
						<aspire:AspireTextBox runat="server" ID="tbDuration" MaxLength="1000" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbDuration" ErrorMessage="This field is required." ValidationGroup="GraduateDirectoryFormWorkExperience" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Roles/Tasks:" AssociatedControlID="tbRoles" />
						<aspire:AspireTextBox runat="server" ID="tbRoles" MaxLength="1000" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbRoles" ErrorMessage="This field is required." ValidationGroup="GraduateDirectoryFormWorkExperience" />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" Text="Save" ButtonType="Primary" ID="btnSaveGraduateDirectoryFormWorkExperience" OnClick="btnSaveGraduateDirectoryFormWorkExperience_OnClick" ValidationGroup="GraduateDirectoryFormWorkExperience" />
					<aspire:AspireModalCloseButton runat="server" CausesValidation="false">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>

	<aspire:AspireModal runat="server" ID="modalGraduateDirectoryFormProject">
		<BodyTemplate>
			<asp:UpdatePanel runat="server">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertGraduateDirectoryFormProject" />
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
								<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="GraduateDirectoryFormProject" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterID" ErrorMessage="This field is required." ValidationGroup="GraduateDirectoryFormProject" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Project Type:" AssociatedControlID="ddlProjectType" />
								<aspire:AspireDropDownList runat="server" ID="ddlProjectType" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlProjectType" ErrorMessage="This field is required." ValidationGroup="GraduateDirectoryFormProject" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Description:" AssociatedControlID="tbDescription" />
						<aspire:AspireTextBox runat="server" ID="tbDescription" MaxLength="1000" TextMode="MultiLine" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbDescription" ErrorMessage="This field is required." ValidationGroup="GraduateDirectoryFormProject" />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" Text="Save" ButtonType="Primary" ID="btnSaveGraduateDirectoryFormProject" OnClick="btnSaveGraduateDirectoryFormProject_OnClick" ValidationGroup="GraduateDirectoryFormProject" />
					<aspire:AspireModalCloseButton runat="server" CausesValidation="false">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>

	<aspire:AspireModal runat="server" ID="modalGraduateDirectoryVolunteerWork">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelVolunteerWork" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertGraduateDirectoryVolunteerWork" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Organization:" AssociatedControlID="tbOrganizationVW" />
						<aspire:AspireTextBox runat="server" ID="tbOrganizationVW" MaxLength="500" ValidationGroup="GraduateDirectoryVolunteerWork" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbOrganizationVW" ErrorMessage="This field is required." ValidationGroup="GraduateDirectoryVolunteerWork" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Description:" AssociatedControlID="tbDescriptionVW" />
						<aspire:AspireTextBox runat="server" ID="tbDescriptionVW" MaxLength="1000" TextMode="MultiLine" ValidationGroup="GraduateDirectoryVolunteerWork" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbDescriptionVW" ErrorMessage="This field is required." ValidationGroup="GraduateDirectoryVolunteerWork" />
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="From Date:" AssociatedControlID="dtpFromDateVW" />
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpFromDateVW" DisplayMode="ShortDate" ValidationGroup="GraduateDirectoryVolunteerWork" />
								<aspire:AspireDateTimeValidator AllowNull="False" runat="server" ControlToValidate="dtpFromDateVW" InvalidRangeErrorMessage="Date is invalid." InvalidDataErrorMessage="Date is invalid." RequiredErrorMessage="This field is required." ValidationGroup="GraduateDirectoryVolunteerWork" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="To Date:" AssociatedControlID="dtpToDateVW" />
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpToDateVW" DisplayMode="ShortDate" ValidationGroup="GraduateDirectoryVolunteerWork" />
								<aspire:AspireDateTimeValidator AllowNull="False" runat="server" ControlToValidate="dtpToDateVW" InvalidRangeErrorMessage="Date is invalid." InvalidDataErrorMessage="Date is invalid." RequiredErrorMessage="This field is required." ValidationGroup="GraduateDirectoryVolunteerWork" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Always">
				<ContentTemplate>
					<aspire:AspireButton runat="server" Text="Save" ButtonType="Primary" ID="btnSaveGraduateDirectoryVolunteerWork" OnClick="btnSaveGraduateDirectoryVolunteerWork_OnClick" ValidationGroup="GraduateDirectoryVolunteerWork" />
					<aspire:AspireModalCloseButton runat="server" CausesValidation="false">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>

	<aspire:AspireModal runat="server" ID="modalGraduateDirectoryWorkshop">
		<BodyTemplate>
			<asp:UpdatePanel runat="server">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertGraduateDirectoryWorkshop" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlType" />
						<aspire:AspireDropDownList runat="server" ID="ddlType" ValidationGroup="GraduateDirectoryWorkshop" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlType" ErrorMessage="This field is required." ValidationGroup="GraduateDirectoryWorkshop" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Description:" AssociatedControlID="tbDescriptionWorkshop" />
						<aspire:AspireTextBox runat="server" ID="tbDescriptionWorkshop" MaxLength="1000" TextMode="MultiLine" ValidationGroup="GraduateDirectoryWorkshop" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbDescriptionWorkshop" ErrorMessage="This field is required." ValidationGroup="GraduateDirectoryWorkshop" />
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="From Date:" AssociatedControlID="dtpFromDateWorkshop" />
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpFromDateWorkshop" DisplayMode="ShortDate" ValidationGroup="GraduateDirectoryWorkshop" />
								<aspire:AspireDateTimeValidator AllowNull="False" runat="server" ControlToValidate="dtpFromDateWorkshop" InvalidRangeErrorMessage="Date is invalid." InvalidDataErrorMessage="Date is invalid." RequiredErrorMessage="This field is required." ValidationGroup="GraduateDirectoryWorkshop" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="To Date:" AssociatedControlID="dtpToDateWorkshop" />
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpToDateWorkshop" DisplayMode="ShortDate" ValidationGroup="GraduateDirectoryWorkshop" />
								<aspire:AspireDateTimeValidator AllowNull="False" runat="server" ControlToValidate="dtpToDateWorkshop" InvalidRangeErrorMessage="Date is invalid." InvalidDataErrorMessage="Date is invalid." RequiredErrorMessage="This field is required." ValidationGroup="GraduateDirectoryWorkshop" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Always">
				<ContentTemplate>
					<aspire:AspireButton runat="server" Text="Save" ButtonType="Primary" ID="btnSaveGraduateDirectoryWorkshop" OnClick="btnSaveGraduateDirectoryWorkshop_OnClick" ValidationGroup="GraduateDirectoryWorkshop" />
					<aspire:AspireModalCloseButton runat="server" CausesValidation="false">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>

	<aspire:AspireModal runat="server" ID="modalGraduateDirectoryExtraCurricularActivity">
		<BodyTemplate>
			<asp:UpdatePanel runat="server">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertGraduateDirectoryExtraCurricularActivity" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Description:" AssociatedControlID="tbDescriptionECA" />
						<aspire:AspireTextBox runat="server" ID="tbDescriptionECA" MaxLength="1000" TextMode="MultiLine" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbDescriptionECA" ErrorMessage="This field is required." ValidationGroup="GraduateDirectoryExtraCurricularActivity" />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" Text="Save" ButtonType="Primary" ID="btnSaveGraduateDirectoryExtraCurricularActivity" OnClick="btnSaveGraduateDirectoryExtraCurricularActivity_OnClick" ValidationGroup="GraduateDirectoryExtraCurricularActivity" />
					<aspire:AspireModalCloseButton runat="server" CausesValidation="false">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>

	<aspire:AspireModal runat="server" ID="modalGraduateDirectoryFormSkill">
		<BodyTemplate>
			<asp:UpdatePanel runat="server">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertGraduateDirectoryFormSkill" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Description:" AssociatedControlID="tbDescriptionSkill" />
						<aspire:AspireTextBox runat="server" ID="tbDescriptionSkill" MaxLength="1000" TextMode="MultiLine" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbDescriptionSkill" ErrorMessage="This field is required." ValidationGroup="GraduateDirectoryFormSkill" />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Always">
				<ContentTemplate>
					<aspire:AspireButton runat="server" Text="Save" ButtonType="Primary" ID="btnSaveGraduateDirectoryFormSkill" OnClick="btnSaveGraduateDirectoryFormSkill_OnClick" ValidationGroup="GraduateDirectoryFormSkill" />
					<aspire:AspireModalCloseButton runat="server" CausesValidation="false">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
