﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ExamSeats.aspx.cs" Inherits="Aspire.Web.Sys.Student.ExamSeatingPlan.ExamSeats" %>

<%@ Register Src="~/Sys/Common/ExamSeatingPlan/StudentExamSeats.ascx" TagPrefix="uc1" TagName="StudentExamSeats" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester: " AssociatedControlID="ddlSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Exam Type: " AssociatedControlID="ddlExamTypeFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlExamTypeFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlExamTypeFilter_OnSelectedIndexChanged" />
		</div>
	</div>
	<uc1:StudentExamSeats runat="server" ID="studentExamSeats" />
</asp:Content>
