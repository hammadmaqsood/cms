﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;

namespace Aspire.Web.Sys.Student.ExamSeatingPlan
{
	[AspirePage("C24561C3-812D-4C48-A463-75E56F9A466F", UserTypes.Student, "~/Sys/Student/ExamSeatingPlan/StudentSlip.aspx", "Student Slip", false, AspireModules.ExamSeatingPlan)]
	public partial class StudentSlip : StudentAndParentPage, IReportViewer
	{
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Student Slips";
		public string ReportName => "Student Slips";
		public bool ExportToExcel => false;
		public bool ExportToWord => false;
		private short? SemesterID => this.Request.GetParameterValue<short>(nameof(this.SemesterID));
		private ExamConduct.ExamTypes? ExamType => this.Request.GetParameterValue<ExamConduct.ExamTypes>(nameof(this.ExamType));

		public static string GetPageUrl(short? semesterID, ExamConduct.ExamTypes? examTypeEnum)
		{
			return typeof(StudentSlip).GetAspirePageAttribute().PageUrl.AttachQueryParams("SemesterID", semesterID, "ExamType", examTypeEnum, ReportView.Inline, true, ReportView.ReportFormat, ReportFormats.PDF);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this.SemesterID == null || this.ExamType == null)
			{
				reportViewer.LocalReport.DataSources.Clear();
				return;
			}
			reportViewer.LocalReport.DataSources.Clear();
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/ExamSeatingPlan/Common/StudentSlips.rdlc".MapPath();
			var registeredCourses = Aspire.BL.Core.ExamSeatingPlan.Common.ExamSeats.GetStudentSlips(this.StudentIdentity.Enrollment, this.SemesterID.Value, this.ExamType.Value, true, this.StudentIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource("RegisteredCourses", registeredCourses));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}