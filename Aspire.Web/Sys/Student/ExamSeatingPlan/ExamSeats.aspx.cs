﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Student.ExamSeatingPlan
{
	[AspirePage("CD89078A-7FF3-48F2-9BDC-3FBEFD41613C", UserTypes.Student, "~/Sys/Student/ExamSeatingPlan/ExamSeats", "Exam Seating Plan", true, AspireModules.ExamSeatingPlan)]
	public partial class ExamSeats : StudentAndParentPage
	{
		public override PageIcon PageIcon => FontAwesomeIcons.solid_list.GetIcon();
		public override string PageTitle => "Exam Seating Plan";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var examSeatsFilterCustomListItems = BL.Core.ExamSeatingPlan.Common.ExamSeats.GetSemestersExamTypes(this.StudentIdentity.LoginSessionGuid);
				if (examSeatsFilterCustomListItems == null || !examSeatsFilterCustomListItems.Semesters.Any() || !examSeatsFilterCustomListItems.ExamTypes.Any())
				{
					this.AddErrorAlert("No exam seating plan record found.");
					Redirect<Dashboard>();
					return;
				}

				this.ddlSemesterIDFilter.DataBind(examSeatsFilterCustomListItems.Semesters);
				this.ddlExamTypeFilter.DataBind(examSeatsFilterCustomListItems.ExamTypes);
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlExamTypeFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlExamTypeFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var examTypeEnum = this.ddlExamTypeFilter.GetSelectedEnumValue<ExamConduct.ExamTypes>();
			this.studentExamSeats.DisplayData(semesterID, examTypeEnum, this.StudentIdentity.Enrollment);
		}
	}
}