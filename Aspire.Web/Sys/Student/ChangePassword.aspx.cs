﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Student
{
	[AspirePage("1D683AA2-3960-4761-85B4-6E7D1635C190", UserTypes.Student, "~/Sys/Student/ChangePassword.aspx", "Change Password", true, AspireModules.None)]
	public partial class ChangePassword : StudentAndParentPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.lock_);
		public override string PageTitle => "Change Password";

		protected void Page_Load(object sender, EventArgs e)
		{
		}
	}
}