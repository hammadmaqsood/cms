﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="RegisterRegularCourses.aspx.cs" Inherits="Aspire.Web.Sys.Student.CourseRegistration.RegisterRegularCourses" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<table class="table table-bordered table-condensed tableCol4">
		<tr>
			<th>Semester</th>
			<td>
				<aspire:Label runat="server" ID="lblSemesterID" />
			</td>
			<th>Class</th>
			<td>
				<aspire:Label runat="server" ID="lblClass" />
			</td>
		</tr>
	</table>
	<asp:Repeater runat="server" ID="repeaterOfferedCourses">
		<HeaderTemplate>
			<div class="table-responsive">
				<table class="table table-bordered table-responsive table-condensed table-hover">
					<thead>
						<tr>
							<th rowspan="2"></th>
							<th colspan="5" class="text-center">Offered Course</th>
							<th colspan="4" class="text-center">Equivalent Student's Roadmap Course</th>
						</tr>
						<tr>
							<th class="text-center">Code</th>
							<th>Title</th>
							<th class="text-center">Credit
							<br />
								Hours</th>
							<th class="text-center">Majors</th>
							<th>Class</th>
							<th class="text-center">Code</th>
							<th>Title</th>
							<th class="text-center">Credit
							<br />
								Hours</th>
							<th class="text-center">Majors</th>
						</tr>
					</thead>
					<tbody>
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
				<td>
					<aspire:Label runat="server" Visible='<%# (bool)Eval("Registered") %>' Text="Registered" />
					<aspire:AspireCheckBox runat="server" ID="cb" Visible='<%# (bool)Eval("Registered")==false %>' ValidationGroup="Register" />
					<aspire:HiddenField runat="server" ID="hf" Mode="Encrypted" Value='<%# Eval("OfferedCourseID") %>' />
				</td>
				<td class="text-center"><%#: Eval("CourseCode") %></td>
				<td><%#: Eval("Title") %></td>
				<td class="text-center"><%#: Eval("CreditHours") %></td>
				<td class="text-center"><%#: Eval("Majors") %> </td>
				<td><%# Eval("ClassNamesHtmlEncoded") %></td>
				<td class="text-center"><%#: Eval("EquivalentCourseCode") %></td>
				<td><%#: Eval("EquivalentTitle") %></td>
				<td class="text-center"><%#: Eval("EquivalentCreditHours") %></td>
				<td class="text-center"><%#: Eval("EquivalentMajors") %> </td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</tbody>
			</table>
			</div>
		</FooterTemplate>
	</asp:Repeater>
	<div class="text-center">
		<aspire:AspireButton runat="server" ButtonType="Primary" Text="Register" ValidationGroup="Register" ID="btnRegister" OnClick="btnRegister_OnClick" />
		<aspire:AspireHyperLink runat="server" CssClass="btn btn-default" Text="Show all offered courses" NavigateUrl="~/Sys/Student/CourseRegistration/OfferedCourses.aspx" />
		<aspire:AspireButton runat="server" ButtonType="Danger" Text="Generate Fee Challan" CausesValidation="False" ID="btnGenerateFeeChallan" OnClick="btnGenerateFeeChallan_OnClick" />
	</div>
</asp:Content>
