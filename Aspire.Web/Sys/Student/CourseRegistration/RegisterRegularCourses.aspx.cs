﻿using Aspire.BL.Core.CourseRegistration.Common;
using Aspire.BL.Core.FeeManagement.Student;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Sys.Student.FeeManagement.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Student.CourseRegistration
{
	[AspirePage("{4738748B-2EBC-40B8-A367-A595FFE224D4}", UserTypes.Student, "~/Sys/Student/CourseRegistration/RegisterRegularCourses.aspx", "Register Regular Courses", true, AspireModules.CourseRegistration)]
	public partial class RegisterRegularCourses : StudentPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_book);
		public override string PageTitle => "Register Regular Courses";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var courses = BL.StudentPortal.CourseRegistration.GetRegularOfferedCoursesForStudent(this.StudentIdentity.StudentID, out var offeredSemesterID, out var studentClass, out bool canStudentRegisterCourseOnline);
				if (offeredSemesterID == null)
				{
					this.AddErrorAlert("Course Registration has been closed.");
					Redirect<Dashboard>();
					return;
				}

				if (canStudentRegisterCourseOnline == false)
				{
					this.AddErrorAlert("You are not allowed to register the course yourself. Please contact your Course Coordinator for course registration.");
					Redirect<Dashboard>();
					return;
				}
				if (studentClass == null)
				{
					this.AddErrorAlert("Contact Course Coordinator. Reason: Class is not defined.");
					Redirect<Dashboard>();
					return;
				}

				this.lblSemesterID.Text = offeredSemesterID.ToSemesterString();
				this.ViewState["OfferedSemesterID"] = offeredSemesterID.Value;
				this.lblClass.Text = studentClass.ClassName;
				this.repeaterOfferedCourses.DataBind(courses);
				this.btnRegister.Enabled = courses.Any(c => !c.Registered);
				this.btnGenerateFeeChallan.Enabled = true;
			}
		}

		protected void btnRegister_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var offeredSemesterID = (short)this.ViewState["OfferedSemesterID"];
			var offeredCourseIDs = new List<int>();
			foreach (RepeaterItem item in this.repeaterOfferedCourses.Items)
			{
				if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
					continue;
				var cb = (AspireCheckBox)item.FindControl("cb");
				if (cb.Visible && cb.Checked)
					offeredCourseIDs.Add(((Lib.WebControls.HiddenField)item.FindControl("hf")).Value.ToInt());
			}

			if (!offeredCourseIDs.Any())
			{
				this.AddWarningAlert("No selected course found.");
				Redirect<RegisterRegularCourses>();
				return;
			}

			var coursesRegister = BL.Core.CourseRegistration.Student.CourseRegistration.RegisterCourses(this.StudentIdentity.StudentID, offeredSemesterID, offeredCourseIDs, false, this.StudentIdentity.LoginSessionGuid);
			coursesRegister.GetErrorMessages().ForEach(message => this.AddErrorAlert(message));
			coursesRegister.GetWarningMessages().ForEach(message => this.AddWarningAlert(message));
			coursesRegister.GetSuccessMessages().ForEach(message => this.AddSuccessAlert(message));
			Redirect<RegisterRegularCourses>();
		}

		protected void btnGenerateFeeChallan_OnClick(object sender, EventArgs e)
		{
			var result = StudentFees.AddStudentFee((short)this.ViewState["OfferedSemesterID"], this.AspireIdentity.LoginSessionGuid);
			switch (result.Status)
			{
				case StudentFees.AddStudentFeeResults.Statuses.StudentFeeAlreadyGenerated:
					this.AddErrorAlert("Contact Accounts Office. Reason: Semester Fee has already been generated.");
					break;
				case StudentFees.AddStudentFeeResults.Statuses.FeeStructureDetailsNotFound:
					this.AddErrorAlert("Contact Accounts Office. Reason: Fee Structure not available.");
					break;
				case StudentFees.AddStudentFeeResults.Statuses.FeeStructureDetailsTutionFeeNotFound:
					this.AddErrorAlert("Contact Account Office. Reason: Tuition Fee not found in Fee Structure.");
					break;
				case StudentFees.AddStudentFeeResults.Statuses.CoursesNotRegistered:
					this.AddErrorAlert("Fee Challan cannot be generated because no registered course found.");
					break;
				case StudentFees.AddStudentFeeResults.Statuses.ContactAccountsOfficeZeroCreditHourFound:
					this.AddErrorAlert("Contact Account Office. Reason: Zero Credit Hours course found in registered courses.");
					break;
				case StudentFees.AddStudentFeeResults.Statuses.ContactAccountsOfficeForeignerStudent:
					this.AddErrorAlert("Contact Account Office. Reason: Foreigner Student.");
					break;
				case StudentFees.AddStudentFeeResults.Statuses.ContactAccountsOfficeArrearsFound:
					this.AddErrorAlert("Contact Account Office. Reason: Arrears found.");
					break;
				case StudentFees.AddStudentFeeResults.Statuses.ContactAccountsOfficeLastSemesterFeeNotFound:
					this.AddErrorAlert("Contact Account Office. Reason: Last semester fee record not found.");
					break;
				case StudentFees.AddStudentFeeResults.Statuses.ContactAccountsOfficeNoPreviousStudentFeeFound:
					this.AddErrorAlert("Contact Account Office. Reason: No fee record found against student.");
					break;
				case StudentFees.AddStudentFeeResults.Statuses.ContactAccountsOfficeArrearsHeadFoundInFeeStructure:
					this.AddErrorAlert("Contact Account Office. Reason: Arrears head found in fee structure.");
					break;
				case StudentFees.AddStudentFeeResults.Statuses.ContactAccountsOfficeStudentStatusIssue:
					this.AddErrorAlert("Contact Account Office. Reason: Student Status is not cleared.");
					break;
				case StudentFees.AddStudentFeeResults.Statuses.ContactAccountsOfficeStudentSemesterStatusIssue:
					this.AddErrorAlert("Contact Account Office. Reason: Student Semester Status is not cleared.");
					break;
				case StudentFees.AddStudentFeeResults.Statuses.Success:
					if (result.StudentFeeIDs?.Any() != true)
						throw new InvalidOperationException();
					ViewFeeChallan.Redirect(result.StudentFeeIDs);
					break;
				case StudentFees.AddStudentFeeResults.Statuses.FeeDueDateNotFound:
					this.AddErrorAlert("Contact Account Office. Reason: Fee Due Date is not set by account office.");
					break;
				case StudentFees.AddStudentFeeResults.Statuses.FeeDueDatePassed:
					this.AddErrorAlert("Contact Account Office. Reason: Fee Due Date has been passed.");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}