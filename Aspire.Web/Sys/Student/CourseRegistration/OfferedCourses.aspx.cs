﻿using Aspire.BL.Core.CourseRegistration.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Student.CourseRegistration
{
	[AspirePage("B5B38CF5-F61B-4C90-9B49-D24FEC6CCE85", UserTypes.Student, "~/Sys/Student/CourseRegistration/OfferedCourses.aspx", "View Offered Courses", true, AspireModules.CourseRegistration)]
	public partial class OfferedCourses : StudentPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_book);
		public override string PageTitle => "Offered Courses";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (this.IsPostBack)
				return;

			var offeredCourses = Aspire.BL.Core.CourseRegistration.Student.CourseRegistration.GetAllOfferedCourses(this.StudentIdentity.LoginSessionGuid);

			if (offeredCourses == null)
			{
				this.AddErrorAlert("Course Registration has been closed or not yet opened.");
				Redirect<Dashboard>();
				return;
			}

			this.lblSemesterID.Text = offeredCourses.OfferedSemesterID.ToSemesterString();
			this.ViewState["OfferedSemesterID"] = offeredCourses.OfferedSemesterID;
			this.lblClass.Text = (offeredCourses.StudentClass?.ClassName).ToNAIfNullOrEmpty();
			if (offeredCourses.StudentClass == null)
			{
				this.AddErrorAlert("Contact Course Coordinator for course registration. Reason: Class is not defined.");
				Redirect<Dashboard>();
				return;
			}
			this.repeaterOfferedCourses.DataBind(offeredCourses.Courses);
		}

		protected void repeaterOfferedCourses_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Request":
					var offeredCourseIDs = new List<int> { e.DecryptedCommandArgumentToInt() };
					var coursesRegister = Aspire.BL.Core.CourseRegistration.Student.CourseRegistration.RegisterCourses(this.StudentIdentity.StudentID, (short)this.ViewState["OfferedSemesterID"], offeredCourseIDs, true, this.StudentIdentity.LoginSessionGuid);
					coursesRegister.GetErrorMessages().ForEach(message => this.AddErrorAlert(message));
					coursesRegister.GetWarningMessages().ForEach(message => this.AddWarningAlert(message));
					coursesRegister.GetSuccessMessages().ForEach(message => this.AddSuccessAlert(message));
					Redirect<OfferedCourses>();
					break;
			}
		}
	}
}