﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="RegisteredCourses.aspx.cs" Inherits="Aspire.Web.Sys.Student.CourseRegistration.RegisteredCourses" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Web.Sys.Student.ClassAttendance" %>
<%@ Import Namespace="Aspire.BL.Core.CourseRegistration.Student" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="table-responsive">
		<asp:Repeater runat="server" ID="repeaterSemesters">
			<ItemTemplate>
				<table class="table table-bordered table-responsive table-condensed table-hover">
					<caption>
						<%#:this.Eval("Semester") %></caption>
					<thead>
						<tr>
							<th class="text-center">#</th>
							<th class="text-center">Code</th>
							<th>Registered Course Title</th>
							<th class="text-center">Credit<br />
								Hours</th>
							<th class="text-center">Majors</th>
							<th>Offered Course Title</th>
							<th>Class</th>
							<th>Teacher Name</th>
							<th>Fee Status</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<asp:Repeater runat="server" DataSource='<%#this.Eval("RegisteredCourses") %>'>
							<ItemTemplate>
								<tr>
									<td class="text-center"><%#: Container.ItemIndex+1 %></td>
									<td class="text-center"><%#: this.Eval("RegisteredCourseCode") %></td>
									<td><%#:this.Eval("RegisteredTitle") %></td>
									<td class="text-center"><%#:this.Eval("RegisteredCreditHours") %></td>
									<td class="text-center"><%#:	this.Eval("RegisteredMajors").ToNAIfNullOrEmpty() %> </td>
									<td><%#:this.Eval("Title") %></td>
									<td><%#:this.Eval("ClassName") %></td>
									<td><%#:this.Eval("TeacherName").ToNAIfNullOrEmpty() %> </td>
									<td class="text-center"><%#:this.Eval("FeeStatusFullName").ToNAIfNullOrEmpty() %> </td>
									<td class="text-center"><aspire:AspireHyperLink runat="server" NavigateUrl="<%# AttendanceSummary.GetPageUrl((int)this.Eval(nameof(CourseRegistration.RegisteredCourse.RegisteredCourseID)))  %>" Text="View Attendance"/></td>
								</tr>
							</ItemTemplate>
						</asp:Repeater>
					</tbody>
				</table>
			</ItemTemplate>
		</asp:Repeater>
	</div>
</asp:Content>
