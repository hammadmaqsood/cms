﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Student.CourseRegistration
{
	[AspirePage("7A831C0C-C313-42C2-A2E5-8D6DB56201DF", UserTypes.Student, "~/Sys/Student/CourseRegistration/RegisteredCourses.aspx", "Registered Courses", true, AspireModules.CourseRegistration)]
	public partial class RegisteredCourses : StudentAndParentPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_book);
		public override string PageTitle => "Registered Courses";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var registeredCourses = Aspire.BL.Core.CourseRegistration.Student.CourseRegistration.GetRegisteredCourses(this.StudentIdentity.StudentID);
				if (!registeredCourses.Any())
				{
					this.AddInfoAlert("You have not registered any course yet.");
					this.repeaterSemesters.Visible = false;
					return;
				}
				var dataSource = registeredCourses.Select(rc => rc.SemesterID).Distinct().ToList().Select(s => new
				{
					SemesterID = s,
					Semester = s.ToSemesterString(),
					RegisteredCourses = registeredCourses.FindAll(rc => rc.SemesterID == s),
				}).OrderByDescending(s => s.SemesterID).ToList();
				this.repeaterSemesters.DataBind(dataSource);
			}
		}
	}
}