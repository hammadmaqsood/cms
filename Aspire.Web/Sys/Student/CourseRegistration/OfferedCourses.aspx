﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="OfferedCourses.aspx.cs" Inherits="Aspire.Web.Sys.Student.CourseRegistration.OfferedCourses" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<table class="table table-bordered table-condensed tableCol4">
		<tr>
			<th>Semester</th>
			<td>
				<aspire:Label runat="server" ID="lblSemesterID" />
			</td>
			<th>Class</th>
			<td>
				<aspire:Label runat="server" ID="lblClass" />
			</td>
		</tr>
	</table>
	<div class="row">
		<div class="form-group col-md-8">
			<aspire:AspireHyperLink runat="server" CssClass="btn btn-default" Text="Show all regular courses" NavigateUrl="~/Sys/Student/CourseRegistration/RegisterRegularCourses.aspx" />
		</div>
		<div class="form-group col-md-4">
			<div class="input-group">
				<input type="text" class="form-control" id="inputSearchOfferedCourses" />
				<div class="input-group-btn">
					<button type="button" class="btn btn-default" title="Clear" id="btnClearOfferedCourses"><i class="glyphicon glyphicon-remove"></i></button>
					<button type="button" class="btn btn-default" title="Search" id="btnSearchOfferedCourses"><i class="glyphicon glyphicon-search"></i></button>
				</div>
			</div>
		</div>
	</div>
	<asp:Repeater runat="server" ID="repeaterOfferedCourses" OnItemCommand="repeaterOfferedCourses_OnItemCommand">
		<HeaderTemplate>
			<div class="table-responsive">
				<table class="table table-bordered table-responsive table-condensed table-hover" id="<%= this.repeaterOfferedCourses.ClientID %>">
					<thead>
						<tr>
							<th colspan="5" class="text-center">Offered Course</th>
							<th colspan="4" class="text-center">Equivalent Student's Roadmap Course</th>
						</tr>
						<tr>
							<th class="text-center">Code</th>
							<th>Title</th>
							<th class="text-center">Credit<br />
								Hours</th>
							<th class="text-center">Majors</th>
							<th>Class</th>
							<th class="text-center">Code</th>
							<th>Title</th>
							<th class="text-center">Credit
							<br />
								Hours</th>
							<th class="text-center">Majors</th>
						</tr>
					</thead>
					<tbody>
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
				<td class="text-center"><%#: Eval("CourseCode") %></td>
				<td><%#: Eval("Title") %></td>
				<td class="text-center"><%#: Eval("CreditHours") %></td>
				<td class="text-center"><%#: Eval("Majors") %> </td>
				<td>
					<%# Eval("ClassNamesHtmlEncoded") %>
				</td>
				<td class="text-center"><%#: Eval("EquivalentCourseCode") %></td>
				<td><%#: Eval("EquivalentTitle") %></td>
				<td class="text-center"><%#: Eval("EquivalentCreditHours") %></td>
				<td class="text-center"><%#: Eval("EquivalentMajors") %> </td>
				<td>
					<aspire:Label runat="server" CssClass="text-success" Text="Registered" Visible='<%# Eval("Registered") %>' />
					<aspire:Label runat="server" CssClass="text-success" Text="Requested" Visible='<%# Eval("Requested") %>' />
					<%--<aspire:Label runat="server" Text='<%# Eval("RequestStatusFullName") %>' Visible='<%# Eval("RequestStatus")!=null %>' />--%>
					<aspire:AspireLinkButton runat="server" EncryptedCommandArgument='<%# Eval("OfferedCourseID") %>' Visible='<%# !(bool)Eval("Registered") && !(bool)Eval("Requested") %>' CausesValidation="False" CommandName="Register" Text="Request" />
				</td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</tbody>
			</table>
			</div>
		</FooterTemplate>
	</asp:Repeater>
	<script type="text/javascript">
		$(document).ready(function () {
			applyFilterEvents($("#<%=this.repeaterOfferedCourses.ClientID %>"), $("#inputSearchOfferedCourses"), $("#btnClearOfferedCourses"), $("#btnSearchOfferedCourses"), false);
		});
	</script>
</asp:Content>
