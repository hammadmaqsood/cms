﻿using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Candidate
{
	[AspirePage("{47A32BA4-A211-4480-A778-9263440AF33F}", UserTypes.Admins, "~/Sys/Candidate/Logoff.aspx", null, false, AspireModules.None)]
	public partial class Logoff : LogoffBasePage
	{
		public static string GetPageUrl(UserLoginHistory.LogOffReasons logoffReason)
		{
			return GetPageUrl(GetPageUrl<Logoff>(), logoffReason);
		}

		public static void Redirect(UserLoginHistory.LogOffReasons logoffReason)
		{
			Redirect(GetPageUrl(logoffReason));
		}

		protected override string LoginPageUrl => GetPageUrl<Logins.Candidate.Login>();
	}
}