﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Aspire.Web.Sys.Candidate.Dashboard" %>

<%@ Register Src="~/Sys/Candidate/DashboardNational.ascx" TagPrefix="uc1" TagName="DashboardNational" %>
<%@ Register Src="~/Sys/Candidate/DashboardInternational.ascx" TagPrefix="uc1" TagName="DashboardInternational" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Important Information</h3>
		</div>
		<div class="panel-body">
			<p style="font-size: x-large">
				For details related to Eligibility Criteria, Course RoadMaps, Admission Quotas Process, Computer Base Test (CBT) Subjects, Exemption From BU Entry Test, Fee Structure and Scholarships please 
				<a target="_blank" rel="noopener noreferrer" href="https://www.bahria.edu.pk/admissions/">click here</a>.
			</p>
		</div>
	</div>
	<uc1:DashboardNational runat="server" ID="dashboardNational" />
	<uc1:DashboardInternational runat="server" ID="dashboardInternational" />
</asp:Content>
