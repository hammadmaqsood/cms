﻿using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Candidate
{
	[AspirePage("{32E31561-AD71-406F-A983-8CF821F78526}", UserTypes.Admins, "~/Sys/Candidate/PrepareSession.aspx", null, false, AspireModules.None)]
	public partial class PrepareSession : BasePage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (SessionHelper.SideBarLinkHtml == null)
			{
				PrepareMenu();
			}
			Redirect(GetPageUrl<Dashboard>(), false);
		}

		private static void PrepareMenu()
		{
			if (CandidateIdentity.Current.ForeignStudent)
				SessionHelper.SetSideBarLinks(DefaultMenus.Candidate.LinksForeignStudent);
			else
				SessionHelper.SetSideBarLinks(DefaultMenus.Candidate.Links);
		}

		protected override void Authenticate()
		{
			if (CandidateIdentity.Current == null)
				throw new AspireAccessDeniedException(UserTypes.Candidate, UserTypes.Candidate);
		}
	}
}