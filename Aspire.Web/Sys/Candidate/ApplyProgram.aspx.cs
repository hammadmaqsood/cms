﻿using Aspire.BL.Core.Admissions.Candidate;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using Aspire.BL.Entities.Common;

namespace Aspire.Web.Sys.Candidate
{
	[AspirePage("993802F2-9893-468B-BA98-59681E0C90F4", UserTypes.Candidate, "~/Sys/Candidate/ApplyProgram.aspx", "Apply For Program", true, AspireModules.None)]
	public partial class ApplyProgram : CandidatePage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_edit);
		public override string PageTitle => "Apply For Program";

		private void AdmissionClosedOrAllProgramsApplied()
		{
			this.AddWarningAlert("Admissions are closed or you have applied for all opened programs.");
			Redirect<Dashboard>();
		}

		private List<Model.Entities.AdmissionCriteriaPreReq> _admissionCriteriaPreReqs = null;
		internal List<Model.Entities.AdmissionCriteriaPreReq> AdmissionCriteriaPreReqs
		{
			get
			{
				var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
				var admissionOpenProgramID = this.ddlChoice1AdmissionOpenProgramID.SelectedValue.ToNullableInt();
				if (instituteID == null || admissionOpenProgramID == null)
					return null;
				if (this._admissionCriteriaPreReqs != null && (int?)this.ViewState[nameof(admissionOpenProgramID)] == admissionOpenProgramID)
					return this._admissionCriteriaPreReqs;
				this._admissionCriteriaPreReqs = ApplyForProgram.GetAdmissionCriteriaPreReqs(instituteID.Value, admissionOpenProgramID.Value, this.CandidateIdentity.LoginSessionGuid);
				this.ViewState[nameof(admissionOpenProgramID)] = admissionOpenProgramID.Value;
				return this._admissionCriteriaPreReqs;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (this.CandidateIdentity.ForeignStudent)
			{
				Redirect<ApplyPrograms>();
				return;
			}
			if (!this.IsPostBack)
			{
				var result = ApplyForProgram.GetNotAppliedAdmissionsOpenInstitutes(this.CandidateIdentity.LoginSessionGuid);

				if (!result.CandidateStatus.HasFlag(Model.Entities.Candidate.Statuses.Completed))
				{
					if (!result.CandidateStatus.HasFlag(Model.Entities.Candidate.Statuses.PictureUploaded))
						this.AddErrorAlert("Profile Picture is not yet uploaded.");
					else
						this.AddErrorAlert("Profile Information is not completed.");
					Redirect<Profile>();
					return;
				}
				if (result.Institutes?.Any() != true)
				{
					this.AdmissionClosedOrAllProgramsApplied();
					return;
				}
				var instituteListItems = result.Institutes.Select(i => new ListItem(i.InstituteName, i.InstituteID.ToString()));
				this.ddlInstituteID.DataBind(instituteListItems, new ListItem("Select Campus", ""));

				this.ddlInstituteID_SelectedIndexChanged(null, null);
			}
		}

		private List<(int AdmissionOpenProgramID, string ProgramName, string ProgramShortName, string ProgramAlias, ProgramDurations ProgramDurationEnum)?> AdmissionOpenPrograms
		{
			get => this.ViewState[nameof(this.AdmissionOpenPrograms)] as List<(int AdmissionOpenProgramID, string ProgramName, string ProgramShortName, string ProgramAlias, ProgramDurations ProgramDurationEnum)?>;
			set => this.ViewState[nameof(this.AdmissionOpenPrograms)] = value;
		}

		protected void ddlInstituteID_SelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			if (instituteID == null)
			{
				this.updatePanelInstituteSelected.Visible = false;
				this.AdmissionOpenPrograms = null;
				this.ddlInstituteID.Focus();
			}
			else
			{
				var notAppliedAdmissionOpenPrograms = ApplyForProgram.GetNotAppliedAdmissionsOpenPrograms(instituteID.Value, this.CandidateIdentity.LoginSessionGuid);
				if (notAppliedAdmissionOpenPrograms?.Any() != true)
				{
					this.AdmissionClosedOrAllProgramsApplied();
					return;
				}
				this.AdmissionOpenPrograms = notAppliedAdmissionOpenPrograms.Select(aop => ((int AdmissionOpenProgramID, string ProgramName, string ProgramShortName, string ProgramAlias, ProgramDurations ProgramDurationEnum)?)(aop.AdmissionOpenProgramID, aop.ProgramName, aop.ProgramShortName, aop.ProgramAlias, aop.ProgramDurationEnum)).ToList();
				var programs = this.AdmissionOpenPrograms.Select(aop => new ListItem(aop.Value.ProgramShortName, aop.Value.AdmissionOpenProgramID.ToString()));
				this.ddlChoice1AdmissionOpenProgramID.DataBind(programs, new ListItem("Select Program Applying For", ""));
				this.updatePanelInstituteSelected.Visible = true;
				this.ddlChoice1AdmissionOpenProgramID_SelectedIndexChanged(null, null);
				this.ddlChoice1AdmissionOpenProgramID.Focus();
			}
		}

		protected void ddlChoice1AdmissionOpenProgramID_SelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			var admissionOpenProgramID = this.ddlChoice1AdmissionOpenProgramID.SelectedValue.ToNullableInt();
			if (instituteID == null || admissionOpenProgramID == null)
			{
				this.updatePanelProgramSelected.Visible = false;
				return;
			}
			var (admissionOpenProgram, researchThemeAreas) = ApplyForProgram.GetAdmissionOpenProgram(instituteID.Value, admissionOpenProgramID.Value, this.CandidateIdentity.LoginSessionGuid);
			if (admissionOpenProgram == null)
			{
				this.AdmissionClosedOrAllProgramsApplied();
				return;
			}

			if (admissionOpenProgram.Program.DegreeLevel == (byte)DegreeLevels.Doctorate)
			{
				this.ddlResearchThemeAreaID.Items.Clear();
				this.dvResearchThemeArea.Visible = true;
				this.ddlResearchThemeAreaID.Items.Add(CommonListItems.Select);
				foreach (var rtArea in researchThemeAreas)
					this.ddlResearchThemeAreaID.AddItem(rtArea.AreaName, rtArea.ResearchThemeAreaID.ToString(), rtArea.ResearchThemeName);
			}
			else
			{
				this.dvResearchThemeArea.Visible = false;
				this.ddlResearchThemeAreaID.Items.Clear();
			}
			this.updatePanelProgramSelected.Visible = true;

			this.tbEligibilityCriteriaStatement.Text = admissionOpenProgram.EligibilityCriteriaStatement;
			this.rblShift.FillShifts(admissionOpenProgram.ShiftEnumFlags);
			if (this.rblShift.Items.Count == 0)
				throw new InvalidOperationException("Shifts not found.");

			this.rblShift.SelectedIndex = 0;
			this.tbShift.Text = this.rblShift.SelectedItem.Text;
			if (this.rblShift.Items.Count > 1)
			{
				this.divrblShift.Visible = this.rblShift.Visible = true;
				this.tbShift.Visible = false;
				this.lblShift.AssociatedControlID = this.rblShift.ID;
			}
			else
			{
				this.divrblShift.Visible = this.rblShift.Visible = false;
				this.tbShift.Visible = true;
				this.lblShift.AssociatedControlID = this.tbShift.ID;
			}

			this.tbAdmissionProcessingFee.Text = $"Rs. {admissionOpenProgram.AdmissionProcessingFee}";

			switch (admissionOpenProgram.EntryTestTypeEnum)
			{
				case AdmissionOpenProgram.EntryTestTypes.None:
					this.tbEntryTestDateTime.Text = string.Empty.ToNAIfNullOrEmpty();
					this.tbEntryTestDuration.Text = string.Empty.ToNAIfNullOrEmpty();
					break;
				case AdmissionOpenProgram.EntryTestTypes.ComputerBasedTest:
					this.tbEntryTestDateTime.Text = $"(Computer Based Test) Starting from {admissionOpenProgram.EntryTestStartDate:D}";
					this.tbEntryTestDuration.Text = $"{admissionOpenProgram.EntryTestDuration} Minutes";
					break;
				case AdmissionOpenProgram.EntryTestTypes.PaperBasedTest:
					this.tbEntryTestDateTime.Text = admissionOpenProgram.EntryTestStartDate?.ToString("F") ?? throw new InvalidOperationException();
					this.tbEntryTestDuration.Text = $"{admissionOpenProgram.EntryTestDuration} Minutes";
					break;
				default:
					throw new NotImplementedEnumException(admissionOpenProgram.EntryTestTypeEnum);
			}
			this.rblCreditTransfer.FillYesNo().SetBooleanValue(false);


			var preReqQualifications = this.AdmissionCriteriaPreReqs?.Select(acpr => acpr.PreReqQualification).Distinct().Select(p => new ListItem(p, p)).ToList();
			if (preReqQualifications?.Any() != true)
				throw new DataException($"Pre-Requisite data is missing in Admission Criteria. AdmissionOpenProgramID: {admissionOpenProgram.AdmissionOpenProgramID}");

			this.ddlPreReqQualification.DataBind(preReqQualifications, new ListItem("Select Pre-Requisite Qualification", ""));
			this.ddlPreReqQualification.SelectedIndex = 0;
			this.ddlPreReqQualification_SelectedIndexChanged(null, null);
			this.ddlPreReqQualification.Focus();
		}

		protected void ddlPreReqQualification_SelectedIndexChanged(object sender, EventArgs e)
		{
			var preReqQualification = this.ddlPreReqQualification.SelectedValue.ToNullIfEmpty();

			if (preReqQualification == null)
			{
				this.updatePanelPreReqQualificationSelected.Visible = false;
				return;
			}
			this.updatePanelPreReqQualificationSelected.Visible = true;

			var admissionCriteriaPreReqs = this.AdmissionCriteriaPreReqs?.Where(acpr => acpr.PreReqQualification == preReqQualification)
				.Select(acpr => new ListItem(acpr.PreReqQualificationResultStatus, acpr.AdmissionCriteriaPreReqID.ToString())).ToList();
			if (admissionCriteriaPreReqs?.Any() != true)
				throw new DataException($"Pre-Requisite data is missing in Admission Criteria. AdmissionOpenProgramID: {this.ddlChoice1AdmissionOpenProgramID.SelectedValue}");
			this.ddlAdmissionCriteriaPreReqID.DataBind(admissionCriteriaPreReqs, new ListItem("Select Pre-Requisite Qualification Result Status", ""));
			this.ddlAdmissionCriteriaPreReqID.SelectedIndex = 0;
			this.ddlAdmissionCriteriaPreReqID_SelectedIndexChanged(null, null);
			this.ddlAdmissionCriteriaPreReqID.Focus();
		}

		protected void ddlAdmissionCriteriaPreReqID_SelectedIndexChanged(object sender, EventArgs e)
		{
			var preReqQualification = this.ddlPreReqQualification.SelectedItem.Text.ToNullIfEmpty();
			var admissionCriteriaPreReqID = this.ddlAdmissionCriteriaPreReqID.SelectedValue.ToNullableInt();
			if (preReqQualification == null || admissionCriteriaPreReqID == null)
			{
				this.updatePanelAdmissionCriteriaPreReqIDSelected.Visible = false;
				return;
			}
			this.updatePanelAdmissionCriteriaPreReqIDSelected.Visible = true;

			var preReqDegreeTypes = this.AdmissionCriteriaPreReqs?
							.Where(acpr => acpr.PreReqQualification == preReqQualification && acpr.AdmissionCriteriaPreReqID == admissionCriteriaPreReqID.Value)
							.SelectMany(acpr => acpr.AdmissionCriteriaPreReqDegrees)
							.Select(acprd => acprd.DisplaySequence).Distinct().ToList().Select(s => new AcademicRecord.AdmissionCriteriaPreq
							{
								AdmissionCriteriaPreReqID = admissionCriteriaPreReqID.Value,
								DisplaySequence = s
							}).OrderBy(s => s.DisplaySequence).ToList();

			if (preReqDegreeTypes?.Any() != true)
				throw new DataException($"AdmissionCriteriaPreReqDegrees data is missing in Admission Criteria. AdmissionCriteriaPreReqID: {admissionCriteriaPreReqID}, AdmissionOpenProgramID: {this.ddlChoice1AdmissionOpenProgramID.SelectedValue}");

			this.repeaterAcademicRecords.DataBind(preReqDegreeTypes);
			this.repeaterAcademicRecords.Focus();
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
			this.ddlChoice1AdmissionOpenProgramID.Items.Cast<ListItem>().ForEach(i =>
			{
				var admissionOpenProgram = this.AdmissionOpenPrograms?.SingleOrDefault(aop => aop?.AdmissionOpenProgramID == i.Value.ToNullableInt());
				i.Attributes["data-ProgramName"] = admissionOpenProgram?.ProgramName;
				i.Attributes["data-ProgramShortName"] = admissionOpenProgram?.ProgramShortName;
				i.Attributes["data-ProgramAlias"] = admissionOpenProgram?.ProgramAlias;
				i.Attributes["data-ProgramDuration"] = admissionOpenProgram?.ProgramDurationEnum.ToFullName();
			});
		}

		protected void btnApply_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var admissionCriteriaPreReqID = this.ddlAdmissionCriteriaPreReqID.SelectedValue.ToInt();
			var choice1AdmissionOpenProgramID = this.ddlChoice1AdmissionOpenProgramID.SelectedValue.ToInt();
			var researchThemeAreaID = this.ddlResearchThemeAreaID.SelectedValue.ToNullableGuid();
			var creditTransfer = this.rblCreditTransfer.SelectedValue.ToBoolean();
			var shiftEnum = this.rblShift.GetSelectedEnumValue<Shifts>();
			var academicRecords = this.repeaterAcademicRecords.Items.Cast<RepeaterItem>().Select(i => ((AcademicRecord)i.FindControl("ucAcademicRecord")).ToAcademicRecord()).ToList();

			var result = ApplyForProgram.Apply(admissionCriteriaPreReqID, choice1AdmissionOpenProgramID, researchThemeAreaID, creditTransfer, shiftEnum, academicRecords, this.CandidateIdentity.LoginSessionGuid);

			switch (result)
			{
				case ApplyForProgram.ApplyForProgramStatuses.Choice1AdmissionNotOpened:
					this.AddErrorAlert($"Admissions are closed/not opened for \"{this.ddlChoice1AdmissionOpenProgramID.SelectedItem.Text}\" program.");
					Redirect<ApplyProgram>();
					return;
				case ApplyForProgram.ApplyForProgramStatuses.Choice1AlreadyApplied:
					this.AddWarningAlert($"You have already applied for \"{this.ddlChoice1AdmissionOpenProgramID.SelectedItem.Text}\" program.");
					Redirect<ApplyProgram>();
					return;
				case ApplyForProgram.ApplyForProgramStatuses.AdmissionCriteriaPreReqNotFound:
					this.AddNoRecordFoundAlert();
					Redirect<ApplyProgram>();
					return;
				case ApplyForProgram.ApplyForProgramStatuses.ProfileInformationIsBlank:
					this.AddErrorAlert("You cannot apply for any program unless profile information is completely provided.");
					Redirect<Profile>();
					return;
				case ApplyForProgram.ApplyForProgramStatuses.ProfileInformationPictureNotUploaded:
					this.AddErrorAlert("You cannot apply for any program unless profile information is completely provided. Profile Picture is missing.");
					Redirect<Profile>();
					return;
				case ApplyForProgram.ApplyForProgramStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<ApplyProgram>();
					return;
				case ApplyForProgram.ApplyForProgramStatuses.Success:
					var admissionOpenProgram = this.AdmissionOpenPrograms?.SingleOrDefault(aop => aop?.AdmissionOpenProgramID == choice1AdmissionOpenProgramID);
					var html = $"Your application for admission in \"{admissionOpenProgram?.ProgramShortName}\" has been saved. Please Download and print the Admission Processing Fee Slip from ".HtmlEncode() + "<a href=\"#step4\">Step 4</a>" + " for payment.".HtmlEncode();
					this.AddSuccessAlert(html, true);
					Redirect<Dashboard>();
					return;
				default:
					throw new NotImplementedEnumException(result);
			}
		}
	}
}