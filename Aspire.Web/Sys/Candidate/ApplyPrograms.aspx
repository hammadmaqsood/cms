﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ApplyPrograms.aspx.cs" Inherits="Aspire.Web.Sys.Candidate.ApplyPrograms" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="panel panel-default">
		<div class="panel-heading">
			<span class="panel-title">Academic Record</span>
		</div>
		<div class="panel-body">
			<div class="form-group" id="divbtnAddAcademicRecord" runat="server">
				<aspire:AspireButton ButtonType="Success" Glyphicon="plus" runat="server" ID="btnAddAcademicRecord" CausesValidation="false" Text="Add" OnClick="btnAddAcademicRecord_Click" />
			</div>
			<aspire:AspireGridView runat="server" Responsive="true" AutoGenerateColumns="false" ItemType="Aspire.Model.Entities.CandidateForeignAcademicRecord" ID="gvAcademicRecords" OnRowCommand="gvAcademicRecords_RowCommand">
				<Columns>
					<asp:TemplateField HeaderText="#">
						<ItemTemplate>
							<%# Container.DataItemIndex + 1 %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Degree">
						<ItemTemplate>
							<%#: Item.DegreeName %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Institute">
						<ItemTemplate>
							<%#: Item.Institute %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Subjects">
						<ItemTemplate>
							<%#: Item.Subjects %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Obtained Marks">
						<ItemTemplate>
							<%#: Item.ObtainedMarks %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Total Marks">
						<ItemTemplate>
							<%#: Item.TotalMarks %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Passing Year">
						<ItemTemplate>
							<%#: Item.PassingYear %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Documents">
						<ItemTemplate>
							<aspire:AspireLinkButton runat="server" ToolTip="Download Document" CausesValidation="false" Text="Download" CommandName="Download" EncryptedCommandArgument="<%# Item.CandidateForeignAcademicRecordID %>" />
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Action">
						<ItemTemplate>
							<aspire:AspireLinkButton ToolTip="Delete" runat="server" CausesValidation="false" ConfirmMessage="Are you sure you want to delete?" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument="<%# Item.CandidateForeignAcademicRecordID %>" />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</aspire:AspireGridView>
		</div>
		<aspire:AspireModal runat="server" ID="modalAcademicRecord" ModalSize="Large" Visible="false" HeaderText="Acacdemic Record">
			<BodyTemplate>
				<asp:UpdatePanel runat="server" ID="updatePanelAcademicRecord" UpdateMode="Conditional">
					<ContentTemplate>
						<aspire:AspireAlert ID="alertAcademicRecord" runat="server" />
						<div class="form-group">
							<aspire:AspireLabel runat="server" AssociatedControlID="tbDegreeName" Text="Degree:" />
							<aspire:AspireTextBox runat="server" ID="tbDegreeName" ValidationGroup="AcademicRecord" MaxLength="500" />
							<aspire:AspireStringValidator runat="server" ID="svtbDegreeName" AllowNull="false" ValidationGroup="AcademicRecord" ControlToValidate="tbDegreeName" RequiredErrorMessage="This field is required." />
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" AssociatedControlID="tbInstitute" Text="Institute/University:" />
							<aspire:AspireTextBox runat="server" ID="tbInstitute" ValidationGroup="AcademicRecord" MaxLength="500" />
							<aspire:AspireStringValidator runat="server" ID="svtbInstitute" AllowNull="false" ValidationGroup="AcademicRecord" ControlToValidate="tbInstitute" RequiredErrorMessage="This field is required." />
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" AssociatedControlID="tbSubjects" Text="Subjects" />
							<aspire:AspireTextBox runat="server" ID="tbSubjects" ValidationGroup="AcademicRecord" MaxLength="500" />
							<aspire:AspireStringValidator runat="server" ID="svtbSubjects" AllowNull="false" ValidationGroup="AcademicRecord" ControlToValidate="tbSubjects" RequiredErrorMessage="This field is required." />
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<aspire:AspireLabel runat="server" AssociatedControlID="tbObtainedMarks" Text="Obtained Marks:" />
									<aspire:AspireTextBox runat="server" ID="tbObtainedMarks" ValidationGroup="AcademicRecord" MaxLength="100" />
									<aspire:AspireDoubleValidator runat="server" ID="dvtbObtainedMarks" AllowNull="false" ValidationGroup="AcademicRecord" ControlToValidate="tbObtainedMarks" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Obtained Marks are not valid." InvalidRangeErrorMessage="Obtained Marks are not valid." MinValue="0" MaxValue="10000" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<aspire:AspireLabel runat="server" AssociatedControlID="tbTotalMarks" Text="Total Marks:" />
									<aspire:AspireTextBox runat="server" ID="tbTotalMarks" ValidationGroup="AcademicRecord" MaxLength="500" />
									<aspire:AspireDoubleValidator runat="server" ID="dvtbTotalMarks" AllowNull="false" ValidationGroup="AcademicRecord" ControlToValidate="tbTotalMarks" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Total Marks are not valid." InvalidRangeErrorMessage="Total Marks are not valid." MinValue="0" MaxValue="10000" />
									<aspire:AspireCompareValidator runat="server" ValidateEmptyText="false" ID="cvtbTotalMarks" ControlToValidate="tbTotalMarks" ControlToCompare="tbObtainedMarks" ValidationGroup="AcademicRecord" Operator="GreaterThanEqual" ErrorMessage="Obtained Marks should not exceed Total Marks." Type="Double" />
								</div>
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" AssociatedControlID="tbPassingYear" Text="Passing Year:" />
							<aspire:AspireTextBox runat="server" ID="tbPassingYear" ValidationGroup="AcademicRecord" MaxLength="500" />
							<aspire:AspireInt16Validator runat="server" ID="svtbPassingYear" AllowNull="false" ValidationGroup="AcademicRecord" ControlToValidate="tbPassingYear" InvalidNumberErrorMessage="Passing Year is not valid." InvalidRangeErrorMessage="Passing Year is not valid." RequiredErrorMessage="This field is required." />
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Upload Document:" AssociatedControlID="fileUploadAcademicRecord" />
							<div>
								<aspire:AspireFileUpload runat="server" ID="fileUploadAcademicRecord" AllowedFileTypes=".pdf,.jpg,.jpeg" MaxFileSize="20971520" MaxNumberOfFiles="1" />
								<aspire:AspireFileUploadValidator AllowNull="false" ValidationGroup="AcademicRecord" runat="server" AspireFileUploadID="fileUploadAcademicRecord" RequiredErrorMessage="File is missing." />
							</div>
							<span class="help-block">Only pdf, jpg or jpeg files are allowed. File size should not exceed 20MB.</span>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</BodyTemplate>
			<FooterTemplate>
				<asp:UpdatePanel runat="server" ID="updatePanelAcademicRecordButtons" UpdateMode="Conditional">
					<ContentTemplate>
						<aspire:AspireButton runat="server" ID="btnSaveAcademicRecord" Text="Save" ValidationGroup="AcademicRecord" OnClick="btnSaveAcademicRecord_Click" />
						<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
					</ContentTemplate>
				</asp:UpdatePanel>
			</FooterTemplate>
		</aspire:AspireModal>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<span class="panel-title">Additional Documents</span>
		</div>
		<ul class="list-group">
			<li class="list-group-item" id="divPassport" runat="server">
				<h4 class="list-group-item-heading">Passport</h4>
				<p class="list-group-item-text">
					<aspire:LinkButton ID="lbtnUploadPassport" runat="server" Text="Upload" CausesValidation="false" OnClick="lbtnUploadPassport_Click" />
					<aspire:LinkButton ID="lbtnDownloadPassport" runat="server" Text="Download" CausesValidation="false" OnClick="lbtnDownloadPassport_Click" />
					<aspire:LinkButton ID="lbtnDeletePassport" ConfirmMessage="Are you sure you want to delete?" runat="server" Text="Delete" CausesValidation="false" OnClick="lbtnDeletePassport_Click" />
				</p>
			</li>
			<li class="list-group-item" id="divStatementOfPurpose" runat="server">
				<h4 class="list-group-item-heading">Statement of Purpose</h4>
				<p class="list-group-item-text">
					<aspire:LinkButton ID="lbtnUploadStatementOfPurpose" runat="server" Text="Upload" CausesValidation="false" OnClick="lbtnUploadStatementOfPurpose_Click" />
					<aspire:LinkButton ID="lbtnDownloadStatementOfPurpose" runat="server" Text="Download" CausesValidation="false" OnClick="lbtnDownloadStatementOfPurpose_Click" />
					<aspire:LinkButton ID="lbtnDeleteStatementOfPurpose" ConfirmMessage="Are you sure you want to delete?" runat="server" Text="Delete" CausesValidation="false" OnClick="lbtnDeleteStatementOfPurpose_Click" />
				</p>
			</li>
			<li class="list-group-item" id="divEnglishLanguageCertificate" runat="server">
				<h4 class="list-group-item-heading">English Language Certificate</h4>
				<p class="list-group-item-text">
					<aspire:LinkButton ID="lbtnUploadEnglishLanguageCertificate" runat="server" Text="Upload" CausesValidation="false" OnClick="lbtnUploadEnglishLanguageCertificate_Click" />
					<aspire:LinkButton ID="lbtnDownloadEnglishLanguageCertificate" runat="server" Text="Download" CausesValidation="false" OnClick="lbtnDownloadEnglishLanguageCertificate_Click" />
					<aspire:LinkButton ID="lbtnDeleteEnglishLanguageCertificate" ConfirmMessage="Are you sure you want to delete?" runat="server" Text="Delete" CausesValidation="false" OnClick="lbtnDeleteEnglishLanguageCertificate_Click" />
				</p>
			</li>
			<li class="list-group-item" id="divResearchPlan" runat="server">
				<h4 class="list-group-item-heading">Research Plan (Only for PHD Candidates)</h4>
				<p class="list-group-item-text">
					<aspire:LinkButton ID="lbtnUploadResearchPlan" runat="server" Text="Upload" CausesValidation="false" OnClick="lbtnUploadResearchPlan_Click" />
					<aspire:LinkButton ID="lbtnDownloadResearchPlan" runat="server" Text="Download" CausesValidation="false" OnClick="lbtnDownloadResearchPlan_Click" />
					<aspire:LinkButton ID="lbtnDeleteResearchPlan" ConfirmMessage="Are you sure you want to delete?" runat="server" Text="Delete" CausesValidation="false" OnClick="lbtnDeleteResearchPlan_Click" />
				</p>
			</li>
		</ul>
	</div>
	<aspire:AspireModal runat="server" ID="modalAdditionalDocument" Visible="false" HeaderText="Upload Additional Documents">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelAdditionalDocument" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert ID="alertAdditionalDocument" runat="server" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Upload Document:" ID="lblDocumentTypeLabel" AssociatedControlID="fileUploadAcademicRecord" />
						<div>
							<aspire:AspireFileUpload runat="server" ID="fileUploadAdditionalDocument" AllowedFileTypes=".pdf" MaxFileSize="20971520" MaxNumberOfFiles="1" />
							<aspire:AspireFileUploadValidator AllowNull="false" ValidationGroup="AdditionalDocument" runat="server" AspireFileUploadID="fileUploadAdditionalDocument" RequiredErrorMessage="File is missing." />
						</div>
						<span class="help-block">Only pdf file is allowed. File size should not exceed 20MB.</span>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelAdditionalDocumentButtons" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSaveAdditionalDocument" Text="Save" ValidationGroup="AdditionalDocument" OnClick="btnSaveAdditionalDocument_Click" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>

	<div class="panel panel-default">
		<div class="panel-heading">
			<span class="panel-title">Programs</span>
		</div>
		<asp:Repeater runat="server" ItemType="Aspire.BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.AdmissionOpenProgram" ID="repeaterAdmissionOpenPrograms" OnItemCommand="repeaterAdmissionOpenPrograms_ItemCommand">
			<HeaderTemplate>
				<table class="table table-bordered table-condensed table-hover AspireGridView">
					<thead>
						<tr>
							<th>#</th>
							<th>Program</th>
							<th>Open Dates</th>
							<th>Sessions</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td>
						<%# Container.ItemIndex + 1 %>
					</td>
					<td>
						<strong>Institute:</strong> <%#: Item.InstituteName %>
						<br />
						<strong>Program:</strong> <%#: Item.ProgramName %> - <%#:Item.DurationEnum.ToFullName() %>
						<br />
						<strong>Eligibility Criteria:</strong> <%#: Item.EligibilityCriteriaStatement%>
					</td>
					<td>
						<%#: Item.AdmissionOpenDateForForeigners.ConcatenateDates(Item.AdmissionClosingDateForForeigners, "d") %>
					</td>
					<td>
						<%#: Item.ShiftEnumFlags.ToFullNames() %>
					</td>
					<td>
						<asp:Literal Text="Applied" Mode="Encode" runat="server" Visible="<%# Item.Applied %>" />
						<aspire:AspireLinkButton Text="Apply" Visible="<%# !Item.Applied %>" runat="server" CausesValidation="false" CommandName="Apply" EncryptedCommandArgument="<%# Item.AdmissionOpenProgramID %>" />
					</td>
				</tr>
			</ItemTemplate>
			<FooterTemplate>
				</tbody>
				</table>
			</FooterTemplate>
		</asp:Repeater>
	</div>

	<div class="panel panel-default" id="divAppliedPrograms" runat="server">
		<div class="panel-heading">
			<span class="panel-title">Applied Programs</span>
		</div>
		<asp:Repeater runat="server" ItemType="Aspire.BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.GetAllInformationForApplyProgramResult.CandidateAppliedProgram" ID="repeaterCandidateAppliedPrograms">
			<HeaderTemplate>
				<table class="table table-bordered table-condensed table-hover AspireGridView">
					<thead>
						<tr>
							<th>#</th>
							<th>Program</th>
							<th>Session</th>
							<th>Credit Transfer</th>
						</tr>
					</thead>
					<tbody>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td>
						<%# Container.ItemIndex + 1 %>
					</td>
					<td>
						<strong>Institute:</strong> <%#: Item.InstituteName %>
						<br />
						<strong>Program:</strong> <%#: Item.ProgramName %> - <%#: Item.DurationEnum.ToFullName() %>
						<br />
						<strong>Eligibility Criteria:</strong> <%#: Item.EligibilityCriteriaStatement%>
						<br />
						<span runat="server" visible='<%#Item.ResearchThemeAreaName.ToNullIfEmpty()!=null%>'><strong>Research Theme Area:</strong> <%#: Item.ResearchThemeAreaName%></span>
					</td>
					<td>
						<%#: Item.ShiftEnum.ToFullNames() %>
					</td>
					<td>
						<%#: Item.CreditTransfer.ToYesNo() %>
					</td>
				</tr>
			</ItemTemplate>
			<FooterTemplate>
				<td colspan="4" runat="server" visible="<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>">No record found.</td>
				</tbody>
				</table>
			</FooterTemplate>
		</asp:Repeater>
	</div>

	<aspire:AspireModal runat="server" ID="modalApplyProgram" Visible="false" ModalSize="Large" HeaderText="Apply Program">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanel1" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert ID="alertApplyProgram" runat="server" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" AssociatedControlID="lblInstituteName" Text="Institute:" />
						<div class="form-control-static">
							<aspire:Label runat="server" ID="lblInstituteName" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" AssociatedControlID="lblProgramName" Text="Program:" />
						<div class="form-control-static">
							<aspire:Label runat="server" ID="lblProgramName" />
						</div>
					</div>
					<div class="form-group" runat="server" id="dvResearchThemeArea">
						<aspire:AspireLabel runat="server" Text="Research Theme Area:" AssociatedControlID="ddlResearchThemeAreaID" />
						<aspire:AspireDropDownListWithOptionGroups runat="server" ValidationGroup="ApplyProgram" ID="ddlResearchThemeAreaID" CausesValidation="False" ToolTip="Selected Research Theme Area" />
						<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="ApplyProgram" ControlToValidate="ddlResearchThemeAreaID" ErrorMessage="This field is required." />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" AssociatedControlID="rblShift" Text="Session:" />
						<div class="form-control-static">
							<aspire:AspireRadioButtonList ID="rblShift" runat="server" ValidationGroup="ApplyProgram" RepeatDirection="Horizontal" RepeatLayout="Flow" />
						</div>
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblShift" ValidationGroup="ApplyProgram" ErrorMessage="This field is required." />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" AssociatedControlID="rblCreditTransfer" Text="Applying for Credit Hours transfer?" />
						<div class="form-control-static">
							<aspire:AspireRadioButtonList ID="rblCreditTransfer" runat="server" ValidationGroup="ApplyProgram" RepeatDirection="Horizontal" RepeatLayout="Flow" />
						</div>
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblCreditTransfer" ValidationGroup="ApplyProgram" ErrorMessage="This field is required." />
					</div>
					<div class="form-group" id="divTerms">
						<div class="text-justify">
							<h4>Undertaking</h4>
							<ul>
								<li>I understand that submitting incomplete, false or misleading information is considered as sufficient grounds for denial of admission and dismissal from Bahria University at any time.</li>
								<li>If my admission is accepted, I intend to provide Bahria University complete documents within the given time frame by the Admissions Directorate. Failing to do so will give Bahria University complete right to cancel my admission.</li>
								<li>I am aware of the eligibility criteria set forth by Bahria University for the program I have applied for.</li>
								<li>I understand that Bahria University is allowed to cancel my admission in case I do not meet the minimum eligibility criteria at any stage of my admission.</li>
								<li>It is my responsibility to inform the university about my in-eligibility as soon as possible.</li>
								<li>In case of Provisional Admission, I fully understand that I have to meet all conditions set in the offer letter. In case of non-compliance with the condition(s) of the provisional offer and admission requirements, Bahria University reserves full rights to cancel my admission.</li>
								<li>I confirm that I hold foreign nationality by birth.</li>
							</ul>
							<div class="checkbox">
								<aspire:AspireCheckBox ValidationGroup="ApplyProgram" ID="cbTerms" runat="server" Text="I certify that all the information I have given is complete and accurate to the best of my knowledge and belief." />
								<aspire:AspireCustomValidator ID="cvcbTerms" runat="server" ValidationGroup="ApplyProgram" ClientValidationFunction="validateTerms" OnServerValidate="cvcbTerms_ServerValidate" />
								<script type="text/javascript">
									function validateTerms(sender, e) {
										var cb = $("#<%= this.cbTerms.ClientID %>");
										e.IsValid = $(cb).is(":checked");
										$('#divTerms').toggleClass("has-error", !e.IsValid);
									}
								</script>
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelApplyProgram" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnApplyProgram" Text="Submit" ValidationGroup="ApplyProgram" OnClick="btnApplyProgram_Click" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
