﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AcademicRecord.ascx.cs" Inherits="Aspire.Web.Sys.Candidate.AcademicRecord" %>

<asp:UpdatePanel runat="server" ID="updatePanelMain" UpdateMode="Conditional">
	<ContentTemplate>
		<div class="row academicRecords">
			<div class="col-md-6">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Degree:" AssociatedControlID="ddlDegreeType" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="ApplyProgram" ID="ddlDegreeType" CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="ddlDegreeType_SelectedIndexChanged" />
				</div>
			</div>
			<div class="col-md-6" id="divExaminationSystem" runat="server">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Examination System:" AssociatedControlID="rblExaminationSystem" />
					<div class="form-control-static">
						<aspire:AspireRadioButtonList runat="server" ID="rblExaminationSystem" CausesValidation="false" ValidationGroup="ApplyProgram" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="True" OnSelectedIndexChanged="rblExaminationSystem_SelectedIndexChanged" />
					</div>
				</div>
			</div>
			<div class="col-md-6" id="divTotalCGPA" runat="server">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Total CGPA:" AssociatedControlID="ddlTotalCGPA" />
					<aspire:AspireDropDownList runat="server" ID="ddlTotalCGPA" CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="ddlTotalCGPA_SelectedIndexChanged" />
				</div>
			</div>
			<div class="col-md-6" id="divObtainedCGPA" runat="server">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Obtained CGPA:" AssociatedControlID="tbObtainedCGPA" />
					<aspire:AspireTextBox runat="server" ValidationGroup="ApplyProgram" ID="tbObtainedCGPA" MaxLength="8" />
					<aspire:AspireDoubleValidator MinValue="0" MaxValue="5" ID="dvtbObtainedCGPA" runat="server" AllowNull="False" ValidationGroup="ApplyProgram" ControlToValidate="tbObtainedCGPA" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Number must be from {min} to {max}." />
				</div>
			</div>
			<div class="col-md-6" id="divObtainedMarks" runat="server">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Obtained Marks:" AssociatedControlID="tbObtainedMarks" />
					<aspire:AspireTextBox runat="server" ValidationGroup="ApplyProgram" CssClass="tbObtainedMarks" ID="tbObtainedMarks" MaxLength="8" />
					<aspire:AspireUInt16Validator ID="ivtbObtainedMarks" runat="server" AllowNull="False" ValidationGroup="ApplyProgram" ControlToValidate="tbObtainedMarks" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Number must be from {min} to {max}." />
				</div>
			</div>
			<div class="col-md-6" id="divTotalMarks" runat="server">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Total Marks:" AssociatedControlID="tbTotalMarks" />
					<aspire:AspireTextBox runat="server" ValidationGroup="ApplyProgram" CssClass="tbTotalMarks" ID="tbTotalMarks" MaxLength="8" />
					<aspire:AspireUInt16Validator ID="ivtbTotalMarks" runat="server" AllowNull="False" ValidationGroup="ApplyProgram" ControlToValidate="tbTotalMarks" RequiredErrorMessage="This field is required." InvalidNumberInvalidRangeErrorMessage="Number must be from {min} to {max}." />
					<aspire:AspireCompareValidator ValidateEmptyText="False" runat="server" ID="cvtbTotalMarks" ErrorMessage="Obtained Marks must be less than or equal to Total Marks." ControlToValidate="tbTotalMarks" ControlToCompare="tbObtainedMarks" Type="Integer" ValidationGroup="ApplyProgram" Operator="GreaterThanEqual" />
				</div>
			</div>
			<div class="col-md-6" id="divPercentage" runat="server">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Percentage:" AssociatedControlID="tbPercentage" />
					<aspire:AspireTextBox runat="server" ValidationGroup="ApplyProgram" CssClass="tbPercentage" ID="tbPercentage" ReadOnly="True" MaxLength="20" />
					<aspire:AspireCustomValidator ValidateEmptyText="True" runat="server" ID="cvtbPercentage" ClientValidationFunction="validatePercentage" ValidationGroup="ApplyProgram" OnServerValidate="cvtbPercentage_OnServerValidate" />
				</div>
			</div>
			<div class="col-md-6" runat="server" id="divddlSubjects">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Subjects:" AssociatedControlID="ddlSubjects" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="ApplyProgram" ID="ddlSubjects" />
				</div>
			</div>
			<div class="col-md-6" id="divtbSubjects" runat="server">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Subjects:" AssociatedControlID="tbSubjects" />
					<aspire:AspireTextBox runat="server" ValidationGroup="ApplyProgram" ID="tbSubjects" MaxLength="500" />
					<aspire:AspireStringValidator ID="svtbSubjects" runat="server" AllowNull="False" ControlToValidate="tbSubjects" RequiredErrorMessage="This field is required." ValidationGroup="ApplyProgram" />
				</div>
			</div>
			<div class="col-md-6" id="divtbInstitute" runat="server">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="tbInstitute" />
					<aspire:AspireTextBox runat="server" ValidationGroup="ApplyProgram" ID="tbInstitute" MaxLength="500" />
					<aspire:AspireStringValidator ID="svtbInstitute" runat="server" AllowNull="False" ControlToValidate="tbInstitute" RequiredErrorMessage="This field is required." ValidationGroup="ApplyProgram" />
				</div>
			</div>
			<div class="col-md-6" id="divtbBoardUniversity" runat="server">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Board/University:" AssociatedControlID="tbBoardUniversity" />
					<aspire:AspireTextBox runat="server" ValidationGroup="ApplyProgram" ID="tbBoardUniversity" MaxLength="500" />
					<aspire:AspireStringValidator ID="svtbBoardUniversity" runat="server" AllowNull="False" ControlToValidate="tbBoardUniversity" RequiredErrorMessage="This field is required." ValidationGroup="ApplyProgram" />
				</div>
			</div>
			<div class="col-md-6" id="divtbPassingYear" runat="server">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Passing Year:" AssociatedControlID="tbPassingYear" />
					<aspire:AspireTextBox runat="server" ValidationGroup="ApplyProgram" ID="tbPassingYear" MaxLength="4" data-inputmask="'alias': 'short'" />
					<aspire:AspireInt16Validator ID="ivtbPassingYear" runat="server" AllowNull="False" ValidationGroup="ApplyProgram" ControlToValidate="tbPassingYear" RequiredErrorMessage="This field is required." InvalidNumberInvalidRangeErrorMessage="Year must be from {min} to {max}." />
				</div>
			</div>
		</div>
	</ContentTemplate>
</asp:UpdatePanel>
