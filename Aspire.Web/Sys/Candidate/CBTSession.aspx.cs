﻿using Aspire.BL.Core.Admissions.Candidate;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Candidate
{
	[AspirePage("E3A32144-7841-4C8E-B638-411CFD3D3EAE", UserTypes.Candidate, "~/Sys/Candidate/CBTSession.aspx", "CBT Session", true, AspireModules.None)]
	public partial class CBTSession : CandidatePage
	{
		public override PageIcon PageIcon => FontAwesomeIcons.solid_calendar.GetIcon();
		public override string PageTitle => "CBT Session";

		public int? CandidateAppliedProgramID => this.Request.GetParameterValue<int>(nameof(this.CandidateAppliedProgramID));

		public static void Redirect(int candidateAppliedProgramID)
		{
			Redirect<CBTSession>("CandidateAppliedProgramID", candidateAppliedProgramID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.CandidateAppliedProgramID == null || this.CandidateIdentity.ForeignStudent)
				{
					Redirect<Dashboard>();
					return;
				}

				var result = Aspire.BL.Core.Admissions.Candidate.ApplyForProgram.GetCBTSessions(this.CandidateAppliedProgramID.Value, this.CandidateIdentity.LoginSessionGuid);
				if (result == null)
				{
					Redirect<Dashboard>();
					return;
				}
				this.lblApplicationNo.Text = result.ApplicationNo.ToString();
				this.lblSemester.Text = result.SemesterID.ToSemesterString();
				this.lblInstituteShortName.Text = result.InstituteShortName;
				this.lblProgramShortName.Text = result.ProgramShortName;

				if (result.CBTSessions == null || !result.CBTSessions.Any())
				{
					this.panelNoSessionAvailable.Visible = true;
					this.repeaterCBTSessions.Visible = false;
				}
				else
				{
					this.panelNoSessionAvailable.Visible = false;
					this.repeaterCBTSessions.Visible = true;
					this.repeaterCBTSessions.DataBind(result.CBTSessions);
				}
			}
		}

		protected void repeaterCBTSessions_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "ConfirmSeat":
					var cbtSessionID = e.DecryptedCommandArgumentToInt();
					var status = Aspire.BL.Core.Admissions.Candidate.ApplyForProgram.ConfirmCBTSession(this.CandidateAppliedProgramID.Value, cbtSessionID, this.CandidateIdentity.LoginSessionGuid);
					switch (status)
					{
						case ApplyForProgram.ConfirmCBTSessionStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							Redirect<Dashboard>();
							return;
						case ApplyForProgram.ConfirmCBTSessionStatuses.SessionFull:
							this.AddErrorAlert("Selected Session has been full. Please try another one.");
							Redirect(this.CandidateAppliedProgramID.Value);
							return;
						case ApplyForProgram.ConfirmCBTSessionStatuses.SessionNotAllowed:
							this.AddErrorAlert("Selected Session is not allowed. Please try another one.");
							Redirect(this.CandidateAppliedProgramID.Value);
							return;
						case ApplyForProgram.ConfirmCBTSessionStatuses.Confirmed:
						case ApplyForProgram.ConfirmCBTSessionStatuses.AlreadyConfirmed:
							Sys.Candidate.Reports.AdmitSlipsReport.Redirect(this.CandidateAppliedProgramID.Value);
							return;
						case ApplyForProgram.ConfirmCBTSessionStatuses.AlreadyAssignedToOtherAppliedProgram:
							this.AddErrorAlert("Selected Session is already occupied for your another Applied Program. Please try another one.");
							Redirect(this.CandidateAppliedProgramID.Value);
							return;
						default:
							throw new NotImplementedEnumException(status);
					}
			}
		}
	}
}