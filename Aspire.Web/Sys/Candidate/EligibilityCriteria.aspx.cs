﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Candidate
{
	[AspirePage("031BDA73-B19A-4F45-BC07-E8FEAD2D214E", UserTypes.Candidate, "~/Sys/Candidate/EligibilityCriteria.aspx", "Eligibility Criteria", true, AspireModules.Admissions)]
	public partial class EligibilityCriteria : CandidatePage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_info_circle);
		public override string PageTitle => "Eligibility Criteria";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var eligibilityCriteriaStatements = BL.Core.Admissions.Candidate.Common.GetEligibilityCriteriaStatements(this.CandidateIdentity.LoginSessionGuid);

				this.rptEligibilityCriteria.DataBind(eligibilityCriteriaStatements.OrderBy(ec => ec.InstituteID));
			}
		}
	}
}