﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="Profile.aspx.cs" Inherits="Aspire.Web.Sys.Candidate.Profile" %>

<%@ Register Src="~/Sys/Common/Admissions/CandidateProfile.ascx" TagPrefix="uc1" TagName="CandidateProfile" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc1:CandidateProfile runat="server" ID="CandidateProfile" />
</asp:Content>
