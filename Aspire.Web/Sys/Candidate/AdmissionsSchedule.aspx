﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="AdmissionsSchedule.aspx.cs" Inherits="Aspire.Web.Sys.Candidate.AdmissionsSchedule" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:Repeater runat="server" ID="rptAdmissionOpen" ItemType="Aspire.BL.Core.Admissions.Candidate.Common.OpenProgram">
		<HeaderTemplate>
			<div class="table-responsive">
				<table class="table table-hover table-bordered">
		</HeaderTemplate>
		<ItemTemplate>
			<thead runat="server" visible="<%# !this.CandidateIdentity.ForeignStudent || Item.Programs.Any(p=> p.AdmissionOpenDateForForeigners != null) %>">
				<tr>
					<th colspan="7">
						<h4><%#: Item.InstituteShortName %></h4>
					</th>
				</tr>
				<tr>
					<th>#</th>
					<th>Program</th>
					<th>Sessions</th>
					<th>Open Date(s)</th>
					<th runat="server" visible="<%# !this.CandidateIdentity.ForeignStudent %>">Entry Test Date</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody runat="server" visible="<%# !this.CandidateIdentity.ForeignStudent || Item.Programs.Any(p=> p.AdmissionOpenDateForForeigners != null) %>">
				<asp:Repeater runat="server" DataSource='<%# Item.Programs %>' ItemType="Aspire.BL.Core.Admissions.Candidate.Common.OpenProgram.Program">
					<ItemTemplate>
						<tr class='<%# Item.Applied ? "success" : "" %>' runat="server" visible="<%# !this.CandidateIdentity.ForeignStudent || Item.AdmissionOpenDateForForeigners != null %>">
							<td><%#: Container.ItemIndex + 1 %></td>
							<td title="<%# Item.ProgramName %>"><%#: Item.ProgramShortName %></td>
							<td><%#: Item.ShiftsEnum.ToFullNames(", ") %></td>
							<td runat="server" visible="<%# !this.CandidateIdentity.ForeignStudent %>"><%#: Item.AdmissionOpenDate.ConcatenateDates(Item.AdmissionClosingDate, "dd-MMM-yyyy", " to ") %></td>
							<td runat="server" visible="<%# this.CandidateIdentity.ForeignStudent %>"><%#: Item.AdmissionOpenDateForForeigners.ConcatenateDates(Item.AdmissionClosingDateForForeigners, "dd-MMM-yyyy", " to ") %></td>
							<td runat="server" visible="<%# !this.CandidateIdentity.ForeignStudent %>"><%#: $"Starting onward {Item.EntryTestStartDate:dd-MMM-yyyy}".ToNAIfNullOrEmpty() %></td>
							<td><%#: Item.Applied ? "Applied" : "Not Applied" %></td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
			</tbody>
		</ItemTemplate>
		<FooterTemplate>
			</table>
			</div>
		</FooterTemplate>
	</asp:Repeater>
</asp:Content>
