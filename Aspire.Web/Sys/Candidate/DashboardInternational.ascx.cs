﻿using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Candidate
{
	public partial class DashboardInternational : System.Web.UI.UserControl
	{
		private new Dashboard Page => (Dashboard)base.Page;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.Page.CandidateIdentity.ForeignStudent)
				{
					var result = BL.Core.Admissions.Candidate.ApplyForProgram.GetAppliedProgramsStatus(this.Page.CandidateIdentity.LoginSessionGuid);
					this.lblProfileInformationStatus.Text = result.CandidateStatusEnum == Model.Entities.Candidate.Statuses.Completed ? "Completed" : "Not Completed";
					this.repeaterStep4ConfirmationOfProvisionalAdmission.DataBind(result.CandidateAppliedProgramStatuses);
					this.repeaterStep5AdmissionFee.DataBind(result.CandidateAppliedProgramStatuses);
					this.Visible = true;
				}
				else
				{
					this.Visible = false;
					return;
				}
			}
		}
	}
}