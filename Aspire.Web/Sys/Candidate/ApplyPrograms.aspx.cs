﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Candidate
{
	[AspirePage("F3473205-C577-40D6-8E2B-2BC5E522742B", UserTypes.Candidate, "~/Sys/Candidate/ApplyPrograms.aspx", "Apply For Program", true, AspireModules.None)]
	public partial class ApplyPrograms : CandidatePage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_edit);
		public override string PageTitle => "Apply For Programs";

		private void RefreshPage()
		{
			Redirect<ApplyPrograms>();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (this.CandidateIdentity.ForeignStudent == false)
			{
				Redirect<ApplyProgram>();
				return;
			}
			if (!this.IsPostBack)
			{
				var result = BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.GetAllInformationForApplyProgram(this.CandidateIdentity.LoginSessionGuid);
				switch (result?.Status)
				{
					case null:
						this.AddNoRecordFoundAlert();
						Redirect<Dashboard>();
						return;
					case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.GetAllInformationForApplyProgramResult.Statuses.NotForeignStudent:
						this.AddErrorAlert("Action is not valid for this Account.");
						Redirect<Dashboard>();
						return;
					case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.GetAllInformationForApplyProgramResult.Statuses.ProfileInformationNotCompleted:
						switch (result.CandidateStatus)
						{
							case Model.Entities.Candidate.Statuses.InformationProvided:
								this.AddErrorAlert("Profile Picture is not provided.");
								break;
							case Model.Entities.Candidate.Statuses.Blank:
							case Model.Entities.Candidate.Statuses.PictureUploaded:
								this.AddErrorAlert("Profile information is not provided.");
								break;
							case Model.Entities.Candidate.Statuses.Completed:
								throw new InvalidOperationException();
							default:
								throw new NotImplementedEnumException(result.CandidateStatus);
						}
						Redirect<Profile>();
						return;
					case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.GetAllInformationForApplyProgramResult.Statuses.Success:
						this.gvAcademicRecords.DataBind(result.CandidateForeignAcademicRecords);
						this.divbtnAddAcademicRecord.Visible = result.EditingAllowed;

						var passport = result.CandidateForeignDocuments.SingleOrDefault(cfd => cfd.DocumentTypeEnum == Model.Entities.CandidateForeignDocument.DocumentTypes.Passport);
						this.lbtnDeletePassport.Visible = passport != null && result.EditingAllowed;
						this.lbtnDownloadPassport.Visible = passport != null;
						this.lbtnUploadPassport.Visible = passport == null && result.EditingAllowed;
						this.lbtnDownloadPassport.EncryptedCommandArgument = passport?.CandidateForeignDocumentID.ToString();
						this.lbtnDeletePassport.EncryptedCommandArgument = passport?.CandidateForeignDocumentID.ToString();
						this.divPassport.Attributes.Add("class", "list-group-item " + (passport != null ? "list-group-item-success" : "list-group-item-danger"));

						var statementOfPurpose = result.CandidateForeignDocuments.SingleOrDefault(cfd => cfd.DocumentTypeEnum == Model.Entities.CandidateForeignDocument.DocumentTypes.StatementOfPurpose);
						this.lbtnDeleteStatementOfPurpose.Visible = statementOfPurpose != null && result.EditingAllowed;
						this.lbtnDownloadStatementOfPurpose.Visible = statementOfPurpose != null;
						this.lbtnUploadStatementOfPurpose.Visible = statementOfPurpose == null && result.EditingAllowed;
						this.lbtnDownloadStatementOfPurpose.EncryptedCommandArgument = statementOfPurpose?.CandidateForeignDocumentID.ToString();
						this.lbtnDeleteStatementOfPurpose.EncryptedCommandArgument = statementOfPurpose?.CandidateForeignDocumentID.ToString();
						this.divStatementOfPurpose.Attributes.Add("class", "list-group-item " + (statementOfPurpose != null ? "list-group-item-success" : "list-group-item-danger"));

						var englishLanguageCertificate = result.CandidateForeignDocuments.SingleOrDefault(cfd => cfd.DocumentTypeEnum == Model.Entities.CandidateForeignDocument.DocumentTypes.EnglishLanguageCertificate);
						this.lbtnDeleteEnglishLanguageCertificate.Visible = englishLanguageCertificate != null && result.EditingAllowed;
						this.lbtnDownloadEnglishLanguageCertificate.Visible = englishLanguageCertificate != null;
						this.lbtnUploadEnglishLanguageCertificate.Visible = englishLanguageCertificate == null && result.EditingAllowed;
						this.lbtnDownloadEnglishLanguageCertificate.EncryptedCommandArgument = englishLanguageCertificate?.CandidateForeignDocumentID.ToString();
						this.lbtnDeleteEnglishLanguageCertificate.EncryptedCommandArgument = englishLanguageCertificate?.CandidateForeignDocumentID.ToString();
						this.divEnglishLanguageCertificate.Attributes.Add("class", "list-group-item " + (englishLanguageCertificate != null ? "list-group-item-success" : "list-group-item-danger"));

						var researchPlan = result.CandidateForeignDocuments.SingleOrDefault(cfd => cfd.DocumentTypeEnum == Model.Entities.CandidateForeignDocument.DocumentTypes.ResearchPlan);
						this.lbtnDeleteResearchPlan.Visible = researchPlan != null && result.EditingAllowed;
						this.lbtnDownloadResearchPlan.Visible = researchPlan != null;
						this.lbtnUploadResearchPlan.Visible = researchPlan == null;
						this.lbtnDownloadResearchPlan.EncryptedCommandArgument = researchPlan?.CandidateForeignDocumentID.ToString();
						this.lbtnDeleteResearchPlan.EncryptedCommandArgument = researchPlan?.CandidateForeignDocumentID.ToString();
						this.divResearchPlan.Attributes.Add("class", "list-group-item" + (researchPlan != null ? " list-group-item-success" : ""));

						this.repeaterAdmissionOpenPrograms.DataBind(result.AdmissionOpenPrograms);
						this.repeaterCandidateAppliedPrograms.DataBind(result.CandidateAppliedPrograms);
						this.divAppliedPrograms.Visible = result.CandidateAppliedPrograms?.Any() == true;
						break;
					default:
						throw new NotImplementedEnumException(result.Status);
				}
			}
		}

		protected void btnAddAcademicRecord_Click(object sender, EventArgs e)
		{
			this.ShowAcademicRecordModal(null);
		}

		private void ShowAcademicRecordModal(Guid? academicRecordCandidateForeignAcademicRecordID)
		{
			this.svtbPassingYear.MinValue = 1950;
			this.svtbPassingYear.MaxValue = (short)DateTime.Now.Year;
			if (academicRecordCandidateForeignAcademicRecordID == null)
			{
				this.modalAcademicRecord.HeaderText = "Add Academic Record";
				this.tbDegreeName.Text = null;
				this.tbInstitute.Text = null;
				this.tbSubjects.Text = null;
				this.tbObtainedMarks.Text = null;
				this.tbTotalMarks.Text = null;
				this.tbPassingYear.Text = null;
				this.fileUploadAcademicRecord.ClearFiles();
			}
			else
			{
				throw new NotImplementedException();
			}
			this.modalAcademicRecord.Show();
			this.modalAcademicRecord.Visible = true;
		}

		protected void btnSaveAcademicRecord_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var degreeName = this.tbDegreeName.Text;
			var institute = this.tbInstitute.Text;
			var subjects = this.tbSubjects.Text;
			var obtainedMarks = this.tbObtainedMarks.Text;
			var totalMarks = this.tbTotalMarks.Text;
			var passingYear = this.tbPassingYear.Text.ToShort();
			var instanceGuid = this.fileUploadAcademicRecord.InstanceGuid;
			var temporarySystemFileID = this.fileUploadAcademicRecord.UploadedFiles.Single().FileID.ToGuid();
			var fileName = this.fileUploadAcademicRecord.UploadedFiles.Single().FileName;
			var result = BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.AddCandidateForeignAcademicRecord(degreeName, institute, subjects, obtainedMarks, totalMarks, passingYear, instanceGuid, temporarySystemFileID, fileName, this.CandidateIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.AddCandidateForeignAcademicRecordStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.AddCandidateForeignAcademicRecordStatuses.AccountNotValid:
					this.AddErrorAlert("Account is not valid.");
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.AddCandidateForeignAcademicRecordStatuses.ErrorRecordLockedDueToProgramApplied:
					this.AddErrorAlert("Academic Record cannot be added once Program is applied.");
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.AddCandidateForeignAcademicRecordStatuses.ErrorProfileInformationIsNotComplete:
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.AddCandidateForeignAcademicRecordStatuses.Success:
					this.AddSuccessMessageHasBeenAdded("Academic Record");
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.AddCandidateForeignAcademicRecordStatuses.ErrorTemporaryFileNotFound:
					this.AddErrorAlert("File not found.");
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.AddCandidateForeignAcademicRecordStatuses.ErrorDocumentIsNotValid:
					this.alertAcademicRecord.AddErrorAlert("Document is not valid.");
					this.updatePanelAcademicRecord.Update();
					return;
				default:
					throw new NotImplementedEnumException(result);
			}
		}

		protected void gvAcademicRecords_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Download":
					var candidateForeignAcademicRecordID = e.DecryptedCommandArgument().ToGuid();
					var result = BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.DownloadCandidateForeignAcademicRecord(candidateForeignAcademicRecordID, this.CandidateIdentity.LoginSessionGuid);
					if (result == null)
					{
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						return;
					}
					this.Response.Clear();
					this.Response.AddHeader("Content-Disposition", $"attachment; filename=\"{result.Value.systemFile.FileName}\"");
					this.Response.AddHeader("Content-Length", result.Value.systemFile.FileSize.ToString());
					this.Response.ContentType = result.Value.systemFile.MimeType;
					this.Response.BinaryWrite(result.Value.fileBytes);
					return;
				case "Delete":
					candidateForeignAcademicRecordID = e.DecryptedCommandArgument().ToGuid();
					e.Handled = true;
					var deleteResult = BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.DeleteCandidateForeignAcademicRecord(candidateForeignAcademicRecordID, this.CandidateIdentity.LoginSessionGuid);
					switch (deleteResult)
					{
						case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.DeleteCandidateForeignAcademicRecordStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.RefreshPage();
							return;
						case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.DeleteCandidateForeignAcademicRecordStatuses.AccountNotValid:
							this.AddErrorAlert("Account is not valid.");
							this.RefreshPage();
							return;
						case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.DeleteCandidateForeignAcademicRecordStatuses.ErrorRecordLockedDueToProgramApplied:
							this.AddErrorAlert("Academic Record cannot be deleted once Program is applied.");
							this.RefreshPage();
							return;
						case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.DeleteCandidateForeignAcademicRecordStatuses.ErrorProfileInformationIsNotComplete:
							this.RefreshPage();
							return;
						case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.DeleteCandidateForeignAcademicRecordStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Academic Record");
							this.RefreshPage();
							return;
						default:
							throw new NotImplementedEnumException(deleteResult);
					}
			}
		}

		protected void lbtnDownloadPassport_Click(object sender, EventArgs e)
		{
			this.DownloadAdditionalDocument(this.lbtnDownloadPassport.EncryptedCommandArgument.ToGuid());
		}

		protected void lbtnDeletePassport_Click(object sender, EventArgs e)
		{
			this.DeleteAdditionalDocument(this.lbtnDeletePassport.EncryptedCommandArgument.ToGuid());
		}

		protected void lbtnUploadPassport_Click(object sender, EventArgs e)
		{
			this.ShowAdditionalDocumentUploadModal(Model.Entities.CandidateForeignDocument.DocumentTypes.Passport);
		}

		private Model.Entities.CandidateForeignDocument.DocumentTypes? DocumentType
		{
			get => (Model.Entities.CandidateForeignDocument.DocumentTypes?)this.ViewState[nameof(this.DocumentType)];
			set => this.ViewState[nameof(this.DocumentType)] = value;
		}

		private void ShowAdditionalDocumentUploadModal(Model.Entities.CandidateForeignDocument.DocumentTypes documentType)
		{
			this.DocumentType = documentType;
			this.lblDocumentTypeLabel.Text = documentType.GetFullName();
			this.fileUploadAdditionalDocument.ClearFiles();
			this.modalAdditionalDocument.Show();
			this.modalAdditionalDocument.Visible = true;
		}

		protected void btnSaveAdditionalDocument_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var file = this.fileUploadAdditionalDocument.UploadedFiles.Single();
			var uploadResult = BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.AddCandidateForeignDocument(this.DocumentType ?? throw new InvalidOperationException(), file.FileName, file.Instance, file.FileID.ToGuid(), this.CandidateIdentity.LoginSessionGuid);
			switch (uploadResult)
			{
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.AddCandidateForeignDocumentStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.AddCandidateForeignDocumentStatuses.AccountNotValid:
					this.AddErrorAlert("Account is not valid.");
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.AddCandidateForeignDocumentStatuses.ErrorRecordLockedDueToProgramApplied:
					this.AddErrorAlert("Record cannot be added once Program is applied.");
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.AddCandidateForeignDocumentStatuses.ErrorProfileInformationIsNotComplete:
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.AddCandidateForeignDocumentStatuses.Success:
					this.AddSuccessMessageHasBeenAdded("Additional Document");
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.AddCandidateForeignDocumentStatuses.ErrorTemporaryFileNotFound:
					this.AddErrorAlert("File not found.");
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.AddCandidateForeignDocumentStatuses.ErrorDocumentIsNotValid:
					this.alertAdditionalDocument.AddErrorAlert("Document is not valid.");
					this.updatePanelAdditionalDocument.Update();
					return;
				default:
					throw new NotImplementedEnumException(uploadResult);
			}
		}

		private void DeleteAdditionalDocument(Guid candidateForeignDocumentID)
		{
			var deleteResult = BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.DeleteCandidateForeignDocument(candidateForeignDocumentID, this.CandidateIdentity.LoginSessionGuid);
			switch (deleteResult)
			{
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.DeleteCandidateForeignDocumentStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.DeleteCandidateForeignDocumentStatuses.AccountNotValid:
					this.AddErrorAlert("Account is not valid.");
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.DeleteCandidateForeignDocumentStatuses.ErrorRecordLockedDueToProgramApplied:
					this.AddErrorAlert("Record cannot be deleted once Program is applied.");
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.DeleteCandidateForeignDocumentStatuses.ErrorProfileInformationIsNotComplete:
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.DeleteCandidateForeignDocumentStatuses.Success:
					this.AddSuccessMessageHasBeenDeleted("Additional Document");
					this.RefreshPage();
					return;
				default:
					throw new NotImplementedEnumException(deleteResult);
			}
		}

		private void DownloadAdditionalDocument(Guid candidateForeignDocumentID)
		{
			var result = BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.DownloadCandidateForeignDocument(candidateForeignDocumentID, this.CandidateIdentity.LoginSessionGuid);
			if (result == null)
			{
				this.AddNoRecordFoundAlert();
				this.RefreshPage();
				return;
			}
			this.Response.Clear();
			this.Response.AddHeader("Content-Disposition", $"attachment; filename=\"{result.Value.systemFile.FileName}\"");
			this.Response.AddHeader("Content-Length", result.Value.systemFile.FileSize.ToString());
			this.Response.ContentType = result.Value.systemFile.MimeType;
			this.Response.BinaryWrite(result.Value.fileBytes);
		}

		protected void lbtnUploadStatementOfPurpose_Click(object sender, EventArgs e)
		{
			this.ShowAdditionalDocumentUploadModal(Model.Entities.CandidateForeignDocument.DocumentTypes.StatementOfPurpose);
		}

		protected void lbtnDownloadStatementOfPurpose_Click(object sender, EventArgs e)
		{
			this.DownloadAdditionalDocument(this.lbtnDeleteStatementOfPurpose.EncryptedCommandArgument.ToGuid());
		}

		protected void lbtnDeleteStatementOfPurpose_Click(object sender, EventArgs e)
		{
			this.DeleteAdditionalDocument(this.lbtnDeleteStatementOfPurpose.EncryptedCommandArgument.ToGuid());
		}

		protected void lbtnUploadEnglishLanguageCertificate_Click(object sender, EventArgs e)
		{
			this.ShowAdditionalDocumentUploadModal(Model.Entities.CandidateForeignDocument.DocumentTypes.EnglishLanguageCertificate);
		}

		protected void lbtnDownloadEnglishLanguageCertificate_Click(object sender, EventArgs e)
		{
			this.DownloadAdditionalDocument(this.lbtnDeleteEnglishLanguageCertificate.EncryptedCommandArgument.ToGuid());
		}

		protected void lbtnDeleteEnglishLanguageCertificate_Click(object sender, EventArgs e)
		{
			this.DeleteAdditionalDocument(this.lbtnDeleteEnglishLanguageCertificate.EncryptedCommandArgument.ToGuid());
		}

		protected void lbtnUploadResearchPlan_Click(object sender, EventArgs e)
		{
			this.ShowAdditionalDocumentUploadModal(Model.Entities.CandidateForeignDocument.DocumentTypes.ResearchPlan);
		}

		protected void lbtnDownloadResearchPlan_Click(object sender, EventArgs e)
		{
			this.DownloadAdditionalDocument(this.lbtnDeleteResearchPlan.EncryptedCommandArgument.ToGuid());
		}

		protected void lbtnDeleteResearchPlan_Click(object sender, EventArgs e)
		{
			this.DeleteAdditionalDocument(this.lbtnDeleteResearchPlan.EncryptedCommandArgument.ToGuid());
		}

		protected void repeaterAdmissionOpenPrograms_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Apply":
					var admissionOpenProgramID = e.DecryptedCommandArgumentToInt();
					var (admissionOpenProgram, researchThemeAreas) = BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.GetAdmissionOpenProgram(admissionOpenProgramID, this.CandidateIdentity.LoginSessionGuid);
					if (admissionOpenProgram == null)
					{
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						return;
					}
					if (admissionOpenProgram.DegreeLevelEnum == DegreeLevels.Doctorate)
					{
						this.ddlResearchThemeAreaID.Items.Clear();
						this.dvResearchThemeArea.Visible = true;
						this.ddlResearchThemeAreaID.Items.Add(CommonListItems.Select);
						foreach (var rtArea in researchThemeAreas)
							this.ddlResearchThemeAreaID.AddItem(rtArea.AreaName, rtArea.ResearchThemeAreaID.ToString(), rtArea.ResearchThemeName);
					}
					else
					{
						this.dvResearchThemeArea.Visible = false;
						this.ddlResearchThemeAreaID.Items.Clear();
					}
					this.ViewState["Choice1AdmissionOpenProgramID"] = admissionOpenProgram.AdmissionOpenProgramID;
					this.lblInstituteName.Text = admissionOpenProgram.InstituteName;
					this.lblProgramName.Text = admissionOpenProgram.ProgramName;
					this.rblShift.FillShifts(admissionOpenProgram.ShiftEnumFlags.GetFlags());
					this.rblCreditTransfer.FillYesNo();
					this.modalApplyProgram.Show();
					this.modalApplyProgram.Visible = true;
					return;
			}
		}

		protected void btnApplyProgram_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var choice1AdmissionOpenProgramID = (int)this.ViewState["Choice1AdmissionOpenProgramID"];
			var researchThemeAreaID = this.ddlResearchThemeAreaID.SelectedValue.ToNullableGuid();
			var shiftEnum = this.rblShift.GetSelectedEnumValue<Shifts>();
			var creditTransfer = this.rblCreditTransfer.SelectedValue.ToBoolean();
			var applyStatus = BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.Apply(choice1AdmissionOpenProgramID, researchThemeAreaID, shiftEnum, creditTransfer, this.CandidateIdentity.LoginSessionGuid);
			switch (applyStatus)
			{
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.ApplyStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.ApplyStatuses.AlreadyApplied:
					this.AddWarningAlert("Application is already submitted.");
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.ApplyStatuses.AccountIsNotValid:
					this.AddErrorAlert("Account is not valid.");
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.ApplyStatuses.ProfileInformationIsBlank:
					this.AddErrorAlert("Profile Information is not yet completed.");
					Redirect<Profile>();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.ApplyStatuses.ProfileInformationPictureNotUploaded:
					this.AddErrorAlert("Profile Picture is not yet uploaded.");
					Redirect<Profile>();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.ApplyStatuses.PassportNotUploaded:
					this.AddErrorAlert("Additional Documents [Passport] is missing.");
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.ApplyStatuses.StatementOfPurposeNotUploaded:
					this.AddErrorAlert("Additional Documents [Statement of Purpose] is missing.");
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.ApplyStatuses.EnglishLanguageCertificateNotUploaded:
					this.AddErrorAlert("Additional Documents [English Language Certificate] is missing.");
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.ApplyStatuses.ResearchPlanNotUploaded:
					this.AddErrorAlert("Additional Documents [Research Plan] is missing.");
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.ApplyStatuses.AcademicRecordNotAdded:
					this.AddErrorAlert("Academic Record is missing.");
					this.RefreshPage();
					return;
				case BL.Core.Admissions.Candidate.ApplyForProgramForeignStudent.ApplyStatuses.Success:
					this.AddSuccessAlert("Application has been submitted.");
					this.RefreshPage();
					return;
				default:
					throw new NotImplementedEnumException(applyStatus);
			}
		}

		protected void cvcbTerms_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
		{
			args.IsValid = this.cbTerms.Checked;
		}
	}
}