﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="ApplyProgram.aspx.cs" Inherits="Aspire.Web.Sys.Candidate.ApplyProgram" %>

<%@ Register Src="~/Sys/Candidate/AcademicRecord.ascx" TagPrefix="uc1" TagName="AcademicRecord" %>
<asp:Content ContentPlaceHolderID="HeadPH" runat="server">
	<style type="text/css">
		.select2-container--bootstrap .select2-selection--single {
			height: inherit !important;
		}

		.table-bordered > tbody > tr > th,
		.table-bordered > tbody > tr > td {
			border: 4px solid #ddd;
		}

		.form-group.undertaking.has-error {
			color: #a94442 !important;
		}

		.cbUndertaking label {
			font-weight: bold;
		}
	</style>
</asp:Content>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:UpdatePanel runat="server" ID="updatePanelMain" UpdateMode="Conditional">
		<ContentTemplate>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Campus:" AssociatedControlID="ddlInstituteID" />
				<aspire:AspireDropDownList runat="server" ValidationGroup="ApplyProgram" ID="ddlInstituteID" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="ddlInstituteID_SelectedIndexChanged" />
			</div>
			<asp:Panel runat="server" ID="updatePanelInstituteSelected">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Program Applying For:" AssociatedControlID="ddlChoice1AdmissionOpenProgramID" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="ApplyProgram" ID="ddlChoice1AdmissionOpenProgramID" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="ddlChoice1AdmissionOpenProgramID_SelectedIndexChanged" ToolTip="Selected Program" />
				</div>
				<asp:Panel runat="server" ID="updatePanelProgramSelected">
					<div class="form-group" runat="server" id="dvResearchThemeArea">
						<aspire:AspireLabel runat="server" Text="Research Theme Area:" AssociatedControlID="ddlResearchThemeAreaID" />
						<aspire:AspireDropDownListWithOptionGroups runat="server" ValidationGroup="ApplyProgram" ID="ddlResearchThemeAreaID" CausesValidation="False" ToolTip="Selected Research Theme Area" />
						<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="ApplyProgram" ControlToValidate="ddlResearchThemeAreaID" ErrorMessage="This field is required." />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Eligibility Criteria:" AssociatedControlID="tbEligibilityCriteriaStatement" />
						<aspire:AspireTextBox MaxLength="2000" ReadOnly="true" TextMode="MultiLine" Rows="2" runat="server" ID="tbEligibilityCriteriaStatement" />
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Entry Test Date/Time:" AssociatedControlID="tbEntryTestDateTime" />
								<aspire:AspireTextBox MaxLength="2000" ReadOnly="true" runat="server" ID="tbEntryTestDateTime" />
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Entry Test Duration:" AssociatedControlID="tbEntryTestDuration" />
								<aspire:AspireTextBox MaxLength="2000" ReadOnly="true" runat="server" ID="tbEntryTestDuration" />
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Admission Processing Fee:" AssociatedControlID="tbAdmissionProcessingFee" />
								<aspire:AspireTextBox MaxLength="2000" ReadOnly="true" runat="server" ID="tbAdmissionProcessingFee" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel ID="lblShift" runat="server" Text="Shift/Session:" AssociatedControlID="rblShift" />
								<div class="form-control-static" id="divrblShift" runat="server">
									<aspire:AspireRadioButtonList runat="server" ID="rblShift" RepeatDirection="Horizontal" RepeatLayout="Flow" />
								</div>
								<aspire:AspireTextBox MaxLength="2000" ReadOnly="true" runat="server" ID="tbShift" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Are you applying for Credit Transfer?" AssociatedControlID="rblCreditTransfer" />
								<div class="form-control-static">
									<aspire:AspireRadioButtonList runat="server" ID="rblCreditTransfer" RepeatDirection="Horizontal" RepeatLayout="Flow" />
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Pre-Requisite Qualification:" AssociatedControlID="ddlPreReqQualification" />
						<aspire:AspireDropDownList runat="server" ID="ddlPreReqQualification" CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="ddlPreReqQualification_SelectedIndexChanged" />
					</div>
					<asp:Panel runat="server" ID="updatePanelPreReqQualificationSelected">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Pre-Requisite Qualification Result Status:" AssociatedControlID="ddlAdmissionCriteriaPreReqID" />
							<aspire:AspireDropDownList runat="server" ID="ddlAdmissionCriteriaPreReqID" CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="ddlAdmissionCriteriaPreReqID_SelectedIndexChanged" />
						</div>
						<asp:Panel runat="server" ID="updatePanelAdmissionCriteriaPreReqIDSelected">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Academic Record</h3>
								</div>
								<table class="table table-striped table-bordered">
									<asp:Repeater runat="server" ID="repeaterAcademicRecords" ItemType="Aspire.Web.Sys.Candidate.AcademicRecord.AdmissionCriteriaPreq">
										<ItemTemplate>
											<tr>
												<th style="vertical-align: baseline; text-align: center"><%# Container.ItemIndex + 1 %></th>
												<td style="width: 100%" class="tdAcademicRecord">
													<uc1:AcademicRecord runat="server" ValidationGroup="ApplyProgram" ID="ucAcademicRecord" AdmissionCriteria="<%# Item %>" />
												</td>
											</tr>
										</ItemTemplate>
									</asp:Repeater>
								</table>
							</div>
							<div class="form-group text-justify undertaking">
								<strong>Undertaking</strong>
								<ul>
									<li>I understand that submitting incomplete, false or misleading information is considered as sufficient grounds for denial of admission and dismissal from BU at any time.</li>
									<li>I intend to provide Bahria University complete documents not later than six weeks after admission.</li>
									<li>I am aware of the eligibility criteria set forth by the university for the program I have applied for.</li>
									<li>I understand that Bahria University is allowed to cancel my admission in case I do not meet the minimum eligibility criteria.</li>
									<li>It is my responsibility to inform the university about my in-eligibility as soon as possible.</li>
								</ul>
								<div>
									<aspire:AspireCheckBox CssClass="cbUndertaking" runat="server" Inline="true" ID="cbCertify" Text="I certify that all the information I have given is complete and accurate to the best of my knowledge and belief." />
								</div>
								<aspire:AspireRequiredFieldValidator runat="server" ID="rfvcbCertify" ErrorMessage="This field is required." ValidationGroup="ApplyProgram" ControlToValidate="cbCertify" />
							</div>
							<div class="form-group text-center">
								<aspire:AspireButton ID="btnApply" ButtonType="Primary" runat="server" Text="Apply" CausesValidation="true" ValidationGroup="ApplyProgram" OnClick="btnApply_Click" />
								<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="Dashboard.aspx" />
							</div>
						</asp:Panel>
					</asp:Panel>
				</asp:Panel>
			</asp:Panel>
		</ContentTemplate>
	</asp:UpdatePanel>
	<script type="text/javascript">
		function autoCalculatePercentage(tbObt, tbTotal, tbPercentage) {
			$(tbObt).add(tbTotal).on("change blur paste", function () {
				var obt = $(tbObt).val().tryToDouble();
				var tot = $(tbTotal).val().tryToDouble();
				try {
					var perc = (obt * 100.0 / tot).toFixed(2);
					if (isFinite(perc))
						$(tbPercentage).val(perc);
					else
						$(tbPercentage).val("");
				} catch (e) {
					$(tbPercentage).val("");
				}
			});
		}
		function validatePercentage(source, args) {
			var tbObtainedMarks = $("#" + source.getAttribute("tbObtainedMarks".toLowerCase()));
			var tbTotalMarks = $("#" + source.getAttribute("tbTotalMarks".toLowerCase()));
			var tbPercentage = $("#" + source.getAttribute("tbPercentage".toLowerCase()));
			var min = source.getAttribute("min".toLowerCase()).toDouble();
			source.errormessage = "Minimum " + min + "% and maximum 100% is required.";
			var obt = tbObtainedMarks.val().tryToDouble();
			var tot = tbTotalMarks.val().tryToDouble();
			args.IsValid = obt !== null && tot !== null && tot !== 0 && obt * 100.0 / tot >= min && obt * 100.0 / tot <= 100;
			ApplyHighlightCss(source, $(tbPercentage).attr("id"), args);
		}
		function formatState(state) {
			var element = $(state.element);
			if ($(element).attr("value") === "")
				return $("<div></div>").text("Select Program");

			var programName = $("<div></div>").text(element.attr("data-ProgramName"));
			var programShortName = $("<div></div>").text(element.attr("data-ProgramShortName"));
			var programAlias = $("<div></div>").text(element.attr("data-ProgramAlias"));
			return $("<div></div>").append(programShortName.css("font-weight", "bold"), programName);
		};
		$(function () {
			function initPage() {
				$("#<%=this.ddlChoice1AdmissionOpenProgramID.ClientID%>").each(function (i, ddl) {
					if (!$(ddl)[0].select2Applied) {
						$(ddl).select2({
							theme: "bootstrap",
							width: "style",
							dropdownParent: $(ddl).parent(),
							templateResult: formatState,
							templateSelection: formatState
						});
						$(ddl)[0].select2Applied = true;
					}
				});

				function layout(divAcademicRecordRows) {
					$(divAcademicRecordRows).each(function (i, divAcademicRecordRow) {
						var parent = $(divAcademicRecordRow).parent();
						var currentRow = null;
						$(">div", divAcademicRecordRow).each(function (i, div) {
							if ($(div).hasClass("col-md-6")) {
								if (currentRow === null)
									currentRow = $("<div></div>").addClass("row").appendTo(parent).append($(div));
								else {
									currentRow.append($(div));
									currentRow = null;
								}
							}
						});
						if ($("*", divAcademicRecordRow).length === 0)
							divAcademicRecordRow.remove();
					});
				}
				layout($("div.row.academicRecords"));

				$(".tdAcademicRecord").each(function (i, tdAcademicRecord) {
					var tbObtainedMarks = $("input.tbObtainedMarks", $(tdAcademicRecord));
					var tbTotalMarks = $("input.tbTotalMarks", $(tdAcademicRecord));
					var tbPercentage = $("input.tbPercentage", $(tdAcademicRecord));
					autoCalculatePercentage(tbObtainedMarks, tbTotalMarks, tbPercentage);
					console.log(i);
				});
			}
			Sys.Application.add_load(initPage);
			initPage();
		});
	</script>
</asp:Content>
