﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Candidate
{
	[AspirePage("EA850976-4BDC-4506-BA1E-CA9BF6E84EFE", UserTypes.Candidate, "~/Sys/Candidate/ChangePassword.aspx", "Change Password", true, AspireModules.None)]
	public partial class ChangePassword : CandidatePage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_lock);

		public override string PageTitle => "Change Password";

		protected void Page_Load(object sender, EventArgs e)
		{
		}
	}
}