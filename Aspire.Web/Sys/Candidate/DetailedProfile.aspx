﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="DetailedProfile.aspx.cs" Inherits="Aspire.Web.Sys.Candidate.DetailedProfile" %>

<%@ Register Src="~/Sys/Common/Admissions/CandidateDetailedProfile.ascx" TagPrefix="uc1" TagName="CandidateDetailedProfile" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc1:CandidateDetailedProfile runat="server" ID="profile" />
</asp:Content>
