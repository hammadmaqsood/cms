﻿<%@ Page Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="Aspire.Web.Sys.Candidate.ChangePassword" %>

<%@ Register Src="~/Sys/Common/ChangePassword.ascx" TagPrefix="uc1" TagName="ChangePassword" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc1:ChangePassword runat="server" ID="userControlChangePassword" />
</asp:Content>
