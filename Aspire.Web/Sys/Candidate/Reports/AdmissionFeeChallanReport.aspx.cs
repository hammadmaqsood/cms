﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Candidate.Reports
{
	[AspirePage("70B80186-34F8-4B90-A535-8560A016E4F0", UserTypes.Candidate, "~/Sys/Candidate/Reports/AdmissionFeeChallanReport.aspx", "Admission Fee Challans", true, AspireModules.Admissions)]
	public partial class AdmissionFeeChallanReport : CandidatePage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Admission Fee Challan";
		public string ReportName => $"Admission Fee Challan [{this.CandidateIdentity.Name}]";
		public bool ExportToExcel => false;
		public bool ExportToWord => false;

		private List<int> StudentFeeIDs
		{
			get
			{
				try
				{
					return this.GetParameterValue(nameof(this.StudentFeeIDs)).Split(',').Select(s => s.ToInt()).ToList();
				}
				catch
				{
					return null;
				}
			}
		}

		public static string GetPageUrl(List<int> studentFeeIDs)
		{
			return GetPageUrl<AdmissionFeeChallanReport>().AttachQueryParams(nameof(StudentFeeIDs), string.Join(",", studentFeeIDs), ReportView.ReportFormat, ReportFormats.PDF, ReportView.Inline, false);
		}

		public static string GetPageUrl(int studentFeeID)
		{
			return GetPageUrl(new List<int> { studentFeeID });
		}


		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.StudentFeeIDs?.Any() != true)
					Redirect<Dashboard>();
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			Common.Methods.RenderFeeChallansReport<Dashboard>(reportViewer, this.StudentFeeIDs);
		}
	}
}