﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Candidate.Reports
{
	[AspirePage("D35E9AB2-197E-4367-B335-F158C92F576C", UserTypes.Candidate, "~/Sys/Candidate/Reports/AdmitSlipsReport.aspx", "Admit Slips", true, AspireModules.Admissions)]
	public partial class AdmitSlipsReport : CandidatePage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Admit Slip(s)";
		public string ReportName => $"Admit Slip(s) [{this.CandidateIdentity.Name}]";
		public bool ExportToExcel => false;
		public bool ExportToWord => false;

		public static string GetPageUrl(int candidateAppliedProgramID)
		{
			return GetPageUrl<AdmitSlipsReport>().AttachQueryParams(ReportView.ReportFormat, ReportFormats.PDF, ReportView.Inline, true, "CandidateAppliedProgramID", candidateAppliedProgramID);
		}

		public static void Redirect(int candidateAppliedProgramID)
		{
			Redirect(GetPageUrl(candidateAppliedProgramID));
		}

		private int? CandidateAppliedProgramID => this.Request.GetParameterValue<int>("CandidateAppliedProgramID");

		protected void Page_Load(object sender, EventArgs e)
		{
			if (this.CandidateIdentity.ForeignStudent)
			{
				Redirect<Dashboard>();
				return;
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			Common.Methods.RenderAdmitSlip<Dashboard>(this, reportViewer, this.CandidateIdentity.CandidateID, this.CandidateAppliedProgramID);
		}
	}
}