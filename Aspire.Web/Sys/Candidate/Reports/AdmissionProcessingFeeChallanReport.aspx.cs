﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Candidate.Reports
{
	[AspirePage("82B9D1B6-3149-4857-915B-237169FEBF76", UserTypes.Candidate, "~/Sys/Candidate/Reports/AdmissionProcessingFeeChallanReport.aspx", "Admission Processing Fee Challan", true, AspireModules.Admissions)]
	public partial class AdmissionProcessingFeeChallanReport : CandidatePage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => this.ReportName;
		public string ReportName => "Admission Processing Fee Challan";
		public bool ExportToExcel => false;
		public bool ExportToWord => false;

		public static string GetPageUrl(int candidateAppliedProgramID)
		{
			return GetPageUrl<AdmissionProcessingFeeChallanReport>().AttachQueryParams(ReportView.ReportFormat, ReportFormats.PDF, ReportView.Inline, true, nameof(CandidateAppliedProgramID), candidateAppliedProgramID);
		}

		private int? CandidateAppliedProgramID => this.GetParameterValue<int>(nameof(this.CandidateAppliedProgramID));

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack || this.CandidateIdentity.ForeignStudent)
			{
				if (this.CandidateAppliedProgramID == null)
					Redirect<Dashboard>();
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			Common.Methods.RenderAdmissionProcessingFeeChallansReport<Dashboard>(this, reportViewer, this.CandidateAppliedProgramID ?? throw new InvalidOperationException());
		}
	}
}