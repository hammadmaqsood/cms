﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Candidate
{
	[AspirePage("7D3546FD-6D93-47DD-9783-319F4FAA56BB", UserTypes.Candidate, "~/Sys/Candidate/ContactUs.aspx", "Contact Us", true, AspireModules.Admissions)]
	public partial class ContactUs : CandidatePage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.phone);
		public override string PageTitle => "Contact Us";

		protected void Page_Load(object sender, EventArgs e)
		{
		}
	}
}