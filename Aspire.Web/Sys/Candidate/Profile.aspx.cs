﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Candidate
{
	[AspirePage("151BE26B-4FB6-48C9-BE9A-C27806DCB864", UserTypes.Candidate, "~/Sys/Candidate/Profile.aspx", "Profile Information", true, AspireModules.Admissions)]
	public partial class Profile : CandidatePage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_user);
		public override string PageTitle => "Profile Information";

		protected void Page_Load(object sender, EventArgs e)
		{
		}
	}
}