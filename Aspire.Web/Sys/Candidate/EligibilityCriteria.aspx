﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="EligibilityCriteria.aspx.cs" Inherits="Aspire.Web.Sys.Candidate.EligibilityCriteria" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:Repeater runat="server" ID="rptEligibilityCriteria" ItemType="Aspire.BL.Core.Admissions.Candidate.Common.EligibilityCriteria">
		<ItemTemplate>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
						<%#: Item.InstituteName %>
					</h3>
				</div>
				<div class="list-group">
					<asp:Repeater runat="server" ID="rptCriteria" DataSource="<%# Item.Programs.OrderBy(p=> p.ProgramName).ThenBy(p=> p.ProgramDurationEnum) %>" ItemType="Aspire.BL.Core.Admissions.Candidate.Common.EligibilityCriteria.Program">
						<ItemTemplate>
							<div class="list-group-item">
								<h4 class="list-group-item-heading"><%# Item.ProgramName %> - <%# Item.ProgramDurationEnum.ToFullName() %></h4>
								<div class="list-group-item-text">
									<%#: Item.EligibilityCriteriaStatement %>
								</div>
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</div>
			</div>
		</ItemTemplate>
	</asp:Repeater>
</asp:Content>
