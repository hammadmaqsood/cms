﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Candidate
{
	public partial class AcademicRecord : UserControl
	{
		private new ApplyProgram Page => (ApplyProgram)base.Page;

		private AdmissionCriteriaPreReqDegree _admissionCriteriaPreReqDegree;

		private AdmissionCriteriaPreReqDegree AdmissionCriteriaPreReqDegree
		{
			get
			{
				if (this._admissionCriteriaPreReqDegree == null)
				{
					var admissionCriteriaPreReqDegreeID = this.ddlDegreeType.SelectedValue.ToInt();
					this._admissionCriteriaPreReqDegree = this.Page.AdmissionCriteriaPreReqs
						.Where(acpr => acpr.AdmissionCriteriaPreReqID == this.AdmissionCriteriaPreReqID)
						.SelectMany(acpr => acpr.AdmissionCriteriaPreReqDegrees)
						.SingleOrDefault(acprd => acprd.AdmissionCriteriaPreReqDegreeID == admissionCriteriaPreReqDegreeID && acprd.DisplaySequence == this.DisplaySequence);
				}
				return this._admissionCriteriaPreReqDegree;
			}
		}

		public sealed class AdmissionCriteriaPreq
		{
			internal AdmissionCriteriaPreq() { }
			public int AdmissionCriteriaPreReqID { get; set; }
			public byte DisplaySequence { get; set; }
		}

		public string ValidationGroup
		{
			get => (string)this.ViewState[nameof(this.ValidationGroup)];
			set => this.ViewState[nameof(this.ValidationGroup)] = value;
		}

		private int AdmissionCriteriaPreReqID
		{
			get => (int)this.ViewState[nameof(this.AdmissionCriteriaPreReqID)];
			set => this.ViewState[nameof(this.AdmissionCriteriaPreReqID)] = value;
		}

		private byte DisplaySequence
		{
			get => (byte)this.ViewState[nameof(this.DisplaySequence)];
			set => this.ViewState[nameof(this.DisplaySequence)] = value;
		}

		public AdmissionCriteriaPreq AdmissionCriteria
		{
			set
			{
				this.AdmissionCriteriaPreReqID = value.AdmissionCriteriaPreReqID;
				this.DisplaySequence = value.DisplaySequence;
				var degreeTypes = this.Page.AdmissionCriteriaPreReqs.Where(acpr => acpr.AdmissionCriteriaPreReqID == this.AdmissionCriteriaPreReqID)
					.SelectMany(acpr => acpr.AdmissionCriteriaPreReqDegrees)
					.Where(acprd => acprd.DisplaySequence == this.DisplaySequence)
					.Select(acprd => new ListItem(acprd.DegreeTypeEnumFullName, acprd.AdmissionCriteriaPreReqDegreeID.ToString()))
					.ToList();
				this.ddlDegreeType.DataBind(degreeTypes);
				this.ddlDegreeType.Enabled = this.ddlDegreeType.Items.Count > 1;
				this.ddlDegreeType.ValidationGroup = this.ValidationGroup;
				this.rblExaminationSystem.ValidationGroup = this.ValidationGroup;
				this.ddlTotalCGPA.ValidationGroup = this.ValidationGroup;
				this.tbObtainedCGPA.ValidationGroup = this.ValidationGroup;
				this.dvtbObtainedCGPA.ValidationGroup = this.ValidationGroup;
				this.tbObtainedMarks.ValidationGroup = this.ValidationGroup;
				this.ivtbObtainedMarks.ValidationGroup = this.ValidationGroup;
				this.tbTotalMarks.ValidationGroup = this.ValidationGroup;
				this.ivtbTotalMarks.ValidationGroup = this.ValidationGroup;
				this.cvtbTotalMarks.ValidationGroup = this.ValidationGroup;
				this.tbPercentage.ValidationGroup = this.ValidationGroup;
				this.cvtbPercentage.ValidationGroup = this.ValidationGroup;
				this.ddlSubjects.ValidationGroup = this.ValidationGroup;
				this.tbSubjects.ValidationGroup = this.ValidationGroup;
				this.svtbSubjects.ValidationGroup = this.ValidationGroup;
				this.tbInstitute.ValidationGroup = this.ValidationGroup;
				this.svtbInstitute.ValidationGroup = this.ValidationGroup;
				this.tbBoardUniversity.ValidationGroup = this.ValidationGroup;
				this.svtbBoardUniversity.ValidationGroup = this.ValidationGroup;
				this.tbPassingYear.ValidationGroup = this.ValidationGroup;
				this.ivtbPassingYear.ValidationGroup = this.ValidationGroup;

				this.ddlDegreeType_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlDegreeType_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.rblExaminationSystem.Items.Clear();
			if (this.AdmissionCriteriaPreReqDegree.AnnualExamSystem)
				this.rblExaminationSystem.Items.Add(new ListItem("Annual System", "A"));
			if (this.AdmissionCriteriaPreReqDegree.SemesterSystem)
				this.rblExaminationSystem.Items.Add(new ListItem("Semester System", "S"));
			if (this.rblExaminationSystem.Items.Count == 0)
				throw new InvalidOperationException("Invalid Criteria. AdmissionCriteriaPreReqID:" + this.AdmissionCriteriaPreReqDegree.AdmissionCriteriaPreReqID);
			this.rblExaminationSystem.SelectedIndex = 0;
			this.divExaminationSystem.Visible = this.rblExaminationSystem.Items.Count > 1;
			this.rblExaminationSystem_SelectedIndexChanged(null, null);

			var subjects = this.AdmissionCriteriaPreReqDegree?.AdmissionCriteriaPreReqDegreeSubjects.Select(s => s.Subjects).ToList();
			if (subjects?.Any() == true)
			{
				this.ddlSubjects.DataBind(subjects);
				this.ddlSubjects.Enabled = this.ddlSubjects.Items.Count > 1;
				this.divddlSubjects.Visible = true;
				this.divtbSubjects.Visible = false;
			}
			else
			{
				this.divddlSubjects.Visible = false;
				this.ddlSubjects.Items.Clear();
				this.divtbSubjects.Visible = true;
				this.tbSubjects.Text = null;
			}
			this.ivtbPassingYear.MinValue = (short)(DateTime.Now.Year - 100);
			this.ivtbPassingYear.MaxValue = (short)DateTime.Now.Year;
		}

		public int? MinPercentage
		{
			get => this.ViewState[nameof(this.MinPercentage)] as int?;
			set => this.ViewState[nameof(this.MinPercentage)] = value;
		}

		protected void rblExaminationSystem_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.AdmissionCriteriaPreReqDegree == null)
				throw new InvalidOperationException();
			var isAnnualSystem = this.rblExaminationSystem.SelectedValue == "A";
			var isSemesterSystem = this.rblExaminationSystem.SelectedValue == "S";

			this.divObtainedCGPA.Visible = this.divTotalCGPA.Visible = isSemesterSystem;
			this.dvtbObtainedCGPA.Enabled = isSemesterSystem;

			this.divObtainedMarks.Visible = this.divTotalMarks.Visible = this.divPercentage.Visible = isAnnualSystem;
			this.ivtbObtainedMarks.Enabled = this.ivtbTotalMarks.Enabled = this.cvtbTotalMarks.Enabled = this.cvtbPercentage.Enabled = isAnnualSystem;

			this.MinPercentage = null;
			if (isAnnualSystem)
			{
				var minPercentage = this.AdmissionCriteriaPreReqDegree.AnnualExamSystemMinimumPercentage ?? 0;
				this.MinPercentage = minPercentage;
				this.cvtbPercentage.Attributes.Add(this.tbObtainedMarks.ID, this.tbObtainedMarks.ClientID);
				this.cvtbPercentage.Attributes.Add(this.tbTotalMarks.ID, this.tbTotalMarks.ClientID);
				this.cvtbPercentage.Attributes.Add(this.tbPercentage.ID, this.tbPercentage.ClientID);
				this.cvtbPercentage.Attributes.Add("min", minPercentage.ToString());
				this.cvtbPercentage.ErrorMessage = $"Minimum {minPercentage}% and maximum 100% is required.";
			}
			if (isSemesterSystem)
			{
				this.ddlTotalCGPA.Items.Clear();
				if (this.AdmissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf4 != null)
					this.ddlTotalCGPA.Items.Add(new ListItem("4.0", "4.0"));
				if (this.AdmissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf5 != null)
					this.ddlTotalCGPA.Items.Add(new ListItem("5.0", "5.0"));

				if (this.ddlTotalCGPA.Items.Count == 0)
					throw new InvalidOperationException("Invalid Criteria");
				this.ddlTotalCGPA_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlTotalCGPA_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.ddlTotalCGPA.SelectedValue == "4.0")
			{
				this.tbObtainedCGPA.Attributes.Add("data-inputmask", "'alias': 'cgpa4'");
				if (this.AdmissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf4 == null)
					throw new InvalidOperationException("SemesterSystemMinCGPAOutOf4 cannot be null for CGPA 4.");
				this.dvtbObtainedCGPA.MinValue = this.AdmissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf4.Value;
				this.dvtbObtainedCGPA.MaxValue = 4f;
			}
			else if (this.ddlTotalCGPA.SelectedValue == "5.0")
			{
				this.tbObtainedCGPA.Attributes.Add("data-inputmask", "'alias': 'cgpa5'");
				if (this.AdmissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf5 == null)
					throw new InvalidOperationException("SemesterSystemMinCGPAOutOf5 cannot be null for CGPA 5.");
				this.dvtbObtainedCGPA.MinValue = this.AdmissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf5.Value;
				this.dvtbObtainedCGPA.MaxValue = 5f;
			}
			else
				throw new InvalidOperationException("Invalid Criteria");
		}

		protected void cvtbPercentage_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			var minPercentage = this.MinPercentage ?? 0d;
			var obtainedMarks = this.tbObtainedMarks.Text.ToDouble();
			var totalMarks = this.tbTotalMarks.Text.ToDouble();
			var percentage = obtainedMarks * 100f / totalMarks;
			args.IsValid = minPercentage <= percentage && percentage <= 100d;
		}

		public CandidateAppliedProgramAcademicRecord ToAcademicRecord()
		{
			var admissionCriteriaPreReqDegreeID = this.ddlDegreeType.SelectedValue.ToInt();
			var degreeType = Enum.GetValues(typeof(DegreeTypes)).Cast<DegreeTypes>().Single(d => d.ToFullName() == this.ddlDegreeType.SelectedItem.Text);
			string subjects;
			if (this.divddlSubjects.Visible)
				subjects = this.ddlSubjects.SelectedValue;
			else if (this.divtbSubjects.Visible)
				subjects = this.tbSubjects.Text.TrimAndCannotBeEmpty();
			else
				throw new InvalidOperationException();
			var institute = this.tbInstitute.Text.TrimAndCannotBeEmpty();
			var boardUniversity = this.tbBoardUniversity.Text.TrimAndCannotBeEmpty();
			var passingYear = this.tbPassingYear.Text.ToShort();
			if (this.rblExaminationSystem.SelectedValue == "S")
			{
				var obtainedMarks = this.tbObtainedCGPA.Text.ToDouble();
				var totalMarks = this.ddlTotalCGPA.SelectedValue.ToDouble();
				if (obtainedMarks < this.dvtbObtainedCGPA.MinValue || this.dvtbObtainedCGPA.MaxValue < obtainedMarks)
					throw new InvalidOperationException();
				var percentage = obtainedMarks * 100d / totalMarks;
				return new CandidateAppliedProgramAcademicRecord
				{
					AdmissionCriteriaPreReqDegreeID = admissionCriteriaPreReqDegreeID,
					DegreeTypeEnum = degreeType,
					Subjects = subjects,
					Institute = institute,
					BoardUniversity = boardUniversity,
					IsCGPA = true,
					ObtainedMarks = obtainedMarks,
					TotalMarks = totalMarks,
					Percentage = percentage,
					PassingYear = passingYear
				};
			}
			else
			{
				var obtainedMarks = this.tbObtainedMarks.Text.ToDouble();
				var totalMarks = this.tbTotalMarks.Text.ToDouble();
				var percentage = obtainedMarks * 100f / totalMarks;
				var minPercentage = this.MinPercentage ?? throw new InvalidOperationException();
				if (percentage < minPercentage || 100 < percentage)
					throw new InvalidOperationException("Percentage tempered.");
				return new CandidateAppliedProgramAcademicRecord
				{
					AdmissionCriteriaPreReqDegreeID = admissionCriteriaPreReqDegreeID,
					DegreeTypeEnum = degreeType,
					Subjects = subjects,
					Institute = institute,
					BoardUniversity = boardUniversity,
					IsCGPA = false,
					ObtainedMarks = obtainedMarks,
					TotalMarks = totalMarks,
					Percentage = percentage,
					PassingYear = passingYear
				};
			}
		}
	}
}