﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Candidate
{
	[AspirePage("E6F9D1B8-0623-4343-957D-3A074A456B36", UserTypes.Candidate, "~/Sys/Candidate/DetailedProfile.aspx", "Profile Summary", true, AspireModules.Admissions)]
	public partial class DetailedProfile : CandidatePage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_user);

		public override string PageTitle => "Profile Summary";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
				this.profile.LoadCandidateInfo(this.CandidateIdentity.CandidateID, this.CandidateIdentity.LoginSessionGuid);
		}
	}
}