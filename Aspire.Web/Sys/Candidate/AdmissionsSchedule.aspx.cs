﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Candidate
{
	[AspirePage("3CEB8616-8146-4443-9ADC-6D0484B5E67F", UserTypes.Candidate, "~/Sys/Candidate/AdmissionsSchedule.aspx", "Admissions Schedule", true, AspireModules.Admissions)]
	public partial class AdmissionsSchedule : CandidatePage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.list);
		public override string PageTitle => $"Admissions Schedule - {this.CandidateIdentity.SemesterID.ToSemesterString()}";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var openPrograms = BL.Core.Admissions.Candidate.Common.GetAllAdmissionOpenProgramsSchedule(this.CandidateIdentity.LoginSessionGuid);
				this.rptAdmissionOpen.DataSource = openPrograms.OrderBy(p => p.InstituteID).ToList();
				this.rptAdmissionOpen.DataBind();
			}
		}
	}
}