﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Candidate
{
	[AspirePage("C18AC46D-3711-420A-AEBB-ABAB54B848C8", UserTypes.Candidate, "~/Sys/Candidate/Dashboard.aspx", "Home", true, AspireModules.None)]
	public partial class Dashboard : CandidatePage
	{
		public override bool BreadcrumbVisible => false;

		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_home);

		public override string PageTitle => "Home";

		protected void Page_Load(object sender, EventArgs e)
		{
		}
	}
}