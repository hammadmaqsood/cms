﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CBTSession.aspx.cs" Inherits="Aspire.Web.Sys.Candidate.CBTSession" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<table class="tableCol4 table table-bordered table-condensed">
		<tbody>
			<tr>
				<th>Application No.</th>
				<td>
					<aspire:Label runat="server" ID="lblApplicationNo" /></td>
				<th>Semester</th>
				<td>
					<aspire:Label runat="server" ID="lblSemester" /></td>
			</tr>
			<tr>
				<th>Institute</th>
				<td>
					<aspire:Label runat="server" ID="lblInstituteShortName" /></td>
				<th>Program</th>
				<td>
					<aspire:Label runat="server" ID="lblProgramShortName" /></td>
			</tr>
		</tbody>
	</table>
	<asp:Repeater runat="server" ID="repeaterCBTSessions" OnItemCommand="repeaterCBTSessions_OnItemCommand" ItemType="Aspire.BL.Core.Admissions.Candidate.ApplyForProgram.GetCBTSessionsResult.CBTSession">
		<HeaderTemplate>
			<table class="table table-bordered table-hover table-striped" id="<%=this.repeaterCBTSessions.ClientID %>">
				<caption>Select your desired session for Computer Based Test.</caption>
				<thead>
					<tr>
						<th>Test Centre</th>
						<th>Date</th>
						<th>Timing</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
				<td><%# Item.InstituteShortName %></td>
				<td><%# Item.StartTime.ToLongDateString() %></td>
				<td><%# Item.Timing %></td>
				<td>
					<aspire:AspireLinkButton runat="server" CausesValidation="False" ConfirmMessage="Are you sure you want to continue?" Text="Confirm Seat" CommandName="ConfirmSeat" EncryptedCommandArgument='<%# Item.CBTSessionID %>' />
				</td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</tbody>
			</table>
		</FooterTemplate>
	</asp:Repeater>
	<asp:Panel runat="server" ID="panelNoSessionAvailable">
		<h3>No session is available right now. Please contact
			<aspire:AspireHyperLink runat="server" NavigateUrl="ContactUs.aspx">admission office</aspire:AspireHyperLink>.
		</h3>
	</asp:Panel>
</asp:Content>
