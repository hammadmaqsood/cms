﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="Aspire.Web.Sys.Candidate.ContactUs" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="table-responsive">
		<table class="table table-bordered table-hover">
			<tbody>
				<tr>
					<td>
						<strong>Bahria University Islamabad Campus</strong>
						<ul>
							<li>Help Line: +92-51-111-111-028 Ext. 1422</li>
							<li>Direct Line: <span>+92-51-9243598</span></li>
							<li>Email: <a class="a" href="mailto:adm@bahria.edu.pk">adm@bahria.edu.pk</a></li>
						</ul>
					</td>
				</tr>
				<tr>
					<td>
						<strong>Bahria University Karachi Campus</strong>
						<ul>
							<li>Help Line: +92-21-111-111-028 Ext. 221</li>
							<li>Email: <a class="a" href="mailto:admissions.bukc@bahria.edu.pk">admissions.bukc@bahria.edu.pk</a></li>
						</ul>
					</td>
				</tr>
				<tr>
					<td>
						<strong>Institute of Professional Psychology, Karachi</strong>
						<ul>
							<li>Help Line: +92-21-99240013-14 Ext. 114</li>
							<li>Email: <a class="a" href="mailto:adm.ipp@bahria.edu.pk">adm.ipp@bahria.edu.pk</a></li>
						</ul>
					</td>
				</tr>
				<tr>
					<td><strong>Bahria University Lahore Campus</strong>
						<ul>
							<li>Help Line: +92-42-99233404, +92-42-99233408 to 15</li>
							<li>Cell Phone: +92-335-6699233</li>
							<li>Email: <a class="a" href="mailto:admissions.bulc@bahria.edu.pk">admissions.bulc@bahria.edu.pk</a></li>
						</ul>
					</td>
				</tr>
				<tr>
					<td><strong>Bahria University Medical & Dental College, Karachi</strong>
						<ul>
							<li>Help Line: +92-21-35319491-9 Ext. 1029</li>
							<li>Email: <a class="a" href="mailto:adm.bumdc@bahria.edu.pk">adm.bumdc@bahria.edu.pk</a></li>
						</ul>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</asp:Content>
