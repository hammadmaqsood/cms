using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Candidate
{
	public abstract class CandidatePage : AspireBasePage, IBreadcrumb, ISidebar, IProfileInfo
	{
		protected override void Authenticate()
		{
			if (this.CandidateIdentity == null)
				throw new AspireAccessDeniedException(UserTypes.Candidate, UserTypes.Candidate);
		}

		private ICandidateIdentity _candidateIdentity;
		public ICandidateIdentity CandidateIdentity => this._candidateIdentity ?? (this._candidateIdentity = this.AspireIdentity as ICandidateIdentity);

		public override AspireThemes AspireTheme => AspireDefaultThemes.Candidate;

		public virtual bool BreadcrumbVisible => true;

		public override bool PageTitleVisible => true;

		public virtual bool SidebarVisible => true;

		public virtual string SidebarHtml => SessionHelper.SideBarLinkHtml;

		public virtual string BreadcrumbHtml => SessionHelper.SideBarLinks?.GetBreadcrumbHtml(this.AspirePageAttribute.AspirePageGuid);

		public bool IsCustomProfileImage => true;

		public string CustomProfileImageUrl => Common.CandidatePicture.GetImageUrl(this.CandidateIdentity.CandidateID, TimeSpan.FromMinutes(10));

		public virtual bool ProfileInfoVisible => true;

		public virtual bool ProfileLinkVisible => true;

		public virtual string ProfileNavigateUrl => GetPageUrl<Profile>();

		public virtual bool LogoffLinkVisible => true;

		public virtual string LogoffNavigateUrl => Logoff.GetPageUrl(UserLoginHistory.LogOffReasons.LogOff);

		public virtual string Username => this.CandidateIdentity.Name;

		public virtual string Name => this.CandidateIdentity.Name;

		public virtual string Email => this.CandidateIdentity.Email;
	}
}