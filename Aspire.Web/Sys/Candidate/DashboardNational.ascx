﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardNational.ascx.cs" Inherits="Aspire.Web.Sys.Candidate.DashboardNational" %>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Admission Process</h3>
	</div>
	<ul class="list-group">
		<li class="list-group-item" id="step1">
			<h3 class="list-group-item-heading">Step 1:
				<aspire:HyperLink runat="server" NavigateUrl="EligibilityCriteria.aspx" Text="Read Eligibility Criteria/Instructions " />
			</h3>
			<p class="list-group-item-text">
				Please read Eligibility Criteria and other instructions carefully before Applying for Admission. You cannot proceed to next Step until and unless current/previous Step is completed.
			</p>
		</li>
		<li class="list-group-item" id="step2">
			<h3 class="list-group-item-heading">Step 2:
				<aspire:HyperLink runat="server" NavigateUrl="Profile.aspx" Text="Provide Profile Information" />
			</h3>
			<p class="list-group-item-text">
				Provide Personal, Father's, Sponsor's Information and your recent Photograph.
			</p>
			<strong>Status:</strong>
			<asp:Label runat="server" ID="lblProfileInformationStatus" />
		</li>
		<li class="list-group-item" id="step3">
			<h3 class="list-group-item-heading">Step 3:
				<aspire:HyperLink runat="server" NavigateUrl="ApplyProgram.aspx" Text="Apply for Program" />
			</h3>
			<p class="list-group-item-text">
				Apply for Program and provide the Academic Information for eligibility criteria.
			</p>
		</li>
		<li class="list-group-item" id="step4">
			<h3 class="list-group-item-heading">Step 4: Submit Admission Processing Fee</h3>
			<p class="list-group-item-text">
				Submission status will be available after Confirmation of payment from the bank (expected after two working days). In case, "Admit Slip" is not enabled after 02 days of fee deposit then send an email along with scan copy of paid slip to Admissions office of respective campus. Email addresses are mentioned in “Contact Us” menu (see on left side).
			</p>
			<asp:Repeater runat="server" ID="repeaterStep4AdmissionProcessingFee">
				<HeaderTemplate>
					<ul class="list-group">
				</HeaderTemplate>
				<ItemTemplate>
					<li class="list-group-item">
						<h4 class="list-group-item-heading"><%#: Eval("ProgramShortName") %> - <%#: Eval("InstituteShortName") %></h4>
						<p class="list-group-item-text">
							<strong>Status:</strong> <%#: Eval("AdmissionProcessingFeeStatus") %>
							<aspire:AspireHyperLink Target="_blank" NavigateUrl='<%# Aspire.Web.Sys.Candidate.Reports.AdmissionProcessingFeeChallanReport.GetPageUrl((int)Eval("CandidateAppliedProgramID")) %>' Visible='<%# Eval("CanDownloadAdmissionProcessingFeeChallan") %>' runat="server" Text="Download Fee Challan" />
						</p>
					</li>
				</ItemTemplate>
				<SeparatorTemplate>
				</SeparatorTemplate>
				<FooterTemplate>
					</ul>
				</FooterTemplate>
			</asp:Repeater>
		</li>
		<li class="list-group-item" id="step5">
			<h3 class="list-group-item-heading">Step 5: Print Admit Slip</h3>
			<p class="list-group-item-text">
				In case the applicant has ETS (GRE/GAT/SAT etc.) scores, please send email with scan copy of ETS result to admissions office of respective campus, (Email addresses are mentioned in “Contact Us” see on left side) in order to get exemption from appearing in Entry Test.
			</p>
			<asp:Repeater runat="server" ID="repeaterStep5PrintAdmitSlip">
				<HeaderTemplate>
					<ul class="list-group">
				</HeaderTemplate>
				<ItemTemplate>
					<li class="list-group-item">
						<h4 class="list-group-item-heading"><%#: Eval("ProgramShortName") %> - <%#: Eval("InstituteShortName") %></h4>
						<p class="list-group-item-text">
							<strong>Status:</strong> <%#: Eval("AdmissionProcessingFeeStatus") %>
							<aspire:AspireHyperLink Target="_blank" NavigateUrl='<%# Aspire.Web.Sys.Candidate.Reports.AdmitSlipsReport.GetPageUrl((int)Eval("CandidateAppliedProgramID")) %>' Visible='<%# Eval("CanDownloadAdmitSlip") %>' runat="server" Text="Download Admit Slip" />
						</p>
					</li>
				</ItemTemplate>
				<SeparatorTemplate>
				</SeparatorTemplate>
				<FooterTemplate>
					</ul>
				</FooterTemplate>
			</asp:Repeater>
		</li>
		<li class="list-group-item" id="step6">
			<h3 class="list-group-item-heading">Step 6: Interview</h3>
			<asp:Repeater runat="server" ID="repeaterStep6Interview">
				<HeaderTemplate>
					<ul class="list-group">
				</HeaderTemplate>
				<ItemTemplate>
					<li class="list-group-item">
						<h4 class="list-group-item-heading"><%#: Eval("ProgramShortName") %> - <%#: Eval("InstituteShortName") %></h4>
						<p class="list-group-item-text">
							<strong>Status:</strong> <%#: Eval("InterviewStatus") %>
						</p>
					</li>
				</ItemTemplate>
				<SeparatorTemplate>
				</SeparatorTemplate>
				<FooterTemplate>
					</ul>
				</FooterTemplate>
			</asp:Repeater>
		</li>
		<li class="list-group-item" id="step7">
			<h3 class="list-group-item-heading">Step 7: Submit Admission Fee</h3>
			<asp:Repeater runat="server" ID="repeaterStep7AdmissionFee">
				<HeaderTemplate>
					<ul class="list-group">
				</HeaderTemplate>
				<ItemTemplate>
					<li class="list-group-item">
						<h4 class="list-group-item-heading"><%#: Eval("ProgramShortName") %> - <%#: Eval("InstituteShortName") %></h4>
						<p class="list-group-item-text">
							<strong>Status:</strong> <%#: Eval("AdmissionFeeStatus") %>
							<aspire:AspireHyperLink Target="_blank" NavigateUrl='<%# Eval("StudentFeeID") == null ? null : Aspire.Web.Sys.Candidate.Reports.AdmissionFeeChallanReport.GetPageUrl((int)Eval("StudentFeeID")) %>' Visible='<%# Eval("CanDownloadAdmissionFeeChallan") %>' runat="server" Text="Download Fee Challan" />
						</p>
					</li>
				</ItemTemplate>
				<SeparatorTemplate>
				</SeparatorTemplate>
				<FooterTemplate>
					</ul>
				</FooterTemplate>
			</asp:Repeater>
		</li>
	</ul>
</div>
