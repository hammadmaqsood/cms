﻿using System;
using System.Collections.Generic;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys
{
	[AspirePage("DD2F2713-F593-4D86-A39D-D33CFEA79F7B", UserTypes.Anonymous, "~/Sys/EmployerSurvey.aspx", "Survey Form-8", true, AspireModules.None)]
	public partial class EmployerSurvey : AnonymousPage
	{

		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.lock_);
		public override string PageTitle => "Employer Survey (Survey Form-8)";
		public override AspireThemes AspireTheme => AspireDefaultThemes.Student;
		private Guid? SurveyCode => this.Request.Params["C"].TryToGuid();
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var code = this.SurveyCode;
				if (code == null)
				{
					this.AddErrorAlert("You are not authorized to access this form.");
					this.QAsurveyUserControl.Visible = false;
					return;
				}
				var surveyUserID = BL.Core.QualityAssurance.Student.Employer.GetSurveyUser(code.Value);
				this.QAsurveyUserControl.LoadData(surveyUserID.QASurveyUserID);
				this.QAsurveyUserControl.Visible = true;
			}
		}
		public static string GetPageUrl(int? qaSurveyUserID)
		{
			return GetAspirePageAttribute<EmployerSurvey>().PageUrl.AttachQueryParam("qaSurveyUserID", qaSurveyUserID);
		}

		public static void Redirect(int? qaSurveyUserID)
		{
			Redirect(GetPageUrl(qaSurveyUserID));
		}
	}
}