﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="FacultyEvaluationComments.aspx.cs" Inherits="Aspire.Web.Sys.Executive.QualityAssurance.FacultyEvaluationComments" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="form-inline">
                <div class="form-group">
                    <aspire:AspireLabel runat="server" Text="Faculty Type:" AssociatedControlID="ddlFacultyType" />
                    <aspire:AspireDropDownList runat="server" ID="ddlFacultyType" ValidationGroup="Paramters" />
                </div>
                <div class="form-group">
                    <aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
                    <aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="Parameters" />
                </div>
                <div class="form-group">
                    <aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
                    <aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" ValidationGroup="Parameters" />
                </div>
                <div class="form-group">
                    <aspire:AspireButton runat="server" ID="btnDisplay" Text="Display" ValidationGroup="Paramters" OnClick="btnDisplay_OnClick" />
                    <aspire:HiddenField runat="server" ID="SurveyConductID" />
                </div>
                <div class="form-group">
                    <aspire:AspireButton runat="server" ID="btnReport" Text="View Report" ValidationGroup="Paramters" OnClick="btnReport_OnClick" />
                </div>
            </div>
        </div>
    </div>
    <p></p>
    <aspire:AspireGridView runat="server" ID="gvFacultySummary" AutoGenerateColumns="False" HorizontalAlign="Center" AllowSorting="True" AllowPaging="True" OnDataBound="gvFacultySummary_OnDataBound" OnSorting="gvFacultySummary_OnSorting" OnPageIndexChanging="gvFacultySummary_OnPageIndexChanging" OnPageSizeChanging="gvFacultySummary_OnPageSizeChanging" OnRowCommand="gvFacultySummary_OnRowCommand">
        <Columns>
            <asp:BoundField HeaderText="Faculty Name" DataField="Name"  SortExpression="Name"  />
            <asp:BoundField HeaderText="Course Title" DataField="Title"  SortExpression="Title" />
            <asp:BoundField HeaderText="Class" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" SortExpression="Class" DataField="Class" />
            <asp:BoundField HeaderText="Mids Evaluation" DataField="BeforeMidsAvg" SortExpression="BeforeMidsAvg" DataFormatString="{0:0.00}" />
            <asp:BoundField HeaderText="Mids Evaluation Average" DataField="overAllAvgBeforeMid" SortExpression="overAllAvgBeforeMid" DataFormatString="{0:f2}" />
            <asp:BoundField HeaderText="Final Evaluation" DataField="BeforeFinalAvg" SortExpression="BeforeFinalAvg"  DataFormatString="{0:f2}" />
            <asp:BoundField HeaderText="Final Evaluation Average" DataField="OverAllAvgBeforeFinal" SortExpression="OverAllAvgBeforeFinal" DataFormatString="{0:f2}" />
            <asp:BoundField HeaderText="Mids & Final Average" DataField="OverAllAverage2" SortExpression="OverAllAverage2"  DataFormatString="{0:f2}" />
            <asp:BoundField HeaderText="Appraisal of HOD" DataField="HodRemarks" />
            <asp:BoundField HeaderText="Director Remarks" DataField="DirectorRemarks" />
            <asp:BoundField HeaderText="DG Remarks" DataField="DgRemarks" />
            <asp:TemplateField HeaderText="Comments">
                <ItemTemplate>
                    <aspire:AspireLinkButton runat="server" ID="btnComment" CausesValidation="False" CommandName="Add" Glyphicon="plus" ButtonType="Success" ToolTip="Add Comments" EncryptedCommandArgument="<%# Container.DataItemIndex %>" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="False">
                <ItemTemplate>
                    <aspire:HiddenField runat="server" ID="FacultyID" Value='<%# this.Eval("FacultyMemberID")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="False">
                <ItemTemplate>
                    <aspire:HiddenField runat="server" ID="DepartmentID" Value='<%# this.Eval("DepartmentID")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="False">
                <ItemTemplate>
                    <aspire:HiddenField runat="server" ID="RemarksID" Value='<%# this.Eval("RemarksID")%>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </aspire:AspireGridView>
    <aspire:AspireLabel runat="server" ID="check"></aspire:AspireLabel>
    <div class="form-group text-center">
        <aspire:AspireButton runat="server" ID="btnLockComments" ButtonType="Danger" Text=" Lock Comments" FontAwesomeIcon="lock_" ValidationGroup="Paramters" Visible="False" OnClick="btnLockComments_OnClick" />
       </div>
    <aspire:AspireModal runat="server" ID="ModelPopup">
        <HeaderTemplate> <header>COMMENTS</header></HeaderTemplate>
       <BodyTemplate>
            <aspire:HiddenField runat="server" ID="lblsurveyID" />
            <aspire:HiddenField runat="server" ID="lblfacultyID" />
            <aspire:HiddenField runat="server" ID="lbldepartmentID" />
            <aspire:HiddenField runat="server" ID="lblmidsAvg" />
            <aspire:HiddenField runat="server" ID="lblfinalAvg" />
            <div class="form-horizontal">
                <div class="form-group">
                    <aspire:AspireLabel runat="server" Text="Faculty Name:" AssociatedControlID="lblName" CssClass="col-md-3" />
                    <div class="form-control-static col-md-9">
                        <aspire:AspireLabel runat="server" ID="lblName"></aspire:AspireLabel>
                    </div>
                </div>
                <div class="form-group">
                    <aspire:AspireLabel runat="server" Text="Total Average:" AssociatedControlID="lblAverage" CssClass="col-md-3" />
                    <div class="form-control-static col-md-9">
                        <aspire:AspireLabel runat="server" ID="lblAverage" ></aspire:AspireLabel>
                    </div>
                </div>
                <div class="form-group">
                    <aspire:AspireLabel runat="server" Text="Comment:" AssociatedControlID="tbComment" CssClass="col-md-3" />
                    <div class="col-md-9">
                        <aspire:AspireTextBox runat="server" ID="tbComment" ValidationGroup="Comments" TextMode="MultiLine" />
                        <aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbComment" ValidationGroup="Comments" ErrorMessage="This field is required." />
                    </div>
                </div>
            </div>
        </BodyTemplate>
        <FooterTemplate>
            <aspire:AspireButton runat="server" ID="btnSave" Text="Save" ValidationGroup="Comments" ButtonType="Primary" OnClick="btnSave_OnClick" />
            <%--<aspire:AspireButton runat="server" ID="btnSubmit" Text="Save and Submit" ValidationGroup="Comments" ButtonType="Primary" OnClick="btnSubmit_OnClick" />--%>
            <aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
        </FooterTemplate>
    </aspire:AspireModal>
</asp:Content>
