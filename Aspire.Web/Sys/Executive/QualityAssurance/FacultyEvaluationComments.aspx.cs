﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using FacultyEvaluationSummary = Aspire.BL.Core.QualityAssurance.Executive.Reports.FacultyEvaluationSummary;

namespace Aspire.Web.Sys.Executive.QualityAssurance
{
    [AspirePage("01C0B9D2-8D9C-41B6-B937-340785A61760", UserTypes.Executive, "~/Sys/Executive/QualityAssurance/FacultyEvaluationComments.aspx", "Comments for Faculty Evaluation", true, AspireModules.QualityAssurance)]
    public partial class FacultyEvaluationComments : ExecutivePage
    {
        public override string PageTitle => "Comments for Faculty Evaluation ";
        public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.commenting_o);

        protected override IReadOnlyDictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]>
        {
            {ExecutivePermissions.QualityAssurance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
        };
        public static string GetPageUrl(byte facultyType, short semesterID, int departmentID)
        {
            return GetAspirePageAttribute<FacultyEvaluationComments>().PageUrl.AttachQueryParams("FacultyType", facultyType, "SemesterID", semesterID, "DepartmentID", departmentID);
        }

        private void RefreshPage()
        {
            var facultyTypeEnum = this.ddlFacultyType.SelectedValue.ToEnum<FacultyMember.FacultyTypes>();
            var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
            var departmentID = this.ddlDepartmentID.SelectedValue.ToInt();
            this.Response.Redirect(GetPageUrl((byte)facultyTypeEnum, semesterID, departmentID));

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)

            {
                this.ddlFacultyType.FillEnums<FacultyMember.FacultyTypes>().SetEnumValue(FacultyMember.FacultyTypes.Permanent).SetSelectedValueIfNotPostback("FacultyType");
                this.ddlSemesterID.FillSemesters().SetSelectedValueIfNotPostback("SemesterID");
                this.ddlDepartmentID.FillDepartments(this.ExecutiveIdentity.InstituteID).SetSelectedValueIfNotPostback("DepartmentID"); 
                this.ViewState.SetSortExpression("Name");
                this.ViewState.SetSortDirection(SortDirection.Ascending);
            }
        }

        protected void btnDisplay_OnClick(object sender, EventArgs e)
        {
            this.GetData(0);

        }

        private int? DepartmentID => this.ddlDepartmentID.SelectedValue.ToNullableInt();
        private short SemesterID => this.ddlSemesterID.SelectedValue.ToShort();
        public string UserRole = string.Empty;
        private void GetData(int pageIndex)
        {
            var facultyTypeEnum = this.ddlFacultyType.SelectedValue.ToEnum<FacultyMember.FacultyTypes>();

            var surveyConductTypeEnum = SurveyConduct.SurveyConductTypes.BeforeMidTerm;
            var surveyConductIDBeforeMidTerm = FacultyEvaluationSummary.GetSurveyConducts(this.ExecutiveIdentity.InstituteID, this.SemesterID, Survey.SurveyTypes.StudentCourseWise, surveyConductTypeEnum);
            surveyConductTypeEnum = SurveyConduct.SurveyConductTypes.BeforeFinalTerm;
            var surveyConductIDBeforeFinalTerm = FacultyEvaluationSummary.GetSurveyConducts(this.ExecutiveIdentity.InstituteID, this.SemesterID, Survey.SurveyTypes.StudentCourseWise, surveyConductTypeEnum);
            this.SurveyConductID.Value = surveyConductIDBeforeFinalTerm.ToString();
            int virtualItemCount;
            if (this.ExecutiveIdentity.Username.ToLower().Contains("hod"))
            {
                this.UserRole = "HOD";
            }
            else if (this.ExecutiveIdentity.Username.ToLower().Contains("dir"))
                this.UserRole = "Director";
            else if (this.ExecutiveIdentity.Username.ToLower().Contains("dg"))
                this.UserRole = "DG";
            //else
            this.ViewState["Role"] = this.UserRole;
            var users = FacultyEvaluationSummary.EvaluationSummary(this.DepartmentID, surveyConductIDBeforeMidTerm ?? 0, surveyConductIDBeforeFinalTerm ?? 0, facultyTypeEnum, this.UserRole, pageIndex, this.gvFacultySummary.PageSize,  this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out virtualItemCount, this.ExecutiveIdentity.LoginSessionGuid);
            this.gvFacultySummary.DataBind(users, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
            this.btnLockComments.Visible = users.Count > 0;
            if (this.UserRole == "HOD")
            {
                this.gvFacultySummary.Columns[9].Visible = false;
                this.gvFacultySummary.Columns[10].Visible = false;
            }
            if (this.UserRole == "Director")
                this.gvFacultySummary.Columns[10].Visible = false;
        }

        protected void gvFacultySummary_OnDataBound(object sender, EventArgs e)
        {
            for (var i = this.gvFacultySummary.Rows.Count - 1; i > 0; i--)
            {
                var row = this.gvFacultySummary.Rows[i];
                var previousRow = this.gvFacultySummary.Rows[i - 1];
                if (row.Cells[0].Text == previousRow.Cells[0].Text)
                {
                    if (previousRow.Cells[0].RowSpan == 0)
                    {
                        if (row.Cells[0].RowSpan == 0)
                        {
                            previousRow.Cells[0].RowSpan += 2;
                            previousRow.Cells[4].RowSpan += 2;
                            previousRow.Cells[6].RowSpan += 2;
                            previousRow.Cells[7].RowSpan += 2;
                            previousRow.Cells[8].RowSpan += 2;
                            previousRow.Cells[9].RowSpan += 2;
                            previousRow.Cells[10].RowSpan += 2;
                            previousRow.Cells[11].RowSpan += 2;
                        }
                        else
                        {
                            previousRow.Cells[0].RowSpan = row.Cells[0].RowSpan + 1;
                            previousRow.Cells[4].RowSpan = row.Cells[4].RowSpan + 1;
                            previousRow.Cells[6].RowSpan = row.Cells[6].RowSpan + 1;
                            previousRow.Cells[7].RowSpan = row.Cells[7].RowSpan + 1;
                            previousRow.Cells[8].RowSpan = row.Cells[8].RowSpan + 1;
                            previousRow.Cells[9].RowSpan = row.Cells[9].RowSpan + 1;
                            previousRow.Cells[10].RowSpan = row.Cells[10].RowSpan + 1;
                            previousRow.Cells[11].RowSpan = row.Cells[11].RowSpan + 1;
                        }
                        row.Cells[0].Visible = false;
                        row.Cells[4].Visible = false;
                        row.Cells[6].Visible = false;
                        row.Cells[7].Visible = false;
                        row.Cells[8].Visible = false;
                        row.Cells[9].Visible = false;
                        row.Cells[10].Visible = false;
                        row.Cells[11].Visible = false;
                    }
                }
            }
        }

        protected void gvFacultySummary_OnSorting(object sender, GridViewSortEventArgs e)
        {
            e.ProcessGridViewSortingEventArgs(this.ViewState);
            this.GetData(this.gvFacultySummary.PageIndex);
        }

        protected void gvFacultySummary_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.GetData(e.NewPageIndex);
        }

        protected void gvFacultySummary_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
        {
            this.gvFacultySummary.PageSize = e.NewPageSize;
            this.GetData(0);
        }
        protected void gvFacultySummary_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            this.UserRole = this.ViewState["Role"].ToString();
            switch (e.CommandName)
            {
                case "Add":
                    e.Handled = true;
                    var rowIndex = e.DecryptedCommandArgumentToInt();
                    this.lblName.Text = this.gvFacultySummary.Rows[rowIndex].Cells[0].Text;
                    this.lblAverage.Text = this.gvFacultySummary.Rows[rowIndex].Cells[7].Text;
                    this.lblsurveyID.Value = this.SurveyConductID.Value;
                    this.lblfacultyID.Value = ((Lib.WebControls.HiddenField)this.gvFacultySummary.Rows[rowIndex].Cells[12].FindControl("FacultyID")).Value;
                    this.lbldepartmentID.Value = ((Lib.WebControls.HiddenField)this.gvFacultySummary.Rows[rowIndex].Cells[13].FindControl("DepartmentID")).Value;
                    this.lblmidsAvg.Value = this.gvFacultySummary.Rows[rowIndex].Cells[4].Text;
                    this.lblfinalAvg.Value = this.gvFacultySummary.Rows[rowIndex].Cells[6].Text;
                    if (this.UserRole =="HOD")
                        this.tbComment.Text = this.gvFacultySummary.Rows[rowIndex].Cells[8].Text.HtmlDecode();
                    else if (this.UserRole == "Director")
                        this.tbComment.Text = this.gvFacultySummary.Rows[rowIndex].Cells[9].Text.HtmlDecode();
                    else if (this.UserRole == "DG")
                        this.tbComment.Text = this.gvFacultySummary.Rows[rowIndex].Cells[10].Text.HtmlDecode();
                    this.ModelPopup.Show();
                    break;
            }

        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            var surveyConductID = this.lblsurveyID.Value.ToInt32();
            var facultyID = this.lblfacultyID.Value.ToInt32();
            var departmentID = this.lbldepartmentID.Value.ToInt32();
            var midsavg = this.lblmidsAvg.Value.ToDouble();
            var finalavg = this.lblfinalAvg.Value.ToDouble();
            var remarks = this.tbComment.Text;
            this.UserRole = this.ViewState["Role"].ToString();

            var result = FacultyEvaluationSummary.SaveRemarks(surveyConductID, facultyID, departmentID, midsavg, finalavg, this.UserRole, remarks, this.ExecutiveIdentity.LoginSessionGuid);
            switch (result)
            {
                case FacultyEvaluationSummary.SaveRemarksStatuses.AddSuccess:
                    this.AddSuccessMessageHasBeenAdded("Comment");
                    break;
                case FacultyEvaluationSummary.SaveRemarksStatuses.UpdateSuccess:
                    this.AddSuccessMessageHasBeenUpdated("Comment");
                    break;
                case FacultyEvaluationSummary.SaveRemarksStatuses.AlreadyLocked:
                    this.AddErrorAlert("Comment is already locked.");
                    break;
                case FacultyEvaluationSummary.SaveRemarksStatuses.InvalidRole:
                    this.AddErrorAlert("User role not found.");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            this.ModelPopup.Hide();
            this.GetData(0);
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            var sid = this.lblsurveyID.Value.ToInt32();
            var facultyID = this.lblfacultyID.Value.ToInt32();
            var departmentID = this.lbldepartmentID.Value.ToInt32();
            var midsavg = this.lblmidsAvg.Value.ToDouble();
            var finalavg = this.lblfinalAvg.Value.ToDouble();
            var remarks = this.tbComment.Text;
            var identity = this.ExecutiveIdentity.LoginSessionGuid;
            this.UserRole = this.ViewState["Role"].ToString();

            var response = FacultyEvaluationSummary.SaveRemarks(sid, facultyID, departmentID, midsavg, finalavg, this.UserRole, remarks, identity);
            switch (response)
            {
                case FacultyEvaluationSummary.SaveRemarksStatuses.AddSuccess:
                    this.AddSuccessMessageHasBeenAdded("Comment");
                    break;
                case FacultyEvaluationSummary.SaveRemarksStatuses.AlreadyLocked:
                    this.AddErrorAlert("Comment  is already locked.");
                    break;
                case FacultyEvaluationSummary.SaveRemarksStatuses.UpdateSuccess:
                    this.AddSuccessMessageHasBeenUpdated("Comment");
                    break;
                case FacultyEvaluationSummary.SaveRemarksStatuses.InvalidRole:
                    this.AddErrorAlert("User role not found.");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            this.RefreshPage();
        }

        protected void btnReport_OnClick(object sender, EventArgs e)
        {
            Reports.FacultyEvaluationSummary.Redirect();
        }
        public static string GetPageUrl()
        {
            return GetPageUrl<FacultyEvaluationComments>();
        }

        public static void Redirect()
        {
            Redirect(GetPageUrl());
        }

        protected void btnLockComments_OnClick(object sender, EventArgs e)
        {
            this.UserRole = this.ViewState["Role"].ToString();
            var colNum = 0;
            if (this.UserRole == "HOD")
            {  colNum = 8; }
            else if (this.UserRole == "Director")
            {  colNum = 9; }
            else if (this.UserRole == "DG")
            {  colNum = 10; }
            var previousRemarksID = 0;
            for (int i = 0; i < this.gvFacultySummary.Rows.Count; i++)
            {
                var remarksID = ((Lib.WebControls.HiddenField)this.gvFacultySummary.Rows[i].Cells[14].FindControl("RemarksID")).Value.ToInt32();
                if (remarksID != 0 && this.gvFacultySummary.Rows[i].Cells[colNum].Text != "&nbsp;")
                {
                    var remarks = this.gvFacultySummary.Rows[i].Cells[colNum].Text;
                    var response = FacultyEvaluationSummary.LockRemarks(remarksID, this.UserRole, remarks, this.ExecutiveIdentity.LoginSessionGuid);
                    if (previousRemarksID != remarksID)
                    {
                        switch (response)
                        {
                            case FacultyEvaluationSummary.SaveRemarksStatuses.UpdateSuccess:
                                this.AddSuccessAlert("Comment about " + this.gvFacultySummary.Rows[i].Cells[0].Text + " has been locked");
                                break;
                            case FacultyEvaluationSummary.SaveRemarksStatuses.InvalidRole:
                                this.AddErrorAlert("User role not found.");
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                        previousRemarksID = remarksID;
                    }
                }
            }
        }

    }
}