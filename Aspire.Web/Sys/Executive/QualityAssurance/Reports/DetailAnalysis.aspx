﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="DetailAnalysis.aspx.cs" Inherits="Aspire.Web.Sys.Executive.QualityAssurance.Reports.DetailAnalysis" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<asp:UpdatePanel runat="server" ID="updatePanelParameters" UpdateMode="Conditional">
		<ContentTemplate>
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="Parameters" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Survey:" AssociatedControlID="ddlSurveyID" />
					<aspire:AspireDropDownList runat="server" ID="ddlSurveyID" ValidationGroup="Parameters" />
				</div>
				<div class="form-group" runat="server">
					<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
					<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlDepartmentID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group" runat="server">
					<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
					<aspire:AspireDropDownList runat="server" ID="ddlProgramID" AutoPostBack="True" ValidationGroup="Parameters" />
				</div>
				<div class="form-group" runat="server">
					<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="ddlSemesterNo" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" ValidationGroup="Parameters" />
				</div>
				<div class="form-group" runat="server">
					<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSection" />
					<aspire:AspireDropDownList runat="server" ID="ddlSection" ValidationGroup="Parameters" />
				</div>
				<div class="form-group" runat="server">
					<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShift" />
					<aspire:AspireDropDownList runat="server" ID="ddlShift" ValidationGroup="Parameters" />
				</div>
				<div class="form-group" runat="server">
					<aspire:AspireLabel runat="server" Text="Faculty Member:" AssociatedControlID="ddlFacultyMemberID" />
					<aspire:AspireDropDownList runat="server" ID="ddlFacultyMemberID" ValidationGroup="Parameters" />
				</div>
				<div class="form-group" runat="server">
					<aspire:AspireButton runat="server" ButtonType="Primary" ValidationGroup="Parameters" ID="btnDisplay" Text="Display" OnClick="btnDisplay_OnClick" />
				</div>
			</div>
		</ContentTemplate>
		<Triggers>
			<asp:PostBackTrigger ControlID="btnDisplay" />
		</Triggers>
	</asp:UpdatePanel>
	<script type="text/javascript">
		$(function () {
			function init() {
				$("#<%=this.ddlFacultyMemberID.ClientID%>").select2({ theme: "bootstrap" });
			}
			init();
			Sys.Application.add_load(init);
		});
	</script>
</asp:Content>
