﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="FacultyEvaluationSummary.aspx.cs" Inherits="Aspire.Web.Sys.Executive.QualityAssurance.Reports.FacultyEvaluationSummary" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Parameters" runat="server">
    <div class="form-inline">
        <div class="form-group">
            <aspire:AspireButton runat="server" ID="btnComments" Text="Add Comments" Glyphicon="plus" ButtonType="Success" ValidationGroup="Paramters" OnClick="btnComments_OnClick" />
            <aspire:HiddenField runat="server" ID="HiddenField1" />
        </div>
        <div class="form-group">
            <aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
            <aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="Parameters" />
        </div>
        <div class="form-group">
            <aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
            <aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" ValidationGroup="Parameters" />
        </div>
        <div class="form-group">
            <aspire:AspireButton runat="server" ID="btnDisplay" Text="Display" ValidationGroup="Paramters" OnClick="btnDisplay_OnClick" />
            <aspire:HiddenField runat="server" ID="Survid" />
        </div>
    </div>
</asp:Content>
