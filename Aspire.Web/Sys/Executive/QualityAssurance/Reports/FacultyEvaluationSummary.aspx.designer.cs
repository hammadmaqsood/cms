﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Web.Sys.Executive.QualityAssurance.Reports {
    
    
    public partial class FacultyEvaluationSummary {
        
        /// <summary>
        /// btnComments control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireButton btnComments;
        
        /// <summary>
        /// HiddenField1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.HiddenField HiddenField1;
        
        /// <summary>
        /// ddlSemesterID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireDropDownList ddlSemesterID;
        
        /// <summary>
        /// ddlDepartmentID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireDropDownList ddlDepartmentID;
        
        /// <summary>
        /// btnDisplay control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireButton btnDisplay;
        
        /// <summary>
        /// Survid control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.HiddenField Survid;
    }
}
