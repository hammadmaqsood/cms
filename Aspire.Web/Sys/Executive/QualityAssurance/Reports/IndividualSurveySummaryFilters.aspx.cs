﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Executive.QualityAssurance.Reports
{
	[AspirePage("01ECDCF1-B2C2-478C-8E02-B09F74B4DBC6", UserTypes.Executive, "~/Sys/Executive/QualityAssurance/Reports/IndividualSurveySummaryFilters.aspx", "Individual Survey Summary Filters", true, AspireModules.QualityAssurance)]
	public partial class IndividualSurveySummaryFilters : ExecutivePage
	{
		protected override IReadOnlyDictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ExecutivePermissions.QualityAssurance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_book);
		public override string PageTitle => "Individual Survey Summary";
		public string ReportName => this.PageTitle;
		public bool ExportToExcel => true;
		public bool ExportToWord => false;

		public short SemesterID => this.ddlSemesterID.SelectedValue.ToShort();
		public Survey.SurveyTypes SurveyType => this.ddlSurveyType.GetSelectedEnumValue<Survey.SurveyTypes>();
		public SurveyConduct.SurveyConductTypes SurveyConductType => this.ddlSurveyConductType.GetSelectedEnumValue<SurveyConduct.SurveyConductTypes>();
		public int? SurveyConductID => this.ddlSurveyConductID.SelectedValue.ToNullableInt();
		public int? DepartmentID => this.divDepartmentID.Visible ? this.ddlDepartmentID.SelectedValue.ToNullableInt() : null;
		public int? ProgramID => this.divProgramID.Visible ? this.ddlProgramID.SelectedValue.ToNullableInt() : null;
		public SemesterNos? SemesterNo => this.divSemesterNo.Visible ? this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null) : null;
		public Sections? Section => this.divSection.Visible ? this.ddlSection.GetSelectedEnumValue<Sections>(null) : null;
		public Shifts? Shift => this.divShift.Visible ? this.ddlShift.GetSelectedEnumValue<Shifts>(null) : null;
		public int? FacultyMemberID => this.divFacultyMemberID.Visible ? this.ddlFacultyMemberID.SelectedValue.ToNullableInt() : null;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Class", SortDirection.Ascending);
				this.ddlSemesterID.FillSemesters();
				this.ddlSurveyType.FillSurveyTypes();
				this.ddlSurveyConductType.FillSurveyConductTypes();
				this.ddlSemesterNo.FillSemesterNos(CommonListItems.All);
				this.ddlSection.FillSections(CommonListItems.All);
				this.ddlShift.FillShifts(CommonListItems.All);
				var facultyMembers = Aspire.BL.Core.Common.Lists.GetFacultyMembersList(this.ExecutiveIdentity.InstituteID, null);
				this.ddlFacultyMemberID.DataBind(facultyMembers, CommonListItems.All);
				this.ddlDepartmentID.FillDepartments(this.ExecutiveIdentity.InstituteID, CommonListItems.All);
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSurveyType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSurveyType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			switch (this.SurveyType)
			{
				case Survey.SurveyTypes.StudentWise:
					this.divDepartmentID.Visible = true;
					this.divProgramID.Visible = true;
					this.divSemesterNo.Visible = true;
					this.divSection.Visible = true;
					this.divShift.Visible = true;
					this.divFacultyMemberID.Visible = false;
					break;
				case Survey.SurveyTypes.StudentCourseWise:
					this.divDepartmentID.Visible = true;
					this.divProgramID.Visible = true;
					this.divSemesterNo.Visible = true;
					this.divSection.Visible = true;
					this.divShift.Visible = true;
					this.divFacultyMemberID.Visible = true;
					break;
				case Survey.SurveyTypes.FacultyMemberWise:
					this.divDepartmentID.Visible = true;
					this.divProgramID.Visible = false;
					this.divSemesterNo.Visible = false;
					this.divSection.Visible = false;
					this.divShift.Visible = false;
					this.divFacultyMemberID.Visible = true;
					break;
				case Survey.SurveyTypes.FacultyMemberCourseWise:
					this.divDepartmentID.Visible = true;
					this.divProgramID.Visible = true;
					this.divSemesterNo.Visible = true;
					this.divSection.Visible = true;
					this.divShift.Visible = true;
					this.divFacultyMemberID.Visible = true;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.ddlSurveyConductType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSurveyConductType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var surveyConducts = Aspire.BL.Core.QualityAssurance.Executive.Reports.Common.GetSurveyConducts(this.ExecutiveIdentity.InstituteID, this.SemesterID, this.SurveyType, this.SurveyConductType);
			this.ddlSurveyConductID.DataBind(surveyConducts);
			this.ddlSurveyConductID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSurveyConductID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.divProgramID.Visible)
			{
				var programs = Aspire.BL.Core.Common.Lists.GetPrograms(this.ExecutiveIdentity.InstituteID, this.DepartmentID, true);
				this.ddlProgramID.DataBind(programs, CommonListItems.All);
			}
			else
				this.ddlProgramID.Items.Clear();
			this.ddlProgramID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterNo_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterNo_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSection_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSection_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlShift_OnSelectedIndexChanged(null, null);
		}

		protected void ddlShift_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlFacultyMemberID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlFacultyMemberID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData();
		}

		protected void gvCourses_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetCourses(0);
		}

		protected void gvCourses_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetCourses(e.NewPageIndex);
		}

		protected void gvCourses_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvCourses.PageSize = e.NewPageSize;
			this.GetCourses(0);
		}

		private void GetData()
		{
			if (this.SurveyConductID == null)
			{
				this.panelCourses.Visible = false;
				this.hlLink.Visible = false;
				return;
			}
			this.hlLink.Visible = this.DepartmentID.HasValue;
			this.hlLink.NavigateUrl = IndividualSurveySummary.GetPageUrl(this.SurveyConductID.Value, this.DepartmentID, this.ProgramID, this.SemesterNo, this.Section, this.Shift, null, null, null, this.FacultyMemberID);
			switch (this.SurveyType)
			{
				case Survey.SurveyTypes.StudentWise:
				case Survey.SurveyTypes.FacultyMemberWise:
					this.panelCourses.Visible = false;
					break;
				case Survey.SurveyTypes.StudentCourseWise:
				case Survey.SurveyTypes.FacultyMemberCourseWise:
					this.panelCourses.Visible = true;
					this.GetCourses(0);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private void GetCourses(int pageIndex)
		{
			int virtualItemCount;
			var offeredCourses = Aspire.BL.Core.CourseOffering.Executive.OfferedCourses.GetOfferedCourses(this.SemesterID, this.DepartmentID, this.ProgramID, this.SemesterNo, this.Section, this.Shift, this.FacultyMemberID, null, null, pageIndex, this.gvCourses.PageSize, out virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.ExecutiveIdentity.LoginSessionGuid);
			var list = offeredCourses.Select(oc => new
			{
				oc.OfferedCourseID,
				oc.Title,
				oc.FacultyMemberName,
				oc.DepartmentName,
				oc.Class,
				oc.Semester,
				Link = IndividualSurveySummary.GetPageUrl(this.SurveyConductID.Value, oc.DepartmentID, oc.ProgramID, oc.SemesterNoEnum, oc.SectionEnum, oc.ShiftEnum, null, null, oc.OfferedCourseID, oc.FacultyMemberID),
			}).ToList();
			this.gvCourses.DataBind(list, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}
	}
}