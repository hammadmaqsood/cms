﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="IndividualSurveySummaryFilters.aspx.cs" Inherits="Aspire.Web.Sys.Executive.QualityAssurance.Reports.IndividualSurveySummaryFilters" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Survey Type:" AssociatedControlID="ddlSurveyType" />
			<aspire:AspireDropDownList runat="server" ID="ddlSurveyType" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlSurveyType_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Survey Conduct Type:" AssociatedControlID="ddlSurveyConductType" />
			<aspire:AspireDropDownList runat="server" ID="ddlSurveyConductType" ValidationGroup="Parameters" AutoPostBack="True" OnSelectedIndexChanged="ddlSurveyConductType_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Survey:" AssociatedControlID="ddlSurveyConductID" />
			<aspire:AspireDropDownList runat="server" ID="ddlSurveyConductID" ValidationGroup="Parameters" AutoPostBack="True" OnSelectedIndexChanged="ddlSurveyConductID_OnSelectedIndexChanged" />
		</div>
		<div class="form-group" runat="server" id="divDepartmentID">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlDepartmentID_OnSelectedIndexChanged" />
		</div>
		<div class="form-group" runat="server" id="divProgramID">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramID" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlProgramID_OnSelectedIndexChanged" />
		</div>
		<div class="form-group" runat="server" id="divSemesterNo">
			<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="ddlSemesterNo" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterNo" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlSemesterNo_OnSelectedIndexChanged" />
		</div>
		<div class="form-group" runat="server" id="divSection">
			<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSection" />
			<aspire:AspireDropDownList runat="server" ID="ddlSection" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlSection_OnSelectedIndexChanged" />
		</div>
		<div class="form-group" runat="server" id="divShift">
			<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShift" />
			<aspire:AspireDropDownList runat="server" ID="ddlShift" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlShift_OnSelectedIndexChanged" />
		</div>
		<div class="form-group" runat="server" id="divFacultyMemberID">
			<aspire:AspireLabel runat="server" Text="Faculty Member:" AssociatedControlID="ddlFacultyMemberID" />
			<aspire:AspireDropDownList runat="server" ID="ddlFacultyMemberID" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlFacultyMemberID_OnSelectedIndexChanged" />
		</div>
	</div>
	<p></p>
	<asp:Panel runat="server" ID="panelCourses">
		<aspire:AspireGridView runat="server" AllowSorting="True" ID="gvCourses" AutoGenerateColumns="False" OnSorting="gvCourses_OnSorting" OnPageIndexChanging="gvCourses_OnPageIndexChanging" OnPageSizeChanging="gvCourses_OnPageSizeChanging">
			<Columns>
				<asp:TemplateField HeaderText="#">
					<ItemTemplate><%#: Container.DataItemIndex + 1 %></ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="Semester" DataField="Semester" />
				<asp:BoundField HeaderText="Department" DataField="DepartmentName" />
				<asp:BoundField HeaderText="Title" DataField="Title" SortExpression="Title" />
				<asp:BoundField HeaderText="Class" DataField="Class" SortExpression="Class" />
				<asp:BoundField HeaderText="Faculty Member" DataField="FacultyMemberName" SortExpression="FacultyMemberName" />
				<asp:TemplateField HeaderText="Action">
					<ItemTemplate>
						<aspire:AspireHyperLink Text="View Summary" Target="_blank" runat="server" NavigateUrl='<%# Eval("Link") %>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
	</asp:Panel>
	<div class="text-center">
		<aspire:AspireHyperLinkButton Target="_blank" ToolTip="Overall Summary according to above filters" runat="server" ID="hlLink" Glyphicon="print" Text="View Summary" />
	</div>
	<script type="text/javascript">
		$(function () {
			$("#<%=this.ddlFacultyMemberID.ClientID%>").select2({ theme: "bootstrap" });
		});
	</script>
</asp:Content>
