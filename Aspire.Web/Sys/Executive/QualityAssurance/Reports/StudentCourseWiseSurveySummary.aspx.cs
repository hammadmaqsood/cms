﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Executive.QualityAssurance.Reports
{
	[AspirePage("2509D433-1CE7-474B-B63A-7BCC70F24650", UserTypes.Executive, "~/Sys/Executive/QualityAssurance/Reports/StudentCourseWiseSurveySummary.aspx", "Student Course-Wise Survey Summary", true, AspireModules.QualityAssurance)]
	public partial class StudentCourseWiseSurveySummary : ExecutivePage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_book);
		public override string PageTitle => "Student CourseWise Survey Summary";
		public string ReportName => "Student CourseWise Survey Summary";
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		protected override IReadOnlyDictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ExecutivePermissions.QualityAssurance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterID.FillSemesters();
				this.ddlDepartmentID.FillDepartments(this.ExecutiveIdentity.InstituteID, CommonListItems.All);
				this.ddlSurveyConductType.FillSurveyConductTypes();
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}
		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSurveyConductType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSurveyConductType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var surveyConductType = this.ddlSurveyConductType.GetSelectedEnumValue<SurveyConduct.SurveyConductTypes>();
			var surveyConducts = Aspire.BL.Core.QualityAssurance.Executive.Reports.Common.GetSurveyConducts(this.ExecutiveIdentity.InstituteID, this.SemesterID, Survey.SurveyTypes.StudentCourseWise, surveyConductType);
			this.ddlSurveyConductID.DataBind(surveyConducts);
		}

		private bool _execute = false;
		protected void btnDisplay_OnClick(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this._execute = true;
			this.RenderReport(reportView.ReportViewer);
			this._execute = false;
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this._execute == false)
				return;
			reportViewer.LocalReport.DataSources.Clear();
			if (this.SurveyConductID == null)
				return;
			var reportFileName = "~/Reports/QualityAssurance/Executive/StudentCourseWiseSurveySummary.rdlc".MapPath();
			var reportDataSet = Aspire.BL.Core.QualityAssurance.Executive.Reports.StudentCourseWiseSurveys.StudentCourseWiseSurveySummary(this.DepartmentID, this.SurveyConductID.Value, this.ExecutiveIdentity.LoginSessionGuid);
			if (reportDataSet == null)
				return;
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.Survey), reportDataSet.Survey));
			reportViewer.LocalReport.EnableExternalImages = true;
		}

		private short SemesterID => this.ddlSemesterID.SelectedValue.ToShort();
		private int? DepartmentID => this.ddlDepartmentID.SelectedValue.ToNullableInt();
		private int? SurveyConductID => this.ddlSurveyConductID.SelectedValue.ToNullableInt();
	}
}