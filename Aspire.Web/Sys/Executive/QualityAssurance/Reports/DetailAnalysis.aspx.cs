﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Executive.QualityAssurance.Reports
{
	[AspirePage("99C5AD31-2495-4531-9E83-C242F8381C1F", UserTypes.Executive, "~/Sys/Executive/QualityAssurance/Reports/DetailAnalysis.aspx", "Detail Analysis", true, AspireModules.QualityAssurance)]
	public partial class DetailAnalysis : ExecutivePage, IReportViewer
	{
		protected override IReadOnlyDictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ExecutivePermissions.QualityAssurance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_book);
		public override string PageTitle => "Detail Analysis";
		public string ReportName => this.PageTitle;
		public bool ExportToExcel => true;
		public bool ExportToWord => false;

		public short SemesterID => this.ddlSemesterID.SelectedValue.ToShort();
		public int? SurveyID => this.ddlSurveyID.SelectedValue.ToNullableInt();
		public int? DepartmentID => this.ddlDepartmentID.SelectedValue.ToNullableInt();
		public int? ProgramID => this.ddlProgramID.SelectedValue.ToNullableInt();
		public SemesterNos? SemesterNo => this.ddlSemesterNo.GetSelectedEnumValue<SemesterNos>(null);
		public Sections? Section => this.ddlSection.GetSelectedEnumValue<Sections>(null);
		public Shifts? Shift => this.ddlShift.GetSelectedEnumValue<Shifts>(null);
		public int? FacultyMemberID => this.ddlFacultyMemberID.SelectedValue.ToNullableInt();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Class", SortDirection.Ascending);
				this.ddlSemesterID.FillSemesters();
				var surveys = BL.Core.QualityAssurance.Executive.Reports.Common.GetSurveys(this.ExecutiveIdentity.InstituteID, Survey.SurveyTypes.StudentCourseWise);
				this.ddlSurveyID.DataBind(surveys);
				this.ddlDepartmentID.FillDepartments(this.ExecutiveIdentity.InstituteID, CommonListItems.All);
				this.ddlSemesterNo.FillSemesterNos(CommonListItems.All);
				this.ddlSection.FillSections(CommonListItems.All);
				this.ddlShift.FillShifts(CommonListItems.All);
				var facultyMembers = BL.Core.Common.Lists.GetFacultyMembersList(this.ExecutiveIdentity.InstituteID, null);
				this.ddlFacultyMemberID.DataBind(facultyMembers, CommonListItems.All);
				this.ddlDepartmentID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlDepartmentID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var programs = BL.Core.Common.Lists.GetPrograms(this.ExecutiveIdentity.InstituteID, this.DepartmentID, true);
			this.ddlProgramID.DataBind(programs, CommonListItems.All);
		}

		private bool _execute ;
		protected void btnDisplay_OnClick(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this._execute = true;
			this.RenderReport(reportView.ReportViewer);
			this._execute = false;
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			var reportView = (ReportView)this.Master;
			reportView.ResetReport();
			if (this._execute == false || this.SurveyID == null)
				return;
			var reportFileName = "~/Reports/QualityAssurance/Executive/DetailAnalysis.rdlc".MapPath();
			var reportDataSet = BL.Core.QualityAssurance.Executive.Reports.DetailAnalysisFacultyEvaluation.GetDetailedAnalysis(this.SurveyID.Value, this.SemesterID, this.DepartmentID, this.ProgramID, this.SemesterNo, this.Section, this.Shift, this.FacultyMemberID, this.ExecutiveIdentity.LoginSessionGuid);
			if (reportDataSet == null)
				return;
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.Survey), reportDataSet.Survey));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}