﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Ajax.Utilities;
using Microsoft.Reporting.WebForms;

namespace Aspire.Web.Sys.Executive.QualityAssurance.Reports
{
    [AspirePage("07E15597-E8BC-4881-A83F-C0A96F787042", UserTypes.Executive, "~/Sys/Executive/QualityAssurance/Reports/FacultyEvaluationSummary.aspx", "Summary of Faculty Evaluation", true, AspireModules.QualityAssurance)]
    public partial class FacultyEvaluationSummary : ExecutivePage, IReportViewer
    {
        protected override IReadOnlyDictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]>
        {
            {ExecutivePermissions.QualityAssurance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
        };
        public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.book);
        public override string PageTitle => "Summary of Faculty Evaluation";
        public string ReportName => this.PageTitle;
        public bool ExportToExcel => true;
        public bool ExportToWord => true;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.ddlSemesterID.FillSemesters();
                this.ddlDepartmentID.FillDepartments(this.ExecutiveIdentity.InstituteID);
                this.ViewState.SetSortExpression("Name");
                this.ViewState.SetSortDirection(SortDirection.Ascending);
            }
        }
        private bool _execute = false;
        protected void btnDisplay_OnClick(object sender, EventArgs e)
        {
            var reportView = (ReportView)this.Master;
            this._execute = true;
            this.RenderReport(reportView.ReportViewer);
            this._execute = false;
        }
        public static string GetPageUrl()
        {
            return GetPageUrl<FacultyEvaluationSummary>();
        }

        public static void Redirect()
        {
            Redirect(GetPageUrl());
        }
        public void RenderReport(ReportViewer reportViewer)
        {
            if (this._execute == false)
                return;
            reportViewer.LocalReport.DataSources.Clear();

            var surveyConductType = SurveyConduct.SurveyConductTypes.BeforeMidTerm;
            var surveyConductID1 = BL.Core.QualityAssurance.Executive.Reports.FacultyEvaluationSummary.GetSurveyConducts(this.ExecutiveIdentity.InstituteID, this.SemesterID, Survey.SurveyTypes.StudentCourseWise, surveyConductType);
            surveyConductType = SurveyConduct.SurveyConductTypes.BeforeFinalTerm;
            var surveyConductID2 = BL.Core.QualityAssurance.Executive.Reports.FacultyEvaluationSummary.GetSurveyConducts(this.ExecutiveIdentity.InstituteID, this.SemesterID, Survey.SurveyTypes.StudentCourseWise, surveyConductType);
            this.Survid.Value = surveyConductID2.ToString();
            this.ddlDepartmentID.GetSelectedEnumValue<User.Statuses>(null);
            reportViewer.LocalReport.DataSources.Clear();
            if (surveyConductID1 == null || surveyConductID2 == null )
                return;
            var reportFileName = "~/Reports/QualityAssurance/Executive/FacultyEvaluationSummary.rdlc".MapPath();
            string role = "";
            Guid identity = this.ExecutiveIdentity.LoginSessionGuid;
            if (this.ExecutiveIdentity.Username.ToLower().Contains("hod"))
                role = "HOD";
            else if (this.ExecutiveIdentity.Username.ToLower().Contains("dir"))
                role = "Director";
            else if (this.ExecutiveIdentity.Username.ToLower().Contains("dg"))
                role = "DG";
            var reportDataSet = BL.Core.QualityAssurance.Executive.Reports.FacultyEvaluationSummary.EvaluationSummaryReport(this.DepartmentID, surveyConductID1 ?? 0, surveyConductID2 ?? 0, role, this.ExecutiveIdentity.LoginSessionGuid);

            if (reportDataSet == null)
                return;
            reportViewer.LocalReport.DataSources.Clear();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = reportFileName;
            reportViewer.LocalReport.DisplayName = this.ReportName;
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.QAPageHeader), new[] { reportDataSet.QAPageHeader }));
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.QAEvaluationSummary), reportDataSet.QAEvaluationSummary));
            reportViewer.LocalReport.EnableExternalImages = true;
            reportViewer.LocalReport.Refresh();
        }
        private int? DepartmentID => this.ddlDepartmentID.SelectedValue.ToNullableInt();
        private short SemesterID => this.ddlSemesterID.SelectedValue.ToShort();

        protected void btnComments_OnClick(object sender, EventArgs e)
        {
            Aspire.Web.Sys.Executive.QualityAssurance.FacultyEvaluationComments.Redirect();
        }
    }
}