﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Executive.QualityAssurance.Reports
{
	[AspirePage("C712748D-912E-43E9-BC45-DD10E94A4518", UserTypes.Executive, "~/Sys/Executive/QualityAssurance/Reports/IndividualSurveySummary.aspx", "Individual Survey Summary", true, AspireModules.QualityAssurance)]
	public partial class IndividualSurveySummary : ExecutivePage, IReportViewer
	{
		protected override IReadOnlyDictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ExecutivePermissions.QualityAssurance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_book);
		public override string PageTitle => "Individual Survey Summary";

		public string ReportName => this.PageTitle;
		public bool ExportToExcel => true;
		public bool ExportToWord => false;

		public int? SurveyConductID => this.Request.GetParameterValue<int>(nameof(this.SurveyConductID));
		public int? DepartmentID => this.Request.GetParameterValue<int>(nameof(this.DepartmentID));
		public int? ProgramID => this.Request.GetParameterValue<int>(nameof(this.ProgramID));
		public SemesterNos? SemesterNo => this.Request.GetParameterValue<SemesterNos>(nameof(this.SemesterNo));
		public Sections? Section => this.Request.GetParameterValue<Sections>(nameof(this.Section));
		public Shifts? Shift => this.Request.GetParameterValue<Shifts>(nameof(this.Shift));
		public int? FacultyMemberID => this.Request.GetParameterValue<int>(nameof(this.FacultyMemberID));
		public int? OfferedCourseID => this.Request.GetParameterValue<int>(nameof(this.OfferedCourseID));
		public int? RegisteredCourseID => this.Request.GetParameterValue<int>(nameof(this.RegisteredCourseID));
		public int? StudentID => this.Request.GetParameterValue<int>(nameof(this.StudentID));

		public static string GetPageUrl(int surveyConductID, int? departmentID, int? programID, SemesterNos? semesterNoEnum, Sections? sectionEnum, Shifts? shiftEnum, int? studentID, int? registeredCourseID, int? offeredCourseID, int? facultyMemberID)
		{
			return GetPageUrl<IndividualSurveySummary>().AttachQueryParams(new Dictionary<string, object>
			{
				{nameof(SurveyConductID), surveyConductID},
				{nameof(DepartmentID), departmentID},
				{nameof(ProgramID), programID},
				{nameof(SemesterNo), semesterNoEnum},
				{nameof(Section), sectionEnum},
				{nameof(Shift), shiftEnum},
				{nameof(StudentID), studentID},
				{nameof(RegisteredCourseID), registeredCourseID},
				{nameof(OfferedCourseID), offeredCourseID},
				{nameof(FacultyMemberID), facultyMemberID},
				//{ReportView.ReportFormat, ReportFormats.PDF},
				//{ReportView.Inline, true}
			});
		}

		protected void Page_Load(object sender, EventArgs e)
		{
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (this.SurveyConductID == null)
			{
				Redirect<IndividualSurveySummaryFilters>();
				return;
			}

			var reportFileName = "~/Reports/QualityAssurance/Executive/SurveySummary.rdlc".MapPath();
			var reportDataSet = Aspire.BL.Core.QualityAssurance.Executive.Reports.Survey.GetIndividualSurveySummary(this.SurveyConductID.Value, this.DepartmentID, this.ProgramID, this.SemesterNo, this.Section, this.Shift, this.StudentID, this.RegisteredCourseID, this.OfferedCourseID, this.FacultyMemberID, this.ExecutiveIdentity.LoginSessionGuid);
			if (reportDataSet == null)
			{
				reportViewer.LocalReport.DataSources.Clear();
				return;
			}
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.Summary), reportDataSet.Summary));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}