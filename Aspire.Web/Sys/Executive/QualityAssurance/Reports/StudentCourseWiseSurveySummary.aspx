﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="StudentCourseWiseSurveySummary.aspx.cs" Inherits="Aspire.Web.Sys.Executive.QualityAssurance.Reports.StudentCourseWiseSurveySummary" %>

<asp:Content ContentPlaceHolderID="Parameters" runat="server">
	<asp:UpdatePanel runat="server" ID="updatePanelFilter" UpdateMode="Conditional">
		<ContentTemplate>
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" AutoPostBack="True" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Survey Conduct Type:" AssociatedControlID="ddlSurveyConductType" />
					<aspire:AspireDropDownList runat="server" ID="ddlSurveyConductType" ValidationGroup="Parameters" AutoPostBack="True" OnSelectedIndexChanged="ddlSurveyConductType_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Survey:" AssociatedControlID="ddlSurveyConductID" />
					<aspire:AspireDropDownList runat="server" ID="ddlSurveyConductID" ValidationGroup="Parameters" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
					<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" ValidationGroup="Parameters" />
				</div>
				<div class="form-group">
					<aspire:AspireButton runat="server" ID="btnDisplay" Text="Display" ValidationGroup="Paramters" OnClick="btnDisplay_OnClick" />
				</div>
			</div>
		</ContentTemplate>
		<Triggers>
			<asp:PostBackTrigger ControlID="btnDisplay" />
		</Triggers>
	</asp:UpdatePanel>
</asp:Content>
