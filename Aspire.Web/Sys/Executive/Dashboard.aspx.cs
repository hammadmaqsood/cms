﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Executive
{
	[AspirePage("77656026-DB5F-4D2E-A810-7DA1CA41A711", UserTypes.Executive, "~/Sys/Executive/Dashboard.aspx", "Home", false, AspireModules.None)]
	public partial class Dashboard : ExecutivePage
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		public override bool BreadcrumbVisible => false;
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_desktop);
		public override string PageTitle => "Dashboard";
	}
}