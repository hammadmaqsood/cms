﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Executive.Feedbacks
{
	[AspirePage("CE929BAC-299C-4731-B028-4A2587FFA2FA", UserTypes.Executive, "~/Sys/Executive/Feedbacks/Feedback.aspx", "Feedback", true, AspireModules.Feedbacks)]
	public partial class Feedback : ExecutivePage
	{
		protected override IReadOnlyDictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ExecutivePermissions.Feedbacks.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override string PageTitle => "Feedback";

		public override PageIcon PageIcon => FontAwesomeIcons.regular_comments.GetIcon();

		public static string GetPageUrl(Guid userFeedbackID)
		{
			return typeof(Feedback).GetAspirePageAttribute().PageUrl.AttachQueryParam(nameof(UserFeedbackID), userFeedbackID);
		}

		public static void Redirect(Guid userFeedbackID)
		{
			Redirect(GetPageUrl(userFeedbackID));
		}

		public Guid? UserFeedbackID => this.Request.GetParameterValue<Guid>(nameof(this.UserFeedbackID));

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.UserFeedbackID == null)
				{
					Redirect<Feedbacks>();
					return;
				}

				var feedback = BL.Core.Feedbacks.Executive.UserFeedback.GetUserFeedback(this.UserFeedbackID.Value, this.AspireIdentity.LoginSessionGuid);
				if (feedback == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Feedbacks>();
					return;
				}

				this.tbFeedbackNo.Text = feedback.FeedbackNo;
				this.tbFeedbackBoxType.Text = feedback.MovedFromFeedbackBoxType == null ? feedback.FeedbackBoxType.GetFullName() : $"{feedback.MovedFromFeedbackBoxType.Value.GetFullName()} -> {feedback.FeedbackBoxType.GetFullName()}";
				this.tbFeedbackType.Text = feedback.FeedbackType.GetFullName();
				this.tbFeedbackDate.Text = $"{feedback.FeedbackDate:g}";

				this.tbFromName.Text = feedback.Name;
				this.tbFromUserType.Text = feedback.UserTypeEnum.ToFullName();
				this.tbFromEmail.Text = feedback.Email;

				this.ltrlFeedbackDetails.Text = feedback.FeedbackDetails;
				this.ltrlFeedbackDetails.Mode = System.Web.UI.WebControls.LiteralMode.PassThrough;
				var userFeedbackMembersList = BL.Core.Feedbacks.Executive.UserFeedback.GetUserFeedbackMembers(feedback.FeedbackBoxTypeEnum, this.AspireIdentity.LoginSessionGuid);
				this.lbAssignedToUserFeedbackMemberIDs.DataBind(userFeedbackMembersList);
				this.lbAssignedToUserFeedbackMemberIDs.SetSelectedValues(feedback.Assignees.Select(a => a.UserFeedbackMemberID.ToString()));
				this.tbNotes.Text = feedback.Notes;
				this.repeaterDiscussion.DataBind(feedback.Discussions.OrderBy(d => d.CreatedDate));
				var discussionsFound = feedback.Discussions.Any();
				this.repeaterDiscussion.Visible = discussionsFound;

				this.ViewState[nameof(UserFeedback.UserFeedbackID)] = feedback.UserFeedbackID;

				this.btnMoveToSpamJunk.Enabled = false;
				this.btnMoveToPending.Enabled = false;
				this.btnMoveToInProcess.Enabled = false;
				this.btnClose.Enabled = false;
				this.btnArchive.Visible = false;
				this.btnUnarchive.Visible = false;
				this.btnMoveToDirectorGeneral.Visible = feedback.FeedbackBoxTypeEnum == UserFeedback.FeedbackBoxTypes.Rector;

				this.divSaveButtons.Visible = false;
				this.divDiscussionAdd.Visible = false;
				this.divDiscussionAddButtons.Visible = false;

				var statusEnum = feedback.Status.GetEnum<UserFeedback.Statuses>();
				switch (statusEnum)
				{
					case UserFeedback.Statuses.Pending:
						this.btnMoveToSpamJunk.Enabled = true;
						this.btnMoveToInProcess.Enabled = true;
						this.btnUnarchive.Visible = feedback.Archived;
						this.divSaveButtons.Visible = true;
						this.divDiscussionAdd.Visible = true;
						this.divDiscussionAddButtons.Visible = false;
						this.btnMarkAsRead.Enabled = true;
						this.btnMarksAsUnread.Enabled = true;
						break;
					case UserFeedback.Statuses.InProcess:
						this.btnMoveToPending.Enabled = !discussionsFound;
						this.btnMoveToSpamJunk.Enabled = true;
						this.btnUnarchive.Visible = feedback.Archived;
						this.divSaveButtons.Visible = true;
						this.divDiscussionAdd.Visible = true;
						this.divDiscussionAddButtons.Visible = false;
						this.btnMarkAsRead.Enabled = true;
						this.btnMarksAsUnread.Enabled = true;
						break;
					case UserFeedback.Statuses.SpamJunk:
						this.btnMoveToPending.Enabled = !discussionsFound;
						this.btnMoveToInProcess.Enabled = true;
						this.btnArchive.Visible = !feedback.Archived;
						this.btnUnarchive.Visible = feedback.Archived;
						this.divSaveButtons.Visible = true;
						this.btnMarkAsRead.Enabled = true;
						this.btnMarksAsUnread.Enabled = false;
						break;
					case UserFeedback.Statuses.Closed:
						this.btnArchive.Visible = !feedback.Archived;
						this.btnUnarchive.Visible = feedback.Archived;
						this.btnClose.ButtonType = AspireButton.ButtonTypes.Primary;
						this.btnMarkAsRead.Enabled = true;
						this.btnMarksAsUnread.Enabled = false;
						break;
					default:
						throw new NotImplementedEnumException(statusEnum);
				}

				this.btnClose.Enabled = discussionsFound && statusEnum != UserFeedback.Statuses.Closed;
				this.btnMoveToSpamJunk.ButtonType = statusEnum == UserFeedback.Statuses.SpamJunk ? AspireButton.ButtonTypes.Primary : AspireButton.ButtonTypes.Default;
				this.btnMoveToPending.ButtonType = statusEnum == UserFeedback.Statuses.Pending ? AspireButton.ButtonTypes.Primary : AspireButton.ButtonTypes.Default;
				this.btnMoveToInProcess.ButtonType = statusEnum == UserFeedback.Statuses.InProcess ? AspireButton.ButtonTypes.Primary : AspireButton.ButtonTypes.Default;
				if (statusEnum != UserFeedback.Statuses.Closed)
					this.btnClose.ButtonType = this.btnClose.Enabled ? AspireButton.ButtonTypes.Danger : AspireButton.ButtonTypes.Default;
				this.btnMarksAsUnread.Visible = !feedback.Unread;
				this.btnMarkAsRead.Visible = feedback.Unread;

				this.tbNotes.ReadOnly = !this.divSaveButtons.Visible;
				this.lbAssignedToUserFeedbackMemberIDs.Enabled = this.divSaveButtons.Visible;
				this.divCancelButton.Visible = !this.divDiscussionAdd.Visible && !this.divSaveButtons.Visible;
			}
		}

		protected void btnSave_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;

			var userFeedbackID = (Guid)this.ViewState[nameof(UserFeedback.UserFeedbackID)];
			var notes = this.tbNotes.Text.TrimAndMakeItNullIfEmpty();
			var assignedToUserFeedbackMemberIDs = this.lbAssignedToUserFeedbackMemberIDs.SelectedValues.Select(v => v.ToGuid()).ToList();
			var status = BL.Core.Feedbacks.Executive.UserFeedback.UpdateUserFeedback(userFeedbackID, notes, assignedToUserFeedbackMemberIDs, this.AspireIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateUserFeedbackStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<Feedbacks>();
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateUserFeedbackStatuses.Success:
					var discussionDetails = this.Request.Unvalidated.Form[this.tbDiscussionDetails.UniqueID];
					if (this.divDiscussionAdd.Visible && !string.IsNullOrEmpty(discussionDetails) && !string.IsNullOrWhiteSpace(discussionDetails))
					{
						var statusDiscussion = BL.Core.Feedbacks.Executive.UserFeedback.AddDiscussion(userFeedbackID, discussionDetails, this.AspireIdentity.LoginSessionGuid);
						switch (statusDiscussion)
						{
							case BL.Core.Feedbacks.Executive.UserFeedback.AddDiscussionStatuses.NoRecordFound:
								this.AddNoRecordFoundAlert();
								Redirect<Feedbacks>();
								return;
							case BL.Core.Feedbacks.Executive.UserFeedback.AddDiscussionStatuses.StatusIsClosed:
								this.AddErrorAlert($"Feedback status is {UserFeedback.Statuses.Closed.GetFullName()}.");
								Redirect(userFeedbackID);
								return;
							case BL.Core.Feedbacks.Executive.UserFeedback.AddDiscussionStatuses.StatusIsSpamJunk:
								this.AddErrorAlert($"Feedback status is {UserFeedback.Statuses.SpamJunk.GetFullName()}.");
								Redirect(userFeedbackID);
								return;
							case BL.Core.Feedbacks.Executive.UserFeedback.AddDiscussionStatuses.Success:
								break;
							default:
								throw new NotImplementedEnumException(statusDiscussion);
						}
					}
					this.AddSuccessAlert("Record has been updated.");
					Redirect(userFeedbackID);
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateUserFeedbackStatuses.StatusIsClosed:
					this.AddErrorAlert("Feedback status is closed.");
					Redirect(userFeedbackID);
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		protected void cvtbDiscussionDetails_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
		{
			args.IsValid = !string.IsNullOrWhiteSpace(this.tbDiscussionDetails.Text);
		}

		protected void btnAddDiscussionCancel_Click(object sender, EventArgs e)
		{
			Redirect(this.UserFeedbackID ?? throw new InvalidOperationException());
		}

		protected void btnAddDiscussion_Click(object sender, EventArgs e)
		{
			var userFeedbackID = (Guid)this.ViewState[nameof(UserFeedback.UserFeedbackID)];
			var discussionDetails = this.Request.Unvalidated.Form[this.tbDiscussionDetails.UniqueID];
			var status = BL.Core.Feedbacks.Executive.UserFeedback.AddDiscussion(userFeedbackID, discussionDetails, this.AspireIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Feedbacks.Executive.UserFeedback.AddDiscussionStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<Feedbacks>();
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.AddDiscussionStatuses.Success:
					this.AddSuccessAlert("Record has been updated.");
					Redirect(userFeedbackID);
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.AddDiscussionStatuses.StatusIsClosed:
					this.AddErrorAlert($"Feedback status is {UserFeedback.Statuses.Closed.GetFullName()}.");
					Redirect(userFeedbackID);
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.AddDiscussionStatuses.StatusIsSpamJunk:
					this.AddErrorAlert($"Feedback status is {UserFeedback.Statuses.SpamJunk.GetFullName()}.");
					Redirect(userFeedbackID);
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		private void ChangeStatus(Model.Entities.UserFeedback.Statuses statusEnum)
		{
			var userFeedbackID = (Guid)this.ViewState[nameof(UserFeedback.UserFeedbackID)];
			var status = BL.Core.Feedbacks.Executive.UserFeedback.UpdateUserFeedbackStatus(userFeedbackID, statusEnum, this.ExecutiveIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateUserFeedbackStatusResults.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<Feedbacks>();
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateUserFeedbackStatusResults.StatusIsClosed:
					this.AddErrorAlert($"Status is {Model.Entities.UserFeedback.Statuses.Closed}.");
					Redirect(userFeedbackID);
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateUserFeedbackStatusResults.StatusIsAlreadyPending:
					this.AddErrorAlert($"Status is already {Model.Entities.UserFeedback.Statuses.Pending}.");
					Redirect(userFeedbackID);
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateUserFeedbackStatusResults.StatusIsAlreadySpamJunk:
					this.AddErrorAlert($"Status is already {Model.Entities.UserFeedback.Statuses.SpamJunk}.");
					Redirect(userFeedbackID);
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateUserFeedbackStatusResults.StatusIsAlreadyInProcess:
					this.AddErrorAlert($"Status is already {Model.Entities.UserFeedback.Statuses.InProcess}.");
					Redirect(userFeedbackID);
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateUserFeedbackStatusResults.Success:
					this.AddSuccessAlert($"Status has been updated.");
					Redirect(userFeedbackID);
					return;
				default:
					throw new NotImplementedEnumException(status);
			}

		}

		private void ChangeArchiveStatus(bool archive)
		{
			var userFeedbackID = (Guid)this.ViewState[nameof(UserFeedback.UserFeedbackID)];
			var status = BL.Core.Feedbacks.Executive.UserFeedback.UpdateUserFeedbackArchiveStatus(userFeedbackID, archive, this.ExecutiveIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Feedbacks.Executive.UserFeedback.ArchiveUserFeedbackStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<Feedbacks>();
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.ArchiveUserFeedbackStatuses.Success:
					this.AddSuccessAlert($"Record has been updated.");
					Redirect(userFeedbackID);
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.ArchiveUserFeedbackStatuses.AlreadyArchived:
					this.AddErrorAlert($"Record is already archived.");
					Redirect(userFeedbackID);
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.ArchiveUserFeedbackStatuses.AlreadyNotArchived:
					this.AddErrorAlert($"Record is already unarchived.");
					Redirect(userFeedbackID);
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.ArchiveUserFeedbackStatuses.PendingStatusCannotBeArchived:
					this.AddErrorAlert($"{Model.Entities.UserFeedback.Statuses.Pending} Feedbacks cannot be archived.");
					Redirect(userFeedbackID);
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.ArchiveUserFeedbackStatuses.InProcessStatusCannotBeArchived:
					this.AddErrorAlert($"{Model.Entities.UserFeedback.Statuses.InProcess} Feedbacks cannot be archived.");
					Redirect(userFeedbackID);
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		private void ChangeReadStatus(bool unread)
		{
			var userFeedbackID = (Guid)this.ViewState[nameof(UserFeedback.UserFeedbackID)];
			var status = BL.Core.Feedbacks.Executive.UserFeedback.UpdateUserFeedbackReadStatus(userFeedbackID, unread, this.ExecutiveIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateUserFeedbackReadStatusResults.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<Feedbacks>();
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateUserFeedbackReadStatusResults.AlreadyRead:
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateUserFeedbackReadStatusResults.AlreadyUnread:
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateUserFeedbackReadStatusResults.Success:
					this.AddSuccessAlert($"Record has been updated.");
					Redirect(userFeedbackID);
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		protected void btnMoveToInProcess_Click(object sender, EventArgs e)
		{
			this.ChangeStatus(UserFeedback.Statuses.InProcess);
		}

		protected void btnMoveToSpamJunk_Click(object sender, EventArgs e)
		{
			this.ChangeStatus(UserFeedback.Statuses.SpamJunk);
		}

		protected void btnMoveToPending_Click(object sender, EventArgs e)
		{
			this.ChangeStatus(UserFeedback.Statuses.Pending);
		}

		protected void btnArchive_Click(object sender, EventArgs e)
		{
			this.ChangeArchiveStatus(true);
		}

		protected void btnUnarchive_Click(object sender, EventArgs e)
		{
			this.ChangeArchiveStatus(false);
		}

		protected void btnMarkAsRead_Click(object sender, EventArgs e)
		{
			this.ChangeReadStatus(false);
		}

		protected void btnMarksAsUnread_Click(object sender, EventArgs e)
		{
			this.ChangeReadStatus(true);
		}

		protected void btnClose_Click(object sender, EventArgs e)
		{
			var userFeedbackID = (Guid)this.ViewState[nameof(UserFeedback.UserFeedbackID)];
			var status = BL.Core.Feedbacks.Executive.UserFeedback.CloseUserFeedback(userFeedbackID, this.ExecutiveIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Feedbacks.Executive.UserFeedback.CloseUserFeedbackStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<Feedbacks>();
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.CloseUserFeedbackStatuses.Success:
					this.AddSuccessAlert($"Feedback has been {Model.Entities.UserFeedback.Statuses.Closed}.");
					Redirect(userFeedbackID);
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.CloseUserFeedbackStatuses.AlreadyClosed:
					this.AddErrorAlert($"Feedback is already {Model.Entities.UserFeedback.Statuses.Closed}.");
					Redirect(userFeedbackID);
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.CloseUserFeedbackStatuses.CannotClosedWithoutProcessing:
					this.AddSuccessAlert($"Feedback cannot be {Model.Entities.UserFeedback.Statuses.Closed} without processing.");
					Redirect(userFeedbackID);
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.CloseUserFeedbackStatuses.StatusCannotBeClosedFromSpamJunk:
					this.AddSuccessAlert($"Feedback cannot be {Model.Entities.UserFeedback.Statuses.Closed} when marked as {Model.Entities.UserFeedback.Statuses.SpamJunk}.");
					Redirect(userFeedbackID);
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.CloseUserFeedbackStatuses.StatusCannotBeClosedFromPending:
					this.AddSuccessAlert($"{Model.Entities.UserFeedback.Statuses.Pending} feedbacks cannot be {Model.Entities.UserFeedback.Statuses.Closed}.");
					Redirect(userFeedbackID);
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}

		protected void btnMoveToDirectorGeneral_Click(object sender, EventArgs e)
		{
			var userFeedbackID = (Guid)this.ViewState[nameof(UserFeedback.UserFeedbackID)];
			var status = BL.Core.Feedbacks.Executive.UserFeedback.UpdateBoxType(userFeedbackID, UserFeedback.FeedbackBoxTypes.DirectorGeneralCampus, this.ExecutiveIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateBoxTypeStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<Feedbacks>();
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateBoxTypeStatuses.CanNotMoveToRector:
					this.AddErrorAlert("Feedback can not be moved to Rector's Box.");
					break;
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateBoxTypeStatuses.AlreadyInRectorBox:
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateBoxTypeStatuses.AlreadyInDGBox:
					Redirect(userFeedbackID);
					return;
				case BL.Core.Feedbacks.Executive.UserFeedback.UpdateBoxTypeStatuses.Success:
					this.AddSuccessAlert("Feedback has been moved to DG's Box.");
					Redirect(userFeedbackID);
					return;
				default:
					throw new NotImplementedEnumException(status);
			}
		}
	}
}