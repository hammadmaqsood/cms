﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Feedbacks.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Feedbacks.Feedbacks" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Box Type:" AssociatedControlID="ddlFeedbackBoxTypeFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlFeedbackBoxTypeFilter" ValidationGroup="Filters" AutoPostBack="true" OnSelectedIndexChanged="ddlFeedbackBoxTypeFilter_SelectedIndexChanged" />
				<aspire:AspireDropDownList runat="server" Visible="false" ID="ddlStatus" ValidationGroup="Filters" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Feedback Type:" AssociatedControlID="ddlFeedbackTypeFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlFeedbackTypeFilter" ValidationGroup="Filters" AutoPostBack="true" OnSelectedIndexChanged="ddlFeedbackTypeFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="User Type:" AssociatedControlID="ddlUserTypeFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlUserTypeFilter" ValidationGroup="Filters" AutoPostBack="true" OnSelectedIndexChanged="ddlUserTypeFilter_SelectedIndexChanged" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Assigned To:" AssociatedControlID="ddlAssignedToUserFeedbackMemberID" />
				<aspire:AspireDropDownList runat="server" ID="ddlAssignedToUserFeedbackMemberID" ValidationGroup="Filters" AutoPostBack="true" OnSelectedIndexChanged="ddlAssignedToUserFeedbackMemberID_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-8">
			<asp:Panel CssClass="form-group" runat="server" DefaultButton="btnSearch">
				<aspire:AspireLabel runat="server" Text="Search:" AssociatedControlID="tbSearch" />
				<div class="input-group">
					<aspire:AspireTextBox runat="server" ID="tbSearch" ValidationGroup="Filters" />
					<div class="input-group-btn">
						<aspire:AspireButton Glyphicon="search" runat="server" ID="btnSearch" OnClick="btnSearch_Click" ButtonType="Primary" ValidationGroup="Filters" />
					</div>
				</div>
			</asp:Panel>
		</div>
	</div>
	<div class="form-group">
		<div class="btn-group btn-group-justified" runat="server" id="divControls">
			<aspire:AspireButton runat="server" ButtonType="Default" ID="btnInbox" EnableViewState="false" CausesValidation="false" OnClick="btnInbox_Click">
				<span class="fas fa-inbox"></span>
				Pending
				<span runat="server" id="badgePending" class="badge"></span>
			</aspire:AspireButton>
			<aspire:AspireButton runat="server" ButtonType="Default" ID="btnInProcess" EnableViewState="false" CausesValidation="false" OnClick="btnInProcess_Click">
				<span class="fas fa-spinner"></span>
				In Process
				<span runat="server" id="badgeInProcess" class="badge"></span>
			</aspire:AspireButton>
			<aspire:AspireButton runat="server" ButtonType="Default" ID="btnSpamJunk" EnableViewState="false" CausesValidation="false" OnClick="btnSpamJunk_Click">
				<span class="fas fa-exclamation-triangle"></span>
				Spam/Junk
				<span runat="server" id="badgeSpamJunk" class="badge"></span>
			</aspire:AspireButton>
			<aspire:AspireButton runat="server" ButtonType="Default" ID="btnClosed" EnableViewState="false" CausesValidation="false" OnClick="btnClosed_Click">
				<span class="fas fa-window-close" ></span>
				Closed
				<span runat="server" id="badgeClosed" class="badge"></span>
			</aspire:AspireButton>
			<aspire:AspireButton runat="server" ButtonType="Default" ID="btnAll" EnableViewState="false" CausesValidation="false" OnClick="btnAll_Click">
				All
				<span runat="server" id="badgeAll" class="badge"></span>
			</aspire:AspireButton>
		</div>
	</div>
	<aspire:AspireGridView runat="server" Responsive="true" AllowSorting="true" ItemType="Aspire.BL.Core.Feedbacks.Executive.UserFeedback.Feedback" ID="gvUserFeedbacks" AutoGenerateColumns="false" OnPageIndexChanging="gvUserFeedbacks_PageIndexChanging" OnPageSizeChanging="gvUserFeedbacks_PageSizeChanging" OnSorting="gvUserFeedbacks_Sorting" OnRowDataBound="gvUserFeedbacks_RowDataBound">
		<Columns>
			<asp:TemplateField HeaderText="Select">
				<ItemTemplate>
					<aspire:HiddenField runat="server" Mode="Encrypted" ID="hfUserFeedbackID" Value="<%# Item.UserFeedbackID %>" />
					<aspire:AspireCheckBox runat="server" ID="cbSelect" ValidationGroup="Select" CssClass="SelectFeedback" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate>
					<%#: Container.DataItemIndex +1 %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Reference No." SortExpression="UserFeedbackID">
				<ItemTemplate>
					<aspire:AspireHyperLink Target="_blank" Text="<%# Item.FeedbackNo %>" runat="server" NavigateUrl="<%# Aspire.Web.Sys.Executive.Feedbacks.Feedback.GetPageUrl(Item.UserFeedbackID) %>" />
					<div>
						<span class="label label-default" runat="server" visible="<%# Item.Archived %>">Archived</span>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Box Type" SortExpression="FeedbackBoxType">
				<ItemTemplate>
					<%#: Item.MovedFromFeedbackBoxType == null ? Item.FeedbackBoxType.GetFullName() : $"{Item.MovedFromFeedbackBoxType.Value.GetFullName()} -> {Item.FeedbackBoxType.GetFullName()}" %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Type" SortExpression="FeedbackType">
				<ItemTemplate>
					<%#: Item.FeedbackType.GetFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Date" SortExpression="FeedbackDate">
				<ItemTemplate>
					<%#: $"{Item.FeedbackDate:g}" %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="From">
				<ItemTemplate>
					<div>
						<span class="label label-default"><%#: Item.UserTypeEnum.ToFullName() %></span>
					</div>
					<span class="label label-info"><%#: Item.Email %></span>
					<div>
						<%#: $"{Item.Name}" %>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Assigned To">
				<ItemTemplate>
					<%# (string.Join("<br/>", Item.AssignedToMembers.Select(m=> m.HtmlEncode()))).ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Status" SortExpression="Status">
				<ItemTemplate>
					<%# Item.Status.GetFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireHyperLink Target="_blank" Text="View" runat="server" NavigateUrl="<%# Aspire.Web.Sys.Executive.Feedbacks.Feedback.GetPageUrl(Item.UserFeedbackID) %>" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<div class="form-group text-center">
		<aspire:AspireButton runat="server" ID="btnPrint" ValidationGroup="Select" Text="Print Selected Feedback" OnClick="btnPrint_Click" />
		<aspire:HiddenField runat="server" EnableViewState="false" ID="hfPrintUrl" />
	</div>
	<script type="text/javascript">
		$(function () {
			$("select.form-control").applySelect2();
			var url = "<%= this.hfPrintUrl.Value.Replace("\"","\\\"") %>";
			if (url !== undefined && url !== null && url !== "" && url.length > 10)
				window.open(url, "_blank");
		});
	</script>
</asp:Content>
