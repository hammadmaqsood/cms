﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Executive.Feedbacks
{
	[AspirePage("539b1d8c-2a37-49fc-bded-e7f724bec9b1", UserTypes.Executive, "~/Sys/Executive/Feedbacks/PrintFeedback.aspx", "Print Feedback", false, AspireModules.Feedbacks)]
	public partial class PrintFeedback : ExecutivePage
	{
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();

		public override string PageTitle => "Print";

		public static string GetPageUrl(string sessionName)
		{
			return typeof(PrintFeedback).GetAspirePageAttribute().PageUrl.AttachQueryParam("SessionName", sessionName);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			var sessionName = this.Request.GetParameterValue("SessionName");
			if (string.IsNullOrEmpty(sessionName))
			{
				Redirect<Feedbacks>();
				return;
			}
			var userFeedbackIDs = this.Session[sessionName] as List<Guid>;
			if (!Configurations.Current.AppSettings.IsDevelopmentPC)
				this.Session[sessionName] = null;
			if (userFeedbackIDs?.Any() != true)
			{
				Redirect<Feedbacks>();
				return;
			}

			var userFeedbacks = BL.Core.Feedbacks.Executive.UserFeedback.GetUserFeedbacks(userFeedbackIDs, this.ExecutiveIdentity.LoginSessionGuid);
			this.repeaterUserFeedback.DataBind(userFeedbacks);
		}
	}
}