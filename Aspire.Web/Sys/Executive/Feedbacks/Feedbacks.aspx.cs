﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Executive.Feedbacks
{
	[AspirePage("34D60C3A-1D3D-4444-8671-7F48DD232462", UserTypes.Executive, "~/Sys/Executive/Feedbacks/Feedbacks.aspx", "Feedbacks", true, AspireModules.Feedbacks)]
	public partial class Feedbacks : ExecutivePage
	{
		protected override IReadOnlyDictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ExecutivePermissions.Feedbacks.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override string PageTitle => "Feedbacks";

		public override PageIcon PageIcon => FontAwesomeIcons.regular_comments.GetIcon();

		public static string GetPageUrl(UserFeedback.FeedbackBoxTypes? feedbackBoxTypeEnum, UserFeedback.FeedbackTypes? feedbackTypeEnum, UserTypes? userTypeEnum, DateTime? feedbackDateFrom, DateTime? feedbackDateTo, UserFeedback.Statuses? statusEnum, bool? unread, Guid? assignedToUserFeedbackMemberID, string searchText, int? pageIndex, int? pageSize, SortDirection sortDirection, string sortExpression)
		{
			return typeof(Feedbacks).GetAspirePageAttribute().PageUrl.AttachQueryParams(
				("FeedbackBoxTypeEnum", feedbackBoxTypeEnum),
				("FeedbackTypeEnum", feedbackTypeEnum),
				("UserTypeEnum", userTypeEnum),
				("FeedbackDateFrom", feedbackDateFrom),
				("FeedbackDateTo", feedbackDateTo),
				("StatusEnum", statusEnum),
				("Unread", unread),
				("AssignedToUserFeedbackMemberID", assignedToUserFeedbackMemberID),
				("SearchText", searchText),
				("PageIndex", pageIndex),
				("PageSize", pageSize),
				("SortDirection", sortDirection),
				("SortExpression", sortExpression));
		}

		public static void Redirect(UserFeedback.FeedbackBoxTypes? feedbackBoxTypeEnum, UserFeedback.FeedbackTypes? feedbackTypeEnum, UserTypes? userTypeEnum, DateTime? feedbackDateFrom, DateTime? feedbackDateTo, UserFeedback.Statuses? statusEnum, bool? unread, Guid? assignedToUserFeedbackMemberID, string searchText, int? pageIndex, int? pageSize, SortDirection sortDirection, string sortExpression)
		{
			Redirect(GetPageUrl(feedbackBoxTypeEnum, feedbackTypeEnum, userTypeEnum, feedbackDateFrom, feedbackDateTo, statusEnum, unread, assignedToUserFeedbackMemberID, searchText, pageIndex, pageSize, sortDirection, sortExpression));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState["FromButtons"] = "0";
				this.ViewState.SetSortProperties(this.GetParameterValue("SortExpression") ?? nameof(UserFeedback.FeedbackDate), this.GetParameterValue<SortDirection>("SortDirection") ?? SortDirection.Descending);

				var canViewRectorBox = this.ExecutiveIdentity.TryDemand(ExecutivePermissions.Feedbacks.CanViewRectorBox, UserGroupPermission.Allowed) != null;
				var canViewDirectorGeneralBox = this.ExecutiveIdentity.TryDemand(ExecutivePermissions.Feedbacks.CanViewDirectorGeneralBox, UserGroupPermission.Allowed) != null;
				var items = new List<ListItem>();
				if (canViewRectorBox)
					items.Add(new ListItem(UserFeedback.FeedbackBoxTypes.Rector.GetFullName(), UserFeedback.FeedbackBoxTypes.Rector.GetGuid().ToString()));
				if (canViewDirectorGeneralBox)
					items.Add(new ListItem(UserFeedback.FeedbackBoxTypes.DirectorGeneralCampus.GetFullName(), UserFeedback.FeedbackBoxTypes.DirectorGeneralCampus.GetGuid().ToString()));
				this.ddlFeedbackBoxTypeFilter.DataBind(items);
				this.ddlFeedbackTypeFilter.FillGuidEnums<UserFeedback.FeedbackTypes>(CommonListItems.All).SetSelectedValueIfNotPostback("FeedbackTypeEnum");
				var userTypes = new[] { UserTypes.Student, UserTypes.Staff, UserTypes.Executive, UserTypes.Faculty }.Select(u => new ListItem
				{
					Text = u.ToFullName(),
					Value = ((short)u).ToString()
				}).OrderBy(i => i.Text).ToList();
				this.ddlUserTypeFilter.DataBind(userTypes, CommonListItems.All).SetSelectedEnumValueIfNotPostback<UserTypes>("UserTypeEnum");

				this.ddlStatus.FillGuidEnums<UserFeedback.Statuses>(CommonListItems.All)
					.SetEnumValue(UserFeedback.Statuses.Pending)
					.SetSelectedEnumGuidValueIfNotPostback<UserFeedback.Statuses>("StatusEnum");

				this.tbSearch.SetTextIfNotPostback("SearchText");
				this.gvUserFeedbacks.PageSize = this.GetParameterValue<int>("PageSize") ?? this.gvUserFeedbacks.PageSize;
				this.ddlFeedbackBoxTypeFilter_SelectedIndexChanged(null, null);
			}
		}

		private bool search = true;

		protected void ddlFeedbackBoxTypeFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			var feedbackBoxTypeEnum = this.ddlFeedbackBoxTypeFilter.GetSelectedGuidEnum<Model.Entities.UserFeedback.FeedbackBoxTypes>();
			var userFeedbackMembers = BL.Core.Feedbacks.Executive.UserFeedback.GetUserFeedbackMembers(feedbackBoxTypeEnum, this.ExecutiveIdentity.LoginSessionGuid);
			this.ddlAssignedToUserFeedbackMemberID.DataBind(userFeedbackMembers, CommonListItems.All).SetSelectedValueIfNotPostback("AssignedToUserFeedbackMemberID");
			this.ddlFeedbackTypeFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlFeedbackTypeFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlUserTypeFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlUserTypeFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlAssignedToUserFeedbackMemberID_SelectedIndexChanged(null, null);
		}

		protected void ddlAssignedToUserFeedbackMemberID_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearch_Click(null, null);
		}

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			var pageIndex = 0;
			if (!this.IsPostBack)
				pageIndex = this.GetParameterValue<int>("PageIndex") ?? pageIndex;
			this.GetData(pageIndex);
		}

		protected void gvUserFeedbacks_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvUserFeedbacks_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvUserFeedbacks.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvUserFeedbacks_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			if (!this.search)
				return;
			var feedbackBoxTypeEnum = this.ddlFeedbackBoxTypeFilter.GetSelectedNullableGuidEnum<UserFeedback.FeedbackBoxTypes>();
			if (feedbackBoxTypeEnum == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect<Dashboard>();
				return;
			}
			var feedbackTypeEnum = this.ddlFeedbackTypeFilter.GetSelectedNullableGuidEnum<UserFeedback.FeedbackTypes>();
			var userTypeEnum = this.ddlUserTypeFilter.GetSelectedEnumValue<UserTypes>(null);
			var statusEnum = this.ddlStatus.GetSelectedNullableGuidEnum<UserFeedback.Statuses>();
			var assignedToUserFeedbackMemberID = this.ddlAssignedToUserFeedbackMemberID.SelectedValue.ToNullableGuid();
			var searchText = this.tbSearch.Text.ToNullIfWhiteSpace();
			var pageSize = this.gvUserFeedbacks.PageSize;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();

			var (feedbacks, virtualItemCount, filteredStatistics, statistics) = BL.Core.Feedbacks.Executive.UserFeedback.GetUserFeedbacks(feedbackBoxTypeEnum.Value, feedbackTypeEnum, userTypeEnum, statusEnum, null, assignedToUserFeedbackMemberID, null, searchText, pageIndex, pageSize, sortExpression, sortDirection, this.AspireIdentity.LoginSessionGuid);
			this.gvUserFeedbacks.DataBind(feedbacks, pageIndex, virtualItemCount, sortExpression, sortDirection);

			this.badgePending.InnerText = GetTotalText(filteredStatistics, statistics, UserFeedback.Statuses.Pending);
			this.badgeInProcess.InnerText = GetTotalText(filteredStatistics, statistics, UserFeedback.Statuses.InProcess);
			this.badgeSpamJunk.InnerText = GetTotalText(filteredStatistics, statistics, UserFeedback.Statuses.SpamJunk);
			this.badgeClosed.InnerText = GetTotalText(filteredStatistics, statistics, UserFeedback.Statuses.Closed);
			this.badgeAll.InnerText = GetTotalText(filteredStatistics, statistics, null);

			this.btnInbox.ButtonType = statusEnum == UserFeedback.Statuses.Pending ? AspireButton.ButtonTypes.Primary : AspireButton.ButtonTypes.Default;
			this.btnInProcess.ButtonType = statusEnum == UserFeedback.Statuses.InProcess ? AspireButton.ButtonTypes.Primary : AspireButton.ButtonTypes.Default;
			this.btnSpamJunk.ButtonType = statusEnum == UserFeedback.Statuses.SpamJunk ? AspireButton.ButtonTypes.Primary : AspireButton.ButtonTypes.Default;
			this.btnClosed.ButtonType = statusEnum == UserFeedback.Statuses.Closed ? AspireButton.ButtonTypes.Primary : AspireButton.ButtonTypes.Default;
			this.btnAll.ButtonType = statusEnum == null ? AspireButton.ButtonTypes.Primary : AspireButton.ButtonTypes.Default;
		}

		private static string GetTotalText(List<BL.Core.Feedbacks.Executive.UserFeedback.FeedBackStatistics> filteredStatistics, List<BL.Core.Feedbacks.Executive.UserFeedback.FeedBackStatistics> statistics, UserFeedback.Statuses? statusEnum)
		{
			int filteredCount;
			int count;
			if (statusEnum == null)
			{
				filteredCount = filteredStatistics.Sum(s => s.Count);
				count = statistics.Sum(s => s.Count);
			}
			else
			{
				filteredCount = filteredStatistics.SingleOrDefault(s => s.StatusEnum == statusEnum)?.Count ?? 0;
				count = statistics.SingleOrDefault(s => s.StatusEnum == statusEnum)?.Count ?? 0;
			}
			if (filteredCount == 0 && count == 0)
				return "0";
			return $"{filteredCount}/{count}";
		}

		protected void gvUserFeedbacks_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			var feedback = e.Row.DataItem as Aspire.BL.Core.Feedbacks.Executive.UserFeedback.Feedback;
			if (feedback?.Unread == true)
				e.Row.CssClass = "text-primary";
		}

		protected void btnInbox_Click(object sender, EventArgs e)
		{
			this.ddlStatus.SetSelectedGuidEnum(UserFeedback.Statuses.Pending);
			this.btnSearch_Click(null, null);
		}

		protected void btnInProcess_Click(object sender, EventArgs e)
		{
			this.ddlStatus.SetSelectedGuidEnum(UserFeedback.Statuses.InProcess);
			this.btnSearch_Click(null, null);
		}

		protected void btnSpamJunk_Click(object sender, EventArgs e)
		{
			this.ddlStatus.SetSelectedGuidEnum(UserFeedback.Statuses.SpamJunk);
			this.btnSearch_Click(null, null);
		}

		protected void btnClosed_Click(object sender, EventArgs e)
		{
			this.ddlStatus.SetSelectedGuidEnum(UserFeedback.Statuses.Closed);
			this.btnSearch_Click(null, null);
		}

		protected void btnAll_Click(object sender, EventArgs e)
		{
			this.ddlStatus.ClearSelection();
			this.btnSearch_Click(null, null);
		}

		protected void btnPrint_Click(object sender, EventArgs e)
		{
			var selectedUserFeedbackID = new List<Guid>();
			foreach (GridViewRow row in this.gvUserFeedbacks.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow)
					continue;
				Lib.WebControls.CheckBox cbSelect = (Aspire.Lib.WebControls.CheckBox)row.FindControl(nameof(cbSelect));
				if (!cbSelect.Checked)
					continue;
				Lib.WebControls.HiddenField hfUserFeedbackID = (Aspire.Lib.WebControls.HiddenField)row.FindControl(nameof(hfUserFeedbackID));
				selectedUserFeedbackID.Add(hfUserFeedbackID.Value.ToGuid());
			}
			if (!selectedUserFeedbackID.Any())
			{
				this.AddErrorAlert("No record selected.");
				return;
			}
			var sessionName = $"UserFeedbackIDs{DateTime.Now}";
			this.Session[sessionName] = selectedUserFeedbackID;
			this.hfPrintUrl.Value = PrintFeedback.GetPageUrl(sessionName).ToAbsoluteUrl();
		}
	}
}