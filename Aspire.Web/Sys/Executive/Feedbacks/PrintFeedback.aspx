﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintFeedback.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Feedbacks.PrintFeedback" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Virtual Suggestions' Box</title>
	<link href="PrintFeedback.min.css" rel="stylesheet" />
</head>
<body>
	<form id="form1" runat="server">
		<div class="feedbackContainer">
			<div class="pageHeader">
				<img src="../../../Images/ReportLogo.gif" alt="BU Logo" />
				<h1><%= ((this.AspireIdentity?.BrandName) ?? "Bahria University").HtmlEncode() %></h1>
				<div class="printDate">
					<span>Printed on <%= DateTime.Now.ToString("F") %></span>
				</div>
			</div>
			<asp:Repeater runat="server" ID="repeaterUserFeedback" ItemType="Aspire.BL.Core.Feedbacks.Executive.UserFeedback.FeedbackDetailed">
				<ItemTemplate>
					<div class="feedback">
						<table class="layout">
							<tbody>
								<tr>
									<th>ID</th>
									<td><%#: Item.FeedbackNo %></td>
									<th>Box Type</th>
									<td><%#: Item.MovedFromFeedbackBoxType == null ? Item.FeedbackBoxType.GetFullName() : $"{Item.MovedFromFeedbackBoxType.Value.GetFullName()} -> {Item.FeedbackBoxType.GetFullName()}" %></td>
								</tr>
								<tr>
									<th>Type</th>
									<td><%#: Item.FeedbackType.GetFullName() %></td>
									<th>Submission Date</th>
									<td><%#: Item.FeedbackDate.ToString("G") %></td>
								</tr>
								<tr>
									<th>User Type</th>
									<td><%#: Item.UserTypeEnum.ToFullName() %></td>
									<th>Assigned To</th>
									<td><%#: string.Join(" | ", Item.Assignees.Select(a=> a.Designation)).ToNAIfNullOrEmpty() %></td>
								</tr>
								<tr>
									<th colspan="4">Details</th>
								</tr>
								<tr>
									<td colspan="4" class="feedbackDetails">
										<%# Item.FeedbackDetails %>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</ItemTemplate>
			</asp:Repeater>
		</div>
		<script type="text/javascript">
			window.print();
		</script>
	</form>
</body>
</html>
