﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Feedback.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Feedbacks.Feedback" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="HeadPH" runat="server">
	<link href="../../../Scripts/Plugins/summernote/summernote.css" rel="stylesheet" />
	<style type="text/css">
		.note-editor.note-frame.panel {
			margin-bottom: 0;
		}

		.well {
			margin-bottom: 0;
		}
	</style>
</asp:Content>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-group">
		<div class="btn-group" runat="server" id="divMoveTo">
			<aspire:AspireButton runat="server" ButtonType="Default" FontAwesomeIcon="solid_inbox" ID="btnMoveToPending" CausesValidation="false" Text="Pending" ConfirmMessage="Are you sure you want to continue?" OnClick="btnMoveToPending_Click" />
			<aspire:AspireButton runat="server" ButtonType="Default" FontAwesomeIcon="solid_spinner" ID="btnMoveToInProcess" CausesValidation="false" Text="In Process" ConfirmMessage="Are you sure you want to continue?" OnClick="btnMoveToInProcess_Click" />
			<aspire:AspireButton runat="server" ButtonType="Default" FontAwesomeIcon="solid_exclamation_triangle" ID="btnMoveToSpamJunk" CausesValidation="false" Text="Spam/Junk" ConfirmMessage="Are you sure you want to continue?" OnClick="btnMoveToSpamJunk_Click" />
			<aspire:AspireButton runat="server" ButtonType="Default" FontAwesomeIcon="regular_window_close" ID="btnClose" CausesValidation="false" Text="Close" ConfirmMessage="Are you sure you want to close?" OnClick="btnClose_Click" />
		</div>
		<aspire:AspireButton runat="server" ButtonType="Default" FontAwesomeIcon="solid_archive" ID="btnArchive" CausesValidation="false" Text="Archive" ConfirmMessage="Are you sure you want to archive?" OnClick="btnArchive_Click" />
		<aspire:AspireButton runat="server" ButtonType="Default" ID="btnUnarchive" CausesValidation="false" Text="Unarchive" ConfirmMessage="Are you sure you want to unarchive?" OnClick="btnUnarchive_Click" />
		<aspire:AspireButton runat="server" ButtonType="Default" ID="btnMarkAsRead" CausesValidation="false" Text="Mark as read" OnClick="btnMarkAsRead_Click" />
		<aspire:AspireButton runat="server" ButtonType="Default" ID="btnMarksAsUnread" CausesValidation="false" Text="Mark as unread" OnClick="btnMarksAsUnread_Click" />
		<aspire:AspireButton runat="server" ButtonType="Default" ID="btnMoveToDirectorGeneral" CssClass="pull-right" ConfirmMessage="Are you sure you want to move this feedback to DG's Box?" FontAwesomeIcon="solid_arrow_right" CausesValidation="false" Text="Move to DG Campus' Box" OnClick="btnMoveToDirectorGeneral_Click" />
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="tbFeedbackNo" Text="Reference No.:" />
				<aspire:AspireTextBox runat="server" ID="tbFeedbackNo" MaxLength="50" ReadOnly="true" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="tbFeedbackBoxType" Text="Box:" />
				<aspire:AspireTextBox runat="server" ID="tbFeedbackBoxType" MaxLength="1000" ReadOnly="true" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="tbFeedbackType" Text="Type:" />
				<aspire:AspireTextBox runat="server" ID="tbFeedbackType" ReadOnly="true" MaxLength="1000" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="tbFeedbackDate" Text="Submission Date:" />
				<aspire:AspireTextBox runat="server" ID="tbFeedbackDate" ReadOnly="true" MaxLength="1000" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="tbFromName" Text="Name:" />
				<aspire:AspireTextBox runat="server" ID="tbFromName" MaxLength="1000" ReadOnly="true" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="tbFromEmail" Text="Email:" />
				<aspire:AspireTextBox runat="server" ID="tbFromEmail" MaxLength="500" ReadOnly="true" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="tbFromUserType" Text="User Type:" />
				<aspire:AspireTextBox runat="server" ID="tbFromUserType" MaxLength="1000" ReadOnly="true" />
			</div>
		</div>
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" AssociatedControlID="ltrlFeedbackDetails" Text="Details:" />
		<div class="well">
			<asp:Literal runat="server" ID="ltrlFeedbackDetails" />
		</div>
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" AssociatedControlID="tbNotes" Text="Notes:" />
		<aspire:AspireTextBox runat="server" ID="tbNotes" MaxLength="100000" TextMode="MultiLine" Rows="3" />
		<div class="help-block">
			These notes will not be visible to any user and can be used for offical purpose (reminders/references/notes/etc).
		</div>
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" AssociatedControlID="lbAssignedToUserFeedbackMemberIDs" Text="Assigned To:" />
		<aspire:AspireListBox SelectionMode="Multiple" runat="server" ID="lbAssignedToUserFeedbackMemberIDs" ValidationGroup="Feedback" />
	</div>

	<asp:Repeater runat="server" ID="repeaterDiscussion" ItemType="Aspire.BL.Core.Feedbacks.Executive.UserFeedback.FeedbackDetailed.Discussion">
		<HeaderTemplate><strong>Discussion:</strong></HeaderTemplate>
		<ItemTemplate>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><%#: Item.CreatedByName %> <em class="pull-right"><%#: Item.CreatedDate.ToString("F") %></em></h3>
				</div>
				<div class="panel-body">
					<%# Item.Details %>
				</div>
			</div>
		</ItemTemplate>
	</asp:Repeater>
	<div class="form-group" id="divDiscussionAdd" runat="server">
		<aspire:AspireLabel runat="server" AssociatedControlID="tbDiscussionDetails" Text="Comments:" />
		<aspire:AspireTextBox CssClass="hidden" runat="server" ValidateRequestMode="Disabled" ID="tbDiscussionDetails" Rows="10" ValidationGroup="Discussion" TextMode="MultiLine" MaxLength="10485760" />
		<aspire:AspireCustomValidator runat="server" ValidateEmptyText="true" ID="cvtbDiscussionDetails" HighlightCssClass="has-error" ControlToValidate="tbDiscussionDetails" ValidationGroup="Discussion" ErrorMessage="This field is required." ClientValidationFunction="ValidateDiscussion" OnServerValidate="cvtbDiscussionDetails_ServerValidate" />
	</div>
	<div class="form-group text-center" runat="server" id="divSaveButtons">
		<aspire:AspireButton runat="server" ID="btnSave1" ValidationGroup="Feedback" Text="Save" ConfirmMessage="Are you sure you want to save?" OnClick="btnSave_Click" />
		<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="Feedbacks.aspx" />
	</div>
	<div class="form-group text-center" runat="server" id="divDiscussionAddButtons" visible="false">
		<aspire:AspireButton runat="server" ID="btnAddDiscussion" ValidationGroup="Discussion" Text="Add Comments" ConfirmMessage="Are you sure you want to add?" OnClick="btnAddDiscussion_Click" />
		<aspire:AspireLinkButton runat="server" ID="btnAddDiscussionCancel" CausesValidation="false" Text="Cancel" OnClick="btnAddDiscussionCancel_Click" />
	</div>
	<div class="form-group text-center" runat="server" id="divCancelButton">
		<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="Feedbacks.aspx" />
	</div>
	<script src="../../../Scripts/Plugins/summernote/summernote.min.js"></script>
	<script type="text/javascript">
		var tbDiscussionDetails = $("#<%= this.tbDiscussionDetails.ClientID %>");
		var closedValue ="<%= Aspire.Model.Entities.UserFeedback.Statuses.Closed.GetGuid().ToString() %>";
		function ValidateDiscussion(source, args) {
			var html = tbDiscussionDetails.summernote("code");
			var hiddenDiv = $("<div></div>").appendTo($("body")).hide().append(html);
			var text = hiddenDiv.text();
			hiddenDiv.remove();
			args.IsValid = text != undefined && text != null && text.trim().length > 0;
			if (args.IsValid)
				tbDiscussionDetails.val(html);
			else
				tbDiscussionDetails.val("");
			source.errormessage = "This field is required.";
			ApplyHighlightCss(source, "<%=this.tbDiscussionDetails.ClientID%>", args);
		}
		$(document).ready(function () {
			$("select.form-control").applySelect2();
			tbDiscussionDetails.summernote({
				height: 300,
				minHeight: 300,
				maxHeight: null,
				focus: false,
				codeviewFilter: true,
				codeviewIframeFilter: true,
				toolbar: [
					['style', ['style']],
					['font', ['bold', 'underline', 'clear']],
					['fontname', ['fontname']],
					['color', ['color']],
					['para', ['ul', 'ol', 'paragraph']],
					['table', ['table']],
					['insert', ['link', 'picture', 'video']]
				]
			});
			if (tbDiscussionDetails.prop("readonly"))
				tbDiscussionDetails.next().find(".note-editable").attr("contenteditable", false);
		});
	</script>
</asp:Content>
