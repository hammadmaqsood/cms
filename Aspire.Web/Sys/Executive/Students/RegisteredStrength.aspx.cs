﻿using Aspire.BL.Core.ExecutiveInformation.Executive.Students;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Executive.Students
{
	[AspirePage("70DD26D8-05AF-4BF6-8E42-DBB911DD2B25", UserTypes.Executive, "~/Sys/Executive/Students/RegisteredStrength.aspx", "Registered Strength", true, AspireModules.CourseRegistration)]
	public partial class RegisteredStrength : ExecutivePage
	{
		public override PageIcon PageIcon => AspireGlyphicons.user.GetIcon();
		public override string PageTitle => "Registered Students' Strength";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters();
				this.ddlDepartmentIDFilter.FillDepartments(this.ExecutiveIdentity.InstituteID, CommonListItems.All);
				this.ddlSemesterNoFilter.FillSemesterNos(CommonListItems.All);
				this.ddlGenderFilter.FillGenders(CommonListItems.All);
				this.ddlSectionFilter.FillSections(CommonListItems.All);
				this.ddlShiftFilter.FillShifts(CommonListItems.All);
				this.ddlCategoryIDFilter.FillCategories(CommonListItems.All);
				this.ddlStatusFilter.FillEnums<Model.Entities.Student.Statuses>(CommonListItems.All);
				if (this.ExecutiveIdentity.TryDemand(ExecutivePermissions.FeeManagement.Module, UserGroupPermission.PermissionValues.Allowed) != null)
					this.cblGroupBy.FillEnums<RegisteredStrengths.StudentsStrengthGroupByFields>().SetEnumValue(RegisteredStrengths.StudentsStrengthGroupByFields.Department);
				else
					this.cblGroupBy.FillEnums(RegisteredStrengths.StudentsStrengthGroupByFields.FeePaid).SetEnumValue(RegisteredStrengths.StudentsStrengthGroupByFields.Department);
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
			this.SetMaximumScriptTimeout();
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			if (departmentID == null)
				this.ddlProgramIDFilter.DataBind(CommonListItems.All);
			else
				this.ddlProgramIDFilter.FillPrograms(this.ExecutiveIdentity.InstituteID, departmentID, true, CommonListItems.All);
			this.ddlProgramIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterNoFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterNoFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSectionFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSectionFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{

			this.ddlShiftFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlShiftFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlGenderFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlGenderFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlCategoryIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlCategoryIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.cblGroupBy_OnSelectedIndexChanged(null, null);
		}

		protected void ddlStatusFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.cblGroupBy_OnSelectedIndexChanged(null, null);
		}

		protected void cblGroupBy_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetStudentsStrength();
		}

		private void GetStudentsStrength()
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var semesterNo = this.ddlSemesterNoFilter.GetSelectedEnumValue<SemesterNos>(null);
			var section = this.ddlSectionFilter.GetSelectedEnumValue<Sections>(null);
			var shift = this.ddlShiftFilter.GetSelectedEnumValue<Shifts>(null);
			var gender = this.ddlGenderFilter.GetSelectedEnumValue<Genders>(null);
			var category = this.ddlCategoryIDFilter.GetSelectedEnumValue<Categories>(null);
			var status = this.ddlStatusFilter.GetSelectedEnumValue<Model.Entities.Student.Statuses>(null);

			BL.Core.ExecutiveInformation.Executive.Students.RegisteredStrengths.StudentsStrengthGroupByFields? groupByFields = null;
			foreach (var field in this.cblGroupBy.GetSelectedEnumValues<BL.Core.ExecutiveInformation.Executive.Students.RegisteredStrengths.StudentsStrengthGroupByFields>())
			{
				if (groupByFields == null)
					groupByFields = field;
				else
					groupByFields = groupByFields.Value | field;
			}

			if (groupByFields == null)
			{
				this.AddErrorAlert("Select at-least one Column.");
				this.gvStrength.DataBindNull();
				return;
			}

			var dataTable = BL.Core.ExecutiveInformation.Executive.Students.RegisteredStrengths.GetStudentsStrength(semesterID, departmentID, programID, semesterNo, section, shift, gender, category, status, groupByFields.Value, this.ExecutiveIdentity.LoginSessionGuid);
			this.gvStrength.DataBind(dataTable.DefaultView);
		}

		protected void gvStrength_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.Footer)
				return;
			var dataTable = ((System.Data.DataView)this.gvStrength.DataSource).Table;
			var count = dataTable.Columns.Count;
			e.Row.Font.Bold = true;
			if (dataTable.Columns.Contains("Fee Paid"))
			{
				e.Row.Cells[0].Text = @"Total";
				e.Row.Cells[count - 2].Text = (dataTable.Compute("Sum(Students)", null) ?? 0).ToString();
				e.Row.Cells[count - 1].Text = (dataTable.Compute("Sum([Fee Paid])", null) ?? 0).ToString();
			}
			else
			{
				e.Row.Cells[count - 2].Text = @"Total";
				e.Row.Cells[count - 1].Text = (dataTable.Compute("Sum(Students)", null) ?? 0).ToString();
			}
		}
	}
}