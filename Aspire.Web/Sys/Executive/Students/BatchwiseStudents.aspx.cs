﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Executive.Students
{
	[AspirePage("D5125340-3FD4-4C98-A714-085686FADDB9", UserTypes.Executive, "~/Sys/Executive/Students/BatchwiseStudents", "Batchwise Students", true, AspireModules.StudentInformation)]
	public partial class BatchwiseStudents : ExecutivePage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.list);
		public override string PageTitle => "Batchwise Students";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlDepartmentIDFilter.FillDepartments(this.ExecutiveIdentity.InstituteID);
				this.ddlSemesterIDFilter.FillSemesters();
				this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			if (departmentID == null)
				this.ddlProgramFilter.DataBind(CommonListItems.All);
			else
				this.ddlProgramFilter.FillPrograms(this.ExecutiveIdentity.InstituteID, departmentID, true, CommonListItems.All);
			this.ddlProgramFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramFilter.SelectedValue.ToNullableInt();
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var students = BL.Core.ExecutiveInformation.Executive.Students.BatchwiseStudents.GetBatchwiseStudents(semesterID, departmentID, programID, this.ExecutiveIdentity.LoginSessionGuid);
			this.gvBatchwiseStudents.DataBind(students);
		}
	}
}