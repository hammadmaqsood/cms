﻿<%@ Page Language="C#" MasterPageFile="~/Sys/Executive/ExecutiveMaster.Master" AutoEventWireup="true" CodeBehind="RegisteredStrength.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Students.RegisteredStrength" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div id="container"></div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlProgramIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="ddlSemesterNoFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterNoFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterNoFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSectionFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlSectionFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlSectionFilter_OnSelectedIndexChanged" />
				</div>

				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShiftFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlShiftFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlShiftFilter_OnSelectedIndexChanged" />
				</div>

				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Gender:" AssociatedControlID="ddlGenderFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlGenderFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlGenderFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Category:" AssociatedControlID="ddlCategoryIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlCategoryIDFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlCategoryIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatusFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlStatusFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlStatusFilter_OnSelectedIndexChanged" />
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<aspire:AspireLabel runat="server" Text="Show Columns:" AssociatedControlID="cblGroupBy" />
				<aspire:AspireCheckBoxList runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" ID="cblGroupBy" AutoPostBack="True" OnSelectedIndexChanged="cblGroupBy_OnSelectedIndexChanged" ValidationGroup="Grouping" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="cblGroupBy" ErrorMessage="Select atleast one 'Group By' clause." ValidationGroup="Grouping" />
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" AutoGenerateColumns="True" ID="gvStrength" AllowSorting="False" AllowPaging="False" ShowFooter="True" OnRowDataBound="gvStrength_OnRowDataBound" />
	<script>
		init($("#<%=this.gvStrength.ClientID%>"));
	</script>
</asp:Content>
