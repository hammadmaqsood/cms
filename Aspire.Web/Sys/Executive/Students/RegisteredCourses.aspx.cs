﻿using Aspire.BL.Core.CourseRegistration.Executive;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Aspire.Model.Entities;

namespace Aspire.Web.Sys.Executive.Students
{
	[AspirePage("8BE38EA4-3FD8-4D8B-9D9A-60C80C46F2A4", UserTypes.Executive, "~/Sys/Executive/Students/RegisteredCourses.aspx", "Registered Courses", true, AspireModules.CourseRegistration)]
	public partial class RegisteredCourses : ExecutivePage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.info_sign);
		public override string PageTitle => "Registered Courses";

		public static string GetPageUrl(int studentID)
		{
			return typeof(RegisteredCourses).GetAspirePageAttribute().PageUrl.AttachQueryParam("StudentID", studentID);
		}

		private CourseRegistration.CourseRegistrationInfo _courseRegistrationInfo;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var studentID = this.Request.GetParameterValue<int>("StudentID");
				if (studentID == null)
				{
					Redirect<StudentSearch>();
					return;
				}

				this._courseRegistrationInfo = Aspire.BL.Core.CourseRegistration.Executive.CourseRegistration.GetCourseRegistrationInfo(studentID.Value, this.ExecutiveIdentity.LoginSessionGuid);
				if (_courseRegistrationInfo == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<StudentSearch>();
					return;
				}

				this.hlEnrollment.Text = this._courseRegistrationInfo.Enrollment;
				this.hlEnrollment.NavigateUrl = StudentProfile.GetPageUrl(this._courseRegistrationInfo.StudentID);
				this.lblStatus.Text = this._courseRegistrationInfo.StatusFullName;
				this.lblStatusDate.Text = (this._courseRegistrationInfo.StatusDate?.ToShortDateString()).ToNAIfNullOrEmpty();
				this.lblFatherName.Text = this._courseRegistrationInfo.FatherName;
				this.lblName.Text = this._courseRegistrationInfo.Name;
				this.lblIntakeSemester.Text = this._courseRegistrationInfo.IntakeSemesterID.ToSemesterString();
				this.lblProgramAlias.Text = this._courseRegistrationInfo.Program;
				this.lblRegistrationNo.Text = (this._courseRegistrationInfo.RegistrationNo?.ToString()).ToNAIfNullOrEmpty();
				this.repeaterStudentSemesters.DataBind(this._courseRegistrationInfo.StudentSemesters.OrderBy(ss => ss.SemesterID).ToList());
			}
		}

		protected List<Aspire.BL.Core.CourseRegistration.Executive.CourseRegistration.RegisteredCourse> GetRegisteredCourses(object studentSemester)
		{
			return this._courseRegistrationInfo.RegisteredCourses
				.Where(rc => rc.OfferedSemesterID == ((StudentSemester)studentSemester).SemesterID)
				.OrderBy(rc => rc.OfferedTitle).ToList();
		}

		protected string GetClassName(object studentSemester)
		{
			var semester = (StudentSemester)studentSemester;
			return AspireFormats.GetClassName(this._courseRegistrationInfo.Program, semester.SemesterNo, semester.Section, semester.Shift);
		}
	}
}