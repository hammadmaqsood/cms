﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="BatchwiseStudents.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Students.BatchwiseStudents" %>

<%@ Import Namespace="Aspire.Web.Sys.Executive.Students" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester: " AssociatedControlID="ddlSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Program: " AssociatedControlID="ddlProgramFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlProgramFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlProgramFilter_OnSelectedIndexChanged" />
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvBatchwiseStudents" AutoGenerateColumns="False">
		<Columns>
			<asp:TemplateField>
				<ItemTemplate><%#: Container.DataItemIndex + 1  %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Department" DataField="DepartmentAlias" />
			<asp:BoundField HeaderText="Program" DataField="ProgramAlias" />
			<asp:BoundField HeaderText="Intake" DataField="IntakeSemester" />
			<asp:BoundField HeaderText="Enrollment" DataField="Enrollment" />
			<asp:BoundField HeaderText="Reg#" DataField="RegistrationNo" />
			<asp:BoundField HeaderText="Name" DataField="Name" />
			<asp:BoundField HeaderText="Status" DataField="StatusFullName" />
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" Text="Details" NavigateUrl='<%# StudentProfile.GetPageUrl((int)this.Eval("StudentID")) %>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" Text="Registered Courses" NavigateUrl='<%# RegisteredCourses.GetPageUrl((int)this.Eval("StudentID")) %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
