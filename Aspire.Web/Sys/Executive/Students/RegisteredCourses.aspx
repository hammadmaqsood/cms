﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="RegisteredCourses.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Students.RegisteredCourses" %>

<%@ Import Namespace="Aspire.BL.Core.CourseRegistration.Executive" %>
<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="table-responsive">
		<table class="table table-bordered table-condensed tableCol4">
			<caption>Student Information</caption>
			<tr>
				<th>Enrollment</th>
				<td>
					<aspire:AspireHyperLink runat="server" ID="hlEnrollment" /></td>
				<th>Registration No.</th>
				<td>
					<aspire:Label runat="server" ID="lblRegistrationNo" /></td>
			</tr>
			<tr>
				<th>Name</th>
				<td>
					<aspire:Label runat="server" ID="lblName" /></td>
				<th>Father Name</th>
				<td>
					<aspire:Label runat="server" ID="lblFatherName" /></td>
			</tr>
			<tr>
				<th>Program</th>
				<td>
					<aspire:Label runat="server" ID="lblProgramAlias" /></td>
				<th>Intake Semester</th>
				<td>
					<aspire:Label runat="server" ID="lblIntakeSemester" /></td>
			</tr>
			<tr>
				<th>Status</th>
				<td>
					<aspire:Label runat="server" ID="lblStatus" /></td>
				<th>Status Date</th>
				<td>
					<aspire:Label runat="server" ID="lblStatusDate" /></td>
			</tr>
		</table>
	</div>
	<asp:Repeater runat="server" ID="repeaterStudentSemesters">
		<HeaderTemplate>
			<table class="table table-bordered table-responsive table-condensed">
				<caption>Registered Courses</caption>
		</HeaderTemplate>
		<ItemTemplate>
			<thead>
				<tr>
					<th colspan="10">
						<%#: ((short)Eval(nameof(StudentSemester.SemesterID))).ToSemesterString() %>
						<span class="pull-right"><%#: this.GetClassName( Container.DataItem) %></span>
					</th>
				</tr>
			</thead>
			<asp:Repeater runat="server" DataSource='<%# GetRegisteredCourses(Container.DataItem) %>'>
				<HeaderTemplate>
					<thead>
						<tr>
							<th>#</th>
							<th>Code</th>
							<th>Title</th>
							<th>Majors</th>
							<th>Credit Hours</th>
							<th>Offered Title</th>
							<th>Offered Class</th>
							<th>Teacher</th>
							<th>Status</th>
							<th>Fee</th>
						</tr>
					</thead>
					<tbody>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><%#: Container.ItemIndex+1 %></td>
						<td><%#: Eval(nameof(CourseRegistration.RegisteredCourse.RoadmapCourseCode)).ToNAIfNullOrEmpty() %></td>
						<td><%#: Eval(nameof(CourseRegistration.RegisteredCourse.RoadmapTitle)).ToNAIfNullOrEmpty() %></td>
						<td><%#: Eval(nameof(CourseRegistration.RegisteredCourse.RoadmapMajors)).ToNAIfNullOrEmpty() %></td>
						<td><%#: Eval(nameof(CourseRegistration.RegisteredCourse.RoadmapCreditHoursFullName)).ToNAIfNullOrEmpty() %></td>
						<td><%#: Eval(nameof(CourseRegistration.RegisteredCourse.OfferedTitle)).ToNAIfNullOrEmpty() %></td>
						<td><%#: Eval(nameof(CourseRegistration.RegisteredCourse.OfferedClass)).ToNAIfNullOrEmpty() %></td>
						<td><%#: Eval(nameof(CourseRegistration.RegisteredCourse.FacultyMemberName)).ToNAIfNullOrEmpty() %></td>
						<td><%#: Eval(nameof(CourseRegistration.RegisteredCourse.RegisteredCourseStatus)).ToNAIfNullOrEmpty() %></td>
						<td><%#: Eval(nameof(CourseRegistration.RegisteredCourse.StudentFeeStatusFullName)).ToNAIfNullOrEmpty() %></td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
					</tbody>
				</FooterTemplate>
			</asp:Repeater>
		</ItemTemplate>
		<FooterTemplate>
			</table>
		</FooterTemplate>
	</asp:Repeater>
</asp:Content>
