﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Executive.Students
{
	[AspirePage("23B7B38A-929B-40B9-AE15-36F4C59FC6AE", UserTypes.Executive, "~/Sys/Executive/Students/StudentProfile.aspx", "Student Profile", true, AspireModules.CourseRegistration)]
	public partial class StudentProfile : ExecutivePage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_user);
		public override string PageTitle => "Profile Information";

		public static string GetPageUrl(int studentID)
		{
			return GetAspirePageAttribute<StudentProfile>().PageUrl.AttachQueryParam("StudentID", studentID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var studentID = this.Request.GetParameterValue<int>("StudentID");
				if (studentID == null)
				{
					Redirect<StudentSearch>();
					return;
				}
				var student = Aspire.BL.Core.ExecutiveInformation.Executive.Students.StudentProfile.GetStudent(studentID.Value, this.AspireIdentity.LoginSessionGuid);
				if (student == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<StudentSearch>();
					return;
				}
				this.imgPicture.ImageUrl = Aspire.Web.Sys.Common.StudentPicture.GetImageUrl(student.StudentID, TimeSpan.Zero);
				var sections = new[]
				{
					new Section("Personal Information")
					{
						Fields = new List<SectionField>
						{
							new SectionField("Enrollment", student.Enrollment, "Registration No.", student.RegistrationNo.ToString()),
							new SectionField("Name", student.Name, "Program", student.AdmissionOpenProgram.Program.ProgramAlias),
							new SectionField("Status", student.StatusEnum?.ToFullName(), "Status Date", student.StatusDate?.ToShortDateString()),
							new SectionField("CNIC", student.CNIC, "Passport No.", student.PassportNo),
							new SectionField("Gender", student.GenderEnum.ToFullName(), "Category", student.CategoryEnum.ToFullName()),
							new SectionField("Date of Birth", student.DOB.ToShortDateString(), "Blood Group", student.BloodGroupEnum?.ToFullName()),
							new SectionField("Nationality", student.Nationality, "Country", student.Country),
							new SectionField("Province", student.Province, "District", student.District),
							new SectionField("Tehsil", student.Tehsil, "Domicile", student.Domicile),
							new SectionField("Area Type", student.AreaTypeEnum?.ToFullName(), "Service No. (If Naval)", student.ServiceNo),
							new SectionField("ETS Score Type", student.ETSTypeEnum?.ToFullName(), "ETS Score", student.ETSObtained != null ? $"{student.ETSObtained:N2}/{student.ETSTotal:N2}" : null),
							new SectionField("Physical Disability", student.PhysicalDisability),
						}
					},
					new Section("Contact Information")
					{
						Fields = new List<SectionField>
						{
							new SectionField("Personal Email", student.PersonalEmail, "University Email", student.UniversityEmail),
							new SectionField("Phone", student.Phone, "Mobile", student.Mobile),
							new SectionField("Present Address", student.CurrentAddress),
							new SectionField("Permanent Address", student.PermanentAddress),
						},
					},
					new Section("Emergency Information")
					{
						Fields = new List<SectionField>
						{
							new SectionField("Contact Name", student.EmergencyContactName, "Mobile", student.EmergencyMobile),
							new SectionField("Phone", student.EmergencyPhone, "Relationship", student.EmergencyRelationship),
							new SectionField("Address", student.EmergencyAddress),
						},
					},
					new Section("Father Information")
					{
						Fields = new List<SectionField>
						{
							new SectionField("Name", student.FatherName, "CNIC", student.FatherCNIC),
							new SectionField("Passport No.", student.FatherPassportNo, "Designation", student.FatherDesignation),
							new SectionField("Department", student.FatherDepartment, "Organization", student.FatherOrganization),
							new SectionField("Service No. (If Naval)", student.FatherServiceNo, "Office Phone", student.FatherOfficePhone),
							new SectionField("Home Phone", student.FatherHomePhone, "Mobile", student.FatherMobile),
							new SectionField("Fax", student.FatherFax),
						},
					},
					new Section("Sponsor Information")
					{
						Fields = new List<SectionField>
						{
							new SectionField("Sponsored By", student.SponsoredByEnum?.ToFullName(), "Name", student.SponsorName),
							new SectionField("CNIC", student.SponsorCNIC, "Passport No.", student.SponsorPassportNo),
							new SectionField("Relationship With Guardian", student.SponsorRelationshipWithGuardian, "Designation", student.SponsorDesignation),
							new SectionField("Department", student.SponsorDepartment, "Organization", student.SponsorOrganization),
							new SectionField("Service No. (If Naval)", student.SponsorServiceNo, "Phone Home", student.SponsorPhoneHome),
							new SectionField("Mobile", student.SponsorMobile, "Phone Office", student.SponsorPhoneOffice),
							new SectionField("Fax", student.SponsorFax),
						},
					},
				};

				this.ltrlStudentInfo.Mode = LiteralMode.PassThrough;
				this.ltrlStudentInfo.Text = string.Join("", sections.Select(s => s.ToString()));
				this.gvAcademicRecords.DataBind(student.StudentAcademicRecords.OrderBy(ar => ar.DegreeType).ToList());
			}
		}

		private sealed class Section
		{
			private string SectionName { get; }
			public List<SectionField> Fields { private get; set; }

			public Section(string sectionName)
			{
				this.SectionName = sectionName;
			}

			public override string ToString()
			{
				return $@"<div class=""table-responsive""><table class=""table table-bordered table-condensed tableCol4"">
<caption>{this.SectionName.HtmlEncode()}</caption>
<tbody>{string.Join("", this.Fields.Select(f => f.ToString()))}</tbody>
</table></div>";
			}
		}

		private sealed class SectionField
		{
			public SectionField(string label1, string value1)
			{
				this.Label1 = label1;
				this.Value1 = value1;
			}

			public SectionField(string label1, string value1, string label2, string value2)
				: this(label1, value1)
			{
				this.Label2 = label2;
				this.Value2 = value2;
			}

			private string Label1 { get; }
			private string Value1 { get; }
			private string Label2 { get; }
			private string Value2 { get; }

			public override string ToString()
			{
				if (this.Label2 == null && this.Value2 == null)
					return $"<tr><th>{this.Label1.HtmlEncode()}</th><td colspan=\"3\">{(this.Value1?.HtmlEncode()).ToNAIfNullOrEmpty()}</td></tr>";
				return $"<tr><th>{this.Label1.HtmlEncode()}</th><td>{(this.Value1?.HtmlEncode()).ToNAIfNullOrEmpty()}</td><th>{this.Label2.HtmlEncode()}</th><td>{(this.Value2?.HtmlEncode()).ToNAIfNullOrEmpty()}</td></tr>";
			}
		}
	}
}