﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="StudentSearch.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Students.StudentSearch" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Web.Sys.Executive.Students" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:Panel runat="server" DefaultButton="btnSearch">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Intake Semester:" AssociatedControlID="ddlSemesterFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterFilter" ValidationGroup="SearchStudent" />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlAdmissionType" />
					<aspire:AspireDropDownList runat="server" ID="ddlAdmissionType" ValidationGroup="SearchStudent" />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="tbEnrollmentFilter" />
					<aspire:AspireTextBox runat="server" ID="tbEnrollmentFilter" ValidationGroup="SearchStudent" />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Registration No.:" AssociatedControlID="tbRegistrationNoFilter" />
					<aspire:AspireTextBox runat="server" ID="tbRegistrationNoFilter" ValidationGroup="SearchStudent" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbNameFilter" />
					<aspire:AspireTextBox runat="server" ID="tbNameFilter" ValidationGroup="SearchStudent" TextTransform="UpperCase" />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Father Name:" AssociatedControlID="tbFatherNameFilter" />
					<aspire:AspireTextBox runat="server" ID="tbFatherNameFilter" ValidationGroup="SearchStudent" TextTransform="UpperCase" />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlProgramFilter" ValidationGroup="SearchStudent" />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Gender:" AssociatedControlID="ddlGenderFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlGenderFilter" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Nationality:" AssociatedControlID="ddlNationality" />
					<aspire:AspireDropDownList runat="server" ID="ddlNationality" />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatus" />
					<aspire:AspireDropDownList runat="server" ID="ddlStatus" />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="&nbsp;" AssociatedControlID="btnSearch" />
					<div>
						<aspire:AspireButton runat="server" Text="Search" ButtonType="Primary" ID="btnSearch" ValidationGroup="SearchStudent" OnClick="btnSearch_OnClick" />
					</div>
				</div>
			</div>
		</div>
	</asp:Panel>
	<aspire:AspireGridView runat="server" ItemType="Aspire.BL.Core.ExecutiveInformation.Executive.Students.StudentSearch.SearchStudentsResult.Student" ID="gvSearchStudent" AutoGenerateColumns="False" AllowSorting="True" OnPageIndexChanging="gvSearchStudent_OnPageIndexChanging" OnPageSizeChanging="gvSearchStudent_OnPageSizeChanging" OnSorting="gvSearchStudent_OnSorting" PageSize="100">
		<Columns>
			<asp:TemplateField HeaderText="Intake Semester" SortExpression="IntakeSemesterID">
				<ItemTemplate>
					<%#: Item.IntakeSemesterID.ToSemesterString() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Type" SortExpression="AdmissionTypeEnum">
				<ItemTemplate>
					<%#: Item.AdmissionTypeEnum.ToFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Enrollment" SortExpression="Enrollment">
				<ItemTemplate>
					<%#: Item.Enrollment %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Name" SortExpression="Name">
				<ItemTemplate>
					<%#: Item.Name %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Program" SortExpression="ProgramAlias">
				<ItemTemplate>
					<%#: Item.ProgramAlias %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Gender" SortExpression="GenderEnum">
				<ItemTemplate>
					<%#: Item.GenderEnum.ToFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="FatherName" SortExpression="FatherName">
				<ItemTemplate>
					<%#: Item.FatherName %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Nationality" SortExpression="Nationality">
				<ItemTemplate>
					<%#: Item.Nationality %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Status" SortExpression="StatusEnum">
				<ItemTemplate>
					<%#: Item.StatusEnum?.ToFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<div class="btn-group">
						<a href="#" class="dropdown-toggle text-nowrap" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions<span class="caret"></span></a>
						<ul class="dropdown-menu dropdown-menu-right">
							<li>
								<aspire:AspireHyperLink runat="server" Target="_blank" Text="Student Profile" NavigateUrl='<%# StudentProfile.GetPageUrl(Item.StudentID) %>' />
							</li>
							<li>
								<aspire:AspireHyperLink runat="server" Target="_blank" Text="Registered Courses" NavigateUrl='<%# RegisteredCourses.GetPageUrl(Item.StudentID) %>' />
							</li>
						</ul>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
