﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Executive.Students
{
	[AspirePage("83D37A7C-68C5-49C0-BED7-15D37C037C30", UserTypes.Executive, "~/Sys/Executive/Students/StudentSearch", "Search Student(s)", true, AspireModules.CourseRegistration)]
	public partial class StudentSearch : ExecutivePage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_search);
		public override string PageTitle => "Search Students";

		public static string GetPageUrl(short? intakeSemesterID, Model.Entities.Student.AdmissionTypes? admissionTypeEnum, string enrollment, int? registrationNo, string name, string fatherName, int? programID, Genders? genderEnum, string nationality, Model.Entities.Student.Statuses? statusEnum, bool statusEnumNullAsAll, int? pageIndex, int? pageSize, string sortExpression, SortDirection? sortDirection, bool search)
		{
			return typeof(StudentSearch).GetAspirePageAttribute().PageUrl.AttachQueryParams(
					("IntakeSemesterID", intakeSemesterID),
					("AdmissionTypeEnum", admissionTypeEnum),
					("Enrollment", enrollment),
					("RegistrationNo", registrationNo),
					("Name", name),
					("FatherName", fatherName),
					("ProgramID", programID),
					("GenderEnum", genderEnum),
					("Nationality", nationality),
					("StatusEnum", statusEnum),
					("StatusEnumNullAsAll", statusEnumNullAsAll),
					("PageIndex", pageIndex),
					("PageSize", pageSize),
					("SortExpression", sortExpression),
					("SortDirection", sortDirection),
					("Search", search));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(this.GetParameterValue("SortExpression") ?? nameof(Model.Entities.Student.Enrollment), this.GetParameterValue<SortDirection>("SortDirection") ?? SortDirection.Ascending);
				this.ddlSemesterFilter.FillSemesters(CommonListItems.All).SetSelectedValueIfNotPostback("IntakeSemesterID");
				this.ddlAdmissionType.FillAdmissionTypes(CommonListItems.All).SetSelectedEnumValueIfNotPostback<Model.Entities.Student.AdmissionTypes>("AdmissionTypeEnum");
				this.tbEnrollmentFilter.SetTextIfNotPostback("Enrollment");
				this.tbRegistrationNoFilter.SetTextIfNotPostback("RegistrationNo");
				this.tbNameFilter.SetTextIfNotPostback("Name");
				this.tbFatherNameFilter.SetTextIfNotPostback("FatherName");
				this.ddlProgramFilter.FillPrograms(this.ExecutiveIdentity.InstituteID, null, true, CommonListItems.All).SetSelectedValueIfNotPostback("ProgramID");
				this.ddlGenderFilter.FillGenders(CommonListItems.All).SetSelectedEnumValueIfNotPostback<Genders>("GenderEnum");
				var nationalities = new[] { "PAKISTANI", "FOREIGNERS" };
				this.ddlNationality.DataBind(nationalities, CommonListItems.All).SetSelectedValueIfNotPostback("Nationality");
				this.ddlStatus.FillStudentStatuses(CommonListItems.None);
				this.ddlStatus.AddStaticListItem(CommonListItems.All);
				var statusEnum = this.GetParameterValue<Model.Entities.Student.Statuses>("StatusEnum");
				if (this.GetParameterValue<bool>("StatusEnumNullAsAll") == true)
				{
					if (statusEnum == null)
						this.ddlStatus.SelectedIndex = 0;
					else
						this.ddlStatus.SetEnumValue(statusEnum.Value);
				}
				else
				{
					if (statusEnum == null)
						this.ddlStatus.SelectedIndex = 1;
					else
						this.ddlStatus.SetEnumValue(statusEnum.Value);
				}
				this.gvSearchStudent.PageSize = this.GetParameterValue<int>("PageSize") ?? this.gvSearchStudent.PageSize;
				if (this.GetParameterValue<bool>("Search") == true)
					this.btnSearch_OnClick(null, null);
			}
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
				this.GetSearchStudents(this.GetParameterValue<int>("PageIndex") ?? 0);
			else
				this.GetSearchStudents(0);
		}

		protected void gvSearchStudent_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetSearchStudents(e.NewPageIndex);
		}

		protected void gvSearchStudent_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvSearchStudent.PageSize = e.NewPageSize;
			this.GetSearchStudents(0);
		}

		protected void gvSearchStudent_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetSearchStudents(0);
		}

		private void GetSearchStudents(int pageIndex)
		{
			var semesterID = this.ddlSemesterFilter.SelectedItem.Value.ToNullableShort();
			var admissionTypeEnum = this.ddlAdmissionType.GetSelectedEnumValue<Model.Entities.Student.AdmissionTypes>(null);
			var enrollment = this.tbEnrollmentFilter.Text.TrimAndMakeItNullIfEmpty();
			var registrationNo = this.tbRegistrationNoFilter.Text.ToNullableInt();
			var name = this.tbNameFilter.Text.TrimAndMakeItNullIfEmpty();
			var fatherName = this.tbFatherNameFilter.Text.TrimAndMakeItNullIfEmpty();
			var programID = this.ddlProgramFilter.SelectedValue.ToNullableInt();
			var genderEnum = this.ddlGenderFilter.GetSelectedEnumValue<Genders>(null);
			var nationality = this.ddlNationality.SelectedValue.ToNullIfWhiteSpace();
			var statusEnum = this.ddlStatus.GetSelectedEnumValue<Model.Entities.Student.Statuses>(null);
			var statusEnumNullAsAll = this.ddlStatus.SelectedIndex == 0;

			var result = BL.Core.ExecutiveInformation.Executive.Students.StudentSearch.SearchStudents(semesterID, admissionTypeEnum, enrollment, registrationNo, name, fatherName, programID, genderEnum, nationality, statusEnum, statusEnumNullAsAll, pageIndex, this.gvSearchStudent.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.ExecutiveIdentity.LoginSessionGuid);
			this.gvSearchStudent.DataBind(result.Students, pageIndex, result.VirtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}
	}
}