﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="StudentProfile.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Students.StudentProfile" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="text-center">
		<asp:Image runat="server" ID="imgPicture" Width="100px" CssClass="img img-thumbnail" AlternateText="Photo" />
	</div>
	<asp:Literal runat="server" ID="ltrlStudentInfo" />
	<aspire:AspireGridView Caption="Academic Record" runat="server" ID="gvAcademicRecords" AutoGenerateColumns="False">
		<Columns>
			<asp:TemplateField HeaderText="Degree">
				<ItemTemplate>
					<%#: ((Aspire.Model.Entities.Common.DegreeTypes)this.Eval("DegreeType")).ToFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Institute" DataField="Institute" />
			<asp:BoundField HeaderText="Board/University" DataField="BoardUniversity" />
			<asp:BoundField HeaderText="Subjects" DataField="Subjects" />
			<asp:TemplateField HeaderText="Marks">
				<ItemTemplate>
					<%#: Eval("ObtainedMarks","{0:#.##}") %>/<%#: Eval("TotalMarks","{0:#.##}") %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Percentage" DataField="Percentage" DataFormatString="{0:#.##}%" />
			<asp:BoundField HeaderText="Passing Year" DataField="PassingYear" />
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
