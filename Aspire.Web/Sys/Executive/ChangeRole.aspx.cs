﻿using Aspire.Lib.Helpers;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Logins.Staff;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Executive
{
	[AspirePage("90B1B173-0DFC-4ED3-9CBC-821424450AAA", UserTypes.Executive, "~/Sys/Executive/ChangeRole.aspx", null, false, AspireModules.None)]
	public partial class ChangeRole : ExecutivePage
	{
		public override PageIcon PageIcon => null;
		public override string PageTitle => null;
		public static string GetPageUrl(int userRoleID)
		{
			return GetAspirePageAttribute<ChangeRole>().PageUrl.AttachQueryParam("UserRoleID", userRoleID);
		}

		public static void Redirect(int userRoleID)
		{
			Redirect(GetPageUrl(userRoleID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			var newUserRoleID = this.Request.GetParameterValue<int>("UserRoleID");
			if (newUserRoleID == null || this.ExecutiveIdentity.Roles.All(r => r.Key != newUserRoleID))
			{
				Redirect<Login>();
				return;
			}
			var loginResult = Aspire.BL.Core.LoginManagement.Executive.ChangeRole(this.ExecutiveIdentity.LoginSessionGuid, newUserRoleID.Value, this.UserAgent, this.ClientIPAddress);
			if (loginResult.LoginFailureReason != null)
				Logoff.Redirect(UserLoginHistory.LogOffReasons.LogOff);
			else
			{
				var loginHistory = loginResult.LoginHistory;
				var executiveIdentity = new ExecutiveIdentity(loginHistory.Username, loginHistory.Name, loginHistory.Email, loginHistory.UserLoginHistoryID, loginHistory.LoginSessionGuid, loginHistory.UserID, loginHistory.UserRoleID, loginHistory.InstituteID, loginHistory.InstituteName, loginHistory.InstituteShortName, loginHistory.InstituteAlias, loginHistory.Roles.ToDictionary(r => r.UserRoleID, r => r.InstituteAlias), this.ClientIPAddress, this.UserAgent);
				SessionHelper.CreateUserSession(executiveIdentity, GetPageUrl<PrepareSession>());
			}
		}
	}
}