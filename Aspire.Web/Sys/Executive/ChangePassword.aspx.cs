﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Executive
{
	[AspirePage("2264168f-5287-4263-b7be-1769d5f8194c", UserTypes.Executive, "~/Sys/Executive/ChangePassword.aspx", "Change Password", true, AspireModules.None)]
	public partial class ChangePassword : ExecutivePage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.lock_);
		public override string PageTitle => "Change Password";

		protected void Page_Load(object sender, EventArgs e)
		{
		}
	}
}