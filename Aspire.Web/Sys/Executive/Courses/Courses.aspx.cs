﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;
using Aspire.Lib.Helpers;
using Aspire.Model.Entities;

namespace Aspire.Web.Sys.Executive.Courses
{
	[AspirePage("C783EBDA-A757-4A5D-A5A5-001562FABF9A", UserTypes.Executive, "~/Sys/Executive/Courses/Courses", "Offered Courses", true, AspireModules.CourseRegistration)]
	public partial class Courses : ExecutivePage
	{
		public override PageIcon PageIcon => AspireGlyphicons.list.GetIcon();
		public override string PageTitle => "Offered Courses";
		//public short? SemesterID => this.Request.GetParameterValue<short>("SemesterID");
		//public int? DepartmentID => this.Request.GetParameterValue<int>("DepartmentID");
		//public int? ProgramID => this.Request.GetParameterValue<int>("ProgramID");
		//public short? SemesterNo => this.Request.GetParameterValue<short>("SemesterNo");
		//public int? Section => this.Request.GetParameterValue<int>("Section");
		//public byte? Shift => this.Request.GetParameterValue<byte>("Shift");
		//public Guid? ReadinessTypeEnum => this.Request.GetParameterValue<Guid>("ReadinessType");
		//public bool? IsFromOnlineTeachingReadinessForm => this.Request.GetParameterValue<bool>("IsFromOnlineTeachingReadinessForm");
		private short? SemesterID
		{
			get => (short?)this.ViewState[nameof(this.SemesterID)];
			set => this.ViewState[nameof(this.SemesterID)] = value;
		}

		private int? DepartmentID
		{
			get => (int?)this.ViewState[nameof(this.DepartmentID)];
			set => this.ViewState[nameof(this.DepartmentID)] = value;
		}

		private int? ProgramID
		{
			get => (int?)this.ViewState[nameof(this.ProgramID)];
			set => this.ViewState[nameof(this.ProgramID)] = value;
		}

		private short? SemesterNo
		{
			get => (short?)this.ViewState[nameof(this.SemesterNo)];
			set => this.ViewState[nameof(this.SemesterNo)] = value;
		}

		private int? Section
		{
			get => (int?)this.ViewState[nameof(this.Section)];
			set => this.ViewState[nameof(this.Section)] = value;
		}

		private byte? Shift
		{
			get => (byte?)this.ViewState[nameof(this.Shift)];
			set => this.ViewState[nameof(this.Shift)] = value;
		}

		private Guid? ReadinessType
		{
			get => (Guid?)this.ViewState[nameof(this.ReadinessType)];
			set => this.ViewState[nameof(this.ReadinessType)] = value;
		}

		private bool? IsFromOnlineTeachingReadinessForm
		{
			get => (bool?)this.ViewState[nameof(this.IsFromOnlineTeachingReadinessForm)];
			set => this.ViewState[nameof(this.IsFromOnlineTeachingReadinessForm)] = value;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var semesterID = this.GetParameterValue<short>(nameof(this.SemesterID));
				var departmentID = this.GetParameterValue<int>(nameof(this.DepartmentID));
				var programID = this.GetParameterValue<int>(nameof(this.ProgramID));
				var semesterNo = this.GetParameterValue<short>(nameof(this.SemesterNo));
				var section = this.GetParameterValue<int>(nameof(this.Section));
				var shift = this.GetParameterValue<byte>(nameof(this.Shift));
				var readinessType = this.GetParameterValue<Guid>(nameof(this.ReadinessType));
				var isFromOnlineTeachingReadinessForm = this.GetParameterValue<bool>(nameof(this.IsFromOnlineTeachingReadinessForm));
				this.SemesterID = semesterID;
				this.DepartmentID = departmentID;
				this.ProgramID = programID;
				this.SemesterNo = semesterNo;
				this.Section = section;
				this.Shift = shift;
				this.ReadinessType = readinessType;
				this.IsFromOnlineTeachingReadinessForm = isFromOnlineTeachingReadinessForm;

				this.ViewState.SetSortProperties("Title", SortDirection.Ascending);
				this.ddlSemesterIDFilter.FillSemesters();
				if (this.SemesterID != null)
					this.ddlSemesterIDFilter.SelectedValue = this.SemesterID.ToString();
				this.ddlDepartmentIDFilter.FillDepartments(this.ExecutiveIdentity.InstituteID, CommonListItems.All).SetSelectedValueIfExists(this.DepartmentID.ToString());
				this.ddlSemesterNoFilter.FillSemesterNos(CommonListItems.All).SetSelectedValueIfExists(this.SemesterNo.ToString());
				this.ddlSectionFilter.FillSections(CommonListItems.All).SetSelectedValueIfNotPostback("Section").SetSelectedValueIfExists(this.Section.ToString());
				this.ddlShiftFilter.FillShifts(CommonListItems.All).SetSelectedValueIfNotPostback("Shift").SetSelectedValueIfExists(this.Shift.ToString());
				this.ddlReadinessType.FillGuidEnums<OnlineTeachingReadiness.ReadinessTypes>(CommonListItems.All).SetSelectedValueIfExists(this.ReadinessType.ToString());
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);

			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var facultyMembers = BL.Core.Common.Lists.GetFacultyMembersList(this.ExecutiveIdentity.InstituteID, semesterID);
			this.ddlFacultyMemberID.DataBind(facultyMembers, CommonListItems.All);
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			if (departmentID == null)
				this.ddlProgramIDFilter.DataBind(CommonListItems.All);
			else
				this.ddlProgramIDFilter.FillPrograms(this.ExecutiveIdentity.InstituteID, departmentID, true, CommonListItems.All).SetSelectedValueIfExists(this.ProgramID.ToString());
			this.ddlProgramIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterNoFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterNoFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSectionFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSectionFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlShiftFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlShiftFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlFacultyMemberID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlFacultyMemberID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlReadinessType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlReadinessType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetOfferedCourses(0);
		}

		protected void gvCourses_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetOfferedCourses(0);
		}


		protected void gvCourses_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvCourses.PageSize = e.NewPageSize;
			this.GetOfferedCourses(0);
		}


		protected void gvCourses_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetOfferedCourses(e.NewPageIndex);
		}

		private void GetOfferedCourses(int pageIndex)
		{
			this.gvCourses.PageSize = 1000;
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var semesterNo = this.ddlSemesterNoFilter.GetSelectedEnumValue<SemesterNos>(null);
			var section = this.ddlSectionFilter.GetSelectedEnumValue<Sections>(null);
			var shift = this.ddlShiftFilter.GetSelectedEnumValue<Shifts>(null);
			var facultyMemberID = this.ddlFacultyMemberID.SelectedValue.ToNullableInt();
			var readinessTypeEnum = this.ddlReadinessType.GetSelectedNullableGuidEnum<OnlineTeachingReadiness.ReadinessTypes>();
			var courses = BL.Core.CourseOffering.Executive.OfferedCourses.GetOfferedCourses(semesterID, departmentID, programID, semesterNo, section, shift, facultyMemberID, readinessTypeEnum, IsFromOnlineTeachingReadinessForm, pageIndex, this.gvCourses.PageSize, out var virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.ExecutiveIdentity.LoginSessionGuid);
			this.gvCourses.DataBind(courses, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
			this.gvCourses.Columns[5].Visible = this.IsFromOnlineTeachingReadinessForm == null || !this.IsFromOnlineTeachingReadinessForm.Value;
			this.gvCourses.Columns[6].Visible = this.IsFromOnlineTeachingReadinessForm != null && this.IsFromOnlineTeachingReadinessForm.Value;
			this.gvCourses.Columns[7].Visible = this.IsFromOnlineTeachingReadinessForm != null && this.IsFromOnlineTeachingReadinessForm.Value;
			this.gvCourses.Columns[9].Visible = this.IsFromOnlineTeachingReadinessForm != null && this.IsFromOnlineTeachingReadinessForm.Value;

		}


	}
}