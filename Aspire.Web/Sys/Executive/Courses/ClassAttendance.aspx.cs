﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Executive.Courses
{
	[AspirePage("88749CF6-A7AF-442F-B9B2-335146CFE63E", UserTypes.Executive, "~/Sys/Executive/Courses/ClassAttendance", "Class Attendance", true, AspireModules.None)]
	public partial class ClassAttendance : ExecutivePage
	{
		public override PageIcon PageIcon => AspireGlyphicons.list.GetIcon();
		public override string PageTitle => "View Class Attendance";

		private int? OfferedCourseID => this.Request.GetParameterValue<int>("OfferedCourseID");

		public static string GetPageUrl(int offeredCourseID)
		{
			return GetPageUrl<ClassAttendance>().AttachQueryParam("OfferedCourseID", offeredCourseID);
		}

		public static void Redirect(int offeredCourseID)
		{
			Redirect(GetPageUrl(offeredCourseID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.OfferedCourseID == null)
				{
					Redirect<Courses>();
					return;
				}

				var offeredCourseAttendance = BL.ClassAttendance.Faculty.GetOfferedCourseAttendances(this.OfferedCourseID.Value);
				if (offeredCourseAttendance == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Courses>();
					return;
				}
				this.hlSummary.NavigateUrl = Executive.Courses.ClassAttendanceSummary.GetPageUrl(offeredCourseAttendance.OfferedCourseID);
				this.lblCourseTitle.Text = $"[{offeredCourseAttendance.CourseCode}] {offeredCourseAttendance.Title}";
				this.lblTeacherName.Text = offeredCourseAttendance.TeacherName.ToNAIfNullOrEmpty();
				this.lblCreditHrs.Text = offeredCourseAttendance.CreditHours.ToCreditHoursFullName();
				this.lblContactHrs.Text = offeredCourseAttendance.ContactHours.ToCreditHoursFullName();
				this.lblClass.Text = offeredCourseAttendance.ClassName;
				this.lblSemesterID.Text = offeredCourseAttendance.SemesterID.ToSemesterString();
				if (offeredCourseAttendance.Details.Any())
					this.gvAttendanceRecord.ShowFooter = true;
				this.gvAttendanceRecord.DataBind(offeredCourseAttendance.Details);
				if (this.gvAttendanceRecord.ShowFooter)
				{
					var hours = offeredCourseAttendance.Details.Sum(d => d.Hours);
					this.gvAttendanceRecord.FooterRow.Cells[3].Text = hours.ToString("N");
				}
			}
		}
	}
}