﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Executive.Courses
{
	[AspirePage("DA042B83-86A6-4D39-AC72-80CC172B4CB2", UserTypes.Executive, "~/Sys/Executive/Courses/OnlineTeachingReadinessForm.aspx", "Online Teaching Readiness Form", true, AspireModules.CourseOffering)]
	public partial class OnlineTeachingReadinessForm : ExecutivePage
	{
		public override PageIcon PageIcon => AspireGlyphicons.globe.GetIcon();
		public override string PageTitle => "Online Teaching Readiness Form";

		public static string GetPageUrl(int offeredCourseID)
		{
			return typeof(OnlineTeachingReadinessForm).GetAspirePageAttribute().PageUrl.AttachQueryParam(nameof(OfferedCourseID), offeredCourseID);
		}

		public static void Redirect(int offeredCourseID)
		{
			Redirect(GetPageUrl(offeredCourseID));
		}

		private int OfferedCourseID
		{
			get => (int)this.ViewState[nameof(this.OfferedCourseID)];
			set => this.ViewState[nameof(this.OfferedCourseID)] = value;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var offeredCourseID = this.GetParameterValue<int>(nameof(this.OfferedCourseID));
				if (offeredCourseID == null)
				{
					Redirect<OnlineTeachingReadinessOfferedCourses>();
					return;
				}

				var offeredCourse = BL.Core.CourseOffering.Executive.OnlineTeachingReadinessOfferedCourses.GetOfferedCourse(offeredCourseID.Value, this.ExecutiveIdentity.LoginSessionGuid);
				if (offeredCourse == null)
				{
					Redirect<OnlineTeachingReadinessOfferedCourses>();
					return;
				}

				this.OfferedCourseID = offeredCourse.OfferedCourseID;

				this.SetControlProperties();
				this.tbCourseCode.Text = offeredCourse.CourseCode;
				this.tbCourseTitle.Text = offeredCourse.Title;
				this.tbClassName.Text = offeredCourse.Class;
				this.tbCreditContactHours.Text = $"{offeredCourse.CreditHours.ToCreditHoursFullName()} / {offeredCourse.ContactHours.ToCreditHoursFullName()}";
				this.tbFacultyMemberType.Text = offeredCourse.FacultyTypeEnum?.ToFullName();
				this.tbFacultyMemberName.Text = offeredCourse.FacultyMemberName;
				this.tbFacultyMemberDepartmentAlias.Text = offeredCourse.FacultyMemberDepartmentAlias;
				this.ReadinessTypeEnum = offeredCourse.OnlineTeachingReadiness?.ReadinessTypeEnum ?? OnlineTeachingReadiness.ReadinessTypes.FullyReady;
				const string decimalFormat = "#.#";
				if (offeredCourse.OnlineTeachingReadiness != null)
				{
					this.EditingAllowed = false;
					switch (offeredCourse.OnlineTeachingReadiness.ReadinessTypeEnum)
					{
						case OnlineTeachingReadiness.ReadinessTypes.NotReady:
							this.ddlNotReadyAlreadyDeliveredFaceToFaceContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.NotReadyAlreadyDeliveredFaceToFaceContactHours?.ToString(decimalFormat);
							this.ddlNotReadyRemainingContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.NotReadyRemainingContactHours?.ToString(decimalFormat);
							this.tbNotReadyTotalContactHours.Text = offeredCourse.OnlineTeachingReadiness.NotReadyTotalContactHours?.ToString(decimalFormat);
							this.ddlNotReadyTheoreticalContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.NotReadyTheoreticalContactHours?.ToString(decimalFormat);
							this.ddlNotReadyLabContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.NotReadyLabContactHours?.ToString(decimalFormat);
							this.ddlNotReadyFieldStudyContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.NotReadyFieldStudyContactHours?.ToString(decimalFormat);
							this.ddlNotReadyOtherContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.NotReadyOtherContactHours?.ToString(decimalFormat);
							this.tbNotReadyReasons.Text = offeredCourse.OnlineTeachingReadiness.NotReadyReasons;

							this.ddlNotReadyAlreadyDeliveredFaceToFaceContactHours.Enabled = this.EditingAllowed;
							this.ddlNotReadyRemainingContactHours.Enabled = this.EditingAllowed;
							this.tbNotReadyTotalContactHours.ReadOnly = !this.EditingAllowed;
							this.ddlNotReadyTheoreticalContactHours.Enabled = this.EditingAllowed;
							this.ddlNotReadyLabContactHours.Enabled = this.EditingAllowed;
							this.ddlNotReadyFieldStudyContactHours.Enabled = this.EditingAllowed;
							this.ddlNotReadyOtherContactHours.Enabled = this.EditingAllowed;
							this.tbNotReadyReasons.ReadOnly = !this.EditingAllowed;
							break;
						case OnlineTeachingReadiness.ReadinessTypes.PartiallyReady:
							this.ddlPartiallyReadyAlreadyDeliveredFaceToFaceContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.PartiallyReadyAlreadyDeliveredFaceToFaceContactHours?.ToString(decimalFormat);
							this.ddlPartiallyReadyToBeDeliveredOnlineContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.PartiallyReadyToBeDeliveredOnlineContactHours?.ToString(decimalFormat);
							this.ddlPartiallyReadyCoveredOnlineTheoreticalContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.PartiallyReadyCoveredOnlineTheoreticalContactHours?.ToString(decimalFormat);
							this.tbPartiallyReadyTotalContactHours.Text = offeredCourse.OnlineTeachingReadiness.PartiallyReadyTotalContactHours?.ToString(decimalFormat);
							this.ddlPartiallyReadyCoveredOnlineLabContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.PartiallyReadyCoveredOnlineLabContactHours?.ToString(decimalFormat);
							this.ddlPartiallyReadyCoveredOnlineFieldStudyContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.PartiallyReadyCoveredOnlineFieldStudyContactHours?.ToString(decimalFormat);
							this.ddlPartiallyReadyCoveredOnlineOtherContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.PartiallyReadyCoveredOnlineOtherContactHours?.ToString(decimalFormat);
							this.ddlPartiallyReadyCanNotCoveredOnlineTheoreticalContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.PartiallyReadyCanNotCoveredOnlineTheoreticalContactHours?.ToString(decimalFormat);
							this.ddlPartiallyReadyCanNotCoveredOnlineLabContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.PartiallyReadyCanNotCoveredOnlineLabContactHours?.ToString(decimalFormat);
							this.ddlPartiallyReadyCanNotCoveredOnlineFieldStudyContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.PartiallyReadyCanNotCoveredOnlineFieldStudyContactHours?.ToString(decimalFormat);
							this.ddlPartiallyReadyCanNotCoveredOnlineOtherContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.PartiallyReadyCanNotCoveredOnlineOtherContactHours?.ToString(decimalFormat);
							this.tbPartiallyReadyReasons.Text = offeredCourse.OnlineTeachingReadiness.PartiallyReadyReasons;
							this.rblPartiallyReadyRemainingContentsCoveredInSemesterID.SelectedValue = offeredCourse.OnlineTeachingReadiness.PartiallyReadyRemainingContentsCoveredInSemesterID.ToString();
							var questions = offeredCourse.OnlineTeachingReadiness.PartiallyReadyQuestionsList;
							if (questions?.Any() != true)
								questions = OnlineTeachingReadiness.Question.PartiallyReadyQuestions;
							this.repeaterPartiallyReadyQuestions.DataBind(questions);

							this.ddlPartiallyReadyAlreadyDeliveredFaceToFaceContactHours.Enabled = this.EditingAllowed;
							this.ddlPartiallyReadyToBeDeliveredOnlineContactHours.Enabled = this.EditingAllowed;
							this.ddlPartiallyReadyCoveredOnlineTheoreticalContactHours.Enabled = this.EditingAllowed;
							this.tbPartiallyReadyTotalContactHours.ReadOnly = !this.EditingAllowed;
							this.ddlPartiallyReadyCoveredOnlineLabContactHours.Enabled = this.EditingAllowed;
							this.ddlPartiallyReadyCoveredOnlineFieldStudyContactHours.Enabled = this.EditingAllowed;
							this.ddlPartiallyReadyCoveredOnlineOtherContactHours.Enabled = this.EditingAllowed;
							this.ddlPartiallyReadyCanNotCoveredOnlineTheoreticalContactHours.Enabled = this.EditingAllowed;
							this.ddlPartiallyReadyCanNotCoveredOnlineLabContactHours.Enabled = this.EditingAllowed;
							this.ddlPartiallyReadyCanNotCoveredOnlineFieldStudyContactHours.Enabled = this.EditingAllowed;
							this.ddlPartiallyReadyCanNotCoveredOnlineOtherContactHours.Enabled = this.EditingAllowed;
							this.tbPartiallyReadyReasons.ReadOnly = !this.EditingAllowed;
							this.rblPartiallyReadyRemainingContentsCoveredInSemesterID.Enabled = this.EditingAllowed;
							break;
						case OnlineTeachingReadiness.ReadinessTypes.FullyReady:
							this.ddlFullyReadyAlreadyDeliveredFaceToFaceContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.FullyReadyAlreadyDeliveredFaceToFaceContactHours?.ToString(decimalFormat);
							this.ddlFullyReadyToBeDeliveredOnlineContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.FullyReadyToBeDeliveredOnlineContactHours?.ToString(decimalFormat);
							this.ddlFullyReadyTheoreticalContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.FullyReadyTheoreticalContactHours?.ToString(decimalFormat);
							this.tbFullyReadyTotalContactHours.Text = offeredCourse.OnlineTeachingReadiness.FullyReadyTotalContactHours?.ToString(decimalFormat);
							this.ddlFullyReadyLabContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.FullyReadyLabContactHours?.ToString(decimalFormat);
							this.ddlFullyReadyFieldStudyContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.FullyReadyFieldStudyContactHours?.ToString(decimalFormat);
							this.ddlFullyReadyOtherContactHours.SelectedValue = offeredCourse.OnlineTeachingReadiness.FullyReadyOtherContactHours?.ToString(decimalFormat);
							questions = offeredCourse.OnlineTeachingReadiness.FullyReadyQuestionsList;
							if (questions?.Any() != true)
								questions = OnlineTeachingReadiness.Question.FullyReadyQuestions;
							this.repeaterFullyReadyQuestions.DataBind(questions);

							this.ddlFullyReadyAlreadyDeliveredFaceToFaceContactHours.Enabled = this.EditingAllowed;
							this.ddlFullyReadyToBeDeliveredOnlineContactHours.Enabled = this.EditingAllowed;
							this.ddlFullyReadyTheoreticalContactHours.Enabled = this.EditingAllowed;
							this.tbFullyReadyTotalContactHours.ReadOnly = !this.EditingAllowed;
							this.ddlFullyReadyLabContactHours.Enabled = this.EditingAllowed;
							this.ddlFullyReadyFieldStudyContactHours.Enabled = this.EditingAllowed;
							this.ddlFullyReadyOtherContactHours.Enabled = this.EditingAllowed;
							break;
						default:
							throw new NotImplementedEnumException(offeredCourse.OnlineTeachingReadiness.ReadinessTypeEnum);
					}

					if (offeredCourse.OnlineTeachingReadiness.SubmittedByClusterHeadDate != null && offeredCourse.SubmittedByUsers != null
						&& offeredCourse.SubmittedByUsers.TryGetValue(offeredCourse.OnlineTeachingReadiness.SubmittedByClusterHeadUserLoginHistoryID.Value, out var name))
					{
						this.lblSubmitAsClusterHead.Text = name;
						this.lblSubmitAsClusterHeadDate.Text = offeredCourse.OnlineTeachingReadiness.SubmittedByClusterHeadDate.Value.ToString("D");
						this.lblSubmitAsClusterHead.Visible = true;
						this.lblSubmitAsClusterHeadDate.Visible = true;
						//	this.btnSubmitAsClusterHead.Visible = false;
					}
					else
					{
						this.lblSubmitAsClusterHead.Visible = false;
						this.lblSubmitAsClusterHeadDate.Visible = false;
						//	this.btnSubmitAsClusterHead.Visible = false;
					}
					if (offeredCourse.OnlineTeachingReadiness.SubmittedByHODDate != null && offeredCourse.SubmittedByUsers != null
						&& offeredCourse.SubmittedByUsers.TryGetValue(offeredCourse.OnlineTeachingReadiness.SubmittedByHODUserLoginHistoryID.Value, out name))
					{
						this.lblSubmitAsHOD.Text = name;
						this.lblSubmitAsHODDate.Text = offeredCourse.OnlineTeachingReadiness.SubmittedByHODDate.Value.ToString("D");
						this.lblSubmitAsHOD.Visible = true;
						this.lblSubmitAsHODDate.Visible = true;
						//	this.btnSubmitAsHOD.Visible = false;
					}
					else
					{
						this.lblSubmitAsHOD.Visible = false;
						this.lblSubmitAsHODDate.Visible = false;
						//		this.btnSubmitAsHOD.Visible = true;
					}
					if (offeredCourse.OnlineTeachingReadiness.SubmittedByDirectorDate != null && offeredCourse.SubmittedByUsers != null
						&& offeredCourse.SubmittedByUsers.TryGetValue(offeredCourse.OnlineTeachingReadiness.SubmittedByDirectorUserLoginHistoryID.Value, out name))
					{
						this.lblSubmitAsDirector.Text = name;
						this.lblSubmitAsDirectorDate.Text = offeredCourse.OnlineTeachingReadiness.SubmittedByDirectorDate.Value.ToString("D");
						this.lblSubmitAsDirector.Visible = true;
						this.lblSubmitAsDirectorDate.Visible = true;
						//		this.btnSubmitAsDirector.Visible = false;
					}
					else
					{
						this.lblSubmitAsDirector.Visible = false;
						this.lblSubmitAsDirectorDate.Visible = false;
						//		this.btnSubmitAsDirector.Visible = true;
					}
					if (offeredCourse.OnlineTeachingReadiness.SubmittedByDGDate != null && offeredCourse.SubmittedByUsers != null
						&& offeredCourse.SubmittedByUsers.TryGetValue(offeredCourse.OnlineTeachingReadiness.SubmittedByDGUserLoginHistoryID.Value, out name))
					{
						this.lblSubmitAsDG.Text = name;
						this.lblSubmitAsDGDate.Text = offeredCourse.OnlineTeachingReadiness.SubmittedByDGDate.Value.ToString("D");
						this.lblSubmitAsDG.Visible = true;
						this.lblSubmitAsDGDate.Visible = true;
						//		this.btnSubmitAsDG.Visible = false;
					}
					else
					{
						this.lblSubmitAsDG.Visible = false;
						this.lblSubmitAsDGDate.Visible = false;
						//	this.btnSubmitAsDG.Visible = true;
					}

					this.btnNotReady.Enabled = this.EditingAllowed;
					this.btnPartiallyReady.Enabled = this.EditingAllowed;
					this.btnFullyReady.Enabled = this.EditingAllowed;
					this.btnNotReady.Enabled = this.EditingAllowed;
					//this.btnSave.Visible = this.EditingAllowed && offeredCourse.OnlineTeachingReadiness.SubmittedByDirectorDate == null;
					//	this.btnSubmitAsClusterHead.Enabled = this.EditingAllowed && offeredCourse.OnlineTeachingReadiness.SubmittedByClusterHeadDate == null;
					//	this.btnSubmitAsHOD.Enabled = this.EditingAllowed && offeredCourse.OnlineTeachingReadiness.SubmittedByClusterHeadDate != null && offeredCourse.OnlineTeachingReadiness.SubmittedByHODDate == null;
					//	this.btnSubmitAsDirector.Enabled = this.EditingAllowed && offeredCourse.OnlineTeachingReadiness.SubmittedByClusterHeadDate != null && offeredCourse.OnlineTeachingReadiness.SubmittedByHODDate != null && offeredCourse.OnlineTeachingReadiness.SubmittedByDirectorDate == null;
					//	this.btnSubmitAsDG.Enabled = this.EditingAllowed && offeredCourse.OnlineTeachingReadiness.SubmittedByClusterHeadDate != null && offeredCourse.OnlineTeachingReadiness.SubmittedByHODDate != null && offeredCourse.OnlineTeachingReadiness.SubmittedByDirectorDate != null && offeredCourse.OnlineTeachingReadiness.SubmittedByDGDate == null;
				}
				else
				{
					this.AddErrorAlert("Online Teaching Readiness form is not filled yet.");
					Redirect<OnlineTeachingReadinessOfferedCourses>();
				}
			}
		}

		protected void SetControlProperties()
		{
			var creditHoursDropDownLists = new[] {
				this.ddlNotReadyAlreadyDeliveredFaceToFaceContactHours,
				this.ddlNotReadyFieldStudyContactHours,
				this.ddlNotReadyLabContactHours,
				this.ddlNotReadyOtherContactHours,
				this.ddlNotReadyRemainingContactHours,
				this.ddlNotReadyTheoreticalContactHours,
				this.ddlPartiallyReadyAlreadyDeliveredFaceToFaceContactHours,
				this.ddlPartiallyReadyCanNotCoveredOnlineFieldStudyContactHours,
				this.ddlPartiallyReadyCanNotCoveredOnlineLabContactHours,
				this.ddlPartiallyReadyCanNotCoveredOnlineOtherContactHours,
				this.ddlPartiallyReadyCanNotCoveredOnlineTheoreticalContactHours,
				this.ddlPartiallyReadyCoveredOnlineFieldStudyContactHours,
				this.ddlPartiallyReadyCoveredOnlineLabContactHours,
				this.ddlPartiallyReadyCoveredOnlineOtherContactHours,
				this.ddlPartiallyReadyCoveredOnlineTheoreticalContactHours,
				this.ddlPartiallyReadyToBeDeliveredOnlineContactHours,
				this.ddlFullyReadyAlreadyDeliveredFaceToFaceContactHours,
				this.ddlFullyReadyFieldStudyContactHours,
				this.ddlFullyReadyLabContactHours,
				this.ddlFullyReadyOtherContactHours,
				this.ddlFullyReadyTheoreticalContactHours,
				this.ddlFullyReadyToBeDeliveredOnlineContactHours,
			};
			var items = Enumerable.Range(0, 73).Where(i => i <= 72).Select(i => i.ToString()).ToList();
			creditHoursDropDownLists.ForEach(ddl => ddl.DataBind(items));
		}

		private OnlineTeachingReadiness.ReadinessTypes ReadinessTypeEnum
		{
			get
			{
				if (this.btnNotReady.ButtonType == AspireButton.ButtonTypes.Primary)
					return OnlineTeachingReadiness.ReadinessTypes.NotReady;
				if (this.btnPartiallyReady.ButtonType == AspireButton.ButtonTypes.Primary)
					return OnlineTeachingReadiness.ReadinessTypes.PartiallyReady;
				if (this.btnFullyReady.ButtonType == AspireButton.ButtonTypes.Primary)
					return OnlineTeachingReadiness.ReadinessTypes.FullyReady;
				else
					throw new InvalidOperationException();
			}
			set
			{
				this.btnNotReady.ButtonType = value == OnlineTeachingReadiness.ReadinessTypes.NotReady ? AspireButton.ButtonTypes.Primary : AspireButton.ButtonTypes.Default;
				this.btnPartiallyReady.ButtonType = value == OnlineTeachingReadiness.ReadinessTypes.PartiallyReady ? AspireButton.ButtonTypes.Primary : AspireButton.ButtonTypes.Default;
				this.btnFullyReady.ButtonType = value == OnlineTeachingReadiness.ReadinessTypes.FullyReady ? AspireButton.ButtonTypes.Primary : AspireButton.ButtonTypes.Default;
				this.btnNotReady.Enabled = value != OnlineTeachingReadiness.ReadinessTypes.NotReady;
				this.btnPartiallyReady.Enabled = value != OnlineTeachingReadiness.ReadinessTypes.PartiallyReady;
				this.btnFullyReady.Enabled = value != OnlineTeachingReadiness.ReadinessTypes.FullyReady;
				this.OnReadinessTypeChanged();
			}
		}

		protected bool EditingAllowed
		{
			get => (bool?)this.ViewState[nameof(this.EditingAllowed)] == true;
			private set => this.ViewState[nameof(this.EditingAllowed)] = value;
		}

		protected void btnReadiness_Click(object sender, EventArgs e)
		{
			if (sender == this.btnNotReady)
				this.ReadinessTypeEnum = OnlineTeachingReadiness.ReadinessTypes.NotReady;
			else if (sender == this.btnPartiallyReady)
				this.ReadinessTypeEnum = OnlineTeachingReadiness.ReadinessTypes.PartiallyReady;
			else if (sender == this.btnFullyReady)
				this.ReadinessTypeEnum = OnlineTeachingReadiness.ReadinessTypes.FullyReady;
			else
				throw new InvalidOperationException();
		}

		private void OnReadinessTypeChanged()
		{
			var readinessTypeEnum = this.ReadinessTypeEnum;
			switch (readinessTypeEnum)
			{
				case OnlineTeachingReadiness.ReadinessTypes.NotReady:
					this.multiView.SetActiveView(this.viewNotReady);
					break;
				case OnlineTeachingReadiness.ReadinessTypes.PartiallyReady:
					this.multiView.SetActiveView(this.viewPartiallyReady);
					if (this.repeaterPartiallyReadyQuestions.Items.Count == 0)
						this.repeaterPartiallyReadyQuestions.DataBind(OnlineTeachingReadiness.Question.PartiallyReadyQuestions);
					break;
				case OnlineTeachingReadiness.ReadinessTypes.FullyReady:
					this.multiView.SetActiveView(this.viewFullyReady);
					if (this.repeaterFullyReadyQuestions.Items.Count == 0)
						this.repeaterFullyReadyQuestions.DataBind(OnlineTeachingReadiness.Question.FullyReadyQuestions);
					break;
				default:
					throw new NotImplementedEnumException(readinessTypeEnum);
			}
		}
	}
}