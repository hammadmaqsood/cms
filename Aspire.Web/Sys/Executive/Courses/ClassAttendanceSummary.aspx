﻿<%@ Page MasterPageFile="~/Base.Master" Language="C#" AutoEventWireup="true" CodeBehind="ClassAttendanceSummary.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Courses.ClassAttendanceSummary" %>

<asp:Content runat="server" ContentPlaceHolderID="BodyPH">
	<style scoped="scoped">
		table#courseInfo th {
			white-space: nowrap;
		}

		table#courseInfo td {
			width: 50%;
		}
	</style>
	<table class="table table-bordered table-condensed" id="courseInfo">
		<caption>Course Information</caption>
		<tbody>
			<tr>
				<th>Title</th>
				<td>
					<aspire:Label runat="server" ID="lblCourseTitle" />
				</td>
				<th>Teacher Name</th>
				<td>
					<aspire:Label runat="server" ID="lblTeacherName" />
				</td>
			</tr>
			<tr>
				<th>Credit Hours</th>
				<td>
					<aspire:Label runat="server" ID="lblCreditHrs" />
				</td>
				<th>Contact Hours</th>
				<td>
					<aspire:Label runat="server" ID="lblContactHrs" />
				</td>
			</tr>
			<tr>
				<th>Class</th>
				<td>
					<aspire:Label runat="server" ID="lblClass" />
				</td>
				<th>Semester</th>
				<td>
					<aspire:Label runat="server" ID="lblSemesterID" />
				</td>
			</tr>
		</tbody>
	</table>
	<div>
		<aspire:AspireHyperLinkButton runat="server" ID="hlViewAttendance" Text="View Attendance" />
	</div>

	<style scoped="scoped">
		table[id$=gvAttendanceRecord] td:first-child,
		table[id$=gvAttendanceRecord] th:first-child,
		table[id$=gvAttendanceRecord] th:nth-child(5),
		table[id$=gvAttendanceRecord] td:nth-child(5),
		table[id$=gvAttendanceRecord] th:nth-child(6),
		table[id$=gvAttendanceRecord] td:nth-child(6),
		table[id$=gvAttendanceRecord] th:nth-child(7),
		table[id$=gvAttendanceRecord] td:nth-child(7),
		table[id$=gvAttendanceRecord] th:nth-child(8),
		table[id$=gvAttendanceRecord] td:nth-child(8) {
			text-align: center;
		}
	</style>
	<aspire:AspireGridView Caption="Attendance Summary" runat="server" ID="gvAttendanceRecord" AutoGenerateColumns="False" OnRowDataBound="gvAttendanceRecord_OnRowDataBound">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%#: Container.DataItemIndex + 1 %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Enrollment" DataField="Enrollment" />
			<asp:BoundField HeaderText="Name" DataField="Name" />
			<asp:BoundField HeaderText="Program" DataField="Program" />
			<asp:TemplateField HeaderText="Total Hours">
				<ItemTemplate><%#: Eval("TotalHours","{0:###.##}") %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Present Hours">
				<ItemTemplate><%#: Eval("PresentHours","{0:###.##}") %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Absent Hours">
				<ItemTemplate><%#: Eval("AbsentHours","{0:###.##}") %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Percentage" DataField="Percentage" DataFormatString="{0:###.##}%" />
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
