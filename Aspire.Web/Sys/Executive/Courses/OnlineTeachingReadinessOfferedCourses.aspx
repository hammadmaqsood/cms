﻿<%@ Page Language="C#" MasterPageFile="~/Sys/Executive/ExecutiveMaster.Master" AutoEventWireup="true" CodeBehind="OnlineTeachingReadinessOfferedCourses.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Courses.OnlineTeachingReadinessOfferedCourses" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div id="container"></div>
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlProgramIDFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="ddlSemesterNoFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterNoFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterNoFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-1">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSectionFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSectionFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlSectionFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-1">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShiftFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlShiftFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlShiftFilter_OnSelectedIndexChanged" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<aspire:AspireLabel runat="server" Text="Show Columns:" AssociatedControlID="cblGroupBy" />
				<aspire:AspireCheckBoxList runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" ID="cblGroupBy" AutoPostBack="True" OnSelectedIndexChanged="cblGroupBy_OnSelectedIndexChanged" ValidationGroup="Grouping" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="cblGroupBy" ErrorMessage="Select atleast one 'Group By' clause." ValidationGroup="Grouping" />
			</div>
		</div>
	</div>
	<p></p>
	<div class="row">
		<div runat="server" id="dvGrid" class="col-md-12">
		</div>
	</div>
	<%--<aspire:AspireGridView runat="server" AutoGenerateColumns="True" ID="gvOnlineTeachingReadinessOfferedCourses" AllowSorting="False" AllowPaging="False" ShowFooter="True" OnRowDataBound="gvOnlineTeachingReadinessOfferedCourses_OnRowDataBound" OnRowCommand="gvOnlineTeachingReadinessOfferedCourses_OnRowCommand" />--%>
	<%--<aspire:AspireModal runat="server" ID="modalOnlineTeachingReadinessOfferedCoursesList" ModalSize="Large" >
		<BodyTemplate>
			<asp:Repeater runat="server" ID="rptOnlineTeachingReadinessOfferedCoursesList" ItemType="Aspire.BL.Core.CourseOffering.Executive.OnlineTeachingReadinessOfferedCourses.OfferedCourse">
				<HeaderTemplate>
					<table class="table table-bordered table-condensed table-striped table-hover">
						<caption>Offered Courses</caption>
						<thead>
							<tr>
								<th>#</th>
								<th>Semester</th>
								<th>Department</th>
								<th>Class</th>
								<th>Faculty Member</th>
								<th>Readiness Type</th>
							</tr>
						</thead>
						<tbody>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><%#: Container.ItemIndex + 1 %></td>
						<td><%#: Item.SemesterID.ToSemesterString() %></td>
						<td><%#: Item.DepartmentName %></td>
						<td><%#: Item.ClassName %></td>
						<td><%#: Item.FacultyMemberName.ToNAIfNullOrEmpty() %></td>
						<td><%#: Item.ReadinessTypeFullName %></td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
					<tr runat="server" visible="<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>">
						<td colspan="10">No record found.</td>
					</tr>
					</tbody>
		</table>
				</FooterTemplate>
			</asp:Repeater>
		</BodyTemplate>
		<FooterTemplate>
			<aspire:AspireModalCloseButton runat="server">Close</aspire:AspireModalCloseButton>
		</FooterTemplate>
	</aspire:AspireModal>--%>
	<%--<script>
		init($("#<%=this.gvOnlineTeachingReadinessOfferedCourses.ClientID%>"));
	</script>--%>
</asp:Content>
