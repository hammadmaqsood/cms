﻿using Aspire.BL.Entities.ClassAttendance;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Executive.Courses
{
	[AspirePage("CCB60357-94B9-4679-80FE-E22675747252", UserTypes.Executive, "~/Sys/Executive/Courses/ClassAttendanceSummary", "Attendance Summary", true, AspireModules.None)]
	public partial class ClassAttendanceSummary : ExecutivePage
	{
		public override PageIcon PageIcon => AspireGlyphicons.list_alt.GetIcon();
		public override string PageTitle => "Class Attendance Summary";

		private int? OfferedCourseID => this.Request.GetParameterValue<int>("OfferedCourseID");

		public static string GetPageUrl(int offeredCourseID)
		{
			return GetPageUrl<ClassAttendanceSummary>().AttachQueryParam("OfferedCourseID", offeredCourseID);
		}
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.OfferedCourseID == null)
				{
					Redirect<Courses>();
					return;
				}

				var offeredCourse = BL.ClassAttendance.Faculty.GetAttendanceSummary(this.OfferedCourseID.Value);
				if (offeredCourse == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Courses>();
					return;
				}
				this.hlViewAttendance.NavigateUrl = ClassAttendance.GetPageUrl(offeredCourse.OfferedCourseID);
				this.lblClass.Text = offeredCourse.ClassName;
				this.lblContactHrs.Text = offeredCourse.ContactHours.ToCreditHoursFullName();
				this.lblCourseTitle.Text = offeredCourse.Title;
				this.lblCreditHrs.Text = offeredCourse.CreditHours.ToCreditHoursFullName();
				this.lblTeacherName.Text = offeredCourse.TeacherName;
				this.lblSemesterID.Text = offeredCourse.SemesterID.ToSemesterString();
				this.gvAttendanceRecord.DataBind(offeredCourse.Summary);
			}
		}
		protected void gvAttendanceRecord_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow)
				return;
			var detail = (CustomAttendanceSummary.Detail)e.Row.DataItem;
			if (detail.Danger)
				e.Row.CssClass = "danger";
		}
	}
}