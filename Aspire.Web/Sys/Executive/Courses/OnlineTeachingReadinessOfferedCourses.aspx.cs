﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using ListItem = System.Web.UI.WebControls.ListItem;

namespace Aspire.Web.Sys.Executive.Courses
{
	[AspirePage("C7CFA369-C7A5-41EA-9601-F997868A14A7", UserTypes.Executive, "~/Sys/Executive/Courses/OnlineTeachingReadinessOfferedCourses.aspx", "Online Teaching Readiness", true, AspireModules.CourseOffering)]
	public partial class OnlineTeachingReadinessOfferedCourses : ExecutivePage
	{
		public override PageIcon PageIcon => AspireGlyphicons.book.GetIcon();
		public override string PageTitle => "Online Teaching Readiness";
		public System.Web.UI.WebControls.GridView GvOnlineTeachingReadinessOfferedCourses = new System.Web.UI.WebControls.GridView();
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters();
				this.ddlDepartmentIDFilter.FillDepartments(this.ExecutiveIdentity.InstituteID, CommonListItems.All);
				this.ddlSemesterNoFilter.FillSemesterNos(CommonListItems.All);
				this.ddlSectionFilter.FillSections(CommonListItems.All);
				this.ddlShiftFilter.FillShifts(CommonListItems.All);
				this.cblGroupBy.FillEnums<BL.Core.CourseOffering.Executive.OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields>().SetEnumValue(BL.Core.CourseOffering.Executive.OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields.Department);
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
			this.SetMaximumScriptTimeout();
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			if (departmentID == null)
				this.ddlProgramIDFilter.DataBind(CommonListItems.All);
			else
				this.ddlProgramIDFilter.FillPrograms(this.ExecutiveIdentity.InstituteID, departmentID, true, CommonListItems.All);
			this.ddlProgramIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var value = ((int)(BL.Core.CourseOffering.Executive.OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields.Program)).ToString();
			if (programID != null)
				this.cblGroupBy.Items.Cast<ListItem>().Single(cbl => cbl.Value == value).Selected = true;
			this.ddlSemesterNoFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterNoFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterNo = this.ddlSemesterNoFilter.SelectedValue.ToNullableShort();
			var value = ((int)(BL.Core.CourseOffering.Executive.OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields.SemesterNo)).ToString();
			if (semesterNo != null)
				this.cblGroupBy.Items.Cast<ListItem>().Single(cbl => cbl.Value == value).Selected = true;
			this.ddlSectionFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSectionFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var section = this.ddlSectionFilter.SelectedValue.ToNullableInt();
			var value = ((int)(BL.Core.CourseOffering.Executive.OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields.Section)).ToString();
			if (section != null)
				this.cblGroupBy.Items.Cast<ListItem>().Single(cbl => cbl.Value == value).Selected = true;
			this.ddlShiftFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlShiftFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var shift = this.ddlShiftFilter.SelectedValue.ToNullableByte();
			var value = ((int)(BL.Core.CourseOffering.Executive.OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields.Shift)).ToString();
			if (shift != null)
				this.cblGroupBy.Items.Cast<ListItem>().Single(cbl => cbl.Value == value).Selected = true;
			this.cblGroupBy_OnSelectedIndexChanged(null, null);
		}

		protected void cblGroupBy_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetOnlineTeachingReadinessOfferedCoursesStrength();
		}

		private void GetOnlineTeachingReadinessOfferedCoursesStrength()
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var semesterNo = this.ddlSemesterNoFilter.GetSelectedEnumValue<SemesterNos>(null);
			var section = this.ddlSectionFilter.GetSelectedEnumValue<Sections>(null);
			var shift = this.ddlShiftFilter.GetSelectedEnumValue<Shifts>(null);

			BL.Core.CourseOffering.Executive.OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields? groupByFields = null;
			foreach (var field in this.cblGroupBy.GetSelectedEnumValues<BL.Core.CourseOffering.Executive.OnlineTeachingReadinessOfferedCourses.OnlineTeachingReadinessOfferedCoursesFields>())
			{
				if (groupByFields == null)
					groupByFields = field;
				else
					groupByFields = groupByFields.Value | field;
			}

			if (groupByFields == null)
			{
				this.AddErrorAlert("Select at-least one Column.");
				this.GvOnlineTeachingReadinessOfferedCourses.DataBindNull();
				return;
			}

			//var (dataTable, offeredCourses) = BL.Core.CourseOffering.Executive.OnlineTeachingReadinessOfferedCourses.GetOnlineTeachingReadinessCourses(semesterID, departmentID, programID, semesterNo, section, shift, groupByFields.Value, this.ExecutiveIdentity.LoginSessionGuid);
			this.GvOnlineTeachingReadinessOfferedCourses = BL.Core.CourseOffering.Executive.OnlineTeachingReadinessOfferedCourses.GetOnlineTeachingReadinessOfferedCoursesGrid(semesterID, departmentID, programID, semesterNo, section, shift, groupByFields.Value, this.ExecutiveIdentity.LoginSessionGuid);
			this.GvOnlineTeachingReadinessOfferedCourses.RowDataBound += new GridViewRowEventHandler(GvOnlineTeachingReadinessOfferedCourses_OnRowDataBound);
			this.GvOnlineTeachingReadinessOfferedCourses.DataBind();

			if (this.GvOnlineTeachingReadinessOfferedCourses.Rows.Count > 0)
			{
				DataTable gvTable = this.GvOnlineTeachingReadinessOfferedCourses.DataSource as DataTable;
				this.GvOnlineTeachingReadinessOfferedCourses.FooterRow.Cells[0].Text = "Total";
				this.GvOnlineTeachingReadinessOfferedCourses.FooterRow.Cells[this.GvOnlineTeachingReadinessOfferedCourses.Columns.Count - 4].Text = (gvTable.Compute("Sum(OfferedCourses)", null) ?? 0).ToString();
				this.GvOnlineTeachingReadinessOfferedCourses.FooterRow.Cells[this.GvOnlineTeachingReadinessOfferedCourses.Columns.Count - 3].Text = (gvTable.Compute("Sum(FullyReady)", null) ?? 0).ToString();
				this.GvOnlineTeachingReadinessOfferedCourses.FooterRow.Cells[this.GvOnlineTeachingReadinessOfferedCourses.Columns.Count - 2].Text = (gvTable.Compute("Sum(PartiallyReady)", null) ?? 0).ToString();
				this.GvOnlineTeachingReadinessOfferedCourses.FooterRow.Cells[this.GvOnlineTeachingReadinessOfferedCourses.Columns.Count - 1].Text = (gvTable.Compute("Sum(NotReady)", null) ?? 0).ToString();
			}
			else
			{
				this.GvOnlineTeachingReadinessOfferedCourses.DataBindNull();
			}
			this.dvGrid.Controls.Add(this.GvOnlineTeachingReadinessOfferedCourses);
		}

		protected void GvOnlineTeachingReadinessOfferedCourses_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				var semesterID = (e.Row.DataItem as DataRowView).Row["SemesterID"].ToString().ToShort();
				var departmentID = (e.Row.DataItem as DataRowView).Row["DepartmentID"].ToString().ToNullableInt();
				var programID = (e.Row.DataItem as DataRowView).Row["ProgramID"].ToString().ToNullableInt();
				var semesterNo = (e.Row.DataItem as DataRowView).Row["SemesterNoValue"].ToString().ToNullableShort();
				var section = (e.Row.DataItem as DataRowView).Row["SectionValue"].ToString().ToNullableInt();
				var shift = (e.Row.DataItem as DataRowView).Row["ShiftValue"].ToString().ToNullableByte();
				//var semesterID=(e.Row.DataItem as DataRowView).Row["SemesterID"].ToString().ToShort();
				AspireHyperLink lnkOfferedCourses = new AspireHyperLink();

				lnkOfferedCourses.NavigateUrl = GetPageUrl<Aspire.Web.Sys.Executive.Courses.Courses>()
					.AttachQueryParams("SemesterID", semesterID, "DepartmentID", departmentID, "ProgramID", programID, "SemesterNo", semesterNo, "Section", section, "Shift", shift, "ReadinessType", null, "IsFromOnlineTeachingReadinessForm", true);
				lnkOfferedCourses.Target = "_blank";
				lnkOfferedCourses.Text = (e.Row.DataItem as DataRowView).Row["OfferedCourses"].ToString();
				e.Row.Cells[this.GvOnlineTeachingReadinessOfferedCourses.Columns.Count - 4].Controls.Add(lnkOfferedCourses);

				AspireHyperLink lnkFullyReady = new AspireHyperLink();
				//(e.Row.DataItem as DataRowView).Row["DepartmentID"].ToString();
				lnkFullyReady.NavigateUrl = GetPageUrl<Aspire.Web.Sys.Executive.Courses.Courses>()
					.AttachQueryParams("SemesterID", semesterID, "DepartmentID", departmentID, "ProgramID", programID, "SemesterNo", semesterNo, "Section", section, "Shift", shift, "ReadinessType", OnlineTeachingReadiness.ReadinessTypes.FullyReady.GetGuid(), "IsFromOnlineTeachingReadinessForm", true);
				lnkFullyReady.Target = "_blank";
				lnkFullyReady.Text = (e.Row.DataItem as DataRowView).Row["FullyReady"].ToString();
				e.Row.Cells[this.GvOnlineTeachingReadinessOfferedCourses.Columns.Count - 3].Controls.Add(lnkFullyReady);

				AspireHyperLink lnkPartiallyReady = new AspireHyperLink();
				//(e.Row.DataItem as DataRowView).Row["DepartmentID"].ToString();
				lnkPartiallyReady.NavigateUrl = GetPageUrl<Aspire.Web.Sys.Executive.Courses.Courses>()
					.AttachQueryParams("SemesterID", semesterID, "DepartmentID", departmentID, "ProgramID", programID, "SemesterNo", semesterNo, "Section", section, "Shift", shift, "ReadinessType", OnlineTeachingReadiness.ReadinessTypes.PartiallyReady.GetGuid(), "IsFromOnlineTeachingReadinessForm", true);
				lnkPartiallyReady.Target = "_blank";
				lnkPartiallyReady.Text = (e.Row.DataItem as DataRowView).Row["PartiallyReady"].ToString();
				e.Row.Cells[this.GvOnlineTeachingReadinessOfferedCourses.Columns.Count - 2].Controls.Add(lnkPartiallyReady);

				AspireHyperLink lnkNotReady = new AspireHyperLink();
				//(e.Row.DataItem as DataRowView).Row["DepartmentID"].ToString();
				lnkNotReady.NavigateUrl = GetPageUrl<Aspire.Web.Sys.Executive.Courses.Courses>()
					.AttachQueryParams("SemesterID", semesterID, "DepartmentID", departmentID, "ProgramID", programID, "SemesterNo", semesterNo, "Section", section, "Shift", shift, "ReadinessType", OnlineTeachingReadiness.ReadinessTypes.NotReady.GetGuid(), "IsFromOnlineTeachingReadinessForm", true);
				lnkNotReady.Target = "_blank";
				lnkNotReady.Text = (e.Row.DataItem as DataRowView).Row["NotReady"].ToString();
				e.Row.Cells[this.GvOnlineTeachingReadinessOfferedCourses.Columns.Count - 1].Controls.Add(lnkNotReady);
			}

			if (e.Row.RowType == DataControlRowType.Footer)
			{
				e.Row.Font.Bold = true;
			}
		}

		protected void gvOnlineTeachingReadinessOfferedCourses_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{

			//if (e.Row.RowType == DataControlRowType.DataRow)
			//{
			//	GridView gr = (GridView) sender; 

			//	int i = 1;
			//	i++;
			//	var dataTable = ((System.Data.DataView)this.gvOnlineTeachingReadinessOfferedCourses.DataSource).Table;

			//	//TemplateField tf = new TemplateField();

			//	AspireLinkButton lbOfferedCourses = new AspireLinkButton();
			//	//	var test = new Literal();
			//	//	string linktest = "~/www.google.com".ToAbsoluteUrl();
			//	//	test.Text = $@"<a href=""{linktest}"">Test Link</a>";

			//	var offeredCoursesCount = e.Row.Cells[dataTable.Columns.IndexOf("Offered Courses")].Text;
			//	lbOfferedCourses.Text = e.Row.Cells[dataTable.Columns.IndexOf("DepartmentID")].Text; 
			//	lbOfferedCourses.CausesValidation = false;
			//	lbOfferedCourses.Click += new EventHandler(testlb_OnClick);
			////	dataTable.Columns.Remove("DepartmentID");
			//	//				lbOfferedCourses.ButtonType = AspireButton.ButtonTypes.Success;
			//	//	lbOfferedCourses.OnClientClick += new EventHandler(btn_OnClick);
			//	//lbOfferedCourses.NavigateUrl = GetPageUrl<Aspire.Web.Sys.Executive.Courses.Courses>();
			//	lbOfferedCourses.ID = i.ToString();
			//	e.Row.Cells[dataTable.Columns.IndexOf("Offered Courses")].Controls.Add(lbOfferedCourses);

			//	Aspire.Lib.WebControls.LinkButton lbFullyReady = new AspireLinkButton();
			//	var fullyReadyCount = e.Row.Cells[dataTable.Columns.IndexOf("Fully Ready")].Text;
			//	lbFullyReady.Text = fullyReadyCount;
			//	lbFullyReady.CommandName = "FullyReady";
			//	e.Row.Cells[dataTable.Columns.IndexOf("Fully Ready")].Controls.Add(lbFullyReady);

			//	Aspire.Lib.WebControls.LinkButton lbPartiallyReady = new AspireLinkButton();
			//	var partiallyReadyCount = e.Row.Cells[dataTable.Columns.IndexOf("Partially Ready")].Text;
			//	lbPartiallyReady.Text = partiallyReadyCount;
			//	lbPartiallyReady.CommandName = "PartiallyReady";
			//	e.Row.Cells[dataTable.Columns.IndexOf("Partially Ready")].Controls.Add(lbPartiallyReady);

			//	Aspire.Lib.WebControls.LinkButton lbNotReady = new AspireLinkButton();
			//	var notReadyCount = e.Row.Cells[dataTable.Columns.IndexOf("Not Ready")].Text;
			//	lbNotReady.Text = notReadyCount;
			//	lbNotReady.CommandName = "NotReady";
			//	e.Row.Cells[dataTable.Columns.IndexOf("Not Ready")].Controls.Add(lbNotReady);
			//	}

			//if (e.Row.RowType == DataControlRowType.Footer)
			//{
			//	var dataTableF = ((System.Data.DataView)this.gvOnlineTeachingReadinessOfferedCourses.DataSource).Table;
			//	var count = dataTableF.Columns.Count;
			//	e.Row.Font.Bold = true;
			//	e.Row.Cells[0].Text = @"Total";
			//	e.Row.Cells[count - 4].Text = (dataTableF.Compute("Sum([Offered Courses])", null) ?? 0).ToString();
			//	e.Row.Cells[count - 3].Text = (dataTableF.Compute("Sum([Fully Ready])", null) ?? 0).ToString();
			//	e.Row.Cells[count - 2].Text = (dataTableF.Compute("Sum([Partially Ready])", null) ?? 0).ToString();
			//	e.Row.Cells[count - 1].Text = (dataTableF.Compute("Sum([Not Ready])", null) ?? 0).ToString();
			//}
		}
	}
}