﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Courses.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Courses.Courses" %>

<%@ Import Namespace="Aspire.Web.Sys.Executive.Courses" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlProgramIDFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="ddlSemesterNoFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterNoFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterNoFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSectionFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSectionFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlSectionFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShiftFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlShiftFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlShiftFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Teacher:" AssociatedControlID="ddlFacultyMemberID" />
				<aspire:AspireDropDownList runat="server" ID="ddlFacultyMemberID" OnSelectedIndexChanged="ddlFacultyMemberID_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Readiness Type:" AssociatedControlID="ddlReadinessType" />
				<aspire:AspireDropDownList runat="server" ID="ddlReadinessType" OnSelectedIndexChanged="ddlReadinessType_OnSelectedIndexChanged" CausesValidation="false" AutoPostBack="True" />
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvCourses" ItemType="Aspire.BL.Core.CourseOffering.Executive.OfferedCourses.OfferedCourse" AutoGenerateColumns="False" AllowSorting="True" OnSorting="gvCourses_OnSorting" OnPageSizeChanging="gvCourses_OnPageSizeChanging" OnPageIndexChanging="gvCourses_OnPageIndexChanging">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%#: Container.DataItemIndex + 1 %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Semester" DataField="Semester" SortExpression="Semester" />
			<asp:BoundField HeaderText="Title" DataField="Title" SortExpression="Title" />
			<asp:BoundField HeaderText="Class" DataField="Class" SortExpression="Class" />
			<asp:BoundField HeaderText="Faculty Member" DataField="FacultyMemberName" SortExpression="FacultyMemberName" />
			<asp:BoundField HeaderText="CNIC" DataField="CNICNA" SortExpression="CNIC" />
			<asp:TemplateField HeaderText="Cluster Head/Subject Expert">
				<ItemTemplate>
					<%# Item.SubmittedByClusterHead %>
					<p></p>
					<aspire:Label runat="server" Text="<%# Item.SubmittedByClusterHeadDate %>" CssClass="label label-primary" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Readiness Type" DataField="ReadinessTypeFullName" SortExpression="ReadinessType" />
			<asp:TemplateField HeaderText="Attendance">
				<ItemTemplate>
					<aspire:AspireHyperLink Text="View Attendance" runat="server" NavigateUrl='<%# ClassAttendance.GetPageUrl((int) Eval("OfferedCourseID")) %>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Readiness Form">
				<ItemTemplate>
					<aspire:AspireHyperLink Text="View Form" runat="server" Target="_blank" NavigateUrl='<%# Aspire.Web.Sys.Executive.Courses.OnlineTeachingReadinessForm.GetPageUrl(Item.OfferedCourseID) %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<script type="text/javascript">
		$(function () {
			$("#<%=this.ddlFacultyMemberID.ClientID%>").select2({ theme: "bootstrap" });
		});
	</script>
</asp:Content>
