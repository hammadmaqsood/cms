﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Executive.OfficialDocuments
{
	[AspirePage("7EAD4355-D0B1-4951-8D46-5567ADC40A1A", UserTypes.Executive, "~/Sys/Executive/OfficialDocuments/Documents.aspx", "Official Documents", true, AspireModules.OfficialDocuments)]
	public partial class Documents : ExecutivePage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.book);
		public override string PageTitle => "Official Documents";

		protected void Page_Load(object sender, EventArgs e)
		{

		}
	}
}