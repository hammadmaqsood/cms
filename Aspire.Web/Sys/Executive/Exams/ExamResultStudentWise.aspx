﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ExamResultStudentWise.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Exams.ExamResultStudentWise" %>
<%@ Register tagPrefix="uc" tagName="ExamResultStudentWise" src="~/Sys/Common/Exam/ExamResultStudentWise.ascx" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc:ExamResultStudentWise runat="server" ID="ucExamResultStudentWise"/>
</asp:Content>
