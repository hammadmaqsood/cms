﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Executive.Exams
{
	[AspirePage("C3B542BD-0E66-453A-AF08-C4AEF459FFCF", UserTypes.Executive, "~/Sys/Executive/Exams/GradeSummary", "Grade Summary", true, AspireModules.Exams)]
	public partial class GradeSummary : ExecutivePage
	{
		protected override IReadOnlyDictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ExecutivePermissions.Exams.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => AspireGlyphicons.list.GetIcon();
		public override string PageTitle => "Examinations";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Title", SortDirection.Ascending);
				this.ddlSemesterIDFilter.FillSemesters();
				this.ddlDepartmentIDFilter.FillDepartments(this.ExecutiveIdentity.InstituteID, CommonListItems.All);
				this.ddlSemesterNoFilter.FillSemesterNos(CommonListItems.All);
				this.ddlSectionFilter.FillSections(CommonListItems.All);
				this.ddlShiftFilter.FillShifts(CommonListItems.All);
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);

			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			//if (departmentID == null)
			//	this.ddlProgramIDFilter.DataBind(CommonListItems.All);
			//else
				this.ddlProgramIDFilter.FillPrograms(this.ExecutiveIdentity.InstituteID, departmentID, false, CommonListItems.All);
			this.ddlProgramIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterNoFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterNoFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSectionFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSectionFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlShiftFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlShiftFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetOfferedCourses(0);
		}

		protected void gvCourses_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetOfferedCourses(0);
		}


		protected void gvCourses_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvCourses.PageSize = e.NewPageSize;
			this.GetOfferedCourses(0);
		}

		protected void gvCourses_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetOfferedCourses(e.NewPageIndex);
		}

		private void GetOfferedCourses(int pageIndex)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			var semesterNo = this.ddlSemesterNoFilter.GetSelectedEnumValue<SemesterNos>(null);
			var section = this.ddlSectionFilter.GetSelectedEnumValue<Sections>(null);
			var shift = this.ddlShiftFilter.GetSelectedEnumValue<Shifts>(null);
			int virtualItemCount;
			var offeredCourseGrades = Aspire.BL.Core.ExecutiveInformation.Executive.Exams.GradesSummary.GetOfferedCoursesGradesSummary(semesterID, departmentID, programID, semesterNo, section, shift, pageIndex, this.gvCourses.PageSize, out virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.ExecutiveIdentity.LoginSessionGuid);
			this.gvCourses.DataBind(offeredCourseGrades, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

	}
}