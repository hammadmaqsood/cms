﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Executive.Exams
{
	[AspirePage("DD149641-7557-4FB8-9F21-F5D42A17A364", UserTypes.Executive, "~/Sys/Executive/Exams/ExamResultStudentWise.aspx", "Exam Result Student Wise", true, AspireModules.Exams)]
	public partial class ExamResultStudentWise : ExecutivePage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.education);
		public override string PageTitle => "Exam Result Student Wise";

		protected override IReadOnlyDictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ExecutivePermissions.Exams.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public static string GetPageUrl(int? studentID)
		{
			return GetAspirePageAttribute<ExamResultStudentWise>().PageUrl.AttachQueryParam("StudentID", studentID);
		}

		public static string GetPageUrl(string enrollment)
		{
			return GetAspirePageAttribute<ExamResultStudentWise>().PageUrl.AttachQueryParam("Enrollment", enrollment);
		}

		public static void Redirect(int? studentID)
		{
			Redirect<ExamResultStudentWise>("StudentID", studentID);
		}

		public static void Redirect(string enrollment)
		{
			Redirect<ExamResultStudentWise>("Enrollment", enrollment);
		}

		protected void Page_Load(object sender, EventArgs e)
		{

		}
	}
}