﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="GradeSummary.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Exams.GradeSummary" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Web.Sys.Executive.Exams" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlProgramIDFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester No.:" AssociatedControlID="ddlSemesterNoFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterNoFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterNoFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Section:" AssociatedControlID="ddlSectionFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlSectionFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlSectionFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Shift:" AssociatedControlID="ddlShiftFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlShiftFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlShiftFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvCourses" AutoGenerateColumns="False" AllowSorting="True" OnSorting="gvCourses_OnSorting" OnPageSizeChanging="gvCourses_OnPageSizeChanging" OnPageIndexChanging="gvCourses_OnPageIndexChanging">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%#: Container.DataItemIndex + 1 %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Semester" SortExpression="Semester">
				<ItemTemplate><%#  ((short)Eval("SemesterID")).ToSemesterString() %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Title" DataField="Title" SortExpression="Title" />
			<asp:BoundField HeaderText="Program" DataField="Program" SortExpression="ProgramAlias" />
			<asp:TemplateField HeaderText="Semester No." SortExpression="SemesterNo">
				<ItemTemplate><%#  ((SemesterNos)Eval("SemesterNo")).ToFullName() %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Section" SortExpression="Section">
				<ItemTemplate><%#  ((Sections)Eval("Section")).ToFullName() %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Shift" SortExpression="Shift">
				<ItemTemplate><%#  ((Shifts)Eval("Shift")).ToFullName() %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Teacher" DataField="Teacher" SortExpression="Teacher" />
			<asp:BoundField HeaderText="A" DataField="GradeACount" />
			<asp:BoundField HeaderText="A-" DataField="GradeAMinusCount" />
			<asp:BoundField HeaderText="B+" DataField="GradeBPlusCount" />
			<asp:BoundField HeaderText="B" DataField="GradeBCount" />
			<asp:BoundField HeaderText="B-" DataField="GradeBMinusCount" />
			<asp:BoundField HeaderText="C+" DataField="GradeCPlusCount" />
			<asp:BoundField HeaderText="C" DataField="GradeCCount" />
			<asp:BoundField HeaderText="C-" DataField="GradeCMinusCount" />
			<asp:BoundField HeaderText="D+" DataField="GradeDPlusCount" />
			<asp:BoundField HeaderText="D" DataField="GradeDCount" />
			<asp:BoundField HeaderText="F" DataField="GradeFCount" />
			<asp:TemplateField HeaderText="Details">
				<ItemTemplate>
					<aspire:HyperLink runat="server" Text="Details" NavigateUrl='<%# ExamDetails.GetPageUrl((int)Eval("OfferedCourseID")) %>'/>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
