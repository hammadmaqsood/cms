﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;
using System.Text;

namespace Aspire.Web.Sys.Executive.Exams
{
	[AspirePage("3C0CF333-770E-49BD-B96A-13FFDE14D726", UserTypes.Executive, "~/Sys/Executive/Exams/ExamDetails", "Exam Details", false, AspireModules.Exams)]
	public partial class ExamDetails : ExecutivePage
	{
		public override PageIcon PageIcon => AspireGlyphicons.list.GetIcon();
		public override string PageTitle => "Exam Details";
		public int? OfferedCourseID => this.Request.GetParameterValue<int>("OfferedCourseID");

		public static string GetPageUrl(int? offeredCourseID)
		{
			if (offeredCourseID != null)
				return GetPageUrl<ExamDetails>().AttachQueryParam("OfferedCourseID", offeredCourseID);
			return GetPageUrl<ExamDetails>();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (this.OfferedCourseID == null)
				Redirect<GradeSummary>();
			else
			{
				var examDetails = Aspire.BL.Core.ExecutiveInformation.Executive.Exams.GradesSummary.GetOfferedCourseExamDetails(this.OfferedCourseID.Value);
				this.lblClass.Text = examDetails.OcClass;
				this.lblContactHrs.Text = examDetails.ContactHours.ToString();
				this.lblCourseTitle.Text = examDetails.Title;
				this.lblCreditHrs.Text = examDetails.CreditHours.ToString();
				this.lblCourseCategory.Text = ((CourseCategories)examDetails.Category).ToString();
				this.lblCourseType.Text = ((CourseTypes)examDetails.CourseType).ToString();
				this.lblTeacherName.Text = examDetails.Teacher;
				this.lblSemesterID.Text = examDetails.SemesterID.ToSemesterString();
				this.gvExamDetails.DataBind(examDetails.Details);

				var grades = Enum.GetValues(typeof(ExamGrades)).Cast<ExamGrades>().OrderBy(i => i, ExamGradeComparer.New());
				var str = new StringBuilder(string.Empty);
				foreach (var grade in grades)
					str.Append($"{grade.ToFullName()}:<b>{examDetails.Details.Count(d => d.Grade == (byte)grade)}</b>, ");
				this.lblGradesSummary.Text = str.ToString().TrimEnd(',');
			}
		}
	}
}
