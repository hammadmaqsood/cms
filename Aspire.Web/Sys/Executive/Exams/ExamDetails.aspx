﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ExamDetails.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Exams.ExamDetails" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<table class="table table-bordered table-condensed tableCol4">
		<caption>Course Information</caption>
		<tbody>
			<tr>
				<th>Title</th>
				<td>
					<aspire:Label runat="server" ID="lblCourseTitle" />
				</td>
				<th>Teacher Name</th>
				<td>
					<aspire:Label runat="server" ID="lblTeacherName" />
				</td>
			</tr>
			<tr>
				<th>Credit Hours</th>
				<td>
					<aspire:Label runat="server" ID="lblCreditHrs" />
				</td>
				<th>Contact Hours</th>
				<td>
					<aspire:Label runat="server" ID="lblContactHrs" />
				</td>
			</tr>
			<tr>
				<th>Category</th>
				<td>
					<aspire:Label runat="server" ID="lblCourseCategory" />
				</td>
				<th>Type</th>
				<td>
					<aspire:Label runat="server" ID="lblCourseType" />
				</td>
			</tr>
			<tr>
				<th>Class</th>
				<td>
					<aspire:Label runat="server" ID="lblClass" />
				</td>
				<th>Semester</th>
				<td>
					<aspire:Label runat="server" ID="lblSemesterID" />
				</td>
			</tr>
		</tbody>
	</table>
	<div class="row text-center">
			<aspire:AspireLabel runat="server" ID="lblGradesSummary" />
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvExamDetails" AutoGenerateColumns="False">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%#: Container.DataItemIndex + 1 %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Enrollment" DataField="Enrollment" />
			<asp:BoundField HeaderText="Registration No." DataField="RegistrationNo" />
			<asp:BoundField HeaderText="Name" DataField="Name" />
			<asp:BoundField HeaderText="Class" DataField="Class" />
			<asp:BoundField HeaderText="Assignments" DataField="Assignments" />
			<asp:BoundField HeaderText="Quizzes" DataField="Quizzes" />
			<asp:BoundField HeaderText="Mid" DataField="Mid" />
			<asp:BoundField HeaderText="Final" DataField="Final" />
			<asp:BoundField HeaderText="Total" DataField="Total" />
			<asp:BoundField HeaderText="Grade" DataField="GradeFullName" />
		</Columns>
	</aspire:AspireGridView>
	<p></p>
</asp:Content>
