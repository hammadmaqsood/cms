﻿using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Executive
{
	[AspirePage("{2CFD497D-8690-4DC9-A899-216E96B4BE08}", UserTypes.Executive, "~/Sys/Executive/Logoff.aspx", null, false, AspireModules.None)]
	public partial class Logoff : LogoffBasePage
	{
		public static string GetPageUrl(UserLoginHistory.LogOffReasons logoffReason)
		{
			return GetPageUrl(GetPageUrl<Logoff>(), logoffReason);
		}

		public static void Redirect(UserLoginHistory.LogOffReasons logoffReason)
		{
			Redirect(GetPageUrl(logoffReason));
		}

		protected override string LoginPageUrl => GetPageUrl<Logins.Executive.Login>();
	}
}