﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="SummaryProgramWise.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Admissions.SummaryProgramWise" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" ValidationGroup="Filter" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlProgramIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlProgramIDFilter_SelectedIndexChanged" ValidationGroup="Filter" />
			</div>
		</div>
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Show/Hide Columns:" AssociatedControlID="cblColumns" />
		<div>
			<aspire:AspireCheckBoxList runat="server" ID="cblColumns" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="cblColumns_SelectedIndexChanged" />
		</div>
	</div>
	<aspire:AspireGridView runat="server" ID="gvAdmissionSummary" ShowFooter="True" ItemType="Aspire.BL.Core.ExecutiveInformation.Executive.Admissions.ProgramWiseSummary.AdmissionsSummary" AutoGenerateColumns="False">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%#: Container.DataItemIndex + 1 %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Department">
				<ItemTemplate><%#: Item.DepartmentName %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Program">
				<ItemTemplate><%#: Item.ProgramAlias %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Duration">
				<ItemTemplate><%#: Item.DurationEnum.ToFullName() %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Intake Target (Students)">
				<ItemTemplate><%#: Item.IntakeTargetStudentsCount %></ItemTemplate>
				<FooterTemplate><%#: this.IntakeTargetStudentsCount %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="National Applicants">
				<ItemTemplate><%#: Item.NationalApplied %></ItemTemplate>
				<FooterTemplate><%#: this.NationalApplied %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Foreign Applicants">
				<ItemTemplate><%#: Item.ForeignApplied %></ItemTemplate>
				<FooterTemplate><%#: this.ForeignApplied %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Admission Processing Fee Submitted">
				<ItemTemplate><%#: Item.AdmissionProcessingFeeSubmitted %></ItemTemplate>
				<FooterTemplate><%#: this.AdmissionProcessingFeeSubmitted %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Only ETS Scorer">
				<ItemTemplate><%#: Item.ETSScorer %></ItemTemplate>
				<FooterTemplate><%#: this.ETSScorer %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Only Appeared In Entry Test">
				<ItemTemplate><%#: Item.AppearedInEntryTest %></ItemTemplate>
				<FooterTemplate><%#: this.AppearedInEntryTest %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="ETS Scorer & Appeared In Entry Test As Well">
				<ItemTemplate><%#: Item.ETSScorerAndAppearedInEntryTest %></ItemTemplate>
				<FooterTemplate><%#: this.ETSScorerAndAppearedInEntryTest %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="ETS Scorer/Appeared In Entry Test">
				<ItemTemplate><%#: Item.ETSScorerOrAppearedInEntryTest %></ItemTemplate>
				<FooterTemplate><%#: this.ETSScorerOrAppearedInEntryTest %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Appeared In Interview">
				<ItemTemplate><%#: Item.AppearedInInterview %></ItemTemplate>
				<FooterTemplate><%#: this.AppearedInInterview %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Interview Cleared">
				<ItemTemplate><%#: Item.InterviewCleared %></ItemTemplate>
				<FooterTemplate><%#: this.InterviewCleared %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Interview Cleared (Shifted From Other Programs/Semesters)">
				<ItemTemplate><%# this.GetHtml(Item.InterviewClearedShiftedFromOtherProgramsDetails) %></ItemTemplate>
				<FooterTemplate><%# this.InterviewClearedShiftedFromOtherProgramsDetailsHtml %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Interview Cleared (Shifted To Other Programs/Semester)">
				<ItemTemplate><%# this.GetHtml(Item.InterviewClearedShiftedToOtherProgramsDetails) %></ItemTemplate>
				<FooterTemplate><%# this.InterviewClearedShiftedToOtherProgramsDetailsHtml %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Admission Fee Challan Issued">
				<ItemTemplate><%#: Item.AdmissionFeeChallansIssued %></ItemTemplate>
				<FooterTemplate><%#: this.AdmissionFeeChallansIssued %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Admission Fee Paid">
				<ItemTemplate><%#: Item.AdmissionFeePaid %></ItemTemplate>
				<FooterTemplate><%#: this.AdmissionFeePaid %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Admission Fee Refunded">
				<ItemTemplate><%#: Item.AdmissionFeeRefunded%></ItemTemplate>
				<FooterTemplate><%#: this.AdmissionFeeRefunded %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Deferred From Other Programs/Semester (Before Enrollment Generation)">
				<ItemTemplate><%# this.GetHtml(Item.DeferredFromOtherProgramsDetailsBeforeEnrollmentGeneration) %></ItemTemplate>
				<FooterTemplate><%# this.DeferredFromOtherProgramsDetailsBeforeEnrollmentGenerationDetailsHtml %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Deferred To Other Programs/Semester (Before Enrollment Generation)">
				<ItemTemplate><%# this.GetHtml(Item.DeferredToOtherProgramsDetailsBeforeEnrollmentGeneration) %></ItemTemplate>
				<FooterTemplate><%# this.DeferredToOtherProgramsDetailsBeforeEnrollmentGenerationDetailsHtml %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Admitted">
				<ItemTemplate><%# this.GetHtml(Item.SemesterID, Item.ProgramID, Item.AdmittedStudents) %></ItemTemplate>
				<FooterTemplate><%# this.AdmittedStudents %></FooterTemplate>
			</asp:TemplateField>
		</Columns>
		<FooterStyle Font-Bold="True" />
	</aspire:AspireGridView>
</asp:Content>
