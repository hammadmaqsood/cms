﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="FormSubmissionTrend.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Admissions.FormSubmissionTrend" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12" >
			<aspire:AspireGridView runat="server" ID="gvFormSubmissionTrend" OnRowDataBound="gvFormSubmissionTrend_RowDataBound" />
		</div>
	</div>
</asp:Content>
