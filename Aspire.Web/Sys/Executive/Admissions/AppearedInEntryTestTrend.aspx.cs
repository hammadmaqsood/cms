﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Executive.Admissions
{
	[AspirePage("797DDDD4-1EDE-4607-BD22-A00077AB47F6", UserTypes.Executive, "~/Sys/Executive/Admissions/AppearedInEntryTestTrend", "Appeared In Test Trend", true, AspireModules.Admissions)]
	public partial class AppearedInEntryTestTrend : ExecutivePage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_list);
		public override string PageTitle => "Appeared in Entry Test Trend";

		protected void Page_Load(object sender, EventArgs e)
		{
			var trend = BL.Core.ExecutiveInformation.Executive.Admissions.AppearedInEntryTestTrend.GetAppearedInEntryTestTrend(this.ExecutiveIdentity.LoginSessionGuid);
			this.gvAppearedInEntryTestTrend.DataBind(trend);
		}

		protected void gvAppearedInEntryTestTrend_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			e.Row.Cells[0].Attributes["width"] = "200px";
		}
	}
}
