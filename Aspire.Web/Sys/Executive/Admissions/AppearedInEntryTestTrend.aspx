﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="AppearedInEntryTestTrend.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Admissions.AppearedInEntryTestTrend" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12" >
			<aspire:AspireGridView runat="server" ID="gvAppearedInEntryTestTrend" OnRowDataBound="gvAppearedInEntryTestTrend_RowDataBound" />
		</div>
	</div>
</asp:Content>
