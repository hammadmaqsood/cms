﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Executive.Admissions
{
	[AspirePage("E6706BC1-269F-41A8-9BA6-A7FCC703419C", UserTypes.Executive, "~/Sys/Executive/Admissions/FormSubmissionTrend", "Form Submission Trend", true, AspireModules.Admissions)]
	public partial class FormSubmissionTrend : ExecutivePage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_list);
		public override string PageTitle => "Admissions Form Submission Trend";

		protected void Page_Load(object sender, EventArgs e)
		{
			var trend = Aspire.BL.Core.ExecutiveInformation.Executive.Admissions.FormSubmissionTrend.GetFormSubmissionTrend(this.ExecutiveIdentity.LoginSessionGuid);
			this.gvFormSubmissionTrend.DataBind(trend);
		}

		protected void gvFormSubmissionTrend_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			e.Row.Cells[0].Attributes["width"] = "200px";
		}

	}
}