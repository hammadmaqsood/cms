﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using Aspire.Model.Entities.Common;

namespace Aspire.Web.Sys.Executive.Admissions.Reports
{
	[AspirePage("FDBC8790-12FB-44DC-8D3B-9B78E5F587E7", UserTypes.Executive, "~/Sys/Executive/Admissions/Reports/EntryTestMarksCertificate.aspx", "Entry Test Marks Certificate", true, AspireModules.Admissions)]
	public partial class EntryTestMarksCertificate : ExecutivePage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Entry Test Marks Certificate";
		public string ReportName => this.PageTitle;
		public bool ExportToExcel => true;
		public bool ExportToWord => true;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlIntakeSemesterIDFilter.FillSemesters();
				if (this.ddlIntakeSemesterIDFilter.Items.Count == 0)
				{
					this.AddErrorAlert("No Semester record found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlDepartmentIDFilter.FillDepartments(this.ExecutiveIdentity.InstituteID);
				this.ddlIntakeSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlIntakeSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlIntakeSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			this.ddlProgramIDFilter.FillPrograms(this.ExecutiveIdentity.InstituteID, departmentID, true, CommonListItems.All);
			this.ddlProgramIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView.ReportViewer);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			var intakeSemesterID = this.ddlIntakeSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			reportViewer.LocalReport.DataSources.Clear();
			var reportFileName = "~/Reports/Admissions/Executive/EntryTestMarksCertificate.rdlc".MapPath();
			var reportDataSet = BL.Core.Admissions.Executive.Reports.EntryTestMarksCertificate.GetEntryTestMarksCertificate(intakeSemesterID, departmentID, programID, this.ExecutiveIdentity.LoginSessionGuid);
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.EntryTestMarksCertificate), reportDataSet.EntryTestMarksCertificate));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}