﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Executive.Admissions
{
	[AspirePage("70F9F409-8A65-4BA8-8988-9CD6E9B8A8BD", UserTypes.Executive, "~/Sys/Executive/Admissions/EnrolledStudentsTrend", "Enrolled Students Trend", true, AspireModules.Admissions)]
	public partial class EnrolledStudentsTrend : ExecutivePage
	{
		public override PageIcon PageIcon => AspireGlyphicons.list.GetIcon();
		public override string PageTitle => "Enrolled Students Trend";

		protected void Page_Load(object sender, EventArgs e)
		{
			var trend = Aspire.BL.Core.ExecutiveInformation.Executive.Admissions.EnrolledStudentsTrend.GetEnrolledStudentsTrend(this.ExecutiveIdentity.LoginSessionGuid);
			this.gvEnrolledStudentsTrend.DataBind(trend);
		}

		protected void gvEnrolledStudentsTrend_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			e.Row.Cells[0].Attributes["width"] = "200px";
		}

	}
}