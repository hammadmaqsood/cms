﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="EnrolledStudentsTrend.aspx.cs" Inherits="Aspire.Web.Sys.Executive.Admissions.EnrolledStudentsTrend" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12" style="overflow-x: auto; width: 100%;">
			<aspire:AspireGridView runat="server" ID="gvEnrolledStudentsTrend" OnRowDataBound="gvEnrolledStudentsTrend_OnRowDataBound" />
		</div>
	</div>
</asp:Content>
