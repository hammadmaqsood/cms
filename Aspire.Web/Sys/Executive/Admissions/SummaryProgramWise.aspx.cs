﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Executive.Admissions
{
	[AspirePage("767DF715-3390-47AC-B77B-98FA8533FD2C", UserTypes.Executive, "~/Sys/Executive/Admissions/SummaryProgramWise", "Program Wise Summary", true, AspireModules.Admissions)]
	public partial class SummaryProgramWise : ExecutivePage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_list);
		public override string PageTitle => "Program Wise Admissions Summary";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlSemesterIDFilter.FillSemesters();
				this.ddlDepartmentIDFilter.FillDepartments(this.ExecutiveIdentity.InstituteID, CommonListItems.All);

				for (int i = 4; i < this.gvAdmissionSummary.Columns.Count; i++)
					this.cblColumns.Items.Add(new ListItem { Text = this.gvAdmissionSummary.Columns[i].HeaderText, Selected = true, Value = i.ToString() });

				this.cblColumns.Items.FindByText("Only ETS Scorer").Selected = false;
				this.cblColumns.Items.FindByText("Only Appeared In Entry Test").Selected = false;
				this.cblColumns.Items.FindByText("ETS Scorer & Appeared In Entry Test As Well").Selected = false;
				this.cblColumns.Items.FindByText("Interview Cleared").Selected = false;
				this.cblColumns.Items.FindByText("Interview Cleared (Shifted From Other Programs/Semesters)").Selected = false;
				this.cblColumns.Items.FindByText("Interview Cleared (Shifted To Other Programs/Semester)").Selected = false;

				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void cblColumns_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.SyncColumnVisibility();
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			if (departmentID == null)
				this.ddlProgramIDFilter.DataBind(CommonListItems.All);
			else
				this.ddlProgramIDFilter.FillPrograms(this.ExecutiveIdentity.InstituteID, departmentID, true, CommonListItems.All);
			this.ddlProgramIDFilter_SelectedIndexChanged(null, null);
		}

		protected List<BL.Core.ExecutiveInformation.Executive.Admissions.ProgramWiseSummary.AdmissionsSummary> programs;

		protected void ddlProgramIDFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDFilter.SelectedValue.ToNullableInt();
			this.programs = BL.Core.ExecutiveInformation.Executive.Admissions.ProgramWiseSummary.GetProgramWiseAdmissionSummary(semesterID, departmentID, programID, this.ExecutiveIdentity.LoginSessionGuid);
			this.gvAdmissionSummary.DataBind(this.programs);
			this.SyncColumnVisibility();
		}

		private void SyncColumnVisibility()
		{
			var selectedColumnIndexes = this.cblColumns.GetSelectedValues().Select(v => v.ToInt());
			for (int i = 4; i < this.gvAdmissionSummary.Columns.Count; i++)
				this.gvAdmissionSummary.Columns[i].Visible = selectedColumnIndexes.Contains(i);
		}

		protected int IntakeTargetStudentsCount => this.programs.Sum(p => p.IntakeTargetStudentsCount);

		protected int NationalApplied => this.programs.Sum(p => p.NationalApplied);

		protected int ForeignApplied => this.programs.Sum(p => p.ForeignApplied);

		protected int AdmissionProcessingFeeSubmitted => this.programs.Sum(p => p.AdmissionProcessingFeeSubmitted);

		protected int ETSScorer => this.programs.Sum(p => p.ETSScorer);

		protected int AppearedInEntryTest => this.programs.Sum(p => p.AppearedInEntryTest);

		protected int ETSScorerAndAppearedInEntryTest => this.programs.Sum(p => p.ETSScorerAndAppearedInEntryTest);

		protected int ETSScorerOrAppearedInEntryTest => this.programs.Sum(p => p.ETSScorerOrAppearedInEntryTest);

		protected int AppearedInInterview => this.programs.Sum(p => p.AppearedInInterview);

		protected int InterviewCleared => this.programs.Sum(p => p.InterviewCleared);

		protected int AdmissionFeeChallansIssued => this.programs.Sum(p => p.AdmissionFeeChallansIssued);

		protected int AdmissionFeePaid => this.programs.Sum(p => p.AdmissionFeePaid);

		protected int AdmissionFeeRefunded => this.programs.Sum(p => p.AdmissionFeeRefunded);

		protected string GetHtml(List<BL.Core.ExecutiveInformation.Executive.Admissions.ProgramWiseSummary.AdmissionsSummary.Detail> details)
		{
			if (details?.Any() != true)
				return "0".HtmlEncode();
			var rowsHtml = details.GroupBy(d => new { d.AdmissionOpenProgramID, d.ProgramAlias, d.SemesterID })
				.OrderBy(g => g.Key.ProgramAlias).ThenBy(g => g.Key.SemesterID)
				.Select(g => $@"<tr>
<td>{g.Key.ProgramAlias.HtmlEncode()}</td>
<td>{g.Key.SemesterID.ToSemesterString().HtmlEncode()}</td>
<td>{g.Sum(d => d.Count).ToString().HtmlEncode()}</td>
</tr>");
			return $"<table class=\"table table-bordered table-condensed table-hover\">" +
				$"<tbody>{rowsHtml.Join("\n")}</tbody>" +
				$"<tfoot><th colspan=\"2\">Total</th><th>{details.Sum(d => d.Count).ToString().HtmlEncode()}</tr></tfoot>" +
				$"</table>";
		}

		protected string InterviewClearedShiftedFromOtherProgramsDetailsHtml => this.GetHtml(this.programs.Where(p => p.InterviewClearedShiftedFromOtherProgramsDetails != null).SelectMany(p => p.InterviewClearedShiftedFromOtherProgramsDetails).ToList());

		protected string InterviewClearedShiftedToOtherProgramsDetailsHtml => this.GetHtml(this.programs.Where(p => p.InterviewClearedShiftedToOtherProgramsDetails != null).SelectMany(p => p.InterviewClearedShiftedToOtherProgramsDetails).ToList());

		protected string DeferredFromOtherProgramsDetailsBeforeEnrollmentGenerationDetailsHtml => this.GetHtml(this.programs.Where(p => p.DeferredFromOtherProgramsDetailsBeforeEnrollmentGeneration != null).SelectMany(p => p.DeferredFromOtherProgramsDetailsBeforeEnrollmentGeneration).ToList());

		protected string DeferredToOtherProgramsDetailsBeforeEnrollmentGenerationDetailsHtml => this.GetHtml(this.programs.Where(p => p.DeferredToOtherProgramsDetailsBeforeEnrollmentGeneration != null).SelectMany(p => p.DeferredToOtherProgramsDetailsBeforeEnrollmentGeneration).ToList());

		protected string GetHtml(short? intakeSemesterID, int? programID, List<BL.Core.ExecutiveInformation.Executive.Admissions.ProgramWiseSummary.AdmissionsSummary.Students> students)
		{
			if (students?.Any() != true)
				return "0".HtmlEncode();
			var rowsHtml = students.GroupBy(s => new { s.AdmissionTypeEnum, s.StatusEnum, s.Foreigner })
				.OrderBy(g => g.Key.AdmissionTypeEnum).ThenBy(g => g.Key.StatusEnum)
				.Select(g => $@"<tr>
<td>{g.Key.AdmissionTypeEnum.ToFullName().HtmlEncode()}</td>
<td>{g.Key.Foreigner.ToYesNo().HtmlEncode()}</td>
<td>{g.Key.StatusEnum?.ToFullName().HtmlEncode()}</td>
<td><a target=""_blank"" href=""{this.ResolveClientUrl(Students.StudentSearch.GetPageUrl(intakeSemesterID, g.Key.AdmissionTypeEnum, null, null, null, null, programID, null, g.Key.Foreigner ? "FOREIGNERS" : "PAKISTANI", g.Key.StatusEnum, false, null, null, null, null, true)).HtmlAttributeEncode()}"">{g.Sum(d => d.StudentsCount).ToString().HtmlEncode()}</a></td>
</tr>");
			return $"<table class=\"table table-bordered table-condensed table-hover\">" +
				$"<thead><tr><th>Type</th><th>Foreigner</th><th>Status</th><th>Count</th></tr></thead>" +
				$"<tbody>{rowsHtml.Join("\n")}</tbody>" +
				$"<tfoot><th colspan=\"3\">Total</th><th>{students.Sum(s => s.StudentsCount).ToString().HtmlEncode()}</tr></tfoot>" +
				$"</table>";
		}

		protected string AdmittedStudents => this.GetHtml(null, null, this.programs.Where(p => p.AdmittedStudents != null).SelectMany(p => p.AdmittedStudents).ToList());
	}
}