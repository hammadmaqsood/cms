using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Executive
{
	public abstract class ExecutivePage : AspireBasePage, IBreadcrumb, ISidebar, IProfileInfo
	{
		protected virtual IReadOnlyDictionary<ExecutivePermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired { get; }
		protected override void Authenticate()
		{
			if (this.ExecutiveIdentity == null)
				throw new AspireAccessDeniedException(UserTypes.Executive, UserTypes.Executive);
			if (this.PermissionsRequired != null)
				foreach (var permission in this.PermissionsRequired)
					this.ExecutiveIdentity.Demand(permission.Key, permission.Value);
		}

		private IExecutiveIdentity _executiveIdentity;
		public IExecutiveIdentity ExecutiveIdentity => this._executiveIdentity ?? (this._executiveIdentity = this.AspireIdentity as IExecutiveIdentity);

		public override AspireThemes AspireTheme => AspireDefaultThemes.Executive;

		public virtual bool BreadcrumbVisible => true;

		public override bool PageTitleVisible => true;

		public virtual bool SidebarVisible => true;

		public virtual string SidebarHtml => SessionHelper.SideBarLinkHtml;

		public virtual string BreadcrumbHtml => SessionHelper.SideBarLinks?.GetBreadcrumbHtml(this.AspirePageAttribute.AspirePageGuid);

		public virtual bool ProfileInfoVisible => false;

		public virtual bool ProfileLinkVisible => false;

		public bool IsCustomProfileImage => false;

		public string CustomProfileImageUrl => null;

		public virtual string ProfileNavigateUrl => null;

		public virtual bool LogoffLinkVisible => true;

		public virtual string LogoffNavigateUrl => Logoff.GetPageUrl(UserLoginHistory.LogOffReasons.LogOff);

		public virtual string Username => this.ExecutiveIdentity.Name;

		public virtual string Name => this.ExecutiveIdentity.Name;

		public virtual string Email => this.ExecutiveIdentity.Email;
	}
}