﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Executive
{
	[AspirePage("2020B278-82F5-4BDE-8BCD-BF56549E2691", UserTypes.Executive, "~/Sys/Executive/PrepareSession.aspx", null, false, AspireModules.None)]
	public partial class PrepareSession : BasePage
	{
		protected override void Authenticate()
		{
			if (ExecutiveIdentity.Current == null)
				throw new AspireAccessDeniedException(UserTypes.Executive, UserTypes.Executive);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SessionHelper.SideBarLinkHtml == null)
			{
				this.PrepareMenu();
			}
			Redirect(GetPageUrl<Dashboard>());
		}

		private void PrepareMenu()
		{
			var homePageAspirePageAttribute = AspirePageAttributesDictionary.GetAspirePageAttribute<Dashboard>();
			var homeAspireSideBarLink = new AspireSideBarLink(null, homePageAspirePageAttribute.AspirePageGuid, homePageAspirePageAttribute.MenuItemText, homePageAspirePageAttribute.PageUrl, true, null, null);
			var userGroupMenuLinks = Aspire.BL.Core.LoginManagement.Executive.GetUserGroupMenuLinks(ExecutiveIdentity.Current.LoginSessionGuid);

			var sidebarLinks = userGroupMenuLinks.ToAspireSideBarLinks(homeAspireSideBarLink);
			SessionHelper.SetSideBarLinks(sidebarLinks);
		}
	}
}