﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Faculty.ClassAttendance
{
	[AspirePage("451D7664-3445-41E4-AB39-7F28108D66F3", UserTypes.Faculty, "~/Sys/Faculty/ClassAttendance/StudentSummary.aspx", "Student Attendance", true, AspireModules.ClassAttendance)]
	public partial class StudentSummary : FacultyPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_list_ol);
		public override string PageTitle => "Student's Attendance";

		public int? RegisteredCourseID => this.Request.GetParameterValue<int>("RegisteredCourseID");

		public static string GetPageUrl(int registeredCourseID)
		{
			return GetPageUrl<StudentSummary>().AttachQueryParam("RegisteredCourseID", registeredCourseID);
		}

		public static void Redirect(int registeredCourseID)
		{
			Redirect(GetPageUrl(registeredCourseID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.RegisteredCourseID == null)
				{
					Redirect<Dashboard>();
					return;
				}
				var offeredCourse = Aspire.BL.ClassAttendance.Faculty.GetStudentAttendanceDetails(this.RegisteredCourseID.Value);
				if (offeredCourse == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Courses>();
					return;
				}

				this.lblEnrollment.Text = offeredCourse.Enrollment;
				this.lblRegistration.Text = offeredCourse.RegistrationNo.ToString();
				this.lblName.Text = offeredCourse.Name;
				this.lblStudentProgram.Text = offeredCourse.StudentClassName;

				this.lblClass.Text = offeredCourse.OfferedCourseClassName;
				this.lblContactHrs.Text = offeredCourse.ContactHours.ToCreditHoursFullName();
				this.lblCourseTitle.Text = offeredCourse.Title;
				this.lblCreditHrs.Text = offeredCourse.CreditHours.ToCreditHoursFullName();
				this.lblTeacherName.Text = offeredCourse.TeacherName;
				this.lblSemesterID.Text = offeredCourse.SemesterID.ToSemesterString();
				this.gvAttendanceDetails.DataBind(offeredCourse.Details);
				if (this.gvAttendanceDetails.FooterRow != null)
				{
					this.gvAttendanceDetails.FooterRow.Cells[5].Text = offeredCourse.ClassesTotalHours.ToString("0.##");
					this.gvAttendanceDetails.FooterRow.Cells[6].Text = $"{offeredCourse.PresentTotalHours:0.##} : {offeredCourse.PresentTotalPercentage:0.##}%";
					this.gvAttendanceDetails.FooterRow.Cells[7].Text = offeredCourse.AbsentTotalHours.ToString("0.##");
				}
			}
		}
	}
}