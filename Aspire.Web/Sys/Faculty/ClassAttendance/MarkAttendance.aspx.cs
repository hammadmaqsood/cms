﻿using Aspire.BL.Entities.ClassAttendance;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Faculty.ClassAttendance
{
	[AspirePage("B88329AF-6877-4337-AC46-679CC34271BD", UserTypes.Faculty, "~/Sys/Faculty/ClassAttendance/MarkAttendance.aspx", "Mark Attendance", true, AspireModules.ClassAttendance)]
	public partial class MarkAttendance : FacultyPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_list_ol);
		public override string PageTitle => this.Request.GetParameterValue<int>(nameof(this.OfferedCourseAttendanceID)) == null ? "Mark Class Attendance" : "Edit Class Attendance";

		public static string GetPageUrl(int offeredCourseID, int? offeredCourseAttendanceID)
		{
			return GetPageUrl<MarkAttendance>().AttachQueryParams(nameof(OfferedCourseID), offeredCourseID, nameof(OfferedCourseAttendanceID), offeredCourseAttendanceID);
		}

		public static void Redirect(int offeredCourseID, int? offeredCourseAttendanceID)
		{
			Redirect(GetPageUrl(offeredCourseID, offeredCourseAttendanceID));
		}

		private bool ByPassChecks => false;//this.FacultyIdentity.ImpersonatedByUserLoginHistoryID != null;
		private int OfferedCourseID
		{
			get => (int)this.ViewState[nameof(this.OfferedCourseID)];
			set => this.ViewState[nameof(this.OfferedCourseID)] = value;
		}
		private int? OfferedCourseAttendanceID
		{
			get => (int?)this.ViewState[nameof(this.OfferedCourseAttendanceID)];
			set => this.ViewState[nameof(this.OfferedCourseAttendanceID)] = value;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var offeredCourseID = this.Request.GetParameterValue<int>(nameof(this.OfferedCourseID));
				var offeredCourseAttendanceID = this.Request.GetParameterValue<int>(nameof(this.OfferedCourseAttendanceID));

				if (offeredCourseID == null)
				{
					Redirect<Courses>();
					return;
				}

				var offeredCourse = BL.ClassAttendance.Faculty.GetOfferedCourseAttendances(offeredCourseID.Value, offeredCourseAttendanceID, this.ByPassChecks, out bool canMark, out bool canEdit, out bool canDelete);
				if (offeredCourse == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Courses>();
					return;
				}
				this.hlCancel.NavigateUrl = ViewClassAttendance.GetPageUrl(offeredCourse.OfferedCourseID);
				this.lblClass.Text = offeredCourse.ClassName;
				this.lblContactHrs.Text = offeredCourse.ContactHours.ToCreditHoursFullName();
				this.lblCourseTitle.Text = offeredCourse.Title;
				this.lblCreditHrs.Text = offeredCourse.CreditHours.ToCreditHoursFullName();
				this.lblTeacherName.Text = offeredCourse.Teacher;
				this.lblSemesterID.Text = offeredCourse.OfferedSemesterID.ToSemesterString();
				this.OfferedCourseID = offeredCourse.OfferedCourseID;
				this.OfferedCourseAttendanceID = offeredCourse.OfferedCourseAttendanceID;

				this.rblNoOfHours.FillHoursValues(true);
				this.rblClassTypes.FillSemesters().FillEnums<OfferedCourseAttendance.ClassTypes>();
				this.ddlRoom.GetRoomsForAttendance(this.FacultyIdentity.InstituteID);

				if (offeredCourse.OfferedCourseAttendanceID != null)
				{
					this.rblClassTypes.SetEnumValue(offeredCourse.ClassTypeEnum.Value);
					this.rblNoOfHours.SetEnumValue(offeredCourse.HoursValue.Value);
					this.dtpClassDate.SelectedDate = offeredCourse.ClassDate.Value;
					this.ddlRoom.SelectedValue = offeredCourse.RoomID.ToString();
					this.tbRemarks.Text = offeredCourse.Remarks;
					this.tbTopicsCovered.Text = offeredCourse.TopicsCovered;

					this.dtpClassDate.ReadOnly = true;
					if (this.ByPassChecks)
						this.dtpClassDate.ReadOnly = false;
					this.rblNoOfHours.Enabled = false;
					this.rblNoOfHours.AutoPostBack = false;
					this.rblNoOfHours.SelectedIndexChanged -= this.rblNoOfHours_OnSelectedIndexChanged;
					this.tbRemarks.ReadOnly = !canEdit;
					this.tbTopicsCovered.ReadOnly = !canEdit;
					this.btnSaveAttendance.Text = "Save";
					this.btnSaveAttendance.Visible = canEdit;
					this.btnDeleteAttendance.Visible = canDelete;
					if (canEdit == false)
						this.AddWarningAlert("You are not authorized to edit this record.");
					if (canDelete == false)
						this.AddWarningAlert("You are not authorized to delete this record.");
				}
				else
				{
					if (canMark == false)
					{
						this.AddErrorAlert("You are not authorized to mark attendance of the selected course.");
						Redirect<Faculty.Courses>();
						return;
					}
					this.rblClassTypes.SetEnumValue(OfferedCourseAttendance.ClassTypes.Class);
					this.rblNoOfHours.Enabled = true;
					this.rblNoOfHours.SetEnumValue(OfferedCourseAttendance.HoursValues.One);
					this.dtpClassDate.SelectedDate = DateTime.Today;
					this.dtpClassDate.ReadOnly = false;
					this.tbRemarks.Text = null;
					this.tbTopicsCovered.Text = null;
					this.btnSaveAttendance.Text = "Save";
					this.btnDeleteAttendance.Visible = false;
				}

				this.rblNoOfHours_OnSelectedIndexChanged(null, null);
			}
		}

		protected void rblNoOfHours_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			List<CustomOfferedCourseAttendanceDetails> attendanceDetails;
			if (this.OfferedCourseAttendanceID == null)
			{
				this.tableSummary.Visible = false;
				attendanceDetails = BL.ClassAttendance.Faculty.GetOfferedCourseAttendanceDetails(this.OfferedCourseID, null);
				var totalHours = this.rblNoOfHours.GetSelectedEnumValue<OfferedCourseAttendance.HoursValues>();
				foreach (var enrollment in attendanceDetails)
				{
					if (enrollment.FeeDefaulter)
						enrollment.HoursValue = OfferedCourseAttendance.HoursValues.Absent;
					else
						enrollment.HoursValue = totalHours;
				}
			}
			else
			{
				attendanceDetails = BL.ClassAttendance.Faculty.GetOfferedCourseAttendanceDetails(this.OfferedCourseID, this.OfferedCourseAttendanceID.Value);
				this.tableSummary.Visible = true;
				var totalHours = this.rblNoOfHours.GetSelectedEnumValue<OfferedCourseAttendance.HoursValues>();
				var presentCell = this.tableSummary.Rows[0].Cells[0];
				var partialCell = this.tableSummary.Rows[0].Cells[1];
				var absentCell = this.tableSummary.Rows[0].Cells[2];
				switch (totalHours)
				{
					case OfferedCourseAttendance.HoursValues.Absent:
						throw new InvalidOperationException();
					case OfferedCourseAttendance.HoursValues.One:
					case OfferedCourseAttendance.HoursValues.OneAndHalf:
						presentCell.Text = $"Present: {attendanceDetails.Count(a => a.HoursValue == totalHours)}";
						partialCell.Visible = false;
						absentCell.Text = $"Absent: {attendanceDetails.Count(a => a.HoursValue != totalHours)}";
						break;
					case OfferedCourseAttendance.HoursValues.Two:
					case OfferedCourseAttendance.HoursValues.Three:
						presentCell.Text = $"Full Present: {attendanceDetails.Count(a => a.HoursValue == totalHours)}";
						partialCell.Text = $"Partial Present: {attendanceDetails.Count(a => a.HoursValue != null && a.HoursValue != OfferedCourseAttendance.HoursValues.Absent && a.HoursValue != totalHours)}";
						absentCell.Text = $"Full Absent: {attendanceDetails.Count(a => (a.HoursValue ?? OfferedCourseAttendance.HoursValues.Absent) == OfferedCourseAttendance.HoursValues.Absent)}";
						break;
					default:
						throw new NotImplementedEnumException(totalHours);
				}
			}

			if (this.OfferedCourseAttendanceID != null || attendanceDetails.Any())
				this.gvOfferedCourseEnrollments.DataBind(attendanceDetails);
			else
			{
				this.AddWarningAlert("No valid student registration found for attendance.");
				Redirect<Courses>();
			}
		}

		protected void gvOfferedCourseEnrollments_OnRowCreated(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow)
				return;
			var rbl = (AspireRadioButtonList)e.Row.FindControl("rbl");
			var totalHours = this.rblNoOfHours.GetSelectedEnumValue<OfferedCourseAttendance.HoursValues>();
			var presentIfOneOrOneAndHalf = totalHours == OfferedCourseAttendance.HoursValues.One || totalHours == OfferedCourseAttendance.HoursValues.OneAndHalf;
			rbl.FillHoursValues(totalHours.GetSmallerOrEqualHourValue(), presentIfOneOrOneAndHalf);

			if (!this.IsPostBack && this.OfferedCourseAttendanceID != null)
			{
				var hours = ((CustomOfferedCourseAttendanceDetails)e.Row.DataItem).HoursValue;
				switch (hours)
				{
					case null:
					case OfferedCourseAttendance.HoursValues.Absent:
						e.Row.CssClass = "bg-danger";
						break;
					case OfferedCourseAttendance.HoursValues.One:
					case OfferedCourseAttendance.HoursValues.OneAndHalf:
					case OfferedCourseAttendance.HoursValues.Two:
					case OfferedCourseAttendance.HoursValues.Three:
						e.Row.CssClass = totalHours == hours.Value ? "bg-success" : "bg-warning";
						break;
					default:
						throw new NotImplementedEnumException(hours);
				}
			}
		}

		protected void btnSaveAttendance_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var attendance = new MarkClassAttendance(this.rblNoOfHours.GetSelectedEnumValue<OfferedCourseAttendance.HoursValues>())
			{
				OfferedCourseID = this.OfferedCourseID,
				OfferedCourseAttendanceID = this.OfferedCourseAttendanceID,
				ClassDate = this.dtpClassDate.SelectedDate.Value.Date,
				ClassType = this.rblClassTypes.SelectedValue.ToByte(),
				RoomID = this.ddlRoom.SelectedValue.ToInt(),
				Remarks = this.tbRemarks.Text,
				TopicsCovered = this.tbTopicsCovered.Text,
			};

			foreach (GridViewRow row in this.gvOfferedCourseEnrollments.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow)
					continue;
				var registeredCourseID = ((Lib.WebControls.HiddenField)row.FindControl("hfRCID")).Value.ToInt();
				var feeDefaulter = ((Lib.WebControls.HiddenField)row.FindControl("hfFeeDefaulter")).Value.ToBoolean();
				var hoursValue = ((AspireRadioButtonList)row.FindControl("rbl")).GetSelectedEnumValue<OfferedCourseAttendance.HoursValues>();
				attendance.AddStudent(registeredCourseID, hoursValue, feeDefaulter);
			}

			if (this.OfferedCourseAttendanceID == null)
			{
				var offeredCourseAttendanceID = Aspire.BL.ClassAttendance.Faculty.AddClassAttendance(attendance, this.ByPassChecks, this.FacultyIdentity.UserLoginHistoryID, out var canMark);
				if (offeredCourseAttendanceID != null)
				{
					if (canMark == true)
					{
						this.AddSuccessAlert("Attendance has been marked.");
						Redirect(attendance.OfferedCourseID, offeredCourseAttendanceID.Value);
						return;
					}
					else
						throw new InvalidOperationException();
				}
				else
				{
					if (canMark == null)
					{
						this.AddNoRecordFoundAlert();
						Redirect<Courses>();
						return;
					}
					if (canMark.Value)
						throw new InvalidOperationException();
					else
					{
						this.AddErrorAlert($"Attendance Marking is not allowed for the date {attendance.ClassDate:D}.");
						return;
					}
				}
			}
			else
			{
				var edited = Aspire.BL.ClassAttendance.Faculty.EditClassAttendance(attendance, this.ByPassChecks, this.FacultyIdentity.UserLoginHistoryID, out var canEdit);
				if (edited)
				{
					if (canEdit == null)
						throw new InvalidOperationException();
					if (canEdit.Value)
					{
						this.AddSuccessAlert("Attendance has been updated.");
						if (this.OfferedCourseAttendanceID != null)
							Redirect(this.OfferedCourseID, this.OfferedCourseAttendanceID.Value);
						else
							Redirect<Courses>();
						return;
					}
					else
						throw new InvalidOperationException();
				}
				else
				{
					if (canEdit == null)
					{
						this.AddNoRecordFoundAlert();
						Redirect<Courses>();
						return;
					}
					if (canEdit.Value)
						throw new InvalidOperationException();
					else
					{
						this.AddErrorAlert("You are not authorized to edit the marked attendance.");
						return;
					}
				}
			}
		}

		protected void btnDeleteAttendance_OnClick(object sender, EventArgs e)
		{
			var deleted = Aspire.BL.ClassAttendance.Faculty.DeleteClassAttendance(this.OfferedCourseAttendanceID ?? throw new InvalidOperationException(), this.ByPassChecks, this.FacultyIdentity.UserLoginHistoryID, out var canDelete);
			if (deleted)
			{
				if (canDelete == null)
					throw new InvalidOperationException();
				else
				{
					this.AddSuccessMessageHasBeenDeleted("Class attendance");
					ViewClassAttendance.Redirect(this.OfferedCourseID);
					return;
				}
			}
			else
			{
				if (canDelete == null)
					this.AddNoRecordFoundAlert();
				else if (!canDelete.Value)
					this.AddErrorAlert("You are not authorized to delete the selected class attendance.");
				else
					throw new InvalidOperationException();
			}
		}
	}
}