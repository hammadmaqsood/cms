﻿using Aspire.BL.Entities.ClassAttendance;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Faculty.ClassAttendance
{
	[AspirePage("AEFDAF07-AFA5-4686-8B36-8066735F4EFC", UserTypes.Faculty, "~/Sys/Faculty/ClassAttendance/ClassSummary.aspx", "Class Summary", true, AspireModules.ClassAttendance)]
	public partial class ClassSummary : FacultyPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.list_alt);
		public override string PageTitle => "Class Attendance Summary";

		private int? OfferedCourseID => this.Request.GetParameterValue<int>("OfferedCourseID");

		public static string GetPageUrl(int offeredCourseID)
		{
			return GetPageUrl<ClassSummary>().AttachQueryParam("OfferedCourseID", offeredCourseID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.OfferedCourseID == null)
				{
					Redirect<Courses>();
					return;
				}

				var offeredCourse = Aspire.BL.ClassAttendance.Faculty.GetAttendanceSummary(this.OfferedCourseID.Value);
				if (offeredCourse == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Courses>();
					return;
				}
				this.hlMarkAttendance.NavigateUrl = MarkAttendance.GetPageUrl(offeredCourse.OfferedCourseID, null);
				this.hlViewAttendance.NavigateUrl = ViewClassAttendance.GetPageUrl(offeredCourse.OfferedCourseID);
				this.lblClass.Text = offeredCourse.ClassName;
				this.lblContactHrs.Text = offeredCourse.ContactHours.ToCreditHoursFullName();
				this.lblCourseTitle.Text = offeredCourse.Title;
				this.lblCreditHrs.Text = offeredCourse.CreditHours.ToCreditHoursFullName();
				this.lblTeacherName.Text = offeredCourse.TeacherName;
				this.lblSemesterID.Text = offeredCourse.SemesterID.ToSemesterString();
				this.gvAttendanceRecord.DataBind(offeredCourse.Summary);
			}
		}

		protected void gvAttendanceRecord_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow)
				return;
			var detail = (CustomAttendanceSummary.Detail)e.Row.DataItem;
			if (detail.Danger)
				e.Row.CssClass = "danger";
		}
	}
}