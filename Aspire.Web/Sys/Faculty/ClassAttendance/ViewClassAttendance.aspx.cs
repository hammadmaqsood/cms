﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Faculty.ClassAttendance
{
	[AspirePage("4C4FB9AC-F20C-4249-B953-AD8BCFCC39F6", UserTypes.Faculty, "~/Sys/Faculty/ClassAttendance/ViewClassAttendance.aspx", "View Class Attendance", true, AspireModules.ClassAttendance)]
	public partial class ViewClassAttendance : FacultyPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.list);
		public override string PageTitle => "View Class Attendance";

		private int? OfferedCourseID => this.Request.GetParameterValue<int>("OfferedCourseID");

		public static string GetPageUrl(int offeredCourseID)
		{
			return GetPageUrl<ViewClassAttendance>().AttachQueryParam("OfferedCourseID", offeredCourseID);
		}

		public static void Redirect(int offeredCourseID)
		{
			Redirect(GetPageUrl(offeredCourseID));
		}

		private bool ByPassChecks => false;//this.FacultyIdentity.ImpersonatedByUserLoginHistoryID != null;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.OfferedCourseID == null)
				{
					Redirect<Courses>();
					return;
				}

				var offeredCourseAttendance = BL.ClassAttendance.Faculty.GetOfferedCourseAttendances(this.OfferedCourseID.Value);
				if (offeredCourseAttendance == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Courses>();
					return;
				}
				this.hlMarkAttendance.NavigateUrl = MarkAttendance.GetPageUrl(offeredCourseAttendance.OfferedCourseID, null);
				this.hlSummary.NavigateUrl = ClassSummary.GetPageUrl(offeredCourseAttendance.OfferedCourseID);
				this.lblCourseTitle.Text = $"[{offeredCourseAttendance.CourseCode}] {offeredCourseAttendance.Title}";
				this.lblTeacherName.Text = offeredCourseAttendance.TeacherName.ToNAIfNullOrEmpty();
				this.lblCreditHrs.Text = offeredCourseAttendance.CreditHours.ToCreditHoursFullName();
				this.lblContactHrs.Text = offeredCourseAttendance.ContactHours.ToCreditHoursFullName();
				this.lblClass.Text = offeredCourseAttendance.ClassName;
				this.lblSemesterID.Text = offeredCourseAttendance.SemesterID.ToSemesterString();
				if (offeredCourseAttendance.Details.Any())
					this.gvAttendanceRecord.ShowFooter = true;
				this.gvAttendanceRecord.DataBind(offeredCourseAttendance.Details);
				if (this.gvAttendanceRecord.ShowFooter)
				{
					var hours = offeredCourseAttendance.Details.Sum(d => d.Hours);
					this.gvAttendanceRecord.FooterRow.Cells[3].Text = hours.ToString("N");
				}
			}
		}

		protected void gvAttendanceRecord_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					bool? canDelete;
					var offeredCourseAttendanceID = e.DecryptedCommandArgumentToInt();
					var deleted = Aspire.BL.ClassAttendance.Faculty.DeleteClassAttendance(offeredCourseAttendanceID, this.ByPassChecks, this.FacultyIdentity.UserLoginHistoryID, out canDelete);
					if (deleted)
					{
						if (canDelete == null)
							throw new InvalidOperationException();
						else
							this.AddSuccessMessageHasBeenDeleted("Class attendance");
					}
					else
					{
						if (canDelete == null)
							this.AddNoRecordFoundAlert();
						else if (!canDelete.Value)
							this.AddErrorAlert("You are not authorized to delete the selected class attendance.");
						else
							throw new InvalidOperationException();
					}
					Redirect(this.OfferedCourseID.Value);
					break;
			}
		}
	}
}