﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Documents.aspx.cs" Inherits="Aspire.Web.Sys.Faculty.OfficialDocuments.Documents" %>

<%@ Register Src="~/Sys/Common/OfficialDocuments/Documents.ascx" TagPrefix="uc1" TagName="Documents" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc1:Documents runat="server" id="documents" />
</asp:Content>