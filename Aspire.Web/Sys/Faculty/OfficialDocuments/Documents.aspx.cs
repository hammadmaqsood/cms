﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Faculty.OfficialDocuments
{
	[AspirePage("A5237AD6-F597-4FD3-AA16-3AC7768E0B15", UserTypes.Faculty, "~/Sys/Faculty/OfficialDocuments/Documents.aspx", "Official Documents", true, AspireModules.OfficialDocuments)]
	public partial class Documents : FacultyPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.book);
		public override string PageTitle => "Official Documents";

		protected void Page_Load(object sender, EventArgs e)
		{

		}
	}
}