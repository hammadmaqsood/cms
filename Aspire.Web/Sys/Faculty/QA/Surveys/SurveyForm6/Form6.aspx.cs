﻿using System;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Faculty.QA.Surveys.SurveyForm6
{
	[AspirePage("C3C4D9B9-52F3-42DD-9077-EB08465EC472", UserTypes.Faculty, "~/Sys/Faculty/QA/Surveys/SurveyForm6/Form6.aspx", "Survey Form 6", true, AspireModules.QualityAssurance)]

	public partial class Form6 : FacultyPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.list_alt);

		public override string PageTitle => "Department Survey (Survey Form-6)";
		public int? SurveyID => this.Request.GetParameterValue("QASurveyUserID").ToInt();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.Page.IsPostBack)
			{
				if (this.SurveyID != null)
					this.QAsurveyUserControl.LoadData(this.SurveyID.Value);
			}
		}

		public static string GetPageUrl(int? qaSurveyUserID)
		{
			return GetAspirePageAttribute<Form6>().PageUrl.AttachQueryParam("QASurveyUserID", qaSurveyUserID);
		}

		public static void Redirect(int? qaSurveyUserID)
		{
			Redirect(GetPageUrl(qaSurveyUserID));
		}
	}
}