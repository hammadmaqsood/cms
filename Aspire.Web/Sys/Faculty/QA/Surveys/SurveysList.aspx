﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="SurveysList.aspx.cs" Inherits="Aspire.Web.Sys.Faculty.QA.Surveys.SurveysList" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<aspire:AspireGridView runat="server" ID="gvSurveysList" OnPageSizeChanging="gvSurveysList_OnPageSizeChanging" OnSorting="gvSurveysList_OnSorting" AutoGenerateColumns="False" Caption="Survey Forms" OnPageIndexChanging="gvSurveysList_OnPageIndexChanging" OnRowCommand="gvSurveysList_OnRowCommand" AllowSorting="True">
		<Columns>
			<asp:TemplateField HeaderText="Sr#">
				<ItemTemplate>
					<%#Container.DataItemIndex+1 %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Survey Name" DataField="SurveyName" SortExpression="SurveyTypeGuid" />
			<asp:BoundField HeaderText="Semester" DataField="SemesterIDEnum" SortExpression="SemesterID" />
			<asp:BoundField HeaderText="Start Date" DataField="StartDate" SortExpression="StartDate" DataFormatString="{0:MM/dd/yyyy}" />
			<asp:BoundField HeaderText="End Date" DataField="EndDate" SortExpression="EndDate" DataFormatString="{0:MM/dd/yyyy}" />
			<asp:BoundField HeaderText="Submission Date" DataField="SubmissionDate" SortExpression="SubmissionDate" DataFormatString="{0:MM/dd/yyyy}" />
			<asp:BoundField HeaderText="Status" DataField="UserStatusFullName" SortExpression="SurvyUserStatus" />
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CausesValidation="False" Text="Evaluate" CommandName="Evaluate" EncryptedCommandArgument='<%#this.Eval("QASurveyUserID")+","+ Eval("SurveyName")%>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
