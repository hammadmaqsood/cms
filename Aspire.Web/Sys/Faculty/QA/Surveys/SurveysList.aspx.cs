﻿using System;
using System.Web.UI.WebControls;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Faculty.QA.Surveys
{
	[AspirePage("CEB27F75-BD08-43BD-BD43-87BDD721AF2D", UserTypes.Faculty, "~/Sys/Faculty/QA/Surveys/SurveysList.aspx", "Surveys List", true, AspireModules.QualityAssurance)]

	public partial class SurveysList : FacultyPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.list_alt);
		public override string PageTitle => "Faculty Surveys";
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.Page.IsPostBack)
			{
				this.ViewState.SetSortProperties("SemesterID", SortDirection.Descending);
				this.GetSurveysList(0);
			}
		}

		protected void gvSurveysList_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvSurveysList.PageSize = e.NewPageSize;
			this.GetSurveysList(0);
		}

		protected void gvSurveysList_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetSurveysList(0);
		}

		protected void gvSurveysList_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetSurveysList(e.NewPageIndex);
		}

		protected void gvSurveysList_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Evaluate":
					e.Handled = true;
					string[] decryptedValues = e.DecryptedCommandArgument().ToString().Split(new char[] { ',' }); ;
					var qaSurveyUserID = Convert.ToInt32(decryptedValues[0]);
					var surveyName = decryptedValues[1];
					switch (surveyName)
					{
						case "Faculty Survey (Survey Form-2)":
							SurveyForm2.Form2.Redirect(qaSurveyUserID);
							break;
						case "Faculty Survey (Survey Form-5)":
							SurveyForm5.Form5.Redirect(qaSurveyUserID);
							break;
						case "Department Survey (Survey Form-6)":
							SurveyForm6.Form6.Redirect(qaSurveyUserID);
							break;
						default:
							break;
					}
					break;
			}
		}

		protected void GetSurveysList(int pageIndex)
		{
			int virtualItemCount;
			var pageSize = this.gvSurveysList.PageSize;
			var surveyList = BL.Core.QualityAssurance.Faculty.SurveysList.GetSurveysList(this.FacultyIdentity.FacultyMemberID, pageIndex, pageSize, out virtualItemCount, "SemesterID", SortDirection.Descending, this.FacultyIdentity.LoginSessionGuid);
			this.gvSurveysList.DataBind(surveyList, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		public static string GetPageUrl()
		{
			return GetAspirePageAttribute<SurveysList>().PageUrl;
		}

		public static void Redirect()
		{
			Redirect(GetPageUrl());
		}
	}
}