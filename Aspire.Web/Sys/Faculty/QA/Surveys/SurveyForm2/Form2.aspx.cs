﻿using System;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Faculty.QA.Surveys.SurveyForm2
{
	[AspirePage("278C7397-2866-4064-BCE6-C9C451F7BE2E", UserTypes.Faculty, "~/Sys/Faculty/QA/Surveys/SurveyForm2/Form2.aspx", "Survey Form 2", true, AspireModules.QualityAssurance)]
	public partial class Form2 : FacultyPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.list_alt);

		public override string PageTitle => "Faculty Course Review Report (Survey Form-2)";
		public int? SurveyID => this.Request.GetParameterValue("QASurveyUserID").ToInt();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.Page.IsPostBack)
			{
				if (this.SurveyID != null)
					this.QAsurveyUserControl.LoadData(this.SurveyID.Value);
			}

		}

		public static string GetPageUrl(int? qaSurveyUserID)
		{
			return GetAspirePageAttribute<Form2>().PageUrl.AttachQueryParam("QASurveyUserID", qaSurveyUserID);
		}

		public static void Redirect(int? qaSurveyUserID)
		{
			Redirect(GetPageUrl(qaSurveyUserID));
		}
	}
}