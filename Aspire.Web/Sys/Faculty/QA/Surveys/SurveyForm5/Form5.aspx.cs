﻿using System;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Faculty.QA.Surveys.SurveyForm5
{
	[AspirePage("5ED45451-A53C-49F2-B363-D3AD6D660FF6", UserTypes.Faculty, "~/Sys/Faculty/QA/Surveys/SurveyForm5/Form5.aspx", "Survey Form 5", true, AspireModules.QualityAssurance)]

	public partial class Form5 : FacultyPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.list_alt);

		public override string PageTitle => "Faculty Survey (Survey Form-5)";
		public int? SurveyID => this.Request.GetParameterValue("QASurveyUserID").ToInt();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.Page.IsPostBack)
			{
				if (this.SurveyID != null)
					this.QAsurveyUserControl.LoadData(this.SurveyID.Value);
			}

		}

		public static string GetPageUrl(int? qaSurveyUserID)
		{
			return GetAspirePageAttribute<Form5>().PageUrl.AttachQueryParam("QASurveyUserID", qaSurveyUserID);
		}

		public static void Redirect(int? qaSurveyUserID)
		{
			Redirect(GetPageUrl(qaSurveyUserID));
		}
	}
}