﻿using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Faculty
{
	[AspirePage("9BBBAF35-C99C-44A5-82BE-68F2F9F41E4A", UserTypes.Faculty, "~/Sys/Faculty/Logoff.aspx", null, false, AspireModules.None)]
	public partial class Logoff : LogoffBasePage
	{
		public static string GetPageUrl(UserLoginHistory.LogOffReasons logoffReason)
		{
			return GetPageUrl(GetPageUrl<Logoff>(), logoffReason);
		}

		public static void Redirect(UserLoginHistory.LogOffReasons logoffReason)
		{
			Redirect(GetPageUrl(logoffReason));
		}

		protected override string LoginPageUrl => GetPageUrl<Logins.Faculty.Login>();
	}
}