﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Courses.aspx.cs" Inherits="Aspire.Web.Sys.Faculty.Courses" %>

<%@ Import Namespace="Aspire.Web.Sys.Faculty.ClassAttendance" %>
<%@ Import Namespace="Aspire.Web.Sys.Faculty.Exams" %>
<%@ Import Namespace="Aspire.Web.Sys.Faculty" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<asp:Repeater runat="server" ID="repeaterCourses" ItemType="Aspire.BL.Core.CourseOffering.Faculty.OfferedCourses.OfferedCourse">
		<HeaderTemplate>
			<table class="table table-bordered table-striped table-hover" id="courses">
				<thead>
					<tr>
						<th>#</th>
						<th>Semester</th>
						<th>Title</th>
						<th>Class</th>
						<th>Credit Hours</th>
						<th>Contact Hours</th>
						<th>Special</th>
						<th>Visiting</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
				<td><%#: Container.ItemIndex + 1 %></td>
				<td><%#: Item.Semester %></td>
				<td><%#: Item.Title %></td>
				<td><%#: Item.Class %></td>
				<td><%#: Item.CreditHoursFullName %></td>
				<td><%#: Item.ContactHoursFullName %></td>
				<td><%#: Item.SpecialOffered.ToYesNo() %></td>
				<td><%#: Item.Visiting.ToYesNo() %></td>
				<td>
					<div class="btn-group">
						<a href="#" class="dropdown-toggle text-nowrap" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions<span class="caret"></span></a>
						<ul class="dropdown-menu dropdown-menu-right">
							<li>
								<aspire:AspireHyperLink runat="server" NavigateUrl="<%# MarkAttendance.GetPageUrl(Item.OfferedCourseID, null) %>" Text="Mark Attendance" />
							</li>
							<li>
								<aspire:AspireHyperLink runat="server" NavigateUrl="<%# ViewClassAttendance.GetPageUrl(Item.OfferedCourseID) %>" Text="View Attendance" />
							</li>
							<li>
								<aspire:AspireHyperLink runat="server" NavigateUrl="<%# ClassSummary.GetPageUrl(Item.OfferedCourseID) %>" Text="Attendance Summary" />
							</li>
							<li role="separator" class="divider"></li>
							<li>
								<aspire:AspireHyperLink runat="server" Text="Export To Excel" NavigateUrl="<%# ExportToExcel.GetPageUrl(Item.OfferedCourseID) %>" />
							</li>
							<li role="separator" class="divider"></li>
							<li>
								<aspire:AspireHyperLink runat="server" Text="Exam Result Sheet" NavigateUrl="<%# MarksSheet.GetPageUrl(Item.OfferedCourseID) %>" />
							</li>
						</ul>
					</div>
				</td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</tbody>
			</table>
		</FooterTemplate>
	</asp:Repeater>
</asp:Content>
