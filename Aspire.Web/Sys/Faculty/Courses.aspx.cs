﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Faculty
{
	[AspirePage("A8D4AE66-08CF-46C8-ADEC-CEE6FDA1A40D", UserTypes.Faculty, "~/Sys/Faculty/Courses.aspx", "Courses", true, AspireModules.None)]
	public partial class Courses : FacultyPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.book);
		public override string PageTitle => "Courses";
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var semesterIDs = BL.Core.CourseOffering.Faculty.OfferedCourses.GetOfferedSemesters(this.FacultyIdentity.LoginSessionGuid);
				this.ddlSemesterID.FillSemesters(semesterIDs, CommonListItems.All);
				if (semesterIDs.Any())
					this.ddlSemesterID.SelectedIndex = 1;
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var courses = BL.Core.CourseOffering.Faculty.OfferedCourses.GetOfferedCourses(semesterID, this.FacultyIdentity.LoginSessionGuid);
			this.repeaterCourses.DataBind(courses);
		}
	}
}