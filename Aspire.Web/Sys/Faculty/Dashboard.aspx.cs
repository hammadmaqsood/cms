﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Faculty
{
	[AspirePage("F919132C-C944-4160-A189-1375791315E0", UserTypes.Faculty, "~/Sys/Faculty/Dashboard.aspx", "Home", false, AspireModules.None)]
	public partial class Dashboard : FacultyPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.FacultyIdentity.Email))
				this.AddErrorAlert("Your email address is not registered with us. Please contact IT department of your campus for update.");
		}

		public override bool BreadcrumbVisible => false;
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_desktop);
		public override string PageTitle => "Dashboard";
	}
}