﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Aspire.Web.Sys.Faculty.Dashboard" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<ul>
		<li>Registrar Notification No. 007/2020: Male students of Bahria University are allowed to wear Shalwar Kameez with Coat/Waist Coat and Shoes on all days of the week.</li>
		<li>A new feature titled <a href="../Common/Feedbacks/Feedbacks.aspx">Virtual Suggestion Box</a> has been added in CMS. Please give your valuable suggestions or complaints for betterment of University. It is to be noted that for campus level suggestions/complaints, please select <strong>DG Campus</strong> as the recipient and for suggestion/complaint pertains to the university level select <strong>Rector</strong> as the recipient.</li>
		<li><strong>BU COVID-19 Policy Guidelines</strong> have been issued based upon <strong>COVID-19 Policy Guidelines received from HEC</strong>.
			<ul>
				<li>
					<aspire:AspireHyperLink runat="server" Target="_blank" NavigateUrl="~/Sys/Faculty/OfficialDocuments/Documents.aspx">HEC COVID-19 Policy Guidance No. 4</aspire:AspireHyperLink></li>
				<li>
					<aspire:AspireHyperLink runat="server" Target="_blank" NavigateUrl="~/Sys/Faculty/OfficialDocuments/Documents.aspx">BU COVID-19 Policy Guidance No. 4</aspire:AspireHyperLink></li>
				<li>
					<aspire:AspireHyperLink runat="server" Target="_blank" NavigateUrl="~/Sys/Faculty/OfficialDocuments/Documents.aspx">BU COVID-19 Policy Guidance No. 5</aspire:AspireHyperLink></li>
				<li>
					<aspire:AspireHyperLink runat="server" Target="_blank" NavigateUrl="~/Sys/Faculty/OfficialDocuments/Documents.aspx">HEC COVID-19 Policy Guidance No. 5</aspire:AspireHyperLink></li>
			</ul>
		</li>
	</ul>
</asp:Content>
