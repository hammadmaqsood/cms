﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.IO;
using System.Linq;

namespace Aspire.Web.Sys.Faculty
{
	public class ExportToExcel : AspireHandler
	{
		public static string GetPageUrl(int offeredCourseID)
		{
			return "~/Sys/Faculty/ExportToExcel.ashx".AttachQueryParam("OfferedCourseID", offeredCourseID);
		}

		protected override void ProcessRequest()
		{
			var offeredCourseID = this.Context.Request.GetParameterValue<int>("OfferedCourseID");
			if (offeredCourseID == null)
				return;
			var registeredStudents = BL.ClassAttendance.Faculty.GetRegisteredStudents(offeredCourseID.Value);

			var title = registeredStudents.FirstOrDefault()?.Title;
			var className = registeredStudents.Select(rc => AspireFormats.GetClassName(rc.ProgramAlias, rc.SemesterNo, rc.Section, rc.Shift)).FirstOrDefault();
			var teacherName = registeredStudents.FirstOrDefault()?.FacultyMemberName;

			var fileName = registeredStudents.Any() ? $"{title} - {className}.xlsx" : "No Data Found.xlsx";
			using (var memoryStream = new MemoryStream())
			using (var document = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook))
			{
				var workbookPart = document.AddWorkbookPart();

				var stylesheet = workbookPart.AddNewPart<WorkbookStylesPart>();
				var fonts = new Fonts(new Font(), new Font(new Bold()));
				var fills = new Fills(new Fill());
				var borders = new Borders(new Border());
				var cellFormats = new CellFormats(new CellFormat { FontId = 0, FillId = 0, BorderId = 0 }, new CellFormat { FontId = 1 });
				stylesheet.Stylesheet = new Stylesheet(fonts, fills, borders, cellFormats);
				stylesheet.Stylesheet.Save();

				workbookPart.Workbook = new Workbook();

				var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
				var sheetData = new SheetData();
				worksheetPart.Worksheet = new Worksheet(sheetData);

				var sheets = workbookPart.Workbook.AppendChild(new Sheets());
				var sheet = new Sheet { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Students" };

				sheets.Append(sheet);

				sheetData.AppendChild(new Row(new Cell
				{
					DataType = CellValues.String,
					StyleIndex = UInt32Value.FromUInt32(1),
					CellValue = new CellValue("Export Date"),
				}, new Cell
				{
					DataType = CellValues.String,
					CellValue = new CellValue(DateTime.Now.ToString("F")),
				}));
				sheetData.AppendChild(new Row(new Cell
				{
					DataType = CellValues.String,
					StyleIndex = UInt32Value.FromUInt32(1),
					CellValue = new CellValue("Title"),
				}, new Cell
				{
					DataType = CellValues.String,
					CellValue = new CellValue(title),
				}));
				sheetData.AppendChild(new Row(new Cell
				{
					DataType = CellValues.String,
					StyleIndex = UInt32Value.FromUInt32(1),
					CellValue = new CellValue("Class"),
				}, new Cell
				{
					DataType = CellValues.String,
					CellValue = new CellValue(className),
				}));
				sheetData.AppendChild(new Row(new Cell
				{
					DataType = CellValues.String,
					StyleIndex = UInt32Value.FromUInt32(1),
					CellValue = new CellValue("Teacher"),
				}, new Cell
				{
					DataType = CellValues.String,
					CellValue = new CellValue(teacherName),
				}));

				var headerRow = new Row();
				headerRow.AppendChild(new Cell
				{
					DataType = CellValues.String,
					StyleIndex = UInt32Value.FromUInt32(1),
					CellValue = new CellValue("#"),
				});
				headerRow.AppendChild(new Cell
				{
					DataType = CellValues.String,
					StyleIndex = UInt32Value.FromUInt32(1),
					CellValue = new CellValue("Enrollment"),
				});
				headerRow.AppendChild(new Cell
				{
					DataType = CellValues.String,
					StyleIndex = UInt32Value.FromUInt32(1),
					CellValue = new CellValue("Registration No."),
				});
				headerRow.AppendChild(new Cell
				{
					DataType = CellValues.String,
					StyleIndex = UInt32Value.FromUInt32(1),
					CellValue = new CellValue("Name"),
				});
				headerRow.AppendChild(new Cell
				{
					DataType = CellValues.String,
					StyleIndex = UInt32Value.FromUInt32(1),
					CellValue = new CellValue("Status"),
				});
				headerRow.AppendChild(new Cell
				{
					DataType = CellValues.String,
					StyleIndex = UInt32Value.FromUInt32(1),
					CellValue = new CellValue("Fee Defaulter"),
				});
				sheetData.AppendChild(headerRow);

				var index = 0;
				foreach (var rc in registeredStudents)
				{
					var row = new Row();
					row.AppendChild(new Cell
					{
						DataType = CellValues.Number,
						CellValue = new CellValue(++index + ""),
					});
					row.AppendChild(new Cell
					{
						DataType = CellValues.String,
						CellValue = new CellValue(rc.Enrollment),
					});
					row.AppendChild(new Cell
					{
						DataType = CellValues.Number,
						CellValue = new CellValue(rc.RegistrationNo + ""),
					});
					row.AppendChild(new Cell
					{
						DataType = CellValues.String,
						CellValue = new CellValue(rc.Name),
					});
					row.AppendChild(new Cell
					{
						DataType = CellValues.String,
						CellValue = new CellValue(AspireFormats.GetRegisteredCourseStatusString(rc.Status, rc.FreezeStatus)),
					});
					row.AppendChild(new Cell
					{
						DataType = CellValues.String,
						CellValue = new CellValue(rc.FeeDefaulter.ToYesNo()),
					});
					sheetData.AppendChild(row);
				}
				workbookPart.Workbook.Save();
				document.Close();

				this.Context.Response.Clear();
				this.Context.Response.ContentType = fileName.ToMimeType();
				this.Context.Response.AddHeader("Content-Disposition", $"attachment; filename=\"{fileName}\"");
				memoryStream.Position = 0;
				this.Context.Response.BinaryWrite(memoryStream.ToArray());
				this.Context.Response.Flush();
				this.Context.Response.Close();
				this.Context.Response.End();
			}
		}

		protected override void Authenticate()
		{
			if (FacultyIdentity.Current == null)
				throw new AspireAccessDeniedException(UserTypes.Faculty, UserTypes.Faculty);
		}
	}
}