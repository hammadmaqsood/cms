using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Faculty
{
	public abstract class FacultyPage : AspireBasePage, IBreadcrumb, ISidebar, IProfileInfo
	{
		protected override void Authenticate()
		{
			if (this.FacultyIdentity == null)
				throw new AspireAccessDeniedException(UserTypes.Faculty, UserTypes.Faculty);
		}

		private IFacultyIdentity _facultyIdentity;
		protected IFacultyIdentity FacultyIdentity => this._facultyIdentity ?? (this._facultyIdentity = Web.Common.FacultyIdentity.Current);

		public override AspireThemes AspireTheme => AspireDefaultThemes.Faculty;

		public virtual bool BreadcrumbVisible => true;

		public override bool PageTitleVisible => true;

		public virtual bool SidebarVisible => true;

		public virtual string SidebarHtml => SessionHelper.SideBarLinkHtml;

		public virtual string BreadcrumbHtml => SessionHelper.SideBarLinks?.GetBreadcrumbHtml(this.AspirePageAttribute.AspirePageGuid);

		public virtual bool ProfileInfoVisible => true;

		public virtual bool ProfileLinkVisible => false;

		public bool IsCustomProfileImage => false;

		public string CustomProfileImageUrl => null;

		public virtual string Username => this.FacultyIdentity.Username;

		public string Name => this.FacultyIdentity.Name;

		public string Email => this.FacultyIdentity.Email;

		public virtual string ProfileNavigateUrl => null;

		public virtual bool LogoffLinkVisible => true;

		public virtual string LogoffNavigateUrl => Logoff.GetPageUrl(UserLoginHistory.LogOffReasons.LogOff);
	}
}