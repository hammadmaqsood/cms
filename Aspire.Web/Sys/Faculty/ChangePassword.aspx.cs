﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Faculty
{
	[AspirePage("D80BA639-A9EA-4B5B-8564-ED04FC17A582", UserTypes.Faculty, "~/Sys/Faculty/ChangePassword.aspx", "Change Password", true, AspireModules.None)]
	public partial class ChangePassword : FacultyPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_lock);
		public override string PageTitle => "Change Password";

		protected void Page_Load(object sender, EventArgs e)
		{

		}
	}
}