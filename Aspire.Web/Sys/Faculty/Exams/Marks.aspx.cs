﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Faculty.Exams
{
	[AspirePage("5618443A-B891-4698-82FC-CEE5A1DE7B3E", UserTypes.Faculty, "~/Sys/Faculty/Exams/Marks.aspx", "Marks", true, AspireModules.Exams)]
	public partial class Marks : FacultyPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => this.ExamType?.ToFullName();

		private OfferedCourseExam.ExamTypes? ExamType => this.Request.GetParameterValue<OfferedCourseExam.ExamTypes>(nameof(this.ExamType));

		public static string GetPageUrl(int offeredCourseID, OfferedCourseExam.ExamTypes examType, int? offeredCourseExamID)
		{
			return GetPageUrl<Marks>().AttachQueryParams("OfferedCourseExamID", offeredCourseExamID, "OfferedCourseID", offeredCourseID, nameof(ExamType), examType);
		}

		public static void Redirect(int offeredCourseID, OfferedCourseExam.ExamTypes examType, int? offeredCourseExamID)
		{
			Redirect(GetPageUrl(offeredCourseID, examType, offeredCourseExamID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
		}
	}
}