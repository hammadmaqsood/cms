﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Faculty.Exams
{
	[AspirePage("5E090294-F4BC-421B-9FB9-9B3C7B8F4CC8", UserTypes.Faculty, "~/Sys/Faculty/Exams/ExamMarks.aspx", "ExamMarks", true, AspireModules.Exams)]
	public partial class ExamMarks : FacultyPage
	{
		public override PageIcon PageIcon => AspireGlyphicons.pencil.GetIcon();
		public override string PageTitle => this.ExamMarksType?.ToFullName();

		private ExamMarksTypes? ExamMarksType => this.Request.GetParameterValue<ExamMarksTypes>(nameof(this.ExamMarksType));

		public static string GetPageUrl(int offeredCourseID, ExamMarksTypes examMarksType)
		{
			return GetPageUrl<ExamMarks>().AttachQueryParams("OfferedCourseID", offeredCourseID, nameof(ExamMarksType), examMarksType);
		}

		public static void Redirect(int offeredCourseID, ExamMarksTypes examMarksType)
		{
			Redirect(GetPageUrl(offeredCourseID, examMarksType));
		}
	}
}