﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Faculty.Exams
{
	[AspirePage("E1AEEEB1-E321-4AD2-BE79-F55FFB151B02", UserTypes.Faculty, "~/Sys/Faculty/Exams/MarksSheet.aspx", "Marks Sheet", true, AspireModules.Exams)]
	public partial class MarksSheet : FacultyPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.list);
		public override string PageTitle => "Marks Sheet";

		public static string GetPageUrl(int offeredCourseID)
		{
			return GetPageUrl<MarksSheet>().AttachQueryParam("OfferedCourseID", offeredCourseID);
		}

		public static void Redirect(int offeredCourseID)
		{
			Redirect(GetPageUrl(offeredCourseID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
		}
	}
}