﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Linq;

namespace Aspire.Web.Sys.Faculty.Exams
{
	[AspirePage("4F0AA2A9-944D-4129-A3D6-B28AAACD0A47", UserTypes.Staff, "~/Sys/Faculty/Exams/ExamEnvelopesSlip.aspx", "Exam Envelopes Slip", true, AspireModules.ExamSeatingPlan)]
	public partial class ExamEnvelopesSlip : FacultyPage, IReportViewer
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.envelope);
		public override string PageTitle => "Exam Envelopes Slip";
		public string ReportName => "Exam Envelopes Slip";
		public bool ExportToExcel => false;
		public bool ExportToWord => false;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (!this.IsPostBack)
				{
					var examConducts = BL.Core.ExamSeatingPlan.Faculty.ExamEnvelopesSlip.GetExamConducts(this.FacultyIdentity.LoginSessionGuid);
					if (!examConducts.Any())
					{
						this.AddNoRecordFoundAlert();
						Redirect<Dashboard>();
						return;
					}
					this.ddlExamConductID.DataBind(examConducts);
					this.ddlExamConductID_SelectedIndexChanged1(null, null);
				}
			}
		}

		private bool _execute = false;

		protected void ddlExamConductID_SelectedIndexChanged1(object sender, EventArgs e)
		{
			this._execute = true;
			var reportView = (ReportView)this.Master;
			this.RenderReport(reportView.ReportViewer);
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			if (!this._execute)
				return;
			this._execute = false;
			reportViewer.LocalReport.DataSources.Clear();
			var examConductID = this.ddlExamConductID.SelectedValue.ToNullableInt();
			if (examConductID == null)
				return;
			var reportFileName = "~/Reports/ExamSeatingPlan/Staff/TeacherCoursesSlip.rdlc".MapPath();
			var reportDataSet = BL.Core.ExamSeatingPlan.Faculty.ExamEnvelopesSlip.GetExamEnvelopesSlips(examConductID.Value, this.FacultyIdentity.LoginSessionGuid);
			if (!reportDataSet.CoursesSlipDetails.Any())
			{
				this.AddNoRecordFoundAlert();
				reportViewer.Reset();
				return;
			}
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.LocalReport.ReportPath = reportFileName;
			reportViewer.LocalReport.DisplayName = this.ReportName;
			reportViewer.DocumentMapCollapsed = true;
			reportViewer.ShowDocumentMapButton = false;
			reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.CoursesSlipDetails), reportDataSet.CoursesSlipDetails));
			reportViewer.LocalReport.EnableExternalImages = true;
		}
	}
}