﻿<%@ Page Language="C#" MasterPageFile="~/ReportView.Master" AutoEventWireup="true" CodeBehind="ExamEnvelopesSlip.aspx.cs" Inherits="Aspire.Web.Sys.Faculty.Exams.ExamEnvelopesSlip" %>

<asp:Content runat="server" ContentPlaceHolderID="Parameters">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Exam:" AssociatedControlID="ddlExamConductID" />
				<aspire:AspireDropDownList runat="server" ID="ddlExamConductID" AutoPostBack="true" ValidationGroup="Parameters" OnSelectedIndexChanged="ddlExamConductID_SelectedIndexChanged1" />
			</div>
		</div>
	</div>
</asp:Content>

