﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Marks.aspx.cs" Inherits="Aspire.Web.Sys.Faculty.Exams.Marks" %>

<%@ Register Src="~/Sys/Common/Exam/Marks.ascx" TagPrefix="uc1" TagName="Marks" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc1:Marks runat="server" id="ucMarks" />
</asp:Content>
