﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Faculty.Exams
{
	[AspirePage("d6230431-4765-41d4-ac45-771e8ad6b12d", UserTypes.Faculty, "~/Sys/Faculty/Exams/MarksSheetChangeDetails.aspx", "Marks Sheet Change Details", true, AspireModules.Exams)]
	public partial class MarksSheetChangeDetails : FacultyPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_history);
		public override string PageTitle => "Marks Sheet Change Details";

		public static string GetPageUrl(int offeredCourseID, Guid? leftMarksSheetChangeID, Guid rightMarksSheetChangeID)
		{
			return GetPageUrl<MarksSheetChangeDetails>().AttachQueryParams("LeftMarksSheetChangeID", leftMarksSheetChangeID, "RightMarksSheetChangeID", rightMarksSheetChangeID, "OfferedCourseID", offeredCourseID);
		}

		public static void Redirect(int offeredCourseID, Guid? leftMarksSheetChangeID, Guid rightMarksSheetChangeID)
		{
			Redirect(GetPageUrl(offeredCourseID, leftMarksSheetChangeID, rightMarksSheetChangeID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
		}
	}
}