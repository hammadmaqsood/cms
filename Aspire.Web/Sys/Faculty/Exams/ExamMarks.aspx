﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ExamMarks.aspx.cs" Inherits="Aspire.Web.Sys.Faculty.Exams.ExamMarks" %>

<%@ Register Src="~/Sys/Common/Exam/ExamMarks.ascx" TagPrefix="uc1" TagName="ExamMarks" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<uc1:ExamMarks runat="server" id="ucExamMarks" />
</asp:Content>
