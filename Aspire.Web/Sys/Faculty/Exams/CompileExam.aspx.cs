﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Faculty.Exams
{
	[AspirePage("54656DBC-8012-4F97-807F-2CF41C66DBFB", UserTypes.Faculty, "~/Sys/Faculty/Exams/CompileExam.aspx", "Compile Marks", true, AspireModules.Exams)]
	public partial class CompileExam : FacultyPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_cogs);
		private int? OfferedCourseID => this.Request.GetParameterValue<int>(nameof(this.OfferedCourseID));
		private CompileExamMarksTypes? CompileExamMarksType => this.Request.GetParameterValue<CompileExamMarksTypes>(nameof(this.CompileExamMarksType));

		public override string PageTitle
		{
			get
			{
				switch (this.CompileExamMarksType)
				{
					case CompileExamMarksTypes.Assignments:
						return "Compile Assignments";
					case CompileExamMarksTypes.Quizzes:
						return "Compile Quizzes";
					case CompileExamMarksTypes.Internals:
						return "Compile Internals";
					case null:
						return null;
					default:
						throw new NotImplementedEnumException(this.CompileExamMarksType);
				}
			}
		}

		public static string GetPageUrl(int offeredCourseID, CompileExamMarksTypes compileExamMarksType)
		{
			return GetPageUrl<CompileExam>().AttachQueryParams(nameof(OfferedCourseID), offeredCourseID, nameof(CompileExamMarksType), compileExamMarksType);
		}

		public static void Redirect(int offeredCourseID, CompileExamMarksTypes compileExamMarksType)
		{
			Redirect(GetPageUrl(offeredCourseID, compileExamMarksType));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (this.OfferedCourseID == null || this.CompileExamMarksType == null)
			{
				Redirect<Dashboard>();
				return;
			}
		}
	}
}