﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;

namespace Aspire.Web.Sys.Faculty.Exams.Reports
{
	[AspirePage("A4A09C87-7180-407A-AFBD-FFFB7FF0CD1C", UserTypes.Faculty, "~/Sys/Faculty/Exams/Reports/AwardList.aspx", "Award List", true, AspireModules.Exams)]
	public partial class AwardList : FacultyPage, IReportViewer
	{
		public override PageIcon PageIcon => AspireGlyphicons.print.GetIcon();
		public override string PageTitle => "Award List";

		public string ReportName => "Award List";
		public bool ExportToExcel => true;
		public bool ExportToWord => false;

		private int? OfferedCourseID => this.Request.GetParameterValue<int>(nameof(this.OfferedCourseID));

		public static string GetPageUrl(int offeredCourseID, bool? inline, ReportFormats? reportFormat)
		{
			return GetPageUrl<AwardList>().AttachQueryParams(nameof(OfferedCourseID), offeredCourseID, ReportView.ReportFormat, reportFormat, ReportView.Inline, inline);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.OfferedCourseID == null)
				{
					Redirect<Courses>();
					return;
				}
			}
		}

		public void RenderReport(ReportViewer reportViewer)
		{
			Common.Exam.CommonMethods.RenderAwardListReport<Courses>(reportViewer, this.OfferedCourseID.Value);
		}
	}
}