﻿using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Faculty
{
	[AspirePage("4F577EC6-8AAA-44D7-B81B-ACAA9DD06F13", UserTypes.Faculty, "~/Sys/Faculty/PrepareSession.aspx", null, false, AspireModules.None)]
	public partial class PrepareSession : BasePage
	{
		protected override void Authenticate()
		{
			if (FacultyIdentity.Current == null)
				throw new AspireAccessDeniedException(UserTypes.Faculty, UserTypes.Faculty);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SessionHelper.SideBarLinkHtml == null)
			{
				PrepareMenu();
			}
			Redirect(GetPageUrl<Dashboard>());
		}

		private void PrepareMenu()
		{
			var sideBarLinks = DefaultMenus.Faculty.Links;
			SessionHelper.SetSideBarLinks(sideBarLinks);
		}
	}
}