﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Admin
{
	[AspirePage("20cdd2db-5b89-4521-960d-dca5d2d5dc0c", UserTypes.Staff, "~/Sys/Admin/ChangePassword.aspx", "Change Password", true, AspireModules.None)]
	public partial class ChangePassword : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => null;
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.lock_);
		public override string PageTitle => "Change Password";

		protected void Page_Load(object sender, EventArgs e)
		{
		}
	}
}