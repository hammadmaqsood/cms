﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.UserManagement
{
	[AspirePage("DCA8AD1C-8129-4AA5-B88F-72190E4F25BC", UserTypes.Admins, "~/Sys/Admin/UserManagement/User.aspx", "User", true, AspireModules.UserManagement)]
	public partial class User : AdminsPage
	{
		public override PageIcon PageIcon => (this.UserID == null ? FontAwesomeIcons.solid_user_plus : FontAwesomeIcons.solid_user).GetIcon();
		public override string PageTitle => "User";

		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.UserManagement.Module, UserGroupPermission.Allowed}
		};

		public static string GetPageUrl(int? userID = null)
		{
			return GetPageUrl<User>().AttachQueryParam("UserID", userID);
		}

		public static void Redirect(int? userID = null)
		{
			Redirect(GetPageUrl(userID));
		}

		private int? UserID => this.Request.GetParameterValue<int>("UserID");

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlStatus.FillEnums<Model.Entities.User.Statuses>();

				if (this.UserID != null)
				{
					var user = BL.Core.UserManagement.Admin.Users.GetUser(this.UserID.Value, this.AdminIdentity.LoginSessionGuid);
					if (user == null)
					{
						this.AddNoRecordFoundAlert();
						Users.Redirect();
						return;
					}

					this.tbUsername.Text = user.Username;
					this.tbName.Text = user.Name;
					this.tbMobile.Text = user.Mobile;
					this.tbEmail.Text = user.Email;
					this.tbJobTitle.Text = user.JobTitle;
					this.ddlStatus.SetEnumValue(user.StatusEnum);

					var roles = user.Roles.OrderBy(r => r.UserType).ThenBy(r => r.InstituteAlias).Select(r => new
					{
						UserTypeEnum = r.UserTypeEnum.ToFullName(),
						InstituteAlias = r.InstituteAlias.ToNAIfNullOrEmpty(),
						GroupName = r.GroupName.ToNAIfNullOrEmpty(),
						GroupID = r.GroupID,
						RoleID = r.UserRoleID,
						r.UserType,
						r.ExpiryDate,
						r.AssignedByUser,
						r.CreatedDate,
						r.StatusFullName,
					}).ToList();
					this.gvAdminRoles.DataBind(roles);
					this.ddlStatus.Enabled = user.Roles.Any();

					var userTypesForRoleModules = user.Roles.Where(r => r.UserTypeEnum.IsStaff() || r.UserTypeEnum.IsExecutive()).OrderBy(r => r.UserType).Select(r => new ListItem
					{
						Text = $"{r.UserTypeEnum.ToFullName()} @ {r.InstituteAlias}",
						Value = $"{r.UserRoleID}:{r.UserType}:{r.InstituteID}",
					}).ToArray();
					this.ddlUserRoleIDRoleModule.DataBind(userTypesForRoleModules);
					this.btnAddRoleModule.Enabled = this.ddlUserRoleIDRoleModule.Items.Count > 0;

					this.gvAdminRoles.Columns[this.gvAdminRoles.Columns.Count - 1].Visible = user.CanModify;
					this.btnSave.Visible = user.CanModify;
					this.btnAddRole.Visible = user.CanModify;
					if (user.Roles.Any(r => r.UserType == UserTypeConstants.Staff || r.UserType == UserTypeConstants.Executive))
					{
						var roleModules = user.RoleModules.Select(r => new
						{
							Role = r.UserTypeFullName,
							r.UserRoleModuleID,
							r.AspireModuleFullName,
							r.Institute,
							Department = r.Department ?? "All",
							Program = r.Program ?? "All",
							IntakeSemester = r.IntakeSemester ?? "All",
						}).OrderBy(r => r.AspireModuleFullName).ThenBy(r => r.Department).ThenBy(r => r.Program).ThenBy(r => r.IntakeSemester).ToList();
						this.gvRoleModules.DataBind(roleModules);
						this.divRoleModules.Visible = true;
					}
					else
						this.divRoleModules.Visible = false;

					if (!user.AccountActivatedByUser)
					{
						this.divAccountActivatedByUser.Visible = true;
						this.lblAccountActivatedByUser.Text = @"Not yet activated by user.";
						this.lbtnSendConfirmationEmail.Visible = user.CanModify;
						if (user.ConfirmationCodeExpiryDate != null)
						{
							if (user.ConfirmationCodeExpiryDate < DateTime.Now)
								this.lbtnSendConfirmationEmail.Text = $"Send Confirmation Email Again (Link Expired)";
							else
								this.lbtnSendConfirmationEmail.Text = $"Send Confirmation Email Again (Expiry: {user.ConfirmationCodeExpiryDate.Value:d} {user.ConfirmationCodeExpiryDate.Value:t})";
						}
						else
							this.lbtnSendConfirmationEmail.Text = "Send Confirmation Email";
					}
					else
						this.divAccountActivatedByUser.Visible = false;

					if (user.AccountLocked)
					{
						this.divAccountLocked.Visible = true;
						this.lblAccountLocked.Text = @"Account locked due to multiple failed attempts.";
						this.lbtnActivateAccount.Visible = user.CanModify;
					}
					else
						this.divAccountLocked.Visible = false;
					if (!user.CanModify)
					{
						this.tbUsername.ReadOnly = true;
						this.tbName.ReadOnly = true;
						this.tbEmail.ReadOnly = true;
						this.tbMobile.ReadOnly = true;
						this.tbJobTitle.ReadOnly = true;
						this.ddlStatus.Enabled = false;
					}
					if (this.ddlStatus.Enabled && this.UserID == this.AdminIdentity.UserID)
						this.ddlStatus.Enabled = false;
					this.lbtnResetPassword.Visible = user.AccountActivatedByUser;
				}
				else
				{
					this.AdminIdentity.Demand(AdminPermissions.UserManagement.ManageUsers, UserGroupPermission.PermissionValues.Allowed);
					this.divAccountActivatedByUser.Visible = false;
					this.divAccountLocked.Visible = false;
					this.divRoles.Visible = false;
					this.divRoleModules.Visible = false;
					this.lbtnResetPassword.Visible = false;
					this.ddlRoleStatus.SetEnumValue(Model.Entities.User.Statuses.Inactive).Enabled = false;
				}
			}
		}

		protected void lbtnSendConfirmationEmail_Click(object sender, EventArgs e)
		{
			this.AdminIdentity.Demand(AdminPermissions.UserManagement.ManageUsers, UserGroupPermission.PermissionValues.Allowed);
			if (this.UserID == null)
				return;
			var pageUrl = GetPageUrl<Logins.User.ResetPassword>().ToAbsoluteUrl();
			var result = BL.Core.UserManagement.Admin.Users.SendAccountActivationEmailToUser(this.UserID.Value, pageUrl, this.AdminIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.UserManagement.Admin.Users.SendAccountActivationEmailToUserResult.NoRecordFound:
					this.AddNoRecordFoundAlert();
					break;
				case BL.Core.UserManagement.Admin.Users.SendAccountActivationEmailToUserResult.UserStatusIsNotActive:
					this.AddErrorAlert("User Status is Inactive.");
					break;
				case BL.Core.UserManagement.Admin.Users.SendAccountActivationEmailToUserResult.NotAuthorized:
					this.AddErrorMessageYouAreNotAuthorizedToPerformThisAction();
					break;
				case BL.Core.UserManagement.Admin.Users.SendAccountActivationEmailToUserResult.Success:
					this.AddSuccessAlert("Confirmation Email has been sent.");
					break;
				default:
					throw new NotImplementedEnumException(result);
			}
			Redirect(this.UserID);
		}

		protected void lbtnActivateAccount_Click(object sender, EventArgs e)
		{
			this.AdminIdentity.Demand(AdminPermissions.UserManagement.ManageUsers, UserGroupPermission.PermissionValues.Allowed);
			throw new NotImplementedException();
		}

		protected void btnSave_Click(object sender, EventArgs e)
		{
			this.AdminIdentity.Demand(AdminPermissions.UserManagement.ManageUsers, UserGroupPermission.PermissionValues.Allowed);
			if (!this.IsValid)
				return;

			if (this.UserID == null)
			{
				var result = BL.Core.UserManagement.Admin.Users.AddUser(this.tbUsername.Text, this.tbName.Text, this.tbEmail.Text, this.tbJobTitle.Text, this.tbMobile.Text, this.AdminIdentity.LoginSessionGuid);
				if (result == null)
					throw new InvalidOperationException();
				switch (result.Status)
				{
					case BL.Core.UserManagement.Admin.Users.AddUserResult.Statuses.UserNameAlreadyExists:
						this.AddErrorAlert("Username already exists.");
						break;
					case BL.Core.UserManagement.Admin.Users.AddUserResult.Statuses.Success:
						this.AddSuccessMessageHasBeenAdded("User");
						Redirect(result.UserID);
						break;
					case BL.Core.UserManagement.Admin.Users.AddUserResult.Statuses.EmailAlreadyExists:
						this.AddErrorAlert("Email already exists.");
						break;
					default:
						throw new NotImplementedEnumException(result.Status);
				}
			}
			else
			{
				var result = BL.Core.UserManagement.Admin.Users.UpdateUser(this.UserID.Value, this.tbUsername.Text, this.tbName.Text, this.tbEmail.Text, this.tbJobTitle.Text, this.tbMobile.Text, this.ddlStatus.GetSelectedEnumValue<Model.Entities.User.Statuses>(), this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.UserManagement.Admin.Users.UpdateUserStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Users.Redirect();
						break;
					case BL.Core.UserManagement.Admin.Users.UpdateUserStatuses.UserNameAlreadyExists:
						this.AddErrorAlert("Username already exists.");
						break;
					case BL.Core.UserManagement.Admin.Users.UpdateUserStatuses.EmailAlreadyExists:
						this.AddErrorAlert("Email already exists.");
						break;
					case BL.Core.UserManagement.Admin.Users.UpdateUserStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("User");
						Redirect(this.UserID.Value);
						break;
					case BL.Core.UserManagement.Admin.Users.UpdateUserStatuses.OnlyMasterAdminCanEdit:
						this.AddWarningAlert("Only Master Admin user can edit this user account.");
						this.btnSave.Enabled = false;
						break;
					case BL.Core.UserManagement.Admin.Users.UpdateUserStatuses.CannotInactiveYourAccount:
						this.AddWarningAlert("You cannot inactive your account.");
						break;
					default:
						throw new NotImplementedEnumException(result);
				}
			}
		}

		protected void gvAdminRoles_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					this.AdminIdentity.Demand(AdminPermissions.UserManagement.ManageUsers, UserGroupPermission.PermissionValues.Allowed);
					e.Handled = true;
					var commandArgument = e.CommandArgument.ToString().Decrypt().Split(',');
					var roleID = commandArgument[0].ToInt();
					var userType = commandArgument[1].ToEnum<UserTypes>();
					var deleteResult = BL.Core.UserManagement.Admin.Users.DeleteUserRole(roleID, this.AdminIdentity.LoginSessionGuid);
					switch (deleteResult)
					{
						case BL.Core.UserManagement.Admin.Users.DeleteUserRoleStatus.NoRecordFound:
							this.AddNoRecordFoundAlert();
							Redirect(this.UserID);
							break;
						case BL.Core.UserManagement.Admin.Users.DeleteUserRoleStatus.Success:
							this.AddSuccessMessageHasBeenDeleted("User Role");
							Redirect(this.UserID);
							break;
						case BL.Core.UserManagement.Admin.Users.DeleteUserRoleStatus.CannotDeleteYourself:
							this.AddErrorAlert("You cannot delete yourself.");
							Redirect(this.UserID);
							break;
						case BL.Core.UserManagement.Admin.Users.DeleteUserRoleStatus.NotAuthorizedToDeleteTheSelectedRole:
							this.AddErrorAlert("You are not authorized to delete the selected user role.");
							Redirect(this.UserID);
							break;
						default:
							throw new NotImplementedEnumException(deleteResult);
					}
					break;
				case "Edit":
					e.Handled = true;
					commandArgument = e.DecryptedCommandArgument().Split(',');
					roleID = commandArgument[0].ToInt();
					userType = commandArgument[1].ToEnum<UserTypes>();
					this.DisplayRoleDialog(roleID, userType);
					break;
			}
		}

		protected void gvAdminRoles_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow)
				return;
			var expiryDate = (DateTime)DataBinder.Eval(e.Row.DataItem, "ExpiryDate");
			if (expiryDate.Date <= DateTime.Today)
				e.Row.CssClass += "danger";
		}

		protected void ddlUserType_SelectedIndexChanged(object sender, EventArgs e)
		{
			var userTypeEnum = this.ddlUserType.GetSelectedEnumValue<UserTypes>();
			this.ddlRoleStatus.FillEnums<UserRole.Statuses>();
			switch (userTypeEnum)
			{
				case UserTypes.MasterAdmin:
					this.ddlInstituteID.Items.Clear();
					this.cblInstituteIDs.Items.Clear();
					this.ddlUserGroupID.Items.Clear();
					this.divcblInstituteIDs.Visible = false;
					this.divddlInstituteID.Visible = false;
					this.divGroup.Visible = false;
					break;
				case UserTypes.Admin:
					this.ddlInstituteID.Items.Clear();
					this.cblInstituteIDs.FillInstitutesForAdmin();
					var userGroups = BL.Core.UserManagement.Admin.UserGroups.GetUserGroupsList(userTypeEnum, null, this.AdminIdentity.LoginSessionGuid);
					this.ddlUserGroupID.DataBind(userGroups);
					this.divcblInstituteIDs.Visible = true;
					this.divddlInstituteID.Visible = false;
					this.divGroup.Visible = true;
					break;
				case UserTypes.Staff:
					this.ddlInstituteID.FillInstitutesForAdmin().Enabled = true;
					this.cblInstituteIDs.Items.Clear();
					this.ddlUserGroupID.Items.Clear();
					this.divcblInstituteIDs.Visible = false;
					this.divddlInstituteID.Visible = true;
					this.divGroup.Visible = true;
					this.ddlInstituteID_SelectedIndexChanged(null, null);
					break;
				case UserTypes.Executive:
					this.ddlInstituteID.FillInstitutesForAdmin().Enabled = true;
					this.cblInstituteIDs.Items.Clear();
					this.ddlUserGroupID.Items.Clear();
					this.divcblInstituteIDs.Visible = false;
					this.divddlInstituteID.Visible = true;
					this.divGroup.Visible = true;
					this.ddlInstituteID_SelectedIndexChanged(null, null);
					break;
				default:
					throw new NotImplementedEnumException(userTypeEnum);
			}
		}

		protected void btnAddRole_Click(object sender, EventArgs e)
		{
			this.DisplayRoleDialog(null, null);
		}

		private void DisplayRoleDialog(int? roleID, UserTypes? userTypeEnum)
		{
			if (this.UserID == null)
				return;
			this.AdminIdentity.Demand(AdminPermissions.UserManagement.ManageUsers, UserGroupPermission.PermissionValues.Allowed);
			this.ViewState["UserRoleID"] = null;
			this.ViewState["UserType"] = null;

			var loginUserIsMasterAdmin = this.AdminIdentity.UserType.IsMasterAdmin();
			this.ddlUserType.Items.Clear();
			if (loginUserIsMasterAdmin)
				this.ddlUserType.AddEnums(UserTypes.MasterAdmin, UserTypes.Admin, UserTypes.Staff, UserTypes.Executive);
			else
				this.ddlUserType.AddEnums(UserTypes.Staff);
			this.ddlUserType.SelectedIndex = 0;
			this.dtpExpiryDate.MaxDate = DateTime.Today.AddMonths(6);
			this.dtpExpiryDate.MinDate = DateTime.Today;
			this.dtvdtpExpiryDate.MinDate = this.dtpExpiryDate.MinDate;
			this.dtvdtpExpiryDate.MaxDate = this.dtpExpiryDate.MaxDate;
			if (roleID == null)
			{
				this.modalRole.HeaderText = "Add Role";
				this.ddlUserType.Enabled = true;
				this.ddlUserType_SelectedIndexChanged(null, null);
				this.dtpExpiryDate.SelectedDate = null;
				this.ddlRoleStatus.Enabled = true;
				this.ddlRoleStatus.ClearSelection();
			}
			else
			{
				if (userTypeEnum == null)
					throw new ArgumentNullException(nameof(userTypeEnum));
				this.modalRole.HeaderText = "Edit Role";
				this.ddlUserType.Enabled = false;
				switch (userTypeEnum.Value)
				{
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
						if (!loginUserIsMasterAdmin)
						{
							this.AddErrorAlert("You are not authorized to edit the selected role.");
							Redirect(this.UserID);
							return;
						}
						var adminRole = BL.Core.UserManagement.Admin.Users.GetUserAdminRole(roleID.Value, this.AdminIdentity.LoginSessionGuid);
						if (adminRole == null)
						{
							this.AddNoRecordFoundAlert();
							Redirect(this.UserID);
							return;
						}
						this.ddlUserType.SetEnumValue(userTypeEnum.Value);
						this.ddlUserType_SelectedIndexChanged(null, null);
						this.cblInstituteIDs.ClearSelection();
						foreach (var userAdminRoleInstitute in adminRole.UserRoleInstitutes)
							this.cblInstituteIDs.Items.FindByValue(userAdminRoleInstitute.InstituteID.ToString()).Selected = true;

						if (!userTypeEnum.Value.IsMasterAdmin())
							this.ddlUserGroupID.SelectedValue = adminRole.UserGroupID.ToString();
						else
							this.ddlUserGroupID.ClearSelection();
						this.dtpExpiryDate.SelectedDate = adminRole.ExpiryDate;
						this.ddlRoleStatus.SetEnumValue(adminRole.StatusEnum);
						if (roleID == this.AdminIdentity.UserRoleID)
						{
							this.ddlRoleStatus.Enabled = false;
							this.dtpExpiryDate.MinDate = DateTime.Today.AddDays(1);
						}
						this.ViewState["UserRoleID"] = adminRole.UserRoleID;
						this.ViewState["UserType"] = userTypeEnum.Value;
						break;
					case UserTypes.Staff:
					case UserTypes.Executive:
						var userRole = BL.Core.UserManagement.Admin.Users.GetUserRole(roleID.Value, userTypeEnum.Value, this.AdminIdentity.LoginSessionGuid);
						if (userRole == null)
						{
							this.AddNoRecordFoundAlert();
							Redirect(this.UserID);
							return;
						}
						this.ddlUserType.SetEnumValue(userTypeEnum.Value);
						this.ddlUserType_SelectedIndexChanged(null, null);
						this.ddlInstituteID.SelectedValue = userRole.InstituteID.ToString();
						this.ddlInstituteID.Enabled = false;
						this.ddlInstituteID_SelectedIndexChanged(null, null);
						this.ddlUserGroupID.SelectedValue = userRole.UserGroupID.ToString();
						this.dtpExpiryDate.SelectedDate = userRole.ExpiryDate;
						this.ddlRoleStatus.SetEnumValue(userRole.StatusEnum);
						this.ViewState["UserRoleID"] = userRole.UserRoleID;
						this.ViewState["UserType"] = userTypeEnum.Value;
						break;
					default:
						throw new NotImplementedEnumException(userTypeEnum);
				}
			}
			this.modalRole.Show();
		}

		protected void ddlInstituteID_SelectedIndexChanged(object sender, EventArgs e)
		{
			var userTypeEnum = this.ddlUserType.GetSelectedEnumValue<UserTypes>();
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableByte();
			if (instituteID != null)
			{
				var userGroups = BL.Core.UserManagement.Admin.UserGroups.GetUserGroupsList(userTypeEnum, instituteID.Value, this.AdminIdentity.LoginSessionGuid);
				this.ddlUserGroupID.DataBind(userGroups);
			}
			else
				this.ddlUserGroupID.Items.Clear();
		}

		protected void btnRoleSave_Click(object sender, EventArgs e)
		{
			if (!this.IsValid || this.UserID == null)
				return;
			var roleID = (int?)this.ViewState["UserRoleID"];

			var userGroupID = this.ddlUserGroupID.SelectedValue.ToNullableInt();
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			var expiryDate = this.dtpExpiryDate.SelectedDate.Value;
			var statusEnum = this.ddlRoleStatus.GetSelectedEnumValue<UserRole.Statuses>();

			if (roleID == null)
			{
				BL.Core.UserManagement.Admin.Users.AddUserRoleStatuses addUserRoleStatuses;
				var userTypeEnum = this.ddlUserType.GetSelectedEnumValue<UserTypes>();
				switch (userTypeEnum)
				{
					case UserTypes.MasterAdmin:
						addUserRoleStatuses = BL.Core.UserManagement.Admin.Users.AddUserMasterAdminRole(this.UserID.Value, expiryDate, statusEnum, this.AdminIdentity.LoginSessionGuid);
						break;
					case UserTypes.Admin:
						var instituteIDs = this.cblInstituteIDs.GetSelectedValues().ToInt().ToList();
						addUserRoleStatuses = BL.Core.UserManagement.Admin.Users.AddUserAdminRole(this.UserID.Value, userGroupID.Value, instituteIDs, expiryDate, statusEnum, this.AdminIdentity.LoginSessionGuid);
						break;
					case UserTypes.Staff:
					case UserTypes.Executive:
						addUserRoleStatuses = BL.Core.UserManagement.Admin.Users.AddUserRole(userTypeEnum, this.UserID.Value, userGroupID.Value, instituteID.Value, expiryDate, statusEnum, this.AdminIdentity.LoginSessionGuid);
						break;
					default:
						throw new NotImplementedEnumException(userTypeEnum);
				}
				switch (addUserRoleStatuses)
				{
					case BL.Core.UserManagement.Admin.Users.AddUserRoleStatuses.RoleAlreadyExists:
						this.AddErrorAlert("User Role already exists.");
						Redirect(this.UserID);
						break;
					case BL.Core.UserManagement.Admin.Users.AddUserRoleStatuses.MasterAdminRoleCannotBeAddedWhenAdminRolesExists:
						this.AddErrorAlert("Master Admin role cannot be added when Admin role has been assigned to the user.");
						Redirect(this.UserID);
						break;
					case BL.Core.UserManagement.Admin.Users.AddUserRoleStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect(this.UserID);
						break;
					case BL.Core.UserManagement.Admin.Users.AddUserRoleStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("User Role");
						Redirect(this.UserID);
						break;
					case BL.Core.UserManagement.Admin.Users.AddUserRoleStatuses.AdminRoleCannotBeAddedWhenMasterAdminRolesExists:
						this.AddErrorAlert("Admin role cannot be added when Master Admin role has been assigned to the user.");
						Redirect(this.UserID);
						break;
					default:
						throw new NotImplementedEnumException(addUserRoleStatuses);
				}
			}
			else
			{
				var userTypeEnum = (UserTypes)this.ViewState["UserType"];
				BL.Core.UserManagement.Admin.Users.UpdateUserRoleStatuses updateUserRoleStatuses;
				switch (userTypeEnum)
				{
					case UserTypes.MasterAdmin:
						updateUserRoleStatuses = BL.Core.UserManagement.Admin.Users.UpdateUserMasterAdminRole(roleID.Value, expiryDate, statusEnum, this.AdminIdentity.LoginSessionGuid);
						break;
					case UserTypes.Admin:
						var instituteIDs = this.cblInstituteIDs.GetSelectedValues().ToInt().ToList();
						updateUserRoleStatuses = BL.Core.UserManagement.Admin.Users.UpdateUserAdminRole(roleID.Value, userGroupID.Value, instituteIDs, expiryDate, statusEnum, this.AdminIdentity.LoginSessionGuid);
						break;
					case UserTypes.Staff:
					case UserTypes.Executive:
						updateUserRoleStatuses = BL.Core.UserManagement.Admin.Users.UpdateUserRole(userTypeEnum, roleID.Value, userGroupID.Value, expiryDate, statusEnum, this.AdminIdentity.LoginSessionGuid);
						break;
					default:
						throw new NotImplementedEnumException(userTypeEnum);
				}
				switch (updateUserRoleStatuses)
				{
					case BL.Core.UserManagement.Admin.Users.UpdateUserRoleStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect(this.UserID);
						break;
					case BL.Core.UserManagement.Admin.Users.UpdateUserRoleStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("User Role");
						Redirect(this.UserID);
						break;
					default:
						throw new NotImplementedEnumException(updateUserRoleStatuses);
				}
			}
		}

		#region User Staff Role Modules

		protected void gvRoleModules_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					var userRolesModuleID = e.DecryptedCommandArgumentToInt();
					var deleteStatus = BL.Core.UserManagement.Admin.Users.DeleteUserRoleModule(userRolesModuleID, this.AdminIdentity.LoginSessionGuid);
					switch (deleteStatus)
					{
						case BL.Core.UserManagement.Admin.Users.DeleteUserStaffRoleModuleStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.UserManagement.Admin.Users.DeleteUserStaffRoleModuleStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Permissions for Module");
							break;
						default:
							throw new NotImplementedEnumException(deleteStatus);
					}
					Redirect(this.UserID.Value);
					return;
			}
		}

		protected void btnAddRoleModule_OnClick(object sender, EventArgs e)
		{
			if (this.UserID == null)
			{
				this.AddNoRecordFoundAlert();
				Users.Redirect();
				return;
			}

			this.ddlUserRoleIDRoleModule.ClearSelection();
			this.ddlUserRoleIDRoleModule_OnSelectedIndexChanged(null, null);

			this.modalRoleModule.HeaderText = "Add Role Module";
			this.btnSaveRoleModule.Text = "Add";
			this.ViewState["UserRoleModuleID"] = null;
			this.modalRoleModule.Show();
		}

		protected void ddlUserRoleIDRoleModule_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var selectedValues = this.ddlUserRoleIDRoleModule.SelectedValue.Split(':');
			var userRoleID = selectedValues[0].ToInt();
			var userType = (UserTypes)selectedValues[1].ToShort();
			var instituteID = selectedValues[2].ToInt();

			var aspireModules = AspirePageAttributesDictionary.GuidDictionary.Select(d => d.Value)
				.Where(a => a.UserTypes.HasFlag(userType)).Select(a => a.AspireModule).Distinct().Except(new[] { AspireModules.None }).OrderBy(m => m.ToFullName()).ToArray();

			this.cblModuleRoleModule.Items.Clear();
			this.cblModuleRoleModule.AddEnums(aspireModules);

			this.ddlDepartmentIDRoleModule.FillDepartments(instituteID, CommonListItems.All);
			this.ddlDepartmentIDRoleModule_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDRoleModule_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var selectedValues = this.ddlUserRoleIDRoleModule.SelectedValue.Split(':');
			var userRoleID = selectedValues[0].ToInt();
			var userType = (UserTypes)selectedValues[1].ToShort();
			var instituteID = selectedValues[2].ToInt();
			var departmentID = this.ddlDepartmentIDRoleModule.SelectedValue.ToNullableInt();
			if (departmentID == null)
			{
				this.ddlProgramIDRoleModule.Items.Clear();
				this.ddlAdmissionOpenProgramIDRoleModule.Items.Clear();
				this.divProgram.Visible = false;
				this.divAdmissionOpenProgram.Visible = false;
			}
			else
			{
				this.divProgram.Visible = true;
				this.ddlProgramIDRoleModule.FillPrograms(instituteID, departmentID.Value, false, CommonListItems.All);
				this.ddlProgramIDRoleModule_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlProgramIDRoleModule_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var programID = this.ddlProgramIDRoleModule.SelectedValue.ToNullableInt();
			if (programID == null)
			{
				this.ddlAdmissionOpenProgramIDRoleModule.Items.Clear();
				this.divAdmissionOpenProgram.Visible = false;
			}
			else
			{
				this.divAdmissionOpenProgram.Visible = true;
				var admissionOpenPrograms = BL.Core.UserManagement.Admin.Users.GetAdmissionOpenProgramsList(programID.Value, this.AdminIdentity.LoginSessionGuid);
				this.ddlAdmissionOpenProgramIDRoleModule.DataBind(admissionOpenPrograms, CommonListItems.All);
			}
		}

		protected void btnSaveRoleModule_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;

			var selectedValues = this.ddlUserRoleIDRoleModule.SelectedValue.Split(':');
			var userRoleID = selectedValues[0].ToInt();
			var userType = (UserTypes)selectedValues[1].ToShort();
			var instituteID = selectedValues[2].ToInt();
			var moduleEnums = this.cblModuleRoleModule.GetSelectedEnumValues<AspireModules>();
			var departmentID = this.ddlDepartmentIDRoleModule.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramIDRoleModule.SelectedValue.ToNullableInt();
			var admissionOpenProgramID = this.ddlAdmissionOpenProgramIDRoleModule.SelectedValue.ToNullableInt();
			foreach (var moduleEnum in moduleEnums)
			{
				var status = BL.Core.UserManagement.Admin.Users.AddUserRoleModule(userRoleID, moduleEnum, instituteID, departmentID, programID, admissionOpenProgramID, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.UserManagement.Admin.Users.AddUserRoleModuleStatuses.Success:
						this.AddSuccessMessageHasBeenAdded($"Permissions for Module \"{moduleEnum.ToFullName()}\"");
						break;
					case BL.Core.UserManagement.Admin.Users.AddUserRoleModuleStatuses.AlreadyExists:
						this.AddErrorAlert($"Permissions already exists or higher level permissions for Module \"{moduleEnum.ToFullName()}\" exists.");
						break;
					case BL.Core.UserManagement.Admin.Users.AddUserRoleModuleStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect(this.UserID.Value);
						return;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
			Redirect(this.UserID.Value);
		}

		#endregion

		protected void lbtnResetPassword_OnClick(object sender, EventArgs e)
		{
			if (this.UserID == null)
				return;
			var user = BL.Core.UserManagement.Admin.Users.GetUser(this.UserID.Value, this.AdminIdentity.LoginSessionGuid);
			if (user == null)
			{
				this.AddNoRecordFoundAlert();
				Users.Redirect();
				return;
			}
			this.lblUsername.Text = user.Username;
			this.lblName.Text = user.Name;
			this.lblEmail.Text = user.Email;
			this.lblStatus.Text = user.StatusEnum.ToString();
			this.tbNewPassword.Text = null;
			this.tbConfirmNewPassword.Text = null;
			this.ddlReason.FillPasswordChangeReasons(CommonListItems.Select);

			this.modelResetPassword.HeaderText = "Reset Password";
			this.modelResetPassword.Show();
		}

		protected void btnChangePassword_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid || this.UserID == null)
				return;
			var newPassword = this.tbNewPassword.Text;
			var passwordChangeReasonEnum = this.ddlReason.GetSelectedEnumValue<PasswordChangeReasons>();
			//var passwordResetPageUrl = GetAspirePageAttribute<Logins.Faculty.PasswordReset>().PageUrl.ToAbsoluteUrl();
			var result = BL.Core.UserManagement.Admin.Users.ResetUserPassword(this.UserID.Value, newPassword, passwordChangeReasonEnum, this.AdminIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.UserManagement.Admin.Users.ResetUserPasswordStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					break;
				case BL.Core.UserManagement.Admin.Users.ResetUserPasswordStatuses.Success:
					this.AddSuccessMessageHasBeenUpdated("User password");
					break;
				default:
					throw new NotImplementedEnumException(result);
			}
			Redirect(this.UserID);
		}

		protected void btnSendResetPasswordLink_OnClick(object sender, EventArgs e)
		{
			if (this.UserID == null)
				return;
			var passwordResetPageUrl = GetAspirePageAttribute<Logins.User.ResetPassword>().PageUrl.ToAbsoluteUrl();
			var result = BL.Core.UserManagement.Admin.Users.SendPasswordResetLinkToUser(this.UserID.Value, passwordResetPageUrl, this.AdminIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.UserManagement.Admin.Users.ResetUserPasswordByLinkStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					break;
				case BL.Core.UserManagement.Admin.Users.ResetUserPasswordByLinkStatuses.Success:
					this.AddSuccessAlert("User password link has been sent.");
					break;
				default:
					throw new NotImplementedEnumException(result);
			}
			Redirect(this.UserID);
		}
	}
}