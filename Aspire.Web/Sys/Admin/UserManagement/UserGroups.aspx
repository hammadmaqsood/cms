﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="UserGroups.aspx.cs" Inherits="Aspire.Web.Sys.Admin.UserManagement.UserGroups" %>

<%@ Import Namespace="Aspire.Web.Sys.Admin.UserManagement" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireHyperLinkButton id="hlAddUserGroup" runat="server" ButtonType="Success" Glyphicon="plus" NavigateUrl="UserGroup.aspx" Text="Add User Group" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel Text="Institute:" runat="server" AssociatedControlID="ddlInstituteID" />
					<aspire:AspireDropDownList ID="ddlInstituteID" AutoPostBack="true" OnSelectedIndexChanged="ddlInstituteID_SelectedIndexChanged" runat="server" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel Text="User Type:" runat="server" AssociatedControlID="ddlUserType" />
					<aspire:AspireDropDownList ID="ddlUserType" AutoPostBack="true" OnSelectedIndexChanged="ddlUserType_SelectedIndexChanged" runat="server" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Search:" AssociatedControlID="tbSearch" />
					<asp:Panel runat="server" CssClass="input-group" DefaultButton="lbtnSearch">
						<aspire:AspireTextBox runat="server" ID="tbSearch" />
						<span class="input-group-btn">
							<aspire:AspireButton Glyphicon="search" ButtonType="Default" OnClick="lbtnSearch_Click" ID="lbtnSearch" runat="server" />
						</span>
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" AutoGenerateColumns="False" AllowSorting="True" ID="gv" OnSorting="gv_Sorting" AllowPaging="True" OnPageIndexChanging="gv_PageIndexChanging" OnRowCommand="gv_RowCommand" OnPageSizeChanging="gv_OnPageSizeChanging">
		<Columns>
			<asp:BoundField HeaderText="Group Name" DataField="GroupName" SortExpression="GroupName" />
			<asp:BoundField HeaderText="User Type" DataField="UserTypeEnum" SortExpression="UserType" />
			<asp:BoundField HeaderText="Institute" DataField="InstituteAlias" SortExpression="InstituteAlias" />
			<asp:BoundField HeaderText="Members" DataField="Members" />
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" Text="Edit Menu" NavigateUrl='<%# UserGroupMenu.GetPageUrl((int)Eval("UserGroupID")) %>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireHyperLinkButton Glyphicon="edit" ButtonType="OnlyIcon" runat="server" NavigateUrl='<%# Aspire.Web.Sys.Admin.UserManagement.UserGroup.GetPageUrl((int) Eval("UserGroupID")) %>' />
					<aspire:AspireLinkButton Glyphicon="remove" ConfirmMessage="Are you sure you want to delete the selected user group?" CommandName="Delete" runat="server" EncryptedCommandArgument='<%# Eval("UserGroupID") %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
