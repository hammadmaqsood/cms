﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="Aspire.Web.Sys.Admin.UserManagement.Users" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireHyperLinkButton ID="hlAddUser" ButtonType="Success" Glyphicon="plus" runat="server" Text="Add User" NavigateUrl="User.aspx" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
					<aspire:AspireDropDownList ValidationGroup="Search" runat="server" ID="ddlInstituteID" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Usertype:" AssociatedControlID="ddlUserType" />
					<aspire:AspireDropDownList ValidationGroup="Search" runat="server" ID="ddlUserType" AutoPostBack="True" OnSelectedIndexChanged="ddlUserType_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Group:" AssociatedControlID="ddlUserGroupID" />
					<aspire:AspireDropDownList ValidationGroup="Search" runat="server" ID="ddlUserGroupID" AutoPostBack="True" OnSelectedIndexChanged="ddlUserGroupID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatus" />
					<aspire:AspireDropDownList ValidationGroup="Search" runat="server" ID="ddlStatus" AutoPostBack="True" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Search:" AssociatedControlID="tbSearch" />
					<asp:Panel runat="server" CssClass="input-group" DefaultButton="lbtnSearch">
						<aspire:AspireTextBox runat="server" ID="tbSearch" CssClass="form-control" />
						<span class="input-group-btn">
							<aspire:AspireButton ValidationGroup="Search" ButtonType="Default" Glyphicon="search" OnClick="lbtnSearch_Click" ID="lbtnSearch" runat="server" />
						</span>
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>

	<aspire:AspireGridView runat="server" AutoGenerateColumns="False" AllowSorting="True" ID="gv" CssClass="topMargin15" OnSorting="gv_Sorting" AllowPaging="True" OnPageIndexChanging="gv_PageIndexChanging" OnRowCommand="gv_RowCommand" OnRowDataBound="gv_RowDataBound" OnPageSizeChanging="gv_OnPageSizeChanging">
		<Columns>
			<asp:TemplateField HeaderText="Username" SortExpression="Username">
				<ItemTemplate>
					<asp:PlaceHolder runat="server" Visible='<%# (bool)Eval("AccountActivatedByUser") %>'><%#: Eval("Username") %></asp:PlaceHolder>
					<asp:PlaceHolder runat="server" Visible='<%# !(bool)Eval("AccountActivatedByUser") %>'><s title="Account is not activated by user"><%#: Eval("Username") %></s></asp:PlaceHolder>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Name" SortExpression="Name">
				<ItemTemplate>
					<%#: Eval("Name") %><br />
					<small>
						<aspire:AspireHyperLink runat="server" ToolTip='<%# Eval("Email","mailto://{0}") %>' NavigateUrl='<%# Eval("Email","mailto://{0}") %>' Text='<%# Eval("Email") %>' /></small>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Job Title" DataField="JobTitle" SortExpression="JobTitle" />
			<asp:BoundField HeaderText="Status" DataField="StatusEnum" SortExpression="Status" />
			<asp:BoundField HeaderText="Roles" DataField="Roles" />
			<asp:BoundField HeaderText="Groups" DataField="Groups" />
			<asp:TemplateField HeaderText="Created On" SortExpression="CreatedOn">
				<ItemTemplate>
					<%#: this.Eval("CreatedOn") %><br />
					<small>
						<%#:this.Eval("CreatedBy") %></small>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Expiry Date" DataField="StringExpiryDates" />
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireHyperLinkButton ButtonType="OnlyIcon" Glyphicon="edit" runat="server" NavigateUrl='<%# Aspire.Web.Sys.Admin.UserManagement.User.GetPageUrl((int) Eval("UserID")) %>' />
					<aspire:AspireLinkButton Visible='<%# this.DeleteAllowed %>' Glyphicon="remove" ConfirmMessage="Are you sure you want to delete the selected user?" CommandName="Delete" runat="server" EncryptedCommandArgument='<%# Eval("UserID") %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
