﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.UserManagement
{
	[AspirePage("7B59CD7C-37C7-480E-A240-C52819580041", UserTypes.Admins, "~/Sys/Admin/UserManagement/UserGroup.aspx", "User Group", true, AspireModules.UserManagement)]
	public partial class UserGroup : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.UserManagement.Module, UserGroupPermission.Allowed}
		};

		public static string GetPageUrl(int? userGroupID = null)
		{
			return GetPageUrl<UserGroup>().AttachQueryParam("UserGroupID", userGroupID);
		}

		public static void Redirect(int? userGroupID = null)
		{
			Redirect(GetPageUrl(userGroupID));
		}

		private int? UserGroupID => this.Request.GetParameterValue<int>("UserGroupID");

		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_users);
		public override string PageTitle => "Group";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlInstituteID.FillInstitutesForAdmin();
				if (this.AdminIdentity.UserType.IsMasterAdmin())
					this.ddlUserType.AddEnums(UserTypes.Admin, UserTypes.Staff, UserTypes.Executive);
				else
					this.ddlUserType.AddEnums(UserTypes.Staff);

				var userGroupID = this.UserGroupID;
				if (userGroupID != null)
				{
					var userGroup = Aspire.BL.Core.UserManagement.Admin.UserGroups.GetUserGroup(userGroupID.Value, this.AdminIdentity.LoginSessionGuid);
					if (userGroup == null)
					{
						this.AddNoRecordFoundAlert();
						Redirect<Sys.Admin.UserManagement.UserGroups>();
						return;
					}

					this.tbGroupName.Text = userGroup.GroupName;
					this.ddlUserType.SetEnumValue(userGroup.UserTypeEnum);
					this.ddlUserType.Enabled = false;
					this.ddlUserType_SelectedIndexChanged(null, null);
					if (!userGroup.UserTypeEnum.IsAdmin())//institute is null for admin group
						this.ddlInstituteID.SelectedValue = userGroup.InstituteID.ToString();
					this.ddlInstituteID.Enabled = false;
				}
				else
				{
					this.AdminIdentity.Demand(AdminPermissions.UserManagement.ManageUserGroups, UserGroupPermission.PermissionValues.Allowed);
					this.ddlUserType_SelectedIndexChanged(null, null);
				}
			}
		}

		protected void ddlUserType_SelectedIndexChanged(object sender, EventArgs e)
		{
			var userType = this.ddlUserType.GetSelectedEnumValue<UserTypes>();
			switch (userType)
			{
				case UserTypes.Admin:
					this.divInstitute.Visible = false;
					break;
				case UserTypes.Executive:
					this.divInstitute.Visible = true;
					break;
				case UserTypes.Staff:
					this.divInstitute.Visible = true;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			List<UserGroupPermission> userGroupPermissions = null;
			if (this.UserGroupID != null)
				userGroupPermissions = Aspire.BL.Core.UserManagement.Admin.UserGroups.GetUserGroupPermissions(this.UserGroupID.Value, this.AdminIdentity.LoginSessionGuid);

			var permissionsList = userType.GetPermissionsList();
			var groupedPermissions = permissionsList.Select(p => p.Module).Distinct().Select(module => new
			{
				Module = module,
				ModuleEnum = (AspireModules)module,
				ModuleFullName = ((AspireModules)module).ToFullName(),
				Permissions = permissionsList.FindAll(p => p.Module == module).Select(p => new
				{
					p.PermissionType,
					p.PermissionTypeFullName,
					PermissionValues = p.PossiblePermissionValues.Select(v => new { Text = v.ToFullName(), Value = v.ToByte().ToString() }),
					Value = userGroupPermissions?.SingleOrDefault(ugp => ugp.Module == p.Module && ugp.PermissionType == p.PermissionType)?.PermissionValue ?? p.DefaultValue,
				}),
			}).ToList();

			this.repeater.DataBind(groupedPermissions);
		}

		protected void lbtnSave_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;

			this.AdminIdentity.Demand(AdminPermissions.UserManagement.ManageUserGroups, UserGroupPermission.PermissionValues.Allowed);
			var userGroupPermissionsList = new List<UserGroupPermission>();
			foreach (var mItem in this.repeater.Items.Cast<RepeaterItem>().Where(i => i.ItemType == ListItemType.Item || i.ItemType == ListItemType.AlternatingItem))
			{
				var module = ((Lib.WebControls.HiddenField)mItem.FindControl("hf")).Value.ToByte();
				var rep = (Repeater)mItem.FindControl("r");
				foreach (var pItem in rep.Items.Cast<RepeaterItem>().Where(i => i.ItemType == ListItemType.Item || i.ItemType == ListItemType.AlternatingItem))
				{
					var permissionType = ((Lib.WebControls.HiddenField)pItem.FindControl("hf")).Value.ToLong();
					var permissionValue = ((AspireRadioButtonList)pItem.FindControl("rbl")).SelectedValue.ToByte();
					userGroupPermissionsList.Add(new UserGroupPermission
					{
						Module = module,
						ModuleName = ((AspireModules)module).ToString(),
						PermissionType = permissionType,
						PermissionTypeName = ((UserGroupPermission.PermissionTypes)permissionType).ToString(),
						PermissionValue = permissionValue,
						PermissionValueName = ((UserGroupPermission.PermissionValues)permissionValue).ToString(),
					});
				}
			}

			if (this.UserGroupID == null)
			{
				var userType = this.ddlUserType.GetSelectedEnumValue<UserTypes>();
				var instituteID = this.ddlInstituteID.Visible ? this.ddlInstituteID.SelectedValue.ToInt() : (int?)null;
				var result = Aspire.BL.Core.UserManagement.Admin.UserGroups.AddUserGroup(userType, this.tbGroupName.Text, instituteID, userGroupPermissionsList, this.AspireIdentity.LoginSessionGuid);
				if (result == null)
					throw new InvalidOperationException();
				switch (result.Status)
				{
					case BL.Core.UserManagement.Admin.UserGroups.AddUserGroupResult.Statuses.GroupNameAlreadyExists:
						this.AddErrorAlert("Group name already exists.");
						break;
					case BL.Core.UserManagement.Admin.UserGroups.AddUserGroupResult.Statuses.Success:
						this.AddSuccessMessageHasBeenAdded("User Group");
						Redirect(result.UserGroup.UserGroupID);
						break;
					default:
						throw new NotImplementedEnumException(result.Status);
				}
			}
			else
			{
				var result = Aspire.BL.Core.UserManagement.Admin.UserGroups.UpdateUserGroup(this.UserGroupID.Value, this.tbGroupName.Text, userGroupPermissionsList, this.AspireIdentity.LoginSessionGuid);
				if (result == null)
					throw new InvalidOperationException();
				switch (result.Status)
				{
					case BL.Core.UserManagement.Admin.UserGroups.UpdateUserGroupResult.Statuses.GroupNameAlreadyExists:
						this.AddErrorAlert("Group name already exists.");
						break;
					case BL.Core.UserManagement.Admin.UserGroups.UpdateUserGroupResult.Statuses.Success:
						this.AddSuccessAlert("User Group has been updated.");
						Redirect<UserGroups>();
						return;
					case BL.Core.UserManagement.Admin.UserGroups.UpdateUserGroupResult.Statuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<UserGroups>();
						return;
					default:
						throw new NotImplementedEnumException(result.Status);
				}
			}
		}
	}
}