﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.UserManagement
{
	[AspirePage("8F250D05-C069-4C06-B975-8318C0798136", UserTypes.Admins, "~/Sys/Admin/UserManagement/Users.aspx", "Users", true, AspireModules.UserManagement)]
	public partial class Users : AdminsPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_users);
		public override string PageTitle => "Users Report";
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.UserManagement.ManageUsers, new [] {UserGroupPermission.PermissionValues.Allowed } }
		};

		private bool? _deleteAllowed;
		protected bool DeleteAllowed
		{
			get
			{
				if (this._deleteAllowed == null)
					this._deleteAllowed = this.AdminIdentity.UserType.IsMasterAdmin();
				return this._deleteAllowed.Value;
			}
		}

		public static void Redirect()
		{
			Redirect<Users>();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortExpression("Name");
				this.ViewState.SetSortDirection(SortDirection.Ascending);
				this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.All);
				this.ddlStatus.FillEnums<Model.Entities.User.Statuses>(CommonListItems.All);
				this.ddlInstituteID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlInstituteID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			if (instituteID == null)
			{
				if (this.AdminIdentity.UserType.IsMasterAdmin())
					this.ddlUserType.AddEnums(CommonListItems.All, UserTypes.MasterAdmin, UserTypes.Admin, UserTypes.Executive, UserTypes.Staff);
				else
					this.ddlUserType.AddEnums(CommonListItems.All, UserTypes.Admin, UserTypes.Executive, UserTypes.Staff);
			}
			else
			{
				this.ddlUserType.AddEnums(CommonListItems.All, UserTypes.Executive, UserTypes.Staff);
			}

			this.ddlUserType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlUserType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			var userTypeEnum = this.ddlUserType.GetSelectedEnumValue<UserTypes>(null);
			switch (userTypeEnum)
			{
				case null:
				case UserTypes.MasterAdmin:
				case UserTypes.Admin:
					this.ddlUserGroupID.DataBind(new[] { CommonListItems.All });
					break;
				case UserTypes.Executive:
				case UserTypes.Staff:
					if (instituteID == null)
						this.ddlUserGroupID.DataBind(new[] { CommonListItems.All });
					else
					{
						var userGroups = Aspire.BL.Core.UserManagement.Admin.UserGroups.GetUserGroupsList(userTypeEnum.Value, instituteID.Value, this.AdminIdentity.LoginSessionGuid);
						this.ddlUserGroupID.DataBind(userGroups, CommonListItems.All);
					}
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.ddlUserGroupID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlUserGroupID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlStatus_SelectedIndexChanged(null, null);
		}

		protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.lbtnSearch_Click(null, null);
		}

		protected void lbtnSearch_Click(object sender, EventArgs e)
		{
			this.RefreshData(0);
		}

		private void RefreshData(int pageIndex)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			var userTypeEnum = this.ddlUserType.GetSelectedEnumValue<UserTypes>(null);
			var userGroupID = this.ddlUserGroupID.SelectedValue.ToNullableInt();
			var statusEnum = this.ddlStatus.GetSelectedEnumValue<Model.Entities.User.Statuses>(null);
			var searchText = this.tbSearch.Text;
			var users = Aspire.BL.Core.UserManagement.Admin.Users.GetUsers(instituteID, userTypeEnum, userGroupID, statusEnum, searchText, pageIndex, this.gv.PageSize, out int virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.AdminIdentity.LoginSessionGuid);
			this.gv.DataBind(users, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		protected void gv_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.RefreshData(this.gv.PageIndex);
		}

		protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.RefreshData(e.NewPageIndex);
		}

		protected void gv_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					this.AdminIdentity.Demand(AdminPermissions.UserManagement.ManageUsers, UserGroupPermission.PermissionValues.Allowed);
					e.Handled = true;
					var userID = e.DecryptedCommandArgumentToInt();
					var status = Aspire.BL.Core.UserManagement.Admin.Users.DeleteUser(userID, this.AdminIdentity.LoginSessionGuid);
					switch (status)
					{
						case BL.Core.UserManagement.Admin.Users.DeleteUserStatus.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.RefreshData(0);
							break;
						case BL.Core.UserManagement.Admin.Users.DeleteUserStatus.CannotDeleteYourself:
							this.AddErrorAlert("You cannot delete yourself.");
							break;
						case BL.Core.UserManagement.Admin.Users.DeleteUserStatus.Success:
							this.AddSuccessMessageHasBeenDeleted("User");
							this.RefreshData(0);
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow)
				return;
			var user = e.Row.DataItem as Aspire.BL.Core.UserManagement.Admin.Users.CustomUser;
			if (user.StatusEnum == Model.Entities.User.Statuses.Inactive)
				e.Row.CssClass += " danger";
			if (user.ExpiryDates.Any(ed => ed <= DateTime.Now))
				e.Row.CssClass += " danger";
		}

		protected void gv_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gv.PageSize = e.NewPageSize;
			this.RefreshData(0);
		}
	}
}