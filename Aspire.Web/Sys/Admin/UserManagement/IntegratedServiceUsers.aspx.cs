﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Net;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.UserManagement
{
	[AspirePage("2E6D3373-EC35-4E2E-8399-A60454F63FFD", UserTypes.MasterAdmin, "~/Sys/Admin/UserManagement/IntegratedServiceUsers.aspx", "Integrated Service Users", true, AspireModules.UserManagement)]
	public partial class IntegratedServiceUsers : MasterAdminPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_users);
		public override string PageTitle => "Integrated Service Users";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.All);
				this.ViewState.SetSortProperties("InstituteAlias", SortDirection.Ascending);
				this.GetData(0);
			}
		}

		protected void btnAdd_OnClick(object sender, EventArgs e)
		{
			this.DisplayIntegratedServiceUsersModel(null);
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var integratedServiceUserID = (int?)this.ViewState[nameof(IntegratedServiceUser.IntegratedServiceUserID)];
			var serviceName = this.tbServiceName.Text;

			var ipAddress = System.Net.IPAddress.Parse(this.tbIPAddress.Text);
			var expiryDate = this.dtpExpiryDate.SelectedDate.Value;
			var statusEnum = this.rblStatus.GetSelectedEnumValue<IntegratedServiceUser.Statuses>();
			var userName = this.tbUsername.Text.ToGuid();
			var password = this.tbPassword.Text.ToNullableGuid();
			if (integratedServiceUserID == null)
			{
				var serviceTypeEnum = this.ddlServiceType.GetSelectedEnumValue<IntegratedServiceUser.ServiceTypes>();
				var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
				if (password == null)
				{
					this.alertModalmodalIntegratedServiceUsers.AddErrorAlert("Password must be an valid Guid.");
					this.updateModalmodalIntegratedServiceUsers.Update();
					return;
				}
				var status = BL.Core.UserManagement.Admin.IntegratedServiceUsers.AddIntegratedServiceUser(serviceName, serviceTypeEnum, instituteID, userName, password.Value, ipAddress, expiryDate, statusEnum, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.UserManagement.Admin.IntegratedServiceUsers.AddIntegratedServiceUserStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Integrated service user");
						Redirect<IntegratedServiceUsers>();
						break;
					case BL.Core.UserManagement.Admin.IntegratedServiceUsers.AddIntegratedServiceUserStatuses.AlreadyExists:
						this.alertModalmodalIntegratedServiceUsers.AddErrorAlert("Username already exists.");
						this.updateModalmodalIntegratedServiceUsers.Update();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var status = BL.Core.UserManagement.Admin.IntegratedServiceUsers.UpdateIntegratedServiceUser(integratedServiceUserID.Value, password, serviceName, ipAddress, expiryDate, statusEnum, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.UserManagement.Admin.IntegratedServiceUsers.UpdateIntegratedServiceUserStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<IntegratedServiceUsers>();
						return;
					case BL.Core.UserManagement.Admin.IntegratedServiceUsers.UpdateIntegratedServiceUserStatuses.UsernameAlreadyExists:
						this.alertModalmodalIntegratedServiceUsers.AddErrorAlert("Username already exists.");
						this.updateModalmodalIntegratedServiceUsers.Update();
						break;
					case BL.Core.UserManagement.Admin.IntegratedServiceUsers.UpdateIntegratedServiceUserStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Integrated service user");
						Redirect<IntegratedServiceUsers>();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		protected void btnGenerateUsername_OnClick(object sender, EventArgs e)
		{
			this.tbUsername.Text = Guid.NewGuid().ToString();
		}

		protected void btnGeneratePassword_OnClick(object sender, EventArgs e)
		{
			this.tbPassword.Text = Guid.NewGuid().ToString();
		}

		protected void gvIntegratedServiceUsers_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Status":
					var integratedServiceUserID = e.DecryptedCommandArgumentToInt();
					var status = BL.Core.UserManagement.Admin.IntegratedServiceUsers.ToggleIntegratedServiceUserStatus(integratedServiceUserID, this.AdminIdentity.LoginSessionGuid);
					switch (status)
					{
						case BL.Core.UserManagement.Admin.IntegratedServiceUsers.UpdateIntegratedServiceUserStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							Redirect<IntegratedServiceUsers>();
							return;
						case BL.Core.UserManagement.Admin.IntegratedServiceUsers.UpdateIntegratedServiceUserStatuses.UsernameAlreadyExists:
							this.alertModalmodalIntegratedServiceUsers.AddErrorAlert("Username already exists.");
							this.updateModalmodalIntegratedServiceUsers.Update();
							break;
						case BL.Core.UserManagement.Admin.IntegratedServiceUsers.UpdateIntegratedServiceUserStatuses.Success:
							this.AddSuccessMessageHasBeenUpdated("Integrated service user");
							Redirect<IntegratedServiceUsers>();
							return;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
				case "Edit":
					e.Handled = true;
					this.DisplayIntegratedServiceUsersModel(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var deleteStatus = BL.Core.UserManagement.Admin.IntegratedServiceUsers.DeleteIntegratedServiceUser(e.DecryptedCommandArgumentToInt(), this.AdminIdentity.LoginSessionGuid);
					switch (deleteStatus)
					{
						case BL.Core.UserManagement.Admin.IntegratedServiceUsers.DeleteIntegratedServiceUserStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.UserManagement.Admin.IntegratedServiceUsers.DeleteIntegratedServiceUserStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Integrated service user");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					Redirect<IntegratedServiceUsers>();
					return;
			}
		}

		private void DisplayIntegratedServiceUsersModel(int? integratedServiceUserID)
		{
			this.ddlServiceType.FillIntegrationServiceTypes();
			this.rblStatus.FillEnums<IntegratedServiceUser.Statuses>();

			if (integratedServiceUserID == null)
			{
				this.tbServiceName.Text = null;
				this.ddlServiceType.ClearSelection();
				this.ddlServiceType.Enabled = true;
				this.ddlInstituteID.ClearSelection();
				this.ddlInstituteID.Enabled = true;
				this.tbUsername.Text = Guid.NewGuid().ToString();
				this.btnGenerateUsername.Enabled = true;
				this.tbPassword.Text = Guid.NewGuid().ToString();
				this.svtbPassword.AllowNull = false;
				this.btnGeneratePassword.Enabled = true;
				this.tbIPAddress.Text = null;
				this.dtpExpiryDate.SelectedDate = DateTime.Today.AddMonths(6);
				this.rblStatus.SetEnumValue(IntegratedServiceUser.StatusActive);

				this.modalIntegratedServiceUsers.HeaderText = "Add User";
				this.ViewState[nameof(IntegratedServiceUser.IntegratedServiceUserID)] = null;
				this.modalIntegratedServiceUsers.Show();
			}
			else
			{
				var integratedServiceUser = BL.Core.UserManagement.Admin.IntegratedServiceUsers.GetIntegratedServiceUser(integratedServiceUserID.Value, this.AdminIdentity.LoginSessionGuid);
				if (integratedServiceUser == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<IntegratedServiceUsers>();
					return;
				}
				this.tbServiceName.Text = integratedServiceUser.ServiceName;
				this.ddlServiceType.SetEnumValue(integratedServiceUser.ServiceTypeEnum);
				this.ddlServiceType.Enabled = false;
				this.ddlInstituteID.SelectedValue = integratedServiceUser.InstituteID.ToString();
				this.ddlInstituteID.Enabled = false;
				this.tbUsername.Text = integratedServiceUser.UserName.ToString();
				this.btnGenerateUsername.Enabled = false;
				this.tbPassword.Text = null;
				this.svtbPassword.AllowNull = true;
				this.btnGeneratePassword.Enabled = true;
				this.tbIPAddress.Text = integratedServiceUser.IPAddress;
				this.dtpExpiryDate.SelectedDate = integratedServiceUser.ExpiryDate;
				this.rblStatus.SetEnumValue(integratedServiceUser.StatusEnum);

				this.modalIntegratedServiceUsers.HeaderText = "Edit User";
				this.ViewState[nameof(integratedServiceUser.IntegratedServiceUserID)] = integratedServiceUserID;
				this.modalIntegratedServiceUsers.Show();
			}
		}

		protected void gvIntegratedServiceUsers_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvIntegratedServiceUsers_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvIntegratedServiceUsers.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvIntegratedServiceUsers_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			int virtualItemCount;
			var intergratedServiceUsers = BL.Core.UserManagement.Admin.IntegratedServiceUsers.GetIntegratedServiceUsers(pageIndex, this.gvIntegratedServiceUsers.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.AdminIdentity.LoginSessionGuid, out virtualItemCount);
			this.gvIntegratedServiceUsers.DataBind(intergratedServiceUsers, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}
	}
}