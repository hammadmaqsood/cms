﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="UserGroup.aspx.cs" Inherits="Aspire.Web.Sys.Admin.UserManagement.UserGroup" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-horizontal">
		<div class="form-group">
			<asp:Label runat="server" CssClass="col-md-2 control-label" Text="Group Name:" AssociatedControlID="tbGroupName" />
			<div class="col-md-6">
				<aspire:AspireTextBox runat="server" ID="tbGroupName" />
				<aspire:AspireStringValidator RequiredErrorMessage="This field is required." runat="server" ControlToValidate="tbGroupName" ValidationGroup="Group" HighlightCssClass="has-error" AllowNull="False" />
			</div>
		</div>
		<div class="form-group">
			<asp:Label runat="server" CssClass="col-md-2 control-label" Text="User Type:" AssociatedControlID="ddlUserType" />
			<div class="col-md-6">
				<aspire:AspireDropDownList ID="ddlUserType" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlUserType_SelectedIndexChanged" />
				<aspire:AspireStringValidator RequiredErrorMessage="This field is required." runat="server" ValidationGroup="Group" ControlToValidate="ddlUserType" HighlightCssClass="has-error" AllowNull="False" />
			</div>
		</div>
		<div class="form-group" id="divInstitute" runat="server">
			<asp:Label runat="server" CssClass="col-md-2 control-label" Text="Institute:" AssociatedControlID="ddlInstituteID" />
			<div class="col-md-6">
				<aspire:AspireDropDownList ID="ddlInstituteID" CssClass="form-control" runat="server" />
				<aspire:AspireStringValidator RequiredErrorMessage="This field is required." runat="server" ValidationGroup="Group" ControlToValidate="ddlInstituteID" HighlightCssClass="has-error" AllowNull="False" />
			</div>
		</div>
	</div>
	<aspire:AspirePanel runat="server" PanelType="Default" ID="panelPermissions" HeaderText="Permissions">
		<asp:Repeater runat="server" ID="repeater">
			<HeaderTemplate>
				<div class="table-responsive">
					<table class=" table table-bordered table-condensed table-hover table-striped">
						<thead>
							<tr>
								<th>Name</th>
								<th>Value</th>
							</tr>
						</thead>
			</HeaderTemplate>
			<ItemTemplate>
				<thead>
					<tr>
						<th colspan="2">
							<%#: Eval("ModuleFullName") %>
							<aspire:HiddenField ID="hf" runat="server" Mode="Encrypted" Value='<%# Eval("Module") %>' />
						</th>
					</tr>
				</thead>
				<tbody>
					<asp:Repeater ID="r" runat="server" DataSource='<%# Eval("Permissions") %>'>
						<ItemTemplate>
							<tr>
								<td style="padding-left: 20px;">
									<%#: Eval("PermissionTypeFullName") %>
									<aspire:HiddenField ID="hf" runat="server" Mode="Encrypted" Value='<%# Eval("PermissionType") %>' />
								</td>
								<td>
									<aspire:AspireRadioButtonList ID="rbl" SelectedValue='<%# Eval("Value") %>' DataSource='<%# Eval("PermissionValues") %>' DataTextField="Text" DataValueField="Value" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" />
								</td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
				</tbody>
			</ItemTemplate>
			<FooterTemplate>
				</table>
				</div>
			</FooterTemplate>
		</asp:Repeater>
	</aspire:AspirePanel>
	<div class="text-center">
		<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save" ValidationGroup="Group" ID="lbtnSave" OnClick="lbtnSave_Click" />
		<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="UserGroups.aspx" />
	</div>
</asp:Content>
