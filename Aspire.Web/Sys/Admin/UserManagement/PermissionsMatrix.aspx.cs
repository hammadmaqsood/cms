﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.UserManagement
{
	[AspirePage("42ABFA2A-45A7-4880-A07F-74E9E0ADC5C7", UserTypes.Admins, "~/Sys/Admin/UserManagement/PermissionsMatrix.aspx", "Permissions Matrix", true, AspireModules.UserManagement)]
	public partial class PermissionsMatrix : AdminsPage
	{
		public override PageIcon PageIcon => Lib.WebControls.AspireGlyphicons.lock_.GetIcon();

		public override string PageTitle => "Permissions Matrix";

		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.UserManagement.Module, new[]{ UserGroupPermission.PermissionValues.Allowed } }
		};

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(nameof(BL.Core.UserManagement.Admin.PermissionsMatrix.Permission.ModuleFullName), SortDirection.Ascending);
				this.ddlUserType.AddEnums(UserTypes.Admin, UserTypes.Staff, UserTypes.Executive);
				this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.All);
				this.ddlModule.FillEnums<AspireModules>(CommonListItems.All);
				this.ddlPermissionValue.FillEnums<UserGroupPermission.PermissionValues>(CommonListItems.All);
				this.ddlUserType_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlUserType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlInstituteID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlInstituteID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlModule_OnSelectedIndexChanged(null, null);
		}

		protected void ddlModule_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlPermissionValue_OnSelectedIndexChanged(null, null);
		}

		protected void ddlPermissionValue_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData();
		}

		protected void gvPermissions_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData();
		}

		private void GetData()
		{
			var userTypeEnum = this.ddlUserType.GetSelectedEnumValue<UserTypes>();
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			var moduleEnum = this.ddlModule.GetSelectedEnumValue<AspireModules>(null);
			//var permissionTypeEnum = this.ddlPermissionType.GetSelectedEnumValue<UserGroupPermission.PermissionTypes>(null);
			var permissionValueEnum = this.ddlPermissionValue.GetSelectedEnumValue<UserGroupPermission.PermissionValues>(null);
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var modules = BL.Core.UserManagement.Admin.PermissionsMatrix.GetPermissionsMatrix(userTypeEnum, instituteID, moduleEnum, permissionValueEnum, sortExpression, sortDirection, this.AdminIdentity.LoginSessionGuid);
			this.gvPermissions.DataBind(modules, sortExpression, sortDirection);
		}
	}
}