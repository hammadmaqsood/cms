﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="PermissionsMatrix.aspx.cs" Inherits="Aspire.Web.Sys.Admin.UserManagement.PermissionsMatrix" %>

<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<asp:Content ContentPlaceHolderID="HeadPH" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel Text="User Type:" runat="server" AssociatedControlID="ddlUserType" />
				<aspire:AspireDropDownList ID="ddlUserType" ValidationGroup="Filters" AutoPostBack="true" OnSelectedIndexChanged="ddlUserType_OnSelectedIndexChanged" runat="server" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel Text="Institute:" runat="server" AssociatedControlID="ddlInstituteID" />
				<aspire:AspireDropDownList ID="ddlInstituteID" ValidationGroup="Filters" AutoPostBack="true" OnSelectedIndexChanged="ddlInstituteID_OnSelectedIndexChanged" runat="server" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel Text="Module:" runat="server" AssociatedControlID="ddlModule" />
				<aspire:AspireDropDownList ID="ddlModule" ValidationGroup="Filters" AutoPostBack="true" OnSelectedIndexChanged="ddlModule_OnSelectedIndexChanged" runat="server" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel Text="Permission Value:" runat="server" AssociatedControlID="ddlPermissionValue" />
				<aspire:AspireDropDownList ID="ddlPermissionValue" ValidationGroup="Filters" AutoPostBack="true" OnSelectedIndexChanged="ddlPermissionValue_OnSelectedIndexChanged" runat="server" />
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvPermissions" AutoGenerateColumns="False" AllowSorting="True" ItemType="Aspire.BL.Core.UserManagement.Admin.PermissionsMatrix.Permission" OnSorting="gvPermissions_OnSorting">
		<Columns>
			<asp:TemplateField HeaderText="Module" SortExpression="ModuleFullName">
				<ItemTemplate>
					<%#: Item.ModuleFullName.ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Permission Name" DataField="PermissionTypeFullName" />
			<asp:BoundField HeaderText="Permission Value" DataField="PermissionValueFullName" />
			<asp:TemplateField HeaderText="Group Name" SortExpression="GroupName">
				<ItemTemplate>
					<aspire:AspireHyperLink Visible="<%# Item.UserGroupID != null %>" runat="server" NavigateUrl="<%# Aspire.Web.Sys.Admin.UserManagement.UserGroup.GetPageUrl(Item.UserGroupID) %>" Text="<%# Item.GroupName %>" Target="_blank" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Institute" SortExpression="InstituteAlias" >
				<ItemTemplate>
					<%# Item.UserGroupID != null ? (Item.InstituteAlias ?? "All") : null %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="User" SortExpression="Name">
				<ItemTemplate>
					<aspire:AspireHyperLink Visible="<%# Item.UserID != null %>" runat="server" NavigateUrl="<%# Aspire.Web.Sys.Admin.UserManagement.User.GetPageUrl(Item.UserID) %>" Text="<%# Item.Name %>" Target="_blank" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Job Title" DataField="JobTitle" SortExpression="JobTitle" />
			<asp:BoundField HeaderText="Status" DataField="StatusFullName" SortExpression="StatusFullName" />
			<asp:BoundField HeaderText="Role Expired" DataField="RoleExpired" SortExpression="RoleExpired" />
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
