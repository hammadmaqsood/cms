﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="Aspire.Web.Sys.Admin.UserManagement.User" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:ScriptManagerProxy runat="server">
		<Scripts>
			<asp:ScriptReference Name="zxcvbn" />
		</Scripts>
	</asp:ScriptManagerProxy>
	<div class="form-horizontal">
		<div class="form-group">
			<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Username:" AssociatedControlID="tbUsername" />
			<div class="col-md-6">
				<aspire:AspireTextBox runat="server" ID="tbUsername" />
				<aspire:AspireStringValidator RequiredErrorMessage="This field is required." runat="server" ControlToValidate="tbUsername" ValidationGroup="User" AllowNull="False" />
			</div>
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Name:" AssociatedControlID="tbName" />
			<div class="col-md-6">
				<aspire:AspireTextBox runat="server" ID="tbName" />
				<aspire:AspireStringValidator RequiredErrorMessage="This field is required." runat="server" ValidationGroup="User" ControlToValidate="tbName" AllowNull="False" />
			</div>
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Email:" AssociatedControlID="tbEmail" />
			<div class="col-md-6">
				<aspire:AspireTextBox runat="server" ID="tbEmail" />
				<aspire:AspireStringValidator RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Email." runat="server" ControlToValidate="tbEmail" ValidationExpression="Email" ValidationGroup="User" AllowNull="False" />
			</div>
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Mobile:" AssociatedControlID="tbMobile" />
			<div class="col-md-6">
				<aspire:AspireTextBox runat="server" ID="tbMobile" data-inputmask="'alias': 'mobile'" />
				<aspire:AspireStringValidator RequiredErrorMessage="This field is required." runat="server" ValidationGroup="User" ControlToValidate="tbMobile" ValidationExpression="Mobile" AllowNull="False" />
			</div>
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Job Title:" AssociatedControlID="tbJobTitle" />
			<div class="col-md-6">
				<aspire:AspireTextBox runat="server" ID="tbJobTitle" />
				<aspire:AspireStringValidator RequiredErrorMessage="This field is required." runat="server" ControlToValidate="tbJobTitle" ValidationGroup="User" AllowNull="False" />
			</div>
		</div>
		<div class="form-group" id="divAccountActivatedByUser" runat="server">
			<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Account Activated By User:" AssociatedControlID="lblAccountActivatedByUser" />
			<div class="col-md-10">
				<p class="form-control-static">
					<aspire:Label ID="lblAccountActivatedByUser" CssClass="form-control-static text-danger" runat="server" />
					<aspire:AspireLinkButton runat="server" Text="Send Confirmation Email Again" ID="lbtnSendConfirmationEmail" OnClick="lbtnSendConfirmationEmail_Click" CausesValidation="False" />
				</p>
			</div>
		</div>
		<div class="form-group" id="divAccountLocked" runat="server">
			<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Account Locked:" AssociatedControlID="lblAccountLocked" />
			<div class="col-md-6">
				<aspire:Label ID="lblAccountLocked" CssClass="form-control-static text-danger" runat="server" />
				<asp:LinkButton runat="server" Text="Activate Account" ID="lbtnActivateAccount" OnClick="lbtnActivateAccount_Click" CausesValidation="False" />
			</div>
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Status:" AssociatedControlID="ddlStatus" />
			<div class="col-md-6">
				<aspire:AspireDropDownList ID="ddlStatus" runat="server" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-6 col-md-offset-2">
				<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="User" Text="Save" ButtonType="Primary" OnClick="btnSave_Click" />
				<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="Users.aspx" />
				<aspire:AspireLinkButton runat="server" ID="lbtnResetPassword" Text="Reset User Password" ButtonType="Danger" CausesValidation="False" OnClick="lbtnResetPassword_OnClick" CssClass="btn btn-danger" Glyphicon="lock_" />
			</div>
		</div>
	</div>
	<div runat="server" id="divRoles">
		<div class="row">
			<div class="col-md-12">
				<aspire:AspireButton Glyphicon="plus" ButtonType="Success" Text="Add Role" runat="server" CausesValidation="False" ID="btnAddRole" OnClick="btnAddRole_Click" />
			</div>
		</div>
		<aspire:AspireGridView runat="server" Caption="Assigned Roles" AutoGenerateColumns="False" ID="gvAdminRoles" OnRowCommand="gvAdminRoles_RowCommand" OnRowDataBound="gvAdminRoles_OnRowDataBound">
			<Columns>
				<asp:BoundField HeaderText="Role" DataField="UserTypeEnum" />
				<asp:BoundField HeaderText="Institute" DataField="InstituteAlias" />
				<asp:TemplateField HeaderText="Group Name" SortExpression="GroupName" ItemStyle-Wrap="False">
					<ItemTemplate>
						<aspire:AspireHyperLink runat="server" Text='<%# this.Eval("GroupName") %>' NavigateUrl='<%#Aspire.Web.Sys.Admin.UserManagement.UserGroup.GetPageUrl((int?)this.Eval("GroupID")) %>' Target="_blank"></aspire:AspireHyperLink>,
						<aspire:AspireHyperLink runat="server" Text="Menus" NavigateUrl='<%#Aspire.Web.Sys.Admin.UserManagement.UserGroupMenu.GetPageUrl((int?)this.Eval("GroupID")) %>' Target="_blank"></aspire:AspireHyperLink>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="Expiry Date" DataField="ExpiryDate" DataFormatString="{0:D}" />
				<asp:BoundField HeaderText="Status" DataField="StatusFullName" />
				<asp:TemplateField HeaderText="Created On" >
					<ItemTemplate>
						<%#: this.Eval("CreatedDate") %><br />
						<small>
							<%#:this.Eval("AssignedByUser") %></small>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Actions">
					<ItemTemplate>
						<aspire:AspireLinkButton ToolTip="Edit" Glyphicon="edit" runat="server" CommandName="Edit" EncryptedCommandArgument='<%# Eval("RoleID")+","+Eval("UserType") %>' />
						<aspire:AspireLinkButton ToolTip="Delete" Glyphicon="remove" runat="server" CausesValidation="False" ConfirmMessage="Are you sure you want to delete the selected role?" CommandName="Delete" EncryptedCommandArgument='<%# Eval("RoleID")+","+Eval("UserType") %>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
		<aspire:AspireModal runat="server" ID="modelResetPassword">
			<BodyTemplate>
				<div class="form-horizontal">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Username:" AssociatedControlID="lblUsername" CssClass="col-md-4" />
						<div class="form-control-static col-md-8">
							<aspire:AspireLabel runat="server" ID="lblUsername" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="lblName" CssClass="col-md-4" />
						<div class="form-control-static col-md-8">
							<aspire:AspireLabel runat="server" ID="lblName" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Email:" AssociatedControlID="lblEmail" CssClass="col-md-4" />
						<div class="form-control-static col-md-8">
							<aspire:AspireLabel runat="server" ID="lblEmail" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="lblStatus" CssClass="col-md-4" />
						<div class="form-control-static col-md-8">
							<aspire:AspireLabel runat="server" ID="lblStatus" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="New Password:" AssociatedControlID="tbNewPassword" />
						<div class="col-md-8">
							<aspire:AspireTextBox runat="server" TextMode="Password" ValidationGroup="ChangePassword" ID="tbNewPassword" PlaceHolder="New Password" />
							<aspire:AspirePasswordStrengthMeter runat="server" TextBoxID="tbNewPassword" />
							<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="ChangePassword" ControlToValidate="tbNewPassword" RequiredErrorMessage="This field is required." />
							<aspire:AspirePasswordPolicyValidator SecurityLevel="Medium" runat="server" ControlToValidate="tbNewPassword" ValidationGroup="ChangePassword" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Confirm New Password:" AssociatedControlID="tbConfirmNewPassword" />
						<div class="col-md-8">
							<aspire:AspireTextBox runat="server" TextMode="Password" ValidationGroup="ChangePassword" ID="tbConfirmNewPassword" PlaceHolder="Confirm New Password" />
							<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="ChangePassword" ControlToValidate="tbConfirmNewPassword" RequiredErrorMessage="This field is required." />
							<aspire:AspireCompareValidator ValidateEmptyText="True" runat="server" ErrorMessage="Passwords do not match." ValidationGroup="ChangePassword" ControlToValidate="tbConfirmNewPassword" ControlToCompare="tbNewPassword" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Reason:" AssociatedControlID="ddlReason" />
						<div class="col-md-8">
							<aspire:AspireDropDownList runat="server" ID="ddlReason" ValidationGroup="ChangePassword" />
							<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ControlToValidate="ddlReason" ValidationGroup="ChangePassword" />
						</div>
					</div>
				</div>
			</BodyTemplate>
			<FooterTemplate>
				<aspire:AspireButton runat="server" Text="Send Reset Password Link" OnClick="btnSendResetPasswordLink_OnClick" ID="btnSendResetPasswordLink" ButtonType="Danger" CausesValidation="False" ConfirmMessage="Are you sure you want to send the password reset link to the user?" />
				<aspire:AspireButton runat="server" ButtonType="Primary" ID="btnChangePassword" Text="Change Password" OnClick="btnChangePassword_OnClick" ValidationGroup="ChangePassword" />
				<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
			</FooterTemplate>
		</aspire:AspireModal>

		<aspire:AspireModal runat="server" ID="modalRole" ModalSize="Large" HeaderText="Add Role">
			<BodyTemplate>
				<asp:UpdatePanel runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<div class="form-horizontal">
							<div class="form-group">
								<aspire:AspireLabel Text="Role:" CssClass="col-md-2" runat="server" AssociatedControlID="ddlUserType" />
								<div class="col-md-10">
									<aspire:AspireDropDownList AutoPostBack="True" ValidationGroup="Role" runat="server" ID="ddlUserType" OnSelectedIndexChanged="ddlUserType_SelectedIndexChanged" />
								</div>
							</div>
							<div class="form-group" id="divddlInstituteID" runat="server">
								<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Institute:" AssociatedControlID="ddlInstituteID" />
								<div class="col-md-10">
									<aspire:AspireDropDownList ValidationGroup="Role" runat="server" ID="ddlInstituteID" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteID_SelectedIndexChanged" />
									<aspire:AspireStringValidator ValidationGroup="Role" AllowNull="False" runat="server" ControlToValidate="ddlInstituteID" ErrorMessage="Required" />
								</div>
							</div>
							<div class="form-group" id="divGroup" runat="server">
								<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Group:" AssociatedControlID="ddlUserGroupID" />
								<div class="col-md-10">
									<aspire:AspireDropDownList ValidationGroup="Role" runat="server" ID="ddlUserGroupID" />
									<aspire:AspireStringValidator ValidationGroup="Role" AllowNull="False" runat="server" ControlToValidate="ddlUserGroupID" RequiredErrorMessage="Required" />
								</div>
							</div>
							<div class="form-group" id="divcblInstituteIDs" runat="server">
								<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Institutes:" AssociatedControlID="cblInstituteIDs" />
								<div class="col-md-10">
									<aspire:AspireCheckBoxList ValidationGroup="Role" runat="server" ID="cblInstituteIDs" RepeatLayout="Flow" RepeatDirection="Vertical" />
									<br />
									<aspire:AspireRequiredFieldValidator ControlToValidate="cblInstituteIDs" runat="server" ErrorMessage="This field is required." ValidationGroup="Role" />
								</div>
							</div>
							<div class="form-group" runat="server">
								<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Expiry Date:" AssociatedControlID="dtpExpiryDate" />
								<div class="col-md-10">
									<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpExpiryDate" ValidationGroup="Role" />
									<aspire:AspireDateTimeValidator InvalidRangeErrorMessage="Invalid Date range." ID="dtvdtpExpiryDate" AllowNull="False" runat="server" ValidationGroup="Role" ControlToValidate="dtpExpiryDate" RequiredErrorMessage="Required" InvalidDataErrorMessage="Invalid Date" />
								</div>
							</div>
							<div class="form-group" runat="server">
								<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Status:" AssociatedControlID="ddlRoleStatus" />
								<div class="col-md-10">
									<aspire:AspireDropDownList runat="server" ID="ddlRoleStatus" ValidationGroup="Role" />
									<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Role" ControlToValidate="ddlRoleStatus" ErrorMessage="This field is required." />
								</div>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</BodyTemplate>
			<FooterTemplate>
				<aspire:Button runat="server" ID="btnRoleSave" CssClass="btn btn-primary" ValidationGroup="Role" Text="Save" OnClick="btnRoleSave_Click" />
				<aspire:AspireModalCloseButton runat="server" ButtonType="FooterCancelButton">Cancel</aspire:AspireModalCloseButton>
			</FooterTemplate>
		</aspire:AspireModal>
	</div>

	<div runat="server" id="divRoleModules">
		<div class="row">
			<div class="col-md-12">
				<aspire:AspireButton Glyphicon="plus" ButtonType="Success" Text="Add Role Modules" runat="server" CausesValidation="False" ID="btnAddRoleModule" OnClick="btnAddRoleModule_OnClick" />
			</div>
		</div>
		<aspire:AspireGridView runat="server" Caption="Assigned Modules" AutoGenerateColumns="False" ID="gvRoleModules" OnRowCommand="gvRoleModules_OnRowCommand">
			<Columns>
				<asp:BoundField HeaderText="Role" DataField="Role" />
				<asp:BoundField HeaderText="Institute" DataField="Institute" />
				<asp:BoundField HeaderText="Module" DataField="AspireModuleFullName" />
				<asp:BoundField HeaderText="Department" DataField="Department" />
				<asp:BoundField HeaderText="Program" DataField="Program" />
				<asp:BoundField HeaderText="Intake" DataField="IntakeSemester" />
				<asp:TemplateField HeaderText="Action">
					<ItemTemplate>
						<aspire:AspireLinkButton ToolTip="Delete" Glyphicon="remove" runat="server" CausesValidation="False" ConfirmMessage="Are you sure you want to delete?" CommandName="Delete" EncryptedCommandArgument='<%# this.Eval("UserRoleModuleID") %>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>

		<aspire:AspireModal runat="server" ID="modalRoleModule" HeaderText="Add Role Module">
			<BodyTemplate>
				<asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateModalRoleModule">
					<ContentTemplate>
						<div class="form-horizontal">
							<div class="form-group">
								<aspire:AspireLabel Text="Role:" CssClass="col-md-4" runat="server" AssociatedControlID="ddlUserRoleIDRoleModule" />
								<div class="col-md-8">
									<aspire:AspireDropDownList CausesValidation="False" AutoPostBack="True" runat="server" ID="ddlUserRoleIDRoleModule" OnSelectedIndexChanged="ddlUserRoleIDRoleModule_OnSelectedIndexChanged" />
									<aspire:AspireRequiredFieldValidator ValidationGroup="RoleModule" runat="server" ControlToValidate="ddlUserRoleIDRoleModule" ErrorMessage="This field is required." />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel Text="Modules:" CssClass="col-md-4" runat="server" AssociatedControlID="cblModuleRoleModule" />
								<div class="col-md-8">
									<aspire:AspireCheckBoxList CausesValidation="False" ValidationGroup="RoleModule" runat="server" ID="cblModuleRoleModule" RepeatDirection="Vertical" RepeatLayout="Flow" />
									<aspire:AspireRequiredFieldValidator ValidationGroup="RoleModule" runat="server" ControlToValidate="cblModuleRoleModule" ErrorMessage="This field is required." />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Department:" AssociatedControlID="ddlDepartmentIDRoleModule" />
								<div class="col-md-8">
									<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDRoleModule" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentIDRoleModule_OnSelectedIndexChanged" />
								</div>
							</div>
							<div class="form-group" id="divProgram" runat="server">
								<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Program:" AssociatedControlID="ddlProgramIDRoleModule" />
								<div class="col-md-8">
									<aspire:AspireDropDownList CausesValidation="False" runat="server" ID="ddlProgramIDRoleModule" AutoPostBack="True" OnSelectedIndexChanged="ddlProgramIDRoleModule_OnSelectedIndexChanged" />
								</div>
							</div>
							<div class="form-group" id="divAdmissionOpenProgram" runat="server">
								<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Intake Semester:" AssociatedControlID="ddlAdmissionOpenProgramIDRoleModule" />
								<div class="col-md-8">
									<aspire:AspireDropDownList runat="server" ID="ddlAdmissionOpenProgramIDRoleModule" />
								</div>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</BodyTemplate>
			<FooterTemplate>
				<aspire:AspireButton ButtonType="Primary" runat="server" ID="btnSaveRoleModule" ValidationGroup="RoleModule" Text="Add" OnClick="btnSaveRoleModule_OnClick" />
				<aspire:AspireModalCloseButton runat="server" ButtonType="FooterCancelButton">Cancel</aspire:AspireModalCloseButton>
			</FooterTemplate>
		</aspire:AspireModal>
	</div>
</asp:Content>
