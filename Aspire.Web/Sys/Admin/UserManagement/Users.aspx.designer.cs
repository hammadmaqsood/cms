﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Web.Sys.Admin.UserManagement {
    
    
    public partial class Users {
        
        /// <summary>
        /// hlAddUser control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireHyperLinkButton hlAddUser;
        
        /// <summary>
        /// ddlInstituteID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireDropDownList ddlInstituteID;
        
        /// <summary>
        /// ddlUserType control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireDropDownList ddlUserType;
        
        /// <summary>
        /// ddlUserGroupID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireDropDownList ddlUserGroupID;
        
        /// <summary>
        /// ddlStatus control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireDropDownList ddlStatus;
        
        /// <summary>
        /// tbSearch control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireTextBox tbSearch;
        
        /// <summary>
        /// lbtnSearch control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireButton lbtnSearch;
        
        /// <summary>
        /// gv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireGridView gv;
    }
}
