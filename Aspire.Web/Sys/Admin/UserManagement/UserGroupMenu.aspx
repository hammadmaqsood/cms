﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="UserGroupMenu.aspx.cs" Inherits="Aspire.Web.Sys.Admin.UserManagement.UserGroupMenu" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="col-md-12">
		<div class="form-horizontal">
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Group Name:" AssociatedControlID="lblGroupName" />
				<p class="form-control-static col-md-10">
					<aspire:Label runat="server" ID="lblGroupName" />
				</p>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="User Type:" AssociatedControlID="lblUserType" />
				<p class="form-control-static col-md-10">
					<aspire:Label runat="server" ID="lblUserType" />
				</p>
			</div>
			<div class="form-group" runat="server" id="divInstitute">
				<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Institute:" AssociatedControlID="lblInstituteAlias" />
				<p class="form-control-static col-md-10">
					<aspire:Label runat="server" ID="lblInstituteAlias" />
				</p>
			</div>
		</div>
	</div>
	<aspire:AspireModal runat="server" ID="modalMenuLink" HeaderText="Add Menu Link" ModalSize="Large">
		<BodyTemplate>
			<asp:UpdatePanel runat="server">
				<ContentTemplate>
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Parent:" AssociatedControlID="ddlParentUserGroupMenuLinkID" />
							<div class="col-md-10">
								<aspire:AspireDropDownList runat="server" ID="ddlParentUserGroupMenuLinkID" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Page:" AssociatedControlID="ddlAspirePageGuid" />
							<div class="col-md-10">
								<aspire:AspireDropDownListWithOptionGroups runat="server" ID="ddlAspirePageGuid" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="ddlAspirePageGuid_SelectedIndexChanged" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Link Text:" AssociatedControlID="tbText" />
							<div class="col-md-10">
								<aspire:AspireTextBox runat="server" ID="tbText" ValidationGroup="Add" MaxLength="100" />
								<aspire:AspireStringValidator ID="svtbText" RequiredErrorMessage="Required" runat="server" AllowNull="False" ControlToValidate="tbText" ValidationGroup="Add" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Visible:" AssociatedControlID="rblVisible" />
							<div class="col-md-10">
								<aspire:AspireRadioButtonList runat="server" ID="rblVisible" ValidationGroup="Add" RepeatLayout="Flow" RepeatDirection="Horizontal" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<aspire:AspireButton ID="lbtnSaveMenuLink" runat="server" ButtonType="Primary" Text="Save" ValidationGroup="Add" OnClick="lbtnSaveMenuLink_Click" />
			<aspire:AspireModalCloseButton ButtonType="FooterCancelButton" runat="server">Cancel</aspire:AspireModalCloseButton>
		</FooterTemplate>
	</aspire:AspireModal>

	<div class="form-group">
		<aspire:AspireButton ID="btnAddMenuLink" CausesValidation="False" runat="server" CommandName="Add" ButtonType="Success" Glyphicon="plus" Text="Add Menu Link" ToolTip="Add" OnClick="btnAddMenuLink_Click" />
		<aspire:AspireButton ButtonType="Danger" runat="server" CausesValidation="False" ConfirmMessage="Are you sure you want to reset to default?" Text="Reset to Default" ID="lbtnReset" OnClick="lbtnReset_Click" />
	</div>
	<asp:Repeater runat="server" ID="repeaterMenu" OnItemCommand="repeaterMenu_ItemCommand">
		<HeaderTemplate>
			<table class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th>Menu Link</th>
						<th>Visibility</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
				<td>
					<aspire:Label runat="server" Width='<%# Unit.Pixel((byte)Eval("IndentLevel")*20) %>' />
					<%#: Eval("LinkText") %>
				</td>
				<td>
					<aspire:AspireLinkButton CausesValidation="False" Text='<%#(bool)Eval("Visible")?"Visible":"Hidden" %>' Visible='<%# Eval("UserGroupMenuLinkID")!=null %>' CommandName="ToggleVisible" runat="server" EncryptedCommandArgument='<%# Eval("UserGroupMenuLinkID") %>' />
				</td>
				<td>
					<aspire:AspireLinkButton CausesValidation="False" runat="server" EncryptedCommandArgument='<%# Eval("UserGroupMenuLinkID") %>' CommandName="Add" Glyphicon="plus" ToolTip="Add Child" />
					<aspire:AspireLinkButton CausesValidation="False" runat="server" EncryptedCommandArgument='<%# Eval("UserGroupMenuLinkID") %>' CommandName="Up" Glyphicon="menu_up" ToolTip="Move Up" />
					<aspire:AspireLinkButton CausesValidation="False" runat="server" EncryptedCommandArgument='<%# Eval("UserGroupMenuLinkID") %>' CommandName="Down" Glyphicon="menu_down" ToolTip="Move Down" />
					<aspire:AspireLinkButton CausesValidation="False" runat="server" EncryptedCommandArgument='<%# Eval("UserGroupMenuLinkID") %>' CommandName="Delete" Glyphicon="remove" ToolTip="Delete" ConfirmMessage="Are you sure you want to delete?" />
				</td>
			</tr>
		</ItemTemplate>
		<FooterTemplate></tbody></table></FooterTemplate>
	</asp:Repeater>
	<div class="text-center">
		<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="UserGroups.aspx" />
	</div>
</asp:Content>
