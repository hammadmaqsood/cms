﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.UserManagement
{
	[AspirePage("52EEEDBA-72E9-4F43-BADD-E50865BA71E2", UserTypes.Admins, "~/Sys/Admin/UserManagement/UserGroupMenu.aspx", "User Group Menu", true, AspireModules.UserManagement)]
	public partial class UserGroupMenu : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.UserManagement.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_bars);
		public override string PageTitle => "User Group Menu";

		public static string GetPageUrl(int? userGroupID)
		{
			return GetPageUrl<UserGroupMenu>().AttachQueryParam("UserGroupID", userGroupID);
		}

		public static void Redirect(int userGroupID)
		{
			Redirect(GetPageUrl(userGroupID));
		}

		public int? UserGroupID => this.Request.GetParameterValue<int>("UserGroupID");

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.UserGroupID == null)
				{
					Redirect<Dashboard>();
					return;
				}
				var result = BL.Core.UserManagement.Admin.UserGroups.GetUserGroupWithMenuLinks(this.UserGroupID.Value, this.AdminIdentity.LoginSessionGuid);
				if (result == null)
					throw new InvalidOperationException();
				switch (result.Status)
				{
					case BL.Core.UserManagement.Admin.UserGroups.GetUserGroupWithMenuLinksResult.Statuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<UserGroups>();
						return;
					case BL.Core.UserManagement.Admin.UserGroups.GetUserGroupWithMenuLinksResult.Statuses.Success:
						var userGroup = result.UserGroup;
						this.lblGroupName.Text = userGroup.GroupName;
						this.lblInstituteAlias.Text = result.InstituteAlias;
						this.lblUserType.Text = userGroup.UserTypeEnum.ToString();
						this.divInstitute.Visible = userGroup.UserTypeEnum.IsStaff();
						var menuLinks = this.ToMenuLinks(userGroup.UserGroupMenuLinks.ToList());
						this.repeaterMenu.DataBind(menuLinks);
						this.ViewState["UserType"] = userGroup.UserTypeEnum;

						var list = new List<ListItem>();
						foreach (var userGroupMenuLink in userGroup.UserGroupMenuLinks)
						{
							string text;
							if (string.IsNullOrWhiteSpace(userGroupMenuLink.LinkText) && userGroupMenuLink.AspirePageGuid != null)
							{
								var aspirePageAttribute = userGroupMenuLink.AspirePageGuid.Value.TryGetAspirePageAttribute();
								if (aspirePageAttribute != null)
									text = aspirePageAttribute.MenuItemText;
								else
									text = "PageNotFound";
							}
							else
								text = userGroupMenuLink.LinkText;
							list.Add(new ListItem(text, userGroupMenuLink.UserGroupMenuLinkID.ToString()));
						}
						this.ddlParentUserGroupMenuLinkID.DataBind(list, CommonListItems.None);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		private List<MenuLink> ToMenuLinks(List<UserGroupMenuLink> userGroupMenuLinks)
		{
			var menuLinks = new List<MenuLink>();
			this.AddMenuLinks(userGroupMenuLinks, null, menuLinks, 0);
			return menuLinks;
		}

		private void AddMenuLinks(List<UserGroupMenuLink> userGroupMenuLinks, int? parentUserGroupMenuLinkID, List<MenuLink> menuLinks, byte indentLevel)
		{
			byte displayIndex = 0;
			foreach (var userGroupMenuLink in userGroupMenuLinks.Where(m => m.ParentUserGroupMenuLinkID == parentUserGroupMenuLinkID).OrderBy(m => m.DisplayIndex).ToList())
			{
				var menuLink = new MenuLink
				{
					AspirePageGuid = userGroupMenuLink.AspirePageGuid,
					Visible = userGroupMenuLink.Visible,
					DisplayIndex = displayIndex++,
					ParentUserGroupMenuLinkID = userGroupMenuLink.ParentUserGroupMenuLinkID,
					LinkText = userGroupMenuLink.LinkText,
					UserGroupMenuLinkID = userGroupMenuLink.UserGroupMenuLinkID,
					IndentLevel = indentLevel
				};
				if (userGroupMenuLink.AspirePageGuid != null)
				{
					var aspirePageAttribute = userGroupMenuLink.AspirePageGuid.Value.TryGetAspirePageAttribute();
					if (aspirePageAttribute != null)
						menuLink.LinkText = aspirePageAttribute.MenuItemText;
					else
						menuLink.LinkText = "PageNotFound";
				}
				menuLinks.Add(menuLink);
				this.AddMenuLinks(userGroupMenuLinks, userGroupMenuLink.UserGroupMenuLinkID, menuLinks, (byte)(indentLevel + 1));
			}
		}

		private sealed class MenuLink
		{
			public byte IndentLevel { get; set; }
			public int? UserGroupMenuLinkID { get; set; }
			public Guid? AspirePageGuid { get; set; }
			public string LinkText { get; set; }
			public bool Visible { get; set; }
			public byte DisplayIndex { get; set; }
			public int? ParentUserGroupMenuLinkID { get; set; }
		}

		private void FillControlsIfRequired()
		{
			var userType = (UserTypes)this.ViewState["UserType"];
			if (this.ddlAspirePageGuid.Items.Count == 0)
			{
				var userPageAttributes = AspirePageAttributesDictionary.GuidDictionary.Where(g => g.Value.DisplayInList && g.Value.UserTypes.HasFlag(userType)).Select(g => g.Value).OrderBy(g => g.AspireModule.ToFullName()).ThenBy(g => g.MenuItemText);
				this.ddlAspirePageGuid.Items.Clear();
				this.ddlAspirePageGuid.Items.Add(CommonListItems.Select);
				foreach (var aspirePageAttribute in userPageAttributes)
					this.ddlAspirePageGuid.AddItem(aspirePageAttribute.MenuItemText, aspirePageAttribute.AspirePageGuid.ToString(), aspirePageAttribute.AspireModule.ToFullName());
			}
			if (this.rblVisible.Items.Count == 0)
				this.rblVisible.DataBind(CommonListItems.YesNo);
		}

		protected void ddlAspirePageGuid_SelectedIndexChanged(object sender, EventArgs e)
		{
			var aspirePageGuid = this.ddlAspirePageGuid.SelectedValue.ToNullableGuid();
			if (aspirePageGuid == null)
			{
				this.svtbText.AllowNull = false;
				return;
			}
			this.svtbText.AllowNull = true;
		}

		protected void lbtnReset_Click(object sender, EventArgs e)
		{
			if (this.UserGroupID == null)
				return;

			this.AdminIdentity.Demand(AdminPermissions.UserManagement.ManageUserGroups, UserGroupPermission.PermissionValues.Allowed);
			var userType = (UserTypes)this.ViewState["UserType"];
			var userGroupMenuLinks = userType.GetAspireSideBarLinks().ToUserGroupMenuLinks();
			var status = BL.Core.UserManagement.Admin.UserGroups.ResetToDefaultUserGroupMenuLinks(this.UserGroupID.Value, userGroupMenuLinks, this.AdminIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.UserManagement.Admin.UserGroups.ResetToDefaultUserGroupMenuLinksStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<UserGroups>();
					break;
				case BL.Core.UserManagement.Admin.UserGroups.ResetToDefaultUserGroupMenuLinksStatuses.Success:
					this.AddSuccessAlert("Menu links has been reverted to default.");
					Redirect(this.UserGroupID.Value);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void lbtnSaveMenuLink_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			this.AdminIdentity.Demand(AdminPermissions.UserManagement.ManageUserGroups, UserGroupPermission.PermissionValues.Allowed);
			var linkText = this.tbText.Text.TrimAndMakeItNullIfEmpty();
			string linkUrl = null;
			var visible = this.rblVisible.SelectedValue.ToBoolean();
			var aspirePageGuid = this.ddlAspirePageGuid.SelectedValue.ToNullableGuid();
			var parentUserGroupMenuLinkID = this.ddlParentUserGroupMenuLinkID.SelectedValue.ToNullableInt();
			var result = BL.Core.UserManagement.Admin.UserGroups.AddUserGroupMenuLink(this.UserGroupID.Value, aspirePageGuid, linkText, linkUrl, parentUserGroupMenuLinkID, visible, this.AdminIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.UserManagement.Admin.UserGroups.AddUserGroupMenuLinkStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect(this.UserGroupID.Value);
					break;
				case BL.Core.UserManagement.Admin.UserGroups.AddUserGroupMenuLinkStatuses.Success:
					this.AddSuccessAlert("Menu Link has been added.");
					Redirect(this.UserGroupID.Value);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void repeaterMenu_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			if (this.UserGroupID == null)
				return;
			this.AdminIdentity.Demand(AdminPermissions.UserManagement.ManageUserGroups, UserGroupPermission.PermissionValues.Allowed);
			switch (e.CommandName)
			{
				case "Add":
					this.DisplayMenuLinkPopup(e.DecryptedCommandArgument().ToNullableInt());
					break;
				case "ToggleVisible":
					var userGroupMenuLinkID = e.DecryptedCommandArgumentToInt();
					var toggleResult = BL.Core.UserManagement.Admin.UserGroups.ToggleVisibilityUserGroupMenuLink(userGroupMenuLinkID, this.AdminIdentity.LoginSessionGuid);
					switch (toggleResult)
					{
						case BL.Core.UserManagement.Admin.UserGroups.ToggleVisibilityUserGroupMenuLinkStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.UserManagement.Admin.UserGroups.ToggleVisibilityUserGroupMenuLinkStatuses.Success:
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					Redirect(this.UserGroupID.Value);
					break;
				case "Up":
					userGroupMenuLinkID = e.DecryptedCommandArgumentToInt();
					var result = BL.Core.UserManagement.Admin.UserGroups.MoveUpUserGroupMenuLink(userGroupMenuLinkID, this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case BL.Core.UserManagement.Admin.UserGroups.MoveUserGroupMenuLinkStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.UserManagement.Admin.UserGroups.MoveUserGroupMenuLinkStatuses.AlreadyAtTop:
							this.AddInfoAlert("Menu Link is already at top.");
							break;
						case BL.Core.UserManagement.Admin.UserGroups.MoveUserGroupMenuLinkStatuses.Success:
							break;
						case BL.Core.UserManagement.Admin.UserGroups.MoveUserGroupMenuLinkStatuses.AlreadyAtBottom:
							this.AddInfoAlert("Menu Link is already at bottom.");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					Redirect(this.UserGroupID.Value);
					break;
				case "Down":
					userGroupMenuLinkID = e.DecryptedCommandArgumentToInt();
					result = BL.Core.UserManagement.Admin.UserGroups.MoveDownUserGroupMenuLink(userGroupMenuLinkID, this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case BL.Core.UserManagement.Admin.UserGroups.MoveUserGroupMenuLinkStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.UserManagement.Admin.UserGroups.MoveUserGroupMenuLinkStatuses.AlreadyAtTop:
							this.AddInfoAlert("Menu Link is already at top.");
							break;
						case BL.Core.UserManagement.Admin.UserGroups.MoveUserGroupMenuLinkStatuses.Success:
							break;
						case BL.Core.UserManagement.Admin.UserGroups.MoveUserGroupMenuLinkStatuses.AlreadyAtBottom:
							this.AddInfoAlert("Menu Link is already at bottom.");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					Redirect(this.UserGroupID.Value);
					break;
				case "Delete":
					userGroupMenuLinkID = e.DecryptedCommandArgumentToInt();
					var deletedStatus = BL.Core.UserManagement.Admin.UserGroups.DeleteUserGroupMenuLink(userGroupMenuLinkID, this.AdminIdentity.LoginSessionGuid);
					switch (deletedStatus)
					{
						case BL.Core.UserManagement.Admin.UserGroups.DeleteUserGroupMenuLinkStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.UserManagement.Admin.UserGroups.DeleteUserGroupMenuLinkStatuses.CannotDeleteChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Menu Link");
							break;
						case BL.Core.UserManagement.Admin.UserGroups.DeleteUserGroupMenuLinkStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Menu Link");
							break;
						default:
							throw new NotImplementedEnumException(deletedStatus);
					}
					Redirect(this.UserGroupID.Value);
					break;
			}
		}

		private void DisplayMenuLinkPopup(int? parentUserGroupMenuLinkID)
		{
			this.FillControlsIfRequired();
			this.ddlAspirePageGuid.ClearSelection();
			this.ddlAspirePageGuid_SelectedIndexChanged(null, null);
			this.rblVisible.SelectedIndex = 0;
			if (parentUserGroupMenuLinkID == null)
				this.ddlParentUserGroupMenuLinkID.ClearSelection();
			else
				this.ddlParentUserGroupMenuLinkID.SelectedValue = parentUserGroupMenuLinkID.ToString();
			this.modalMenuLink.Show();
		}

		protected void btnAddMenuLink_Click(object sender, EventArgs e)
		{
			this.DisplayMenuLinkPopup(null);
		}
	}
}