﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.UserManagement
{
	[AspirePage("ACCAB953-28D4-4FE1-B599-0855334CB1A1", UserTypes.Admins, "~/Sys/Admin/UserManagement/UserGroups.aspx", "User Groups", true, AspireModules.UserManagement)]
	public partial class UserGroups : AdminsPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_users);
		public override string PageTitle => "User Groups Report";

		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.UserManagement.ManageUserGroups, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.AdminIdentity.UserType.IsMasterAdmin())
					this.ddlUserType.AddEnums(CommonListItems.All, UserTypes.Admin, UserTypes.Staff, UserTypes.Executive);
				else
					this.ddlUserType.AddEnums(UserTypes.Staff);

				this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.All);
				this.ViewState.SetSortExpression("GroupName");
				this.ViewState.SetSortDirection(SortDirection.Ascending);
				this.lbtnSearch_Click(null, null);
			}
		}

		protected void ddlInstituteID_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlUserType_SelectedIndexChanged(null, null);
		}

		protected void ddlUserType_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.lbtnSearch_Click(null, null);
		}

		protected void lbtnSearch_Click(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			var searchText = this.tbSearch.Text;
			var userTypeEnum = this.ddlUserType.GetSelectedEnumValue<UserTypes>(null);
			var userGroups = Aspire.BL.Core.UserManagement.Admin.UserGroups.GetUserGroups(this.ddlInstituteID.SelectedValue.ToNullableByte(), userTypeEnum, searchText, pageIndex, this.gv.PageSize, out int virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.AdminIdentity.LoginSessionGuid);
			this.gv.DataBind(userGroups, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		protected void gv_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(this.gv.PageIndex);
		}

		protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gv_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					this.AdminIdentity.Demand(AdminPermissions.UserManagement.ManageUserGroups, UserGroupPermission.PermissionValues.Allowed);
					e.Handled = true;
					var userGroupID = e.DecryptedCommandArgumentToInt();
					var result = Aspire.BL.Core.UserManagement.Admin.UserGroups.DeleteUserGroup(userGroupID, this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case BL.Core.UserManagement.Admin.UserGroups.DeleteUserGroupStatus.NoRecordFound:
							this.AddNoRecordFoundAlert();
							Redirect<UserGroups>();
							break;
						case BL.Core.UserManagement.Admin.UserGroups.DeleteUserGroupStatus.CannotDeleteChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("User Group");
							break;
						case BL.Core.UserManagement.Admin.UserGroups.DeleteUserGroupStatus.Success:
							this.AddSuccessMessageHasBeenDeleted("User Group");
							Redirect<UserGroups>();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		protected void gv_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gv.PageSize = e.NewPageSize;
			this.GetData(0);
		}
	}
}