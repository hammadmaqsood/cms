﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="IntegratedServiceUsers.aspx.cs" Inherits="Aspire.Web.Sys.Admin.UserManagement.IntegratedServiceUsers" %>

<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Import Namespace="Aspire.BL.Core.UserManagement.Admin" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<aspire:AspireButton runat="server" ID="btnAdd" ButtonType="Success" Text="Add" CausesValidation="False" Glyphicon="plus" OnClick="btnAdd_OnClick" />
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvIntegratedServiceUsers" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gvIntegratedServiceUsers_OnPageIndexChanging" OnPageSizeChanging="gvIntegratedServiceUsers_OnPageSizeChanging" OnSorting="gvIntegratedServiceUsers_OnSorting" OnRowCommand="gvIntegratedServiceUsers_OnRowCommand">
		<Columns>
			<asp:BoundField HeaderText="Institute" DataField="Institute" SortExpression="InstituteAlias" />
			<asp:BoundField HeaderText="Service Name" DataField="ServiceName" SortExpression="ServiceName" />
			<asp:BoundField HeaderText="Service Type" DataField="ServiceTypeFullName" SortExpression="ServiceType" />
			<asp:BoundField HeaderText="Username" DataField="UserName" SortExpression="UserName" />
			<asp:BoundField HeaderText="IP Address" DataField="IPAddress" SortExpression="IPAddress" />
			<asp:BoundField HeaderText="Created Date" DataField="CreatedDate" SortExpression="CreatedDate" DataFormatString="{0:d}" />
			<asp:BoundField HeaderText="Expiry Date" DataField="ExpiryDate" SortExpression="ExpiryDate" DataFormatString="{0:d}" />
			<asp:TemplateField HeaderText="Status" SortExpression="Status">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CommandName="Status" ConfirmMessage="Are you sure you want to change status?" CausesValidation="False" EncryptedCommandArgument='<%# Eval(nameof(IntegratedServiceUser.IntegratedServiceUserID)) %>' Text='<%# Eval(nameof(IntegratedServiceUsers.IntegratedServiceUser.StatusFullName))%>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" Glyphicon="edit" CommandName="Edit" CausesValidation="False" EncryptedCommandArgument='<%# Eval(nameof(IntegratedServiceUser.IntegratedServiceUserID)) %>' />
					<aspire:AspireLinkButton runat="server" Glyphicon="remove" CommandName="Delete" CausesValidation="False" EncryptedCommandArgument='<%# Eval(nameof(IntegratedServiceUser.IntegratedServiceUserID)) %>' ConfirmMessage='<%# Eval(nameof(IntegratedServiceUser.ServiceName),"Are you sure you want to delete \"{0}\"?") %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalIntegratedServiceUsers">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updateModalmodalIntegratedServiceUsers" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertModalmodalIntegratedServiceUsers" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Service Name:" AssociatedControlID="tbServiceName" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbServiceName" MaxLength="50" ValidationGroup="Required" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbServiceName" ErrorMessage="This field is required." ValidationGroup="Required" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Service Type:" AssociatedControlID="ddlServiceType" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlServiceType"  />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlServiceType" ErrorMessage="This field is required." ValidationGroup="Required" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" ValidationGroup="Required" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Username:" AssociatedControlID="tbUsername" CssClass="col-md-3" />
							<div class="col-md-9">
								<div class="input-group">
									<aspire:AspireTextBox runat="server" ID="tbUsername" MaxLength="50" AutoCompleteType="Disabled" ValidationGroup="Required" ReadOnly="True" />
									<div class="input-group-btn">
										<aspire:AspireButton runat="server" CausesValidation="False" ID="btnGenerateUsername" Text="New" ButtonType="Primary" OnClick="btnGenerateUsername_OnClick" />
									</div>
								</div>
								<aspire:AspireStringValidator ValidationExpression="GUID" AllowNull="False" runat="server" ControlToValidate="tbUserName" ErrorMessage="This field is required." InvalidDataErrorMessage="Invalid GUID." ValidationGroup="Required" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Password:" AssociatedControlID="tbPassword" CssClass="col-md-3" />
							<div class="col-md-9">
								<div class="input-group">
									<aspire:AspireTextBox runat="server" ID="tbPassword" MaxLength="50" AutoCompleteType="Disabled" ValidationGroup="Required" />
									<div class="input-group-btn">
										<aspire:AspireButton runat="server" ID="btnGeneratePassword" CausesValidation="False" Text="New" ButtonType="Primary" OnClick="btnGeneratePassword_OnClick" />
									</div>
								</div>
								<aspire:AspireStringValidator ID="svtbPassword" ValidationExpression="GUID" AllowNull="True" runat="server" ControlToValidate="tbPassword" ErrorMessage="This field is required." InvalidDataErrorMessage="Invalid GUID." ValidationGroup="Required" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="IP Address:" AssociatedControlID="tbIPAddress" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox data-inputmask="'alias': 'ip'" runat="server" ID="tbIPAddress" MaxLength="15" ValidationGroup="Required" />
								<aspire:AspireStringValidator ValidationExpression="IPV4" AllowNull="False" runat="server" ControlToValidate="tbIPAddress" ErrorMessage="This field is required." ValidationGroup="Required" InvalidDataErrorMessage="Invalid IP Address" RequiredErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Expiry Date:" AssociatedControlID="dtpExpiryDate" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpExpiryDate" ValidationGroup="Required" />
								<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dtpExpiryDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." AllowNull="False" ValidationGroup="Required" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="rblStatus" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireRadioButtonList runat="server" ID="rblStatus" CausesValidation="False" RepeatDirection="Horizontal" RepeatLayout="Flow" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" ValidationGroup="Required" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
