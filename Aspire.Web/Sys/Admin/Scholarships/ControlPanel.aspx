﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ControlPanel.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Scholarships.ControlPanel" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<aspire:AspireButton runat="server" ID="btnAdd" ButtonType="Success" Glyphicon="plus" Text="Add" OnClick="btnAdd_OnClick" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlInstituteIDFilter" AutoPostBack="True" ValidationGroup="Filter" OnSelectedIndexChanged="ddlInstituteIDFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" AutoPostBack="True" ValidationGroup="Filter" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Scholarship:" AssociatedControlID="ddlScholarshipTypeFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlScholarshipTypeFilter" AutoPostBack="True" ValidationGroup="Filter" OnSelectedIndexChanged="ddlScholarshipTypeFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatusFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlStatusFilter" AutoPostBack="True" ValidationGroup="Filter" OnSelectedIndexChanged="ddlStatusFilter_OnSelectedIndexChanged" />
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ItemType="Aspire.BL.Core.Scholarship.Admin.ScholarshipAnnouncements.ScholarshipAnnouncement" ID="gvControlPanel" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnRowCommand="gvControlPanel_OnRowCommand" OnSorting="gvControlPanel_OnSorting" OnPageIndexChanging="gvControlPanel_OnPageIndexChanging" OnPageSizeChanging="gvControlPanel_OnPageSizeChanging">
		<Columns>
			<asp:BoundField HeaderText="Institute" SortExpression="InstituteAlias" DataField="InstituteAlias" />
			<asp:BoundField HeaderText="Scholarship" SortExpression="ScholarshipTypeFullName" DataField="ScholarshipTypeFullName" />
			<asp:BoundField HeaderText="Semester" SortExpression="Semester" DataField="Semester" />
			<asp:BoundField HeaderText="Start Date" SortExpression="StartDate" DataField="StartDate" DataFormatString="{0:d}" />
			<asp:TemplateField HeaderText="End Date" SortExpression="EndDate">
				<ItemTemplate>
					<%# (Item.EndDate?.ToString("d")).ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Status" SortExpression="StatusFullName">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CommandName="Status" CausesValidation="False" EncryptedCommandArgument='<%#this.Eval("ScholarshipAnnouncementID")%>' Text='<%#this.Eval("StatusFullName") %>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" ToolTip="Edit" CausesValidation="False" CommandName="Edit" Glyphicon="edit" EncryptedCommandArgument='<%#this.Eval("ScholarshipAnnouncementID")%>' />
					<aspire:AspireLinkButton runat="server" ToolTip="Delete" CausesValidation="False" CommandName="Delete" Glyphicon="remove" EncryptedCommandArgument='<%#this.Eval("ScholarshipAnnouncementID")%>' ConfirmMessage='<%#this.Eval("ScholarshipTypeFullName","Are you sure you want to delete scholarship of {0}?") %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalControlPanel">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updateModalControlPanel" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertModalControlPanel" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
						<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" ValidationGroup="Save" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteID_OnSelectedIndexChanged" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlInstituteID" ErrorMessage="This field is required." ValidationGroup="Save" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Scholarship:" AssociatedControlID="ddlScholarshipType" />
						<aspire:AspireDropDownList runat="server" ID="ddlScholarshipType" ValidationGroup="Save" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlScholarshipType" ErrorMessage="This field is required." ValidationGroup="Save" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
						<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="Save" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterID" ErrorMessage="This field is required." ValidationGroup="Save" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Start Date:" AssociatedControlID="dtpStartDate" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpStartDate" ValidationGroup="Save" />
						<aspire:AspireDateTimeValidator AllowNull="False" runat="server" ControlToValidate="dtpStartDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid date." ValidationGroup="Save" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="End Date:" AssociatedControlID="dtpEndDate" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpEndDate" ValidationGroup="Save" />
						<aspire:AspireDateTimeValidator AllowNull="true" runat="server" ControlToValidate="dtpEndDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid date." ValidationGroup="Save" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="rblStatus" />
						<div>
							<aspire:AspireRadioButtonList runat="server" ID="rblStatus" ValidationGroup="Save" RepeatLayout="Flow" RepeatDirection="Horizontal" />
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" OnClick="btnSave_OnClick" ValidationGroup="Save" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
