﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Scholarships
{
	[AspirePage("4F160C4C-91ED-40FE-BDBC-D1A019ABA51B", UserTypes.Admins, "~/Sys/Admin/Scholarships/ControlPanel.aspx", "Control Panel", true, AspireModules.Scholarship)]
	public partial class ControlPanel : AdminsPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.cog);
		public override string PageTitle => "Control Panel: Scholarship Announcement";

		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.Scholarship.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public void Redirect(int? instituteID, short? semesterID, Scholarship.ScholarshipTypes? scholarshipTypeEnum, ScholarshipAnnouncement.Statuses? statusEnum, SortDirection? sortDirection, string sortExpression, int? pageIndex, int? pageSize)
		{
			Redirect<ControlPanel>(nameof(Scholarship.InstituteID), instituteID,
				nameof(ScholarshipAnnouncement.SemesterID), semesterID,
				nameof(Scholarship.ScholarshipType), scholarshipTypeEnum,
				nameof(ScholarshipAnnouncement.Status), statusEnum,
				nameof(SortDirection), sortDirection,
				"SortExpression", sortExpression,
				"PageIndex", pageIndex,
				"PageSize", pageSize
			);
		}
		private void RefreshPage()
		{
			this.Redirect(this.ddlInstituteIDFilter.SelectedValue.ToNullableInt(),
				this.ddlSemesterIDFilter.SelectedValue.ToNullableShort(),
				this.ddlScholarshipTypeFilter.GetSelectedEnumValue<Scholarship.ScholarshipTypes>(null),
				this.ddlStatusFilter.GetSelectedEnumValue<ScholarshipAnnouncement.Statuses>(null),
				this.ViewState.GetSortDirection(),
				this.ViewState.GetSortExpression(),
				this.gvControlPanel.PageIndex,
				this.gvControlPanel.PageSize);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortDirection(this.Request.GetParameterValue<SortDirection>(nameof(SortDirection)) ?? SortDirection.Ascending);
				this.ViewState.SetSortExpression(this.Request.GetParameterValue("SortExpression") ?? nameof(Institute.InstituteAlias));
				var pageSize = this.GetParameterValue<int>("PageSize");
				if (pageSize != null)
					this.gvControlPanel.PageSize = pageSize.Value;
				this.ddlInstituteIDFilter.FillInstitutesForAdmin(CommonListItems.All).SetSelectedValueIfNotPostback(nameof(Scholarship.InstituteID));
				this.ddlScholarshipTypeFilter.FillScholarshipTypes(CommonListItems.All).SetSelectedValueIfNotPostback(nameof(Scholarship.ScholarshipType));
				this.ddlSemesterIDFilter.FillSemesters(CommonListItems.All).SetSelectedValueIfNotPostback(nameof(ScholarshipAnnouncement.SemesterID));
				this.ddlStatusFilter.FillScholarshipStatuses(CommonListItems.All).SetSelectedValueIfNotPostback(nameof(ScholarshipAnnouncement.Status));
				this.ddlInstituteIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlInstituteIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlScholarshipTypeFilter_OnSelectedIndexChanged(null, null);
		}
		protected void ddlScholarshipTypeFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlStatusFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlStatusFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
				this.GetData(this.GetParameterValue<int>("PageIndex") ?? 0);
			else
				this.GetData(0);
		}

		protected void gvControlPanel_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Status":
					var scholarshipAnnouncement = BL.Core.Scholarship.Admin.ScholarshipAnnouncements.ToggleScholarshipAnnouncementStatus(e.DecryptedCommandArgumentToInt(), this.AdminIdentity.LoginSessionGuid);
					if (scholarshipAnnouncement == null)
					{
						this.AddNoRecordFoundAlert();
						Redirect<ControlPanel>();
						return;
					}
					this.GetData(this.gvControlPanel.PageIndex);
					break;
				case "Edit":
					e.Handled = true;
					this.DisplayControlPanelModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var resultDelete = BL.Core.Scholarship.Admin.ScholarshipAnnouncements.DeleteScholarshipAnnouncement(e.DecryptedCommandArgumentToInt(), this.AdminIdentity.LoginSessionGuid);
					switch (resultDelete)
					{
						case BL.Core.Scholarship.Admin.ScholarshipAnnouncements.DeleteScholarshipAnnouncementStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.Scholarship.Admin.ScholarshipAnnouncements.DeleteScholarshipAnnouncementStatuses.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Scholarship Announcement");
							break;
						case BL.Core.Scholarship.Admin.ScholarshipAnnouncements.DeleteScholarshipAnnouncementStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Scholarship Announcement");
							break;
						default:
							throw new ArgumentOutOfRangeException(nameof(resultDelete), resultDelete, null);
					}

					Redirect<ControlPanel>();
					break;
			}
		}

		protected void gvControlPanel_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvControlPanel_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvControlPanel_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvControlPanel.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void btnAdd_OnClick(object sender, EventArgs e)
		{
			this.DisplayControlPanelModal(null);
		}

		private int? ScholarshipAnnouncementID
		{
			get => (int?)this.ViewState[nameof(this.ScholarshipAnnouncementID)];
			set => this.ViewState[nameof(this.ScholarshipAnnouncementID)] = value;
		}

		private void DisplayControlPanelModal(int? scholarshipAnnouncementID)
		{
			this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.Select);
			this.ddlSemesterID.FillSemesters();
			this.rblStatus.FillEnums<Scholarship.Statuses>();
			
			if (scholarshipAnnouncementID == null)
			{
				this.ViewState["Modal"] = "Add";
				this.ddlInstituteID_OnSelectedIndexChanged(null, null);
				this.ddlInstituteID.Enabled = true;
				this.ddlSemesterID.Enabled = true;
				this.ddlScholarshipType.Enabled = true;
				this.dtpStartDate.SelectedDate = null;
				this.dtpEndDate.SelectedDate = null;
				this.rblStatus.SetEnumValue(Scholarship.Statuses.Inactive);

				this.modalControlPanel.HeaderText = "Add Scholarship Announcement";
				this.ScholarshipAnnouncementID = null;
			}
			else
			{
				var scholarshipAnnouncement = BL.Core.Scholarship.Admin.ScholarshipAnnouncements.GetScholarshipAnnouncement(scholarshipAnnouncementID.Value, this.AdminIdentity.LoginSessionGuid);
				if (scholarshipAnnouncement == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<ControlPanel>();
					return;
				}

				this.ViewState["Modal"] = "Edit";
				this.ddlInstituteID.SelectedValue = scholarshipAnnouncement.InstituteID.ToString();
				this.ddlInstituteID_OnSelectedIndexChanged(null, null);
				this.ddlInstituteID.Enabled = false;
				this.ddlSemesterID.SelectedValue = scholarshipAnnouncement.SemesterID.ToString();
				this.ddlSemesterID.Enabled = false;
				this.ddlScholarshipType.SelectedValue = scholarshipAnnouncement.ScholarshipID.ToString(); //----
				this.ddlScholarshipType.Enabled = false;
				this.dtpStartDate.SelectedDate = scholarshipAnnouncement.StartDate;
				this.dtpEndDate.SelectedDate = scholarshipAnnouncement.EndDate;
				this.rblStatus.SetEnumValue(scholarshipAnnouncement.StatusEnum);
				this.modalControlPanel.HeaderText = "Edit Scholarship Announcement";
				this.ScholarshipAnnouncementID = scholarshipAnnouncementID;
			}
			this.modalControlPanel.Show();
		}

		protected void ddlInstituteID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var modal = this.ViewState["Modal"];
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();

			if (modal.ToString() == "Add")
			{
				if (instituteID == null)
					this.ddlScholarshipType.DataBind(CommonListItems.Select);
				else
					this.ddlScholarshipType.FillScholarshipTypes(CommonListItems.Select).SetSelectedValueIfNotPostback(nameof(Scholarship.ScholarshipType));
			}
			else
			{
				var scholarships = BL.Core.Scholarship.Admin.ScholarshipAnnouncements.GetScholarships(instituteID.Value, this.AdminIdentity.LoginSessionGuid);
				this.ddlScholarshipType.DataBind(scholarships, CommonListItems.Select);
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;

			var scholarshipAnnouncementID = this.ScholarshipAnnouncementID;
			var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			//var scholarshipID = this.ddlScholarshipType.SelectedValue.ToInt();
			var startDate = this.dtpStartDate.SelectedDate ?? throw new InvalidOperationException();
			var endDate = this.dtpEndDate.SelectedDate;
			var status = this.rblStatus.SelectedValue.ToEnum<ScholarshipAnnouncement.Statuses>();
			var scholarshipTypeEnum = (Model.Entities.Scholarship.ScholarshipTypes)this.ddlScholarshipType.SelectedValue.ToInt();

			if (scholarshipAnnouncementID == null)
			{
				//BL.Core.Scholarship.Admin.Scholarships.AddScholarship(instituteID, scholarshipType, this.AdminIdentity.LoginSessionGuid);
				var resultAdd = BL.Core.Scholarship.Admin.ScholarshipAnnouncements.AddScholarshipAnnouncement(instituteID, scholarshipTypeEnum, semesterID, startDate, endDate, status, this.AdminIdentity.LoginSessionGuid);
				switch (resultAdd)
				{
					case BL.Core.Scholarship.Admin.ScholarshipAnnouncements.AddScholarshipAnnouncementResults.AlreadyExists:
						this.alertModalControlPanel.AddErrorAlert("Scholarship already exists for specified institute and semester.");
						this.updateModalControlPanel.Update();
						return;
					case BL.Core.Scholarship.Admin.ScholarshipAnnouncements.AddScholarshipAnnouncementResults.Success:
						this.AddSuccessMessageHasBeenAdded("Scholarship Announcement");
						this.RefreshPage();
						return;
					default:
						throw new ArgumentOutOfRangeException(nameof(resultAdd), resultAdd, null);
				}
			}
			else
			{
				var resultUpdate = BL.Core.Scholarship.Admin.ScholarshipAnnouncements.UpdateScholarshipAnnouncement(scholarshipAnnouncementID.Value, startDate, endDate, status, this.AdminIdentity.LoginSessionGuid);

				switch (resultUpdate)
				{
					case BL.Core.Scholarship.Admin.ScholarshipAnnouncements.UpdateScholarshipAnnouncementResults.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<ControlPanel>();
						return;
					case BL.Core.Scholarship.Admin.ScholarshipAnnouncements.UpdateScholarshipAnnouncementResults.Success:
						this.AddSuccessMessageHasBeenUpdated("Scholarship Announcement");
						this.RefreshPage();
						return;
					default:
						throw new ArgumentOutOfRangeException(nameof(resultUpdate), resultUpdate, null);
				}
			}
		}

		private void GetData(int pageIndex)
		{
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToNullableInt();
			var scholarshipTypeEnum = this.ddlScholarshipTypeFilter.GetSelectedEnumValue<Scholarship.ScholarshipTypes>(null);
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var status = this.ddlStatusFilter.GetSelectedEnumValue<Scholarship.Statuses>(null);
			var scholarshipAnnouncement = BL.Core.Scholarship.Admin.ScholarshipAnnouncements.GetScholarshipAnnouncements(instituteID, scholarshipTypeEnum, semesterID, status, pageIndex, this.gvControlPanel.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out var virtualItemCount, this.AdminIdentity.LoginSessionGuid);
			this.gvControlPanel.DataBind(scholarshipAnnouncement, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}
	}
}