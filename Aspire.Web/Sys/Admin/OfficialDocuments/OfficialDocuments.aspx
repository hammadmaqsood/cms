﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="OfficialDocuments.aspx.cs" Inherits="Aspire.Web.Sys.Admin.OfficialDocuments.OfficialDocuments" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel Text="&nbsp;" runat="server" AssociatedControlID="btnAdd" />
				<div>
					<aspire:AspireButton runat="server" ID="btnAdd" Text="Add" CausesValidation="False" Glyphicon="plus" ButtonType="Success" OnClick="btnAdd_Click" />
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlInstituteIDFilter" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteIDFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="User Type:" AssociatedControlID="ddlUserTypeFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlUserTypeFilter" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlUserTypeFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatusFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlStatusFilter" ValidationGroup="Filters" AutoPostBack="true" OnSelectedIndexChanged="ddlStatusFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Search:" AssociatedControlID="tbSearchText" />
				<asp:Panel runat="server" CssClass="input-group" DefaultButton="btnSearch">
					<aspire:AspireTextBox runat="server" ID="tbSearchText" ValidationGroup="Filters" />
					<div class="input-group-btn">
						<aspire:AspireButton ButtonType="Default" Glyphicon="search" runat="server" ID="btnSearch" ValidationGroup="Search" OnClick="btnSearch_Click" />
					</div>
				</asp:Panel>
			</div>
		</div>
	</div>
	<aspire:AspireGridView ItemType="Aspire.BL.Core.OfficialDocuments.Admin.Documents.OfficialDocument" ID="gvDocuments" runat="server" AllowSorting="True" AllowPaging="True" AutoGenerateColumns="False" OnRowCommand="gvDocuments_RowCommand" OnSorting="gvDocuments_Sorting" OnPageIndexChanging="gvDocuments_PageIndexChanging" OnPageSizeChanging="gvDocuments_PageSizeChanging">
		<Columns>
			<asp:TemplateField HeaderText="Institute" SortExpression="InstituteAlias">
				<ItemTemplate>
					<%#: Item.InstituteID == null ? "All" : Item.InstituteAlias %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="User Types" SortExpression="UserTypeFlags">
				<ItemTemplate>
					<%#: Item.UserTypeFlags.ToFullNames() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Document Name" SortExpression="DocumentName">
				<ItemTemplate>
					<aspire:AspireLinkButton Text="<%# Item.DocumentName %>" ToolTip="Click to Download File" runat="server" CausesValidation="False" EncryptedCommandArgument="<%# Item.OfficialDocumentID %>" CommandName="Download" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Provided By" SortExpression="ProvidedBy">
				<ItemTemplate>
					<%#: Item.ProvidedBy %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Uploaded On" SortExpression="UploadedOn">
				<ItemTemplate>
					<%#: $"{Item.UploadedOn:G}" %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Status" SortExpression="Status">
				<ItemTemplate>
					<%#: Item.Status.GetFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireLinkButton Glyphicon="edit" runat="server" CausesValidation="False" CommandName="Edit" EncryptedCommandArgument="<%# Item.OfficialDocumentID %>" />
					<aspire:AspireLinkButton Glyphicon="remove" runat="server" CausesValidation="False" CommandName="Delete" EncryptedCommandArgument="<%# Item.OfficialDocumentID %>" ConfirmMessage="Are you sure you want to delete?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<aspire:AspireModal runat="server" ID="modalDocument" Visible="false">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelModalDocument" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="modalAlert" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
						<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" ValidationGroup="Document" />
						<aspire:AspireRequiredFieldValidator runat="server" ID="rfvddlInstituteID" ControlToValidate="ddlInstituteID" ValidationGroup="Document" ErrorMessage="This field is required." />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="User Types:" AssociatedControlID="lbUserTypeFlags" />
						<aspire:AspireListBox SelectionMode="Multiple" runat="server" ID="lbUserTypeFlags" ValidationGroup="Document" />
						<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ControlToValidate="lbUserTypeFlags" ValidationGroup="Document" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Document Name:" AssociatedControlID="tbDocumentName" />
						<aspire:AspireTextBox runat="server" ID="tbDocumentName" MaxLength="255" ValidationGroup="Document" />
						<aspire:AspireStringValidator AllowNull="False" runat="server" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Document Name is invalid." ControlToValidate="tbDocumentName" ValidationGroup="Document" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Provided By:" AssociatedControlID="tbProvidedBy" />
						<aspire:AspireTextBox runat="server" ID="tbProvidedBy" MaxLength="1000" ValidationGroup="Document" />
						<aspire:AspireStringValidator AllowNull="False" runat="server" RequiredErrorMessage="This field is required." ControlToValidate="tbProvidedBy" ValidationGroup="Document" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatus" />
						<aspire:AspireDropDownList runat="server" ID="ddlStatus" ValidationGroup="Document" />
						<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ControlToValidate="ddlStatus" ValidationGroup="Document" />
					</div>
					<div class="form-group" runat="server" id="divFileUpload">
						<aspire:AspireLabel runat="server" Text="File:" AssociatedControlID="fileUpload" />
						<div>
							<aspire:AspireFileUpload runat="server" ID="fileUpload" AllowedFileTypes=".pdf" MaxFileSize="52428800" MaxNumberOfFiles="1" />
						</div>
						<aspire:AspireFileUploadValidator runat="server" AspireFileUploadID="fileUpload" ValidationGroup="Document" RequiredErrorMessage="File is not yet uploaded." UploadingInProgressMessage="File upload is in progress." />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelModalDocumentFooter" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="Document" Text="Save" OnClick="btnSave_Click" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
	<script type="text/javascript">
		$(function () {
			var init = function () {
				$("#<%=this.lbUserTypeFlags.ClientID%>").applySelect2();
			};
			init();
			Sys.Application.add_load(init);
		});
	</script>
</asp:Content>
