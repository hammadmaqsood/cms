﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.OfficialDocuments
{
	[AspirePage("03961DCC-4006-4D4A-8532-F263272311C7", UserTypes.MasterAdmin, "~/Sys/Admin/OfficialDocuments/OfficialDocuments.aspx", "Official Documents", true, AspireModules.OfficialDocuments)]
	public partial class OfficialDocuments : MasterAdminPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_book);
		public override string PageTitle => "Official Documents";

		public static string GetPageUrl(UserTypes? userTypeEnum, int? instituteID, Model.Entities.OfficialDocument.Statuses? statusEnum, string searchText, SortDirection sortDirection, string sortExpression, int? pageIndex, int? pageSize)
		{
			return GetPageUrl<OfficialDocuments>().AttachQueryParams(
				(nameof(UserType), userTypeEnum),
				(nameof(InstituteID), instituteID),
				(nameof(Status), statusEnum),
				(nameof(SearchText), searchText),
				(nameof(SortDirection), sortDirection),
				(nameof(SortExpression), sortExpression),
				(nameof(PageIndex), pageIndex),
				(nameof(PageSize), pageSize));
		}

		public static void Redirect(UserTypes? userTypeEnum, int? instituteID, Model.Entities.OfficialDocument.Statuses? statusEnum, string searchText, SortDirection sortDirection, string sortExpression, int pageIndex, int pageSize)
		{
			Redirect(GetPageUrl(userTypeEnum, instituteID, statusEnum, searchText, sortDirection, sortExpression, pageIndex, pageSize));
		}

		private UserTypes? UserType => this.GetParameterValue<UserTypes>(nameof(this.UserType));
		private Model.Entities.OfficialDocument.Statuses? Status => this.GetParameterValue<Model.Entities.OfficialDocument.Statuses>(nameof(this.Status));
		private int? InstituteID => this.GetParameterValue<int>(nameof(this.InstituteID));
		private string SearchText => this.GetParameterValue(nameof(this.SearchText));
		private SortDirection? SortDirection => this.GetParameterValue<SortDirection>(nameof(this.SortDirection));
		private string SortExpression => this.GetParameterValue(nameof(this.SortExpression));
		private int? PageIndex => this.GetParameterValue<int>(nameof(this.PageIndex));
		private int? PageSize => this.GetParameterValue<int>(nameof(this.PageSize));

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(nameof(BL.Core.OfficialDocuments.Admin.Documents.OfficialDocument.UploadedOn), System.Web.UI.WebControls.SortDirection.Descending);
				var userTypes = BL.Core.OfficialDocuments.Admin.Documents.ValidUserTypes.Select(t => new CustomListItem { ID = (short)t, Text = t.ToFullName() }).OrderBy(t => t.Text);
				this.ddlUserTypeFilter.DataBind(userTypes, CommonListItems.All);
				this.ddlInstituteIDFilter.FillInstitutesForAdmin(CommonListItems.All);
				this.ddlStatusFilter.FillGuidEnums<Model.Entities.OfficialDocument.Statuses>(CommonListItems.All);

				if (this.InstituteID != null)
					this.ddlInstituteIDFilter.SelectedValue = this.InstituteID.Value.ToString();
				this.ddlInstituteIDFilter_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlInstituteIDFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (!this.IsPostBack && this.UserType != null)
				this.ddlUserTypeFilter.SelectedValue = this.UserType.Value.ToString();
			this.ddlUserTypeFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlUserTypeFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (!this.IsPostBack && !string.IsNullOrWhiteSpace(this.SearchText))
				this.tbSearchText.Text = this.SearchText.Trim();
			this.ddlStatusFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlStatusFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearch_Click(null, null);
		}

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var pageIndex = this.PageIndex ?? 0;
				if (this.PageSize != null)
					this.gvDocuments.PageSize = this.PageSize.Value;
				if (this.SortDirection != null)
					this.ViewState.SetSortDirection(this.SortDirection.Value);
				if (this.SortExpression != null)
					this.ViewState.SetSortExpression(this.SortExpression);
				this.GetData(pageIndex);
			}
			else
				this.GetData(0);
		}

		protected void btnAdd_Click(object sender, EventArgs e)
		{
			this.ShowModal(null);
		}

		protected void gvDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.ShowModal(e.DecryptedCommandArgumentToInt());
					return;
				case "Delete":
					{
						e.Handled = true;
						var officialDocumentID = e.DecryptedCommandArgumentToInt();
						var deleteStatus = BL.Core.OfficialDocuments.Admin.Documents.DeleteOfficialDocument(officialDocumentID, this.AdminIdentity.LoginSessionGuid);
						switch (deleteStatus)
						{
							case BL.Core.OfficialDocuments.Admin.Documents.DeleteOfficialDocumentStatuses.NoRecordFound:
								this.AddNoRecordFoundAlert();
								this.RefreshPage();
								return;
							case BL.Core.OfficialDocuments.Admin.Documents.DeleteOfficialDocumentStatuses.Success:
								this.AddSuccessMessageHasBeenDeleted("Document");
								this.RefreshPage();
								return;
							default:
								throw new NotImplementedEnumException(deleteStatus);
						}
					}
				case "Download":
					{
						var officialDocumentID = e.DecryptedCommandArgumentToInt();
						var result = BL.Core.OfficialDocuments.Admin.Documents.ReadOfficialDocument(officialDocumentID, this.AdminIdentity.LoginSessionGuid);
						if (result == null)
						{
							this.RefreshPage();
							return;
						}

						this.Response.Clear();
						var docName = result.Value.OfficialDocument.DocumentName;
						var desiredFileExtension = result.Value.SystemFile.FileExtensionEnum.ToFileExtension();
						if (System.IO.Path.GetExtension(docName) != desiredFileExtension)
							docName += desiredFileExtension;
						this.Response.AddHeader("Content-Disposition", $"attachment; filename=\"{docName}\"");
						this.Response.AddHeader("Content-Length", result.Value.FileBytes.Length.ToString());
						this.Response.ContentType = docName.ToMimeType();
						this.Response.BinaryWrite(result.Value.FileBytes);
						return;
					}
			}
		}

		protected void gvDocuments_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvDocuments_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvDocuments.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			var userTypeEnum = this.ddlUserTypeFilter.GetSelectedEnumValue<UserTypes>(null);
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToNullableInt();
			var statusEnum = this.ddlStatusFilter.GetSelectedNullableGuidEnum<Model.Entities.OfficialDocument.Statuses>();
			var searchText = this.tbSearchText.Text;
			var pageSize = this.gvDocuments.PageSize;
			var sortDirection = this.ViewState.GetSortDirection();
			var sortExpression = this.ViewState.GetSortExpression();
			var (officialDocuments, virtualItemCount) = BL.Core.OfficialDocuments.Admin.Documents.GetOfficialDocuments(instituteID, userTypeEnum, statusEnum, searchText, pageIndex, pageSize, sortDirection, sortExpression, this.AdminIdentity.LoginSessionGuid);
			this.gvDocuments.DataBind(officialDocuments, pageIndex, virtualItemCount, sortExpression, sortDirection);
		}

		private void ShowModal(int? officialDocumentID)
		{
			if (this.AdminIdentity.UserType.IsMasterAdmin())
			{
				this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.All);
				this.rfvddlInstituteID.Enabled = false;
			}
			else
			{
				this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.Select);
				this.rfvddlInstituteID.Enabled = true;
			}

			var userTypes = BL.Core.OfficialDocuments.Admin.Documents.ValidUserTypes.Select(t => new CustomListItem { ID = (short)t, Text = t.ToFullName() }).OrderBy(t => t.Text);
			this.lbUserTypeFlags.DataBind(userTypes);
			this.tbDocumentName.Text = null;
			this.ddlStatus.FillGuidEnums<Model.Entities.OfficialDocument.Statuses>();
			this.fileUpload.ClearFiles();

			if (officialDocumentID == null)
			{
				this.ddlInstituteID.Enabled = true;
				this.divFileUpload.Visible = true;
				this.ViewState[nameof(Model.Entities.OfficialDocument.OfficialDocumentID)] = null;
			}
			else
			{
				var officialDocument = BL.Core.OfficialDocuments.Admin.Documents.GetOfficialDocument(officialDocumentID.Value, this.AdminIdentity.LoginSessionGuid);
				if (officialDocument == null)
				{
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				}

				this.ddlInstituteID.SelectedValue = officialDocument.InstituteID.ToString();
				this.ddlInstituteID.Enabled = false;
				this.tbDocumentName.Text = officialDocument.DocumentName;
				this.tbProvidedBy.Text = officialDocument.ProvidedBy;
				var userTypeEnums = ((UserTypes)officialDocument.UserTypes).GetFlags().ToArray();
				this.lbUserTypeFlags.ClearSelection();
				foreach (var userTypeEnum in userTypeEnums)
					this.lbUserTypeFlags.Items.FindByValue(((short)userTypeEnum).ToString()).Selected = true;
				this.ddlStatus.SetSelectedGuidEnum(officialDocument.StatusEnum);
				this.divFileUpload.Visible = false;
				this.ViewState[nameof(Model.Entities.OfficialDocument.OfficialDocumentID)] = officialDocumentID;
			}

			this.modalDocument.Visible = true;
			this.modalDocument.HeaderText = officialDocumentID != null ? "Edit Document" : "Add Document";
			this.modalDocument.Show();
		}

		protected void btnSave_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var officialDocumentID = (int?)this.ViewState[nameof(Model.Entities.OfficialDocument.OfficialDocumentID)];
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			var userTypeFlags = this.lbUserTypeFlags.GetSelectedEnumValues<UserTypes>().Combine() ?? throw new InvalidOperationException();
			var documentName = this.tbDocumentName.Text;
			var providedBy = this.tbProvidedBy.Text;
			var statusEnum = this.ddlStatus.GetSelectedGuidEnum<Model.Entities.OfficialDocument.Statuses>();
			var instanceGuid = this.fileUpload.InstanceGuid;
			if (officialDocumentID == null)
			{
				var temporarySystemFileID = this.fileUpload.UploadedFiles.Single().FileID.ToGuid();
				var status = BL.Core.OfficialDocuments.Admin.Documents.AddOfficialDocument(instituteID, userTypeFlags, documentName, providedBy, instanceGuid, temporarySystemFileID, statusEnum, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.OfficialDocuments.Admin.Documents.AddOfficialDocumentStatuses.FileNotFound:
						this.modalAlert.AddErrorAlert("File not found.");
						this.updatePanelModalDocument.Update();
						return;
					case BL.Core.OfficialDocuments.Admin.Documents.AddOfficialDocumentStatuses.Success:
						this.AddSuccessAlert("Document has been uploaded.");
						this.RefreshPage();
						return;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
			else
			{
				var status = BL.Core.OfficialDocuments.Admin.Documents.UpdateOfficialDocument(officialDocumentID.Value, userTypeFlags, documentName, providedBy, statusEnum, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.OfficialDocuments.Admin.Documents.UpdateOfficialDocumentStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						return;
					case BL.Core.OfficialDocuments.Admin.Documents.UpdateOfficialDocumentStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Document");
						this.RefreshPage();
						return;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
		}

		private void RefreshPage()
		{
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToNullableInt();
			var userTypeEnum = this.ddlUserTypeFilter.GetSelectedEnumValue<UserTypes>(null);
			var statusEnum = this.ddlStatusFilter.GetSelectedNullableGuidEnum<Model.Entities.OfficialDocument.Statuses>();
			var searchText = this.tbSearchText.Text;
			var pageIndex = this.gvDocuments.PageIndex;
			var pageSize = this.gvDocuments.PageSize;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			Redirect(userTypeEnum, instituteID, statusEnum, searchText, sortDirection, sortExpression, pageIndex, pageSize);
		}
	}
}