﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ResetStudentsPassword.aspx.cs" Inherits="Aspire.Web.Sys.Admin.StudentPortal.ResetStudentsPassword" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:ScriptManagerProxy runat="server">
		<Scripts>
			<asp:ScriptReference Name="zxcvbn" />
		</Scripts>
	</asp:ScriptManagerProxy>
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstitute" />
				<aspire:AspireDropDownList runat="server" ID="ddlInstitute" AutoPostBack="True" OnSelectedIndexChanged="ddlInstitute_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartment" />
				<aspire:AspireDropDownList runat="server" ID="ddlDepartment" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartment_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgram" />
				<aspire:AspireDropDownList runat="server" ID="ddlProgram" AutoPostBack="True" OnSelectedIndexChanged="ddlProgram_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Search:" AssociatedControlID="tbSearch" />
				<asp:Panel runat="server" CssClass="input-group" DefaultButton="btnSearch">
					<aspire:AspireTextBox runat="server" ID="tbSearch" PlaceHolder="Search..." TrimText="True" />
					<div class="input-group-btn">
						<aspire:AspireButton runat="server" ID="btnSearch" Glyphicon="search" OnClick="btnSearch_OnClick" />
					</div>
				</asp:Panel>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ItemType="Aspire.BL.Core.StudentInformation.Admin.Students.Student" ID="gvStudents" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" OnSorting="OnSorting" OnPageSizeChanging="OnPageSizeChanging" OnPageIndexChanging="OnPageIndexChanging" OnRowCommand="gvStudents_OnRowCommand">
		<Columns>
			<asp:BoundField HeaderText="Institute" DataField="InstituteAlias" SortExpression="InstituteAlias" />
			<asp:BoundField HeaderText="Program" DataField="ProgramAlias" SortExpression="ProgramAlias" />
			<asp:BoundField HeaderText="Enrollment" DataField="Enrollment" SortExpression="Enrollment" />
			<asp:BoundField HeaderText="Name" DataField="Name" SortExpression="Name" />
			<asp:BoundField HeaderText="Email" DataField="UniversityEmail" SortExpression="UniversityEmail" />
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CommandName="ResetPassword" CausesValidation="False" Text="Reset Password" EncryptedCommandArgument="<%# Item.StudentID %>" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalResetPassword" ModalSize="Large">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelResetPassword" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertModalResetPassword" />
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Enrollment:" AssociatedControlID="lblEnrollment" />
								<div>
									<aspire:Label runat="server" ID="lblEnrollment" />
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="lblProgramVal" />
								<div>
									<aspire:Label runat="server" ID="lblProgramVal" />
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="lblName" />
								<div>
									<aspire:Label runat="server" ID="lblName" />
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Father Name:" AssociatedControlID="lblFatherName" />
								<div>
									<aspire:Label runat="server" ID="lblFatherName" />
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="University Email:" AssociatedControlID="lblUniversityEmail" />
								<div>
									<aspire:Label runat="server" ID="lblUniversityEmail" />
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Personal Email:" AssociatedControlID="lblPersonalEmail" />
								<div>
									<aspire:Label runat="server" ID="lblPersonalEmail" />
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Students</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<aspire:AspireLabel runat="server" Text="New Password:" AssociatedControlID="tbNewPasswordStudent" />
										<aspire:AspireTextBox runat="server" TrimText="False" TextMode="Password" ValidationGroup="ResetPasswordStudent" ID="tbNewPasswordStudent" PlaceHolder="New Password" />
										<aspire:AspirePasswordStrengthMeter runat="server" TextBoxID="tbNewPasswordStudent" />
										<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="ResetPasswordStudent" ControlToValidate="tbNewPasswordStudent" RequiredErrorMessage="This field is required." />
										<aspire:AspirePasswordPolicyValidator SecurityLevel="Medium" runat="server" ControlToValidate="tbNewPasswordStudent" ValidationGroup="ResetPasswordStudent" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<aspire:AspireLabel runat="server" Text="Confirm New Password:" AssociatedControlID="tbConfirmNewPasswordStudent" />
										<aspire:AspireTextBox runat="server" TrimText="False" TextMode="Password" ValidationGroup="ResetPasswordStudent" ID="tbConfirmNewPasswordStudent" PlaceHolder="Confirm New Password" />
										<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="ResetPasswordStudent" ControlToValidate="tbConfirmNewPasswordStudent" RequiredErrorMessage="This field is required." />
										<aspire:AspireCompareValidator ValidateEmptyText="True" runat="server" ErrorMessage="Passwords do not match." ValidationGroup="ResetPasswordStudent" ControlToValidate="tbConfirmNewPasswordStudent" ControlToCompare="tbNewPasswordStudent" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<aspire:AspireLabel runat="server" Text="Reason:" AssociatedControlID="ddlReasonStudent" />
										<aspire:AspireDropDownList runat="server" ID="ddlReasonStudent" ValidationGroup="ChangePasswordStudent" />
										<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ControlToValidate="ddlReasonStudent" ValidationGroup="ResetPasswordStudent" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label style="display: block; visibility: hidden">&nbsp;</label>
										<aspire:AspireButton runat="server" ID="btnResetPasswordStudent" Text="Reset Password" ButtonType="Primary" ValidationGroup="ResetPasswordStudent" OnClick="btnResetPasswordStudent_OnClick" />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Parents</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<aspire:AspireLabel runat="server" Text="Parent's Email:" AssociatedControlID="tbParentEmail" />
										<aspire:AspireTextBox runat="server" ValidationGroup="ParentEmail" ID="tbParentEmail" MaxLength="100" />
										<aspire:AspireStringValidator ValidationExpression="Email" runat="server" AllowNull="false" ValidationGroup="ParentEmail" ControlToValidate="tbParentEmail" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid email address." />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label style="display: block; visibility: hidden">&nbsp;</label>
										<aspire:AspireButton runat="server" ID="btnUpdateParentEmail" Text="Update" ButtonType="Primary" ValidationGroup="ParentEmail" OnClick="btnUpdateParentEmail_OnClick" />
									</div>
								</div>
							</div>
							<hr />
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<aspire:AspireLabel runat="server" Text="New Password:" AssociatedControlID="tbNewPasswordParents" />
										<aspire:AspireTextBox runat="server" TextMode="Password" ValidationGroup="ResetPasswordParents" ID="tbNewPasswordParents" PlaceHolder="New Password" />
										<aspire:AspirePasswordStrengthMeter runat="server" TextBoxID="tbNewPasswordParents" />
										<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="ResetPasswordParents" ControlToValidate="tbNewPasswordParents" RequiredErrorMessage="This field is required." />
										<aspire:AspirePasswordPolicyValidator SecurityLevel="Medium" runat="server" ControlToValidate="tbNewPasswordParents" ValidationGroup="ResetPasswordParents" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<aspire:AspireLabel runat="server" Text="Confirm New Password:" AssociatedControlID="tbConfirmNewPasswordParents" />
										<aspire:AspireTextBox runat="server" TextMode="Password" ValidationGroup="ResetPasswordParents" ID="tbConfirmNewPasswordParents" PlaceHolder="Confirm New Password" />
										<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="ResetPasswordParents" ControlToValidate="tbConfirmNewPasswordParents" RequiredErrorMessage="This field is required." />
										<aspire:AspireCompareValidator ValidateEmptyText="True" runat="server" ErrorMessage="Passwords do not match." ValidationGroup="ResetPasswordParents" ControlToValidate="tbConfirmNewPasswordParents" ControlToCompare="tbNewPasswordParents" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<aspire:AspireLabel runat="server" Text="Reason:" AssociatedControlID="ddlReasonParents" />
										<aspire:AspireDropDownList runat="server" ID="ddlReasonParents" ValidationGroup="ChangePasswordParents" />
										<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ControlToValidate="ddlReasonParents" ValidationGroup="ResetPasswordParents" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label style="display: block; visibility: hidden">&nbsp;</label>
										<aspire:AspireButton runat="server" ID="btnResetPasswordParents" Text="Reset Password" ButtonType="Primary" ValidationGroup="ResetPasswordParents" OnClick="btnResetPasswordParents_OnClick" />
										<aspire:AspireButton runat="server" ID="btnSendResetPasswordLinkParents" Text="Send Password Reset Link" ButtonType="Primary" ValidationGroup="ResetPasswordParentsLink" OnClick="btnSendResetPasswordLinkParents_OnClick" ConfirmMessage="Are you sure you want to send password reset link?" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<aspire:AspireModalCloseButton runat="server">Close</aspire:AspireModalCloseButton>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
