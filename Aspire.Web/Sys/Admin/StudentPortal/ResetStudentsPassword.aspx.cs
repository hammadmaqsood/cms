﻿using Aspire.BL.Core.StudentInformation.Admin;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.StudentPortal
{
	[AspirePage("7BB19237-1BC2-4811-AE91-25A268805348", UserTypes.Admins, "~/Sys/Admin/StudentPortal/ResetStudentsPassword.aspx", "Reset Students Password ", true, AspireModules.StudentInformation)]
	public partial class ResetStudentsPassword : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.StudentInformation.Module, new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};

		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.lock_);
		public override string PageTitle => "Reset Students Password";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Enrollment", SortDirection.Ascending);
				this.ddlInstitute.FillInstitutesForAdmin();
				this.ddlInstitute_OnSelectedIndexChanged(null, null);
			}
		}

		#region Filters

		protected void ddlInstitute_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstitute.SelectedValue.ToInt();
			this.ddlDepartment.FillDepartments(instituteID, CommonListItems.All);
			this.ddlDepartment_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartment_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstitute.SelectedValue.ToInt();
			var departmentID = this.ddlDepartment.SelectedValue.ToNullableInt();
			this.ddlProgram.FillPrograms(instituteID, departmentID, true, CommonListItems.All);
			this.ddlProgram_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgram_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearch_OnClick(null, null);
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		#endregion

		#region Gridview Events 

		protected void OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvStudents.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvStudents_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "ResetPassword":
					var studentID = e.DecryptedCommandArgumentToInt();
					this.DisplayResetPasswordModal(studentID);
					break;
			}
		}

		private void GetData(int pageIndex)
		{
			var instituteID = this.ddlInstitute.SelectedValue.ToInt();
			var departmentID = this.ddlDepartment.SelectedValue.ToNullableInt();
			var programID = this.ddlProgram.SelectedValue.ToNullableInt();
			var searchText = this.tbSearch.Text;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var students = BL.Core.StudentInformation.Admin.Students.GetStudents(instituteID, departmentID, programID, searchText, out var virtualItemCount, pageIndex, this.gvStudents.PageSize, sortExpression, sortDirection, this.AdminIdentity.LoginSessionGuid);
			this.gvStudents.DataBind(students, pageIndex, virtualItemCount, sortExpression, sortDirection);
		}

		#endregion

		#region Modal

		private void DisplayResetPasswordModal(int studentID)
		{
			var student = BL.Core.StudentInformation.Admin.Students.GetStudentInfo(studentID, this.AdminIdentity.LoginSessionGuid);
			if (student == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect<ResetStudentsPassword>();
				return;
			}
			this.lblEnrollment.Text = student.Enrollment;
			this.lblProgramVal.Text = student.ProgramAlias;
			this.lblName.Text = student.Name;
			this.lblFatherName.Text = student.FatherName;
			this.lblUniversityEmail.Text = student.UniversityEmail.ToNAIfNullOrEmpty();
			this.lblPersonalEmail.Text = student.PersonalEmail.ToNAIfNullOrEmpty();
			this.ddlReasonStudent.FillPasswordChangeReasons(CommonListItems.Select);
			this.tbParentEmail.Text = student.ParentsEmail;
			this.ddlReasonParents.FillPasswordChangeReasons(CommonListItems.Select);
			this.modalResetPassword.HeaderText = "Reset Password";
			this.ViewState["StudentID"] = studentID;
			this.modalResetPassword.Show();
		}

		protected void btnResetPasswordStudent_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var studentID = (int)this.ViewState["StudentID"];
			var newPassword = this.tbNewPasswordStudent.Text;
			var reasonEnum = this.ddlReasonStudent.GetSelectedEnumValue<PasswordChangeReasons>();
			var result = BL.Core.StudentInformation.Admin.Students.ResetStudentPassword(studentID, newPassword, reasonEnum, this.AdminIdentity.LoginSessionGuid);
			switch (result)
			{
				case Students.ResetStudentPasswordResult.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<ResetStudentsPassword>();
					return;
				case Students.ResetStudentPasswordResult.EmailIsRequired:
					this.alertModalResetPassword.AddErrorAlert("University/Personal Email for this student is not defined.");
					return;
				case Students.ResetStudentPasswordResult.Success:
					this.alertModalResetPassword.AddSuccessAlert("Password has been reset and intimation email has been sent to student.");
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		#endregion

		protected void btnUpdateParentEmail_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var studentID = (int)this.ViewState["StudentID"];
			var parentEmail = this.tbParentEmail.Text;
			var result = BL.Core.StudentInformation.Admin.Students.UpdateParentEmail(studentID, parentEmail, this.AdminIdentity.LoginSessionGuid);
			switch (result)
			{
				case Students.UpdateParentEmailResult.Success:
					this.alertModalResetPassword.AddSuccessAlert("Parent's Email has been updated.");
					break;
				case Students.UpdateParentEmailResult.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<ResetStudentsPassword>();
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void btnResetPasswordParents_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var studentID = (int)this.ViewState["StudentID"];
			var newPassword = this.tbNewPasswordParents.Text;
			var reasonEnum = this.ddlReasonParents.GetSelectedEnumValue<PasswordChangeReasons>();
			var result = BL.Core.StudentInformation.Admin.Students.ResetParentPassword(studentID, newPassword, reasonEnum, this.AdminIdentity.LoginSessionGuid);
			switch (result)
			{
				case Students.ResetParentPasswordResult.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<ResetStudentsPassword>();
					return;
				case Students.ResetParentPasswordResult.EmailIsRequired:
					this.alertModalResetPassword.AddErrorAlert("Parent's Email for this student is not defined.");
					return;
				case Students.ResetParentPasswordResult.Success:
					this.alertModalResetPassword.AddSuccessAlert("Password has been reset and intimation email has been sent on parent's email.");
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void btnSendResetPasswordLinkParents_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var studentID = (int)this.ViewState["StudentID"];
			var url = typeof(Logins.Student.PasswordReset).GetAspirePageAttribute().PageUrl.ToAbsoluteUrl();
			var result = BL.Core.StudentInformation.Admin.Students.SendPasswordResetLinkForParent(studentID, url, this.AdminIdentity.LoginSessionGuid);
			switch (result)
			{
				case Students.SendPasswordResetLinkForParentResult.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<ResetStudentsPassword>();
					return;
				case Students.SendPasswordResetLinkForParentResult.EmailIsRequired:
					this.alertModalResetPassword.AddErrorAlert("Parent's Email for this student is not defined.");
					return;
				case Students.SendPasswordResetLinkForParentResult.Success:
					this.alertModalResetPassword.AddSuccessAlert("Link for password reset has been sent.");
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}