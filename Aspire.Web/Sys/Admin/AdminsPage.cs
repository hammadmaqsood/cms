using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Admin
{
	public abstract class AdminsPage : AspireBasePage, IBreadcrumb, ISidebar, IProfileInfo
	{
		protected abstract IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired { get; }

		protected override void Authenticate()
		{
			if (this.AdminIdentity == null)
				throw new AspireAccessDeniedException(UserTypes.Admins, UserTypes.Admins);
			if (this.PermissionsRequired != null)
				foreach (var permission in this.PermissionsRequired)
					this.AdminIdentity.Demand(permission.Key, permission.Value);
		}

		private IAdminIdentity _adminIdentity;
		protected IAdminIdentity AdminIdentity => this._adminIdentity ?? (this._adminIdentity = this.AspireIdentity as IAdminIdentity);

		public override AspireThemes AspireTheme => this.AdminIdentity.UserType.IsMasterAdmin() ? AspireDefaultThemes.MasterAdmin : AspireDefaultThemes.Admin;

		public virtual bool BreadcrumbVisible => true;

		public override bool PageTitleVisible => true;

		public virtual bool SidebarVisible => true;

		public virtual string SidebarHtml => SessionHelper.SideBarLinkHtml;

		public virtual string BreadcrumbHtml => SessionHelper.SideBarLinks?.GetBreadcrumbHtml(this.AspirePageAttribute.AspirePageGuid);

		public virtual bool ProfileInfoVisible => false;

		public virtual bool ProfileLinkVisible => false;

		public bool IsCustomProfileImage => false;

		public string CustomProfileImageUrl => null;

		public virtual string ProfileNavigateUrl => null;

		public virtual bool LogoffLinkVisible => true;

		public virtual string LogoffNavigateUrl => Logoff.GetPageUrl(UserLoginHistory.LogOffReasons.LogOff);

		public virtual string Username => this.AdminIdentity.Name;

		public virtual string Name => this.AdminIdentity.Name;

		public virtual string Email => this.AdminIdentity.Email;
	}
}