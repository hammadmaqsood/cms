﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="AdmissionOpen.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Admissions.AdmissionOpen" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
				<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" OnSelectedIndexChanged="ddlInstituteID_OnSelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
				<aspire:AspireDropDownList runat="server" ID="ddlProgramID" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlProgramID_OnSelectedIndexChanged" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlProgramID" ValidationGroup="Save" ErrorMessage="This field is required." />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Intake Semester:" AssociatedControlID="ddlSemesterID" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterID" ValidationGroup="Save" ErrorMessage="This field is required." />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Is Summer Regular Semester:" AssociatedControlID="rblIsSummerRegularSemester" />
				<div class="form-control-static">
					<aspire:AspireRadioButtonList runat="server" ID="rblIsSummerRegularSemester" RepeatLayout="Flow" RepeatDirection="Horizontal" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="rblIsSummerRegularSemester_OnSelectedIndexChanged" />
				</div>
				<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Save" ControlToValidate="rblIsSummerRegularSemester" ErrorMessage="This field is required." />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Final Semester:" AssociatedControlID="ddlFinalSemesterID" />
				<aspire:AspireDropDownList runat="server" ID="ddlFinalSemesterID" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlFinalSemesterID" ErrorMessage="This field is required." ValidationGroup="Save" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Max. Semester (Time-bar):" AssociatedControlID="ddlMaxSemesterID" />
				<aspire:AspireDropDownList runat="server" ID="ddlMaxSemesterID" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlMaxSemesterID" ErrorMessage="This field is required." ValidationGroup="Save" />
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Admission Settings</h3>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Shifts:" AssociatedControlID="cbShifts" />
						<div class="form-control-static">
							<aspire:AspireCheckBoxList runat="server" ID="cbShifts" RepeatDirection="Horizontal" RepeatLayout="Flow" ValidationGroup="Save" />
						</div>
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="cbShifts" ErrorMessage="This field is required." ValidationGroup="Save" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Admission Criteria:" AssociatedControlID="ddlAdmissionCriteria" />
						<aspire:AspireDropDownList runat="server" ID="ddlAdmissionCriteria" DataTextField="CriteriaName" DataValueField="AdmissionCriteriaID" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlAdmissionCriteria" ErrorMessage="This field is required." ValidationGroup="Save" />
					</div>
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Eligibility Criteria Statement:" AssociatedControlID="tbEligibilityCriteriaStatement" />
				<aspire:AspireTextBox Rows="3" TextMode="MultiLine" runat="server" ID="tbEligibilityCriteriaStatement" ValidationGroup="Save" MaxLength="2000" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbEligibilityCriteriaStatement" ErrorMessage="This field is required." ValidationGroup="Save" />
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Admission Opening Date (International Students):" AssociatedControlID="dpAdmissionOpenDateForForeigners" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dpAdmissionOpenDateForForeigners" ValidationGroup="Save" DisplayMode="ShortDateTime" />
						<aspire:AspireDateTimeValidator runat="server" ID="dtvAdmissionOpenDateForForeigners" ControlToValidate="dpAdmissionOpenDateForForeigners" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." ValidationGroup="Save" AllowNull="True" Format="ShortDateTime" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Admission Closing Date (International Students):" AssociatedControlID="dpAdmissionCloseDateForForeigners" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dpAdmissionCloseDateForForeigners" ValidationGroup="Save" DisplayMode="ShortDateTime" />
						<aspire:AspireDateTimeValidator runat="server" ID="dtvAdmissionCloseDateForForeigners" ControlToValidate="dpAdmissionCloseDateForForeigners" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." ValidationGroup="Save" AllowNull="True" Format="ShortDateTime" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Admission Opening Date (National Students):" AssociatedControlID="dpAdmissionOpenDate" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dpAdmissionOpenDate" ValidationGroup="Save" DisplayMode="ShortDateTime" />
						<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dpAdmissionOpenDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." ValidationGroup="Save" AllowNull="False" Format="ShortDateTime" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Admission Closing Date (National Students):" AssociatedControlID="dpAdmissionCloseDate" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dpAdmissionCloseDate" ValidationGroup="Save" DisplayMode="ShortDateTime" />
						<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dpAdmissionCloseDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." ValidationGroup="Save" AllowNull="False" Format="ShortDateTime" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Admission Processing Fee:" AssociatedControlID="tbAdmissionProcessingFee" />
						<aspire:AspireTextBox runat="server" ID="tbAdmissionProcessingFee" ValidationGroup="Save" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbAdmissionProcessingFee" ErrorMessage="This field is required." ValidationGroup="Save" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Admission Processing Fee Due Date:" AssociatedControlID="dtpAdmissionProcessingFeeDueDate" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpAdmissionProcessingFeeDueDate" ValidationGroup="Save" DisplayMode="ShortDateTime" />
						<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dtpAdmissionProcessingFeeDueDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." ValidationGroup="Save" AllowNull="False" Format="ShortDateTime" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Entry Test Type:" AssociatedControlID="ddlEntryTestType" />
						<aspire:AspireDropDownList runat="server" ID="ddlEntryTestType" AutoPostBack="True" OnSelectedIndexChanged="ddlEntryTestType_OnSelectedIndexChanged" ValidationGroup="Save" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlEntryTestType" ErrorMessage="This field is required." ValidationGroup="AdmissionOpen" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Entry Test Duration:" AssociatedControlID="tbEntryTestDuration" />
						<div class="input-group">
							<aspire:AspireTextBox runat="server" ID="tbEntryTestDuration" ValidationGroup="Save" />
							<div class="input-group-addon">Minutes</div>
						</div>
						<aspire:AspireByteValidator ID="bvEntryTestDuration" runat="server" ControlToValidate="tbEntryTestDuration" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid Number." ValidationGroup="Save" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Entry Tests (From):" AssociatedControlID="dtpEntryTestStartDate" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpEntryTestStartDate" ValidationGroup="Save" />
						<aspire:AspireDateTimeValidator ID="dtvEntryTestStartDate" runat="server" ControlToValidate="dtpEntryTestStartDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." ValidationGroup="Save" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Entry Tests (To):" AssociatedControlID="dtpEntryTestEndDate" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpEntryTestEndDate" ValidationGroup="Save" />
						<aspire:AspireDateTimeValidator runat="server" ID="dtvEntryTestEndDate" ControlToValidate="dtpEntryTestEndDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." ValidationGroup="Save" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Interviews (From):" AssociatedControlID="dpInterviewStartDate" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dpInterviewStartDate" ValidationGroup="Save" />
						<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dpInterviewStartDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." ValidationGroup="Save" AllowNull="False" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Interviews (To):" AssociatedControlID="dpInterviewEndDate" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dpInterviewEndDate" ValidationGroup="Save" />
						<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dpInterviewEndDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." ValidationGroup="Save" AllowNull="False" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Login Allowed Date:" AssociatedControlID="dpLoginAllowedDate" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dpLoginAllowedDate" ValidationGroup="Save" />
						<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dpLoginAllowedDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." ValidationGroup="Save" AllowNull="False" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Intake Target (Students Count):" AssociatedControlID="tbIntakeTargetStudentsCount" />
						<aspire:AspireTextBox runat="server" ID="tbIntakeTargetStudentsCount" MaxLength="5" ValidationGroup="Save" />
						<aspire:AspireInt16Validator runat="server" ControlToValidate="tbIntakeTargetStudentsCount" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid number." InvalidRangeErrorMessage="Invalid number." ValidationGroup="Save" AllowNull="False" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 text-center">
			<div class="form-group">
				<aspire:AspireButton runat="server" ID="btnSaveAdmissionOpen" ButtonType="Primary" ValidationGroup="Save" Text="Save" OnClick="btnSaveAdmissionOpen_OnClick" />
				<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="~/Sys/Admin/Admissions/AdmissionsOpen.aspx" />
			</div>
		</div>
	</div>
</asp:Content>
