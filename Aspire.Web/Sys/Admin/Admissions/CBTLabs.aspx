﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CBTLabs.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Admissions.CBTLabs" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireButton ID="btnAddLab" ButtonType="Success" Glyphicon="plus" Text="Add Lab" runat="server" CausesValidation="false" OnClick="btnAddLab_OnClick" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlInstituteIDFilter" CausesValidation="False" OnSelectedIndexChanged="ddlInstituteIDFilter_OnSelectedIndexChanged" AutoPostBack="True" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView ID="gvCBTLabs" ItemType="Aspire.BL.Core.Admissions.Admin.CBTLabs.CBTLab" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" OnPageIndexChanging="gvCBTLabs_PageIndexChanging" OnPageSizeChanging="gvCBTLabs_PageSizeChanging" OnRowCommand="gvCBTLabs_RowCommand" OnSorting="gvCBTLabs_Sorting">
		<Columns>
			<asp:BoundField DataField="InstituteAlias" SortExpression="InstituteAlias" HeaderText="Institute" />
			<asp:BoundField DataField="TestCentreName" SortExpression="TestCentreName" HeaderText="Test Centre" />
			<asp:BoundField DataField="LabName" SortExpression="LabName" HeaderText="Lab Name" />
			<asp:BoundField DataField="Computers" SortExpression="Computers" HeaderText="Computers" />
			<asp:BoundField DataField="StatusFullName" SortExpression="Status" HeaderText="Status" />
			<asp:TemplateField ItemStyle-Wrap="False">
				<ItemTemplate>
					<aspire:AspireLinkButton Glyphicon="edit" runat="server" CommandName="Edit" CausesValidation="False" EncryptedCommandArgument='<%# Item.CBTLabID %>' />
					<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Delete" Glyphicon="remove" EncryptedCommandArgument='<%# Item.CBTLabID %>' ConfirmMessage='<%# $"Are you sure you want to delete Lab \"{Item.LabName}\"?" %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalCBTLab">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelCBTLab" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertModalCBTLab" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" ValidationGroup="Save" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlInstituteID" ErrorMessage="This field is required." ValidationGroup="Save" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Test Centre Name:" AssociatedControlID="tbTestCentreName" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbTestCentreName" MaxLength="200" ValidationGroup="Save" />
								<aspire:AspireStringValidator AllowNull="False" runat="server" ControlToValidate="tbTestCentreName" RequiredErrorMessage="This field is required." ValidationGroup="Save" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Lab Name:" AssociatedControlID="tbLabName" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbLabName" MaxLength="200" ValidationGroup="Save" />
								<aspire:AspireStringValidator AllowNull="False" runat="server" ControlToValidate="tbLabName" RequiredErrorMessage="This field is required." ValidationGroup="Save" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Computers:" AssociatedControlID="tbComputers" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbComputers" MaxLength="8" ValidationGroup="Save" />
								<aspire:AspireByteValidator AllowNull="False" runat="server" ControlToValidate="tbComputers" MinValue="1" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid number." InvalidRangeErrorMessage="Valid range is 1-255." ValidationGroup="Save" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="rblStatus" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireRadioButtonList runat="server" ID="rblStatus" ValidationGroup="Save" RepeatLayout="Flow" RepeatDirection="Horizontal" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblStatus" ErrorMessage="This field is required." ValidationGroup="Save" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" OnClick="btnSave_OnClick" ValidationGroup="Save" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
