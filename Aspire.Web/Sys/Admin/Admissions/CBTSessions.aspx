﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CBTSessions.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Admissions.CBTSessions" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireButton ID="btnAddSession" ButtonType="Success" Glyphicon="plus" Text="Add Session" runat="server" CausesValidation="false" OnClick="btnAddSession_OnClick" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlInstituteIDFilter" CausesValidation="False" OnSelectedIndexChanged="ddlInstituteIDFilter_OnSelectedIndexChanged" AutoPostBack="True" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView ID="gvCBTSessions" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" OnPageIndexChanging="gvCBTSessions_PageIndexChanging" OnPageSizeChanging="gvCBTSessions_PageSizeChanging" OnRowCommand="gvCBTSessions_RowCommand" OnSorting="gvCBTSessions_Sorting">
		<Columns>
			<asp:BoundField DataField="InstituteAlias" SortExpression="InstituteAlias" HeaderText="Institute" />
			<asp:BoundField DataField="StartTime" SortExpression="StartTime" HeaderText="Start Date/Time" />
			<asp:BoundField DataField="DurationInMinutes" SortExpression="DurationInMinutes" HeaderText="Duration (Minutes)" />
			<asp:BoundField DataField="Labs" SortExpression="Labs" HeaderText="Labs" />
			<asp:BoundField DataField="Computers" SortExpression="Computers" HeaderText="Computers" />
			<asp:BoundField DataField="ConfirmedSeats" SortExpression="ConfirmedSeats" HeaderText="Confirmed Seats" />
			<asp:TemplateField ItemStyle-Wrap="False">
				<ItemTemplate>
					<aspire:AspireLinkButton Glyphicon="edit" runat="server" CommandName="Edit" CausesValidation="False" EncryptedCommandArgument='<%# Eval("CBTSessionID") %>' />
					<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Delete" Glyphicon="remove" EncryptedCommandArgument='<%# Eval("CBTSessionID") %>' ConfirmMessage="Are you sure you want to delete Session?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalCBTSession">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelCBTSession" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertModalCBTSession" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" CausesValidation="False" ValidationGroup="Save" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteID_OnSelectedIndexChanged" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlInstituteID" ErrorMessage="This field is required." ValidationGroup="Save" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Start Date/Time:" AssociatedControlID="dtpStartTime" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpStartTime" DisplayMode="ShortDateTime" ValidationGroup="Save" />
								<aspire:AspireDateTimeValidator Format="ShortDateTime" AllowNull="False" runat="server" ControlToValidate="dtpStartTime" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Invalid Dates." InvalidDataErrorMessage="Invalid Date/Time." ValidationGroup="Save" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Test Duration:" AssociatedControlID="tbDurationInMinutes" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbDurationInMinutes" MaxLength="8" ValidationGroup="Save" />
								<aspire:AspireByteValidator AllowNull="False" runat="server" ControlToValidate="tbDurationInMinutes" MinValue="1" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid number." InvalidRangeErrorMessage="Valid range is 1-255." ValidationGroup="Save" />
							</div>
						</div>
					</div>
					<asp:Repeater runat="server" ID="repeaterCBTLabs">
						<HeaderTemplate>
							<table id='<%=this.repeaterCBTLabs.ClientID %>' class="table table-bordered table-responsive table-striped">
								<thead>
									<tr>
										<th>Select</th>
										<th>Lab Name</th>
										<th>Computers</th>
									</tr>
								</thead>
								<tbody>
						</HeaderTemplate>
						<ItemTemplate>
							<tr>
								<td>
									<aspire:AspireCheckBox runat="server" ID="cbSelect" Checked='<%# Eval("Selected") %>' ValidationGroup="Save" />
									<aspire:HiddenField runat="server" Value='<%# Eval("CBTLabID") %>' Mode="Encrypted" ID="hfCBTLabID" Visible="False" />
								</td>
								<td><%# Eval("LabName") %></td>
								<td>
									<aspire:AspireTextBox Text='<%# Eval("Computers") %>' runat="server" ID="tbComputers" ValidationGroup="Save" MaxLength="5" />
									<aspire:AspireByteValidator runat="server" ControlToValidate="tbComputers" AllowNull="True" ValidationGroup="Save" MinValue="1" MaxValue="255" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Valid Range 1-255." />
								</td>
							</tr>
						</ItemTemplate>
						<FooterTemplate>
							</tbody>
							<tfoot></tfoot>
							</table>
						</FooterTemplate>
					</asp:Repeater>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" OnClick="btnSave_OnClick" ValidationGroup="Save" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
