﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Admissions
{
	[AspirePage("E5F443BA-BF07-4154-B9F0-C8CF69FA9683", UserTypes.Admins, "~/Sys/Admin/Admissions/CBTLabs.aspx", "CBT Labs", true, AspireModules.Admissions)]
	public partial class CBTLabs : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.Admissions.Module, new []{UserGroupPermission.PermissionValues.Allowed, } },
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "CBT Labs";

		public static string GetPageURL(int? instituteID)
		{
			return typeof(CBTLabs).GetAspirePageAttribute().PageUrl.AttachQueryParam("InstituteID", instituteID);
		}

		public static void Redirect(int? instituteID)
		{
			Redirect(GetPageURL(instituteID));
		}

		private void Refresh()
		{
			Redirect(this.ddlInstituteIDFilter.SelectedValue.ToNullableInt());
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("LabName", SortDirection.Ascending);
				this.ddlInstituteIDFilter.FillInstitutesForAdmin(CommonListItems.All).SetSelectedValueIfNotPostback("InstituteID");
				this.ddlInstituteIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlInstituteIDFilter_OnSelectedIndexChanged(object o, EventArgs e)
		{
			this.GetData(0);
		}

		protected void gvCBTLabs_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvCBTLabs_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(this.gvCBTLabs.PageIndex);
		}

		protected void gvCBTLabs_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvCBTLabs.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvCBTLabs_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageCBTLabs, UserGroupPermission.PermissionValues.Allowed);
					this.ShowModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageCBTLabs, UserGroupPermission.PermissionValues.Allowed);
					var cbtLabID = e.DecryptedCommandArgumentToInt();
					var result = BL.Core.Admissions.Admin.CBTLabs.DeleteCBTLab(cbtLabID, this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case BL.Core.Admissions.Admin.CBTLabs.DeleteCBTLabStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.Admissions.Admin.CBTLabs.DeleteCBTLabStatuses.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("CBT Lab");
							break;
						case BL.Core.Admissions.Admin.CBTLabs.DeleteCBTLabStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("CBT Lab");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.GetData(0);
					break;
			}
		}

		private void GetData(int pageIndex)
		{
			var cbtLabs = BL.Core.Admissions.Admin.CBTLabs.GetCBTLabs(this.ddlInstituteIDFilter.SelectedValue.ToNullableInt(), pageIndex, this.gvCBTLabs.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out int virtualItemCount, this.AdminIdentity.LoginSessionGuid);
			this.gvCBTLabs.DataBind(cbtLabs, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		protected void btnAddLab_OnClick(object sender, EventArgs e)
		{
			this.ShowModal(null);
		}

		private void ShowModal(int? cbtLabID)
		{
			this.ddlInstituteID.FillInstitutesForAdmin();
			this.rblStatus.FillCBTLabStatuses();
			this.ViewState["CBTLabID"] = null;
			if (cbtLabID == null)
			{
				this.ddlInstituteID.Enabled = true;
				this.tbTestCentreName.Text = null;
				this.tbLabName.Text = null;
				this.tbComputers.Text = null;
				this.rblStatus.SelectedIndex = 0;
				this.modalCBTLab.HeaderText = "Add CBT Lab";
			}
			else
			{
				var cbtLab = BL.Core.Admissions.Admin.CBTLabs.GetCBTLab(cbtLabID.Value, this.AdminIdentity.LoginSessionGuid);
				if (cbtLab == null)
				{
					this.AddNoRecordFoundAlert();
					this.GetData(0);
					return;
				}
				this.ddlInstituteID.SelectedValue = cbtLab.InstituteID.ToString();
				this.ddlInstituteID.Enabled = false;
				this.tbTestCentreName.Text = cbtLab.TestCentreName;
				this.tbLabName.Text = cbtLab.LabName;
				this.tbComputers.Text = cbtLab.Computers.ToString();
				this.rblStatus.SetEnumValue(cbtLab.StatusEnum);
				this.modalCBTLab.HeaderText = "Edit CBT Lab";
				this.ViewState["CBTLabID"] = cbtLabID.Value;
			}
			this.modalCBTLab.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var cbtLabID = this.ViewState["CBTLabID"] as int?;
			var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
			var testCentreName = this.tbTestCentreName.Text;
			var labName = this.tbLabName.Text;
			var computers = this.tbComputers.Text.ToByte();
			var statusEnum = this.rblStatus.GetSelectedEnumValue<CBTLab.Statuses>();

			if (cbtLabID == null)
			{
				var status = BL.Core.Admissions.Admin.CBTLabs.AddCBTLab(instituteID, testCentreName, labName, computers, statusEnum, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.Admissions.Admin.CBTLabs.AAddCBTLabStatuses.LabNameAlreadyExists:
						this.alertModalCBTLab.AddErrorAlert("Lab Name already exists.");
						this.updatePanelCBTLab.Update();
						return;
					case BL.Core.Admissions.Admin.CBTLabs.AAddCBTLabStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("CBT Lab");
						this.Refresh();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var status = BL.Core.Admissions.Admin.CBTLabs.UpdateCBTLab(cbtLabID.Value, testCentreName, labName, computers, statusEnum, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.Admissions.Admin.CBTLabs.UpdateCBTLabStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.Refresh();
						break;
					case BL.Core.Admissions.Admin.CBTLabs.UpdateCBTLabStatuses.LabNameAlreadyExists:
						this.alertModalCBTLab.AddErrorAlert("Lab Name already exists.");
						this.updatePanelCBTLab.Update();
						break;
					case BL.Core.Admissions.Admin.CBTLabs.UpdateCBTLabStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("CBT Lab");
						this.Refresh();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
	}
}