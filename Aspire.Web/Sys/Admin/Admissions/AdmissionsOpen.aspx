﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="AdmissionsOpen.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Admissions.AdmissionsOpen" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="hlAdd" Text="&nbsp;" />
				<div>
					<aspire:AspireHyperLinkButton runat="server" ID="hlAdd" ButtonType="Success" Glyphicon="plus" Text="Add" NavigateUrl="AdmissionOpen.aspx" />
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstitutes" />
				<aspire:AspireDropDownList runat="server" ID="ddlInstitutes" CausesValidation="False" OnSelectedIndexChanged="ddlInstitutes_OnSelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesters" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesters" CausesValidation="False" OnSelectedIndexChanged="ddlSemesters_OnSelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
	</div>
	<aspire:AspireGridView runat="server" ID="gvAdmissionsOpen" ItemType="Aspire.BL.Core.Admissions.Admin.AdmissionOpenPrograms.AdmissionOpenProgram" PageSize="100" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnSorting="gvAdmissionsOpen_OnSorting" OnPageIndexChanging="gvAdmissionsOpen_OnPageIndexChanging" OnPageSizeChanging="gvAdmissionsOpen_OnPageSizeChanging" OnRowCommand="gvAdmissionsOpen_OnRowCommand">
		<Columns>
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireHyperLinkButton ToolTip="Edit" ButtonType="OnlyIcon" runat="server" Glyphicon="edit" NavigateUrl='<%# Aspire.Web.Sys.Admin.Admissions.AdmissionOpen.GetPageUrl((int) this.Eval("AdmissionOpenProgramID")) %>' />
					<aspire:AspireLinkButton ToolTip="Delete" runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%# this.Eval("AdmissionOpenProgramID") %>'
						ConfirmMessage='<%# this.Eval("ProgramAlias","Are you sure you want to delete admission open for program \"{0}\"")%>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Program" SortExpression="ProgramAlias">
				<ItemTemplate>
					<span class="label label-info"><%#: Item.InstituteAlias %></span>
					<span class="label label-success"><%#: Item.ShiftsFullName %></span>
					<span class="label label-default" title="Offered Semester > Final Semester > Max. Semester"><%#: $"{Item.Semester} > {Item.FinalSemester} > {Item.MaxSemester}" %></span>
					<div>
						<%#: Item.ProgramAlias %>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Open Dates (International Students)" SortExpression="AdmissionOpenDatesForForeigners">
				<ItemTemplate>
					<%#: Item.AdmissionOpenDatesForForeigners %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Last Date of Processing Fee" SortExpression="AdmissionProcessingFeeDueDate">
				<ItemTemplate>
					<span class="label label-default">Rs. <%#: Item.AdmissionProcessingFee %></span>
					<div>
						<%#: Item.AdmissionProcessingFeeDueDate %>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Open Dates (National Students)" SortExpression="AdmissionOpenDates">
				<ItemTemplate>
					<%#: Item.AdmissionOpenDates %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Entry Test" SortExpression="EntryTestDates">
				<ItemTemplate>
					<span class="label label-info" runat="server" visible="<%# Item.EntryTestDuration!=null %>"><%#: Item.EntryTestDuration %> Minutes</span>
					<span class="label label-default" runat="server"><%#: Item.EntryTestTypeFullName %></span>
					<div>
						<%#: Item.EntryTestDates %>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Interview Dates" SortExpression="InterviewDates">
				<ItemTemplate>
					<%#: Item.InterviewDates %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Login Allowed End Date" SortExpression="LoginAllowedEndDate">
				<ItemTemplate>
					<%#: Item.LoginAllowedEndDate %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Equivalent Programs">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CausesValidation="false" CommandName="EquivalentProgram" Text="<%# Item.EquivalentPrograms %>" EncryptedCommandArgument="<%# Item.AdmissionOpenProgramID %>" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Intake Target (Students Count)" SortExpression="IntakeTargetStudentsCount">
				<ItemTemplate>
					<%#: Item.IntakeTargetStudentsCount %>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalEquivalentPrograms">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateEquivalentProgramModal">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="modalEquivalentProgramAlert" />
					<aspire:AspireGridView runat="server" ID="gvAdmissionsOpenProgramEquivalents" AutoGenerateColumns="False" OnRowCommand="gvAdmissionsOpenProgramEquivalents_OnRowCommand">
						<Columns>
							<asp:TemplateField HeaderText="#">
								<ItemTemplate>
									<%#: Container.DataItemIndex + 1 %>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField HeaderText="Equivalent Program" DataField="Text" />
							<asp:TemplateField HeaderText="Action">
								<ItemTemplate>
									<aspire:AspireLinkButton runat="server" Glyphicon="remove" CausesValidation="false" CommandName="Delete" EncryptedCommandArgument='<%# this.Eval("ID") %>' ConfirmMessage='<%# this.Eval("Text","Are you sure you want to delete equivalent admission open program \"{0}\"")%>' />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</aspire:AspireGridView>
					<p></p>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Equivalent Program:" AssociatedControlID="ddlEquivalentProgram" />
						<aspire:AspireDropDownList runat="server" ID="ddlEquivalentProgram" ValidationGroup="AddEquivalentProgram" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlEquivalentProgram" ValidationGroup="AddEquivalentProgram" ErrorMessage="This field is required." />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnAddEquivalents" Text="Add" ValidationGroup="AddEquivalentProgram" ButtonType="Primary" OnClick="btnAddEquivalents_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
