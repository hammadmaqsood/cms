﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Web.Sys.Admin.Admissions
{


	public partial class AdmissionsOpen
	{

		/// <summary>
		/// hlAdd control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireHyperLinkButton hlAdd;

		/// <summary>
		/// ddlInstitutes control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlInstitutes;

		/// <summary>
		/// ddlSemesters control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlSemesters;

		/// <summary>
		/// gvAdmissionsOpen control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireGridView gvAdmissionsOpen;

		/// <summary>
		/// modalEquivalentPrograms control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireModal modalEquivalentPrograms;

		/// <summary>
		/// updateEquivalentProgramModal control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::System.Web.UI.UpdatePanel updateEquivalentProgramModal;

		/// <summary>
		/// modalEquivalentProgramAlert control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireAlert modalEquivalentProgramAlert;

		/// <summary>
		/// gvAdmissionsOpenProgramEquivalents control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireGridView gvAdmissionsOpenProgramEquivalents;

		/// <summary>
		/// ddlEquivalentProgram control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireDropDownList ddlEquivalentProgram;

		/// <summary>
		/// btnAddEquivalents control.
		/// </summary>
		/// <remarks>
		/// Auto-generated field.
		/// To modify move field declaration from designer file to code-behind file.
		/// </remarks>
		protected global::Aspire.Lib.WebControls.AspireButton btnAddEquivalents;
	}
}
