﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Admissions
{
	[AspirePage("A40BBFDE-A349-4C08-9684-18C2248E5357", UserTypes.Admins, "~/Sys/Admin/Admissions/CBTSessions.aspx", "CBT Sessions", true, AspireModules.Admissions)]
	public partial class CBTSessions : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.Admissions.Module, new []{UserGroupPermission.PermissionValues.Allowed, } },
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "CBT Sessions";

		public static string GetPageURL(int? instituteID)
		{
			return typeof(CBTSessions).GetAspirePageAttribute().PageUrl.AttachQueryParam("InstituteID", instituteID);
		}

		public static void Redirect(int? instituteID)
		{
			Redirect(GetPageURL(instituteID));
		}

		private void Refresh()
		{
			Redirect(this.ddlInstituteIDFilter.SelectedValue.ToNullableInt());
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("InstituteAlias", SortDirection.Ascending);
				this.ddlInstituteIDFilter.FillInstitutesForAdmin(CommonListItems.All).SetSelectedValueIfNotPostback("InstituteID");
				this.ddlInstituteIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlInstituteIDFilter_OnSelectedIndexChanged(object o, EventArgs e)
		{
			this.GetData(0);
		}

		protected void gvCBTSessions_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvCBTSessions_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(this.gvCBTSessions.PageIndex);
		}

		protected void gvCBTSessions_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvCBTSessions.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvCBTSessions_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageCBTSessions, UserGroupPermission.PermissionValues.Allowed);
					this.ShowModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageCBTSessions, UserGroupPermission.PermissionValues.Allowed);
					var cbtSessionID = e.DecryptedCommandArgumentToInt();
					var result = Aspire.BL.Core.Admissions.Admin.CBTSessions.DeleteCBTSession(cbtSessionID, this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case BL.Core.Admissions.Admin.CBTSessions.DeleteCBTSessionStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.Admissions.Admin.CBTSessions.DeleteCBTSessionStatuses.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("CBT Session");
							break;
						case BL.Core.Admissions.Admin.CBTSessions.DeleteCBTSessionStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("CBT Session");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.GetData(0);
					break;
			}
		}

		private void GetData(int pageIndex)
		{
			var cbtSessions = Aspire.BL.Core.Admissions.Admin.CBTSessions.GetCBTSessions(this.ddlInstituteIDFilter.SelectedValue.ToNullableInt(), null, pageIndex, this.gvCBTSessions.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out int virtualItemCount, this.AdminIdentity.LoginSessionGuid);
			this.gvCBTSessions.DataBind(cbtSessions, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		protected void btnAddSession_OnClick(object sender, EventArgs e)
		{
			this.ShowModal(null);
		}

		private void ShowModal(int? cbtSessionID)
		{
			this.ddlInstituteID.FillInstitutesForAdmin();
			this.ViewState["CBTSessionID"] = null;
			if (cbtSessionID == null)
			{
				this.ddlInstituteID.Enabled = true;
				this.dtpStartTime.SelectedDate = null;
				this.tbDurationInMinutes.Text = null;
				this.ddlInstituteID_OnSelectedIndexChanged(null, null);
				this.modalCBTSession.HeaderText = "Add CBT Session";
			}
			else
			{
				var cbtSession = Aspire.BL.Core.Admissions.Admin.CBTSessions.GetCBTSession(cbtSessionID.Value, this.AdminIdentity.LoginSessionGuid);
				if (cbtSession == null)
				{
					this.AddNoRecordFoundAlert();
					this.GetData(0);
					return;
				}
				this.ddlInstituteID.SelectedValue = cbtSession.InstituteID.ToString();
				this.ddlInstituteID.Enabled = false;
				this.dtpStartTime.SelectedDate = cbtSession.StartTime;
				this.tbDurationInMinutes.Text = cbtSession.DurationInMinutes.ToString();

				var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
				var cbtLabs = Aspire.BL.Core.Admissions.Admin.CBTSessions.GetCBTLabs(instituteID, this.AdminIdentity.LoginSessionGuid);
				var labs = cbtLabs.Select(l => new
				{
					l.CBTLabID,
					l.LabName,
					Computers = cbtSession.Labs.SingleOrDefault(sl => sl.CBTLabID == l.CBTLabID)?.Computers ?? l.Computers,
					Selected = cbtSession.Labs.Any(sl => sl.CBTLabID == l.CBTLabID),
				}).ToList();
				this.repeaterCBTLabs.DataBind(labs);

				this.modalCBTSession.HeaderText = "Edit CBT Session";
				this.ViewState["CBTSessionID"] = cbtSessionID.Value;
			}
			this.modalCBTSession.Show();
		}

		protected void ddlInstituteID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
			var cbtLabs = Aspire.BL.Core.Admissions.Admin.CBTSessions.GetCBTLabs(instituteID, this.AdminIdentity.LoginSessionGuid);
			var labs = cbtLabs.Select(l => new
			{
				l.CBTLabID,
				l.LabName,
				l.Computers,
				Selected = true,
			}).ToList();
			this.repeaterCBTLabs.DataBind(labs);
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var cbtSessionID = this.ViewState["CBTSessionID"] as int?;
			var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
			var startTime = this.dtpStartTime.SelectedDate.Value;
			var durationInMinutes = this.tbDurationInMinutes.Text.ToByte();
			var labs = this.repeaterCBTLabs.Items.Cast<RepeaterItem>()
				.Where(i => i.ItemType == ListItemType.AlternatingItem || i.ItemType == ListItemType.Item)
				.Select(i =>
				{
					var selected = ((Aspire.Lib.WebControls.AspireCheckBox)i.FindControl("cbSelect")).Checked;
					var cbtLabID = ((Aspire.Lib.WebControls.HiddenField)i.FindControl("hfCBTLabID")).Value.ToInt();
					var computers = ((Aspire.Lib.WebControls.AspireTextBox)i.FindControl("tbComputers")).Text.ToNullableByte();
					if (selected)
						return new
						{
							CBTLabID = cbtLabID,
							Computers = computers.Value,
						};
					return null;
				}).Where(i => i != null).ToDictionary(i => i.CBTLabID, i => i.Computers);
			if (!labs.Any())
			{
				this.alertModalCBTSession.AddErrorAlert("No Lab selected.");
				this.updatePanelCBTSession.Update();
				return;
			}

			if (cbtSessionID == null)
			{
				var status = Aspire.BL.Core.Admissions.Admin.CBTSessions.AddCBTSession(instituteID, startTime, durationInMinutes, labs, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.Admissions.Admin.CBTSessions.AddCBTSessionStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("CBT Session");
						this.Refresh();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var status = Aspire.BL.Core.Admissions.Admin.CBTSessions.UpdateCBTSession(cbtSessionID.Value, startTime, durationInMinutes, labs, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.Admissions.Admin.CBTSessions.UpdateCBTSessionStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.Refresh();
						break;
					case BL.Core.Admissions.Admin.CBTSessions.UpdateCBTSessionStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("CBT Session");
						this.Refresh();
						break;
					case BL.Core.Admissions.Admin.CBTSessions.UpdateCBTSessionStatuses.CannotUpdateChildRecordExists:
						this.alertModalCBTSession.AddErrorAlert("CBT Session cannot be updated because child record exists.");
						this.updatePanelCBTSession.Update();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
	}
}