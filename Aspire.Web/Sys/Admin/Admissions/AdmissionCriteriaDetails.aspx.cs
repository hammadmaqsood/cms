﻿using Aspire.BL.Core.Admissions.Admin;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Admissions
{
	[AspirePage("8ABC9A84-2656-4F26-8C84-FD83A872CE78", UserTypes.Admins, "~/Sys/Admin/Admissions/AdmissionCriteriaDetails.aspx", "Admission Criteria Details", true, AspireModules.Admissions)]
	public partial class AdmissionCriteriaDetails : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.Admissions.Module, new []{UserGroupPermission.PermissionValues.Allowed, } },
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Admission Criteria Details";

		public static string GetPageUrl(int? admissionCriteriaID)
		{
			return GetPageUrl<AdmissionCriteriaDetails>().AttachQueryParam("AdmissionCriteriaID", admissionCriteriaID);
		}

		private int? AdmissionCriteriaID => this.Request.GetParameterValue<int>("AdmissionCriteriaID");
		private int InstituteID => this.ddlInstituteID.SelectedValue.ToByte();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("PreReqQualification", SortDirection.Ascending);
				this.ViewState.SetSortProperties1("DegreeType", SortDirection.Ascending);
				this.ViewState.SetSortProperties2("Subjects", SortDirection.Ascending);
				this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.Select);
				if (this.ddlInstituteID.Items.Count <= 1)
				{
					this.AddErrorAlert("No institute found.");
					Redirect<AdmissionCriteria>();
					return;
				}

				if (this.AdmissionCriteriaID != null)
				{
					this.btnSaveCriteriaName.Text = @"Save";
					this.panelDetails.Visible = true;

					var admissionCriteria = BL.Core.Admissions.Admin.AdmissionCriterias.GetAdmissionCriteria(this.AdmissionCriteriaID.Value, this.AdminIdentity.LoginSessionGuid);
					if (admissionCriteria == null)
					{
						this.AddNoRecordFoundAlert();
						Redirect<AdmissionCriteria>();
						return;
					}
					this.ddlInstituteID.SelectedValue = admissionCriteria.InstituteID.ToString();
					this.ddlInstituteID.Enabled = false;
					this.tbCriteriaName.Text = admissionCriteria.CriteriaName;

					this.SetgvAdmissionCriteriaPreReqs();
				}
				else
				{
					this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageAdmissionCriteria, UserGroupPermission.PermissionValues.Allowed);
					this.btnSaveCriteriaName.Text = @"Add";
					this.panelDetails.Visible = false;
				}
			}
		}

		private void RefreshPage()
		{
			Redirect(GetPageUrl(this.AdmissionCriteriaID));
		}

		#region Add / Update Admission Criteria

		protected void btnSaveCriteriaName_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageAdmissionCriteria, UserGroupPermission.PermissionValues.Allowed);
			if (this.AdmissionCriteriaID == null)// going for Add new Admission Criteria
			{
				var result = BL.Core.Admissions.Admin.AdmissionCriterias.AddAdmissionCriteria(this.InstituteID, this.tbCriteriaName.Text, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case AdmissionCriterias.AddAdmissionCriteriaStatuses.AlreadyExists:
						this.AddErrorAlert("Admission criteria name already exists.");
						break;
					case AdmissionCriterias.AddAdmissionCriteriaStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Admission criteria");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				Redirect<AdmissionCriteria>();// b/c no return the ID of newly added Admission Criteria.
			}
			else
			{
				var result = BL.Core.Admissions.Admin.AdmissionCriterias.UpdateAdmissionCriteria(this.AdmissionCriteriaID.Value, this.tbCriteriaName.Text, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case AdmissionCriterias.UpdateAdmissionCriteriaStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						break;
					case AdmissionCriterias.UpdateAdmissionCriteriaStatuses.AlreadyExists:
						this.AddErrorAlert("Admission criteria name already exists.");
						break;
					case AdmissionCriterias.UpdateAdmissionCriteriaStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Admission criteria");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				this.RefreshPage();
			}
		}

		#endregion

		#region AdmissionCriteriaPreReq

		protected void gvAdmissionCriteriaPreReqs_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayAddPreReqModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageAdmissionCriteria, UserGroupPermission.PermissionValues.Allowed);
					var admissionCriteriaPreReqIDDelete = e.DecryptedCommandArgumentToInt();
					var result = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqs.DeleteAdmissionCriteriaPreReq(admissionCriteriaPreReqIDDelete, this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case AdmissionCriteriaPreReqs.DeleteAdmissionCriteriaPreReqStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case AdmissionCriteriaPreReqs.DeleteAdmissionCriteriaPreReqStatuses.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Admission criteria pre-requisite");
							break;
						case AdmissionCriteriaPreReqs.DeleteAdmissionCriteriaPreReqStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Admission criteria pre-requisite");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.RefreshPage();
					break;
			}
		}

		protected void gvAdmissionCriteriaPreReqs_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.SetgvAdmissionCriteriaPreReqsDeg();
		}

		protected void gvAdmissionCriteriaPreReqs_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.SetgvAdmissionCriteriaPreReqs();
		}

		protected void btnAdddPreReq_OnClick(object sender, EventArgs e)
		{
			this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageAdmissionCriteria, UserGroupPermission.PermissionValues.Allowed);
			this.DisplayAddPreReqModal(null);
		}

		protected void btnSavePreReq_OnClick(object sender, EventArgs e)
		{
			this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageAdmissionCriteria, UserGroupPermission.PermissionValues.Allowed);
			var admissionCriteriaPreReqID = (int?)this.ViewState[nameof(Model.Entities.AdmissionCriteriaPreReq.AdmissionCriteriaPreReqID)];
			var preReqQualification = this.tbPreReqQualification.Text;
			var preReqQualificationResultStatus = this.tbResultStatus.Text;
			var resultAwaitedDegreeType = this.ddlResultAwaitedDegreeType.SelectedValue.ToNullableByte();
			if (admissionCriteriaPreReqID == null)
			{
				var admissionCriteriaID = this.AdmissionCriteriaID.Value;
				var result = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqs.AddAdmissionCriteriaPreReq(admissionCriteriaID, preReqQualification, preReqQualificationResultStatus, resultAwaitedDegreeType, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case AdmissionCriteriaPreReqs.AddAdmissionCriteriaPreReqStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Admission criteria pre-requisite");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				this.RefreshPage();
			}
			else
			{
				var result = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqs.UpdateAdmissionCriteriaPreReq(admissionCriteriaPreReqID, preReqQualification, preReqQualificationResultStatus, resultAwaitedDegreeType, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case AdmissionCriteriaPreReqs.UpdateAdmissionCriteriaPreReqStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						break;
					case AdmissionCriteriaPreReqs.UpdateAdmissionCriteriaPreReqStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Admission criteria pre-requisite");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				this.RefreshPage();
			}
		}

		#endregion

		#region AdmissionCriteriaPreRegDegree

		protected void gvAdmissionCriteriaPreReqDegrees_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayPreReqDegreeModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageAdmissionCriteria, UserGroupPermission.PermissionValues.Allowed);
					var admissionCriteriaPreReqDegreeID = e.DecryptedCommandArgumentToInt();
					var result = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqDegrees.DeleteAdmissionCriteriaPreReqDegree(admissionCriteriaPreReqDegreeID, this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case AdmissionCriteriaPreReqDegrees.DeleteAdmissionCriteriaPreReqDegreeStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case AdmissionCriteriaPreReqDegrees.DeleteAdmissionCriteriaPreReqDegreeStatuses.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Admission criteria pre-requisite degree");
							break;
						case AdmissionCriteriaPreReqDegrees.DeleteAdmissionCriteriaPreReqDegreeStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Admission criteria pre-requisite degree");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.RefreshPage();
					break;
			}
		}

		protected void gvAdmissionCriteriaPreReqDegrees_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.SetgvAdmissionCriteriaPreReqsDegSubject();
		}

		protected void gvAdmissionCriteriaPreReqDegrees_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs1(this.ViewState);
			this.SetgvAdmissionCriteriaPreReqsDeg();
		}

		protected void btnAddPreReqDegree_OnClick(object sender, EventArgs e)
		{
			this.DisplayPreReqDegreeModal(null);
		}

		protected void btnSavePreRegDegree_OnClick(object sender, EventArgs e)
		{
			this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageAdmissionCriteria, UserGroupPermission.PermissionValues.Allowed);
			var admissionCriteriaPreReqDegree = new Model.Entities.AdmissionCriteriaPreReqDegree();
			admissionCriteriaPreReqDegree.DegreeType = this.ddlDegree.SelectedValue.ToByte();
			admissionCriteriaPreReqDegree.AnnualExamSystem = this.rblAnnualExamSystem.SelectedValue.ToBoolean();
			admissionCriteriaPreReqDegree.AnnualExamSystemMinimumPercentage = this.tbMinimumPercentage.Text.ToNullableInt();
			admissionCriteriaPreReqDegree.SemesterSystem = this.rblSemesterExamSystem.SelectedValue.ToBoolean();
			admissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf4 = this.tbMinimumCGPA4.Text.ToNullableDouble();
			admissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf5 = this.tbMinimumCGPA5.Text.ToNullableDouble();
			admissionCriteriaPreReqDegree.DisplaySequence = this.ddlDisplaySequence.SelectedValue.ToByte();
			admissionCriteriaPreReqDegree.MeritListWeightage = this.tbMeritListWeightage.Text.ToByte();

			if (this.ViewState[nameof(Model.Entities.AdmissionCriteriaPreReqDegree.AdmissionCriteriaPreReqDegreeID)] == null)
			{
				admissionCriteriaPreReqDegree.AdmissionCriteriaPreReqID = (int)this.gvAdmissionCriteriaPreReqs.SelectedValue;
				var result = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqDegrees.AddAdmissionCriteriaPreReqDegree(admissionCriteriaPreReqDegree, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case AdmissionCriteriaPreReqDegrees.AddAdmissionCriteriaPreReqDegreeStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Admission criteria pre-requisite degree");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				this.RefreshPage();
			}
			else
			{
				admissionCriteriaPreReqDegree.AdmissionCriteriaPreReqDegreeID = (int)this.ViewState["AdmissionCriteriaPreReqDegreeID"];
				var result = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqDegrees.UpdateAdmissionCriteriaPreReqDegree(admissionCriteriaPreReqDegree, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case AdmissionCriteriaPreReqDegrees.UpdateAdmissionCriteriaPreReqDegreeStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						break;
					case AdmissionCriteriaPreReqDegrees.UpdateAdmissionCriteriaPreReqDegreeStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Admission criteria pre-requisite degree");
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				this.RefreshPage();
			}
		}

		protected void rblAnnualExamSystem_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var isAnnual = this.rblAnnualExamSystem.SelectedValue == CommonListItems.YesValue;

			if (!isAnnual)
				this.tbMinimumPercentage.Text = null;
			this.dvMinimumPercentage.Visible = isAnnual;
			this.rdvMinimumPercentage.Enabled = isAnnual;

			var isSemester = this.rblSemesterExamSystem.SelectedValue == CommonListItems.YesValue;
			if (isAnnual == false && isSemester == false)
			{
				this.rblSemesterExamSystem.SelectedValue = CommonListItems.YesValue;
				this.rblSemesterExamSystem_OnSelectedIndexChanged(null, null);
			}
		}

		protected void rblSemesterExamSystem_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var isSemester = this.rblSemesterExamSystem.SelectedValue == CommonListItems.YesValue;
			this.dvMinimumCGPA4.Visible = isSemester;
			this.dvMinimumCGPA5.Visible = isSemester;
			this.rdvMinimumCGPA4.Enabled = isSemester;
			this.rdvMinimumCGPA5.Enabled = isSemester;
			if (!isSemester)
			{
				this.tbMinimumCGPA4.Text = null;
				this.tbMinimumCGPA5.Text = null;
			}

			var isAnnual = this.rblAnnualExamSystem.SelectedValue == CommonListItems.YesValue;
			if (isSemester == false && isAnnual == false)
			{
				this.rblAnnualExamSystem.SelectedValue = CommonListItems.YesValue;
				this.rblAnnualExamSystem_OnSelectedIndexChanged(null, null);
			}
		}

		#endregion

		#region AdmissionCriteriaPreReqDegreeSubjects

		protected void gvAdmissionCriteriaPreReqDegreeSubjects_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayPreReqDegreeSubjectsModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageAdmissionCriteria, UserGroupPermission.PermissionValues.Allowed);
					var admissionCriteriaPreReqDegSubjectID = e.DecryptedCommandArgumentToInt();
					var result = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqDegreeSubjects.DeleteAdmissionCriteriaPreReqDegreeSubject(admissionCriteriaPreReqDegSubjectID, this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case AdmissionCriteriaPreReqDegreeSubjects.DeleteAdmissionCriteriaPreReqDegreeSubjectStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case AdmissionCriteriaPreReqDegreeSubjects.DeleteAdmissionCriteriaPreReqDegreeSubjectStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Subjects", true);
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.RefreshPage();
					break;
			}
		}

		protected void gvAdmissionCriteriaPreReqDegreeSubjects_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs2(this.ViewState);
			this.SetgvAdmissionCriteriaPreReqsDegSubject();
		}

		protected void btnAddSubjects_OnClick(object sender, EventArgs e)
		{
			this.DisplayPreReqDegreeSubjectsModal(null);
		}

		protected void btnSavePreReqDegreeSubjt_OnClick(object sender, EventArgs e)
		{
			this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageAdmissionCriteria, UserGroupPermission.PermissionValues.Allowed);
			var admissionCriteriaPreReqDegSubjectID = (int?)this.ViewState["AdmissionCriteriaPreReqDegreeSubjectID"];
			if (admissionCriteriaPreReqDegSubjectID == null)
			{
				var admissionCriteriaPreReqDegreeID = (int)this.gvAdmissionCriteriaPreReqDegrees.SelectedValue;
				var result = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqDegreeSubjects.AddAdmissionCriteriaPreReqDegreeSubject(admissionCriteriaPreReqDegreeID, this.tbSubjects.Text, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case AdmissionCriteriaPreReqDegreeSubjects.AddAdmissionCriteriaPreReqDegreeSubjectStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Subjects", true);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				this.RefreshPage();
			}
			else
			{
				var result = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqDegreeSubjects.UpdateAdmissionCriteriaPreReqDegreeSubject(admissionCriteriaPreReqDegSubjectID, this.tbSubjects.Text, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case AdmissionCriteriaPreReqDegreeSubjects.UpdateAdmissionCriteriaPreReqDegreeSubjectStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						break;
					case AdmissionCriteriaPreReqDegreeSubjects.UpdateAdmissionCriteriaPreReqDegreeSubjectStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Admission criteria pre-requisite degree subjects", true);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				this.RefreshPage();
			}
		}

		#endregion

		#region Page Specific Functions (private) 

		private void SetgvAdmissionCriteriaPreReqs()
		{
			var admissionCriteriaID = this.AdmissionCriteriaID;
			if (admissionCriteriaID == null)
				throw new InvalidOperationException();

			var admissionCriteriaPreReq = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqs.GetAdmissionCriteriaPreReqs(this.InstituteID, admissionCriteriaID.Value, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
			this.gvAdmissionCriteriaPreReqs.DataBind(admissionCriteriaPreReq, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
			if (admissionCriteriaPreReq.Any())
				this.gvAdmissionCriteriaPreReqs.SelectedIndex = 0;
			else
				this.btnAddPreReqDegree.Enabled = false;

			this.gvAdmissionCriteriaPreReqs_SelectedIndexChanged(null, null);
		}

		private void SetgvAdmissionCriteriaPreReqsDeg()
		{
			var admissionCriteriaPreReqID = (int?)this.gvAdmissionCriteriaPreReqs.SelectedValue;
			if (admissionCriteriaPreReqID != null)
			{
				var admissionCriteriaPreReqDegree = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqDegrees.GetAdmissionCriteriaPreReqDegrees(admissionCriteriaPreReqID.Value, this.ViewState.GetSortExpression1(), this.ViewState.GetSortDirection1(), this.AdminIdentity.LoginSessionGuid);
				this.gvAdmissionCriteriaPreReqDegrees.DataBind(admissionCriteriaPreReqDegree, this.ViewState.GetSortExpression1(), this.ViewState.GetSortDirection1());
				if (admissionCriteriaPreReqDegree.Any())
					this.gvAdmissionCriteriaPreReqDegrees.SelectedIndex = 0;
				else
					this.gvAdmissionCriteriaPreReqDegrees.SelectedIndex = -1;
				this.btnAddPreReqDegree.Enabled = true;
			}
			else
			{
				this.gvAdmissionCriteriaPreReqDegrees.SelectedIndex = -1;
				this.gvAdmissionCriteriaPreReqDegrees.DataBind(null);
				this.btnAddPreReqDegree.Enabled = false;
			}
			this.gvAdmissionCriteriaPreReqDegrees_OnSelectedIndexChanged(null, null);
		}

		private void SetgvAdmissionCriteriaPreReqsDegSubject()
		{
			var admissionCriteriaPreReqDegreeID = (int?)this.gvAdmissionCriteriaPreReqDegrees.SelectedValue;
			if (admissionCriteriaPreReqDegreeID != null)
			{
				var admissionCriteriaPreReqDegreeSubjt = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqDegreeSubjects.GetAdmissionCriteriaPreReqDegreeSubjects(admissionCriteriaPreReqDegreeID.Value, this.ViewState.GetSortExpression2(), this.ViewState.GetSortDirection2(), this.AdminIdentity.LoginSessionGuid);
				this.gvAdmissionCriteriaPreReqDegreeSubjects.DataBind(admissionCriteriaPreReqDegreeSubjt, this.ViewState.GetSortExpression2(), this.ViewState.GetSortDirection2());
				this.btnAddSubjects.Enabled = true;
			}
			else
			{
				this.gvAdmissionCriteriaPreReqDegreeSubjects.DataBindNull();
				this.btnAddSubjects.Enabled = false;
			}
		}

		private void DisplayAddPreReqModal(int? admissionCriteriaPreReqID)
		{
			if (admissionCriteriaPreReqID == null)
			{
				this.tbPreReqQualification.Text = null;
				this.tbResultStatus.Text = null;
				this.ddlResultAwaitedDegreeType.FillDegreeTypes(CommonListItems.None).ClearSelection();
				this.btnSavePreReq.Text = @"Add";
				this.modalPreReq.HeaderText = "Add Pre-Requisite";
				this.modalPreReq.Show();
				this.ViewState[nameof(Model.Entities.AdmissionCriteriaPreReq.AdmissionCriteriaPreReqID)] = null;
			}
			else
			{
				var admissionCriteriaPreReq = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqs.GetAdmissionCriteriaPreReq(admissionCriteriaPreReqID.Value, this.AdminIdentity.LoginSessionGuid);
				if (admissionCriteriaPreReq == null)
				{
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
				}
				else
				{
					this.tbPreReqQualification.Text = admissionCriteriaPreReq.PreReqQualification;
					this.tbResultStatus.Text = admissionCriteriaPreReq.PreReqQualificationResultStatus;
					this.ddlResultAwaitedDegreeType.FillDegreeTypes(CommonListItems.None).SetEnumValue(admissionCriteriaPreReq.ResultAwaitedDegreeTypeEnum);
					this.btnSavePreReq.Text = @"Save";
					this.modalPreReq.HeaderText = "Edit Pre-Requisite";
					this.modalPreReq.Show();
					this.ViewState[nameof(Model.Entities.AdmissionCriteriaPreReq.AdmissionCriteriaPreReqID)] = admissionCriteriaPreReq.AdmissionCriteriaPreReqID;
				}
			}
		}

		private void DisplayPreReqDegreeModal(int? admissionCriteriaPreRegDegreeID)
		{
			var admissionCriteriaPreRegID = (int?)this.gvAdmissionCriteriaPreReqs.SelectedValue;
			if (admissionCriteriaPreRegID == null)
			{
				this.RefreshPage();
				return;
			}

			var admissionCriteriaPreReq = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqs.GetAdmissionCriteriaPreReq(admissionCriteriaPreRegID.Value, this.AdminIdentity.LoginSessionGuid);
			if (admissionCriteriaPreReq == null)
			{
				this.AddNoRecordFoundAlert();
				this.RefreshPage();
				return;
			}
			this.lblPreReqValueQualificationforDeg.Text = admissionCriteriaPreReq.PreReqQualification;
			this.lblResultStatusValueforDeg.Text = admissionCriteriaPreReq.PreReqQualificationResultStatus;
			this.lblResultAwaitedDegreeTypeValueforDeg.Text = admissionCriteriaPreReq.ResultAwaitedDegreeTypeFullName.ToNAIfNullOrEmpty();
			this.ddlDegree.FillDegreeTypes();
			this.rblAnnualExamSystem.FillYesNo();
			this.rblSemesterExamSystem.FillYesNo();
			this.ddlDisplaySequence.DataBind(Enumerable.Range(0, 30).Select(i => i.ToString()));

			if (admissionCriteriaPreRegDegreeID == null)
			{
				this.ddlDegree.ClearSelection();
				this.rblAnnualExamSystem.SetSelectedValueYes();
				this.rblAnnualExamSystem_OnSelectedIndexChanged(null, null);
				this.tbMinimumPercentage.Text = null;
				this.rblSemesterExamSystem.SetSelectedValueNo();
				this.rblSemesterExamSystem_OnSelectedIndexChanged(null, null);
				this.tbMinimumCGPA4.Text = null;
				this.tbMinimumCGPA5.Text = null;
				this.ddlDisplaySequence.ClearSelection();
				this.tbMeritListWeightage.Text = null;
				this.ViewState[nameof(Model.Entities.AdmissionCriteriaPreReqDegree.AdmissionCriteriaPreReqDegreeID)] = null;
				this.btnSavePreRegDegree.Text = @"Add";
				this.modalPreReqDegree.HeaderText = "Add Pre-Requisite Degree";
				this.modalPreReqDegree.Show();
			}
			else
			{
				var admissionCriteriaPreReqDeg = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqDegrees.GetAdmissionCriteriaPreReqDegree(admissionCriteriaPreRegDegreeID.Value, this.AdminIdentity.LoginSessionGuid);
				if (admissionCriteriaPreReqDeg == null)
				{
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				}

				this.ddlDegree.SetEnumValue(admissionCriteriaPreReqDeg.DegreeTypeEnum);
				this.rblAnnualExamSystem.SetSelectedValue(admissionCriteriaPreReqDeg.AnnualExamSystem);
				this.rblAnnualExamSystem_OnSelectedIndexChanged(null, null);
				this.tbMinimumPercentage.Text = admissionCriteriaPreReqDeg.AnnualExamSystemMinimumPercentage.ToString();
				this.rblSemesterExamSystem.SetSelectedValue(admissionCriteriaPreReqDeg.SemesterSystem);
				this.rblSemesterExamSystem_OnSelectedIndexChanged(null, null);
				this.tbMinimumCGPA4.Text = admissionCriteriaPreReqDeg.SemesterSystemMinCGPAOutOf4.ToString();
				this.tbMinimumCGPA5.Text = admissionCriteriaPreReqDeg.SemesterSystemMinCGPAOutOf5.ToString();
				this.ddlDisplaySequence.SelectedValue = admissionCriteriaPreReqDeg.DisplaySequence.ToString();
				this.tbMeritListWeightage.Text = admissionCriteriaPreReqDeg.MeritListWeightage.ToString();
				this.ViewState[nameof(Model.Entities.AdmissionCriteriaPreReqDegree.AdmissionCriteriaPreReqDegreeID)] = admissionCriteriaPreReqDeg.AdmissionCriteriaPreReqDegreeID;
				this.btnSavePreRegDegree.Text = @"Save";
				this.modalPreReqDegree.HeaderText = "Edit Pre-Requisite Degree";
				this.modalPreReqDegree.Show();
			}
		}

		private void DisplayPreReqDegreeSubjectsModal(int? admissionCriteriaPreReqDegreeSubjectID)
		{
			var admissionCriteriaPreRegID = (int?)this.gvAdmissionCriteriaPreReqs.SelectedValue;
			if (admissionCriteriaPreRegID == null)
			{
				this.RefreshPage();
				return;
			}

			var admissionCriteriaPreReq = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqs.GetAdmissionCriteriaPreReq(admissionCriteriaPreRegID.Value, this.AdminIdentity.LoginSessionGuid);
			if (admissionCriteriaPreReq == null)
			{
				this.AddNoRecordFoundAlert();
				this.RefreshPage();
				return;
			}

			this.lblPreReqQualificationValueforSubjt.Text = admissionCriteriaPreReq.PreReqQualification;
			this.lblResultStatusValueforSubjt.Text = admissionCriteriaPreReq.PreReqQualificationResultStatus;
			this.lblResultAwaitedDegreeTypeValueforSubjt.Text = admissionCriteriaPreReq.ResultAwaitedDegreeTypeFullName.ToNAIfNullOrEmpty();

			var admissionCriteriaPreRegDegreeID = (int?)this.gvAdmissionCriteriaPreReqDegrees.SelectedValue;
			if (admissionCriteriaPreRegDegreeID == null)
			{
				this.AddNoRecordFoundAlert();
				this.RefreshPage();
				return;
			}
			var admissionCriteriaPreReqDegree = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqDegrees.GetAdmissionCriteriaPreReqDegree(admissionCriteriaPreRegDegreeID.Value, this.AdminIdentity.LoginSessionGuid);
			if (admissionCriteriaPreReqDegree == null)
			{
				this.AddNoRecordFoundAlert();
				this.RefreshPage();
				return;
			}
			this.lblDegreeValueforSubjt.Text = admissionCriteriaPreReqDegree.DegreeTypeEnumFullName;

			if (admissionCriteriaPreReqDegreeSubjectID == null)
			{
				this.tbSubjects.Text = null;
				this.btnSavePreReqDegreeSubjt.Text = @"Add";
				this.modalPreReq.HeaderText = "Add Subjects";
				this.ViewState[nameof(AdmissionCriteriaPreReqDegreeSubject.AdmissionCriteriaPreReqDegreeSubjectID)] = null;
				this.modalPreReqDegreeSubject.Show();
			}
			else
			{
				var admissionCriteriaPreReqDegreeSubject = BL.Core.Admissions.Admin.AdmissionCriteriaPreReqDegreeSubjects.GetAdmissionCriteriaPreReqDegreeSubject(admissionCriteriaPreReqDegreeSubjectID.Value, this.AdminIdentity.LoginSessionGuid);
				if (admissionCriteriaPreReqDegreeSubject == null)
				{
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				}

				this.tbSubjects.Text = admissionCriteriaPreReqDegreeSubject.Subjects;
				this.btnSavePreReqDegreeSubjt.Text = @"Save";
				this.modalPreReq.HeaderText = "Edit Subjects";
				this.ViewState[nameof(AdmissionCriteriaPreReqDegreeSubject.AdmissionCriteriaPreReqDegreeSubjectID)] = admissionCriteriaPreReqDegreeSubject.AdmissionCriteriaPreReqDegreeSubjectID;
				this.modalPreReqDegreeSubject.Show();
			}
		}

		#endregion
	}
}
