﻿using Aspire.BL.Core.Admissions.Admin;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Admissions
{
	[AspirePage("C49F19ED-65DB-4F63-BA94-2B06B9D08B7D", UserTypes.Admins, "~/Sys/Admin/Admissions/AdmissionsOpen.aspx", "Admissions Open", true, AspireModules.Admissions)]
	public partial class AdmissionsOpen : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.Admissions.Module, new []{UserGroupPermission.PermissionValues.Allowed, } },
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Admissions Open";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("ProgramAlias", SortDirection.Ascending);
				this.ddlInstitutes.FillInstitutesForAdmin(CommonListItems.All);
				if (this.ddlInstitutes.Items.Count == 0)
				{
					this.AddErrorAlert("No institute found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlSemesters.FillSemesters();
				this.ddlInstitutes_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlInstitutes_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesters_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesters_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void gvAdmissionsOpen_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvAdmissionsOpen_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvAdmissionsOpen_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvAdmissionsOpen.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvAdmissionsOpen_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "EquivalentProgram":
					e.Handled = true;
					this.DisplayEquivalentProgramsModel(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageAdmissionOpening, UserGroupPermission.PermissionValues.Allowed);
					var admissionOpenProgramID = e.DecryptedCommandArgumentToInt();
					var result = AdmissionOpenPrograms.DeleteAdmissionOpenProgram(admissionOpenProgramID, this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case AdmissionOpenPrograms.DeleteAdmissionOpenProgramStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case AdmissionOpenPrograms.DeleteAdmissionOpenProgramStatuses.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Admission open program");
							break;
						case AdmissionOpenPrograms.DeleteAdmissionOpenProgramStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Admission open program");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.GetData(0);
					break;
			}
		}

		protected void btnAddEquivalents_OnClick(object sender, EventArgs e)
		{
			this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageAdmissionOpening, UserGroupPermission.PermissionValues.Allowed);
			var admissionOpenProgramID = (int?)this.ViewState["AdmissionOpenProgramID"];
			if (admissionOpenProgramID != null)
			{
				var equivalentAdmissionOpenProgramID = this.ddlEquivalentProgram.SelectedValue.ToInt();
				var result = AdmissionOpenPrograms.AddEquivalentAdmissionOpenProgram(admissionOpenProgramID.Value, equivalentAdmissionOpenProgramID, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case AdmissionOpenPrograms.AddAdmissionOpenProgramEquivalentStatuses.AlreadyExists:
						this.modalEquivalentProgramAlert.AddMessage(AspireAlert.AlertTypes.Error, "Equivalent admission open program already exists.");
						this.updateEquivalentProgramModal.Update();
						break;
					case AdmissionOpenPrograms.AddAdmissionOpenProgramEquivalentStatuses.AddWithWarning:
						this.modalEquivalentProgramAlert.AddWarningAlert("Dates for the Equivalent Program does not match with actual program.");
						this.updateEquivalentProgramModal.Update();
						break;
					case AdmissionOpenPrograms.AddAdmissionOpenProgramEquivalentStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Equivalent admission open program");
						Redirect<AdmissionsOpen>();
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(result), result, null);
				}
			}
		}

		protected void gvAdmissionsOpenProgramEquivalents_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageAdmissionOpening, UserGroupPermission.PermissionValues.Allowed);
					var admissionOpenProgramEquivalentID = e.DecryptedCommandArgumentToInt();
					var result = AdmissionOpenPrograms.DeleteAdmissionOpenProgramEquivalent(admissionOpenProgramEquivalentID, this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case AdmissionOpenPrograms.DeleteAdmissionOpenProgramEquivalentStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case AdmissionOpenPrograms.DeleteAdmissionOpenProgramEquivalentStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Equivalent admission open program ");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					Redirect<AdmissionsOpen>();
					return;
			}
		}

		private void GetData(int pageIndex)
		{
			var semesterID = this.ddlSemesters.SelectedValue.ToNullableShort();
			var instituteID = this.ddlInstitutes.SelectedValue.ToNullableInt();
			var admissionOpen = AdmissionOpenPrograms.GetAdmissionOpenPrograms(semesterID, instituteID, pageIndex, this.gvAdmissionsOpen.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.AdminIdentity.LoginSessionGuid, out var virtualItemCount);
			this.gvAdmissionsOpen.DataBind(admissionOpen, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		private void DisplayEquivalentProgramsModel(int admissionOpenProgramID)
		{
			var record = AdmissionOpenPrograms.GetAdmissionOpenProgram(admissionOpenProgramID, this.AdminIdentity.LoginSessionGuid);
			var equivalentAdmissionOpenPrograms = AdmissionOpenPrograms.GetEquivalentAdmissionOpenPrograms(admissionOpenProgramID, this.AdminIdentity.LoginSessionGuid);
			this.gvAdmissionsOpenProgramEquivalents.DataBind(equivalentAdmissionOpenPrograms);
			this.ddlEquivalentProgram.DataBind(AdmissionOpenPrograms.GetAdmissionOpenProgramsForEquivalency(record.Value.AdmissionOpenProgram.Program.InstituteID, record.Value.AdmissionOpenProgram.SemesterID, admissionOpenProgramID, this.AdminIdentity.LoginSessionGuid));

			this.ViewState[nameof(AdmissionOpenProgram.AdmissionOpenProgramID)] = admissionOpenProgramID;
			this.modalEquivalentPrograms.HeaderText = "Equivalents Programs";
			this.modalEquivalentPrograms.Show();
		}
	}
}