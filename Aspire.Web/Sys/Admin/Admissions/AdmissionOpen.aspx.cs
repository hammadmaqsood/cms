﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Admissions
{
	[AspirePage("954BDAE0-C59F-485F-AFA9-A880C7948D83", UserTypes.Admins, "~/Sys/Admin/Admissions/AdmissionOpen.aspx", "Admission Open", true, AspireModules.Admissions)]
	public partial class AdmissionOpen : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.Admissions.Module, new []{UserGroupPermission.PermissionValues.Allowed, } },
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => this.AdmissionOpenProgramID == null ? "Open Admission" : "Edit Opened Admission";

		private int? AdmissionOpenProgramID => this.GetParameterValue<int>(nameof(this.AdmissionOpenProgramID));

		public static string GetPageUrl(int? admissionOpenProgramID)
		{
			return GetPageUrl<AdmissionOpen>().AttachQueryParam(nameof(AdmissionOpenProgramID), admissionOpenProgramID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlInstituteID.FillInstitutesForAdmin();
				this.ddlSemesterID.FillSemesters();
				this.rblIsSummerRegularSemester.FillYesNo();
				this.cbShifts.FillShifts();
				this.ddlEntryTestType.FillEnums<AdmissionOpenProgram.EntryTestTypes>();

				if (this.AdmissionOpenProgramID == null)
				{
					this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageAdmissionOpening, UserGroupPermission.PermissionValues.Allowed);
					this.ddlInstituteID.Enabled = true;
					this.ddlInstituteID.ClearSelection();
					this.ddlInstituteID_OnSelectedIndexChanged(null, null);

					this.ddlFinalSemesterID.ClearSelection();
					this.ddlMaxSemesterID.ClearSelection();

					this.cbShifts.ClearSelection();
					this.ddlAdmissionCriteria.ClearSelection();
					this.tbEligibilityCriteriaStatement.Text = null;

					this.dpAdmissionOpenDateForForeigners.SelectedDate = null;
					this.dpAdmissionCloseDateForForeigners.SelectedDate = null;

					this.dpAdmissionOpenDate.SelectedDate = null;
					this.dpAdmissionCloseDate.SelectedDate = null;

					this.tbAdmissionProcessingFee.Text = null;
					this.dtpAdmissionProcessingFeeDueDate.SelectedDate = null;

					this.ddlEntryTestType.ClearSelection();
					this.ddlEntryTestType_OnSelectedIndexChanged(null, null);
					this.tbEntryTestDuration.Text = null;

					this.dtpEntryTestStartDate.SelectedDate = null;
					this.dtpEntryTestEndDate.SelectedDate = null;

					this.dpInterviewStartDate.SelectedDate = null;
					this.dpInterviewEndDate.SelectedDate = null;

					this.dpLoginAllowedDate.SelectedDate = null;

					this.tbIntakeTargetStudentsCount.Text = null;

					this.btnSaveAdmissionOpen.Text = "Add";
				}
				else
				{
					var result = BL.Core.Admissions.Admin.AdmissionOpenPrograms.GetAdmissionOpenProgram(this.AdmissionOpenProgramID.Value, this.AdminIdentity.LoginSessionGuid);
					if (result?.AdmissionOpenProgram == null)
					{
						this.AddNoRecordFoundAlert();
						Redirect<AdmissionsOpen>();
						return;
					}
					var admissionOpenProgram = result.Value.AdmissionOpenProgram;
					this.ddlInstituteID.Enabled = false;
					this.ddlInstituteID.SelectedValue = admissionOpenProgram.Program.InstituteID.ToString();
					this.ddlInstituteID_OnSelectedIndexChanged(null, null);

					this.ddlProgramID.Enabled = false;
					this.ddlProgramID.SelectedValue = admissionOpenProgram.ProgramID.ToString();
					this.ddlProgramID_OnSelectedIndexChanged(null, null);

					this.ddlSemesterID.Enabled = false;
					this.ddlSemesterID.SelectedValue = admissionOpenProgram.SemesterID.ToString();
					this.ddlSemesterID_OnSelectedIndexChanged(null, null);

					this.rblIsSummerRegularSemester.SetBooleanValue(admissionOpenProgram.IsSummerRegularSemester);
					this.rblIsSummerRegularSemester_OnSelectedIndexChanged(null, null);

					this.ddlFinalSemesterID.SelectedValue = admissionOpenProgram.FinalSemesterID.ToString();
					this.ddlMaxSemesterID.SelectedValue = admissionOpenProgram.MaxSemesterID.ToString();

					this.cbShifts.Items.Cast<ListItem>().ForEach(i => i.Selected = admissionOpenProgram.ShiftsEnum.HasFlag(i.Value.ToEnum<Shifts>()));
					this.ddlAdmissionCriteria.SelectedValue = admissionOpenProgram.AdmissionCriteriaID.ToString();
					this.tbEligibilityCriteriaStatement.Text = admissionOpenProgram.EligibilityCriteriaStatement;

					this.dpAdmissionOpenDateForForeigners.SelectedDate = admissionOpenProgram.AdmissionOpenDateForForeigners;
					this.dpAdmissionCloseDateForForeigners.SelectedDate = admissionOpenProgram.AdmissionClosingDateForForeigners;
					this.dtvAdmissionOpenDateForForeigners.AllowNull = !result.Value.foreignStudentsFound;
					this.dtvAdmissionCloseDateForForeigners.AllowNull = !result.Value.foreignStudentsFound;

					this.dpAdmissionOpenDate.SelectedDate = admissionOpenProgram.AdmissionOpenDate;
					this.dpAdmissionCloseDate.SelectedDate = admissionOpenProgram.AdmissionClosingDate;

					this.tbAdmissionProcessingFee.Text = admissionOpenProgram.AdmissionProcessingFee.ToString();
					this.dtpAdmissionProcessingFeeDueDate.SelectedDate = admissionOpenProgram.AdmissionProcessingFeeDueDate;

					this.ddlEntryTestType.SetEnumValue(admissionOpenProgram.EntryTestTypeEnum);
					this.ddlEntryTestType_OnSelectedIndexChanged(null, null);
					this.tbEntryTestDuration.Text = admissionOpenProgram.EntryTestDuration.ToString();
					this.dtpEntryTestStartDate.SelectedDate = admissionOpenProgram.EntryTestStartDate;
					this.dtpEntryTestEndDate.SelectedDate = admissionOpenProgram.EntryTestEndDate;

					this.dpInterviewStartDate.SelectedDate = admissionOpenProgram.InterviewsStartDate;
					this.dpInterviewEndDate.SelectedDate = admissionOpenProgram.InterviewsEndDate;

					this.dpLoginAllowedDate.SelectedDate = admissionOpenProgram.LoginAllowedEndDate;

					this.tbIntakeTargetStudentsCount.Text = admissionOpenProgram.IntakeTargetStudentsCount.ToString();

					this.btnSaveAdmissionOpen.Text = @"Save";
				}
			}
		}

		protected void ddlInstituteID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
			this.ddlProgramID.FillPrograms(instituteID, null, true);
			var admissionCriteriaList = BL.Core.Admissions.Admin.AdmissionOpenPrograms.GetAdmissionCriteriaList(instituteID, this.AdminIdentity.LoginSessionGuid);
			this.ddlAdmissionCriteria.DataBind(admissionCriteriaList);
			this.ddlAdmissionCriteria.ClearSelection();
			this.ddlProgramID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var semesterTypeEnum = Semester.GetSemesterType(semesterID);
			switch (semesterTypeEnum)
			{
				case SemesterTypes.Summer:
					this.rblIsSummerRegularSemester.SetSelectedValueYes();
					this.rblIsSummerRegularSemester.Enabled = false;
					break;
				case SemesterTypes.Spring:
				case SemesterTypes.Fall:
					this.rblIsSummerRegularSemester.SetSelectedValueNo();
					this.rblIsSummerRegularSemester.Enabled = true;
					break;
				default:
					throw new NotImplementedEnumException(semesterTypeEnum);
			}

			if (this.AdmissionOpenProgramID != null)
			{
				this.rblIsSummerRegularSemester_OnSelectedIndexChanged(null, null);
			}
			else
			{
				var programID = this.ddlProgramID.SelectedValue.ToInt();
				var intakeSemesterID = this.ddlSemesterID.SelectedValue.ToShort();
				var lastAdmissionOpenProgram = BL.Core.Admissions.Admin.AdmissionOpenPrograms.GetLastAdmissionOpenProgram(programID, intakeSemesterID, this.AdminIdentity.LoginSessionGuid);
				if (lastAdmissionOpenProgram == null)
				{
					this.rblIsSummerRegularSemester_OnSelectedIndexChanged(null, null);
					this.cbShifts.ClearSelection();
					this.ddlAdmissionCriteria.ClearSelection();
					this.tbEligibilityCriteriaStatement.Text = null;
					this.tbAdmissionProcessingFee.Text = null;
					this.ddlEntryTestType.ClearSelection();
					this.tbEntryTestDuration.Text = null;
					this.ddlEntryTestType_OnSelectedIndexChanged(null, null);
				}
				else
				{
					if (this.rblIsSummerRegularSemester.Enabled)
						this.rblIsSummerRegularSemester.SetBooleanValue(lastAdmissionOpenProgram.IsSummerRegularSemester);
					this.rblIsSummerRegularSemester_OnSelectedIndexChanged(null, null);
					this.cbShifts.Items.Cast<ListItem>().ForEach(i => i.Selected = lastAdmissionOpenProgram.ShiftsEnum.HasFlag(i.Value.ToEnum<Shifts>()));
					this.ddlAdmissionCriteria.SetSelectedValueIfExists(lastAdmissionOpenProgram.AdmissionCriteriaID.ToString());
					this.tbEligibilityCriteriaStatement.Text = lastAdmissionOpenProgram.EligibilityCriteriaStatement;
					this.tbAdmissionProcessingFee.Text = lastAdmissionOpenProgram.AdmissionProcessingFee.ToString();
					this.ddlEntryTestType.SetEnumValue(lastAdmissionOpenProgram.EntryTestTypeEnum);
					this.ddlEntryTestType_OnSelectedIndexChanged(null, null);
					if (this.tbEntryTestDuration.Enabled)
						this.tbEntryTestDuration.Text = lastAdmissionOpenProgram.EntryTestDuration.ToString();
				}
			}
		}

		protected void rblIsSummerRegularSemester_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var isSummerRegularSemester = this.rblIsSummerRegularSemester.SelectedValue.ToBoolean();
			this.ddlFinalSemesterID.Items.Clear();
			this.ddlFinalSemesterID.AddStaticListItem(CommonListItems.Select);
			this.ddlMaxSemesterID.Items.Clear();
			this.ddlMaxSemesterID.AddStaticListItem(CommonListItems.Select);
			var currentSemester = semesterID;
			const int count = 30;
			for (var i = 0; i < count; i++)
			{
				this.ddlFinalSemesterID.Items.Add(new ListItem(currentSemester.ToSemesterString(), currentSemester.ToString()));
				this.ddlMaxSemesterID.Items.Add(new ListItem(currentSemester.ToSemesterString(), currentSemester.ToString()));
				currentSemester = Semester.NextSemester(currentSemester);
			}

			currentSemester = semesterID;
			for (var i = 0; i < count; i++)
			{
				var item = this.ddlFinalSemesterID.Items.FindByValue(currentSemester.ToString());
				if (item != null)
					item.Text = $"{(i + 1):D2} - {currentSemester.ToSemesterString()}";
				item = this.ddlMaxSemesterID.Items.FindByValue(currentSemester.ToString());
				if (item != null)
					item.Text = $"{(i + 1):D2} - {currentSemester.ToSemesterString()}";
				currentSemester = Semester.NextRegularSemester(currentSemester, isSummerRegularSemester);
			}
		}

		protected void ddlEntryTestType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var entryTestTypeEnum = this.ddlEntryTestType.GetSelectedEnumValue<AdmissionOpenProgram.EntryTestTypes>();
			switch (entryTestTypeEnum)
			{
				case AdmissionOpenProgram.EntryTestTypes.None:
					this.dtpEntryTestStartDate.SelectedDate = null;
					this.dtpEntryTestStartDate.Enabled = false;
					this.dtvEntryTestStartDate.AllowNull = true;
					this.dtvEntryTestStartDate.Enabled = false;

					this.dtpEntryTestEndDate.SelectedDate = null;
					this.dtpEntryTestEndDate.Enabled = false;
					this.dtvEntryTestEndDate.AllowNull = true;
					this.dtvEntryTestEndDate.Enabled = false;

					this.tbEntryTestDuration.Text = null;
					this.tbEntryTestDuration.Enabled = false;
					this.bvEntryTestDuration.AllowNull = true;
					this.bvEntryTestDuration.Enabled = false;
					break;
				case AdmissionOpenProgram.EntryTestTypes.ComputerBasedTest:
					this.dtpEntryTestStartDate.SelectedDate = null;
					this.dtpEntryTestStartDate.Enabled = true;
					this.dtvEntryTestStartDate.AllowNull = false;
					this.dtvEntryTestStartDate.Enabled = true;

					this.dtpEntryTestEndDate.SelectedDate = null;
					this.dtpEntryTestEndDate.Enabled = true;
					this.dtvEntryTestEndDate.AllowNull = false;
					this.dtvEntryTestEndDate.Enabled = true;

					this.tbEntryTestDuration.Text = null;
					this.tbEntryTestDuration.Enabled = true;
					this.bvEntryTestDuration.AllowNull = false;
					this.bvEntryTestDuration.Enabled = true;
					break;
				case AdmissionOpenProgram.EntryTestTypes.PaperBasedTest:
					this.dtpEntryTestStartDate.SelectedDate = null;
					this.dtpEntryTestStartDate.Enabled = true;
					this.dtvEntryTestStartDate.AllowNull = false;
					this.dtvEntryTestStartDate.Enabled = true;

					this.dtpEntryTestEndDate.SelectedDate = null;
					this.dtpEntryTestEndDate.Enabled = false;
					this.dtvEntryTestEndDate.AllowNull = true;
					this.dtvEntryTestEndDate.Enabled = false;

					this.tbEntryTestDuration.Text = null;
					this.tbEntryTestDuration.Enabled = true;
					this.bvEntryTestDuration.AllowNull = false;
					this.bvEntryTestDuration.Enabled = true;
					break;
				default:
					throw new NotImplementedEnumException(entryTestTypeEnum);
			}
		}

		protected void btnSaveAdmissionOpen_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageAdmissionOpening, UserGroupPermission.PermissionValues.Allowed);
			var shifts = this.cbShifts.GetSelectedEnumValues<Shifts>().Combine() ?? throw new InvalidOperationException();
			var admissionOpenProgram = new AdmissionOpenProgram
			{
				ProgramID = this.ddlProgramID.SelectedValue.ToInt(),
				SemesterID = this.ddlSemesterID.SelectedValue.ToShort(),
				IsSummerRegularSemester = this.rblIsSummerRegularSemester.SelectedValue.ToBoolean(),
				FinalSemesterID = this.ddlFinalSemesterID.SelectedValue.ToShort(),
				MaxSemesterID = this.ddlMaxSemesterID.SelectedValue.ToShort(),
				ShiftsEnum = shifts,
				AdmissionCriteriaID = this.ddlAdmissionCriteria.SelectedValue.ToInt(),
				EligibilityCriteriaStatement = this.tbEligibilityCriteriaStatement.Text.TrimAndMakeItNullIfEmpty(),
				AdmissionOpenDate = this.dpAdmissionOpenDate.SelectedDate ?? throw new InvalidOperationException(),
				AdmissionClosingDate = this.dpAdmissionCloseDate.SelectedDate ?? throw new InvalidOperationException(),
				AdmissionProcessingFee = this.tbAdmissionProcessingFee.Text.ToShort(),
				AdmissionProcessingFeeDueDate = this.dtpAdmissionProcessingFeeDueDate.SelectedDate ?? throw new InvalidOperationException(),
				EntryTestTypeEnum = this.ddlEntryTestType.GetSelectedEnumValue<AdmissionOpenProgram.EntryTestTypes>(),
				EntryTestDuration = this.tbEntryTestDuration.Text.ToNullableByte(),
				EntryTestStartDate = this.dtpEntryTestStartDate.SelectedDate,
				EntryTestEndDate = this.dtpEntryTestEndDate.SelectedDate,
				InterviewsStartDate = this.dpInterviewStartDate.SelectedDate ?? throw new InvalidOperationException(),
				InterviewsEndDate = this.dpInterviewEndDate.SelectedDate ?? throw new InvalidOperationException(),
				LoginAllowedEndDate = this.dpLoginAllowedDate.SelectedDate ?? throw new InvalidOperationException(),
				AdmissionOpenDateForForeigners = this.dpAdmissionOpenDateForForeigners.SelectedDate,
				AdmissionClosingDateForForeigners = this.dpAdmissionCloseDateForForeigners.SelectedDate,
				IntakeTargetStudentsCount = this.tbIntakeTargetStudentsCount.Text.ToInt()
			};

			if (this.AdmissionOpenProgramID == null)
			{
				var validationResults = BL.Core.Admissions.Admin.AdmissionOpenPrograms.AddAdmissionOpenProgram(admissionOpenProgram, this.AdminIdentity.LoginSessionGuid);
				switch (validationResults)
				{
					case AdmissionOpenProgram.ValidationResults.None:
						throw new InvalidOperationException();
					case AdmissionOpenProgram.ValidationResults.AlreadyExists:
						this.AddErrorAlert("Admission open program is already exists.");
						break;
					case AdmissionOpenProgram.ValidationResults.Success:
						this.AddSuccessMessageHasBeenAdded("Admission open program");
						Redirect<AdmissionsOpen>();
						break;
					case AdmissionOpenProgram.ValidationResults.InvalidDateSequence:
						this.AddErrorAlert("Invalid date sequence.");
						break;
					case AdmissionOpenProgram.ValidationResults.InvalidAdmissionOpenDates:
						this.AddErrorAlert("Invalid admission open and close dates.");
						break;
					case AdmissionOpenProgram.ValidationResults.InvalidInterviewDates:
						this.AddErrorAlert("Invalid interview start and end dates.");
						break;
					case AdmissionOpenProgram.ValidationResults.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<AdmissionsOpen>();
						return;
					case AdmissionOpenProgram.ValidationResults.InvalidAdmissionProcessingDueDate:
						this.AddErrorAlert("Admission Processing Due Date cannot be less than Admission Closing date.");
						break;
					case AdmissionOpenProgram.ValidationResults.InvalidEntryTestDates:
						this.AddErrorAlert("Invalid Entry Test start and end dates.");
						break;
					case AdmissionOpenProgram.ValidationResults.InvalidFinalSemester:
						this.AddErrorAlert("Final Semester is not valid.");
						break;
					case AdmissionOpenProgram.ValidationResults.InvalidMaxSemester:
						this.AddErrorAlert("Max Semester is not valid.");
						break;
					case AdmissionOpenProgram.ValidationResults.InvalidAdmissionOpenDatesForForeigners:
						this.AddErrorAlert("Admission Open Dates for International Students are not valid.");
						break;
					default:
						throw new NotImplementedEnumException(validationResults);
				}
			}
			else
			{
				admissionOpenProgram.AdmissionOpenProgramID = this.AdmissionOpenProgramID.Value;
				var validationResults = BL.Core.Admissions.Admin.AdmissionOpenPrograms.UpdateAdmissionOpenProgram(admissionOpenProgram, this.AdminIdentity.LoginSessionGuid);
				switch (validationResults)
				{
					case AdmissionOpenProgram.ValidationResults.None:
						throw new InvalidOperationException();
					case AdmissionOpenProgram.ValidationResults.AlreadyExists:
						this.AddErrorAlert("Admission open program is already exists.");
						break;
					case AdmissionOpenProgram.ValidationResults.Success:
						this.AddSuccessMessageHasBeenUpdated("Admission open program");
						Redirect<AdmissionsOpen>();
						break;
					case AdmissionOpenProgram.ValidationResults.InvalidDateSequence:
						this.AddErrorAlert("Invalid date sequence.");
						break;
					case AdmissionOpenProgram.ValidationResults.InvalidAdmissionOpenDates:
						this.AddErrorAlert("Invalid admission open and close dates.");
						break;
					case AdmissionOpenProgram.ValidationResults.InvalidInterviewDates:
						this.AddErrorAlert("Invalid interview start and end dates.");
						break;
					case AdmissionOpenProgram.ValidationResults.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<AdmissionsOpen>();
						break;
					case AdmissionOpenProgram.ValidationResults.InvalidAdmissionProcessingDueDate:
						this.AddErrorAlert("Admission Processing Due Date cannot be less than Admission Closing date.");
						break;
					case AdmissionOpenProgram.ValidationResults.InvalidEntryTestDates:
						this.AddErrorAlert("Invalid Entry Test start and end dates.");
						break;
					case AdmissionOpenProgram.ValidationResults.InvalidFinalSemester:
						this.AddErrorAlert("Final Semester is not valid.");
						break;
					case AdmissionOpenProgram.ValidationResults.InvalidMaxSemester:
						this.AddErrorAlert("Max Semester is not valid.");
						break;
					case AdmissionOpenProgram.ValidationResults.InvalidAdmissionOpenDatesForForeigners:
						this.AddErrorAlert("Admission Open Dates for International Students are not valid.");
						break;
					default:
						throw new NotImplementedEnumException(validationResults);
				}
			}
		}
	}
}