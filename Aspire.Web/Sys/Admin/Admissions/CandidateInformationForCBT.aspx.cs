﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Sys.Admin.Admissions
{
	[AspirePage("878E77CB-70D9-4A03-ADD3-9A4970C8C10B", UserTypes.Staff, "~/Sys/Admin/Admissions/CandidateInformationForCBT.aspx", "Candidate Information For CBT", true, AspireModules.Admissions)]
	public partial class CandidateInformationForCBT : AdminsPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.print);
		public override string PageTitle => "Candidate Information For CBT";
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.Admissions.Module, new []{UserGroupPermission.PermissionValues.Allowed, } },
		};

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlInstituteIDFilter.FillInstitutesForAdmin();
				this.ddlSemesterIDFilter.FillSemesters();
				this.ddlPaid.FillYesNo(CommonListItems.All);
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
				this.tbApplicationNos.Text = null;
			}
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlInstituteIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlInstituteIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToInt();
			this.ddlDepartmentIDFilter.FillDepartments(instituteID, CommonListItems.All);
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToInt();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var admissionOpenPrograms = BL.Core.Common.Lists.GetAdmissionOpenPrograms(semesterID, instituteID, departmentID, true);
			this.ddlAdmissionOpenProgramIDFilter.DataBind(admissionOpenPrograms, CommonListItems.All);
		}

		protected void btnExport_OnClick(object sender, EventArgs e)
		{
			string csv = string.Empty;
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToInt();
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var programID = this.ddlAdmissionOpenProgramIDFilter.SelectedValue.ToNullableInt();
			var paid = this.ddlPaid.SelectedValue.ToNullableBoolean();
			var forCBT = this.cbForCBT.Checked;
			var applicationNonsString = this.tbApplicationNos.Text.Trim();
			List<int> applicationNonsList = new List<int>();
			int? applicationNo = null;
			this.tbApplicationNos.Text = null;
			if (!string.IsNullOrEmpty(applicationNonsString))
			{
				try
				{
					if (applicationNonsString.Contains(","))
					{

						applicationNonsList = applicationNonsString.Split(',').Select(s => s.ToInt()).ToList();
						applicationNo = null;
					}
					else
					{
						applicationNo = applicationNonsString.ToNullableInt();
					}
				}
				catch
				{
					this.AddErrorAlert("Enter application no with comma separated.");
					Redirect<CandidateInformationForCBT>();
					return;
				}
			}
			var data = BL.Core.Admissions.Admin.CandidateInformationForCBT.GetCandidateInformation(semesterID, instituteID, departmentID, programID, paid, forCBT, applicationNonsList, applicationNo, this.AdminIdentity.LoginSessionGuid);
			if (!data.Any())
			{
				this.AddNoRecordFoundAlert();
				return;
			}


			if (forCBT)
			{
				csv = data.ConvertToCSVFormat(new List<CSVHelper.CSVColumnMapping>
			{
				new CSVHelper.CSVColumnMapping {ColumnIndex = 1, ColumnName = "ApplicationNo", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.FinalApplicationNo)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 2, ColumnName = "Name", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.FinalName)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 3, ColumnName = "Email", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.Email)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 4, ColumnName = "ProgramAlias", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.ProgramAlias)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 5, ColumnName = "Password", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.Password)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 6, ColumnName = "TestAttempt", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.TestAttempt)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 7, ColumnName = "Year", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.Year)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 8, ColumnName = "SemesterType", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.SemesterType)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 9, ColumnName = "InstituteAlias", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.InstituteAlias)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 10, ColumnName = "ApplyDate", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.ApplyDate)},
			});
			}

			else
			{
				csv = data.ConvertToCSVFormat(new List<CSVHelper.CSVColumnMapping>
			{
				new CSVHelper.CSVColumnMapping {ColumnIndex = 0, ColumnName = "SemesterID", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.SemesterID)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 1, ColumnName = "Semester", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.Semester)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 2, ColumnName = "InstituteID", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.InstituteID)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 3, ColumnName = "InstituteAlias", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.InstituteAlias)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 4, ColumnName = "ApplicationNo", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.FinalApplicationNo)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 5, ColumnName = "Name", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.FinalName)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 6, ColumnName = "ProgramAlias", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.ProgramAlias)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 7, ColumnName = "Email", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.Email)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 8, ColumnName = "Phone", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.Phone)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 9, ColumnName = "Mobile", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.Mobile)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 10, ColumnName = "Password", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.Password)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 11, ColumnName = "DepositDate", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.DepositDate)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 12, ColumnName = "ApplyDate", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.ApplyDate)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 13, ColumnName = "CBT Institute", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.CBTLabInstituteAlias)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 14, ColumnName = "CBT Lab", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.LabName)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 15, ColumnName = "CBT Start Time", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.CBTLabStartTime)},
				new CSVHelper.CSVColumnMapping {ColumnIndex = 16, ColumnName = "Total Time (Minutes)", PropertyName=nameof(BL.Core.Admissions.Admin.CandidateInformationForCBT.CandidateDetail.CBTLabDurationInMinutes)},
			});
			}

			this.Response.Clear();
			this.Response.Buffer = true;
			var instituteAlias = data.First().InstituteAlias;
			var fileName = $"Candidate Info {instituteAlias}-{semesterID}-{DateTime.Now:yyyyMMddhhmmss}.csv";
			this.Response.AddHeader("content-disposition", $"attachment;filename=\"{fileName}\"");
			this.Response.Charset = "";
			this.Response.ContentType = "text/csv";
			this.Response.Output.Write(csv);
			this.Response.Flush();
			this.Response.End();
		}
	}
}