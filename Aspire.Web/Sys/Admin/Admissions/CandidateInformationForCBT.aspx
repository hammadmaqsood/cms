﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="CandidateInformationForCBT.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Admissions.CandidateInformationForCBT" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" AutoPostBack="True" ValidationGroup="Export" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlInstituteIDFilter" OnSelectedIndexChanged="ddlInstituteIDFilter_OnSelectedIndexChanged" AutoPostBack="True" ValidationGroup="Export" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" ValidationGroup="Export" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlAdmissionOpenProgramIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlAdmissionOpenProgramIDFilter" AutoPostBack="True" ValidationGroup="Export" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Admission Processing Fee Status:" AssociatedControlID="ddlPaid" />
			<aspire:AspireDropDownList runat="server" ID="ddlPaid" AutoPostBack="True" ValidationGroup="Export" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="For CBT:" AssociatedControlID="cbForCBT" />
			<aspire:AspireCheckBox runat="server" ID="cbForCBT" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="ApplicationNos:" AssociatedControlID="tbApplicationNos" />
			<aspire:AspireTextBox runat="server" ID="tbApplicationNos" PlaceHolder="Enter application no(s) with comma seperated." />
		</div>
		<div class="form-group">
			<aspire:AspireButton ID="btnExport" ButtonType="Primary" runat="server" Text="Export To CSV" Glyphicon="export" OnClick="btnExport_OnClick" ValidationGroup="Export" />
		</div>
	</div>
</asp:Content>
