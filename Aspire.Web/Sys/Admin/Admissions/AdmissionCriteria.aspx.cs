﻿using Aspire.BL.Core.Admissions.Admin;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Admissions
{
	[AspirePage("587A331C-C8F9-4A6A-B898-90286CDAE02A", UserTypes.Admins, "~/Sys/Admin/Admissions/AdmissionCriteria.aspx", "Admission Criteria", true, AspireModules.Admissions)]
	public partial class AdmissionCriteria : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.Admissions.Module, new []{UserGroupPermission.PermissionValues.Allowed, } },
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Admission Criteria";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("CriteriaName", SortDirection.Ascending);
				this.ddlInstitutes.FillInstitutesForAdmin(CommonListItems.All);
				this.GetData(0);
			}
		}

		protected void ddlInstitutes_OnSelectedIndexChanged(object o, EventArgs e)
		{
			this.GetData(0);
		}

		protected void gvAdmissionCriteria_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvAdmissionCriteria_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(this.gvAdmissionCriteria.PageIndex);
		}

		protected void gvAdmissionCriteria_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvAdmissionCriteria.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvAdmissionCriteria_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					this.AdminIdentity.Demand(AdminPermissions.Admissions.ManageAdmissionCriteria, UserGroupPermission.PermissionValues.Allowed);
					var admissionCriteriaID = e.DecryptedCommandArgumentToInt();
					var result = Aspire.BL.Core.Admissions.Admin.AdmissionCriterias.DeleteAdmissionCriteria(admissionCriteriaID, this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case AdmissionCriterias.DeleteAdmissionCriteriaStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case AdmissionCriterias.DeleteAdmissionCriteriaStatuses.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Admission Criteria");
							break;
						case AdmissionCriterias.DeleteAdmissionCriteriaStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Admission criteria");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					Redirect<AdmissionCriteria>();
					break;
			}
		}

		private void GetData(int pageIndex)
		{
			int virtualItemCount;
			var admissionCriteria = Aspire.BL.Core.Admissions.Admin.AdmissionCriterias.GetAdmissionCriterias(this.ddlInstitutes.SelectedValue.ToNullableInt(), pageIndex, this.gvAdmissionCriteria.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out virtualItemCount, this.AdminIdentity.LoginSessionGuid);
			this.gvAdmissionCriteria.DataBind(admissionCriteria, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}
	}
}