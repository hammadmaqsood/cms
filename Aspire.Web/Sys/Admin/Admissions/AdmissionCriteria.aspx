﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="AdmissionCriteria.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Admissions.AdmissionCriteria" %>

<%@ Import Namespace="Aspire.Web.Sys.Admin.Admissions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireHyperLinkButton Glyphicon="plus" Text="Add Criteria" runat="server" NavigateUrl="AdmissionCriteriaDetails.aspx" CausesValidation="false" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstitutes" />
					<aspire:AspireDropDownList runat="server" ID="ddlInstitutes" CausesValidation="False" OnSelectedIndexChanged="ddlInstitutes_OnSelectedIndexChanged" AutoPostBack="True" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView ID="gvAdmissionCriteria" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" OnPageIndexChanging="gvAdmissionCriteria_PageIndexChanging" OnPageSizeChanging="gvAdmissionCriteria_PageSizeChanging" OnRowCommand="gvAdmissionCriteria_RowCommand" OnSorting="gvAdmissionCriteria_Sorting">
		<Columns>
			<asp:BoundField DataField="InstituteAlias" SortExpression="InstituteAlias" HeaderText="Institute" />
			<asp:BoundField DataField="CriteriaName" SortExpression="CriteriaName" HeaderText="Criteria Name" />
			<asp:TemplateField ItemStyle-Wrap="False">
				<ItemTemplate>
					<aspire:AspireHyperLinkButton Glyphicon="edit" runat="server" ButtonType="OnlyIcon" NavigateUrl='<%# AdmissionCriteriaDetails.GetPageUrl((int)Eval("AdmissionCriteriaID")) %>' />
					<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Delete" Glyphicon="remove" EncryptedCommandArgument='<%# Eval("AdmissionCriteriaID") %>' ConfirmMessage='<%# Eval("CriteriaName", "Are you sure you want to delete admission criteria \"{0}\"?") %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
