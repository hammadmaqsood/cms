﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="AdmissionCriteriaDetails.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Admissions.AdmissionCriteriaDetails" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-horizontal">
		<div class="form-group">
			<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Criteria Name:" AssociatedControlID="tbCriteriaName" />
			<div class="col-md-6">
				<aspire:AspireTextBox ID="tbCriteriaName" runat="server" ValidationGroup="Name" MaxLength="50" />
			</div>
			<div class="col-md-4 form-control-static">
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ControlToValidate="tbCriteriaName" ValidationGroup="Name" />
			</div>
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" CssClass="col-md-2" Text="Institute:" AssociatedControlID="ddlInstituteID" />
			<div class="col-md-6">
				<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" />
			</div>
			<div class="col-md-4 form-control-static">
				<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ControlToValidate="ddlInstituteID" ValidationGroup="Name" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-offset-2 col-md-6">
				<aspire:AspireButton ID="btnSaveCriteriaName" runat="server" ButtonType="Primary" Text="Save" ValidationGroup="Name" OnClick="btnSaveCriteriaName_OnClick" />
				<aspire:AspireHyperLink runat="server" NavigateUrl="AdmissionCriteria.aspx" Text="Cancel" />
			</div>
		</div>
	</div>
		<p></p>
	<asp:Panel runat="server" ID="panelDetails">
		<p>
			<aspire:AspireButton runat="server" Glyphicon="plus" Text="Add Pre-Requisite" CausesValidation="false" ID="btnAdddPreReq" OnClick="btnAdddPreReq_OnClick" />
		</p>
		<aspire:AspireGridView ID="gvAdmissionCriteriaPreReqs" runat="server" DataKeyNames="AdmissionCriteriaPreReqID" AutoGenerateColumns="false" AllowSorting="true" OnRowCommand="gvAdmissionCriteriaPreReqs_RowCommand" OnSelectedIndexChanged="gvAdmissionCriteriaPreReqs_SelectedIndexChanged" OnSorting="gvAdmissionCriteriaPreReqs_Sorting">
			<Columns>
				<asp:BoundField HeaderText="Pre-Req Qualifiaction" DataField="PreReqQualification" SortExpression="PreReqQualification" />
				<asp:BoundField HeaderText="Result Status" DataField="PreReqQualificationResultStatus" SortExpression="PreReqQualificationResultStatus" />
				<asp:BoundField HeaderText="Result Awaited Degree Type" DataField="ResultAwaitedDegreeTypeFullName" SortExpression="ResultAwaitedDegreeType" />
				<asp:TemplateField ItemStyle-Wrap="False">
					<ItemTemplate>
						<aspire:AspireLinkButton CausesValidation="False" runat="server" EncryptedCommandArgument='<%#Eval("AdmissionCriteriaPreReqID") %>' Glyphicon="edit" CommandName="Edit" />
						<aspire:AspireLinkButton CausesValidation="False" runat="server" EncryptedCommandArgument='<%#Eval("AdmissionCriteriaPreReqID") %>' Glyphicon="remove" CommandName="Delete" ConfirmMessage='<%# Eval("PreReqQualification","Are you sure you want to delete pre-requisite \"{0}\"?")%>' />
						<aspire:LinkButton CausesValidation="false" runat="server" Text="Select" CommandName="Select" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
			<SelectedRowStyle Font-Bold="True" />
		</aspire:AspireGridView>
		<p></p>
		<p>
			<aspire:AspireButton ID="btnAddPreReqDegree" runat="server" Glyphicon="plus" Text="Add Pre-Requisite Degree" CausesValidation="false" OnClick="btnAddPreReqDegree_OnClick" />
		</p>
		<aspire:AspireGridView ID="gvAdmissionCriteriaPreReqDegrees" runat="server" DataKeyNames="AdmissionCriteriaPreReqDegreeID" AutoGenerateColumns="false" AllowSorting="true" OnRowCommand="gvAdmissionCriteriaPreReqDegrees_OnRowCommand" OnSelectedIndexChanged="gvAdmissionCriteriaPreReqDegrees_OnSelectedIndexChanged" OnSorting="gvAdmissionCriteriaPreReqDegrees_OnSorting">
			<Columns>
				<asp:BoundField HeaderText="Degree Type" DataField="DegreeTypeEnumFullName" SortExpression="DegreeType" />
				<asp:TemplateField HeaderText="Annual Exam System" SortExpression="AnnualExamSystem">
					<ItemTemplate>
						<aspire:Label runat="server" Text='<%# ((bool)Eval("AnnualExamSystem")).ToYesNo() %>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Semester Exam System" SortExpression="SemesterSystem">
					<ItemTemplate>
						<aspire:Label runat="server" Text='<%# ((bool)Eval("SemesterSystem")).ToYesNo() %>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="Annual Exam System Minimum Percentage" DataFormatString="{0}%" DataField="AnnualExamSystemMinimumPercentage" SortExpression="AnnualExamSystemMinimumPercentage" />
				<asp:BoundField HeaderText="Semester System Min CGPA/4.0" DataField="SemesterSystemMinCGPAOutOf4" SortExpression="SemesterSystemMinCGPAOutOf4" />
				<asp:BoundField HeaderText="Semester System Min CGPA/5.0" DataField="SemesterSystemMinCGPAOutOf5" SortExpression="SemesterSystemMinCGPAOutOf5" />
				<asp:BoundField HeaderText="Display Sequence" DataField="DisplaySequence" SortExpression="DisplaySequence" />
				<asp:BoundField HeaderText="Merit List Weightage" DataFormatString="{0}%" DataField="MeritListWeightage" SortExpression="MeritListWeightage" />
				<asp:TemplateField ItemStyle-Wrap="False">
					<ItemTemplate>
						<aspire:AspireLinkButton CausesValidation="False" runat="server" EncryptedCommandArgument='<%#Eval("AdmissionCriteriaPreReqDegreeID") %>' Glyphicon="edit" CommandName="Edit" />
						<aspire:AspireLinkButton CausesValidation="False" runat="server" EncryptedCommandArgument='<%#Eval("AdmissionCriteriaPreReqDegreeID") %>' Glyphicon="remove" CommandName="Delete" ConfirmMessage='<%# Eval("DegreeTypeEnumFullName","Are you sure you want to delete degree \"{0}\"?")%>' />
						<aspire:LinkButton CausesValidation="false" runat="server" Text="Select" CommandName="Select" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
			<SelectedRowStyle Font-Bold="True" />
		</aspire:AspireGridView>
		<p></p>
		<p>
			<aspire:AspireButton runat="server" Glyphicon="plus" Text="Add Subjects" ID="btnAddSubjects" CausesValidation="false" OnClick="btnAddSubjects_OnClick" />
		</p>
		<aspire:AspireGridView ID="gvAdmissionCriteriaPreReqDegreeSubjects" runat="server" AutoGenerateColumns="false" AllowSorting="true" OnRowCommand="gvAdmissionCriteriaPreReqDegreeSubjects_OnRowCommand" OnSorting="gvAdmissionCriteriaPreReqDegreeSubjects_OnSorting">
			<Columns>
				<asp:BoundField HeaderText="Subjects" DataField="Subjects" SortExpression="Subjects" />
				<asp:TemplateField ItemStyle-Wrap="False">
					<ItemTemplate>
						<aspire:AspireLinkButton CausesValidation="False" runat="server" EncryptedCommandArgument='<%#Eval("AdmissionCriteriaPreReqDegreeSubjectID") %>' Glyphicon="edit" CommandName="Edit" />
						<aspire:AspireLinkButton CausesValidation="False" runat="server" EncryptedCommandArgument='<%#Eval("AdmissionCriteriaPreReqDegreeSubjectID") %>' Glyphicon="remove" CommandName="Delete" ConfirmMessage='<%# Eval("Subjects","Are you sure you want to delete subjects \"{0}\"?")%>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
	</asp:Panel>
	<aspire:AspireModal runat="server" ID="modalPreReq">
		<BodyTemplate>
			<asp:Panel runat="server" DefaultButton="btnSavePreReq" class="form-horizontal">
				<div class="form-group">
					<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Pre Req Qualification:" AssociatedControlID="tbPreReqQualification" />
					<div class="col-md-8">
						<aspire:AspireTextBox runat="server" ID="tbPreReqQualification" ValidationGroup="PreReq" MaxLength="100" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbPreReqQualification" ValidationGroup="PreReq" ErrorMessage="This field is required." />
					</div>
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Result Status:" AssociatedControlID="tbResultStatus" />
					<div class="col-md-8">
						<aspire:AspireTextBox runat="server" ID="tbResultStatus" ValidationGroup="PreReq" MaxLength="100" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbResultStatus" ValidationGroup="PreReq" ErrorMessage="This field is required." />
					</div>
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Result Awaited Degree Type:" AssociatedControlID="ddlResultAwaitedDegreeType" />
					<div class="col-md-8">
						<aspire:AspireDropDownList runat="server" ID="ddlResultAwaitedDegreeType" />
					</div>
				</div>
			</asp:Panel>
		</BodyTemplate>
		<FooterTemplate>
			<aspire:AspireButton runat="server" ID="btnSavePreReq" ButtonType="Primary" ValidationGroup="PreReq" Text="Save" OnClick="btnSavePreReq_OnClick" />
			<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
		</FooterTemplate>
	</aspire:AspireModal>
	<aspire:AspireModal runat="server" ID="modalPreReqDegree">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<asp:Panel runat="server" DefaultButton="btnSavePreRegDegree" class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Pre Req Qualification:" AssociatedControlID="lblPreReqValueQualificationforDeg" />
							<div class="form-control-static col-md-8">
								<aspire:AspireLabel runat="server" ID="lblPreReqValueQualificationforDeg" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Result Status:" AssociatedControlID="lblResultStatusValueforDeg" />
							<div class="form-control-static col-md-8">
								<aspire:AspireLabel runat="server" ID="lblResultStatusValueforDeg" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Result Awaited Degree Type:" AssociatedControlID="lblResultAwaitedDegreeTypeValueforDeg" />
							<div class="form-control-static col-md-8">
								<aspire:AspireLabel runat="server" ID="lblResultAwaitedDegreeTypeValueforDeg" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Degree:" AssociatedControlID="ddlDegree" />
							<div class="col-md-8">
								<aspire:AspireDropDownList runat="server" ID="ddlDegree" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Annual Exam System:" AssociatedControlID="rblAnnualExamSystem" />
							<div class="form-control-static col-md-8">
								<aspire:AspireRadioButtonList runat="server" ID="rblAnnualExamSystem" AutoPostBack="True" RepeatDirection="Horizontal" RepeatLayout="Flow" OnSelectedIndexChanged="rblAnnualExamSystem_OnSelectedIndexChanged" />
							</div>
						</div>
						<div class="form-group" id="dvMinimumPercentage" runat="server">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Minimum Percentage:" AssociatedControlID="tbMinimumPercentage" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" ID="tbMinimumPercentage" ValidationGroup="RequiredPreRegDegree" />
								<aspire:AspireDoubleValidator runat="server" ID="rdvMinimumPercentage" ControlToValidate="tbMinimumPercentage" ValidationGroup="RequiredPreRegDegree" MinValue="0" MaxValue="100" AllowNull="False" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Percentage." RequiredErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Semester Exam System:" AssociatedControlID="rblSemesterExamSystem" />
							<div class="form-control-static col-md-8">
								<aspire:AspireRadioButtonList runat="server" ID="rblSemesterExamSystem" AutoPostBack="True" RepeatDirection="Horizontal" RepeatLayout="Flow" OnSelectedIndexChanged="rblSemesterExamSystem_OnSelectedIndexChanged" />
							</div>
						</div>
						<div class="form-group" id="dvMinimumCGPA4" runat="server">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Minimum CGPA out of 4.0:" AssociatedControlID="tbMinimumCGPA4" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" ID="tbMinimumCGPA4" ValidationGroup="RequiredPreRegDegree" />
								<aspire:AspireDoubleValidator runat="server" ID="rdvMinimumCGPA4" ControlToValidate="tbMinimumCGPA4" ValidationGroup="RequiredPreRegDegree" MinValue="0" MaxValue="4" AllowNull="False" InvalidRangeErrorMessage="Invalid CGPA." InvalidNumberErrorMessage="Invalid Number." RequiredErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group" id="dvMinimumCGPA5" runat="server">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Minimum CGPA out of 5.0:" AssociatedControlID="tbMinimumCGPA5" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" ID="tbMinimumCGPA5" ValidationGroup="RequiredPreRegDegree" />
								<aspire:AspireDoubleValidator runat="server" ID="rdvMinimumCGPA5" ControlToValidate="tbMinimumCGPA5" ValidationGroup="RequiredPreRegDegree" MinValue="0" MaxValue="5" AllowNull="False" InvalidRangeErrorMessage="Invalid CGPA." InvalidNumberErrorMessage="Invalid Number." RequiredErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Display Sequence:" AssociatedControlID="ddlDisplaySequence" />
							<div class="col-md-8">
								<aspire:AspireDropDownList runat="server" ID="ddlDisplaySequence" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Merit List Weightage (%):" AssociatedControlID="tbMeritListWeightage" />
							<div class="col-md-8 ">
								<aspire:AspireTextBox runat="server" ID="tbMeritListWeightage" ValidationGroup="RequiredPreRegDegree" />
								<aspire:AspireByteValidator runat="server" ControlToValidate="tbMeritListWeightage" ValidationGroup="RequiredPreRegDegree" MinValue="0" MaxValue="100" AllowNull="False" InvalidNumberErrorMessage="Invalid Number." InvalidRangeErrorMessage="Invalid Percentage." RequiredErrorMessage="This field is required." />
							</div>
						</div>
					</asp:Panel>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<aspire:AspireButton runat="server" ID="btnSavePreRegDegree" ButtonType="Primary" ValidationGroup="RequiredPreRegDegree" Text="Save" OnClick="btnSavePreRegDegree_OnClick" />
			<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
		</FooterTemplate>
	</aspire:AspireModal>
	<aspire:AspireModal runat="server" ID="modalPreReqDegreeSubject">
		<BodyTemplate>
			<asp:Panel runat="server" DefaultButton="btnSavePreReqDegreeSubjt" class="form-horizontal">
				<div class="form-group">
					<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Pre Req Qualification:" AssociatedControlID="lblPreReqQualificationValueforSubjt" />
					<div class="form-control-static col-md-8">
						<aspire:AspireLabel runat="server" ID="lblPreReqQualificationValueforSubjt" />
					</div>
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Result Status:" AssociatedControlID="lblResultStatusValueforSubjt" />
					<div class="form-control-static col-md-8">
						<aspire:AspireLabel runat="server" ID="lblResultStatusValueforSubjt" />
					</div>
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Result Awaited Degree Type:" AssociatedControlID="lblResultAwaitedDegreeTypeValueforSubjt" />
					<div class="form-control-static col-md-8">
						<aspire:AspireLabel runat="server" ID="lblResultAwaitedDegreeTypeValueforSubjt" />
					</div>
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Degree:" AssociatedControlID="lblDegreeValueforSubjt" />
					<div class="form-control-static col-md-8">
						<aspire:AspireLabel runat="server" ID="lblDegreeValueforSubjt" />
					</div>
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Subjects:" AssociatedControlID="tbSubjects" />
					<div class="col-md-8">
						<aspire:AspireTextBox runat="server" ID="tbSubjects" ValidationGroup="RequiredPreRegDegreeSubjt" MaxLength="100" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbSubjects" ValidationGroup="RequiredPreRegDegreeSubjt" ErrorMessage="This field is required." />
					</div>
				</div>
			</asp:Panel>
		</BodyTemplate>
		<FooterTemplate>
			<aspire:AspireButton runat="server" ID="btnSavePreReqDegreeSubjt" ButtonType="Primary" ValidationGroup="RequiredPreRegDegreeSubjt" Text="Save" OnClick="btnSavePreReqDegreeSubjt_OnClick" />
			<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
