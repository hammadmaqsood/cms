﻿using Aspire.BL.Core.CourseRegistration.Admin;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.CourseRegistration
{
	[AspirePage("4B0553C2-7E0A-4FEF-9CF1-1BD46B49EE86", UserTypes.Admins, "~/Sys/Admin/CourseRegistration/ControlPanel.aspx", "Control Panel", true, AspireModules.CourseRegistration)]
	public partial class ControlPanel : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.CourseRegistration.Module, new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.cog);
		public override string PageTitle => "Control Panel: Course Registration";

		public static void Redirect(short? semesterID, int? instituteID, int? departmentID, int? programID, CourseRegistrationSetting.Types? typeEnum, CourseRegistrationSetting.Statuses? statusEnum, DateTime? from, DateTime? to, SortDirection sortDirection, string sortExpression, int? pageSize)
		{
			Redirect<ControlPanel>("SemesterID", semesterID, "InstituteID", instituteID, "DepartmentID", departmentID, "ProgramID", programID, "Type", typeEnum, "Status", statusEnum, "From", from, "To", to, "SortDirection", sortDirection, "SortExpression", sortExpression, "PageSize", pageSize);
		}

		private void RefreshPage()
		{
			ControlPanel.Redirect(this.ddlSemesterID.SelectedValue.ToNullableShort(), this.ddlInstituteID.SelectedValue.ToNullableInt(), this.ddlDepartmentID.SelectedValue.ToNullableInt(), this.ddlProgramID.SelectedValue.ToNullableInt(), this.ddlType.GetSelectedEnumValue<CourseRegistrationSetting.Types>(null), this.ddlStatus.GetSelectedEnumValue<CourseRegistrationSetting.Statuses>(null), this.dtpFrom.SelectedDate, this.dtpTo.SelectedDate, this.ViewState.GetSortDirection(), this.ViewState.GetSortExpression(), this.gvControlPanel.PageSize);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Semester", SortDirection.Ascending);
				this.ddlSemesterID.FillSemesters(CommonListItems.All).SetSelectedValueIfNotPostback("SemesterID");
				this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.All).SetSelectedValueIfNotPostback("InstituteID");
				this.ddlType.FillEnums<CourseRegistrationSetting.Types>(CommonListItems.All).SetSelectedValueIfNotPostback("Type");
				this.ddlStatus.FillEnums<CourseRegistrationSetting.Statuses>(CommonListItems.All).SetSelectedValueIfNotPostback("Status");

				this.dtpFrom.SelectedDate = this.Request.GetParameterValue<DateTime>("From");
				this.dtpTo.SelectedDate = this.Request.GetParameterValue<DateTime>("To");
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		#region Filters for Control Panel Record

		protected void btnAdd_OnClick(object sender, EventArgs e)
		{
			this.DisplayControlPanelModal(null);
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlInstituteID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlInstituteID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			if (instituteID == null)
				this.ddlDepartmentID.DataBind(CommonListItems.All).SetSelectedValueIfNotPostback("DepartmentID");
			else
				this.ddlDepartmentID.FillDepartments(instituteID.Value, CommonListItems.All).SetSelectedValueIfNotPostback("DepartmentID");
			this.ddlDepartmentID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			if (instituteID == null)
				this.ddlProgramID.DataBind(CommonListItems.All).SetSelectedValueIfNotPostback("ProgramID");
			else
				this.ddlProgramID.FillPrograms(instituteID.Value, departmentID, true, CommonListItems.All).SetSelectedValueIfNotPostback("ProgramID");
			this.ddlProgramID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlStatus_OnSelectedIndexChanged(null, null);
		}

		protected void ddlStatus_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearch_OnClick(null, null);
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		#endregion

		#region Control Panel

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var typeEnum = this.rblType.GetSelectedEnumValue<CourseRegistrationSetting.Types>(null);
			var fromDate = this.dpFromDate.SelectedDate;
			var toDate = this.dpToDate.SelectedDate;
			if (fromDate == null || toDate == null)
				throw new InvalidOperationException();
			var statusEnum = this.rblStatus.GetSelectedEnumValue<CourseRegistrationSetting.Statuses>();
			var semesterID = this.ddlSemesterForSave.SelectedValue.ToShort();
			var instituteID = this.ddlInstituteForSave.SelectedValue.ToInt();
			var programIDs = this.cblPrograms.GetSelectedValues().Where(s => !string.IsNullOrWhiteSpace(s)).Select(s => s.ToInt()).ToList();
			var result = CourseRegistrationSettings.AddOrUpdateCourseRegistrationSettings(semesterID, instituteID, programIDs, typeEnum, fromDate.Value, toDate.Value, statusEnum, this.AdminIdentity.LoginSessionGuid);
			switch (result.Status)
			{
				case CourseRegistrationSettings.AddOrUpdateCourseRegistrationSettingsResult.Statuses.None:
					throw new InvalidOperationException();
				case CourseRegistrationSettings.AddOrUpdateCourseRegistrationSettingsResult.Statuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<ControlPanel>();
					return;
				case CourseRegistrationSettings.AddOrUpdateCourseRegistrationSettingsResult.Statuses.InvalidDateRange:
					this.alertModalControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Invalid From and To date range.");
					this.updateModalControlPanel.Update();
					return;
				case CourseRegistrationSettings.AddOrUpdateCourseRegistrationSettingsResult.Statuses.Success:
					this.AddSuccessAlert(result.SuccessAdd + " out of " + result.TotalRecords + " record(s) Added", true);
					this.AddSuccessAlert(result.SuccessUpdate + " out of " + result.TotalRecords + " record(s) Updated", true);
					this.RefreshPage();
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void gvControlPanel_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvControlPanel_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvControlPanel_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvControlPanel.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvControlPanel_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Status":
					var courseRegistrationSetting = CourseRegistrationSettings.ToggleCourseRegistrationSettingStatus(e.DecryptedCommandArgumentToInt(), this.AdminIdentity.LoginSessionGuid);
					if (courseRegistrationSetting == null)
					{
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						return;
					}
					this.GetData(this.gvControlPanel.PageIndex);
					break;
				case "Edit":
					e.Handled = true;
					this.DisplayControlPanelModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var resultDelete = CourseRegistrationSettings.DeleteCourseRegistrationSetting(e.DecryptedCommandArgumentToInt(), this.AdminIdentity.LoginSessionGuid);
					switch (resultDelete)
					{
						case CourseRegistrationSettings.DeleteCourseRegistrationSettingStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case CourseRegistrationSettings.DeleteCourseRegistrationSettingStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Registration open dates");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.RefreshPage();
					break;
			}
		}

		protected void gvControlPanel_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				var courseRegistrationSetting = (CourseRegistrationSettings.CourseRegistrationSetting)e.Row.DataItem;
				var fromDate = courseRegistrationSetting.FromDate.Date;
				var toDate = courseRegistrationSetting.ToDate.Date;
				if (DateTime.Today > toDate)
					e.Row.CssClass = "danger";
				else if (DateTime.Today >= fromDate)
					e.Row.CssClass = courseRegistrationSetting.Status == (byte)CourseRegistrationSetting.Statuses.Inactive
						? "warning"
						: "success";
			}
		}

		protected void ddlInstituteForSave_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteForSave.SelectedValue.ToInt();
			this.ddlDepartmentIDForSave.FillDepartments(instituteID, CommonListItems.All);
			this.ddlDepartmentIDForSave_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDForSave_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteForSave.SelectedValue.ToInt();
			var departmentID = this.ddlDepartmentIDForSave.SelectedValue.ToNullableInt();
			if (departmentID == null)
				this.cblPrograms.FillProgramsWithDepartment(instituteID, null, true, CommonListItems.All);
			else
				this.cblPrograms.FillPrograms(instituteID, departmentID.Value, true, CommonListItems.All);
		}

		#endregion

		#region Page Specific Functions

		private void GetData(int pageIndex)
		{
			var pageSize = this.Request.GetParameterValue<int>("PageSize");
			if (pageSize != null)
				this.gvControlPanel.PageSize = pageSize.Value;
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var typeEnum = this.ddlType.GetSelectedEnumValue<CourseRegistrationSetting.Types>(null);
			var statusEnum = this.ddlStatus.GetSelectedEnumValue<CourseRegistrationSetting.Statuses>(null);
			var fromDate = this.dtpFrom.SelectedDate;
			var toDate = this.dtpTo.SelectedDate;

			var courseRegistrationSetting = CourseRegistrationSettings.GetCourseRegistrationSettings(semesterID, instituteID, departmentID, programID, typeEnum, statusEnum, fromDate, toDate, pageIndex, this.gvControlPanel.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out var virtualItemCount, this.AdminIdentity.LoginSessionGuid);
			this.gvControlPanel.DataBind(courseRegistrationSetting, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		private void DisplayControlPanelModal(int? courseRegistrationSettingID)
		{
			this.ddlSemesterForSave.FillSemesters();
			this.ddlInstituteForSave.FillInstitutesForAdmin();

			this.modalControlPanel.HeaderText = "Add/Edit";
			this.rblType.FillEnums<CourseRegistrationSetting.Types>(CommonListItems.All);
			this.rblStatus.FillEnums<CourseRegistrationSetting.Statuses>();

			if (courseRegistrationSettingID == null)
			{
				this.ddlSemesterForSave.ClearSelection();
				this.rblType.SetEnumValue(CourseRegistrationSetting.Types.Student);
				this.dpFromDate.SelectedDate = null;
				this.dpToDate.SelectedDate = null;
				this.rblStatus.SetEnumValue(CourseRegistrationSetting.Statuses.Active);
				this.ddlInstituteForSave.ClearSelection();
				this.ddlInstituteForSave_OnSelectedIndexChanged(null, null);
			}
			else
			{
				var courseRegistrationSetting = CourseRegistrationSettings.GetCourseRegistrationSetting(courseRegistrationSettingID.Value, this.AdminIdentity.LoginSessionGuid);
				if (courseRegistrationSetting == null)
				{
					this.AddNoRecordFoundAlert();
					this.GetData(0);
					return;
				}
				this.ddlSemesterForSave.SelectedValue = courseRegistrationSetting.SemesterID.ToString();
				this.rblType.SetEnumValue(courseRegistrationSetting.TypeEnum);
				this.dpFromDate.SelectedDate = courseRegistrationSetting.FromDate;
				this.dpToDate.SelectedDate = courseRegistrationSetting.ToDate;
				this.rblStatus.SetEnumValue(courseRegistrationSetting.StatusEnum);
				this.ddlInstituteForSave.SelectedValue = courseRegistrationSetting.Program.InstituteID.ToString();
				this.ddlInstituteForSave_OnSelectedIndexChanged(null, null);
				this.ddlDepartmentIDForSave.SelectedValue = null;
				this.ddlDepartmentIDForSave_OnSelectedIndexChanged(null, null);
				this.cblPrograms.SelectedValue = courseRegistrationSetting.ProgramID.ToString();
			}
			this.modalControlPanel.Show();
		}

		#endregion
	}
}