﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Admin.FacultyManagement
{
	[AspirePage("CC8B03B9-7F90-45AA-8964-C4DA9D1DFA88", UserTypes.Admin, "~/Sys/Admin/FacultyManagement/FacultyMember.aspx", "Faculty Member", true, AspireModules.FacultyManagement)]
	public partial class FacultyMember : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.FacultyManagement.Module, new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_user_plus);
		public override string PageTitle => (this.FacultyMemberID == null) ? "Add Faculty Member" : "Edit Faculty Member";

		private int? FacultyMemberID => this.Request.GetParameterValue<int>(nameof(this.FacultyMemberID));

		public static string GetPageUrl(int? facultyMemberID)
		{
			if (facultyMemberID != null)
				return GetPageUrl<FacultyMember>().AttachQueryParam(nameof(FacultyMemberID), facultyMemberID);
			return GetPageUrl<FacultyMember>();
		}

		public static void Redirect(int? facultyMemberID)
		{
			Redirect(GetPageUrl(facultyMemberID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlFacultyType.FillFacultyTypes(CommonListItems.Select);
				this.ddlTitle.FillFacultyMemberTitles(CommonListItems.Select);
				this.ddlStatus.FillFacultyMemberStatuses(CommonListItems.Select);
				this.ddlGender.FillGenders(CommonListItems.Select);
				this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.Select);
				if (this.ddlInstituteID.Items.Count == 1)
				{
					this.AddErrorAlert("No institute record found.");
					Redirect<FacultyMembers>();
					return;
				}

				if (this.FacultyMemberID != null)
				{
					var facultyMember = Aspire.BL.Core.FacultyManagement.Admin.FacultyMembers.GetFacultyMember(this.FacultyMemberID.Value, this.AdminIdentity.LoginSessionGuid);
					if (facultyMember == null)
					{
						this.AddNoRecordFoundAlert();
						Redirect<FacultyMembers>();
						return;
					}
					this.ddlInstituteID.SelectedValue = facultyMember.InstituteID.ToString();
					this.ddlInstituteID.Enabled = false;
					this.ddlInstituteID_OnSelectedIndexChanged(null, null);
					this.ddlDepartmentID.SelectedValue = facultyMember.DepartmentID.ToString();
					this.ddlTitle.SetEnumValue(facultyMember.TitleEnum);
					this.tbName.Text = facultyMember.Name;
					this.tbDisplayName.Text = facultyMember.DisplayName;
					this.tbUsername.Text = facultyMember.UserName;
					this.tbEmail.Text = facultyMember.Email;
					this.ddlGender.SetEnumValue(facultyMember.GenderEnum);
					this.tbCNIC.Text = facultyMember.CNIC;
					this.tbPassportNo.Text = facultyMember.PassportNo;
					this.ddlFacultyType.SetEnumValue(facultyMember.FacultyTypeEnum);
					this.ddlStatus.SetEnumValue(facultyMember.StatusEnum);
				}
				else
				{
					this.ddlInstituteID.Enabled = true;
					this.ddlInstituteID_OnSelectedIndexChanged(null, null);
				}
			}
		}

		protected void ddlInstituteID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			if (instituteID == null)
				this.ddlDepartmentID.DataBind(CommonListItems.Select);
			else
				this.ddlDepartmentID.FillDepartments(instituteID.Value, CommonListItems.Select);
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToInt();
			var titleEnum = this.ddlTitle.GetSelectedEnumValue<Model.Entities.FacultyMember.Titles>();
			var name = this.tbName.Text;
			var displayName = this.tbDisplayName.Text;
			var username = this.tbUsername.Text;
			var email = this.tbEmail.Text;
			var genderEnum = this.ddlGender.GetSelectedEnumValue<Genders>();
			var cnic = this.tbCNIC.Text;
			var passportNo = this.tbPassportNo.Text;
			var facultyTypeEnum = this.ddlFacultyType.GetSelectedEnumValue<Model.Entities.FacultyMember.FacultyTypes>();
			var statusEnum = this.ddlStatus.GetSelectedEnumValue<Model.Entities.FacultyMember.Statuses>();

			if (this.FacultyMemberID == null)
			{
				var status = BL.Core.FacultyManagement.Admin.FacultyMembers.AddFacultyMember(instituteID, departmentID, titleEnum, name, displayName, username, email, genderEnum, cnic, passportNo, facultyTypeEnum, statusEnum, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.FacultyManagement.Admin.FacultyMembers.AddFacultyMemberStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Faculty member");
						Redirect<FacultyMembers>();
						return;
					case BL.Core.FacultyManagement.Admin.FacultyMembers.AddFacultyMemberStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<FacultyMembers>();
						return;
					case BL.Core.FacultyManagement.Admin.FacultyMembers.AddFacultyMemberStatuses.UsernameAlreadyExists:
						this.AddErrorAlert("Username already exists.");
						return;
					case BL.Core.FacultyManagement.Admin.FacultyMembers.AddFacultyMemberStatuses.CNICAlreadyExists:
						this.AddErrorAlert("CNIC already exists.");
						return;
					case BL.Core.FacultyManagement.Admin.FacultyMembers.AddFacultyMemberStatuses.DisplayNameIsRequiredBecauseNameAlreadyExists:
						this.AddErrorAlert("Provide Display Name because faculty member with the provided name already exists.");
						return;
					case BL.Core.FacultyManagement.Admin.FacultyMembers.AddFacultyMemberStatuses.DisplayNameMustBeUnique:
						this.AddErrorAlert("Provide unique Display Name because faculty member with the provided name/display name already exists.");
						return;
					case BL.Core.FacultyManagement.Admin.FacultyMembers.AddFacultyMemberStatuses.EmailAlreadyExists:
						this.AddErrorAlert("Email already exists.");
						return;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
			else
			{
				var status = BL.Core.FacultyManagement.Admin.FacultyMembers.UpdateFacultyMember(this.FacultyMemberID.Value, departmentID, titleEnum, name, displayName, username, email, genderEnum, cnic, passportNo, facultyTypeEnum, statusEnum, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.FacultyManagement.Admin.FacultyMembers.UpdateFacultyMemberStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Faculty member");
						Redirect<FacultyMembers>();
						return;
					case BL.Core.FacultyManagement.Admin.FacultyMembers.UpdateFacultyMemberStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<FacultyMembers>();
						return;
					case BL.Core.FacultyManagement.Admin.FacultyMembers.UpdateFacultyMemberStatuses.UsernameAlreadyExists:
						this.AddErrorAlert("Username already exists.");
						return;
					case BL.Core.FacultyManagement.Admin.FacultyMembers.UpdateFacultyMemberStatuses.CNICAlreadyExists:
						this.AddErrorAlert("CNIC already exists.");
						return;
					case BL.Core.FacultyManagement.Admin.FacultyMembers.UpdateFacultyMemberStatuses.DisplayNameIsRequiredBecauseNameAlreadyExists:
						this.AddErrorAlert("Provide Display Name because faculty member with the provided name already exists.");
						return;
					case BL.Core.FacultyManagement.Admin.FacultyMembers.UpdateFacultyMemberStatuses.DisplayNameMustBeUnique:
						this.AddErrorAlert("Provide unique Display Name because faculty member with the provided name/display name already exists.");
						return;
					case BL.Core.FacultyManagement.Admin.FacultyMembers.UpdateFacultyMemberStatuses.EmailAlreadyExists:
						this.AddErrorAlert("Email already exists.");
						return;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
		}
	}
}