﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="FacultyMembers.aspx.cs" Inherits="Aspire.Web.Sys.Admin.FacultyManagement.FacultyMembers" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<aspire:AspireHyperLinkButton class="pull-left" ButtonType="Success" runat="server" Text="Add Faculty Member" Glyphicon="plus" NavigateUrl="FacultyMember.aspx" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
				<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" OnSelectedIndexChanged="ddlInstituteID_OnSelectedIndexChanged" AutoPostBack="True" ValidationGroup="Filters" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
				<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" OnSelectedIndexChanged="ddlDepartmentID_OnSelectedIndexChanged" AutoPostBack="True" ValidationGroup="Filters" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatus" />
				<aspire:AspireDropDownList runat="server" ID="ddlStatus" OnSelectedIndexChanged="ddlStatus_OnSelectedIndexChanged" AutoPostBack="True" ValidationGroup="Filters" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Faculty Type:" AssociatedControlID="ddlFacultyType" />
				<aspire:AspireDropDownList runat="server" ID="ddlFacultyType" OnSelectedIndexChanged="ddlFacultyType_OnSelectedIndexChanged" AutoPostBack="True" ValidationGroup="Filters" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Search:" AssociatedControlID="tbSearchText" />
				<asp:Panel runat="server" CssClass="input-group" DefaultButton="btnSearch">
					<aspire:AspireTextBox runat="server" ID="tbSearchText" PlaceHolder="Search" ValidationGroup="Filters" />
					<div class="input-group-btn">
						<aspire:AspireButton ButtonType="Default" Glyphicon="search" runat="server" ID="btnSearch" OnClick="btnSearch_OnClick" ValidationGroup="Filters" />
					</div>
				</asp:Panel>
			</div>
		</div>
	</div>
	<aspire:AspireGridView runat="server" ID="gvFacultyMembers" ItemType="Aspire.BL.Core.FacultyManagement.Admin.FacultyMembers.FacultyMember" OnPageSizeChanging="gvFacultyMembers_OnPageSizeChanging" OnSorting="gvFacultyMembers_OnSorting" AutoGenerateColumns="False" OnRowCommand="gvFacultyMembers_OnRowCommand" OnPageIndexChanging="gvFacultyMembers_OnPageIndexChanging" AllowSorting="True">
		<Columns>
			<asp:TemplateField HeaderText="Title" SortExpression="TitleEnum">
				<ItemTemplate>
					<%#: (Item.TitleEnum?.ToFullName()).ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Name" SortExpression="Name">
				<ItemTemplate>
					<%#: Item.Name.ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Display Name" SortExpression="DisplayName">
				<ItemTemplate>
					<%#: Item.DisplayName.ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Email" SortExpression="Email">
				<ItemTemplate>
					<%#: Item.Email.ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Department" SortExpression="DepartmentAlias">
				<ItemTemplate>
					<%#: Item.DepartmentAlias.ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Gender" SortExpression="GenderEnum">
				<ItemTemplate>
					<%#: Item.GenderEnum.ToFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Type" SortExpression="FacultyTypeEnum">
				<ItemTemplate>
					<%#: Item.FacultyTypeEnum.ToFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireHyperLinkButton ButtonType="OnlyIcon" ToolTip="Edit" Target="_blank" Glyphicon="edit" runat="server" NavigateUrl='<%# Aspire.Web.Sys.Admin.FacultyManagement.FacultyMember.GetPageUrl(Item.FacultyMemberID) %>' />
					<aspire:AspireLinkButton ToolTip="Delete" CausesValidation="False" runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%# Item.FacultyMemberID %>' ConfirmMessage='<%# $"Are you sure want to delete this faculty member \"{Item.Name}\"?" %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
