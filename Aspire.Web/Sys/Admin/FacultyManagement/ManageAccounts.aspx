﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ManageAccounts.aspx.cs" Inherits="Aspire.Web.Sys.Admin.FacultyManagement.ManageAccounts" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Model.Entities" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:ScriptManagerProxy runat="server">
		<Scripts>
			<asp:ScriptReference Name="zxcvbn" />
		</Scripts>
	</asp:ScriptManagerProxy>
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
				<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" OnSelectedIndexChanged="ddlInstituteID_OnSelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartments" />
				<aspire:AspireDropDownList runat="server" ID="ddlDepartments" OnSelectedIndexChanged="ddlDepartments_OnSelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Faculty Type:" AssociatedControlID="ddlFacultyType" />
				<aspire:AspireDropDownList runat="server" ID="ddlFacultyType" OnSelectedIndexChanged="ddlFacultyType_OnSelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel Text="Search:" AssociatedControlID="tbSearchText" runat="server" />
				<asp:Panel runat="server" CssClass="input-group" DefaultButton="btnSearch">
					<aspire:AspireTextBox runat="server" ID="tbSearchText" ValidationGroup="Filters" PlaceHolder="Search..." />
					<div class="input-group-btn">
						<aspire:AspireButton ButtonType="Default" Glyphicon="search" runat="server" ID="btnSearch" ValidationGroup="Search" OnClick="btnSearch_OnClick" />
					</div>
				</asp:Panel>
			</div>
		</div>
	</div>
	<aspire:AspireGridView runat="server" ItemType="Aspire.BL.Core.FacultyManagement.Admin.FacultyMembers.FacultyMemberAccount" ID="gvFacultyMembers" OnPageSizeChanging="gvFacultyMembers_OnPageSizeChanging" OnSorting="gvFacultyMembers_OnSorting" AutoGenerateColumns="False" OnRowCommand="gvFacultyMembers_OnRowCommand" OnPageIndexChanging="gvFacultyMembers_OnPageIndexChanging" AllowSorting="True">
		<Columns>
			<asp:TemplateField HeaderText="Title" SortExpression="TitleEnum">
				<ItemTemplate>
					<%#: (Item.TitleEnum?.ToFullName()).ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Name" SortExpression="Name">
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" Text='<%# Item.Name.ToNAIfNullOrEmpty() %>' NavigateUrl='<%# Aspire.Web.Sys.Admin.FacultyManagement.FacultyMember.GetPageUrl(Item.FacultyMemberID) %>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Display Name" SortExpression="DisplayName">
				<ItemTemplate>
					<%#: Item.DisplayName.ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Email" SortExpression="Email">
				<ItemTemplate>
					<%#: Item.Email.ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="CNIC" SortExpression="CNIC">
				<ItemTemplate>
					<%#: Item.CNIC.ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Department" SortExpression="DepartmentAlias">
				<ItemTemplate>
					<%#: Item.DepartmentAlias.ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Type" SortExpression="FacultyTypeEnum">
				<ItemTemplate>
					<%#: Item.FacultyTypeEnum.ToFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CausesValidation="false" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument='<%# Item.FacultyMemberID %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalAccount" ModalSize="Large" HeaderText="Account Information">
		<BodyTemplate>
			<aspire:AspireAlert runat="server" ID="alertmodalAccount" />
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Title:" AssociatedControlID="lblTitle" />
						<div class="form-control-static">
							<aspire:Label runat="server" ID="lblTitle" />
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="lblName" />
						<div class="form-control-static">
							<aspire:Label runat="server" ID="lblName" />
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Display Name:" AssociatedControlID="lblDisplayName" />
						<div class="form-control-static">
							<aspire:Label runat="server" ID="lblDisplayName" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="lblDepartmentAlias" />
						<div class="form-control-static">
							<aspire:Label runat="server" ID="lblDepartmentAlias" />
						</div>
					</div>
				</div>
 				<div class="col-md-4">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Email:" AssociatedControlID="lblEmail" />
						<div class="form-control-static">
							<aspire:Label runat="server" ID="lblEmail" />
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Username:" AssociatedControlID="lblUserName" />
						<div class="form-control-static">
							<aspire:Label runat="server" ID="lblUserName" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="New Password:" AssociatedControlID="tbNewPassword" />
						<aspire:AspireTextBox runat="server" TextMode="Password" ValidationGroup="ChangePassword" ID="tbNewPassword" PlaceHolder="New Password" />
						<aspire:AspirePasswordStrengthMeter runat="server" TextBoxID="tbNewPassword" />
						<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="ChangePassword" ControlToValidate="tbNewPassword" RequiredErrorMessage="This field is required." />
						<aspire:AspirePasswordPolicyValidator SecurityLevel="Medium" runat="server" ControlToValidate="tbNewPassword" ValidationGroup="ChangePassword" />
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Confirm New Password:" AssociatedControlID="tbConfirmNewPassword" />
						<aspire:AspireTextBox runat="server" TextMode="Password" ValidationGroup="ChangePassword" ID="tbConfirmNewPassword" PlaceHolder="Confirm New Password" />
						<aspire:AspireCompareValidator ValidateEmptyText="True" runat="server" ErrorMessage="Passwords do not match." ValidationGroup="ChangePassword" ControlToValidate="tbConfirmNewPassword" ControlToCompare="tbNewPassword" />
					</div>
				</div>
 				<div class="col-md-4">
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Reason:" AssociatedControlID="ddlReason" />
						<aspire:AspireDropDownList runat="server" ID="ddlReason" ValidationGroup="ChangePassword" />
						<aspire:AspireRequiredFieldValidator runat="server" ErrorMessage="This field is required." ControlToValidate="ddlReason" ValidationGroup="ChangePassword" />
					</div>
				</div>
				<div class="col-md-6">
				</div>
			</div>
		</BodyTemplate>
		<FooterTemplate>
			<aspire:AspireButton runat="server" Text="Send Password Reset Link" OnClick="btnSendPasswordResetLink_OnClick" ID="btnSendPasswordResetLink" ButtonType="Danger" CausesValidation="False" ConfirmMessage="Are you sure you want to send the password reset link to the faculty member?" />
			<aspire:AspireButton runat="server" ButtonType="Primary" ID="btnResetPassword" Text="Change Password" OnClick="btnResetPassword_OnClick" ValidationGroup="ChangePassword" />
			<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
