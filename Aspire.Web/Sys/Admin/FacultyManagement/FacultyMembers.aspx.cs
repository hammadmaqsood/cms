﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.FacultyManagement
{
	[AspirePage("CB641935-0330-4F05-A8BB-DA3B744B4C0C", UserTypes.Admin, "~/Sys/Admin/FacultyManagement/FacultyMembers.aspx", "Faculty Members", true, AspireModules.FacultyManagement)]
	public partial class FacultyMembers : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.FacultyManagement.Module, new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_user);
		public override string PageTitle => "Faculty Members";

		public static string GetPageUrl(int? instituteID, int? departmentID, Model.Entities.FacultyMember.Statuses? statusEnum, Model.Entities.FacultyMember.FacultyTypes? facultyTypeEnum, string searchText, string sortExpression, SortDirection? sortDirection, int? pageIndex, int? pageSize)
		{
			return typeof(FacultyMembers).GetAspirePageAttribute().PageUrl.AttachQueryParams(
				("InstituteID", instituteID),
				("DepartmentID", departmentID),
				("StatusEnum", statusEnum),
				("FacultyTypeEnum", facultyTypeEnum),
				("SearchText", searchText),
				("SortExpression", sortExpression),
				("SortDirection", sortDirection),
				("PageIndex", pageIndex),
				("PageSize", pageSize));
		}

		public static void Redirect(int? instituteID, int? departmentID, Model.Entities.FacultyMember.Statuses? statusEnum, Model.Entities.FacultyMember.FacultyTypes? facultyTypeEnum, string searchText, string sortExpression, SortDirection? sortDirection, int? pageIndex, int? pageSize)
		{
			Redirect(GetPageUrl(instituteID, departmentID, statusEnum, facultyTypeEnum, searchText, sortExpression, sortDirection, pageIndex, pageSize));
		}

		private void RefreshPage()
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var statusEnum = this.ddlStatus.GetSelectedEnumValue<Model.Entities.FacultyMember.Statuses>(null);
			var facultyTypeEnum = this.ddlStatus.GetSelectedEnumValue<Model.Entities.FacultyMember.FacultyTypes>(null);
			var searchText = this.tbSearchText.Text;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var pageIndex = this.gvFacultyMembers.PageIndex;
			var pageSize = this.gvFacultyMembers.PageSize;
			Redirect(instituteID, departmentID, statusEnum, facultyTypeEnum, searchText, sortExpression, sortDirection, pageIndex, pageSize);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(nameof(BL.Core.FacultyManagement.Admin.FacultyMembers.FacultyMember.Name), SortDirection.Ascending);
				this.ddlStatus.FillEnums<Model.Entities.FacultyMember.Statuses>(CommonListItems.All);
				this.ddlFacultyType.FillEnums<Model.Entities.FacultyMember.FacultyTypes>(CommonListItems.All);
				this.ddlInstituteID.FillInstitutesForAdmin();
				this.ddlInstituteID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void gvFacultyMembers_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					var facultyMemberID = e.DecryptedCommandArgumentToInt();
					var status = Aspire.BL.Core.FacultyManagement.Admin.FacultyMembers.DeleteFacultyMember(facultyMemberID, this.AdminIdentity.LoginSessionGuid);
					switch (status)
					{
						case BL.Core.FacultyManagement.Admin.FacultyMembers.DeleteFacultyMemberStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.RefreshPage();
							return;
						case BL.Core.FacultyManagement.Admin.FacultyMembers.DeleteFacultyMemberStatuses.ChildRecordExistsOfferedCourses:
						case BL.Core.FacultyManagement.Admin.FacultyMembers.DeleteFacultyMemberStatuses.ChildRecordExistsQASurveyUsers:
						case BL.Core.FacultyManagement.Admin.FacultyMembers.DeleteFacultyMemberStatuses.ChildRecordExistsQASummaryFacultyEvaluations:
						case BL.Core.FacultyManagement.Admin.FacultyMembers.DeleteFacultyMemberStatuses.ChildRecordExistsSurveyUsers:
							this.AddErrorAlert("Faculty Member cannot be deleted because child record exists.");
							this.RefreshPage();
							return;
						case BL.Core.FacultyManagement.Admin.FacultyMembers.DeleteFacultyMemberStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Faculty member record");
							this.RefreshPage();
							return;
						default:
							throw new NotImplementedEnumException(status);
					}
			}
		}

		protected void ddlInstituteID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			if (instituteID == null)
			{
				this.AddErrorAlert("No Institute found.");
				Redirect<Dashboard>();
				return;
			}
			this.ddlDepartmentID.FillDepartments(instituteID.Value, CommonListItems.All);
			this.ddlDepartmentID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlStatus_OnSelectedIndexChanged(null, null);
		}

		protected void ddlStatus_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlFacultyType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlFacultyType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlTitle_OnSelectedIndexChanged(null, null);
		}

		protected void ddlTitle_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearch_OnClick(null, null);
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			this.GetFacultyMembers(0);
		}

		private void GetFacultyMembers(int pageIndex)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var status = this.ddlStatus.GetSelectedEnumValue<Model.Entities.FacultyMember.Statuses>(null);
			var facultyType = this.ddlFacultyType.GetSelectedEnumValue<Model.Entities.FacultyMember.FacultyTypes>(null);
			var searchText = this.tbSearchText.Text;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var (facultyMembers, virtualItemCount) = BL.Core.FacultyManagement.Admin.FacultyMembers.GetFacultyMembers(instituteID, departmentID, status, facultyType, searchText, pageIndex, this.gvFacultyMembers.PageSize, sortExpression, sortDirection, this.AdminIdentity.LoginSessionGuid);
			this.gvFacultyMembers.DataBind(facultyMembers, pageIndex, virtualItemCount, sortExpression, sortDirection);
		}

		protected void gvFacultyMembers_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvFacultyMembers.PageSize = e.NewPageSize;
			this.GetFacultyMembers(0);
		}

		protected void gvFacultyMembers_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetFacultyMembers(0);
		}

		protected void gvFacultyMembers_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetFacultyMembers(e.NewPageIndex);
		}
	}
}