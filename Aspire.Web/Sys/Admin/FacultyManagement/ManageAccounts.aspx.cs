﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.FacultyManagement
{
	[AspirePage("8B71AF50-1D2C-486C-8D42-7B301CAEDE2B", UserTypes.Admin, "~/Sys/Admin/FacultyManagement/ManageAccounts.aspx", "Manage Accounts", true, AspireModules.FacultyManagement)]
	public partial class ManageAccounts : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.FacultyManagement.Module, new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};

		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_cog);
		public override string PageTitle => "Manage Faculty Accounts";

		private void RefreshPage() { }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(nameof(BL.Core.FacultyManagement.Admin.FacultyMembers.FacultyMemberAccount.Name), SortDirection.Ascending);
				this.ddlInstituteID.FillInstitutesForAdmin();
				if (this.ddlInstituteID.Items.Count == 0)
				{
					this.AddErrorAlert("No institute record found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlSemesterID.FillSemesters();
				if (this.ddlSemesterID.Items.Count == 0)
				{
					this.AddErrorAlert("No semester record found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlFacultyType.FillEnums<Model.Entities.FacultyMember.FacultyTypes>(CommonListItems.All);
				this.ddlInstituteID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlInstituteID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
			this.ddlDepartments.FillDepartments(instituteID, CommonListItems.All);
			this.ddlDepartments_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartments_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlSemesterID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlFacultyType_OnSelectedIndexChanged(null, null);
		}

		protected void ddlFacultyType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearch_OnClick(null, null);
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			this.GetFacultyMembers(0);
		}

		private void GetFacultyMembers(int pageIndex)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
			var departmentID = this.ddlDepartments.SelectedValue.ToNullableInt();
			var facultyTypeEnum = this.ddlFacultyType.GetSelectedEnumValue<Model.Entities.FacultyMember.FacultyTypes>(null);
			var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
			var searchText = this.tbSearchText.Text;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var (facultyMemberAccounts, virtualItemCount) = BL.Core.FacultyManagement.Admin.FacultyMembers.GetFacultyMembersAccounts(instituteID, semesterID, departmentID, facultyTypeEnum, searchText, pageIndex, this.gvFacultyMembers.PageSize, sortExpression, sortDirection, this.AdminIdentity.LoginSessionGuid);
			this.gvFacultyMembers.DataBind(facultyMemberAccounts, pageIndex, virtualItemCount, sortExpression, sortDirection);
		}

		protected void gvFacultyMembers_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvFacultyMembers.PageSize = e.NewPageSize;
			this.GetFacultyMembers(0);
		}

		protected void gvFacultyMembers_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetFacultyMembers(0);
		}

		protected void gvFacultyMembers_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetFacultyMembers(e.NewPageIndex);
		}

		protected void gvFacultyMembers_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					var facultyMemberID = e.DecryptedCommandArgumentToInt();
					this.DisplayModalAccount(facultyMemberID);
					break;
			}
		}

		protected void btnResetPassword_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var facultyMemberID = (int)this.ViewState["FacultyMemberID"];
			var newPassword = this.tbNewPassword.Text;
			var reason = this.ddlReason.GetSelectedEnumValue<PasswordChangeReasons>();
			var reset = BL.Core.FacultyManagement.Admin.FacultyMembers.ResetFacultyMemberPassword(facultyMemberID, newPassword, reason, this.AdminIdentity.LoginSessionGuid);
			if (reset)
			{
				this.AddSuccessAlert("Password has been reset.");
				this.modalAccount.Hide();
				return;
			}
			else
			{
				this.AddNoRecordFoundAlert();
				Redirect<ManageAccounts>();
				return;
			}
		}

		protected void btnSendPasswordResetLink_OnClick(object sender, EventArgs e)
		{
			var facultyMemberID = (int)this.ViewState[nameof(Model.Entities.FacultyMember.FacultyMemberID)];
			var passwordResetPageUrl = GetAspirePageAttribute<Logins.Faculty.PasswordReset>().PageUrl.ToAbsoluteUrl();
			var reset = BL.Core.FacultyManagement.Admin.FacultyMembers.SendPasswordResetLinkToFacultyMember(facultyMemberID, passwordResetPageUrl, this.AdminIdentity.LoginSessionGuid);
			if (reset)
			{
				this.AddSuccessAlert("Password reset link has been sent.");
				this.modalAccount.Hide();
				return;
			}
			else
			{
				this.AddNoRecordFoundAlert();
				Redirect<ManageAccounts>();
				return;
			}
		}

		private void DisplayModalAccount(int facultyMemberID)
		{
			var facultyMemberAccount = BL.Core.FacultyManagement.Admin.FacultyMembers.GetFacultyMemberAccount(facultyMemberID, this.AdminIdentity.LoginSessionGuid);
			if (facultyMemberAccount == null)
			{
				this.AddNoRecordFoundAlert();
				this.RefreshPage();
				return;
			}

			if (string.IsNullOrWhiteSpace(facultyMemberAccount.Email) || facultyMemberAccount.Email.TryValidateEmailAddress() == null)
			{
				this.AddErrorAlert("Email address is missing or invalid. Provide email to proceed further.");
				FacultyMember.Redirect(facultyMemberAccount.FacultyMemberID);
				return;
			}
			if (string.IsNullOrWhiteSpace(facultyMemberAccount.CNIC))
			{
				this.AddErrorAlert("CNIC is missing or invalid. Provide CNIC to proceed further.");
				FacultyMember.Redirect(facultyMemberAccount.FacultyMemberID);
				return;
			}
			if (string.IsNullOrWhiteSpace(facultyMemberAccount.Username))
			{
				this.AddErrorAlert("Username is missing. Provide username to proceed further.");
				FacultyMember.Redirect(facultyMemberAccount.FacultyMemberID);
				return;
			}
			this.lblTitle.Text = (facultyMemberAccount.TitleEnum?.ToFullName()).ToNAIfNullOrEmpty();
			this.lblName.Text = facultyMemberAccount.Name.ToNAIfNullOrEmpty();
			this.lblDisplayName.Text = facultyMemberAccount.DisplayName.ToNAIfNullOrEmpty();
			this.lblDepartmentAlias.Text = facultyMemberAccount.DepartmentAlias.ToNAIfNullOrEmpty();
			this.lblEmail.Text = facultyMemberAccount.Email.ValidateEmailAddress();
			this.lblUserName.Text = facultyMemberAccount.Username.CannotBeEmptyOrWhitespace();
			this.ddlReason.FillPasswordChangeReasons(CommonListItems.Select);
			this.ViewState[nameof(Model.Entities.FacultyMember.FacultyMemberID)] = facultyMemberID;
			this.modalAccount.Show();
		}
	}
}