﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="FacultyMember.aspx.cs" Inherits="Aspire.Web.Sys.Admin.FacultyManagement.FacultyMember" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
				<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" OnSelectedIndexChanged="ddlInstituteID_OnSelectedIndexChanged" ValidationGroup="AddFaculty" AutoPostBack="True" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlInstituteID" ErrorMessage="This field is required." ValidationGroup="AddFaculty" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
				<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" ValidationGroup="AddFaculty" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlDepartmentID" ErrorMessage="This field is required." ValidationGroup="AddFaculty" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Title:" AssociatedControlID="ddlTitle" />
				<aspire:AspireDropDownList runat="server" ID="ddlTitle" ValidationGroup="AddFaculty" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlTitle" ErrorMessage="This field is required." ValidationGroup="AddFaculty" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbName" />
				<aspire:AspireTextBox runat="server" ID="tbName" ValidationGroup="AddFaculty" TextTransform="UpperCase" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbName" ErrorMessage="This field is required." ValidationGroup="AddFaculty" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Display Name:" AssociatedControlID="tbDisplayName" />
				<aspire:AspireTextBox runat="server" ID="tbDisplayName" ValidationGroup="AddFaculty" TextTransform="UpperCase" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Username:" AssociatedControlID="tbUsername" />
				<aspire:AspireTextBox runat="server" ID="tbUsername" ValidationGroup="AddFaculty" TextTransform="LowerCase" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbUsername" ErrorMessage="This field is required." ValidationGroup="AddFaculty" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Email:" AssociatedControlID="tbEmail" />
				<aspire:AspireTextBox runat="server" ID="tbEmail" ValidationGroup="AddFaculty" TextTransform="LowerCase" />
				<aspire:AspireStringValidator runat="server" ControlToValidate="tbEmail" ErrorMessage="This field is required." ValidationGroup="AddFaculty" ValidationExpression="Email" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid email address." AllowNull="False" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Gender:" AssociatedControlID="ddlGender" />
				<aspire:AspireDropDownList runat="server" ID="ddlGender" ValidationGroup="AddFaculty" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlGender" ErrorMessage="This field is required." ValidationGroup="AddFaculty" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="CNIC:" AssociatedControlID="tbCNIC" />
				<aspire:AspireTextBox data-inputmask="'alias': 'cnic'" runat="server" ID="tbCNIC" MaxLength="15" ValidationGroup="AddFaculty" />
				<aspire:AspireStringValidator runat="server" ValidationExpression="CNIC" ControlToValidate="tbCNIC" ValidationGroup="AddFaculty" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid CNIC." AllowNull="False" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Passport No.:" AssociatedControlID="tbPassportNo" />
				<aspire:AspireTextBox runat="server" ID="tbPassportNo" MaxLength="50" ValidationGroup="AddFaculty" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlFacultyType" />
				<aspire:AspireDropDownList runat="server" ID="ddlFacultyType" ValidationGroup="AddFaculty" />
				<aspire:AspireRequiredFieldValidator ValidationGroup="AddFaculty" runat="server" ControlToValidate="ddlFacultyType" ErrorMessage="This field is required." />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatus" />
				<aspire:AspireDropDownList runat="server" ID="ddlStatus" ValidationGroup="AddFaculty" />
				<aspire:AspireRequiredFieldValidator ValidationGroup="AddFaculty" runat="server" ControlToValidate="ddlStatus" ErrorMessage="This field is required." />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group text-center">
			<aspire:AspireButton runat="server" ID="btnSave" ButtonType="Primary" Text="Save" OnClick="btnSave_OnClick" ValidationGroup="AddFaculty" />
			<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="~/Sys/Admin/FacultyManagement/FacultyMembers.aspx" />
		</div>
	</div>
</asp:Content>
