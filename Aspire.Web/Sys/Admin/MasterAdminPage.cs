using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Admin
{
	public abstract class MasterAdminPage : AdminsPage
	{
		protected override void Authenticate()
		{
			if (this.AdminIdentity == null || !this.AdminIdentity.UserType.IsMasterAdmin())
				throw new AspireAccessDeniedException(UserTypes.MasterAdmin, UserTypes.MasterAdmin);
		}

		public override AspireThemes AspireTheme => AspireDefaultThemes.MasterAdmin;

		protected sealed override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => null;
	}
}