﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="StudentAttendance.aspx.cs" Inherits="Aspire.Web.Sys.Admin.ClassAttendance.StudentAttendance" %>

<%@ Import Namespace="Aspire.Model.Entities" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<style scoped="scoped">
		table.tab th {
			white-space: nowrap;
		}

		table.tab td {
			width: 50%;
		}
	</style>
	<table class="table table-bordered table-condensed tab">
		<caption>Student Information</caption>
		<tbody>
			<tr>
				<th>Enrollment:</th>
				<td>
					<aspire:AspireLabel runat="server" ID="lblEnrollment" /></td>
				<th>Registration No.</th>
				<td>
					<aspire:Label runat="server" ID="lblRegistration" />
				</td>
			</tr>
			<tr>
				<th>Name</th>
				<td>
					<aspire:Label runat="server" ID="lblName" /></td>
				<th>Program</th>
				<td>
					<aspire:Label runat="server" ID="lblStudentProgram" /></td>
			</tr>
		</tbody>
	</table>
	<table class="table table-bordered table-condensed tab">
		<caption>Course Information</caption>
		<tbody>
			<tr>
				<th>Title</th>
				<td>
					<aspire:Label runat="server" ID="lblCourseTitle" />
				</td>
				<th>Teacher Name</th>
				<td>
					<aspire:Label runat="server" ID="lblTeacherName" />
				</td>
			</tr>
			<tr>
				<th>Credit Hours</th>
				<td>
					<aspire:Label runat="server" ID="lblCreditHrs" />
				</td>
				<th>Contact Hours</th>
				<td>
					<aspire:Label runat="server" ID="lblContactHrs" />
				</td>
			</tr>
			<tr>
				<th>Class</th>
				<td>
					<aspire:Label runat="server" ID="lblClass" />
				</td>
				<th>Semester</th>
				<td>
					<aspire:Label runat="server" ID="lblSemesterID" />
				</td>
			</tr>
		</tbody>
	</table>
	<style scoped="scoped">
		table[id$=gvAttendanceDetails] td:first-child,
		table[id$=gvAttendanceDetails] th:first-child,
		table[id$=gvAttendanceDetails] th:nth-child(6),
		table[id$=gvAttendanceDetails] td:nth-child(6),
		table[id$=gvAttendanceDetails] th:nth-child(7),
		table[id$=gvAttendanceDetails] td:nth-child(7),
		table[id$=gvAttendanceDetails] th:nth-child(8),
		table[id$=gvAttendanceDetails] td:nth-child(8),
		table[id$=gvAttendanceDetails] th:nth-child(9),
		table[id$=gvAttendanceDetails] td:nth-child(9) {
			text-align: center;
		}
	</style>
	<aspire:AspireGridView Caption="Attendance Summary" runat="server" ID="gvStudentAttendance" AutoGenerateColumns="False" OnRowDataBound="gvStudentAttendance_OnRowDataBound">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate>
					<%#: Container.DataItemIndex + 1 %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField Visible="False">
				<ItemTemplate>
					<aspire:HiddenField runat="server" Mode="Encrypted" ID="hfOfferedCourseAttendanceID" Value='<%# this.Eval("OfferedCourseAttendanceID") %>' />
					<aspire:HiddenField runat="server" Mode="Encrypted" ID="hfTotalHours" Value='<%# this.Eval("TotalHours","{0:0.##}") %>' />
					<aspire:HiddenField runat="server" Mode="Encrypted" ID="hfAttendanceHours" Value='<%# this.Eval("Hours","{0:0.##}") %>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Class Date" DataField="ClassDate" DataFormatString="{0:D}" />
			<%--<asp:BoundField HeaderText="Class Date" DataField="OfferedCourseAttendanceDetailID"  />--%>
			<asp:BoundField HeaderText="Class Type" DataField="ClassTypeFullName" />
			<asp:BoundField HeaderText="Room" DataField="RoomName" />
			<asp:BoundField HeaderText="Remarks" DataField="Remarks" />
			<%--<asp:BoundField HeaderText="Hours" DataField="Hours" />--%>
			<asp:TemplateField HeaderText="Hours (Present)">
				<ItemTemplate>
					<aspire:AspireRadioButtonList runat="server" ID="rblHours" RepeatLayout="Flow" RepeatDirection="Horizontal" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<div class="text-center">
		<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" CausesValidation="False" OnClick="btnSave_OnClick" ConfirmMessage="Are you sure you want to update the attendance of a student?" />
		<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="~/Sys/Admin/ClassAttendance/MarkAttendance.aspx" />
	</div>
</asp:Content>
