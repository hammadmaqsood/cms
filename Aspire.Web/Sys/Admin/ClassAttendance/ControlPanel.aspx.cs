﻿using Aspire.BL.Core.ClassAttendance.Admin;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.ClassAttendance
{
	[AspirePage("D8D8E831-49F8-4F1A-B3A8-3BDBCC6EA7B9", UserTypes.Admins, "~/Sys/Admin/ClassAttendance/ControlPanel.aspx", "Control Panel", true, AspireModules.ClassAttendance)]
	public partial class ControlPanel : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.ClassAttendance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}},
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.cog);
		public override string PageTitle => "Control Panel: Class Attendance";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("SemesterID", SortDirection.Ascending);
				this.ddlSemesterIDFilter.FillSemesters(CommonListItems.All);
				this.ddlInstituteIDFilter.FillInstitutesForAdmin(CommonListItems.All);
				this.ddlStatusFilter.FillEnums<ClassAttendanceSetting.Statuses>(CommonListItems.All);
				this.ddlSemesterIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		#region Filters for Control Panel Record

		protected void btnAdd_OnClick(object sender, EventArgs e)
		{
			this.DisplayControlPanelModal(null);
		}

		protected void ddlSemesterIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlInstituteIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlInstituteIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlStatusFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlStatusFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearch_OnClick(null, null);
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		#endregion

		#region Control Panel

		protected void gvClassAttendanceSettings_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Status":
					var result = BL.Core.ClassAttendance.Admin.AttendanceSettings.ToggleClassAttendanceSettingStatus(e.DecryptedCommandArgumentToInt(), this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case AttendanceSettings.UpdateClassAttendanceSettingStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							Redirect<ControlPanel>();
							return;
						case AttendanceSettings.UpdateClassAttendanceSettingStatuses.Success:
							this.AddSuccessMessageHasBeenUpdated("Status");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.GetData(this.gvClassAttendanceSettings.PageIndex);
					break;
				case "Edit":
					e.Handled = true;
					this.DisplayControlPanelModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var status = BL.Core.ClassAttendance.Admin.AttendanceSettings.DeleteClassAttendanceSetting(e.DecryptedCommandArgumentToInt(), this.AdminIdentity.LoginSessionGuid);
					switch (status)
					{
						case AttendanceSettings.DeleteClassAttendanceSettingStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case AttendanceSettings.DeleteClassAttendanceSettingStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Setting");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					Redirect<ControlPanel>();
					return;
			}
		}

		protected void gvClassAttendanceSettings_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				var classAttendanceSetting = (BL.Core.ClassAttendance.Admin.AttendanceSettings.GetClassAttendanceSettingsResult.Setting)e.Row.DataItem;
				var fromDate = classAttendanceSetting.FromDate.Date;
				var toDate = classAttendanceSetting.ToDate.Date;
				if (DateTime.Today > toDate)
					e.Row.CssClass = "danger";
				else if (DateTime.Today >= fromDate)
					switch ((ClassAttendanceSetting.Statuses)classAttendanceSetting.Status)
					{
						case ClassAttendanceSetting.Statuses.Active:
							e.Row.CssClass = "success";
							break;
						case ClassAttendanceSetting.Statuses.Inactive:
							e.Row.CssClass = "warning";
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
			}
		}

		protected void gvClassAttendanceSettings_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvClassAttendanceSettings_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvClassAttendanceSettings_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvClassAttendanceSettings.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void ddlInstituteID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
			this.ddlProgramID.FillPrograms(instituteID, null, true, CommonListItems.All);
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var classAttendanceSettingID = (int?)this.ViewState["ClassAttendanceSettingID"];
			var fromDate = this.dtpFromDate.SelectedDate.Value;
			var toDate = this.dtpToDate.SelectedDate.Value;
			var canMarkPastDays = this.tbCanMarkPastDays.Text.ToByte();
			var canEditPastDays = this.tbCanEditPastDays.Text.ToByte();
			var statusEnum = this.rblStatus.GetSelectedEnumValue<ClassAttendanceSetting.Statuses>();
			if (fromDate > toDate)
			{
				this.alertModalControlPanel.AddErrorAlert("From Date must be less than or equal to To Date.");
				this.updateModalControlPanel.Update();
				return;
			}

			if (classAttendanceSettingID == null)
			{
				var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
				var semesterID = this.ddlSemesterID.SelectedValue.ToShort();
				var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
				var result = BL.Core.ClassAttendance.Admin.AttendanceSettings.AddClassAttendanceSetting(semesterID, instituteID, programID, fromDate, toDate, canMarkPastDays, canEditPastDays, statusEnum, this.AdminIdentity.LoginSessionGuid);
				var total = result.Added + result.Updated;
				if (result.Added == 1)
					this.AddSuccessAlert($"{result.Added} record has been added.");
				else if (result.Added > 1)
					this.AddSuccessAlert($"{result.Added} records have been added.");
				if (result.Updated == 1)
					this.AddSuccessAlert($"{result.Updated} record has been updated.");
				else if (result.Updated > 1)
					this.AddSuccessAlert($"{result.Updated} records have been updated.");

				if (total == 0)
					this.AddErrorAlert("No record have been added or updated.");
				Redirect<ControlPanel>();
			}
			else
			{
				var status = BL.Core.ClassAttendance.Admin.AttendanceSettings.UpdateClassAttendanceSetting(classAttendanceSettingID.Value, fromDate, toDate, canMarkPastDays, canEditPastDays, statusEnum, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case AttendanceSettings.UpdateClassAttendanceSettingStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<ControlPanel>();
						return;
					case AttendanceSettings.UpdateClassAttendanceSettingStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Class Attendance Setting");
						Redirect<ControlPanel>();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		#endregion

		#region Private Specific Functions

		private void DisplayControlPanelModal(int? classAttendanceSettingID)
		{
			this.ddlSemesterID.FillSemesters();
			this.ddlInstituteID.FillInstitutesForAdmin();

			this.rblStatus.FillEnums<ClassAttendanceSetting.Statuses>();
			if (classAttendanceSettingID == null)
			{
				this.ddlSemesterID.ClearSelection();
				this.ddlSemesterID.Enabled = true;
				this.ddlInstituteID.ClearSelection();
				this.ddlInstituteID_OnSelectedIndexChanged(null, null);
				this.ddlInstituteID.Enabled = true;
				this.ddlProgramID.ClearSelection();
				this.ddlProgramID.Enabled = true;
				this.dtpFromDate.SelectedDate = null;
				this.dtpFromDate.Enabled = true;
				this.dtpToDate.SelectedDate = null;
				this.dtpToDate.Enabled = true;
				this.rblStatus.SetEnumValue(ClassAttendanceSetting.Statuses.Inactive);
				this.tbCanMarkPastDays.Text = null;
				this.tbCanEditPastDays.Text = null;

				this.modalControlPanel.HeaderText = "Add Class Attendance";
				this.btnSave.Text = "Add";
				this.ViewState["ClassAttendanceSettingID"] = null;
				this.modalControlPanel.Show();
			}
			else
			{
				var classAttendanceSetting = BL.Core.ClassAttendance.Admin.AttendanceSettings.GetClassAttendanceSetting(classAttendanceSettingID.Value, this.AdminIdentity.LoginSessionGuid);
				if (classAttendanceSetting == null)
				{
					this.AddNoRecordFoundAlert();
					this.GetData(0);
					return;
				}
				this.ddlSemesterID.SelectedValue = classAttendanceSetting.SemesterID.ToString();
				this.ddlSemesterID.Enabled = false;
				this.ddlInstituteID.SelectedValue = classAttendanceSetting.Program.InstituteID.ToString();
				this.ddlInstituteID_OnSelectedIndexChanged(null, null);
				this.ddlInstituteID.Enabled = false;
				this.ddlProgramID.SelectedValue = classAttendanceSetting.ProgramID.ToString();
				this.ddlProgramID.Enabled = false;
				this.dtpFromDate.SelectedDate = classAttendanceSetting.FromDate;
				this.dtpToDate.SelectedDate = classAttendanceSetting.ToDate;
				this.rblStatus.SetEnumValue(classAttendanceSetting.StatusEnum);
				this.tbCanMarkPastDays.Text = classAttendanceSetting.CanMarkPastDays.ToString();
				this.tbCanEditPastDays.Text = classAttendanceSetting.CanEditPastDays.ToString();

				this.modalControlPanel.HeaderText = "Edit Class Attendance";
				this.btnSave.Text = "Save";
				this.ViewState["ClassAttendanceSettingID"] = classAttendanceSettingID.Value;
				this.modalControlPanel.Show();
			}
		}

		private void GetData(int pageIndex)
		{
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToNullableShort();
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToNullableInt();
			var statusEnum = this.ddlStatusFilter.GetSelectedEnumValue<ClassAttendanceSetting.Statuses>(null);
			var classAttendanceSettingsResult = BL.Core.ClassAttendance.Admin.AttendanceSettings.GetClassAttendanceSettings(semesterID, instituteID, statusEnum, pageIndex, this.gvClassAttendanceSettings.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.AdminIdentity.LoginSessionGuid);
			this.gvClassAttendanceSettings.DataBind(classAttendanceSettingsResult.Settings, pageIndex, classAttendanceSettingsResult.VirtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		#endregion
	}
}