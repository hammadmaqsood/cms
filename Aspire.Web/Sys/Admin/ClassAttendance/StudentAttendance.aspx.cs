﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using HiddenField = Aspire.Lib.WebControls.HiddenField;

namespace Aspire.Web.Sys.Admin.ClassAttendance
{
	[AspirePage("177332B3-2260-4BAA-B12A-E4BB87DB7820", UserTypes.Admins, "~/Sys/Admin/ClassAttendance/StudentAttendance.aspx", "Student Attendance", true, AspireModules.ClassAttendance)]
	public partial class StudentAttendance : AdminsPage
	{

		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.ClassAttendance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}},
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.list);
		public override string PageTitle => "Student Attendance";

		public static string GetPageUrl(int instituteID, int? registeredCourseID)
		{
			return GetPageUrl<StudentAttendance>().AttachQueryParams("InstituteID", instituteID, "RegisteredCourseID", registeredCourseID);
		}

		private int? InstituteID => this.Request.GetParameterValue<int>("InstituteID");
		private int? RegisteredCourseID => this.Request.GetParameterValue<int>("RegisteredCourseID");
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.RegisteredCourseID == null || this.InstituteID == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<MarkAttendance>();
					return;
				}
				var summary = BL.Core.ClassAttendance.Admin.ClassAttendance.GetStudentAttendanceDetails(this.InstituteID.Value, this.RegisteredCourseID.Value, this.AdminIdentity.LoginSessionGuid);
				if (summary == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<MarkAttendance>();
					return;
				}
				this.lblEnrollment.Text = summary.Enrollment;
				this.lblRegistration.Text = summary.RegistrationNo.ToString();
				this.lblName.Text = summary.Name;
				this.lblStudentProgram.Text = summary.StudentProgramAlias;

				this.lblClass.Text = summary.OfferedClass;
				this.lblContactHrs.Text = summary.ContactHours.ToCreditHoursFullName();
				this.lblCourseTitle.Text = summary.Title;
				this.lblCreditHrs.Text = summary.CreditHours.ToCreditHoursFullName();
				this.lblTeacherName.Text = summary.FacultyMemberName;
				this.lblSemesterID.Text = summary.OfferedSemesterID.ToSemesterString();
				this.gvStudentAttendance.DataBind(summary.AttendanceDetails.OrderBy(ad=> ad.ClassDate).ThenBy(ad=> ad.Hours));
				this.btnSave.Visible = summary.DeletedDate == null;
				this.btnSave.Enabled = summary.AttendanceDetails.Any();
			}
		}

		protected void gvStudentAttendance_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow)
				return;
			var rblHours = (AspireRadioButtonList)e.Row.FindControl("rblHours");
			var hfTotalHours = (HiddenField)e.Row.FindControl("hfTotalHours");
			var hfAttendanceHours = (HiddenField)e.Row.FindControl("hfAttendanceHours");
			var totalHours = hfTotalHours.Value.ToDecimal().ToHoursValue();
			var attendanceHourValue = hfAttendanceHours.Value.ToDecimal().ToHoursValue();
			var presentIfOneOrOneAndHalf = totalHours == OfferedCourseAttendance.HoursValues.One || totalHours == OfferedCourseAttendance.HoursValues.OneAndHalf;
			rblHours.FillHoursValues(totalHours.GetSmallerOrEqualHourValue(), presentIfOneOrOneAndHalf);
			rblHours.SetEnumValue(attendanceHourValue);
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var offeredCourseAttendanceIDs = new Dictionary<int, decimal>();
			foreach (GridViewRow row in this.gvStudentAttendance.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow)
					continue;
				var hfOfferedCourseAttendanceID = (HiddenField)row.FindControl("hfOfferedCourseAttendanceID");
				var rblHours = (AspireRadioButtonList)row.FindControl("rblHours");
				var hours = rblHours.GetSelectedEnumValue<OfferedCourseAttendance.HoursValues>().ToDecimal();
				offeredCourseAttendanceIDs.Add(hfOfferedCourseAttendanceID.Value.ToInt(), hours);
			}
			var result = BL.Core.ClassAttendance.Admin.ClassAttendance.UpdateOfferedCourseAttendanceDetailsOfStudent(this.InstituteID.Value, this.RegisteredCourseID.Value, offeredCourseAttendanceIDs, this.AdminIdentity.LoginSessionGuid);
			this.AddSuccessAlert($"{result.SuccessCount} out of {result.TotalRecord} attendance record(s) of student has been updated.");
			Redirect(GetPageUrl(this.InstituteID.Value, result.RegisteredCourseID));
		}
	}
}