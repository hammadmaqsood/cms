﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ControlPanel.aspx.cs" Inherits="Aspire.Web.Sys.Admin.ClassAttendance.ControlPanel" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireButton runat="server" ID="btnAdd" ButtonType="Success" CausesValidation="False" Glyphicon="plus" Text="Add" OnClick="btnAdd_OnClick" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Filter" ID="ddlSemesterIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteIDFilter" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Filter" ID="ddlInstituteIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatusFilter" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Filter" ID="ddlStatusFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlStatusFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<asp:Panel runat="server" DefaultButton="btnSearch">
						<aspire:AspireButton runat="server" Text="Search" ValidationGroup="Filter" ID="btnSearch" ButtonType="Primary" OnClick="btnSearch_OnClick" />
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvClassAttendanceSettings" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnSorting="gvClassAttendanceSettings_OnSorting" OnPageIndexChanging="gvClassAttendanceSettings_OnPageIndexChanging" OnPageSizeChanging="gvClassAttendanceSettings_OnPageSizeChanging" OnRowCommand="gvClassAttendanceSettings_OnRowCommand" OnRowDataBound="gvClassAttendanceSettings_OnRowDataBound">
		<Columns>
			<asp:BoundField HeaderText="Semester" SortExpression="SemesterID" DataField="Semester" />
			<asp:BoundField HeaderText="Institute" SortExpression="InstituteAlias" DataField="InstituteAlias" />
			<asp:BoundField HeaderText="Program" SortExpression="ProgramAlias" DataField="ProgramAlias" />
			<asp:BoundField HeaderText="From" SortExpression="FromDate" DataField="fromDate" DataFormatString="{0:D}" />
			<asp:BoundField HeaderText="To" SortExpression="ToDate" DataField="toDate" DataFormatString="{0:D}" />
			<asp:BoundField HeaderText="Can Mark Past Days" SortExpression="CanMarkPastDays" DataField="CanMarkPastDays" />
			<asp:BoundField HeaderText="Can Mark Edit Days" SortExpression="CanEditPastDays" DataField="CanEditPastDays" />
			<asp:TemplateField HeaderText="Status" SortExpression="Status">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Status" EncryptedCommandArgument='<%# Eval("ClassAttendanceSettingID") %>' Text='<%# Eval("StatusFullName") %>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Edit" Glyphicon="edit" EncryptedCommandArgument='<%# Eval("ClassAttendanceSettingID") %>' />
					<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Delete" Glyphicon="remove" EncryptedCommandArgument='<%# Eval("ClassAttendanceSettingID") %>' ConfirmMessage="Are you sure you want to delete?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>


	<aspire:AspireModal runat="server" ID="modalControlPanel">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updateModalControlPanel" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertModalControlPanel" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="Save" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterID" ErrorMessage="This field is required." ValidationGroup="Save" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" ValidationGroup="Save" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteID_OnSelectedIndexChanged" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlInstituteID" ErrorMessage="This field is required." ValidationGroup="Save" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlProgramID" ValidationGroup="Save" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="From:" AssociatedControlID="dtpFromDate" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpFromDate" ValidationGroup="Save" />
								<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dtpFromDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid date." ValidationGroup="Save" AllowNull="False" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="To:" AssociatedControlID="dtpToDate" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpToDate" ValidationGroup="Save" />
								<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dtpToDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid date." ValidationGroup="Save" AllowNull="False" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Can Mark Past Days:" AssociatedControlID="tbCanMarkPastDays" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbCanMarkPastDays" ValidationGroup="Save" />
								<aspire:AspireByteValidator runat="server" ControlToValidate="tbCanMarkPastDays" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Invalid range." InvalidNumberErrorMessage="Invalid number." ValidationGroup="Save" AllowNull="False" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Can Edit Past Days:" AssociatedControlID="tbCanEditPastDays" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbCanEditPastDays" ValidationGroup="Save" />
								<aspire:AspireByteValidator runat="server" ControlToValidate="tbCanEditPastDays" RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Invalid range." InvalidNumberErrorMessage="Invalid number." ValidationGroup="Save" AllowNull="False" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="rblStatus" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireRadioButtonList runat="server" ID="rblStatus" ValidationGroup="Save" RepeatLayout="Flow" RepeatDirection="Horizontal" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" OnClick="btnSave_OnClick" ValidationGroup="Save" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
