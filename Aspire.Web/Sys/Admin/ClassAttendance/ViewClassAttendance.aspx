﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ViewClassAttendance.aspx.cs" Inherits="Aspire.Web.Sys.Admin.ClassAttendance.ViewClassAttendance" %>

<%@ Import Namespace="Aspire.Web.Sys.Admin.ClassAttendance" %>
<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<style>
		table#courseInfo th {
			white-space: nowrap;
		}

		table#courseInfo td {
			width: 50%;
		}
	</style>
	<table class="table table-bordered table-condensed" id="courseInfo">
		<caption>Course Information</caption>
		<tbody>
			<tr>
				<th>Title</th>
				<td>
					<aspire:Label runat="server" ID="lblCourseTitle" />
				</td>
				<th>Teacher Name</th>
				<td>
					<aspire:Label runat="server" ID="lblTeacherName" />
				</td>
			</tr>
			<tr>
				<th>Credit Hours</th>
				<td>
					<aspire:Label runat="server" ID="lblCreditHrs" />
				</td>
				<th>Contact Hours</th>
				<td>
					<aspire:Label runat="server" ID="lblContactHrs" />
				</td>
			</tr>
			<tr>
				<th>Class</th>
				<td>
					<aspire:Label runat="server" ID="lblClass" />
				</td>
				<th>Semester</th>
				<td>
					<aspire:Label runat="server" ID="lblSemesterID" />
				</td>
			</tr>
		</tbody>
	</table>
	<div>
		<aspire:AspireHyperLinkButton runat="server" Glyphicon="plus" ID="hlMarkClassWiseAttendance" ButtonType="Success" Text="Mark Attendance" />
	</div>
	<style scoped="scoped">
		table[id$=gvAttendanceRecord] td:first-child,
		table[id$=gvAttendanceRecord] th:first-child,
		table[id$=gvAttendanceRecord] td:nth-child(4),
		table[id$=gvAttendanceRecord] th:nth-child(4) {
			text-align: center;
		}
	</style>
	<aspire:AspireGridView Caption="Attendance Record" runat="server" Condensed="true" ItemType="Aspire.BL.Entities.ClassAttendance.CustomViewClassAttendance.Detail" ID="gvAttendanceRecord" AutoGenerateColumns="False" ShowFooter="True">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate>
					<%#: Container.DataItemIndex + 1 %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Class Date" DataField="ClassDate" DataFormatString="{0:D}" />
			<asp:BoundField HeaderText="Class Type" DataField="ClassTypeFullName" />
			<asp:TemplateField HeaderText="Hours">
				<ItemTemplate><%#: Item.HoursValueFullName %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Room" DataField="RoomName" />
			<asp:TemplateField HeaderText="Remarks / Topics Covered">
				<ItemTemplate>
					<strong>Remarks:</strong>
					<p><%# (Item.Remarks?.HtmlEncode(true)).ToNAIfNullOrEmpty() %></p>
					<strong>Topics Covered:</strong>
					<p><%# (Item.TopicsCovered?.HtmlEncode(true)).ToNAIfNullOrEmpty() %></p>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Marked on Date/Time" DataField="MarkedDate" DataFormatString="{0:F}" />
			<asp:TemplateField ItemStyle-Wrap="false" HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireHyperLinkButton Glyphicon="edit" ButtonType="OnlyIcon" runat="server" NavigateUrl='<%# MarkClassWiseAttendance.GetPageUrl(Item.InstituteID, Item.OfferedCourseID, Item.OfferedCourseAttendanceID) %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
		<FooterStyle Font-Bold="True" />
	</aspire:AspireGridView>
</asp:Content>
