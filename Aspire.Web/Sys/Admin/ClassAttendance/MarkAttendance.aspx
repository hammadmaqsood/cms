﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="MarkAttendance.aspx.cs" Inherits="Aspire.Web.Sys.Admin.ClassAttendance.MarkAttendance" %>

<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Import Namespace="Aspire.Web.Sys.Admin.ClassAttendance" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-inline">
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlInstituteIDFilter" />
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDFilter" />
		</div>
		<div class="form-group">
			<div class="input-group">
				<aspire:AspireTextBox runat="server" ID="tbSearch" />
				<div class="input-group-btn">
					<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Search <span class="caret"></span></button>
					<ul class="dropdown-menu dropdown-menu-right">
						<li>
							<aspire:LinkButton runat="server" ID="lbFacultyMember" CausesValidation="False" Text="Faculty Member" OnClick="lbFacultyMember_OnClick" /></li>
						<li>
							<aspire:LinkButton runat="server" ID="lbStudent" Text="Student" OnClick="lbStudent_OnClick" ValidationGroup="SearchEnrollment" /></li>
					</ul>
				</div>
			</div>
			<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbSearch" ValidationGroup="SearchEnrollment" ErrorMessage="Enrollment is required." />
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvCourses" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gvCourses_OnPageIndexChanging" OnPageSizeChanging="gvCourses_OnPageSizeChanging" OnSorting="gvCourses_OnSorting">
		<Columns>
			<asp:BoundField HeaderText="Institute" DataField="InstituteAlias" SortExpression="InstituteAlias" />
			<asp:BoundField HeaderText="Title" DataField="Title" SortExpression="Title" />
			<asp:BoundField HeaderText="Class" DataField="ClassName" SortExpression="ClassName" />
			<asp:BoundField HeaderText="Faculty Member" DataField="FacultyMemberNameNA" SortExpression="FacultyMemberName" />
			<asp:TemplateField HeaderText="Is Deleted">
				<ItemTemplate>
					<aspire:AspireLabel runat="server" Visible="<%# ((int?)this.Eval(nameof(RegisteredCours.RegisteredCourseID)))!=null %>" Text='<%# this.Eval("IsDeletedYesNo") %>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" Visible="<%# ((int?)this.Eval(nameof(RegisteredCours.RegisteredCourseID)))!=null %>" NavigateUrl="<%# StudentAttendance.GetPageUrl((int)this.Eval(nameof(Institute.InstituteID)),(int?)this.Eval(nameof(RegisteredCours.RegisteredCourseID)))%>" Text="Mark Attendance" Target="_blank" />
					<aspire:AspireHyperLink runat="server" Visible="<%# ((int?)this.Eval(nameof(RegisteredCours.RegisteredCourseID)))==null %>" NavigateUrl="<%# ViewClassAttendance.GetPageUrl((int)this.Eval(nameof(OfferedCours.OfferedCourseID)))%>" Text="View Class Attendance" Target="_blank" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
