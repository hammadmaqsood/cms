﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="MarkClassWiseAttendance.aspx.cs" Inherits="Aspire.Web.Sys.Admin.ClassAttendance.MarkClassWiseAttendance" %>

<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<style>
		table.tab > tbody > tr > th {
			white-space: nowrap;
			vertical-align: middle;
		}

		table.tab > tbody > tr > td {
			width: 50%;
		}
	</style>
	<table class="table table-bordered table-condensed tab">
		<caption>Course Information</caption>
		<tbody>
			<tr>
				<th>Title</th>
				<td>
					<aspire:Label runat="server" ID="lblCourseTitle" />
				</td>
				<th>Teacher Name</th>
				<td>
					<aspire:Label runat="server" ID="lblTeacherName" />
				</td>
			</tr>
			<tr>
				<th>Credit Hours</th>
				<td>
					<aspire:Label runat="server" ID="lblCreditHrs" />
				</td>
				<th>Contact Hours</th>
				<td>
					<aspire:Label runat="server" ID="lblContactHrs" />
				</td>
			</tr>
			<tr>
				<th>Class</th>
				<td>
					<aspire:Label runat="server" ID="lblClass" />
				</td>
				<th>Semester</th>
				<td>
					<aspire:Label runat="server" ID="lblSemesterID" />
				</td>
			</tr>
		</tbody>
	</table>
	<div>
		<aspire:AspireHyperLinkButton runat="server" Glyphicon="camera" ID="hlViewClassAttendance" ButtonType="Default" Text="View Class Attendance" />
	</div>
	<table class="table table-bordered table-condensed tab">
		<caption>Attendance Information</caption>
		<tbody>
			<tr>
				<th>Class Date</th>
				<td>
					<aspire:AspireDateTimePickerTextbox HorizontalPosition="Left" VerticalPosition="Bottom" UserManualEntryAllowed="false" runat="server" ID="dtpClassDate" ValidationGroup="ClassAttendance" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="dtpClassDate" ErrorMessage="This field is required." ValidationGroup="ClassAttendance" />
					<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dtpClassDate" InvalidDataErrorMessage="Invalid Date." ValidationGroup="ClassAttendance" />
				</td>
				<th>Room</th>
				<td>
					<aspire:AspireDropDownList runat="server" ID="ddlRoom" ValidationGroup="ClassAttendance" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlRoom" ErrorMessage="This field is required." ValidationGroup="ClassAttendance" />
				</td>
			</tr>
			<tr>
				<th>Class Type</th>
				<td>
					<aspire:AspireRadioButtonList runat="server" ID="rblClassTypes" RepeatDirection="Horizontal" RepeatLayout="Flow" ValidationGroup="ClassAttendance" />
				</td>
				<th>No. of Hours</th>
				<td>
					<aspire:AspireRadioButtonList runat="server" ID="rblNoOfHours" RepeatDirection="Horizontal" RepeatLayout="Flow" ValidationGroup="ClassAttendance" AutoPostBack="True" OnSelectedIndexChanged="rblNoOfHours_OnSelectedIndexChanged" />
				</td>
			</tr>
			<tr>
				<th>Remarks</th>
				<td colspan="3">
					<aspire:AspireTextBox runat="server" ID="tbRemarks" ValidateRequestMode="Disabled" MaxLength="1000" Rows="2" TextMode="MultiLine" ValidationGroup="ClassAttendance" />
				</td>
			</tr>
			<tr>
				<th>Topics Covered<br />
					in this Lecture
					<br />
					<small class="text-danger">(mandatory)</small></th>
				<td colspan="3">
					<aspire:AspireTextBox runat="server" ID="tbTopicsCovered" ValidateRequestMode="Disabled" MaxLength="2000" Rows="3" TextMode="MultiLine" ValidationGroup="ClassAttendance" />
					<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbTopicsCovered" ErrorMessage="This field is required." ValidationGroup="ClassAttendance" />
				</td>
			</tr>
		</tbody>
	</table>
	<asp:Table runat="server" CssClass="table table-bordered table-condensed" ID="tableSummary">
		<asp:TableRow runat="server" TableSection="TableHeader">
			<asp:TableHeaderCell runat="server" CssClass="text-center col-xs-3 bg-success" />
			<asp:TableHeaderCell runat="server" CssClass="text-center col-xs-3 bg-warning" />
			<asp:TableHeaderCell runat="server" CssClass="text-center col-xs-3 bg-danger" />
		</asp:TableRow>
	</asp:Table>
	<aspire:AspireGridView runat="server" ID="gvOfferedCourseEnrollments" Stripped="False" Condensed="False" AutoGenerateColumns="False" OnRowCreated="gvOfferedCourseEnrollments_OnRowCreated">
		<Columns>
			<asp:TemplateField HeaderText="#" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
				<ItemTemplate>
					<%#: Container.DataItemIndex + 1 %>
					<aspire:HiddenField runat="server" Mode="Encrypted" Value='<%# this.Eval("RegisteredCourseID") %>' ID="hfRegisteredCourseID" />
					<aspire:HiddenField runat="server" Mode="Encrypted" Value='<%# this.Eval("OfferedCourseAttendaceDetailID") %>' ID="hfOCAID" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Enrollment" DataField="Enrollment" />
			<asp:BoundField HeaderText="Name" DataField="Name" />
			<asp:TemplateField HeaderText="Hours (Present)">
				<ItemTemplate>
					<aspire:AspireRadioButtonList runat="server" ID="rblHours" SelectedValue='<%# (byte)(OfferedCourseAttendance.HoursValues)this.Eval("HoursValue") %>' RepeatLayout="Flow" RepeatDirection="Horizontal" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Program" DataField="Program" />
			<asp:TemplateField HeaderText="Fee Paid">
				<ItemTemplate><%#: ((bool)Eval("FeePaid")).ToYesNo() %></ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<div class="text-center">
		<aspire:AspireButton runat="server" ID="btnDeleteAttendance" OnClick="btnDeleteAttendance_OnClick" ConfirmMessage="Are you sure you want to delete the class attendance?" Text="Delete" ButtonType="Danger" CausesValidation="False" />
		<aspire:AspireButton runat="server" ButtonType="Primary" Text="Save" ValidationGroup="ClassAttendance" ID="btnSaveAttendance" OnClick="btnSaveAttendance_OnClick" />
		<aspire:AspireHyperLink ID="hlCancel" runat="server" Text="Cancel" NavigateUrl="~/Sys/Admin/ClassAttendance/MarkAttendance.aspx" />
	</div>
</asp:Content>
