﻿using Aspire.BL.Entities.ClassAttendance;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.ClassAttendance
{
	[AspirePage("CC82212E-6514-445F-98AF-D61D07DAF86C", UserTypes.Admins, "~/Sys/Admin/ClassAttendance/MarkClassWiseAttendance.aspx", "Mark Class Wise Attendance", true, AspireModules.ClassAttendance)]
	public partial class MarkClassWiseAttendance : AdminsPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.list);
		public override string PageTitle => "Class Wise Attendance";
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.ClassAttendance.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}},
		};

		private bool ByPassChecks => true;
		private int OfferedCourseID
		{
			get => (int)this.ViewState[nameof(this.OfferedCourseID)];
			set => this.ViewState[nameof(this.OfferedCourseID)] = value;
		}
		private int? OfferedCourseAttendanceID
		{
			get => (int?)this.ViewState[nameof(this.OfferedCourseAttendanceID)];
			set => this.ViewState[nameof(this.OfferedCourseAttendanceID)] = value;
		}
		private int InstituteID
		{
			get => (int)this.ViewState[nameof(this.InstituteID)];
			set => this.ViewState[nameof(this.InstituteID)] = value;
		}

		public static string GetPageUrl(int instituteID, int offeredCourseID, int? offeredCourseAttendanceID)
		{
			return GetPageUrl<MarkClassWiseAttendance>().AttachQueryParams(nameof(InstituteID), instituteID, nameof(OfferedCourseID), offeredCourseID, nameof(OfferedCourseAttendanceID), offeredCourseAttendanceID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var instituteID = this.Request.GetParameterValue<int>("InstituteID");
				var offeredCourseID = this.Request.GetParameterValue<int>(nameof(this.OfferedCourseID));
				var offeredCourseAttendanceID = this.Request.GetParameterValue<int>(nameof(this.OfferedCourseAttendanceID));

				if (instituteID == null || offeredCourseID == null)
				{
					Redirect<MarkAttendance>();
					return;
				}
				this.hlViewClassAttendance.NavigateUrl = ViewClassAttendance.GetPageUrl(offeredCourseID.Value);
				//todo Making Single Source For Faculty, Staff , or Admin, With their permission.
				var offeredCourse = BL.ClassAttendance.Faculty.GetOfferedCourseAttendances(offeredCourseID.Value, offeredCourseAttendanceID, this.ByPassChecks, out bool canMark, out bool canEdit, out bool canDelete); // Making same source as in faculty.
				if (offeredCourse == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<MarkAttendance>();
					return;
				}
				this.OfferedCourseID = offeredCourse.OfferedCourseID;
				this.OfferedCourseAttendanceID = offeredCourse.OfferedCourseAttendanceID;
				this.InstituteID = instituteID.Value;
				this.lblClass.Text = offeredCourse.ClassName;
				this.lblContactHrs.Text = offeredCourse.ContactHours.ToCreditHoursFullName();
				this.lblCourseTitle.Text = offeredCourse.Title;
				this.lblCreditHrs.Text = offeredCourse.CreditHours.ToCreditHoursFullName();
				this.lblTeacherName.Text = offeredCourse.Teacher;
				this.lblSemesterID.Text = offeredCourse.OfferedSemesterID.ToSemesterString();

				this.rblNoOfHours.FillHoursValues(true);
				this.rblClassTypes.FillSemesters().FillEnums<OfferedCourseAttendance.ClassTypes>();
				this.ddlRoom.GetRoomsForAttendance(instituteID.Value);
				if (offeredCourse.OfferedCourseAttendanceID != null)
				{
					this.rblClassTypes.SetEnumValue(offeredCourse.ClassTypeEnum.Value);
					this.rblNoOfHours.SetEnumValue(offeredCourse.HoursValue.Value);
					this.dtpClassDate.SelectedDate = offeredCourse.ClassDate.Value;
					this.ddlRoom.SelectedValue = offeredCourse.RoomID.ToString();
					this.tbRemarks.Text = offeredCourse.Remarks;
					this.tbTopicsCovered.Text = offeredCourse.TopicsCovered;
					this.dtpClassDate.ReadOnly = this.dtpClassDate.Enabled = false;
					this.rblNoOfHours.Enabled = false;
					this.rblNoOfHours.AutoPostBack = false;
					this.rblNoOfHours.SelectedIndexChanged -= this.rblNoOfHours_OnSelectedIndexChanged;
					this.btnSaveAttendance.Text = "Save";
					this.btnSaveAttendance.ConfirmMessage = "Are you sure you want to update the attendance of whole class?";
					this.btnDeleteAttendance.Visible = true;
				}
				else
				{
					this.rblClassTypes.SetEnumValue(OfferedCourseAttendance.ClassTypes.Class);
					this.rblNoOfHours.Enabled = true;
					this.rblNoOfHours.SetEnumValue(OfferedCourseAttendance.HoursValues.One);
					this.dtpClassDate.SelectedDate = DateTime.Today;
					this.dtpClassDate.ReadOnly = false;
					this.btnSaveAttendance.Text = "Add";
					this.btnSaveAttendance.ConfirmMessage = "Are you sure you want to add the attendance of whole class?";
					this.btnDeleteAttendance.Visible = false;
				}
				this.rblNoOfHours_OnSelectedIndexChanged(null, null);
			}
		}

		protected void gvOfferedCourseEnrollments_OnRowCreated(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow)
				return;
			var rblHours = (AspireRadioButtonList)e.Row.FindControl("rblHours");
			var totalHours = this.rblNoOfHours.GetSelectedEnumValue<OfferedCourseAttendance.HoursValues>();
			var presentIfOneOrOneAndHalf = totalHours == OfferedCourseAttendance.HoursValues.One || totalHours == OfferedCourseAttendance.HoursValues.OneAndHalf;
			rblHours.FillHoursValues(totalHours.GetSmallerOrEqualHourValue(), presentIfOneOrOneAndHalf);
			if (!this.IsPostBack && this.OfferedCourseAttendanceID != null)
			{
				var hours = ((Aspire.BL.Entities.ClassAttendance.CustomOfferedCourseAttendanceDetails)e.Row.DataItem).HoursValue;
				switch (hours)
				{
					case null:
					case OfferedCourseAttendance.HoursValues.Absent:
						e.Row.CssClass = "bg-danger";
						break;
					case OfferedCourseAttendance.HoursValues.One:
					case OfferedCourseAttendance.HoursValues.OneAndHalf:
					case OfferedCourseAttendance.HoursValues.Two:
					case OfferedCourseAttendance.HoursValues.Three:
						e.Row.CssClass = totalHours == hours.Value ? "bg-success" : "bg-warning";
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		protected void btnSaveAttendance_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var registeredCourseIDs = new Dictionary<int, decimal>();
			var roomID = this.ddlRoom.SelectedValue.ToInt();
			var classTypeEnum = this.rblClassTypes.GetSelectedEnumValue<OfferedCourseAttendance.ClassTypes>();
			var remarks = this.tbRemarks.Text;
			var topicsCovered = this.tbTopicsCovered.Text;
			foreach (GridViewRow row in this.gvOfferedCourseEnrollments.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow)
					continue;
				var hfRegisteredCourseID = (Aspire.Lib.WebControls.HiddenField)row.FindControl("hfRegisteredCourseID");
				var rblHours = (AspireRadioButtonList)row.FindControl("rblHours");
				var hours = rblHours.GetSelectedEnumValue<OfferedCourseAttendance.HoursValues>().ToDecimal();
				registeredCourseIDs.Add(hfRegisteredCourseID.Value.ToInt(), hours);
			}
			if (this.OfferedCourseAttendanceID == null)
			{
				var classDate = this.dtpClassDate.SelectedDate.Value.Date;
				var hoursValue = this.rblNoOfHours.GetSelectedEnumValue<OfferedCourseAttendance.HoursValues>();
				var result = BL.Core.ClassAttendance.Admin.ClassAttendance.AddOfferedCourseAttendanceDetailsOfClass(this.InstituteID, this.OfferedCourseID, registeredCourseIDs, classDate, roomID, classTypeEnum, hoursValue, remarks, topicsCovered, this.AdminIdentity.LoginSessionGuid);
				this.AddSuccessAlert($"{result.SuccessCount} out of {result.TotalRecord} attendance record(s) of class has been added.");
				Redirect(GetPageUrl(this.InstituteID, this.OfferedCourseID, result.OfferedCourseAttendanceID));
			}
			else
			{
				var result = BL.Core.ClassAttendance.Admin.ClassAttendance.UpdateOfferedCourseAttendanceDetailsOfClass(this.InstituteID, this.OfferedCourseAttendanceID.Value, registeredCourseIDs, roomID, classTypeEnum, remarks, topicsCovered, this.AdminIdentity.LoginSessionGuid);
				this.AddSuccessAlert($"{result.SuccessCount} out of {result.TotalRecord} attendance record(s) of class has been updated.");
				Redirect(GetPageUrl(this.InstituteID, this.OfferedCourseID, result.OfferedCourseAttendanceID));
			}
		}

		protected void btnDeleteAttendance_OnClick(object sender, EventArgs e)
		{
			if (this.OfferedCourseAttendanceID == null)
				return;
			var result = BL.Core.ClassAttendance.Admin.ClassAttendance.DeleteOfferedCourseAttendanceDetailsOfClass(this.InstituteID, this.OfferedCourseAttendanceID.Value, this.AdminIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.ClassAttendance.Admin.ClassAttendance.DeleteOfferedCourseAttendanceDetailOfClassStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					break;
				case BL.Core.ClassAttendance.Admin.ClassAttendance.DeleteOfferedCourseAttendanceDetailOfClassStatuses.CanNotDelete:
					this.AddErrorAlert("You are unable to delete the class attendance");
					break;
				case BL.Core.ClassAttendance.Admin.ClassAttendance.DeleteOfferedCourseAttendanceDetailOfClassStatuses.Success:
					this.AddSuccessMessageHasBeenDeleted("Class attendance");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			Redirect(ViewClassAttendance.GetPageUrl(this.OfferedCourseID));
		}

		protected void rblNoOfHours_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			List<CustomOfferedCourseAttendanceDetails> attendanceDetails;
			//todo Making Single Source For Faculty, Staff , or Admin, With their permission.
			if (this.OfferedCourseAttendanceID == null)
			{
				this.tableSummary.Visible = false;
				attendanceDetails = BL.ClassAttendance.Faculty.GetOfferedCourseAttendanceDetails(this.OfferedCourseID, null);
				var totalHours = this.rblNoOfHours.GetSelectedEnumValue<OfferedCourseAttendance.HoursValues>();
				foreach (var enrollment in attendanceDetails)
					enrollment.HoursValue = totalHours;
			}
			else
			{
				attendanceDetails = BL.ClassAttendance.Faculty.GetOfferedCourseAttendanceDetails(this.OfferedCourseID, this.OfferedCourseAttendanceID.Value); // Making same source as in faculty.
				this.tableSummary.Visible = true;
				var totalHours = this.rblNoOfHours.GetSelectedEnumValue<OfferedCourseAttendance.HoursValues>();
				var presentCell = this.tableSummary.Rows[0].Cells[0];
				var partialCell = this.tableSummary.Rows[0].Cells[1];
				var absentCell = this.tableSummary.Rows[0].Cells[2];
				switch (totalHours)
				{
					case OfferedCourseAttendance.HoursValues.Absent:
						throw new InvalidOperationException();
					case OfferedCourseAttendance.HoursValues.One:
					case OfferedCourseAttendance.HoursValues.OneAndHalf:
						presentCell.Text = $"Present: {attendanceDetails.Count(a => a.HoursValue == totalHours)}";
						partialCell.Visible = false;
						absentCell.Text = $"Absent: {attendanceDetails.Count(a => a.HoursValue != totalHours)}";
						break;
					case OfferedCourseAttendance.HoursValues.Two:
					case OfferedCourseAttendance.HoursValues.Three:
						presentCell.Text = $"Full Present: {attendanceDetails.Count(a => a.HoursValue == totalHours)}";
						partialCell.Text = $"Partial Present: {attendanceDetails.Count(a => a.HoursValue != null && a.HoursValue != OfferedCourseAttendance.HoursValues.Absent && a.HoursValue != totalHours)}";
						absentCell.Text = $"Full Absent: {attendanceDetails.Count(a => (a.HoursValue ?? OfferedCourseAttendance.HoursValues.Absent) == OfferedCourseAttendance.HoursValues.Absent)}";
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			if (this.OfferedCourseAttendanceID != null || attendanceDetails.Any())
				this.gvOfferedCourseEnrollments.DataBind(attendanceDetails);
			else
			{
				this.AddWarningAlert("No valid student registration found for attendance.");
				Redirect<MarkAttendance>();
			}
		}
	}
}