﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.ClassAttendance
{
	[AspirePage("2E4E4D70-23AB-4DA2-BF46-FED9F7B3AF2B", UserTypes.Admins, "~/Sys/Admin/ClassAttendance/MarkAttendance.aspx", "Mark Attendance", true, AspireModules.ClassAttendance)]
	public partial class MarkAttendance : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.ClassAttendance.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_bookmark);
		public override string PageTitle => "Mark Attendance";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Title", SortDirection.Ascending);
				this.ddlSemesterIDFilter.FillSemesters();
				if (this.ddlSemesterIDFilter.Items.Count == 0)
				{
					this.AddErrorAlert("No semester record found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlInstituteIDFilter.FillInstitutesForAdmin();
				if (this.ddlInstituteIDFilter.Items.Count == 0)
				{
					this.AddErrorAlert("No institute record found.");
					Redirect<Dashboard>();
					return;
				}
			}
		}

		private void GetData(int pageIndex)
		{
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToInt();
			var semesterID = this.ddlSemesterIDFilter.SelectedValue.ToShort();
			var enrollment = this.ViewState["Enrollment"] as string;
			var facultyMember = this.ViewState["FacultyMember"] as string;
			int virtualItemsCount;
			var offeredCourses = BL.Core.ClassAttendance.Admin.ClassAttendance.GetOfferedCourses(instituteID, semesterID, enrollment, facultyMember, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), pageIndex, this.gvCourses.PageSize, out virtualItemsCount, this.AdminIdentity.LoginSessionGuid);
			this.gvCourses.DataBind(offeredCourses, pageIndex, virtualItemsCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}


		protected void gvCourses_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvCourses_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvCourses.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvCourses_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void lbFacultyMember_OnClick(object sender, EventArgs e)
		{
			this.ViewState["Enrollment"] = null;
			this.ViewState["FacultyMember"] = this.tbSearch.Text;
			this.GetData(0);
		}

		protected void lbStudent_OnClick(object sender, EventArgs e)
		{
			this.ViewState["Enrollment"] = this.tbSearch.Text;
			this.ViewState["FacultyMember"] = null;
			this.GetData(0);
		}
	}
}