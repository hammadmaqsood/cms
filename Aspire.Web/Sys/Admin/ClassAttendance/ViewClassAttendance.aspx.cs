﻿using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;

namespace Aspire.Web.Sys.Admin.ClassAttendance
{
	[AspirePage("B8412BBA-8BB9-44C1-AE4E-FE3E60735D73", UserTypes.Admins, "~/Sys/Admin/ClassAttendance/ViewClassAttendance.aspx", "View Class Attendance", true, AspireModules.ClassAttendance)]
	public partial class ViewClassAttendance : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.ClassAttendance.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.list);
		public override string PageTitle => "View Class Attendance";

		public static string GetPageUrl(int offeredCourseID)
		{
			return GetPageUrl<ViewClassAttendance>().AttachQueryParam("OfferedCourseID", offeredCourseID);
		}

		public static void Redirect(int offeredCourseID)
		{
			Redirect(GetPageUrl(offeredCourseID));
		}

		private int? OfferedCourseID => this.Request.GetParameterValue<int>("OfferedCourseID");

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.OfferedCourseID == null)
				{
					Redirect<MarkAttendance>();
					return;
				}
				//todo Making Single Source For Faculty, Staff , or Admin, With their permission.
				var offeredCourseAttendance = BL.ClassAttendance.Faculty.GetOfferedCourseAttendances(this.OfferedCourseID.Value); // Making same source as in faculty.
				if (offeredCourseAttendance == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<MarkAttendance>();
					return;
				}
				this.hlMarkClassWiseAttendance.NavigateUrl = MarkClassWiseAttendance.GetPageUrl(offeredCourseAttendance.InstituteID, offeredCourseAttendance.OfferedCourseID, null);
				this.lblCourseTitle.Text = $"[{offeredCourseAttendance.CourseCode}] {offeredCourseAttendance.Title}";
				this.lblTeacherName.Text = offeredCourseAttendance.TeacherName.ToNAIfNullOrEmpty();
				this.lblCreditHrs.Text = offeredCourseAttendance.CreditHours.ToCreditHoursFullName();
				this.lblContactHrs.Text = offeredCourseAttendance.ContactHours.ToCreditHoursFullName();
				this.lblClass.Text = offeredCourseAttendance.ClassName;
				this.lblSemesterID.Text = offeredCourseAttendance.SemesterID.ToSemesterString();
				if (offeredCourseAttendance.Details.Any())
					this.gvAttendanceRecord.ShowFooter = true;
				this.gvAttendanceRecord.DataBind(offeredCourseAttendance.Details);
				if (this.gvAttendanceRecord.ShowFooter && this.gvAttendanceRecord.FooterRow != null)
				{
					var hours = offeredCourseAttendance.Details.Sum(d => d.Hours);
					this.gvAttendanceRecord.FooterRow.Cells[3].Text = hours.ToString("N");
				}
			}
		}
	}
}