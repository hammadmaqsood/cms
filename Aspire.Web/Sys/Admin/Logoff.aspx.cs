﻿using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Admin
{
	[AspirePage("{0BF799F9-09DB-4CDF-ADD2-08779155A2CB}", UserTypes.Admins, "~/Sys/Admin/Logoff.aspx", null, false, AspireModules.None)]
	public partial class Logoff : LogoffBasePage
	{
		public static string GetPageUrl(UserLoginHistory.LogOffReasons logoffReason)
		{
			return GetPageUrl(GetPageUrl<Logoff>(), logoffReason);
		}

		public static void Redirect(UserLoginHistory.LogOffReasons logoffReason)
		{
			Redirect(GetPageUrl(logoffReason));
		}

		protected override string LoginPageUrl => GetPageUrl<Logins.Admin.Login>();
	}
}