﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.FileServer
{
	[AspirePage("C654DFCC-62A0-4E39-A3B6-64341F503034", UserTypes.Admins, "~/Sys/Admin/FileServer/FileExplorer.aspx", "File Explorer", true, AspireModules.FileServer)]
	public partial class FileExplorer : AdminsPage
	{
		public override PageIcon PageIcon => Aspire.Lib.WebControls.FontAwesomeIcons.solid_folder_open.GetIcon();

		public override string PageTitle => "File Explorer";

		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.FeeManagement.Module,UserGroupPermission.Allowed}
		};

		public static string GetPageUrl(SystemFolder.FolderTypes? folderTypeEnum, bool showDeleted, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection)
		{
			return typeof(FileExplorer).GetAspirePageAttribute().PageUrl.AttachQueryParams(
				("ShowDeleted", showDeleted),
				("SearchText", searchText),
				("FolderTypeEnum", folderTypeEnum),
				("PageIndex", pageIndex),
				("PageSize", pageSize),
				("SortExpression", sortExpression),
				("SortDirection", sortDirection));
		}

		public static void Redirect(SystemFolder.FolderTypes? folderTypeEnum, bool showDeleted, string searchText, int pageIndex, int pageSize, string sortExpression, SortDirection sortDirection)
		{
			Redirect(GetPageUrl(folderTypeEnum, showDeleted, searchText, pageIndex, pageSize, sortExpression, sortDirection));
		}

		private void RefreshPage()
		{
			var showDeleted = this.ddlShowDeleted.SelectedValue.ToBoolean();
			var searchText = this.tbSearchText.Text;
			var folderTypeEnum = this.ddlFolderType.GetSelectedEnumValue<SystemFolder.FolderTypes>(null);
			var systemFilesPageIndex = this.gvSystemFiles.PageIndex;
			var systemFilesPageSize = this.gvSystemFiles.PageSize;
			var systemFilesSortExpression = this.ViewState.GetSortExpression();
			var systemFilesSortDirection = this.ViewState.GetSortDirection();
			Redirect(folderTypeEnum, showDeleted, searchText, systemFilesPageIndex, systemFilesPageSize, systemFilesSortExpression, systemFilesSortDirection);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(this.GetParameterValue("SortExpression") ?? nameof(SystemFile.CreatedDate), this.GetParameterValue<SortDirection>("SortDirection") ?? SortDirection.Descending);
				this.ddlFolderType.FillEnums<SystemFolder.FolderTypes>(CommonListItems.All);
				this.ddlShowDeleted.FillYesNo().SetEnumValue(this.GetParameterValue<bool>("ShowDeleted") ?? false);
				this.tbSearchText.Text = this.GetParameterValue("SearchText");
				this.gvSystemFiles.PageSize = this.GetParameterValue<int>("PageSize") ?? this.gvSystemFiles.PageSize;
				this.ddlShowDeleted_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlFolderType_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlShowDeleted_SelectedIndexChanged(null, null);
		}

		protected void ddlShowDeleted_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearch_Click(null, null);
		}

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
				this.GetData(this.GetParameterValue<int>("PageIndex") ?? 0);
			else
				this.GetData(0);
		}


		protected void gvSystemFiles_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvSystemFiles_PageSizeChanging(object sender, Lib.WebControls.AspireGridView.PageSizeEventArgs e)
		{
			this.gvSystemFiles.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvSystemFiles_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
		{

		}

		protected void gvSystemFiles_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected string TotalSize = 0.ToDiskSize();

		private void GetData(int pageIndex)
		{
			var folderTypeEnum = this.ddlFolderType.GetSelectedEnumValue<SystemFolder.FolderTypes>(null);
			var showDeleted = this.ddlShowDeleted.SelectedValue.ToBoolean();
			var searchText = this.tbSearchText.Text;
			var pageSize = this.gvSystemFiles.PageSize;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var result = BL.Core.FileServer.MasterAdmin.FileExplorer.GetSystemFiles(folderTypeEnum, showDeleted, searchText, pageIndex, pageSize, sortDirection, sortExpression, this.AdminIdentity.LoginSessionGuid);
			this.TotalSize = result.TotalSize.ToDiskSize(2);
			this.gvSystemFiles.DataBind(result.Files, pageIndex, result.VirtualItemCount, sortExpression, sortDirection);
		}
	}
}