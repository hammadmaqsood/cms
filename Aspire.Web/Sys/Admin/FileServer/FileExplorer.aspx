﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Base.Master" CodeBehind="FileExplorer.aspx.cs" Inherits="Aspire.Web.Sys.Admin.FileServer.FileExplorer" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<asp:Content runat="server" ContentPlaceHolderID="BodyPH">
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlFolderType" />
				<aspire:AspireDropDownList runat="server" ID="ddlFolderType" OnSelectedIndexChanged="ddlFolderType_SelectedIndexChanged" AutoPostBack="True" ValidationGroup="Filters" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Show Deleted:" AssociatedControlID="ddlShowDeleted" />
				<aspire:AspireDropDownList runat="server" ID="ddlShowDeleted" OnSelectedIndexChanged="ddlShowDeleted_SelectedIndexChanged" AutoPostBack="True" ValidationGroup="Filters" />
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<aspire:AspireLabel Text="Search:" AssociatedControlID="tbSearchText" runat="server" />
				<asp:Panel runat="server" CssClass="input-group" DefaultButton="btnSearch">
					<aspire:AspireTextBox runat="server" ID="tbSearchText" ValidationGroup="Filters" PlaceHolder="Search..." />
					<div class="input-group-btn">
						<aspire:AspireButton ButtonType="Default" Glyphicon="search" runat="server" ID="btnSearch" ValidationGroup="Search" OnClick="btnSearch_Click" />
					</div>
				</asp:Panel>
			</div>
		</div>
	</div>
	<aspire:AspireGridView runat="server" ID="gvSystemFiles" ShowFooter="true" AllowSorting="true" AutoGenerateColumns="false" ItemType="Aspire.BL.Core.FileServer.MasterAdmin.FileExplorer.GetSystemFilesResult.SystemFile" OnPageIndexChanging="gvSystemFiles_PageIndexChanging" OnPageSizeChanging="gvSystemFiles_PageSizeChanging" OnRowCommand="gvSystemFiles_RowCommand" OnSorting="gvSystemFiles_Sorting">
		<Columns>
			<asp:TemplateField HeaderText="File ID">
				<ItemTemplate><%#: Item.SystemFileID %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Folder Type" SortExpression="FolderType">
				<ItemTemplate><%#: Item.FolderType %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="File Name" SortExpression="FileName">
				<ItemTemplate><%#: Item.FileName %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Extension" SortExpression="FileExtension">
				<ItemTemplate><%#: Item.FileExtension %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="FileSize" SortExpression="FileSize">
				<ItemTemplate><%#: Item.FileSize.ToDiskSize(2) %></ItemTemplate>
				<FooterTemplate><%#: this.TotalSize %></FooterTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Created Date" SortExpression="CreatedDate">
				<ItemTemplate><%#: Item.CreatedDate.ToString("g") %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Created By" SortExpression="CreateByUserName">
				<ItemTemplate><%#: $"{Item.CreatedByUserName} ({Item.CreatedByUserTypeEnum.ToFullName()})" %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Deleted Date" SortExpression="DeletedDate">
				<ItemTemplate><%#: (Item.DeletedDate?.ToString("g")).ToNAIfNullOrEmpty() %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Deleted By" SortExpression="DeletedByUserName">
				<ItemTemplate><%#: Item.DeletedDate == null ? string.Empty.ToNAIfNullOrEmpty(): $"{Item.DeletedByUserName} ({Item.DeletedByUserTypeEnum?.ToFullName()})" %></ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
