﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="FeeHeads.aspx.cs" Inherits="Aspire.Web.Sys.Admin.FeeManagement.FeeHeads" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-group">
		<aspire:AspireButton runat="server" ButtonType="Success" ID="btnAddFeeHead" CausesValidation="False" Text="Add" Glyphicon="plus" OnClick="btnAddFeeHead_OnClick" />
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlInstituteIDFilter" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteIDFilter_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlFeeHeadTypeFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlFeeHeadTypeFilter" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlFeeHeadTypeFilter_SelectedIndexChanged" />
		</div>
		<div class="col-md-3">
			<aspire:AspireLabel runat="server" Text="Rule:" AssociatedControlID="ddlRuleTypeFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlRuleTypeFilter" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlRuleTypeFilter_SelectedIndexChanged" />
		</div>
		<div class="col-md-3">
			<aspire:AspireLabel runat="server" Text="Refundable:" AssociatedControlID="ddlRefundableFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlRefundableFilter" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlRefundableFilter_SelectedIndexChanged" />
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<aspire:AspireLabel runat="server" Text="Concession:" AssociatedControlID="ddlConcessionFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlConcessionFilter" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlConcessionFilter_SelectedIndexChanged" />
		</div>
		<div class="col-md-3">
			<aspire:AspireLabel runat="server" Text="Visible:" AssociatedControlID="ddlVisibleFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlVisibleFilter" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlVisibleFilter_SelectedIndexChanged" />
		</div>
		<div class="col-md-3">
			<aspire:AspireLabel runat="server" Text="WHT Applicable:" AssociatedControlID="ddlWithholdingTaxApplicableFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlWithholdingTaxApplicableFilter" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlWithholdingTaxApplicableFilter_SelectedIndexChanged" />
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Search:" AssociatedControlID="tbSearchFilter" />
				<asp:Panel runat="server" CssClass="input-group" DefaultButton="btnSearchFilter">
					<aspire:AspireTextBox runat="server" ID="tbSearchFilter" ValidationGroup="Filters" />
					<div class="input-group-btn">
						<aspire:AspireButton ButtonType="Default" Glyphicon="search" runat="server" ID="btnSearchFilter" ValidationGroup="Search" OnClick="btnSearchFilter_Click" />
					</div>
				</asp:Panel>
			</div>

		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvFeeHead" ItemType="Aspire.Model.Entities.FeeHead" AutoGenerateColumns="False" OnRowCommand="gvFeeHead_OnRowCommand">
		<Columns>
			<asp:BoundField HeaderText="Institute" DataField="Institute.InstituteAlias" />
			<asp:TemplateField HeaderText="#">
				<ItemTemplate>
					<%#: Item.DisplayIndex + 1 %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Type" DataField="FeeHeadTypeFullName" />
			<asp:BoundField HeaderText="Name" DataField="HeadName" />
			<asp:BoundField HeaderText="Short Name" DataField="HeadShortName" />
			<asp:BoundField HeaderText="Alias" DataField="HeadAlias" />
			<asp:BoundField HeaderText="Rule" DataField="RuleTypeEnum" />
			<asp:TemplateField HeaderText="Refundable">
				<ItemTemplate>
					<%#: Item.Refundable.ToYesNo() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Concession">
				<ItemTemplate>
					<%#: Item.Concession.ToYesNo() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Withholding Tax Applicable">
				<ItemTemplate>
					<%#: Item.WithholdingTaxApplicable.ToYesNo() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Visible">
				<ItemTemplate>
					<%#: Item.Visible.ToYesNo() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Actions" ItemStyle-Wrap="False">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CausesValidation="false" ToolTip="Move Up" CommandName="Up" Glyphicon="chevron_up" EncryptedCommandArgument="<%# Item.FeeHeadID %>" />
					<aspire:AspireLinkButton runat="server" CausesValidation="false" ToolTip="Move Down" CommandName="Down" Glyphicon="chevron_down" EncryptedCommandArgument="<%# Item.FeeHeadID %>" />
					<aspire:AspireLinkButton runat="server" CausesValidation="false" ToolTip="Edit" CommandName="Edit" Glyphicon="edit" EncryptedCommandArgument="<%# Item.FeeHeadID %>" />
					<aspire:AspireLinkButton runat="server" CausesValidation="false" ToolTip="Delete" CommandName="Delete" Glyphicon="remove" ConfirmMessage='<%# $"Are you sure you want to delete fee head \"{Item.HeadName}\"?" %>' EncryptedCommandArgument="<%# Item.FeeHeadID %>" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalFeeHead">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updatePanelAddFeeHead">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="modalAlert" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
						<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" ValidationGroup="Add" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlInstituteID" ValidationGroup="Add" ErrorMessage="Required" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Heads Type:" AssociatedControlID="ddlFeeHeadType" />
						<aspire:AspireDropDownList runat="server" ID="ddlFeeHeadType" ValidationGroup="Add" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlFeeHeadType" ValidationGroup="Add" ErrorMessage="Required" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Head Name:" AssociatedControlID="tbHeadName" />
						<aspire:AspireTextBox runat="server" ID="tbHeadName" ValidationGroup="Add" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbHeadName" ErrorMessage="This field is required." ValidationGroup="Add" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Head Short Name:" AssociatedControlID="tbHeadShortName" />
						<aspire:AspireTextBox runat="server" ID="tbHeadShortName" ValidationGroup="Add" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbHeadShortName" ErrorMessage="This field is required." ValidationGroup="Add" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Head Alias:" AssociatedControlID="tbHeadAlias" />
						<aspire:AspireTextBox runat="server" ID="tbHeadAlias" ValidationGroup="Add" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbHeadAlias" ErrorMessage="This field is required." ValidationGroup="Add" />
					</div>
					<div class="row">
						<div class="form-group col-md-6">
							<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="rblRuleType" />
							<div>
								<aspire:AspireRadioButtonList runat="server" ID="rblRuleType" RepeatDirection="Horizontal" RepeatLayout="Flow" ValidationGroup="Add" />
							</div>
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblRuleType" ErrorMessage="This field is required." ValidationGroup="Add" />
						</div>
						<div class="form-group col-md-6">
							<aspire:AspireLabel runat="server" Text="Refundable:" AssociatedControlID="rblRefundable" />
							<div>
								<aspire:AspireRadioButtonList runat="server" ID="rblRefundable" RepeatDirection="Horizontal" RepeatLayout="Flow" ValidationGroup="Add" />
							</div>
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblRefundable" ErrorMessage="This field is required." ValidationGroup="Add" />
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-6">
							<aspire:AspireLabel runat="server" Text="Concession:" AssociatedControlID="rblConcession" />
							<div>
								<aspire:AspireRadioButtonList runat="server" ID="rblConcession" RepeatDirection="Horizontal" RepeatLayout="Flow" ValidationGroup="Add" />
							</div>
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblConcession" ErrorMessage="This field is required." ValidationGroup="Add" />
						</div>
						<div class="form-group col-md-6">
							<aspire:AspireLabel runat="server" Text="Withholding Tax Applicable:" AssociatedControlID="rblWithholdingTaxApplicable" />
							<div>
								<aspire:AspireRadioButtonList runat="server" ID="rblWithholdingTaxApplicable" RepeatDirection="Horizontal" RepeatLayout="Flow" ValidationGroup="Add" />
							</div>
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblWithholdingTaxApplicable" ErrorMessage="This field is required." ValidationGroup="Add" />
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-6">
							<aspire:AspireLabel runat="server" Text="Visible:" AssociatedControlID="rblVisible" />
							<div>
								<aspire:AspireRadioButtonList runat="server" ID="rblVisible" RepeatDirection="Horizontal" RepeatLayout="Flow" ValidationGroup="Add" />
							</div>
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblVisible" ErrorMessage="This field is required." ValidationGroup="Add" />
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" OnClick="btnSave_OnClick" ValidationGroup="Add" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
