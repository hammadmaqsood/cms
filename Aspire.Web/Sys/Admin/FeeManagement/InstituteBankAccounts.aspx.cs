﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.FeeManagement
{
	[AspirePage("43D99B81-707F-4E62-B37A-20F25B7B4A91", UserTypes.Admins, "~/Sys/Admin/FeeManagement/InstituteBankAccounts.aspx", "Bank Accounts", true, AspireModules.FeeManagement)]
	public partial class InstituteBankAccounts : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.FeeManagement.Module, new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_building);
		public override string PageTitle => "Bank Accounts";

		public static string GetPageURL(int? instituteID, InstituteBankAccount.FeeTypes? feeTypeEnum, InstituteBankAccount.AccountTypes? accountTypeEnum, bool? active, string sortExpression, SortDirection? sortDirection)
		{
			return typeof(InstituteBankAccounts).GetAspirePageAttribute().PageUrl.AttachQueryParams(new Dictionary<string, object>
			{
				{nameof(InstituteID), instituteID},
				{nameof(FeeTypeEnum), feeTypeEnum},
				{nameof(AccountTypeEnum), accountTypeEnum},
				{nameof(Active), active},
				{nameof(SortExpression), sortExpression},
				{nameof(SortDirection), sortDirection}
			});
		}

		public static void Redirect(int? instituteID, InstituteBankAccount.FeeTypes? feeTypeEnum, InstituteBankAccount.AccountTypes? accountTypeEnum, bool? active, string sortExpression, SortDirection? sortDirection)
		{
			Redirect(GetPageURL(instituteID, feeTypeEnum, accountTypeEnum, active, sortExpression, sortDirection));
		}

		private int? InstituteID => this.Request.GetParameterValue<int>(nameof(this.InstituteID));
		private InstituteBankAccount.FeeTypes? FeeTypeEnum => this.Request.GetParameterValue<InstituteBankAccount.FeeTypes>(nameof(this.FeeTypeEnum));
		private InstituteBankAccount.AccountTypes? AccountTypeEnum => this.Request.GetParameterValue<InstituteBankAccount.AccountTypes>(nameof(this.AccountTypeEnum));
		private bool? Active => this.Request.GetParameterValue<bool>(nameof(this.Active));
		private string SortExpression => this.Request.GetParameterValue(nameof(this.SortExpression));
		private SortDirection? SortDirection => this.Request.GetParameterValue<SortDirection>(nameof(this.SortDirection));

		private void Refresh()
		{
			Redirect(this.ddlInstituteIDFilter.SelectedValue.ToNullableInt(),
				this.ddlFeeTypeFilter.GetSelectedEnumValue<InstituteBankAccount.FeeTypes>(null),
				this.ddlAccountTypeFilter.GetSelectedEnumValue<InstituteBankAccount.AccountTypes>(null),
				this.ddlActiveFilter.SelectedValue.ToNullableBoolean(),
				this.ViewState.GetSortExpression(),
				this.ViewState.GetSortDirection());
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(this.SortExpression ?? nameof(Institute.InstituteAlias), this.SortDirection ?? System.Web.UI.WebControls.SortDirection.Ascending);
				this.ddlInstituteIDFilter.FillInstitutesForAdmin(CommonListItems.All).SetSelectedValueIfNotPostback(nameof(this.InstituteID));
				this.ddlFeeTypeFilter.FillInstituteBankAccountFeeTypes(CommonListItems.All).SetSelectedEnumValueIfNotPostback<InstituteBankAccount.FeeTypes>(nameof(this.FeeTypeEnum));
				this.ddlAccountTypeFilter.FillInstituteBankAccountTypes(CommonListItems.All).SetSelectedEnumValueIfNotPostback<InstituteBankAccount.AccountTypes>(nameof(this.AccountTypeEnum));
				this.ddlActiveFilter.FillYesNo(CommonListItems.All).SetBooleanValue(this.Active);
				this.ddlInstituteIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void btnAddBankAccount_OnClick(object sender, EventArgs e)
		{
			this.DisplayBankAccountModal(null);
		}

		protected void ddlInstituteIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlFeeTypeFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlFeeTypeFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlAccountTypeFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlAccountTypeFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlActiveFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlActiveFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void gvBankAccounts_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayBankAccountModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var instituteBankAccountID = e.DecryptedCommandArgumentToInt();
					var result = BL.Core.FeeManagement.Admin.InstituteBankAccounts.DeleteInstituteBankAccount(instituteBankAccountID, this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case BL.Core.FeeManagement.Admin.InstituteBankAccounts.DeleteInstituteBankAccountStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.FeeManagement.Admin.InstituteBankAccounts.DeleteInstituteBankAccountStatuses.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Bank account");
							break;
						case BL.Core.FeeManagement.Admin.InstituteBankAccounts.DeleteInstituteBankAccountStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Bank account");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.Refresh();
					break;
			}
		}

		protected void gvBankAccounts_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvBankAccounts_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvBankAccounts.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvBankAccounts_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var instituteBankAccountID = (int?)this.ViewState[nameof(InstituteBankAccount.InstituteBankAccountID)];
			var instituteID = this.ddlInstitute.SelectedValue.ToInt();
			var feeTypeEnum = this.ddlFeeType.GetSelectedEnumValue<InstituteBankAccount.FeeTypes>();
			var bankID = this.ddlBankID.SelectedValue.ToInt();
			var accountTitle = this.tbAccountTitle.Text;
			var accountTitleForDisplay = this.tbAccountTitleForDisplay.Text;
			var accountNo = this.tbAccountNo.Text;
			var accountNoForDisplay = this.tbAccountNoForDisplay.Text;
			var branchCode = this.tbBranchCode.Text;
			var branchName = this.tbBranchName.Text;
			var branchAddress = this.tbBranchAddress.Text;
			var branchForDisplay = this.tbBranchForDisplay.Text;
			var accountTypeEnum = this.ddlAccountType.GetSelectedEnumValue<InstituteBankAccount.AccountTypes>();
			var active = this.rblActive.SelectedValue.ToBoolean();

			if (instituteBankAccountID == null)
			{
				var result = BL.Core.FeeManagement.Admin.InstituteBankAccounts.AddInstituteBankAccount(instituteID, feeTypeEnum, bankID, accountTitle, accountTitleForDisplay, accountNo, accountNoForDisplay, branchCode, branchName, branchAddress, branchForDisplay, accountTypeEnum, active, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.FeeManagement.Admin.InstituteBankAccounts.AddInstituteBankAccountsStatuses.AlreadyExists:
						this.modalAlertBankAccount.AddMessage(AspireAlert.AlertTypes.Error, "Bank account already exists");
						this.updateModalBankAccountPanel.Update();
						break;
					case BL.Core.FeeManagement.Admin.InstituteBankAccounts.AddInstituteBankAccountsStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Bank account");
						this.Refresh();
						break;
					case BL.Core.FeeManagement.Admin.InstituteBankAccounts.AddInstituteBankAccountsStatuses.NotConsistent:
						this.modalAlertBankAccount.AddErrorAlert("Institute can have only one active online account per fee type and bank.");
						this.updateModalBankAccountPanel.Update();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var result = BL.Core.FeeManagement.Admin.InstituteBankAccounts.UpdateInstituteBankAccount(instituteBankAccountID.Value, accountTitle, accountTitleForDisplay, accountNo, accountNoForDisplay, branchCode, branchName, branchAddress, branchForDisplay, accountTypeEnum, active, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.FeeManagement.Admin.InstituteBankAccounts.UpdateInstituteBankAccountStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.Refresh();
						break;
					case BL.Core.FeeManagement.Admin.InstituteBankAccounts.UpdateInstituteBankAccountStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Bank account");
						this.Refresh();
						break;
					case BL.Core.FeeManagement.Admin.InstituteBankAccounts.UpdateInstituteBankAccountStatuses.NotConsistent:
						this.modalAlertBankAccount.AddErrorAlert("Institute can have only one active online account per fee type and bank.");
						this.updateModalBankAccountPanel.Update();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		private void DisplayBankAccountModal(int? instituteBankAccountID)
		{
			this.ddlInstitute.FillInstitutesForAdmin();
			this.ddlBankID.DataBind(BL.Core.Common.Lists.GetBanksList());
			this.ddlFeeType.FillInstituteBankAccountFeeTypes();
			this.ddlAccountType.FillInstituteBankAccountTypes();
			this.rblActive.FillYesNo();
			if (instituteBankAccountID == null)
			{
				this.ddlInstitute.ClearSelection();
				this.ddlInstitute.Enabled = true;
				this.ddlFeeType.ClearSelection();
				this.ddlFeeType.Enabled = true;
				this.ddlBankID.ClearSelection();
				this.ddlBankID.Enabled = true;
				this.tbAccountTitle.Text = null;
				this.tbAccountTitleForDisplay.Text = null;
				this.tbAccountNo.Text = null;
				this.tbAccountNoForDisplay.Text = null;
				this.tbBranchCode.Text = null;
				this.tbBranchName.Text = null;
				this.tbBranchAddress.Text = null;
				this.tbBranchForDisplay.Text = null;
				this.ddlAccountType.ClearSelection();
				this.rblActive.SetSelectedValueYes();

				this.modalBankAccount.HeaderText = "Add Bank Account";
				this.ViewState[nameof(InstituteBankAccount.InstituteBankAccountID)] = null;
				this.modalBankAccount.Show();
			}
			else
			{
				var instituteBankAccount = BL.Core.FeeManagement.Admin.InstituteBankAccounts.GetInstituteBankAccount(instituteBankAccountID.Value, this.AdminIdentity.LoginSessionGuid);
				this.ddlInstitute.SelectedValue = instituteBankAccount.InstituteID.ToString();
				this.ddlInstitute.Enabled = false;
				this.ddlFeeType.SetEnumValue(instituteBankAccount.FeeTypeEnum);
				this.ddlFeeType.Enabled = false;
				this.ddlBankID.SelectedValue = instituteBankAccount.BankID.ToString();
				this.ddlBankID.Enabled = false;
				this.tbAccountTitle.Text = instituteBankAccount.AccountTitle;
				this.tbAccountTitleForDisplay.Text = instituteBankAccount.AccountTitleForDisplay;
				this.tbAccountNo.Text = instituteBankAccount.AccountNo;
				this.tbAccountNoForDisplay.Text = instituteBankAccount.AccountNoForDisplay;
				this.tbBranchCode.Text = instituteBankAccount.BranchCode;
				this.tbBranchName.Text = instituteBankAccount.BranchName;
				this.tbBranchAddress.Text = instituteBankAccount.BranchAddress;
				this.tbBranchForDisplay.Text = instituteBankAccount.BranchForDisplay;
				this.ddlAccountType.SetEnumValue(instituteBankAccount.AccountTypeEnum);
				this.rblActive.SetBooleanValue(instituteBankAccount.Active);

				this.modalBankAccount.HeaderText = "Edit Bank Account";
				this.ViewState[nameof(InstituteBankAccount.InstituteBankAccountID)] = instituteBankAccount.InstituteBankAccountID;
				this.modalBankAccount.Show();
			}
		}

		private void GetData(int pageIndex)
		{
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToNullableInt();
			var feeTypeEnum = this.ddlFeeTypeFilter.GetSelectedEnumValue<InstituteBankAccount.FeeTypes>(null);
			var accountTypeEnum = this.ddlAccountTypeFilter.GetSelectedEnumValue<InstituteBankAccount.AccountTypes>(null);
			var active = this.ddlActiveFilter.SelectedValue.ToNullableBoolean();
			var instituteBankAccounts = BL.Core.FeeManagement.Admin.InstituteBankAccounts.GetInstituteBankAccounts(instituteID, feeTypeEnum, accountTypeEnum, active, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), pageIndex, this.gvBankAccounts.PageSize, out var virtualItemCount, this.AdminIdentity.LoginSessionGuid);
			this.gvBankAccounts.DataBind(instituteBankAccounts, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}
	}
}