﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.FeeManagement
{
	[AspirePage("0B6F8E9F-ED88-4F5D-8CCE-28E4BF527B1C", UserTypes.Admins, "~/Sys/Admin/FeeManagement/FeeHeads.aspx", "Fee Heads", true, AspireModules.FeeManagement)]
	public partial class FeeHeads : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.FeeManagement.Module,UserGroupPermission.Allowed}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.pencil);
		public override string PageTitle => "Fee Heads";

		public static void Redirect(int? instituteID, FeeHead.FeeHeadTypes? feeHeadType, FeeHead.RuleTypes? ruleType, bool? refundable, bool? concession, bool? visible, bool? withholdingTaxApplicable, string searchText)
		{
			Redirect<FeeHeads>(nameof(InstituteID), instituteID,
				nameof(FeeHeadType), feeHeadType,
				nameof(RuleType), ruleType,
				nameof(Refundable), refundable,
				nameof(Concession), concession,
				nameof(Visible), visible,
				nameof(WithholdingTaxApplicable), withholdingTaxApplicable,
				nameof(SearchText), searchText);
		}

		private int? InstituteID => this.GetParameterValue<int>(nameof(this.InstituteID));
		private FeeHead.FeeHeadTypes? FeeHeadType => this.GetParameterValue<FeeHead.FeeHeadTypes>(nameof(this.FeeHeadType));
		private FeeHead.RuleTypes? RuleType => this.GetParameterValue<FeeHead.RuleTypes>(nameof(this.RuleType));
		private bool? Refundable => this.GetParameterValue<bool>(nameof(this.Refundable));
		private bool? Concession => this.GetParameterValue<bool>(nameof(this.Concession));
		private new bool? Visible => this.GetParameterValue<bool>(nameof(this.Visible));
		private bool? WithholdingTaxApplicable => this.GetParameterValue<bool>(nameof(this.WithholdingTaxApplicable));
		private string SearchText => this.GetParameterValue(nameof(this.SearchText));

		private void RefreshPage()
		{
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToNullableInt();
			var feeHeadTypeEnum = this.ddlFeeHeadTypeFilter.GetSelectedEnumValue<FeeHead.FeeHeadTypes>(null);
			var ruleTypeEnum = this.ddlRuleTypeFilter.GetSelectedEnumValue<FeeHead.RuleTypes>(null);
			var refundable = this.ddlRefundableFilter.SelectedValue.ToNullableBoolean();
			var concession = this.ddlConcessionFilter.SelectedValue.ToNullableBoolean();
			var visible = this.ddlVisibleFilter.SelectedValue.ToNullableBoolean();
			var withholdingTaxAppliable = this.ddlWithholdingTaxApplicableFilter.SelectedValue.ToNullableBoolean();
			var searchText = this.tbSearchFilter.Text.ToNullIfWhiteSpace();
			Redirect(instituteID, feeHeadTypeEnum, ruleTypeEnum, refundable, concession, visible, withholdingTaxAppliable, searchText);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlInstituteIDFilter.FillInstitutesForAdmin(CommonListItems.All);
				if (this.InstituteID != null)
					this.ddlInstituteIDFilter.SelectedValue = this.InstituteID.ToString();

				this.ddlFeeHeadTypeFilter.FillEnums<FeeHead.FeeHeadTypes>(CommonListItems.All);
				if (this.FeeHeadType != null)
					this.ddlFeeHeadTypeFilter.SetEnumValue(this.FeeHeadType.Value);

				this.ddlRuleTypeFilter.FillEnums<FeeHead.RuleTypes>(CommonListItems.All);
				if (this.RuleType != null)
					this.ddlRuleTypeFilter.SetEnumValue(this.RuleType.Value);

				this.ddlRefundableFilter.FillYesNo(CommonListItems.All);
				if (this.Refundable != null)
					this.ddlRefundableFilter.SetBooleanValue(this.Refundable.Value);

				this.ddlConcessionFilter.FillYesNo(CommonListItems.All);
				if (this.Concession != null)
					this.ddlConcessionFilter.SetBooleanValue(this.Concession.Value);

				this.ddlVisibleFilter.FillYesNo(CommonListItems.All);
				if (this.Visible != null)
					this.ddlVisibleFilter.SetBooleanValue(this.Visible.Value);

				this.ddlWithholdingTaxApplicableFilter.FillYesNo(CommonListItems.All);
				if (this.WithholdingTaxApplicable != null)
					this.ddlWithholdingTaxApplicableFilter.SetBooleanValue(this.WithholdingTaxApplicable.Value);

				if (!string.IsNullOrWhiteSpace(this.SearchText))
					this.tbSearchFilter.Text = this.SearchText;

				this.ddlInstituteIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void btnAddFeeHead_OnClick(object sender, EventArgs e)
		{
			this.DisplayFeeHeadsModal(null);
		}

		protected void ddlInstituteIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlFeeHeadTypeFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlFeeHeadTypeFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlRuleTypeFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlRuleTypeFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlRefundableFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlRefundableFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlConcessionFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlConcessionFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlVisibleFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlVisibleFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlWithholdingTaxApplicableFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlWithholdingTaxApplicableFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearchFilter_Click(null, null);
		}

		protected void btnSearchFilter_Click(object sender, EventArgs e)
		{
			this.GetData();
		}

		private void GetData()
		{
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToNullableInt();
			var feeHeadTypeEnum = this.ddlFeeHeadTypeFilter.GetSelectedEnumValue<FeeHead.FeeHeadTypes>(null);
			var ruleTypeEnum = this.ddlRuleTypeFilter.GetSelectedEnumValue<FeeHead.RuleTypes>(null);
			var refundable = this.ddlRefundableFilter.SelectedValue.ToNullableBoolean();
			var concession = this.ddlConcessionFilter.SelectedValue.ToNullableBoolean();
			var visible = this.ddlVisibleFilter.SelectedValue.ToNullableBoolean();
			var withholdingTaxAppliable = this.ddlWithholdingTaxApplicableFilter.SelectedValue.ToNullableBoolean();
			var searchText = this.tbSearchFilter.Text.ToNullIfWhiteSpace();
			var feeHeads = BL.Core.FeeManagement.Admin.FeeHeads.GetFeeHeads(instituteID, feeHeadTypeEnum, ruleTypeEnum, refundable, concession, visible, withholdingTaxAppliable, searchText, this.AdminIdentity.LoginSessionGuid);
			this.gvFeeHead.DataBind(feeHeads);
		}

		protected void gvFeeHead_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Up":
					var feeHeadIDForUp = e.DecryptedCommandArgumentToInt();
					var movedUp = BL.Core.FeeManagement.Admin.FeeHeads.ShiftFeeHeadUp(feeHeadIDForUp, this.AdminIdentity.LoginSessionGuid);
					switch (movedUp)
					{
						case BL.Core.FeeManagement.Admin.FeeHeads.ShiftFeeHeadStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.FeeManagement.Admin.FeeHeads.ShiftFeeHeadStatuses.Success:
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.RefreshPage();
					break;
				case "Down":
					var feeHeadIDForDown = e.DecryptedCommandArgumentToInt();
					var movedDown = BL.Core.FeeManagement.Admin.FeeHeads.ShiftFeeHeadDown(feeHeadIDForDown, this.AdminIdentity.LoginSessionGuid);
					switch (movedDown)
					{
						case BL.Core.FeeManagement.Admin.FeeHeads.ShiftFeeHeadStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.FeeManagement.Admin.FeeHeads.ShiftFeeHeadStatuses.Success:
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.RefreshPage();
					break;
				case "Edit":
					e.Handled = true;
					this.DisplayFeeHeadsModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var feeHeadID = e.DecryptedCommandArgumentToInt();
					var status = BL.Core.FeeManagement.Admin.FeeHeads.DeleteFeeHead(feeHeadID, this.AdminIdentity.LoginSessionGuid);
					switch (status)
					{
						case BL.Core.FeeManagement.Admin.FeeHeads.DeleteFeeHeadStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.FeeManagement.Admin.FeeHeads.DeleteFeeHeadStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Fee Head");
							break;
						case BL.Core.FeeManagement.Admin.FeeHeads.DeleteFeeHeadStatuses.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Fee Head");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.RefreshPage();
					break;
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var feeHeadID = (int?)this.ViewState["FeeHeadID"];
			var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
			var feeHeadTypeEnum = this.ddlFeeHeadType.GetSelectedEnumValue<FeeHead.FeeHeadTypes>();
			var headName = this.tbHeadName.Text;
			var headShortName = this.tbHeadShortName.Text;
			var headAlias = this.tbHeadAlias.Text;
			var ruleTypeEnum = this.rblRuleType.GetSelectedEnumValue<FeeHead.RuleTypes>();
			var refundable = this.rblRefundable.SelectedValue.ToBoolean();
			var visible = this.rblVisible.SelectedValue.ToBoolean();
			var concession = this.rblConcession.SelectedValue.ToBoolean();
			var withholdingTaxApplicable = this.rblWithholdingTaxApplicable.SelectedValue.ToBoolean();

			if (feeHeadID == null)
			{
				var result = BL.Core.FeeManagement.Admin.FeeHeads.AddFeeHead(instituteID, feeHeadTypeEnum, headName, headShortName, headAlias, ruleTypeEnum, refundable, concession, visible, withholdingTaxApplicable, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.FeeManagement.Admin.FeeHeads.AddFeeHeadStatuses.HeadNameAlreadyExists:
						this.modalAlert.AddErrorAlert("Name already exists.");
						this.updatePanelAddFeeHead.Update();
						return;
					case BL.Core.FeeManagement.Admin.FeeHeads.AddFeeHeadStatuses.HeadShortNameAlreadyExists:
						this.modalAlert.AddErrorAlert("Short Name already exists.");
						this.updatePanelAddFeeHead.Update();
						return;
					case BL.Core.FeeManagement.Admin.FeeHeads.AddFeeHeadStatuses.HeadAliasAlreadyExists:
						this.modalAlert.AddErrorAlert("Alias already exists.");
						this.updatePanelAddFeeHead.Update();
						return;
					case BL.Core.FeeManagement.Admin.FeeHeads.AddFeeHeadStatuses.TutionFeeAlreadyExists:
						this.modalAlert.AddErrorAlert("Head for Tution Fee already exists.");
						this.updatePanelAddFeeHead.Update();
						break;
					case BL.Core.FeeManagement.Admin.FeeHeads.AddFeeHeadStatuses.ArrearsAlreadyExists:
						this.modalAlert.AddErrorAlert("Head for Arrears already exists.");
						this.updatePanelAddFeeHead.Update();
						break;
					case BL.Core.FeeManagement.Admin.FeeHeads.AddFeeHeadStatuses.WithholdingTaxAlreadyExists:
						this.modalAlert.AddErrorAlert("Head for Withholding Tax already exists.");
						this.updatePanelAddFeeHead.Update();
						break;
					case BL.Core.FeeManagement.Admin.FeeHeads.AddFeeHeadStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Fee Head");
						this.RefreshPage();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var result = BL.Core.FeeManagement.Admin.FeeHeads.UpdateFeeHead(feeHeadID.Value, headName, headShortName, headAlias, ruleTypeEnum, refundable, byte.MaxValue, concession, visible, withholdingTaxApplicable, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.FeeManagement.Admin.FeeHeads.UpdateFeeHeadStatus.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						break;
					case BL.Core.FeeManagement.Admin.FeeHeads.UpdateFeeHeadStatus.HeadNameAlreadyExists:
						this.modalAlert.AddErrorAlert("Name already exists.");
						this.updatePanelAddFeeHead.Update();
						return;
					case BL.Core.FeeManagement.Admin.FeeHeads.UpdateFeeHeadStatus.HeadShortNameAlreadyExists:
						this.modalAlert.AddErrorAlert("Short Name already exists.");
						this.updatePanelAddFeeHead.Update();
						return;
					case BL.Core.FeeManagement.Admin.FeeHeads.UpdateFeeHeadStatus.HeadAliasAlreadyExists:
						this.modalAlert.AddErrorAlert("Alias already exists.");
						this.updatePanelAddFeeHead.Update();
						return;
					case BL.Core.FeeManagement.Admin.FeeHeads.UpdateFeeHeadStatus.Success:
						this.AddSuccessMessageHasBeenUpdated("Fee Head");
						this.RefreshPage();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		private void DisplayFeeHeadsModal(int? feeHeadID)
		{
			this.ddlInstituteID.FillInstitutesForAdmin();
			this.ddlFeeHeadType.FillEnums<FeeHead.FeeHeadTypes>();
			this.rblRuleType.FillEnums<FeeHead.RuleTypes>();
			this.rblRefundable.FillYesNo();
			this.rblVisible.FillYesNo();
			this.rblConcession.FillYesNo();
			this.rblWithholdingTaxApplicable.FillYesNo();

			if (feeHeadID == null)
			{
				this.ddlInstituteID.ClearSelection();
				this.ddlInstituteID.Enabled = true;
				this.ddlFeeHeadType.ClearSelection();
				this.ddlFeeHeadType.Enabled = true;
				this.tbHeadName.Text = null;
				this.tbHeadShortName.Text = null;
				this.tbHeadAlias.Text = null;
				this.rblRuleType.SetEnumValue(FeeHead.RuleTypes.Credit);
				this.rblRefundable.SetSelectedValueNo();
				this.rblVisible.SetSelectedValueYes();
				this.rblConcession.SetSelectedValueNo();
				this.rblWithholdingTaxApplicable.SetSelectedValueNo();

				this.modalFeeHead.HeaderText = "Add Fee Head";
				this.btnSave.Text = "Add";
				this.ViewState["FeeHeadID"] = null;
				this.modalFeeHead.Show();
			}
			else
			{
				var feeHead = BL.Core.FeeManagement.Admin.FeeHeads.GetFeeHead(feeHeadID.Value, this.AdminIdentity.LoginSessionGuid);
				this.ddlInstituteID.SelectedValue = feeHead.InstituteID.ToString();
				this.ddlInstituteID.Enabled = false;
				this.ddlFeeHeadType.SetEnumValue(feeHead.FeeHeadTypeEnum);
				this.ddlFeeHeadType.Enabled = false;
				this.tbHeadName.Text = feeHead.HeadName;
				this.tbHeadShortName.Text = feeHead.HeadShortName;
				this.tbHeadAlias.Text = feeHead.HeadAlias;
				this.rblRuleType.SetEnumValue(feeHead.RuleTypeEnum);
				this.rblRefundable.SetSelectedValue(feeHead.Refundable);
				this.rblVisible.SetSelectedValue(feeHead.Visible);
				this.rblConcession.SetSelectedValue(feeHead.Concession);
				this.rblWithholdingTaxApplicable.SetSelectedValue(feeHead.WithholdingTaxApplicable);

				this.modalFeeHead.HeaderText = "Edit Fee Head";
				this.btnSave.Text = "Save";
				this.ViewState["FeeHeadID"] = feeHeadID;
				this.modalFeeHead.Show();
			}
		}
	}
}