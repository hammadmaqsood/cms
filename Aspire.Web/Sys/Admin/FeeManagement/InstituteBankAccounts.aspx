﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="InstituteBankAccounts.aspx.cs" Inherits="Aspire.Web.Sys.Admin.FeeManagement.InstituteBankAccounts" %>

<%@ Import Namespace="Aspire.Model.Entities" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireButton runat="server" ButtonType="Success" ID="btnAddBankAccount" CausesValidation="False" Text="Add" Glyphicon="plus" OnClick="btnAddBankAccount_OnClick" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlInstituteIDFilter" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Fee Type:" AssociatedControlID="ddlFeeTypeFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlFeeTypeFilter" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlFeeTypeFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Account Type:" AssociatedControlID="ddlAccountTypeFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlAccountTypeFilter" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlAccountTypeFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Active:" AssociatedControlID="ddlActiveFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlActiveFilter" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlActiveFilter_OnSelectedIndexChanged" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvBankAccounts" AllowSorting="True" AutoGenerateColumns="False" OnPageSizeChanging="gvBankAccounts_OnPageSizeChanging" OnPageIndexChanging="gvBankAccounts_OnPageIndexChanging" OnSorting="gvBankAccounts_OnSorting" OnRowCommand="gvBankAccounts_OnRowCommand">
		<columns>
			<asp:BoundField HeaderText="Institute" DataField="InstituteAlias" SortExpression="InstituteAlias" />
			<asp:BoundField HeaderText="Fee Type" DataField="FeeTypeFullName" SortExpression="FeeType" />
			<asp:BoundField HeaderText="Bank" DataField="BankAlias" SortExpression="BankAlias" />
			<asp:TemplateField HeaderText="Account No." SortExpression="AccountNo">
				<ItemTemplate>
					<%#: Eval(nameof(InstituteBankAccount.AccountNo)) %>
					<br />
					<%#: Eval(nameof(InstituteBankAccount.AccountNoForDisplay)) %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Account Title" SortExpression="AccountTitle">
				<ItemTemplate>
					<%#: Eval(nameof(InstituteBankAccount.AccountTitle)) %>
					<br />
					<%#: Eval(nameof(InstituteBankAccount.AccountTitleForDisplay)) %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Branch" SortExpression="BranchCode">
				<ItemTemplate>
					<%#: Eval(nameof(InstituteBankAccount.BranchCode)) %> - <%#: Eval(nameof(InstituteBankAccount.BranchName)) %> - <%#: Eval(nameof(InstituteBankAccount.BranchAddress)) %>
					<br />
					<%#: Eval(nameof(InstituteBankAccount.BranchForDisplay)) %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Account Type" DataField="AccountTypeFullName" SortExpression="AccountTypeFullName" />
			<asp:BoundField HeaderText="Is Active" DataField="ActiveYesNo" SortExpression="Active" />
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Edit" Glyphicon="edit" EncryptedCommandArgument='<%# Eval(nameof(InstituteBankAccount.InstituteBankAccountID))%>' />
					<aspire:AspireLinkButton runat="server" CausesValidation="False" CommandName="Delete" Glyphicon="remove" ConfirmMessage="Are you sure you want to delete this record?" EncryptedCommandArgument='<%# Eval(nameof(InstituteBankAccount.InstituteBankAccountID)) %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalBankAccount">
		<bodytemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateModalBankAccountPanel">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="modalAlertBankAccount" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstitute" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlInstitute" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlInstitute" ErrorMessage="This field is required." ValidationGroup="Required" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Fee Type:" AssociatedControlID="ddlFeeType" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlFeeType" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlFeeType" ErrorMessage="This field is required." ValidationGroup="Required" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Bank:" CssClass="col-md-3" AssociatedControlID="ddlBankID" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlBankID" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlBankID" ErrorMessage="This field is required." ValidationGroup="Required" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Account Title:" AssociatedControlID="tbAccountTitle" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbAccountTitle" TextTransform="UpperCase" MaxLength="200" />
								<aspire:AspireStringValidator runat="server" ControlToValidate="tbAccountTitle" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid title." ValidationGroup="Required" AllowNull="False" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Account Title (Display):" AssociatedControlID="tbAccountTitleForDisplay" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbAccountTitleForDisplay" MaxLength="500" />
								<aspire:AspireStringValidator runat="server" ControlToValidate="tbAccountTitleForDisplay" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid title." ValidationGroup="Required" AllowNull="True" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Account No.:" AssociatedControlID="tbAccountNo" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbAccountNo" MaxLength="100" />
								<aspire:AspireStringValidator runat="server" ControlToValidate="tbAccountNo" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid title." ValidationGroup="Required" AllowNull="False" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Account No. (Display):" AssociatedControlID="tbAccountNoForDisplay" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbAccountNoForDisplay" MaxLength="500" />
								<aspire:AspireStringValidator runat="server" RequiredErrorMessage="This field is required." ControlToValidate="tbAccountNoForDisplay" InvalidDataErrorMessage="Invalid title." ValidationGroup="Required" AllowNull="True" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Branch Code:" CssClass="col-md-3" AssociatedControlID="tbBranchCode" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbBranchCode" MaxLength="10" />
								<aspire:AspireStringValidator AllowNull="True" runat="server" RequiredErrorMessage="This field is required." ControlToValidate="tbBranchCode" ErrorMessage="This field is required." ValidationGroup="Required" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Branch Name:" CssClass="col-md-3" AssociatedControlID="tbBranchName" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbBranchName" MaxLength="500" />
								<aspire:AspireStringValidator AllowNull="True" runat="server" RequiredErrorMessage="This field is required." ControlToValidate="tbBranchName" ErrorMessage="This field is required." ValidationGroup="Required" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Branch Address:" CssClass="col-md-3" AssociatedControlID="tbBranchAddress" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbBranchAddress" MaxLength="500" />
								<aspire:AspireStringValidator AllowNull="True" runat="server" RequiredErrorMessage="This field is required." ControlToValidate="tbBranchAddress" ErrorMessage="This field is required." ValidationGroup="Required" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Branch (Display):" AssociatedControlID="tbBranchForDisplay" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbBranchForDisplay" MaxLength="500" />
								<aspire:AspireStringValidator AllowNull="True" RequiredErrorMessage="This field is required." runat="server" ControlToValidate="tbBranchForDisplay" ErrorMessage="This field is required." ValidationGroup="Required" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Account Type:" AssociatedControlID="ddlAccountType" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlAccountType" ValidationGroup="Required" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlAccountType" ErrorMessage="This field is required." ValidationGroup="Required" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Active:" AssociatedControlID="rblActive" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireRadioButtonList runat="server" ID="rblActive" RepeatDirection="Horizontal" RepeatLayout="Flow" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblActive" ErrorMessage="This field is required." ValidationGroup="Required" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</bodytemplate>
		<footertemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" OnClick="btnSave_OnClick" ValidationGroup="Required" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</footertemplate>
	</aspire:AspireModal>
</asp:Content>
