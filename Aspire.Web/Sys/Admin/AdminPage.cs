using Aspire.Model.Entities.Common;
using Aspire.Web.Common;

namespace Aspire.Web.Sys.Admin
{
	public abstract class AdminPage : AdminsPage
	{
		protected override void Authenticate()
		{
			base.Authenticate();
			if (!this.AdminIdentity.UserType.IsAdmin())
				throw new AspireAccessDeniedException(UserTypes.Admin, UserTypes.Admin);
		}

		public override AspireThemes AspireTheme => AspireDefaultThemes.Admin;
	}
}