﻿using Aspire.BL.Core.Exams.Admin;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Feedbacks
{
	[AspirePage("d9c96e70-b642-4d7b-8522-b607613b0317", UserTypes.Admins, "~/Sys/Admin/Feedbacks/Assignees.aspx", "Assignees", true, AspireModules.Feedbacks)]
	public partial class Assignees : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.Feedbacks.Module, new [] {UserGroupPermission.PermissionValues.Allowed }}
		};

		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_users);

		public override string PageTitle => "Assignees List";

		public static void Redirect(UserFeedback.FeedbackBoxTypes? feedbackBoxTypeEnum, int? instituteID, UserFeedbackMember.Statuses? statusEnum, SortDirection sortDirection, string sortExpression, int? pageIndex, int? pageSize)
		{
			Redirect<Assignees>("FeedbackBoxTypeEnum", feedbackBoxTypeEnum, "InstituteID", instituteID, "StatusEnum", statusEnum, "SortDirection", sortDirection, "SortExpression", sortExpression, "PageIndex", pageIndex, "PageSize", pageSize);
		}

		private void RefreshPage()
		{
			var feedbackBoxTypeEnum = this.ddlFeedbackBoxTypeFilter.GetSelectedNullableGuidEnum<UserFeedback.FeedbackBoxTypes>();
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToNullableInt();
			var statusEnum = this.ddlStatusFilter.GetSelectedNullableGuidEnum<UserFeedbackMember.Statuses>();
			var pageIndex = this.gvAssignees.PageIndex;
			var pageSize = this.gvAssignees.PageSize;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			Redirect(feedbackBoxTypeEnum, instituteID, statusEnum, sortDirection, sortExpression, pageIndex, pageSize);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("MemberName", SortDirection.Ascending);
				this.ddlFeedbackBoxTypeFilter.FillGuidEnums<UserFeedback.FeedbackBoxTypes>(CommonListItems.All).SetSelectedValueIfNotPostback("FeedbackBoxTypeEnum");
				this.ddlInstituteIDFilter.FillInstitutesForAdmin(CommonListItems.All).SetSelectedValueIfNotPostback("InstituteID");
				this.ddlStatusFilter.FillGuidEnums<UserFeedbackMember.Statuses>(CommonListItems.All).SetSelectedValueIfNotPostback("StatusEnum");
				this.ddlFeedbackBoxTypeFilter_SelectedIndexChanged(null, null);
			}
		}

		protected void btnAdd_Click(object sender, EventArgs e)
		{
			this.DisplayAssignee(null);
		}

		protected void ddlFeedbackBoxTypeFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlInstituteIDFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlInstituteIDFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			var feedbackBoxTypeEnum = this.ddlFeedbackBoxTypeFilter.GetSelectedNullableGuidEnum<UserFeedback.FeedbackBoxTypes>();
			switch (feedbackBoxTypeEnum)
			{
				case UserFeedback.FeedbackBoxTypes.Rector:
					this.ddlInstituteIDFilter.ClearSelection();
					this.ddlInstituteIDFilter.Enabled = false;
					break;
				case null:
				case UserFeedback.FeedbackBoxTypes.DirectorGeneralCampus:
					this.ddlInstituteIDFilter.Enabled = true;
					break;
				default:
					throw new NotImplementedEnumException(feedbackBoxTypeEnum);
			}
			this.ddlStatusFilter_SelectedIndexChanged(null, null);
		}

		protected void ddlStatusFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
				this.GetData(this.Request.GetParameterValue<int>("PageIndex") ?? 0);
			else
				this.GetData(0);
		}

		protected void gvAssignees_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvAssignees_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvAssignees_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvAssignees.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvAssignees_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayAssignee(e.DecryptedCommandArgumentToGuid());
					break;
				case "Delete":
					e.Handled = true;
					var result = BL.Core.Feedbacks.Admin.UserFeedbackMembers.DeleteUserFeedbackMember(e.DecryptedCommandArgumentToGuid(), this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case BL.Core.Feedbacks.Admin.UserFeedbackMembers.DeleteUserFeedbackMemberStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.RefreshPage();
							return;
						case BL.Core.Feedbacks.Admin.UserFeedbackMembers.DeleteUserFeedbackMemberStatuses.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Assignee");
							this.RefreshPage();
							return;
						case BL.Core.Feedbacks.Admin.UserFeedbackMembers.DeleteUserFeedbackMemberStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Assignee");
							this.RefreshPage();
							return;
						default:
							throw new NotImplementedEnumException(result);
					}
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;

			var feedbackBoxTypeEnum = this.ddlFeedbackBoxType.GetSelectedGuidEnum<UserFeedback.FeedbackBoxTypes>();
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			var designation = this.tbDesignation.Text;
			var memberName = this.tbMemberName.Text;
			var statusEnum = this.ddlStatus.GetSelectedGuidEnum<UserFeedbackMember.Statuses>();
			var userFeedbackMemberID = (Guid?)this.ViewState[nameof(UserFeedbackMember.UserFeedbackMemberID)];

			var result = userFeedbackMemberID == null
				? BL.Core.Feedbacks.Admin.UserFeedbackMembers.AddUserFeedbackMember(feedbackBoxTypeEnum, instituteID, designation, memberName, statusEnum, this.AdminIdentity.LoginSessionGuid)
				: BL.Core.Feedbacks.Admin.UserFeedbackMembers.UpdateUserFeedbackMember(userFeedbackMemberID.Value, designation, memberName, statusEnum, this.AdminIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.Feedbacks.Admin.UserFeedbackMembers.AddOrUpdateUserFeedbackMemberStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				case BL.Core.Feedbacks.Admin.UserFeedbackMembers.AddOrUpdateUserFeedbackMemberStatuses.Success:
					this.AddSuccessMessageHasBeenAdded("Assignee");
					this.RefreshPage();
					return;
				case BL.Core.Feedbacks.Admin.UserFeedbackMembers.AddOrUpdateUserFeedbackMemberStatuses.DesignationAlreadyExists:
					this.AddErrorAlert("Designation already exists.");
					this.updateModalAssignee.Update();
					return;
				default:
					throw new NotImplementedEnumException(result);
			}
		}

		protected void ddlFeedbackBoxType_SelectedIndexChanged(object sender, EventArgs e)
		{
			var feedbackBoxTypeEnum = this.ddlFeedbackBoxType.GetSelectedNullableGuidEnum<UserFeedback.FeedbackBoxTypes>();
			switch (feedbackBoxTypeEnum)
			{
				case UserFeedback.FeedbackBoxTypes.Rector:
					this.ddlInstituteID.ClearSelection();
					this.ddlInstituteID.Enabled = false;
					break;
				case null:
				case UserFeedback.FeedbackBoxTypes.DirectorGeneralCampus:
					this.ddlInstituteID.Enabled = true;
					break;
				default:
					throw new NotImplementedEnumException(feedbackBoxTypeEnum);
			}
		}

		private void GetData(int pageIndex)
		{
			var feedbackBoxTypeEnum = this.ddlFeedbackBoxTypeFilter.GetSelectedNullableGuidEnum<UserFeedback.FeedbackBoxTypes>();
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToNullableInt();
			var statusEnum = this.ddlStatusFilter.GetSelectedNullableGuidEnum<UserFeedbackMember.Statuses>();
			var pageSize = this.gvAssignees.PageSize;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var assignees = BL.Core.Feedbacks.Admin.UserFeedbackMembers.GetUserFeedbackMembersList(feedbackBoxTypeEnum, instituteID, statusEnum, pageIndex, pageSize, sortExpression, sortDirection, this.AdminIdentity.LoginSessionGuid);
			this.gvAssignees.DataBind(assignees.userFeedbackMembers, pageIndex, assignees.virtualItemCount, sortExpression, sortDirection);
		}

		private void DisplayAssignee(Guid? userFeedbackMemberID)
		{
			this.ddlFeedbackBoxType.FillGuidEnums<UserFeedback.FeedbackBoxTypes>(CommonListItems.Select);
			this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.Select);
			this.ddlStatus.FillGuidEnums<UserFeedbackMember.Statuses>(CommonListItems.Select);
			this.tbDesignation.Text = null;
			this.tbMemberName.Text = null;

			if (userFeedbackMemberID == null)
			{
				this.ddlFeedbackBoxType.Enabled = true;
				this.ddlStatus.Enabled = true;
				this.modalAssignee.HeaderText = "Add Assignee";
				this.ViewState[nameof(UserFeedbackMember.UserFeedbackMemberID)] = null;
			}
			else
			{
				UserFeedbackMember userFeedbackMember = BL.Core.Feedbacks.Admin.UserFeedbackMembers.GetUserFeedbackMember(userFeedbackMemberID.Value, this.AdminIdentity.LoginSessionGuid);
				if (userFeedbackMember == null)
				{
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				}
				this.ddlFeedbackBoxType.SetSelectedGuidEnum(userFeedbackMember.FeedbackBoxType.GetEnum<UserFeedback.FeedbackBoxTypes>());
				this.ddlFeedbackBoxType.Enabled = false;
				this.ddlFeedbackBoxType_SelectedIndexChanged(null, null);
				this.ddlInstituteID.SelectedValue = userFeedbackMember.InstituteID.ToString();
				this.ddlInstituteID.Enabled = false;
				this.ddlStatus.SetSelectedGuidEnum(userFeedbackMember.StatusEnum);
				this.tbMemberName.Text = userFeedbackMember.MemberName;
				this.tbDesignation.Text = userFeedbackMember.MemberDesignation;
				this.modalAssignee.HeaderText = "Update Assignee";
				this.ViewState[nameof(UserFeedbackMember.UserFeedbackMemberID)] = userFeedbackMember.UserFeedbackMemberID;
			}
			this.modalAssignee.Visible = true;
			this.modalAssignee.Show();
		}
	}
}