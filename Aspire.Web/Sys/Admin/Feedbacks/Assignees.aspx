﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Assignees.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Feedbacks.Assignees" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-3">
			<aspire:AspireLabel runat="server" Text="&nbsp;" AssociatedControlID="btnAdd" />
			<div>
				<aspire:AspireButton runat="server" ID="btnAdd" Text="Add" CausesValidation="False" Glyphicon="plus" ButtonType="Success" OnClick="btnAdd_Click" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Box Type:" AssociatedControlID="ddlFeedbackBoxTypeFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlFeedbackBoxTypeFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlFeedbackBoxTypeFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlInstituteIDFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteIDFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatusFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlStatusFilter" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlStatusFilter_SelectedIndexChanged" />
			</div>
		</div>
	</div>
	<aspire:AspireGridView runat="server" ItemType="Aspire.Model.Entities.UserFeedbackMember" ID="gvAssignees" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnSorting="gvAssignees_OnSorting" OnPageIndexChanging="gvAssignees_OnPageIndexChanging" OnPageSizeChanging="gvAssignees_OnPageSizeChanging" OnRowCommand="gvAssignees_OnRowCommand">
		<Columns>
			<asp:TemplateField HeaderText="Box Type" SortExpression="FeedbackBoxType">
				<ItemTemplate>
					<%#: Item.FeedbackBoxType.GetFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Institute" SortExpression="InstituteAlias">
				<ItemTemplate>
					<%#: (Item.Institute?.InstituteAlias).ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Designation" SortExpression="MemberDesignation">
				<ItemTemplate>
					<%#: Item.MemberDesignation %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Name" SortExpression="MemberName">
				<ItemTemplate>
					<%#: Item.MemberName %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Status" SortExpression="Status">
				<ItemTemplate>
					<%#: Item.Status.GetFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument='<%# Item.UserFeedbackMemberID %>' />
					<aspire:AspireLinkButton runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%# Item.UserFeedbackMemberID %>' ConfirmMessage="Are you sure you want to delete this record?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<aspire:AspireModal runat="server" ID="modalAssignee">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updateModalAssignee" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertModalAssignee" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Box Type:" AssociatedControlID="ddlFeedbackBoxType" />
						<aspire:AspireDropDownList runat="server" ID="ddlFeedbackBoxType" ValidationGroup="Assignee" AutoPostBack="True" OnSelectedIndexChanged="ddlFeedbackBoxType_SelectedIndexChanged" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlFeedbackBoxType" ErrorMessage="This field is required." ValidationGroup="Assignee" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
						<aspire:AspireDropDownList CausesValidation="False" runat="server" ID="ddlInstituteID" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Designation:" AssociatedControlID="tbDesignation" />
						<aspire:AspireTextBox runat="server" ID="tbDesignation" MaxLength="500" />
						<aspire:AspireStringValidator runat="server" ControlToValidate="tbDesignation" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Designation is not valid." AllowNull="False" ValidationGroup="Assignee" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbMemberName" />
						<aspire:AspireTextBox runat="server" ID="tbMemberName" MaxLength="100" />
						<aspire:AspireStringValidator runat="server" ControlToValidate="tbMemberName" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Name is not valid." AllowNull="False" ValidationGroup="Assignee" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatus" />
						<aspire:AspireDropDownList runat="server" ID="ddlStatus" RepeatDirection="Horizontal" RepeatLayout="Flow" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlStatus" ErrorMessage="This field is required." ValidationGroup="Assignee" />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" ValidationGroup="Assignee" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
