﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Admin.Administration
{
	[AspirePage("{036E8CE7-56A7-4DC7-98F2-7AEA59FB9A9B}", UserTypes.Admins, "~/Sys/Admin/Administration/Department.aspx", "Department", true, AspireModules.Administration)]
	public partial class Department : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.Administration.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_edit);
		public override string PageTitle => this.DepartmentID.HasValue ? "Edit Department" : "Add Department";

		public static string GetPageUrl(int departmentID)
		{
			return GetPageUrl<Department>().AttachQueryParam(nameof(DepartmentID), departmentID);
		}

		private int? DepartmentID => this.Request.GetParameterValue<int>(nameof(this.DepartmentID));

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.Select);
				if (this.ddlInstituteID.Items.Count == 1)
				{
					this.AddErrorAlert("No institute found.");
					Redirect<Departments>();
					return;
				}

				if (this.DepartmentID != null)
				{
					this.ddlInstituteID.Enabled = false;
					var department = BL.Core.Administration.Admin.Departments.GetDepartment(this.DepartmentID.Value, this.AdminIdentity.LoginSessionGuid);
					if (department == null)
					{
						this.AddNoRecordFoundAlert();
						Redirect<Departments>();
						return;
					}
					this.ddlInstituteID.SelectedValue = department.InstituteID.ToString();
					this.ddlInstituteID_SelectedIndexChanged(null, null);
					this.ddlFacultyID.SelectedValue = department.FacultyID.ToString();
					this.tbDepartmentName.Text = department.DepartmentName;
					this.tbDepartmentShortName.Text = department.DepartmentShortName;
					this.tbDepartmentAlias.Text = department.DepartmentAlias;
				}
				else
				{
					this.AdminIdentity.Demand(AdminPermissions.Administration.ManageDepartments, UserGroupPermission.PermissionValues.Allowed);
					this.ddlInstituteID_SelectedIndexChanged(null, null);
				}
			}
		}

		protected void ddlInstituteID_SelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			if (instituteID == null)
				this.ddlFacultyID.DataBind(CommonListItems.Select);
			else
				this.ddlFacultyID.FillFaculties(instituteID.Value, CommonListItems.Select);
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			this.AdminIdentity.Demand(AdminPermissions.Administration.ManageDepartments, UserGroupPermission.PermissionValues.Allowed);
			var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
			var facultyID = this.ddlFacultyID.SelectedValue.ToGuid();
			var departmentName = this.tbDepartmentName.Text;
			var departmentShortName = this.tbDepartmentShortName.Text;
			var departmentAlias = this.tbDepartmentAlias.Text.ToUpper();
			if (this.DepartmentID == null)
			{
				var result = BL.Core.Administration.Admin.Departments.AddDepartment(instituteID, facultyID, departmentName, departmentShortName, departmentAlias, this.AdminIdentity.LoginSessionGuid);
				switch (result.Status)
				{
					case BL.Core.Administration.Admin.Departments.AddDepartmentResult.Statuses.NameAlreadyExists:
						this.AddErrorAlert("Department Name already exist.");
						break;
					case BL.Core.Administration.Admin.Departments.AddDepartmentResult.Statuses.ShortNameAlreadyExists:
						this.AddErrorAlert("Department Short Name already exist.");
						break;
					case BL.Core.Administration.Admin.Departments.AddDepartmentResult.Statuses.AliasAlreadyExists:
						this.AddErrorAlert("Department Alias already exist.");
						break;
					case BL.Core.Administration.Admin.Departments.AddDepartmentResult.Statuses.Success:
						this.AddSuccessMessageHasBeenAdded("Department");
						Redirect<Departments>();
						break;
					default:
						throw new NotImplementedEnumException(result.Status);
				}
			}
			else
			{
				var status = BL.Core.Administration.Admin.Departments.UpdateDepartment(this.DepartmentID.Value, facultyID, departmentName, departmentShortName, departmentAlias, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.Administration.Admin.Departments.UpdateDepartmentStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<Departments>();
						return;
					case BL.Core.Administration.Admin.Departments.UpdateDepartmentStatuses.NameAlreadyExists:
						this.AddErrorAlert("Department Name already exist.");
						break;
					case BL.Core.Administration.Admin.Departments.UpdateDepartmentStatuses.ShortNameAlreadyExists:
						this.AddErrorAlert("Department Short Name already exist.");
						break;
					case BL.Core.Administration.Admin.Departments.UpdateDepartmentStatuses.AliasAlreadyExists:
						this.AddErrorAlert("Department Alias already exist.");
						break;
					case BL.Core.Administration.Admin.Departments.UpdateDepartmentStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Department");
						Redirect<Departments>();
						return;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
		}
	}
}