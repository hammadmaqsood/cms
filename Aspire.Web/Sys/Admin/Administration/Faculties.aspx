﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Faculties.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Administration.Faculties" %>

<%@ Import Namespace="Aspire.Web.Sys.Admin.Administration" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-8">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="&nbsp;" AssociatedControlID="hlAddFaculty" />
				<div>
					<aspire:AspireHyperLinkButton ButtonType="Success" runat="server" Glyphicon="plus" Text="Add Faculty" ID="hlAddFaculty" NavigateUrl="Faculty.aspx" />
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstitute" />
				<aspire:AspireDropDownList runat="server" ID="ddlInstitute" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="ddlInstitute_OnSelectedIndexChanged" />
			</div>
		</div>
	</div>
	<div class="table-responsive">
		<aspire:AspireGridView runat="server" ID="gvFaculties" ItemType="Aspire.BL.Core.Administration.Admin.Faculties.Faculty" AutoGenerateColumns="False" AllowSorting="True" OnPageIndexChanging="gvFaculties_OnPageIndexChanging" OnPageSizeChanging="gvFaculties_OnPageSizeChanging" OnSorting="gvFaculties_OnSorting" OnRowCommand="gvFaculties_RowCommand">
			<Columns>
				<asp:BoundField HeaderText="Institute" DataField="InstituteAlias" SortExpression="InstituteAlias" />
				<asp:BoundField HeaderText="Name" DataField="FacultyName" SortExpression="FacultyName" />
				<asp:BoundField HeaderText="Short Name" DataField="FacultyShortName" SortExpression="FacultyShortName" />
				<asp:BoundField HeaderText="Alias" DataField="FacultyAlias" SortExpression="FacultyAlias" />
				<asp:TemplateField HeaderText="Actions">
					<ItemTemplate>
						<aspire:AspireHyperLinkButton ButtonType="OnlyIcon" Glyphicon="edit" runat="server" NavigateUrl="<%# Faculty.GetPageUrl(Item.FacultyID) %>" />
						<aspire:AspireLinkButton runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument="<%# Item.FacultyID %>" ConfirmMessage="Are you sure, you want to delete this record?" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
	</div>
</asp:Content>
