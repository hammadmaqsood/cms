﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Administration
{
	[AspirePage("A6F42E4D-2F6C-4701-BC57-FBD670FB37AB", UserTypes.Admins, "~/Sys/Admin/Administration/Semesters.aspx", "Manage Semesters", true, AspireModules.Administration)]
	public partial class Semesters : MasterAdminPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_bicycle);
		public override string PageTitle => "Semesters";

		private void RefreshPage()
		{
			Redirect<Semesters>();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var semesters = BL.Core.Administration.Admin.Semesters.GetSemesters(this.AdminIdentity.LoginSessionGuid);
				var years = Enumerable.Range(2000, DateTime.Today.Year - 2000 + 2)
					.SelectMany(year => Enum.GetValues(typeof(SemesterTypes)).Cast<SemesterTypes>().Select(s => Semester.GetSemesterID((short)year, s)))
					.Where(semesterID => semesters.All(s => s.SemesterID != semesterID))
					.Select(Semester.GetYear)
					.Distinct()
					.OrderBy(s => s)
					.ToList();
				this.gvSemesters.DataBind(semesters);
				this.ddlSemesterType.FillEnums<SemesterTypes>();
				this.ddlYear.DataBind(years.Select(y => y.ToString()));
			}
		}

		protected void gvSemesters_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					{
						e.Handled = true;
						var semesterID = e.DecryptedCommandArgument().ToShort();
						var result = BL.Core.Administration.Admin.Semesters.DeleteSemester(semesterID, this.AdminIdentity.LoginSessionGuid);
						switch (result)
						{
							case BL.Core.Administration.Admin.Semesters.DeleteSemesterStatuses.NoRecordFound:
								this.AddNoRecordFoundAlert();
								this.RefreshPage();
								return;
							case BL.Core.Administration.Admin.Semesters.DeleteSemesterStatuses.ChildRecordExists:
								this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Semester");
								this.RefreshPage();
								return;
							case BL.Core.Administration.Admin.Semesters.DeleteSemesterStatuses.Success:
								this.AddSuccessMessageHasBeenDeleted("Semester");
								this.RefreshPage();
								return;
							default:
								throw new NotImplementedEnumException(result);
						}
					}
				case "Edit":
					{
						e.Handled = true;
						var args = e.DecryptedCommandArgument().Split(':');
						var semesterID = args[0].ToShort();
						var instituteID = args[1].ToInt();
						this.modalInstituteSemester.Visible = true;
						this.modalInstituteSemester.Show();
						this.ddlInstituteIDUpdate.FillInstitutesForAdmin();
						this.ddlInstituteIDUpdate.SelectedValue = instituteID.ToString();
						this.ddlInstituteIDUpdate.Enabled = false;
						this.ddlSemesterIDUpdate.FillSemesters();
						this.ddlSemesterIDUpdate.SelectedValue = semesterID.ToString();
						this.ddlSemesterIDUpdate.Enabled = false;
						var instituteSemester = BL.Core.Administration.Admin.Semesters.GetInstituteSemester(semesterID, instituteID, this.AdminIdentity.LoginSessionGuid);
						this.dtpClassesEndDateUpdate.SelectedDate = instituteSemester?.ClassesStartDate;
						this.dtpClassesEndDateUpdate.SelectedDate = instituteSemester?.ClassesEndDate;
						return;
					}
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var semesterTypeEnum = this.ddlSemesterType.GetSelectedEnumValue<SemesterTypes>();
			var year = this.ddlYear.SelectedValue.ToShort();
			var semesterID = Semester.GetSemesterID(year, semesterTypeEnum);
			var result = BL.Core.Administration.Admin.Semesters.AddSemester(semesterID, this.AdminIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.Administration.Admin.Semesters.AddSemesterStatuses.AlreadyExists:
					this.AddErrorAlert("Semester is already exists");
					break;
				case BL.Core.Administration.Admin.Semesters.AddSemesterStatuses.Success:
					this.AddSuccessMessageHasBeenAdded("Semester");
					break;
				default:
					throw new NotImplementedEnumException(result);
			}
			this.RefreshPage();
		}

		protected void btnUpdate_Click(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteIDUpdate.SelectedValue.ToInt();
			var semesterID = this.ddlSemesterIDUpdate.SelectedValue.ToShort();
			var classesStartDate = this.dtpClassesStartDateUpdate.SelectedDate?.Date ?? throw new InvalidOperationException();
			var classesEndDate = this.dtpClassesEndDateUpdate.SelectedDate?.Date;
			var result = BL.Core.Administration.Admin.Semesters.AddOrUpdateInstituteSemester(semesterID, instituteID, classesStartDate, classesEndDate, this.AdminIdentity.LoginSessionGuid);
			switch (result)
			{
				case BL.Core.Administration.Admin.Semesters.AddOrUpdateInstituteSemesterStatuses.InvalidDates:
					this.alertSemester.AddErrorAlert("Dates range are not valid.");
					this.updatePanelSemester.Update();
					return;
				case BL.Core.Administration.Admin.Semesters.AddOrUpdateInstituteSemesterStatuses.Success:
					this.AddSuccessAlert("Semester dates have been updated.");
					this.RefreshPage();
					return;
				default:
					throw new NotImplementedEnumException(result);
			}
		}
	}
}