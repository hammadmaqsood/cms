﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Program.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Administration.Program" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
		<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" CausesValidation="False" ValidationGroup="SaveProgram" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteID_OnSelectedIndexChanged" />
		<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlInstituteID" ErrorMessage="This field is required." ValidationGroup="SaveProgram" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
		<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" CausesValidation="False" ValidationGroup="SaveProgram" />
		<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlDepartmentID" ErrorMessage="This field is required." ValidationGroup="SaveProgram" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbProgramName" />
		<aspire:AspireTextBox runat="server" MaxLength="1000" ID="tbProgramName" />
		<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbProgramName" RequiredErrorMessage="This field is required." ValidationGroup="SaveProgram" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Short Name:" AssociatedControlID="tbProgramShortName" />
		<aspire:AspireTextBox runat="server" MaxLength="500" ID="tbProgramShortName" />
		<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbProgramShortName" RequiredErrorMessage="This field is required." ValidationGroup="SaveProgram" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Alias:" AssociatedControlID="tbProgramAlias" />
		<aspire:AspireTextBox runat="server" MaxLength="250" ID="tbProgramAlias" TextTransform="UpperCase" />
		<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbProgramAlias" RequiredErrorMessage="This field is required." ValidationGroup="SaveProgram" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Duration:" AssociatedControlID="ddlDuration" />
		<aspire:AspireDropDownList runat="server" ID="ddlDuration" ValidationGroup="SaveProgram" />
		<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlDuration" ErrorMessage="This field is required." ValidationGroup="SaveProgram" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Code:" AssociatedControlID="tbProgramCode" />
		<aspire:AspireTextBox runat="server" MaxLength="2" ID="tbProgramCode" />
		<aspire:AspireStringValidator runat="server" AllowNull="true" ControlToValidate="tbProgramCode" RequiredErrorMessage="This field is required." ValidationGroup="SaveProgram" ValidationExpression="Custom" CustomValidationExpression="\d{2}" InvalidDataErrorMessage="Institute Code is not valid." />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Degree Level:" AssociatedControlID="ddlDegreeLevel" />
		<aspire:AspireDropDownList runat="server" ID="ddlDegreeLevel" CausesValidation="True" ValidationGroup="SaveProgram" />
		<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlDegreeLevel" ErrorMessage="This field is required." ValidationGroup="SaveProgram" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlProgramType" />
		<aspire:AspireDropDownList runat="server" ID="ddlProgramType" CausesValidation="True" ValidationGroup="SaveProgram" />
		<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlProgramType" ErrorMessage="This field is required." ValidationGroup="SaveProgram" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Initiated Date:" AssociatedControlID="dtpInitiatedDate" />
		<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpInitiatedDate" DisplayMode="ShortDate" ValidationGroup="SaveProgram" />
		<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dtpInitiatedDate" AllowNull="True" InvalidRangeErrorMessage="Invalid Date." InvalidDataErrorMessage="Invalid Date." RequiredErrorMessage="This field is required." ValidationGroup="SaveProgram" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Default Exam Marks Marks Policy:" AssociatedControlID="ddlDefaultExamMarksPolicyID" />
		<aspire:AspireDropDownList runat="server" ID="ddlDefaultExamMarksPolicyID" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Default Exam Remarks Policy:" AssociatedControlID="ddlDefaultExamRemarksPolicyID" />
		<aspire:AspireDropDownList runat="server" ID="ddlDefaultExamRemarksPolicyID" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Maximum Courses Allowed For Registration Per Semester:" AssociatedControlID="tbMaxAllowedCoursesPerSemester" />
		<aspire:AspireTextBox runat="server" MaxLength="2" ID="tbMaxAllowedCoursesPerSemester" />
		<aspire:AspireByteValidator runat="server" AllowNull="false" MinValue="1" ControlToValidate="tbMaxAllowedCoursesPerSemester" RequiredErrorMessage="This field is required." ValidationGroup="SaveProgram" InvalidNumberErrorMessage="Number is not valid." InvalidRangeErrorMessage="Number is not valid." />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Maximum Credit Hours Allowed For Registration Per Semester:" AssociatedControlID="tbMaxAllowedCreditHoursPerSemester" />
		<aspire:AspireTextBox runat="server" MaxLength="2" ID="tbMaxAllowedCreditHoursPerSemester" />
		<aspire:AspireByteValidator runat="server" AllowNull="false" MinValue="1" ControlToValidate="tbMaxAllowedCreditHoursPerSemester" RequiredErrorMessage="This field is required." ValidationGroup="SaveProgram" InvalidNumberErrorMessage="Number is not valid." InvalidRangeErrorMessage="Number is not valid." />
	</div>
	<div class="form-group text-center">
		<aspire:AspireButton runat="server" ID="btnSave" ButtonType="Primary" ValidationGroup="SaveProgram" Text="Save" OnClick="btnSave_OnClick" />
		<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="Programs.aspx" />
	</div>
</asp:Content>
