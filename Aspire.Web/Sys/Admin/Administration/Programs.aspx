﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Programs.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Administration.Programs" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="&nbsp;" />
				<div>
					<aspire:AspireHyperLinkButton ButtonType="Success" runat="server" Glyphicon="plus" Text="Add Program" NavigateUrl="Program.aspx" />
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
				<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteID_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Faculty:" AssociatedControlID="ddlFacultyID" />
				<aspire:AspireDropDownList runat="server" ID="ddlFacultyID" ValidationGroup="Filters" OnSelectedIndexChanged="ddlFacultyID_OnSelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
				<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" ValidationGroup="Filters" OnSelectedIndexChanged="ddlDepartmentID_SelectedIndexChanged" AutoPostBack="True" />
			</div>
		</div>
	</div>
	<aspire:AspireGridView runat="server" ItemType="Aspire.Model.Entities.Program" ID="gvProgram" AutoGenerateColumns="False" AllowSorting="True" PageSize="50" OnPageIndexChanging="gvProgram_PageIndexChanging" OnPageSizeChanging="gvProgram_PageSizeChanging" OnSorting="gvProgram_Sorting">
		<Columns>
			<asp:BoundField HeaderText="Institute" DataField="Institute.InstituteAlias" SortExpression="InstituteAlias" />
			<asp:BoundField HeaderText="Faculty" DataField="Department.Faculty.FacultyAlias" SortExpression="FacultyAlias" />
			<asp:BoundField HeaderText="Department" DataField="Department.DepartmentAlias" SortExpression="DepartmentAlias" />
			<asp:BoundField HeaderText="Name" DataField="ProgramName" SortExpression="ProgramName" />
			<asp:BoundField HeaderText="Short Name" DataField="ProgramShortName" SortExpression="ProgramShortName" />
			<asp:BoundField HeaderText="Alias" DataField="ProgramAlias" SortExpression="ProgramAlias" />
			<asp:BoundField HeaderText="Duration" DataField="DurationFullName" SortExpression="Duration" />
			<asp:TemplateField HeaderText="Code" SortExpression="ProgramCode">
				<ItemTemplate><%#: Item.ProgramCode.ToNAIfNullOrEmpty() %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Type" DataField="ProgramTypeFullName" SortExpression="ProgramType" />
			<asp:BoundField HeaderText="Degree Level" DataField="DegreeLevelFullName" SortExpression="DegreeLevel" />
			<asp:BoundField HeaderText="Maxumum Courses Allowed for Registration Per Semester" DataField="MaxAllowedCoursesPerSemester" SortExpression="MaxAllowedCoursesPerSemester" />
			<asp:BoundField HeaderText="Maximum Credit Hours Allowed for Registration Per Semester" DataField="MaxAllowedCreditHoursPerSemester" SortExpression="MaxAllowedCreditHoursPerSemester" />
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireHyperLinkButton ToolTip="Edit" ButtonType="OnlyIcon" Glyphicon="edit" runat="server" NavigateUrl="<%# Aspire.Web.Sys.Admin.Administration.Program.GetPageUrl(Item.ProgramID) %>" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
