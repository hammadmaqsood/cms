﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Institutes.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Administration.Institutes" %>

<%@ Import Namespace="Aspire.Web.Sys.Admin.Administration" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-group">
		<aspire:AspireHyperLinkButton ButtonType="Success" ID="hlbtnAddInstitute" runat="server" Glyphicon="plus" Text="Add Institute" NavigateUrl="Institute.aspx" />
	</div>
	<aspire:AspireGridView runat="server" ItemType="Aspire.Model.Entities.Institute" ID="gvInstitutes" AutoGenerateColumns="False" AllowSorting="True" OnPageIndexChanging="gvInstitutes_OnPageIndexChanging" OnPageSizeChanging="gvInstitutes_OnPageSizeChanging" OnSorting="gvInstitutes_OnSorting">
		<Columns>
			<asp:BoundField HeaderText="Code" DataField="InstituteCode" SortExpression="InstituteCode" />
			<asp:BoundField HeaderText="Name" DataField="InstituteName" SortExpression="InstituteName" />
			<asp:BoundField HeaderText="Short Name" DataField="InstituteShortName" SortExpression="InstituteShortName" />
			<asp:BoundField HeaderText="Alias" DataField="InstituteAlias" SortExpression="InstituteAlias" />
			<asp:HyperLinkField HeaderText="Website" DataTextField="Website" DataNavigateUrlFields="Website" DataTextFormatString="{0}" DataNavigateUrlFormatString="{0}" Target="_blank" SortExpression="Website" />
			<asp:BoundField HeaderText="Address" DataField="MailAddress" SortExpression="MailAddress" />
			<asp:BoundField HeaderText="Phone" DataField="Phone" SortExpression="Phone" />
			<asp:BoundField HeaderText="Fax" DataField="Fax" SortExpression="Fax" />
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireHyperLinkButton ButtonType="OnlyIcon" Glyphicon="edit" runat="server" NavigateUrl="<%# Institute.GetPageUrl(Item.InstituteID) %>" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
