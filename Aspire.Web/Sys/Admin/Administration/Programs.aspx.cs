﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Administration
{
	[AspirePage("{0E4DEF39-9D58-43D5-9673-8A3199FCBFB2}", UserTypes.Admins, "~/Sys/Admin/Administration/Programs.aspx", "Manage Programs", true, AspireModules.Administration)]
	public partial class Programs : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.Administration.Module, new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_edit);
		public override string PageTitle => "Manage Programs";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(nameof(Model.Entities.Program.Institute.InstituteAlias), SortDirection.Ascending);
				this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.All);
				if (this.ddlInstituteID.Items.Count == 1)
				{
					this.AddWarningAlert("No institute found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlInstituteID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlInstituteID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			if (instituteID == null)
				this.ddlFacultyID.DataBind(CommonListItems.All);
			else
				this.ddlFacultyID.FillFaculties(instituteID.Value, CommonListItems.All);
			this.ddlFacultyID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlFacultyID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			if (instituteID == null)
				this.ddlDepartmentID.DataBind(CommonListItems.All);
			else
			{
				var facultyID = this.ddlFacultyID.SelectedValue.ToNullableGuid();
				this.ddlDepartmentID.FillDepartments(instituteID.Value, facultyID, CommonListItems.All);
			}

			this.ddlDepartmentID_SelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentID_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void gvProgram_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvProgram_Sorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(this.gvProgram.PageIndex);
		}

		protected void gvProgram_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvProgram.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableByte();
			var facultyID = this.ddlFacultyID.SelectedValue.ToNullableGuid();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableShort();
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var programs = BL.Core.Administration.Admin.Programs.GetPrograms(instituteID, facultyID, departmentID, sortExpression, sortDirection, pageIndex, this.gvProgram.PageSize, out var virtualItemCount, this.AdminIdentity.LoginSessionGuid);
			this.gvProgram.DataBind(programs, pageIndex, virtualItemCount, sortExpression, sortDirection);
		}
	}
}