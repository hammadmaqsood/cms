﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Institute.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Administration.Institute" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Institute ID:" AssociatedControlID="tbInstituteID" />
		<aspire:AspireTextBox runat="server" MaxLength="2" ID="tbInstituteID" />
		<aspire:AspireInt32Validator runat="server" AllowNull="False" ControlToValidate="tbInstituteID" RequiredErrorMessage="This field is required." ValidationGroup="Institute" InvalidNumberErrorMessage="Invalid number." InvalidRangeErrorMessage="Invalid range." />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbInstituteName" />
		<aspire:AspireTextBox runat="server" MaxLength="1000" ID="tbInstituteName" />
		<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbInstituteName" RequiredErrorMessage="This field is required." ValidationGroup="Institute" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Short Name:" AssociatedControlID="tbShortInstituteName" />
		<aspire:AspireTextBox runat="server" MaxLength="500" ID="tbShortInstituteName" />
		<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbShortInstituteName" RequiredErrorMessage="This field is required." ValidationGroup="Institute" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Alias:" AssociatedControlID="tbInstituteAlias" />
		<aspire:AspireTextBox runat="server" MaxLength="250" ID="tbInstituteAlias" TextTransform="UpperCase" />
		<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbInstituteAlias" RequiredErrorMessage="This field is required." ValidationGroup="Institute" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Code:" AssociatedControlID="tbInstituteCode" />
		<aspire:AspireTextBox runat="server" MaxLength="2" ID="tbInstituteCode" />
		<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbInstituteCode" RequiredErrorMessage="This field is required." ValidationGroup="Institute" ValidationExpression="Custom" CustomValidationExpression="\d{2}" InvalidDataErrorMessage="Invalid Institute code format." />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Website:" AssociatedControlID="tbwebiste" />
		<aspire:AspireTextBox runat="server" MaxLength="200" ID="tbwebiste" />
		<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbwebiste" RequiredErrorMessage="This field is required." ValidationGroup="Institute" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Address:" AssociatedControlID="tbAddress" />
		<aspire:AspireTextBox runat="server" MaxLength="500" ID="tbAddress" />
		<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbAddress" RequiredErrorMessage="This field is required." ValidationGroup="Institute" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Phone:" AssociatedControlID="tbPhone" />
		<aspire:AspireTextBox runat="server" MaxLength="50" ID="tbPhone" />
		<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbPhone" RequiredErrorMessage="This field is required." ValidationGroup="Institute" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Fax:" AssociatedControlID="tbFax" />
		<aspire:AspireTextBox runat="server" MaxLength="50" ID="tbFax" />
		<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbFax" RequiredErrorMessage="This field is required." ValidationGroup="Institute" />
	</div>
	<div class="form-group text-center">
		<aspire:AspireButton runat="server" ID="btnSave" ButtonType="Primary" ValidationGroup="Institute" Text="Save" OnClick="btnSave_OnClick" />
		<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="Institutes.aspx" />
	</div>
</asp:Content>
