﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Admin.Administration
{
	[AspirePage("{0C473A87-A532-4CDC-98D5-92D64E40D79B}", UserTypes.Admins, "~/Sys/Admin/Administration/Program.aspx", "Program", true, AspireModules.Administration)]
	public partial class Program : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.Administration.Module, new[] {UserGroupPermission.PermissionValues.Allowed}},
		};

		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_edit);
		public override string PageTitle => this.ProgramID.HasValue ? "Edit Program" : "Add Program";
		private int? ProgramID => this.Request.GetParameterValue<short>(nameof(this.ProgramID));

		public static string GetPageUrl(int programID)
		{
			return GetPageUrl<Program>().AttachQueryParam("ProgramID", programID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.Select);
				if (this.ddlInstituteID.Items.Count == 1)
				{
					this.AddWarningAlert("No institute found.");
					Redirect<Dashboard>();
				}
				this.ddlDegreeLevel.FillDegreeLevels(CommonListItems.Select);
				this.ddlProgramType.FillProgramTypes(CommonListItems.Select);
				this.ddlDuration.FillProgramDurations(CommonListItems.Select);

				if (this.ProgramID != null)
				{
					var program = BL.Core.Administration.Admin.Programs.GetProgram(this.ProgramID.Value, this.AdminIdentity.LoginSessionGuid);
					if (program == null)
					{
						this.AddNoRecordFoundAlert();
						Redirect<Programs>();
						return;
					}
					this.ddlInstituteID.SelectedValue = program.InstituteID.ToString();
					this.ddlInstituteID.Enabled = false;
					this.ddlInstituteID_OnSelectedIndexChanged(null, null);
					this.ddlDepartmentID.SelectedValue = program.DepartmentID.ToString();
					this.tbProgramName.Text = program.ProgramName;
					this.tbProgramShortName.Text = program.ProgramShortName;
					this.tbProgramAlias.Text = program.ProgramAlias;
					this.ddlDuration.SetEnumValue(program.DurationEnum);
					this.tbProgramCode.Text = program.ProgramCode;
					this.ddlDegreeLevel.SelectedValue = program.DegreeLevel.ToString();
					this.ddlProgramType.SelectedValue = program.ProgramType.ToString();
					this.dtpInitiatedDate.SelectedDate = program.InitiatedDate;
					this.ddlDefaultExamMarksPolicyID.SelectedValue = program.DefaultExamMarksPolicyID.ToString();
					this.ddlDefaultExamRemarksPolicyID.SelectedValue = program.DefaultExamRemarksPolicyID.ToString();
					this.tbMaxAllowedCreditHoursPerSemester.Text = program.MaxAllowedCreditHoursPerSemester.ToString();
					this.tbMaxAllowedCoursesPerSemester.Text = program.MaxAllowedCoursesPerSemester.ToString();
				}
				else
				{
					this.AdminIdentity.Demand(AdminPermissions.Administration.ManagePrograms, UserGroupPermission.PermissionValues.Allowed);
					this.ddlInstituteID_OnSelectedIndexChanged(null, null);
				}
			}
		}

		protected void ddlInstituteID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			if (instituteID == null)
			{
				this.ddlDepartmentID.DataBind(CommonListItems.Select);
				this.ddlDefaultExamMarksPolicyID.DataBind(CommonListItems.Select);
				this.ddlDefaultExamRemarksPolicyID.DataBind(CommonListItems.Select);
			}
			else
			{
				this.ddlDepartmentID.FillDepartments(instituteID.Value, null, CommonListItems.Select);
				var (examMarksPolicies, examRemarksPolicies) = BL.Core.Administration.Admin.Programs.GetPolicies(instituteID.Value, this.AdminIdentity.LoginSessionGuid);
				this.ddlDefaultExamMarksPolicyID.DataBind(examMarksPolicies, CommonListItems.Select);
				this.ddlDefaultExamRemarksPolicyID.DataBind(examRemarksPolicies, CommonListItems.Select);
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			this.AdminIdentity.Demand(AdminPermissions.Administration.ManagePrograms, UserGroupPermission.PermissionValues.Allowed);
			var instituteID = this.ddlInstituteID.SelectedValue.ToByte();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableShort();
			var programName = this.tbProgramName.Text;
			var programShortName = this.tbProgramShortName.Text;
			var programAlias = this.tbProgramAlias.Text;
			var durationEnum = this.ddlDuration.GetSelectedEnumValue<ProgramDurations>();
			var programCode = this.tbProgramCode.Text;
			var degreeLevelEnum = this.ddlDegreeLevel.GetSelectedEnumValue<DegreeLevels>();
			var programTypeEnum = this.ddlProgramType.GetSelectedEnumValue<ProgramTypes>();
			var initiatedDate = this.dtpInitiatedDate.SelectedDate;
			var defaultExamMarksPolicyID = this.ddlDefaultExamMarksPolicyID.SelectedValue.ToNullableInt();
			var defaultExamRemarksPolicyID = this.ddlDefaultExamRemarksPolicyID.SelectedValue.ToNullableInt();
			var maxAllowedCreditHoursPerSemester = this.tbMaxAllowedCreditHoursPerSemester.Text.ToByte();
			var maxAllowedCoursesPerSemester = this.tbMaxAllowedCoursesPerSemester.Text.ToByte();

			if (this.ProgramID == null)
			{
				var result = BL.Core.Administration.Admin.Programs.AddProgram(instituteID, departmentID, programName, programShortName, programAlias, durationEnum, programCode, degreeLevelEnum, programTypeEnum, initiatedDate, defaultExamMarksPolicyID, defaultExamRemarksPolicyID, maxAllowedCoursesPerSemester, maxAllowedCreditHoursPerSemester, this.AdminIdentity.LoginSessionGuid);
				switch (result.Status)
				{
					case BL.Core.Administration.Admin.Programs.AddProgramResult.Statuses.ShortNameAlreadyExists:
						this.AddErrorAlert("Program Short Name already exists.");
						break;
					case BL.Core.Administration.Admin.Programs.AddProgramResult.Statuses.AliasAlreadyExists:
						this.AddErrorAlert("Program Alias already exists.");
						break;
					case BL.Core.Administration.Admin.Programs.AddProgramResult.Statuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<Programs>();
						return;
					case BL.Core.Administration.Admin.Programs.AddProgramResult.Statuses.Success:
						this.AddSuccessMessageHasBeenAdded("Program");
						Redirect<Programs>();
						return;
					default:
						throw new NotImplementedEnumException(result.Status);
				}
			}
			else
			{
				var status = BL.Core.Administration.Admin.Programs.UpdateProgram(this.ProgramID.Value, departmentID, programName, programShortName, programAlias, durationEnum, programCode, degreeLevelEnum, programTypeEnum, initiatedDate, defaultExamMarksPolicyID, defaultExamRemarksPolicyID, maxAllowedCoursesPerSemester, maxAllowedCreditHoursPerSemester, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.Administration.Admin.Programs.UpdateProgramStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<Programs>();
						break;
					case BL.Core.Administration.Admin.Programs.UpdateProgramStatuses.ShortNameAlreadyExists:
						this.AddErrorAlert("Program Short Name already exists.");
						break;
					case BL.Core.Administration.Admin.Programs.UpdateProgramStatuses.AliasAlreadyExists:
						this.AddErrorAlert("Program Alias already exists.");
						break;
					case BL.Core.Administration.Admin.Programs.UpdateProgramStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Program");
						Redirect<Programs>();
						return;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
		}
	}
}