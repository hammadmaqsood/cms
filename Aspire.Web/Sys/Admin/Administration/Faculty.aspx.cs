﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Admin.Administration
{
	[AspirePage("{4B1B8283-2729-4035-A751-75183B09B884}", UserTypes.Admins, "~/Sys/Admin/Administration/Faculty.aspx", "Faculty", true, AspireModules.Administration)]
	public partial class Faculty : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.Administration.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_edit);
		public override string PageTitle => this.FacultyID.HasValue ? "Edit Faculty" : "Add Faculty";

		public static string GetPageUrl(Guid facultyID)
		{
			return GetPageUrl<Faculty>().AttachQueryParam(nameof(FacultyID), facultyID);
		}

		private Guid? FacultyID => this.Request.GetParameterValue<Guid>(nameof(this.FacultyID));

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.Select);
				if (this.ddlInstituteID.Items.Count == 1)
				{
					this.AddErrorAlert("No institute found.");
					Redirect<Departments>();
					return;
				}

				if (this.FacultyID != null)
				{
					this.ddlInstituteID.Enabled = false;
					var faculty = BL.Core.Administration.Admin.Faculties.GetFaculty(this.FacultyID.Value, this.AdminIdentity.LoginSessionGuid);
					if (faculty == null)
					{
						this.AddNoRecordFoundAlert();
						Redirect<Faculties>();
						return;
					}
					this.ddlInstituteID.SelectedValue = faculty.InstituteID.ToString();
					this.tbFacultyName.Text = faculty.FacultyName;
					this.tbFacultyShortName.Text = faculty.FacultyShortName;
					this.tbFacultyAlias.Text = faculty.FacultyAlias;
				}
				else
				{
					this.AdminIdentity.Demand(AdminPermissions.Administration.ManageFaculties, UserGroupPermission.PermissionValues.Allowed);
				}
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
			var facultyName = this.tbFacultyName.Text;
			var facultyShortName = this.tbFacultyShortName.Text;
			var facultyAlias = this.tbFacultyAlias.Text.ToUpper();
			if (this.FacultyID == null)
			{
				var result = Aspire.BL.Core.Administration.Admin.Faculties.AddFaculty(instituteID, facultyName, facultyShortName, facultyAlias, this.AdminIdentity.LoginSessionGuid);
				switch (result.Status)
				{
					case BL.Core.Administration.Admin.Faculties.AddFacultyResult.Statuses.NameAlreadyExists:
						this.AddErrorAlert("Faculty Name already exist.");
						return;
					case BL.Core.Administration.Admin.Faculties.AddFacultyResult.Statuses.ShortNameAlreadyExists:
						this.AddErrorAlert("Faculty Short Name already exist.");
						return;
					case BL.Core.Administration.Admin.Faculties.AddFacultyResult.Statuses.AliasAlreadyExists:
						this.AddErrorAlert("Faculty Alias already exist.");
						return;
					case BL.Core.Administration.Admin.Faculties.AddFacultyResult.Statuses.Success:
						this.AddSuccessMessageHasBeenAdded("Faculty");
						Redirect<Faculties>();
						break;
					default:
						throw new NotImplementedEnumException(result.Status);
				}
			}
			else
			{
				var status = Aspire.BL.Core.Administration.Admin.Faculties.UpdateFaculty(this.FacultyID.Value, facultyName, facultyShortName, facultyAlias, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.Administration.Admin.Faculties.UpdateFacultyStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<Faculties>();
						return;
					case BL.Core.Administration.Admin.Faculties.UpdateFacultyStatuses.NameAlreadyExists:
						this.AddErrorAlert("Faculty Name already exist.");
						break;
					case BL.Core.Administration.Admin.Faculties.UpdateFacultyStatuses.ShortNameAlreadyExists:
						this.AddErrorAlert("Faculty Short Name already exist.");
						break;
					case BL.Core.Administration.Admin.Faculties.UpdateFacultyStatuses.AliasAlreadyExists:
						this.AddErrorAlert("Faculty Alias already exist.");
						break;
					case BL.Core.Administration.Admin.Faculties.UpdateFacultyStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Faculty");
						Redirect<Faculties>();
						return;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
		}
	}
}