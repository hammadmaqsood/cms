﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Faculty.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Administration.Faculty" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
		<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" CausesValidation="False" ValidationGroup="Faculty" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbFacultyName" />
		<aspire:AspireTextBox runat="server" MaxLength="1000" ID="tbFacultyName" />
		<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbFacultyName" RequiredErrorMessage="This field is required." ValidationGroup="Faculty" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Short Name:" AssociatedControlID="tbFacultyShortName" />
		<aspire:AspireTextBox runat="server" MaxLength="500" ID="tbFacultyShortName" />
		<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbFacultyShortName" RequiredErrorMessage="This field is required." ValidationGroup="Faculty" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Alias:" AssociatedControlID="tbFacultyAlias" />
		<aspire:AspireTextBox runat="server" MaxLength="250" ID="tbFacultyAlias" TextTransform="UpperCase" />
		<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbFacultyAlias" RequiredErrorMessage="This field is required." ValidationGroup="Faculty" />
	</div>
	<div class="form-group text-center">
		<aspire:AspireButton runat="server" ID="btnSave" ButtonType="Primary" ValidationGroup="Faculty" Text="Save" OnClick="btnSave_OnClick" />
		<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="Faculties.aspx" />
	</div>
</asp:Content>
