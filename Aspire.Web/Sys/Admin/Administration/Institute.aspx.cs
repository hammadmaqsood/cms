﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Web.Sys.Admin.Administration
{
	[AspirePage("{4DE07C53-A1DB-49C7-AB38-CF25911BA82D}", UserTypes.Admins, "~/Sys/Admin/Administration/Institute.aspx", "Institute", true, AspireModules.Administration)]
	public partial class Institute : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.Administration.Module, UserGroupPermission.Allowed},
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_edit);
		public override string PageTitle => this.InstituteID.HasValue ? "Edit Institute" : "Add Institute";
		private int? InstituteID => this.Request.GetParameterValue<int>("InstituteID");

		public static string GetPageUrl(int instituteID)
		{
			return GetPageUrl<Institute>().AttachQueryParam("InstituteID", instituteID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.InstituteID != null)
				{
					var institute = BL.Core.Administration.Admin.Institutes.GetInstitute(this.InstituteID.Value, this.AdminIdentity.LoginSessionGuid);
					if (institute == null)
					{
						this.AddNoRecordFoundAlert();
						Redirect<Institutes>();
						return;
					}
					this.tbInstituteID.Text = institute.InstituteID.ToString();
					this.tbInstituteID.ReadOnly = true;
					this.tbInstituteName.Text = institute.InstituteName;
					this.tbShortInstituteName.Text = institute.InstituteShortName;
					this.tbInstituteAlias.Text = institute.InstituteAlias;
					this.tbInstituteCode.Text = institute.InstituteCode;
					this.tbwebiste.Text = institute.Website;
					this.tbAddress.Text = institute.MailAddress;
					this.tbPhone.Text = institute.Phone;
					this.tbFax.Text = institute.Fax;
				}
				else
				{
					if (!this.AdminIdentity.UserType.IsMasterAdmin())
					{
						this.AddErrorMessageYouAreNotAuthorizedToPerformThisAction();
						Redirect<Institutes>();
					}
				}
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var instituteName = this.tbInstituteName.Text;
			var instituteShortName = this.tbShortInstituteName.Text;
			var instituteAlias = this.tbInstituteAlias.Text.ToUpper();
			var instituteCode = this.tbInstituteCode.Text;
			var website = this.tbwebiste.Text;
			var mailAddress = this.tbAddress.Text;
			var phone = this.tbPhone.Text;
			var fax = this.tbFax.Text;
			if (this.InstituteID == null)
			{
				if (!this.AdminIdentity.UserType.IsMasterAdmin())
					throw new AspireNotAuthorizedException(UserTypes.MasterAdmin, AdminPermissions.Administration.ManageInstitutes, new[] { UserGroupPermission.PermissionValues.Allowed, }, null);
				var result = BL.Core.Administration.Admin.Institutes.AddInstitute(this.tbInstituteID.Text.ToInt(), instituteName, instituteShortName, instituteAlias, instituteCode, website, mailAddress, phone, fax, this.AdminIdentity.LoginSessionGuid);
				switch (result.Status)
				{
					case BL.Core.Administration.Admin.Institutes.AddInstituteResult.Statuses.NameAlreadyExists:
						this.AddErrorAlert("Institute Name already exists.");
						break;
					case BL.Core.Administration.Admin.Institutes.AddInstituteResult.Statuses.ShortNameAlreadyExists:
						this.AddErrorAlert("Institute Short Name already exists.");
						break;
					case BL.Core.Administration.Admin.Institutes.AddInstituteResult.Statuses.AliasAlreadyExists:
						this.AddErrorAlert("Institute Alias already exists.");
						break;
					case BL.Core.Administration.Admin.Institutes.AddInstituteResult.Statuses.Success:
						this.AddSuccessMessageHasBeenAdded("Institute");
						Redirect<Institutes>();
						return;
					default:
						throw new NotImplementedEnumException(result.Status);
				}
			}
			else
			{
				this.AdminIdentity.Demand(AdminPermissions.Administration.ManageInstitutes, UserGroupPermission.PermissionValues.Allowed);
				var status = BL.Core.Administration.Admin.Institutes.UpdateInstitute(this.InstituteID.Value, instituteName, instituteShortName, instituteAlias, instituteCode, website, mailAddress, phone, fax, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.Administration.Admin.Institutes.UpdateInstituteStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<Institutes>();
						return;
					case BL.Core.Administration.Admin.Institutes.UpdateInstituteStatuses.NameAlreadyExists:
						this.AddErrorAlert("Institute Name already exists.");
						break;
					case BL.Core.Administration.Admin.Institutes.UpdateInstituteStatuses.ShortNameAlreadyExists:
						this.AddErrorAlert("Institute Short Name already exists.");
						break;
					case BL.Core.Administration.Admin.Institutes.UpdateInstituteStatuses.AliasAlreadyExists:
						this.AddErrorAlert("Institute Alias already exists.");
						break;
					case BL.Core.Administration.Admin.Institutes.UpdateInstituteStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Institute");
						Redirect<Institutes>();
						return;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
		}
	}
}