﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Departments.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Administration.Departments" %>

<%@ Import Namespace="Aspire.Web.Sys.Admin.Administration" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="&nbsp;" />
				<div>
					<aspire:AspireHyperLinkButton ButtonType="Success" runat="server" Glyphicon="plus" Text="Add Department" NavigateUrl="Department.aspx" />
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
				<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteID_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Faculty:" AssociatedControlID="ddlFacultyID" />
				<aspire:AspireDropDownList runat="server" ID="ddlFacultyID" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlFacultyID_OnSelectedIndexChanged" />
			</div>
		</div>
	</div>
	<aspire:AspireGridView runat="server" ID="gvDepartments" ItemType="Aspire.BL.Core.Administration.Admin.Departments.Department" AutoGenerateColumns="False" AllowSorting="True" OnPageIndexChanging="gvDepartments_OnPageIndexChanging" OnPageSizeChanging="gvDepartments_OnPageSizeChanging" OnSorting="gvDepartments_OnSorting" OnRowCommand="gvDepartments_RowCommand">
		<Columns>
			<asp:BoundField HeaderText="Institute" DataField="InstituteAlias" SortExpression="InstituteAlias" />
			<asp:BoundField HeaderText="Faculty" DataField="FacultyAlias" SortExpression="FacultyAlias" />
			<asp:BoundField HeaderText="Name" DataField="DepartmentName" SortExpression="DepartmentName" />
			<asp:BoundField HeaderText="Short Name" DataField="DepartmentShortName" SortExpression="DepartmentShortName" />
			<asp:BoundField HeaderText="Alias" DataField="DepartmentAlias" SortExpression="DepartmentAlias" />
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireHyperLinkButton ButtonType="OnlyIcon" Glyphicon="edit" runat="server" NavigateUrl="<%# Department.GetPageUrl(Item.DepartmentID) %>" />
					<aspire:AspireLinkButton runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument="<%# Item.DepartmentID %>" ConfirmMessage="Are you sure you want to delete this record?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
