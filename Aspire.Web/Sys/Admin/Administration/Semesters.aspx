﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Semesters.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Administration.Semesters" %>

<%@ Import Namespace="Aspire.Model.Entities" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Semester Type:" AssociatedControlID="ddlSemesterType" />
				<aspire:AspireDropDownList runat="server" ID="ddlSemesterType" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterType" ErrorMessage="This field is required." ValidationGroup="Required" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Year:" AssociatedControlID="ddlYear" />
				<aspire:AspireDropDownList runat="server" ID="ddlYear" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlYear" ErrorMessage="This field is required." ValidationGroup="Required" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<div>
					<aspire:AspireLabel runat="server" Text="&nbsp;" AssociatedControlID="btnSave" />
				</div>
				<aspire:AspireButton runat="server" ID="btnSave" Text="Add" ButtonType="Primary" ValidationGroup="Required" OnClick="btnSave_OnClick" />
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvSemesters" ItemType="Aspire.BL.Core.Administration.Admin.Semesters.InstituteSemester" AutoGenerateColumns="False" OnRowCommand="gvSemesters_OnRowCommand">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate>
					<%#: Container.DataItemIndex + 1 %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Semester">
				<ItemTemplate>
					<%#: Item.SemesterID.ToSemesterString() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Institute">
				<ItemTemplate>
					<%#: Item.InstituteAlias %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Classes Start Date">
				<ItemTemplate>
					<%#: $"{Item.ClassesStartDate:d}".ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Classes End Date">
				<ItemTemplate>
					<%#: $"{Item.ClassesEndDate:d}".ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireLinkButton ToolTip="Edit" runat="server" CausesValidation="False" CommandName="Edit" Glyphicon="edit" EncryptedCommandArgument='<%# $"{Item.SemesterID}:{Item.InstituteID}" %>' />
					<aspire:AspireLinkButton ToolTip="Delete" runat="server" CausesValidation="False" CommandName="Delete" Glyphicon="remove" ConfirmMessage="Are you sure you want to delete Semester?" EncryptedCommandArgument='<%# Item.SemesterID %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal ID="modalInstituteSemester" HeaderText="Semester" runat="server" Visible="false">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelSemester" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert ID="alertSemester" runat="server" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterIDUpdate" />
						<aspire:AspireDropDownList runat="server" ID="ddlSemesterIDUpdate" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlInstituteIDUpdate" />
						<aspire:AspireDropDownList runat="server" ID="ddlInstituteIDUpdate" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Classes Start Date:" AssociatedControlID="dtpClassesStartDateUpdate" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpClassesStartDateUpdate" />
						<aspire:AspireDateTimeValidator AllowNull="false" runat="server" ControlToValidate="dtpClassesStartDateUpdate" InvalidDataErrorMessage="Invalid Date." InvalidRangeErrorMessage="Invalid Date Range." RequiredErrorMessage="This field is reqired." />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Classes End Date:" AssociatedControlID="dtpClassesEndDateUpdate" />
						<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpClassesEndDateUpdate" />
						<aspire:AspireDateTimeValidator AllowNull="true" runat="server" ControlToValidate="dtpClassesEndDateUpdate" InvalidDataErrorMessage="Invalid Date." InvalidRangeErrorMessage="Invalid Date Range." RequiredErrorMessage="This field is reqired." />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelSemesterButtons" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnUpdate" Text="Save" ValidationGroup="SemesterUpdate" OnClick="btnUpdate_Click" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
