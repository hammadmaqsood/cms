﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Administration
{
	[AspirePage("7F6E252C-0C6F-4A36-A00E-95E07E3CF87B", UserTypes.Admins, "~/Sys/Admin/Administration/Contacts.aspx", "Contacts", true, AspireModules.Administration)]
	public partial class Contacts : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.Administration.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => AspireGlyphicons.book.GetIcon();
		public override string PageTitle => "Contacts";

		private static string GetPageUrl(int? instituteID, Guid? facultyID, int? departmentID, int? pageIndex, int? pageSize, SortDirection sortDirection, string sortExpression)
		{
			return GetPageUrl<Contacts>().AttachQueryParams("InstituteID", instituteID, "FacultyID", facultyID, "DepartmentID", departmentID, "PageIndex", pageIndex, "PageSize", pageSize, "SortDirection", sortDirection, "SortExpression", sortExpression);
		}

		private static void Redirect(int? instituteID, Guid? facultyID, int? departmentID, int? pageIndex, int? pageSize, SortDirection sortDirection, string sortExpression)
		{
			Redirect(GetPageUrl(instituteID, facultyID, departmentID, pageIndex, pageSize, sortDirection, sortExpression));
		}

		private void RefreshPage()
		{
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToNullableInt();
			var facultyID = this.ddlFacultyIDFilter.SelectedValue.ToNullableGuid();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var pageIndex = this.gvContacts.PageIndex;
			var pageSize = this.gvContacts.PageSize;
			Redirect(instituteID, facultyID, departmentID, pageIndex, pageSize, sortDirection, sortExpression);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(this.Request.GetParameterValue("SortExpression") ?? nameof(Contact.Name), this.Request.GetParameterValue<SortDirection>("SortDirection") ?? SortDirection.Ascending);
				this.gvContacts.PageSize = this.GetParameterValue<int>("PageSize") ?? this.gvContacts.PageSize;
				this.ddlInstituteIDFilter.FillInstitutesForAdmin(CommonListItems.All).SetSelectedValueIfNotPostback("InstituteID");
				this.ddlInstituteIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlInstituteIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToNullableInt();
			if (instituteID == null)
				this.ddlFacultyIDFilter.DataBind(CommonListItems.All);
			else
				this.ddlFacultyIDFilter.FillFaculties(instituteID.Value, CommonListItems.All).SetSelectedValueIfNotPostback("FacultyID");
			this.ddlFacultyIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlFacultyIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToNullableInt();
			if (instituteID == null)
				this.ddlDepartmentIDFilter.DataBind(CommonListItems.All);
			else
			{
				var facultyID = this.ddlFacultyIDFilter.SelectedValue.ToNullableGuid();
				this.ddlDepartmentIDFilter.FillDepartments(instituteID.Value, facultyID, CommonListItems.All).SetSelectedValueIfNotPostback("DepartmentID");
			}
			this.ddlDepartmentIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
				this.GetData(this.GetParameterValue<int>("PageIndex") ?? 0);
			else
				this.GetData(0);
		}

		protected void gvContacts_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvContacts_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvContacts_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvContacts.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvContacts_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayContact(e.DecryptedCommandArgument().ToGuid());
					return;
				case "Delete":
					var status = BL.Core.Administration.Admin.Contacts.DeleteContact(e.DecryptedCommandArgumentToGuid(), this.AspireIdentity.LoginSessionGuid);
					switch (status)
					{
						case BL.Core.Administration.Admin.Contacts.DeleteContactStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Contact");
							this.RefreshPage();
							return;
						case BL.Core.Administration.Admin.Contacts.DeleteContactStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.RefreshPage();
							return;
						default:
							throw new NotImplementedEnumException(status);
					}
			}
		}

		private void GetData(int pageIndex)
		{
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToNullableInt();
			var facultyID = this.ddlFacultyIDFilter.SelectedValue.ToNullableGuid();
			var departmentID = this.ddlDepartmentIDFilter.SelectedValue.ToNullableInt();
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var pageSize = this.gvContacts.PageSize;
			var data = BL.Core.Administration.Admin.Contacts.GetContacts(instituteID, facultyID, departmentID, pageIndex, pageSize, sortExpression, sortDirection, out var virtualItemCount, this.AspireIdentity.LoginSessionGuid);
			this.gvContacts.DataBind(data, pageIndex, virtualItemCount, sortExpression, sortDirection);
		}

		protected void btnAddContact_OnClick(object sender, EventArgs e)
		{
			this.DisplayContact(null);
		}

		protected void ddlInstituteID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			if (instituteID == null)
				this.ddlFacultyID.DataBind(CommonListItems.Select);
			else
				this.ddlFacultyID.FillFaculties(instituteID.Value, CommonListItems.Select);
			this.ddlFacultyID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlFacultyID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			if (instituteID == null)
				this.ddlDepartmentID.DataBind(CommonListItems.Select);
			else
			{
				var facultyID = this.ddlFacultyID.SelectedValue.ToNullableGuid();
				this.ddlDepartmentID.FillDepartments(instituteID.Value, facultyID, CommonListItems.Select);
			}
		}

		private void DisplayContact(Guid? contactID)
		{
			this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.Select);
			this.ddlDesignation.FillGuidEnums<Contact.Designations>(CommonListItems.Select);

			if (contactID == null)
			{
				this.modalContact.HeaderText = "Add Contact";
				this.ddlInstituteID.ClearSelection();
				this.ddlInstituteID_OnSelectedIndexChanged(null, null);
				this.ddlDesignation.ClearSelection();
				this.ddlInstituteID.Enabled = true;
				this.ddlFacultyID.Enabled = true;
				this.ddlDepartmentID.Enabled = true;
				this.ddlDesignation.Enabled = true;
				this.tbName.Text = null;
				this.tbEmail.Text = null;
			}
			else
			{
				var contact = BL.Core.Administration.Admin.Contacts.GetContact(contactID.Value, this.AspireIdentity.LoginSessionGuid);
				if (contact == null)
				{
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				}

				this.ddlInstituteID.SelectedValue = contact.InstituteID.ToString();
				this.ddlInstituteID_OnSelectedIndexChanged(null, null);
				this.ddlFacultyID.SelectedValue = contact.FacultyID.ToString();
				this.ddlFacultyID_OnSelectedIndexChanged(null, null);
				this.ddlDepartmentID.SelectedValue = contact.DepartmentID.ToString();
				this.ddlDesignation.SetSelectedGuidEnum(contact.DesignationEnum);
				this.ddlInstituteID.Enabled = false;
				this.ddlFacultyID.Enabled = false;
				this.ddlDepartmentID.Enabled = false;
				this.ddlDesignation.Enabled = false;
				this.tbName.Text = contact.Name;
				this.tbEmail.Text = contact.Email;
				this.modalContact.HeaderText = "Edit Contact";
			}

			this.ViewState[nameof(Contact.ContactID)] = contactID;
			this.modalContact.Show();
			this.modalContact.Visible = true;
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var contactID = (Guid?)this.ViewState[nameof(Contact.ContactID)];
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			var facultyID= this.ddlInstituteID.SelectedValue.ToNullableGuid();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var designation = this.ddlDesignation.GetSelectedGuidEnum<Contact.Designations>();
			var name = this.tbName.Text;
			var email = this.tbEmail.Text;

			if (contactID == null)
			{
				var status = BL.Core.Administration.Admin.Contacts.AddContact(instituteID,facultyID, departmentID, designation, name, email, this.AspireIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.Administration.Admin.Contacts.AddContactStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Contact");
						this.RefreshPage();
						break;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
			else
			{
				var status = BL.Core.Administration.Admin.Contacts.UpdateContact(contactID.Value, name, email, this.AspireIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.Administration.Admin.Contacts.UpdateContactStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						return;
					case BL.Core.Administration.Admin.Contacts.UpdateContactStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Contact");
						this.RefreshPage();
						return;
					default:
						throw new NotImplementedEnumException(status);
				}
			}
		}
	}
}