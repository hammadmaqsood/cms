﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Administration
{
	[AspirePage("{C593F89D-7A4B-4148-AE90-DB0190A89D9A}", UserTypes.Admins, "~/Sys/Admin/Administration/Departments.aspx", "Manage Departments", true, AspireModules.Administration)]
	public partial class Departments : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.Administration.Module, new[] {UserGroupPermission.PermissionValues.Allowed}},
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_edit);
		public override string PageTitle => "Manage Departments";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(nameof(BL.Core.Administration.Admin.Departments.Department.InstituteAlias), SortDirection.Ascending);
				this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.All);
				if (this.ddlInstituteID.Items.Count == 0)
				{
					this.AddWarningAlert("No institute found.");
					Redirect<Dashboard>();
				}
				this.ddlInstituteID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlInstituteID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			if (instituteID == null)
				this.ddlFacultyID.DataBind(CommonListItems.All);
			else
				this.ddlFacultyID.FillFaculties(instituteID.Value, CommonListItems.All);
			this.ddlFacultyID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlFacultyID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void gvDepartments_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvDepartments_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvDepartments.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvDepartments_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(this.gvDepartments.PageIndex);
		}

		protected void gvDepartments_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					var departmentID = e.DecryptedCommandArgumentToInt();
					var status = BL.Core.Administration.Admin.Departments.DeleteDepartment(departmentID, this.AspireIdentity.LoginSessionGuid);
					switch (status)
					{
						case BL.Core.Administration.Admin.Departments.DeleteDepartmentStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.GetData(0); break;
						case BL.Core.Administration.Admin.Departments.DeleteDepartmentStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Department");
							this.GetData(0);
							break;
						case BL.Core.Administration.Admin.Departments.DeleteDepartmentStatuses.ChildRecordExistsExamInvigilators:
						case BL.Core.Administration.Admin.Departments.DeleteDepartmentStatuses.ChildRecordExistsPrograms:
						case BL.Core.Administration.Admin.Departments.DeleteDepartmentStatuses.ChildRecordExistsFacultyMembers:
						case BL.Core.Administration.Admin.Departments.DeleteDepartmentStatuses.ChildRecordExistsUserRoleModules:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Department");
							break;
						default:
							throw new NotImplementedEnumException(status);
					}
					break;
			}
		}

		private void GetData(int pageIndex)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			var facultyID = this.ddlFacultyID.SelectedValue.ToNullableGuid();
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var departments = BL.Core.Administration.Admin.Departments.GetDepartments(instituteID, facultyID, sortExpression, sortDirection, pageIndex, this.gvDepartments.PageSize, out var virtualItemCount, this.AdminIdentity.LoginSessionGuid);
			this.gvDepartments.DataBind(departments, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}
	}
}