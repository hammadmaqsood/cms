﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Department.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Administration.Department" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
		<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" CausesValidation="False" ValidationGroup="Dept" AutoPostBack="true" OnSelectedIndexChanged="ddlInstituteID_SelectedIndexChanged" />
		<aspire:AspireRequiredFieldValidator runat="server" ID="rfvInstituteID" ValidationGroup="Dept" ErrorMessage="This field is required." ControlToValidate="ddlInstituteID" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Faculty:" AssociatedControlID="ddlFacultyID" />
		<aspire:AspireDropDownList runat="server" ID="ddlFacultyID" CausesValidation="False" ValidationGroup="Dept" AutoPostBack="true" />
		<aspire:AspireRequiredFieldValidator runat="server" ID="rfvFacultyID" ErrorMessage="This field is required." ValidationGroup="Dept" ControlToValidate="ddlFacultyID" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbDepartmentName" />
		<aspire:AspireTextBox runat="server" MaxLength="1000" ID="tbDepartmentName" />
		<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbDepartmentName" RequiredErrorMessage="This field is required." ValidationGroup="Dept" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Short Name:" AssociatedControlID="tbDepartmentShortName" />
		<aspire:AspireTextBox runat="server" MaxLength="500" ID="tbDepartmentShortName" />
		<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbDepartmentShortName" RequiredErrorMessage="This field is required." ValidationGroup="Dept" />
	</div>
	<div class="form-group">
		<aspire:AspireLabel runat="server" Text="Alias:" AssociatedControlID="tbDepartmentAlias" />
		<aspire:AspireTextBox runat="server" MaxLength="250" ID="tbDepartmentAlias" TextTransform="UpperCase" />
		<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbDepartmentAlias" RequiredErrorMessage="This field is required." ValidationGroup="Dept" />
	</div>
	<div class="form-group text-center">
		<aspire:AspireButton runat="server" ID="btnSave" ButtonType="Primary" ValidationGroup="Dept" Text="Save" OnClick="btnSave_OnClick" />
		<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="Departments.aspx" />
	</div>
</asp:Content>
