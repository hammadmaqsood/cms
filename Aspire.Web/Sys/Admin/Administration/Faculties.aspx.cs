﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Administration
{
	[AspirePage("{3839615D-B30A-4C61-A63B-3EF1013590C9}", UserTypes.Admins, "~/Sys/Admin/Administration/Faculties.aspx", "Manage Faculties", true, AspireModules.Administration)]
	public partial class Faculties : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.Administration.Module, new[] {UserGroupPermission.PermissionValues.Allowed}},
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_edit);
		public override string PageTitle => "Manage Faculties";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("InstituteAlias", SortDirection.Ascending);
				this.ddlInstitute.FillInstitutesForAdmin(CommonListItems.All);
				if (this.ddlInstitute.Items.Count == 0)
				{
					this.AddWarningAlert("No institute found.");
					Redirect<Dashboard>();
					return;
				}
				this.ddlInstitute_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlInstitute_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void gvFaculties_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvFaculties_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvFaculties.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvFaculties_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(this.gvFaculties.PageIndex);
		}

		protected void gvFaculties_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					var facultyID = e.DecryptedCommandArgumentToGuid();
					this.AdminIdentity.Demand(AdminPermissions.Administration.ManageFaculties, UserGroupPermission.PermissionValues.Allowed);
					var status = BL.Core.Administration.Admin.Faculties.DeleteFaculty(facultyID, this.AspireIdentity.LoginSessionGuid);
					switch (status)
					{
						case BL.Core.Administration.Admin.Faculties.DeleteFacultyStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.GetData(0); break;
						case BL.Core.Administration.Admin.Faculties.DeleteFacultyStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Faculty");
							this.GetData(0);
							break;
						case BL.Core.Administration.Admin.Faculties.DeleteFacultyStatuses.ChildRecordExistsContacts:
						case BL.Core.Administration.Admin.Faculties.DeleteFacultyStatuses.ChildRecordExistsDepartments:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Faculty");
							break;
						default:
							throw new NotImplementedEnumException(status);
					}
					break;
			}
		}

		private void GetData(int pageIndex)
		{
			var instituteID = this.ddlInstitute.SelectedValue.ToNullableByte();
			var faculties = BL.Core.Administration.Admin.Faculties.GetFaculties(instituteID, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), pageIndex, this.gvFaculties.PageSize, out int virtualItemCount, this.AdminIdentity.LoginSessionGuid);
			this.gvFaculties.DataBind(faculties, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}
	}
}