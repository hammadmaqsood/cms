﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Contacts.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Administration.Contacts" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="&nbsp;" AssociatedControlID="btnAddContact" />
				<div>
					<aspire:AspireButton ID="btnAddContact" runat="server" CausesValidation="False" Glyphicon="plus" ButtonType="Success" Text="Add Contact" OnClick="btnAddContact_OnClick" />
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlInstituteIDFilter" OnSelectedIndexChanged="ddlInstituteIDFilter_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Faculty:" AssociatedControlID="ddlFacultyIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlFacultyIDFilter" OnSelectedIndexChanged="ddlFacultyIDFilter_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlInstituteIDFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlDepartmentIDFilter" OnSelectedIndexChanged="ddlDepartmentIDFilter_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
			</div>
		</div>
	</div>
	<aspire:AspireGridView runat="server" AutoGenerateColumns="False" AllowSorting="True" ID="gvContacts" ItemType="Aspire.Model.Entities.Contact" OnSorting="gvContacts_OnSorting" OnPageIndexChanging="gvContacts_OnPageIndexChanging" OnPageSizeChanging="gvContacts_OnPageSizeChanging" OnRowCommand="gvContacts_OnRowCommand">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Institute" SortExpression="InstituteAlias">
				<ItemTemplate><%#: (Item.Institute?.InstituteAlias).ToNAIfNullOrEmpty() %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Faculty" SortExpression="FacultyAlias">
				<ItemTemplate><%#: (Item.Faculty?.FacultyAlias).ToNAIfNullOrEmpty() %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Department" SortExpression="DepartmentAlias">
				<ItemTemplate><%#: (Item.Department?.DepartmentAlias).ToNAIfNullOrEmpty() %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Designation" SortExpression="Designation">
				<ItemTemplate><%#: Item.DesignationFullName.ToNAIfNullOrEmpty() %></ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Name" SortExpression="Name">
				<ItemTemplate><%#: Item.Name.ToNAIfNullOrEmpty() %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" EncryptedCommandArgument="<%# Item.ContactID %>" Glyphicon="edit" CommandName="Edit" ToolTip="Edit" CausesValidation="False" />
					<aspire:AspireLinkButton runat="server" EncryptedCommandArgument="<%# Item.ContactID %>" Glyphicon="remove" CommandName="Delete" ToolTip="Delete" ConfirmMessage="Are you sure you want to delete this contact?" CausesValidation="False" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<aspire:AspireModal runat="server" ID="modalContact" ModalSize="None">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelContact" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertContact" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
						<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" ValidationGroup="SaveContact" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteID_OnSelectedIndexChanged" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Faculty:" AssociatedControlID="ddlFacultyID" />
						<aspire:AspireDropDownList runat="server" ID="ddlFacultyID" ValidationGroup="SaveContact" AutoPostBack="True" OnSelectedIndexChanged="ddlFacultyID_OnSelectedIndexChanged" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
						<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" ValidationGroup="SaveContact" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Designation:" AssociatedControlID="ddlDesignation" />
						<aspire:AspireDropDownList runat="server" ID="ddlDesignation" ValidationGroup="SaveContact" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlDesignation" ValidationGroup="SaveContact" ErrorMessage="This field is required." />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Name:" AssociatedControlID="tbName" />
						<aspire:AspireTextBox runat="server" ID="tbName" MaxLength="100" ValidationGroup="SaveContact" />
						<aspire:AspireStringValidator ValidationGroup="SaveContact" ControlToValidate="tbName" runat="server" AllowNull="True" RequiredErrorMessage="This field is required." />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Email:" AssociatedControlID="tbName" />
						<aspire:AspireTextBox runat="server" ID="tbEmail" MaxLength="100" ValidationGroup="SaveContact" />
						<aspire:AspireStringValidator ValidationGroup="SaveContact" ControlToValidate="tbEmail" ValidationExpression="Email" runat="server" AllowNull="False" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Email Address is not valid." />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Always">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="SaveContact" Text="Save" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
