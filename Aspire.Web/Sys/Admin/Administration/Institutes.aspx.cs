﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Aspire.Lib.Extensions.Exceptions;

namespace Aspire.Web.Sys.Admin.Administration
{
	[AspirePage("{18021B37-3D68-4320-8D70-1507F524C993}", UserTypes.Admins, "~/Sys/Admin/Administration/Institutes.aspx", "Manage Institutes", true, AspireModules.Administration)]
	public partial class Institutes : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.Administration.Module, new[] {UserGroupPermission.PermissionValues.Allowed}}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_edit);
		public override string PageTitle => "Manage Institutes";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.hlbtnAddInstitute.Visible = this.AdminIdentity.UserType.IsMasterAdmin();
				this.ViewState.SetSortProperties(nameof(Model.Entities.Institute.InstituteID), SortDirection.Ascending);
				this.GetData(0);
			}
		}

		protected void gvInstitutes_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvInstitutes_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvInstitutes.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvInstitutes_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(this.gvInstitutes.PageIndex);
		}

		private void GetData(int pageIndex)
		{
			var institutes = BL.Core.Administration.Admin.Institutes.GetInstitutes(this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), pageIndex, this.gvInstitutes.PageSize, out var virtualItemCount, this.AdminIdentity.LoginSessionGuid);
			this.gvInstitutes.DataBind(institutes, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}
	}
}