﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="MarkingAndGradingSchemeAssignment.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Exams.MarkingAndGradingSchemeAssignment" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
					<aspire:AspireDropDownList ValidationGroup="Filter" runat="server" ID="ddlInstituteID" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
					<aspire:AspireDropDownList ValidationGroup="Filter" runat="server" ID="ddlDepartmentID" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Degree Level:" AssociatedControlID="ddlDegreeLevel" />
					<aspire:AspireDropDownList ValidationGroup="Filter" runat="server" ID="ddlDegreeLevel" AutoPostBack="True" OnSelectedIndexChanged="ddlDegreeLevel_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
					<aspire:AspireDropDownList ValidationGroup="Filter" runat="server" ID="ddlProgramID" AutoPostBack="True" OnSelectedIndexChanged="ddlProgramID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Intake Semester:" AssociatedControlID="ddlIntakeSemesterID" />
					<aspire:AspireDropDownList ValidationGroup="Filter" runat="server" ID="ddlIntakeSemesterID" AutoPostBack="True" OnSelectedIndexChanged="ddlIntakeSemesterID_OnSelectedIndexChanged" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvAdmissionOpenPrograms" AutoGenerateColumns="False" AllowSorting="True" OnSorting="gvAdmissionOpenPrograms_OnSorting" OnPageIndexChanging="gvAdmissionOpenPrograms_OnPageIndexChanging" OnPageSizeChanging="gvAdmissionOpenPrograms_OnPageSizeChanging">
		<Columns>
			<asp:BoundField DataField="DegreeLevelEnum" HeaderText="Level" SortExpression="DegreeLevelEnum" />
			<asp:BoundField DataField="ProgramAlias" HeaderText="Program" SortExpression="ProgramAlias" />
			<asp:BoundField DataField="IntakeSemester" HeaderText="IntakeSemester" SortExpression="IntakeSemester" />
			<asp:TemplateField HeaderText="Marking Scheme" SortExpression="MarkingSchemeName">
				<ItemTemplate>
					<aspire:HiddenField runat="server" ID="hf" Value='<%# $"{Eval("AdmissionOpenProgramID")}:{Eval("ExamMarkingSchemeID")}:{Eval("ExamMarksGradingSchemeID")}" %>' Visible="False" />
					<aspire:AspireDropDownList runat="server" ValidationGroup="Update" ID="ddlExamMarkingSchemeID" SelectedValue='<%# Eval("ExamMarkingSchemeID") %>' DataSource='<%# this.ExamMarkingSchemes  %>' DataTextField="Text" DataValueField="ID" />
					<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Update" ControlToValidate="ddlExamMarkingSchemeID" ErrorMessage="This field is required." />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Grading Scheme" SortExpression="GradingSchemeName">
				<ItemTemplate>
					<aspire:AspireDropDownList runat="server" ValidationGroup="Update" ID="ddlExamMarksGradingSchemeID" SelectedValue='<%# Eval("ExamMarksGradingSchemeID") %>' DataSource='<%# this.ExamMarksGradingSchemes  %>' DataTextField="Text" DataValueField="ID" />
					<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="Update" ControlToValidate="ddlExamMarksGradingSchemeID" ErrorMessage="This field is required." />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<div class="text-center">
		<aspire:AspireButton runat="server" ID="btnUpdate" ValidationGroup="Update" Text="Update" OnClick="btnUpdate_OnClick" />
		<aspire:AspireHyperLink runat="server" NavigateUrl="MarkingAndGradingSchemeAssignment.aspx" Text="Cancel" />
	</div>
</asp:Content>
