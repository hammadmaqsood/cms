﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ControlPanel.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Exams.ControlPanel" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireButton runat="server" ID="btnAdd" Text="Add/Edit" CausesValidation="False" Glyphicon="pencil" ButtonType="Success" OnClick="btnAdd_OnClick" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterID" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemesterID" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlSemesterID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
					<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentID" />
					<aspire:AspireDropDownList runat="server" ID="ddlDepartmentID" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Program:" AssociatedControlID="ddlProgramID" />
					<aspire:AspireDropDownList runat="server" ID="ddlProgramID" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlProgramID_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatus" />
					<aspire:AspireDropDownList runat="server" ID="ddlStatus" ValidationGroup="Filter" AutoPostBack="True" OnSelectedIndexChanged="ddlStatus_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="From:" AssociatedControlID="dtpFrom" />
					<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpFrom" ValidationGroup="Filter" />
					<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dtpFrom" InvalidDataErrorMessage="Invalid Date." AllowNull="True" ValidationGroup="Filter" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="To:" AssociatedControlID="dtpTo" />
					<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpTo" ValidationGroup="Filter" />
					<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dtpTo" InvalidDataErrorMessage="Invalid Date." AllowNull="True" ValidationGroup="Filter" />
				</div>
				<div class="form-group">
					<aspire:AspireButton runat="server" ID="btnSearch" OnClick="btnSearch_OnClick" ButtonType="Primary" Text="Search" ValidationGroup="Filter" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvControlPanel" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnSorting="gvControlPanel_OnSorting" OnPageIndexChanging="gvControlPanel_OnPageIndexChanging" OnPageSizeChanging="gvControlPanel_OnPageSizeChanging" OnRowCommand="gvControlPanel_OnRowCommand" OnRowDataBound="gvControlPanel_OnRowDataBound">
		<Columns>
			<asp:BoundField HeaderText="Semester" DataField="Semester" SortExpression="Semester" />
			<asp:BoundField HeaderText="Institute" DataField="InstituteAlias" SortExpression="InstituteAlias" />
			<asp:BoundField HeaderText="Department" DataField="DepartmentAlias" SortExpression="DepartmentAlias" />
			<asp:BoundField HeaderText="Program" DataField="ProgramAlias" SortExpression="ProgramAlias" />
			<asp:BoundField HeaderText="Exam Marks Types" SortExpression="ExamMarksTypesFullNames" DataField="ExamMarksTypesFullNames" />
			<asp:BoundField HeaderText="From" DataField="FromDate" SortExpression="FromDate" DataFormatString="{0:d}" />
			<asp:BoundField HeaderText="To" DataField="ToDate" SortExpression="ToDate" DataFormatString="{0:d}" />
			<asp:TemplateField HeaderText="Status" SortExpression="Status">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CommandName="Status" EncryptedCommandArgument='<%#Eval("ExamMarksSettingID") %>' Text='<%#Eval("StatusFullName")%>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument='<%#Eval("ExamMarksSettingID") %>' />
					<aspire:AspireLinkButton runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%#Eval("ExamMarksSettingID") %>' ConfirmMessage='<%#Eval("ProgramAlias","Are you sure you want to delete exam marks settings of program \"{0}\"?") %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<aspire:AspireModal runat="server" ID="modalControlPanel">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updateModalControlPanel" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertModalControlPanel" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterForSave" />
						<aspire:AspireDropDownList runat="server" ID="ddlSemesterForSave" ValidationGroup="RequiredControlPanel" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterForSave" ErrorMessage="This field is required." ValidationGroup="RequiredControlPanel" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Marks Types:" AssociatedControlID="cblExamMarksTypes" />
						<div>
							<aspire:AspireCheckBoxList runat="server" ID="cblExamMarksTypes" ValidationGroup="RequiredControlPanel" RepeatLayout="Flow" RepeatDirection="Horizontal" />
						</div>
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="cblExamMarksTypes" ErrorMessage="This field is required." ValidationGroup="RequiredControlPanel" />
					</div>
					<div class="row">
						<div class="form-group col-md-6">
							<aspire:AspireLabel runat="server" Text="From:" AssociatedControlID="dpFromDate" />
							<aspire:AspireDateTimePickerTextbox runat="server" ID="dpFromDate" />
							<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dpFromDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." AllowNull="False" ValidationGroup="RequiredControlPanel" />
						</div>
						<div class="form-group col-md-6">
							<aspire:AspireLabel runat="server" Text="To:" AssociatedControlID="dpToDate" />
							<aspire:AspireDateTimePickerTextbox runat="server" ID="dpToDate" />
							<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dpToDate" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." AllowNull="False" ValidationGroup="RequiredControlPanel" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="rblStatus" />
						<div>
							<aspire:AspireRadioButtonList runat="server" ID="rblStatus" RepeatDirection="Horizontal" RepeatLayout="Flow" />
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteForSave" />
						<aspire:AspireDropDownList CausesValidation="False" runat="server" ID="ddlInstituteForSave" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteForSave_OnSelectedIndexChanged" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlInstituteForSave" ErrorMessage="This field is required." ValidationGroup="RequiredControlPanel" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Department:" AssociatedControlID="ddlDepartmentIDForSave" />
						<aspire:AspireDropDownList CausesValidation="False" runat="server" ID="ddlDepartmentIDForSave" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentIDForSave_OnSelectedIndexChanged" />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Programs:" AssociatedControlID="cblPrograms" />
						<div>
							<aspire:AspireCheckBoxList runat="server" ID="cblPrograms" RepeatLayout="Flow" RepeatDirection="Vertical" />
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" ValidationGroup="RequiredControlPanel" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
	<script type="text/javascript">
		cblApplySelectAll("#<%=this.cblPrograms.ClientID%>");
	</script>
</asp:Content>
