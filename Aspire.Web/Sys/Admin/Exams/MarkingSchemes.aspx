﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="MarkingSchemes.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Exams.MarkingSchemes" %>

<%@ Import Namespace="Aspire.Web.Sys.Admin.Exams" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireHyperLinkButton runat="server" Glyphicon="plus" ButtonType="Success" Text="Add" NavigateUrl="MarkingScheme.aspx" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
					<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteID_OnSelectedIndexChanged" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvExamMarkingSchemes" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gvExamMarkingSchemes_OnPageIndexChanging" OnPageSizeChanging="gvExamMarkingSchemes_OnPageSizeChanging" OnSorting="gvExamMarkingSchemes_OnSorting" OnRowCommand="gvExamMarkingSchemes_OnRowCommand">
		<Columns>
			<asp:BoundField HeaderText="Institute" SortExpression="InstituteAlias" DataField="InstituteAlias" />
			<asp:BoundField HeaderText="Name" SortExpression="MarkingSchemeName" DataField="MarkingSchemeName" />
			<asp:BoundField HeaderText="Type" SortExpression="MarkingSchemeType" DataField="MarkingSchemeTypeFullName" />
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireHyperLinkButton ToolTip="Edit" Target="_blank" ButtonType="OnlyIcon" runat="server" Glyphicon="edit" NavigateUrl='<%# MarkingScheme.GetPageUrl((int)Eval("ExamMarkingSchemeID")) %>' />
					<aspire:AspireLinkButton ToolTip="Delete" CausesValidation="false" runat="server" CommandName="Delete" Glyphicon="remove" EncryptedCommandArgument='<%#Eval("ExamMarkingSchemeID")%>' ConfirmMessage="Are you sure you want to delete?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
