﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Aspire.BL.Core.Exams.Admin;

namespace Aspire.Web.Sys.Admin.Exams
{
	[AspirePage("F52DEF9F-1A14-4752-B58D-E5559B4FCD89", UserTypes.Admins, "~/Sys/Admin/Exams/MarksGradingSchemes.aspx", "Marks Grading Schemes", true, AspireModules.Exams)]
	public partial class MarksGradingSchemes : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.Exams.Module, new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_edit);
		public override string PageTitle => "Marks Grading Schemes";

		public static string GetPageUrl(int? instituteID, SortDirection? sortDirection, string sortExpression, int? pageSize)
		{
			return typeof(MarksGradingSchemes).GetAspirePageAttribute().PageUrl.AttachQueryParams("InstituteID", instituteID, "SortDirection", sortDirection, "SortExpression", sortExpression, "PageSize", pageSize);
		}

		public static void Redirect(int? instituteID, SortDirection? sortDirection, string sortExpression, int? pageSize)
		{
			Redirect(GetPageUrl(instituteID, sortDirection, sortExpression, pageSize));
		}

		private void RefreshPage()
		{
			Redirect(this.ddlInstituteID.SelectedValue.ToNullableInt(), this.ViewState.GetSortDirection(), this.ViewState.GetSortExpression(), this.gvExamMarksGradingSchemes.PageSize);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(this.Request.GetParameterValue("SortExpression") ?? "GradingSchemeName", this.Request.GetParameterValue<SortDirection>("SortDirection") ?? SortDirection.Ascending);
				this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.All).SetSelectedValueIfNotPostback("InstituteID");
				var pageSize = this.Request.GetParameterValue<int>("PageSize");
				if (pageSize != null)
					this.gvExamMarksGradingSchemes.PageSize = pageSize.Value;
				this.ddlInstituteID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlInstituteID_OnSelectedIndexChanged(object o, EventArgs e)
		{
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			var examMarksGradingSchemes = ExamMarksGradingSchemes.GetExamMarksGradingSchemes(instituteID, pageIndex, this.gvExamMarksGradingSchemes.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out var virtualItemCount, this.AdminIdentity.LoginSessionGuid);
			this.gvExamMarksGradingSchemes.DataBind(examMarksGradingSchemes, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		protected void gvExamMarksGradingSchemes_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvExamMarksGradingSchemes_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvExamMarksGradingSchemes.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvExamMarksGradingSchemes_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvExamMarksGradingSchemes_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					var examMarksGradingSchemeID = e.DecryptedCommandArgumentToInt();
					var deleted = ExamMarksGradingSchemes.DeleteExamMarksGradingScheme(examMarksGradingSchemeID, out var isChildExists, this.AdminIdentity.LoginSessionGuid);
					if (deleted)
					{
						if (isChildExists)
							throw new InvalidOperationException();
						else
							this.AddSuccessMessageHasBeenDeleted("Exam grading scheme");
					}
					else
					{
						if (isChildExists)
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Exam grading scheme");
						else
							this.AddNoRecordFoundAlert();
					}
					this.RefreshPage();
					break;
			}
		}
	}
}