﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Exams
{
	[AspirePage("D89CFA92-A795-4AA2-8E03-F3BEB95C8AA7", UserTypes.Admins, "~/Sys/Admin/Exams/ExamRemarksPolicy.aspx", "Exam Remarks Policy", true, AspireModules.Exams)]
	public partial class ExamRemarksPolicy : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.Exams.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => AspireGlyphicons.book.GetIcon();
		public override string PageTitle => "Exam Remarks Policy";

		public static string GetPageUrl(int? instituteID, int? pageIndex, int? pageSize, SortDirection sortDirection, string sortExpression, int? examRemarksPolicyID)
		{
			return GetPageUrl<ExamRemarksPolicy>().AttachQueryParams("InstituteID", instituteID, "PageIndex", pageIndex, "PageSize", pageSize, "SortDirection", sortDirection, "SortExpression", sortExpression, "ExamRemarksPolicyID", examRemarksPolicyID);
		}

		public static void Redirect(int? instituteID, int? pageIndex, int? pageSize, SortDirection sortDirection, string sortExpression, int? examRemarksPolicyID)
		{
			Redirect(GetPageUrl(instituteID, pageIndex, pageSize, sortDirection, sortExpression, examRemarksPolicyID));
		}

		private void RefreshPage(int? examRemarksPolicyID)
		{
			Redirect(this.ddlInstituteIDFilter.SelectedValue.ToNullableInt(), this.gvExamRemarksPolicies.PageIndex, this.gvExamRemarksPolicies.PageSize, this.ViewState.GetSortDirection(), this.ViewState.GetSortExpression(), examRemarksPolicyID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(this.Request.GetParameterValue("SortExpression") ?? nameof(Model.Entities.ExamRemarksPolicy.ExamRemarksPolicyName), this.Request.GetParameterValue<SortDirection>("SortDirection") ?? SortDirection.Ascending);
				this.ddlInstituteIDFilter.FillInstitutesForAdmin(CommonListItems.All).SetSelectedValueIfNotPostback("InstituteID");
				this.ddlInstituteIDFilter_OnSelectedIndexChanged(null, null);
				var examRemarksPolicyID = this.GetParameterValue<int>("ExamRemarksPolicyID");
				if (examRemarksPolicyID != null)
					this.DisplayPolicy(examRemarksPolicyID);
			}
		}

		protected void ddlInstituteIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void gvExamRemarksPolicies_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvExamRemarksPolicies_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvExamRemarksPolicies_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvExamRemarksPolicies.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvExamRemarksPolicies_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayPolicy(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var status = BL.Core.Exams.Admin.ExamRemarksPolicy.DeleteExamRemarksPolicy(e.DecryptedCommandArgumentToInt(), this.AspireIdentity.LoginSessionGuid);
					switch (status)
					{
						case BL.Core.Exams.Admin.ExamRemarksPolicy.DeleteExamRemarksPolicyStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Exam Remarks Policy");
							this.RefreshPage(null);
							return;
						case BL.Core.Exams.Admin.ExamRemarksPolicy.DeleteExamRemarksPolicyStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.RefreshPage(null);
							return;
						case BL.Core.Exams.Admin.ExamRemarksPolicy.DeleteExamRemarksPolicyStatuses.ChildRecordExists:
							this.AddErrorAlert("Exam Remarks Policy can not be deleted because it has been attached to Programs.");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		private void GetData(int pageIndex)
		{
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToNullableInt();
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var data = BL.Core.Exams.Admin.ExamRemarksPolicy.GetExamRemarksPolicies(instituteID, pageIndex, this.gvExamRemarksPolicies.PageSize, sortExpression, sortDirection, out var virtualItemCount, this.AspireIdentity.LoginSessionGuid);
			this.gvExamRemarksPolicies.DataBind(data, pageIndex, virtualItemCount, sortExpression, sortDirection);
		}

		protected void btnAddExamMarksPolicy_OnClick(object sender, EventArgs e)
		{
			this.DisplayPolicy(null);
		}

		private void DisplayPolicy(int? examRemarksPolicyID)
		{
			this.ddlInstituteID.FillInstitutesForAdmin();
			this.ddlValidFromSemesterID.FillSemesters(CommonListItems.Select);
			this.ddlValidUpToSemesterID.FillSemesters(CommonListItems.Empty);
			if (examRemarksPolicyID == null)
			{
				this.ddlInstituteID.ClearSelection();
				this.ddlInstituteID.Enabled = true;
				this.tbExamRemarksPolicyName.Text = null;
				this.tbExamRemarksPolicyDescription.Text = null;
				this.ddlValidFromSemesterID.ClearSelection();
				this.ddlValidUpToSemesterID.ClearSelection();
				this.panelDetails.Visible = false;
				this.modalPolicy.HeaderText = "Add Exam Remarks Policy";
			}
			else
			{
				var examRemarksPolicy = BL.Core.Exams.Admin.ExamRemarksPolicy.GetExamRemarksPolicy(examRemarksPolicyID.Value, this.AspireIdentity.LoginSessionGuid);
				if (examRemarksPolicy == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<ExamRemarksPolicy>();
					return;
				}

				this.ddlInstituteID.SelectedValue = examRemarksPolicy.InstituteID.ToString();
				this.ddlInstituteID.Enabled = false;
				this.tbExamRemarksPolicyName.Text = examRemarksPolicy.ExamRemarksPolicyName;
				this.tbExamRemarksPolicyDescription.Text = examRemarksPolicy.ExamRemarksPolicyDescription;
				this.ddlValidFromSemesterID.SelectedValue = examRemarksPolicy.ValidFromSemesterID.ToString();
				this.ddlValidUpToSemesterID.SelectedValue = examRemarksPolicy.ValidUpToSemesterID.ToString();
				this.panelDetails.Visible = true;
				this.repeaterExamRemarksPolicyDetails.DataBind(examRemarksPolicy.ExamRemarksPolicyDetails.OrderByDescending(d => d.FirstSemester).ThenBy(d => d.Sequence));
				this.modalPolicy.HeaderText = "Edit Exam Remarks Policy";
				this.ddlExamRemarksType.ClearSelection();
				var examRemarksTypes = Enum.GetValues(typeof(ExamRemarksTypes))
					.Cast<ExamRemarksTypes>()
					.Except(new[] { ExamRemarksTypes.Qualified })
					.Select(t => new ListItem
					{
						Text = t.ToFullName(),
						Value = ((byte)t).ToString()
					});
				this.ddlExamRemarksType.DataBind(examRemarksTypes, CommonListItems.Select);
				this.tbLessThanCGPA.Text = null;
				this.cbFirstSemester.Checked = false;
			}

			this.ViewState[nameof(Model.Entities.ExamRemarksPolicy.ExamRemarksPolicyID)] = examRemarksPolicyID;
			this.modalPolicy.Show();
			this.updatePanelPolicy.Update();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var examRemarksPolicyID = (int?)this.ViewState[nameof(Model.Entities.ExamRemarksPolicy.ExamRemarksPolicyID)];
			var examRemarksPolicy = new Model.Entities.ExamRemarksPolicy
			{
				ExamRemarksPolicyID = examRemarksPolicyID ?? default(int),
				InstituteID = this.ddlInstituteID.SelectedValue.ToInt(),
				ExamRemarksPolicyName = this.tbExamRemarksPolicyName.Text.TrimAndCannotBeEmpty(),
				ExamRemarksPolicyDescription = this.tbExamRemarksPolicyDescription.Text.TrimAndMakeItNullIfEmpty(),
				ValidFromSemesterID = this.ddlValidFromSemesterID.SelectedValue.ToShort(),
				ValidUpToSemesterID = this.ddlValidUpToSemesterID.SelectedValue.ToNullableShort()
			};

			if (examRemarksPolicyID == null)
			{
				var status = BL.Core.Exams.Admin.ExamRemarksPolicy.AddExamRemarksPolicy(examRemarksPolicy, this.AspireIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.Exams.Admin.ExamRemarksPolicy.AddExamRemarksPolicyStatuses.NameAlreadyExists:
						this.alertPolicy.AddErrorAlert("Policy name already exists.");
						this.updatePanelPolicy.Update();
						return;
					case BL.Core.Exams.Admin.ExamRemarksPolicy.AddExamRemarksPolicyStatuses.Success:
						this.AddSuccessAlert("Policy has been added.");
						this.RefreshPage(examRemarksPolicy.ExamRemarksPolicyID);
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var status = BL.Core.Exams.Admin.ExamRemarksPolicy.UpdateExamRemarksPolicy(examRemarksPolicy, this.AspireIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.Exams.Admin.ExamRemarksPolicy.UpdateExamRemarksPolicyStatuses.NameAlreadyExists:
						this.alertPolicy.AddErrorAlert("Policy name already exists.");
						this.updatePanelPolicy.Update();
						return;
					case BL.Core.Exams.Admin.ExamRemarksPolicy.UpdateExamRemarksPolicyStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.RefreshPage(null);
						return;
					case BL.Core.Exams.Admin.ExamRemarksPolicy.UpdateExamRemarksPolicyStatuses.Success:
						this.AddSuccessAlert("Policy has been updated.");
						this.RefreshPage(null);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		protected void btnAddExamRemarksPolicyDetail_OnClick(object sender, EventArgs e)
		{
			var examRemarksPolicyID = (int)this.ViewState[nameof(Model.Entities.ExamRemarksPolicy.ExamRemarksPolicyID)];
			var examRemarksTypeEnum = this.ddlExamRemarksType.GetSelectedEnumValue<ExamRemarksTypes>();
			var lessThanCGPA = this.tbLessThanCGPA.Text.ToDecimal();
			var firstSemester = this.cbFirstSemester.Checked;
			var status = BL.Core.Exams.Admin.ExamRemarksPolicy.AddExamRemarksPolicyDetail(examRemarksPolicyID, examRemarksTypeEnum, lessThanCGPA, firstSemester, this.AdminIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Exams.Admin.ExamRemarksPolicy.AddExamRemarksPolicyDetailStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					this.RefreshPage(null);
					return;
				case BL.Core.Exams.Admin.ExamRemarksPolicy.AddExamRemarksPolicyDetailStatuses.FirstSemesterRecordAlreadyExists:
					this.alertPolicy.AddErrorAlert("First semester record already exists.");
					this.updatePanelPolicy.Update();
					break;
				case BL.Core.Exams.Admin.ExamRemarksPolicy.AddExamRemarksPolicyDetailStatuses.RemarksAlreadyExists:
					this.alertPolicy.AddErrorAlert("Remarks already exists.");
					this.updatePanelPolicy.Update();
					break;
				case BL.Core.Exams.Admin.ExamRemarksPolicy.AddExamRemarksPolicyDetailStatuses.Success:
					this.RefreshPage(examRemarksPolicyID);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void repeaterExamRemarksPolicyDetails_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			var examRemarksPolicyID = (int)this.ViewState[nameof(Model.Entities.ExamRemarksPolicy.ExamRemarksPolicyID)];
			var status = BL.Core.Exams.Admin.ExamRemarksPolicy.DeleteExamRemarksPolicyDetail(e.DecryptedCommandArgumentToInt(), this.AdminIdentity.LoginSessionGuid);
			switch (status)
			{
				case BL.Core.Exams.Admin.ExamRemarksPolicy.DeleteExamRemarksPolicyDetailStatuses.NoRecordFound:
					this.RefreshPage(examRemarksPolicyID);
					return;
				case BL.Core.Exams.Admin.ExamRemarksPolicy.DeleteExamRemarksPolicyDetailStatuses.Success:
					this.RefreshPage(examRemarksPolicyID);
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}