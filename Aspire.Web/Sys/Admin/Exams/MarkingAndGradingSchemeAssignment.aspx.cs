﻿using Aspire.BL.Core.Exams.Admin;
using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Exams
{
	[AspirePage("04C01AED-860F-4864-AC07-345CD9326192", UserTypes.Admins, "~/Sys/Admin/Exams/MarkingAndGradingSchemeAssignment.aspx", "Marking and Grading Scheme Assignment", true, AspireModules.Exams)]
	public partial class MarkingAndGradingSchemeAssignment : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.Exams.Module, new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};

		public override PageIcon PageIcon => FontAwesomeIcons.solid_edit.GetIcon();

		public override string PageTitle => "Marking and Grading Scheme Assignment";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(nameof(ManageMarkingAndGradingSchemeAssignment.GetAdmissionOpenProgramsResult.AdmissionOpenProgram.ProgramAlias), SortDirection.Ascending);
				this.ddlInstituteID.FillInstitutesForAdmin();
				this.ddlDegreeLevel.FillEnums<DegreeLevels>(CommonListItems.All);
				this.ddlIntakeSemesterID.FillSemesters(CommonListItems.All);
				this.ddlInstituteID_OnSelectedIndexChanged(null, null);
			}
		}

		private int? InstituteID => this.ddlInstituteID.SelectedValue.ToNullableInt();
		private int? DepartmentID => this.InstituteID == null ? null : this.ddlDepartmentID.SelectedValue.ToNullableInt();
		private DegreeLevels? DegreeLevel => this.InstituteID == null ? null : this.ddlDegreeLevel.GetSelectedEnumValue<DegreeLevels>(null);
		private int? ProgramID => this.InstituteID == null ? null : this.ddlProgramID.SelectedValue.ToNullableInt();
		private short? IntakeSemesterID => this.InstituteID == null ? null : this.ddlIntakeSemesterID.SelectedValue.ToNullableShort();

		protected void ddlInstituteID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.InstituteID != null)
				this.ddlDepartmentID.FillDepartments(this.InstituteID.Value, CommonListItems.All);
			else
				this.ddlDepartmentID.DataBind(CommonListItems.All);
			this.ddlDepartmentID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlDegreeLevel_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDegreeLevel_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.InstituteID != null)
			{
				if (this.DepartmentID != null)
					this.ddlProgramID.FillPrograms(this.InstituteID.Value, this.DepartmentID.Value, this.DegreeLevel, true, CommonListItems.All);
				else
					this.ddlProgramID.FillProgramsWithDepartment(this.InstituteID.Value, this.DepartmentID, this.DegreeLevel, true, CommonListItems.All);
			}
			else
				this.ddlProgramID.Items.Clear();
			this.ddlProgramID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlIntakeSemesterID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlIntakeSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected List<CustomListItem> ExamMarkingSchemes { get; private set; }
		protected List<CustomListItem> ExamMarksGradingSchemes { get; private set; }

		private void GetData(int pageIndex)
		{
			if (this.InstituteID == null)
			{
				this.gvAdmissionOpenPrograms.DataBindNull();
				return;
			}
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var result = ManageMarkingAndGradingSchemeAssignment.GetAdmissionOpenPrograms(this.InstituteID.Value, this.DepartmentID, this.DegreeLevel, this.ProgramID, this.IntakeSemesterID, pageIndex, this.gvAdmissionOpenPrograms.PageSize, sortExpression, sortDirection, this.AdminIdentity.LoginSessionGuid);
			this.ExamMarkingSchemes = result.ExamMarkingSchemes;
			this.ExamMarksGradingSchemes = result.ExamMarksGradingSchemes;
			this.gvAdmissionOpenPrograms.DataBind(result.AdmissionOpenPrograms, pageIndex, result.VirtualItemCount, sortExpression, sortDirection);
		}

		protected void gvAdmissionOpenPrograms_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvAdmissionOpenPrograms_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvAdmissionOpenPrograms_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvAdmissionOpenPrograms.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void btnUpdate_OnClick(object sender, EventArgs e)
		{
			var admissionOpenPrograms = new List<ManageMarkingAndGradingSchemeAssignment.AdmissionOpenProgram>();
			foreach (GridViewRow gvRow in this.gvAdmissionOpenPrograms.Rows)
			{
				if (gvRow.RowType != DataControlRowType.DataRow)
					continue;
				var hf = ((Lib.WebControls.HiddenField)gvRow.FindControl("hf")).Value.Split(':');
				var admissionOpenProgramID = hf[0].ToInt();
				var oldExamMarkingSchemeID = hf[1].ToInt();
				var oldExamMarksGradingSchemeID = hf[2].ToInt();
				var newExamMarkingSchemeID = ((AspireDropDownList)gvRow.FindControl("ddlExamMarkingSchemeID")).SelectedValue.ToInt();
				var newExamMarksGradingSchemeID = ((AspireDropDownList)gvRow.FindControl("ddlExamMarksGradingSchemeID")).SelectedValue.ToInt();

				if (oldExamMarkingSchemeID != newExamMarkingSchemeID || oldExamMarksGradingSchemeID != newExamMarksGradingSchemeID)
					admissionOpenPrograms.Add(new ManageMarkingAndGradingSchemeAssignment.AdmissionOpenProgram
					{
						AdmissionOpenProgramID = admissionOpenProgramID,
						OldExamMarkingSchemeID = oldExamMarkingSchemeID,
						NewExamMarkingSchemeID = newExamMarkingSchemeID,
						OldExamMarksGradingSchemeID = oldExamMarksGradingSchemeID,
						NewExamMarksGradingSchemeID = newExamMarksGradingSchemeID,
					});
			}

			if (!admissionOpenPrograms.Any())
				this.AddInfoAlert("No changes found.");
			else
			{
				var updated = ManageMarkingAndGradingSchemeAssignment.UpdateMarksAndGradingSchemes(admissionOpenPrograms, this.AdminIdentity.LoginSessionGuid);
				switch (updated)
				{
					case 0:
						this.AddInfoAlert("No record updated.");
						break;
					case 1:
						this.AddSuccessAlert($"{updated} record updated.");
						break;
					default:
						this.AddSuccessAlert($"{updated} records updated.");
						break;
				}
			}
			this.GetData(this.gvAdmissionOpenPrograms.PageIndex);
		}
	}
}