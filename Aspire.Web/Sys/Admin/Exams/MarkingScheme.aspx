﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="MarkingScheme.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Exams.MarkingScheme" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<asp:Panel CssClass="form-horizontal" runat="server" DefaultButton="btnSave">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" CssClass="col-md-2" />
					<div class="col-md-7">
						<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" />
					</div>
					<div class="col-md-3 form-control-static">
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlInstituteID" ErrorMessage="This field is required." ValidationGroup="Scheme" />
					</div>
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Scheme Name:" AssociatedControlID="tbMarkingSchemeName" CssClass="col-md-2" />
					<div class="col-md-7">
						<aspire:AspireTextBox runat="server" MaxLength="100" ID="tbMarkingSchemeName" ValidationGroup="Scheme" />
					</div>
					<div class="col-md-3 form-control-static">
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbMarkingSchemeName" ErrorMessage="This field is required." ValidationGroup="Scheme" />
					</div>
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlMarkingSchemeType" CssClass="col-md-2" />
					<div class="col-md-7">
						<aspire:AspireDropDownList runat="server" ID="ddlMarkingSchemeType" />
					</div>
					<div class="col-md-3 form-control-static">
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlMarkingSchemeType" ErrorMessage="This field is required." ValidationGroup="Scheme" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-offset-2 col-md-7">
						<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" ValidationGroup="Scheme" OnClick="btnSave_OnClick" />
						<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="~/Sys/Admin/Exams/MarkingSchemes.aspx" />
					</div>
				</div>
			</asp:Panel>
		</div>
	</div>
	<asp:Panel runat="server" ID="panelExamMarkingScheme">
		<div>
			<aspire:AspireButton runat="server" CausesValidation="False" ID="btnAddDetail" Text="Add" Glyphicon="plus" OnClick="btnAddDetail_OnClick" />
		</div>
		<aspire:AspireGridView runat="server" ID="gvExamMarkingSchemeDetails" AutoGenerateColumns="False" OnRowCommand="gvExamMarkingSchemeDetails_OnRowCommand" Caption="Details">
			<Columns>
				<asp:BoundField HeaderText="Category" DataField="CourseCategoryFullName" />
				<asp:BoundField HeaderText="Category Total" DataField="CourseCategoryTotal" />
				<asp:BoundField HeaderText="Exam Marks Type" DataField="ExamMarksTypeFullName" />
				<asp:BoundField HeaderText="Minimum Marks" DataField="MinMarks" />
				<asp:BoundField HeaderText="Maximum Marks" DataField="MaxMarks" />
				<asp:BoundField HeaderText="Weightage" DataField="ExamMarksTypeWeightage" DataFormatString="{0}%" />
				<asp:TemplateField>
					<ItemTemplate>
						<aspire:AspireLinkButton ToolTip="Edit" CausesValidation="False" runat="server" CommandName="Edit" Glyphicon="edit" EncryptedCommandArgument='<%# Eval("ExamMarkingSchemeDetailID") %>' />
						<aspire:AspireLinkButton ToolTip="Delete" CausesValidation="False" runat="server" CommandName="Delete" Glyphicon="remove" EncryptedCommandArgument='<%# Eval("ExamMarkingSchemeDetailID") %>' ConfirmMessage="Are you sure you want to delete?" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
		<aspire:AspireModal runat="server" ID="modalExamMarkingScheme" ModalSize="None">
			<BodyTemplate>
				<asp:UpdatePanel runat="server" ID="updatePanelExamMarkingScheme" UpdateMode="Conditional">
					<ContentTemplate>
						<aspire:AspireAlert runat="server" ID="alertModal" />
						<div class="form-horizontal">
							<div class="form-group">
								<aspire:AspireLabel runat="server" CssClass="col-md-3" Text="Marks Type:" AssociatedControlID="ddlExamMarksType" />
								<div class="col-md-9">
									<aspire:AspireDropDownList runat="server" ID="ddlExamMarksType" />
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamMarksType" ErrorMessage="This field is required." ValidationGroup="Detail" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" CssClass="col-md-3" Text="Course Category:" AssociatedControlID="ddlCourseCategory" />
								<div class="col-md-9">
									<aspire:AspireDropDownList runat="server" ID="ddlCourseCategory" />
									<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlCourseCategory" ErrorMessage="This field is required." ValidationGroup="Detail" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" CssClass="col-md-3" Text="Minimum Marks:" AssociatedControlID="tbMinMarks" />
								<div class="col-md-9">
									<aspire:AspireTextBox data-inputmask="'alias': 'marks'" runat="server" ID="tbMinMarks" ValidationGroup="Detail" MaxLength="8" />
									<aspire:AspireDoubleValidator runat="server" AllowNull="False" RequiredErrorMessage="This field is required." ControlToValidate="tbMinMarks" InvalidRangeErrorMessage="Marks Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." ValidationGroup="Detail" MinValue="0" MaxValue="9999.999" />
								</div>
							</div>
							<div class="form-group">
								<aspire:AspireLabel runat="server" CssClass="col-md-3" Text="Maximum Marks:" AssociatedControlID="tbMaxMarks" />
								<div class="col-md-9">
									<aspire:AspireTextBox data-inputmask="'alias': 'marks'" runat="server" ID="tbMaxMarks" ValidationGroup="Detail" MaxLength="8" />
									<aspire:AspireDoubleValidator runat="server" AllowNull="False" RequiredErrorMessage="This field is required." ControlToValidate="tbMaxMarks" InvalidRangeErrorMessage="Marks Valid Range is {min} to {max}" InvalidNumberErrorMessage="Invalid Number." ValidationGroup="Detail" MinValue="0.001" MaxValue="9999.999" />
									<aspire:AspireCompareValidator Type="Double" runat="server" ControlToValidate="tbMaxMarks" ControlToCompare="tbMinMarks" ErrorMessage="Maximum Marks must be greater than or equal to Minimum Marks." ValidateEmptyText="False" Operator="GreaterThanEqual" ValidationGroup="Detail" />
								</div>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</BodyTemplate>
			<FooterTemplate>
				<asp:UpdatePanel runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<aspire:AspireButton runat="server" ID="btnSaveDetail" ButtonType="Primary" Text="Add" OnClick="btnSaveDetail_OnClick" ValidationGroup="Detail" />
						<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
					</ContentTemplate>
				</asp:UpdatePanel>
			</FooterTemplate>
		</aspire:AspireModal>
	</asp:Panel>
</asp:Content>
