﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="MarksGradingScheme.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Exams.MarksGradingScheme" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstitute" CssClass="col-md-2" />
					<div class="col-md-6">
						<aspire:AspireDropDownList runat="server" ID="ddlInstitute" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlInstitute" ErrorMessage="This field is required." ValidationGroup="Scheme" />
					</div>
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Grading Scheme Name:" AssociatedControlID="tbGradingSchemeName" CssClass="col-md-2" />
					<div class="col-md-6">
						<aspire:AspireTextBox runat="server" ID="tbGradingSchemeName" ValidationGroup="Scheme" />
						<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbGradingSchemeName" ErrorMessage="This field is required." ValidationGroup="Scheme" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-offset-2 col-md-2">
						<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ButtonType="Primary" ValidationGroup="Scheme" OnClick="btnSave_OnClick" />
						<aspire:AspireHyperLink runat="server" Text="Cancel" NavigateUrl="~/Sys/Admin/Exams/MarksGradingSchemes.aspx" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<asp:Panel runat="server" ID="panelExamMgsRanges">
		<div class="form-inline">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Regular Semester:" AssociatedControlID="ddlRegularSemester" />
				<aspire:AspireDropDownList runat="server" ID="ddlRegularSemester" ValidationGroup="SchemeRange" />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Grade:" AssociatedControlID="ddlExamGrades" />
				<aspire:AspireDropDownList runat="server" ID="ddlExamGrades" />
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamGrades" ErrorMessage="This field is required." ValidationGroup="SchemeRange" />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Min Marks:" AssociatedControlID="tbMinMarks" />
				<aspire:AspireTextBox runat="server" ID="tbMinMarks" />
				<aspire:AspireInt32Validator runat="server" ControlToValidate="tbMinMarks" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid number." AllowNull="False" ValidationGroup="SchemeRange" />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Max Marks:" AssociatedControlID="tbMaxMarks" />
				<aspire:AspireTextBox runat="server" ID="tbMaxMarks" />
				<aspire:AspireInt32Validator runat="server" ControlToValidate="tbMaxMarks" RequiredErrorMessage="This field is required." InvalidNumberErrorMessage="Invalid number." AllowNull="False" ValidationGroup="SchemeRange" />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Grade Points:" AssociatedControlID="ddlGradePoints" />
				<aspire:AspireDropDownList runat="server" ID="ddlGradePoints">
					<Items>
						<asp:ListItem Text="4.00" />
						<asp:ListItem Text="3.67" />
						<asp:ListItem Text="3.50" />
						<asp:ListItem Text="3.33" />
						<asp:ListItem Text="3.00" />
						<asp:ListItem Text="2.67" />
						<asp:ListItem Text="2.50" />
						<asp:ListItem Text="2.33" />
						<asp:ListItem Text="2.00" />
						<asp:ListItem Text="1.67" />
						<asp:ListItem Text="1.50" />
						<asp:ListItem Text="1.33" />
						<asp:ListItem Text="1.00" />
						<asp:ListItem Text="0" />
					</Items>
				</aspire:AspireDropDownList>
				<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlGradePoints" ErrorMessage="This field is required." ValidationGroup="SchemeRange" />
			</div>
			<div class="form-group">
				<aspire:AspireButton runat="server" ID="btnAdd" ButtonType="Primary" Text="Add" OnClick="btnAdd_OnClick" ValidationGroup="SchemeRange" />
			</div>
		</div>
		<p></p>
		<aspire:AspireGridView runat="server" ID="gvExamMgsRanges" AutoGenerateColumns="False" OnRowCommand="gvExamMgsRanges_OnRowCommand" Caption="Marks Grading Scheme Ranges">
			<Columns>
				<asp:BoundField HeaderText="Regular Semester" DataField="RegularSemesterYesNo" />
				<asp:BoundField HeaderText="Grade" DataField="ExamGradesFullName" />
				<asp:BoundField HeaderText="Minimum Marks" DataField="MinMarks" />
				<asp:BoundField HeaderText="Maximum Marks" DataField="MaxMarks" />
				<asp:BoundField HeaderText="Grade Points" DataField="GradePoints" DataFormatString="{0:N2}" />
				<asp:TemplateField>
					<ItemTemplate>
						<aspire:AspireLinkButton runat="server" ToolTip="Delete" CommandName="Delete" Glyphicon="remove" EncryptedCommandArgument='<%# Eval("ExamMarksGradingSchemesGradeRangeID") %>' ConfirmMessage="Are you sure you want to delete?" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</aspire:AspireGridView>
	</asp:Panel>
</asp:Content>
