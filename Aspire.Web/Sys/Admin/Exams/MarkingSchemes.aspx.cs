﻿using Aspire.BL.Core.Exams.Admin;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Exams
{
	[AspirePage("BD6F3096-E60B-4C05-A486-AE25853104A4", UserTypes.Admins, "~/Sys/Admin/Exams/MarkingSchemes.aspx", "Marking Schemes", true, AspireModules.Exams)]
	public partial class MarkingSchemes : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.Exams.ManageExamMarkingSchemes, new []{UserGroupPermission.PermissionValues.Allowed, }}
		};

		public override PageIcon PageIcon => AspireGlyphicons.cog.GetIcon();
		public override string PageTitle => "Marking Schemes";

		public static string GetPageUrl(int? instituteID, SortDirection? sortDirection, string sortExpression, int? pageSize)
		{
			return typeof(MarkingSchemes).GetAspirePageAttribute().PageUrl.AttachQueryParams("InstituteID", instituteID, "SortDirection", sortDirection, "SortExpression", sortExpression, "PageSize", pageSize);
		}

		public static void Redirect(int? instituteID, SortDirection? sortDirection, string sortExpression, int? pageSize)
		{
			Redirect(GetPageUrl(instituteID, sortDirection, sortExpression, pageSize));
		}

		private void RefreshPage()
		{
			Redirect(this.ddlInstituteID.SelectedValue.ToNullableInt(), this.ViewState.GetSortDirection(), this.ViewState.GetSortExpression(), this.gvExamMarkingSchemes.PageSize);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(this.Request.GetParameterValue("SortExpression") ?? "MarkingSchemeName", this.Request.GetParameterValue<SortDirection>("SortDirection") ?? SortDirection.Ascending);
				this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.All).SetSelectedValueIfNotPostback("InstituteID");
				var pageSize = this.Request.GetParameterValue<int>("PageSize");
				if (pageSize != null)
					this.gvExamMarkingSchemes.PageSize = pageSize.Value;
				this.ddlInstituteID_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlInstituteID_OnSelectedIndexChanged(object o, EventArgs e)
		{
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			var result = ExamMarkingSchemes.GetExamMarkingSchemes(instituteID, pageIndex, this.gvExamMarkingSchemes.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), this.AdminIdentity.LoginSessionGuid);
			this.gvExamMarkingSchemes.DataBind(result.Schemes, pageIndex, result.VirtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		protected void gvExamMarkingSchemes_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvExamMarkingSchemes_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvExamMarkingSchemes.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvExamMarkingSchemes_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvExamMarkingSchemes_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					var examMarkingSchemeID = e.DecryptedCommandArgumentToInt();
					var status = ExamMarkingSchemes.DeleteExamMarkingScheme(examMarkingSchemeID, this.AdminIdentity.LoginSessionGuid);
					switch (status)
					{
						case ExamMarkingSchemes.DeleteExamMarkingSchemeStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamMarkingSchemes.DeleteExamMarkingSchemeStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Exam Marking Scheme");
							break;
						case ExamMarkingSchemes.DeleteExamMarkingSchemeStatuses.ChildRecordExistsAdmissionOpenPrograms:
						case ExamMarkingSchemes.DeleteExamMarkingSchemeStatuses.ChildRecordExistsExamMarkingSchemeDetails:
						case ExamMarkingSchemes.DeleteExamMarkingSchemeStatuses.ChildRecordExistsPrograms:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Exam Marking Scheme");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.RefreshPage();
					break;
			}
		}
	}
}