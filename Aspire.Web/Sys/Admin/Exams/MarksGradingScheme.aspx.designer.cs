﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Web.Sys.Admin.Exams {
    
    
    public partial class MarksGradingScheme {
        
        /// <summary>
        /// ddlInstitute control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireDropDownList ddlInstitute;
        
        /// <summary>
        /// tbGradingSchemeName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireTextBox tbGradingSchemeName;
        
        /// <summary>
        /// btnSave control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireButton btnSave;
        
        /// <summary>
        /// panelExamMgsRanges control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel panelExamMgsRanges;
        
        /// <summary>
        /// ddlRegularSemester control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireDropDownList ddlRegularSemester;
        
        /// <summary>
        /// ddlExamGrades control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireDropDownList ddlExamGrades;
        
        /// <summary>
        /// tbMinMarks control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireTextBox tbMinMarks;
        
        /// <summary>
        /// tbMaxMarks control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireTextBox tbMaxMarks;
        
        /// <summary>
        /// ddlGradePoints control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireDropDownList ddlGradePoints;
        
        /// <summary>
        /// btnAdd control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireButton btnAdd;
        
        /// <summary>
        /// gvExamMgsRanges control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Aspire.Lib.WebControls.AspireGridView gvExamMgsRanges;
    }
}
