﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ExamMarksPolicy.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Exams.ExamMarksPolicy" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-9">
			<aspire:AspireLabel runat="server" Text="" AssociatedControlID="btnAddExamMarksPolicy" />
			<div>
				<aspire:AspireButton ID="btnAddExamMarksPolicy" runat="server" CausesValidation="False" Glyphicon="plus" ButtonType="Success" Text="Add Exam Marks Policy" OnClick="btnAddExamMarksPolicy_OnClick" />
			</div>
		</div>
		<div class="col-md-3">
			<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlInstituteIDFilter" OnSelectedIndexChanged="ddlInstituteIDFilter_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" AutoGenerateColumns="False" AllowSorting="True" ID="gvExamMarksPolicies" ItemType="Aspire.Model.Entities.ExamMarksPolicy" OnSorting="gvExamMarksPolicies_OnSorting" OnPageIndexChanging="gvExamMarksPolicies_OnPageIndexChanging" OnPageSizeChanging="gvExamMarksPolicies_OnPageSizeChanging" OnRowCommand="gvExamMarksPolicies_OnRowCommand">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%# Container.DataItemIndex+1 %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="Institute.InstituteAlias" HeaderText="Institute" SortExpression="InstituteAlias" />
			<asp:BoundField DataField="ExamMarksPolicyName" HeaderText="Policy Name" SortExpression="ExamMarksPolicyName" />
			<asp:BoundField DataField="ExamMarksPolicyDescription" HeaderText="Policy Description" SortExpression="ExamMarksPolicyDescription" />
			<asp:BoundField DataField="Summary" HeaderText="Summary" />
			<asp:BoundField DataField="Validity" HeaderText="Validity" SortExpression="ValidFromSemesterID" />
			<asp:BoundField DataField="ChecksumMD5" HeaderText="Checksum MD5" />
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" EncryptedCommandArgument="<%# Item.ExamMarksPolicyID %>" Glyphicon="edit" CommandName="Edit" ToolTip="Edit" CausesValidation="False" />
					<aspire:AspireLinkButton runat="server" EncryptedCommandArgument="<%# Item.ExamMarksPolicyID %>" FontAwesomeIcon="solid_copy" CommandName="Copy" ToolTip="Copy" CausesValidation="False" />
					<aspire:AspireLinkButton runat="server" EncryptedCommandArgument="<%# Item.ExamMarksPolicyID %>" Glyphicon="remove" CommandName="Delete" ToolTip="Delete" ConfirmMessage="Are you sure you want to delete this record?" CausesValidation="False" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<aspire:AspireModal runat="server" ID="modalPolicy" ModalSize="Large">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelPolicy" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertPolicy" />
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
						<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" ValidationGroup="SavePolicy" />
						<aspire:AspireRequiredFieldValidator ValidationGroup="SavePolicy" runat="server" ControlToValidate="ddlInstituteID" ErrorMessage="This field is required." />
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Policy Name:" AssociatedControlID="tbExamMarksPolicyName" />
						<aspire:AspireTextBox runat="server" ID="tbExamMarksPolicyName" MaxLength="500" ValidationGroup="SavePolicy" />
						<aspire:AspireStringValidator ValidationGroup="SavePolicy" ControlToValidate="tbExamMarksPolicyName" runat="server" AllowNull="True" RequiredErrorMessage="This field is required." />
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Valid From:" AssociatedControlID="ddlValidFromSemesterID" />
								<aspire:AspireDropDownList runat="server" ID="ddlValidFromSemesterID" ValidationGroup="SavePolicy" />
								<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="SavePolicy" ControlToValidate="ddlValidFromSemesterID" ErrorMessage="This field is required." />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Valid Up To:" AssociatedControlID="ddlValidUpToSemesterID" />
								<aspire:AspireDropDownList runat="server" ID="ddlValidUpToSemesterID" ValidationGroup="SavePolicy" />
								<aspire:AspireCompareValidator runat="server" ControlToValidate="ddlValidUpToSemesterID" ControlToCompare="ddlValidFromSemesterID" Operator="GreaterThanEqual" Type="Integer" ErrorMessage="Valid Up To Semester must be greater than or equal to Valid From Semester." ValidationGroup="SavePolicy" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<aspire:AspireLabel runat="server" Text="Description:" AssociatedControlID="tbExamMarksPolicyDescription" />
						<aspire:AspireTextBox runat="server" TextMode="MultiLine" ID="tbExamMarksPolicyDescription" MaxLength="1000" ValidationGroup="SavePolicy" />
						<aspire:AspireStringValidator ControlToValidate="tbExamMarksPolicyDescription" ValidationGroup="SavePolicy" runat="server" AllowNull="False" RequiredErrorMessage="This field is required." />
					</div>
					<hr />
					<div class="row">
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Marks (A):" AssociatedControlID="ddlAMarks" />
							<aspire:AspireDropDownList runat="server" ID="ddlAMarks" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Grade Points (A):" AssociatedControlID="ddlAGradePoints" />
							<aspire:AspireDropDownList runat="server" ID="ddlAGradePoints" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Marks (A-):" AssociatedControlID="ddlAMinusMarks" />
							<aspire:AspireDropDownList runat="server" ID="ddlAMinusMarks" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Grade Points (A-):" AssociatedControlID="ddlAMinusGradePoints" />
							<aspire:AspireDropDownList runat="server" ID="ddlAMinusGradePoints" ValidationGroup="SavePolicy" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Marks (B+):" AssociatedControlID="ddlBPlusMarks" />
							<aspire:AspireDropDownList runat="server" ID="ddlBPlusMarks" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Grade Points (B+):" AssociatedControlID="ddlBPlusGradePoints" />
							<aspire:AspireDropDownList runat="server" ID="ddlBPlusGradePoints" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Marks (B):" AssociatedControlID="ddlBMarks" />
							<aspire:AspireDropDownList runat="server" ID="ddlBMarks" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Grade Points (B):" AssociatedControlID="ddlBGradePoints" />
							<aspire:AspireDropDownList runat="server" ID="ddlBGradePoints" ValidationGroup="SavePolicy" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Marks (B-):" AssociatedControlID="ddlBMinusMarks" />
							<aspire:AspireDropDownList runat="server" ID="ddlBMinusMarks" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Grade Points (B-):" AssociatedControlID="ddlBMinusGradePoints" />
							<aspire:AspireDropDownList runat="server" ID="ddlBMinusGradePoints" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Marks (C+):" AssociatedControlID="ddlCPlusMarks" />
							<aspire:AspireDropDownList runat="server" ID="ddlCPlusMarks" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Grade Points (C+):" AssociatedControlID="ddlCPlusGradePoints" />
							<aspire:AspireDropDownList runat="server" ID="ddlCPlusGradePoints" ValidationGroup="SavePolicy" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Marks (C):" AssociatedControlID="ddlCMarks" />
							<aspire:AspireDropDownList runat="server" ID="ddlCMarks" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Grade Points (C):" AssociatedControlID="ddlCGradePoints" />
							<aspire:AspireDropDownList runat="server" ID="ddlCGradePoints" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Marks (C-):" AssociatedControlID="ddlCMinusMarks" />
							<aspire:AspireDropDownList runat="server" ID="ddlCMinusMarks" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Grade Points (C-):" AssociatedControlID="ddlCMinusGradePoints" />
							<aspire:AspireDropDownList runat="server" ID="ddlCMinusGradePoints" ValidationGroup="SavePolicy" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Marks (D+):" AssociatedControlID="ddlDPlusMarks" />
							<aspire:AspireDropDownList runat="server" ID="ddlDPlusMarks" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Grade Points (D+):" AssociatedControlID="ddlDPlusGradePoints" />
							<aspire:AspireDropDownList runat="server" ID="ddlDPlusGradePoints" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Marks (D):" AssociatedControlID="ddlDMarks" />
							<aspire:AspireDropDownList runat="server" ID="ddlDMarks" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Grade Points (D):" AssociatedControlID="ddlDGradePoints" />
							<aspire:AspireDropDownList runat="server" ID="ddlDGradePoints" ValidationGroup="SavePolicy" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Marks (F):" AssociatedControlID="ddlFMarks" />
							<aspire:AspireDropDownList runat="server" ID="ddlFMarks" ValidationGroup="SavePolicy" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlFMarks" ErrorMessage="This field is required." ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Grade Points (F):" AssociatedControlID="ddlFGradePoints" />
							<aspire:AspireDropDownList runat="server" ID="ddlFGradePoints" ValidationGroup="SavePolicy" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlFGradePoints" ErrorMessage="This field is required." ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Summer Grade Capping:" AssociatedControlID="ddlSummerGradeCapping" />
							<aspire:AspireDropDownList runat="server" ID="ddlSummerGradeCapping" ValidationGroup="SavePolicy" />
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-md-9 form-group">
							<aspire:AspireLabel runat="server" Text="Course Marks Entry Type:" AssociatedControlID="ddlCourseMarksEntryType" />
							<aspire:AspireDropDownList runat="server" ID="ddlCourseMarksEntryType" ValidationGroup="SavePolicy" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlCourseMarksEntryType" ErrorMessage="This field is required." ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Course Assignments:" AssociatedControlID="ddlCourseAssignments" />
							<aspire:AspireDropDownList runat="server" ID="ddlCourseAssignments" ValidationGroup="SavePolicy" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Course Quizzes:" AssociatedControlID="ddlCourseQuizzes" />
							<aspire:AspireDropDownList runat="server" ID="ddlCourseQuizzes" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Course Internals:" AssociatedControlID="ddlCourseInternals" />
							<aspire:AspireDropDownList runat="server" ID="ddlCourseInternals" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Course Mid:" AssociatedControlID="ddlCourseMid" />
							<aspire:AspireDropDownList runat="server" ID="ddlCourseMid" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Course Final:" AssociatedControlID="ddlCourseFinal" />
							<aspire:AspireDropDownList runat="server" ID="ddlCourseFinal" ValidationGroup="SavePolicy" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlCourseFinal" ErrorMessage="This field is required." ValidationGroup="SavePolicy" />
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-md-9 form-group">
							<aspire:AspireLabel runat="server" Text="Lab Marks Entry Type:" AssociatedControlID="ddlLabMarksEntryType" />
							<aspire:AspireDropDownList runat="server" ID="ddlLabMarksEntryType" ValidationGroup="SavePolicy" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlLabMarksEntryType" ErrorMessage="This field is required." ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Lab Assignments:" AssociatedControlID="ddlLabAssignments" />
							<aspire:AspireDropDownList runat="server" ID="ddlLabAssignments" ValidationGroup="SavePolicy" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Lab Quizzes:" AssociatedControlID="ddlLabQuizzes" />
							<aspire:AspireDropDownList runat="server" ID="ddlLabQuizzes" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Lab Internals:" AssociatedControlID="ddlLabInternals" />
							<aspire:AspireDropDownList runat="server" ID="ddlLabInternals" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Lab Mid:" AssociatedControlID="ddlLabMid" />
							<aspire:AspireDropDownList runat="server" ID="ddlLabMid" ValidationGroup="SavePolicy" />
						</div>
						<div class="col-md-3 form-group">
							<aspire:AspireLabel runat="server" Text="Lab Final:" AssociatedControlID="ddlLabFinal" />
							<aspire:AspireDropDownList runat="server" ID="ddlLabFinal" ValidationGroup="SavePolicy" />
							<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlLabFinal" ErrorMessage="This field is required." ValidationGroup="SavePolicy" />
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Always">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="SavePolicy" Text="Save" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
