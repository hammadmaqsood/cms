﻿using Aspire.BL.Core.Exams.Admin;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Exams
{
	[AspirePage("0F111FFA-AB4A-4376-9199-F8D4D45B58FB", UserTypes.Admins, "~/Sys/Admin/Exams/ControlPanel.aspx", "Control Panel", true, AspireModules.Exams)]
	public partial class ControlPanel : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.Exams.Module, new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.cog);
		public override string PageTitle => "Control Panel: Exam Marks";

		public static void Redirect(short? semesterID, int? instituteID, int? departmentID, int? programID, ExamMarksSetting.Statuses? statusEnum, DateTime? from, DateTime? to, SortDirection sortDirection, string sortExpression, int? pageSize)
		{
			Redirect<ControlPanel>("SemesterID", semesterID, "InstituteID", instituteID, "DepartmentID", departmentID, "ProgramID", programID, "Status", statusEnum, "From", from, "To", to, "SortDirection", sortDirection, "SortExpression", sortExpression, "PageSize", pageSize);
		}

		private void RefreshPage()
		{
			Redirect(this.ddlSemesterID.SelectedValue.ToNullableShort(), this.ddlInstituteID.SelectedValue.ToNullableInt(), this.ddlDepartmentID.SelectedValue.ToNullableInt(), this.ddlProgramID.SelectedValue.ToNullableInt(), this.ddlStatus.GetSelectedEnumValue<ExamMarksSetting.Statuses>(null), this.dtpFrom.SelectedDate, this.dtpTo.SelectedDate, this.ViewState.GetSortDirection(), this.ViewState.GetSortExpression(), this.gvControlPanel.PageSize);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Semester", SortDirection.Descending);
				this.ddlSemesterID.FillSemesters(CommonListItems.All).SetSelectedValueIfNotPostback("SemesterID");
				this.ddlInstituteID.FillInstitutesForAdmin(CommonListItems.All).SetSelectedValueIfNotPostback("InstituteID");
				this.ddlStatus.FillEnums<CourseRegistrationSetting.Statuses>(CommonListItems.All).SetSelectedValueIfNotPostback("Status");
				this.dtpFrom.SelectedDate = this.Request.GetParameterValue<DateTime>("From");
				this.dtpTo.SelectedDate = this.Request.GetParameterValue<DateTime>("To");
				this.ddlSemesterID_OnSelectedIndexChanged(null, null);
			}
		}

		#region Filters for Control Panel Record

		protected void btnAdd_OnClick(object sender, EventArgs e)
		{
			this.DisplayControlPanelModal(null);
		}

		protected void ddlSemesterID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlInstituteID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlInstituteID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			if (instituteID == null)
				this.ddlDepartmentID.DataBind(CommonListItems.All).SetSelectedValueIfNotPostback("DepartmentID");
			else
				this.ddlDepartmentID.FillDepartments(instituteID.Value, CommonListItems.All).SetSelectedValueIfNotPostback("DepartmentID");
			this.ddlDepartmentID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			if (instituteID == null)
				this.ddlProgramID.DataBind(CommonListItems.All).SetSelectedValueIfNotPostback("ProgramID");
			else
				this.ddlProgramID.FillPrograms(instituteID.Value, departmentID, true, CommonListItems.All).SetSelectedValueIfNotPostback("ProgramID");
			this.ddlProgramID_OnSelectedIndexChanged(null, null);
		}

		protected void ddlProgramID_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlStatus_OnSelectedIndexChanged(null, null);
		}

		protected void ddlStatus_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearch_OnClick(null, null);
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		#endregion

		#region Control Panel

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var examMarksTypesEnumFlags = this.cblExamMarksTypes.GetSelectedEnumValues<ExamMarksTypes>().Combine();
			if (examMarksTypesEnumFlags == null)
				throw new InvalidOperationException();
			var fromDate = this.dpFromDate.SelectedDate;
			var toDate = this.dpToDate.SelectedDate;
			if (fromDate == null || toDate == null)
				throw new InvalidOperationException();
			var statusEnum = this.rblStatus.GetSelectedEnumValue<ExamMarksSetting.Statuses>();
			var semesterID = this.ddlSemesterForSave.SelectedValue.ToShort();
			var instituteID = this.ddlInstituteForSave.SelectedValue.ToInt();
			var programIDs = this.cblPrograms.GetSelectedValues().Where(s => !string.IsNullOrWhiteSpace(s)).Select(s => s.ToInt()).ToList();
			var result = ExamMarksSettings.AddOrUpdateExamMarksSettingSettings(semesterID, instituteID, programIDs, examMarksTypesEnumFlags.Value, fromDate.Value, toDate.Value, statusEnum, this.AdminIdentity.LoginSessionGuid);
			switch (result.Status)
			{
				case ExamMarksSettings.AddOrUpdateExamMarksSettingSettingsResult.Statuses.None:
					throw new InvalidOperationException();
				case ExamMarksSettings.AddOrUpdateExamMarksSettingSettingsResult.Statuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					Redirect<ControlPanel>();
					return;
				case ExamMarksSettings.AddOrUpdateExamMarksSettingSettingsResult.Statuses.InvalidDateRange:
					this.alertModalControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Invalid From and To date range.");
					this.updateModalControlPanel.Update();
					return;
				case ExamMarksSettings.AddOrUpdateExamMarksSettingSettingsResult.Statuses.Success:
					if (result.SuccessAdd > 0)
						this.AddSuccessAlert(result.SuccessAdd + " out of " + result.TotalRecords + " record(s) Added", true);
					if (result.SuccessUpdate > 0)
						this.AddSuccessAlert(result.SuccessUpdate + " out of " + result.TotalRecords + " record(s) Updated", true);
					this.RefreshPage();
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected void gvControlPanel_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvControlPanel_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvControlPanel_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvControlPanel.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvControlPanel_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Status":
					var examMarksSetting = ExamMarksSettings.ToggleExamMarksSettingStatus(e.DecryptedCommandArgumentToInt(), this.AdminIdentity.LoginSessionGuid);
					if (examMarksSetting == null)
					{
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						return;
					}
					this.GetData(this.gvControlPanel.PageIndex);
					break;
				case "Edit":
					e.Handled = true;
					this.DisplayControlPanelModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var resultDelete = ExamMarksSettings.DeleteExamMarksSetting(e.DecryptedCommandArgumentToInt(), this.AdminIdentity.LoginSessionGuid);
					switch (resultDelete)
					{
						case ExamMarksSettings.DeleteExamMarksSettingStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamMarksSettings.DeleteExamMarksSettingStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Exam Marks Setting");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.RefreshPage();
					break;
			}
		}

		protected void gvControlPanel_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				var examMarksSetting = (ExamMarksSettings.ExamMarksSetting)e.Row.DataItem;
				var fromDate = examMarksSetting.FromDate.Date;
				var toDate = examMarksSetting.ToDate.Date;
				if (DateTime.Today > toDate)
					e.Row.CssClass = "danger";
				else if (DateTime.Today >= fromDate)
					e.Row.CssClass = examMarksSetting.StatusEnum == ExamMarksSetting.Statuses.Inactive
						? "warning"
						: "success";
			}
		}

		protected void ddlInstituteForSave_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteForSave.SelectedValue.ToInt();
			this.ddlDepartmentIDForSave.FillDepartments(instituteID, CommonListItems.All);
			this.ddlDepartmentIDForSave_OnSelectedIndexChanged(null, null);
		}

		protected void ddlDepartmentIDForSave_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteForSave.SelectedValue.ToInt();
			var departmentID = this.ddlDepartmentIDForSave.SelectedValue.ToNullableInt();
			if (departmentID == null)
				this.cblPrograms.FillProgramsWithDepartment(instituteID, null, true, CommonListItems.All);
			else
				this.cblPrograms.FillPrograms(instituteID, departmentID.Value, true, CommonListItems.All);
		}

		#endregion

		#region Page Specific Functions

		private void GetData(int pageIndex)
		{
			if (!this.IsPostBack)
			{
				var pageSize = this.Request.GetParameterValue<int>("PageSize");
				if (pageSize != null)
					this.gvControlPanel.PageSize = pageSize.Value;
			}
			var semesterID = this.ddlSemesterID.SelectedValue.ToNullableShort();
			var instituteID = this.ddlInstituteID.SelectedValue.ToNullableInt();
			var departmentID = this.ddlDepartmentID.SelectedValue.ToNullableInt();
			var programID = this.ddlProgramID.SelectedValue.ToNullableInt();
			var statusEnum = this.ddlStatus.GetSelectedEnumValue<ExamMarksSetting.Statuses>(null);
			var fromDate = this.dtpFrom.SelectedDate;
			var toDate = this.dtpTo.SelectedDate;

			var examMarkSettings = ExamMarksSettings.GetExamMarksSettings(semesterID, instituteID, departmentID, programID, statusEnum, fromDate, toDate, pageIndex, this.gvControlPanel.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out var virtualItemCount, this.AdminIdentity.LoginSessionGuid);
			this.gvControlPanel.DataBind(examMarkSettings, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		private void DisplayControlPanelModal(int? examMarksSettingID)
		{
			this.ddlSemesterForSave.FillSemesters();
			this.ddlInstituteForSave.FillInstitutesForAdmin();

			this.modalControlPanel.HeaderText = "Add/Edit";
			this.cblExamMarksTypes.FillEnums<ExamMarksTypes>();
			this.rblStatus.FillEnums<ExamMarksSetting.Statuses>();

			if (examMarksSettingID == null)
			{
				this.ddlSemesterForSave.ClearSelection();
				this.cblExamMarksTypes.ClearSelection();
				this.dpFromDate.SelectedDate = null;
				this.dpToDate.SelectedDate = null;
				this.rblStatus.SetEnumValue(ExamMarksSetting.Statuses.Active);
				this.ddlInstituteForSave.ClearSelection();
				this.ddlInstituteForSave_OnSelectedIndexChanged(null, null);
			}
			else
			{
				var examMarksSetting = ExamMarksSettings.GetExamMarksSetting(examMarksSettingID.Value, this.AdminIdentity.LoginSessionGuid);
				if (examMarksSetting == null)
				{
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				}
				this.ddlSemesterForSave.SelectedValue = examMarksSetting.SemesterID.ToString();
				var examMarksTypes = examMarksSetting.ExamMarksTypesEnum.GetFlags().ToArray();
				foreach (ListItem item in this.cblExamMarksTypes.Items)
					item.Selected = examMarksTypes.Contains((ExamMarksTypes)item.Value.ToInt());
				this.dpFromDate.SelectedDate = examMarksSetting.FromDate;
				this.dpToDate.SelectedDate = examMarksSetting.ToDate;
				this.rblStatus.SetEnumValue(examMarksSetting.StatusEnum);
				this.ddlInstituteForSave.SelectedValue = examMarksSetting.Program.InstituteID.ToString();
				this.ddlInstituteForSave_OnSelectedIndexChanged(null, null);
				this.ddlDepartmentIDForSave.SelectedValue = examMarksSetting.Program.DepartmentID.ToString();
				this.ddlDepartmentIDForSave_OnSelectedIndexChanged(null, null);
				this.cblPrograms.SelectedValue = examMarksSetting.ProgramID.ToString();
			}
			this.modalControlPanel.Show();
		}

		#endregion
	}
}