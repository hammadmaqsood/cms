﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Exams
{
	[AspirePage("B393EEEB-6F78-4BE6-BC99-0CD434988475", UserTypes.Admins, "~/Sys/Admin/Exams/ExamMarksPolicy.aspx", "Exam Marks Policy", true, AspireModules.Exams)]
	public partial class ExamMarksPolicy : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.Exams.Module, new[] {UserGroupPermission.PermissionValues.Allowed,}}
		};

		public override PageIcon PageIcon => AspireGlyphicons.book.GetIcon();
		public override string PageTitle => "Exam Marks Policy";

		public static string GetPageUrl(int? instituteID, int? pageIndex, int? pageSize, SortDirection sortDirection, string sortExpression)
		{
			return GetPageUrl<ExamMarksPolicy>().AttachQueryParams("InstituteID", instituteID, "PageIndex", pageIndex, "PageSize", pageSize, "SortDirection", sortDirection, "SortExpression", sortExpression);
		}

		public static void Redirect(int? instituteID, int? pageIndex, int? pageSize, SortDirection sortDirection, string sortExpression)
		{
			Redirect(GetPageUrl(instituteID, pageIndex, pageSize, sortDirection, sortExpression));
		}

		private void RefreshPage()
		{
			Redirect(this.ddlInstituteIDFilter.SelectedValue.ToNullableInt(), this.gvExamMarksPolicies.PageIndex, this.gvExamMarksPolicies.PageSize, this.ViewState.GetSortDirection(), this.ViewState.GetSortExpression());
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(this.Request.GetParameterValue("SortExpression") ?? nameof(Model.Entities.ExamMarksPolicy.ExamMarksPolicyName), this.Request.GetParameterValue<SortDirection>("SortDirection") ?? SortDirection.Ascending);
				this.ddlInstituteIDFilter.FillInstitutesForAdmin(CommonListItems.All).SetSelectedValueIfNotPostback("InstituteID");
				this.ddlInstituteIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlInstituteIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void gvExamMarksPolicies_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvExamMarksPolicies_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvExamMarksPolicies_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvExamMarksPolicies.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvExamMarksPolicies_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplayPolicy(e.DecryptedCommandArgumentToInt(), false);
					break;
				case "Copy":
					this.DisplayPolicy(e.DecryptedCommandArgumentToInt(), true);
					break;
				case "Delete":
					var status = BL.Core.Exams.Admin.ExamMarksPolicy.DeleteExamMarksPolicy(e.DecryptedCommandArgumentToInt(), this.AspireIdentity.LoginSessionGuid);
					switch (status)
					{
						case BL.Core.Exams.Admin.ExamMarksPolicy.DeleteExamMarksPolicyStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Exam Marks Policy");
							this.RefreshPage();
							return;
						case BL.Core.Exams.Admin.ExamMarksPolicy.DeleteExamMarksPolicyStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.RefreshPage();
							return;
						case BL.Core.Exams.Admin.ExamMarksPolicy.DeleteExamMarksPolicyStatuses.ChildRecordExists:
							this.AddErrorAlert("Exam Marks Policy can not be deleted because it has been attached to Programs.");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}

					break;
			}
		}

		private void GetData(int pageIndex)
		{
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToNullableInt();
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var data = BL.Core.Exams.Admin.ExamMarksPolicy.GetExamMarksPolicies(instituteID, pageIndex, this.gvExamMarksPolicies.PageSize, sortExpression, sortDirection, out var virtualItemCount, this.AspireIdentity.LoginSessionGuid);
			this.gvExamMarksPolicies.DataBind(data, pageIndex, virtualItemCount, sortExpression, sortDirection);
		}

		protected void btnAddExamMarksPolicy_OnClick(object sender, EventArgs e)
		{
			this.DisplayPolicy(null, false);
		}

		private void DisplayPolicy(int? examMarksPolicyID, bool copy)
		{
			this.ddlInstituteID.FillInstitutesForAdmin();

			this.ddlAMarks.DataBind(MarksRanges);
			this.ddlAMinusMarks.DataBind(MarksRanges);
			this.ddlBPlusMarks.DataBind(MarksRanges);
			this.ddlBMarks.DataBind(MarksRanges);
			this.ddlBMinusMarks.DataBind(MarksRanges);
			this.ddlCPlusMarks.DataBind(MarksRanges);
			this.ddlCMarks.DataBind(MarksRanges);
			this.ddlCMinusMarks.DataBind(MarksRanges);
			this.ddlDPlusMarks.DataBind(MarksRanges);
			this.ddlDMarks.DataBind(MarksRanges);
			this.ddlFMarks.DataBind(MarksRanges);
			this.ddlAGradePoints.DataBind(GradePoints);
			this.ddlAMinusGradePoints.DataBind(GradePoints);
			this.ddlBPlusGradePoints.DataBind(GradePoints);
			this.ddlBGradePoints.DataBind(GradePoints);
			this.ddlBMinusGradePoints.DataBind(GradePoints);
			this.ddlCPlusGradePoints.DataBind(GradePoints);
			this.ddlCGradePoints.DataBind(GradePoints);
			this.ddlCMinusGradePoints.DataBind(GradePoints);
			this.ddlDPlusGradePoints.DataBind(GradePoints);
			this.ddlDGradePoints.DataBind(GradePoints);
			this.ddlFGradePoints.DataBind(GradePoints);

			this.ddlCourseMarksEntryType.FillMarksEntryTypes();
			this.ddlCourseAssignments.DataBind(Marks);
			this.ddlCourseQuizzes.DataBind(Marks);
			this.ddlCourseInternals.DataBind(Marks);
			this.ddlCourseMid.DataBind(Marks);
			this.ddlCourseFinal.DataBind(Marks);
			this.ddlLabMarksEntryType.FillMarksEntryTypes();
			this.ddlLabAssignments.DataBind(Marks);
			this.ddlLabQuizzes.DataBind(Marks);
			this.ddlLabInternals.DataBind(Marks);
			this.ddlLabMid.DataBind(Marks);
			this.ddlLabFinal.DataBind(Marks);

			this.ddlSummerGradeCapping.FillExamGrades(CommonListItems.None, ExamGrades.F);

			this.ddlValidFromSemesterID.FillSemesters(CommonListItems.Select);
			this.ddlValidUpToSemesterID.FillSemesters(CommonListItems.Select);

			if (examMarksPolicyID == null)
			{
				if (copy)
					throw new ArgumentException("Must be false.", nameof(copy));
				this.ddlInstituteID.ClearSelection();
				this.ddlInstituteID.Enabled = true;
				this.tbExamMarksPolicyName.Text = null;
				this.ddlValidFromSemesterID.ClearSelection();
				this.ddlValidUpToSemesterID.ClearSelection();
				this.tbExamMarksPolicyDescription.Text = null;
				this.ddlAMarks.ClearSelection();
				this.ddlAMinusMarks.ClearSelection();
				this.ddlBPlusMarks.ClearSelection();
				this.ddlBMarks.ClearSelection();
				this.ddlBMinusMarks.ClearSelection();
				this.ddlCPlusMarks.ClearSelection();
				this.ddlCMarks.ClearSelection();
				this.ddlCMinusMarks.ClearSelection();
				this.ddlDPlusMarks.ClearSelection();
				this.ddlDMarks.ClearSelection();
				this.ddlFMarks.ClearSelection();

				this.ddlAGradePoints.ClearSelection();
				this.ddlAMinusGradePoints.ClearSelection();
				this.ddlBPlusGradePoints.ClearSelection();
				this.ddlBGradePoints.ClearSelection();
				this.ddlBMinusGradePoints.ClearSelection();
				this.ddlCPlusGradePoints.ClearSelection();
				this.ddlCGradePoints.ClearSelection();
				this.ddlCMinusGradePoints.ClearSelection();
				this.ddlDPlusGradePoints.ClearSelection();
				this.ddlDGradePoints.ClearSelection();
				this.ddlFGradePoints.ClearSelection();
				this.ddlSummerGradeCapping.ClearSelection();

				this.ddlCourseMarksEntryType.ClearSelection();
				this.ddlCourseAssignments.ClearSelection();
				this.ddlCourseQuizzes.ClearSelection();
				this.ddlCourseInternals.ClearSelection();
				this.ddlCourseMid.ClearSelection();
				this.ddlCourseFinal.ClearSelection();
				this.ddlLabMarksEntryType.ClearSelection();
				this.ddlLabAssignments.ClearSelection();
				this.ddlLabQuizzes.ClearSelection();
				this.ddlLabInternals.ClearSelection();
				this.ddlLabMid.ClearSelection();
				this.ddlLabFinal.ClearSelection();
				this.modalPolicy.HeaderText = "Add Exam Marks Policy";
			}
			else
			{
				var examMarksPolicy = BL.Core.Exams.Admin.ExamMarksPolicy.GetExamMarksPolicy(examMarksPolicyID.Value, this.AspireIdentity.LoginSessionGuid);
				if (examMarksPolicy == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<ExamMarksPolicy>();
					return;
				}

				this.ddlInstituteID.SelectedValue = examMarksPolicy.InstituteID.ToString();
				this.ddlInstituteID.Enabled = copy;
				this.tbExamMarksPolicyName.Text = examMarksPolicy.ExamMarksPolicyName + (copy ? " Copy" : null);
				this.ddlValidFromSemesterID.SelectedValue = examMarksPolicy.ValidFromSemesterID.ToString();
				this.ddlValidUpToSemesterID.SelectedValue = examMarksPolicy.ValidUpToSemesterID.ToString();
				this.tbExamMarksPolicyDescription.Text = examMarksPolicy.ExamMarksPolicyDescription;
				var getRange = new Func<byte?, byte?, string>(delegate (byte? min, byte? max)
				{
					if (min == null && max == null)
						return null;
					if (min != null && max != null)
						return $"{min:D2}-{max:D2}";
					throw new InvalidOperationException();
				});
				this.ddlAMarks.SelectedValue = getRange(examMarksPolicy.AMin, examMarksPolicy.AMax);
				this.ddlAMinusMarks.SelectedValue = getRange(examMarksPolicy.AMinusMin, examMarksPolicy.AMinusMax);
				this.ddlBPlusMarks.SelectedValue = getRange(examMarksPolicy.BPlusMin, examMarksPolicy.BPlusMax);
				this.ddlBMarks.SelectedValue = getRange(examMarksPolicy.BMin, examMarksPolicy.BMax);
				this.ddlBMinusMarks.SelectedValue = getRange(examMarksPolicy.BMinusMin, examMarksPolicy.BMinusMax);
				this.ddlCPlusMarks.SelectedValue = getRange(examMarksPolicy.CPlusMin, examMarksPolicy.CPlusMax);
				this.ddlCMarks.SelectedValue = getRange(examMarksPolicy.CMin, examMarksPolicy.CMax);
				this.ddlCMinusMarks.SelectedValue = getRange(examMarksPolicy.CMinusMin, examMarksPolicy.CMinusMax);
				this.ddlDPlusMarks.SelectedValue = getRange(examMarksPolicy.DPlusMin, examMarksPolicy.DPlusMax);
				this.ddlDMarks.SelectedValue = getRange(examMarksPolicy.DMin, examMarksPolicy.DMax);
				this.ddlFMarks.SelectedValue = getRange(0, examMarksPolicy.FMax);

				this.ddlAGradePoints.SelectedValue = examMarksPolicy.AGradePoint?.ToString("0.00");
				this.ddlAMinusGradePoints.SelectedValue = examMarksPolicy.AMinusGradePoint?.ToString("0.00");
				this.ddlBPlusGradePoints.SelectedValue = examMarksPolicy.BPlusGradePoint?.ToString("0.00");
				this.ddlBGradePoints.SelectedValue = examMarksPolicy.BGradePoint?.ToString("0.00");
				this.ddlBMinusGradePoints.SelectedValue = examMarksPolicy.BMinusGradePoint?.ToString("0.00");
				this.ddlCPlusGradePoints.SelectedValue = examMarksPolicy.CPlusGradePoint?.ToString("0.00");
				this.ddlCGradePoints.SelectedValue = examMarksPolicy.CGradePoint?.ToString("0.00");
				this.ddlCMinusGradePoints.SelectedValue = examMarksPolicy.CMinusGradePoint?.ToString("0.00");
				this.ddlDPlusGradePoints.SelectedValue = examMarksPolicy.DPlusGradePoint?.ToString("0.00");
				this.ddlDGradePoints.SelectedValue = examMarksPolicy.DGradePoint?.ToString("0.00");
				this.ddlFGradePoints.SelectedValue = examMarksPolicy.FGradePoint.ToString("0.00");
				this.ddlSummerGradeCapping.SetEnumValue(examMarksPolicy.SummerCappingExamGradeEnum);

				this.ddlCourseMarksEntryType.SetEnumValue(examMarksPolicy.CourseMarksEntryTypeEnum);
				this.ddlCourseAssignments.SelectedValue = examMarksPolicy.CourseAssignments.ToString();
				this.ddlCourseQuizzes.SelectedValue = examMarksPolicy.CourseQuizzes.ToString();
				this.ddlCourseInternals.SelectedValue = examMarksPolicy.CourseInternals.ToString();
				this.ddlCourseMid.SelectedValue = examMarksPolicy.CourseMid.ToString();
				this.ddlCourseFinal.SelectedValue = examMarksPolicy.CourseFinal.ToString();
				this.ddlLabMarksEntryType.SetEnumValue(examMarksPolicy.LabMarksEntryTypeEnum);
				this.ddlLabAssignments.SelectedValue = examMarksPolicy.LabAssignments.ToString();
				this.ddlLabQuizzes.SelectedValue = examMarksPolicy.LabQuizzes.ToString();
				this.ddlLabInternals.SelectedValue = examMarksPolicy.LabInternals.ToString();
				this.ddlLabMid.SelectedValue = examMarksPolicy.LabMid.ToString();
				this.ddlLabFinal.SelectedValue = examMarksPolicy.LabFinal.ToString();
				this.modalPolicy.HeaderText = copy ? "Add Exam Marks Policy" : "Edit Exam Marks Policy";
			}

			this.ViewState[nameof(Model.Entities.ExamMarksPolicy.ExamMarksPolicyID)] = copy ? null : examMarksPolicyID;
			this.modalPolicy.Show();
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var examMarksPolicyID = (int?)this.ViewState["ExamMarksPolicyID"];
			var examMarksPolicy = new Model.Entities.ExamMarksPolicy
			{
				ExamMarksPolicyID = examMarksPolicyID ?? default(int),
				InstituteID = this.ddlInstituteID.SelectedValue.ToInt(),
				ExamMarksPolicyName = this.tbExamMarksPolicyName.Text.TrimAndCannotBeEmpty(),
				ValidFromSemesterID = this.ddlValidFromSemesterID.SelectedValue.ToShort(),
				ValidUpToSemesterID = this.ddlValidUpToSemesterID.SelectedValue.ToNullableShort(),
				ExamMarksPolicyDescription = this.tbExamMarksPolicyDescription.Text.TrimAndMakeItNullIfEmpty(),
				CourseMarksEntryTypeEnum = this.ddlCourseMarksEntryType.GetSelectedEnumValue<Model.Entities.ExamMarksPolicy.MarksEntryTypes>(),
				CourseAssignments = this.ddlCourseAssignments.SelectedValue.ToNullableByte(),
				CourseQuizzes = this.ddlCourseQuizzes.SelectedValue.ToNullableByte(),
				CourseInternals = this.ddlCourseInternals.SelectedValue.ToNullableByte(),
				CourseMid = this.ddlCourseMid.SelectedValue.ToNullableByte(),
				CourseFinal = this.ddlCourseFinal.SelectedValue.ToByte(),
				LabMarksEntryTypeEnum = this.ddlLabMarksEntryType.GetSelectedEnumValue<Model.Entities.ExamMarksPolicy.MarksEntryTypes>(),
				LabAssignments = this.ddlLabAssignments.SelectedValue.ToNullableByte(),
				LabQuizzes = this.ddlLabQuizzes.SelectedValue.ToNullableByte(),
				LabInternals = this.ddlLabInternals.SelectedValue.ToNullableByte(),
				LabMid = this.ddlLabMid.SelectedValue.ToNullableByte(),
				LabFinal = this.ddlLabFinal.SelectedValue.ToByte(),
				AMin = GetRangeValue(this.ddlAMarks, 0),
				AMax = GetRangeValue(this.ddlAMarks, 1),
				AMinusMin = GetRangeValue(this.ddlAMinusMarks, 0),
				AMinusMax = GetRangeValue(this.ddlAMinusMarks, 1),
				BPlusMin = GetRangeValue(this.ddlBPlusMarks, 0),
				BPlusMax = GetRangeValue(this.ddlBPlusMarks, 1),
				BMin = GetRangeValue(this.ddlBMarks, 0),
				BMax = GetRangeValue(this.ddlBMarks, 1),
				BMinusMin = GetRangeValue(this.ddlBMinusMarks, 0),
				BMinusMax = GetRangeValue(this.ddlBMinusMarks, 1),
				CPlusMin = GetRangeValue(this.ddlCPlusMarks, 0),
				CPlusMax = GetRangeValue(this.ddlCPlusMarks, 1),
				CMin = GetRangeValue(this.ddlCMarks, 0),
				CMax = GetRangeValue(this.ddlCMarks, 1),
				CMinusMin = GetRangeValue(this.ddlCMinusMarks, 0),
				CMinusMax = GetRangeValue(this.ddlCMinusMarks, 1),
				DPlusMin = GetRangeValue(this.ddlDPlusMarks, 0),
				DPlusMax = GetRangeValue(this.ddlDPlusMarks, 1),
				DMin = GetRangeValue(this.ddlDMarks, 0),
				DMax = GetRangeValue(this.ddlDMarks, 1),
				FMax = GetRangeValue(this.ddlFMarks, 1) ?? throw new InvalidOperationException("F cannot be empty."),
				AGradePoint = this.ddlAGradePoints.SelectedValue.ToNullableDecimal(),
				AMinusGradePoint = this.ddlAMinusGradePoints.SelectedValue.ToNullableDecimal(),
				BPlusGradePoint = this.ddlBPlusGradePoints.SelectedValue.ToNullableDecimal(),
				BGradePoint = this.ddlBGradePoints.SelectedValue.ToNullableDecimal(),
				BMinusGradePoint = this.ddlBMinusGradePoints.SelectedValue.ToNullableDecimal(),
				CPlusGradePoint = this.ddlCPlusGradePoints.SelectedValue.ToNullableDecimal(),
				CGradePoint = this.ddlCGradePoints.SelectedValue.ToNullableDecimal(),
				CMinusGradePoint = this.ddlCMinusGradePoints.SelectedValue.ToNullableDecimal(),
				DPlusGradePoint = this.ddlDPlusGradePoints.SelectedValue.ToNullableDecimal(),
				DGradePoint = this.ddlDGradePoints.SelectedValue.ToNullableDecimal(),
				FGradePoint = this.ddlFGradePoints.SelectedValue.ToDecimal(),
				SummerCappingExamGradeEnum = this.ddlSummerGradeCapping.GetSelectedEnumValue<ExamGrades>(null),
			};
			var errorMessage = examMarksPolicy.Validate();
			if (errorMessage != null)
			{
				this.alertPolicy.AddErrorAlert(errorMessage);
				this.updatePanelPolicy.Update();
				return;
			}

			if (examMarksPolicyID == null)
			{
				var status = BL.Core.Exams.Admin.ExamMarksPolicy.AddExamMarksPolicy(examMarksPolicy, this.AspireIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.Exams.Admin.ExamMarksPolicy.AddExamMarksPolicyStatuses.NameAlreadyExists:
						this.alertPolicy.AddErrorAlert("Policy name already exists.");
						this.updatePanelPolicy.Update();
						return;
					case BL.Core.Exams.Admin.ExamMarksPolicy.AddExamMarksPolicyStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Exam Marks Policy");
						this.RefreshPage();
						return;
					case BL.Core.Exams.Admin.ExamMarksPolicy.AddExamMarksPolicyStatuses.RecordNotValid:
						this.AddErrorAlert("Record has validation errors.");
						this.RefreshPage();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var status = BL.Core.Exams.Admin.ExamMarksPolicy.UpdateExamMarksPolicy(examMarksPolicy, this.AspireIdentity.LoginSessionGuid);
				switch (status)
				{
					case BL.Core.Exams.Admin.ExamMarksPolicy.UpdateExamMarksPolicyStatuses.NameAlreadyExists:
						this.alertPolicy.AddErrorAlert("Policy name already exists.");
						this.updatePanelPolicy.Update();
						return;
					case BL.Core.Exams.Admin.ExamMarksPolicy.UpdateExamMarksPolicyStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Exam Marks Policy");
						this.RefreshPage();
						return;
					case BL.Core.Exams.Admin.ExamMarksPolicy.UpdateExamMarksPolicyStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						return;
					case BL.Core.Exams.Admin.ExamMarksPolicy.UpdateExamMarksPolicyStatuses.RecordNotValid:
						this.AddErrorAlert("Record has validation errors.");
						this.RefreshPage();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		private static IEnumerable<ListItem> MarksRanges => new[]
		{
			new ListItem("", ""),
			new ListItem("87-100 [A]", "87-100"),
			new ListItem("85-100 [A]", "85-100"),
			new ListItem("80-86 [B+]", "80-86"),
			new ListItem("80-84 [A-]", "80-84"),
			new ListItem("75-79 [B+]", "75-79"),
			new ListItem("72-79 [B]", "72-79"),
			new ListItem("71-74 [B]", "71-74"),
			new ListItem("68-70 [B-]", "68-70"),
			new ListItem("66-71 [C+]", "66-71"),
			new ListItem("64-67 [C+]", "64-67"),
			new ListItem("60-63 [C]", "60-63"),
			new ListItem("60-65 [C]", "60-65"),
			new ListItem("57-59 [C-]", "57-59"),
			new ListItem("54-56 [D+]", "54-56"),
			new ListItem("50-59 [D]", "50-59"),
			new ListItem("50-53 [D]", "50-53"),
			new ListItem("00-59 [F]", "00-59"),
			new ListItem("00-49 [F]", "00-49"),
		};

		private static IEnumerable<ListItem> GradePoints => new[]
			{
			new ListItem(""),
			new ListItem("4.00 [A]", "4.00"),
			new ListItem("3.67 [A-]", "3.67"),
			new ListItem("3.50 [B+]", "3.50"),
			new ListItem("3.33 [B+]", "3.33"),
			new ListItem("3.00 [B]", "3.00"),
			new ListItem("2.67 [B-]", "2.67"),
			new ListItem("2.50 [C+]", "2.50"),
			new ListItem("2.33 [C+]", "2.33"),
			new ListItem("2.00 [C]", "2.00"),
			new ListItem("1.67 [C-]", "1.67"),
			new ListItem("1.50 [D]", "1.50"),
			new ListItem("1.33 [D+]", "1.33"),
			new ListItem("1.00 [D]", "1.00"),
			new ListItem("0.00 [F]", "0.00"),
		};

		private static IEnumerable<ListItem> Marks => new[]
		{
			new ListItem(""),
			new ListItem("05"),
			new ListItem("10"),
			new ListItem("15"),
			new ListItem("20"),
			new ListItem("25"),
			new ListItem("30"),
			new ListItem("35"),
			new ListItem("40"),
			new ListItem("45"),
			new ListItem("50"),
			new ListItem("55"),
			new ListItem("60"),
			new ListItem("65"),
			new ListItem("70"),
			new ListItem("75"),
			new ListItem("80"),
			new ListItem("85"),
			new ListItem("90"),
			new ListItem("95"),
			new ListItem("100")
		};

		private static byte? GetRangeValue(AspireDropDownList ddl, int index)
		{
			if (string.IsNullOrEmpty(ddl.SelectedValue))
				return null;
			return ddl.SelectedValue.Split('-')[index].ToByte();
		}
	}
}