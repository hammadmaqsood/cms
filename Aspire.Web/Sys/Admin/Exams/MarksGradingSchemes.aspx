﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="MarksGradingSchemes.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Exams.MarksGradingSchemes" %>

<%@ Import Namespace="Aspire.Web.Sys.Admin.Exams" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireHyperLinkButton runat="server" Glyphicon="plus" ButtonType="Success" Text="Add" NavigateUrl="MarksGradingScheme.aspx" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
					<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteID_OnSelectedIndexChanged" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvExamMarksGradingSchemes" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gvExamMarksGradingSchemes_OnPageIndexChanging" OnPageSizeChanging="gvExamMarksGradingSchemes_OnPageSizeChanging" OnSorting="gvExamMarksGradingSchemes_OnSorting" OnRowCommand="gvExamMarksGradingSchemes_OnRowCommand">
		<Columns>
			<asp:BoundField HeaderText="Institute" SortExpression="InstituteAlias" DataField="Institute.InstituteAlias" />
			<asp:BoundField HeaderText="Grading Scheme Name" SortExpression="GradingSchemeName" DataField="GradingSchemeName" />
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireHyperLinkButton ToolTip="Edit" ButtonType="OnlyIcon" runat="server" Glyphicon="edit" NavigateUrl='<%# MarksGradingScheme.GetPageUrl((int)Eval("ExamMarksGradingSchemeID")) %>' />
					<aspire:AspireLinkButton ToolTip="Delete" CausesValidation="False" runat="server" CommandName="Delete" Glyphicon="remove" EncryptedCommandArgument='<%#Eval("ExamMarksGradingSchemeID")%>' ConfirmMessage='<%#Eval("GradingSchemeName","Are you sure you want to delete?") %>' />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
