﻿using Aspire.BL.Core.Exams.Admin;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Exams
{
	[AspirePage("0244C273-F76F-4B1C-9D38-C33D8B4CD98F", UserTypes.Admins, "~/Sys/Admin/Exams/MarkingScheme.aspx", "Exam Marking Scheme", true, AspireModules.Exams)]
	public partial class MarkingScheme : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{AdminPermissions.Exams.ManageExamMarkingSchemes, new []{UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_edit);
		public override string PageTitle => this.ExamMarkingSchemeID == null ? "Add Exam Marking Scheme" : "Edit Exam Marking Scheme";
		private int? ExamMarkingSchemeID => this.Request.GetParameterValue<int>("ExamMarkingSchemeID");

		public static string GetPageUrl(int? examMarkingSchemeID)
		{
			return GetPageUrl<MarkingScheme>().AttachQueryParam("ExamMarkingSchemeID", examMarkingSchemeID);
		}

		public static void Redirect(int? examMarkingSchemeID)
		{
			Redirect(GetPageUrl(examMarkingSchemeID));
		}

		private void RefreshPage()
		{
			Redirect(this.ExamMarkingSchemeID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlInstituteID.FillInstitutesForAdmin();
				this.ddlMarkingSchemeType.FillMarkingSchemeTypes();
				if (this.ExamMarkingSchemeID == null)
				{
					this.AdminIdentity.Demand(AdminPermissions.Exams.ManageExamMarkingSchemes, UserGroupPermission.PermissionValues.Allowed);
					this.ddlInstituteID.ClearSelection();
					this.tbMarkingSchemeName.Text = null;

					this.btnSave.Text = "Add";
					this.panelExamMarkingScheme.Visible = false;
				}
				else
				{
					var examMarkingScheme = ExamMarkingSchemes.GetExamMarkingScheme(this.ExamMarkingSchemeID.Value, this.AdminIdentity.LoginSessionGuid);
					if (examMarkingScheme == null)
					{
						this.AddNoRecordFoundAlert();
						Redirect<MarkingSchemes>();
						return;
					}
					this.ddlInstituteID.SelectedValue = examMarkingScheme.InstituteID.ToString();
					this.ddlInstituteID.Enabled = false;
					this.tbMarkingSchemeName.Text = examMarkingScheme.MarkingSchemeName;
					this.ddlMarkingSchemeType.Enabled = false;
					this.btnSave.Text = "Save";

					this.ddlCourseCategory.FillCourseCategories();
					this.tbMinMarks.Text = null;
					this.tbMaxMarks.Text = null;
					var details = examMarkingScheme.ExamMarkingSchemeDetails.OrderBy(d => d.CourseCategory).ThenBy(d => d.ExamMarksType).Select(d => new
					{
						d.ExamMarkingSchemeDetailID,
						CourseCategoryFullName = d.CourseCategoryEnum.ToFullName(),
						CourseCategoryTotal = examMarkingScheme.ExamMarkingSchemeDetails.Where(emsd => emsd.CourseCategory == d.CourseCategory).Sum(emsd => emsd.MaxMarks).ToExamMarksString(),
						ExamMarksTypeFullName = d.ExamMarksTypeEnum.ToFullName(),
						MinMarks = d.MinMarks.ToExamMarksString(),
						MaxMarks = d.MaxMarks.ToExamMarksString(),
						ExamMarksTypeWeightage = (d.MaxMarks * 100 / examMarkingScheme.ExamMarkingSchemeDetails.Where(emsd => emsd.CourseCategory == d.CourseCategory).Sum(emsd => emsd.MaxMarks)).ToExamMarksString(),
					}).ToList();
					this.gvExamMarkingSchemeDetails.DataBind(details);
				}
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			this.AdminIdentity.Demand(AdminPermissions.Exams.ManageExamMarkingSchemes, UserGroupPermission.PermissionValues.Allowed);
			var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
			var markingSchemeName = this.tbMarkingSchemeName.Text;
			var markingSchemeTypeEnum = this.ddlMarkingSchemeType.GetSelectedEnumValue<ExamMarkingScheme.MarkingSchemeTypes>();
			if (this.ExamMarkingSchemeID == null)
			{
				var result = ExamMarkingSchemes.AddExamMarkingScheme(instituteID, markingSchemeName, markingSchemeTypeEnum, this.AdminIdentity.LoginSessionGuid);
				switch (result.Status)
				{
					case ExamMarkingSchemes.AddExamMarkingSchemeResult.Statuses.NameAlreadyExists:
						this.AddErrorAlert("Scheme name already exists for specified institute.");
						return;
					case ExamMarkingSchemes.AddExamMarkingSchemeResult.Statuses.Success:
						this.AddSuccessMessageHasBeenAdded("Exam Marking Scheme");
						Redirect(result.ExamMarkingSchemeID);
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var status = ExamMarkingSchemes.UpdateExamMarkingScheme(this.ExamMarkingSchemeID.Value, markingSchemeName, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case ExamMarkingSchemes.UpdateExamMarkingSchemeStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<MarkingSchemes>();
						return;
					case ExamMarkingSchemes.UpdateExamMarkingSchemeStatuses.NameAlreadyExists:
						this.AddErrorAlert("Scheme name already exists for specified institute.");
						break;
					case ExamMarkingSchemes.UpdateExamMarkingSchemeStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Exam Marking Scheme");
						this.RefreshPage();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		protected void gvExamMarkingSchemeDetails_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					this.AdminIdentity.Demand(AdminPermissions.Exams.ManageExamMarkingSchemes, UserGroupPermission.PermissionValues.Allowed);
					var examMarkingSchemeDetailID = e.DecryptedCommandArgumentToInt();
					var status = ExamMarkingSchemes.DeleteExamMarkingSchemeDetail(examMarkingSchemeDetailID, this.AdminIdentity.LoginSessionGuid);
					switch (status)
					{
						case ExamMarkingSchemes.DeleteExamMarkingSchemeDetailStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamMarkingSchemes.DeleteExamMarkingSchemeDetailStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Record");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.RefreshPage();
					break;
				case "Edit":
					e.Handled = true;
					this.ShowDetailModal(e.DecryptedCommandArgumentToInt());
					break;
			}
		}

		protected void btnAddDetail_OnClick(object sender, EventArgs e)
		{
			this.ShowDetailModal(null);
		}

		private void ShowDetailModal(int? examMarkingSchemeDetailID)
		{
			if (examMarkingSchemeDetailID == null)
			{
				this.AdminIdentity.Demand(AdminPermissions.Exams.ManageExamMarkingSchemes, UserGroupPermission.PermissionValues.Allowed);
				this.ddlCourseCategory.ClearSelection();
				this.ddlExamMarksType.ClearSelection();
				this.ddlCourseCategory.Enabled = true;
				this.ddlExamMarksType.Enabled = true;
				this.tbMinMarks.Text = null;
				this.tbMaxMarks.Text = null;
				this.modalExamMarkingScheme.HeaderText = "Add Exam Marking Detail";
				this.btnSaveDetail.Text = "Add";
				this.ViewState["ExamMarkingSchemeDetailID"] = null;
			}
			else
			{
				var examMarkingSchemeDetail = ExamMarkingSchemes.GetExamMarkingSchemeDetail(examMarkingSchemeDetailID.Value, this.AdminIdentity.LoginSessionGuid);
				if (examMarkingSchemeDetail == null)
				{
					this.AddNoRecordFoundAlert();
					this.RefreshPage();
					return;
				}
				this.ddlCourseCategory.SetEnumValue(examMarkingSchemeDetail.CourseCategoryEnum).Enabled = false;
				this.ddlExamMarksType.SetEnumValue(examMarkingSchemeDetail.ExamMarksTypeEnum).Enabled = false;
				this.tbMinMarks.Text = examMarkingSchemeDetail.MinMarks.ToExamMarksString();
				this.tbMaxMarks.Text = examMarkingSchemeDetail.MaxMarks.ToExamMarksString();
				this.modalExamMarkingScheme.HeaderText = "Edit Exam Marking Detail";
				this.btnSaveDetail.Text = "Save";
				this.ViewState["ExamMarkingSchemeDetailID"] = examMarkingSchemeDetail.ExamMarkingSchemeDetailID;
			}
			this.modalExamMarkingScheme.Show();
		}

		protected void btnSaveDetail_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			this.AdminIdentity.Demand(AdminPermissions.Exams.ManageExamMarkingSchemes, UserGroupPermission.PermissionValues.Allowed);
			var examMarkingSchemeDetailID = (int?)this.ViewState["ExamMarkingSchemeDetailID"];
			var minMarks = this.tbMinMarks.Text.ToDecimal();
			var maxMarks = this.tbMaxMarks.Text.ToDecimal();
			if (examMarkingSchemeDetailID == null)
			{
				var courseCategory = this.ddlCourseCategory.GetSelectedEnumValue<CourseCategories>();
				var examMarksType = this.ddlExamMarksType.GetSelectedEnumValue<ExamMarksTypes>();
				var status = ExamMarkingSchemes.AddExamMarkingSchemeDetail(this.ExamMarkingSchemeID.Value, courseCategory, examMarksType, minMarks, maxMarks, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case ExamMarkingSchemes.AddExamMarkingSchemeDetailStatuses.AlreadyExists:
						this.alertModal.AddErrorAlert("Exam Marking Scheme Detail already exists.");
						this.updatePanelExamMarkingScheme.Update();
						break;
					case ExamMarkingSchemes.AddExamMarkingSchemeDetailStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						return;
					case ExamMarkingSchemes.AddExamMarkingSchemeDetailStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Exam Marking Scheme Detail");
						this.RefreshPage();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var status = ExamMarkingSchemes.UpdateExamMarkingSchemeDetail(examMarkingSchemeDetailID.Value, minMarks, maxMarks, this.AdminIdentity.LoginSessionGuid);
				switch (status)
				{
					case ExamMarkingSchemes.UpdateExamMarkingSchemeDetailStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.RefreshPage();
						return;
					case ExamMarkingSchemes.UpdateExamMarkingSchemeDetailStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Exam Marking Scheme Detail");
						this.RefreshPage();
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
	}
}