﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ExamRemarksPolicy.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Exams.ExamRemarksPolicy" %>

<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<%@ Import Namespace="Aspire.Lib.Extensions" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-9">
			<aspire:AspireLabel runat="server" Text="" AssociatedControlID="btnAddExamRemarksPolicy" />
			<div>
				<aspire:AspireButton ID="btnAddExamRemarksPolicy" runat="server" CausesValidation="False" Glyphicon="plus" ButtonType="Success" Text="Add Exam Marks Policy" OnClick="btnAddExamMarksPolicy_OnClick" />
			</div>
		</div>
		<div class="col-md-3">
			<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteIDFilter" />
			<aspire:AspireDropDownList runat="server" ID="ddlInstituteIDFilter" OnSelectedIndexChanged="ddlInstituteIDFilter_OnSelectedIndexChanged" ValidationGroup="Filters" AutoPostBack="True" />
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" AutoGenerateColumns="False" AllowSorting="True" ID="gvExamRemarksPolicies" ItemType="Aspire.Model.Entities.ExamRemarksPolicy" OnSorting="gvExamRemarksPolicies_OnSorting" OnPageIndexChanging="gvExamRemarksPolicies_OnPageIndexChanging" OnPageSizeChanging="gvExamRemarksPolicies_OnPageSizeChanging" OnRowCommand="gvExamRemarksPolicies_OnRowCommand">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="Institute.InstituteAlias" HeaderText="Institute" SortExpression="InstituteAlias" />
			<asp:BoundField DataField="ExamRemarksPolicyName" HeaderText="Policy Name" SortExpression="ExamRemarksPolicyName" />
			<asp:BoundField DataField="ExamRemarksPolicyDescription" HeaderText="Policy Description" SortExpression="ExamRemarksPolicyDescription" />
			<asp:TemplateField HeaderText="Summary">
				<ItemTemplate><%# Item.Summary.HtmlEncode(true) %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="Validity" HeaderText="Validity" SortExpression="ValidFromSemesterID" />
			<asp:TemplateField HeaderText="Actions">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" EncryptedCommandArgument="<%# Item.ExamRemarksPolicyID %>" Glyphicon="edit" CommandName="Edit" ToolTip="Edit" CausesValidation="False" />
					<aspire:AspireLinkButton runat="server" EncryptedCommandArgument="<%# Item.ExamRemarksPolicyID %>" Glyphicon="remove" CommandName="Delete" ToolTip="Delete" ConfirmMessage="Are you sure you want to delete this record?" CausesValidation="False" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
	<aspire:AspireModal runat="server" ID="modalPolicy" ModalSize="Large">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updatePanelPolicy" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertPolicy" />
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteID" />
								<aspire:AspireDropDownList runat="server" ID="ddlInstituteID" ValidationGroup="SavePolicy" />
								<aspire:AspireRequiredFieldValidator ValidationGroup="SavePolicy" runat="server" ControlToValidate="ddlInstituteID" ErrorMessage="This field is required." />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Policy Name:" AssociatedControlID="tbExamRemarksPolicyName" />
								<aspire:AspireTextBox runat="server" ID="tbExamRemarksPolicyName" MaxLength="500" ValidationGroup="SavePolicy" />
								<aspire:AspireStringValidator ValidationGroup="SavePolicy" ControlToValidate="tbExamRemarksPolicyName" runat="server" AllowNull="False" RequiredErrorMessage="This field is required." />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Description:" AssociatedControlID="tbExamRemarksPolicyDescription" />
								<aspire:AspireTextBox runat="server" TextMode="MultiLine" ID="tbExamRemarksPolicyDescription" MaxLength="1000" ValidationGroup="SavePolicy" />
								<aspire:AspireStringValidator ControlToValidate="tbExamRemarksPolicyDescription" ValidationGroup="SavePolicy" runat="server" AllowNull="False" RequiredErrorMessage="This field is required." />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Valid From:" AssociatedControlID="ddlValidFromSemesterID" />
								<aspire:AspireDropDownList runat="server" ID="ddlValidFromSemesterID" ValidationGroup="SavePolicy" />
								<aspire:AspireRequiredFieldValidator ControlToValidate="ddlValidFromSemesterID" ValidationGroup="SavePolicy" runat="server" ErrorMessage="This field is required." />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<aspire:AspireLabel runat="server" Text="Valid Up To:" AssociatedControlID="ddlValidUpToSemesterID" />
								<aspire:AspireDropDownList runat="server" ID="ddlValidUpToSemesterID" ValidationGroup="SavePolicy" />
								<aspire:AspireCompareValidator runat="server" ControlToValidate="ddlValidUpToSemesterID" ControlToCompare="ddlValidFromSemesterID" Operator="GreaterThanEqual" Type="Integer" ValidationGroup="SavePolicy" ErrorMessage='"Valid From" semester must less than or equal to "Valid Up To" semester.' />
							</div>
						</div>
					</div>
					<asp:Panel runat="server" ID="panelDetails">
						<table class="table table-bordered table-striped table-hover">
							<caption>Rules</caption>
							<thead>
								<tr>
									<th>#</th>
									<th>First Semester</th>
									<th>Remarks</th>
									<th>Less than CGPA</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<asp:Repeater runat="server" ID="repeaterExamRemarksPolicyDetails" ItemType="Aspire.Model.Entities.ExamRemarksPolicyDetail" OnItemCommand="repeaterExamRemarksPolicyDetails_OnItemCommand">
									<ItemTemplate>
										<tr>
											<td><%#: Item.Sequence %></td>
											<td><%#: Item.FirstSemester.ToYesNo() %></td>
											<td><%#: Item.ExamRemarksTypeEnum.ToFullName() %></td>
											<td><%#: Item.LessThanCGPA.FormatGPA() %></td>
											<td>
												<aspire:AspireLinkButton runat="server" ConfirmMessage="Are you sure you want to delete?" CausesValidation="False" EncryptedCommandArgument="<%# Item.ExamRemarksPolicyDetailID %>" CommandName="Delete" Glyphicon="remove" />
											</td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
								<tr>
									<td></td>
									<td>
										<aspire:AspireCheckBox runat="server" ID="cbFirstSemester" ValidationGroup="DetailsAdd" />
									</td>
									<td>
										<aspire:AspireDropDownList runat="server" ID="ddlExamRemarksType" ValidationGroup="DetailsAdd" />
										<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlExamRemarksType" ValidationGroup="DetailsAdd" ErrorMessage="This field is required." />
									</td>
									<td>
										<aspire:AspireTextBox runat="server" ID="tbLessThanCGPA" MaxLength="4" ValidationGroup="DetailsAdd" />
										<aspire:AspireDoubleValidator AllowNull="False" MinValue="0" MaxValue="4.0" runat="server" ControlToValidate="tbLessThanCGPA" ValidationGroup="DetailsAdd" InvalidNumberErrorMessage="Invalid CGPA." InvalidRangeErrorMessage="CGPA must be from 0 to 4." RequiredErrorMessage="This field is required." />
									</td>
									<td>
										<aspire:AspireLinkButton runat="server" Glyphicon="plus" ValidationGroup="DetailsAdd" ID="btnAddExamRemarksPolicyDetail" OnClick="btnAddExamRemarksPolicyDetail_OnClick" />
									</td>
								</tr>
							</tbody>
						</table>
					</asp:Panel>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Always">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" ValidationGroup="SavePolicy" Text="Save" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
