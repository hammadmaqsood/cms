﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Aspire.BL.Core.Exams.Admin;

namespace Aspire.Web.Sys.Admin.Exams
{
	[AspirePage("338D6954-6407-4F20-A051-24342B1A36E8", UserTypes.Admins, "~/Sys/Admin/Exams/MarksGradingScheme.aspx", "Marks Grading Scheme", true, AspireModules.Exams)]
	public partial class MarksGradingScheme : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.Exams.Module, new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_edit);
		public override string PageTitle => this.ExamMarksGradingSchemeID == null ? "Add Marks Grading Scheme" : "Edit Marks Grading Scheme";

		public int? ExamMarksGradingSchemeID => this.Request.GetParameterValue<int>("ExamMarksGradingSchemeID");

		public static string GetPageUrl(int? examMarksGradingSchemeID)
		{
			return GetPageUrl<MarksGradingScheme>().AttachQueryParam("ExamMarksGradingSchemeID", examMarksGradingSchemeID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlInstitute.FillInstitutesForAdmin();
				this.ddlRegularSemester.FillYesNo(CommonListItems.All);
				if (this.ExamMarksGradingSchemeID == null)
				{
					this.ddlInstitute.ClearSelection();
					this.tbGradingSchemeName.Text = null;

					this.btnSave.Text = "Add";
					this.panelExamMgsRanges.Visible = false;
				}
				else
				{
					var examMarksGradingsScheme = ExamMarksGradingSchemes.GetExamMarksGradingScheme(this.ExamMarksGradingSchemeID.Value, this.AdminIdentity.LoginSessionGuid);
					this.ddlInstitute.SelectedValue = examMarksGradingsScheme.InstituteID.ToString();
					this.tbGradingSchemeName.Text = examMarksGradingsScheme.GradingSchemeName;
					this.btnSave.Text = "Save";

					this.ddlExamGrades.FillExamGrades();
					this.GetData();
					this.panelExamMgsRanges.Visible = true;
				}
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstitute.SelectedValue.ToInt();
			var gradingSchemeName = this.tbGradingSchemeName.Text;
			bool alreadyExists;
			if (this.ExamMarksGradingSchemeID == null)
			{
				var examMarksGradingScheme = ExamMarksGradingSchemes.AddExamMarksGradingScheme(instituteID, gradingSchemeName, out alreadyExists, this.AdminIdentity.LoginSessionGuid);
				if (examMarksGradingScheme == null)
				{
					if (alreadyExists)
						this.AddErrorAlert("Exam marks grading scheme already exists with provide name.");
					else
						throw new InvalidOperationException();
				}
				else
				{
					if (alreadyExists)
						throw new InvalidOperationException();
					else
					{
						this.AddSuccessMessageHasBeenAdded("Exam marks grading scheme");
						Redirect(GetPageUrl(examMarksGradingScheme.ExamMarksGradingSchemeID));
					}
				}
			}
			else
			{
				var examMarksGradingScheme = ExamMarksGradingSchemes.UpdateExamMarksGradingScheme(this.ExamMarksGradingSchemeID.Value, gradingSchemeName, out alreadyExists, this.AdminIdentity.LoginSessionGuid);
				if (examMarksGradingScheme == null)
				{
					if (alreadyExists)
						this.AddErrorAlert("Exam marks grading scheme already exists with provide name or institute.");
					else
						this.AddNoRecordFoundAlert();
				}
				else
				{
					if (alreadyExists)
						throw new InvalidOperationException();
					else
					{
						this.AddSuccessMessageHasBeenUpdated("Exam marks grading scheme");
						Redirect(GetPageUrl(examMarksGradingScheme.ExamMarksGradingSchemeID));
					}
				}
			}
		}

		protected void gvExamMgsRanges_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Delete":
					e.Handled = true;
					var examMarksGradingSchemesGradeRangeID = e.DecryptedCommandArgumentToInt();
					var result = ExamMarksGradingSchemes.DeleteExamMarksGradingSchemesGradeRange(examMarksGradingSchemesGradeRangeID, this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case ExamMarksGradingSchemes.DeleteExamMarksGradingSchemesGradeRangeStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case ExamMarksGradingSchemes.DeleteExamMarksGradingSchemesGradeRangeStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Exam grade range");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					this.GetData();
					break;
			}
		}

		private void GetData()
		{
			var examMarksGradingSchemeGradeRanges = ExamMarksGradingSchemes.GetExamMarksGradingSchemeGradeRanges(this.ExamMarksGradingSchemeID, this.AdminIdentity.LoginSessionGuid);
			this.gvExamMgsRanges.DataBind(examMarksGradingSchemeGradeRanges);
		}

		protected void btnAdd_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var gradeEnum = this.ddlExamGrades.GetSelectedEnumValue<ExamGrades>();
			var minMarks = this.tbMinMarks.Text.ToDecimal();
			var maxMarks = this.tbMaxMarks.Text.ToDecimal();
			var gradePoints = this.ddlGradePoints.SelectedValue.ToDecimal();
			var isSummerRegular = this.ddlRegularSemester.SelectedValue.ToNullableBoolean();
			if (isSummerRegular == null)
			{
				AddExamMarksGradingSchemeGradeRanges(this.ExamMarksGradingSchemeID.Value, true, gradeEnum, minMarks, maxMarks, gradePoints);
				AddExamMarksGradingSchemeGradeRanges(this.ExamMarksGradingSchemeID.Value, false, gradeEnum, minMarks, maxMarks, gradePoints);
			}
			else
				AddExamMarksGradingSchemeGradeRanges(this.ExamMarksGradingSchemeID.Value, isSummerRegular.Value, gradeEnum, minMarks, maxMarks, gradePoints);
		}

		private void AddExamMarksGradingSchemeGradeRanges(int examMarksGradingSchemeID, bool regularSemester, ExamGrades gradeEnum, decimal minMarks, decimal maxMarks, decimal gradePoints)
		{
			var result = ExamMarksGradingSchemes.AddExamMarksGradingSchemeGradeRanges(examMarksGradingSchemeID, regularSemester, gradeEnum, minMarks, maxMarks, gradePoints, this.AdminIdentity.LoginSessionGuid);
			switch (result)
			{
				case ExamMarksGradingSchemes.AddExamMarksGradingSchemeGradeRangesStatuses.NoRecordFound:
					this.AddNoRecordFoundAlert();
					break;
				case ExamMarksGradingSchemes.AddExamMarksGradingSchemeGradeRangesStatuses.AlreadyExists:
					this.AddErrorAlert("Range already exists with selected grade.");
					break;
				case ExamMarksGradingSchemes.AddExamMarksGradingSchemeGradeRangesStatuses.Success:
					this.AddSuccessMessageHasBeenAdded("Exam grade range");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.GetData();
		}
	}
}