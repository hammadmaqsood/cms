﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Admin
{
	[AspirePage("{64E4CC90-82F9-46F9-9F88-F26E6EFB623D}", UserTypes.Admins, "~/Sys/Admin/PrepareSession.aspx", null, false, AspireModules.None)]
	public partial class PrepareSession : BasePage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (SessionHelper.SideBarLinkHtml == null)
			{
				PrepareMenu();
			}
			Redirect(GetPageUrl<Dashboard>(), false);
		}

		private static void PrepareMenu()
		{
			var adminIdentity = AdminIdentity.Current;
			AspireSideBarLinks sideBarLinks;
			switch (adminIdentity.UserType)
			{
				case UserTypes.MasterAdmin:
					sideBarLinks = DefaultMenus.MasterAdmin.Links;
					break;
				case UserTypes.Admin:
					var homePageAspirePageAttribute = AspirePageAttributesDictionary.GetAspirePageAttribute<Sys.Admin.Dashboard>();
					var homeAspireSideBarLink = new AspireSideBarLink(null, homePageAspirePageAttribute.AspirePageGuid, homePageAspirePageAttribute.MenuItemText, homePageAspirePageAttribute.PageUrl, true, null, null);
					var userGroupMenuLinks = Aspire.BL.Core.LoginManagement.Admin.GetUserGroupMenuLinks(adminIdentity.LoginSessionGuid);
					sideBarLinks = userGroupMenuLinks.ToAspireSideBarLinks(homeAspireSideBarLink);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			SessionHelper.SetSideBarLinks(sideBarLinks);
		}

		protected override void Authenticate()
		{
			if (AdminIdentity.Current == null)
				throw new AspireAccessDeniedException(UserTypes.Admins, UserTypes.Admins);
		}
	}
}