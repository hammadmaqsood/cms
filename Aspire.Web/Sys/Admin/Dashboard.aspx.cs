﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using Aspire.Model.Entities;

namespace Aspire.Web.Sys.Admin
{
	[AspirePage("{0C473A87-A532-4CDC-98D5-92D64C40E59C}", UserTypes.Admins, "~/Sys/Admin/Dashboard.aspx", "Home", false, AspireModules.None)]
	public partial class Dashboard : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => null;
		public override bool BreadcrumbVisible => false;
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_desktop);
		public override string PageTitle => "Dashboard";

		protected void Page_Load(object sender, EventArgs e)
		{

		}
	}
}