﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Surveys.aspx.cs" Inherits="Aspire.Web.Sys.Admin.QualityAssurance.Surveys" %>
<%@ Import Namespace="Aspire.Web.Sys.Admin.QualityAssurance" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireButton runat="server" ID="btnAdd" Glyphicon="plus" Text="Add" ButtonType="Success" CausesValidation="False" OnClick="btnAdd_OnClick" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlInstituteIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteIDFilter_OnSelectedIndexChanged" />
				</div>
			</div>
		</div>
	</div>

	<p></p>
	<aspire:AspireGridView runat="server" ID="gvSurveys" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gvSurveys_OnPageIndexChanging" OnPageSizeChanging="gvSurveys_OnPageSizeChanging" OnSorting="gvSurveys_OnSorting" OnRowCommand="gvSurveys_OnRowCommand">
		<Columns>
			<asp:BoundField HeaderText="Institute" SortExpression="InstituteAlias" DataField="Institute.InstituteAlias" />
			<asp:TemplateField HeaderText="Survey Name" SortExpression="SurveyName">
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" NavigateUrl='<%# SurveysGroupsQuestionsOptions.GetPageUrl((int)this.Eval("InstituteID"),(int)this.Eval("SurveyID")) %>' Text='<%# this.Eval("SurveyName") %>'/>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Survey Type" SortExpression="SurveyType" DataField="SurveyTypeFullName" />
			<asp:BoundField HeaderText="Description" SortExpression="Description" DataField="Description" />
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument='<%#this.Eval("SurveyID") %>' />
					<aspire:AspireLinkButton runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%#this.Eval("SurveyID") %>' ConfirmMessage="Are you sure you want to delete survey?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalSurveys">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateModalSurveys">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertSurveys" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstitute" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlInstitute" ValidationGroup="Req" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlInstitute" ValidationGroup="Req" ErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Survey Name:" AssociatedControlID="tbSurveyName" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbSurveyName" ValidationGroup="Req" MaxLength="100" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbSurveyName" ValidationGroup="Req" ErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Survey Type:" AssociatedControlID="ddlSurveyType" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlSurveyType" ValidationGroup="Req" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSurveyType" ValidationGroup="Req" ErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Description:" AssociatedControlID="tbDescription" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbDescription" ValidationGroup="Req" TextMode="MultiLine" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ValidationGroup="Req" ButtonType="Primary" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
