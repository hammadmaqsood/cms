﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ControlPanel.aspx.cs" Inherits="Aspire.Web.Sys.Admin.QualityAssurance.ControlPanel" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<aspire:AspireButton runat="server" ID="btnAdd" ButtonType="Success" Glyphicon="plus" Text="Add" OnClick="btnAdd_OnClick" />
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemester" />
					<aspire:AspireDropDownList runat="server" ID="ddlSemester" AutoPostBack="True" OnSelectedIndexChanged="ddlSemester_OnSelectedIndexChanged" />
				</div>

				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstitute" />
					<aspire:AspireDropDownList runat="server" ID="ddlInstitute" AutoPostBack="True" OnSelectedIndexChanged="ddlInstitute_OnSelectedIndexChanged" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ID="gvControlPanel" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gvControlPanel_OnPageIndexChanging" OnPageSizeChanging="gvControlPanel_OnPageSizeChanging" OnSorting="gvControlPanel_OnSorting" OnRowCommand="gvControlPanel_OnRowCommand" OnRowDataBound="gvControlPanel_OnRowDataBound">
		<Columns>
			<asp:BoundField HeaderText="Institute" SortExpression="InstituteAlias" DataField="InstituteAlias" />
			<asp:BoundField HeaderText="Semester" SortExpression="Semester" DataField="SemesterFullName" />
			<asp:BoundField HeaderText="Survey Name" SortExpression="SurveyName" DataField="SurveyName" />
			<asp:BoundField HeaderText="Type" SortExpression="Type" DataField="TypeFullName" />
			<asp:BoundField HeaderText="From" SortExpression="OpenDate" DataField="OpenDate" DataFormatString="{0:dd-MMM-yyyy}" />
			<asp:BoundField HeaderText="To" SortExpression="CloseDate" DataField="CloseDate" DataFormatString="{0:dd-MMM-yyyy}" />
			<asp:TemplateField HeaderText="Status" SortExpression="Status">
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" CommandName="Status" EncryptedCommandArgument='<%#Eval("SurveyConductID") %>' Text='<%#Eval("StatusEnumFullName")%>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<ItemTemplate>
					<aspire:AspireLinkButton runat="server" Glyphicon="edit" CommandName="Edit" EncryptedCommandArgument='<%#this.Eval("SurveyConductID") %>' />
					<aspire:AspireLinkButton runat="server" Glyphicon="remove" CommandName="Delete" EncryptedCommandArgument='<%#this.Eval("SurveyConductID") %>' ConfirmMessage="Are you sure you want to delete survey conduct schedule?" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>

	<aspire:AspireModal runat="server" ID="modalControlPanel">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updateModalControlPanel" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertControlPanel" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Semester:" AssociatedControlID="ddlSemesterForSave" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlSemesterForSave" ValidationGroup="Req" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSemesterForSave" ValidationGroup="Req" ErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteForSave" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlInstituteForSave" ValidationGroup="Req" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteForSave_OnSelectedIndexChanged" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlInstituteForSave" ValidationGroup="Req" ErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Survey Name:" AssociatedControlID="ddlSurveyName" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlSurveyName" ValidationGroup="Req" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlSurveyName" ValidationGroup="Req" ErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlType" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlType" ValidationGroup="Req" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlType" ValidationGroup="Req" ErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="From:" AssociatedControlID="dpOpenDate" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dpOpenDate" ValidationGroup="Req" />
								<aspire:AspireDateTimeValidator runat="server" ControlToValidate="dpOpenDate" ValidationGroup="Req" AllowNull="False" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Date." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="To:" AssociatedControlID="dpCloseDate" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDateTimePickerTextbox runat="server" ID="dpCloseDate" ValidationGroup="Req" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="rblStatus" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireRadioButtonList runat="server" ID="rblStatus" RepeatLayout="Flow" RepeatDirection="Horizontal" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSave" Text="Save" ValidationGroup="Req" ButtonType="Primary" OnClick="btnSave_OnClick" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
