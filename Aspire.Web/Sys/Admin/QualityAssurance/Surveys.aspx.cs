﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Aspire.Lib.Extensions;

namespace Aspire.Web.Sys.Admin.QualityAssurance
{
	[AspirePage("AA620EDC-72EE-408C-A03E-79AE4E27C8C2", UserTypes.Admins, "~/Sys/Admin/QualityAssurance/Surveys.aspx", "Surveys", true, AspireModules.QualityAssurance)]
	public partial class Surveys : AdminsPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_camera);
		public override string PageTitle => "Surveys";
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.QualityAssurance.Module, new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};

		protected void Page_Load()
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(nameof(Model.Entities.Survey.SurveyName), SortDirection.Ascending);
				this.ddlInstituteIDFilter.FillInstitutesForAdmin(CommonListItems.All);
				this.ddlInstituteIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlInstituteIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void btnAdd_OnClick(object sender, EventArgs e)
		{
			this.DisplaySurveyModal(null);
		}

		protected void gvSurveys_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvSurveys_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvSurveys.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvSurveys_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvSurveys_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					e.Handled = true;
					this.DisplaySurveyModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var surveyID = e.DecryptedCommandArgumentToInt();
					var result = BL.Core.QualityAssurance.Admin.Survey.DeleteSurvey(surveyID, this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case BL.Core.QualityAssurance.Admin.Survey.DeleteSurveysStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							break;
						case BL.Core.QualityAssurance.Admin.Survey.DeleteSurveysStatuses.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Survey");
							break;
						case BL.Core.QualityAssurance.Admin.Survey.DeleteSurveysStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Survey");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					Redirect<Surveys>();
					break;
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var surveyID = (int?)this.ViewState[nameof(Survey.SurveyID)];
			var surveyName = this.tbSurveyName.Text;
			var surveyTypeEnum = this.ddlSurveyType.GetSelectedEnumValue<Survey.SurveyTypes>();
			var description = this.tbDescription.Text;
			if (surveyID == null)
			{
				var instituteID = this.ddlInstitute.SelectedValue.ToInt();
				var result = BL.Core.QualityAssurance.Admin.Survey.AddSurvey(instituteID, surveyName, surveyTypeEnum, description, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.QualityAssurance.Admin.Survey.AddSurveysStatuses.AlreadyExists:
						this.alertSurveys.AddMessage(AspireAlert.AlertTypes.Error, "Survey already exists");
						this.updateModalSurveys.Update();
						break;
					case BL.Core.QualityAssurance.Admin.Survey.AddSurveysStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Survey");
						Redirect<Surveys>();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var result = BL.Core.QualityAssurance.Admin.Survey.UpdateSurvey(surveyID.Value, surveyName, surveyTypeEnum, description, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case BL.Core.QualityAssurance.Admin.Survey.UpdateSurveysStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<Surveys>();
						break;
					case BL.Core.QualityAssurance.Admin.Survey.UpdateSurveysStatuses.AlreadyExists:
						this.alertSurveys.AddMessage(AspireAlert.AlertTypes.Error, "Survey already exists");
						this.updateModalSurveys.Update();
						break;
					case BL.Core.QualityAssurance.Admin.Survey.UpdateSurveysStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Survey");
						Redirect<Surveys>();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		private void DisplaySurveyModal(int? surveyID)
		{
			this.ddlInstitute.FillInstitutesForAdmin();
			this.ddlSurveyType.FillEnums<Survey.SurveyTypes>();
			if (surveyID == null)
			{
				this.ddlInstitute.ClearSelection();
				this.ddlInstitute.Enabled = true;
				this.tbSurveyName.Text = null;
				this.ddlSurveyType.ClearSelection();
				this.tbDescription.Text = null;

				this.modalSurveys.HeaderText = "Add Survey";
				this.btnSave.Text = @"Add";
				this.ViewState[nameof(Survey.SurveyID)] = null;
				this.modalSurveys.Show();
			}
			else
			{
				var survey = BL.Core.QualityAssurance.Admin.Survey.GetSurvey(surveyID.Value, this.AdminIdentity.LoginSessionGuid);
				if (survey == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Surveys>();
					return;
				}
				this.ddlInstitute.SelectedValue = survey.InstituteID.ToString();
				this.ddlInstitute.Enabled = false;
				this.tbSurveyName.Text = survey.SurveyName;
				this.ddlSurveyType.SetEnumValue(survey.SurveyTypeEnum);
				this.tbDescription.Text = survey.Description;

				this.modalSurveys.HeaderText = "Edit Survey";
				this.btnSave.Text = @"Save";
				this.ViewState[nameof(Survey.SurveyID)] = surveyID.Value;
				this.modalSurveys.Show();
			}
		}

		private void GetData(int pageIndex)
		{
			int virtualItemCount;
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToNullableInt();
			var surveys = BL.Core.QualityAssurance.Admin.Survey.GetSurveys(instituteID, pageIndex, this.gvSurveys.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), out virtualItemCount, this.AdminIdentity.LoginSessionGuid);
			this.gvSurveys.DataBind(surveys, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}
	}
}