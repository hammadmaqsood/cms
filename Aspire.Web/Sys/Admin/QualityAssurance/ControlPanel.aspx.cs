﻿using Aspire.BL.Entities.QualityAssurance.Admin;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.QualityAssurance
{
	[AspirePage("86343700-A2CC-46E5-BDC4-D66F513181B2", UserTypes.Admins, "~/Sys/Admin/QualityAssurance/ControlPanel.aspx", "Control Panel", true, AspireModules.QualityAssurance)]
	public partial class ControlPanel : AdminsPage
	{
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.QualityAssurance.Module, new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.cog);
		public override string PageTitle => "Control Panel: Quality Assurance Surveys";

		private void Refresh()
		{
			var instituteID = this.ddlInstitute.SelectedValue.ToInt();
			var semesterID = this.ddlSemester.SelectedValue.ToShort();
			Redirect<ControlPanel>("SemesterID",semesterID,"InstituteID",instituteID);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("InstituteAlias", SortDirection.Ascending);
				this.ddlSemester.FillSemesters(CommonListItems.All).SetSelectedValueIfNotPostback("SemesterID");
				this.ddlInstitute.FillInstitutesForAdmin(CommonListItems.All).SetSelectedValueIfNotPostback("InstituteID");
				this.ddlSemester_OnSelectedIndexChanged(null, null);
			}
		}

		#region Filters for Control Panel Record

		protected void btnAdd_OnClick(object sender, EventArgs e)
		{
			this.AdminIdentity.Demand(AdminPermissions.QualityAssurance.ManageControlPanel, UserGroupPermission.PermissionValues.Allowed);
			this.DisplayControlPanelModal(null);
		}

		protected void ddlSemester_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlInstitute_OnSelectedIndexChanged(null, null);
		}

		protected void ddlInstitute_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		#endregion

		#region Control Panel

		protected void ddlInstituteForSave_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteForSave.SelectedValue.ToInt();
			this.ddlSurveyName.DataBind(BL.QualityAssurance.Admin.GetSurveys(instituteID));
		}

		protected void gvControlPanel_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Status":
					e.Handled = true;
					this.AdminIdentity.Demand(AdminPermissions.QualityAssurance.ManageControlPanel, UserGroupPermission.PermissionValues.Allowed);
					var surveyConductIDForStatus = e.DecryptedCommandArgumentToInt();
					var surveyConduct = BL.QualityAssurance.Admin.GetSurveyConduct(surveyConductIDForStatus);
					if (surveyConduct == null) return;
					SurveyConduct result = null;
					if (surveyConduct.StatusEnum == SurveyConduct.Statuses.Inactive)
						result = BL.QualityAssurance.Admin.UpdateSurveyConductStatus(surveyConduct.SurveyConductID, (byte)SurveyConduct.Statuses.Active, this.AdminIdentity.UserLoginHistoryID);
					else
						result = BL.QualityAssurance.Admin.UpdateSurveyConductStatus(surveyConduct.SurveyConductID, (byte)SurveyConduct.Statuses.Inactive, this.AdminIdentity.UserLoginHistoryID);
					if (result == null)
						this.AddNoRecordFoundAlert();
					this.GetData(this.gvControlPanel.PageIndex);
					break;
				case "Edit":
					e.Handled = true;
					this.DisplayControlPanelModal(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					e.Handled = true;
					var surveyConductID = e.DecryptedCommandArgumentToInt();
					bool isDatePassed;
					this.AdminIdentity.Demand(AdminPermissions.QualityAssurance.ManageControlPanel, UserGroupPermission.PermissionValues.Allowed);
					var deleted = BL.QualityAssurance.Admin.DeleteSurveyConduct(surveyConductID, this.AdminIdentity.UserLoginHistoryID, out isDatePassed);
					if (deleted)
					{
						if (isDatePassed)
							throw new InvalidOperationException();
						else
							this.AddSuccessMessageHasBeenDeleted("Survey conduct schedule");
					}
					else
					{
						if (isDatePassed)
							this.AddErrorAlert("Records of previous dates can't be deleted.");
						else
							this.AddNoRecordFoundAlert();
					}
					this.Refresh();
					break;
			}
		}

		protected void gvControlPanel_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				var courseRegistrationSetting = (CustomSurveyConduct)e.Row.DataItem;
				var openDate = courseRegistrationSetting.OpenDate.Date;
				if (courseRegistrationSetting.CloseDate != null)
				{
					var closeDate = courseRegistrationSetting.CloseDate.Value.Date;
					if (DateTime.Today > closeDate)
						e.Row.CssClass = "danger";
					else if (DateTime.Today >= openDate)
					{
						e.Row.CssClass = courseRegistrationSetting.Status == (byte)CourseRegistrationSetting.Statuses.Inactive ? "warning" : "success";
					}
				}
				else
				{
					if (DateTime.Today >= openDate)
					{
						e.Row.CssClass = courseRegistrationSetting.Status == (byte)CourseRegistrationSetting.Statuses.Inactive ? "warning" : "success";
					}
				}
			}
		}

		protected void gvControlPanel_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		protected void gvControlPanel_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvControlPanel_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvControlPanel.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			this.AdminIdentity.Demand(AdminPermissions.QualityAssurance.ManageControlPanel, UserGroupPermission.PermissionValues.Allowed);
			var surveyConductID = (int?)this.ViewState["SurveyConductID"];
			var openDate = this.dpOpenDate.SelectedDate.Value;
			var closeDate = this.dpCloseDate.SelectedDate;
			var status = this.rblStatus.SelectedValue.ToByte();
			if (surveyConductID == null)
			{
				var semesterID = this.ddlSemesterForSave.SelectedValue.ToShort();
				var institute = this.ddlInstituteForSave.SelectedValue.ToInt();
				var surveyID = this.ddlSurveyName.SelectedValue.ToInt();
				var type = this.ddlType.SelectedValue.ToByte();
				var surveyConductResult = BL.QualityAssurance.Admin.AddSurveyConduct(semesterID, institute, surveyID, type, openDate, closeDate, status, this.AdminIdentity.UserLoginHistoryID);
				switch (surveyConductResult)
				{
					case SurveyConduct.ValidationResults.None:
						throw new InvalidOperationException();
					case SurveyConduct.ValidationResults.NoRecordFound:
						this.AddNoRecordFoundAlert();
						break;
					case SurveyConduct.ValidationResults.AlreadyExists:
						this.alertControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Record already exists.");
						this.updateModalControlPanel.Update();
						break;
					case SurveyConduct.ValidationResults.InvalidOpenDate:
						this.alertControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Current or future dates allowed in \"From\" field.");
						this.updateModalControlPanel.Update();
						break;
					case SurveyConduct.ValidationResults.InvalidDateRange:
						this.alertControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Invalid open and close dates.");
						this.updateModalControlPanel.Update();
						break;
					case SurveyConduct.ValidationResults.Success:
						this.AddSuccessMessageHasBeenAdded("Survey conduct schedule");
						this.Refresh();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				this.AdminIdentity.Demand(AdminPermissions.QualityAssurance.ManageControlPanel, UserGroupPermission.PermissionValues.Allowed);
				var surveyConductResult = BL.QualityAssurance.Admin.UpdateSurveyConduct(surveyConductID.Value, openDate, closeDate, status, this.AdminIdentity.UserLoginHistoryID);
				switch (surveyConductResult)
				{
					case SurveyConduct.ValidationResults.None:
						throw new InvalidOperationException();
					case SurveyConduct.ValidationResults.NoRecordFound:
						this.AddNoRecordFoundAlert();
						break;
					case SurveyConduct.ValidationResults.AlreadyExists:
						this.alertControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Record already exists.");
						this.updateModalControlPanel.Update();
						break;
					case SurveyConduct.ValidationResults.InvalidOpenDate:
						this.alertControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Current or future dates allowed in \"From\" field.");
						this.updateModalControlPanel.Update();
						break;
					case SurveyConduct.ValidationResults.InvalidDateRange:
						this.alertControlPanel.AddMessage(AspireAlert.AlertTypes.Error, "Invalid open and close dates.");
						this.updateModalControlPanel.Update();
						break;
					case SurveyConduct.ValidationResults.Success:
						this.AddSuccessMessageHasBeenUpdated("Survey conduct schedule");
						this.Refresh();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		#endregion

		#region Page Specific Functions

		private void DisplayControlPanelModal(int? surveyConductID)
		{
			this.ddlSemesterForSave.FillSemesters();
			this.ddlInstituteForSave.FillInstitutesForAdmin();
			this.ddlInstituteForSave_OnSelectedIndexChanged(null,null);
			
			this.ddlType.FillEnums<SurveyConduct.SurveyConductTypes>();
			this.rblStatus.FillEnums<SurveyConduct.Statuses>();

			if (surveyConductID == null)
			{
				this.ddlSemesterForSave.ClearSelection();
				this.ddlSemesterForSave.Enabled = true;
				this.ddlInstituteForSave.ClearSelection();
				this.ddlInstituteForSave.Enabled = true;
				this.ddlInstituteForSave_OnSelectedIndexChanged(null,null);
				this.ddlSurveyName.ClearSelection();
				this.ddlSurveyName.Enabled = true;
				this.ddlType.ClearSelection();
				this.ddlType.Enabled = true;
				this.dpOpenDate.SelectedDate = null;
				this.dpCloseDate.SelectedDate = null;
				this.rblStatus.SetEnumValue(SurveyConduct.Statuses.Inactive);

				this.modalControlPanel.HeaderText = "Add";
				this.btnSave.Text = "Add";
				this.ViewState["SurveyConductID"] = null;
				this.modalControlPanel.Show();
			}
			else
			{
				var surveyConduct = BL.QualityAssurance.Admin.GetSurveyConduct(surveyConductID.Value);
				if (surveyConduct == null)
				{
					this.AddNoRecordFoundAlert();
					this.GetData(0);
					return;
				}
				this.ddlSemesterForSave.SelectedValue = surveyConduct.SemesterID.ToString();
				this.ddlSemesterForSave.Enabled = false;
				this.ddlInstituteForSave.SelectedValue = surveyConduct.Survey.InstituteID.ToString();
				this.ddlInstituteForSave.Enabled = false;
				this.ddlInstituteForSave_OnSelectedIndexChanged(null,null);
				this.ddlSurveyName.SelectedValue = surveyConduct.SurveyID.ToString();
				this.ddlSurveyName.Enabled = false;
				this.ddlType.SelectedValue = surveyConduct.SurveyConductType.ToString();
				this.ddlType.Enabled = false;
				this.dpOpenDate.SelectedDate = surveyConduct.OpenDate;
				this.dpCloseDate.SelectedDate = surveyConduct.CloseDate;
				this.rblStatus.SetEnumValue(surveyConduct.StatusEnum);

				this.modalControlPanel.HeaderText = "Edit";
				this.btnSave.Text = "Save";
				this.ViewState["SurveyConductID"] = surveyConductID;
				this.modalControlPanel.Show();
			}
		}

		private void GetData(int pageIndex)
		{
			var semesterID = this.ddlSemester.SelectedValue.ToNullableShort();
			var instituteID = this.ddlInstitute.SelectedValue.ToNullableInt();
			int virtualItemCount;
			var surveyConduct = BL.QualityAssurance.Admin.GetSurveyConducts(pageIndex, this.gvControlPanel.PageSize, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection(), semesterID, instituteID, out virtualItemCount);
			this.gvControlPanel.DataBind(surveyConduct, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}

		#endregion

		
	}
}