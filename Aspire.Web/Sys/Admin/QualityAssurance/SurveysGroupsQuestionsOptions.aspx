<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="SurveysGroupsQuestionsOptions.aspx.cs" Inherits="Aspire.Web.Sys.Admin.QualityAssurance.SurveysGroupsQuestionsOptions" %>

<%@ Import Namespace="Aspire.Model.Entities" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<style type="text/css">
		.panel {
			margin-bottom: 10px;
		}

		.outerBorder {
			border: 1px darkgrey solid;
			padding: 5px;
		}
	</style>
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlInstituteIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Surveys:" AssociatedControlID="ddlSurveyIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlSurveyIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlSurveyIDFilter_OnSelectedIndexChanged" />
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<div class="outerBorder">
		<div class="alert"><strong>Survey Question Groups</strong></div>
		<div class="list-group">
			<div class="list-group-item" runat="server" id="divSurveyQuestionGroups">
				No groups found.
			</div>
		</div>
		<asp:Repeater runat="server" ID="repeaterQuestionGroups" OnItemCommand="repeaterQuestionGroups_OnItemCommand">
			<ItemTemplate>

				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="col-md-1">
							<aspire:AspireLinkButton runat="server" CommandName="GroupUp" Glyphicon="chevron_up" CausesValidation="False" EncryptedCommandArgument="<%# this.Eval(nameof(SurveyQuestionGroup.SurveyQuestionGroupID)) %>" />
							<aspire:AspireLinkButton runat="server" CommandName="GroupDown" Glyphicon="chevron_down" CausesValidation="False" EncryptedCommandArgument="<%# this.Eval(nameof(SurveyQuestionGroup.SurveyQuestionGroupID)) %>" />
						</div>
						<div class="col-md-10">
							<h3 class="panel-title"><%#:this.Eval("QuestionGroupName")%></h3>
						</div>
						<aspire:AspireLinkButton runat="server" CommandName="Edit" Glyphicon="edit" CausesValidation="False" EncryptedCommandArgument="<%# this.Eval(nameof(SurveyQuestionGroup.SurveyQuestionGroupID)) %>" />
						<aspire:AspireLinkButton runat="server" CommandName="Delete" Glyphicon="remove" CausesValidation="False" EncryptedCommandArgument="<%# this.Eval(nameof(SurveyQuestionGroup.SurveyQuestionGroupID)) %>" ConfirmMessage='<%# this.Eval("QuestionGroupName","Are you sure you want to delete survey question group \"{0}\"?") %>' />
					</div>

					<div class="panel-body">

						<div class="list-group">
							<div class="list-group-item" runat="server" visible='<%# !((IEnumerable<SurveyQuestion>)this.Eval("SurveyQuestions")).Any() %>'>
								No questions found.
							</div>
						</div>
						<asp:Repeater runat="server" ID="repeaterQuestions" DataSource='<%# ((IEnumerable<SurveyQuestion>)this.Eval("SurveyQuestions")).OrderBy(q=> q.DisplayIndex) %>' Visible='<%# ((IEnumerable<SurveyQuestion>)this.Eval("SurveyQuestions")).Any() %>' OnItemCommand="repeaterQuestions_OnItemCommand">
							<ItemTemplate>
								<div class="list-group">
									<div class="list-group-item">
										<div class="form-horizontal">
											<div class="form-group">
												<div class="col-md-6">
													<span class="form-control-static col-md-10"><%#: this.Eval("QuestionNo") %>. <%#: this.Eval("QuestionText") %></span>
													<div class="form-control-static col-md-2">
														<aspire:AspireLinkButton runat="server" CommandName="Up" Glyphicon="chevron_up" CausesValidation="False" EncryptedCommandArgument="<%# this.Eval(nameof(SurveyQuestion.SurveyQuestionID)) %>" />
														<aspire:AspireLinkButton runat="server" CommandName="Down" Glyphicon="chevron_down" CausesValidation="False" EncryptedCommandArgument="<%# this.Eval(nameof(SurveyQuestion.SurveyQuestionID)) %>" />
														<aspire:AspireLinkButton runat="server" CommandName="Edit" Glyphicon="edit" CausesValidation="False" EncryptedCommandArgument="<%# this.Eval(nameof(SurveyQuestion.SurveyQuestionID)) %>" />
														<aspire:AspireLinkButton runat="server" CommandName="Delete" Glyphicon="remove" CausesValidation="False" EncryptedCommandArgument="<%# this.Eval(nameof(SurveyQuestion.SurveyQuestionID)) %>" ConfirmMessage='<%# this.Eval("QuestionText","Are you sure you want to delete survey question \"{0}\"?") %>' />
													</div>
												</div>
												<div class="col-md-6" runat="server" visible='<%# (SurveyQuestion.QuestionTypes)this.Eval(nameof(SurveyQuestion.QuestionTypeEnum))==SurveyQuestion.QuestionTypes.Options && ((IEnumerable<SurveyQuestionOption>)this.Eval("SurveyQuestionOptions")).Any()%>'>
													<div class="col-md-10">
														<aspire:AspireRadioButtonList runat="server" ID="rblOptions" ValidationGroup="Survey" DataSource='<%# this.Eval("SurveyQuestionOptions") %>' DataTextField="OptionTextAndWeightage" DataValueField="SurveyQuestionOptionID" CausesValidation="False" RepeatDirection="Horizontal" RepeatLayout="Flow" />
													</div>
													<div class="form-control-static col-md-2">
														<aspire:AspireLinkButton runat="server" CommandName="EditOption" ID="btnEditOption" Glyphicon="edit" CausesValidation="False" />
														<aspire:AspireLinkButton runat="server" CommandName="DeleteOption" ID="btnDeleteOption" Glyphicon="remove" CausesValidation="False" ConfirmMessage='<%# this.Eval("QuestionText","Are you sure you want to delete an OPTION of selected question \"{0}\"?") %>' />
													</div>
												</div>
												<div class="col-md-6" runat="server" visible='<%# (SurveyQuestion.QuestionTypes)this.Eval(nameof(SurveyQuestion.QuestionTypeEnum))==SurveyQuestion.QuestionTypes.Options && !((IEnumerable<SurveyQuestionOption>)this.Eval("SurveyQuestionOptions")).Any() %>'>
													<div class="form-control-static col-md-12">
														<span>No options found.</span>
													</div>
												</div>
												<div class="col-md-6  col-md-offset-6" runat="server" visible='<%# (SurveyQuestion.QuestionTypes)this.Eval(nameof(SurveyQuestion.QuestionTypeEnum))==SurveyQuestion.QuestionTypes.Options && ((IEnumerable<SurveyQuestionOption>)this.Eval("SurveyQuestionOptions")).Any() %>'>
													<div class="col-md-12">
														<label class="alert alert-info">Select an option first, then you can edit or delete option.</label>
													</div>
												</div>

												<div class="col-md-12" runat="server" visible="<%# (SurveyQuestion.QuestionTypes)this.Eval(nameof(SurveyQuestion.QuestionTypeEnum))==SurveyQuestion.QuestionTypes.Text %>">
													<div class="col-md-12">
														<aspire:AspireTextBox runat="server" ValidationGroup="Survey" TextMode="MultiLine" ReadOnly="True" />
													</div>
												</div>

											</div>
											<div class="form-group">
												<div class="col-md-6 col-md-offset-6">
													<div class="col-md-12">
														<aspire:AspireButton runat="server" ID="btnAddOptions" Text="Add Options" Glyphicon="plus" ButtonType="Success" CausesValidation="False" Visible="<%# (SurveyQuestion.QuestionTypes)this.Eval(nameof(SurveyQuestion.QuestionTypeEnum))==SurveyQuestion.QuestionTypes.Options %>" EncryptedCommandArgument="<%#this.Eval(nameof(SurveyQuestionOption.SurveyQuestionID)) %>" OnClick="btnAddOptions_OnClick" />
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</ItemTemplate>
						</asp:Repeater>
						<p></p>
						<div class="form-horizontal">
							<div class="form-group" style="margin-bottom: 0">
								<div class="col-md-12">
									<aspire:AspireButton runat="server" ID="btnAddQuestion" Text="Add Questions" Glyphicon="plus" ButtonType="Success" CausesValidation="False" EncryptedCommandArgument="<%#this.Eval(nameof(SurveyQuestionGroup.SurveyQuestionGroupID)) %>" OnClick="btnAddQuestion_OnClick" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</ItemTemplate>
		</asp:Repeater>
		<aspire:AspireButton runat="server" ID="btnAddGroup" Text="Add Group" Glyphicon="plus" ButtonType="Success" CausesValidation="False" OnClick="btnAddGroup_OnClick" />
	</div>

	<aspire:AspireModal runat="server" ID="modalGroups">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updateModalGroups" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertModalGroups" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="lblInstitute" CssClass="col-md-3" />
							<div class="form-control-static col-md-9">
								<aspire:Label runat="server" ID="lblInstitute" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Survey Name:" AssociatedControlID="lblSurveyName" CssClass="col-md-3" />
							<div class="form-control-static col-md-9">
								<aspire:Label runat="server" ID="lblSurveyName" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Group Name:" AssociatedControlID="tbQuestionGroupName" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbQuestionGroupName" MaxLength="100" ValidationGroup="ReqGroup" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbQuestionGroupName" ErrorMessage="This field is required." ValidationGroup="ReqGroup" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSaveGroup" Text="Save" ButtonType="Primary" OnClick="btnSaveGroup_OnClick" ValidationGroup="ReqGroup" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>

	<aspire:AspireModal runat="server" ID="modalQuestions">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updateModalQuestions" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertModalQuestions" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Question Group:" AssociatedControlID="lblQuestionGroup" CssClass="col-md-3" />
							<div class="form-control-static col-md-9">
								<aspire:Label runat="server" ID="lblQuestionGroup" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Question No:" AssociatedControlID="tbQuestionNo" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbQuestionNo" MaxLength="50" ValidationGroup="ReqQuestion" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbQuestionNo" ErrorMessage="This field is required." ValidationGroup="ReqQuestion" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Question Text:" AssociatedControlID="tbQuestionText" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbQuestionText" ValidationGroup="ReqQuestion" TextMode="MultiLine" MaxLength="2000" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbQuestionText" ErrorMessage="This field is required." ValidationGroup="ReqQuestion" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Question Type:" AssociatedControlID="ddlQuestionType" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireDropDownList runat="server" ID="ddlQuestionType" ValidationGroup="ReqQuestion" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="ddlQuestionType" ErrorMessage="This field is required." ValidationGroup="ReqQuestion" />
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSaveQuestion" Text="Save" ButtonType="Primary" OnClick="btnSaveQuestion_OnClick" ValidationGroup="ReqQuestion" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>

	<aspire:AspireModal runat="server" ID="modalOptions">
		<BodyTemplate>
			<asp:UpdatePanel runat="server" ID="updateModalOptions" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireAlert runat="server" ID="alertModalOptions" />
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Question:" AssociatedControlID="lblQuestion" CssClass="col-md-3" />
							<div class="form-control-static col-md-9">
								<aspire:Label runat="server" ID="lblQuestion" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Option Text:" AssociatedControlID="tbOptionText" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbOptionText" ValidationGroup="ReqOption" MaxLength="1000" />
								<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbOptionText" ErrorMessage="This field is required." ValidationGroup="ReqOption" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Weightage:" AssociatedControlID="tbWeightage" CssClass="col-md-3" />
							<div class="col-md-9">
								<aspire:AspireTextBox runat="server" ID="tbWeightage" ValidationGroup="ReqOption" TextMode="MultiLine" />
								<%--<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="tbWeightage" ErrorMessage="This field is required." ValidationGroup="ReqOption" />--%>
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</BodyTemplate>
		<FooterTemplate>
			<asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<aspire:AspireButton runat="server" ID="btnSaveOptions" Text="Save" ButtonType="Primary" OnClick="btnSaveOptions_OnClick" ValidationGroup="ReqOption" />
					<aspire:AspireModalCloseButton runat="server">Cancel</aspire:AspireModalCloseButton>
				</ContentTemplate>
			</asp:UpdatePanel>
		</FooterTemplate>
	</aspire:AspireModal>
</asp:Content>
