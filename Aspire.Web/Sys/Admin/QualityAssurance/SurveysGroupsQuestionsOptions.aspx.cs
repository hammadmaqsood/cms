﻿using Aspire.BL.Core.QualityAssurance.Admin;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.QualityAssurance
{
	[AspirePage("EE8DABFF-292A-43A8-9856-7F0147AE5ACF", UserTypes.Admins, "~/Sys/Admin/QualityAssurance/SurveysGroupsQuestionsOptions.aspx", "Surveys Groups, Questions & Options", true, AspireModules.QualityAssurance)]
	public partial class SurveysGroupsQuestionsOptions : AdminsPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_briefcase);
		public override string PageTitle => "Surveys Groups, Questions & Options";
		protected override IReadOnlyDictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]> PermissionsRequired => new Dictionary<AdminPermissionType, UserGroupPermission.PermissionValues[]>
		{
			{ AdminPermissions.QualityAssurance.Module, new [] {UserGroupPermission.PermissionValues.Allowed, }}
		};

		public static string GetPageUrl(int instituteID, int surveyID)
		{
			return GetPageUrl<SurveysGroupsQuestionsOptions>().AttachQueryParams("InstituteID", instituteID, "SurveyID", surveyID);
		}

		public int? InstituteID => this.Request.GetParameterValue<int>("InstituteID");
		public int? SurveyID => this.Request.GetParameterValue<int>("SurveyID");

		private void Refresh()
		{
			Redirect(GetPageUrl(this.InstituteIDFilter, this.SurveyIDFilter));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlInstituteIDFilter.FillInstitutesForAdmin();
				this.ddlInstituteIDFilter.SetSelectedValueIfNotPostback("InstituteID");
				if (this.InstituteID != null)
					this.ddlInstituteIDFilter.SelectedValue = this.InstituteID.ToString();
				this.ddlInstituteIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		#region Filters Events

		protected void ddlInstituteIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToInt();
			var surveys = BL.Core.QualityAssurance.Admin.SurveyGroupQuestionOption.GetSurveys(instituteID, this.AdminIdentity.LoginSessionGuid);
			this.ddlSurveyIDFilter.DataBind(surveys);
			this.ddlSurveyIDFilter.SetSelectedValueIfNotPostback("SurveyID");
			this.ddlSurveyIDFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSurveyIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData();
		}

		#endregion

		#region Get Survey Tables Data

		private void GetData()
		{
			var survey = BL.Core.QualityAssurance.Admin.SurveyGroupQuestionOption.GetSurveysGroupsQuestionsOptions(this.InstituteIDFilter, this.SurveyIDFilter, this.AdminIdentity.LoginSessionGuid);
			if (!survey.SurveyQuestionGroups.Any())
			{
				this.repeaterQuestionGroups.DataBind(null);
				this.divSurveyQuestionGroups.Visible = true;
				this.repeaterQuestionGroups.Visible = false;
			}
			else
			{
				this.repeaterQuestionGroups.DataBind(survey.SurveyQuestionGroups.OrderBy(sqg => sqg.DisplayIndex));
				this.repeaterQuestionGroups.Visible = true;
				this.divSurveyQuestionGroups.Visible = false;
			}
		}

		#endregion

		#region Survey Question Groups

		protected void btnAddGroup_OnClick(object sender, EventArgs e)
		{
			this.DisplayModalGroups(null);
		}

		protected void btnSaveGroup_OnClick(object sender, EventArgs e)
		{
			var surveyQuestionGroupID = (int?)this.ViewState[nameof(SurveyQuestion.SurveyQuestionGroupID)];
			var questionGroupName = this.tbQuestionGroupName.Text;
			if (surveyQuestionGroupID == null)
			{
				var result = BL.Core.QualityAssurance.Admin.SurveyGroupQuestionOption.AddSurveyQuestionGroup(this.SurveyIDFilter, this.InstituteIDFilter, questionGroupName, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case SurveyGroupQuestionOption.AddSurveyQuestionGroupsStatuses.AlreadyExists:
						this.alertModalGroups.AddMessage(AspireAlert.AlertTypes.Error, "Survey question group already exists.");
						this.updateModalGroups.Update();
						break;
					case SurveyGroupQuestionOption.AddSurveyQuestionGroupsStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Survey question group");
						this.Refresh();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var result = BL.Core.QualityAssurance.Admin.SurveyGroupQuestionOption.UpdateSurveyQuestionGroup(surveyQuestionGroupID.Value, this.InstituteIDFilter, questionGroupName, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case SurveyGroupQuestionOption.UpdateSurveysQuestionGroupStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						Redirect<SurveysGroupsQuestionsOptions>();
						break;
					case SurveyGroupQuestionOption.UpdateSurveysQuestionGroupStatuses.AlreadyExists:
						this.alertModalGroups.AddMessage(AspireAlert.AlertTypes.Error, "Survey question group already exists.");
						this.updateModalGroups.Update();
						break;
					case SurveyGroupQuestionOption.UpdateSurveysQuestionGroupStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Survey question group");
						this.Refresh();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		protected void repeaterQuestionGroups_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					this.DisplayModalGroups(e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					var surveyQuestionGroupID = e.DecryptedCommandArgumentToInt();
					var result = BL.Core.QualityAssurance.Admin.SurveyGroupQuestionOption.DeleteSurveyQuestionGroup(surveyQuestionGroupID, this.InstituteIDFilter, this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case SurveyGroupQuestionOption.DeleteSurveysQuestionGroupStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.Refresh();
							break;
						case SurveyGroupQuestionOption.DeleteSurveysQuestionGroupStatuses.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Survey question group");
							this.Refresh();
							break;
						case SurveyGroupQuestionOption.DeleteSurveysQuestionGroupStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Survey question group");
							this.Refresh();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					Redirect<SurveysGroupsQuestionsOptions>();
					break;
			}
		}

		private void DisplayModalGroups(int? surveyQuestionGroupID)
		{
			if (surveyQuestionGroupID == null)
			{
				this.lblInstitute.Text = this.ddlInstituteIDFilter.SelectedItem.Text;
				this.lblSurveyName.Text = this.ddlSurveyIDFilter.SelectedItem.Text;
				this.tbQuestionGroupName.Text = null;

				this.modalGroups.HeaderText = "Add Question Group";
				this.btnSaveGroup.Text = @"Add";
				this.ViewState[nameof(SurveyQuestionGroup.SurveyQuestionGroupID)] = null;
				this.modalGroups.Show();
			}
			else
			{
				var surveyQuestionGroup = BL.Core.QualityAssurance.Admin.SurveyGroupQuestionOption.GetSurveyQuestionGroup(surveyQuestionGroupID.Value, this.InstituteIDFilter, this.AdminIdentity.LoginSessionGuid);
				this.lblInstitute.Text = this.ddlInstituteIDFilter.SelectedItem.Text;
				this.lblSurveyName.Text = this.ddlSurveyIDFilter.SelectedItem.Text;
				this.tbQuestionGroupName.Text = surveyQuestionGroup.QuestionGroupName;

				this.modalGroups.HeaderText = "Edit Question Group";
				this.btnSaveGroup.Text = @"Save";
				this.ViewState[nameof(SurveyQuestionGroup.SurveyQuestionGroupID)] = surveyQuestionGroupID;
				this.modalGroups.Show();
			}
		}

		#endregion

		#region Survey Questions

		protected void btnAddQuestion_OnClick(object sender, EventArgs e)
		{
			var aspireBtnSaveQuestion = (AspireButton)sender;
			var surveyQuestionGroupID = aspireBtnSaveQuestion.EncryptedCommandArgument.ToInt();
			this.DisplayModalQuestions(surveyQuestionGroupID, null);

		}

		protected void btnSaveQuestion_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var surveyQuestionGroupID = (int?)this.ViewState[nameof(SurveyQuestionGroup.SurveyQuestionGroupID)];
			var surveyQuestionID = (int?)this.ViewState[nameof(SurveyQuestion.SurveyQuestionID)];
			var questionNo = this.tbQuestionNo.Text;
			var questionText = this.tbQuestionText.Text;
			var questionTypeEnum = this.ddlQuestionType.GetSelectedEnumValue<SurveyQuestion.QuestionTypes>();
			if (surveyQuestionID == null)
			{
				if (surveyQuestionGroupID == null)
					return;
				var result = BL.Core.QualityAssurance.Admin.SurveyGroupQuestionOption.AddSurveyQuestion(this.InstituteIDFilter, surveyQuestionGroupID.Value, questionNo, questionText, questionTypeEnum, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case SurveyGroupQuestionOption.AddSurveyQuestionStatuses.AlreadyExists:
						this.alertModalQuestions.AddMessage(AspireAlert.AlertTypes.Error, "Question already exists with specified No: or text.");
						this.updateModalQuestions.Update();
						break;
					case SurveyGroupQuestionOption.AddSurveyQuestionStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Survey question");
						this.Refresh();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var result = BL.Core.QualityAssurance.Admin.SurveyGroupQuestionOption.UpdateSurveyQuestion(this.InstituteIDFilter, surveyQuestionID.Value, questionNo, questionText, questionTypeEnum, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case SurveyGroupQuestionOption.UpdateSurveyQuestionStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.Refresh();
						break;
					case SurveyGroupQuestionOption.UpdateSurveyQuestionStatuses.AlreadyExists:
						this.alertModalQuestions.AddMessage(AspireAlert.AlertTypes.Error, "Question already exists with specified No: or text.");
						this.updateModalQuestions.Update();
						break;
					case SurveyGroupQuestionOption.UpdateSurveyQuestionStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Survey question");
						this.Refresh();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		protected void repeaterQuestions_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					this.DisplayModalQuestions(null, e.DecryptedCommandArgumentToInt());
					break;
				case "Delete":
					var surveyQuestionID = e.DecryptedCommandArgumentToInt();
					var result = BL.Core.QualityAssurance.Admin.SurveyGroupQuestionOption.DeleteSurveyQuestion(surveyQuestionID, this.InstituteIDFilter, this.AdminIdentity.LoginSessionGuid);
					switch (result)
					{
						case SurveyGroupQuestionOption.DeleteSurveysQuestionStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.Refresh();
							break;
						case SurveyGroupQuestionOption.DeleteSurveysQuestionStatuses.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Survey question");
							this.Refresh();
							break;
						case SurveyGroupQuestionOption.DeleteSurveysQuestionStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Survey question");
							this.Refresh();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					Redirect<SurveysGroupsQuestionsOptions>();
					break;
				case "EditOption":
					var aspireRblOptionsEdit = (AspireRadioButtonList)e.Item.FindControl("rblOptions");
					if (aspireRblOptionsEdit == null)
						return;
					if (aspireRblOptionsEdit.SelectedItem == null)
					{
						this.AddErrorAlert("Select an option first, then you can edit or delete option.");
						this.Refresh();
						return;
					}
					var surveyQuestionOptionIDforEdit = aspireRblOptionsEdit.SelectedValue.ToInt();
					this.DisplayModalOptions(null, surveyQuestionOptionIDforEdit);
					break;
				case "DeleteOption":
					var aspireRblOptionsDelete = (AspireRadioButtonList)e.Item.FindControl("rblOptions");
					if (aspireRblOptionsDelete == null)
						return;
					if (aspireRblOptionsDelete.SelectedItem == null)
					{
						this.AddErrorAlert("Select an option first, then you can edit or delete option.");
						this.Refresh();
						return;
					}
					var surveyQuestionOptionIDforDelete = aspireRblOptionsDelete.SelectedValue.ToInt();
					var resultOptionDelete = BL.Core.QualityAssurance.Admin.SurveyGroupQuestionOption.DeleteSurveyOption(surveyQuestionOptionIDforDelete, this.InstituteIDFilter, this.AdminIdentity.LoginSessionGuid);
					switch (resultOptionDelete)
					{
						case SurveyGroupQuestionOption.DeleteSurveysOptionStatuses.NoRecordFound:
							this.AddNoRecordFoundAlert();
							this.Refresh();
							break;
						case SurveyGroupQuestionOption.DeleteSurveysOptionStatuses.ChildRecordExists:
							this.AddErrorMessageCannotBeDeletedBecauseChildRecordExists("Survey question option");
							this.Refresh();
							break;
						case SurveyGroupQuestionOption.DeleteSurveysOptionStatuses.Success:
							this.AddSuccessMessageHasBeenDeleted("Survey question option");
							this.Refresh();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
			}
		}

		private void DisplayModalQuestions(int? surveyQuestionGroupID, int? surveyQuestionID)
		{
			this.ddlQuestionType.FillEnums<SurveyQuestion.QuestionTypes>();
			if (surveyQuestionID == null)
			{
				if (surveyQuestionGroupID == null)
					return;
				var surveyQuestionGroup = BL.Core.QualityAssurance.Admin.SurveyGroupQuestionOption.GetSurveyQuestionGroup(surveyQuestionGroupID.Value, this.InstituteIDFilter, this.AdminIdentity.LoginSessionGuid);
				this.lblQuestionGroup.Text = surveyQuestionGroup.QuestionGroupName;
				this.tbQuestionNo.Text = null;
				this.tbQuestionText.Text = null;
				this.ddlQuestionType.ClearSelection();

				this.modalQuestions.HeaderText = "Add Question";
				this.btnSaveGroup.Text = @"Add";
				this.ViewState[nameof(SurveyQuestion.SurveyQuestionID)] = null;
				this.ViewState[nameof(SurveyQuestionGroup.SurveyQuestionGroupID)] = surveyQuestionGroupID;
				this.modalQuestions.Show();
			}
			else
			{
				var surveyQuestion = BL.Core.QualityAssurance.Admin.SurveyGroupQuestionOption.GetSurveyQuestion(surveyQuestionID.Value, this.InstituteIDFilter, this.AdminIdentity.LoginSessionGuid);
				this.lblQuestionGroup.Text = surveyQuestion.SurveyQuestionGroup.QuestionGroupName;
				this.tbQuestionNo.Text = surveyQuestion.QuestionNo;
				this.tbQuestionText.Text = surveyQuestion.QuestionText;
				this.ddlQuestionType.SetEnumValue(surveyQuestion.QuestionTypeEnum);

				this.modalQuestions.HeaderText = "Edit Question";
				this.btnSaveGroup.Text = @"Save";
				this.ViewState[nameof(SurveyQuestion.SurveyQuestionID)] = surveyQuestionID;
				this.ViewState[nameof(SurveyQuestionGroup.SurveyQuestionGroupID)] = null;
				this.modalQuestions.Show();
			}
		}

		#endregion

		#region Survey Options

		protected void btnAddOptions_OnClick(object sender, EventArgs e)
		{
			var aspireBtnAddOptions = (AspireButton)sender;
			var surveyQuestionID = aspireBtnAddOptions.EncryptedCommandArgument.ToInt();
			this.DisplayModalOptions(surveyQuestionID, null);
		}

		protected void btnSaveOptions_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var surveyQuestionID = (int?)this.ViewState[nameof(SurveyQuestion.SurveyQuestionID)];
			var surveyQuestionOptionID = (int?)this.ViewState[nameof(SurveyQuestionOption.SurveyQuestionOptionID)];
			var optionText = this.tbOptionText.Text;
			var weightage = this.tbWeightage.Text.ToNullableDouble();
			if (surveyQuestionOptionID == null)
			{
				if (surveyQuestionID == null)
					return;

				var result = BL.Core.QualityAssurance.Admin.SurveyGroupQuestionOption.AddSurveyOption(this.InstituteIDFilter, surveyQuestionID.Value, optionText, weightage, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case SurveyGroupQuestionOption.AddSurveyOptionStatuses.AlreadyExists:
						this.alertModalOptions.AddMessage(AspireAlert.AlertTypes.Error, "Option of question is already exists with specified text or weightage.");
						this.updateModalOptions.Update();
						break;
					case SurveyGroupQuestionOption.AddSurveyOptionStatuses.Success:
						this.AddSuccessMessageHasBeenAdded("Survey question option");
						this.Refresh();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			else
			{
				var result = BL.Core.QualityAssurance.Admin.SurveyGroupQuestionOption.UpdateSurveyOption(this.InstituteIDFilter, surveyQuestionOptionID.Value, optionText, weightage, this.AdminIdentity.LoginSessionGuid);
				switch (result)
				{
					case SurveyGroupQuestionOption.UpdateSurveyOptionStatuses.NoRecordFound:
						this.AddNoRecordFoundAlert();
						this.Refresh();
						break;
					case SurveyGroupQuestionOption.UpdateSurveyOptionStatuses.AlreadyExists:
						this.alertModalOptions.AddMessage(AspireAlert.AlertTypes.Error, "Option of question is already exists with specified text or weightage.");
						this.updateModalOptions.Update();
						break;
					case SurveyGroupQuestionOption.UpdateSurveyOptionStatuses.Success:
						this.AddSuccessMessageHasBeenUpdated("Survey question option");
						this.Refresh();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		private void DisplayModalOptions(int? surveyQuestionID, int? surveyQuestionOptionID)
		{
			if (surveyQuestionOptionID == null)
			{
				if (surveyQuestionID == null)
					return;
				var surveyQuestion = BL.Core.QualityAssurance.Admin.SurveyGroupQuestionOption.GetSurveyQuestion(surveyQuestionID.Value, this.InstituteIDFilter, this.AdminIdentity.LoginSessionGuid);
				this.lblQuestion.Text = $"{surveyQuestion.QuestionNo}. {surveyQuestion.QuestionText}";
				this.tbOptionText.Text = null;
				this.tbWeightage.Text = null;

				this.modalOptions.HeaderText = "Add Option";
				this.btnSaveOptions.Text = @"Add";
				this.ViewState[nameof(SurveyQuestionOption.SurveyQuestionOptionID)] = null;
				this.ViewState[nameof(SurveyQuestion.SurveyQuestionID)] = surveyQuestionID;
				this.modalOptions.Show();
			}
			else
			{
				var surveyQuestionOption = BL.Core.QualityAssurance.Admin.SurveyGroupQuestionOption.GetSurveyOption(surveyQuestionOptionID.Value, this.InstituteIDFilter, this.AdminIdentity.LoginSessionGuid);
				this.lblQuestion.Text = $"{surveyQuestionOption.SurveyQuestion.QuestionNo}. {surveyQuestionOption.SurveyQuestion.QuestionText}";
				this.tbOptionText.Text = surveyQuestionOption.OptionText;
				this.tbWeightage.Text = surveyQuestionOption.Weightage.ToString();

				this.modalOptions.HeaderText = "Edit Option";
				this.btnSaveOptions.Text = @"Save";
				this.ViewState[nameof(SurveyQuestionOption.SurveyQuestionOptionID)] = surveyQuestionOptionID;
				this.ViewState[nameof(SurveyQuestion.SurveyQuestionID)] = null;
				this.modalOptions.Show();
			}
		}

		#endregion

		private int InstituteIDFilter => this.ddlInstituteIDFilter.SelectedValue.ToInt();
		private int SurveyIDFilter => this.ddlSurveyIDFilter.SelectedValue.ToInt();
	}
}