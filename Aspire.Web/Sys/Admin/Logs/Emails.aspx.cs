﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Admin.Logs
{
	[AspirePage("F448F518-2135-47AA-A8AA-5D58A8330BFF", UserTypes.MasterAdmin, "~/Sys/Admin/Logs/Emails.aspx", "Emails", true, AspireModules.Logs)]
	public partial class Emails : MasterAdminPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.envelope);
		public override string PageTitle => "Emails";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(nameof(BL.Core.Logs.Admin.Emails.Email.CreatedDate), System.Web.UI.WebControls.SortDirection.Descending);
				this.ddlEmailTypeFilter.FillGuidEnums<Model.Entities.Email.EmailTypes>(CommonListItems.All);
				this.ddlStatusFilters.FillGuidEnums<Model.Entities.Email.Statuses>(CommonListItems.All);
				this.ddlEmailTypeFilter_SelectedIndexChanged(null, null);
			}
		}

		protected void ddlEmailTypeFilter_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlStatusFilters_SelectedIndexChanged(null, null);
		}

		protected void ddlStatusFilters_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearchFilters_Click(null, null);
		}

		protected void btnSearchFilters_Click(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void gvEmails_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvEmails_PageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvEmails.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvEmails_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		private void GetData(int pageIndex)
		{
			var emailTypeEnum = this.ddlEmailTypeFilter.GetSelectedNullableGuidEnum<Model.Entities.Email.EmailTypes>();
			var statusEnum = this.ddlStatusFilters.GetSelectedNullableGuidEnum<Model.Entities.Email.Statuses>();
			var from = this.dtpFromFilters.SelectedDate;
			var to = this.dtpToFilters.SelectedDate;
			var searchText = this.tbSearchFilters.Text;
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			var (emails, virtualItemCount) = BL.Core.Logs.Admin.Emails.GetEmails(emailTypeEnum, statusEnum, from, to, searchText, pageIndex, this.gvEmails.PageSize, sortExpression, sortDirection, this.AdminIdentity.LoginSessionGuid);
			this.gvEmails.DataBind(emails, pageIndex, virtualItemCount, sortExpression, sortDirection);
		}

		protected void btnSendTestEmail_Click(object sender, EventArgs e)
		{
			var createdDate = DateTime.Now;
			var expiryDate = createdDate.AddMinutes(5);
			BL.Core.Common.EmailsManagement.EmailHelper.AddEmailAndSaveChanges(Model.Entities.Email.EmailTypes.TestEmail,
				subject: "Test Email",
				body: $"This is test email generated on {createdDate} and it must be received before {expiryDate}.",
				isBodyHtml: false,
				priorityEnum: Model.Entities.Email.Priorities.Highest,
				createdDate: createdDate,
				expiryDate: expiryDate,
				loginSessionGuid: this.AdminIdentity.LoginSessionGuid,
				toList: new[] { (this.AdminIdentity.Name, this.AdminIdentity.Email) },
				attachmentList: new[] { ("Test Attachment.txt", System.Text.Encoding.UTF8.GetBytes("This is test file.")) });
			this.AddSuccessAlert("Email has been queued.");
			this.GetData(this.gvEmails.PageIndex);
		}
	}
}