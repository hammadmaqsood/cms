﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Emails.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Logs.Emails" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="ddlEmailTypeFilter" />
				<aspire:AspireDropDownList runat="server" ID="ddlEmailTypeFilter" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlEmailTypeFilter_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Status:" AssociatedControlID="ddlStatusFilters" />
				<aspire:AspireDropDownList runat="server" ID="ddlStatusFilters" ValidationGroup="Filters" AutoPostBack="True" OnSelectedIndexChanged="ddlStatusFilters_SelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="From (Created On):" AssociatedControlID="dtpFromFilters" />
				<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpFromFilters" ValidationGroup="Filters" />
				<aspire:AspireDateTimeValidator AllowNull="True" runat="server" ControlToValidate="dtpFromFilters" ValidationGroup="Filters" InvalidDataErrorMessage="Invalid Date." RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Invalid Date range." />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="To (Created On):" AssociatedControlID="dtpToFilters" />
				<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpToFilters" ValidationGroup="Filters" />
				<aspire:AspireDateTimeValidator AllowNull="True" runat="server" ControlToValidate="dtpToFilters" ValidationGroup="Filters" InvalidDataErrorMessage="Invalid Date." RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Invalid Date range." />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Search:" AssociatedControlID="tbSearchFilters" />
				<asp:Panel runat="server" CssClass="input-group" DefaultButton="btnSearchFilters">
					<aspire:AspireTextBox runat="server" ID="tbSearchFilters" CssClass="form-control" />
					<span class="input-group-btn">
						<aspire:AspireButton ValidationGroup="Search" ButtonType="Primary" Glyphicon="search" OnClick="btnSearchFilters_Click" ID="btnSearchFilters" runat="server" />
					</span>
				</asp:Panel>
			</div>
		</div>
		<div class="col-md-offset-6 col-md-3">
			<div class="form-group pull-right">
				<div>
					<aspire:AspireLabel runat="server" Text="&nbsp;" AssociatedControlID="btnSendTestEmail" />
				</div>
				<aspire:AspireButton runat="server" ButtonType="Success" FontAwesomeIcon="regular_check_circle" Text="Send Test Email" CausesValidation="false" ConfirmMessage="Are you sure you want to send test email to yourself?" ID="btnSendTestEmail" OnClick="btnSendTestEmail_Click" />
			</div>
		</div>
	</div>
	<aspire:AspireGridView runat="server" ItemType="Aspire.BL.Core.Logs.Admin.Emails.Email" ID="gvEmails" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gvEmails_PageIndexChanging" OnPageSizeChanging="gvEmails_PageSizeChanging" OnSorting="gvEmails_Sorting">
		<Columns>
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" Target="_blank" Text="View" NavigateUrl="<%# Aspire.Web.Sys.Admin.Logs.Email.GetPageUrl(Item.EmailID) %>" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="To">
				<ItemTemplate>
					<%#: string.Join(" | ", Item.To.Select(ee=> string.IsNullOrWhiteSpace(ee.DisplayName)? ee.EmailAddress: $"{ee.DisplayName} <{ee.EmailAddress}>" )) %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Subject" SortExpression="Subject">
				<ItemTemplate>
					<%#: Item.Subject %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Created Date" SortExpression="CreatedDate">
				<ItemTemplate>
					<%#: Item.CreatedDate %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Expiry Date" SortExpression="ExpiryDate">
				<ItemTemplate>
					<%#: Item.ExpiryDate.ToNAIfNullOrEmpty() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Priority" SortExpression="PriorityEnum">
				<ItemTemplate>
					<%#: Item.PriorityEnum.ToFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Attempts" SortExpression="AttemptsCount">
				<ItemTemplate>
					<%#: Item.AttemptsCount %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Status" SortExpression="Status">
				<ItemTemplate>
					<%#: Item.StatusEnum.GetFullName() %>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
