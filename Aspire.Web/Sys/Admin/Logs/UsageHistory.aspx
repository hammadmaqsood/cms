﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="UsageHistory.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Logs.UsageHistory" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Page:" AssociatedControlID="ddlPage" />
				<aspire:AspireDropDownList runat="server" ID="ddlPage" ValidationGroup="Search" AutoPostBack="True" OnSelectedIndexChanged="ddlPage_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Post Back:" AssociatedControlID="ddlIsPostBack" />
				<aspire:AspireDropDownList runat="server" ID="ddlIsPostBack" ValidationGroup="Search" AutoPostBack="True" OnSelectedIndexChanged="ddlIsPostBack_OnSelectedIndexChanged" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Period:" AssociatedControlID="ddlPeriod" />
				<aspire:AspireDropDownList runat="server" ID="ddlPeriod" ValidationGroup="Search" AutoPostBack="True" OnSelectedIndexChanged="ddlPeriod_OnSelectedIndexChanged">
					<Items>
						<asp:ListItem>Last 5 Minutes</asp:ListItem>
						<asp:ListItem>Last 10 Minutes</asp:ListItem>
						<asp:ListItem>Last 15 Minutes</asp:ListItem>
						<asp:ListItem>Last 30 Minutes</asp:ListItem>
						<asp:ListItem>Last 45 Minutes</asp:ListItem>
						<asp:ListItem>Last 1 Hour</asp:ListItem>
						<asp:ListItem>Last 2 Hours</asp:ListItem>
						<asp:ListItem>Last 3 Hours</asp:ListItem>
						<asp:ListItem>Last 5 Hours</asp:ListItem>
						<asp:ListItem>Last 8 Hours</asp:ListItem>
						<asp:ListItem>Last 12 Hours</asp:ListItem>
						<asp:ListItem Selected="True">Last 1 Day</asp:ListItem>
						<asp:ListItem>Last 2 Days</asp:ListItem>
						<asp:ListItem>Last 3 Days</asp:ListItem>
						<asp:ListItem>Last 15 Days</asp:ListItem>
						<asp:ListItem>Last 1 Month</asp:ListItem>
						<asp:ListItem>Last 2 Month</asp:ListItem>
						<asp:ListItem>Last 3 Month</asp:ListItem>
						<asp:ListItem>Last 1 Year</asp:ListItem>
						<asp:ListItem>Last 2 Years</asp:ListItem>
						<asp:ListItem>Last 3 Years</asp:ListItem>
						<asp:ListItem>Last 4 Years</asp:ListItem>
						<asp:ListItem>Last 5 Years</asp:ListItem>
						<asp:ListItem>All</asp:ListItem>
					</Items>
				</aspire:AspireDropDownList>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ItemType="Aspire.Web.Sys.Admin.Logs.UsageHistory.History" ID="gvUsageHistory" AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvUsageHistory_OnSorting">
		<Columns>
			<asp:TemplateField HeaderText="#">
				<ItemTemplate><%#: Container.DataItemIndex + 1 %></ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Module" DataField="Module" SortExpression="Module" />
			<asp:BoundField HeaderText="Page" DataField="Page" SortExpression="Page" />
			<asp:BoundField HeaderText="User Types" DataField="UserTypes" SortExpression="UserTypes" />
			<asp:BoundField HeaderText="Hits" DataField="Hits" SortExpression="Hits" />
			<asp:BoundField HeaderText="Post Back" DataField="IsPostBack" SortExpression="IsPostBack" />
			<asp:BoundField HeaderText="Min (ms)" DataField="Min" SortExpression="Min" />
			<asp:BoundField HeaderText="Avg (ms)" DataField="Avg" SortExpression="Avg" />
			<asp:BoundField HeaderText="Max (ms)" DataField="Max" SortExpression="Max" />
		</Columns>
	</aspire:AspireGridView>
	<script type="text/javascript">
		$(function () {
			$("#<%=this.ddlPage.ClientID%>").applySelect2();
		})
	</script>
</asp:Content>
