﻿using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Logs
{
	[AspirePage("099B74D7-AAF5-49AF-81F1-373E3E8E2B57", UserTypes.MasterAdmin, "~/Sys/Admin/Logs/Errors.aspx", "Errors", true, AspireModules.Logs)]
	public partial class Errors : MasterAdminPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.warning_sign);
		public override string PageTitle => "Errors";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties(nameof(WebException.ExceptionDate), SortDirection.Descending);
				this.ddlUserType.FillEnums<UserTypes>(CommonListItems.All);
				this.ddlUserType_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlUserType_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearch_OnClick(null, null);
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		protected void gvErrors_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvWebExceptions.PageSize = e.NewPageSize;
			this.GetData(0);
		}
		protected void gvErrors_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvWebExceptions_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
		}

		private void GetData(int pageIndex)
		{
			var userTypeEnum = this.ddlUserType.GetSelectedEnumValue<UserTypes>(null);
			var from = this.dtpFrom.SelectedDate;
			var to = this.dtpTo.SelectedDate;
			var searchText = this.tbSearch.Text;
			var (webExceptions, virtualItemCount) = BL.Core.Logs.Admin.Errors.GetWebExceptions(userTypeEnum, from, to, searchText, pageIndex, this.gvWebExceptions.PageSize, this.AdminIdentity.LoginSessionGuid);
			this.gvWebExceptions.DataBind(webExceptions, pageIndex, virtualItemCount, this.ViewState.GetSortExpression(), this.ViewState.GetSortDirection());
		}
	}
}