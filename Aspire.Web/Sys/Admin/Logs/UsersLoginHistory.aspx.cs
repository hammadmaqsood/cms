using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Logs
{
	[AspirePage("7773D570-6C14-4B2F-984C-2CD442432351", UserTypes.MasterAdmin, "~/Sys/Admin/Logs/UsersLoginHistory.aspx", "Users Login History", true, AspireModules.Logs)]
	public partial class UsersLoginHistory : MasterAdminPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.record);
		public override string PageTitle => "User Login History";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlInstituteIDFilter.FillInstitutesForAdmin(CommonListItems.All);
				this.ddlUserTypeFilter.FillUserTypes(CommonListItems.All);
				this.ddlSubUserTypeFilter.FillSubUserTypes(CommonListItems.All);
				this.ddlLoginStatusFilter.FillEnums<Model.Entities.UserLoginHistory.LoginStatuses>(CommonListItems.All);
				this.ddlLoginFailureReasonFilter.FillEnums<Model.Entities.UserLoginHistory.LoginFailureReasons>(CommonListItems.All);
				this.ddlLogOffReasonFilter.FillEnums<Model.Entities.UserLoginHistory.LogOffReasons>(CommonListItems.All);
				this.ddlInstituteIDFilter_OnSelectedIndexChanged(null, null);
			}
		}

		#region Filters
		protected void ddlInstituteIDFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlUserTypeFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlUserTypeFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			var userType = this.ddlUserTypeFilter.GetSelectedEnumValue<UserTypes>(null);
			switch (userType)
			{
				case UserTypes.Anonymous:
				case UserTypes.System:
				case UserTypes.MasterAdmin:
				case UserTypes.Admin:
				case UserTypes.Executive:
				case UserTypes.Staff:
				case UserTypes.Candidate:
				case UserTypes.Faculty:
				case UserTypes.Alumni:
				case UserTypes.IntegratedService:
				case UserTypes.ManualSQL:
				case UserTypes.Admins:
				case UserTypes.Any:
				case null:
					this.divSubUserType.Visible = false;
					this.ddlSubUserTypeFilter.ClearSelection();
					break;
				case UserTypes.Student:
					this.divSubUserType.Visible = true;
					this.ddlSubUserTypeFilter.ClearSelection();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.ddlSubUserTypeFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlSubUserTypeFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlLoginStatusFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlLoginStatusFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlLoginFailureReasonFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlLoginFailureReasonFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlLogOffReasonFilter_OnSelectedIndexChanged(null, null);
		}

		protected void ddlLogOffReasonFilter_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnSearch_OnClick(null, null);
		}

		protected void btnSearch_OnClick(object sender, EventArgs e)
		{
			this.GetData(0);
		}

		#endregion

		#region User Login History

		protected void gvUserLoginHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			this.GetData(e.NewPageIndex);
		}

		protected void gvUserLoginHistory_OnPageSizeChanging(object sender, AspireGridView.PageSizeEventArgs e)
		{
			this.gvUserLoginHistory.PageSize = e.NewPageSize;
			this.GetData(0);
		}

		protected void gvUserLoginHistory_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData(0);
		}

		#endregion

		private void GetData(int pageIndex)
		{
			var instituteID = this.ddlInstituteIDFilter.SelectedValue.ToNullableInt();
			var userTypeEnum = this.ddlUserTypeFilter.GetSelectedEnumValue<UserTypes>(null);
			var subUserTypeEnum = this.ddlSubUserTypeFilter.GetSelectedEnumValue<SubUserTypes>(null);
			var loginStatusEnum = this.ddlLoginStatusFilter.GetSelectedEnumValue<Model.Entities.UserLoginHistory.LoginStatuses>(null);
			var loginFailureReasonEnum = this.ddlLoginFailureReasonFilter.GetSelectedEnumValue<Model.Entities.UserLoginHistory.LoginFailureReasons>(null);
			var logOffReasonEnum = this.ddlLogOffReasonFilter.GetSelectedEnumValue<Model.Entities.UserLoginHistory.LogOffReasons>(null);
			var searchText = this.tbSearch.Text;
			var (virtualItemCount, userLoginHistories) = BL.Core.Logs.Admin.UsersLoginHistory.GetUserLoginHistory(instituteID, userTypeEnum, subUserTypeEnum, loginStatusEnum, loginFailureReasonEnum, logOffReasonEnum, searchText, pageIndex, this.gvUserLoginHistory.PageSize, this.AdminIdentity.LoginSessionGuid);
			this.gvUserLoginHistory.DataBind(userLoginHistories, pageIndex, virtualItemCount);
		}
	}
}