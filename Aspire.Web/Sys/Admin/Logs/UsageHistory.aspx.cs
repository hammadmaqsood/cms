﻿using Aspire.BL.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Logs
{
	[AspirePage("2A4AF758-6E19-44F9-B971-3192A8EB8431", UserTypes.MasterAdmin, "~/Sys/Admin/Logs/UsageHistory.aspx", "Usage History", true, AspireModules.Logs)]
	public partial class UsageHistory : MasterAdminPage
	{
		public override PageIcon PageIcon => FontAwesomeIcons.solid_history.GetIcon();
		public override string PageTitle => "Usage History";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState.SetSortProperties("Module", SortDirection.Ascending);
				var pages = AspirePageAttributesDictionary.TypeDictionary.Select(t => new ListItem
				{
					Text = $"{t.Value.AspireModule}/{t.Value.MenuItemText}",
					Value = t.Value.AspirePageGuid.ToString()
				});
				this.ddlPage.DataBind(pages, CommonListItems.All);
				this.ddlIsPostBack.FillYesNo(CommonListItems.All);
				this.ddlPage_OnSelectedIndexChanged(null, null);
			}
		}

		protected void ddlPage_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlIsPostBack_OnSelectedIndexChanged(null, null);
		}

		protected void ddlIsPostBack_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.ddlPeriod_OnSelectedIndexChanged(null, null);
		}

		protected void ddlPeriod_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			this.GetData();
		}

		protected void gvUsageHistory_OnSorting(object sender, GridViewSortEventArgs e)
		{
			e.ProcessGridViewSortingEventArgs(this.ViewState);
			this.GetData();
		}

		protected sealed class History
		{
			public string Module { get; internal set; }
			public string Page { get; internal set; }
			public string UserTypes { get; internal set; }
			public string IsPostBack { get; internal set; }
			public int Hits { get; internal set; }
			public int Min { get; internal set; }
			public int Avg { get; internal set; }
			public int Max { get; internal set; }
		}

		private void GetData()
		{
			var pageGuid = this.ddlPage.SelectedValue.ToNullableGuid();
			var isPostBack = this.ddlIsPostBack.SelectedValue.ToNullableBoolean();
			DateTime? fromDate = null;
			DateTime? toDate = null;
			var period = this.ddlPeriod.SelectedValue;
			if (period != "All")
			{
				if (period.StartsWith("Last "))
				{
					int GetNumber(string s) => -period.Replace("Last", "").Replace($"{s}s", "").Replace(s, "").Trim().ToInt();
					if (period.Contains("Minute"))
						fromDate = DateTime.Now.AddMinutes(GetNumber("Minute"));
					else if (period.Contains("Hour"))
						fromDate = DateTime.Now.AddHours(GetNumber("Hour"));
					else if (period.Contains("Day"))
						fromDate = DateTime.Now.AddDays(GetNumber("Day"));
					else if (period.Contains("Month"))
						fromDate = DateTime.Now.AddMonths(GetNumber("Month"));
					else if (period.Contains("Year"))
						fromDate = DateTime.Now.AddYears(GetNumber("Year"));
				}
				if (fromDate == null)
					throw new InvalidOperationException($"{period} value is invalid.");
				toDate = DateTime.Now;
			}

			var data = BL.Core.Logs.Admin.UsageHistory.GetData(pageGuid, isPostBack, fromDate, toDate, this.AdminIdentity.LoginSessionGuid);
			var result = from d in data
						 join a in AspirePageAttributesDictionary.GuidDictionary on d.PageGUID equals a.Key
						 select new History
						 {
							 Module = a.Value.AspireModule.ToFullName().ToNAIfNullOrEmpty(),
							 Page = a.Value.MenuItemText ?? a.Value.AspirePageGuid.ToString(),
							 UserTypes = a.Value.UserTypes.ToFullNames().ToNAIfNullOrEmpty(),
							 IsPostBack = d.IsPostBack.ToYesNo(),
							 Hits = d.Count,
							 Min = d.Min,
							 Avg = d.Avg,
							 Max = d.Max,
						 };
			var sortExpression = this.ViewState.GetSortExpression();
			var sortDirection = this.ViewState.GetSortDirection();
			switch (sortExpression)
			{
				case nameof(History.Module):
					result = result.OrderBy(sortDirection, r => r.Module);
					break;
				case nameof(History.Page):
					result = result.OrderBy(sortDirection, r => r.Page);
					break;
				case nameof(History.UserTypes):
					result = result.OrderBy(sortDirection, r => r.UserTypes);
					break;
				case nameof(History.Hits):
					result = result.OrderBy(sortDirection, r => r.Hits);
					break;
				case nameof(History.IsPostBack):
					result = result.OrderBy(sortDirection, r => r.IsPostBack);
					break;
				case nameof(History.Min):
					result = result.OrderBy(sortDirection, r => r.Min);
					break;
				case nameof(History.Avg):
					result = result.OrderBy(sortDirection, r => r.Avg);
					break;
				case nameof(History.Max):
					result = result.OrderBy(sortDirection, r => r.Max);
					break;
				default:
					throw new SortingNotImplementedException(sortExpression);
			}
			this.gvUsageHistory.DataBind(result.ToList(), sortExpression, sortDirection);
		}
	}
}