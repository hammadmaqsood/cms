﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Sys.Admin.Logs
{
	[AspirePage("F8ACDD1E-574B-4B1B-ADCF-341ABF99CFC3", UserTypes.MasterAdmin, "~/Sys/Admin/Logs/Email.aspx", "Email", true, AspireModules.Logs)]
	public partial class Email : MasterAdminPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.envelope);
		public override string PageTitle => "Email";

		private Guid? EmailID => this.GetParameterValue<Guid>(nameof(this.EmailID));

		public static string GetPageUrl(Guid emailID)
		{
			return typeof(Email).GetAspirePageAttribute().PageUrl.AttachQueryParam(nameof(EmailID), emailID);
		}

		public static void Redirect(Guid emailID)
		{
			Redirect(GetPageUrl(emailID));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.EmailID == null)
				{
					Redirect<Emails>();
					return;
				}
				var emailInfo = BL.Core.Logs.Admin.Emails.GetEmailInfo(this.EmailID.Value, this.AdminIdentity.LoginSessionGuid);
				if (emailInfo == null)
				{
					this.AddNoRecordFoundAlert();
					Redirect<Emails>();
					return;
				}
				this.repeaterEmail.DataBind(new[] { emailInfo });
			}
		}

		protected void repeaterAttachments_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Download":
					var emailID = this.EmailID ?? throw new InvalidOperationException();
					var emailAttachment = BL.Core.Logs.Admin.Emails.GetEmailAttachment(emailID, e.DecryptedCommandArgumentToGuid(), this.AdminIdentity.LoginSessionGuid);
					if (emailAttachment == null)
					{
						this.AddNoRecordFoundAlert();
						Redirect(emailID);
						return;
					}
					this.Response.AppendHeader("Content-Disposition", $"attachment; filename= \"{emailAttachment.FileName.Replace("\"", "\\\"")}\"");
					this.Response.AddHeader("Content-Length", emailAttachment.FileContents.Length.ToString());
					this.Response.ContentType = emailAttachment.FileName.ToMimeType();
					this.Response.BinaryWrite(emailAttachment.FileContents);
					this.Response.End();
					return;
			}
		}
	}
}