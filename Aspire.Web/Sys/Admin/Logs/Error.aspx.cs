﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Sys.Admin.Logs
{
	[AspirePage("79EDEEF6-BC5F-437C-96F6-409677A63301", UserTypes.MasterAdmin, "~/Sys/Admin/Logs/Error.aspx", "Error", true, AspireModules.Logs)]
	public partial class Error : MasterAdminPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.warning_sign);
		public override string PageTitle => "Error";
		private int? WebExceptionID => this.Request.GetParameterValue<int>("WebExceptionID");

		public static string GetPageUrl(int webExceptionID)
		{
			return GetPageUrl<Error>().AttachQueryParam("WebExceptionID", webExceptionID);
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			if (this.WebExceptionID == null)
			{
				Redirect<Error>();
				return;
			}
			var exception = BL.Core.Logs.Admin.Errors.GetWebException(this.WebExceptionID.Value, this.AdminIdentity.LoginSessionGuid);
			if (exception == null)
			{
				this.AddNoRecordFoundAlert();
				Redirect<Errors>();
				return;
			}
			this.Context.Response.Clear();
			this.Response.Write(exception.YSOD);
			this.Context.Response.Flush();
			this.Context.Response.End();
		}
	}
}