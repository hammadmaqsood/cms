﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Errors.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Logs.Errors" %>

<%@ Import Namespace="Aspire.BL.Core.Logs.Admin" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="User Type:" />
					<aspire:AspireDropDownList runat="server" ID="ddlUserType" AutoPostBack="True" OnSelectedIndexChanged="ddlUserType_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="From:" AssociatedControlID="dtpFrom" />
					<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpFrom" ValidationGroup="Search" />
					<aspire:AspireDateTimeValidator AllowNull="True" runat="server" ControlToValidate="dtpFrom" ValidationGroup="Search" InvalidDataErrorMessage="Invalid Date." RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Invalid Date range." />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="To:" AssociatedControlID="dtpTo" />
					<aspire:AspireDateTimePickerTextbox runat="server" ID="dtpTo" ValidationGroup="Search" />
					<aspire:AspireDateTimeValidator AllowNull="True" runat="server" ControlToValidate="dtpTo" ValidationGroup="Search" InvalidDataErrorMessage="Invalid Date." RequiredErrorMessage="This field is required." InvalidRangeErrorMessage="Invalid Date range." />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Search:" AssociatedControlID="tbSearch" />
					<asp:Panel runat="server" CssClass="input-group" DefaultButton="btnSearch">
						<aspire:AspireTextBox runat="server" ID="tbSearch" CssClass="form-control" />
						<span class="input-group-btn">
							<aspire:AspireButton ValidationGroup="Search" ButtonType="Primary" Glyphicon="search" OnClick="btnSearch_OnClick" ID="btnSearch" runat="server" />
						</span>
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ItemType="Aspire.BL.Core.Logs.Admin.Errors.WebException" ID="gvWebExceptions" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="False" OnPageIndexChanging="gvErrors_OnPageIndexChanging" OnPageSizeChanging="gvErrors_OnPageSizeChanging" OnSorting="gvWebExceptions_OnSorting">
		<Columns>
			<asp:TemplateField HeaderText="Action">
				<ItemTemplate>
					<aspire:AspireHyperLink runat="server" Visible="<%# (bool)this.Eval(nameof(Errors.WebException.HasYSOD)) %>" Target="_blank" Text="Details" NavigateUrl="<%# Aspire.Web.Sys.Admin.Logs.Error.GetPageUrl((int)this.Eval(nameof(Errors.WebException.WebExceptionID))) %>" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Error Details">
				<ItemTemplate>
					<strong>Date:</strong> <%#: Item.ExceptionDate.ToString("F") %><br />
					<strong>User Type:</strong> <%#: Item.UserTypeEnumString %><br />
					<strong>Username:</strong> <%#: Item.UserNameString %><br />
					<strong>Page URL:</strong> <%#: Item.PageURL %><br />
					<strong>Error Code:</strong> <%#: Item.ErrorCode %><br />
					<strong>User Agent:</strong> <%#: Item.UserAgent %><br />
					<strong>IP Address:</strong> <%#: Item.IPAddress %><br />
					<strong>Message:</strong> <%#: Item.ExceptionMessage %>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</aspire:AspireGridView>
</asp:Content>
