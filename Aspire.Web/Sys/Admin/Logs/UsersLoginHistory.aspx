﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="UsersLoginHistory.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Logs.UsersLoginHistory" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="row">
		<div class="col-md-12">
			<div class="form-inline pull-right">
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Institute:" AssociatedControlID="ddlInstituteIDFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlInstituteIDFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlInstituteIDFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="User Type:" AssociatedControlID="ddlUserTypeFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlUserTypeFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlUserTypeFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group" runat="server" id="divSubUserType">
					<aspire:AspireLabel runat="server" Text="Sub-User Type:" AssociatedControlID="ddlSubUserTypeFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlSubUserTypeFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlSubUserTypeFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Login Status:" AssociatedControlID="ddlLoginStatusFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlLoginStatusFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlLoginStatusFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Login Failure Reason:" AssociatedControlID="ddlLoginFailureReasonFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlLoginFailureReasonFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlLoginFailureReasonFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Log Off Reason:" AssociatedControlID="ddlLogOffReasonFilter" />
					<aspire:AspireDropDownList runat="server" ID="ddlLogOffReasonFilter" AutoPostBack="True" OnSelectedIndexChanged="ddlLogOffReasonFilter_OnSelectedIndexChanged" />
				</div>
				<div class="form-group">
					<aspire:AspireLabel runat="server" Text="Search:" AssociatedControlID="tbSearch" />
					<asp:Panel runat="server" CssClass="input-group" DefaultButton="btnSearch">
						<aspire:AspireTextBox runat="server" ID="tbSearch" CssClass="form-control" />
						<span class="input-group-btn">
							<aspire:AspireButton ValidationGroup="Search" ButtonType="Primary" Glyphicon="search" OnClick="btnSearch_OnClick" ID="btnSearch" runat="server" />
						</span>
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>
	<p></p>
	<aspire:AspireGridView runat="server" ItemType="Aspire.BL.Core.Logs.Admin.UsersLoginHistory.UserLoginHistory" ID="gvUserLoginHistory" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="gvUserLoginHistory_OnPageIndexChanging" OnPageSizeChanging="gvUserLoginHistory_OnPageSizeChanging" OnSorting="gvUserLoginHistory_OnSorting">
		<Columns>
			<asp:BoundField HeaderText="User Type" DataField="UserTypeFullName" />
			<asp:BoundField HeaderText="Username" DataField="UserNameString" />
			<asp:BoundField HeaderText="Name" DataField="Name" />
			<asp:BoundField HeaderText="IP Address" ItemStyle-CssClass="ip" DataField="IPAddress" ItemStyle-Wrap="False" />
			<asp:BoundField HeaderText="User Agent" DataField="UserAgent" />
			<asp:BoundField HeaderText="Status" DataField="StatusString" HtmlEncode="False" />
			<asp:TemplateField HeaderText="Date">
				<ItemTemplate>
					<%#: Item.LoginDate.ToString("G") %>
					<br />
					<%#: Item.LogOffDate?.ToString("G") %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Logoff Reason" DataField="LogOffReasonEnumString" />
		</Columns>
	</aspire:AspireGridView>
	<script type="text/javascript">
		function htmlEncode(value) { return $("<div/>").text(value).html(); }
		var gv = $("#<%=this.gvUserLoginHistory.ClientID%>");
		var ips = $(".ip", gv).each(function (i, td) {
			var ipAddress = $(td).text();
			if (ipAddress != null)
				switch (ipAddress) {
					case "":
					case "::1":
					case "localhost":
					case "127.0.0.1":
						break;
					default:
						$.ajaxSetup({ cache: true });
						$.getJSON("https://geoip.nekudo.com/api/" + ipAddress,
							function (data) {
								if (data.country.code != undefined) {
									$("<span></span>").prependTo($(td)).css("margin-right", "5px").text(data.country.code);
									var location = "Country: " + htmlEncode(data.country.name);
									if (data.city.length)
										location += "\n" + "City: " + htmlEncode(data.city);
									$(td).attr("title", location);
								}
							}
						);
						break;
				}
		});
	</script>
</asp:Content>
