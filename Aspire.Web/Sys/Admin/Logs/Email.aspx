﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" ValidateRequest="false" AutoEventWireup="true" CodeBehind="Email.aspx.cs" Inherits="Aspire.Web.Sys.Admin.Logs.Email" %>

<%@ Import Namespace="Aspire.Lib.Extensions" %>
<%@ Import Namespace="Aspire.Model.Entities.Common" %>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:Repeater runat="server" ID="repeaterEmail" ItemType="Aspire.BL.Core.Logs.Admin.Emails.EmailInfo">
		<ItemTemplate>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="tbEmailType" />
				<aspire:AspireTextBox ID="tbEmailType" runat="server" ReadOnly="true" Text="<%# Item.Email.EmailType.GetFullName() %>" />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Created By:" AssociatedControlID="tbCreatedBy" />
				<aspire:AspireTextBox ID="tbCreatedBy" runat="server" ReadOnly="true" Text='<%# $"{Item.CreatedByUserName} ({Item.CreatedByUserTypeEnum.ToFullName()})" %>' />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="To:" AssociatedControlID="ltrlTo" />
				<div class="form-control" style="background-color: #eee">
					<asp:Literal ID="ltrlTo" Mode="Encode" runat="server" Text='<%# Item.Email.EmailRecipients.Where(ee=> ee.RecipientTypeEnum == Aspire.Model.Entities.EmailRecipient.RecipientTypes.To).Select(ee => string.IsNullOrWhiteSpace(ee.DisplayName) ? ee.EmailAddress : $"{ee.DisplayName} <{ee.EmailAddress}>" ).Join(" | ") %>' />
				</div>
			</div>
			<div class="form-group" runat="server" visible="<%# Item.Email.EmailRecipients.Any(ee=> ee.RecipientTypeEnum == Aspire.Model.Entities.EmailRecipient.RecipientTypes.Cc) %>">
				<aspire:AspireLabel runat="server" Text="Cc:" AssociatedControlID="ltrlCC" />
				<div class="form-control" style="background-color: #eee">
					<asp:Literal ID="ltrlCC" Mode="Encode" runat="server" Text='<%# Item.Email.EmailRecipients.Where(ee=> ee.RecipientTypeEnum == Aspire.Model.Entities.EmailRecipient.RecipientTypes.Cc).Select(ee => string.IsNullOrWhiteSpace(ee.DisplayName) ? ee.EmailAddress : $"{ee.DisplayName} <{ee.EmailAddress}>" ).Join(" | ") %>' />
				</div>
			</div>
			<div class="form-group" runat="server" visible="<%# Item.Email.EmailRecipients.Any(ee=> ee.RecipientTypeEnum == Aspire.Model.Entities.EmailRecipient.RecipientTypes.Bcc) %>">
				<aspire:AspireLabel runat="server" Text="Bcc:" AssociatedControlID="ltrlBcc" />
				<div class="form-control" style="background-color: #eee">
					<asp:Literal ID="ltrlBcc" Mode="Encode" runat="server" Text='<%# Item.Email.EmailRecipients.Where(ee=> ee.RecipientTypeEnum == Aspire.Model.Entities.EmailRecipient.RecipientTypes.Bcc).Select(ee => string.IsNullOrWhiteSpace(ee.DisplayName) ? ee.EmailAddress : $"{ee.DisplayName} <{ee.EmailAddress}>" ).Join(" | ") %>' />
				</div>
			</div>
			<div class="form-group" runat="server" visible="<%# Item.Email.EmailRecipients.Any(ee=> ee.RecipientTypeEnum == Aspire.Model.Entities.EmailRecipient.RecipientTypes.ReplyTo) %>">
				<aspire:AspireLabel runat="server" Text="Reply To:" AssociatedControlID="ltrlReplyTo" />
				<div class="form-control" style="background-color: #eee">
					<asp:Literal ID="ltrlReplyTo" Mode="Encode" runat="server" Text='<%# Item.Email.EmailRecipients.Where(ee=> ee.RecipientTypeEnum == Aspire.Model.Entities.EmailRecipient.RecipientTypes.ReplyTo).Select(ee => string.IsNullOrWhiteSpace(ee.DisplayName) ? ee.EmailAddress : $"{ee.DisplayName} <{ee.EmailAddress}>" ).Join(" | ") %>' />
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Subject:" AssociatedControlID="tbSubject" />
				<aspire:AspireTextBox ID="tbSubject" runat="server" ReadOnly="true" Text='<%# Item.Email.Subject %>' />
			</div>
			<div class="form-group" runat="server" visible="<%# Item.Email.EmailAttachments.Any() %>">
				<aspire:AspireLabel runat="server" Text="Attachments:" Font-Bold="true" />
				<div class="form-control" style="background-color: #eee">
					<asp:Repeater runat="server" ItemType="Aspire.Model.Entities.EmailAttachment" DataSource="<%# Item.Email.EmailAttachments %>" ID="repeaterAttachments" OnItemCommand="repeaterAttachments_OnItemCommand">
						<ItemTemplate>
							<aspire:AspireLinkButton runat="server" CommandName="Download" CausesValidation="false" Text="<%# Item.FileName %>" EncryptedCommandArgument="<%# Item.EmailAttachmentID %>" />
						</ItemTemplate>
						<SeparatorTemplate>| </SeparatorTemplate>
					</asp:Repeater>
				</div>
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" Text="Body:" Font-Bold="true" />
				<div class="embed-responsive embed-responsive-16by9 well" style="height: 250px">
					<iframe class="embed-responsive-item" srcdoc="<%# (Item.Email.IsBodyHtml ? Item.Email.Body : Item.Email.Body.HtmlEncode(true)).HtmlAttributeEncode() %>"></iframe>
				</div>
			</div>
			<asp:Repeater runat="server" ItemType="Aspire.Model.Entities.EmailSendAttempt" DataSource="<%# Item.Email.EmailSendAttempts.OrderBy(esa=> esa.AttemptDate) %>">
				<HeaderTemplate>
					<div class="table-responsive">
						<table class="table table-bordered table-condensed table-hover">
							<caption>Email Sending Attempts</caption>
							<thead>
								<tr>
									<th>#</th>
									<th>From</th>
									<th>Attempted On</th>
									<th>Status</th>
									<th>Error Message</th>
								</tr>
							</thead>
							<tbody>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><%#: Container.ItemIndex + 1 %></td>
						<td><%#: (Item.SMTPServer?.FromEmailAddress).ToNAIfNullOrEmpty() %></td>
						<td><%#: Item.AttemptDate %></td>
						<td><%#: Item.Status.GetFullName() %></td>
						<td><%#: Item.ErrorMessage.ToNAIfNullOrEmpty() %></td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
					<tr runat="server" visible="<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>">
						<td colspan="10">No record found.</td>
					</tr>
					</tbody>
						</table>
					</div>
				</FooterTemplate>
			</asp:Repeater>
		</ItemTemplate>
	</asp:Repeater>
</asp:Content>
