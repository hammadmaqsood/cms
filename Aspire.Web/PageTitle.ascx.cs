﻿using System;
using System.Web.UI.WebControls;
using Aspire.Web.Common;

namespace Aspire.Web
{
	public partial class PageTitle : System.Web.UI.UserControl
	{
		protected void Page_PreRender(object sender, EventArgs e)
		{
			var pageTitle = this.Page as IPageTitle;
			if (pageTitle != null)
			{
				this.Page.Title = pageTitle.PageTitle + " - " + "Bahria University";
				if (pageTitle.PageTitleVisible)
				{
					if (pageTitle.PageIcon != null)
					{
						this.icon.Attributes.Add("class", pageTitle.PageIcon.ToString());
						this.icon.Visible = true;
					}
					else
						this.icon.Visible = false;
					this.ltrl.Text = pageTitle.PageTitle;
					switch (pageTitle.PageTitleHorizontalAlign)
					{
						case HorizontalAlign.NotSet:
							break;
						case HorizontalAlign.Left:
							this.h1.Attributes.Add("class", "text-left");
							break;
						case HorizontalAlign.Center:
							this.h1.Attributes.Add("class", "text-center");
							break;
						case HorizontalAlign.Right:
							this.h1.Attributes.Add("class", "text-right");
							break;
						case HorizontalAlign.Justify:
							this.h1.Attributes.Add("class", "text-justify");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
					return;
				}
			}
			this.Controls.Clear();
		}
	}
}