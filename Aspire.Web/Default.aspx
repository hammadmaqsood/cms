﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Aspire.Web.Default" %>

<asp:Content runat="server" ContentPlaceHolderID="BodyPH">
	<style type="text/css">
		.well {
			color: #FFFFFF;
			text-align: center;
		}

			.well.candidate {
				background: #005F9C;
			}

			.well.student {
				background: #7A8C74;
			}

			.well.faculty {
				background: #615C5C;
			}

			.well.staff {
				background: #34495E;
			}

			.well.admin {
				background: #E74C3C;
			}

			.well.executive {
				background: #849A47;
			}
	</style>
	<div style="margin-top: 4%"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="col-md-11">
					<div class="well candidate">
						<h1 class="text-center">Admission</h1>
						<aspire:AspireHyperLinkButton ID="hlCandidate" runat="server" NavigateUrl="~/Logins/Candidate/Login.aspx" Text="Apply Online" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="col-md-11">
					<div class="well student">
						<h1 class="text-center">Student</h1>
						<aspire:AspireHyperLinkButton ID="hlStudent" runat="server" NavigateUrl="~/Logins/Student/Login.aspx" Text="Sign In" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="col-md-11">
					<div class="well faculty">
						<h1 class="text-center">Faculty Member</h1>
						<aspire:AspireHyperLinkButton ID="hlFaculty" runat="server" NavigateUrl="~/Logins/Faculty/Login.aspx" Text="Sign In" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="col-md-11">
					<div class="well staff">
						<h1 class="text-center">Administration Staff</h1>
						<aspire:AspireHyperLinkButton ID="hlStaff" runat="server" NavigateUrl="~/Logins/Staff/Login.aspx" Text="Sign In" />
					</div>
				</div>
			</div>
			<div class="col-md-6" runat="server" visible="False" id="divExecutive">
				<div class="col-md-11">
					<div class="well executive">
						<h1 class="text-center">Executive</h1>
						<aspire:AspireHyperLinkButton ID="hlExecutive" runat="server" NavigateUrl="~/Logins/Executive/Login.aspx" Text="Sign In" />
					</div>
				</div>
			</div>
			<div class="col-md-6" runat="server" visible="False" id="divAdmin">
				<div class="col-md-11">
					<div class="well admin">
						<h1 class="text-center">Administrator</h1>
						<aspire:AspireHyperLinkButton ID="hlAdmin" runat="server" NavigateUrl="~/Logins/Admin/Login.aspx" Text="Sign In" />
					</div>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
