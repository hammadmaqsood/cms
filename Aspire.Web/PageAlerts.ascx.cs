﻿using System;
using System.Web.UI;

namespace Aspire.Web
{
	public partial class PageAlerts : UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
			PageAlertsController.DisplayMessages(this.phAlerts);
		}
	}
}