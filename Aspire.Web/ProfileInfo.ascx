﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileInfo.ascx.cs" Inherits="Aspire.Web.ProfileInfo" %>

<ul class="nav navbar-nav navbar-right">
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">
			<asp:Image runat="server" ID="imgAvatar" />
			<asp:Label runat="server" ID="lblUsername" CssClass="name" />
			<span class="caret"></span>
		</a>
		<ul class="dropdown-menu" role="menu">
			<li class="hidden-sm hidden-xs">
				<div>
					<asp:Image runat="server" ID="imgAvatarBig" />
					<asp:Label runat="server" ID="lblName" CssClass="name" />
					<asp:Label runat="server" ID="lblEmail" CssClass="email" />
				</div>
			</li>
			<li id="liProfile" runat="server">
				<asp:HyperLink NavigateUrl="#" ID="hlProfile" Text="Profile" runat="server" />
			</li>
			<asp:PlaceHolder runat="server" ID="phRoles" />
			<li id="liChangePassword" runat="server">
				<asp:HyperLink ID="hlChangePassword" Text="Change Password" runat="server" />
			</li>
			<li id="liFeedback" runat="server">
				<asp:HyperLink NavigateUrl="~/Sys/Common/Feedback.aspx" Text="Give Feedback" Target="_blank" runat="server" />
			</li>
			<li>
				<asp:HyperLink NavigateUrl="#" ID="hlLogoff" Text="Sign Out" runat="server" />
			</li>
		</ul>
	</li>
</ul>
