﻿using Aspire.BL.Core.FileServer;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.IO;
using System.Web;
using System.Web.SessionState;

namespace Aspire.Web.HttpHandlers
{
	public class FileUpload : IHttpHandler, IRequiresSessionState
	{
		private static void SendResponse(HttpContext context, bool success, object jsonObject)
		{
			context.Response.ContentType = "application/json";
			context.Response.Write(new
			{
				success,
				result = jsonObject
			}.ToJsonString());
		}

		public void ProcessRequest(HttpContext context)
		{
			if (context.Request.UrlReferrer != null && context.Request.UrlReferrer?.ToString().StartsWith("~".ToAbsoluteUrl()) != true)
				return;
			try
			{
				var currentLoginSessionGuid = AspireIdentity.Current?.LoginSessionGuid ?? UserLoginHistory.SystemLoginSessionGuid;
				var httpMethod = context.Request.HttpMethod;
				switch (httpMethod)
				{
					case "POST":
						{
							var instanceGuid = context.Request.Form["Instance"].TryToGuid();
							if (instanceGuid == null)
								return;
							var action = context.Request.Form["Action"];
							switch (action)
							{
								case "ADD":
									{
										var httpPostedFile = context.Request.Files[0];
										byte[] bytes;
										using (var binaryReader = new BinaryReader(httpPostedFile.InputStream))
										{
											bytes = binaryReader.ReadBytes(httpPostedFile.ContentLength);
										}

										var fileName = httpPostedFile.FileName;
										var fileExtensionEnum = fileName.ToFileExtension();
										if (fileExtensionEnum == SystemFile.FileExtensions.None)
										{
											SendResponse(context, false, "File type is not allowed.");
											return;
										}

										var systemFile = TemporaryFiles.AddTemporarySystemFile(instanceGuid.Value, fileName, fileExtensionEnum, bytes, currentLoginSessionGuid);
										SendResponse(context, true, new
										{
											FileID = systemFile.SystemFileID,
											systemFile.FileName,
											FileType = systemFile.FileExtensionEnum.ToString()
										});
										return;
									}
								case "DELETE":
									{
										var systemFileID = context.Request.Form["FileID"].TryToGuid();
										if (systemFileID == null)
										{
											SendResponse(context, false, "FileID is missing.");
											return;
										}

										var deleteStatus = TemporaryFiles.DeleteTemporarySystemFile(instanceGuid.Value, systemFileID.Value, currentLoginSessionGuid);
										switch (deleteStatus)
										{
											case TemporaryFiles.DeleteTemporarySystemFileStatuses.NoRecordFound:
												SendResponse(context, true, false);
												return;
											case TemporaryFiles.DeleteTemporarySystemFileStatuses.Success:
												SendResponse(context, true, true);
												return;
											default:
												throw new NotImplementedEnumException(deleteStatus);
										}
									}
								default:
									SendResponse(context, false, $"\"{action}\" not supported.");
									return;
							}
						}
					case "GET":
						{
							var instanceGuid = context.Request.Params["Instance"].TryToGuid();
							if (instanceGuid == null)
								return;
							var systemFileID = context.Request.Params["FileID"].TryToGuid();
							if (systemFileID == null)
							{
								SendResponse(context, false, "FileID is missing.");
								return;
							}

							var fileInfo = TemporaryFiles.ReadTemporarySystemFile(instanceGuid.Value, systemFileID.Value, currentLoginSessionGuid);
							if (fileInfo == null)
							{
								SendResponse(context, false, "File does not exists.");
								return;
							}
							context.Response.Clear();
							context.Response.AddHeader("Content-Disposition", $"attachment; filename=\"{fileInfo.Value.systemFile.FileName}\"");
							context.Response.AddHeader("Content-Length", fileInfo.Value.systemFile.FileSize.ToString());
							context.Response.ContentType = fileInfo.Value.systemFile.FileExtensionEnum.ToMimeType();
							context.Response.BinaryWrite(fileInfo.Value.fileBytes);
						}
						return;
					default:
						SendResponse(context, false, $"Method {httpMethod} Not Supported.");
						return;
				}
			}
			catch (Exception exc)
			{
#if DEBUG
				var message = exc.Message;
#else
				var message = "Unknown error occurred.";
#endif
				SendResponse(context, false, message);
			}
		}

		public bool IsReusable => false;
	}
}