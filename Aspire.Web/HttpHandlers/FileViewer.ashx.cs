﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Entities;
using Aspire.Web.Common;
using System;
using System.Globalization;
using System.Web;
using System.Web.SessionState;

namespace Aspire.Web.HttpHandlers
{
	public sealed class FileViewer : IHttpHandler, IRequiresSessionState
	{
		public static string Url => "~/HttpHandlers/FileViewer.ashx";

		public static string GetUrl(Guid fileGuid, bool inline, string fileName, bool encodeUserSession)
		{
			return encodeUserSession
				? Url.ToAbsoluteUrl().AttachQueryParams("FileGuid", fileGuid, "Inline", inline, "FileName", fileName, EncryptionModes.ConstantSaltUserSession)
				: Url.ToAbsoluteUrl().AttachQueryParams("FileGuid", fileGuid, "Inline", inline, "FileName", fileName, EncryptionModes.ConstantSaltUserSessionNone);
		}

		private static void InvalidParameters(HttpContext context)
		{
			context.Response.ContentType = "text/plain";
			context.Response.Write("Invalid URL Parameters.");
		}

		private static void FileDoesNotExists(HttpContext context)
		{
			context.Response.ContentType = "text/plain";
			context.Response.Write("File does not exists.");
		}

		public void ProcessRequest(HttpContext context)
		{
			//var fileGuid = context.Request.GetParameterValue<Guid>("FileGuid");
			//if (fileGuid == null)
			//{
			//	InvalidParameters(context);
			//	return;
			//}
			//var inline = context.Request.GetParameterValue<bool>("Inline");
			//if (inline == null)
			//{
			//	InvalidParameters(context);
			//	return;
			//}
			//var fileName = context.Request.GetParameterValue<bool>("FileName");
			//var fileInfo = BL.Core.FileServer.Files.GetFile(fileGuid.Value, AspireIdentity.Current?.LoginSessionGuid);
			//if (fileInfo?.FileExists != true)
			//{
			//	FileDoesNotExists(context);
			//	return;
			//}

			//context.Response.Clear();
			//context.Response.AddHeader("Content-Disposition", $"{(inline.Value ? "inline" : "attachment")}; filename=\"{fileName}\"");
			//context.Response.AddHeader("Content-Length", fileInfo.FileSize.ToString(CultureInfo.InvariantCulture));
			//switch (fileInfo.FileTypeEnum)
			//{
			//	case File.FileTypes.Unknown:
			//		throw new InvalidOperationException();
			//	case File.FileTypes.Pdf:
			//		context.Response.ContentType = "application/pdf";
			//		break;
			//	case File.FileTypes.Jpg:
			//		context.Response.ContentType = "image/jpeg";
			//		break;
			//	case File.FileTypes.Png:
			//		context.Response.ContentType = "image/png";
			//		break;
			//	default:
			//		throw new ArgumentOutOfRangeException();
			//}
			////context.Response.Cache.SetMaxAge(TimeSpan.FromMinutes(20));
			//context.Response.WriteFile(fileInfo.FullFileName);
		}

		public bool IsReusable => false;
	}
}