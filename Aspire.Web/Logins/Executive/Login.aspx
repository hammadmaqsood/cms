﻿<%@ Page Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Aspire.Web.Logins.Executive.Login" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:Panel runat="server" CssClass="row" Style="margin-top: 2%" DefaultButton="btnLogin">
		<div class="col-md-4 col-md-offset-4">
			<div class="form-group">
				<aspire:AspireAlert ID="loginAlert" runat="server" />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="tbEmail" Text="Email:" />
				<div class="input-group">
					<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
					<aspire:AspireTextBox runat="server" TextTransform="LowerCase" MaxLength="100" TextMode="Email" ID="tbEmail" PlaceHolder="someone@example.com" />
				</div>
				<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="Login" ControlToValidate="tbEmail" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Email Address." ValidationExpression="Email" />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="tbPassword" Text="Password:" />
				<div class="input-group">
					<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
					<aspire:AspireTextBox runat="server" MaxLength="100" ValidationGroup="Login" TextMode="Password" ID="tbPassword" PlaceHolder="Password" />
				</div>
				<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="Login" ControlToValidate="tbPassword" RequiredErrorMessage="This field is required." />
			</div>
			<div class="form-group" id="divReCaptcha" runat="server">
				<aspire:ReCaptcha runat="server" ID="reCaptcha" />
				<aspire:RecaptchaValidator runat="server" ID="cvRecaptcha" ValidationGroup="Login" ErrorMessage="Verify you are not a robot." CssClass="text-danger" />
			</div>
			<div class="form-group">
				<aspire:AspireButton Text="Sign In" ID="btnLogin" ValidationGroup="Login" OnClick="btnLogin_Click" ButtonType="Primary" CssClass="btn-block" runat="server" />
			</div>
		</div>
		<asp:Literal Mode="PassThrough" ID="literalJs" runat="server" />
	</asp:Panel>
	<script type="text/javascript">
		deleteAllCookies();
	</script>
</asp:Content>
