﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.HtmlControls;

namespace Aspire.Web.Logins
{
	internal static class Common
	{
		public static void AddAttempt(this AspireBasePage page, HtmlGenericControl divReCaptcha, ReCaptcha reCaptcha)
		{
			var attempts = ((int?)page.Session["LoginAttempts"] ?? 1) + 1;
			page.Session["LoginAttempts"] = attempts;
			page.DisplayCaptchaIfRequired(divReCaptcha, reCaptcha);
		}

		public static void DisplayCaptchaIfRequired(this AspireBasePage page, HtmlGenericControl divReCaptcha, ReCaptcha reCaptcha)
		{
			var attempts = (int?)page.Session["LoginAttempts"] ?? 0;
			var captchaVisible = attempts > 3;
			if (captchaVisible == false)
			{
				var browser = page.Request.Browser.Browser.ToNullIfWhiteSpace();
				captchaVisible = browser == null || browser.ToLower().Contains("unknown");
			}
			divReCaptcha.Visible = captchaVisible;
			reCaptcha.Visible = captchaVisible;
		}

		public static void ProcessLoginFailureStatus(AspireBasePage page, UserTypes userTypeEnum, UserLoginHistory.LoginFailureReasons reason, string blockedReason, AspireAlert alert)
		{
			switch (reason)
			{
				case UserLoginHistory.LoginFailureReasons.UsernameNotFound:
				case UserLoginHistory.LoginFailureReasons.InvalidPassword:
				case UserLoginHistory.LoginFailureReasons.AccountIsInactive:
				case UserLoginHistory.LoginFailureReasons.NoRoleFound:
				case UserLoginHistory.LoginFailureReasons.CandidateLoginNotAllowedInSemester:
				case UserLoginHistory.LoginFailureReasons.UserRoleIsInactive:
					alert.AddMessage(AspireAlert.AlertTypes.Error, "Invalid credentials provided.");
#if DEBUG
					alert.AddMessage(AspireAlert.AlertTypes.Error, reason.ToString());
#endif
					break;
				case UserLoginHistory.LoginFailureReasons.AccountExpired:
					if ((userTypeEnum & UserTypes.Candidate) != 0)
						goto case UserLoginHistory.LoginFailureReasons.UsernameNotFound;
					else
						alert.AddMessage(AspireAlert.AlertTypes.Error, "Your account has expired. Contact your administrator.");
					break;
				case UserLoginHistory.LoginFailureReasons.IPAddressNotAuthorized:
					alert.AddMessage(AspireAlert.AlertTypes.Error, $"IP Address {page.ClientIPAddress} is not authorized.");
					break;
				case UserLoginHistory.LoginFailureReasons.StudentStatusAdmissionCanceled:
					alert.AddMessage(AspireAlert.AlertTypes.Error, $"Your status is \"Admission Canceled\". Contact Admission Office.");
					break;
				case UserLoginHistory.LoginFailureReasons.StudentStatusBlocked:
					if (string.IsNullOrWhiteSpace(blockedReason))
						alert.AddMessage(AspireAlert.AlertTypes.Error, $"Your status is \"Blocked\".");
					else
						alert.AddMessage(AspireAlert.AlertTypes.Error, $"Your status is \"Blocked\". Reason: <strong>{blockedReason.HtmlEncode()}</strong>", true);
					break;
				case UserLoginHistory.LoginFailureReasons.StudentStatusDeferred:
					alert.AddMessage(AspireAlert.AlertTypes.Error, $"Your status is \"Deferred\".");
					break;
				case UserLoginHistory.LoginFailureReasons.StudentStatusDropped:
					alert.AddMessage(AspireAlert.AlertTypes.Error, $"Your status is \"Dropped\".");
					break;
				case UserLoginHistory.LoginFailureReasons.StudentStatusExpelled:
					alert.AddMessage(AspireAlert.AlertTypes.Error, $"Your status is \"Expelled\".");
					break;
				case UserLoginHistory.LoginFailureReasons.StudentStatusGraduated:
					alert.AddMessage(AspireAlert.AlertTypes.Error, $"Your status is \"Graduated\".");
					break;
				case UserLoginHistory.LoginFailureReasons.StudentStatusLeft:
					alert.AddMessage(AspireAlert.AlertTypes.Error, $"Your status is \"Left\".");
					break;
				case UserLoginHistory.LoginFailureReasons.StudentStatusProgramChanged:
					alert.AddMessage(AspireAlert.AlertTypes.Error, $"Your status is \"Program Changed\".");
					break;
				case UserLoginHistory.LoginFailureReasons.StudentStatusRusticated:
					alert.AddMessage(AspireAlert.AlertTypes.Error, $"Your status is \"Rusticated\".");
					break;
				case UserLoginHistory.LoginFailureReasons.StudentStatusTransferredToOtherCampus:
					alert.AddMessage(AspireAlert.AlertTypes.Error, $"Your status is \"Transferred To Other Campus\".");
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(reason), reason, null);
			}
		}
	}
}