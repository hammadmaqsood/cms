﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Logins.User
{
	[AspirePage("D6E10BC9-C30C-4E1F-B2E3-CD19EB245936", UserTypes.Staff | UserTypes.Admin, "~/Logins/User/ResetPassword.aspx", "Reset Password", false, AspireModules.None)]
	public partial class ResetPassword : AnonymousPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_lock);
		public override string PageTitle => "Reset Password";
		public override AspireThemes AspireTheme => AspireDefaultThemes.Staff;

		public static string GetPageUrl(Guid confirmationCode)
		{
			return $"{GetPageUrl<ResetPassword>()}?C={confirmationCode.ToString("N").UrlEncode()}";
		}

		private Guid? ConfirmationCode => this.Request.Params["C"].ToNullableGuid();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.ConfirmationCode == null)
				{
					Redirect<Logins.Staff.Login>();
					return;
				}
				var result = Aspire.BL.Core.UserManagement.Admin.Users.GetUserByConfirmationCode(this.ConfirmationCode.Value);
				if (result == null)
					throw new InvalidOperationException();
				switch (result.Status)
				{
					case BL.Core.UserManagement.Admin.Users.GetUserByConfirmationCodeResult.Statuses.Success:
						this.lblUsername.Text = result.Username;
						this.lblName.Text = result.Name;
						this.lblEmail.Text = result.Email;
						break;
					case BL.Core.UserManagement.Admin.Users.GetUserByConfirmationCodeResult.Statuses.NoRecordFound:
					case BL.Core.UserManagement.Admin.Users.GetUserByConfirmationCodeResult.Statuses.ConfirmationCodeExpired:
					case BL.Core.UserManagement.Admin.Users.GetUserByConfirmationCodeResult.Statuses.StatusIsNotActive:
						this.AddErrorAlert("Confirmation Link is invalid or expired.");
						Redirect<Logins.Staff.Login>();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var securePassword = this.tbPassword.Text.ToSecureString();
			var result = BL.Core.UserManagement.Admin.Users.ResetPassword(this.ConfirmationCode.Value, securePassword);
			securePassword.Dispose();
			switch (result.Status)
			{
				case BL.Core.UserManagement.Admin.Users.ResetPasswordResult.Statuses.NoRecordFound:
				case BL.Core.UserManagement.Admin.Users.ResetPasswordResult.Statuses.ConfirmationCodeExpired:
				case BL.Core.UserManagement.Admin.Users.ResetPasswordResult.Statuses.StatusIsNotActive:
					this.AddErrorAlert("Confirmation Link is invalid or expired.");
					Redirect<Logins.Staff.Login>();
					break;
				case BL.Core.UserManagement.Admin.Users.ResetPasswordResult.Statuses.Success:
					this.AddSuccessAlert("Password has been changed.");
					if (result.UserTypes == null || !result.UserTypes.Any())
					{
						Redirect<Default>();
						return;
					}
					if (result.UserTypes.Contains(UserTypes.Staff))
						Redirect<Staff.Login>();
					else if (result.UserTypes.Contains(UserTypes.Executive))
						Redirect<Executive.Login>();
					else if (result.UserTypes.Contains(UserTypes.Admin))
						Redirect<Admin.Login>();
					else
						Redirect<Default>();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}