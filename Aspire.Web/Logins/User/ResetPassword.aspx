﻿<%@ Page Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="Aspire.Web.Logins.User.ResetPassword" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:ScriptManagerProxy runat="server">
		<Scripts>
			<asp:ScriptReference Name="zxcvbn" />
		</Scripts>
	</asp:ScriptManagerProxy>
	<div class="form-horizontal">
		<div class="form-group">
			<aspire:AspireLabel runat="server" CssClass="col-sm-3 col-md-2 col-lg-2" AssociatedControlID="lblUsername" Text="Username:" />
			<div class="col-sm-9 col-md-5 col-lg-4 form-control-static">
				<aspire:Label runat="server" ID="lblUsername" />
			</div>
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" CssClass="col-sm-3 col-md-2 col-lg-2" AssociatedControlID="lblName" Text="Name:" />
			<div class="col-sm-9 col-md-5 col-lg-4 form-control-static">
				<aspire:Label runat="server" ID="lblName" />
			</div>
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" CssClass="col-sm-3 col-md-2 col-lg-2" AssociatedControlID="lblEmail" Text="Email:" />
			<div class="col-sm-9 col-md-5 col-lg-4 form-control-static">
				<aspire:Label runat="server" ID="lblEmail" />
			</div>
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" CssClass="col-sm-3 col-md-2 col-lg-2" Text="New Password:" AssociatedControlID="tbPassword" />
			<div class="col-sm-9 col-md-5 col-lg-4">
				<aspire:AspireTextBox runat="server" ValidationGroup="ChangePassword" ID="tbPassword" TextMode="Password" PlaceHolder="New Password" />
				<aspire:AspirePasswordStrengthMeter runat="server" TextBoxID="tbPassword" />
				<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="ChangePassword" ControlToValidate="tbPassword" ErrorMessage="This field is required." />
				<aspire:AspirePasswordPolicyValidator runat="server" ValidationGroup="ChangePassword" ControlToValidate="tbPassword" SecurityLevel="Medium" />
			</div>
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" CssClass="col-sm-3 col-md-2 col-lg-2" Text="Confirm New Password:" AssociatedControlID="tbConfirmPassword" />
			<div class="col-sm-9 col-md-5 col-lg-4">
				<aspire:AspireTextBox runat="server" ID="tbConfirmPassword" ValidationGroup="ChangePassword" TextMode="Password" PlaceHolder="Confirm New Password" />
				<aspire:AspireCompareValidator ValidateEmptyText="True" runat="server" ValidationGroup="ChangePassword" ControlToCompare="tbPassword" ControlToValidate="tbConfirmPassword" ErrorMessage="Passwords do not match." Type="String" Operator="Equal" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-9 col-sm-offset-3 col-md-6 col-md-offset-2 col-lg-4 col-lg-offset-2">
				<aspire:AspireButton Text="Submit" ID="btnSubmit" ValidationGroup="ChangePassword" ButtonType="Primary" runat="server" OnClick="btnSubmit_Click" />
				<aspire:AspireHyperLink runat="server" Text="Back to Login" NavigateUrl="../Staff/Login.aspx" />
			</div>
		</div>
	</div>
</asp:Content>
