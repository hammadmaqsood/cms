﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ConfirmRegistration.aspx.cs" Inherits="Aspire.Web.Logins.Candidate.ConfirmRegistration" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:ScriptManagerProxy runat="server">
		<Scripts>
			<asp:ScriptReference Name="zxcvbn" />
		</Scripts>
	</asp:ScriptManagerProxy>
	<div class="row" style="margin-top: 3%;">
		<div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Activate Account</h3>
				</div>
				<div class="panel-body">
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" AssociatedControlID="tbSemesterID" Text="Semester:" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" ValidationGroup="ChangePassword" ID="tbSemesterID" ReadOnly="True" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" AssociatedControlID="tbEmail" Text="Email:" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" TextTransform="LowerCase" ValidationGroup="ChangePassword" ID="tbEmail" ReadOnly="True" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" AssociatedControlID="tbName" Text="Full Name (as per SSC):" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" TextTransform="UpperCase" ValidationGroup="ChangePassword" ID="tbName" ReadOnly="False" PlaceHolder="Full Name (as per SSC)" />
								<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="ChangePassword" ControlToValidate="tbName" ErrorMessage="This field is required." />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="New Password:" AssociatedControlID="tbPassword" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" ValidationGroup="ChangePassword" ID="tbPassword" TextMode="Password" PlaceHolder="New Password" />
								<aspire:AspirePasswordStrengthMeter runat="server" TextBoxID="tbPassword" />
								<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="ChangePassword" ControlToValidate="tbPassword" ErrorMessage="This field is required." />
								<aspire:AspirePasswordPolicyValidator runat="server" ValidationGroup="ChangePassword" ControlToValidate="tbPassword" SecurityLevel="Medium" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Confirm New Password:" AssociatedControlID="tbConfirmPassword" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" ID="tbConfirmPassword" ValidationGroup="ChangePassword" TextMode="Password" PlaceHolder="Confirm New Password" />
								<aspire:AspireCompareValidator ValidateEmptyText="True" runat="server" ValidationGroup="ChangePassword" ControlToCompare="tbPassword" ControlToValidate="tbConfirmPassword" ErrorMessage="Passwords do not match." Type="String" Operator="Equal" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-md-offset-4">
								<aspire:AspireButton Text="Submit" ID="btnSubmit" ValidationGroup="ChangePassword" ButtonType="Primary" runat="server" OnClick="btnSubmit_Click" />
								<aspire:AspireHyperLink runat="server" Text="Back to Login" NavigateUrl="Login.aspx" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
