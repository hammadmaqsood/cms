﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="Aspire.Web.Logins.Candidate.ForgotPassword" %>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyPH" runat="server">
	<div class="container">
		<div class="row" style="margin-top: 2%;">
			<div class="col-md-offset-3 col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Forgot Password</h3>
					</div>
					<asp:Panel runat="server" DefaultButton="btnSend" CssClass="panel-body">
						<aspire:Label CssClass="help-block" runat="server" Text="Provide your email address that you used to register. We'll send you an email with a link to reset your password." AssociateControlID="tbEmail" />
						<div class="form-group">
							<aspire:AspireLabel ID="lblSemester" runat="server" AssociatedControlID="tbSemester" Text="Semester:" />
							<aspire:AspireTextBox runat="server" ID="tbSemester" ReadOnly="True" />
							<div>
								<aspire:AspireRadioButtonList runat="server" ID="rblSemesterID" RepeatDirection="Horizontal" RepeatLayout="Flow" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" Text="Email:" AssociatedControlID="tbEmail" />
							<aspire:AspireTextBox ID="tbEmail" AutoCompleteType="Disabled" runat="server" MaxLength="100" TextMode="Email" ValidationGroup="ForgotPassword" />
							<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationExpression="Email" ValidationGroup="ForgotPassword" ControlToValidate="tbEmail" RequiredErrorMessage="This field is required." />
						</div>
						<div class="form-group" id="divReCaptcha" runat="server">
							<aspire:ReCaptcha runat="server" ID="reCaptcha" />
							<aspire:RecaptchaValidator runat="server" ID="cvRecaptcha" ValidationGroup="ForgotPassword" ErrorMessage="Verify you are not a robot." CssClass="text-danger" />
						</div>
						<div class="form-group text-center">
							<aspire:AspireButton Text="Send" ID="btnSend" ValidationGroup="ForgotPassword" ButtonType="Primary" CssClass="" runat="server" OnClick="btnSend_Click" />
							<aspire:AspireHyperLink runat="server" Text="Back to Login" NavigateUrl="Login.aspx" />
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
