﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Logins.Candidate
{
	[AspirePage("E4B256B0-148B-49B0-91AD-2AD9AD2E26D1", UserTypes.Anonymous, "~/Logins/Candidate/Login.aspx", "Candidate Login", false, AspireModules.None)]
	public partial class Login : LoginBasePage
	{
		public override AspireThemes AspireTheme => AspireDefaultThemes.Candidate;
		public override string PageTitle => "Admissions";
		public override PageIcon PageIcon => null;
		public override HorizontalAlign PageTitleHorizontalAlign => HorizontalAlign.Center;

		private List<BL.Core.Admissions.Candidate.Common.Semester> _loginSemesters;
		private List<BL.Core.Admissions.Candidate.Common.Semester> LoginSemesters
		{
			get
			{
				if (this._loginSemesters == null)
					this._loginSemesters = BL.Core.Admissions.Candidate.Common.GetAdmissionOpenSemesters();
				return this._loginSemesters;
			}
		}

		public static void Redirect(string email)
		{
			Redirect<Login>("Email", email, EncryptionModes.ConstantSaltNoneUserSessionNone);
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			if (Web.Common.AspireIdentity.Current != null)
				Default.RedirectToHomeScreen();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var loginAllowedSemesters = BL.Core.Admissions.Candidate.Common.GetCandidateLoginAllowedSemesterIDs();
				this.rblSemesterID.DataBindSemesters(loginAllowedSemesters);
				if (loginAllowedSemesters?.Any() != true)
				{
					Redirect<AdmissionsClosed>();
					return;
				}
				else if (loginAllowedSemesters.Count == 1)
				{
					this.rblSemesterID.SelectedIndex = 0;
					this.tbSemester.Text = this.rblSemesterID.SelectedItem.Text;
					this.divrblSemester.Visible = false;
					this.divtbSemester.Visible = true;
				}
				else
				{
					this.rblSemesterID.ClearSelection();
					this.tbSemester.Text = null;
					this.divrblSemester.Visible = true;
					this.divtbSemester.Visible = false;
				}

				var email = this.Request.GetParameterValue("Email");
				if (!string.IsNullOrWhiteSpace(email))
					this.tbEmail.Text = email;
				this.DisplayCaptchaIfRequired(this.divReCaptcha, this.reCaptcha);

#if DEBUG
				if (this.Request.IsLocal)
				{
					this.tbEmail.Text = this.Request.Params["CandidateUsername"];
					var password = this.Request.Params["Password"];
					if (!string.IsNullOrWhiteSpace(password))
						this.literalJs.Text = $"<script type=\"text/javascript\">{this.tbPassword.ClientID}.value = \"{password.Replace("\"", "\\\"")}\";</script>";
				}
#endif
			}

			if (string.IsNullOrWhiteSpace(this.tbEmail.Text))
				this.tbEmail.Focus();
			else
				this.tbPassword.Focus();
		}

		protected void btnLogin_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			this.AddAttempt(this.divReCaptcha, this.reCaptcha);
			var semesterID = this.rblSemesterID.SelectedValue.ToShort();
			var loginResult = BL.Core.LoginManagement.Candidate.Login(semesterID, this.tbEmail.Text, this.tbPassword.Text, null, null, this.UserAgent, this.ClientIPAddress);
			if (loginResult.LoginFailureReason != null)
				Common.ProcessLoginFailureStatus(this, UserTypes.Candidate, loginResult.LoginFailureReason.Value, null, this.loginAlert);
			else
			{
				var candidateIdentity = new CandidateIdentity(loginResult.UserName, loginResult.Name, loginResult.Email, loginResult.UserLoginHistoryID, loginResult.LoginSessionGuid, loginResult.CandidateID, loginResult.ForeignStudent, loginResult.SemesterID, loginResult.GenderEnum, loginResult.IPAddress, loginResult.UserAgent);
				SessionHelper.CreateUserSession(candidateIdentity, GetPageUrl<Sys.Candidate.PrepareSession>());
			}
		}
	}
}