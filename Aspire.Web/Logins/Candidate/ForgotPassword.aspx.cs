﻿using Aspire.BL.Core.Admissions.Candidate;
using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Logins.Candidate
{
	[AspirePage("{DA21945D-23C5-4B6C-B6BA-E3D03A68A7C4}", UserTypes.Anonymous, "~/Logins/Candidate/ForgotPassword.aspx", null, false, AspireModules.None)]
	public partial class ForgotPassword : AnonymousPage
	{
		public override AspireThemes AspireTheme => AspireDefaultThemes.Candidate;
		public override PageIcon PageIcon => null;
		public override HorizontalAlign PageTitleHorizontalAlign => HorizontalAlign.Center;
		public override string PageTitle => "Admissions";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var semesterIDs = Aspire.BL.Core.Admissions.Candidate.Common.GetCandidateLoginAllowedSemesterIDs();
				this.rblSemesterID.DataBindSemesters(semesterIDs);
				if (this.rblSemesterID.Items.Count == 0)
				{
					Redirect<AdmissionsClosed>();
					return;
				}
				this.rblSemesterID.SelectedIndex = 0;
				this.rblSemesterID.Visible = this.rblSemesterID.Items.Count > 1;
				this.tbSemester.Visible = this.rblSemesterID.Items.Count == 1;
				if (this.tbSemester.Visible)
				{
					this.tbSemester.Text = this.rblSemesterID.SelectedItem.Text;
					this.lblSemester.AssociatedControlID = this.tbSemester.ID;
				}
				else
					this.lblSemester.AssociatedControlID = this.rblSemesterID.ID;
			}
		}

		protected void btnSend_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var pageURL= typeof(ResetPassword).GetAspirePageAttribute().PageUrl.ToAbsoluteUrl();
			var result = ProfileInformation.GenerateResetPasswordCode(this.tbEmail.Text, this.rblSemesterID.SelectedValue.ToShort(), pageURL);
			switch (result.Status)
			{
				case ProfileInformation.GenerateResetPasswordCodeResult.Statuses.AdmissionNotOpen:
					Redirect<AdmissionsClosed>();
					return;
				case ProfileInformation.GenerateResetPasswordCodeResult.Statuses.NoRecordFound:
					this.AddErrorAlert(this.tbEmail.Text + " is not registered.");
					this.tbEmail.Text = null;
					return;
				case ProfileInformation.GenerateResetPasswordCodeResult.Statuses.Success:
					this.AddSuccessAlert("Please visit your email inbox. You will soon receive an email with password reset link.");
					Redirect<Login>();
					return;
				default:
					throw new NotImplementedEnumException(result.Status);
			}
		}
	}
}
