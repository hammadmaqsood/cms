﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Logins.Candidate
{
	[AspirePage("{34BEB576-480D-4197-A1B0-74AB0F3823BC}", UserTypes.Anonymous, "~/Logins/Candidate/Registration.aspx", null, false, AspireModules.None)]
	public partial class Registration : AnonymousPage
	{
		public override AspireThemes AspireTheme => AspireThemes.Deepsea;
		public override PageIcon PageIcon => null;
		public override string PageTitle => "Admissions";
		public override HorizontalAlign PageTitleHorizontalAlign => HorizontalAlign.Center;

		private List<BL.Core.Admissions.Candidate.Common.Semester> _openSemesters;
		private List<BL.Core.Admissions.Candidate.Common.Semester> OpenSemesters
		{
			get
			{
				if (this._openSemesters == null)
					this._openSemesters = BL.Core.Admissions.Candidate.Common.GetAdmissionOpenSemesters();
				return this._openSemesters;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.rblSemesterID.DataBindSemesters(this.OpenSemesters.Select(s => s.SemesterID).Distinct().OrderBy(s => s));
				var semestersCount = this.rblSemesterID.Items.Count;
				if (semestersCount == 0)
				{
					Redirect<AdmissionsClosed>();
					return;
				}
				if (semestersCount == 1)
					this.rblSemesterID.SelectedIndex = 0;
				this.rblSemesterID_SelectedIndexChanged(null, null);
			}
		}

		protected void rblSemesterID_SelectedIndexChanged(object sender, EventArgs e)
		{
			var semesterID = this.rblSemesterID.SelectedValue.ToNullableShort();
			var semestersCount = this.rblSemesterID.Items.Count;
			if (semestersCount == 0)
				throw new InvalidOperationException();

			this.rblSemesterID.Visible = semestersCount > 1;
			this.tbSemester.Visible = semestersCount == 1;
			this.tbSemester.Text = semesterID?.ToSemesterString();

			var nationalStudentListItem = new ListItem
			{
				Text = "National Student (Pakistani)",
				Value = CommonListItems.FalseValue,
				Enabled = false,
				Selected = false
			};
			var internationalStudentListItem = new ListItem
			{
				Text = "International Student",
				Value = CommonListItems.TrueValue,
				Enabled = false,
				Selected = false
			};
			this.rblForeignStudent.DataBind(new[] { nationalStudentListItem, internationalStudentListItem });

			if (semesterID != null)
			{
				nationalStudentListItem.Enabled = this.OpenSemesters.Any(s => s.SemesterID == semesterID.Value && s.LocalAllowed);
				internationalStudentListItem.Enabled = this.OpenSemesters.Any(s => s.SemesterID == semesterID.Value && s.ForeignerAllowed);
				if (nationalStudentListItem.Enabled)
					nationalStudentListItem.Selected = true;
				else if (internationalStudentListItem.Enabled)
					internationalStudentListItem.Selected = true;
				else
					throw new InvalidOperationException();
			}
		}

		protected void btnRegister_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;

			var semesterID = this.rblSemesterID.SelectedValue.ToShort();
			var email = this.tbEmail.Text;
			var name = this.tbName.Text;
			var foreignStudent = this.rblForeignStudent.SelectedValue.ToBoolean();
			var pageURL = GetAspirePageAttribute<ConfirmRegistration>().PageUrl.ToAbsoluteUrl();
			var result = BL.Core.Admissions.Candidate.Registration.RegisterCandidate(email, name, foreignStudent, semesterID, pageURL);
			switch (result.Status)
			{
				case BL.Core.Admissions.Candidate.Registration.RegisterCandidateResult.Statuses.AdmissionsNotOpen:
					Redirect<AdmissionsClosed>();
					return;
				case BL.Core.Admissions.Candidate.Registration.RegisterCandidateResult.Statuses.EmailAlreadyRegistered:
					this.AddErrorAlert($"The email address {this.tbEmail.Text} is already registered. You can reset your password by clicking on \"Forgot Password\".");
					Login.Redirect(this.tbEmail.Text);
					break;
				case BL.Core.Admissions.Candidate.Registration.RegisterCandidateResult.Statuses.Success:
					this.AddSuccessAlert("Please check you email inbox. You will soon receive the confirmation email.");
					Redirect<Login>();
					return;
				case BL.Core.Admissions.Candidate.Registration.RegisterCandidateResult.Statuses.SuccessAlreadyExists:
					this.AddSuccessAlert("We have sent you confirmation email again. Please check you email inbox. You will soon receive the confirmation email.");
					Redirect<Login>();
					return;
				default:
					throw new NotImplementedEnumException(result.Status);
			}
		}
	}
}