﻿<%@ Page Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="Aspire.Web.Logins.Candidate.ResetPassword" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:ScriptManagerProxy runat="server">
		<Scripts>
			<asp:ScriptReference Name="zxcvbn" />
		</Scripts>
	</asp:ScriptManagerProxy>
	<div class="row" style="margin-top: 3%;">
		<div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Reset Password</h3>
				</div>
				<div class="panel-body">
					<div class="form-horizontal">
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" AssociatedControlID="tbSemester" Text="Semester:" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" ID="tbSemester" ReadOnly="True" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" AssociatedControlID="tbEmail" Text="Email:" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" TextTransform="LowerCase" ID="tbEmail" ReadOnly="True" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" AssociatedControlID="tbName" Text="Name:" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" ID="tbName" ReadOnly="True" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="New Password:" AssociatedControlID="tbPassword" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" ValidationGroup="ChangePassword" ID="tbPassword" TextMode="Password" PlaceHolder="New Password" />
								<aspire:AspirePasswordStrengthMeter runat="server" TextBoxID="tbPassword" />
								<aspire:AspireRequiredFieldValidator runat="server" ValidationGroup="ChangePassword" ControlToValidate="tbPassword" ErrorMessage="This field is required." />
								<aspire:AspirePasswordPolicyValidator runat="server" ValidationGroup="ChangePassword" ControlToValidate="tbPassword" SecurityLevel="Medium" />
							</div>
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" CssClass="col-md-4" Text="Confirm New Password:" AssociatedControlID="tbConfirmPassword" />
							<div class="col-md-8">
								<aspire:AspireTextBox runat="server" ID="tbConfirmPassword" ValidationGroup="ChangePassword" TextMode="Password" PlaceHolder="Confirm New Password" />
								<aspire:AspireCompareValidator ValidateEmptyText="True" runat="server" ValidationGroup="ChangePassword" ControlToCompare="tbPassword" ControlToValidate="tbConfirmPassword" ErrorMessage="Passwords do not match." Type="String" Operator="Equal" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-md-offset-4">
								<aspire:AspireButton Text="Submit" ID="btnSubmit" ValidationGroup="ChangePassword" ButtonType="Primary" runat="server" OnClick="btnSubmit_Click" />
								<aspire:AspireHyperLink runat="server" Text="Back to Login" NavigateUrl="Login.aspx" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
