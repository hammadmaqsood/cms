﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Logins.Candidate
{
	[AspirePage("D569AF43-0551-403B-A878-212F9E754648", UserTypes.Anonymous, "~/Logins/Candidate/ConfirmRegistration.aspx", null, false, AspireModules.None)]
	public partial class ConfirmRegistration : AnonymousPage
	{
		public override AspireThemes AspireTheme => AspireThemes.Deepsea;
		public override PageIcon PageIcon => null;
		public override string PageTitle => "Admissions";
		public override HorizontalAlign PageTitleHorizontalAlign => HorizontalAlign.Center;
		private const string VerificationCode = "VerificationCode";
		private Guid? Code => this.Request.Params["C"].TryToGuid();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (!BL.Core.Admissions.Candidate.Common.GetAdmissionOpenSemesters().Any())
				{
					Redirect<AdmissionsClosed>();
					return;
				}
				var verificationCode = this.Code;
				if (verificationCode == null)
				{
					Redirect<Login>();
					return;
				}
				var candidateTemp = BL.Core.Admissions.Candidate.Registration.GetCandidateTemp(verificationCode.Value);
				if (candidateTemp == null || candidateTemp.CodeExpiryDate <= DateTime.Now)
				{
					this.LinkExpiredOrInvalid();
					return;
				}

				this.tbSemesterID.Text = candidateTemp.SemesterID.ToSemesterString();
				this.tbEmail.Text = candidateTemp.Email;
				this.tbName.Text = candidateTemp.Name;
			}
		}

		private void LinkExpiredOrInvalid()
		{
			this.AddErrorAlert("Verification link has been expired or invalid or used. Please register again.");
			Redirect<Login>();
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (!this.IsValid || this.Code == null)
				return;

			var name = this.tbName.Text;
			var email = this.tbEmail.Text;
			var password = this.tbPassword.Text;
			var result = BL.Core.Admissions.Candidate.Registration.ConfirmRegistration(this.Code.Value, name, password);
			switch (result.Status)
			{
				case BL.Core.Admissions.Candidate.Registration.ConfirmRegistrationResult.Statuses.NoRecordFound:
					this.LinkExpiredOrInvalid();
					break;
				case BL.Core.Admissions.Candidate.Registration.ConfirmRegistrationResult.Statuses.EmailAlreadyRegisteredAndConfirmed:
					this.AddErrorAlert("Account has already been activated.");
					Login.Redirect(this.tbEmail.Text);
					break;
				case BL.Core.Admissions.Candidate.Registration.ConfirmRegistrationResult.Statuses.AdmissionNotOpen:
					Redirect<AdmissionsClosed>();
					break;
				case BL.Core.Admissions.Candidate.Registration.ConfirmRegistrationResult.Statuses.RegisteredSuccessfully:
					this.AddSuccessAlert("Your account has been activated. Please login to continue.");
					Login.Redirect(this.tbEmail.Text);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}