﻿<%@ Page Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="Aspire.Web.Logins.Candidate.Registration" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="container">
		<div class="row" style="margin-top: 2%">
			<div class="col-md-12 col-lg-8 col-lg-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Candidate Registration</h3>
					</div>
					<asp:UpdatePanel runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:Panel CssClass="panel-body" runat="server" DefaultButton="btnRegister">
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="Applying for Semester:" AssociatedControlID="rblSemesterID" />
									<aspire:AspireTextBox runat="server" ID="tbSemester" ValidationGroup="Register" ReadOnly="True" />
									<div>
										<aspire:AspireRadioButtonList runat="server" ID="rblSemesterID" RepeatLayout="Flow" RepeatDirection="Horizontal" CausesValidation="false" OnSelectedIndexChanged="rblSemesterID_SelectedIndexChanged" AutoPostBack="True" />
										<div>
											<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblSemesterID" ValidationGroup="Register" ErrorMessage="This field is required." />
										</div>
									</div>
								</div>
								<div class="form-group">
									<aspire:AspireLabel runat="server" AssociatedControlID="tbName">Full Name (as per <abbr title="Secondary School Certificate">SSC</abbr>):</aspire:AspireLabel>
									<aspire:AspireTextBox runat="server" AutoCompleteType="Disabled" TextTransform="UpperCase" ID="tbName" PlaceHolder="Full Name (as per SSC)" MaxLength="100" ValidationGroup="Register" />
									<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbName" RequiredErrorMessage="This field is required." ValidationGroup="Register" InvalidDataErrorMessage="Invalid Name." ValidationExpression="Custom" CustomValidationExpression="^[A-Z]+([\s][A-Z]+)*$" />
								</div>
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="Email:" AssociatedControlID="tbEmail" />
									<aspire:AspireTextBox runat="server" AutoCompleteType="Disabled" TextTransform="LowerCase" ID="tbEmail" PlaceHolder="someone@example.com" MaxLength="100" ValidationGroup="Register" />
									<aspire:AspireStringValidator runat="server" AllowNull="False" ControlToValidate="tbEmail" RequiredErrorMessage="This field is required." ValidationGroup="Register" InvalidDataErrorMessage="Invalid Email address." ValidationExpression="Email" />
								</div>
								<div class="form-group">
									<aspire:AspireLabel runat="server" Text="Type:" AssociatedControlID="rblForeignStudent" />
									<div>
										<aspire:AspireRadioButtonList runat="server" ID="rblForeignStudent" RepeatLayout="Flow" RepeatDirection="Horizontal" ValidationGroup="Register" />
									</div>
									<div>
										<aspire:AspireRequiredFieldValidator runat="server" ControlToValidate="rblForeignStudent" ValidationGroup="Register" ErrorMessage="This field is required." />
									</div>
								</div>
								<div class="form-group" id="divReCaptcha" runat="server">
									<aspire:ReCaptcha runat="server" ID="reCaptcha" />
									<aspire:RecaptchaValidator runat="server" ID="cvRecaptcha" ValidationGroup="Register" ErrorMessage="Verify you are not a robot." CssClass="text-danger" />
								</div>
								<div class="form-group">
									<aspire:AspireButton Text="Register" ButtonType="Primary" ValidationGroup="Register" ID="btnRegister" CssClass="btn-block" runat="server" OnClick="btnRegister_Click" />
								</div>
								<div class="form-group">
									<aspire:AspireHyperLink CssClass="btn btn-success btn-block" runat="server" Text="Already have an account?" NavigateUrl="Login.aspx" />
								</div>
							</asp:Panel>
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
