﻿using Aspire.BL.Core.Admissions.Candidate;
using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Logins.Candidate
{
	[AspirePage("F864ACA2-706A-445E-89DD-31384B521E19", UserTypes.Anonymous, "~/Logins/Candidate/ResetPassword.aspx", null, false, AspireModules.None)]
	public partial class ResetPassword : AnonymousPage
	{
		public override PageIcon PageIcon => new PageIcon(FontAwesomeIcons.solid_lock);
		public override string PageTitle => "Reset Password";
		public override AspireThemes AspireTheme => AspireThemes.Deepsea;
		private Guid? ConfirmationCode => this.Request.Params["C"].TryToGuid();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.ConfirmationCode == null)
				{
					Redirect<Login>();
					return;
				}

				var result = Aspire.BL.Core.Admissions.Candidate.ProfileInformation.GetCandidateInfoForResetPassword(this.ConfirmationCode.Value);
				switch (result.Status)
				{
					case ProfileInformation.GetCandidateInfoForResetPasswordResult.Statuses.NoRecordFound:
					case ProfileInformation.GetCandidateInfoForResetPasswordResult.Statuses.ConfirmationCodeExpired:
					case ProfileInformation.GetCandidateInfoForResetPasswordResult.Statuses.LoginNotAllowedForSemester:
						this.LinkExpiredOrInvalid();
						return;
					case null:
						this.tbSemester.Text = result.CandidateInfo.SemesterID.ToString();
						this.tbName.Text = result.CandidateInfo.Name;
						this.tbEmail.Text = result.CandidateInfo.Email;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		private void LinkExpiredOrInvalid()
		{
			this.AddErrorAlert("Link has been expired or invalid or already used.");
			Redirect<Login>();
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;

			var result = ProfileInformation.ResetPassword(this.ConfirmationCode.Value, this.tbPassword.Text);
			switch (result.Status)
			{
				case ProfileInformation.ResetPasswordResult.Statuses.Success:
					this.AddSuccessAlert("Password has been reset.");
					Candidate.Login.Redirect(result.CandidateInfo.Email);
					return;
				case ProfileInformation.ResetPasswordResult.Statuses.LoginNotAllowedForSemester:
				case ProfileInformation.ResetPasswordResult.Statuses.ConfirmationCodeHasBeenExpired:
				case ProfileInformation.ResetPasswordResult.Statuses.NoRecordFound:
					this.LinkExpiredOrInvalid();
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}