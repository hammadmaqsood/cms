﻿<%@ Page Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" CodeBehind="AdmissionsClosed.aspx.cs" Inherits="Aspire.Web.Logins.Candidate.AdmissionsClosed" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="text-center" style="margin-top: 5%">
		<span class="h1">Admissions are not currently open.</span>
	</div>
	<div class="text-center" style="margin-top: 2%">
		<asp:HyperLink runat="server" Text="Back to Login" NavigateUrl="~/Default.aspx" />
	</div>
</asp:Content>
