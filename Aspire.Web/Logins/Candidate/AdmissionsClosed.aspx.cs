﻿using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Linq;

namespace Aspire.Web.Logins.Candidate
{
	[AspirePage("{00EAB735-3F85-42FD-804A-5851F4F2298B}", UserTypes.Anonymous, "~/Logins/Candidate/AdmissionsClosed.aspx", "Admissions Closed", false, AspireModules.None)]
	public partial class AdmissionsClosed : AnonymousPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Aspire.BL.Core.Admissions.Candidate.Common.GetAdmissionOpenSemesters().Any())
				Redirect<Login>();
		}

		public override AspireThemes AspireTheme => AspireThemes.Deepsea;

		public override PageIcon PageIcon => null;

		public override bool PageTitleVisible => false;

		public override string PageTitle => null;
	}
}