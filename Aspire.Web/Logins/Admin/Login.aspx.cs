﻿using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web;
using System.Web.UI.WebControls;

namespace Aspire.Web.Logins.Admin
{
	[AspirePage("B6887B78-B445-4309-9F36-599588452617", UserTypes.Admins, "~/Logins/Admin/Login.aspx", "Admin Login", false, AspireModules.None)]
	public partial class Login : LoginBasePage
	{
		public override AspireThemes AspireTheme => AspireDefaultThemes.Admin;
		public override bool PageTitleVisible => true;
		public override string PageTitle => "Administrator";
		public override PageIcon PageIcon => null;
		public override HorizontalAlign PageTitleHorizontalAlign => HorizontalAlign.Center;

		public static string GetPageUrl(string email, AspireAlert.AlertTypes? alertType, string message)
		{
			return GetAspirePageAttribute<Login>().PageUrl.AttachQueryParams("Email", email, "AlertType", alertType, "Message", message, EncryptionModes.ConstantSaltNoneUserSessionNone);
		}

		public static void Redirect()
		{
			Redirect(null, null, null);
		}

		public static void Redirect(string email, AspireAlert.AlertTypes? alertType, string message)
		{
			HttpContext.Current.Response.Redirect(GetPageUrl(email, alertType, message));
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			if (Web.Common.AspireIdentity.Current != null)
				Default.RedirectToHomeScreen();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var email = this.Request.GetParameterValue("Email");
				if (!string.IsNullOrWhiteSpace(email))
					this.tbEmail.Text = email;

#if DEBUG
				if (this.Request.IsLocal)
				{
					this.tbEmail.Text = this.Request.Params["UserName"];
					var password = this.Request.Params["Password"];
					if (!string.IsNullOrWhiteSpace(password))
						this.literalJs.Text = $"<script type=\"text/javascript\">{this.tbPassword.ClientID}.value = \"{password.Replace("\"", "\\\"")}\";</script>";
				}
#endif
				this.DisplayCaptchaIfRequired(this.divReCaptcha, this.reCaptcha);
			}

			if (string.IsNullOrWhiteSpace(this.tbEmail.Text))
				this.tbEmail.Focus();
			else
				this.tbPassword.Focus();
		}

		protected void btnLogin_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			this.AddAttempt(this.divReCaptcha, this.reCaptcha);
			var loginResult = BL.Core.LoginManagement.Admin.Login(this.tbEmail.Text, this.tbPassword.Text, this.UserAgent, this.ClientIPAddress);
			if (loginResult.LoginFailureReason != null)
				Common.ProcessLoginFailureStatus(this, UserTypes.Admins, loginResult.LoginFailureReason.Value, null, this.loginAlert);
			else
			{
				var loginHistory = loginResult.LoginHistory;
				var adminIdentity = new AdminIdentity(loginHistory.Username, loginHistory.Name, loginHistory.Email, loginHistory.UserTypeEnum, loginHistory.UserLoginHistoryID, loginHistory.LoginSessionGuid, loginHistory.UserID, loginHistory.UserRoleID, this.ClientIPAddress, this.UserAgent);
				SessionHelper.CreateUserSession(adminIdentity, GetPageUrl<Sys.Admin.PrepareSession>());
			}
		}
	}
}