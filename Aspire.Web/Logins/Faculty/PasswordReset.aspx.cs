using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Logins.Faculty
{
	[AspirePage("0EB536AC-55C7-4D75-B787-DFA9EBCC83F4", UserTypes.Faculty, "~/Logins/Faculty/PasswordReset.aspx", "Password Reset", false, AspireModules.None)]
	public partial class PasswordReset : AnonymousPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.lock_);
		public override string PageTitle => "Reset Password";
		public override AspireThemes AspireTheme => AspireDefaultThemes.Faculty;

		public Guid? ConfirmationCode => this.Request.Params["C"].TryToGuid();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				if (this.ConfirmationCode == null)
				{
					Redirect<Login>();
					return;
				}

				var facultyMember = BL.Core.LoginManagement.Faculty.GetFacultyMember(this.ConfirmationCode.Value);
				if (facultyMember == null)
				{
					this.AddErrorAlert("Confirmation Link is invalid. It's also possible that the verification link has expired.");
					Redirect<Login>();
					return;
				}
				this.lblFullName.Text = facultyMember.FullName;
			}
		}

		protected void btnSubmit_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var confirmationCode = this.ConfirmationCode.Value;
			var newPassword = this.tbNewPassword.Text;
			var passwordChanged = BL.Core.LoginManagement.Faculty.ResetPassword(confirmationCode, newPassword);
			if (passwordChanged)
				this.AddSuccessAlert("Your password has been changed successfully.");
			else
				this.AddErrorAlert("Confirmation Link is invalid. It's also possible that the verification link has expired.");
			Redirect<Login>();
		}
	}
}