﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web;
using System.Web.UI.WebControls;

namespace Aspire.Web.Logins.Faculty
{
	[AspirePage("7166E6FC-F536-4F14-BFE1-44FD93E86BDF", UserTypes.Faculty, "~/Logins/Faculty/Login.aspx", null, false, AspireModules.None)]
	public partial class Login : LoginBasePage
	{
		public override AspireThemes AspireTheme => AspireDefaultThemes.Faculty;
		public override bool PageTitleVisible => true;
		public override string PageTitle => "Faculty Member";
		public override PageIcon PageIcon => null;
		public override HorizontalAlign PageTitleHorizontalAlign => HorizontalAlign.Center;

		public static string GetPageUrl(string email, AspireAlert.AlertTypes? alertType, string message)
		{
			return GetAspirePageAttribute<Login>().PageUrl.AttachQueryParams("Email", email, "AlertType", alertType, "Message", message, EncryptionModes.ConstantSaltNoneUserSessionNone);
		}

		public static void Redirect()
		{
			Redirect(null, null, null);
		}

		public static void Redirect(string email, AspireAlert.AlertTypes? alertType, string message)
		{
			HttpContext.Current.Response.Redirect(GetPageUrl(email, alertType, message));
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			if (Web.Common.AspireIdentity.Current != null)
				Default.RedirectToHomeScreen();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlInstituteID.FillInstitutesShortNames(CommonListItems.Select);
				var email = this.Request.GetParameterValue("Email");
				if (!string.IsNullOrWhiteSpace(email))
					this.tbUserName.Text = email;
				var instituteID_ = BL.Core.Common.Common.GetFirstInstituteID(this.ClientIPAddress);
				if (instituteID_ != null)
					this.ddlInstituteID.SetSelectedValueIfExists(instituteID_.Value.ToString());

#if DEBUG
				if (this.Request.IsLocal)
				{
					this.tbUserName.Text = this.Request.Params["FacultyUsername"];
					var password = this.Request.Params["Password"];
					if (!string.IsNullOrWhiteSpace(password))
						this.literalJs.Text = $"<script type=\"text/javascript\">{this.tbPassword.ClientID}.value = \"{password.Replace("\"", "\\\"")}\";</script>";
					var instituteID = this.Request.Params["InstituteID"].TryToInt();
					if (instituteID != null && this.ddlInstituteID.Items.FindByValue(instituteID.Value.ToString()) != null)
						this.ddlInstituteID.SelectedValue = instituteID.Value.ToString();
				}
#endif
				this.DisplayCaptchaIfRequired(this.divReCaptcha, this.reCaptcha);

			}

			if (string.IsNullOrWhiteSpace(this.tbUserName.Text))
				this.tbUserName.Focus();
			else
				this.tbPassword.Focus();
		}

		protected void btnLogin_Click(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			this.AddAttempt(this.divReCaptcha, this.reCaptcha);
			var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
			var loginResult = BL.Core.LoginManagement.Faculty.Login(instituteID, this.tbUserName.Text, this.tbPassword.Text, this.UserAgent, this.ClientIPAddress);
			if (loginResult.LoginFailureReason != null)
				Common.ProcessLoginFailureStatus(this, UserTypes.Faculty, loginResult.LoginFailureReason.Value, null, this.loginAlert);
			else
			{
				var loginInfo = loginResult.LoginInfo;
				var facultyIdentity = new FacultyIdentity(loginInfo.UserName, loginInfo.Name, loginInfo.Email, loginInfo.UserLoginHistoryID, loginInfo.LoginSessionGuid, loginInfo.InstituteID, loginInfo.InstituteName, loginInfo.InstituteShortName, loginInfo.InstituteAlias, loginInfo.FacultyMemberID, this.ClientIPAddress, this.UserAgent);
				SessionHelper.CreateUserSession(facultyIdentity, GetPageUrl<Sys.Faculty.PrepareSession>());
			}
		}
	}
}