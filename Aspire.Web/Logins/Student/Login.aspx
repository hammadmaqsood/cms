﻿<%@ Page Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Aspire.Web.Logins.Student.Login" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:Panel runat="server" CssClass="row" Style="margin-top: 2%" DefaultButton="btnLogin">
		<div class="col-md-4 col-md-offset-4">
			<div class="form-group">
				<aspire:AspireAlert ID="loginAlert" runat="server" />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="tbEnrollment" Text="Enrollment:" />
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					<aspire:AspireTextBox runat="server" TextTransform="UpperCase" MaxLength="100" ID="tbEnrollment" PlaceHolder="Enrollment" />
				</div>
				<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="Login" ControlToValidate="tbEnrollment" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Enrollment." />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="tbPassword" Text="Password:" />
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
					<aspire:AspireTextBox runat="server" MaxLength="100" ValidationGroup="Login" TextMode="Password" ID="tbPassword" PlaceHolder="Password" />
				</div>
				<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="Login" ControlToValidate="tbPassword" RequiredErrorMessage="This field is required." />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="ddlInstituteID" Text="Institute:" />
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-university"></i></span>
					<aspire:AspireDropDownList runat="server" ValidationGroup="Login" ID="ddlInstituteID" />
				</div>
				<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="Login" ControlToValidate="ddlInstituteID" RequiredErrorMessage="This field is required." />
			</div>
			<div class="form-group">
				<aspire:AspireLabel runat="server" AssociatedControlID="ddlSubUserType" Text="Role:" />
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user-cog"></i></span>
					<aspire:AspireDropDownList runat="server" ValidationGroup="Login" ID="ddlSubUserType" />
				</div>
				<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="Login" ControlToValidate="ddlSubUserType" RequiredErrorMessage="This field is required." />
			</div>
			<div class="form-group">
				<aspire:ReCaptcha runat="server" ID="reCaptcha" />
				<aspire:RecaptchaValidator runat="server" ID="cvRecaptcha" ValidationGroup="Login" ErrorMessage="Verify you are not a robot." CssClass="text-danger" />
			</div>
			<div class="form-group">
				<aspire:AspireButton ButtonType="Primary" Text="Sign In" ID="btnLogin" ValidationGroup="Login" OnClick="btnLogin_Click" CssClass="btn-block" runat="server" />
			</div>
			<div class="form-group">
				<div class="text-right">
					<aspire:AspireHyperLink runat="server" Text="Forgot Password?" NavigateUrl="ForgotPassword.aspx" />
				</div>
			</div>
		</div>
		<asp:Literal Mode="PassThrough" ID="literalJs" runat="server" />
	</asp:Panel>
	<script type="text/javascript">
		deleteAllCookies();
	</script>
</asp:Content>
