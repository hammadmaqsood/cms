﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Logins.Student
{
	[AspirePage("780F315D-B591-4612-85F6-15111EA5218C", UserTypes.Anonymous, "~/Logins/Student/Login.aspx", "Student Login", false, AspireModules.None)]
	public partial class Login : LoginBasePage
	{
		public override AspireThemes AspireTheme => AspireDefaultThemes.Student;
		public override bool PageTitleVisible => true;
		public override string PageTitle => "Student";
		public override PageIcon PageIcon => null;
		public override HorizontalAlign PageTitleHorizontalAlign => HorizontalAlign.Center;

		public static string GetPageUrl(string enrollment, AspireAlert.AlertTypes? alertType, string message)
		{
			return GetAspirePageAttribute<Login>().PageUrl.AttachQueryParams("Enrollment", enrollment, "AlertType", alertType, "Message", message, EncryptionModes.ConstantSaltNoneUserSessionNone);
		}

		public static void Redirect(string email, AspireAlert.AlertTypes? alertType, string message)
		{
			Redirect(GetPageUrl(email, alertType, message));
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			if (Web.Common.AspireIdentity.Current != null)
				Default.RedirectToHomeScreen();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlInstituteID.FillInstitutesShortNames(CommonListItems.Select);
				var instituteID_ = BL.Core.Common.Common.GetFirstInstituteID(this.ClientIPAddress);
				if (instituteID_ != null)
					this.ddlInstituteID.SetSelectedValueIfExists(instituteID_.Value.ToString());
				this.ddlSubUserType.DataBind(new[]
				{
					new ListItem("Student", SubUserTypes.None.ToString()),
					new ListItem(SubUserTypes.Parents.ToFullName(), SubUserTypes.Parents.ToString()),
				});
				var enrollment = this.Request.GetParameterValue("Enrollment");
				if (!string.IsNullOrWhiteSpace(enrollment))
					this.tbEnrollment.Text = enrollment;

#if DEBUG
				if (this.Request.IsLocal)
				{
					this.tbEnrollment.Text = this.Request.Params["Enrollment"];
					var password = this.Request.Params["Password"];
					if (!string.IsNullOrWhiteSpace(password))
						this.literalJs.Text = $"<script type=\"text/javascript\">{this.tbPassword.ClientID}.value = \"{password.Replace("\"", "\\\"")}\";</script>";
					var instituteID = this.Request.Params["InstituteID"].TryToInt();
					if (instituteID != null && this.ddlInstituteID.Items.FindByValue(instituteID.Value.ToString()) != null)
						this.ddlInstituteID.SelectedValue = instituteID.Value.ToString();
				}
#endif
				this.loginAlert.AddInfoAlert("Use the below Forgot Password link for generating new password for first time use.");
			}

			if (string.IsNullOrWhiteSpace(this.tbEnrollment.Text))
				this.tbEnrollment.Focus();
			else
				this.tbPassword.Focus();
		}

		protected void btnLogin_Click(object sender, EventArgs e)
		{
			if (this.IsPostbackAndNotValid)
				return;
			var instituteID = this.ddlInstituteID.SelectedValue.ToInt();
			var subUserTypeEnum = this.ddlSubUserType.GetSelectedEnumValue<SubUserTypes>();
			var loginResult = BL.Core.LoginManagement.Student.Login(instituteID, this.tbEnrollment.Text, this.tbPassword.Text, subUserTypeEnum, this.UserAgent, this.ClientIPAddress);
			if (loginResult.LoginFailureReason != null)
				Common.ProcessLoginFailureStatus(this, UserTypes.Student, loginResult.LoginFailureReason.Value, loginResult.BlockedReason, this.loginAlert);
			else
			{
				var loginInfo = loginResult.LoginInfo;
				var studentIdentity = new StudentIdentity(loginInfo.UserName, loginInfo.Name, loginInfo.UniversityEmail, loginInfo.SubUserTypeEnum, loginInfo.UserLoginHistoryID, loginInfo.LoginSessionGuid, loginInfo.InstituteID, loginInfo.InstituteCode, loginInfo.InstituteName, loginInfo.InstituteShortName, loginInfo.InstituteAlias, loginInfo.StudentID, loginInfo.Enrollment, this.ClientIPAddress, this.UserAgent);
				SessionHelper.CreateUserSession(studentIdentity, GetPageUrl<Sys.Student.PrepareSession>());
			}
		}
	}
}