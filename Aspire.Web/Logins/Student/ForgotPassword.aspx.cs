﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web.Logins.Student
{
	[AspirePage("DFA48453-4BF7-411F-967C-DDF62F585877", UserTypes.Anonymous, "~/Logins/Student/ForgotPassword.aspx", null, false, AspireModules.None)]
	public partial class ForgotPassword : AnonymousPage
	{
		public override AspireThemes AspireTheme => AspireDefaultThemes.Student;
		public override PageIcon PageIcon => null;
		public override HorizontalAlign PageTitleHorizontalAlign => HorizontalAlign.Center;
		public override string PageTitle => "Student";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ddlInstituteID.FillInstitutesShortNames(CommonListItems.Select);
				var instituteID_ = BL.Core.Common.Common.GetFirstInstituteID(this.ClientIPAddress);
				if (instituteID_ != null)
					this.ddlInstituteID.SetSelectedValueIfExists(instituteID_.Value.ToString());

#if DEBUG
				if (this.Request.IsLocal)
				{
					this.tbEnrollment.Text = this.Request.Params["Enrollment"];
					this.tbUniversityEmail.Text = this.Request.Params["Email"];
					this.ddlInstituteID.SelectedIndex = 1;
				}
#endif
			}

			this.tbEnrollment.Focus();
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (this.IsPostbackAndNotValid)
				return;
			var result = BL.Core.LoginManagement.Student.ForgotPassword(this.tbEnrollment.Text, this.tbUniversityEmail.Text, this.ddlInstituteID.SelectedValue.ToInt(), GetPageUrl<PasswordReset>().ToAbsoluteUrl());
			if (result == null)
			{
				this.AddErrorAlert("Provided information is invalid.");
				return;
			}
			if (result.PersonalEmail != null && result.UniversityEmail != null)
			{
				this.AddSuccessAlert($"Email containing Password Reset Link has been sent to {result.PersonalEmail} and {result.UniversityEmail}.");
				Redirect<ForgotPassword>();
			}
			else if (result.PersonalEmail != null)
			{
				this.AddSuccessAlert($"Email containing Password Reset Link has been sent to {result.PersonalEmail}.");
				Redirect<ForgotPassword>();
			}
			else if (result.UniversityEmail != null)
			{
				this.AddSuccessAlert($"Email containing Password Reset Link has been sent to {result.UniversityEmail}.");
				Redirect<ForgotPassword>();
			}
		}
	}
}