﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="PasswordReset.aspx.cs" Inherits="Aspire.Web.Logins.Student.PasswordReset" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:ScriptManagerProxy runat="server">
		<Scripts>
			<asp:ScriptReference Name="zxcvbn" />
		</Scripts>
	</asp:ScriptManagerProxy>
	<div class="form-horizontal">
		<div class="form-group">
			<aspire:AspireLabel runat="server" CssClass="col-sm-3 col-md-3 col-lg-2" Text="New Password:" AssociatedControlID="tbNewPassword" />
			<div class="col-sm-9 col-md-6 col-lg-4">
				<aspire:AspireTextBox runat="server" TextMode="Password" ValidationGroup="Reset" ID="tbNewPassword" PlaceHolder="New Password" />
				<aspire:AspirePasswordStrengthMeter runat="server" TextBoxID="tbNewPassword" />
			</div>
			<div class="col-sm-offset-3 col-sm-9 col-md-offset-0 col-md-3 col-lg-6 form-control-static">
				<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="Reset" ControlToValidate="tbNewPassword" RequiredErrorMessage="This field is required." />
				<aspire:AspirePasswordPolicyValidator runat="server" ControlToValidate="tbNewPassword" ValidationGroup="Reset" />
			</div>
		</div>
		<div class="form-group">
			<aspire:AspireLabel runat="server" CssClass="col-sm-3 col-md-3 col-lg-2" Text="Confirm New Password:" AssociatedControlID="tbConfirmNewPassword" />
			<div class="col-sm-9 col-md-6 col-lg-4">
				<aspire:AspireTextBox runat="server" TextMode="Password" ValidationGroup="Reset" ID="tbConfirmNewPassword" PlaceHolder="Confirm New Password" />
			</div>
			<div class="col-sm-offset-3 col-sm-9 col-md-offset-0 col-md-3 col-lg-6 form-control-static">
				<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="Reset" ControlToValidate="tbConfirmNewPassword" RequiredErrorMessage="This field is required." />
				<aspire:AspireCompareValidator ValidateEmptyText="True" runat="server" ErrorMessage="Passwords do not match." ValidationGroup="Reset" ControlToValidate="tbConfirmNewPassword" ControlToCompare="tbNewPassword" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-9 col-md-offset-3 col-md-6 col-lg-offset-2 col-lg-4">
				<aspire:AspireButton runat="server" ID="btnSubmit" ButtonType="Primary" ValidationGroup="Reset" Text="Submit" OnClick="btnSubmit_OnClick" />
				<aspire:AspireHyperLink runat="server" NavigateUrl="Login.aspx" Text="Cancel" />
			</div>
		</div>
	</div>
</asp:Content>
