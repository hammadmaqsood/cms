﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="Aspire.Web.Logins.Student.ForgotPassword" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<div class="container">
		<div class="row" style="margin-top: 2%;">
			<div class="col-md-offset-3 col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Forgot Password</h3>
					</div>
					<asp:Panel runat="server" DefaultButton="btnSubmit" CssClass="panel-body">
						<aspire:Label CssClass="help-block" runat="server" Text="Provide your university email address. We'll send you an email with a link to reset your password. In case if university email account is not created then provide your personal email address." />
						<div class="form-group">
							<aspire:AspireLabel runat="server" AssociatedControlID="tbEnrollment" Text="Enrollment:" />
							<div class="input-group">
								<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
								<aspire:AspireTextBox runat="server" MaxLength="100" ID="tbEnrollment" PlaceHolder="Enrollment" ValidationGroup="Forgot" />
							</div>
							<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="Forgot" ControlToValidate="tbEnrollment" RequiredErrorMessage="This field is required." InvalidDataErrorMessage="Invalid Enrollment." />
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" AssociatedControlID="tbUniversityEmail" Text="Email:" />
							<div class="input-group">
								<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
								<aspire:AspireTextBox runat="server" MaxLength="100" ValidationGroup="Forgot" ID="tbUniversityEmail" PlaceHolder="University Email Address" />
							</div>
							<aspire:AspireStringValidator runat="server" ValidationExpression="Email" InvalidDataErrorMessage="Invalid email address." AllowNull="false" ValidationGroup="Forgot" ControlToValidate="tbUniversityEmail" RequiredErrorMessage="This field is required." />
						</div>
						<div class="form-group">
							<aspire:AspireLabel runat="server" AssociatedControlID="ddlInstituteID" Text="Institute:" />
							<div class="input-group">
								<span class="input-group-addon"><span class="fa fa-university"></span></span>
								<aspire:AspireDropDownList runat="server" ValidationGroup="Forgot" ID="ddlInstituteID" />
							</div>
							<aspire:AspireStringValidator runat="server" AllowNull="false" ValidationGroup="Forgot" ControlToValidate="ddlInstituteID" RequiredErrorMessage="This field is required." />
						</div>
						<div class="form-group">
							<aspire:ReCaptcha runat="server" ID="reCaptcha" />
							<aspire:RecaptchaValidator runat="server" ID="cvRecaptcha" ValidationGroup="Forgot" ErrorMessage="Verify you are not a robot." CssClass="text-danger" />
						</div>
						<div class="form-group text-center">
							<aspire:AspireButton Text="Submit" ID="btnSubmit" ValidationGroup="Forgot" ButtonType="Primary" runat="server" OnClick="btnSubmit_Click" />
							<aspire:AspireHyperLink runat="server" Text="Back to Login" NavigateUrl="Login.aspx" />
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
