﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web.Logins.Student
{
	[AspirePage("E77CC74C-EDCF-4C89-9E45-A013B234D250", UserTypes.Student, "~/Logins/Student/PasswordReset.aspx", "Password Reset", false, AspireModules.None)]
	public partial class PasswordReset : AnonymousPage
	{
		public override PageIcon PageIcon => new PageIcon(AspireGlyphicons.lock_);
		public override string PageTitle => "Reset Password";
		public override AspireThemes AspireTheme => AspireDefaultThemes.Student;
		private Guid? ConfirmationCode => this.Request.Params["C"].TryToGuid();
		private Guid? ConfirmationCodeForParents => this.Request.Params["PC"].TryToGuid();

		private (Guid, bool)? GetConfirmationCode()
		{
			if (this.ConfirmationCode == null)
			{
				if (this.ConfirmationCodeForParents == null)
				{
					Redirect<Login>();
					return null;
				}
				return (this.ConfirmationCodeForParents.Value, true);
			}
			return (this.ConfirmationCode.Value, false);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				var code = this.GetConfirmationCode();
				if (code == null)
				{
					Redirect<Login>();
					return;
				}
				var status = BL.Core.LoginManagement.Student.GetConfirmationCodeStatus(code.Value.Item1, code.Value.Item2);
				switch (status)
				{
					case BL.Core.LoginManagement.Student.ConfirmationCodeStatuses.NoRecordFound:
					case BL.Core.LoginManagement.Student.ConfirmationCodeStatuses.LinkExpired:
						Login.Redirect(null, AspireAlert.AlertTypes.Error, "Confirmation Link is invalid. It's also possible that the verification link has expired.");
						return;
					case BL.Core.LoginManagement.Student.ConfirmationCodeStatuses.Success:
						this.tbNewPassword.Text = null;
						this.tbConfirmNewPassword.Text = null;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		protected void btnSubmit_OnClick(object sender, EventArgs e)
		{
			if (!this.IsValid)
				return;
			var code = this.GetConfirmationCode();
			if (code == null)
			{
				Redirect<Login>();
				return;
			}
			var newPassword = this.tbNewPassword.Text;
			var status = BL.Core.LoginManagement.Student.ResetPassword(code.Value.Item1, code.Value.Item2, newPassword);
			switch (status)
			{
				case BL.Core.LoginManagement.Student.ResetPasswordStatuses.NoRecordFound:
				case BL.Core.LoginManagement.Student.ResetPasswordStatuses.LinkExpired:
					Login.Redirect(null, AspireAlert.AlertTypes.Error, "Confirmation Link is invalid. It's also possible that the verification link has expired.");
					break;
				case BL.Core.LoginManagement.Student.ResetPasswordStatuses.Success:
					Login.Redirect(null, AspireAlert.AlertTypes.Success, "Your password has been changed.");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}