﻿using Aspire.Lib.Extensions;
using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web
{
	public partial class Breadcrumb : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			var breadcrumbHtml = (string)this.ViewState["BreadcrumbHtml"];
			if (breadcrumbHtml == null)
			{
				var breadcrumbPage = this.Page as IBreadcrumb;
				var sideBar = this.Page as ISidebar;
				if (breadcrumbPage != null && breadcrumbPage.BreadcrumbVisible && sideBar != null)
					breadcrumbHtml = sideBar.BreadcrumbHtml.TrimAndMakeItNullIfEmpty();
			}
			if (breadcrumbHtml == null)
				this.Controls.Clear();
			else
				this.Controls.Add(new Literal() { Text = @"<div id=""breadCrumb"" class=""hidden-sm hidden-xs"">" + breadcrumbHtml + "</div>" });
		}
	}
}