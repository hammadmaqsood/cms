﻿using System.Web.Services;

namespace Aspire.Web.IntegrationServices.AlliedBank
{
	[WebService(Namespace = "https://cms.bahria.edu.pk/IntegrationServices/AlliedBank/Service")]
	[WebServiceBinding(ConformsTo = WsiProfiles.None)]
	[System.ComponentModel.ToolboxItem(false)]
	public sealed class Service : WebService
	{
		[WebMethod]
		public string BillInquiry(string userName, string password, string consumerNumber, string bankMnemonic, string reserved)
		{
			return BL.Core.IntegrationServices.AlliedBank.Service.BillInquiry(userName, password, consumerNumber, bankMnemonic, reserved);
		}

		[WebMethod]
		public string BillPayment(string userName, string password, string consumerNumber, string authId, string amount, string tranDate, string tranTime, string bankMnemonic, string reserved)
		{
			return BL.Core.IntegrationServices.AlliedBank.Service.BillPayment(userName, password, consumerNumber, authId, amount, tranDate, tranTime, bankMnemonic, reserved);
		}

		[WebMethod]
		public string EchoMessage(string userName, string password, string ping)
		{
			return BL.Core.IntegrationServices.AlliedBank.Service.EchoMessage(userName, password, ping);
		}
	}
}
