﻿using Aspire.BL.Core.IntegrationServices.HR;
using Aspire.Model.Entities;
using Microsoft.Reporting.WebForms;

namespace Aspire.Web.IntegrationServices.HR
{
	public sealed class Service : BL.Core.IntegrationServices.HR.Service
	{
		public override byte[] GetTeacherEvaluation(string cnic, int offeredCourseID, SurveyConductTypes surveyConductType)
		{
			var reportFileName = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "Reports/QualityAssurance/Executive/SurveySummary.rdlc";
			var reportDataSet = this.GetTeacherEvaluation(cnic, offeredCourseID, (SurveyConduct.SurveyConductTypes)surveyConductType);
			if (reportDataSet == null)
				return null;

			using (var reportViewer = new ReportViewer())
			{
				reportViewer.ProcessingMode = ProcessingMode.Local;
				reportViewer.LocalReport.ReportPath = reportFileName;
				reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.PageHeader), new[] { reportDataSet.PageHeader }));
				reportViewer.LocalReport.DataSources.Add(new ReportDataSource(nameof(reportDataSet.Summary), reportDataSet.Summary));
				reportViewer.LocalReport.EnableExternalImages = true;
				return reportViewer.LocalReport.Render("PDF");
			}
		}
	}
}
