﻿using Aspire.Lib.Extensions;
using System.Collections.Generic;
using System.Web.Services;

namespace Aspire.Web.IntegrationServices.LMS
{
	[WebService(Namespace = "https://cms.bahria.edu.pk/IntegrationServices/LMS/Service")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	public sealed class Service : WebService
	{
		[WebMethod]
		public string VerifySession(string userName, string password, string key)
		{
			var userNameGuid = userName.TryToGuid();
			var passwordGuid = password.TryToGuid();
			var keyGuid = key.TryToGuid();
			if (userNameGuid == null || passwordGuid == null || keyGuid == null)
				return null;
			var user = BL.Core.IntegrationServices.LMS.Service.VerifySession(userNameGuid.Value, passwordGuid.Value, keyGuid.Value);
			return new { user.ID, user.UserType, user.Email, user.Name }.ToJsonString();
		}

		[WebMethod]
		public BL.Core.IntegrationServices.LMS.User VerifySessionNew(string userName, string password, string key)
		{
			var userNameGuid = userName.TryToGuid();
			var passwordGuid = password.TryToGuid();
			var keyGuid = key.TryToGuid();
			if (userNameGuid == null || passwordGuid == null || keyGuid == null)
				return null;
			return BL.Core.IntegrationServices.LMS.Service.VerifySession(userNameGuid.Value, passwordGuid.Value, keyGuid.Value);
		}

		[WebMethod(EnableSession = false)]
		public string Ping(string userName, string password)
		{
			var userNameGuid = userName.TryToGuid();
			var passwordGuid = password.TryToGuid();
			if (userNameGuid == null || passwordGuid == null)
				return null;
			return BL.Core.IntegrationServices.LMS.Service.Ping(userNameGuid.Value, passwordGuid.Value);
		}

		[WebMethod(EnableSession = false)]
		public List<BL.Core.IntegrationServices.LMS.Semester> GetSemesters1(string userName, string password)
		{
			var userNameGuid = userName.TryToGuid();
			var passwordGuid = password.TryToGuid();
			if (userNameGuid == null || passwordGuid == null)
				return null;
			return BL.Core.IntegrationServices.LMS.Service.GetSemesters1(userNameGuid.Value, passwordGuid.Value);
		}

		[WebMethod(EnableSession = false)]
		public List<BL.Core.IntegrationServices.LMS.Institute> GetInstitutes2(string userName, string password)
		{
			var userNameGuid = userName.TryToGuid();
			var passwordGuid = password.TryToGuid();
			if (userNameGuid == null || passwordGuid == null)
				return null;
			return BL.Core.IntegrationServices.LMS.Service.GetInstitutes2(userNameGuid.Value, passwordGuid.Value);
		}

		[WebMethod(EnableSession = false)]
		public List<BL.Core.IntegrationServices.LMS.Department> GetDepartments3(string userName, string password)
		{
			var userNameGuid = userName.TryToGuid();
			var passwordGuid = password.TryToGuid();
			if (userNameGuid == null || passwordGuid == null)
				return null;
			return BL.Core.IntegrationServices.LMS.Service.GetDepartments3(userNameGuid.Value, passwordGuid.Value);
		}

		[WebMethod(EnableSession = false)]
		public List<BL.Core.IntegrationServices.LMS.Program> GetPrograms4(string userName, string password)
		{
			var userNameGuid = userName.TryToGuid();
			var passwordGuid = password.TryToGuid();
			if (userNameGuid == null || passwordGuid == null)
				return null;
			return BL.Core.IntegrationServices.LMS.Service.GetPrograms4(userNameGuid.Value, passwordGuid.Value);
		}

		[WebMethod(EnableSession = false)]
		public List<BL.Core.IntegrationServices.LMS.FacultyMember> GetFacultyMembers5(string userName, string password, short offeredSemesterID)
		{
			var userNameGuid = userName.TryToGuid();
			var passwordGuid = password.TryToGuid();
			if (userNameGuid == null || passwordGuid == null)
				return null;
			return BL.Core.IntegrationServices.LMS.Service.GetFacultyMembers5(userNameGuid.Value, passwordGuid.Value, offeredSemesterID);
		}

		[WebMethod(EnableSession = false)]
		public List<BL.Core.IntegrationServices.LMS.Student> GetStudents6(string userName, string password, short offeredSemesterID)
		{
			var userNameGuid = userName.TryToGuid();
			var passwordGuid = password.TryToGuid();
			if (userNameGuid == null || passwordGuid == null)
				return null;
			return BL.Core.IntegrationServices.LMS.Service.GetStudents6(userNameGuid.Value, passwordGuid.Value, offeredSemesterID);
		}

		[WebMethod(EnableSession = false)]
		public List<BL.Core.IntegrationServices.LMS.OfferedCourse> GetOfferedCourses7(string userName, string password, short offeredSemesterID)
		{
			var userNameGuid = userName.TryToGuid();
			var passwordGuid = password.TryToGuid();
			if (userNameGuid == null || passwordGuid == null)
				return null;
			return BL.Core.IntegrationServices.LMS.Service.GetOfferedCourses7(userNameGuid.Value, passwordGuid.Value, offeredSemesterID);
		}

		[WebMethod(EnableSession = false)]
		public List<BL.Core.IntegrationServices.LMS.RegisteredCourse> GetRegisteredCourses8(string userName, string password, short offeredSemesterID)
		{
			var userNameGuid = userName.TryToGuid();
			var passwordGuid = password.TryToGuid();
			if (userNameGuid == null || passwordGuid == null)
				return null;
			return BL.Core.IntegrationServices.LMS.Service.GetRegisteredCourses8(userNameGuid.Value, passwordGuid.Value, offeredSemesterID);
		}
	}
}
