﻿using Aspire.Web.Common;
using System;
using System.Web.UI.WebControls;

namespace Aspire.Web
{
	public partial class SideBar : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			var sidebarPage = this.Page as ISidebar;
			if (sidebarPage != null && sidebarPage.SidebarVisible && !string.IsNullOrWhiteSpace(sidebarPage.SidebarHtml))
				this.ph.Controls.Add(new Literal { Text = sidebarPage.SidebarHtml });
			else
				this.Controls.Clear();
		}
	}
}