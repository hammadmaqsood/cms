﻿using Aspire.BL.Core.Logs.Common;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.IdentityModel.Services;
using System.IdentityModel.Tokens;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.UI.WebControls;

namespace Aspire.Web
{
	public class Global : HttpApplication
	{
		public static bool LiveSite = false;
		public static bool RunWebJobs = false;
		private static void PerformCodingValidations()
		{
			BL.Common.Common.InitEnumValues();
			BL.Core.UserManagement.Admin.UserGroups.ValidateAndCorrectUserGroupPermissions();
			BL.Core.UserManagement.Admin.Users.ValidateAndCorrectUserRoleModules();
		}

		protected void Application_Start(object sender, EventArgs e)
		{
			if (System.Web.Hosting.HostingEnvironment.SiteName.Equals("cms.bahria.edu.pk", StringComparison.InvariantCultureIgnoreCase))
			{
				LiveSite = true;
				RunWebJobs = true;
			}
			else if (System.Web.Hosting.HostingEnvironment.SiteName.Equals("staging.cms.bahria.edu.pk", StringComparison.InvariantCultureIgnoreCase))
			{
				LiveSite = true;
				RunWebJobs = false;
			}
			else
			{
				LiveSite = false;
				RunWebJobs = true;
			}

			WebControl.DisabledCssClass += " disabled";
			PerformCodingValidations();
			AspirePageAttributesDictionary.Initialize();
			AspireHelpMapInitializer.Initialize();
			ErrorHandler.ClearAndSaveErrorLog();
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);

			if (RunWebJobs)
			{
				var jobs = BL.Core.RecurringTasks.BaseTask.GetJobs();
				foreach (var job in jobs)
					TasksHelper.AddTaskAndRun(job);
				BL.Core.Common.EmailsManagement.EmailHelper.QueuePendingEmails();
				TasksHelper.AddTaskAndRun(BL.Core.Common.EmailsManagement.EmailHelper.EmailEngine.StartEmailSending);
			}
		}

		protected void Session_Start(object sender, EventArgs e)
		{
			this.Session.Timeout = 30;
		}

		protected void Application_BeginRequest(object sender, EventArgs e)
		{
			switch (this.Request.Url.Scheme)
			{
				case "https":
					this.Response.AddHeader("Strict-Transport-Security", "max-age=31536000");
					return;
				case "http":
					if (Configurations.Current.AppSettings.IsDevelopmentPC)
					{
						this.Response.Write("Use Https.");
						this.Response.End();
						return;
					}
					var path = this.Request.Url.ToString();
					if (!path.StartsWith("http://"))
						throw new InvalidOperationException(this.Request.Url.ToString());
					path = path.Insert(4, "s");
					HttpContext.Current.Response.RedirectPermanent(path, true);
					return;
				default:
					throw new ArgumentOutOfRangeException(nameof(this.Request.Url.Scheme), this.Request.Url.Scheme, this.Request.Url.ToString());
			}
		}

		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{
		}

		protected void Application_PreSendRequestHeaders()
		{
			this.Response.Headers.Remove("Server");
		}

		protected void Application_Error(object sender, EventArgs e)
		{
			var exc = this.Context.Error;
			if (exc == null)
				return;
			if (ProcessError(exc))
			{
				this.Context.ClearError();
				return;
			}
			if (exc.InnerException != null && ProcessError(exc.InnerException))
			{
				this.Context.ClearError();
				return;
			}
		}

		private static bool ProcessError(Exception exc)
		{
			var noJavascriptException = exc as NoJavascriptException;
			if (noJavascriptException != null)
			{
				HttpContext.Current.ClearError();
				NoJavascript.Redirect(noJavascriptException.PreviousPageUrl);
				return true;
			}

			var aspireAccessDeniedException = exc as AspireAccessDeniedException;
			if (aspireAccessDeniedException != null)
			{
				HttpContext.Current.ClearError();
				Default.Redirect(aspireAccessDeniedException);
				return true;
			}

			var aspireNotAuthorizedException = exc as AspireNotAuthorizedException;
			if (aspireNotAuthorizedException != null)
			{
				HttpContext.Current.ClearError();
				Default.Redirect(aspireNotAuthorizedException);
				return true;
			}

			var aspirePermissionsNotFoundException = exc as AspirePermissionsNotFound;
			if (aspirePermissionsNotFoundException != null)
			{
				HttpContext.Current.ClearError();
				Default.Redirect(aspirePermissionsNotFoundException);
				return true;
			}

			if (exc is SecurityTokenException)
			{
				if (FederatedAuthentication.SessionAuthenticationModule != null)
					FederatedAuthentication.SessionAuthenticationModule.SignOut();
				HttpContext.Current.ClearError();
				HttpContext.Current.Response.Redirect(HttpContext.Current.Request.Url.ToString(), false);
				return true;
			}
			if (exc is AspireEncryptionException)
			{
				HttpContext.Current.ClearError();
				Default.RedirectToHomeScreen("Invalid Url. Try again.");
				return true;
			}

			if (exc is IPAddressChangedException || exc is UserAgentChangedException)
			{
				HttpContext.Current.ClearError();
				var logOffReason = exc is IPAddressChangedException ? UserLoginHistory.LogOffReasons.IPAddressChanged : UserLoginHistory.LogOffReasons.UserAgentChanged;
				switch (AspireIdentity.Current?.UserType)
				{
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
						Sys.Admin.Logoff.Redirect(logOffReason);
						return true;
					case UserTypes.Executive:
						Sys.Executive.Logoff.Redirect(logOffReason);
						return true;
					case UserTypes.Staff:
						Sys.Staff.Logoff.Redirect(logOffReason);
						return true;
					case UserTypes.Candidate:
						Sys.Candidate.Logoff.Redirect(logOffReason);
						return true;
					case UserTypes.Student:
						Sys.Student.Logoff.Redirect(logOffReason);
						return true;
					case UserTypes.Faculty:
						Sys.Faculty.Logoff.Redirect(logOffReason);
						return true;
					case null:
						BasePage.Redirect<Default>();
						return true;
					default:
						throw new NotImplementedEnumException(AspireIdentity.Current?.UserType);
				}
			}

			if (exc is System.Web.UI.ViewStateException)
			{
				HttpContext.Current.ClearError();
				var url = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path) + $"?{DateTime.Now:u}";
				HttpContext.Current.Response.Redirect(url);
				return true;
			}
			return false;
		}

		protected void Session_End(object sender, EventArgs e)
		{

		}

		protected void Application_End(object sender, EventArgs e)
		{
			TasksHelper.CancelAllTasksAndWait();
		}
	}
}