﻿using Aspire.Lib.Extensions;
using Aspire.Lib.WebControls;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Aspire.Web
{
	public static class PageAlertsController
	{
		private class Alert
		{
			public Guid UserLoginSessionGuid { get; set; }
			public string Message { get; set; }
			public bool HtmlEncoded { get; set; }
			public AspireAlert.AlertTypes AlertType { get; set; }
			public DateTime ExpiredOn { get; } = DateTime.Now.AddMinutes(ExpiryMinutes);
		}

		private static readonly List<Alert> Alerts = new List<Alert>();

		private const string UserSessionGuid = "$UserSessionGuid$";
		private static Guid CurrentUserSessionGuid
		{
			get
			{
				var aspireIdentity = HttpContext.Current.User.Identity as AspireIdentity;
				if (aspireIdentity != null)
					return aspireIdentity.LoginSessionGuid;

				var guid = (Guid?)HttpContext.Current.Session[UserSessionGuid];
				if (guid == null)
				{
					guid = Guid.NewGuid();
					HttpContext.Current.Session[UserSessionGuid] = guid;
				}
				return guid.Value;
			}
		}

		private const int ExpiryMinutes = 20;
		private static DateTime _nextFlushTime = DateTime.Now.AddMinutes(ExpiryMinutes);
		private static void AddAlert(AspireAlert.AlertTypes alertType, string message, bool htmlEncoded)
		{
			lock (Alerts)
			{
				Alerts.Add(new Alert
				{
					UserLoginSessionGuid = CurrentUserSessionGuid,
					AlertType = alertType,
					Message = message,
					HtmlEncoded = htmlEncoded,
				});
				if (_nextFlushTime < DateTime.Now)
				{
					Alerts.RemoveAll(a => a.ExpiredOn <= DateTime.Now);
					_nextFlushTime = DateTime.Now.AddMinutes(ExpiryMinutes);
				}
			}
		}

		private static List<Alert> CurrentUserAlerts
		{
			get
			{
				if (HttpContext.Current.Session == null)
					return null;
				var currentUserSessionGuid = CurrentUserSessionGuid;
				lock (Alerts)
				{
					return Alerts.Where(a => a.UserLoginSessionGuid == currentUserSessionGuid).ToList();
				}
			}
		}

		public static void DisplayMessages(Control control)
		{
			control.Controls.Clear();
			var alerts = CurrentUserAlerts;
			if (alerts == null || !alerts.Any())
				return;
			var alertControl = new AspireAlert();
			foreach (var alert in alerts)
			{
				alertControl.AddMessage(alert.AlertType, alert.Message, alert.HtmlEncoded);
				lock (Alerts)
				{
					Alerts.Remove(alert);
				}
			}
			control.Controls.Add(alertControl);
		}

		public static void AddAlert(this IAlert alert, AspireAlert.AlertTypes alertType, string message, bool htmlEncoded)
		{
			var control = alert as AspireAlert;
			if (control != null)
				control.AddMessage(alertType, message, htmlEncoded);
			else
				AddAlert(alertType, message, htmlEncoded);
		}

		#region Helpers

		public static void AddSuccessAlert(this IAlert alert, string message, bool htmlEncoded = false)
		{
			alert.AddAlert(AspireAlert.AlertTypes.Success, message, htmlEncoded);
		}

		public static void AddSuccessAlerts(this IAlert alert, bool htmlEncoded, IEnumerable<string> messages)
		{
			messages?.ForEach(message => alert.AddAlert(AspireAlert.AlertTypes.Success, message, htmlEncoded));
		}

		public static void AddWarningAlert(this IAlert alert, string message, bool htmlEncoded = false)
		{
			alert.AddAlert(AspireAlert.AlertTypes.Warning, message, htmlEncoded);
		}

		public static void AddWarningAlerts(this IAlert alert, bool htmlEncoded, IEnumerable<string> messages)
		{
			messages?.ForEach(message => alert.AddAlert(AspireAlert.AlertTypes.Warning, message, htmlEncoded));
		}

		public static void AddErrorAlert(this IAlert alert, string message, bool htmlEncoded = false)
		{
			alert.AddAlert(AspireAlert.AlertTypes.Error, message, htmlEncoded);
		}

		public static void AddErrorAlerts(this IAlert alert, bool htmlEncoded, IEnumerable<string> messages)
		{
			messages?.ForEach(message => alert.AddAlert(AspireAlert.AlertTypes.Error, message, htmlEncoded));
		}

		public static void AddInfoAlert(this IAlert alert, string message, bool htmlEncoded = false)
		{
			alert.AddAlert(AspireAlert.AlertTypes.Info, message, htmlEncoded);
		}

		public static void AddNoRecordFoundAlert(this IAlert alert)
		{
			alert.AddErrorAlert("No record found.");
		}

		public static void AddErrorMessageCannotBeDeletedBecauseChildRecordExists(this IAlert alert, string name)
		{
			alert.AddErrorAlert($"{name} cannot be deleted because child record exists.");
		}

		public static void AddErrorMessageCannotBeUpdatedBecauseChildRecordExists(this IAlert alert, string name)
		{
			alert.AddErrorAlert($"{name} cannot be updated because child record exists.");
		}

		public static void AddSuccessMessageHasBeenDeleted(this IAlert alert, string name, bool plural = false)
		{
			alert.AddSuccessAlert($"{name} {(plural ? "have" : "has")} been deleted.");
		}

		public static void AddSuccessMessageHasBeenAdded(this IAlert alert, string name, bool plural = false)
		{
			alert.AddSuccessAlert($"{name} {(plural ? "have" : "has")} been added.");
		}

		public static void AddSuccessMessageHasBeenUpdated(this IAlert alert, string name, bool plural = false)
		{
			alert.AddSuccessAlert($"{name} {(plural ? "have" : "has")} been updated.");
		}

		public static void AddErrorMessageYouAreNotAuthorizedToPerformThisAction(this IAlert alert)
		{
			alert.AddErrorAlert("You are not authorized to perform this action.");
		}

		#endregion
	}
}