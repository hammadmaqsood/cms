﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="NoJavascript.aspx.cs" Inherits="Aspire.Web.NoJavascript" %>

<asp:Content ContentPlaceHolderID="HeadPH" runat="server">
	<style>
		body > form#form1 > div#pageContainer + footer#footer {
			left: 0 !important;
		}
	</style>
</asp:Content>
<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<h1 class="text-uppercase text-center">Javascript is disabled</h1>
	<p class="text-center">
		Some content on this page cannot be displayed because you have JavaScript disabled.
		To proceed, please enable active scripting for this site in your browser settings.
	</p>
	<div class="text-center">
		<asp:HyperLink CssClass="text-center" runat="server" ID="hlBack" Text="Back to Previous Page" />
	</div>
	<script type="text/javascript">
		window.location = "<%=this.PrevPageUrlClientUrl%>";
	</script>
</asp:Content>
