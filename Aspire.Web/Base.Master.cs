﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Web;
using System.Web.UI;

namespace Aspire.Web
{
	public partial class Base : MasterPage
	{
		protected AspireThemes AspireTheme
		{
			get
			{
				var themePage = this.Page as IPageTheme;
				if (themePage != null)
					return themePage.AspireTheme;
				throw new InvalidOperationException("Specify ThemeName.");
			}
		}

		private bool SideBarButtonVisible
		{
			get
			{
				var sideBarPage = this.Page as ISidebar;
				if (sideBarPage != null)
					return sideBarPage.SidebarVisible;
				return false;
			}
		}

		private const string AntiXsrfTokenKey = "_";
		private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
		private string _antiXsrfTokenValue;

		protected void Page_Init(object sender, EventArgs e)
		{
			var requestCookie = this.Request.Cookies[AntiXsrfTokenKey];
			if (requestCookie != null && Guid.TryParse(requestCookie.Value, out _))
			{
				this._antiXsrfTokenValue = requestCookie.Value;
				this.Page.ViewStateUserKey = this._antiXsrfTokenValue;
			}
			else
			{
				this.Page.ViewStateUserKey = this._antiXsrfTokenValue = Guid.NewGuid().ToString("N");
				this.Response.Cookies.Set(new HttpCookie(AntiXsrfTokenKey)
				{
					HttpOnly = true,
					Value = this._antiXsrfTokenValue,
					Secure = true,
				});
			}
		}

		protected void Page_PreLoad(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ViewState[AntiXsrfTokenKey] = this.Page.ViewStateUserKey;
				this.ViewState[AntiXsrfUserNameKey] = AspireIdentity.Current?.Username ?? string.Empty;
			}
			else
			{
				if ((string)this.ViewState[AntiXsrfTokenKey] != this._antiXsrfTokenValue || (string)this.ViewState[AntiXsrfUserNameKey] != (AspireIdentity.Current?.Username ?? string.Empty))
					throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (this.IsPostBack && string.IsNullOrWhiteSpace(this.hfJsEnabled.Value))
				throw new NoJavascriptException(this.Page.Request.Url.ToString());

			if (!this.IsPostBack)
			{
				var aspireIdentity = AspireIdentity.Current;
				if (aspireIdentity != null)
					switch (aspireIdentity.UserType)
					{
						case UserTypes.Admin:
						case UserTypes.MasterAdmin:
							this.brandName.HRef = this.brandShortName.HRef = this.brandAlias.HRef = AspirePageAttributesDictionary.GetPageUrl<Sys.Admin.Dashboard>();
							break;
						case UserTypes.Candidate:
							this.brandName.HRef = this.brandShortName.HRef = this.brandAlias.HRef = AspirePageAttributesDictionary.GetPageUrl<Sys.Candidate.Dashboard>();
							break;
						case UserTypes.Staff:
							this.brandName.HRef = this.brandShortName.HRef = this.brandAlias.HRef = AspirePageAttributesDictionary.GetPageUrl<Sys.Staff.Dashboard>();
							break;
						case UserTypes.Student:
							this.brandName.HRef = this.brandShortName.HRef = this.brandAlias.HRef = AspirePageAttributesDictionary.GetPageUrl<Sys.Student.Dashboard>();
							break;
						case UserTypes.Faculty:
							this.brandName.HRef = this.brandShortName.HRef = this.brandAlias.HRef = AspirePageAttributesDictionary.GetPageUrl<Sys.Faculty.Dashboard>();
							break;
						case UserTypes.Executive:
							this.brandName.HRef = this.brandShortName.HRef = this.brandAlias.HRef = AspirePageAttributesDictionary.GetPageUrl<Sys.Executive.Dashboard>();
							break;
						case UserTypes.Anonymous:
						case UserTypes.System:
						case UserTypes.Alumni:
						case UserTypes.IntegratedService:
						case UserTypes.ManualSQL:
						case UserTypes.Admins:
						case UserTypes.Any:
							throw new InvalidOperationException();
						default:
							throw new NotImplementedEnumException(aspireIdentity.UserType);
					}
				else
					this.brandName.HRef = this.brandShortName.HRef = this.brandAlias.HRef = AspirePageAttributesDictionary.GetPageUrl<Default>();

				if (!(this.Page is NoJavascript))
				{
					var noJavascriptUrl = NoJavascript.GetPageUrl(this.Request.Url.ToString()).ResolveClientUrl(this.Page).HtmlAttributeEncode();
					this.phNoScript.Controls.Add(new LiteralControl($"<noscript><meta http-equiv=\"Refresh\" content=\"0;URL={noJavascriptUrl}\" /></noscript>"));
				}
			}
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.Page.Title))
				this.Page.Title = "CMS - Bahria University";
			this.aSideBar.Visible = this.SideBarButtonVisible;
			if (Lib.Helpers.Configurations.Current.AppSettings.DEBUG)
				this.pageHead.Controls.AddAt(0, new LiteralControl($@"<script type=""text/javascript"">
var aspireRootPath=""{"~/".ToAbsoluteUrl()}"";
window.LiveSite={Global.LiveSite.ToString().ToLower()};
window.Debugging=true;
window.IsLocal={this.Request.IsLocal.ToString().ToLower()};
</script>"));
			else
				this.pageHead.Controls.AddAt(0, new LiteralControl($@"<script type=""text/javascript"">
var aspireRootPath=""{"~/".ToAbsoluteUrl()}"";
window.LiveSite={Global.LiveSite.ToString().ToLower()};
</script>"));
		}

		protected void ScriptManager_OnAsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
		{
			((ScriptManager)sender).AsyncPostBackErrorMessage = "An unexpected error may have occurred while processing your request.";
#if DEBUG
			if (this.Request.IsLocal)
				((ScriptManager)sender).AsyncPostBackErrorMessage = e.Exception.ToString();
#endif
			BL.Core.Logs.Common.ErrorHandler.LogException(AspireIdentity.Current?.UserLoginHistoryID, e.Exception, -1, this.Request.Url.AbsoluteUri, this.Request.UserAgent, this.Request.UserHostAddress);
		}
	}
}