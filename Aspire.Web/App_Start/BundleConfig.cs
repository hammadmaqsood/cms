﻿using System.Web.Optimization;
using System.Web.UI;

namespace Aspire.Web
{
	public class BundleConfig
	{
		public static void RegisterBundles(BundleCollection bundles)
		{
			BundleTable.EnableOptimizations = true;
			var jQueryVersion = "3.3.1";
			ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new ScriptResourceDefinition
			{
				Path = $"~/Scripts/Plugins/jquery/jquery-{jQueryVersion}.min.js",
				DebugPath = $"~/Scripts/Plugins/jquery/jquery-{jQueryVersion}.js"
			});
			ScriptManager.ScriptResourceMapping.AddDefinition("jquery-ui", new ScriptResourceDefinition
			{
				Path = "~/Scripts/Plugins/jquery-ui/jquery-ui.min.js",
				DebugPath = "~/Scripts/Plugins/jquery-ui/jquery-ui.js",
			});
			ScriptManager.ScriptResourceMapping.AddDefinition("AspireCore", new ScriptResourceDefinition
			{
				Path = "~/Scripts/Bundles/Aspire.min.js",
				DebugPath = "~/Scripts/Bundles/Aspire.js",
			});

			RegisterWebFormJs(bundles);
			RegisterMsAjaxJs(bundles);
			RegisterZxcvbn(bundles);
		}

		private static void RegisterWebFormJs(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/bundles/WebFormsJs").Include(
				"~/Scripts/WebForms/WebForms.js",
				"~/Scripts/WebForms/WebUIValidation.js",
				"~/Scripts/WebForms/MenuStandards.js",
				"~/Scripts/WebForms/Focus.js",
				"~/Scripts/WebForms/GridView.js",
				"~/Scripts/WebForms/DetailsView.js",
				"~/Scripts/WebForms/TreeView.js",
				"~/Scripts/WebForms/WebParts.js"));
		}

		private static void RegisterMsAjaxJs(BundleCollection bundles)
		{
			// Order is very important for these files to work, they have explicit dependencies
			bundles.Add(new ScriptBundle("~/bundles/MsAjaxJs").Include(
				"~/Scripts/WebForms/MsAjax/MicrosoftAjax.js",
				"~/Scripts/WebForms/MsAjax/MicrosoftAjaxApplicationServices.js",
				"~/Scripts/WebForms/MsAjax/MicrosoftAjaxTimer.js",
				"~/Scripts/WebForms/MsAjax/MicrosoftAjaxWebForms.js"));
		}

		private static void RegisterZxcvbn(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/bundles/zxcvbn").Include(
				"~/Scripts/Plugins/zxcvbn/zxcvbn.js",
				"~/Scripts/Plugins/zxcvbn/zxcvbn-bootstrap-strength-meter.js"));

			ScriptManager.ScriptResourceMapping.AddDefinition("zxcvbn", new ScriptResourceDefinition
			{
				Path = "~/bundles/zxcvbn",
			});
		}
	}
}