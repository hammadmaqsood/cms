using Microsoft.AspNet.FriendlyUrls;
using System.Web.Routing;

namespace Aspire.Web
{
	public static class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			var settings = new FriendlyUrlSettings
			{
				//AutoRedirectMode = RedirectMode.Temporary
			};
			routes.EnableFriendlyUrls(settings);
		}
	}
}
