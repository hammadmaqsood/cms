﻿using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web
{
	[AspirePage("49AF3F44-1982-41C2-A96F-71538265598F", UserTypes.Any, "~/Admissions.aspx", "Admissions", false, AspireModules.None)]
	public partial class Admissions : BasePage
	{
		protected override void Authenticate()
		{
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			Redirect<Logins.Candidate.Login>();
		}
	}
}