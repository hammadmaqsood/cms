﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageTitle.ascx.cs" Inherits="Aspire.Web.PageTitle" %>
<div id="pageHeading">
	<h1 id="h1" runat="server">
		<span runat="server" id="icon"></span>
		<asp:Literal ID="ltrl" runat="server" />
	</h1>
</div>
