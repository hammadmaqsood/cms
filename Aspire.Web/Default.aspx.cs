﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web
{
	[AspirePage("{CE8635BC-4989-4B5B-BCF4-A01F1EA6E642}", UserTypes.Any, "~/Default.aspx", "Default", false, AspireModules.None)]
	public partial class Default : AspireBasePage
	{
		public override AspireThemes AspireTheme => AspireThemes.Asphalt;
		public override bool PageTitleVisible => false;
		public override PageIcon PageIcon { get; } = null;
		public override string PageTitle => "CMS - Bahria University";

		protected override void Authenticate()
		{
			//Not required.
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToHomeScreen();
#if DEBUG
			if (this.Request.IsLocal)
			{
				this.divExecutive.Visible = true;
				this.divAdmin.Visible = true;
				var queryParams = $"?UserName={this.Request.Params["UserName"].UrlEncode()}&Password={this.Request.Params["Password"].UrlEncode()}&Enrollment={this.Request.Params["Enrollment"].UrlEncode()}&InstituteID={this.Request.Params["InstituteID"]}&FacultyUsername={this.Request.Params["FacultyUsername"]}&CandidateUsername={this.Request.Params["CandidateUsername"]}&";
				this.hlCandidate.NavigateUrl += queryParams;
				this.hlStudent.NavigateUrl += queryParams;
				this.hlStaff.NavigateUrl += queryParams;
				this.hlFaculty.NavigateUrl += queryParams;
				this.hlExecutive.NavigateUrl += queryParams;
				this.hlAdmin.NavigateUrl += queryParams;
			}
#endif
		}

		public static Type GetDashboardPage()
		{
			switch (Web.Common.AspireIdentity.Current?.UserType)
			{
				case UserTypes.MasterAdmin:
				case UserTypes.Admin:
					return typeof(Sys.Admin.Dashboard);
				case UserTypes.Staff:
					return typeof(Sys.Staff.Dashboard);
				case UserTypes.Candidate:
					return typeof(Sys.Candidate.Dashboard);
				case UserTypes.Student:
					return typeof(Sys.Student.Dashboard);
				case UserTypes.Faculty:
					return typeof(Sys.Faculty.Dashboard);
				case UserTypes.Executive:
					return typeof(Sys.Executive.Dashboard);
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static void RedirectToHomeScreen(string message = null)
		{
			if (!string.IsNullOrWhiteSpace(message))
				PageAlertsController.AddErrorAlert(null, message);
			var aspireIdentity = Web.Common.AspireIdentity.Current;
			if (aspireIdentity == null)
				return;
			switch (aspireIdentity.UserType)
			{
				case UserTypes.MasterAdmin:
				case UserTypes.Admin:
					AspireBasePage.Redirect<Sys.Admin.Dashboard>(false);
					return;
				case UserTypes.Staff:
					AspireBasePage.Redirect<Sys.Staff.Dashboard>(false);
					return;
				case UserTypes.Candidate:
					AspireBasePage.Redirect<Sys.Candidate.Dashboard>(false);
					return;
				case UserTypes.Student:
					AspireBasePage.Redirect<Sys.Student.Dashboard>(false);
					return;
				case UserTypes.Faculty:
					AspireBasePage.Redirect<Sys.Faculty.Dashboard>(false);
					return;
				case UserTypes.Executive:
					AspireBasePage.Redirect<Sys.Executive.Dashboard>(false);
					return;
				case UserTypes.Alumni:
					throw new NotImplementedException();
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static void RedirectToLoginScreen(UserTypes userType)
		{
			switch (userType)
			{
				case UserTypes.MasterAdmin | UserTypes.Admin:
				case UserTypes.MasterAdmin:
				case UserTypes.Admin:
					AspireBasePage.Redirect<Logins.Admin.Login>(false);
					return;
				case UserTypes.Staff:
					AspireBasePage.Redirect<Logins.Staff.Login>(false);
					return;
				case UserTypes.Candidate:
					AspireBasePage.Redirect<Logins.Candidate.Login>(false);
					return;
				case UserTypes.Student:
					AspireBasePage.Redirect<Logins.Student.Login>(false);
					return;
				case UserTypes.Faculty:
					AspireBasePage.Redirect<Logins.Faculty.Login>(false);
					return;
				case UserTypes.Executive:
					AspireBasePage.Redirect<Logins.Executive.Login>(false);
					return;
				case UserTypes.Alumni:
					throw new NotImplementedException();
				case UserTypes.Anonymous:
				case UserTypes.System:
				case UserTypes.IntegratedService:
				case UserTypes.Any:
					throw new InvalidOperationException();
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		internal static void Redirect(AspireAccessDeniedException aspireAccessDeniedException)
		{
			if (aspireAccessDeniedException == null)
				throw new ArgumentNullException(nameof(aspireAccessDeniedException));

			var aspireIdentity = Web.Common.AspireIdentity.Current;
			if (aspireIdentity != null)
			{
				if (aspireAccessDeniedException.UserType.HasFlag(aspireIdentity.UserType))
					throw new InvalidOperationException("User already have required role. Reconsider Business Logic.");
				switch (aspireAccessDeniedException.UserType)
				{
					case UserTypes.Anonymous:
						RedirectToHomeScreen("You need to logout in order to access the requested page.");
						return;
					case UserTypes.Admins:
					case UserTypes.MasterAdmin:
					case UserTypes.Admin:
					case UserTypes.Staff:
					case UserTypes.Candidate:
					case UserTypes.Executive:
					case UserTypes.Student:
					case UserTypes.Faculty:
					case UserTypes.Alumni:
						RedirectToHomeScreen("You are not authorized to access the requested page.");
						return;
					default:
						throw new ArgumentOutOfRangeException("Usertype not handled: " + aspireAccessDeniedException.UserType);
				}
			}
			else
			{
				if (aspireAccessDeniedException.LoginScreenUserType == null)
					throw new InvalidOperationException("Reconsider Business Logic.");
				RedirectToLoginScreen(aspireAccessDeniedException.LoginScreenUserType.Value);
			}
		}

		internal static void Redirect(AspirePermissionsNotFound aspirePermissionsNotFound)
		{
			RedirectToHomeScreen(aspirePermissionsNotFound.ToString());
		}

		internal static void Redirect(AspireNotAuthorizedException aspireNotAuthorizedException)
		{
			if (aspireNotAuthorizedException == null)
				throw new ArgumentNullException(nameof(aspireNotAuthorizedException));

			RedirectToHomeScreen("You are not authorized to perform this operation.");
		}
	}
}