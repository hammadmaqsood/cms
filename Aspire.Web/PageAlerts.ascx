﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageAlerts.ascx.cs" Inherits="Aspire.Web.PageAlerts" %>

<asp:UpdatePanel ID="up" runat="server" UpdateMode="Always" EnableViewState="False">
	<ContentTemplate>
		<asp:PlaceHolder runat="server" ID="phAlerts" EnableViewState="False" />
	</ContentTemplate>
</asp:UpdatePanel>
