﻿if (window.Debugging !== true)
	history.pushState({ page: window.location.href }, document.title, window.location.protocol + "//" + window.location.host + window.location.pathname);
var media = window.matchMedia("(max-width: 767px)");
$(function () {
	if (window.LiveSite !== true) {
		$("<div></div>").text("Test Environment").css({
			"background-color": "yellow",
			"color": "red",
			"width": "150px",
			"border": "0 none",
			"border-radius": "0 0 10px 10px",
			"font-size": "16px",
			"font-weight": "bold",
			"text-align": "center",
			"padding": "0",
			"margin": "0",
			"position": "fixed",
			"top": "0",
			"left": "calc(50% - 75px)"
		}).appendTo($("div#pageContainer>header#header"));
	}
	initSideBar();
	initAvatars();
	initScrollToTop();
	initThemes();
	initInputMasks();
	initAlertModal();
	disableSelection();
	AspireModal.initProcessingModalForPartialPostbacks();
	Sys.Application.add_load(function () {
		initInputMasks();
	});
});

function deleteAllCookies() {
	setCookie("SideBarVisible", 1, -1);
}

function setCookie(cookieName, cookieValue, expiryMinutes) {
	var d = new Date();
	var expiryMilliseconds = expiryMinutes * 60 * 1000;
	d.setTime(d.getTime() + expiryMilliseconds);
	var expires = "expires=";
	if (expiryMinutes <= 0)
		expires += "Thu, 01 Jan 1970 00:00:00 UTC";
	else
		expires += d.toUTCString();
	document.cookie = cookieName + "=" + cookieValue + "; " + expires + ";path=/";
}

function getCookie(cookieName) {
	var name = cookieName + "=";
	var ca = document.cookie.split(";");
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) === " ")
			c = c.substring(1);
		if (c.indexOf(name) === 0)
			return c.substring(name.length, c.length);
	}
	return "";
}

function hideSideBar() {
	setCookie("SideBarVisible", 0, 20);
	$("body>form>div#pageContainer").addClass("HideSideBar");
}
function showSideBar() {
	setCookie("SideBarVisible", 1, 20);
	$(">aside#sideBar", $("body>form>div#pageContainer").removeClass("HideSideBar")).focus();
}

function initSideBar() {
	var pageContainer = $("body>form>div#pageContainer");
	var header = $(">header#header", pageContainer);
	var containerFluid = $(">.container-fluid", header);
	var navbarHeader = $(">.navbar-header", containerFluid);
	var sideBarButton = $(">a#aSideBar", navbarHeader).attr("title", "Show/Hide Menu").tooltip({ placement: "bottom" });
	var collapseButton = $(">button.navbar-toggle", navbarHeader);
	var accountsNavbar = $(">div#AccountsNavbar", containerFluid);
	var sideBar = $(">aside#sideBar", pageContainer);

	if (!sideBar.length) {
		pageContainer.addClass("HideSideBar");
		return;
	}
	if (getCookie("SideBarVisible") !== "1" && getCookie("SideBarVisible") !== "0")
		setCookie("SideBarVisible", 1, 1);

	if (getCookie("SideBarVisible") === "1")
		hideSideBar();
	else
		showSideBar();

	collapseButton.click(hideSideBar);

	if (sideBar.length === 0) {
		hideSideBar();
	}
	else {
		if (!media.matches)
			$("div#sideMenuList", sideBar).slimscroll();

		sideBarButton.click(function () {
			if (accountsNavbar.hasClass("collapse in"))
				accountsNavbar.collapse("hide");

			if (pageContainer.hasClass("HideSideBar"))
				showSideBar();
			else
				hideSideBar();
		}).click();
	}
}

function initAvatars() {
	$.each($("img.avatar"), function (i, v) {
		$($("<canvas></canvas>").css({ 'background-color': "#FFFFFF" }).attr({ 'width': $(v).attr("width"), 'height': $(v).attr("height") }))
			.insertAfter($(this))
			.jdenticon($(v).attr("data-jdenticon-hash"), 0);
		$(this).remove();
	});
}

function initScrollToTop() {
	var scrollTopDiv = $("<div></div>").appendTo($("body")).addClass("ScrollToTop").append($("<span><span class=\"fa fa-4x fa-arrow-circle-up\"></span></span>")).attr("title", "Scroll to Top");
	$(document).on("scroll", function () {
		$(scrollTopDiv).toggleClass("Show", $(window).scrollTop() > 500);
	});
	var verticalOffset;
	$(scrollTopDiv).click(function () {
		verticalOffset = typeof verticalOffset !== "undefined" ? verticalOffset : 0;
		$("html, body").animate({
			scrollTop: $("body").offset().top
		}, 500, "linear");
	});
}

function initThemes() {
	if (window.Debugging === undefined || window.Debugging === null || window.Debugging !== true)
		return;
	if (window.IsLocal === undefined || window.IsLocal === null || window.IsLocal !== true)
		return;
	var themes = ["Fire", "Deepsea", "Amethyst", "Army", "Asphalt", "City", "Autumn", "Cherry", "Dawn", "Diamond", "Grass", "Leaf", "Night", "Ocean", "Oil", "Stone", "Sun", "Tulip", "Wood"];
	var bgColor = ["#C0392B", "#005F9C", "#9B59B6", "#7A8C74", "#34495E", "#615C5C", "#CC8F3F", "#E74C3C", "#D35400", "#1ABC9C", "#849A47", "#27AE60", "#454545", "#009FBC", "#B6B159", "#95A5A6", "#F39C12", "#8E44AD", "#B67559"];
	var ul = $("#AccountsNavbar>ul:last-child");
	var li = $("<li class=\"dropdown\"></li>").prependTo($(ul)).append($("<a style=\"line-height:42px; vertical-align:middle;\" href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\">Themes</a>"));
	var ulDropDownMenu = $("<ul class=\"dropdown-menu\" role=\"menu\"></ul>").appendTo($(li));
	$.each(themes, function (i, v) {
		var a = $("<a href=\"#\" style=\"height:20px;margin:0;padding:0;display:block\"></a>").css("background-color", bgColor[i]).text(v).click(function () {
			$("body").removeAttr("class").addClass(v);
		});
		$("<li></li>").append(a).appendTo($(ulDropDownMenu));
	});
}

function initInputMasks() {
	$.extend($.inputmask.defaults.aliases, {
		'cnic': {
			autoUnmask: false,
			clearMaskOnLostFocus: true,
			clearIncomplete: true,
			mask: "99999-9999999-9",
			definitions: { '9': { validator: "[0-9]" } }
		},
		'mobile': {
			autoUnmask: false,
			clearMaskOnLostFocus: true,
			clearIncomplete: true,
			mask: "0399-9999999",
			definitions: {
				'9': { validator: "[0-9]" }
			}
		},
		'mobilePK': {
			autoUnmask: false,
			clearMaskOnLostFocus: true,
			clearIncomplete: true,
			mask: "0399-9999999",
			definitions: {
				'9': { validator: "[0-9]" }
			}
		},
		'mobileInternational': {
			autoUnmask: false,
			clearMaskOnLostFocus: true,
			clearIncomplete: true,
			mask: "001999999999[9][9][9][9][9][9][9][9][9][9]",
			definitions: {
				'1': { validator: "[1-9]" },
				'9': { validator: "[0-9]" }
			}
		},
		'phoneInternational': {
			autoUnmask: false,
			clearMaskOnLostFocus: true,
			clearIncomplete: true,
			mask: "001999999999[9][9][9][9][9][9][9][9][9][9]",
			definitions: {
				'1': { validator: "[1-9]" },
				'9': { validator: "[0-9]" }
			}
		},
		'phonePK': {
			autoUnmask: false,
			clearMaskOnLostFocus: true,
			clearIncomplete: true,
			mask: "0199999999[9][9][9][9][9][9][9][9][9][9]",
			definitions: {
				'1': { validator: "[1-9]" },
				'9': { validator: "[0-9]" }
			}
		},
		'marks': {
			autoUnmask: false,
			clearMaskOnLostFocus: true,
			clearIncomplete: true,
			alias: "decimal",
			digits: 3,
			digitsOptional: true,
			min: 0,
			max: 9999.999,
			rightAlign: false
		},
		'cgpa4': {
			autoUnmask: false,
			clearMaskOnLostFocus: true,
			clearIncomplete: true,
			alias: "decimal",
			digits: 2,
			digitsOptional: true,
			min: 0,
			max: 4.0,
			rightAlign: false
		},
		'cgpa5': {
			autoUnmask: false,
			clearMaskOnLostFocus: true,
			clearIncomplete: true,
			alias: "decimal",
			digits: 2,
			digitsOptional: true,
			min: 0,
			max: 5.0,
			rightAlign: false
		},
		'byte': {
			autoUnmask: false,
			clearMaskOnLostFocus: true,
			clearIncomplete: true,
			alias: "decimal",
			digits: 0,
			digitsOptional: true,
			min: 0,
			max: 255,
			rightAlign: false
		},
		'short': {
			autoUnmask: false,
			clearMaskOnLostFocus: true,
			clearIncomplete: true,
			alias: "decimal",
			digits: 0,
			digitsOptional: true,
			min: -32768,
			max: 32767,
			rightAlign: false
		},
		'ushort': {
			autoUnmask: false,
			clearMaskOnLostFocus: true,
			clearIncomplete: true,
			alias: "decimal",
			digits: 0,
			digitsOptional: true,
			min: 0,
			max: 65535,
			rightAlign: false
		},
		'int': {
			autoUnmask: false,
			clearMaskOnLostFocus: true,
			clearIncomplete: true,
			alias: "decimal",
			digits: 0,
			digitsOptional: true,
			min: -2147483648,
			max: 2147483647,
			rightAlign: false
		},
		'ntncnic': {
			autoUnmask: false,
			clearMaskOnLostFocus: true,
			clearIncomplete: true,
			mask: "(9999999-9)|(99999-9999999-9)"
		}
	});
	$(":input").inputmask();
}

var alertModalID = "alertModal";
function initAlertModal() {
	var divAlertModal = $("<div class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"alertModalLabel\" aria-hidden=\"true\"></div>").appendTo($("body")).attr("id", alertModalID);
	var modalDialog = $("<div class=\"modal-dialog\"></div>").appendTo(divAlertModal);
	var modalContent = $("<div class=\"modal-content\"></div>").appendTo(modalDialog);
	var modalHeader = $("<div class=\"modal-header\"></div>").appendTo(modalContent);
	$("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>").appendTo(modalHeader);
	$("<h4 class=\"modal-title\" id=\"alertModalLabel\"></h4>").appendTo(modalHeader);
	var modalBody = $("<div class=\"modal-body\"></div>").appendTo(modalContent);
	var modalFooter = $("<div class=\"modal-footer\"></div>").appendTo(modalContent);
	$("<button type=\"button\" class=\"btn btn-primary\">Yes</button></div>").appendTo(modalFooter).attr("id", "yes");
	$("<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">No</button>").appendTo(modalFooter);
}

function disableSelection() {
	var noSelectCssClass = "noselect";
	var pageContainer = $("body>form>div#pageContainer");
	var header = $(">header#header", pageContainer).addClass(noSelectCssClass);
	//var containerFluid = $(">.container-fluid", header);
	//var navbarHeader = $(">.navbar-header", containerFluid);
	//var sideBarButton = $(">a#aSideBar", navbarHeader);
	//var collapseButton = $(">button.navbar-toggle", navbarHeader);
	//var accountsNavbar = $(">div#AccountsNavbar", containerFluid);
	var sideBar = $(">aside#sideBar", pageContainer).addClass(noSelectCssClass);
	var footer = $("footer#footer").addClass(noSelectCssClass);
}
var confirmModalResult = false;

function AspireConfirm(control, title, msg) {
	var alertModal = $("#" + alertModalID);
	var yesButton = $("button#yes", alertModal);
	if (!confirmModalResult) {
		alertModal.find(".modal-title").text(title);
		alertModal.find(".modal-body").text(msg);
		yesButton.off("click").click(function () {
			alertModal.modal("hide");
			confirmModalResult = true;
			if (control)
				control.click();
		});
		alertModal.modal({
			backdrop: "static", show: true
		});
		yesButton.focus();
		return false;
	}
	else {
		confirmModalResult = false;
		return true;
	}
}

function createAspireEmptyModal(sizeClass, title) {
	var modal = $("<div></div>").addClass("modal fade").attr({
		"tabindex": "-1", "role": "dialog", "aria-labelledby": "alertModalLabel", "aria-hidden": "true"
	});
	var modalDialog = $("<div></div>").addClass("modal-dialog").appendTo(modal);
	if (sizeClass !== undefined)
		modalDialog.addClass(sizeClass);
	var modalContent = $("<div></div>").addClass("modal-content").appendTo(modalDialog);
	var modalHeader = $("<div></div>").addClass("modal-header").appendTo(modalContent);
	var close = $(" <button></button>").addClass("close").attr({ "type": "button", "data-dismiss": "modal", "aria-label": "Close" }).appendTo(modalHeader)
		.append($("<span></span>").attr({ "aria-hidden": "true" }).html("&times;"));
	var modalTitle = $("<h4></h4>").addClass("modal-title").appendTo(modalHeader);
	if (title !== undefined)
		modalTitle.text(title);
	var modalBody = $("<div></div>").addClass("modal-body").appendTo(modalContent);
	var modalFooter = $("<div></div>").addClass("modal-footer").appendTo(modalContent);
	$("<button type=\"button\" class=\"btn btn-primary\">Ok</button>").appendTo(modalFooter);
	$("<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>").appendTo(modalFooter);
	return modal.appendTo($("body"));
}

function getModalBody(modal) {
	return $(modal).find("div.modal-body");
}

function getModalFooterPrimaryButton(modal) {
	return $(modal).find("div.modal-footer").find("button.btn.btn-primary");
}

function getModalFooterDefaultButton(modal) {
	return $(modal).find("div.modal-footer").find("button.btn.btn-default");
}

function searchStudent(btnSearch) {
	var btn = $(btnSearch);
	var modal = AspireModal.createFullModal("Search Student", "modal-lg", true, null, null, null, "Cancel", "btn btn-default");
	var selectedEnrollment = null;
	var tbEnrollment, tbRegistrationNo, tbName, tbFatherName, tbCNIC, ddlIntakeSemesterID, ddlProgramID, table;
	var sortDirection = "Ascending";
	var sortExpression = "Enrollment";
	var displayNoRecordFoundIfRequired = function () {
		var tBody = $("tbody", table);
		if (tBody.length === 0)
			tBody = $("<tbody></tbody>").appendTo(table);
		if ($("tr", tBody).length > 0)
			return;
		var tr = $("<tr></tr>").appendTo(tBody);
		$("<td>No record found.</td>").attr("colspan", $("thead th", table).length).appendTo(tr);
	};
	var search = function () {
		if (sortDirection === "Ascending")
			sortDirection = "Descending";
		else if (sortDirection === "Descending")
			sortDirection = "Ascending";
		var tBody = $("tbody", table);
		$("tr", tBody).remove();
		var params = {
			enrollment: tbEnrollment.val().trim().toUpperCase(),
			registrationNo: tbRegistrationNo.val().toNullableInt(),
			name: tbName.val().trim().toUpperCase(),
			fatherName: tbFatherName.val().trim().toUpperCase(),
			cnic: tbCNIC.val(),
			intakeSemesterID: ddlIntakeSemesterID.val().toNullableInt(),
			programID: ddlProgramID.val().toNullableInt(),
			sortExpression: sortExpression,
			sortDirection: sortDirection
		};
		console.log(params);
		$.ajax({
			type: "POST",
			url: window.aspireRootPath + "Sys/Common/WebService.asmx/SearchStudents",
			data: JSON.stringify(params),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			cache: false,
			success: function (response) {
				$(response.d).each(function (i, student) {
					var tr = $("<tr></tr>").appendTo(tBody);
					$("<td></td>").append($("<a/>").attr("href", "#").text(student.Enrollment).click(function () {
						selectedEnrollment = student.Enrollment;
						AspireModal.close(modal);
					})).appendTo(tr);
					$("<td></td>").text(student.RegistrationNo).appendTo(tr);
					$("<td></td>").text(student.Name).appendTo(tr);
					$("<td></td>").text(student.FatherName).appendTo(tr);
					$("<td></td>").text(student.IntakeSemester).appendTo(tr);
					$("<td></td>").text(student.ProgramAlias).appendTo(tr);
					$("<td></td>").text(student.StatusFullName).appendTo(tr);
				});
				displayNoRecordFoundIfRequired();
			},
			failure: function (msg) {
				console.error(msg);
			}
		});
	};
	var onShown = function () {
		var modalBody = AspireModal.getModalBody(modal);
		var row = $("<div></div>").addClass("row").appendTo(modalBody);
		tbEnrollment = $("<input />").attr({ "type": "text", "class": "form-control", maxlength: 15 }).css({ "text-transform": "uppercase" });
		var span = $("<label>Enrollment:</label>").addClass("control-label");
		$("<div></div>").addClass("form-group col-md-6").appendTo(row).append(span).append(tbEnrollment);

		tbRegistrationNo = $("<input />").attr({ "maxlength": 9, "type": "text", "class": "form-control" });
		span = $("<label>Registration No.:</label>").addClass("control-label");
		$("<div></div>").addClass("form-group col-md-6").appendTo(row).append(span).append(tbRegistrationNo);
		tbRegistrationNo.inputmask("integer", { rightAlign: false });

		row = $("<div></div>").addClass("row").appendTo(modalBody);
		tbName = $("<input />").attr({ "type": "text", "class": "form-control", maxlength: 100 }).css({ "text-transform": "uppercase" });
		span = $("<label>Name:</label>").addClass("control-label");
		$("<div></div>").addClass("form-group col-md-6").appendTo(row).append(span).append(tbName);

		tbFatherName = $("<input />").attr({ "type": "text", "class": "form-control", maxlength: 100 }).css({ "text-transform": "uppercase" });
		span = $("<label>Father Name:</label>").addClass("control-label");
		$("<div></div>").addClass("form-group col-md-6").appendTo(row).append(span).append(tbFatherName);

		row = $("<div></div>").addClass("row").appendTo(modalBody);
		tbCNIC = $("<input />").attr({ "type": "text", "class": "form-control" });
		span = $("<label>CNIC:</label>").addClass("control-label");
		$("<div></div>").addClass("form-group col-md-6").appendTo(row).append(span).append(tbCNIC);
		tbCNIC.inputmask({ alias: "cnic", "clearIncomplete": true });

		ddlIntakeSemesterID = $("<select />").attr({ "class": "form-control" });
		span = $("<label>Intake Semester:</label>").addClass("control-label");
		$("<div></div>").addClass("form-group col-md-6").appendTo(row).append(span).append(ddlIntakeSemesterID);

		ddlProgramID = $("<select />").attr({ "class": "form-control" });
		span = $("<label>Program:</label>").addClass("control-label");
		$("<div></div>").addClass("form-group col-md-6").appendTo(row).append(span).append(ddlProgramID);

		var btnFind = $("<button></button>").attr({ "class": "btn btn-primary btn-block" }).text("Search").click(search);
		span = $("<label></label>").addClass("control-label").html("&nbsp;");
		$("<div></div>").addClass("form-group col-md-3").appendTo(row).append(span).append(btnFind);

		var btnClear = $("<button></button>").attr({ "class": "btn btn-default btn-block" }).text("Clear").click(
			function () {
				tbEnrollment.val("");
				tbRegistrationNo.val("");
				tbName.val("");
				tbFatherName.val("");
				tbCNIC.val("");
				ddlIntakeSemesterID.val("");
				ddlProgramID.val("");
				$("tbody", table).remove();
				displayNoRecordFoundIfRequired();
			});
		span = $("<label></label>").addClass("control-label").html("&nbsp;");
		$("<div></div>").addClass("form-group col-md-3").appendTo(row).append(span).append(btnClear);

		$(tbEnrollment).add($(tbRegistrationNo)).add($(tbName)).add($(tbFatherName)).add($(tbCNIC)).keydown(function (event) {
			if ((event.keyCode ? event.keyCode : event.which) === 13)
				btnFind.click();
		});

		$.ajax({
			type: "POST",
			url: window.aspireRootPath + "Sys/Common/WebService.asmx/GetSemestersAndPrograms",
			data: JSON.stringify({}),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			cache: false,
			success: function (response) {
				$("<option></option>").text("All").attr("value", "").prop("selected", true).appendTo(ddlIntakeSemesterID);
				$(response.d.Semesters).each(function (i, s) {
					$("<option></option>").text(s.Semester).attr("value", s.SemesterID).appendTo(ddlIntakeSemesterID);
				});
				$("<option></option>").text("All").attr("value", "").prop("selected", true).appendTo(ddlProgramID);
				if (response.d.Programs !== null)
					$(response.d.Programs).each(function (i, s) {
						$("<option></option>").text(s.Text).attr("value", s.ID).appendTo(ddlProgramID);
					});
			},
			failure: function (msg) {
				console.error(msg);
			}
		});

		table = $("<table></table>").addClass("table-bordered table-striped table-hover table AspireGridView sortable").appendTo(modalBody);
		var tHead = $("<thead/>").appendTo(table);
		var tr = $("<tr></tr>").appendTo(tHead);

		var sort = function (sortExp, a) {
			sortExpression = sortExp;
			search();
			$("thead th", table).removeClass("Asc").removeClass("Des");
			console.log(a);
			if (sortDirection === "Ascending")
				$(a).closest("th").addClass("Asc");
			else if (sortDirection === "Descending")
				$(a).closest("th").addClass("Des");
		};
		$("<th></th>").addClass("Asc").appendTo(tr).append($("<a/>").attr("href", "#").text("Enrollment").click(function () { sort("Enrollment", this); }));
		$("<th></th>").appendTo(tr).append($("<a/>").attr("href", "#").text("Registration No.").click(function () { sort("RegistrationNo", this); }));
		$("<th></th>").appendTo(tr).append($("<a/>").attr("href", "#").text("Name").click(function () { sort("Name", this); }));
		$("<th></th>").appendTo(tr).append($("<a/>").attr("href", "#").text("Father Name").click(function () { sort("FatherName", this); }));
		$("<th></th>").appendTo(tr).append($("<a/>").attr("href", "#").text("Intake Semester").click(function () { sort("IntakeSemesterID", this); }));
		$("<th></th>").appendTo(tr).append($("<a/>").attr("href", "#").text("Program").click(function () { sort("ProgramAlias", this); }));
		$("<th></th>").appendTo(tr).append($("<a/>").attr("href", "#").text("Status").click(function () { sort("Status", this); }));
		displayNoRecordFoundIfRequired();
	};
	var onHidden = function () {
		if (selectedEnrollment !== null) {
			console.log($(".input.form-control", btn.closest(".input-group")));
			$("input.form-control", btn.closest(".input-group")).val(selectedEnrollment);
			btn.prev()[0].click();
		}
	};
	AspireModal.open(modal, true, true, true, onShown, onHidden, null, null);
}

jQuery.fn.extend({
	applySelect2: function () {
		return this.each(function () {
			$(this).select2({
				theme: "bootstrap",
				width: "style",
				dropdownParent: $(this).parent(),
				closeOnSelect: !$(this).prop("multiple")
			});
			if ($(this).hasClass("disabled"))
				$(this).prop("disabled", true);
		});
	}
});