﻿function TryToInt(str) {
	var exp = /^\s*[-\+]?\d+\s*$/;
	if (str === null || str === undefined || str.match(exp) === null)
		return null;
	var num = parseInt(str, 10);
	return isNaN(num) ? null : num;
}

String.prototype.tryToInt = function () { return TryToInt(this); };

function ToInt(num) {
	var i = TryToInt(num);
	if (i !== null)
		return i;
	if (num === null)
		num = "";
	throw "Not a valid Integer. str: " + num;
}

String.prototype.toInt = function () { return ToInt(this); };

function ToNullableInt(num) {
	if (num === null || num === undefined || num.trim().length === 0)
		return null;
	return ToInt(num);
}

String.prototype.toNullableInt = function () { return ToNullableInt(this); };

/**
* Decimal adjustment of a number.
*
* @param {String}  type  The type of adjustment.
* @param {Number}  value The number.
* @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
* @returns {Number} The adjusted value.
*/
function decimalAdjust(type, value, exp) {
	// If the exp is undefined or zero...
	if (typeof exp === "undefined" || +exp === 0)
		return Math[type](value);
	value = +value;
	exp = +exp;
	// If the value is not a number or the exp is not an integer...
	if (isNaN(value) || !(exp % 1 === 0))
		return NaN;
	// Shift
	value = value.toString().split("e");
	value = Math[type](+(value[0] + "e" + (value[1] ? +value[1] - exp : -exp)));
	// Shift back
	value = value.toString().split("e");
	return +(value[0] + "e" + (value[1] ? +value[1] + exp : exp));
}

// Decimal round
if (!Math.round10)
	Math.round10 = function (value, exp) {
		return decimalAdjust("round", value, exp);
	};
// Decimal floor
if (!Math.floor10)
	Math.floor10 = function (value, exp) {
		return decimalAdjust("floor", value, exp);
	};
// Decimal ceil
if (!Math.ceil10)
	Math.ceil10 = function (value, exp) {
		return decimalAdjust("ceil", value, exp);
	};

function TryToDouble(str) {
	if (str === null || str === undefined)
		return null;
	var exp = /^\s*[-\+]?[0-9]*\.?[0-9]+([eE][-\+]?[0-9]+)?\s*$/;//new RegExp("^\\s*([-\\+])?(\\d*)\\" + "." + "?(\\d*)\\s*$");
	var m = str.match(exp);
	if (m === null)
		return null;
	var num = parseFloat(str);
	return (isNaN(num) ? null : num);
}

String.prototype.tryToDouble = function () { return TryToDouble(this); };

function ToDouble(num) {
	var i = TryToDouble(num);
	if (i !== null)
		return i;
	if (num === null)
		num = "";
	throw "Not a valid Double. str: " + num;
}

String.prototype.toDouble = function () { return ToDouble(this); };

function ToNullableDouble(num) {
	if (num === null || num === undefined || num.trim().length === 0)
		return null;
	return ToDouble(num);
}

String.prototype.toNullableDouble = function () { return ToNullableDouble(this); };

function TryToDate(str, formats) {
	if (str === null || str === undefined || str.trim().length === 0)
		return null;
	if (formats === undefined || formats === null || formats.trim().length === 0)
		return Date.parseLocale(str);
	return Date.parseLocale(str, formats);
}

String.prototype.tryToDate = function (formats) { return TryToDate(this, formats); };

function ToDate(str, formats) {
	var date = TryToDate(str, formats);
	if (date !== null)
		return date;
	if (str === null)
		str = "";
	throw "Not a valid Date. str: " + str;
}

String.prototype.toDate = function (formats) { return ToDate(this, formats); };

function ToNullableDate(str, formats) {
	if (str === null || str === undefined || str.trim().length === 0)
		return null;
	return ToDate(str, formats);
}

String.prototype.toNullableDate = function (formats) { return ToNullableDate(this, formats); };

function ApplyHighlightCss(source, controlToValidateID, args) {
	if (args.IsValid)
		source.errormessage = null;
	source.innerHTML = source.errormessage;
	var highlightCssClass = source.getAttribute("data-HighlightCssClass".toLowerCase());
	if (highlightCssClass !== null && highlightCssClass.length > 0) {
		var controlToValidate = $("#" + controlToValidateID);
		var parent = controlToValidate.closest(".form-group");
		var control = parent.length > 0 ? parent : controlToValidate;
		if (args.IsValid)
			control.removeClass(highlightCssClass);
		else
			control.addClass(highlightCssClass);
	}
}

function ValidateInteger(source, args) {
	var allowNull = source.getAttribute("data-AllowNull".toLowerCase()).toLowerCase() === "true";
	var minValue = ToInt(source.getAttribute("data-MinValue".toLowerCase()));
	var maxValue = ToInt(source.getAttribute("data-MaxValue".toLowerCase()));
	var value = args.Value.trim();
	if (value === null || value.length === 0) {
		if (allowNull)
			args.IsValid = true;
		else {
			args.IsValid = false;
			source.errormessage = source.getAttribute("data-RequiredErrorMessage".toLowerCase());
		}
	}
	else {
		var i = TryToInt(value);
		if (i === null) {
			args.IsValid = false;
			source.errormessage = source.getAttribute("data-InvalidNumberErrorMessage".toLowerCase());
		}
		else if (minValue > i || maxValue < i) {
			args.IsValid = false;
			source.errormessage = source.getAttribute("data-InvalidRangeErrorMessage".toLowerCase());
		}
		else
			args.IsValid = true;
	}
	ApplyHighlightCss(source, source.controltovalidate, args);
}

function ValidateDouble(source, args) {
	var allowNull = source.getAttribute("data-AllowNull".toLowerCase()).toLowerCase() === "true";
	var minValue = ToDouble(source.getAttribute("data-MinValue".toLowerCase()));
	var maxValue = ToDouble(source.getAttribute("data-MaxValue".toLowerCase()));
	var value = args.Value.trim();
	if (value === null || value.length === 0) {
		if (allowNull)
			args.IsValid = true;
		else {
			args.IsValid = false;
			source.errormessage = source.getAttribute("data-RequiredErrorMessage".toLowerCase());
		}
	}
	else {
		var i = TryToDouble(value);
		if (i === null) {
			args.IsValid = false;
			source.errormessage = source.getAttribute("data-InvalidNumberErrorMessage".toLowerCase());
		}
		else if (minValue > i || maxValue < i) {
			args.IsValid = false;
			source.errormessage = source.getAttribute("data-InvalidRangeErrorMessage".toLowerCase());
		}
		else
			args.IsValid = true;
	}
	ApplyHighlightCss(source, source.controltovalidate, args);
}

function ValidateString(source, args) {
	var allowNull = source.getAttribute("data-AllowNull".toLowerCase()).toLowerCase() === "true";
	var validationExpression = source.getAttribute("data-ValidationExpression".toLowerCase());
	var value = args.Value;
	if (value === null || value.trim().length === 0) {
		if (allowNull)
			args.IsValid = true;
		else {
			args.IsValid = false;
			source.errormessage = source.getAttribute("data-RequiredErrorMessage".toLowerCase());
		}
	}
	else {
		if (validationExpression !== null && validationExpression.length > 0) {
			var pattern = new RegExp(validationExpression);
			if (pattern.test(value))
				args.IsValid = true;
			else {
				args.IsValid = false;
				source.errormessage = source.getAttribute("data-InvalidDataErrorMessage".toLowerCase());
			}
		}
		else
			args.IsValid = true;
	}

	ApplyHighlightCss(source, source.controltovalidate, args);
}

function ValidateRequiredField(source, args) {
	var controlToValidate = $("#" + source.getAttribute("data-val-controltovalidate"));
	var controlType = controlToValidate.prop("type");
	var value;
	if (controlType !== undefined)
		switch (controlType) {
			case "text":
			case "select-one":
			case "textarea":
			case "file":
			case "password":
				value = controlToValidate.val();
				break;
			case "select-multiple":
				if ($("option:selected", controlToValidate).length > 0)
					value = "selected found";
				break;
			case "checkbox":
				if ($(controlToValidate).is(":checked"))
					value = "1";
				break;
			default:
				throw controlType + " is not supported or implemented.";
		}
	else {
		if (controlToValidate.hasClass("AspireCheckBoxList")) {
			if ($("input:checkbox:checked", controlToValidate).length > 0)
				value = "checked found";
		}
		else if (controlToValidate.hasClass("AspireRadioButtonList")) {
			if ($("input:radio:checked", controlToValidate).length > 0)
				value = "checked found";
		}
		else
			throw "RequiredFieldValidator is not supported or implemented for the control with " + controlToValidate;
	}
	if (value === null || value === undefined || value.trim().length === 0) {
		args.IsValid = false;
		source.errormessage = source.getAttribute("data-RequiredErrorMessage".toLowerCase());
	}
	else
		args.IsValid = true;
	ApplyHighlightCss(source, source.controltovalidate, args);
}

function ValidateCompare(source, args) {
	var value = args.Value.trim();
	var dataValValidateEmptyText = source.getAttribute("data-val-ValidateEmptyText".toLowerCase());
	var validateEmptyText = dataValValidateEmptyText !== null && dataValValidateEmptyText.toLowerCase() === "true";
	if (!validateEmptyText && (value === null || value.length <= 0)) {
		args.IsValid = true;
		return;
	}
	var controlToCompareID = source.getAttribute("data-ControlToCompareID".toLowerCase());
	var controlToCompare = $("#" + controlToCompareID);
	var controlToCompareValue = controlToCompare.val().trim();
	if (!validateEmptyText && (controlToCompareValue === null || controlToCompareValue.length <= 0)) {
		args.IsValid = true;
		return;
	}

	var operator = source.getAttribute("data-Operator".toLowerCase());
	var type = source.getAttribute("data-Type".toLowerCase());
	var caseSensitiveComparison = source.getAttribute("data-CaseSensitiveComparison".toLowerCase()).toLowerCase() === "true";
	var comparisonErrorMessage = source.getAttribute("data-ComparisonErrorMessage".toLowerCase());
	var dateTimeFormat = source.getAttribute("data-DateTimeFormat".toLowerCase());

	function validate() {
		var stringValue = value;
		var integerValue = TryToInt(stringValue);
		var doubleValue = TryToDouble(stringValue);
		var dateTimeValue = TryToDate(stringValue, dateTimeFormat);
		var compareStringValue = controlToCompareValue;
		var compareIntegerValue = TryToInt(compareStringValue);
		var compareDoubleValue = TryToDouble(compareStringValue);
		var compareDateTimeValue = TryToDate(compareStringValue, dateTimeFormat);
		switch (operator) {
			case "Equal":
				switch (type) {
					case "String":
						return caseSensitiveComparison
							? stringValue.localeCompare(compareStringValue) === 0
							: stringValue.toLocaleLowerCase().localeCompare(compareStringValue.toLocaleLowerCase()) === 0;
					case "Integer":
						return integerValue !== null && integerValue === compareIntegerValue;
					case "Double":
						return doubleValue !== null && doubleValue === compareDoubleValue;
					case "Date":
						return dateTimeValue !== null && dateTimeValue === compareDateTimeValue;
					case "Currency":
						throw type + " NotSupported";
					default:
						throw type + " ArgumentOutOfRangeException";
				}
			case "NotEqual":
				switch (type) {
					case "String":
						return caseSensitiveComparison
							? stringValue.localeCompare(compareStringValue) !== 0
							: stringValue.toLocaleLowerCase().localeCompare(compareStringValue.toLocaleLowerCase()) !== 0;
					case "Integer":
						return integerValue !== null && integerValue !== compareIntegerValue;
					case "Double":
						return doubleValue !== null && doubleValue !== compareDoubleValue;
					case "Date":
						return dateTimeValue !== null && dateTimeValue !== compareDateTimeValue;
					case "Currency":
						throw type + " NotSupported";
					default:
						throw type + " ArgumentOutOfRangeException";
				}
			case "GreaterThan":
				switch (type) {
					case "String":
						return caseSensitiveComparison
							? stringValue.localeCompare(compareStringValue) > 0
							: stringValue.toLocaleLowerCase().localeCompare(compareStringValue.toLocaleLowerCase()) > 0;
					case "Integer":
						return integerValue !== null && integerValue > compareIntegerValue;
					case "Double":
						return doubleValue !== null && doubleValue > compareDoubleValue;
					case "Date":
						return dateTimeValue !== null && dateTimeValue > compareDateTimeValue;
					case "Currency":
						throw type + " NotSupported";
					default:
						throw type + " ArgumentOutOfRangeException";
				}
			case "GreaterThanEqual":
				switch (type) {
					case "String":
						return caseSensitiveComparison
							? stringValue.localeCompare(compareStringValue) >= 0
							: stringValue.toLocaleLowerCase().localeCompare(compareStringValue.toLocaleLowerCase()) >= 0;
					case "Integer":
						return integerValue !== null && integerValue >= compareIntegerValue;
					case "Double":
						return doubleValue !== null && doubleValue >= compareDoubleValue;
					case "Date":
						return dateTimeValue !== null && dateTimeValue >= compareDateTimeValue;
					case "Currency":
						throw type + " NotSupported";
					default:
						throw type + " ArgumentOutOfRangeException";
				}
			case "LessThan":
				switch (type) {
					case "String":
						return caseSensitiveComparison
							? stringValue.localeCompare(compareStringValue) < 0
							: stringValue.toLocaleLowerCase().localeCompare(compareStringValue.toLocaleLowerCase()) < 0;
					case "Integer":
						return integerValue !== null && integerValue < compareIntegerValue;
					case "Double":
						return doubleValue !== null && doubleValue < compareDoubleValue;
					case "Date":
						return dateTimeValue !== null && dateTimeValue < compareDateTimeValue;
					case "Currency":
						throw type + " NotSupported";
					default:
						throw type + " ArgumentOutOfRangeException";
				}
			case "LessThanEqual":
				switch (type) {
					case "String":
						return caseSensitiveComparison
							? stringValue.localeCompare(compareStringValue) <= 0
							: stringValue.toLocaleLowerCase().localeCompare(compareStringValue.toLocaleLowerCase()) <= 0;
					case "Integer":
						return integerValue !== null && integerValue <= compareIntegerValue;
					case "Double":
						return doubleValue !== null && doubleValue <= compareDoubleValue;
					case "Date":
						return dateTimeValue !== null && dateTimeValue <= compareDateTimeValue;
					case "Currency":
						throw type + " NotSupported";
					default:
						throw type + " ArgumentOutOfRangeException";
				}
			case "DataTypeCheck":
				switch (type) {
					case "String":
						return true;
					case "Integer":
						return integerValue !== null;
					case "Double":
						return doubleValue !== null;
					case "Date":
						return dateTimeValue !== null;
					case "Currency":
						throw type + " NotSupported";
					default:
						throw type + " ArgumentOutOfRangeException";
				}
			default:
				throw type + " ArgumentOutOfRangeException";
		}
	}

	args.IsValid = validate();
	if (!args.IsValid)
		source.errormessage = comparisonErrorMessage;
	ApplyHighlightCss(source, source.controltovalidate, args);
}

function ValidateDateTime(source, args) {
	var allowNull = source.getAttribute("data-AllowNull".toLowerCase()).toLowerCase() === "true";
	var format = source.getAttribute("data-Format".toLowerCase());
	var minDate = source.getAttribute("data-MinDate".toLowerCase()).toNullableDate("f");
	var maxDate = source.getAttribute("data-MaxDate".toLowerCase()).toNullableDate("f");
	var value = args.Value;
	if (value === null || value.trim().length === 0) {
		if (allowNull)
			args.IsValid = true;
		else {
			args.IsValid = false;
			source.errormessage = source.getAttribute("data-RequiredErrorMessage".toLowerCase());
		}
	}
	else {
		var date = TryToDate(value, format);
		if (date === null) {
			args.IsValid = false;
			source.errormessage = source.getAttribute("data-InvalidDataErrorMessage".toLowerCase());
		} else {
			var invalidRangeMessage = source.getAttribute("data-InvalidRangeErrorMessage".toLowerCase());
			if (minDate !== null && date < minDate) {
				args.IsValid = false;
				source.errormessage = invalidRangeMessage;
			}
			else if (maxDate !== null && maxDate < date) {
				args.IsValid = false;
				source.errormessage = invalidRangeMessage;
			}
			else
				args.IsValid = true;
		}
	}
	ApplyHighlightCss(source, source.controltovalidate, args);
}

function ValidatePasswordPolicy(source, args) {
	var minimumPasswordLength = ToInt(source.getAttribute("data-MinimumPasswordLength".toLowerCase()));
	var minimumNoOfLetters = ToInt(source.getAttribute("data-MinimumNoOfLetters".toLowerCase()));
	var minimumNoOfUpperCaseChars = ToInt(source.getAttribute("data-MinimumNoOfUpperCaseChars".toLowerCase()));
	var minimumNoOfLowerCaseChars = ToInt(source.getAttribute("data-MinimumNoOfLowerCaseChars".toLowerCase()));
	var minimumNoOfNumbers = ToInt(source.getAttribute("data-MinimumNoOfNumbers".toLowerCase()));
	var minimumNoOfSymbols = ToInt(source.getAttribute("data-MinimumNoOfSymbols".toLowerCase()));
	var value = args.Value;

	function checkMinimumPasswordLength() {
		if (minimumPasswordLength < 0)
			return true;
		return value.length >= minimumPasswordLength;
	}

	var upperCaseCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	var lowerCaseCharacters = "abcdefghijklmnopqrstuvwxyz";
	var letters = upperCaseCharacters + lowerCaseCharacters;
	var digits = "0123456789";
	var symbols = "`~!@#$%^&*()_+-={}|\:\";'<>?,./";

	function checkMinimumNoOfLetters() {
		if (minimumNoOfLetters < 0)
			return true;
		var len = value.length;
		var found = 0;
		for (var i = 0; i < len; i++)
			if (letters.indexOf(value[i]) >= 0)
				found++;
		return found >= minimumNoOfLetters;
	}

	function checkMinimumNoOfUpperCaseChars() {
		if (minimumNoOfUpperCaseChars < 0)
			return true;
		var len = value.length;
		var found = 0;
		for (var i = 0; i < len; i++)
			if (upperCaseCharacters.indexOf(value[i]) >= 0)
				found++;
		return found >= minimumNoOfUpperCaseChars;
	}

	function checkMinimumNoOfLowerCaseChars() {
		if (minimumNoOfLowerCaseChars < 0)
			return true;
		var len = value.length;
		var found = 0;
		for (var i = 0; i < len; i++)
			if (lowerCaseCharacters.indexOf(value[i]) >= 0)
				found++;
		return found >= minimumNoOfLowerCaseChars;
	}

	function checkMinimumNoOfNumbers() {
		if (minimumNoOfNumbers < 0)
			return true;
		var len = value.length;
		var found = 0;
		for (var i = 0; i < len; i++)
			if (digits.indexOf(value[i]) >= 0)
				found++;
		return found >= minimumNoOfNumbers;
	}

	function checkMinimumNoOfSymbols() {
		if (minimumNoOfSymbols < 0)
			return true;
		var len = value.length;
		var found = 0;
		for (var i = 0; i < len; i++)
			if (symbols.indexOf(value[i]) >= 0)
				found++;
		return found >= minimumNoOfSymbols;
	}

	args.IsValid = checkMinimumPasswordLength() && checkMinimumNoOfLetters() && checkMinimumNoOfUpperCaseChars() && checkMinimumNoOfLowerCaseChars() && checkMinimumNoOfNumbers() && checkMinimumNoOfSymbols();
	if (!args.IsValid)
		source.errormessage = source.getAttribute("data-val-ErrorMessage".toLowerCase());
	ApplyHighlightCss(source, source.controltovalidate, args);
}

function ValidateRecaptcha(source, args) {
	var googleResponse = $('#g-recaptcha-response').val();
	if (args !== undefined)
		args.IsValid = googleResponse.length > 0;
	else {
		if (googleResponse.length > 0)
			$(".RecaptchaValidator").hide();
	}
}

function TrimAndTransformIfRequired(input) {
	var value = $(input).val();
	var trimText = $(input).attr("data-TrimText".toLowerCase()).toLowerCase() === "true";
	if (trimText)
		value = value.trim();
	var textTransform = $(input).css("text-transform");
	if (textTransform === "uppercase")
		value = value.toUpperCase();
	else if (textTransform === "lowercase")
		value = value.toLowerCase();
	if ($(input).val() !== value) {
		$(input).val(value);
		$(input).change();
	}
}

function TrimTextArea(ta, maxLength) {
	if (ta.value.length > maxLength)
		ta.value = ta.value.substr(0, maxLength);
};

function InitDateTimePicker() {
	$(".input-group.date.DatePickerTextbox").each(function (i, v) {
		var displayMode = $(v).attr("data-DisplayMode".toLowerCase());
		var userManualEntryAllowed = $(v).attr("data-UserManualEntryAllowed".toLowerCase()).toLowerCase() === "true";
		var horizontalPosition = $(v).attr("data-HorizontalPosition".toLowerCase()).toLowerCase();
		var verticalPosition = $(v).attr("data-VerticalPosition".toLowerCase()).toLowerCase();
		var showClear = $(v).attr("data-ShowClear".toLowerCase()).toLowerCase() === "true";
		var showClose = $(v).attr("data-ShowClose".toLowerCase()).toLowerCase() === "true";
		var sideBySide = $(v).attr("data-SideBySide".toLowerCase()).toLowerCase() === "true";
		var dateFormat;
		var viewMode;
		var showTodayButton;
		switch (displayMode) {
			case "LongTime":
				dateFormat = "hh:mm:ss A";
				viewMode = "days";
				showTodayButton = false;
				break;
			case "ShortDate":
				dateFormat = "DD/MM/YYYY";
				viewMode = "days";
				showTodayButton = true;
				break;
			case "ShortTime":
				dateFormat = "hh:mm a";
				viewMode = "days";
				showTodayButton = false;
				break;
			case "ShortDateTime":
				dateFormat = "DD/MM/YYYY hh:mm a";
				viewMode = "days";
				showTodayButton = true;
				break;
			case "LongDateLongTime":
			case "LongDateShortTime":
			case "LongDate":
			default:
				throw "InitDateTimePicker: Not supported displayMode: " + displayMode;
		}
		var readonly = $("input", $(v)).prop("readonly");
		if (!userManualEntryAllowed)
			$("input", $(v)).addClass("whiteBg").prop("readonly", true);
		var ignoreReadonly = !readonly;
		$(v).datetimepicker({
			useStrict: false,
			showTodayButton: showTodayButton,
			allowInputToggle: false,
			useCurrent: false,
			ignoreReadonly: ignoreReadonly,
			viewMode: viewMode,
			format: dateFormat,
			showClear: showClear,
			showClose: showClose,
			sideBySide: sideBySide,
			widgetPositioning: {
				horizontal: horizontalPosition,
				vertical: verticalPosition
			}
		});

		var textBox = $("input:text", $(v));
		var textBoxID = textBox.attr("id");
		var validators = [];
		for (i = 0; i < Page_Validators.length; i++)
			if (Page_Validators[i].controltovalidate === textBoxID)
				validators.push(Page_Validators[i]);
		textBox.on("blur", function () {
			$(validators).each(function (ii, vv) {
				ValidatorValidate(vv);
			});
		});
	});
}

function InitAspirePasswordStrengthMeter(progressBarID) {
	var progressBarContainer = $("#" + progressBarID);
	var progressBar = $(".progress-bar", progressBarContainer);
	var textBoxID = progressBarContainer.attr("data-TextBoxID".toLowerCase());
	var ratingLabels = progressBarContainer.attr("data-RatingLabels".toLowerCase());
	$(progressBar).zxcvbnProgressBar({
		passwordInput: "#" + textBoxID,
		ratings: ratingLabels.split(",")
	});
	$("#" + textBoxID).on("keyup", function () {
		if ($(this).val().length === 0)
			progressBarContainer.hide("slow");
		else
			progressBarContainer.show("slow");
	});
}

function DecorateAspireCheckboxes() {
	function wrapInputInsideLabel(label, input, labelClass) {
		if ($(label).length === 0)
			label = $("<label></label>").insertAfter($(input));
		$(label).css({ "margin-bottom": 0 }).addClass(labelClass).prepend($(input));
		$(input).after(" ");
		return label;
	}

	function wrapInputInsideLabelAndThenDiv(label, input, className) {
		var parent = $(input).parent();
		var div = $("<div></div>").addClass(className).appendTo(parent);
		return div.after(" ").append(wrapInputInsideLabel(label, input));
	}

	var propApplied = "applied";
	$("span.AspireCheckBox").each(function (i, span) {
		if (!$(span).prop(propApplied)) {
			var cb = $("input:checkbox", $(span));
			var label = cb.next("label");
			// ReSharper disable UnknownCssClass
			var inlineClass = $(span).hasClass("Inline") ? "checkbox-inline" : null;
			$(span).removeClass("Inline");
			// ReSharper restore UnknownCssClass
			wrapInputInsideLabel(label, cb, inlineClass);
			$(span).prop(propApplied, true);
		}
	});

	$("span.AspireRadioButton").each(function (i, span) {
		if (!$(span).prop(propApplied)) {
			var cb = $("input:radio", $(span));
			var label = cb.next("label");
			// ReSharper disable UnknownCssClass
			var inlineClass = $(span).hasClass("Inline") ? "radio-inline" : null;
			$(span).removeClass("Inline");
			// ReSharper restore UnknownCssClass
			wrapInputInsideLabel(label, cb, inlineClass);
			$(span).prop(propApplied, true);
		}
	});

	function applyToList(selector, class1, class2) {
		$(selector).each(function (i, list) {
			if (!$(list).prop(propApplied)) {
				var isVertical = $("br", list).length > 0;
				if (isVertical) {
					$("br", $(list)).remove();
					$("input", $(list)).each(function (index, input) {
						var cb = $(input);
						var label = cb.next();
						if (!label.is("label"))
							label = null;
						wrapInputInsideLabelAndThenDiv(label, input, class1);
					});
				}
				else {
					$("input", $(list)).each(function (index, input) {
						var cb = $(input);
						var label = cb.next();
						if (!label.is("label"))
							label = null;
						wrapInputInsideLabel(label, input).addClass(class2);
					});
				}
				if ($(list).hasClass("aspNetDisabled")) {
					$(".aspNetDisabled", $(list)).each(function (ii, spanDisabled) {
						$(">*", $(spanDisabled)).insertBefore($(spanDisabled));
					}).remove();
				}
				$(list).prop(propApplied, true);
			}
		});
	}
	applyToList("span.AspireRadioButtonList", "radio", "radio-inline");
	applyToList("span.AspireCheckBoxList", "checkbox", "checkbox-inline");
}

function ButtonClick(btn) {
	var log = function (msg) { if (window.Debugging) console.log(btn.id + ": " + msg); };
	var enabled = btn.getAttribute("data-enabled".toLowerCase()).toLowerCase() === "true";
	if (!enabled) {
		log("Button is not enabled.");
		return false;
	}
	if (btn.DisabledAfterClick) {
		log("Button has been disabled after click.");
		return false;
	}
	log("Clicked");
	var confirmMessageTitle = btn.getAttribute("data-ConfirmMessageTitle".toLowerCase());
	var confirmMessage = btn.getAttribute("data-ConfirmMessage".toLowerCase());
	var disableOnClick = btn.getAttribute("data-DisableOnClick".toLowerCase()).toLowerCase() === "true";
	var onClientClick = btn.getAttribute("data-OnClientClick".toLowerCase());
	var causesValidation = btn.getAttribute("data-CausesValidation".toLowerCase()).toLowerCase() === "true";
	var validationGroup = btn.getAttribute("data-ValidationGroup".toLowerCase());

	if (confirmMessageTitle.length === 0) confirmMessageTitle = null;
	if (confirmMessage.length === 0) confirmMessage = null;
	if (onClientClick.length === 0) onClientClick = null;

	if (causesValidation) {
		if (!window.Page_ClientValidate(validationGroup)) {
			log("Page validation failed. ValidationGroup: " + validationGroup);
			return false;
		}
		else
			window.Page_ValidationActive = false;
	}
	else
		window.Page_ValidationActive = false;

	if (btn.ConfirmationDialogDisplayed === null) {
		if (onClientClick !== undefined && onClientClick !== null) {
			var onClientClickResult = (new Function(onClientClick))();
			log("onClientClickResult: " + onClientClickResult);
			if (onClientClickResult !== true && onClientClickResult !== false)
				console.warn("OnClientClick property must return true or false.");
			if (!onClientClickResult)
				return false;
		}
	}

	if (confirmMessage !== null) {
		btn.ConfirmationDialogDisplayed = true;
		var confirmationResult;
		if (typeof (window.AspireModal) !== "undefined")
			confirmationResult = window.AspireModal.confirm(btn, confirmMessageTitle, confirmMessage);
		else
			throw "AspireModal is not available.";

		log("Confirmation displayed.");
		if (confirmationResult === false)
			return false;
	}
	log("returning true");
	btn.ConfirmationDialogDisplayed = null;
	if (disableOnClick)
		btn.DisabledAfterClick = true;
	return true;
}

$(function () {
	var callWhenDocumentReady = function () {
		InitDateTimePicker();
		DecorateAspireCheckboxes();
	};
	callWhenDocumentReady();
	Sys.Application.add_load(callWhenDocumentReady);
});

function applyModal(clientID, show, partialPostback) {
	if (!partialPostback) {
		var func = function () {
			$("#" + clientID).modal({ backdrop: "static", keyboard: false, show: show });
			Sys.Application.remove_load(func);
		};
		Sys.Application.add_load(func);
	} else {
		var funcPartialPostback = function () {
			$("#" + clientID).modal({ backdrop: "static", keyboard: false, show: show });
			Sys.WebForms.PageRequestManager.getInstance().remove_endRequest(funcPartialPostback);
		};
		Sys.WebForms.PageRequestManager.getInstance().add_endRequest(funcPartialPostback);
	}
}

function initFileUpload(aspireFileUploadClientID, instance) {
	var fu = $("#" + aspireFileUploadClientID + "_fu");
	if (fu[0].FileUploadInitialized)
		return;
	var btn = $("#" + aspireFileUploadClientID + "_btn");
	var hfid = $(fu).attr("data-hfid");
	var hf = $("#" + hfid);
	//if (window.Debugging && window.IsLocal)
	//	hf.attr("type", "text").css({ width: "100%" });
	var allowedFileTypes = $(fu).attr("data-AllowedFileTypes".toLowerCase()).toLowerCase().split(",");
	var maxNumberOfFiles = $(fu).attr("data-MaxNumberOfFiles").toInt();
	var maxFileSize = $(fu).attr("data-MaxFileSize").toInt();
	var getUrl = function () { return window.aspireRootPath + "/HttpHandlers/FileUpload.ashx"; };
	var div = $("#" + aspireFileUploadClientID + "_div").show();
	var ui = {
		filesInfo: function () {
			var info = [];
			$("div.File.Uploaded", $(div)).not(".Error").each(function (i, divFile) {
				var fileInfo = $(divFile).data("FileInfo");
				info.push({ Instance: instance, FileID: fileInfo.FileID, FileType: fileInfo.FileType, FileName: fileInfo.FileName });
			});
			if (info.length > maxNumberOfFiles)
				throw "More than " + maxNumberOfFiles + " are uploaded.";
			return info;
		},
		updateValue: function () {
			var btnEnabled;
			if ($("div.InProgress", $(div)).length > 0) {
				$(hf).val(null);
				btnEnabled = true;
			}
			else {
				var info = ui.filesInfo();
				$(hf).val(null).val(JSON.stringify(info));
				btnEnabled = info.length >= maxNumberOfFiles;
			}
			$(btn).prop("disabled", btnEnabled);
		},
		getFileExtension: function (fileName) {
			if (fileName !== null && fileName.indexOf(".") > 0) {
				var fileExtension = fileName.split(".").pop().toLowerCase();
				if (fileExtension || fileExtension.length > 0)
					return "." + fileExtension.toLowerCase();
			}
			return null;
		},
		isFileTypeAllowed: function (fileExtension) {
			for (var i = 0; i < allowedFileTypes.length; i++)
				if (allowedFileTypes[i] === fileExtension)
					return true;
			return false;
		},
		getDeleteButton: function () {
			return $("<a/>").addClass("text-danger").attr({ title: "Delete" }).css("cursor", "pointer")
				.append($("<i/>").addClass("glyphicon glyphicon-remove").css({ "margin-right": "5px" }));
		},
		getProgressBar: function () {
			return $("<div/>").addClass("progress").css("margin-bottom", "0").append($("<div/>").addClass("progress-bar progress-bar-striped active")
				.attr({ "role": "progressbar", "aria-valuenow": "0", "aria-valuemin": "0", "aria-valuemax": "100" }));
		},
		error: function (divFile, actionClass, errorMessage) {
			$(divFile).addClass("Error text-danger").addClass(actionClass).removeClass("InProgress");
			$(".progress", $(divFile)).hide();
			if (errorMessage === null || errorMessage.length === 0)
				throw "errorMessage is not provided.";
			$("<span/>").text(errorMessage).appendTo($(divFile).append(" "));
			ui.updateValue();
		},
		progress: function (divFile, actionClass, percentage) {
			$(divFile).addClass("InProgress").addClass(actionClass);
			if (percentage === null)
				throw "percentage is not provided.";
			$(".progress", $(divFile)).show();
			$(".progress-bar", $(divFile)).addClass("progress-bar-info").css({ width: percentage + "%" });
			ui.updateValue();
		},
		success: function (divFile, actionClass, fileInfo) {
			$(divFile).addClass("text-success").addClass(actionClass).removeClass("InProgress");
			$(".progress", $(divFile)).hide();
			$(divFile).data("FileInfo", fileInfo);
			ui.updateValue();
		},
		getMaxNumberOfFilesErrorMessage: function () {
			if (maxNumberOfFiles === 1)
				return "Maximum one file can be uploaded.";
			else if (maxNumberOfFiles > 1)
				return "Maximum " + maxNumberOfFiles + " files can be uploaded.";
			else
				throw "Invalid maxNumberOfFiles: " + maxNumberOfFiles;
		},
		formatFileSize: function (bytes, decimalPoint) {
			if (bytes === 0)
				return "0 Bytes";
			var kilo = 1024;
			var dm = decimalPoint || 2;
			var sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
			var i = Math.floor(Math.log(bytes) / Math.log(kilo));
			return parseFloat((bytes / Math.pow(kilo, i)).toFixed(dm)) + " " + sizes[i];
		},
		getMaxFileSizeErrorMessage: function () {
			return "File size exceeds maximum limit " + ui.formatFileSize(maxFileSize, 2) + ".";
		},
		getFileTypeNotAllowedErrorMessage: function () {
			if (allowedFileTypes.length === 0)
				throw "allowedFileTypes is empty.";
			var fileTypes = "";
			for (var i = 0; i < allowedFileTypes.length; i++)
				fileTypes = fileTypes + ", " + allowedFileTypes[i];
			fileTypes = fileTypes.substring(2);
			return "Only " + fileTypes + " files are allowed.";
		},
		getFormData: function (action, fileID, file) {
			var formData = new FormData();
			formData.append("Action", action);
			formData.append("Instance", instance);
			if (fileID !== null)
				formData.append("FileID", fileID);
			if (file !== null)
				formData.append(0, file);
			return formData;
		},
		getXMLHttpRequest: function (divFile, progressMessage) {
			return function () {
				var xmlHttpRequest = new XMLHttpRequest();
				xmlHttpRequest.upload.addEventListener("progress",
					function (evt) { ui.progress($(divFile), progressMessage, evt.lengthComputable ? evt.loaded / evt.total * 100 : 100); }, false);
				return xmlHttpRequest;
			};
		},
		ajaxError: function (jqXHR, error, errorThrown) {
			console.error("jqXHR: " + jqXHR);
			console.error("error: " + error);
			console.error("errorThrown: " + errorThrown);
		},
		deleteFile: function (divFile) {
			if ($(divFile).hasClass("Error") || $(divFile).hasClass("ReadyToUpload")) {
				ui.progress($(divFile), "Deleting", 100);
				$(divFile).remove();
				ui.updateValue();
			}
			else if ($(divFile).hasClass("Uploaded")) {
				$.ajax({
					type: "POST",
					url: getUrl(),
					data: ui.getFormData("DELETE", $(divFile).data("FileInfo").FileID, null),
					cache: false,
					dataType: "json",
					processData: false, // Don't process the files
					contentType: false, // Set content type to false as jQuery will tell the server its a query string request
					beforeSend: function () { ui.progress($(divFile), "Deleting...", 0); },
					xhr: ui.getXMLHttpRequest(divFile, "Deleting..."),
					success: function (data) {
						$(div).removeClass("Deleting");
						if (data.success) {
							ui.progress($(divFile), "Deleted", 100);
							$(divFile).remove();
							ui.updateValue();
						} else
							ui.error($(divFile), "NotDeleted", data.result);
					},
					error: function (jqXHR, error, errorThrown) {
						ui.ajaxError(jqXHR, error, errorThrown);
						$(div).removeClass("Deleting");
						ui.error($(divFile), "NotDeleted", "Error occurred while deleting.");
					}
				});
			}
			else
				console.error("Delete not handled for other classes. " + $(divFile).attr("class"));
		},
		addFileControls: function (fileInfo) {
			var divFile = $("<div/>").addClass("File").data("FileInfo", fileInfo).css({ "margin-bottom": "5px" }).appendTo(div)
				.append(ui.getDeleteButton().click(function () { ui.deleteFile($(divFile)) }))
				.append($("<a/>").addClass("fileName").text(fileInfo.FileName))
				.append(ui.getProgressBar());

			if (fileInfo.FileID !== null)
				ui.success(divFile, "Uploaded", { FileName: fileInfo.FileName, FileType: fileInfo.FileType, FileID: fileInfo.FileID, File: null });
			else {
				var fileExtension = ui.getFileExtension(fileInfo.FileName);
				if (fileExtension === null || !ui.isFileTypeAllowed(fileExtension))
					ui.error(divFile, "InvalidFileType", ui.getFileTypeNotAllowedErrorMessage());
				else if (maxFileSize < fileInfo.File.size)
					ui.error(divFile, "LargeFileSize", ui.getMaxFileSizeErrorMessage());
				else
					ui.progress(divFile, "ReadyToUpload", 0);
			}
			return divFile;
		},
		isInProgress: function () { return $("div.File.InProgress", $(div)).length > 0; },
		processFiles: function (files) {
			$.each(files, function (i, file) { ui.addFileControls({ FileName: file.name, FileType: null, FileID: null, File: file }); });
			ui.updateValue();
			var processPendingFile = function () {
				var divFiles = $("div.File.ReadyToUpload", div);
				if (!divFiles.length)
					return;
				var divFile = divFiles[0];
				$.ajax({
					url: getUrl(),
					type: "POST",
					data: ui.getFormData("ADD", null, $(divFile).data("FileInfo").File),
					cache: false,
					dataType: "json",
					processData: false, // Don't process the files
					contentType: false, // Set content type to false as jQuery will tell the server its a query string request
					beforeSend: function (jqXHR, settings) {
						$(divFile).removeClass("ReadyToUpload");
						if (maxNumberOfFiles <= ui.filesInfo().length) {
							jqXHR.abort();
							ui.error($(divFile), "ErrorMaxNumberOfFiles", ui.getMaxNumberOfFilesErrorMessage());
							processPendingFile();
						}
						else
							ui.progress($(divFile), "Uploading", 0);
					},
					xhr: ui.getXMLHttpRequest(divFile, "Uploading..."),
					success: function (data, textStatus, jqXHR) {
						ui.progress($(divFile), "Uploading", 100);
						$(divFile).removeClass("Uploading");
						if (data.success)
							ui.success($(divFile), "Uploaded", { FileName: data.result.FileName, FileType: data.result.FileType, FileID: data.result.FileID, File: null });
						else
							ui.error($(divFile), "UploadingError", data.result);
						processPendingFile();
					},
					error: function (jqXHR, error, errorThrown) {
						ui.ajaxError(jqXHR, error, errorThrown);
						$(divFile).removeClass("Uploading");
						ui.error($(divFile), "UploadingError", errorThrown);
					}
				});
			};
			processPendingFile();
		}
	};
	btn.click(function () {
		if (ui.isInProgress() || $(btn).prop("disabled") === true)
			return;
		$(btn).prop("disabled", true);
		var timerHandle = setTimeout(function () {
			if (ui.isInProgress()) return;
			clearTimeout(timerHandle);
			$(btn).prop("disabled", false);
		}, 300);
		$(fu).off("change").on("change", function () { ui.processFiles(this.files); }).click();
	});

	var json = $(hf).val();
	if (json && json.length > 0)
		$(JSON.parse(json)).each(function (i, fileInfo) { ui.addFileControls(fileInfo); });

	fu[0].FileUploadInitialized = true;
}

function AspireFileUploadValidator(source, args) {
	var aspireFileUploadID = source.getAttribute("data-AspireFileUploadID");
	var fu = $("#" + aspireFileUploadID + "_fu");
	var hfID = $(fu).attr("data-hfid");
	var controlToValidate = $("#" + hfID);
	var allowNull = source.getAttribute("data-AllowNull".toLowerCase()).toLowerCase() === "true";
	var requiredErrorMessage = source.getAttribute("data-RequiredErrorMessage".toLowerCase());
	var uploadingInProgressMessage = source.getAttribute("data-UploadingInProgressMessage".toLowerCase());
	var inProgress = $("div.File.InProgress", $("#" + aspireFileUploadID + "_div")).length > 0;
	if (inProgress) {
		args.IsValid = false;
		source.errormessage = uploadingInProgressMessage;
	}
	else {
		var json = controlToValidate.val();
		if (json && json.length > 0) {
			var files = JSON.parse(json);
			if (files === null || files.length <= 0) {
				if (allowNull)
					args.IsValid = true;
				else {
					args.IsValid = false;
					source.errormessage = requiredErrorMessage;
				}
			} else
				args.IsValid = true;
		} else {
			if (allowNull)
				args.IsValid = true;
			else {
				args.IsValid = false;
				source.errormessage = requiredErrorMessage;
			}
		}
	}
	ApplyHighlightCss(source, aspireFileUploadID, args);
}
