﻿"use strict";
var AspireModal = {
	createModal: function () {
		return $("<div role=\"dialog\" aria-hidden=\"true\"></div>")
			.addClass("modal fade AspireModal")
			.attr({ "tabindex": "-1", "role": "dialog", "aria-hidden": "true" });
	},
	createModalDialog: function (modalSizeCssClass) {
		return $("<div></div>").addClass("modal-dialog").addClass(modalSizeCssClass);
	},
	getModalDialog: function (modal) {
		return $(modal).find(".modal-dialog");
	},
	createModalContent: function () {
		return $("<div></div>").addClass("modal-content");
	},
	getModalContent: function (modal) {
		return $(modal).find(".modal-content");
	},
	createModalHeader: function () {
		return $("<div></div>").addClass("modal-header");
	},
	getModalHeader: function (modal) {
		return AspireModal.getModalContent(modal).find(".modal-header");
	},
	createModalCloseButton: function () {
		return $("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>");
	},
	getModalCloseButton: function (modal) {
		return AspireModal.getModalHeader(modal).find("button.close");
	},
	createModalTitle: function (title) {
		return $("<h4 class=\"modal-title\"></h4>").addClass("modal-title").text(title);
	},
	getModalTitle: function (modal) {
		return AspireModal.getModalHeader(modal).find(".modal-title");
	},
	createModalBody: function () {
		return $("<div></div>").addClass("modal-body");
	},
	getModalBody: function (modal) {
		return AspireModal.getModalContent(modal).find(".modal-body");
	},
	createModalFooter: function () {
		return $("<div></div>").addClass("modal-footer");
	},
	getModalFooter: function (modal) {
		return AspireModal.getModalContent(modal).find(".modal-footer");
	},
	createFooterButton: function (btnText, btnCssClass, closesDialog, okButton) {
		if (btnText === null)
			btnText = "Close";
		if (btnCssClass === null)
			btnCssClass = "btn btn-default";
		var btn = $("<button type=\"button\"></button>")
			.text(btnText)
			.addClass(btnCssClass);
		if (closesDialog)
			btn.attr("data-dismiss", "modal");
		if (okButton)
			btn.attr("ok-button", "true");
		return btn;
	},
	createFooterCancelButton: function (btnText, btnCssClass) {
		return AspireModal.createFooterButton(btnText, btnCssClass, true, false);
	},
	getFooterCancelButton: function (modal) {
		return $("button[data-dismiss=\"modal\"]", AspireModal.getModalFooter(modal));
	},
	createFooterOkButton: function (btnText, btnCssClass) {
		return AspireModal.createFooterButton(btnText, btnCssClass, false, true);
	},
	getFooterOkButton: function (modal) {
		return $("button[ok-button=\"true\"]", AspireModal.getModalFooter(modal));
	},
	setZIndex: function (modal) {
		var zIndex = 1040 + (10 * $(".modal:visible").length);
		$(modal).css("z-index", zIndex);
		setTimeout(function () {
			$(".modal-backdrop").not(".modal-stack").css("z-index", zIndex - 1).addClass("modal-stack");
		}, 0);
	},
	destroyModalBackDrop: function (zIndex) {
		$(".modal-backdrop.modal-stack").each(function (i, v) {
			if ($(v).css("z-index") === zIndex + 1)
				$(v).remove();
		});
	},
	arrangeZIndexes: function () {
		if (!$(".modal.in").length && !$("body").hasClass("modal-open"))
			return;
		var backdrops = $(".modal-backdrop.in");
		var modals = $(".modal.in");
		if (modals.length !== backdrops.length)
			return;
		modals.each(function (index, modal) {
			var modalZIndex = $(modal).css("z-index") - 1;
			var backdropZIndex = $(backdrops[index]).css("z-index");
			if (modalZIndex !== backdropZIndex)
				$(backdrops[index]).css("z-index", modalZIndex);
		});
	},
	destroyModal: function (modal) {
		var zIndex = $(modal).css("z-index");
		$(modal).data("bs.modal", null);
		$(modal).remove();
		AspireModal.destroyModalBackDrop(zIndex);
		if (!$(".modal.in").length && !$("body").hasClass("modal-open")) {
			$(".modal-backdrop").remove();
		}
		AspireModal.arrangeZIndexes();
	},
	alert: function (title, msg, modalSizeCssClass, onShown, onHidden, onShow, onHide) {
		var bodyContent = msg !== null ? $("<p></p>").text(msg) : null;
		var modal = AspireModal.createFullModal(title, modalSizeCssClass, true, bodyContent, null, null, "Ok", "btn btn-primary").addClass("AspireModal");
		return AspireModal.open(modal, "static", true, true, onShown, onHidden, onShow, onHide);
	},
	confirmModalResult: false,
	confirm: function (control, title, msg, modalSizeCssClass) {
		if (!AspireModal.confirmModalResult) {
			var bodyContent = msg !== null ? $("<p></p>").text(msg) : null;
			var modal = AspireModal.createFullModal(title, modalSizeCssClass, true, bodyContent, "Yes", "btn btn-primary", "No", "btn btn-default").addClass("AspireModal");
			AspireModal.getFooterOkButton(modal)
				.click(function () {
					AspireModal.confirmModalResult = true;
					AspireModal.close(modal, false);
					$(control)[0].click();
				});
			AspireModal.open(modal, "static", true, true);
			return false;
		} else {
			AspireModal.confirmModalResult = false;
			return true;
		}
	},
	resetBodyClass: function () {
		if ($(".modal.in").length)
			$("body").addClass("modal-open");
		else
			$("body").removeClass("modal-open").css("padding-right", "");
	},
	open: function (modal, backdrop, keyboard, destroyOnClose, onShown, onHidden, onShow, onHide) {
		AspireModal.arrangeZIndexes();
		return $(modal).modal({ backdrop: backdrop, keyboard: keyboard, show: true })
			.on("show.bs.modal", function () {
				if (onShow !== undefined && onShow !== null)
					onShow(arguments);
			})
			.on("shown.bs.modal", function () {
				AspireModal.setZIndex($(this));
				if (onShown !== undefined && onShown !== null)
					onShown(arguments);
			})
			.on("hide.bs.modal", function () {
				if (onHide !== undefined && onHide !== null)
					onHide(arguments);
			})
			.on("hidden.bs.modal", function () {
				var zIndex = $(modal).css("z-index");
				AspireModal.resetBodyClass();
				if (onHidden !== undefined && onHidden !== null)
					onHidden(arguments);
				if (destroyOnClose)
					AspireModal.destroyModal(modal);
				setTimeout(function () { AspireModal.destroyModalBackDrop(zIndex); }, 100);
			});
	},
	close: function (modal, destroy) {
		$(modal).modal("hide");
		if (destroy)
			AspireModal.destroyModal(modal);
	},
	closeAll: function (destroy) {
		$(".modal.in").each(function (index, modal) {
			AspireModal.close(modal, destroy);
		});
	},
	createFullModal: function (title, modalSizeCssClass, modalCloseButtonVisible, bodyContents, okButtonText, okButtonCssClass, cancelButtonText, cancelButtonCssClass) {
		var modal = AspireModal.createModal();
		var modalDialog = AspireModal.createModalDialog(modalSizeCssClass).appendTo(modal);
		var modalContent = AspireModal.createModalContent().appendTo(modalDialog);
		var modalHeader = AspireModal.createModalHeader().appendTo(modalContent);
		if (modalCloseButtonVisible)
			modalHeader.append(AspireModal.createModalCloseButton());
		modalHeader.append(AspireModal.createModalTitle(title));
		AspireModal.createModalBody().appendTo(modalContent).append(bodyContents);
		var modalFooter = AspireModal.createModalFooter().appendTo(modalContent);
		if (okButtonText !== null)
			AspireModal.createFooterOkButton(okButtonText, okButtonCssClass).appendTo(modalFooter);
		if (cancelButtonText !== null)
			AspireModal.createFooterCancelButton(cancelButtonText, cancelButtonCssClass).appendTo(modalFooter);
		return modal;
	},
	isProgressModalVisible: function () {
		return $(".modal.UpdatePanelProgressModal").length > 0;
	},
	showProgressModal: function (onShown, onHidden, onShow, onHide) {
		var spinner = $("<i class=\"fa fa-spin fa-sync fa-5x fa-fw\"></i>");
		var spinnerDiv = $("<div></div>").addClass("spinnerContainer").append(spinner);
		var processingModal = AspireModal.createModal().addClass("UpdatePanelProgressModal");
		var modalDialog = AspireModal.createModalDialog().appendTo(processingModal);
		var modalContent = AspireModal.createModalContent().appendTo(modalDialog);
		var modalBody = AspireModal.createModalBody().append(spinnerDiv).appendTo(modalContent);
		AspireModal.open(processingModal, "static", false, true, onShown, onHidden, onShow, onHide);
		return processingModal;
	},
	closeProgressModal: function () {
		AspireModal.close($(".modal.UpdatePanelProgressModal"));
	},
	initProcessingModalForPartialPostbacks: function () {
		var processingModal;
		var timerBeginRequest;
		var beginRequest = function () {
			clearTimeout(timerBeginRequest);
			timerBeginRequest = setTimeout(function () { processingModal = AspireModal.showProgressModal(null, null, null, null); }, 0);
		};
		var cleanup = function () {
			if (timerBeginRequest !== null)
				clearTimeout(timerBeginRequest);
			if (processingModal !== undefined) {
				AspireModal.close(processingModal);
				processingModal = null;
			}
			AspireModal.resetBodyClass();
		};
		var endRequest = function (sender, args) {
			var lastError = args.get_error();
			if (lastError !== null) {
				args.set_errorHandled(true);
				console.error(lastError.message);
				AspireModal.alert("Message", "An unexpected error may have occurred while processing your request.");
			};
			setTimeout(cleanup, 200);
		}
		Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
		Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);
	}
};
