﻿function filterRows(table, searchText, caseSensitive) {
	searchText = searchText.trim();
	if (!caseSensitive)
		searchText = searchText.toLowerCase();
	var recordsFound = 0;
	$("tr", $(table)).each(function (index, row) {
		var visible = true;
		if ($(row).parent().is("thead"))
			visible = true;
		else if ($(row).parent().is("tfoot"))
			visible = true;
		else {
			if (searchText == null || searchText.length === 0)
				recordsFound++;
			else
				if (searchText.length > 0) {
					var rowText = $(row).text();
					if (!caseSensitive)
						rowText = rowText.toLowerCase();
					visible = rowText.indexOf(searchText) >= 0;
					if (visible)
						recordsFound++;
				}
		}
		if (visible)
			$(row).show();
		else
			$(row).hide();
	});
	if (recordsFound > 0)
		$("tbody", $(table)).show();
	else
		$("tbody", $(table)).hide();
	return recordsFound;
};

function setOrAppendTableFooter(table, recordsFound) {
	var footer = $("tfoot", $(table));
	if (footer.length === 0)
		footer = $("<tfoot><tr><td colspan=\"255\"></td></tr></tfoot>").appendTo($(table));
	var th = $("td", footer);
	var message;
	if (recordsFound === 0)
		message = "No record found.";
	else if (recordsFound === 1)
		message = recordsFound + " record found.";
	else if (recordsFound > 1)
		message = recordsFound + " records found.";
	else throw "invalid record found value.";
	th.text(message);
}

function applyFilterEvents(table, inputSearch, btnClear, btnSearch, caseSensitive, onFilter) {
	$(inputSearch).on("change blur keyup paste", function () {
		var recordsFound = filterRows(table, $(this).val(), caseSensitive);
		if (onFilter != undefined)
			onFilter();
		//setOrAppendTableFooter(table, recordsFound);
	});
	$(btnSearch).click(function () { $(inputSearch).change(); }).click();
	$(btnClear).click(function () { $(inputSearch).val("").change(); });
}

function cblApplySelectAll(cblSelector) {
	function applyListener() {
		$(function () {
			var checkboxes = $("input", $(cblSelector)).off("change");
			checkboxes.change(function () {
				if ($(this).val() === "")
					checkboxes.prop("checked", $(this).is(":checked"));
				else {
					var cbSelectAll;
					var allChecked = true;
					checkboxes.each(function (ii, cbb) {
						if ($(cbb).val() === "")
							cbSelectAll = $(cbb);
						else if (allChecked)
							allChecked = $(cbb).prop("checked");
					});
					cbSelectAll.prop("checked", allChecked);
				}
			});
		});
	}
	applyListener();
	Sys.Application.add_load(applyListener);
}

function applySelectAllToCheckBoxes(cbSelectAll, checkboxesList) {
	function applyListener() {
		cbSelectAll.off("change").change(function () {
			checkboxesList.prop("checked", $(this).is(":checked"));
		});
		checkboxesList.off("change").change(function () {
			if (!$(this).is(":checked"))
				cbSelectAll.prop("checked", false);
			else {
				var allChecked = true;
				checkboxesList.each(function (i, cb) {
					if (!$(cb).is(":checked"))
						allChecked = false;
				});
				cbSelectAll.prop("checked", allChecked);
			}
		});
 	}
	applyListener();
	Sys.Application.add_load(applyListener);
}