﻿using Aspire.Web.Common;

namespace Aspire.Web
{
	public static class AspireHelpMapInitializer
	{
		internal static void Initialize()
		{
			typeof(Aspire.Web.Logins.Candidate.Login).AddLink("#");
		}
	}
}
