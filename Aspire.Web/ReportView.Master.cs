﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.Helpers;
using Aspire.Web.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Linq;

namespace Aspire.Web
{
	public partial class ReportView : System.Web.UI.MasterPage
	{
		public new BasePage Page => (BasePage)base.Page;
		private IReportViewer ReportViewPage => (IReportViewer)this.Page;
		public static string ReportFormat => "ReportFormat";
		public static string Inline => "Inline";
		public ReportViewer ReportViewer => this.reportViewer;

		protected void Page_Load(object sender, EventArgs e)
		{
			this.Page.SetMaximumScriptTimeout();
			if (!this.IsPostBack)
			{
				this.ReportViewPage.RenderReport(this.reportViewer);
				var reportFormat = this.Request.GetParameterValue<ReportFormats>(ReportFormat);
				var inline = this.Request.GetParameterValue<bool>(Inline);
				if (reportFormat != null)
					this.Export(reportFormat.Value, inline ?? true);

				this.liExcel.Visible = this.ReportViewPage.ExportToExcel;
				this.liWord.Visible = this.ReportViewPage.ExportToWord;
				this.btnExportDropDown.Visible = this.liExcel.Visible || this.liWord.Visible;
				this.ulExport.Visible = this.liExcel.Visible || this.liWord.Visible;
			}
		}

		protected void btnExport_OnClick(object sender, EventArgs e)
		{
			var reportFormat = (ReportFormats)Enum.Parse(typeof(ReportFormats), ((System.Web.UI.WebControls.LinkButton)sender).CommandName);
			this.Export(reportFormat, false);
		}

		protected void Export(ReportFormats format, bool inline)
		{
			this.Context.Response.Clear();
			this.Context.Response.Buffer = true;

			var fileName = this.ReportViewPage.ReportName;
			switch (format)
			{
				case ReportFormats.Excel:
					this.Context.Response.ContentType = "application/vnd.ms-excel";
					fileName += ".xls";
					break;
				case ReportFormats.PDF:
					this.Context.Response.ContentType = "application/pdf";
					fileName += ".pdf";
					break;
				case ReportFormats.Word:
					this.Context.Response.ContentType = "application/msword";
					fileName += ".doc";
					break;
				case ReportFormats.Image:
					throw new NotImplementedException();
				default:
					throw new NotImplementedEnumException(format);
			}

			var fileBytes = this.reportViewer.LocalReport.Render(format.ToString());
			this.Context.Response.AddHeader("content-disposition", (inline ? "inline" : "attachment") + $"; filename=\"{fileName}\"");
			this.Context.Response.BinaryWrite(fileBytes);
			this.Context.Response.Flush();
		}

		protected void reportViewer_OnReportError(object sender, ReportErrorEventArgs e)
		{
			if (e.Exception.Message.ToLower().Contains("ASP.NET session has expired or could not be found".ToLower()))
			{
				e.Handled = true;
				Default.RedirectToHomeScreen();
			}
		}

		public void ResetReport()
		{
			this.reportViewer.LocalReport.DataSources.Clear();
			this.reportViewer.LocalReport.ReportPath = null;
			this.reportViewer.Reset();
			this.divExportButtons.Visible = false;
		}

		protected void reportViewer_OnPreRender(object sender, EventArgs e)
		{
			this.divExportButtons.Visible = this.reportViewer.LocalReport.ReportPath != null && this.reportViewer.LocalReport.DataSources.Any();
		}
	}
}