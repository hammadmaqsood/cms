﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Web
{
	public partial class ProfileInfo : UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			var profileInfo = this.Page as IProfileInfo;
			if (profileInfo == null)
			{
				this.Controls.Clear();
				return;
			}
			this.lblUsername.Text = profileInfo.Username;
			this.lblName.Text = profileInfo.Name;
			this.lblEmail.Text = profileInfo.Email;
			this.lblEmail.Visible = !string.IsNullOrWhiteSpace(profileInfo.Email);
			this.liProfile.Visible = profileInfo.ProfileLinkVisible;
			if (profileInfo.ProfileLinkVisible)
				this.hlProfile.NavigateUrl = profileInfo.ProfileNavigateUrl;
			this.liFeedback.Visible = new UserTypes?[] { UserTypes.Staff, UserTypes.Faculty, UserTypes.Executive }.Contains(AspireIdentity.Current?.UserType);
			this.hlLogoff.NavigateUrl = profileInfo.LogoffNavigateUrl;
			if (profileInfo.IsCustomProfileImage)
			{
				this.imgAvatar.ImageUrl = profileInfo.CustomProfileImageUrl;
				this.imgAvatarBig.ImageUrl = this.imgAvatar.ImageUrl;
			}
			else
			{
				var emailHash = (profileInfo.Email.ToNullIfWhiteSpace() ?? "").ComputeHashMD5();
				this.imgAvatar.Attributes.Add("width", "32");
				this.imgAvatar.Attributes.Add("height", "32");
				this.imgAvatar.Attributes.Add("data-jdenticon-hash", emailHash);
				this.imgAvatar.CssClass = "avatar";

				this.imgAvatarBig.Attributes.Add("width", "100");
				this.imgAvatarBig.Attributes.Add("height", "100");
				this.imgAvatarBig.Attributes.Add("data-jdenticon-hash", emailHash);
				this.imgAvatarBig.CssClass = "avatar";
			}

			var aspireIdentity = AspireIdentity.Current;
			if (aspireIdentity != null)
			{
				var staffUserIdentity = aspireIdentity as StaffIdentity;
				if (staffUserIdentity != null)
					this.SetRoles(staffUserIdentity.Roles, staffUserIdentity.UserRoleID, staffUserIdentity.UserType);
				else
				{
					var executiveUserIdentity = aspireIdentity as ExecutiveIdentity;
					if (executiveUserIdentity != null)
						this.SetRoles(executiveUserIdentity.Roles, executiveUserIdentity.UserRoleID, executiveUserIdentity.UserType);
				}
			}
			switch (aspireIdentity?.UserType)
			{
				case UserTypes.MasterAdmin:
				case UserTypes.Admin:
					this.liChangePassword.Visible = true;
					this.hlChangePassword.NavigateUrl = typeof(Sys.Admin.ChangePassword).GetAspirePageAttribute().PageUrl;
					break;
				case UserTypes.Executive:
					this.liChangePassword.Visible = true;
					this.hlChangePassword.NavigateUrl = typeof(Sys.Executive.ChangePassword).GetAspirePageAttribute().PageUrl;
					break;
				case UserTypes.Staff:
					this.liChangePassword.Visible = true;
					this.hlChangePassword.NavigateUrl = typeof(Sys.Staff.ChangePassword).GetAspirePageAttribute().PageUrl;
					break;
				case UserTypes.Candidate:
					this.liChangePassword.Visible = true;
					this.hlChangePassword.NavigateUrl = typeof(Sys.Candidate.ChangePassword).GetAspirePageAttribute().PageUrl;
					break;
				case UserTypes.Student:
					this.liChangePassword.Visible = true;
					this.hlChangePassword.NavigateUrl = typeof(Sys.Student.ChangePassword).GetAspirePageAttribute().PageUrl;
					break;
				case UserTypes.Faculty:
					this.liChangePassword.Visible = true;
					this.hlChangePassword.NavigateUrl = typeof(Sys.Faculty.ChangePassword).GetAspirePageAttribute().PageUrl;
					break;
				case UserTypes.Anonymous:
				case UserTypes.System:
				case UserTypes.Alumni:
				case UserTypes.IntegratedService:
				case UserTypes.ManualSQL:
				case UserTypes.Admins:
				case UserTypes.Any:
				case null:
				default:
					this.liChangePassword.Visible = false;
					break;
			}
		}

		private void SetRoles(IReadOnlyDictionary<int, string> roles, int userRoleID, UserTypes userTypeEnum)
		{
			foreach (var userRole in roles)
			{
				var cssClass = userRole.Key == userRoleID ? "class=\"Active\"" : string.Empty;
				this.phRoles.Controls.Add(new LiteralControl($"<li {cssClass}>"));
				var hyperlink = new HyperLink
				{
					Text = $"{userTypeEnum.ToFullName()} @ {userRole.Value}",
					Enabled = userRole.Key != userRoleID,
				};
				switch (userTypeEnum)
				{
					case UserTypes.Executive:
						hyperlink.NavigateUrl = Sys.Executive.ChangeRole.GetPageUrl(userRole.Key);
						break;
					case UserTypes.Staff:
						hyperlink.NavigateUrl = Sys.Staff.ChangeRole.GetPageUrl(userRole.Key);
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(userTypeEnum), userTypeEnum, null);
				}
				this.phRoles.Controls.Add(hyperlink);
				this.phRoles.Controls.Add(new LiteralControl("</li>"));
			}
		}
	}
}