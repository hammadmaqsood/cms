﻿using Aspire.Lib.Helpers;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;

namespace Aspire.Web
{
	[AspirePage("{18FA9924-2A2D-48B8-825E-023A881D15C7}", UserTypes.Admin, "~/NoJavascript.aspx", "No Javascript", false, AspireModules.None)]
	public partial class NoJavascript : AspireBasePage
	{
		protected override void Authenticate()
		{
			//Not Required
		}

		public static string GetPageUrl(string prevPageUrl)
		{
			return GetPageUrl<NoJavascript>().AttachQueryParam("PrevPageUrl", prevPageUrl, EncryptionModes.ConstantSaltNoneUserSessionNone);
		}

		public static void Redirect(string prevPageUrl)
		{
			AspireBasePage.Redirect(GetPageUrl(prevPageUrl));
		}

		private string PrevPageUrl => this.Request.GetParameterValue("PrevPageUrl");

		protected void Page_Load(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.PrevPageUrl))
				this.hlBack.Visible = false;
			else
				this.hlBack.NavigateUrl = this.PrevPageUrl;
		}

		protected string PrevPageUrlClientUrl
		{
			get
			{
				var prevPageUrl = this.PrevPageUrl ?? "~";
				return this.ResolveClientUrl(prevPageUrl);
			}
		}

		public override AspireThemes AspireTheme => AspireDefaultThemes.MasterAdmin;

		public override bool PageTitleVisible => false;

		public override PageIcon PageIcon => null;

		public override string PageTitle => null;
	}
}