using Aspire.Lib.Extensions.Exceptions;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using Aspire.Web.Sys.Executive.Admissions;
using Aspire.Web.Sys.Executive.Admissions.Reports;
using Aspire.Web.Sys.Executive.Courses;
using Aspire.Web.Sys.Executive.Exams;
using Aspire.Web.Sys.Executive.QualityAssurance.Reports;
using Aspire.Web.Sys.Executive.Students;
using Aspire.Web.Sys.Faculty.ClassAttendance;
using Aspire.Web.Sys.Staff.Admissions;
using Aspire.Web.Sys.Staff.Admissions.Reports;
using Aspire.Web.Sys.Staff.ClassAttendance;
using Aspire.Web.Sys.Staff.ClassAttendance.Reports;
using Aspire.Web.Sys.Staff.CourseOffering;
using Aspire.Web.Sys.Staff.CourseRegistration;
using Aspire.Web.Sys.Staff.Courses;
using Aspire.Web.Sys.Staff.ExamSeatingPlan;
using Aspire.Web.Sys.Staff.ExamSeatingPlan.Reports;
using Aspire.Web.Sys.Staff.FeeManagement;
using Aspire.Web.Sys.Staff.Students;
using Aspire.Web.Sys.Staff.Students.Reports;
using Aspire.Web.Sys.Student.CourseRegistration;
using System.Collections.Generic;
using System.Linq;
using Courses = Aspire.Web.Sys.Staff.ClassAttendance.Courses;
using OnlineTeachingReadinessForm = Aspire.Web.Sys.Executive.Courses.OnlineTeachingReadinessForm;
using StudentsAcademicRecords = Aspire.Web.Sys.Staff.Students.Reports.StudentsAcademicRecords;
using ViewClassAttendance = Aspire.Web.Sys.Faculty.ClassAttendance.ViewClassAttendance;

namespace Aspire.Web
{
	internal static class DefaultMenus
	{
		public static AspireSideBarLink AddLink(this AspireSideBarLinks links, string menuText, bool visible, string target = null, string cssClass = null)
		{
			return links.AddLink(new AspireSideBarLink(null, null, menuText, null, visible, target, cssClass));
		}

		public static AspireSideBarLink AddLink<T>(this AspireSideBarLinks links, bool visible, string target = null, string cssClass = null) where T : BasePage
		{
			var aspirePageAttribute = AspirePageAttributesDictionary.GetAspirePageAttribute<T>();
			return links.AddLink(null, aspirePageAttribute.AspirePageGuid, aspirePageAttribute.MenuItemText, aspirePageAttribute.PageUrl, visible, target, cssClass);
		}

		public static AspireSideBarLink AddSubLink(this AspireSideBarLink link, string menuText, bool visible, string target = null, string cssClass = null)
		{
			return link.AddSubLink(link, null, menuText, null, visible, target, cssClass);
		}

		public static AspireSideBarLink AddSubLink<T>(this AspireSideBarLink link, bool visible, string target = null, string cssClass = null) where T : BasePage
		{
			var aspirePageAttribute = AspirePageAttributesDictionary.GetAspirePageAttribute<T>();
			return link.AddSubLink(link, aspirePageAttribute.AspirePageGuid, aspirePageAttribute.MenuItemText, aspirePageAttribute.PageUrl, visible, target, cssClass);
		}

		public static AspireSideBarLinks GetAspireSideBarLinks(this UserTypes userType)
		{
			switch (userType)
			{
				case UserTypes.MasterAdmin:
					return MasterAdmin.Links;
				case UserTypes.Admin:
					return Admin.Links;
				case UserTypes.Staff:
					return Staff.Links;
				case UserTypes.Executive:
					return Executive.Links;
				default:
					throw new NotImplementedEnumException(userType);
			}
		}

		public static UserGroupMenuLink ToUserGroupMenuLink(this AspireSideBarLink aspireSideBarLink, UserGroupMenuLink parentUserGroupMenuLink)
		{
			if (aspireSideBarLink.AspirePageGuid == null)
				return new UserGroupMenuLink
				{
					AspirePageGuid = aspireSideBarLink.AspirePageGuid,
					DisplayIndex = aspireSideBarLink.DisplayIndex,
					LinkText = aspireSideBarLink.Text,
					LinkUrl = aspireSideBarLink.NavigateUrl,
					Visible = aspireSideBarLink.Visible,
					UserGroupMenuLink1 = parentUserGroupMenuLink,
				};
			var aspirePageAttribute = aspireSideBarLink.AspirePageGuid.Value.GetAspirePageAttribute();
			var userGroupMenuLink = new UserGroupMenuLink
			{
				AspirePageGuid = aspireSideBarLink.AspirePageGuid,
				DisplayIndex = aspireSideBarLink.DisplayIndex,
				LinkText = aspireSideBarLink.Text,
				LinkUrl = aspireSideBarLink.NavigateUrl,
				Visible = aspireSideBarLink.Visible,
				UserGroupMenuLink1 = parentUserGroupMenuLink,
			};
			if (aspirePageAttribute.MenuItemText == userGroupMenuLink.LinkText)
				userGroupMenuLink.LinkText = null;
			if (aspirePageAttribute.PageUrl == userGroupMenuLink.LinkUrl)
				userGroupMenuLink.LinkUrl = null;
			return userGroupMenuLink;
		}

		public static List<UserGroupMenuLink> ToUserGroupMenuLinks(this AspireSideBarLinks aspireSideBarLinks)
		{
			aspireSideBarLinks.SetDisplayIndexes();
			var userGroupMenuLinks = new List<UserGroupMenuLink>();
			foreach (var aspireSideBarLink in aspireSideBarLinks.Links)
				aspireSideBarLink.AddUserGroupMenuLinks(null, userGroupMenuLinks);
			return userGroupMenuLinks;
		}

		private static void AddUserGroupMenuLinks(this AspireSideBarLink aspireSideBarLink, UserGroupMenuLink parentUserGroupMenuLink, List<UserGroupMenuLink> userGroupMenuLinks)
		{
			var userGroupMenuLink = aspireSideBarLink.ToUserGroupMenuLink(parentUserGroupMenuLink);
			userGroupMenuLinks.Add(userGroupMenuLink);
			foreach (var subLink in aspireSideBarLink.SubLinks)
				subLink.AddUserGroupMenuLinks(userGroupMenuLink, userGroupMenuLinks);
		}

		private static AspireSideBarLink ToAspireSideBarLink(this UserGroupMenuLink userGroupMenuLink, AspireSideBarLink parentAspireSideBarLink)
		{
			if (userGroupMenuLink.AspirePageGuid == null)
				return new AspireSideBarLink(parentAspireSideBarLink, userGroupMenuLink.AspirePageGuid, userGroupMenuLink.LinkText, userGroupMenuLink.LinkUrl, userGroupMenuLink.Visible, null, null);
			var aspirePageAttribute = userGroupMenuLink.AspirePageGuid.Value.TryGetAspirePageAttribute();
			if (aspirePageAttribute == null)
				return null;
			var linkText = userGroupMenuLink.LinkText;
			var linkUrl = userGroupMenuLink.LinkUrl;
			if (string.IsNullOrWhiteSpace(linkText))
				linkText = aspirePageAttribute.MenuItemText;
			if (string.IsNullOrWhiteSpace(linkUrl))
				linkUrl = aspirePageAttribute.PageUrl;
			return new AspireSideBarLink(parentAspireSideBarLink, userGroupMenuLink.AspirePageGuid, linkText, linkUrl, userGroupMenuLink.Visible, null, null);
		}

		public static AspireSideBarLinks ToAspireSideBarLinks(this List<UserGroupMenuLink> userGroupMenuLinks, AspireSideBarLink homeAspireSideBarLink)
		{
			var aspireSideBarLinks = new AspireSideBarLinks(homeAspireSideBarLink);
			var selectedUserGroupMenuLinks = userGroupMenuLinks.Where(l => l.ParentUserGroupMenuLinkID == null).OrderBy(l => l.DisplayIndex).ToList();
			var sideBarLinks = selectedUserGroupMenuLinks.ToAspireSideBarLinks(null, userGroupMenuLinks);
			aspireSideBarLinks.AddLinks(sideBarLinks);
			return aspireSideBarLinks;
		}

		private static List<AspireSideBarLink> ToAspireSideBarLinks(this List<UserGroupMenuLink> selectedUserGroupMenuLinks, AspireSideBarLink parentAspireSideBarLink, List<UserGroupMenuLink> allUserGroupMenuLinks)
		{
			var aspireSideBarMenuLinks = new List<AspireSideBarLink>();

			foreach (var selectedUserGroupMenuLink in selectedUserGroupMenuLinks)
			{
				var aspireSideBarLink = selectedUserGroupMenuLink.ToAspireSideBarLink(parentAspireSideBarLink);
				if (aspireSideBarLink == null)
					continue;
				var childUserGroupLinks = allUserGroupMenuLinks.Where(l => l.ParentUserGroupMenuLinkID == selectedUserGroupMenuLink.UserGroupMenuLinkID).OrderBy(l => l.DisplayIndex).ToList();
				var childSideBarLinks = childUserGroupLinks.ToAspireSideBarLinks(aspireSideBarLink, allUserGroupMenuLinks);
				aspireSideBarLink.AddSubLinks(childSideBarLinks);
				aspireSideBarMenuLinks.Add(aspireSideBarLink);
			}
			return aspireSideBarMenuLinks;
		}

		public static class MasterAdmin
		{
			public static readonly AspireSideBarLinks Links;

			static MasterAdmin()
			{
				var homePageAspirePageAttribute = AspirePageAttributesDictionary.GetAspirePageAttribute<Sys.Admin.Dashboard>();
				var homeSideBarLink = new AspireSideBarLink(null, homePageAspirePageAttribute.AspirePageGuid, homePageAspirePageAttribute.MenuItemText, homePageAspirePageAttribute.PageUrl, true, null, null);
				Links = new AspireSideBarLinks(homeSideBarLink);

				var userManagement = Links.AddLink("User Management", true);
				var users = userManagement.AddSubLink<Sys.Admin.UserManagement.Users>(true);
				{
					users.AddSubLink<Sys.Admin.UserManagement.User>(false);
				}

				var userGroups = userManagement.AddSubLink<Sys.Admin.UserManagement.UserGroups>(true);
				{
					userGroups.AddSubLink<Sys.Admin.UserManagement.UserGroup>(false);
					userGroups.AddSubLink<Sys.Admin.UserManagement.UserGroupMenu>(false);
				}
				userManagement.AddSubLink<Sys.Admin.UserManagement.IntegratedServiceUsers>(true);
				userManagement.AddSubLink<Sys.Admin.UserManagement.PermissionsMatrix>(true);
				var administration = Links.AddLink("Administration", true);
				{
					administration.AddSubLink<Sys.Admin.Administration.Institutes>(true).AddSubLink<Sys.Admin.Administration.Institute>(false);
					administration.AddSubLink<Sys.Admin.Administration.Semesters>(true);
					administration.AddSubLink<Sys.Admin.Administration.Faculties>(true).AddSubLink<Sys.Admin.Administration.Faculty>(false);
					administration.AddSubLink<Sys.Admin.Administration.Departments>(true).AddSubLink<Sys.Admin.Administration.Department>(false);
					administration.AddSubLink<Sys.Admin.Administration.Programs>(true).AddSubLink<Sys.Admin.Administration.Program>(false);
					administration.AddSubLink<Sys.Admin.Administration.Contacts>(true);
				}

				var admissions = Links.AddLink("Admissions", true);
				{
					admissions.AddSubLink<Sys.Admin.Admissions.AdmissionCriteria>(true).AddSubLink<Sys.Admin.Admissions.AdmissionCriteriaDetails>(false);
					admissions.AddSubLink<Sys.Admin.Admissions.AdmissionsOpen>(true).AddSubLink<Sys.Admin.Admissions.AdmissionOpen>(false);
					admissions.AddSubLink<Sys.Admin.Admissions.CBTLabs>(true);
					admissions.AddSubLink<Sys.Admin.Admissions.CBTSessions>(true);
					admissions.AddSubLink<Sys.Admin.Admissions.CandidateInformationForCBT>(true);
				}

				var courseRegistration = Links.AddLink("Course Registration", true);
				{
					courseRegistration.AddSubLink<Sys.Admin.CourseRegistration.ControlPanel>(true);
				}

				var feeManagement = Links.AddLink("Fee Management", true);
				{
					feeManagement.AddSubLink<Sys.Admin.FeeManagement.FeeHeads>(true);
					feeManagement.AddSubLink<Sys.Admin.FeeManagement.InstituteBankAccounts>(true);
				}

				var classAttendance = Links.AddLink("Class Attendance", true);
				{
					classAttendance.AddSubLink<Sys.Admin.ClassAttendance.ControlPanel>(true);
					classAttendance.AddSubLink<Sys.Admin.ClassAttendance.MarkAttendance>(true).AddSubLink<Sys.Admin.ClassAttendance.StudentAttendance>(false).AddSubLink<Sys.Admin.ClassAttendance.ViewClassAttendance>(false).AddSubLink<Sys.Admin.ClassAttendance.MarkClassWiseAttendance>(false);
				}

				var qualityAssurance = Links.AddLink("Quality Assurance", true);
				{
					qualityAssurance.AddSubLink<Sys.Admin.QualityAssurance.ControlPanel>(true);
					qualityAssurance.AddSubLink<Sys.Admin.QualityAssurance.Surveys>(true).AddSubLink<Sys.Admin.QualityAssurance.SurveysGroupsQuestionsOptions>(false);
				}

				var facultyManagement = Links.AddLink("Faculty Management", true);
				{
					facultyManagement.AddSubLink<Sys.Admin.FacultyManagement.FacultyMembers>(true).AddSubLink<Sys.Admin.FacultyManagement.FacultyMember>(false);
					facultyManagement.AddSubLink<Sys.Admin.FacultyManagement.ManageAccounts>(true);
				}

				var exams = Links.AddLink("Exams", true);
				{
					exams.AddSubLink<Sys.Admin.Exams.ControlPanel>(true);
					exams.AddSubLink<Sys.Admin.Exams.ExamMarksPolicy>(true);
					exams.AddSubLink<Sys.Admin.Exams.ExamRemarksPolicy>(true);
				}

				var studentPortal = Links.AddLink("Student Portal", true);
				{
					studentPortal.AddSubLink<Sys.Admin.StudentPortal.ResetStudentsPassword>(true);
				}

				var documents = Links.AddLink("Official Documents", true);
				{
					documents.AddSubLink<Sys.Admin.OfficialDocuments.OfficialDocuments>(true);
				}

				var scholarships = Links.AddLink("Scholarships", true);
				{
					scholarships.AddSubLink<Sys.Admin.Scholarships.ControlPanel>(true);
				}

				var fileServer = Links.AddLink("File Server", true);
				{
					fileServer.AddSubLink<Sys.Admin.FileServer.FileExplorer>(true);
				}

				var feedbacks = Links.AddLink("Feedback", true);
				{
					feedbacks.AddSubLink<Sys.Admin.Feedbacks.Assignees>(true);
				}

				var logs = Links.AddLink("Logs", true);
				{
					logs.AddSubLink<Sys.Admin.Logs.Emails>(true);
					logs.AddSubLink<Sys.Admin.Logs.Errors>(true).AddSubLink<Sys.Admin.Logs.Error>(false);
					logs.AddSubLink<Sys.Admin.Logs.UsersLoginHistory>(true);
					logs.AddSubLink<Sys.Admin.Logs.UsageHistory>(true);
				}
			}
		}

		public static class Admin
		{
			public static AspireSideBarLinks Links => MasterAdmin.Links;

			static Admin()
			{
			}
		}

		public static class Staff
		{
			public static readonly AspireSideBarLinks Links;

			static Staff()
			{
				var homePageAspirePageAttribute = AspirePageAttributesDictionary.GetAspirePageAttribute<Sys.Staff.Dashboard>();
				var homeSideBarLink = new AspireSideBarLink(null, homePageAspirePageAttribute.AspirePageGuid, homePageAspirePageAttribute.MenuItemText, homePageAspirePageAttribute.PageUrl, true, null, null);

				Links = new AspireSideBarLinks(homeSideBarLink);
				var admissions = Links.AddLink("Admissions", true);
				{
					admissions.AddSubLink<SearchCandidate>(true).AddSubLink<CandidateDetails>(false);
					admissions.AddSubLink<ETSScores>(true);
					admissions.AddSubLink<AdmissionProcessingFee>(true);
					admissions.AddSubLink<CandidateInterview>(true);
					admissions.AddSubLink<CandidateStatus>(true);
					admissions.AddSubLink<GenerateEnrollments>(true);
					admissions.AddSubLink<StudentTransfer>(true);
					var reports = admissions.AddSubLink("Reports", true);
					{
						reports.AddSubLink<CandidateNominalReport>(true);
						reports.AddSubLink<ETSScorerNominalReport>(true);
						reports.AddSubLink<SummaryOfAppliedCandidates>(true);
						reports.AddSubLink<MeritListReport>(true);
						reports.AddSubLink<CandidateDetailsReport>(true);
						reports.AddSubLink<ProgramWiseSummeryReport>(true);
					}
				}
				var coursesRoadmap = Links.AddLink("Courses Roadmap", true);
				{
					coursesRoadmap.AddSubLink<Sys.Staff.Courses.Courses>(true).AddSubLink<Course>(false);
					coursesRoadmap.AddSubLink<CoursesEquivalents>(true).AddSubLink<CourseEquivalents>(false);
					coursesRoadmap.AddSubLink<OfferCourses>(true);
					coursesRoadmap.AddSubLink<Sys.Staff.CourseOffering.DateSheets>(true);
					coursesRoadmap.AddSubLink<CoursesCleanup>(true);
					coursesRoadmap.AddSubLink<Sys.Staff.CourseOffering.OfferedCourses>(true);
				}
				var courseRegistration = Links.AddLink("Course Registration", true);
				{
					courseRegistration.AddSubLink<RegisterCourse>(true);
					courseRegistration.AddSubLink<Sys.Staff.CourseRegistration.RegisteredCourses>(true);
					courseRegistration.AddSubLink<FirstSemesterCourseRegistration>(true);
					courseRegistration.AddSubLink<StudentSemesters>(true);
					courseRegistration.AddSubLink<CreditsTransfer>(true);
					courseRegistration.AddSubLink<ExemptedCourses>(true);
					courseRegistration.AddSubLink<SemesterFreeze>(true);
					courseRegistration.AddSubLink<CourseWithdrawal>(true);
					courseRegistration.AddSubLink<TimeBarredStudents>(true);
					courseRegistration.AddSubLink<CancelledAdmissions>(true);
				}

				var classAttendance = Links.AddLink("Class Attendance", true);
				{
					classAttendance.AddSubLink<Courses>(true).AddSubLink<Sys.Staff.ClassAttendance.ViewClassAttendance>(false).AddSubLink<Sys.Staff.ClassAttendance.ClassWiseAttendance>(false);
					classAttendance.AddSubLink<StudentWiseAttendance>(true);
					var reports = classAttendance.AddSubLink("Reports", true);
					{
						reports.AddSubLink<ShortAttendance>(true);
						reports.AddSubLink<CourseWiseAttendanceSummary>(true);
					}
				}

				var feeManagement = Links.AddLink("Fee Management", true);
				{
					feeManagement.AddSubLink<FeeStructures>(true);
					feeManagement.AddSubLink<FeeDueDates>(true);
					feeManagement.AddSubLink<FeeChallan>(true);
					feeManagement.AddSubLink<ViewFeeChallans>(true);
					feeManagement.AddSubLink<FeePayment>(true);
					feeManagement.AddSubLink<FeeConcessions>(true);
					feeManagement.AddSubLink<AttachCourseToChallan>(true);
					feeManagement.AddSubLink<FeePaymentFirstSemester>(true);
					feeManagement.AddSubLink<TutionFeeIncrement>(true);
					feeManagement.AddSubLink<Sys.Staff.FeeManagement.Reports.SelectReport>(true);
					feeManagement.AddSubLink<Sys.Staff.FeeManagement.Reports.ProgramWiseSummary>(true);
				}
				var studentsOperations = Links.AddLink("Student Information", true);
				{
					studentsOperations.AddSubLink<Sys.Staff.Students.Profile.Profile>(true);
					studentsOperations.AddSubLink<Sys.Staff.Students.StudentProfileView>(true);
					studentsOperations.AddSubLink<Sys.Staff.Students.CommunityServices>(true);
					studentsOperations.AddSubLink<Sys.Staff.Students.AcademicRecords>(true);
					studentsOperations.AddSubLink<Sys.Staff.Students.StudentDocuments>(true);
					studentsOperations.AddSubLink<SearchStudents>(true);
					studentsOperations.AddSubLink<Sys.Staff.Students.DisciplineCases>(true).AddSubLink<Sys.Staff.Students.DisciplineCasesStudents>(false);
					studentsOperations.AddSubLink<Sys.Staff.Students.DisciplineCaseActivities>(true);
					studentsOperations.AddSubLink<Sys.Staff.Students.UpdateRegistrationNos>(true);
					var reports = studentsOperations.AddSubLink("Reports", true);
					{
						reports.AddSubLink<Sys.Staff.Students.Reports.StudentDocuments>(true);
						reports.AddSubLink<StudentsAcademicRecords>(true);
						reports.AddSubLink<StudentsInformation>(true);
						reports.AddSubLink<RegistrationForm>(true);
						reports.AddSubLink<StudentInfoAndAcademicRecords>(true);
						reports.AddSubLink<GraduateDirectory>(true);
						reports.AddSubLink<DisciplineCaseStudents>(true);
						reports.AddSubLink<StudentUploadedDocuments>(true);
					}
				}

				var examsManagement = Links.AddLink("Exams", true);
				{
					var transcripts = examsManagement.AddSubLink("Transcripts", true);
					{
						transcripts.AddSubLink<Sys.Staff.Exams.Transcripts.CGPADebugger>(true);
					}
					var courses = examsManagement.AddSubLink<Sys.Staff.Exams.Courses>(true);
					{
						courses.AddSubLink<Sys.Staff.Exams.MarksSheet>(false);
						courses.AddSubLink<Sys.Staff.Exams.Marks>(false);
						courses.AddSubLink<Sys.Staff.Exams.ExamMarks>(false);
						courses.AddSubLink<Sys.Staff.Exams.CompileExam>(false);
					}
					examsManagement.AddSubLink<Sys.Staff.Exams.MarksSubmission>(true);
					examsManagement.AddSubLink<Sys.Staff.Exams.MarksSheetChanges>(true);
					examsManagement.AddSubLink<Sys.Staff.Exams.ProjectThesisInternships>(true).AddSubLink<Sys.Staff.Exams.ProjectThesisInternshipMarks>(false);
					examsManagement.AddSubLink<Sys.Staff.Exams.ExamResultStudentWise>(true);
					examsManagement.AddSubLink<Sys.Staff.Exams.ExamMarksPolicy>(true);
					examsManagement.AddSubLink<Sys.Staff.Exams.ExamRemarksPolicy>(true);
					examsManagement.AddSubLink<Sys.Staff.Exams.ExportData>(true);
					var reports = examsManagement.AddSubLink("Reports", true);
					{
						reports.AddSubLink<Sys.Staff.Exams.Reports.ProjectThesisInternshipsAwardList>(true);
						reports.AddSubLink<Sys.Staff.Exams.Reports.SemesterResult>(true);
						reports.AddSubLink<Sys.Staff.Exams.Reports.MarksEntryStatus>(true);
						reports.AddSubLink<Sys.Staff.Exams.Reports.StudentsSessionCompleted>(true);
						reports.AddSubLink<Sys.Staff.Exams.Reports.SemesterFreeze>(true);
					}
				}

				var scholarship = Links.AddLink("Scholarship", true);
				{
					scholarship.AddSubLink<Sys.Staff.Scholarship.SemesterWorkload>(true);
					scholarship.AddSubLink<Sys.Staff.Scholarship.MeritScholarship>(true);
					scholarship.AddSubLink<Sys.Staff.Scholarship.ScholarshipDashboard>(true);
				}

				var examSeatingPlan = Links.AddLink("Exam Seating Plan", true);
				{
					var reports = examSeatingPlan.AddSubLink("Reports", true);
					{
						reports.AddSubLink<DateSheet>(true);
						reports.AddSubLink<StudentAttendanceSheet>(true);
						reports.AddSubLink<StudentsHavingMultiplePapers>(true);
						reports.AddSubLink<ExaminerEnvelope>(true);
						reports.AddSubLink<InvigilationDuties>(true);
						reports.AddSubLink<InvigilatorDutiesDateWise>(true);
						reports.AddSubLink<InvigilatorsAttendance>(true);
						reports.AddSubLink<InvigilatorSlip>(true);
						reports.AddSubLink<InvigilatorsPayment>(true);
						reports.AddSubLink<PaperDistributionEnvelopeTitles>(true);
						reports.AddSubLink<QuestionPapersEnvelopeTitles>(true);
						reports.AddSubLink<PaperReceiving>(true);
						reports.AddSubLink<QuestionPaperReceiving>(true);
						reports.AddSubLink<SeatingSummary>(true);
						reports.AddSubLink<StudentSlips>(true);
						reports.AddSubLink<TeacherCoursesSlip>(true);
					}
					examSeatingPlan.AddSubLink<InvigilatorsDuties>(true);
					examSeatingPlan.AddSubLink<InvigilationDutiesAttendance>(true);
					examSeatingPlan.AddSubLink<ClearExtraSeats>(true);
					examSeatingPlan.AddSubLink<IndividualSeating>(true);
					examSeatingPlan.AddSubLink<DeleteSeats>(true);
					examSeatingPlan.AddSubLink<Execute>(true);
					examSeatingPlan.AddSubLink<StudentExamSeats>(true);
					examSeatingPlan.AddSubLink<OfferedRooms>(true);
					examSeatingPlan.AddSubLink<Sys.Staff.ExamSeatingPlan.DateSheets>(true);
					examSeatingPlan.AddSubLink<DateAndSessions>(true);
					examSeatingPlan.AddSubLink<Sessions>(true);
					examSeatingPlan.AddSubLink<Dates>(true);
					examSeatingPlan.AddSubLink<Conducts>(true);
					examSeatingPlan.AddSubLink<Rooms>(true);
					examSeatingPlan.AddSubLink<Buildings>(true);
					examSeatingPlan.AddSubLink<InvigilatorsAvailability>(true);
					examSeatingPlan.AddSubLink<Invigilators>(true);
					examSeatingPlan.AddSubLink<Designations>(true);
				}
				var qualityAssurance = Links.AddLink("Quality Assurance", true);
				{
					var surveyForm2 = qualityAssurance.AddSubLink("Faculty Course Review Report", true);
					{
						surveyForm2.AddSubLink<Sys.Staff.QA.Surveys.SurveyForm2.SurveyControlPanel>(true);
						surveyForm2.AddSubLink<Sys.Staff.QA.Surveys.SurveyForm2.Generate>(true);
						surveyForm2.AddSubLink<Sys.Staff.QA.Surveys.SurveyForm2.SummaryAnalysis.SummaryControlPanel>(true);
					}
					var surveyForm5 = qualityAssurance.AddSubLink("Faculty Survey", true);
					{
						surveyForm5.AddSubLink<Sys.Staff.QA.Surveys.SurveyForm5.SurveyControlPanel>(true);
						surveyForm5.AddSubLink<Sys.Staff.QA.Surveys.SurveyForm5.Generate>(true);
						surveyForm5.AddSubLink<Sys.Staff.QA.Surveys.SurveyForm5.SummaryAnalysis.SummaryControlPanel>(true);
					}
					var surveyForm6 = qualityAssurance.AddSubLink("Survey of Department Offering Ph.D. Programs", true);
					{
						surveyForm6.AddSubLink<Sys.Staff.QA.Surveys.SurveyForm6.SurveyControlPanel>(true);
						surveyForm6.AddSubLink<Sys.Staff.QA.Surveys.SurveyForm6.Generate>(true);
						surveyForm6.AddSubLink<Sys.Staff.QA.Surveys.SurveyForm6.SummaryAnalysis.SummaryControlPanel>(true);
					}
					var surveyForm7 = qualityAssurance.AddSubLink("Alumni Survey", true);
					{
						surveyForm7.AddSubLink<Sys.Staff.QA.Surveys.SurveyForm7.SurveyControlPanel>(true);
						surveyForm7.AddSubLink<Sys.Staff.QA.Surveys.SurveyForm7.Generate>(true);
						surveyForm7.AddSubLink<Sys.Staff.QA.Surveys.SurveyForm7.SummaryAnalysis.SummaryControlPanel>(true);
					}
					var surveyForm8 = qualityAssurance.AddSubLink("Employer Survey", true);
					{
						surveyForm8.AddSubLink<Sys.Staff.QA.Surveys.SurveyForm8.SurveyControlPanel>(true);
						surveyForm8.AddSubLink<Sys.Staff.QA.Surveys.SurveyForm8.Generate>(true);
						surveyForm8.AddSubLink<Sys.Staff.QA.Surveys.SurveyForm8.SummaryAnalysis.SummaryControlPanel>(true);
					}
				}
				var officialDocuments = Links.AddLink<Sys.Staff.OfficialDocuments.Documents>(true);
				var libraryManagement = Links.AddLink("Library Management", true);
				{
					libraryManagement.AddSubLink<Aspire.Web.Sys.Staff.LibraryManagement.StudentLibraryDefaulter>(true);
				}

				//Links.AddLink<Sys.Staff.Complaints.StudentComplaints>(true);
				Links.AddLink<Sys.Common.Feedbacks.Feedbacks>(true).AddSubLink<Sys.Common.Feedbacks.Feedback>(false);

				var alumni = Links.AddLink("Alumni", true);
				{
					alumni.AddSubLink<Sys.Staff.Alumni.AlumniJobPositions>(true);
					alumni.AddSubLink<Sys.Staff.Alumni.Report.AlumniProfessionalInformation>(true);
				}

				var forms = Links.AddLink("Forms", true);
				{
					var studentClearanceForm = forms.AddSubLink("Student Clearance Form", true);
					{
						studentClearanceForm.AddSubLink<Sys.Staff.Forms.StudentClearanceForm.StudentClearanceForm>(true);
						studentClearanceForm.AddSubLink<Sys.Staff.Forms.StudentClearanceForm.SearchClearanceForm>(true);
					}
				}
			}
		}

		public static class Student
		{
			public static readonly AspireSideBarLinks Links;

			static Student()
			{
				var homePageAspirePageAttribute = AspirePageAttributesDictionary.GetAspirePageAttribute<Sys.Student.Dashboard>();
				var homeSideBarLink = new AspireSideBarLink(null, homePageAspirePageAttribute.AspirePageGuid, homePageAspirePageAttribute.MenuItemText, homePageAspirePageAttribute.PageUrl, true, null, null);

				Links = new AspireSideBarLinks(homeSideBarLink);
				Links.AddLink<Sys.Student.Profile>(true);

				var courseRegistration = Links.AddLink("Course Registration", true);
				{
					courseRegistration.AddSubLink<Sys.Student.CourseRegistration.RegisteredCourses>(true);
					courseRegistration.AddSubLink<RegisterRegularCourses>(true);
					courseRegistration.AddSubLink<Sys.Student.CourseRegistration.OfferedCourses>(true);
				}
				Links.AddLink<Sys.Student.Exams.ExamResult>(true);
				Links.AddLink<Sys.Student.QualityAssurance.QualityAssuranceSurveys>(true);
				Links.AddLink<Sys.Student.FeeManagement.FeeChallans>(true);
				Links.AddLink<Sys.Student.ExamSeatingPlan.ExamSeats>(true);
				Links.AddLink<Sys.Student.StudentInformation.CommunityServices>(true);
				Links.AddLink<Sys.Student.StudentInformation.AcademicDocuments>(true);
				Links.AddLink<Sys.Student.OfficialDocuments.Documents>(true);
				Links.AddLink<Sys.Student.Scholarships.AvailableScholarships>(true);
				Links.AddLink<Sys.Student.EmailAccount>(true);
				Links.AddLink<Sys.Common.GoToLMS>(true, "_blank");
				Links.AddLink<Sys.Student.QualityAssurance.HECOnlineEducationSurvey>(true);
				//Links.AddSubLink<Aspire.Web.Sys.Student.Complaints.RegisterComplaints>(true);
				//var forms = Links.AddLink("Forms", true);
				//{
				//	forms.AddSubLink<Sys.Student.Forms.StudentClearanceForm.StudentClearanceForm>(true);
				//}
				Links.AddLink<Sys.Common.Feedbacks.Feedbacks>(true).AddSubLink<Sys.Common.Feedbacks.Feedback>(false);
			}
		}

		public static class StudentParent
		{
			public static readonly AspireSideBarLinks Links;

			static StudentParent()
			{
				var homePageAspirePageAttribute = AspirePageAttributesDictionary.GetAspirePageAttribute<Sys.Student.Dashboard>();
				var homeSideBarLink = new AspireSideBarLink(null, homePageAspirePageAttribute.AspirePageGuid, homePageAspirePageAttribute.MenuItemText, homePageAspirePageAttribute.PageUrl, true, null, null);

				Links = new AspireSideBarLinks(homeSideBarLink);
				Links.AddLink<Sys.Student.Profile>(true);

				var courseRegistration = Links.AddLink("Course Registration", true);
				{
					courseRegistration.AddSubLink<Sys.Student.CourseRegistration.RegisteredCourses>(true);
				}

				var exams = Links.AddLink("Exams", true);
				{
					exams.AddSubLink<Sys.Student.Exams.ExamResult>(true);
				}

				//Links.AddSubLink<Aspire.Web.Sys.Student.Complaints.RegisterComplaints>(true);
				Links.AddLink<Sys.Student.FeeManagement.FeeChallans>(true);
				Links.AddLink<Sys.Student.StudentInformation.CommunityServices>(true);
				//Links.AddLink<Sys.Student.StudentInformation.Documents>(true);
				Links.AddLink<Sys.Common.Feedbacks.Feedbacks>(true).AddSubLink<Sys.Common.Feedbacks.Feedback>(false);
			}
		}

		public static class Faculty
		{
			public static readonly AspireSideBarLinks Links;

			static Faculty()
			{
				var homePageAspirePageAttribute = AspirePageAttributesDictionary.GetAspirePageAttribute<Sys.Faculty.Dashboard>();
				var homeSideBarLink = new AspireSideBarLink(null, homePageAspirePageAttribute.AspirePageGuid, homePageAspirePageAttribute.MenuItemText, homePageAspirePageAttribute.PageUrl, true, null, null);

				Links = new AspireSideBarLinks(homeSideBarLink);

				var courses = Links.AddLink<Sys.Faculty.Courses>(true);
				{
					courses.AddSubLink<MarkAttendance>(false);
					courses.AddSubLink<StudentSummary>(false);
					courses.AddSubLink<ViewClassAttendance>(false);
					courses.AddSubLink<ClassSummary>(false);

					var marksSheet = courses.AddSubLink<Sys.Faculty.Exams.MarksSheet>(false);
					{
						marksSheet.AddSubLink<Sys.Faculty.Exams.Marks>(false);
						marksSheet.AddSubLink<Sys.Faculty.Exams.ExamMarks>(false);
						marksSheet.AddSubLink<Sys.Faculty.Exams.CompileExam>(false);
					}
				}

				var qualityAssurance = Links.AddLink("Quality Assurance", true);
				{
					qualityAssurance.AddSubLink<Sys.Faculty.QA.Surveys.SurveysList>(true);
				}

				Links.AddLink<Sys.Faculty.Exams.ExamEnvelopesSlip>(true);
				Links.AddLink<Sys.Faculty.OfficialDocuments.Documents>(true);
				Links.AddLink<Sys.Common.GoToLMS>(true, "_blank");
				Links.AddLink<Sys.Common.Feedbacks.Feedbacks>(true).AddSubLink<Sys.Common.Feedbacks.Feedback>(false);
			}
		}

		public static class Candidate
		{
			public static readonly AspireSideBarLinks Links;
			public static readonly AspireSideBarLinks LinksForeignStudent;

			static Candidate()
			{
				var homePageAspirePageAttribute = AspirePageAttributesDictionary.GetAspirePageAttribute<Sys.Candidate.Dashboard>();
				var homeSideBarLink = new AspireSideBarLink(null, homePageAspirePageAttribute.AspirePageGuid, homePageAspirePageAttribute.MenuItemText, homePageAspirePageAttribute.PageUrl, true, null, null);
				Links = new AspireSideBarLinks(homeSideBarLink);
				Links.AddLink<Sys.Candidate.Dashboard>(true);
				Links.AddLink<Sys.Candidate.AdmissionsSchedule>(true);
				Links.AddLink<Sys.Candidate.EligibilityCriteria>(true);
				Links.AddLink<Sys.Candidate.Profile>(true);
				Links.AddLink<Sys.Candidate.ApplyProgram>(true);
				Links.AddLink<Sys.Candidate.DetailedProfile>(true);
				Links.AddLink<Sys.Candidate.ContactUs>(true);

				LinksForeignStudent = new AspireSideBarLinks(homeSideBarLink);
				LinksForeignStudent.AddLink<Sys.Candidate.Dashboard>(true);
				LinksForeignStudent.AddLink<Sys.Candidate.AdmissionsSchedule>(true);
				LinksForeignStudent.AddLink<Sys.Candidate.EligibilityCriteria>(true);
				LinksForeignStudent.AddLink<Sys.Candidate.Profile>(true);
				LinksForeignStudent.AddLink<Sys.Candidate.ApplyPrograms>(true);
				LinksForeignStudent.AddLink<Sys.Candidate.DetailedProfile>(true);
				LinksForeignStudent.AddLink<Sys.Candidate.ContactUs>(true);
			}
		}

		public static class Executive
		{
			public static readonly AspireSideBarLinks Links;

			static Executive()
			{
				var homePageAspirePageAttribute = AspirePageAttributesDictionary.GetAspirePageAttribute<Sys.Executive.Dashboard>();
				var homeSideBarLink = new AspireSideBarLink(null, homePageAspirePageAttribute.AspirePageGuid, homePageAspirePageAttribute.MenuItemText, homePageAspirePageAttribute.PageUrl, true, null, null);

				Links = new AspireSideBarLinks(homeSideBarLink);
				var students = Links.AddLink("Students", true);
				{
					students.AddSubLink<RegisteredStrength>(true);
					students.AddSubLink<BatchwiseStudents>(true);
					students.AddSubLink<Sys.Executive.Students.StudentProfile>(false);
					students.AddSubLink<Sys.Executive.Students.RegisteredCourses>(false);
					students.AddSubLink<Sys.Executive.Students.StudentSearch>(true);
				}

				var courses = Links.AddLink("Courses", true);
				{
					courses.AddSubLink<Aspire.Web.Sys.Executive.Courses.Courses>(true);
					courses.AddSubLink<ClassAttendance>(false);
					courses.AddSubLink<ClassAttendanceSummary>(false);
					courses.AddSubLink<OnlineTeachingReadinessOfferedCourses>(true).AddSubLink<OnlineTeachingReadinessForm>(false);
				}

				var exams = Links.AddLink("Exams", true);
				{
					exams.AddSubLink<GradeSummary>(true);
					exams.AddSubLink<ExamResultStudentWise>(false);
				}

				var admissions = Links.AddLink("Admissions", true);
				{
					admissions.AddSubLink<SummaryProgramWise>(true);
					admissions.AddSubLink<FormSubmissionTrend>(true);
					admissions.AddSubLink<AppearedInEntryTestTrend>(true);
					admissions.AddSubLink<EnrolledStudentsTrend>(true);
					admissions.AddSubLink<EntryTestMarksCertificate>(true);
				}

				var qualityAssurance = Links.AddLink("Quality Assurance", true);
				{
					qualityAssurance.AddSubLink<StudentCourseWiseSurveySummary>(true);
					qualityAssurance.AddSubLink<IndividualSurveySummaryFilters>(true).AddSubLink<IndividualSurveySummary>(false);
					qualityAssurance.AddSubLink<DetailAnalysis>(true);
				}

				var officialDocuments = Links.AddLink<Web.Sys.Executive.OfficialDocuments.Documents>(true);

				Links.AddLink<Web.Sys.Common.GoToLMS>(true, "_blank");

				Links.AddLink<Sys.Common.Feedbacks.Feedbacks>(true).AddSubLink<Sys.Common.Feedbacks.Feedback>(false);
			}
		}
	}
}