﻿<%@ Page Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="Aspire.Web.ErrorPages.Error" %>

<asp:Content ContentPlaceHolderID="BodyPH" runat="server">
	<asp:Panel ID="panelAspxError" runat="server" CssClass="container-fluid text-center" Visible="False">
		<h1>
			<span class="fas fa-exclamation-triangle fa-3x text-danger"></span>
			<br />
			There was a problem serving the requested page.
		</h1>
		<p class="h3">
			An unexpected error may have occurred while processing your request.
		</p>
		<div class="help-block">
			<h3>What do I do now?</h3>
			<p class="h5">
				Try refreshing the page, or going back and trying to complete the request again. The problem may only be temporary.
				If the problem persists and you need help immediately,
				<aspire:AspireHyperLink runat="server" ID="hlContactUs" NavigateUrl="mailto:cmssupport@bahria.edu.pk">contact us</aspire:AspireHyperLink>.
			</p>
		</div>
		<div class="text-center">
			<aspire:AspireHyperLink runat="server" NavigateUrl="~/Default.aspx" Text="Go to Home Page" />
		</div>
	</asp:Panel>
	<asp:Panel runat="server" ID="panel404" CssClass="container-fluid text-center" Visible="False">
		<h1><span class="fas fa-exclamation-triangle fa-2x text-danger"></span>
			404! Not Found</h1>
		<p class="h3">
			The requested URL
			<span style="word-break: break-all; font-weight: bold">
				<aspire:Label runat="server" ID="lblUrl" /></span>
			was not found on this server.
		</p>
		<div class="text-center" style="margin: 3%">
			<asp:HyperLink runat="server" NavigateUrl="~/Default.aspx" Text="Go to Home Page" />
		</div>
	</asp:Panel>
</asp:Content>
