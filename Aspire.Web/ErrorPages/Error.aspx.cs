﻿using Aspire.BL.Core.Logs.Common;
using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using Aspire.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aspire.Web.ErrorPages
{
	[AspirePage("B9E27893-16D2-4235-920B-5B13C4CF79FB", UserTypes.Admin, "~/ErrorPages/Error.aspx", "Error", false, AspireModules.None)]
	public partial class Error : AspireBasePage
	{
		public override AspireThemes AspireTheme
		{
			get
			{
				if (Common.AspireIdentity.Current != null)
					return AspireDefaultThemes.GetDefaultThemeForCurrentUser();
				return AspireThemes.Fire;
			}
		}
		public override PageIcon PageIcon => null;
		public override string PageTitle => "Error";
		public override bool PageTitleVisible => false;

		protected override void Authenticate()
		{
			//Not Required
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			var errorInfo = new List<string> {
				$"Date: {DateTime.Now:F}",
				$"URL: {this.Request?.Url}",
				$"User Agent: {this.UserAgent}",
				$"User Type: {(this.AspireIdentity?.UserType.ToFullName()).ToNAIfNullOrEmpty()}",
				$"Username: {(this.AspireIdentity?.Username).ToNAIfNullOrEmpty()}",
				$"===============================================================================",
				$"",
				$"Provide details here (if any) ...",
				$"",
				$"",
			};
			var lastError = this.Server.GetLastError();
			var handled = false;
			if (lastError != null)
			{
				var errorCode = (lastError as HttpException)?.GetHttpCode() ?? -1;
				ErrorHandler.LogException(Common.AspireIdentity.Current?.UserLoginHistoryID, lastError, errorCode, this.Request.Url.AbsoluteUri, this.UserAgent, this.ClientIPAddress);
				if ((lastError as HttpException)?.GetHttpCode() == 404)
				{
					this.lblUrl.Text = this.Request.Url.ToString();
					this.panel404.Visible = true;
				}
				handled = true;
			}
			else
			{
				//IIS,404;https://localhost:44300/Logins/Candidate/Registration1.aspx1
				var param0 = this.Request.Params[0];
				if (param0.StartsWith("IIS,"))
				{
					param0 = param0.Substring(4);
					var semiColonIndex = param0.IndexOf(';');
					if (semiColonIndex > 0)
					{
						var errorCode = param0.Substring(0, semiColonIndex).TryToInt();
						if (errorCode != null)
						{
							var pageUrl = param0.Substring(semiColonIndex + 1);
							var exclusionList = new[]
							{
								"apple-touch-icon",
							};

							if (!exclusionList.Any(s => pageUrl.ToLower().Contains(s.ToLower())))
								ErrorHandler.LogException(Common.AspireIdentity.Current?.UserLoginHistoryID, null, errorCode.Value, pageUrl, this.UserAgent, this.ClientIPAddress, "IIS Error Code: " + errorCode);
							if (errorCode == 404)
							{
								this.lblUrl.Text = pageUrl;
								this.panel404.Visible = true;
							}
							handled = true;
						}
					}
				}
			}

			if (handled == false)
			{
				if (this.AspireIdentity?.UserType == null)
					Redirect<Default>();
				else
					Default.RedirectToHomeScreen();
				return;
			}

			this.panelAspxError.Visible = !this.panel404.Visible;
			var subject = "Error occurred in CMS";
			var body = errorInfo.Join(Environment.NewLine);
			var contactUsUrl = $"mailto:cmssupport@bahria.edu.pk?subject={subject.UrlPathEncode()}&body={body.UrlPathEncode()}&";
			this.hlContactUs.NavigateUrl = contactUsUrl;
		}
	}
}