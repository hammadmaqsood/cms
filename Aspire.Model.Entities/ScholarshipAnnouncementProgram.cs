//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Model.Entities
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// todo: Description is missing for [dbo].[ScholarshipAnnouncementPrograms] table.
    /// </summary>
    public partial class ScholarshipAnnouncementProgram
    {
        public int ScholarshipAnnouncementProgramID { get; set; }
        public int ScholarshipAnnouncementID { get; set; }
        public Nullable<int> ProgramID { get; set; }
        public Nullable<int> AdmissionOpenProgramID { get; set; }
    
        /// <summary>
        /// <para>Foreign Key Name: FK_ScholarshipAnnouncementPrograms_AdmissionOpenPrograms_AdmissionOpenProgramID</para>
        /// <para>Foreign Key Base Table: [dbo].[ScholarshipAnnouncementPrograms]</para>
        /// <para>Foreign Key Base Columns: [AdmissionOpenProgramID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[AdmissionOpenPrograms]</para>
        /// <para>Primary/Unique Key Base Columns: [AdmissionOpenProgramID]</para>
        /// </summary>
        public virtual AdmissionOpenProgram AdmissionOpenProgram { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_ScholarshipAnnouncementPrograms_Programs_ProgramID</para>
        /// <para>Foreign Key Base Table: [dbo].[ScholarshipAnnouncementPrograms]</para>
        /// <para>Foreign Key Base Columns: [ProgramID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Programs]</para>
        /// <para>Primary/Unique Key Base Columns: [ProgramID]</para>
        /// </summary>
        public virtual Program Program { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_ScholarshipAnnouncementPrograms_ScholarshipAnnouncements_ScholarshipAnnouncementID</para>
        /// <para>Foreign Key Base Table: [dbo].[ScholarshipAnnouncementPrograms]</para>
        /// <para>Foreign Key Base Columns: [ScholarshipAnnouncementID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[ScholarshipAnnouncements]</para>
        /// <para>Primary/Unique Key Base Columns: [ScholarshipAnnouncementID]</para>
        /// </summary>
        public virtual ScholarshipAnnouncement ScholarshipAnnouncement { get; set; }
    }
}
