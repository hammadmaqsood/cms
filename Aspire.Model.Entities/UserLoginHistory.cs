//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Model.Entities
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// todo: Description is missing for [dbo].[UserLoginHistory] table.
    /// </summary>
    public partial class UserLoginHistory
    {
        public UserLoginHistory()
        {
            this.AccountTransactions = new HashSet<AccountTransaction>();
            this.AdmissionOpenProgramExamMarksPolicies = new HashSet<AdmissionOpenProgramExamMarksPolicy>();
            this.ComplaintComments = new HashSet<ComplaintComment>();
            this.Courses = new HashSet<Cours>();
            this.Courses1 = new HashSet<Cours>();
            this.FacultyMemberPinCodes = new HashSet<FacultyMemberPinCode>();
            this.FormStudentClearanceDepartmentStatuses = new HashSet<FormStudentClearanceDepartmentStatus>();
            this.FormStudentClearances = new HashSet<FormStudentClearance>();
            this.OfferedCourses = new HashSet<OfferedCours>();
            this.OfferedCourses1 = new HashSet<OfferedCours>();
            this.OfferedCourses2 = new HashSet<OfferedCours>();
            this.OnlineTeachingReadinesses = new HashSet<OnlineTeachingReadiness>();
            this.OnlineTeachingReadinesses1 = new HashSet<OnlineTeachingReadiness>();
            this.OnlineTeachingReadinesses2 = new HashSet<OnlineTeachingReadiness>();
            this.OnlineTeachingReadinesses3 = new HashSet<OnlineTeachingReadiness>();
            this.PasswordChangeHistories = new HashSet<PasswordChangeHistory>();
            this.ProjectThesisInternships = new HashSet<ProjectThesisInternship>();
            this.ProjectThesisInternships1 = new HashSet<ProjectThesisInternship>();
            this.ProjectThesisInternships2 = new HashSet<ProjectThesisInternship>();
            this.QASummaryFacultyEvaluations = new HashSet<QASummaryFacultyEvaluation>();
            this.QASummaryFacultyEvaluations1 = new HashSet<QASummaryFacultyEvaluation>();
            this.QASummaryFacultyEvaluations2 = new HashSet<QASummaryFacultyEvaluation>();
            this.QASurveySummaryAnalysis = new HashSet<QASurveySummaryAnalysi>();
            this.RegisteredCourses = new HashSet<RegisteredCours>();
            this.RegisteredCourses1 = new HashSet<RegisteredCours>();
            this.StudentAcademicRecords = new HashSet<StudentAcademicRecord>();
            this.StudentCustomInfos = new HashSet<StudentCustomInfo>();
            this.StudentFees = new HashSet<StudentFee>();
            this.StudentLibraryDefaulters = new HashSet<StudentLibraryDefaulter>();
            this.Students = new HashSet<Student>();
            this.SystemFiles = new HashSet<SystemFile>();
            this.SystemFiles1 = new HashSet<SystemFile>();
            this.UserFeedbackDiscussions = new HashSet<UserFeedbackDiscussion>();
            this.UserFeedbacks = new HashSet<UserFeedback>();
            this.Emails = new HashSet<Email>();
            this.FeePaymentLogs = new HashSet<FeePaymentLog>();
            this.IntegratedServiceLogs = new HashSet<IntegratedServiceLog>();
            this.UsageHistories = new HashSet<UsageHistory>();
            this.UserLoginHistory1 = new HashSet<UserLoginHistory>();
            this.WebExceptions = new HashSet<WebException>();
        }
    
        public int UserLoginHistoryID { get; set; }
        public System.Guid LoginSessionGuid { get; set; }
        public short UserType { get; set; }
        public byte SubUserType { get; set; }
        public Nullable<int> UserID { get; set; }
        public Nullable<int> UserRoleID { get; set; }
        public Nullable<int> CandidateID { get; set; }
        public Nullable<int> StudentID { get; set; }
        public Nullable<int> FacultyMemberID { get; set; }
        public Nullable<int> IntegratedServiceUserID { get; set; }
        public string UserName { get; set; }
        public string IPAddress { get; set; }
        public string UserAgent { get; set; }
        public byte LoginStatus { get; set; }
        public System.DateTime LoginDate { get; set; }
        public Nullable<byte> LoginFailureReason { get; set; }
        public Nullable<System.DateTime> LogOffDate { get; set; }
        public Nullable<byte> LogOffReason { get; set; }
        public Nullable<int> ImpersonatedByUserLoginHistoryID { get; set; }
    
        /// <summary>
        /// <para>Foreign Key Name: FK_AccountTransactions_UserLoginHistory_UserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[AccountTransactions]</para>
        /// <para>Foreign Key Base Columns: [UserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<AccountTransaction> AccountTransactions { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_AdmissionOpenProgramExamMarksPolicies_UserLoginHistory_VerifiedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[AdmissionOpenProgramExamMarksPolicies]</para>
        /// <para>Foreign Key Base Columns: [VerifiedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<AdmissionOpenProgramExamMarksPolicy> AdmissionOpenProgramExamMarksPolicies { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_UserLoginHistory_Candidates_CandidateID</para>
        /// <para>Foreign Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Foreign Key Base Columns: [CandidateID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Candidates]</para>
        /// <para>Primary/Unique Key Base Columns: [CandidateID]</para>
        /// </summary>
        public virtual Candidate Candidate { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_ComplaintComments_UserLoginHistory_UserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[ComplaintComments]</para>
        /// <para>Foreign Key Base Columns: [UserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<ComplaintComment> ComplaintComments { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_Courses_UserLoginHistory_StatusChangedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[Courses]</para>
        /// <para>Foreign Key Base Columns: [StatusChangedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<Cours> Courses { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_Courses_UserLoginHistory_SubstitutedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[Courses]</para>
        /// <para>Foreign Key Base Columns: [SubstitutedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<Cours> Courses1 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_FacultyMemberPinCodes_UserLoginHistory_UserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[FacultyMemberPinCodes]</para>
        /// <para>Foreign Key Base Columns: [UserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<FacultyMemberPinCode> FacultyMemberPinCodes { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_UserLoginHistory_FacultyMembers_FacultyMemberID</para>
        /// <para>Foreign Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Foreign Key Base Columns: [FacultyMemberID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[FacultyMembers]</para>
        /// <para>Primary/Unique Key Base Columns: [FacultyMemberID]</para>
        /// </summary>
        public virtual FacultyMember FacultyMember { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_FormStudentClearanceDepartmentStatuses_UserLoginHistory_UserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[FormStudentClearanceDepartmentStatuses]</para>
        /// <para>Foreign Key Base Columns: [UserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<FormStudentClearanceDepartmentStatus> FormStudentClearanceDepartmentStatuses { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_FormStudentClearances_UserLoginHistory_StudentClearanceSSCStatusChangedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[FormStudentClearances]</para>
        /// <para>Foreign Key Base Columns: [StudentClearanceSSCStatusChangedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<FormStudentClearance> FormStudentClearances { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_UserLoginHistory_IntegratedServiceUsers_IntegratedServiceUserID</para>
        /// <para>Foreign Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Foreign Key Base Columns: [IntegratedServiceUserID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[IntegratedServiceUsers]</para>
        /// <para>Primary/Unique Key Base Columns: [IntegratedServiceUserID]</para>
        /// </summary>
        public virtual IntegratedServiceUser IntegratedServiceUser { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_OfferedCourses_UserLoginHistory_SubmittedToCampusByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[OfferedCourses]</para>
        /// <para>Foreign Key Base Columns: [SubmittedToCampusByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<OfferedCours> OfferedCourses { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_OfferedCourses_UserLoginHistory_SubmittedToHODByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[OfferedCourses]</para>
        /// <para>Foreign Key Base Columns: [SubmittedToHODByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<OfferedCours> OfferedCourses1 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_OfferedCourses_UserLoginHistory_SubmittedToUniversityByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[OfferedCourses]</para>
        /// <para>Foreign Key Base Columns: [SubmittedToUniversityByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<OfferedCours> OfferedCourses2 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_OnlineTeachingReadinesses_UserLoginHistory_SubmittedByClusterHeadUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[OnlineTeachingReadinesses]</para>
        /// <para>Foreign Key Base Columns: [SubmittedByClusterHeadUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<OnlineTeachingReadiness> OnlineTeachingReadinesses { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_OnlineTeachingReadinesses_UserLoginHistory_SubmittedByDGUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[OnlineTeachingReadinesses]</para>
        /// <para>Foreign Key Base Columns: [SubmittedByDGUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<OnlineTeachingReadiness> OnlineTeachingReadinesses1 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_OnlineTeachingReadinesses_UserLoginHistory_SubmittedByDirectorUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[OnlineTeachingReadinesses]</para>
        /// <para>Foreign Key Base Columns: [SubmittedByDirectorUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<OnlineTeachingReadiness> OnlineTeachingReadinesses2 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_OnlineTeachingReadinesses_UserLoginHistory_SubmittedByHODUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[OnlineTeachingReadinesses]</para>
        /// <para>Foreign Key Base Columns: [SubmittedByHODUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<OnlineTeachingReadiness> OnlineTeachingReadinesses3 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_PasswordChangeHistories_UserLoginHistory_UserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[PasswordChangeHistories]</para>
        /// <para>Foreign Key Base Columns: [UserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<PasswordChangeHistory> PasswordChangeHistories { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_ProjectThesisInternships_UserLoginHistory_SubmittedToCampusByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[ProjectThesisInternships]</para>
        /// <para>Foreign Key Base Columns: [SubmittedToCampusByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<ProjectThesisInternship> ProjectThesisInternships { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_ProjectThesisInternships_UserLoginHistory_SubmittedToHODByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[ProjectThesisInternships]</para>
        /// <para>Foreign Key Base Columns: [SubmittedToHODByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<ProjectThesisInternship> ProjectThesisInternships1 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_ProjectThesisInternships_UserLoginHistory_SubmittedToUniversityByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[ProjectThesisInternships]</para>
        /// <para>Foreign Key Base Columns: [SubmittedToUniversityByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<ProjectThesisInternship> ProjectThesisInternships2 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_QASummaryFacultyEvaluations_UserLoginHistory_DGRemarksUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[QASummaryFacultyEvaluations]</para>
        /// <para>Foreign Key Base Columns: [DGRemarksUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<QASummaryFacultyEvaluation> QASummaryFacultyEvaluations { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_QASummaryFacultyEvaluations_UserLoginHistory_DirectorRemarksUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[QASummaryFacultyEvaluations]</para>
        /// <para>Foreign Key Base Columns: [DirectorRemarksUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<QASummaryFacultyEvaluation> QASummaryFacultyEvaluations1 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_QASummaryFacultyEvaluations_UserLoginHistory_HODRemarksUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[QASummaryFacultyEvaluations]</para>
        /// <para>Foreign Key Base Columns: [HODRemarksUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<QASummaryFacultyEvaluation> QASummaryFacultyEvaluations2 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_QASurveySummaryAnalysi_UserLoginHistory_SubmittedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[QASurveySummaryAnalysi]</para>
        /// <para>Foreign Key Base Columns: [SubmittedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<QASurveySummaryAnalysi> QASurveySummaryAnalysis { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_RegisteredCourses_UserLoginHistory_DeletedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[RegisteredCourses]</para>
        /// <para>Foreign Key Base Columns: [DeletedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<RegisteredCours> RegisteredCourses { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_RegisteredCourses_UserLoginHistory_RegisteredByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[RegisteredCourses]</para>
        /// <para>Foreign Key Base Columns: [RegisteredByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<RegisteredCours> RegisteredCourses1 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_StudentAcademicRecords_UserLoginHistory_VerifiedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[StudentAcademicRecords]</para>
        /// <para>Foreign Key Base Columns: [VerifiedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<StudentAcademicRecord> StudentAcademicRecords { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_StudentCustomInfos_UserLoginHistory_CreatedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[StudentCustomInfos]</para>
        /// <para>Foreign Key Base Columns: [CreatedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<StudentCustomInfo> StudentCustomInfos { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_StudentFees_UserLoginHistory_CreatedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[StudentFees]</para>
        /// <para>Foreign Key Base Columns: [CreatedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<StudentFee> StudentFees { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_StudentLibraryDefaulters_UserLoginHistory_LastUpdatedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[StudentLibraryDefaulters]</para>
        /// <para>Foreign Key Base Columns: [LastUpdatedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<StudentLibraryDefaulter> StudentLibraryDefaulters { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_Students_UserLoginHistory_StudentDocumentsLastUpdatedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[Students]</para>
        /// <para>Foreign Key Base Columns: [StudentDocumentsLastUpdatedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<Student> Students { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_UserLoginHistory_Students_StudentID</para>
        /// <para>Foreign Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Foreign Key Base Columns: [StudentID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Students]</para>
        /// <para>Primary/Unique Key Base Columns: [StudentID]</para>
        /// </summary>
        public virtual Student Student { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_SystemFiles_UserLoginHistory_CreatedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[SystemFiles]</para>
        /// <para>Foreign Key Base Columns: [CreatedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<SystemFile> SystemFiles { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_SystemFiles_UserLoginHistory_DeletedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[SystemFiles]</para>
        /// <para>Foreign Key Base Columns: [DeletedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<SystemFile> SystemFiles1 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_UserFeedbackDiscussions_UserLoginHistory_CreatedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[UserFeedbackDiscussions]</para>
        /// <para>Foreign Key Base Columns: [CreatedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<UserFeedbackDiscussion> UserFeedbackDiscussions { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_UserFeedbacks_UserLoginHistory_UserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[UserFeedbacks]</para>
        /// <para>Foreign Key Base Columns: [UserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<UserFeedback> UserFeedbacks { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_Emails_UserLoginHistory_CreatedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [logs].[Emails]</para>
        /// <para>Foreign Key Base Columns: [CreatedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<Email> Emails { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_FeePaymentLogs_UserLoginHistory_UserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [logs].[FeePaymentLogs]</para>
        /// <para>Foreign Key Base Columns: [UserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<FeePaymentLog> FeePaymentLogs { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_IntegratedServiceLogs_UserLoginHistory_UserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [logs].[IntegratedServiceLogs]</para>
        /// <para>Foreign Key Base Columns: [UserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<IntegratedServiceLog> IntegratedServiceLogs { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_UsageHistory_UserLoginHistory_UserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [logs].[UsageHistory]</para>
        /// <para>Foreign Key Base Columns: [UserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<UsageHistory> UsageHistories { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_UserLoginHistory_UserLoginHistory_ImpersonatedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Foreign Key Base Columns: [ImpersonatedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<UserLoginHistory> UserLoginHistory1 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_UserLoginHistory_UserLoginHistory_ImpersonatedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Foreign Key Base Columns: [ImpersonatedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual UserLoginHistory UserLoginHistory2 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_UserLoginHistory_UserRoles_UserRoleID</para>
        /// <para>Foreign Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Foreign Key Base Columns: [UserRoleID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserRoles]</para>
        /// <para>Primary/Unique Key Base Columns: [UserRoleID]</para>
        /// </summary>
        public virtual UserRole UserRole { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_UserLoginHistory_Users_UserID</para>
        /// <para>Foreign Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Foreign Key Base Columns: [UserID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Users]</para>
        /// <para>Primary/Unique Key Base Columns: [UserID]</para>
        /// </summary>
        public virtual User User { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_WebExceptions_UserLoginHistory_UserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [logs].[WebExceptions]</para>
        /// <para>Foreign Key Base Columns: [UserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual ICollection<WebException> WebExceptions { get; set; }
    }
}
