//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Model.Entities
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// todo: Description is missing for [dbo].[ViewActiveExecutiveUserLogins] table.
    /// </summary>
    public partial class ViewActiveExecutiveUserLogin
    {
        public int UserLoginHistoryID { get; set; }
        public System.Guid LoginSessionGuid { get; set; }
        public short UserType { get; set; }
        public Nullable<int> UserID { get; set; }
        public Nullable<int> UserRoleID { get; set; }
        public Nullable<int> InstituteID { get; set; }
        public Nullable<int> UserGroupID { get; set; }
    }
}
