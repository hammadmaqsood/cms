//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Model.Entities
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// todo: Description is missing for [dbo].[StudentCommunityServices] table.
    /// </summary>
    public partial class StudentCommunityService
    {
        public int StudentCommunityServiceID { get; set; }
        public int StudentID { get; set; }
        public string Organization { get; set; }
        public string JobTitle { get; set; }
        public System.DateTime CompletedDate { get; set; }
        public short SemesterID { get; set; }
        public byte Hours { get; set; }
    
        /// <summary>
        /// <para>Foreign Key Name: FK_StudentCommunityServices_Semesters_SemesterID</para>
        /// <para>Foreign Key Base Table: [dbo].[StudentCommunityServices]</para>
        /// <para>Foreign Key Base Columns: [SemesterID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Semesters]</para>
        /// <para>Primary/Unique Key Base Columns: [SemesterID]</para>
        /// </summary>
        public virtual Semester Semester { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_StudentCommunityServices_Students_StudentID</para>
        /// <para>Foreign Key Base Table: [dbo].[StudentCommunityServices]</para>
        /// <para>Foreign Key Base Columns: [StudentID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Students]</para>
        /// <para>Primary/Unique Key Base Columns: [StudentID]</para>
        /// </summary>
        public virtual Student Student { get; set; }
    }
}
