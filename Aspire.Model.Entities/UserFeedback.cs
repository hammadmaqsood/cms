//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Model.Entities
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// todo: Description is missing for [dbo].[UserFeedbacks] table.
    /// </summary>
    public partial class UserFeedback
    {
        public UserFeedback()
        {
            this.UserFeedbackAssignees = new HashSet<UserFeedbackAssignee>();
            this.UserFeedbackDiscussions = new HashSet<UserFeedbackDiscussion>();
        }
    
        public System.Guid UserFeedbackID { get; set; }
        public int UserLoginHistoryID { get; set; }
        public string FeedbackNo { get; set; }
        public System.Guid FeedbackType { get; set; }
        public string FeedbackDetails { get; set; }
        public System.DateTime FeedbackDate { get; set; }
        public System.Guid Status { get; set; }
        public string Notes { get; set; }
        public bool Archived { get; set; }
        public System.DateTime LastUpdatedDate { get; set; }
        public bool Unread { get; set; }
        public Nullable<System.Guid> MovedFromFeedbackBoxType { get; set; }
        public System.Guid FeedbackBoxType { get; set; }
    
        /// <summary>
        /// <para>Foreign Key Name: FK_UserFeedbackAssignees_UserFeedbacks_UserFeedbackID</para>
        /// <para>Foreign Key Base Table: [dbo].[UserFeedbackAssignees]</para>
        /// <para>Foreign Key Base Columns: [UserFeedbackID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserFeedbacks]</para>
        /// <para>Primary/Unique Key Base Columns: [UserFeedbackID]</para>
        /// </summary>
        public virtual ICollection<UserFeedbackAssignee> UserFeedbackAssignees { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_UserFeedbackDiscussions_UserFeedbacks_UserFeedbackID</para>
        /// <para>Foreign Key Base Table: [dbo].[UserFeedbackDiscussions]</para>
        /// <para>Foreign Key Base Columns: [UserFeedbackID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserFeedbacks]</para>
        /// <para>Primary/Unique Key Base Columns: [UserFeedbackID]</para>
        /// </summary>
        public virtual ICollection<UserFeedbackDiscussion> UserFeedbackDiscussions { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_UserFeedbacks_UserLoginHistory_UserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[UserFeedbacks]</para>
        /// <para>Foreign Key Base Columns: [UserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual UserLoginHistory UserLoginHistory { get; set; }
    }
}
