//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Model.Entities
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// todo: Description is missing for [dbo].[StudentCreditsTransfers] table.
    /// </summary>
    public partial class StudentCreditsTransfer
    {
        public StudentCreditsTransfer()
        {
            this.StudentCreditsTransferredCourses = new HashSet<StudentCreditsTransferredCours>();
        }
    
        public int StudentCreditsTransferID { get; set; }
        public int StudentID { get; set; }
        public string InstituteName { get; set; }
        public System.DateTime TransferredDate { get; set; }
        public short TransferredSemesterID { get; set; }
        public string CommitteeMembers { get; set; }
        public string Remarks { get; set; }
    
        /// <summary>
        /// <para>Foreign Key Name: FK_StudentCreditsTransferredCourses_StudentCreditsTransfers_StudentCreditsTransferID</para>
        /// <para>Foreign Key Base Table: [dbo].[StudentCreditsTransferredCourses]</para>
        /// <para>Foreign Key Base Columns: [StudentCreditsTransferID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[StudentCreditsTransfers]</para>
        /// <para>Primary/Unique Key Base Columns: [StudentCreditsTransferID]</para>
        /// </summary>
        public virtual ICollection<StudentCreditsTransferredCours> StudentCreditsTransferredCourses { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_StudentCreditsTransfers_Students_StudentID</para>
        /// <para>Foreign Key Base Table: [dbo].[StudentCreditsTransfers]</para>
        /// <para>Foreign Key Base Columns: [StudentID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Students]</para>
        /// <para>Primary/Unique Key Base Columns: [StudentID]</para>
        /// </summary>
        public virtual Student Student { get; set; }
    }
}
