//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Model.Entities
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// todo: Description is missing for [dbo].[ComplaintComments] table.
    /// </summary>
    public partial class ComplaintComment
    {
        public System.Guid ComplaintCommentID { get; set; }
        public System.Guid ComplaintID { get; set; }
        public int UserLoginHistoryID { get; set; }
        public System.DateTime Date { get; set; }
        public string Text { get; set; }
        public System.Guid Type { get; set; }
        public Nullable<System.Guid> Status { get; set; }
    
        /// <summary>
        /// <para>Foreign Key Name: FK_ComplaintComments_Complaints_ComplaintID</para>
        /// <para>Foreign Key Base Table: [dbo].[ComplaintComments]</para>
        /// <para>Foreign Key Base Columns: [ComplaintID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Complaints]</para>
        /// <para>Primary/Unique Key Base Columns: [ComplaintID]</para>
        /// </summary>
        public virtual Complaint Complaint { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_ComplaintComments_UserLoginHistory_UserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[ComplaintComments]</para>
        /// <para>Foreign Key Base Columns: [UserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual UserLoginHistory UserLoginHistory { get; set; }
    }
}
