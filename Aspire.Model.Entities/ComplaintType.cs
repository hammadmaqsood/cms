//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Model.Entities
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// todo: Description is missing for [dbo].[ComplaintTypes] table.
    /// </summary>
    public partial class ComplaintType
    {
        public ComplaintType()
        {
            this.Complaints = new HashSet<Complaint>();
        }
    
        public System.Guid ComplaintTypeID { get; set; }
        public int InstituteID { get; set; }
        public string Name { get; set; }
    
        /// <summary>
        /// <para>Foreign Key Name: FK_Complaints_ComplaintTypes_ComplaintTypeID</para>
        /// <para>Foreign Key Base Table: [dbo].[Complaints]</para>
        /// <para>Foreign Key Base Columns: [ComplaintTypeID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[ComplaintTypes]</para>
        /// <para>Primary/Unique Key Base Columns: [ComplaintTypeID]</para>
        /// </summary>
        public virtual ICollection<Complaint> Complaints { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_ComplaintTypes_Institutes_InstituteID</para>
        /// <para>Foreign Key Base Table: [dbo].[ComplaintTypes]</para>
        /// <para>Foreign Key Base Columns: [InstituteID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Institutes]</para>
        /// <para>Primary/Unique Key Base Columns: [InstituteID]</para>
        /// </summary>
        public virtual Institute Institute { get; set; }
    }
}
