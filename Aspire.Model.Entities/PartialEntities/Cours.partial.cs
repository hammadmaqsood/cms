﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class Cours
	{
		[EnumGuids]
		public enum Statuses
		{
			[EnumGuidValue("0CE34F9C-E9F3-4BDC-B587-6DE7949E3304", "Pending")]
			Pending,
			[EnumGuidValue("B79CA228-1BC4-4D5A-9139-63E91173286E", "Rejected")]
			Rejected,
			[EnumGuidValue("F08D3844-22E6-4172-8FA2-499658B9AD67", "Verified")]
			Verified,
		}

		public SemesterNos SemesterNoEnum
		{
			get => (SemesterNos)this.SemesterNo;
			set => this.SemesterNo = (short)value;
		}

		public CourseCategories CourseCategoryEnum
		{
			get => (CourseCategories)this.CourseCategory;
			set => this.CourseCategory = (byte)value;
		}

		public CourseTypes CourseTypeEnum
		{
			get => (CourseTypes)this.CourseType;
			set => this.CourseType = (byte)value;
		}

		public Statuses StatusEnum
		{
			get => this.Status.GetEnum<Statuses>();
			set => this.Status = value.GetGuid();
		}

		public string StatusFullName => this.Status.GetFullName();

		public Cours Validate()
		{
			this.CourseCode = this.CourseCode.TrimAndCannotEmptyAndMustBeUpperCase();
			this.Title = this.Title.TrimAndCannotBeEmpty();
			this.Remarks = this.Remarks.TrimAndMakeItNullIfEmpty();
			this.Status.ValidateEnum<Statuses>();
			return this;
		}
	}
}
