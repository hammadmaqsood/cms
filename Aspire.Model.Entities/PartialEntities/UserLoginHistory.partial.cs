﻿using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Model.Entities
{
	public partial class UserLoginHistory
	{
		public enum LoginStatuses : byte
		{
			Success = 1,
			Failure = 2,
		}

		public LoginStatuses LoginStatusEnum
		{
			get => (LoginStatuses)this.LoginStatus;
			set => this.LoginStatus = (byte)value;
		}

		public enum LoginFailureReasons : byte
		{
			AccountExpired = 1,
			AccountIsInactive = 2,
			CandidateLoginNotAllowedInSemester = 3,
			InvalidPassword = 4,
			IPAddressNotAuthorized = 5,
			NoRoleFound = 6,
			UsernameNotFound = 7,
			UserRoleIsInactive = 8,
			StudentStatusAdmissionCanceled = 9,
			StudentStatusBlocked = 10,
			StudentStatusDeferred = 11,
			StudentStatusDropped = 12,
			StudentStatusExpelled = 13,
			StudentStatusGraduated = 14,
			StudentStatusLeft = 15,
			StudentStatusProgramChanged = 16,
			StudentStatusRusticated = 17,
			StudentStatusTransferredToOtherCampus = 18,
		}

		public LoginFailureReasons? LoginFailureReasonEnum
		{
			get => (LoginFailureReasons?)this.LoginFailureReason;
			set => this.LoginFailureReason = (byte?)value;
		}

		public enum LogOffReasons : byte
		{
			ChangeRole = 1,
			LogOff = 2,
			SessionTimeout = 3,
			IPAddressChanged = 4,
			UserAgentChanged = 5,
		}

		public LogOffReasons? LogOffReasonEnum
		{
			get => (LogOffReasons?)this.LogOffReason;
			set => this.LogOffReason = (byte?)value;
		}

		public UserTypes UserTypeEnum
		{
			get => (UserTypes)this.UserType;
			set => this.UserType = (short)value;
		}

		public SubUserTypes SubUserTypeEnum
		{
			get => (SubUserTypes)this.SubUserType;
			set => this.SubUserType = (byte)value;
		}

		[Obsolete]
		public static readonly int SystemUserLoginHistoryID = 1;
		public static readonly Guid SystemLoginSessionGuid = Guid.Empty;

		public void Validate()
		{
			if (string.IsNullOrWhiteSpace(this.UserName))
				throw new InvalidOperationException();
			if (string.IsNullOrWhiteSpace(this.IPAddress) && this.UserTypeEnum != UserTypes.IntegratedService)
				throw new InvalidOperationException();
			if (this.UserTypeEnum == UserTypes.System)
				throw new InvalidOperationException();
			switch (this.LoginStatusEnum)
			{
				case LoginStatuses.Success:
					if (this.UserID == null && this.CandidateID == null && this.StudentID == null && this.FacultyMemberID == null && this.IntegratedServiceUserID == null)
						throw new InvalidOperationException();
					if (this.UserID != null && this.UserRoleID == null)
						throw new InvalidOperationException();

					if (this.LogOffDate != null && this.LogOffReason == null)
						throw new InvalidOperationException();
					if (this.LogOffDate == null && this.LogOffReason != null)
						throw new InvalidOperationException();
					if (this.LogOffDate != null && this.LoginDate > this.LogOffDate.Value)
						throw new InvalidOperationException();
					break;
				case LoginStatuses.Failure:
					if (this.LoginFailureReason == null)
						throw new InvalidOperationException();
					break;
				default:
					throw new NotImplementedEnumException(this.LoginStatusEnum);
			}
		}
	}
}
