﻿using System;

namespace Aspire.Model.Entities
{
	public partial class Scholarship
	{
		public enum ScholarshipTypes : byte
		{
			QarzeHasana = 1,
			StudentStudyLoan = 2,
		}

		public ScholarshipTypes ScholarshipTypeEnum
		{
			get => (ScholarshipTypes)this.ScholarshipType;
			set => this.ScholarshipType = (byte)value;
		}

		public enum ValidationResults
		{
			None,
			NoRecordFound,
			AlreadyExists,
			InvalidFromDate,
			InvalidDateRange,
			Success,
		}

		public enum Statuses : byte
		{
			Active = 1,
			Inactive = 2,
		}

		public static ValidationResults Validate(DateTime fromDate, DateTime toDate)
		{
			if (fromDate.Date < DateTime.Today)
				return ValidationResults.InvalidFromDate;
			if (fromDate.Date > toDate.Date)
				return ValidationResults.InvalidDateRange;
			return ValidationResults.Success;
		}
	}
}
