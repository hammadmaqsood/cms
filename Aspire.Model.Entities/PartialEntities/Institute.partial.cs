﻿using Aspire.Lib.Extensions;

namespace Aspire.Model.Entities
{
	public partial class Institute
	{
		public enum Statuses : byte
		{
			Active = 1,
			Inactive = 2,
		}

		public static readonly byte StatusActive = (byte)Statuses.Active;

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}

		public Institute Validate()
		{
			this.InstituteName = this.InstituteName.TrimAndCannotBeEmpty();
			this.InstituteShortName = this.InstituteShortName.TrimAndCannotBeEmpty();
			this.InstituteAlias = this.InstituteAlias.TrimAndCannotEmptyAndMustBeUpperCase();
			this.InstituteCode = this.InstituteCode.TrimAndCannotBeEmpty().MustMatch(@"^\d{2}$");
			return this;
		}
	}
}
