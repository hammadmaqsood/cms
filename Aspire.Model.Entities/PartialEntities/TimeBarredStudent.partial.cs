﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class TimeBarredStudent
	{
		public CourseCategories CourseCategoriesAllowedEnum
		{
			get { return (CourseCategories)this.CourseCategoriesAllowed; }
			set { this.CourseCategoriesAllowed = (byte)value; }
		}

		public string CourseCategoriesAllowedFullNames => this.CourseCategoriesAllowedEnum.ToFullNames();

		public string MinSemesterString => this.MinSemesterID.ToSemesterString();
		public string MaxSemesterString => this.MaxSemesterID.ToSemesterString();
	}
}
