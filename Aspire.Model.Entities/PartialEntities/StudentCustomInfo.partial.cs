﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class StudentCustomInfo
	{
		[EnumGuids]
		public enum Types
		{
			[EnumGuidValue("ccaa6f14-939d-4cac-85fb-96ee5102e74a", "Other")]
			Other,
		}

		public Types? TypeEnum
		{
			get => this.Type?.GetEnum<Types>();
			set => this.Type = value?.GetGuid();
		}
	}
}
