﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Model.Entities
{
	public partial class InstituteBankAccount
	{
		public enum FeeTypes : byte
		{
			AdmissionProcessingFee = 1,
			NewAdmissionFee = 2,
			StudentFee = 3,
		}

		public FeeTypes FeeTypeEnum
		{
			get => (FeeTypes)this.FeeType;
			set => this.FeeType = (byte)value;
		}

		public string FeeTypeFullName => this.FeeTypeEnum.ToFullName();

		[Flags]
		public enum AccountTypes : byte
		{
			Manual = 1,
			Online = 2,
			ManualOnline = Manual | Online
		}

		public AccountTypes AccountTypeEnum
		{
			get => (AccountTypes)this.AccountType;
			set => this.AccountType = (byte)value;
		}

		public InstituteBankAccount Validate()
		{
			this.AccountTitle = this.AccountTitle.TrimAndCannotBeEmpty();
			this.AccountTitleForDisplay = this.AccountTitleForDisplay.TrimAndMakeItNullIfEmpty();
			this.AccountNo = this.AccountNo.TrimAndMakeItNullIfEmpty();
			this.AccountNoForDisplay = this.AccountNoForDisplay.TrimAndMakeItNullIfEmpty();
			this.BranchCode = this.BranchCode.TrimAndMakeItNullIfEmpty();
			this.BranchName = this.BranchName.TrimAndMakeItNullIfEmpty();
			this.BranchAddress = this.BranchAddress.TrimAndMakeItNullIfEmpty();
			this.BranchForDisplay = this.BranchForDisplay.TrimAndMakeItNullIfEmpty();
			return this;
		}
	}
}
