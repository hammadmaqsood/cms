﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class UserFeedbackMember
	{
		[EnumGuids]
		public enum Statuses
		{
			[EnumGuidValue("f2322d1c-6f5b-4bd0-8fa9-02744a07b836", "Active")]
			Active,
			[EnumGuidValue("25c12b2b-90a1-4bd6-bc8d-4f3b222b7782", "Inactive")]
			Inactive
		}

		public Statuses StatusEnum
		{
			get => this.Status.GetEnum<Statuses>();
			set => this.Status = value.GetGuid();
		}
	}
}