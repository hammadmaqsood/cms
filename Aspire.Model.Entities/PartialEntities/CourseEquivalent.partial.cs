﻿using Aspire.Lib.Extensions;

namespace Aspire.Model.Entities
{
	public partial class CourseEquivalent
	{
		public CourseEquivalent Validate()
		{
			this.Remarks = this.Remarks.TrimAndMakeItNullIfEmpty();
			return this;
		}
	}
}
