﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class CandidateForeignDocument
	{
		[EnumGuids]
		public enum DocumentTypes
		{
			[EnumGuidValue("5C8054EF-47E6-466C-9854-27CC1A009FD7", "Passport")]
			Passport,
			[EnumGuidValue("3C432BCD-600F-4E0A-B17C-04EE19E7FF31", "Statement of Purpose")]
			StatementOfPurpose,
			[EnumGuidValue("6BA795C8-F84E-4E66-B4E0-05B3E2CCCE33", "Research Plan (For PHD Only)")]
			ResearchPlan,
			[EnumGuidValue("03190E6C-A9B4-4AFB-AAA7-11DFA18DA4E7", "English Language Certificate")]
			EnglishLanguageCertificate
		}

		public DocumentTypes DocumentTypeEnum
		{
			get => this.DocumentType.GetEnum<DocumentTypes>();
			set => this.DocumentType = value.GetGuid();
		}
	}
}
