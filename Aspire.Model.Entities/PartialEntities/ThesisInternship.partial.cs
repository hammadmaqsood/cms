﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.Model.Entities
{
	public partial class ThesisInternship
	{
		public enum RecordTypes : byte
		{
			Internship = 1,
			Thesis = 2,
		}

		public RecordTypes RecordTypeEnum
		{
			get => (RecordTypes)this.RecordType;
			set => this.RecordType = (byte)value;
		}

		public string RecordTypeFullName => this.RecordTypeEnum.ToString();

		public string Semester => this.SemesterID.ToSemesterString();

		public ExamGrades GradeEnum
		{
			get => (ExamGrades)this.Grade;
			set => this.Grade = (byte)value;
		}

		public string GradeFullName => this.GradeEnum.ToFullName();

		public ThesisInternship Validate(ExamMarksPolicy examMarksPolicy)
		{
			this.Title = this.Title.TrimAndCannotBeEmpty();
			switch (this.RecordTypeEnum)
			{
				case RecordTypes.Internship:
					this.Organization = this.Organization.TrimAndCannotBeEmpty();
					this.FromDate = this.FromDate.CannotBeNull();
					this.ToDate = this.ToDate.CannotBeNull();
					break;
				case RecordTypes.Thesis:
					this.Organization = null;
					this.FromDate = null;
					this.ToDate = null;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			this.DefenceDate = this.DefenceDate.CannotBeNull();
			this.SubmissionDate = this.SubmissionDate.CannotBeNull();
			this.Remarks = this.Remarks.TrimAndMakeItNullIfEmpty();
			if (this.ThesisInternshipMarks.Any(m => m.ObtainedMarks > m.TotalMarks))
				throw new InvalidOperationException("ObtainedMarks must be less than or equal to TotalMarks.");
			if (!this.ThesisInternshipMarks.Any())
				throw new InvalidOperationException("No ThesisInternshipMarks details found.");

			var obtained = this.ThesisInternshipMarks.Sum(t => t.ObtainedMarks);
			var total = this.ThesisInternshipMarks.Sum(t => t.TotalMarks);
			this.Total = (byte)Math.Round(obtained * 100 / total, 0, MidpointRounding.AwayFromZero);

			var (examGradeEnum, gradePoints) = examMarksPolicy.GetExamGradeAndGradePoints((byte)this.Total, false);

			this.GradeEnum = examGradeEnum;
			this.GradePoint = gradePoints * this.CreditHours;
			return this;
		}
	}
}
