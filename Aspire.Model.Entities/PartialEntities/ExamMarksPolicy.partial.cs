﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Model.Entities
{
	public partial class ExamMarksPolicy
	{
		public enum MarksEntryTypes
		{
			AssignmentsQuizzesMidFinal = 1,
			AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal = 2,
			AssignmentQuizCompileInternalsMidFinal = 3,
			Final = 4,
		}

		public MarksEntryTypes CourseMarksEntryTypeEnum
		{
			get => (MarksEntryTypes)this.CourseMarksEntryType;
			set => this.CourseMarksEntryType = (byte)value;
		}

		public MarksEntryTypes LabMarksEntryTypeEnum
		{
			get => (MarksEntryTypes)this.LabMarksEntryType;
			set => this.LabMarksEntryType = (byte)value;
		}

		public ExamGrades? SummerCappingExamGradeEnum
		{
			get => (ExamGrades?)this.SummerCappingExamGrade;
			set => this.SummerCappingExamGrade = (byte?)value;
		}

		public string Validity
		{
			get
			{
				if (this.ValidUpToSemesterID == null)
					return $"{this.ValidFromSemesterID.ToSemesterString()} onward";
				return $"{this.ValidFromSemesterID.ToSemesterString()} - {this.ValidUpToSemesterID.ToSemesterString()}";
			}
		}

		private static string MarksMustBeNotNull(string marksName, decimal? marks)
		{
			if (marks == null)
				return $"{marksName} can not be null.";
			if (marks < 1)
				return $"{marksName} can not be less than 1.";
			if (marks > 100)
				return $"{marksName} can not be greater than 100.";
			return null;
		}

		private static string MarksMustBeNull(string marksName, decimal? marks)
		{
			return marks != null ? $"{marksName} must be null." : null;
		}

		private static string ValidateMarksEntryType(MarksEntryTypes marksEntryTypeEnum, CourseCategories courseCategory, decimal? assignments, decimal? quizzes, decimal? internals, decimal? mid, decimal? final)
		{
			string errorMessage;
			switch (marksEntryTypeEnum)
			{
				case MarksEntryTypes.AssignmentsQuizzesMidFinal:
				case MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
					errorMessage = MarksMustBeNotNull($"{courseCategory.ToFullName()} Assignments", assignments)
									?? MarksMustBeNotNull($"{courseCategory.ToFullName()} Quizzes", quizzes)
									?? MarksMustBeNull($"{courseCategory.ToFullName()} Internals", internals)
									?? MarksMustBeNotNull($"{courseCategory.ToFullName()} Mid", mid)
									?? MarksMustBeNotNull($"{courseCategory.ToFullName()} Final", final);
					break;
				case MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					errorMessage = MarksMustBeNull($"{courseCategory.ToFullName()} Assignments", assignments)
							?? MarksMustBeNull($"{courseCategory.ToFullName()} Quizzes", quizzes)
							?? MarksMustBeNotNull($"{courseCategory.ToFullName()} Internals", internals)
							?? MarksMustBeNotNull($"{courseCategory.ToFullName()} Mid", mid)
							?? MarksMustBeNotNull($"{courseCategory.ToFullName()} Final", final);
					break;
				case MarksEntryTypes.Final:
					errorMessage = MarksMustBeNull($"{courseCategory.ToFullName()} Assignments", assignments)
							?? MarksMustBeNull($"{courseCategory.ToFullName()} Quizzes", quizzes)
							?? MarksMustBeNull($"{courseCategory.ToFullName()} Internals", internals)
							?? MarksMustBeNull($"{courseCategory.ToFullName()} Mid", mid)
							?? MarksMustBeNotNull($"{courseCategory.ToFullName()} Final", final);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			if (errorMessage == null)
			{
				var total = (assignments ?? 0) + (quizzes ?? 0) + (internals ?? 0) + (mid ?? 0) + (final ?? 0);
				if (total != 100)
					return $"{courseCategory.ToFullName()} Marks Total must be 100.";
			}
			return errorMessage;
		}

		public string Validate()
		{
			if (this.ValidUpToSemesterID != null && this.ValidFromSemesterID > this.ValidUpToSemesterID.Value)
				return "Valid From Semester must be less than or equal to Valid Up To Semester.";

			var grades = this.Grades.Where(g => g?.Min != null || g?.Max != null || g?.GradePoint != null).ToList();
			var gradeErrors = grades.Where(g => !(g != null && g.Value.Min != null && g.Value.Max != null && g.Value.Min.Value <= g.Value.Max.Value && 0 <= g.Value.Min && g.Value.Max <= 100 && g.Value.GradePoint != null && 0 <= g.Value.GradePoint && g.Value.GradePoint <= 4));

			var firstGradeError = gradeErrors.FirstOrDefault();
			if (firstGradeError != null)
				return $"Values for {firstGradeError.Value.Grade.ToFullName()} are not valid.";

			ExamGrades? lastGrade = null;
			for (byte marks = 0; marks <= 100; marks++)
			{
				var selectedGrades = grades.Where(g => g != null && g.Value.Min <= marks && marks <= g.Value.Max).ToList();
				if (selectedGrades.Count != 1)
					return $"Grade for Marks {marks} is missing or duplicated.";
				var grade = selectedGrades.SingleOrDefault()?.Grade;
				if (grade == null)
					throw new InvalidOperationException();
				if (lastGrade == null)
					lastGrade = grade;
				else
				{
					if (ExamGradeComparer.GreaterThan(lastGrade.Value, grade.Value))
						return "Grades must be assigned in sequence.";
					lastGrade = grade.Value;
				}
			}

			if (this.SummerCappingExamGradeEnum != null && !grades.Any(g => g != null && g.Value.Grade == this.SummerCappingExamGradeEnum))
				return $"Values for Summer Capping Grade {this.SummerCappingExamGradeEnum.Value.ToFullName()} must be defined.";

			return ValidateMarksEntryType(this.CourseMarksEntryTypeEnum, CourseCategories.Course, this.CourseAssignments, this.CourseQuizzes, this.CourseInternals, this.CourseMid, this.CourseFinal)
				?? ValidateMarksEntryType(this.LabMarksEntryTypeEnum, CourseCategories.Lab, this.LabAssignments, this.LabQuizzes, this.LabInternals, this.LabMid, this.LabFinal);
		}

		private IEnumerable<(byte? Min, byte? Max, ExamGrades Grade, decimal? GradePoint)?> Grades
			=> new List<(byte? Min, byte? Max, ExamGrades Grade, decimal? GradePoint)?>
			{
				(this.AMin, this.AMax, ExamGrades.A, this.AGradePoint),
				(this.AMinusMin, this.AMinusMax, ExamGrades.AMinus, this.AMinusGradePoint),
				(this.BPlusMin, this.BPlusMax, ExamGrades.BPlus, this.BPlusGradePoint),
				(this.BMin, this.BMax, ExamGrades.B, this.BGradePoint),
				(this.BMinusMin, this.BMinusMax, ExamGrades.BMinus, this.BMinusGradePoint),
				(this.CPlusMin, this.CPlusMax, ExamGrades.CPlus, this.CPlusGradePoint),
				(this.CMin, this.CMax, ExamGrades.C, this.CGradePoint),
				(this.CMinusMin, this.CMinusMax, ExamGrades.CMinus, this.CMinusGradePoint),
				(this.DPlusMin, this.DPlusMax, ExamGrades.DPlus, this.DPlusGradePoint),
				(this.DMin, this.DMax, ExamGrades.D, this.DGradePoint),
				((byte?) 0, this.FMax, ExamGrades.F, this.FGradePoint)
			};

		public bool IsExamGradeValidForUse(ExamGrades examGradeEnum)
		{
			return this.Grades.Any(g => g?.Min != null && g.Value.Max != null && g.Value.GradePoint != null && g.Value.Grade == examGradeEnum);
		}

		private (ExamGrades Grade, decimal GradePoint)? CappedGrade
		{
			get
			{
				if (this.SummerCappingExamGradeEnum == null)
					return null;
				var grade = this.Grades.SingleOrDefault(g => g?.Grade == this.SummerCappingExamGradeEnum.Value);
				if (grade == null)
					return null;
				return (grade.Value.Grade, grade.Value.GradePoint ?? throw new InvalidOperationException());
			}
		}

		public (ExamGrades ExamGradeEnum, decimal GradePoints) GetExamGradeAndGradePoints(byte total, bool applyGradeCapping)
		{
			var grade = this.Grades.Where(v => v != null).SingleOrDefault(v => v.Value.Min != null && v.Value.Max != null && v.Value.Min.Value <= total && total <= v.Value.Max.Value);
			if (grade == null)
				throw new InvalidOperationException($"Grade not found. Total: {total}, {nameof(this.ExamMarksPolicyID)}: {this.ExamMarksPolicyID}");
			if (applyGradeCapping)
			{
				var cappedGrade = this.CappedGrade;
				if (cappedGrade != null)
					if (ExamGradeComparer.GreaterThanOrEqualTo(grade.Value.Grade, cappedGrade.Value.Grade))
						return cappedGrade.Value;
			}
			return (grade.Value.Grade, grade.Value.GradePoint ?? throw new InvalidOperationException());
		}

		public string GetGradeRangesInHtml(bool cappedGrades)
		{
			IEnumerable<string> ranges;
			if (cappedGrades == false || this.SummerCappingExamGradeEnum == null)
				ranges = this.Grades
					.Select(g =>
					{
						if (g == null)
							return null;
						var min = (decimal?)g.Value.Min;
						var max = (decimal?)g.Value.Max;
						if (min == null || max == null)
							return null;
						return $"<B>{g.Value.Grade.ToFullName()}</B>: {min.Value.ToExamMarksString()}-{max.Value.ToExamMarksString()}";
					})
					.Where(s => s != null);
			else
			{
				ranges = this.Grades.SkipWhile(g => g?.Grade != this.SummerCappingExamGradeEnum.Value)
					.Select(g =>
					{
						if (g == null)
							return null;
						var min = (decimal?)g.Value.Min;
						var max = (decimal?)g.Value.Max;
						if (min == null || max == null)
							return null;
						if (g.Value.Grade == this.SummerCappingExamGradeEnum.Value)
							max = 100;
						return $"<B>{g.Value.Grade.ToFullName()}</B>: {min.Value.ToExamMarksString()}-{max.Value.ToExamMarksString()}";
					})
					.Where(s => s != null);
			}
			return string.Join(", ", ranges);
		}

		public string Summary
		{
			get
			{
				var grades = this.Grades.Where(g => g?.Min != null).Select(g => $"{g.Value.Grade.ToFullName()}{g.Value.Min}");
				var course = new List<string>();
				if (this.CourseAssignments != null)
					course.Add($"A{this.CourseAssignments}");
				if (this.CourseQuizzes != null)
					course.Add($"Q{this.CourseQuizzes}");
				if (this.CourseInternals != null)
					course.Add($"I{this.CourseInternals}");
				if (this.CourseMid != null)
					course.Add($"M{this.CourseMid}");
				course.Add($"F{this.CourseFinal}");
				var lab = new List<string>();
				if (this.LabAssignments != null)
					lab.Add($"A{this.LabAssignments}");
				if (this.LabQuizzes != null)
					lab.Add($"Q{this.LabQuizzes}");
				if (this.LabInternals != null)
					lab.Add($"I{this.LabInternals}");
				if (this.LabMid != null)
					lab.Add($"M{this.LabMid}");
				lab.Add($"F{this.LabFinal}");
				var summerCapping = string.Empty;
				if (this.SummerCappingExamGradeEnum != null)
					summerCapping = $"Cap{this.SummerCappingExamGradeEnum.Value.ToFullName()}";
				return $"[{string.Join("", grades)}{summerCapping}]C[{this.CourseMarksEntryTypeEnum.ToAbbreviation()}:{string.Join("", course)}]L[{this.LabMarksEntryTypeEnum.ToAbbreviation()}:{string.Join("", lab)}]";
			}
		}

		public string CalculateChecksumMD5()
		{
			return new Dictionary<string, string>
			{
				[nameof(this.InstituteID)] = this.InstituteID.ToString(),
				[nameof(this.AMin)] = this.AMin?.ToString(),
				[nameof(this.AMax)] = this.AMax?.ToString(),
				[nameof(this.AMinusMin)] = this.AMinusMin?.ToString(),
				[nameof(this.AMinusMax)] = this.AMinusMax?.ToString(),
				[nameof(this.BPlusMin)] = this.BPlusMin?.ToString(),
				[nameof(this.BPlusMax)] = this.BPlusMax?.ToString(),
				[nameof(this.BMin)] = this.BMin?.ToString(),
				[nameof(this.BMax)] = this.BMax?.ToString(),
				[nameof(this.BMinusMin)] = this.BMinusMin?.ToString(),
				[nameof(this.BMinusMax)] = this.BMinusMax?.ToString(),
				[nameof(this.CPlusMin)] = this.CPlusMin?.ToString(),
				[nameof(this.CPlusMax)] = this.CPlusMax?.ToString(),
				[nameof(this.CMin)] = this.CMin?.ToString(),
				[nameof(this.CMax)] = this.CMax?.ToString(),
				[nameof(this.CMinusMin)] = this.CMinusMin?.ToString(),
				[nameof(this.CMinusMax)] = this.CMinusMax?.ToString(),
				[nameof(this.DPlusMin)] = this.DPlusMin?.ToString(),
				[nameof(this.DPlusMax)] = this.DPlusMax?.ToString(),
				[nameof(this.DMin)] = this.DMin?.ToString(),
				[nameof(this.DMax)] = this.DMax?.ToString(),
				[nameof(this.FMax)] = this.FMax.ToString(),
				[nameof(this.AGradePoint)] = this.AGradePoint?.ToString(),
				[nameof(this.AMinusGradePoint)] = this.AMinusGradePoint?.ToString(),
				[nameof(this.BPlusGradePoint)] = this.BPlusGradePoint?.ToString(),
				[nameof(this.BGradePoint)] = this.BGradePoint?.ToString(),
				[nameof(this.BMinusGradePoint)] = this.BMinusGradePoint?.ToString(),
				[nameof(this.CPlusGradePoint)] = this.CPlusGradePoint?.ToString(),
				[nameof(this.CGradePoint)] = this.CGradePoint?.ToString(),
				[nameof(this.CMinusGradePoint)] = this.CMinusGradePoint?.ToString(),
				[nameof(this.DPlusGradePoint)] = this.DPlusGradePoint?.ToString(),
				[nameof(this.DGradePoint)] = this.DGradePoint?.ToString(),
				[nameof(this.FGradePoint)] = this.FGradePoint.ToString(),
				[nameof(this.SummerCappingExamGrade)] = this.SummerCappingExamGrade?.ToString(),
				[nameof(this.CourseMarksEntryType)] = this.CourseMarksEntryTypeEnum.ToString(),
				[nameof(this.CourseAssignments)] = this.CourseAssignments?.ToString(),
				[nameof(this.CourseQuizzes)] = this.CourseQuizzes?.ToString(),
				[nameof(this.CourseInternals)] = this.CourseInternals?.ToString(),
				[nameof(this.CourseMid)] = this.CourseMid?.ToString(),
				[nameof(this.CourseFinal)] = this.CourseFinal.ToString(),
				[nameof(this.LabMarksEntryType)] = this.LabMarksEntryTypeEnum.ToString(),
				[nameof(this.LabAssignments)] = this.LabAssignments?.ToString(),
				[nameof(this.LabQuizzes)] = this.LabQuizzes?.ToString(),
				[nameof(this.LabInternals)] = this.LabInternals?.ToString(),
				[nameof(this.LabMid)] = this.LabMid?.ToString(),
				[nameof(this.LabFinal)] = this.LabFinal.ToString(),
			}.Where(v => !string.IsNullOrEmpty(v.Value))
			.Select(v => $"{v.Key}:{v.Value}").Join(",").ComputeHashMD5();
		}
	}
}