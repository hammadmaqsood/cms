﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class OfficialDocument
	{
		[EnumGuids]
		public enum Statuses
		{
			[EnumGuidValue("C8C02DF9-A79F-4BCA-8446-F34982DF29C0", "Active")]
			Active,
			[EnumGuidValue("32B7F786-5F48-4318-BA55-45E72FC947CB", "Inactive")]
			Inactive
		}

		public Statuses StatusEnum
		{
			get => this.Status.GetEnum<Statuses>();
			set => this.Status = value.GetGuid();
		}
	}
}
