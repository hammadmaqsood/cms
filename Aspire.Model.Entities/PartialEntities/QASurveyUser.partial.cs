﻿using Aspire.Lib.Extensions;

namespace Aspire.Model.Entities
{
	public partial class QASurveyUser
	{
		public enum Statuses : byte
		{
			LinkNotSent = 1,
			NotStarted = 2,
			InProgress = 3,
			Completed = 4
		}

		public static readonly byte StatusLinkNotSent = (byte)Statuses.LinkNotSent;
		public static readonly byte StatusNotStarted = (byte)Statuses.NotStarted;
		public static readonly byte StatusInProgress = (byte)Statuses.InProgress;
		public static readonly byte StatusCompleted = (byte)Statuses.Completed;

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}
		public string StatusFullName => this.StatusEnum.ToString().SplitCamelCasing();
	}
}
