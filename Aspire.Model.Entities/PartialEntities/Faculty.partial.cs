﻿using Aspire.Lib.Extensions;

namespace Aspire.Model.Entities
{
	public partial class Faculty
	{
		public Faculty Validate()
		{
			this.FacultyName = this.FacultyName.TrimAndCannotBeEmpty();
			this.FacultyShortName = this.FacultyShortName.TrimAndCannotBeEmpty();
			this.FacultyAlias = this.FacultyAlias.TrimAndCannotEmptyAndMustBeUpperCase();
			return this;
		}
	}
}
