﻿using System;

namespace Aspire.Model.Entities
{
	public partial class SurveyConduct
	{
		public enum SurveyConductTypes : byte
		{
			BeforeMidTerm = 1,
			//AfterMidTerm = 2,
			BeforeFinalTerm = 4,
			//AfterFinalTerm = 8,
		}

		public enum Statuses : byte
		{
			Active = 1,
			Inactive = 2,
		}

		public static readonly byte StatusActive = (byte)Statuses.Active;
		public static readonly byte StatusInactive = (byte)Statuses.Inactive;
		public static readonly byte SurveyConductTypeBeforeMidTerm = (byte)SurveyConductTypes.BeforeMidTerm;
		public static readonly byte SurveyConductTypeBeforeFinalTerm = (byte)SurveyConductTypes.BeforeFinalTerm;

		public SurveyConductTypes SurveyConductTypeEnum
		{
			get => (SurveyConductTypes)this.SurveyConductType;
			set => this.SurveyConductType = (byte)value;
		}

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}

		public enum ValidationResults
		{
			None,
			NoRecordFound,
			AlreadyExists,
			InvalidOpenDate,
			InvalidDateRange,
			Success,
			InvalidCloseDate
		}

		public static ValidationResults Validate(DateTime openDate, DateTime? closeDate)
		{
			if (openDate.Date < DateTime.Today)
				return ValidationResults.InvalidOpenDate;
			if (closeDate == null)
				return ValidationResults.Success;
			return openDate.Date > closeDate.Value.Date ? ValidationResults.InvalidDateRange : ValidationResults.Success;
		}
	}
}
