﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class StudentFeeChallan
	{
		public enum Statuses : byte
		{
			NotPaid = 1,
			Paid = 2,
		}

		public static readonly byte StatusPaid = (byte)Statuses.Paid;
		public static readonly byte StatusNotPaid = (byte)Statuses.NotPaid;

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}

		public InstallmentNos InstallmentNoEnum
		{
			get => (InstallmentNos)this.InstallmentNo;
			set => this.InstallmentNo = (byte)value;
		}

		public bool IsZero => this.Amount == 0 && this.StatusEnum == Statuses.Paid && this.AccountTransactionID == null;

		public string StatusFullName => this.StatusEnum.ToFullName();

		public string GetFullChallanNo(string instituteCode)
		{
			return AspireFormats.ChallanNo.GetFullChallanNoString(instituteCode, this.StudentFee.NewAdmission ? InstituteBankAccount.FeeTypes.NewAdmissionFee : InstituteBankAccount.FeeTypes.StudentFee, this.StudentFeeChallanID);
		}
	}
}
