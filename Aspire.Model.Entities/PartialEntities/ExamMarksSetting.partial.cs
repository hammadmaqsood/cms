﻿using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Model.Entities
{
	public partial class ExamMarksSetting
	{
		public enum Statuses : byte
		{
			Active = 1,
			Inactive = 2,
		}

		public static readonly byte StatusActive = (byte)Statuses.Active;
		public static readonly byte StatusInactive = (byte)Statuses.Inactive;

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}

		public ExamMarksTypes ExamMarksTypesEnum
		{
			get => (ExamMarksTypes)this.ExamMarksTypes;
			set => this.ExamMarksTypes = (byte)value;
		}

		public enum ValidationResults
		{
			None,
			NoRecordFound,
			AlreadyExists,
			InvalidFromDate,
			InvalidDateRange,
			Success,
		}

		public static ValidationResults Validate(DateTime fromDate, DateTime toDate)
		{
			if (fromDate.Date < DateTime.Today)
				return ValidationResults.InvalidFromDate;
			if (fromDate.Date > toDate.Date)
				return ValidationResults.InvalidDateRange;
			return ValidationResults.Success;
		}
	}
}
