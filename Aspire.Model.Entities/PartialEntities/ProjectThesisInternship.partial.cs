﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.Model.Entities
{
	public partial class ProjectThesisInternship
	{
		[EnumGuids]
		public enum RecordTypes
		{
			[EnumGuidValue("EB887E4A-3075-451A-97F4-8C17506E7A02", "Internship")]
			Internship,
			[EnumGuidValue("976B84C8-E5E1-4C23-AA41-053C4B0213CC", "Project")]
			Project,
			[EnumGuidValue("D5B049C7-ECD1-4E03-B88E-446489E7E6D6", "Thesis")]
			Thesis
		}

		public RecordTypes RecordTypeEnum
		{
			get => this.RecordType.GetEnum<RecordTypes>();
			set => this.RecordType = value.GetGuid();
		}

		public string RecordTypeFullName => this.RecordType.GetFullName();

		public string Semester => this.SemesterID.ToSemesterString();

		public ExamGrades GradeEnum
		{
			get => (ExamGrades)this.Grade;
			set => this.Grade = (byte)value;
		}

		public string GradeFullName => this.GradeEnum.ToFullName();

		public ProjectThesisInternship Validate(ExamMarksPolicy examMarksPolicy)
		{
			switch (this.RecordTypeEnum)
			{
				case RecordTypes.Internship:
					this.Title = this.Title.MustBeNullOrEmpty();
					this.Organization = this.Organization.TrimAndCannotBeEmpty();
					this.FromDate = this.FromDate.CannotBeNull();
					this.ToDate = this.ToDate.CannotBeNull();
					this.ReportSubmissionDate = this.ReportSubmissionDate.CannotBeNull();
					this.DefenceVivaDate = this.DefenceVivaDate.CannotBeNull();
					this.CompletionDate = this.DefenceVivaDate;
					break;
				case RecordTypes.Project:
					this.Title = this.Title.TrimAndCannotBeEmpty();
					this.Organization = this.Organization.TrimAndMakeItNullIfEmpty();
					this.FromDate = this.FromDate.MustBeNull();
					this.ToDate = this.ToDate.MustBeNull();
					this.ReportSubmissionDate = this.ReportSubmissionDate.CannotBeNull();
					this.DefenceVivaDate = this.DefenceVivaDate.CannotBeNull();
					this.CompletionDate = this.CompletionDate.CannotBeNull();
					break;
				case RecordTypes.Thesis:
					this.Title = this.Title.TrimAndCannotBeEmpty();
					this.Organization = this.Organization.MustBeNullOrEmpty();
					this.FromDate = this.FromDate.MustBeNull();
					this.ToDate = this.ToDate.MustBeNull();
					this.ReportSubmissionDate = this.ReportSubmissionDate.CannotBeNull();
					this.DefenceVivaDate = this.DefenceVivaDate.CannotBeNull();
					this.CompletionDate = this.CompletionDate.CannotBeNull();
					break;
				default:
					throw new NotImplementedEnumException(this.RecordTypeEnum);
			}
			this.Remarks = this.Remarks.TrimAndMakeItNullIfEmpty();

			this.ProjectThesisInternshipExaminers.ForEach(e => e.Validate());

			if (!this.ProjectThesisInternshipMarks.Any())
				throw new InvalidOperationException("No ProjectThesisInternshipMarks found.");
			if (this.ProjectThesisInternshipMarks.Any(m => m.ObtainedMarks > m.TotalMarks))
				throw new InvalidOperationException("ObtainedMarks must be less than or equal to TotalMarks.");

			var obtained = this.ProjectThesisInternshipMarks.Sum(t => t.ObtainedMarks);
			var total = this.ProjectThesisInternshipMarks.Sum(t => t.TotalMarks);
			this.Total = (byte)Math.Round(obtained * 100M / total, 0, MidpointRounding.AwayFromZero);
			var (examGradeEnum, gradePoints) = examMarksPolicy.GetExamGradeAndGradePoints((byte)this.Total, false);

			this.GradeEnum = examGradeEnum;
			this.GradePoints = gradePoints;
			this.Product = this.GradePoints * this.CreditHours;
			return this;
		}
	}
}
