﻿using Aspire.Lib.Extensions;

namespace Aspire.Model.Entities
{
	public partial class StudentCreditsTransfer
	{
		public StudentCreditsTransfer Validate()
		{
			this.CommitteeMembers = this.CommitteeMembers.TrimAndCannotBeEmpty();
			this.InstituteName = this.InstituteName.TrimAndCannotBeEmpty();
			this.Remarks = this.Remarks.TrimAndCannotBeEmpty();
			return this;
		}
	}
}
