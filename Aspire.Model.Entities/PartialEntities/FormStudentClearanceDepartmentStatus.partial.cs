﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class FormStudentClearanceDepartmentStatus
	{
		[EnumGuids]
		public enum DepartmentStatuses
		{
			[EnumGuidValue("bcd0c81e-6c67-431c-b4ff-0df9ef7f860f", "Pending")]
			Pending,
			[EnumGuidValue("41289b5d-5eba-4c66-b6a9-c53c670cf431", "Not Approved")]
			NotApproved,
			[EnumGuidValue("beba959d-6954-4705-98b2-bc8bf79b14f8", "Approved")]
			Approved
		}

		public DepartmentStatuses DepartmentStatusEnum
		{
			get => this.DepartmentStatus.GetEnum<DepartmentStatuses>();
			set => this.DepartmentStatus = value.GetGuid();
		}

		[EnumGuids]
		public enum Departments
		{
			[EnumGuidValue("ca3e5b07-6792-4eaf-89ae-9dda71973c67", "Main Library")]
			MainLibrary,
			[EnumGuidValue("abb7386f-61fc-420b-ae84-8b8dc4924058", "Law Library")]
			LawLibrary,
			[EnumGuidValue("00310653-610c-4fcb-afee-4224f1fe772a", "Labs/Server Room")]
			LabsOrServerRoom,
			[EnumGuidValue("8998145c-1aca-4293-ad54-c70c2a56e762", "Laptop Section")]
			LaptopSection,
			[EnumGuidValue("63a3b836-38ae-4ae2-87d3-97543dd606bd", "Student Affairs")]
			StudentAffairs,
			[EnumGuidValue("c7a48e2f-5c36-4915-8817-c2c466e248b0", "Hostel Warden (Female Students Only)")]
			HostelWarden,
			[EnumGuidValue("1bb6fe9e-4795-4167-a03a-66a56f7bade7", "Examination Cell")]
			ExaminationCell,
			[EnumGuidValue("54f45a97-088d-472d-8638-3921d5777a46", "Admission Office (1st Semester Only)")]
			AdmissionOfficeFor1stSemesterOnly,
			[EnumGuidValue("be0c26c4-58aa-4ed9-87ce-954312746b08", "Security/Sport")]
			SecurityAndSports,
			[EnumGuidValue("4cfb6093-6c38-4998-b792-07cf0404effb", "Student Advisor")]
			StudentAdvisor,
			[EnumGuidValue("a071231a-56c2-418a-bc91-e4692016cf44", "Accounts Section")]
			AccountsSection
		}

		public Departments DepartmentEnum
		{
			get => this.Department.GetEnum<Departments>();
			set => this.Department = value.GetGuid();
		}
	}
}
