﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Model.Entities
{
	public partial class Student
	{
		public enum AdmissionTypes : byte
		{
			Admission = 1,
			Transferred = 2,
			AdmissionDeferred = 3,
		}

		public static readonly byte AdmissionTypeAdmission = (byte)AdmissionTypes.Admission;
		public static readonly byte AdmissionTypeTransferred = (byte)AdmissionTypes.Transferred;
		public static readonly byte AdmissionTypeAdmissionDeferred = (byte)AdmissionTypes.AdmissionDeferred;

		public AdmissionTypes AdmissionTypeEnum
		{
			get => (AdmissionTypes)this.AdmissionType;
			set => this.AdmissionType = (byte)value;
		}

		public AreaTypes? AreaTypeEnum
		{
			get => (AreaTypes?)this.AreaType;
			set => this.AreaType = (byte?)value;
		}

		public Genders GenderEnum
		{
			get => (Genders)this.Gender;
			set => this.Gender = (byte)value;
		}

		public Categories CategoryEnum
		{
			get => (Categories)this.Category;
			set => this.Category = (byte)value;
		}

		public BloodGroups? BloodGroupEnum
		{
			get => (BloodGroups?)this.BloodGroup;
			set => this.BloodGroup = (byte?)value;
		}

		public Sponsorships? SponsoredByEnum
		{
			get => (Sponsorships?)this.SponsoredBy;
			set => this.SponsoredBy = (byte?)value;
		}

		public ETSTypes? ETSTypeEnum
		{
			get => (ETSTypes?)this.ETSType;
			set => this.ETSType = (byte?)value;
		}

		public enum Statuses : byte
		{
			AdmissionCancelled = 1,
			Blocked = 2,
			Deferred = 3,
			Dropped = 4,
			Expelled = 5,
			Graduated = 6,
			Left = 7,
			ProgramChanged = 8,
			Rusticated = 9,
			TransferredToOtherCampus = 10,
		}

		public static readonly byte StatusAdmissionCancelled = (byte)Statuses.AdmissionCancelled;
		public static readonly byte StatusBlocked = (byte)Statuses.Blocked;
		public static readonly byte StatusDeferred = (byte)Statuses.Deferred;
		public static readonly byte StatusDropped = (byte)Statuses.Dropped;
		public static readonly byte StatusExpelled = (byte)Statuses.Expelled;
		public static readonly byte StatusGraduated = (byte)Statuses.Graduated;
		public static readonly byte StatusLeft = (byte)Statuses.Left;
		public static readonly byte StatusProgramChanged = (byte)Statuses.ProgramChanged;
		public static readonly byte StatusRusticated = (byte)Statuses.Rusticated;
		public static readonly byte StatusTransferredToOtherCampus = (byte)Statuses.TransferredToOtherCampus;

		public Statuses? StatusEnum
		{
			get => (Statuses?)this.Status;
			set => this.Status = (byte?)value;
		}

		[EnumGuids]
		public enum StudentDocumentsStatuses
		{
			[EnumGuidValue("B5D12F12-A493-44A6-9962-4BC0AA4CDE14", "Pending")]
			VerificationPending,
			[EnumGuidValue("D76425DE-5D9C-4E32-8BAD-C9D83F9AECA6", "Rejected")]
			Rejected,
			[EnumGuidValue("8E59DC3C-89DB-4321-ACD7-07A698DC03DA", "Verified")]
			Verified
		}

		public StudentDocumentsStatuses? StudentDocumentsStatusEnum
		{
			get => this.StudentDocumentsStatus?.GetEnum<StudentDocumentsStatuses>();
			set => this.StudentDocumentsStatus = value.GetGuid();
		}

		public Student Validate()
		{
			this.Enrollment = this.Enrollment.TrimAndCannotBeEmpty();
			this.Name = this.Name.TrimAndCannotEmptyAndMustBeUpperCase();
			this.UniversityEmail.ToNullIfEmpty();
			this.CNIC = this.CNIC.TrimAndMakeItNullIfEmpty();
			this.PassportNo = this.PassportNo.TrimAndMakeItNullIfEmpty();
			this.Gender = this.Gender.CannotBeNull();
			this.Category = this.Category.CannotBeNull();
			this.DOB = this.DOB.CannotBeNullAndFutureDate();
			this.BloodGroup = this.BloodGroup.CannotBeNull();
			this.Phone = this.Phone.TrimAndMakeItNullIfEmpty();
			this.Mobile = this.Mobile.TrimAndMakeItNullIfEmpty();
			this.Nationality = this.Nationality.TrimAndCannotBeEmpty();
			//this.Country = this.Country;
			//this.Province = this.Province;
			//this.District = this.District;
			//this.Tehsil = this.Tehsil;
			//this.Domicile = this.Domicile;
			//this.AreaType = this.AreaType;
			//this.CurrentAddress = this.CurrentAddress;
			//this.PermanentAddress = this.PermanentAddress;
			//this.ServiceNo = this.ServiceNo;
			//this.EmergencyContactName = this.EmergencyContactName;
			//this.EmergencyMobile = this.EmergencyMobile;
			//this.EmergencyPhone = this.EmergencyPhone;
			//this.EmergencyRelationship = this.EmergencyRelationship;
			//this.EmergencyAddress = this.EmergencyAddress;
			//this.PhysicalDisability = this.PhysicalDisability;
			this.FatherName = this.FatherName.TrimAndCannotEmptyAndMustBeUpperCase();
			//this.FatherCNIC = this.FatherCNIC;
			//this.FatherPassportNo = this.FatherPassportNo;
			//this.FatherDesignation = this.FatherDesignation;
			//this.FatherDepartment = this.FatherDepartment;
			//this.FatherOrganization = this.FatherOrganization;
			//this.FatherServiceNo = this.FatherServiceNo;
			//this.FatherOfficePhone = this.FatherOfficePhone;
			//this.FatherHomePhone = this.FatherHomePhone;
			//this.FatherMobile = this.FatherMobile;
			//this.FatherFax = this.FatherFax;
			//this.SponsoredBy = this.SponsoredBy;
			//this.SponsorName = this.SponsorName;
			//this.SponsorCNIC = this.SponsorCNIC;
			//this.SponsorPassportNo = this.SponsorPassportNo;
			//this.SponsorRelationshipWithGuardian = this.SponsorRelationshipWithGuardian;
			//this.SponsorDesignation = this.SponsorDesignation;
			//this.SponsorDepartment = this.SponsorDepartment;
			//this.SponsorOrganization = this.SponsorOrganization;
			//this.SponsorServiceNo = this.SponsorServiceNo;
			//this.SponsorPhoneHome = this.SponsorPhoneHome;
			//this.SponsorMobile = this.SponsorMobile;
			//this.SponsorPhoneOffice = this.SponsorPhoneOffice;
			//this.SponsorFax = this.SponsorFax;
			//this.FinalCGPA = this.FinalCGPA;
			//this.FinalTranscriptNo = this.FinalTranscriptNo;
			//this.FinalTranscriptIssueDate = this.FinalTranscriptIssueDate;
			return this;
		}

		public void CopyValuesAndValidate(Student fromStudent)
		{
			if (this.StatusEnum != fromStudent.StatusEnum)
			{
				if (this.DeferredToStudentID != null)
					throw new InvalidOperationException("Status cannot be changed after deferment.");
				this.StatusDate = DateTime.Now;
				this.StatusEnum = fromStudent.StatusEnum;
			}
			this.DeferredToAdmissionOpenProgramID = this.StatusEnum == Statuses.Deferred ? fromStudent.DeferredToAdmissionOpenProgramID : null;
			this.DeferredToShift = this.StatusEnum == Statuses.Deferred ? fromStudent.DeferredToShift : null;

			if (this.DeferredToAdmissionOpenProgramID != null && this.DeferredToShift == null)
				throw new InvalidOperationException();
			if (this.DeferredToAdmissionOpenProgramID == null && this.DeferredToShift != null)
				throw new InvalidOperationException();

			this.Name = fromStudent.Name;
			this.RegistrationNo = fromStudent.RegistrationNo;
			this.UniversityEmail = fromStudent.UniversityEmail;
			this.CNIC = fromStudent.CNIC;
			this.PassportNo = fromStudent.PassportNo;
			this.Gender = fromStudent.Gender;
			this.Category = fromStudent.Category;
			this.DOB = fromStudent.DOB;
			this.BloodGroup = fromStudent.BloodGroup;
			this.Phone = fromStudent.Phone;
			this.Mobile = fromStudent.Mobile;
			this.Nationality = fromStudent.Nationality;
			this.Country = fromStudent.Country;
			this.Province = fromStudent.Province;
			this.District = fromStudent.District;
			this.Tehsil = fromStudent.Tehsil;
			this.Domicile = fromStudent.Domicile;
			this.AreaType = fromStudent.AreaType;
			this.CurrentAddress = fromStudent.CurrentAddress;
			this.PermanentAddress = fromStudent.PermanentAddress;
			this.ServiceNo = fromStudent.ServiceNo;
			this.EmergencyContactName = fromStudent.EmergencyContactName;
			this.EmergencyMobile = fromStudent.EmergencyMobile;
			this.EmergencyPhone = fromStudent.EmergencyPhone;
			this.EmergencyRelationship = fromStudent.EmergencyRelationship;
			this.EmergencyAddress = fromStudent.EmergencyAddress;
			this.PhysicalDisability = fromStudent.PhysicalDisability;
			this.Religion = fromStudent.Religion;
			this.NextOfKin = fromStudent.NextOfKin;
			this.FatherName = fromStudent.FatherName;
			this.FatherCNIC = fromStudent.FatherCNIC;
			this.FatherPassportNo = fromStudent.FatherPassportNo;
			this.FatherDesignation = fromStudent.FatherDesignation;
			this.FatherDepartment = fromStudent.FatherDepartment;
			this.FatherOrganization = fromStudent.FatherOrganization;
			this.FatherServiceNo = fromStudent.FatherServiceNo;
			this.FatherOfficePhone = fromStudent.FatherOfficePhone;
			this.FatherHomePhone = fromStudent.FatherHomePhone;
			this.FatherMobile = fromStudent.FatherMobile;
			this.FatherFax = fromStudent.FatherFax;
			this.SponsoredBy = fromStudent.SponsoredBy;
			this.SponsorName = fromStudent.SponsorName;
			this.SponsorCNIC = fromStudent.SponsorCNIC;
			this.SponsorPassportNo = fromStudent.SponsorPassportNo;
			this.SponsorRelationshipWithGuardian = fromStudent.SponsorRelationshipWithGuardian;
			this.SponsorDesignation = fromStudent.SponsorDesignation;
			this.SponsorDepartment = fromStudent.SponsorDepartment;
			this.SponsorOrganization = fromStudent.SponsorOrganization;
			this.SponsorServiceNo = fromStudent.SponsorServiceNo;
			this.SponsorPhoneHome = fromStudent.SponsorPhoneHome;
			this.SponsorMobile = fromStudent.SponsorMobile;
			this.SponsorPhoneOffice = fromStudent.SponsorPhoneOffice;
			this.SponsorFax = fromStudent.SponsorFax;
			this.FinalCGPA = fromStudent.FinalCGPA;
			this.FinalTranscriptNo = fromStudent.FinalTranscriptNo;
			this.FinalTranscriptIssueDate = fromStudent.FinalTranscriptIssueDate;
			this.DegreeNo = fromStudent.DegreeNo;
			this.DegreeGazetteNo = fromStudent.DegreeGazetteNo;
			this.DegreeIssueDate = fromStudent.DegreeIssueDate;
			this.DegreeIssuedToStudent = fromStudent.DegreeIssuedToStudent;

			this.Validate();
		}
	}
}
