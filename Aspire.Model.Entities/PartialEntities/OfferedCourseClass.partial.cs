﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class OfferedCourseClass
	{
		public SemesterNos SemesterNoEnum
		{
			get => (SemesterNos)this.SemesterNo;
			set => this.SemesterNo = (short)value;
		}

		public Sections SectionEnum
		{
			get => (Sections)this.Section;
			set => this.Section = (int)value;
		}

		public Shifts ShiftEnum
		{
			get => (Shifts)this.Shift;
			set => this.Shift = (byte)value;
		}

		public string ClassName => AspireFormats.GetClassName(this.SemesterNo, this.Section, this.Shift);
	}
}
