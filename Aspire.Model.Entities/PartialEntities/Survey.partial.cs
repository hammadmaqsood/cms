﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class Survey
	{
		public enum SurveyTypes : byte
		{
			StudentWise = 1,
			StudentCourseWise = 2,
			FacultyMemberWise = 3,
			FacultyMemberCourseWise = 4,
		}

		public static readonly byte SurveyTypeStudentWise = (byte)SurveyTypes.StudentWise;
		public static readonly byte SurveyTypeStudentCourseWise = (byte)SurveyTypes.StudentCourseWise;
		public static readonly byte SurveyTypeFacultyMemberWise = (byte)SurveyTypes.FacultyMemberWise;
		public static readonly byte SurveyTypeFacultyMemberCourseWise = (byte)SurveyTypes.FacultyMemberCourseWise;

		public SurveyTypes SurveyTypeEnum
		{
			get { return (SurveyTypes)this.SurveyType; }
			set { this.SurveyType = (byte)value; }
		}

		public string SurveyTypeFullName => this.SurveyTypeEnum.ToFullName();

		public Survey Validate()
		{
			this.SurveyName = this.SurveyName.TrimAndCannotBeEmpty();
			this.Description = this.Description.Trim();
			return this;
		}
	}
}