﻿namespace Aspire.Model.Entities
{
	public partial class ScholarshipApplicationApproval
	{
		public enum ApprovalStatuses : byte
		{
			Pending = 1,
			Approved = 2,
			Rejected = 3,
		}

		public ApprovalStatuses ApprovalStatusEnum
		{
			get => (ApprovalStatuses)this.ApprovalStatus;
			set => this.ApprovalStatus = (byte)value;
		}

		public enum ChequeStatuses : byte
		{
			No = 1,
			Yes = 2,
		}

		public ChequeStatuses ChequeStatusesEnum
		{
			get => (ChequeStatuses)this.ChequeStatus;
			set => this.ChequeStatus = (byte)value;
		}
	}
}
