﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class Complaint
	{
		[EnumGuids]
		public enum Statuses
		{
			[EnumGuidValue("7483C1A5-3D19-4069-85A5-36B17F623A3E", "Pending")]
			Pending,
			[EnumGuidValue("5452B1DE-CD63-4C4C-B58F-94C369DBB739", "Resolved")]
			Resolved,
		}

		public Statuses StatusEnum
		{
			get => this.Status.GetEnum<Statuses>();
			set => this.Status = value.GetGuid();
		}
	}
}
