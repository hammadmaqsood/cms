﻿using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Model.Entities
{
	public partial class OfferedCours
	{
		public enum Statuses : byte
		{
			All = 1,
			Blocked = 2,
			Coordinators = 3,
			Restricted = 4,
			Students = 5,
		}

		public static readonly byte StatusAll = (byte)Statuses.All;
		public static readonly byte StatusStudents = (byte)Statuses.Students;
		public static readonly byte StatusCoordinators = (byte)Statuses.Coordinators;
		public static readonly byte StatusRestricted = (byte)Statuses.Restricted;
		public static readonly byte StatusBlocked = (byte)Statuses.Blocked;

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}

		public SemesterNos SemesterNoEnum
		{
			get => (SemesterNos)this.SemesterNo;
			set => this.SemesterNo = (short)value;
		}

		public Sections SectionEnum
		{
			get => (Sections)this.Section;
			set => this.Section = (int)value;
		}

		public Shifts ShiftEnum
		{
			get => (Shifts)this.Shift;
			set => this.Shift = (byte)value;
		}

		public enum ExamCompilationTypes : byte
		{
			OverallAverage = 1,
		}

		public ExamCompilationTypes? AssignmentsExamCompilationTypeEnum
		{
			get => (ExamCompilationTypes?)this.AssignmentsExamCompilationType;
			set => this.AssignmentsExamCompilationType = (byte?)value;
		}

		public ExamCompilationTypes? QuizzesExamCompilationTypeEnum
		{
			get => (ExamCompilationTypes?)this.QuizzesExamCompilationType;
			set => this.QuizzesExamCompilationType = (byte?)value;
		}

		public ExamCompilationTypes? InternalsExamCompilationTypeEnum
		{
			get => (ExamCompilationTypes?)this.InternalsExamCompilationType;
			set => this.InternalsExamCompilationType = (byte?)value;
		}

		public enum MarksEntryStatuses
		{
			Allowed,
			MarksEntryLocked,
			AssignmentsMarksSubmitted,
			QuizzesMarksSubmitted,
			InternalsMarksSubmitted,
			MidsMarksSubmitted,
			FinalMarksSubmitted
		}

		public MarksEntryStatuses GetMarksEntryStatus(UserTypes userTypeEnum, ExamMarksTypes examMarksTypeEnum)
		{
			return GetMarksEntryStatus(userTypeEnum, examMarksTypeEnum, this.MarksEntryLocked, this.AssignmentsMarksSubmitted, this.QuizzesMarksSubmitted, this.InternalsMarksSubmitted, this.MidMarksSubmitted, this.FinalMarksSubmitted);
		}

		public static MarksEntryStatuses GetMarksEntryStatus(UserTypes userTypeEnum, ExamMarksTypes examMarksTypeEnum, bool marksEntryLocked, bool assignmentsMarksSubmitted, bool quizzesMarksSubmitted, bool internalsMarksSubmitted, bool midMarksSubmitted, bool finalMarksSubmitted)
		{
			if (marksEntryLocked)
				return MarksEntryStatuses.MarksEntryLocked;
			switch (userTypeEnum)
			{
				case UserTypes.Staff:
					return MarksEntryStatuses.Allowed;
				case UserTypes.Faculty:
					switch (examMarksTypeEnum)
					{
						case ExamMarksTypes.Assignments:
							if (internalsMarksSubmitted)
								return MarksEntryStatuses.InternalsMarksSubmitted;
							return assignmentsMarksSubmitted ? MarksEntryStatuses.AssignmentsMarksSubmitted : MarksEntryStatuses.Allowed;
						case ExamMarksTypes.Quizzes:
							if (internalsMarksSubmitted)
								return MarksEntryStatuses.InternalsMarksSubmitted;
							return quizzesMarksSubmitted ? MarksEntryStatuses.QuizzesMarksSubmitted : MarksEntryStatuses.Allowed;
						case ExamMarksTypes.Internals:
							return internalsMarksSubmitted ? MarksEntryStatuses.InternalsMarksSubmitted : MarksEntryStatuses.Allowed;
						case ExamMarksTypes.Mid:
							return midMarksSubmitted ? MarksEntryStatuses.MidsMarksSubmitted : MarksEntryStatuses.Allowed;
						case ExamMarksTypes.Final:
							return finalMarksSubmitted ? MarksEntryStatuses.FinalMarksSubmitted : MarksEntryStatuses.Allowed;
						default:
							throw new ArgumentOutOfRangeException(nameof(examMarksTypeEnum), examMarksTypeEnum, null);
					}
				default:
					throw new ArgumentOutOfRangeException(nameof(userTypeEnum), userTypeEnum, null);
			}
		}

		public static MarksEntryStatuses GetMarksEntryStatus(UserTypes userTypeEnum, OfferedCourseExam.ExamTypes examTypeEnum, bool marksEntryLocked, bool assignmentsMarksSubmitted, bool quizzesMarksSubmitted, bool internalsMarksSubmitted)
		{
			switch (examTypeEnum)
			{
				case OfferedCourseExam.ExamTypes.Assignment:
					return GetMarksEntryStatus(userTypeEnum, ExamMarksTypes.Assignments, marksEntryLocked, assignmentsMarksSubmitted, quizzesMarksSubmitted, internalsMarksSubmitted, false, false);
				case OfferedCourseExam.ExamTypes.Quiz:
					return GetMarksEntryStatus(userTypeEnum, ExamMarksTypes.Quizzes, marksEntryLocked, assignmentsMarksSubmitted, quizzesMarksSubmitted, internalsMarksSubmitted, false, false);
				default:
					throw new ArgumentOutOfRangeException(nameof(examTypeEnum), examTypeEnum, null);
			}
		}
	}
}
