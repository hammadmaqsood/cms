﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class ExamSession
	{
		public Shifts ShiftEnum
		{
			get { return (Shifts)this.Shift; }
			set { this.Shift = (byte)value; }
		}
	}
}
