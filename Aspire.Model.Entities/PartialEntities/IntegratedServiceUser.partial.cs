﻿using Aspire.Lib.Extensions;

namespace Aspire.Model.Entities
{
	public partial class IntegratedServiceUser
	{
		public enum ServiceTypes : byte
		{
			CBT = 1,
			BankAlfalah = 2,
			AlliedBank = 3,
			HR = 4,
			OBE = 5,
			LMS = 6
		}

		public static readonly byte ServiceTypeCBT = (byte)ServiceTypes.CBT;
		public static readonly byte ServiceTypeBankAlfalah = (byte)ServiceTypes.BankAlfalah;
		public static readonly byte ServiceTypeAlliedBank = (byte)ServiceTypes.AlliedBank;
		public static readonly byte ServiceTypeHR = (byte)ServiceTypes.HR;

		public enum Statuses : byte
		{
			Active = 1,
			Inactive = 2,
		}

		public static readonly byte StatusActive = (byte)Statuses.Active;
		public static readonly byte StatusInactive = (byte)Statuses.Inactive;

		public ServiceTypes ServiceTypeEnum
		{
			get => (ServiceTypes)this.ServiceType;
			set => this.ServiceType = (byte)value;
		}

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}

		public IntegratedServiceUser Validate(bool forAdd)
		{
			if (forAdd)
				this.ExpiryDate = this.ExpiryDate.CannotBeNullAndPreviousDate();
			this.ServiceName = this.ServiceName.CannotBeEmptyOrWhitespace();
			this.UserName = this.UserName.CannotBeDefaultOrEmpty();
			return this;
		}
	}
}

