﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class UserRole
	{
		public enum Statuses : byte
		{
			Active = 1,
			Inactive = 2,
		}

		public Statuses StatusEnum
		{
			get { return (Statuses)this.Status; }
			set { this.Status = (byte)value; }
		}

		public UserTypes UserTypeEnum
		{
			get { return (UserTypes)this.UserType; }
			set { this.UserType = (short)value; }
		}
	}
}
