﻿namespace Aspire.Model.Entities
{
	public partial class ScholarshipApplication
	{
		public enum Statuses : byte
		{
			InProgress = 1,
			Submitted = 2,
			NotApplied = 3,
			DateOver = 4,
		}

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}

		public enum DocumentsStatuses : byte
		{
			NotSubmitted = 1,
			Submitted = 2,
		}

		public DocumentsStatuses DocumentStatusesEnum
		{
			get => (DocumentsStatuses)this.DocumentsStatus;
			set => this.DocumentsStatus = (byte)value;
		}
	}
}
