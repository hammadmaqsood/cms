﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Model.Entities
{
	public partial class SystemFile
	{
		[Flags]
		public enum FileExtensions
		{
			None = 1 << 0,
			Png = 1 << 1,
			Jpg = 1 << 2,
			Jpeg = 1 << 3,
			Pdf = 1 << 4,
			Csv = 1 << 5,
			Xls = 1 << 6,
			Xlsx = 1 << 7,
			Doc = 1 << 8,
			Docx = 1 << 9
		}

		[Flags]
		public enum FileContentsTypes
		{
			None = 1 << 0,
			Png = 1 << 1,
			Jpg = 1 << 2,
			Jpeg = 1 << 3,
			Pdf = 1 << 4,
			Csv = 1 << 5
		}

		public FileExtensions FileExtensionEnum
		{
			get => this.FileExtension.ToEnum<FileExtensions>();
			set => this.FileExtension = value.ToString();
		}

		public string MimeType => this.FileExtensionEnum.ToMimeType();

		public FileContentsTypes FileContentsTypeEnum
		{
			get => this.FileContentsType.ToEnum<FileContentsTypes>();
			set => this.FileContentsType = value.ToString();
		}
	}
}
