﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Model.Entities
{
	public partial class Candidate
	{
		public enum EditStatuses : byte
		{
			CanEdit = 1,
			CanNotEditBasicInfo = 2,
			CanEditContactInformation = 3,
			Readonly = 4,
		}

		public EditStatuses EditStatusEnum
		{
			get => (EditStatuses)this.EditStatus;
			set => this.EditStatus = (byte)value;
		}

		[Flags]
		public enum Statuses : byte
		{
			Blank = 1,
			PictureUploaded = 2,
			InformationProvided = 4,
			Completed = PictureUploaded | InformationProvided
		}

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}

		public Genders? GenderEnum
		{
			get => (Genders?)this.Gender;
			set => this.Gender = (byte?)value;
		}

		public BloodGroups? BloodGroupEnum
		{
			get => (BloodGroups?)this.BloodGroup;
			set => this.BloodGroup = (byte?)value;
		}

		public Categories? CategoryEnum
		{
			get => (Categories?)this.Category;
			set => this.Category = (byte?)value;
		}

		public Sponsorships? SponsoredByEnum
		{
			get => (Sponsorships?)this.SponsoredBy;
			set => this.SponsoredBy = (byte?)value;
		}

		public AreaTypes? AreaTypeEnum
		{
			get => (AreaTypes?)this.AreaType;
			set => this.AreaType = (byte?)value;
		}

		public enum SourceOfInformations : byte
		{
			Friends = 1,
			Newspaper = 2,
			Recommendation = 3,
			Signboard = 4,
			SocialMedia = 5,
			Website = 6,
			BUVisitingTeamAtOurCollege = 7,
			ExpoExhibitionSeminar = 8,
		}

		public SourceOfInformations? SourceOfInformationEnum
		{
			get => (SourceOfInformations?)this.SourceOfInformation;
			set => this.SourceOfInformation = (byte?)value;
		}

		public void Validate()
		{
			this.Email = this.Email.MustBeEmailAddress();
			this.Name = this.Name.TrimAndCannotEmptyAndMustBeUpperCase();
			this.CNIC = this.ForeignStudent ? this.CNIC.MustBeNullOrEmpty() : this.CNIC.MustBeCNIC();
			this.PassportNo = this.ForeignStudent ? this.PassportNo.TrimAndCannotBeEmpty() : this.PassportNo.MustBeNullOrEmpty();
			this.GenderEnum = this.GenderEnum.CannotBeNull();
			this.CategoryEnum = this.CategoryEnum.CannotBeNull();
			this.DOB = this.DOB.CannotBeNullAndFutureDate();
			this.BloodGroupEnum = this.BloodGroupEnum.CannotBeNull();
			this.Mobile = this.Mobile.TrimAndCannotBeEmpty();
			this.Phone = this.Phone.TrimAndCannotBeEmpty();
			this.Nationality = this.Nationality.TrimAndCannotBeEmpty();
			this.Country = this.Country.TrimAndCannotBeEmpty();
			if (this.ForeignStudent)
			{
				if (this.Nationality.ToLower().Contains("Pakistan".ToLower()))
					throw new InvalidOperationException();
				if (this.Country.ToLower().Contains("Pakistan".ToLower()))
					throw new InvalidOperationException();

				this.Province = this.Province.TrimAndCannotBeEmpty();
				this.District = this.District.TrimAndCannotBeEmpty();
				this.Tehsil = this.Tehsil.MustBeNullOrEmpty();
				this.Domicile = this.Domicile.MustBeNullOrEmpty();
				this.AreaType = this.AreaType.MustBeNull();
				this.ServiceNo = this.ServiceNo.MustBeNullOrEmpty();
				this.NTNOrCNIC = this.NTNOrCNIC.MustBeNullOrEmpty();
			}
			else
			{
				if (this.Nationality.ToLower() != "Pakistani".ToLower())
					throw new InvalidOperationException();
				if (this.Country.ToLower() != "Pakistan".ToLower())
					throw new InvalidOperationException();

				this.Province = this.Province.TrimAndCannotBeEmpty();
				this.District = this.District.TrimAndCannotBeEmpty();
				this.Tehsil = this.Tehsil.TrimAndMakeItNullIfEmpty();
				this.Domicile = this.Domicile.TrimAndMakeItNullIfEmpty();
				this.AreaTypeEnum = this.AreaTypeEnum.CannotBeNull();
				this.ServiceNo = this.ServiceNo.TrimAndMakeItNullIfEmpty();
				this.NTNOrCNIC = this.NTNOrCNIC.TrimAndCannotBeEmpty();
			}
			this.CurrentAddress = this.CurrentAddress.TrimAndCannotBeEmpty();
			this.PermanentAddress = this.PermanentAddress.TrimAndCannotBeEmpty();
			this.SourceOfInformationEnum = this.SourceOfInformationEnum.CannotBeNull();
			this.NextOfKin = this.NextOfKin.CannotBeNull();
			this.NextOfKinRelationship = this.NextOfKinRelationship.CannotBeNull();
			this.EmergencyContactName = this.EmergencyContactName.TrimAndCannotBeEmpty();
			this.EmergencyMobile = this.EmergencyMobile.TrimAndCannotBeEmpty();
			this.EmergencyPhone = this.EmergencyPhone.TrimAndCannotBeEmpty();
			this.Religion = this.Religion.TrimAndCannotBeEmpty();
			this.PhysicalDisability = this.PhysicalDisability.ToNullIfWhiteSpaceOrNA();
			this.PakistanBarCouncilNo = this.PakistanBarCouncilNo.ToNullIfWhiteSpaceOrNA();

			this.FatherName = this.FatherName.TrimAndCannotEmptyAndMustBeUpperCase();
			if (this.ForeignStudent)
			{
				this.FatherCNIC = this.FatherCNIC.MustBeNullOrEmpty();
				this.FatherPassportNo = this.IsFatherAlive ? this.FatherPassportNo.TrimAndCannotBeEmpty() : this.FatherPassportNo.Trim();
				this.FatherServiceNo = this.FatherServiceNo.MustBeNullOrEmpty();
			}
			else
			{
				this.FatherCNIC = this.IsFatherAlive?this.FatherCNIC.MustBeCNIC():this.FatherCNIC;
				this.FatherPassportNo = this.FatherPassportNo.MustBeNullOrEmpty();
				this.FatherServiceNo = this.FatherServiceNo.TrimAndMakeItNullIfEmpty();
			}

			this.FatherDesignation = this.FatherDesignation.TrimAndMakeItNullIfEmpty();
			this.FatherDepartment = this.FatherDepartment.TrimAndMakeItNullIfEmpty();
			this.FatherOrganization = this.FatherOrganization.TrimAndMakeItNullIfEmpty();
			this.FatherOfficePhone = this.FatherOfficePhone.TrimAndMakeItNullIfEmpty();
			this.FatherHomePhone = this.FatherHomePhone.TrimAndMakeItNullIfEmpty();
			this.FatherMobile = this.FatherMobile.TrimAndMakeItNullIfEmpty();
			this.FatherFax = this.FatherFax.TrimAndMakeItNullIfEmpty();

			this.SponsoredByEnum = this.SponsoredByEnum.CannotBeNull();

			switch (this.SponsoredByEnum.Value)
			{
				case Sponsorships.Father:
					this.SponsorName = this.SponsorName.MustBeNullOrEmpty();
					this.SponsorCNIC = this.SponsorCNIC.MustBeNullOrEmpty();
					this.SponsorPassportNo = this.SponsorPassportNo.MustBeNullOrEmpty();
					this.SponsorRelationshipWithGuardian = this.SponsorRelationshipWithGuardian.MustBeNullOrEmpty();
					this.SponsorDesignation = this.SponsorDesignation.MustBeNullOrEmpty();
					this.SponsorDepartment = this.SponsorDepartment.MustBeNullOrEmpty();
					this.SponsorOrganization = this.SponsorOrganization.MustBeNullOrEmpty();
					this.SponsorMobile = this.SponsorMobile.MustBeNullOrEmpty();
					this.SponsorPhoneOffice = this.SponsorPhoneOffice.MustBeNullOrEmpty();
					this.SponsorPhoneHome = this.SponsorPhoneHome.MustBeNullOrEmpty();
					this.SponsorFax = this.SponsorFax.MustBeNullOrEmpty();
					this.SponsorServiceNo = this.SponsorServiceNo.MustBeNullOrEmpty();
					if (this.ForeignStudent)
					{
						this.AnnualFamilyIncome = this.AnnualFamilyIncome.MustBeNullOrEmpty();
					}
					else
					{
						this.AnnualFamilyIncome = this.AnnualFamilyIncome.TrimAndCannotBeEmpty();
					}
					break;
				case Sponsorships.Self:
					this.SponsorName = this.SponsorName.MustBeNullOrEmpty();
					this.SponsorCNIC = this.SponsorCNIC.MustBeNullOrEmpty();
					this.SponsorPassportNo = this.SponsorPassportNo.MustBeNullOrEmpty();
					this.SponsorRelationshipWithGuardian = this.SponsorRelationshipWithGuardian.MustBeNullOrEmpty();
					this.SponsorDesignation = this.SponsorDesignation.TrimAndCannotBeEmpty();
					this.SponsorDepartment = this.SponsorDepartment.TrimAndCannotBeEmpty();
					this.SponsorOrganization = this.SponsorOrganization.TrimAndCannotBeEmpty();
					this.SponsorMobile = this.SponsorMobile.MustBeNullOrEmpty();
					this.SponsorPhoneOffice = this.SponsorPhoneOffice.TrimAndCannotBeEmpty();
					this.SponsorPhoneHome = this.SponsorPhoneHome.MustBeNullOrEmpty();
					this.SponsorFax = this.SponsorFax.TrimAndMakeItNullIfEmpty();
					if (this.ForeignStudent)
					{
						this.SponsorServiceNo = this.SponsorServiceNo.MustBeNullOrEmpty();
						this.AnnualFamilyIncome = this.AnnualFamilyIncome.MustBeNullOrEmpty();
					}
					else
					{
						this.SponsorServiceNo = this.SponsorServiceNo.TrimAndMakeItNullIfEmpty();
						this.AnnualFamilyIncome = this.AnnualFamilyIncome.TrimAndCannotBeEmpty();
					}
					break;
				case Sponsorships.Guardian:
					this.SponsorName = this.SponsorName.TrimAndCannotBeEmpty();
					if (this.ForeignStudent)
					{
						this.SponsorCNIC = this.SponsorCNIC.MustBeNullOrEmpty();
						this.SponsorPassportNo = this.SponsorPassportNo.TrimAndCannotBeEmpty();
						this.SponsorServiceNo = this.SponsorServiceNo.MustBeNullOrEmpty();
						this.AnnualFamilyIncome = this.AnnualFamilyIncome.MustBeNullOrEmpty();
					}
					else
					{
						this.SponsorCNIC = this.SponsorCNIC.MustBeCNIC();
						this.SponsorPassportNo = this.SponsorPassportNo.MustBeNullOrEmpty();
						this.SponsorServiceNo = this.SponsorServiceNo.TrimAndMakeItNullIfEmpty();
						this.AnnualFamilyIncome = this.AnnualFamilyIncome.TrimAndCannotBeEmpty();
					}
					this.SponsorRelationshipWithGuardian = this.SponsorRelationshipWithGuardian.TrimAndCannotBeEmpty();
					this.SponsorDesignation = this.SponsorDesignation.TrimAndCannotBeEmpty();
					this.SponsorDepartment = this.SponsorDepartment.TrimAndCannotBeEmpty();
					this.SponsorOrganization = this.SponsorOrganization.TrimAndCannotBeEmpty();
					this.SponsorMobile = this.SponsorMobile.TrimAndCannotBeEmpty();
					this.SponsorPhoneOffice = this.SponsorPhoneOffice.TrimAndMakeItNullIfEmpty();
					this.SponsorPhoneHome = this.SponsorPhoneHome.TrimAndCannotBeEmpty();
					this.SponsorFax = this.SponsorFax.TrimAndMakeItNullIfEmpty();
					break;
				case Sponsorships.Organization:
					this.SponsorName = this.SponsorName.TrimAndCannotBeEmpty();
					this.SponsorCNIC = this.SponsorCNIC.MustBeNullOrEmpty();
					this.SponsorPassportNo = this.SponsorPassportNo.MustBeNullOrEmpty();
					this.SponsorRelationshipWithGuardian = this.SponsorRelationshipWithGuardian.TrimAndCannotBeEmpty();
					this.SponsorDesignation = this.SponsorDesignation.MustBeNullOrEmpty();
					this.SponsorDepartment = this.SponsorDepartment.MustBeNullOrEmpty();
					this.SponsorOrganization = this.SponsorOrganization.MustBeNullOrEmpty();
					this.SponsorMobile = this.SponsorMobile.MustBeNullOrEmpty();
					this.SponsorPhoneOffice = this.SponsorPhoneOffice.TrimAndCannotBeEmpty();
					this.SponsorPhoneHome = this.SponsorPhoneHome.MustBeNullOrEmpty();
					this.SponsorFax = this.SponsorFax.TrimAndMakeItNullIfEmpty();
					this.SponsorServiceNo = this.SponsorServiceNo.MustBeNullOrEmpty();
					this.AnnualFamilyIncome = this.AnnualFamilyIncome.MustBeNullOrEmpty();
					break;
				default:
					throw new NotImplementedEnumException(this.SponsoredByEnum);
			}

			switch (this.CategoryEnum)
			{
				case Categories.NavalRetired:
				case Categories.NavalServing:
				case Categories.Naval:
					(this.ServiceNo ?? this.FatherServiceNo ?? this.SponsorServiceNo).CannotBeEmptyOrWhitespace();
					break;
				case Categories.Others:
					break;
				default:
					throw new NotImplementedEnumException(this.CategoryEnum);
			}
		}

		public bool CanEdit(string fieldName)
		{
			switch (this.EditStatusEnum)
			{
				case EditStatuses.CanEdit:
					switch (fieldName)
					{
						case nameof(this.Email):
							return false;
						case nameof(this.PictureSystemFolderID):
						case nameof(this.Name):
						case nameof(this.BURegistrationNo):
						case nameof(this.CNIC):
						case nameof(this.PassportNo):
						case nameof(this.Gender):
						case nameof(this.Category):
						case nameof(this.DOB):
						case nameof(this.BloodGroup):
						case nameof(this.Mobile):
						case nameof(this.Phone):
						case nameof(this.Nationality):
						case nameof(this.Country):
						case nameof(this.Province):
						case nameof(this.District):
						case nameof(this.Tehsil):
						case nameof(this.Domicile):
						case nameof(this.CurrentAddress):
						case nameof(this.PermanentAddress):
						case nameof(this.SourceOfInformation):
						case nameof(this.NextOfKin):
						case nameof(this.NextOfKinRelationship):
						case nameof(this.EmergencyContactName):
						case nameof(this.EmergencyMobile):
						case nameof(this.EmergencyPhone):
						case nameof(this.ETSScorer):
						case nameof(this.Religion):
						case nameof(this.PhysicalDisability):
						case nameof(this.AreaType):
						case nameof(this.ServiceNo):
						case nameof(this.PakistanBarCouncilNo):
						case nameof(this.NTNOrCNIC):
						case nameof(this.IsFatherAlive):
						case nameof(this.FatherName):
						case nameof(this.FatherCNIC):
						case nameof(this.FatherPassportNo):
						case nameof(this.FatherDesignation):
						case nameof(this.FatherDepartment):
						case nameof(this.FatherOrganization):
						case nameof(this.FatherMobile):
						case nameof(this.FatherOfficePhone):
						case nameof(this.FatherHomePhone):
						case nameof(this.FatherFax):
						case nameof(this.FatherServiceNo):
						case nameof(this.SponsoredBy):
						case nameof(this.SponsorName):
						case nameof(this.SponsorCNIC):
						case nameof(this.SponsorPassportNo):
						case nameof(this.SponsorRelationshipWithGuardian):
						case nameof(this.SponsorDesignation):
						case nameof(this.SponsorDepartment):
						case nameof(this.SponsorOrganization):
						case nameof(this.SponsorMobile):
						case nameof(this.SponsorPhoneHome):
						case nameof(this.SponsorPhoneOffice):
						case nameof(this.SponsorFax):
						case nameof(this.SponsorServiceNo):
						case nameof(this.AnnualFamilyIncome):
							return true;
						default:
							throw new ArgumentOutOfRangeException(nameof(fieldName), fieldName, null);
					}
				case EditStatuses.CanNotEditBasicInfo:
					switch (fieldName)
					{
						case nameof(this.PictureSystemFolderID):
						case nameof(this.Name):
						case nameof(this.Email):
						case nameof(this.CNIC):
						case nameof(this.PassportNo):
							return false;
						case nameof(this.BURegistrationNo):
						case nameof(this.Gender):
						case nameof(this.Category):
						case nameof(this.DOB):
						case nameof(this.BloodGroup):
						case nameof(this.Mobile):
						case nameof(this.Phone):
						case nameof(this.Nationality):
						case nameof(this.Country):
						case nameof(this.Province):
						case nameof(this.District):
						case nameof(this.Tehsil):
						case nameof(this.Domicile):
						case nameof(this.CurrentAddress):
						case nameof(this.PermanentAddress):
						case nameof(this.SourceOfInformation):
						case nameof(this.NextOfKin):
						case nameof(this.NextOfKinRelationship):
						case nameof(this.EmergencyContactName):
						case nameof(this.EmergencyMobile):
						case nameof(this.EmergencyPhone):
						case nameof(this.ETSScorer):
						case nameof(this.Religion):
						case nameof(this.PhysicalDisability):
						case nameof(this.AreaType):
						case nameof(this.ServiceNo):
						case nameof(this.PakistanBarCouncilNo):
						case nameof(this.NTNOrCNIC):
						case nameof(this.IsFatherAlive):
						case nameof(this.FatherName):
						case nameof(this.FatherCNIC):
						case nameof(this.FatherPassportNo):
						case nameof(this.FatherDesignation):
						case nameof(this.FatherDepartment):
						case nameof(this.FatherOrganization):
						case nameof(this.FatherMobile):
						case nameof(this.FatherOfficePhone):
						case nameof(this.FatherHomePhone):
						case nameof(this.FatherFax):
						case nameof(this.FatherServiceNo):
						case nameof(this.SponsoredBy):
						case nameof(this.SponsorName):
						case nameof(this.SponsorCNIC):
						case nameof(this.SponsorPassportNo):
						case nameof(this.SponsorRelationshipWithGuardian):
						case nameof(this.SponsorDesignation):
						case nameof(this.SponsorDepartment):
						case nameof(this.SponsorOrganization):
						case nameof(this.SponsorMobile):
						case nameof(this.SponsorPhoneHome):
						case nameof(this.SponsorPhoneOffice):
						case nameof(this.SponsorFax):
						case nameof(this.SponsorServiceNo):
						case nameof(this.AnnualFamilyIncome):
							return false;
						default:
							throw new ArgumentOutOfRangeException(nameof(fieldName), fieldName, null);
					}
				case EditStatuses.CanEditContactInformation:
					switch (fieldName)
					{
						case nameof(this.PictureSystemFolderID):
						case nameof(this.Name):
						case nameof(this.Email):
						case nameof(this.BURegistrationNo):
						case nameof(this.CNIC):
						case nameof(this.PassportNo):
						case nameof(this.Gender):
						case nameof(this.Category):
						case nameof(this.DOB):
						case nameof(this.BloodGroup):
							return false;
						case nameof(this.Mobile):
						case nameof(this.Phone):
							return true;
						case nameof(this.Nationality):
						case nameof(this.Country):
						case nameof(this.Province):
						case nameof(this.District):
						case nameof(this.Tehsil):
						case nameof(this.Domicile):
							return false;
						case nameof(this.CurrentAddress):
						case nameof(this.PermanentAddress):
							return true;
						case nameof(this.SourceOfInformation):
						case nameof(this.NextOfKin):
						case nameof(this.NextOfKinRelationship):
						case nameof(this.EmergencyContactName):
						case nameof(this.EmergencyMobile):
						case nameof(this.EmergencyPhone):
						case nameof(this.ETSScorer):
						case nameof(this.Religion):
						case nameof(this.PhysicalDisability):
						case nameof(this.AreaType):
						case nameof(this.ServiceNo):
						case nameof(this.PakistanBarCouncilNo):
						case nameof(this.NTNOrCNIC):
						case nameof(this.IsFatherAlive):
						case nameof(this.FatherName):
						case nameof(this.FatherCNIC):
						case nameof(this.FatherPassportNo):
						case nameof(this.FatherDesignation):
						case nameof(this.FatherDepartment):
						case nameof(this.FatherOrganization):
						case nameof(this.FatherMobile):
						case nameof(this.FatherOfficePhone):
						case nameof(this.FatherHomePhone):
						case nameof(this.FatherFax):
						case nameof(this.FatherServiceNo):
						case nameof(this.SponsoredBy):
						case nameof(this.SponsorName):
						case nameof(this.SponsorCNIC):
						case nameof(this.SponsorPassportNo):
						case nameof(this.SponsorRelationshipWithGuardian):
						case nameof(this.SponsorDesignation):
						case nameof(this.SponsorDepartment):
						case nameof(this.SponsorOrganization):
						case nameof(this.SponsorMobile):
						case nameof(this.SponsorPhoneHome):
						case nameof(this.SponsorPhoneOffice):
						case nameof(this.SponsorFax):
						case nameof(this.SponsorServiceNo):
						case nameof(this.AnnualFamilyIncome):
							return false;
						default:
							throw new ArgumentOutOfRangeException(nameof(fieldName), fieldName, null);
					}
				case EditStatuses.Readonly:
					switch (fieldName)
					{
						case nameof(this.PictureSystemFolderID):
						case nameof(this.Name):
						case nameof(this.Email):
						case nameof(this.BURegistrationNo):
						case nameof(this.CNIC):
						case nameof(this.PassportNo):
						case nameof(this.Gender):
						case nameof(this.Category):
						case nameof(this.DOB):
						case nameof(this.BloodGroup):
						case nameof(this.Mobile):
						case nameof(this.Phone):
						case nameof(this.Nationality):
						case nameof(this.Country):
						case nameof(this.Province):
						case nameof(this.District):
						case nameof(this.Tehsil):
						case nameof(this.Domicile):
						case nameof(this.CurrentAddress):
						case nameof(this.PermanentAddress):
						case nameof(this.SourceOfInformation):
						case nameof(this.NextOfKin):
						case nameof(this.NextOfKinRelationship):
						case nameof(this.EmergencyContactName):
						case nameof(this.EmergencyMobile):
						case nameof(this.EmergencyPhone):
						case nameof(this.ETSScorer):
						case nameof(this.Religion):
						case nameof(this.PhysicalDisability):
						case nameof(this.AreaType):
						case nameof(this.ServiceNo):
						case nameof(this.PakistanBarCouncilNo):
						case nameof(this.NTNOrCNIC):
						case nameof(this.IsFatherAlive):
						case nameof(this.FatherName):
						case nameof(this.FatherCNIC):
						case nameof(this.FatherPassportNo):
						case nameof(this.FatherDesignation):
						case nameof(this.FatherDepartment):
						case nameof(this.FatherOrganization):
						case nameof(this.FatherMobile):
						case nameof(this.FatherOfficePhone):
						case nameof(this.FatherHomePhone):
						case nameof(this.FatherFax):
						case nameof(this.FatherServiceNo):
						case nameof(this.SponsoredBy):
						case nameof(this.SponsorName):
						case nameof(this.SponsorCNIC):
						case nameof(this.SponsorPassportNo):
						case nameof(this.SponsorRelationshipWithGuardian):
						case nameof(this.SponsorDesignation):
						case nameof(this.SponsorDepartment):
						case nameof(this.SponsorOrganization):
						case nameof(this.SponsorMobile):
						case nameof(this.SponsorPhoneHome):
						case nameof(this.SponsorPhoneOffice):
						case nameof(this.SponsorFax):
						case nameof(this.SponsorServiceNo):
						case nameof(this.AnnualFamilyIncome):
							return false;
						default:
							throw new ArgumentOutOfRangeException(nameof(fieldName), fieldName, null);
					}
				default:
					throw new NotImplementedEnumException(this.EditStatusEnum);
			}
		}

		public void CopyValues(Candidate fromCandidate)
		{
			//if (this.CanEdit(nameof(this.Email)))
			//	throw new InvalidOperationException();
			//if (this.CanEdit(nameof(this.PictureSystemFolderID)))
			//	throw new InvalidOperationException();
			if (this.CanEdit(nameof(this.Name)))
				this.Name = fromCandidate.Name;
			if (this.CanEdit(nameof(this.BURegistrationNo)))
				this.BURegistrationNo = fromCandidate.BURegistrationNo;
			if (this.CanEdit(nameof(this.CNIC)))
				this.CNIC = fromCandidate.CNIC;
			if (this.CanEdit(nameof(this.PassportNo)))
				this.PassportNo = fromCandidate.PassportNo;
			if (this.CanEdit(nameof(this.Gender)))
				this.Gender = fromCandidate.Gender;
			if (this.CanEdit(nameof(this.Category)))
				this.Category = fromCandidate.Category;
			if (this.CanEdit(nameof(this.DOB)))
				this.DOB = fromCandidate.DOB;
			if (this.CanEdit(nameof(this.BloodGroup)))
				this.BloodGroup = fromCandidate.BloodGroup;
			if (this.CanEdit(nameof(this.Mobile)))
				this.Mobile = fromCandidate.Mobile;
			if (this.CanEdit(nameof(this.Phone)))
				this.Phone = fromCandidate.Phone;
			if (this.CanEdit(nameof(this.Nationality)))
				this.Nationality = fromCandidate.Nationality;
			if (this.CanEdit(nameof(this.Country)))
				this.Country = fromCandidate.Country;
			if (this.CanEdit(nameof(this.Province)))
				this.Province = fromCandidate.Province;
			if (this.CanEdit(nameof(this.District)))
				this.District = fromCandidate.District;
			if (this.CanEdit(nameof(this.Tehsil)))
				this.Tehsil = fromCandidate.Tehsil;
			if (this.CanEdit(nameof(this.Domicile)))
				this.Domicile = fromCandidate.Domicile;
			if (this.CanEdit(nameof(this.CurrentAddress)))
				this.CurrentAddress = fromCandidate.CurrentAddress;
			if (this.CanEdit(nameof(this.PermanentAddress)))
				this.PermanentAddress = fromCandidate.PermanentAddress;
			if (this.CanEdit(nameof(this.SourceOfInformation)))
				this.SourceOfInformation = fromCandidate.SourceOfInformation;
			if (this.CanEdit(nameof(this.NextOfKin)))
				this.NextOfKin = fromCandidate.NextOfKin;
			if (this.CanEdit(nameof(this.NextOfKinRelationship)))
				this.NextOfKinRelationship = fromCandidate.NextOfKinRelationship;
			if (this.CanEdit(nameof(this.EmergencyContactName)))
				this.EmergencyContactName = fromCandidate.EmergencyContactName;
			if (this.CanEdit(nameof(this.EmergencyMobile)))
				this.EmergencyMobile = fromCandidate.EmergencyMobile;
			if (this.CanEdit(nameof(this.EmergencyPhone)))
				this.EmergencyPhone = fromCandidate.EmergencyPhone;
			if (this.CanEdit(nameof(this.ETSScorer)))
				this.ETSScorer = fromCandidate.ETSScorer;
			if (this.CanEdit(nameof(this.Religion)))
				this.Religion = fromCandidate.Religion;
			if (this.CanEdit(nameof(this.PhysicalDisability)))
				this.PhysicalDisability = fromCandidate.PhysicalDisability;
			if (this.CanEdit(nameof(this.AreaType)))
				this.AreaType = fromCandidate.AreaType;
			if (this.CanEdit(nameof(this.ServiceNo)))
				this.ServiceNo = fromCandidate.ServiceNo;
			if (this.CanEdit(nameof(this.PakistanBarCouncilNo)))
				this.PakistanBarCouncilNo = fromCandidate.PakistanBarCouncilNo;
			if (this.CanEdit(nameof(this.NTNOrCNIC)))
				this.NTNOrCNIC = fromCandidate.NTNOrCNIC;
			if(this.CanEdit(nameof(this.IsFatherAlive)))
				this.IsFatherAlive = fromCandidate.IsFatherAlive;
			if (this.CanEdit(nameof(this.FatherName)))
				this.FatherName = fromCandidate.FatherName;
			if (this.CanEdit(nameof(this.FatherCNIC)))
				this.FatherCNIC = fromCandidate.FatherCNIC;
			if (this.CanEdit(nameof(this.FatherPassportNo)))
				this.FatherPassportNo = fromCandidate.FatherPassportNo;
			if (this.CanEdit(nameof(this.FatherDesignation)))
				this.FatherDesignation = fromCandidate.FatherDesignation;
			if (this.CanEdit(nameof(this.FatherDepartment)))
				this.FatherDepartment = fromCandidate.FatherDepartment;
			if (this.CanEdit(nameof(this.FatherOrganization)))
				this.FatherOrganization = fromCandidate.FatherOrganization;
			if (this.CanEdit(nameof(this.FatherMobile)))
				this.FatherMobile = fromCandidate.FatherMobile;
			if (this.CanEdit(nameof(this.FatherOfficePhone)))
				this.FatherOfficePhone = fromCandidate.FatherOfficePhone;
			if (this.CanEdit(nameof(this.FatherHomePhone)))
				this.FatherHomePhone = fromCandidate.FatherHomePhone;
			if (this.CanEdit(nameof(this.FatherFax)))
				this.FatherFax = fromCandidate.FatherFax;
			if (this.CanEdit(nameof(this.FatherServiceNo)))
				this.FatherServiceNo = fromCandidate.FatherServiceNo;
			if (this.CanEdit(nameof(this.SponsoredBy)))
				this.SponsoredBy = fromCandidate.SponsoredBy;
			if (this.CanEdit(nameof(this.SponsorName)))
				this.SponsorName = fromCandidate.SponsorName;
			if (this.CanEdit(nameof(this.SponsorCNIC)))
				this.SponsorCNIC = fromCandidate.SponsorCNIC;
			if (this.CanEdit(nameof(this.SponsorPassportNo)))
				this.SponsorPassportNo = fromCandidate.SponsorPassportNo;
			if (this.CanEdit(nameof(this.SponsorRelationshipWithGuardian)))
				this.SponsorRelationshipWithGuardian = fromCandidate.SponsorRelationshipWithGuardian;
			if (this.CanEdit(nameof(this.SponsorDesignation)))
				this.SponsorDesignation = fromCandidate.SponsorDesignation;
			if (this.CanEdit(nameof(this.SponsorDepartment)))
				this.SponsorDepartment = fromCandidate.SponsorDepartment;
			if (this.CanEdit(nameof(this.SponsorOrganization)))
				this.SponsorOrganization = fromCandidate.SponsorOrganization;
			if (this.CanEdit(nameof(this.SponsorMobile)))
				this.SponsorMobile = fromCandidate.SponsorMobile;
			if (this.CanEdit(nameof(this.SponsorPhoneHome)))
				this.SponsorPhoneHome = fromCandidate.SponsorPhoneHome;
			if (this.CanEdit(nameof(this.SponsorPhoneOffice)))
				this.SponsorPhoneOffice = fromCandidate.SponsorPhoneOffice;
			if (this.CanEdit(nameof(this.SponsorFax)))
				this.SponsorFax = fromCandidate.SponsorFax;
			if (this.CanEdit(nameof(this.SponsorServiceNo)))
				this.SponsorServiceNo = fromCandidate.SponsorServiceNo;
			if (this.CanEdit(nameof(this.AnnualFamilyIncome)))
				this.AnnualFamilyIncome = fromCandidate.AnnualFamilyIncome;
			this.Validate();
		}
	}
}
