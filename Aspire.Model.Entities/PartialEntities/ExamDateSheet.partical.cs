﻿namespace Aspire.Model.Entities
{
	public partial class ExamDateSheet
	{
		public enum ValidationResult
		{
			InvalidData,
			Success,
		}

		public ValidationResult Validate()
		{
			if (this.QuestionPaperReceivedDate == null
				&& this.AnswerSheetsReceivedByExaminerDate == null
				&& this.AnswerSheetsReceivedByExaminerCount == null
				&& this.AnswerSheetsReceivedByExamCellDate == null
				&& this.AnswerSheetsReceivedByExamCellCount == null)
				return ValidationResult.Success;

			if (this.QuestionPaperReceivedDate != null
				&& this.AnswerSheetsReceivedByExaminerDate == null
				&& this.AnswerSheetsReceivedByExaminerCount == null
				&& this.AnswerSheetsReceivedByExamCellDate == null
				&& this.AnswerSheetsReceivedByExamCellCount == null)
				return ValidationResult.Success;

			if (this.QuestionPaperReceivedDate != null
				&& this.AnswerSheetsReceivedByExaminerDate != null
				&& this.AnswerSheetsReceivedByExaminerCount >= 1
				&& this.QuestionPaperReceivedDate.Value <= this.AnswerSheetsReceivedByExaminerDate.Value
				&& this.AnswerSheetsReceivedByExamCellDate == null
				&& this.AnswerSheetsReceivedByExamCellCount == null)
				return ValidationResult.Success;

			if (this.QuestionPaperReceivedDate != null
				&& this.AnswerSheetsReceivedByExaminerDate != null
				&& this.AnswerSheetsReceivedByExaminerCount >= 1
				&& this.QuestionPaperReceivedDate.Value <= this.AnswerSheetsReceivedByExaminerDate.Value
				&& this.AnswerSheetsReceivedByExamCellDate != null
				&& this.AnswerSheetsReceivedByExamCellCount >= 1
				&& this.AnswerSheetsReceivedByExaminerDate.Value <= this.AnswerSheetsReceivedByExamCellDate.Value
				&& this.AnswerSheetsReceivedByExaminerCount >= this.AnswerSheetsReceivedByExamCellCount)
				return ValidationResult.Success;

			return ValidationResult.InvalidData;
		}
	}
}