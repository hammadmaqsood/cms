﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class UserFeedbackDiscussion
	{
		[EnumGuids]
		public enum Statuses
		{
			[EnumGuidValue("9be46be9-d3b4-4a56-a455-1869492cbf95", "Visible")]
			Visible,
			[EnumGuidValue("287f84c6-81e7-481a-8446-52b7451e7729", "Hidden")]
			Hidden
		}

		public Statuses StatusEnum
		{
			get => this.Status.GetEnum<Statuses>();
			set => value.GetGuid();
		}
	}
}