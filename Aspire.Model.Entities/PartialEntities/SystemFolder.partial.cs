﻿using Aspire.Lib.Extensions;

namespace Aspire.Model.Entities
{
	public partial class SystemFolder
	{
		public enum FolderTypes
		{
			Custom,
			Temporary,
			CandidatePicture,
			StudentPicture,
			OfficialDocuments,
			StudentDocuments,
			CandidateForeignAcademicRecord,
			CandidateForeignDocuments
		}

		public FolderTypes FolderTypeEnum
		{
			get => this.FolderType.ToEnum<FolderTypes>();
			set => this.FolderType = value.ToString();
		}
	}
}
