﻿namespace Aspire.Model.Entities
{
	public partial class QASurveySummaryAnalysi
	{
		public enum Statuses : byte
		{
			NotStarted = 1,
			InProgress = 2,
			Completed = 3
		}

		public Statuses SummaryStatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}

		public string StatusFullName => this.SummaryStatusEnum.ToString();

		public enum ValidationResults
		{
			None,
			NoRecordFound,
			AlreadyExists,
			Success,
			SummaryGenerated,
			StaffCannotEditThisRecord
		}
	}
}
