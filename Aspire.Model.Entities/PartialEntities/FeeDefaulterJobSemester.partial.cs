﻿namespace Aspire.Model.Entities
{
	public partial class FeeDefaulterJobSemester
	{
		public enum Statuses
		{
			Active = 1,
			Inactive = 2,
		}

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}
	}
}