﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Model.Entities
{
	public partial class UserRoleModule
	{
		public AspireModules ModuleEnum
		{
			get { return (AspireModules)this.Module; }
			set { this.Module = (byte)value; }
		}

		public AspireModules ModuleNameEnum
		{
			get { return this.ModuleName.ToEnum<AspireModules>(); }
			set { this.ModuleName = value.ToString(); }
		}

		public UserRoleModule Validate()
		{
			if (this.ModuleEnum != this.ModuleNameEnum)
				throw new InvalidOperationException();
			return this;
		}

		public bool TryValidate()
		{
			try
			{
				this.Validate();
				return true;
			}
			catch (InvalidOperationException)
			{
				return false;
			}
		}

		public UserRoleModule Clone()
		{
			return new UserRoleModule
			{
				UserRoleID = this.UserRoleID,
				ModuleEnum = this.ModuleNameEnum,
				ModuleNameEnum = this.ModuleNameEnum,
				DepartmentID = this.DepartmentID,
				ProgramID = this.ProgramID,
				AdmissionOpenProgramID = this.AdmissionOpenProgramID,
			}.Validate();
		}
	}
}

