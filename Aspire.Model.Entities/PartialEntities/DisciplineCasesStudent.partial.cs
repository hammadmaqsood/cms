﻿using System;

namespace Aspire.Model.Entities
{
	public partial class DisciplineCasesStudent
	{

		[Flags]
		public enum RemarksTypes : byte
		{
			Fine = 1,
			WarningLetter = 2,
			Counselled = 4,
		}

		public RemarksTypes? RemarksTypeEnum
		{
			get { return (RemarksTypes?)this.RemarksType; }
			set { this.RemarksType = (byte?)value; }
		}
	}
}
