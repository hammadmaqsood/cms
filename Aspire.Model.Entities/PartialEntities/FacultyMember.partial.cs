﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class FacultyMember
	{
		public enum Titles : byte
		{
			Mr = 1,
			Miss = 2,
			Mrs = 3,
			Dr = 4,
			Ms = 5,
			Prof = 6
		}

		public enum Statuses : byte
		{
			Active = 1,
			Inactive = 2,
		}

		public enum FacultyTypes : byte
		{
			Permanent = 1,
			Visiting = 2,
		}

		public Titles? TitleEnum
		{
			get => (Titles?)this.Title;
			set => this.Title = (byte?)value;
		}

		public Genders GenderEnum
		{
			get => (Genders)this.Gender;
			set => this.Gender = (byte)value;
		}

		public FacultyTypes FacultyTypeEnum
		{
			get => (FacultyTypes)this.FacultyType;
			set => this.FacultyType = (byte)value;
		}

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}

		public string FullName => this.TitleEnum == null ? this.Name : $"{this.TitleEnum}. {this.Name}";

		public string FullDisplayName => string.IsNullOrWhiteSpace(this.DisplayName) ? this.FullName : this.DisplayName;
	}
}
