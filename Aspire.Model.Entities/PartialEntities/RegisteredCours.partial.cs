﻿using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Model.Entities
{
	public partial class RegisteredCours
	{
		public enum Statuses : byte
		{
			AttendanceDefaulter = 1,
			Deferred = 2,
			Incomplete = 4,
			WithdrawWithFee = 8,
			WithdrawWithoutFee = 16,
			UnfairMeans = 32
		}

		public Statuses? StatusEnum
		{
			get => (Statuses?)this.Status;
			set => this.Status = (byte?)value;
		}

		public FreezedStatuses? FreezeStatusEnum
		{
			get => (FreezedStatuses?)this.FreezedStatus;
			set => this.FreezedStatus = (byte?)value;
		}


		public ExamGrades? GradeEnum
		{
			get => (ExamGrades?)this.Grade;
			set => this.Grade = (byte?)value;
		}

		[Flags]
		public enum BypassedCheckTypes : byte
		{
			None = 0,
			PreRequisites = 1,
			ImprovementCheck = 2,
			RegistrationOpenCheck = 4,
			MaxAllowedCoursesPerSemester = 8,
			MaxAllowedCreditHoursPerSemester = 16
		}

		public BypassedCheckTypes BypassedChecksEnum
		{
			get => (BypassedCheckTypes)this.BypassedChecks;
			set => this.BypassedChecks = (byte)value;
		}
	}
}
