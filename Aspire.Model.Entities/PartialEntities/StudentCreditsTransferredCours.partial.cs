﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Model.Entities
{
	public partial class StudentCreditsTransferredCours
	{
		public ExamGrades? EquivalentGradeEnum
		{
			get => (ExamGrades?)this.EquivalentGrade;
			set => this.EquivalentGrade = (byte?)value;
		}

		public StudentCreditsTransferredCours Validate()
		{
			this.CourseTitle = this.CourseTitle.TrimAndCannotBeEmpty();
			this.Grade = this.Grade.TrimAndCannotBeEmpty();
			if (this.EquivalentCourseID == null && this.EquivalentGrade == null)
				return this;
			if (this.EquivalentCourseID != null && this.EquivalentGrade != null)
				return this;
			throw new InvalidOperationException("Both EquivalentCourseID and EquivalentGrade should be null or null.");
		}
	}
}
