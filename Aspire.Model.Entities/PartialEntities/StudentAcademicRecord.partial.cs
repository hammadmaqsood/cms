﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Model.Entities
{
	public partial class StudentAcademicRecord
	{
		public enum Statuses : byte
		{
			Objection = 1,
			Pending = 2,
			Verified = 3,
		}

		[Flags]
		public enum VerificationStatuses : byte
		{
			None = 0,
			Uploaded = 1,
			Verified = 2,
		}

		public VerificationStatuses? VerificationStatusEnum
		{
			get => (VerificationStatuses?)this.VerificationStatus;
			set => this.VerificationStatus = (byte?)value;
		}

		public DegreeTypes DegreeTypeEnum
		{
			get => (DegreeTypes)this.DegreeType;
			set => this.DegreeType = (byte)value;
		}

		public Statuses? StatusEnum
		{
			get => (Statuses?)this.Status;
			set => this.Status = (byte?)value;
		}

		public string StatusFullName => this.StatusEnum?.ToFullName();

		public StudentAcademicRecord Validate()
		{
			if (this.ObtainedMarks > this.TotalMarks)
				throw new InvalidOperationException();
			// ReSharper disable once CompareOfFloatsByEqualityOperator
			if (this.TotalMarks == 0)
				throw new InvalidOperationException();
			this.Percentage = this.ObtainedMarks * 100f / this.TotalMarks;
			this.Subjects = this.Subjects.TrimAndCannotBeEmpty();
			this.Institute = this.Institute.TrimAndCannotBeEmpty();
			this.BoardUniversity = this.BoardUniversity.TrimAndCannotBeEmpty();
			this.Remarks = this.Remarks.TrimAndMakeItNullIfEmpty();
			return this;
		}
	}
}
