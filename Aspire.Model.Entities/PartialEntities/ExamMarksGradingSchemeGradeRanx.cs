﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class ExamMarksGradingSchemeGradeRanx
	{
		public ExamGrades GradeEnum
		{
			get => (ExamGrades)this.Grade;
			set => this.Grade = (byte)value;
		}

		public string ExamGradesFullName => this.GradeEnum.ToFullName();

		public string RegularSemesterYesNo => this.RegularSemester.ToYesNo();
	}
}
