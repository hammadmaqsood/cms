﻿namespace Aspire.Model.Entities
{
	public partial class UserGroup
	{
		public Common.UserTypes UserTypeEnum
		{
			get { return (Common.UserTypes)this.UserType; }
			set { this.UserType = (short)value; }
		}
	}
}
