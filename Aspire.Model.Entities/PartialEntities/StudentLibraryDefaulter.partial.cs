﻿namespace Aspire.Model.Entities
{
	public partial class StudentLibraryDefaulter
	{
		public enum Statuses : byte
		{
			Allowed,
			Blocked,
		}

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}

		public static readonly byte StatusAllowed = (byte)Statuses.Allowed;
		public static readonly byte StatusBlocked = (byte)Statuses.Blocked;
	}
}
