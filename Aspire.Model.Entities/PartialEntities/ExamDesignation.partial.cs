﻿using Aspire.Lib.Extensions;

namespace Aspire.Model.Entities
{
	public partial class ExamDesignation
	{
		public ExamDesignation Validate()
		{
			this.Designation = this.Designation.TrimAndCannotBeEmpty();
			return this;
		}
	}
}
