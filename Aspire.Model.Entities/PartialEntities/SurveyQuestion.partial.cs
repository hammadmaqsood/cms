﻿using Aspire.Lib.Extensions;

namespace Aspire.Model.Entities
{
	public partial class SurveyQuestion
	{
		public enum QuestionTypes : byte
		{
			Options = 1,
			Text = 2,
		}

		public QuestionTypes QuestionTypeEnum
		{
			get { return (QuestionTypes)this.QuestionType; }
			set { this.QuestionType = (byte)value; }
		}

		public static readonly byte QuestionTypeOptions = (byte)QuestionTypes.Options;
		public static readonly byte QuestionTypeText = (byte)QuestionTypes.Text;

		public SurveyQuestion Validate()
		{
			this.QuestionNo = this.QuestionNo.TrimAndCannotBeEmpty();
			this.QuestionText = this.QuestionText.TrimAndCannotBeEmpty();
			return this;
		}
	}
}