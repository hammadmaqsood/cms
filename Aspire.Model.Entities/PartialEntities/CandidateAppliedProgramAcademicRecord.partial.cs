﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.Model.Entities
{
	public partial class CandidateAppliedProgramAcademicRecord
	{
		public DegreeTypes DegreeTypeEnum
		{
			get { return (DegreeTypes)this.DegreeType; }
			set { this.DegreeType = (byte)value; }
		}

		public CandidateAppliedProgramAcademicRecord Validate(AdmissionCriteriaPreReqDegree admissionCriteriaPreReqDegree, DateTime dateOfBirth)
		{
			if (this.AdmissionCriteriaPreReqDegreeID != admissionCriteriaPreReqDegree.AdmissionCriteriaPreReqDegreeID)
				throw new InvalidOperationException();
			if (this.ObtainedMarks < 0 || this.TotalMarks < 0)
				throw new InvalidOperationException();
			if (this.ObtainedMarks > this.TotalMarks)
				throw new InvalidOperationException();
			this.Percentage = this.ObtainedMarks * 100d / this.TotalMarks;
			if (this.Percentage < 0 || 100 < this.Percentage)
				throw new InvalidOperationException();

			if (this.IsCGPA)
			{
				if (!admissionCriteriaPreReqDegree.SemesterSystem)
					throw new InvalidOperationException();

				if (this.TotalMarks == 4d)
				{
					if (admissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf4 != null)
						if (this.ObtainedMarks < admissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf4.Value)
							throw new InvalidOperationException();
				}
				else if (this.TotalMarks == 5d)
				{
					if (admissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf5 != null)
						if (this.ObtainedMarks < admissionCriteriaPreReqDegree.SemesterSystemMinCGPAOutOf5.Value)
							throw new InvalidOperationException();
				}
				else
					throw new InvalidOperationException();
			}
			else
			{
				if (!admissionCriteriaPreReqDegree.AnnualExamSystem)
					throw new InvalidOperationException();
				if (admissionCriteriaPreReqDegree.AnnualExamSystemMinimumPercentage != null)
					if (this.Percentage < admissionCriteriaPreReqDegree.AnnualExamSystemMinimumPercentage.Value)
						throw new InvalidOperationException();
			}

			this.Subjects = this.Subjects.TrimAndCannotBeEmpty();
			if (admissionCriteriaPreReqDegree.AdmissionCriteriaPreReqDegreeSubjects.Any())
				if (!admissionCriteriaPreReqDegree.AdmissionCriteriaPreReqDegreeSubjects.Any(s => s.Subjects == this.Subjects))
					throw new InvalidOperationException();

			this.Institute = this.Institute.TrimAndCannotBeEmpty();
			this.BoardUniversity = this.BoardUniversity.TrimAndCannotBeEmpty();
			if (this.PassingYear < 1900 || this.PassingYear < dateOfBirth.Year + 12 || DateTime.Now.Year < this.PassingYear)
				throw new InvalidOperationException();

			return this;
		}
	}
}
