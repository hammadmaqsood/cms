﻿using Aspire.Lib.Extensions;

namespace Aspire.Model.Entities
{
	public partial class Department
	{
		public enum Statuses : byte
		{
			Active = 1,
			Inactive = 2,
		}

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}

		public Department Validate()
		{
			this.DepartmentName = this.DepartmentName.TrimAndCannotBeEmpty();
			this.DepartmentShortName = this.DepartmentShortName.TrimAndCannotBeEmpty();
			this.DepartmentAlias = this.DepartmentAlias.TrimAndCannotEmptyAndMustBeUpperCase();
			return this;
		}
	}
}
