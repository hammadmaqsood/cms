﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class AdmissionCriteriaPreReq
	{
		public DegreeTypes? ResultAwaitedDegreeTypeEnum
		{
			get { return (DegreeTypes?)this.ResultAwaitedDegreeType; }
			set { this.ResultAwaitedDegreeType = (byte?)value; }
		}

		public string ResultAwaitedDegreeTypeFullName => this.ResultAwaitedDegreeTypeEnum?.ToFullName();

		public AdmissionCriteriaPreReq Validate()
		{
			this.PreReqQualification = this.PreReqQualification.TrimAndCannotBeEmpty();
			this.PreReqQualificationResultStatus = this.PreReqQualificationResultStatus.TrimAndCannotBeEmpty();
			return this;
		}
	}
}
