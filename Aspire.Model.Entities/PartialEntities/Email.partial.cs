﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Model.Entities
{
	public partial class Email
	{
		public const string DefaultFromDisplayName = "Bahria University";

		public enum Priorities : byte
		{
			Highest = 1,
			VeryHigh = 2,
			High = 3,
			Normal = 4,
			Low = 5,
			VeryLow = 6,
			Lowest = 7
		}

		[EnumGuids]
		public enum Statuses
		{
			[EnumGuidValue("{210D6122-8707-41E6-8DA1-CE97774F0A4E}", "Pending")]
			Pending,
			[EnumGuidValue("{E31738D1-58CF-4178-A344-4BEC94ADD5A0}", "Success")]
			Success,
			[EnumGuidValue("{CADC1C04-C107-4239-851E-BD5AD4544998}", "Success (No Recipient)")]
			SuccessNoRecipient,
			[EnumGuidValue("{420A8310-CA19-4849-B222-5CBA2A246620}", "Expired")]
			Expired,
			[EnumGuidValue("{63828148-88D8-4655-BEE4-3920DFACB9C9}", "SMTP Server Not Available")]
			SMTPServerNotAvailable,
			[EnumGuidValue("{C93F23D0-6B00-4134-96C3-438247346B74}", "Error")]
			Error,
			[EnumGuidValue("{7115A60F-394E-4F78-BAF6-94299C4B5B74}", "Attempts Exceeded")]
			AttemptsExceeded,
		}

		[EnumGuids]
		public enum EmailTypes
		{
			[EnumGuidValue("{FD363DBB-1FB7-4DC8-94C1-614CA0064928}", "Candidate Registration")]
			CandidateRegistration,
			[EnumGuidValue("A6DBA1B2-DBB8-4B31-98EF-353D20220125", "User Password Has Been Reset")]
			UserPasswordHasBeenReset,
			[EnumGuidValue("6e6fdc8b-4f29-476d-bf52-5494566bc7cd", "User Reset Password Link By Admin")]
			UserResetPasswordLinkByAdmin,
			[EnumGuidValue("ad59e5a5-8e69-4838-8bb0-fafabefb9e6c", "Faculty Member Pin Code")]
			FacultyMemberPinCode,
			[EnumGuidValue("fed32532-3826-43d3-a942-5d14316bc294", "Fee Defaulter")]
			FeeDefaulter,
			[EnumGuidValue("cf7ad590-4330-4832-87f8-3eed1cbd33ab", "Bulk Update Registered Course Status To Attendance Defaulter")]
			BulkUpdateRegisteredCourseStatusToAttendanceDefaulter,
			[EnumGuidValue("0bd6d625-48f5-4029-8dd5-89ade3e47251", "User Account Activation")]
			UserAccountActivation,
			[EnumGuidValue("127b1fc6-eb07-4afb-be87-7a50facd9d94", "Student Confirm Personal Email")]
			StudentConfirmPersonalEmail,
			[EnumGuidValue("d880956e-09b5-45d3-947d-8a6e09e81e76", "Student Forgot Password")]
			StudentForgotPassword,
			[EnumGuidValue("fafadb1e-964b-49ce-9220-586b37f469a6", "Student Parent Reset Password Link")]
			StudentParentResetPasswordLink,
			[EnumGuidValue("3cddc4cf-7618-4427-9ad6-c4720839b901", "Student Parent Reset Password By Admin")]
			StudentParentResetPasswordByAdmin,
			[EnumGuidValue("067a2914-064e-4e03-a3aa-ab27fdb833dd", "Student Reset Password By Admin")]
			StudentResetPasswordByAdmin,
			[EnumGuidValue("cbbe43b6-3203-46a7-b7a0-6c6ad9fae6a8", "Faculty Member Password Reset By Admin Email")]
			FacultyMemberPasswordResetByAdminEmail,
			[EnumGuidValue("44862304-764f-4da9-bda1-509db919096d", "Faculty Member Password Has Been Reset By Admin")]
			FacultyMemberPasswordHasBeenResetByAdmin,
			[EnumGuidValue("ebee0b9d-8624-43ac-8edb-0f6e5fb64ddd", "Candidate Welcome Email")]
			CandidateWelcomeEmail,
			[EnumGuidValue("48cf24a1-9abe-4f52-95a1-122e610814d3", "Candidate Password Has Been Reset")]
			CandidatePasswordHasBeenReset,
			[EnumGuidValue("42f39203-31f3-4061-aa6a-17847396938f", "Candidate Forgot Password")]
			CandidateForgotPassword,
			[EnumGuidValue("139b2c59-4024-4b5d-a3df-74a63a7a950d", "User Feedback")]
			UserFeedback,
			[EnumGuidValue("05290b97-af0d-40ab-9744-e7d2d16aa226", "Merit List")]
			MeritList,
			[EnumGuidValue("C5297FC5-0FB5-4208-8C3D-D2067401860F", "Marks Sheet Changes To Faculty Member")]
			MarksSheetChangesToFacultyMember,
			[EnumGuidValue("536E7583-40B3-48A4-B0F6-D3742F7F2281", "Marks Sheet Changes To Management")]
			MarksSheetChangesToManagement,
			[EnumGuidValue("942c647c-25b4-43b2-bb25-e444b22e6282", "Test Email")]
			TestEmail,
			[EnumGuidValue("EB0F8359-B352-4329-8682-CC1E1F2DF0FB", "Project/Thesis/Internship Marks Changes To Management")]
			ProjectThesisInternshipMarksChangesToManagement,
			[EnumGuidValue("C4B09999-E090-4701-B1FC-D386E9AFCE53", "Fee Structure Updated")]
			FeeStructureUpdated,
			[EnumGuidValue("B1CEDA61-D21F-4DB9-97A6-F4518D38AB21", "Alumni Survey Form")]
			AlumniSurveyForm,
			[EnumGuidValue("EA12C00E-3A18-42D7-BBCE-6B06037F3184", "Employer Survey Form")]
			EmployerSurveyForm,
			[EnumGuidValue("6C45F73A-0320-4DF7-B4BB-80C4E9AD9873", "Student Clearance Form Confirmation Code")]
			StudentClearanceFormConfirmationCode
		}

		public Priorities PriorityEnum
		{
			get => (Priorities)this.Priority;
			set => this.Priority = (byte)value;
		}

		public EmailTypes EmailTypeEnum
		{
			get => this.EmailType.GetEnum<EmailTypes>();
			set => this.EmailType = value.GetGuid();
		}

		public Statuses StatusEnum
		{
			get => this.Status.GetEnum<Statuses>();
			set => this.Status = value.GetGuid();
		}

		public Email Validate()
		{
			this.EmailType.ValidateEnum<EmailTypes>();
			this.Subject = this.Subject.TrimAndCannotBeEmpty();
			this.Body = this.Body.TrimAndCannotBeEmpty();
			if (this.CreatedDate > this.ExpiryDate)
				throw new InvalidOperationException();
			this.FromDisplayName = this.FromDisplayName.ToNullIfWhiteSpace();
			this.Status.ValidateEnum<Statuses>();
			if (!this.EmailRecipients.Any())
				throw new InvalidOperationException();
			if (this.EmailAttachments?.GroupBy(ea => ea.FileName.ToLower()).Any(f => f.Count() > 1) == true)
				throw new InvalidOperationException("File names are duplicated.");
			return this;
		}

		public static Email CreateNew(EmailTypes emailTypeEnum, string subject, string body, bool isBodyHtml, Priorities priorityEnum, int createdByUserLoginHistoryID, DateTime createdDate, DateTime? expiryDate, string fromDisplayName)
		{
			return new Email
			{
				EmailID = Guid.NewGuid(),
				EmailTypeEnum = emailTypeEnum,
				Subject = subject,
				Body = body,
				IsBodyHtml = isBodyHtml,
				CreatedDate = createdDate,
				PriorityEnum = priorityEnum,
				ExpiryDate = expiryDate,
				FromDisplayName = fromDisplayName ?? DefaultFromDisplayName,
				CreatedByUserLoginHistoryID = createdByUserLoginHistoryID,
				StatusEnum = Statuses.Pending
			};
		}

		public void AddAttachment((string FileName, byte[] FileContents) attachment)
		{
			if (this.EmailAttachments == null)
				this.EmailAttachments = new List<EmailAttachment>();
			this.EmailAttachments.Add(new EmailAttachment
			{
				EmailAttachmentID = Guid.NewGuid(),
				FileName = attachment.FileName,
				FileContents = attachment.FileContents
			}.Validate());
		}

		public Email AddAttachments(params (string FileName, byte[] FileContents)[] attachmentList)
		{
			attachmentList?.ForEach(this.AddAttachment);
			return this;
		}

		public Email AddRecipient((string DisplayName, string EmailAddress) address, EmailRecipient.RecipientTypes recipientTypeEnum)
		{
			if (this.EmailRecipients == null)
				this.EmailRecipients = new List<EmailRecipient>();
			this.EmailRecipients.Add(new EmailRecipient
			{
				EmailRecipientID = Guid.NewGuid(),
				EmailAddress = address.EmailAddress,
				DisplayName = address.DisplayName,
				RecipientTypeEnum = recipientTypeEnum,
			}.Validate());
			return this;
		}

		public Email AddRecipients(EmailRecipient.RecipientTypes recipientTypeEnum, params (string DisplayName, string EmailAddress)[] recipients)
		{
			recipients?.ForEach(a => this.AddRecipient(a, recipientTypeEnum));
			return this;
		}

		public Email ToList(params (string DisplayName, string EmailAddress)[] addresses)
		{
			return this.AddRecipients(EmailRecipient.RecipientTypes.To, addresses);
		}

		public Email CcList(params (string DisplayName, string EmailAddress)[] addresses)
		{
			return this.AddRecipients(EmailRecipient.RecipientTypes.Cc, addresses);
		}

		public Email BccList(params (string DisplayName, string EmailAddress)[] addresses)
		{
			return this.AddRecipients(EmailRecipient.RecipientTypes.Bcc, addresses);
		}

		public Email ReplyToList(params (string DisplayName, string EmailAddress)[] addresses)
		{
			return this.AddRecipients(EmailRecipient.RecipientTypes.ReplyTo, addresses);
		}
	}
}
