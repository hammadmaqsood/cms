﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class StudentSemester
	{
		public enum ResultRemarksTypes : byte
		{
			None = 0,
			Chance = 1,
			Dropped = 2,
			Probation = 3,
			Probation1 = 4,
			Probation2 = 5,
			Probation3 = 6,
			Qualified = 7,
			Relegated = 8,
		}

		public enum Statuses : byte
		{
			NotRegistered = 1,
			Registered = 2,
		}

		public static readonly byte StatusRegistered = (byte)Statuses.Registered;

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}

		public string SemesterFullName => this.SemesterID.ToSemesterString();
		public string StatusFullName => this.StatusEnum.ToFullName();

		public SemesterNos SemesterNoEnum
		{
			get => (SemesterNos)this.SemesterNo;
			set => this.SemesterNo = (short)value;
		}

		public string SemesterNoFullName => this.SemesterNoEnum.ToFullName();

		public Sections SectionEnum
		{
			get => (Sections)this.Section;
			set => this.Section = (int)value;
		}

		public string SectionFullName => this.SectionEnum.ToFullName();

		public Shifts ShiftEnum
		{
			get => (Shifts)this.Shift;
			set => this.Shift = (byte)value;
		}

		public string ShiftFullName => this.ShiftEnum.ToFullName();

		public FreezedStatuses? FreezedStatusEnum
		{
			get => (FreezedStatuses?)this.FreezedStatus;
			set => this.FreezedStatus = (byte?)value;
		}

		public string FreezedStatusFullName => this.FreezedStatusEnum?.ToFullName();

		public ExamRemarksTypes? ExamRemarksTypeEnum
		{
			get => (ExamRemarksTypes?)this.ExamRemarksType;
			set => this.ExamRemarksType = (byte?)value;
		}

		public string ExamRemarksTypeFullName => this.ExamRemarksTypeEnum?.ToFullName();
	}
}
