﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Model.Entities
{
	public partial class UserGroupPermission
	{
		public enum PermissionTypes : byte
		{
			AdministrationModule,
			AdmissionsModule,
			AssignTeacher,
			ClassAttendanceModule,
			ClearExtraSeats,
			CourseOfferingModule,
			CourseRegistrationModule,
			CoursesModule,
			DeleteSeat,
			ExamSeatingPlanModule,
			ExamsModule,
			ExecuteSeatingPlan,
			FacultyManagementModule,
			FeeManagementModule,
			GenerateEnrollments,
			IndividualSeating,
			LockMarksEntry,
			ManageAdmissionCriteria,
			ManageAdmissionOpening,
			ManageAdmissionProcessingFee,
			ManageBankAccounts,
			ManageBlockedStudents,
			ManageCancelledAdmissions,
			ManageClassAttendanceSettings,
			ManageCourseRegistrationSettings,
			ManageControlPanel,
			ManageCoursePreRequisites,
			ManageCourses,
			ManageDepartments,
			ManageEquivalentCourses,
			ManageETSScore,
			ManageExamBuildings,
			ManageExamConducts,
			ManageExamDateAndSessions,
			ManageExamDates,
			ManageExamDateSheet,
			ManageExamDesignations,
			ManageExamInvigilatorDuties,
			ManageExamInvigilators,
			ManageExamMarkingSchemes,
			ManageExamMarksGradingSchemes,
			ManageExamOfferedRooms,
			ManageExamRooms,
			ManageExamSessions,
			ManageExemptedCourses,
			ManageFacultyMemberAccount,
			ManageFacultyMembers,
			ManageFeeChallan,
			ManageFeeConcessionTypes,
			ManageFeeHeads,
			ManageFeePayment,
			ManageFeeStructure,
			ManageInstitutes,
			ManageInterviews,
			ManageOfferedCourses,
			ManagePrograms,
			ManageRegisteredCourseStatus,
			ManageSemesterFreezed,
			ManageSemesters,
			ManageStudentAccount,
			ManageStudentCreditTransfer,
			ManageStudentProfile,
			ManageStudentSemesters,
			ManageSurveys,
			ManageSurveysGroupsQuestionsOptions,
			ManageTimeBarredStudents,
			ManageUserGroups,
			ManageUsers,
			QualityAssuranceModule,
			RegisterCourse,
			StudentInformationModule,
			UnlockMarksEntry,
			UpdateMarks,
			UserManagementModule,
			ErrorsModule,
			ManageCommunityServices,
			ChangeProfileInformation,
			ChangeAppliedProgram,
			ChangeCandidateStatus,
			StudentTransfer,
			ScholarshipModule,
			ManageSemesterWorkload,
			PgpModule,
			LibraryManagementModule,
			ManageStudentLibraryDefaulter,
			ManageCBTLabs,
			ManageCBTSessions,
			ExportCandidateInformationForCBT,
			CompileMerit,
			ManageMarkingAndGradingSchemeAssignment,
			ManageFeeDueDates,
			ManageExamMarksPolicies,
			SwapAdmissionProcessingFeeChallanNos,
			CanByPassPreRequisitesCheck,
			CanByPassImprovementCheck,
			CanByPassRegistrationOpenCheck,
			CanRegisterTimeBarredStudents,
			CanChangeStudentSectionShift,
			CanDeleteCourseRegistration,
			CanDeleteCourseRegistrationWithAttendance,
			ManageFeePaymentOfLockedRecord,
			ManageAdmissionProcessingFeeForLockedRecords,
			CanDeleteFeeChallanContainingWithHoldingTax,
			ManageStudentDisciplineCase,
			ManageDisciplineCasesStudentRemarks,
			QarzeHasanaModule,
			CanEditQarzeHasanaApplicationForm,
			CanManageQarzeHasanaCampusRecommendations,
			CanManageQarzeHasanaDSARecommendations,
			CanManageQarzeHasanaFinalRecommendations,
			CanGenerateQarzeHasnaApplicationsForSpecificSemesters,
			CanGenerateQarzeHasnaApplicationForAllSemester,
			CanAddAndDeleteQarzeHasanaApplicationForm,
			ManageExamRemarksPolicies,
			CompileCGPAAndRemarks,
			ManageCourseCustomMarks,
			ManageCourseSummerGradeCapping,
			ManageCoursesHavingChildRecords,
			CanMergeClasses,
			OfficialDocumentsModule,
			ManageExecutiveDocuments,
			ManageStaffDocuments,
			ManageFacultyDocuments,
			ManageStudentDocuments,
			ViewStudentDocuments,
			UploadStudentDocuments,
			VerifyStudentDocuments,
			SurveyForm2,
			ManageConductSurveyForm2,
			GenerateLinksSurveyForm2,
			ManageSummarySurveyForm2,
			SurveyForm4,
			ManageConductSurveyForm4,
			GenerateLinksSurveyForm4,
			ManageSummarySurveyForm4,
			SurveyForm5,
			ManageConductSurveyForm5,
			GenerateLinksSurveyForm5,
			ManageSummarySurveyForm5,
			SurveyForm6,
			ManageConductSurveyForm6,
			GenerateLinksSurveyForm6,
			ManageSummarySurveyForm6,
			SurveyForm7,
			ManageConductSurveyForm7,
			GenerateLinksSurveyForm7,
			ManageSummarySurveyForm7,
			SurveyForm8,
			ManageConductSurveyForm8,
			GenerateLinksSurveyForm8,
			ManageSummarySurveyForm8,
			Employers,
			ViewGraduateDirectory,
			ManageCourseSubstitution,
			CanDeleteCourse,
			CanProcessCoursesCleanUp,
			ManageDisciplineCase,
			UniversityLevelMarksEntry,
			CanUnlockCampusLevelMarksEntry,
			CampusLevelMarksEntry,
			DepartmentLevelMarksEntry,
			ManageContacts,
			DepartmentLevelMarksEntryOfProjectThesisInternship,
			ComplaintsModule,
			ManageComplaints,
			ManageDisciplineCaseActivities,
			ManageStudentGPACGPA,
			CanManageQarzeHasnaStudentsFeeDetails,
			ManageFaculties,
			CanRevertSubmitToHOD,
			CanRevertSubmitToCampus,
			CanRevertSubmitToUniversity,
			CanRevertSubmitToHODOfProjectThesisInternship,
			CanRevertSubmitToCampusOfProjectThesisInternship,
			CanRevertSubmitToUniversityOfProjectThesisInternship,
			LMSModule,
			FeedbacksModule,
			CanRespondAsRector,
			AlumniModule,
			CanManageJobPositions,
			CanByPassMaxAllowedCoursesPerSemester,
			CanByPassMaxAllowedCreditHoursPerSemester,
			UpdateRegistrationNos,
			CanRespondAsDirectorGeneral,
			CanViewRectorBox,
			CanViewDirectorGeneralBox,
			ManageAssignees,
			Forms,
			StudentClearanceForm,
			StudentClearanceFormCanInitiateOrEdit,
			StudentClearanceFormCanChangeSSCStatus,
			StudentClearanceFormMainLibrarySignature,
			StudentClearanceFormLawLibrarySignature,
			StudentClearanceFormLabsServerRoomSignature,
			StudentClearanceFormLaptopSectionSignature,
			StudentClearanceFormStudentAffairsSignature,
			StudentClearanceFormHostelWardenSignature,
			StudentClearanceFormExamCellSignature,
			StudentClearanceFormAdmissionOfficeSignature,
			StudentClearanceFormSecurityAndSportsSignature,
			StudentClearanceFormStudentAdvisorSignature,
			StudentClearanceFormAccountsSectionSignature,
			CanViewOnlineTeachingReadinessForm,
			CanSubmitOnlineTeachingReadinessFormAsClusterHead,
			CanSubmitOnlineTeachingReadinessFormAsHOD,
			CanSubmitOnlineTeachingReadinessFormAsDirector,
			CanSubmitOnlineTeachingReadinessFormAsDG,
			CanUndoSubmitOnlineTeachingReadinessFormAsHOD,
			CanUndoSubmitOnlineTeachingReadinessFormAsDirector,
			CanUndoSubmitOnlineTeachingReadinessFormAsDG
		}

		public enum PermissionValues : byte
		{
			Allowed = 1,
			Denied = 2,
			Readonly = 3,
			Special = 4,
		}

		public AspireModules ModuleEnum
		{
			get => (AspireModules)this.Module;
			set
			{
				this.Module = (byte)value;
				this.ModuleNameEnum = value;
			}
		}

		public AspireModules ModuleNameEnum
		{
			get => this.ModuleName.ToEnum<AspireModules>();
			set => this.ModuleName = value.ToString();
		}

		public PermissionTypes PermissionTypeEnum
		{
			get => (PermissionTypes)this.PermissionType;
			set
			{
				this.PermissionType = (long)value;
				this.PermissionTypeNameEnum = value;
			}
		}

		public PermissionTypes PermissionTypeNameEnum
		{
			get => this.PermissionTypeName.ToEnum<PermissionTypes>();
			set => this.PermissionTypeName = value.ToString();
		}

		public PermissionValues PermissionValueEnum
		{
			get => (PermissionValues)this.PermissionValue;
			set
			{
				this.PermissionValue = (byte)value;
				this.PermissionValueNameEnum = value;
			}
		}

		public PermissionValues PermissionValueNameEnum
		{
			get => this.PermissionValueName.ToEnum<PermissionValues>();
			set => this.PermissionValueName = value.ToString();
		}

		public static readonly PermissionValues[] Allowed = { PermissionValues.Allowed };
		public static readonly PermissionValues[] Denied = { PermissionValues.Denied };
		public static readonly PermissionValues[] AllowedDenied = { PermissionValues.Allowed, PermissionValues.Denied };
		public static readonly PermissionValues[] AllowedDeniedSpecial = { PermissionValues.Allowed, PermissionValues.Denied, PermissionValues.Special, };
		public static readonly PermissionValues[] AllowedDeniedReadonly = { PermissionValues.Allowed, PermissionValues.Denied, PermissionValues.Readonly, };

		public UserGroupPermission Validate()
		{
			if (this.ModuleEnum != this.ModuleNameEnum)
				throw new InvalidOperationException();
			if (this.PermissionTypeEnum != this.PermissionTypeNameEnum)
				throw new InvalidOperationException();
			if (this.PermissionValueEnum != this.PermissionValueNameEnum)
				throw new InvalidOperationException();
			return this;
		}

		public bool TryValidate()
		{
			try
			{
				this.Validate();
				return true;
			}
			catch (InvalidOperationException)
			{
				return false;
			}
		}

		public UserGroupPermission Clone()
		{
			return new UserGroupPermission
			{
				UserGroupID = this.UserGroupID,
				ModuleEnum = this.ModuleNameEnum,
				ModuleNameEnum = this.ModuleNameEnum,
				PermissionValueEnum = this.PermissionValueNameEnum,
				PermissionValueNameEnum = this.PermissionValueNameEnum,
				PermissionTypeEnum = this.PermissionTypeNameEnum,
				PermissionTypeNameEnum = this.PermissionTypeNameEnum,
			}.Validate();
		}
	}
}
