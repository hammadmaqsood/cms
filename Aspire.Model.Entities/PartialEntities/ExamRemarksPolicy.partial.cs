﻿using Aspire.Model.Entities.Common;
using System.Linq;

namespace Aspire.Model.Entities
{
	public partial class ExamRemarksPolicy
	{
		public string Validity
		{
			get
			{
				if (this.ValidUpToSemesterID == null)
					return $"{this.ValidFromSemesterID.ToSemesterString()} onward";
				return $"{this.ValidFromSemesterID.ToSemesterString()} - {this.ValidUpToSemesterID.ToSemesterString()}";
			}
		}

		public string IsValid
		{
			get
			{
				var firstSemester = this.ExamRemarksPolicyDetails.SingleOrDefault(erpd => erpd.FirstSemester);
				if (firstSemester != null)
				{
					if (firstSemester.ExamRemarksTypeEnum != ExamRemarksTypes.Drop)
						return $"Exam Remarks Policy is not valid. Remarks of First Semester must be {ExamRemarksTypes.Drop.ToFullName()}.";
				}
				if (this.ExamRemarksPolicyDetails.All(erpd => erpd.FirstSemester))
					return "Exam Remarks Policy rules are not defined.";
				var lastExamRemarksPolicyDetail = this.ExamRemarksPolicyDetails.OrderByDescending(erpd => erpd.Sequence).FirstOrDefault(erpd => !erpd.FirstSemester);
				if (lastExamRemarksPolicyDetail?.ExamRemarksTypeEnum != ExamRemarksTypes.Drop)
					return $"Exam Remarks Policy is not valid. Last remarks must be {ExamRemarksTypes.Drop.ToFullName()}.";
				foreach (var examRemarksPolicyDetail in this.ExamRemarksPolicyDetails)
					if (examRemarksPolicyDetail.LessThanCGPA < 0 || examRemarksPolicyDetail.LessThanCGPA > 4)
						return $"Exam Remarks Policy is not valid. CGPA must be less than or equal to 4.";
				return null;
			}
		}

		public string Summary
		{
			get
			{
				var summary = $"{this.ValidFromSemesterID.ToSemesterString()} - {this.ValidUpToSemesterID?.ToSemesterString() ?? "onward"}";
				foreach (var detail in this.ExamRemarksPolicyDetails.OrderByDescending(erpd => erpd.FirstSemester).ThenBy(erpd => erpd.Sequence))
				{
					summary += $"\n{detail.Sequence}: < {detail.LessThanCGPA.FormatGPA()} => {detail.ExamRemarksTypeEnum.ToFullName()}";
					if (detail.FirstSemester)
						summary += "(First Semester)";
				}
				return summary.Trim().Trim(',');
			}
		}
	}
}
