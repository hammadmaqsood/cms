﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class PasswordChangeHistory
	{
		public PasswordChangeReasons ChangeReasonEnum
		{
			get { return (PasswordChangeReasons)this.ChangeReason; }
			set { this.ChangeReason = (byte)value; }
		}
	}
}
