﻿using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Model.Entities
{
	public partial class StudentDocument
	{
		[AttributeUsage(AttributeTargets.Field)]
		public sealed class DocumentAttribute : Attribute
		{
			public bool AllowMultiple { get; }
			public IEnumerable<(Pages Page, bool Required)> Pages { get; }
			public DocumentAttribute(bool allowMultiple, Pages[] pages, params bool[] required)
			{
				this.AllowMultiple = allowMultiple;
				var pagesList = new List<(Pages Page, bool Required)>();
				for (var i = 0; i < pages.Length; i++)
					pagesList.Add((pages[i], required[i]));
				if (pagesList.Select(p => p.Page).Distinct().Count() != pagesList.Count())
					throw new InvalidOperationException("Pages are not unique.");
				else
					this.Pages = pagesList;
			}
		}

		[EnumGuids]
		public enum Pages
		{
			[EnumGuidValue("2694CA6B-E452-4ED9-A17E-11A560D20AC5", "Sheet 1 Front Side")]
			Sheet1Front,
			[EnumGuidValue("DAB38EC2-D3EA-459A-8D3B-86EBE6A986C6", "Sheet 1 Back Side")]
			Sheet1Back,
			[EnumGuidValue("AC27FD32-0DAF-4082-B72D-8408B80A701F", "Sheet 2 Front Side")]
			Sheet2Front,
			[EnumGuidValue("DE6C49C3-BA83-4BEE-882D-D4A3CF2C587E", "Sheet 2 Back Side")]
			Sheet2Back,
			[EnumGuidValue("6D043493-D366-4B04-A81A-4F7E799E9330", "Sheet 3 Front Side")]
			Sheet3Front,
			[EnumGuidValue("72A54DC5-3B49-4249-BF71-62F867AA2F7F", "Sheet 3 Back Side")]
			Sheet3Back,
		}

		[EnumGuids]
		public enum DocumentTypes
		{
			[EnumGuidValue("83009928-AF7A-4941-954D-35888B9D3C95", "CNIC")]
			[Document(false, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, true)]
			CNIC,
			[EnumGuidValue("DE6B6D26-4A8B-4C30-A69C-7A86AE16514A", "Passport")]
			[Document(false, new[] { Pages.Sheet1Front, Pages.Sheet2Front, Pages.Sheet3Front }, true, false, false)]
			Passport,
			[EnumGuidValue("59FFBF3C-FEF7-4895-8005-FCF98726FFDF", "Form B")]
			[Document(false, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			FormB,
			[EnumGuidValue("7ADBF519-2078-476A-AE68-223A1ADB291A", "Domicile")]
			[Document(false, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			Domicile,
			[EnumGuidValue("39F8697D-45FC-43CA-8E63-9C6208547046", "SSC Marks Sheet")]
			[Document(false, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			SSCMarksSheet,
			[EnumGuidValue("F86DE6D7-DC4E-4176-95DD-4E5144C39606", "SSC Certificate")]
			[Document(false, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			SSCCertificate,
			[EnumGuidValue("9A74901B-FDC7-4972-A081-695E0CBA4549", "HSSC Marks Sheet")]
			[Document(false, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			HSSCMarksSheet,
			[EnumGuidValue("D49208F9-0416-4C71-B21B-1B75B459139F", "HSSC Certificate")]
			[Document(false, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			HSSCCertificate,
			[EnumGuidValue("26C4B638-9DD0-4FFB-A113-4F7475D0A3EC", "O-Level - General Certificate of Education (From Cambridge)")]
			[Document(false, new[] { Pages.Sheet1Front, Pages.Sheet1Back, Pages.Sheet2Front, Pages.Sheet2Back, Pages.Sheet3Front, Pages.Sheet3Back }, true, false, false, false, false, false)]
			OLevelGeneralCertificateFromCambridge,
			[EnumGuidValue("0AD69CF8-5098-435A-8D3D-FEBC1DAD5530", "O-Level - Equivalence Certificate (From IBCC)")]
			[Document(false, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			OLevelEquivalenceCertificateFromIBCC,
			[EnumGuidValue("3E7E2549-1D72-4B7F-8E9E-5C404010F97A", "A-Level - General Certificate of Education (From Cambridge)")]
			[Document(false, new[] { Pages.Sheet1Front, Pages.Sheet1Back, Pages.Sheet2Front, Pages.Sheet2Back, Pages.Sheet3Front, Pages.Sheet3Back }, true, false, false, false, false, false)]
			ALevelGeneralCertificateFromCambridge,
			[EnumGuidValue("60ABD748-EA4D-43F7-BE84-F3A6EFCEDC07", "A-Level - Equivalence Certificate (From IBCC)")]
			[Document(false, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			ALevelEquivalenceCertificateFromIBCC,
			[EnumGuidValue("1E0BA276-132D-48A0-8EB1-66AB9821FC86", "Bachelors (2 Years) Marks Sheet/Transcript")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			Bachelors2YearsMarksSheet,
			[EnumGuidValue("73B20799-FDA9-4B36-8158-8EC9A9119B7E", "Bachelors (2 Years) Degree")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			Bachelors2YearsDegree,
			[EnumGuidValue("BC9DB786-84C9-4F0C-A9D5-1922E121F672", "Bachelors (4 Years) Marks Sheet/Transcript")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			Bachelors4YearsMarksSheet,
			[EnumGuidValue("8C252AE9-4F3D-4675-9CAB-32EEE63E1820", "Bachelors (4 Years) Degree")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			Bachelors4YearsDegree,
			[EnumGuidValue("60C00F3A-9591-4DDC-844E-59D9BB486451", "Bachelors - HEC Equivalence Certificate for Foreign Degree")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			BachelorsEquivalenceCertificateFromHEC,
			[EnumGuidValue("096FB384-2374-41C4-B2C8-380BCC46F74C", "Masters Marks Sheet/Transcript")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			MastersMarksSheet,
			[EnumGuidValue("3FE3D1E1-B5F2-4623-B733-2021D756DF66", "Masters Degree")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			MastersDegree,
			[EnumGuidValue("371569E5-624C-456F-8701-70086EFC344A", "Masters - HEC Equivalence Certificate for Foreign Degree")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			MastersEquivalenceCertificateFromHEC,
			[EnumGuidValue("58058CA2-3DCD-4676-872E-4805497AD488", "MS/M.Phil Marks Sheet/Transcript")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			MSMPhilMarksSheet,
			[EnumGuidValue("877EA868-119A-4B5F-8B5A-9AB9E70EBB43", "MS/M.Phil Degree")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			MSMPhilDegree,
			[EnumGuidValue("949E705E-8E7E-40F1-8E9F-38FD5DA33C0B", "MS/M.Phil - HEC Equivalence Certificate for Foreign Degree")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			MSMPhilEquivalenceCertificateFromHEC,
			[EnumGuidValue("CF127E6A-395E-4126-9825-61022008A2E5", "SAT-II Score")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			SAT2Score,
			[EnumGuidValue("4FB51F85-729E-4AEC-AF92-317FC95997EB", "GAT Score")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			GATScore,
			[EnumGuidValue("14D59F66-F64B-4C85-8DBB-C7E5289228EA", "Undertaking")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back, Pages.Sheet2Front, Pages.Sheet2Back }, true, false, false, false)]
			Undertaking,
			[EnumGuidValue("588F7C7B-BB6D-4D65-A135-34AE4EF6A9B6", "PMDC Registration Form")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			PMDCRegistrationForm,
			[EnumGuidValue("A7047D9E-ABC2-490E-B4C7-1D9FFCD66B39", "Acceptance Form")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			AcceptanceForm,
			[EnumGuidValue("11D75EA6-6520-451B-BD86-2B65124292E7", "Confidential Form")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			ConfidentialForm,
			[EnumGuidValue("C207FB14-822E-41ED-B932-C7AC438DCBF8", "Medical Report")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back, Pages.Sheet2Front, Pages.Sheet2Back }, true, false, false, false)]
			MedicalReport,
			[EnumGuidValue("ECC93A85-D894-48D5-96EE-817E0360D34C", "MBBS Degree")]
			[Document(false, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			MBBSDegree,
			[EnumGuidValue("7CB6B980-D6E2-4253-8128-FB26CD9A8770", "BDS Degree")]
			[Document(false, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			BDSDegree,
			[EnumGuidValue("7F5DD895-8161-4EE8-B132-77BB7FD8876F", "House Job Certificate")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			HouseJobCertificate,
			[EnumGuidValue("FF33FFBA-8F6F-453C-A81E-E9849F1C1708", "Interim Transcript")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			InterimTranscript,
			[EnumGuidValue("6B2E8883-C256-4BD2-8E1E-B7E730164FAA", "Consolidated Transcript")]
			[Document(true, new[] { Pages.Sheet1Front, Pages.Sheet1Back }, true, false)]
			ConsolidatedTranscript
		}

		[EnumGuids]
		public enum Statuses
		{
			[EnumGuidValue("2D6B025F-3809-4DFC-B6C2-E5798CA3B86D", "Verification Pending")]
			VerificationPending,
			[EnumGuidValue("870B85BD-F95E-427C-8FDC-1D883A4D361C", "Rejected")]
			Rejected,
			[EnumGuidValue("C600C6AF-718F-4A23-BEFB-E8BD525B09CE", "Verified")]
			Verified,
		}

		public Pages PageEnum
		{
			get => this.Page.GetEnum<Pages>();
			set => this.Page = value.GetGuid();
		}

		public DocumentTypes DocumentTypeEnum
		{
			get => this.DocumentType.GetEnum<DocumentTypes>();
			set => this.DocumentType = value.GetGuid();
		}

		public Statuses StatusEnum
		{
			get => this.Status.GetEnum<Statuses>();
			set => this.Status = value.GetGuid();
		}

		public string Dimensions => $"{this.ImageWidthPixels} x {this.ImageHeightPixels}";
		public string Width => $"{this.ImageWidthPixels} pixels";
		public string Height => $"{this.ImageHeightPixels} pixels";
		public string Resolution => this.ImageDpiX.Equals(this.ImageDpiY) ? $"{this.ImageDpiX} dpi" : $"{this.ImageDpiX} dpi x {this.ImageDpiY} dpi";

		public static DocumentAttribute GetDocumentAttribute(DocumentTypes documentTypeEnum)
		{
			return documentTypeEnum.GetCustomAttribute<DocumentAttribute>();
		}
	}
}