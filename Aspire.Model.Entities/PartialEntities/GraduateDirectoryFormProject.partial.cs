﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class GraduateDirectoryFormProject
	{
		public enum ProjectTypes : byte
		{
			Interim = 1,
			FinalSemester = 2,
		}

		public ProjectTypes ProjectTypeEnum
		{
			get => (ProjectTypes)this.ProjectType;
			set => this.ProjectType = (byte)value;
		}

		public string ProjectTypeFullName => this.ProjectTypeEnum.ToFullName();
	}
}
