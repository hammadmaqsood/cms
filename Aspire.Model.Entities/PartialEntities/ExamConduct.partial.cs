﻿namespace Aspire.Model.Entities
{
	public partial class ExamConduct
	{
		public enum ExamTypes : byte
		{
			Mid = 1,
			Final = 2,
		}

		public static readonly byte ExamTypeMid = (byte)ExamTypes.Mid;
		public static readonly byte ExamTypeFinal = (byte)ExamTypes.Final;

		public ExamTypes ExamTypeEnum
		{
			get { return (ExamTypes)this.ExamType; }
			set { this.ExamType = (byte)value; }
		}
	}
}
