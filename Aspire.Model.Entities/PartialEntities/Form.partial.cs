﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class Form
	{
		[EnumGuids]
		public enum FormTypes
		{
			[EnumGuidValue("129ba3b3-ca97-40fe-a613-6a41d904b13a", "Student Clearance Form", "Student Clearance Form")]
			StudentClearanceForm
		}

		public FormTypes FormTypeEnum
		{
			get => this.FormType.GetEnum<FormTypes>();
			set => this.FormType = value.GetGuid();
		}

		[EnumGuids]
		public enum FormStatuses
		{
			[EnumGuidValue("A7B2CBFD-3711-4CCA-B196-57F405E9363B", "Not Initiated")]
			NotInitiated,
			[EnumGuidValue("5816EAE4-1989-439C-BAA1-97A390AEF966", "Initiated")]
			Initiated,
			[EnumGuidValue("0BCB633D-C8F3-4F56-B0AF-D45C95C43BCA", "In Process")]
			InProcess,
			[EnumGuidValue("0CEF230E-9CE5-4C76-B688-9BBA257BEBE6", "Not Verified By SSC")]
			NotVerifiedBySSC, 
			[EnumGuidValue("9771080C-01D7-4129-A9B9-B463AA860AEE", "Cancelled")]
			Cancelled, 
			[EnumGuidValue("834FAB1D-DA8A-4D27-9F02-3DCC73CB616F", "Rejected")]
			Rejected,
			[EnumGuidValue("D22D4EC5-5C18-4AAE-82A2-DC34B9C1387F", "Completed")]
			Completed
		}

		public FormStatuses FormStatusEnum
		{
			get => this.FormStatus.GetEnum<FormStatuses>();
			set => this.FormStatus = value.GetGuid();
		}
	}
}
