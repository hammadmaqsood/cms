﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Model.Entities
{
	public partial class OnlineTeachingReadiness
	{
		[EnumGuids]
		public enum ReadinessTypes
		{
			[EnumGuidValue("9936b422-3050-4cd8-b0f5-c2af38087c3c", "Not Ready")]
			NotReady,
			[EnumGuidValue("1ef1ee44-8c48-4c4f-812c-349d46e19992", "Partially Ready")]
			PartiallyReady,
			[EnumGuidValue("23783867-6007-4fd0-bb39-bdb7401d3e88", "Fully Ready")]
			FullyReady
		}

		[Serializable]
		public sealed class Question
		{
			public int QuestionNo { get; set; }
			public string QuestionText { get; set; }
			public bool? Ready { get; set; }
			public string NotReadyReason { get; set; }

			public Question Validate()
			{
				this.QuestionText.CannotBeEmptyOrWhitespace();
				if (this.Ready == false)
					this.NotReadyReason.CannotBeEmptyOrWhitespace();
				else
					this.NotReadyReason.MustBeNull();
				return this;
			}

			public static List<Question> PartiallyReadyQuestions => new List<Question>
			{
				new OnlineTeachingReadiness.Question { QuestionNo = 1, QuestionText = "Can the FM effectively use BU Learning Management System (LMS)?" },
				new OnlineTeachingReadiness.Question { QuestionNo = 2, QuestionText = "Faculty Member aware/trained on BU LMS/Microsoft Team/Zoom Tools?" },
				new OnlineTeachingReadiness.Question { QuestionNo = 3, QuestionText = "FM is ready to deliver lecture through MS Team/Zoom, record it and share the link on BU LMS?" },
				new OnlineTeachingReadiness.Question { QuestionNo = 4, QuestionText = "Connectivity available for both synchronous and asynchronous teaching mode?" },
				new OnlineTeachingReadiness.Question { QuestionNo = 5, QuestionText = "Availability of Desktop/Laptop/ Smart devices with Faculty Member?" },
				new OnlineTeachingReadiness.Question { QuestionNo = 6, QuestionText = "Weekly Lecture Plan uploaded on BU LMS?" },
				new OnlineTeachingReadiness.Question { QuestionNo = 7, QuestionText = "FM is ready to upload Lecture Slides on BU LMS?" },
				new OnlineTeachingReadiness.Question { QuestionNo = 8, QuestionText = "FM is ready to upload Assignments on BU LMS?" }
			};

			public static List<Question> FullyReadyQuestions => PartiallyReadyQuestions;
		}

		public List<Question> PartiallyReadyQuestionsList
		{
			get => this.PartiallyReadyQuestions?.FromJsonString<List<Question>>();
			set => this.PartiallyReadyQuestions = value?.ToJsonString();
		}

		public List<Question> FullyReadyQuestionsList
		{
			get => this.FullyReadyQuestions?.FromJsonString<List<Question>>();
			set => this.FullyReadyQuestions = value?.ToJsonString();
		}

		public ReadinessTypes ReadinessTypeEnum
		{
			get => this.ReadinessType.GetEnum<ReadinessTypes>();
			set => this.ReadinessType = value.GetGuid();
		}

		public IEnumerable<string> Validate()
		{
			static string ValidateHours(decimal? hours, string name)
			{
				if (hours == null)
					return $"{name} is not provided.";
				if (hours < 0)
					return $"{name} cannot be less than zero.";
				return null;
			}

			IEnumerable<string> GetErrors()
			{
				switch (this.ReadinessTypeEnum)
				{
					case ReadinessTypes.NotReady:
						yield return ValidateHours(this.NotReadyAlreadyDeliveredFaceToFaceContactHours, "Face to face contact hours");
						yield return ValidateHours(this.NotReadyRemainingContactHours, "Remaining Contact hours");
						yield return ValidateHours(this.NotReadyTotalContactHours, "Total contact hours");
						if (this.NotReadyAlreadyDeliveredFaceToFaceContactHours.Value + this.NotReadyRemainingContactHours.Value != this.NotReadyTotalContactHours.Value)
							yield return "Total contact hours value is not valid.";
						yield return ValidateHours(this.NotReadyTheoreticalContactHours, "Theoritical contact hours");
						yield return ValidateHours(this.NotReadyLabContactHours, "Lab based contact hours");
						yield return ValidateHours(this.NotReadyFieldStudyContactHours, "Field Visit/clinical contact hours");
						yield return ValidateHours(this.NotReadyOtherContactHours, "Other contact hours");
						var total = this.NotReadyTheoreticalContactHours + this.NotReadyLabContactHours + this.NotReadyFieldStudyContactHours + this.NotReadyOtherContactHours;
						if (this.NotReadyTotalContactHours != total)
							yield return "Sum of contact hours is not equal to Total Contact Hours.";
						break;
					case ReadinessTypes.PartiallyReady:
						yield return ValidateHours(this.PartiallyReadyAlreadyDeliveredFaceToFaceContactHours, "Face to face contact hours");
						yield return ValidateHours(this.PartiallyReadyToBeDeliveredOnlineContactHours, "Contact hours to be delivered online");
						yield return ValidateHours(this.PartiallyReadyTotalContactHours, "Total contact hours");
						if (this.PartiallyReadyAlreadyDeliveredFaceToFaceContactHours.Value + this.PartiallyReadyToBeDeliveredOnlineContactHours.Value != this.PartiallyReadyTotalContactHours.Value)
							yield return "Total contact hours value is not valid.";
						yield return ValidateHours(this.PartiallyReadyCoveredOnlineTheoreticalContactHours, "Theoritical (covered online) contact hours");
						yield return ValidateHours(this.PartiallyReadyCoveredOnlineLabContactHours, "Lab based (covered online) contact hours");
						yield return ValidateHours(this.PartiallyReadyCoveredOnlineFieldStudyContactHours, "Field Visit/clinical (covered online) contact hours");
						yield return ValidateHours(this.PartiallyReadyCoveredOnlineOtherContactHours, "Other (covered online) contact hours");
						yield return ValidateHours(this.PartiallyReadyCanNotCoveredOnlineTheoreticalContactHours, "Theoritical (not covered online) contact hours");
						yield return ValidateHours(this.PartiallyReadyCanNotCoveredOnlineLabContactHours, "Lab Based (not covered online) contact hours");
						yield return ValidateHours(this.PartiallyReadyCanNotCoveredOnlineFieldStudyContactHours, "Field Visit/clinical (not covered online) contact hours");
						yield return ValidateHours(this.PartiallyReadyCanNotCoveredOnlineOtherContactHours, "Other (not covered online) contact hours");
						total = this.PartiallyReadyAlreadyDeliveredFaceToFaceContactHours;
						total += this.PartiallyReadyCoveredOnlineTheoreticalContactHours + this.PartiallyReadyCoveredOnlineLabContactHours + this.PartiallyReadyCoveredOnlineFieldStudyContactHours + this.PartiallyReadyCoveredOnlineOtherContactHours;
						total += this.PartiallyReadyCanNotCoveredOnlineTheoreticalContactHours + this.PartiallyReadyCanNotCoveredOnlineLabContactHours + this.PartiallyReadyCanNotCoveredOnlineFieldStudyContactHours + this.PartiallyReadyCanNotCoveredOnlineOtherContactHours;
						if (this.PartiallyReadyTotalContactHours != total)
							yield return "Sum of contact hours is not equal to Total Contact Hours.";
						break;
					case ReadinessTypes.FullyReady:
						yield return ValidateHours(this.FullyReadyAlreadyDeliveredFaceToFaceContactHours, "Face to face contact hours");
						yield return ValidateHours(this.FullyReadyToBeDeliveredOnlineContactHours, "To be delivered Contact hours");
						yield return ValidateHours(this.FullyReadyTotalContactHours, "Total contact hours");
						if (this.FullyReadyAlreadyDeliveredFaceToFaceContactHours.Value + this.FullyReadyToBeDeliveredOnlineContactHours.Value != this.FullyReadyTotalContactHours.Value)
							yield return "Total contact hours value is not valid.";
						yield return ValidateHours(this.FullyReadyTheoreticalContactHours, "Theoritical contact hours");
						yield return ValidateHours(this.FullyReadyLabContactHours, "Lab based contact hours");
						yield return ValidateHours(this.FullyReadyFieldStudyContactHours, "Field Visit/clinical contact hours");
						yield return ValidateHours(this.FullyReadyOtherContactHours, "Other contact hours");
						total = this.FullyReadyTheoreticalContactHours + this.FullyReadyLabContactHours + this.FullyReadyFieldStudyContactHours + this.FullyReadyOtherContactHours;
						if (this.FullyReadyTotalContactHours != total)
							yield return "Sum of contact hours is not equal to Total Contact Hours.";
						break;
					default:
						throw new NotImplementedEnumException(this.ReadinessTypeEnum);
				}
			}
			return GetErrors().Where(r => r != null);
		}
	}
}
