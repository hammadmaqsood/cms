﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class ComplaintComment
	{
		[EnumGuids]
		public enum CommentTypes
		{
			[EnumGuidValue("{23901861-A4E3-422F-B2E8-EAEA660939EE}", "Comment")]
			Comment,
			[EnumGuidValue("{72F468DA-B3F6-4AAE-98D8-FA766EADBAA0}", "FeedBack")]
			FeedBack,
		}

		[EnumGuids]
		public enum Statuses
		{
			[EnumGuidValue("{36E7B59F-03DC-44B9-ACE6-C8EB95DC3390}", "Active")]
			Active,
			[EnumGuidValue("{51A637DA-AEA4-409B-BAF5-58438456BA52}", "Inactive")]
			Inactive,
		}

		public CommentTypes CommentTypeEnum
		{
			get => this.Type.GetEnum<CommentTypes>();
			set => this.Type = value.GetGuid();
		}

		public Statuses? StatusEnum
		{
			get => this.Status?.GetEnum<Statuses>();
			set => this.Status = value?.GetGuid();
		}
	}
}
