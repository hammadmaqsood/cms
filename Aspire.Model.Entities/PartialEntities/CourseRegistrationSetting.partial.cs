﻿using System;

namespace Aspire.Model.Entities
{
	public partial class CourseRegistrationSetting
	{
		public enum Types : byte
		{
			Coordinator = 1,
			Student = 2,
		}

		public static readonly byte TypeStudent = (byte)Types.Student;
		public static readonly byte TypeCoordinator = (byte)Types.Coordinator;

		public enum Statuses : byte
		{
			Active = 1,
			Inactive = 2,
		}

		public static readonly byte StatusActive = (byte)Statuses.Active;
		public static readonly byte StatusInactive = (byte)Statuses.Inactive;

		public Types TypeEnum
		{
			get { return (Types)this.Type; }
			set { this.Type = (byte)value; }
		}

		public Statuses StatusEnum
		{
			get { return (Statuses)this.Status; }
			set { this.Status = (byte)value; }
		}

		public enum ValidationResults
		{
			None,
			NoRecordFound,
			AlreadyExists,
			InvalidFromDate,
			InvalidDateRange,
			Success,
		}

		public static ValidationResults Validate(DateTime fromDate, DateTime toDate)
		{
			if (fromDate.Date > toDate.Date)
				return ValidationResults.InvalidDateRange;
			return ValidationResults.Success;
		}
	}
}
