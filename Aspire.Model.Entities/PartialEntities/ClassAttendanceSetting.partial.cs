﻿using System;
using System.Data;

namespace Aspire.Model.Entities
{
	public partial class ClassAttendanceSetting
	{
		public enum Statuses : byte
		{
			Active = 1,
			Inactive = 2,
		}

		public static readonly byte StatusActive = (byte)Statuses.Active;
		public static readonly byte StatusInactive = (byte)Statuses.Inactive;

		public Statuses StatusEnum
		{
			get { return (Statuses)this.Status; }
			set { this.Status = (byte)value; }
		}

		[Obsolete]
		public enum ValidationResults
		{
			None,
			NoRecordFound,
			AlreadyExists,
			InvalidFromDate,
			InvalidDateRange,
			Success,
		}

		public ClassAttendanceSetting Validate()
		{
			if (this.FromDate > this.ToDate)
				throw new DataException("From Date must be less than or equal to To Date.");
			return this;
		}
	}
}
