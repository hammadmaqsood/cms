﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class CandidateAppliedProgram
	{
		public enum Statuses : byte
		{
			None = 0,
			Refunded = 1,
		}

		public Statuses StatusEnum
		{
			get => (Statuses?)this.Status ?? Statuses.None;
			set => this.Status = (byte)value;
		}

		public ETSTypes? ETSTypeEnum
		{
			get => (ETSTypes?)this.ETSType;
			set => this.ETSType = (byte?)value;
		}

		public DegreeTypes? ResultAwaitedDegreeTypeEnum
		{
			get => (DegreeTypes?)this.ResultAwaitedDegreeType;
			set => this.ResultAwaitedDegreeType = (byte?)value;
		}

		public Shifts ShiftEnum
		{
			get => (Shifts)this.Shift;
			set => this.Shift = (byte)value;
		}

		public Shifts? AdmittedShiftEnum
		{
			get => (Shifts?)this.AdmittedShift;
			set => this.AdmittedShift = (byte?)value;
		}

		public Shifts? DeferredToShiftEnum
		{
			get => (Shifts?)this.DeferredToShift;
			set => this.DeferredToShift = (byte?)value;
		}

		public string FullChallanNo => AspireFormats.ChallanNo.GetFullChallanNoString(this.AdmissionOpenProgram1.Program.Institute.InstituteCode, InstituteBankAccount.FeeTypes.AdmissionProcessingFee, this.ChallanNo);
	}
}