﻿namespace Aspire.Model.Entities
{
	public partial class ExamMarkingScheme
	{
		public enum MarkingSchemeTypes : byte
		{
			AssignmentsQuizzesMidFinal = 1,
			InternalsMidFinal = 2
		}
	}
}
