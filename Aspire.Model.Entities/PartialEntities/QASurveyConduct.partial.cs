﻿using System;

namespace Aspire.Model.Entities
{
	public partial class QASurveyConduct
	{
		public enum Statuses : byte
		{
			Active = 1,
			Inactive = 2,
		}

		public Statuses SurveyStatusEnum
		{
			get => (Statuses)this.SurveyStatus;
			set => this.SurveyStatus = (byte)value;
		}

		public enum ValidationResults
		{
			None,
			NoRecordFound,
			AlreadyExists,
			InvalidOpenDate,
			InvalidDateRange,
			Success,
			InvalidCloseDate,
			DatePassed,
			ChildRecordExists,
			SummaryGenerated
		}
		public static ValidationResults Validate(DateTime openDate, DateTime? closeDate)
		{
			if (openDate.Date < DateTime.Today)
				return ValidationResults.InvalidOpenDate;
			if (closeDate == null)
				return ValidationResults.Success;
			return openDate.Date > closeDate.Value.Date ? ValidationResults.InvalidDateRange : ValidationResults.Success;
		}
	}
}
