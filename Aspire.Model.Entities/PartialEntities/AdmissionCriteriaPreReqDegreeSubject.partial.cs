﻿using Aspire.Lib.Extensions;

namespace Aspire.Model.Entities
{
	public partial class AdmissionCriteriaPreReqDegreeSubject
	{
		public AdmissionCriteriaPreReqDegreeSubject Validate()
		{
			this.Subjects = this.Subjects.TrimAndCannotBeEmpty();
			return this;
		}
	}
}
