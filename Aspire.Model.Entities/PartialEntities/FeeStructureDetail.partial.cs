﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class FeeStructureDetail
	{
		public SemesterNos? SemesterNoEnum
		{
			get { return (SemesterNos?)this.SemesterNo; }
			set { this.SemesterNo = (short?)value; }
		}

		public Categories? CategoryEnum
		{
			get { return (Categories?)this.Category; }
			set { this.Category = (byte?)value; }
		}

		public string SemesterNoEnumFullName => this.SemesterNoEnum?.ToFullName() ?? "All";

		public string CategoryEnumFullName => this.CategoryEnum?.ToFullName() ?? "All";
	}
}
