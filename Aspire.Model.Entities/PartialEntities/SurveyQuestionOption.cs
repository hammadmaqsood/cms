﻿using Aspire.Lib.Extensions;

namespace Aspire.Model.Entities
{
	public partial class SurveyQuestionOption
	{
		public SurveyQuestionOption Validate()
		{
			this.OptionText = this.OptionText.TrimAndCannotBeEmpty();
			return this;
		}

		public string OptionTextAndWeightage => $"{this.OptionText}-V:{this.Weightage.ToNAIfNullOrEmpty()}";
	}
}