﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class GraduateDirectoryFormWorkshop
	{
		public enum Types : byte
		{
			Workshop = 1,
			Seminar = 2,
		}

		public Types TypeEnum
		{
			get => (Types)this.Type;
			set => this.Type = (byte)value;
		}

		public string TypeFullName => this.TypeEnum.ToFullName();
	}
}
