﻿using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.Model.Entities
{
	public partial class AccountTransaction
	{
		public static readonly string AlliedBankInstructions = "Payment can be made from ATM, myABL, SMS Banking and Any Branch of ABL.";
		public static readonly string BankAlfalahInstructions = "Payment can be made from Any Branch of Bank Alfalah.";

		public enum Services : byte
		{
			Manual = 1,
			BankAlfalah = 2,
			AlliedBank = 3
		}

		public enum TransactionChannels : byte
		{
			Campus = 1,
			ATM = 2,
			MobileBanking = 3,
			Branch = 4,
			InternetBanking = 5,
			ContactCenter = 6,
		}

		public enum PaymentTypes : byte
		{
			Cash = 1,
			Account = 2,
			DemandDraft = 3,
			PayOrder = 4,
			Cheque = 5
		}

		public Services ServiceEnum
		{
			get => (Services)this.Service;
			set => this.Service = (byte)value;
		}

		public string ServiceFullName => this.ServiceEnum.ToFullName();

		public TransactionChannels TransactionChannelEnum
		{
			get => (TransactionChannels)this.TransactionChannel;
			set => this.TransactionChannel = (byte)value;
		}

		public string TransactionChannelFullName => this.TransactionChannelEnum.ToFullName();

		public PaymentTypes PaymentTypeEnum
		{
			get => (PaymentTypes)this.PaymentType;
			set => this.PaymentType = (byte)value;
		}

		public sealed class ValidValue
		{
			internal ValidValue() { }
			public Services Service { get; internal set; }
			public TransactionChannels TransactionChannel { get; internal set; }
			public PaymentTypes PaymentType { get; internal set; }
		}

		public static readonly ValidValue[] ValidValues =
		{
			new ValidValue {Service = Services.AlliedBank, TransactionChannel = TransactionChannels.ATM, PaymentType = PaymentTypes.Account},
			new ValidValue {Service = Services.AlliedBank, TransactionChannel = TransactionChannels.Branch, PaymentType = PaymentTypes.Cash},
			new ValidValue {Service = Services.AlliedBank, TransactionChannel = TransactionChannels.ContactCenter, PaymentType = PaymentTypes.Account},
			new ValidValue {Service = Services.AlliedBank, TransactionChannel = TransactionChannels.InternetBanking, PaymentType = PaymentTypes.Account},
			new ValidValue {Service = Services.AlliedBank, TransactionChannel = TransactionChannels.MobileBanking, PaymentType = PaymentTypes.Account},
			new ValidValue {Service = Services.BankAlfalah, TransactionChannel = TransactionChannels.Branch, PaymentType = PaymentTypes.Cash},
			new ValidValue {Service = Services.Manual, TransactionChannel = TransactionChannels.Branch, PaymentType = PaymentTypes.Cash},
		};

		public AccountTransaction ValidateServiceChannelPaymentType()
		{
			if (ValidValues.Any(vv => vv.Service == this.ServiceEnum && vv.TransactionChannel == this.TransactionChannelEnum && vv.PaymentType == this.PaymentTypeEnum))
				return this;
			throw new InvalidOperationException($"Values are not valid. {nameof(this.Service)}: {this.ServiceEnum}, {nameof(this.TransactionChannel)}: {this.TransactionChannelEnum}, {nameof(this.PaymentType)}: {this.PaymentTypeEnum}");
		}

		public string PaymentTypeFullName => this.PaymentTypeEnum.ToFullName();
	}
}
