﻿using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Model.Entities
{
	[Serializable]
	public partial class Semester
	{
		public SemesterTypes SemesterTypeEnum
		{
			get => (SemesterTypes)this.SemesterType;
			set => this.SemesterType = (byte)value;
		}

		public override string ToString()
		{
			return ToString(this.SemesterTypeEnum, this.Year);
		}

		public static Semester ParseSemester(short semesterID)
		{
			return new Semester
			{
				SemesterID = semesterID,
				Year = GetYear(semesterID),
				SemesterTypeEnum = GetSemesterType(semesterID),
				Visible = true,
			};
		}

		public static string ToString(SemesterTypes semesterType, short year)
		{
			switch (semesterType)
			{
				case SemesterTypes.Spring:
					return "Spring-" + year;
				case SemesterTypes.Summer:
					return "Summer-" + year;
				case SemesterTypes.Fall:
					return "Fall-" + year;
				//case SemesterTypes.AnnualExam:
				//	return "Annual " + year;
				//case SemesterTypes.SupplementaryExam:
				//	return "Supplementary " + year;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static string ToString(short semesterID)
		{
			var semesterType = (SemesterTypes)(semesterID % 10);
			var year = (short)(semesterID / 10);
			return ToString(semesterType, year);
		}

		public static short GetSemesterID(short year, SemesterTypes semesterType)
		{
			switch (semesterType)
			{
				case SemesterTypes.Spring:
				case SemesterTypes.Summer:
				case SemesterTypes.Fall:
					return (short)(year * 10 + ((byte)semesterType));
				default:
					throw new ArgumentOutOfRangeException(nameof(semesterType), semesterType, null);
			}
		}

		public static short GetSemesterID(short year, byte semesterType)
		{
			return GetSemesterID(year, (SemesterTypes)semesterType);
		}

		public static short GetYear(short semesterID)
		{
			var year = (short)(semesterID / 10);
			var semesterType = (SemesterTypes)(semesterID % 10);
			switch (semesterType)
			{
				case SemesterTypes.Spring:
				case SemesterTypes.Summer:
				case SemesterTypes.Fall:
					return year;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static SemesterTypes GetSemesterType(short semesterID)
		{
			var semesterType = (SemesterTypes)(semesterID % 10);
			switch (semesterType)
			{
				case SemesterTypes.Spring:
				case SemesterTypes.Summer:
				case SemesterTypes.Fall:
					return semesterType;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static bool IsSpring(short semesterID)
		{
			return GetSemesterType(semesterID) == SemesterTypes.Spring;
		}

		public static bool IsSummer(short semesterID)
		{
			return GetSemesterType(semesterID) == SemesterTypes.Summer;
		}

		public static bool IsFall(short semesterID)
		{
			return GetSemesterType(semesterID) == SemesterTypes.Fall;
		}

		public static short PreviousSemester(short semesterID)
		{
			var year = GetYear(semesterID);
			var semester = GetSemesterType(semesterID);
			switch (semester)
			{
				case SemesterTypes.Spring:
					return GetSemesterID((short)(year - 1), SemesterTypes.Fall);
				case SemesterTypes.Summer:
					return GetSemesterID(year, SemesterTypes.Spring);
				case SemesterTypes.Fall:
					return GetSemesterID(year, SemesterTypes.Summer);
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static short NextSemester(short semesterID)
		{
			var year = GetYear(semesterID);
			var semester = GetSemesterType(semesterID);
			switch (semester)
			{
				case SemesterTypes.Spring:
					return GetSemesterID(year, SemesterTypes.Summer);
				case SemesterTypes.Summer:
					return GetSemesterID(year, SemesterTypes.Fall);
				case SemesterTypes.Fall:
					return GetSemesterID((short)(year + 1), SemesterTypes.Spring);
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static short PreviousRegularSemester(short semesterID, bool isSummerRegularSemester)
		{
			var semester = GetSemesterType(semesterID);
			switch (semester)
			{
				case SemesterTypes.Spring:
					return PreviousSemester(semesterID);
				case SemesterTypes.Summer:
					return PreviousSemester(semesterID);
				case SemesterTypes.Fall:
					if (isSummerRegularSemester)
						return PreviousSemester(semesterID);
					return PreviousSemester(PreviousSemester(semesterID));
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static short NextRegularSemester(short semesterID, bool isSummerRegularSemester)
		{
			var semester = GetSemesterType(semesterID);
			switch (semester)
			{
				case SemesterTypes.Spring:
					if (isSummerRegularSemester)
						return NextSemester(semesterID);
					return NextSemester(NextSemester(semesterID));
				case SemesterTypes.Summer:
					return NextSemester(semesterID);
				case SemesterTypes.Fall:
					return NextSemester(semesterID);
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static short AddSemesters(short semesterID, int semesters, bool isSummerRegularSemester)
		{
			if (semesters < 0)
				for (var i = semesters; i < 0; i++)
					semesterID = PreviousRegularSemester(semesterID, isSummerRegularSemester);
			if (semesterID > 0)
				for (var i = 0; i < semesters; i++)
					semesterID = NextRegularSemester(semesterID, isSummerRegularSemester);
			return semesterID;
		}
	}
}