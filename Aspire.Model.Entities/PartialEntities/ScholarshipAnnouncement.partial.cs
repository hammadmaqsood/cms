﻿using System;

namespace Aspire.Model.Entities
{
	public partial class ScholarshipAnnouncement
	{
		public Scholarship.ScholarshipTypes ScholarshipTypeEnum
		{
			get => (Scholarship.ScholarshipTypes)this.Scholarship.ScholarshipType;
			set => this.Scholarship.ScholarshipType = (byte)value;
		}

		public enum Statuses : byte
		{
			Active = 1,
			Inactive = 2,
		}

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}

		public ScholarshipAnnouncement Validate()
		{
			if (this.EndDate != null && this.StartDate > this.EndDate.Value)
				throw new InvalidOperationException("StartDate must be less than or equal to EndDate.");
			return this;
		}
	}
}
