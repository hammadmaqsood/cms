﻿namespace Aspire.Model.Entities
{
	public partial class ExamOfferedRoom
	{
		public enum RoomUsageTypes : byte
		{
			SinglePaper = 1,
			MultiplePapers = 2,
		}

		public RoomUsageTypes RoomUsageTypeEnum
		{
			get { return (RoomUsageTypes)this.RoomUsageType; }
			set { this.RoomUsageType = (byte)value; }
		}

		public static readonly byte RoomUsageTypeSinglePaper = (byte)RoomUsageTypes.SinglePaper;
		public static readonly byte RoomUsageTypeMultiplePapers = (byte)RoomUsageTypes.MultiplePapers;
	}
}
