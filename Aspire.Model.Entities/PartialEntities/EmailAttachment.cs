﻿using Aspire.Lib.Extensions;
using System;

namespace Aspire.Model.Entities
{
	public partial class EmailAttachment
	{
		public EmailAttachment Validate()
		{
			this.FileName = this.FileName.TrimAndCannotBeEmpty();
			if (this.FileContents == null)
				throw new InvalidOperationException();
			return this;
		}
	}
}
