﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class MarksSheetChanx
	{
		[EnumGuids]
		public enum ActionTypes
		{
			[EnumGuidValue("229FC83A-11A9-4207-89E8-42E26441D6C0", "Add Assignment")] AddAssignment,
			[EnumGuidValue("16665F9B-2B8E-46BC-8080-D24DA9FAEE60", "Add Quiz")] AddQuiz,
			[EnumGuidValue("A371ED34-7D2F-43C7-AC66-EE01EA73C3A0", "Delete Assignment")] DeleteAssignment,
			[EnumGuidValue("DA2C99DC-1556-4270-AE42-7B4333F3D19B", "Delete Quiz")] DeleteQuiz,
			[EnumGuidValue("54974CC9-682E-44A9-8313-D4838408EA8A", "Update Assignment")] UpdateAssignment,
			[EnumGuidValue("76BE34E1-419E-4732-9E71-4E9AA64F342D", "Update Quiz")] UpdateQuiz,
			[EnumGuidValue("D4CC49F3-63AB-4E66-9B90-03DBA024271A", "Compile Assignments")] CompileAssignments,
			[EnumGuidValue("A4021154-675C-465B-945B-D0D7AA3370B7", "Compile Quizzes")] CompileQuizzes,
			[EnumGuidValue("5FB009FD-473F-42DA-A21C-40A5EEF5DFB5", "Compile Internals")] CompileInternals,
			[EnumGuidValue("3C848FF0-55C0-4D91-9C3A-189C41C18F4C", "Update Assignments")] UpdateAssignments,
			[EnumGuidValue("CE2D1D3F-6C7C-4E78-B09E-74014DE38C41", "Update Quizzes")] UpdateQuizzes,
			[EnumGuidValue("7B8C2430-1358-406A-9DA1-6FCD6B812398", "Update Mid")] UpdateMid,
			[EnumGuidValue("247FC57F-411A-47C1-911F-2AA32D79F2AD", "Update Final")] UpdateFinal,
			[EnumGuidValue("6063AA53-A55C-4335-84F7-80E01EC410A3", "Compile Total/Grades")] CompileTotalAndGrades,
			[EnumGuidValue("D46AF4ED-F45A-4A6D-916C-BE58AB2EF5C0", "Lock Marks Entry")] LockMarksEntry,
			[EnumGuidValue("18EC879B-285E-48DE-842B-64F4C8333DDC", "Unlock Marks Entry")] UnlockMarksEntry,
			[EnumGuidValue("CD88B999-FBDE-46A2-9081-96C94351A952", "Unlock Marks Entry for Registered Course")] UnlockMarksEntryForRegisteredCourse,
			[EnumGuidValue("497ABA18-0258-4BB2-A4ED-CC054A144204", "Unlock Assignment")] UnlockAssignment,
			[EnumGuidValue("692FEA2A-0A31-4968-A259-08BA7D5B82EE", "Unlock Assignments")] UnlockAssignments,
			[EnumGuidValue("A026E060-8B02-4586-9CC7-A34C2266121E", "Unlock Quiz")] UnlockQuiz,
			[EnumGuidValue("5F3CDF5F-1DA9-4192-B0A4-F1BD94F28CAC", "Unlock Quizzes")] UnlockQuizzes,
			[EnumGuidValue("E9917FE3-2A57-4E81-A630-51D08A59F1D6", "Unlock Internals")] UnlockInternals,
			[EnumGuidValue("834F4D3D-C3C2-4D9A-882B-FFA7043C8624", "Unlock Mid")] UnlockMid,
			[EnumGuidValue("2ACB9482-514C-443D-9C8C-3D236762D963", "Unlock Final")] UnlockFinal,
			[EnumGuidValue("A4A5D196-BB13-48CE-AF5E-44DC255B1F2A", "Submit to HoD")] SubmitToHOD,
			[EnumGuidValue("9E3E6F13-3F99-4190-AE47-97EEB293885A", "Submit to Campus")] SubmitToCampus,
			[EnumGuidValue("63282D2B-A673-45CE-B8DB-5313B7F1A804", "Submit to BUHO Exams")] SubmitToUniversity,
			[EnumGuidValue("E0AF879B-B52C-4300-ABA1-584D62EFEAE4", "Revert Submit to HoD")] RevertSubmitToHOD,
			[EnumGuidValue("995C6F55-3EC9-4CAA-9D70-244CDCB95669", "Revert Submit to Campus")] RevertSubmitToCampus,
			[EnumGuidValue("AA11E6BD-98DC-4C42-927A-2DE463EB63CE", "Revert Submit to BUHO Exams")] RevertSubmitToUniversity,
		}

		public ActionTypes ActionTypeEnum
		{
			get => this.ActionType.GetEnum<ActionTypes>();
			set => this.ActionType = value.GetGuid();
		}

		public string ActionTypeFullName => this.ActionTypeEnum.GetFullName();

		[EnumGuids]
		public enum ManagementEmailRecipients : byte
		{
			[EnumGuidValue("998AFC52-932C-4937-AD86-E86EC0FC81F4", "Before Submit To University")]
			BeforeSubmitToUniversity,
			[EnumGuidValue("D92C1D0C-98AA-488A-9F94-D65A8EC569F6", "After Submit To University")]
			AfterSubmitToUniversity
		}

		public ManagementEmailRecipients? ManagementEmailRecipientsEnum
		{
			get => this.ManagementEmailRecipient?.GetEnum<ManagementEmailRecipients>();
			set => this.ManagementEmailRecipient = value?.GetGuid();
		}
	}
}
