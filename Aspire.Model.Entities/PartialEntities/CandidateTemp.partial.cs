using Aspire.Lib.Extensions;
using System;
using System.Data;

namespace Aspire.Model.Entities
{
	public partial class CandidateTemp
	{
		public CandidateTemp Validate()
		{
			this.Name = this.Name.TrimAndCannotEmptyAndMustBeUpperCase();
			this.Email = this.Email.ValidateEmailAddress().CannotBeEmptyOrWhitespace();
			if (this.CodeGeneratedDate > this.CodeExpiryDate)
				throw new InvalidOperationException("Expire<Generated");
			if (Guid.Empty == this.VerificationCode)
				throw new DataException();
			return this;
		}
	}
}