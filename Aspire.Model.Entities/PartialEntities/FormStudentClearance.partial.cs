﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class FormStudentClearance
	{
		[EnumGuids]
		public enum StudentClearanceStatuses
		{
			[EnumGuidValue("22a83b67-39f6-4f28-b041-b71f8cbe95df", "Not Initiated")]
			NotInitiated,
			[EnumGuidValue("bfd3e175-4083-4904-bda8-a44547b6d00b", "Initiated")]
			Initiated,
			[EnumGuidValue("c57a48e3-81a7-4c0d-a897-1fe05ea2dc84", "In Process")]
			InProcess,
			[EnumGuidValue("48314AA2-E856-4FDB-94C5-1FF854C033A2", "Not Verified By SSC")]
			NotVerifiedBySSC,
			[EnumGuidValue("40933E4E-77FE-464E-8BF9-A3191C6B0D0F", "Cancelled")]
			Cancelled,
			[EnumGuidValue("79913320-FA4A-4CB6-A371-9626D77FE5D3", "Rejected")]
			Rejected,
			[EnumGuidValue("6a4ef259-aa16-4891-9ffe-8845dd03adc0", "Completed")]
			Completed
		}

		public StudentClearanceStatuses StudentClearanceStatusEnum
		{
			get => this.StudentClearanceStatus.GetEnum<StudentClearanceStatuses>();
			set => this.StudentClearanceStatus = value.GetGuid();
		}

		[EnumGuids]
		public enum StudentClearanceSSCStatuses
		{
			[EnumGuidValue("691FE3C7-875B-4427-BD9C-892897993CA7", "Pending")]
			Pending,
			[EnumGuidValue("567DDA25-EEE4-49EF-855B-6163CBF480CA", "Not Verified")]
			NotVerified,
			[EnumGuidValue("EF3EA882-09E6-4417-8284-AE49ED41D18A", "Verified")]
			Verified
		}

		public StudentClearanceSSCStatuses StudentClearanceSSCStatusEnum
		{
			get => this.StudentClearanceSSCStatus.GetEnum<StudentClearanceSSCStatuses>();
			set => this.StudentClearanceSSCStatus = value.GetGuid();
		}
	}
}
