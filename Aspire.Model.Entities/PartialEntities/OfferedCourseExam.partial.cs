﻿namespace Aspire.Model.Entities
{
	public partial class OfferedCourseExam
	{
		public enum ExamTypes : byte
		{
			Assignment = 1,
			Quiz = 2,
		}

		public ExamTypes ExamTypeEnum
		{
			get => (ExamTypes)this.ExamType;
			set => this.ExamType = (byte)value;
		}
	}
}