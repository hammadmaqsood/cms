﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Aspire.Model.Entities
{
	public partial class StudentFee
	{
		public enum FeeTypes : byte
		{
			Attendance = 1,
			Challan = 2,
			Refund = 3,
		}

		public FeeTypes FeeTypeEnum
		{
			get => (FeeTypes)this.FeeType;
			set => this.FeeType = (byte)value;
		}

		public static readonly byte FeeTypeChallan = (byte)FeeTypes.Challan;
		public static readonly byte FeeTypeRefund = (byte)FeeTypes.Refund;

		public enum Statuses : byte
		{
			Paid = 1,
			NotPaid = 2,
		}

		public static readonly byte StatusNotPaid = (byte)Statuses.NotPaid;
		public static readonly byte StatusPaid = (byte)Statuses.Paid;

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}

		public string StatusFullName => this.StatusEnum.ToString().SplitCamelCasing();

		public string Semester => this.SemesterID.ToSemesterString();

		public bool IsZero => this.GrandTotalAmount == 0 && this.StatusEnum == Statuses.Paid;

		public InstallmentNos NoOfInstallmentsEnum
		{
			get => (InstallmentNos)this.NoOfInstallments;
			set => this.NoOfInstallments = (byte)value;
		}

		public StudentFee Validate(List<FeeHead> feeHeads)
		{
			switch (this.FeeTypeEnum)
			{
				case FeeTypes.Attendance:
					if (this.NoOfInstallmentsEnum != InstallmentNos.One)
						throw new InvalidOperationException("NoOfInstallments must be One.");
					if (this.TotalAmount != 0 || this.ConcessionAmount != 0 || this.GrandTotalAmount != 0)
						throw new InvalidOperationException("Amounts must be 0.");
					if (this.StudentFeeChallans?.Any() ?? false)
						throw new InvalidOperationException("StudentFeeChallans cannot be added to FeeTypeAttendance.");
					if (this.StudentFeeDetails?.Any() ?? false)
						throw new InvalidOperationException("StudentFeeDetails cannot be added to FeeTypeAttendance.");
					if (this.StudentFeeConcessionTypes?.Any() ?? false)
						throw new InvalidOperationException("StudentFeeConcessionTypes cannot be added to FeeTypeAttendance.");
					return this;
				case FeeTypes.Challan:
				case FeeTypes.Refund:
					if (this.StudentFeeDetails.Any(d => feeHeads.All(fh => fh.FeeHeadID != d.FeeHeadID)))
						throw new InvalidDataException("Invalid Fee Head Found.");

					var detailedTotalAmount = 0;
					var detailedConcessionAmount = 0;
					var detailedGrandTotal = 0;

					foreach (var studentFeeDetail in this.StudentFeeDetails)
					{
						if (studentFeeDetail.GrandTotalAmount != (studentFeeDetail.TotalAmount ?? 0) - (studentFeeDetail.ConcessionAmount ?? 0))
							throw new InvalidDataException("StudentFeeDetail record is invalid.");
						switch (feeHeads.Find(fh => fh.FeeHeadID == studentFeeDetail.FeeHeadID).RuleTypeEnum)
						{
							case FeeHead.RuleTypes.Credit:
								detailedTotalAmount += studentFeeDetail.TotalAmount ?? 0;
								detailedConcessionAmount += studentFeeDetail.ConcessionAmount ?? 0;
								detailedGrandTotal += studentFeeDetail.GrandTotalAmount;
								break;
							case FeeHead.RuleTypes.Debit:
								detailedTotalAmount -= studentFeeDetail.TotalAmount ?? 0;
								detailedConcessionAmount -= studentFeeDetail.ConcessionAmount ?? 0;
								detailedGrandTotal -= studentFeeDetail.GrandTotalAmount;
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}
					}

					var withholdingTaxFeeHeadID = feeHeads.SingleOrDefault(fh => fh.FeeHeadTypeEnum == FeeHead.FeeHeadTypes.WithholdingTax)?.FeeHeadID;
					if (withholdingTaxFeeHeadID != null)
					{
						if (this.StudentFeeDetails.Any(sfd => sfd.FeeHeadID == withholdingTaxFeeHeadID.Value))
						{
							if (this.StudentFeeDetails.Count != 1)
								throw new InvalidOperationException("Generate a separate Fee Challan for Withholding Tax.");
							this.ExemptWHT = true;
						}
					}

					if (this.TotalAmount != detailedTotalAmount)
						throw new InvalidDataException("Sum of TotalAmount is not valid.");
					if (this.ConcessionAmount != detailedConcessionAmount)
						throw new InvalidDataException("Sum of ConcessionAmount is not valid.");
					if (this.GrandTotalAmount != detailedGrandTotal)
						throw new InvalidDataException("Sum of GrandTotalAmount is not valid.");

					if (this.GrandTotalAmount != (this.TotalAmount - this.ConcessionAmount))
						throw new InvalidDataException("GrandTotalAmount is not valid.");
					InstallmentNos[] installments;
					switch (this.NoOfInstallmentsEnum)
					{
						case InstallmentNos.One:
							installments = new[] { InstallmentNos.One };
							break;
						case InstallmentNos.Two:
							installments = new[] { InstallmentNos.One, InstallmentNos.Two };
							break;
						case InstallmentNos.Three:
							installments = new[] { InstallmentNos.One, InstallmentNos.Two, InstallmentNos.Three };
							break;
						case InstallmentNos.Four:
							installments = new[] { InstallmentNos.One, InstallmentNos.Two, InstallmentNos.Three, InstallmentNos.Four, };
							break;
						case InstallmentNos.Five:
							installments = new[] { InstallmentNos.One, InstallmentNos.Two, InstallmentNos.Three, InstallmentNos.Four, InstallmentNos.Five, };
							break;
						default:
							throw new NotImplementedEnumException(this.NoOfInstallmentsEnum);
					}

					if (this.StudentFeeChallans.Count != installments.Length)
						throw new InvalidDataException("Fee Challans must be equal to No. of Installments.");
					foreach (var installmentNo in installments)
						if (this.StudentFeeChallans.All(c => c.InstallmentNoEnum != installmentNo))
							throw new InvalidDataException("Fee Challans does not exists. Installment No.: " + installmentNo);

					if (this.StudentFeeDetails.Any(d => d.ConcessionAmount != null) && !this.StudentFeeConcessionTypes.Any())
						throw new InvalidDataException("Concession types must be mentioned when concession is given.");

					if (this.StudentFeeDetails.Any(d => d.TotalAmount == 0 || d.ConcessionAmount == 0))
						throw new InvalidDataException("Amount and Concession values must be null if zero.");

					if (this.StudentFeeChallans.Sum(c => c.Amount) != this.GrandTotalAmount)
						throw new InvalidDataException("Sum of Installment Amount is not valid.");
					return this;
				default:
					throw new NotImplementedEnumException(this.FeeTypeEnum);
			}
		}

		public StudentFee ValidateForAdd(List<FeeHead> feeHeads)
		{
			this.Validate(feeHeads);
			if (this.StudentFeeConcessionTypes.Any(sfct => sfct.StudentFeeConcessionTypeID != 0))
				throw new InvalidOperationException("StudentFeeConcessionTypeID must be 0.");
			if (this.StudentFeeDetails.Any(sfd => sfd.StudentFeeDetailID != 0))
				throw new InvalidOperationException("StudentFeeDetailID must be 0.");
			if (this.StudentFeeChallans.Any(sfc => sfc.StudentFeeChallanID != 0))
				throw new InvalidOperationException("StudentFeeChallanID must be 0.");
			if (this.StudentFeeChallans.Count != 1 || this.NoOfInstallmentsEnum != InstallmentNos.One)
				throw new InvalidOperationException("NoOfInstallments must be one for Refund Type.");

			var studentFeeChallan = this.StudentFeeChallans.Single();

			switch (studentFeeChallan.StatusEnum)
			{
				case StudentFeeChallan.Statuses.NotPaid:
					if (studentFeeChallan.Amount == 0)
						throw new InvalidOperationException("Status must be paid for zero amount.");
					break;
				case StudentFeeChallan.Statuses.Paid:
					if (studentFeeChallan.Amount != 0)
						throw new InvalidOperationException("Status cannot be paid if amount is not zero.");
					break;
				default:
					throw new NotImplementedEnumException(studentFeeChallan.StatusEnum);
			}
			if (studentFeeChallan.AccountTransactionID != null)
				throw new InvalidOperationException(nameof(studentFeeChallan.AccountTransactionID));

			switch (this.StatusEnum)
			{
				case Statuses.NotPaid:
					if (this.GrandTotalAmount == 0)
						throw new InvalidOperationException("Status must be paid for zero amount.");
					break;
				case Statuses.Paid:
					if (this.GrandTotalAmount != 0)
						throw new InvalidOperationException("Status cannot be paid if amount is not zero.");
					break;
				default:
					throw new NotImplementedEnumException(this.StatusEnum);
			}

			switch (this.FeeTypeEnum)
			{
				case FeeTypes.Attendance:
					break;
				case FeeTypes.Challan:
					break;
				case FeeTypes.Refund:
					if (this.StudentFeeConcessionTypes.Any())
						throw new InvalidOperationException("Fee Concession must be null for Refund Type.");
					break;
				default:
					throw new NotImplementedEnumException(this.FeeTypeEnum);
			}

			return this;
		}
	}
}
