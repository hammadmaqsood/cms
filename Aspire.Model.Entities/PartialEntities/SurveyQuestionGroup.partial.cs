﻿using Aspire.Lib.Extensions;

namespace Aspire.Model.Entities
{
	public partial class SurveyQuestionGroup
	{
		public SurveyQuestionGroup Validate()
		{
			this.QuestionGroupName = this.QuestionGroupName.TrimAndCannotBeEmpty();
			return this;
		}
	}
}
