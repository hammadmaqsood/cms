﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class AlumniJobPosition
	{
		[EnumGuids]
		public enum JobStatuses
		{
			[EnumGuidValue("1D684586-AAE2-418D-AE62-8A364AC61066", "Employed")]
			Employed,
			[EnumGuidValue("89CECF72-1412-4F97-B587-A9D9700284AF", "Unemployed")]
			Unemployed,
			[EnumGuidValue("747E17EE-C71A-4C9F-A92B-E7F98B603CE5", "Self Employed")]
			SelfEmpoyed,
			[EnumGuidValue("237B9C2F-52F4-4875-8E60-81D99368C41A", "Home Maker")]
			HomeMaker,
			[EnumGuidValue("F4A3DDAA-ECDC-40E0-8E27-1E963C1D96AE", "Higer Studies")]
			HigherStudies,
			[EnumGuidValue("70A691ED-89EE-4817-8089-B0C32A5DACF0", "Non Contactable")]
			NonContactable,
			[EnumGuidValue("A95D21E7-3D1D-414D-B8A0-9F09DC589F0D", "Other")]
			Other,
		}

		public JobStatuses JobStatusEnum
		{
			get => this.JobStatus.GetEnum<JobStatuses>();
			set => this.JobStatus = value.GetGuid();
		}
	}
}
