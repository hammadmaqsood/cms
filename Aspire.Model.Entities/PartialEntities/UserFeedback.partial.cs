﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities.Common;
using System;
using System.Linq;

namespace Aspire.Model.Entities
{
	public partial class UserFeedback
	{
		[EnumGuids]
		public enum FeedbackBoxTypes
		{
			[EnumGuidValue("36996536-5a21-42f9-b3a4-c7192c14fe68", "Rector")]
			Rector = 1,
			[EnumGuidValue("22a5fe3f-e2ca-4a5d-ba3f-dc9ea4a7c3ba", "DG Campus")]
			DirectorGeneralCampus = 2,
		}

		[EnumGuids]
		public enum FeedbackTypes
		{
			[EnumGuidValue("B12578A7-F35C-4C48-B7FF-C0DD745E6615", "Suggestion")]
			Suggestion,
			[EnumGuidValue("4ae709e9-1ec7-47f5-bbe4-6230c5f8746e", "Complaint")]
			Complaint
		}

		[EnumGuids]
		public enum Statuses
		{
			[EnumGuidValue("f276e882-e65e-4366-b92d-a12d75a1c837", "Pending")]
			Pending,
			[EnumGuidValue("82d319f0-2118-4569-8f58-1d39a9486171", "In Process")]
			InProcess,
			[EnumGuidValue("f5f76503-d905-48b5-985a-79d3befc07bb", "Closed")]
			Closed,
			[EnumGuidValue("6358caf7-7cce-43eb-9018-402c30509e6c", "Spam/Junk")]
			SpamJunk
		}

		public FeedbackBoxTypes FeedbackBoxTypeEnum
		{
			get => this.FeedbackBoxType.GetEnum<FeedbackBoxTypes>();
			set => this.FeedbackBoxType = value.GetGuid();
		}

		public FeedbackTypes FeedbackTypeEnum
		{
			get => this.FeedbackType.GetEnum<FeedbackTypes>();
			set => this.FeedbackType = value.GetGuid();
		}

		public Statuses StatusEnum
		{
			get => this.Status.GetEnum<Statuses>();
			set => this.Status = value.GetGuid();
		}

		public UserFeedback Validate()
		{
			this.FeedbackBoxType = this.FeedbackBoxType.ValidateEnum<FeedbackBoxTypes>();
			this.FeedbackType = this.FeedbackType.ValidateEnum<FeedbackTypes>();
			this.FeedbackDetails = this.FeedbackDetails.CannotBeEmptyOrWhitespace();
			this.Status = this.Status.ValidateEnum<Statuses>();
			switch (this.StatusEnum)
			{
				case Statuses.Pending:
					if (this.UserFeedbackDiscussions?.Any() == true)
						throw new InvalidOperationException();
					break;
				case Statuses.SpamJunk:
					break;
				case Statuses.InProcess:
					break;
				case Statuses.Closed:
					if (this.UserFeedbackDiscussions?.Any() != true)
						throw new InvalidOperationException();
					break;
				default:
					throw new NotImplementedEnumException(this.StatusEnum);
			}
			return this;
		}
	}
}