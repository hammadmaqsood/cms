﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class SMTPServer
	{
		[EnumGuids]
		public enum Statuses
		{
			[EnumGuidValue("{C6740F9B-0810-4FBC-80ED-AF29002867DE}", "Active")]
			Active,
			[EnumGuidValue("{38743814-A631-43CB-A64A-F0F58016AEAD}", "Inactive")]
			Inactive,
		}

		public Statuses StatusEnum
		{
			get => this.Status.GetEnum<Statuses>();
			set => this.Status = value.GetGuid();
		}

		public SMTPServer Validate()
		{
			return this;
		}
	}
}
