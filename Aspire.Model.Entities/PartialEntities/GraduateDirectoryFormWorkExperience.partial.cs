﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class GraduateDirectoryFormWorkExperience
	{
		public enum ExperienceTypes : byte
		{
			Internship = 1,
			PartTimeJob = 2,
			FullTimeJob = 3,
		}

		public ExperienceTypes? ExperienceTypeEnum
		{
			get => (ExperienceTypes?)this.ExperienceType;
			set => this.ExperienceType = (byte?)value;
		}

		public string ExperienceTypeFullName => this.ExperienceTypeEnum?.ToFullName();
	}
}
