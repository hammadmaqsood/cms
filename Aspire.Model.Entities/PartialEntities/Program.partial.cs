﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class Program
	{
		public enum Statuses : byte
		{
			Active = 1,
			Inactive = 2,
		}

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}

		public string StatusFullName => this.StatusEnum.ToFullName();

		public DegreeLevels DegreeLevelEnum
		{
			get => (DegreeLevels)this.DegreeLevel;
			set => this.DegreeLevel = (byte)value;
		}

		public string DegreeLevelFullName => this.DegreeLevelEnum.ToFullName();

		public ProgramTypes ProgramTypeEnum
		{
			get => (ProgramTypes)this.ProgramType;
			set => this.ProgramType = (byte)value;
		}

		public string ProgramTypeFullName => this.ProgramTypeEnum.ToFullName();

		public ProgramDurations DurationEnum
		{
			get => (ProgramDurations)this.Duration;
			set => this.Duration = (byte)value;
		}

		public string DurationFullName => this.DurationEnum.ToFullName();

		public Program Validate()
		{
			this.ProgramName = this.ProgramName.TrimAndCannotBeEmpty();
			this.ProgramShortName = this.ProgramShortName.TrimAndCannotBeEmpty();
			this.ProgramAlias = this.ProgramAlias.TrimAndCannotEmptyAndMustBeUpperCase();
			this.ProgramCode = this.ProgramCode.TrimAndMakeItNullIfEmpty();
			if (this.ProgramCode != null)
				this.ProgramCode = this.ProgramCode.MustMatch(@"^\d{2}$");
			return this;
		}
	}
}
