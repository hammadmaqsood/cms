﻿namespace Aspire.Model.Entities
{
	public partial class RecurringTask
	{
		public enum Statuses : byte
		{
			Success = 1,
			Failed = 2,
			CancelledSafely = 3,
			Cancelled = 4,
		}

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}
	}
}
