﻿using System.IO;
using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class AdmissionCriteriaPreReqDegree
	{
		public DegreeTypes DegreeTypeEnum
		{
			get { return (DegreeTypes)this.DegreeType; }
			set { this.DegreeType = (byte)value; }
		}

		public string DegreeTypeEnumFullName => this.DegreeTypeEnum.ToFullName();

		public void Validate()
		{
			if (this.AnnualExamSystem)
				if (this.AnnualExamSystemMinimumPercentage == null)
					throw new InvalidDataException(nameof(this.AnnualExamSystemMinimumPercentage) + " cannot be null.");
			if (this.SemesterSystem)
			{
				if (this.SemesterSystemMinCGPAOutOf4 == null)
					throw new InvalidDataException(nameof(this.SemesterSystemMinCGPAOutOf4) + " cannot be null.");
				if (this.SemesterSystemMinCGPAOutOf5 == null)
					throw new InvalidDataException(nameof(this.SemesterSystemMinCGPAOutOf5) + " cannot be null.");
			}
			if (this.AnnualExamSystem == false && this.SemesterSystem == false)
				throw new InvalidDataException("Both Annual System and Semester System cannot be false.");
		}
	}
}
