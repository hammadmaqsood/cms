﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class ProjectThesisInternshipChanx
	{
		[EnumGuids]
		public enum ActionTypes
		{
			[EnumGuidValue("094B5F59-10AC-4B74-BE42-B3C0C99D8865", "Added")] Added,
			[EnumGuidValue("440C4833-69EA-4131-8264-D75221816234", "Updated")] Updated,
			[EnumGuidValue("CA72CFB2-A875-4E1C-87C1-DEF5AFBE9788", "Deleted")] Deleted,
			[EnumGuidValue("CD1D35B7-A8D3-4D7E-B86E-84BD6F85B790", "Registered Course Attached")] RegisteredCourseAttached,
			[EnumGuidValue("B163A3D9-A182-47E2-9B23-DFA493A74590", "Registered Course Detached")] RegisteredCourseDetached,
			[EnumGuidValue("3D519DDB-DFC8-492E-963E-037EE1FD14EC", "Lock Marks Entry")] LockMarksEntry,
			[EnumGuidValue("BB688F3C-AB98-4120-89B8-A8A166F6F1BA", "Unlock Marks Entry")] UnlockMarksEntry,
			[EnumGuidValue("381DA48F-3FE0-48E0-A7C7-BAF4A0D2CA74", "Submit to HoD")] SubmitToHOD,
			[EnumGuidValue("53412F12-6943-4D9D-8F54-1EC5D34369C3", "Submit to Campus")] SubmitToCampus,
			[EnumGuidValue("E3233B16-B01B-4628-BBC7-EB1A78D8C042", "Submit to BUHO Exams")] SubmitToUniversity,
			[EnumGuidValue("6D479517-98E8-4534-ABB4-FE65DE7F1CCD", "Revert Submit to HoD")] RevertSubmitToHOD,
			[EnumGuidValue("B5270008-3A05-4DB5-A4EC-9C472DCD6659", "Revert Submit to Campus")] RevertSubmitToCampus,
			[EnumGuidValue("3B28A6D2-A766-49C8-8B6E-5969B75A370F", "Revert Submit to BUHO Exams")] RevertSubmitToUniversity,
		}

		public ActionTypes ActionTypeEnum
		{
			get => this.ActionType.GetEnum<ActionTypes>();
			set => this.ActionType = value.GetGuid();
		}

		public string ActionTypeFullName => this.ActionTypeEnum.GetFullName();

		[EnumGuids]
		public enum ManagementEmailRecipients : byte
		{
			[EnumGuidValue("3142DB03-F661-4BFE-9BFC-3F6784DE598E", "After Submit To HoD")]
			AfterSubmitToHOD,
			[EnumGuidValue("8FB3E55B-B5EA-41B7-9533-7F3610E96DC4", "After Submit To Campus")]
			AfterSubmitToCampus,
			[EnumGuidValue("9062493F-9C90-4B99-9667-534FE551B892", "After Submit To BUHO Exams")]
			AfterSubmitToUniversity
		}

		public ManagementEmailRecipients? ManagementEmailRecipientsEnum
		{
			get => this.ManagementEmailRecipient?.GetEnum<ManagementEmailRecipients>();
			set => this.ManagementEmailRecipient = value?.GetGuid();
		}
	}
}
