﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class ExamRemarksPolicyDetail
	{
		public ExamRemarksTypes ExamRemarksTypeEnum
		{
			get => (ExamRemarksTypes)this.ExamRemarksType;
			set => this.ExamRemarksType = (byte)value;
		}
	}
}
