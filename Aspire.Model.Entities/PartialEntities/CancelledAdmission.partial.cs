﻿using Aspire.Lib.Extensions;

namespace Aspire.Model.Entities
{
	public partial class CancelledAdmission
	{
		public enum Statuses : byte
		{
			Blocked = 1,
			Resolved = 2,
		}

		public Statuses StatusEnum
		{
			get { return (Statuses)this.Status; }
			set { this.Status = (byte)value; }
		}

		public static byte StatusBlocked => (byte)Statuses.Blocked;
		public static byte StatusResolved => (byte)Statuses.Resolved;

		public CancelledAdmission Validate()
		{
			this.Remarks = this.Remarks.Trim().TrimAndCannotBeEmpty();
			return this;
		}
	}
}
