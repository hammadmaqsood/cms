﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class EmailSendAttempt
	{
		[EnumGuids]
		public enum Statuses
		{
			[EnumGuidValue("{97640037-18DF-4AAD-B935-28A3DF344EC6}", "Success")]
			Success,
			[EnumGuidValue("{92D41872-7A21-4FE5-AB5B-576B1A7D9838}", "Success (No Recipient)")]
			SuccessNoRecipient,
			[EnumGuidValue("{DDAD1699-E235-4BAA-A41C-D26E5AB7684B}", "Expired")]
			Expired,
			[EnumGuidValue("{42BA99B3-8742-4254-BD84-02FBA0BB488E}", "SMTP Server Not Available")]
			SMTPServerNotAvailable,
			[EnumGuidValue("{DA0AB691-77CB-431D-AD18-80476A7D5D2C}", "Error")]
			Error,
			[EnumGuidValue("{051A776B-92AA-4359-9B62-1C028794BC3E}", "Attempts Exceeded")]
			AttemptsExceeded
		}

		public Statuses StatusEnum
		{
			get => this.Status.GetEnum<Statuses>();
			set => this.Status = value.GetGuid();
		}
	}
}