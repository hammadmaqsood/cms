﻿using System;

namespace Aspire.Model.Entities
{
	public partial class TransferredStudentCourseMapping
	{
		[Flags]
		public enum Statuses
		{
			Included = 1,
			Excluded = 2,
		}

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}
	}
}
