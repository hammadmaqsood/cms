﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class OfferedCourseAttendance
	{
		public enum ClassTypes : byte
		{
			Class = 1,
			Lab = 2,
		}

		public ClassTypes ClassTypeEnum
		{
			get { return (ClassTypes)this.ClassType; }
			set { this.ClassType = (byte)value; }
		}

		public enum HoursValues
		{
			Absent = 1,
			One = 2,
			OneAndHalf = 3,
			Two = 4,
			Three = 5,
		}

		public HoursValues HoursValue
		{
			get { return this.Hours.ToHoursValue(); }
			set { this.Hours = value.ToDecimal(); }
		}
	}
}
