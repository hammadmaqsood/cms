﻿namespace Aspire.Model.Entities
{
	public partial class ExamBuilding
	{
		public enum Statuses : byte
		{
			Active = 1,
			Inactive = 2,
		}

		public Statuses StatusEnum
		{
			get { return (Statuses)this.Status; }
			set { this.Status = (byte)value; }
		}

		public static readonly byte StatusActive = (byte)Statuses.Active;
		public static readonly byte StatusInactive = (byte)Statuses.Inactive;

		public string StatusFullName => this.StatusEnum.ToString();
	}
}
