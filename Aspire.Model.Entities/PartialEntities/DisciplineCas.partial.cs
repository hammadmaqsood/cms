﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aspire.Lib.Extensions;

namespace Aspire.Model.Entities
{
	public partial class DisciplineCas
	{
		public DisciplineCas Validate()
		{
			this.CaseDate = this.CaseDate.CannotBeNullAndFutureDate();
			this.CaseFileNo = this.CaseFileNo.TrimAndCannotBeEmpty();
			this.Location = this.Location.TrimAndCannotBeEmpty();
			return this;
		}
	}
}
