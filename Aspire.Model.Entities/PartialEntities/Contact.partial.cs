﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class Contact
	{
		[EnumGuids]
		public enum Designations
		{
			[EnumGuidValue("4C557802-EF60-466B-B9DF-5CB9614E9D02", "Rector")]
			Rector,
			[EnumGuidValue("DF319465-434E-4BA7-87C9-0283AEE3B468", "Pro-Rector")]
			ProRector,
			[EnumGuidValue("A30273EB-CEB2-415B-AF96-5852A0EC4F4A", "Director General")]
			DirectorGeneral,
			[EnumGuidValue("5F0A5082-1865-49D5-9F8B-38D1FCE17F58", "Dean")]
			Dean,
			[EnumGuidValue("84A289EF-4F5E-4A13-9C3D-59C06B17645C", "Registrar")]
			Registrar,
			[EnumGuidValue("D08EC7BA-BC56-4D20-90B6-88603FE2F076", "Director Academic Affairs")]
			DirectorAcademicAffairs,
			[EnumGuidValue("048293A5-5740-48A7-AFE7-9DCC5682811B", "Director Exams")]
			DirectorExams,
			[EnumGuidValue("6EC4B8D4-D578-4496-9F5C-341C4205678D", "Director Campus")]
			DirectorCampus,
			[EnumGuidValue("5EA72A20-3ED3-4E96-93A5-BB116C7B44FE", "Head of Department")]
			HeadOfDepartment,
			[EnumGuidValue("C6144A66-10DC-4667-B4AB-264FA452F879", "Deputy Director Exams")]
			DeputyDirectorExams,
			[EnumGuidValue("0947186F-D878-4E29-9353-B66B6A54693D", "Deputy Director Academics")]
			DeputyDirectorAcademics,
			[EnumGuidValue("CA395007-3706-46CB-9369-73579F2DCEE6", "Assistant Director Exam")]
			AssistantDirectorExam,
		}

		public Designations DesignationEnum
		{
			get => this.Designation.GetEnum<Designations>();
			set => this.Designation = value.GetGuid();
		}

		public string DesignationFullName => this.DesignationEnum.GetFullName();

		public Contact Validate()
		{
			this.Name = this.Name.TrimAndMakeItNullIfEmpty();
			this.Email = this.Email.ToLower().MustBeEmailAddress();
			return this;
		}
	}
}
