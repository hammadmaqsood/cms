﻿using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Model.Entities
{
	public partial class QAStudentOnlineTeachingSurvey
	{
		[EnumGuids]
		[Flags]
		public enum Q12Options
		{
			[EnumGuidValue("da21c6f9-e0c6-44f1-b214-3fe6eebad2ae", "Email")]
			Email = 1,
			[EnumGuidValue("826c519e-3069-4d1a-8d48-ca2e48f0d8bb", "Mobile Phone")]
			MobilePhone = 2,
			[EnumGuidValue("9213da36-c592-4d06-8533-c766badd7c0c", "Whatsapp")]
			WhatsApp = 4,
			[EnumGuidValue("83224158-1d40-491e-a2d9-002f32aa372f", "Landline")]
			Landline = 8,
			[EnumGuidValue("4fc5ccd1-09c9-4bdd-b931-30387450eab5", "Snail Mail (i.e. ordinary post)")]
			SnailMail = 12
		}

		[EnumGuids]
		[Flags]
		public enum Q13Options
		{
			[EnumGuidValue("031520c6-e741-4633-9fa6-475713650f07", "Broadband Connection (at home)")]
			Broadband = 1,
			[EnumGuidValue("0dfd3ffb-a531-4e1b-872d-524bbed7e8b9", "Mobile package(on phone)")]
			MobilePackage = 2,
			[EnumGuidValue("f87079e5-77b8-4eba-9d1a-61b4ca652d5e", "I go to an Internet Cafe")]
			GoToInternetCafe = 4,
			[EnumGuidValue("b8bc26ab-f033-4176-aad6-aea6a28745a6", "I go to a friend's house")]
			GoToFriendHouse = 8,
			[EnumGuidValue("7ce47052-dfad-414f-931f-589e3cb3cf6c", "I do not have access to the Internet")]
			DoNotHaveAccessToInternet = 16
		}

		[EnumGuids]
		[Flags]
		public enum Q14Options
		{
			[EnumGuidValue("b1a07863-b62d-4d44-a5ad-717e6977b1fb", "3G or 4G")]
			ThreeGOr4G = 1,
			[EnumGuidValue("e4e23702-d8a5-4b97-ae53-623160296929", "2G or less")]
			TwoGOrLess = 2
		}

		[EnumGuids]
		[Flags]
		public enum Q16Options
		{
			[EnumGuidValue("5f263bb3-aa56-4a46-87b4-71101137edd7", "No")]
			No = 1,
			[EnumGuidValue("51304d7b-0d0c-44b8-9924-c3a7279047b6", "Antenna")]
			Antenna = 2,
			[EnumGuidValue("e4dbedb7-0418-477a-9f26-d7f0b82b8ede", "Cable")]
			Cable = 4,
			[EnumGuidValue("d6bcbcbe-96fa-4540-9ea6-61f679125a3d", "Dish")]
			Dish = 8
		}

		[EnumGuids]
		[Flags]
		public enum Q17Options
		{
			[EnumGuidValue("1493849e-fd24-4f04-b200-7c8ab2f6a8aa", "None")]
			None = 1,
			[EnumGuidValue("e340e9d7-8d57-432e-b998-f74469b394e2", "0-6 hours")]
			ZeroTo6Hours = 2,
			[EnumGuidValue("be1c4ff7-df14-4830-b3dd-a446bd573932", "6-12 hours per day")]
			SixTo12Hours = 4,
			[EnumGuidValue("a0789502-188b-4884-9b2d-c1b125bbaf2f", "More than 12 hours per day")]
			MoreThan12Hours = 8,
			[EnumGuidValue("6c462ebf-edbe-4187-b00b-7894fded1f2c", "There is no electricity in my area")]
			NoElectricity = 12
		}

		[EnumGuids]
		public enum Q18Options
		{
			[EnumGuidValue("12396052-d7c0-473a-b1d4-4da856356544", "Yes")]
			Yes = 1,
			[EnumGuidValue("43f5b873-8fea-4d5b-9496-a9e9ed6287d4", "No")]
			No = 2,
		}

		[EnumGuids]
		[Flags]
		public enum Q19Options
		{
			[EnumGuidValue("261224fb-c27f-4d6e-9012-c881f49f4d30", "Microsoft Teams")]
			MicrosoftTeams = 1,
			[EnumGuidValue("7505e010-3a1c-4fb4-b52f-724fd90e8a69", "Zoom")]
			Zoom = 2,
			[EnumGuidValue("95396b98-3b2a-4856-bff7-0b7b6e65014d", "Google Classroom")]
			GoogleClassroom = 4,
			[EnumGuidValue("f39e767d-8107-4741-a76f-dc8f6ea6b2ba", "Other")]
			Other = 8
		}

		[EnumGuids]
		[Flags]
		public enum Q20Options
		{
			[EnumGuidValue("edee3a95-edcf-46f8-b1b4-308d803fcdc1", "Satisfied")]
			Satisfied = 1,
			[EnumGuidValue("441b453a-067c-4004-a8b8-da6d9b8511e6", "Somewhat Satisfied")]
			SomewhatSatisfied = 2,
			[EnumGuidValue("f00010c1-0ec1-47c2-a935-84d3653ffb0a", "Not Satisfied")]
			NotSatisfied = 4
		}

		[EnumGuids]
		[Flags]
		public enum Q21Options
		{
			[EnumGuidValue("0591cd3c-57fd-4053-a69f-83407f896dff", "Provide training to professors")]
			ProvideTrainingToProfessors = 1,
			[EnumGuidValue("418269df-4234-43df-8909-7d85a76e22c6", "Improve quality of courses")]
			ImproveQualityOfCourses = 2,
			[EnumGuidValue("a09de44e-8f16-438b-b67a-750cae199d37", "Improve the software and LMS")]
			ImproveTheSoftwareAndLMS = 4,
			[EnumGuidValue("b8668d3c-4a94-4f06-8f97-bcf18be0e347", "Provide backups on the website")]
			ProvideBackupsOnTheWebsite = 8,
			[EnumGuidValue("9a6fa016-7396-4609-ad7d-096f459d8dfe", "Use WhatsApp to send lectures")]
			UseWhatsappToSendLectures = 16,
			[EnumGuidValue("a9e98264-2261-460e-b807-4b17c10a9787", "Other (please specify briefly)")]
			Other = 32
		}
	}
}
