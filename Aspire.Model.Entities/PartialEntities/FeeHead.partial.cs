﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Model.Entities
{
	public partial class FeeHead
	{
		public enum FeeHeadTypes : byte
		{
			Arrears = 1,
			TutionFee = 2,
			WithholdingTax = 3,
			Custom = byte.MaxValue,
		}

		public FeeHeadTypes FeeHeadTypeEnum
		{
			get => (FeeHeadTypes)this.FeeHeadType;
			set => this.FeeHeadType = (byte)value;
		}

		public string FeeHeadTypeFullName => this.FeeHeadTypeEnum.ToFullName();

		public enum RuleTypes : byte
		{
			Credit = 1,
			Debit = 2,
		}

		public RuleTypes RuleTypeEnum
		{
			get => (RuleTypes)this.RuleType;
			set => this.RuleType = (byte)value;
		}

		public FeeHead Validate()
		{
			this.HeadName = this.HeadName.TrimAndCannotBeEmpty();
			this.HeadAlias = this.HeadAlias.TrimAndCannotBeEmpty();
			this.HeadShortName = this.HeadShortName.TrimAndCannotBeEmpty();

			if (this.WithholdingTaxApplicable)
			{
				if (this.FeeHeadTypeEnum == FeeHeadTypes.WithholdingTax)
					throw new InvalidOperationException("Withholding Tax is not applicable on Withholding Tax Fee Head.");
			}
			return this;
		}
	}
}
