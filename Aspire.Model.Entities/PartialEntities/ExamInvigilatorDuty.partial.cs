﻿namespace Aspire.Model.Entities
{
	public partial class ExamInvigilatorDuty
	{
		public enum InvigilatorTypes : byte
		{
			InvigilatorOne = 1,
			InvigilatorTwo = 2
		}

		public static readonly byte InvigilatorTypeInvigilatorOne = (byte)InvigilatorTypes.InvigilatorOne;
		public static readonly byte InvigilatorTypeInvigilatorTwo = (byte)InvigilatorTypes.InvigilatorTwo;
		public InvigilatorTypes InvigilatorTypeEnum
		{
			get { return (InvigilatorTypes)this.InvigilatorType; }
			set { this.InvigilatorType = (byte)value; }
		}

		public enum AttendanceStatuses : byte
		{
			Absent = 1,
			Present = 2,
		}

		public AttendanceStatuses AttendanceStatusEnum
		{
			get { return (AttendanceStatuses)this.AttendanceStatus; }
			set { this.AttendanceStatus = (byte)value; }
		}

		public static readonly byte AttendanceStatusAbsent = (byte)AttendanceStatuses.Absent;
		public static readonly byte AttendanceStatusPresent = (byte)AttendanceStatuses.Present;
	}
}
