﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;

namespace Aspire.Model.Entities
{
	public partial class AdmissionOpenProgram
	{
		public enum EntryTestTypes : byte
		{
			None = 1,
			ComputerBasedTest = 2,
			PaperBasedTest = 4,
		}

		public EntryTestTypes EntryTestTypeEnum
		{
			get => (EntryTestTypes)this.EntryTestType;
			set => this.EntryTestType = (byte)value;
		}

		public string EntryTestTypeFullName => this.EntryTestTypeEnum.ToFullName();

		public string EntryTestDateString
		{
			get
			{
				switch (this.EntryTestTypeEnum)
				{
					case EntryTestTypes.None:
						return string.Empty.ToNAIfNullOrEmpty();
					case EntryTestTypes.ComputerBasedTest:
						return this.EntryTestStartDate.ConcatenateDates(this.EntryTestEndDate, "D", " to ");
					case EntryTestTypes.PaperBasedTest:
						return this.EntryTestStartDate?.ToString("F") ?? throw new InvalidOperationException();
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		public enum ValidationResults
		{
			None,
			NoRecordFound,
			AlreadyExists,
			InvalidDateSequence,
			InvalidAdmissionOpenDates,
			InvalidAdmissionProcessingDueDate,
			InvalidInterviewDates,
			InvalidEntryTestDates,
			InvalidFinalSemester,
			InvalidMaxSemester,
			InvalidAdmissionOpenDatesForForeigners,
			Success,
		}

		public Shifts ShiftsEnum
		{
			get => (Shifts)this.Shifts;
			set => this.Shifts = (byte)value;
		}

		public IEnumerable<Shifts> ShiftEnumFlags
		{
			get => this.ShiftsEnum.GetFlags();
			set => this.ShiftsEnum = value.Combine() ?? throw new InvalidOperationException();
		}

		public ExamSystemTypes ExamSystemTypeEnum
		{
			get => (ExamSystemTypes)this.ExamSystemType;
			set => this.ExamSystemType = (byte)value;
		}

		public ValidationResults Validate()
		{
			if (this.FinalSemesterID != null && this.FinalSemesterID < this.SemesterID)
				return ValidationResults.InvalidFinalSemester;
			if (this.MaxSemesterID < this.SemesterID)
				return ValidationResults.InvalidMaxSemester;

			this.EligibilityCriteriaStatement = this.EligibilityCriteriaStatement.TrimAndCannotBeEmpty();

			if (this.AdmissionOpenDate > this.AdmissionClosingDate)
				return ValidationResults.InvalidAdmissionOpenDates;
			if (this.AdmissionClosingDate > this.AdmissionProcessingFeeDueDate)
				return ValidationResults.InvalidAdmissionProcessingDueDate;

			if (this.AdmissionOpenDateForForeigners != null && this.AdmissionClosingDateForForeigners == null)
				return ValidationResults.InvalidAdmissionOpenDatesForForeigners;
			if (this.AdmissionOpenDateForForeigners == null && this.AdmissionClosingDateForForeigners != null)
				return ValidationResults.InvalidAdmissionOpenDatesForForeigners;
			if (this.AdmissionOpenDateForForeigners != null && this.AdmissionClosingDateForForeigners != null && this.AdmissionOpenDateForForeigners.Value > this.AdmissionClosingDateForForeigners.Value)
				return ValidationResults.InvalidAdmissionOpenDatesForForeigners;

			switch (this.EntryTestTypeEnum)
			{
				case EntryTestTypes.None:
					if (this.EntryTestStartDate != null || this.EntryTestEndDate != null || this.EntryTestDuration != null)
						throw new InvalidOperationException();
					break;
				case EntryTestTypes.ComputerBasedTest:
					if (this.EntryTestStartDate == null || this.EntryTestEndDate == null || this.EntryTestDuration == null)
						throw new InvalidOperationException();
					if (this.EntryTestStartDate.Value > this.EntryTestEndDate.Value)
						return ValidationResults.InvalidEntryTestDates;
					break;
				case EntryTestTypes.PaperBasedTest:
					if (this.EntryTestStartDate != this.EntryTestEndDate || this.EntryTestDuration == null)
						throw new InvalidOperationException();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			if (this.InterviewsStartDate != null && this.InterviewsEndDate != null && this.InterviewsStartDate.Value > this.InterviewsEndDate.Value)
				return ValidationResults.InvalidEntryTestDates;

			if (this.IntakeTargetStudentsCount < 0)
				throw new InvalidOperationException();

			return ValidationResults.Success;
		}
	}
}
