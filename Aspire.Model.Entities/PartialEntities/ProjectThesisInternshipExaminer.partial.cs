﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class ProjectThesisInternshipExaminer
	{
		[EnumGuids]
		public enum ExaminerTypes
		{
			[EnumGuidValue("EA1D5E4D-4DF5-4D92-87F7-C0C7D58DB0C7", "Internal Examiner")]
			InternalExaminer,
			[EnumGuidValue("A45B3C30-6B23-45DA-80D1-696EBD033927", "External Examiner")]
			ExternalExaminer,
			[EnumGuidValue("FCA9DEB6-A9F8-42D7-A155-D87C92BF7700", "Internal Supervisor")]
			InternalSupervisor,
			[EnumGuidValue("7A9E3033-F11B-4D4F-A4D8-B15A81B80B65", "External Supervisor")]
			ExternalSupervisor
		}

		public ExaminerTypes ExaminerTypesEnum
		{
			get => this.ExaminerType.GetEnum<ExaminerTypes>();
			set => this.ExaminerType = value.GetGuid();
		}

		public string ExaminerTypesFullName => this.ExaminerType.GetFullName();

		public ProjectThesisInternshipExaminer Validate()
		{
			this.ExaminerName = this.ExaminerName.TrimAndCannotBeEmpty();
			this.ExaminerType.ValidateEnum<ExaminerTypes>();
			return this;
		}
	}
}
