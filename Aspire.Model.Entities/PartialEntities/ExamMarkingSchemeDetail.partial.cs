﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class ExamMarkingSchemeDetail
	{
		public ExamMarksTypes ExamMarksTypeEnum
		{
			get => (ExamMarksTypes)this.ExamMarksType;
			set => this.ExamMarksType = (byte)value;
		}

		public CourseCategories CourseCategoryEnum
		{
			get => (CourseCategories)this.CourseCategory;
			set => this.CourseCategory = (byte)value;
		}
	}
}
