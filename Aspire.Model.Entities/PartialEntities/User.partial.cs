﻿using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class User
	{
		public enum Statuses : byte
		{
			Active = 1,
			Inactive = 2,
		}

		public Statuses StatusEnum
		{
			get => (Statuses)this.Status;
			set => this.Status = (byte)value;
		}

		public string StatusFullName => this.StatusEnum.ToFullName();
	}
}
