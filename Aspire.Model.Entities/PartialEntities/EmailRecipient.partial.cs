﻿using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;

namespace Aspire.Model.Entities
{
	public partial class EmailRecipient
	{
		[EnumGuids]
		public enum RecipientTypes
		{
			[EnumGuidValue("{7ABAAB25-16BF-458B-AF56-5B4BAD5FA21E}", "To")]
			To,
			[EnumGuidValue("{AC84E7E5-4E2A-4D08-B291-8F1853DD97D1}", "CC")]
			Cc,
			[EnumGuidValue("{8251C189-D3ED-4392-9E62-2CE67606B4D8}", "BCC")]
			Bcc,
			[EnumGuidValue("{A7AF4C70-B80C-497D-8F80-9D3C1187A90B}", "Reply To")]
			ReplyTo
		}

		public RecipientTypes RecipientTypeEnum
		{
			get => this.RecipientType.GetEnum<RecipientTypes>();
			set => this.RecipientType = value.GetGuid();
		}

		public EmailRecipient Validate()
		{
			this.DisplayName = this.DisplayName.TrimAndMakeItNullIfEmpty();
			this.EmailAddress = this.EmailAddress.MustBeEmailAddress();
			this.RecipientType.ValidateEnum<RecipientTypes>();
			return this;
		}
	}
}