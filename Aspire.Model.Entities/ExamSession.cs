//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Model.Entities
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// todo: Description is missing for [dbo].[ExamSessions] table.
    /// </summary>
    public partial class ExamSession
    {
        public ExamSession()
        {
            this.ExamDateAndSessions = new HashSet<ExamDateAndSession>();
            this.ExamInvigilatorExamSessions = new HashSet<ExamInvigilatorExamSession>();
        }
    
        public int ExamSessionID { get; set; }
        public int ExamConductID { get; set; }
        public string SessionName { get; set; }
        public System.TimeSpan StartTime { get; set; }
        public byte TotalTimeInMinutes { get; set; }
        public short InvigilationPayRate { get; set; }
        public byte Shift { get; set; }
        public byte Sequence { get; set; }
    
        /// <summary>
        /// <para>Foreign Key Name: FK_ExamSessions_ExamConducts_ExamConductID</para>
        /// <para>Foreign Key Base Table: [dbo].[ExamSessions]</para>
        /// <para>Foreign Key Base Columns: [ExamConductID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[ExamConducts]</para>
        /// <para>Primary/Unique Key Base Columns: [ExamConductID]</para>
        /// </summary>
        public virtual ExamConduct ExamConduct { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_ExamDateAndSessions_ExamSessions_ExamSessionID</para>
        /// <para>Foreign Key Base Table: [dbo].[ExamDateAndSessions]</para>
        /// <para>Foreign Key Base Columns: [ExamSessionID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[ExamSessions]</para>
        /// <para>Primary/Unique Key Base Columns: [ExamSessionID]</para>
        /// </summary>
        public virtual ICollection<ExamDateAndSession> ExamDateAndSessions { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_ExamInvigilatorExamSessions_ExamSessions_ExamSessionID</para>
        /// <para>Foreign Key Base Table: [dbo].[ExamInvigilatorExamSessions]</para>
        /// <para>Foreign Key Base Columns: [ExamSessionID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[ExamSessions]</para>
        /// <para>Primary/Unique Key Base Columns: [ExamSessionID]</para>
        /// </summary>
        public virtual ICollection<ExamInvigilatorExamSession> ExamInvigilatorExamSessions { get; set; }
    }
}
