﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Aspire.Model.Entities.Common
{
	public static class AspireFormats
	{
		public static string GetClassName(SemesterNos? semesterNo, Sections? section, Shifts? shift)
		{
			return string.Join(" ", semesterNo?.ToFullName(), section != null ? $"({section.Value.ToFullName()})" : null, shift?.ToFullName()).TrimAndMakeItNullIfEmpty();
		}

		public static string GetClassName(short? semesterNo, int? section, byte? shift)
		{
			return GetClassName((SemesterNos?)semesterNo, (Sections?)section, (Shifts?)shift);
		}

		public static string GetClassName(string program, SemesterNos? semesterNo, Sections? section, Shifts? shift)
		{
			var className = GetClassName(semesterNo, section, shift);
			return className == null
				? program
				: $"{program}-{className}";
		}

		public static string GetClassName(string program, short? semesterNo, int? section, byte? shift)
		{
			return GetClassName(program, (SemesterNos?)semesterNo, (Sections?)section, (Shifts?)shift);
		}

		public static string GetClassName(string program, SemesterNos? semesterNoEnum)
		{
			return GetClassName(program, semesterNoEnum, null, null);
		}

		public static string GetClassName(string program, short? semesterNo)
		{
			return GetClassName(program, semesterNo, null, null);
		}

		public static readonly string ExamMarksFormat = "0.###";

		public static string ToExamMarksString(this decimal marks)
		{
			return marks.ToString(ExamMarksFormat, CultureInfo.CurrentCulture);
		}

		public static string GetRegisteredCourseStatusHtml(byte? status, byte? freezeStatus)
		{
			var list = new List<Tuple<string, string>>();
			if (status != null)
				list.AddRange(((RegisteredCours.Statuses)status.Value).GetFlags().Select(s => new Tuple<string, string>(s.ToFullName(), s.ToShortName())));
			if (freezeStatus != null)
			{
				var fs = (FreezedStatuses)freezeStatus.Value;
				list.Add(new Tuple<string, string>(fs.ToFullName(), fs.ToShortName()));
			}
			return string.Join(", ", list.Select(s => $"<abbr title=\"{s.Item1.HtmlAttributeEncode()}\" class=\"initialism\">{s.Item2.HtmlEncode()}</abbr>"));
		}

		public static string GetRegisteredCourseStatusHtml(RegisteredCours.Statuses? statusEnum, FreezedStatuses? freezeStatusEnum)
		{
			return GetRegisteredCourseStatusHtml((byte?)statusEnum, (byte?)freezeStatusEnum);
		}

		public static string GetRegisteredCourseStatusString(byte? status, byte? freezeStatus)
		{
			var list = new List<string>();
			if (status != null)
				list.AddRange(((RegisteredCours.Statuses)status.Value).GetFlags().Select(s => s.ToShortName()));
			if (freezeStatus != null)
				list.Add(((FreezedStatuses)freezeStatus.Value).ToShortName());
			return string.Join(", ", list.Distinct());
		}

		public static string GetRegisteredCourseStatusString(RegisteredCours.Statuses? statusEnum, FreezedStatuses? freezeStatusEnum)
		{
			return GetRegisteredCourseStatusString((byte?)statusEnum, (byte?)freezeStatusEnum);
		}

		public static string GetExamDateAndSessionString(DateTime dateTime, string sessionName)
		{
			return $"{dateTime:d} {sessionName}";
		}

		public static string GetExamDateAndSessionString(DateTime? dateTime, string sessionName)
		{
			return dateTime == null ? null : GetExamDateAndSessionString(dateTime.Value, sessionName);
		}

		public struct ChallanNo
		{
			public byte InstituteCodeByte { get; }
			public string InstituteCode => this.InstituteCodeByte.ToString("D2");
			public byte FeeTypeByte { get; }
			public InstituteBankAccount.FeeTypes FeeTypeEnum => (InstituteBankAccount.FeeTypes)this.FeeTypeByte;
			public bool? NewAdmission { get; }
			public int ChallanID { get; }
			public decimal FullChallanNo => $"{this.InstituteCodeByte:D2}{this.FeeTypeByte:D1}{this.ChallanID:D9}".ToDecimal();

			private const byte MinInstituteCode = 01;
			private const byte MaxInstituteCode = 99;
			private const uint MinChallanID = 0;
			private const uint MaxChallanID = 999999999;
			private const decimal MinFullChallanNo = 010000000000M;
			private const decimal MaxFullChallanNo = 993999999999M;
			public static readonly string FullChallanNoDecimalFormat = 0.ToString("D12");

			public ChallanNo(byte instituteCode, byte feeType, int challanID)
			{
				if (instituteCode < MinInstituteCode || instituteCode > MaxInstituteCode)
					throw new ArgumentException($"{nameof(instituteCode)} is invalid. Must be two digit. {nameof(instituteCode)}: {instituteCode}");
				if (!Enum.IsDefined(typeof(InstituteBankAccount.FeeTypes), feeType))
					throw new ArgumentException($"FeeType is invalid. FeeType: {feeType}");
				if (challanID < MinChallanID || MaxChallanID < challanID)
					throw new ArgumentException($"{nameof(challanID)} is invalid. Invalid Length. {nameof(challanID)}: {challanID}");
				this.InstituteCodeByte = instituteCode;
				this.FeeTypeByte = feeType;
				this.ChallanID = challanID;
				switch ((InstituteBankAccount.FeeTypes)feeType)
				{
					case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
						this.NewAdmission = null;
						break;
					case InstituteBankAccount.FeeTypes.NewAdmissionFee:
						this.NewAdmission = true;
						break;
					case InstituteBankAccount.FeeTypes.StudentFee:
						this.NewAdmission = false;
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(feeType), feeType, null);
				}
			}

			public ChallanNo(string instituteCode, InstituteBankAccount.FeeTypes feeTypeEnum, int challanID)
				: this(instituteCode.ToByte(), (byte)feeTypeEnum, challanID) { }

			public override string ToString()
			{
				return this.FullChallanNo.ToString(FullChallanNoDecimalFormat);
			}

			private const string ChallanNoRegex = @"^(?<InstituteCode>\d{2})(?<FeeChallanType>\d{1})(?<ChallanNo>\d{9})$";

			public static ChallanNo Parse(string fullChallanNoString)
			{
				if (fullChallanNoString.Length == 11)
					fullChallanNoString = $"0{fullChallanNoString}";
				var match = Regex.Match(fullChallanNoString, ChallanNoRegex);
				if (match.Success)
				{
					var instituteCode = match.Groups["InstituteCode"].Value.ToByte();
					var feeType = match.Groups["FeeChallanType"].Value.ToByte();
					var challanNo = match.Groups["ChallanNo"].Value.ToInt();
					return new ChallanNo(instituteCode, feeType, challanNo);
				}
				throw new ArgumentException($"Full Challan No is invalid. Full Challan No: {fullChallanNoString}.");
			}

			public static ChallanNo? TryParse(string fullChallanNoString)
			{
				try
				{
					return Parse(fullChallanNoString);
				}
				catch
				{
					return null;
				}
			}

			public static ChallanNo Parse(decimal fullChallanNo)
			{
				if (MinFullChallanNo <= fullChallanNo && fullChallanNo < MaxFullChallanNo)
					return Parse(fullChallanNo.ToString(FullChallanNoDecimalFormat));
				throw new ArgumentException($"Full Challan No is invalid. Full Challan No: {fullChallanNo}.");
			}

			public static ChallanNo? TryParse(decimal fullChallanNo)
			{
				try
				{
					return Parse(fullChallanNo);
				}
				catch
				{
					return null;
				}
			}

			public static string GetFullChallanNoString(string instituteCode, InstituteBankAccount.FeeTypes feeTypeEnum, int challanID)
			{
				return new ChallanNo(instituteCode, feeTypeEnum, challanID).FullChallanNo.ToString(FullChallanNoDecimalFormat);
			}

			public static decimal GetFullChallanNoDecimal(string instituteCode, InstituteBankAccount.FeeTypes feeTypeEnum, int challanID)
			{
				return new ChallanNo(instituteCode, feeTypeEnum, challanID).FullChallanNo;
			}

			public static string GetFullChallanNoString(string instituteCode, bool newAdmission, int studentFeeChallanID)
			{
				return new ChallanNo(instituteCode, newAdmission ? InstituteBankAccount.FeeTypes.NewAdmissionFee : InstituteBankAccount.FeeTypes.StudentFee, studentFeeChallanID).FullChallanNo.ToString(FullChallanNoDecimalFormat);
			}

			public static decimal GetFullChallanNoDecimal(string instituteCode, bool newAdmission, int studentFeeChallanID)
			{
				return new ChallanNo(instituteCode, newAdmission ? InstituteBankAccount.FeeTypes.NewAdmissionFee : InstituteBankAccount.FeeTypes.StudentFee, studentFeeChallanID).FullChallanNo;
			}
		}

		public static string ToCreditHoursFullName(this decimal creditHours)
		{
			return creditHours.ToString("0.##");
		}

		public static string FormatGPA(this decimal creditHours)
		{
			return creditHours.ToString("0.##");
		}

		public static string FormatGPA(this decimal? creditHours)
		{
			return creditHours?.FormatGPA();
		}

		public static string FormatGradePoints(this decimal gradePoints)
		{
			return gradePoints.ToString("0.##");
		}

		public static string FormatGradePoints(this decimal? gradePoints)
		{
			return gradePoints?.FormatGradePoints();
		}

		public static string FormatProduct(this decimal product)
		{
			return product.ToString("0.##");
		}

		public static string FormatProduct(this decimal? product)
		{
			return product?.FormatProduct();
		}

		public static string GetGradeString(ExamGrades? gradeEnum, RegisteredCours.Statuses? statusEnum)
		{
			switch (statusEnum)
			{
				case null:
				case RegisteredCours.Statuses.AttendanceDefaulter:
				case RegisteredCours.Statuses.UnfairMeans:
				case RegisteredCours.Statuses.Deferred:
					return gradeEnum?.ToFullName();
				case RegisteredCours.Statuses.Incomplete:
					return "I";
				case RegisteredCours.Statuses.WithdrawWithFee:
				case RegisteredCours.Statuses.WithdrawWithoutFee:
					return "W";
				default:
					throw new NotImplementedEnumException(statusEnum);
			}
		}
	}
}