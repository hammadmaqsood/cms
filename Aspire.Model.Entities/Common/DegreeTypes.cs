﻿namespace Aspire.Model.Entities.Common
{
	public enum DegreeTypes : byte
	{
		SSC = 1,
		OLEVEL = 2,
		HSSCI = 3,
		HSSCII = 4,
		HSSC = 5,
		ALEVEL = 6,
		DIPLOMA = 7,
		BACHELORS = 8,
		BACHELORSI = 9,
		BACHELORSII = 10,
		MASTERS = 11,
		MASTERSI = 12,
		MASTERSII = 13,
		MS = 14,
		MPHIL = 15,
		PHD = 16,
		OTHERS = byte.MaxValue,
	}
}
