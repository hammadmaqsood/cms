﻿namespace Aspire.Model.Entities.Common
{
	public enum BloodGroups : byte
	{
		Unknown = 0,
		ABNegative = 1,
		ABPositive = 2,
		ANegative = 3,
		APositive = 4,
		BNegative = 5,
		BPositive = 6,
		ONegative = 7,
		OPositive = 8,
	}
}
