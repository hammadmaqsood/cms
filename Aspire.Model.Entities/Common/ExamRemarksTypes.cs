﻿namespace Aspire.Model.Entities.Common
{
	public enum ExamRemarksTypes : byte
	{
		None = 0,
		Chance = 1,
		Drop = 2,
		Probation = 3,
		Probation1 = 4,
		Probation2 = 5,
		Probation3 = 6,
		Qualified = 7,
		Relegated = 8,
	}
}