﻿using System;

namespace Aspire.Model.Entities.Common
{
	[Flags]
	public enum Shifts : byte
	{
		Morning = 1,
		Evening = 2,
		Weekend = 4,
	}
}
