﻿namespace Aspire.Model.Entities.Common
{
	public sealed class ExecutivePermissions
	{
		public sealed class Admissions : ExecutivePermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Admissions;

			private Admissions(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public new static readonly Admissions Module = new Admissions(UserGroupPermission.PermissionTypes.AdmissionsModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Admissions[] PermissionsList =
			{
				Module,
			};
		}

		public sealed class ClassAttendance : ExecutivePermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.ClassAttendance;

			private ClassAttendance(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public new static readonly ClassAttendance Module = new ClassAttendance(UserGroupPermission.PermissionTypes.ClassAttendanceModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ClassAttendance[] PermissionsList =
			{
				Module
			};
		}

		public sealed class CourseRegistration : ExecutivePermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.CourseRegistration;

			private CourseRegistration(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public new static readonly CourseRegistration Module = new CourseRegistration(UserGroupPermission.PermissionTypes.CourseRegistrationModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration[] PermissionsList =
			{
				Module,
			};
		}

		public sealed class Courses : ExecutivePermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Courses;

			private Courses(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public new static readonly Courses Module = new Courses(UserGroupPermission.PermissionTypes.CoursesModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Courses[] PermissionsList =
			{
				Module,
			};
		}

		public sealed class CourseOffering : ExecutivePermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.CourseOffering;

			private CourseOffering(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public new static readonly CourseOffering Module = new CourseOffering(UserGroupPermission.PermissionTypes.CourseOfferingModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);

			public static readonly CourseOffering[] PermissionsList =
			{
				Module,
			};
		}

		public sealed class Exams : ExecutivePermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Exams;

			private Exams(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public new static readonly Exams Module = new Exams(UserGroupPermission.PermissionTypes.ExamsModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams[] PermissionsList =
			{
				Module,
			};
		}

		public sealed class FeeManagement : ExecutivePermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.FeeManagement;

			private FeeManagement(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public new static readonly FeeManagement Module = new FeeManagement(UserGroupPermission.PermissionTypes.FeeManagementModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly FeeManagement[] PermissionsList =
			{
				Module,
			};
		}

		public sealed class StudentInformation : ExecutivePermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.StudentInformation;

			private StudentInformation(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public new static readonly StudentInformation Module = new StudentInformation(UserGroupPermission.PermissionTypes.StudentInformationModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly StudentInformation[] PermissionsList =
			{
				Module,
			};
		}

		public sealed class QualityAssurance : ExecutivePermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.QualityAssurance;

			private QualityAssurance(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public new static readonly QualityAssurance Module = new QualityAssurance(UserGroupPermission.PermissionTypes.QualityAssuranceModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance[] PermissionsList =
			{
				Module,
			};
		}

		public sealed class LMS : ExecutivePermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.LMS;

			private LMS(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public new static readonly LMS Module = new LMS(UserGroupPermission.PermissionTypes.LMSModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly LMS[] PermissionsList =
			{
				Module,
			};
		}

		public sealed class Feedbacks : ExecutivePermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Feedbacks;

			private Feedbacks(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public new static readonly Feedbacks Module = new Feedbacks(UserGroupPermission.PermissionTypes.FeedbacksModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Feedbacks CanViewRectorBox = new Feedbacks(UserGroupPermission.PermissionTypes.CanViewRectorBox, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Feedbacks CanViewDirectorGeneralBox = new Feedbacks(UserGroupPermission.PermissionTypes.CanViewDirectorGeneralBox, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Feedbacks CanRespondAsRector = new Feedbacks(UserGroupPermission.PermissionTypes.CanRespondAsRector, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Feedbacks CanRespondAsDirectorGeneral = new Feedbacks(UserGroupPermission.PermissionTypes.CanRespondAsDirectorGeneral, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Feedbacks[] PermissionsList =
			{
				Module,
				CanViewRectorBox,
				CanViewDirectorGeneralBox,
				CanRespondAsRector,
				CanRespondAsDirectorGeneral
			};
		}
	}
}