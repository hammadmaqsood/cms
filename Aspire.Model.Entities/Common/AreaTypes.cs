﻿namespace Aspire.Model.Entities.Common
{
	public enum AreaTypes : byte
	{
		Urban = 1,
		Rural = 2,
	}
}
