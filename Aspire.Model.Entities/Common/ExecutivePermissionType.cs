namespace Aspire.Model.Entities.Common
{
	public abstract class ExecutivePermissionType : AspirePermissionType
	{
		public sealed override UserTypes UserTypeEnum => UserTypes.Executive;

		protected ExecutivePermissionType(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
		{
		}
	}
}