﻿using System;

namespace Aspire.Model.Entities.Common
{
	/// <summary>
	/// Assignments, Quizzes, Internals, Mid and Final
	/// </summary>
	[Flags]
	public enum ExamMarksTypes : byte
	{
		Assignments = 1,
		Quizzes = 2,
		Mid = 4,
		Final = 8,
		Internals = 16,
	}
}
