namespace Aspire.Model.Entities.Common
{
	public static class UserTypeConstants
	{
		public static readonly short Anonymous = (short)UserTypes.Anonymous;
		public static readonly short System = (short)UserTypes.System;
		public static readonly short MasterAdmin = (short)UserTypes.MasterAdmin;
		public static readonly short Admin = (short)UserTypes.Admin;
		public static readonly short Executive = (short)UserTypes.Executive;
		public static readonly short Staff = (short)UserTypes.Staff;
		public static readonly short Candidate = (short)UserTypes.Candidate;
		public static readonly short Student = (short)UserTypes.Student;
		public static readonly short Faculty = (short)UserTypes.Faculty;
		public static readonly short Alumni = (short)UserTypes.Alumni;
		public static readonly short IntegratedService = (short)UserTypes.IntegratedService;
		public static readonly short ManualSQL = (short)UserTypes.ManualSQL;
	}
}