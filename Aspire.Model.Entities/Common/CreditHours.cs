﻿using System.Collections.Generic;

namespace Aspire.Model.Entities.Common
{
	public static class CreditHours
	{
		public static readonly decimal Zero = 0M;
		public static readonly decimal One = 1M;
		public static readonly decimal OnePointFive = 1.5M;
		public static readonly decimal Two = 2M;
		public static readonly decimal Three = 3M;
		public static readonly decimal Four = 4M;
		public static readonly decimal Five = 5M;
		public static readonly decimal Six = 6M;
		public static readonly decimal Seven = 7M;
		public static readonly decimal Eight = 8M;
		public static readonly decimal Nine = 9M;
		public static readonly decimal Ten = 10M;
		public static readonly decimal Eleven = 11M;
		public static readonly decimal Twelve = 12M;
		public static readonly decimal ThirtySix = 36M;

		public static IEnumerable<decimal> AllCreditHours
		{
			get
			{
				yield return Zero;
				yield return One;
				yield return OnePointFive;
				yield return Two;
				yield return Three;
				yield return Four;
				yield return Five;
				yield return Six;
				yield return Seven;
				yield return Eight;
				yield return Nine;
				yield return Ten;
				yield return Eleven;
				yield return Twelve;
				yield return ThirtySix;
			}
		}
	}
}