﻿namespace Aspire.Model.Entities.Common
{
	public enum ExamGrades : byte
	{
		A = 1,
		BPlus = 2,
		B = 3,
		CPlus = 4,
		C = 5,
		D = 6,
		F = 7,
		AMinus = 8,
		BMinus = 9,
		CMinus = 10,
		DPlus = 11,
	}
}
