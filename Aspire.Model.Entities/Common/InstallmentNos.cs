﻿namespace Aspire.Model.Entities.Common
{
	public enum InstallmentNos : byte
	{
		One = 1,
		Two = 2,
		Three = 3,
		Four = 4,
		Five = 5
	}
}
