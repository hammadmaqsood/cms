﻿using System;
using System.Linq;

namespace Aspire.Model.Entities.Common
{
	public sealed class AspireNotAuthorizedException : Exception
	{
		private UserTypes? UserTypeEnum { get; }
		private AspirePermissionType AspirePermissionType { get; }
		private UserGroupPermission.PermissionValues[] PermissionValuesDemanded { get; }
		private UserGroupPermission.PermissionValues? PermissionValueFound { get; }
		private int? DemandedInstituteID { get; }
		private int? DemandedDepartmentID { get; }
		private int? DemandedProgramID { get; }
		private int? DemandedAdmissionOpenProgramID { get; }

		public AspireNotAuthorizedException()
		{
		}

		public AspireNotAuthorizedException(UserTypes userTypeEnum)
		{
			this.UserTypeEnum = userTypeEnum;
		}

		public AspireNotAuthorizedException(UserTypes userTypeEnum, AspirePermissionType aspirePermissionType, UserGroupPermission.PermissionValues[] permissionValuesDemanded, UserGroupPermission.PermissionValues? permissionValueFound)
		{
			this.UserTypeEnum = userTypeEnum;
			this.AspirePermissionType = aspirePermissionType;
			this.PermissionValuesDemanded = permissionValuesDemanded;
			this.PermissionValueFound = permissionValueFound;
		}

		public AspireNotAuthorizedException(UserTypes userTypeEnum, AspirePermissionType aspirePermissionType, UserGroupPermission.PermissionValues[] permissionValuesDemanded, UserGroupPermission.PermissionValues? permissionValueFound, int? demandedInstituteID, int? demandedDepartmentID, int? demandedProgramID, int? demandedAdmissionOpenProgramID)
		{
			this.UserTypeEnum = userTypeEnum;
			this.AspirePermissionType = aspirePermissionType;
			this.PermissionValuesDemanded = permissionValuesDemanded;
			this.PermissionValueFound = permissionValueFound;
			this.DemandedInstituteID = demandedInstituteID;
			this.DemandedDepartmentID = demandedDepartmentID;
			this.DemandedProgramID = demandedProgramID;
			this.DemandedAdmissionOpenProgramID = demandedAdmissionOpenProgramID;
		}

		public override string Message
		{
			get
			{
				var message = string.Empty;
				if (this.UserTypeEnum != null)
					message += $"User Type: {this.UserTypeEnum.Value.ToFullName()}. ";
				message += $"Module: {this.AspirePermissionType?.ModuleEnum.ToFullName()}. ";
				message += $"Permissions: {this.AspirePermissionType?.PermissionTypeFullName}. ";
				if (this.PermissionValuesDemanded != null && this.PermissionValuesDemanded.Any())
					message += $"Demanded: {string.Join(",", this.PermissionValuesDemanded.Select(v => v.ToFullName()))}. ";
				if (this.PermissionValueFound != null)
					message += $"Found: {this.PermissionValueFound.Value.ToFullName()}. ";
				if (this.DemandedInstituteID != null)
					message += $"Demanded InstituteID: {this.DemandedInstituteID}. ";
				if (this.DemandedDepartmentID != null)
					message += $"Demanded DepartmentID: {this.DemandedDepartmentID}. ";
				if (this.DemandedProgramID != null)
					message += $"Demanded ProgramID: {this.DemandedProgramID}. ";
				if (this.DemandedAdmissionOpenProgramID != null)
					message += $"Demanded AdmissionOpenProgramID: {this.DemandedAdmissionOpenProgramID}. ";
				return message.Trim();
			}
		}
	}
}
