﻿using Aspire.Lib.Extensions;
using Aspire.Lib.Extensions.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Aspire.Model.Entities.Common
{
	public static class Extensions
	{
		private static IEnumerable<T> GetFlags<T>(this T @enum) where T : struct, IConvertible
		{
			var type = typeof(T);
			if (!type.IsEnum)
				throw new ArgumentException("@enum");
			var _enum = (Enum)Convert.ChangeType(@enum, type);
			return Enum.GetValues(type).Cast<Enum>().Where(value => _enum.HasFlag(value)).Select(value => (T)Convert.ChangeType(value, type));
		}

		public static string ToFullName(this OfferedCours.ExamCompilationTypes examCompilationTypes)
		{
			switch (examCompilationTypes)
			{
				case OfferedCours.ExamCompilationTypes.OverallAverage:
					return "Overall Average";
				default:
					throw new ArgumentOutOfRangeException(nameof(examCompilationTypes), examCompilationTypes, null);
			}
		}

		public static string ToFullName(this GraduateDirectoryFormProject.ProjectTypes projectType)
		{
			switch (projectType)
			{
				case GraduateDirectoryFormProject.ProjectTypes.Interim:
					return "Interim";
				case GraduateDirectoryFormProject.ProjectTypes.FinalSemester:
					return "Final";
				default:
					throw new ArgumentOutOfRangeException(nameof(projectType), projectType, null);
			}
		}

		public static string ToFullName(this SemesterTypes semesterTypes)
		{
			switch (semesterTypes)
			{
				case SemesterTypes.Fall:
					return "Fall";
				case SemesterTypes.Spring:
					return "Spring";
				case SemesterTypes.Summer:
					return "Summer";
				default:
					throw new ArgumentOutOfRangeException(nameof(semesterTypes), semesterTypes, null);
			}
		}

		public static string ToFullName(this AreaTypes areaType)
		{
			switch (areaType)
			{
				case AreaTypes.Urban:
					return "Urban";
				case AreaTypes.Rural:
					return "Rural";
				default:
					throw new ArgumentOutOfRangeException(nameof(areaType), areaType, null);
			}
		}

		public static string ToFullName(this Candidate.SourceOfInformations sourceOfInformation)
		{
			switch (sourceOfInformation)
			{
				case Candidate.SourceOfInformations.Friends:
					return "Friends/Word of Mouth";
				case Candidate.SourceOfInformations.Newspaper:
					return "Newspaper";
				case Candidate.SourceOfInformations.Recommendation:
					return "Recommendation";
				case Candidate.SourceOfInformations.Signboard:
					return "Signboard";
				case Candidate.SourceOfInformations.SocialMedia:
					return "Social Media";
				case Candidate.SourceOfInformations.Website:
					return "Website";
				case Candidate.SourceOfInformations.BUVisitingTeamAtOurCollege:
					return "BU Visiting Team at our College";
				case Candidate.SourceOfInformations.ExpoExhibitionSeminar:
					return "Expo/Exhibition/Seminar";
				default:
					throw new NotImplementedEnumException(sourceOfInformation);
			}
		}

		public static string ToFullName(this InstituteBankAccount.AccountTypes accountType)
		{
			switch (accountType)
			{
				case InstituteBankAccount.AccountTypes.Manual:
					return "Manual";
				case InstituteBankAccount.AccountTypes.Online:
					return "Online";
				case InstituteBankAccount.AccountTypes.ManualOnline:
					return "Manual+Online";
				default:
					throw new ArgumentOutOfRangeException(nameof(accountType), accountType, null);
			}
		}

		public static string ToFullName(this InstituteBankAccount.FeeTypes feeType)
		{
			switch (feeType)
			{
				case InstituteBankAccount.FeeTypes.AdmissionProcessingFee:
					return "Admission Processing Fee";
				case InstituteBankAccount.FeeTypes.NewAdmissionFee:
					return "New Admission Fee";
				case InstituteBankAccount.FeeTypes.StudentFee:
					return "Student Fee";
				default:
					throw new ArgumentOutOfRangeException(nameof(feeType), feeType, null);
			}
		}

		public static string ToFullName(this MeritListReportTypes meritListReportType)
		{
			switch (meritListReportType)
			{
				case MeritListReportTypes.UnderGraduate:
					return "Under Graduate";
				case MeritListReportTypes.Graduate:
					return "Graduate";
				default:
					throw new ArgumentOutOfRangeException(nameof(meritListReportType), meritListReportType, null);
			}
		}

		public static string ToFullName(this SurveyConduct.SurveyConductTypes surveyConductType)
		{
			switch (surveyConductType)
			{
				case SurveyConduct.SurveyConductTypes.BeforeMidTerm:
					return "Before Mid Term";
				//case SurveyConduct.SurveyConductTypes.AfterMidTerm:
				//	return "After Mid Term";
				case SurveyConduct.SurveyConductTypes.BeforeFinalTerm:
					return "Before Final Term";
				//case SurveyConduct.SurveyConductTypes.AfterFinalTerm:
				//	return "After Final Term";
				default:
					throw new ArgumentOutOfRangeException(nameof(surveyConductType), surveyConductType, null);
			}
		}

		public static string ToFullName(this BloodGroups bloodGroup)
		{
			switch (bloodGroup)
			{
				case BloodGroups.Unknown:
					return "Unknown";
				case BloodGroups.APositive:
					return "A+";
				case BloodGroups.ANegative:
					return "A-";
				case BloodGroups.BPositive:
					return "B+";
				case BloodGroups.BNegative:
					return "B-";
				case BloodGroups.ABPositive:
					return "AB+";
				case BloodGroups.ABNegative:
					return "AB-";
				case BloodGroups.OPositive:
					return "O+";
				case BloodGroups.ONegative:
					return "O-";

				default:
					throw new ArgumentOutOfRangeException(nameof(bloodGroup), bloodGroup, null);
			}
		}

		public static string ToFullName(this CourseCategories courseCategory)
		{
			switch (courseCategory)
			{
				case CourseCategories.Course:
					return "Course";
				case CourseCategories.Internship:
					return "Internship";
				case CourseCategories.Lab:
					return "Lab";
				case CourseCategories.Project:
					return "Project";
				case CourseCategories.Thesis:
					return "Thesis";
				default:
					throw new ArgumentOutOfRangeException(nameof(courseCategory), courseCategory, null);
			}
		}

		public static string ToFullName(this CourseTypes courseType)
		{
			switch (courseType)
			{
				case CourseTypes.Core:
					return "Core";
				case CourseTypes.Elective:
					return "Elective";
				default:
					throw new ArgumentOutOfRangeException(nameof(courseType), courseType, null);
			}
		}

		public static string ToFullName(this StudentSemester.Statuses status)
		{
			switch (status)
			{
				case StudentSemester.Statuses.NotRegistered:
					return "Not Registered";
				case StudentSemester.Statuses.Registered:
					return "Registered";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToFullName(this SemesterNos semesterNo)
		{
			switch (semesterNo)
			{
				case SemesterNos.Zero:
					return "0";
				case SemesterNos.One:
					return "1";
				case SemesterNos.Two:
					return "2";
				case SemesterNos.Three:
					return "3";
				case SemesterNos.Four:
					return "4";
				case SemesterNos.Five:
					return "5";
				case SemesterNos.Six:
					return "6";
				case SemesterNos.Seven:
					return "7";
				case SemesterNos.Eight:
					return "8";
				case SemesterNos.Nine:
					return "9";
				case SemesterNos.Ten:
					return "10";
				default:
					throw new ArgumentOutOfRangeException(nameof(semesterNo), semesterNo, null);
			}
		}

		public static string ToFullNames(this SemesterNos semesterNos, string separator = ", ")
		{
			return string.Join(separator, semesterNos.GetFlags().Select(s => s.ToFullName()));
		}

		public static string ToFullNames(this Categories categories, string separator = ", ")
		{
			return string.Join(separator, categories.GetFlags().Select(s => s.ToFullName()));
		}

		public static string ToFullName(this DegreeTypes degreeType)
		{
			switch (degreeType)
			{
				case DegreeTypes.SSC:
					return "SSC";
				case DegreeTypes.OLEVEL:
					return "O-Level";
				case DegreeTypes.HSSCI:
					return "HSSC-I";
				case DegreeTypes.HSSCII:
					return "HSSC-II";
				case DegreeTypes.HSSC:
					return "HSSC";
				case DegreeTypes.ALEVEL:
					return "A-Level";
				case DegreeTypes.DIPLOMA:
					return "Diploma";
				case DegreeTypes.BACHELORS:
					return "Bachelors";
				case DegreeTypes.MASTERS:
					return "Masters";
				case DegreeTypes.MS:
					return "MS";
				case DegreeTypes.PHD:
					return "PHD";
				case DegreeTypes.BACHELORSI:
					return "Bachelors-I";
				case DegreeTypes.BACHELORSII:
					return "Bachelors-II";
				case DegreeTypes.MASTERSI:
					return "Masters-I";
				case DegreeTypes.MASTERSII:
					return "Masters-II";
				case DegreeTypes.MPHIL:
					return "M.Phil";
				case DegreeTypes.OTHERS:
					return "Others";
				default:
					throw new ArgumentOutOfRangeException(nameof(degreeType), degreeType, null);
			}
		}

		public static string ToFullName(this ETSTypes etsType)
		{
			switch (etsType)
			{
				case ETSTypes.GAT:
					return "GAT";
				case ETSTypes.GMAT:
					return "GMAT";
				case ETSTypes.GRE:
					return "GRE";
				case ETSTypes.SAT_I:
					return "SAT-I";
				case ETSTypes.SAT_II:
					return "SAT-II";
				case ETSTypes.NTS:
					return "NTS";
				case ETSTypes.LAT:
					return "LAT";
				default:
					throw new ArgumentOutOfRangeException(nameof(etsType), etsType, null);
			}
		}

		public static bool IsNaval(this Categories category)
		{
			switch (category)
			{
				case Categories.NavalRetired:
					return true;
				case Categories.NavalServing:
					return true;
				case Categories.Others:
					return false;
				case Categories.Naval:
					return true;
				case 0:
					return false;
				default:
					throw new ArgumentOutOfRangeException(nameof(category), category, null);
			}
		}

		public static string ToFullName(this Categories category)
		{
			switch (category)
			{
				case Categories.NavalServing:
					return "Naval Serving";
				case Categories.NavalRetired:
					return "Naval Retired";
				case Categories.Naval:
					return "Naval";
				case Categories.Others:
					return "Others";
				default:
					throw new ArgumentOutOfRangeException(nameof(category), category, null);
			}
		}

		public static string ToFullName(this Genders gender)
		{
			switch (gender)
			{
				case Genders.Male:
					return "Male";
				case Genders.Female:
					return "Female";
				default:
					throw new ArgumentOutOfRangeException(nameof(gender), gender, null);
			}
		}

		public static string ToFullName(this FacultyMember.FacultyTypes facultyTypeEnum)
		{
			switch (facultyTypeEnum)
			{
				case FacultyMember.FacultyTypes.Permanent:
					return "Permanent";
				case FacultyMember.FacultyTypes.Visiting:
					return "Visiting";
				default:
					throw new NotImplementedEnumException(facultyTypeEnum);
			}
		}

		public static string ToFullName(this FacultyMember.Titles titleEnum)
		{
			switch (titleEnum)
			{
				case FacultyMember.Titles.Mr:
					return "Mr.";
				case FacultyMember.Titles.Miss:
					return "Miss.";
				case FacultyMember.Titles.Mrs:
					return "Mrs.";
				case FacultyMember.Titles.Dr:
					return "Dr.";
				case FacultyMember.Titles.Ms:
					return "Ms.";
				case FacultyMember.Titles.Prof:
					return "Prof.";
				default:
					throw new NotImplementedEnumException(titleEnum);
			}
		}

		public static string ToFullName(this FacultyMember.Statuses statusEnum)
		{
			switch (statusEnum)
			{
				case FacultyMember.Statuses.Active:
					return "Active";
				case FacultyMember.Statuses.Inactive:
					return "Inactive";
				default:
					throw new NotImplementedEnumException(statusEnum);
			}
		}

		public static string ToFullName(this Sponsorships sponsorship)
		{
			switch (sponsorship)
			{
				case Sponsorships.Father:
					return "Father";
				case Sponsorships.Self:
					return "Self";
				case Sponsorships.Guardian:
					return "Guardian";
				case Sponsorships.Organization:
					return "Organization";
				default:
					throw new ArgumentOutOfRangeException(nameof(sponsorship), sponsorship, null);
			}
		}

		public static string ToFullName(this DegreeLevels degreeLevel)
		{
			switch (degreeLevel)
			{
				case DegreeLevels.Diploma:
					return "Diploma";
				case DegreeLevels.Undergraduate:
					return "Undergraduate";
				case DegreeLevels.Masters:
					return "Masters";
				case DegreeLevels.Postgraduate:
					return "Postgraduate";
				case DegreeLevels.Doctorate:
					return "Doctorate";
				default:
					throw new ArgumentOutOfRangeException(nameof(degreeLevel), degreeLevel, null);
			}
		}

		public static string ToFullName(this ProgramTypes programType)
		{
			switch (programType)
			{
				case ProgramTypes.Degree:
					return "Degree";
				case ProgramTypes.ShortCourse:
					return "Short Course";
				default:
					throw new ArgumentOutOfRangeException(nameof(programType), programType, null);
			}
		}

		public static string ToFullName(this ProgramDurations programDuration)
		{
			switch (programDuration)
			{
				case ProgramDurations.OneYear:
					return "1 Year";
				case ProgramDurations.OnePointFiveYears:
					return "1.5 Years";
				case ProgramDurations.TwoYears:
					return "2 Years";
				case ProgramDurations.TwoPointFiveYears:
					return "2.5 Years";
				case ProgramDurations.ThreeYears:
					return "3 Years";
				case ProgramDurations.ThreePointFiveYears:
					return "3.5 Years";
				case ProgramDurations.FourYears:
					return "4 Years";
				case ProgramDurations.FiveYears:
					return "5 Years";
				case ProgramDurations.FourMonths:
					return "4 Months";
				case ProgramDurations.SixMonths:
					return "6 Months";
				default:
					throw new ArgumentOutOfRangeException(nameof(programDuration), programDuration, null);
			}
		}

		public static string ToShortName(this ProgramDurations programDuration)
		{
			switch (programDuration)
			{
				case ProgramDurations.OneYear:
					return "1Y";
				case ProgramDurations.OnePointFiveYears:
					return "1.5Y";
				case ProgramDurations.TwoYears:
					return "2Y";
				case ProgramDurations.TwoPointFiveYears:
					return "2.5Y";
				case ProgramDurations.ThreeYears:
					return "3Y";
				case ProgramDurations.ThreePointFiveYears:
					return "3.5Y";
				case ProgramDurations.FourYears:
					return "4Y";
				case ProgramDurations.FiveYears:
					return "5Y";
				case ProgramDurations.FourMonths:
					return "4M";
				case ProgramDurations.SixMonths:
					return "6M";
				default:
					throw new ArgumentOutOfRangeException(nameof(programDuration), programDuration, null);
			}
		}

		public static string ToFullName(this Shifts shifts)
		{
			switch (shifts)
			{
				case Shifts.Morning:
					return "Morning";
				case Shifts.Evening:
					return "Evening";
				case Shifts.Weekend:
					return "Weekend";
				default:
					throw new ArgumentOutOfRangeException(nameof(shifts), shifts, null);
			}
		}

		public static string ToFullName(this ExamSystemTypes examSystem)
		{
			switch (examSystem)
			{
				case ExamSystemTypes.SemesterSystem:
					return "Semester";
				default:
					throw new ArgumentOutOfRangeException(nameof(examSystem), examSystem, null);
			}
		}

		public static string ToFullName(this ExamGrades examGrade)
		{
			switch (examGrade)
			{
				case ExamGrades.A:
					return "A";
				case ExamGrades.BPlus:
					return "B+";
				case ExamGrades.B:
					return "B";
				case ExamGrades.CPlus:
					return "C+";
				case ExamGrades.C:
					return "C";
				case ExamGrades.D:
					return "D";
				case ExamGrades.F:
					return "F";
				case ExamGrades.AMinus:
					return "A-";
				case ExamGrades.BMinus:
					return "B-";
				case ExamGrades.CMinus:
					return "C-";
				case ExamGrades.DPlus:
					return "D+";
				default:
					throw new ArgumentOutOfRangeException(nameof(examGrade), examGrade, null);
			}
		}

		public static string ToFullName(this ExamGrades? examGrade)
		{
			return examGrade?.ToFullName();
		}

		public static string ToFullNames(this Shifts shifts, string separator = ", ")
		{
			return string.Join(separator, shifts.GetFlags().Select(s => s.ToFullName()));
		}

		public static string ToFullNames(this CourseCategories courseCategories, string separator = ", ")
		{
			return string.Join(separator, courseCategories.GetFlags().Select(s => s.ToFullName()));
		}

		public static string ToFullName(this OfferedCourseExam.ExamTypes examType)
		{
			switch (examType)
			{
				case OfferedCourseExam.ExamTypes.Assignment:
					return "Assignment";
				case OfferedCourseExam.ExamTypes.Quiz:
					return "Quiz";
				default:
					throw new ArgumentOutOfRangeException(nameof(examType), examType, null);
			}
		}

		public static string ToFullName(this ClassAttendanceSetting.Statuses status)
		{
			switch (status)
			{
				case ClassAttendanceSetting.Statuses.Active:
					return "Active";
				case ClassAttendanceSetting.Statuses.Inactive:
					return "Inactive";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToFullName(this ExamMarksSetting.Statuses status)
		{
			switch (status)
			{
				case ExamMarksSetting.Statuses.Active:
					return "Active";
				case ExamMarksSetting.Statuses.Inactive:
					return "Inactive";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToFullName(this CourseRegistrationSetting.Statuses status)
		{
			switch (status)
			{
				case CourseRegistrationSetting.Statuses.Active:
					return "Active";
				case CourseRegistrationSetting.Statuses.Inactive:
					return "Inactive";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToFullName(this ExamMarksTypes examMarksType)
		{
			switch (examMarksType)
			{
				case ExamMarksTypes.Assignments:
					return "Assignments";
				case ExamMarksTypes.Quizzes:
					return "Quizzes";
				case ExamMarksTypes.Mid:
					return "Mid";
				case ExamMarksTypes.Final:
					return "Final";
				case ExamMarksTypes.Internals:
					return "Internals";
				default:
					throw new ArgumentOutOfRangeException(nameof(examMarksType), examMarksType, null);
			}
		}

		public static UserTypes? Combine(this IEnumerable<UserTypes> userTypes)
		{
			UserTypes? result = null;
			foreach (var userType in userTypes)
			{
				if (result == null)
					result = userType;
				else
					result = result.Value | userType;
			}
			return result;
		}

		public static Sections? Combine(this IEnumerable<Sections> sections)
		{
			Sections? result = null;
			foreach (var section in sections)
			{
				if (result == null)
					result = section;
				else
					result = result.Value | section;
			}
			return result;
		}

		public static Shifts? Combine(this IEnumerable<Shifts> shifts)
		{
			Shifts? result = null;
			foreach (var shift in shifts)
			{
				if (result == null)
					result = shift;
				else
					result = result.Value | shift;
			}
			return result;
		}

		public static SemesterNos? Combine(this IEnumerable<SemesterNos> semesterNos)
		{
			SemesterNos? result = null;
			foreach (var semesterNo in semesterNos)
			{
				if (result == null)
					result = semesterNo;
				else
					result = result.Value | semesterNo;
			}
			return result;
		}

		public static DisciplineCasesStudent.RemarksTypes? Combine(this IEnumerable<DisciplineCasesStudent.RemarksTypes> remarksTypes)
		{
			DisciplineCasesStudent.RemarksTypes? result = null;
			foreach (var remarksType in remarksTypes)
			{
				if (result == null)
					result = remarksType;
				else
					result = result.Value | remarksType;
			}
			return result;
		}

		public static Categories? Combine(this IEnumerable<Categories> categories)
		{
			Categories? result = null;
			foreach (var category in categories)
			{
				if (result == null)
					result = category;
				else
					result = result.Value | category;
			}
			return result;
		}

		public static CourseCategories? Combine(this IEnumerable<CourseCategories> courseCategories)
		{
			CourseCategories? result = null;
			foreach (var courseCategory in courseCategories)
			{
				if (result == null)
					result = courseCategory;
				else
					result = result.Value | courseCategory;
			}
			return result;
		}

		public static ExamMarksTypes? Combine(this IEnumerable<ExamMarksTypes> examMarksTypesEnums)
		{
			ExamMarksTypes? result = null;
			foreach (var examMarksTypesEnum in examMarksTypesEnums)
			{
				if (result == null)
					result = examMarksTypesEnum;
				else
					result = result.Value | examMarksTypesEnum;
			}
			return result;
		}

		public static string ToFullName(this Sections section)
		{
			switch (section)
			{
				case Sections.A:
					return "A";
				case Sections.B:
					return "B";
				case Sections.C:
					return "C";
				case Sections.D:
					return "D";
				case Sections.E:
					return "E";
				case Sections.F:
					return "F";
				case Sections.G:
					return "G";
				case Sections.H:
					return "H";
				case Sections.I:
					return "I";
				case Sections.J:
					return "J";
				case Sections.K:
					return "K";
				case Sections.L:
					return "L";
				case Sections.M:
					return "M";
				case Sections.N:
					return "N";
				case Sections.O:
					return "O";
				case Sections.P:
					return "P";
				case Sections.Q:
					return "Q";
				case Sections.R:
					return "R";
				case Sections.S:
					return "S";
				case Sections.T:
					return "T";
				case Sections.U:
					return "U";
				case Sections.V:
					return "V";
				case Sections.W:
					return "W";
				case Sections.X:
					return "X";
				case Sections.Y:
					return "Y";
				case Sections.Z:
					return "Z";
				default:
					throw new ArgumentOutOfRangeException(nameof(section), section, null);
			}
		}

		public static string ToFullNames(this Sections section, string separator = ", ")
		{
			return string.Join(separator, section.GetFlags().Select(s => s.ToFullName()));
		}

		public static string ToFullName(this OfferedCours.Statuses status)
		{
			switch (status)
			{
				case OfferedCours.Statuses.All:
					return "All";
				case OfferedCours.Statuses.Students:
					return "Students";
				case OfferedCours.Statuses.Coordinators:
					return "Coordinators";
				case OfferedCours.Statuses.Restricted:
					return "Restricted";
				case OfferedCours.Statuses.Blocked:
					return "Blocked";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToFullName(this Student.Statuses status)
		{
			switch (status)
			{
				case Student.Statuses.AdmissionCancelled:
					return "Admission Canceled";
				case Student.Statuses.Blocked:
					return "Blocked";
				case Student.Statuses.Deferred:
					return "Deferred";
				case Student.Statuses.Dropped:
					return "Dropped";
				case Student.Statuses.Expelled:
					return "Expelled";
				case Student.Statuses.Graduated:
					return "Graduated";
				case Student.Statuses.Left:
					return "Left";
				case Student.Statuses.ProgramChanged:
					return "Program Changed";
				case Student.Statuses.Rusticated:
					return "Rusticated";
				case Student.Statuses.TransferredToOtherCampus:
					return "Transferred To Other Campus";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToFullName(this StudentFeeChallan.Statuses status)
		{
			switch (status)
			{
				case StudentFeeChallan.Statuses.NotPaid:
					return "Not Paid";
				case StudentFeeChallan.Statuses.Paid:
					return "Paid";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToFullName(this UserRole.Statuses status)
		{
			switch (status)
			{
				case UserRole.Statuses.Active:
					return "Active";
				case UserRole.Statuses.Inactive:
					return "Inactive";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToFullName(this UserTypes userType)
		{
			switch (userType)
			{
				case UserTypes.Anonymous:
					return "Anonymous";
				case UserTypes.System:
					return "System";
				case UserTypes.MasterAdmin:
					return "Master Administrator";
				case UserTypes.Admin:
					return "Administrator";
				case UserTypes.Executive:
					return "Executive";
				case UserTypes.Staff:
					return "Staff";
				case UserTypes.Candidate:
					return "Candidate";
				case UserTypes.Student:
					return "Student";
				case UserTypes.Faculty:
					return "Faculty";
				case UserTypes.Alumni:
					return "Alumni";
				case UserTypes.IntegratedService:
					return "Integrated Service";
				case UserTypes.ManualSQL:
					return "Manual SQL";
				case UserTypes.Admins:
					return "Administrators";
				case UserTypes.Any:
					return "Any";
				default:
					throw new ArgumentOutOfRangeException(nameof(userType), userType, null);
			}
		}

		public static string ToFullName(this SubUserTypes subUserType)
		{
			switch (subUserType)
			{
				case SubUserTypes.None:
					return "N/A";
				case SubUserTypes.Parents:
					return "Parents";
				default:
					throw new ArgumentOutOfRangeException(nameof(subUserType), subUserType, null);
			}
		}

		public static string ToFullNames(this UserTypes userType, string separator = ", ")
		{
			return string.Join(separator, userType.GetFlags().Select(t => t.ToFullName()));
		}

		public static string ToFullName(this InstallmentNos installmentNo)
		{
			switch (installmentNo)
			{
				case InstallmentNos.One:
					return "1";
				case InstallmentNos.Two:
					return "2";
				case InstallmentNos.Three:
					return "3";
				case InstallmentNos.Four:
					return "4";
				case InstallmentNos.Five:
					return "5";
				default:
					throw new ArgumentOutOfRangeException(nameof(installmentNo), installmentNo, null);
			}
		}

		public static string ToFullName(this PasswordChangeReasons passwordChangeReason)
		{
			switch (passwordChangeReason)
			{
				case PasswordChangeReasons.UserForgotHisPassword:
					return "Changed By Admin (User Forgot Password)";
				case PasswordChangeReasons.ManuallySettingForNewUser:
					return "Changed By Admin (Manually setting for new user)";
				case PasswordChangeReasons.PasswordPotentiallyCompromised:
					return "Changed By Admin (Password potentially compromised)";
				case PasswordChangeReasons.Troubleshooting:
					return "Changed By Admin (Troubleshooting)";
				case PasswordChangeReasons.ByUserItSelf:
					return "Password Changed";
				case PasswordChangeReasons.SetDuringRegistration:
					return "Set During Registration";
				case PasswordChangeReasons.UsingResetLink:
					return "Using Reset Link";
				default:
					throw new ArgumentOutOfRangeException(nameof(passwordChangeReason), passwordChangeReason, null);
			}
		}

		public static string ToFullName(this FreezedStatuses freezeStatus)
		{
			switch (freezeStatus)
			{
				case FreezedStatuses.FreezedWithFee:
					return "Freeze With Fee";
				case FreezedStatuses.FreezedWithoutFee:
					return "Freeze Without Fee";
				default:
					throw new ArgumentOutOfRangeException(nameof(freezeStatus), freezeStatus, null);
			}
		}

		public static string ToFullName(this StudentSemester.ResultRemarksTypes resultRemarksType)
		{
			switch (resultRemarksType)
			{
				case StudentSemester.ResultRemarksTypes.None:
					return null;
				case StudentSemester.ResultRemarksTypes.Chance:
					return "Chance";
				case StudentSemester.ResultRemarksTypes.Probation1:
					return "Probation 1";
				case StudentSemester.ResultRemarksTypes.Probation2:
					return "Probation 2";
				case StudentSemester.ResultRemarksTypes.Probation3:
					return "Probation 3";
				case StudentSemester.ResultRemarksTypes.Relegated:
					return "Relegated";
				case StudentSemester.ResultRemarksTypes.Dropped:
					return "Dropped";
				case StudentSemester.ResultRemarksTypes.Probation:
					return "Probation";
				case StudentSemester.ResultRemarksTypes.Qualified:
					return "Qualified";
				default:
					throw new ArgumentOutOfRangeException(nameof(resultRemarksType), resultRemarksType, null);
			}
		}

		public static string ToShortName(this FreezedStatuses freezedStatus)
		{
			switch (freezedStatus)
			{
				case FreezedStatuses.FreezedWithFee:
					return "FF";
				case FreezedStatuses.FreezedWithoutFee:
					return "FR";
				default:
					throw new ArgumentOutOfRangeException(nameof(freezedStatus), freezedStatus, null);
			}
		}

		public static string ToFullName(this AdmissionOpenProgram.EntryTestTypes entryTestTypeEnum)
		{
			switch (entryTestTypeEnum)
			{
				case AdmissionOpenProgram.EntryTestTypes.None:
					return null;
				case AdmissionOpenProgram.EntryTestTypes.ComputerBasedTest:
					return "Computer Based Test";
				case AdmissionOpenProgram.EntryTestTypes.PaperBasedTest:
					return "Paper Based Test";
				default:
					throw new ArgumentOutOfRangeException(nameof(entryTestTypeEnum), entryTestTypeEnum, null);
			}
		}

		public static string ToFullName(this StudentFee.Statuses status)
		{
			switch (status)
			{
				case StudentFee.Statuses.NotPaid:
					return "Not Paid";
				case StudentFee.Statuses.Paid:
					return "Paid";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToFullName(this RegisteredCours.Statuses status)
		{
			switch (status)
			{
				case RegisteredCours.Statuses.WithdrawWithFee:
					return "Withdrawal with Fee";
				case RegisteredCours.Statuses.WithdrawWithoutFee:
					return "Withdrawal without Fee";
				case RegisteredCours.Statuses.Incomplete:
					return "Incomplete";
				case RegisteredCours.Statuses.AttendanceDefaulter:
					return "Attendance Defaulter";
				case RegisteredCours.Statuses.Deferred:
					return "Deferred";
				case RegisteredCours.Statuses.UnfairMeans:
					return "Unfair Means";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToFullName(this ExamInvigilatorDuty.AttendanceStatuses status)
		{
			switch (status)
			{
				case ExamInvigilatorDuty.AttendanceStatuses.Absent:
					return "Absent";
				case ExamInvigilatorDuty.AttendanceStatuses.Present:
					return "Present";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToShortName(this RegisteredCours.Statuses status)
		{
			switch (status)
			{
				case RegisteredCours.Statuses.WithdrawWithFee:
					return "WF";
				case RegisteredCours.Statuses.WithdrawWithoutFee:
					return "W";
				case RegisteredCours.Statuses.Incomplete:
					return "I";
				case RegisteredCours.Statuses.AttendanceDefaulter:
					return "AD";
				case RegisteredCours.Statuses.Deferred:
					return "DF";
				case RegisteredCours.Statuses.UnfairMeans:
					return "UFM";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToFullNames(this RegisteredCours.Statuses status, string separator = ", ")
		{
			return string.Join(separator, status.GetFlags().Select(s => s.ToFullName()));
		}

		public static string ToFullNames(this ExamMarksTypes examMarksTypes, string separator = ", ")
		{
			return string.Join(separator, examMarksTypes.GetFlags().Select(s => s.ToFullName()));
		}

		public static string ToSemesterString(this short semesterID)
		{
			return Semester.ToString(semesterID);
		}

		public static string ToSemesterString(this short? semesterID)
		{
			return semesterID?.ToSemesterString();
		}

		public static short ToShort(this UserTypes userType)
		{
			return (short)userType;
		}

		public static bool IsAdmin(this UserTypes userType)
		{
			return userType == UserTypes.Admin;
		}

		public static bool IsStaff(this UserTypes userType)
		{
			return userType == UserTypes.Staff;
		}

		public static bool IsCandidate(this UserTypes userType)
		{
			return userType == UserTypes.Candidate;
		}

		public static bool IsStudent(this UserTypes userType)
		{
			return userType == UserTypes.Student;
		}

		public static bool IsFaculty(this UserTypes userType)
		{
			return userType == UserTypes.Faculty;
		}

		public static bool IsMasterAdmin(this UserTypes userType)
		{
			return userType == UserTypes.MasterAdmin;
		}

		public static bool IsExecutive(this UserTypes userType)
		{
			return userType == UserTypes.Executive;
		}

		public static bool IsAdminOrMasterAdmin(this UserTypes userType)
		{
			return userType == UserTypes.MasterAdmin || userType == UserTypes.Admin;
		}

		public static UserTypes ToUserType(this byte userType)
		{
			return (UserTypes)userType;
		}

		public static UserTypes ToUserType(this string userType)
		{
			return (UserTypes)Enum.Parse(typeof(UserTypes), userType);
		}

		public static SemesterNos Increment(this SemesterNos semesterNo)
		{
			switch (semesterNo)
			{
				case SemesterNos.Zero:
					return SemesterNos.One;
				case SemesterNos.One:
					return SemesterNos.Two;
				case SemesterNos.Two:
					return SemesterNos.Three;
				case SemesterNos.Three:
					return SemesterNos.Four;
				case SemesterNos.Four:
					return SemesterNos.Five;
				case SemesterNos.Five:
					return SemesterNos.Six;
				case SemesterNos.Six:
					return SemesterNos.Seven;
				case SemesterNos.Seven:
					return SemesterNos.Eight;
				case SemesterNos.Eight:
					return SemesterNos.Nine;
				case SemesterNos.Nine:
					return SemesterNos.Ten;
				case SemesterNos.Ten:
					return SemesterNos.Ten;
				default:
					throw new ArgumentOutOfRangeException(nameof(semesterNo), semesterNo, null);
			}
		}

		public static SemesterNos Decrement(this SemesterNos semesterNo)
		{
			switch (semesterNo)
			{
				case SemesterNos.Zero:
					return SemesterNos.Zero;
				case SemesterNos.One:
					return SemesterNos.Zero;
				case SemesterNos.Two:
					return SemesterNos.One;
				case SemesterNos.Three:
					return SemesterNos.Two;
				case SemesterNos.Four:
					return SemesterNos.Three;
				case SemesterNos.Five:
					return SemesterNos.Four;
				case SemesterNos.Six:
					return SemesterNos.Five;
				case SemesterNos.Seven:
					return SemesterNos.Six;
				case SemesterNos.Eight:
					return SemesterNos.Seven;
				case SemesterNos.Nine:
					return SemesterNos.Eight;
				case SemesterNos.Ten:
					return SemesterNos.Nine;
				default:
					throw new ArgumentOutOfRangeException(nameof(semesterNo), semesterNo, null);
			}
		}

		public static SemesterNos GetMaxSemesterNo(this ProgramDurations programDuration, SemesterTypes intakeSemesterType, bool isSummerRegularSemester)
		{
			switch (intakeSemesterType)
			{
				case SemesterTypes.Spring:
					break;
				case SemesterTypes.Summer:
					if (!isSummerRegularSemester)
						throw new InvalidOperationException("Summer Intake and Summer Semester is not regular. How & Why?");
					break;
				case SemesterTypes.Fall:
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(intakeSemesterType), intakeSemesterType, null);
			}
			switch (programDuration)
			{
				case ProgramDurations.OneYear:
					return isSummerRegularSemester ? SemesterNos.Three : SemesterNos.Two;
				case ProgramDurations.OnePointFiveYears:
					return isSummerRegularSemester ? SemesterNos.Four : SemesterNos.Three;
				case ProgramDurations.TwoYears:
					return isSummerRegularSemester ? SemesterNos.Six : SemesterNos.Four;
				case ProgramDurations.TwoPointFiveYears:
					return isSummerRegularSemester ? SemesterNos.Seven : SemesterNos.Five;
				case ProgramDurations.ThreeYears:
					return isSummerRegularSemester ? SemesterNos.Nine : SemesterNos.Six;
				case ProgramDurations.ThreePointFiveYears:
					return isSummerRegularSemester ? SemesterNos.Nine : SemesterNos.Seven;
				case ProgramDurations.FourYears:
					if (isSummerRegularSemester || intakeSemesterType == SemesterTypes.Summer)
						throw new NotSupportedException("Summer cannot be regular for 4 years program.");
					return SemesterNos.Eight;
				case ProgramDurations.FiveYears:
					if (isSummerRegularSemester || intakeSemesterType == SemesterTypes.Summer)
						throw new NotSupportedException("Summer cannot be regular for 5 years program.");
					return SemesterNos.Ten;
				case ProgramDurations.FourMonths:
					return SemesterNos.One;
				case ProgramDurations.SixMonths:
					return SemesterNos.One;
				default:
					throw new ArgumentOutOfRangeException(nameof(programDuration), programDuration, null);
			}
		}

		public static int GetTotalSemesterNo(this ProgramDurations programDuration, bool isSummerRegularSemester)
		{
			if (isSummerRegularSemester == false)
				switch (programDuration)
				{
					case ProgramDurations.OneYear:
						return 2;
					case ProgramDurations.OnePointFiveYears:
						return 3;
					case ProgramDurations.TwoYears:
						return 4;
					case ProgramDurations.TwoPointFiveYears:
						return 5;
					case ProgramDurations.ThreeYears:
						return 6;
					case ProgramDurations.ThreePointFiveYears:
						return 7;
					case ProgramDurations.FourYears:
						return 8;
					case ProgramDurations.FiveYears:
						return 10;
					case ProgramDurations.FourMonths:
						return 1;
					case ProgramDurations.SixMonths:
						return 1;
					default:
						throw new ArgumentOutOfRangeException(nameof(programDuration), programDuration, null);
				}
			switch (programDuration)
			{
				case ProgramDurations.OneYear:
					return 2;
				case ProgramDurations.OnePointFiveYears:
					return 3;
				case ProgramDurations.TwoYears:
					return 4;
				case ProgramDurations.TwoPointFiveYears:
					return 5;
				case ProgramDurations.ThreeYears:
					return 6;
				case ProgramDurations.ThreePointFiveYears:
					return 9;
				case ProgramDurations.FourYears:
					return 8;
				case ProgramDurations.FiveYears:
					return 10;
				case ProgramDurations.FourMonths:
					return 1;
				case ProgramDurations.SixMonths:
					return 1;
				default:
					throw new ArgumentOutOfRangeException(nameof(programDuration), programDuration, null);
			}
		}

		public static short GetLastSemester(this short semesterID, ProgramDurations programDuration, bool isSummerRegularSemester)
		{
			return Semester.AddSemesters(semesterID, programDuration.GetTotalSemesterNo(isSummerRegularSemester) - 1, isSummerRegularSemester);
		}

		public static SemesterNos NextSemesterNo(this SemesterNos lastSemesterNo, short forSemesterID, SemesterTypes intakeSemesterType, ProgramDurations duration, bool isSummerRegularSemester)
		{
			var forSemesterType = Semester.GetSemesterType(forSemesterID);
			SemesterNos nextSemesterNo;
			switch (forSemesterType)
			{
				case SemesterTypes.Spring:
					nextSemesterNo = lastSemesterNo.Increment();
					break;
				case SemesterTypes.Summer:
					nextSemesterNo = isSummerRegularSemester ? lastSemesterNo.Increment() : lastSemesterNo;
					break;
				case SemesterTypes.Fall:
					nextSemesterNo = lastSemesterNo.Increment();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			var maxLastSemester = duration.GetMaxSemesterNo(intakeSemesterType, isSummerRegularSemester);
			if (nextSemesterNo > maxLastSemester)
				nextSemesterNo = maxLastSemester;
			return nextSemesterNo;
		}

		public static string ToFullName(this OfferedCourseAttendance.HoursValues hours, bool presentIfOneOrOneAndHalf)
		{
			switch (hours)
			{
				case OfferedCourseAttendance.HoursValues.Absent:
					return "Absent";
				case OfferedCourseAttendance.HoursValues.One:
					return presentIfOneOrOneAndHalf ? "Present" : "1";
				case OfferedCourseAttendance.HoursValues.OneAndHalf:
					return presentIfOneOrOneAndHalf ? "Present" : "1.5";
				case OfferedCourseAttendance.HoursValues.Two:
					return "2";
				case OfferedCourseAttendance.HoursValues.Three:
					return "3";
				default:
					throw new ArgumentOutOfRangeException(nameof(hours), hours, null);
			}
		}

		public static decimal ToDecimal(this OfferedCourseAttendance.HoursValues hours)
		{
			switch (hours)
			{
				case OfferedCourseAttendance.HoursValues.Absent:
					return 0M;
				case OfferedCourseAttendance.HoursValues.One:
					return 1M;
				case OfferedCourseAttendance.HoursValues.OneAndHalf:
					return 1.5M;
				case OfferedCourseAttendance.HoursValues.Two:
					return 2M;
				case OfferedCourseAttendance.HoursValues.Three:
					return 3M;
				default:
					throw new ArgumentOutOfRangeException(nameof(hours), hours, null);
			}
		}

		public static OfferedCourseAttendance.HoursValues ToHoursValue(this decimal value)
		{
			switch (value)
			{
				case 0M:
					return OfferedCourseAttendance.HoursValues.Absent;
				case 1M:
					return OfferedCourseAttendance.HoursValues.One;
				case 1.5M:
					return OfferedCourseAttendance.HoursValues.OneAndHalf;
				case 2M:
					return OfferedCourseAttendance.HoursValues.Two;
				case 3M:
					return OfferedCourseAttendance.HoursValues.Three;
				default:
					throw new ArgumentOutOfRangeException(nameof(value), value, null);
			}
		}

		public static List<OfferedCourseAttendance.HoursValues> GetSmallerOrEqualHourValue(this OfferedCourseAttendance.HoursValues hours)
		{
			switch (hours)
			{
				case OfferedCourseAttendance.HoursValues.Absent:
					throw new InvalidOperationException();
				case OfferedCourseAttendance.HoursValues.One:
					return new List<OfferedCourseAttendance.HoursValues> { OfferedCourseAttendance.HoursValues.Absent, OfferedCourseAttendance.HoursValues.One };
				case OfferedCourseAttendance.HoursValues.OneAndHalf:
					return new List<OfferedCourseAttendance.HoursValues> { OfferedCourseAttendance.HoursValues.Absent, OfferedCourseAttendance.HoursValues.OneAndHalf };
				case OfferedCourseAttendance.HoursValues.Two:
					return new List<OfferedCourseAttendance.HoursValues> { OfferedCourseAttendance.HoursValues.Absent, OfferedCourseAttendance.HoursValues.One, OfferedCourseAttendance.HoursValues.Two };
				case OfferedCourseAttendance.HoursValues.Three:
					return new List<OfferedCourseAttendance.HoursValues> { OfferedCourseAttendance.HoursValues.Absent, OfferedCourseAttendance.HoursValues.One, OfferedCourseAttendance.HoursValues.Two, OfferedCourseAttendance.HoursValues.Three };
				default:
					throw new ArgumentOutOfRangeException(nameof(hours), hours, null);
			}
		}

		public static string ToFullName(this UserGroupPermission.PermissionValues permissionValue)
		{
			return permissionValue.ToString().SplitCamelCasing();
		}

		public static string ToFullName(this AspireModules module)
		{
			switch (module)
			{
				case AspireModules.None:
					return null;
				default:
					return module.ToString().SplitCamelCasing();
			}
		}

		public static string ToFullName(this UserGroupPermission.PermissionTypes permissionType)
		{
			return permissionType.ToString().SplitCamelCasing();
		}

		public static string ToFullName(this ExamConduct.ExamTypes examType)
		{
			switch (examType)
			{
				case ExamConduct.ExamTypes.Mid:
					return "Mid-Term";
				case ExamConduct.ExamTypes.Final:
					return "Final-Term";
				default:
					throw new ArgumentOutOfRangeException(nameof(examType), examType, null);
			}
		}

		public static string ToFullName(this ExamOfferedRoom.RoomUsageTypes roomUsageTypes)
		{
			switch (roomUsageTypes)
			{
				case ExamOfferedRoom.RoomUsageTypes.SinglePaper:
					return "Single Paper";
				case ExamOfferedRoom.RoomUsageTypes.MultiplePapers:
					return "Multiple Papers";
				default:
					throw new ArgumentOutOfRangeException(nameof(roomUsageTypes), roomUsageTypes, null);
			}
		}

		public static string ToFullName(this ExamBuilding.Statuses status)
		{
			switch (status)
			{
				case ExamBuilding.Statuses.Active:
					return "Active";
				case ExamBuilding.Statuses.Inactive:
					return "Inactive";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToFullName(this User.Statuses status)
		{
			switch (status)
			{
				case User.Statuses.Active:
					return "Active";
				case User.Statuses.Inactive:
					return "Inactive";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToFullName(this StudentFee.FeeTypes feeType)
		{
			switch (feeType)
			{
				case StudentFee.FeeTypes.Attendance:
					return "Attendance";
				case StudentFee.FeeTypes.Challan:
					return "Fee Challan";
				case StudentFee.FeeTypes.Refund:
					return "Refund";
				default:
					throw new ArgumentOutOfRangeException(nameof(feeType), feeType, null);
			}
		}

		public static string ToFullName(this ExamRoom.Statuses status)
		{
			switch (status)
			{
				case ExamRoom.Statuses.Active:
					return "Active";
				case ExamRoom.Statuses.Inactive:
					return "Inactive";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static List<AspirePermissionType> GetPermissionsList(this UserTypes userType)
		{
			Type declaringType;
			Type baseType;
			switch (userType)
			{
				case UserTypes.Admin:
					declaringType = typeof(AdminPermissions);
					baseType = typeof(AdminPermissionType);
					break;
				case UserTypes.Staff:
					declaringType = typeof(StaffPermissions);
					baseType = typeof(StaffPermissionType);
					break;
				case UserTypes.Executive:
					declaringType = typeof(ExecutivePermissions);
					baseType = typeof(ExecutivePermissionType);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(userType), userType, null);
			}

			var getPermissionsList = new Func<Type, AspirePermissionType[]>(type => type.GetFields(BindingFlags.Public | BindingFlags.Static).Where(f => f.Name == "PermissionsList").SelectMany(f => (AspirePermissionType[])f.GetValue(null)).ToArray());

			var permissionsList = declaringType.Assembly.GetTypes().Where(t => t.DeclaringType == declaringType && t.BaseType == baseType).SelectMany(t => getPermissionsList(t)).ToList();
			if (permissionsList.Any(p => p.UserTypeEnum != userType))
				throw new InvalidOperationException("Invalid permission set found. Correct the UserType.");
			return permissionsList;
		}

		public static string ToFullName(this ExamInvigilator.Statuses status)
		{
			switch (status)
			{
				case ExamInvigilator.Statuses.Active:
					return "Active";
				case ExamInvigilator.Statuses.Inactive:
					return "Inactive";
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static string ToFullName(this IntegratedServiceUser.Statuses status)
		{
			switch (status)
			{
				case IntegratedServiceUser.Statuses.Active:
					return "Active";
				case IntegratedServiceUser.Statuses.Inactive:
					return "Inactive";
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static string ToFullName(this IntegratedServiceUser.ServiceTypes serviceType)
		{
			switch (serviceType)
			{
				case IntegratedServiceUser.ServiceTypes.CBT:
					return "CBT";
				case IntegratedServiceUser.ServiceTypes.BankAlfalah:
					return "Bank Alfalah";
				case IntegratedServiceUser.ServiceTypes.AlliedBank:
					return "Allied Bank";
				case IntegratedServiceUser.ServiceTypes.HR:
					return "HR";
				case IntegratedServiceUser.ServiceTypes.OBE:
					return "OBE";
				case IntegratedServiceUser.ServiceTypes.LMS:
					return "LMS";
				default:
					throw new NotImplementedEnumException(serviceType);
			}
		}

		public static string ToFullName(this Survey.SurveyTypes surveyType)
		{
			switch (surveyType)
			{
				case Survey.SurveyTypes.StudentWise:
					return "Student Wise";
				case Survey.SurveyTypes.StudentCourseWise:
					return "Student Course Wise";
				case Survey.SurveyTypes.FacultyMemberWise:
					return "Faculty Member Wise";
				case Survey.SurveyTypes.FacultyMemberCourseWise:
					return "Faculty Member Course Wise";
				default:
					throw new NotImplementedEnumException(surveyType);
			}
		}

		public static string ToFullName(this CandidateAppliedProgram.Statuses status)
		{
			switch (status)
			{
				case CandidateAppliedProgram.Statuses.None:
					return null;
				case CandidateAppliedProgram.Statuses.Refunded:
					return "Refunded";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToFullName(this CBTLab.Statuses status)
		{
			switch (status)
			{
				case CBTLab.Statuses.Active:
					return "Active";
				case CBTLab.Statuses.Inactive:
					return "Inactive";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToFullName(this AccountTransaction.Services service)
		{
			switch (service)
			{
				case AccountTransaction.Services.Manual:
					return "Manual";
				case AccountTransaction.Services.BankAlfalah:
					return "Bank Alfalah";
				case AccountTransaction.Services.AlliedBank:
					return "ABL";
				default:
					throw new ArgumentOutOfRangeException(nameof(service), service, null);
			}
		}

		public static string ToFullName(this AccountTransaction.TransactionChannels transactionChannel)
		{
			switch (transactionChannel)
			{
				case AccountTransaction.TransactionChannels.Campus:
					return "Campus";
				case AccountTransaction.TransactionChannels.ATM:
					return "ATM";
				case AccountTransaction.TransactionChannels.MobileBanking:
					return "Mobile Banking";
				case AccountTransaction.TransactionChannels.Branch:
					return "Branch";
				case AccountTransaction.TransactionChannels.InternetBanking:
					return "Internet Banking";
				case AccountTransaction.TransactionChannels.ContactCenter:
					return "Contact Center";
				default:
					throw new ArgumentOutOfRangeException(nameof(transactionChannel), transactionChannel, null);
			}
		}

		public static string ToFullName(this AccountTransaction.PaymentTypes paymentType)
		{
			switch (paymentType)
			{
				case AccountTransaction.PaymentTypes.Cash:
					return "Cash";
				case AccountTransaction.PaymentTypes.Account:
					return "Account";
				case AccountTransaction.PaymentTypes.DemandDraft:
					return "DemandDraft";
				case AccountTransaction.PaymentTypes.PayOrder:
					return "Pay Order";
				case AccountTransaction.PaymentTypes.Cheque:
					return "Cheque";
				default:
					throw new ArgumentOutOfRangeException(nameof(paymentType), paymentType, null);
			}
		}

		public static string ToFullName(this ExamMarkingScheme.MarkingSchemeTypes markingSchemeType)
		{
			switch (markingSchemeType)
			{
				case ExamMarkingScheme.MarkingSchemeTypes.AssignmentsQuizzesMidFinal:
					return "Assignments, Quizzes, Mid & Final";
				case ExamMarkingScheme.MarkingSchemeTypes.InternalsMidFinal:
					return "Internals, Mid & Final";
				default:
					throw new ArgumentOutOfRangeException(nameof(markingSchemeType), markingSchemeType, null);
			}
		}

		public static string ToFullName(this ExamMarksPolicy.MarksEntryTypes marksEntryType)
		{
			switch (marksEntryType)
			{
				case ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
					return "Assignments, Quizzes, Mid and Final";
				case ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
					return "Assignment, Compile Assignments, Quiz, Compile Quizzes, Mid and Final";
				case ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					return "Assignment, Quiz, Compile Internals, Mid and Final";
				case ExamMarksPolicy.MarksEntryTypes.Final:
					return "Only Final";
				default:
					throw new ArgumentOutOfRangeException(nameof(marksEntryType), marksEntryType, null);
			}
		}

		public static string ToFullName(this Scholarship.ScholarshipTypes scholarshipType)
		{
			switch (scholarshipType)
			{
				case Scholarship.ScholarshipTypes.QarzeHasana:
					return "Qarz-e-Hasna";
				case Scholarship.ScholarshipTypes.StudentStudyLoan:
					return "Student Study Loan";
				default:
					throw new ArgumentOutOfRangeException(nameof(scholarshipType), scholarshipType, null);
			}
		}

		public static string ToFullName(this Scholarship.Statuses status)
		{
			switch (status)
			{
				case Scholarship.Statuses.Active:
					return "Active";
				case Scholarship.Statuses.Inactive:
					return "Inactive";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToFullName(this ScholarshipApplication.Statuses? status)
		{
			switch (status)
			{
				case ScholarshipApplication.Statuses.InProgress:
					return "In Progress";
				case ScholarshipApplication.Statuses.Submitted:
					return "Applied";
				case ScholarshipApplication.Statuses.DateOver:
					return "Closed";
				case ScholarshipApplication.Statuses.NotApplied:
				case null:
					return "Open";
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		public static string ToFullName(this ScholarshipApplication.DocumentsStatuses? documentsStatus)
		{
			switch (documentsStatus)
			{
				case ScholarshipApplication.DocumentsStatuses.NotSubmitted:
					return "Not Submitted";
				case ScholarshipApplication.DocumentsStatuses.Submitted:
					return "Submitted";
				case null:
					return string.Empty.ToNAIfNullOrEmpty();
				default:
					throw new ArgumentOutOfRangeException(nameof(documentsStatus), documentsStatus, null);
			}
		}

		public static string ToFullName(this ScholarshipApplicationApproval.ApprovalStatuses approvalStatus)
		{
			switch (approvalStatus)
			{
				case ScholarshipApplicationApproval.ApprovalStatuses.Pending:
					return "Pending";
				case ScholarshipApplicationApproval.ApprovalStatuses.Approved:
					return "Approved";
				case ScholarshipApplicationApproval.ApprovalStatuses.Rejected:
					return "Rejected";
				default:
					throw new ArgumentOutOfRangeException(nameof(approvalStatus), approvalStatus, null);
			}
		}

		public static string ToFullName(this ScholarshipApplicationApproval.ChequeStatuses isChequeReady)
		{
			switch (isChequeReady)
			{
				case ScholarshipApplicationApproval.ChequeStatuses.No:
					return "No";
				case ScholarshipApplicationApproval.ChequeStatuses.Yes:
					return "Yes";
				default:
					throw new ArgumentOutOfRangeException(nameof(isChequeReady), isChequeReady, null);
			}
		}

		public static string ToAbbreviation(this ExamMarksPolicy.MarksEntryTypes marksEntryType)
		{
			switch (marksEntryType)
			{
				case ExamMarksPolicy.MarksEntryTypes.AssignmentsQuizzesMidFinal:
					return "AQMF";
				case ExamMarksPolicy.MarksEntryTypes.AssignmentCompileAssignmentsQuizCompileQuizzesMidFinal:
					return "cAcQMF";
				case ExamMarksPolicy.MarksEntryTypes.AssignmentQuizCompileInternalsMidFinal:
					return "cIMF";
				case ExamMarksPolicy.MarksEntryTypes.Final:
					return "F";
				default:
					throw new ArgumentOutOfRangeException(nameof(marksEntryType), marksEntryType, null);
			}
		}

		public static string ToFullName(this FeeHead.FeeHeadTypes feeHeadTypeEnum)
		{
			switch (feeHeadTypeEnum)
			{
				case FeeHead.FeeHeadTypes.TutionFee:
					return "Tution Fee";
				case FeeHead.FeeHeadTypes.Arrears:
					return "Arrears";
				case FeeHead.FeeHeadTypes.WithholdingTax:
					return "Withholding Tax";
				case FeeHead.FeeHeadTypes.Custom:
					return "Custom";
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static string ToFullName(this TransferredStudentCourseMapping.Statuses statusEnum)
		{
			switch (statusEnum)
			{
				case TransferredStudentCourseMapping.Statuses.Included:
					return "Included";
				case TransferredStudentCourseMapping.Statuses.Excluded:
					return "Excluded";
				default:
					throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
			}
		}

		public static string ToFullName(this ExamRemarksTypes examRemarksTypeEnum)
		{
			switch (examRemarksTypeEnum)
			{
				case ExamRemarksTypes.None:
					return null;
				case ExamRemarksTypes.Qualified:
					return "Qualified";
				case ExamRemarksTypes.Probation:
					return "Probation";
				case ExamRemarksTypes.Chance:
					return "Chance";
				case ExamRemarksTypes.Drop:
					return "Dropped";
				case ExamRemarksTypes.Probation1:
					return "Probation 1";
				case ExamRemarksTypes.Probation2:
					return "Probation 2";
				case ExamRemarksTypes.Probation3:
					return "Probation 3";
				case ExamRemarksTypes.Relegated:
					return "Relegated";
				default:
					throw new ArgumentOutOfRangeException(nameof(examRemarksTypeEnum), examRemarksTypeEnum, null);
			}
		}

		public static string ToFullName(this Program.Statuses statusEnum)
		{
			switch (statusEnum)
			{
				case Program.Statuses.Active:
					return "Active";
				case Program.Statuses.Inactive:
					return "Inactive";
				default:
					throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
			}
		}

		public static string ToFullName(this StudentAcademicRecord.Statuses statusEnum)
		{
			switch (statusEnum)
			{
				case StudentAcademicRecord.Statuses.Pending:
					return "Pending";
				case StudentAcademicRecord.Statuses.Objection:
					return "Objection";
				case StudentAcademicRecord.Statuses.Verified:
					return "Verified";
				default:
					throw new ArgumentOutOfRangeException(nameof(statusEnum), statusEnum, null);
			}
		}

		public static string ToFullName(this GraduateDirectoryFormWorkExperience.ExperienceTypes experienceTypeEnum)
		{
			switch (experienceTypeEnum)
			{
				case GraduateDirectoryFormWorkExperience.ExperienceTypes.Internship:
					return "Internship";
				case GraduateDirectoryFormWorkExperience.ExperienceTypes.PartTimeJob:
					return "Part-Time Job";
				case GraduateDirectoryFormWorkExperience.ExperienceTypes.FullTimeJob:
					return "Full-Time Job";
				default:
					throw new ArgumentOutOfRangeException(nameof(experienceTypeEnum), experienceTypeEnum, null);
			}
		}

		public static string ToFullName(this GraduateDirectoryFormWorkshop.Types typeEnum)
		{
			switch (typeEnum)
			{
				case GraduateDirectoryFormWorkshop.Types.Workshop:
					return "Workshop";
				case GraduateDirectoryFormWorkshop.Types.Seminar:
					return "Seminar";
				default:
					throw new NotImplementedEnumException(typeEnum);
			}
		}

		public static string ToFullName(this Student.AdmissionTypes admissionTypeEnum)
		{
			switch (admissionTypeEnum)
			{
				case Student.AdmissionTypes.Admission:
					return "Admission";
				case Student.AdmissionTypes.Transferred:
					return "Transferred";
				case Student.AdmissionTypes.AdmissionDeferred:
					return "Admission Deferred";
				default:
					throw new NotImplementedEnumException(admissionTypeEnum);
			}
		}

		public static string ToFullName(this StudentDocument.Pages pageEnum)
		{
			switch (pageEnum)
			{
				case StudentDocument.Pages.Sheet1Front:
					return "Sheet 1 (Front Side)";
				case StudentDocument.Pages.Sheet1Back:
					return "Sheet 1 (Back Side)";
				case StudentDocument.Pages.Sheet2Front:
					return "Sheet 2 (Front Side)";
				case StudentDocument.Pages.Sheet2Back:
					return "Sheet 2 (Back Side)";
				case StudentDocument.Pages.Sheet3Front:
					return "Sheet 3 (Front Side)";
				case StudentDocument.Pages.Sheet3Back:
					return "Sheet 3 (Back Side)";
				default:
					throw new ArgumentOutOfRangeException(nameof(pageEnum), pageEnum, null);
			}
		}

		public static string ToFullName(this Email.Priorities priorityEnum)
		{
			switch (priorityEnum)
			{
				case Email.Priorities.Highest:
					return "Highest";
				case Email.Priorities.VeryHigh:
					return "Very High";
				case Email.Priorities.High:
					return "High";
				case Email.Priorities.Normal:
					return "Normal";
				case Email.Priorities.Low:
					return "Low";
				case Email.Priorities.VeryLow:
					return "Very Low";
				case Email.Priorities.Lowest:
					return "Lowest";
				default:
					throw new NotImplementedEnumException(priorityEnum);
			}
		}

		public static string ToFullName(this CompileExamMarksTypes compileExamMarksTypeEnum)
		{
			switch (compileExamMarksTypeEnum)
			{
				case CompileExamMarksTypes.Assignments:
					return "Assignments";
				case CompileExamMarksTypes.Quizzes:
					return "Quizzes";
				case CompileExamMarksTypes.Internals:
					return "Internals";
				default:
					throw new NotImplementedEnumException(compileExamMarksTypeEnum);
			}
		}

		public static string ToFileExtension(this SystemFile.FileExtensions fileExtensionEnum)
		{
			switch (fileExtensionEnum)
			{
				case SystemFile.FileExtensions.None:
					throw new InvalidOperationException("File extension is unknown.");
				case SystemFile.FileExtensions.Png:
					return ".png";
				case SystemFile.FileExtensions.Jpg:
					return ".jpg";
				case SystemFile.FileExtensions.Jpeg:
					return ".jpeg";
				case SystemFile.FileExtensions.Pdf:
					return ".pdf";
				case SystemFile.FileExtensions.Csv:
					return ".csv";
				case SystemFile.FileExtensions.Xls:
					return ".xls";
				case SystemFile.FileExtensions.Xlsx:
					return ".xlsx";
				case SystemFile.FileExtensions.Doc:
					return ".doc";
				case SystemFile.FileExtensions.Docx:
					return ".docx";
				default:
					throw new NotImplementedEnumException(fileExtensionEnum);
			}
		}

		public static SystemFile.FileExtensions ToFileExtension(this string fileName)
		{
			if (string.IsNullOrWhiteSpace(fileName))
				return SystemFile.FileExtensions.None;
			switch (System.IO.Path.GetExtension(fileName).ToLower())
			{
				case ".pdf":
					return SystemFile.FileExtensions.Pdf;
				case ".jpg":
					return SystemFile.FileExtensions.Jpg;
				case ".jpeg":
					return SystemFile.FileExtensions.Jpeg;
				case ".png":
					return SystemFile.FileExtensions.Png;
				case ".csv":
					return SystemFile.FileExtensions.Csv;
				case ".xls":
					return SystemFile.FileExtensions.Xls;
				case ".xlsx":
					return SystemFile.FileExtensions.Xlsx;
				case ".doc":
					return SystemFile.FileExtensions.Doc;
				case ".docx":
					return SystemFile.FileExtensions.Docx;
				default:
					return SystemFile.FileExtensions.None;
			}
		}

		public static string ToMimeType(this SystemFile.FileExtensions fileExtensionEnum)
		{
			switch (fileExtensionEnum)
			{
				case SystemFile.FileExtensions.None:
					return "application/octet-stream";
				case SystemFile.FileExtensions.Png:
					return "image/png";
				case SystemFile.FileExtensions.Jpg:
				case SystemFile.FileExtensions.Jpeg:
					return "image/jpeg";
				case SystemFile.FileExtensions.Pdf:
					return "application/pdf";
				case SystemFile.FileExtensions.Csv:
					return "text/csv";
				case SystemFile.FileExtensions.Xls:
					return "application/vnd.ms-excel";
				case SystemFile.FileExtensions.Xlsx:
					return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
				case SystemFile.FileExtensions.Doc:
					return "application/msword";
				case SystemFile.FileExtensions.Docx:
					return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
				default:
					throw new NotImplementedEnumException(fileExtensionEnum);
			}
		}

		public static string ToMimeType(this string fileName)
		{
			return fileName.ToFileExtension().ToMimeType();
		}
	}
}