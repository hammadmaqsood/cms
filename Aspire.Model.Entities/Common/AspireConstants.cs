﻿namespace Aspire.Model.Entities.Common
{
	public static class AspireConstants
	{
		public static readonly string BankAlfalahName = "Bank Alfalah";
		public static readonly string AlliedBankName = "Allied Bank";
	}
}
