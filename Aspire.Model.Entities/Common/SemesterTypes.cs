﻿namespace Aspire.Model.Entities.Common
{
	public enum SemesterTypes : byte
	{
		Spring = 1,
		Summer = 2,
		Fall = 3,
		//AnnualExam = 4,
		//SupplementaryExam = 5,
	}
}
