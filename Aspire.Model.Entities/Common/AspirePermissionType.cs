using Aspire.Lib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Model.Entities.Common
{
	public abstract class AspirePermissionType
	{
		public short UserType => this.UserTypeEnum.ToShort();
		public abstract UserTypes UserTypeEnum { get; }
		public byte Module => this.ModuleEnum.ToByte();
		public abstract AspireModules ModuleEnum { get; }
		public short PermissionType => (short)this.PermissionTypeEnum;
		public UserGroupPermission.PermissionTypes PermissionTypeEnum { get; }
		public string PermissionTypeFullName => this.PermissionTypeEnum.ToFullName();
		public byte DefaultValue => (byte)this.DefaultValueEnum;
		public UserGroupPermission.PermissionValues DefaultValueEnum { get; }
		public IReadOnlyCollection<UserGroupPermission.PermissionValues> PossiblePermissionValues { get; }

		protected AspirePermissionType(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues)
		{
			this.PermissionTypeEnum = permissionType;
			this.DefaultValueEnum = defaultValue;
			if (!possiblePermissionValues.Any())
				throw new ArgumentException(nameof(possiblePermissionValues));
			if (possiblePermissionValues.Distinct().Count() != possiblePermissionValues.Length)
				throw new ArgumentException(nameof(possiblePermissionValues));
			this.PossiblePermissionValues = possiblePermissionValues.ToList().AsReadOnly();
		}
	}
}