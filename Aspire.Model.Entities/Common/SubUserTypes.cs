﻿using System;

namespace Aspire.Model.Entities.Common
{
	[Flags]
	public enum SubUserTypes : byte
	{
		None = 0,
		Parents = 1,
	}
}