﻿namespace Aspire.Model.Entities.Common
{
	public enum DegreeLevels : byte
	{
		Diploma = 1,
		Undergraduate = 0,
		Masters = 2,
		Postgraduate = 3,
		Doctorate = 4
	}
}
