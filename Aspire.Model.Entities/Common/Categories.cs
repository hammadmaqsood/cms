﻿using System;

namespace Aspire.Model.Entities.Common
{
	[Flags]
	public enum Categories : byte
	{
		NavalRetired = 1,
		NavalServing = 2,
		Naval = NavalRetired | NavalServing,
		Others = 4,
	}
}
