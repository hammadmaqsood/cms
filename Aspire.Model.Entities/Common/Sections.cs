﻿using System;

namespace Aspire.Model.Entities.Common
{
	[Flags]
	public enum Sections
	{
		A = 1,
		B = A * 2,
		C = B * 2,
		D = C * 2,
		E = D * 2,
		F = E * 2,
		G = F * 2,
		H = G * 2,
		I = H * 2,
		J = I * 2,
		K = J * 2,
		L = K * 2,
		M = L * 2,
		N = M * 2,
		O = N * 2,
		P = O * 2,
		Q = P * 2,
		R = Q * 2,
		S = R * 2,
		T = S * 2,
		U = T * 2,
		V = U * 2,
		W = V * 2,
		X = W * 2,
		Y = X * 2,
		Z = Y * 2
	}
}
