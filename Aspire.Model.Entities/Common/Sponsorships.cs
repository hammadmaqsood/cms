﻿namespace Aspire.Model.Entities.Common
{
	public enum Sponsorships : byte
	{
		Father = 1,
		Self = 2,
		Guardian = 4,
		Organization = 8,
	}
}
