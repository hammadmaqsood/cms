﻿using System.Collections.Generic;
using System.Linq;

namespace Aspire.Model.Entities.Common
{
	public sealed class ExamGradeComparer : IComparer<ExamGrades>
	{
		private static readonly (ExamGrades Grade, byte Points)[] ExamGradesInSequence =
		{
			(ExamGrades.A, 11),
			(ExamGrades.AMinus, 10),
			(ExamGrades.BPlus, 9),
			(ExamGrades.B, 8),
			(ExamGrades.BMinus, 7),
			(ExamGrades.CPlus, 6),
			(ExamGrades.C, 5),
			(ExamGrades.CMinus, 4),
			(ExamGrades.DPlus, 3),
			(ExamGrades.D, 2),
			(ExamGrades.F ,1)
		};

		public int Compare(ExamGrades x, ExamGrades y)// A, CPlus
		{
			return ExamGradesInSequence.Single(g => g.Grade == x).Points.CompareTo(ExamGradesInSequence.Single(g => g.Grade == y).Points);
		}

		public static IComparer<ExamGrades> New()
		{
			return new ExamGradeComparer();
		}

		public static bool LessThan(ExamGrades x, ExamGrades y) => ExamGradesInSequence.Single(g => g.Grade == x).Points < ExamGradesInSequence.Single(g => g.Grade == y).Points;
		public static bool LessThanOrEqualTo(ExamGrades x, ExamGrades y) => ExamGradesInSequence.Single(g => g.Grade == x).Points <= ExamGradesInSequence.Single(g => g.Grade == y).Points;
		public static bool EqualTo(ExamGrades x, ExamGrades y) => ExamGradesInSequence.Single(g => g.Grade == x).Points == ExamGradesInSequence.Single(g => g.Grade == y).Points;
		public static bool GreaterThan(ExamGrades x, ExamGrades y) => ExamGradesInSequence.Single(g => g.Grade == x).Points > ExamGradesInSequence.Single(g => g.Grade == y).Points;
		public static bool GreaterThanOrEqualTo(ExamGrades x, ExamGrades y) => ExamGradesInSequence.Single(g => g.Grade == x).Points >= ExamGradesInSequence.Single(g => g.Grade == y).Points;
	}
}
