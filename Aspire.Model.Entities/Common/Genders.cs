﻿using System;

namespace Aspire.Model.Entities.Common
{
	[Flags]
	public enum Genders : byte
	{
		Male = 1,
		Female = 2,
	}
}
