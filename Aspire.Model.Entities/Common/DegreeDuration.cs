﻿namespace Aspire.Model.Entities.Common
{
	public enum ProgramDurations : byte
	{
		OneYear = 1,
		OnePointFiveYears = 2,
		TwoYears = 3,
		TwoPointFiveYears = 4,
		ThreeYears = 5,
		ThreePointFiveYears = 6,
		FourYears = 7,
		FiveYears = 8,
		FourMonths = 9,
		SixMonths = 10,
	}
}
