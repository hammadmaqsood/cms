namespace Aspire.Model.Entities.Common
{
	public abstract class StaffPermissionType : AspirePermissionType
	{
		public sealed override UserTypes UserTypeEnum => UserTypes.Staff;

		protected StaffPermissionType(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
		{
		}
	}
}