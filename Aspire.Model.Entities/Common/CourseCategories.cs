﻿using System;

namespace Aspire.Model.Entities.Common
{
	[Flags]
	public enum CourseCategories : byte
	{
		Course = 1,
		Internship = 2,
		Lab = 4,
		Project = 8,
		Thesis = 16,
	}
}