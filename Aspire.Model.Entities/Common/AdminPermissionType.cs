namespace Aspire.Model.Entities.Common
{
	public abstract class AdminPermissionType : AspirePermissionType
	{
		public sealed override UserTypes UserTypeEnum => UserTypes.Admin;

		protected AdminPermissionType(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
		{
		}
	}
}