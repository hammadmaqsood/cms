﻿namespace Aspire.Model.Entities.Common
{
	public enum AspireModules : byte
	{
		None = 0,
		Administration,
		Admissions,
		ClassAttendance,
		CourseOffering,
		CourseRegistration,
		Courses,
		Exams,
		ExamSeatingPlan,
		FacultyManagement,
		FeeManagement,
		Logs,
		QualityAssurance,
		StudentInformation,
		TimeTable,
		UserManagement,
		Scholarship,
		Pgp,
		LibraryManagement,
		OfficialDocuments,
		Complaints,
		FileServer,
		LMS,
		Feedbacks,
		Alumni,
		Forms
	}
}
