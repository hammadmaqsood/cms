﻿using System;

namespace Aspire.Model.Entities.Common
{
	[Flags]
	public enum UserTypes : short
	{
		Anonymous = 1,
		System = 2,
		MasterAdmin = 4,
		Admin = 8,
		Executive = 16,
		Staff = 32,
		Candidate = 64,
		Student = 128,
		Faculty = 256,
		Alumni = 512,
		IntegratedService = 1024,
		ManualSQL = 2048,
		Admins = MasterAdmin | Admin,
		Any = Anonymous | MasterAdmin | Admin | Executive | Staff | Candidate | Student | Faculty | Alumni,
	}
}
