﻿// ReSharper disable InconsistentNaming
namespace Aspire.Model.Entities.Common
{
	public enum ETSTypes : byte
	{
		GAT = 1,
		GMAT = 2,
		GRE = 3,
		SAT_I = 4,
		SAT_II = 5,
		NTS = 6,
		LAT = 7,
	}
}