﻿using Aspire.Lib.Extensions;
using System;

namespace Aspire.Model.Entities.Common
{
	[AttributeUsage(AttributeTargets.Field)]
	public sealed class EnumGuidValueAttribute : Attribute
	{
		public Guid Guid { get; }
		public string FullName { get; }
		public string Description { get; }

		public EnumGuidValueAttribute(string guid, string fullName, string description = null)
		{
			this.Guid = guid.ToGuid();
			this.FullName = fullName;
			this.Description = description;
		}
	}
}
