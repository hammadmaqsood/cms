﻿using Aspire.Lib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Aspire.Model.Entities.Common
{
	public static class EnumGuidValues
	{
		private static Dictionary<Enum, EnumGuidValueAttribute> EnumAttributes { get; }
		private static Dictionary<Guid, (Enum Enum, EnumGuidValueAttribute Attribute)> GuidAttributes { get; }

		internal static T GetCustomAttribute<T>(this Enum enumValue) where T : Attribute
		{
			return enumValue.GetType().GetMember(enumValue.ToString()).Single().GetCustomAttribute<T>();
		}

		static EnumGuidValues()
		{
			var allEnums = typeof(EnumGuidValues).Assembly.GetTypes()
							.Where(t => t.IsEnum && t.GetCustomAttribute<EnumGuidsAttribute>() != null)
							.SelectMany(t => Enum.GetValues(t).Cast<Enum>());
			var allAttributes = allEnums.ToDictionary(e => e, e => e.GetCustomAttribute<EnumGuidValueAttribute>());

			IEnumerable<string> Validate()
			{
				var guids = new HashSet<Guid>();
				foreach (var attribute in allAttributes)
				{
					if (attribute.Value == null)
						yield return $"{attribute.Key} EnumGuidsAttribute is missing.";
					else
					{
						if (guids.Contains(attribute.Value.Guid))
							yield return $"{attribute.Key} EnumGuidsAttribute's Guid is already in use. Guid: {attribute.Value.Guid}";
						else
							guids.Add(attribute.Value.Guid);
					}
				}
			}
			var errorMessages = Validate().ToList();
			if (errorMessages.Any())
				throw new InvalidOperationException(errorMessages.Join("\n"));
			else
			{
				EnumAttributes = allAttributes;
				GuidAttributes = allAttributes.ToDictionary(a => a.Value.Guid, a => (a.Key, a.Value));
			}
		}

		public static EnumGuidValueAttribute GetEnumGuidValueAttribute(this Enum @enum)
		{
			if (EnumAttributes.TryGetValue(@enum, out var attribute))
				return attribute;
			throw new InvalidOperationException($"EnumGuidValueAttribute not found for {@enum.GetType().FullName}.{@enum}");
		}

		public static (Enum Enum, EnumGuidValueAttribute Attribute) GetEnumGuidValueAttribute(this Guid guid)
		{
			if (GuidAttributes.TryGetValue(guid, out var value))
				return value;
			throw new InvalidOperationException($"EnumGuidValueAttribute not found for {guid}");
		}

		public static string GetFullName(this Enum enumValue)
		{
			return enumValue.GetEnumGuidValueAttribute().FullName;
		}

		public static string GetDescription(this Enum enumValue)
		{
			return enumValue.GetEnumGuidValueAttribute().Description;
		}

		public static Guid GetGuid(this Enum enumValue)
		{
			return enumValue.GetEnumGuidValueAttribute().Guid;
		}

		public static string GetFullName(this Guid guid)
		{
			return guid.GetEnumGuidValueAttribute().Attribute.FullName;
		}

		public static Guid ValidateEnum<T>(this Guid guid) where T : Enum
		{
			if (GuidAttributes.TryGetValue(guid, out var value) && value.Enum.GetType() == typeof(T))
				return guid;
			throw new InvalidOperationException($"Guid {guid} is not defined for Enum {typeof(T)}.");
		}

		public static string GetDescription(this Guid guid)
		{
			return guid.GetEnumGuidValueAttribute().Attribute.Description;
		}

		public static Enum GetEnum(this Guid guid)
		{
			return guid.GetEnumGuidValueAttribute().Enum;
		}

		public static T GetEnum<T>(this Guid guid) where T : Enum
		{
			return (T)guid.GetEnum();
		}
	}
}
