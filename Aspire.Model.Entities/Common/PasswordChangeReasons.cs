﻿namespace Aspire.Model.Entities.Common
{
	public enum PasswordChangeReasons : byte
	{
		ByUserItSelf = 1,
		ManuallySettingForNewUser = 2,
		PasswordPotentiallyCompromised = 3,
		SetDuringRegistration = 4,
		Troubleshooting = 5,
		UserForgotHisPassword = 6,
		UsingResetLink = 7,
	}
}
