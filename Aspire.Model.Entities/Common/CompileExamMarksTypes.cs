﻿namespace Aspire.Model.Entities.Common
{
	public enum CompileExamMarksTypes : byte
	{
		Assignments = 1,
		Quizzes = 2,
		Internals = 3,
	}
}