﻿using System;

namespace Aspire.Model.Entities.Common
{
	[AttributeUsage(AttributeTargets.Enum)]
	public sealed class EnumGuidsAttribute : Attribute
	{
	}
}
