﻿namespace Aspire.Model.Entities.Common
{
	public enum ProgramTypes : byte
	{
		Degree = 1,
		ShortCourse = 2,
	}
}
