﻿using System;

namespace Aspire.Model.Entities.Common
{
	[Flags]
	public enum SemesterNos : short
	{
		Zero = 1,
		One = 2,
		Two = 4,
		Three = 8,
		Four = 16,
		Five = 32,
		Six = 64,
		Seven = 128,
		Eight = 256,
		Nine = 512,
		Ten = 1024,
	}
}
