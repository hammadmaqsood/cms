﻿namespace Aspire.Model.Entities.Common
{
	public enum FreezedStatuses : byte
	{
		FreezedWithFee = 1,
		FreezedWithoutFee = 2,
	}
}
