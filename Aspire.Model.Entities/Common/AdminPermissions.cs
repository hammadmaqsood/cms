namespace Aspire.Model.Entities.Common
{
	public sealed class AdminPermissions
	{
		public sealed class UserManagement : AdminPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.UserManagement;

			private UserManagement(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly UserManagement Module = new UserManagement(UserGroupPermission.PermissionTypes.UserManagementModule, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.AllowedDenied);
			public static readonly UserManagement ManageUsers = new UserManagement(UserGroupPermission.PermissionTypes.ManageUsers, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly UserManagement ManageUserGroups = new UserManagement(UserGroupPermission.PermissionTypes.ManageUserGroups, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly UserManagement[] PermissionsList =
			{
				Module,
				ManageUsers,
				ManageUserGroups,
			};
		}

		public sealed class Administration : AdminPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Administration;

			private Administration(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly Administration Module = new Administration(UserGroupPermission.PermissionTypes.AdministrationModule, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.AllowedDenied);
			public static readonly Administration ManageInstitutes = new Administration(UserGroupPermission.PermissionTypes.ManageInstitutes, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Administration ManageSemesters = new Administration(UserGroupPermission.PermissionTypes.ManageSemesters, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Administration ManageDepartments = new Administration(UserGroupPermission.PermissionTypes.ManageDepartments, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Administration ManageFaculties = new Administration(UserGroupPermission.PermissionTypes.ManageFaculties, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Administration ManagePrograms = new Administration(UserGroupPermission.PermissionTypes.ManagePrograms, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Administration ManageContacts = new Administration(UserGroupPermission.PermissionTypes.ManageContacts, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Administration[] PermissionsList =
			{
				Module,
				ManageInstitutes,
				ManageFaculties,
				ManageDepartments,
				ManagePrograms,
				ManageSemesters,
				ManageContacts
			};
		}

		public sealed class Admissions : AdminPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Admissions;

			private Admissions(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly Admissions Module = new Admissions(UserGroupPermission.PermissionTypes.AdmissionsModule, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.AllowedDenied);
			public static readonly Admissions ManageAdmissionCriteria = new Admissions(UserGroupPermission.PermissionTypes.ManageAdmissionCriteria, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Admissions ManageAdmissionOpening = new Admissions(UserGroupPermission.PermissionTypes.ManageAdmissionOpening, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Admissions ManageCBTLabs = new Admissions(UserGroupPermission.PermissionTypes.ManageCBTLabs, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Admissions ManageCBTSessions = new Admissions(UserGroupPermission.PermissionTypes.ManageCBTSessions, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Admissions ExportCandidateInformationForCBT = new Admissions(UserGroupPermission.PermissionTypes.ExportCandidateInformationForCBT, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Admissions[] PermissionsList =
			{
				Module,
				ManageAdmissionCriteria,
				ManageAdmissionOpening,
				ManageCBTLabs,
				ManageCBTSessions,
				ExportCandidateInformationForCBT,
			};
		}

		public sealed class ClassAttendance : AdminPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.ClassAttendance;

			private ClassAttendance(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly ClassAttendance Module = new ClassAttendance(UserGroupPermission.PermissionTypes.ClassAttendanceModule, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.AllowedDenied);
			public static readonly ClassAttendance ManageClassAttendanceSettings = new ClassAttendance(UserGroupPermission.PermissionTypes.ManageClassAttendanceSettings, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ClassAttendance[] PermissionsList =
			{
				Module,
				ManageClassAttendanceSettings,
			};
		}

		public sealed class CourseRegistration : AdminPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.CourseRegistration;

			private CourseRegistration(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly CourseRegistration Module = new CourseRegistration(UserGroupPermission.PermissionTypes.CourseRegistrationModule, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration ManageCourseRegistrationSettings = new CourseRegistration(UserGroupPermission.PermissionTypes.ManageCourseRegistrationSettings, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration[] PermissionsList =
			{
				Module,
				ManageCourseRegistrationSettings,
			};
		}

		public sealed class Courses : AdminPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Courses;

			private Courses(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly Courses Module = new Courses(UserGroupPermission.PermissionTypes.CoursesModule, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.AllowedDenied);
			public static readonly Courses[] PermissionsList =
			{
				Module,
			};
		}

		public sealed class Exams : AdminPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Exams;

			private Exams(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly Exams Module = new Exams(UserGroupPermission.PermissionTypes.ExamsModule, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.AllowedDenied);
			public static readonly Exams ManageControlPanel = new Exams(UserGroupPermission.PermissionTypes.ManageControlPanel, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams ManageExamMarksGradingSchemes = new Exams(UserGroupPermission.PermissionTypes.ManageExamMarksGradingSchemes, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams ManageExamMarkingSchemes = new Exams(UserGroupPermission.PermissionTypes.ManageExamMarkingSchemes, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams ManageMarkingAndGradingSchemeAssignment = new Exams(UserGroupPermission.PermissionTypes.ManageMarkingAndGradingSchemeAssignment, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams ManageExamMarksPolicies = new Exams(UserGroupPermission.PermissionTypes.ManageExamMarksPolicies, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams ManageExamRemarksPolicies = new Exams(UserGroupPermission.PermissionTypes.ManageExamRemarksPolicies, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams[] PermissionsList =
			{
				Module,
				ManageControlPanel,
				ManageExamMarkingSchemes,
				ManageExamMarksGradingSchemes,
				ManageMarkingAndGradingSchemeAssignment,
				ManageExamMarksPolicies,
				ManageExamRemarksPolicies
			};
		}

		public sealed class FeeManagement : AdminPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.FeeManagement;

			private FeeManagement(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly FeeManagement Module = new FeeManagement(UserGroupPermission.PermissionTypes.FeeManagementModule, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.AllowedDenied);
			public static readonly FeeManagement ManageFeeHeads = new FeeManagement(UserGroupPermission.PermissionTypes.ManageFeeHeads, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly FeeManagement ManageBankAccounts = new FeeManagement(UserGroupPermission.PermissionTypes.ManageBankAccounts, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly FeeManagement[] PermissionsList =
			{
				Module,
				ManageFeeHeads,
				ManageBankAccounts,
			};
		}

		public sealed class QualityAssurance : AdminPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.QualityAssurance;

			private QualityAssurance(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly QualityAssurance Module = new QualityAssurance(UserGroupPermission.PermissionTypes.QualityAssuranceModule, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance ManageControlPanel = new QualityAssurance(UserGroupPermission.PermissionTypes.ManageControlPanel, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance ManageSurveys = new QualityAssurance(UserGroupPermission.PermissionTypes.ManageSurveys, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance ManageSurveysGroupsQuestionsOptions = new QualityAssurance(UserGroupPermission.PermissionTypes.ManageSurveysGroupsQuestionsOptions, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance[] PermissionsList =
			{
				Module,
				ManageControlPanel,
				ManageSurveys,
				ManageSurveysGroupsQuestionsOptions,
			};
		}

		public sealed class FacultyManagement : AdminPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.FacultyManagement;

			private FacultyManagement(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly FacultyManagement Module = new FacultyManagement(UserGroupPermission.PermissionTypes.FacultyManagementModule, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.AllowedDenied);
			public static readonly FacultyManagement ManageFacultyMembers = new FacultyManagement(UserGroupPermission.PermissionTypes.ManageFacultyMembers, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly FacultyManagement ManageFacultyMemberAccount = new FacultyManagement(UserGroupPermission.PermissionTypes.ManageFacultyMemberAccount, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly FacultyManagement[] PermissionsList =
			{
				Module,
				ManageFacultyMembers,
				ManageFacultyMemberAccount,
			};
		}

		public sealed class StudentInformation : AdminPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.StudentInformation;

			private StudentInformation(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly StudentInformation Module = new StudentInformation(UserGroupPermission.PermissionTypes.StudentInformationModule, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.AllowedDenied);
			public static readonly StudentInformation ManageStudentAccount = new StudentInformation(UserGroupPermission.PermissionTypes.ManageStudentAccount, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly StudentInformation[] PermissionsList =
			{
				Module,
				ManageStudentAccount,
			};
		}

		public sealed class Scholarship : AdminPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Scholarship;

			private Scholarship(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly Scholarship Module = new Scholarship(UserGroupPermission.PermissionTypes.ScholarshipModule, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.AllowedDenied);
			public static readonly Scholarship ManageControlPanel = new Scholarship(UserGroupPermission.PermissionTypes.ManageControlPanel, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Scholarship[] PermissionsList =
			{
				Module,
				ManageControlPanel
			};
		}

		public sealed class OfficialDocuments : AdminPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.OfficialDocuments;

			private OfficialDocuments(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly OfficialDocuments Module = new OfficialDocuments(UserGroupPermission.PermissionTypes.OfficialDocumentsModule, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.AllowedDenied);
			public static readonly OfficialDocuments ManageExecutiveDocuments = new OfficialDocuments(UserGroupPermission.PermissionTypes.ManageExecutiveDocuments, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly OfficialDocuments ManageStaffDocuments = new OfficialDocuments(UserGroupPermission.PermissionTypes.ManageStaffDocuments, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly OfficialDocuments ManageFacultyDocuments = new OfficialDocuments(UserGroupPermission.PermissionTypes.ManageFacultyDocuments, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly OfficialDocuments ManageStudentDocuments = new OfficialDocuments(UserGroupPermission.PermissionTypes.ManageStudentDocuments, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly OfficialDocuments[] PermissionsList =
			{
				Module,
				ManageExecutiveDocuments,
				ManageStaffDocuments,
				ManageFacultyDocuments,
				ManageStudentDocuments
			};
		}

		public sealed class Feedbacks : AdminPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Feedbacks;

			private Feedbacks(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly Feedbacks Module = new Feedbacks(UserGroupPermission.PermissionTypes.FeedbacksModule, UserGroupPermission.PermissionValues.Allowed, UserGroupPermission.AllowedDenied);
			public static readonly Feedbacks ManageAssignees = new Feedbacks(UserGroupPermission.PermissionTypes.ManageAssignees, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Feedbacks[] PermissionsList =
			{
				Module,
				ManageAssignees
			};
		}
	}
}