﻿namespace Aspire.Model.Entities.Common
{
	public sealed class StaffPermissions
	{
		public sealed class Admissions : StaffPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Admissions;

			private Admissions(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly Admissions Module = new Admissions(UserGroupPermission.PermissionTypes.AdmissionsModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Admissions ManageAdmissionProcessingFee = new Admissions(UserGroupPermission.PermissionTypes.ManageAdmissionProcessingFee, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Admissions ManageAdmissionProcessingFeeForLockedRecords = new Admissions(UserGroupPermission.PermissionTypes.ManageAdmissionProcessingFeeForLockedRecords, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Admissions SwapAdmissionProcessingFeeChallanNos = new Admissions(UserGroupPermission.PermissionTypes.SwapAdmissionProcessingFeeChallanNos, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Admissions ManageETSScore = new Admissions(UserGroupPermission.PermissionTypes.ManageETSScore, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDeniedSpecial);
			public static readonly Admissions ManageInterviews = new Admissions(UserGroupPermission.PermissionTypes.ManageInterviews, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Admissions GenerateEnrollments = new Admissions(UserGroupPermission.PermissionTypes.GenerateEnrollments, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Admissions ChangeProfileInformation = new Admissions(UserGroupPermission.PermissionTypes.ChangeProfileInformation, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Admissions ChangeAppliedProgram = new Admissions(UserGroupPermission.PermissionTypes.ChangeAppliedProgram, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Admissions ChangeCandidateStatus = new Admissions(UserGroupPermission.PermissionTypes.ChangeCandidateStatus, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Admissions StudentTransfer = new Admissions(UserGroupPermission.PermissionTypes.StudentTransfer, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Admissions[] PermissionsList =
			{
				Module,
				ManageAdmissionProcessingFee,
				ManageAdmissionProcessingFeeForLockedRecords,
				SwapAdmissionProcessingFeeChallanNos,
				ManageETSScore,
				ManageInterviews,
				GenerateEnrollments,
				ChangeProfileInformation,
				ChangeAppliedProgram,
				ChangeCandidateStatus,
				StudentTransfer,
			};
		}

		public sealed class ClassAttendance : StaffPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.ClassAttendance;

			private ClassAttendance(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly ClassAttendance Module = new ClassAttendance(UserGroupPermission.PermissionTypes.ClassAttendanceModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ClassAttendance[] PermissionsList =
			{
				Module
			};
		}

		public sealed class CourseRegistration : StaffPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.CourseRegistration;

			private CourseRegistration(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly CourseRegistration Module = new CourseRegistration(UserGroupPermission.PermissionTypes.CourseRegistrationModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration RegisterCourse = new CourseRegistration(UserGroupPermission.PermissionTypes.RegisterCourse, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration CanByPassRegistrationOpenCheck = new CourseRegistration(UserGroupPermission.PermissionTypes.CanByPassRegistrationOpenCheck, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration CanByPassPreRequisitesCheck = new CourseRegistration(UserGroupPermission.PermissionTypes.CanByPassPreRequisitesCheck, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration CanByPassImprovementCheck = new CourseRegistration(UserGroupPermission.PermissionTypes.CanByPassImprovementCheck, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration CanByPassMaxAllowedCoursesPerSemester = new CourseRegistration(UserGroupPermission.PermissionTypes.CanByPassMaxAllowedCoursesPerSemester, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration CanByPassMaxAllowedCreditHoursPerSemester = new CourseRegistration(UserGroupPermission.PermissionTypes.CanByPassMaxAllowedCreditHoursPerSemester, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration CanRegisterTimeBarredStudents = new CourseRegistration(UserGroupPermission.PermissionTypes.CanRegisterTimeBarredStudents, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration CanChangeStudentSectionShift = new CourseRegistration(UserGroupPermission.PermissionTypes.CanChangeStudentSectionShift, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration CanDeleteCourseRegistration = new CourseRegistration(UserGroupPermission.PermissionTypes.CanDeleteCourseRegistration, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration CanDeleteCourseRegistrationWithAttendance = new CourseRegistration(UserGroupPermission.PermissionTypes.CanDeleteCourseRegistrationWithAttendance, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration ManageStudentSemesters = new CourseRegistration(UserGroupPermission.PermissionTypes.ManageStudentSemesters, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration ManageStudentGPACGPA = new CourseRegistration(UserGroupPermission.PermissionTypes.ManageStudentGPACGPA, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration ManageSemesterFreezed = new CourseRegistration(UserGroupPermission.PermissionTypes.ManageSemesterFreezed, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration ManageStudentCreditTransfer = new CourseRegistration(UserGroupPermission.PermissionTypes.ManageStudentCreditTransfer, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration ManageExemptedCourses = new CourseRegistration(UserGroupPermission.PermissionTypes.ManageExemptedCourses, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration ManageRegisteredCourseStatus = new CourseRegistration(UserGroupPermission.PermissionTypes.ManageRegisteredCourseStatus, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration ManageTimeBarredStudents = new CourseRegistration(UserGroupPermission.PermissionTypes.ManageTimeBarredStudents, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration ManageCanceledAdmissions = new CourseRegistration(UserGroupPermission.PermissionTypes.ManageCancelledAdmissions, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseRegistration[] PermissionsList =
			{
				Module,
				RegisterCourse,
				CanByPassRegistrationOpenCheck,
				CanByPassPreRequisitesCheck,
				CanByPassImprovementCheck,
				CanByPassMaxAllowedCoursesPerSemester,
				CanByPassMaxAllowedCreditHoursPerSemester,
				CanRegisterTimeBarredStudents,
				CanChangeStudentSectionShift,
				CanDeleteCourseRegistration,
				CanDeleteCourseRegistrationWithAttendance,
				ManageStudentSemesters,
				ManageStudentGPACGPA,
				ManageSemesterFreezed,
				ManageStudentCreditTransfer,
				ManageExemptedCourses,
				ManageRegisteredCourseStatus,
				ManageTimeBarredStudents,
				ManageCanceledAdmissions,
			};
		}

		public sealed class Courses : StaffPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Courses;

			private Courses(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly Courses Module = new Courses(UserGroupPermission.PermissionTypes.CoursesModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Courses ManageCourses = new Courses(UserGroupPermission.PermissionTypes.ManageCourses, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Courses ManageCourseCustomMarks = new Courses(UserGroupPermission.PermissionTypes.ManageCourseCustomMarks, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Courses ManageCourseSummerGradeCapping = new Courses(UserGroupPermission.PermissionTypes.ManageCourseSummerGradeCapping, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Courses ManageCoursePreRequisites = new Courses(UserGroupPermission.PermissionTypes.ManageCoursePreRequisites, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Courses ManageEquivalentCourses = new Courses(UserGroupPermission.PermissionTypes.ManageEquivalentCourses, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Courses ManageCourseSubstitution = new Courses(UserGroupPermission.PermissionTypes.ManageCourseSubstitution, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Courses CanDeleteCourse = new Courses(UserGroupPermission.PermissionTypes.CanDeleteCourse, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Courses CanProcessCoursesCleanUp = new Courses(UserGroupPermission.PermissionTypes.CanProcessCoursesCleanUp, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Courses[] PermissionsList =
			{
				Module,
				ManageCourses,
				ManageCourseCustomMarks,
				ManageCourseSummerGradeCapping,
				ManageCoursePreRequisites,
				ManageEquivalentCourses,
				ManageCourseSubstitution,
				CanDeleteCourse,
				CanProcessCoursesCleanUp
			};
		}

		public sealed class CourseOffering : StaffPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.CourseOffering;

			private CourseOffering(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly CourseOffering Module = new CourseOffering(UserGroupPermission.PermissionTypes.CourseOfferingModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseOffering ManageOfferedCourses = new CourseOffering(UserGroupPermission.PermissionTypes.ManageOfferedCourses, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseOffering AssignTeacher = new CourseOffering(UserGroupPermission.PermissionTypes.AssignTeacher, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseOffering ManageExamDateSheet = new CourseOffering(UserGroupPermission.PermissionTypes.ManageExamDateSheet, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseOffering CanMergeClasses = new CourseOffering(UserGroupPermission.PermissionTypes.CanMergeClasses, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseOffering CanViewOnlineTeachingReadinessForm = new CourseOffering(UserGroupPermission.PermissionTypes.CanViewOnlineTeachingReadinessForm, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseOffering CanSubmitOnlineTeachingReadinessFormAsClusterHead = new CourseOffering(UserGroupPermission.PermissionTypes.CanSubmitOnlineTeachingReadinessFormAsClusterHead, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseOffering CanSubmitOnlineTeachingReadinessFormAsHOD = new CourseOffering(UserGroupPermission.PermissionTypes.CanSubmitOnlineTeachingReadinessFormAsHOD, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseOffering CanSubmitOnlineTeachingReadinessFormAsDirector = new CourseOffering(UserGroupPermission.PermissionTypes.CanSubmitOnlineTeachingReadinessFormAsDirector, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseOffering CanSubmitOnlineTeachingReadinessFormAsDG = new CourseOffering(UserGroupPermission.PermissionTypes.CanSubmitOnlineTeachingReadinessFormAsDG, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseOffering CanUndoSubmitOnlineTeachingReadinessFormAsHOD = new CourseOffering(UserGroupPermission.PermissionTypes.CanUndoSubmitOnlineTeachingReadinessFormAsHOD, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseOffering CanUndoSubmitOnlineTeachingReadinessFormAsDirector = new CourseOffering(UserGroupPermission.PermissionTypes.CanUndoSubmitOnlineTeachingReadinessFormAsDirector, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseOffering CanUndoSubmitOnlineTeachingReadinessFormAsDG = new CourseOffering(UserGroupPermission.PermissionTypes.CanUndoSubmitOnlineTeachingReadinessFormAsDG, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly CourseOffering[] PermissionsList =
			{
				Module,
				ManageOfferedCourses,
				AssignTeacher,
				CanMergeClasses,
				ManageExamDateSheet,
				CanViewOnlineTeachingReadinessForm,
				CanSubmitOnlineTeachingReadinessFormAsClusterHead,
				CanSubmitOnlineTeachingReadinessFormAsHOD,
				CanSubmitOnlineTeachingReadinessFormAsDirector,
				CanSubmitOnlineTeachingReadinessFormAsDG,
				CanUndoSubmitOnlineTeachingReadinessFormAsHOD,
				CanUndoSubmitOnlineTeachingReadinessFormAsDirector,
				CanUndoSubmitOnlineTeachingReadinessFormAsDG
			};
		}

		public sealed class Exams : StaffPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Exams;

			private Exams(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly Exams Module = new Exams(UserGroupPermission.PermissionTypes.ExamsModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams UpdateMarks = new Exams(UserGroupPermission.PermissionTypes.UpdateMarks, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams LockMarksEntry = new Exams(UserGroupPermission.PermissionTypes.LockMarksEntry, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams UnlockMarksEntry = new Exams(UserGroupPermission.PermissionTypes.UnlockMarksEntry, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams ManageExamMarksPolicies = new Exams(UserGroupPermission.PermissionTypes.ManageExamMarksPolicies, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams ManageExamRemarksPolicies = new Exams(UserGroupPermission.PermissionTypes.ManageExamRemarksPolicies, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams CompileCGPAAndRemarks = new Exams(UserGroupPermission.PermissionTypes.CompileCGPAAndRemarks, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams UniversityLevelMarksEntry = new Exams(UserGroupPermission.PermissionTypes.UniversityLevelMarksEntry, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams CanUnlockCampusLevelMarksEntry = new Exams(UserGroupPermission.PermissionTypes.CanUnlockCampusLevelMarksEntry, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams CampusLevelMarksEntry = new Exams(UserGroupPermission.PermissionTypes.CampusLevelMarksEntry, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams DepartmentLevelMarksEntry = new Exams(UserGroupPermission.PermissionTypes.DepartmentLevelMarksEntry, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams DepartmentLevelMarksEntryOfProjectThesisInternship = new Exams(UserGroupPermission.PermissionTypes.DepartmentLevelMarksEntryOfProjectThesisInternship, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams CanRevertSubmitToHOD = new Exams(UserGroupPermission.PermissionTypes.CanRevertSubmitToHOD, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams CanRevertSubmitToCampus = new Exams(UserGroupPermission.PermissionTypes.CanRevertSubmitToCampus, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams CanRevertSubmitToUniversity = new Exams(UserGroupPermission.PermissionTypes.CanRevertSubmitToUniversity, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams CanRevertSubmitToHODOfProjectThesisInternship = new Exams(UserGroupPermission.PermissionTypes.CanRevertSubmitToHODOfProjectThesisInternship, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams CanRevertSubmitToCampusOfProjectThesisInternship = new Exams(UserGroupPermission.PermissionTypes.CanRevertSubmitToCampusOfProjectThesisInternship, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Exams CanRevertSubmitToUniversityOfProjectThesisInternship = new Exams(UserGroupPermission.PermissionTypes.CanRevertSubmitToUniversityOfProjectThesisInternship, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);

			public static readonly Exams[] PermissionsList =
			{
				Module,
				UpdateMarks,
				LockMarksEntry,
				UnlockMarksEntry,
				ManageExamMarksPolicies,
				ManageExamRemarksPolicies,
				CompileCGPAAndRemarks,
				UniversityLevelMarksEntry,
				CanUnlockCampusLevelMarksEntry,
				CampusLevelMarksEntry,
				DepartmentLevelMarksEntry,
				DepartmentLevelMarksEntryOfProjectThesisInternship,
				CanRevertSubmitToHOD,
				CanRevertSubmitToCampus,
				CanRevertSubmitToUniversity,
				CanRevertSubmitToHODOfProjectThesisInternship,
				CanRevertSubmitToCampusOfProjectThesisInternship,
				CanRevertSubmitToUniversityOfProjectThesisInternship
			};
		}

		public sealed class ExamSeatingPlan : StaffPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.ExamSeatingPlan;

			private ExamSeatingPlan(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly ExamSeatingPlan Module = new ExamSeatingPlan(UserGroupPermission.PermissionTypes.ExamSeatingPlanModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ExamSeatingPlan ClearExtraSeats = new ExamSeatingPlan(UserGroupPermission.PermissionTypes.ClearExtraSeats, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ExamSeatingPlan DeleteSeat = new ExamSeatingPlan(UserGroupPermission.PermissionTypes.DeleteSeat, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ExamSeatingPlan ExecuteSeatingPlan = new ExamSeatingPlan(UserGroupPermission.PermissionTypes.ExecuteSeatingPlan, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ExamSeatingPlan IndividualSeating = new ExamSeatingPlan(UserGroupPermission.PermissionTypes.IndividualSeating, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ExamSeatingPlan ManageExamBuildings = new ExamSeatingPlan(UserGroupPermission.PermissionTypes.ManageExamBuildings, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ExamSeatingPlan ManageExamConducts = new ExamSeatingPlan(UserGroupPermission.PermissionTypes.ManageExamConducts, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ExamSeatingPlan ManageExamDateAndSessions = new ExamSeatingPlan(UserGroupPermission.PermissionTypes.ManageExamDateAndSessions, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ExamSeatingPlan ManageExamDates = new ExamSeatingPlan(UserGroupPermission.PermissionTypes.ManageExamDates, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ExamSeatingPlan ManageExamDateSheet = new ExamSeatingPlan(UserGroupPermission.PermissionTypes.ManageExamDateSheet, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ExamSeatingPlan ManageExamDesignations = new ExamSeatingPlan(UserGroupPermission.PermissionTypes.ManageExamDesignations, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ExamSeatingPlan ManageExamInvigilatorDuties = new ExamSeatingPlan(UserGroupPermission.PermissionTypes.ManageExamInvigilatorDuties, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ExamSeatingPlan ManageExamInvigilators = new ExamSeatingPlan(UserGroupPermission.PermissionTypes.ManageExamInvigilators, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ExamSeatingPlan ManageExamOfferedRooms = new ExamSeatingPlan(UserGroupPermission.PermissionTypes.ManageExamOfferedRooms, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ExamSeatingPlan ManageExamRooms = new ExamSeatingPlan(UserGroupPermission.PermissionTypes.ManageExamRooms, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ExamSeatingPlan ManageExamSessions = new ExamSeatingPlan(UserGroupPermission.PermissionTypes.ManageExamSessions, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly ExamSeatingPlan[] PermissionsList =
			{
				Module,
				ClearExtraSeats,
				DeleteSeat,
				ExecuteSeatingPlan,
				IndividualSeating,
				ManageExamBuildings,
				ManageExamConducts,
				ManageExamDateAndSessions,
				ManageExamDates,
				ManageExamDateSheet,
				ManageExamDesignations,
				ManageExamInvigilatorDuties,
				ManageExamInvigilators,
				ManageExamOfferedRooms,
				ManageExamRooms,
				ManageExamSessions,
			};
		}

		public sealed class FeeManagement : StaffPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.FeeManagement;

			private FeeManagement(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly FeeManagement Module = new FeeManagement(UserGroupPermission.PermissionTypes.FeeManagementModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly FeeManagement ManageFeeStructure = new FeeManagement(UserGroupPermission.PermissionTypes.ManageFeeStructure, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly FeeManagement ManageFeeConcessionTypes = new FeeManagement(UserGroupPermission.PermissionTypes.ManageFeeConcessionTypes, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly FeeManagement ManageFeeChallan = new FeeManagement(UserGroupPermission.PermissionTypes.ManageFeeChallan, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly FeeManagement CanDeleteFeeChallanContainingWithHoldingTax = new FeeManagement(UserGroupPermission.PermissionTypes.CanDeleteFeeChallanContainingWithHoldingTax, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly FeeManagement ManageFeePayment = new FeeManagement(UserGroupPermission.PermissionTypes.ManageFeePayment, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly FeeManagement ManageFeePaymentOfLockedRecords = new FeeManagement(UserGroupPermission.PermissionTypes.ManageFeePaymentOfLockedRecord, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly FeeManagement ManageFeeDueDates = new FeeManagement(UserGroupPermission.PermissionTypes.ManageFeeDueDates, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly FeeManagement[] PermissionsList =
			{
				Module,
				ManageFeeChallan,
				CanDeleteFeeChallanContainingWithHoldingTax,
				ManageFeePaymentOfLockedRecords,
				ManageFeeConcessionTypes,
				ManageFeePayment,
				ManageFeeStructure,
				ManageFeeDueDates,
			};
		}

		public sealed class StudentInformation : StaffPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.StudentInformation;

			private StudentInformation(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly StudentInformation Module = new StudentInformation(UserGroupPermission.PermissionTypes.StudentInformationModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly StudentInformation ManageStudentProfile = new StudentInformation(UserGroupPermission.PermissionTypes.ManageStudentProfile, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDeniedReadonly);
			public static readonly StudentInformation ManageCommunityServices = new StudentInformation(UserGroupPermission.PermissionTypes.ManageCommunityServices, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly StudentInformation ManageDisciplineCase = new StudentInformation(UserGroupPermission.PermissionTypes.ManageDisciplineCase, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly StudentInformation ManageStudentDisciplineCase = new StudentInformation(UserGroupPermission.PermissionTypes.ManageStudentDisciplineCase, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly StudentInformation ManageDisciplineCasesStudentRemarks = new StudentInformation(UserGroupPermission.PermissionTypes.ManageDisciplineCasesStudentRemarks, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly StudentInformation ViewStudentDocuments = new StudentInformation(UserGroupPermission.PermissionTypes.ViewStudentDocuments, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly StudentInformation UploadStudentDocuments = new StudentInformation(UserGroupPermission.PermissionTypes.UploadStudentDocuments, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly StudentInformation VerifyStudentDocuments = new StudentInformation(UserGroupPermission.PermissionTypes.VerifyStudentDocuments, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly StudentInformation ViewGraduateDirectory = new StudentInformation(UserGroupPermission.PermissionTypes.ViewGraduateDirectory, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly StudentInformation ManageDisciplineCaseActivities = new StudentInformation(UserGroupPermission.PermissionTypes.ManageDisciplineCaseActivities, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly StudentInformation UpdateRegistrationNos = new StudentInformation(UserGroupPermission.PermissionTypes.UpdateRegistrationNos, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);

			public static readonly StudentInformation[] PermissionsList =
			{
				Module,
				ManageStudentProfile,
				ManageCommunityServices,
				ManageDisciplineCase,
				ManageStudentDisciplineCase,
				ManageDisciplineCasesStudentRemarks,
				ViewStudentDocuments,
				UploadStudentDocuments,
				VerifyStudentDocuments,
				ViewGraduateDirectory,
				ManageDisciplineCaseActivities,
				UpdateRegistrationNos
			};
		}

		public sealed class QualityAssurance : StaffPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.QualityAssurance;

			private QualityAssurance(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly QualityAssurance Module = new QualityAssurance(UserGroupPermission.PermissionTypes.QualityAssuranceModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance SurveyForm2 = new QualityAssurance(UserGroupPermission.PermissionTypes.SurveyForm2, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDeniedReadonly);
			public static readonly QualityAssurance ManageConductSurveyForm2 = new QualityAssurance(UserGroupPermission.PermissionTypes.ManageConductSurveyForm2, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance GenerateLinksSurveyForm2 = new QualityAssurance(UserGroupPermission.PermissionTypes.GenerateLinksSurveyForm2, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance ManageSummarySurveyForm2 = new QualityAssurance(UserGroupPermission.PermissionTypes.ManageSummarySurveyForm2, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance SurveyForm4 = new QualityAssurance(UserGroupPermission.PermissionTypes.SurveyForm4, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDeniedReadonly);
			public static readonly QualityAssurance ManageConductSurveyForm4 = new QualityAssurance(UserGroupPermission.PermissionTypes.ManageConductSurveyForm4, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance GenerateLinksSurveyForm4 = new QualityAssurance(UserGroupPermission.PermissionTypes.GenerateLinksSurveyForm4, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance ManageSummarySurveyForm4 = new QualityAssurance(UserGroupPermission.PermissionTypes.ManageSummarySurveyForm4, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance SurveyForm5 = new QualityAssurance(UserGroupPermission.PermissionTypes.SurveyForm5, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDeniedReadonly);
			public static readonly QualityAssurance ManageConductSurveyForm5 = new QualityAssurance(UserGroupPermission.PermissionTypes.ManageConductSurveyForm5, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance GenerateLinksSurveyForm5 = new QualityAssurance(UserGroupPermission.PermissionTypes.GenerateLinksSurveyForm5, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance ManageSummarySurveyForm5 = new QualityAssurance(UserGroupPermission.PermissionTypes.ManageSummarySurveyForm5, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance SurveyForm6 = new QualityAssurance(UserGroupPermission.PermissionTypes.SurveyForm6, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDeniedReadonly);
			public static readonly QualityAssurance ManageConductSurveyForm6 = new QualityAssurance(UserGroupPermission.PermissionTypes.ManageConductSurveyForm6, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance GenerateLinksSurveyForm6 = new QualityAssurance(UserGroupPermission.PermissionTypes.GenerateLinksSurveyForm6, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance ManageSummarySurveyForm6 = new QualityAssurance(UserGroupPermission.PermissionTypes.ManageSummarySurveyForm6, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance SurveyForm7 = new QualityAssurance(UserGroupPermission.PermissionTypes.SurveyForm7, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDeniedReadonly);
			public static readonly QualityAssurance ManageConductSurveyForm7 = new QualityAssurance(UserGroupPermission.PermissionTypes.ManageConductSurveyForm7, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance GenerateLinksSurveyForm7 = new QualityAssurance(UserGroupPermission.PermissionTypes.GenerateLinksSurveyForm7, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance ManageSummarySurveyForm7 = new QualityAssurance(UserGroupPermission.PermissionTypes.ManageSummarySurveyForm7, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance SurveyForm8 = new QualityAssurance(UserGroupPermission.PermissionTypes.SurveyForm8, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDeniedReadonly);
			public static readonly QualityAssurance ManageConductSurveyForm8 = new QualityAssurance(UserGroupPermission.PermissionTypes.ManageConductSurveyForm8, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance GenerateLinksSurveyForm8 = new QualityAssurance(UserGroupPermission.PermissionTypes.GenerateLinksSurveyForm8, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance ManageSummarySurveyForm8 = new QualityAssurance(UserGroupPermission.PermissionTypes.ManageSummarySurveyForm8, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly QualityAssurance Employers = new QualityAssurance(UserGroupPermission.PermissionTypes.Employers, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);

			public static readonly QualityAssurance[] PermissionsList =
			{
				Module,
				SurveyForm2,
				ManageConductSurveyForm2,
				GenerateLinksSurveyForm2,
				ManageSummarySurveyForm2,
				SurveyForm4,
				ManageConductSurveyForm4,
				GenerateLinksSurveyForm4,
				ManageSummarySurveyForm4,
				SurveyForm5,
				ManageConductSurveyForm5,
				GenerateLinksSurveyForm5,
				ManageSummarySurveyForm5,
				SurveyForm6,
				ManageConductSurveyForm6,
				GenerateLinksSurveyForm6,
				ManageSummarySurveyForm6,
				SurveyForm7,
				ManageConductSurveyForm7,
				GenerateLinksSurveyForm7,
				ManageSummarySurveyForm7,
				SurveyForm8,
				ManageConductSurveyForm8,
				GenerateLinksSurveyForm8,
				ManageSummarySurveyForm8,
				Employers
			};
		}

		public sealed class Scholarship : StaffPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Scholarship;

			private Scholarship(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly Scholarship Module = new Scholarship(UserGroupPermission.PermissionTypes.ScholarshipModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Scholarship ManageSemesterWorkload = new Scholarship(UserGroupPermission.PermissionTypes.ManageSemesterWorkload, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Scholarship CompileMerit = new Scholarship(UserGroupPermission.PermissionTypes.CompileMerit, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Scholarship QarzeHasanaModule = new Scholarship(UserGroupPermission.PermissionTypes.QarzeHasanaModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Scholarship CanEditQarzeHasanaApplicationForm = new Scholarship(UserGroupPermission.PermissionTypes.CanEditQarzeHasanaApplicationForm, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Scholarship CanAddAndDeleteQarzeHasanaApplicationForm = new Scholarship(UserGroupPermission.PermissionTypes.CanAddAndDeleteQarzeHasanaApplicationForm, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Scholarship CanManageQarzeHasanaCampusRecommendations = new Scholarship(UserGroupPermission.PermissionTypes.CanManageQarzeHasanaCampusRecommendations, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Scholarship CanManageQarzeHasanaDSARecommendations = new Scholarship(UserGroupPermission.PermissionTypes.CanManageQarzeHasanaDSARecommendations, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Scholarship CanManageQarzeHasanaFinalRecommendations = new Scholarship(UserGroupPermission.PermissionTypes.CanManageQarzeHasanaFinalRecommendations, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Scholarship CanGenerateQarzeHasnaApplicationsForSpecificSemesters = new Scholarship(UserGroupPermission.PermissionTypes.CanGenerateQarzeHasnaApplicationsForSpecificSemesters, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Scholarship CanGenerateQarzeHasnaApplicationForAllSemester = new Scholarship(UserGroupPermission.PermissionTypes.CanGenerateQarzeHasnaApplicationForAllSemester, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Scholarship CanManageQarzeHasnaStudentsFeeDetails = new Scholarship(UserGroupPermission.PermissionTypes.CanManageQarzeHasnaStudentsFeeDetails, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Scholarship[] PermissionsList =
			{
				Module,
				ManageSemesterWorkload,
				CompileMerit,
				QarzeHasanaModule,
				CanEditQarzeHasanaApplicationForm,
				CanManageQarzeHasanaCampusRecommendations,
				CanManageQarzeHasanaDSARecommendations,
				CanManageQarzeHasanaFinalRecommendations,
				CanGenerateQarzeHasnaApplicationsForSpecificSemesters,
				CanGenerateQarzeHasnaApplicationForAllSemester,
				CanAddAndDeleteQarzeHasanaApplicationForm,
				CanManageQarzeHasnaStudentsFeeDetails
			};
		}

		public sealed class Pgp : StaffPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Pgp;

			private Pgp(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly Pgp Module = new Pgp(UserGroupPermission.PermissionTypes.PgpModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Pgp[] PermissionsList =
			{
				Module,
			};
		}

		public sealed class LibraryManagement : StaffPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.LibraryManagement;

			private LibraryManagement(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly LibraryManagement Module = new LibraryManagement(UserGroupPermission.PermissionTypes.LibraryManagementModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly LibraryManagement ManageStudentLibraryDefaulter = new LibraryManagement(UserGroupPermission.PermissionTypes.ManageStudentLibraryDefaulter, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly LibraryManagement[] PermissionsList =
			{
				Module,
				ManageStudentLibraryDefaulter,
			};
		}

		public sealed class OfficialDocuments : StaffPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.OfficialDocuments;

			private OfficialDocuments(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly OfficialDocuments Module = new OfficialDocuments(UserGroupPermission.PermissionTypes.OfficialDocumentsModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly OfficialDocuments[] PermissionsList =
			{
				Module
			};
		}

		public sealed class Complaints : StaffPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Complaints;

			private Complaints(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly Complaints Module = new Complaints(UserGroupPermission.PermissionTypes.ComplaintsModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Complaints ManageComplaints = new Complaints(UserGroupPermission.PermissionTypes.ManageComplaints, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Complaints[] PermissionsList =
			{
				Module,
				ManageComplaints
			};
		}

		public sealed class Alumni : StaffPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Alumni;

			private Alumni(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly Alumni Module = new Alumni(UserGroupPermission.PermissionTypes.AlumniModule, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Alumni CanManageJobPositions = new Alumni(UserGroupPermission.PermissionTypes.CanManageJobPositions, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Alumni[] PermissionsList =
			{
				Module,
				CanManageJobPositions,
			};
		}

		public sealed class Forms : StaffPermissionType
		{
			public override AspireModules ModuleEnum => AspireModules.Forms;

			private Forms(UserGroupPermission.PermissionTypes permissionType, UserGroupPermission.PermissionValues defaultPermissionValue, params UserGroupPermission.PermissionValues[] possiblePermissionValues) : base(permissionType, defaultPermissionValue, possiblePermissionValues)
			{
			}

			public static new readonly Forms Module = new Forms(UserGroupPermission.PermissionTypes.Forms, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Forms StudentClearanceForm = new Forms(UserGroupPermission.PermissionTypes.StudentClearanceForm, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Forms StudentClearanceFormCanInitiateOrEdit = new Forms(UserGroupPermission.PermissionTypes.StudentClearanceFormCanInitiateOrEdit, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Forms StudentClearanceFormCanChangeSSCStatus = new Forms(UserGroupPermission.PermissionTypes.StudentClearanceFormCanChangeSSCStatus, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Forms StudentClearanceFormMainLibrarySignature = new Forms(UserGroupPermission.PermissionTypes.StudentClearanceFormMainLibrarySignature, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Forms StudentClearanceFormLawLibrarySignature = new Forms(UserGroupPermission.PermissionTypes.StudentClearanceFormLawLibrarySignature, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Forms StudentClearanceFormLabsServerRoomSignature = new Forms(UserGroupPermission.PermissionTypes.StudentClearanceFormLabsServerRoomSignature, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Forms StudentClearanceFormLaptopSectionSignature = new Forms(UserGroupPermission.PermissionTypes.StudentClearanceFormLaptopSectionSignature, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Forms StudentClearanceFormStudentAffairsSignature = new Forms(UserGroupPermission.PermissionTypes.StudentClearanceFormStudentAffairsSignature, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Forms StudentClearanceFormHostelWardenSignature = new Forms(UserGroupPermission.PermissionTypes.StudentClearanceFormHostelWardenSignature, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Forms StudentClearanceFormExamCellSignature = new Forms(UserGroupPermission.PermissionTypes.StudentClearanceFormExamCellSignature, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Forms StudentClearanceFormAdmissionOfficeSignature = new Forms(UserGroupPermission.PermissionTypes.StudentClearanceFormAdmissionOfficeSignature, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Forms StudentClearanceFormSecurityAndSportsSignature = new Forms(UserGroupPermission.PermissionTypes.StudentClearanceFormSecurityAndSportsSignature, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Forms StudentClearanceFormStudentAdvisorSignature = new Forms(UserGroupPermission.PermissionTypes.StudentClearanceFormStudentAdvisorSignature, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Forms StudentClearanceFormAccountsSectionSignature = new Forms(UserGroupPermission.PermissionTypes.StudentClearanceFormAccountsSectionSignature, UserGroupPermission.PermissionValues.Denied, UserGroupPermission.AllowedDenied);
			public static readonly Forms[] PermissionsList =
			{
				Module,
				StudentClearanceForm,
				StudentClearanceFormCanInitiateOrEdit,
				StudentClearanceFormCanChangeSSCStatus,
				StudentClearanceFormMainLibrarySignature,
				StudentClearanceFormLawLibrarySignature,
				StudentClearanceFormLabsServerRoomSignature,
				StudentClearanceFormLaptopSectionSignature,
				StudentClearanceFormStudentAffairsSignature,
				StudentClearanceFormHostelWardenSignature,
				StudentClearanceFormExamCellSignature,
				StudentClearanceFormAdmissionOfficeSignature,
				StudentClearanceFormSecurityAndSportsSignature,
				StudentClearanceFormStudentAdvisorSignature,
				StudentClearanceFormAccountsSectionSignature
			};
		}
	}
}