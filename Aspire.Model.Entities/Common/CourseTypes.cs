﻿namespace Aspire.Model.Entities.Common
{
	public enum CourseTypes : byte
	{
		Core = 1,
		Elective = 2,
	}
}
