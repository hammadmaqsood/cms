//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Model.Entities
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// todo: Description is missing for [dbo].[PasswordChangeHistories] table.
    /// </summary>
    public partial class PasswordChangeHistory
    {
        public int PasswordChangeHistoryID { get; set; }
        public Nullable<int> UserID { get; set; }
        public Nullable<int> CandidateID { get; set; }
        public Nullable<int> StudentID { get; set; }
        public Nullable<int> StudentIDOfParent { get; set; }
        public Nullable<int> FacultyMemberID { get; set; }
        public System.DateTime ChangedDate { get; set; }
        public int UserLoginHistoryID { get; set; }
        public byte ChangeReason { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    
        /// <summary>
        /// <para>Foreign Key Name: FK_PasswordChangeHistories_Candidates_CandidateID</para>
        /// <para>Foreign Key Base Table: [dbo].[PasswordChangeHistories]</para>
        /// <para>Foreign Key Base Columns: [CandidateID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Candidates]</para>
        /// <para>Primary/Unique Key Base Columns: [CandidateID]</para>
        /// </summary>
        public virtual Candidate Candidate { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_PasswordChangeHistories_FacultyMembers_FacultyMemberID</para>
        /// <para>Foreign Key Base Table: [dbo].[PasswordChangeHistories]</para>
        /// <para>Foreign Key Base Columns: [FacultyMemberID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[FacultyMembers]</para>
        /// <para>Primary/Unique Key Base Columns: [FacultyMemberID]</para>
        /// </summary>
        public virtual FacultyMember FacultyMember { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_PasswordChangeHistories_Students_StudentID</para>
        /// <para>Foreign Key Base Table: [dbo].[PasswordChangeHistories]</para>
        /// <para>Foreign Key Base Columns: [StudentID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Students]</para>
        /// <para>Primary/Unique Key Base Columns: [StudentID]</para>
        /// </summary>
        public virtual Student Student { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_PasswordChangeHistories_Students_StudentIDOfParent</para>
        /// <para>Foreign Key Base Table: [dbo].[PasswordChangeHistories]</para>
        /// <para>Foreign Key Base Columns: [StudentIDOfParent]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Students]</para>
        /// <para>Primary/Unique Key Base Columns: [StudentID]</para>
        /// </summary>
        public virtual Student Student1 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_PasswordChangeHistories_UserLoginHistory_UserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[PasswordChangeHistories]</para>
        /// <para>Foreign Key Base Columns: [UserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual UserLoginHistory UserLoginHistory { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_PasswordChangeHistories_Users_UserID</para>
        /// <para>Foreign Key Base Table: [dbo].[PasswordChangeHistories]</para>
        /// <para>Foreign Key Base Columns: [UserID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Users]</para>
        /// <para>Primary/Unique Key Base Columns: [UserID]</para>
        /// </summary>
        public virtual User User { get; set; }
    }
}
