//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Model.Entities
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// todo: Description is missing for [dbo].[Courses] table.
    /// </summary>
    public partial class Cours
    {
        public Cours()
        {
            this.CourseEquivalents = new HashSet<CourseEquivalent>();
            this.CourseEquivalents1 = new HashSet<CourseEquivalent>();
            this.CoursePreReqs = new HashSet<CoursePreReq>();
            this.CoursePreReqs1 = new HashSet<CoursePreReq>();
            this.CoursePreReqs2 = new HashSet<CoursePreReq>();
            this.CoursePreReqs3 = new HashSet<CoursePreReq>();
            this.Courses1 = new HashSet<Cours>();
            this.OfferedCourses = new HashSet<OfferedCours>();
            this.RegisteredCourses = new HashSet<RegisteredCours>();
            this.StudentCreditsTransferredCourses = new HashSet<StudentCreditsTransferredCours>();
            this.StudentExemptedCourses = new HashSet<StudentExemptedCours>();
            this.StudentExemptedCourses1 = new HashSet<StudentExemptedCours>();
            this.TransferredStudentCourseMappings = new HashSet<TransferredStudentCourseMapping>();
        }
    
        public int CourseID { get; set; }
        public int AdmissionOpenProgramID { get; set; }
        public string CourseCode { get; set; }
        public string Title { get; set; }
        public decimal CreditHours { get; set; }
        public decimal ContactHours { get; set; }
        public byte CourseCategory { get; set; }
        public byte CourseType { get; set; }
        public Nullable<int> ProgramMajorID { get; set; }
        public short SemesterNo { get; set; }
        public string Remarks { get; set; }
        public Nullable<int> MigrationID { get; set; }
        public bool CustomMarks { get; set; }
        public bool SummerGradeCapping { get; set; }
        public System.Guid Status { get; set; }
        public Nullable<int> StatusChangedByUserLoginHistoryID { get; set; }
        public Nullable<System.DateTime> StatusDate { get; set; }
        public Nullable<int> SubstitutedCourseID { get; set; }
        public Nullable<int> SubstitutedByUserLoginHistoryID { get; set; }
        public Nullable<System.DateTime> SubstitutedDateTime { get; set; }
    
        /// <summary>
        /// <para>Foreign Key Name: FK_Courses_AdmissionOpenPrograms_AdmissionOpenProgramID</para>
        /// <para>Foreign Key Base Table: [dbo].[Courses]</para>
        /// <para>Foreign Key Base Columns: [AdmissionOpenProgramID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[AdmissionOpenPrograms]</para>
        /// <para>Primary/Unique Key Base Columns: [AdmissionOpenProgramID]</para>
        /// </summary>
        public virtual AdmissionOpenProgram AdmissionOpenProgram { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_CourseEquivalents_Courses_CourseID</para>
        /// <para>Foreign Key Base Table: [dbo].[CourseEquivalents]</para>
        /// <para>Foreign Key Base Columns: [CourseID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Courses]</para>
        /// <para>Primary/Unique Key Base Columns: [CourseID]</para>
        /// </summary>
        public virtual ICollection<CourseEquivalent> CourseEquivalents { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_CourseEquivalents_Courses_EquivalentCourseID</para>
        /// <para>Foreign Key Base Table: [dbo].[CourseEquivalents]</para>
        /// <para>Foreign Key Base Columns: [EquivalentCourseID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Courses]</para>
        /// <para>Primary/Unique Key Base Columns: [CourseID]</para>
        /// </summary>
        public virtual ICollection<CourseEquivalent> CourseEquivalents1 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_CoursePreReqs_Courses_CourseID</para>
        /// <para>Foreign Key Base Table: [dbo].[CoursePreReqs]</para>
        /// <para>Foreign Key Base Columns: [CourseID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Courses]</para>
        /// <para>Primary/Unique Key Base Columns: [CourseID]</para>
        /// </summary>
        public virtual ICollection<CoursePreReq> CoursePreReqs { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_CoursePreReqs_Courses_PreReqCourseID1</para>
        /// <para>Foreign Key Base Table: [dbo].[CoursePreReqs]</para>
        /// <para>Foreign Key Base Columns: [PreReqCourseID1]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Courses]</para>
        /// <para>Primary/Unique Key Base Columns: [CourseID]</para>
        /// </summary>
        public virtual ICollection<CoursePreReq> CoursePreReqs1 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_CoursePreReqs_Courses_PreReqCourseID2</para>
        /// <para>Foreign Key Base Table: [dbo].[CoursePreReqs]</para>
        /// <para>Foreign Key Base Columns: [PreReqCourseID2]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Courses]</para>
        /// <para>Primary/Unique Key Base Columns: [CourseID]</para>
        /// </summary>
        public virtual ICollection<CoursePreReq> CoursePreReqs2 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_CoursePreReqs_Courses_PreReqCourseID3</para>
        /// <para>Foreign Key Base Table: [dbo].[CoursePreReqs]</para>
        /// <para>Foreign Key Base Columns: [PreReqCourseID3]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Courses]</para>
        /// <para>Primary/Unique Key Base Columns: [CourseID]</para>
        /// </summary>
        public virtual ICollection<CoursePreReq> CoursePreReqs3 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_Courses_Courses_SubstitutedCourseID</para>
        /// <para>Foreign Key Base Table: [dbo].[Courses]</para>
        /// <para>Foreign Key Base Columns: [SubstitutedCourseID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Courses]</para>
        /// <para>Primary/Unique Key Base Columns: [CourseID]</para>
        /// </summary>
        public virtual ICollection<Cours> Courses1 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_Courses_Courses_SubstitutedCourseID</para>
        /// <para>Foreign Key Base Table: [dbo].[Courses]</para>
        /// <para>Foreign Key Base Columns: [SubstitutedCourseID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Courses]</para>
        /// <para>Primary/Unique Key Base Columns: [CourseID]</para>
        /// </summary>
        public virtual Cours Cours1 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_Courses_ProgramMajors_ProgramMajorID</para>
        /// <para>Foreign Key Base Table: [dbo].[Courses]</para>
        /// <para>Foreign Key Base Columns: [ProgramMajorID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[ProgramMajors]</para>
        /// <para>Primary/Unique Key Base Columns: [ProgramMajorID]</para>
        /// </summary>
        public virtual ProgramMajor ProgramMajor { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_Courses_UserLoginHistory_StatusChangedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[Courses]</para>
        /// <para>Foreign Key Base Columns: [StatusChangedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual UserLoginHistory UserLoginHistory { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_Courses_UserLoginHistory_SubstitutedByUserLoginHistoryID</para>
        /// <para>Foreign Key Base Table: [dbo].[Courses]</para>
        /// <para>Foreign Key Base Columns: [SubstitutedByUserLoginHistoryID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserLoginHistory]</para>
        /// <para>Primary/Unique Key Base Columns: [UserLoginHistoryID]</para>
        /// </summary>
        public virtual UserLoginHistory UserLoginHistory1 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_OfferedCourses_Courses_CourseID</para>
        /// <para>Foreign Key Base Table: [dbo].[OfferedCourses]</para>
        /// <para>Foreign Key Base Columns: [CourseID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Courses]</para>
        /// <para>Primary/Unique Key Base Columns: [CourseID]</para>
        /// </summary>
        public virtual ICollection<OfferedCours> OfferedCourses { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_RegisteredCourses_Courses_CourseID</para>
        /// <para>Foreign Key Base Table: [dbo].[RegisteredCourses]</para>
        /// <para>Foreign Key Base Columns: [CourseID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Courses]</para>
        /// <para>Primary/Unique Key Base Columns: [CourseID]</para>
        /// </summary>
        public virtual ICollection<RegisteredCours> RegisteredCourses { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_StudentCreditsTransferredCourses_Courses_EquivalentCourseID</para>
        /// <para>Foreign Key Base Table: [dbo].[StudentCreditsTransferredCourses]</para>
        /// <para>Foreign Key Base Columns: [EquivalentCourseID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Courses]</para>
        /// <para>Primary/Unique Key Base Columns: [CourseID]</para>
        /// </summary>
        public virtual ICollection<StudentCreditsTransferredCours> StudentCreditsTransferredCourses { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_StudentExemptedCourses_Courses_CourseID</para>
        /// <para>Foreign Key Base Table: [dbo].[StudentExemptedCourses]</para>
        /// <para>Foreign Key Base Columns: [CourseID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Courses]</para>
        /// <para>Primary/Unique Key Base Columns: [CourseID]</para>
        /// </summary>
        public virtual ICollection<StudentExemptedCours> StudentExemptedCourses { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_StudentExemptedCourses_Courses_SubstitutedCourseID</para>
        /// <para>Foreign Key Base Table: [dbo].[StudentExemptedCourses]</para>
        /// <para>Foreign Key Base Columns: [SubstitutedCourseID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Courses]</para>
        /// <para>Primary/Unique Key Base Columns: [CourseID]</para>
        /// </summary>
        public virtual ICollection<StudentExemptedCours> StudentExemptedCourses1 { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_TransferredStudentCourseMappings_Courses_ToCourseID</para>
        /// <para>Foreign Key Base Table: [dbo].[TransferredStudentCourseMappings]</para>
        /// <para>Foreign Key Base Columns: [ToCourseID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Courses]</para>
        /// <para>Primary/Unique Key Base Columns: [CourseID]</para>
        /// </summary>
        public virtual ICollection<TransferredStudentCourseMapping> TransferredStudentCourseMappings { get; set; }
    }
}
