//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Model.Entities
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// todo: Description is missing for [dbo].[UserGroups] table.
    /// </summary>
    public partial class UserGroup
    {
        public UserGroup()
        {
            this.UserGroupMenuLinks = new HashSet<UserGroupMenuLink>();
            this.UserGroupPermissions = new HashSet<UserGroupPermission>();
            this.UserRoles = new HashSet<UserRole>();
        }
    
        public int UserGroupID { get; set; }
        public Nullable<int> InstituteID { get; set; }
        public short UserType { get; set; }
        public string GroupName { get; set; }
        public bool IsDeleted { get; set; }
    
        /// <summary>
        /// <para>Foreign Key Name: FK_UserGroups_Institutes_InstituteID</para>
        /// <para>Foreign Key Base Table: [dbo].[UserGroups]</para>
        /// <para>Foreign Key Base Columns: [InstituteID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[Institutes]</para>
        /// <para>Primary/Unique Key Base Columns: [InstituteID]</para>
        /// </summary>
        public virtual Institute Institute { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_UserGroupMenuLinks_UserGroups_UserGroupID</para>
        /// <para>Foreign Key Base Table: [dbo].[UserGroupMenuLinks]</para>
        /// <para>Foreign Key Base Columns: [UserGroupID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserGroups]</para>
        /// <para>Primary/Unique Key Base Columns: [UserGroupID]</para>
        /// </summary>
        public virtual ICollection<UserGroupMenuLink> UserGroupMenuLinks { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_UserGroupPermissions_UserGroups_UserGroupID</para>
        /// <para>Foreign Key Base Table: [dbo].[UserGroupPermissions]</para>
        /// <para>Foreign Key Base Columns: [UserGroupID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserGroups]</para>
        /// <para>Primary/Unique Key Base Columns: [UserGroupID]</para>
        /// </summary>
        public virtual ICollection<UserGroupPermission> UserGroupPermissions { get; set; }
        /// <summary>
        /// <para>Foreign Key Name: FK_UserRoles_UserGroups_UserGroupID</para>
        /// <para>Foreign Key Base Table: [dbo].[UserRoles]</para>
        /// <para>Foreign Key Base Columns: [UserGroupID]</para>
        /// <para>Primary/Unique Key Base Table: [dbo].[UserGroups]</para>
        /// <para>Primary/Unique Key Base Columns: [UserGroupID]</para>
        /// </summary>
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
