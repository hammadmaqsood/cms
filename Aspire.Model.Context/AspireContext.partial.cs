﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices;

namespace Aspire.Model.Context
{
	public partial class AspireContext
	{
		private readonly bool _transactionCreated;
		private bool _transactionHasUncommittedChanges;

		public AspireContext(bool createTransaction, string nameOrConnectionString = null)
			: base(nameOrConnectionString ?? "name=AspireContext")
		{
			if (createTransaction)
			{
				this.Database.BeginTransaction();
				this._transactionCreated = true;
				this._transactionHasUncommittedChanges = false;
			}
		}

		private static void TransactionHasUncommittedChanges()
		{
			throw new InvalidOperationException("Transaction has uncommitted changes.");
		}

		protected override void Dispose(bool disposing)
		{
			if (this._transactionCreated)
			{
				this.Database.CurrentTransaction?.Rollback();
				if (this._transactionHasUncommittedChanges && Marshal.GetExceptionCode() == 0)
				{
					base.Dispose(disposing);
					TransactionHasUncommittedChanges();
					return;
				}
			}
			base.Dispose(disposing);
		}

		public void IncreaseCommandTimeout()
		{
			this.Database.CommandTimeout = (int?)TimeSpan.FromMinutes(3).TotalSeconds;
		}

		private void SetContextInfo(int userLoginHistoryID, bool loggingEnabled)
		{
			var connectionState = this.Database.Connection.State;
			if (connectionState == System.Data.ConnectionState.Closed)
				this.Database.Connection.Open();

			var paramLogging = new SqlParameter("@Logging", System.Data.SqlDbType.Bit) { Value = loggingEnabled };
			var paramUserLoginHistoryID = new SqlParameter("@UserLoginHistoryID", System.Data.SqlDbType.Int) { Value = userLoginHistoryID };
			this.Database.ExecuteSqlCommand(@"EXEC sys.sp_set_session_context @key=N'Logging',@value=@Logging,@readonly=0;
EXEC sys.sp_set_session_context @key=N'UserLoginHistoryID',@value=@UserLoginHistoryID,@readonly=0;
", paramLogging, paramUserLoginHistoryID);
		}

		public int SaveChanges(int userLoginHistoryID, bool loggingEnabled)
		{
			this.SetContextInfo(userLoginHistoryID, loggingEnabled);
			if (this._transactionCreated)
				this._transactionHasUncommittedChanges = true;
			try
			{
				return base.SaveChanges();
			}
			catch (Exception exc)
			{
				var messages = this.GetValidationErrors()
									?.SelectMany(e => e.ValidationErrors.Select(ve => $"{ve.PropertyName}: {ve.ErrorMessage}"))
									.Distinct()
									.ToList()
								?? new List<string>();
				var innerExceptions = 10;
				var exception = exc;
				do
				{
					messages.Add($"Exception Message: {exception.Message}");
					messages.Add($"Exception StackTrace: {exception.StackTrace}");
					exception = exception.InnerException;
				} while (exception != null && --innerExceptions > 0);
				throw new AspireContextException(messages);
			}
		}

		public int SaveChanges(int userLoginHistoryID)
		{
			return this.SaveChanges(userLoginHistoryID, true);
		}

		public int SaveChangesAsSystemUser(bool loggingEnabled)
		{
			return this.SaveChanges(1, loggingEnabled);
		}

		public int SaveChangesAsSystemUser()
		{
			return this.SaveChangesAsSystemUser(true);
		}

		public void CommitTransaction()
		{
			this.Database.CurrentTransaction.Commit();
			this._transactionHasUncommittedChanges = false;
		}

		public void RollbackTransaction()
		{
			this.Database.CurrentTransaction?.Rollback();
			this._transactionHasUncommittedChanges = false;
		}

		public int SaveChangesAndCommitTransaction(int userLoginHistoryID)
		{
			if (!this._transactionCreated)
				throw new InvalidOperationException("SaveChangesAndCommitTransaction cannot be called without transactions.");
			var result = this.SaveChanges(userLoginHistoryID);
			this.CommitTransaction();
			return result;
		}

		public int SaveChangesAndCommitTransaction(int userLoginHistoryID, bool loggingEnabled)
		{
			if (!this._transactionCreated)
				throw new InvalidOperationException("SaveChangesAndCommitTransaction cannot be called without transactions.");
			var result = this.SaveChanges(userLoginHistoryID, loggingEnabled);
			this.CommitTransaction();
			return result;
		}

		public int SaveChangesAsSystemUserAndCommitTransaction(bool loggingEnabled)
		{
			if (!this._transactionCreated)
				throw new InvalidOperationException("SaveChangesAsSystemUserAndCommitTransaction cannot be called without transactions.");
			var result = this.SaveChangesAsSystemUser(loggingEnabled);
			this.CommitTransaction();
			return result;
		}

		public int SaveChangesAsSystemUserAndCommitTransaction()
		{
			if (!this._transactionCreated)
				throw new InvalidOperationException("SaveChangesAsSystemUserAndCommitTransaction cannot be called without transactions.");
			var result = this.SaveChangesAsSystemUser();
			this.CommitTransaction();
			return result;
		}

		public void DemandTransaction()
		{
			if (!this._transactionCreated)
				throw new InvalidOperationException("Transaction is not started yet.");
		}
	}
}
