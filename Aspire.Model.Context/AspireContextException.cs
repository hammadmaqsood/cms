﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Aspire.Model.Context
{
	[Serializable]
	public sealed class AspireContextException : Exception
	{
		public AspireContextException(IEnumerable<string> errors) : base(string.Join(Environment.NewLine, errors))
		{
		}

		private AspireContextException(SerializationInfo info, StreamingContext context)
			: base(info, context) { }
	}
}