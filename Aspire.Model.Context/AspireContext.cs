﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aspire.Model.Context
{
    using Aspire.Model.Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class AspireContext : DbContext
    {
        public AspireContext()
            : base("name=AspireContext")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<AccountTransaction> AccountTransactions { get; set; }
        public virtual DbSet<AdmissionCriteria> AdmissionCriterias { get; set; }
        public virtual DbSet<AdmissionCriteriaPreReqDegree> AdmissionCriteriaPreReqDegrees { get; set; }
        public virtual DbSet<AdmissionCriteriaPreReqDegreeSubject> AdmissionCriteriaPreReqDegreeSubjects { get; set; }
        public virtual DbSet<AdmissionCriteriaPreReq> AdmissionCriteriaPreReqs { get; set; }
        public virtual DbSet<AdmissionOpenProgramEquivalent> AdmissionOpenProgramEquivalents { get; set; }
        public virtual DbSet<AdmissionOpenProgramExamMarksPolicy> AdmissionOpenProgramExamMarksPolicies { get; set; }
        public virtual DbSet<AdmissionOpenProgram> AdmissionOpenPrograms { get; set; }
        public virtual DbSet<AlumniJobPosition> AlumniJobPositions { get; set; }
        public virtual DbSet<Bank> Banks { get; set; }
        public virtual DbSet<BUResult> BUResults { get; set; }
        public virtual DbSet<BUResultSemesterCours> BUResultSemesterCourses { get; set; }
        public virtual DbSet<BUResultSemester> BUResultSemesters { get; set; }
        public virtual DbSet<CancelledAdmission> CancelledAdmissions { get; set; }
        public virtual DbSet<CandidateAppliedProgramAcademicRecord> CandidateAppliedProgramAcademicRecords { get; set; }
        public virtual DbSet<CandidateAppliedProgram> CandidateAppliedPrograms { get; set; }
        public virtual DbSet<CandidateForeignAcademicRecord> CandidateForeignAcademicRecords { get; set; }
        public virtual DbSet<CandidateForeignDocument> CandidateForeignDocuments { get; set; }
        public virtual DbSet<Candidate> Candidates { get; set; }
        public virtual DbSet<CandidateTemp> CandidateTemps { get; set; }
        public virtual DbSet<CBTLab> CBTLabs { get; set; }
        public virtual DbSet<CBTSessionLab> CBTSessionLabs { get; set; }
        public virtual DbSet<CBTSession> CBTSessions { get; set; }
        public virtual DbSet<ClassAttendanceSetting> ClassAttendanceSettings { get; set; }
        public virtual DbSet<ComplaintComment> ComplaintComments { get; set; }
        public virtual DbSet<Complaint> Complaints { get; set; }
        public virtual DbSet<ComplaintType> ComplaintTypes { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<CourseEquivalent> CourseEquivalents { get; set; }
        public virtual DbSet<CoursePreReq> CoursePreReqs { get; set; }
        public virtual DbSet<CourseRegistrationSetting> CourseRegistrationSettings { get; set; }
        public virtual DbSet<Cours> Courses { get; set; }
        public virtual DbSet<CustomField> CustomFields { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<DisciplineCaseActivity> DisciplineCaseActivities { get; set; }
        public virtual DbSet<DisciplineCas> DisciplineCases { get; set; }
        public virtual DbSet<DisciplineCasesStudent> DisciplineCasesStudents { get; set; }
        public virtual DbSet<DisciplineCaseStudentActivity> DisciplineCaseStudentActivities { get; set; }
        public virtual DbSet<Employer> Employers { get; set; }
        public virtual DbSet<EnumValue> EnumValues { get; set; }
        public virtual DbSet<ExamBuilding> ExamBuildings { get; set; }
        public virtual DbSet<ExamConduct> ExamConducts { get; set; }
        public virtual DbSet<ExamDateAndSession> ExamDateAndSessions { get; set; }
        public virtual DbSet<ExamDate> ExamDates { get; set; }
        public virtual DbSet<ExamDateSheet> ExamDateSheets { get; set; }
        public virtual DbSet<ExamDesignation> ExamDesignations { get; set; }
        public virtual DbSet<ExamInvigilatorDuty> ExamInvigilatorDuties { get; set; }
        public virtual DbSet<ExamInvigilatorExamSession> ExamInvigilatorExamSessions { get; set; }
        public virtual DbSet<ExamInvigilator> ExamInvigilators { get; set; }
        public virtual DbSet<ExamMarkingSchemeDetail> ExamMarkingSchemeDetails { get; set; }
        public virtual DbSet<ExamMarkingScheme> ExamMarkingSchemes { get; set; }
        public virtual DbSet<ExamMarksGradingSchemeGradeRanx> ExamMarksGradingSchemeGradeRanges { get; set; }
        public virtual DbSet<ExamMarksGradingScheme> ExamMarksGradingSchemes { get; set; }
        public virtual DbSet<ExamMarksPolicy> ExamMarksPolicies { get; set; }
        public virtual DbSet<ExamMarksPolicyProgram> ExamMarksPolicyPrograms { get; set; }
        public virtual DbSet<ExamMarksSetting> ExamMarksSettings { get; set; }
        public virtual DbSet<ExamOfferedRoom> ExamOfferedRooms { get; set; }
        public virtual DbSet<ExamRemarksPolicy> ExamRemarksPolicies { get; set; }
        public virtual DbSet<ExamRemarksPolicyDetail> ExamRemarksPolicyDetails { get; set; }
        public virtual DbSet<ExamRoom> ExamRooms { get; set; }
        public virtual DbSet<ExamSeat> ExamSeats { get; set; }
        public virtual DbSet<ExamSession> ExamSessions { get; set; }
        public virtual DbSet<Faculty> Faculties { get; set; }
        public virtual DbSet<FacultyMemberPinCode> FacultyMemberPinCodes { get; set; }
        public virtual DbSet<FacultyMember> FacultyMembers { get; set; }
        public virtual DbSet<FeeConcessionType> FeeConcessionTypes { get; set; }
        public virtual DbSet<FeeDefaulterJobSemester> FeeDefaulterJobSemesters { get; set; }
        public virtual DbSet<FeeDueDate> FeeDueDates { get; set; }
        public virtual DbSet<FeeHead> FeeHeads { get; set; }
        public virtual DbSet<FeeStructureDetail> FeeStructureDetails { get; set; }
        public virtual DbSet<FeeStructure> FeeStructures { get; set; }
        public virtual DbSet<Form> Forms { get; set; }
        public virtual DbSet<FormStudentClearanceDepartmentStatus> FormStudentClearanceDepartmentStatuses { get; set; }
        public virtual DbSet<FormStudentClearance> FormStudentClearances { get; set; }
        public virtual DbSet<GraduateDirectoryExtraCurricularActivity> GraduateDirectoryExtraCurricularActivities { get; set; }
        public virtual DbSet<GraduateDirectoryFormProject> GraduateDirectoryFormProjects { get; set; }
        public virtual DbSet<GraduateDirectoryForm> GraduateDirectoryForms { get; set; }
        public virtual DbSet<GraduateDirectoryFormSkill> GraduateDirectoryFormSkills { get; set; }
        public virtual DbSet<GraduateDirectoryFormVolunteerWork> GraduateDirectoryFormVolunteerWorks { get; set; }
        public virtual DbSet<GraduateDirectoryFormWorkExperience> GraduateDirectoryFormWorkExperiences { get; set; }
        public virtual DbSet<GraduateDirectoryFormWorkshop> GraduateDirectoryFormWorkshops { get; set; }
        public virtual DbSet<InstituteBankAccount> InstituteBankAccounts { get; set; }
        public virtual DbSet<InstituteBuilding> InstituteBuildings { get; set; }
        public virtual DbSet<Institute> Institutes { get; set; }
        public virtual DbSet<InstituteSemester> InstituteSemesters { get; set; }
        public virtual DbSet<IntegratedServiceUser> IntegratedServiceUsers { get; set; }
        public virtual DbSet<IPAddress> IPAddresses { get; set; }
        public virtual DbSet<OfferedCourseAttendanceDetail> OfferedCourseAttendanceDetails { get; set; }
        public virtual DbSet<OfferedCourseAttendance> OfferedCourseAttendances { get; set; }
        public virtual DbSet<OfferedCourseClass> OfferedCourseClasses { get; set; }
        public virtual DbSet<OfferedCourseExam> OfferedCourseExams { get; set; }
        public virtual DbSet<OfferedCours> OfferedCourses { get; set; }
        public virtual DbSet<OfficialDocument> OfficialDocuments { get; set; }
        public virtual DbSet<OnlineTeachingReadiness> OnlineTeachingReadinesses { get; set; }
        public virtual DbSet<PasswordChangeHistory> PasswordChangeHistories { get; set; }
        public virtual DbSet<PgpSupervisor> PgpSupervisors { get; set; }
        public virtual DbSet<PgpThesi> PgpThesis { get; set; }
        public virtual DbSet<PgpThesisSupervisor> PgpThesisSupervisors { get; set; }
        public virtual DbSet<PgpThesisTitle> PgpThesisTitles { get; set; }
        public virtual DbSet<ProgramMajor> ProgramMajors { get; set; }
        public virtual DbSet<ProgramResearchTheme> ProgramResearchThemes { get; set; }
        public virtual DbSet<Program> Programs { get; set; }
        public virtual DbSet<ProjectThesisInternshipExaminer> ProjectThesisInternshipExaminers { get; set; }
        public virtual DbSet<ProjectThesisInternshipMark> ProjectThesisInternshipMarks { get; set; }
        public virtual DbSet<ProjectThesisInternship> ProjectThesisInternships { get; set; }
        public virtual DbSet<Prospectus> Prospectuses { get; set; }
        public virtual DbSet<ProspectusProgramCours> ProspectusProgramCourses { get; set; }
        public virtual DbSet<ProspectusProgramCoursesPreReq> ProspectusProgramCoursesPreReqs { get; set; }
        public virtual DbSet<ProspectusProgramMajor> ProspectusProgramMajors { get; set; }
        public virtual DbSet<ProspectusProgram> ProspectusPrograms { get; set; }
        public virtual DbSet<QASummaryFacultyEvaluationDetail> QASummaryFacultyEvaluationDetails { get; set; }
        public virtual DbSet<QASummaryFacultyEvaluation> QASummaryFacultyEvaluations { get; set; }
        public virtual DbSet<QASurveyConduct> QASurveyConducts { get; set; }
        public virtual DbSet<QASurveySummaryAnalysi> QASurveySummaryAnalysis { get; set; }
        public virtual DbSet<QASurveyUser> QASurveyUsers { get; set; }
        public virtual DbSet<RecurringTask> RecurringTasks { get; set; }
        public virtual DbSet<RegisteredCourseCustomMark> RegisteredCourseCustomMarks { get; set; }
        public virtual DbSet<RegisteredCourseExamMark> RegisteredCourseExamMarks { get; set; }
        public virtual DbSet<RegisteredCours> RegisteredCourses { get; set; }
        public virtual DbSet<RequestedCours> RequestedCourses { get; set; }
        public virtual DbSet<ResearchThemeArea> ResearchThemeAreas { get; set; }
        public virtual DbSet<ResearchTheme> ResearchThemes { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<ScholarshipAnnouncementProgram> ScholarshipAnnouncementPrograms { get; set; }
        public virtual DbSet<ScholarshipAnnouncement> ScholarshipAnnouncements { get; set; }
        public virtual DbSet<ScholarshipApplicationApproval> ScholarshipApplicationApprovals { get; set; }
        public virtual DbSet<ScholarshipApplication> ScholarshipApplications { get; set; }
        public virtual DbSet<Scholarship> Scholarships { get; set; }
        public virtual DbSet<ScholarshipStudent> ScholarshipStudents { get; set; }
        public virtual DbSet<ScholarshipWorkload> ScholarshipWorkloads { get; set; }
        public virtual DbSet<Semester> Semesters { get; set; }
        public virtual DbSet<StudentAcademicRecord> StudentAcademicRecords { get; set; }
        public virtual DbSet<StudentCommunityService> StudentCommunityServices { get; set; }
        public virtual DbSet<StudentCreditsTransferredCours> StudentCreditsTransferredCourses { get; set; }
        public virtual DbSet<StudentCreditsTransfer> StudentCreditsTransfers { get; set; }
        public virtual DbSet<StudentCustomFieldValue> StudentCustomFieldValues { get; set; }
        public virtual DbSet<StudentCustomInfo> StudentCustomInfos { get; set; }
        public virtual DbSet<StudentDocument> StudentDocuments { get; set; }
        public virtual DbSet<StudentExemptedCours> StudentExemptedCourses { get; set; }
        public virtual DbSet<StudentFeeChallan> StudentFeeChallans { get; set; }
        public virtual DbSet<StudentFeeConcessionType> StudentFeeConcessionTypes { get; set; }
        public virtual DbSet<StudentFeeDetail> StudentFeeDetails { get; set; }
        public virtual DbSet<StudentFee> StudentFees { get; set; }
        public virtual DbSet<StudentLibraryDefaulter> StudentLibraryDefaulters { get; set; }
        public virtual DbSet<StudentProgramMajor> StudentProgramMajors { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<StudentSemester> StudentSemesters { get; set; }
        public virtual DbSet<SurveyConduct> SurveyConducts { get; set; }
        public virtual DbSet<SurveyQuestionGroup> SurveyQuestionGroups { get; set; }
        public virtual DbSet<SurveyQuestionOption> SurveyQuestionOptions { get; set; }
        public virtual DbSet<SurveyQuestion> SurveyQuestions { get; set; }
        public virtual DbSet<Survey> Surveys { get; set; }
        public virtual DbSet<SurveyUserAnswer> SurveyUserAnswers { get; set; }
        public virtual DbSet<SurveyUser> SurveyUsers { get; set; }
        public virtual DbSet<SystemFile> SystemFiles { get; set; }
        public virtual DbSet<SystemFolder> SystemFolders { get; set; }
        public virtual DbSet<TableName> TableNames { get; set; }
        public virtual DbSet<Tehsil> Tehsils { get; set; }
        public virtual DbSet<ThesisInternshipMark> ThesisInternshipMarks { get; set; }
        public virtual DbSet<ThesisInternship> ThesisInternships { get; set; }
        public virtual DbSet<TimeBarredStudent> TimeBarredStudents { get; set; }
        public virtual DbSet<TransferredStudentCourseMapping> TransferredStudentCourseMappings { get; set; }
        public virtual DbSet<TransferredStudent> TransferredStudents { get; set; }
        public virtual DbSet<UserFeedbackAssignee> UserFeedbackAssignees { get; set; }
        public virtual DbSet<UserFeedbackDiscussion> UserFeedbackDiscussions { get; set; }
        public virtual DbSet<UserFeedbackMember> UserFeedbackMembers { get; set; }
        public virtual DbSet<UserFeedback> UserFeedbacks { get; set; }
        public virtual DbSet<UserGroupMenuLink> UserGroupMenuLinks { get; set; }
        public virtual DbSet<UserGroupPermission> UserGroupPermissions { get; set; }
        public virtual DbSet<UserGroup> UserGroups { get; set; }
        public virtual DbSet<UserLoginHistory> UserLoginHistories { get; set; }
        public virtual DbSet<UserPreference> UserPreferences { get; set; }
        public virtual DbSet<UserRoleInstitute> UserRoleInstitutes { get; set; }
        public virtual DbSet<UserRoleModule> UserRoleModules { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<WithholdingTaxSetting> WithholdingTaxSettings { get; set; }
        public virtual DbSet<DDLLog> DDLLogs { get; set; }
        public virtual DbSet<DMLLogDetail> DMLLogDetails { get; set; }
        public virtual DbSet<DMLLog> DMLLogs { get; set; }
        public virtual DbSet<EmailAttachment> EmailAttachments { get; set; }
        public virtual DbSet<EmailRecipient> EmailRecipients { get; set; }
        public virtual DbSet<Email> Emails { get; set; }
        public virtual DbSet<EmailSendAttempt> EmailSendAttempts { get; set; }
        public virtual DbSet<FeePaymentLog> FeePaymentLogs { get; set; }
        public virtual DbSet<IntegratedServiceLog> IntegratedServiceLogs { get; set; }
        public virtual DbSet<MarksSheetChanx> MarksSheetChanges { get; set; }
        public virtual DbSet<ProjectThesisInternshipChanx> ProjectThesisInternshipChanges { get; set; }
        public virtual DbSet<SMTPServer> SMTPServers { get; set; }
        public virtual DbSet<StudentFeeDefaulterEmail> StudentFeeDefaulterEmails { get; set; }
        public virtual DbSet<UsageHistory> UsageHistories { get; set; }
        public virtual DbSet<WebException> WebExceptions { get; set; }
        public virtual DbSet<ViewActiveAdminUserLogin> ViewActiveAdminUserLogins { get; set; }
        public virtual DbSet<ViewActiveCandidateUserLogin> ViewActiveCandidateUserLogins { get; set; }
        public virtual DbSet<ViewActiveExecutiveUserLoginGroupPermission> ViewActiveExecutiveUserLoginGroupPermissions { get; set; }
        public virtual DbSet<ViewActiveExecutiveUserLoginGroupPermissionsModule> ViewActiveExecutiveUserLoginGroupPermissionsModules { get; set; }
        public virtual DbSet<ViewActiveExecutiveUserLogin> ViewActiveExecutiveUserLogins { get; set; }
        public virtual DbSet<ViewActiveFacultyUserLogin> ViewActiveFacultyUserLogins { get; set; }
        public virtual DbSet<ViewActiveStaffUserLoginGroupPermission> ViewActiveStaffUserLoginGroupPermissions { get; set; }
        public virtual DbSet<ViewActiveStaffUserLoginGroupPermissionsModule> ViewActiveStaffUserLoginGroupPermissionsModules { get; set; }
        public virtual DbSet<ViewActiveStaffUserLogin> ViewActiveStaffUserLogins { get; set; }
        public virtual DbSet<ViewActiveStudentUserLogin> ViewActiveStudentUserLogins { get; set; }
        public virtual DbSet<ViewActiveUserLoginHistory> ViewActiveUserLoginHistories { get; set; }
        public virtual DbSet<ViewActiveUserRoleLoginHistory> ViewActiveUserRoleLoginHistories { get; set; }
        public virtual DbSet<QAStudentOnlineTeachingSurvey> QAStudentOnlineTeachingSurveys { get; set; }
    
        [DbFunction("AspireContext", "GetAllExamOfferedRoomSeats")]
        public virtual IQueryable<GetAllExamOfferedRoomSeats_Result> GetAllExamOfferedRoomSeats(Nullable<int> examConductID, Nullable<byte> roomUsageType)
        {
            var examConductIDParameter = examConductID.HasValue ?
                new ObjectParameter("ExamConductID", examConductID) :
                new ObjectParameter("ExamConductID", typeof(int));
    
            var roomUsageTypeParameter = roomUsageType.HasValue ?
                new ObjectParameter("RoomUsageType", roomUsageType) :
                new ObjectParameter("RoomUsageType", typeof(byte));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<GetAllExamOfferedRoomSeats_Result>("[AspireContext].[GetAllExamOfferedRoomSeats](@ExamConductID, @RoomUsageType)", examConductIDParameter, roomUsageTypeParameter);
        }
    
        public virtual ObjectResult<Nullable<int>> GetNextAdmissionProcessingFeeChallanNo()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("GetNextAdmissionProcessingFeeChallanNo");
        }
    
        public virtual ObjectResult<Nullable<int>> GetNextStudentFeeChallanNo()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("GetNextStudentFeeChallanNo");
        }
    }
}
