using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspire.Web.Common
{
	public sealed class AspirePermissionsNotFound : Exception
	{
		public AspirePermissionType PermissionType { get; }
		public UserGroupPermission.PermissionValues? PermissionValueEnum { get; }
		public IReadOnlyCollection<UserGroupPermission.PermissionValues> RequiredAnyPermissionValueEnum { get; }

		public AspirePermissionsNotFound(AspirePermissionType permissionType, UserGroupPermission.PermissionValues? permissionValueEnum, UserGroupPermission.PermissionValues requiredPermissionValueEnum)
		{
			this.PermissionType = permissionType;
			this.PermissionValueEnum = permissionValueEnum;
			this.RequiredAnyPermissionValueEnum = new List<UserGroupPermission.PermissionValues> { requiredPermissionValueEnum }.AsReadOnly();
		}

		public AspirePermissionsNotFound(AspirePermissionType permissionType, UserGroupPermission.PermissionValues? permissionValueEnum, params UserGroupPermission.PermissionValues[] requiredPermissionValueEnums)
		{
			this.PermissionType = permissionType;
			this.PermissionValueEnum = permissionValueEnum;
			this.RequiredAnyPermissionValueEnum = requiredPermissionValueEnums.ToList().AsReadOnly();
		}

		public override string ToString()
		{
#if DEBUG
			return $"Permissions Not Found. " +
				   $"Module: {this.PermissionType.ModuleEnum}, " +
				   $"Type: {this.PermissionType.PermissionTypeEnum}, " +
				   $"Found Permission: {this.PermissionValueEnum?.ToString() ?? "N/A"}, " +
				   $"Required: [{string.Join("|", this.RequiredAnyPermissionValueEnum)}]";
#else
			return @"You are not authorized to perform the selected task.";
#endif
		}
	}
}