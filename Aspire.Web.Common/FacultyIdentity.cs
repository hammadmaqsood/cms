using Aspire.Model.Entities.Common;
using System;
using System.Runtime.Serialization;

namespace Aspire.Web.Common
{
	[Serializable]
	public sealed class FacultyIdentity : AspireIdentity, IFacultyIdentity
	{
		public int FacultyMemberID { get; }

		public int InstituteID { get; }

		public FacultyIdentity(string username, string name, string email, int userLoginHistoryID, Guid userLoginSessionGuid, int instituteID, string instituteName, string instituteShortName, string instituteAlias, int facultyMemberID, string ipAddress, string userAgent)
			: base(username, name, email, UserTypes.Faculty, SubUserTypes.None, userLoginHistoryID, userLoginSessionGuid, ipAddress, userAgent, instituteName, instituteShortName, instituteAlias)
		{
			this.FacultyMemberID = facultyMemberID;
			this.InstituteID = instituteID;
		}

		public FacultyIdentity(SerializationInfo serializationInfo, StreamingContext streamingContext)
			: base(serializationInfo, streamingContext)
		{
			this.FacultyMemberID = GetValue<int>(serializationInfo, nameof(this.FacultyMemberID));
			this.InstituteID = GetValue<int>(serializationInfo, nameof(this.InstituteID));
		}

		public new void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			base.GetObjectData(serializationInfo, streamingContext);
			serializationInfo.AddValue(nameof(this.FacultyMemberID), this.FacultyMemberID);
			serializationInfo.AddValue(nameof(this.InstituteID), this.InstituteID);
		}

		public static new IFacultyIdentity Current => AspireIdentity.Current as IFacultyIdentity;
	}
}