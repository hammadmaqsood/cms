using Aspire.Model.Entities.Common;
using System.Security;

namespace Aspire.Web.Common
{
	public class AspireAccessDeniedException : SecurityException
	{
		public AspireAccessDeniedException(UserTypes userType, UserTypes loginScreenUserType)
		{
			this.UserType = userType;
			this.LoginScreenUserType = loginScreenUserType;
		}

		public AspireAccessDeniedException(UserTypes userType)
		{
			this.UserType = userType;
			this.LoginScreenUserType = null;
		}

		public UserTypes UserType { get; }

		public UserTypes? LoginScreenUserType { get; }
	}
}