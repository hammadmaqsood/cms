using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Runtime.Serialization;

namespace Aspire.Web.Common
{
	[Serializable]
	public sealed class AdminIdentity : AspireUserRoleIdentity, IAdminIdentity
	{
		public AdminIdentity(string username, string name, string email, UserTypes userTypeEnum, int userLoginHistoryID, Guid userLoginSessionGuid, int userID, int userRoleID, string ipAddress, string userAgent)
			: base(username, name, email, userTypeEnum, userLoginHistoryID, userLoginSessionGuid, userID, userRoleID, ipAddress, userAgent, "Bahria University", "Bahria University", "Bahria University")
		{
			if (!userTypeEnum.IsAdminOrMasterAdmin())
				throw new InvalidOperationException("UserType can only be Admin or MasterAdmin.");
		}

		public AdminIdentity(SerializationInfo serializationInfo, StreamingContext streamingContext)
			: base(serializationInfo, streamingContext)
		{
		}

		public Admin.AdminLoginHistory Demand(AdminPermissionType adminPermissionType, params UserGroupPermission.PermissionValues[] requiredPermissionValueEnums)
		{
			return Admin.DemandAdminPermissions(this.LoginSessionGuid, adminPermissionType, requiredPermissionValueEnums);
		}

		public static new IAdminIdentity Current => AspireUserRoleIdentity.Current as IAdminIdentity;
	}
}