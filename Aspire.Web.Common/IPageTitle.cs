using System.Web.UI.WebControls;

namespace Aspire.Web.Common
{
	public interface IPageTitle
	{
		bool PageTitleVisible { get; }
		PageIcon PageIcon { get; }
		string PageTitle { get; }
		HorizontalAlign PageTitleHorizontalAlign { get; }
	}
}