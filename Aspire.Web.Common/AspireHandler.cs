﻿using System.Web;
using System.Web.SessionState;

namespace Aspire.Web.Common
{
	public abstract class AspireHandler : IHttpHandler, IRequiresSessionState
	{
		protected HttpContext Context { get; private set; }

		public void ProcessRequest(HttpContext context)
		{
			this.Context = context;
			this.Authenticate();
			this.ProcessRequest();
		}

		protected abstract void ProcessRequest();

		protected abstract void Authenticate();

		public virtual bool IsReusable => false;
	}
}