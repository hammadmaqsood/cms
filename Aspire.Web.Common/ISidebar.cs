﻿namespace Aspire.Web.Common
{
	public interface ISidebar
	{
		bool SidebarVisible { get; }
		string SidebarHtml { get; }
		string BreadcrumbHtml { get; }
	}
}