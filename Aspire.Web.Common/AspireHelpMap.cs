﻿using System;
using System.Collections.Generic;

namespace Aspire.Web.Common
{
	public static class AspireHelpMap
	{
		private static readonly Dictionary<Type, string> Links = new Dictionary<Type, string>();

		public static void AddLink(this Type pageType, string helpUrl)
		{
			if (!pageType.IsSubclassOf(typeof(BasePage)))
				throw new InvalidOperationException();
			const string baseHelpUrl = "";
			Links.Add(pageType, baseHelpUrl + helpUrl);
		}

		internal static string GetHelpLink(this BasePage page)
		{
			var type = page.GetType().BaseType;
			if (type != null && Links.ContainsKey(type))
				return Links[type];
			return "#missing";
		}
	}
}
