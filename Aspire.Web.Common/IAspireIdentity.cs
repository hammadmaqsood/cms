﻿using Aspire.Model.Entities.Common;
using System;
using System.Runtime.Serialization;

namespace Aspire.Web.Common
{
	public interface IAspireIdentity : Lib.Helpers.IAspireIdentity, ISerializable
	{
		DateTime LastUpdatedOn { get; }

		string BrandName { get; }

		string BrandShortName { get; }

		string BrandAlias { get; }

		string Name { get; }

		string Username { get; }

		UserTypes UserType { get; }

		SubUserTypes SubUserType { get; }

		string Email { get; }

		int UserLoginHistoryID { get; }

		string IPAddress { get; }

		string UserAgent { get; }
	}
}