﻿namespace Aspire.Web.Common
{
	public interface IAspireUserRoleIdentity : IAspireIdentity
	{
		int UserID { get; }

		int UserRoleID { get; }
	}
}