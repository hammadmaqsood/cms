﻿using Aspire.Model.Entities.Common;

namespace Aspire.Web.Common
{
	public interface ICandidateIdentity : IAspireIdentity
	{
		int CandidateID { get; }

		bool ForeignStudent { get; }

		short SemesterID { get; }

		Genders GenderEnum { get; }
	}
}