﻿using Aspire.Lib.WebControls;
using System;
using System.Web;
using System.Web.SessionState;

namespace Aspire.Web.Common
{
	public static class SessionHelper
	{
		private static HttpSessionState Session => HttpContext.Current.Session;

		public static void DestroyCurrentUserSession(string redirectToUrl)
		{
			HttpContext.Current.Session.Abandon();
			HttpContext.Current.Response.Redirect(string.IsNullOrWhiteSpace(redirectToUrl) ? "~" : redirectToUrl);
		}

		public static void CreateUserSession(IAspireIdentity aspireIdentity, string redirectToUrl)
		{
			Session.Clear();
			AspireIdentity = aspireIdentity ?? throw new ArgumentNullException(nameof(aspireIdentity));
			HttpContext.Current.Response.Redirect(redirectToUrl);
		}

		private static readonly string AspireIdentitySessionName = $"${nameof(AspireIdentity)}$";

		internal static IAspireIdentity AspireIdentity
		{
			get => (IAspireIdentity)Session?[AspireIdentitySessionName];
			private set => Session[AspireIdentitySessionName] = value;
		}

		private static readonly string SideBarLinksSessionName = $"${nameof(SideBarLinks)}$";

		public static AspireSideBarLinks SideBarLinks
		{
			get => (AspireSideBarLinks)Session[SideBarLinksSessionName];
			private set => Session[SideBarLinksSessionName] = value;
		}

		private static readonly string SideBarLinkHtmlSessionName = $"${nameof(SideBarLinkHtml)}$";

		public static string SideBarLinkHtml
		{
			get => (string)Session[SideBarLinkHtmlSessionName];
			private set => Session[SideBarLinkHtmlSessionName] = value;
		}

		public static void SetSideBarLinks(AspireSideBarLinks sideBarLinks)
		{
			SideBarLinkHtml = sideBarLinks.ToHtml();
			SideBarLinks = sideBarLinks;
		}

		private static readonly string FacultyMemberPinCodeSessionName = $"${nameof(FacultyMemberPinCode)}$";

		public static string FacultyMemberPinCode
		{
			get => (string)Session[FacultyMemberPinCodeSessionName];
			private set => Session[FacultyMemberPinCodeSessionName] = value;
		}

		public static void SetFacultyMemberPinCodeVerified(string pinCode)
		{
			if (FacultyMemberPinCode != null)
				throw new InvalidOperationException("Session for Pin Code already set.");
			FacultyMemberPinCode = pinCode;
		}

		private static readonly string StudentDocumentsReadSessionName = $"${nameof(StudentDocumentsRead)}$";

		public static bool? StudentDocumentsRead
		{
			get => Session[StudentDocumentsReadSessionName] as bool?;
			private set => Session[StudentDocumentsReadSessionName] = value;
		}

		public static void SetStudentDocumentsRead()
		{
			if (StudentDocumentsRead != true)
				StudentDocumentsRead = true;
		}
	}
}
