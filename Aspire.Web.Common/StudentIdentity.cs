using Aspire.Lib.Extensions.Exceptions;
using Aspire.Model.Entities.Common;
using System;
using System.Runtime.Serialization;

namespace Aspire.Web.Common
{
	[Serializable]
	public sealed class StudentIdentity : AspireIdentity, IStudentIdentity
	{
		public int StudentID { get; }

		public string Enrollment { get; }

		public int InstituteID { get; }

		public string InstituteCode { get; }

		public SubUserTypes SubUserTypeEnum { get; }

		public StudentIdentity(string username, string name, string email, SubUserTypes subUserTypeEnum, int userLoginHistoryID, Guid userLoginSessionGuid, int instituteID, string instituteCode, string instituteName, string instituteShortName, string instituteAlias, int studentID, string enrollment, string ipAddress, string userAgent)
			: base(username, name, email, UserTypes.Student, subUserTypeEnum, userLoginHistoryID, userLoginSessionGuid, ipAddress, userAgent, instituteName, instituteShortName, instituteAlias)
		{
			this.StudentID = studentID;
			this.Enrollment = enrollment;
			this.InstituteID = instituteID;
			this.InstituteCode = instituteCode;

			switch (subUserTypeEnum)
			{
				case SubUserTypes.None:
				case SubUserTypes.Parents:
					this.SubUserTypeEnum = subUserTypeEnum;
					break;
				default:
					throw new NotImplementedEnumException(subUserTypeEnum);
			}
		}

		public StudentIdentity(SerializationInfo serializationInfo, StreamingContext streamingContext)
			: base(serializationInfo, streamingContext)
		{
			this.StudentID = GetValue<int>(serializationInfo, nameof(this.StudentID));
			this.Enrollment = GetValue<string>(serializationInfo, nameof(this.Enrollment));
			this.InstituteID = GetValue<int>(serializationInfo, nameof(this.InstituteID));
			this.InstituteCode = GetValue<string>(serializationInfo, nameof(this.InstituteCode));
			this.SubUserTypeEnum = GetValue<SubUserTypes>(serializationInfo, nameof(this.SubUserTypeEnum));
		}

		public override void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			base.GetObjectData(serializationInfo, streamingContext);
			serializationInfo.AddValue(nameof(this.StudentID), this.StudentID);
			serializationInfo.AddValue(nameof(this.Enrollment), this.Enrollment);
			serializationInfo.AddValue(nameof(this.InstituteID), this.InstituteID);
			serializationInfo.AddValue(nameof(this.InstituteCode), this.InstituteCode);
			serializationInfo.AddValue(nameof(this.SubUserTypeEnum), this.SubUserTypeEnum);
		}

		public static new IStudentIdentity Current => AspireIdentity.Current as IStudentIdentity;
	}
}