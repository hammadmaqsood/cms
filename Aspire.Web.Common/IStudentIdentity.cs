﻿using Aspire.Model.Entities.Common;

namespace Aspire.Web.Common
{
	public interface IStudentIdentity : IAspireIdentity
	{
		int StudentID { get; }

		string Enrollment { get; }

		int InstituteID { get; }

		string InstituteCode { get; }

		SubUserTypes SubUserTypeEnum { get; }
	}
}