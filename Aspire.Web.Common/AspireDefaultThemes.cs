using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Web.Common
{
	public static class AspireDefaultThemes
	{
		public static readonly AspireThemes MasterAdmin = AspireThemes.Fire;
		public static readonly AspireThemes Admin = AspireThemes.Cherry;
		public static readonly AspireThemes Candidate = AspireThemes.Deepsea;
		public static readonly AspireThemes Staff = AspireThemes.Asphalt;
		public static readonly AspireThemes Student = AspireThemes.Army;
		public static readonly AspireThemes Faculty = AspireThemes.City;
		public static readonly AspireThemes Executive = AspireThemes.Grass;

		public static AspireThemes GetDefaultThemeForCurrentUser()
		{
			switch (AspireIdentity.Current.UserType)
			{
				case UserTypes.Anonymous:
					break;
				case UserTypes.System:
					break;
				case UserTypes.Admin:
					return Admin;
				case UserTypes.MasterAdmin:
					return MasterAdmin;
				case UserTypes.Executive:
					return Executive;
				case UserTypes.Staff:
					return Staff;
				case UserTypes.Candidate:
					return Candidate;
				case UserTypes.Student:
					return Student;
				case UserTypes.Faculty:
					return Faculty;
				case UserTypes.Alumni:
					break;
				case UserTypes.Admins:
					break;
				case UserTypes.Any:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			throw new ArgumentOutOfRangeException();
		}
	}
}