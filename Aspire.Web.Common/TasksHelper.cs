﻿using Aspire.Lib.Extensions.Exceptions;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Aspire.Web.Common
{
	public static class TasksHelper
	{
		private static readonly CancellationTokenSource CancellationTokenSource = new CancellationTokenSource();
		private static readonly ConcurrentDictionary<Guid, Task> Tasks = new ConcurrentDictionary<Guid, Task>();
		private static readonly object TaskLock = new object();

		public static void AddTaskAndRun(Action<CancellationToken> action, TaskCreationOptions taskCreationOption = TaskCreationOptions.None)
		{
			lock (TaskLock)
			{
				if (CancellationTokenSource.IsCancellationRequested)
					throw new InvalidOperationException();
				var task = new Task(() => action(CancellationTokenSource.Token), taskCreationOption);
				var taskGuid = Guid.NewGuid();
				var added = Tasks.TryAdd(taskGuid, task);
				if (added == false)
					throw new InvalidOperationException();
				task.ContinueWith((t) =>
				{
					RunAfterCompletion(t, taskGuid);
				});
				task.Start();
			}
		}

		private static void RemoveTask(Task task, Guid taskGuid)
		{
			var removed = Tasks.TryRemove(taskGuid, out var _task);
			if (removed == false || task != _task)
				throw new InvalidOperationException();
			task.Dispose();
		}

		public static void RunAfterCompletion(Task task, Guid taskGuid)
		{
			switch (task.Status)
			{
				case TaskStatus.Created:
				case TaskStatus.WaitingForActivation:
				case TaskStatus.WaitingToRun:
				case TaskStatus.Running:
				case TaskStatus.WaitingForChildrenToComplete:
					throw new InvalidOperationException();
				case TaskStatus.Faulted:
					BL.Core.Logs.Common.ErrorHandler.LogException(null, task.Exception, -1, "Task", null, null);
					RemoveTask(task, taskGuid);
					return;
				case TaskStatus.RanToCompletion:
				case TaskStatus.Canceled:
					RemoveTask(task, taskGuid);
					return;
				default:
					throw new NotImplementedEnumException(task.Status);
			}
		}

		public static void CancelAllTasksAndWait()
		{
			lock (TaskLock)
			{
				CancellationTokenSource.Cancel();
				Task.WaitAll(Tasks.Values.ToArray());
				CancellationTokenSource.Dispose();
			}
		}
	}
}
