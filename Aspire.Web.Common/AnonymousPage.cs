using Aspire.Model.Entities.Common;

namespace Aspire.Web.Common
{
	public abstract class AnonymousPage : AspireBasePage
	{
		protected override void Authenticate()
		{
			if (this.AspireIdentity != null)
				throw new AspireAccessDeniedException(UserTypes.Anonymous);
		}
	}
}