namespace Aspire.Web.Common
{
	public interface IProfileInfo
	{
		bool ProfileInfoVisible { get; }
		bool ProfileLinkVisible { get; }
		string ProfileNavigateUrl { get; }
		bool LogoffLinkVisible { get; }
		string LogoffNavigateUrl { get; }
		bool IsCustomProfileImage { get; }
		string CustomProfileImageUrl { get; }
		string Username { get; }
		string Name { get; }
		string Email { get; }
	}
}