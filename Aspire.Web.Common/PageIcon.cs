using Aspire.Lib.WebControls;
using System;

namespace Aspire.Web.Common
{
	public sealed class PageIcon
	{
		public AspireGlyphicons Glyphicon { get; }
		public FontAwesomeIcons FontAwesomeIcon { get; }

		public PageIcon(AspireGlyphicons glyphicon)
		{
			if (glyphicon == AspireGlyphicons.None)
				throw new InvalidOperationException("None cannot be specified. Specify null instead.");
			this.Glyphicon = glyphicon;
			this.FontAwesomeIcon = FontAwesomeIcons.None;
		}

		public PageIcon(FontAwesomeIcons fontAwesomeIcon)
		{
			if (fontAwesomeIcon == FontAwesomeIcons.None)
				throw new InvalidOperationException("None cannot be specified. Specify null instead.");
			this.Glyphicon = AspireGlyphicons.None;
			this.FontAwesomeIcon = fontAwesomeIcon;
		}

		public override string ToString()
		{
			if (this.Glyphicon != AspireGlyphicons.None)
				return this.Glyphicon.ToCssClass();
			if (this.FontAwesomeIcon != FontAwesomeIcons.None)
				return this.FontAwesomeIcon.ToCssClass();
			throw new InvalidOperationException();
		}
	}
}