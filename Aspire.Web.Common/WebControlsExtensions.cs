﻿using Aspire.BL.Entities.Common;
using Aspire.Lib.Extensions;
using Aspire.Lib.Helpers;
using Aspire.Lib.WebControls;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

namespace Aspire.Web.Common
{
	public static class WebControlsExtensions
	{
		#region Gridview, Repeater, ListView, etc.

		public static string DecryptedCommandArgument(this GridViewCommandEventArgs e)
		{
			return e.CommandArgument?.ToString().Decrypt();
		}

		public static int DecryptedCommandArgumentToInt(this GridViewCommandEventArgs e)
		{
			return e.DecryptedCommandArgument().ToInt();
		}

		public static Guid DecryptedCommandArgumentToGuid(this GridViewCommandEventArgs e)
		{
			return e.DecryptedCommandArgument().ToGuid();
		}

		public static string DecryptedCommandArgument(this RepeaterCommandEventArgs e)
		{
			return e.CommandArgument?.ToString().Decrypt();
		}

		public static int DecryptedCommandArgumentToInt(this RepeaterCommandEventArgs e)
		{
			return e.DecryptedCommandArgument().ToInt();
		}

		public static Guid DecryptedCommandArgumentToGuid(this RepeaterCommandEventArgs e)
		{
			return e.DecryptedCommandArgument().ToGuid();
		}

		public static Repeater DataBind(this Repeater repeater, object dataSource)
		{
			repeater.DataSource = dataSource;
			repeater.DataBind();
			return repeater;
		}

		public static ListView DataBind(this ListView lv, object dataSource)
		{
			lv.DataSource = dataSource;
			lv.DataBind();
			return lv;
		}

		public static System.Web.UI.WebControls.GridView DataBindNull(this System.Web.UI.WebControls.GridView gv)
		{
			gv.DataSource = null;
			gv.DataBind();
			return gv;
		}

		public static ListView DataBindNull(this ListView lv)
		{
			lv.DataSource = null;
			lv.DataBind();
			return lv;
		}

		#endregion

		#region Helper Extensions

		public static ListControl SetSelectedValueIfNotPostback(this ListControl listControl, string queryParamName)
		{
			if (!listControl.Page.IsPostBack)
			{
				var value = listControl.Page.Request.GetParameterValue(queryParamName);
				if (value != null)
				{
					var item = listControl.Items.FindByValue(value);
					if (item != null)
						listControl.SelectedValue = value;
				}
			}
			return listControl;
		}

		public static ListControl SetSelectedEnumValueIfNotPostback<T>(this ListControl listControl, string queryParamName) where T : struct
		{
			if (!listControl.Page.IsPostBack)
			{
				var value = listControl.Page.Request.GetParameterValue<T>(queryParamName);
				if (value != null)
				{
					var selectedValue = ((Enum)(object)value).ToUnderlyingType().ToString();
					var item = listControl.Items.FindByValue(selectedValue);
					if (item != null)
						listControl.SelectedValue = selectedValue;
				}
			}
			return listControl;
		}

		public static ListControl SetSelectedEnumGuidValueIfNotPostback<T>(this ListControl listControl, string queryParamName) where T : struct
		{
			if (!listControl.Page.IsPostBack)
			{
				var value = listControl.Page.Request.GetParameterValue<T>(queryParamName);
				if (value != null)
				{
					var selectedValue = ((Enum)(object)value).GetGuid();
					var item = listControl.Items.FindByValue(selectedValue.ToString());
					if (item != null)
						listControl.SelectedValue = selectedValue.ToString();
				}
			}
			return listControl;
		}

		public static ListControl SetSelectedBooleanValueIfNotPostback(this ListControl listControl, string queryParamName)
		{
			if (!listControl.Page.IsPostBack)
			{
				var value = listControl.Page.Request.GetParameterValue<bool>(queryParamName);
				if (value != null)
				{
					var item = listControl.Items.FindByValue(value.Value.ToOneZero());
					if (item != null)
						listControl.SetBooleanValue(value.Value);
				}
			}
			return listControl;
		}

		public static System.Web.UI.WebControls.TextBox SetTextIfNotPostback(this System.Web.UI.WebControls.TextBox tb, string queryParamName)
		{
			if (!tb.Page.IsPostBack)
			{
				var value = tb.Page.Request.GetParameterValue(queryParamName);
				if (value != null)
					tb.Text = value;
			}
			return tb;
		}

		private static IEnumerable<T> GetEnumValues<T>(params T[] enumValuesToBeExcluded) where T : struct, IConvertible
		{
			var type = typeof(T);
			if (!type.IsEnum)
				throw new InvalidOperationException("T must be enum.");
			return Enum.GetValues(type).Cast<T>().Where(e => !enumValuesToBeExcluded.Contains(e));
		}

		private static IEnumerable<ListItem> GetEnumValuesListItems<T>(params T[] enumValuesToBeExcluded) where T : struct, IConvertible
		{
			var baseType = Enum.GetUnderlyingType(typeof(T));
			return GetEnumValues(enumValuesToBeExcluded).Select(e => new ListItem
			{
				Text = e.ToString(CultureInfo.InvariantCulture).SplitCamelCasing(),
				Value = Convert.ChangeType(e, baseType)?.ToString()
			});
		}

		public static IEnumerable<ListItem> GetEnumGuidValuesListItems<T>(params T[] enumValuesToBeExcluded) where T : struct, Enum
		{
			return Enum.GetValues(typeof(T)).Cast<T>().Except(enumValuesToBeExcluded)
					.Select(e =>
					{
						var a = e.GetEnumGuidValueAttribute();
						return new ListItem(a.FullName, a.Guid.ToString());
					});
		}

		public static T GetSelectedEnumValue<T>(this ListControl listControl) where T : struct, IConvertible
		{
			return (T)Enum.Parse(typeof(T), listControl.SelectedValue);
		}

		public static T? GetSelectedNullableGuidEnum<T>(this ListControl listControl) where T : struct, Enum
		{
			return listControl.SelectedValue.ToNullableGuid()?.GetEnum<T>();
		}

		public static T GetSelectedGuidEnum<T>(this ListControl listControl) where T : Enum
		{
			return listControl.SelectedValue.ToGuid().GetEnum<T>();
		}

		public static List<T> GetSelectedGuidEnums<T>(this ListControl listControl) where T : Enum
		{
			return listControl.GetSelectedValues().Select(v => v.ToGuid().GetEnum<T>()).ToList();
		}

		public static ListControl SetSelectedGuidEnum<T>(this ListControl listControl, T @enum) where T : Enum
		{
			listControl.SelectedValue = @enum.GetGuid().ToString();
			return listControl;
		}

		public static ListControl SetSelectedGuidEnums<T>(this ListControl listControl, T enumFlags) where T : Enum
		{
			foreach (ListItem listItem in listControl.Items)
				listItem.Selected = enumFlags.HasFlag(listItem.Value.ToGuid().GetEnum());
			return listControl;
		}

		public static ListControl SetSelectedGuidEnums<T>(this ListControl listControl, IEnumerable<T> enumFlags) where T : Enum
		{
			foreach (ListItem listItem in listControl.Items)
				listItem.Selected = enumFlags.Contains(listItem.Value.ToGuid().GetEnum<T>());
			return listControl;
		}

		public static ListControl SetSelectedGuidEnum<T>(this ListControl listControl, T? @enum) where T : struct, Enum
		{
			if (@enum == null)
				listControl.SelectedValue = null;
			else
				listControl.SetSelectedGuidEnum(@enum.Value);
			return listControl;
		}

		public static IEnumerable<T> GetSelectedEnumValues<T>(this ListControl listControl) where T : struct, IConvertible
		{
			return listControl.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => (T)Enum.Parse(typeof(T), i.Value));
		}

		public static T? GetSelectedEnumValue<T>(this ListControl listControl, T? defaultValue) where T : struct, IConvertible
		{
			if (string.IsNullOrEmpty(listControl.SelectedValue))
				return defaultValue;
			return listControl.GetSelectedEnumValue<T>();
		}

		public static IEnumerable<string> GetSelectedValues(this ListControl listControl)
		{
			return listControl.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value);
		}

		public static ListControl SetSelectedValue(this ListControl listControl, string value)
		{
			listControl.SelectedValue = value;
			return listControl;
		}

		public static ListControl SetSelectedValueIfExists(this ListControl listControl, string value)
		{
			if (listControl.Items.FindByValue(value) != null)
				listControl.SelectedValue = value;
			return listControl;
		}

		public static ListControl SetSelectedValue(this ListControl listControl, string value, string defaultValue)
		{
			return listControl.SetSelectedValue(value ?? defaultValue);
		}

		public static ListControl SetSelectedValue(this ListControl listControl, bool value)
		{
			return listControl.SetSelectedValue(value ? CommonListItems.TrueValue : CommonListItems.NoValue);
		}

		public static ListControl SetSelectedValueYes(this ListControl listControl)
		{
			return listControl.SetSelectedValue(CommonListItems.YesValue);
		}

		public static ListControl SetSelectedValueNo(this ListControl listControl)
		{
			return listControl.SetSelectedValue(CommonListItems.NoValue);
		}

		public static ListControl SetEnumValue<T>(this ListControl listControl, T value) where T : struct, IConvertible
		{
			listControl.SelectedValue = Convert.ToInt32(value).ToString();
			return listControl;
		}

		public static ListControl SetEnumValue<T>(this ListControl listControl, T? value, T defaultValue) where T : struct, IConvertible
		{
			return listControl.SetEnumValue(value ?? defaultValue);
		}

		public static ListControl SetEnumValue<T>(this ListControl listControl, T? value) where T : struct, IConvertible
		{
			if (value == null)
				return listControl.SetSelectedValue(CommonListItems.EmptyValue);
			return listControl.SetSelectedValue(Convert.ToInt32(value.Value).ToString());
		}

		public static ListControl SetBooleanValue(this ListControl listControl, bool? value)
		{
			switch (value)
			{
				case true:
					return listControl.SetSelectedValue(CommonListItems.TrueValue);
				case null:
					return listControl.SetSelectedValue(CommonListItems.EmptyValue);
				default:
					return listControl.SetSelectedValue(CommonListItems.FalseValue);
			}
		}

		public static ListControl SelectAll(this ListControl listControl)
		{
			listControl.Items.Cast<ListItem>().ForEach(i => i.Selected = true);
			return listControl;
		}

		public static ListControl SelectNone(this ListControl listControl)
		{
			listControl.Items.Cast<ListItem>().ForEach(i => i.Selected = false);
			return listControl;
		}

		public static ListControl SetSelectedValues(this ListControl listControl, IEnumerable<string> selectedValues)
		{
			listControl.Items.Cast<ListItem>().ForEach(i => i.Selected = selectedValues.Contains(i.Value));
			return listControl;
		}

		public static ListControl AddStaticListItem(this ListControl listControl, ListItem staticListItem)
		{
			if (staticListItem != null)
				listControl.Items.Insert(0, staticListItem);
			return listControl;
		}

		public static ListControl DataBind(this ListControl listControl, ListItem item)
		{
			listControl.Items.Clear();
			listControl.Items.Add(item);
			return listControl;
		}

		public static ListControl DataBind(this ListControl listControl, IEnumerable<ListItem> items)
		{
			listControl.Items.Clear();
			listControl.Items.AddRange(items.ToArray());
			return listControl;
		}

		public static ListControl DataBind(this ListControl listControl, IEnumerable<ListItem> items, string selectedValue)
		{
			return listControl.DataBind(items).SetSelectedValue(selectedValue);
		}

		public static ListControl DataBind(this ListControl listControl, IEnumerable<ListItem> items, ListItem staticListItem)
		{
			return listControl.DataBind(items).AddStaticListItem(staticListItem);
		}

		private static ListControl DataBind(this IEnumerable<ListItem> items, ListControl listControl)
		{
			return listControl.DataBind(items);
		}

		private static ListControl DataBind(this IEnumerable<ListItem> items, ListControl listControl, ListItem staticListItem)
		{
			return listControl.DataBind(items, staticListItem);
		}

		public static ListControl DataBind(this ListControl listControl, IEnumerable<string> items)
		{
			return items.Select(i => new ListItem(i)).DataBind(listControl);
		}

		public static ListControl DataBind(this ListControl listControl, IEnumerable<string> items, ListItem staticListItem)
		{
			return listControl.DataBind(items).AddStaticListItem(staticListItem);
		}

		public static ListControl AddEnums<T>(this ListControl listControl, params T[] enumValues) where T : struct, IConvertible
		{
			var type = typeof(T);
			if (!type.IsEnum)
				throw new InvalidOperationException("T must be enum.");
			var baseType = Enum.GetUnderlyingType(typeof(T));
			var items = enumValues.Select(e => new ListItem
			{
				Text = e.ToString(CultureInfo.InvariantCulture),
				Value = Convert.ChangeType(e, baseType)?.ToString()
			});
			return listControl.DataBind(items);
		}

		public static ListControl AddEnums<T>(this ListControl listControl, ListItem staticListItem, params T[] enumValues) where T : struct, IConvertible
		{
			return listControl.AddEnums(enumValues).AddStaticListItem(staticListItem);
		}

		public static ListControl FillEnums<T>(this ListControl listControl, ListItem staticListItem, params T[] enumValuesToBeExcluded) where T : struct, IConvertible
		{
			return GetEnumValuesListItems(enumValuesToBeExcluded).DataBind(listControl, staticListItem);
		}

		public static ListControl FillEnums<T>(this ListControl listControl, params T[] enumValuesToBeExcluded) where T : struct, IConvertible
		{
			return GetEnumValuesListItems(enumValuesToBeExcluded).DataBind(listControl);
		}

		public static ListControl FillGuidEnums<T>(this ListControl listControl, ListItem staticListItem, params T[] enumValuesToBeExcluded) where T : struct, Enum
		{
			return GetEnumGuidValuesListItems(enumValuesToBeExcluded).DataBind(listControl, staticListItem);
		}

		public static ListControl FillGuidEnums<T>(this ListControl listControl, params T[] enumValuesToBeExcluded) where T : struct, Enum
		{
			return GetEnumGuidValuesListItems(enumValuesToBeExcluded).DataBind(listControl);
		}

		#endregion

		#region Database Common Lists

		public static ListControl DataBind(this ListControl listControl, IEnumerable<CustomListItem> items)
		{
			return items.Select(i => new ListItem(i.Text, i.ID.ToString())).DataBind(listControl);
		}

		public static ListControl DataBind(this ListControl listControl, IEnumerable<CustomListItem> items, ListItem staticListItem)
		{
			return listControl.DataBind(items).AddStaticListItem(staticListItem);
		}

		public static ListControl DataBind(this ListControl listControl, IEnumerable<CustomListItemGuid> items)
		{
			return items.Select(i => new ListItem(i.Text, i.ID.ToString())).DataBind(listControl);
		}

		public static ListControl DataBind(this ListControl listControl, IEnumerable<CustomListItemGuid> items, ListItem staticListItem)
		{
			return listControl.DataBind(items).AddStaticListItem(staticListItem);
		}

		public static ListControl DataBind(this IEnumerable<CustomListItem> items, ListControl listControl)
		{
			return listControl.DataBind(items);
		}

		public static ListControl DataBind(this IEnumerable<CustomListItem> items, ListControl listControl, ListItem staticListItem)
		{
			return listControl.DataBind(items, staticListItem);
		}

		private static readonly List<ListItem> Countries = BL.Core.Common.Lists.GetAllCountries().Select(c => new ListItem(c.CountryName)).ToList();

		private static readonly List<Tehsil> Tehsils = BL.Core.Common.Lists.GetAllTehsils();

		public static ListControl FillCountries(this ListControl listControl, ListItem staticListItem = null)
		{
			return listControl.DataBind(Countries, staticListItem);
		}

		public static ListControl FillProvinces(this ListControl listControl, ListItem staticListItem = null)
		{
			return Tehsils.Select(t => t.ProvinceName).Distinct().Select(t => new ListItem(t, t)).DataBind(listControl, staticListItem);
		}

		public static ListControl FillDistricts(this ListControl listControl, string province, ListItem staticListItem = null)
		{
			return Tehsils.Where(t => t.ProvinceName == province).Select(t => t.DistrictName).Distinct().Select(t => new ListItem(t, t)).DataBind(listControl, staticListItem);
		}

		public static ListControl FillTehsils(this ListControl listControl, string province, string district, ListItem staticListItem = null)
		{
			return Tehsils.Where(t => t.ProvinceName == province && t.DistrictName == district).Select(t => t.TehsilName).Distinct().Select(t => new ListItem(t, t)).DataBind(listControl, staticListItem);
		}

		public static ListControl FillAllDistricts(this DropDownListWithOptionGroups listControl, ListItem staticListItem = null)
		{
			var districts = Tehsils.Select(t => new { t.ProvinceName, t.DistrictName }).Distinct().OrderBy(t => t.ProvinceName).ThenBy(t => t.DistrictName).ToList();
			listControl.Items.Clear();
			if (staticListItem != null)
				listControl.Items.Add(staticListItem);
			foreach (var district in districts)
				listControl.AddItem(district.DistrictName, district.DistrictName, district.ProvinceName);
			return listControl;
		}

		public static ListControl FillSemesters(this ListControl listControl, IEnumerable<short> semesterIDs, ListItem staticListItem = null)
		{
			listControl.Items.Clear();
			var items = semesterIDs.OrderByDescending(semesterID => semesterID).Select(semesterID => new ListItem(semesterID.ToSemesterString(), semesterID.ToString())).ToArray();
			listControl.Items.AddRange(items);
			return listControl.AddStaticListItem(staticListItem);
		}

		public static ListControl FillSemesters(this ListControl listControl, ListItem staticListItem = null)
		{
			return listControl.FillSemesters(BL.Core.Common.Lists.GetSemestersList(true), staticListItem);
		}

		public static ListControl FillSemesters(this ListControl listControl, short minimumSemesterID, ListItem staticListItem = null)
		{
			return listControl.FillSemesters(BL.Core.Common.Lists.GetSemestersList(true).Where(s => s >= minimumSemesterID), staticListItem);
		}

		public static ListControl FillInstitutesShortNames(this ListControl listControl, ListItem staticListItem = null)
		{
			return BL.Core.Common.Lists.GetInstitutesShortNamesList(Institute.Statuses.Active).DataBind(listControl, staticListItem);
		}

		public static ListControl FillInstitutesForAdmin(this ListControl listControl, ListItem staticListItem = null)
		{
			var institutes = BL.Core.Administration.Admin.Institutes.GetAssignedInstitutesList(AdminIdentity.Current.LoginSessionGuid);
			return listControl.DataBind(institutes, staticListItem);
		}

		public static ListControl FillAllActiveInstitutes(this ListControl listControl, ListItem staticListItem = null)
		{
			var institutes = BL.Core.Common.Lists.GetAllActiveInstitutes(AspireIdentity.Current.LoginSessionGuid);
			return listControl.DataBind(institutes, staticListItem);
		}

		public static ListControl FillFaculties(this ListControl listControl, int instituteID, ListItem staticListItem = null)
		{
			var faculties = BL.Core.Common.Lists.GetFacultiesList(instituteID);
			return listControl.DataBind(faculties, staticListItem);
		}

		public static ListControl FillDepartments(this ListControl listControl, int instituteID, Guid? facultyID, ListItem staticListItem = null)
		{
			return BL.Core.Common.Lists.GetDepartmentsList(instituteID, facultyID).DataBind(listControl, staticListItem);
		}

		public static ListControl FillDepartments(this ListControl listControl, int instituteID, ListItem staticListItem = null)
		{
			return listControl.FillDepartments(instituteID, null, staticListItem);
		}

		public static ListControl FillPrograms(this ListControl listControl, int instituteID, int? departmentID, bool considerNullDepartmentIDasAll, ListItem staticListItem = null)
		{
			return BL.Core.Common.Lists.GetPrograms(instituteID, departmentID, considerNullDepartmentIDasAll).DataBind(listControl, staticListItem);
		}

		public static ListControl FillPrograms(this ListControl listControl, int instituteID, int? departmentID, DegreeLevels? degreeLevelEnum, bool considerNullDepartmentIDasAll, ListItem staticListItem = null)
		{
			return BL.Core.Common.Lists.GetPrograms(instituteID, departmentID, degreeLevelEnum, considerNullDepartmentIDasAll).DataBind(listControl, staticListItem);
		}

		public static ListControl FillProgramsWithDepartment(this ListControl listControl, int instituteID, int? departmentID, bool considerNullDepartmentIDasAll, ListItem staticListItem = null)
		{
			return BL.Core.Common.Lists.GetProgramsWithDepartment(instituteID, departmentID, considerNullDepartmentIDasAll).DataBind(listControl, staticListItem);
		}

		public static ListControl FillProgramsWithDepartment(this ListControl listControl, int instituteID, int? departmentID, DegreeLevels? degreeLevelEnum, bool considerNullDepartmentIDasAll, ListItem staticListItem = null)
		{
			return BL.Core.Common.Lists.GetProgramsWithDepartment(instituteID, departmentID, degreeLevelEnum, considerNullDepartmentIDasAll).DataBind(listControl, staticListItem);
		}

		public static ListControl DataBindSemesters(this ListControl listControl, IEnumerable<short> semesterIDs)
		{
			return listControl.DataBind(semesterIDs.Select(semesterID => new ListItem(semesterID.ToSemesterString(), semesterID.ToString())));
		}

		public static ListControl GetRoomsForAttendance(this ListControl listControl, int instituteID)
		{
			return BL.Core.Common.Lists.GetRoomsForAttendance(instituteID).DataBind(listControl);
		}

		#endregion

		#region Common Enums

		public static ListControl FillGenders(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<Genders>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillSponsorships(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<Sponsorships>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillBloodGroups(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<BloodGroups>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillAreaTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<AreaTypes>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillFacultyTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<FacultyMember.FacultyTypes>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillFacultyMemberStatuses(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<FacultyMember.Statuses>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillFacultyMemberTitles(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<FacultyMember.Titles>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillSourceOfInformation(this ListControl listControl, ListItem staticListItem = null, params Candidate.SourceOfInformations[] enumValuesToBeExcluded)
		{
			return GetEnumValues<Candidate.SourceOfInformations>().Where(e => !enumValuesToBeExcluded.Contains(e)).Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillAccountTransactionServices(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<AccountTransaction.Services>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillTransactionChannels(this ListControl listControl, IEnumerable<AccountTransaction.TransactionChannels> transactionChannels, ListItem staticListItem = null)
		{
			return transactionChannels.Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillPaymentTypes(this ListControl listControl, IEnumerable<AccountTransaction.PaymentTypes> paymentTypes, ListItem staticListItem = null)
		{
			return paymentTypes.Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillInstituteBankAccountTypes(this ListControl listControl, ListItem staticListItem = null, params InstituteBankAccount.AccountTypes[] instituteBankAccountTypesToBeExcluded)
		{
			return GetEnumValues(instituteBankAccountTypesToBeExcluded).Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillInstituteBankAccountFeeTypes(this ListControl listControl, ListItem staticListItem = null, params InstituteBankAccount.FeeTypes[] instituteBankAccountFeeTypesToBeExcluded)
		{
			return GetEnumValues(instituteBankAccountFeeTypesToBeExcluded).Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillMeritListReportTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<MeritListReportTypes>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillSurveyTypes(this ListControl listControl, ListItem staticListItem = null, params Survey.SurveyTypes[] surveyTypesToBeExcluded)
		{
			return GetEnumValues(surveyTypesToBeExcluded).Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillSurveyConductTypes(this ListControl listControl, ListItem staticListItem = null, params SurveyConduct.SurveyConductTypes[] surveyConductTypesToBeExcluded)
		{
			return GetEnumValues(surveyConductTypesToBeExcluded).Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillCreditHours(this ListControl listControl, ListItem staticListItem = null)
		{
			return listControl.DataBind(CreditHours.AllCreditHours.Select(c => c.ToCreditHoursFullName()));
		}

		public static ListControl FillCourseCategories(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<CourseCategories>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillExamMarksTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<ExamMarksTypes>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillScholarshipTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<Scholarship.ScholarshipTypes>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillScholarshipStatuses(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<Scholarship.Statuses>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillScholarshipApprovalStatuses(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<ScholarshipApplicationApproval.ApprovalStatuses>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillScholarshipApplicationsChequeStatuses(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<ScholarshipApplicationApproval.ChequeStatuses>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillExamMarksTypes(this ListControl listControl, ExamMarkingScheme.MarkingSchemeTypes markingSchemeTypeEnum, ListItem staticListItem = null)
		{
			switch (markingSchemeTypeEnum)
			{
				case ExamMarkingScheme.MarkingSchemeTypes.AssignmentsQuizzesMidFinal:
					return new[] { ExamMarksTypes.Assignments, ExamMarksTypes.Quizzes, ExamMarksTypes.Mid, ExamMarksTypes.Final }.Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
				case ExamMarkingScheme.MarkingSchemeTypes.InternalsMidFinal:
					return new[] { ExamMarksTypes.Internals, ExamMarksTypes.Mid, ExamMarksTypes.Final }.Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
				default:
					throw new ArgumentOutOfRangeException(nameof(markingSchemeTypeEnum), markingSchemeTypeEnum, null);
			}
		}

		public static ListControl FillCourseTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<CourseTypes>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillGraduateDirectoryFormWorkExperienceTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<GraduateDirectoryFormWorkExperience.ExperienceTypes>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillGraduateDirectoryFormWorkshopTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<GraduateDirectoryFormWorkshop.Types>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillMarkingSchemeTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<ExamMarkingScheme.MarkingSchemeTypes>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillSemesterNos(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<SemesterNos>().Select(value => new ListItem(value.ToFullName(), ((short)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillETSTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<ETSTypes>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillYesNo(this ListControl listControl, ListItem staticListItem = null)
		{
			return CommonListItems.YesNo.DataBind(listControl, staticListItem);
		}

		public static ListControl FillBoolean(this ListControl listControl, string trueLabel, string falseLabel, ListItem staticListItem = null)
		{
			return new[] { new ListItem(trueLabel, CommonListItems.TrueValue), new ListItem(falseLabel, CommonListItems.FalseValue) }.DataBind(listControl, staticListItem);
		}

		public static ListControl FillDegreeLevels(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<DegreeLevels>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillProgramTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<ProgramTypes>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillProgramDurations(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<ProgramDurations>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillShifts(this ListControl listControl, IEnumerable<Shifts> shifts, ListItem staticListItem = null)
		{
			return shifts.Select(shift => new ListItem(shift.ToFullName(), ((byte)shift).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillShifts(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<Shifts>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillSections(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<Sections>().Select(value => new ListItem(value.ToFullName(), ((int)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillDegreeTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<DegreeTypes>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillStudentAcademicRecordStatuses(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<StudentAcademicRecord.Statuses>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillExamSystemTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<ExamSystemTypes>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillCategories(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues(Categories.Naval).Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillExamRoomStatuses(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<ExamRoom.Statuses>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillExamBuildingStatuses(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<ExamBuilding.Statuses>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillFeeTypes(this ListControl listControl, ListItem staticListItem = null, params StudentFee.FeeTypes[] feeTypesExcluded)
		{
			return GetEnumValues(feeTypesExcluded).Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillExamGrades(this ListControl listControl, ListItem staticListItem = null, params ExamGrades[] examGradesExcluded)
		{
			return GetEnumValues(examGradesExcluded).OrderByDescending(g => g, ExamGradeComparer.New()).Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillInstallmentNos(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<InstallmentNos>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillFreezeStatuses(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<FreezedStatuses>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillPasswordChangeReasons(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues(PasswordChangeReasons.ByUserItSelf, PasswordChangeReasons.UsingResetLink, PasswordChangeReasons.SetDuringRegistration).Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillStudentFeeStatus(this ListControl listControl, ListItem staticListItem = null, params StudentFee.Statuses[] statusesToBeExcluded)
		{
			return GetEnumValues(statusesToBeExcluded).Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillStudentFeeChallanStatus(this ListControl listControl, ListItem staticListItem = null, params StudentFeeChallan.Statuses[] statusesToBeExcluded)
		{
			return GetEnumValues(statusesToBeExcluded).Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillExamConductExamTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<ExamConduct.ExamTypes>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillRoomUsageTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<ExamOfferedRoom.RoomUsageTypes>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillExamCompilationTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<OfferedCours.ExamCompilationTypes>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillHoursValues(this ListControl listControl, IEnumerable<OfferedCourseAttendance.HoursValues> hours, bool presentIfOneOrOneAndHalf, ListItem staticListItem = null)
		{
			return hours.Select(value => new ListItem(value.ToFullName(presentIfOneOrOneAndHalf), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillHoursValues(this ListControl listControl, bool excludeAbsent, ListItem staticListItem = null)
		{
			if (excludeAbsent)
				return GetEnumValues<OfferedCourseAttendance.HoursValues>().Where(e => e != OfferedCourseAttendance.HoursValues.Absent).Select(value => new ListItem(value.ToFullName(false), ((byte)value).ToString())).DataBind(listControl, staticListItem);
			return GetEnumValues<OfferedCourseAttendance.HoursValues>().Select(value => new ListItem(value.ToFullName(false), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillIntegrationServiceTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<IntegratedServiceUser.ServiceTypes>().Select(value => new ListItem(value.ToFullName(), ((int)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillCBTLabStatuses(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<CBTLab.Statuses>().Select(value => new ListItem(value.ToFullName(), ((int)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillMarksEntryTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<ExamMarksPolicy.MarksEntryTypes>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillUserTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<UserTypes>().Select(value => new ListItem(value.ToFullName(), ((short)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillSubUserTypes(this ListControl listControl, ListItem staticListItem = null)
		{
			return GetEnumValues<SubUserTypes>().Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillStudentStatuses(this ListControl listControl, ListItem staticListItem = null, params Student.Statuses[] statusesToBeExcluded)
		{
			return GetEnumValues(statusesToBeExcluded).Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillOfferedCourseStatuses(this ListControl listControl, ListItem staticListItem = null, params OfferedCours.Statuses[] statusesToBeExcluded)
		{
			return GetEnumValues(statusesToBeExcluded).Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		public static ListControl FillAdmissionTypes(this ListControl listControl, ListItem staticListItem = null, params Student.AdmissionTypes[] admissionTypesToBeExcluded)
		{
			return GetEnumValues(admissionTypesToBeExcluded).Select(value => new ListItem(value.ToFullName(), ((byte)value).ToString())).DataBind(listControl, staticListItem);
		}

		#endregion

		public static PageIcon GetIcon(this AspireGlyphicons icon)
		{
			return new PageIcon(icon);
		}

		public static PageIcon GetIcon(this FontAwesomeIcons icon)
		{
			return new PageIcon(icon);
		}
	}
}
