using Aspire.Model.Entities.Common;
using System;

namespace Aspire.Web.Common
{
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	public sealed class AspirePageAttribute : Attribute
	{
		public Guid AspirePageGuid { get; }
		public UserTypes UserTypes { get; }
		public string PageUrl { get; }
		public string MenuItemText { get; }
		public bool DisplayInList { get; }
		public AspireModules AspireModule { get; }

		public AspirePageAttribute(string aspirePageGuid, UserTypes userTypes, string pageUrl, string menuItemText, bool displayInList, AspireModules aspireModule)
		{
			if (displayInList && string.IsNullOrWhiteSpace(menuItemText))
				throw new ArgumentNullException(nameof(menuItemText), @"Text cannot be null or empty or whitespaces when displayInList is true.");
			if (!Guid.TryParse(aspirePageGuid, out _))
				throw new InvalidOperationException("Guid is not correct. Url:" + pageUrl);
			this.AspirePageGuid = Guid.Parse(aspirePageGuid);
			this.UserTypes = userTypes;
			this.PageUrl = pageUrl;
			this.MenuItemText = menuItemText;
			this.DisplayInList = displayInList;
			this.AspireModule = aspireModule;
		}
	}
}