﻿using Aspire.Lib.Helpers;
using Aspire.Model.Entities;
using System;

namespace Aspire.Web.Common
{
	public abstract class LogoffBasePage : BasePage
	{
		protected UserLoginHistory.LogOffReasons LogOffReason => this.Request.GetParameterValue<UserLoginHistory.LogOffReasons>("LogOffReason") ?? UserLoginHistory.LogOffReasons.LogOff;

		protected static string GetPageUrl(string pageUrl, UserLoginHistory.LogOffReasons reason)
		{
			return pageUrl.AttachQueryParam("LogOffReason", reason);
		}

		protected abstract string LoginPageUrl { get; }

		protected override void Authenticate()
		{
			if (this.AspireIdentity == null)
				Redirect(this.LoginPageUrl);
		}

		protected override void OnLoad(EventArgs e)
		{
			BL.Core.LoginManagement.Common.Logoff(this.AspireIdentity.LoginSessionGuid, this.LogOffReason);
			SessionHelper.DestroyCurrentUserSession(this.LoginPageUrl);
		}
	}
}