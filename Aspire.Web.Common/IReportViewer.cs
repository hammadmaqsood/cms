﻿using Microsoft.Reporting.WebForms;

namespace Aspire.Web.Common
{
	public interface IReportViewer
	{
		void RenderReport(ReportViewer reportViewer);
		string ReportName { get; }
		bool ExportToExcel { get; }
		bool ExportToWord { get; }
	}
}