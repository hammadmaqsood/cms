﻿// ReSharper disable All
namespace Aspire.Web.Common
{
	public enum AspireThemes
	{
		Fire,
		Deepsea,
		Amethyst,
		Army,
		Asphalt,
		City,
		Autumn,
		Cherry,
		Dawn,
		Diamond,
		Grass,
		Leaf,
		Night,
		Ocean,
		Oil,
		Stone,
		Sun,
		Tulip,
		Wood,
	}
}