﻿using System;

namespace Aspire.Web.Common
{
	public class NoJavascriptException : Exception
	{
		public string PreviousPageUrl { get; }
		public NoJavascriptException(string prevPageUrl)
		{
			this.PreviousPageUrl = prevPageUrl;
		}
	}
}
