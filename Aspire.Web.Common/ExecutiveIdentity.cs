using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Aspire.Web.Common
{
	[Serializable]
	public sealed class ExecutiveIdentity : AspireUserRoleIdentity, IExecutiveIdentity
	{
		public int InstituteID { get; }

		public IReadOnlyDictionary<int, string> Roles { get; }

		public ExecutiveIdentity(string username, string name, string email, int userLoginHistoryID, Guid userLoginSessionGuid, int userID, int userRoleID, int instituteID, string instituteName, string instituteShortName, string instituteAlias, Dictionary<int, string> userRoles, string ipAddress, string userAgent)
			: base(username, name, email, UserTypes.Executive, userLoginHistoryID, userLoginSessionGuid, userID, userRoleID, ipAddress, userAgent, instituteName, instituteShortName, instituteAlias)
		{
			if (userRoles == null || !userRoles.Any())
				throw new ArgumentException(nameof(userRoles));

			this.InstituteID = instituteID;
			this.Roles = userRoles;
		}

		public ExecutiveIdentity(SerializationInfo serializationInfo, StreamingContext streamingContext)
			: base(serializationInfo, streamingContext)
		{
			this.InstituteID = GetValue<int>(serializationInfo, nameof(this.InstituteID));
			var rolesCount = GetValue<int>(serializationInfo, "RolesCount");
			var roles = new Dictionary<int, string>();
			for (var i = 0; i < rolesCount; i++)
				roles.Add(GetValue<int>(serializationInfo, $"{nameof(this.UserRoleID)}{i}"), GetValue<string>(serializationInfo, $"InstituteAlias{i}"));
			this.Roles = roles;
		}

		public new void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			base.GetObjectData(serializationInfo, streamingContext);
			serializationInfo.AddValue(nameof(this.InstituteID), this.InstituteID);
			serializationInfo.AddValue("RolesCount", this.Roles.Count);
			var index = 0;
			foreach (var role in this.Roles)
			{
				serializationInfo.AddValue($"{nameof(this.UserRoleID)}{index}", role.Key);
				serializationInfo.AddValue($"InstituteAlias{index}", role.Value);
				index++;
			}
		}

		public Executive.ExecutiveModulePermissions Demand(ExecutivePermissionType executivePermissionType, params UserGroupPermission.PermissionValues[] demandedPermissionValueEnums)
		{
			return Executive.DemandExecutiveModulePermissions(this.LoginSessionGuid, executivePermissionType, demandedPermissionValueEnums);
		}

		public Executive.ExecutiveModulePermissions TryDemand(ExecutivePermissionType executivePermissionType, params UserGroupPermission.PermissionValues[] demandedPermissionValueEnums)
		{
			return Executive.TryDemandExecutiveModulePermissions(this.LoginSessionGuid, executivePermissionType, demandedPermissionValueEnums);
		}

		public static new IExecutiveIdentity Current => AspireUserRoleIdentity.Current as IExecutiveIdentity;
	}
}