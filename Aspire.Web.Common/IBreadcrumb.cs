namespace Aspire.Web.Common
{
	public interface IBreadcrumb
	{
		bool BreadcrumbVisible { get; }
	}
}