﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System.Collections.Generic;

namespace Aspire.Web.Common
{
	public interface IExecutiveIdentity : IAspireUserRoleIdentity
	{
		int InstituteID { get; }

		IReadOnlyDictionary<int, string> Roles { get; }

		Executive.ExecutiveModulePermissions Demand(ExecutivePermissionType executivePermissionType, params UserGroupPermission.PermissionValues[] requiredPermissionValueEnums);

		Executive.ExecutiveModulePermissions TryDemand(ExecutivePermissionType executivePermissionType, params UserGroupPermission.PermissionValues[] requiredPermissionValueEnums);
	}
}