using Aspire.Model.Entities.Common;
using System;
using System.Runtime.Serialization;

namespace Aspire.Web.Common
{
	[Serializable]
	public sealed class CandidateIdentity : AspireIdentity, ICandidateIdentity
	{
		public int CandidateID { get; }

		public bool ForeignStudent { get; }

		public short SemesterID { get; }

		public Genders GenderEnum { get; }

		public CandidateIdentity(string username, string name, string email, int userLoginHistoryID, Guid userLoginSessionGuid, int candidateID, bool foreignStudent, short semesterID, Genders genderEnum, string ipAddress, string userAgent)
			: base(username, name, email, UserTypes.Candidate, SubUserTypes.None, userLoginHistoryID, userLoginSessionGuid, ipAddress, userAgent, "Bahria University", "Bahria University", "Bahria University")
		{
			this.CandidateID = candidateID;
			this.ForeignStudent = foreignStudent;
			this.SemesterID = semesterID;
			this.GenderEnum = genderEnum;
		}

		public CandidateIdentity(SerializationInfo serializationInfo, StreamingContext streamingContext)
			: base(serializationInfo, streamingContext)
		{
			this.CandidateID = GetValue<int>(serializationInfo, nameof(this.CandidateID));
			this.ForeignStudent = GetValue<bool>(serializationInfo, nameof(this.ForeignStudent));
			this.SemesterID = GetValue<short>(serializationInfo, nameof(this.SemesterID));
			this.GenderEnum = GetValue<Genders>(serializationInfo, nameof(this.GenderEnum));
		}

		public new void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			base.GetObjectData(serializationInfo, streamingContext);
			AddValue(serializationInfo, nameof(this.CandidateID), this.CandidateID);
			AddValue(serializationInfo, nameof(this.ForeignStudent), this.ForeignStudent);
			AddValue(serializationInfo, nameof(this.SemesterID), this.SemesterID);
			AddValue(serializationInfo, nameof(this.GenderEnum), this.GenderEnum);
		}

		public static new ICandidateIdentity Current => AspireIdentity.Current as ICandidateIdentity;
	}
}