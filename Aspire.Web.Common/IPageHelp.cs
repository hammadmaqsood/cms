namespace Aspire.Web.Common
{
	public interface IPageHelp
	{
		string HelpNavigateUrl { get; }
	}
}