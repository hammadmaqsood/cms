﻿namespace Aspire.Web.Common
{
	public interface IFacultyIdentity : IAspireIdentity
	{
		int FacultyMemberID { get; }

		int InstituteID { get; }
	}
}