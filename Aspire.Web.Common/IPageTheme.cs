﻿namespace Aspire.Web.Common
{
	public interface IPageTheme
	{
		AspireThemes AspireTheme { get; }
	}
}