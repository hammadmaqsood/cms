using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Aspire.Web.Common
{
	[Serializable]
	public sealed class StaffIdentity : AspireUserRoleIdentity, IStaffIdentity
	{
		public int InstituteID { get; }

		public string InstituteCode { get; }

		public IReadOnlyDictionary<int, string> Roles { get; }

		public StaffIdentity(string username, string name, string email, int userLoginHistoryID, Guid userLoginSessionGuid, int userID, int userRoleID, int instituteID, string instituteCode, string instituteName, string instituteShortName, string instituteAlias, Dictionary<int, string> userRoles, string ipAddress, string userAgent)
			: base(username, name, email, UserTypes.Staff, userLoginHistoryID, userLoginSessionGuid, userID, userRoleID, ipAddress, userAgent, instituteName, instituteShortName, instituteAlias)
		{
			if (userRoles == null || !userRoles.Any())
				throw new ArgumentException(nameof(userRoles));

			this.InstituteID = instituteID;
			this.InstituteCode = instituteCode;
			this.Roles = userRoles;
		}

		public StaffIdentity(SerializationInfo serializationInfo, StreamingContext streamingContext)
			: base(serializationInfo, streamingContext)
		{
			this.InstituteID = GetValue<int>(serializationInfo, nameof(this.InstituteID));
			this.InstituteCode = GetValue<string>(serializationInfo, nameof(this.InstituteCode));
			var rolesCount = GetValue<int>(serializationInfo, "RolesCount");
			var roles = new Dictionary<int, string>();
			for (var i = 0; i < rolesCount; i++)
				roles.Add(GetValue<int>(serializationInfo, $"{nameof(this.UserRoleID)}{i}"), GetValue<string>(serializationInfo, $"InstituteAlias{i}"));
			this.Roles = roles;
		}

		public new void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			base.GetObjectData(serializationInfo, streamingContext);
			serializationInfo.AddValue(nameof(this.InstituteID), this.InstituteID);
			serializationInfo.AddValue(nameof(this.InstituteCode), this.InstituteCode);
			serializationInfo.AddValue("RolesCount", this.Roles.Count);
			var index = 0;
			foreach (var role in this.Roles)
			{
				serializationInfo.AddValue($"{nameof(this.UserRoleID)}{index}", role.Key);
				serializationInfo.AddValue($"InstituteAlias{index}", role.Value);
				index++;
			}
		}

		public UserGroupPermission.PermissionValues? GetStaffPermissionValue(StaffPermissionType staffPermissionType)
		{
			return Staff.GetStaffPermissionValue(this.LoginSessionGuid, staffPermissionType);
		}

		public Staff.StaffGroupPermission Demand(StaffPermissionType staffPermissionType, params UserGroupPermission.PermissionValues[] requiredPermissionValueEnums)
		{
			return Staff.DemandStaffPermissions(this.LoginSessionGuid, staffPermissionType, requiredPermissionValueEnums);
		}

		public Staff.StaffGroupPermission TryDemand(StaffPermissionType staffPermissionType, params UserGroupPermission.PermissionValues[] requiredPermissionValueEnums)
		{
			return Staff.TryDemandStaffPermissions(this.LoginSessionGuid, staffPermissionType, requiredPermissionValueEnums);
		}

		public static new IStaffIdentity Current => AspireUserRoleIdentity.Current as IStaffIdentity;
	}
}