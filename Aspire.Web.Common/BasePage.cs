﻿using Aspire.Lib.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI;

namespace Aspire.Web.Common
{
	public abstract class BasePage : Page
	{
		private readonly DateTime _startDateTime = DateTime.Now;
		private IAspireIdentity _aspireIdentity;
		public IAspireIdentity AspireIdentity => this._aspireIdentity ?? (this._aspireIdentity = Common.AspireIdentity.Current);

		protected abstract void Authenticate();
		protected override void OnPreInit(EventArgs e)
		{
			if (!(this.Page is LogoffBasePage))
			{
				if (this.AspireIdentity != null)
				{
					if (this.ClientIPAddress != this.AspireIdentity.IPAddress)
						throw new IPAddressChangedException();
					if (this.UserAgent != this.AspireIdentity.UserAgent)
						throw new UserAgentChangedException();
				}
			}
			this.Authenticate();
			base.OnPreInit(e);
		}

		protected override void OnInit(EventArgs e)
		{
			this.Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
			this.Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
			this.Response.AppendHeader("Expires", "0"); // Proxies.
			base.OnInit(e);
		}

		public string ClientIPAddress => this.Page.Request.UserHostAddress;
		public string UserAgent => this.Page.Request.UserAgent;

		private static void SetDateFormats(CultureInfo cultureInfo)
		{
			cultureInfo.DateTimeFormat.ShortDatePattern = @"dd/MM/yyyy";
			cultureInfo.DateTimeFormat.LongDatePattern = @"dddd, d MMMM yyyy";
			cultureInfo.DateTimeFormat.ShortTimePattern = @"h:mm tt";
			cultureInfo.DateTimeFormat.LongTimePattern = @"h:mm:ss tt";
		}

		public static void SetCurrentThreadCulture()
		{
			var culture = CultureInfo.CreateSpecificCulture("en-GB");
			var uiCulture = new CultureInfo("en-GB");
			SetDateFormats(culture);
			SetDateFormats(uiCulture);

			Thread.CurrentThread.CurrentCulture = culture;
			Thread.CurrentThread.CurrentUICulture = uiCulture;
		}

		protected override void InitializeCulture()
		{
			this.UICulture = "en-GB";
			this.Culture = "en-GB";
			SetCurrentThreadCulture();
			base.InitializeCulture();
		}

		protected override void OnUnload(EventArgs e)
		{
			base.OnUnload(e);
			BL.Core.Logs.Common.UsageHistory.AddUsageHistory(this.AspireIdentity?.UserLoginHistoryID, this._startDateTime, DateTime.Now, this.IsPostBack, this.AspirePageAttribute.AspirePageGuid);
		}

		public bool IsPostbackAndNotValid => this.IsPostBack && !this.IsValid;

		#region Helper Methods

		public static AspirePageAttribute GetAspirePageAttribute<T>() where T : BasePage
		{
			return AspirePageAttributesDictionary.GetAspirePageAttribute<T>();
		}

		public static string GetPageUrl<T>() where T : BasePage
		{
			return GetAspirePageAttribute<T>().PageUrl;
		}

		private AspirePageAttribute _aspirePageAttribute;
		public AspirePageAttribute AspirePageAttribute => this._aspirePageAttribute ?? (this._aspirePageAttribute = this.GetAspirePageAttribute());

		#region Redirect

		private const bool DefaultEndResponse = true;
		private const EncryptionModes DefaultEncryptionMode = QueryStringHelper.DefaultEncryptionMode;

		public static void Redirect(string navigateUrl, bool endResponse = DefaultEndResponse)
		{
			HttpContext.Current.Response.Redirect(navigateUrl, endResponse);
		}

		public static void Redirect<T>(bool endResponse = DefaultEndResponse) where T : BasePage
		{
			HttpContext.Current.Response.Redirect(GetAspirePageAttribute<T>().PageUrl, endResponse);
		}

		public static void Redirect<T>(string param1Name, object param1Value, string param2Name, object param2Value, string param3Name, object param3Value, string param4Name, object param4Value, string param5Name, object param5Value, string param6Name, object param6Value, string param7Name, object param7Value, string param8Name, object param8Value, string param9Name, object param9Value, string param10Name, object param10Value, string param11Name, object param11Value, EncryptionModes encryptionMode = DefaultEncryptionMode, bool endResponse = DefaultEndResponse) where T : BasePage
		{
			HttpContext.Current.Response.Redirect(GetAspirePageAttribute<T>().PageUrl.AttachQueryParams(param1Name, param1Value, param2Name, param2Value, param3Name, param3Value, param4Name, param4Value, param5Name, param5Value, param6Name, param6Value, param7Name, param7Value, param8Name, param8Value, param9Name, param9Value, param10Name, param10Value, param11Name, param11Value, encryptionMode), endResponse);
		}

		public static void Redirect<T>(string param1Name, object param1Value, string param2Name, object param2Value, string param3Name, object param3Value, string param4Name, object param4Value, string param5Name, object param5Value, string param6Name, object param6Value, string param7Name, object param7Value, string param8Name, object param8Value, string param9Name, object param9Value, string param10Name, object param10Value, EncryptionModes encryptionMode = DefaultEncryptionMode, bool endResponse = DefaultEndResponse) where T : BasePage
		{
			HttpContext.Current.Response.Redirect(GetAspirePageAttribute<T>().PageUrl.AttachQueryParams(param1Name, param1Value, param2Name, param2Value, param3Name, param3Value, param4Name, param4Value, param5Name, param5Value, param6Name, param6Value, param7Name, param7Value, param8Name, param8Value, param9Name, param9Value, param10Name, param10Value, encryptionMode), endResponse);
		}

		public static void Redirect<T>(string param1Name, object param1Value, string param2Name, object param2Value, string param3Name, object param3Value, string param4Name, object param4Value, string param5Name, object param5Value, string param6Name, object param6Value, string param7Name, object param7Value, string param8Name, object param8Value, string param9Name, object param9Value, EncryptionModes encryptionMode = DefaultEncryptionMode, bool endResponse = DefaultEndResponse) where T : BasePage
		{
			HttpContext.Current.Response.Redirect(GetAspirePageAttribute<T>().PageUrl.AttachQueryParams(param1Name, param1Value, param2Name, param2Value, param3Name, param3Value, param4Name, param4Value, param5Name, param5Value, param6Name, param6Value, param7Name, param7Value, param8Name, param8Value, param9Name, param9Value, encryptionMode), endResponse);
		}

		public static void Redirect<T>(string param1Name, object param1Value, string param2Name, object param2Value, string param3Name, object param3Value, string param4Name, object param4Value, string param5Name, object param5Value, string param6Name, object param6Value, string param7Name, object param7Value, string param8Name, object param8Value, EncryptionModes encryptionMode = DefaultEncryptionMode, bool endResponse = DefaultEndResponse) where T : BasePage
		{
			HttpContext.Current.Response.Redirect(GetAspirePageAttribute<T>().PageUrl.AttachQueryParams(param1Name, param1Value, param2Name, param2Value, param3Name, param3Value, param4Name, param4Value, param5Name, param5Value, param6Name, param6Value, param7Name, param7Value, param8Name, param8Value, encryptionMode), endResponse);
		}

		public static void Redirect<T>(string param1Name, object param1Value, string param2Name, object param2Value, string param3Name, object param3Value, string param4Name, object param4Value, string param5Name, object param5Value, string param6Name, object param6Value, string param7Name, object param7Value, EncryptionModes encryptionMode = DefaultEncryptionMode, bool endResponse = DefaultEndResponse) where T : BasePage
		{
			HttpContext.Current.Response.Redirect(GetAspirePageAttribute<T>().PageUrl.AttachQueryParams(param1Name, param1Value, param2Name, param2Value, param3Name, param3Value, param4Name, param4Value, param5Name, param5Value, param6Name, param6Value, param7Name, param7Value, encryptionMode), endResponse);
		}

		public static void Redirect<T>(string param1Name, object param1Value, string param2Name, object param2Value, string param3Name, object param3Value, string param4Name, object param4Value, string param5Name, object param5Value, string param6Name, object param6Value, EncryptionModes encryptionMode = DefaultEncryptionMode, bool endResponse = DefaultEndResponse) where T : BasePage
		{
			HttpContext.Current.Response.Redirect(GetAspirePageAttribute<T>().PageUrl.AttachQueryParams(param1Name, param1Value, param2Name, param2Value, param3Name, param3Value, param4Name, param4Value, param5Name, param5Value, param6Name, param6Value, encryptionMode), endResponse);
		}

		public static void Redirect<T>(string param1Name, object param1Value, string param2Name, object param2Value, string param3Name, object param3Value, string param4Name, object param4Value, string param5Name, object param5Value, EncryptionModes encryptionMode = DefaultEncryptionMode, bool endResponse = DefaultEndResponse) where T : BasePage
		{
			HttpContext.Current.Response.Redirect(GetAspirePageAttribute<T>().PageUrl.AttachQueryParams(param1Name, param1Value, param2Name, param2Value, param3Name, param3Value, param4Name, param4Value, param5Name, param5Value, encryptionMode), endResponse);
		}

		public static void Redirect<T>(string param1Name, object param1Value, string param2Name, object param2Value, string param3Name, object param3Value, string param4Name, object param4Value, EncryptionModes encryptionMode = DefaultEncryptionMode, bool endResponse = DefaultEndResponse) where T : BasePage
		{
			HttpContext.Current.Response.Redirect(GetAspirePageAttribute<T>().PageUrl.AttachQueryParams(param1Name, param1Value, param2Name, param2Value, param3Name, param3Value, param4Name, param4Value, encryptionMode), endResponse);
		}

		public static void Redirect<T>(string param1Name, object param1Value, string param2Name, object param2Value, string param3Name, object param3Value, EncryptionModes encryptionMode = DefaultEncryptionMode, bool endResponse = DefaultEndResponse) where T : BasePage
		{
			HttpContext.Current.Response.Redirect(GetAspirePageAttribute<T>().PageUrl.AttachQueryParams(param1Name, param1Value, param2Name, param2Value, param3Name, param3Value, encryptionMode), endResponse);
		}

		public static void Redirect<T>(string param1Name, object param1Value, string param2Name, object param2Value, EncryptionModes encryptionMode = DefaultEncryptionMode, bool endResponse = DefaultEndResponse) where T : BasePage
		{
			HttpContext.Current.Response.Redirect(GetAspirePageAttribute<T>().PageUrl.AttachQueryParams(param1Name, param1Value, param2Name, param2Value, encryptionMode), endResponse);
		}

		public static void Redirect<T>(string param1Name, object param1Value, EncryptionModes encryptionMode = DefaultEncryptionMode, bool endResponse = DefaultEndResponse) where T : BasePage
		{
			HttpContext.Current.Response.Redirect(GetAspirePageAttribute<T>().PageUrl.AttachQueryParam(param1Name, param1Value, encryptionMode), endResponse);
		}

		#endregion

		public void SetMaximumScriptTimeout()
		{
			this.Server.ScriptTimeout = (int)TimeSpan.FromMinutes(this.Session.Timeout).TotalSeconds;
		}

		#region Query Parameters

		private Dictionary<string, string> _requestParameters;
		protected Dictionary<string, string> RequestParameters => this._requestParameters ?? (this._requestParameters = this.Request.GetAllParameters());

		protected string GetParameterValue(string paramName)
		{
			this.RequestParameters.TryGetValue(paramName, out var value);
			return value;
		}

		public T? GetParameterValue<T>(string paramName) where T : struct
		{
			var val = this.GetParameterValue(paramName);
			if (string.IsNullOrEmpty(val))
				return null;
			if (typeof(T) == typeof(Guid))
				return (T)(object)Guid.Parse(val);
			if (typeof(T).IsEnum)
				return (T)Enum.Parse(typeof(T), val);
			return (T?)Convert.ChangeType(val, typeof(T));
		}

		#endregion

		#endregion
	}
}