using System.Web.UI.WebControls;

namespace Aspire.Web.Common
{
	public static class CommonListItems
	{
		public static readonly string AllLabel = "All";

		public static readonly string SelectLabel = "Select";

		public static readonly string YesLabel = "Yes";

		public static readonly string NoLabel = "No";

		public static readonly string TrueValue = "1";

		public static readonly string FalseValue = "0";

		public static readonly string YesValue = TrueValue;

		public static readonly string NoValue = FalseValue;

		public static readonly string NoneLabel = "None";

		public static readonly string EmptyValue = "";

		public static readonly string AllValue = EmptyValue;

		public static readonly string SelectValue = EmptyValue;

		public static readonly string NoneValue = EmptyValue;

		public static ListItem All => new ListItem(AllLabel, EmptyValue);

		public static ListItem Select => new ListItem(SelectLabel, EmptyValue);

		public static ListItem None => new ListItem(NoneLabel, EmptyValue);

		public static ListItem Empty => new ListItem(EmptyValue, EmptyValue);

		public static ListItem Yes => new ListItem(YesLabel, YesValue);

		public static ListItem No => new ListItem(NoLabel, NoValue);

		public static ListItem[] YesNo => new[] { Yes, No };
	}
}