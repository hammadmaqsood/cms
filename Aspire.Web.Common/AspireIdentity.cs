using Aspire.Lib.Extensions;
using Aspire.Model.Entities.Common;
using System;
using System.Runtime.Serialization;

namespace Aspire.Web.Common
{
	[Serializable]
	public abstract class AspireIdentity : IAspireIdentity
	{
		public string BrandName { get; }
		public string BrandShortName { get; }
		public string BrandAlias { get; }
		public DateTime LastUpdatedOn { get; }
		public Guid LoginSessionGuid { get; }
		public string Name { get; }
		public string Username { get; }
		public UserTypes UserType { get; }
		public SubUserTypes SubUserType { get; }
		public string Email { get; }
		public int UserLoginHistoryID { get; }
		public string IPAddress { get; }
		public string UserAgent { get; }

		protected AspireIdentity(string username, string name, string email, UserTypes userTypeEnum, SubUserTypes subUserTypeEnum, int userLoginHistoryID, Guid userLoginSessionGuid, string ipAddress, string userAgent, string brandName, string brandShortName, string brandAlias)
		{
			this.LastUpdatedOn = DateTime.Now;
			this.Username = username;
			this.Name = name.TrimAndMakeItNullIfEmpty();
			this.Email = email.TrimAndMakeItNullIfEmpty();
			this.UserType = userTypeEnum;
			this.SubUserType = subUserTypeEnum;
			this.UserLoginHistoryID = userLoginHistoryID;
			this.LoginSessionGuid = userLoginSessionGuid;
			this.IPAddress = ipAddress;
			this.UserAgent = userAgent;
			this.BrandName = brandName;
			this.BrandShortName = brandShortName;
			this.BrandAlias = brandAlias;
		}

		protected static void AddValue<T>(SerializationInfo serializationInfo, string name, T value)
		{
			serializationInfo.AddValue(name, value, typeof(T));
		}

		protected static T GetValue<T>(SerializationInfo serializationInfo, string name)
		{
			return (T)serializationInfo.GetValue(name, typeof(T));
		}

		protected AspireIdentity(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			this.LastUpdatedOn = GetValue<DateTime>(serializationInfo, nameof(this.LastUpdatedOn));
			this.Username = GetValue<string>(serializationInfo, nameof(this.Username));
			this.Name = GetValue<string>(serializationInfo, nameof(this.Name));
			this.Email = GetValue<string>(serializationInfo, nameof(this.Email));
			this.UserType = GetValue<UserTypes>(serializationInfo, nameof(this.UserType));
			this.SubUserType = GetValue<SubUserTypes>(serializationInfo, nameof(this.SubUserType));
			this.UserLoginHistoryID = GetValue<int>(serializationInfo, nameof(this.UserLoginHistoryID));
			this.LoginSessionGuid = GetValue<Guid>(serializationInfo, nameof(this.LoginSessionGuid));
			this.IPAddress = GetValue<string>(serializationInfo, nameof(this.IPAddress));
			this.UserAgent = GetValue<string>(serializationInfo, nameof(this.UserAgent));
			this.BrandName = GetValue<string>(serializationInfo, nameof(this.BrandName));
			this.BrandShortName = GetValue<string>(serializationInfo, nameof(this.BrandShortName));
			this.BrandAlias = GetValue<string>(serializationInfo, nameof(this.BrandAlias));
		}

		public virtual void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			AddValue(serializationInfo, nameof(this.LastUpdatedOn), this.LastUpdatedOn);
			AddValue(serializationInfo, nameof(this.Username), this.Username);
			AddValue(serializationInfo, nameof(this.Name), this.Name);
			AddValue(serializationInfo, nameof(this.Email), this.Email);
			AddValue(serializationInfo, nameof(this.UserType), this.UserType);
			AddValue(serializationInfo, nameof(this.SubUserType), this.SubUserType);
			AddValue(serializationInfo, nameof(this.UserLoginHistoryID), this.UserLoginHistoryID);
			AddValue(serializationInfo, nameof(this.LoginSessionGuid), this.LoginSessionGuid);
			AddValue(serializationInfo, nameof(this.IPAddress), this.IPAddress);
			AddValue(serializationInfo, nameof(this.UserAgent), this.UserAgent);
			AddValue(serializationInfo, nameof(this.BrandName), this.BrandName);
			AddValue(serializationInfo, nameof(this.BrandShortName), this.BrandShortName);
			AddValue(serializationInfo, nameof(this.BrandAlias), this.BrandAlias);
		}

		public static IAspireIdentity Current => SessionHelper.AspireIdentity;
	}
}