using Aspire.Model.Entities.Common;
using System;
using System.Runtime.Serialization;

namespace Aspire.Web.Common
{
	[Serializable]
	public abstract class AspireUserRoleIdentity : AspireIdentity, IAspireUserRoleIdentity
	{
		public int UserID { get; }

		public int UserRoleID { get; }

		protected AspireUserRoleIdentity(string username, string name, string email, UserTypes userTypeEnum, int userLoginHistoryID, Guid userLoginSessionGuid, int userID, int userRoleID, string ipAddress, string userAgent, string brandName, string brandShortName, string brandAlias)
			: base(username, name, email, userTypeEnum, SubUserTypes.None, userLoginHistoryID, userLoginSessionGuid, ipAddress, userAgent, brandName, brandShortName, brandAlias)
		{
			this.UserID = userID;
			this.UserRoleID = userRoleID;
		}

		protected AspireUserRoleIdentity(SerializationInfo serializationInfo, StreamingContext streamingContext)
			: base(serializationInfo, streamingContext)
		{
			this.UserID = GetValue<int>(serializationInfo, nameof(this.UserID));
			this.UserRoleID = GetValue<int>(serializationInfo, nameof(this.UserRoleID));
		}

		public override void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			base.GetObjectData(serializationInfo, streamingContext);
			AddValue(serializationInfo, nameof(this.UserID), this.UserID);
			AddValue(serializationInfo, nameof(this.UserRoleID), this.UserRoleID);
		}

		public static new IAspireUserRoleIdentity Current => AspireIdentity.Current as IAspireUserRoleIdentity;
	}
}