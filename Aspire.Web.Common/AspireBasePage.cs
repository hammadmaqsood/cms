using Aspire.Lib.WebControls;
using System.Web.UI.WebControls;

namespace Aspire.Web.Common
{
	public abstract class AspireBasePage : BasePage, IPageTheme, IPageHelp, IPageTitle, IAlert
	{
		public abstract AspireThemes AspireTheme { get; }

		public virtual string HelpNavigateUrl => this.GetHelpLink();

		public virtual bool PageTitleVisible => true;

		public abstract PageIcon PageIcon { get; }

		public abstract string PageTitle { get; }

		public virtual HorizontalAlign PageTitleHorizontalAlign => HorizontalAlign.Left;
	}
}