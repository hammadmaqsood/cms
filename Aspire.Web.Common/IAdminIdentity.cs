﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;

namespace Aspire.Web.Common
{
	public interface IAdminIdentity : IAspireUserRoleIdentity
	{
		Admin.AdminLoginHistory Demand(AdminPermissionType adminPermissionType, params UserGroupPermission.PermissionValues[] requiredPermissionValueEnums);
	}
}