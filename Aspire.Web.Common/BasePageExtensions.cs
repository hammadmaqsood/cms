using Aspire.Lib.Extensions;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspire.Web.Common
{
	public static class BasePageExtensions
	{
		public static void SetValue(this StateBag viewState, string key, object value)
		{
			viewState[key] = value;
		}

		public static T GetValue<T>(this StateBag viewState, string key, T defaultValue)
		{
			if (viewState[key] == null)
				return defaultValue;
			return (T)viewState[key];
		}

		#region Helper Methods

		public static string GetSortExpression(this StateBag viewState)
		{
			return (string)viewState["SE"];
		}

		public static void SetSortExpression(this StateBag viewState, string sortExpression)
		{
			viewState["SE"] = sortExpression;
		}

		public static SortDirection GetSortDirection(this StateBag viewState)
		{
			return (SortDirection)viewState["SD"];
		}

		public static void SetSortDirection(this StateBag viewState, SortDirection sortDirection)
		{
			viewState["SD"] = sortDirection;
		}

		public static string GetSortExpression1(this StateBag viewState)
		{
			return (string)viewState["SE1"];
		}

		public static void SetSortExpression1(this StateBag viewState, string sortExpression)
		{
			viewState["SE1"] = sortExpression;
		}

		public static SortDirection GetSortDirection1(this StateBag viewState)
		{
			return (SortDirection)viewState["SD1"];
		}

		public static void SetSortDirection1(this StateBag viewState, SortDirection sortDirection)
		{
			viewState["SD1"] = sortDirection;
		}

		public static string GetSortExpression2(this StateBag viewState)
		{
			return (string)viewState["SE2"];
		}

		public static void SetSortExpression2(this StateBag viewState, string sortExpression)
		{
			viewState["SE2"] = sortExpression;
		}

		public static SortDirection GetSortDirection2(this StateBag viewState)
		{
			return (SortDirection)viewState["SD2"];
		}

		public static void SetSortDirection2(this StateBag viewState, SortDirection sortDirection)
		{
			viewState["SD2"] = sortDirection;
		}

		public static void SetSortProperties(this StateBag viewState, string sortExpression, SortDirection sortDirection)
		{
			viewState.SetSortExpression(sortExpression);
			viewState.SetSortDirection(sortDirection);
		}

		public static void SetSortProperties1(this StateBag viewState, string sortExpression, SortDirection sortDirection)
		{
			viewState.SetSortExpression1(sortExpression);
			viewState.SetSortDirection1(sortDirection);
		}

		public static void SetSortProperties2(this StateBag viewState, string sortExpression, SortDirection sortDirection)
		{
			viewState.SetSortExpression2(sortExpression);
			viewState.SetSortDirection2(sortDirection);
		}

		public static void ProcessGridViewSortingEventArgs(this GridViewSortEventArgs e, StateBag viewState)
		{
			if (e.SortExpression != viewState.GetSortExpression())
			{
				viewState.SetSortExpression(e.SortExpression);
				viewState.SetSortDirection(e.SortDirection);
			}
			else
				viewState.SetSortDirection(viewState.GetSortDirection().Toggle());
		}

		public static void ProcessGridViewSortingEventArgs1(this GridViewSortEventArgs e, StateBag viewState)
		{
			if (e.SortExpression != viewState.GetSortExpression1())
			{
				viewState.SetSortExpression1(e.SortExpression);
				viewState.SetSortDirection1(e.SortDirection);
			}
			else
				viewState.SetSortDirection1(viewState.GetSortDirection1().Toggle());
		}

		public static void ProcessGridViewSortingEventArgs2(this GridViewSortEventArgs e, StateBag viewState)
		{
			if (e.SortExpression != viewState.GetSortExpression2())
			{
				viewState.SetSortExpression2(e.SortExpression);
				viewState.SetSortDirection2(e.SortDirection);
			}
			else
				viewState.SetSortDirection2(viewState.GetSortDirection2().Toggle());
		}

		#endregion
	}
}