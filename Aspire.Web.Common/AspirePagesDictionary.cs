﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Aspire.Web.Common
{
	public static class AspirePageAttributesDictionary
	{
		private static readonly IReadOnlyDictionary<Type, AspirePageAttribute> _typeDictionary;
		private static readonly IReadOnlyDictionary<Guid, AspirePageAttribute> _guidDictionary;
		public static IReadOnlyDictionary<Type, AspirePageAttribute> TypeDictionary => _typeDictionary;
		public static IReadOnlyDictionary<Guid, AspirePageAttribute> GuidDictionary => _guidDictionary;
		static AspirePageAttributesDictionary()
		{
			var typeDictionary = new Dictionary<Type, AspirePageAttribute>();
			var guidDictionary = new Dictionary<Guid, AspirePageAttribute>();
			foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies().Where(a => a.FullName.StartsWith("Aspire.")))
			{
				var pages = assembly.GetTypes().Where(t => t.IsClass && !t.IsAbstract && t.IsSubclassOf(typeof(System.Web.UI.Page)));
				foreach (var page in pages)
				{
					if (page.Namespace?.StartsWith("Aspire.Web") != true)
						continue;
					if (!page.IsSubclassOf(typeof(BasePage)))
						throw new InvalidOperationException("Each page in Application must be inherited from Aspire.Web.Common.BasePage. Page: " + page.FullName + ".");
					var aspirePageAttribute = page.GetCustomAttribute<AspirePageAttribute>();
					if (aspirePageAttribute == null)
						throw new InvalidOperationException("AspirePageAttribute is not defined for " + page.FullName + ".");
					if (typeDictionary.Keys.Contains(page))
						throw new InvalidOperationException($"Page already exists. Page: {page}");
					typeDictionary.Add(page, aspirePageAttribute);
					if (guidDictionary.Keys.Contains(aspirePageAttribute.AspirePageGuid))
						throw new InvalidOperationException($"AspirePageGuid already exists. Guid: {aspirePageAttribute.AspirePageGuid}, Page: {page}");
					guidDictionary.Add(aspirePageAttribute.AspirePageGuid, aspirePageAttribute);
				}
			}
			_typeDictionary = typeDictionary;
			_guidDictionary = guidDictionary;
		}

		public static AspirePageAttribute GetAspirePageAttribute<T>() where T : BasePage
		{
			return TypeDictionary[typeof(T)];
		}

		public static AspirePageAttribute GetAspirePageAttribute(this BasePage page)
		{
			return TypeDictionary[page?.GetType().BaseType ?? throw new ArgumentNullException(nameof(page))];
		}

		public static AspirePageAttribute GetAspirePageAttribute(this Guid aspirePageGuid)
		{
			return TryGetAspirePageAttribute(aspirePageGuid) ?? throw new InvalidOperationException($"{aspirePageGuid} is not found in AspirePageAttributes Dictionary.");
		}

		public static AspirePageAttribute TryGetAspirePageAttribute(this Guid aspirePageGuid)
		{
			return GuidDictionary.TryGetValue(aspirePageGuid, out var aspirePageAttribute) ? aspirePageAttribute : null;
		}

		public static AspirePageAttribute GetAspirePageAttribute<T>(this T page) where T : BasePage
		{
			return TypeDictionary[typeof(T)];
		}

		public static AspirePageAttribute GetAspirePageAttribute(this Type type)
		{
			return TypeDictionary[type];
		}

		public static string GetPageUrl<T>() where T : BasePage
		{
			return GetAspirePageAttribute<T>().PageUrl;
		}

		public static string GetPageUrl(this BasePage page)
		{
			return page.GetAspirePageAttribute().PageUrl;
		}

		public static void Initialize()
		{
			var dictionary = TypeDictionary;
		}
	}
}
