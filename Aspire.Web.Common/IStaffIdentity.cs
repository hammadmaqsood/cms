﻿using Aspire.BL.Core.Common.Permissions;
using Aspire.Model.Entities;
using Aspire.Model.Entities.Common;
using System.Collections.Generic;

namespace Aspire.Web.Common
{
	public interface IStaffIdentity : IAspireUserRoleIdentity
	{
		int InstituteID { get; }

		string InstituteCode { get; }

		IReadOnlyDictionary<int, string> Roles { get; }

		UserGroupPermission.PermissionValues? GetStaffPermissionValue(StaffPermissionType staffPermissionType);

		Staff.StaffGroupPermission Demand(StaffPermissionType staffPermissionType, params UserGroupPermission.PermissionValues[] requiredPermissionValueEnums);

		Staff.StaffGroupPermission TryDemand(StaffPermissionType staffPermissionType, params UserGroupPermission.PermissionValues[] requiredPermissionValueEnums);
	}
}